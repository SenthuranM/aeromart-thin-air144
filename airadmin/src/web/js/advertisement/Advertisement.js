jQuery(document).ready(function(){  //the page is ready				
			
			$("#divSearch").decoratePanel("Search Advertisement");
			$("#divResultsPanel").decoratePanel("Advertisement");
			$("#divDispAdvertisement").decoratePanel("Add/Modify Advertisement");			
			$('#btnAdd').decorateButton();
			$('#btnEdit').decorateButton();
			$('#btnDelete').decorateButton();
			$('#btnClose').decorateButton();
			$('#btnReset').decorateButton();
			$('#btnSave').decorateButton();
			$('#btnUpload').decorateButton();
			$('#btnSearch').decorateButton();
			$('#btnView').decorateButton();
			
			disableAdvertisementButton();
			createPopUps();
			PopUpData();
			
			var grdArray = new Array();	
			jQuery("#listAdvertisement").jqGrid({ 
				url:'showAdvertisement!searchAdvertisement.action',
				postData: {
					"advertisementSrch.origin": function() { return $("#selOriginSearch").val(); },
					"advertisementSrch.destination": function() { return $("#selDestinationSearch").val(); },
					"advertisementSrch.title": function() { return $("#srchAdvertisementTitle").val(); },
					"advertisementSrch.language": function() { return $("#selLanguageSearch").val(); }
				},
				datatype: "json",
				jsonReader : {
					  root: "rows", 
					  page: "page",
					  total: "total",
					  records: "records",
					  repeatitems: false,					 
					  id: "0"				  
					},													
				colNames:['&nbsp;','Title','Segment', 'Language','Image','Status','Remarks','version','createdBy','createdDate','modifiedBy','modifiedDate','advertisementId','Image','&nbsp;'], 
				colModel:[ 	{name:'Id', width:20, jsonmap:'id'},   
						 	{name:'advertisementTitle',index:'advertisementTitle', width:220, jsonmap:'advertisement.advertisementTitle'}, 
						   	{name:'advertisementSegmentCode' ,index:'advertisementSegmentCode' ,width:225, align:"center", jsonmap:'advertisementSegmentCode'},
						   	{name:'advertisementLanguage',index:'advertisementLanguage' ,width:90, align:"center",jsonmap:'advertisement.advertisementLanguage'},
						   	{name:'advertisementImage',index:'advertisementImage' ,width:225, hidden:true, align:"center",  jsonmap:'advertisement.advertisementImage'},
						   	{name:'status',index:'status' ,width:90, align:"center", jsonmap:'advertisement.status'},
						   	{name:'advertisementRemarks',index:'advertisementRemarks' ,width:360, align:"center", hidden:true, jsonmap:'advertisement.advertisementRemarks'},
						   	{name:'version', index:'version' ,width:225, align:"center",hidden:true,  jsonmap:'advertisement.version'},						   							   	
						   	{name:'createdBy', index:'createdBy',width:225, align:"center",hidden:true,  jsonmap:'advertisement.createdBy'},
						   	{name:'createdDate', index:'createdDate',width:225, align:"center",hidden:true,  jsonmap:'advertisement.createdDate'},
						   	{name:'modifiedBy', index:'modifiedBy',width:225, align:"center",hidden:true,  jsonmap:'advertisement.modifiedBy'},
						   	{name:'modifiedDate', index:'modifiedDate',width:225, align:"center",hidden:true,  jsonmap:'advertisement.modifiedDate'},
						 	{name:'advertisementId', index:'advertisementId',width:225, align:"center",hidden:true,  jsonmap:'advertisement.advertisementId'},
						 	{name:'imageButton', index:'imageButton',width:40, sortable:false},
						 	{name:'advertisementCheck', index:'advertisementCheck', align:"center", width:15, sortable:false}
						  ], imgpath: '../../themes/default/images/jquery', multiselect:false,
				pager: jQuery('#Advertisementpager'),
				rowNum:20,						
				viewrecords: true,
				height:210,
				width:915,
				loadui:'block',
				onCellSelect : function(rowid, iCol, cellcontent) {
						if(iCol == 13){
							var image = jQuery("#listAdvertisement").getCell(rowid,'advertisementImage');
							var ImagePath = advertisementImagePath;
							res = ImagePath.substring(0 , ImagePath.length - 5);
							$("#showAdvertisementImage").attr('src', res + image);
							showPopUp(1);
						}else if(iCol == 14){
							var id = "advertisementCheck"+rowid;
							var checkBoxSel = document.getElementById(id);
							var advertisementId = jQuery("#listAdvertisement").getCell(rowid,'advertisementId');
							if(checkBoxSel != null && checkBoxSel.checked == true){
								idsOfSelectedAdvertisements.push(rowid);
								advertisementIdArrayToDelete.push(advertisementId);
							}else{
								if(idsOfSelectedAdvertisements.length > 0){
								 var index = idsOfSelectedAdvertisements.indexOf(rowid);
								 var indexID = advertisementIdArrayToDelete.indexOf(advertisementId);
									 if (index > -1 && indexID > -1) {
									 	idsOfSelectedAdvertisements.splice(index, 1);
									 	advertisementIdArrayToDelete.splice(indexID, 1);
									 }
								}
							}
						}
				  },
				onSelectRow: function(rowid){
						fillForm(rowid);
						disableControls(true);
						enableSearch();
						disableAdvertisementButton();			
						enableGridButtons();
						enablePopupControls();
						$("#rowNo").val(rowid);
						$('#btnView').show();
						$("#btnView").attr('disabled', false); 
						var imageName = jQuery("#listAdvertisement").getCell(rowid,'advertisementImage');
						$('#lblFileName').text(imageName);

					},
				gridComplete: function(){ 
				        var ids = jQuery("#listAdvertisement").getDataIDs(); 
				        for(var i=0;i<ids.length;i++){ 
				            var cl = ids[i]; 
				            CheckBox = "<input type="+'"checkbox"'+ "value="+'"'+cl+'"'+"id="+'"advertisementCheck'+cl+'"'+" >";
				            Label = "<label><font>Click Here</font></label>"; 
				            jQuery("#listAdvertisement").setRowData(ids[i],{imageButton:Label}) 
				            jQuery("#listAdvertisement").setRowData(ids[i],{advertisementCheck:CheckBox}) 
				        } 
				    },
				 loadComplete: function (){
					 	selectCheckBoxesForSrch();
				 	}
				  }).navGrid("#Advertisementpager",{refresh: true, edit: false, add: false, del: false, search: true});
			
			var selectedCOS = "";
			var selectedCOSArr = null;
			
			var options = {
				cache: false,	
				beforeSubmit:showRequest,  // pre-submit callback - this can be used to do the validation part 
				success:showResponse,   // post-submit callback	
				error:showError,
				dataType:  'json'        // 'xml', 'script', or 'json' (expected server response type)  
			}; 

			var delOptions = {	
				cache: false, 
				beforeSubmit:  showDelete,  // pre-submit callback - this can be used to do the validation part 
				success: showResponse,   // post-submit callback 				 
				url: 'showAdvertisement!deleteAdvertisement.action',         // override for form's 'action' attribute 
				dataType:  'json',        // 'xml', 'script', or 'json' (expected server response type) 
				resetForm: true        // reset the form after successful submit 
				 
			 }; 

		    
			// pre-submit callback 
			function showRequest(formData, jqForm, options) { 
			    top[2].ShowProgress();
			    return true; 
			} 
			
			function selectCheckBoxesForSrch() { 
				var rows = $("#listAdvertisement")[0].rows;

		        if(idsOfSelectedAdvertisements.length > 0 && idsOfSelectedAdvertisements.length == advertisementIdArrayToDelete.length){
		        	for (i = 0; i < rows.length ; i++) {
		        		advertisementId = jQuery("#listAdvertisement").getCell(rows[i].id,'advertisementId');
		        		index = advertisementIdArrayToDelete.indexOf(advertisementId);
		        		if(index > -1){
				        	var id = "advertisementCheck"+rows[i].id;
							document.getElementById(id).checked = true;
		        		}
			        }
		        }
		        
			} 
			
			function showResponse(responseText, statusText)  {
				top[2].HideProgress();
//				clearForm();

				showCommonError(responseText['msgType'], responseText['succesMsg']);
			    if(responseText['msgType'] != "Error") {
			    	$('#frmAdvertisement').clearForm();
			    	jQuery("#listAdvertisement").trigger("reloadGrid");
			    	disableAdvertisementButton();
			    	$("#btnAdd").attr('disabled', false); 
			    	$("#btnSave").disableButton();
			    	disableForm(true);
			    	disableAdvertisementButton();
			    	alert(responseText['succesMsg']);
					idsOfSelectedAdvertisements = [];
					advertisementIdArrayToDelete = [];
					clearForm();
					selectCheckBoxesForSrch();
			    } else {
			    	jQuery("#listAdvertisement").trigger("reloadGrid");
			    	selectCheckBoxesForSrch();
			    }
			} 	
			function showError(responseText, statusText) { 
				    showCommonError("Error", "Internat Server Error please contact the Admin");
				    return true; 
			} 

			function showDelete(formData, jqForm, options) {				
				return confirm(deleteRecoredCfrm); 
			} 
			
			function disableForm(cond){
				$("#advertisementTitle").attr('disabled',cond);
				$("#advertisementLanguage").attr('disabled',cond);
				$("#advertisementOrigin").attr('disabled',cond);
				$("#advertisementDestination").attr('disabled',cond);
				$("#advertisementVia1").attr('disabled',cond);
				$("#advertisementVia2").attr('disabled',cond);
				$("#advertisementVia3").attr('disabled',cond);
				$("#advertisementVia4").attr('disabled',cond);
				$("#AdvertisementImage").attr('disabled',cond);
				$("#selStatus").attr('disabled',cond);
				$("#btnSave").attr('disabled',cond); 	
			}
			function searchValidation(){
				if($("#selOriginSearch").val() == $("#selDestinationSearch").val()){
					if($("#selOriginSearch").val() == ""){
						return true;
					}else if($("#selDestinationSearch").val() == ""){
						return true;
					}else{
						showCommonError("Error", originDestinationSame);
						return false;
					}
				}
				return true;
			}
			
			function validateAdvertisement(){
	
				if(	trim($("#advertisementTitle").val()) == "") {			    	
			    	showCommonError("Error",advertisementTitleRqrd);
			    	return false; 
			    }
			    if( trim($('#advertisementLanguage>option:selected').text()) == "") {
			    	showCommonError("Error", languageRqrd);
			    	return false; 
			    }
			    if(	trim($('#advertisementSegmentCode>option:selected').text()) == "") {			    	
			    	showCommonError("Error", segmentRqrd);
			    	return false; 
			    }
			    return true;
			}
			$('#btnSearch').click(function() {
				if(searchValidation()){
					var newUrl = 'showAdvertisement!searchAdvertisement.action?';
				 	newUrl += 'advertisementSrch.origin=' + $("#selOriginSearch").val();
				 	newUrl += '&advertisementSrch.destination=' + $("#selDestinationSearch").val();
				 	newUrl += '&advertisementSrch.title=' + $("#srchAdvertisementTitle").val();
				 	newUrl += '&advertisementSrch.language=' + $("#selLanguageSearch").val();
				 	
				 	$("#listAdvertisement").clearGridData();
				 	$.getJSON(newUrl, function(response){
						$("#listAdvertisement")[0].addJSONData(response);
						selectCheckBoxesForSrch();
				 	});	
				}
			});
			
			$('#btnSave').click(function() {
				
				var seg = getAllSegments().substr(0, getAllSegments().length - 1);

				var ImageData = $("#advertisementImage").val();
				
				var fileValue = $("#uploadImage").val().split('/').pop().split('\\').pop();
				if(fileValue != ""){
					setField("advertisementImage", fileValue);
				}
				if(seg != null){
					setField("advertisementSegmentCode", seg);
				}
				if(getText("hdnMode") == 'EDIT'){
					$("#advertisementTitle").attr('disabled', true);
					$("#advertisementLanguage").attr('disabled', true);
					if(validateAdvertisement()){
						var fileValue = $("#editUploadImage").val().split('/').pop().split('\\').pop();
						if(fileValue != ""){
							setField("editAdvertisementImage", fileValue);
						}
						alert("Saving Advertisement changes will update Advertisements on all flights. This will take some time");				
						setField("advertisementImage",ImageData);
						$("#advertisementTitle").attr('disabled', false);
						$("#advertisementLanguage").attr('disabled', false);
						$('#frmAdvertisement').ajaxSubmitWithFileInputs(options);
						return true;
					}
				}else{	
					disableControls(false);
					if(getText("hdnModel") == 'ADD') {
						setField("hdnMode","ADD");
						if(	trim($("#uploadImage").val()) == "") {			    	
					    	showCommonError("Error", bannerRqrd);
					    	return false; 
					    }
					}
					if(validateAdvertisement()){
						alert("Saving Advertisement changes will update Advertisements on all flights. This will take some time");				
						$('#frmAdvertisement').ajaxSubmitWithFileInputs(options);
						return true;
					}
				}
				return false;	
						
			});
			 
			$('#btnAdd').click(function() {
				disableControls(false);
				$("#addAnchor").show();
				$("#removeAnchor").show();
				$("#addAnchorSpace").hide();
				$("#removeAnchorSpace").hide();
				$("#btnView").hide();
				$("#listAdvertisement").jqGrid("resetSelection");
				$('#advertisementSegmentCode').find('option').remove();
				$('#frmAdvertisement').clearForm();		
				$("#imgAdvertisement").attr('src', '');
				$("#advertisementSegmentCode").enableButton();
				$("#version").val('-1');
				$("#rowNo").val('');
				$('#dvUpLoad').html($('#dvUpLoad').html());
				$('#btnEdit').disableButton();
				$('#btnDelete').disableButton();
				$('#btnUpload').enableButton();
				$("#btnSave").enableButton();
				$('#dvUpLoad').show();
				$('#dvEditUpLoad').hide();
				$("#btnReset").enableButton();
				setField("hdnMode","ADD");
				setField("hdnModel","ADD");
				$("#status").val('ACT');
				$("#lblFileName").text("");
			}); 	

			
			$('#btnEdit').click(function() {
				disableControls(false);	
				editDisableControls(true);
				$("#addAnchor").show();
				$("#removeAnchor").show();
				$("#addAnchorSpace").hide();
				$("#removeAnchorSpace").hide();
				$("#btnView").show();
				$('#btnDelete').disableButton();
				$("#btnSave").enableButton();
				$('#uploadImage').attr('disabled', true);
				$('#advertisementTitle').attr('disabled', true);
				$("#btnReset").enableButton();
				$('#dvEditUpLoad').show();
				$('#dvUpLoad').hide();
				setField("hdnMode","EDIT");
			}); 
			
			$('#btnReset').click(function() {
				if($('#rowNo').val() == '') {
					$('#frmAdvertisement').resetForm();
					$('#imgAdvertisement').attr('src', '');
					$('#dvUpLoad').html($('#dvUpLoad').html());
					clearForm();
				}else {
					rowid = $("#rowNo").val();
					mode = $("#hdnMode").val();
					fillForm($("#rowNo").val());
					$("#rowNo").val(rowid);
					$("#hdnMode").val(mode);
					$('#dvEditUpLoad').show();
					$('#dvUpLoad').hide();
				}
					
			});
			$('#btnDelete').click(function() {
				disableControls(false);
				if(idsOfSelectedAdvertisements.length > 0){
					if(advertisementIdArrayToDelete.length == idsOfSelectedAdvertisements.length){
						var stringData = JSON.stringify(advertisementIdArrayToDelete);
						setField("advertisementDeleteId",stringData);
						$('#frmAdvertisement').ajaxSubmit(delOptions);

					}else{
						showCommonError("Error",reloadPageForCheckBox);
					}
				}else{
					showCommonError("Error",selCheckBox);
				}
				disableForm(true);
				return false;			
			}); 
			
			function fillForm(rowid) {
				setField("hdnMode","");
				clearForm();
				jQuery("#listAdvertisement").GridToForm(rowid, '#frmAdvertisement');
				var advertisementSegmentCode = jQuery("#listAdvertisement").getCell(rowid,'advertisementSegmentCode');
				setAllSegments(advertisementSegmentCode);
				$('#dvEditUpLoad').hide();
				$('#dvUpLoad').show();
				if(jQuery("#listAdvertisement").getCell(rowid,'status') == 'ACT'){
					$("#status").val("ACT");								
				}else {																
					$("#status").val("INA");								
				}														
			}
			
			
			function  disableControls(cond){
				$("input").each(function(){ 
				      $(this).attr('disabled', cond); 
				});	
				$("textarea").each(function(){ 
				      $(this).attr('disabled', cond); 
				});	
				$("select").each(function(){ 
				      $(this).attr('disabled', cond); 
				});
				$("file").each(function(){ 
				      $(this).attr('disabled', cond); 
				});	
				$("#btnClose").enableButton();
				$('input[type=checkbox]').each(function () {
					 $(this).attr('disabled', false);
				});
				
			}
			
			function  editDisableControls(cond){  
				$('#advertisementTitle').attr("disabled", cond);
				$('#uploadImage').attr("disabled", cond);
			}
			
			function enableSearch(){
				$('#selOriginSearch').attr("disabled", false); 	
				$('#selDestinationSearch').attr("disabled", false); 
				$('#srchAdvertisementTitle').attr("disabled", false); 
				$('#selLanguageSearch').attr("disabled", false); 
				$('#btnSearch').enableButton();
			}
			
			function disableAdvertisementButton(){
				$("#btnEdit").disableButton();
				$("#btnDelete").disableButton();
				$("#btnReset").disableButton();
				$("#btnSave").disableButton();
				$("#btnView").hide();
				$("#addAnchor").hide();
				$("#removeAnchor").hide();
				$("#addAnchorSpace").show();
				$("#removeAnchorSpace").show();

			}

			function enableGridButtons(){
				$("#btnAdd").enableButton();
				$("#btnEdit").enableButton();
				$("#btnDelete").enableButton();
			}
			
			function enablePopupControls(){			
				$("#language").enableButton();
				$("#add").enableButton();
				$("#remove").enableButton();
				$("#translation").enableButton();
				$("#txtAdvertisementForDisplayOL").enableButton();
				$("#txtAdvertisementTitleForDisplayOL").enableButton();				
				$("#btnPopupClose").enableButton();
			}
			disableControls(true);
			$("#btnAdd").enableButton();
			$('#selOriginSearch').attr("disabled", false); 	
			$('#selDestinationSearch').attr("disabled", false); 
			$('#srchAdvertisementTitle').attr("disabled", false); 
			$('#selLanguageSearch').attr("disabled", false); 
			$('#btnSearch').enableButton();

			function PopUpData(){
				var html = '<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0"> <tr><td> <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4"> <tr><td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td> <td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader"> Advertisement For Display </font></td> <td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td> </tr><tr><td class="FormBackGround"></td><td class="FormBackGround" valign="top"> <table><tr><td width="663" align="left"><table width="98%" height="105"  border="0" cellpadding="0" cellspacing="0"> <tr align="left" valign="middle" class="fntMedium"> <td width="19%" class="fntMedium"></td> <td width="33%" align="left" style="padding-bottom:3px; ">';		
					html += '</td><img  name="showAdvertisementImage" id= "showAdvertisementImage" src="" alt="" height="150"><td width="13%" rowspan="3" align="center" valign="top"></td> <td width="35%" rowspan="3" align="left" valign="top">';
					html +=	'</td> </tr><tr><td align="top"><span id="spnSta"></span></td></tr> <tr><td align="right">&nbsp;</td></tr></table> </td><td class="FormBackGround"></td></tr><tr> <td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td> <td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td> <td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td> </tr></table> </td></tr></table>';

				DivWrite("spnPopupData",html);
			}
		
});

var fileAllowed = false ;
		
		function setPageEdited(isEdited) {
			top[1].objTMenu.tabPageEdited(screenId, isEdited);
		}	

		function addToList() {
			var isContained = false;
			top[2].HidePageMessage();
			var dept = getValue("advertisementOrigin");
			var arr = getValue("advertisementDestination");
			var via1 = getValue("advertisementVia1");
			var via2 = getValue("advertisementVia2");
			var via3 = getValue("advertisementVia3");
			var via4 = getValue("advertisementVia4");

			if (dept == "") {
				showCommonError("Error", depatureRequired);
				getFieldByID("advertisementOrigin").focus();

			} else if (arr == "") {
				showCommonError("Error", arrivalRequired);
				getFieldByID("advertisementDestination").focus();

			// restricted adding ***/***
			} else if ((dept == arr) && (dept == '***' && via1 == "")) {
				showCommonError("Error", invalidOND);
				getFieldByID("advertisementOrigin").focus();

			} else if (dept == arr && dept != '***') {
				showCommonError("Error", depatureArriavlSame);
				getFieldByID("advertisementOrigin").focus();

			} else if (arr == via1 || arr == via2 || arr == via3 || arr == via4) {
				showCommonError("Error", depatureArriavlSame);
				getFieldByID("advertisementOrigin").focus();

			} else if (dept == via1 || dept == via2 || dept == via3 || dept == via4) {
				showCommonError("Error", arriavlViaSame);
				getFieldByID("advertisementDestination").focus();

			} else if ((via2 != "" || via3 != "" || via4 != "") && via1 == "") {
				showCommonError("Error", depatureViaSame);
				getFieldByID("advertisementVia1").focus();

			} else if ((via3 != "" || via4 != "") && via2 == "") {
				showCommonError("Error", vianotinSequence);
				getFieldByID("advertisementVia2").focus();

			} else if ((via4 != "") && via3 == "") {
				showCommonError("Error", vianotinSequence);
				getFieldByID("advertisementVia3").focus();

			} else if ((via1 != "") && (via1 == via2 || via1 == via3 || via1 == via4)) {
				showCommonError("Error", "via Same");
				getFieldByID("advertisementVia1").focus();

			} else if ((via2 != "") && (via2 == via3 || via2 == via4)) {
				showCommonError("Error", viaSame);
				getFieldByID("advertisementVia2").focus();

			} else if ((via3 != "") && (via3 == via4)) {
				showCommonError("Error", viaSame);
				getFieldByID("advertisementVia3").focus();
			} else {
				var str = "";
				str += dept;

				if (via1 != "") {
					str += "/" + via1;
				}
				if (via2 != "") {
					str += "/" + via2;
				}
				if (via3 != "") {
					str += "/" + via3;
				}
				if (via4 != "") {
					str += "/" + via4;
				}

				str += "/" + arr;

				var control = document.getElementById("advertisementSegmentCode");
				for ( var r = 0; r < control.length; r++) {
					if (control.options[r].text == str) {
						isContained = true;
						showERRMessage(OnDExists);
						break;
					}
				}
				if (isContained == false) {
					control.options[control.length] = new Option(str, str);
					clearStations();
				}
			}
		}
		
		function removeFromList() {
			var control = document.getElementById("advertisementSegmentCode");

			var selIndex = control.selectedIndex;
			if (selIndex != -1) {
				for (i = control.length - 1; i >= 0; i--) {
					if (control.options[i].selected) {
						control.options[i] = null;
					}
				}
				if (control.length > 0) {
					control.selectedIndex = selIndex == 0 ? 0 : selIndex - 1;
					clearStations();
				}
			}
		}
		
		function clearStations() {
			getFieldByID("advertisementOrigin").value = '';
			getFieldByID("advertisementDestination").value = '';
			getFieldByID("advertisementVia1").value = '';
			getFieldByID("advertisementVia2").value = '';
			getFieldByID("advertisementVia3").value = '';
			getFieldByID("advertisementVia4").value = '';
		}
		
		function getAllSegments() {
			var control = document.getElementById("advertisementSegmentCode");
			var values = "";
			for ( var t = 0; t < (control.length); t++) {
				values += control.options[t].text + ",";
			}
			return values;
		}

		function setAllSegments(segs) {
			var control = document.getElementById("advertisementSegmentCode");
			var selSegs = segs.split(",");

			for ( var i = 0; i < selSegs.length; i++) {
				if (trim(selSegs[i]) != "") {
					control.options[control.length] = new Option(selSegs[i], selSegs[i]);
				}
			}
		}
		
		function clearForm() {
			$('#frmAdvertisement').resetForm();
			$('#dvUpLoad').html($('#dvUpLoad').html());
			getFieldByID("advertisementOrigin").value = '';
			getFieldByID("advertisementDestination").value = '';
			$('#advertisementSegmentCode')
		    .find('option')
		    .remove();
			$('#lblFileName').text("");

		}
		function disableTitleAndLang() {
			 $("#btnSave").disableButton();
 			  $("#advertisementTitle").attr('disabled', true);
			  $("#advertisementLanguage").attr('disabled', true);
		}

		function setFileName(id){
			 var input = document.getElementById(id);
			 var fileName = input.files[0].name;
			 $("#lblFileName").text(fileName);


		}

		function imageValidate(id){ // validate the pixel size and the maximum size of the image 2MB
			 if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
			      alert('The File APIs are not fully supported in this browser.');
			      return;
			    }   
			 input = document.getElementById(id);
			 if (!input) {
			      alert("Um, couldn't find the fileinput element.");
			    }
			 else if (!input.files) {
			      alert("This browser doesn't seem to support the `files` property of file inputs.");
			 }
			 else if (!input.files[0]) {
			      alert("Please select a file before clicking 'Load'");               
			 }
			 else {
			      file = input.files[0];
			      fr = new FileReader();
			      
			      var URL = window.URL || window.webkitURL,
			      imageUrl,
			      image;

			  if (URL) {
			      imageUrl = URL.createObjectURL(file);
			      image = document.getElementById('validateImage');
			      $("#btnSave").enableButton();
			      image.onload = function() {
			          URL.revokeObjectURL(imageUrl);
//			          alert(this.width + 'x' + this.height + "file size"  + file.size);
			          if(!((this.width < advertisementImageMaxWidth && this.width > advertisementImageMinWidth) && (this.height < advertisementImageMaxHeight && this.height > advertisementImageMinHeight))){
			        	  if(getText("hdnMode") == 'EDIT')
			        		  $('#dvEditUpLoad').html($('#dvEditUpLoad').html());
			        	  else
			        		  $('#dvUpLoad').html($('#dvUpLoad').html());
			  			  image.src = "";  
			  			  pixelNotAllowed = pixelNotAllowed.replace("$1", advertisementImageMinHeight).replace("$2", advertisementImageMaxHeight).replace("$3", advertisementImageMinWidth).replace("$4", advertisementImageMaxWidth);
			  			  showCommonError("Error", pixelNotAllowed);
			  			  disableTitleAndLang();
			  			  getFieldByID(id).focus();
			          }
			          
			          if(!(file.size <= advertisementImageSize * 1024 * 1024)){
			        	  image.src = "";
			        	  sizeNotAllowed.replace("$1", advertisementImageSize);
			        	 showCommonError("Error", sizeNotAllowed);
			        	 disableTitleAndLang();
				  		 getFieldByID(id).focus();
			          }
			          
			      };
			      image.src = imageUrl;      
			  }
			 }
		}
		
		function showImage(){
			var rowid = $("#rowNo").val();
			if(rowid != undefined){
				var image = jQuery("#listAdvertisement").getCell(rowid,'advertisementImage');
				var ImagePath = advertisementImagePath;
				res = ImagePath.substring(0 , ImagePath.length - 5);
				$("#showAdvertisementImage").attr('src', res + image);
				showPopUp(1);
			}
		}

		var idsOfSelectedAdvertisements = new Array();	
		var advertisementIdArrayToDelete = new Array();	
		
		