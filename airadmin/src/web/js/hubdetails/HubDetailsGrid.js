/*
 *********************************************************
	Description		: Hub Details
	Author			: Nadika Dhanusha Mahatantila
	Created on		: Feb 19, 2008  
 *********************************************************	
 */

var screenId = "SC_ADMN_024";
var valueSeperator = "~";

if (getTabValues(screenId, valueSeperator, "intLastRec") == "") {
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
}

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "12%";
objCol1.arrayIndex = 1;
objCol1.headerText = "Hub";
objCol1.itemAlign = "center"

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "17%";
objCol2.arrayIndex = 2;
objCol2.headerText = "Min Connection Time";
objCol2.itemAlign = "center"

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "17%";
objCol3.arrayIndex = 3;
objCol3.headerText = "Max Connection Time";
objCol3.itemAlign = "center"

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "17%";
objCol4.arrayIndex = 4;
objCol4.headerText = "Inward Carrier Code";
objCol4.itemAlign = "center"

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "17%";
objCol5.arrayIndex = 5;
objCol5.headerText = "Outward Carrier Code";
objCol5.itemAlign = "center"

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "12%";
objCol6.arrayIndex = 6;
objCol6.headerText = "Status";
objCol6.itemAlign = "left"

// ---------------- Grid
var objDG = new DataGrid("spnHubDetails");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol6);

objDG.width = "99%";
objDG.height = "200px"; // "234px";
objDG.seqNoWidth = "8%";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = arrData;
objDG.seqNo = true;
objDG.seqStartNo = getTabValues(screenId, valueSeperator, "intLastRec"); // Grid
objDG.pgnumRecTotal = totalRecords; // parent.arrData.length; // remove as per
									// return record size
objDG.paging = true
objDG.pgnumRecPage = 20;
objDG.pgonClick = "gridNavigations";
objDG.rowClick = "rowClick";
objDG.displayGrid();

function myRecFunction(intRecNo, intErrMsg) {
	if (intErrMsg != "") {
		objMsg.MessageText = "<li> " + intErrMsg;
		objMsg.MessageType = "Error";
		ShowPageMessage();
	}
}
