/*
 *********************************************************
	Description		: Hub Details
	Author			: Nadika Dhanusha Mahatantila
	Created on		: Feb 19, 2008  
 *********************************************************	
 */

var strRowData;
var strGridRow;
var intLastRec;
var intStat = 0;

var screenId = "SC_ADMN_024";
var valueSeperator = "~";
var srchValueSeperator = ":";

/*
 * Page Button Clicks
 */
function pgBtnClick(strID) {
	switch (strID) {
	case 0:
		// clear
		resetClick();
		break;

	case 1:
		// Add
		addClick();
		break;

	case 2:
		// Save
		saveHubDetails();
		break;

	case 3:
		// Delete
		deleteClick();
		break;

	case 4:
		// close
		top[1].objTMenu.tabRemove(screenId);
		break;

	case 5:
		// Edit
		editClick();
		break;

	case 6:
		// search
		searchHubDetails();
		break;
	}
}

// On window load
function winOnLoad(strMsg, strMsgType) {
	clearControls();
	disableControls(true);
	setPageEdited(false);
	setField("hdnModel", "DISPLAY");

	if (top[1].objTMenu.focusTab == screenId) {
		setDefaultFocus();
	}

	var strTemp = getTabValues(screenId, valueSeperator, "strSearchCriteria");

	if (strTemp != null && strTemp != "") {
		if (strTemp.indexOf(srchValueSeperator) != -1) {
			var arrTemp = strTemp.split(srchValueSeperator);

			for (i = 0; i < arrSearch.length; i++) {
				setField(arrSearch[i][0], arrTemp[i]);
			}
		}
	}

	if (arrFormData != null && arrFormData.length > 0) {
		strGridRow = getTabValues(screenId, valueSeperator, "strGridRow"); // Get
																			// saved
																			// grid
																			// Row
		setPageEdited(true);

		for (i = 0; i < arrControls.length; i++) {
			setField(arrControls[i][0], arrFormData[0][i + 1]);

			// Status checkbox
			if (i == 5) {
				if (arrFormData[0][i + 1] == "Active") {
					getFieldByID(arrControls[i][0]).checked = true;
				} else {
					getFieldByID(arrControls[i][0]).checked = false;
				}
			}
		}
		setField("hdnVersion", arrFormData[0][7]);
		setField("hdnId", arrFormData[0][8]);
		setField("hdnHubCode", arrFormData[0][1]);
		disableControls(false);

		if (!isAddMode) {
			if (top[1].objTMenu.focusTab == screenId) {
				document.getElementById(arrControls[1][0]).focus();
			}
			setField("hdnMode", "EDIT");
			Disable(arrControls[0][0], true);

		} else {
			if (top[1].objTMenu.focusTab == screenId) {
				document.getElementById(arrControls[0][0]).focus();
			}
			setField("hdnMode", "ADD");
		}

		Disable("btnSave", false);
	}

	if (saveSuccess == 1) {
		// alert("Record Successfully saved!");
		saveSuccess = 0;
	}

	showCommonError(strMsgType, strMsg);
}

function searchHubDetails() {
	objOnFocus();

	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setField("hdnMode", "SEARCH");
		setField("hdnModel", "");

		// Search Data
		var hdnSearch = "";
		for ( var i = 0; i < arrSearch.length; i++) {
			hdnSearch += getText(arrSearch[i][0]) + srchValueSeperator; // 0 - N
		}

		setTabValues(screenId, valueSeperator, "strSearchCriteria", hdnSearch);
		setTabValues(screenId, valueSeperator, "intLastRec", 1);
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}

// on Grid Row click
function rowClick(strRowNo) {

	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {

		clearControls();
		intStat = 1;
		objOnFocus();
		disableControls(true);
		Disable("btnAdd", "");
		Disable("btnEdit", "");
		Disable("btnDelete", "");

		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;

		for (i = 0; i < arrControls.length; i++) {
			setField(arrControls[i][0], strRowData[i]);

			// Status checkbox
			if (i == 5) {
				if (arrData[strRowNo][i + 1] == "Active") {
					getFieldByID(arrControls[i][0]).checked = true;
				} else {
					getFieldByID(arrControls[i][0]).checked = false;
				}
			}
		}
		setField("hdnId", arrData[strRowNo][0]);
		setField("hdnVersion", arrData[strRowNo][7]);
		setField("hdnHubCode", strRowData[0]);

		if (arrData[strRowNo][8] == 'N') {
			Disable("btnDelete", true);
		} else {
			Disable("btnDelete", false);
		}

		setPageEdited(false);
	}
}

// on Add Button click
function addClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		objOnFocus();
		setPageEdited(false);
		clearControls();
		disableControls(false);
		intStat = 0;
		Disable("btnEdit", true);
		Disable("btnDelete", true);
		Disable("btnSave", false);
		setField("hdnMode", "ADD");
		setField("hdnModel", "SAVE");
		setField("hdnVersion", "");
		setField("hdnId", "");
		setField("hdnHubCode", "");
		getFieldByID(arrControls[0][0]).focus();
		getFieldByID(arrControls[5][0]).checked = true;
	}
}

// on Edit Button CLick
function editClick() {
	objOnFocus();
	if (intStat == 1) {
		disableControls(false);
		Disable(arrControls[0][0], true);
		getFieldByID(arrControls[1][0]).focus();

		Disable("btnDelete", true);
		Disable("btnEdit", false);
		Disable("btnSave", false);

		setField("hdnMode", "EDIT");
		setField("hdnModel", "SAVE");
	} else {
		showERRMessage(errSelectHubToEdit);
	}
}

// on Delete Button click
function deleteClick() {
	objOnFocus();
	if (intStat == 1) {
		var strConf = confirm(deleteConf);

		if (strConf == true) {
			setField("hdnModel", "DELETE");

			setTabValues(screenId, valueSeperator, "intLastRec", 1); // Delete
			setField("hdnRecNo", getTabValues(screenId, valueSeperator,
					"intLastRec"));

			// Search Data
			var hdnSearch = "";
			for ( var i = 0; i < arrSearch.length; i++) {
				hdnSearch += getText(arrSearch[i][0]) + srchValueSeperator; // 0 -
																			// N
			}

			setTabValues(screenId, valueSeperator, "strSearchCriteria",
					hdnSearch);
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	} else {
		showERRMessage(errSelectHubToDelete);
	}
}

// on Save Button click
function saveHubDetails() {
	var strHub = getValue(arrControls[0][0]);
	var strMinConnectTime = getValue(arrControls[1][0]);
	var strMaxConnectTime = getValue(arrControls[2][0]);

	setField("hdnRecNo", getTabValues(screenId, valueSeperator, "intLastRec"));

	/*
	 * Mandatory Fields check
	 */
	// Validate the Hub
	if (strHub == null || strHub == "") {
		showERRMessage(errDropdownRequired + " " + arrControls[0][1]);
		getFieldByID(arrControls[0][0]).focus();
		return;
	}

	// Validate the Min Connection Time
	if (strMinConnectTime == null || strMinConnectTime == "") {
		showERRMessage(buildError(errFieldRequired, arrControls[1][1]));
		getFieldByID(arrControls[1][0]).focus();
		return;
	}

	// Validate the Max Connection Time
	if (strMaxConnectTime == null || strMaxConnectTime == "") {
		showERRMessage(buildError(errFieldRequired, arrControls[2][1]));
		getFieldByID(arrControls[2][0]).focus();
		return;
	}

	/*
	 * Min & Max Connection Time other validations
	 */
	// Zero validation
	if (!validateZeroForConnectionTime()) {
		return false;
	}

	// Validate the time format
	if (!validateConnectionTimeFormat()) {
		return false;
	}
	
	if(!validateMinMaxConnectionTime()){
		return false;
	}

	var strMode = getText("hdnMode");
	disableControls(false);

	setField("hdnModel", "SAVE");
	setTabValues(screenId, valueSeperator, "strGridRow", strGridRow); // Save
																		// the
																		// grid
																		// Row

	// Search Data
	var hdnSearch = "";
	for ( var i = 0; i < arrSearch.length; i++) {
		hdnSearch += getText(arrSearch[i][0]) + srchValueSeperator; // 0 - N
	}

	setTabValues(screenId, valueSeperator, "strSearchCriteria", hdnSearch);
	document.forms[0].submit();
	top[2].ShowProgress();

}


function validateMinMaxConnectionTime(){
	function getMinutes(timeStr){
		var timePat = /^(\d{1,}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
		var matchArray = timeStr.match(timePat);		
		return (parseInt(matchArray[1],10)*60 + parseInt(matchArray[2],10));
		
	}

	var min = getMinutes(getText(arrControls[1][0]));
	var max = getMinutes(getText(arrControls[2][0]));

	if(min > max){
		showERRMessage(buildError(errInvalidConnectionTimeValues, arrControls[1][1], arrControls[2][1]));
		return false;
	}
	return true;
}

// Reset Button Click
function resetClick() {
	var strMode = getText("hdnMode");

	if (strMode == "ADD") {
		clearControls();
		getFieldByID(arrControls[5][0]).checked = true;
		setPageEdited(false);
		Disable("btnSave", false);
		getFieldByID(arrControls[0][0]).focus();
	}

	if (strMode == "EDIT") {
		if (strGridRow < arrData.length
				&& arrData[strGridRow][1] == getText(arrControls[0][0])) {

			for (i = 0; i < arrControls.length; i++) {
				setField(arrControls[i][0], arrData[strGridRow][i + 1]);

				// Status checkbox
				if (i == 5) {
					if (arrData[strGridRow][i + 1] == "Active") {
						getFieldByID(arrControls[i][0]).checked = true;
					} else {
						getFieldByID(arrControls[i][0]).checked = false;
					}
				}
			}

			setField("hdnId", arrData[strGridRow][0]);
			setField("hdnVersion", arrData[strGridRow][7]);
			setField("hdnHubCode", arrData[strGridRow][1]);
			setPageEdited(false);
			Disable("btnSave", false);
			getFieldByID(arrControls[1][0]).focus();

		} else {
			// Grid row not found. Clear it off.
			clearControls();
			disableControls(true);
			setField("hdnModel", "DISPLAY");
			setPageEdited(false);
			buttonSetFocus("btnAdd");
			Disable("btnEdit", "true");
			Disable("btnDelete", "true");
			Disable("btnSave", "true");
			Disable("btnReset", "true");
		}
	}
	objOnFocus();
}

// To Handle Paging
function gridNavigations(intRecNo, intErrMsg) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		objOnFocus();
		if (intRecNo <= 0)
			intRecNo = 1;

		setField("hdnModel", "DISPLAY");
		setTabValues(screenId, valueSeperator, "intLastRec", intRecNo);
		setField("hdnRecNo", getTabValues(screenId, valueSeperator,
				"intLastRec"));

		if (intErrMsg != "") {
			top[2].objMsg.MessageText = "<li> " + intErrMsg;
			top[2].objMsg.MessageType = "Error";
			top[2].ShowPageMessage();
		} else {

			// Search Data
			var hdnSearch = "";
			for ( var i = 0; i < arrSearch.length; i++) {
				hdnSearch += getText(arrSearch[i][0]) + srchValueSeperator; // 0 -
																			// N
			}

			setTabValues(screenId, valueSeperator, "strSearchCriteria",
					hdnSearch);
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	}
}

function objOnFocus() {
	top[2].HidePageMessage();
}

/*
 * Clear Controls
 */
function clearControls() {
	for ( var i = 0; i < arrControls.length; i++) {
		setField(arrControls[i][0], "");
	}
}

/*
 * Disable Controls
 */
function disableControls(blnStatus) {
	for ( var i = 0; i < arrControls.length; i++) {
		Disable(arrControls[i][0], blnStatus);
	}
}

/*
 * Default Focus
 */
function setDefaultFocus() {
	getFieldByID(arrSearch[0][0]).focus();
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
	if (isEdited) {
		Disable("btnSave", "");
	} else {
		Disable("btnSave", "false");
	}
}

function clickChange() {
	objOnFocus();
	setPageEdited(true);
}

function MinConnectTimePress() {
	objOnFocus();
	setPageEdited(true);
	var strCR = getText(arrControls[1][0]);
	var strLen = strCR.length;
	var blnEmpty = isEmpty(strCR);

	if (blnEmpty) {
		setField(arrControls[1][0], strCR.substr(0, strLen - 1));
		getFieldByID(arrControls[1][0]).focus();
	}
}

function MaxConnectTimePress() {
	objOnFocus();
	setPageEdited(true);
	var strCR = getText(arrControls[2][0]);
	var strLen = strCR.length;
	var blnEmpty = isEmpty(strCR);

	if (blnEmpty) {
		setField(arrControls[2][0], strCR.substr(0, strLen - 1));
		getFieldByID(arrControls[2][0]).focus();
	}
}

/*
 * Zero validation for Time
 */
function validateZeroForConnectionTime() {
	for (i = 1; i <= 2; i++) {
		if ((getText(arrControls[i][0]) == "0:00")
				|| (getText(arrControls[i][0]) == "00:00")
				|| (getText(arrControls[i][0]) <= 0)) {
			showERRMessage(buildError(errZeroConnectionTime, arrControls[i][1]));
			getFieldByID(arrControls[i][0]).focus();
			return false;
		}
	}

	return true;
}

/*
 * Validate the time format
 */
function validateConnectionTimeFormat() {
	for (i = 1; i <= 2; i++) {
		objOnFocus();
		clickChange();
		var strCR = getText(arrControls[i][0]);
		var blnVal = IsValidTime(trim(strCR));

		if (!blnVal) {
			showERRMessage(buildError(errInvalidConnectionTimeFormat,
					arrControls[i][1]));
			getFieldByID(arrControls[i][0]).focus();
			return false;
		}
	}

	return true;
}

function IsValidTime(timeStr) {

	var timePat = /^(\d{1,}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
	var matchArray = timeStr.match(timePat);
	if (matchArray == null) {
		// alert("Time is not in a valid format.");
		return false;
	}
	hour = matchArray[1];
	minute = matchArray[2];
	second = matchArray[4];
	ampm = matchArray[6];

	if (second == "") {
		second = null;
	}

	if (minute < 0 || minute > 59) {
		// alert ("Minute must be between 0 and 59.");
		return false;
	}
	if (second != null && (second < 0 || second > 59)) {
		// alert ("Second must be between 0 and 59.");
		return false;
	}
	return true;
}

/*
 * Builds the Error msg
 */
function buildError(strMessage) {
	if (arguments.length > 1) {
		for ( var i = 0; i < arguments.length - 1; i++) {
			strMessage = strMessage.replace("#" + (i + 1), arguments[i + 1]);
		}
		strMessage = strMessage
	}
	return strMessage;
}
