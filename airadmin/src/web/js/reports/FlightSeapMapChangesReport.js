var objWindow;
var value;
var screenId="UC_REPM_084";
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent){
	objCal1.ID = strID;
	objCal1.top = 150 ;
	objCal1.left = 0 ;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function winOnLoad() {
	getFieldByID("txtFromDate").focus();

}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function viewClick() {
	top[2].HidePageMessage(); 
	if(validateFields()){
	setField("hdnMode", "VIEW");
	setField("selFlightOverbookType", "ShowOverbook");
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
	top[0].objWindow = window.open("about:blank", "CWindow", strProp);
	var objForm = document.getElementById("frmSeatMapChangesReport");
	objForm.target = "CWindow";
	objForm.action = "showFlightSeatMapChanges.action?hdnLive=OFFLINE";
	objForm.submit();
	}
}

function  validateFields(){
	if(getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
		return false;
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
		return false;
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
		return false;
	} else if (compareDate(getText("txtFromDate"),getText("txtToDate")) != -1) {
		showCommonError("Error", fromDtExceed);
		getFieldByID("txtToDate").focus();
		return false;	
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();
		return false;
	} 
	return true;
}

function compareDate(date1,date2){
	var tempDay;
	var tempMonth;
	var tempYear;

	tempDay=date1.substring(0,2);
	tempMonth=date1.substring(3,5);
	tempYear=date1.substring(6,10); 	
	var tmpDate1=(tempYear+tempMonth+tempDay);
	
	
	tempDay=date2.substring(0,2);
	tempMonth=date2.substring(3,5);
	tempYear=date2.substring(6,10); 	
	var tmpDate2=(tempYear+tempMonth+tempDay);
	
	if (tmpDate1 > tmpDate2){
		return 1;
	} else {
		return -1;	
	}

}

function closeClick() {
	top[1].objTMenu.tabRemove(screenId);
}
