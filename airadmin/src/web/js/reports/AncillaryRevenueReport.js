var objWindow;
var value;
var screenId="UC_REPM_070";
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent){
	objCal1.ID = strID;
	objCal1.top = 150 ;
	objCal1.left = 0 ;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function winOnLoad() {
	getFieldByID("txtFromDate").focus();
	document.querySelector('[id^="spn"]').addEventListener("click", function(){
		if (this.val() === ">>") {
			$('#hdnAllAgentsSelected').val(true);
		} else {
			$('#hdnAllAgentsSelected').val(false);
		}
	});
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function viewClick() {
	top[2].HidePageMessage(); 
	setField("hdnAgents",getAgents());
	if(validateFields()){
	checkAllAgentsSelection();
	setField("hdnMode", "VIEW");
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
	top[0].objWindow = window.open("about:blank", "CWindow", strProp);
	var objForm = document.getElementById("frmAncillaryRevenueReport");
	objForm.target = "CWindow";
	objForm.action = "showAncillaryRevenueReport.action?hdnLive=OFFLINE";
	objForm.submit();
	}
}

function checkAllAgentsSelection(){
	if(document.getElementById("lstRoles").length > 0){
		setField("hdnAllAgentsSelected",false);
	} else {
		setField("hdnAllAgentsSelected",true);
	}
}

function  validateFields(){
	var currDt = new Date();
	var dtfromDate = null;
	var dttoDate = null;
	var strFrmDate = getText("txtFromDate");
	var strToDate = getText("txtToDate");
	if(dtfromDate != "") {
		dtfromDate = new Date(strFrmDate.substr(6,4), (parseFloat(strFrmDate.substr(3,2)) - 1), strFrmDate.substr(0,2));
	}
	if(strToDate != "") {
		dttoDate = new Date(strToDate.substr(6,4), (parseFloat(strToDate.substr(3,2)) - 1), strToDate.substr(0,2));
	}
	if(getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
		return false;
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
		return false;
	} else if(currDt < dtfromDate){	
		showCommonError("Error", fromDtExceedsToday);
		getFieldByID("txtFromDate").focus();
		return false;
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
		return false;
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();
		return false;
	} else if(currDt < dttoDate){	
		showCommonError("Error", toDtExceedsToday);
		getFieldByID("txtToDate").focus();
		return false;
	} else if(dtfromDate > dttoDate){
		showCommonError("Error", fromDtExceed);
		getFieldByID("txtToDate").focus();
		return false;
	} else if (getText("hdnAgents") == "") {
		showCommonError("Error", agentsRqrd);
		return false;
	} else if (validateAgentCount(getText("hdnAgents"))) {
		showCommonError("Error", agentExceed + repAgentLimit);
		return false;
	}
	return true;
}

function validateAgentCount(agentString) {
	if(agentString.split(',').length > repAgentLimit){
		return true;
	}
	return false;
}

function closeClick() {
	top[1].objTMenu.tabRemove(screenId);
}

function getAgents(){
		var strAgents=ls.getSelectedDataWithGroup();
		var newAgents;
		if(strAgents.indexOf(":")!=-1){
			strAgents=replaceall(strAgents,":" , ",");
		}
		if(strAgents.indexOf("|")!=-1){
			newAgents=replaceall(strAgents,"|" , ",");
		}else{
			newAgents=strAgents;
		}
		return newAgents;
	}