var objWindow;
var screenID = "UC_REPM_056";

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();
setField("hdnLive", repLive);

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {

	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function winOnLoad() {

	if (arrData.length == 1) {
		lscc.moveOptions(">>");
	}

	if (top[1].objTMenu.tabGetValue(screenID) != ""
			&& top[1].objTMenu.tabGetValue(screenID) != null) {
		var strSearch = top[1].objTMenu.tabGetValue(screenID).split("#");
		setField("txtFromDate", strSearch[0]);
		setField("txtToDate", strSearch[1]);
		setField("selAgencies", strSearch[2]);
		if (strSearch[3] == 'true') {
			getFieldByID("chkTAs").checked = true;
		} else {
			getFieldByID("chkTAs").checked = false;
			Disable('chkTAs', true);
		}
		if (strSearch[4] == 'true') {
			getFieldByID("chkCOs").checked = true;
		} else {
			getFieldByID("chkCOs").checked = false;
			Disable('chkCOs', true);
		}
		lscc.selectedData(strSearch[5]);

	} else {
		Disable('chkTAs', true);
		Disable('chkCOs', true);
	}
}

function clickAgencies() {

	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
		Disable('chkCOs', false);
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
		Disable('chkCOs', true);
		getFieldByID("chkCOs").checked = false;
	}
}

function changeAgencies() {
	ls.clear();
}

function setGSA() {

	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
	}
}

function viewClick() {

	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();

	} else if (dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true && checkGreat()
			&& validateAgents() && validateCarrierCode()) {

		setField("hdnMode", "VIEW");
		setField("hdnAgents", getAgents());
		setField("hdnCarrierCode", lscc.getselectedData());

		var strProp = 'toolbar=no,location=no,status=no,menubar=Yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmInterlineSales");
		objForm.target = "CWindow";

		objForm.action = "showInterlineSalesReport.action";
		objForm.submit();
		top[2].HidePageMessage();
	}

}

function validateAgents() {
	var validate = false;
	if (trim(ls.getselectedData()).length > 0) {
		validate = true;
	} else {
		showCommonError("Error", agentsRqrd);
	}
	return validate;
}

function validateCarrierCode() {
	var validate = false;
	if (trim(lscc.getselectedData()).length > 0) {
		validate = true;
	} else {
		showCommonError("Error", carrierCodeRqrd);
	}
	return validate;
}

function getAgentClick() {

	if (trim(getValue("selAgencies")) == '') {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
		return;
	}
	setField("hdnAgents", "");
	var strSearchCriteria = getValue("txtFromDate") + "#"
			+ getValue("txtToDate") + "#" + getValue("selAgencies") + "#"
			+ getFieldByID("chkTAs").checked + "#"
			+ getFieldByID("chkCOs").checked + "#" + lscc.getselectedData();
	top[1].objTMenu.tabSetValue(screenID, strSearchCriteria);
	setField("hdnMode", "SEARCH");
	document.getElementById("frmInterlineSales").target = "_self";
	document.forms[0].submit();
	top[2].ShowProgress();
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function dataChanged() {
	objOnFocus();
	setPageEdited(true);
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenID, isEdited);

}

function checkGreat() {

	var validate = false;
	if (!CheckDates(getText("txtFromDate"), getText("txtToDate"))) {
		validate = false;
		showCommonError("Error", fromDtExceed);
		getFieldByID("txtToDate").focus();
	} else if (!CheckDates(repStartDate, getText("txtFromDate"))) {
		showCommonError("Error", rptReriodExceeds + " " + repStartDate);
		getFieldByID("txtFromDate").focus();
	} else {
		validate = true;
	}
	return validate;
}

function settingValidation(cntfield) {

	if (!dateChk(cntfield)) {
		showERRMessage(invaliddate);
		getFieldByID(cntfield).focus();
		return;
	}
}

function getAgents() {

	var strAgents = ls.getSelectedDataWithGroup();
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents;
	}

	return newAgents;
}

function chkClick() {
	dataChanged();
}

function closeClick() {

	setPageEdited(false);
	objOnFocus();
	top[1].objTMenu.tabRemove(screenID)
}

function ckheckPageEdited() {

	if (top[1].objTMenu.tabGetPageEidted(screenID))
		return confirm("Changes will be lost! Do you wish to Continue?");
	else
		return true;
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}