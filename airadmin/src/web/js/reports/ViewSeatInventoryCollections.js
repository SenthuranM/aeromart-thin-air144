var objWindow;

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

setField("hdnLive", repLive);

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	case "2":
		setField("txtBookedFromDate",strDate);
		break;
	case "3":
		setField("txtBookedToDate",strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

var screenId = "UC_REPM_015";
function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}
function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}
function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function reportTypeOnChage(){
	var currval = getValue("selReportType");
	setField("txtBookedFromDate", "");
	setField("txtBookedToDate", "");
	if(currval == "BY_AGENT"){
		$("#tblBookedDateOpt").show();
		document.getElementById("departureMandatory").style.display = "block";
		document.getElementById("arrivalMandatory").style.display = "block";
	} else {
		$("#tblBookedDateOpt").hide();
		document.getElementById("departureMandatory").style.display = "none";
		document.getElementById("arrivalMandatory").style.display = "none";
	}
}

function viewClick(isSchedule) {	
	var rptVal = getValue("selReportType");
	var isValidateSuccess = false;
	if(rptVal == ""){
		showCommonError("Error", reportOptionNotSelected);
		getFieldByID("selReportType").focus();
	} else if(rptVal == "BY_AGENT"){
		if (getText("txtFromDate") == "") {
			showCommonError("Error", fromDtEmpty);
			getFieldByID("txtFromDate").focus();
		} else if (dateValidDate(getText("txtFromDate")) == false) {
			showCommonError("Error", fromDtInvalid);
			getFieldByID("txtFromDate").focus();
		} else if (getText("txtToDate") == "") {
			showCommonError("Error", toDtEmpty);
			getFieldByID("txtToDate").focus();
		} else if (dateValidDate(getText("txtToDate")) == false) {
			showCommonError("Error", toDtinvalid);
			getFieldByID("txtToDate").focus();
		} else if (validateReportDate("txtFromDate", "txtToDate", repStartDate)) {
			if (getText("txtBookedFromDate") != "" && dateValidDate(getText("txtBookedFromDate")) == false) {
				showCommonError("Error", bookedFromDtInvalid);
			    getFieldByID("txtBookedFromDate").focus();
			} else if (getText("txtBookedToDate") != "" && dateValidDate(getText("txtBookedToDate")) == false) {
				showCommonError("Error", bookedToDtInvalid);
				getFieldByID("txtBookedToDate").focus();
			} else if (getText("txtBookedToDate") != "" && getText("txtBookedFromDate") == "") {
				showCommonError("Error", fromDtEmpty);
				getFieldByID("txtBookedFromDate").focus();
			} else if (getText("txtBookedFromDate") != "" && getText("txtBookedToDate") == "") {
				showCommonError("Error", toDtEmpty);
				getFieldByID("txtBookedToDate").focus();
			} else if (getText("txtBookedFromDate") != "" && getText("txtBookedToDate") != "" 
				&& !validateReportDate("txtBookedFromDate", "txtBookedToDate", repStartDate)) {
				showCommonError("Error", bookedFromDtInvalid);
			    getFieldByID("txtBookedFromDate").focus();
			} else if(getText("selDept")== ""){
				showCommonError("Error",depatureRqrd);
			    getFieldByID("selDept").focus();
			} else if (getText("selArival")== ""){
				showCommonError("Error",arrivalRqrd);
			    getFieldByID("selArival").focus();
			} else {
				isValidateSuccess = true;
			}
		} else {
			showCommonError("Error", fromDtInvalid);
			getFieldByID("txtFromDate").focus();
		}
	} else if(rptVal == "BY_FLIGHT" || rptVal == "BY_STATION"){		
		if (getText("txtFromDate") == "") {
			showCommonError("Error", fromDtEmpty);
			getFieldByID("txtFromDate").focus();
		} else if (dateValidDate(getText("txtFromDate")) == false) {
			showCommonError("Error", fromDtInvalid);
			getFieldByID("txtFromDate").focus();
		} else if (getText("txtToDate") == "") {
			showCommonError("Error", toDtEmpty);
			getFieldByID("txtToDate").focus();
		} else if (dateValidDate(getText("txtToDate")) == false) {
			showCommonError("Error", toDtinvalid);
			getFieldByID("txtToDate").focus();
		} else if(validateReportDate("txtFromDate", "txtToDate", repStartDate)){
			isValidateSuccess = true;
		}
	}
		
	if(isValidateSuccess){
		if (getText("selDept") == getText("selArival")
				&& getText("selDept") != "" && getText("selArival") != "") {
			showCommonError("Error", sameSegment);
			getFieldByID("selArival").focus();
		} else if (getText("selDept") == getText("selVia1")
				&& getText("selDept") != "" && getText("selVia1") != ""
				|| getText("selDept") == getText("selVia2")
				&& getText("selDept") != "" && getText("selVia2") != ""
				|| getText("selDept") == getText("selVia3")
				&& getText("selDept") != "" && getText("selVia3") != ""
				|| getText("selDept") == getText("selVia4")
				&& getText("selDept") != "" && getText("selVia4") != "") {
			showCommonError("Error", sameViaDept);
			getFieldByID("selDept").focus();
		} else if (getText("selArival") == getText("selVia1")
				&& getText("selArival") != "" && getText("selVia1") != ""
				|| getText("selArival") == getText("selVia2")
				&& getText("selArival") != "" && getText("selVia2") != ""
				|| getText("selArival") == getText("selVia3")
				&& getText("selArival") != "" && getText("selVia3") != ""
				|| getText("selArival") == getText("selVia4")
				&& getText("selArival") != "" && getText("selVia4") != "") {
			showCommonError("Error", sameViaArival);
			getFieldByID("selVia2").focus();
		} else if (getText("selVia1") != ""
				&& (getText("selVia1") == getText("selVia2")
						|| getText("selVia1") == getText("selVia3") || getText("selVia1") == getText("selVia4"))) {
			showCommonError("Error", sameVia);
			getFieldByID("selVia1").focus();
		} else if (getText("selVia2") != ""
				&& (getText("selVia2") == getText("selVia3") || getText("selVia2") == getText("selVia4"))) {
			showCommonError("Error", sameVia);
			getFieldByID("selVia2").focus();
		} else if (getText("selVia3") != ""
				&& (getText("selVia3") == getText("selVia4"))) {
			showCommonError("Error", sameVia);
			getFieldByID("selVia3").focus();
		} else if (validateCarrierCode()) {

			setField("hdnMode", "VIEW");

			if (showCarrierCombo) {
				setField("hdnCarrierCode", lscc2.getselectedData());
			}
			if(isSchedule){
				scheduleReport();
			}else {
				var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
					top[0].objWindow = window.open("about:blank", "CWindow", strProp);
					var objForm = document
							.getElementById("frmViewSeatInventoryCollections");
					objForm.target = "CWindow";

					objForm.action = "viewSeatInventoryCollections.action?uniqueID=" + generateRandomID();
					objForm.submit();
					top[2].HidePageMessage();
			}
		}
	}
}

/**
 * Generate random ID
 * 
 */
function generateRandomID() {
	return (new Date()).getTime();
}

function scheduleReport() {

	UI_SchedRept.displayForm({divName:'divSchedFrom', formName:'frmViewSeatInventoryCollections', 
		composerName: 'seatInventoryCollections',
		errorCallback:function(message){
			showERRMessage(message);
		},
		successCallback:function(message){
			showCommonError("Confirmation", message);
		}
	});
}

function validateCarrierCode() {
	var validate = false;
	if (!showCarrierCombo || trim(lscc2.getselectedData()).length > 0) {
		validate = true;
	} else {
		showCommonError("Error", carrierCodeRqrd);
	}
	return validate;
}

function pageLoad() {	
	$("#tblBookedDateOpt").hide();
	getFieldByID("selReportType").focus();
	if (showCarrierCombo) {

		// setVisible("carrierCode" , true);
		document.getElementById("carrierCode").style.display = "block";
		var listBox = document.getElementById("lstCarrierCodes1");

		var listBox2 = document.getElementById("lstAssignedCarrierCodes1");
		listBox2 = lscc2;

		if (methods.length > 0) {
			for ( var i = 0; i < listBox.options.length; i++) {
				if (listBox.options[i].value == defaultCarrier) {
					listBox2.selectedData(defaultCarrier);

				}
			}
		}
	}
}
