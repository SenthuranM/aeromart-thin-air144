function UI_AgentUserRolesReport() {
}

$(document).ready(function() {

	$("#divSearch").decoratePanel("Agent User Roles Report");
	$("#divResultsPanel").decoratePanel("Roles");

	UI_AgentUserRolesReport.buildGrid();
	top[2].HideProgress();

	$('#btnSearch').click(function() {
		UI_AgentUserRolesReport.searchClicked()
	});

});

UI_AgentUserRolesReport.buildGrid = function() {

	$("#tblAgentUserRolesReport").jqGrid({
		url : 'showAgentUserRolesReport.action',
		datatype : "json",
		jsonReader : {
			root : "colAgentUserRoles",
			page : "page",
			total : "total",
			records : "records",
			repeatitems : false,
			id : "0"
		},
		colNames : [ '&nbsp;', 'Assigned Roles' ],
		colModel : [ {
			name : 'id',
			width : 10,
			jsonmap : 'id'
		}, {
			name : "agentUserRolesResultDTO.role",
			index : 'role',
			width : 90,
			align : "left"
		},

		],
		imgpath : '../../themes/default/images/jquery',
		multiselect : false,
		pager : $('#divAirportMsgPager'),
		rowNum : 20,
		viewrecords : true,
		height : 360,
		width : 920,

		loadComplete : function(e) {
		},
		beforeRequest : function() {
		}
	}).navGrid("#divAirportMsgPager", {
		refresh : false,
		edit : false,
		add : false,
		del : false,
		search : false
	});
}

UI_AgentUserRolesReport.searchClicked = function() {
	var postData = {};
	if ($('#searchPrivilege').val() < 0) {
		showCommonError("Error", "Select a Privilege");
		getFieldByID("searchPrivilege").focus();
	} else {
		postData['agentUserRolesSearchDTO.privilege'] = $('#searchPrivilege')
				.val();
		$("#tblAgentUserRolesReport").setGridParam({
			page : 1,
			postData : postData
		});
		$("#tblAgentUserRolesReport").trigger("reloadGrid");
	}
}
