var objWindow;
var value;
var screenID = "UC_REPM_022";

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtDepTimeLocal", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenID, isEdited);
}

function viewClick() {
	if (getText("txtDepTimeLocal") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtDepTimeLocal").focus();
	} else if (dateValidDate(getText("txtDepTimeLocal")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtDepTimeLocal").focus();
	} else if (dateValidDate(getText("txtDepTimeLocal")) == true) {
		// setField("hdnOpType",getText("selGDSCode"));
		setField("hdnMode", "VIEW");
		setField("hdnLive", repLive);
		var strProp = 'toolbar=no,location=no,status=no,menubar=Yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmAVSSeat");
		objForm.target = "CWindow";
		objForm.action = "showAVSSeatReport.action";
		objForm.submit();
		top[2].HidePageMessage();
	}
}

function settingValidation(cntfield) {
	if (!dateChk(cntfield)) {
		showERRMessage(invaliddate);
		getFieldByID(cntfield).focus();
		return;
	}
}

function dataChanged() {
	objOnFocus();
	setPageEdited(true);
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function chkClick() {
	dataChanged();
}

function closeClick() {
	setPageEdited(false);
	objOnFocus();
	top[1].objTMenu.tabRemove(screenID)

}
function ckheckPageEdited() {
	if (top[1].objTMenu.tabGetPageEidted(screenID))
		return confirm("Changes will be lost! Do you wish to Continue?");
	else
		return true;
}

function checkGreat() {
	var validate = false;
	if (!CheckDates(getText("txtDepTimeLocal"))) {
		validate = false;
		showCommonError("Error", fromDtExceed);

	} else if (!CheckDates(repStartDate, getText("txtDepTimeLocal"))) {
		showCommonError("Error", rptReriodExceeds + " " + repStartDate);
		getFieldByID("txtDepTimeLocal").focus();
	} else {
		validate = true;
	}
	return validate;
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}