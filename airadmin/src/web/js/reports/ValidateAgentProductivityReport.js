var objWindow;
var screenID = "UC_REPM_011";
setField("hdnLive", repLive);

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function winOnLoad() {
	setField("hdnLive", repLive);
	if (top[1].objTMenu.tabGetValue(screenID) != ""
			&& top[1].objTMenu.tabGetValue(screenID) != null) {
		var strSearch = top[1].objTMenu.tabGetValue(screenID).split("#");
		setField("txtFromDate", strSearch[0]);
		setField("txtToDate", strSearch[1]);
		setField("selAgencies", strSearch[2]);

	}
}

function viewClick() {

	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true && checkGreat()
			&& validateAgents()) {

		setField("hdnMode", "VIEW");
		setField("hdnAgents", trim(ls.getselectedData()));
		var strProp = 'toolbar=no,location=no,status=no,menubar=Yes,scrollbars=Yes,width=900,height=600,resizable=Yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmAgentProductivity");
		objForm.target = "CWindow";
		objForm.action = "showAgentProductivityReport.action";
		objForm.submit();
		top[2].HidePageMessage();
	}

}

function getAgentClick() {
	if (trim(getValue("selAgencies")) == '') {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
		return;
	}
	var strSearchCriteria = getValue("txtFromDate") + "#"
			+ getValue("txtToDate") + "#" + getValue("selAgencies");
	top[1].objTMenu.tabSetValue(screenID, strSearchCriteria);
	setField("hdnMode", "SEARCH");
	document.getElementById("frmAgentProductivity").target = "_self";
	document.forms[0].submit();
	top[2].ShowProgress();
}

function exportClick() {
	alert("Export Click");
}

function objOnFocus() {
	top[2].HidePageMessage();
}
function settingValidation(cntfield) {
	if (!dateChk(cntfield)) {
		showERRMessage(invaliddate);
		getFieldByID(cntfield).focus();
		return;
	}
}

function dataChanged() {
	objOnFocus();
	setPageEdited(true);
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenID, isEdited);

}

function validateAgents() {
	var validate = false;
	if (trim(ls.getselectedData()).length > 0) {
		validate = true;
	} else {
		showCommonError("Error", agentsRqrd);
	}
	return validate;
}

function chkClick() {
	dataChanged();
}

function closeClick() {
	setPageEdited(false);
	objOnFocus();
	top[1].objTMenu.tabRemove(screenID)

}
function ckheckPageEdited() {
	if (top[1].objTMenu.tabGetPageEidted(screenID))
		return confirm("Changes will be lost! Do you wish to Continue?");
	else
		return true;
}

function changeAgencies() {
	ls.clear();
}

function checkGreat() {
	var validate = false;
	if (!CheckDates(getText("txtFromDate"), getText("txtToDate"))) {
		validate = false;
		showCommonError("Error", fromDtExceed);
		getFieldByID("txtToDate").focus();
	} else {
		validate = true;
	}
	return validate;
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}