
var arrParamData = {};

function UI_ShowYiledReports(){}

UI_ShowYiledReports.salesFactorMap = null;
UI_ShowYiledReports.yeildFactorMap = null;
UI_ShowYiledReports.isGraph = null;
UI_ShowYiledReports.isSaleFactor = null;

/*
 * Onload Function
 */
$(document).ready(function(){	
  ShowProgressOnPopup();
  setVisible("btnClose", false);
  UI_ShowYiledReports.retriveAndDisplayDistributionData();
});

function ShowProgressOnPopup(){
	setVisible("divLoadMsg", true);
}
	
function HideProgressOnPopup(){
	setVisible("divLoadMsg", false);
}

UI_ShowYiledReports.retriveAndDisplayDistributionData = function(){

       arrParamData['reportType'] = window.opener.UI_YiledReports.reportType;
       arrParamData['viewType']  = window.opener.UI_YiledReports.viewType ;
       arrParamData['selectedYear']  = window.opener.UI_YiledReports.selectedYear ;
       arrParamData['radReportOption']  = window.opener.UI_YiledReports.radReportOption ;

	$.ajax( {
		url : 'showYeildTrendAnalysisReport!viewReport.action',
		type : 'POST',
		data : arrParamData,
		dataType : 'json',
		success : function(response) {
			UI_ShowYiledReports.submitDistributionSuccess(response);
		},
		error : function(requestObj, textStatus, errorThrown) {
			handleCommonAjaxInvocationErrors(requestObj);
		}
	});	
}

UI_ShowYiledReports.submitDistributionSuccess= function(response){
	     HideProgressOnPopup();             
	     document.getElementById('main_tbl').style.display = 'block';
	     setVisible("btnClose", true);
		//UI_ShowYiledReports.create
	     UI_ShowYiledReports.salesFactorMap = response.salesFactorMap;	
	     UI_ShowYiledReports.yeildFactorMap = response.yeildFactorMap;	
	     UI_ShowYiledReports.isGraph = response.graph;
	     UI_ShowYiledReports.isSaleFactor = response.saleFactor;
      
    $("#rpt_name").empty();
    if (UI_ShowYiledReports.isSaleFactor) 
	   $("#rpt_name").append("Trend Analysis Chart SF%");
    else
	   $("#rpt_name").append("Trend Analysis Chart by Yield");

	$("#rpt_name").addClass("large-font");
    var strYears=new Array();
    var x = 0;
     for(strChannel in UI_ShowYiledReports.salesFactorMap) {
         strYears[x] = strChannel;
         x++;
     }

      	var dataGrg = new google.visualization.DataTable();
      	dataGrg.addColumn('string', 'Year');
      	dataGrg.addColumn('number', strYears[0]);
      	dataGrg.addColumn('number', strYears[1]);

        var cnt1 = 0;
        var index = 0;
        var inner = null;
      
        dataGrg.addRows(12);

        var innerPrevYear = null;
        var innerCurrYear = null;

            if (UI_ShowYiledReports.isSaleFactor) {
        	innerPrevYear =  UI_ShowYiledReports.salesFactorMap[strYears[0]];
        	innerCurrYear =  UI_ShowYiledReports.salesFactorMap[strYears[1]];
	    } else  {
    	innerPrevYear = UI_ShowYiledReports.yeildFactorMap[strYears[0]];
	   	innerCurrYear = UI_ShowYiledReports.yeildFactorMap[strYears[1]];
	    }
	
	    dataGrg.setValue(0, 0, 'Jan');
	    dataGrg.setValue(0, 1, innerPrevYear['Jan']);
	    dataGrg.setValue(0, 2, innerCurrYear['Jan']);
	    dataGrg.setValue(1, 0, 'Feb');
	    dataGrg.setValue(1, 1, innerPrevYear['Feb']);
	    dataGrg.setValue(1, 2, innerCurrYear['Feb']);
	    dataGrg.setValue(2, 0, 'Mar');
	    dataGrg.setValue(2, 1, innerPrevYear['Mar']);
	    dataGrg.setValue(2, 2, innerCurrYear['Mar']);
	    dataGrg.setValue(3, 0, 'Apr');
	    dataGrg.setValue(3, 1, innerPrevYear['Apr']);
	    dataGrg.setValue(3, 2, innerCurrYear['Apr']);
	    dataGrg.setValue(4, 0, 'May');
	    dataGrg.setValue(4, 1, innerPrevYear['May']);
	    dataGrg.setValue(4, 2, innerCurrYear['May']);
	    dataGrg.setValue(5, 0, 'Jun');
	    dataGrg.setValue(5, 1, innerPrevYear['Jun']);
	    dataGrg.setValue(5, 2, innerCurrYear['Jun']);

	    dataGrg.setValue(6, 0, 'Jul');
	    dataGrg.setValue(6, 1, innerPrevYear['Jul']);
	    dataGrg.setValue(6, 2, innerCurrYear['Jul']);
	    dataGrg.setValue(7, 0, 'Aug');
	    dataGrg.setValue(7, 1, innerPrevYear['Aug']);
	    dataGrg.setValue(7, 2, innerCurrYear['Aug']);
	    dataGrg.setValue(8, 0, 'Sep');
	    dataGrg.setValue(8, 1, innerPrevYear['Sep']);
	    dataGrg.setValue(8, 2, innerCurrYear['Sep']);
	    dataGrg.setValue(9, 0, 'Oct');
	    dataGrg.setValue(9, 1, innerPrevYear['Oct']);
	    dataGrg.setValue(9, 2, innerCurrYear['Oct']);
	    dataGrg.setValue(10, 0, 'Nov');
	    dataGrg.setValue(10, 1, innerPrevYear['Nov']);
	    dataGrg.setValue(10, 2, innerCurrYear['Nov']);
	    dataGrg.setValue(11, 0, 'Dec');
	    dataGrg.setValue(11, 1, innerPrevYear['Dec']);
	    dataGrg.setValue(11, 2, innerCurrYear['Dec']);

        if (UI_ShowYiledReports.isGraph) {
	        UI_ShowYiledReports.columnChartSF = new google.visualization.LineChart(document.getElementById('chart_div'));
			UI_ShowYiledReports.columnChartSF.draw(dataGrg, {width: 850, height: 240, colors:['red','#3543AB'] , 
				  hAxis: {title: 'Month', titleTextStyle: {color: 'red'}} , backgroundColor:'#f6f6f6' ,
	                                                  tooltipFontSize:20
	                        });
        }
		
	    var dataSFRpt = new google.visualization.DataTable();
	 	dataSFRpt.addColumn('string', 'Month');
	 	dataSFRpt.addColumn('number', strYears[0]);
	 	dataSFRpt.addColumn('number', strYears[1]);
	 	dataSFRpt.addRows(12);
	 	
	 	dataSFRpt.setCell(0, 0, 'Jan');
	 	dataSFRpt.setCell(0, 1, innerPrevYear['Jan']);
	 	dataSFRpt.setCell(0, 2, innerCurrYear['Jan']);
	 	dataSFRpt.setCell(1, 0, 'Feb');
	 	dataSFRpt.setCell(1, 1, innerPrevYear['Feb']);
	 	dataSFRpt.setCell(1, 2, innerCurrYear['Feb']);
	 	dataSFRpt.setCell(2, 0, 'Mar');
	 	dataSFRpt.setCell(2, 1, innerPrevYear['Mar']);
	 	dataSFRpt.setCell(2, 2, innerCurrYear['Mar']);
	 	dataSFRpt.setCell(3, 0, 'Apr');
	 	dataSFRpt.setCell(3, 1, innerPrevYear['Apr']);
	 	dataSFRpt.setCell(3, 2, innerCurrYear['Apr']);

	 	dataSFRpt.setCell(4, 0, 'May');
	 	dataSFRpt.setCell(4, 1, innerPrevYear['May']);
	 	dataSFRpt.setCell(4, 2, innerCurrYear['May']);
	 	dataSFRpt.setCell(5, 0, 'Jun');
	 	dataSFRpt.setCell(5, 1, innerPrevYear['Jun']);
	 	dataSFRpt.setCell(5, 2, innerCurrYear['Jun']);
	 	dataSFRpt.setCell(6, 0, 'Jul');
	 	dataSFRpt.setCell(6, 1, innerPrevYear['Jul']);
	 	dataSFRpt.setCell(6, 2, innerCurrYear['Jul']);
	 	dataSFRpt.setCell(7, 0, 'Aug');
	 	dataSFRpt.setCell(7, 1, innerPrevYear['Aug']);
	 	dataSFRpt.setCell(7, 2, innerCurrYear['Aug']);

	 	dataSFRpt.setCell(8, 0, 'Sep');
	 	dataSFRpt.setCell(8, 1, innerPrevYear['Sep']);
	 	dataSFRpt.setCell(8, 2, innerCurrYear['Sep']);
	 	dataSFRpt.setCell(9, 0, 'Oct');
	 	dataSFRpt.setCell(9, 1, innerPrevYear['Oct']);
	 	dataSFRpt.setCell(9, 2, innerCurrYear['Oct']);
	 	dataSFRpt.setCell(10, 0, 'Nov');
	 	dataSFRpt.setCell(10, 1, innerPrevYear['Nov']);
	 	dataSFRpt.setCell(10, 2, innerCurrYear['Nov']);
	 	dataSFRpt.setCell(11, 0, 'Dec');
	 	dataSFRpt.setCell(11, 1, innerPrevYear['Dec']);
	 	dataSFRpt.setCell(11, 2, innerCurrYear['Dec']);

	 	if (!UI_ShowYiledReports.isGraph) {
		 var cssClassNames = {
		    'headerRow': 'headerRow-font large-font',
		    'tableRow': 'tableRow-background',
		    'oddTableRow': 'oddTableRow-background',
		    'selectedTableRow': 'selectedTableRow-background large-font',
		    'hoverTableRow': '',
		    'headerCell': 'headerCell-border',
		    'tableCell': '',
		    'rowNumberCell': ''};

		var options = {'showRowNumber': false, 'allowHtml': true, 'cssClassNames': cssClassNames , 'width': 450, 'height': 260};

		var strHTMLText = "<table id='tblBkgChnl' width='100%' border='0' cellpadding='0' cellspacing='0' style='border-bottom:1px solid grey'>";
		strHTMLText += "<tr>";
		strHTMLText += "<td class='thinRight thinTop' align='center' width='100px'><font><b>Month<\/b><\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop' align='right' width='30px'><font><b>&nbsp;"+strYears[0]+"&nbsp;<\/b><\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop' align='right' width='40px'><font><b>&nbsp;"+strYears[1]+"&nbsp;<\/b><\/font><\/td>";

		strHTMLText += "<\/tr>"; 

		strHTMLText += "<tr align='right'>";
		strHTMLText += "<td class='thinRight thinTop rowBGColor' align='left' width='100px'><font>Jan<\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop rowBGColor' width='30px'><font>&nbsp;"+innerPrevYear['Jan']+"&nbsp;<\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop rowBGColor' width='40px'><font>&nbsp;"+innerCurrYear['Jan']+"&nbsp;<\/font><\/td>";

		strHTMLText += "<\/tr>"; 

		strHTMLText += "<tr align='right'>";

		strHTMLText += "<td class='thinRight thinTop' align='left' width='100px'><font>Feb<\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop' width='30px'><font>&nbsp;"+innerPrevYear['Feb']+"&nbsp;<\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop' width='40px'><font>&nbsp;"+innerCurrYear['Feb']+"&nbsp;<\/font><\/td>";

		strHTMLText += "<\/tr>"; 
		strHTMLText += "<tr align='right'>";

		strHTMLText += "<td class='thinRight thinTop rowBGColor' align='left' width='100px'><font>Mar<\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop rowBGColor'' width='30px'><font>&nbsp;"+innerPrevYear['Mar']+"&nbsp;<\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop rowBGColor' width='40px'><font>&nbsp;"+innerCurrYear['Mar']+"&nbsp;<\/font><\/td>";

		strHTMLText += "<\/tr>"; 
		strHTMLText += "<tr align='right'>";

		strHTMLText += "<td class='thinRight' align='left' width='100px'><font>Apr<\/font><\/td>";
		strHTMLText += "<td class='thinRight' width='30px'><font>&nbsp;"+innerPrevYear['Apr']+"&nbsp;<\/font><\/td>";
		strHTMLText += "<td class='thinRight' width='40px'><font>&nbsp;"+innerCurrYear['Apr']+"&nbsp;<\/font><\/td>";

		strHTMLText += "<\/tr>"; 
		strHTMLText += "<tr align='right'>";

		strHTMLText += "<td class='thinRight thinTop rowBGColor' align='left' width='100px'><font>May<\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop rowBGColor' width='30px'><font>&nbsp;"+innerPrevYear['Apr']+"&nbsp;<\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop rowBGColor' width='40px'><font>&nbsp;"+innerCurrYear['Apr']+"&nbsp;<\/font><\/td>";

		strHTMLText += "<\/tr>"; 
		strHTMLText += "<tr align='right'>";

		strHTMLText += "<td class='thinRight' align='left' width='100px'><font>Jun<\/font><\/td>";
		strHTMLText += "<td class='thinRight' width='30px'><font>&nbsp;"+innerPrevYear['Jun']+"&nbsp;<\/font><\/td>";
		strHTMLText += "<td class='thinRight' width='40px'><font>&nbsp;"+innerCurrYear['Jun']+"&nbsp;<\/font><\/td>";

		strHTMLText += "<\/tr>"; 
		strHTMLText += "<tr align='right'>";

		strHTMLText += "<td class='thinRight thinTop rowBGColor' align='left' width='100px'><font>Jul<\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop rowBGColor' width='30px'><font>&nbsp;"+innerPrevYear['Jun']+"&nbsp;<\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop rowBGColor' width='40px'><font>&nbsp;"+innerCurrYear['Jun']+"&nbsp;<\/font><\/td>";

		strHTMLText += "<\/tr>"; 
		
		strHTMLText += "<tr align='right'>";

		strHTMLText += "<td class='thinRight thinTop' align='left' width='100px'><font>Aug<\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop' width='30px'><font>&nbsp;"+innerPrevYear['Aug']+"&nbsp;<\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop' width='40px'><font>&nbsp;"+innerCurrYear['Aug']+"&nbsp;<\/font><\/td>";

		strHTMLText += "<\/tr>"; 

		strHTMLText += "<tr align='right'>";

		strHTMLText += "<td class='thinRight thinTop rowBGColor' align='left' width='100px'><font>Sep<\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop rowBGColor' width='30px'><font>&nbsp;"+innerPrevYear['Sep']+"&nbsp;<\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop rowBGColor' width='40px'><font>&nbsp;"+innerCurrYear['Sep']+"&nbsp;<\/font><\/td>";

		strHTMLText += "<\/tr>"; 

		strHTMLText += "<tr align='right'>";

		strHTMLText += "<td class='thinRight thinTop' align='left' width='100px'><font>Oct<\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop' width='30px'><font>&nbsp;"+innerPrevYear['Oct']+"&nbsp;<\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop' width='40px'><font>&nbsp;"+innerCurrYear['Oct']+"&nbsp;<\/font><\/td>";

		strHTMLText += "<\/tr>"; 

		strHTMLText += "<tr align='right'>";

		strHTMLText += "<td class='thinRight thinTop rowBGColor' align='left' width='100px'><font>Nov<\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop rowBGColor' width='30px'><font>&nbsp;"+innerPrevYear['Nov']+"&nbsp;<\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop rowBGColor' width='40px'><font>&nbsp;"+innerCurrYear['Nov']+"&nbsp;<\/font><\/td>";

		strHTMLText += "<\/tr>"; 

		strHTMLText += "<tr align='right'>";

		strHTMLText += "<td class='thinRight thinTop' align='left' width='100px'><font>Dec<\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop' width='30px'><font>&nbsp;"+innerPrevYear['Dec']+"&nbsp;<\/font><\/td>";
		strHTMLText += "<td class='thinRight thinTop' width='40px'><font>&nbsp;"+innerCurrYear['Dec']+"&nbsp;<\/font><\/td>";

		strHTMLText += "<\/tr>"; 
		
		strHTMLText += "<\/table>";
		
		$("#chart_div").empty();
		$("#chart_div").append(strHTMLText);

	 		//var table = new google.visualization.Table(document.getElementById('chart_div'));
			//table.draw(dataSFRpt,options);
	 		//table.draw(dataSFRpt, {width: 850, height: 340, fontSize:'10px',showRowNumber: false, backgroundColor:'#f6f6f6'});
	 	}

}

function closeClick() {
        var objPopupWin = window.opener.top[2].objCWindow;
	objPopupWin.close();
}