var objWindow;
var value;
setField("hdnLive",repLive);
	
	var objCal1 = new Calendar("spnCalendarDG1");
	objCal1.onClick = "setDate";
	objCal1.blnDragCalendar = false;
	objCal1.buildCalendar();
	
function setDate(strDate, strID){
		switch (strID){
			case "0" : setField("txtFromDate",strDate);break ;
			case "1" : setField("txtToDate",strDate);break ;
		}
}

function LoadCalendar(strID, objEvent){
		objCal1.ID = strID;
		objCal1.top = 1 ;
		objCal1.left = 0 ;
		objCal1.onClick = "setDate";
		objCal1.showCalendar(objEvent);
}
	var screenId="UC_REPM_007";
function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))){
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function beforeUnload(){
		if ((top[0].objWindow) && (!top[0].objWindow.closed))	{
			top[0].objWindow.close();
		}
}

function setPageEdited(isEdited){
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}
function validateDate(){	
		var tempDay;
		var tempMonth;
		var tempYear;
		var validate = false;
		
		var dateFrom = getText("txtFromDate");
		var dateTo = getText("txtToDate");
	
		tempIStartDate = dateFrom;
		tempIEndDate = dateTo;
				
		tempDay=tempIStartDate.substring(0,2);
		tempMonth=tempIStartDate.substring(3,5);
		tempYear=tempIStartDate.substring(6,10); 	
		var tempOStartDate=(tempYear+tempMonth+tempDay);		
		
		tempDay=tempIEndDate.substring(0,2);
		tempMonth=tempIEndDate.substring(3,5);
		tempYear=tempIEndDate.substring(6,10); 	
		var tempOEndDate=(tempYear+tempMonth+tempDay);			
	
		var dtC = new Date();
		var dtCM = dtC.getMonth() + 1;
		var dtCD = dtC.getDate();
		if (dtCM < 10){dtCM = "0" + dtCM}
		if (dtCD < 10){dtCD = "0" + dtCD}
	 
		var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
		
		tempDay=strSysDate.substring(0,2);
		tempMonth=strSysDate.substring(3,5);
		tempYear=strSysDate.substring(6,10);
		
		strSysODate=(tempYear+tempMonth+tempDay); 
		
		/*if (tempOStartDate > strSysODate){
			showCommonError("Error",fromDtExceedsToday);
			getFieldByID("txtFromDate").focus();
		} else if (tempOEndDate > strSysODate){
			showCommonError("Error",toDtExceedsToday);
			getFieldByID("txtToDate").focus();
		} else */
		if(tempOStartDate > tempOEndDate){
			showCommonError("Error",fromDtExceed);
			getFieldByID("txtToDate").focus();
		} else if (!CheckDates(repStartDate,getText("txtFromDate"))) {
			showCommonError("Error",rptReriodExceeds +" "+ repStartDate);
			getFieldByID("txtFromDate").focus();			
		}  else {
			validate = true;				
		}
		return validate;	
}

function viewClick(){		
	if(getText("txtFromDate")==""){
		showCommonError("Error",fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if(dateValidDate(getText("txtFromDate"))==false){
		showCommonError("Error",fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	}  else if(getText("txtToDate")==""){		
		showCommonError("Error",toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if(dateValidDate(getText("txtToDate"))==false){		
		showCommonError("Error",toDtinvalid);
		getFieldByID("txtToDate").focus();
		
		
	}else if((getFieldByID("radAgentOptionAgent").checked) 
					&& trim(getFieldByID("selagent").value) == ""){
		
		showCommonError("Error", selectedAgency);
		getFieldByID("selagent").focus();
	} else if(dateValidDate(getText("txtFromDate"))==true 
			&& dateValidDate(getText("txtToDate"))==true && validateDate()) {
		

		if(getFieldByID("radAgentOptionAgent").checked)
		{
			setField("hdnMode","DETAIL");	
		}else {
			setField("hdnMode","VIEW");	
		}
					
		
		var agentTypeCode = trim(getText("selagent"));
		setField("hdnAgentTypeCode",agentTypeCode);
					
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank","CWindow",strProp);			
		var objForm  = document.getElementById("frmOutStandingReport");
			objForm.target = "CWindow";
			
		objForm.action = "showOutstandingBalanceReport.action";
		objForm.submit();
		top[2].HidePageMessage();		
	}
}

function pageLoadOB(){
		getFieldByID("txtFromDate").focus();
		Disable("selagent",true);
	
}

function chkClick(strSegmentFlight){
			
		/*if(getFieldByID("radSegment").checked == true){								
			getFieldByID("selSegFrom").focus();			
		} else if (getFieldByID("radFlight").checked == true){
			Disable("txtFlightNo","");
			getFieldByID("txtFlightNo").focus();
		}	*/
}

function agentsClick() {
	
	if(getFieldByID("radAgentOptionALL").checked) {
		setField("selagent","");
		Disable("selagent",true);
	}else if (getFieldByID("radAgentOptionAgent").checked)	{
		Disable("selagent",false);

	}
}