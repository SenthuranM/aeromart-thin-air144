var objWindow;
setField("hdnLive", repLive);

var screenId = "UC_REPM_088";
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
    switch (strID) {
        case "0":
            setField("txtFromDate", strDate);
            break;
        case "1":
            setField("txtToDate", strDate);
            break;
    }
}
function setPageEdited(isEdited) {
    top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function beforeUnload() {
    if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
        top[0].objWindow.close();
    }
}

function closeClick() {
    if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
        setPageEdited(false);
        top[1].objTMenu.tabRemove(screenId);
    }
}

function LoadCalendar(strID, objEvent) {
    objCal1.ID = strID;
    objCal1.top = 1;
    objCal1.left = 0;
    objCal1.onClick = "setDate";
    objCal1.showCalendar(objEvent);
}

function LoadCalendarTo(strID, objEvent) {
    objCal1.ID = strID;
    objCal1.top = 1;
    objCal1.left = 0;
    objCal1.onClick = "setDate";
    objCal1.showCalendar(objEvent);
}

function validateDate(txtFromDate, txtToDate) {
    var tempDay;
    var tempMonth;
    var tempYear;
    var validate = false;

    var dateFrom = getText(txtFromDate);
    var dateTo = getText(txtToDate);

    tempIStartDate = dateFrom;
    tempIEndDate = dateTo;

    tempDay = tempIStartDate.substring(0, 2);
    tempMonth = tempIStartDate.substring(3, 5);
    tempYear = tempIStartDate.substring(6, 10);
    var tempOStartDate = (tempYear + tempMonth + tempDay);

    tempDay = tempIEndDate.substring(0, 2);
    tempMonth = tempIEndDate.substring(3, 5);
    tempYear = tempIEndDate.substring(6, 10);
    var tempOEndDate = (tempYear + tempMonth + tempDay);

    var dtC = new Date();
    var dtCM = dtC.getMonth() + 1;
    var dtCD = dtC.getDate();
    if (dtCM < 10) {
        dtCM = "0" + dtCM;
    }
    if (dtCD < 10) {
        dtCD = "0" + dtCD;
    }

    var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear();

    tempDay = strSysDate.substring(0, 2);
    tempMonth = strSysDate.substring(3, 5);
    tempYear = strSysDate.substring(6, 10);

    strSysODate = (tempYear + tempMonth + tempDay);

    if (dateFrom == "" && dateTo != "") {
        showCommonError("Error", fromDtEmpty);
        getFieldByID(txtFromDate).focus();

    } else if (dateTo == "" && dateFrom != "") {
        showCommonError("Error", toDtEmpty);
        getFieldByID(txtToDate).focus();

    } else if (tempOStartDate > tempOEndDate) {
        showCommonError("Error", fromDtExceed);
        getFieldByID(txtToDate).focus();

    } else {
        validate = true;
    }
    return validate;
}

function viewClick(isSchedule) {

    var selectedCountries = convertToStrArray(document.getElementById("selectedCntries").options);

    if ((getText("txtFromDate") == "")) {
        showCommonError("Error", fromDtEmpty);
        getFieldByID("txtFromDate").focus();
        return;

    } else if ((getText("txtFromDate") != "") && dateValidDate(getText("txtFromDate")) == false) {
        showCommonError("Error", fromDtInvalid);
        getFieldByID("txtFromDate").focus();
        return;

    } else if ((getText("txtToDate") == "" )) {
        showCommonError("Error", toDtEmpty);
        getFieldByID("txtToDate").focus();
        return;

    } else if ((getText("txtToDate") != "") && dateValidDate(getText("txtToDate")) == false) {
        showCommonError("Error", toDtinvalid);
        getFieldByID("txtToDate").focus();
        return;

    } else if (selectedCountries.length == 0) {
        showCommonError("Error", countryEmpty);
        getFieldByID("btnCountries").focus();
    }

    else if (validateDate("txtFromDate", "txtToDate")) {

        setField("hdnMode", "VIEW");
        setField("hdnCountry", selectedCountries.toString());

        if(isSchedule){
            scheduleReport();
        }else {
            var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
            top[0].objWindow = window.open("about:blank", "CWindow", strProp);
            var objForm = document.getElementById("frmOriginCountry");
            objForm.target = "CWindow";

            objForm.action = "showOriginCountryOfCallReport.action";
            objForm.submit();
            top[2].HidePageMessage();
        }
    }
}

function scheduleReport() {

    UI_SchedRept.displayForm({divName:'divSchedFrom', formName:'frmOriginCountry',
        composerName: 'originCountryOfCallReport',
        errorCallback:function(message){
            showERRMessage(message);
        },
        successCallback:function(message){
            showCommonError("Confirmation", message);
        }
    });
}

function showPopUpData() {
    var htmlCountryList = '';
    htmlCountryList += '<table width="99%" align="center" border="0" cellpadding="1" cellspacing="0" style="background-color: #5A5C63;">';
    htmlCountryList += '<tr><td>';
    htmlCountryList += '<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4">';
    htmlCountryList += '<tr><td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td>';
    htmlCountryList += '<td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader">';
    htmlCountryList += 'Select Countries';
    htmlCountryList += '</font></td><td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td>';
    htmlCountryList += '</tr><tr><td class="FormBackGround"></td><td class="FormBackGround" valign="top">';
    htmlCountryList += '<table><tr><td align="left">';
    htmlCountryList += '</td></tr>';
    htmlCountryList += '<tr><td align="top"><span id="spnCountrList"></span></td></tr>';
    htmlCountryList += '<tr><td align="right"><input type="button" id="btnInsertCountry" value= "Insert" class="Button" onClick="return insertSelectedCountries();">&nbsp;<input type="button" id="btnPopupClose" value= "Close" class="Button" onClick="return showCountryMultiSelect(false);"></td></tr></table>';
    htmlCountryList += '</td><td class="FormBackGround"></td></tr><tr>';
    htmlCountryList += '<td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td>';
    htmlCountryList += '<td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>';
    htmlCountryList += '<td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td>';
    htmlCountryList += '</tr></table>';
    htmlCountryList += '</td></tr></table>';

    DivWrite("Country", htmlCountryList);
}

function clearListBox(listBox) {
    var control = document.getElementById(listBox);
    control.options.length = 0;
    for ( var t = 0; t <= (control.length); t++) {
        control.options[t] = null;
    }
}

function showCountryMultiSelect(show) {
    setVisible("Country", show);
}

function loadCountryList() {
    countryListBox.drawListBox();
}

function insertSelectedCountries() {
    clearListBox("selectedCntries");
    var control = document.getElementById("selectedCntries");
    if (countryListBox.getselectedData() != "" && countryListBox.getselectedData() != null) {
        var countries = (countryListBox.getselectedData()).split(",");
        if (countries != "" && countries.length > 0) {
            for (var i = 0; i < countries.length; i++) {
                control.options[i] = new Option(countries[i], countries[i]);
            }
        }
    }
    showCountryMultiSelect(false);
}

function pageLoadSC() {
    getFieldByID("txtFromDate").focus();
    showPopUpData();
    setTimeout('loadCountryList()', 2000);
}

function convertToStrArray(selectedItemsFromListBox) {
    var selectedItems = new Array();
    if (selectedItemsFromListBox != "" && selectedItemsFromListBox != null) {
        for (i = 0; i < selectedItemsFromListBox.length; i++) {
            selectedItems[i] = selectedItemsFromListBox[i].text;
        }
    }
    return selectedItems;
}




