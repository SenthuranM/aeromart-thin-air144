var objWindow;
var value;

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}
function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

var screenId = "SC_PROMO_07";
function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function viewClick() {
	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();
	} else if(!validateViaPoints()){
		
	} else if (dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true
			&& validateReportDate("txtFromDate", "txtToDate", repStartDate)) {

		setField("hdnMode", "VIEW");

		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmFlightLoyaltyMemberReport");
		objForm.target = "CWindow";
		objForm.action = "flightLoyaltyMemberReport.action";
		objForm.submit();
		top[2].HidePageMessage();
	}
}

function pageLoadRR() {
	$("#txtFromDate, #txtToDate").datepicker({
		showOn: "button",
		buttonImage: "../../images/calendar_no_cache.gif",
		buttonImageOnly: true,
		dateFormat: 'dd/mm/yy'
	});
	
	populateAllAirportsForSearch();
}

function validateViaPoints(){
	if (($("#selVia1").val() != "") ||  ($("#selVia2").val() != "")) {
		
		var source = $("#selFromStn").val();
		var via1 = $("#selVia1").val();
		var via2 = $("#selVia2").val();
		var dest = $("#selToStn").val();

		if(dest.length == 0){
			getFieldByID("selToStn").focus();
			showCommonError("Error", viaPointsWithoutSD);
			return false;
		}else if(source.length == 0){
			getFieldByID("selFromStn").focus();
			showCommonError("Error", viaPointsWithoutSD);
			return false;
		}
		
		var pointList = [];
		pointList.push(source);
		pointList.push(via1);
		pointList.push(via2);
		pointList.push(dest);
		 
		if(hasDuplicates(pointList)){
			getFieldByID("selVia1").focus();
			showCommonError("Error", viaPointConflict);
			return false;
		}
	}
	return true;
}

function hasDuplicates(array) {
    var valuesSoFar = [];
    for (var i = 0; i < array.length; ++i) {
        var value = array[i];
        if (valuesSoFar.indexOf(value) !== -1) {
            return true;
        }
        valuesSoFar.push(value);
    }
    return false;
}
//function to populate all online airports for search criteria
function populateAllAirportsForSearch() {
	var objLB = new listBox();
	objLB.dataArray = arrOnlineAirports;
	var strID = "selFromStn,selToStn";
	objLB.id = strID;
	objLB.blnFirstEmpty = true; // first value empty or not
	objLB.firstValue = ""; // first value
	objLB.firstTextValue = ""; // first Text
	objLB.fillListBox();
	
	
	objLB = new listBox();
	objLB.dataArray = arrOnlineAirports;
	strID = "selVia1";
	objLB.id = strID;
	objLB.blnFirstEmpty = true; // first value empty or not
	objLB.firstValue = ""; // first value
	objLB.firstTextValue = "Via 1"; // first Text
	objLB.fillListBox();
	
	objLB = new listBox();
	objLB.dataArray = arrOnlineAirports;
	strID = "selVia2";
	objLB.id = strID;
	objLB.blnFirstEmpty = true; // first value empty or not
	objLB.firstValue = ""; // first value
	objLB.firstTextValue = "Via 2"; // first Text
	objLB.fillListBox();
}

function settingValidation(cntfield) {
	if (!dateChk(cntfield)) {
		showERRMessage(invaliddate);
		getFieldByID(cntfield).focus();
		return;
	}
}