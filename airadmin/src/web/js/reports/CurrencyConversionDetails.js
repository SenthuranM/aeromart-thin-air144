var objWindow;
var screenId = "UC_REPM_030";
setField("hdnLive", repLive);
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function rptOnLoad() {
}
function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
} 

function winOnLoad() {
	
}

function getCurrencies() {
	var strCurrencies = lspm.getSelectedDataWithGroup();
	var newCurrencies;
	if (strCurrencies.indexOf(":") != -1) {
		strCurrencies = replaceall(strCurrencies, ":", ",");
	}
	if (strCurrencies.indexOf("|") != -1) {
		newCurrencies = replaceall(strCurrencies, "|", ",");
	} else {
		newCurrencies = strCurrencies
	}
	return newCurrencies;
}

function viewClick() {	
	var isValid = true;
	var ischkCurrency = getFieldByID('chkCurrency').checked;
	
	if (getCurrencies() == '') {
		showCommonError("Error", currencyCodeRqrd);				
	} else if (ischkCurrency && getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();		
	} else if (dateValidDateCurrency(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();		
	} else if (ischkCurrency && getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();		
	} else if (dateValidDateCurrency(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();		
	} else  {			
		    setField("hdnCurrencies", getCurrencies());	
			setField("hdnMode", "VIEW");
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10';		
			top[0].objWindow = window.open("about:blank", "CWindow", strProp);
			var objForm = document.forms[0];		
			objForm.target = "CWindow";
			objForm.action = "currencyConversionDetailsReport.action";
			objForm.submit();
			top[2].HidePageMessage();
		}
}

function objOnFocus() {
	top[2].HidePageMessage();
} 

function checkClick() {
	if (getFieldByID('chkCurrency').checked) {
		getFieldByID('spanFromMan').innerHTML = '*';
		getFieldByID('spanToMan').innerHTML = '*';
	} else {
		getFieldByID('spanFromMan').innerHTML = '';
		getFieldByID('spanToMan').innerHTML = '';
	}
}

function dateValidDateCurrency(strDate){ 
	if (strDate != '') {
		var dtD = strDate.substring(0,2) 
		var dtM = strDate.substring(3,5)
		var dtY = strDate.substring(6,10) 
		if ((isNaN(dtD)) || (isNaN(dtM)) || (isNaN(dtY))){
			return false;
		}
		
		if ((Number(dtD) == 0) || (Number(dtM) == 0) || (Number(dtY) == 0)){
			return false;
		}
		
		// is it Valid Date
		if (Number(dtM) > 12){	
			return false;
		}
		
		if (Number(dtD) > getDaysInMonth(dtM, dtY)){	
			return false;
		}
	}
	return true;
}



