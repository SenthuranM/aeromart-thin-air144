var objWindow;
var screenId = "UC_REPM_008";
setField("hdnLive", repLive);

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function rptOnLoad() {
}
function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtInvoiceFrom", strDate);
		break;
	case "1":
		setField("txtInvoiceTo", strDate);
		break;
	}
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function LoadCalendar(strID, objEvent){
	objCal1.ID = strID;
	objCal1.top = 25 ;
	objCal1.left = 0 ;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function getSystemDate() {
	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10) {
		dtCM = "0" + dtCM
	}
	if (dtCD < 10) {
		dtCD = "0" + dtCD
	}

	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear();

	return strSysDate;

}

function setMonth(strCode) {
	var control = document.getElementById("selMonth");
	for ( var t = 0; t < (control.length); t++) {
		if (control.options[t].text == strCode) {
			control.options[t].selected = true;
			break;
		}
	}
}

function winOnLoad() {
	getFieldByID("selYear").focus();
	if (repShowpay == 'true') {
		document.getElementById('divPay').style.display = 'block';
	} else {
		document.getElementById('divPay').style.display = 'none';

	}
	if (top[1].objTMenu.tabGetValue(screenId) != ""
			&& top[1].objTMenu.tabGetValue(screenId) != null) {

		var strSearch = top[1].objTMenu.tabGetValue(screenId).split("#");
		if(isJSTrnsWsInv == false) { 
			setField("selYear", strSearch[0]);
			setField("selMonth", strSearch[1]);
		}
		setField("selAgencies", strSearch[2]);
		setField("selBP", strSearch[5]);
		if (strSearch[3] == 'true') {
			getFieldByID("chkTAs").checked = true;
		} else {
			getFieldByID("chkTAs").checked = false;
			Disable('chkTAs', true);
		}
		if (strSearch[4] == 'true') {
			getFieldByID("chkCOs").checked = true;
		} else {
			getFieldByID("chkCOs").checked = false;
			Disable('chkCOs', true);
		}
		if (getFieldByID("chkBase")) {
			if (strSearch[6] == 'true') {
				getFieldByID("chkBase").checked = true;
			} else {
				getFieldByID("chkBase").checked = false;
			}
		}
	 
		if(isJSTrnsWsInv){
			try{
				setField("txtInvoiceFrom",strSearch[7]);
				setField("txtInvoiceTo",strSearch[8]);
			} catch(e) { }
		}
			
	} else {
		var strSysDateArr = getSystemDate().split("/");
		setMonth(strSysDateArr);
		setField("selMonth", strSysDateArr[1]);
		Disable('chkTAs', true);
		Disable('chkCOs', true);
	}
}

function clickAgencies() {
	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
		Disable('chkCOs', false);
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
		Disable('chkCOs', true);
		getFieldByID("chkCOs").checked = false;
	}
}

function changeAgencies() {
	ls.clear();
}

function getAgents() {
	var strAgents = ls.getSelectedDataWithGroup();
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents
	}
	return newAgents;
}

function viewClick(isSchedule){
	setField("hdnMode", "VIEW");
	generateReport(isSchedule)
}

function exportClick(){
	setField("hdnMode", "EXPORT");
	generateReport();
}

function generateReport(isSchedule) {	
	setField("hdnAgents", getAgents());
	setField("hdnEntityText",getFieldByID("selEntity").options[getFieldByID("selEntity").selectedIndex].innerText);
	var strSysDateArr = getSystemDate().split("/");
	var numField = Number(getFieldByID("selMonth").value);
	var thisMonth = Number(strSysDateArr[1]);
	var selecYear = Number(getFieldByID("selYear").value);
	var thisyear = Number(strSysDateArr[2]);
	var reportDate = "";
	if (Number(getFieldByID("selBP").value == 1)) {
		reportDate = "01";
	} else {
		reportDate = "16";
	}
	reportDate += "/" + getFieldByID("selMonth").value + "/"
			+ getFieldByID("selYear").value;

	if(isJSTrnsWsInv){
		 
		
		if (getFieldByID("txtInvoiceFrom").value == "") {
			showCommonError("Error", fromDtEmpty);
			getFieldByID("txtInvoiceFrom").focus();
		} else if(getFieldByID("txtInvoiceTo").value == "") {
			showCommonError("Error", toDtEmpty);
			getFieldByID("txtInvoiceTo").focus();
		} else if (dateValidDate(getText("txtInvoiceFrom")) == false) {
			showCommonError("Error", fromDtInvalid);
			getFieldByID("txtInvoiceFrom").focus();
		} else if (dateValidDate(getText("txtInvoiceTo")) == false) {
			showCommonError("Error", toDtInvalid);
			getFieldByID("txtInvoiceTo").focus();
		} else if (getText("selAgencies") == "") {
			showCommonError("Error", agentTypeRqrd);
			getFieldByID("selAgencies").focus();
		} else if (ls.getSelectedDataWithGroup() == "" && getText("selAgencies") != "All") {
			showCommonError("Error", agentsRqrd);
		}else {		
			if(isSchedule){
				scheduleReport();
			}else {
				var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
				top[0].objWindow = window.open("about:blank", "CWindow", strProp);
				var objForm = document.getElementById("frmInvoiceSummaryReport");
				objForm.target = "CWindow";
				objForm.action = "showInvoiceSummaryReport.action";
				objForm.submit();

				top[2].HidePageMessage();
			}
		}
	}else {
		if (getFieldByID("selYear").value == "") {
			showCommonError("Error", yearEmpty);
			getFieldByID("selYear").focus();
		} else if (getFieldByID("selMonth").value == "") {
			showCommonError("Error", monthEmpty);
			getFieldByID("selMonth").focus();
		} else if (getText("selAgencies") == "") {
			showCommonError("Error", agentTypeRqrd);
			getFieldByID("selAgencies").focus();
		}  else if (thisyear == selecYear && numField > thisMonth) {
			showCommonError("Error", monthGreater);
		} else if (ls.getSelectedDataWithGroup() == "" && getText("selAgencies") != "All") {
			showCommonError("Error", agentsRqrd);
		}else if ((isWeeklyInvoiceEnabled == false || isWeeklyInvoiceEnabled == "false") && !CheckDates(repStartDate, reportDate)) {
			showCommonError("Error", rptReriodExceeds + " " + repStartDate);
			getFieldByID("selMonth").focus();
		} else {	
			if(isSchedule){
				scheduleReport();
			}else {
				//setField("hdnMode", "VIEW");
				var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
				top[0].objWindow = window.open("about:blank", "CWindow", strProp);
				var objForm = document.getElementById("frmInvoiceSummaryReport");
				objForm.target = "CWindow";
				objForm.action = "showInvoiceSummaryReport.action";
				objForm.submit();

				top[2].HidePageMessage();
			}
		}
	}
}

function scheduleReport() {

	UI_SchedRept.displayForm({divName:'divSchedFrom', formName:'frmInvoiceSummaryReport', 
		composerName: 'invoiceSummary',
		errorCallback:function(message){
			showERRMessage(message);
		},
		successCallback:function(message){
			showCommonError("Confirmation", message);
		}
	});
}

function getAgentClick() {
	if (getValue("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
	} else {
		var strSearchCriteria = getValue("selYear") + "#"
				+ getValue("selMonth") + "#" + getValue("selAgencies") + "#"
				+ getFieldByID("chkTAs").checked + "#"
				+ getFieldByID("chkCOs").checked + "#" + getValue("selBP");
		if (getFieldByID("chkBase")) {
			strSearchCriteria += "#" + getFieldByID("chkBase").checked
		} else {
			strSearchCriteria += "#null";
		}
		if(isJSTrnsWsInv == true){
			strSearchCriteria += "#"+getValue("txtInvoiceFrom")+"#"+getValue("txtInvoiceTo");
		}
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		setField("hdnMode", "SEARCH");
		setField("hdnAgents", "");
		var objForm = document.getElementById("frmInvoiceSummaryReport");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}

function objOnFocus() {
	top[2].HidePageMessage();
}