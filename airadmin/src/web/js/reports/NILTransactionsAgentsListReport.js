var objWindow;
setField("hdnLive", repLive);

var screenId = "UC_REPM_095";
var objCal1 = new Calendar("spnCalendarDG1");
var selectedAllAgents = false;
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function radioOptionChanged() {
	if (getText("radAgencey") == "Agencey") {
		Disable("selAgencies", "true");
		Disable("btnGetAgent", "true");
	} else {
		Disable("selAgencies", "");
		Disable("btnGetAgent", "");
	}
}

function winOnLoad() {
	getFieldByID("txtFromDate").focus();
	
	if (modDetailsEnabled == 'false') {
		setVisible("chkModifications", false);
		document.getElementById('divModifications').style.display = 'none';
	}
	if (top[1].objTMenu.tabGetValue(screenId) != ""
			&& top[1].objTMenu.tabGetValue(screenId) != null) {
		var strSearch = top[1].objTMenu.tabGetValue(screenId).split("#");
		setField("txtFromDate", strSearch[0]);
		setField("txtToDate", strSearch[1]);
		setField("selAgencies", strSearch[2]);
		lspm.selectedData(strSearch[4]);
		if (strSearch[3] == 'true') {
			getFieldByID("chkTAs").checked = true;
		} else {
			getFieldByID("chkTAs").checked = false;
			Disable('chkTAs', true);
		}
		if (strSearch[5] == 'true') {
			getFieldByID("chkCOs").checked = true;
		} else {
			getFieldByID("chkCOs").checked = false;
			Disable('chkCOs', true);
		}
		if(getFieldByID("chkSales")){
			if (strSearch[7] == 'true') {
				getFieldByID("chkSales").checked = true;
			}else {
				getFieldByID("chkSales").checked = false;			
			}
		}
		if(getFieldByID("chkRefunds")){
			if (strSearch[8] == 'true') {
				getFieldByID("chkRefunds").checked = true;
			}else {
				getFieldByID("chkRefunds").checked = false;			
			}
		}
		setField("selFlightType",strSearch[9]);
		if(getFieldByID("chkModifications")){
			if (strSearch[10] == 'true') {
				getFieldByID("chkModifications").checked = true;
			}else {
				getFieldByID("chkModifications").checked = false;			
			}
		}
		setField("selFareDiscountCode", strSearch[11]);
		if(getFieldByID("chkLocalTime")){
			if (strSearch[12] == 'true') {
				getFieldByID("chkLocalTime").checked = true;
			}else {
				getFieldByID("chkLocalTime").checked = false;			
			}
		}
		
	} else {
		Disable('chkTAs', true);
		Disable('chkCOs', true);
	}
}

function changeAgencies() {
	ls.clear();
}

function clickAgencies() {
	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
		Disable('chkCOs', false);
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
		Disable('chkCOs', true);
		getFieldByID("chkCOs").checked = false;
	}
}

function getAgents() {
	var strAgents;
	if (getText("selAgencies") != "All") {
		strAgents = ls.getSelectedDataWithGroup();
	} else {
		strAgents = ls.getselectedData();
		if(ls.getNotSelectedDataWithGroup() == "" && strAgents != ""){
			selectedAllAgents = true;
			return "";
		}
	}
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents;
	}
	return newAgents;
}

function viewClick(isSchedule) {
	var currDt = new Date();
	var dtfromDate = null;
	var dttoDate = null;
	var strFrmDate = getText("txtFromDate");
	var strToDate = getText("txtToDate");
	if(dtfromDate != "") {
		dtfromDate = new Date(strFrmDate.substr(6,4), (parseFloat(strFrmDate.substr(3,2)) - 1), strFrmDate.substr(0,2));
	}
	if(strToDate != "") {
		dttoDate = new Date(strToDate.substr(6,4), (parseFloat(strToDate.substr(3,2)) - 1), strToDate.substr(0,2));
	}
	
	if (displayAgencyMode == 1 || displayAgencyMode == 2) {
		setField("hdnAgents", getAgents());
	} else {
		setField("hdnAgents", "");
	}
	setField("hdnPayments", lspm.getselectedData());
	setField("hdnReportView", "SUMMARY");
	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();

	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();

	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();

	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();
		
	} else if(currDt < dtfromDate){	
		showCommonError("Error", fromDtExceedsToday);
		getFieldByID("txtFromDate").focus();
		
	} else if(currDt < dttoDate){	
		showCommonError("Error", toDtExceedsToday);
		getFieldByID("txtToDate").focus();
		
	} else if (lspm.getselectedData() == "") {
		showCommonError("Error", paymentmodesRqrd);

	} else if (displayAgencyMode == 1 && getText("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();

	} else if ((displayAgencyMode == 1 || displayAgencyMode == 2)
			&& (getText("hdnAgents") == "" && !selectedAllAgents)) {
		showCommonError("Error", agentsRqrd);
	} else if(!getFieldByID("chkSales").checked && !getFieldByID("chkRefunds").checked && !getFieldByID("chkModifications").checked){
		showCommonError("Error", salesOrRefundsNotSelected);
		getFieldByID("chkSales").focus();
	} else if (dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true
			&& validateReportDate("txtFromDate", "txtToDate", repStartDate)) {

		if(isSchedule) {
			scheduleReport();
		} else {
			
			if(offlineReportParams != ""){
				var offlineReportParamArr = offlineReportParams.split(",");
				var isOfflineReportEnabled = (offlineReportParamArr[0] == "Y") ? true:false;
				if(repLive == "LIVE" && isOfflineReportEnabled){
					var noOfDaysToLiveReport = offlineReportParamArr[1];
					var currentSystemDate = stringToDate(systemDate);
					var validFromDate = DateToString(addDays(currentSystemDate, -1 * noOfDaysToLiveReport));
					var selectedToDate = getText("txtToDate");
					var selectedFromDate = getText("txtFromDate");	
					
					if(compareDate(selectedToDate, validFromDate) != -1 && compareDate(selectedFromDate, validFromDate) == -1){					
						showCommonError("Error", invalidLiveReport);
						getFieldByID("txtToDate").focus();
						return false;				
					}
				}
			}
			
			if(selectedAllAgents){
				setField("hdnSelectedAllAgents", "true");
				setField("hdnAgents","");
			} else {
				setField("hdnSelectedAllAgents", "false");
			}
			
			setField("hdnMode", "VIEW");
			// setField("hdnRptType","SUMMARY");
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
			top[0].objWindow = window.open("about:blank", "CWindow", strProp);
			var objForm = document.getElementById("frmPage");
			objForm.target = "CWindow";
			objForm.action = "showNILTransactionsAgentsListReport.action";
			objForm.submit();
			top[2].HidePageMessage();
		}
	}

}

function scheduleReport() {

	UI_SchedRept.displayForm({divName:'divSchedFrom', formName:'frmPage', 
		composerName: 'NILTransactionsAgents',
		errorCallback:function(message){
			showERRMessage(message);
		},
		successCallback:function(message){
			showCommonError("Confirmation", message);
		}
	});
}

function getAgentClick() {
	if (getValue("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
	} else {
		var strSearchCriteria = getValue("txtFromDate") + "#"
				+ getValue("txtToDate") + "#" + getValue("selAgencies") + "#"
				+ getFieldByID("chkTAs").checked + "#" + lspm.getselectedData()
				+ "#" + getFieldByID("chkCOs").checked;
		
		strSearchCriteria += "#"+false;
			
		strSearchCriteria += "#" + getFieldByID("chkSales").checked + "#" + getFieldByID("chkRefunds").checked 
				+"#"+getValue("selFlightType") + "#" + getFieldByID("chkModifications").checked;
		
		strSearchCriteria += getFieldByID("selFareDiscountCode")==null ? "#" : ("#" + getValue("selFareDiscountCode"));
		strSearchCriteria += "#" + getFieldByID("chkLocalTime").checked
		
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		setField("hdnAgents", "");
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmPage");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}

function objOnFocus() {
	top[2].HidePageMessage();
}
