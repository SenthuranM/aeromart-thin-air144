var objWindow;
var value;
setField("hdnLive", repLive);

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function LoadCalendarTo(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

var screenId = "UC_REPM_057";

function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function winOnLoad() {
	getFieldByID("txtFromDate").focus();
	if (arrData.length == 1) {
		lscc.moveOptions(">>");
	}
	if (top[1].objTMenu.tabGetValue(screenId) != ""
			&& top[1].objTMenu.tabGetValue(screenId) != null) {

		var strSearch = top[1].objTMenu.tabGetValue(screenId).split("#");
		setField("txtFromDate", strSearch[0]);
		setField("txtToDate", strSearch[1]);
		setField("selAgencies", strSearch[2]);
		bcls.selectedData(strSearch[4]);
		stls.selectedData(strSearch[6]);
		setAllSegments(strSearch[7]);
		lscc.selectedData(strSearch[8]);

		if (strSearch[3] == 'true') {
			getFieldByID("chkTAs").checked = true;
		} else {
			getFieldByID("chkTAs").checked = false;
			Disable('chkTAs', true);
		}
		if (strSearch[5] == 'true') {
			getFieldByID("chkCOs").checked = true;
		} else {
			getFieldByID("chkCOs").checked = false;
			Disable('chkCOs', true);
		}
	} else {
		Disable('chkTAs', true);
		Disable('chkCOs', true);
	}
}

function viewClick() {
	var tempDay;
	var tempMonth;
	var tempYear;
	var validate = false;

	var dateFrom = getText("txtFromDate");
	var dateTo = getText("txtToDate");

	tempIStartDate = dateFrom;
	tempIEndDate = dateTo;

	tempDay = tempIStartDate.substring(0, 2);
	tempMonth = tempIStartDate.substring(3, 5);
	tempYear = tempIStartDate.substring(6, 10);
	var tempOStartDate = (tempYear + tempMonth + tempDay);

	tempDay = tempIEndDate.substring(0, 2);
	tempMonth = tempIEndDate.substring(3, 5);
	tempYear = tempIEndDate.substring(6, 10);
	var tempOEndDate = (tempYear + tempMonth + tempDay);

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10) {
		dtCM = "0" + dtCM
	}
	if (dtCD < 10) {
		dtCD = "0" + dtCD
	}

	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear();

	tempDay = strSysDate.substring(0, 2);
	tempMonth = strSysDate.substring(3, 5);
	tempYear = strSysDate.substring(6, 10);

	strSysODate = (tempYear + tempMonth + tempDay);

	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();
	} else if (tempOStartDate > tempOEndDate) {
		showCommonError("Error", fromDtExceed);
		getFieldByID("txtToDate").focus();
	} else if (trim(lscc.getselectedData()).length == 0) {
		showCommonError("Error", carrierCodeRqrd);
	} else {

		var seg = getAllSegments().substr(0, getAllSegments().length - 1);
		setField("hdnMode", "VIEW");
		setField("hdnAgents", trim(ls.getselectedData()));
		setField("hdnBCs", trim(bcls.getselectedData()));
		setField("hdnStations", trim(stls.getselectedData()));
		setField("hdnSegments", seg);
		setField("hdnCarrierCode", trim(lscc.getselectedData()));

		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=600,height=330,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmInterlineRevenue");
		objForm.target = "CWindow";
		objForm.action = "showInterlineRevenueReport.action";
		objForm.submit();
		top[2].HidePageMessage();

	}
}

function addToList() {
	var isContained = false;
	top[2].HidePageMessage();
	var dept = getValue("selDeparture");
	var arr = getValue("selArrival");
	var via1 = getValue("selVia1");
	var via2 = getValue("selVia2");
	var via3 = getValue("selVia3");
	var via4 = getValue("selVia4");

	if (dept == "") {
		showERRMessage(depatureRequired);
		getFieldByID("selDeparture").focus();

	} else if (arr == "") {
		showERRMessage(arrivalRequired);
		getFieldByID("selArrival").focus();

	} else if (dept == arr) {
		showERRMessage(depatureArriavlSame);
		getFieldByID("selArrival").focus();

	} else if (arr == via1 || arr == via2 || arr == via3 || arr == via4) {
		showERRMessage(arriavlViaSame);
		getFieldByID("selArrival").focus();

	} else if (dept == via1 || dept == via2 || dept == via3 || dept == via4) {
		showERRMessage(depatureViaSame);
		getFieldByID("selDeparture").focus();

	} else if ((via2 != "" || via3 != "" || via4 != "") && via1 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia1").focus();

	} else if ((via3 != "" || via4 != "") && via2 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia2").focus();

	} else if ((via4 != "") && via3 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia3").focus();

	} else if ((via1 != "") && (via1 == via2 || via1 == via3 || via1 == via4)) {
		showERRMessage(viaSame);
		getFieldByID("selVia1").focus();

	} else if ((via2 != "") && (via2 == via3 || via2 == via4)) {
		showERRMessage(viaSame);
		getFieldByID("selVia2").focus();

	} else if ((via3 != "") && (via3 == via4)) {
		showERRMessage(viaSame);
		getFieldByID("selVia3").focus();
	} else {
		var str = "";
		str += dept;

		if (via1 != "") {
			str += "/" + via1;
		}
		if (via2 != "") {
			str += "/" + via2;
		}
		if (via3 != "") {
			str += "/" + via3;
		}
		if (via4 != "") {
			str += "/" + via4;
		}

		str += "/" + arr;

		var control = document.getElementById("selSegment");
		for ( var r = 0; r < control.length; r++) {
			if (control.options[r].text == str) {
				isContained = true;
				showERRMessage(OnDExists);
				break;
			}
		}
		if (isContained == false) {
			control.options[control.length] = new Option(str, str);
			clearStations();
		}
	}
}

function removeFromList() {
	var control = document.getElementById("selSegment");

	var selIndex = control.selectedIndex;
	if (selIndex != -1) {
		for (i = control.length - 1; i >= 0; i--) {
			if (control.options[i].selected) {
				control.options[i] = null;
			}
		}
		if (control.length > 0) {
			control.selectedIndex = selIndex == 0 ? 0 : selIndex - 1;
			clearStations();
		}
	}
}

function clickAgencies() {
	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
		Disable('chkCOs', false);
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
		Disable('chkCOs', true);
		getFieldByID("chkCOs").checked = false;
	}
}

function changeAgencies() {
	ls.clear();
}

function getAgentClick() {
	if (getValue("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
	} else {
		var strSearchCriteria = getValue("txtFromDate") + "#"
				+ getValue("txtToDate") + "#" + getValue("selAgencies") + "#"
				+ getFieldByID("chkTAs").checked + "#" + bcls.getselectedData()
				+ "#" + getFieldByID("chkCOs").checked + "#"
				+ stls.getselectedData() + "#" + getAllSegments() + "#"
				+ lscc.getselectedData();
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		setField("hdnAgents", "");
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmInterlineRevenue");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}

function clearStations() {
	getFieldByID("selDeparture").value = '';
	getFieldByID("selArrival").value = '';
	getFieldByID("selVia1").value = '';
	getFieldByID("selVia2").value = '';
	getFieldByID("selVia3").value = '';
	getFieldByID("selVia4").value = '';
}

function getAllSegments() {
	var control = document.getElementById("selSegment");
	var values = "";
	for ( var t = 0; t < (control.length); t++) {
		values += control.options[t].text + ",";
	}
	return values;
}

function setAllSegments(segs) {
	var control = document.getElementById("selSegment");
	var selSegs = segs.split(",");

	for ( var i = 0; i < selSegs.length; i++) {
		if (trim(selSegs[i]) != "") {
			control.options[control.length] = new Option(selSegs[i], selSegs[i]);
		}
	}

}
