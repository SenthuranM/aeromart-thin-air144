var objWindow;
var value;
var screenID = "UC_REPG_ST3";
setField("hdnLive", repLive);

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenID, isEdited);
}
function scheduleClick() {
	setField("hdnMode", "SCHEDULE");
	viewClick(true);
}

function viewClick(isSchedule) {
	if (getText("txtFromDate") == ""){
		showCommonError("Error", "From Date is Required.");
		getFieldByID("txtFromDate").focus();
	} else if (!dateChk(getText("txtFromDate"))){
		showCommonError("Error", "Wrong From Date Format.");
		getFieldByID("txtDate").focus();
	} else if (getText("txtToDate") == ""){
		showCommonError("Error", "To Date is Required.");
		getFieldByID("txtFromDate").focus();
	} else if (!dateChk(getText("txtToDate"))){
		showCommonError("Error", "Wrong To Date Format.");
		getFieldByID("txtDate").focus();
	} else if(checkGreat() && !validateDateRangeMonth()){
		showCommonError("Error","Maximum date range is 31.");
		getFieldByID("txtFromDate").focus();
	} else if (checkGreat()){
		if(isSchedule){
			scheduleReport();
		} else {
			setField("hdnMode", "VIEW");
			var strProp = 'toolbar=no,location=no,status=no,menubar=Yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
			top[0].objWindow = window.open("about:blank", "CWindow", strProp);
			var objForm = document.getElementById("frmGstr3Report");
			objForm.target = "CWindow";
			objForm.action = "showGSTR3Report.action";
			objForm.submit();
			top[2].HidePageMessage();
		}
	}
}

function scheduleReport(){
	UI_SchedRept.displayForm({divName:'divSchedFrom', formName:'frmGstr3Report',
		composerName: 'gstr3Report',
		errorCallback:function(message){
			showERRMessage(message);
		},
		successCallback:function(message){
			showCommonError("Confirmation", message);
		}
	});
}

function settingValidation(cntfield) {
	if (!dateChk(cntfield)) {
		showERRMessage(invaliddate);
		getFieldByID(cntfield).focus();
		return;
	}
}

function dataChanged() {
	objOnFocus();
	setPageEdited(true);
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function chkClick() {
	dataChanged();
}

function closeClick() {
	setPageEdited(false);
	objOnFocus();
	top[1].objTMenu.tabRemove(screenID)

}
function ckheckPageEdited() {
	if (top[1].objTMenu.tabGetPageEidted(screenID))
		return confirm("Changes will be lost! Do you wish to Continue?");
	else
		return true;
}

function checkGreat() {
	
	var tempDay;
	var tempMonth;
	var tempYear;
	var validate = false;
	
	var dateFrom = getText("txtFromDate");
	var dateTo = getText("txtToDate");

	tempIStartDate = dateFrom;
	tempIEndDate = dateTo;
			
	tempDay=tempIStartDate.substring(0,2);
	tempMonth=tempIStartDate.substring(3,5);
	tempYear=tempIStartDate.substring(6,10); 	
	var tempOStartDate=(tempYear+tempMonth+tempDay);		
	
	tempDay=tempIEndDate.substring(0,2);
	tempMonth=tempIEndDate.substring(3,5);
	tempYear=tempIEndDate.substring(6,10); 	
	var tempOEndDate=(tempYear+tempMonth+tempDay);			

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10){dtCM = "0" + dtCM}
	if (dtCD < 10){dtCD = "0" + dtCD}
 
	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
	
	tempDay=strSysDate.substring(0,2);
	tempMonth=strSysDate.substring(3,5);
	tempYear=strSysDate.substring(6,10);
	
	strSysODate=(tempYear+tempMonth+tempDay); 

	if(tempOStartDate > tempOEndDate){
		showCommonError("Error",fromDtExceed);
		getFieldByID("txtToDate").focus();
	} else if (!CheckDates(repStartDate, getText("txtFromDate"))) {
		showCommonError("Error", rptReriodExceeds + " " + repStartDate);
		getFieldByID("txtFromDate").focus();
	} else if (getText("passengerReportType") == "available" && tempOStartDate < strSysODate){
		showCommonError("Error",fromDatelessThanToday);
		getFieldByID("txtFromDate").focus();
	} else {
		validate = true;
	}
	return validate;
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function changeState(){
	var showState = false;
	$('#selState').find('option').remove().end();
	if($('#selCountry').val() != undefined && $('#selCountry').val() != ""){
		var selectedCountry = $('#selCountry').val();
		if(countryStateArry[selectedCountry] != undefined ){
			showState = true;
			$( 'select[name="selState"]' ).append(countryStateArry[selectedCountry]);
		}
	}
	if(showState){
		$("#stateRow").show();
	} else {
		$("#stateRow").hide();
	}		
}

function validateDateRangeMonth(){
	var valid = true;
	var dateRange = 31;
	
	var tempDay;
	var tempMonth;
	var tempYear;
	
	var dateFrom = getText("txtFromDate");
	tempDay = dateFrom.substring(0,2);
	tempMonth = dateFrom.substring(3,5);
	tempYear = dateFrom.substring(6,10);
	
	var date1 = new Date(tempYear, tempMonth, tempDay);
	
	var dateTo = getText("txtToDate");
	tempDay = dateTo.substring(0,2);
	tempMonth = dateTo.substring(3,5);
	tempYear = dateTo.substring(6,10);
	
	var date2 = new Date(tempYear, tempMonth, tempDay);
	
	var timeDiff = Math.abs(date2.getTime() - date1.getTime());
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
	
	if (diffDays > dateRange){
		valid = false;
	}
	
	return valid;
}

//On Page loading make sure that all the page input fields are cleared
function winOnLoad() {
	$('#selCountry').change(function() {
		dataChanged();
		changeState();	
	});
}