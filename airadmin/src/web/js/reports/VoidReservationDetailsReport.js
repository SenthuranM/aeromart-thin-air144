var objWindow;
var value;
var screenId="UC_REPM_082";
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();


function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtDepFromDate", strDate);
		break;
	case "1":
		setField("txtDepToDate", strDate);
		break;
	}
}


function LoadCalendar(strID, objEvent){
	objCal1.ID = strID;
	objCal1.top = 150 ;
	objCal1.left = 0 ;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}


function closeClick() {
	top[1].objTMenu.tabRemove(screenId);
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function winOnLoad() {
	getFieldByID("selAgencies").focus();
	if (top[1].objTMenu.tabGetValue(screenId) != ""
			&& top[1].objTMenu.tabGetValue(screenId) != null) {
		var strSearch = top[1].objTMenu.tabGetValue(screenId).split("#");

		setField("selAgencies", strSearch[0]);
		Disable('chkTAs', false);
		Disable('chkCOs', false);

		if (strSearch[1] == 'true') {
			getFieldByID("chkTAs").checked = true;
		} else {
			getFieldByID("chkTAs").checked = false;
			Disable('chkTAs', true);
		}
		if (strSearch[2] == 'true') {
			getFieldByID("chkCOs").checked = true;
		} else {
			getFieldByID("chkCOs").checked = false;
			Disable('chkCOs', true);
		}
		if (strSearch[3] != null && strSearch[3] != "") {
			setSelectedDataWithGroup(strSearch[3],ls);
		}
		if (strSearch[4] != null && strSearch[4] != "") {
			setSelectedDataWithGroup(strSearch[4],ls2);
		}
		if(strSearch[5] != null && strSearch[5] != ""){
			setField("txtDepFromDate",strSearch[5]);
		}
		if(strSearch[6] != null && strSearch[6] != ""){
			setField("txtDepToDate",strSearch[6]);
		}
	} else {
		Disable('chkTAs', true);
		Disable('chkCOs', true);
	}
}


function clickAgencies() {
	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
		Disable('chkCOs', false);
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
		Disable('chkCOs', true);
		getFieldByID("chkCOs").checked = false;
	}
}


function loadUsersClick() {

	var agents = getAgents();

	if (getValue("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
	} else if (agents == undefined || agents == null || agents == "") {
		showCommonError("Error", agentsRqrd);
		getFieldByID("lstRoles").focus();
	} else {
		ls2.removeAllFromListbox();
		//ls4.removeAllFromListbox();
		var strSearchCriteria = getValue("selAgencies") + "#"
				+ getFieldByID("chkTAs").checked + "#"
				+ getFieldByID("chkCOs").checked + "#"
				+ ls.getSelectedDataWithGroup() + "#"
				+ ls2.getSelectedDataWithGroup() + "#"
				+ getValue('txtDepFromDate') + "#"
				+ getValue('txtDepToDate') + "#";
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		setField("hdnAgents", getAgents());
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmVoidReservationDetailsReport");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}


function changeAgencies() {
	ls.clear();
	ls2.clear();
	//ls3.removeAllFromListbox();
	//ls4.clear();
}


function getAgentClick() {
	if (getValue("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
	} else {
		ls.clear();
		ls2.clear();
		//ls4.clear();
		var strSearchCriteria = getValue("selAgencies") + "#"
				+ getFieldByID("chkTAs").checked + "#"
				+ getFieldByID("chkCOs").checked + "#"
				+ ls.getSelectedDataWithGroup() + "#"
				+ ls2.getSelectedDataWithGroup() + "#"
				+ getValue('txtDepFromDate') + "#"
				+ getValue('txtDepToDate') + "#";
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		setField("hdnAgents", "");
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmVoidReservationDetailsReport");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}


function getUsers() {
	var strUsers = ls2.getSelectedDataWithGroup();
	var newUsers;
	if (strUsers.indexOf(":") != -1) {
		strUsers = replaceall(strUsers, ":", ",");
	}
	if (strUsers.indexOf("|") != -1) {
		newUsers = replaceall(strUsers, "|", ",");
	} else {
		newUsers = strUsers;
	}
	return newUsers;
}


function getAgents() {
	var strAgents = ls.getSelectedDataWithGroup();
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents;
	}
	return newAgents;
}


function setSelectedDataWithGroup(strData,listBoxID) {
	var str = "";

	var groups = strData.split("|");
	var groupList = new Array();

	for ( var x = 0; x < groups.length; x++) {

		if (groups[x].indexOf(":") != -1) {
			var dd = groups[x].split(":");
			groupList = dd[1].split(",");

			for ( var z = 0; z < groupList.length; z++) {
				if (str == "") {
					str += dd[0] + ":" + groupList[z];
				} else {
					str += "," + dd[0] + ":" + groupList[z];
				}
			}
		} else {
			if (str == "") {
				str += groups[x];
			} else {
				str += "," + groups[x];
			}
		}
	}
	listBoxID.move('>', str);
}

function removeSelectedDataWithGroup(strData,listBoxID) {
	var str = "";

	var groups = strData.split("|");
	var groupList = new Array();

	for ( var x = 0; x < groups.length; x++) {

		if (groups[x].indexOf(":") != -1) {
			var dd = groups[x].split(":");
			groupList = dd[1].split(",");

			for ( var z = 0; z < groupList.length; z++) {
				if (str == "") {
					str += dd[0] + ":" + groupList[z];
				} else {
					str += "," + dd[0] + ":" + groupList[z];
				}
			}
		} else {
			if (str == "") {
				str += groups[x];
			} else {
				str += "," + groups[x];
			}
		}
	}
	listBoxID.move('<', str);
}


function viewClick() {
	
	if (getText("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
	}else if (getUsers() == "") {
		showCommonError("Error", usersRqrd);
		getFieldByID("lstUsers").focus();
	}else if (getFieldByID("txtDepFromDate").value == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtDepFromDate").focus();
	}else if(getFieldByID("txtDepToDate").value == ""){
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtDepToDate").focus();
	}else{
		setField("hdnMode", "VIEW");
		setField("hdnUsers",getUsers());
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmVoidReservationDetailsReport");
		objForm.target = "CWindow";
		objForm.action = "showVoidReservationDetailsReport.action";
		objForm.submit();
		top[2].HidePageMessage();
	}
}

