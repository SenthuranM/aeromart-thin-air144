var objWindow;
var value;
var screenID = "UC_REPM_027";
setField("hdnLive",repLive);
	
	var objCal1 = new Calendar("spnCalendarDG1");
	objCal1.onClick = "setDate";
	objCal1.blnDragCalendar = false;
	objCal1.buildCalendar();
	
function setDate(strDate, strID){
	switch (strID){
		case "0" : setField("txtFromDate",strDate);break ;
		case "1" : setField("txtToDate",strDate);break ;
		case "2" : setField("txtDepFromDate",strDate);break ;
		case "3" : setField("txtDepToDate",strDate);break ;
	}
}

function LoadCalendar(strID, objEvent){
	objCal1.ID = strID;
	objCal1.top = 1 ;
	objCal1.left = 0 ;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}


function setPageEdited(isEdited){
	top[1].objTMenu.tabPageEdited(screenID, isEdited);
}

function radioReportChanged(){
	if(getValue("radOption")=="POST_DEP"){
		setField("hdnRptType","POST_DEP");
	}else {
		setField("hdnRptType","PRE_DEP");
	}
}

function zuluClick() {
	setField("hdnTime","ZULU");
}
	
function localClick() {
	setField("hdnTime","LOCAL");
}


function viewClick(){
	
	if(getValue("radOption")=="POST_DEP"){
		setField("hdnRptType","POST_DEP");
	}else {
		setField("hdnRptType","PRE_DEP");
	}
	if(getValue("chkZulu")=="ZULU"){
		setField("hdnTime","ZULU");
	}else{
		setField("hdnTime","LOCAL");
	}
				
	if(getText("txtFromDate")==""){
		showCommonError("Error",fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if(dateValidDate(getText("txtFromDate"))==false){
		showCommonError("Error",fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	}  else if(getText("txtToDate")==""){
		showCommonError("Error",toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if(dateValidDate(getText("txtToDate"))==false){
		showCommonError("Error",toDtinvalid);
		getFieldByID("txtToDate").focus();
	}else if((getText("txtDepFromDate")!= "") && dateValidDate(getText("txtDepFromDate"))==false){
		showCommonError("Error",fromDtInvalid);
		getFieldByID("txtDepFromDate").focus();		 		
	} else if((getText("txtDepToDate")!= "") && dateValidDate(getText("txtDepToDate"))==false){
		showCommonError("Error",toDtinvalid);
		getFieldByID("txtDepToDate").focus();
		
	}else if(dateValidDate(getText("txtFromDate"))==true 
			&& dateValidDate(getText("txtToDate"))==true && checkGreat()) {
		setField("hdnMode","VIEW");
		var strProp = 'toolbar=no,location=no,status=no,menubar=Yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'	
		top[0].objWindow = window.open("about:blank","CWindow",strProp);
		var objForm  = document.getElementById("frmCNXReserv");
		objForm.target = "CWindow";
		objForm.action = "showCancelReserReport.action";
		objForm.submit();
		top[2].HidePageMessage();
	
	}
}

function settingValidation(cntfield){
	if (!dateChk(cntfield))	{
		showERRMessage(invaliddate);
		getFieldByID(cntfield).focus();
		return;
	}
}

function dataChanged(){
	objOnFocus();
	setPageEdited(true);
}

function objOnFocus(){
	top[2].HidePageMessage();
}

function chkClick() {
	dataChanged();
}

function closeClick() {
		setPageEdited(false);
		objOnFocus();
		top[1].objTMenu.tabRemove(screenID)
	
}
function ckheckPageEdited() {
	if (top[1].objTMenu.tabGetPageEidted(screenID))
	return confirm("Changes will be lost! Do you wish to Continue?");
	else return true;
}

function checkGreat() {
	var validate = false;
	if(!CheckDates(getText("txtFromDate"),getText("txtToDate"))) {
		validate = false;
		showCommonError("Error",fromDtExceed);
		getFieldByID("txtToDate").focus();
	}else if (!CheckDates(repStartDate,getText("txtFromDate"))) {
		showCommonError("Error",rptReriodExceeds +" "+ repStartDate);
		getFieldByID("txtFromDate").focus();			
	} else if(!CheckDates(getText("txtDepFromDate"),getText("txtDepToDate"))) {
		validate = false;
		showCommonError("Error",fromDtExceed);
		getFieldByID("txtDepToDate").focus();
	}
	else {
		validate = true;
	}
	return validate;
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}	
}
