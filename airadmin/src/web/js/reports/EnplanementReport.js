var objWindow;
var value;
setField("hdnLive", repLive);

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function winOnLoad() {
	getFieldByID("txtFromDate").focus();

}
function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

var screenId = "UC_REPM_004";
function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function viewClick() {

	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true
			&& validateReportDate("txtFromDate", "txtToDate", repStartDate)) {
		
		var seg = getAllSegments().substr(0, getAllSegments().length - 1);
		setField("hdnSegments",seg);
		setField("hdnMode", "VIEW");
		
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmEnplanementReport");
		objForm.target = "CWindow";

		objForm.action = "showEnplanementReport.action";
		objForm.submit();
		top[2].HidePageMessage();
	}
}

function chkClick() {
	
}

function addToList() {
	var isContained = false;
	top[2].HidePageMessage();
	var dept = getValue("selDeparture");
	var arr = getValue("selArrival");
	var via1 = getValue("selVia1");
	var via2 = getValue("selVia2");
	var via3 = getValue("selVia3");
	var via4 = getValue("selVia4");

	if (dept == "") {
		showERRMessage(depatureRequired);
		getFieldByID("selDeparture").focus();

	} else if (arr == "") {
		showERRMessage(arrivalRequired);
		getFieldByID("selArrival").focus();

	} else if (dept == arr) {
		showERRMessage(depatureArriavlSame);
		getFieldByID("selArrival").focus();

	} else if (arr == via1 || arr == via2 || arr == via3 || arr == via4) {
		showERRMessage(arriavlViaSame);
		getFieldByID("selArrival").focus();

	} else if (dept == via1 || dept == via2 || dept == via3 || dept == via4) {
		showERRMessage(depatureViaSame);
		getFieldByID("selDeparture").focus();

	} else if ((via2 != "" || via3 != "" || via4 != "") && via1 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia1").focus();

	} else if ((via3 != "" || via4 != "") && via2 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia2").focus();

	} else if ((via4 != "") && via3 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia3").focus();

	} else if ((via1 != "") && (via1 == via2 || via1 == via3 || via1 == via4)) {
		showERRMessage(viaSame);
		getFieldByID("selVia1").focus();

	} else if ((via2 != "") && (via2 == via3 || via2 == via4)) {
		showERRMessage(viaSame);
		getFieldByID("selVia2").focus();

	} else if ((via3 != "") && (via3 == via4)) {
		showERRMessage(viaSame);
		getFieldByID("selVia3").focus();
	} else {
		var str = "";
		str += dept;

		if (via1 != "") {
			str += "/" + via1;
		}
		if (via2 != "") {
			str += "/" + via2;
		}
		if (via3 != "") {
			str += "/" + via3;
		}
		if (via4 != "") {
			str += "/" + via4;
		}

		str += "/" + arr;

		var control = document.getElementById("selSegment");
		for ( var r = 0; r < control.length; r++) {
			if (control.options[r].text == str) {
				isContained = true;
				showERRMessage(OnDExists);
				break;
			}
		}
		if (isContained == false) {
			control.options[control.length] = new Option(str, str);
			clearStations();
		}
	}
}

function removeFromList() {
	var control = document.getElementById("selSegment");

	var selIndex = control.selectedIndex;
	if (selIndex != -1) {
		for (i = control.length - 1; i >= 0; i--) {
			if (control.options[i].selected) {
				control.options[i] = null;
			}
		}
		if (control.length > 0) {
			control.selectedIndex = selIndex == 0 ? 0 : selIndex - 1;
			clearStations();
		}
	}
}

function clearStations() {
	getFieldByID("selDeparture").value = '';
	getFieldByID("selArrival").value = '';
	getFieldByID("selVia1").value = '';
	getFieldByID("selVia2").value = '';
	getFieldByID("selVia3").value = '';
	getFieldByID("selVia4").value = '';
}

function getAllSegments() {
	var control = document.getElementById("selSegment");
	var values = "";
	for ( var t = 0; t < (control.length); t++) {
		values += control.options[t].text + ",";
	}
	return values;
}
