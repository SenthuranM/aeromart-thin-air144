var objWindow;
var value;
setField("hdnLive", repLive);

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

var screenId = "UC_REPM_026";
function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function viewClick() {
	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();
	} else if (getFieldByID("radCustomerNationality").checked == true
			&& getFieldByID("selNationality").value == "") {
		showCommonError("Error", nationalityEmpty);
		getFieldByID("selNationality").focus();
	} else if (getFieldByID("radCustomerCountry").checked == true
			&& getFieldByID("selCountryOfRes").value == "") {
		showCommonError("Error", countryEmpty);
		getFieldByID("selCountryOfRes").focus();
	} else if (getFieldByID("chkSector").checked == true
			&& getFieldByID("selSectorFrom").value == "") {
		showCommonError("Error", sectorFromEmpty);
		getFieldByID("selSectorFrom").focus();
	} else if (getFieldByID("chkSector").checked == true
			&& getFieldByID("selSectorTo").value == "") {
		showCommonError("Error", sectorToEmpty);
		getFieldByID("selSectorTo").focus();
	} else if (getFieldByID("chkBookingStatus").checked == true
			&& getFieldByID("selBookingStatus").value == ""){
		showCommonError("Error", "TAIR-90167:Booking Status should be selected");
		getFieldByID("selBookingStatus").focus();
	} else if (dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true
			&& validateReportDate("txtFromDate", "txtToDate", repStartDate)) {

		if(getFieldByID("chkGetAllPax").checked == true){
			setField("hdnRequestAllPassengers",true);
		}
		setField("hdnMode", "VIEW");

		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmWebProfile");
		objForm.target = "CWindow";

		objForm.action = "showWebProfile.action";
		objForm.submit();
		top[2].HidePageMessage();
	}
}

function chkClick() {
	if (getFieldByID("radCustomerNationality").checked == true) {
		Disable("selNationality", false);
		getFieldByID("selNationality").focus();
	} else {
		setField("selNationality", "");
		Disable("selNationality", true);
	}
	if (getFieldByID("radCustomerCountry").checked == true) {
		Disable("selCountryOfRes", false);
		getFieldByID("selCountryOfRes").focus();
	} else {
		setField("selCountryOfRes", "");
		Disable("selCountryOfRes", true);
	}
}

function cusTravelCheck() {
	var validate = false;
	if (getFieldByID("chkSector").checked == true) {
		if (getFieldByID("selSectorFrom").value == "") {
			showCommonError("Error", sectorFromEmpty);
			getFieldByID("selSectorFrom").focus();
		} else if (getFieldByID("selSectorTo").value == "") {
			showCommonError("Error", sectorToEmpty);
			getFieldByID("selSectorTo").focus();
		}
		validate = true;
	}
	return validate;
}

function pageLoadCus() {
	Disable("selSectorFrom", true);
	Disable("selSectorTo", true);
	Disable("selBookingStatus", true);
	Disable("selNationality", true);
	Disable("selCountryOfRes", true);
	getFieldByID("txtFromDate").focus();
}

function checkClick() {
	if (getFieldByID("chkSector").checked == true) {
		Disable("selSectorFrom", false);
		Disable("selSectorTo", false);
		getFieldByID("selSectorFrom").focus();
	} else {
		Disable("selSectorFrom", true);
		Disable("selSectorTo", true);
		setField("selSectorFrom", "");
		setField("selSectorTo", "");
	}
}

function checkBookingStatusClick(){
	if (getFieldByID("chkBookingStatus").checked == true) {
		Disable("selBookingStatus", false);
		getFieldByID("selBookingStatus").focus();
	} else {
		Disable("selBookingStatus", true);
		setField("selBookingStatus", "");
	}
}
