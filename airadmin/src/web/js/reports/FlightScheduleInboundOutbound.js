var objWindow;
var value;
setField("hdnLive", repLive);

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}
var screenId = "UC_REPM_017";
function closeClick() {
	setPageEdited(false);
	top[1].objTMenu.tabRemove(screenId);

}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function dataChanged() {
	setPageEdited(true);
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function viewClick() {
	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true && checkGreat()) {
		setField("hdnMode", "VIEW");
		var strProp = 'toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmPage");
		objForm.target = "CWindow";
		objForm.action = "flightScheduleInboundOutboundReport.action";
		objForm.submit();
		top[2].HidePageMessage();

	}
}

function PreviousMonth() {
	objOnFocus();
	var strStartD = getFieldByID("txtFromDate").value;
	var strStopD = "";
	var strDaysStopDate = "";
	setField("txtFromDate", addMonths(-1, "01" + strStartD.substr(2, 8)));
	strStopD = addMonths(-1, "01" + strStartD.substr(2, 8));
	strDaysStopDate = getDaysInMonth(strStopD.substr(3, 2), strStopD.substr(6,
			4));
	setField("txtToDate", addMonths(0, strDaysStopDate + strStopD.substr(2, 8)));
}

function NextMonth() {
	objOnFocus();
	var strStartD = getFieldByID("txtToDate").value;
	var strStopD = "";
	var strDaysStopDate = "";
	setField("txtFromDate", addMonths(1, "01" + strStartD.substr(2, 8)));
	strStopD = addMonths(1, "01" + strStartD.substr(2, 8));
	strDaysStopDate = getDaysInMonth(strStopD.substr(3, 2), strStopD.substr(6,
			4));
	setField("txtToDate", addMonths(0, strDaysStopDate + strStopD.substr(2, 8)));
}

function checkGreat() {
	var validate = false;
	if (!CheckDates(getText("txtFromDate"), getText("txtToDate"))) {
		validate = false;
		showCommonError("Error", fromDtExceed);
		getFieldByID("txtToDate").focus();
	} else if (!CheckDates(repStartDate, getText("txtFromDate"))) {
		showCommonError("Error", rptReriodExceeds + " " + repStartDate);
		getFieldByID("txtFromDate").focus();
	} else {
		validate = true;
	}
	return validate;
}

function objOnFocus() {
	top[2].HidePageMessage();
}