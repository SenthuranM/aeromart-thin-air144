var objWindow;
setField("hdnLive", repLive);
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromArrDate", strDate);
		break;
	case "1":
		setField("txtToArrDate", strDate);
		break;		
	case "2":
		setField("txtFromDepDate", strDate);
		break;
	case "3":
		setField("txtToDepDate", strDate);
		break;

	}
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}
var screenId = "UC_REPM_088";

function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function radioReportChanged(){
	if(getValue("radOption")=="DETAIL"){
		setField("hdnRptType","DETAIL");
	}else if(getValue("radOption")=="PAX_DETAILS"){
		setField("hdnRptType","PAX_DETAILS");
	}else {
		setField("hdnRptType","SUMMARY");
	}
}

function viewClick() {
	
	if (getValue("radOption") == "DETAIL") {
		setField("hdnRptType", "DETAIL");
	} 
	else if(getValue("radOption")=="PAX_DETAILS"){
		setField("hdnRptType","PAX_DETAILS");
	}else {
		setField("hdnRptType", "SUMMARY");
	}

	if (displayAgencyMode == 1) {
		setField("hdnAgents", getAgents());
	} else {
		setField("hdnAgents", "");
	}
	if ((getText("txtFromDepDate") == "") && (getText("txtToDepDate") == "")
			&& getText("txtFromArrDate") == "" && getText("txtToArrDate") == "") {
		showCommonError("Error", dateRangeRequired);
		getFieldByID("txtFromDepDate").focus();
	}
	if ((getText("txtFromDepDate") != "")
			&& dateValidDate(getText("txtFromDepDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDepDate").focus();
	} else if ((getText("txtFromDepDate") != "")
			&& (getText("txtToDepDate") == "")) {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDepDate").focus();
	} else if ((getText("txtToDepDate") != "")
			&& dateValidDate(getText("txtToDepDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDepDate").focus();
	} else if ((getText("txtFromArrDate") != "")
			&& dateValidDate(getText("txtFromArrDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromArrDate").focus();
	} else if (getText("txtFromArrDate") != "" && getText("txtToArrDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToArrDate").focus();
	} else if ((getText("txtToArrDate") != "") && dateValidDate(getText("txtToArrDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToArrDate").focus();
	} else if ((getText("txtFlightNumber") != "")
			&& (!isFlightNoValid("txtFlightNumber"))) {
		showCommonError("Error", flightNoInvalid);
		getFieldByID("txtFlightNumber").focus();
	} else if ((displayAgencyMode == 1)
			&& ( getText("hdnRptType") == "DETAIL" || getText("hdnRptType") == "PAX_DETAILS" ) && getText("hdnAgents") == "") {
		showCommonError("Error", agentsRqrd);
	} else if (getText("txtFromArrDate") != "" && getText("txtToArrDate") != "" && !CheckDates(getText("txtFromArrDate"),getText("txtToArrDate"))) {
		showCommonError("Error", fromDtExceed);
		getFieldByID("txtFromArrDate").focus();
	} else if (getText("txtFromDepDate") != "" && getText("txtToDepDate") != "" && !CheckDates(getText("txtFromDepDate"),getText("txtToDepDate"))) {
		showCommonError("Error", fromDtExceed);
		getFieldByID("txtFromDepDate").focus();
	} else if (getText("txtFromArrDate") != "" && getText("txtFromDepDate") != "" && !CheckDates(getText("txtFromArrDate"),getText("txtFromDepDate"))) {
		showCommonError("Error", "Departure From Date Cannot be Less than Arrival From Date");
		getFieldByID("txtFromDepDate").focus();
	} else if ((getText("txtFromArrDate") != "" && getText("txtToArrDate") != "" && CheckDates(getText("txtFromArrDate"),getText("txtToArrDate"))) || 
			(getText("txtFromDepDate") != "" && getText("txtToDepDate") != "" && CheckDates(getText("txtFromDepDate"),getText("txtToDepDate")))) {

		setField("hdnMode", "VIEW");
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmOnHoldPag");
		objForm.target = "CWindow";

		objForm.action = "showIntlFlightArrivalDepartureReport.action";
		objForm.submit();
		top[2].HidePageMessage();

	}
}

function load() {
	getFieldByID("txtFromDepDate").focus();
	if (top[1].objTMenu.tabGetValue(screenId) != ""
			&& top[1].objTMenu.tabGetValue(screenId) != null) {
		var strSearch = top[1].objTMenu.tabGetValue(screenId).split("#");
		setField("txtFromArrDate", strSearch[0]);
		setField("txtToArrDate", strSearch[1]);
		setField("selAgencies", strSearch[2]);
		setField("txtFlightNumber", strSearch[5]);
		
		if (strSearch[3] == 'true') {
			getFieldByID("chkTAs").checked = true;
		} else {
			getFieldByID("chkTAs").checked = false;
			Disable('chkTAs', true);
		}
		if (strSearch[4] == 'true') {
			getFieldByID("chkCOs").checked = true;
		} else {
			getFieldByID("chkCOs").checked = false;
			Disable('chkCOs', true);
		}
		setField("txtFromDepDate", strSearch[6]);
		setField("txtToDepDate", strSearch[7]);
		if(strSearch[8] == 'DETAIL'){
			getFieldByID("radOption").checked = true;
		}else if(strSearch[8] == 'SUMMARY'){
			getFieldByID("radOptionSummary").checked = true;
		}else if(strSearch[8] == 'PAX_DETAILS'){
			getFieldByID("radOptionPaxDetails").checked = true;
		}
		if (strSearch[9] != null && strSearch[9] != "") {
			if(ls.getNotSelectedDataWithGroup() == strSearch[9]){
                setSelectedDataWithGroup(strSearch[9],ls,true);
			}
			else{
                setSelectedDataWithGroup(strSearch[9],ls,false);                        
			}
		}
		
		if (strSearch[10] != null && strSearch[10] != "") {
			setSelectedDataWithGroup(strSearch[10],ls2);
		}
		
	} else {
		Disable('chkTAs', true);
		Disable('chkCOs', true);
	}
}

function radioOptionChanged() {

	if (getText("radAgencey") == "Agencey") {
		Disable("selAgencies", "true");
		Disable("btnGetAgent", "true");
	} else {
		Disable("selAgencies", "");
		Disable("btnGetAgent", "");
	}
}

function clickAgencies() {

	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
		Disable('chkCOs', false);
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
		Disable('chkCOs', true);
		getFieldByID("chkCOs").checked = false;
	}
}

function changeAgencies() {
	ls.clear();
}

function getAgents() {
	var strAgents;
	if (getText("selAgencies") != "All") {
		strAgents = ls.getSelectedDataWithGroup();
	} else {
		strAgents = ls.getselectedData();
	}
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents;
	}
	return newAgents;
}


function getAgentClick() {
	if (getValue("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
	} else {
		var strSearchCriteria = getValue("txtFromArrDate") + "#"
				+ getValue("txtToArrDate") + "#" + getValue("selAgencies") + "#"
				+ getFieldByID("chkTAs").checked + "#"
				+ getFieldByID("chkCOs").checked + "#"
				+ getValue("txtFlightNumber") + "#" 
				+ getValue("txtFromDepDate") + "#"
				+ getValue("txtToDepDate")+ "#"
				+ getValue("radOption")+ "#"
				+"#"
				+"#";
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		setField("hdnAgents", "");
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmOnHoldPag");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}


function setSelectedDataWithGroup(strData,listBoxID,isAll) {
	var str = "";

	var groups = strData.split("|");
	var groupList = new Array();
	
	if(isAll){
        listBoxID.moveOptions('>>');
    }
    else{
    	for ( var x = 0; x < groups.length; x++) {

			if (groups[x].indexOf(":") != -1) {
				var dd = groups[x].split(":");
				groupList = dd[1].split(",");

				for ( var z = 0; z < groupList.length; z++) {
					if (str == "") {
						str += dd[0] + ":" + groupList[z];
					} else {
						str += "," + dd[0] + ":" + groupList[z];
					}
				}
			} else {
				if (str == "") {
					str += groups[x];
				} else {
					str += "," + groups[x];
				}
			}
		}
    	listBoxID.move('>', str);
    }
}

function isFlightNoValid(txtfield) {
	var strflt = getFieldByID(txtfield).value;
	var strLen = strflt.length;
	var valid = false;
	if ((trim(strflt) != "") && (isAlphaNumeric(trim(strflt)))) {
		setField(txtfield, trim(strflt)); 
		valid = true;
	}
	return valid;
}
