var objWindow;
setField("hdnLive", repLive);
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;		
	case "2":
		setField("txtFromDepDate", strDate);
		break;
	}
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}
var screenId = "UC_REPM_082";

function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function scheduleClick() {
	setField("hdnMode","SCHEDULE");	
	viewReport(true);
}

function viewClick() {
	setField("hdnMode","VIEW");	
	viewReport(false);
}

function scheduleReport(){
	UI_SchedRept.displayForm({divName:'divSchedFrom', formName:'frmOnHoldPag', 
		composerName: 'agentChargeAdjustmentReport',
		errorCallback:function(message){
			showERRMessage(message);
		},
		successCallback:function(message){
			showCommonError("Confirmation", message);
		}
	});
}

function viewReport(isSchedule) {	
	if (displayAgencyMode == 1 || displayAgencyMode == 2) {
		setField("hdnAgents", getAgents());
	} else {
		setField("hdnAgents", "");
	}
	setField("hdnUsers", getUsers());
	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();
	} else if (displayAgencyMode == 1 && getText("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();

	} else if ((displayAgencyMode == 1 || displayAgencyMode == 2)
			&& getText("hdnAgents") == "") {
		showCommonError("Error", agentsRqrd);
	} else if (dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true
			&& validateReportDate("txtFromDate", "txtToDate", repStartDate)) {
		if(isSchedule){
			scheduleReport();
		} else {
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
			top[0].objWindow = window.open("about:blank", "CWindow", strProp);
			var objForm = document.getElementById("frmOnHoldPag");
			objForm.target = "CWindow";

			objForm.action = "showAgentChargeAdjustmentsReport.action";
			objForm.submit();
			top[2].HidePageMessage();
		}
	}
}

function load() {
	getFieldByID("txtFromDate").focus();

	
	if (top[1].objTMenu.tabGetValue(screenId) != ""
			&& top[1].objTMenu.tabGetValue(screenId) != null) {
		var strSearch = top[1].objTMenu.tabGetValue(screenId).split("#");
		setField("txtFromDate", strSearch[0]);
		setField("txtToDate", strSearch[1]);
		setField("selAgencies", strSearch[2]);
		

		if (strSearch[9] != null && strSearch[9] != "") {
			if(ls.getNotSelectedDataWithGroup() == strSearch[9]){
                setSelectedDataWithGroup(strSearch[9],ls,true);
			}
			else{
                setSelectedDataWithGroup(strSearch[9],ls,false);                        
			}
		}
		
		if (strSearch[10] != null && strSearch[10] != "") {
			setSelectedDataWithGroup(strSearch[10],ls2);
		}
		
		setNominals(strSearch[15]);

	} else {

	}
}

function radioOptionChanged() {

	if (getText("radAgencey") == "Agencey") {
		Disable("selAgencies", "true");
		Disable("btnGetAgent", "true");
	} else {
		Disable("selAgencies", "");
		Disable("btnGetAgent", "");
	}
}

function getNominals(){
	var selectedNominals;
	
	if(getFieldByID("chkTypeR").checked)
		selectedNominals = "chkTypeR";
	
	if(getFieldByID("chkTypeC").checked)			
		selectedNominals += "*chkTypeC";	
	
	if(getFieldByID("chkTypeD").checked)
		selectedNominals += "*chkTypeD";	
	
	if(getFieldByID("chkTypeRR").checked)
		selectedNominals += "*chkTypeRR";
	
	if(getFieldByID("chkTypeA").checked)
		selectedNominals += "*chkTypeA";
	
	if(getFieldByID("chkTypeQ").checked)
		selectedNominals += "*chkTypeQ";
	
	if(getFieldByID("chkTypeRV").checked)
		selectedNominals += "*chkTypeRV";
	
	if(getFieldByID("chkTypeBSP").checked)
		selectedNominals += "*chkTypeBSP";
	
	return selectedNominals;
}

function setNominals(nominals){
	
	var nominalsList = nominals.split("*");
	for(var i= 0;i<nominalsList.length;i++ ){
		if(getFieldByID(nominalsList[i]))
		getFieldByID(nominalsList[i]).checked = true;
	}
}



function changeAgencies() {
	ls.clear();
}

function getAgents() {
	var strAgents;
	if (getText("selAgencies") != "All") {
		strAgents = ls.getSelectedDataWithGroup();
	} else {
		strAgents = ls.getselectedData();
	}
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents;
	}
	return newAgents;
}
function getUsers() {
	var strUsers = ls2.getselectedData();
	var newUsers;
	if (strUsers.indexOf(":") != -1) {
		strUsers = replaceall(strUsers, ":", ",");
	}
	if (strUsers.indexOf("|") != -1) {
		newUsers = replaceall(strUsers, "|", ",");
	} else {
		newUsers = strUsers;
	}
	return newUsers;
}
function getAgentClick() {
	if (getValue("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
	} else {
		var strSearchCriteria = getValue("txtFromDate") + "#"
				+ getValue("txtToDate") + "#" + getValue("selAgencies") + "#"
				+ "#"+"#"+"#"+"#"+"#"+"#"+"#"
				+"#"+"#"+"#"+"#"+getNominals();
		
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		setField("hdnAgents", "");
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmOnHoldPag");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}


function loadUsersClick() {

	var agents = getAgents();

	if (displayAgencyMode == 1  && getValue("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
	} else if (agents == undefined || agents == null || agents == "") {
		showCommonError("Error", agentsRqrd);
		getFieldByID("lstRoles").focus();
	} else {
		ls2.removeAllFromListbox();		
		
		var strSearchCriteria = getValue("txtFromDate") + "#"
		+ getValue("txtToDate") + "#" + getValue("selAgencies") + "#"
		+ "#"+"#"+"#"+"#"+"#"+"#"
		+ ls.getSelectedDataWithGroup() + "#"
		+ ls2.getSelectedDataWithGroup() + "#"+"#"+"#"+"#"+getNominals();
		
		
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		setField("hdnAgents", getAgents());
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmOnHoldPag");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}

function setSelectedDataWithGroup(strData,listBoxID,isAll) {
	var str = "";

	var groups = strData.split("|");
	var groupList = new Array();
	
	if(isAll){
        listBoxID.moveOptions('>>');
    }
    else{
    	for ( var x = 0; x < groups.length; x++) {

			if (groups[x].indexOf(":") != -1) {
				var dd = groups[x].split(":");
				groupList = dd[1].split(",");

				for ( var z = 0; z < groupList.length; z++) {
					if (str == "") {
						str += dd[0] + ":" + groupList[z];
					} else {
						str += "," + dd[0] + ":" + groupList[z];
					}
				}
			} else {
				if (str == "") {
					str += groups[x];
				} else {
					str += "," + groups[x];
				}
			}
		}
    	listBoxID.move('>', str);
    }
}

function isFlightNoValid(txtfield) {
	var strflt = getFieldByID(txtfield).value;
	var strLen = strflt.length;
	var valid = false;
	if ((trim(strflt) != "") && (isAlphaNumeric(trim(strflt)))) {
		setField(txtfield, trim(strflt)); 
		valid = true;
	}
	return valid;
}

function checkWithSysDate(strdate){
	
	var dateValue = getText(strdate);
	var tempDate = dateValue;
	
	tempDay=tempDate.substring(0,2);
	tempMonth=tempDate.substring(3,5);
	tempYear=tempDate.substring(6,10); 	
	var tempODate=(tempYear+tempMonth+tempDay);			

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10){dtCM = "0" + dtCM}
	if (dtCD < 10){dtCD = "0" + dtCD}
 
	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
	
	tempDay=strSysDate.substring(0,2);
	tempMonth=strSysDate.substring(3,5);
	tempYear=strSysDate.substring(6,10);
	
	strSysODate=(tempYear+tempMonth+tempDay); 
			
	if(tempODate > strSysODate){		
		return false;
	} else{		
		return true;
	}
}