var objWindow;
var value;
var screenId="UC_REPM_083";
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();


function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtBookingFromDate", strDate);
		break;
	case "1":
		setField("txtBookingToDate", strDate);
		break;
	}
}


function LoadCalendar(strID, objEvent){
	objCal1.ID = strID;
	objCal1.top = 150 ;
	objCal1.left = 0 ;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}


function closeClick() {
	top[1].objTMenu.tabRemove(screenId);
}


function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}


function winOnLoad() {
	getFieldByID("selAgencies").focus();
	if (top[1].objTMenu.tabGetValue(screenId) != ""
			&& top[1].objTMenu.tabGetValue(screenId) != null) {
		var strSearch = top[1].objTMenu.tabGetValue(screenId).split("#");

		setField("selAgencies", strSearch[0]);
		Disable('chkTAs', false);
		Disable('chkCOs', false);

		if (strSearch[1] == 'true') {
			getFieldByID("chkTAs").checked = true;
		} else {
			getFieldByID("chkTAs").checked = false;
			Disable('chkTAs', true);
		}
		if (strSearch[2] == 'true') {
			getFieldByID("chkCOs").checked = true;
		} else {
			getFieldByID("chkCOs").checked = false;
			Disable('chkCOs', true);
		}
		if (strSearch[3] != null && strSearch[3] != "") {
			setSelectedDataWithGroup(strSearch[3],ls);
		}
		if(strSearch[4] != null && strSearch[4] != ""){
			setField("txtBookingFromDate",strSearch[4]);
		}
		if(strSearch[5] != null && strSearch[5] != ""){
			setField("txtBookingToDate",strSearch[5]);
		}
	} else {
		Disable('chkTAs', true);
		Disable('chkCOs', true);
	}
}


function clickAgencies() {
	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
		Disable('chkCOs', false);
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
		Disable('chkCOs', true);
		getFieldByID("chkCOs").checked = false;
	}
}


function changeAgencies() {
	ls.clear();
}


function getAgentClick() {
	if (getValue("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
	} else {
		ls.clear();
		var strSearchCriteria = getValue("selAgencies") + "#"
				+ getFieldByID("chkTAs").checked + "#"
				+ getFieldByID("chkCOs").checked + "#"
				+ ls.getSelectedDataWithGroup() + "#"
				+ getValue('txtBookingFromDate') + "#"
				+ getValue('txtBookingToDate') + "#";
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		setField("hdnAgents", "");
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmBookingCountDetailsReport");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}


function getAgents() {
	var strAgents = ls.getSelectedDataWithGroup();
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents;
	}
	return newAgents;
}


function setSelectedDataWithGroup(strData,listBoxID) {
	var str = "";

	var groups = strData.split("|");
	var groupList = new Array();

	for ( var x = 0; x < groups.length; x++) {

		if (groups[x].indexOf(":") != -1) {
			var dd = groups[x].split(":");
			groupList = dd[1].split(",");

			for ( var z = 0; z < groupList.length; z++) {
				if (str == "") {
					str += dd[0] + ":" + groupList[z];
				} else {
					str += "," + dd[0] + ":" + groupList[z];
				}
			}
		} else {
			if (str == "") {
				str += groups[x];
			} else {
				str += "," + groups[x];
			}
		}
	}
	listBoxID.move('>', str);
}


function removeSelectedDataWithGroup(strData,listBoxID) {
	var str = "";

	var groups = strData.split("|");
	var groupList = new Array();

	for ( var x = 0; x < groups.length; x++) {

		if (groups[x].indexOf(":") != -1) {
			var dd = groups[x].split(":");
			groupList = dd[1].split(",");

			for ( var z = 0; z < groupList.length; z++) {
				if (str == "") {
					str += dd[0] + ":" + groupList[z];
				} else {
					str += "," + dd[0] + ":" + groupList[z];
				}
			}
		} else {
			if (str == "") {
				str += groups[x];
			} else {
				str += "," + groups[x];
			}
		}
	}
	listBoxID.move('<', str);
}


function viewClick() {
	
	if (getFieldByID("txtBookingFromDate").value == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtBookingFromDate").focus();
	}else if(getFieldByID("txtBookingToDate").value == ""){
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtBookingToDate").focus();
	}else{
		setField("hdnMode", "VIEW");
		setField("hdnAgents",getAgents());
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmBookingCountDetailsReport");
		objForm.target = "CWindow";
		objForm.action = "showBookingCountDetailsReport.action";
		objForm.submit();
		top[2].HidePageMessage();
	}
}