var objWindow;
var screenid ="UC_REPM_058";
var objForm = document.getElementById("frmTicketWiseForwardSalesReport");

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();
top[2].HideProgress();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
	
}

function closeClick() {
	if (top.loadCheck(top.pageEdited)) {
		setPageEdited(false);
		top.strSearchCriteria = "";
		top.LoadHome();
	}
}

function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}

function viewClick() {
	if(getValue("txtFromDate") == ""){
		getFieldByID("txtFromDate").focus();
	}else if(getValue("txtToDate") == ""){
		getFieldByID("txtToDate").focus();
	}else if (validateReportDate("txtFromDate", "txtToDate")){
	setField("hdnMode", "VIEW");
	objForm.action = "showTicketWiseForwardSalesReport.action";
	objForm.submit();
	}
}

function validateReportDate(strFromDate, strTodate){
	var tempDay;
	var tempMonth;
	var tempYear;
	var validate = false;
	
	var dateFrom = getText(strFromDate);
	var dateTo = getText(strTodate);

	tempIStartDate = dateFrom;
	tempIEndDate = dateTo;
			
	tempDay=tempIStartDate.substring(0,2);
	tempMonth=tempIStartDate.substring(3,5);
	tempYear=tempIStartDate.substring(6,10); 	
	var tempOStartDate=(tempYear+tempMonth+tempDay);		
	
	tempDay=tempIEndDate.substring(0,2);
	tempMonth=tempIEndDate.substring(3,5);
	tempYear=tempIEndDate.substring(6,10); 	
	var tempOEndDate=(tempYear+tempMonth+tempDay);	
	
	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10){dtCM = "0" + dtCM}
	if (dtCD < 10){dtCD = "0" + dtCD}
 
	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
	
	tempDay=strSysDate.substring(0,2);
	tempMonth=strSysDate.substring(3,5);
	tempYear=strSysDate.substring(6,10);
	
	strSysODate=(tempYear+tempMonth+tempDay); 
	
	if(tempOStartDate > tempOEndDate){
		showCommonError("Error","To Date Cannot Be Less Than From Date");
		getFieldByID(strTodate).focus();
	}
	else {
		validate = true;				
	}
	
	return validate;	
}

function winOnLoad() {
	getFieldByID("txtFromDate").focus();

	if (top.strSearchCriteria != "" && top.strSearchCriteria != null) {
		var strSearch = top.strSearchCriteria.split("#");
		setField("txtFromDate", strSearch[0]);
		setField("txtToDate", strSearch[1]);
	}
}