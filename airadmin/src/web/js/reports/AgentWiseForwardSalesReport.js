var objWindow;
var screenId ="UC_REPM_060";
var objForm = document.getElementById("frmAgentWiseForwardSalesReport");

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();
top[2].HideProgress();

function winOnLoad() {
	if (top[1].objTMenu.tabGetValue(screenId) != ""
		&& top[1].objTMenu.tabGetValue(screenId) != null) {
		var strSearch = top[1].objTMenu.tabGetValue(screenId).split("#");
		setField("txtFromDate", strSearch[0]);
		setField("txtToDate", strSearch[1]);
		setField("selStations",strSearch[2]);
		setField("selCountry",strSearch[3]);
		setField("selAgencies",strSearch[4]);
	}
}

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function validateReportDate(strFromDate, strTodate){
	var tempDay;
	var tempMonth;
	var tempYear;
	var validate = false;
	
	var dateFrom = getText(strFromDate);
	var dateTo = getText(strTodate);

	tempIStartDate = dateFrom;
	tempIEndDate = dateTo;
			
	tempDay=tempIStartDate.substring(0,2);
	tempMonth=tempIStartDate.substring(3,5);
	tempYear=tempIStartDate.substring(6,10); 	
	var tempOStartDate=(tempYear+tempMonth+tempDay);		
	
	tempDay=tempIEndDate.substring(0,2);
	tempMonth=tempIEndDate.substring(3,5);
	tempYear=tempIEndDate.substring(6,10); 	
	var tempOEndDate=(tempYear+tempMonth+tempDay);	
	
	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10){dtCM = "0" + dtCM}
	if (dtCD < 10){dtCD = "0" + dtCD}
 
	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
	
	tempDay=strSysDate.substring(0,2);
	tempMonth=strSysDate.substring(3,5);
	tempYear=strSysDate.substring(6,10);
	
	strSysODate=(tempYear+tempMonth+tempDay); 
	
	if(tempOStartDate > tempOEndDate){
		showCommonError("Error","To Date Cannot Be Less Than From Date");
		getFieldByID(strTodate).focus();
	}
	else {
		validate = true;				
	}
	
	return validate;	
}

function viewClick() {
	if (displayAgencyMode == 1 || displayAgencyMode == 2) {
		setField("hdnAgents",getAgents());
	} else {
		setField("hdnAgents","");
	}
	
	if(getValue("txtFromDate") == ""){
		showCommonError("Error", fromDtEmpty); 
		getFieldByID("txtFromDate").focus();
	}else if(getValue("txtToDate") == ""){
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	}else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	}else if ((validateReportDate("txtFromDate", "txtToDate")) && (displayAgencyMode == 1 || displayAgencyMode == 2)){
		setField("hdnMode", "VIEW");
		var agentIds = trim(ls.getselectedData());
		setField("hdnAgents",agentIds);	
		objForm.action = "showAgentWiseForwardSalesReport.action";
		objForm.submit();
	}
}

function getAgentClick() {
	if (getValue("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
	} else {
		var strSearchCriteria = getValue("txtFromDate") + "#" 
								+ getValue("txtToDate") + "#" 
								+ getValue("selStations") + "#" 
								+ getValue("selCountry") + "#" 
								+ getValue("selAgencies");
		strSearchCriteria += "#" + false;
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		setField("hdnAgents", "");
		setField("hdnMode", "SEARCH");
		objForm.target = "_self";
		objForm.submit();
		ShowProgress();
	}
}

function closeClick() {
	if (top.loadCheck(top.pageEdited)) {
		setPageEdited(false);
		top.strSearchCriteria = "";
		top.LoadHome();
	}
}

function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}

function getAgents() {
	var strAgents = ls.getSelectedDataWithGroup();
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents;
	}
	return newAgents;
}