var objWindow;
var value;
var screenId="UC_REPM_083";
setField("hdnLive",repLive);
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();


function winOnLoad() {
	getFieldByID("selOrigin").focus();
}


function viewClick() {	
	if(validate()){
		setField("hdnMode", "VIEW");
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmFlightConnectivityReport");
		objForm.target = "CWindow";
		objForm.action = "showFlightConnectivityReport.action";
		objForm.submit();
		top[2].HidePageMessage();
	}
}


function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function closeClick() {
	top[1].objTMenu.tabRemove(screenId);
}

function LoadCalendar(strID, objEvent){
	objCal1.ID = strID;
	objCal1.top = 150 ;
	objCal1.left = 0 ;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFlightDate", strDate);
		break;
	}
}

function validate(){
	var flightType = getValue("radFlightOption");
	
	if(trim(getValue("selOrigin")) == ''){
		showCommonError("Error",originRequired);
		getFieldByID("selOrigin").focus();
		return false;
	}
	if(trim(getValue("selDestination")) == ''){
		showCommonError("Error",destinationRequired);
		getFieldByID("selOrigin").focus();
		return false;
	}
	if(!dateValidDate(getValue("txtFlightDate"))){
		showCommonError("Error",dateInvalid);
		getFieldByID("txtFlightDate").focus();
		return false;
	}
	return true;
}