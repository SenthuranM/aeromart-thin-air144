var objWindow;
var value;
setField("hdnLive",repLive);
	
	var objCal1 = new Calendar("spnCalendarDG1");
	objCal1.onClick = "setDate";
	objCal1.blnDragCalendar = false;
	objCal1.buildCalendar();
	
function setDate(strDate, strID){
	switch (strID){
		case "0" : setField("txtFromDate",strDate);break ;
		case "1" : setField("txtToDate",strDate);break ;
	}
}

function beforeUnload(){
	if ((top[0].objWindow) && (!top[0].objWindow.closed))	{
		top[0].objWindow.close();
	}
}

function LoadCalendar(strID, objEvent){

	objCal1.ID = strID;
	objCal1.top = 1 ;
	objCal1.left = 0 ;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);	
	
}
	var screenId="UC_REPM_084";
	function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))){
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function setPageEdited(isEdited){
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function viewClick(){	
	if(getText("txtFromDate")==""){
		showCommonError("Error",fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	}else if(getText("txtToDate")==""){
		showCommonError("Error",toDtEmpty);
		getFieldByID("txtToDate").focus();
	} /*else if(getText("selAgencies")=="" && ls.getselectedData().length == 0){
		showCommonError("Error",agencyNull);
		getFieldByID("selAgencies").focus();
	} */ 
	else if(getText("txtAmount") == ""){
		showCommonError("Error",salesAmountRequired);
		getFieldByID("txtAmount").focus();
	}
	else if(isNaN(trim(getText("txtAmount"))) || trim(getText("txtAmount")) < 0){
		showCommonError("Error",salesAmountInvalid);
		getFieldByID("txtAmount").focus();
	}else if( validateReportDate("txtFromDate", "txtToDate", repStartDate)==true) {
			
		setField("hdnMode","VIEW");	
		//var agentIds = trim(ls.getselectedData());
		//setField("hdnAgents",agentIds);
		
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'	
		top[0].objWindow = window.open("about:blank","CWindow",strProp);			
		var objForm  = document.getElementById("frmAgentSalesPerformanceReport");
			objForm.target = "CWindow";
	
		objForm.action = "showAgentSalesPerformanceReport.action";
		objForm.submit();
		top[2].HidePageMessage();
	
	}
}

	function pageLoad(){		
		Disable("selAgent",true);		
	}
	
	function winOnLoad() {		
		if(top[1].objTMenu.tabGetValue("UC_REPM_049") != "" && top[1].objTMenu.tabGetValue("UC_REPM_049") != null)	{		
			var strSearch=top[1].objTMenu.tabGetValue("UC_REPM_049").split("#");
						
			Disable("txtFromDate", false);
			setField("txtFromDate",strSearch[0]);
			Disable("txtToDate", false);
			setField("txtToDate",strSearch[1]);
			//setField("selAgencies",strSearch[2]);					
		} 
	}
	
	function getGetUser(){
		
	}
	
	function getAgentClick(){
		if (trim(getValue("selAgencies")) == '') {
			showCommonError("Error",agentTypeRqrd);
			getFieldByID("selAgencies").focus();
			return;
		}
		var period;
				
		
		var strSearchCriteria = getValue("txtFromDate")+"#"+getValue("txtToDate")+"#"+getValue("selAgencies")+"#"+period;
		top[1].objTMenu.tabSetValue("UC_REPM_049",strSearchCriteria);
		
		setField("hdnMode","SEARCH");
		document.forms[0].target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
	
	function chkClick(){
		Disable("selAgent",false);
	}
	
	function chkClickPeriod(){
		if(getFieldByID("radReportPeriodO").checked == true){
			Disable("txtFromDate", false);
			Disable("txtToDate", false);
			//setField("selAgencies", "");
			getFieldByID("txtFromDate").focus();
			ls.removeAllFromListbox();
		} else {
			Disable("txtFromDate", true);
			Disable("txtToDate", true);
			setField("txtFromDate", "");
			setField("txtToDate", "");
			setField("selAgencies", "");
			ls.removeAllFromListbox();
		}
	}
	 function AgentOnChange(){	
	 	ls.removeAllFromListbox();
	 	ls.clear();
	 }
