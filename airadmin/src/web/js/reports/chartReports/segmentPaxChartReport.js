var objWindow;
var screenID = "UC_REPM_064";

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {

	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function winOnLoad() {
	 hideChartImg();
	if (top[1].objTMenu.tabGetValue(screenID) != ""
			&& top[1].objTMenu.tabGetValue(screenID) != null) {
		var strSearch = top[1].objTMenu.tabGetValue(screenID).split("#");
		setField("txtFromDate", strSearch[0]);
		setField("txtToDate", strSearch[1]);

	} 
}


function viewClick() {
	top[2].HidePageMessage();
	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true && checkGreat()) {

		viewClickBar();
	}

}

function viewClickBar(){
	hideChartImg();
	var img = getFieldByID("chartImg1");	
	var hdnMode = "VIEW";
	var hdnFromDate = getText("txtFromDate");	
	var hdnToDate = getText("txtToDate");	
	img.src = "";
	img.src = "showSegmentPaxChartReport.action?hdnMode=VIEW&hdnFromDate="+hdnFromDate+"&hdnToDate="+
		hdnToDate+"&hdnDate="+new Date();	
	showChartImg();
}

function hideChartImg(){
	getFieldByID("chartImg1").style.display = 'none';
}
function showChartImg(){
	getFieldByID("chartImg1").style.display = 'block';
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function dataChanged() {
	objOnFocus();
	setPageEdited(true);
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenID, isEdited);

}

function checkGreat() {

	var validate = false;
	if (!CheckDates(getText("txtFromDate"), getText("txtToDate"))) {
		validate = false;
		showCommonError("Error", fromDtExceed);
		getFieldByID("txtToDate").focus();
//	} else if (!CheckDates(repStartDate, getText("txtFromDate"))) {
//		showCommonError("Error", rptReriodExceeds + " " + repStartDate);
//		getFieldByID("txtFromDate").focus();
//	} 
	}else {
		validate = true;
	}
	return validate;
}

function settingValidation(cntfield) {

	if (!dateChk(cntfield)) {
		showERRMessage(invaliddate);
		getFieldByID(cntfield).focus();
		return;
	}
}


function chkClick() {
	dataChanged();
}

function closeClick() {
	setPageEdited(false);
	objOnFocus();
	top[1].objTMenu.tabRemove(screenID)
}

function ckheckPageEdited() {

	if (top[1].objTMenu.tabGetPageEidted(screenID))
		return confirm("Changes will be lost! Do you wish to Continue?");
	else
		return true;
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}