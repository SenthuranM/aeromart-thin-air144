var objWindow;
var value;
var screenId = "UC_REPM_084";
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtSalesFromDate", strDate);
		break;
	case "1":
		setField("txtSalesToDate", strDate);
		break;
	case "2":
		setField("txtFlightFromDate", strDate);
		break;
	case "3":
		setField("txtFlightToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 150;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function winOnLoad() {
	getFieldByID("txtSalesFromDate").focus();

}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function viewClick() {
	top[2].HidePageMessage();
	setField("hdnAgents", getAgents());
	setField("hdnStations", trim(lsstn.getselectedData()));
	setField("hdnAncillaries", trim(lsanci.getselectedData()));
	setField("hdnSalesChannels", trim(lspm1.getselectedData()));
	
	if (validateFields()) {
		setField("hdnMode", "VIEW");
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmAncillaryRevenueReport");
		objForm.target = "CWindow";
		objForm.action = "showAdvancedAncillaryRevenueReport.action?hdnLive=OFFLINE";
		objForm.submit();
	}
}

function validateFields() {
	var currDt = new Date();
	var dtfromDate = null;
	var dttoDate = null;
	var dtFltFromDate = null;
	var dtFltToDate = null;
	var strFrmDate = getText("txtSalesFromDate");
	var strToDate = getText("txtSalesToDate");
	var strFltFrmDate = getText("txtFlightFromDate");
	var strFltToDate = 	getText("txtFlightToDate");
	
	if (dtfromDate != "") {
		dtfromDate = new Date(strFrmDate.substr(6, 4), (parseFloat(strFrmDate
				.substr(3, 2)) - 1), strFrmDate.substr(0, 2));
	}
	if (strToDate != "") {
		dttoDate = new Date(strToDate.substr(6, 4), (parseFloat(strToDate
				.substr(3, 2)) - 1), strToDate.substr(0, 2));
	}
	if(strFltFrmDate != ""){
		dtFltFromDate = new Date(strFltFrmDate.substr(6, 4), (parseFloat(strFltFrmDate
				.substr(3, 2)) - 1), strFltFrmDate.substr(0, 2));
	}
	if(strFltToDate != ""){
		dtFltToDate = new Date(strFltToDate.substr(6, 4), (parseFloat(strFltToDate
				.substr(3, 2)) - 1), strFltToDate.substr(0, 2));	
	}
	
	if(getText("txtSalesFromDate") == "" && getText("txtSalesToDate") == "" && getText("txtFlightFromDate") == "" && getText("txtFlightToDate") == ""){
		showCommonError("Error", "Either Sales dates or Flight dates are mandatory");
		etFieldByID("txtSalesFromDate").focus();
		return false;
	}
	
	if(getText("txtSalesFromDate") != "" || getText("txtSalesToDate") != ""){
		if (getText("txtSalesFromDate") == "") {
			showCommonError("Error", "Sales from date cannot be empty");
			getFieldByID("txtSalesFromDate").focus();
			return false;
		} else if (dateValidDate(getText("txtSalesFromDate")) == false) {
			showCommonError("Error", "Sales from date is invalid");
			getFieldByID("txtSalesFromDate").focus();
			return false;
		} else if (currDt < dtfromDate) {
			showCommonError("Error", "Saled from date cannot exceed current date");
			getFieldByID("txtSalesFromDate").focus();
			return false;
		} else if (getText("txtSalesToDate") == "") {
			showCommonError("Error", "Sales to date cannot be empty");
			getFieldByID("txtSalesToDate").focus();
			return false;
		} else if (dateValidDate(getText("txtSalesToDate")) == false) {
			showCommonError("Error", "Sales to date is invalid");
			getFieldByID("txtSalesToDate").focus();
			return false;
		} else if (currDt < dttoDate) {
			showCommonError("Error", "Saled to date cannot exceed current date");
			getFieldByID("txtSalesToDate").focus();
			return false;
		} else if (dtfromDate > dttoDate) {
			showCommonError("Error", "Sales to date cannot exceed Sales from date");
			getFieldByID("txtSalesToDate").focus();
			return false;
		} 
	}
	
	if(getText("txtFlightFromDate") != "" || getText("txtFlightToDate") != ""){
		if(getText("txtFlightFromDate") == ""){
			showCommonError("Error", "Flight from date cannot be empty");
			getFieldByID("txtFlightFromDate").focus();
			return false;
		}else if(dateValidDate(getText("txtFlightFromDate")) == false){
			showCommonError("Error", "Flight from date is invalid ");
			getFieldByID("txtFlightFromDate").focus();
			return false;
		}else if (currDt < dtFltFromDate) {
			showCommonError("Error", "Flight from date cannot exceed current date");
			getFieldByID("txtFlightFromDate").focus();
			return false;
		} else if (getText("txtFlightToDate") == "") {
			showCommonError("Error", "Flight to date cannot be empty");
			getFieldByID("txtFlightToDate").focus();
			return false;
		}else if ((dateValidDate(getText("txtFlightToDate")) == false)){
			showCommonError("Error", "Flight to date is invalid");
			getFieldByID("txtFlightToDate").focus();
			return false;
		}else if (currDt < dtFltToDate) {
			showCommonError("Error", "Flight to date cannot exceed current date ");
			getFieldByID("txtFlightToDate").focus();
			return false;
		} else if (dtFltFromDate > dtFltToDate) {
			showCommonError("Error", "Flight from date cannot exceed flight to date ");
			getFieldByID("txtFlightToDate").focus();
			return false;
		}
	}
		
	 if (getText("hdnSalesChannels") == "") {
		showCommonError("Error", "Sales channels required ");
		return false;
	} else if (getText("hdnAncillaries") == "") {
		showCommonError("Error", "Ancillaries required ");
		return false;	
	} else if (!isWebChannel() && getText("hdnAgents") == "") {
		showCommonError("Error", agentsRqrd);
		return false;
	} else if (!isWebChannel() && getText("hdnStations") == "") {
		showCommonError("Error", "Point of sales required ");
		return false;
	}else if (isWebChannel() && getText("hdnStations").length > 0 ){
		showCommonError("Error", "Cannot contain point of sales for web bookings");
		return false;
	}
	 
	return true;
}


function isWebChannel(){
	var selectedChannels= getText("hdnSalesChannels");
	if(selectedChannels == "4"){
		return true;
	}
	return false;
}


function closeClick() {
	top[1].objTMenu.tabRemove(screenId);
}

function getAgents() {
	var strAgents = ls.getSelectedDataWithGroup();
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents;
	}
	return newAgents;
}