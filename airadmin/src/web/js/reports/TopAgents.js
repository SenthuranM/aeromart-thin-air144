var objWindow;
var value;
setField("hdnLive", repLive);

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}
var screenId = "UC_REPM_012";
function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function KPValidatePositiveInteger(control) {
	var strCC = getText(control);
	var strLen = strCC.length;
	var blnVal = isPositiveInt(strCC)
	if (!blnVal) {
		setField(control, strCC.substr(0, strLen - 1));
		getFieldByID(control).focus();
	}

}

function scheduleClick() {
	setField("hdnMode","SCHEDULE");	
	viewReport(true);
}

function viewClick() {
	setField("hdnMode","VIEW");	
	viewReport(false);
}

function scheduleClick() {
	setField("hdnMode","SCHEDULE");	
	viewReport(true);
}

function viewReport(isSchedule) {
	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();
	} else if (getText("selFrom") == "" && getText("selTo") != "") {
		showCommonError("Error", stationFromRqrd);
		getFieldByID("selFrom").focus();
	} else if (getText("selTo") == "" && getText("selFrom") != "") {
		showCommonError("Error", stationToRqrd);
		getFieldByID("selTo").focus();
	} else if (getFieldByID("radNoTopAgentsO").checked == true
			&& getText("txtTopAgentsOther") == "") {
		showCommonError("Error", noOfAgentsRqrd);
		getFieldByID("selTo").focus();
	} else if(!validDateRange(getText("txtFromDate"), getText("txtToDate"), 184) // 184 is used because report to be generated for 6 months 
			&& getText("selTo") == "" && getText("selFrom") == ""){
		showCommonError("Error", daysLessThan31);
		getFieldByID("selTo").focus();
	} else if (dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true
			&& validateReportDate("txtFromDate", "txtToDate", repStartDate)) {
			if(isSchedule){
				scheduleReport();
			} else {
				var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
				top[0].objWindow = window.open("about:blank", "CWindow", strProp);
				var objForm = document.getElementById("frmTopAgents");
				objForm.target = "CWindow";
		
				objForm.action = "showTopAgentsReport.action";
				objForm.submit();
				top[2].HidePageMessage();
			}

	}
}

function scheduleReport(){
	UI_SchedRept.displayForm({divName:'divSchedFrom', formName:'frmTopAgents', 
		composerName: 'topAgentsReport',
		errorCallback:function(message){
			showERRMessage(message);
		},
		successCallback:function(message){
			showCommonError("Confirmation", message);
		}
	});
}

function validDateRange(fromDate, toDate, dateRange){
	var fromDay=fromDate.substring(0,2);
	var fromMonth=fromDate.substring(3,5);
	var fromYear=fromDate.substring(6,10);
	
	var toDay=toDate.substring(0,2);
	var toMonth=toDate.substring(3,5);
	var toYear=toDate.substring(6,10);
	
	var fDate = new Date(fromYear, fromMonth-1, fromDay, 0, 0, 0);
	var tDate = new Date(toYear, toMonth-1, toDay, 23, 59, 59);
	var timeDiff = Math.abs(tDate.getTime() - fDate.getTime());
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
	if(diffDays <= dateRange){
		return true;
	} 
	return false;
}

function pageLoadTA() {
	getFieldByID("txtFromDate").focus();
	Disable("txtTopAgentsOther", true);
}

function checkTA() {
	if (getFieldByID("radNoTopAgentsO").checked == true) {
		Disable("txtTopAgentsOther", false);
	} else {
		setField("txtTopAgentsOther", "");
		Disable("txtTopAgentsOther", true);
	}
}