var objWindow;
var value;
setField("hdnLive", repLive);

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

var screenId="UC_REPM_075";
function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}


function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function viewReportExtPmtReconcil() {


	if (getText("txtFromDate") == "") {

		showCommonError("Error", fromDtEmpty);

		getFieldByID("txtFromDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == false) {

		showCommonError("Error", fromDtInvalid);

		getFieldByID("txtFromDate").focus();
	} else if (getText("txtToDate") == "") {

		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {

		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();
	}/* else if (getText("txtFlightNumber") != "") {
		var blnIsAlpNum = isAlphaNumeric(trim(getText("txtFlightNumber")));
		
		if(blnIsAlpNum != true){
			showCommonError("Error", flightNoInvalid );	
			getFieldByID("txtFlightNumber").focus();
			return false;
		}
		//showCommonError("Error", flightNoEmpty);
		
	}*/ else if (dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true
			&& validateReportDate("txtFromDate", "txtToDate", repStartDate)) {
		
		if (getText("txtAgentCode") != ""){
			if(isAlphaNumeric(trim(getText("txtAgentCode"))) != true){
				showCommonError("Error", flightNoInvalid );	
				getFieldByID("txtAgentCode").focus();
				return false;
			}
		}
		
		setField("hdnMode", "VIEW");
		setField("hdnRptType", "Summary");
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmExtPmtReconcil");
		objForm.target = "CWindow";
		objForm.action = "showAgentExtPmtReconcilReport.action";
		objForm.submit();
		top[2].HidePageMessage();

	}
}

function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function pageLoadTS() {
	getFieldByID("txtFromDate").focus();
}


function hasWhiteSpace(str){
	if(trim(str).search(/ /) == -1){
		return false;
	}else{
		return true;
	}
}

var r={
  'special':/[^0-9a-zA-Z.]+$/  
}

function valid(o,w){
  o.value = o.value.replace(r[w],'');
}
