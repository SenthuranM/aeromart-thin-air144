var objWindow;
setField("hdnLive", repLive);
function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

var screenId = "UC_REPM_009";

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtCreatedDate", strDate);
		break;
	case "1":
		setField("txtCreatedDateTo", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 350;
	objCal1.left = 400;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function getAgents() {
	var strAgents = ls.getSelectedDataWithGroup();
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents;
	}
	return newAgents;
}

function winOnLoad() {
	getFieldByID("selAgencies").focus();
	if (top[1].objTMenu.tabGetValue(screenId) != ""
			&& top[1].objTMenu.tabGetValue(screenId) != null) {
		var strSearch = top[1].objTMenu.tabGetValue(screenId).split("#");
		setField("selAgencies", strSearch[0]);
		setField("selStation", strSearch[2]);
		setField("selTerritory", strSearch[3]);
		setField("selCountry", strSearch[3]);
		if (strSearch[1] == 'true') {
			getFieldByID("chkTAs").checked = true;
		} else {
			getFieldByID("chkTAs").checked = false;
			Disable('chkTAs', true);
		}
		if (strSearch[5] == 'true') {
			getFieldByID("chkCOs").checked = true;
		} else {
			getFieldByID("chkCOs").checked = false;
			Disable('chkCOs', true);
		}

		if (getText("selAgencies").toUpperCase().indexOf("GSA") != -1) {
			Disable("selTerritory", "");
		} else {
			Disable("selTerritory", true);
		}

	} else {
		Disable('chkTAs', true);
		Disable('chkCOs', true);
	}
}

function clickAgencies() {
	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
		Disable('chkCOs', false);
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
		Disable('chkCOs', true);
		getFieldByID("chkCOs").checked = false;
	}
}

function changeAgencies() {
	ls.clear();
	if (getText("selAgencies").toUpperCase().indexOf("GSA") != -1) {
		Disable("selTerritory", "");
	} else {
		Disable("selTerritory", true);
	}
}

function viewClick() {
	if (displayAgencyMode == 1 || displayAgencyMode == 2) {
		setField("hdnAgents",getAgents());
	} else {
		setField("hdnAgents","");
	}

	if (trim(getText("txtCreatedDate")) != ""
			&& dateValidDate(getText("txtCreatedDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtCreatedDate").focus();
		return false;

	} else if (trim(getText("txtCreatedDateTo")) != ""
			&& dateValidDate(getText("txtCreatedDateTo")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtCreatedDate").focus();
		return false;

	} else if (trim(getText("txtCreatedDateTo")) != ""
			&& dateValidDate(getText("txtCreatedDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtCreatedDateTo").focus();
		return false;

	} else if (trim(getText("txtCreatedDate")) != ""
			&& dateValidDate(getText("txtCreatedDateTo")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtCreatedDateTo").focus();
		return false;
		
	} else if((trim(getText("txtCreatedDate")) != "" && trim(getText("txtCreatedDateTo")) != "") && (!CheckDates(trim(getText("txtCreatedDate")),trim(getText("txtCreatedDateTo"))))){
		getFieldByID("txtCreatedDateTo").focus();
		showERRMessage(fromDtExceed);
		return false;
		
	} else if (displayAgencyMode == 1  && getText("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();

	} else if((displayAgencyMode == 1 || displayAgencyMode == 2) && getText("hdnAgents")==""){
		showERRMessage(arrError["agentsRqrd"]);
		
	} else {
		setField("hdnMode", "VIEW");
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10';
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmAgentGsaDetails");
		objForm.target = "CWindow";
		objForm.action = "showAgentGSADetails.action";
		objForm.submit();
		top[2].HidePageMessage();
	}

}

function getAgentClick() {
	if (getValue("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
	} else {
		ls.removeAllFromListbox();
		var strSearchCriteria = getValue("selAgencies") + "#"
				+ getFieldByID("chkTAs").checked + "#"
				+ getFieldByID("selStation") + "#"
				+ getFieldByID("selTerritory") + "#"
				+ getFieldByID("selCountry") + "#"
				+ getFieldByID("chkCOs").checked;
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		setField("hdnAgents", "");
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmAgentGsaDetails");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}

function settingValidation(cntfield) {
	if (!dateChk(cntfield)) {
		showERRMessage(invaliddate);
		getFieldByID(cntfield).focus();
		return;
	}
}