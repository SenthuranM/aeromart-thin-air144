var objWindow;
var screenID = "UC_REPM_050";

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();
setField("hdnLive", repLive);

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {

	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function winOnLoad() {

	if (top[1].objTMenu.tabGetValue(screenID) != ""
			&& top[1].objTMenu.tabGetValue(screenID) != null) {
		var strSearch = top[1].objTMenu.tabGetValue(screenID).split("#");
		setField("txtFromDate", strSearch[0]);
		setField("txtToDate", strSearch[1]);
		setField("selTaskGroup", strSearch[2]);

		if (getFieldByID("selTaskGroup").value == "") {
			Disable('selTask', false);
		} else {
			setField("selTask", strSearch[3]);
		}
		setField("selUserId", strSearch[4]);
		setField("selAgencies", strSearch[5]);
		if (strSearch[6] == 'true') {
			getFieldByID("chkTAs").checked = true;
		} else {
			getFieldByID("chkTAs").checked = false;
			Disable('chkTAs', true);
		}
		if (strSearch[7] == 'true') {
			getFieldByID("chkCOs").checked = true;
		} else {
			getFieldByID("chkCOs").checked = false;
			Disable('chkCOs', true);
		}
		setField("txtSearch", strSearch[8]);

	} else {
		Disable('chkTAs', true);
		Disable('chkCOs', true);
		Disable('selTask', true);

	}
}

function clickAgencies() {

	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
		Disable('chkCOs', false);
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
		Disable('chkCOs', true);
		getFieldByID("chkCOs").checked = false;
	}
}

function clickTaskGroup() {
	if (getFieldByID("selTaskGroup").value == "") {
		Disable('selTask', true);
	} else {
		Disable('selTask', false);
	}
}

function changeTaskGroup() {

	if (getFieldByID("selTaskGroup").value == "") {
		setText("selTask", "");
		Disable('selTask', true);
	} else {
		Disable('selTask', false);
		setText("selTask", "");
		setField("hdnAgents", "");
		var strSearchCriteria = getValue("txtFromDate") + "#"
				+ getValue("txtToDate") + "#" + getValue("selTaskGroup") + "#"
				+ getValue("selTask") + "#" + getValue("selUserId") + "#"
				+ getValue("selAgencies") + "#"
				+ getFieldByID("chkTAs").checked + "#"
				+ getFieldByID("chkCOs").checked + "#" + getValue("txtSearch");
		top[1].objTMenu.tabSetValue(screenID, strSearchCriteria);
		setField("hdnMode", "SEARCH");
		document.getElementById("frmAdminAudit").target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();

	}
}

function changeAgencies() {
	ls.clear();
}

function setGSA() {

	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
	}
}

function viewClick() {

	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();
	} else if (getFieldByID("selTaskGroup").value == "") {
		showCommonError("Error", taskGroupRqrd);
		getFieldByID("selTaskGroup").focus();
	} else if (dateValidDate(getText("txtFromDate")) == true
			&& dateValidDate(getText("txtToDate")) == true && checkGreat()) {

		setField("hdnMode", "VIEW");
		setField("hdnAgents", getAgents());
		var strProp = 'toolbar=no,location=no,status=no,menubar=Yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmAdminAudit");
		objForm.target = "CWindow";
		objForm.action = "showAuditReport.action";
		objForm.submit();
		top[2].HidePageMessage();
	}

}

function getAgentClick() {

	if (trim(getValue("selAgencies")) == '') {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
		return;
	}
	setField("hdnAgents", "");
	var strSearchCriteria = getValue("txtFromDate") + "#"
			+ getValue("txtToDate") + "#" + getValue("selTaskGroup") + "#"
			+ getValue("selTask") + "#" + getValue("selUserId") + "#"
			+ getValue("selAgencies") + "#" + getFieldByID("chkTAs").checked
			+ "#" + getFieldByID("chkCOs").checked + "#"
			+ getValue("txtSearch");
	top[1].objTMenu.tabSetValue(screenID, strSearchCriteria);
	setField("hdnMode", "SEARCH");
	document.getElementById("frmAdminAudit").target = "_self";
	document.forms[0].submit();
	top[2].ShowProgress();
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function dataChanged() {
	objOnFocus();
	setPageEdited(true);
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenID, isEdited);

}

function checkGreat() {

	var validate = false;
	if (!CheckDates(getText("txtFromDate"), getText("txtToDate"))) {
		validate = false;
		showCommonError("Error", fromDtExceed);
		getFieldByID("txtToDate").focus();
	} else if (!CheckDates(repStartDate, getText("txtFromDate"))) {
		showCommonError("Error", rptReriodExceeds + " " + repStartDate);
		getFieldByID("txtFromDate").focus();
	} else {
		validate = true;
	}
	return validate;
}

function settingValidation(cntfield) {

	if (!dateChk(cntfield)) {
		showERRMessage(invaliddate);
		getFieldByID(cntfield).focus();
		return;
	}
}

function getAgents() {

	var strAgents = ls.getSelectedDataWithGroup();
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents;
	}

	return newAgents;
}

function chkClick() {
	dataChanged();
}

function closeClick() {
	setPageEdited(false);
	objOnFocus();
	top[1].objTMenu.tabRemove(screenID)
}

function ckheckPageEdited() {

	if (top[1].objTMenu.tabGetPageEidted(screenID))
		return confirm("Changes will be lost! Do you wish to Continue?");
	else
		return true;
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}