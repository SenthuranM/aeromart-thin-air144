var objWindow;
setField("hdnLive", repLive);

var screenId = "UC_REPM_075";
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function LoadCalendarTo(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}


function radioOptionChanged() {
	if (getText("radAgencey") == "Agencey") {
		Disable("selAgencies", "true");
		Disable("btnGetAgent", "true");
	} else {
		Disable("selAgencies", "");
		Disable("btnGetAgent", "");
	}
}

function winOnLoad() {
	getFieldByID("txtFromDate").focus();	
	if (top[1].objTMenu.tabGetValue(screenId) != ""
			&& top[1].objTMenu.tabGetValue(screenId) != null) {
		var strSearch = top[1].objTMenu.tabGetValue(screenId).split("#");
		setField("txtFromDate", strSearch[0]);
		setField("txtToDate", strSearch[1]);
		setField("selAgencies", strSearch[2]);
		//lspm.selectedData(strSearch[5]);
		lspm1.selectedData(strSearch[5]);
		stls.selectedData(strSearch[6]);
		lscc.selectedData(strSearch[7]);
		if (strSearch[3] == 'true') {
			getFieldByID("chkTAs").checked = true;
		} else {
			getFieldByID("chkTAs").checked = false;
			Disable('chkTAs', true);
		}
		if (strSearch[4] == 'true') {
			getFieldByID("chkCOs").checked = true;
		} else {
			getFieldByID("chkCOs").checked = false;
			Disable('chkCOs', true);
		}		
	} else {
		Disable('chkTAs', true);
		Disable('chkCOs', true);
	}
}

function changeAgencies() {
	ls.clear();
}

function clickAgencies() {
	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
		Disable('chkCOs', false);
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
		Disable('chkCOs', true);
		getFieldByID("chkCOs").checked = false;
	}
}

function getAgents() {
	var strAgents;
	if (getText("selAgencies") != "All") {
		strAgents = ls.getSelectedDataWithGroup();
	} else {
		strAgents = ls.getselectedData();
	}
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents;
	}
	return newAgents;
}

function viewClick() {
	var tempDay;
	var tempMonth;
	var tempYear;
	var validate = false;

	var dateFrom = getText("txtFromDate");
	var dateTo = getText("txtToDate");

	tempIStartDate = dateFrom;
	tempIEndDate = dateTo;

	tempDay = tempIStartDate.substring(0, 2);
	tempMonth = tempIStartDate.substring(3, 5);
	tempYear = tempIStartDate.substring(6, 10);
	var tempOStartDate = (tempYear + tempMonth + tempDay);

	tempDay = tempIEndDate.substring(0, 2);
	tempMonth = tempIEndDate.substring(3, 5);
	tempYear = tempIEndDate.substring(6, 10);
	var tempOEndDate = (tempYear + tempMonth + tempDay);

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10) {
		dtCM = "0" + dtCM
	}
	if (dtCD < 10) {
		dtCD = "0" + dtCD
	}

	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear();

	tempDay = strSysDate.substring(0, 2);
	tempMonth = strSysDate.substring(3, 5);
	tempYear = strSysDate.substring(6, 10);

	strSysODate = (tempYear + tempMonth + tempDay);

	if (getText("txtFromDate") == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtFromDate").focus();
	} else if (dateValidDate(getText("txtFromDate")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtFromDate").focus();
	} else if (getText("txtToDate") == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtToDate").focus();
	} else if (dateValidDate(getText("txtToDate")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtToDate").focus();
	} else if (tempOStartDate > tempOEndDate) {
		showCommonError("Error", fromDtExceed);
		getFieldByID("txtToDate").focus();
	} else if (trim(lscc.getselectedData()).length == 0) {
		showCommonError("Error", carrierCodeRqrd);
	} else {

		setField("hdnMode", "VIEW");
		if(displayAgencyMode == 0){
			setField("hdnAgents", currentAgentCode);
		} else {
			setField("hdnAgents", trim(ls.getselectedData()));
		}
		setField("hdnPayments", "");
		setField("hdnSalesChannels", trim(lspm1.getselectedData()));
		setField("hdnStations", trim(stls.getselectedData()));
		setField("hdnCarrierCode", trim(lscc.getselectedData()));
		setField("hdnOnholdStatus", getFieldByID("chkOnhold").checked);

		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmBookedPAXSegment");
		objForm.target = "CWindow";
		objForm.action = "showBookedPAXSegmentReport.action";
		objForm.submit();
		top[2].HidePageMessage();

	}
}

function getAgentClick() {
	if (getValue("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
	} else {
		var strSearchCriteria = getValue("txtFromDate") + "#" + getValue("txtToDate") + "#" + getValue("selAgencies") + "#"
				+ getFieldByID("chkTAs").checked + "#" + getFieldByID("chkCOs").checked
				+ "#" + lspm1.getselectedData() 
				+ "#" + stls.getselectedData() + "#" + lscc.getselectedData();
		if (getFieldByID("chkBase")) {
			strSearchCriteria += "#" + getFieldByID("chkBase").checked
		}
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		setField("hdnAgents", "");
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmBookedPAXSegment");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}

function objOnFocus() {
	top[2].HidePageMessage();
}