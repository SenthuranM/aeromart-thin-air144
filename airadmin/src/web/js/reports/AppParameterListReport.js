var objWindow;
var value;
var screenId="UC_REPM_079";
setField("hdnLive", repLive);

function winOnLoad() {
	getFieldByID("selFields").focus();
	if (top[1].objTMenu.tabGetValue(screenId) != ""
		&& top[1].objTMenu.tabGetValue(screenId) != null) {
		var strSearch = top[1].objTMenu.tabGetValue(screenId).split("#");
		setField("selFields", strSearch[0]);
	}
}

function viewClick() {
	
	var selectedParamArray = getSelectedParams().split(",");
	
	if (selectedParamArray==null || selectedParamArray== ""|| selectedParamArray.length == 0) {
		showCommonError("Error",selParamEmpty);		
	} else {
		setField("hdnMode", "VIEW");
		setField("hdnParams", getSelectedParams());

		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
			top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmAppParametersReport");
		objForm.target = "CWindow";
		objForm.action = "showAppParameterReport.action";
		objForm.submit();
		top[2].HidePageMessage();
	}	
}

function getParamsClick() {
	if (getValue("selFields") == "") {
		showCommonError("Error",selDrpValue);
		getFieldByID("selFields").focus();
	} else {
		if (getText("selFields") == "Key") {
			setField("hdnParamsType", "Key");
		} else if (getText("selFields") == "Description") {
			setField("hdnParamsType", "Description");
		} else if(getText("selFields") == null) {
			setField("hdnParamsType", null);			
		}
		var strSearchCriteria = getValue("selFields");
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		var objForm = document.getElementById("frmAppParametersReport");
		setField("hdnMode", "SEARCH");
		objForm.target = "_self";
		objForm.submit();
		top[2].ShowProgress();
	}
}

function getSelectedParams() {
	var strSelectedList;
	if (getText("selFields") != null) {
		strSelectedList = ls.getselectedData();
	} 
	
	var newSelectedList;
	if (strSelectedList.indexOf(":") != -1) {
		newSelectedList = replaceall(strSelectedList, ":", ",");
	}
	if (strSelectedList.indexOf("|") != -1) {
		newSelectedList = replaceall(strSelectedList, "|", ",");
	} else {
		newSelectedList = strSelectedList;
	}
	return newSelectedList;
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function changeAgencies() {
	ls.clear();
}

function closeClick() {
	top[1].objTMenu.tabRemove(screenId);
}
