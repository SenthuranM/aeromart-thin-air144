var objWindow;
setField("hdnLive", repLive);

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

var screenId = "UC_REPM_025";

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtTransFrom", strDate);
		break;
	case "1":
		setField("txtTransTo", strDate);
		break;

	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 350;
	objCal1.left = 400;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}
function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function getAgents() {
	var strAgents = ls.getSelectedDataWithGroup();
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents;
	}
	return newAgents;
}

function winOnLoad() {
	getFieldByID("selAgencies").focus();
	if (top[1].objTMenu.tabGetValue(screenId) != ""
			&& top[1].objTMenu.tabGetValue(screenId) != null) {
		var strSearch = top[1].objTMenu.tabGetValue(screenId).split("#");
		setField("selAgencies", strSearch[0]);

		if (strSearch[1] == 'true') {
			getFieldByID("chkTAs").checked = true;
		} else {
			getFieldByID("chkTAs").checked = false;
			Disable('chkTAs', true);
		}
		if (strSearch[2] == 'true') {
			getFieldByID("chkCOs").checked = true;
		} else {
			getFieldByID("chkCOs").checked = false;
			Disable('chkCOs', true);
		}

		if (getText("selAgencies").toUpperCase().indexOf("GSA") != -1) {
			Disable("selTerritory", "");
		} else {

			Disable("selTerritory", true);
		}

	} else {
		Disable('chkTAs', true);
		Disable('chkCOs', true);
	}

	if (!gsaV2Enabled) {
		Disable('chkTypeAO', true);
	}
}

function clickAgencies() {
	if (getValue("selAgencies") == 'GSA') {
		Disable('chkTAs', false);
		Disable('chkCOs', false);
	} else {
		Disable('chkTAs', true);
		getFieldByID("chkTAs").checked = false;
		Disable('chkCOs', true);
		getFieldByID("chkCOs").checked = false;
	}
}

function changeAgencies() {
	ls.clear();
}

function viewClick() {

	setField("hdnRptType", getValue("selRepOption"));

	if (displayAgencyMode == 1 || displayAgencyMode == 2) {
		setField("hdnAgents", getAgents());
	} else {
		setField("hdnAgents", currentAgentCode);
	}

	if (displayAgencyMode == 1 && getText("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();

	} else if ((displayAgencyMode == 1 || displayAgencyMode == 2)
			&& ls.getSelectedDataWithGroup() == "") {
		showCommonError("Error", agentsRqrd);
		// getFieldByID("selAgencies").focus();
	} else if (trim(getText("txtTransFrom")) == "") {
		showCommonError("Error", fromDtEmpty);
		getFieldByID("txtTransFrom").focus();
		return false;

	} else if (dateValidDate(getText("txtTransFrom")) == false) {
		showCommonError("Error", fromDtInvalid);
		getFieldByID("txtTransFrom").focus();
		return false;

	} else if (trim(getText("txtTransTo")) == "") {
		showCommonError("Error", toDtEmpty);
		getFieldByID("txtTransTo").focus();
		return false;

	} else if (dateValidDate(getText("txtTransTo")) == false) {
		showCommonError("Error", toDtinvalid);
		getFieldByID("txtTransTo").focus();
		return false;
		// getFieldByID("selAgencies").focus();
	} else if (!CheckDates(repStartDate, getText("txtTransFrom"))) {
		showCommonError("Error", rptReriodExceeds + " " + repStartDate);
		getFieldByID("txtTransFrom").focus();
	} else if ((getValue("selRepOption") == "Summary")
			&& !validateSeven(getText("txtTransFrom"), getText("txtTransTo"))) {
		showCommonError("Error", sevenexceed);
		getFieldByID("txtTransTo").focus();
		return false;

	} else if ((trim(getText("txtTransFrom")) != "" && trim(getText("txtTransTo")) != "")
			&& (!CheckDates(trim(getText("txtTransFrom")),
					trim(getText("txtTransTo"))))) {
		getFieldByID("txtTransTo").focus();
		showERRMessage(fromDtExceed);
	} else {

		setField("hdnMode", "VIEW");
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmAgenTransDetails");
		objForm.target = "CWindow";
		objForm.action = "showAgentTransDetailsReport.action";
		objForm.submit();
		top[2].HidePageMessage();
	}

}

function getAgentClick() {
	if (getValue("selAgencies") == "") {
		showCommonError("Error", agentTypeRqrd);
		getFieldByID("selAgencies").focus();
	} else {
		ls.removeAllFromListbox();
		var strSearchCriteria = getValue("selAgencies") + "#"
				+ getFieldByID("chkTAs").checked + "#"
				+ getFieldByID("chkCOs").checked;
		top[1].objTMenu.tabSetValue(screenId, strSearchCriteria);
		setField("hdnAgents", "");
		setField("hdnMode", "SEARCH");
		var objForm = document.getElementById("frmAgenTransDetails");
		objForm.target = "_self";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}

function settingValidation(cntfield) {
	if (!dateChk(cntfield)) {
		showERRMessage(invaliddate);
		getFieldByID(cntfield).focus();
		return;
	}
}

function validateSeven(strFrom, strTo) {
	// If full search is allowed skip validation (AARESAA-4534)
	if (priviSearchFullAllowed == 'true') {
		return true;
	}
	var firstDate = new Date();
	var secondDate = new Date();

	var first = strFrom.split("/");
	var second = strTo.split("/");

	firstDate.setFullYear(first[2], first[1], first[0]);
	secondDate.setFullYear(second[2], second[1], second[0]);
	var firstTime = firstDate.getTime();
	var secondTime = secondDate.getTime();
	var noOfdays = (secondTime - firstTime) / (1000 * 60 * 60 * 24);
	if (noOfdays >= 7) {
		return false;
	}

	return true;
}

function changeOption() {
	if (getValue("selRepOption") == "AgentDetail") {
		// If full full is allowed skip validationa for 'R' Only (AARESAA-4534)
		if (priviSearchFullAllowed != 'true') {
			Disable("chkTypeR", "true");
		}
	}
	if (getValue("selRepOption") == "Summary") {
		Disable("chkTypeR", "");
	}

}