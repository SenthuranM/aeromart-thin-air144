var objWindow;
var value;
setField("hdnLive", repLive);

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

var screenId="UC_REPM_069";
function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}


function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}


	function validateDate(){	
		var tempDay;
		var tempMonth;
		var tempYear;
		var validate = false;
		
		var dateFrom = getText("txtFromDate");
		var dateTo = getText("txtToDate");
	
		tempIStartDate = dateFrom;
		tempIEndDate = dateTo;
				
		tempDay=tempIStartDate.substring(0,2);
		tempMonth=tempIStartDate.substring(3,5);
		tempYear=tempIStartDate.substring(6,10); 	
		var tempOStartDate=(tempYear+tempMonth+tempDay);		
		
		tempDay=tempIEndDate.substring(0,2);
		tempMonth=tempIEndDate.substring(3,5);
		tempYear=tempIEndDate.substring(6,10); 	
		var tempOEndDate=(tempYear+tempMonth+tempDay);			
	
		var dtC = new Date();
		var dtCM = dtC.getMonth() + 1;
		var dtCD = dtC.getDate();
		if (dtCM < 10){dtCM = "0" + dtCM;}
		if (dtCD < 10){dtCD = "0" + dtCD;}
	 
		var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
		
		tempDay=strSysDate.substring(0,2);
		tempMonth=strSysDate.substring(3,5);
		tempYear=strSysDate.substring(6,10);
		
		strSysODate=(tempYear+tempMonth+tempDay); 		
		
		if(tempOStartDate > tempOEndDate){
			showERRMessage(arrError["fromDtExceed"]);
			
			getFieldByID("txtToDate").focus();
		} else {
			validate = true;				
		}
		return validate;	
	}
	
	function viewClick(){
		
		if (getText("txtFromDate") == "") {

			showCommonError("Error", fromDtEmpty);

			getFieldByID("txtFromDate").focus();
		} else if (dateValidDate(getText("txtFromDate")) == false) {

			showCommonError("Error", fromDtInvalid);

			getFieldByID("txtFromDate").focus();
		} else if (getText("txtToDate") == "") {

			showCommonError("Error", toDtEmpty);
			getFieldByID("txtToDate").focus();
		} else if (dateValidDate(getText("txtToDate")) == false) {

			showCommonError("Error", toDtinvalid);
			getFieldByID("txtToDate").focus();
		}
		else if (dateValidDate(getText("txtFromDate")) == true
				&& dateValidDate(getText("txtToDate")) == true
				&& validateReportDate("txtFromDate", "txtToDate", repStartDate)) {
			
			setField("hdnMode","VIEW");	
			setField("hdnReportView","SUMMARY");
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10';
			top[0].objWindow = window.open("about:blank","CWindow",strProp);			
			var objForm  = document.getElementById("frmHalaServiceCommissionReport");
			objForm.target = "CWindow";				
			objForm.action = "showHalaCommissionReport.action";
			objForm.submit();
			top[2].HidePageMessage();
		
		}
	}
	
	/**
	 * Validates the from date & to date
	 * @return
	 */
	function checkDates(){
		var frmDate = getText("txtFromDate");
		var toDate = getText("txtToDate");
		var retVal = compareDate(frmDate,toDate);		
		if(retVal>0)
			return false;	
		else
			return true;
			
	}
	
	function setPageEdited(isEdited) {
		top.pageEdited = isEdited;
	}

	function objOnFocus() {
		top[2].HidePageMessage();
	}

	function pageLoadTS() {
		getFieldByID("txtFromDate").focus();
	}
	
	