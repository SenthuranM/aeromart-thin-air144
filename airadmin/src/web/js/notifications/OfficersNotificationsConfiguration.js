function UI_configMobNums() {} 


$(document).ready(function(){

	UI_configMobNums.constructConfigMobNumsGrid();
	
	top[2].HideProgress();

	$("#btnAddConfigMobNums").click(function() { 
		UI_configMobNums.addConfigOnClick();
	});
	
	$("#btnEditConfigMobNums").click(function() { 
		UI_configMobNums.editConfigOnClick();
	});
	
	$("#btnDeleteConfigMobNums").click(function() { 
		UI_configMobNums.deleteConfigOnClick();
	});
	
	$("#btnCloseConfigMobNums").click(function() { 
		UI_configMobNums.closeOnClick();
	});
	
	$("#btnResetConfigMobNums").click(function() { 
		UI_configMobNums.resetOnClick();
	});
	
	$("#btnSaveConfigMobNums").click(function() { 
		UI_configMobNums.saveConfigOnClick();
	});
	
});



UI_configMobNums.constructConfigMobNumsGrid = function(){	
	
	var mydata = null;
	disableControls(true);
	$("#btnAddConfigMobNums").removeAttr("disabled");
	$("#tblConfigMobNums").jqGrid({ 
		
		datatype: function(postdata) {			

			 postdata['officerMobNumsConfigsSearchDTO.officerName']= $('#contName').val();
			 postdata['officerMobNumsConfigsSearchDTO.mobNumber']= $('#mobNumber').val();

	        jQuery.ajax({
	           url: 'showOfficersNotificationsConfiguration.action',
	           data:postdata,
	           dataType:"json",
	           complete: function(jsonData,stat){	        	  
	              if(stat=="success") {
	            	 mydata = eval("("+jsonData.responseText+")");
	            	 $("#tblConfigMobNums")[0].addJSONData(mydata);            	
	              }
	           }
	        });
	    },
		jsonReader : {
			  root: "colOfficersMobileNumbersConfigs", 
			  page: "page",
			  total: "total",
			  records: "records",
			  repeatitems: false,				  
			  id: "0"				  
			},													
	    colNames:['&nbsp;','Officer Id','Officer Name','Mobile Number','Contatc Email','Version'],
        colModel:[
                {name:'id', width:5, jsonmap:'id' },
                {name:'OfficerMobNumsConfigsResultsTO.OfficerId',  index: 'OfficerId', width:6, align:"center"},
                {name:'OfficerMobNumsConfigsResultsTO.OfficerName',  index: 'OfficerName', width:13, align:"center"},
                {name:'OfficerMobNumsConfigsResultsTO.MobNumber',  index: 'MobNumber', width:18, align:"center"},
                {name:'OfficerMobNumsConfigsResultsTO.emailAddress',  index: 'OfficerContactEmail', width:13, align:"center"},
                {name:'OfficerMobNumsConfigsResultsTO.version',index: 'version', width:4, hidden:true , align:"center" },
                               	
    		],
		imgpath: '../../themes/default/images/jquery',
		multiselect:false,
		pager: $('#divConfigMobNumsPager'),
		rowNum:10,						
		viewrecords: true,
		height:180,
		width:920,	
		onSelectRow: function(rowid){
			UI_configMobNums.fillForm(rowid);
			disableControls(true);
			enableGridButtons();
		},
	   	loadComplete: function (e){	   
	   	},
	   	beforeRequest: function(){
	   	}	 
	}).navGrid("#divConfigMobNumsPager",{refresh: true, edit: false, add: false, del: false, search: false});
		
}


UI_configMobNums.addConfigOnClick = function () {
	disableControls(false);
	$('#frmAddModifyConfigs').resetForm();
	$("#spnOfficerId").html("");
	$('#btnEditConfigMobNums').disableButton();
	$('#btnDeleteConfigMobNums').disableButton();
	setField("hdnMode","ADD");
	setField("version","-1");
}


UI_configMobNums.editConfigOnClick = function () {
	disableControls(false);
	$('#btnDeleteConfigMobNums').disableButton();
	$("#btnSaveConfigMobNums").enableButton();
	setField("hdnMode","EDIT");
}


UI_configMobNums.deleteConfigOnClick = function () {
	disableControls(false);
	$("#spnOfficerId").html("");
	var cancelOptions = {
			cache : false,
			success : showDeleteResponse,
			url : 'showOfficersNotificationsConfiguration!deleteOfficersMobileNumbersConfig.action',
			dataType : 'json',
			resetForm : false
	};
	
	$('#frmAddModifyConfigs').ajaxSubmit(cancelOptions);
	disableForm(true);
	return false;	
}

UI_configMobNums.closeOnClick = function () {
	top.LoadHome();
}

UI_configMobNums.resetOnClick = function () {
	//$('#frmAddModifyConfigs').resetForm();
	setField("txtMobCntry", "");
	setField("txtMobArea", "");
	setField("txtMobiNo", "");
	setField("contactName", "");
	setField("contactEmail", "");
	
	
	
}

UI_configMobNums.saveConfigOnClick = function () {
	disableControls(false);
	var saveOptions = {
			cache : false,
			beforeSubmit: showRequest,
			success : showResponse,
			url : 'showOfficersNotificationsConfiguration.action',
			dataType : 'json',
			resetForm : false
	};
	
	getMobileNumber();
//	setField("hdnMode","ADD");
	
	if($("#hdnId").val()==""){
		setField("version","-1");
	}
	$('#frmAddModifyConfigs').ajaxSubmit(saveOptions);
	return false;	
}

//// sesOnlineAirportCodeListData
//
//// function to populate all online airports for search criteria
UI_configMobNums.populateAllAirportsForSearch = function (){	
	var objLB = new listBox();
	objLB.dataArray = arrOnlineAirports;
	var strID = "selFromSrch,selToSrch";
	objLB.id = strID;
	objLB.blnFirstEmpty = true;	// first value empty or not
	objLB.firstValue = "";		// first value
	objLB.firstTextValue = "";	// first Text
	objLB.fillListBox();
}

UI_configMobNums.populateAllAirportsToAddConfig = function (){	
	var objLB = new listBox();
	objLB.dataArray = arrOnlineAirports;
	var strID = "selFromOpt,selToOpt";
	objLB.id = strID;
	objLB.blnFirstEmpty = true;	// first value empty or not
	objLB.firstValue = "ANY";		// first value
	objLB.firstTextValue = "ANY";	// first Text
	objLB.fillListBox();
}


UI_configMobNums.fillForm = function(rowid) {
	
	
	setField("hdnId",$("#tblConfigMobNums").getCell(rowid,'OfficerMobNumsConfigsResultsTO.OfficerId'));
	
	$("#spnOfficerId").html( jQuery("#tblConfigMobNums").getCell(rowid,'OfficerMobNumsConfigsResultsTO.OfficerId') );
	
	$("#contactName").val( jQuery("#tblConfigMobNums").getCell(rowid,'OfficerMobNumsConfigsResultsTO.OfficerName') );
	
	$("#contactEmail").val( jQuery("#tblConfigMobNums").getCell(rowid,'OfficerMobNumsConfigsResultsTO.emailAddress') );

	$("#moblieNumber").val( jQuery("#tblConfigMobNums").getCell(rowid,'OfficerMobNumsConfigsResultsTO.MobNumber') );
	
	$("#version").val( jQuery("#tblConfigMobNums").getCell(rowid,'OfficerMobNumsConfigsResultsTO.version') );
	setMobileNumber();

}


UI_configMobNums.validateSearch = function (){
	return true;
}


function showResponse(responseText, statusText)  {
	top[2].HideProgress();
	showCommonError(responseText['msgType'], responseText['messageTxt']);
    if(responseText['msgType'] != "false") {
    	$('#frmAddModifyConfigs').clearForm();
    	jQuery("#tblConfigMobNums").trigger("reloadGrid");
    	$("#btnAddConfigMobNums").attr('disabled', false); 
    	$("#btnSaveConfigMobNums").enableButton();
    	alert("Record Successfully added");
    }			    
} 		

function showDeleteResponse(responseText, statusText)  {
	top[2].HideProgress();
	showCommonError(responseText['msgType'], responseText['messageTxt']);
    if(responseText['msgType'] != "false") {
    	$('#frmAddModifyConfigs').clearForm();
    	jQuery("#tblConfigMobNums").trigger("reloadGrid");
    	$("#btnAddConfigMobNums").attr('disabled', false); 
    	$("#btnSaveConfigMobNums").enableButton();
    	alert("Record Deleted");
    }			    
} 	




//pre-submit callback 
function showRequest(formData, jqForm, options) { 
	
    if(	trim($("#contactName").val()) == "") {			    	
    	showCommonError("Error", "Enter a Contact Name...");
    	getFieldByID("contactName").focus();
    	return false; 
    }
    if(!isAlphaNumericTextArea(trim($("#contactName").val()))) {			    	
    	showCommonError("Error", "Special Characters & Numbers Not Valid For Contact Name...");
    	getFieldByID("contactName").focus();
    	return false; 
    }
	
    var mobileCountryCode = trim($('#txtMobCntry').val());
    var mobileAreaCode = trim($('#txtMobArea').val());
    var mobileuserCode = trim($('#txtMobiNo').val());
    var emailAddress = trim($("#contactEmail").val());
    
    if(reqIsMobileNumMandatory){
    	if(isValidMobileNumber()){    	
		    if(trim(emailAddress) != "" ) {			    	
		    	return isValidEmail();
	        }
    	}else{
    		return false;
    	}
	}
    
    if(reqIsEmailMandatory){
    	if(isValidEmail()){
    		if(!(mobileCountryCode =="" && mobileAreaCode =="" && mobileuserCode =="")){
	    		return isValidMobileNumber();
	    	}
    	}else{
    		return false;
    }
    }
    
    if(!reqIsMobileNumMandatory && !reqIsEmailMandatory){
    	 if(emailAddress =="" && mobileCountryCode =="" && mobileAreaCode =="" && mobileuserCode ==""){
    		 showCommonError("Error", "Either Email or Mobile Number can't be empty....");
    	     getFieldByID("contactEmail").focus();
    	     return false;  
    	 }
    	 
    	 if(emailAddress != ""){
    		 return isValidEmail();
    	 }
    	 if(mobileCountryCode !="" || mobileAreaCode !="" || mobileuserCode !=""){
    		 return isValidMobileNumber();
    	 }
    }
   
    top[2].ShowProgress();
    return true; 
} 

function isValidEmail(){
	if(!checkEmail(trim($("#contactEmail").val())) || trim($("#contactEmail").val()) == "") {			    	
    	showCommonError("Error", "Invalid Email Address....");
    	getFieldByID("contactEmail").focus();
    	return false; 
    }
	return true;
}
function isValidMobileNumber(){
    if(!IsNumeric($('#txtMobCntry').val()) || trim($('#txtMobCntry').val()) == ""){
    	showCommonError("Error", "Invalide Country Code...");
    	getFieldByID("txtMobCntry").focus();
    	return false;
    }
    if(!IsNumeric($('#txtMobArea').val()) || trim($('#txtMobArea').val()) == ""){
    	showCommonError("Error", "Invalide Area Code...");
    	getFieldByID("txtMobArea").focus();
    	return false;
    }
    if(!IsNumeric($('#txtMobiNo').val()) || trim($("#txtMobiNo").val()) == "" ){
    	showCommonError("Error", "Invalide Mobile Number...");
    	getFieldByID("txtMobiNo").focus();
    	return false;
    }
    
    return true;
}


function  disableControls(cond){
	$("input").each(function(){ 
	      $(this).attr('disabled', cond); 
	});	
	$("textarea").each(function(){ 
	      $(this).attr('disabled', cond); 
	});	
	$("select").each(function(){ 
	      $(this).attr('disabled', cond); 
	});
	$("file").each(function(){ 
	      $(this).attr('disabled', cond); 
	});	
	$("#btnCloseConfigMobNums").enableButton();
	
}

function getMobileNumber(){
	var mobNum = $('#txtMobCntry').val() + "-" + $('#txtMobArea').val() +"-" +$('#txtMobiNo').val() ;
	setField("moblieNumber", mobNum);
	
}

function setMobileNumber(){
		var mobNum = $('#moblieNumber').val().split("-");
		setField("txtMobCntry", mobNum[0]);
		setField("txtMobArea", mobNum[1]);
		setField("txtMobiNo", mobNum[2]);
}

function enableGridButtons(){
	$("#btnAddConfigMobNums").enableButton();
	$("#btnDeleteConfigMobNums").enableButton();
	$("#btnEditConfigMobNums").enableButton();
	$("#btnResetConfigMobNums").enableButton();
}

function disableForm(cond){
	$("txtMobCntry").attr('disabled',cond);
	$("txtMobArea").attr('disabled',cond);
	$("txtMobiNo").attr('disabled',cond);
	$("contactName").attr('disabled',cond);
	$("#btnSaveConfigMobNums").attr('disabled',cond); 	
};

function IsNumeric(input)
{
	if(input.indexOf("-")>-1){
		return false;
	}else{
		return (input - 0) == input && (''+input).trim().length > 0;
	}
}

function isAlphaNumericTextArea(s){return RegExp("^[a-zA-Z-/ \w\s\&._]+$").test(s);}
