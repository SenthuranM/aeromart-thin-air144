var objCol11 = new DGColumn();
objCol11.columnType = "label";
objCol11.width = "5%";
objCol11.arrayIndex = 0;
objCol11.toolTip = "Title";
objCol11.headerText = "Title";
objCol11.itemAlign = "left"
objCol11.sort = "true";

var objCol18 = new DGColumn();
objCol18.columnType = "label";
objCol18.width = "5%";
objCol18.arrayIndex = 15;
objCol18.toolTip = "Pax Type";
objCol18.headerText = "Type";
objCol18.itemAlign = "left"

var objCol12 = new DGColumn();
objCol12.columnType = "label";
// objCol12.width = "20%";
objCol12.arrayIndex = 1;
objCol12.toolTip = "First Name";
objCol12.headerText = "First Name";
objCol12.itemAlign = "left"

var objCol13 = new DGColumn();
objCol13.columnType = "label";
// objCol13.width = "20%";
objCol13.arrayIndex = 2;
objCol13.toolTip = "Last Name";
objCol13.headerText = "Last Name";
objCol13.itemAlign = "left"

var objCol14 = new DGColumn();
objCol14.columnType = "label";
objCol14.width = "18%";
objCol14.arrayIndex = 3;
objCol14.toolTip = "PNR";
objCol14.headerText = "PNR";
objCol14.itemAlign = "left"

var objCol15 = new DGColumn();
objCol15.columnType = "label";
objCol15.width = "10%";
objCol15.arrayIndex = 4;
objCol15.toolTip = "Destination";
objCol15.headerText = "Destination";
objCol15.itemAlign = "left"

var objCol17 = new DGColumn();
objCol17.columnType = "label";
objCol17.width = "10%";
objCol17.arrayIndex = 6;
objCol17.toolTip = "Processing Status";
objCol17.headerText = "Processing Status";
objCol17.itemAlign = "left"

// ---------------- Grid
var objDG1 = new DataGrid("spnPFSProcessing");
objDG1.addColumn(objCol18);
objDG1.addColumn(objCol11);
objDG1.addColumn(objCol12);
objDG1.addColumn(objCol13);
objDG1.addColumn(objCol14);
objDG1.addColumn(objCol15);
objDG1.addColumn(objCol17);

objDG1.width = "99%";
objDG1.height = "120px";
objDG1.headerBold = false;
objDG1.rowSelect = true;
objDG1.arrGridData = arrData;
objDG1.seqNo = true;
objDG1.seqStartNo = 1;
objDG1.paging = true;
objDG1.pgnumRecPage = 20;
objDG1.pgonClick = "gridNavigations";
objDG1.rowClick = "RowClick";
objDG1.displayGrid();
objDG1.shortCutKeyFunction = "Body_onKeyDown";
