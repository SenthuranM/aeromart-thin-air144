/*
 * Author : Menaka P. Wedige
*/
var screenId = "SC_SHDS_003";
var valueSeperator = "~";
var strRowData;
var strGridRow;
var strPFSGridRow;
var intLastRec;
var initialArr=new Array();
initialArr=arrData;
var closeAction;

var paxStatusArr= new Array("NOSHO" ,"GOSHO", "NOREC", "OFFLK");
var paxStatusOptArr= new Array("N" ,"G", "R", "O");

var processTypeArr= new Array("All" ,"Parsed", "Reconciled","Unparsed","Waiting");
var processTypeOptArr= new Array("All" ,"P", "R","U", "W");
var isPax=true;
var initStat = "";
var genAction = "" ;
var pfsStat = "";
var pfsPaxStat = "";
var isChangedPAX="NO";
var currentPFS="";

var values=opener.currentPFSData;

var DownloadTS = values[0];
var FlightNo = values[1];
var FlightDate = values[2];
var PFSId = values[3];
var FromAirport = values[11];
var FromAddress = values[12];
var Status ="";
if(pfsState!=""){
	var state;
	if(pfsState=="P"){
		state="Parsed"
	}if(pfsState=="U"){
		state="Unparsed"
	}if(pfsState=="R"){
		state="Processed"
	}if(pfsState=="W"){
		state="Waiting"
	}
	 Status = state;
}else{
	 Status = values[4];
}
var PfsContent=values[5];
setField("txtaPFSCmnts",PfsContent);
DivWrite("spnContent",PfsContent);
//On Page loading make sure that all the page input fields are cleared
function winOnLoad(strMsg,strMsgType) {
	if(strMsg != null && strMsgType != null)
		showWindowCommonError(strMsgType,strMsg);
	objOnFocus();
	setField("hdnCurrentPfs",values);
	genAction="PL";
	getFieldByID("selTitle").focus();
	opener.setPageEdited(false,isPax);
	populateStatusCombo(arrTitle,"selTitle",arrTitle);
	populatePaxTypeCombo(paxTypes,"selPaxType",paxTypes);
	populateCombo(paxStatusArr,"selAddStatus",paxStatusOptArr);
	setDefaultAirport();
	setField("selCC","Y");
	setField("selPaxCat","A");
	if(arrFormData!=null && arrFormData.length>0){
		setField("hdnCurrentPfs",arrFormData[0]);		
		values=arrFormData[0].split(",");
		setField("selPaxType",arrFormData[17]);
		setPaxTitles(getFieldByID("selPaxType").value);
		setField("selTitle",arrFormData[1]);
		setField("txtFirstName",arrFormData[2]);
		setField("txtLastName",arrFormData[3]);
		setField("txtPNR",arrFormData[4]);
		setField("selDest",arrFormData[5]);
		setField("selAddStatus",arrFormData[6]);
		changeStatusOnload();
		setField("hdnPaxVersion",arrFormData[7]);
		setField("hdnProcessSta",arrFormData[8]);
		setField("hdnCurrentRowNum",arrFormData[9]);
		strGridRow=arrFormData[9];
		setField("hdnMode",arrFormData[10]);
		setField("hdnUIMode",arrFormData[11]);
		setField("hdnPaxID",arrFormData[12]);
		setField("hdnPFSID",arrFormData[13]);
		setField("txtPaxNO",arrFormData[14]);
		setField("selCC",arrFormData[15]);
		setField("selPaxCat",arrFormData[16]);
		if(arrFormData[18] == ""){
			document.getElementById("radByName").checked = true;
		}else{
			document.getElementById("radByETicket").checked = true;
			setField("txtETicket",arrFormData[18]);
		}
		 DownloadTS = values[0];
		 FlightNo = values[1];
		 FlightDate = values[2];
		 PFSId = values[3];
		 FromAirport = values[11];
		 FromAddress = values[12];
		 Status ="";
		 if(pfsState!=""){
		var state;
		if(pfsState=="P"){
			state="Parsed"
		}if(pfsState=="U"){
			state="Unparsed"
		}if(pfsState=="R"){
			state="Processed"
		}if(pfsState=="W"){
			state="Waiting"
		}
		 Status = state;
	}else{
		 Status = values[4];
	}
	var PfsContent=values[5];
	setField("txtaPFSCmnts",PfsContent);
	DivWrite("spnContent",PfsContent);
		if(arrFormData[11]=="ADD"){
			genAction="CA";
			
		}
		if(arrFormData[11]=="EDIT"){
			genAction="CE";
		} 
		setField("hdnState","");
		opener.setPageEdited(true,isPax);
			if(saveSuccess==-1){
				getFieldByID("selTitle").focus();
			}if(saveSuccess==-2){
				getFieldByID("selTitle").focus();
			}if(saveSuccess==-3){
				getFieldByID("txtFirstName").focus();
			}if(saveSuccess==-4){
				getFieldByID("txtLastName").focus();
			}if(saveSuccess==-5){
				getFieldByID("txtPNR").focus();
			}if(saveSuccess==-6){
				getFieldByID("selDest").focus();
			}if(saveSuccess==-7){
				getFieldByID("selAddStatus").focus();
			}
	}

	if(saveSuccess==0){
			alert("Record Successfully saved!");
			isChangedPAX="YES";
			setField("hdnState","");
	}
	if(saveSuccess==1){
		isChangedPAX="YES";
		setField("hdnState","");
		
	}
	
	var strCurrentPFS="<table width='80%' border='0' cellpadding='0' cellspacing='4' ID='Table9'><tr>"
									+"<td><font><b>PFS ID</font></td>"
									+"<td><font><b>Downloaded Time</font></td>"
									+"<td><font><b>Flight No</font></td>"
									+"<td><font><b>Flight Date</font></td>"
									+"<td><font><b>Airport</font></td>"
									+"<td><font><b>SITA Address</font></td>"
									+"<td><font><b>PFS Status</font></td></tr>"
													+ "<tr><td><font>" +PFSId+"</font></td>"
													+"<td><font>"+DownloadTS+"</td>"		
													+ "<td><font>" +FlightNo+"</font></td>"
													+ "<td><font>" +FlightDate+"</font></td>"
													+ "<td><font>" +FromAirport+"</font></td>"
													+ "<td><font>" +FromAddress+"</font></td>"
													+ "<td><font>" +Status+"</font></td></tr>";

	
	document.getElementById("spnHeader").innerHTML=strCurrentPFS;
	currentPFS=opener.currentPFS;
	setVisible("spnHeader",true);
	controlBehaviour(initStat, genAction,pfsStat,pfsPaxStat);
	if(arrFormData[11] == "ADDTBA") {
		Disable("txtPaxNO",false);		
		getFieldByID("txtPaxNO").focus();
		Disable("txtFirstName",true);
		Disable("txtLastName",true);
		Disable("selAddStatus",true);
		Disable("selPaxType",true);
		Disable("selTitle",true);
		Disable("selDest",true);
	}
	if(arrFormData[17] != 'IN'){
		disableParent();
	}	
}


function setDefaultAirport(){
	if(getFieldByID("selDest").options.length==2){
			getFieldByID("selDest").options[1].selected=true;
	}

}

function populateCombo(dataArr,controlName,optArr){
	var control=document.getElementById(controlName);
	for(var t=0;t<dataArr.length;t++){
		control.options[t]=new Option(dataArr[t],optArr[t]);
	}

}

function populateStatusCombo(dataArr,controlName,optArr){
	var control=document.getElementById(controlName);
	control.options[0]=new Option("","");
	var p=0;
	for(var t=0;t<dataArr.length;t++){
		var fullTitle=dataArr[t];
		var strTitle=new String(fullTitle);
		var strArr=strTitle.split(",");
		p=t+1;
		control.options[p]=new Option(strArr[1],strArr[0]);
	}

}
function populatePaxTypeCombo(dataArr,controlName,optArr){
	var control=document.getElementById(controlName);
	control.options[0]=new Option("","");
	var p=0;
	//sort the array
	sort(dataArr,1);
	for(var t=0;t<dataArr.length;t++){
		var fullTitle=dataArr[t];
		var strTitle=new String(fullTitle);
		var strArr=strTitle.split(",");
	//	p=t+1;
		control.options[t+1]=new Option(strArr[1],strArr[0]);
	}

}

function addClick(){
	closeAction="DA";
	if(pfsDetailChanged(closeAction)){
		setField("hdnPaxVersion","-1")
		opener.setPageEdited(false,isPax);
		clearControls();
		genAction="CA";
		controlBehaviour(initStat, genAction,pfsStat,pfsPaxStat);
		getFieldByID("selTitle").focus();
		setField("hdnMode","ADD");
		setField("hdnPFSID",PFSId);
		setField("hdnUIMode","ADD");
		Disable("selCC",false);
		Disable("selPaxCat",true);
		setField("hdnIsParent","N");
		disableParent();
		document.getElementById("radByETicket").checked = true;
		clickByETicket();
	}	
}

function tbaClick(){
	closeAction="DA";
	if(pfsDetailChanged(closeAction)){
		setField("hdnPaxVersion","-1")
		opener.setPageEdited(false,isPax);
		clearControls();
		genAction="CA";
		controlBehaviour(initStat, genAction,pfsStat,pfsPaxStat);
		Disable("txtPaxNO",false);		
		getFieldByID("txtPaxNO").focus();
		setField("txtFirstName" , "T.B.A.");
		setField("txtLastName", "T.B.A.");
		setField("selAddStatus", "G");
		setField("selCC", "Y");
		setField("selPaxCat", "A");		
		Disable("txtFirstName",true);
		Disable("txtLastName",true);
		Disable("selAddStatus",true);
		Disable("selTitle",true);
		Disable("selDest",true);
		Disable("selPaxType",true);
		Disable("radByETicket",true);
		Disable("radByName",true);
		changeStatus();
		setField("hdnMode","ADD");
		setField("hdnPFSID",PFSId);
		setField("hdnUIMode","ADDTBA");
		disableParent();
	}	
}

function editClick(){
	genAction="CE";
	controlBehaviour(initStat, genAction,pfsStat,pfsPaxStat);
	changeStatus();
	getFieldByID("selTitle").focus();
	setField("hdnMode","EDIT");
	setField("hdnUIMode","EDIT");
	Disable("selPaxType",false);
	Disable("selCC",false);
//	Disable("selPaxType",true);
	if(getValue("selAddStatus")=="N"){
//		Disable("selCC",true);
		Disable("selPaxCat",true);		
 	}else {
//		Disable("selCC",false);
		Disable("selPaxCat",false);
 	}
	Disable("selParent", true);
	disableType(false);	
	disableAdultTitleChanges(getValue("selTitle"), getValue("selPaxType"), getValue("txtFirstName"), getValue("txtLastName"));
}

function disableAdultTitleChanges(title, type, firstName, lastName){
	// don't allow to change pax type for an adult if he/she has any infant under his/her name
	for(var i = 0; i < arrData.length; i++){
		if(arrData[i][0] == title && arrData[i][15] == type && arrData[i][1] == firstName && arrData[i][2] == lastName ){
			if(arrData[i][16] != ""){
				Disable("selPaxType", true);
				break;
			}
		}
	}
}

function deleteClick(){
	genAction="CD";
	closeAction="R";
	var status=confirm(deleteMessage);
	if(status==true){
		setField("hdnMode","DELETE");
		setField("hdnUIMode","DELETE");
		controlBehaviour(initStat, genAction,pfsStat,pfsPaxStat);
		isChangedPAX="YES";
		document.forms[0].submit();
	}
}

function deleteAllErrorsClick(){
	setField("hdnPFSID",arrData[0][11]);	
	var status=confirm(deleteAllMessage);
	if(status==true){
		setField("hdnMode","DELETEALL");
		setField("hdnUIMode","DELETEALL");
		document.forms[0].submit();
	}
}

function clearControls(){
	setField("selTitle","");
	setField("selPaxType",paxTypes[0][0]);
	setField("txtFirstName","");
	setField("txtLastName","");
	setField("txtPNR","");
	setField("selDest","");
	setDefaultAirport();
	setField("selAddStatus","N");
	setField("txtPaxNO","");
	setField("hdnPaxID","");
	setField("selCC","Y");
	setField("selPaxCat","A");
	setField("selParent","");	
	setField("txtETicket","");
}

function controlBehaviour(initStat, genAction,pfsStat,pfsPaxStat){
	//initStat - Initial Status
	//	AR - All reconciled when page load
	//genAction,
	//pfsStat,
	//pfsPaxStat
	//Enable("btnAdd", (genAction == "PL"  || genAction=="CA" || genAction=="CE"));
	Enable("btnSave", (genAction=="CA" || genAction=="CE") );
	Enable("btnReset", (genAction=="CA" || genAction=="CE") );
	Enable("btnEdit", (genAction == "SR1" && pfsPaxStat != "P" ));
	Enable("btnDelete", (genAction == "SR1" && pfsPaxStat != "P" ));
	enableDataEntryControls(genAction);


}

function enableDataEntryControls(action){

	if(action=="SR1" || action=="PL"){
		Disable("selTitle",true);
		Disable("selPaxType",true);
		Disable("txtFirstName",true);
		Disable("txtLastName",true);
		Disable("txtPNR",true);
		Disable("selDest",true);
		Disable("selAddStatus",true);
		Disable("selCC",true);
		Disable("selPaxCat",true);		
		Disable("txtPaxNO",true);
		Disable("selParent",true);
		Disable("txtETicket",true);
		disableType(true);
	}else{
		if(document.getElementById("radByName").checked){
			Disable("txtETicket",true);
		}
		Disable("selTitle",false);
		Disable("selPaxType",false);
		Disable("txtFirstName",false);
		Disable("txtLastName",false);
		Disable("txtPNR",false);
		Disable("selDest",false);
		Disable("selAddStatus",false);
		Disable("selCC",false);
		Disable("selPaxCat",false);
		Disable("txtPaxNO",true);
		Disable("selParent",false);
		Disable("txtETicket",false);
		disableType(false);
	}
	if(action=="CD"){
		Disable("txtPaxNO",false);
		Disable("selCC",false);
		Disable("selPaxCat",false);
	}

}

function Enable(strControlId, enable){
	Disable(strControlId,!enable);
}

function setStatus(strSelStatus,ctrlName){
	var control=document.getElementById(ctrlName);

	for(var t=0;t<(control.length);t++){
			if(control.options[t].text==strSelStatus){
			control.options[t].selected=true;
			break;
		}
	}

}

//on Grid2 Row click
function RowClick(strRowNo){
	objOnFocus();
	closeAction="DRC";
	if(pfsDetailChanged(closeAction)){
		closeAction="DG";
		genAction = "SR1"
		opener.setPageEdited(false,isPax);
		pfsPaxStat=arrData[strRowNo][8];
		//pfsStat=arrData[strRowNo][8];
		strGridRow=strRowNo;
		controlBehaviour(initStat, genAction,pfsStat,pfsPaxStat);
		setField("hdnGridRow",strRowNo);
		setField("hdnMode","");
		setField("selPaxType",arrData[strRowNo][15]);
		setPaxTitles(getFieldByID("selPaxType").value);
		setField("selTitle",arrData[strRowNo][0]);
		setField("txtFirstName",arrData[strRowNo][1]);
		setField("txtLastName",arrData[strRowNo][2]);
		setField("txtPNR",arrData[strRowNo][3]);
		setField("selDest",arrData[strRowNo][4]);
		setField("selAddStatus",arrData[strRowNo][7]);
		setField("txtPNR",arrData[strRowNo][3]);
		setField("hdnPaxVersion",arrData[strRowNo][10]);
		setField("hdnPaxID",arrData[strRowNo][9]);
		setField("hdnProcessSta",arrData[strRowNo][8]);
		setField("hdnCurrentRowNum",strRowNo);
		setStatus(arrData[strRowNo][6],"selAddStatus");
		setField("hdnPFSID",arrData[strRowNo][11]);
		setField("selCC",arrData[strRowNo][13]);
		setField("selPaxCat",arrData[strRowNo][14]);	
		setField("txtPaxNO","");
		setField("hdnIsParent",arrData[strRowNo][17]);
		
		setField("txtETicket",arrData[strRowNo][18]);
		
		document.getElementById("radByName").checked = true;
		
		if(arrData[strGridRow][16] !=  ''){
			document.getElementById("spnInfantName").style.display = "";
			document.getElementById('spnInfantName').innerHTML= "Infant Detail           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +  arrData[strGridRow][16];
		}	else {
			document.getElementById("spnInfantName").style.display = "none";
		}	
	}
}


function processSavePFS(mode) {
	objOnFocus();
	var rowCount=1;
	closeAction="R";
	var pfsData="";
	var validated=false;
	var validateControls=false;
	var addedPFS="";
	isChangedPAX = "YES";
	if(document.getElementById("radByName").checked){

		if (getValue("selPaxType") == '') {
			showWindowCommonError("Error", paxTypeRqrd);
			getFieldByID("selPaxType").focus();
			return false;
		}
		if(getValue("selTitle")==''){
			showWindowCommonError("Error",titleRqrd);
			getFieldByID("selTitle").focus();
			return false;
		}

		if(getText("txtFirstName")==""){
			showWindowCommonError("Error",firstNameRqrd);
			getFieldByID("txtFirstName").focus();
			return false;
		}
		
		if(getText("txtLastName")==""){
			showWindowCommonError("Error",lastNameRqrd);
			getFieldByID("txtLastName").focus();
			return false;
		}
		
		if(getValue("selDest")==""){
			getFieldByID("selDest").focus();				
			showWindowCommonError("Error",destinationRqrd);				
			return false;
		}
		if(getValue("selPaxType")=='IN'){
			if(getValue("selParent") == ''){
				getFieldByID("selParent").focus();				
				showWindowCommonError("Error",infantPaxParentReq);				
				return false;
			}
			
		}
		
		if((getValue("selAddStatus")=="N" || getValue("selAddStatus")=="R") && getText("txtPNR")=="" ){
			showWindowCommonError("Error",PNRRqrd);
			getFieldByID("txtPNR").focus();
			return false; 
		} 
		
	} 
	
	if(document.getElementById("radByETicket").checked && getText("txtETicket")==""){
		showWindowCommonError("Error",eTicketReq);
		getFieldByID("txtETicket").focus();
		return false; 
	}
//	if(document.getElementById("radByETicket").checked && (getValue("selAddStatus")=="N" || getValue("selAddStatus")=="R") && getText("txtPNR")=="" ){
//		showWindowCommonError("Error",PNRRqrd);
//		getFieldByID("txtPNR").focus();
//		return false; 
//	} 
	
	if(!document.getElementById("txtPaxNO").disabled && getText("hdnUIMode") == "ADDTBA"){
		var tmpPaxNo = getText("txtPaxNO");
		if(tmpPaxNo == "" || isNaN(tmpPaxNo) || Number(tmpPaxNo) < 1) {
			showWindowCommonError("Error",paxnoRqrd);
			getFieldByID("txtPaxNO").focus();
			return false;
		}
	}
	
	document.forms[0].hdnMode.value=mode;
	enableDataEntryControls("CD");
	document.forms[0].submit();
	
		
}


function resetPFS(){
	objOnFocus();
	getFieldByID("selTitle").focus();
	opener.setPageEdited(false,isPax);
		var mode=getText("hdnUIMode");
	if(mode=="ADD"){
		genAction="CA";
		clearControls();
	
	}if(mode=="EDIT"){
		genAction="CE";
		if (strGridRow<arrData.length && arrData[strGridRow][9] == getText("hdnPaxID")) {
			setField("hdnMode","");
			setField("selTitle",arrData[strGridRow][0]);
			setField("txtFirstName",arrData[strGridRow][1]);
			setField("txtLastName",arrData[strGridRow][2]);
			setField("txtPNR",arrData[strGridRow][3]);
			setField("selDest",arrData[strGridRow][4]);
			setField("selAddStatus",arrData[strGridRow][7]);
			setField("txtPNR",arrData[strGridRow][3]);
			setField("hdnPaxVersion",arrData[strGridRow][10]);
			setField("hdnPaxID",arrData[strGridRow][9]);
			setField("hdnProcessSta",arrData[strGridRow][8]);
			setField("selCC",arrData[strGridRow][13]);
			setField("selPaxCat",arrData[strGridRow][14]);
			
			setField("hdnCurrentRowNum",strGridRow);
			changeStatus();
			setStatus(arrData[strGridRow][6],"selAddStatus");
		}else{
			clearControls();
			genAction="PL";
		}
	}
	
	controlBehaviour(initStat, genAction,pfsStat,pfsPaxStat);
	
	if(mode=="ADDTBA") {
		tbaClick();
	}
}

function objOnFocus(){
	opener.top[2].HidePageMessage();
}

function pageOnChange(){
		opener.setPageEdited(true,isPax);
	
}

function chkChanges(){
	
	if (opener.top[1].objTMenu.tabGetPageEidted(screenId)) {	
		return confirm("Changes will be lost! Do you wish to Continue?");
	}else{		
		return true;
	}
}


function windowclose(){
	closeAction="DC";
	if(pfsDetailChanged(closeAction)){
		checkPFSDetailChanged();
	}
	
}

	function pfsDetailChanged(action){
		var confirmed=true;
		if((action=="GD" || action=="DC" || action=="DA" || action=="DRC") && opener.isPfsPAXScreenEdited){
			confirmed=confirm("Changes will be lost! Do you wish to Continue?");
			if(confirmed){
				opener.setPageEdited(false, isPax);
			}
		}else if((action=="DC" ) && (opener.isPfsScreenEdited)){// Detail Close Clicked when header changed
			confirmed=confirm("Changes in PFS Header screen will be lost! Do you wish to Continue?");
			if(confirmed){
				opener.setPageEdited(false, isPax);
			}
		
		}else if(action=="DFC" && opener.isPfsPAXScreenEdited){// Window unlod
			opener.setPageEdited(false, isPax);
		}

		return confirmed;

	}

	function reloadPFSHEader(){
		if(closeAction!="R"){
			if(isChangedPAX == "YES"){
			//	confirmed=confirm("Changes in PFS Header screen will be lost! Do you wish to Continue?");
		//	if(confirmed){
					var strTxt=opener.top[1].objTMenu.tabGetValue(screenId);
					var arrTabData=new Array()
					if(strTxt!=""){
						 arrTabData=strTxt.split("~");
					}
					opener.location="showPFSProcessing.action?hdnMode=SEARCH&hdnUIMode=SEARCH"+ "&hdnRecNo="+arrTabData[4]+"&txtFrom="+arrTabData[0]+"&txtTo="+arrTabData[1]+"&selAirport="+arrTabData[2]+"&selProcessType="+arrTabData[3];
			}

		 // }
		}

	}

	
	function checkPFSDetailChanged(){
		if(isChangedPAX == "YES"){
			var strTxt=opener.top[1].objTMenu.tabGetValue(screenId);
			var arrTabData=new Array()
			if(strTxt!=""){
				 arrTabData=strTxt.split("~");
			}
			opener.location="showPFSProcessing.action?hdnMode=SEARCH&hdnUIMode=SEARCH"+ "&hdnRecNo="+arrTabData[4]+"&txtFrom="+arrTabData[0]+"&txtTo="+arrTabData[1]+"&selAirport="+arrTabData[2]+"&selProcessType="+arrTabData[3];
			window.close();
		} else {
			window.close();
		}
	}
	

function resetVariables() {
	pfsDetailChanged("DFC");
}

function changeStatus(){
	if(getValue("selAddStatus")=="G"){
		Disable("txtPNR",true);
		Disable("txtETicket",true);
		Disable("selCC",false);
		Disable("selPaxCat",false);
		setField("txtPNR","");
		if(genAction != "CE"){
			document.getElementById("radByName").checked = true;
			document.getElementById("radByETicket").disabled=true;
			setUIControlsOnStatusChange(getValue("selAddStatus"));
		}
 	}else if(getValue("selAddStatus")=="R"){
		Disable("txtPNR",false)
		Disable("txtETicket",false);
		Disable("selCC",false);
		Disable("selPaxCat",false);
		
		if(genAction != "CE"){
			document.getElementById("radByETicket").checked = true;
			document.getElementById("radByETicket").disabled=false;
			setUIControlsOnStatusChange(getValue("selAddStatus"));
		}
 	}else if(getValue("selAddStatus")=="N"){
 		Disable("txtPNR",false)
		Disable("txtETicket",false);
 		Disable("selCC",true);
		Disable("selPaxCat",true);
		
		if(genAction != "CE"){
			document.getElementById("radByETicket").checked = true;
			document.getElementById("radByETicket").disabled=false;
			setUIControlsOnStatusChange(getValue("selAddStatus"));
		}
 	}
}

function changeStatusOnload(){
	if(getValue("selAddStatus")=="G"){
		Disable("txtPNR",true);
		setText("txtPNR","");
 	}else{
		Disable("txtPNR",false)
 	}
 	if(getValue("selAddStatus")=="N"){
		Disable("selCC",true);
		Disable("selPaxCat",true);		
 	}else {
		Disable("selCC",false);
		Disable("selPaxCat",false);
 	}
}
function paxChange(){
	var paxType = getFieldByID("selPaxType").value;
	if(paxType == 'IN'){
		Disable("selParent", false);
		document.getElementById("spnParentReq").style.display = "";
	} else {
		getFieldByID("selParent").value = '';
		Disable("selParent", true);
		document.getElementById("spnParentReq").style.display = "none";
	}	
	
	setPaxTitles(paxType);
}

function setPaxTitles(paxType){
	
	$('#selTitle').empty();
	var control=document.getElementById("selTitle");
	 
	if(paxType == 'IN'){		
		
		populateStatusCombo(arrInfantTitles,"selTitle",arrInfantTitles);
		
	} else if(paxType == 'CH'){
		
		populateStatusCombo(arrTitleChild,"selTitle",arrTitleChild);
		
	} else {
		
		populateStatusCombo(arrTitle,"selTitle",arrTitle);
	}	
}

function clickByETicket(){
	Disable("txtETicket",false);
	Disable("selTitle",true);
	Disable("selPaxType",true);
	Disable("selParent",true);
	Disable("txtFirstName",true);
	Disable("txtLastName",true);
	clearControls();
	setField("selPaxType","");
	populateStatusCombo(arrTitle,"selTitle",arrTitle);
	getFieldByID("txtETicket").focus();
}

function clickByName(){
	Disable("selTitle",false);
	Disable("selPaxType",false);
	Disable("selParent",true);
	Disable("txtFirstName",false);
	Disable("txtLastName",false);	
	Disable("txtETicket",true);
	setField("txtETicket","");
	setField("selPaxType",paxTypes[0][0]);
	populateStatusCombo(arrTitle,"selTitle",arrTitle);
	getFieldByID("selTitle").focus();
}

function disableType(val){
	document.getElementById("radByETicket").disabled=val;
	document.getElementById("radByName").disabled=val;
}

function disableParent(){
	Disable("selParent", true);	
	document.getElementById("spnInfantName").style.display = "none";
}

function setUIControlsOnStatusChange(status){
	if(status == "G"){
		Disable("selTitle",false);
		Disable("selPaxType",false);
		Disable("selParent",true);
		Disable("txtFirstName",false);
		Disable("txtLastName",false);	
		setField("txtPNR","");
		setField("selDest","");
		setField("txtPaxNO","");
		setField("hdnPaxID","");
		setField("selCC","Y");
		setField("selPaxCat","A");
		setField("txtETicket","");
		setDefaultAirport();
		getFieldByID("selTitle").focus();
	}else if(status == "R"){
		Disable("selTitle",true);
		Disable("selPaxType",true);
		Disable("selParent",true);
		Disable("txtFirstName",true);
		Disable("txtLastName",true);
		setField("selTitle","");
		setField("selPaxType","");
		setField("txtFirstName","");
		setField("txtLastName","");
		setField("selDest","");
		setField("txtPaxNO","");
		setField("hdnPaxID","");
		setField("selCC","Y");
		setField("selPaxCat","A");
		setField("selParent","");
		setDefaultAirport();
		getFieldByID("txtETicket").focus();
	}else{
		Disable("selTitle",true);
		Disable("selPaxType",true);
		Disable("selParent",true);
		Disable("txtFirstName",true);
		Disable("txtLastName",true);
		setField("selTitle","");
		setField("selPaxType","");
		setField("txtFirstName","");
		setField("txtLastName","");
		setField("selDest","");
		setField("txtPaxNO","");
		setField("hdnPaxID","");
		setField("selCC","Y");
		setField("selPaxCat","A");
		setField("selParent","");
		setDefaultAirport();
		getFieldByID("txtETicket").focus();
	}
}
