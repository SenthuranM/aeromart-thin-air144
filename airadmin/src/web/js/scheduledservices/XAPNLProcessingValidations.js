/*
 * Author : Dhanushka Navin Ranatunga
 */
var screenId = "SC_SHDS_0033";
var valueSeperator = "~";
var strRowData;
var strGridRow = -1;
var intLastRec;
var initialArr = new Array();
var currentPFSData = "";
var currentData = "";
initialArr = arrData;
var closeClicked = false;
// var confirmed=false;
var pfsHEaderAction;
var titleArr = new Array("Mr", "Mrs", "Child", "Infant");

var paxStatusArr = new Array("NoSho", "GoSho", "NoRec", "OffId");
var paxStatusOptArr = new Array("N", "G", "R", "O");

var processTypeArr = new Array("All", "Parsed", "Processed", "Unparsed");
var processTypeOptArr = new Array("All", "P", "R", "U");

var processStatusTypeArr = new Array("", "Parsed", "Processed", "Unparsed");
var processStatusTypeOptArr = new Array("", "P", "R", "U");

var initStat = "";
var genAction = "";
var pfsStat = "";
var isPFSEmpty = true;
var pfsPaxStat = "";
var isPfsScreenEdited = false;
var isPfsPAXScreenEdited = false;
var pfsId = "";
var isPFS = false;

// On Page loading make sure that all the page input fields are cleared
function winOnLoad(strMsg, strMsgType) {
	objOnFocus();
	if (strMsg != null && strMsgType != null)
		showCommonError(strMsgType, strMsg);
	getFieldByID("txtFrom").focus();
	genAction = "PL";
	setPageEdited(false, isPFS);
	setField("hdnAirport", getText("selAirport"));
	top[2].HideProgress();
	populateCombo(processTypeArr, "selProcessType", processTypeOptArr);
	populateCombo(processStatusTypeArr, "selStatus", processStatusTypeOptArr);
	getData();

	if (arrFormData != null && arrFormData.length > 0) {
		setPageEdited(true, isPFS);
		setField("hdnPFSData", arrFormData[0]);
		setField("hdnCurrentPfs", arrFormData[1]);
		currentPFSData = arrFormData[1];
		currentData = arrFormData[1];
		setField("hdnVersion", arrFormData[2]);
		setField("hdnCurrentRowNum", arrFormData[3]);
		strGridRow = arrFormData[3];
		setField("txtDownloadTS", arrFormData[4]);
		setField("txtFlightNo", arrFormData[5]);
		setField("txtFlightDateTime", arrFormData[6]);
		setField("selDest", arrFormData[7]);
		setField("txFromAddress", arrFormData[8]);
		setField("selStatus", arrFormData[9]);
		if (arrFormData[10] != "0") {
			DivWrite("spnPFSID", arrFormData[10]);
		}
		setField("hdnPFSID", arrFormData[10]);
		setField("hdnMode", arrFormData[11]);
		setField("hdnUIMode", arrFormData[12]);
		setField("txtDownloadTime", arrFormData[13]);
		setField("txFlightTime", arrFormData[14]);
		if (arrFormData[15] != null && arrFormData[15] != "") {
			setField("txtaRulesCmnts", arrFormData[15]);
			// DivWrite("spnContent",arrFormData[15]);
			writeXAPNLContent(arrFormData[15]);
		}
		setField("hdnPaxCount", arrFormData[16]);
		if (arrFormData[12] == "ADD") {
			genAction = "CA";
		}
		if (arrFormData[12] == "EDIT") {
			genAction = "CE";
		}
		if (arrFormData[11] == "PROCESS") {
			setPageEdited(false, isPFS);
			genAction = "SR";
			if (arrFormData[4] != "" && arrFormData[14] != ""
					&& arrFormData[5] != "" && arrFormData[7] != "") {
				isPFSEmpty = false;
			} else {
				isPFSEmpty = true;
			}
			pfsStat = "P";
		}

		if (saveSuccess == -1) {
			getFieldByID("txtDownloadTS").focus();
		}
		if (saveSuccess == -2) {
			getFieldByID("txtDownloadTS").focus();
		}
		if (saveSuccess == -3) {
			getFieldByID("txtDownloadTime").focus();
		}
		if (saveSuccess == -4) {
			getFieldByID("txtFlightNo").focus();
		}
		if (saveSuccess == -5) {
			getFieldByID("txtFlightDateTime").focus();
		}
		if (saveSuccess == -6) {
			getFieldByID("txFlightTime").focus();
		}
		if (saveSuccess == -7) {
			getFieldByID("selDest").focus();
		}
		if (saveSuccess == -8) {
			getFieldByID("txFromAddress").focus();
		}
	}

	if (saveSuccess == 0) {
		alert("Record Successfully saved!");
	}
	if (saveSuccess == 1) {
		alert("Record Successfully Processed!");
	}
	if (saveSuccess == 3) {
		alert("Record Processed with Errors!");
	}

	controlBehaviour(initStat, genAction, pfsStat, pfsPaxStat, isPFSEmpty);
}

function writeXAPNLContent(src) {
	if (browser.isIE) {
		DivWrite("spnContent", src);
		setDisplay("spnContent", true);
	} else {
		setField("txtaRulesCmnts", src.replace(/<BR>/g, '\n'));
		setDisplay("txtaRulesCmnts", true);
	}
}

function gridNavigations(intRecNo, intErrMsg) {

	pfsHEaderAction = "GN";
	if (pfsHeaderChanged(pfsHEaderAction)) {
		setField("hdnMode", "PAGING");
		setField("hdnRecNo", intRecNo);
		setData();
		if (intErrMsg != "") {
			top[2].objMsg.MessageText = "<li> " + intErrMsg;
			top[2].objMsg.MessageType = "Error";
			showCommonError("Error", intErrMsg);
		} else {
			document.forms[0].submit();
			objDG.seqStartNo = intRecNo;
		}
		setPageEdited(false, isPFS);
	}

}

function setData() {
	var searchData = getText("txtFrom") + "~" + getText("txtTo") + "~"
			+ getText("selAirport") + "~" + getValue("selProcessType") + "~"
			+ getText("hdnRecNo");
	top[1].objTMenu.tabSetValue(screenId, searchData);
}

function testDate(dt) {
	var validated = true;

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10) {
		dtCM = "0" + dtCM
	}
	if (dtCD < 10) {
		dtCD = "0" + dtCD
	}
	var strSysDate = systemDate;
	var valid = dateChk(strSysDate);

	var tempDay = dt.substring(0, 2);
	var tempMonth = dt.substring(3, 5);
	var tempYear = dt.substring(6, 10);
	var tempOStartDate = (tempYear + tempMonth + tempDay);

	tempDay = strSysDate.substring(0, 2);
	tempMonth = strSysDate.substring(3, 5);
	tempYear = strSysDate.substring(6, 10);
	var strSysODate = (tempYear + tempMonth + tempDay);

	if (valid) {
		// if(CheckDates(dt,strSysDate)){
		if (tempOStartDate >= strSysODate) {
			validated = false;
		}

	}

	return validated;

}

function getData() {
	var strTxt = top[1].objTMenu.tabGetValue(screenId);
	if (strTxt != "") {
		var arrTabData = strTxt.split("~");
		setField("txtFrom", arrTabData[0]);
		setField("txtTo", arrTabData[1]);
		setField("selAirport", arrTabData[2]);
		setField("selProcessType", arrTabData[3]);

	}
}

function populateCombo(dataArr, controlName, optArr) {
	var control = document.getElementById(controlName);
	for ( var t = 0; t < dataArr.length; t++) {
		control.options[t] = new Option(dataArr[t], optArr[t]);
	}

}

function ReceivedPFSRowClick(strRowNo) {
	objOnFocus();
	pfsHEaderAction = "HG";
	if (pfsHeaderChanged(pfsHEaderAction)) {
		// if(top.loadCheck(getPageEdited())){
		clearControls();
		genAction = "SR"; // Row selected in Grid 1
		pfsStat = arrData[strRowNo][6]; // PFS Status
		// is PFS Details Empty
		var strDownloadTs = arrData[strRowNo][2];
		if (arrData[strRowNo][1] != "" && strDownloadTs.length == 16
				&& arrData[strRowNo][2] != "" && arrData[strRowNo][11] != "") {
			isPFSEmpty = false;
		} else {
			isPFSEmpty = true;
		}

		strGridRow = strRowNo;
		setField("hdnCurrentRowNum", strRowNo);

		DivWrite("spnPFSID", arrData[strRowNo][3]);
		var dateFull = trim(arrData[strRowNo][0]);
		var date = "";
		var time = "";
		if (dateFull.indexOf(" ") != -1) {
			date = dateFull.substring(0, dateFull.indexOf(" "));
			time = dateFull.substring(dateFull.indexOf(" "));
		} else {
			date = dateFull;
			time = "";
		}
		setField("txtDownloadTS", trim(date));
		setField("txtDownloadTime", trim(time));
		setField("txtFlightNo", arrData[strRowNo][1]);

		dateFull = trim(arrData[strRowNo][2]);
		if (dateFull.indexOf(" ") != -1) {
			date = dateFull.substring(0, dateFull.indexOf(" "));
			time = dateFull.substring(dateFull.indexOf(" "));
		} else {
			date = dateFull;
			time = "";
		}

		setField("txtFlightDateTime", trim(date));
		setField("txFlightTime", trim(time));
		setField("selDest", arrData[strRowNo][11]);
		setField("txFromAddress", arrData[strRowNo][12]);
		setField("selStatus", arrData[strRowNo][6]);
		setField("hdnPFSID", arrData[strRowNo][3]);
		setField("hdnVersion", arrData[strRowNo][13]);
		pfsId = arrData[strRowNo][3];
		setField("hdnCurrentPfs", arrData[strRowNo]);
		setField("txtaRulesCmnts", arrData[strRowNo][5]);

		// DivWrite("spnContent",arrData[strRowNo][5]);
		writeXAPNLContent(arrData[strRowNo][5]);

		setField("hdnPaxCount", arrData[strRowNo][14]);
		controlBehaviour(initStat, genAction, pfsStat, pfsPaxStat, isPFSEmpty);
		setPageEdited(false, isPFS);
	}
}

function addClick() {
	pfsHEaderAction = "HA";
	if (pfsHeaderChanged(pfsHEaderAction)) {
		// if (top.loadCheck(getPageEdited())){
		clearControls();
		genAction = "CA";
		setField("hdnVersion", "-1");
		setField("selStatus", "U");
		setField("hdnMode", "ADD");
		setField("hdnUIMode", "ADD");
		controlBehaviour(initStat, genAction, pfsStat, pfsPaxStat, isPFSEmpty);
		getFieldByID("txtDownloadTS").focus();
	}

}

function deleteClick() {
	var status = confirm(deleteMessage);
	genAction = "CD";
	if (status) {
		setField("hdnMode", "DELETE");
		setField("hdnUIMode", "DELETE");
		enableDataEntryControls("CD");
		Disable("selStatus", false);
		setField("hdnRecNo", "1");
		setData();
		document.forms[0].submit();
	}

}

function editClick() {
	genAction = "CE";
	setField("hdnMode", "EDIT");
	setField("hdnUIMode", "EDIT");
	controlBehaviour(initStat, genAction, pfsStat, pfsPaxStat, isPFSEmpty);
	getFieldByID("txtDownloadTS").focus();

}

function clearControls() {
	setField("txtDownloadTS", getVal("hdnCurrentDate"));
	DivWrite("spnPFSID", "");
	setField("selStatus", "");
	setField("txFromAddress", "");
	setField("txtFlightNo", "");
	setField("selDest", "-1");
	setField("hdnPFSID", "");
	setField("txtFlightDateTime", "");
	setField("txtDownloadTime", "00:00");
	setField("txFlightTime", "");
	setField("txtaRulesCmnts", "");

	// DivWrite("spnContent","");
	writeXAPNLContent("");

}

function controlBehaviour(initStat, genAction, pfsStat, pfsPaxStat, isPFSEmpty) {
	// initStat - Initial Status
	// AR - All reconciled when page load
	// genAction,
	// pfsStat,
	// pfsPaxStat
	// Enable("btnAdd", (genAction=="CD" || genAction=="SR" || genAction=="PL"
	// || genAction=="CA" || genAction=="CE"));
	// Enable("btnEdit", ((genAction=="CD" || genAction=="SR" ||
	// genAction=="CE") && (pfsStat=="U" )));
	var paxCount = getText("hdnPaxCount");
	// Enable("btnDelete", ((genAction=="CD" || genAction=="SR" ||
	// genAction=="CE") && (pfsStat=="U" && paxCount==0)));
	// Enable("btnSave", (genAction=="CA" || genAction=="CE") );
	Enable("btnReset", (genAction == "CA" || genAction == "CE"));
	// Enable("btnProcess", (genAction=="SR") && (pfsStat=="P") );
	Enable("btnPFSDetails", (genAction == "SR" || genAction == "CE")
			&& (isPFSEmpty == false));
	enableDataEntryControls(genAction);

}

function beforeUnload() {
	// if (top[1].objTMenu.tabIsAvailable(screenId)){
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
		setPageEdited(false, isPFS);
		setPageEdited(false, true);
	}

}

function closeParentAndChildWindow() {
	pfsHEaderAction = "HC";
	if (pfsHeaderChanged(pfsHEaderAction)) {
		if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
			top[0].objWindow.close();
		}
		top[1].objTMenu.tabRemove(screenId);
	}
}

function pfsHeaderChanged(action) {
	var confirmed = true;
	if ((action == "HC") && (isPfsScreenEdited && isPfsPAXScreenEdited)) { // Header
																			// close
																			// clicked
																			// when
																			// both
																			// screens
																			// changed
		confirmed = top.loadCheck(getPageEdited());
		if (confirmed) {
			setPageEdited(false, isPFS);
			setPageEdited(false, true);
		}
	} else if ((action == "HC" || action == "HG" || action == "HS"
			|| action == "VD" || action == "GN" || action == "HA")
			&& (isPfsScreenEdited)) { // Actions when pfs header changed
		confirmed = top.loadCheck(getPageEdited());
		if (confirmed) {
			setPageEdited(false, isPFS);
			// setPageEdited(false, true);
		}

	} else if ((action == "HC" || action == "HS" || action == "VD"
			|| action == "GN" || action == "HSV")
			&& (isPfsPAXScreenEdited)) { // Actions when pfs details changed
		confirmed = confirm("Changes will be lost in PFS Detail screen! Do you wish to Continue?");
		if (confirmed) {
			setPageEdited(false, isPFS);
			setPageEdited(false, true);
		}
	}
	return confirmed;
}

function viewPFSDetails() {

	var currentPFS = "";
	var pfs = getText("hdnPFSID");
	pfsHEaderAction = "VD";
	if (pfsHeaderChanged(pfsHEaderAction)) {

		if (strGridRow != -1) {
			setField("hdnCurrentPfs", arrData[strGridRow]);
			currentPFSData = arrData[strGridRow];
			currentData = arrData[strGridRow][0] + "," + arrData[strGridRow][1]
					+ "," + arrData[strGridRow][2] + ","
					+ arrData[strGridRow][3] + "," + arrData[strGridRow][4]
					+ ", ," + arrData[strGridRow][6] + ","
					+ arrData[strGridRow][7] + "," + arrData[strGridRow][8]
					+ "," + arrData[strGridRow][9] + ","
					+ arrData[strGridRow][10] + "," + arrData[strGridRow][11]
					+ "," + arrData[strGridRow][12] + ","
					+ arrData[strGridRow][13];
			currentPFS = arrData[strGridRow][0] + "," + arrData[strGridRow][1]
					+ "," + arrData[strGridRow][2] + ","
					+ arrData[strGridRow][3] + "," + arrData[strGridRow][11]
					+ "," + arrData[strGridRow][6];

		}

		if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
			top[0].objWindow.close();
		}
		setPageEdited(false, isPFS);
		var strFilename = "showXAPNLDetailProcessing.action?hdnCurrentPfs="
				+ currentData + "&hdnPFSID=" + pfs + "&hdnState=true"; // showAgentwiseFC.do
		var intHeight = 700;
		var intWidth = 900;
		strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
				+ intWidth
				+ ',height='
				+ intHeight
				+ ',resizable=no,top='
				+ ((window.screen.height - intHeight) / 2)
				+ ',left='
				+ (window.screen.width - intWidth) / 2;
		top[0].objWindow = window.open(strFilename, "CWindow", strProp);

	}

	setPageEdited(false, isPFS);
}

function enableDataEntryControls(action) {

	if (action == "SR" || action == "PL") {
		Disable("selStatus", true);
		Disable("txtFlightNo", true);
		Disable("txtDownloadTS", true);
		Disable("txFromAddress", true);
		Disable("selDest", true);
		Disable("txtFlightDateTime", true);
		Disable("txtDownloadTime", true);
		Disable("txFlightTime", true);
	} else {
		// Disable("selStatus",false);
		Disable("selStatus", true);
		Disable("txtFlightNo", false);
		Disable("txtDownloadTS", false);
		Disable("txFromAddress", false);
		Disable("selDest", false);
		Disable("txtFlightDateTime", false);
		Disable("txtDownloadTime", false);
		Disable("txFlightTime", false);

	}

}

function Enable(strControlId, enable) {
	Disable(strControlId, !enable);
}

function setStatus(strSelStatus, ctrlName) {
	var control = document.getElementById(ctrlName);

	for ( var t = 0; t < (control.length); t++) {
		if (control.options[t].text == strSelStatus) {
			control.options[t].selected = true;
			break;
		}
	}

}

function fromDateValidation(cntfield) {
	if (!dateChk(cntfield)) {
		showCommonError("Error", fromDateInvalid);
		if (top[1].objTMenu.focusTab == screenId)
			getFieldByID(cntfield).focus();
		return;
	}
}

function toDateValidation(cntfield) {
	if (!dateChk(cntfield)) {
		showCommonError("Error", toDateInvalid);
		if (top[1].objTMenu.focusTab == screenId)
			getFieldByID(cntfield).focus();
		return;
	}
}

function searchData() {
	objOnFocus();
	pfsHEaderAction = "HS";
	if (pfsHeaderChanged(pfsHEaderAction)) {

		setPageEdited(false, isPFS);
		fromDateValidation('txtFrom');
		toDateValidation('txtTo');
		var StartD = "";
		var EndD = "";
		var today = formatDate(getVal("hdnCurrentDate"), "dd/mm/yyyy",
				"dd-mmm-yyyy");

		if (!isEmpty(getText("txtFrom"))) {
			StartD = formatDate(getVal("txtFrom"), "dd/mm/yyyy", "dd-mmm-yyyy");
		}
		if (!isEmpty(getText("txtTo"))) {
			EndD = formatDate(getVal("txtTo"), "dd/mm/yyyy", "dd-mmm-yyyy");
		}
		// Compare to date/From date and current date
		if (!isEmpty(getText("txtFrom")) && !dateValidDate(getText("txtFrom"))) {
			showCommonError("Error", fromDateInvalid);
			if (top[1].objTMenu.focusTab == screenId)
				getFieldByID("txtFrom").focus();
			return false;
		}
		if (!isEmpty(getText("txtTo")) && !dateValidDate(getText("txtTo"))) {
			showCommonError("Error", toDateInvalid);
			if (top[1].objTMenu.focusTab == screenId)
				getFieldByID("txtFrom").focus();
			return false;
		}
		if (!isEmpty(getText("txtFrom")) && compareDates(today, StartD, '<')) {
			showCommonError("Error", fromDateLessthanCurrentDate);
			if (top[1].objTMenu.focusTab == screenId)
				getFieldByID("txtFrom").focus();
			return false;
		}
		if (!isEmpty(getText("txtTo")) && compareDates(today, EndD, '<')) {
			showCommonError("Error", toDateLessthanToday);
			if (top[1].objTMenu.focusTab == screenId)
				getFieldByID("txtTo").focus();
			return false;
		}
		if (!isEmpty(getText("txtTo")) && compareDates(StartD, EndD, '>')) {
			showCommonError("Error", todateLessthanFromDate);
			if (top[1].objTMenu.focusTab == screenId)
				getFieldByID("txtTo").focus();
			return false;
		}

		setData();
		top[0].initializeVar();
		document.forms[0].hdnUIMode.value = "SEARCH";
		document.forms[0].hdnMode.value = "SEARCH";
		top[2].ShowProgress();
		setField("hdnRecNo", "1");
		document.forms[0].submit();
	}

}

function validateDatetime(status, dateFull, rowCount, index) {
	var validated = false;
	var fulFormattedVal = "";
	var errorMsg = "";
	if (dateFull != "") {
		fulFormattedVal = checkDate(dateFull);
		if (fulFormattedVal.indexOf(" ") != -1) {
			date = trim(fulFormattedVal.substring(0, fulFormattedVal
					.indexOf(" ")));
			time = trim(fulFormattedVal.substring(fulFormattedVal.indexOf(" ")));
		} else {
			date = fulFormattedVal;
			time = "";
		}
		objDG.setCellValue(rowCount - 1, index, fulFormattedVal);
		arrData[rowCount - 1][index] = fulFormattedVal;
		if (status == "flightTS") {
			if (date != "" && !dateValidDate(date)) {
				errorMsg = flightDateFormatInvalid;
			} else if (time != "" && !IsValidTime(time)) {
				errorMsg = flightTimeFormatInvalid;
			} else {

			}
		} else {
			if (date != "" && !dateValidDate(date)) {
				errorMsg = downloadTSDateFormatInvalid;
			} else if (time != "" && !IsValidTime(time)) {
				errorMsg = downloadTSTimeFormatInvalid;
			}

		}

	}

	return errorMsg;
}

function validateDatetimeControls(status, dateFull) {
	var validated = false;
	var fulFormattedVal = "";
	var errorMsg = "";
	if (dateFull != "") {
		fulFormattedVal = checkDate(dateFull);
		if (fulFormattedVal.indexOf(" ") != -1) {
			date = trim(fulFormattedVal.substring(0, fulFormattedVal
					.indexOf(" ")));
			time = trim(fulFormattedVal.substring(fulFormattedVal.indexOf(" ")));
		} else {
			date = fulFormattedVal;
			time = "";
		}
		if (status == "flightTS") {
			if (date != "" && !dateValidDate(date)) {
				errorMsg = flightDateFormatInvalid;
			} else if (time != "" && !IsValidTime(time)) {
				errorMsg = flightTimeFormatInvalid;
			} else {

			}
		} else {
			if (date != "" && !dateValidDate(date)) {
				errorMsg = downloadTSDateFormatInvalid;
			} else if (time != "" && !IsValidTime(time)) {
				errorMsg = downloadTSTimeFormatInvalid;
			}

		}

	}

	return errorMsg;
}

function compareDays(downloadTime, flightTime) {
	var downloadTm = trim(downloadTime);
	var flightTm = trim(flightTime);
	var downloadDate;
	var downloadTime;
	var flightDate;
	var flightTime;
	var StartD;
	var EndD;
	var errorMsg = "";
	if (downloadTm != "" && flightTm != "") {
		if (downloadTm.indexOf(" ") != -1) {
			downloadDate = trim(downloadTm
					.substring(0, downloadTm.indexOf(" ")));
			downloadTime = trim(downloadTm.substring(downloadTm.indexOf(" ")));
			var tempDay = downloadDate.substring(0, 2);
			var tempMonth = downloadDate.substring(3, 5);
			var tempYear = downloadDate.substring(6, 10);
			StartD = tempDay + tempMonth + tempYear;
		} else {
			downloadDate = downloadTm;
			flightTime = "";
		}

		if (flightTm.indexOf(" ") != -1) {
			flightDate = trim(flightTm.substring(0, flightTm.indexOf(" ")));
			flightTime = trim(flightTm.substring(flightTm.indexOf(" ")));
			var tempDay = flightDate.substring(0, 2);
			var tempMonth = flightDate.substring(3, 5);
			var tempYear = flightDate.substring(6, 10);
			EndD = tempDay + tempMonth + tempYear;
		} else {
			flightDate = flightTm;
			flightTime = "";
		}

		if (StartD < EndD) {
			errorMsg = DownloadTSGreaterFlightTs;
		} else if (EndD == StartD
				&& compareTime(downloadTime, flightTime) == false) {
			errorMsg = DownloadTSGreaterFlightTs;
		}
	}
	return errorMsg;

}

function compareTime(outTmFrom, outTmTo) {
	var outFTm = parseFloat(outTmFrom.replace(":", "."));
	var outTTm = parseFloat(outTmTo.replace(":", "."));

	if (outFTm < outTTm) {
		return false;
	} else {
		return true;
	}

}

function processAllXAPNL() {
	if (strGridRow != -1) {
		setField("hdnCurrentPfs", arrData[strGridRow]);
		currentPFSData = arrData[strGridRow];
	}

	document.forms[0].hdnMode.value = "PROCESS";
	enableDataEntryControls("CE");
	setData();
	// top[2].ShowProgress();
	setField("hdnRecNo", "1");
	Disable("selStatus", false);
	document.forms[0].submit();

}

function processSavePFS(mode) {
	objOnFocus();
	var pfsData = "";
	var validateControls = false;
	var downloadDate = getText("txtDownloadTS");
	var downloadTime = trim(getText("txtDownloadTime"));
	pfsHEaderAction = "HSV";
	if (pfsHeaderChanged(pfsHEaderAction)) {

		var flightDate = getText("txtFlightDateTime");
		var flightTime = trim(getText("txFlightTime"));
		var today = formatDate(getVal("hdnCurrentDate"), "dd/mm/yyyy",
				"dd-mmm-yyyy");
		var StartD = "";
		var EndD = "";
		var start = "";
		var end = "";
		if (!isEmpty(getText("txtDownloadTS"))) {
			StartD = formatDate(getVal("txtDownloadTS"), "dd/mm/yyyy",
					"dd-mmm-yyyy");
			var tempDay = downloadDate.substring(0, 2);
			var tempMonth = downloadDate.substring(3, 5);
			var tempYear = downloadDate.substring(6, 10);
			start = tempDay + tempMonth + tempYear;
		}
		if (!isEmpty(getText("txtFlightDateTime"))) {
			EndD = formatDate(getVal("txtFlightDateTime"), "dd/mm/yyyy",
					"dd-mmm-yyyy");
			var tempDay = flightDate.substring(0, 2);
			var tempMonth = flightDate.substring(3, 5);
			var tempYear = flightDate.substring(6, 10);
			end = tempDay + tempMonth + tempYear;
		}
		if (!document.getElementById("selDest").disabled) {
			if (downloadDate == "") {
				showCommonError("Error", downloadTSRqrd);
				getFieldByID("txtDownloadTS").focus();
				return false;
			}
			if (downloadTime == "") {
				showCommonError("Error", downloadTimeRqrd);
				getFieldByID("txtDownloadTime").focus();
				return false;
			}
			if (!dateValidDate(downloadDate)) {
				showCommonError("Error", downloadTSTimeFormatInvalid);
				getFieldByID("txtDownloadTS").focus();
				return false;
			}
			if (!IsValidTime(downloadTime)) {
				showCommonError("Error", downloadTSTimeFormatInvalid);
				getFieldByID("txtDownloadTime").focus();
				return false;

			}
			if (flightDate != "" && !dateValidDate(flightDate)) {
				showCommonError("Error", flightTimeFormatInvalid);
				getFieldByID("txtFlightDateTime").focus();
				return false;

			}
			if (flightTime != "" && !IsValidTime(flightTime)) {
				showCommonError("Error", flightTimeFormatInvalid);
				getFieldByID("txFlightTime").focus();
				return false;

			}
			if (downloadDate != "" && compareDates(today, StartD, '<')) {
				showCommonError("Error", DownloadTSlessThanToday);
				if (top[1].objTMenu.focusTab == screenId)
					getFieldByID("txtDownloadTS").focus();
				return false;

			}
			if (flightDate != "" && compareDates(StartD, EndD, '<')) {
				showCommonError("Error", DownloadTSGreaterFlightTs);
				if (top[1].objTMenu.focusTab == screenId)
					getFieldByID("txtDownloadTS").focus();
				return false;

			}
			if (flightTime != "" && start == end
					&& compareTime(downloadTime, flightTime) == false) {
				showCommonError("Error", DownloadTSGreaterFlightTs);
				if (top[1].objTMenu.focusTab == screenId)
					getFieldByID("txFlightTime").focus();
				return false;
			} else {
				validateControls = true;
			}
		} else {
			validateControls = true;

		}

		if (validateControls) {
			if (strGridRow != -1) {
				setField("hdnCurrentPfs", arrData[strGridRow]);
				currentPFSData = arrData[strGridRow];
			}
			setField("hdnPFSData", pfsData.substring(0, pfsData.length - 2));
			document.forms[0].hdnMode.value = mode;
			if (mode == "PROCESS") {
				enableDataEntryControls("CE");
			}
			setData();
			top[2].ShowProgress();
			setField("hdnRecNo", "1");
			Disable("selStatus", false);
			document.forms[0].submit();

		}
	}

}
function clearAllControls() {
	clearControls();
	setField("txtDownloadTS", "");
	setField("txtDownloadTime", "");

}
function resetPFS() {
	objOnFocus();
	getFieldByID("txtDownloadTS").focus();
	var mode = getText("hdnUIMode");
	if (mode == "ADD") {
		genAction = "CA";
		clearControls();
		setField("selStatus", "U");

	}
	if (mode == "EDIT") {
		genAction = "CE";
		if (strGridRow < arrData.length
				&& arrData[strGridRow][3] == getText("hdnPFSID")) {
			var dateFull = trim(arrData[strGridRow][0]);
			var date = "";
			var time = "";
			if (dateFull.indexOf(" ") != -1) {
				date = dateFull.substring(0, dateFull.indexOf(" "));
				time = dateFull.substring(dateFull.indexOf(" "));
			} else {
				date = dateFull;
				time = "";
			}
			setField("txtDownloadTS", trim(date));
			setField("txtDownloadTime", trim(time));

			dateFull = trim(arrData[strGridRow][2]);
			if (dateFull.indexOf(" ") != -1) {
				date = dateFull.substring(0, dateFull.indexOf(" "));
				time = dateFull.substring(dateFull.indexOf(" "));
			} else {
				date = dateFull;
				time = "";
			}

			setField("txtFlightDateTime", trim(date));
			setField("txFlightTime", trim(time));
			setField("txtFlightNo", arrData[strGridRow][1]);
			setField("selDest", arrData[strGridRow][11]);
			setField("txFromAddress", arrData[strGridRow][12]);
			setField("selStatus", arrData[strGridRow][6]);
			setField("hdnPFSID", arrData[strGridRow][3]);
			setField("hdnVersion", arrData[strGridRow][13]);
		} else {
			clearAllControls();
			genAction = "PL";
		}
	}

	controlBehaviour(initStat, genAction, pfsStat, pfsPaxStat, isPFSEmpty);
	setPageEdited(false, isPFS);
	// gridLoadCheck();

}

function objOnFocus() {
	top[2].HidePageMessage();
}

function pageOnChange() {
	setPageEdited(true, isPFS);
}

function setPageEdited(isEdited, isPfsPAXScreen) {
	var isEitherOneEdited = false;
	if (isPfsPAXScreen) {
		isPfsPAXScreenEdited = isEdited;
	} else {
		isPfsScreenEdited = isEdited;
	}

	if (isPfsScreenEdited || isPfsPAXScreenEdited) {
		isEitherOneEdited = true;
	}

	top[1].objTMenu.tabPageEdited(screenId, isEitherOneEdited);

}

function getPageEdited() {
	return top[1].objTMenu.tabGetPageEidted(screenId);
}

// Calender
var objCal1 = new Calendar("spnCalendarDG1");

objCal1.onClick = "setDate";
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFrom", strDate);
		break;
	case "1":
		setField("txtTo", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 50;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function LoadCalendarForAdd(strID, objEvent) {
	if (genAction == "CA" || genAction == "CE") {
		objCal1.ID = strID;
		objCal1.top = 130;
		objCal1.left = 0;
		objCal1.onClick = "setDates";
		objCal1.showCalendar(objEvent);
	}
}

function setDates(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtDownloadTS", strDate);
		break;
	case "1":
		setField("txtFlightDateTime", strDate);
		break;
	pageOnChange();
}
}
