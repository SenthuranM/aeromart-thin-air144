var objCol11 = new DGColumn();
objCol11.columnType = "label";
objCol11.width = "5%";
objCol11.arrayIndex = 0;
objCol11.toolTip = "Title";
objCol11.headerText = "Title";
objCol11.itemAlign = "left";
objCol11.sort = "true";

var objCol18 = new DGColumn();
objCol18.columnType = "label";
objCol18.width = "5%";
objCol18.arrayIndex = 15;
objCol18.toolTip = "Pax Type";
objCol18.headerText = "Type";
objCol18.itemAlign = "left";

var objCol12 = new DGColumn();
objCol12.columnType = "label";
objCol12.width = "7%";
objCol12.arrayIndex = 1;
objCol12.toolTip = "First Name";
objCol12.headerText = "First Name";
objCol12.itemAlign = "left";

var objCol13 = new DGColumn();
objCol13.columnType = "label";
objCol13.width = "7%";
objCol13.arrayIndex = 2;
objCol13.toolTip = "Last Name";
objCol13.headerText = "Last Name";
objCol13.itemAlign = "left";

var objCol14 = new DGColumn();
objCol14.columnType = "label";
objCol14.width = "7%";
objCol14.arrayIndex = 3;
objCol14.toolTip = "PNR";
objCol14.headerText = "PNR";
objCol14.itemAlign = "left";

var objCol15 = new DGColumn();
objCol15.columnType = "label";
objCol15.width = "5%";
objCol15.arrayIndex = 4;
objCol15.toolTip = "Destination";
objCol15.headerText = "Dest";
objCol15.itemAlign = "left";

var objCol16 = new DGColumn();
objCol16.columnType = "label";
objCol16.width = "3%";
objCol16.arrayIndex = 5;
objCol16.toolTip = "Status";
objCol16.headerText = "Status";
objCol16.itemAlign = "left";

var objCol17 = new DGColumn();
objCol17.columnType = "label";
objCol17.width = "8%";
objCol17.arrayIndex = 6;
objCol17.toolTip = "Processing Status";
objCol17.headerText = "Processing Status";
objCol17.itemAlign = "left";
	
var objCol19 = new DGColumn();
objCol19.columnType = "label";
objCol19.width = "10%";
objCol19.arrayIndex = 16;
objCol19.toolTip = "Eticket No";
objCol19.headerText = "Eticket No";
objCol19.itemAlign = "left";

var objCol20 = new DGColumn();
objCol20.columnType = "label";
objCol20.width = "9%";
objCol20.arrayIndex = 19;
objCol20.toolTip = "Passport Number";
objCol20.headerText = "Passport No";
objCol20.itemAlign = "left";

var objCol21 = new DGColumn();
objCol21.columnType = "label";
objCol21.width = "7%";
objCol21.arrayIndex = 20;
objCol21.toolTip = "Passport Expiry Date";
objCol21.headerText = "Expiry Date";
objCol21.itemAlign = "left";

var objCol22 = new DGColumn();
objCol22.columnType = "label";
objCol22.width = "3%";
objCol22.arrayIndex = 21;
objCol22.toolTip = "Country Of Issue";
objCol22.headerText = "Country of Issue";
objCol22.itemAlign = "left";

var objCol23 = new DGColumn();
objCol23.columnType = "label";
objCol23.width = "3%";
objCol23.arrayIndex = 22;
objCol23.toolTip = "Seat Number";
objCol23.headerText = "Seat No";
objCol23.itemAlign = "left";

var objCol24 = new DGColumn();
objCol24.columnType = "label";
objCol24.width = "4%";
objCol24.arrayIndex = 23;
objCol24.toolTip = "Weight Indicator";
objCol24.headerText = " Wt. Indicator";
objCol24.itemAlign = "left";

var objCol25 = new DGColumn();
objCol25.columnType = "label";
objCol25.width = "5%";
objCol25.arrayIndex = 24;
objCol25.toolTip = "No Of Checked Baggage";
objCol25.headerText = "Checked Bags";
objCol25.itemAlign = "left";

var objCol26 = new DGColumn();
objCol26.columnType = "label";
objCol26.width = "5%";
objCol26.arrayIndex = 25;
objCol26.toolTip = "Checked Baggage Weight";
objCol26.headerText = "Checked Wt.";
objCol26.itemAlign = "left";

var objCol27 = new DGColumn();
objCol27.columnType = "label";
objCol27.width = "5%";
objCol27.arrayIndex = 26;
objCol27.toolTip = "Unchecked Baggage Weight";
objCol27.headerText = "Unchecked Wt.";
objCol27.itemAlign = "left";

// ---------------- Grid
var objDG1 = new DataGrid("spnPRLProcessing");
objDG1.addColumn(objCol18);
objDG1.addColumn(objCol11);
objDG1.addColumn(objCol12);
objDG1.addColumn(objCol13);
objDG1.addColumn(objCol19);
objDG1.addColumn(objCol14);
objDG1.addColumn(objCol15);
objDG1.addColumn(objCol16);
objDG1.addColumn(objCol17);
objDG1.addColumn(objCol20);
objDG1.addColumn(objCol21);
objDG1.addColumn(objCol22);
objDG1.addColumn(objCol23);
objDG1.addColumn(objCol24);
objDG1.addColumn(objCol25);
objDG1.addColumn(objCol26);
objDG1.addColumn(objCol27);


objDG1.width = "100%";
objDG1.height = "120px";
objDG1.headerBold = false;
objDG1.rowSelect = true;
objDG1.arrGridData = arrData;
objDG1.seqNo = true;
objDG1.seqStartNo = 1;
objDG1.paging = true;
objDG1.pgnumRecPage = 20;
objDG1.pgonClick = "gridNavigations";
objDG1.rowClick = "RowClick";
objDG1.displayGrid();
objDG1.shortCutKeyFunction = "Body_onKeyDown";
