
var values=opener.currentPFSData;
var DownloadTS = values[0];
var FlightNo = values[1];
var FlightDate = values[2];
var PFSId = values[3];
var FromAirport = values[11];
var FromAddress = values[12];
var Status = values[4];

jQuery(document).ready(function(){  

	displayPFSHeaderDetails();
						
			jQuery("#listPFSHistory").jqGrid({ 
								
				url:"showPFSHistory.action?pfsID=" + PFSId ,
				datatype: "json",
				jsonReader : {
					  root: "rows", 
					  page: "page",
					  records: "records",
					  repeatitems: false,					 
					  id: "0"				  
					},													
				colNames:['Date','Action', 'System Note', 'User Note', 'User Name', 'IP Address'], 
				
				colModel:[    
					{name:'timeStampZulu',  jsonmap:'timeStampZulu', width:90, align:"center"},
					{name:'action', jsonmap:'action', width:100, align:"left"},
					{name:'systemNote', jsonmap:'systemNote', width:260, align:"left"},
					{name:'userNote', jsonmap:'userNote', width:290, align:"left"},
					{name:'userName', jsonmap:'userName', width:150, align:"center"},
					{name:'ipAddress',jsonmap:'ipAddress', width:150, align:"center"}
				],
	
				imgpath: '../../themes/default/images/jquery', multiselect:false,
				//pager: jQuery('#PFSHistoryPager'),
				//rowNum:20,						
				viewrecords: true,
				autowidth: true,
				height: 500,
				loadui:'block'
										
				});  //.navGrid("#PFSHistoryPager",{refresh: true, edit: false, add: false, del: false, search: false});
								
		});

		
function displayPFSHeaderDetails(){
	
	 DownloadTS = values[0];
	 FlightNo = values[1];
	 FlightDate = values[2];
	 PFSId = values[3];
	 FromAirport = values[11];
	 FromAddress = values[12];
	 Status = values[4];	
	 var strCurrentPFS="<table width='80%' border='0' cellpadding='0' cellspacing='4' ID='Table9'><tr>"
									+"<td><font><b>PFS ID</font></td>"
									+"<td><font><b>Downloaded Time</font></td>"
									+"<td><font><b>Flight No</font></td>"
									+"<td><font><b>Flight Date</font></td>"
									+"<td><font><b>Airport</font></td>"
									+"<td><font><b>SITA Address</font></td>"
									+"<td><font><b>PFS Status</font></td></tr>"
													+ "<tr><td><font>" +PFSId+"</font></td>"
													+"<td><font>"+DownloadTS+"</td>"		
													+ "<td><font>" +FlightNo+"</font></td>"
													+ "<td><font>" +FlightDate+"</font></td>"
													+ "<td><font>" +FromAirport+"</font></td>"
													+ "<td><font>" +FromAddress+"</font></td>"
													+ "<td><font>" +Status+"</font></td></tr>";
	
	document.getElementById("spnHeader").innerHTML=strCurrentPFS;
}
