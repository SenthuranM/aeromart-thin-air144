var objCol11 = new DGColumn();
objCol11.columnType = "label";
objCol11.width = "5%";
objCol11.arrayIndex = 0;
objCol11.toolTip = "Title";
objCol11.headerText = "Title";
objCol11.itemAlign = "left"
objCol11.sort = "true";

var objCol18 = new DGColumn();
objCol18.columnType = "label";
objCol18.width = "5%";
objCol18.arrayIndex = 14;
objCol18.toolTip = "Pax Type";
objCol18.headerText = "Type";
objCol18.itemAlign = "left"

var objCol12 = new DGColumn();
objCol12.columnType = "label";
objCol12.width = "17%";
objCol12.arrayIndex = 1;
objCol12.toolTip = "First Name";
objCol12.headerText = "First Name";
objCol12.itemAlign = "left"

var objCol13 = new DGColumn();
objCol13.columnType = "label";
objCol13.width = "17%";
objCol13.arrayIndex = 2;
objCol13.toolTip = "Last Name";
objCol13.headerText = "Last Name";
objCol13.itemAlign = "left"

var objCol15 = new DGColumn();
objCol15.columnType = "label";
objCol15.width = "9%";
objCol15.arrayIndex = 4;
objCol15.toolTip = "Destination";
objCol15.headerText = "Destination";
objCol15.itemAlign = "left"

var objCol16 = new DGColumn();
objCol16.columnType = "label";
objCol16.width = "7%";
objCol16.arrayIndex = 5;
objCol16.toolTip = "Status";
objCol16.headerText = "Status";
objCol16.itemAlign = "left"

var objCol17 = new DGColumn();
objCol17.columnType = "label";
objCol17.width = "10%";
objCol17.arrayIndex = 6;
objCol17.toolTip = "Processing Status";
objCol17.headerText = "Processing Status";
objCol17.itemAlign = "left"
	
var objCol19 = new DGColumn();
objCol19.columnType = "label";
objCol19.width = "15%";
objCol19.arrayIndex = 15;
objCol19.toolTip = "Eticket No";
objCol19.headerText = "Eticket No";
objCol19.itemAlign = "left"

// ---------------- Grid
var objDG1 = new DataGrid("spnETLProcessing");
objDG1.addColumn(objCol18);
objDG1.addColumn(objCol11);
objDG1.addColumn(objCol12);
objDG1.addColumn(objCol13);
objDG1.addColumn(objCol19);
objDG1.addColumn(objCol15);
objDG1.addColumn(objCol16);
objDG1.addColumn(objCol17);


objDG1.width = "99%";
objDG1.height = "120px";
objDG1.headerBold = false;
objDG1.rowSelect = true;
objDG1.arrGridData = arrData;
objDG1.seqNo = true;
objDG1.seqStartNo = 1;
objDG1.paging = true;
objDG1.pgnumRecPage = 20;
objDG1.pgonClick = "gridNavigations";
objDG1.rowClick = "RowClick";
objDG1.displayGrid();
objDG1.shortCutKeyFunction = "Body_onKeyDown";
