var screenId = "SC_SHDS_0035";
var valueSeperator = "~";
var strRowData;
var strGridRow;
var strPRLGridRow;
var intLastRec;
var initialArr=new Array();
initialArr=arrData;
var closeAction;

var paxStatusArr= new Array("FLOWN" ,"NOT BOARDED");
var paxStatusOptArr= new Array("F" ,"B");

var processTypeArr= new Array("All" ,"Parsed", "Reconciled","Unparsed");
var processTypeOptArr= new Array("All" ,"P", "R","U");

var weightIndicatorsArr = new Array("KILOS","POUNDS","PIECES ONLY");
var weightIndicatorsOptArr = new Array("K","L","P");
var isPax=true;
var initStat = "";
var genAction = "" ;
var prlStat = "";
var prlPaxStat = "";
var isChangedPAX="NO";
var currentPRL="";

var values=opener.currentPRLData;

var DownloadTS = values[0];
var FlightNo = values[1];
var FlightDate = values[2];
var PRLId = values[3];
var FromAirport = values[11];
var FromAddress = values[12];
var Status ="";
if(prlState!=""){
	var state;
	if(prlState=="P"){
		state="Parsed"
	}if(prlState=="U"){
		state="Unparsed"
	}if(prlState=="R"){
		state="Processed"
	}
	 Status = state;
}else{
	 Status = values[4];
}
var PrlContent=values[5];
setField("txtaPRLCmnts",PrlContent);

//On Page loading make sure that all the page input fields are cleared
function winOnLoad(strMsg,strMsgType) {
	if(strMsg != null && strMsgType != null)
		showWindowCommonError(strMsgType,strMsg);
	objOnFocus();
	setField("hdnCurrentPrl",values);
	genAction="PL";
	getFieldByID("selTitle").focus();
	opener.setPageEdited(false,isPax);
	populateStatusCombo(arrTitle,"selTitle",arrTitle);
	populatePaxTypeCombo(paxTypes,"selPaxType",paxTypes);
	populateCombo(paxStatusArr,"selAddStatus",paxStatusOptArr);
	populateWeightIndicCombo(weightIndicatorsArr,"selWeightIndicator",weightIndicatorsOptArr);
	setDefaultAirport();
	setField("selCC","Y");
	//setField("selPaxCat","A");
	if(arrFormData!=null && arrFormData.length>0){
		setField("hdnCurrentPrl",arrFormData[0]);		
		values=arrFormData[0].split(",");
		setField("selTitle",arrFormData[1]);
		setField("selPaxType",arrFormData[17]);
		setField("txtFirstName",arrFormData[2]);
		setField("txtLastName",arrFormData[3]);
		setField("txtPNR",arrFormData[4]);
		setField("selDest",arrFormData[5]);
		setField("selAddStatus",arrFormData[6]);
		changeStatus();
		setField("hdnPaxVersion",arrFormData[7]);
		setField("hdnProcessSta",arrFormData[8]);
		setField("hdnCurrentRowNum",arrFormData[9]);
		strGridRow=arrFormData[9];
		setField("hdnMode",arrFormData[10]);
		setField("hdnUIMode",arrFormData[11]);
		setField("hdnPaxID",arrFormData[12]);
		setField("hdnPRLID",arrFormData[13]);
		//setField("txtPaxNO",arrFormData[14]);
		setField("selCC",arrFormData[15]);
		//setField("selPaxCat",arrFormData[16]);		
		 DownloadTS = values[0];
		 FlightNo = values[1];
		 FlightDate = values[2];
		 PRLId = values[3];
		 FromAirport = values[11];
		 FromAddress = values[12];
		 Status ="";
		 if(prlState!=""){
		var state;
		if(prlState=="P"){
			state="Parsed"
		}if(prlState=="U"){
			state="Unparsed"
		}if(prlState=="R"){
			state="Processed"
		}
		 Status = state;
	}else{
		 Status = values[4];
	}
	var PrlContent=values[5];
	setField("txtaPRLCmnts",PrlContent);
	DivWrite("spnContent",PrlContent);
		if(arrFormData[11]=="ADD"){
			genAction="CA";
			
		}
		if(arrFormData[11]=="EDIT"){
			genAction="CE";
		} 
		setField("hdnState","");
		opener.setPageEdited(true,isPax);
			if(saveSuccess==-1){
				getFieldByID("selTitle").focus();
			}if(saveSuccess==-2){
				getFieldByID("selTitle").focus();
			}if(saveSuccess==-3){
				getFieldByID("txtFirstName").focus();
			}if(saveSuccess==-4){
				getFieldByID("txtLastName").focus();
			}if(saveSuccess==-5){
				getFieldByID("txtPNR").focus();
			}if(saveSuccess==-6){
				getFieldByID("selDest").focus();
			}if(saveSuccess==-7){
				getFieldByID("selAddStatus").focus();
			}
	}

	if(saveSuccess==0){
			alert("Record Successfully saved!");
			isChangedPAX="YES";
			setField("hdnState","");
	}
	if(saveSuccess==1){
		isChangedPAX="YES";
		setField("hdnState","");
		
	}
	
	var strCurrentPRL="<table width='80%' border='0' cellpadding='0' cellspacing='4' ID='Table9'><tr>"
									+"<td><font><b>PRL ID</font></td>"
									+"<td><font><b>Downloaded Time</font></td>"
									+"<td><font><b>Flight No</font></td>"
									+"<td><font><b>Flight Date</font></td>"
									+"<td><font><b>Airport</font></td>"
									+"<td><font><b>SITA Address</font></td>"
									+"<td><font><b>PRL Status</font></td></tr>"
													+ "<tr><td><font>" +PRLId+"</font></td>"
													+"<td><font>"+DownloadTS+"</td>"		
													+ "<td><font>" +FlightNo+"</font></td>"
													+ "<td><font>" +FlightDate+"</font></td>"
													+ "<td><font>" +FromAirport+"</font></td>"
													+ "<td><font>" +FromAddress+"</font></td>"
													+ "<td><font>" +Status+"</font></td></tr>";

	
	document.getElementById("spnHeader").innerHTML=strCurrentPRL;
	currentPRL=opener.currentPRL;
	setVisible("spnHeader",true);
	controlBehaviour(initStat, genAction,prlStat,prlPaxStat);
	if(arrFormData[11] == "ADDTBA") {
		//Disable("txtPaxNO",false);		
		//getFieldByID("txtPaxNO").focus();
		Disable("txtFirstName",true);
		Disable("txtLastName",true);
		Disable("selAddStatus",true);
		Disable("selPaxType",true);
		Disable("selTitle",true);
		Disable("selDest",true);
	}
	
	disableParent();
}


function setDefaultAirport(){
	if(getFieldByID("selDest").options.length==2){
			getFieldByID("selDest").options[1].selected=true;
	}

}

function populateCombo(dataArr,controlName,optArr){
	var control=document.getElementById(controlName);
	for(var t=0;t<dataArr.length;t++){
		control.options[t]=new Option(dataArr[t],optArr[t]);
	}

}

function populateStatusCombo(dataArr,controlName,optArr){
	var control=document.getElementById(controlName);
	control.options[0]=new Option("","");
	var p=0;
	for(var t=0;t<dataArr.length;t++){
		var fullTitle=dataArr[t];
		var strTitle=new String(fullTitle);
		var strArr=strTitle.split(",");
		p=t+1;
		control.options[p]=new Option(strArr[1],strArr[0]);
	}

}
function populatePaxTypeCombo(dataArr,controlName,optArr){
	var control=document.getElementById(controlName);
	//control.options[0]=new Option("","");
	var p=0;
	//sort the array
	sort(dataArr,1);
	for(var t=0;t<dataArr.length;t++){
		var fullTitle=dataArr[t];
		var strTitle=new String(fullTitle);
		var strArr=strTitle.split(",");
	//	p=t+1;
		control.options[t]=new Option(strArr[1],strArr[0]);
	}

}

function populateWeightIndicCombo(dataArr,controlName,optArr){
	var control=document.getElementById(controlName);
	for(var t=0;t<dataArr.length;t++){
		control.options[t]=new Option(dataArr[t],optArr[t]);
	}
}


function tbaClick(){
	closeAction="DA";
	if(prlDetailChanged(closeAction)){
		setField("hdnPaxVersion","-1")
		opener.setPageEdited(false,isPax);
		clearControls();
		genAction="CA";
		controlBehaviour(initStat, genAction,prlStat,prlPaxStat);
		//Disable("txtPaxNO",false);		
		//getFieldByID("txtPaxNO").focus();
		setField("txtFirstName" , "T.B.A.");
		setField("txtLastName", "T.B.A.");
		setField("selAddStatus", "G");
		setField("selCC", "Y");
		//setField("selPaxCat", "A");		
		Disable("txtFirstName",true);
		Disable("txtLastName",true);
		Disable("selAddStatus",true);
		Disable("selTitle",true);
		Disable("selDest",true);
		Disable("selPaxType",true);
		changeStatus();
		setField("hdnMode","ADD");
		setField("hdnPRLID",PRLId);
		setField("hdnUIMode","ADDTBA");
		disableParent();
	}	
}

function clearControls(){
	setField("selTitle","");
	setField("selPaxType",paxTypes[0][0]);
	setField("txtFirstName","");
	setField("txtLastName","");
	setField("txtPNR","");
	setField("selDest","");
	setDefaultAirport();
	setField("selAddStatus","N");
	//setField("txtPaxNO","");
	setField("hdnPaxID","");
	setField("selCC","Y");
	//setField("selPaxCat","A");
	setField("selParent","");
	setField("txtPassportNo","");
	setField("txtPassportExpDate","");
	setField("txtCountryOfIssue","");
	setField("txtSeatNo","");
	setField("selWeightIndicator","K");
	setField("txtNoOfCheckedBag","");
	setField("txtCheckedBagWeight","");
	setField("txtUncheckedBagWeight","");
}


function enableDataEntryControls(action){

	if(action=="SR1" || action=="PL"){
		Disable("selTitle",true);
		Disable("selPaxType",true);
		Disable("txtFirstName",true);
		Disable("txtLastName",true);
		Disable("txtPNR",true);
		Disable("selDest",true);
		Disable("selAddStatus",true);
		Disable("selCC",true);
		//Disable("selPaxCat",true);		
		//Disable("txtPaxNO",true);
		Disable("selParent",true);
		Disable("txtEtNo",true);
		Disable("txtInfEtNo",true);
		Disable("txtPassportNo",true);
		Disable("txtPassportExpDate",true);
		Disable("txtCountryOfIssue",true);
		Disable("txtSeatNo",true);
		Disable("selWeightIndicator",true);
		Disable("txtNoOfCheckedBag",true);
		Disable("txtCheckedBagWeight",true);
		Disable("txtUncheckedBagWeight",true);
		
	}else{
		Disable("selTitle",false);
		Disable("selPaxType",false);
		Disable("txtFirstName",false);
		Disable("txtLastName",false);
		Disable("txtPNR",false);
		Disable("selDest",false);
		Disable("selAddStatus",false);
		Disable("selCC",false);
		//Disable("selPaxCat",false);
		//Disable("txtPaxNO",true);
		Disable("selParent",false);
		Disable("txtEtNo",false);
		Disable("txtInfEtNo",false);
		Disable("txtInfEtNo",false);
		Disable("txtPassportNo",false);
		Disable("txtPassportExpDate",false);
		Disable("txtCountryOfIssue",false);
		Disable("txtSeatNo",false);
		Disable("selWeightIndicator",false);
		Disable("txtNoOfCheckedBag",false);
		Disable("txtCheckedBagWeight",false);
		Disable("txtUncheckedBagWeight",false);

	}
	if(action=="CD"){
		//Disable("txtPaxNO",false);
		Disable("selCC",false);
		//Disable("selPaxCat",false);
	}

}

function Enable(strControlId, enable){
	Disable(strControlId,!enable);
}

function setStatus(strSelStatus,ctrlName){
	var control=document.getElementById(ctrlName);

	for(var t=0;t<(control.length);t++){
			if(control.options[t].text==strSelStatus){
			control.options[t].selected=true;
			break;
		}
	}

}

// Calender
var objCal1 = new Calendar("spnCalendarDG1");

objCal1.buildCalendar();

function LoadCalendarForAdd(strID, objEvent) {
	if (genAction == "CA" || genAction == "CE") {
		objCal1.ID = strID;
		objCal1.top = 130;
		objCal1.left = 0;
		objCal1.onClick = "setDates";
		objCal1.showCalendar(objEvent);
	}
}

function setDates(strDate, strID) {
	switch (strID) {
		case "0":
			setField("txtPassportExpDate", strDate);
			break;
	}
}


//on Grid2 Row click
function RowClick(strRowNo){
	objOnFocus();
	closeAction="DRC";
	if(prlDetailChanged(closeAction)){
		closeAction="DG";
		genAction = "SR1";
		opener.setPageEdited(false,isPax);
		prlPaxStat=arrData[strRowNo][8];
		//prlStat=arrData[strRowNo][8];
		strGridRow=strRowNo;
		controlBehaviour(initStat, genAction,prlStat,prlPaxStat);
		setField("hdnGridRow",strRowNo);
		setField("hdnMode","");
		setField("selTitle",arrData[strRowNo][0]);
		setField("selPaxType",arrData[strRowNo][15]);
		setField("txtFirstName",arrData[strRowNo][1]);
		setField("txtLastName",arrData[strRowNo][2]);
		setField("txtPNR",arrData[strRowNo][3]);
		setField("selDest",arrData[strRowNo][4]);
		setField("selAddStatus",arrData[strRowNo][7]);
		setField("txtPNR",arrData[strRowNo][3]);
		setField("hdnPaxVersion",arrData[strRowNo][10]);
		setField("hdnPaxID",arrData[strRowNo][9]);
		setField("hdnProcessSta",arrData[strRowNo][8]);
		setField("hdnCurrentRowNum",strRowNo);
		setStatus(arrData[strRowNo][6],"selAddStatus");
		setField("hdnPRLID",arrData[strRowNo][11]);
		setField("selCC",arrData[strRowNo][13]);
		setField("txtEtNo",arrData[strRowNo][16]);
		setField("txtInfEtNo",arrData[strRowNo][18]);
		//setField("selPaxCat",arrData[strRowNo][14]);	
		//setField("txtPaxNO","");
		setField("hdnIsParent", arrData[strRowNo][17]);
		setField("txtPassportNo", arrData[strRowNo][19]);
		setField("txtPassportExpDate", arrData[strRowNo][20]);
		setField("txtCountryOfIssue", arrData[strRowNo][21]);
		setField("txtSeatNo", arrData[strRowNo][22]);
		setField("selWeightIndicator", arrData[strRowNo][23]);
		setField("txtNoOfCheckedBag", arrData[strRowNo][24]);
		setField("txtCheckedBagWeight", arrData[strRowNo][25]);
		setField("txtUncheckedBagWeight", arrData[strRowNo][26]);
		setField("hdnCoupNumber", arrData[strRowNo][27]);
		setField("hdnInfCoupNumber", arrData[strRowNo][28]);
			
	}
}




function resetPRL(){
	objOnFocus();
	getFieldByID("selTitle").focus();
	opener.setPageEdited(false,isPax);
		var mode=getText("hdnUIMode");
	if(mode=="ADD"){
		genAction="CA";
		clearControls();
	
	}if(mode=="EDIT"){
		genAction="CE";
		if (strGridRow<arrData.length && arrData[strGridRow][9] == getText("hdnPaxID")) {
			setField("hdnMode","");
			setField("selTitle",arrData[strGridRow][0]);
			setField("txtFirstName",arrData[strGridRow][1]);
			setField("txtLastName",arrData[strGridRow][2]);
			setField("txtPNR",arrData[strGridRow][3]);
			setField("selDest",arrData[strGridRow][4]);
			setField("selAddStatus",arrData[strGridRow][7]);
			setField("txtPNR",arrData[strGridRow][3]);
			setField("hdnPaxVersion",arrData[strGridRow][10]);
			setField("hdnPaxID",arrData[strGridRow][9]);
			setField("hdnProcessSta",arrData[strGridRow][8]);
			setField("selCC",arrData[strGridRow][13]);
			//setField("selPaxCat",arrData[strGridRow][14]);
			
			setField("hdnCurrentRowNum",strGridRow);
			changeStatus();
			setStatus(arrData[strGridRow][6],"selAddStatus");
		}else{
			clearControls();
			genAction="PL";
		}
	}
	controlBehaviour(initStat, genAction,prlStat,prlPaxStat);
	
	if(mode=="ADDTBA") {
		tbaClick();
	}
}

function objOnFocus(){
	opener.top[2].HidePageMessage();
}

function pageOnChange(){
		opener.setPageEdited(true,isPax);
	
}

function chkChanges(){
	
	if (opener.top[1].objTMenu.tabGetPageEidted(screenId)) {	
		return confirm("Changes will be lost! Do you wish to Continue?");
	}else{		
		return true;
	}
}


function windowclose(){
	closeAction="DC";
	if(prlDetailChanged(closeAction)){
		checkPRLDetailChanged();
	}
	
}

	function prlDetailChanged(action){
		var confirmed=true;
		if((action=="GD" || action=="DC" || action=="DA" || action=="DRC") && opener.isPrlPAXScreenEdited){
			confirmed=confirm("Changes will be lost! Do you wish to Continue?");
			if(confirmed){
				opener.setPageEdited(false, isPax);
			}
		}else if((action=="DC" ) && (opener.isPrlScreenEdited)){// Detail Close Clicked when header changed
			confirmed=confirm("Changes in PRL Header screen will be lost! Do you wish to Continue?");
			if(confirmed){
				opener.setPageEdited(false, isPax);
			}
		
		}else if(action=="DFC" && opener.isPrlPAXScreenEdited){// Window unlod
			opener.setPageEdited(false, isPax);
		}

		return confirmed;

	}

	function reloadPRLHEader(){
		if(closeAction!="R"){
			if(isChangedPAX == "YES"){
			//	confirmed=confirm("Changes in PRL Header screen will be lost! Do you wish to Continue?");
		//	if(confirmed){
					var strTxt=opener.top[1].objTMenu.tabGetValue(screenId);
					var arrTabData=new Array()
					if(strTxt!=""){
						 arrTabData=strTxt.split("~");
					}
					opener.location="showPRLProcessing.action?hdnMode=SEARCH&hdnUIMode=SEARCH"+ "&hdnRecNo="+arrTabData[4]+"&txtFrom="+arrTabData[0]+"&txtTo="+arrTabData[1]+"&selAirport="+arrTabData[2]+"&selProcessType="+arrTabData[3];
			}

		 // }
		}

	}

	
	function checkPRLDetailChanged(){
		if(isChangedPAX == "YES"){
			var strTxt=opener.top[1].objTMenu.tabGetValue(screenId);
			var arrTabData=new Array()
			if(strTxt!=""){
				 arrTabData=strTxt.split("~");
			}
			opener.location="showPRLProcessing.action?hdnMode=SEARCH&hdnUIMode=SEARCH"+ "&hdnRecNo="+arrTabData[4]+"&txtFrom="+arrTabData[0]+"&txtTo="+arrTabData[1]+"&selAirport="+arrTabData[2]+"&selProcessType="+arrTabData[3];
			window.close();
		} else {
			window.close();
		}
	}
	

function resetVariables() {
	prlDetailChanged("DFC");
}

function changeStatus(){
	if(getValue("selAddStatus")=="G"){
		Disable("txtPNR",true);
		setText("txtPNR","");
 	}else{
		Disable("txtPNR",false)
 	}
 	if(getValue("selAddStatus")=="N"){
		Disable("selCC",true);
		//Disable("selPaxCat",true);		
 	}else {
		Disable("selCC",false);
		//Disable("selPaxCat",false);
 	}
}

function paxChange(){
	var paxType = getFieldByID("selPaxType").value;
	if(paxType == 'IN'){
		Disable("selParent", false);
		document.getElementById("spnParentReq").style.display = "";
	} else {
		getFieldByID("selParent").value = '';
		Disable("selParent", true);
		document.getElementById("spnParentReq").style.display = "none";
	}	
}

function disableParent(){
	Disable("selParent", true);		
}

function deleteClick() {
	var status = confirm(deleteMessage);
	if (status) {
		setField("hdnMode", "DELETE");
		document.forms[0].submit();
		clearControls();
	}
}

function editClick() {
	genAction = "CE";
	setField("hdnMode", "EDIT");
	setField("hdnUIMode", "EDIT");
	controlBehaviour(initStat, genAction, prlStat, prlPaxStat);
}

function processSavePRL(mode) {

	setField("hdnMode", mode);
	setField("hdnPRLID", PRLId);
	setField("hdnDownloadDate", DownloadTS);
	setField("hdnFlightNo", FlightNo);
	setField("hdnFlightDate", FlightDate);
	setField("hdnOrigin", FromAirport);
	setField("hdnPRLProcessStatus", Status);
	validateFields();

	isChangedPAX = "YES";
	document.forms[0].hdnMode.value = mode;
	document.forms[0].submit();
}


function validateFields(){
	if (getValue("txtPassportNo") == '') {
		showWindowCommonError("Error", passportNoRqrd);
		return false;
	}
	if (getValue("txtPassportExpDate") == '') {
		showWindowCommonError("Error", passportExpDateRqrd);
		return false;
	}

	if (getText("txtCountryOfIssue") == "") {
		showWindowCommonError("Error", issueCountryRqrd);
		return false;
	}

	if (getText("txtSeatNo") == "") {
		showWindowCommonError("Error", seatNoRqrd);
		return false;
	}

	if (getText("txtNoOfCheckedBag") == "") {
		showWindowCommonError("Error", noOfcheckBagRqrd);
		return false;
	}

	if (getText("txtCheckedBagWeight") == "") {
		showWindowCommonError("Error", checkedBagWeightRqrd);
		return false;
	}
}
/**
 * AR - All reconciled when page load
 * @param initStat - Initial Status
 * @param genAction
 * @param prlStat
 * @param prlPaxStat
 */
function controlBehaviour(initStat, genAction, prlStat, prlPaxStat) {
	Enable("btnSave", (genAction == "CE"));
	Enable("btnReset", (genAction == "CE"));
	Enable("btnEdit", (genAction == "SR1" && prlPaxStat != "P" ));
	Enable("btnDelete", (genAction == "SR1" && prlPaxStat != "P" ));
	enableDataEntryControls(genAction);
}

function baggageInfoChange() {
	var weightIndicator = getFieldByID("selWeightIndicator").value;
	if (weightIndicator == "P") {
		setField("txtCheckedBagWeight", "");
		setField("txtUncheckedBagWeight", "");
		Disable("txtCheckedBagWeight", true);
		Disable("txtUncheckedBagWeight", true);
	} else {
		Disable("txtCheckedBagWeight", false);
		Disable("txtUncheckedBagWeight", false);
	}
}



