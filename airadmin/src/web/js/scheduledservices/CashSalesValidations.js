/*
 * Author : Menaka P. Wedige
 */
var screenId = "SC_SHDS_004";
var valueSeperator = "~";
var strRowData;
var strGridRow;
var intLastRec;

// On Page loading make sure that all the page input fields are cleared
function winOnLoad(strMsg, strMsgType) {
	objOnFocus();
	if (saveSuccess == 1) {
		alert("Record Successfully generated!");
		saveSuccess = 0;
	} else if (saveSuccess == 2) {
		alert("Record Successfully transfered!");
		saveSuccess = 0;
	}
	if (isAgentSelect) {
		var noUsers = ls.getNotSelectedDataWithGroup();
		if (noUsers == "" || noUsers == null) {
			showCommonError("Error", noUsersForAgent);
		}
		document.forms[0].hdnUIMode.value = "AGENTCHANGE";
		setVisible("spn1", true);
		if (strMsg != null && strMsgType != null)
			showCommonError(strMsgType, strMsg);
		// to set the Agent, From and To Dates after search
		if (agentCode != null) {
			setField("selAgent", agentCode);
		}
		if (fromDate != null) {
			setField("txtFrom", fromDate);
		}
		if (toDate != null) {
			setField("txtTo", toDate);
		}
	}

	// Keep the search variables remain same
	if (isSearchMode) {

		document.forms[0].hdnUIMode.value = "SEARCH";
		setVisible("spn1", true);
		if (strMsg != null && strMsgType != null)
			showCommonError(strMsgType, strMsg);
		// to set the Agent, From and To Dates and Selected Users after search
		if (agentCode != null) {
			setField("selAgent", agentCode);
		}
		if (fromDate != null) {
			setField("txtFrom", fromDate);
		}
		if (toDate != null) {
			setField("txtTo", toDate);
		}
		if (SelectedUsers != null) {
			ls.selectedData(SelectedUsers);
		}

	}

	if (top[1].objTMenu.focusTab == screenId)
		getFieldByID("txtFrom").focus();
	
	if(isShowReport){
		setVisible('btnViewReport',true);
		Disable("btnViewReport",true);
	}else {
		setVisible('btnViewReport',false);
	}
	
	Disable("btnGenTransfer", "true");	
	setPageEdited(false);
}

// on Grid Row click
function RowClick(strRowNo) {
	objOnFocus();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;
		setField("hdnGridRow", strRowNo);
		setField("hdnDateOfSale", arrCashData[strRowNo][1]);
		setField("hdnFromDate", arrCashData[strRowNo][3]);
		setField("hdnToDate", arrCashData[strRowNo][4]);
		if (arrCashData[strRowNo][9] == "Y") {
			getFieldByID("btnGenTransfer").value = "Re-transfer";
		} else if (arrCashData[strRowNo][9] == "N") {
			showCommonError("Error", seeLogEntry);
			getFieldByID("btnGenTransfer").value = "Transfer";
		} else {
			getFieldByID("btnGenTransfer").value = "Generate";
		}
		setField("hdnVersion", arrCashData[strRowNo][6]);
		setPageEdited(false);
		setField("hdnMode", "");
	}
}

function AgentChange() {
	objOnFocus();
	pageOnChange();
	document.forms[0].hdnUIMode.value = "AGENTCHANGE";
	document.forms[0].hdnMode.value = "";
	document.forms[0].target = "_self";
	document.forms[0].submit();
}

function fromDateValidation(cntfield) {
	if (!dateChk(cntfield)) {
		showCommonError("Error", fromDateInvalid);
		if (top[1].objTMenu.focusTab == screenId)
			getFieldByID(cntfield).focus();
		return;
	}
}

function toDateValidation(cntfield) {
	if (!dateChk(cntfield)) {
		showCommonError("Error", toDateInvalid);
		if (top[1].objTMenu.focusTab == screenId)
			getFieldByID(cntfield).focus();
		return;
	}
}

function searchData() {
	objOnFocus();
	if (isEmpty(getText("txtFrom"))) {
		showCommonError("Error", fromDateRqrd);
		if (top[1].objTMenu.focusTab == screenId)
			getFieldByID("txtFrom").focus();
		return;
	}
	if (isEmpty(getText("txtTo"))) {
		showCommonError("Error", toDateRqrd);
		if (top[1].objTMenu.focusTab == screenId)
			getFieldByID("txtTo").focus();
		return;
	}

	fromDateValidation('txtFrom');
	toDateValidation('txtTo');

	var StartD = formatDate(getVal("txtFrom"), "dd/mm/yyyy", "dd-mmm-yyyy");
	var today = formatDate(getVal("hdnCurrentDate"), "dd/mm/yyyy",
			"dd-mmm-yyyy");
	var EndD = formatDate(getVal("txtTo"), "dd/mm/yyyy", "dd-mmm-yyyy");

	// compare from date and to date
	if (compareDates(StartD, EndD, '>')) {
		showCommonError("Error", todateLessthanFromDate);
		if (top[1].objTMenu.focusTab == screenId)
			getFieldByID("txtTo").focus();
		return false;
	}
	// Compare to date and current date
	if (compareDates(today, EndD, '<=')) {
		showCommonError("Error", todateLessthanCurrentDate);
		if (top[1].objTMenu.focusTab == screenId)
			getFieldByID("txtTo").focus();
		return false;
	}

	if (document.getElementById("selAgent").value == "-1") {
		showCommonError("Error", agentRqrd);
		if (top[1].objTMenu.focusTab == screenId)
			getFieldByID("selAgent").focus();
		return;
	}
	if (ls.getselectedData() == "") {
		showCommonError("Error", userRqrd);
		if (top[1].objTMenu.focusTab == screenId)
			getFieldByID("lstUsers").focus();
		return;
	}

	setField("hdnUsers", trim(ls.getselectedData()));
	document.forms[0].hdnUIMode.value = "SEARCH";
	document.forms[0].hdnMode.value = "";
	document.forms[0].target = "_self";
	top[2].ShowProgress();
	document.forms[0].submit();
}

function generateOrTransfer() {
	objOnFocus();
	setField("hdnRecNo", getTabValues(screenId, valueSeperator, "intLastRec"));
	setTabValues(screenId, valueSeperator, "strGridRow", strGridRow); // Save
																		// the
																		// grid
																		// Row
	var strArray = new Array();
	var strDateOfsSalesArray = new Array();
	var count = 0;
	strDateOfsSalesArray[count] = getVal("hdnDateOfSale");
	var selscedRowId = objDG.getSelectedColumn(5);
	for ( var i = 0; i < selscedRowId.length; i++) {

		if ((selscedRowId[i][1]) && ((arrCashData[i][6] != "T"))
				&& ((arrCashData[i][6] == "G") || (arrCashData[i][6] == ""))) {
			document.forms[0].hdnMode.value = "GENERATE";
			strArray[count] = arrCashData[i][1] + "," + arrCashData[i][2];
			strDateOfsSalesArray[count] = arrCashData[i][1];
			count++
		} else if ((selscedRowId[i][1]) && ((arrCashData[i][6] == "T"))
				&& ((arrCashData[i][6] != "G") || (arrCashData[i][6] != ""))) {
			document.forms[0].hdnMode.value = "TRANSFER";
			strArray[count] = arrCashData[i][1] + "," + arrCashData[i][2];
			strDateOfsSalesArray[count] = arrCashData[i][1];
			count++
		}
	}
	if (strArray.length == 0) {
		Disable("btnGenTransfer", "true");
		getFieldByID("btnGenTransfer").value = "Generate/Transfer";
		showCommonError("Error", "please select a record");
		return;
	}
	if (arrCashData[strGridRow][9] == "U" || arrCashData[strGridRow][9] == "") {
		var confirmStr = confirm(generateRecoredCfrm)
		if (!confirmStr)
			return;
		document.forms[0].hdnMode.value = "GENERATE";
	} else if (arrCashData[strGridRow][9] == "N") {
		var confirmStr = confirm(transferRecoredCfrm)
		if (!confirmStr)
			return;
		document.forms[0].hdnMode.value = "TRANSFER";
	} else if (arrCashData[strGridRow][9] == "Y") {
		var confirmStr = confirm(reTransferRecoredCfrm)
		if (!confirmStr)
			return;
		document.forms[0].hdnMode.value = "TRANSFER";
	}
	document.forms[0].hdnSelectedRows.value = strArray;
	document.forms[0].hdnSelectedDates.value = strDateOfsSalesArray;
	setField("hdnUsers", trim(ls.getselectedData()));
	top[2].ShowProgress();
	document.forms[0].target = "_self";
	document.forms[0].submit();
	setPageEdited(false);
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function pageOnChange() {
	top[1].objTMenu.tabPageEdited(screenId, true);
}
function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

// Calender
var objCal1 = new Calendar("spnCalendarDG1");

objCal1.onClick = "setDate";
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFrom", strDate);
		break;
	case "1":
		setField("txtTo", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 130;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function viewReport (){
	var strArray = new Array();
	var strDateOfsSalesArray = new Array();
	var count = 0;
	strDateOfsSalesArray[count] = getVal("hdnDateOfSale");
	var selscedRowId = objDG.getSelectedColumn(5);
	for ( var i = 0; i < selscedRowId.length; i++) {
		if ( (selscedRowId[i][1]) && ( (arrCashData[i][6] == "G") || (arrCashData[i][6] == "T") 
						|| (arrCashData[i][6] == "") ) ) {
			strDateOfsSalesArray[count] = arrCashData[i][1];
			strArray[count] = arrCashData[i][1] + "," + arrCashData[i][2];	
			count++;
		}
	}
	if (strArray.length == 0) {
		showCommonError("Error", "please select a record");
		return;
	}		
	document.forms[0].hdnSelectedRows.value = strArray;
	document.forms[0].hdnSelectedDates.value = strDateOfsSalesArray;
	setField("hdnUsers", trim(ls.getselectedData()));
	
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
	top[0].objWindow = window.open("about:blank", "CWindow", strProp);	
	document.forms[0].target = "CWindow";
	document.forms[0].hdnMode.value = "REPORT";	
	document.forms[0].submit();
	top[2].HidePageMessage();
}

function displayDownLoadReportBtn (){	
	if(isShowReport){
		Disable("btnViewReport", false);
	}else {
		Disable("btnViewReport",true);
	}
}
