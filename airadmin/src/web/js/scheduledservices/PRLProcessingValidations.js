var screenId = "SC_SHDS_0035";
var valueSeperator = "~";
var strRowData;
var strGridRow = -1;
var intLastRec;
var initialArr = new Array();
var currentPRLData = "";
var currentData = "";
initialArr = arrData;
var closeClicked = false;
// var confirmed=false;
var prlHEaderAction;





var processTypeArr = new Array("All", "Parsed", "Processed", "Unparsed");
var processTypeOptArr = new Array("All", "P", "R", "U");

var processStatusTypeArr = new Array("", "Parsed", "Processed", "Unparsed");
var processStatusTypeOptArr = new Array("", "P", "R", "U");

var initStat = "";
var genAction = "";
var prlStat = "";
var isPRLEmpty = true;
var prlPaxStat = "";
var isPrlScreenEdited = false;
var isPrlPAXScreenEdited = false;
var prlId = "";
var isPRL = false;

// On Page loading make sure that all the page input fields are cleared
function winOnLoad(strMsg, strMsgType) {
	objOnFocus();
	if (strMsg != null && strMsgType != null)
		showCommonError(strMsgType, strMsg);
	getFieldByID("txtFrom").focus();
	genAction = "PL";
	setPageEdited(false, isPRL);
	setField("hdnAirport", getText("selAirport"));
	top[2].HideProgress();
	populateCombo(processTypeArr, "selProcessType", processTypeOptArr);	
	populateCombo(processStatusTypeArr, "selStatus", processStatusTypeOptArr);	
	getData();

	if (arrFormData != null && arrFormData.length > 0) {
		setPageEdited(true, isPRL);
		setField("hdnPRLData", arrFormData[0]);
		setField("hdnCurrentPrl", arrFormData[1]);
		currentPRLData = arrFormData[1];
		currentData = arrFormData[1];
		setField("hdnVersion", arrFormData[2]);
		setField("hdnCurrentRowNum", arrFormData[3]);
		strGridRow = arrFormData[3];
		setField("txtDownloadTS", arrFormData[4]);
		setField("txtFlightNo", arrFormData[5]);
		setField("txtFlightDateTime", arrFormData[6]);
		setField("selDest", arrFormData[7]);
		setField("txFromAddress", arrFormData[8]);
		setField("selStatus", arrFormData[9]);
		if (arrFormData[10] != "0") {
			DivWrite("spnPRLID", arrFormData[10]);
		}
		setField("hdnPRLID", arrFormData[10]);
		setField("hdnMode", arrFormData[11]);
		setField("hdnUIMode", arrFormData[12]);
		setField("txtDownloadTime", arrFormData[13]);
		setField("txFlightTime", arrFormData[14]);
		if (arrFormData[15] != null && arrFormData[15] != "") {
			setField("txtaRulesCmnts", arrFormData[15]);
			DivWrite("spnContent", arrFormData[15]);
		}
		setField("hdnPaxCount", arrFormData[16]);
		if (arrFormData[12] == "ADD") {
			genAction = "CA";
		}
		if (arrFormData[12] == "EDIT") {
			genAction = "CE";
		}
		if (arrFormData[11] == "PROCESS") {
			setPageEdited(false, isPRL);
			genAction = "SR";
			if (arrFormData[4] != "" && arrFormData[14] != ""
					&& arrFormData[5] != "" && arrFormData[7] != "") {
				isPRLEmpty = false;
			} else {
				isPRLEmpty = true;
			}
			prlStat = "P";
		}

	}

	if (saveSuccess == 1) {
		alert("Record Successfully Processed!");
	}
	if (saveSuccess == 3) {
		alert("Record Processed with Errors!");
	}

	controlBehaviour(initStat, genAction, prlStat, prlPaxStat, isPRLEmpty);
	
	PRLContentPopUpData();
}

function gridNavigations(intRecNo, intErrMsg) {

	prlHEaderAction = "GN";
	if (prlHeaderChanged(prlHEaderAction)) {
		setField("hdnMode", "PAGING");
		setField("hdnRecNo", intRecNo);
		setData();
		if (intErrMsg != "") {
			top[2].objMsg.MessageText = "<li> " + intErrMsg;
			top[2].objMsg.MessageType = "Error";
			showCommonError("Error", intErrMsg);
		} else {
			document.forms[0].submit();
			objDG.seqStartNo = intRecNo;
		}
		setPageEdited(false, isPRL);
	}

}

function setData() {
	var searchData = getText("txtFrom") + "~" + getText("txtTo") + "~"
			+ getText("selAirport") + "~" + getValue("selProcessType") + "~"
			+ getText("hdnRecNo");
	top[1].objTMenu.tabSetValue(screenId, searchData);
}

function testDate(dt) {
	var validated = true;

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10) {
		dtCM = "0" + dtCM
	}
	if (dtCD < 10) {
		dtCD = "0" + dtCD
	}
	var strSysDate = systemDate;
	var valid = dateChk(strSysDate);

	var tempDay = dt.substring(0, 2);
	var tempMonth = dt.substring(3, 5);
	var tempYear = dt.substring(6, 10);
	var tempOStartDate = (tempYear + tempMonth + tempDay);

	tempDay = strSysDate.substring(0, 2);
	tempMonth = strSysDate.substring(3, 5);
	tempYear = strSysDate.substring(6, 10);
	var strSysODate = (tempYear + tempMonth + tempDay);

	if (valid) {
		// if(CheckDates(dt,strSysDate)){
		if (tempOStartDate >= strSysODate) {
			validated = false;
		}
	}
	return validated;

}

function getData() {
	var strTxt = top[1].objTMenu.tabGetValue(screenId);
	if (strTxt != "") {
		var arrTabData = strTxt.split("~");
		setField("txtFrom", arrTabData[0]);
		setField("txtTo", arrTabData[1]);
		setField("selAirport", arrTabData[2]);
		setField("selProcessType", arrTabData[3]);

	}
}

function populateCombo(dataArr, controlName, optArr) {
	var control = document.getElementById(controlName);
	for ( var t = 0; t < dataArr.length; t++) {
		control.options[t] = new Option(dataArr[t], optArr[t]);
	}

}

function ReceivedPRLRowClick(strRowNo) {
	objOnFocus();
	prlHEaderAction = "HG";
	if (prlHeaderChanged(prlHEaderAction)) {
		clearControls();
		genAction = "SR"; // Row selected in Grid 1
		prlStat = arrData[strRowNo][6]; 
		var strDownloadTs = arrData[strRowNo][2];
		if (arrData[strRowNo][1] != "" && strDownloadTs.length == 16
				&& arrData[strRowNo][2] != "" && arrData[strRowNo][11] != "") {
			isPRLEmpty = false;
		} else {
			isPRLEmpty = true;
		}

		strGridRow = strRowNo;
		setField("hdnCurrentRowNum", strRowNo);

		DivWrite("spnPRLID", arrData[strRowNo][3]);
		var dateFull = trim(arrData[strRowNo][0]);
		var date = "";
		var time = "";
		if (dateFull.indexOf(" ") != -1) {
			date = dateFull.substring(0, dateFull.indexOf(" "));
			time = dateFull.substring(dateFull.indexOf(" "));
		} else {
			date = dateFull;
			time = "";
		}
		setField("txtDownloadTS", trim(date));
		setField("txtDownloadTime", trim(time));
		setField("txtFlightNo", arrData[strRowNo][1]);

		dateFull = trim(arrData[strRowNo][2]);
		if (dateFull.indexOf(" ") != -1) {
			date = dateFull.substring(0, dateFull.indexOf(" "));
			time = dateFull.substring(dateFull.indexOf(" "));
		} else {
			date = dateFull;
			time = "";
		}

		setField("txtFlightDateTime", trim(date));
		setField("txFlightTime", trim(time));
		setField("selDest", arrData[strRowNo][11]);
		setField("txFromAddress", arrData[strRowNo][12]);
		setField("selStatus", arrData[strRowNo][6]);
		setField("hdnPRLID", arrData[strRowNo][3]);
		setField("hdnVersion", arrData[strRowNo][13]);
		prlId = arrData[strRowNo][3];
		setField("hdnCurrentPrl", arrData[strRowNo]);
		setField("txtaRulesCmnts", arrData[strRowNo][5]);
		DivWrite("spnContent", arrData[strRowNo][5]);
		setField("hdnPaxCount", arrData[strRowNo][14]);
		setField("txtPRLContent",arrData[strRowNo][5]);
		controlBehaviour(initStat, genAction, prlStat, prlPaxStat, isPRLEmpty);
		
		setPageEdited(false, isPRL);
	}
}

function addClick() {
	prlHEaderAction = "HA";
	if (prlHeaderChanged(prlHEaderAction)) {
		clearControls();
		genAction = "CA";
		setField("hdnVersion", "-1");
		setField("selStatus", "U");
		setField("hdnMode", "ADD");
		setField("hdnUIMode", "ADD");
		controlBehaviour(initStat, genAction, prlStat, prlPaxStat, isPRLEmpty);
		getFieldByID("txtDownloadTS").focus();
	}

}

function deleteClick() {
	var status = confirm(deleteMessage);
	genAction = "CD";
	if (status) {
		setField("hdnMode", "DELETE");
		setField("hdnUIMode", "DELETE");
		enableDataEntryControls("CD");
		Disable("selStatus", false);
		setField("hdnRecNo", "1");
		setData();
		document.forms[0].submit();
	}

}

function makeParse() {
	setField("hdnMode", "PARSE");
	setField("hdnUIMode", "PARSE");
	enableDataEntryControls("CD");
	Disable("selStatus", false);
	setField("hdnRecNo", "1");
	setData();
	document.forms[0].submit();
}

function editClick() {
	genAction = "CE";
	setField("hdnMode", "EDIT");
	setField("hdnUIMode", "EDIT");
	controlBehaviour(initStat, genAction, prlStat, prlPaxStat, isPRLEmpty);
	getFieldByID("txtDownloadTS").focus();

}

function clearControls() {
	setField("txtDownloadTS", getVal("hdnCurrentDate"));
	DivWrite("spnPRLID", "");
	setField("selStatus", "");
	setField("txFromAddress", "");
	setField("txtFlightNo", "");
	setField("selDest", "-1");
	setField("hdnPRLID", "");
	setField("txtFlightDateTime", "");
	setField("txtDownloadTime", "00:00");
	setField("txFlightTime", "");
	setField("txtaRulesCmnts", "");
	DivWrite("spnContent", "");

}

/**
 *
 * @param initStat
 * @param genAction
 * @param prlStat
 * @param prlPaxStat
 * @param isPRLEmpty
 */
function controlBehaviour(initStat, genAction, prlStat, prlPaxStat, isPRLEmpty) {

	var paxCount = getText("hdnPaxCount");
	Enable(
		"btnDelete",
		((genAction == "CD" || genAction == "SR" || genAction == "CE") && (prlStat != "R")));
	Enable("btnReset", (genAction == "CA" || genAction == "CE"));
	Enable("btnProcess", (genAction == "SR") && (prlStat == "N" || prlStat == "P" ||prlStat == "E" || prlStat == "W"));
	Enable("btnPRLDetails", (genAction == "SR" || genAction == "CE")
			&& (isPRLEmpty == false));
	Enable("btnEditContent",(genAction=="SR" && prlStat=="P"));
	enableDataEntryControls(genAction);

}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
		setPageEdited(false, isPRL);
		setPageEdited(false, true);
	}
}

function closeParentAndChildWindow() {
	prlHEaderAction = "HC";
	if (prlHeaderChanged(prlHEaderAction)) {
		if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
			top[0].objWindow.close();
		}
		top[1].objTMenu.tabRemove(screenId);
	}
}

function prlHeaderChanged(action) {
	var confirmed = true;
	if ((action == "HC") && (isPrlScreenEdited && isPrlPAXScreenEdited)) { // Header
																			// close
																			// clicked
																			// when
																			// both
																			// screens
																			// changed
		confirmed = top.loadCheck(getPageEdited());
		if (confirmed) {
			setPageEdited(false, isPRL);
			setPageEdited(false, true);
		}
	} else if ((action == "HC" || action == "HG" || action == "HS"
			|| action == "VD" || action == "GN" || action == "HA")
			&& (isPrlScreenEdited)) {
		confirmed = top.loadCheck(getPageEdited());
		if (confirmed) {
			setPageEdited(false, isPRL);
			// setPageEdited(false, true);
		}
	} else if ((action == "HC" || action == "HS" || action == "VD"
			|| action == "GN" || action == "HSV")
			&& (isPrlPAXScreenEdited)) { 
		confirmed = confirm("Changes will be lost in PRL Detail screen! Do you wish to Continue?");
		if (confirmed) {
			setPageEdited(false, isPRL);
			setPageEdited(false, true);
		}
	}
	return confirmed;
}

function viewPRLDetails() {

	var currentPRL = "";
	var prl = getText("hdnPRLID");
	prlHEaderAction = "VD";
	if (prlHeaderChanged(prlHEaderAction)) {

		if (strGridRow != -1) {
			setField("hdnCurrentPrl", arrData[strGridRow]);
			currentPRLData = arrData[strGridRow];
			currentData = arrData[strGridRow][0] + "," + arrData[strGridRow][1]
					+ "," + arrData[strGridRow][2] + ","
					+ arrData[strGridRow][3] + "," + arrData[strGridRow][4]
					+ ", ," + arrData[strGridRow][6] + ","
					+ arrData[strGridRow][7] + "," + arrData[strGridRow][8]
					+ "," + arrData[strGridRow][9] + ","
					+ arrData[strGridRow][10] + "," + arrData[strGridRow][11]
					+ "," + arrData[strGridRow][12] + ","
					+ arrData[strGridRow][13];
			currentPRL = arrData[strGridRow][0] + "," + arrData[strGridRow][1]
					+ "," + arrData[strGridRow][2] + ","
					+ arrData[strGridRow][3] + "," + arrData[strGridRow][11]
					+ "," + arrData[strGridRow][6];

		}

		if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
			top[0].objWindow.close();
		}
		setPageEdited(false, isPRL);
		var strFilename = "showPRLDetailProcessing.action?hdnCurrentPrl="
				+ currentData + "&hdnPRLID=" + prl + "&hdnState=true"; // showAgentwiseFC.do
		var intHeight = 700;
		var intWidth = 900;
		strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
				+ intWidth
				+ ',height='
				+ intHeight
				+ ',resizable=no,top='
				+ ((window.screen.height - intHeight) / 2)
				+ ',left='
				+ (window.screen.width - intWidth) / 2;
		top[0].objWindow = window.open(strFilename, "PRLDetails", strProp);

	}

	setPageEdited(false, isPRL);
}

function enableDataEntryControls(action) {

	if (action == "SR" || action == "PL") {
		Disable("selStatus", true);
		Disable("txtFlightNo", true);
		Disable("txtDownloadTS", true);
		Disable("txFromAddress", true);
		Disable("selDest", true);
		Disable("txtFlightDateTime", true);
		Disable("txtDownloadTime", true);
		Disable("txFlightTime", true);
	} else {
		// Disable("selStatus",false);
		Disable("selStatus", true);
		Disable("txtFlightNo", false);
		Disable("txtDownloadTS", false);
		Disable("txFromAddress", false);
		Disable("selDest", false);
		Disable("txtFlightDateTime", false);
		Disable("txtDownloadTime", false);
		Disable("txFlightTime", false);

	}

}

function Enable(strControlId, enable) {
	Disable(strControlId, !enable);
}

function setStatus(strSelStatus, ctrlName) {
	var control = document.getElementById(ctrlName);

	for ( var t = 0; t < (control.length); t++) {
		if (control.options[t].text == strSelStatus) {
			control.options[t].selected = true;
			break;
		}
	}

}

function fromDateValidation(cntfield) {
	if (!dateChk(cntfield)) {
		showCommonError("Error", fromDateInvalid);
		if (top[1].objTMenu.focusTab == screenId)
			getFieldByID(cntfield).focus();
		return;
	}
}

function toDateValidation(cntfield) {
	if (!dateChk(cntfield)) {
		showCommonError("Error", toDateInvalid);
		if (top[1].objTMenu.focusTab == screenId)
			getFieldByID(cntfield).focus();
		return;
	}
}

function searchData() {
	objOnFocus();
	prlHEaderAction = "HS";
	if (prlHeaderChanged(prlHEaderAction)) {		
		
		fromDateValidation('txtFrom');
		toDateValidation('txtTo');
		var StartD = "";
		var EndD = "";
		var today = formatDate(getVal("hdnCurrentDate"), "dd/mm/yyyy",
				"dd-mmm-yyyy");

		if (!isEmpty(getText("txtFrom"))) {
			StartD = formatDate(getVal("txtFrom"), "dd/mm/yyyy", "dd-mmm-yyyy");
		}
		if (!isEmpty(getText("txtTo"))) {
			EndD = formatDate(getVal("txtTo"), "dd/mm/yyyy", "dd-mmm-yyyy");
		}
		// Compare to date/From date and current date
		if (!isEmpty(getText("txtFrom")) && !dateValidDate(getText("txtFrom"))) {
			showCommonError("Error", fromDateInvalid);
			if (top[1].objTMenu.focusTab == screenId)
				getFieldByID("txtFrom").focus();
			return false;
		}
		if (!isEmpty(getText("txtTo")) && !dateValidDate(getText("txtTo"))) {
			showCommonError("Error", toDateInvalid);
			if (top[1].objTMenu.focusTab == screenId)
				getFieldByID("txtFrom").focus();
			return false;
		}
		if (!isEmpty(getText("txtFrom")) && compareDates(today, StartD, '<')) {
			showCommonError("Error", fromDateLessthanCurrentDate);
			if (top[1].objTMenu.focusTab == screenId)
				getFieldByID("txtFrom").focus();
			return false;
		}
		if (!isEmpty(getText("txtTo")) && compareDates(today, EndD, '<')) {
			showCommonError("Error", toDateLessthanToday);
			if (top[1].objTMenu.focusTab == screenId)
				getFieldByID("txtTo").focus();
			return false;
		}
		if (!isEmpty(getText("txtTo")) && compareDates(StartD, EndD, '>')) {
			showCommonError("Error", todateLessthanFromDate);
			if (top[1].objTMenu.focusTab == screenId)
				getFieldByID("txtTo").focus();
			return false;
		}

		setData();
		top[0].initializeVar();
		document.forms[0].hdnUIMode.value = "SEARCH";
		document.forms[0].hdnMode.value = "SEARCH";
		top[2].ShowProgress();
		setField("hdnRecNo", "1");
		document.forms[0].submit();
	}

}

function validateDatetime(status, dateFull, rowCount, index) {
	var validated = false;
	var fulFormattedVal = "";
	var errorMsg = "";
	if (dateFull != "") {
		fulFormattedVal = checkDate(dateFull);
		if (fulFormattedVal.indexOf(" ") != -1) {
			date = trim(fulFormattedVal.substring(0, fulFormattedVal
					.indexOf(" ")));
			time = trim(fulFormattedVal.substring(fulFormattedVal.indexOf(" ")));
		} else {
			date = fulFormattedVal;
			time = "";
		}
		objDG.setCellValue(rowCount - 1, index, fulFormattedVal);
		arrData[rowCount - 1][index] = fulFormattedVal;
		if (status == "flightTS") {
			if (date != "" && !dateValidDate(date)) {
				errorMsg = flightDateFormatInvalid;
			} else if (time != "" && !IsValidTime(time)) {
				errorMsg = flightTimeFormatInvalid;
			}
		} else {
			if (date != "" && !dateValidDate(date)) {
				errorMsg = downloadTSDateFormatInvalid;
			} else if (time != "" && !IsValidTime(time)) {
				errorMsg = downloadTSTimeFormatInvalid;
			}
		}
	}
	return errorMsg;
}

function validateDatetimeControls(status, dateFull) {
	var validated = false;
	var fulFormattedVal = "";
	var errorMsg = "";
	if (dateFull != "") {
		fulFormattedVal = checkDate(dateFull);
		if (fulFormattedVal.indexOf(" ") != -1) {
			date = trim(fulFormattedVal.substring(0, fulFormattedVal
					.indexOf(" ")));
			time = trim(fulFormattedVal.substring(fulFormattedVal.indexOf(" ")));
		} else {
			date = fulFormattedVal;
			time = "";
		}
		if (status == "flightTS") {
			if (date != "" && !dateValidDate(date)) {
				errorMsg = flightDateFormatInvalid;
			} else if (time != "" && !IsValidTime(time)) {
				errorMsg = flightTimeFormatInvalid;
			}
		} else {
			if (date != "" && !dateValidDate(date)) {
				errorMsg = downloadTSDateFormatInvalid;
			} else if (time != "" && !IsValidTime(time)) {
				errorMsg = downloadTSTimeFormatInvalid;
			}
		}
	}
	return errorMsg;
}

function compareDays(downloadTime, flightTime) {
	var downloadTm = trim(downloadTime);
	var flightTm = trim(flightTime);
	var downloadDate;
	var downloadTime;
	var flightDate;
	var flightTime;
	var StartD;
	var EndD;
	var errorMsg = "";
	if (downloadTm != "" && flightTm != "") {
		if (downloadTm.indexOf(" ") != -1) {
			downloadDate = trim(downloadTm
					.substring(0, downloadTm.indexOf(" ")));
			downloadTime = trim(downloadTm.substring(downloadTm.indexOf(" ")));
			var tempDay = downloadDate.substring(0, 2);
			var tempMonth = downloadDate.substring(3, 5);
			var tempYear = downloadDate.substring(6, 10);
			StartD = tempDay + tempMonth + tempYear;
		} else {
			downloadDate = downloadTm;
			flightTime = "";
		}

		if (flightTm.indexOf(" ") != -1) {
			flightDate = trim(flightTm.substring(0, flightTm.indexOf(" ")));
			flightTime = trim(flightTm.substring(flightTm.indexOf(" ")));
			var tempDay = flightDate.substring(0, 2);
			var tempMonth = flightDate.substring(3, 5);
			var tempYear = flightDate.substring(6, 10);
			EndD = tempDay + tempMonth + tempYear;
		} else {
			flightDate = flightTm;
			flightTime = "";
		}

		if (StartD < EndD) {
			errorMsg = DownloadTSGreaterFlightTs;
		} else if (EndD == StartD
				&& compareTime(downloadTime, flightTime) == false) {
			errorMsg = DownloadTSGreaterFlightTs;
		}
	}
	return errorMsg;

}

function compareTime(outTmFrom, outTmTo) {
	var outFTm = parseFloat(outTmFrom.replace(":", "."));
	var outTTm = parseFloat(outTmTo.replace(":", "."));
	if (outFTm <= outTTm) {
		return false;
	} else {
		return true;
	}
}

function processSavePRL(mode) {
	objOnFocus();
	var prlData = "";
	var validateControls = false;
	var downloadDate = getText("txtDownloadTS");
	var downloadTime = trim(getText("txtDownloadTime"));
	prlHEaderAction = "HSV";
	if (prlHeaderChanged(prlHEaderAction)) {

		var flightDate = getText("txtFlightDateTime");
		var flightTime = trim(getText("txFlightTime"));
		var today = formatDate(getVal("hdnCurrentDate"), "dd/mm/yyyy",
				"dd-mmm-yyyy");
		var currentTime = getVal("hdnCurrentTime");		
		var StartD = "";
		var EndD = "";
		var start = "";
		var end = "";
		if (!isEmpty(getText("txtDownloadTS"))) {
			StartD = formatDate(getVal("txtDownloadTS"), "dd/mm/yyyy",
					"dd-mmm-yyyy");
			var tempDay = downloadDate.substring(0, 2);
			var tempMonth = downloadDate.substring(3, 5);
			var tempYear = downloadDate.substring(6, 10);
			start = tempDay + tempMonth + tempYear;
		}
		if (!isEmpty(getText("txtFlightDateTime"))) {
			EndD = formatDate(getVal("txtFlightDateTime"), "dd/mm/yyyy",
					"dd-mmm-yyyy");
			var tempDay = flightDate.substring(0, 2);
			var tempMonth = flightDate.substring(3, 5);
			var tempYear = flightDate.substring(6, 10);
			end = tempDay + tempMonth + tempYear;
		}
		if (!document.getElementById("selDest").disabled) {
			if (downloadDate == "") {
				showCommonError("Error", downloadTSRqrd);
				getFieldByID("txtDownloadTS").focus();
				return false;
			}
			if (downloadTime == "") {
				showCommonError("Error", downloadTimeRqrd);
				getFieldByID("txtDownloadTime").focus();
				return false;
			}
			if (!dateValidDate(downloadDate)) {
				showCommonError("Error", downloadTSTimeFormatInvalid);
				getFieldByID("txtDownloadTS").focus();
				return false;
			}
			if (!IsValidTime(downloadTime)) {
				showCommonError("Error", downloadTSTimeFormatInvalid);
				getFieldByID("txtDownloadTime").focus();
				return false;

			}
			if (flightDate != "" && !dateValidDate(flightDate)) {
				showCommonError("Error", flightTimeFormatInvalid);
				getFieldByID("txtFlightDateTime").focus();
				return false;

			}
			if (flightTime != "" && !IsValidTime(flightTime)) {
				showCommonError("Error", flightTimeFormatInvalid);
				getFieldByID("txFlightTime").focus();
				return false;

			}
			if (downloadDate != "" && compareDates(today, StartD, '<')) {
				showCommonError("Error", DownloadTSlessThanToday);
				if (top[1].objTMenu.focusTab == screenId)
					getFieldByID("txtDownloadTS").focus();
				return false;

			}
			if (flightDate != "" && compareDates(StartD, EndD, '<')) {
				showCommonError("Error", DownloadTSGreaterFlightTs);
				if (top[1].objTMenu.focusTab == screenId)
					getFieldByID("txtDownloadTS").focus();
				return false;

			}
			if (flightTime != "" && start == end
					&& compareTime(currentTime, flightTime) == false) {
				showCommonError("Error", FlightTSGreaterFlightTs);
				if (top[1].objTMenu.focusTab == screenId)
					getFieldByID("txFlightTime").focus();
				return false;
			}
			if (flightTime != "" && start == end
					&& compareTime(downloadTime, flightTime) == false) {
				showCommonError("Error", DownloadTSGreaterFlightTs);
				if (top[1].objTMenu.focusTab == screenId)
					getFieldByID("txFlightTime").focus();
				return false;
			} else {
				validateControls = true;
			}
		} else {
			validateControls = true;

		}

		if (validateControls) {
			if (strGridRow != -1) {
				setField("hdnCurrentPrl", arrData[strGridRow]);
				currentPRLData = arrData[strGridRow];
			}
			setField("hdnPRLData", prlData.substring(0, prlData.length - 2));
			document.forms[0].hdnMode.value = mode;
			if (mode == "PROCESS") {
				enableDataEntryControls("CE");
			}
			setData();
			top[2].ShowProgress();
			setField("hdnRecNo", "1");
			Disable("selStatus", false);
			document.forms[0].submit();

		}
	}

}
function clearAllControls() {
	clearControls();
	setField("txtDownloadTS", "");
	setField("txtDownloadTime", "");

}
function resetPRL() {
	objOnFocus();
	getFieldByID("txtDownloadTS").focus();
	var mode = getText("hdnUIMode");
	if (mode == "ADD") {
		genAction = "CA";
		clearControls();
		setField("selStatus", "U");

	}
	if (mode == "EDIT") {
		genAction = "CE";
		if (strGridRow < arrData.length
				&& arrData[strGridRow][3] == getText("hdnPRLID")) {
			var dateFull = trim(arrData[strGridRow][0]);
			var date = "";
			var time = "";
			if (dateFull.indexOf(" ") != -1) {
				date = dateFull.substring(0, dateFull.indexOf(" "));
				time = dateFull.substring(dateFull.indexOf(" "));
			} else {
				date = dateFull;
				time = "";
			}
			setField("txtDownloadTS", trim(date));
			setField("txtDownloadTime", trim(time));

			dateFull = trim(arrData[strGridRow][2]);
			if (dateFull.indexOf(" ") != -1) {
				date = dateFull.substring(0, dateFull.indexOf(" "));
				time = dateFull.substring(dateFull.indexOf(" "));
			} else {
				date = dateFull;
				time = "";
			}

			setField("txtFlightDateTime", trim(date));
			setField("txFlightTime", trim(time));
			setField("txtFlightNo", arrData[strGridRow][1]);
			setField("selDest", arrData[strGridRow][11]);
			setField("txFromAddress", arrData[strGridRow][12]);
			setField("selStatus", arrData[strGridRow][6]);
			setField("hdnPRLID", arrData[strGridRow][3]);
			setField("hdnVersion", arrData[strGridRow][13]);
		} else {
			clearAllControls();
			genAction = "PL";
		}
	}

	controlBehaviour(initStat, genAction, prlStat, prlPaxStat, isPRLEmpty);
	setPageEdited(false, isPRL);
	// gridLoadCheck();

}

function objOnFocus() {
	top[2].HidePageMessage();
}

function pageOnChange() {
	setPageEdited(true, isPRL);
}

function setPageEdited(isEdited, isPrlPAXScreen) {
	var isEitherOneEdited = false;
	if (isPrlPAXScreen) {
		isPrlPAXScreenEdited = isEdited;
	} else {
		isPrlScreenEdited = isEdited;
	}

	if (isPrlScreenEdited || isPrlPAXScreenEdited) {
		isEitherOneEdited = true;
	}

	top[1].objTMenu.tabPageEdited(screenId, isEitherOneEdited);

}

function getPageEdited() {
	return top[1].objTMenu.tabGetPageEidted(screenId);
}

// Calender
var objCal1 = new Calendar("spnCalendarDG1");

objCal1.onClick = "setDate";
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFrom", strDate);
		break;
	case "1":
		setField("txtTo", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 50;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function LoadCalendarForAdd(strID, objEvent) {
	if (genAction == "CA" || genAction == "CE") {
		objCal1.ID = strID;
		objCal1.top = 130;
		objCal1.left = 0;
		objCal1.onClick = "setDates";
		objCal1.showCalendar(objEvent);
	}
}

function setDates(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtDownloadTS", strDate);
		break;
	case "1":
		setField("txtFlightDateTime", strDate);
		break;
	pageOnChange();
}
}

function editContent(){
	
	showPopUp(1); 
}	

function PRLContentPopUpData(){
	var html = '';
	html+='<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">';
	html+='<tr><td>';
	html+='<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4">';
	html+='<tr><td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td>';
	html+='<td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader">';
	html+='Edit Contents';
	html+='</font></td><td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td>';
	html+='</tr><tr><td class="FormBackGround"></td><td class="FormBackGround" valign="top">';
	html+='<table><tr><td align="left">';
	html+='<font>Editor</font>';
	html+='</td></tr>';
	html+='<tr><td align="top"><textarea rows="20" name="txtPRLContent" id="txtPRLContent" cols="90"></textarea></td></tr>';
	html+='<tr><td align="right">&nbsp;<input type="button" id="btnPopupSave" value= "Save" class="Button" onClick="return PRLContentSave();"></td></tr></table>';
	html+='</td><td class="FormBackGround"></td></tr><tr>';
	html+='<td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td>';
	html+='<td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>';
	html+='<td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td>';
	html+='</tr></table>';
	html+='</td></tr></table>';
 	DivWrite("spnPopupData",html);
}

function PopUpData(){
	var html = '';
	html+='<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">';
	html+='<tr><td>';
	html+='<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4">';
	html+='<tr><td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td>';
	html+='<td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader">';
	html+='Edit Contents';
	html+='</font></td><td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td>';
	html+='</tr><tr><td class="FormBackGround"></td><td class="FormBackGround" valign="top">';
	html+='<table><tr><td align="left">';
	html+='<font>Editor</font>';
	html+='</td></tr>';
	html+='<tr><td align="top"> <select size="5" name="alternateFlight">';
	for(var a= 0; a<arrFLData.length; a++){
		html+=' <option value="'+a+'">'+arrFLData[a][0]+' '+arrFLData[a][1]+' '+arrFLData[a][2]+'</option>';
	}
	html+='</select></td></tr>';
	html+='<tr><td align="right">nbsp;<input type="button" id="btnPopupSave" value= "Save" class="Button" onClick="return PRLContentSave();"></td></tr></table>';
	html+='</td><td class="FormBackGround"></td></tr><tr>';
	html+='<td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td>';
	html+='<td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>';
	html+='<td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td>';
	html+='</tr></table>';
	html+='</td></tr></table>';
 	//DivWrite("spnPopupData1",html);
}

	
function PRLContentSave(){
		setField("txtaRulesCmnts", getValue("txtPRLContent"));
		DivWrite("spnContent", getValue("txtPRLContent"));
		setField("hdnMode", "UPDATE");
		document.forms[0].submit();
		hidePopUp(1);
}

function processStateChange() {
	var js;
    if (httpReq.readyState==4){
		showPageProgressMessage();
		resetMe();

   		if (httpReq.status==200){
   			js=refineJS(httpReq.responseText);
	   		eval(js);
			if(aMessage['code']=="ERROR"){
				disableButtons(false);
				showPageERRMessage(aMessage['desc'],"pwdOld");
			}else{
				var url=request['contextPath']+'/public/showRedirector?hdnMode=CHANGEPASSWORD';
				showPageConfMessage(aMessage['desc'],"");
				alert(raiseError('XBE-ERR-35'));
				window.location.replace(url);
			}
		}else{
			disableButtons(false);
       		showPageERRMessage(raiseError('XBE-ERR-33'),"pwdOld");
     	}
    }
}
