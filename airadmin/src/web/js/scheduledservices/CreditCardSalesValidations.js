/*
 * Author : Menaka P. Wedige
 */
var screenId = "SC_SHDS_005";
var valueSeperator = "~";
var strRowData;
var strGridRow;
var intLastRec;

// On Page loading make sure that all the page input fields are cleared
function winOnLoad(strMsg, strMsgType) {
	objOnFocus();
	if (saveSuccess == 1) {
		alert("Record Successfully Generated!");
		saveSuccess = 0;
	} else if (saveSuccess == 2) {
		alert("Record Successfully Transferd!");
		saveSuccess = 0;
	}
	// Keep the search variables remain same
	if (isSearchMode) {
		document.forms[0].hdnUIMode.value = "SEARCH";
		if (strMsg != null && strMsgType != null)
			showCommonError(strMsgType, strMsg);

		if (fromDate != null) {
			setField("txtFrom", fromDate);
		}
		if (toDate != null) {
			setField("txtTo", toDate);
		}
		if (selectedCardTypes != null) {
			ls.selectedData(selectedCardTypes);

		}
	}

	if (top[1].objTMenu.focusTab == screenId)
		getFieldByID("txtFrom").focus();

	Disable("btnGenTransfer", "true");
	if(isShowReport){
		setVisible('btnViewReport',true);
		Disable("btnViewReport",true);
	}else {
		setVisible('btnViewReport',false);
	}
	setVisible("spn1", true);
	setPageEdited(false);

}

// on Grid Row click
function RowClick(strRowNo) {
	objOnFocus();

	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;
		setField("hdnGridRow", strRowNo);
		setField("hdnIndex", arrCreditData[strRowNo][1]);
		setField("hdnDateOfSale", arrCreditData[strRowNo][1]);
		setField("hdnCardType", arrCreditData[strRowNo][2]);
		setField("hdnAmount", arrCreditData[strRowNo][3]);
		setField("hdnTransTimestamp", arrCreditData[strRowNo][4]);
		setField("hdnTransStatus", arrCreditData[strRowNo][5]);

		if (arrCreditData[strRowNo][9] == "N") {
			getFieldByID("btnGenTransfer").value = "Transfer";
			showCommonError("Error", seeLogEntry);
		} else if (arrCreditData[strRowNo][9] == "Y") {
			getFieldByID("btnGenTransfer").value = "Re-transfer";
		} else {
			getFieldByID("btnGenTransfer").value = "Generate";
		}
		setPageEdited(false);
		setField("hdnMode", "");
	}
}

function fromDateValidation(cntfield) {
	if (!dateChk(cntfield)) {
		showCommonError("Error", fromDateInvalid);
		if (top[1].objTMenu.focusTab == screenId)
			getFieldByID(cntfield).focus();
		return;
	}
}

function toDateValidation(cntfield) {
	if (!dateChk(cntfield)) {
		showCommonError("Error", toDateInvalid);
		if (top[1].objTMenu.focusTab == screenId)
			getFieldByID(cntfield).focus();
		return;
	}
}

function searchData() {
	objOnFocus();
	if (isEmpty(getText("txtFrom"))) {
		showCommonError("Error", fromDateRqrd);
		if (top[1].objTMenu.focusTab == screenId)
			getFieldByID("txtFrom").focus();
		return;
	}
	if (isEmpty(getText("txtTo"))) {
		showCommonError("Error", toDateRqrd);
		if (top[1].objTMenu.focusTab == screenId)
			getFieldByID("txtTo").focus();
		return;
	}

	fromDateValidation('txtFrom');
	toDateValidation('txtTo');

	var StartD = formatDate(getVal("txtFrom"), "dd/mm/yyyy", "dd-mmm-yyyy");
	var today = formatDate(getVal("hdnCurrentDate"), "dd/mm/yyyy",
			"dd-mmm-yyyy");
	var EndD = formatDate(getVal("txtTo"), "dd/mm/yyyy", "dd-mmm-yyyy");

	// compare from date and to date
	if (compareDates(StartD, EndD, '>')) {
		showCommonError("Error", todateLessthanFromDate);
		if (top[1].objTMenu.focusTab == screenId)
			getFieldByID("txtTo").focus();
		return false;
	}

	// Compare to date and current date
	if (compareDates(today, EndD, '<=')) {
		showCommonError("Error", todateLessthanCurrentDate);
		if (top[1].objTMenu.focusTab == screenId)
			getFieldByID("txtTo").focus();
		return false;
	}

	if (ls.getselectedData() == "") {
		showCommonError("Error", cardTypeRqrd);
		if (top[1].objTMenu.focusTab == screenId)
			getFieldByID("lstCardTypes").focus();
		return;
	}

	setField("hdnCardTypes", trim(ls.getselectedData()));
	// once search button was pressed set the hidden value to "SEARCH"
	document.forms[0].hdnUIMode.value = "SEARCH";
	document.forms[0].hdnMode.value = "";
	top[2].ShowProgress();
	document.forms[0].target = "_self";
	document.forms[0].submit();
}

function generateOrTransfer() {
	objOnFocus();

	setField("hdnRecNo", getTabValues(screenId, valueSeperator, "intLastRec"));
	setTabValues(screenId, valueSeperator, "strGridRow", strGridRow); // Save
																		// the
																		// grid
																		// Row
	var strArray = new Array();
	var strDateOfsSalesArray = new Array();
	var strCardTypeArray = new Array();
	var count = 0;

	strDateOfsSalesArray[count] = getVal("hdnDateOfSale");
	strCardTypeArray[count] = getVal("hdnCardType");
	var selscedRowId = objDG.getSelectedColumn(5);
	for ( var i = 0; i < selscedRowId.length; i++) {
		if ((selscedRowId[i][1])
				&& (arrCreditData[i][6] != "T")
				&& ((arrCreditData[i][6] == "G") || (arrCreditData[i][6] == ""))) {
			document.forms[0].hdnMode.value = "GENERATE";
			strArray[count] = arrCreditData[i][1] + "," + arrCreditData[i][7];
			strDateOfsSalesArray[count] = arrCreditData[i][1];
			strCardTypeArray[count] = arrCreditData[i][2];
			count++
		} else if ((selscedRowId[i][1])
				&& (arrCreditData[i][6] == "T")
				&& ((arrCreditData[i][6] != "G") || (arrCreditData[i][6] != ""))) {
			document.forms[0].hdnMode.value = "TRANSFER";
			strArray[count] = arrCreditData[i][1] + "," + arrCreditData[i][7];
			strDateOfsSalesArray[count] = arrCreditData[i][1];
			strCardTypeArray[count] = arrCreditData[i][2];
			count++
		}
	}
	if (strArray.length == 0) {
		Disable("btnGenTransfer", "true");
		getFieldByID("btnGenTransfer").value = "Generate/Transfer";
		showCommonError("Error", "please select a record");
		return;
	}
	if (arrCreditData[strGridRow][9] == "U"
			|| arrCreditData[strGridRow][9] == "") {
		var confirmStr = confirm(generateRecoredCfrm)
		if (!confirmStr)
			return;
		document.forms[0].hdnMode.value = "GENERATE";
	} else if (arrCreditData[strGridRow][9] == "N") {
		var confirmStr = confirm(transferRecoredCfrm)
		if (!confirmStr)
			return;
		document.forms[0].hdnMode.value = "TRANSFER";
	} else if (arrCreditData[strGridRow][9] == "Y") {
		var confirmStr = confirm(reTransferRecoredCfrm)
		if (!confirmStr)
			return;
		document.forms[0].hdnMode.value = "TRANSFER";
	}

	document.forms[0].hdnSelectedRows.value = strArray;
	document.forms[0].hdnSelectedDates.value = strDateOfsSalesArray;
	document.forms[0].hdnSelectedCardTypes.value = strCardTypeArray;
	setField("hdnCardTypes", trim(ls.getselectedData()));
	top[2].ShowProgress();
	document.forms[0].target = "_self";
	document.forms[0].submit();
	setPageEdited(false);
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function pageOnChange() {
	top[1].objTMenu.tabPageEdited(screenId, true);
}
function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

// Calender
var objCal1 = new Calendar("spnCalendarDG1");

objCal1.onClick = "setDate";
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFrom", strDate);
		break;
	case "1":
		setField("txtTo", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 130;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function viewReport (){
	var strArray = new Array();
	var strDateOfsSalesArray = new Array();
	var count = 0;
	strDateOfsSalesArray[count] = getVal("hdnDateOfSale");
	var selscedRowId = objDG.getSelectedColumn(5);
	for ( var i = 0; i < selscedRowId.length; i++) {
		if ( (selscedRowId[i][1]) && ( (arrCreditData[i][6] == "G") || (arrCreditData[i][6] == "T") 
						|| (arrCreditData[i][6] == "") ) ) {
			strDateOfsSalesArray[count] = arrCreditData[i][1];
			strArray[count] = arrCreditData[i][1] + "," + arrCreditData[i][7];
			count++;
		}
	}
	if (strArray.length == 0) {
		showCommonError("Error", "please select a record");
		return;
	}		
	document.forms[0].hdnSelectedRows.value = strArray;
	document.forms[0].hdnSelectedDates.value = strDateOfsSalesArray;
	
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
	top[0].objWindow = window.open("about:blank", "CWindow", strProp);	
	document.forms[0].target = "CWindow";
	document.forms[0].hdnMode.value = "REPORT";	
	document.forms[0].submit();
	top[2].HidePageMessage();
}

function displayDownLoadReportBtn (){	
	if(isShowReport){
		Disable("btnViewReport", false);
	}else {
		Disable("btnViewReport",true);
	}
}
