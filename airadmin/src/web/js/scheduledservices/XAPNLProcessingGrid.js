var screenId = "SC_SHDS_0033";
var valueSeperator = "~";
var intLastRec = 1;
/*
 * if (getTabValues(screenId, valueSeperator, "intLastRec")==""){
 * setTabValues(screenId, valueSeperator, "intLastRec", 1); }
 */

var strTxt = top[1].objTMenu.tabGetValue(screenId);
if (strTxt != "") {
	var arrTabData = strTxt.split("~");
	if (arrTabData[4] != null && arrTabData[4] != "") {
		intLastRec = arrTabData[4];
	}
}

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "5%";
objCol1.arrayIndex = 3;
objCol1.toolTip = "XAPNL ID";
objCol1.headerText = "XAPNL ID";
objCol1.itemAlign = "left"

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "20%";
objCol2.arrayIndex = 0;
objCol2.toolTip = "Download Timestamp";
objCol2.headerText = "Download Timestamp";
objCol2.ID = 'txtDownloadTS';
objCol2.sort = "true";

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "10%";
objCol3.arrayIndex = 1;
objCol3.toolTip = "Flight Number";
objCol3.headerText = "Flight Number";
objCol3.itemAlign = "center"
objCol3.sort = "true";

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "20%";
objCol4.arrayIndex = 2;
objCol4.toolTip = "Flight Date Time";
objCol4.headerText = "Flight Date Time";
objCol4.itemAlign = "center"

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "10%";
objCol5.arrayIndex = 11;
objCol5.toolTip = "Airport";
objCol5.headerText = "Airport";
objCol5.itemAlign = "left"

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "20%";
objCol6.arrayIndex = 12;
objCol6.toolTip = "SITA Address";
objCol6.headerText = "SITA Address";
objCol6.itemAlign = "left"

var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "15%";
objCol7.arrayIndex = 4;
objCol7.toolTip = "Processing Status";
objCol7.headerText = "Processing Status";
objCol7.itemAlign = "left"

// ---------------- Grid
var objDG = new DataGrid("spnDepartureData");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol6);
objDG.addColumn(objCol7);

objDG.width = "99%";
objDG.height = "185px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = arrData;
objDG.seqNo = true;
objDG.pgnumRecTotal = totalRes;
objDG.seqStartNo = intLastRec; // Grid
objDG.paging = true;
objDG.pgnumRecPage = 20;
objDG.pgonClick = "gridNavigations";
objDG.rowClick = "ReceivedPFSRowClick";
objDG.displayGrid();
objDG.shortCutKeyFunction = "Body_onKeyDown";

function gridLoadCheck() {
	var count = 0;
	if (objDG.loaded) {
		clearTimeout(objTempTimer);
		for ( var t = 0; t < arrData.length; t++) {
			pfsStat = arrData[t][6];
			if (arrData[t][6] == "U" && arrData[t][13] != "Added") {
				objDG.setDisable(t, "0", true);
				objDG.setDisable(t, "1", false);
				objDG.setDisable(t, "2", false);
				objDG.setDisable(t, "3", true);
				objDG.setDisable(t, "4", true);
				objDG.setDisable(t, "5", true);
				objDG.setDisable(t, "6", true);
				initStat = "NR";

			} else if (arrData[t][6] == "R") {
				pfsStat = "R";
				count++;
				objDG.setDisable(t, "", true);

			} else if (arrData[t][13] == "Added") {
				objDG.setDisable(t, "0", false);
				objDG.setDisable(t, "1", false);
				objDG.setDisable(t, "2", false);
				objDG.setDisable(t, "3", false);
				objDG.setDisable(t, "4", false);
				objDG.setDisable(t, "5", false);
				objDG.setDisable(t, "6", false);
			} else {

				objDG.setDisable(t, "", true);
			}
		}

		if (count == arrData.length) {
			initStat = "AR";

		}

	}
}

var objTempTimer = setInterval("gridLoadCheck()", 300)

function setFormattedDate(control) {
	var pgEdited = getPageEdited();
	var value = control.value;
	if (value != "") {
		control.value = checkDate(value);
	}
	setPageEdited(pgEdited, isPFS);
}

function checkDate(control) {
	var validated = false;
	var dateFull = trim(control);
	var Datevalue;
	var date, time;
	if (dateFull.indexOf(" ") != -1) {
		date = dateFull.substring(0, dateFull.indexOf(" "));
		time = dateFull.substring(dateFull.indexOf(" "));
	} else {
		date = dateFull;
		time = "";
	}

	if (date != "" && time != "") {
		setPageEdited(true);
		var val = dateChk(date);

		if (dateChk(date)) {
			Datevalue = val + " " + fromatTime(time);
		} else {
			Datevalue = dateFull;
		}

	} else if (date != "" && time == "") {
		setPageEdited(true);
		var val = dateChk(date);
		if (dateChk(date)) {
			Datevalue = val + " " + "00:00";
		} else {
			Datevalue = dateFull;
		}
	} else {
		Datevalue = dateFull;
	}
	return Datevalue;
}

function fromatTime(strTm) {

	var strTime = trim(strTm);
	if (trim(strTime) != "") {
		var index = strTime.length - 2;
		var hourindex = strTime.search(":");
		if (hourindex != -1) {

		} else {
			var mn = "00";
			if (strTime.length == 3 || strTime.length == 4)
				mn = strTime.substr(index, 2);

			var hr = 0;
			if (strTime.length == 3) {
				hr = strTime.substr(0, 1);
			} else {
				hr = strTime.substr(0, 2);
			}
			var timecolon = hr + ":" + mn;

			strTime = timecolon;
		}
	}

	return strTime;

}

function viewPFS() {
	if ((objWindow) && (!objWindow.closed)) {
		objWindow.close();
	}
	// objOnFocus();
	var intWidth = 0;
	var intHeight = 0;
	var strProp = '';
	var strMsgType;

	// TODO
	if ((objDG.getCellValue(rowNo, 3)) == "UNPARSED") {
		document.forms[0].hdnMode.value = "VIEWPFS";
		strMsgType = "VIEW";
	} else
		strMsgType = "GRID";
	setField("hdnRecNo", getTabValues(screenId, valueSeperator, "intLastRec"));
	setTabValues(screenId, valueSeperator, "strGridRow", strGridRow); // Save
																		// the
																		// grid
																		// Row
	setPageEdited(false);
	intHeight = 295;
	intWidth = 720;
	strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=Yes,width='
			+ intWidth
			+ ',height='
			+ intHeight
			+ ',resizable=no,top='
			+ ((window.screen.height - intHeight) / 2)
			+ ',left='
			+ (window.screen.width - intWidth) / 2;
	objWindow = window.open("about:blank", "CWindow", strProp);
	objForm = document.getElementById("frmXAPNL");
	objForm.target = "CWindow";
	objForm.action = "showPrintPFS.action?strMsgType=" + strMsgType;
	objForm.submit();

}
