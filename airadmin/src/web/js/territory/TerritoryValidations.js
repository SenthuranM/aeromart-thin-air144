/*
 * Author : K.Srikanth
 */

var strRowData;
var strGridRow;
var intLastRec;

var screenId = "SC_ADMN_003";
var valueSeperator = "~";
var objCmb1 = new combo();
var objCmb2 = new combo();

function validateTerritory() {
	if (validateTextField(getValue("txtTerritoryId"), territoryIdRqrd)) {
		getFieldByID("txtTerritoryId").focus();
		return;
	}

	if (getFieldByID("hdnMode").value == "ADD") {
		for ( var i = 0; i < arrData.length; i++) {
			if (arrData[i][1] == getFieldByID("txtTerritoryId").value) {
				showCommonError("Error", territoryIdExist);
				getFieldByID("txtTerritoryId").focus();
				return;
			}
		}
	}

	if (validateTextField(getValue("txtDesc"), territoryDescRqrd)) {
		getFieldByID("txtDesc").focus();
		return;
	}

	return true;
}

// On Page loading make sure that all the page input fields are cleared
function winOnLoad(strMsg, strMsgType) {
	disableInputControls();
	clearTerritory();
	setPageEdited(false);
	if (top[1].objTMenu.focusTab == screenId) {
		buttonSetFocus("btnAdd");
	}
	if (strMsg != null && strMsgType != null)
		showCommonError(strMsgType, strMsg);
	
	var strTmpSearch = searchData;
	if (strTmpSearch != "" && strTmpSearch != null) { // sets the search data
		var strSearch = strTmpSearch.split(",");
		objCmb1.setComboValue(strSearch[0]);
		objCmb2.setComboValue(strSearch[1]);
	}

	Disable("btnEdit", true);
	Disable("btnDelete", true);
	if (arrFormData != null && arrFormData.length > 0) {
		strGridRow = getTabValues(screenId, valueSeperator, "strGridRow"); // Get
																			// saved
																			// grid
																			// Row
		setPageEdited(true);
		setField("txtTerritoryId", arrFormData[0][1]);
		setField("txtDesc", arrFormData[0][2]);
		setField("txtRemarks", arrFormData[0][3]);
		setField("hdnVersion", arrFormData[0][5]);

		if (arrFormData[0][4] == "on")
			getFieldByID("chkStatus").checked = true;
		else
			getFieldByID("chkStatus").checked = false;

		enableInputControls();

		if (isAddMode) {
			setField("hdnMode", "ADD");
			Disable("txtTerritoryId", false);
			if (top[1].objTMenu.focusTab == screenId) {
				getFieldByID("txtTerritoryId").focus();
			}
		} else {
			setField("hdnMode", "EDIT");
			Disable("txtTerritoryId", true);
		}
	}

	if (isSuccessfulSaveMessageDisplayEnabled) {
		if (isSaveTransactionSuccessful) {
			alert("Record Successfully saved!");
		}
	}
	carrierPrefixControl();
	if (isRemoteUser == "true") {
		Disable("btnAdd", true);
		Disable("btnEdit", true);
		Disable("btnDelete", true);	
	}
}

/*
 * Function call for SEARCH Territories click
 */
function searchTerritory() {
	objOnFocus();
	var tmpTerriName;
	var tmpTerriCode;
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setField("hdnMode", "SEARCH");
		if(objCmb1.getText() != "" && objCmb1.getText() != "ALL"){ // this is for refresh the selected value
			tmpTerriName = objCmb1.getComboValue();
		} else {
			tmpTerriName = "*";
		}
		if(objCmb2.getText() != "" && objCmb2.getText() != "ALL"){ // this is for refresh the selected value
			tmpTerriCode = objCmb2.getComboValue();
		} else {
			tmpTerriCode = "*";
		}	
		searchData = tmpTerriName + "," + tmpTerriCode;
		setField("hdnRecNo", 1);
		setField("hdnSearchData", searchData);
		document.forms[0].submit();
		top[2].ShowProgress();
	}

}


// on Grid Row click
function RowClick(strRowNo) {
	objOnFocus();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;
		
		
		var territoryAirline = strRowData[0].substring(0,3);
		if (defaultAirlineCode == "") {
			territoryAirline = "";
			setField("txtTerritoryId", strRowData[0]);
		} else {
			setField("txtTerritoryId", strRowData[0].substr(3));
		}
		
		document.getElementById("spnAirlineCode").innerHTML= territoryAirline;
		setField("txtDesc", trim(strRowData[1]));
		setField("txtRemarks", trim(strRowData[2]));
		setField("hdnVersion", arrData[strRowNo][6]);
		setField("txtBank", arrData[strRowNo][7]);
		setField("txtAccountNumber", arrData[strRowNo][8]);
		setField("txtSwiftCode", arrData[strRowNo][9]);
		setField("txtBankAddress", arrData[strRowNo][10]);
		setField("txtIBANNo", arrData[strRowNo][11]);
		setField("txtBeneficiaryName", arrData[strRowNo][12]);

		if (strRowData[3] == "Active")
			getFieldByID("chkStatus").checked = true;
		else
			getFieldByID("chkStatus").checked = false;

		setPageEdited(false);
		disableInputControls();
		if (territoryAirline != defaultAirlineCode || isRemoteUser == "true") {
			Disable("btnEdit", true);
			Disable("btnDelete", true);			
		} else {
			Disable("btnEdit", false);
			Disable("btnDelete", false);
		}
	}
}

function enableInputControls() {
	Disable("txtTerritoryId", false);
	Disable("txtDesc", false);
	Disable("txtRemarks", false);
	Disable("btnSave", false);
	Disable("btnReset", false);
	Disable("chkStatus", false);
	Disable("txtBank", false);
	Disable("txtBankAddress", false);
	Disable("txtAccountNumber", false);
	Disable("txtSwiftCode", false);
	Disable("txtIBANNo", false);
	Disable("txtBeneficiaryName", false);
}

function disableInputControls() {
	Disable("txtTerritoryId", true);
	Disable("txtDesc", true);
	Disable("txtRemarks", true);
	Disable("btnSave", true);
	Disable("btnReset", true);
	Disable("chkStatus", true);
	Disable("txtBank", true);
	Disable("txtBankAddress", true);
	Disable("txtAccountNumber", true);
	Disable("txtSwiftCode", true);
	Disable("txtIBANNo", true);
	Disable("txtBeneficiaryName", true);
}

function clearTerritory() {
	setField("hdnVersion", "");
	setField("txtTerritoryId", "");
	setField("txtDesc", "");
	setField("txtRemarks", "");
	setField("txtBank", "");
	setField("txtBankAddress", "");
	setField("txtAccountNumber", "");
	setField("txtSwiftCode", "");
	setField("txtIBANNo", "");
	setField("txtBeneficiaryName", "");
	
}

function saveTerritory() {
	if (!validateTerritory())
		return;

	// setField("hdnRecNo",(top[0].intLastRec));
	var tempName = trim(getText("txtRemarks"));
	tempName = replaceall(tempName, "'", "`");
	tempName = replaceEnter(tempName);
	setField("txtRemarks", tempName);
	setField("txtBank", getText("txtBank"));
	setField("txtBankAddress", getText("txtBankAddress"));
	setField("txtAccountNumber", getText("txtAccountNumber"));
	setField("txtSwiftCode", getText("txtSwiftCode"));
	setField("txtIBANNo", getText("txtIBANNo"));
	setField("txtBeneficiaryName", getText("txtBeneficiaryName"));
	setField("hdnRecNo", getTabValues(screenId, valueSeperator, "intLastRec"));
	setTabValues(screenId, valueSeperator, "strGridRow", strGridRow); // Save
																		// the
																		// grid
																		// Row
	Disable("txtTerritoryId", false);
	document.forms[0].submit();
}

function resetTerritory() {
	var strMode = getText("hdnMode");
	if (strMode == "ADD") {
		clearTerritory();
		getFieldByID("txtTerritoryId").focus();
		getFieldByID("chkStatus").checked = true;
	}
	if (strMode == "EDIT") {
		if (strGridRow < arrData.length
				&& arrData[strGridRow][1] == getText("txtTerritoryId")) {
			getFieldByID("txtDesc").focus();
			setField("txtTerritoryId", arrData[strGridRow][1]);
			setField("txtDesc", arrData[strGridRow][2]);
			setField("txtRemarks", arrData[strGridRow][3]);
			if (arrData[strGridRow][4] == "Active")
				getFieldByID("chkStatus").checked = true;
			else
				getFieldByID("chkStatus").checked = false;
			setField("hdnVersion", arrData[strGridRow][6]);
		} else {
			// Clear the data entry area
			disableInputControls();
			clearTerritory();
			// document.getElementById('btnAdd').focus();
			buttonSetFocus("btnAdd");
			Disable("btnEdit", true);
			Disable("btnDelete", true);
		}
	}
	setPageEdited(false);
	objOnFocus();
}

function updateTerritory() {
	if (!validateTerritory())
		return;

	document.forms[0].submit();

}

// on Add Button click
function addClick() {
	objOnFocus();
	carrierPrefixControl();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		top[2].HidePageMessage();
		setPageEdited(true);
		Disable("txtTerritoryId", false);
		clearTerritory();
		enableInputControls();
		setField("hdnMode", "ADD");
		getFieldByID("chkStatus").checked = "true";
		Disable("btnEdit", true);
		Disable("btnDelete", true);
		getFieldByID("txtTerritoryId").focus();
		// top[2].ShowProgress();
	}
}


function carrierPrefixControl() {
	if (defaultAirlineCode == "") {
		$("#spnCntAirlineCode").hide();
	} else {
		$("#spnCntAirlineCode").show();
		document.getElementById("spnAirlineCode").innerHTML=defaultAirlineCode;
	}	
}
function clickChange() {
	objOnFocus();
	setPageEdited(true);
}

// on Edit Button CLick
function editClick() {
	objOnFocus();
	top[2].HidePageMessage();
	Disable("btnDelete", true);
	if (getFieldByID("txtTerritoryId").value == ""
			|| getFieldByID("txtTerritoryId").value == null) {
		showCommonError("Error", "Please select row to edit!");
		Disable("txtTerritoryId", true);
		disableInputControls();
		return;

	}
	// setPageEdited(false);
	enableInputControls();
	setField("hdnMode", "EDIT");
	Disable("txtTerritoryId", true);
	getFieldByID("txtDesc").focus();
}

// on Delete Button click
function deleteClick() {
	// setField("hdnRecNo",(top[0].intLastRec));
	if (getFieldByID("txtTerritoryId").value == ""
			|| getFieldByID("txtTerritoryId").value == null) {
		showCommonError("Error", "Please select row to delete!");
	} else {
		var confirmStr = confirm(deleteConf)
		if (!confirmStr)
			return;

		enableInputControls();
		setField("txtTerritoryId", strRowData[0]);
		setField("hdnMode", "DELETE");
		setTabValues(screenId, valueSeperator, "intLastRec", 1); // Delete
		setField("hdnRecNo", getTabValues(screenId, valueSeperator,
				"intLastRec"));
		document.forms[0].submit();
		disableInputControls();
	}
}

function CCPress() {
	var strCC = getText("txtTerritoryId");
	var strLen = strCC.length;
	var blnVal = isAlphaNumeric(strCC);
	if (!blnVal) {
		setField("txtTerritoryId", strCC.substr(0, strLen - 1));
		getFieldByID("txtTerritoryId").focus();
	}
}

function CDPress() {
	var strCD = getText("txtDesc");
	var strLen = strCD.length;
	var blnVal = isAlphaNumeric(strCD);
	if (!blnVal) {
		setField("txtDesc", strCD.substr(0, strLen - 1));
		getFieldByID("txtDesc").focus();
	}
}

function validateTA(objControl, length) {
	var strValue = objControl.value;
	var strLength = strValue.length;
	var blnVal = isEmpty(strValue);
	if (blnVal) {
		setField("txtRemarks", strValue.substr(0, strLength - 1));
		getFieldByID("txtRemarks").focus();
	}
	if (strLength > length) {
		strValue = strValue.substr(0, length);
		objControl.value = strValue;
	}
}

function valDescription(objTextBox) {
	setPageEdited(true);
	objOnFocus();
	commaValidate(objTextBox);
}

function Save(msg) {
	top[2].objMsg.MessageText = msg;
	top[2].objMsg.MessageType = "Confirmation";
	top[2].ShowPageMessage();
}

function Delete() {
	top[2].objMsg.MessageText = "Selected record will get deleted.";
	top[2].objMsg.MessageType = "Warning";
	top[2].ShowPageMessage();
}

function myRecFunction(intRecNo, intErrMsg) {
	top[2].HidePageMessage();
	if (intErrMsg != "") {
		top[2].objMsg.MessageText = intErrMsg;
		top[2].objMsg.MessageType = "Error";
		top[2].ShowPageMessage();
	}
}

// To Handle Paging
function gridNavigations(intRecNo, intErrMsg) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setField("hdnMode", "PAGING");
		objOnFocus();
		// top[0].intLastRec=intRecNo;
		if (intRecNo <= 0)
			intRecNo = 1;
		setTabValues(screenId, valueSeperator, "intLastRec", intRecNo);
		// setField("hdnRecNo",(top[0].intLastRec));
		setField("hdnRecNo", getTabValues(screenId, valueSeperator,
				"intLastRec"));
		if (intErrMsg != "") {
			top[2].objMsg.MessageText = intErrMsg;
			top[2].objMsg.MessageType = "Error";
			top[2].ShowPageMessage();
		} else {
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	}
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}
function pageOnChange() {
	top[2].HidePageMessage();
	setPageEdited(true);
}

var begin = [["ALL", "ALL"]];	
buildTerriIDCombos(arrTerriID);


function buildTerriIDCombos(arrName) {	
	objCmb1.id = "selTerritoryID";
	objCmb1.top = "26";
	objCmb1.left = "150";
	objCmb1.width = "200";
	objCmb1.textIndex = 0;
	objCmb1.valueIndex = 0;
	objCmb1.tabIndex = 1;
	objCmb1.selectedText = "ALL";
	objCmb1.selectedValue = "*";
	objCmb1.arrData = begin.concat(arrName.sort()); // add "ALL" element begin of the list
	objCmb1.onChange = "objOnFocus";

	
	
}

buildTerriDescCombos(arrTerriID);

function buildTerriDescCombos(arrName) {		
	objCmb2.id = "selTerritoryDesc";
	objCmb2.top = "26";
	objCmb2.left = "550";
	objCmb2.width = "200";
	objCmb2.textIndex = 1;
	objCmb2.valueIndex = 1;
	objCmb2.tabIndex = 1;
	objCmb2.selectedText = "ALL";
	objCmb2.selectedValue = "*";
	objCmb2.arrData = begin.concat(removeDuplicate(arrName, 1).sort(by(1)));
	objCmb2.onChange = "objOnFocus";
	objCmb2.buildCombo();	
}

function removeDuplicate(buildarray, index) {
	var tempArray = new Array();
	var count = 1;
	buildarray.sort();
	var temp = buildarray[0][index];
	tempArray[0] = buildarray[0];
	var isMatch = new Boolean(false); 
	for ( var i = 1; i < buildarray.length; i++) {
		for (var j = 0; j < count; j++) {	
			if (buildarray[i][index] == tempArray[j][index]) {
				isMatch = true;
				break;
			} else {
				isMatch = false;
			}
		}
		if (!isMatch) {
			tempArray[count] = buildarray[i];
			count++;
		}
	}
	return tempArray
}

function by(i) {
return function(a,b){a = a[i];b = b[i];return a.toLowerCase() == b.toLowerCase() ? 0 : (a.toLowerCase() < b.toLowerCase() ? -1 : 1)}
}