var screenId = "SC_ADMN_003";
var valueSeperator = "~";

if (getTabValues(screenId, valueSeperator, "intLastRec") == "") {
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
}
var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "15%";
objCol1.arrayIndex = 1;
// objCol1.toolTip = "Territory Id" ;
objCol1.headerText = "Territory ID";
objCol1.itemAlign = "left"
objCol1.sort = "true";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "24%";
objCol2.arrayIndex = 2;
// objCol2.toolTip = "Territory Description" ;
objCol2.headerText = "Territory Description";
objCol2.itemAlign = "left"

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "30%";
objCol3.arrayIndex = 3;
// objCol3.toolTip = "Remarks" ;
objCol3.headerText = "Remarks";
objCol3.itemAlign = "Left"

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "10%";
objCol4.arrayIndex = 4;
// objCol4.toolTip = "status" ;
objCol4.headerText = "Status";
objCol4.itemAlign = "left";

	
var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "20%";
objCol5.arrayIndex = 7;
objCol5.headerText = "Bank";
objCol5.itemAlign = "left";
	
var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "10%";
objCol6.arrayIndex = 8;
objCol6.headerText = "Account Number";
objCol6.itemAlign = "left"
	
var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "10%";
objCol7.arrayIndex = 9;
objCol7.headerText = "Swift Code";
objCol7.itemAlign = "left";
	


// ---------------- Grid
var objDG = new DataGrid("spnTerritories");

objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3); // rEmark
objDG.addColumn(objCol4); // Status
objDG.addColumn(objCol5);
//objDG.addColumn(objCol6);
//objDG.addColumn(objCol7);


objDG.width = "99%";
objDG.height = "180px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = arrData;
objDG.seqNo = true;
objDG.seqStartNo = getTabValues(screenId, valueSeperator, "intLastRec"); // Grid
objDG.pgnumRecTotal = totalRecords; // remove as per return record size
objDG.paging = true
objDG.pgnumRecPage = 20;
objDG.pgonClick = "gridNavigations";
objDG.rowClick = "RowClick";
objDG.displayGrid();
