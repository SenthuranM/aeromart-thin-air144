var screenId = "SC_ADMN_025";
var valueSeperator = "^";

var recNumPerPage = 20;
if (getTabValues(screenId, valueSeperator, "intLastRec") == "") {
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
}

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "20%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Model No";
objCol1.headerText = "Model No";
objCol1.itemAlign = "left";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "15%";
objCol2.arrayIndex = 2;
objCol2.toolTip = "Template IDS";
objCol2.headerText = "Template ID";
objCol2.itemAlign = "right";

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "20%";
objCol3.arrayIndex = 3;
objCol3.toolTip = "Template Code";
objCol3.headerText = "Template Code";
objCol3.itemAlign = "left";

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "20%";
objCol4.arrayIndex = 4;
objCol4.toolTip = "Description";
objCol4.headerText = "Description";
objCol4.itemAlign = "left";

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "10%";
objCol5.arrayIndex = 5;
objCol5.toolTip = "Default Charge";
objCol5.headerText = "Default Charge";
objCol5.itemAlign = "right";

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "14%";
objCol6.arrayIndex = 9;
objCol6.toolTip = "Status";
objCol6.headerText = "Status";
objCol6.itemAlign = "center";

// ---------------- Grid

var objDG = new DataGrid("spnSeatCharges");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol6);

objDG.width = "99%";
objDG.height = "142px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = modelChargeData;
objDG.seqNo = true;
objDG.seqNoWidth = "4%";
objDG.backGroundColor = "#ECECEC";
objDG.seqStartNo = getTabValues(screenId, valueSeperator, "intLastRec"); // Grid;
objDG.pgnumRecTotal = totalNoOfRecords; // remove as per return record size
objDG.paging = true
objDG.pgnumRecPage = recNumPerPage;
objDG.pgonClick = "gridNavigations";
objDG.rowClick = "gridClick";
objDG.displayGrid();

function myRecFunction(intRecNo, intErrMsg) {
	if (intErrMsg != "") {
		parent.objMsg.MessageText = "<li> " + intErrMsg;
		parent.objMsg.MessageType = "Error";
		parent.ShowPageMessage();
	}
}

// /------------page Functionality

var strGridRow;
var templateCode;
var selectedModel;
var templateCharges = new Array();
var templDefCharge;
var preDefCharge;
var seatCharges = new Array();
var defaultAmountInitVal=-1;

function disbleControls(cond) {

	Disable("btnEdit", cond);
	Disable("btnCancel", cond);
	Disable("selFltModelNo", cond);
	Disable("txtTepmplateCode", cond);
	Disable("txtDescription", cond);
	Disable("txtChargeAmt", cond);
	Disable("txtRemarks", cond);
	Disable("btnReset", cond);
	Disable("btnSave", cond);
	Disable("btnSeatMap", cond);
	Disable("chkStatus", cond);
	Disable("localCurrCode" , cond);
}

function gridClick(strRowNo) {

	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {

		strGridRow = strRowNo;
		setField("selFltModelNo", modelChargeData[strGridRow][1]);
		setField("hdnModelNo", modelChargeData[strGridRow][1]);
		setField("hdnTmplId", modelChargeData[strGridRow][2]);
		document.getElementById("spnTemplateId").innerHTML = modelChargeData[strGridRow][2];
		setField("txtTepmplateCode", modelChargeData[strGridRow][3]);
		setField("txtDescription", modelChargeData[strGridRow][4]);
		setField("txtChargeAmt", modelChargeData[strGridRow][5]);
		templDefCharge = modelChargeData[strGridRow][5];
		setField("txtRemarks", modelChargeData[strGridRow][6]);
		setField("hdnVersion", modelChargeData[strGridRow][7]);
		if (modelChargeData[strGridRow][9] == "ACT") {
			getFieldByID("chkStatus").checked = true;
		} else {
			getFieldByID("chkStatus").checked = false;
		}
		
		setField("localCurrCode", modelChargeData[strGridRow][10]);
		document.getElementById("lblLocalCurrencyCode").innerHTML = modelChargeData[strGridRow][10];
		
		defaultAmountInitVal =  modelChargeData[strGridRow][5];
		templateCode = modelChargeData[strGridRow][3];
		selectedModel = getText("selFltModelNo");
		disbleControls(true);
		Disable("btnEdit", false);
		Disable("btnCancel", false);
		setPageEdited(false);
		preDefCharge = modelChargeData[strGridRow][5];
		setField("hdnChargeAmount", preDefCharge);
		document.getElementById("localCurrCode").style.visibility = "hidden";
		document.getElementById("lblLocalCurrencyCode").style.visibility = "visible";
	}
}

function searchTemplate() {
	setField("hdnMode", "SEARCH");
	getFieldByID("formSeatMap").submit();
	setTabValues(screenId, valueSeperator, "strSearchCriteria",
			getText("selModelNo") + "," + getText("selTemplate"));
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
	top[2].ShowProgress();

}

function closeClick() {
	if (ckheckPageEdited()) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId)
	}
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);

}

function ckheckPageEdited() {
	if (top[1].objTMenu.tabGetPageEidted(screenId))
		return confirm("Changes will be lost! Do you wish to Continue?");
	else
		return true;
}

function validateHandlingChrg() {
	setPageEdited(true);
	var strCR = getText("txtChargeAmt");
	var strLen = strCR.length;
	var blnVal = currencyValidate(strCR, 3, 2);
	var wholeNumber;
	if (!blnVal) {
		if (strCR.indexOf(".") != -1) {
			wholeNumber = strCR.substr(0, strCR.indexOf("."));
			if (wholeNumber.length > 6) {
				setField("txtChargeAmt", strCR
						.substr(0, wholeNumber.length - 1));
			} else {
				setField("txtChargeAmt", strCR.substr(0, strLen - 1));
			}
		} else {
			setField("txtChargeAmt", strCR.substr(0, strLen - 1));
		}

		getFieldByID("txtChargeAmt").focus();
	}
}

function saveClick() {
	if (isEmpty(getText("txtTepmplateCode"))) {
		showERRMessage(codeFromRqrd);
		getFieldByID("txtTepmplateCode").focus();
		return;
	} else if (isEmpty(getText("selFltModelNo"))) {
		showERRMessage(modelRqrd);
		getFieldByID("selFltModelNo").focus();
		return;
	} else if (isEmpty(getText("txtDescription"))) {
		showERRMessage(descriptionRqrd);
		getFieldByID("txtDescription").focus();
		return;
	}else if(isEmpty(getText("localCurrCode"))){
		showCommonError(localCurrencyCodeRqrd);
		getFieldByID("localCurrCode").focus();
		return false;		
	} else {
		if (isEmpty(getText("txtChargeAmt"))) {
			setField("txtChargeAmt", 0);
		}

		if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
			top[0].objWindow.close();
		}
		
		alert("Saving Seat Charge Template will take some time.....");
		
		var strMode = getText("hdnMode");
		if (strMode == "ADD") {
			setField("hdnIsEditDefaultAmount", "false");
		}
		if (strMode == "EDIT") {
			if (defaultAmountInitVal != getText("txtChargeAmt")){
				setField("hdnIsEditDefaultAmount", "true");
			}else{
				setField("hdnIsEditDefaultAmount", "false");
			}
		}
		var tempName = trim(getText("txtRemarks"));
		tempName = replaceall(tempName, "'", "`");
		tempName = replaceEnter(tempName);
		setField("txtRemarks", tempName);
		Disable("selFltModelNo", false);
		Disable("localCurrCode", false);
		setField("hdnMode", "SAVE");
		getFieldByID("formSeatMap").submit();
		setTabValues(screenId, valueSeperator, "strSearchCriteria",
				getText("selModelNo") + "," + getText("selTemplate"));
		setField("hdnRecNo", getTabValues(screenId, valueSeperator,
				"intLastRec"));
		top[2].ShowProgress();
	}
}

function resetClick() {
	var strMode = getText("hdnMode");
	setPageEdited(false);
	if (strMode == "ADD") {
		clearControls();
		setField("hdnMode", "ADD");
		setField("hdnAction", "ADD");
		setField("hdnModelNo", getText("selModelNo"));
		
	}
	if (strMode == "EDIT") {
		setField("hdnMode", "EDIT");
		setField("hdnAction", "EDIT");
		if (strGridRow != "") {
			setField("selFltModelNo", modelChargeData[strGridRow][1]);
			setField("hdnModelNo", modelChargeData[strGridRow][1]);
			setField("hdnTmplId", modelChargeData[strGridRow][2]);
			document.getElementById("spnTemplateId").innerHTML = modelChargeData[strGridRow][2];
			setField("txtTepmplateCode", modelChargeData[strGridRow][3]);
			setField("txtDescription", modelChargeData[strGridRow][4]);
			setField("txtChargeAmt", modelChargeData[strGridRow][5]);
			setField("txtRemarks", modelChargeData[strGridRow][6]);
			setField("hdnVersion", modelChargeData[strGridRow][7]);
			
			if (modelChargeData[strGridRow][9] == "ACT") {
				getFieldByID("chkStatus").checked = true;
			} else {
				getFieldByID("chkStatus").checked = false;
			}
			Disable("selFltModelNo", true);
			setField("localCurrCode", modelChargeData[strGridRow][10]);
			document.getElementById("lblLocalCurrencyCode").innerHTML = modelChargeData[strGridRow][10];
			document.getElementById("localCurrCode").style.visibility = "hidden";
		
		}
	}
	Disable("btnReset", true);

}

function addClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		clearControls();
		disbleControls(false);
		Disable("btnSeatMap", true);
		setField("hdnMode", "ADD");
		setField("hdnAction", "ADD");
		setField("hdnModelNo", getText("selModelNo"));
		getFieldByID("chkStatus").checked = true;
		document.getElementById("lblLocalCurrencyCode").style.visibility = "hidden";
		document.getElementById("localCurrCode").value=reqBaseCurr;
		document.getElementById("localCurrCode").style.visibility = "visible";		
		
	}
}

function editClick() {
	disbleControls(false);
	Disable("selFltModelNo", true);
	setField("hdnMode", "EDIT");
	setField("hdnAction", "EDIT");
	document.getElementById("localCurrCode").style.visibility = "hidden";
}

function cancelClick() {
	var strconfirm = confirm(deleteConf);
	if (strconfirm) {
		disbleControls(false);
		setField("hdnRecNo", 0);
		setField("hdnMode", "DELETE");
		setTabValues(screenId, valueSeperator, "strSearchCriteria",
				getText("selModelNo") + "," + getText("selTemplate"));
		document.formSeatMap.submit();
		top[2].ShowProgress();
		disbleControls(true);
	}

}

function seatMapClick() {

	templateCharges = modelChargeData[strGridRow][8];
	seatCharges = templateCharges;
	setField("hdnSeatsCharge", templateCharges);
	
	if (isEmpty(getText("selFltModelNo"))) {
		showERRMessage(modelRqrd);
		getFieldByID("selFltModelNo").focus();
		return;
	} else {
		var strCR = getText("txtChargeAmt");
		if (trim(strCR) == "") {
			setField("txtChargeAmt", "0");
		}
		var intWidth = 1200;
		var intHeight = 580;
		var strFilename = "showSeat.action?hdnMode=VIEW&selFltModelNo="
				+ getText("selFltModelNo")+"&hdnTmplId="+getText("hdnTmplId");;
		var strProp = 'toolbar=no,location=no,status=yes,menubar=no,scrollbars=no,width='
				+ intWidth
				+ ',height='
				+ intHeight
				+ ',resizable=no,top='
				+ ((window.screen.height - intHeight) / 2)
				+ ',left='
				+ (window.screen.width - intWidth) / 2;
		top[0].objWindow = window.open(strFilename, "CWindow", strProp);
	}
}

function clearControls() {

	document.getElementById("spnTemplateId").innerHTML = "";
	setField("hdnModelNo", "");
	setField("txtTepmplateCode", "");
	setField("txtDescription", "");
	setField("txtChargeAmt", "");
	setField("txtRemarks", "");
	setField("hdnVersion", "");
	setField("hdnTmplId", "");
	setField("hdnSeatsCharge", "");
	setField("selFltModelNo", "");
	templateCode = "";
	selectedModel = "";
	templateCharges = new Array();
	templDefCharge = "0.0";
	seatCharges = new Array();
}

function validateTA(objControl, length) {
	var strValue = objControl.value;
	var strLength = strValue.length;
	var blnVal = isEmpty(strValue);
	if (blnVal) {
		setField("txtRemarks", strValue.substr(0, strLength - 1));
		getFieldByID("txtRemarks").focus();
	}
	if (strLength > length) {
		strValue = strValue.substr(0, length);
		objControl.value = strValue;
	}
}

/*
 * Function Call for page edit
 */
function clickChange() {
	setPageEdited(true);
}

/**
 * setting thr functionality
 */
function setSeatChages(arrStPrice) {
	seatCharges = arrStPrice;
	//arrayClone(arrStPrice, templateCharges);
	setField("hdnSeatsCharge", seatCharges);
	setPageEdited(true);
}

function setDefault() {
	templDefCharge = getText("txtChargeAmt");
}

function setPageEdited(isEdited) {
	blnPageedit = isEdited;
}

/*
 * Function to Handle the Paging
 */
function gridNavigations(intRecNo, intErrMsg) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {

		if (intRecNo <= 0)
			intRecNo = 1;

		setTabValues(screenId, valueSeperator, "intLastRec", intRecNo);
		setField("hdnRecNo", getTabValues(screenId, valueSeperator,
				"intLastRec"));
		if (intErrMsg != "") {
			top[2].objMsg.MessageText = "<li> " + intErrMsg;
			top[2].objMsg.MessageType = "Error";
			top[2].ShowPageMessage();
		} else {
			top[1].objTMenu.tabSetValue(screenId, "1" + valueSeperator
					+ getText("selModelNo") + "," + getText("selTemplate"));
			setTabValues(screenId, valueSeperator, "intLastRec", intRecNo);
			document.formSeatMap.submit();
			top[2].ShowProgress();
		}
	}
}

// page load function
function winOnLoad(strMsg, strMsgType) {

	var strSearchData = getTabValues(screenId, valueSeperator,
			"strSearchCriteria");
	if (strSearchData != "") {
		var arrSearch = strSearchData.split(",");
		setField("selModelNo", arrSearch[0]);
		setField("selTemplate", arrSearch[1]);
	}
	
	if (arrFormData != null && arrFormData.length > 0) { // if error occured

		setField("selFltModelNo", arrFormData[1]);
		setField("hdnModelNo", arrFormData[1]);
		setField("hdnTmplId", arrFormData[2]);
		setField("txtTepmplateCode", arrFormData[3]);
		setField("txtDescription", arrFormData[4]);
		setField("txtChargeAmt", arrFormData[5]);
		setField("txtRemarks", arrFormData[6]);
		setField("hdnVersion", arrFormData[7]);
		setField("hdnSeatsCharge", arrFormData[8]);
		if (arrFormData[9] == "Active") {
			getFieldByID("chkStatus").checked = true;
		} else {
			getFieldByID("chkStatus").checked = false;
		}
		setPageEdited(true);
		disbleControls(false);
		if (arrFormData[10] == "EDIT") {
			Disable("selFltModelNo", true);
		}
	} else {
		setPageEdited(false);
		clearControls();
		disbleControls(true);
	}
	if (strMsg != null && strMsg != "" && strMsgType != null
			&& strMsgType != "") {
		showCommonError(strMsgType, strMsg);
	}

	if (strMsg == trim("Record Successfully updated")) {
		alert("Record Successfully Saved!");
	}

	if (strMsg == trim("Save/Update operation is successful")) {
		alert("Record Successfully Saved!");
	}

}