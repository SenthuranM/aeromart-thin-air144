var objWindow;
var strRowData;
var strGridRow = -1;	

var strLegs;
var strLegDtls;

var blnZuluTime = true;
var arrLegs;
var blnCalander = false;
var blnAdd = false;
var valueSeperator = "^";
var isNotFirstMngFlt = false;
var isTimeInZulu = false;

var strSunday = "";
var strMonday = "";
var strTuesday = "";
var strWednesday = "";
var strThursday = "";
var strFriday = "";
var strSaturday = "";

var screenID = "SC_FLGT_001";
var invScreenID = "SC_INVN_002";
var tabValue = getTabValue();

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.buildCalendar();

var FLT_TYPE_DESC = {"INT":"INT", "DOM":"DOM"};
var flightDeptTime;
	
function setDate(strDate, strID){
	switch (strID){
		case "0" : setField("txtStartDateSearch",strDate);break ;
		case "1" : setField("txtStopDateSearch",strDate);break ;
		case "3" : setField("txtDepatureDate",strDate); break;
		case "4" : setField("fromDatePicker",strDate); break;
		case "5" : setField("toDatePicker",strDate); break;
	}
}

	function LoadCalendar(strID, objEvent){
		objCal1.ID = strID;
		if (strID == 0){
			objCal1.top = 20;
			objCal1.left = 75;
		}else if (strID == 1){
			objCal1.top = 20;
			objCal1.left = 175;
		}else if (strID == 3){
			objCal1.top = 320;
			objCal1.left = 200;
		}else{
			objCal1.top = 320;
			objCal1.left = 380;
		}
		objCal1.onClick = "setDate";
		if ((strID == 0) && (!getFieldByID("txtStartDateSearch").disabled)) {
			objCal1.showCalendar(objEvent);		
		} else if ((strID == 1) && (!getFieldByID("txtStopDateSearch").disabled)) {
			objCal1.showCalendar(objEvent);
		} else if ((strID == 4) && (!getFieldByID("fromDatePicker").disabled)) {
			objCal1.showCalendar(objEvent);
		} else if ((strID == 5) && (!getFieldByID("toDatePicker").disabled)) {
			objCal1.showCalendar(objEvent);
		} else if(blnCalander) {
			objCal1.showCalendar(objEvent);
		}
	}
	
	function LoadCalendarNew(strID, objEvent){
		objCal.top = 0;
			objCal.left = 20;
			objCal.ShowCalendar(objEvent);
			objCal.onClick = "setDate";
	}
	
	function dataChanged(){
		setPageEdited(true);

	}
	// Populate Leg day -3 to +3
	function populateSelDay(){
			var arrDay = new Array();
			arrDay[0] = new Array(-1,-1);
			arrDay[1] = new Array(1,"+1");
			var objLB = new listBox();
			objLB.dataArray = arrDay; 
			var intDD = 5;
			var strID = "";

			for (var i = 1 ; i <= intDD ;i++){
				if (strID != ""){strID += ",";}
					strID += "selDepDay" + i;
				}
			for (var i = 1 ; i <= intDD ;i++){
				if (strID != ""){strID += ",";}
					strID += "selArrDay" + i;
			}
			objLB.id = strID;
			objLB.blnFirstEmpty = true;	// first value empty or not
			objLB.firstValue = "0";		// first value 
			objLB.firstTextValue = "0";	// first Text
			objLB.fillListBox();
	}
	
	//function to populate all online active airports
	function onLoadPopulateAirports(){	
		var objLB = new listBox();
		objLB.dataArray = arrAptData; 
		var intDD = 5;
		var strID = "";
	
		for (var i = 1 ; i <= intDD ;i++){
			if (strID != ""){ strID += ","; }
			strID += "selFromStn" + i;
		}
		for (var i = 1 ; i <= intDD ;i++){
			if (strID != ""){ strID += ","; }
			strID += "selToStn" + i;
		}
		objLB.id = strID;
		objLB.blnFirstEmpty = true;	// first value empty or not
		objLB.firstValue = "";		// first value 
		objLB.firstTextValue = "";	// first Text
		objLB.fillListBox();
	}
	
	//function to populate all online airports
	function onLoadPopulateAllAirports(){	
		var objLB = new listBox();
		objLB.dataArray = arrOnlineAirports; 
		var intDD = 5;
		var strID = "";
	
		for (var i = 1 ; i <= intDD ;i++){
			if (strID != ""){ strID += ","; }
			strID += "selFromStn" + i;
		}
		for (var i = 1 ; i <= intDD ;i++){
			if (strID != ""){ strID += ","; }
			strID += "selToStn" + i;
		}
		objLB.id = strID;
		objLB.blnFirstEmpty = true;	// first value empty or not
		objLB.firstValue = "";		// first value 
		objLB.firstTextValue = "";	// first Text
		objLB.fillListBox();
	}
	
	//function to populate all online airports for search criteria
	function populateAllAirportsForSearch(){	
		var objLB = new listBox();
		objLB.dataArray = arrOnlineAirports; 
		var strID = "selFromStn6,selToStn6";
		objLB.id = strID;
		objLB.blnFirstEmpty = true;	// first value empty or not
		objLB.firstValue = "";		// first value 
		objLB.firstTextValue = "";	// first Text
		objLB.fillListBox();
	}	
	
	//On Page loading make sure that all the page input fields are cleared
	function PreviousMonth() {
		if ((!getFieldByID("txtStartDateSearch").disabled) 
				&& (!getFieldByID("txtStopDateSearch").disabled)) {
			objOnFocus();
			var strStartD = getFieldByID("txtStartDateSearch").value;
			var strStopD = "";
			var strDaysStopDate = "";		
			setField("txtStartDateSearch",addMonths(-1,"01"+strStartD.substr(2,8)));
			strStopD = addMonths(-1,"01"+strStartD.substr(2,8));
			strDaysStopDate = getDaysInMonth(strStopD.substr(3,2),strStopD.substr(6,4));		
			setField("txtStopDateSearch",addMonths(0,strDaysStopDate+strStopD.substr(2,8)));
		}
	}
	
	function NextMonth() {
		if ((!getFieldByID("txtStartDateSearch").disabled) 
				&& (!getFieldByID("txtStopDateSearch").disabled)) {	
			objOnFocus();
			var strStartD = getFieldByID("txtStartDateSearch").value;
			var strStopD = "";
			var strDaysStopDate = "";
			setField("txtStartDateSearch",addMonths(1,"01"+strStartD.substr(2,8)));
			strStartD = getFieldByID("txtStartDateSearch").value;
			strDaysStopDate = getDaysInMonth(strStartD.substr(3,2),strStartD.substr(6,4));	
			setField("txtStopDateSearch",addMonths(0,strDaysStopDate+strStartD.substr(2,8)));			
		}
	}
	
	//Validtaions when searching
	function SearchValid(){
		//Mandatory fields		
		if (getVal("txtStartDateSearch") == "") {
			showERRMessage(startdate);
			return false;
		}
		if (getVal("txtStopDateSearch") == "") {
			showERRMessage(stopdate);
			return false;
		}
		if (!dateChk("txtStartDateSearch")){
			showERRMessage(invalidDate);
			getFieldByID("txtStartDateSearch").focus();
			return false;
		}
		if (!dateChk("txtStopDateSearch")){
			showERRMessage(invalidDate);
			getFieldByID("txtStopDateSearch").focus();
			return false;
		}
		if ((getVal("txtFlightNoSearch") != "") && (!flightValid("txtFlightNoSearch"))) {
			showERRMessage(invalidFltNo);
			getFieldByID("txtFlightNoSearch").focus();
			return false;
		}
		
		//compare start date and stop date
		var schStartD = formatDate(getVal("txtStartDateSearch"), "dd/mm/yyyy","dd-mmm-yyyy");
		var schStopD = formatDate(getVal("txtStopDateSearch"), "dd/mm/yyyy","dd-mmm-yyyy");
		
		if (compareDates(schStartD,schStopD,'>')){
			showERRMessage(stopdategreat);
			return false;
		}
		
		var schcarrier = trim(getVal("txtFlightNoStartSearch"));
		var blnValid = false;
		for(var cl=0;cl < arrCarriers.length;cl++) {
			if(arrCarriers[cl] == schcarrier) {
				blnValid = true;
				break;
			}
		}
		if(schcarrier != "" && !blnValid) {
			showERRMessage(invalidcarrier);
			getFieldByID("txtFlightNoStartSearch").focus();
			return false;
		}
		return true;
	}
	
	//call the functoion  onload
	function winOnLoad(strMsg, strMsgType, strWarnMsg) {
								//DEBUG POINTS
								//alert(getTabValue()[2]+ " getTabValue()[2] ONLOAD") 
								//alert(getTabValue()[3] + " getTabValue()[3] ONLOAD SCHED")  
			closeChildWindows();
			writeFrequency();
			//document.getElementById("chkAll").checked = true;
			Disable("btnViewHistory",true);
			Disable("btnMsgHistory", true);
			Disable("btnPubMessages", true);			
			$('#btnMsgHistory').hide();
			$('#btnPubMessages').hide();
			Disable("btnNoteRollForwardSearch",true);
			setField("hdnReprotect", "false");
			setField("txtFlightNoStartSearch",defCarrCode);
			setField("txtFlightNoStart",defCarrCode);
			document.getElementById("frmFlight").target = "_self";	
			setPageEdited(false);
			populateSelDay();
			populateAllAirportsForSearch();
			onLoadPopulateAllAirports();
			setVisible("tblOptSearch", true);
			if ((getFieldByID("hdnMode").value == "ADD") 
					|| (getParamValue("hdnMode") == "ADD")) {
				onLoadPopulateAirports();	
			}			
			clearSearchSection();
			
			if(isFlightUserNoteEnabled = false){
				setVisible("txtUserNotes",false);
			}
			
			var strOnlySche = getParamValue("hdnIncludeScheduled");
			setField("selOperationTypeSearch",defaultOperationType);  	//Search
			setField("selStatusSearch",defaultStatus);	
			if ((getTabValue() != "")  && (getTabValue() != null) 
					&& (getTabValue()[1] !="") && (getTabValue()[1] != null)) {
				var strSearch = getTabValue()[1].split(",");
				setField("txtStartDateSearch",strSearch[0]);
				setField("txtStopDateSearch",strSearch[1]);
				setField("txtFlightNoStartSearch",strSearch[8]);
				setField("txtFlightNoSearch",strSearch[2]);
				setField("selFromStn6",strSearch[3]);
				getFieldByID("selFromStn6").selected=strSearch[3];
				setField("selToStn6",strSearch[4]);
				getFieldByID("selToStn6").selected=strSearch[4];	
				setField("selOperationTypeSearch",strSearch[5]);
				getFieldByID("selOperationTypeSearch").selected=strSearch[5];
				setField("selFlightTypeSearch", strSearch[10]);
				
				setField("hdnStartDateSearch",strSearch[0]);
				setField("hdnStopDateSearch",strSearch[1]);
				setField("hdnFlightNoStartSearch",strSearch[8]);
				setField("hdnFlightNoSearch",strSearch[2]);
				setField("hdnSelFromStn6",strSearch[3]);
				setField("hdnSelToStn6",strSearch[4]);
				setField("hdnSelOperationTypeSearch",strSearch[5]);
				setField("hdnSelFlightTypeSearch", strSearch[10]);
//				setField("hdnSelStatusSearch",defaultStatus);
				setField("hdnIncludeScheduled",strSearch[9]);
				strOnlySche = strSearch[9];
				
				if (strSearch[10] == "true")
					document.getElementById("chkSunday").checked = true;
				if (strSearch[11] == "true")
					document.getElementById("chkMonday").checked = true;
				if (strSearch[12] == "true")
					document.getElementById("chkTuesday").checked = true;
				if (strSearch[13] == "true")
					document.getElementById("chkWednesday").checked = true;
				if (strSearch[14] == "true")
					document.getElementById("chkThursday").checked = true;
				if (strSearch[15] == "true")
					document.getElementById("chkFriday").checked = true;
				if (strSearch[16] == "true")
					document.getElementById("chkSaturday").checked = true;				
				

				setField("selStatusSearch",strSearch[6]);
				getFieldByID("selStatusSearch").selected=strSearch[6];	
				
				if (strSearch[7] == "LOCAL") {
				//	document.forms[0].radTZ[0].checked = true;  // uncomment set the time mode
					setField("hdnTimeMode","LOCAL");
					
				} else {
				//	document.forms[0].radTZ[1].checked = true;
					setField("hdnTimeMode","ZULU");	
				}
				
				//decodeFrequency(strSearch[11]);
			}
			
			var strFlightFrequency = getParamValue("hdnFrequency");
			
			var flightStartTime = getParamValue("hdnStartTime");
			var flightEndTime = getParamValue("hdnEndTime");
			if(strOnlySche == "false"){
				document.getElementById("chkOnlySche").checked = false;
			} else {
				document.getElementById("chkOnlySche").checked = true;
			}
			
//			if(flightStartTime != ""){
//				setField("txtStartTime", flightStartTime);
//			}
//			
//			if(flightEndTime != ""){
//				setField("txtEndTime", flightEndTime);
//			}
						
			if(strFlightFrequency != "" && strFlightFrequency != undefined){
				var strFFre = strFlightFrequency.split(",");
				
				if (strFFre[0] == "true")
					document.getElementById("chkSunday").checked = true;
				if (strFFre[1] == "true")
					document.getElementById("chkMonday").checked = true;
				if (strFFre[2] == "true")
					document.getElementById("chkTuesday").checked = true;
				if (strFFre[3] == "true")
					document.getElementById("chkWednesday").checked = true;
				if (strFFre[4] == "true")
					document.getElementById("chkThursday").checked = true;
				if (strFFre[5] == "true")
					document.getElementById("chkFriday").checked = true;
				if (strFFre[6] == "true")
					document.getElementById("chkSaturday").checked = true;
			}
	
			if(multipleFltTypeVisibility.onlySingle){
				setDisplay("divFlightTypeSearch",false);
				setDisplay("lblFlightType",false);
				
				setDisplay("selectFltType",false);
			}
			
			
			
			disableInputSection(true);
		 
			
			disableLegSection(true);
			disableTerminalSection(true);		
			disableSaveResetButtons(true);
			disableGridButtons(true);
			Disable("btnCopy",true);
			Disable("btnGDS", true);
			Disable("btnManageFlight", true);
			Disable("btnReprotectPAX",true);
			
			
			var fromPage = getParamValue("hdnFromPage");
			var fromFromPage = getParamValue("hdnFromFromPage");
			if((fromPage == "" || fromPage=="INVENTORY") && isInManageFltMode == "MANAGE"){
				setField("hdnFromPage",isInManageFltMode);
				fromPage = getParamValue("hdnFromPage");
			}

			setField("hdnFromPage", getParamValue("hdnFromPage"));	
			if(fromFromPage != "" && fromFromPage != undefined){
				setField("hdnFromFromPage", fromFromPage);
			} else {
				setField("hdnFromFromPage", fromPage);
			}

			
			if (fromPage == "INVENTORY") {
				//setVisible("btnManageFlight", false);
				setField("hdnFromPage", "");
			} else if (fromPage == "SCHEDULE") {
				setField("hdnScheduleId", getParamValue("hdnScheduleId"));
				setField("selStatusSearch","All");
				getFieldByID("selStatusSearch").selected="";
				setVisible("btnManageFlight", false);
				setVisible("tblOptSearch", false);
				
				if(getParamValue("hdnScheduleId") != ""){
					setField("hdnScheduleId", getParamValue("hdnScheduleId"));
					setSchedID(getParamValue("hdnScheduleId"));
					 
				}
				if(getTabValue()[3] != 'undefined' || getTabValue()[3] != ""){
					setField("hdnScheduleId",getTabValue()[3]);
					setField("hdnScheduleId", getParamValue("hdnScheduleId"));
					 
				}
				
				disableSearchSection(true);
				Disable("btnAdd", true);
			}else if(fromPage == "MANAGE" || isInManageFltMode=="MANAGE" || getTabValue()[2] == "MANAGE"){
				if(fromPage == "MANAGE"){
					disableManageSection(true);
					setIsFirstSearch(fromPage);
					document.getElementById("chkOnlySche").checked = true;
				}
				// to resolve reload issues
				setVisible("tblOptSearch", true);
												
				if(getParamValue("hdnScheduleId") != ""){
					setField("hdnScheduleId", getParamValue("hdnScheduleId"));
					setSchedID(getParamValue("hdnScheduleId"));
					 
				}
				if(getTabValue()[3] != 'undefined' || getTabValue()[3] != ""){
					setField("hdnScheduleId",getTabValue()[3]);
					setField("hdnScheduleId", getParamValue("hdnScheduleId"));
					 
				}
				 
				disableSearchSection(false);

				setField("selStatusSearch","All");
				Disable("btnAdd", true);
				Disable("chkOnlySche", true);
				//Disable("txtStartTime", true);
				//Disable("txtEndTime", true);
				//getFieldByID("selStatusSearch").selected="CNX";	
				 
				disableManageSection(true);
			} else {
				disableSearchSection(false);
				Disable("btnAdd", false);
				if (top[1].objTMenu.focusTab == screenID) {
					getFieldByID("txtStartDateSearch").focus();			
				}
			}
			
			if(fromFromPage == "SCHEDULE"){				
				disableSearchSection(true);				
				setVisible("tblOptSearch", false);
				setVisible("btnManageFlight", false);
				//setField("selStatusSearch","");
				Disable("btnAdd", true);
			}

			var error = false;		
			var warning = false;		
			//show the error and other messages		
			if(strMsg != null && strMsg != "" && strMsgType != null && strMsgType != "") {
				showCommonError(strMsgType, strMsg);
				if(strMsgType == "Error") {
					error = true;
					disableInputSection(false);	
					disableLegSection(false);
					disableTerminalSection(false);
					disableSaveResetButtons(false);					
				}
			}
			if(strWarnMsg != null && trim(strWarnMsg) != "") {
				setWarnings(strWarnMsg);
				warning = true;
			}
		
			if ((error) || (warning)) {
				setModel(model);
				strGridRow = getFieldByID("hdnGridRow").value;		
				Disable("selDepDay1", true);
				if (getFieldByID("hdnMode").value == "ADD"){
					Disable("selStatus", true);
					blnAdd = true;
				} else if (getFieldByID("hdnMode").value == "UPDATE") {
					if (checkGridRow()) {
						setFlightScheduleId();
						setIndicators();
						editClick();							
					} else {
						if(model['displayWarning']==''){
							clearInputSection();
							clearLegsection();
						}
						
						disableSaveResetButtons(true);
						disableInputSection(true);									
					}		
				}
//				setTimeMode();			
			}
			setTimeMode();			
			if(model['displayWarning']==''){
				clearInputSection();
				clearLegsection();
			}
			else{
			//	populateRowData(model['gridRow']);
			//	setInputData(model);
				getFieldByID("spnFlights").innerHTML='<font>'+model['displayWarning']+'</font>';
			}
			//show popup
			if (trim(popupmessage).length != 0) {
				alert(popupmessage);
				if(model['displayWarning']==''){
					clearInputSection();
					clearLegsection();
				}
				
				disableSaveResetButtons(true);				
			}if(model['displayWarning']==''){
				clearInputSection();
				clearLegsection();
			}
			else{
				setInputData(model);
				displayLegsEdit(model['hdnLegArray']);
				getFieldByID("spnFlights").innerHTML='<font>'+model['displayWarning']+'</font>';
			}
			setPageEdited(false);
			
			// disable code share view
			if(isCodeShareEnable  == 'false'){
				setDisplay("CodeShareOC",false);
				setDisplay("Table3",false);
			}
			
			if (isFlightNeedReprotect == "true") {
				setModel(model);
				setReprotectFromIdentifier("FLIGHT");
				setField("hdnMode", "CANCEL");
				var intWidth = 720;
				var intHeight = 475; 
				var intLeft = (window.screen.width - 1000) / 2;
				var intTop = (window.screen.height - 710) / 2;
				var	strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=' + intWidth + ',height=' + intHeight + ',resizable=no,top=' + intTop + ',left=' + intLeft;
				top[0].objCancelWindow = window.open("showFile!cancelFlight.action","cancelWindow", strProp);
			}		
			try{
			writeLedgend(objDG);
			setDefaultTimeMode();
			}catch(e){}
			
}
			
	var setDefaultTimeMode = function() {
		if(isTimeInZulu){
			document.frmFlight.radTZ[1].checked = true;
		}else{
			document.frmFlight.radTZ[0].checked = true;
		}
	}
	
//set the  warnings
function setWarnings(strWarnMsg) {
	var ewarnType = strWarnMsg.split(",");
	setModel(model);
	for(var key in model){
		  if(key=='selSeatChargeTemplate'){
			  	var opt = document.createElement('option');
				opt.value = model[key];
				opt.innerHTML = model[key];
				document.getElementById('selSeatChargeTemplate').appendChild(opt);
		  }
	}
	disableInputSection(false);	
	disableLegSection(false);
	disableSaveResetButtons(false);
	var strconfirm = confirm(ewarnType[1]);
		if (strconfirm == true) {
			if(ewarnType[0] == "coflict") setField("hdnAllowConf","true");
			if(ewarnType[0] == "duration") setField("hdnAllowDuration","true");
			if(ewarnType[0] == "model") setField("hdnAllowModel","true");
			if(ewarnType[0] == "segment") setField("hdnAllowSegment","true");
			if(ewarnType[0] == "overlap") setField("hdnAllowOverlap","true");
			document.forms[0].submit();		
			top[2].ShowProgress();
		}else {
			//setting the duration errors
			var legsdrr;
			if (trim(durError).length != 0) {
				legsdrr = durError.split(",");		 
				var errlength  = (legsdrr.length - 1) / 3 ;
				var errlegno;
				for (var i = 0; i < errlength; i++ ) {	
					errlegno = legsdrr[i * 3];
					document.getElementById('spnDuration'+errlegno).innerHTML = legsdrr[(parseInt(i * 3) + 2)];										
					if (legsdrr[(parseInt(i * 3) + 1)] != 0) {
						document.getElementById('spnError'+errlegno).innerHTML = "<font class='mandatory'> X </font>";
					}
				}
			}
		}
}
	
function SearchData(){
	if (isPageEdited()) {
		closeChildWindows();
		var fromPage = getParamValue("hdnFromPage");
		 
		setField("hdnReprotect", "false");
		document.getElementById("frmFlight").target = "_self";			
		setPageEdited(false);
		objOnFocus();
		if(SearchValid()==false) return;
		
		if (document.forms[0].radTZ[1].checked == true) {
			document.forms[0].hdnTimeMode.value="ZULU";
		} else {
			document.forms[0].hdnTimeMode.value="LOCAL";
		}
	    
		//once search button was pressed set the hidden value to "SEARCH"
		document.forms[0].hdnFromPage.value = "";
		document.forms[0].hdnMode.value = "SEARCH";
		setSearchRecNo(1);
		
		setField("hdnFrequency", getFlightFrequency());
//		if (trim(getFieldByID("txtStartTime").value) != "") {
//			if (!timeCheck(1, "txtStartTime"))
//				return;
//			var fstArr = getFieldByID("txtStartTime").value.split(":");
//			if (Number(fstArr[0]) < 0 || Number(fstArr[0]) > 23) {
//				showERRMessage(parent.arrError['invalidStartTime']);
//				return;
//			}
//		}
//		if (trim(getFieldByID("txtEndTime").value) != "") {
//			if (!timeCheck(2, "txtEndTime"))
//				return;
//			var fstArr = getFieldByID("txtEndTime").value.split(":");
//			if (Number(fstArr[0]) < 0 || Number(fstArr[0]) > 23) {
//				showERRMessage(parent.arrError['invalidEndTime']);
//				return;
//			}
//		}
		//setField("hdnStartTime", getFieldByID("txtStartTime").value);
		//setField("hdnEndTime", getFieldByID("txtEndTime").value);
		setField("hdnIncludeScheduled", getChecked("chkOnlySche"));
		
		setField("hdnStartDateSearch",getText("txtStartDateSearch"));
		setField("hdnStopDateSearch",getText("txtStopDateSearch"));
		setField("hdnFlightNoStartSearch",getValue("txtFlightNoStartSearch"));
		setField("hdnFlightNoSearch",getText("txtFlightNoSearch"));
		setField("hdnSelFromStn6",getValue("selFromStn6"));
		setField("hdnSelToStn6",getValue("selToStn6"));
		setField("hdnSelOperationTypeSearch",getValue("selOperationTypeSearch"));
		setField("hdnSelStatusSearch",getValue("selStatusSearch"));	
		setField("hdnSelFlightTypeSearch", getValue("selFlightTypeSearch"));
		setField("hdnModelNo", getFieldByID("selModelNo").value);
		
		if(fromPage == "MANAGE" || isInManageFltMode=="MANAGE"){
			setField("hdnSelStatusSearch","");
			setIsFirstSearch("MANAGE");
			setField("hdnFromPage", fromPage);
			var status = "";
			var schdId = getTabValue()[3];	 
			setSearchManageFlight(fromPage, schdId, status);
						
		}else{
			if(fromPage == "SCHEDULE"){
				setField("hdnSelStatusSearch","");
				setIsFirstSearch("SCHEDULE");
				setField("hdnFromPage", "SCHEDULE");
			}
			setSearchCriteria();
		}
		setTabValue(screenID);
		if(fromPage != "" && getParamValue("hdnFromFromPage") == ""){
			setField("hdnFromFromPage", fromPage);			
		} else if (getParamValue("hdnFromFromPage") != ""){
			//AARESAA-13441 fix
			//setField("hdnSelStatusSearch","");
		}
		document.forms[0].submit();
		top[2].ShowProgress();		
	}
}


var encodeFrequency = function() {
	var strFr = "SMTWHFA";
	 
	if(document.getElementById("chkSunday").checked){
		 
		strFr.replace("S", "1");
	}
	if(document.getElementById("chkMonday").checked){
		strFr.replace("M", "1");
	}
	if(document.getElementById("chkTuesday").checked){
		strFr.replace("T", "1");
	}
	if(document.getElementById("chkWednesday").checked){
		strFr.replace("W", "1");
	}
	
	return strFr;
}

var decodeFrequency = function(tabValue) {
 var arrFrq  = tabValue.split("");
	 if(arrFrq[0] == "1"){
		 document.getElementById("chkSunday").checked = true;
	 }
	 if(arrFrq[1] == "1"){
		 document.getElementById("chkMonday").checked = true;
	 }
	 if(arrFrq[2] == "1"){
		 document.getElementById("chkTuesday").checked = true;
	 }
}
	
var setSearchManageFlight = function(fromPage, schdId, status) {
	tabValue[1] = getText("txtStartDateSearch")+","+getText("txtStopDateSearch")+","+getText("txtFlightNoSearch")+","+getValue("selFromStn6")+","+getValue("selToStn6")+","+getValue("selOperationTypeSearch")+","+status+","+getValue("hdnTimeMode")+","+getValue("txtFlightNoStartSearch")+","+fromPage+","+schdId;
}

function chkvalidate() {

	var selscedtobld = objDG.getSelectedColumn(16);
	var strArray = new Array();
	 var xxxx = "";
	 var strFltData = "";
	 var isAppendDel = false;
	for ( var i = 0; i < selscedtobld.length; i++) {
	 
		if(isAppendDel){ strFltData +="_"; }
		 var fltId = flightData[i][16];
		 var schedualID = "0";
		 if(flightData[i][15] != "&nbsp" && flightData[i][15] != "" && flightData[i][15] != " ") {
			 schedualID = flightData[i][15];
		 }

		if (selscedtobld[i][1]) {
			
			strFltData += fltId+"|"+flightData[i][13]+"|T|"+schedualID+"|"+flightData[i][34]+"|"+flightData[i][42];
				 
		}else{
			strFltData += fltId+"|"+flightData[i][13]+"|F|"+schedualID+"|"+flightData[i][34]+"|"+flightData[i][42];
		}
		isAppendDel = true;
	}

	if (strArray.length > 1) {
		 
		setField("hdnManageFltIds", strArray);
	}
	 
	return strFltData;
  
}// end chkvalidate
function CWindowOpen(strIndex,w,h,s){
	setField("hdnReprotect", "false");
	beforeUnload();
	document.getElementById("frmFlight").target = "_self";	
	var intWidth = 0;
	var intHeight = 0 ; 
	var strProp = '';
	
	if(getTabValue()[3] != 'undefined' && getTabValue()[3] != ""){
		setField("hdnScheduleId",getTabValue()[3]);
		setField("hdnScheduleId", getParamValue("hdnScheduleId"));
	}
	
	var strFlightId = getFieldByID("hdnFlightId").value;
    var scheduleID = getFieldByID("hdnScheduleId").value; 
	var strManageFltIds = chkvalidate();
	setField("hdnManageFltIds", strManageFltIds);
	var strFltDate = getFieldByID("txtDepatureDate").value;
	var modelNo = getFieldByID("selModelNo").value;

	var status = getFieldByID("selStatus").value;
	var operationType = getFieldByID("selOperationType").value;
	
	var strOverlapFltID = getFieldByID("hdnOverlapFlightId").value;
	if (strOverlapFltID == "&nbsp") {
		strOverlapFltID = "";
	}
	
	 
	
	var strSaveMode = "";
	var strMode = getField("hdnMode").value;
	if (strMode == "ADD") {
		strSaveMode = "SAVE";
	}else {
		strSaveMode = "UPDATE";
	}
	
	var strTimeinMode = "";
	if(document.forms[0].radTZ[1].checked == true)
		strTimeinMode = "ZULU";
	else
		strTimeinMode = "LOCAL";
	
	var strFilename = "";
	
	if (document.forms[0].radTZ[1].checked == true) {
		document.forms[0].hdnTimeMode.value="ZULU";
	} else {
		document.forms[0].hdnTimeMode.value="LOCAL";	
	}

	setSearchCriteria();
	setTabValue(screenID);
		
	switch (strIndex){
		case 1:
			var strTrip = flightData[strGridRow][3] + "/" + flightData[strGridRow][4];
			strFilename = "showFile!copyFlight.action?strFlightId="+strFlightId+"&strFltDate="+strFltDate+"&strTrip="+strTrip+"&strOverlapFltID="+strOverlapFltID; 
			intHeight = 325;
			intWidth = 600;
			break;
		case 2 :
			var stLegrArray = buildLeg().split("~");
			strLegs = stLegrArray[0];
			strLegDtls = stLegrArray[1];
			strFilename = "showFlightSegmentValidation.action?strFlightId="+strFlightId+"&strLegs="+strLegs+"&strLegDtls="+strLegDtls+"&strSaveMode="+strSaveMode+"&strStartDate="+strFltDate+"&strOverlapFltID="+strOverlapFltID+"&strModel="+modelNo+"&strStatus="+status+"&hdnTimeMode="+strTimeinMode+"&selOperationTypeSearch="+operationType+"&strFromPage=FLIGHT";
			intHeight = 350;
			intWidth = 720;
			break;
		case 4:
			strFilename = "PaxTransfer.jsp"; 
			intHeight = 410;
			intWidth = 750;
			break;										
		case 5:
			strFilename = "Transfer.jsp"; 
			intHeight = 365;
			intWidth = 750;
			break;	
		case 6:
//			strFilename = "showManageFlights.action?strManageFltIds="+strManageFltIds+"&strSchedId="+scheduleID; 
//			 
//			intHeight = 465;
//			intWidth = 700;
//			$("#tabs").tabs( "enable" , 1);
//			$( "#tabs" ).tabs( "option", "active", 1 );
//			document.getElementById('mngFltPopup').src = strFilename;
//
		break;	
		case 7 :
			var stLegrArray = buildLeg().split("~");
			strLegs = stLegrArray[0];
			strLegDtls = stLegrArray[1];
			strFilename = "showFlightTerminalValidation.action?strFlightId="+strFlightId+"&strLegs="+strLegs+"&strLegDtls="+strLegDtls+"&strSaveMode="+strSaveMode+"&strStartDate="+strFltDate+"&strOverlapFltID="+strOverlapFltID+"&strModel="+modelNo+"&strStatus="+status+"&hdnTimeMode="+strTimeinMode+"&selOperationTypeSearch="+operationType+"&strFromPage=FLIGHT";
			intHeight = 300;
			intWidth = 620;
			break;
		}
		
		strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=' + intWidth + ',height=' + intHeight + ',resizable=no,top=' + ((window.screen.height - intHeight) / 2) + ',left=' + (window.screen.width - intWidth) / 2;
		top[0].objWindow = window.open(strFilename,"CWindow", strProp);
}
	
function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed))	{
		top[0].objWindow.close();
	}
	closeChildWindows();
}

//clear the input section
function clearInputSection() {
	setField("txtDepatureDate", "");
	setField("txtFlightNo", "");
	setField("selOperationType", "");
	setField("selStatus", "");
	setField("selModelNo","");
	
	if(multipleFltTypeVisibility.onlySingle){
		setField("selFlightType",multipleFltTypeVisibility.enabledType);
	}else{
		setField("selFlightType","");
	}

	setField("hdnFlightId","");
	setField("hdnVersion","");
	setField("hdnScheduleId","");
	setField("hdnOverlapFlightId","");
	setField("hdnOverLapSeg", "");
	setField("hdnSegArray","");
	setField("hdnTerminalArray","");
 	setField("hdnOverLapSegId", "");	 	
 	setField("txtRemarks", "");
 	setField("txtUserNotes", "");
 	setField("selSeatChargeTemplate", "");	
	setField("selMealTemplate", "");	
 	
	blnAdd = false;
	setStyleClass("tdReservExcist", "fltStatus01");
	setStyleClass("tdFltOverLapped", "fltStatus01");
	setStyleClass("tdFltChanged", "fltStatus01");
	
	document.getElementById('spnSchedId').innerHTML = "";
	document.getElementById('spnFlightId').innerHTML = "";
	document.getElementById('spnCapacity').innerHTML = "";
	document.getElementById('selSeatChargeTemplate').innerHTML 	="";
	setField("hndCodeShare", "");
	document.getElementById('sel_CodeShare').innerHTML = "";
	setField("hndCsOCCarrierCode", "");
	setField("hndCsOCFlightNo", "");
	setField("txtCsOCCarrierCode", "");
	setField("txtCsOCFlightNo", "");
	
}


//dissable or enable  input section
function disableInputSection(cond) {
	Disable("txtDepatureDate",cond);
	Disable("txtFlightNo",cond);
	Disable("selOperationType",cond);
	Disable("selStatus",cond);
	Disable("selModelNo",cond);
	Disable("selFlightType", cond);
	Disable("txtRemarks", cond);
	Disable("txtUserNotes", cond);
	Disable("selSeatChargeTemplate", cond);
	Disable("selMealTemplate", cond);	

	Disable("txtFlightNoStart",cond);
	Disable("carrier", cond);
	Disable("flightnum", cond);
	document.getElementById("add_CodeShare").disabled = cond;
	document.getElementById("del_CodeShare").disabled = cond;
	document.getElementById("sel_CodeShare").disabled=cond;
	
	if (cond == true) {
		blnCalander = false;
	} else {
		blnCalander = true;
	}
	
}

//clear the search area
function clearSearchSection() {
	setField("txtFlightNoSearch","");
	setField("selFromStn6","");
	setField("selToStn6","");
	strGridRow = -1;	
}

//enable / disable the search area
function disableSearchSection(cond) {
	Disable("txtFlightNoStartSearch",cond);
	Disable("txtFlightNoSearch",cond);
	Disable("selFromStn6",cond);
	Disable("selToStn6",cond);
	Disable("f",cond);
	Disable("txtStartDateSearch",cond);
	Disable("txtStopDateSearch",cond);
	Disable("selOperationTypeSearch",cond);
	Disable("selStatusSearch",cond);
	Disable("radTZ",cond);	
}

//enable / disable the search area
function disableManageSection(cond) {
	Disable("txtFlightNoStartSearch",cond);
	Disable("txtFlightNoSearch",cond);
	Disable("selFromStn6",cond);
	Disable("selToStn6",cond);
	Disable("f",cond);
	//Disable("txtStartDateSearch",cond);
	//Disable("txtStopDateSearch",cond);
	Disable("selOperationTypeSearch",cond);
	Disable("selStatusSearch",cond);
	Disable("radTZ",cond);	
}


//enable dissable save reset button
function disableSaveResetButtons(cond) {
	Disable("btnSave",cond);
	Disable("btnReset",cond);
}

//enable / disable grid buttons
function disableGridButtons(cond) {
	Disable("btnCancel",cond);
	Disable("btnEdit",cond);
	Disable("btnSeatAllocate",cond);
	if(cond)
		setField("hdnViewModeOnly","true");
	
}

//chech whether the same for from and to
function checkSameLeg(from,to) {
	
	var opType = getFieldByID("selOperationType").value;
	var bypassChecksForOperations = false;
	if(opType!=null && opType == 6) bypassChecksForOperations = true;	
	
	if (!bypassChecksForOperations && (trim(getValue(from)) != "") && (trim(getValue(to)) != "") && getValue(from) == getValue(to) ) {
		showERRMessage(sameairport);
		getFieldByID(to).focus();	
	}							 
	
}

//sets the aircraft model capacity
function loadAircraftModelCapacity() {
	objOnFocus();
	dataChanged();
	var ModelSelected = getFieldByID("selModelNo").value;
	for (var i = 0 ; i < arrAircraftModelData.length ;i++){
		if ((ModelSelected != "") && (ModelSelected == arrAircraftModelData[i][0])) {		
			document.getElementById('spnCapacity').innerHTML = arrAircraftModelData[i][1];
			break;
		}
	}
//	document.getElementById('spnAircraftModel').innerHTML = ModelSelected;
	//var seatTemplates='A310&2&2|A310&3&3';
	var selectChargeTemplate = document.getElementById('selSeatChargeTemplate');
	selectChargeTemplate.options.length = 0;
	var n = seatTemplates.split("|"); 
	var optFirts = document.createElement('option');
	optFirts.value = "";
	optFirts.innerHTML = "";
	selectChargeTemplate.appendChild(optFirts);
	
	for (var i=0;i<n.length-1;i++)
	{
		var n1=n[i].split("&"); 
		if(n1[0]==ModelSelected){
			var opt = document.createElement('option');
		    opt.value = n1[1];
		    opt.innerHTML = n1[2];
		    selectChargeTemplate.appendChild(opt);
		}	
	}
	setField("hdnMainMode", "isNeedChecked");
}

//dynamically populates the aircraft model list accoding to the selected operation type
function onChangeLoadAircraftModels(){
	
	var operationType = getFieldByID("selOperationType").value;
	
	for(var i=0; i<arrOperationTypeData.length;i++){
		if(arrOperationTypeData[i][0]==operationType){
			var aircraftTypeCode = arrOperationTypeData[i][2];
			
			var strOperationTypes="<option value=''></option>"
			for(var j=0;j<arrAircraftModelData.length;j++){
				
				if(arrAircraftModelData[j][4]==aircraftTypeCode){   
					 if(arrAircraftModelData[j][3]=="ACT"){ 
						 strOperationTypes = strOperationTypes+ "<option value='" + arrAircraftModelData[j][0] + "'>" + arrAircraftModelData[j][5] +"("+arrAircraftModelData[j][0]+")</option>";
					 }
				}
			}
			document.getElementById('selModelNo').innerHTML = strOperationTypes;
		}
	}
	setField("selModelNo", "");
}


//this exceted when the grid is clicked
function gridClick(strRowNo) {
	if (isPageEdited()) {
		setPageEdited(false);
		objOnFocus();
		onLoadPopulateAllAirports();	
		clearInputSection();
		clearLegsection();
		disableInputSection(true);	
		disableLegSection(true);
		disableSaveResetButtons(true);
		disableGridButtons(true);
		Disable("btnReprotectPAX",true);
		Disable("btnCopy", false);
		Disable("btnGDS", false);
		Disable("btnPubMessages", false);	
		// check gds flight and enable msg button
		if(flightData[strRowNo][48] != undefined && flightData[strRowNo][48] == "true"){
			$('#btnMsgHistory').show();
			Disable("btnMsgHistory", false);
		}else{
			Disable("btnMsgHistory", true);
			$('#btnMsgHistory').hide();
		}
		Disable("btnViewHistory",false);
		Disable("btnNoteRollForwardSearch",false);
		
		populateRowData(strRowNo);
		focusTab(0);
		setPageEdited(false);
		hidePublisheMessageAudit(flightData[strRowNo][31]);
		flightDeptTime = flightData[strGridRow][26];
	}
}

function populateRowData(strRowNo) {
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;
		var scheduleId = strRowData[14];
		setField("hdnGridRow", strGridRow);		
		setField("hdnScheduleId",strRowData[14]);
		if(blnZuluTime) {
			setField("txtDepatureDate",strRowData[1]);
			arrLegs = flightData[strRowNo][18];
			setField("hdnLegArray",flightData[strRowNo][18]);
			setField("hdnFlightDate",flightData[strRowNo][29]);
		}else {
			setField("txtDepatureDate",flightData[strRowNo][22]);
			arrLegs = flightData[strRowNo][23];
			setField("hdnLegArray",flightData[strRowNo][23]);
			setField("hdnFlightDate",flightData[strRowNo][29]);
		}
		
		
		setField("txtFlightNo",strRowData[0].substr(2,5));		
		setField("txtFlightNoStart",strRowData[0].substr(0,2));
		setField("hdnFlightNo",strRowData[0]);
		setField("selOperationType",strRowData[7]);
		setField("selStatus",flightData[strRowNo][13]);
		setField("hdnPrevStatus",flightData[strRowNo][13]);
		
		setField("selModelNo",flightData[strRowNo][17]);
		setField("hdnModelNo",flightData[strRowNo][17]);
		
	//	document.getElementById('spnAircraftModel').innerHTML =flightData[strRowNo][17];
		if(document.getElementById("selModelNo").value==""){
			
	            var obj=document.getElementById("selModelNo");         
	                   
	           opt = document.createElement("option");
	           opt.value = flightData[strRowNo][17];
	           opt.text=flightData[strRowNo][43]+"("+ flightData[strRowNo][17]+")";
	           opt.disabled = 'disabled';
	           obj.appendChild(opt);
	           
	            document.getElementById("selModelNo").value = flightData[strRowNo][17];
		}
		else{
			for(i= document.getElementById("selModelNo").length-1;i>=0;i--)
			{  
				var isCurrentModel = (document.getElementById("selModelNo").options[i].value == flightData[strRowNo][17]);
			if( document.getElementById("selModelNo").options[i].disabled && !isCurrentModel ){
				 document.getElementById("selModelNo").remove(i);
				}
			}
		
		}
		if (strRowData[8] == "&nbsp") {
			setField("hdnOverlapFlightId","");
		}else {
			setField("hdnOverlapFlightId",strRowData[8]);
		}
		
			
		var ModelSelected = flightData[strRowNo][17];	
		for (var i = 0 ; i < arrAircraftModelData.length ;i++){
			if ((ModelSelected != "")&&(ModelSelected == arrAircraftModelData[i][0])){
					document.getElementById('spnCapacity').innerHTML = arrAircraftModelData[i][1];
			}
		}	
		var selectChargeTemplate = document.getElementById('selSeatChargeTemplate');
		selectChargeTemplate.options.length = 0;
		var optFirst = document.createElement('option');
		optFirst.value = "";
		optFirst.innerHTML = "";
		selectChargeTemplate.appendChild(optFirst);
		var n = seatTemplates.split("|"); 
		for (var i=0;i<n.length-1;i++)
		{
			var n1=n[i].split("&"); 
			if(n1[0]==ModelSelected){
				var opt = document.createElement('option');
				opt.value = n1[1];
				opt.innerHTML = n1[2];
				selectChargeTemplate.appendChild(opt);
			}	
		}
			
		
		var csold = flightData[strRowNo][11];
		var chold = flightData[strRowNo][12];
		var reser = parseInt(csold) + parseInt(chold);
				
		if(trim(flightData[strRowNo][9]) != "&nbsp")
			setStyleClass("tdFltOverLapped", "fltStatus02");		
		if(reser > 0)
			setStyleClass("tdReservExcist", "fltStatus04");
		if(flightData[strRowNo][19] == "true")
			setStyleClass("tdFltChanged", "fltStatus03");
				
		document.getElementById('spnSchedId').innerHTML = strRowData[14];
		document.getElementById('spnFlightId').innerHTML = flightData[strRowNo][16];
		setField("hdnVersion",flightData[strRowNo][20]);
		setField("hdnFlightId",flightData[strRowNo][16]);
		
		//display the legs	
		displayLegs(arrLegs);
		//enabling buttons	
		//set segment
		setSegmentArray(flightData[strRowNo][27]);
		var cdepdate = flightData[strRowNo][2];
		var cstatus =  flightData[strRowNo][13];
		
		var Today = getFieldByID("hdnCurrentDate").value;
		var diffMonth = (Today.split("/")[2] - cdepdate.split("/")[2]) * 12 + Today.split("/")[1] - cdepdate.split("/")[1]; 
		var diffDays = Today.split("/")[0] - cdepdate.split("/")[0];
		var isWithinActiveTimeDuration = false;
		if (diffDays < pastFlightActivationDurationInDays) {
			isWithinActiveTimeDuration = true;
		}
		if(cstatus=='ACT'){
			Disable("btnReprotectPAX", false);
		}
		cdepdate = formatDate(cdepdate,"dd/mm/yyyy","dd-mmm-yyyy");		
		Today = formatDate(Today,"dd/mm/yyyy","dd-mmm-yyyy");
		setField("hdnGDSPublishing", flightData[strRowNo][31]);
		if (compareDates(cdepdate, Today, '<')) { 
			//departure date less than or equals to current date 
			//Seat allocation button is enabled
			disableGridButtons(true);
			Disable("btnSeatAllocate", false);
			if ((isDelFlownFlts == "true" || isDelFlownFlts == true) && cstatus != "CNX" && isWithinActiveTimeDuration
				&& modificationForPastFlight) {
				Disable("btnCancel", false);
			}
			if (privActivatePastFlights == "true" && cstatus != "CNX" && isWithinActiveTimeDuration && modificationForPastFlight) {
				Disable("btnEdit", false);
			}
			if (modificationForPastFlight) {
				if ((cstatus == "CLS") && (reser > 0) || (reqFlightsToBeReprotected && (cstatus == "CNX") && (reser > 0))) { // Closed and having reservation
					// Reprotect button is enabled
					Disable("btnReprotectPAX", false);
				}
			}
		} else {
			disableGridButtons(false);
		
			if (cstatus == "CLS") { // Closed
				// Edit & seat allocation button is enabled
				Disable("btnCancel", true);	
				Disable("btnEdit", false);				
				Disable("btnSeatAllocate", false);
			}
			
			if ((cstatus == "CLS") && (reser > 0) || (reqFlightsToBeReprotected && (cstatus == "CNX") && (reser > 0))) { // Closed and having reservation
				// Reprotect button is enabled
				Disable("btnReprotectPAX", false);	
			}
			
			if ((cstatus == "CNX")) {
				Disable("btnCancel", true);	
				Disable("btnEdit", true);				
			}
		}		
			
		if (reser > 0) {
			airportDisable(true);
		}
		if (trim(strRowData[14]) != "&nbsp") {
			airportDisable(true);
			Disable("txtDepatureDate", true);
		}
		
		if (getValue("hdnTimeMode") == "LOCAL") {
			setLocaTime();
		} else {
			setZuluTime();
		}
		if(scheduleId != "&nbsp" && flightData[strRowNo][47] !=1 ){
			if(flightData[strRowNo][31] == "")
				Disable("btnGDS", true);
			setField("hdnViewModeOnly", "true");
		}
		else
			setField("hdnViewModeOnly", "false");
		
		if(flightData[strRowNo][44] != "" || flightData[strRowNo][45] != ""){
			Disable("btnGDS", true);
		}
		
		setField("hdnTerminalArray", flightData[strRowNo][35]);
		
		setField("selFlightType", flightData[strRowNo][36]);
		
		setField("txtRemarks", flightData[strRowNo][38]);		
		
		setField("selMealTemplate", flightData[strRowNo][39]);
		
		setField("selSeatChargeTemplate", flightData[strRowNo][40]);		
		
		setField("txtUserNotes", flightData[strRowNo][41]);
		
		setField("hdnTxtUserNotes",flightData[strRowNo][41]);
		
		setField("txtCsOCCarrierCode", flightData[strRowNo][44]);	
		
		setField("txtCsOCFlightNo", flightData[strRowNo][45]);	
		
		setField("hndCsOCFlightNo", flightData[strRowNo][45]);		

		setField("hdnMainMode", "isNeedChecked");
		
		getFieldByID("spnFlights").innerHTML="";
		
		//CodeShare view
		setCodeShareArr(flightData[strRowNo][46]);
		
		disableCtrlForCodeShare();
		
		if(flightData[strRowNo][46]=="" || !flightData[strRowNo][46].length > 0){
			setField("hdnHaveMCFlight", "false");
		}else  {
			setField("hdnHaveMCFlight", "true");
		}
		
		hidePublisheMessageAudit(flightData[strRowNo][31]);
}


//clear the leg section
function clearLegsection() {
	for(var j=1;j < 6;j++) {
		setField("selFromStn"+j, "");
		setField("selToStn"+j, "");
		setField("txtDepTime"+j, "");
		setField("selDepDay"+j, 0);
		setField("txtArrTime"+j, "");
		setField("selArrDay"+j, 0);
		
		document.getElementById('spnDuration'+j).innerHTML = "";
		document.getElementById('spnError'+j).innerHTML = "";
	}
	
	
}

//enable dissable leg section
function disableLegSection(cond) {
	for(var j=1;j < 6;j++) {
		Disable("selFromStn"+j,cond);
		Disable("selToStn"+j,cond);
		Disable("txtDepTime"+j,cond);
		Disable("selDepDay"+j,cond);
		Disable("txtArrTime"+j,cond);
		Disable("selArrDay"+j,cond);
	}
	Disable("btnValidseg", cond);
}
//enable dissable leg section
function disableTerminalSection(cond) {	
	Disable("btnValidTerminal", cond);
}
//the add function
function addClick() {
	if (isPageEdited()) {
		closeChildWindows();
		setField("hdnReprotect", "false");
		document.getElementById("frmFlight").target = "_self";			
		setPageEdited(false);
		objOnFocus();
		onLoadPopulateAirports();
		disableInputSection(false);
		disableLegSection(false);
		disableTerminalSection(false);		
		Disable("selDepDay1",true);
		Disable("btnViewHistory",true);
		Disable("btnMsgHistory", true);
		Disable("btnNoteRollForwardSearch",true);
		
		clearInputSection();
		clearLegsection();	
		setField("hdnMode", "ADD");
		setField("selStatus", "CRE");
		Disable("selStatus", true);
		disableSaveResetButtons(false);
		disableGridButtons(true);
		Disable("btnReprotectPAX",true);
		Disable("btnCopy",true);
		Disable("btnGDS", true);
		
		setField("txtFlightNoStart", defCarrCode);
		blnAdd = true;
		getFieldByID("txtDepatureDate").focus();
		//Haider set the gdsIds list to defualt 
//		gdsIds.options.length =0;
//		for(k=0;k<copyOfGDSIds.length;k++){
//			gdsIds.options[k] = copyOfGDSIds[k];
//		}
//		setField("selGDSPublishing", "-1");
		
	}
}

//the edit function
function editClick() {
	if (isPageEdited()) {
		closeChildWindows();
		setField("hdnReprotect", "false");
		document.getElementById("frmFlight").target = "_self";		
		setPageEdited(false);
//		objOnFocus();
		disableInputSection(false);
		//Haider disable gds select when editing Flight
		Disable("selGDSPublishing", true);
		disableLegSection(false);
		disableTerminalSection(false);
		if (scheduleExist()) { 
			Disable("selOperationType", true);		
			Disable("txtDepatureDate", true);
			blnCalander = false;	
			Disable("txtFlightNoStart", true);		
			Disable("txtFlightNo", true);				
			airportDisable(true);
			Disable("selDepDay1", false);	
		} else if (reservationExist()) {
			Disable("txtDepatureDate", true);
			blnCalander = false;	
			Disable("txtFlightNoStart", true);		
			Disable("txtFlightNo", true);				
			airportDisable(true);
			Disable("selDepDay1", false);
		}
		 
		if(isEditFlightNo=="true" || isEditFlightNo==true) {
			Disable("txtFlightNo", false);	
		}
		
		if(isOverlapped()) {
	        overlapLegDisable();	
		}
		if (getFieldByID("selFromStn1").disabled) {
			Disable("selDepDay1", false);
		} else {
			Disable("selDepDay1", true);	
		}
		setField("hdnMode", "UPDATE");
		Disable("btnEdit", true);	
		disableSaveResetButtons(false);
		disableGridButtons("true");	
		Disable("btnCopy", true);
		Disable("btnGDS", true);	
	//	setField("hdnMainMode", "isNeedChecked");
		if(document.getElementById("txtCsOCFlightNo").value != "" && document.getElementById("txtCsOCCarrierCode").value != ""){
			Disable("carrier", true);
			Disable("flightnum", true);
			document.getElementById("add_CodeShare").disabled = true;
			document.getElementById("del_CodeShare").disabled = true;
			document.getElementById("sel_CodeShare").disabled=true;
		}
	}
}

var flashBox = function(control) {
	
}

function amendFlight() {
	closeChildWindows();
	setField("hdnReprotect", "false");
	document.getElementById("frmFlight").target = "_self";	
	var intWidth = 720;
	var intHeight = 220; 
	var	strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=' + intWidth + ',height=' + intHeight + ',resizable=no,top=' + ((window.screen.height - intHeight) / 2) + ',left=' + (window.screen.width - intWidth) / 2;
	var strActionUrl = "showFile!amendFlight.action";
	strActionUrl += "?hdnDepatureDateTime=" + flightDeptTime;
	top[0].objWindow = window.open(strActionUrl, "CWindow", strProp);
}

//the cancel function
function cancelClick() {
	if (isPageEdited()) {
		closeChildWindows();
		setField("hdnReprotect", "false");
		document.getElementById("frmFlight").target = "_self";		
		setPageEdited(false);
		objOnFocus();
		disableSaveResetButtons(true);
		disableGridButtons("true");	
		Disable("btnCopy", true);		
		Disable("btnGDS", true);
		 
		var msg = "Are you sure you want to cancel the selected record ?";
		if (isOverlapped()) {
			msg = "Do You Wish to Cancel Ovelapping Record ?";
		}
		if(isFlown()) {
			
			msg = "This flight is already flown. \n Are you sure you want to cancel the selected record ?"
		}
		
		var strconfirm = confirm(msg);
		if (strconfirm == true) {			
			setField("hdnMode", "CANCEL");
			setField("hdnRecNo", tabValue[0]);	
			setField("hdnModelNo", getFieldByID("selModelNo").value);
			setSearchCriteria();
			setTabValue(screenID);			
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	}
}

//the copy function
function copyClick() {
	if (isPageEdited()) {
		closeChildWindows();
		setField("hdnReprotect", "false");
		document.getElementById("frmFlight").target = "_self";		
		setPageEdited(false);
		objOnFocus();
		disableSaveResetButtons(true);
		disableGridButtons("true");	
		Disable("btnCopy", true);
		Disable("btnGDS", true);
				
		setField("hdnMode","COPY");
		CWindowOpen(1);
	}
}


//checks the validity of the input section
function validateFlightInput() {
	var depDate = getFieldByID("txtDepatureDate").value;
	var Today = getFieldByID("hdnCurrentDate").value;
	var opType = getFieldByID("selOperationType").value;

	if (trim(depDate) == "") {
		showERRMessage(dptdatenull);
		getFieldByID("txtDepatureDate").focus();
		return false
	}
	if (!dateChk("txtDepatureDate")) {
		showERRMessage(invalidDate);
		if (!getFieldByID("txtDepatureDate").disabled) {
			getFieldByID("txtDepatureDate").focus();
		}
		return false;
	}
	if (opType != 6 && (!getFieldByID("txtDepatureDate").disabled) 
			&& (!CheckDates(Today,depDate))) {
		showERRMessage(lesscurent);
		getFieldByID("txtDepatureDate").focus();
		return false
	}
	var flightType = getFieldByID("selFlightType").value;
	if(trim(flightType) == ""){
		showERRMessage(flttypenull);
		getFieldByID("txtDepatureDate").focus();
		return false
	}
	var flightNo = getFieldByID("txtFlightNo").value;
	if(trim(flightNo) == "") {
		showERRMessage(fltnonull);
		getFieldByID("txtFlightNo").focus();
		return false
	}
	if (!flightValid("txtFlightNo")) {
		showERRMessage(invalidFltNo);
		getFieldByID("txtFlightNo").focus();
		return false;
	}	
	
	var strfltVAl = getVal("txtFlightNo").substr(0,2);
	if (trim(opType) == "") {
		showERRMessage(optypenull);
		getFieldByID("selOperationType").focus();
		return false
	}

	if(opType != 6 && privAddOnlyOpFlights == 'true') {
		showERRMessage(onlyOpTypeOp);
		getFieldByID("selOperationType").focus();
		return false		
	}
	
	var aircfraftModel = getFieldByID("selModelNo").value;
	if (trim(aircfraftModel) == "") {
		showERRMessage(modelnull);
		getFieldByID("selModelNo").focus();
		return false
	}
	if (!checkAircraftModel()) {
		showERRMessage(modelinactive);
		if (!getFieldByID("selModelNo").disabled) {
			getFieldByID("selModelNo").focus();
		}
		return false;	
	}
	
	var svecarrier = trim(getVal("txtFlightNoStart"));
	var blnValid = false;
	for(var cl=0;cl < arrCarriers.length;cl++) {
		if(arrCarriers[cl] == svecarrier) {
			blnValid = true;
			break;
		}
	}
	if(!blnValid) {
		showERRMessage(invalidcarrier);
		getFieldByID("txtFlightNoStart").focus();
		return false;
	}
	
	if (trim(getVal("txtRemarks"))!="" && !isAlphaNumericTextArea(trim(getVal("txtRemarks")))) {
		showERRMessage(invalidCharRem);
		getFieldByID("txtRemarks").focus();
		return false;
	}	
	if((trim(getVal("txtCsOCCarrierCode"))!="" || trim(getVal("txtCsOCFlightNo"))!= "") && trim(getFieldByID("sel_CodeShare").value) != ""){
		showERRMessage(invalidOCandMC);
		getFieldByID("sel_CodeShare").focus();
		return false;
	}
	return true;
}

function optypeValidation(){
	var opType = getFieldByID("selOperationType").value;
	var valid = false;
	if(opType != 6 ) {
		valid = true;		
	} else {
		showERRMessage(editOpTypeOp);
	}
	return valid;
}

function legValidation() {
	var legfrom1 = getFieldByID("selFromStn1").value;
	if (trim(legfrom1) == "") {
		showERRMessage(fromnull);
		getFieldByID("selFromStn1").focus();
		return false;
	}
	
	var opType = getFieldByID("selOperationType").value;
	var bypassChecksForOperations = false;
	if(opType!=null && opType == 6) bypassChecksForOperations = true;
	
	for (var j=1;j < 6;j++) {
		var legFrom = getFieldByID("selFromStn"+j).value;
		if (trim(legFrom) != "") {
			if (!checkDepArrLeg(legFrom, j, true)) {
				showERRMessage(depleginactive);
				if (!getFieldByID("selFromStn"+j).disabled) {
					getFieldByID("selFromStn"+j).focus();
				}
				return false;			
			}
			var legTo = getFieldByID("selToStn"+j).value;
			if (trim(legTo) == "" ) {
				showERRMessage(tonull);
				getFieldByID("selToStn"+j).focus();
				return false;
			}else {
				if (!checkDepArrLeg(legTo, j, false)) {
					showERRMessage(arrleginactive);
					if (!getFieldByID("selToStn"+j).disabled) {
						getFieldByID("selToStn"+j).focus();
					}
					return false;			
				}			
				if ( !bypassChecksForOperations && trim(legFrom) == trim(legTo)) {
					showERRMessage(sameairport);
					getFieldByID("selToStn"+j).focus();
					return false;
				}
			
				var depTime = getFieldByID("txtDepTime"+j).value;
				if (trim(depTime) == "") {
					showERRMessage(deptimenull);
					getFieldByID("txtDepTime"+j).focus();
					return false;
				}
				if (IsValidTime(depTime) == false) {
					showERRMessage(deptimeinvalid);
					getFieldByID("txtDepTime"+j).focus();
					return false;				
				}	 
				var arrTime = getFieldByID("txtArrTime"+j).value;
				if(trim(arrTime) == "") {
					showERRMessage(arrtimenull);
					getFieldByID("txtArrTime"+j).focus();
					return false;
				}
				if(IsValidTime(arrTime) == false) {
					showERRMessage(arrtimeinvalid);
					getFieldByID("txtArrTime"+j).focus();
					return false;
				}				
				if(j > 1) {
					var arnStan =  getFieldByID("selToStn"+(j-1)).value;
					if (trim(legFrom) != trim(arnStan)) {
						showERRMessage(arrdepsame);
						getFieldByID("selFromStn"+j).focus();
						return false;
					}
					var k = j-1;
					if (checkMinDuration("selFromStn"+j,"txtArrTime"+k, "txtDepTime"+j,"selArrDay"+k,"selDepDay"+j) == false) {					
						showERRMessage(indeptime);
						getFieldByID("txtDepTime"+j).focus();
						return false;
					}
				}
			}
		}			
	}
	return true;
}

function validSegment() {	
	var mode = getField("hdnMode").value;
	if (validateFlightInput() == false) return false;
	if (buildLeg() == false ) return false;
	var legs = buildLeg().split("~");
	var arrLegs = legs[0].split(",");
	var isMultiLeg = !(arrLegs.length < 4);
	var allow = false;
	if (mode == "ADD") {
		if (isMultiLeg) {
			allow = true;
		} else {
			allow = false;
		}
	} else {
		if (mode == "UPDATE") {
			if (!isMultiLeg) {
				allow = false;
			} else if (scheduleExist()) {
				allow = false;
			//} else if (reservationExist()) {
			//	allow = false;			
			} else {
				allow = true;
			}
		}
	}
	if (!allow) {
		if (!isMultiLeg) {
			showERRMessage(singlelegvalid);
		} else {
			showERRMessage(mlitilegvalid);
		}
		return false;	
	}
	CWindowOpen(2);
}

function validTerminal() {	
	var mode = getField("hdnMode").value;
	if (validateFlightInput() == false) return false;
	if (buildLeg() == false ) return false;
	var legs = buildLeg().split("~");
	var arrLegs = legs[0].split(",");
	var isMultiLeg = !(arrLegs.length < 4);
	var allow = true;
	if (mode == "ADD") {
		if (isMultiLeg) {
			allow = true;
		} 
	} else {
		if (mode == "UPDATE") {
			if (scheduleExist()) {
				allow = false;					
			} else {
				allow = true;
			}
		}
	}
	if (!allow) {		
		//showERRMessage(mlitilegvalid);		
		//return false;	
	}
	CWindowOpen(7);
}
function buildLeg() {
	if(legValidation() == false) return false;
	var legArr = "";
	var fromtoarray = "";
	for (var j=1;j < 6;j++) {
		
		var legFrom = getFieldByID("selFromStn"+j).value;
		
		if (trim(legFrom) != "") {
			var legTo =   getFieldByID("selToStn"+j).value;
			var depTime = getFieldByID("txtDepTime"+j).value;		
			var arrTime = getFieldByID("txtArrTime"+j).value;		
			var depDay =  getFieldByID("selDepDay"+j).value;
			var arrDay =  getFieldByID("selArrDay"+j).value;
			legArr += legFrom +"_"+ legTo +"_"+ depTime +"_"+ arrTime +"_"+ depDay +"_"+ arrDay + ",";
			fromtoarray += legFrom +","+ legTo +",";
		}
		
	}
	return fromtoarray +"~"+ legArr;
}
function buildLegForEdit() {
	if(legValidation() == false) return false;
	
	var legArr = new Array(6);
	for (var j=1;j < 6;j++) {
		legArr[j-1] = new Array(9);
		legArr[j-1][0] = j;
		legArr[j-1][1] = getFieldByID("selFromStn"+j).value;
		legArr[j-1][2] = getFieldByID("selToStn"+j).value;
		legArr[j-1][3] = getFieldByID("txtDepTime"+j).value;
		legArr[j-1][4] = getFieldByID("selDepDay"+j).value;
		legArr[j-1][5] = getFieldByID("txtArrTime"+j).value;
		legArr[j-1][6] = getFieldByID("selArrDay"+j).value;
		legArr[j-1][7] = getFieldByID("spnDuration"+j).innerHTML;
		legArr[j-1][8] = getFieldByID("spnError"+j).innerHTML+'~';
	}
	
	return legArr;
}
function viewHistoryClick(){
	var fromDate = $("#fromDatePicker").val();
	var toDate = $("#toDatePicker").val();
	if(fromDate==""){
		showERRMessage("Please select the from date");
		return false;
	}
	if(toDate==""){
		showERRMessage("Please select the to date");
		return false;
	}
	$("#fromDate").val(fromDate);
	$("#toDate").val(toDate);
	if ($("#fromDatePicker").datepicker('getDate')>$("#toDatePicker").datepicker('getDate')){
		showERRMessage("The from date must fall earlier than the to date");
		return false;
	}
	setField("hdnShowResHistory", getChecked("showResHistory"));	
	setField("hdnMode","VIEW");
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'	
		top[0].objWindow = window.open("about:blank","CWindow",strProp);
		var objForm = document.getElementById("frmFlight");
		objForm.target = "CWindow";	
		objForm.action = "showFlight.action";
		objForm.submit();
		
		$("#popup").dialog('close');
		clearPopup();
}

function viewEnterDatesClick(val){
	
	if(val!=null && val.trim()!=""){
		setField("hdnAuditType",val);
	}else{
		setField("hdnAuditType","HISTORY");
	}
	
	$("#popup").dialog({
		open: function() {
			$('#fromDatePicker').removeAttr("disabled");
			$('#toDatePicker').removeAttr("disabled");
			if(document.getElementById("showResHistory") != null) {
				document.getElementById("showResHistory").checked = true;
			}
			document.getElementById("popup").style["display"]="inherit";
		},
		close: function () {
			$('#fromDatePicker').datepicker('hide');
			$('#toDatePicker').datepicker('hide');
			document.getElementById("popup").style["display"]="none";
			clearPopup();
		}
	});

	$("#fromDatePicker").datepicker({
		buttonImage: '../../images/calendar_no_cache.gif',
	    buttonImageOnly: true,
	    changeMonth: true,
	    changeYear: true,
	    showOn: 'both',
	    dateFormat: "dd/mm/yy",
	    buttonText: "View Calander"
	});
	$( "#fromDatePicker" ).datepicker( "option", "maxDate", "+0d" );
	
	$("#toDatePicker").datepicker({
		buttonImage: '../../images/calendar_no_cache.gif',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showOn: 'both',
	    dateFormat: "dd/mm/yy",
	    buttonText: "View Calander"
	});
	$( "#toDatePicker" ).datepicker( "option", "maxDate", "+0d" );
}

function clearPopup(){
    $("#fromDatePicker").val("");	
    $("#toDatePicker").val("");
}

function saveClick() {
	
	closeChildWindows();
	setField("hdnReprotect", "false");
	document.getElementById("frmFlight").target = "_self";	
	setPageEdited(false);
	setCodeShareArray();
	setField("hndCsOCCarrierCode", (getText("txtCsOCCarrierCode") == "undefined" || getText("txtCsOCCarrierCode") == null) ? "" : trim(getText("txtCsOCCarrierCode")));
	setField("hndCsOCFlightNo", (getText("txtCsOCFlightNo") == "undefined" || getText("txtCsOCFlightNo") == null) ? "" : trim(getText("txtCsOCFlightNo")));
	objOnFocus();
	var strMode = getField("hdnMode").value;
	var mainMode = getField("hdnMainMode").value;
	var isValidFlightInputs = validateFlightInput();
	
	if (mainMode=='isNeedChecked'&& isValidFlightInputs == false) {
		if (getFieldByID("selModelNo").value != getFieldByID("hdnModelNo").value){
			setField("hdnMainMode", "isNeedChecked");
		}
		else{
			setField("hdnMainMode", "");
		}
	} else if(isValidFlightInputs == true){
		setField("hdnRecNo", tabValue[0]);	
		if (strMode=="ADD" && legValidation() == true) {
			setField("hdnMode","ADD");
			document.forms[0].hdnMode.value="ADD";
			airportDisable(false);
			Disable("selDepDay1", false);
			disableInputSection(false);	
			document.forms[0].submit();		
			top[2].ShowProgress();
		} else if(strMode=="UPDATE") {
			
			if (mainMode=='isNeedChecked'){
				if (getFieldByID("selModelNo").value != getFieldByID("hdnModelNo").value){
					setField("hdnMainMode", "isNeedChecked");
				}
				else{
					setField("hdnMainMode", "");
				}
			}
			var isConfirm =  true;
			
		/*	if(mainMode=='isNeedChecked'&&isFlightOverbook()){
				isConfirm = window.confirm("Flight will be Overbooked.Do you wish to continue?");
			} else {
				isConfirm = true;
			}*/
			
			if(isConfirm){
				if(!setStatusChange() || setStatusChange() == "false" || !optypeValidation()) return false;
				if (!checkOverlapping()) {
					return false;
				}		
				setField("hdnLegArray",buildLegForEdit());
				setField("hdnMode","UPDATE");
				document.forms[0].hdnMode.value="UPDATE";
				if (checkAmendAlert()) {
					amendFlight();
				} else {
					airportDisable(false);
					Disable("selDepDay1", false);
					disableInputSection(false);	
					document.forms[0].submit();		
					top[2].ShowProgress();			
				}
			}
		}
	}	
}

function isFlightOverbook(){
	for(var i = 0; i < arrAircraftModelData.length;i++){
		if(getFieldByID("selModelNo").value == arrAircraftModelData[i][0]){
			var modelCapacity = arrAircraftModelData[i][1].substring(14,17);
			if(parseInt(strRowData[10]) + parseInt(strRowData[11]) > parseInt(modelCapacity)){
				return true;
			}
			return false;
		}
	}
	return false;
}

var splitIndex = 0;
var splitArray = new Array();

function split(string,text) {
	splitArray = string.split(text);
	splitIndex = splitArray.length;
}
function getMinutes(string){
	splitIndex = 0;
	if (string != "") {
		split(string,':');
	}		
	for (var i=splitIndex-1, j=1, answer=0; i>=0; i=i-1, j=j*60)
		answer += splitArray[i]*j - 0;		
	return answer;
}

function validatedSegments(segments, olapseg, olapfltId) {	 
	 setField("hdnOverLapSeg", "true");
	 setField("hdnSegArray",segments);
	 setField("hdnOverLapSegId", olapseg);
	 setField("hdnOverlapFlightId" , olapfltId);
}
function validatedTerminals(terminals) {	 
	 setField("hdnTerminalArray",terminals);	 
}
function getValidatedTerminals() {	 
	return getFieldByID("hdnTerminalArray").value;	  
}
//hide the message
function objOnFocus(){
	top[2].HidePageMessage();
}

function setStatusChange() {
	objOnFocus();
	dataChanged();
	var stStatus = getFieldByID("selStatus").value;	
	var selMode = getFieldByID("hdnMode").value;	
	var prevStatus = getFieldByID("hdnPrevStatus").value;;
	if (selMode == "UPDATE") {
		if (prevStatus == stStatus) {
			return true;
		}
		
		if((prevStatus == "ACT" && stStatus == "CLS") || (prevStatus == "CLS" && stStatus == "ACT")) {		
			return true;
		}
		else{
			showERRMessage(statusinvalid);
			return false;
		}
	}
	
}

//function to check the minimum stop over time
function checkMinDuration(airport,arrivalTime, depatureTime, arrvalday, depday) {
	var depAirport =  getFieldByID(airport).value;
	var arrival = getMinutes(getFieldByID(arrivalTime).value) + (getFieldByID(arrvalday).value * 24 *60);		
	var depature = getMinutes(getFieldByID(depatureTime).value) + (getFieldByID(depday).value * 24 *60);	
	for (var i = 0 ; i < arrMinStopOverTimeData.length ;i++){
		if ((depAirport == arrMinStopOverTimeData[i][0])&&((depature-arrival) < arrMinStopOverTimeData[i][1])){			
			return false;
		}
	}
}
	
	
function resetClick() {
	if (isPageEdited()) {
		objOnFocus();
		setPageEdited(false);
		var strMode = getField("hdnMode").value;		
		if (strMode=="ADD") {
			addClick();
		}
		if (strMode == "UPDATE") {
			gridClick(strGridRow);
			editClick();
		}		
	}
}

function flightValid(txtfield) {
	objOnFocus();
	var strflt = getFieldByID(txtfield).value;
	var strLen = strflt.length;
	var valid = false;
	if ((trim(strflt) != "") && (isAlphaNumeric(trim(strflt)))) {
		setField(txtfield, trim(strflt)); 
		valid = true;
	}
	return valid;
}

function airportDisable(cond) {
	for(var j=1;j < 6;j++) {
		Disable("selFromStn"+j,cond);
		Disable("selToStn"+j,cond);		
	}
	
}

function setLocaTime() {
	blnZuluTime = false;
	setField("hdnTimeMode","LOCAL");
	if((!blnAdd) && (strGridRow > -1)) {
		arrLegs = flightData[strGridRow][23];
		setField("txtDepatureDate",flightData[strGridRow][22]);
		displayLegs(arrLegs);
	}
}


function setZuluTime() {
	blnZuluTime = true;
	setField("hdnTimeMode","ZULU");
	if((!blnAdd) && (strGridRow > -1)) {
		arrLegs = flightData[strGridRow][18];
		setField("txtDepatureDate",flightData[strGridRow][2]);
		displayLegs(arrLegs);
	}
}

function displayLegs(arrLegs) {
	var j=1;
	for (var i=0;i<arrLegs.length ;i++ ) {
		setField("selFromStn"+j,arrLegs[i][1]);
		setField("selToStn"+j,arrLegs[i][2]);
		setField("txtDepTime"+j,arrLegs[i][3]);
		setField("selDepDay"+j,arrLegs[i][4]);
		setField("txtArrTime"+j,arrLegs[i][5]);
		setField("selArrDay"+j,arrLegs[i][6]);
		document.getElementById('spnDuration'+j).innerHTML = arrLegs[i][7];
		j++;
	}
}
function displayLegsEdit(arrLegs){
	var arrLegsSplit=arrLegs.split("~");
	var j=1;
	for (var i=0;i<arrLegsSplit.length ;i++ ) {
		var arrLegsSplit2=arrLegsSplit[i].split(",")
		if(arrLegsSplit2.length == 7){
			setField("selFromStn"+j,arrLegsSplit2[1]);
			setField("selToStn"+j,arrLegsSplit2[2]);
			setField("txtDepTime"+j,arrLegsSplit2[3]);
			setField("selDepDay"+j,arrLegsSplit2[4]);
			setField("txtArrTime"+arrLegsSplit2[5]);
			setField("selArrDay"+j,arrLegsSplit2[6]);
			document.getElementById('spnDuration'+j).innerHTML = arrLegsSplit2[7];
			j++;			
		}
	}
}


function reservationExist() {
	var seatsSold = flightData[strGridRow][11];
	var seatsOnhold = flightData[strGridRow][12];
	var reservations = parseInt(seatsSold) + parseInt(seatsOnhold);
	if (reservations > 0) {
		return true;
	}
	return false;
}

function scheduleExist() {
	if (flightData[strGridRow][15] != "&nbsp") { // Schedule id not null
		return true;
	}
	return false;
}

function getTabValue() {
	var tabValue = top[1].objTMenu.tabGetValue(screenID).split("^");	
	if ((tabValue == "") || (tabValue == null)) {
		tabValue = new Array("1", "");
	}
	return tabValue;
}

function setTabValue(screenID) {
	top[1].objTMenu.tabSetValue(screenID, tabValue[0]+"^"+tabValue[1]+"^"+tabValue[2]+"^"+tabValue[3]);
}

function setSearchCriteria() {
	 
	tabValue[1] = getText("txtStartDateSearch")+","+getText("txtStopDateSearch")+","+getText("txtFlightNoSearch")+","+getValue("selFromStn6")+","+getValue("selToStn6")+","+getValue("selOperationTypeSearch")+","+getValue("selStatusSearch")+","+getValue("hdnTimeMode")+","+getValue("txtFlightNoStartSearch")+","+getChecked("chkOnlySche")+","+getValue("selFlightTypeSearch");
}

function setMangeFlightCriteria() {
	tabValue[1] = getText("txtStartDateSearch")+","+getText("txtStopDateSearch")+","+getText("txtFlightNoSearch")+","+getValue("selFromStn6")+","+getValue("selToStn6")+","+getValue("selOperationTypeSearch")+",'all',"+getValue("hdnTimeMode")+","+getValue("txtFlightNoStartSearch");
}


function setSearchRecNo(recNo) {
	tabValue[0] = recNo;
}

var setIsFirstSearch = function(value){
	tabValue[2] = value;
}

var setSchedID = function(value){
	tabValue[3] = value;
}

function gridNavigations(intRecNo, intErrMsg) {	
	if (intRecNo <= 0) {
		intRecNo = 1;
	}
	 
	setSearchRecNo(intRecNo);	
	setTabValue(screenID);
	setField("hdnRecNo", intRecNo);
	if (intErrMsg != ""){
		top[2].objMsg.MessageText = "<li> " + intErrMsg ;
		top[2].objMsg.MessageType = "Error" ; 
		top[2].ShowPageMessage();
	}else{
		if (isPageEdited()) {
			setPageEdited(false);
			 
				disableSearchSection(false);
			 
			document.forms[0].hdnScheduleId.value = getParamValue("hdnScheduleId");
			document.forms[0].hdnMode.value="SEARCH";
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	}
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenID, isEdited);
}

function isPageEdited() {
	return top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenID));
}

function closeClick() {
	if (isPageEdited()) {
		setPageEdited(false);
		objOnFocus();
		top[1].objTMenu.tabRemove(screenID);
	}
}

function confirmUpdate(alert, email, strTo, strSubject, strContent, strSmsAlert, strEmailAlert, strSmsCnf, strSmsOnH, openEtCnf) {
	setTabValue(screenID);
	setField("hdnReSchedAlert", alert);
	setField("hdnReSchedEmail", email);
	setField("hdnEmailTo", strTo);
	setField("hdnEmailSubject", strSubject);
	setField("hdnEmailContent", strContent);
	
	setField("hdnSMSAlert", strSmsAlert);
	setField("hdnEmailAlert", strEmailAlert);
	setField("hdnSmsCnf", strSmsCnf);
	setField("hdnSmsOnH", strSmsOnH);
	setField("hdnOpenEtStatus", openEtCnf);

	document.forms[0].hdnMode.value="UPDATE";
	airportDisable(false);
	Disable("selDepDay1", false);
	disableInputSection(false);		
	document.forms[0].submit();
	top[2].ShowProgress();	  
}

function checkOverlapping() {
	var valid = true;
	if ((checkGridRow()) && (isOverlapped())) {
		var depError = false;	
		var opTypeError = false;	
		var acModelError = false;	
		var statusError = false;	
		var depStationError = false;	
		var depTimeError = false;	
		var depDayError = false;	
		var arrStationError = false;	
		var arrTimeError = false;	
		var arrDayError = false;	
												
		var msg = "Do you wish to update overlapping flight?";
		var msg1 = "Overlapping segments may get updated. Do you wish to continue?";
		
		if (!getFieldByID("txtDepatureDate").disabled) {
			if ((blnZuluTime) && (getFieldByID("txtDepatureDate").value != flightData[strGridRow][2])) {
				depError = true;
			} 
			if ((!blnZuluTime) && (getFieldByID("txtDepatureDate").value != flightData[strGridRow][22])) {
				depError = true;		
			}
		}
		if (!getFieldByID("selOperationTypeSearch").disabled) {
			if (getFieldByID("selOperationTypeSearch").value != flightData[strGridRow][8]) {
				opTypeError = true;
			} 
		}	
		if (!getFieldByID("selModelNo").disabled) {
			if (getFieldByID("selModelNo").value != flightData[strGridRow][17]) {
				acModelError = true;
			} 		
		}
		if (!getFieldByID("selStatus").disabled) {
			if (getFieldByID("selStatus").value != flightData[strGridRow][13]) {
				statusError = true;
			} 		
		}
		
		var j = 1;
		var legs;
		if (blnZuluTime) {
			legs = flightData[strGridRow][18];
		} else {
			legs = flightData[strGridRow][23];
		}
		for (var i = 0; i < legs.length; i++ ) {
			if (!getFieldByID("selFromStn" + j).disabled) {
				if (getFieldByID("selFromStn" + j).value != legs[i][1]) {
					depStationError = true;
				}
			}
			if (!getFieldByID("selToStn" + j).disabled) {
				if (getFieldByID("selToStn" + j).value != legs[i][2]) {
					arrStationError = true;
				}
			}
			if (!getFieldByID("txtDepTime" + j).disabled) {
				if (getFieldByID("txtDepTime" + j).value != legs[i][3]) {
					depTimeError = true;
				}
			}			
			if (!getFieldByID("selDepDay" + j).disabled) {
				if (getFieldByID("selDepDay" + j).value != legs[i][4]) {
					depDayError = true;
				}
			}			
			if (!getFieldByID("txtArrTime" + j).disabled) {
				if (getFieldByID("txtArrTime" + j).value != legs[i][5]) {
					arrTimeError = true;
				}
			}			
			if (!getFieldByID("selArrDay" + j).disabled) {
				if (getFieldByID("selArrDay" + j).value != legs[i][6]) {
					arrDayError = true;
				}
			}			
			j++;
		}
		
		if ((opTypeError) || (acModelError) || (statusError)) {
			if (!confirm(msg)) {
				valid = false;
			}					
		} else if ((depError) || (depStationError) || (depTimeError) || (depDayError) 
				|| (arrStationError) || (arrTimeError) || (arrDayError)) {
			if (!confirm(msg1)) {
				valid = false;
			}					
		}		
	}
	return valid;	
}

function checkAmendAlert() {
	var valid = false;
	if ((checkGridRow()) && (reservationExist())) {
		var depChanged = false;	
		var flightNoChanged = false;	
		var routeChanged = false;	
		var timeChanged = false;	
		var terminalChanged = false;
		
		if((getFieldByID("hdnTerminalArray").value != flightData[strGridRow][35])){
			terminalChanged = true;
		}
		
		if (!getFieldByID("txtDepatureDate").disabled) {
			if ((blnZuluTime) && (getFieldByID("txtDepatureDate").value != flightData[strGridRow][2])) {
				depChanged = true;
			} 
			if ((!blnZuluTime) && (getFieldByID("txtDepatureDate").value != flightData[strGridRow][22])) {
				depChanged = true;		
			}
		}
		if (!getFieldByID("txtFlightNoStart").disabled) {
			if (getFieldByID("txtFlightNoStart").value != flightData[strGridRow][1].substr(0,2)) {
				flightNoChanged = true;
			} 
		}	
		if (!getFieldByID("txtFlightNo").disabled) {
			 
			if (getFieldByID("txtFlightNo").value != flightData[strGridRow][1].substr(2,4)) {
				var isChange =  window.confirm("Are you sure you want to update the flight number ? \n\n If you select cancel flight number will remain the same");
				if(isChange) {
					flightNoChanged = true;
				} else {
					setField("txtFlightNo", flightData[strGridRow][1].substr(2,4)); 
				}
				
			} 		
		}

		var j = 1;
		var legs;
		if (blnZuluTime) {
			legs = flightData[strGridRow][18];
		} else {
			legs = flightData[strGridRow][23];
		}
		for (var i = 0; i < legs.length; i++ ) {
			if (!getFieldByID("selFromStn" + j).disabled) {
				if (getFieldByID("selFromStn" + j).value != legs[i][1]) {
					routeChanged = true;
				}
			}
			if (!getFieldByID("selToStn" + j).disabled) {
				if (getFieldByID("selToStn" + j).value != legs[i][2]) {
					routeChanged = true;
				}
			}
			if (!getFieldByID("txtDepTime" + j).disabled) {
				if (getFieldByID("txtDepTime" + j).value != legs[i][3]) {
					timeChanged = true;
				}
			}			
			if (!getFieldByID("selDepDay" + j).disabled) {
				if (getFieldByID("selDepDay" + j).value != legs[i][4]) {
					timeChanged = true;
				}
			}			
			if (!getFieldByID("txtArrTime" + j).disabled) {
				if (getFieldByID("txtArrTime" + j).value != legs[i][5]) {
					timeChanged = true;
				}
			}			
			if (!getFieldByID("selArrDay" + j).disabled) {
				if (getFieldByID("selArrDay" + j).value != legs[i][6]) {
					timeChanged = true;
				}
			}			
			j++;
		}
		
		if ((depChanged) || (flightNoChanged) || (routeChanged) || (timeChanged) || (terminalChanged)) {
			valid = true;
		}		
	}
	return valid;	
}

function checkAircraftModel() {
	var valid = false;
	var gridData;	
	var mode = getField("hdnMode").value;
	var modelSelected = getFieldByID("selModelNo").value;
	for (var i = 0; i < arrAircraftModelData.length; i++) {
		if ((modelSelected != "") && (trim(modelSelected) == trim(arrAircraftModelData[i][0]))) {	
			if ((mode == "ADD") && (arrAircraftModelData[i][3] == "ACT")) {
				valid = true;
			} else if (mode == "UPDATE") {
				if (checkGridRow()) {
					gridData = flightData[strGridRow][17];	
					if ((trim(modelSelected) == trim(gridData)) && (arrAircraftModelData[i][3] != "ACT")) {
						valid = true;
					} else if (arrAircraftModelData[i][3] == "ACT") {
						valid = true;
					}
				}
			}
			break;			
		}
	}	
	return valid;
}

//Check if the grid row and the flight details to edit re the same
function checkGridRow() {
	var valid = false;
	if ((strGridRow != null) && (strGridRow > -1) 
			&& (flightData != null) && (flightData.length > 0) 
			&& (trim(getFieldByID("hdnFlightId").value) == trim(flightData[strGridRow][16]))) {
		valid = true;
	}
	return valid;

}

function setTimeMode() {
	if (getValue("hdnTimeMode") == "LOCAL") {
		document.forms[0].radTZ[0].checked = true;  
		setField("hdnTimeMode", "LOCAL");
		isTimeInZulu = false;
	} else {
		document.forms[0].radTZ[1].checked = true;
		setField("hdnTimeMode","ZULU");	
		isTimeInZulu = true;
	}
}

function setFlightScheduleId() {
	document.getElementById('spnSchedId').innerHTML = getFieldByID("hdnScheduleId").value;
	document.getElementById('spnFlightId').innerHTML = getFieldByID("hdnFlightId").value;
}

function setIndicators() {
	if (trim(flightData[strGridRow][9]) != "&nbsp") {
		setStyleClass("tdFltOverLapped", "fltStatus02");		
	}
	if (reservationExist()) {
		setStyleClass("tdReservExcist", "fltStatus04");
	}
	if (flightData[strGridRow][19] == "true") {
		setStyleClass("tdFltChanged", "fltStatus03");
	}
}

function isOverlapped() {
	var valid = true;
	if (trim(flightData[strGridRow][9]) == "&nbsp") {
		valid = false;
	}
	return valid;
}

var isFlown = function(){
	 
	var cdepdate = flightData[strGridRow][2];
	 
	var Today = getFieldByID("hdnCurrentDate").value;
	cdepdate = formatDate(cdepdate,"dd/mm/yyyy","dd-mmm-yyyy");		
	Today = formatDate(Today,"dd/mm/yyyy","dd-mmm-yyyy");
	if (compareDates(cdepdate, Today, '<')) { 
		return true;
	} else {
		return false;
	}
}

function checkDepArrLeg(leg, legNumber, isDep) {
	var valid = false;
	var gridData;
	var mode = getField("hdnMode").value;
	var mainMode=getField("hdnMainMode").value;
	
	if (mode == "UPDATE") {
		if (checkGridRow()) {
			gridData = flightData[strGridRow][23];	
			for (var i = 0; i < arrAptData.length; i++) {
				if (leg == arrAptData[i][0]) {
					valid = true;
					break;
				}
			}
			if ((!valid) && (legNumber <= gridData.length)) {
				if ((isDep) && (leg == gridData[(legNumber-1)][1])) {
					valid = true;
				} else if ((!isDep) && (leg == gridData[(legNumber-1)][2])) {
					valid = true;			
				} else {
					valid = false;
				}
			}
		}
	} else {
		valid = true;
	}
	return valid;
}

function getQueryStringForSeatAllocation() {
	var cabinClassCodes = "";
	var cabinClassCode = "";
	var flightSumm = "";
	var queryString = "";
	var depDate = "";
	
	if(blnZuluTime) {
		depDate = flightData[strGridRow][2];
	}else {
		depDate = flightData[strGridRow][22];
	}
	
	var today = getFieldByID("hdnCurrentDate").value;
	var flightModel = flightData[strGridRow][17];
	var arrAircraftClassOfService;
	
	for (var i = 0; i < arrAircraftModelData.length; i++){
		if ((flightModel != "") && (flightModel == arrAircraftModelData[i][0])){
			arrAircraftClassOfService = arrAircraftModelData[i][2];
			cabinClassCodes = arrAircraftModelData[i][2].split("#");
			break;
		}
	}
	
	//Checking whether the CC Code is default CC. then make it as defaulty selected
	cabinClassCode = defaultclassofservice;
	if (cabinClassCodes.length > 0) {
		cabinClassCode = cabinClassCodes[0];
		for (var i = 0; i < cabinClassCodes.length; i++) {
			if (cabinClassCodes[i] == defaultclassofservice) {
				cabinClassCode = defaultclassofservice;
			}
		}			
	}
	
	var ftDepartTime = "";
	var ftAriveTIme = "";
	if(blnZuluTime) {
		ftDepartTime = flightData[strGridRow][26]; 
		ftAriveTIme = flightData[strGridRow][25];
	}else {
		ftDepartTime = flightData[strGridRow][29]; 
		ftAriveTIme = flightData[strGridRow][28];
	}

	flightSumm += flightData[strGridRow][1]  + "|"; 	//FlightNumber
	flightSumm += flightData[strGridRow][3]  + "|"; 	//Departure	
	flightSumm += ftDepartTime				 + "|"; 	//DepartureDateTime	
	
	flightSumm += flightData[strGridRow][4]  + "|"; 	//Arrival
	flightSumm += ftAriveTIme				 + "|"; 	//ArrivalDateTime
	var airports = flightData[strGridRow][5].split("/");	//ViaPoints
	if (airports.length > 2) {
		for (var i = 1; i < airports.length; i++) {
			if (i == airports.length - 2) {
				flightSumm += airports[i]  + "|"; 	
				break;
			} else {
				flightSumm += airports[i]  + ", ";
			}
		}
	} else {
		flightSumm += ''  + "|"; 	//ViaPoints
	}
	flightSumm += flightData[strGridRow][13]  + "|"; 	//FlightStatus
	if (!isOverlapped()) { 								//isOverlappingFlight
		flightSumm += 'N' + "|"; 
	} else {
		flightSumm += 'Y' + "|";
	}
	flightSumm += arrAircraftClassOfService + "|"; 		//CabinClassCodes
	flightSumm += defaultclassofservice + "|"; 			//DefaultCabinClass
	flightSumm += flightData[strGridRow][16] + "|"; 	//FlightId
	flightSumm += flightData[strGridRow][17] + "|"; 	//ModelNumber
	if (!isOverlapped()) { 								//OverlappingFlightId
		flightSumm += 'null' + "|"; 								
	} else {
		flightSumm += flightData[strGridRow][9] + "|"; 			
	}
		
	flightSumm += flightData[strGridRow][30] + "|";  //cannot edit for inventory
	
    var askValue = (Number(flightData[strGridRow][14])*1000);
	flightSumm += askValue  + "|"; 	//Ask Value
	var strSchedExist = '';	
	if(flightData[strGridRow][15] != null && flightData[strGridRow][15] != '&nbsp') {
		strSchedExist = 'Y';
	}else {
		strSchedExist = 'N';
	}
	flightSumm += strSchedExist  + "|"; 	//schedule exist
	flightSumm += hideLogicalCC + "|";		//hide locigal cc details
	flightSumm += hideWaitListing + "|";		//hide wait listing details
	flightSumm += "@";


	//var strFligtNumber = "?hdnFlightNumber=" + flightData[strGridRow][1];
	var strFligtID = "?hdnFlightID=" + flightData[strGridRow][16];
	var strClassOfService = "&hdnClassOfService=" + cabinClassCode;
	//var strDepartureDate = "&hdnDepartureDate=" + flightData[strGridRow][2];
	var temp = null;
	if (isOverlapped()) { 								//OverlappingFlightId
		temp = flightData[strGridRow][9]; 			
	}
	var overLap = "&hdnOlFlightId=" + temp;
	var model = "&hdnModelNo=" + flightData[strGridRow][17];

	//var summary = "&hdnFlightSumm=" + flightSumm;
	setField("hdnFlightSumm",flightSumm);
	//queryString = strFligtID + strClassOfService + overLap + model;
	queryString = '?hdnFrom=Flight&hdnFLString={"FlightID":"'+flightData[strGridRow][16]+'", "ClassOfService":"'+cabinClassCode+'", "OlFlightId":"'+temp+'", "ModelNo":"'+flightData[strGridRow][17]+'", "FlightSumm":""}'
	setTabValues(screenID, valueSeperator, "tempSearch", 0 + "#" + 0 + "#" + "");
	setTabValues(screenID, valueSeperator, "strInventoryFlightDataRow", flightData[strGridRow][1] +"~"+flightData[strGridRow][3]+"~"+ftDepartTime+"~"+flightData[strGridRow][4]+"~"+ftAriveTIme+"~"+flightData[strGridRow][17]+"~"+ askValue );

	return queryString;
}

function seatAllocation() {
	if (isPageEdited()) {
		setPageEdited(false);
		objOnFocus();
		getFieldByID("frmFlight").target = "_self";
		if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(invScreenID))) {
			if ((top[1].objTMenu.tabIsAvailable(invScreenID) 
					&& top[1].objTMenu.tabRemove(invScreenID)) 
					|| (!top[1].objTMenu.tabIsAvailable(invScreenID)))  {
				disableSaveResetButtons(true);
				disableGridButtons("true");	
				Disable("btnCopy", true);
				Disable("btnGDS", true);
				
				var searchCriteria = getTabValue()[1].split(",");
				setSearchCriteria();
				setTabValue(screenID);
				if ((searchCriteria == null) || (searchCriteria == "") || (searchCriteria.length <= 0)) {
					if ((tabValue[0] == "") || (tabValue[0] <= 0)) {
						setSearchRecNo(1);					
					}
					setSearchCriteria();
					setTabValue(screenID);
				}
				var strAction = "showLoadJsp!loadAllocateSearch.action" + getQueryStringForSeatAllocation();
				top[1].objTMenu.tabSetTabInfo(screenID, invScreenID, "showLoadJsp!loadAllocateSearch.action", "Inventory", false);
								
				getFieldByID("frmFlight").action = strAction;
				getFieldByID("frmFlight").submit();
				
				top[2].ShowProgress();
			}
		}
	}	
}

function getParamValue(paramName) {
	var paramValue = "";
	if ((model == null) || (model[paramName] == null) || (model[paramName] == "null")) {
		paramValue = "";
	} else {
		paramValue = model[paramName];
	}
	return paramValue;
}

function overlapLegDisable() {
	var olapLegs;
	if (blnZuluTime) {
		olapLegs = flightData[strGridRow][18];
	} else {
		olapLegs = flightData[strGridRow][23];
	}	
	var j=1;
	for (var i=0;i<olapLegs.length ;i++ ) {
		
		if(olapLegs[i][8] == "true") {
			Disable("selFromStn"+j, true);
			Disable("selToStn"+j, true);	
		}				
		j++;
	}
}

function setSegmentArray(strSegments) {
	var strValString = "";
	for(var i=0; i < strSegments.length;i++) {
		strValString += strSegments[i][1]  + "_";
		strValString += strSegments[i][2] + ","; 
	}
	setField("hdnSegArray", strValString);
}

function setReprotectFromIdentifier(strFormIdentifier) {
	top[0].strReprotectFrom = strFormIdentifier;
}

function reprotectClick() {
	Disable("btnReprotectPAX", true);
	setField("hdnReprotect", "false");
	closeChildWindows();

	disableSaveResetButtons(true);
	disableGridButtons(true);	
	Disable("btnCopy", true);
	Disable("btnGDS", true);
	
	// to set the mode of JSP page button click
	setReprotectFromIdentifier("FLIGHT");

	var intLeft = (window.screen.width - 1000) / 2;
	var intTop = (window.screen.height - 765) / 2;
	var winWidth = 1000;
	var winheight = 690;

	var strFlightId = getFieldByID("hdnFlightId").value;
	var strActionUrl="showFlightsToReprotect.action";
	strActionUrl += "?hdnFlightId=" + strFlightId;

	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=' + winWidth + ',height=' + winheight + ',resizable=no,top=' + intTop + ',left=' + intLeft;
	top[0].objReprotectWindow = window.open(strActionUrl, "reprotectWindow", strProp);
}

function reprotectClick_v2() {
	Disable("btnReprotectPAX", true);
	setField("hdnReprotect", "false");
	closeChildWindows();

	disableSaveResetButtons(true);
	disableGridButtons(true);	
	Disable("btnCopy", true);
	Disable("btnGDS", true);
	
	// to set the mode of JSP page button click
	setReprotectFromIdentifier("FLIGHT");

	var intLeft = (window.screen.width - 1000) / 2;
	var intTop = (window.screen.height - 765) / 2;
	var winWidth = 1000;
	var winheight = 690;

	var strFlightId = getFieldByID("hdnFlightId").value;
	var strActionUrl="showFlightsToReprotect!execute_V2.action";
	strActionUrl += "?hdnFlightId=" + strFlightId;

	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=' + winWidth + ',height=' + winheight + ',resizable=no,top=' + intTop + ',left=' + intLeft;
	top[0].objReprotectWindow = window.open(strActionUrl, "reprotectWindow", strProp);
}

function confirmCancel(strAlerts, strEmail, strReschdalt, strReschdEml, strReschedAll, strTo, strSubject, strContent, strSmsAlert, strEmailAlert, strSmsCnf, strSmsOnH) {	
	closeChildWindows();
	setTabValue(screenID);	
	setField("hdnCancelAlert", strAlerts);
	setField("hdnReSchedAlert", strReschdalt);
	setField("hdnCancelEmail", strEmail);
	setField("hdnReSchedEmail", strReschdEml);
	setField("hdnEmailTo", strTo);
	setField("hdnEmailSubject", strSubject);
	setField("hdnEmailContent", strContent);	
	setField("hdnFlightId", reproFlightId);	
	
	setField("hdnSMSAlert", strSmsAlert);
	setField("hdnEmailAlert", strEmailAlert);	
	setField("hdnSmsCnf", strSmsCnf);
	setField("hdnSmsOnH", strSmsOnH);
	
	setField("hdnReprotect", "false");
	document.getElementById("frmFlight").target = "_self";		
	document.forms[0].hdnMode.value="CONFIRM.CANCEL";
	document.forms[0].submit();
	top[2].ShowProgress();	
}

function reprotectRefresh() {
	setField("hdnReprotect", "true");
	if ((top[0].objCancelWindow) && (!top[0].objCancelWindow.closed)) {
		document.forms[0].target = "cancelWindow";
		document.forms[0].submit();
	} else {
		closeChildWindows();
	}
}

function closeCancelChildWindow() {
	if ((top[0].objCancelWindow) && (!top[0].objCancelWindow.closed)) {
		top[0].objCancelWindow.close();
	}
}
function closeReprotectChildWindow() {
	if ((top[0].objReprotectWindow) && (!top[0].objReprotectWindow.closed))	{
		top[0].objReprotectWindow.close();
	}
}
function closeRollforwardChildWindow() {
	if ((top[0].objRollForwardWindow) && (!top[0].objRollForwardWindow.closed))	{
		top[0].objRollForwardWindow.close();
	}
}
function closeChildWindows() {
	closeRollforwardChildWindow();
	closeReprotectChildWindow();
	closeCancelChildWindow();
}

function settingValidation(cntfield){
	if (top[1].objTMenu.focusTab == screenID){
		if (!dateChk(cntfield))	{
			showERRMessage(invalidDate);		
			getFieldByID(cntfield).focus();		
		return;
		}
	}
}
//Haider 21Oct08 This function is called when the gds publish button clicked
function GDSPublishDetails(){
	
	if (isPageEdited()){
		setPageEdited(false);
		closeChildWindows();
		setField("hdnReprotect", "false");
		objOnFocus();
		var intLeft = (window.screen.width - 800) / 2;
		var intTop = (window.screen.height - 290) / 2;

		var url = "showFltGDSPublishing.action?hdnFlightId="+getValue("hdnFlightId")+"&hdnVersion="+getValue("hdnVersion")+"&hdnViewModeOnly="+getValue("hdnViewModeOnly")+
					"&hdnGDSPublishing="+getValue("hdnGDSPublishing")+"&hdnMode="+getValue("hdnMode")+"&hdnHaveMCFlight="+getValue("hdnHaveMCFlight"); 
		disableSaveResetButtons(true);
		disableGridButtons("true");	
		Disable("btnCopy", true);
		Disable("btnGDS", true);

		window.open(url,"CFltGDSWindow",'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=800,height=370,resizable=no,top=' + intTop + ',left=' + intLeft);
	}
}


// redirect to manage flt
function manageFlight() {
	 
	if (isPageEdited()) {
		closeChildWindows();
		setField("hdnReprotect", "false");
		document.getElementById("frmFlight").target = "_self";		
		setPageEdited(false);
		objOnFocus();
		disableSaveResetButtons(true);
		disableGridButtons("true");	
		//Disable("btnManageFlight", true);
		Disable("btnGDS", true);
		 
		setField("hdnMode","MANAGE");
		setField("hdnModelNo", getFieldByID("selModelNo").value);	
		
		var strManageFltIds = chkvalidate();
		setField("hdnManageFltIds", strManageFltIds);
		var scheduleID = getFieldByID("hdnScheduleId").value; 
		strFilename = "showManageFlights.action?strManageFltIds="+strManageFltIds+"&strSchedId="+scheduleID; 
		 

		$("#tabs").tabs( "enable" , 1);
		$( "#tabs" ).tabs( "option", "active", 1 );
		document.getElementById('mngFltPopup').src = strFilename;
		
		 
	}
}
// confirmManageFlight
var confirmManageFlight = function(opt1,opt2,opt3,opt4,opt5, reinstate, scheduleID){
	//processSelectedOption("radActOpnSel"),processSelectedOption("radActClsSel"),processSelectedOption("radActOpenAll"),processSelectedOption("radActClsAll")
	
    setField("hdnradActOpnSel",opt1); 
    setField("hdnradActClsSel",opt2); 
    setField("hdnradActOpenAll",opt3); 
    setField("hdnradActClsAll",opt4); 
    setField("hdnradActSysClsSel",opt5)
    
	setField("hdnReInstate",reinstate);	
	setField("hdnScheduleId", scheduleID); 
	document.forms[0].hdnMode.value="SAVEMANAGE";
	
	top[1].objTMenu.tabSetValue(screenID, "1^"+tabValue[1]+"^"+tabValue[2]+"^"+tabValue[3]);
	document.forms[0].submit();
	top[2].ShowProgress();	
	 
}

var disableManageFlights = function(control){
	$("#tabs").tabs( control , 1);
}

var focusTab = function(control){
	$( "#tabs" ).tabs( "option", "active", control);
}
 
function getFlightFrequency(){
	var strFrequency = "";
	strFrequency += getChecked("chkSunday") + "," + getChecked("chkMonday") + "," + getChecked("chkTuesday") + "," + getChecked("chkWednesday") + "," +
					getChecked("chkThursday") + "," + getChecked("chkFriday") + "," + getChecked("chkSaturday");
	return strFrequency;
}

function timeCheck(id, obj) {
	if (!isTimeFormatValid(obj)) {
		switch (id) {
		case 0:
			msg = parent.arrError['invalidConnectionLimit'];
			break;
		case 1:
			msg = parent.arrError['invalidStartTime'];
			break;
		case 2:
			msg = parent.arrError['invalidEndTime'];
			break;
		}
		showERRMessage(msg);
		getFieldByID(obj).focus();
		return false;
	}
	return true;
}

function isTimeFormatValid(objTime) {
	// Checks if time is in HH:MM format - HH can be more than 24 hours
	var timePattern = /^(\d{1,2}):(\d{2})$/;
	var matchArray = trim(getValue(objTime)).match(timePattern);
	if (matchArray == null) {
		return false;
	}
	var hour = matchArray[1];
	var min = matchArray[2];
	if ((min < 0) || (min > 59)) {
		return false;
	}
	if (hour.length < 2) {
		setField(objTime, "0" + trim(getValue(objTime)));
	}
	return true;
}
function validateTA(objControl, length) {
	var strValue = objControl.value;
	var strLength = strValue.length;
	var blnVal = isEmpty(strValue);
	if (blnVal) {
		setField("txtRemarks", strValue.substr(0, strLength - 1));
		getFieldByID("txtRemarks").focus();
	}
	if (strLength > length) {
		strValue = strValue.substr(0, length);
		objControl.value = strValue;
	}
}

function isAlphaNumericTextArea(s){return RegExp("^[a-zA-Z0-9-/ \w\s\&._]+$").test(s);}

function spclCharsVali(val){
	customValidate(val,/[`!@#$%*?\[\]{}()|\\\/+=:,;^~]/g);
}

function setInputData(model) {
	var arrHdnLegs;
	var scheduleId = model['hdnScheduleId'];
	setField("hdnGridRow", model['hdnGridRow']);	
	var strRowNo=model['hdnGridRow'];
	setField("hdnScheduleId",scheduleId);
	if(blnZuluTime) {
		setField("txtDepatureDate", model['txtDepatureDate']);
		arrHdnLegs = model['hdnLegArray'];
		setField("hdnFlightDate",model['txtDepatureDate']);
	}else {
		setField("txtDepatureDate",model['txtDepatureDate']);
		arrHdnLegs = model['hdnLegArray'];
		setField("hdnFlightDate",model['txtDepatureDate']);
	}
	setField("txtFlightNo", model['txtFlightNo']);		
	setField("txtFlightNoStart",model['txtFlightNoStart']);
	setField("hdnFlightNo",model['txtFlightNo']);
	setField("selOperationType",model['selOperationType']);
	setField("selStatus",model['selStatus']);
	setField("hdnPrevStatus",model['hdnPrevStatus']);
	
	setField("selModelNo",model['selModelNo']);

	if(document.getElementById("selModelNo").value==""){
		
            var obj=document.getElementById("selModelNo");         
                   
           opt = document.createElement("option");
           opt.value = model['selModelNo'];
           opt.text=model['selModelNo'];
           opt.disabled = 'disabled';
           obj.appendChild(opt);
           
           document.getElementById("selModelNo").value = model['selModelNo'];
	}
	if (model['hdnOverlapFlightId'] == "&nbsp") {
		setField("hdnOverlapFlightId","");
	}else {
		setField("hdnOverlapFlightId",model['hdnOverlapFlightId']);
	}
	
		
	var ModelSelected = model['selModelNo'];	
	for (var i = 0 ; i < arrAircraftModelData.length ;i++){
		if ((ModelSelected != "")&&(ModelSelected == arrAircraftModelData[i][0])){
				document.getElementById('spnCapacity').innerHTML = arrAircraftModelData[i][1];
		}
	}	
	
	var n = seatTemplates.split("|"); 
	var	strChargeTemplate='<option value=""></option>';
	for (var i=0;i<n.length-1;i++)
	{
		var n1=n[i].split("&"); 
		if(n1[0]==ModelSelected){
			
			strChargeTemplate = strChargeTemplate+ "<option value='" +n1[1] + "'>" +n1[2] + "</option>";
				
		}	
	}
		
	document.getElementById('selSeatChargeTemplate').innerHTML = strChargeTemplate;

	var csold = flightData[strRowNo][11];
	var chold = flightData[strRowNo][12];
	var reser = parseInt(csold) + parseInt(chold);
			
	if(trim(flightData[strRowNo][9]) != "&nbsp")
		setStyleClass("tdFltOverLapped", "fltStatus02");		
	if(reser > 0)
		setStyleClass("tdReservExcist", "fltStatus04");
	if(flightData[strRowNo][19] == "true")
		setStyleClass("tdFltChanged", "fltStatus03");
			
	document.getElementById('spnSchedId').innerHTML = model['hdnScheduleId'];
	document.getElementById('spnFlightId').innerHTML = model['hdnFlightId'];
	setField("hdnVersion",model['hdnVersion']);
	setField("hdnFlightId",model['hdnFlightId']);
		
	//display the legs	
	//displayLegs(arrHdnLegs);
	//enabling buttons	
	//set segment
	setSegmentArray(model['hdnSegArray']);
	var cdepdate = model['txtDepatureDate'];
	var cstatus = model['selStatus'];
	
	var Today = getFieldByID("hdnCurrentDate").value;
	var diffDays = Today.split("/")[0] - cdepdate.split("/")[0];
	var isWithinActiveTimeDuration = false;
	if (diffDays < pastFlightActivationDurationInDays) {
		isWithinActiveTimeDuration = true;
	}
	if(cstatus=='ACT'){
		Disable("btnReprotectPAX", false);	
	}
	cdepdate = formatDate(cdepdate,"dd/mm/yyyy","dd-mmm-yyyy");		
	Today = formatDate(Today,"dd/mm/yyyy","dd-mmm-yyyy");
	
	var gdsList = flightData[strRowNo][31];
	setField("hdnGDSPublishing", gdsList);
	if (compareDates(cdepdate, Today, '<')) { 
		//departure date less than or equals to current date 
		//Seat allocation button is enabled
		disableGridButtons(true);
		Disable("btnSeatAllocate", false);
		if((isDelFlownFlts =="true" || isDelFlownFlts==true) && cstatus != "CNX" && isWithinActiveTimeDuration && modificationForPastFlight) {
			Disable("btnCancel", false);
		}		
		if(privActivatePastFlights == 'true' && isWithinActiveTimeDuration && modificationForPastFlight){
			Disable("btnEdit", false);
		}	
	} else {
		disableGridButtons(false);
	
		if (cstatus == "CLS") { // Closed
			// Edit & seat allocation button is enabled
			Disable("btnCancel", true);	
			Disable("btnEdit", false);				
			Disable("btnSeatAllocate", false);
		}
		
		if ((cstatus == "CLS") && (reser > 0)) { // Closed and having reservation
			// Reprotect button is enabled
			Disable("btnReprotectPAX", false);	
		}
		
		if (cstatus == "CNX") {
			Disable("btnCancel", true);	
			Disable("btnEdit", true);				
		}
	}		
		
	if (reser > 0) {
		airportDisable(true);
	}
/*	if (trim(strRowData[14]) != "&nbsp") {
		airportDisable(true);
		Disable("txtDepatureDate", true);
	}*/
	
	if (getValue("hdnTimeMode") == "LOCAL") {
		setLocaTime();
	} else {
		setZuluTime();
	}
	if(scheduleId != "&nbsp"){
		if(flightData[strRowNo][31] == "")
			Disable("btnGDS", true);
		setField("hdnViewModeOnly", "true");
	}
	else
		setField("hdnViewModeOnly", "false");
	
	setField("hdnTerminalArray", model['hdnTerminalArray']);
	
	setField("selFlightType", model['selFlightType']);
	
	setField("txtRemarks", model['txtRemarks']);		
	
	setField("selMealTemplate", model['selMealTemplate']);
	
	setField("selSeatChargeTemplate", model['selSeatChargeTemplate']);		
	
	setField("txtUserNotes", model['txtRemarks']);
	
	setField("txtCsOCCarrierCode", model['txtCsOCCarrierCode']);		
	
	setField("txtCsOCFlightNo", model['txtCsOCFlightNo']);		
	
	setField("hdnTxtUserNotes",model['txtUserNotes']);
	hidePublisheMessageAudit(gdsList);
	Disable("btnSave",false);
}
function getXMLHttpRequest() {
	  var xmlHttpReq = false;
	  // to create XMLHttpRequest object in non-Microsoft browsers
	  if (window.XMLHttpRequest) {
	    xmlHttpReq = new XMLHttpRequest();
	  } else if (window.ActiveXObject) {
	    try {
	      // to create XMLHttpRequest object in later versions
	      // of Internet Explorer
	      xmlHttpReq = new ActiveXObject("Msxml2.XMLHTTP");
	    } catch (exp1) {
	      try {
	        // to create XMLHttpRequest object in older versions
	        // of Internet Explorer
	        xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
	      } catch (exp2) {
	        xmlHttpReq = false;
	      }
	    }
	  }
	  return xmlHttpReq;
	}
	/*
	 * AJAX call starts with this function
	 */
	function makeRequest() {
	  var xmlHttpRequest = getXMLHttpRequest();
	  xmlHttpRequest.onreadystatechange = getReadyStateHandler(xmlHttpRequest);
	  xmlHttpRequest.open("POST", "showFlight.action", true);
	  xmlHttpRequest.setRequestHeader("Content-Type",
	      "application/x-www-form-urlencoded");
	  xmlHttpRequest.send(null);
	}
	 
	/*
	 * Returns a function that waits for the state change in XMLHttpRequest
	 */
	function getReadyStateHandler(xmlHttpRequest) {
	 
	  // an anonymous function returned
	  // it listens to the XMLHttpRequest instance
	  return function() {
	    if (xmlHttpRequest.readyState == 4) {
	      if (xmlHttpRequest.status == 200) {
	    	  alert( xmlHttpRequest.responseText);
	      } else {
	        alert("HTTP error " + xmlHttpRequest.status + ": " + xmlHttpRequest.statusText);
	      }
	    }
	  };
	}
	
	//add CodeShare to select box
	function addCodeShareFlight(){
		
		var Carrier =getFieldByID("carrier").value;
		var FlightNum = getFieldByID("flightnum").value.toUpperCase();
		
		if (( Carrier !="") && (FlightNum!="")) {
			var valid = true;	;
			var CSFlight =  Carrier+ "|"+ FlightNum;
			
			var control = document.getElementById("sel_CodeShare");
			for ( var r = 0; r < control.length; r++) {
				var enterdCSFlight = control.options[r].value;
				
				if (enterdCSFlight == CSFlight) {
					valid = false;
					showERRMessage(codeShareExists);
					getFieldByID("carrier").focus();
					getFieldByID("flightnum").focus();
					break;
				}
				if(enterdCSFlight.split("|")[0] == Carrier){
					valid = false;
					showERRMessage(codeShareExists);
					getFieldByID("carrier").focus();
					break;
				}
						
			}
			if (valid) {
				control.options[control.length] = new Option(CSFlight, CSFlight);
				getFieldByID("carrier").value = '';
				getFieldByID("flightnum").value = '';
			}
		} else {
			if(Carrier == ""){
				showERRMessage(CScarriernull);
			}
			if(FlightNum == ""){
				showERRMessage(CSfltnonull);
			}
			
		}		
	}

	//remove CodeShare from select box
	function removeFromList() {
		var control = document.getElementById("sel_CodeShare");

		var selIndex = control.selectedIndex;
		var flag = false;
		if (selIndex != -1) {
			for (i = control.length - 1; i >= 0; i--) {
				if (control.options[i].selected) {					
					if(!isPublishedToCodeShare(control.options[i].value)){
						control.options[i] = null;	
						flag = true;
					} else {						
						flag = false;
						showERRMessage(codeShareUnPublish);
						break;
					}		
					
				}
			}
			if (flag && control.length > 0) {
				control.selectedIndex = selIndex == 0 ? 0 : selIndex - 1;
				getFieldByID("carrier").value = '';
				getFieldByID("flightnum").value = '';
			}
		}
	}
	//set field to Save CodeShare
	function setCodeShareArray() {
		var control =  document.getElementById("sel_CodeShare");
		var strCodeShare= "";
		for ( var r = 0; r < control.length; r++) {
			strCodeShare += control.options[r].value+ ",";
		}
		setField("hndCodeShare", strCodeShare);
	}
	
	//View CodeShare
	function setCodeShareArr(CodeShareArr) {
		
		var control = document.getElementById("sel_CodeShare");
		for ( var i = 0; i < CodeShareArr.length; i++) {        //generating html
			control.options[i] = new Option(CodeShareArr[i][0], CodeShareArr[i][0]);
		}
	}
	
	function isPublishedToCodeShare(val) {
//		val:"A1|W5440"
		if(getFieldByID("hdnMode").value != "ADD"){
			if(flightData[strGridRow][31]!=undefined && flightData[strGridRow][31]!=null){
				var publishedGdsIdsArr = flightData[strGridRow][31].split(",");
				if(val!=undefined && val!="" && publishedGdsIdsArr!=undefined 
						&& publishedGdsIdsArr!=null && publishedGdsIdsArr.length>0){
					var codeShareArr = val.split("|");		
					if(codeShareArr!=undefined && codeShareArr!=null 
							&& gdsCarrierCodeArray!=undefined && gdsCarrierCodeArray!=null){
						for(var i=0;i<gdsCarrierCodeArray.length;i++){
							var gdsCarrierCodeText = gdsCarrierCodeArray[i]; //"3|R1"
	
							var gdsCarrierCodeArr =gdsCarrierCodeText.split("|");
							var gdsId;
							if(gdsCarrierCodeArr[1] == codeShareArr[0]){
								gdsId = gdsCarrierCodeArr[0];
							}
							
							//check is already published to any code share carriers
							for(var j=0;j<publishedGdsIdsArr.length;j++){
								if(publishedGdsIdsArr[j]==gdsId){
									return true;
								}
							}
							
						}
					}
	
				}
			}
		}
		
		return false;
	}
	
	/**
	 * restrict control operation of flight which is not actuallly operated by this carrier. 
	 * */
	function disableCtrlForCodeShare() {
		if (trim(getVal("txtCsOCFlightNo")) != ""
				&& trim(getVal("txtCsOCCarrierCode")) != "") {			
			Disable("btnEdit", true);				
			Disable("btnCopy", true);
			Disable("btnGDS", true);
		}
	}
	
	function hidePublisheMessageAudit(gdsList){
		if(gdsList != undefined && gdsList !=null && gdsList != ""){
			$('#btnPubMessages').show();
			Disable("btnPubMessages", false);				
		}else {		
			Disable("btnPubMessages", true);
			$('#btnPubMessages').hide();
		}	
	}
