var pageEdited = false;
arrSegData = sort(arrSegData, 1);
var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "60%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Flight No";
objCol1.headerText = "Segment";
objCol1.itemAlign = "left";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "20%";
objCol2.arrayIndex = 2;
objCol2.toolTip = "Validitiy";
objCol2.headerText = "Valid Y/N";
objCol2.itemAlign = "center";

var objCol3 = new DGColumn();
objCol3.columnType = "checkbox";
objCol3.width = "20%";
objCol3.arrayIndex = 0;
objCol3.linkOnClick = "invalidateSegments";
objCol3.toolTip = "Validitiy";
objCol3.headerText = "Valid Y/N";
objCol3.itemAlign = "center";

// ---------------- Grid
var objDG = new DataGrid("spnValidSeg");
objDG.addColumn(objCol1);
objDG.addColumn(objCol3);
objDG.width = "99%";
objDG.height = "90px";
objDG.headerBold = false;
objDG.arrGridData = arrSegData;
objDG.rowOver = false;
objDG.seqNo = false;
objDG.backGroundColor = "#ECECEC";
objDG.setColors = "setReversColorGrid";

var objDG2 = new DataGrid("spnGridOLapSeg");
objDG2.addColumn(objCol1);
objDG2.addColumn(objCol2);
objDG2.width = "99%";
objDG2.height = "90px";
objDG2.headerBold = false;
objDG2.rowOver = false;
objDG2.seqNo = false;
objDG2.backGroundColor = "#ECECEC";
objDG2.setColors = "setColorGrid";
objDG2.displayGrid();

var overLapSegCode = "";
var blnOverlap = false;
var overschedid = "";
var olSegment = new Array();

// setting the color in the grid
function setColorGrid(strData) {
	var arrReturnData = strData;
	if (getValue("selOLSch") != "") {
		for ( var i = 0; i < arrSegData.length; i++) {
			if (arrSegData[i][1] == arrReturnData[1]) {
				overLapSegCode = arrSegData[i][2]; // arrOLSegData[getValue("selOLSch")][arrReturnData[0]][3];
				return "green";
			}
		}
	}
}

function setReversColorGrid(strData) {
	if (getValue("selOLSch") != "") {
		for ( var i = 0; i < olSegment.length; i++) {
			if (olSegment[i][1] == strData[1]) {
				return "green";
			}
		}
	}
}

// function when selected overlap
function spnGridOLapSeg_onChange() {
	var strValue = getValue("selOLSch");
	var loadGrid = "";
	setVisible("spnGridOLapSeg", false);
	setVisible("divOLap3", false);
	if (strValue != "") {
		setVisible("spnGridOLapSeg", true);
		setVisible("divOLap3", true);
		dataChanged();
		arrOLSegData[strValue] = sort(arrOLSegData[strValue], 1);
		objDG2.arrGridData = arrOLSegData[strValue];
		olSegment = arrOLSegData[strValue];
		loadGrid = objDG.getSelectedColumn(1);
		objDG2.refresh();
		objDG.refresh();

		setVisible("divRemove", true);
		objTimer = setInterval('oLapGridCheck()', 300);

		clearInterval(objTimer);

		for ( var i = 0; i < loadGrid.length; i++) {
			if ((loadGrid[i][1] == "false") || !(loadGrid[i][1])) {
				objDG.setCellValue(i, 1, "false");
			} else {
				objDG.setCellValue(i, 1, "true");
			}

		}

	}
}

function oLapGridCheck() {
	if (objDG2.loaded) {
		clearInterval(objTimer);
		objDG.refresh();
	}
}
// show the overlaps
function ViewOverLap() {
	var strArray = objDG.getSelectedColumn(1);
	blnOverlap = true;
	var validity = false;
	for ( var j = 0; j < strArray.length; j++) {
		if (strArray[j][1]) {
			validity = true;
			break;
		}
	}
	if (validity) {
		setVisible("divOLap1", true);
	} else {
		validateError(onevalidseg);
		return;
	}
}

var objTimer = setInterval("onLoad()", 300);

function onLoad() {
	var loadGrid = "";
	if (objDG.loaded == true) {
		clearInterval(objTimer);
		setVisible("divOLap1", false);
		setVisible("divOLap2", false);
		setVisible("divOLap3", false);
		setVisible("spnGridOLapSeg", false);
		setVisible("divRemove", false);

		for ( var i = 0; i < arrSegData.length; i++) {
			if ((arrSegData[i][2] == "false") || !arrSegData[i][2]) {
				objDG.setCellValue(i, 1, "false");
			} else {
				objDG.setCellValue(i, 1, "true");
			}

		}

		var selectedValues = opener.getFieldByID("hdnSegArray").value;
		var arrSelectedData = new Array();
		if ((selectedValues != null) && (selectedValues != "")) {
			arrSelectedData = selectedValues.split(",");
		}

		for ( var i = 0; i < arrSelectedData.length; i++) {
			if ((arrSelectedData[i] != null) && (arrSelectedData[i] != "")) {
				var arrSelectedSegData = arrSelectedData[i].split("_");
				for ( var j = 0; j < arrSegData.length; j++) {
					if (arrSegData[j][1] == arrSelectedSegData[0]) {
						if (arrSelectedSegData[1] == "false") {
							objDG.setCellValue(j, 1, "false");
						} else {
							objDG.setCellValue(j, 1, "true");
						}
						break;
					}
				}
			}
		}
		loadGrid = objDG.getSelectedColumn(1);
		var olid = trim(getFieldByID("hdnOverlapId").value);
		if (olid == "null" || olid == "") {
			clearInterval(objTimer);
			Disable("btnOLap", "");
		} else {

			Disable("btnOLap", "false");
			setField("selOLSch", olid);
			Disable("selOLSch", "false");
			setVisible("spnGridOLapSeg", true);
			setVisible("divOLap1", true);
			setVisible("divOLap2", true);
			setVisible("divOLap3", true);
			arrOLSegData = sort(arrOLSegData, 1);
			objDG2.arrGridData = arrOLSegData;
			olSegment = arrOLSegData;
			overschedid = olid;
			document.getElementById('spnOlapId').innerHTML = "<font>" + olid
					+ "</font>";
			objDG2.refresh();
			objDG.refresh();

			setVisible("divRemove", true);
			objTimer = setInterval('oLapGridCheck()', 300);
			clearInterval(objTimer);
			for ( var i = 0; i < loadGrid.length; i++) {
				if ((loadGrid[i][1] == "false") || !(loadGrid[i][1])) {
					objDG.setCellValue(i, 1, "false");
				} else {
					objDG.setCellValue(i, 1, "true");
				}

			}

		}
		if (arrOLSegData.length < 1) {
			Disable("btnOLap", "false");
		}
	}
	setPageEdited(false);
}

function confirmValidSegment() {
	var strValidSegment = "";
	var strOLapSchedId = getValue("selOLSch");
	Disable("selOLSch", "");

	// check for button click and not selected id
	if (blnOverlap && (trim(getFieldByID("selOLSch").value) == "")) {
		var strconfirm = confirm("Warning: Overlap Flight Number not selected");
		if (strconfirm == true) {
			var strArray = objDG.getSelectedColumn(1);
			if (!validateSegments(strArray, strOLapSchedId))
				return;
		} // end of string confirmation
		// if button clicked and selected the id
	} else if ((blnOverlap && (trim(getFieldByID("selOLSch").value) != ""))
			|| overschedid != "") {
		if (trim(getFieldByID("selOLSch").value) == "")
			strOLapSchedId = overschedid;
		var strArray = objDG.getSelectedColumn(1);
		if (!validateSegments(strArray, strOLapSchedId))
			return;

	} else if (!blnOverlap) {
		var strArray = objDG.getSelectedColumn(1);
		if (!validateSegments(strArray, strOLapSchedId))
			return;
	} else {
		validateError(nosegfound);
	}
}

function validateError(msg) {
	objMsg.MessageText = msg;
	objMsg.MessageType = "Error";
	ShowPageMessage();
	var olid = trim(getFieldByID("hdnOverlapId").value);
	if (olid != "null" && olid != "") {
		Disable("selOLSch", "false");
	}
}

function removeOverLap() {
	var strValidSegment = "";
	var strconfirm = confirm("Overlap with Schedule removed. Confirm update ");
	if (strconfirm == true) {
		Disable("selOLSch", true);
		var strArray = objDG.getSelectedColumn(1);
		if (!validateSegments(strArray, ""))
			return;
	}
}

function validateSegments(strArray, olap) {
	var strValidSegment = "";
	var validity = false;
	for ( var j = 0; j < strArray.length; j++) {
		if (strArray[j][1]) {
			validity = true;
			break;
		}
	}

	if (validity) {
		for ( var i = 0; i < strArray.length; i++) {
			if ((!strArray[i][1]) && (arrSegData[i][3] == "true")) {
				validateError(resExist);
				return false;
			}
		}

		for ( var i = 0; i < arrSegData.length; i++) {
			strValidSegment += arrSegData[i][1] + "_";
			strValidSegment += strArray[i][1] + ",";
		}
		if (olap != "") {
			var selscedtobld = objDG.getSelectedColumn(1);
			for ( var i = 0; i < olSegment.length; i++) {
				for ( var j = 0; j < arrSegData.length; j++) {
					if (olSegment[i][1] == arrSegData[j][1]) {
						if ((olSegment[i][2] == "Y") && (selscedtobld[j][1])) {
							validateError(needinvalid);
							return false;
						}
					}
				}
			}
		}
		opener.validatedSegments(strValidSegment, overLapSegCode, olap);
		opener.top[0].objWindow.close();
		// opener.objWindow.close();
	} else {
		validateError(onevalidseg);
		return false;
	}
}

function setPageEdited(isEdited) {
	pageEdited = isEdited;
}

function dataChanged() {
	setPageEdited(true);
}

function invalidateSegments() {
	dataChanged();
}

function cancelClick() {
	if (pageEdited) {
		var cnf = confirm("Changes will be lost! Do you wish to Continue?");
		if (cnf) {
			setPageEdited(false);
			window.close();
		}
	} else {
		setPageEdited(false);
		window.close();
	}
}