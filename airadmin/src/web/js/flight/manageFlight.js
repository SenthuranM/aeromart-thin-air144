var pageEdited = false;
 
 
 
function closeClick() {
	if (pageEdited) {
		var cnf = confirm("Changes will be lost! Do you wish to Continue?");
		if (cnf) {
			setPageEdited(false);
			parent.closeClick();
		}
	} else {
		setPageEdited(false);
		parent.closeClick();
	}
}

function setPageEdited(isEdited) {
	pageEdited = isEdited;
}

 


// Clear all input controls
function clearInputControls() {
 

}

function enableInputControls() {
 

}

function clearClick() {
	clearInputControls();
	enableInputControls();
}

var processSelectedOption = function(control) {
	try{
		return getValue(control);
	}catch(e){
		return "false";
	}
		
}

var haveSelected = function(){
	
}

function confirmClick(){
	 
	var reinstate = processSelectedOption("chkreinst");
	var canConf = true;
	var cnf = confirm("Please confirm to continue with this operation ?");
	 
	if(hdnNoOptions == true || hdnNoOptions == "true"){
		canConf = false;	
	}
	
	if (cnf) {
		if(canConf) {
			parent.confirmManageFlight(processSelectedOption("radActOpnSel"),processSelectedOption("radActClsSel"),processSelectedOption("radActOpenAll"),processSelectedOption("radActClsAll"),processSelectedOption("radActSysClsSel"), reinstate, hdnScheduleID);
		}else {
			alert("There are no updates to save !")
		}		
		//window.close();
	}	 
}

var selectOption = function(name){
	 try{
		//document.getElementById(name).checked = true;
	 }catch(e){ }	
}

function onLoad(){
	if(hdnNoOptions == true || hdnNoOptions == 'true'){
		Disable("btnConfirm", true);
	}else {
		Disable("btnConfirm", false);
	}
}


function handleDependancies(conroller) {
	 try{
		var conrolled = getControlled(conroller);
		var subDependant = getSubDependantCtrl(conroller);
	 
			var thisControl =  getValue(conroller);
			if(thisControl == "true"){
				getFieldByID(conrolled).checked = false;
				Disable(conrolled, true);
				getFieldByID(subDependant).checked = false;
				Disable(subDependant, true);
			}else{
				Disable(conrolled, false);
				Disable(subDependant, false);
			}
	 }catch(e){
		 
	 }
}

function getControlled(controller){
	if(controller == "radActOpenAll"){
		return "radActClsAll";
	}else if(controller == "radActClsAll"){
		return "radActOpenAll";
	}
}

function handleSimpleDependnt(conroller){
	try{
		var dependant = getSubDependantCtrl(conroller);
		var thisControl =  getValue(conroller);
		if(thisControl == "true"){
			getFieldByID(dependant).checked = false;
			Disable(dependant, true);
		}else{
			Disable(dependant, false);
		}
	}catch(e){
		 
	 }
}

function getSubDependantCtrl(controller){
	if(controller == "radActOpenAll"){
		return "radActOpnSel";
	}else if(controller == "radActClsAll"){
		return "radActClsSel";
	}else if(controller == "radActOpnSel"){
		return "radActOpenAll";
	}else if(controller == "radActClsSel"){
		return "radActClsAll";
	}
}

onLoad();
