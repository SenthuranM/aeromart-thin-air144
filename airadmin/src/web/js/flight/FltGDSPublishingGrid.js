var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "20%";
objCol1.arrayIndex = 1;
objCol1.headerText = "GDS Code";
objCol1.itemAlign = "center"
objCol1.sort = true;

var objCol2 = new DGColumn();
objCol2.columnType = "CUSTOM";
objCol2.width = "30%";
objCol2.arrayIndex = 2;
objCol2.toolTip = "Publish Status";
objCol2.headerText = "Publish Status";
objCol2.ID = 'publishing';
objCol2.htmlTag = "<select  id='publishing' name='publishing' style='width:100px' :CUSTOM:><option value='1'>Publish</option><option value='0'>Unpublish</option></select>";
objCol2.itemAlign = "center"

var objCol3 = new DGColumn();
objCol3.columnType = "LABEL";
objCol3.width = "10%";
objCol3.arrayIndex = 3;
objCol3.toolTip = "GDS Status";
objCol3.headerText = "GDS Status";
objCol3.itemAlign = "center";

var objCol4 = new DGColumn();
objCol4.columnType = "LABEL";
objCol4.width = "20%";
objCol4.arrayIndex = 4;
// objCol4.toolTip = "Status" ;
objCol4.headerText = "Publish Mechanism";
objCol4.itemAlign = "center"

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "20%";
objCol5.arrayIndex = 5;
objCol5.headerText = "Additional Information";
objCol5.itemAlign = "center"

// ---------------- Grid
var objDG = new DataGrid("spnGDS");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.width = "99%";
objDG.height = "200px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = arrData;
objDG.seqNo = true;
objDG.seqNoWidth = "5%";
objDG.rowClick = "RowClick";
objDG.displayGrid();
objDG.shortCutKeyFunction = "Body_onKeyDown";
