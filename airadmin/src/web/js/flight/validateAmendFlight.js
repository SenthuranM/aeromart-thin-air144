var pageEdited = false;
document.getElementById('divEmailDesc').style.display = 'none';
setVisible("divClearButton", false);
function confirmClick() {
	var strAlerts = getFieldByID("chkGA1").checked;
	var strEmail = getFieldByID("chkSE1").checked;
	var strTo = getFieldByID("txtTo").value;
	var strSubject = getFieldByID("txtSubject").value;
	var strContent = getFieldByID("txtContents").value;

	var strSmsAlert = getFieldByID("chkSendSMS").checked;
	var strEmailAlert = getFieldByID("chkSendEmail").checked;

	var strSmsCnf = getFieldByID("chkSmsCnf").checked;
	var strSmsOnH = getFieldByID("chkSmsOnH").checked;
	var openEtCnf = "";

	if (strEmail) {
		var strAddress = getFieldByID("txtTo").value;
		if (trim(strAddress) == "") {
			validateError(opener.noaddress);
			getFieldByID("txtTo").focus();
			return;
		}
		if (!checkEmail(strAddress)) {
			validateError(opener.invalidmailformat);
			getFieldByID("txtTo").focus();
			return;
		}
		var strContent = getFieldByID("txtContents").value;
		if (strContent.length > 255) {
			validateError(opener.invalidlength);
			getFieldByID("txtContents").focus();
			return;
		}
	}
	if (strSmsAlert) {
		if (!strSmsCnf && !strSmsOnH) {

			validateError(opener.smsempty);
			getFieldByID("chkSmsCnf").focus();
			return;
		}
	}
	if (getFieldByID("chkOpenEtickets")) {
		openEtCnf = getFieldByID("chkOpenEtickets").checked;
	}

	opener.confirmUpdate(strAlerts, strEmail, strTo, strSubject, strContent,
			strSmsAlert, strEmailAlert, strSmsCnf, strSmsOnH, openEtCnf);
	window.close();
}

function closeClick() {
	if (pageEdited) {
		var cnf = confirm("Changes will be lost! Do you wish to Continue?");
		if (cnf) {
			setPageEdited(false);
			window.close();
		}
	} else {
		setPageEdited(false);
		window.close();
	}
}

function setPageEdited(isEdited) {
	pageEdited = isEdited;
}

function validateError(msg) {
	objMsg.MessageText = msg;
	objMsg.MessageType = "Error";
	ShowPageMessage();
}

function checkClick() {
	if ((getFieldByID("chkGA1").checked) || (getFieldByID("chkSE1").checked)) {
		setPageEdited(true);
	}
	if ((getFieldByID("chkSE1").checked)) {
		document.getElementById('divEmailDesc').style.display = 'block';
		setVisible("divClearButton", true);
		window.resizeTo(720, 425);
	} else {
		document.getElementById('divEmailDesc').style.display = 'none';
		setVisible("divClearButton", false);
		window.resizeTo(720, 275);
	}
}

function writeEmail() {
	var strHTMLText = "";
	strHTMLText += '<table width="100%" border="0" cellpadding="2" cellspacing="2">';
	strHTMLText += '	<tr>';
	strHTMLText += '		<td colspan="2"><hr></td>';
	strHTMLText += '	</tr>';
	strHTMLText += '	<tr>';
	strHTMLText += '		<td><font class="fntBold">EMail - To Call Center</font></td>';
	strHTMLText += '	</tr>';
	strHTMLText += '	<tr>';
	strHTMLText += '		<td><font>To</font></td>';
	strHTMLText += '		<td><input type="text" id="txtTo" style="width:150px;" maxlength="255" name="txtTo" value=""></td>';
	strHTMLText += '	</tr>';
	strHTMLText += '	<tr>';
	strHTMLText += '		<td><font>Subject</font></td>';
	strHTMLText += '		<td><input type="text" id="txtSubject" style="width:150px;" maxlength="255" name="txtSubject" value=""></td>';
	strHTMLText += '	</tr>';
	strHTMLText += '	<tr>';
	strHTMLText += '		<td><font>Contents</font></td>';
	strHTMLText += '		<td><textarea name="txtContents" id="txtContents" cols="50" rows="3"></textarea></td>';
	strHTMLText += '	</tr>';
	strHTMLText += '</table>';

	DivWrite("spnEmail", strHTMLText);
}

// Clear all input controls
function clearInputControls() {
	setField("txtTo", "");
	setField("txtSubject", "");
	setField("txtContents", "");

}

function enableInputControls() {
	Disable("txtTo", "");
	Disable("txtSubject", "");
	Disable("txtContents", "");

}

function clearClick() {
	clearInputControls();
	enableInputControls();
}
writeEmail();

function enableSMSChk() {

	if (document.getElementById("chkSendSMS").checked == true) {
		Disable("chkSmsCnf", false);
		Disable("chkSmsOnH", false);
	} else {
		Disable("chkSmsCnf", true);
		Disable("chkSmsOnH", true);
		if (document.getElementById("chkSmsCnf").checked == true) {
			document.getElementById("chkSmsCnf").checked = false;
		}
		if (document.getElementById("chkSmsOnH").checked == true) {
			document.getElementById("chkSmsOnH").checked = false;
		}
	}

}

if (opener.sendSMSForPax == "Y") {
	Disable("chkSendSMS", false);
	Disable("chkSmsCnf", true);
	Disable("chkSmsOnH", true);

} else {
	Disable("chkSendSMS", true);
	Disable("chkSmsCnf", true);
	Disable("chkSmsOnH", true);
}
if (opener.sendEmailForPax == "Y") {
	Disable("chkSendEmail", false);

} else {
	Disable("chkSendEmail", true);
}