

	function UI_commons(){}
	/*
	 * Booking Info Tab Page On Load
	 */

	UI_commons.strDTFormat = "dd/mm/yy";
	
	UI_commons.readOnly = function (status){
	   if(status){
		   $("#page-mask").css("display", "block");
	   }else{
		   $("#page-mask").css("display", "none");
	   }
   	}

   	UI_commons.initializeMessage = function(){
		top[2].MessageReset();
	}
   	
	
	/*
	 * Page On Change 
	 */
	UI_commons.pageOnChange = function(){
		top[2].HidePageMessage();
	}
   	
	
	/*
	 * Show Progress Bar
	 */
	UI_commons.showProgress = function(){
		$("#divLoadBg").removeClass('loadingDone');
		$("#divLoadBg").addClass('loadingInProgress');
		
		$("#divLoadMsg").show();
	}
	
	/*
	 * Reset Page Time out 
	 */
	UI_commons.resetTimeOut = function(){
		if (arguments.length == 1){
			opener.top.ResetTimeOut();
		}else{
			top.ResetTimeOut();
		}
	}
	
	/*
	 * Hide Progress Bar
	 */
	UI_commons.hideProgress = function(){
		$("#divLoadMsg").hide();
		$("#divLoadBg").removeClass('loadingInProgress');
		$("#divLoadBg").addClass('loadingDone');
		try{
			UI_commons.resetTimeOut();
		}catch(ex){}
	}
	
	function UI_usrNoteRolFwd(){}
	
	UI_usrNoteRolFwd.initialize = function(){
			UI_commons.readOnly(true);
			$("#btnRollForward").show();
			$("#btnRollForward").attr('style', 'width:auto;');
			Disable("btnRollForward",false);
			$("#btnRollForward").attr('title', '');
			$("#btnRollForward").click(function(){UI_usrNoteRolFwd.showOHDRollForward();});
			$("#btnNoteRollForwardSearchAction").click(function(){UI_usrNoteRolFwd.noteRollForwardOnClick();});
			$("#btnNoteRollForwardCancel").click(function(){
				UI_commons.readOnly(false);$('#divNoteRollForward').hide();
		   		$("#rollForwardStartDate").val("");
		   		$("#rollForwardEndDate").val("");
		   		top[2].MessageReset();
			});
			$("#btnNoteRollForwardConfirm").click(function(){UI_usrNoteRolFwd.noteRollForwardConfirmOnClick();});
			$("#chkNoteRFAll").click(function(){UI_usrNoteRolFwd.toggleChecked(this.checked)});
			
	}


   UI_usrNoteRolFwd.convertDeptDate = function(deptDateStr){
	   var deptDateArr = deptDateStr.split('/');
	   return new Date(deptDateArr[1] + '/' + deptDateArr[0] + '/' + deptDateArr[2]);
   }
   
   UI_usrNoteRolFwd.getMinDateForRollForward = function(allowedDate){
		var minDate = new Date();
		if(allowedDate != null && allowedDate != ""){
			var dtArr = allowedDate.split('/');
			minDate = new Date(parseInt(dtArr[2], 10), parseInt(dtArr[1], 10)-1, parseInt(dtArr[0], 10));
		}
		
		return minDate;
		
	}
   
   UI_usrNoteRolFwd.setDates = function(){
	     // departure date of the selected value
	   $("#rollForwardStartDate").datepicker("destroy");
	   $("#rollForwardEndDate").datepicker("destroy");
	   var departueDate = $("#hdnFlightDate").val();
	   var rfMinDate = UI_usrNoteRolFwd.isFutureDate(departueDate);    
		$("#rollForwardStartDate").datepicker({ dateFormat: UI_commons.strDTFormat, changeMonth: 'true' , 
				changeYear: 'true', showButtonPanel: 'true', minDate: rfMinDate, beforeShow: _setZIndex});
		
		$("#rollForwardEndDate").datepicker({ dateFormat: UI_commons.strDTFormat, changeMonth: 'true' , 
				changeYear: 'true', showButtonPanel: 'true', minDate: rfMinDate, beforeShow: _setZIndex });
   }
   
   UI_usrNoteRolFwd.showNoteRollForward = function(){
	   
	   UI_usrNoteRolFwd.initialize();
	   UI_usrNoteRolFwd.setDates();
	    UI_usrNoteRolFwd.enableRollForwardButtons(true);
	    $("#btnNoteRollForwardConfirm").hide();
		$("#divRFAvailFlights").hide();
		$('#divNoteRollForward').show();
		$(window).scrollTop($('#divNoteRollForward').offset().top);
   }
   
	_setZIndex = function(dateText, inst){
		if($("#ui-datepicker-div")){			
			$("#ui-datepicker-div").css('z-index', '1002');
		}
	}
	
   UI_usrNoteRolFwd.isFutureDate = function(date){
	   
	   if(!date){
		   return null;
	   }
	   
	   var today = new Date();
	   var date = date.split(' ')[0].split('-');
	   var checkDate =  new Date(date[2],date[1]-1,date[0]);
	   
	   if(today > checkDate){
		   return today;
	   }else{
		   return checkDate;
	   }
   }
   
   // search button click event
   UI_usrNoteRolFwd.noteRollForwardOnClick = function(){
   	if (!UI_usrNoteRolFwd.noteRFSubmitted) {
		UI_usrNoteRolFwd.enableRollForwardButtons(false);
		$("#divRFAvailFlights").hide();
		if(!UI_usrNoteRolFwd.validateNoteRFSearch()){
			return false;
		}else{
			top[2].MessageReset();
		}
		UI_usrNoteRolFwd.noteRFSubmitted = true;
		var data = {};
		//data['jsonResSegs'] = UI_reservation.jsonSegs;
		data['startDate'] = $("#rollForwardStartDate").val();
		var endDate = $("#rollForwardEndDate").val().split("/");
		data['endDate'] = (parseInt(endDate[0],10)+1) + "/" + endDate[1] + "/" + endDate[2];
		data['userNote'] = $("#hdnTxtUserNotes").val();
		data['flightId'] = $("#hdnFlightId").val();
		data['schedId'] = $("#hdnScheduleId").val();	
		data['deptDate'] = $("#hdnFlightDate").val();
		$.ajax( {
			type : 'POST',
			dataType : 'json',
			url : 'userNoteRollForwardAvailability.action',
			data : data,
			success : function(response) {
				UI_usrNoteRolFwd.rollforwardSearchSuccess(response);
			},
			error : function(response){
				UI_usrNoteRolFwd.noteRollForwardConfirmError(response);
			},
			cache : false
			});
	}
   }
   
   UI_usrNoteRolFwd.validateNoteRFSearch = function(){
   		var startDateStr = $("#rollForwardStartDate").val();
		var endDateStr = $("#rollForwardEndDate").val();
		
		if (startDateStr=="" || startDateStr==null){
			showERRMessage(startdate);
			$("#rollForwardStartDate").focus();		
			UI_usrNoteRolFwd.enableRollForwardButtons(true);
			return false;
		}
		
		if (endDateStr=="" || endDateStr==null){
			showERRMessage(stopdate);
			$("#rollForwardEndDate").focus();		
			UI_usrNoteRolFwd.enableRollForwardButtons(true);
			return false;
		}
		
		if (!CheckDates(startDateStr, endDateStr)){
			showERRMessage(stopdategreat);
			$("#rollForwardEndDate").focus();	
			UI_usrNoteRolFwd.enableRollForwardButtons(true);
			return false;
		}
		
		return true;		
   }
   
   UI_usrNoteRolFwd.rollforwardSearchSuccess = function(response){
   		UI_usrNoteRolFwd.enableRollForwardButtons(true);
   		UI_usrNoteRolFwd.noteRFSubmitted  = false;
		
		$('#trNoteRollForwardTemp').parent().find('tr').filter(function() {			
			var regex = new RegExp('trNoteRollForwardTempd_*');
	        return regex.test(this.id);
	    }).remove();
		
		var dateInt = function(strDate){
			  var d = strDate.split('-');
			  var dd= new Date(d[0],parseInt(d[1],10)-1,d[2],0,0,0);
			  return dd.getDay();
		}
		
		if(response.success){
			
			if(response.availFlightRS != null && response.availFlightRS != ""){
					$("#divRFAvailFlights").show();
					var rollForwardConfirmEnabled = false;
					UI_usrNoteRolFwd.rollForwardAvailFlights = response.availFlightRS;
					
					for(var i=0;i<response.availFlightRS.length;i++){
						var deptDateTime = response.availFlightRS[i].departureDate;
						var deptDate = (deptDateTime!=null)?deptDateTime.split("T")[0]:"";
						
						var tmpId = 'trNoteRollForwardTempd_' + deptDate;
						var chId = 'chkNoteRF_' + deptDate;
						
						var clone = $('#trNoteRollForwardTemp').clone().show();
						clone.attr('id',tmpId);
						clone.appendTo($('#trNoteRollForwardTemp').parent());  
						
						var chBox = $('#'+tmpId+' input');
						chBox.attr('id',chId);		
						chBox.attr('class',"checkbox");
						chBox.attr('tabIndex',9000+i);
						chBox.val(i);
											
						var label = $('#'+tmpId+' label');
						label.attr('id','lblNoteRF_'+deptDate);
						
						var dispLabel = deptDate;
						chBox.addClass('rfDate');
						
						rollForwardConfirmEnabled = true;
						dispLabel += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+response.availFlightRS[i].flightId;
						chBox.addClass(dateInt(deptDate)+'');
						chBox.addClass('7');
				
						label.html(dispLabel);
						label.click(function(e){ var a = e.target.id.split('_'); $('#chkNoteRF_'+a[1]).click()});
					}
					
					$("#chkNoteRFAll").attr("checked",false);
					if(rollForwardConfirmEnabled){
						$("#trNoteRFConfirm").show();
					    $("#btnNoteRollForwardConfirm").show();
					} else {
						$("#trNoteRFConfirm").hide();
					}
			}
		}
   }
   
   UI_usrNoteRolFwd.noteRollforwardError = function(response){
   		UI_usrNoteRolFwd.enableRollForwardButtons(true);
   		UI_usrNoteRolFwd.noteRFSubmitted = false;
   }
   
   UI_usrNoteRolFwd.noteRollForwardConfirmOnClick = function(){
	   	top[2].MessageReset();
   		UI_usrNoteRolFwd.enableRollForwardButtons(false);
   		var checkedArr = $("#divRFAvailFlights input:checked.rfDate");
   		var count = checkedArr.length;
		
		if(count == 0){
			alert("Please select atleast one flight to roll forward");
			UI_usrNoteRolFwd.enableRollForwardButtons(true);
		} else {
			var submitFlights = "";
			for(var i=0;i<count;i++){
				submitFlights += UI_usrNoteRolFwd.rollForwardAvailFlights[checkedArr[i].value].flightId + ",";
			}
			
			var data = {};
			data['jsonRollForwardFlights'] = submitFlights;
			data['userNote'] = $("#hdnTxtUserNotes").val();
			data['schedId'] = $("#hdnScheduleId").val();	
			
			$.ajax({
				type: "POST",
				dataType: 'json',
				data: data,
				url: "userNoteRollForward.action",
				success: UI_usrNoteRolFwd.noteRollForwardConfirmSuccess,
				error: UI_usrNoteRolFwd.noteRollForwardConfirmError,
				cache : false
			});
			
		}
	
   }
   
   UI_usrNoteRolFwd.noteRollForwardConfirmSuccess = function(response){   
	  
	alert("User note has been successfully roll forwarded.");
    
   	UI_usrNoteRolFwd.enableRollForwardButtons(true);
   	$("#divRFAvailFlights").css("display" , "none");
   	$("#trNoteRFConfirm").hide();
   	$("#rollForwardStartDate").val("");
   	$("#rollForwardEndDate").val("");
   	SearchData();
   }
   

   
   UI_usrNoteRolFwd.toggleChecked = function(status){
	   $(".checkbox").each( function() {
		   $(this).attr("checked",status);
		   $(this).attr("disabled",status);
		})
   }
   

   
   UI_usrNoteRolFwd.noteRollForwardConfirmError = function(response){
   		UI_usrNoteRolFwd.enableRollForwardButtons(true);
   		$("#divRFAvailFlights").hide();
   		$("#trNoteRFConfirm").hide();
   		$("#rollForwardStartDate").val("");
   		$("#rollForwardEndDate").val("");
   }
   
   UI_usrNoteRolFwd.enableRollForwardButtons = function(opt){
   		if(opt) {
   			Disable("btnNoteRollForwardSearchAction",false);
			Disable("btnNoteRollForwardCancel",false);
			Disable("btnNoteRollForwardConfirm",false);
		} else {
			Disable("btnNoteRollForwardSearchAction",true);
			Disable("btnNoteRollForwardCancel",true);
			Disable("btnNoteRollForwardConfirm",true);
		}
	}
	
