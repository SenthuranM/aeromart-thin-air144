var objWindow;
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtCopyToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 10;
	objCal1.left = 0;
	objCal1.showCalendar(objEvent);
	objCal1.onClick = "setDate";
}

function dataChanged() {
	setPageEdited(true);
}

// On Page loading make sure that all the page input fields are cleared
function winOnLoad() {
	var olapFlightId = getFieldByID("hdnOverlapFlightId").value;
	if (olapFlightId == "") {
		olapFlightId = "-";
	}
	clearInputControls();
	setPageEdited(false);
	document.getElementById('spnFlightId').innerHTML = getFieldByID("hdnFlightId").value;
	document.getElementById('spnFlightseg').innerHTML = getFieldByID("hdnTrip").value;
	document.getElementById('spnDeptDate').innerHTML = getFieldByID("hdnStartD").value;
	document.getElementById('spnOlapFlightId').innerHTML = olapFlightId;
	getFieldByID("txtCopyToDate").focus();

}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function confirmCopy() {
	// Mandotory Fields

	var strFromD = getFieldByID("txtCopyToDate").value;
	if (trim(strFromD) == "") {
		validateError(opener.cpytonull);
		getFieldByID("txtCopyToDate").focus();
		return;
	} else {
		var cpDate = dateChk(strFromD);
		var strOriginalDate = dateChk(getFieldByID("hdnStartD").value);
		var toDAy = dateChk(getFieldByID("hdnCurrentDate").value);

		if (!dateChk("txtCopyToDate")) {
			validateError(opener.invalidDate);
			getFieldByID("txtCopyToDate").focus();
			return;
		}
		if (CheckDates(cpDate, toDAy)) {
			validateError(opener.togreater);
			getFieldByID("txtCopyToDate").focus();
			return;

		} else if (CheckDates(cpDate, strOriginalDate)
				&& CheckDates(strOriginalDate, cpDate)) {
			validateError(opener.samedate);
			getFieldByID("txtCopyToDate").focus();
			return;
		} else {
			var strFlightId = getFieldByID("hdnFlightId").value;
			var recNo = opener.getTabValue()[0];
			// var strSearch=opener.top[0].strSearchCriteria.split(",");
			var strSearch = opener.getTabValue()[1].split(",");
			var searchstd = strSearch[0];
			var searchstpd = strSearch[1];
			var searchfltno = strSearch[2];
			var searchfrm = strSearch[3];
			var searchto = strSearch[4];
			var searchoptype = strSearch[5];
			var searchstat = strSearch[6];
			var searchtime = strSearch[7];
			var searchstartflt = strSearch[8];
			opener.location = "showFlight.action?&hdnRecNo=" + recNo
					+ "&hdnMode=COPY&hdnFlightId=" + strFlightId
					+ "&hdnCopyToDay=" + strFromD + "&txtStartDateSearch="
					+ searchstd + "&txtStopDateSearch=" + searchstpd
					+ "&txtFlightNoSearch=" + searchfltno + "&selFromStn6="
					+ searchfrm + "&selToStn6=" + searchto
					+ "&selOperationTypeSearch=" + searchoptype
					+ "&selStatusSearch=" + searchstat + "&hdnTimeMode="
					+ searchtime + "&txtFlightNoStartSearch=" + searchstartflt;
			opener.top[0].objWindow.close();
			opener.top[2].ShowProgress();
		}
	}
}

// Clear all input controls
function clearInputControls() {
	setField("txtCopyToDate", "");
}

function validateError(msg) {
	objMsg.MessageText = msg;
	objMsg.MessageType = "Error";
	ShowPageMessage();
}

function cancelClick() {
	if (pageEdited) {
		var cnf = confirm("Changes will be lost! Do you wish to Continue?");
		if (cnf) {
			setPageEdited(false);
			window.close();
		}
	} else {
		setPageEdited(false);
		window.close();
	}
}

function setPageEdited(isEdited) {
	pageEdited = isEdited;
}