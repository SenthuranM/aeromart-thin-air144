function UI_flightPaxSt() {} 

$(document).ready(function(){
	
	UI_flightPaxSt.loadFlightPage();
	top[2].HideProgress();
	
	$("#btnSearch").click(function() { 
		$("#divFlownFlightsPager").clearGridData();
		UI_flightPaxSt.searchFlightOnClick();
		top[2].HideProgress();
	});
	
	$("#btnClose").click(function() { 
		UI_flightPaxSt.closeOnClick();
	});
	
	$("#btnUpdateStatus").click(function() { 
		UI_flightPaxSt.updateStatusOnClick();
	});
	
	$("#txtDept").datepicker({
		showOn: "button",
		buttonImage: "../../images/calendar_no_cache.gif",
		buttonImageOnly: true,
		dateFormat: 'dd/mm/yy'
	});
});

UI_flightPaxSt.loadFlightPage = function(){	
	var mydata = null;
	disableControls(true);
	$("#tblFlownFlights").jqGrid({ 
		
		datatype: function(postdata) {			

			postdata['flightsSearchReqDTO.flightNumber']= $('#txtFlightNo').val();
			postdata['flightsSearchReqDTO.departureDate']= $('#txtDept').val();
	
			jQuery.ajax({
	           url: 'showChangePAXandETStatus.action',
	           data:postdata,
	           dataType:"json",
	           complete: function(jsonData,stat){	        	  
	              if(stat=="success") {
	            	 mydata = eval("("+jsonData.responseText+")");
	            	 $("#tblFlownFlights")[0].addJSONData(mydata);      
	            	 top[2].HideProgress();
	              }else if(stat=="error") {
	            	top[2].HideProgress();
	            	showCommonError(responseText['msgType'], responseText['messageTxt']);
				}
	              
	           }
	        });
	    },
		jsonReader : {
			  root: "colFlownFlightDetails", 
			  page: "page",
			  total: "total",
			  records: "records",
			  repeatitems: false,				  
			  id: "0"				  
			},													
	    colNames:['FlightNumber','DepartureDate','Segment'],
        colModel:[
                {name:'flightsResultsResDTO.flightNumber',  index: 'FlightNumber', width:12, align:"center"},
                {name:'flightsResultsResDTO.departureDate',  index: 'DepartureDate', width:12, align:"center"},
                {name:'flightsResultsResDTO.segment',  index: 'Segment', width:12, align:"center"},
                               	
    		],
		imgpath: '../../themes/default/images/jquery',
		multiselect:false,
		pager: $('#divFlownFlightsPager'),
		rowNum:10,						
		viewrecords: true,
		height:180,
		width:920,	
		onSelectRow: function(rowid){
			UI_flightPaxSt.fillForm(rowid);
			disableControls(false);
		},
	   	loadComplete: function (e){	   
	   	},
	   	beforeRequest: function(){
	   	}	 
	}).navGrid("#divFlownFlightsPager",{refresh: false, edit: false, add: false, del: false, search: false});
		
}

UI_flightPaxSt.closeOnClick = function () {
	top[1].objTMenu.tabRemove(screenId);
}

UI_flightPaxSt.fillForm = function(rowid) {
	
	$("#flightNumber").val( jQuery("#tblFlownFlights").getCell(rowid,'flightsResultsResDTO.flightNumber') );
	
	$("#deptDate").val( jQuery("#tblFlownFlights").getCell(rowid,'flightsResultsResDTO.departureDate') );
}

function  disableControls(cond){
	$("#btnUpdateStatus").attr('disabled', cond); 
}
	
UI_flightPaxSt.validateSearch = function () {
	
	var deparDate = formatDate(getVal("txtDept"), "dd/mm/yyyy", "dd-mmm-yyyy");
	var Today = formatDate(getVal("hdnCurrentDate"), "dd/mm/yyyy","dd-mmm-yyyy");
	var flightNo = trim($("#txtFlightNo").val());
		
	if (flightNo == "") {
		showERRMessage(mandatoryflight);	
		getFieldByID("txtFlightNo").focus();
		return false;
	}
	if (getVal("txtDept") == "")	{
		showERRMessage(mandatorydate);	
		getFieldByID("txtDept").focus();		
		return false;	
		
	}else if(!dateChk(getVal("txtDept"))){
		showERRMessage(invaliddate);	
		getFieldByID("txtDept").focus();		
		return false;
	}	
	if (compareDates(Today, deparDate, '<')) {
		showERRMessage(futureDate);	
		getFieldByID("txtDept").focus();
		return false;
	}
	
	return true;
}

UI_flightPaxSt.updateStatusOnClick = function () {
	disableControls(false);
	top[2].ShowProgress();
	var updateOptions = {
			cache : false,
			success : showResponse,
			url : 'showChangePAXandETStatus.action',
			dataType : 'json',
			resetForm : false
	};
	setField("hdnMode","UPDATE");
	  
	$('#frmFlightPaxSt').ajaxSubmit(updateOptions);
	return false;	
}

function showResponse(responseText, statusText)  {
	top[2].HideProgress();
	showCommonError(responseText['msgType'], responseText['messageTxt']);
    if(responseText['msgType'] != "false") {
    	$('#frmFlightPaxSt').clearForm();
    	jQuery("#tblFlownFlights").trigger("reloadGrid");
    	$("#btnUpdateStatus").attr('disabled', false); 
    	alert("PAX & ET Status Successfully Updated");
    	window.location.href = window.location.href;
    }
}
    
UI_flightPaxSt.searchFlightOnClick = function (){
	
    	if(UI_flightPaxSt.validateSearch()) {	
    		top[2].ShowProgress();		
    		var newUrl = 'showChangePAXandETStatus.action?';
    		newUrl += 'page=' + '0';
    	 	newUrl += '&flightsSearchReqDTO.flightNumber=' + $("#txtFlightNo").val();
    	 	newUrl += '&flightsSearchReqDTO.departureDate=' + $("#txtDept").val();
    	 	
    	 	
    	 	$("#tblFlownFlights").trigger("reloadGrid");
    	 	$("#divFlownFlightsPager").clearGridData();
    	 	
    	 	$.getJSON(newUrl, function(jsonData,stat){ 
    	 		top[2].HideProgress();
    	 		var mydata = eval("("+jsonData.responseText+")");
    	 		$("#tblFlownFlights")[0].addJSONData(mydata); 
    	 		if(!jsonData.success)
    	 			showCommonError(jsonData.msgType, jsonData.messageTxt);
    	 	});
    	}
}

