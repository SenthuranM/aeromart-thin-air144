/*
 * Author : K.Srikanth
 */

var strRowData;
var strGridRow;
var intLastRec;

var objCmbCountryTemp = new combo();
var objCmbFormCountryTemp = new combo();

var screenId = "SC_ADMN_023";
var valueSeperator = "~";

function validateStation() {
	if (validateTextField(getValue("txtStationId"), stationIdRqrd)) {
		getFieldByID("txtStationId").focus();
		return;
	}

	if (getValue("txtStationId") != null || getValue("txtStationId") != "") {
		if (getValue("txtStationId").length < 3) {
			showCommonError("Error", stationIdlength);
			getFieldByID("txtStationId").focus();
			return false;

		}
	}

	if (getFieldByID("hdnMode").value == "ADD") {
		for ( var i = 0; i < arrData.length; i++) {
			if (arrData[i][1] == getFieldByID("txtStationId").value) {
				showCommonError("Error", stationIdExist);
				getFieldByID("txtStationId").focus();
				return;
			}
		}
	}

	if (validateTextField(getValue("txtDesc"), stationDescRqrd)) {
		getFieldByID("txtDesc").focus();
		return;
	}

	if (getValue("selTerritory") == "-1") {
		showCommonError("Error", stationtertryRqrd);
		getFieldByID("selTerritory").focus();
		return;
	}

	if (getValue("selCountry") == "-1") {
		showCommonError("Error", stationcntryReqrd);
		getFieldByID("selCountry").focus();
		return;
	}
	if(!(getValue("selCountry") == "-1")){
		var selectedCountry = $('#selCountry').val();
		if(countryStateArry[selectedCountry] != undefined ){
			if($("#selState").val() == ""){
				showCommonError("Error", stateReqrd);
				changeState();
				getFieldByID("selState").focus();
				return;
			}
		}
	}
	return true;
}

// On Page loading make sure that all the page input fields are cleared
function winOnLoad(strMsg, strMsgType) {
	Disable("btnEdit", true);
	Disable("btnDelete", true);

	if (saveSuccess == 1) {
		alert("Record Successfully saved!");
		saveSuccess = 0;
	}
	
	if(!isAllowExtendedNameModificaion){
		setVisible("thresholdTime",false);
		setVisible("thresholdTimeInput",false);
	}

	if (strMsg != null && strMsgType != null)
		showCommonError(strMsgType, strMsg);
	if (top[1].objTMenu.focusTab == screenId) {
		getFieldByID("selCountry1").focus();
	}
	disableInputControls();
	setPageEdited(false);
	if (strCountryCode != null) {
		setField("selTerritory", strCountryCode);
		getFieldByID("selTerritory").selected = strCountryCode;
	}

	Disable("btnEdit", true);
	Disable("btnDelete", true);
	var strTmpSearch = getTabValues(screenId, valueSeperator,
			"strSearchCriteria");
	// if ((top[0].strSearchCriteria != "") && (top[0].strSearchCriteria !=
	// null)) {
	if ((strTmpSearch != "") && (strTmpSearch != null)) {
		var strSearch = strTmpSearch.split(",");
		setField("selCountry1", strSearch[0]);
		getFieldByID("selCountry1").selected = strSearch[0];
	}

	if (arrFormData != null && arrFormData.length > 0) {
		setPageEdited(true);
		strGridRow = getTabValues(screenId, valueSeperator, "strGridRow"); // Get
																			// saved
																			// grid
																			// Row
		setField("txtStationId", arrFormData[0][1]);
		setField("txtDesc", trim(arrFormData[0][2]));

		setField("selTerritory", arrFormData[0][3]);
		getFieldByID("selTerritory").selected = arrFormData[0][3];

		setField("selCountry", arrFormData[0][7]);
		getFieldByID("selCountry").selected = arrFormData[0][7];

		if (arrFormData[0][4] == 1) {
			getFieldByID("radOnline1").checked = true;
		} else {
			getFieldByID("radOnline2").checked = true;
		}

		if (arrFormData[0][5] == "on")
			getFieldByID("chkStatus").checked = true;
		else
			getFieldByID("chkStatus").checked = false;

		setField("txtRemarks", arrFormData[0][6]);

		if (arrFormData[0][8] != null && arrFormData[0][8] != "")
			setField("hdnVersion", arrFormData[0][8]);
		else
			setField("hdnVersion", "");

		enableInputControls();

		if (!isAddMode) {
			Disable("txtStationId", true);
			setField("hdnMode", "EDIT");
		}

		if (isAddMode) {
			setField("hdnMode", "ADD");
		}
		
		setField("txtContact", arrFormData[0][9]);
		setField("txtPhone", arrFormData[0][10]);
		setField("txtFax", arrFormData[0][11]);
		setField("txtThresholdTime",arrFormData[0][14]);
		
		if (trim(strMsg) == "TAIR-70049: Territory is inactive") {
			if (top[1].objTMenu.focusTab == screenId) {
				getFieldByID("selTerritory").focus();
			}
		} else if (trim(strMsg) == "TAIR-70050: Country is inactive") {
			if (top[1].objTMenu.focusTab == screenId) {
				getFieldByID("selCountry").focus();
			}
		} else if (trim(strMsg) == "TAIR-70006: Station Code Already Exists") {
			if (top[1].objTMenu.focusTab == screenId) {
				getFieldByID("txtStationId").focus();
			}
		}
	}

	$('#selCountry').change(function() {
		clickChange();
		changeState();	
	});
	
	$( "#btnSearch" ).click(function() {
		searchCountry()
	});	
	
	
	$("#stateRow").hide();
}

// on Grid Row click
function RowClick(strRowNo) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		populateFromRow(strRowNo);
	}
}

// Populate from Grid Row
function populateFromRow(strRowNo) {

	objOnFocus();
	strRowData = objDG.getRowValue(strRowNo);
	strGridRow = strRowNo;
	setField("txtStationId", arrData[strRowNo][1]);

	var tmp = trim(arrData[strRowNo][2]);
	setField("txtDesc", tmp);

	if (arrData[strRowNo][5] == 'Yes') {
		getFieldByID("radOnline1").checked = true;
	} else {
		getFieldByID("radOnline2").checked = true;
	}

	if (arrData[strRowNo][6] == "Active") {
		getFieldByID("chkStatus").checked = true;
	} else {
		getFieldByID("chkStatus").checked = false;
	}

	setField("txtRemarks", arrData[strRowNo][7]);

	setField("hdnVersion", arrData[strRowNo][8]);

	setField("selTerritory", arrData[strRowNo][9]);
	getFieldByID("selTerritory").selected = arrData[strRowNo][9];
	setField("selCountry", arrData[strRowNo][10]);
	getFieldByID("selCountry").selected = arrData[strRowNo][10];
	setPageEdited(false);
	disableInputControls();
	Disable("btnEdit", false);
	Disable("btnDelete", false);
	
	setField("txtContact",arrData[strRowNo][11]);
	setField("txtPhone",arrData[strRowNo][12]);
	setField("txtFax",arrData[strRowNo][13]);
	setField("txtThresholdTime",arrData[strRowNo][14]);
	if(arrData[strRowNo][15] != undefined && arrData[strRowNo][15] != ''){
		changeState();
		$("#selState").val(arrData[strRowNo][15]);
	} else {
		$('#selState').find('option').remove().end();
		changeState();
	}
	
}

function selectChange(strCountryCode) {

}

function searchCountry() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		objOnFocus();
		setTabValues(screenId, valueSeperator, "strSearchCriteria",
				getValue("selCountry1"));
		// once search button was pressed set the hidden value to "SEARCH"
		document.forms[0].hdnMode.value = "SEARCH";
		setTabValues(screenId, valueSeperator, "intLastRec", 1); // search
		setField("hdnRecNo", getTabValues(screenId, valueSeperator,
				"intLastRec"));
		document.forms[0].submit();
	}
}

function enableInputControls() {
	Disable("txtStationId", false);
	Disable("txtDesc", false);
	Disable("selTerritory", false);
	Disable("selCountry", false);
	Disable("radOnline", false);
	Disable("txtRemarks", false);
	Disable("chkStatus", false);
	Disable("btnSave", false);
	Disable("btnReset", false);
	Disable("radOnline1", false);
	Disable("radOnline2", false);
	Disable("txtContact", false);
	Disable("txtPhone", false);
	Disable("txtFax", false);
	Disable("txtThresholdTime", false);
	Disable("selState", false);
}

function disableInputControls() {
	Disable("txtStationId", true);
	Disable("radOnline", true);
	Disable("txtDesc", true);
	Disable("selTerritory", true);
	Disable("selCountry", true);
	Disable("txtRemarks", true);
	Disable("chkStatus", true);
	Disable("btnSave", true);
	Disable("btnReset", true);
	Disable("radOnline1", true);
	Disable("radOnline2", true);
	Disable("txtContact", true);
	Disable("txtPhone", true);
	Disable("txtFax", true);
	Disable("txtThresholdTime", true);
	Disable("selState", true);
}

function clearStation() {
	clearOnly();
	getFieldByID("txtStationId").focus();
}

function clearOnly() {
	setField("hdnVersion", "");
	setField("txtStationId", "");
	setField("txtDesc", "");
	setField("txtRemarks", "");
	getFieldByID("radOnline1").checked = true;
	getFieldByID("chkStatus").checked = true;
	setField("selTerritory", "-1");
	setField("selCountry", "-1");
	setField("txtContact", "");
	setField("txtPhone", "");
	setField("txtFax", "");
	setField("txtThresholdTime","");
}

function saveStation() {
	objOnFocus();
	if (!validateStation())
		return;

	// setField("hdnRecNo",(top[0].intLastRec));
	var tempName = trim(getText("txtRemarks"));
	tempName = replaceall(tempName, "'", "`");
	tempName = replaceEnter(tempName);
	setField("txtRemarks", tempName);
	setField("hdnRecNo", getTabValues(screenId, valueSeperator, "intLastRec"));
	setTabValues(screenId, valueSeperator, "strGridRow", strGridRow); // Save
																		// the
																		// grid
																		// Row
	Disable("txtStationId", false);

	setTabValues(screenId, valueSeperator, "intLastRec", 1); // search
	setField("hdnRecNo", getTabValues(screenId, valueSeperator, "intLastRec"));
	document.forms[0].submit();
}

function resetStation() {
	objOnFocus();
	var strMode = getText("hdnMode");
	if (strMode == "ADD") {
		clearStation();
		getFieldByID("txtStationId").focus();
	}
	if (strMode == "EDIT") {
		if (strGridRow < arrData.length
				&& arrData[strGridRow][1] == getText("txtStationId")) {
			// top.parent.pageEdited = false;
			setPageEdited(false);
			// RowClick(strGridRow);
			populateFromRow(strGridRow);
			enableInputControls();
			Disable("txtStationId", true);
			Disable("btnDelete", true);
			getFieldByID("txtDesc").focus();
			objOnFocus();
		} else {
			clearOnly();
			disableInputControls();
			Disable("btnEdit", true);
			Disable("btnDelete", true);
			// getFieldByID("btnAdd").focus();
			buttonSetFocus("btnAdd");
		}
	}
	setPageEdited(false);
	$("#stateRow").hide();
	// objOnFocus();
}

// on Add Button click
function addClick() {

	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		objOnFocus();
		setPageEdited(false);
		Disable("txtStationId", false);
		clearStation();
		enableInputControls();
		setField("hdnMode", "ADD");
		getFieldByID("txtStationId").focus();
		getFieldByID("chkStatus").checked = "true";
		Disable("btnEdit", true);
		Disable("btnDelete", true);
		$("#stateRow").hide();
	}
}

// on Edit Button CLick
function editClick() {

	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		objOnFocus();
		setPageEdited(false);
		enableInputControls();
		setField("hdnMode", "EDIT");
		Disable("txtStationId", true);
		Disable("btnDelete", true);
		getFieldByID("txtDesc").focus();
	}
}

// on Delete Button click
function deleteClick() {
	objOnFocus();
	if (getFieldByID("txtStationId").value == ""
			|| getFieldByID("txtStationId").value == null) {
		showCommonError("Error", stationrowRqrd);
	} else {
		var confirmStr = confirm(deleteConf)
		if (!confirmStr)
			return;
		enableInputControls();
		setField("txtStationId", strRowData[0]);
		setField("hdnMode", "DELETE");
		setTabValues(screenId, valueSeperator, "intLastRec", 1); // Delete
		document.forms[0].submit();
		disableInputControls();
	}
	$("#stateRow").hide();
}

function CCPress() {
	var strCC = getText("txtStationId");
	var strLen = strCC.length;
	var blnVal = isAlphaNumeric(strCC);
	if (!blnVal) {
		setField("txtStationId", strCC.substr(0, strLen - 1));
		getFieldByID("txtStationId").focus();
	}
}

function CDPress() {
	var strCD = getText("txtDesc");
	var strLen = strCD.length;
	var blnVal = isAlphaNumeric(strCD);
	if (!blnVal) {
		setField("txtDesc", strCD.substr(0, strLen - 1));
		getFieldByID("txtDesc").focus();
	}
}

function Save(msg) {
	top[2].objMsg.MessageText = msg;
	top[2].objMsg.MessageType = "Confirmation";
	top[2].ShowPageMessage();
}

function Delete() {
	top[2].objMsg.MessageText = "Selected record will get deleted.";
	top[2].objMsg.MessageType = "Warning";
	top[2].ShowPageMessage();
}

function myRecFunction(intRecNo, intErrMsg) {
	top[2].HidePageMessage();
	if (intErrMsg != "") {
		top[2].objMsg.MessageText = intErrMsg;
		top[2].objMsg.MessageType = "Error";
		top[2].ShowPageMessage();
	}
}

// To Handle Paging
function gridNavigations(intRecNo, intErrMsg) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		objOnFocus();
		setField("hdnMode", "");

		if (intErrMsg != "") {
			top[2].objMsg.MessageText = "<li> " + intErrMsg;
			top[2].objMsg.MessageType = "Error";
			top[2].ShowPageMessage();

		} else {
			if (intRecNo <= 0)
				intRecNo = 1;
			setTabValues(screenId, valueSeperator, "intLastRec", intRecNo);
			setField("hdnRecNo", getTabValues(screenId, valueSeperator,
					"intLastRec"));
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	}
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
	// top.pageEdited=isEdited;
}

function fillCombos() {
	objCmbCountryTemp.id = "selCountryTemp";
	objCmbCountryTemp.name = "selCountryTemp";
	if (browser.isIE) {
		objCmbCountryTemp.top = "73";
	} else {
		objCmbCountryTemp.top = "68";
	}

	objCmbCountryTemp.left = "225";
	objCmbCountryTemp.width = "175";
	objCmbCountryTemp.height = "50";
	objCmbCountryTemp.textIndex = 1;
	objCmbCountryTemp.onChange = "pageOnChange";
	objCmbCountryTemp.arrData = arrCountry; // 2D Data Array

	objCmbFormCountryTemp.id = "selFormCountryTemp";
	objCmbFormCountryTemp.name = "selFormCountryTemp";
	if (browser.isIE) {
		objCmbFormCountryTemp.top = "504";
	} else {
		objCmbFormCountryTemp.top = "494";
	}

	objCmbFormCountryTemp.left = "276";
	objCmbFormCountryTemp.width = "175";
	objCmbFormCountryTemp.height = "50";
	objCmbFormCountryTemp.textIndex = 1;
	objCmbFormCountryTemp.onChange = "pageOnChange";
	objCmbFormCountryTemp.arrData = arrCountry; // 2D Data Array
	objCmbFormCountryTemp.buildCombo();
}

function pageOnChange() {
	top[2].HidePageMessage();
	setPageEdited(true);
}

function closeStation() {
	top[1].objTMenu.tabRemove(screenId);
}

function clickChange() {
	objOnFocus();
	setPageEdited(true);
}

function valContact(objTextBox) {
	setPageEdited(true, false);
	objOnFocus();
	var strCC = objTextBox.value;
	var strLen = strCC.length;
	var blnVal = isEmpty(strCC);
	if (blnVal) {
		setField("txtContact", strCC.substr(0, strLen - 1));
		getFieldByID("txtContact").focus();
	} else
		alphaNumericValidate(objTextBox);
}

function changeState(){
	var showState = false;
	$('#selState').find('option').remove().end();
	if($('#selCountry').val() != undefined && $('#selCountry').val() != ""){
		var selectedCountry = $('#selCountry').val();
		if(countryStateArry[selectedCountry] != undefined ){
			showState = true;
			$( 'select[name="selState"]' ).append('<option value=""><option/>');
			$( 'select[name="selState"]' ).append(countryStateArry[selectedCountry]);
		}
	}
	if(showState){
		$("#stateRow").show();
	} else {
		$("#stateRow").hide();
	}		
}
