var screenId = "SC_ADMN_023";
var valueSeperator = "~";

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "15%";
objCol1.arrayIndex = 1;
// objCol1.toolTip = "Station Id" ;
objCol1.headerText = "Station Code";
objCol1.itemAlign = "left"
objCol1.sort = "true";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "24%";
objCol2.arrayIndex = 2;
// objCol2.toolTip = "Station Name" ;
objCol2.headerText = "Station Name";
objCol2.itemAlign = "left"

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "16%";
objCol3.arrayIndex = 3;
// objCol3.toolTip = "Territory" ;
objCol3.headerText = "Territory";
objCol3.itemAlign = "left"

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "16%";
objCol4.arrayIndex = 4;
// objCol4.toolTip = "Country" ;
objCol4.headerText = "Country";
objCol4.itemAlign = "left"

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "10%";
objCol5.arrayIndex = 5;
// objCol5.toolTip = "Online" ;
objCol5.headerText = "Online";
objCol5.itemAlign = "left"

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "30%";
objCol6.arrayIndex = 6;
// objCol6.toolTip = "Status" ;
objCol6.headerText = "Status";
objCol6.itemAlign = "Left"

// ---------------- Grid
var objDG = new DataGrid("spnStations");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol6);

if (getTabValues(screenId, valueSeperator, "intLastRec") == "") {
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
}

objDG.width = "99%";
objDG.height = "140px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = arrData;
objDG.seqNo = true;
objDG.seqStartNo = getTabValues(screenId, valueSeperator, "intLastRec"); // Grid
objDG.pgnumRecTotal = totalRecords; // remove as per return record size
objDG.paging = true
objDG.pgnumRecPage = 20;
objDG.pgonClick = "gridNavigations";
objDG.rowClick = "RowClick";
objDG.displayGrid();
