var screenId = "SC_ADMN_046";
var valueSeperator = "~";
if (getTabValues(screenId, valueSeperator, "intLastRec") == "") {
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
}

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "14%";
objCol1.arrayIndex = 2;
objCol1.headerText = "Agent Code";
objCol1.itemAlign = "left";
objCol1.sort = "true";

// ---------------- Grid
var objDG = new DataGrid("spnAgents");
objDG.addColumn(objCol1);

objDG.width = "25%";
objDG.height = "375px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = arrData;
objDG.seqNo = true;
objDG.seqStartNo = getTabValues(screenId, valueSeperator, "intLastRec"); // Grid
objDG.pgnumRecTotal = totalRecords; // remove as per return record size
objDG.paging = true
objDG.pgnumRecPage = 20;
objDG.pgonClick = "gridNavigations";
objDG.rowClick = "RowClick";
objDG.displayGrid();
objDG.shortCutKeyFunction = "Body_onKeyDown";
