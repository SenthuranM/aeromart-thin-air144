var strRowData;
var strGridRow;
var intLastRec;

var agentCode;

var screenId = "SC_ADMN_046";
var valueSeperator = "~";


var agentCodesAddArray = [];
var agentCodesDeleteArray = [];
var agentCodesPreviousArray = [];
var agentCodesCurrentArray = [];

function validateAdminFeeAgent() {
	return true;
}

// On Page loading make sure that all the page input fields are cleared
function winOnLoad(strMsg, strMsgType) {
	// Menaka
	if (saveSuccess == 1) {
		alert("Record Successfully saved!");
		saveSuccess = 0;
	}
	// Keep the search variables remain same
	if (isSearchMode) {
		document.forms[0].hdnUIMode.value = "SEARCH";
	}
	// Menaka
	if (strMsg != null && strMsgType != null)
		showCommonError(strMsgType, strMsg);

	enableInputControls(true);
	setPageEdited(false);

	if (arrFormData != null && arrFormData.length > 0) {
		strGridRow = getTabValues(screenId, valueSeperator, "strGridRow"); // Get
																			// saved
																			// grid
																			// Row
		setPageEdited(true);
		enableInputControls(false);

	}
}

function selectChange(strControl, strCountryCode) {
	setField(strControl, strCountryCode);
}

// on Grid Row click
function RowClick(strRowNo) {
	objOnFocus();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;
		setField("hdnGridRow", strRowNo);
		
		agentCode = arrData[strRowNo][2];
		
		setPageEdited(false);
		enableInputControls(true);
	}
}

function searchAgent() {
	objOnFocus();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		enableInputControls(true);
		document.forms[0].hdnUIMode.value = "SEARCH";
		document.forms[0].hdnMode.value = "";
		document.forms[0].submit();
	}
	clearAdminFeeAgent();
}

function enableInputControls(blnStatus) {
	
}

function clearAdminFeeAgent() {
	setField("txtAdminFeeAgent", "");
}

function Save(msg) {
	top[2].objMsg.MessageText = msg;
	top[2].objMsg.MessageType = "Confirmation";
	top[2].ShowPageMessage();
}

function Delete() {
	top[2].objMsg.MessageText = "Selected record will get deleted.";
	top[2].objMsg.MessageType = "Warning";
	top[2].ShowPageMessage();
}

function myRecFunction(intRecNo, intErrMsg) {
	top[2].HidePageMessage();
	if (intErrMsg != "") {
		top[2].objMsg.MessageText = intErrMsg;
		top[2].objMsg.MessageType = "Error";
		top[2].ShowPageMessage();
	}
}

// To Handle Paging
function gridNavigations(intRecNo, intErrMsg) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setField("hdnMode", "PAGING");
		objOnFocus();
		// top[0].intLastRec=intRecNo;
		if (intRecNo <= 0)
			intRecNo = 1;
		setTabValues(screenId, valueSeperator, "intLastRec", intRecNo);
		// setField("hdnRecNo",(top[0].intLastRec));
		setField("hdnRecNo", getTabValues(screenId, valueSeperator,
				"intLastRec"));
		if (intErrMsg != "") {
			top[2].objMsg.MessageText = "<li> " + intErrMsg;
			top[2].objMsg.MessageType = "Error";
			top[2].ShowPageMessage();
		} else {
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	}
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function pageOnChange() {
	top[1].objTMenu.tabPageEdited(screenId, true);

}
function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function valAddress(objTextBox) {
	setPageEdited(true);
	objOnFocus();
	customValidate(objTextBox, /[\s,]/g);
}
function valLocation(objTextBox) {
	setPageEdited(true);
	objOnFocus();
	customValidate(objTextBox, /[,]/g);
}

function addToList() {
	var isContained = false;
	top[2].HidePageMessage();

	
	if (agentCode == null || agentCode == "") {
		//showCommonError("Error", depatureRequired);

	} else {
		var str = "";
		str = agentCode;

		var control = document.getElementById("selAgents");
		for ( var r = 0; r < control.length; r++) {
			if (control.options[r].text == str) {
				isContained = true;
				//showERRMessage(OnDExists);
				break;
			}
		}
		if (isContained == false) {
			control.options[control.length] = new Option(str, str);
		}
	}
}

function removeFromList() {
	var control = document.getElementById("selAgents");

	var selIndex = control.selectedIndex;
	if (selIndex != -1) {
		for (i = control.length - 1; i >= 0; i--) {
			if (control.options[i].selected) {
				control.options[i] = null;
			}
		}
		if (control.length > 0) {
			control.selectedIndex = selIndex == 0 ? 0 : selIndex - 1;
		}
	}

}

function saveAgent() {
	
	agentCodesAddArray = [];
	agentCodesDeleteArray = [];
	agentCodesPreviousArray = [];
	agentCodesCurrentArray = [];
	
	var strAgentCodes="";
	if(getFieldByID("selAgents")){
		var intCount = getFieldByID("selAgents").length ;
		 for (var i = 0; i < intCount; i++){
		 	//if (getFieldByID("selAgents").options[i].selected){
		 		//strAgentCodes+=getFieldByID("selAgents").options[i].value+",";
		 		agentCodesCurrentArray[i] = getFieldByID("selAgents").options[i].value;
		   	//}
		 }
	}	
	
	if(getFieldByID("sel_Admin_Fee_Agent_Initial")){
		var intCount = getFieldByID("sel_Admin_Fee_Agent_Initial").length ;
		 for (var i = 0; i < intCount; i++){
		 	//if (getFieldByID("sel_Admin_Fee_Agent_Initial").options[i].selected){
		 		agentCodesPreviousArray[i] = getFieldByID("sel_Admin_Fee_Agent_Initial").options[i].value;
		   	//}
		 }
	}
	
	for (var i=0;i<agentCodesPreviousArray.length;i++){
		var agentAvailableInCurrentArray = false;
		for (var j=0;j<agentCodesCurrentArray.length;j++){
			if (agentCodesPreviousArray[i] == agentCodesCurrentArray[j]){
				agentAvailableInCurrentArray = true;
				break;
			}
		}
		if (!agentAvailableInCurrentArray){
			agentCodesDeleteArray[agentCodesDeleteArray.length] = agentCodesPreviousArray[i];
		}
	}
	
	for (var i=0;i<agentCodesCurrentArray.length;i++){
		var agentAvailableInPreviousArray = false;
		for (var j=0;j<agentCodesPreviousArray.length;j++){
			if (agentCodesCurrentArray[i] == agentCodesPreviousArray[j]){
				agentAvailableInPreviousArray = true;
				break;
			}
		}
		if (!agentAvailableInPreviousArray){
			agentCodesAddArray[agentCodesAddArray.length] = agentCodesCurrentArray[i];
		}
	}
	
	var strAddAgentCodes = "";
	var strDeleteAgentCodes = "";
	
	for (var i = 0; i < agentCodesAddArray.length; i++){
		strAddAgentCodes+=agentCodesAddArray[i]+",";
	}
	
	for (var i = 0; i < agentCodesDeleteArray.length; i++){
		strDeleteAgentCodes+=agentCodesDeleteArray[i]+",";
	}

	setField("hdnAgentCodes", strAgentCodes);
	setField("hdnAddAgentCodes", strAddAgentCodes);
	setField("hdnDeleteAgentCodes", strDeleteAgentCodes);
	setField("hdnMode", "SAVE");
	clearAdminFeeAgent();
	document.forms[0].submit();
	setPageEdited(false);

}