var screenId = "SC_ADMN_006";
var valueSeperator = "~";
if (getTabValues(screenId, valueSeperator, "intLastRec") == "") {
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
}

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "15%";
objCol1.arrayIndex = 1;
// objCol1.toolTip = "Country ID" ;
objCol1.headerText = "Country ID";
objCol1.itemAlign = "left"
objCol1.sort = "true";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "47%";
objCol2.arrayIndex = 2;
// objCol2.toolTip = "Country Description" ;
objCol2.headerText = "Country Description";
objCol2.itemAlign = "left"
objCol2.sort = "true";

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "9%";
objCol3.arrayIndex = 3;
// objCol3.toolTip = "Status" ;
objCol3.headerText = "Status";
objCol3.itemAlign = "left"

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "8%";
objCol4.arrayIndex = 6;
// objCol4.toolTip = "Currency" ;
objCol4.headerText = "Currency";
objCol4.itemAlign = "left"

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "8%";
objCol5.arrayIndex = 7;
// objCol5.toolTip = "Region" ;
objCol5.headerText = "Region";
objCol5.itemAlign = "left"	

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "10%";
objCol6.arrayIndex = 8;
// objCol6.toolTip = "Allow On-Hold" ;
objCol6.headerText = "Allow On-Hold";
objCol6.itemAlign = "left";

// ---------------- Grid
var objDG = new DataGrid("spnCountries");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol6);
objDG.addColumn(objCol3);

objDG.width = "99%";
objDG.height = "180px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = arrData;
objDG.seqNo = true;
objDG.seqStartNo = getTabValues(screenId, valueSeperator, "intLastRec"); // Grid
objDG.pgnumRecTotal = totalRecords; // remove as per return record size
objDG.paging = true
objDG.pgnumRecPage = 20;
objDG.pgonClick = "gridNavigations";
objDG.rowClick = "RowClick";
objDG.displayGrid();
