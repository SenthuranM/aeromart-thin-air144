/*
 * Author : K.Srikanth
 */

var strRowData;
var strGridRow;
var intLastRec;
var screenId = "SC_ADMN_006";
var valueSeperator = "~";
var objCmb1 = new combo();

function validateCountry() {

	if (getFieldByID("hdnMode").value == "ADD") {
		for ( var i = 0; i < arrData.length; i++) {
			if (arrData[i][1] == getFieldByID("txtCountryId").value) {
				showCommonError("Error", countryIdExist);
				getFieldByID("txtCountryId").focus();
				return;
			}
		}
	}

	if (validateTextField(getValue("txtCountryId"), countryIdRqrd)) {
		getFieldByID("txtCountryId").focus();
		return;
	}

	if (getFieldByID("hdnMode").value == "ADD") {
		if (getValue("txtCountryId") != null) {
			if (getValue("txtCountryId").length < 2) {
				showCommonError("Error", countryIdLenViolates);
				getFieldByID("txtCountryId").focus();
				return;
			}
		}
	}

	if (validateTextField(getValue("txtCountryDes"), countryDescRqrd)) {
		getFieldByID("txtCountryDes").focus();
		return;
	}

	if (getFieldByID("hdnMode").value == "ADD") {
		if (getValue("txtCountryDes") != null
				&& getValue("txtCountryDes").length > 0) {
			for ( var i = 0; i < arrData.length; i++) {
				if (getValue("txtCountryDes").toUpperCase() == arrData[i][2]
						.toUpperCase()) {
					showCommonError("Error", countryDescExist);
					getFieldByID("txtCountryDes").focus();
					return;
				}
			}
		}
	}

	if (getFieldByID("selCurrencyCode").value == "-1") {
		showCommonError("Error", currencyCodeRqrd);
		getFieldByID("selCurrencyCode").focus();
		return;
	}
	
	if (getFieldByID("selRegionCode").value == "-1") {
		showCommonError("Error", regionCodeRqrd);
		getFieldByID("selRegionCode").focus();
		return;
	}

    if (validateTextField(getValue("txtIsoCodeAlpha3"), isoAlpha3CodeRqrd)) {
        getFieldByID("txtIsoCodeAlpha3").focus();
        return;
    }

	return true;
}

// On Page loading make sure that all the page input fields are cleared
function winOnLoad(strMsg, strMsgType) {
	if (isBSPEnabledForAirLine){
		document.getElementById("BSPEnableRw").style.display = '';
	} else {
		document.getElementById("BSPEnableRw").style.display = 'none';
	}
	clearCountry();
	disableInputControls();
	setPageEdited(false);

	Disable("btnEdit", "true");
	Disable("btnDelete", "true");

	if (strMsg != null && strMsgType != null)
		showCommonError(strMsgType, strMsg);
	// var arrFormData= new Array();

	if (top[1].objTMenu.focusTab == screenId) {
		buttonSetFocus("btnAdd");
	}
	
	var strTmpSearch = searchData;
	if (strTmpSearch != "" && strTmpSearch != null) { // sets the search data
		var strSearch = strTmpSearch.split(",");
		objCmb1.setComboValue(strSearch[0]);
	}

	if (arrFormData != null && arrFormData.length > 0) {
		setPageEdited(true);
		strGridRow = getTabValues(screenId, valueSeperator, "strGridRow"); // Get
																			// saved
																			// grid
																			// Row
		setField("txtCountryId", arrFormData[0][1]);
		setField("txtCountryDes", arrFormData[0][2]);
		setField("txtRemarks", arrFormData[0][3]);

		Disable("btnSave", true);
		Disable("btnReset", true);

		if (arrFormData[0][4] == "Active" || arrFormData[0][4] == "active"
				|| arrFormData[0][4] == "on") {
			getFieldByID("chkStatus").checked = true;
			Disable("btnSave", false);
			Disable("btnReset", false);
		} else {
			getFieldByID("chkStatus").checked = false;
			Disable("btnSave", false);
			Disable("btnReset", false);
		}

		setField("selCurrencyCode", arrFormData[0][5]);
		setField("selRegionCode", arrFormData[0][7]);
		setField("txtIsoCodeAlpha3", arrFormData[0][11]);

		enableInputControls();

		if (!isAddMode) {
			Disable("txtCountryId", true);
			setField("hdnMode", "EDIT");
			setField("hdnVersion", arrFormData[0][6]);
			if (top[1].objTMenu.focusTab == screenId) {
				getFieldByID("txtCountryDes").focus();
			}
		} else {
			Disable("txtCountryId", false);
			setField("hdnMode", "ADD");
			if (top[1].objTMenu.focusTab == screenId) {
				getFieldByID("txtCountryId").focus();
			}
		}
	} else {
		if (isSuccessfulSaveMessageDisplayEnabled) {
			if (isSaveTransactionSuccessful) {
				alert("Record Successfully saved!");
			}
		}
	}

}

function disableInputControls() {
	Disable("txtCountryId", true);
	Disable("txtCountryDes", true);
	Disable("txtRemarks", true);
	Disable("selCurrencyCode", true);
	Disable("selRegionCode", true);
	Disable("btnSave", true);
	Disable("btnReset", true);
	Disable("chkOhdEnabled", true);
	Disable("chkStatus", true);
	Disable("chkBSPEnabled",true);
	Disable("txtIsoCodeAlpha3",true);
}

/*
 * Function call for SEARCH Button Click
 */
function searchCountry() {
	objOnFocus();
	var tmpSearchData;
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setField("hdnMode", "SEARCH");
		if(objCmb1.getText() != ""){ // this is for refresh the selected value
		 	tmpSearchData = objCmb1.getComboValue();
		} else {
			tmpSearchData = objCmb1.getText();
		}		
		setField("hdnRecNo", 1);
		setField("hdnSearchData", tmpSearchData);
		document.forms[0].submit();
		top[2].ShowProgress();
	}

}


// on Grid Row click
function RowClick(strRowNo) {
	objOnFocus();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;

		setField("txtCountryId", arrData[strRowNo][1]);
		setField("txtCountryDes", arrData[strRowNo][2]);
		setField("txtRemarks", arrData[strRowNo][4]);
		setField("hdnVersion", arrData[strRowNo][5]);

		if (arrData[strRowNo][3] == "Active"
				|| arrData[strRowNo][3] == "active")
			getFieldByID("chkStatus").checked = true;
		else
			getFieldByID("chkStatus").checked = false;

		if (arrData[strRowNo][6] == "") {
			setField("selCurrencyCode", "-1");
		} else {
			setField("selCurrencyCode", arrData[strRowNo][6]);
		}
		
		if (arrData[strRowNo][7] == "") {
			setField("selRegionCode", "-1");
		} else {
			setField("selRegionCode", arrData[strRowNo][7]);
		}
		
		if(arrData[strRowNo][9] == "Y" ||
				arrData[strRowNo][9] == "y"){
			getFieldByID("chkOhdEnabled").checked = true;			
		} else {
			getFieldByID("chkOhdEnabled").checked = false;
		}
		
		if(arrData[strRowNo][10] == "Y" ||
				arrData[strRowNo][10] == "y") {
			getFieldByID("chkBSPEnabled").checked = true;
		} else {
			getFieldByID("chkBSPEnabled").checked = false;
		}

        setField("txtIsoCodeAlpha3", arrData[strRowNo][11]);

        setPageEdited(false);
		disableInputControls();

		Disable("btnSave", true);
		Disable("btnReset", true);

		Disable("btnEdit", false);
		Disable("btnDelete", false);
	}
}

function enableInputControls() {

	Disable("txtCountryId", false);
	Disable("txtCountryDes", false);
	Disable("txtRemarks", false);
	Disable("selCurrencyCode", false);
	Disable("selRegionCode", false);
	Disable("btnSave", false);
	Disable("btnReset", false);
	Disable("chkOhdEnabled", false);
	Disable("chkStatus", false);
	Disable("chkBSPEnabled", false);
	Disable("txtIsoCodeAlpha3", false);
}

function clearCountry() {
	setField("hdnVersion", "");
	setField("txtCountryId", "");
	setField("txtCountryDes", "");
	setField("selCurrencyCode", "-1");
	setField("selRegionCode", "-1");
	setField("txtRemarks", "");
	getFieldByID("chkStatus").checked = false;
    getFieldByID("chkOhdEnabled").checked = false;
    getFieldByID("chkBSPEnabled").checked = false;
    setField("txtIsoCodeAlpha3", "");
}

function saveCountry() {
	enableInputControls();
	if (!validateCountry())
		return;

	// setField("hdnRecNo",(top[0].intLastRec));
	var tempName = trim(getText("txtRemarks"));
	tempName = replaceall(tempName, "'", "`");
	tempName = replaceEnter(tempName);
	setField("txtRemarks", tempName);
	setField("hdnRecNo", getTabValues(screenId, valueSeperator, "intLastRec"));
	Disable("txtCountryId", false);
	setTabValues(screenId, valueSeperator, "strGridRow", strGridRow); // Save
																		// the
																		// grid
																		// Row
	document.forms[0].submit();
}

function resetCountry() {
	var strMode = getText("hdnMode");
	if (strMode == "ADD") {
		clearCountry();
		getFieldByID("txtCountryId").focus();

	}
	if (strMode == "EDIT") {
		if (strGridRow < arrData.length
				&& arrData[strGridRow][1] == getText("txtCountryId")) {
			getFieldByID("txtCountryDes").focus();
			setField("txtCountryId", arrData[strGridRow][1]);
			setField("txtCountryDes", arrData[strGridRow][2]);
			setField("txtRemarks", arrData[strGridRow][4]);

			if (arrData[strGridRow][6] == "") {
				setField("selCurrencyCode", "-1");
			} else {
				setField("selCurrencyCode", arrData[strGridRow][6]);
			}
			
			if (arrData[strGridRow][7] == "") {
				setField("selRegionCode", "-1");
			} else {
				setField("selRegionCode", arrData[strGridRow][7]);
			}

			if (arrData[strGridRow][3] == "Active"
					|| arrData[strGridRow][3] == "active")
				getFieldByID("chkStatus").checked = true;
			else
				getFieldByID("chkStatus").checked = false;
			setField("hdnVersion", arrData[strGridRow][5]);

			if(arrData[strGridRow][9] == "Y" ||
					arrData[strGridRow][9] == "y"){
				getFieldByID("chkOhdEnabled").checked = true;			
			} else {
				getFieldByID("chkOhdEnabled").checked = false;
			}
			
			if(arrData[strGridRow][10] == "Y" ||
					arrData[strGridRow][10] == "y"){
				getFieldByID("chkBSPEnabled").checked = true;			
			} else {
				getFieldByID("chkBSPEnabled").checked = false;
			}

            setField("txtIsoCodeAlpha3", arrData[strGridRow][11]);
        } else {
			// Grid row not found. Clear it off.
			clearCountry();
			disableInputControls();
			Disable("btnEdit", "true");
			Disable("btnDelete", "true");
			// getFieldByID("btnAdd").focus();
			buttonSetFocus("btnAdd");
		}
	}
	objOnFocus();
	setPageEdited(false);
}

function updateCountry() {
	if (!validateCountry())
		return;
	document.forms[0].submit();
}

// on Add Button click
function addClick() {

	objOnFocus();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		clearCountry();
		setPageEdited(true);
		enableInputControls();
		setField("hdnMode", "ADD");
		getFieldByID("chkStatus").checked = true;
		getFieldByID("txtCountryId").focus();
		Disable("btnEdit", "true");
		Disable("btnDelete", "true");
	}
}

// on Edit Button CLick
function editClick() {
	objOnFocus();
	if (getFieldByID("txtCountryId").value == ""
			|| getFieldByID("txtCountryId").value == null) {
		showCommonError("Error", "Please select row to delete!");
		Disable("btnDelete", "true");
	} else {
		Disable("btnDelete", "true");
		enableInputControls();
		setPageEdited(true);
		setField("hdnMode", "EDIT");
		Disable("txtCountryId", true);
		getFieldByID("txtCountryDes").focus();
	}
}

// on Delete Button click
function deleteClick() {
	objOnFocus();
	if (getFieldByID("txtCountryId").value == ""
			|| getFieldByID("txtCountryId").value == null) {
		showCommonError("Error", "Please select row to delete!");
	} else {

		var confirmStr = confirm(deleteRecoredCfrm)
		if (!confirmStr)
			return;

		enableInputControls();
		setField("txtCountryId", strRowData[0]);
		// setField("hdnRecNo",(top[0].intLastRec));
		setField("hdnMode", "DELETE");
		setTabValues(screenId, valueSeperator, "intLastRec", 1); // Delete
		setField("hdnRecNo", getTabValues(screenId, valueSeperator,
				"intLastRec"));
		document.forms[0].submit();
		setPageEdited(false);
		disableInputControls();
	}
}

// to show error messages
function RecMsgFunction(intRecNo, intErrMsg) {
	top[2].HidePageMessage();
	if (intErrMsg != "") {
		top[2].objMsg.MessageText = intErrMsg;
		top[2].objMsg.MessageType = "Error";
		top[2].ShowPageMessage();
	}
}

// To Handle Paging
function gridNavigations(intRecNo, intErrMsg) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setField("hdnMode", "PAGING");
		objOnFocus();
		if (intRecNo <= 0)
			intRecNo = 1;
		// top[0].intLastRec=intRecNo;
		setTabValues(screenId, valueSeperator, "intLastRec", intRecNo);
		// setField("hdnRecNo",(top[0].intLastRec));
		setField("hdnRecNo", getTabValues(screenId, valueSeperator,
				"intLastRec"));
		if (intErrMsg != "") {
			top[2].objMsg.MessageText = intErrMsg;
			top[2].objMsg.MessageType = "Error";
			top[2].ShowPageMessage();
		} else {
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	}
}

function validateTA(objControl, length) {
	var strValue = objControl.value;
	var strLength = strValue.length;
	var blnVal = isEmpty(strValue);
	if (blnVal) {
		setField("txtRemarks", strValue.substr(0, strLength - 1));
		getFieldByID("txtRemarks").focus();
	}
	if (strLength > length) {
		strValue = strValue.substr(0, length);
		objControl.value = strValue;
	}
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

buildCombos(arrCountry);

function buildCombos(arrName) {	
	var begin = [["ALL", "ALL"]];	
	objCmb1.id = "selCountryDesc";
	objCmb1.top = "26";
	objCmb1.left = "230";
	objCmb1.width = "250";
	objCmb1.textIndex = 1;
	objCmb1.valueIndex = 0;
	objCmb1.tabIndex = 1;
	objCmb1.selectedText = "ALL";
	objCmb1.selectedValue = "ALL";
	objCmb1.arrData = begin.concat(arrName); // add "ALL" element begin of the list	
	objCmb1.onChange = "objOnFocus";
	objCmb1.buildCombo();
}