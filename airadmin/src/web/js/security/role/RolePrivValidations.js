/*
 * Author : Thushara
 */

var strRowData;
var strGridRow;
var blnCallFunc = false;
var screenId = "SC_ADMN_001";
var valueSeperator = "~";

/*
 * On Load Function calls when loading
 */
function winOnLoad(strMsg, strMsgType) {

	clearControls();
	Disable("txtRoleId");
	disableInputControls();
	setField("hdnModel", "DISPLAY");

	if (strMsg != null && strMsgType != null)
		showCommonAlert(strMsgType, strMsg);

	setField("hdnMode", "DISPLAY");
	setField("txtRoleNameSearch", getTabValues(screenId, valueSeperator,
			"strSearchCriteria"));

	if (top[1].objTMenu.focusTab == screenId) {
		getFieldByID("txtRoleNameSearch").focus();
	}
	ls.blnClientValidate = true;
	ls.onClickEvents = "1";
	ls.ancestorArray = dependancy;

	if (arrFormData != null && arrFormData.length > 0) { // if error occured
		strGridRow = getTabValues(screenId, valueSeperator, "strGridRow"); // Get
																			// saved
																			// grid
																			// Row
		setPageEdited(true);
		setField("txtRoleId", arrFormData[0][1]);
		setField("txtRoleName", arrFormData[0][2]);
		setField("txtRemarks", arrFormData[0][3]);

		if (arrFormData[0][4] == "Active" || arrFormData[0][4] == "ACTIVE"
				|| arrFormData[0][4] == "on")
			getFieldByID("chkStatus").checked = true;
		else
			getFieldByID("chkStatus").checked = false;

		setField("hdnVersion", arrFormData[0][6]);
		ls.selectedData(arrFormData[0][7]);

		setSelectedAgentType(arrFormData[0][8].split(","));

		setField("hdnNoAgent",arrFormData[0][8].split(","));
		
		setServiceChannel(arrFormData[0][10]);
		
		enableInputControls();

		if (!isAddMode || !isCopyMode) {
			Disable("txtRoleId", true);
		}

		if (isAddMode) { // when adding a record
			setField("hdnMode", "ADD");
			setField("hdnAction", "ADD");

			Disable("txtRoleName", "")
			if (top[1].objTMenu.focusTab == screenId) {
				getFieldByID("txtRoleName").focus();
			}
			Disable("txtRemarks", "");
			Disable("chkStatus", "");
			ls.disable(false);
			setField("hdnMode", "ADD");
			setField("hdnAction", "ADD");
			Disable("btnSave", false);
			Disable("btnReset", "");
			Disable("btnDelete", true);
			blnCallFunc = true;
			Disable("btnSave", false);
			Disable("btnReset", false);

		} else if (!isCopyMode) {// copying an exxisting record
			setField("hdnMode", "COPY");
			setField("hdnAction", "COPY");

			Disable("txtRoleName", "")
			if (top[1].objTMenu.focusTab == screenId) {
				getFieldByID("txtRoleName").focus();
			}
			Disable("txtRemarks", "");
			Disable("chkStatus", "");
			ls.disable(false);
			setField("hdnMode", "COPY");
			setField("hdnAction", "COPY");
			Disable("btnSave", false);
			Disable("btnReset", "");
			Disable("btnDelete", true);
			blnCallFunc = true;
			Disable("btnSave", false);
			Disable("btnReset", false);

		} else { // editing an exxisting record
			setField("hdnMode", "EDIT");
			setField("hdnAction", "EDIT");

			Disable("txtRoleName", "")
			if (top[1].objTMenu.focusTab == screenId) {
				getFieldByID("txtRoleName").focus();
			}
			Disable("txtRemarks", "");
			Disable("chkStatus", "");
			ls.disable(false);
			setField("hdnMode", "EDIT");
			setField("hdnAction", "EDIT");
			Disable("btnSave", false);
			Disable("btnDelete", true);
			blnCallFunc = true;
			Disable("btnSave", false);
			Disable("btnReset", false);

		}
	} else {
		setPageEdited(false);
	}

}

/*
 * Function call for SEARCH Button Click
 */
function searchRolePrivilege() {
	objOnFocus();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setField("hdnMode", "SEARCH");
		setTabValues(screenId, valueSeperator, "strSearchCriteria",
				getText("txtRoleNameSearch"));
		setTabValues(screenId, valueSeperator, "intLastRec", 1);
		document.frmRoles.submit();
		top[2].ShowProgress();
	}
}

/*
 * Function call for GRID ROW Click
 */
function rowClick(strRowNo) {
	objOnFocus();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		top[2].ShowProgress();
		setPageEdited(false);
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;
		setField("txtRoleId", strRowData[0]);
		setField("txtRoleName", strRowData[1]);
		setField("txtRemarks", roleData[strRowNo][6]);
		setField("hdnVersion", roleData[strRowNo][4]);
		if (roleData[strRowNo][3] == "Active") {
			getFieldByID("chkStatus").checked = true;
		} else {
			getFieldByID("chkStatus").checked = false;
		}

		blnCallFunc = false;
		ls.selectedData(roleData[strRowNo][5]);
		deselectAgents();
		deselectVisibleAgents();
		setSelectedAgentType(roleData[strRowNo][9]);
		setVisibleAgentType(roleData[strRowNo][10]);
		setServiceChannel(roleData[strRowNo][11]);
		setField("selIncExc", roleData[strRowNo][12]);
		setField("selApplicableAirline", roleData[strRowNo][13]);
		disableInputControls();
		Disable("btnAdd", false);
		Disable("btnEdit", false);
		Disable("btnCopy", false);
		Disable("btnDelete", false);
		top[2].HideProgress();
	}
}

/*
 * Function call for ADD Button Click
 */
function addRolePrivilege() {
	objOnFocus();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		clearControls();
		Disable("btnEdit", false);
		Disable("btnDelete", false);
		Disable("btnCopy", false);
		Disable("txtRoleName", false);
		Disable("txtRemarks", false);
		Disable("chkStatus", false);
		if(isServiceCh){
			if(isServiceChEditable) {
				Disable("selServiceChannel", false);
			} else {
				Disable("selServiceChannel", true);
			}
		}
		getFieldByID("chkStatus").checked = true;
		ls.disable(false);
		deselectAgents();
		deselectVisibleAgents();
		deselectServiceChannels();
		setSelectedAgentType(crragent);
		setVisibleAgentType(crragent);
		setField("hdnMode", "ADD");
		setField("hdnAction", "ADD");
		blnCallFunc = true;
		setStyleClass("txtRoleId", "InputBorder fltStatus02");
		getFieldByID("txtRoleName").focus();
		setPageEdited(false);
		Disable("btnSave", false);
		Disable("btnReset", false);
		Disable("selAgentType", false);
		Disable("selVisibleAgentType", false);
		Disable("selIncExc", false);
		setField("selIncExc", "");
		setField("selApplicableAirline", defAirlineCode);
		Disable("selApplicableAirline", true);
	}
}

/*
 * Function call for EDIT Button Click
 */
function editRolePrivilege() {
	var strMode = getText("hdnMode");
	setStyleClass("txtRoleId", "InputBorder fltStatus02");
	if (!(strMode != null && strMode == "EDIT")) {
		if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
			objOnFocus();
			Disable("txtRoleName", false)
			getFieldByID("txtRoleName").focus();
			Disable("txtRemarks", false);
			Disable("chkStatus", false);
			Disable("selAgentType", false);
			Disable("selVisibleAgentType", false);
			if(isServiceCh){
				if(isServiceChEditable) {
					Disable("selServiceChannel", false);
				} else {
					Disable("selServiceChannel", true);
				}
			}
			ls.disable(false);
			setField("hdnMode", "EDIT");
			setField("hdnAction", "EDIT");
			Disable("btnSave", false);
			Disable("btnDelete", true);
			blnCallFunc = true;
			setPageEdited(false);
			Disable("btnSave", false);
			Disable("btnReset", false);
			Disable("selIncExc", false);
			if(getValue("selIncExc") != ""){				
				Disable("selApplicableAirline", false);
			} else {
				Disable("selApplicableAirline", true);
			}
		}
	} else {
		objOnFocus();
		Disable("txtRoleName", false)
		getFieldByID("txtRoleName").focus();
		Disable("txtRemarks", false);
		Disable("chkStatus", false);
		ls.disable(false);
		setField("hdnMode", "EDIT");
		setField("hdnAction", "EDIT");
		Disable("btnSave", false);
		Disable("btnDelete", true);
		blnCallFunc = true;
		setPageEdited(false);
		Disable("btnSave", false);
		Disable("btnReset", false);
		Disable("selAgentType", false);
		Disable("selVisibleAgentType", false);
		if(isServiceCh){
			if(isServiceChEditable) {
				Disable("selServiceChannel", false);
			} else {
				Disable("selServiceChannel", true);
			}
		}
		Disable("selIncExc", false);
		if(getValue("selIncExc") != ""){				
			Disable("selApplicableAirline", false);
		} else {
			Disable("selApplicableAirline", true);
		}
	}
}

/*
 * Function call for COPY Button Click
 */
function copyRolePrivilege() {
	objOnFocus();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		clearTopControls();
		Disable("btnEdit", true);
		Disable("btnDelete", true);
		Disable("btnAdd", true);
		Disable("btnCopy", true);
		Disable("txtRoleName", false);
		Disable("txtRemarks", false);
		Disable("chkStatus", false);
		Disable("selAgentType", false);
		Disable("selVisibleAgentType", false);
		if(isServiceCh){
			if(isServiceChEditable) {
				Disable("selServiceChannel", false);
			} else {
				Disable("selServiceChannel", true);
			}
		}
		getFieldByID("chkStatus").checked = true;
		ls.disable(true);
		setField("hdnMode", "COPY");
		setField("hdnAction", "COPY");
		blnCallFunc = true;
		setStyleClass("txtRoleId", "InputBorder fltStatus02");
		getFieldByID("txtRoleName").focus();
		setPageEdited(false);
		Disable("btnSave", false);
		Disable("btnReset", false);
		Disable("selIncExc", false);
		Disable("selApplicableAirline", false);
		setField("selIncExc", '');
	}
}

/*
 * Function Call for DELETE Button Click
 */
function deleteRolePrivilege() {
	objOnFocus();
	var strconfirm = confirm(deleteConf);
	setField("hdnRecNo", getTabValues(screenId, valueSeperator, "intLastRec"));
	if (strconfirm == true) {
		enableInputControls();
		setField("hdnRecNo", getTabValues(screenId, valueSeperator,
				"intLastRec"));
		setField("hdnMode", "DELETE");
		setField("hdnRoleId", getValue("txtRoleId"));
		setTabValues(screenId, valueSeperator, "strSearchCriteria",
				getText("txtRoleNameSearch"));
		document.frmRoles.submit();
		top[2].ShowProgress();
		disableInputControls();
	}
}

/*
 * Function Call for SAVE Button Click
 */
function saveData() {
	objOnFocus();
	Disable("txtRoleId", "true");
	setField("hdnAssignPrivileges", trim(ls.getselectedData()));
	
	var strMode = getText("hdnMode");
	if((strMode != null && strMode == "EDIT")) {
		if(!getFieldByID("selAgentType") && !isAgentSelected())
		{
			setField("hdnNoAgent",roleData[strGridRow][9]);
		}

	}
	else {
	setField("hdnNoAgent","");
	}
	
	if((strMode != null && strMode == "ADD")){
		if(getValue("selIncExc") == "EXC"){
			showERRAlertMessage(excRoleInvalid);
			return;
		}
	}
	
	if (isEmpty(getText("txtRoleName"))) {
		showERRAlertMessage(roleNameRqrd);
		getFieldByID("txtRoleName").focus();

	} else if(getValue("selIncExc") != "" &&
			getValue("selApplicableAirline") == defAirlineCode){ 
		showERRAlertMessage(applicableAirlineInvalid);
		
	} else if (getFieldByID("selAgentType") && !isAgentSelected()) {
		showERRAlertMessage(agentRqed);
		getFieldByID("selAgentType").focus();

	} else if (getFieldByID("selVisibleAgentType") && !isAccesibleSelected()) {
		showERRAlertMessage(accAgentReqrd);
		getFieldByID("selVisibleAgentType").focus();
	} else if (getFieldByID("selServiceChannel") && !isServiceChSelected()){
		showERRAlertMessage(servChRqrd);
		getFeildByID("selServiceChannel").focus();
	} else {
		Disable("txtRoleId", false);
		var invalidAgentTypes = [];

		if((strMode != null && strMode == "EDIT")) {
			if((roleData[strGridRow][14].length > 0)) {

				if (getFieldByID("selVisibleAgentType")) {
					for(var j = 0; j < roleData[strGridRow][14].length ; j++ ){
						var intCount = getFieldByID("selVisibleAgentType").length;
						var found = false;
						for ( var i = 0; i < intCount; i++) {
							if (getFieldByID("selVisibleAgentType").options[i].selected) {
								if(roleData[strGridRow][14][j] == getFieldByID("selVisibleAgentType").options[i].value){
									found = true;
								}
							}

							if(!found && i == intCount -1){
								var currentSize = invalidAgentTypes.length;
								invalidAgentTypes[currentSize]= roleData[strGridRow][14][j];
							}
						}

					}


				}

			}
		}

		setField("txtRoleId", trim(getText("txtRoleId")));
		setField("txtRoleName", trim(getText("txtRoleName")));
		setField("txtRemarks", trim(getText("txtRemarks")));
		var tempName = trim(getText("txtRemarks"));
		tempName = replaceall(tempName, "'", "`");
		tempName = replaceEnter(tempName);
		setField("txtRemarks", tempName);
		setField("hdnRecNo", getTabValues(screenId, valueSeperator,
				"intLastRec"));
		setField("hdnMode", "SAVE");
		setTabValues(screenId, valueSeperator, "strSearchCriteria",
				getText("txtRoleNameSearch"));
		setTabValues(screenId, valueSeperator, "strGridRow", strGridRow); // Save
																			// the
		if(invalidAgentTypes.length > 0 ){
		
			//If needed role can be removed from respective users. But considering data inconsistancies that may
			//occur, dropping of roles from existing users did not implement. - AARESAA-6555
		   var warningMessage = buildError(agTypeChangeWarn,invalidAgentTypes);
		   if(confirm(warningMessage)){
			 document.frmRoles.submit();
		     top[2].ShowProgress();
		   }
				
		} else {
		  document.frmRoles.submit();
		  top[2].ShowProgress();
		}																	// grid
																			// Row
		
	}
}

/*
 * Function Call for page edit
 */
function clickChange() {
	objOnFocus();
	setPageEdited(true);
}

function setNotEditedOnly(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

/*
 * Function Button behavior of the page
 */
function setPageEdited(isEdited) {
	setNotEditedOnly(isEdited);
	if (isEdited) {
		Disable("btnSave", "");
		Disable("btnReset", "");

	} else {
		Disable("btnSave", "false");
		Disable("btnReset", "false");
		Disable("btnDelete", "false");
	}
}

/*
 * Function Disables the UI Controls
 */
function disableInputControls() {
	Disable("txtRoleId", true);
	Disable("txtRoleName", true);
	Disable("txtRemarks", true);
	Disable("btnEdit", true);
	Disable("btnDelete", true);
	Disable("btnCopy", true);
	Disable("chkStatus", true);
	getFieldByID("selIncExc").disabled = true;
	getFieldByID("selApplicableAirline").disabled = true;
	ls.disable(true);
	if (getFieldByID("selAgentType"))
		getFieldByID("selAgentType").disabled = true;
	if (getFieldByID("selVisibleAgentType"))
		getFieldByID("selVisibleAgentType").disabled = true;
	if (getFieldByID("selServiceChannel"))	
		getFieldByID("selServiceChannel").disabled = true;
}

/*
 * Function Enables the UI Controls
 */
function enableInputControls() {
	Disable("txtRoleId", true);
	Disable("txtRoleName", false);
	Disable("txtRemarks", false);
	Disable("btnEdit", false);
	Disable("btnDelete", false);
	Disable("btnCopy", false);
	Disable("chkStatus", false);
	ls.disable(false);
	if (getFieldByID("selAgentType"))
		getFieldByID("selAgentType").disabled = false;
	if (getFieldByID("selVisibleAgentType"))
		getFieldByID("selVisibleAgentType").disabled = false;
	if (getFieldByID("selServiceChannel"))
		getFieldByID("selServiceChannel").disabled = false;
}

/*
 * Function Clear the Controls
 */
function clearControls() {
	blnCallFunc = false;
	objOnFocus();
	setField("txtRoleId", "");
	setField("txtRoleName", "");
	setField("txtRemarks", "");
	setField("hdnVersion", "");
	setField("hdnNoAgent","");
	setField("hdnMode", "");
	getFieldByID("chkStatus").checked = false;
	ls.removeAllFromListbox("lstAssignedRoles", "lstRoles", 2);
	deselectAgents();
	deselectVisibleAgents();
	deselectServiceChannels();
}

/*
 * Function Clear the all Controls except for the AssignedRoles
 */
function clearTopControls() {
	blnCallFunc = false;
	objOnFocus();
	setField("txtRoleId", "");
	setField("txtRoleName", "");
	setField("txtRemarks", "");
	setField("hdnVersion", "");
	setField("hdnNoAgent","");
	setField("hdnMode", "");
	getFieldByID("chkStatus").checked = false;
	deselectAgents();
	deselectVisibleAgents();
}

/*
 * Function Call for RESET Button Click
 */
function resetClick() {
	var strMode = getText("hdnMode");
	objOnFocus();
	if (strMode == "ADD") {
		clearControls();
		getFieldByID("chkStatus").checked = true;
		getFieldByID("txtRoleName").focus();
		setField("hdnMode", "ADD");
	}
	if (strMode == "COPY") {
		clearTopControls();
		getFieldByID("chkStatus").checked = true;
		getFieldByID("txtRoleName").focus();
		setField("hdnMode", "COPY");
	}
	if (strMode == "EDIT") {
		if (strGridRow < roleData.length
				&& roleData[strGridRow][1] == getText("txtRoleId")) {
			setField("hdnMode", "EDIT");
			enableInputControls();
			setField("txtRoleId", roleData[strGridRow][1]);
			setField("txtRoleName", roleData[strGridRow][2]);
			setField("txtRemarks", roleData[strGridRow][6]);
			if (roleData[strGridRow][3] == "Active") {
				getFieldByID("chkStatus").checked = true;
			} else {
				getFieldByID("chkStatus").checked = false;
			}
			setField("hdnVersion", roleData[strGridRow][4]);
			blnCallFunc = false;
			getFieldByID("txtRoleName").focus();
			ls.selectedData(roleData[strGridRow][5]);
			deselectAgents();
			deselectVisibleAgents();
			setSelectedAgentType(roleData[strGridRow][9]);
			setField("hdnNoAgent",roleData[strGridRow][9]);
			setVisibleAgentType(roleData[strGridRow][10]);
			setServiceChannel(roleData[strGridRow][11]);
			setField("selIncExc", roleData[strGridRow][12]);
			setField("selApplicableAirline", roleData[strGridRow][13]);
		} else {
			// Clear the data entry area
			clearControls();
			Disable("txtRoleId");
			disableInputControls();
			setField("hdnModel", "DISPLAY");
			Disable("btnAdd", false);
			buttonSetFocus("btnAdd");
			Disable("btnEdit", true);
			Disable("btnDelete", true);
			Disable("btnCopy", true);
		}
	}
	setNotEditedOnly(false);
}

/*
 * Function To Move the Dependencies of a Privilege
 */
function moveDependancy() {
	if (blnCallFunc) {
		var strData = "";
		var privId = ls.getMarkedData();
		var strSelectedIndex = privId.split(",");
		for (depedlen = 0; depedlen < dependancy.length; depedlen++) {
			for (selLen = 0; selLen < strSelectedIndex.length; selLen++) {
				if (dependancy[depedlen][0] == strSelectedIndex[selLen]) {
					if (strData != "") {
						strData += ","
					}
					strData += dependancy[depedlen];
				}
			}
		}
		blnCallFunc = false;
		ls.selectedData(strData);
		blnCallFunc = true;
	}
	return true;
}

/*
 * Function to Hide the Page Message
 */
function objOnFocus() {
	top[2].HidePageMessage();
}

/*
 * Function to Handle the Paging
 */
function gridNavigations(intRecNo, intErrMsg) {
	objOnFocus();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {

		if (intRecNo <= 0)
			intRecNo = 1;

		setTabValues(screenId, valueSeperator, "intLastRec", intRecNo);
		setField("hdnRecNo", getTabValues(screenId, valueSeperator,
				"intLastRec"));
		if (intErrMsg != "") {
			top[2].objMsg.MessageText = "<li> " + intErrMsg;
			top[2].objMsg.MessageType = "Error";
			top[2].ShowPageMessage();
		} else {
			setTabValues(screenId, valueSeperator, "strSearchCriteria",
					getText("txtRoleNameSearch"));
			document.frmRoles.submit();
			top[2].ShowProgress();
		}
	}
}

/*
 * Function to Validate the Role Name Field
 */
function nameVAalidate() {
	objOnFocus();
	var strRoleName = getText("txtRoleName");
	var strLen = strRoleName.length;
	var blnVal = isAlphaNumericWhiteSpace(strRoleName);
	if (!blnVal) {
		setField("txtRoleName", strRoleName.substr(0, strLen - 1));
		getFieldByID("txtRoleName").focus();
	}
}

/*
 * Function to Validate Remarks Field
 */
function validateTA(objControl, length) {
	var strValue = objControl.value;
	var strLength = strValue.length;
	var blnVal = isEmpty(strValue);
	if (blnVal) {
		setField("txtRemarks", strValue.substr(0, strLength - 1));
		getFieldByID("txtRemarks").focus();
	}
	if (strLength > length) {
		strValue = strValue.substr(0, length);
		objControl.value = strValue;
	}
}

/** sets the agent types from an Array */
function setSelectedAgentType(arrCode) {
	if (getFieldByID("selAgentType")) {
		var intCount = getFieldByID("selAgentType").length;
		for ( var agl = 0; agl < arrCode.length; agl++) {
			for ( var i = 0; i < intCount; i++) {
				if (trim(getFieldByID("selAgentType").options[i].value) == trim(arrCode[agl])) {
					getFieldByID("selAgentType").options[i].selected = true;
					break;
				}
			}
		}
	}
}

/** sets the visible agent types from an Array */
function setVisibleAgentType(arrCode) {
	if (getFieldByID("selVisibleAgentType")) {
		var intCount = getFieldByID("selVisibleAgentType").length;
		for ( var agl = 0; agl < arrCode.length; agl++) {
			for ( var i = 0; i < intCount; i++) {
				if (trim(getFieldByID("selVisibleAgentType").options[i].value) == trim(arrCode[agl])) {
					getFieldByID("selVisibleAgentType").options[i].selected = true;
					break;
				}
			}
		}
	}

}
/** sets the visible agent types from an Array */
function setServiceChannel(val) {
	if (getFieldByID("selServiceChannel")) {
		var intCount = getFieldByID("selServiceChannel").length;
		for ( var i = 0; i < intCount; i++) {	
			if ((getFieldByID("selServiceChannel").options[i].value) == (val)) {
				getFieldByID("selServiceChannel").options[i].selected = true;
				break;
			}
		}

	}

}

/** deselect the visible agent types * */
function deselectVisibleAgents() {
	if (getFieldByID("selVisibleAgentType")) {
		var intCount = getFieldByID("selVisibleAgentType").length;
		for ( var i = 0; i < intCount; i++) {
			getFieldByID("selVisibleAgentType").options[i].selected = false;
		}
	}

}

/** deselect the service channels */
function deselectServiceChannels() {
	if (getFieldByID("selServiceChannel")) {
		var intCount = getFieldByID("selServiceChannel").length;
		for ( var i = 0; i < intCount; i++) {
			getFieldByID("selServiceChannel").options[i].selected = false;
		}
	}

}

/** deselect the agent types * */
function deselectAgents() {
	if (getFieldByID("selAgentType")) {
		var intCount = getFieldByID("selAgentType").length;
		for ( var i = 0; i < intCount; i++) {
			getFieldByID("selAgentType").options[i].selected = false;
		}
	}
}

/** validate for mandatory check * */
function isAgentSelected() {
	if (getFieldByID("selAgentType")) {
		var intCount = getFieldByID("selAgentType").length;
		for ( var i = 0; i < intCount; i++) {
			if (getFieldByID("selAgentType").options[i].selected) {
				return true;
			}
		}
	}
	return false;
}
/** validate for mandatory check * */
function isServiceChSelected() {
	if (getFieldByID("selServiceChannel")) {
		var intCount = getFieldByID("selServiceChannel").length;
		for ( var i = 0; i < intCount; i++) {
			if (getFieldByID("selServiceChannel").options[i].selected) {
				return true;
			}
		}
	}
	return false;
}
/** validate for mandatory check * */
function isAccesibleSelected() {
	if (getFieldByID("selVisibleAgentType")) {
		var intCount = getFieldByID("selVisibleAgentType").length;
		for ( var i = 0; i < intCount; i++) {
			if (getFieldByID("selVisibleAgentType").options[i].selected) {
				return true;
			}
		}
	}
	return false;
}

function setApplicableAirline(selValue){
	if(selValue == ""){
		setField("selApplicableAirline", defAirlineCode);
		Disable("selApplicableAirline", true);
	} else {
		Disable("selApplicableAirline", false);
	}
}