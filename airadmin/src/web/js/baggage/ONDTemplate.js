var selectedBaggage = '';
var baggageMandatory = null;
var baggageCheck = new Array();
var templatesData = {};
var selectedTemplateGridRowId;
var agentsMultiSelect;
var flightMultiSelect; 
var salesChannels=[
    {value:"ALL" , label:"**ALL**"},
    {value:"3" , label:"Agent"},
    {value:"4" , label:"Web"},
    {value:"12" , label:"API"},
    {value:"16" , label:"GDS"},
    {value:"20" , label:"IOS"},
    {value:"21" , label:"Android"}]; //This is Stupid,but not refactoring now
var agentAvailableChannelCodes=["3", "16"];

jQuery(document)
		.ready(
				function() { // the page is ready

					var grdArray = new Array();
					var ondnew="";

					$("#divSearchPanel").decoratePanel("Search OND Codes");
//					$("#divDispBaggageTemp").decoratePanel(
//							"Add/Modify OND Code");
					$("#divResultsPanel").decoratePanel("OND Codes");

                    $("#baggageTemplateTabs").tabs();

					$('#btnAdd').decorateButton();
					$('#btnEdit').decorateButton();
					$('#btnDelete').decorateButton();
					$('#btnClose').decorateButton();
					$('#btnReset').decorateButton();
					$('#btnSave').decorateButton();
					$('#btnSearch').decorateButton();
					$('#btnChgAdd').decorateButton();
					$('#btnChgEdit').decorateButton();
					$('#selTemplate').mouseover(function() {
						ondForBaggageTemplateCodesReload();
					});
					addSalesChannels();

//					enableGridButtons(false);
					enbleFormButtons(false);
					
                    agentsMultiSelect = new Listbox('lstAvailableAgents', 'lstAssignedAgents', 'agentsSpan', 'agentsMultiSelect');
                    agentsMultiSelect.group1 = agentsStationsJson.stations;
                    agentsMultiSelect.list1 = agentsStationsJson.agents;
                    agentsMultiSelect.height = '150px';
                    agentsMultiSelect.width  = '350px';
                    agentsMultiSelect.headingLeft = 'All Agents';
                    agentsMultiSelect.headingRight = 'Selected Agents';
                    agentsMultiSelect.filter = true;
                    agentsMultiSelect.filteringCharCount=3;
                    agentsMultiSelect.drawListBox();
                    
                   // controlAgentTab();
                    
                    var flights=$(flightList);
                    
                 
                    flightMultiSelect=new Listbox('lstAvailableFlights', 'lstAssignedFlights', 'flightsSpan', 'flightMultiSelect');
                    flightMultiSelect.group1 = getFlightPaneData(flights);
                    flightMultiSelect.height = '150px';
                    flightMultiSelect.width  = '200px';
                    flightMultiSelect.headingLeft = 'All Flights';
                    flightMultiSelect.headingRight = 'Selected Flights';
                    flightMultiSelect.filter = true;
                    flightMultiSelect.filteringCharCount=1;
                    flightMultiSelect.drawListBox();
                    //hide >> , << buttons
                    $('#flightsSpan_1').hide();
                    $('#flightsSpan_4').hide();
                    
                    $('#addBookingClasses').click(function(){addToSelectPane('bcAvailable', 'bcAssigned')});
                    $('#removeBookingClasses').click(function(){removeFromSelectPane('bcAssigned', 'bcAvailable')});
                    
                    $('#addChannel').click(function(){
                    	addToSelectPane('channelAvailable', 'channelAssigned');
                    	controlAgentTab();
                    });
                    $('#removeChannel').click(function(){
                    	removeFromSelectPane('channelAssigned', 'channelAvailable');
                    	controlAgentTab();
                    });
                    
                    $('#addOnd').click(function(){addOnd()});
                    $('#removeOnd').click(function(){removeOnd()});

					jQuery("#listONDCode")
							.jqGrid(
									{
										url : 'showONDCode!searchONDCodes.action?selStatus='
												+ $("#selStatus").val(),
										datatype : "json",
										jsonReader : {
											root : "rows",
											page : "page",
											total : "total",
											records : "records",
											repeatitems : false,
											id : "0",
											userdata : "baggageMandatory"
										},
										colNames : [ '&nbsp;', 'Template Id','Template Code', 'Description' ,'From Date','To Date',
                                                        'Version'],
										colModel : [
												{
													name : 'id',
													width : 40,
													jsonmap : 'id'
												},
												{
													name : 'templateId',
													width : 125,
													jsonmap : 'baggageTemplate.templateId'
												},
												{
													name : 'templateCode',
													width : 175,
													jsonmap : 'baggageTemplate.templateCode'
												},
                                                {
													name : 'description',
													width : 250,
													jsonmap : 'baggageTemplate.description'
												},
												{	
													name : 'fromDate',
													width : 100,
													jsonmap : 'baggageTemplate.fromDate',
													formatter : getDatePart	
												},
												{
													name : 'toDate',
													width : 100,
													jsonmap : 'baggageTemplate.toDate',
													formatter : getDatePart		
												},
												{
													name : 'version',
													width : 50,
													align : "center",
													jsonmap : 'baggageTemplate.version',
													hidden : true
												}
										],
										imgpath : '../../themes/default/images/jquery',
										multiselect : false,
										pager : jQuery('#temppager'),
										rowNum : 20,
										viewrecords : true,
										height : 175,
										width : 925,
										loadui : 'block',
										onSelectRow : function(rowid) {
//											if (isPageEdited()) {
//												$('#frmBaggage').clearForm();
//												hideMessage();
//												setPageEdited(false);
//												$("#rowNo").val(rowid);
//												fillForm(rowid);
//												disableControls(true);
//												enableSearch();
//												enableGridButtons(true);
//												enbleFormButtons(false);
//											}
                                            loadBaggageTemplateToPane(rowid);
                                            selectedTemplateGridRowId = rowid;
                                            hideMessage();
                                            disableControls(true);
                                            enbleFormButtons(false);
                                            enableSearch();
											enableGridButtons(true);
                                            $("#btnEdit").attr('disabled', false);


//											baggageMandatory = jQuery("#listONDCode").getGridParam('userdata');
										} ,
                                        loadComplete : function (response) {
                                            var resp = eval("(" + response.responseText + ")");
                                            templatesData = resp.rows;

                  
                                            
                                        }
									}).navGrid("#temppager", {
								refresh : true,
								edit : false,
								add : false,
								del : false,
								search : false
							});

					var options = {
						cache : false,
						beforeSubmit : showRequest, // pre-submit callback -
						// this can be used to do
						// the validation part
						success : showResponse, // post-submit callback
						dataType : 'json' // 'xml', 'script', or 'json'
					// (expected server response type)

					};

					var delOptions = {
						cache : false,
						beforeSubmit : showDelete, // pre-submit callback -
						// this can be used to do
						// the validation part
						success : showResponse, // post-submit callback
						url : 'showONDCode!deleteONDCodes.action', // override
						// for
						// form's
						// 'action'
						// attribute
						dataType : 'json', // 'xml', 'script', or 'json'
						// (expected server response type)
						resetForm : true
					// reset the form after successful submit

					};
					
					function getFlightPaneData(flights){
						var availFlights=[];
	                    var allPropertyArray=[];
	                    allPropertyArray.push("**ALL**");
	                    allPropertyArray.push("ALL"); 
	                    availFlights.push(allPropertyArray);
	                    for(var i=0;i<flights.length;i++){
	                    	var propertyArray=[];
	                    	propertyArray.push(flights[i].label);
	                    	propertyArray.push(flights[i].value);    
	                    	availFlights.push(propertyArray);
	                    }
	                    return availFlights;
					}
					
					function fillForm(rowid) {
						
						jQuery("#listONDCode").GridToForm(rowid,
								"#frmBaggage");
						
						if (jQuery("#listONDCode").getCell(rowid, 'status') == 'ACT') {
							$("#ondStatusTemp").attr('checked', true);
						} else {
							$("#ondStatusTemp").attr('checked', false);
						}
						$("#templateCode").val(jQuery("#listONDCode").getCell(rowid, 'templateCode'));
						$("#templateId").val(jQuery("#listONDCode").getCell(rowid, 'templateId'));

						var ondCode= jQuery("#listONDCode").getCell(rowid, 'ondCode');
						//alert(ondCode);
						var ondPoints = ondCode.split("/");
						for ( var i = 0; i < ondPoints.length; i++) {
							if(i==0){
								$("#selDepature option:contains(" + ondPoints[i] + ")").attr('selected', 'selected');
							} else if ( (i+1) == ondPoints.length){
								$("#selArrival option:contains(" + ondPoints[i] + ")").attr('selected', 'selected');
							} else {
								$("#selVia"+i+" option:contains(" + ondPoints[i] + ")").attr('selected', 'selected');
							}
						}
						if(jQuery("#listONDCode").getCell(rowid, 'fromDate').length != "" && jQuery("#listONDCode").getCell(rowid, 'fromDate').length != ""){
							$('#dateRange').attr('disabled', false);
							$("#dateRange").val(jQuery("#listONDCode").getCell(rowid, 'fromDate') + ' - ' + jQuery("#listONDCode").getCell(rowid, 'toDate'));
							$('#dateRange').attr('disabled', true);
						}
						//$("#dateRange").val(jQuery("#listONDCode").getCell(rowid, 'fromDate'));
						
				}

					function validate(){
						
						
						if ($("#selTemplate").val() == null || $("#selTemplate").val() == "") {
							showCommonError("Error", ondBaggTempCodeRqrd);
							return false;
						}
						
						if(validateOndCode()){
							//if ($("#ondCodeNew").val() == null || $("#ondCodeNew").val() == "") {
							if (ondnew == null || ondnew == "") {	
								showCommonError("Error", ondCodeRqrd);
								return false;
							}
						} else{
							return false;
						}
						
						return true;
					}
					
					// pre-submit callback
					function showRequest(formData, jqForm, options) {
						if ($("#baggageCharges").val() == null || $("#baggageCharges").val() == "") {
							showCommonError("Error", baggageRqrd);
							return false;
						}				

						top[2].ShowProgress();
						return true;
					}

					function showResponse(responseText, statusText) {
						top[2].HideProgress();
						showCommonError(responseText['msgType'],
								responseText['succesMsg']);
						//AARESAA-13467 fix: Disable drop downs
						disableControlsInFrmBaggage(true);
                            if (responseText['msgType'] != "Error") {
							alert(responseText['succesMsg']);
							setPageEdited(false);
							$('#frmBaggage').clearForm();
							//jQuery("#listBaggageChg").clearGridData();
							jQuery("#listONDCode").trigger("reloadGrid");
							enableGridButtons(false);
							enbleFormButtons(false);
							$("select#selONDCode").html(
									responseText['templOption']);

						}else{
							//AARESAA-13717 : Enable edit if error saving.
							disableControls(false);
						 }


					}

					function showDelete(formData, jqForm, options) {
						return confirm(deleteRecoredCfrm);
					}
					$('#btnSave').click(function() {
						hideMessage();
						disableControls(false);

//						if(validate()){

//							alert("Saving OND Code will take some time.....");
							$("#baggageCharges").val(ondnew);
							$('#templateCode').attr('disabled', false);
							$('#selDepature').attr('disabled', false);
							$('#selVia1').attr('disabled', false);
							$('#selVia2').attr('disabled', false);
							$('#selVia3').attr('disabled', false);
							$('#selVia4').attr('disabled', false);
							$('#selArrival').attr('disabled', false);

                            var baggageTo = constructBaggageTemplateTo();
                            top[2].ShowProgress();
                            $.ajax({
                                url : 'showONDCode!saveBaggageTemplate.action',
                                beforeSend : top[2].ShowProgress(),
                                data : baggageTo,
                                type : "POST",
                                dataType : "json",
                                complete : function(resp) {
                                    top[2].HideProgress();

                                    resp = eval("("+resp.responseText+")");
                                    showCommonError(resp.msgType, resp.succesMsg );

                                    jQuery("#listONDCode").trigger("reloadGrid");
                                }
                            });


							return true;

//                        }

						return false;
					});

					function buildONDStr() {
						var chargStr = "";
					
						
						
						$("#selSelected option").each(function()
							{
									chargStr += $(this).val() + " "
							});
						
						
						//$("#ondStatusTemp").val($("#status").val());
						//alert($("#ondStatusTemp").val());
						return chargStr;
					}
					
					$('#cos').change(function(){
						loadBaggageList('false',"","");
					});
					
					function loadBaggageList(condition,baggageId,baggageValue) {
						var data = {};				
						data["isGridBaggage"] = condition;
						data["selBaggageId"] = baggageId;
						data["baggageValue"] = baggageValue;
						
						if (condition == "true") {
							$("#selBaggage").attr('disabled', true); 
						} else {
							$("#selBaggage").attr('disabled', false); 
						}
						
						$("#frmBaggage").ajaxSubmit({
						dataType: 'json', 
						data:data, 
						url:"showBaggageTemplate!getBaggagesForCabinClass.action",							
						success: loadSuccess});			
					}
					
					function addSalesChannels(){
                    	$("#channelAvailable").html('');
                    	
                    	var channelAvailHtml='';
                        for(var i=0;i< salesChannels.length;i++){
                        	channelAvailHtml+="<option value=\"" + salesChannels[i].value + "\">" + salesChannels[i].label + "</option>";
                        }
                        $("#channelAvailable").html(channelAvailHtml);
                    }
					
					loadSuccess = function(response){
						if(response.baggageMandatory != null){
							baggageMandatory = response.baggageMandatory;
						}
						if (response.baggageForCC != null && trim(response.baggageForCC) != "") {
							$("#selBaggage").html(response.baggageForCC);
						}else {
							alert("No Baggages for selected cabin class");
							$("#selBaggage").html("<option value=''>");
							$("#selBaggage").attr('disabled', true); 
						}
					}

					$('#btnSearch')
							.click(
									function() {
										hideMessage();
										var strUrl = "showONDCode!searchONDCodes.action?selONDCode="
												+ $("#selONDCode").val()
												+ "&selStatus="
												+ $("#selStatus").val()
												+"&selONDtemplateCodeID ="+
												$("#selONDtemplateCodeID").val();
										jQuery("#listONDCode")
												.setGridParam({
													url : strUrl
												});
										jQuery("#listONDCode").trigger(
												"reloadGrid");
										enbleFormButtons(false);
										enableGridButtons(false);
										$('#frmBaggage').clearForm();
										$("#version").val('-1');
										jQuery("#listBaggageChg")
												.clearGridData();
										grdArray  = new Array();
										disableControls(true);
										enableSearch();
//										$("#btnAdd").attr('disabled', false);
//										$("#btnEdit").attr('disabled', false);
									});

					$('#btnAdd').click(function() {
						hideMessage();
						disableControls(false);
						$('#frmBaggage').clearForm();
						$("#version").val('-1');
						jQuery("#listBaggageChg").clearGridData();
						$("#selSelected").empty();
						$("#templateId").val('');
						grdArray = new Array();
						enbleFormButtons(true);
					});

					$('#btnEdit').click(function() {
						hideMessage();
						disableControls(false);

//						$('#selDepature').attr('disabled', true);
//						$('#selVia1').attr('disabled', true);
//						$('#selVia2').attr('disabled', true);
//						$('#selVia3').attr('disabled', true);
//						$('#selVia4').attr('disabled', true);
//						$('#selArrival').attr('disabled', true);
						enbleFormButtons(true);
					});
					$('#btnReset').click(function() {
                        loadBaggageTemplateToPane(selectedTemplateGridRowId);

					});
					$('#btnDelete').click(function() {
						disableControls(false);
						$('#frmBaggage').ajaxSubmit(delOptions);
						return false;
					});
					
					function disableControls(cond) {
						$("input").each(function() {
							$(this).attr('disabled', cond);
						});
						$("textarea").each(function() {
							$(this).attr('disabled', cond);
						});
						$("select").each(function() {
							$(this).attr('disabled', cond);
						});
						$("#btnClose").attr('disabled', false);
						$("#dateRange").attr('disabled', true);
						
					}
					
					function disableControlsInFrmBaggage(cond){
						$('#frmBaggage').find('input, button, select').attr('disabled','disabled');
						$("#btnClose").attr('disabled', false);
					}
					
					function enableGridButtons(cond) {
						if (cond) {
							$("#btnEdit").enableButton();
							$("#btnDelete").enableButton();
							
						} else {
							$("#btnEdit").disableButton();
							$("#btnDelete").disableButton();
							
						}

					}

					function enbleFormButtons(cond) {
						if (cond) {
							$("#btnReset").enableButton();
							$("#btnSave").enableButton();
						} else {
							$("#btnReset").disableButton();
							$("#btnSave").disableButton();
						}

					}

					function enableSearch() {
						$("#btnSearch").attr('disabled', false);
						$("#selONDCode").attr('disabled', false);
						$("#selStatus").attr('disabled', false);
						$("#selONDtemplateCodeID").attr('disabled', false);
					}

					disableControls(true);
					$("#btnAdd").attr('disabled', false);
					enableSearch();

					function setPageEdited(isEdited) {
						top[1].objTMenu.tabPageEdited(screenId, isEdited);
						hideMessage();
					}

					function isPageEdited() {
						return top.loadCheck(top[1].objTMenu
								.tabGetPageEidted(screenId));
					}
					function hideMessage() {
						top[2].HidePageMessage();
					}
					
					function validateOndCode() {
						var isContained = false;
						hideMessage();
						var deptVal = getText("selDepature");
						var arrVal  = getText("selArrival");
						var via1Val = getText("selVia1");
						var via2Val = getText("selVia2");
						var via3Val = getText("selVia3");
						var via4Val = getText("selVia4");
						
						var dept = $("#selDepature option:selected").text();
						var arr  = $("#selArrival option:selected").text();
						var via1 = $("#selVia1 option:selected").text();
						var via2 = $("#selVia2 option:selected").text();
						var via3 = $("#selVia3 option:selected").text();
						var via4 = $("#selVia4 option:selected").text();

						if (dept == "") {
							showERRMessage(depatureRqrd);
							getFieldByID("selDepature").focus();
							return false;

						} else if (dept != "" && deptVal == "INA") {
							showERRMessage(departureInactive);
							getFieldByID("selDepature").focus();
							return false;

						} else if (arr == "") {
							showERRMessage(arrivalRqrd);
							getFieldByID("selArrival").focus();
							return false;

						} else if (arr != "" && arrVal == "INA") {
							showERRMessage(arrivalInactive);
							getFieldByID("selArrival").focus();
							return false;

						} else if (dept == arr && dept != "All") {
							showERRMessage(depatureArriavlSame);
							getFieldByID("selArrival").focus();
							return false;

						} else if (arr == via1 || arr == via2 || arr == via3 || arr == via4) {
							showERRMessage(arriavlViaSame);
							getFieldByID("selArrival").focus();
							return false;

						} else if (dept == via1 || dept == via2 || dept == via3 || dept == via4) {
							showERRMessage(depatureViaSame);
							getFieldByID("selDepature").focus();
							return false;

						} else if ((via2 != "" || via3 != "" || via4 != "") && via1 == "") {
							showERRMessage(vianotinSequence);
							getFieldByID("selVia1").focus();
							return false;

						} else if ((via3 != "" || via4 != "") && via2 == "") {
							showERRMessage(vianotinSequence);
							getFieldByID("selVia1").focus();
							return false;

						} else if ((via4 != "") && via3 == "") {
							showERRMessage(vianotinSequence);
							getFieldByID("selVia1").focus();
							return false;

						} else if ((via1 != "" && via2 != "" && via3 != "")
								&& (via1 == via2 || via1 == via3 || via1 == via4)) {
							showERRMessage(viaEqual);
							getFieldByID("selVia1").focus();
							return false;

						} else if ((via1 != "" && via2 != "" && via3 != "")
								&& (via2 == via3 || via2 == via4)) {
							showERRMessage(viaEqual);
							getFieldByID("selVia2").focus();
							return false;

						} else if ((via1 != "" && via2 != "" && via3 != "") && (via3 == via4)) {
							showERRMessage(viaEqual);
							getFieldByID("selVia4").focus();
							return false;

						} else if ((via1 != "" && via2 != "") && via1 == via2) {
							showERRMessage(viaEqual);
							getFieldByID("selVia1").focus();
							return false;

						} else if (via1 != "" && via1Val == "INA") {
							showERRMessage(via1 + " " + viaInactive);
							getFieldByID("selVia1").focus();
							return false;
						} else if (via2 != "" && via2Val == "INA") {
							showERRMessage(via2 + " " + viaInactive);
							getFieldByID("selVia2").focus();
							return false;
						} else if (via3 != "" && via3Val == "INA") {
							showERRMessage(via3 + " " + viaInactive);
							getFieldByID("selVia3").focus();
							return false;
						} else if (via4 != "" && via4Val == "INA") {
							showERRMessage(via4 + " " + viaInactive);
							getFieldByID("selVia4").focus();
							return false;
						} else {
							var str = "";
							str += dept;

							if (via1 != "") {
								str += "/" + via1;
							}
							if (via2 != "") {
								str += "/" + via2;
							}
							if (via3 != "") {
								str += "/" + via3;
							}
							if (via4 != "") {
								str += "/" + via4;
							}
							str += "/" + arr;

							
							ondnew=str;
							return true
							
						}
					}
					
					function getDatePart(cellvalue){
						if(cellvalue != null){
							var tempDate = cellvalue.substring(0,10);
							var formatedDate =  formatDate(tempDate, "yyyy/mm/dd", "dd/mm/yyyy");
							return formatedDate;
						}else{
							return '&nbsp';
						}
					}
					
					function ondForBaggageTemplateCodesReload(){
						if(top[1].objTMenu._arrTabNeedtoReload[1] != ""){
							$("select#selTemplate").empty().append(top[1].objTMenu._arrTabNeedtoReload[2])
							top[1].objTMenu._arrTabNeedtoReload[1] = "";
							top[1].objTMenu._arrTabNeedtoReload[2] = "";
							$('#mybutton').click(function(){
							    var $option = $('select#selTemplate option:content()');
							    $option.attr('value', '');
							    $option.text('');
							});
						}
					}

                    var addToSelectPane = function (src, dest) {
                        var optVal;
                        var optText;

//                        var selected = $("select#" + src + " option:selected");
//                        $("#" + dest).append(selected);


                        $("select#" + src + " option:selected").each(
                            function () {
                                optVal = $(this).val();
                                optText = $(this).text();

                                if ($("select#" + dest + " option[value='" + optVal + "']").length == 0) {
                                	if ( $("#"+dest+" option[value='"+optVal+"']").length == 0 ){                                		
                                		if(optVal == "ALL"){
                                			$("#" + dest).prepend("<option value=\"" + optVal + "\">" + optText + "</option>");
                                		} else {                                		
                                			$("#" + dest).append("<option value=\"" + optVal + "\">" + optText + "</option>");
                                		}
                                	}
                                    $("select#" + src + " option").remove("[value='" + optVal + "']");
                                }
                            });

                    }

                    var removeFromSelectPane = function (dest, src) {
                        var optVal;

                        $("select#" + dest + " option:selected").each(
                            function () {
                                optVal = $(this).val();
                                optText = $(this).text();
                                if ( $("#"+src+" option[value='"+optVal+"']").length == 0 ){                                	  
                                	if(optVal == "ALL"){
                                		$("#" + src).prepend("<option value=\"" + optVal + "\">" + optText + "</option>");
                                	} else {                                	
                                		$("#" + src).append("<option value=\"" + optVal + "\">" + optText + "</option>");
                                	}
                                }
                                $("select#" + dest + " option").remove("[value='" + optVal + "']");
                            });
                    }
                    
                    

                    var addOnd = function() {
                        var val;
                        var temp;

                        if (validateOndCode()) {
                            val = $("select#selDepature option:selected").val();
                            temp = $("#selVia1 option:selected").val();
                            if (temp != null && temp != '') {
                                val = val + "/" + temp;
                            }
                            temp = $("#selVia2 option:selected").val();
                            if (temp != null && temp != '') {
                                val = val + "/" + temp;
                            }
                            temp = $("#selVia3 option:selected").val();
                            if (temp != null && temp != '') {
                                val = val + "/" + temp;
                            }
                            temp = $("#selVia4 option:selected").val();
                            if (temp != null && temp != '') {
                                val = val + "/" + temp;
                            }
                            val = val + "/" + $("select#selArrival option:selected").val();

                            if ($("select#assignedOnds option[value=\"" + val + "\"]").length == 0) {
                                $("select#assignedOnds").append("<option value=\"" + val + "\">" + val + "</option>");
                            }
                        }
                    }

                    var removeOnd = function() {
                        $("select#assignedOnds option:selected").remove();
                    }

                    var constructBaggageTemplateTo = function () {

                        var baggageTemplate = {};
                        var bookingClasses = new Array();
                        var agents = new Array();
                        var flights = new Array();
                        var onds = new Array();
                        var channels = new Array();


                        $("select#bcAssigned option").each(
                            function () {
                                baggageTemplate['baggageTemplateTo.bookingClasses[\'' + $(this).val() + '\']' ] = $(this).text();
                            });

                        var i;
                        
                        var selectedFlights=flightMultiSelect.getselectedDataAsArray();
                        for(i=0;i<selectedFlights.length;i++){
                        	 baggageTemplate['baggageTemplateTo.flights[\'' + selectedFlights[i] + '\']' ] = selectedFlights[i];
                        }
                        var ondIndex = 0;
                        $("select#assignedOnds option").each(
                            function () {
                            	
                            	baggageTemplate['baggageTemplateTo.onds[' + ondIndex++ + ']' ]=$(this).val();
                            });
                        baggageTemplate['baggageTemplateTo.templateId'] = $("#templateId").val();

                        //get sales channels
                        $("select#channelAssigned option").each(
                                function () {
                                    baggageTemplate['baggageTemplateTo.salesChannels[\'' + $(this).val() + '\']' ] = $(this).text();
                                });
                        //get agents only if agent tab visible
                        if($('#tabAgents').is(":visible")){
                        	console.log("agent tab visble");
                        	var selectedAgents = agentsMultiSelect.getselectedDataAsArray();
                            for(i = 0; i < selectedAgents.length; i++) {
                                baggageTemplate['baggageTemplateTo.agents[\'' + selectedAgents[i] + '\']' ] = selectedAgents[i];
                            }
                        }
                        
                        return baggageTemplate;
                    }

                    var loadBaggageTemplateToPane = function(rowid) {                    	
                        $("#assignedAgents").html('');
                        $("#bcAssigned").html('');
                        $("#assignedFlights").html('');
                        $("#assignedOnds").html('');
                        $("#channelAssigned").html('');
                        
                        resetSelectBox('bcAvailable', bkgClassOptions);
                        addSalesChannels();
                        
                        var mod=rowid % 20;
                        var template = null;
                        if(mod === 0){
                           template = templatesData[20 - 1].baggageTemplate;
                        }else{
                           template = templatesData[mod - 1].baggageTemplate;
                        } 
                        
                        $("#templateId").val(template.templateId);
                        $("#templateCode").val(template.templateCode);

                        var temp;
                        var ondsHtml = '';
                        var bcsHtml = '';
                        var flightsHtml = '';
                        var agentsHtml = '';
                        var channelHtml='';

                        for (temp in template.onds) {
                            ondsHtml += "<option value=\"" + template.onds[temp] + "\">" + template.onds[temp] + "</option>";
                        }
                        $('#assignedOnds').html(ondsHtml);

                        agentsMultiSelect.moveOptions("<<");
                        var agents = [];
                        for (temp in template.agents) {
                            agents.push(temp);
                        }
                        agentsMultiSelect.selectData(agents);

                        for (temp in template.bookingClasses) {
                            bcsHtml += "<option value=\"" + temp + "\">" + temp + "</option>";
                            $("select#bcAvailable option").remove("[value='" + temp + "']");
                        }
                        $("#bcAssigned").html(bcsHtml);
                        
                        flightMultiSelect.moveOptions("<<");
                        var flights=[];
                        for (temp in template.flights) {
                        	flights.push(temp)
                        }
                        flightMultiSelect.selectData(flights);
                        
                        var selectedChannels = getSelectedChannels(template.salesChannels);
                        for(var i=0;i<selectedChannels.length;i++){
                        	channelHtml += "<option value=\"" + selectedChannels[i].value + "\">" + selectedChannels[i].label + "</option>";
                        	$("select#channelAvailable option").remove("[value='" + selectedChannels[i].value + "']");
                        }
                        $("#channelAssigned").html(channelHtml);
                        
                        controlAgentTab();
                        
          
                    }
                    /**
                     * Check agent available sales channel is selected
                     */
                    var isEnableAgentTab = function () {
                    	var index=-1;
                    	var isEnable=false;
                    	$("select#channelAssigned option").each(
                        	function(i,opt){
                        		index=agentAvailableChannelCodes.indexOf(opt.value);
                        		if(index!=-1){
                        			isEnable=true;
                        		}
                        	}                        	
                        );
                    	return isEnable;
                    }
                    
                    /**
                     * Enable/disable agent tab
                     */
                    var controlAgentTab = function () {
                    	var isEnable=isEnableAgentTab();
                    	if(isEnable){
                    		//do enable
                    		$("#tabAgents").show();
                    	}else{
                    		//do disable
                    		$("#tabAgents").hide();
                    	}
                    }
                    var getSelectedChannels = function (channelCodes) {
                    	var selectedChannels=[];
                    	for (temp in channelCodes) {                    	
                    		for(var i=0;i<salesChannels.length;i++){
                        		if(salesChannels[i].value === temp){
                        			selectedChannels.push(salesChannels[i]);
                        			break;
                        		}
                        	}
                    	}
                    	return selectedChannels;
                    }
                    
                    var resetSelectBox = function(selectBox, defaultOptions){
                    	$('#' + selectBox).empty().append('<option value="ALL">**ALL**</option>');
                    	$('#' + selectBox).append(defaultOptions);
                    }

                });
