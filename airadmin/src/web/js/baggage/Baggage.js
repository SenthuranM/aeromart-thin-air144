jQuery(document)
		.ready(
				function() { // the page is ready

					$("#divSearch").decoratePanel("Search Baggages");
					$("#divResultsPanel").decoratePanel("Baggages");
					$("#divDispBaggage").decoratePanel("Add/Modify Baggage");
					$('#btnAdd').decorateButton();
					$('#btnEdit').decorateButton();
					$('#btnDelete').decorateButton();
					$('#btnClose').decorateButton();
					$('#btnReset').decorateButton();
					$('#btnSave').decorateButton();
					$('#btnSearch').decorateButton();
					$('#btnBaggageFD').decorateButton();
					disableBaggageButton();
					createPopUps();
					PopUpData();
					var grdArray = new Array();
					jQuery("#listBaggage").jqGrid(
							{
								url : 'showBaggage!searchBaggage.action',
								datatype : "json",
								jsonReader : {
									root : "rows",
									page : "page",
									total : "total",
									records : "records",
									repeatitems : false,
									id : "0"
								},
								colNames : [ '&nbsp;', 'Baggage','Weight (Kg)',
										'Description', 'IATA', 'Status',
										'baggageId', 'CabinClass', 'LogicalCabinClass', 'version',
										'createdBy', 'createdDate',
										'modifiedBy', 'modifiedDate','baggagesForDisplay'],
								colModel : [ {
									name : 'Id',
									width : 30,
									jsonmap : 'id'
								}, {
									name : 'baggageName',
									index : 'baggageName',
									width : 140,
									jsonmap : 'baggage.baggageName'
								}, {
									name : 'baggageWeight',
									index : 'baggageWeight',
									width : 60,
									jsonmap : 'baggage.baggageWeight'
								}, {
									name : 'description',
									index : 'description',
									width : 330,
									jsonmap : 'baggage.description'
								}, {
									name : 'iataCode',
									width : 60,
									align : "center",
									jsonmap : 'baggage.iataCode'
								}, {
									name : 'status',
									width : 60,
									align : "center",
									jsonmap : 'baggage.status'
								}, {
									name : 'baggageId',
									width : 30,
									align : "center",
									hidden : true,
									jsonmap : 'baggage.baggageId'
								}, {
									name : 'cabinClassCode',
									width : 100,
									hidden : true,
									align : "center",
									jsonmap : 'ccList'
								},  {
									name : 'logicalCCCode',
									width : 100,
									hidden : true,
									align : "center",
									jsonmap : 'lccList'
								}, {
									name : 'version',
									width : 225,
									align : "center",
									hidden : true,
									jsonmap : 'baggage.version'
								}, {
									name : 'createdBy',
									width : 30,
									align : "center",
									hidden : true,
									jsonmap : 'baggage.createdBy'
								}, {
									name : 'createdDate',
									width : 30,
									align : "center",
									hidden : true,
									jsonmap : 'baggage.createdDate'
								}, {
									name : 'modifiedBy',
									width : 30,
									align : "center",
									hidden : true,
									jsonmap : 'baggage.modifiedBy'
								}, {
									name : 'modifiedDate',
									width : 30,
									align : "center",
									hidden : true,
									jsonmap : 'baggage.modifiedDate'
								},{name:'baggageForDisplay', width:225, align:"center",hidden:true}								

								],
								imgpath : '../../themes/default/images/jquery',
								multiselect : false,
								pager : jQuery('#baggagepager'),
								rowNum : 20,
								viewrecords : true,
								height : 210,
								width : 930,
								loadui : 'block',
								onSelectRow : function(rowid) {
									$("#rowNo").val(rowid);
									fillForm(rowid);
									disableControls(true);
									enableSearch();
									disableBaggageButton();
									enableGridButtons();
									enablePopupControls();
									setBaggageForDisplayList();
								}
							}).navGrid("#baggagepager", {
						refresh : true,
						edit : false,
						add : false,
						del : false,
						search : false
					});

					var options = {
						cache : false,
						beforeSubmit : showRequest, // pre-submit callback -
													// this can be used to do
													// the validation part
						success : showResponse, // post-submit callback
						dataType : 'json' // 'xml', 'script', or 'json'
											// (expected server response type)
					};

					var delOptions = {
						cache : false,
						beforeSubmit : showDelete, // pre-submit callback -
													// this can be used to do
													// the validation part
						success : showResponse, // post-submit callback
						url : 'showBaggage!deleteBaggage.action', // override
																	// for
																	// form's
																	// 'action'
																	// attribute
						dataType : 'json', // 'xml', 'script', or 'json'
											// (expected server response type)
						resetForm : true
					// reset the form after successful submit

					};
					function validateBaggage(){
						var isSelectCOS = false;
						
						if (getText("hdnMode") == 'SAVELISTFORDISP'){
							return true;
						}
											
						if (trim($("#baggageName").val()) == "") {
							showCommonError("Error", baggageNameRqrd);
							return false;
						}
						if (trim($("#description").val()) == "") {
							showCommonError("Error", descriptionRqrd);
							return false;
						}

						if(!isPositiveInt(trim($("#baggageWeight").val()))) {
							showCommonError("Error","Please enter a valid Baggage Weight");
							return false;
						}


						if (trim($("#iataCode").val()) == "-1"
								|| trim($("#iataCode").val()) == "") {
							showCommonError("Error", iataCodeRqrd);
							return false;
						}
						$('#cos option').each(function(i, option){ 
							var selVal = $(this).val();
							if($('#cos option[value$="' + selVal + '"]').attr('selected')){
								isSelectCOS = true;
							}					
					    });
					    if(isSelectCOS == false){
					    	showCommonError("Error", "Class of Service Required");
					    	return false;
					    }
						return true;
					}

					// pre-submit callback
					function showRequest(formData, jqForm, options) {
						top[2].ShowProgress();
						return true;
					}

					function showResponse(responseText, statusText) {
						top[2].HideProgress();
						showCommonError(responseText['msgType'],
								responseText['succesMsg']);
						if (responseText['msgType'] != "Error") {
							$('#frmBaggage').clearForm();

							jQuery("#listBaggage").trigger("reloadGrid");
							disableBaggageButton();
							$("#btnAdd").attr('disabled', false);
							$("#frmBaggage :input").prop("disabled", true);
//							$("#btnSave").enableButton();
							alert(responseText['succesMsg']);
						}

					}

					function showDelete(formData, jqForm, options) {
						return confirm(deleteRecoredCfrm);
					}
					
					function setSelectedCOSList(rowid){
						$('#cos option').each(function(i, option){ 
							var selVal = $(this).val();
							$('#cos option[value$="' + selVal + '"]').attr('selected', false);
						});
						var cabinClass = "";
						var logocalCC = "";
						var cc = jQuery("#listBaggage").getCell(rowid,'cabinClassCode');
						var lcc = jQuery("#listBaggage").getCell(rowid,'logicalCCCode');
						if(cc != ""){
							cabinClass = cc.split(",");
						}
						if(lcc != ""){
							logocalCC = lcc.split(",");
						}
						
						$('#cos option').each(function(i, option){ 
							var selVal = $(this).val();
							var selArr = selVal.split("_");
							if(lcc != ""){
								for(var j = 0;j < logocalCC.length;j++){
									if (trim(selArr[0]) == trim(logocalCC[j])){
										$('#cos option[value$="' + selVal + '"]').attr('selected', true);
								    }
								}
							}
							if(cc != ""){
								for(var j = 0;j < cabinClass.length;j++){
									if (trim(selArr[0]) == trim(cabinClass[j])){
										$('#cos option[value$="' + selVal + '"]').attr('selected', true);
								    }
								}
							}
						});
					}
					$('#btnSave').click(
									function() {
										disableControls(false);

										if ($("#status").attr('checked')) {
											$("#status").val('ACT');
										} else {
											$("#status").val('INA');
										}

										if (getText("hdnModel") == 'ADD') {
											setField("hdnMode", "ADD");
										}

										$("#baggageWeight").val(trim($("#baggageWeight").val()));
										$("#baggagesForDisplay").val(buildBaggageForDisplayStr());
										if(validateBaggage()){
										alert("Saving Baggage changes will update Baggage on all flights. This will take some time");											
										$('#frmBaggage').ajaxSubmit(options);
										return true;
										}
										return false;
									});

					$("#cos").change(function () {				
						$("#cos option:selected").each(function () {
							var selVal = $(this).val();
							var selArr = selVal.split("_");
							if (selArr[0] == selArr[1]){
								$('#cos option[value!="' + selVal + '"][value$="_' + selArr[0] + '"]').attr('selected', false);
						    }
						});
					});
					
					$('#btnAdd').click(function() {
						disableControls(false);
						resetBaggageForm();
						$("#version").val('-1');
						$("#rowNo").val('');
						$('#btnEdit').disableButton();
						$('#btnDelete').disableButton();
						$("#btnSave").enableButton();
						setField("hdnMode", "ADD");
						setField("hdnModel", "ADD");
					});

					$('#btnEdit').click(function() {
						disableControls(false);
						$('#baggageName').attr("disabled", true);
						setSelectedCOSList($("#rowNo").val());
						$('#btnDelete').disableButton();
						$('#btnUpload').enableButton();
						$("#btnSave").enableButton();
						setField("hdnMode", "EDIT");
					});
					$('#btnReset').click(function() {
						if ($("#rowNo").val() == '') {
							resetBaggageForm();
						} else {

							fillForm($("#rowNo").val());
						}

					});
					$('#btnDelete').click(function() {
						disableControls(false);
						$('#frmBaggage').ajaxSubmit(delOptions);
						return false;
					});

					$('#btnSearch')
							.click(
									function() {
										var newUrl = 'showBaggage!searchBaggage.action?';
										newUrl += 'bagSrch.baggageName='
												+ $("#srchBaggageName").val();
										newUrl += '&bagSrch.status='
												+ $("#selStatus").val();
										newUrl += '&bagSrch.iataCode='
												+ $("#selIATACode").val();

										$("#listBaggage").clearGridData();
										$.getJSON(newUrl, function(response) {
											$("#listBaggage")[0]
													.addJSONData(response);

										});
									});

					function fillForm(rowid) {
						setField("hdnMode", "");
						jQuery("#listBaggage").GridToForm(rowid, '#frmBaggage');
						if (jQuery("#listBaggage").getCell(rowid, 'status') == 'ACT') {
							$("#status").attr('checked', true);
						} else {
							$("#status").attr('checked', false);
						}
						setSelectedCOSList(rowid);
						enableGridButtons();
					}
					function disableControls(cond) {
						$("input").each(function() {
							$(this).attr('disabled', cond);
						});
						$("textarea").each(function() {
							$(this).attr('disabled', cond);
						});
						$("select").each(function() {
							$(this).attr('disabled', cond);
						});
						$("#btnClose").enableButton();
						if (listForDisplayEnabled) {
							$("#btnBaggageFD").enableButton();
						} else {
							$('#btnBaggageFD').attr("disabled", true); 
						}

					}

					function enableSearch() {
						$('#srchBaggageName').attr("disabled", false);
						$('#selStatus').attr("disabled", false);
						$('#selIATACode').attr("disabled", false);
						$('#btnSearch').enableButton();
					}

					function disableBaggageButton() {
						$("#btnEdit").disableButton();
						$("#btnDelete").disableButton();
						$("#btnReset").disableButton();
						$("#btnSave").disableButton();
					}

					function enableGridButtons() {
						$("#btnAdd").enableButton();
						$("#btnEdit").enableButton();
						$("#btnDelete").enableButton();
						$("#btnReset").enableButton();
						if (listForDisplayEnabled) {
							$("#btnBaggageFD").enableButton();
						} else {
							$('#btnBaggageFD').attr("disabled", true); 
						}

					}

					disableControls(true);
					$("#btnAdd").enableButton();
					$('#selIATACode').attr("disabled", false);
					$('#srchBaggageName').attr("disabled", false);
					$('#selStatus').attr("disabled", false);
					$('#btnSearch').enableButton();

					function setPageEdited(isEdited) {
						top[1].objTMenu.tabPageEdited(screenId, isEdited);
					}				

					function enablePopupControls(){
						$("#language").enableButton();
						$("#add").enableButton();
						$("#remove").enableButton();
						$("#translation").enableButton();
						$("#txtBaggageForDisplayOL").enableButton();
						//$("#txtBaggageTitleForDisplayOL").enableButton();				
						$("#btnUpdate").enableButton();
						$("#btnPopupClose").enableButton();
					}

				});

					/*- Language based description -*/					
					function PopUpData(){
						var html = '<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0"> <tr><td> <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4"> <tr><td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td> <td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader"> Baggage Description For Display </font></td> <td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td> </tr><tr><td class="FormBackGround"></td><td class="FormBackGround" valign="top"> <table><tr><td width="663" align="left"><table width="98%" height="105"  border="0" cellpadding="0" cellspacing="0"> <tr align="left" valign="middle" class="fntMedium"> <td width="19%" class="fntMedium"><font>Language</font></td> <td width="33%" align="left" style="padding-bottom:3px; ">';
							html += '<select name="language" id="language"  style="width:150px; "> ';
							html += '<option value="-1"></option>'+languageHTML;				
							html += '</select></td> <td width="13%" rowspan="3" align="center" valign="top"><input name="add" type="button" class="Button" id="add" style="width:50px; " title="Add Item"  onclick="addBaggageForDisplay();" value="&gt;&gt;" /> <input name="remove" type="button" class="Button" id="remove" style="width:50px;" title="Remove Item" onclick="removeBaggageForDisplay();" value="&lt;&lt;" /></td> <td width="35%" rowspan="3" align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0"> <tr> <td><select name="translation" size="5" id="translation" style="width:200px; height:100px; "> </select></td> </tr> <tr> </tr> </table></td> </tr>  ';
							
							html += '<tr align="left" valign="middle" class="fntMedium"> <td width="25%" valign="top" class="fntMedium"><font>Baggage Description</font><font class="mandatory">&nbsp;*</font></td> <td valign="top"><input name="txtBaggageForDisplayOL" type="text" id="txtBaggageForDisplayOL" size="32" maxlength="125" /></td> </tr>'; 
							html +=	'</table></td> </tr><tr><td align="top"><span id="spnSta"></span></td></tr> <tr><td align="right"><input type="button" id="btnUpdate" value= "Save" class="Button" onClick="saveBaggageForDisplay();"> &nbsp;<input type="button" id="btnPopupClose" value= "Close" class="Button" onClick="return POSdisable(true);"></td></tr></table> </td><td class="FormBackGround"></td></tr><tr> <td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td> <td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td> <td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td> </tr></table> </td></tr></table>';

						DivWrite("spnPopupData",html);
					}

					function POSenable(){			
						if(!listForDisplayEnabled)
							return;
						if(getFieldByID("description").value=="" || getFieldByID("description").value==null){
								alert("No Baggage is selected, please select Baggage and try again");
								return;
							}
						
						if(getValue('hdnModel') == 'ADD'){
							var baggageList = getFieldByID("translation");
							baggageList.options.length = 0;
							setField("txtBaggageForDisplayOL", "");
							//setField("txtBaggageTitleForDisplayOL", "");
							setField("language","-1");
						}
						
						setField("hdnMode", "SAVELISTFORDISP");
						//setBaggageForDisplayList();
						showPopUp(1);
					}

					function POSdisable(bFromClose){			
						if(!listForDisplayEnabled)
							return;
						if(bFromClose){
							//setField("hdnModel","");
							setField("hdnMode","");
						}
						 hidePopUp(1);
					}


					function addBaggageForDisplay(){			
						if(!listForDisplayEnabled)
							return;
						if(getValue('txtBaggageForDisplayOL').length ==0){
							return false;
						}
						var selLang = getValue("language");
						if(selLang == '-1')
							return false;
						if(findValueInList(selLang, "translation") == true){
							alert("Baggage description for the selected language already exist");
							return false;
						}

						var selLangName = getCurSelectedLabel("language");
						var baggageDesc = getValue("txtBaggageForDisplayOL");
						//var baggageName = getValue("txtBaggageTitleForDisplayOL");						
						//addToList("translation", selLangName+"=>"+baggageName, selLang);
						addToList("translation", selLangName+"=>"+baggageDesc, selLang);
						setField("txtBaggageForDisplayOL", "");
						//setField("txtBaggageTitleForDisplayOL", "");
						setField("language","-1");

					}
					
					function removeBaggageForDisplay(){			
						if(!listForDisplayEnabled)
							return;
						var baggageList = getFieldByID("translation");
						var selected = baggageList.selectedIndex;
						if(selected != -1){
							var langCode = baggageList.options[selected].value;
							var label = baggageList.options[selected].text.split("=>");
							setField("language", langCode);
							setField("txtBaggageForDisplayOL", label[1]);
							//setField("txtBaggageTitleForDisplayOL", label[1]);
							baggageList.remove(selected);
						}
					}
					
					function saveBaggageForDisplay(){			
						if(!listForDisplayEnabled)
							return;
						if(getValue('description').length>0 && getValue("language")!='-1'){
							if(confirm("The current edited language is not added to the list, do you want to continue?")==false)
								return false;
						}
						var strMode = getText("hdnMode");
						var baggageList = getFieldByID("translation");
						var size = baggageList.length;
						var langCode;						
						var baggageName;
						var baggageDesc;
						var label;
						var str="";
						var size;
						var i=0;
						var j=0;
						var newData = new Array();
						var count = 0;
					
						var baggageTranslations = jQuery("#listBaggage").getCell($("#rowNo").val(),'baggageForDisplay');
						
						var arrBaggageTrl = new Array();
						if(baggageTranslations != null && baggageTranslations != "")
							arrBaggageTrl = baggageTranslations.split("~");
						
						var baggageTrl;
						grdArray = new Array();
						
						for(var i =0;i<arrBaggageTrl.length-1;i++){
							grdArray[i] = new Array(); 
							baggageTrl = arrBaggageTrl[i].split(",");
							grdArray[i][0] = baggageTrl[0];
							grdArray[i][1] = baggageTrl[1];
							grdArray[i][2] = baggageTrl[2];							
						}
						
						for(i=0;i<baggageList.length;i++){
							langCode = baggageList.options[i].value;
							label = baggageList.options[i].text.split("=>");
							//baggageName = label[1];
							if (label.length > 1){
								baggageName = label[1];
								baggageDesc = label[1];
							}else{
								baggageName = getFieldByID("baggageName");
								baggageDesc = getFieldByID("description");
							}		
							
								
							size = grdArray.length;
							var found = false;
							for(j=0;j<size;j++){
								if(grdArray[j][0] == langCode){									
									newData[count++] = new Array(grdArray[j][0],langCode,baggageDesc);
									found = true;
									break;
								}
							}
							if(!found){								
								newData[count++] = new Array("-1",langCode,baggageDesc);								
							}
						}
						var langCode2;
						var size1 = grdArray.length;
						var size2 = newData.length;
						for(i=0;i<size1;i++){//original list
							found = false;
							for(j=0;j<size2;j++){//new data
								langCode = grdArray[i][1];
								langCode2 = newData[j][1];
								if(langCode == langCode2){//the lang code exist in the new data
									found = true;
								}
							}
							if(!found){								
								newData[newData.length] = new Array( grdArray[i][0],grdArray[i][1],baggageDesc);
							}
						}
						grdArray = newData;

						if(validateBaggageForDisplay() == false){
							return false;
						}
						$('#btnSave').click();
						return POSdisable(false);
					}
					
					function setBaggageForDisplayList(){			
						if(!listForDisplayEnabled)
							return;

						if(jQuery("#listBaggage").getCell($("#rowNo").val())>=0 ){
							setField("txtBaggageForDisplayOL", "");
							setField("language","-1");
							//setField("txtBaggageTitleForDisplayOL", "");						

							var baggageList = getFieldByID("translation");	
							baggageList.options.length = 0;
							var langCode ;
							var baggageName ;
							var baggageDesc;
							var langName;
							
							var i=0;
							
							var baggageTranslations = jQuery("#listBaggage").getCell($("#rowNo").val(),'baggageForDisplay');
							
							var arrBaggageTrl = new Array();
							if(baggageTranslations != null && baggageTranslations != "")
								arrBaggageTrl = baggageTranslations.split("~");
							var baggageTrl;
							grdArray = new Array();
							for(var i =0;i<arrBaggageTrl.length-1;i++){
								grdArray[i] = new Array(); 
								baggageTrl = arrBaggageTrl[i].split(",");
								grdArray[i][0] = baggageTrl[0]; //id
								grdArray[i][1] = baggageTrl[1]; //langcode
								grdArray[i][2] = baggageTrl[2]; //desc								
							}
							for(i=0;i<grdArray.length;i++){
								langCode = grdArray[i][1];
								baggageName = grdArray[i][2];
								baggageDesc = grdArray[i][2];

								if(baggageDesc == "null")
									continue;
								langName = getListLabel("language", langCode);
								addToList("translation",langName+"=>"+baggageDesc,langCode);
							}
						}
					}
					
					function validateBaggageForDisplay(){
						if(!listForDisplayEnabled)
							return;
						var strMode = getText("hdnMode");
						var size2=getFieldByID("language").length;
						var valid = true;
						var size1;
						var i;
						var j;
						var langCode;						
						var baggageName;
						var baggageDesc;
						var found = false;
						if(strMode == "ADD")
							valid = false;
						else{
							size1 = grdArray.length;
							if(size1 != (size2-1))//-1 to ignore the first empty item
								valid = false;
							else{
								for(i=0;i<size1;i++){//original list
									baggageName = grdArray[i][1];
									baggageDesc = grdArray[i][1];
									if(baggageDesc == "null"){
										valid = false;
										break;
									}
								}
							}
						}
						if(!valid){
							alert("Baggage description for display is missing for all or some languages, original baggage description will copied to all languages") ;
							if(strMode != "ADD"){

									var descr = getValue("description");
									var title = getValue("baggageName");

									for(i=1;i<size2;i++){//lang  list, start from 1 to ignore the first item (empty)
										found = false;
										langCode = getFieldByID("language").options[i].value;
										for(j=0;j<size1;j++){
											baggageName = grdArray[j][2];
											baggageDesc = grdArray[j][2];
											if(baggageName == "null"){
												grdArray[j][2] = descr;
												//grdArray[j][2] = title;
											}
											if(langCode == grdArray[j][1]){
												found = true;
												continue;
											}
										}
										
										if(!found){
											grdArray[grdArray.length] = new Array("-1",langCode,descr);
										}

											
									}
							}
						}
						return true;
						}
					
					
					function buildBaggageForDisplayStr(){		
						//ltid,langCode,lt	
						var strMode = getText("hdnMode");
						var baggageStr = "";
						
						if(strMode == "ADD"){													

							baggageName = getValue("baggageName");
							baggageDesc = getValue("description");
							
							var size2=getFieldByID("language").length;
							for(i=1;i<size2;i++){
								langCode = getFieldByID("language").options[i].value;
								
								if(baggageStr.length>0)
									baggageStr += "~";

								baggageStr=baggageStr +"-1,"+ langCode+","+baggageDesc;
							}
						}else {
							prepareBaggageToSave();
							if(grdArray != null){					
								for(var j=0;j<grdArray.length;j++){
									baggageStr += grdArray[j][0]+","; //id,langcode,desc
									baggageStr += grdArray[j][1]+",";
									baggageStr += grdArray[j][2];									
									baggageStr += "~";	
							   	}
							}	
						}	
						//alert(baggageStr);	

						return baggageStr;
					}
					
					function prepareBaggageToSave(){
						var strMode = getText("hdnMode");
						var baggageList = getFieldByID("translation");
						var size = baggageList.length;
						var langCode;
						var baggageName;
						var baggageDesc;
						var label;
						var str="";
						var size;
						var i=0;
						var j=0;
						var newData = new Array();
						var count = 0;
						
						var baggageTranslations = jQuery("#listBaggage").getCell($("#rowNo").val(),'baggageForDisplay');
						var arrBaggageTrl = new Array();
						if(baggageTranslations != null && baggageTranslations != "")
							arrBaggageTrl = baggageTranslations.split("~");
						var baggageTrl;
						grdArray = new Array();						
						for(var i =0;i<arrBaggageTrl.length-1;i++){
							grdArray[i] = new Array(); 
							baggageTrl = arrBaggageTrl[i].split(",");
							grdArray[i][0] = baggageTrl[0];  //id
							grdArray[i][1] = baggageTrl[1];  //langcode
							grdArray[i][2] = baggageTrl[2];  //desc					
										
						}
						////////////
						for(i=0;i<baggageList.length;i++){
							langCode = baggageList.options[i].value;
							label = baggageList.options[i].text.split("=>");
							
							if (label.length > 1 && label[1]!=""){
								baggageName = label[1];
								baggageDesc = label[1];
							}else{
								baggageName = getValue("baggageName");
								baggageDesc = getValue("description");
							}
								
							size = grdArray.length;
							var found = false;
							for(j=0;j<size;j++){
								if(grdArray[j][1] == langCode){									
									newData[count++] = new Array(grdArray[j][0],langCode,baggageDesc);
									found = true;
									break;
								}
							}
							if(!found){								
								newData[count++] = new Array( "-1",langCode,baggageDesc);
								
							}
						}
						var langCode2;
						var size1 = grdArray.length;
						var size2 = newData.length;
						for(i=0;i<size1;i++){//original list
							found = false;
							for(j=0;j<size2;j++){//new data
								langCode = grdArray[i][1];
								langCode2 = newData[j][1];
								if(langCode == langCode2){//the lang code exist in the new data
									found = true;
								}
							}
							if(!found){								
								newData[newData.length] = new Array(grdArray[i][0],grdArray[i][1],getValue("description"));
							}
						}
						grdArray = newData;		
						
					}
					
					function resetBaggageForm(){
						$('#frmBaggage').each(function(){
					        this.reset();
						});
					}
					
					/*----- End of -----*/
