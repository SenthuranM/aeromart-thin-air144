var selectedBaggage = '';
var baggageMandatory = null;
var baggageCheck = new Array();

jQuery(document)
		.ready(
				function() { // the page is ready

					var grdArray = new Array();

					$("#divSearchPanel").decoratePanel("Search Templates");
					$("#divDispBaggageTemp").decoratePanel(
							"Add/Modify Baggage Template");
					$("#divResultsPanel").decoratePanel("Baggage Templates");
					$('#btnAdd').decorateButton();
					$('#btnEdit').decorateButton();
					$('#btnDelete').decorateButton();
					$('#btnClose').decorateButton();
					$('#btnReset').decorateButton();
					$('#btnSave').decorateButton();
					$('#btnSearch').decorateButton();
					$('#btnChgAdd').decorateButton();
					$('#btnChgEdit').decorateButton();
					enableGridButtons(false);
					enbleFormButtons(false);
					jQuery("#listBaggageTemp")
							.jqGrid(
									{
										url : 'showBaggageTemplate!searchBaggageTemplate.action?selStatus='
												+ $("#selStatus").val(),
										datatype : "json",
										jsonReader : {
											root : "rows",
											page : "page",
											total : "total",
											records : "records",
											repeatitems : false,
											id : "0",
											userdata : "baggageMandatory"
										},
										colNames : [ '&nbsp;', 'Template Id',
												'Template Code', 'Description',
												'Status', 'baggageCharges',
												'Version', 'createdBy',
												'createdDate',
												'ChargeLocalCurrencyCode'],
										colModel : [
												{
													name : 'id',
													width : 40,
													jsonmap : 'id'
												},
												{
													name : 'templateId',
													width : 125,
													jsonmap : 'baggageTemplate.templateId'
												},
												{
													name : 'templateCode',
													width : 175,
													jsonmap : 'baggageTemplate.templateCode'
												},
												{
													name : 'description',
													width : 275,
													jsonmap : 'baggageTemplate.description'
												},
												{
													name : 'status',
													width : 150,
													align : "center",
													jsonmap : 'baggageTemplate.status'
												},
												{
													name : 'baggageCharge',
													width : 225,
													align : "center",
													hidden : true
												},
												{
													name : 'version',
													jsonmap : 'baggageTemplate.version',
													width : 225,
													align : "center",
													hidden : true
												},
												{
													name : 'createdBy',
													jsonmap : 'baggageTemplate.createdBy',
													width : 225,
													align : "center",
													hidden : true
												},
												{
													name : 'createdDate',
													jsonmap : 'baggageTemplate.createdDate',
													width : 225,
													align : "center",
													hidden : true
												},
												{
													name : 'chargeLocalCurrencyCode',
													jsonmap : 'baggageTemplate.chargeLocalCurrencyCode',
													width : 150,
													align : "center",
													hidden : true
												}

										],
										imgpath : '../../themes/default/images/jquery',
										multiselect : false,
										pager : jQuery('#temppager'),
										rowNum : 20,
										viewrecords : true,
										height : 175,
										width : 925,
										loadui : 'block',
										onSelectRow : function(rowid) {
											if (isPageEdited()) {
												$('#frmBaggage').clearForm();
												hideMessage();
												setPageEdited(false);
												$("#rowNo").val(rowid);
												fillForm(rowid);
												disableControls(true);
												enableSearch();
												enableGridButtons(true);
												enbleFormButtons(false);
											}
											baggageMandatory = jQuery("#listBaggageTemp").getGridParam('userdata');
										}
									}).navGrid("#temppager", {
								refresh : true,
								edit : false,
								add : false,
								del : false,
								search : false
							});

					var options = {
						cache : false,
						beforeSubmit : showRequest, // pre-submit callback -
						// this can be used to do
						// the validation part
						success : showResponse, // post-submit callback
						dataType : 'json' // 'xml', 'script', or 'json'
					// (expected server response type)

					};

					var delOptions = {
						cache : false,
						beforeSubmit : showDelete, // pre-submit callback -
						// this can be used to do
						// the validation part
						success : showResponse, // post-submit callback
						url : 'showBaggageTemplate!deleteTemplate.action', // override
						// for
						// form's
						// 'action'
						// attribute
						dataType : 'json', // 'xml', 'script', or 'json'
						// (expected server response type)
						resetForm : true
					// reset the form after successful submit

					};

					function fillForm(rowid) {
						jQuery("#listBaggageTemp").GridToForm(rowid,
								"#frmBaggage");
						if (jQuery("#listBaggageTemp").getCell(rowid, 'status') == 'ACT') {
							$("#status").attr('checked', true);
						} else {
							$("#status").attr('checked', false);
						}						
						code = jQuery("#listBaggageTemp").getCell(rowid,'chargeLocalCurrencyCode');
						$("#localCurrCode").val(code);
						$("#lblLocalCurrencyCode").html(code);
						
						var charge = jQuery("#listBaggageTemp").getCell(rowid,
								'baggageCharge');
						var arrCharge = charge.split("~");
						var mlcharge;
						grdArray = new Array();

						for ( var i = 0; i < arrCharge.length - 1; i++) {
							grdArray[i] = new Array();
							mlcharge = arrCharge[i].split(",");
							grdArray[i]["recId"] = i + 1;
							grdArray[i]["baggage"] = mlcharge[0];
							grdArray[i]["chargeId"] = mlcharge[1];
							grdArray[i]["templateId"] = mlcharge[2];
							grdArray[i]["baggageId"] = mlcharge[3];
							grdArray[i]["bagAmount"] = mlcharge[4];
							grdArray[i]["allocPieces"] = mlcharge[5];
							grdArray[i]["bagVersion"] = mlcharge[6];
							grdArray[i]["bagStatus"] = mlcharge[7];
							grdArray[i]["cosDescription"] = mlcharge[8];
							grdArray[i]["cabinAndLogicalCC"] = mlcharge[9];
							grdArray[i]["defaultBaggage"] = mlcharge[10];
						}
						populateBaggageGrid();

					}

					function validateBaggage(){
						if (trim($("#templateCode").val()) == "") {
							showCommonError("Error", tempCodeRqrd);
							return false;
						}
						
						if (trim($("#description").val()) == "") {
							showCommonError("Error", descriptionRqrd);
							return false;
						}
						if(!isDefaultSet()){
							showCommonError("Error",defBaggRqrd);
							return false;
						}
						if(!isDefaultBaggageActive()){
							showCommonError("Error",defBaggAct);
							return false;
						}
						
						if($("#localCurrCode").val() == null){
							showCommonError("Error", localCurrencyCodeRqrd);
							return false;
						}
						
						return true;
					}
					
					// pre-submit callback
					function showRequest(formData, jqForm, options) {
						if (trim($("#baggageCharges").val()) == "") {
							showCommonError("Error", baggageRqrd);
							return false;
						}				

						top[2].ShowProgress();
						return true;
					}

					function showResponse(responseText, statusText) {
						top[2].HideProgress();
						showCommonError(responseText['msgType'],
								responseText['succesMsg']);
						if (responseText['msgType'] != "Error") {
							alert(responseText['succesMsg']);
							setPageEdited(false);
							$('#frmBaggage').clearForm();
							jQuery("#listBaggageChg").clearGridData();
							jQuery("#listBaggageTemp").trigger("reloadGrid");
							enableGridButtons(false);
							enbleFormButtons(false);
							$("select#selTemplate").html(
									responseText['templOption']);

						}

					}

					function showDelete(formData, jqForm, options) {
						return confirm(deleteRecoredCfrm);
					}
					$('#btnSave').click(function() {
						hideMessage();
						disableControls(false);
						if ($("#status").attr('checked')) {
							$("#status").val('ACT');
						} else {
							$("#status").val('INA');
						}
						if(validateBaggage()){
							alert("Saving Baggage Template will take some time.....");						

							$("#baggageCharges").val(buildChargeStr());
							$('#templateCode').attr('disabled', false);
							$('#frmBaggage').ajaxSubmit(options);
							return true;
						}						
						return false;
					});

					function buildChargeStr() {
						var chargStr = "";
						for ( var j = 0; j < grdArray.length; j++) {
							chargStr += grdArray[j]["recId"] + ",";
							chargStr += grdArray[j]["baggage"] + ",";
							chargStr += grdArray[j]["chargeId"] + ",";
							chargStr += grdArray[j]["templateId"] + ",";
							chargStr += grdArray[j]["baggageId"] + ",";
							chargStr += grdArray[j]["bagAmount"] + ",";
							chargStr += grdArray[j]["allocPieces"] + ",";
							chargStr += grdArray[j]["bagVersion"] + ",";
							chargStr += grdArray[j]["bagStatus"] + ",";
							chargStr += grdArray[j]["cabinAndLogicalCC"] + ",";
							chargStr += grdArray[j]["defaultBaggage"];
							chargStr += "~";
						}
						return chargStr;
					}
					
					$('#cos').change(function(){
						loadBaggageList('false',"","");
					});
					
					function loadBaggageList(condition,baggageId,baggageValue) {
						var data = {};				
						data["isGridBaggage"] = condition;
						data["selBaggageId"] = baggageId;
						data["baggageValue"] = baggageValue;
						
						if (condition == "true") {
							$("#selBaggage").attr('disabled', true); 
						} else {
							$("#selBaggage").attr('disabled', false); 
						}
						
						$("#frmBaggage").ajaxSubmit({
						dataType: 'json', 
						data:data, 
						url:"showBaggageTemplate!getBaggagesForCabinClass.action",							
						success: loadSuccess});			
					}
					
					loadSuccess = function(response){
						if(response.baggageMandatory != null){
							baggageMandatory = response.baggageMandatory;
						}
						if (response.baggageForCC != null && trim(response.baggageForCC) != "") {
							$("#selBaggage").html(response.baggageForCC);
						}else {
							alert("No Baggages for selected cabin class");
							$("#selBaggage").html("<option value=''>");
							$("#selBaggage").attr('disabled', true); 
						}
					}

					$('#btnSearch')
							.click(
									function() {
										hideMessage();
										var strUrl = "showBaggageTemplate!searchBaggageTemplate.action?selTemplate="
												+ $("#selTemplate").val()
												+ "&selStatus="
												+ $("#selStatus").val();
										jQuery("#listBaggageTemp")
												.setGridParam({
													url : strUrl
												});
										jQuery("#listBaggageTemp").trigger(
												"reloadGrid");
										enbleFormButtons(false);
										enableGridButtons(false);
										$('#frmBaggage').clearForm();
										$("#version").val('-1');
										jQuery("#listBaggageChg")
												.clearGridData();
										grdArray = new Array();
										disableControls(true);
										enableSearch();
										$("#btnAdd").attr('disabled', false);
									});

					$('#btnAdd').click(function() {
						hideMessage();
						disableControls(false);
						$('#frmBaggage').clearForm();
						$("#version").val('-1');
						jQuery("#listBaggageChg").clearGridData();
						grdArray = new Array();
						enbleFormButtons(true);
					});

					$('#btnEdit').click(function() {
						hideMessage();
						disableControls(false);

						var baggageRow = $("#selBaggage").val();

						if (baggageRow != null) {
							$("#cos").attr('disabled', true);
							$("#selBaggage").attr('disabled', true);
						}

						$('#templateCode').attr('disabled', true);
						enbleFormButtons(true);
					});
					$('#btnReset').click(function() {
						hideMessage();

						$("#selBaggage").val('');
						$("#amount").val('');
						$("#mstatus").val('');
						$("#mdefault").val('');
						
						selectedBaggage = '';
						if ($("#rowNo").val() == '') {
							grdArray = new Array();
							$("#templateCode").val('');
							$("#description").val('');
							$("#status").val('');

							// $('#frmBaggage').resetForm();
							jQuery("#listBaggageChg").clearGridData();
						} else {
							fillForm($("#rowNo").val());
						}

					});
					$('#btnDelete').click(function() {
						disableControls(false);
						$('#frmBaggage').ajaxSubmit(delOptions);
						return false;
					});

					function disableControls(cond) {
						$("input").each(function() {
							$(this).attr('disabled', cond);
						});
						$("textarea").each(function() {
							$(this).attr('disabled', cond);
						});
						$("select").each(function() {
							$(this).attr('disabled', cond);
						});
						$("#btnClose").attr('disabled', false);
					}

					function enableGridButtons(cond) {
						if (cond) {
							$("#btnEdit").enableButton();
							$("#btnDelete").enableButton();
						} else {
							$("#btnEdit").disableButton();
							$("#btnDelete").disableButton();
						}

					}

					function enbleFormButtons(cond) {
						if (cond) {
							$("#btnReset").enableButton();
							$("#btnSave").enableButton();
						} else {
							$("#btnReset").disableButton();
							$("#btnSave").disableButton();
						}

					}

					function enableSearch() {
						$("#btnSearch").attr('disabled', false);
						$("#selTemplate").attr('disabled', false);
						$("#selStatus").attr('disabled', false);
					}

					disableControls(true);
					$("#btnAdd").attr('disabled', false);
					enableSearch();
					jQuery("#listBaggageChg")
							.jqGrid(
									{
										datatype : 'clientSide',
										// bag-name,charge-id,template-id,bag-id,amount,alloc-pieces,vers,status,cabin-class
										colNames : [ '&nbsp;', 'Baggage',
												'chargeId', 'templateId',
												'baggageId', 'Amount',
												'Pieces', 'version', 'Status',
												'COS', 'cabinAndLogicalCC', 'Default' ],
										colModel : [ {
											name : 'recId',
											index : 'recId',
											width : 40,
											sorttype : 'int'
										}, {
											name : 'baggage',
											index : 'baggage',
											width : 150
										}, {
											name : 'chargeId',
											index : 'chargeId',
											width : 40,
											sorttype : 'int',
											hidden : true
										}, {
											name : 'templateId',
											index : 'templateId',
											width : 40,
											sorttype : 'int',
											hidden : true
										}, {
											name : 'baggageId',
											index : 'baggageId',
											width : 40,
											sorttype : 'int',
											hidden : true
										}, {
											name : 'bagAmount',
											index : 'bagAmount',
											width : 140
										}, {
											name : 'allocPieces',
											index : 'allocPieces',
											width : 100,
											hidden : true
										}, {
											name : 'bagVersion',
											index : 'bagVersion',
											width : 40,
											sorttype : 'int',
											hidden : true
										}, {
											name : 'bagStatus',
											index : 'bagStatus',
											align : "right",
											width : 90,
											sortable : false
										}, {
											name : 'cosDescription',
											index : 'cosDescription',
											width : 180
										} , {name:'cabinAndLogicalCC', width:225, align:"center",hidden:true},
										{
											name : 'defaultBaggage',
											index : 'defaultBaggage',
											width : 120
										} ],
										viewrecords : true,
										imgpath : '../../themes/default/images/jquery',
										multiselect : false,
										loadui : 'block',
										height : 100,
										onSelectRow : function(rowid) {

											selectedBaggage = rowid;
											hideMessage();
											
											$("#selBaggage").attr('disabled',
													false);

											$("#selBaggage")
													.val(
															jQuery(
																	"#listBaggageChg")
																	.getCell(
																			rowid,
																			'baggageId'));
											
											var sltdbaggage = jQuery("#listBaggageChg").getCell(rowid,'baggageId')
											$("#selBaggage").val(sltdbaggage);
											loadBaggageList('true',sltdbaggage,jQuery("#listBaggageChg").getCell(rowid,'baggage'));
											
											
											$("#amount")
													.val(
															jQuery(
																	"#listBaggageChg")
																	.getCell(
																			rowid,
																			'bagAmount'));
											$("#cabinClassCode")
													.val(
															jQuery(
																	"#listBaggageChg")
																	.getCell(
																			rowid,
																			'cabinClass'));
											$("#mstatus")
													.val(
															jQuery(
																	"#listBaggageChg")
																	.getCell(
																			rowid,
																			'bagStatus'));										
											
											var classOfService = jQuery("#listBaggageChg").getCell(rowid,'cabinAndLogicalCC');
								    		$('#cos option').each(function(i, option){ 
												var selVal = $(this).val();
												if (trim(selVal) == trim(classOfService)){
													$('#cos option[value$="' + selVal + '"]').attr('selected', true);
												}				
											});
											if (jQuery("#listBaggageChg").getCell(rowid, 'defaultBaggage') == 'Y') {
												$("#mdefault").attr('checked', true);
											} else {
												$("#mdefault").attr('checked', false);
											}
											
											
											$("#selBaggage").attr('disabled',
													true);
											$("#cos").attr(
													'disabled', true);
										}

									});

					$('#btnChgAdd')
							.click(
									function() {

										var selBaggText = $(
												"#selBaggage option:selected")
												.text();
										var selBaggId = $("#selBaggage").val();

										var selCCText = $(
												"#cos option:selected")
												.text();
										var selCCId = $("#cos")
												.val();

										var bagAmount = $("#amount").val();
										var bagStatus = $("#mstatus").val();
											
										

										$("#cos").attr('disabled',
												false);
										$("#selBaggage")
												.attr('disabled', false);
										$("#mstatus").attr('disabled', false);
										$("#mdefault").attr('disabled', false);

										if ((selCCId == null)
												|| (trim(selCCId) == "")) {
											showCommonError("Error", ccRqrd);
											return false;
										} else if ((selBaggText == null)
												|| (trim(selBaggText) == "")) {
											showCommonError("Error",
													baggageRqrd);
											return false;
										} else if ((isNaN(parseFloat(trim(bagAmount))))
												|| (trim(bagAmount) < 0)) {
											showCommonError("Error", baggageAmt);
											return false;
										} else if (($("#mstatus").val() == null)
												|| (trim($("#mstatus").val()) == "")) {
											showCommonError("Error",
													baggageStatus);
											return false;
										} else if (!checkForBaggage(false)) {
											if(baggageCheck[0] == "existBaggage"){
												showCommonError("Error",
														baggageExst);
											}											
											else if(baggageCheck[0] == "defaultBaggage"){
												showCommonError("Error","Deafault Baggage Already Defined");
											}
											return false;
										} else {

											setPageEdited(true);
											var msize = grdArray.length;

											var defaultBaggage = 'N';

											if ($("#mdefault").attr('checked')) {
												defaultBaggage = 'Y';
											} 

											grdArray[msize] = new Array();
											grdArray[msize]["recId"] = msize + 1;
											grdArray[msize]["baggage"] = selBaggText;
											grdArray[msize]["chargeId"] = '';
											grdArray[msize]["templateId"] = trim($(
													"#templateId").val());
											grdArray[msize]["baggageId"] = trim(selBaggId);
											grdArray[msize]["bagAmount"] = bagAmount;
											grdArray[msize]["allocPieces"] = '100';
											grdArray[msize]["bagVersion"] = -1;
											grdArray[msize]["bagStatus"] = bagStatus;
											grdArray[msize]["defaultBaggage"] = defaultBaggage;
											grdArray[msize]["cabinAndLogicalCC"] = trim($("#cos").val());
											if(trim(selCCId.split("_")[0]) == trim(selCCId.split("_")[1])){
												grdArray[msize]["cosDescription"] = trim($("#cos option:selected").text());
											}else{
												grdArray[msize]["cosDescription"] = trim($("#cos option:selected").text()).substring(2);
											}
											jQuery("#listBaggageChg")
													.addRowData(msize + 1,
															grdArray[msize]);

								
											$("#selBaggage").val('');
											$("#amount").val('');
											$("#mstatus").val('');
											$("#mdefault").val('');	
										}
									});

					function checkForBaggage(isUpdate) {
						var isDefaultBaggage = "";
						var cabinAndLogicalCC = $("#cos").val();
						var classOfService = cabinAndLogicalCC.split("_");
						var selectedBaggageLcc = classOfService[0];
						var selectedBaggageCabin = classOfService[1];
						
						if($("#mdefault").attr('checked')){
							isDefaultBaggage = "Y";
						}
						
						var baggageArr = trim($("#selBaggage").val())
								.split(",");
						for ( var i = 0; i < grdArray.length; i++) {
							if(!isUpdate) //Run this code if its not an update. (Is a new add)
							{
								var gridCabinAndLogicalCC = grdArray[i]["cabinAndLogicalCC"];
								var gridCosArr = gridCabinAndLogicalCC.split('_');
								var gridBaggageLcc = gridCosArr[0];
								var gridbaggabeCabin = gridCosArr[1];
								
								if(grdArray[i]["baggageId"] == baggageArr[0]){
									if(gridCabinAndLogicalCC == cabinAndLogicalCC){
										baggageCheck[0] = "existBaggage";
										return false;
									} else if(selectedBaggageCabin == gridbaggabeCabin){
										var selBaggageLastChar = selectedBaggageLcc.substr(-1);
										var gridBaggageLastChar = gridBaggageLcc.substr(-1);
										if((selBaggageLastChar == '^' && gridBaggageLastChar != '^')
												|| (selBaggageLastChar != '^' && gridBaggageLastChar == '^')){
											baggageCheck[0] = "existBaggage";
											return false;
										}
									}
								}
							}
							if(isDefaultBaggage == "Y"){
								/*
								 * Combo box values are set as LogicalCabinClass.logical_cabin_class_code^-CabinClassCode.cabin_class_code
								 * for sub elements and CabinClassCode.cabin_class_code-CabinClassCode.cabin_class_code for parent
								 * elements.
								 */
								var grdCos = grdArray[i]["cabinAndLogicalCC"].split("_");
								if(grdCos[1] == classOfService[1] && grdArray[i]["defaultBaggage"] == isDefaultBaggage){
									baggageCheck[0] = "defaultBaggage";
									return false;
								}
								else if(((grdCos[0] == classOfService[0]) || (grdCos[0] == classOfService[1]) ||
										(classOfService[0] == classOfService[1] && grdCos[1] == classOfService[1])) && 
										grdArray[i]["defaultBaggage"] == isDefaultBaggage){
									baggageCheck[0] = "defaultBaggage";
									return false;
								}
							}
						}
						return true;
					}

					$('#btnChgEdit').click(
							function() {

								var bagAmount = $("#amount").val();
								var bagStatus = $("#mstatus").val();
								

								if (selectedBaggage == null
										|| selectedBaggage == '') {
									return;
								} else if ((isNaN(parseFloat(trim(bagAmount))))
										|| (trim(bagAmount) < 0)) {
									showCommonError("Error", baggageAmt);
									return false;
								} else if ((bagStatus == null)
										|| (trim(bagStatus) == "")) {
									showCommonError("Error", baggageStatus);
									return false;
								}else if (!checkForBaggage(true)) {
									if(baggageCheck[0] == "defaultBaggage"){
										showCommonError("Error","Deafault Baggage Already Defined");
										return false;
									}									
								}
								//$("#btnChgEdit").attr('disabled', true); 
								$("#cos").attr('disabled', false);  
								var inId = jQuery("#listBaggageChg").getGridParam("selrow");
								
								if (inId == null || inId == '') {
									return;
								}								

								setPageEdited(true);
								
								var defaultBaggage = 'N';

								if ($("#mdefault").attr('checked')) {
									defaultBaggage = 'Y';
								} 

								grdArray[inId - 1]["bagAmount"] = parseFloat(bagAmount);
								grdArray[inId - 1]["bagStatus"] = bagStatus;
								grdArray[inId - 1]["defaultBaggage"] = defaultBaggage;
								
								jQuery("#listBaggageChg").setRowData(inId, {
									bagAmount : parseFloat(bagAmount),
									bagStatus : bagStatus,
									defaultBaggage : defaultBaggage
								});
							});

					function populateBaggageGrid() {
						jQuery("#listBaggageChg").clearGridData();
						for ( var i = 0; i < grdArray.length; i++) {
							jQuery("#listBaggageChg").addRowData(i + 1,
									grdArray[i]);
						}
					}

					function isDefaultSet(){
						for ( var i = 0; i < grdArray.length; i++) {
							if(jQuery("#listBaggageChg").getCell((i+1), 'defaultBaggage')=='Y' || (baggageMandatory != null && baggageMandatory == false)){
								return true;
							}
						}				
						return false;	
					}

					function isDefaultBaggageActive(){
						for ( var i = 0; i < grdArray.length; i++) {
							if((jQuery("#listBaggageChg").getCell((i+1), 'defaultBaggage')=='Y' && jQuery("#listBaggageChg").getCell((i+1), 'bagStatus') == 'ACT') || (baggageMandatory != null && baggageMandatory == false)){
								return true;
							}
						}				
						return false;	
					}

					

					function setPageEdited(isEdited) {
						top[1].objTMenu.tabPageEdited(screenId, isEdited);
						hideMessage();
					}

					function isPageEdited() {
						return top.loadCheck(top[1].objTMenu
								.tabGetPageEidted(screenId));
					}
					function hideMessage() {
						top[2].HidePageMessage();
					}

				});