$(document).ready(function(){
	UI_invTemplates.ready();
});

function UI_invTemplates(){
	
}
UI_invTemplates.selAirCraftModel ;
UI_invTemplates.ready = function (){
	
	$("#divSearch").decoratePanel("Search AirCraft Model");
	$("#divResultsPanel").decoratePanel("Inventory Templates");
	$('#btnbtnAddRWIvn').decorateButton();
	$('#btnEdit').decorateButton();
	$('#btnbtnAddDefaultIvn').decorateButton();
	$('#btnSearch').decorateButton();
	$("#btnAddRWIvn").click(function(){UI_invTemplates.addRWInvTemp();});
	$("#btnAddDefaultIvn").click(function(){UI_invTemplates.addDefInvTemp();});
	$("#btnEdit").click(function(){UI_editSelInvTemp();});
	UI_invTemplates.createGrid();
	$("#btnSearch").click(function() {UI_invTemplates.createGrid();})
	UI_invTemplates.getAirCraftDetails();
		
}

UI_invTemplates.getAirCraftDetails = function () {
	$.ajax({
        url : 'mngInvTemps!loadInitData.action',
        dataType:"json",
        type : "POST",		           
		success : UI_invTemplates.AirCraftSuccess
     });
}

UI_invTemplates.AirCraftSuccess = function (response) {
	UI_invTemplates.airCraftList = response.airCraftList;
	try { 
		eval(response.errorList);
	} catch (err){
		console.log("eval() error ==> errorList");
	}
	
	try { 
		eval(response.airCraftList);
	} catch (err){
		console.log("eval() error ==> airCraftList");
	}
	$('#selAircraftModel').html("<option value=''>Options</option>"+UI_invTemplates.airCraftList);
}

UI_invTemplates.createGrid =function(){
	/*This  method create grid to display aircraft wise existing template details */
	$("#InvTemplDetailsGridContainer").empty().append('<table id="invTemplGrid"></table><div id="invTempPages"></div>')
	var data = {};
	data['airCraftModel'] = $("#selAircraftModel").val();
	
	var invTempGrid = $("#invTemplGrid").jqGrid({			
		datatype : function(postdata) {
			$.ajax({
				url : "searchInvTemps.action",
				data : $.extend(postdata, data),
				dataType : "json",
				type : "POST",
				complete : function(jsonData, stat) {
					if (stat == "success") {
						mydata = eval("("+ jsonData.responseText + ")");
						if (mydata.msgType != "Error") {
							$.data(invTempGrid[0], "gridData", mydata.invTemplateList);
							invTempGrid[0].addJSONData(mydata);
						} else {
							alert(mydata.message);
						}
					}
				}
			});
		},		
		height : 200,
		colNames : [ 'Template ID', 'AirCraft Model', 'Segment Code','Status'],
		colModel : [ {name : 'templateID',index : 'templateID',jsonmap : 'invTempDTO.invTempID',width : 120},
		             {name : 'airCraftModel',index : 'airCraftModel',jsonmap : 'invTempDTO.airCraftModel',width : 185},
		             {name : 'segmentCode',index : 'segmentCode',jsonmap : 'invTempDTO.segment',width : 150},
		             {name : 'status',index : 'status',jsonmap : 'invTempDTO.status',width : 100}
		             ],
		caption : "Inventory Template Details",
		rowNum : 20,
		viewRecords : true,
		width : 920,
		pager : jQuery('#invTempPages'),
		jsonReader : {
			root : "invTemplateList",
			page : "page",
			total : "total",
			records : "records",
			repeatitems : false,
			id : "0"
		},
	}).navGrid("#invTempPages", {refresh : true,edit : false,add : false,del : false,search : false});	
}

UI_invTemplates.addRWInvTemp= function(){	
	UI_invTemplates.selAirCraftModel=$("#selAircraftModel").val();
	if(UI_invTemplates.selAirCraftModel.length==0 && UI_invTemplates.selAirCraftModel != null){
		showERRMessage("Select AIrcraft Model");
		return;
	}
	objOnFocus();
	top.LoadPage("showInventoryTemplates!templateBCAlloc.action?showRW=Y&airCraftModel="+UI_invTemplates.selAirCraftModel,"SC_INVN_006", "Template BC Allocations");
	top[1].objTMenu.tabSetValue("SC_INVN_006", "1^");
	top[1].objTMenu.tabRemove('SC_INVN_005');
}

UI_invTemplates.addDefInvTemp = function(){
	UI_invTemplates.selAirCraftModel=$("#selAircraftModel").val();	
	if(UI_invTemplates.selAirCraftModel.length==0 && UI_invTemplates.selAirCraftModel != null){
		showERRMessage("Select AIrcraft Model");
		return;
	}
	objOnFocus();
	top.LoadPage("showInventoryTemplates!templateBCAlloc.action?showRW=N&airCraftModel="+UI_invTemplates.selAirCraftModel,"SC_INVN_006", "Template BC Allocations");
	top[1].objTMenu.tabSetValue("SC_INVN_006", "1^");
	top[1].objTMenu.tabRemove('SC_INVN_005')
}

UI_editSelInvTemp = function() {
	UI_invTemplates.selAirCraftModel=$("#selAircraftModel").val();
	var rowId = $("#invTemplGrid").getGridParam('selrow');
	if(rowId == null || rowId <= 0){
		showERRMessage("Select Inventory Template");
		return;
	}
	var rowData = $("#invTemplGrid").getRowData(rowId);
	var templateID = rowData.templateID;
    var segment = rowData.segmentCode;
    var showSegment = "Y";
    if(segment == null || segment.length == 0){
    	showSegment = "N";
    }
    var status = rowData.status;
    objOnFocus();
    var url = "showInventoryTemplates!templateBCAlloc.action?showRW="+showSegment+"&templateID="+templateID+"&segment="+segment+"&status="+status+"&airCraftModel="+rowData.airCraftModel;
	top.LoadPage(url,"SC_INVN_006", "Template BC Allocations");
	top[1].objTMenu.tabSetValue("SC_INVN_006", "1^");
	top[1].objTMenu.tabRemove('SC_INVN_005')	
}

function objOnFocus() {
	top[2].HidePageMessage();	
}
