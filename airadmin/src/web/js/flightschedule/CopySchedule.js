var blnPageedit = false;
var strGridRow = opener.strGridRow;
var objWindow;
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.buildCalendar();

function setDate(strDate, strID) {
	dataChanged();
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 10;
	objCal1.left = 0;
	objCal1.showCalendar(objEvent);
	objCal1.onClick = "setDate";
}

// var dtToday = new Date();
// dtToday = dtToday.getDate() + "/" + (dtToday.getMonth() + 1) + "/" +
// dtToday.getFullYear();

// var strMode = getText("hdnMode");

function setPageEdited(isEdited) {
	blnPageedit = isEdited;
}

function dataChanged() {
	setPageEdited(true);
}
// On Page loading make sure that all the page input fields are cleared
function winOnLoad() {
	clearInputControls();
	setPageEdited(false);
	setField("hdnScheduleId", opener.arrData[strGridRow][11]);
	if (opener.blnZuluTime) {
		setField("hdnStartD", formatDate(opener.arrData[strGridRow][2],
				"dd/mm/yyyy", "dd/mm/yyyy"));
		setField("hdnStopD", formatDate(opener.arrData[strGridRow][3],
				"dd/mm/yyyy", "dd/mm/yyyy"));

	} else {
		setField("hdnStartD", formatDate(opener.arrData[strGridRow][30],
				"dd/mm/yyyy", "dd/mm/yyyy"));
		setField("hdnStopD", formatDate(opener.arrData[strGridRow][31],
				"dd/mm/yyyy", "dd/mm/yyyy"));

	}
	getFieldByID("txtFromDate").focus();

}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function ConfirmCopy() {
	// Mandotory Fields

	if (isEmpty(getVal("txtFromDate"))) {
		validateError(opener.copyfromdate);
		getFieldByID("txtFromDate").focus();
		return;
	}

	if (!dateChk("txtFromDate")) {
		validateError(opener.splinvaliddate);
		getFieldByID("txtFromDate").focus();
		return;
	}

	if (isEmpty(getVal("txtToDate"))) {
		validateError(opener.copytodate);
		getFieldByID("txtToDate").focus();
		return;
	}
	if (!dateChk("txtToDate")) {
		validateError(opener.splinvaliddate);
		getFieldByID("txtToDate").focus();
		return;
	}

	// compare start date and current date
	var StartD = formatDate(getVal("txtFromDate"), "dd/mm/yyyy", "dd-mmm-yyyy");
	var Today = formatDate(getVal("hdnCurrentDate"), "dd/mm/yyyy",
			"dd-mmm-yyyy");
	var StopD = formatDate(getVal("txtToDate"), "dd/mm/yyyy", "dd-mmm-yyyy");
	var OriginalStartD = formatDate(getVal("hdnStartD"), "dd/mm/yyyy",
			"dd-mmm-yyyy");
	var OriginalStopD = formatDate(getVal("hdnStopD"), "dd/mm/yyyy",
			"dd-mmm-yyyy");

	if (compareDates(Today, StartD, '>=')) {
		validateError(opener.cpylessthancurr);
		getFieldByID("txtFromDate").focus();
		return;
	}
	// compare start date and stop date
	if (compareDates(StartD, StopD, '>=')) {
		validateError(opener.tolessthanfrom);
		getFieldByID("txtToDate").focus();
		return;
	}

	// compare Date overlap
	if ((compareDates(OriginalStartD, StartD, '>='))
			&& (compareDates(OriginalStartD, StopD, '<='))) {
		validateError(opener.fromconflict);
		getFieldByID("txtFromDate").focus();
		return;
	}

	if ((compareDates(OriginalStopD, StartD, '>='))
			&& (compareDates(OriginalStopD, StopD, '<='))) {
		validateError(opener.toconflict);
		getFieldByID("txtToDate").focus();
		return;
	}

	setPageEdited(false);
	var strFromD = getFieldByID("txtFromDate").value;
	var strToD = getFieldByID("txtToDate").value;
	var strSchdId = getFieldByID("hdnScheduleId").value;
	var strSearchCriteria = opener.top[1].objTMenu.tabGetValue("SC_SCHD_001")
			.split("^")[1];
	var strSearch = strSearchCriteria.split(",");
	var searchstd = strSearch[0];
	var searchstpd = strSearch[1];
	var searchfltno = strSearch[2];
	var searchfrm = strSearch[3];
	var searchto = strSearch[4];
	var searchoptype = strSearch[5];
	var searchstat = strSearch[6];
	var searchbldst = strSearch[7];
	var searchFltstart = strSearch[8];
	var searchtime = strSearch[9];

	opener.disableCommonButtons("false");
	opener.location = "showSchedule.action?&hdnMode=COPY&hdnScheduleId="
			+ strSchdId + "&hdnCopyStartD=" + strFromD + "&hdnCopyStopD="
			+ strToD + "&txtStartDateSearch=" + searchstd
			+ "&txtStopDateSearch=" + searchstpd + "&txtFlightNoSearch="
			+ searchfltno + "&selFromStn6=" + searchfrm + "&selToStn6="
			+ searchto + "&selOperationTypeSearch=" + searchoptype
			+ "&selStatusSearch=" + searchstat + "&selBuildStatusSearch="
			+ searchbldst + "&txtFlightNoStartSearch=" + searchFltstart
			+ "&hdnTimeMode=" + searchtime;
	opener.top[0].objWindow.close();
	opener.top[2].ShowProgress();
}

// Clear all input controls
function clearInputControls() {
	setField("txtFromDate", "");
	setField("txtToDate", "");
}

function validateError(msg) {
	objMsg.MessageText = msg;
	objMsg.MessageType = "Error";
	ShowPageMessage();
}

function cancelClick() {
	if (blnPageedit) {
		var cnf = confirm("Changes will be lost! Do you wish to Continue?");
		if (cnf) {
			setPageEdited(false);
			window.close();
		}
	} else {
		setPageEdited(false);
		window.close();
	}
}
