var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "9%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Flight Number";
objCol1.headerText = "Flight"
objCol1.itemAlign = "left"

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "6%";
objCol2.arrayIndex = 2;
objCol2.toolTip = "Departure";
objCol2.headerText = "Departure";
objCol2.itemAlign = "center"

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "15%";
objCol3.arrayIndex = 3;
objCol3.toolTip = "Departure Date/Time";
objCol3.headerText = "Departure Date/Time";
objCol3.itemAlign = "center"

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "6%";
objCol4.arrayIndex = 4;
objCol4.toolTip = "Arrival";
objCol4.headerText = "Arrival";
objCol4.itemAlign = "center"

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "15%";
objCol5.arrayIndex = 5;
objCol5.toolTip = "Arrival Date/Time";
objCol5.headerText = "Arrival Date/Time";
objCol5.itemAlign = "center"

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "10%";
objCol6.arrayIndex = 6;
objCol6.toolTip = "Route";
objCol6.headerText = "Route";
objCol6.itemAlign = "left"

var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "10%";
objCol7.arrayIndex = 8;
objCol7.toolTip = "Sold Adult/Infant";
objCol7.headerText = "Sold Adult/Infant<BR>(F/J/Y)";
objCol7.itemAlign = "right"

var objCol8 = new DGColumn();
objCol8.columnType = "label";
objCol8.width = "10%";
objCol8.arrayIndex = 9;
objCol8.toolTip = "Onhold Adult/Infant";
objCol8.headerText = "Onhold Adult/Infant<BR>(F/J/Y)"
objCol8.itemAlign = "right"

var objCol9 = new DGColumn();
objCol9.columnType = "label";
objCol9.width = "10%";
objCol9.arrayIndex = 7;
objCol9.toolTip = "Available Adult/Infant";
objCol9.headerText = "Available Adult/Infant<BR>(F/J/Y)"
objCol9.itemAlign = "right"

var objCol10 = new DGColumn();
objCol10.columnType = "custom";
objCol10.width = "8%";
objCol10.arrayIndex = 11;
objCol10.toolTip = "Select";
objCol10.headerText = "Select";
objCol10.htmlTag = "<input type='button' class='Button' name='btnSelect' id='btnSelect' value='Sel' style='width:30px'>";
objCol10.itemAlign = "center"

// ---------------- Grid
var objDG = new DataGrid("spnSearch");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol6);
objDG.addColumn(objCol7);
objDG.addColumn(objCol8);
objDG.addColumn(objCol9);
// objDG.addColumn(objCol10);

objDG.width = "99%";
objDG.height = "105px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = arrFlightsToReprotectData;
objDG.seqNo = true;
objDG.seqNoWidth = "4%";
objDG.rowClick = "RowClick";
objDG.seqStartNo = recNo;
objDG.pgnumRecTotal = totalNoOfRecords; // remove as per return record size
objDG.paging = true;
objDG.pgnumRecPage = 20;
objDG.pgonClick = "gridNavigations";
objDG.displayGrid();

function reProtectSchedule() {
	setFiled("hdnMode", "REPROTECT_SCHEDULE");
	document.form[0].submit();
	top[2].ShowProgress();
}

function returnSchedule() {
	setFiled("hdnMode", "SCHEDULE");
	document.form[0].submit();
	top[2].ShowProgress();
}
