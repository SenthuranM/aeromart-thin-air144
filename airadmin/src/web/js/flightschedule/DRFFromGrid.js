// JavaScript Document
// Author -Srikanth, Shakir

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "22%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Segment";
objCol1.headerText = "Segment";
objCol1.itemAlign = "left"

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "15%";
objCol2.arrayIndex = 2;
objCol2.toolTip = "Sold Adult/Infant";
objCol2.headerText = "Sold Adult/Infant";
objCol2.itemAlign = "right"

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "15%";
objCol3.arrayIndex = 3;
objCol3.toolTip = "On Hold Adult/Infant";
objCol3.headerText = "On Hold Adult/Infant";
objCol3.itemAlign = "right"

var objCol4 = new DGColumn();
objCol4.columnType = "checkbox";
objCol4.width = "15%";
objCol4.arrayIndex = 4;
objCol4.toolTip = "Transfer / All";
objCol4.headerText = "Transfer / All";
objCol4.linkOnClick = "chkChange"
objCol4.itemAlign = "center"

var objCol5 = new DGColumn();
objCol5.columnType = "CUSTOM";
objCol5.width = "14%";
objCol5.arrayIndex = 5;
objCol5.toolTip = "Reprotect Adult";
objCol5.headerText = "Reprotect <br> Adult";
objCol5.htmlTag = "<input :CUSTOM:  type='text' id='txtReprotect' name='txtReprotect' onKeyUp='parent.validateTxtBox(this)' onKeyPress='parent.validateTxtBox(this)' maxlength='8' align='right' style='width:60px'>";
objCol5.ID = "txtReprotect";
// objCol5.onChange = "txtChange";
objCol5.itemAlign = "center";


var objCol6 = new DGColumn();
objCol6.columnType = "CUSTOM";
objCol6.width = "14%";
objCol6.arrayIndex = 6;
objCol6.toolTip = "Click to Select PNR";
objCol6.headerText = "PNR";
objCol6.htmlTag = "<input :CUSTOM: type='button' id='btnPaxPNR' name='btnPaxPNR' align='center' style='width:30px' onClick='parent.selectReprotectPNR(:ROW:)' value='PNR'>";
objCol6.ID = "btnPaxPNR";
// objCol6.onChange = "txtChange";
objCol6.itemAlign = "center";


// ---------------- Grid
var objDGFrom = new DataGrid("spnReprotectFrom");
objDGFrom.addColumn(objCol1);
objDGFrom.addColumn(objCol2);
objDGFrom.addColumn(objCol3);
objDGFrom.addColumn(objCol4);
objDGFrom.addColumn(objCol5);
objDGFrom.addColumn(objCol6);

objDGFrom.width = "99%";
objDGFrom.height = "100px";
objDGFrom.headerBold = false;
objDGFrom.seqNo = true;
objDGFrom.rowOver = true;
objDGFrom.rowSelect = true;
objDGFrom.rowMultiSelect = true;
objDGFrom.backGroundColor = "#F4F4F4";
objDGFrom.rowClick = "fromRowClick";
objDGFrom.displayGrid();

var blnRowClick = false;
var fromGridRowNumber = -1;
function fromRowClick(strRowNo) {
	fromGridRowNumber = strRowNo;
	blnRowClick = true;
}

function chkChange(rowid) {
	setPageEdited(true);
	var arrRData = objDGFrom.getSelectedColumn(3);
	if (arrRData[rowid][1] != false) {
		addTextBox(rowid);
		objDGFrom.setDisable(rowid, 4, true);
	} else {
		objDGFrom.setDisable(rowid, 4, false);
		objDGFrom.setCellValue(rowid, 4, "");
	}
}

function addTextBox(strRowNo) {
	var fromSold = objDGFrom.getCellValue(strRowNo, 1);
	var fromOnHold = objDGFrom.getCellValue(strRowNo, 2);

	fromSold = fromSold.split("/");
	fromOnHold = fromOnHold.split("/");

	var totAdult = parseInt(fromSold[0]) + parseInt(fromOnHold[0]);
	var totInfant = parseInt(fromSold[1]) + parseInt(fromOnHold[1]);

	objDGFrom.setCellValue(strRowNo, 4, totAdult);
}