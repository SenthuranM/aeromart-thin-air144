// JavaScript Document
// Author -Srikanth

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "34%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Segment";
objCol1.headerText = "Segment";
objCol1.itemAlign = "left"

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "21%";
objCol2.arrayIndex = 2;
objCol2.toolTip = "Sold Adult/Infant";
objCol2.headerText = "Sold Adult/Infant";
objCol2.itemAlign = "right"

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "21%";
objCol3.arrayIndex = 3;
objCol3.toolTip = "On Hold Adult/Infant";
objCol3.headerText = "On Hold Adult/Infant";
objCol3.itemAlign = "right"

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "21%";
objCol4.arrayIndex = 4;
objCol4.toolTip = "Available Adult/Infant";
objCol4.headerText = "Available Adult/Infant";
objCol4.itemAlign = "right"

// ---------------- Grid
var objDGOther = new DataGrid("spnOther");
objDGOther.addColumn(objCol1);
objDGOther.addColumn(objCol2);
objDGOther.addColumn(objCol3);
objDGOther.addColumn(objCol4);

objDGOther.width = "99%";
objDGOther.height = "100px";
objDGOther.headerBold = false;
objDGOther.seqNo = true;
objDGOther.rowOver = true;
objDGOther.rowSelect = true;
objDGOther.backGroundColor = "#F4F4F4";
objDGOther.rowClick = "parent.otherRowClick";
objDGOther.displayGrid();
