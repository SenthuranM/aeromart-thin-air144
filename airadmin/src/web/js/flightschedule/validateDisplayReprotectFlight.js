// JavaScript Document
// Author - Srikanth

var strGridRow = -1;
var logicalCCCode = "";
var pageEdited = false;
var isParentOpener = false;
var rollFwdBtn;
var objWindow;
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();
var newSegment = '';
var oldSegment = '';

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtDepartureDateFrom", strDate);
		break;
	case "1":
		setField("txtDepartureDateTo", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}


function allowDisplayRollFwdButton(){
	if(allowDisplayRollFwdBtn === "true"){
		setVisible("spnViewRollForwardBtn", true);
		Disable("btnCRF", true);
	}
}

/** *************************************************************************** */
/** ****************** P A G E L O A D E V E N T ********************** */
/** *************************************************************************** */
function winOnLoad(strMsg, strMsgType) {
	if (typeof (opener.top[0].strReprotectFrom) == "string") {
		isParentOpener = true; // Flight Screen Reprotect Button Click
	} else {
		isParentOpener = false; // Intermediate Window
	}
	closeChildWindow();
	getFieldByID("txtDepartureDateFrom").focus();
	
	// preparing the COS combo list
	var logicalCCLength = parent.arrBaseArray.length;
	
	if (logicalCCLength == 0) {
		Disable("btnConfirm", true);
	} else {

		// setting the values of Re-protect From labels
		document.getElementById('spnFlightNo').innerHTML = parent.arrBaseArray[0][2];
		document.getElementById('spnDeparture').innerHTML = parent.arrBaseArray[0][4];
		document.getElementById('spnArrival').innerHTML = parent.arrBaseArray[0][3];
		document.getElementById('spnDepDate').innerHTML = parent.arrBaseArray[0][7];

		setField("hdnFlightId", parent.arrBaseArray[0][6]);
		setField("hdnFlightNo", parent.arrBaseArray[0][2]);
		setField("hdnDepatureDate", parent.arrBaseArray[0][7]);
		var logicalCCDesc = "";
		var strLogicalClassOfServiceHtml = '<select id="selLogicalCC" ' + 'name="selLogicalCC" size="1" style="width:100px" ' + 'onChange="changeLogicalCC(this.value)">';
		var isSelected = "";
		for ( var logicalCCCount = 0; logicalCCCount < logicalCCLength; logicalCCCount++) {
			if (logicalCCCode == "" || logicalCCCode != parent.arrBaseArray[logicalCCCount][0]) {
				logicalCCCode = parent.arrBaseArray[logicalCCCount][0];
				logicalCCDesc = parent.arrBaseArray[logicalCCCount][1];
				if (logicalCCCode == "Y") {
					isSelected = "selected";
				}
				strLogicalClassOfServiceHtml += '<option value="' + logicalCCCode + '" ' + isSelected
						+ '>' + logicalCCDesc + '</option>';
			}
		}
		strLogicalClassOfServiceHtml += '</select>';
		document.getElementById('spnCOSFrom').innerHTML = strLogicalClassOfServiceHtml;
		if (logicalCCLength == 1) {
			Disable("selLogicalCC", true);
		} else {
			Disable("selLogicalCC", false);
		}
	
		// setting the from data grid
		resetChangeLogicalCC(parent.arrBaseArray,
				getValue("selLogicalCC"));
		// objDGFrom.arrGridData = parent.arrBaseArray[0][5];
		// objDGFrom.refresh();
		objDGFrom.setDisable("", "", true);
		setField("hdnFlightId", parent.arrBaseArray[0][6]);
	
		if ((strMsg != null) && (strMsgType != null)) {
			showWindowCommonError(strMsgType, strMsg);
		}
		
		if (autoDateFillEnabled == "true") {
			var currentDate = new Date();
			var yyyy = currentDate.getFullYear().toString();
			var mm = (currentDate.getMonth()+1).toString();
			var dd  = currentDate.getDate().toString();
			var dateAsString = (dd[1]?dd:"0"+1) +"/"+ (mm[1]?mm:"0"+mm[0]) + "/" + yyyy;
			setFieldValues("txtDepartureDateFrom", dateAsString);
			setFieldValues("txtDepartureDateTo", dateAsString);
		}
	
		// After search button was clicked
		if (isPostBack) {
			if (departureDateFrom != null)
				setFieldValues("txtDepartureDateFrom", departureDateFrom);
			if (departureDateTo != null)
				setFieldValues("txtDepartureDateTo", departureDateTo);
			if (flightNumber != null)
				setFieldValues("txtFlightNumber", flightNumber);
			if (departure != null)
				setFieldValues("selDeparture", departure);
			if (arrival != null)
				setFieldValues("selArrival", arrival);
			if (via1 != null)
				setFieldValues("selVia1", via1);
			if (via2 != null)
				setFieldValues("selVia2", via2);
			if (via3 != null)
				setFieldValues("selVia3", via3);
			if (via4 != null)
				setFieldValues("selVia4", via4);
	
		}
		Disable("btnMove", true);
		Disable("btnConfirm", true);
		// Disable("btnCRF", true);
		Disable("chkGA", true);
		Disable("btnReset", true);
		var strPageButtonMode;
		if (isParentOpener) {
			strPageButtonMode = opener.top[0].strReprotectFrom;
		} else {
			strPageButtonMode = opener.opener.top[0].strReprotectFrom;
		}
		// if coming thru flight cancel
		if (strPageButtonMode == "FLIGHT") {
			setVisible("spnViewRollForwardBtn", false);
		} else {
			setVisible("spnViewRollForwardBtn", true);
			Disable("btnCRF", true);
		}
		
		allowDisplayRollFwdButton();
	}

	// show popup
	if (trim(popupmessage).length != 0) {
		alert(popupmessage);
		if (!isParentOpener) {
			opener.opener.reprotectRefresh();
		}else if(allowDisplayRollFwdBtn && isParentOpener){
			opener.reprotectRefresh();
		}
	}

	if (sendSMSForPax == "Y") {
		Disable("chkSendSMS", false);
		Disable("chkSmsCnf", true);
		Disable("chkSmsOnH", true);

	} else {
		Disable("chkSendSMS", true);
		Disable("chkSmsCnf", true);
		Disable("chkSmsOnH", true);
	}
	if (sendEmailForPax == "Y") {
		Disable("chkSendEmail", false);

	} else {
		Disable("chkSendEmail", true);
	}
}

/** *************************************************************************** */
/** **** V A L I D A T E F L I G H T S R E - P R O T E C T S E A R C H ***** */
/** *************************************************************************** */
function validateSearchFlightReprotect() {
	var strToDate = document.getElementById("txtDepartureDateTo").value;
	var strFromDate = document.getElementById("txtDepartureDateFrom").value;
	var isToDateLessthanFromDate = CheckDates(strToDate, strFromDate);

	if (validateWindowTextField(strFromDate, departureDateFromRqrd)) {
		getFieldByID("txtDepartureDateFrom").focus();
		return;
	}

	if ((allowReprotectPAX != "Y") && (systemDate != "null")
			&& (CheckDates(strFromDate, systemDate))) {
		showWindowCommonError("Error", departureDateFromBnsRleVltd);
		document.getElementById("txtDepartureDateFrom").focus();
		return;
	}

	if (validateWindowTextField(strToDate, departureDateToRqrd)) {
		getFieldByID("txtDepartureDateTo").focus();
		return;
	}

	if (strFromDate != strToDate) {
		if (isToDateLessthanFromDate) {
			showWindowCommonError("Error", departureDateToBnsRleVltd);
			document.getElementById("txtDepartureDateFrom").focus();
			return;
		}
	}

	if (document.getElementById("selDeparture").value != "-1"
			&& document.getElementById("selArrival").value != "-1") {
		if (document.getElementById("selDeparture").value == document
				.getElementById("selArrival").value) {
			showWindowCommonError("Error", departureAndArrivalBnsRleVltd);
			document.getElementById("selArrival").focus();
			return;
		}
	}

	var strDeparture = document.getElementById("selDeparture").value;
	var strArrival = document.getElementById("selArrival").value;

	var strVia1 = document.getElementById("selVia1").value;
	var strVia2 = document.getElementById("selVia2").value;
	var strVia3 = document.getElementById("selVia3").value;
	var strVia4 = document.getElementById("selVia4").value;

	var arrSegments = new Array();
	arrSegments[0] = strDeparture;
	arrSegments[1] = strArrival;

	arrSegments[2] = strVia1;
	arrSegments[3] = strVia2;
	arrSegments[4] = strVia3;
	arrSegments[5] = strVia4;

	var blnDuplicate = false;
	for ( var i = 0; i < arrSegments.length; i++) {
		if (arrSegments[i] != "-1") {
			for ( var m = i; m < arrSegments.length; m++) {
				if (arrSegments[m] != "-1") {
					if (arrSegments[m] == arrSegments[i]) {
						if (m != i) {
							blnDuplicate = true;
							break;
						}
					}
				}
			}
		}
		if (blnDuplicate) {
			break;
		}
	}

	if (blnDuplicate) {
		showWindowCommonError("Error", reprotectViaPointsInvalid);
		return;
	}
	return true;
}

/** ************************************************************************* */
/** ************ F L I G H T S R E - P R O T E C T S E A R C H *********** */
/** ************************************************************************* */
function searchFlightsToReprotect() {
	if ((!isPageEdited()) || (isPageEdited() && isConfirmed())) {
		HidePageMessage();
		if (!validateSearchFlightReprotect()) {
			return;
		}
		setField("hdnFlightId", parent.arrBaseArray[0][6]);
		setField("hdnDepatureDateTime",parent.arrBaseArray[0][9]);
		var strActionUrl = "showFlightsToReprotect.action";
		// strActionUrl += "?hdnFlightId="+strFlightId;

		// once search button was pressed set the hidden value to "SEARCH"
		document.forms[0].action = strActionUrl;
		document.forms[0].target = "_self";
		document.forms[0].hdnMode.value = "SEARCH";
		document.forms[0].submit();
	}
}

/** ************************************************************************* */
/** ********* R O W C L I C K E V E N T O F M A I N G R I D ********** */
/** ************************************************************************* */
function RowClick(strRowNo) {
	if ((!isPageEdited()) || (isPageEdited() && isConfirmed())) {
		HidePageMessage();
		initValues();
		disableConfirmButtons(true);
		// Code to reset the Reprotect - From grid
		resetChangeLogicalCC(parent.arrBaseArray,
				getValue("selLogicalCC"));
		Disable("selLogicalCC", false);
		setVisible("spnReprotectTo", false);

		var originClassFound = false;
		var sameFlight = false;
		var logicalCC = getValue("selLogicalCC");

		var arrRow = arrFlightsToReprotectData[strRowNo];
		newSegment = arrRow[14].substring(arrRow[14], arrRow[14].length - 1,	1);
		newSegment = arrRow[14].substring(arrRow[14], arrRow[14].length - 1,	1);
		var arrRowLogicalCC = arrRow[15].substring(arrRow[15], arrRow[15].length - 1,
				1);
		var arrLogicalCCes = arrRowLogicalCC.split(":");

		var logicalCCCode = "";
		var logicalCCDesc = "";
		// Check if logicalCC in FROM found in TO
		for ( var logicalCCCount = 0; logicalCCCount < arrLogicalCCes.length; logicalCCCount++) {
			logicalCCCode = arrLogicalCCes[logicalCCCount];
			if (logicalCC == logicalCCCode) {
				originClassFound = true;
				break;
			}
		}
		if (arrRow[10] == parent.arrBaseArray[0][6]) {
			sameFlight = true;
		}
		if ((!originClassFound) || (sameFlight)) {
			if (sameFlight) {
				showWindowCommonError("Error", sameFlightSelected);
			} else if (!originClassFound) {
				showWindowCommonError("Error", classOfServiceNotAvailable);
			}
			return false;
		} else {
			// Enabling the FROM Grid
			var logicalCCLength = parent.arrBaseArray.length;
			if (logicalCCLength > 0) {
				Disable("selLogicalCC", false);
			} else {
				Disable("selLogicalCC", true);
			}
			Disable("btnMove", false);
			objDGFrom.setDisable("", "", false);
			
			var arrCabinClassesDes = arrRow[22].split("<BR>");

			var strLogicalClassOfServiceHtmlOther = "";
			var selectOption = "";
			strRowData = objDG.getRowValue(strRowNo);
			strGridRow = strRowNo;
//			strClassOfServiceHtmlOther = '<select id="selClassOfServiceOther" name="selClassOfServiceOther" size="1" style="width:100px" onChange="changeClassOfServiceOther(this.value)">';
//			for ( var j = 0; j < arrCabinClassesDes.length; j++) {
//				if(arrCabinClassesDes[j] != ''){
//					var cabinClassesDes = arrCabinClassesDes[j].split(",");
//					for(var k=0;k<cabinClassesDes.length;k++){
//						var cabinClasses = cabinClassesDes[k];
//						ccCode = cabinClasses.split(":")[0];
//						ccDesc = cabinClasses.split(":")[1];
//						if (ccCode == cabinClass) {
//							selectOption = "selected";
//						} else {
//							selectOption = "";
//						}
//						strClassOfServiceHtmlOther += '<option value="' + ccCode + '" '
//								+ selectOption + '>' + ccDesc + "</option>";
//					}
//				}
//			}
			strLogicalClassOfServiceHtmlOther = '<select id="selLogicalCCOther" name="selLogicalCCOther" size="1" style="width:100px" onChange="changeLogicalClassOfServiceOther(this.value)">';
			var logicalCCLength = parent.arrBaseArray.length;
			for ( var logicalCCCount = 0; logicalCCCount < logicalCCLength; logicalCCCount++) {
				logicalCCCode = parent.arrBaseArray[logicalCCCount][0];
				logicalCCDesc = parent.arrBaseArray[logicalCCCount][1];
				if (logicalCCCode == logicalCC) {
					selectOption = "selected";
				} else {
					selectOption = "";
				}
				strLogicalClassOfServiceHtmlOther += '<option value="' + logicalCCCode + '" '
						+ selectOption + '>' + logicalCCDesc + "</option>";
			}
			strLogicalClassOfServiceHtmlOther += '</select>';

			// setting the values of Re-protect From labels
			document.getElementById('spnFlightNoOther').innerHTML = arrRow[1];
			document.getElementById('spnDepartureOther').innerHTML = arrRow[2];
			document.getElementById('spnArrivalOther').innerHTML = arrRow[4];
			document.getElementById('spnCOSOther').innerHTML = strLogicalClassOfServiceHtmlOther;
			//Allow to change the cabin class while reprotecting.
			//Disable("selLogicalCCOther", true);
			/*
			 * if (arrRow[6] == null || arrRow[6] == "") {
			 * setVisible("spnViewViaOther", false);
			 * document.getElementById('spnViaOther').innerHTML = ""; } else {
			 * setVisible("spnViewViaOther", true);
			 * document.getElementById('spnViaOther').innerHTML = arrRow[6]; }
			 */
			document.getElementById('spnModelCap').innerHTML = arrRow[12];
			document.getElementById('spnDepDateOther').innerHTML = arrRow[20];
			Disable("btnReset", true);
			changeLogicalClassOfServiceOther(logicalCC);
			setVisible("spnReprotectTo", true);
			if(agentReprotectEmailEnabled){
				setVisible("agentEmailRow", true);
			}else{
				setVisible("agentEmailRow", false);
			}
		}
	}
}

var otherGridRowNumber = -1;
function otherRowClick(strRowNo) {
	otherGridRowNumber = strRowNo;
	// objDGOther.arrGridData[strRowNo][5]
}

/** *************************************************************** */
/** ****** R E - P R O T E C T P A X V A L I D A T I O N ******** */
/** *************************************************************** */
function reprotectPassenger() {
	HidePageMessage();
	var strLogicalCCCode = getValue("selLogicalCC"); 
	var strTargetCabinClassCode = getValue("selLogicalCCOther");
	var tempFromSelectedRows = "";
	var tempOtherSelectedRows = "";

	var finalOtherAvailableAdult = 0;
	var finalOtherAvailableInfant = 0;

	var availableAdult = 0;
	var availableInfant = 0;

	var finalSoldAdult = 0;
	var finalSoldInfant = 0;

	var finalOnHoldAdult = 0;
	var finalOnHoldInfant = 0;

	tempFromSelectedRows = String(objDGFrom.getSelectedRows());
	tempOtherSelectedRows = String(objDGOther.getSelectedRows());

	var strSourceFlightDistributions = "";

	var totalFromSoldAdult = 0;
	var totalFromSoldInfant = 0;

	var totalFromOnHoldAdult = 0;
	var totalFromOnHoldInfant = 0;

	if ((tempFromSelectedRows != "") && (tempOtherSelectedRows != "")) {
		// contains the first grid selected row numbers
		var fromSelectedArray = tempFromSelectedRows.split(",");	
		var selectedPnrStr = getValue("hdnSelectedPNR");
		var selectedPnrSoldOhdCount = getValue("hdnSelectedSoldOhdCount");
		
		var fromGrandTotal = 0;
		//var otherAvailableTotal = finalOtherAvailableAdult
		//		+ finalOtherAvailableInfant;
		for ( var count = 0; count < fromSelectedArray.length; count++) {
			var selectedRowNum = fromSelectedArray[count];
			var tmpSold = String(objDGFrom.getCellValue(
					fromSelectedArray[count], 1));
			tmpSold = tmpSold.split("/");
			var tmpOnHold = String(objDGFrom.getCellValue(
					fromSelectedArray[count], 2));
			tmpOnHold = tmpOnHold.split("/");

			var soldAdult = parseInt(tmpSold[0]);
			var soldInfant = parseInt(tmpSold[1]);

			var onHoldAdult = parseInt(tmpOnHold[0]);
			var onHoldInfant = parseInt(tmpOnHold[1]);

			var fromTotal = soldAdult + onHoldAdult;
			var tmpReprodectAdultTxt = String(objDGFrom.getCellValue(
					fromSelectedArray[count], 4));

			if ((trim(tmpReprodectAdultTxt) == "")
					|| (parseInt(trim(tmpReprodectAdultTxt)) == 0) || isNaN(trim(tmpReprodectAdultTxt))) {
				showWindowCommonError("Error", adultValueEmpty);
				objDGFrom.setColumnFocus(fromSelectedArray[count], 4);
				return false;
			}
			if (tmpReprodectAdultTxt > fromTotal) {
				showWindowCommonError("Error", reprotectAdultValueGter);
				objDGFrom.setColumnFocus(fromSelectedArray[count], 4);
				return false;
			}
			
			
			//validate re-protect adult value and selected PNR total adult count are equal 
						
			var strSourceFlightSegmentId = objDGFrom.arrGridData[selectedRowNum][7]; // ok
			var isTransferAll = objDGFrom.getSelectedColumn(3)[selectedRowNum][1];
			
			var tmpReprodectAdultTxt = String(objDGFrom.getCellValue(selectedRowNum, 4));
			var transferSeatsCount = parseInt(tmpReprodectAdultTxt);
			
			if(!isTransferAll && selectedPnrStr!=null && selectedPnrStr!="") {
				var fltSegArray = selectedPnrStr.split("@");		
				
				for(i=0;i<fltSegArray.length;i++){
					var fltSeg = fltSegArray[i].split(":");
					if(strSourceFlightSegmentId==fltSeg[0]){
						var pnrAdultCount = 0;
						var pnrInfo = fltSeg[1].split(",");
						for(j=0;j<pnrInfo.length;j++){
							var pnr = null;
							
							if(pnrInfo[j]!=null)
								pnr = pnrInfo[j].split("#");
							
							if(pnr!=null && pnr.length > 1)
								pnrAdultCount = pnrAdultCount + parseInt(pnr[1]);
							
						}
						
						if(transferSeatsCount != pnrAdultCount){
							showWindowCommonError("Error",pnrAdultValueNotEqual);
							objDGFrom.setColumnFocus(selectedRowNum, 4);
							return false;
						}
						break;
					}
				}
				
				
				
				
			}

			/*
			 * fromGrandTotal += parseInt(tmpReprodectAdultTxt); if
			 * (fromGrandTotal > otherAvailableTotal) { showErrorMsg("Error",
			 * "Total reprotect adults should not be greater than available");
			 * objDGFrom.setColumnFocus(fromSelectedArray[0], 4); return false; }
			 */
		}

		var isTransferAll = false;
		for ( var count = 0; count < fromSelectedArray.length; count++) {
			isTransferAll = false;
			var selectedRowNum = fromSelectedArray[count];
			var tmpSold = String(objDGFrom.getCellValue(
					fromSelectedArray[count], 1));
			tmpSold = tmpSold.split("/");

			var tmpOnHold = String(objDGFrom.getCellValue(
					fromSelectedArray[count], 2));
			tmpOnHold = tmpOnHold.split("/");

			isTransferAll = objDGFrom.getSelectedColumn(3)[selectedRowNum][1];

			var tmpReprodectAdultTxt = String(objDGFrom.getCellValue(
					fromSelectedArray[count], 4));
			tmpReprodectAdultTxt = parseInt(tmpReprodectAdultTxt);

			var soldAdult = parseInt(tmpSold[0]);
			var soldInfant = parseInt(tmpSold[1]);
			var onHoldAdult = parseInt(tmpOnHold[0]);
			var onHoldInfant = parseInt(tmpOnHold[1]);

			// laterly Added
			var transferSeatsCount = tmpReprodectAdultTxt;
			var strSourceFlightId = objDGFrom.arrGridData[selectedRowNum][6]; // ok
			var strSourceSegmentCode = objDGFrom.arrGridData[selectedRowNum][1]; // ok
			oldSegment = strSourceSegmentCode;
			var strSourceFlightSegmentId = objDGFrom.arrGridData[selectedRowNum][7]; // ok
			// var infantCount = 0;
			// creating the first row by adding the variables
			// and separates the individual variables by ','
			var strSourceFlightDistribution = strSourceFlightId + ",";
			strSourceFlightDistribution += strSourceSegmentCode + ",";
			strSourceFlightDistribution += strSourceFlightSegmentId + ",";
			strSourceFlightDistribution += transferSeatsCount + ",";
			strSourceFlightDistribution += isTransferAll;

			// creating the selected rows by adding the individual row and
			// separates by adding ':'
			// after every row
			strSourceFlightDistributions += strSourceFlightDistribution;
			if ((fromSelectedArray.length - 1) != count) {
				strSourceFlightDistributions += ":";
			}
			// end of new code

			var checkInfant = false;
			var totalCount = soldAdult + onHoldAdult;
			if (totalCount == transferSeatsCount) {
				checkInfant = true;
			}			
			
			if(!isTransferAll && selectedPnrStr!=null && selectedPnrStr!="" && selectedPnrSoldOhdCount != null && selectedPnrSoldOhdCount !="") {				
				var soldOhdCount = selectedPnrSoldOhdCount.split("#");	
				try {
					var pnrSoldAdult = parseInt(String(soldOhdCount[0]));
					var pnrOhdAdult = parseInt(String(soldOhdCount[1]));
					if(pnrSoldAdult > 0){
						totalFromSoldAdult = pnrSoldAdult;
						totalFromOnHoldAdult = (transferSeatsCount - pnrSoldAdult);
					} else if(pnrOhdAdult > 0){
						totalFromOnHoldAdult = pnrOhdAdult;
						totalFromSoldAdult = (transferSeatsCount - pnrOhdAdult);
					} else {
						var availSoldSeat = (transferSeatsCount - soldAdult);
						if (parseInt(availSoldSeat) >= 0) {
							totalFromSoldAdult += soldAdult;
							totalFromOnHoldAdult += availSoldSeat;
						} else {
							totalFromSoldAdult += transferSeatsCount;
						}
					}
				}catch(e) { }				
			} else {
				var availSoldSeat = (transferSeatsCount - soldAdult);
				if (parseInt(availSoldSeat) >= 0) {
					totalFromSoldAdult += soldAdult;
					totalFromOnHoldAdult += availSoldSeat;
				} else {
					totalFromSoldAdult += transferSeatsCount;
				}
			}

			if (checkInfant) {
				totalFromSoldInfant += soldInfant;
				totalFromOnHoldInfant += onHoldInfant;
			}
		}// end of for loop

		// preparing the target flight informations
		// referring the other grid - [ only one row can be selected ]
		var strTargetFlightId = arrFlightsToReprotectData[strGridRow][10]; // ok
		var strTargetSegmentCode = objDGOther.getCellValue(otherGridRowNumber,
				0); // ok
		var strTargetFlightSegmentId = objDGOther.arrGridData[otherGridRowNumber][5]; // ok

		setField("hdnLogicalCabinClassCode", strLogicalCCCode);
		setField("hdnTargetCabinClassCode", strTargetCabinClassCode);
		setField("hdnTargetFlightId", strTargetFlightId);
		setField("hdnTargetSegmentCode", strTargetSegmentCode);
		setField("hdnTargetFlightSegmentId", strTargetFlightSegmentId);

		// the hidden variable contains the prepared source flight distribution
		// details
		setField("hdnSourceFlightDistributions", strSourceFlightDistributions);
		setField("hdnFlightId", parent.arrBaseArray[0][6]);

		// preparing the other grid's available figures
		var newAvailable ='';
		var tmpAvailable = String(objDGOther.getCellValue(
				tempOtherSelectedRows, 3));
		var tmpAvailableArr = tmpAvailable.split(":");
		var isOverbooked = false;
		for(var ind=0; ind < tmpAvailableArr.length; ind++){
			tmpAvailable = tmpAvailableArr[ind].split("/");
			finalOtherAvailableAdult = tmpAvailable[0];
			finalOtherAvailableInfant = tmpAvailable[1];
			// Setting the other grid's Available adult/infant column after
			// reproteced
			availableAdult = parseInt(finalOtherAvailableAdult)
					- parseInt(totalFromSoldAdult) - parseInt(totalFromOnHoldAdult);
			availableInfant = parseInt(finalOtherAvailableInfant)
					- parseInt(totalFromSoldInfant)
					- parseInt(totalFromOnHoldInfant);
			if(availableAdult < 0){
				isOverbooked = true;				
			}
			if(newAvailable == ''){
				newAvailable = availableAdult + "/" + availableInfant;
			}
			else{
				newAvailable = newAvailable + ":" + availableAdult + "/" + availableInfant;
			}
		}
		if(isOverbooked){
			alert('Please note that target flight will be overbooked!');
		}
		objDGOther.setCellValue(tempOtherSelectedRows, 3, newAvailable);

		// preparing the other grid's sold figures
		var newSold = '';
		var tmpSold = String(objDGOther.getCellValue(tempOtherSelectedRows, 1));
		var tmpSoldArr = tmpSold.split(":");
		for(var ind=0; ind< tmpSoldArr.length; ind++){
			tmpSold = tmpSoldArr[ind].split("/");
			finalOtherSoldAdult = tmpSold[0];
			finalOtherSoldInfant = tmpSold[1];
			// Setting the other grid's Sold adult/infant column after reproteced
			finalSoldAdult = parseInt(finalOtherSoldAdult)
					+ parseInt(totalFromSoldAdult);
			finalSoldInfant = parseInt(finalOtherSoldInfant)
					+ parseInt(totalFromSoldInfant);
			if(newSold == ''){
				newSold = finalSoldAdult + "/" + finalSoldInfant;
			}
			else{
				newSold = newSold + ":" + finalSoldAdult + "/" + finalSoldInfant;
			}
		}
		objDGOther.setCellValue(tempOtherSelectedRows, 1, newSold);

		// preparing the other grid's onhold figures
		var newOnHold = '';
		var tmpOnHold = String(objDGOther
				.getCellValue(tempOtherSelectedRows, 2));
		var tmpOnHoldArr = tmpOnHold.split(":");
		for(var ind=0; ind< tmpOnHoldArr.length; ind++){
			tmpOnHold = tmpOnHoldArr[ind].split("/");
			finalOtherOnHoldAdult = tmpOnHold[0];
			finalOtherOnHoldInfant = tmpOnHold[1];
			// Setting the other grid's on hold adult/infant column after reproteced
			finalOnHoldAdult = parseInt(finalOtherOnHoldAdult)
					+ parseInt(totalFromOnHoldAdult);
			;
			finalOnHoldInfant = parseInt(finalOtherOnHoldInfant)
					+ parseInt(totalFromOnHoldInfant);
			if(newOnHold == ''){
				newOnHold = finalOnHoldAdult + "/" + finalOnHoldInfant;
			}
			else{
				newOnHold = newOnHold + ":" + finalOnHoldAdult + "/" + finalOnHoldInfant;
			}
		}
		objDGOther.setCellValue(tempOtherSelectedRows, 2, newOnHold);

		Disable("selLogicalCC", true);
		Disable("selLogicalCCOther", true);
		Disable("btnMove", true);
		objDGFrom.setDisable("", "", true);
		objDGOther.setDisable("", "", true);
		Disable("btnReset", false);
		disableConfirmButtons(false);
		Disable("chkGA", false);
		
		if (sendSMSForPax == "Y") {
			Disable("chkSendSMS", false);
			Disable("chkSmsCnf", true);
			Disable("chkSmsOnH", true);

		} else {
			Disable("chkSendSMS", true);
			Disable("chkSmsCnf", true);
			Disable("chkSmsOnH", true);
		}
		if (sendEmailForPax == "Y") {
			Disable("chkSendEmail", false);

		} else {
			Disable("chkSendEmail", true);
		}

	} else {
		showWindowCommonError("Error", gridNotSelected);
		return;
	}
}

/** ************************************************************************ */
/** ************ CHANGE THE REPROTECT_FROM GRID LOGICALCLASS OF SERVICE **** */
/** ************************************************************************ */
var logicalCCCodeFrom = "";
function changeLogicalCC(selCCCode) {
	logicalCCCodeFrom = selCCCode;
	var logicalCCLength = parent.arrBaseArray.length;
	var arrReprotectSeg = new Array();
	var rowNum = 0;
	for ( var logicalCCCount = 0; logicalCCCount < logicalCCLength; logicalCCCount++) {
		if (selCCCode == parent.arrBaseArray[logicalCCCount][0]) {
			// setting the from data grid
			arrReprotectSeg[rowNum] = new Array();
			var row = parent.arrBaseArray[logicalCCCount][5];
			row[0][0] = rowNum;
			arrReprotectSeg[rowNum] = row[0];
			rowNum++;
		}
	}
	objDGFrom.arrGridData = arrReprotectSeg;
	objDGFrom.refresh();
	clearCellValues(rowNum);
	changeLogicalClassOfServiceOther(selCCCode);
}

/** ************************************************************************************ */
/** CHANGE THE REPROTECT_TO GRID BASED ON REPROTECT_FROM GRID LOGICAL CLASS OF SERVICE * */
/** ************************************************************************************ */
function changeLogicalClassOfServiceOther(selCCCode) {
	if ((objDGOther.loaded) && (arrFlightsToReprotectData != "")
			&& (strGridRow != -1)) {
		setPageEdited(true);
		setField("selLogicalCCOther", selCCCode);

		var arrRow = arrFlightsToReprotectData[strGridRow];
		var arrSolds = arrRow[8].split("<BR>");
		var arrOnHold = arrRow[9].split("<BR>");
		var arrAvailable = arrRow[7].split("<BR>");
		var arrLogicalCCes = arrRow[17].split("<BR>");
		var arrSegments = arrRow[18].split("<BR>");
		var arrSegIDs = arrRow[19].split("<BR>");

		var arrReprotectSeg = new Array();
		if(arrRow[21] == 'connection'){
			var i = 0;
			for ( var j = 0; j < arrLogicalCCes.length; j++) {
				var logicalCCes = arrLogicalCCes[j].split(",");
				for ( var k = 0; k < logicalCCes.length; k++) {
					if (logicalCCes[k] == selCCCode) {
						arrReprotectSeg[i] = new Array();
						arrReprotectSeg[i][0] = i;
						arrReprotectSeg[i][1] = arrSegments[j].split(",")[0]; 
						
						var segSold = '';
						for(var ind=0; ind < arrSolds.length; ind++){
							var segS = arrSolds[ind];
							if(segS.split("-")[0] == selCCCode){
								if(segSold != ''){
									segSold = segSold + "<BR>" + segS.split("-")[1];
								}
								else{
									segSold = segS.split("-")[1];
								}
							}
						}
						arrReprotectSeg[i][2] = segSold;
										
						var segOnHold = '';
						for(var ind=0; ind < arrOnHold.length; ind++){
							var segOh = arrOnHold[ind];
							if(segOh.split("-")[0] == selCCCode){
								if(segOnHold != ''){
									segOnHold = segOnHold + "<BR>" + segOh.split("-")[1];
								}
								else{
									segOnHold = segOh.split("-")[1];
								}
							}
						}
						arrReprotectSeg[i][3] = segOnHold;
						
						var segAvailability = '';
						for(var ind=0; ind < arrAvailable.length; ind++){
							var segAvail = arrAvailable[ind];
							if(segAvail.split("-")[0] == selCCCode){
								if(segAvailability != ''){
									segAvailability = segAvailability + "<BR>" + segAvail.split("-")[1];
								}
								else{
									segAvailability = segAvail.split("-")[1];
								}
							}
						}													
						arrReprotectSeg[i][4] = segAvailability; 
																				// adult/infant
						arrReprotectSeg[i][5] = arrSegIDs[j].split(",")[0]; // segment
																			// id
						i++;
					}
				}
			}
		}
		else{
			var i = 0;
			for ( var j = 0; j < arrLogicalCCes.length; j++) {
				var logicalCCes = arrLogicalCCes[j].split(",");
				for ( var k = 0; k < logicalCCes.length; k++) {
					if (logicalCCes[k] == selCCCode) {
						arrReprotectSeg[i] = new Array();
						arrReprotectSeg[i][0] = i;
						arrReprotectSeg[i][1] = arrSegments[j].split(",")[k]; // segment
																				// code
						arrReprotectSeg[i][2] = arrSolds[j].split(",")[k]; // sold
																			// adult/infant
						arrReprotectSeg[i][3] = arrOnHold[j].split(",")[k]; // on
																			// hold
																			// adult/infant
						arrReprotectSeg[i][4] = arrAvailable[j].split(",")[k]; // available
																				// adult/infant
						arrReprotectSeg[i][5] = arrSegIDs[j].split(",")[k]; // segment
																			// id
						i++;
					}
				}
			}
		}
		objDGOther.arrGridData = arrReprotectSeg;
		objDGOther.refresh();
	} else {
		objDGFrom.setDisable("", "", true);
	}
}

/** *********************************************************************** */
/** ********** R E S E T R E L A T E D C L I C K E V E N T ************* */
/** *********************************************************************** */
// A function to reset the reprotect-from grid, when reset button was pressed
function resetChangeLogicalCC(arrayTemp, selCCCode) {
	var logicalCCLength = arrayTemp.length;
	var arrReprotectSeg = new Array();
	var rowNum = 0;
	for ( var logicalCCCount = 0; logicalCCCount < logicalCCLength; logicalCCCount++) {
		if (selCCCode == arrayTemp[logicalCCCount][0]) {
			arrReprotectSeg[rowNum] = new Array();
			var row = arrayTemp[logicalCCCount][5];
			row[0][0] = rowNum;
			arrReprotectSeg[rowNum] = row[0];
			rowNum++;
		}
	}
	objDGFrom.arrGridData = arrReprotectSeg;
	objDGFrom.refresh();
	clearCellValues(rowNum);
}

function resetReprotect() {
	initValues();
	disableConfirmButtons(true);

	// Calling the RowClick method to reset the Reprotect - Other/To grid
	RowClick(strGridRow);

	// Code to reset the Reprotect - From grid
	resetChangeLogicalCC(parent.arrBaseArray,
			getValue("selLogicalCC"));

	getFieldByID("chkGA").checked = false;
	getFieldByID("chkSendSMS").checked = false;
	getFieldByID("chkSendEmail").checked = false;

	getFieldByID("chkSmsCnf").checked = false;
	getFieldByID("chkSmsOnH").checked = false;

	Disable("chkGA", true);
	Disable("btnReset", true);
	Disable("chkSendEmail", true);
	Disable("chkSendSMS", true);

	Disable("chkSmsCnf", true);
	Disable("chkSmsOnH", true);
}

/** ************************************************************************** */
/** * C O N F I R M A N D R O L L F O R W A R D B U T T O N E V E N T *** */
/** ************************************************************************** */
function confirmReprotectRollForward() {
	if (getFieldByID("chkGA").checked) {
		setField("hdnAlert", true);
	} else {
		setField("hdnAlert", false);
	}
	if (getFieldByID("chkSendSMS").checked) {
		setField("hdnSMSAlert", true);
	} else {
		setField("hdnSMSAlert", false);
	}
	if (getFieldByID("chkSendEmail").checked) {
		setField("hdnEmailAlert", true);
	} else {
		setField("hdnEmailAlert", false);
	}
	if (getFieldByID("chkSmsCnf").checked) {
		setField("hdnSmsCnf", true);
	} else {
		setField("hdnSmsCnf", false);
	}
	if (getFieldByID("chkSmsOnH").checked) {
		setField("hdnSmsOnH", true);
	} else {
		setField("hdnSmsOnH", false);
	}
	if (getFieldByID("chkSendEmailOriginAgent").checked) {
		setField("hdnEmailOriginAgent", true);
	} else {
		setField("hdnEmailOriginAgent", false);
	}
	if (getFieldByID("chkSendEmailOwnerAgent").checked) {
		setField("hdnEmailOwnerAgent", true);
	} else {
		setField("hdnEmailOwnerAgent", false);
	}
	
	var strSmsAlert = getFieldByID("chkSendSMS").checked;
	var strSmsCnf = getFieldByID("chkSmsCnf").checked;
	var strSmsOnH = getFieldByID("chkSmsOnH").checked;

	if (strSmsAlert) {
		if (!strSmsCnf && !strSmsOnH) {
			showWindowCommonError("Error", smsempty);
			document.getElementById("chkSmsCnf").focus();
			return;
		}
	}
	var sourceScheduleId = parent.arrBaseArray[0][8];
	var targetScheduleId = arrFlightsToReprotectData[strGridRow][11];
	if (sourceScheduleId == targetScheduleId) {
		showWindowCommonError("Error", sameScheduledFlight);
		return false;
	} else if ((sourceScheduleId == "") || (sourceScheduleId == "null")
			|| (targetScheduleId == "") || (targetScheduleId == "null")) {
		showWindowCommonError("Error", nonScheduledFlight);
		return false;
	} else {
		var intLeft = (window.screen.width - 1000) / 2;
		var intTop = (window.screen.height - 710) / 2;
		var winWidth = 850;
		var winheight = 380;
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
				+ winWidth
				+ ',height='
				+ winheight
				+ ',resizable=no,top='
				+ intTop + ',left=' + intLeft;
		if (!isParentOpener) {
			opener.opener.top[0].objRollForwardWindow = window.open(
					'showFile!reprotectRollForwad.action', 'CRollWindow', strProp);
		}else if(allowDisplayRollFwdBtn && isParentOpener){
			opener.top[0].objRollForwardWindow = window.open(
					'showFile!reprotectRollForwad.action', 'CRollWindow', strProp);
		}
	}
}

/** *************************************************************************** */
/** ********** C O N F I R M B U T T O N C L I C K E V E N T *************** */
/** *************************************************************************** */
function confirmReprotectPAX() {
	var proceed = false;
	var openEticketsInPastFlights = "";
	if(oldSegment != newSegment){
		proceed = confirm("Original and New Flights have different routes!. Are you sure you want to proceed ?");
	}
	else{
		proceed = true;
	}
	if(proceed){
		if (getFieldByID("chkGA").checked) {
			setField("hdnAlert", true);
		} else {
			setField("hdnAlert", false);
		}
	
		if (getFieldByID("chkSendSMS").checked) {
			setField("hdnSMSAlert", true);
		} else {
			setField("hdnSMSAlert", false);
		}
		if (getFieldByID("chkSendEmail").checked) {
			setField("hdnEmailAlert", true);
		} else {
			setField("hdnEmailAlert", false);
		}
		if (getFieldByID("chkSmsCnf").checked) {
			setField("hdnSmsCnf", true);
		} else {
			setField("hdnSmsCnf", false);
		}
		if (getFieldByID("chkSmsOnH").checked) {
			setField("hdnSmsOnH", true);
		} else {
			setField("hdnSmsOnH", false);
		}
		if (getFieldByID("chkSendEmailOriginAgent").checked) {
			setField("hdnEmailOriginAgent", true);
		} else {
			setField("hdnEmailOriginAgent", false);
		}
		if (getFieldByID("chkSendEmailOwnerAgent").checked) {
			setField("hdnEmailOwnerAgent", true);
		} else {
			setField("hdnEmailOwnerAgent", false);
		}
	
		var strSmsAlert = getFieldByID("chkSendSMS").checked;
		var strSmsCnf = getFieldByID("chkSmsCnf").checked;
		var strSmsOnH = getFieldByID("chkSmsOnH").checked;
	
		if (strSmsAlert) {
			if (!strSmsCnf && !strSmsOnH) {
				showWindowCommonError("Error", smsempty);
				document.getElementById("chkSmsCnf").focus();
				return;
			}
		}

		setField("hdnDepatureDateTime",parent.arrBaseArray[0][9]);

		if (getFieldByID("chkOpenEtickets")) {
			openEticketsInPastFlights = getFieldByID("chkOpenEtickets").checked;
		}
		setField("hdnOpenEtStatus", openEticketsInPastFlights);

		document.forms[0].target = "_self";
		document.forms[0].hdnMode.value = "CONFIRM.REPROTECT";
		document.forms[0].submit();
	}
}

/** *************************************************** */
/** *************** COMMON FUNCTIONS ****************** */
/** *************************************************** */
// To Handle Paging
function gridNavigations(recNo, intErrMsg) {
	if ((!isPageEdited()) || (isPageEdited() && isConfirmed())) {
		if (recNo <= 0) {
			recNo = 1;
		}
		setField("hdnRecNo", recNo);
		if (intErrMsg != "") {
			objMsg.MessageText = "<li> " + intErrMsg;
			objMsg.MessageType = "Error";
			ShowPageMessage();
		} else {
			setField("hdnFlightId", parent.arrBaseArray[0][6]);
			document.forms[0].target = "_self";
			document.forms[0].hdnMode.value = "SEARCH";
			document.forms[0].submit();
		}
	}
}

function setFieldValues(strFieldName, strValue) {
	setField(strFieldName, strValue);
}

function showErrorMsg(strType, strMsg) {
	objMsg.MessageText = strMsg;
	objMsg.MessageType = strType;
	ShowPageMessage();
}

function validateWindowTextField(textValue, msg) {
	var blnStatus = false;
	if (textValue == null || textValue == "") {
		objMsg.MessageText = msg;
		objMsg.MessageType = "Error";
		ShowPageMessage();
		blnStatus = true;
	}
	return blnStatus;
}

function validateTxtBox(objTextBox) {
	setPageEdited(true);
	var strLen = objTextBox.value.length;
	var strText = objTextBox.value;
	var blnVal = isPositiveInt(strText);
	if (!blnVal) {
		objTextBox.value = strText.substr(0, strLen - 1);
		objTextBox.focus();
	}
}

function disableConfirmButtons(val) {
	Disable("btnConfirm", val);
	Disable("btnCRF", val);
}

function initValues() {
	setField("hdnLogicalCabinClassCode", "");
	setField("hdnTargetFlightId", "");
	setField("hdnTargetSegmentCode", "");
	setField("hdnTargetFlightSegmentId", "");
	setField("hdnSourceFlightDistributions", "");
	setField("hdnSelectedPNR", "");
	setField("hdnSelectedSoldOhdCount", "");
}

function setPageEdited(isEdited) {
	pageEdited = isEdited;
}

function isPageEdited() {
	return pageEdited;
}

function closeClick() {
	if ((!isPageEdited()) || (isPageEdited() && isConfirmed())) {
		window.close();
	}
}

function isConfirmed() {
	var cnf = confirm("Changes will be lost! Do you wish to Continue?");
	if (cnf) {
		setPageEdited(false);
		return true;
	}
	return false;
}

function clearCellValues(count) {
	for ( var row = 0; row < count; row++) {
		objDGFrom.setCellValue(row, 4, "");
	}
}

function closeChildWindow() {
	if (!isParentOpener) {
		objWindow = opener.opener.top[0].objRollForwardWindow;
		if ((objWindow != "") && (objWindow != null) && (!objWindow.closed)) {
			objWindow.close();
		}
	}else if(allowDisplayRollFwdBtn && isParentOpener){
		objWindow = opener.top[0].objRollForwardWindow;
		if ((objWindow != "") && (objWindow != null) && (!objWindow.closed)) {
			objWindow.close();
		}
	}
}

function enableSMSChk() {

	if (document.getElementById("chkSendSMS").checked == true) {
		Disable("chkSmsCnf", false);
		Disable("chkSmsOnH", false);
	} else {
		Disable("chkSmsCnf", true);
		Disable("chkSmsOnH", true);

		if (document.getElementById("chkSmsCnf").checked == true) {
			document.getElementById("chkSmsCnf").checked = false;
		}
		if (document.getElementById("chkSmsOnH").checked == true) {
			document.getElementById("chkSmsOnH").checked = false;
		}
	}

}


function selectReprotectPNR(strRowNo) {
	var selectedRowNum = String(strRowNo);
	if(selectedRowNum!=null && selectedRowNum!=""){
		var intLeft = (window.screen.width - 1000) / 2;
		var intTop = (window.screen.height - 765) / 2;
		var winheight = 750;
		if (externalIntlFlightDetailCaptureEnabled == "true") {
			var winWidth = 1180;
		} else {
			var winWidth = 700;
		}
		

		var isTransferAll = objDGFrom.getSelectedColumn(3)[selectedRowNum][1];
		var strFlightId = objDGFrom.arrGridData[selectedRowNum][7]; // ok
		var selectCabinClass = getValue("selLogicalCC");
		
		var transferAll = 'N';
		
		if(isTransferAll){
			transferAll = 'Y';
		}				

		var strActionUrl = window.document.location.protocol;
		strActionUrl += "//"+window.document.location.host;
		strActionUrl += "/airadmin/private/master/";
		strActionUrl += "showFile!selectReprotectPNR.action";
		strActionUrl += "?hdnFlightSegId=" + strFlightId+'&hdnCabinClass='+selectCabinClass+'&hdnTransferAll='+transferAll+'&rowNo='+selectedRowNum;

		var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
				+ winWidth+ ',height='+ winheight+ ',resizable=no,top='+ intTop+ ',left=' + intLeft;
		opener.top[0].objRollForwardWindow = window.open(strActionUrl,"selectPnr", strProp);
	}
}