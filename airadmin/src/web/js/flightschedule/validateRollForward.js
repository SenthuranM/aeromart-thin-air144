var pageEdited = false;
function winOnLoad(strMsg, strMsgType) {
	// disable all the frequncy checkboxes
	// disableFrequncyAndClear(true);
	// getFieldByID("chkAllOccur").checked = true;

	// Getting the values from the DisplayReProtectFlights.jsp controls and
	// assign to ReprotectRollForward.jsp controls
	document.getElementById('spnFlightNo').innerHTML = opener.parent.arrBaseArray[0][2];
	document.getElementById('spnOrigin').innerHTML = opener.parent.arrBaseArray[0][4];
	document.getElementById('spnDest').innerHTML = opener.parent.arrBaseArray[0][3];
	setField("txtFromDate", opener.parent.arrBaseArray[0][7]);

	document.getElementById('spnFlightNoRe').innerHTML = opener.document
			.getElementById("spnFlightNoOther").innerHTML;
	document.getElementById('spnOriginRe').innerHTML = opener.document
			.getElementById("spnDepartureOther").innerHTML;
	document.getElementById('spnDestRe').innerHTML = opener.document
			.getElementById("spnArrivalOther").innerHTML;

	var strLogicalCCCode = opener.document.getElementById("hdnLogicalCabinClassCode").value;
	var strTargetFlightID = opener.document.getElementById("hdnTargetFlightId").value;
	var strTargetSegmentCode = opener.document
			.getElementById("hdnTargetSegmentCode").value;
	var strTargetFlightSegmentId = opener.document
			.getElementById("hdnTargetFlightSegmentId").value;
	var strSourceFlightDistributions = opener.document
			.getElementById("hdnSourceFlightDistributions").value;
	var flightId = opener.document.getElementById("hdnFlightId").value;
	var generateAlert = opener.document.getElementById("hdnAlert").value;

	setField("hdnLogicalCabinClassCode", strLogicalCCCode);
	setField("hdnTargetFlightId", strTargetFlightID);
	setField("hdnTargetSegmentCode", strTargetSegmentCode);
	setField("hdnTargetFlightSegmentId", strTargetFlightSegmentId);
	setField("hdnSourceFlightDistributions", strSourceFlightDistributions);
	setField("hdnFlightId", flightId);
	setField("hdnAlert", generateAlert);
}

function validateRollForward() {
	if (getValue("txtFromDate") == "") {
		showWindowCommonError("Error", opener.rollForwardFromDateRqrd);
		return;
	}

	if (getValue("txtTillDate") == null || getValue("txtTillDate") == "") {
		showWindowCommonError("Error", opener.rollForwardTillDateRqrd);
		getFieldByID("txtTillDate").focus();
		return;
	}

	var strFromDate = getValue("txtFromDate");
	var strTillDate = getValue("txtTillDate");
	var isTillDateLessthanFromDate = CheckDates(strTillDate, strFromDate);

	if (!(strFromDate == strTillDate)) {
		if (isTillDateLessthanFromDate) {
			showWindowCommonError("Error",
					opener.rollForwardTillDateLessthanFromDate);
			getFieldByID("txtTillDate").focus();
			return;
		}
	}

	if ((!getFieldByID("chkSunday").checked)
			&& (!getFieldByID("chkMonday").checked)
			&& (!getFieldByID("chkTuesday").checked)
			&& (!getFieldByID("chkWednesday").checked)
			&& (!getFieldByID("chkThursday").checked)
			&& (!getFieldByID("chkFriday").checked)
			&& (!getFieldByID("chkSaturday").checked)) {
		showWindowCommonError("Error", opener.rollForwardFrequncyInvalid);
		getFieldByID("chkMonday").focus();
		return;
	}
	return true;
}

function disableFrequncies(objCheck) {
	setPageEdited(true);
	if (objCheck.checked) {
		disableFrequncyAndClear(true);
	} else {
		disableFrequncyAndClear(false);
	}
}

function disableFrequncyAndClear(disable) {

}

function rollForwardReprotect() {
	if (!validateRollForward()) {
		return false;
	}
	Disable("btnRoll", true);
	Disable("txtFromDate", false);
	setField("hdnFlightId", getFieldByID("hdnFlightId").value);

	setField("hdnAlert",opener.document.getElementById("hdnAlert").value);
    setField("hdnSMSAlert",opener.document.getElementById("hdnSMSAlert").value);
    setField("hdnEmailAlert",opener.document.getElementById("hdnEmailAlert").value);
    setField("hdnSmsCnf",opener.document.getElementById("hdnSmsCnf").value);
    setField("hdnEmailAlert",opener.document.getElementById("hdnEmailAlert").value);
    setField("hdnSmsOnH",opener.document.getElementById("hdnSmsOnH").value);
    setField("hdnEmailOriginAgent",opener.document.getElementById("hdnEmailOriginAgent").value);
    setField("hdnEmailOwnerAgent",opener.document.getElementById("hdnEmailOwnerAgent").value);

	setField("hdnMode", "ROLLFORWARD");
	document.forms[0].target = "reprotectWindow";
	document.forms[0].submit();
}

var objWindow;
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.blnDragCalendar = false;
objCal1.buildCalendar();

function setDate(strDate, strID) {
	setPageEdited(true);
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtTillDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 1;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function setPageEdited(isEdited) {
	pageEdited = isEdited;
}

function isPageEdited() {
	return pageEdited;
}

function closeClick() {
	if ((!isPageEdited()) || (isPageEdited() && isConfirmed())) {
		window.close();
	}
}

function isConfirmed() {
	var cnf = confirm("Changes will be lost! Do you wish to Continue?");
	if (cnf) {
		setPageEdited(false);
		return true;
	}
	return false;
}

function dataChanged() {
	setPageEdited(true);
}