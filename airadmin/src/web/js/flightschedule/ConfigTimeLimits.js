
function UI_configTimeLimits() {} 


//UI_configTimeLimits.ready = function(){


$(document).ready(function(){

	UI_configTimeLimits.populateDropboxData();
	
	UI_configTimeLimits.constructConfigTimeLimitsGrid();
	
	UI_configTimeLimits.fillBCDropDown(); 
	
	top[2].HideProgress();
	
	//disableControls(true);
	
	$("#btnAddConfigTimeLmts").click(function() { 
		UI_configTimeLimits.addConfigOnClick();
	});
	
	$("#btnEditConfigTimeLmts").click(function() { 
		UI_configTimeLimits.editConfigOnClick();
	});
	
	$("#btnDeleteConfigTimeLmts").click(function() { 
		UI_configTimeLimits.deleteConfigOnClick();
	});
	
	$("#btnCloseConfigTimeLmts").click(function() { 
		UI_configTimeLimits.closeOnClick();
	});
	
	$("#btnResetConfigTimeLmts").click(function() { 
		UI_configTimeLimits.resetOnClick();
	});
	
	$("#btnSaveConfigTimeLmts").click(function() { 
		UI_configTimeLimits.saveConfigOnClick();
	});
	
	$("#btnSearchConfigTimeLmts").click(function() { 
		UI_configTimeLimits.searchRequestMadeOnClick();
	});
	
	$("#selCabinClass").change(function(){
		UI_configTimeLimits.fillBCDropDown();
	});
	
});


UI_configTimeLimits.populateDropboxData = function(){	
	UI_configTimeLimits.populateAllAirportsForSearch();	
	UI_configTimeLimits.populateAllAirportsToAddConfig();
}


UI_configTimeLimits.constructConfigTimeLimitsGrid = function(){	
	
	var mydata = null;
	$("#tblConfigOnholdTimeLimits").jqGrid({ 
		
		datatype: function(postdata) {			
			
			 postdata['timeLimitSearch.origin']= $('#selFromSrch').val();
			 postdata['timeLimitSearch.destination']= $('#selToSrch').val() ;
			 postdata['timeLimitSearch.flightType'] = $('#setFltTypeSrch').val();
			 postdata['timeLimitSearch.bookingClass'] = $('#selBookingClassSrch').val();
			 postdata['timeLimitSearch.agentCode'] = $('#selAgentCodeSrch').val();
			 postdata['timeLimitSearch.salesChannel'] = $('#selModuleCodeSrch').val();	
			 postdata['timeLimitSearch.ranking'] = $('#selRankSrch').val();
			 
	        jQuery.ajax({
	           url: 'showConfigOnholdTimeLimits.action',
	           data:postdata,
	           //data: "{}",
	           dataType:"json",
	           complete: function(jsonData,stat){	        	  
	              if(stat=="success") {
	            	 mydata = eval("("+jsonData.responseText+")");
	            	 $("#tblConfigOnholdTimeLimits")[0].addJSONData(mydata);            	
	              }
	           }
	        });
	    },
		jsonReader : {
			  root: "colOnholdTimeConfigs", 
			  page: "page",
			  total: "total",
			  records: "records",
			  repeatitems: false,				  
			  id: "0"				  
			},													
	    colNames:['&nbsp;','Id','Ond Code','Booking Class','Flight Type','Cabin Class','Agent Code','StartCutover Time','EndCutover Time','Release Time','Module Code', 'Rank' , 'Version','RelTimeWrt'],
        colModel:[
                {name:'id', width:5, jsonmap:'id' },
                 
                {name:'onholdTimeConfigsResultsTO.releaseTimeId',  index: 'releaseId', width:6, align:"center"},
                {name:'onholdTimeConfigsResultsTO.ondCode',  index: 'ondCode', width:13, align:"center"},
                {name:'onholdTimeConfigsResultsTO.bookingClass',  index: 'bookingClass', width:18, align:"center"},
                {name:'onholdTimeConfigsResultsTO.fltType',  index: 'fltType', width:15, align:"center"},
                
                {name:'onholdTimeConfigsResultsTO.cabinClass',  index: 'cabinClass', width:15, align:"center"},
                {name:'onholdTimeConfigsResultsTO.agentCode',  index: 'agentCode', width:15, align:"center"},
                
                {name:'onholdTimeConfigsResultsTO.startCutoverTime',  index: 'startCutoverTime', width:23, align:"center"},       
               	{name:'onholdTimeConfigsResultsTO.endCutoverTime',  index: 'endCutoverTime', width:21, align:"center"},
               	{name:'onholdTimeConfigsResultsTO.releaseTime',index: 'releaseTime', width:20, align:"center" },	
               	{name:'onholdTimeConfigsResultsTO.moduleCode',index: 'moduleCode', width:18, align:"center"},
               	{name:'onholdTimeConfigsResultsTO.rank',index: 'rank', width:6, align:"center"},
               	{name:'onholdTimeConfigsResultsTO.version',index: 'version', width:4, hidden:true , align:"center" },
            	{name:'onholdTimeConfigsResultsTO.relTimeWrt',index: 'version', width:12, align:"center" },
               	
    		],
		imgpath: '../../themes/default/images/jquery',
		multiselect:false,
		pager: $('#divConfigOnholdTimeLimitsPager'),
		rowNum:10,						
		viewrecords: true,
		height:180,
		width:920,	
		onSelectRow: function(rowid){
			UI_configTimeLimits.fillForm(rowid);
			disableControls(true);
			enableSearch();
			enableGridButtons();
		},
	   	loadComplete: function (e){	   
	   	},
	   	beforeRequest: function(){
	   	}	 
	}).navGrid("#divConfigOnholdTimeLimitsPager",{refresh: true, edit: false, add: false, del: false, search: false});
		
}


UI_configTimeLimits.addConfigOnClick = function () {
	$('#frmAddModifyConfigs').resetForm();
	disableControls(false);
	$('#btnEditConfigTimeLmts').disableButton();
	$('#btnDeleteConfigTimeLmts').disableButton();
	setField("hdnMode","ADD");
	setField("version","-1");
}


UI_configTimeLimits.editConfigOnClick = function () {
	disableControls(false);
	$('#btnDeleteConfigTimeLmts').disableButton();
	$("#btnSaveConfigTimeLmts").enableButton();
	setField("hdnMode","EDIT");
}


UI_configTimeLimits.deleteConfigOnClick = function () {
	disableControls(false);
	var cancelOptions = {
			cache : false,
			success : showDeleteResponse,
			url : 'showConfigOnholdTimeLimits!deleteOnholdTimeConfig.action',
			dataType : 'json',
			resetForm : false
	};
	
	$('#frmAddModifyConfigs').ajaxSubmit(cancelOptions);
	disableForm(true);
	return false;	
}

UI_configTimeLimits.closeOnClick = function () {
	top.LoadHome();
}

UI_configTimeLimits.resetOnClick = function () {
	$('#frmAddModifyConfigs').resetForm();
}

UI_configTimeLimits.saveConfigOnClick = function () {
	disableControls(false);
	var saveOptions = {
			cache : false,
			beforeSubmit: showRequest,
			success : showResponse,
			url : 'showConfigOnholdTimeLimits.action',
			dataType : 'json',
			resetForm : false
	};
	
	setField("hdnMode","ADD");
	if($("#hdnId").val()==""){
		setField("version","-1");
	}
	$('#frmAddModifyConfigs').ajaxSubmit(saveOptions);
	return false;	
}

//// sesOnlineAirportCodeListData
//
//// function to populate all online airports for search criteria
UI_configTimeLimits.populateAllAirportsForSearch = function (){	
	var objLB = new listBox();
	objLB.dataArray = arrOnlineAirports;
	var strID = "selFromSrch,selToSrch";
	objLB.id = strID;
	objLB.blnFirstEmpty = true;	// first value empty or not
	objLB.firstValue = "";		// first value
	objLB.firstTextValue = "";	// first Text
	objLB.fillListBox();
}

UI_configTimeLimits.populateAllAirportsToAddConfig = function (){	
	var objLB = new listBox();
	objLB.dataArray = arrOnlineAirports;
	var strID = "selFromOpt,selToOpt";
	objLB.id = strID;
	objLB.blnFirstEmpty = true;	// first value empty or not
	objLB.firstValue = "ANY";		// first value
	objLB.firstTextValue = "ANY";	// first Text
	objLB.fillListBox();
}


UI_configTimeLimits.fillForm = function(rowid) {
	
	
	setField("hdnId",$("#tblConfigOnholdTimeLimits").getCell(rowid,'onholdTimeConfigsResultsTO.releaseTimeId'));
	
	if($("#tblConfigOnholdTimeLimits").getCell(rowid,'onholdTimeConfigsResultsTO.fltType') == 'Domestic'){
		$("#selFltType").val("DOM");
	}else {
		$("#selFltType").val("INT");
	}
	
		
	if($("#tblConfigOnholdTimeLimits").getCell(rowid,'onholdTimeConfigsResultsTO.moduleCode') == 'XBE'){
		$("#selModuleCode").val("XBE");
	}else if($("#tblConfigOnholdTimeLimits").getCell(rowid,'onholdTimeConfigsResultsTO.moduleCode') == 'IBE'){
		$("#selModuleCode").val("IBE");
	}else if($("#tblConfigOnholdTimeLimits").getCell(rowid,'onholdTimeConfigsResultsTO.moduleCode') == 'API'){
		$("#selModuleCode").val("API");
	}else{
		$("#selModuleCode").val("ANY");
	}
	

	var ondCode = jQuery("#tblConfigOnholdTimeLimits").getCell(rowid,'onholdTimeConfigsResultsTO.ondCode');
	if(ondCode != "ANY"){
		var ondSpltArr = ondCode.split("/");
		$("#selFromOpt").val(ondSpltArr[0]);
		$("#selToOpt").val(ondSpltArr[1]);
	}else{
		$("#selFromOpt").val("ANY");
		$("#selToOpt").val("ANY");
	}
	
	$("#selBookingClass").val( jQuery("#tblConfigOnholdTimeLimits").getCell(rowid,'onholdTimeConfigsResultsTO.bookingClass') );
	
	$("#selCabinClass").val( jQuery("#tblConfigOnholdTimeLimits").getCell(rowid,'onholdTimeConfigsResultsTO.cabinClass') );
	
	$("#selAgentCode").val( jQuery("#tblConfigOnholdTimeLimits").getCell(rowid,'onholdTimeConfigsResultsTO.agentCode') );
	
	$("#selRank").val( jQuery("#tblConfigOnholdTimeLimits").getCell(rowid,'onholdTimeConfigsResultsTO.rank') );
	
	$("#startCutoverTime").val( jQuery("#tblConfigOnholdTimeLimits").getCell(rowid,'onholdTimeConfigsResultsTO.startCutoverTime') );
	
	$("#endCutoverTime").val( jQuery("#tblConfigOnholdTimeLimits").getCell(rowid,'onholdTimeConfigsResultsTO.endCutoverTime') );
	
	$("#releaseTime").val( jQuery("#tblConfigOnholdTimeLimits").getCell(rowid,'onholdTimeConfigsResultsTO.releaseTime') );
	
	$("#version").val( jQuery("#tblConfigOnholdTimeLimits").getCell(rowid,'onholdTimeConfigsResultsTO.version') );
	
	$("#selRelTimeRespectTo").val( jQuery("#tblConfigOnholdTimeLimits").getCell(rowid,'onholdTimeConfigsResultsTO.relTimeWrt') );
}


UI_configTimeLimits.validateSearch = function (){
	return true;
}


UI_configTimeLimits.searchRequestMadeOnClick = function (){
	
	if(UI_configTimeLimits.validateSearch()) {	
							
		var newUrl = 'showConfigOnholdTimeLimits.action?';
		newUrl += 'page=' + '0';
	 	newUrl += '&timeLimitSearch.origin=' + $("#selFromSrch").val();
	 	newUrl += '&timeLimitSearch.destination=' + $("#selToSrch").val();
	 	newUrl += '&timeLimitSearch.flightType=' + $("#setFltTypeSrch").val();
	 	newUrl += '&timeLimitSearch.bookingClass=' + $("#selBookingClassSrch").val();
	 	newUrl += '&timeLimitSearch.agentCode=' + $("#selAgentCodeSrch").val();
	 	newUrl += '&timeLimitSearch.salesChannel=' + $("#selModuleCodeSrch").val();
	 	newUrl += '&timeLimitSearch.ranking=' + $("#selRankSrch").val();
	 	
	 	
	 	$("#tblConfigOnholdTimeLimits").trigger("reloadGrid");
	 	$("#divConfigOnholdTimeLimitsPager").clearGridData();
	 	
	 	$.getJSON(newUrl, function(jsonData,stat){ 
	 		var mydata = eval("("+jsonData.responseText+")");
	 		$("#tblConfigOnholdTimeLimits")[0].addJSONData(mydata); 		
	 	});	
	}
	
}


function showResponse(responseText, statusText)  {
	top[2].HideProgress();
	showCommonError(responseText['msgType'], responseText['messageTxt']);
    if(responseText['msgType'] != "false") {
    	$('#frmAddModifyConfigs').clearForm();
    	jQuery("#tblConfigOnholdTimeLimits").trigger("reloadGrid");
    	$("#btnAddConfigTimeLmts").attr('disabled', false); 
    	$("#btnSaveConfigTimeLmts").enableButton();
    	alert("Record Successfully added");
    }			    
} 		

function showDeleteResponse(responseText, statusText)  {
	top[2].HideProgress();
	showCommonError(responseText['msgType'], responseText['messageTxt']);
    if(responseText['msgType'] != "false") {
    	$('#frmAddModifyConfigs').clearForm();
    	jQuery("#tblConfigOnholdTimeLimits").trigger("reloadGrid");
    	$("#btnAddConfigTimeLmts").attr('disabled', false); 
    	$("#btnSaveConfigTimeLmts").enableButton();
    	alert("Record Deleted");
    }			    
} 	

UI_configTimeLimits.fillBCDropDown = function(){
	var cos = $("#selCabinClass").val().split("-")[0];	
	var bookingClassCode =  new Array();
	var count = 0;
	if($("#selCabinClass").val() == "ANY"){
		var bcCodeArr = new Array();
		bcCodeArr[0] = "ANY";
		bcCodeArr[1] = "ANY";
		bookingClassCode[count] = bcCodeArr;
		count++;
	}
	
	for(var i = 0;i<arrBookingClassCode.length;i++){
		if($("#selCabinClass").val() == "ANY"){
			var bcCodeArr = new Array();
			bcCodeArr[0] = arrBookingClassCode[i][0];
			bcCodeArr[1] = arrBookingClassCode[i][3];
			bookingClassCode[count] = bcCodeArr;
			count++;
		}else if($("#selCabinClass").val().split("-")[1] == null){
			if(arrBookingClassCode[i][2] == cos){
				var bcCodeArr = new Array();
				bcCodeArr[0] = arrBookingClassCode[i][0];
				bcCodeArr[1] = arrBookingClassCode[i][3];
				bookingClassCode[count] = bcCodeArr;
				count++;
			}
		}else if(arrBookingClassCode[i][1] == cos){
			var bcCodeArr = new Array();
			bcCodeArr[0] = arrBookingClassCode[i][0];
			bcCodeArr[1] = arrBookingClassCode[i][3];
			bookingClassCode[count] = bcCodeArr;
			count++;
		}			
	}
$("#selBookingClass").empty();
$("#selBookingClass").fillDropDown({ dataArray:bookingClassCode , keyIndex:0 , valueIndex:1, firstEmpty: false });
}	


//pre-submit callback 
function showRequest(formData, jqForm, options) { 
	
    if(	trim($("#selBookingClass").val()) == "") {			    	
    	showCommonError("Error", "Enter a booking class...");
    	return false; 
    }if(	trim($("#selCabinClass").val()) == "") {			    	
    	showCommonError("Error", "Enter a cabin class...");
    	return false; 
    }if(	trim($("#selFltType").val()) == "") {			    	
    	showCommonError("Error", "Enter a flight type...");
    	return false; 
    }if(	trim($("#selAgentCode").val()) == "") {			    	
    	showCommonError("Error", "Enter an agent code...");
    	return false; 
    }if(	trim($("#selModuleCode").val()) == "") {			    	
    	showCommonError("Error", "Enter a module code...");
    	return false; 
    }if(	trim($("#selRank").val()) == "") {			    	
    	showCommonError("Error", "Enter a rank....");
    	return false; 
    }if(	trim($("#startCutoverTime").val()) == "") {			    	
    	showCommonError("Error", "Enter a startcutover time...");
    	return false; 
    }if(	trim($("#endCutoverTime").val()) == "") {			    	
    	showCommonError("Error", "Enter a endcutover time...");
    	return false; 
    }if(	trim($("#releaseTime").val()) == "") {			    	
    	showCommonError("Error", "Enter a release time...");
    	return false; 
    }if(	trim($("#selRelTimeRespectTo").val()) == "") {    	
    	showCommonError("Error", "Enter a Release Time Respect to value....");
    	return false; 
    }if( trim($("#selFromOpt").val()) == trim($("#selToOpt").val()) ){
    	if(trim($("#selFromOpt").val()) != "ANY"){
    	showCommonError("Error", "Origin and Destination cannot be the same...");
    	return false;
    	}
    }
    
    top[2].ShowProgress();
    return true; 
} 


function  disableControls(cond){
	$("input").each(function(){ 
	      $(this).attr('disabled', cond); 
	});	
	$("textarea").each(function(){ 
	      $(this).attr('disabled', cond); 
	});	
	$("select").each(function(){ 
	      $(this).attr('disabled', cond); 
	});
	$("file").each(function(){ 
	      $(this).attr('disabled', cond); 
	});	
	$("#btnCloseConfigTimeLmts").enableButton();
	
}


function enableSearch(){
	$('#selFromSrch').attr("disabled", false); 
	$('#selToSrch').attr("disabled", false); 
	$('#selBookingClassSrch').attr("disabled", false);
	$('#setFltTypeSrch').attr("disabled", false); 
	$('#selAgentCodeSrch').attr("disabled", false);
	$('#selModuleCodeSrch').attr("disabled", false);
	$('#selAgentCodeSrch').attr("disabled", false);
	$('#selRankSrch').attr("disabled", false);
	$('#btnSearchConfigTimeLmts').enableButton();
}


function enableGridButtons(){
	$("#btnAddConfigTimeLmts").enableButton();
	$("#btnDeleteConfigTimeLmts").enableButton();
	$("#btnEditConfigTimeLmts").enableButton();
	$("#btnResetConfigTimeLmts").enableButton();
}

function disableForm(cond){
	$("#selFromOpt").attr('disabled',cond);
	$("#selToOpt").attr('disabled',cond);
	$("#selBookingClass").attr('disabled',cond);
	$("#selCabinClass").attr('disabled',cond);
	$("#selFltType").attr('disabled',cond);
	$("#selAgentCode").attr('disabled',cond); 	
	$("#selModuleCode").attr('disabled',cond); 
	$("#selRank").attr('disabled',cond); 
	$("#startCutoverTime").attr('disabled',cond); 
	$("#endCutoverTime").attr('disabled',cond); 
	$("#releaseTime").attr('disabled',cond); 
	$("#btnSaveConfigTimeLmts").attr('disabled',cond); 	
};




