var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.buildCalendar();
var strMode = getText("hdnMode");
var strRowData;
var strGridRow;
var strLegs;
var strLegDtls;
var strOverlap;
var blnZuluTime = true;
var blngrdselected = false;
var arrLegs;
var blnCalander = false;
var blnvalseg = false;
var strTopValue;
var screenID = "SC_SCHD_001";
var isClickOnTerminal = false;
var noYears = 10; 

var FLT_TYPE_DESC = {"INT":"INT", "DOM":"DOM"};

// On Page loading make sure that all the page input fields are cleared
function winOnLoad(strMsg, strMsgType) {
	closeChildWindows();
	setField("hdnReprotect", "false");
	setField("txtFlightNoStart", defCarrCode);
	setField("txtFlightNoStartSearch", defCarrCode);	
	document.getElementById("frmSchedule").target = "_self";
	populateSelDay();
	populateAllAirportsForSearch();
	onLoadPopulateAllAirports();
	if (getFieldByID("hdnMode").value == "ADD") {
		onLoadPopulateAirports();
	}
	clearSearchSection();
	setField("selOperationTypeSearch", defaultOperationType); // Search
	setField("selStatusSearch", defaultStatus);
	// Search
	strTopValue = top[1].objTMenu.tabGetValue("SC_SCHD_001").split("^");
	var strSearchCriteria = strTopValue[1];
	
	if(multipleFltTypeVisibility.onlySingle){
		setDisplay("selFlightTypeSearch",false);
		setDisplay("flightTypeLbl",false);
		setDisplay("selectFltType",false);
	}
	
	if (strSearchCriteria != "" && strSearchCriteria != null) {

		var strSearch = strSearchCriteria.split(",");
		setField("txtStartDateSearch", strSearch[0]);
		setField("txtStopDateSearch", strSearch[1]);
		setField("txtFlightNoSearch", strSearch[2]);
		setField("txtFlightNoStartSearch", strSearch[8]);
		setField("selFromStn6", strSearch[3]);
		getFieldByID("selFromStn6").selected = strSearch[3];
		setField("selToStn6", strSearch[4]);
		getFieldByID("selToStn6").selected = strSearch[4];
		setField("selOperationTypeSearch", strSearch[5]);
		getFieldByID("selOperationTypeSearch").selected = strSearch[5];
		setField("selStatusSearch", strSearch[6]);
		getFieldByID("selStatusSearch").selected = strSearch[6];
		setField("selBuildStatusSearch", strSearch[7]);
		getFieldByID("selBuildStatusSearch").selected = strSearch[7];
		if (strSearch[9] == "LOCAL") {			
			setField("hdnTimeMode", "LOCAL");
		} else {			
			setField("hdnTimeMode", "ZULU");
		}
		setField("selFlightTypeSearch", strSearch[10]);
		
		
	}
	clearInputSection();
	clearLegSection();

	disableInputSection(true);
	disableLegSection(true);
	disableTerminalSection(true);
	disableSaveResetButtons(true);
	Disable("btnMsgHistory", true);
	Disable("btnPubMessages", true);	
	$('#btnMsgHistory').hide();
	$('#btnPubMessages').hide();	
	if (arrData.length == 0) {
		Disable("chkSelectAll", true);
	}
	if (top[1].objTMenu.focusTab == "SC_SCHD_001") {
		getFieldByID("txtStartDateSearch").focus();
	}

	if (strMsg != null && strMsg != "" && strMsgType != null
			&& strMsgType != "") {
		showCommonError(strMsgType, strMsg);
		if (strMsgType == "Error") {

			populateValuesForSeatChargeTemplate();
			// set the model and enabling the input section
			setModel(model);
			disableInputSection(false);
			disableLegSection(false);
			disableTerminalSection(false);
			Disable("selD_Day1", true);
			disableSaveResetButtons(false);

			if (getFieldByID("hdnTimeMode").value == "LOCAL") {
				document.forms[0].radTZ[0].checked = true;
				blnZuluTime = false;

			} else if (getFieldByID("hdnTimeMode").value == "ZULU") {
				document.forms[0].radTZ[1].checked = true;
				blnZuluTime = true;
			}

			// setting the duration errors
			var legsdrr;
			if (hasdurError == "true") {
				var errlegno;
				for ( var i = 1; i < duration.length; i++) {
					errlegno = "<font>" + duration[i][0] + " </font>";
					document.getElementById('spnDuration' + i).innerHTML = errlegno;
					if (duration[i][1] != "0") {
						document.getElementById('spnError' + i).innerHTML = "<font class='mandatory'> X </font>";
					}
				}
			}
			var strChkmode = getFieldByID("hdnMode").value;
			if (strChkmode == "UPDATE" || strChkmode == "UPDATE.CANCEL") {
				disableLegSection(true);
				setField("hdnMode", "UPDATE");
			} else if (strChkmode == "EDITLEG"
					|| strChkmode == "UPDATE.CANCELLEG") {
				disableInputSection(true);
				setField("hdnMode", "EDITLEG");
			} else if (strChkmode == "ADD") {

			} else if (strChkmode == "CANCELFORCE") {
				disableLegSection(true);
				disableInputSection(false);
				setField("hdnMode", "CANCEL");
			} else {
				disableLegSection(true);
				disableInputSection(true);
			}
			strMode = getFieldByID("hdnMode").value;
			validateerrors();
			Disable("btnReset", false);
			Disable("btnSave", false);
		}

	}

	disableCommonButtons(true);
	if (reprotect == "true") {
		setModel(model);
		var strChkmode = getFieldByID("hdnMode").value;
		if (strChkmode == "UPDATE") {
			disableInputSection(false);

		} else if (strChkmode == "EDITLEG") {
			disableLegSection(false);
			Disable("btnValidseg", false);
		}
		strMode = getFieldByID("hdnMode").value;
		if (getFieldByID("hdnTimeMode").value == "LOCAL") {
			document.forms[0].radTZ[0].checked = true;
			blnZuluTime = false;

		} else if (getFieldByID("hdnTimeMode").value == "ZULU") {
			document.forms[0].radTZ[1].checked = true;
			blnZuluTime = true;
		}
		validateerrors();
		top[0].strReprotectFrom = "SCHEDULE";
		var intWidth = 720;
		var intHeight = 475;
		var intLeft = (window.screen.width - 1000) / 2;
		var intTop = (window.screen.height - 710) / 2;
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
				+ intWidth
				+ ',height='
				+ intHeight
				+ ',resizable=no,top='
				+ intTop + ',left=' + intLeft;
		top[0].objCancelWindow = window.open("showFile!cancelFlight.action",
				"cancelWindow", strProp);
	}
	if ((strMsg != null && strMsg != "" && strMsgType != null
			&& strMsgType != "" && strMsgType == "Error")
			|| (reprotect == "true")) {

	} else {
		setPageEdited(false);
	}
	// disable code share view
	if(isCodeShareEnable  == 'false'){
		setDisplay("CodeShareOC",false);
		setDisplay("Table3",false);
	}
	
	// show popup
	if (trim(popupmessage).length != 0) {
		alert(popupmessage);
		setPageEdited(false);
		clearInputSection();
		clearLegSection();

		disableInputSection(true);
		disableLegSection(true);
	}

	setDefaultTimeMode();
	showFlightOpBy();
	
}
//to Show the operated by drop down
function showFlightOpBy(){
	if (defCarrCode==='D0'){
		document.getElementById("selOpBy").setAttribute("style", "display:inline;width:40px");
		document.getElementById("fntOpBy").setAttribute("style", "display:inline");
	}
}

function getValidatedTerminals() {	 
	return getFieldByID("hdnTerminalArray").value;	  
}
var resetDefaultTimeMode = function(){
	if(isTimeInZulu){
		document.frmSchedule.radTZ[1].checked = true;
		setZuluTime();
	}else{
		document.frmSchedule.radTZ[0].checked = true;
		setLocalTime();
	}
}

function validateerrors() {
	if (getFieldByID("hdnMode").value != "ADD"
			&& getFieldByID("hdnGridRow").value != "") {
		strGridRow = getFieldByID("hdnGridRow").value;
		if (arrData[strGridRow][11] != getFieldByID("hdnScheduleId").value) {
			disableLegSection(true);
			disableInputSection(true);
			disableInputSection(true);
			disableLegSection(true);
		} else {
			document.getElementById('spnSchedId').innerHTML = arrData[strGridRow][11];
			document.getElementById('spnStatus').innerHTML = arrData[strGridRow][12];
			blngrdselected = true;
			var ModelSelected = arrData[strGridRow][41];
			for ( var i = 0; i < arrAircraftModelData.length; i++) {
				if ((ModelSelected != "")
						&& (ModelSelected == arrAircraftModelData[i][0])) {
					document.getElementById('spnCapacity').innerHTML = arrAircraftModelData[i][1];
				}
			}
			if (arrData[strGridRow][8] == "Y")
				setStyleClass("tdSchdOverlap", "fltStatus02");
			if (arrData[strGridRow][9] > 0)
				setStyleClass("tdFltExists", "fltStatus04");
			if (arrData[strGridRow][24] == "true")
				setStyleClass("tdFltChanged", "fltStatus03");
			if (arrData[strGridRow][10] == "BLT") {
				dissableAirports();
			}

		}
	}
}

// populate the days in the leg section
function populateSelDay() {
	var arrDay = new Array();
	arrDay[0] = new Array(-1, -1);
	arrDay[1] = new Array(1, "+1");
	var objLB = new listBox();
	objLB.dataArray = arrDay;
	var intDD = 5;
	var strID = "";

	for ( var i = 1; i <= intDD; i++) {
		if (strID != "") {
			strID += ",";
		}
		strID += "selD_Day" + i;
	}
	for ( var i = 1; i <= intDD; i++) {
		if (strID != "") {
			strID += ",";
		}
		strID += "selA_Day" + i;
	}
	objLB.id = strID;
	objLB.blnFirstEmpty = true; // first value empty or not
	objLB.firstValue = "0"; // first value
	objLB.firstTextValue = "0"; // first Text
	objLB.fillListBox();
}

// function to populate all online active airports
function onLoadPopulateAirports() {
	var objLB = new listBox();
	objLB.dataArray = arrAptData;
	var intDD = 5;
	var strID = "";

	for ( var i = 1; i <= intDD; i++) {
		if (strID != "") {
			strID += ",";
		}
		strID += "selFromStn" + i;
	}
	for ( var i = 1; i <= intDD; i++) {
		if (strID != "") {
			strID += ",";
		}
		strID += "selToStn" + i;
	}
	objLB.id = strID;
	objLB.blnFirstEmpty = true; // first value empty or not
	objLB.firstValue = ""; // first value
	objLB.firstTextValue = ""; // first Text
	objLB.fillListBox();
}

// function to populate all online airports
function onLoadPopulateAllAirports() {
	var objLB = new listBox();
	objLB.dataArray = arrOnlineAirports;
	var intDD = 5;
	var strID = "";

	for ( var i = 1; i <= intDD; i++) {
		if (strID != "") {
			strID += ",";
		}
		strID += "selFromStn" + i;
	}
	for ( var i = 1; i <= intDD; i++) {
		if (strID != "") {
			strID += ",";
		}
		strID += "selToStn" + i;
	}
	objLB.id = strID;
	objLB.blnFirstEmpty = true; // first value empty or not
	objLB.firstValue = ""; // first value
	objLB.firstTextValue = ""; // first Text
	objLB.fillListBox();
}

// function to populate all online airports for search criteria
function populateAllAirportsForSearch() {
	var objLB = new listBox();
	objLB.dataArray = arrOnlineAirports;
	var strID = "selFromStn6,selToStn6";
	objLB.id = strID;
	objLB.blnFirstEmpty = true; // first value empty or not
	objLB.firstValue = ""; // first value
	objLB.firstTextValue = ""; // first Text
	objLB.fillListBox();
	writeSelectAll();
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
	closeChildWindows();
}

// function to open the popup windows
function CWindowOpen(strIndex) {
	objOnFocus();
	setField("hdnReprotect", "false");
	beforeUnload();
	document.getElementById("frmSchedule").target = "_self";
	var intWidth = 0;
	var intHeight = 0;
	var strProp = '';
	var strSchdId = getFieldByID("hdnScheduleId").value;
	var strStartD = getFieldByID("txtStartDate").value;
	var strStopD = getFieldByID("txtStopDate").value;
	var modelNo = getFieldByID("selAircraftModel").value;
	var strOpType = getFieldByID("selOperationType").value;
	var strBldStatus = getFieldByID("hdnBulidStatus").value;
	var strOverlapSchID = getFieldByID("hdnOverlapSchID").value;

	var strTimeinMode = "";
	if (document.forms[0].radTZ[1].checked == true)
		strTimeinMode = "ZULU";
	else
		strTimeinMode = "LOCAL";

	if (strMode == "UPDATE") {
		strSaveMode = "UPDATE";
	} else if (strMode == "EDITLEG") {
		strSaveMode = "EDITLEG";
	} else {
		strSaveMode = "SAVE";
	}

	if (document.getElementById("chkSunday").checked == true) {
		var strSunday = "true";
	}
	if (document.getElementById("chkMonday").checked == true) {
		var strMonday = "true";
	}
	if (document.getElementById("chkTuesday").checked == true) {
		var strTuesday = "true";
	}
	if (document.getElementById("chkWednesday").checked == true) {
		var strWednesday = "true";
	}
	if (document.getElementById("chkThursday").checked == true) {
		var strThursday = "true";
	}
	if (document.getElementById("chkFriday").checked == true) {
		var strFriday = "true";
	}
	if (document.getElementById("chkSaturday").checked == true) {
		var strSaturday = "true";
	}

	if (document.forms[0].radTZ[1].checked == true)
		document.forms[0].hdnTimeMode.value = "ZULU";
	else
		document.forms[0].hdnTimeMode.value = "LOCAL";
	top[1].objTMenu
			.tabSetValue("SC_SCHD_001", "1^" + getText("txtStartDateSearch")
					+ "," + getText("txtStopDateSearch") + ","
					+ getText("txtFlightNoSearch") + ","
					+ getValue("selFromStn6") + "," + getValue("selToStn6")
					+ "," + getValue("selOperationTypeSearch") + ","
					+ getValue("selStatusSearch") + ","
					+ getValue("selBuildStatusSearch") + ","
					+ getText("txtFlightNoStartSearch") + ","
					+ getValue("hdnTimeMode") + ","
					+ getValue("selFlightTypeSearch"));

	var strFilename = "";
	switch (strIndex) {
	case 0:
		strFilename = "showFile!splitSchedule.action";
		intHeight = 330;
		intWidth = 600;
		break;
	// Copy Schedule
	case 1:
		strFilename = "showFile!copySchedule.action";
		intHeight = 330;
		intWidth = 600;
		break;
	// Validate Segments
	case 2:
		var stLegrArray = getLegs().split("~");
		strLegs = stLegrArray[0];
		strLegDtls = stLegrArray[1];
		strFilename = "showSegmentValidation.action?strSchdId=" + strSchdId
				+ "&strLegs=" + strLegs + "&strLegDtls=" + strLegDtls
				+ "&strSaveMode=" + strSaveMode + "&strBldStatus="
				+ strBldStatus + "&strStartDate=" + strStartD + "&strStopDate="
				+ strStopD + "&strSunday=" + strSunday + "&strMonday="
				+ strMonday + "&strTuesday=" + strTuesday + "&strWednesday="
				+ strWednesday + "&strThursday=" + strThursday + "&strFriday="
				+ strFriday + "&strSaturday=" + strSaturday + "&strModel="
				+ modelNo + "&strOpType=" + strOpType + "&hdnOverlapSchID="
				+ strOverlapSchID + "&hdnTimeMode=" + strTimeinMode
				+ "&strFromPage=SCHEDULE";
		intHeight = 350;
		intWidth = 770;
		break;
	case 3 :
		isClickOnTerminal = true;
		var stLegrArray = getLegs().split("~");
		strLegs = stLegrArray[0];
		strLegDtls = stLegrArray[1];
		strFilename = "showFlightTerminalValidation.action?strSchdId="+strSchdId
		+ "&strLegs=" + strLegs + "&strLegDtls=" + strLegDtls
		+ "&strSaveMode=" + strSaveMode + "&strBldStatus="
		+ strBldStatus + "&strStartDate=" + strStartD + "&strStopDate="
		+ strStopD + "&strSunday=" + strSunday + "&strMonday="
		+ strMonday + "&strTuesday=" + strTuesday + "&strWednesday="
		+ strWednesday + "&strThursday=" + strThursday + "&strFriday="
		+ strFriday + "&strSaturday=" + strSaturday + "&strModel="
		+ modelNo + "&strOpType=" + strOpType + "&hdnOverlapSchID="
		+ strOverlapSchID + "&hdnTimeMode=" + strTimeinMode
		+ "&strFromPage=SCHEDULE";
		intHeight = 300;
		intWidth = 620;
		break;
	}

	strProp = 'toolbar=no,location=no,status=yes,menubar=no,scrollbars=no,width='
			+ intWidth
			+ ',height='
			+ intHeight
			+ ',resizable=no,top='
			+ ((window.screen.height - intHeight) / 2)
			+ ',left='
			+ (window.screen.width - intWidth) / 2;
	top[0].objWindow = window.open(strFilename, "CWindow", strProp);
}

function PreviousMonth() {
	objOnFocus();
	var strStartD = getFieldByID("txtStartDateSearch").value;
	var strStopD = "";
	var strDaysStopDate = "";
	setField("txtStartDateSearch", addMonths(-1, "01" + strStartD.substr(2, 8)));
	strStopD = addMonths(-1, "01" + strStartD.substr(2, 8));
	strDaysStopDate = getDaysInMonth(strStopD.substr(3, 2), strStopD.substr(6,
			4));
	setField("txtStopDateSearch", addMonths(0, strDaysStopDate
			+ strStopD.substr(2, 8)));
}
function NextMonth() {
	objOnFocus();
	var strStartD = getFieldByID("txtStartDateSearch").value;
	var strStopD = "";
	var strDaysStopDate = "";
	setField("txtStartDateSearch", addMonths(1, "01" + strStartD.substr(2, 8)));
	strStopD = addMonths(1, "01" + strStartD.substr(2, 8));
	strDaysStopDate = getDaysInMonth(strStopD.substr(3, 2), strStopD.substr(6,
			4));
	setField("txtStopDateSearch", addMonths(0, strDaysStopDate
			+ strStopD.substr(2, 8)));
}

// Validtaions when searching
function SearchValid() {
	objOnFocus();
	var msg = "";
	if (trim(getVal("txtStartDateSearch")) == "") {
		showERRMessage(mandatorysearch);
		getFieldByID("txtStartDateSearch").focus();
		return false;
	} else {
		if (trim(getVal("txtStopDateSearch")) == "") {
			showERRMessage(mandatorysearch);
			getFieldByID("txtStopDateSearch").focus();
			return false;
		} else {
			// compare start date and stop date
			var schStartD = formatDate(getVal("txtStartDateSearch"),
					"dd/mm/yyyy", "dd-mmm-yyyy");
			var schStopD = formatDate(getVal("txtStopDateSearch"),
					"dd/mm/yyyy", "dd-mmm-yyyy");

			if (!dateChk("txtStartDateSearch")) {
				showERRMessage(splinvaliddate);
				getFieldByID("txtStartDateSearch").focus();
				return false;
			}
			if (!dateChk("txtStopDateSearch")) {
				showERRMessage(splinvaliddate);
				getFieldByID("txtStopDateSearch").focus();
				return false;
			}
			if (compareDates(schStartD, schStopD, '>=')) {
				showERRMessage(startdateless);
				getFieldByID("txtStopDateSearch").focus();
				return false;
			}

			var schcarrier = trim(getVal("txtFlightNoStartSearch"));
			var blnValid = false;
			for ( var cl = 0; cl < arrCarriers.length; cl++) {
				if (arrCarriers[cl] == schcarrier) {
					blnValid = true;
					break;
				}
			}
			if (schcarrier != "" && !blnValid) {
				showERRMessage(invalidcarrier);
				getFieldByID("txtFlightNoStartSearch").focus();
				return false;
			}
		}
	}
}

// Clear all input controls in search section
function clearSearchSection() {
	setField("txtFlightNoSearch", "");
	setField("selFromStn6", "");
	setField("selToStn6", "");

}

// Clear all input controls in input section
function clearInputSection() {
	setField("txtStartDate", "");
	setField("txtStopDate", "");
	document.getElementById('spnStatus').innerHTML = "";
	document.getElementById('spnSchedId').innerHTML = "";
	setField("selOperationType", "");
	setField("txtFlightNo", "");
	setField("selAircraftModel", "");
	setField("txtFlightNoStart", "");
	
	if(multipleFltTypeVisibility.onlySingle){
		setField("selFlightType", multipleFltTypeVisibility.enabledType);
	}else{
		setField("selFlightType", '');
	}

	document.getElementById('spnCapacity').innerHTML = "";
	clearFrequency();

	setStyleClass("tdFltExists", "fltStatus01");
	setStyleClass("tdSchdOverlap", "fltStatus01");
	setStyleClass("tdFltChanged", "fltStatus01");

	setField("hdnOverLapSeg", "");
	setField("hdnOverLapSegId", "")
	setField("hdnOverLapSegSchedId", "");
	setField("hdnSegArray", "");
	setField("hdnTerminalArray", "");
	setField("hdnScheduleId", "");
	setField("hdnOverlapSchID", "");
	setField("hdnBulidStatus", "");
	setField("hdnStatus", "");
	setField("txtRemarks", "");	
	setField("selSeatChargeTemplate", "");	
	setField("selMealTemplate", "");	
	setField("hndCodeShare", "");
	setField("hndCsOCCarrierCode", "");
	setField("hndCsOCFlightNo", "");
	document.getElementById('sel_CodeShare').innerHTML = "";
	setField("txtCsOCCarrierCode", "");
	setField("txtCsOCFlightNo", "");


}

function clearFrequency() {

	document.getElementById('chkMonday').checked = false;
	document.getElementById('chkTuesday').checked = false;
	document.getElementById('chkWednesday').checked = false;
	document.getElementById('chkThursday').checked = false;
	document.getElementById('chkFriday').checked = false;
	document.getElementById('chkSaturday').checked = false;
	document.getElementById('chkSunday').checked = false;
	document.getElementById('chkAll').checked = false;
}

// Clear all input controls in Leg Details section
function clearLegSection() {
	for ( var k = 1; k < 6; k++) {
		setField("selFromStn" + k, "");
		setField("selToStn" + k, "");
		setField("txtD_Time" + k, "");
		setField("txtA_Time" + k, "");
		setField("selD_Day" + k, "0");
		setField("selA_Day" + k, "0");
		
		document.getElementById('spnDuration' + k).innerHTML = "";
		document.getElementById('spnError' + k).innerHTML = "";
	}
}

// Disable input section
function disableInputSection(cond) {
	Disable("txtStartDate", cond);
	Disable("txtStopDate", cond);
	Disable("selOperationType", cond);
	Disable("txtFlightNo", cond);
	Disable("selAircraftModel", cond);
	Disable("chkMonday", cond);
	Disable("chkTuesday", cond);
	Disable("chkWednesday", cond);
	Disable("chkThursday", cond);
	Disable("chkFriday", cond);
	Disable("chkSaturday", cond);
	Disable("chkSunday", cond);
	Disable("txtFlightNoStart", cond);
	Disable("chkAll", cond);
	Disable("selFlightType", cond);
	Disable("txtRemarks", cond);
	Disable("selSeatChargeTemplate", cond);
	Disable("selMealTemplate", cond);
	Disable("carrier", cond);
	Disable("flightnum", cond);
	document.getElementById("add_CodeShare").disabled = cond;
	document.getElementById("del_CodeShare").disabled = cond;
	document.getElementById("sel_CodeShare").disabled=cond;
	
	if (cond == true) {
		blnCalander = false;
	} else {
		blnCalander = true;
	}

}
// Enable input section

// Disable leg section
function disableLegSection(cond) {
	Disable("btnValidseg", cond);
	for ( var k = 1; k < 6; k++) {
		Disable("selFromStn" + k, cond);
		Disable("selToStn" + k, cond);
		Disable("txtD_Time" + k, cond);
		Disable("txtA_Time" + k, cond);
		Disable("selD_Day" + k, cond);
		Disable("selA_Day" + k, cond);
	}
}
//Disable Terminal section
function disableTerminalSection(cond) {
	Disable("btnValidTerminal", cond);	
}
// Disable common buttons on Grid Section
function disableCommonButtons(cond) {
	Disable("btnEdit", cond);
	Disable("btnCancel", cond);
	Disable("btnSplit", cond);
	Disable("btnCopy", cond);
	Disable("btnBuild", cond);
	Disable("btnFlight", cond);
	Disable("btnFlightMnt", cond);
	Disable("btnValidseg", cond);
	Disable("btnValidTerminal", cond);
	Disable("btnEditLeg", cond);
	Disable("btnGDS", cond);

}
// Disable other buttons on Grid Section
function disablePrintBuildButtons(cond) {
	Disable("btnBuild", cond);	
}
// Disable other buttons on Grid Section
function disableSaveResetButtons(cond) {

	Disable("btnSave", cond);
	Disable("btnReset", cond);
}

function disableViewMsgHistory(strGridRow) {
	if(arrData[strGridRow][59] != undefined && arrData[strGridRow][59] == "true"){
		$('#btnMsgHistory').show();		
		Disable("btnMsgHistory", false);
	}else {
		Disable("btnMsgHistory", true);
		$('#btnMsgHistory').hide();		
	}
}

function SearchData() {
	objOnFocus();
	if (ckheckPageEdited()) {
		document.getElementById("frmSchedule").target = "_self";
		closeChildWindows();
		if (SearchValid() == false)
			return;
		setField("txtFlightNoSearch", trim(getText("txtFlightNoSearch")));
		setField("txtFlightNoStartSearch",
				trim(getText("txtFlightNoStartSearch")));
		if (document.forms[0].radTZ[1].checked == true)
			document.forms[0].hdnTimeMode.value = "ZULU";
		else
			document.forms[0].hdnTimeMode.value = "LOCAL";

		// once search button was pressed set the hidden value to "SEARCH"
		document.forms[0].hdnMode.value = "SEARCH";
		top[1].objTMenu.tabSetValue("SC_SCHD_001", "1^"
				+ getText("txtStartDateSearch") + ","
				+ getText("txtStopDateSearch") + ","
				+ getText("txtFlightNoSearch") + "," + getValue("selFromStn6")
				+ "," + getValue("selToStn6") + ","
				+ getValue("selOperationTypeSearch") + ","
				+ getValue("selStatusSearch") + ","
				+ getValue("selBuildStatusSearch") + ","
				+ getText("txtFlightNoStartSearch") + ","
				+ getValue("hdnTimeMode") + ","
				+ getValue("selFlightTypeSearch"));

		document.forms[0].submit();
		top[2].ShowProgress();
	}

}

function AddData() {
	objOnFocus();
	onLoadPopulateAirports();
	if (ckheckPageEdited()) {
		closeChildWindows();
		strMode = "ADD";
		disableSaveResetButtons(false);
		disableLegSection(false);
		Disable("selD_Day1", true);
		disableInputSection(false);

		clearInputSection();
		clearLegSection();
		Disable("btnEdit", true);
		Disable("btnEditLeg", true);
		Disable("btnCancel", true);
		Disable("btnSplit", true);
		Disable("btnCopy", true);
		Disable("btnFlight", true);
		Disable("btnFlightMnt", true);
		Disable("btnBuild", true);
		Disable("btnGDS", true);
		disableTerminalSection(false);
		setField("txtFlightNoStart", defCarrCode);
		setField("hdnMode", "ADD");
		blngrdselected = false;
		getFieldByID("txtStartDate").focus();
		
		resetDefaultTimeMode();
	}
}
function EditData() {
	objOnFocus();
	if (ckheckPageEdited()) {
		resetDefaultTimeMode();
		setField("hdnReprotect", "false");
		closeChildWindows();
		strMode = "UPDATE";
		resetClick();
		disableSaveResetButtons(false);
		disableInputSection(false);
		disableLegSection(true);
		Disable("btnValidseg", true);
		disableTerminalSection(false);
		getFieldByID("txtStartDate").focus();
		
		if(document.getElementById("txtCsOCFlightNo").value != "" && document.getElementById("txtCsOCCarrierCode").value != ""){
			Disable("carrier", true);
			Disable("flightnum", true);
			document.getElementById("add_CodeShare").disabled = true;
			document.getElementById("del_CodeShare").disabled = true;
			document.getElementById("sel_CodeShare").disabled=true;
		}
		
		
	}
}


var setDefaultTimeMode = function() {
	if (getFieldByID("hdnTimeMode").value == "LOCAL") {
		document.forms[0].radTZ[0].checked = true;
		blnZuluTime = false;

	} else if (getFieldByID("hdnTimeMode").value == "ZULU") {
		document.forms[0].radTZ[1].checked = true;
		blnZuluTime = true;
	} else {
		if(isTimeInZulu){
			document.frmSchedule.radTZ[1].checked = true;
		}else{
			document.frmSchedule.radTZ[0].checked = true;
		}
	}
}

function CancelData() {
	objOnFocus();
	if (ckheckPageEdited()) {
		setField("hdnReprotect", "false");
		closeChildWindows();
		var strconfirm = confirm(deletemessage);
		var olsched = arrData[strGridRow][26];
		if (strconfirm == true) {
			if (strRowData[9] == "Y") {
				var strconfirm = confirm("The Schedule overlaps with "
						+ olsched + ". Do you wish to delete both?");
				if (strconfirm == true) {
					setField("hdnMode", "CANCEL");
					document.getElementById("frmSchedule").target = "_self";
					document.forms[0].submit();
					top[2].ShowProgress();
				}
			} else {
				if (strRowData[9] == "N") {
					setField("hdnMode", "CANCEL");
					document.getElementById("frmSchedule").target = "_self";
					document.forms[0].submit();
					top[2].ShowProgress();
				}
			}
		}
	}
}

function SplitData() {
	objOnFocus();
	if (ckheckPageEdited()) {
		setField("hdnMode", "SPLIT");
		CWindowOpen(0);
	}
}

function BuildScheduleWithTemplates() {
	var cbutton = $("<input/>").attr({"type":"button", "value":"Cancel", "id":"btnCancel"});
	var sbutton = $("<input/>").attr({"type":"button", "value":"Ok", "id":"btnBuildWithInv"});
	sbutton.click(function(){
		setField("hdnBuildDefaultInventory", true);
		popClose();
		ContinueBuildFlights();
	});
	cbutton.click(function(){
		setField("hdnBuildDefaultInventory", false);
		popClose();
		ContinueBuildFlights();
	});
	var  spn = $("<span></span>");
	spn.append(sbutton, cbutton);
	$("#popUp").openMyPopUp({
		"width":400,
		"height":100,
		"appendobj":false,
		"headerHTML":$("<lable>Build schedule with default inventory</label>"),
		"bodyHTML":$("<lable>Do you need to build flights with default inventory templates?</label>"),
		"footerHTML":spn
	});
}

function exportData() {
	 if (pfsHeaderChanged(pfsHEaderAction)) {
	  document.forms[0].hdnUIMode.value = "EXPORT";
	  document.forms[0].hdnMode.value = "EXPORT";
	 }
	 submitSearch();
	}


function BuildData() {
	objOnFocus();	
	setField("hdnReprotect", "false");
	if (ckheckPageEdited()) {
		setField("hdnReprotect", "false");
		closeChildWindows();			
		
		var strconfirm = confirm("Do you want to build Flights ?");
		if (strconfirm == true) {			
			if(hdnBuildDefaultInvTemplate == "true"){
				BuildScheduleWithTemplates();
			} else{
				ContinueBuildFlights();
			}			
		}
	}
}

function ContinueBuildFlights(){
	var blnolapschd = false;
	var strArray = new Array();
	var count = 0;
	strArray[count] = getVal("hdnScheduleId");
	if (getVal("hdnOverlapSchID") != ""
			&& getVal("hdnOverlapSchID") != "null") {
		count = count + 1;
		strArray[count] = getVal("hdnOverlapSchID");
		blnolapschd = true;
	}
	var selscedtobld = objDG.getSelectedColumn(14);
	for ( var i = 0; i < selscedtobld.length; i++) {
		if ((selscedtobld[i][1]) && (arrData[i][10] != "BLT")) {
			strArray[count] = arrData[i][11];
			if (arrData[i][8] == "Y") {
				count = count + 1;
				strArray[count] = getVal("hdnOverlapSchID");
				blnolapschd = true;
			}
			count = count + 1;
		}
	}
	setField("hdnMode", "BUILD");
	strArray = removeDuplicate(strArray);
	if (blnolapschd == true) {
		var strconfirmolap = confirm("Overlaps with another schedule. This will build both the schedules. Do you wish to continue ?");
	} else {
		disableCommonButtons(true);
		setField("hdnRecNo", intrecno);
		document.forms[0].hdnRecNo.value = intrecno;
		document.forms[0].hdnSelectedSchedules.value = strArray;
		document.forms[0].hdnMode.value = "BUILD";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
	if (strconfirmolap == true) {
		disableCommonButtons(true);
		setField("hdnRecNo", intrecno);
		document.forms[0].hdnRecNo.value = intrecno;
		document.forms[0].hdnSelectedSchedules.value = strArray;
		document.forms[0].hdnMode.value = "BUILD";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}

function CopyData() {
	objOnFocus();
	if (ckheckPageEdited()) {
		setField("hdnMode", "COPY");
		CWindowOpen(1);
	}
}

function FlightInstance() {
	/* This code segment opens a new tab and shows the flights in it */
	objOnFocus();
	var strScdId = arrData[strGridRow][11];
	if (ckheckPageEdited()) {
		setField("hdnReprotect", "false");
		closeChildWindows();
		if (strRowData[11] != "BLT") {
			showERRMessage(buildneed);
			return;
		} else {
			if (top.loadCheck(top[1].objTMenu.tabGetPageEidted("SC_FLGT_001"))) {
				if ((top[1].objTMenu.tabIsAvailable("SC_FLGT_001") && top[1].objTMenu
						.tabRemove("SC_FLGT_001"))
						|| (!top[1].objTMenu.tabIsAvailable("SC_FLGT_001"))) {
					var schedflstd = getFieldByID("txtStartDate").value;
					var schfltstpd = getFieldByID("txtStopDate").value;
					var schfltno = getFieldByID("txtFlightNo").value;
					var schfltstartno = getFieldByID("txtFlightNoStart").value;
					var schfltopty = getFieldByID("selOperationType").value;
					var fromAirport = arrData[strGridRow][14];
					var toAirport = arrData[strGridRow][15];
					var schStatus = arrData[strGridRow][12];
					if (document.forms[0].radTZ[1].checked == true)
						document.forms[0].hdnTimeMode.value = "ZULU";
					else
						document.forms[0].hdnTimeMode.value = "LOCAL";
					top.LoadPage("showFlight.action?hdnScheduleId=" + strScdId
							+ "&hdnMode=SEARCH&hdnFromPage=SCHEDULE",
							"SC_FLGT_001", "Flights");
					top[1].objTMenu.tabSetValue("SC_FLGT_001", "1^"
							+ schedflstd + "," + schfltstpd + "," + schfltno
							+ "," + fromAirport + "," + toAirport + ","
							+ schfltopty + ",," + getValue("hdnTimeMode") + ","
							+ schfltstartno + ",," + getValue("selFlightTypeSearch"));
				}
			}
		}

	}

	/* ---------------------------------------------------------------- */
}

function FlightManage() {
	/* This code segment opens a new tab and shows the flights in it */
	objOnFocus();
	 
	var strScdId = arrData[strGridRow][11];
	var noOfFutureflts = arrData[strGridRow][44];

	if (ckheckPageEdited()) {
		setField("hdnReprotect", "false");
		closeChildWindows();
		if (strRowData[11] != "BLT") {
			showERRMessage(buildneed);
			return;
		}else if(noOfFutureflts == 0 || noOfFutureflts == "0"){
			showERRMessage(noOpenFlts);
			return;
		}else {
			if (top.loadCheck(top[1].objTMenu.tabGetPageEidted("SC_FLGT_001"))) {
				if ((top[1].objTMenu.tabIsAvailable("SC_FLGT_001") && top[1].objTMenu
						.tabRemove("SC_FLGT_001"))
						|| (!top[1].objTMenu.tabIsAvailable("SC_FLGT_001"))) {
					var schedflstd ="";
					schedflstd = new Date();
					var month = schedflstd.getMonth() + 1;
					var day = schedflstd.getDate();
					var year = schedflstd.getFullYear();
					var strFromDate = day+"/"+month+"/"+year;
					var schfltstpd = getFieldByID("txtStopDate").value;
					var schfltno = getFieldByID("txtFlightNo").value;
					var schfltstartno = getFieldByID("txtFlightNoStart").value;
					var schfltopty = getFieldByID("selOperationType").value;
					var fromAirport = arrData[strGridRow][14]
					var toAirport = arrData[strGridRow][15]
					if (document.forms[0].radTZ[1].checked == true)
						document.forms[0].hdnTimeMode.value = "ZULU";
					else
						document.forms[0].hdnTimeMode.value = "LOCAL";
					top.LoadPage("showFlight.action?hdnScheduleId=" + strScdId
							+ "&hdnMode=SEARCH&hdnFromPage=MANAGE",
							"SC_FLGT_001", "Flights");
					top[1].objTMenu.tabSetValue("SC_FLGT_001", "1^"
							+ strFromDate + "," + schfltstpd + "," + schfltno
							+ "," + fromAirport + "," + toAirport + ","
							+ schfltopty + ",," + getValue("hdnTimeMode") + ","
							+ schfltstartno + ","+ "true" + "," + getChecked("chkSunday") + ","
							+ getChecked("chkMonday") + "," + getChecked("chkTuesday") + ","
							+ getChecked("chkWednesday") + "," + getChecked("chkThursday") + ","
							+ getChecked("chkFriday") + "," + getChecked("chkSaturday"));
				}
			}
		}

	}

	/* ---------------------------------------------------------------- */
}

function EditLegData() {
	objOnFocus();
	setField("hdnReprotect", "false");
	if (ckheckPageEdited()) {
		resetDefaultTimeMode();
		strMode = "EDITLEG";
		setField("hdnMode", "EDITLEG");
		resetClick();
		disableSaveResetButtons(false);
		Disable("btnValidseg", false);
		disableInputSection(true);
		disableLegSection(false);
		//Disable only for overlapping flights
		if (arrData[strGridRow][8] == "Y") {
			Disable("selD_Day1", true);
		}
		if (arrData[strGridRow][10] == "BLT") {
			dissableAirports();
		} else if (arrData[strGridRow][8] == "Y") {
			setOverlapleg();
		} else {
			getFieldByID("selFromStn1").focus();
		}
	}
}
function removeDuplicate(buildarray) {
	var tempArray = new Array();
	var count = 1;
	buildarray.sort();
	var temp = buildarray[0];
	tempArray[0] = buildarray[0];
	for ( var i = 1; i < buildarray.length; i++) {
		if (temp != buildarray[i]) {
			tempArray[count] = buildarray[i];
			temp = buildarray[i];
			count = count + 1;
		}
	}
	return tempArray
}

// ***********************************************
function isSame(id1, id2, msg, cond) {
	objOnFocus();
	var blnErr = false;
	var val1 = getVal(id1);
	var val2 = getVal(id2);
	var obj = getField(id2);

	if ((val1 != 99) && (val2 != 99)) {
		if (cond == "EQ") {
			if (val1 == val2) {
				blnErr = true;
			}
		} else if (cond == "NEQ") {
			if (val1 != val2) {
				blnErr = true;
			}
		}
	}
	if (blnErr) {
		showERRMessage(msg);
		if (obj[0]) {
			if (obj.tagName == "SELECT") {
				obj.focus();
				// setField(id2,"");
			} else {
				obj[0].focus()
			}
		} else if (!(obj.readOnly || obj.disabled || obj.type == 'hidden'
				|| obj.style.visibility == 'hidden' || obj.style.display == 'none')) {
			obj.focus();
		}
	} else {
		setField(id2, trim(val2));
	}

	return blnErr;
}
// ***********************************************
function LegFirstRowValid() {
	objOnFocus();
	if (isError('NULL', incompleteleg, 'selFromStn1'))
		return false;
	if (!checkDepArrLeg(getFieldByID("selFromStn1").value, 1, true)) {
		showERRMessage(inactivedeptarue);
		if (!getFieldByID("selFromStn1").disabled) {
			getFieldByID("selFromStn1").focus();
		}
		return false;
	}
	if (isError('NULL', incompleteleg, 'selToStn1'))
		return false;
	if (!checkDepArrLeg(getFieldByID("selToStn1").value, 1, false)) {
		showERRMessage(inactivearrival);
		if (!getFieldByID("selToStn1").disabled) {
			getFieldByID("selToStn1").focus();
		}
		return false;
	}
	if (isSame('selFromStn1', 'selToStn1', departurearrival, 'EQ'))
		return false;
	if (isError('NULL', incompleteleg, 'txtD_Time1'))
		return false;
	if (IsValidTime(document.forms[0].txtD_Time1.value) == false) {
		showERRMessage(invaliddepaturetime);
		getFieldByID("txtD_Time1").focus();
		return false;
	}
	if (isError('NULL', incompleteleg, 'txtA_Time1'))
		return false;
	if (IsValidTime(document.forms[0].txtA_Time1.value) == false) {
		showERRMessage(invalidarrivaltime);
		getFieldByID("txtA_Time1").focus();
		return false;
	}
}
function LegSecondRowValid() {
	objOnFocus();
	if (LegFirstRowValid() == false)
		return false;
	if (isError('NULL', incompleteleg, 'selFromStn2'))
		return false;
	if (!checkDepArrLeg(getFieldByID("selFromStn2").value, 2, true)) {
		showERRMessage(inactivedeptarue);
		if (!getFieldByID("selFromStn2").disabled) {
			getFieldByID("selFromStn2").focus();
		}
		return false;
	}
	if (isError('NULL', incompleteleg, 'selToStn2'))
		return false;
	if (!checkDepArrLeg(getFieldByID("selToStn2").value, 2, false)) {
		showERRMessage(inactivearrival);
		if (!getFieldByID("selToStn2").disabled) {
			getFieldByID("selToStn2").focus();
		}
		return false;
	}
	if (isSame('selToStn1', 'selFromStn2', invaliddepature, 'NEQ'))
		return false;
	if (isSame('selFromStn2', 'selToStn2', departurearrival, 'EQ'))
		return false;
	if (isError('NULL', incompleteleg, 'txtD_Time2'))
		return false;
	if (IsValidTime(document.forms[0].txtD_Time2.value) == false) {
		getFieldByID("txtD_Time2").focus();
		showERRMessage(invaliddepaturetime);
		return false;
	}
	if (isError('NULL', incompleteleg, 'txtA_Time2'))
		return false;
	if (IsValidTime(document.forms[0].txtA_Time2.value) == false) {
		getFieldByID("txtA_Time2").focus();
		showERRMessage(invalidarrivaltime);
		return false;
	}
	if (checkMinDuration("selFromStn2", "txtA_Time1", "txtD_Time2",
			"selA_Day1", "selD_Day2") == false) {
		getFieldByID("txtD_Time2").focus();
		showERRMessage(invaliddepTime);
		return false;
	}

}

function LegThirdRowValid() {
	if (LegSecondRowValid() == false)
		return false;
	if (isError('NULL', incompleteleg, 'selFromStn3'))
		return false;
	if (!checkDepArrLeg(getFieldByID("selFromStn3").value, 3, true)) {
		showERRMessage(inactivedeptarue);
		if (!getFieldByID("selFromStn3").disabled) {
			getFieldByID("selFromStn3").focus();
		}
		return false;
	}
	if (isError('NULL', incompleteleg, 'selToStn3'))
		return false;
	if (!checkDepArrLeg(getFieldByID("selToStn3").value, 3, false)) {
		showERRMessage(inactivearrival);
		if (!getFieldByID("selToStn3").disabled) {
			getFieldByID("selToStn3").focus();
		}
		return false;
	}
	if (isSame('selToStn2', 'selFromStn3', invaliddepature, 'NEQ'))
		return false;
	if (isSame('selFromStn3', 'selToStn3', departurearrival, 'EQ'))
		return false;
	if (isError('NULL', incompleteleg, 'txtD_Time3'))
		return false;
	if (IsValidTime(document.forms[0].txtD_Time3.value) == false) {
		getFieldByID("txtD_Time3").focus();
		showERRMessage(invaliddepaturetime);
		return false;
	}
	if (isError('NULL', incompleteleg, 'txtA_Time3'))
		return false;
	if (IsValidTime(document.forms[0].txtA_Time3.value) == false) {
		getFieldByID("txtA_Time3").focus();
		showERRMessage(invalidarrivaltime);
		return false;
	}
	if (checkMinDuration("selFromStn3", "txtA_Time2", "txtD_Time3",
			"selA_Day2", "selD_Day3") == false) {
		getFieldByID("txtD_Time2").focus();
		showERRMessage(invaliddepTime);
		return false;
	}
}

function LegForthRowValid() {
	if (LegThirdRowValid() == false)
		return false;
	if (isError('NULL', incompleteleg, 'selFromStn4'))
		return false;
	if (!checkDepArrLeg(getFieldByID("selFromStn4").value, 4, true)) {
		showERRMessage(inactivedeptarue);
		if (!getFieldByID("selFromStn4").disabled) {
			getFieldByID("selFromStn4").focus();
		}
		return false;
	}
	if (isError('NULL', incompleteleg, 'selToStn4'))
		return false;
	if (!checkDepArrLeg(getFieldByID("selToStn4").value, 4, false)) {
		showERRMessage(inactivearrival);
		if (!getFieldByID("selToStn4").disabled) {
			getFieldByID("selToStn4").focus();
		}
		return false;
	}
	if (isSame('selToStn3', 'selFromStn4', invaliddepature, 'NEQ'))
		return false;
	if (isSame('selFromStn4', 'selToStn4', departurearrival, 'EQ'))
		return false;
	if (isError('NULL', incompleteleg, 'txtD_Time4'))
		return false;
	if (IsValidTime(document.forms[0].txtD_Time4.value) == false) {
		getFieldByID("txtD_Time4").focus();
		showERRMessage(invaliddepaturetime);
		return false;
	}
	if (isError('NULL', incompleteleg, 'txtA_Time4'))
		return false;
	if (IsValidTime(document.forms[0].txtA_Time4.value) == false) {
		getFieldByID("txtA_Time4").focus();
		showERRMessage(invalidarrivaltime);
		return false;
	}
	if (checkMinDuration("selFromStn4", "txtA_Time3", "txtD_Time4",
			"selA_Day3", "selD_Day4") == false) {
		getFieldByID("txtD_Time2").focus();
		showERRMessage(invaliddepTime);
		return false;
	}
}

function LegFifthRowValid() {
	if (LegForthRowValid() == false)
		return false;
	if (isError('NULL', incompleteleg, 'selFromStn5'))
		return false;
	if (!checkDepArrLeg(getFieldByID("selFromStn5").value, 5, true)) {
		showERRMessage(inactivedeptarue);
		if (!getFieldByID("selFromStn5").disabled) {
			getFieldByID("selFromStn5").focus();
		}
		return false;
	}
	if (isError('NULL', incompleteleg, 'selToStn5'))
		return false;
	if (!checkDepArrLeg(getFieldByID("selToStn5").value, 5, false)) {
		showERRMessage(inactivearrival);
		if (!getFieldByID("selToStn5").disabled) {
			getFieldByID("selToStn5").focus();
		}
		return false;
	}
	if (isSame('selToStn4', 'selFromStn5', invaliddepature, 'NEQ'))
		return false;
	if (isSame('selFromStn5', 'selToStn5', departurearrival, 'EQ'))
		return false;
	if (isError('NULL', incompleteleg, 'txtD_Time5'))
		return false;
	if (IsValidTime(document.forms[0].txtD_Time5.value) == false) {
		getFieldByID("txtD_Time5").focus();
		showERRMessage(invaliddepaturetime);
		return false;
	}
	if (isError('NULL', incompleteleg, 'txtA_Time5'))
		return false;
	if (IsValidTime(document.forms[0].txtA_Time5.value) == false) {
		getFieldByID("txtA_Time5").focus();
		showERRMessage(invalidarrivaltime);
		return false;
	}
	if (checkMinDuration("selFromStn5", "txtA_Time4", "txtD_Time5",
			"selA_Day4", "selD_Day5") == false) {
		getFieldByID("txtD_Time2").focus();
		showERRMessage(invaliddepTime);
		return false;
	}
}

function selFromStn1_Onchange() {
	objOnFocus();
	dataChanged();
	if (isSame('selFromStn1', 'selToStn1', departurearrival, 'EQ'))
		return;
}
function selFromStn2_Onchange() {
	objOnFocus();
	dataChanged();
	if (LegFirstRowValid() == false) {
		setField("selFromStn2", "");
		return;
	}
	if (isSame('selToStn1', 'selFromStn2', invaliddepature, 'NEQ'))
		return;
	if (isSame('selFromStn2', 'selToStn2', departurearrival, 'EQ'))
		return;
}
function selFromStn3_Onchange() {
	objOnFocus();
	dataChanged();
	if (LegFirstRowValid() == false) {
		setField("selFromStn3", "");
		return;
	}
	if (LegSecondRowValid() == false) {
		setField("selFromStn3", "");
		return;
	}
	if (isSame('selToStn2', 'selFromStn3', invaliddepature, 'NEQ'))
		return;
	if (isSame('selFromStn3', 'selToStn3', departurearrival, 'EQ'))
		return;
}
function selFromStn4_Onchange() {
	objOnFocus();
	dataChanged();
	if (LegFirstRowValid() == false) {
		setField("selFromStn4", "");
		return;
	}
	if (LegSecondRowValid() == false) {
		setField("selFromStn4", "");
		return;
	}
	if (LegThirdRowValid() == false) {
		setField("selFromStn4", "");
		return;
	}
	if (isSame('selToStn3', 'selFromStn4', invaliddepature, 'NEQ'))
		return;
	if (isSame('selFromStn4', 'selToStn4', departurearrival, 'EQ'))
		return;
}
function selFromStn5_Onchange() {
	objOnFocus();
	dataChanged();
	if (LegFirstRowValid() == false) {
		setField("selFromStn5", "");
		return;
	}
	if (LegSecondRowValid() == false) {
		setField("selFromStn5", "");
		return;
	}
	if (LegThirdRowValid() == false) {
		setField("selFromStn5", "");
		return;
	}
	if (LegForthRowValid() == false) {
		setField("selFromStn5", "");
		return;
	}
	if (isSame('selToStn4', 'selFromStn5', invaliddepature, 'NEQ'))
		return;
	if (isSame('selFromStn5', 'selToStn5', departurearrival, 'EQ'))
		return;
}
function selToStn1_Onchange() {
	objOnFocus();
	dataChanged();
	if (isError('NULL', fromAirportNull, 'selFromStn1'))
		return;
	if (isSame('selFromStn1', 'selToStn1', departurearrival, 'EQ'))
		return;
}
function selToStn2_Onchange() {
	objOnFocus();
	dataChanged();
	if (LegFirstRowValid() == false) {
		setField("selToStn2", "");
		return;
	}
	if (isSame('selFromStn2', 'selToStn2', departurearrival, 'EQ'))
		return;
	// if(isSame('selFromStn3','selToStn2','flightschedule.selToStn.Notsame','NEQ'))return;

}
function selToStn3_Onchange() {
	objOnFocus();
	dataChanged();
	if (LegFirstRowValid() == false) {
		setField("selToStn3", "");
		return;
	}
	if (LegSecondRowValid() == false) {
		setField("selToStn3", "");
		return;
	}
	if (isSame('selFromStn3', 'selToStn3', departurearrival, 'EQ'))
		return;
	// if(isSame('selFromStn4','selToStn3','flightschedule.selToStn.same','NEQ'))return;

}
function selToStn4_Onchange() {
	objOnFocus();
	dataChanged();
	if (LegFirstRowValid() == false) {
		setField("selToStn4", "");
		return;
	}
	if (LegSecondRowValid() == false) {
		setField("selToStn4", "");
		return;
	}
	if (LegThirdRowValid() == false) {
		setField("selToStn4", "");
		return;
	}
	if (isSame('selFromStn4', 'selToStn4', departurearrival, 'EQ'))
		return;
	// if(isSame('selFromStn5','selToStn4','flightschedule.selToStn.same','NEQ'))return;

}
function selToStn5_Onchange() {
	objOnFocus();
	dataChanged();
	if (LegFirstRowValid() == false) {
		setField("selToStn5", "");
		return;
	}
	if (LegSecondRowValid() == false) {
		setField("selToStn5", "");
		return;
	}
	if (LegThirdRowValid() == false) {
		setField("selToStn5", "");
		return;
	}
	if (LegForthRowValid() == false) {
		setField("selToStn5", "");
		return;
	}
	if (isSame('selFromStn5', 'selToStn5', departurearrival, 'EQ'))
		return;
}

function selDaysOnchange() {
	objOnFocus();
	dataChanged();
}

var splitIndex = 0;
var splitArray = new Array();

function split(string, text) {
	splitArray = string.split(text);
	splitIndex = splitArray.length;
}
function getMinutes(string) {
	splitIndex = 0;
	if (string != "") {
		split(string, ':');
	}
	for ( var i = splitIndex - 1, j = 1, answer = 0; i >= 0; i = i - 1, j = j * 60)
		answer += splitArray[i] * j - 0;

	return answer;
}

function ValidSegments() {
	objOnFocus();
	if (SaveValidations() == false)
		return;

	// Validate legs
	var FromStnSe1l = getFieldByID("selFromStn1").value;
	var FromStnSe12 = getFieldByID("selFromStn2").value;
	if (FromStnSe1l == "") {
		showERRMessage(onelegneeded);
		getFieldByID("selFromStn1").focus();
		return;
	}
	if ((FromStnSe12 == "") && (FromStnSe1l != "")) {
		showERRMessage(morethanoneleg);
		return;
	}
	CWindowOpen(2);
	blnvalseg = true;
}
function validTerminal() {	
	objOnFocus();
	//if (SaveValidations() == false)
	//	return;

	
	CWindowOpen(3);
	blnvalseg = true;
}
//add CodeShare to select box
function addCodeShareFlight(){
	
	var Carrier =getFieldByID("carrier").value;
	var FlightNum = getFieldByID("flightnum").value.toUpperCase();
	
	if (( Carrier !="") && (FlightNum!="")) {
		var valid = true;	;
		var CSFlight =  Carrier+ "|"+ FlightNum;
		
		var control = document.getElementById("sel_CodeShare");
		for ( var r = 0; r < control.length; r++) {
			var enterdCSFlight = control.options[r].value;
			
			if (enterdCSFlight == CSFlight) {
				valid = false;
				showERRMessage(codeShareExists);
				getFieldByID("carrier").focus();
				getFieldByID("flightnum").focus();
				break;
			}
			if(enterdCSFlight.split("|")[0] == Carrier){
				valid = false;
				showERRMessage(codeShareExists);
				getFieldByID("carrier").focus();
				break;
			}
					
		}
		if (valid) {
			control.options[control.length] = new Option(CSFlight, CSFlight);
			getFieldByID("carrier").value = '';
			getFieldByID("flightnum").value = '';
		}
	} else {
			if(Carrier == ""){
				showERRMessage(CScarriernull);
			}
			if(FlightNum == ""){
				showERRMessage(CSfltnonull);
			}
		
	}	
}

//remove CodeShare from select box
function removeFromList() {
	var control = document.getElementById("sel_CodeShare");

	var selIndex = control.selectedIndex;
	var flag = false;
	if (selIndex != -1) {
		for (i = control.length - 1; i >= 0; i--) {
			if (control.options[i].selected) {
				if(!isPublishedToCodeShare(control.options[i].value)){
					control.options[i] = null;	
					flag = true;
				} else {						
					flag = false;
					showERRMessage(codeShareUnPublish);
					break;
				}
				
			}
		}
		if (flag && control.length > 0) {
			control.selectedIndex = selIndex == 0 ? 0 : selIndex - 1;
			getFieldByID("carrier").value = '';
			getFieldByID("flightnum").value = '';
		}
	}
}

//setfield to save CodeShare
function setCodeShareArray() {
	var control =  document.getElementById("sel_CodeShare");
	var strCodeShare= "";
	for ( var r = 0; r < control.length; r++) {
		strCodeShare += control.options[r].value+ ",";
	}
	setField("hndCodeShare", strCodeShare);
}

function SaveValidations() {
	objOnFocus();
	// Mandotory Fields
	if (isError('NULL', startDate, 'txtStartDate'))
		return false;
	if (isError('NULL', stopDate, 'txtStopDate'))
		return false;
	// compare start date and current date
	var StartD = formatDate(getVal("txtStartDate"), "dd/mm/yyyy", "dd-mmm-yyyy");
	var Today = formatDate(getVal("hdnCurrentDate"), "dd/mm/yyyy",
			"dd-mmm-yyyy");
	var StopD = formatDate(getVal("txtStopDate"), "dd/mm/yyyy", "dd-mmm-yyyy");
	
	var currentYear = Today.substring(7,11);
	var yearOfStop = Number(StopD.substring(7,11));
	var maxYearInCalendar = Number(currentYear) +Number(noYears)- Number(1);

	if (!dateChk("txtStartDate")) {
		showERRMessage(splinvaliddate);
		if (!getFieldByID("txtStartDate").disabled) {
			getFieldByID("txtStartDate").focus();
		}
		return;
	}
	if (!dateChk("txtStopDate")) {
		showERRMessage(splinvaliddate);
		if (!getFieldByID("txtStopDate").disabled) {
			getFieldByID("txtStopDate").focus();
		}

		return;
	}
	if (compareDates(Today, StartD, '>=')) {
		showERRMessage(lesscurrentdate);
		if (!getFieldByID("txtStartDate").disabled) {
			getFieldByID("txtStartDate").focus();
		}
		return false;
	}
	// compare start date and stop date
	if (compareDates(StartD, StopD, '>=')) {
		showERRMessage(startdateless);
		if (!getFieldByID("txtStopDate").disabled) {
			getFieldByID("txtStopDate").focus();
		}
		return false;
	}	
	
	if(yearOfStop > maxYearInCalendar){
		showERRMessage(stopdategreatercaldate +' '+ maxYearInCalendar.toString());
		if (!getFieldByID("txtStopDate").disabled) {
			getFieldByID("txtStopDate").focus();
		}
		return false;
	}
	
	// Mandotory Fields
	if (isError('NULL', optTypenull, 'selOperationType'))
		return false;
	if (isError('NULL', flightnonull, 'txtFlightNo'))
		return false;
	if (isError('NULL', aircraftModelnull, 'selAircraftModel'))
		return false;
	
	if (isError('NULL', flttypenull, 'selFlightType'))
		return false;
	

	if (!checkAircraftModel()) {
		showERRMessage(invalidmodel);
		if (!getFieldByID("selAircraftModel").disabled) {
			getFieldByID("selAircraftModel").focus();
		}
		return false;
	}

	var svecarrier = trim(getVal("txtFlightNoStart"));
	var blnValid = false;
	for ( var cl = 0; cl < arrCarriers.length; cl++) {
		if (arrCarriers[cl] == svecarrier) {
			blnValid = true;
			break;
		}
	}
	if (!blnValid) {
		showERRMessage(invalidcarrier);
		getFieldByID("txtFlightNoStart").focus();
		return false;
	}

	// check Frequency all null
	if (document.getElementById("chkSunday").checked == false) {
		if (document.getElementById("chkMonday").checked == false) {
			if (document.getElementById("chkTuesday").checked == false) {
				if (document.getElementById("chkWednesday").checked == false) {
					if (document.getElementById("chkThursday").checked == false) {
						if (document.getElementById("chkFriday").checked == false) {
							if (document.getElementById("chkSaturday").checked == false) {
								showERRMessage(invalidFreq);
								// document.getElementById("chkSunday").focus;
								return false;
							}
						}
					}
				}
			}
		}
	}

	var tempLegs = getLegs();
	if (tempLegs != false) {
		var stLegrArray = tempLegs.split('~');
		strLegs = stLegrArray[0];
		strLegDtls = stLegrArray[1];
	} else {
		return false;
	}		
	
	if (trim(getVal("txtRemarks"))!="" && !isAlphaNumericTextArea(trim(getVal("txtRemarks")))) {
		showERRMessage(invalidCharRem);
		getFieldByID("txtRemarks").focus();
		return false;
	}	
	if((trim(getVal("txtCsOCCarrierCode"))!="" || trim(getVal("txtCsOCFlightNo"))!= "") && trim(getFieldByID("sel_CodeShare").value)!=""){
		showERRMessage(invalidOCandMC);
		getFieldByID("sel_CodeShare").focus();
		return false;
	}
}

function getGDSList() {

	var strReturn = "";
	var intLengrh = arrData[strGridRow][42].length;
	for ( var i = 0; i < intLengrh; i++) {
		if (strReturn != "") {
			strReturn += ",";
		}
		strReturn += arrData[strGridRow][42][i];
	}

	return strReturn;
}

function SaveData() {
	objOnFocus();
	setField("hdnReprotect", "false");
	document.getElementById("frmSchedule").target = "_self";
	closeChildWindows();
	setCodeShareArray();
	setField("hndCsOCCarrierCode", trim(getText("txtCsOCCarrierCode")));
	setField("hndCsOCFlightNo", trim(getText("txtCsOCFlightNo")));
	setField("hdnGridRow", strGridRow);
	setField("txtFlightNoSearch", trim(getText("txtFlightNoSearch")));
	setField("txtFlightNoStartSearch", trim(getText("txtFlightNoStartSearch")));
	if (document.forms[0].radTZ[1].checked == true)
		document.forms[0].hdnTimeMode.value = "ZULU";
	else
		document.forms[0].hdnTimeMode.value = "LOCAL";

	top[1].objTMenu
			.tabSetValue("SC_SCHD_001", intrecno + "^"
					+ getText("txtStartDateSearch") + ","
					+ getText("txtStopDateSearch") + ","
					+ getText("txtFlightNoSearch") + ","
					+ getValue("selFromStn6") + "," + getValue("selToStn6")
					+ "," + getValue("selOperationTypeSearch") + ","
					+ getValue("selStatusSearch") + ","
					+ getValue("selBuildStatusSearch") + ","
					+ getText("txtFlightNoStartSearch") + ","
					+ getValue("hdnTimeMode") + ","
					+ getValue("selFlightType"));

	if (strMode == "null") {
		strMode = getFieldByID("hdnMode").value;
	}
	if (strMode != "ADD" && arrData[strGridRow][8] == "Y") {
		var strconfirmolap = confirm("Overlaps with schedule "
				+ arrData[strGridRow][26]
				+ ". This will update both the schedules. Do you wish to continue ?");
	}
	if (strMode == "ADD") {
		if (SaveValidations() == false)
			return;
		disableCommonButtons(true);
		disableSaveResetButtons(true);
		Disable("selD_Day1", false);
		setField("hdnMode", "ADD");
		document.forms[0].hdnMode.value = "ADD";
		document.forms[0].submit();
		top[2].ShowProgress();
	} else if (strMode == "UPDATE") {
		disableLegSection(false);
		if ((SaveValidations() == false) || (editValidations() == false)) {
			disableLegSection(true);
			return;
		}
		if (arrData[strGridRow][8] != "Y" || strconfirmolap == true) {
			setField("hdnMode", "UPDATE");
			document.forms[0].hdnMode.value = "UPDATE";
			disableCommonButtons(true);
			disableSaveResetButtons(true);
			setField("hdnRecNo", intrecno);
			Disable("selD_Day1", false);
			document.forms[0].hdnRecNo.value = intrecno;
			document.forms[0].submit();
			top[2].ShowProgress();
		}

	} else if (strMode == "EDITLEG") {
		disableInputSection(false);
		disableLegSection(false);
		if (SaveValidations() == false) {
			disableInputSection(true);
			Disable("selD_Day1", true);
			if (arrData[strGridRow][10] == "BLT") {
				dissableAirports();
			} else if (arrData[strGridRow][8] == "Y") {
				setOverlapleg();
			}
			return;
		}
		if (arrData[strGridRow][8] != "Y" || strconfirmolap == true) {
			setField("hdnMode", "EDITLEG");
			disableCommonButtons(true);
			disableSaveResetButtons(true);
			setField("hdnRecNo", intrecno);
			Disable("selD_Day1", false);
			document.forms[0].hdnMode.value = "EDITLEG";
			document.forms[0].hdnRecNo.value = intrecno;
			document.forms[0].submit();
			top[2].ShowProgress();
		} else {
			disableInputSection(true);
			Disable("selD_Day1", true);
			if (arrData[strGridRow][10] == "BLT") {
				dissableAirports();
			} else if (arrData[strGridRow][8] == "Y") {
				setOverlapleg();
			}
			return;
		}

	}

} // End Save data

function resetClick() {
	// ToDo Clear info bar
	objOnFocus();

	if (ckheckPageEdited()) {
		clearInputSection();
		clearLegSection();
		Disable("selD_Day1", true);
		if (strMode == "ADD") {
			strMode = "ADD";
			disableSaveResetButtons(false);
			disableLegSection(false);
			disableInputSection(false);
			setField("txtFlightNoStart", defCarrCode);
			setField("hdnMode", "ADD");
			getFieldByID("txtStartDate").focus();
		}
		if (strMode == "UPDATE" || strMode == "EDITLEG") {
			// populate the original values selected
			setField("hdnScheduleId", arrData[strGridRow][11]);
			setField("hdnBulidStatus", arrData[strGridRow][10]);
			setField("hdnStatus", arrData[strGridRow][12]);
			setField("hdnTerminalArray", arrData[strGridRow][45]);
			if (blnZuluTime) {
				setField("txtStartDate", arrData[strGridRow][52]);
				setField("txtStopDate", arrData[strGridRow][53]);		
				arrLegs = arrData[strGridRow][23];
			} else {
				setField("txtStartDate", arrData[strGridRow][54]);
				setField("txtStopDate", arrData[strGridRow][55]);	
				arrLegs = arrData[strGridRow][32];
			}
			setField("selOperationType", arrData[strGridRow][40]);
			setField("txtFlightNo", arrData[strGridRow][1].substr(2, 5));
			setField("txtFlightNoStart", arrData[strGridRow][1].substr(0, 2));
			setField("selAircraftModel", arrData[strGridRow][41]);
			setField("hdnVersion", arrData[strGridRow][25]);
			setSegmentArray(arrData[strGridRow][27]);
			setPageEdited(false);
			document.getElementById('spnSchedId').innerHTML = arrData[strGridRow][11];
			document.getElementById('spnStatus').innerHTML = arrData[strGridRow][12];
			var ModelSelected = arrData[strGridRow][41];
			for ( var i = 0; i < arrAircraftModelData.length; i++) {
				if ((ModelSelected != "")
						&& (ModelSelected == arrAircraftModelData[i][0])) {
					document.getElementById('spnCapacity').innerHTML = arrAircraftModelData[i][1];
				}
			}
			if (arrData[strGridRow][8] == "Y")
				setStyleClass("tdSchdOverlap", "fltStatus02");
			if (arrData[strGridRow][9] > 0)
				setStyleClass("tdFltExists", "fltStatus04");
			if (arrData[strGridRow][24] == "true")
				setStyleClass("tdFltChanged", "fltStatus03");
			setField("hdnOverlapSchID", arrData[strGridRow][26]);
			setField("selFlightType", arrData[strGridRow][46]);
			setField("txtRemarks", arrData[strGridRow][48]);
			setField("selMealTemplate", arrData[strGridRow][49]);			
			setField("selSeatChargeTemplate", arrData[strGridRow][50]);		
			
			// Setting leg details
			displayLegs(arrLegs);
			// checkboxes
			displayFrequency();
		}
		setPageEdited(false);
		if (strMode != "EDITLEG") {
			Disable("txtStartDate", false);
			getFieldByID("txtStartDate").focus();
		}
		//CodeShare view
		setCodeShareArr(arrData[strGridRow][56]);
		
		setField("txtCsOCCarrierCode", arrData[strGridRow][57]);
		setField("txtCsOCFlightNo", arrData[strGridRow][58]);

	}
}

// on Grid Row click
function RowClick(strRowNo) {
	objOnFocus();
	if (ckheckPageEdited()) {
		
		onLoadPopulateAllAirports();
		clearInputSection();
		clearLegSection();
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;
		blngrdselected = true;
		setField("hdnScheduleId", arrData[strRowNo][11]);
		setField("hdnStatus", arrData[strRowNo][12]);
		setField("hdnBulidStatus", arrData[strRowNo][10]);
		setField("hdnTerminalArray", arrData[strRowNo][45]);
		if (blnZuluTime) {			
			setField("txtStartDate", arrData[strRowNo][52]);
			setField("txtStopDate", arrData[strRowNo][53]);					
			arrLegs = arrData[strRowNo][23];
		} else {
			setField("txtStartDate", arrData[strRowNo][54]);
			setField("txtStopDate", arrData[strRowNo][55]);	
			arrLegs = arrData[strRowNo][32];
		}
		document.getElementById('spnSchedId').innerHTML = arrData[strRowNo][11];
		document.getElementById('spnStatus').innerHTML = arrData[strRowNo][12];
		setField("selOperationType", arrData[strRowNo][40]);
		setField("txtFlightNoStart", strRowData[0].substr(0, 2));
		setField("txtFlightNo", strRowData[0].substr(2, 5));
		setField("selAircraftModel", arrData[strRowNo][41]);
		var ModelSelected = arrData[strRowNo][41];
		for ( var i = 0; i < arrAircraftModelData.length; i++) {
			if ((ModelSelected != "")
					&& (ModelSelected == arrAircraftModelData[i][0])) {
				document.getElementById('spnCapacity').innerHTML = arrAircraftModelData[i][1];
			}
		}
		
		var n = seatTemplates.split("|"); 
		var	strChargeTemplate='<option value=""></option>';
		for (var i=0;i<n.length-1;i++)
		{
			var n1=n[i].split("&"); 
			if(n1[0]==ModelSelected){
				
				strChargeTemplate = strChargeTemplate+ "<option value='" +n1[1] + "'>" +n1[2] + "</option>";
					
			}	
		}
			
		document.getElementById('selSeatChargeTemplate').innerHTML = strChargeTemplate;
		
		displayFrequency();
		setSegmentArray(arrData[strRowNo][27]);
		if (arrData[strRowNo][8] == "Y")
			setStyleClass("tdSchdOverlap", "fltStatus02");
		if (arrData[strRowNo][9] > 0)
			setStyleClass("tdFltExists", "fltStatus04");
		if (arrData[strRowNo][24] == "true")
			setStyleClass("tdFltChanged", "fltStatus03");
		setField("hdnVersion", arrData[strRowNo][25]);
		setPageEdited(false);
		// checkboxes
		displayLegs(arrLegs);
		setField("hdnOverlapSchID", arrData[strRowNo][26]); // Overlaped
															// schedule id
		disableInputSection(true);
		disableLegSection(true);

		var StartD = formatDate(getVal("txtStartDate"), "dd/mm/yyyy",
				"dd-mmm-yyyy");
		var Today = formatDate(getVal("hdnCurrentDate"), "dd/mm/yyyy",
				"dd-mmm-yyyy");
		var StopD = formatDate(getVal("txtStopDate"), "dd/mm/yyyy",
				"dd-mmm-yyyy");
		// validate buttons
		var intLength = arrData.length;
		for ( var i = 0; i < intLength; i++) {
			objDG.setCellValue(i, 14, "");
		}
		setField("chkSelectAll", "");

		disableCommonButtons(false);
		disableSaveResetButtons(true);
		Disable("btnValidseg", true);
		Disable("btnValidTerminal", true);	
		setField("hdnViewModeOnly", "false");
		var gdsList = getGDSList();
		setField("hdnGDSPublishing", gdsList);
		if (compareDates(Today, StartD, '>=')
				|| (!(arrData[strRowNo][12] == "ACT") && !(arrData[strRowNo][12] == "ERR"))) {
			Disable("btnEdit", true);
			Disable("btnEditLeg", true);
			Disable("btnCancel", true);
			Disable("btnBuild", true);
			//Commented to allow gds publish for already started schedules
			//setField("hdnViewModeOnly", "true");
		}
		if (compareDates(Today, StopD, '>=')
				|| (!(arrData[strRowNo][12] == "ACT") && !(arrData[strRowNo][12] == "ERR"))) {
			Disable("btnEdit", true);
			Disable("btnEditLeg", true);
			Disable("btnCancel", true);
			Disable("btnBuild", true);
			Disable("btnSplit", true);
			setField("hdnViewModeOnly", "true");
		}
		if (arrData[strRowNo][10] == "BLT") {
			Disable("btnBuild", true);
		}
		if (arrData[strRowNo][12] == "ERR") {
			Disable("btnBuild", true);
			Disable("btnSplit", true);
			Disable("btnCopy", true);
			Disable("btnGDS", true);
		}
		setField("selFlightType", arrData[strRowNo][46]);
		setField("txtRemarks", arrData[strRowNo][48]);
		setField("selMealTemplate", arrData[strRowNo][49]);			
		setField("selSeatChargeTemplate", arrData[strRowNo][50]);	
		resetDefaultTimeMode();
		
		//Codeshare view
		setCodeShareArr(arrData[strRowNo][56]);
		//document.forms[0].radTZ[0].checked = false;
		//document.forms[0].radTZ[1].checked = false;
		setField("txtCsOCCarrierCode", arrData[strRowNo][57]);	
		setField("txtCsOCFlightNo", arrData[strRowNo][58]);	
		disableCtrlForCodeShare();
		if(arrData[strGridRow][56]=="" || !arrData[strGridRow][56].length > 0){
			setField("hdnHaveMCFlight", "false");
		}else  {
			setField("hdnHaveMCFlight", "true");
		}
		disableViewMsgHistory(strRowNo);
		hidePublisheMessageAudit(gdsList);
	}
}

function getLegs() {
	var strbuildlegs = "";
	var strbuildLegDetails = "";
	var FromStnSe1l = getFieldByID("selFromStn1").value;
	var ToStnSe1l = getFieldByID("selToStn1").value;
	var FromStnSe12 = getFieldByID("selFromStn2").value;
	var ToStnSe12 = getFieldByID("selToStn2").value;
	var FromStnSe13 = getFieldByID("selFromStn3").value;
	var ToStnSe13 = getFieldByID("selToStn3").value;
	var FromStnSe14 = getFieldByID("selFromStn4").value;
	var ToStnSe14 = getFieldByID("selToStn4").value;
	var FromStnSe15 = getFieldByID("selFromStn5").value;
	var ToStnSe15 = getFieldByID("selToStn5").value;

	var D_Time1 = getFieldByID("txtD_Time1").value;
	var D_Time2 = getFieldByID("txtD_Time2").value;
	var D_Time3 = getFieldByID("txtD_Time3").value;
	var D_Time4 = getFieldByID("txtD_Time4").value;
	var D_Time5 = getFieldByID("txtD_Time5").value;
	var A_Time1 = getFieldByID("txtA_Time1").value;
	var A_Time2 = getFieldByID("txtA_Time2").value;
	var A_Time3 = getFieldByID("txtA_Time3").value;
	var A_Time4 = getFieldByID("txtA_Time4").value;
	var A_Time5 = getFieldByID("txtA_Time5").value;
	D_Time1 = getMinutes(D_Time1);
	A_Time1 = getMinutes(A_Time1);
	D_Time2 = getMinutes(D_Time2);
	A_Time2 = getMinutes(A_Time2);
	D_Time3 = getMinutes(D_Time3);
	A_Time3 = getMinutes(A_Time3);
	D_Time4 = getMinutes(D_Time4);
	A_Time4 = getMinutes(A_Time4);
	D_Time5 = getMinutes(D_Time5);
	A_Time5 = getMinutes(A_Time5);

	if (FromStnSe1l == "") {

		showERRMessage(onelegneeded);
		if (!getFieldByID("selFromStn1").disabled) {
			getFieldByID("selFromStn1").focus();
		}
		return false;

	} else if (FromStnSe12 == ""  && !isClickOnTerminal) {

		if (LegFirstRowValid() == false)
			return false;

		setField("hdnArrivalStn", ToStnSe1l);

	} 
	else if (FromStnSe12 == "" && isClickOnTerminal) {

		if (LegFirstRowValid() == false)
			return false;
                        
                strbuildLegDetails = addArrayLeg("selFromStn1", "selToStn1",
				"txtD_Time1", "txtA_Time1", "selD_Day1", "selA_Day1");
		strbuildlegs = addleg("selFromStn1", "selToStn1");	

	} 
	else if (FromStnSe13 == "") {

		strbuildLegDetails = addArrayLeg("selFromStn1", "selToStn1",
				"txtD_Time1", "txtA_Time1", "selD_Day1", "selA_Day1");
		strbuildlegs = addleg("selFromStn1", "selToStn1");
		if (LegSecondRowValid() == false)
			return false;

		setField("hdnArrivalStn", ToStnSe12);

		strbuildLegDetails += ","
				+ addArrayLeg("selFromStn2", "selToStn2", "txtD_Time2",
						"txtA_Time2", "selD_Day2", "selA_Day2");
		strbuildlegs += "," + addleg("selFromStn2", "selToStn2");

	} else if (FromStnSe14 == "") {

		if (LegThirdRowValid() == false)
			return false;

		setField("hdnArrivalStn", ToStnSe13);
		strbuildLegDetails = addArrayLeg("selFromStn1", "selToStn1",
				"txtD_Time1", "txtA_Time1", "selD_Day1", "selA_Day1");
		strbuildLegDetails += ","
				+ addArrayLeg("selFromStn2", "selToStn2", "txtD_Time2",
						"txtA_Time2", "selD_Day2", "selA_Day2");
		strbuildLegDetails += ","
				+ addArrayLeg("selFromStn3", "selToStn3", "txtD_Time3",
						"txtA_Time3", "selD_Day3", "selA_Day3");

		strbuildlegs = addleg("selFromStn1", "selToStn1");
		strbuildlegs += "," + addleg("selFromStn2", "selToStn2");
		strbuildlegs += "," + addleg("selFromStn3", "selToStn3");

	} else if (FromStnSe15 == "") {
		if (LegForthRowValid() == false)
			return false;

		setField("hdnArrivalStn", ToStnSe14);
		strbuildLegDetails = addArrayLeg("selFromStn1", "selToStn1",
				"txtD_Time1", "txtA_Time1", "selD_Day1", "selA_Day1");
		strbuildLegDetails += ","
				+ addArrayLeg("selFromStn2", "selToStn2", "txtD_Time2",
						"txtA_Time2", "selD_Day2", "selA_Day2");
		strbuildLegDetails += ","
				+ addArrayLeg("selFromStn3", "selToStn3", "txtD_Time3",
						"txtA_Time3", "selD_Day3", "selA_Day3");
		strbuildLegDetails += ","
				+ addArrayLeg("selFromStn4", "selToStn4", "txtD_Time4",
						"txtA_Time4", "selD_Day4", "selA_Day4");

		strbuildlegs = addleg("selFromStn1", "selToStn1");
		strbuildlegs += "," + addleg("selFromStn2", "selToStn2");
		strbuildlegs += "," + addleg("selFromStn3", "selToStn3");
		strbuildlegs += "," + addleg("selFromStn4", "selToStn4");

	} else if (FromStnSe15 != "") {

		if (LegFifthRowValid() == false)
			return false;

		setField("hdnArrivalStn", ToStnSe15);
		strbuildLegDetails = addArrayLeg("selFromStn1", "selToStn1",
				"txtD_Time1", "txtA_Time1", "selD_Day1", "selA_Day1");
		strbuildLegDetails += ","
				+ addArrayLeg("selFromStn2", "selToStn2", "txtD_Time2",
						"txtA_Time2", "selD_Day2", "selA_Day2");
		strbuildLegDetails += ","
				+ addArrayLeg("selFromStn3", "selToStn3", "txtD_Time3",
						"txtA_Time3", "selD_Day3", "selA_Day3");
		strbuildLegDetails += ","
				+ addArrayLeg("selFromStn4", "selToStn4", "txtD_Time4",
						"txtA_Time4", "selD_Day4", "selA_Day4");
		strbuildLegDetails += ","
				+ addArrayLeg("selFromStn5", "selToStn5", "txtD_Time5",
						"txtA_Time5", "selD_Day5", "selA_Day5");

		strbuildlegs = addleg("selFromStn1", "selToStn1");
		strbuildlegs += "," + addleg("selFromStn2", "selToStn2");
		strbuildlegs += "," + addleg("selFromStn3", "selToStn3");
		strbuildlegs += "," + addleg("selFromStn4", "selToStn4");
		strbuildlegs += "," + addleg("selFromStn5", "selToStn5");

	}
	return strbuildlegs + "~" + strbuildLegDetails;
}

// function to check the minimum stop over time
function checkMinDuration(airport, arrivalTime, depatureTime, arrvalday, depday) {
	var depAirport = getFieldByID(airport).value;
	var arrival = getMinutes(getFieldByID(arrivalTime).value)
			+ (getFieldByID(arrvalday).value * 24 * 60);
	var depature = getMinutes(getFieldByID(depatureTime).value)
			+ (getFieldByID(depday).value * 24 * 60);
	for ( var i = 0; i < arrMinStopOverTimeData.length; i++) {
		if ((depAirport == arrMinStopOverTimeData[i][0])
				&& ((depature - arrival) < arrMinStopOverTimeData[i][1])) {
			return false;
		}
	}
}

function objOnFocus() {
	top[2].HidePageMessage();
}

// function to create the array
function addArrayLeg(from, to, dep, arr, depvar, arrivvar) {
	var detleg = "";
	detleg = getFieldByID(from).value + "_" + getFieldByID(to).value + "_"
			+ getFieldByID(dep).value + "_" + getFieldByID(arr).value + "_"
			+ getFieldByID(depvar).value + "_" + getFieldByID(arrivvar).value;
	return detleg;
}

// function to build legs
function addleg(from, to) {
	var legs = "";
	legs = getFieldByID(from).value + "," + getFieldByID(to).value;
	return legs;
}

function validatedSegments(segments, olapseg, olapschedId) {

	setField("hdnOverLapSeg", "true");
	setField("hdnSegArray", segments);
	setField("hdnOverLapSegId", olapseg);
	setField("hdnOverLapSegSchedId", olapschedId);
	setField("hdnOverlapSchID", olapschedId);

}

// To Handle Paging
function gridNavigations(intRecNo, intErrMsg) {
	objOnFocus();
	if (ckheckPageEdited()) {
		setField("hdnRecNo", intRecNo);
		if (intErrMsg != "") {
			top[2].objMsg.MessageText = "<li> " + intErrMsg;
			top[2].objMsg.MessageType = "Error";
			top[2].ShowPageMessage();
		} else {
			document.forms[0].hdnMode.value = "SEARCH";
			top[1].objTMenu.tabSetValue("SC_SCHD_001", intRecNo + "^"
					+ getText("txtStartDateSearch") + ","
					+ getText("txtStopDateSearch") + ","
					+ getText("txtFlightNoSearch") + ","
					+ getValue("selFromStn6") + "," + getValue("selToStn6")
					+ "," + getValue("selOperationTypeSearch") + ","
					+ getValue("selStatusSearch") + ","
					+ getValue("selBuildStatusSearch") + ","
					+ getText("txtFlightNoStartSearch") + ","
					+ getValue("hdnTimeMode"));
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	}
}

function dataChanged() {
	objOnFocus();
	setPageEdited(true);
}

// call to populate data

function loadAircraftModelCapacity() {
	dataChanged();
	var ModelSelected = getFieldByID("selAircraftModel").value;
	for ( var i = 0; i < arrAircraftModelData.length; i++) {
		if ((ModelSelected != "")
				&& (ModelSelected == arrAircraftModelData[i][0])) {
			document.getElementById('spnCapacity').innerHTML = arrAircraftModelData[i][1];
		}
	}
	
	var n = seatTemplates.split("|"); 
	var	strChargeTemplate='<option value=""></option>';
	for (var i=0;i<n.length-1;i++)
	{
		var n1=n[i].split("&"); 
		if(n1[0]==ModelSelected){
			
			strChargeTemplate = strChargeTemplate+ "<option value='" +n1[1] + "'>" +n1[2] + "</option>";
				
		}	
	}
		
	document.getElementById('selSeatChargeTemplate').innerHTML = strChargeTemplate;
}

// function to set the date
function setDate(strDate, strID) {
	objOnFocus();
	switch (strID) {
	case "0":
		setField("txtStartDateSearch", strDate);
		break;
	case "1":
		setField("txtStopDateSearch", strDate);
		break;
	case "3":
		setField("txtStartDate", strDate);
		break;
	case "4":
		setField("txtStopDate", strDate);
		break;
	}
}

// function to load the calendar
function LoadCalendar(strID, objEvent) {
	objOnFocus();
	objCal1.currentDate = getVal("hdnCurrentDate");
	objCal1.ID = strID;
	if (strID == 0) {
		objCal1.top = 20;
		objCal1.left = 75;
		if ((trim(getVal("txtStartDateSearch")).length > 0)
				&& (dateChk('txtStartDateSearch')))
			objCal1.currentDate = getVal("txtStartDateSearch");

	} else if (strID == 1) {
		objCal1.top = 20;
		objCal1.left = 175;
		if ((trim(getVal("txtStopDateSearch")).length > 0)
				&& (dateChk('txtStopDateSearch')))
			objCal1.currentDate = getVal("txtStopDateSearch");

	} else if (strID == 3) {
		objCal1.top = 320;
		objCal1.left = 200;
		if ((trim(getVal("txtStartDate")).length > 0)
				&& (dateChk('txtStartDate')))
			objCal1.currentDate = getVal("txtStartDate");

	} else if (strID == 4) {
		objCal1.top = 320;
		objCal1.left = 380;
		if ((trim(getVal("txtStopDate")).length > 0)
				&& (dateChk('txtStopDate')))
			objCal1.currentDate = getVal("txtStopDate");

	}
	objCal1.onClick = "setDate";
	if ((strID == 1) || (strID == 0)) {
		objCal1.showCalendar(objEvent);
	} else if (blnCalander) {
		objCal1.showCalendar(objEvent);
	}

}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited("SC_SCHD_001", isEdited);

}

function ckheckPageEdited() {
	if (top[1].objTMenu.tabGetPageEidted("SC_SCHD_001"))
		return confirm("Changes will be lost! Do you wish to Continue?");
	else
		return true;
}

function setTimeWithColon(field, strTime) {
	if (trim(strTime) != "") {

		var hourindex = strTime.search(":");
		if (hourindex != -1) {
			setField(field, strTime);
		} else {
			var hh = strTime.substr(0, 2);
			var mm = strTime.substr(2, strTime.length);
			for ( var i = hh.length; i < 2; i++) {
				hh = "0" + hh;
			}
			for ( var i = mm.length; i < 2; i++) {
				mm = mm + "0";
			}
			mm = mm.substr(0, 2);
			var timecolon = hh + ":" + mm;
			setField(field, timecolon);
			strTime = timecolon;
		}
	}
	return IsValidTime(strTime);
}

function setLocalTime() {
	blnZuluTime = false;

	if (blngrdselected) {
		clearConvertSection();
		setField("txtStartDate", arrData[strGridRow][54]);
		setField("txtStopDate", arrData[strGridRow][55]);	
		arrLegs = arrData[strGridRow][32];
		displayLegs(arrLegs);
		displayFrequency();
	}
}

function setZuluTime() {
	blnZuluTime = true;

	if (blngrdselected) {
		clearConvertSection();		
		setField("txtStartDate", arrData[strGridRow][52]);
		setField("txtStopDate", arrData[strGridRow][53]);	
		arrLegs = arrData[strGridRow][23];
		displayLegs(arrLegs);
		displayFrequency();
	}
}

function clearConvertSection() {
	setField("txtStartDate", "");
	setField("txtStopDate", "");
	for ( var k = 1; k < 6; k++) {
		setField("txtD_Time" + k, "");
		setField("txtA_Time" + k, "");
		setField("selD_Day" + k, "0");
		setField("selA_Day" + k, "0");
	}
}

function displayLegs(legstodisp) {
	var j = 1;
	for (i = 0; i < legstodisp.length; i++) {
		setField("selFromStn" + j, legstodisp[i][1]);
		setField("selToStn" + j, legstodisp[i][2]);
		setField("txtD_Time" + j, legstodisp[i][3]);
		setField("selD_Day" + j, legstodisp[i][4]);
		setField("txtA_Time" + j, legstodisp[i][5]);
		setField("selA_Day" + j, legstodisp[i][6]);
		document.getElementById('spnDuration' + j).innerHTML = legstodisp[i][7];
		j++;
	}

}

function displayFrequency() {
	clearFrequency();
	if (blnZuluTime) {
		if (arrData[strGridRow][17] == "true")
			document.getElementById("chkSunday").checked = true;
		if (arrData[strGridRow][18] == "true")
			document.getElementById("chkMonday").checked = true;
		if (arrData[strGridRow][19] == "true")
			document.getElementById("chkTuesday").checked = true;
		if (arrData[strGridRow][20] == "true")
			document.getElementById("chkWednesday").checked = true;
		if (arrData[strGridRow][21] == "true")
			document.getElementById("chkThursday").checked = true;
		if (arrData[strGridRow][22] == "true")
			document.getElementById("chkFriday").checked = true;
		if (arrData[strGridRow][16] == "true")
			document.getElementById("chkSaturday").checked = true;
	} else {
		if (arrData[strGridRow][34] == "true")
			document.getElementById("chkSunday").checked = true;
		if (arrData[strGridRow][35] == "true")
			document.getElementById("chkMonday").checked = true;
		if (arrData[strGridRow][36] == "true")
			document.getElementById("chkTuesday").checked = true;
		if (arrData[strGridRow][37] == "true")
			document.getElementById("chkWednesday").checked = true;
		if (arrData[strGridRow][38] == "true")
			document.getElementById("chkThursday").checked = true;
		if (arrData[strGridRow][39] == "true")
			document.getElementById("chkFriday").checked = true;
		if (arrData[strGridRow][33] == "true")
			document.getElementById("chkSaturday").checked = true;
	}
}

function validateFrequency() {
	var validfr = true;
	if (blnZuluTime) {
		if ((arrData[strGridRow][17] == "true")
				&& (document.getElementById("chkSunday").checked == true))
			return true;
		if ((arrData[strGridRow][18] == "true")
				&& (document.getElementById("chkMonday").checked == true))
			return true;
		if ((arrData[strGridRow][19] == "true")
				&& (document.getElementById("chkTuesday").checked == true))
			return true;
		if ((arrData[strGridRow][20] == "true")
				&& (document.getElementById("chkWednesday").checked == true))
			return true;
		if ((arrData[strGridRow][21] == "true")
				&& (document.getElementById("chkThursday").checked == true))
			return true;
		if ((arrData[strGridRow][22] == "true")
				&& (document.getElementById("chkFriday").checked == true))
			return true;
		if ((arrData[strGridRow][16] == "true")
				&& (document.getElementById("chkSaturday").checked == true))
			return true;
		if (validfr) {
			showERRMessage(newFreq);
			return false;
		}

	} else {
		if ((arrData[strGridRow][34] == "true")
				&& (document.getElementById("chkSunday").checked == true))
			return true;
		if ((arrData[strGridRow][35] == "true")
				&& (document.getElementById("chkMonday").checked == true))
			return true;
		if ((arrData[strGridRow][36] == "true")
				&& (document.getElementById("chkTuesday").checked == true))
			return true;
		if ((arrData[strGridRow][37] == "true")
				&& (document.getElementById("chkWednesday").checked == true))
			return true;
		if ((arrData[strGridRow][38] == "true")
				&& (document.getElementById("chkThursday").checked == true))
			return true;
		if ((arrData[strGridRow][39] == "true")
				&& (document.getElementById("chkFriday").checked == true))
			return true;
		if ((arrData[strGridRow][33] == "true")
				&& (document.getElementById("chkSaturday").checked == true))
			if (validfr) {
				showERRMessage(newFreq);
				return false;
			}
	}

}

function selectFrequency() {

	if (document.getElementById("chkAll").checked) {

		document.getElementById("chkSunday").checked = true;
		document.getElementById("chkMonday").checked = true;
		document.getElementById("chkTuesday").checked = true;
		document.getElementById("chkWednesday").checked = true;
		document.getElementById("chkThursday").checked = true;
		document.getElementById("chkFriday").checked = true;
		document.getElementById("chkSaturday").checked = true;
	} else {
		document.getElementById("chkSunday").checked = false;
		document.getElementById("chkMonday").checked = false;
		document.getElementById("chkTuesday").checked = false;
		document.getElementById("chkWednesday").checked = false;
		document.getElementById("chkThursday").checked = false;
		document.getElementById("chkFriday").checked = false;
		document.getElementById("chkSaturday").checked = false;
	}
}

function settingValidation(cntfield) {
	if (top[1].objTMenu.focusTab == "SC_SCHD_001") {
		if (!dateChk(cntfield)) {
			showERRMessage(splinvaliddate);
			getFieldByID(cntfield).focus();
			return;
		}
	}
}

function editValidations() {

	var edStartD = formatDate(getVal("txtStartDate"), "dd/mm/yyyy",
			"dd-mmm-yyyy");
	var edStopD = formatDate(getVal("txtStopDate"), "dd/mm/yyyy", "dd-mmm-yyyy");
	var originalStartD;
	var originalStopD;

	if (blnZuluTime) {
		originalStartD = formatDate(arrData[strGridRow][52],"dd/mm/yyyy", "dd-mmm-yyyy");
		originalStopD = formatDate(arrData[strGridRow][53], "dd/mm/yyyy", "dd-mmm-yyyy");
	} else {
		originalStartD = formatDate(arrData[strGridRow][54],"dd/mm/yyyy", "dd-mmm-yyyy");
		originalStopD = formatDate(arrData[strGridRow][55],"dd/mm/yyyy", "dd-mmm-yyyy");
	}

	if ((compareDates(edStartD, originalStartD, '<'))
			&& (compareDates(edStopD, originalStartD, '<'))) {
		showERRMessage(editbothless);
		getFieldByID("txtStartDate").focus();
		return false;
	}

	if ((compareDates(edStartD, originalStopD, '>'))
			&& (compareDates(edStopD, originalStopD, '>'))) {
		showERRMessage(editbothgreat);
		getFieldByID("txtStartDate").focus();
		return false;
	}

	if (validateFrequency() == false)
		return false;

}

function hdeErrmessages() {
	objOnFocus();
}

function checkAircraftModel() {
	var valid = false;
	var gridData;
	var modelSelected = getFieldByID("selAircraftModel").value;
	for ( var i = 0; i < arrAircraftModelData.length; i++) {
		if ((modelSelected != "")
				&& (trim(modelSelected) == trim(arrAircraftModelData[i][0]))) {
			if ((strMode == "ADD") && (arrAircraftModelData[i][3] == "ACT")) {
				valid = true;
			} else if (strMode == "UPDATE" || strMode == "EDITLEG") {
				gridData = arrData[strGridRow][41];
				if ((trim(modelSelected) == trim(gridData))
						&& (arrAircraftModelData[i][3] != "ACT")) {
					valid = true;
				} else if (arrAircraftModelData[i][3] == "ACT") {
					valid = true;
				}

			}
			break;
		}
	}
	return valid;
}

function checkDepArrLeg(leg, legNumber, isDep) {
	var valid = false;
	var gridData;

	if (strMode == "EDITLEG") {
		if (blngrdselected) {
			if (blngrdselected && blnZuluTime) {

				gridData = arrData[strGridRow][23];
			} else {

				gridData = arrData[strGridRow][32];
			}
			for ( var i = 0; i < arrAptData.length; i++) {
				if (leg == arrAptData[i][0]) {
					valid = true;
					break;
				}
			}
			if ((!valid) && (legNumber <= gridData.length)) {
				if ((isDep) && (leg == gridData[(legNumber - 1)][1])) {
					valid = true;
				} else if ((!isDep) && (leg == gridData[(legNumber - 1)][2])) {
					valid = true;
				} else {
					valid = false;
				}
			}
		}
	} else {
		valid = true;
	}
	return valid;
}

function dissableAirports() {

	for ( var k = 1; k < 6; k++) {
		Disable("selFromStn" + k, true);
		Disable("selToStn" + k, true);
	}
}

function setOverlapleg() {
	var j = 1;
	for ( var i = 0; i < arrLegs.length; i++) {

		if (arrLegs[i][9] == "true") {
			Disable("selFromStn" + j, true);
			Disable("selToStn" + j, true);
		}
		j++;
	}

}

function setSegmentArray(strSegments) {
	var strValString = "";
	for ( var i = 0; i < strSegments.length; i++) {
		strValString += strSegments[i][1] + "_";
		strValString += strSegments[i][2] + ",";
	}
	setField("hdnSegArray", strValString);
}

function closeClick() {
	if (ckheckPageEdited()) {
		setPageEdited(false);
		objOnFocus();
		top[1].objTMenu.tabRemove('SC_SCHD_001')
	}
}

function confirmCancel(strAlerts, strEmail, strReschdalt, strReschdEml,
		strReschedAll, strTo, strSubject, strContent, strSmsAlert,
		strEmailAlert, strSmsCnf, strSmsOnH, strSmsAlert2, strEmailAlert2) {
	closeChildWindows();
	setField("hndCsOCCarrierCode", trim(getText("txtCsOCCarrierCode")));
	setField("hndCsOCFlightNo", trim(getText("txtCsOCFlightNo")));
	setField("txtFlightNoSearch", trim(getText("txtFlightNoSearch")));
	setField("txtFlightNoStartSearch", trim(getText("txtFlightNoStartSearch")));
	setCodeShareArray();
	if (document.forms[0].radTZ[1].checked == true)
		document.forms[0].hdnTimeMode.value = "ZULU";
	else
		document.forms[0].hdnTimeMode.value = "LOCAL";

	top[1].objTMenu
			.tabSetValue("SC_SCHD_001", intrecno + "^"
					+ getText("txtStartDateSearch") + ","
					+ getText("txtStopDateSearch") + ","
					+ getText("txtFlightNoSearch") + ","
					+ getValue("selFromStn6") + "," + getValue("selToStn6")
					+ "," + getValue("selOperationTypeSearch") + ","
					+ getValue("selStatusSearch") + ","
					+ getValue("selBuildStatusSearch") + ","
					+ getText("txtFlightNoStartSearch") + ","
					+ getValue("hdnTimeMode") + ","
					+ getValue("selFlightTypeSearch"));

	if (getFieldByID("hdnMode").value == "CANCEL") {
		setField("hdnMode", "CANCELFORCE");
	} else if (getFieldByID("hdnMode").value == "UPDATE") {
		setField("hdnMode", "UPDATE.CANCEL");
	} else {
		setField("hdnMode", "UPDATE.CANCELLEG");
	}

	setField("hdnCancelAlert", strAlerts);
	setField("hdnReSchedAlert", strReschdalt);
	setField("hdnCancelEmail", strEmail);
	setField("hdnReSchedEmail", strReschdEml);
	setField("hdnRescheduleAll", strReschedAll);

	setField("hdnEmailTo", strTo);
	setField("hdnEmailSubject", strSubject);
	setField("hdnEmailContent", strContent);

	setField("hdnSMSAlert", strSmsAlert);
	setField("hdnEmailAlert", strEmailAlert);
	setField("hdnSmsCnf", strSmsCnf);
	setField("hdnSmsOnH", strSmsOnH);
	setField("hdnSMSAlert2", strSmsAlert2);
	setField("hdnEmailAlert2", strEmailAlert2);

	disableInputSection("");
	disableLegSection("");

	setField("hdnReprotect", "false");
	document.getElementById("frmSchedule").target = "_self";
	document.forms[0].hdnRecNo.value = intrecno;
	document.forms[0].submit();
	top[2].ShowProgress();

}

function reprotectRefresh() {
	setField("hdnReprotect", "true");
	if (getFieldByID("hdnMode").value != "CANCEL") {
		disableInputSection(false);
		disableLegSection(false);
		disableSaveResetButtons(true);
	}
	if ((top[0].objCancelWindow) && (!top[0].objCancelWindow.closed)) {
		document.forms[0].target = "cancelWindow";
		document.forms[0].submit();
	} else {
		closeChildWindows();
	}

}

function closeCancelChildWindow() {
	if ((top[0].objCancelWindow) && (!top[0].objCancelWindow.closed)) {
		top[0].objCancelWindow.close();
	}
}
function closeReprotectChildWindow() {
	if ((top[0].objReprotectWindow) && (!top[0].objReprotectWindow.closed)) {
		top[0].objReprotectWindow.close();
	}
}
function closeRollforwardChildWindow() {
	if ((top[0].objRollForwardWindow) && (!top[0].objRollForwardWindow.closed)) {
		top[0].objRollForwardWindow.close();
	}
}
function closeChildWindows() {
	closeRollforwardChildWindow();
	closeReprotectChildWindow();
	closeCancelChildWindow();
}
// Haider 21Oct08 This function is called when the gds publish button clicked
function GDSPublishDetails() {

	if (ckheckPageEdited()) {
		var id = getValue("hdnScheduleId");
		
		clearInputSection();
		clearLegSection();
		setPageEdited(false);
		disableCommonButtons(true);
		var intLeft = (window.screen.width - 800) / 2;
		var intTop = (window.screen.height - 290) / 2;
		//var id = strRowData[12];
		var version = getValue("hdnVersion");
		var viewMode = getValue("hdnViewModeOnly");
		var gdsPublishing = getValue("hdnGDSPublishing");
		var mode = getValue("hdnMode");
		// document.getElementById("frmSchedule").hdnMode.value="";
		var url = "showGDSPublishing.action?hdnScheduleId=" + id
				+ "&hdnVersion=" + version + "&hdnViewModeOnly=" + viewMode
				+ "&hdnGDSPublishing=" + gdsPublishing + "&hdnMode=" + mode
				+"&hdnHaveMCFlight="+getValue("hdnHaveMCFlight");
		window
				.open(
						url,
						"CGDSWindow",
						'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=800,height=370,resizable=no,top='
								+ intTop + ',left=' + intLeft);
	}
}
function validatedTerminals(terminals) {	 
	 setField("hdnTerminalArray",terminals);	 
}
function validateTA(objControl, length) {
	var strValue = objControl.value;
	var strLength = strValue.length;
	var blnVal = isEmpty(strValue);
	if (blnVal) {
		setField("txtRemarks", strValue.substr(0, strLength - 1));
		getFieldByID("txtRemarks").focus();
	}
	if (strLength > length) {
		strValue = strValue.substr(0, length);
		objControl.value = strValue;
	}
}

function isAlphaNumericTextArea(s){return RegExp("^[a-zA-Z0-9-/ \w\s\&._]+$").test(s);}

function spclCharsVali(val){
	customValidate(val,/[`!@#$%*?\[\]{}()|\\\/+=:,;^~]/g);
}

//View CodeShare
function setCodeShareArr(CodeShareArr) {
	
	var control = document.getElementById("sel_CodeShare");
	for ( var i = 0; i < CodeShareArr.length; i++) {			//generating html
		control.options[i] = new Option(CodeShareArr[i][0], CodeShareArr[i][0]);
	}
}

function populateValuesForSeatChargeTemplate () {
	var selectedAirCraftModel = model.selAircraftModel;
	if (selectedAirCraftModel != null && selectedAirCraftModel != "") {
		var n = seatTemplates.split("|"); 
		var	strChargeTemplate='<option value=""></option>';
		for (var i=0;i<n.length-1;i++)
		{
			var n1=n[i].split("&"); 
			if(n1[0]==selectedAirCraftModel){
				
				strChargeTemplate = strChargeTemplate+ "<option value='" +n1[1] + "'>" +n1[2] + "</option>";
					
			}	
		}
			
		document.getElementById('selSeatChargeTemplate').innerHTML = strChargeTemplate;
	}
}

function isPublishedToCodeShare(val) {
//	val:"A1|W5440"
	if(getFieldByID("hdnMode").value != "ADD"){
		if(arrData[strGridRow][42]!=undefined && arrData[strGridRow][42]!=null){
			var publishedGdsIdsArr = arrData[strGridRow][42];
			if(val!=undefined && val!="" && publishedGdsIdsArr!=undefined 
					&& publishedGdsIdsArr!=null && publishedGdsIdsArr.length>0){
				var codeShareArr = val.split("|");		
				if(codeShareArr!=undefined && codeShareArr!=null 
						&& gdsCarrierCodeArray!=undefined && gdsCarrierCodeArray!=null){
					for(var i=0;i<gdsCarrierCodeArray.length;i++){
						var gdsCarrierCodeText = gdsCarrierCodeArray[i]; //"3|R1"
	
						var gdsCarrierCodeArr =gdsCarrierCodeText.split("|");
						var gdsId;
						if(gdsCarrierCodeArr[1] == codeShareArr[0]){
							gdsId = gdsCarrierCodeArr[0];
						}
						
						//check is already published to any code share carriers
						for(var j=0;j<publishedGdsIdsArr.length;j++){
							if(publishedGdsIdsArr[j]==gdsId){
								return true;
							}
						}
						
					}
				}
	
			}
		}
	}
	
	return false;
}

/**
 * restrict control operation of flight which is not actuallly operated by this carrier. 
 * */
function disableCtrlForCodeShare() {
	if (trim(getVal("txtCsOCFlightNo")) != ""
			&& trim(getVal("txtCsOCCarrierCode")) != "") {
		var cond = true;
		Disable("btnEdit", cond);
		Disable("btnSplit", cond);
		Disable("btnCopy", cond);
		Disable("btnFlightMnt", cond);
		Disable("btnEditLeg", cond);
		Disable("btnGDS", cond);
	}
}

//function viewMsgHistoryClick(){
//	setField("hdnShowMsgHistory", "true");
//	setField("hdnMode","VIEW");
//	var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'	
//		top[0].objWindow = window.open("about:blank","CWindow",strProp);
//		var objForm = document.getElementById("frmSchedule");
//		objForm.target = "CWindow";	
//		objForm.action = "showSchedule.action";
//		objForm.submit();
//		
//		setField("hdnShowMsgHistory", "fasle");
//}	

function viewEnterDatesClick(val){
	
	if(val!=null && val.trim()!=""){
		setField("hdnAuditType",val);
	}else{
		setField("hdnAuditType","IN_MSG");
	}
	
	$("#popup").dialog({
		open: function() {
			$('#fromDatePicker').removeAttr("disabled");
			$('#toDatePicker').removeAttr("disabled");
			if(document.getElementById("showResHistory") != null) {
				document.getElementById("showResHistory").checked = true;
			}
			document.getElementById("popup").style["display"]="inherit";
		},
		close: function () {
			$('#fromDatePicker').datepicker('hide');
			$('#toDatePicker').datepicker('hide');
			document.getElementById("popup").style["display"]="none";
			clearPopup();
		}
	});

	$("#fromDatePicker").datepicker({
		buttonImage: '../../images/calendar_no_cache.gif',
	    buttonImageOnly: true,
	    changeMonth: true,
	    changeYear: true,
	    showOn: 'both',
	    dateFormat: "dd/mm/yy",
	    buttonText: "View Calander"
	});
	$( "#fromDatePicker" ).datepicker( "option", "maxDate", "+0d" );
	
	$("#toDatePicker").datepicker({
		buttonImage: '../../images/calendar_no_cache.gif',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        showOn: 'both',
	    dateFormat: "dd/mm/yy",
	    buttonText: "View Calander"
	});
	$( "#toDatePicker" ).datepicker( "option", "maxDate", "+0d" );
}



function viewHistoryClick(){
	var fromDate = $("#fromDatePicker").val();
	var toDate = $("#toDatePicker").val();
	if(fromDate==""){
		showERRMessage("Please select the from date");
		return false;
	}
	if(toDate==""){
		showERRMessage("Please select the to date");
		return false;
	}
	$("#fromDate").val(fromDate);
	$("#toDate").val(toDate);
	if ($("#fromDatePicker").datepicker('getDate')>$("#toDatePicker").datepicker('getDate')){
		showERRMessage("The from date must fall earlier than the to date");
		return false;
	}
	setField("hdnShowResHistory", getChecked("showResHistory"));	
	setField("hdnMode","VIEW");
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'	
		top[0].objWindow = window.open("about:blank","CWindow",strProp);
		var objForm = document.getElementById("frmSchedule");
		objForm.target = "CWindow";	
		objForm.action = "showSchedule.action";
		objForm.submit();
		
		$("#popup").dialog('close');
		clearPopup();
}

function clearPopup(){
    $("#fromDatePicker").val("");	
    $("#toDatePicker").val("");
    setField("hdnAuditType","");
}

function hidePublisheMessageAudit(gdsList){
	if(gdsList != undefined && gdsList !=null && gdsList != ""){
		$('#btnPubMessages').show();
		Disable("btnPubMessages", false);				
	}else {		
		Disable("btnPubMessages", true);
		$('#btnPubMessages').hide();
	}	
}
