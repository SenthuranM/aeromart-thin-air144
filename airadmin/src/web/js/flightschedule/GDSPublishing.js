var screenID = "SC_SCHD_026";
var viewOnlyMode = getValue("hdnViewModeOnly");
var bEditMode = false;
var bDisabled = false;
function winOnLoad(strMsg, strMsgType) {
	if (strMsg != null && strMsgType != null)
		showWindowCommonError(strMsgType, strMsg);
	if (viewOnlyMode == "true")
		bDisabled = true;
	disableControls();
	// show popup
	if (trim(popupmessage).length != 0) {
		alert(popupmessage);
		opener.SearchData();
		closeClick();
	}

}
function disableControls() {
	nRows = arrData.length
	var bDisabledSave = bDisabled;
	if (objDG.loaded) {
		for ( var e = 0; e < nRows; e++) {
			if (objDG.getCellValue(e, 2) == "Inactive") {
				objDG.setDisable(e, "", true);
			//	bDisabledSave = true;
			} else {
				objDG.setDisable(e, "", bDisabled);
				bDisabledSave = bDisabled;
			}
		}
	} else {
		setTimeout("disableControls()", 300);
	}
	Disable("btnSave", bDisabledSave);
}
function onLoadGrid() {

}
function RowClick(strRowNo) {
}
function saveGDSPublishing() {
		var gdsIds = "";
		var CodeShareCarrier = false;
		for (i = 0; i < arrData.length; i++) {
				if (arrData[i][2] == "1") {
				if (gdsIds != "")
					gdsIds += ",";
				gdsIds += arrData[i][6];
	
				if (arrData[i][7] == "true") {
					CodeShareCarrier = true;
				}
			}
		}
		
		if(getValue("hdnHaveMCFlight")== "false" && CodeShareCarrier == true){
			alert("Please enter the codeshared flight number (CodeShare MC Flight) to continue..");
			
		}else{
		setField("hdnMode", "SAVE");
		setField("hdnGDSPublishing", gdsIds);
		setField("hdnVersion", version);
		var objForm2 = document.getElementById("GDSData");
		objForm2.target = "CGDSWindow";
		objForm2.action = "showGDSPublishing.action";
		objForm2.submit();
	}
}
function closeClick() {
	if (bEditMode == true) {
		if (confirm("Changes will be lost! Do you wish to Continue?") == true)
			window.close();
	} else
		window.close();
}
