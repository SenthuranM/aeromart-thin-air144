//Javascript Document
//Author -Shakir

var intCellPNR = -1
var strDAlterClass = ""
var strMode = opener.getFieldByID("hdnMode").value;
var blnPageedit = false;
var objWindow;
var arrData;

function cancelOnLoad() {
	if (opener.getFieldByID("hdnReprotect").value == "true") {
		opener.arrreproData = arrreproData;
	}
	arrData = opener.arrreproData;
	var strStatemet = "!There are existing Flights for the Schedule. Update will affect the existing Flights";

	document.getElementById('spnSchd').innerHTML = opener.reproScheduleid;
	document.getElementById('spnStartD').innerHTML = opener.reprostdate;
	document.getElementById('spnEndD').innerHTML = opener.reprostpdate;
	document.getElementById('spnSchedFlightNo').innerHTML = opener.reprofltno;
	document.getElementById('spnSchedDest').innerHTML = opener.reprodestination;
	document.getElementById('spnFreq').innerHTML = opener.reprofreq;
	document.getElementById('spnschedOrg').innerHTML = opener.reproorg;

	document.getElementById('spnFltId').innerHTML = opener.reproFlightId;
	document.getElementById('spnDepatDate').innerHTML = opener.reproFltDepDate;
	document.getElementById('spnFlightNo').innerHTML = opener.reproFltNum;
	document.getElementById('spnDest').innerHTML = opener.reproFltDest;
	document.getElementById('spnOrg').innerHTML = opener.reproFltOrg;

	if (opener.screenID == "SC_SCHD_001") {
		if (strMode != "CANCEL")
			document.getElementById('spnOverlappingString').innerHTML = strStatemet;
		document.getElementById('divSchedHeader').style.display = 'block';
		document.getElementById('divFlightHeader').style.display = 'none';
		document.getElementById('spnCancelHeader').innerHTML = 'Confirm Flight Schedule Cancellation/Update';
	} else if (opener.screenID == "SC_FLGT_001") {
		document.getElementById('divSchedHeader').style.display = 'none';
		document.getElementById('divFlightHeader').style.display = 'block';
		document.getElementById('spnCancelHeader').innerHTML = 'Confirm Flight Cancellation';
	}
	if (strMode != "CANCEL" && opener.screenID != "SC_FLGT_001") {
		document.getElementById('divAlertHeader').style.display = 'block';
		document.getElementById('divOverWrite').style.display = 'block';
		document.getElementById('divResched').style.display = 'block';
		document.getElementById('divSendSMSOrEmail').style.display = 'block';
		document.getElementById('divSMSForCnfOrOnH').style.display = 'block';

	} else {
		document.getElementById('divAlertHeader').style.display = 'none';
		document.getElementById('divOverWrite').style.display = 'none';
		document.getElementById('divResched').style.display = 'none';
		document.getElementById('divSendSMSOrEmail').style.display = 'none';
		document.getElementById('divSMSForCnfOrOnH').style.display = 'none';
	}
	if (strMode == "CANCEL") {
		if (arrData.length > 0) {
			document.getElementById('divAlertHeader').style.display = 'block';
			document.getElementById('divSendSMSOrEmail').style.display = 'block';
			document.getElementById('divSMSForCnfOrOnH').style.display = 'block';
		} else {
			document.getElementById('divAlertHeader').style.display = 'none';
			document.getElementById('divSendSMSOrEmail').style.display = 'none';
			document.getElementById('divSMSForCnfOrOnH').style.display = 'none';
		}
		setVisible("divUpdateButton", false);
		setVisible("divCancelButton", true);
	} else {
		setVisible("divCancelButton", false);
		setVisible("divUpdateButton", true);
	}

	if (arrData.length == 0) {
		document.getElementById('divCancelFlt').style.display = 'none';
		Disable("btnPaxDetails", true);
	} else if (arrData.length > 0) {
		document.getElementById('divCancelFlt').style.display = 'block';
	}
	document.getElementById('divEmailDesc').style.display = 'none';
	setVisible("divClearButton", false);
	writeTable();
	focusChildWindow();

	if (opener.sendSMSForPax == "Y") {
		Disable("chkSendSMS", false);
		Disable("chkSendSMS2", false);
		Disable("chkSmsCnf", true);
		Disable("chkSmsOnH", true);

	} else {
		Disable("chkSendSMS", true);
		Disable("chkSendSMS2", true);
		Disable("chkSmsCnf", true);
		Disable("chkSmsOnH", true);
	}
	if (opener.sendEmailForPax == "Y") {
		Disable("chkSendEmail", false);

	} else {
		Disable("chkSendEmail", true);
	}
}

function confirmClick() {
	var strAlerts = getFieldByID("chkGA1").checked;
	var strEmail = getFieldByID("chkSE1").checked;
	var strReschdalt = getFieldByID("chkGA2").checked;
	var strReschdEml = getFieldByID("chkSE2").checked;
	var strReschedAll = getFieldByID("radOverwrite").checked;
	var strTo = "";
	var strSubject = "";
	var strContent = "";

	var strSmsAlert = getFieldByID("chkSendSMS").checked;
	var strsmsAlert2 = getFieldByID("chkSendSMS2").checked;
	var strEmailAlert = getFieldByID("chkSendEmail").checked;
	var strEmailAlert2 = getFieldByID("chkSendEmail2").checked;

	var strSmsCnf = getFieldByID("chkSmsCnf").checked;
	var strSmsOnH = getFieldByID("chkSmsOnH").checked;

	if (strEmail || strReschdEml) {
		var strAddress = getFieldByID("txtTo").value;
		if (trim(strAddress) == "") {
			validateError(opener.noaddress);
			getFieldByID("txtTo").focus();
			return;
		}
		if (!checkEmail(strAddress)) {
			validateError(opener.invalidmailformat);
			getFieldByID("txtTo").focus();
			return;
		}
		var strContent = getFieldByID("txtContents").value;
		if (strContent.length > 255) {
			validateError(opener.invalidlength);
			getFieldByID("txtContents").focus();
			return;
		}
		strTo = getFieldByID("txtTo").value;
		strSubject = getFieldByID("txtSubject").value;
		strContent = getFieldByID("txtContents").value;
	}

	if (strSmsAlert || strsmsAlert2) {
		if (!strSmsCnf && !strSmsOnH) {

			validateError(opener.smsempty);
			getFieldByID("chkSmsCnf").focus();
			return;
		}
	}
	opener.confirmCancel(strAlerts, strEmail, strReschdalt, strReschdEml,
			strReschedAll, strTo, strSubject, strContent, strSmsAlert,
			strEmailAlert, strSmsCnf, strSmsOnH, strsmsAlert2, strEmailAlert2);
}

function reservationPNROnClick(strAlterClass, strRow) {
	if (intCellPNR != -1) {
		if (strDAlterClass != "") {
			setStyleClass("tdCol_0_" + intCellPNR,
					"GridItemRow GridItem cursorHand GridAlternateRow");
		} else {
			setStyleClass("tdCol_0_" + intCellPNR,
					"GridItemRow GridItem cursorHand");
		}
	}
	intCellPNR = strRow
	strDAlterClass = strAlterClass
	setStyleClass("tdCol_0_" + strRow, "GridItemSelected");
}

function writeTable() {
	var tempVar;

	var strHTMLText = "";

	strHTMLText = '<table width="100%" border="0" cellpadding="2" cellspacing="0" align="center">';
	strHTMLText += '<tr>';
	strHTMLText += '	<td width="3%"  align="centre" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font>&nbsp;</font></td>';
	strHTMLText += '	<td width="14%" align="centre" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font>Departure Date</font></td>';
	strHTMLText += '	<td width="10%" align="centre" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font>Flight ID</font></td>';
	strHTMLText += '	<td width="20%" align="centre" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font>Segment</font></td>';
	strHTMLText += '	<td width="15%" align="centre" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font>Seats Sold Adult/Infant</font></td>';
	strHTMLText += '	<td width="8%"  align="centre" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font>No of PNRs</font></td>';
	strHTMLText += '	<td width="15%" align="centre" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font>Seats on Hold Adult/Infant</font></td>';
	strHTMLText += '	<td width="8%"  align="centre" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font>No of PNRs</font></td>';
	strHTMLText += '	<td width="7%"  align="centre" class="GridRow GridHeader GridHeaderBand GridHDBorder"><font>&nbsp</font></td>';
	strHTMLText += '</tr>';

	var strCode1 = ""
	var intRowSpan = 0
	var strLCode = "";
	var strAlternateClass = "";
	var intSeqNo = 1;
	for ( var count = 0; count < arrData.length; count++) {
		strRowSpan = ""
		intRowSpan = 0;
		if (strLCode != arrData[count][2]) {
			strLCode = arrData[count][2];
			for ( var i = count; i < arrData.length; i++) {
				if (arrData[i][2] == strLCode) {
					intRowSpan++;
				} else {
					break;
				}
			}
			strRowSpan = 'rowspan="' + intRowSpan + '"';
		}

		strHTMLText += '		<tr>';

		if (strCode1 != arrData[count][2]) {
			if (strAlternateClass != "") {
				strAlternateClass = ""
			} else {
				strAlternateClass = " GridAlternateRow"
			}

			strHTMLText += ' <td ' + strRowSpan + ' align="center" class="GridRow GridHeader GridHeaderBand GridHDBorder">'
			strHTMLText += ' <font>' + (intSeqNo++) + '<\/font>';
			strHTMLText += ' <\/td>'
			strHTMLText += ' <td align="center" id="tdCol_0_' + intSeqNo + '" '
					+ strRowSpan + '  class="GridItemRow GridItem cursorHand '
					+ strAlternateClass + '"  onclick="reservationPNROnClick('
					+ "'" + strAlternateClass + "'" + ',' + intSeqNo + ')">'
			strHTMLText += ' <font>' + arrData[count][1] + '<\/font>';
			strHTMLText += ' <\/td>'
			strHTMLText += ' <td id="tdCol_0_' + intSeqNo + '" ' + strRowSpan
					+ '  class="GridItemRow GridItem cursorHand '
					+ strAlternateClass + '"  onclick="reservationPNROnClick('
					+ "'" + strAlternateClass + "'" + ',' + intSeqNo + ')">'
			strHTMLText += ' <font>' + arrData[count][2] + '<\/font>';
			strHTMLText += ' <\/td>'
		}

		strHTMLText += '			<td class="GridItemRow GridItem cursorHand '
				+ strAlternateClass + '" onclick="reservationPNROnClick(' + "'"
				+ strAlternateClass + "'" + ',' + intSeqNo + ')" ><font>'
				+ arrData[count][3] + '</font>';
		strHTMLText += '			</td>';
		strHTMLText += '			<td class="GridItemRow GridItem cursorHand '
				+ strAlternateClass + '" onclick="reservationPNROnClick(' + "'"
				+ strAlternateClass + "'" + ',' + intSeqNo + ')" ><font>'
				+ arrData[count][4] + '</font>';
		strHTMLText += '			</td>';
		strHTMLText += '			<td class="GridItemRow GridItem cursorHand '
				+ strAlternateClass + '" onclick="reservationPNROnClick(' + "'"
				+ strAlternateClass + "'" + ',' + intSeqNo + ')" ><font>'
				+ arrData[count][5] + '</font>';
		strHTMLText += '			</td>';
		strHTMLText += '			<td class="GridItemRow GridItem cursorHand '
				+ strAlternateClass + '" onclick="reservationPNROnClick(' + "'"
				+ strAlternateClass + "'" + ',' + intSeqNo + ')" ><font>'
				+ arrData[count][6] + '</font>';
		strHTMLText += '			</td>';
		strHTMLText += '			<td class="GridItemRow GridItem cursorHand '
				+ strAlternateClass + '" onclick="reservationPNROnClick(' + "'"
				+ strAlternateClass + "'" + ',' + intSeqNo + ')" ><font>'
				+ arrData[count][7] + '</font>';
		strHTMLText += '			</td>';

		if (strCode1 != arrData[count][2]) {
			strHTMLText += ' <td align="center" id="tdCol_0_' + intSeqNo + '" '
					+ strRowSpan + '  class="GridItemRow GridItem cursorHand '
					+ strAlternateClass + '"  onclick="reservationPNROnClick('
					+ "'" + strAlternateClass + "'" + ',' + intSeqNo + ')">'
			strHTMLText += ' <input name="btnRP" type="button" class="Button" id="btnRP" value="RP" style="width:20px"   title="Re-protect Flight" onClick="RPClick_V2(' + count + ')">';
			strHTMLText += ' </td>'
			strCode1 = arrData[count][2];
		}

		strHTMLText += '		<\/tr>';
	}
	if (arrData.length == 0) {
		strHTMLText += '<tr><td colspan="8" align="center"><font class="fntBold">No Data Found</font></td></tr>';
	}
	strHTMLText += '		<\/table>';
	DivWrite("spnCancel", strHTMLText);
}

function RPClick(rowNo) {
	setPageEdited(true);
	opener.closeReprotectChildWindow();
	var intLeft = (window.screen.width - 1000) / 2;
	var intTop = (window.screen.height - 765) / 2;
	var winWidth = 1000;
	var winheight = 750;

	var strFlightId = arrData[rowNo][2];
	var strActionUrl = "showFlightsToReprotect.action";
	strActionUrl += "?hdnFlightId=" + strFlightId;

	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
			+ winWidth
			+ ',height='
			+ winheight
			+ ',resizable=no,top='
			+ intTop
			+ ',left=' + intLeft;
	opener.top[0].objReprotectWindow = window.open(strActionUrl,
			"reprotectWindow", strProp);
}


function writeAlertHeader() {
	var strHTMLText = "";
	strHTMLText += '<table width="100%" border="0" cellpadding="0" cellspacing="4">';
	strHTMLText += '<tr>';
	strHTMLText += '<td width="40%">';
	strHTMLText += '<font class="fntBold">Send Alerts - Select Options</font>';
	strHTMLText += '</td>';
	strHTMLText += '<td width="15%">';
	strHTMLText += '<font class="fntBold">Alerts</font>';
	strHTMLText += '</td>';
	strHTMLText += '<td width="15%">';
	strHTMLText += '<font class="fntBold">Group Mail</font>';
	strHTMLText += '</td>';
	strHTMLText += '<td width="15%">';
	strHTMLText += '<font class="fntBold"> SMS PAX </font>';
	strHTMLText += '</td>';
	strHTMLText += '<td width="15%">';
	strHTMLText += '<font class="fntBold"> Email PAX </font>';
	strHTMLText += '</td>';
	strHTMLText += '</tr>';
	strHTMLText += '</table>';

	DivWrite("spnAlertHeader", strHTMLText);

}

function writeReSchedCheck() {
	var strHTMLText = "";
	strHTMLText += '<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table11">';
	strHTMLText += '<tr>';
	strHTMLText += '<td width="40%">';
	strHTMLText += '<font>Re-scheduled Flights </font>';
	strHTMLText += '</td>';
	strHTMLText += '<td width="15%">';
	strHTMLText += '<input type="checkbox" id="chkGA2" name="chkGA2" class="NoBorder"  onClick="dataChanged()" value="true" checked>';
	strHTMLText += '</td>';
	strHTMLText += '<td width="15%">';
	strHTMLText += '<input type="checkbox" id="chkSE2" name="chkSE2" class="NoBorder"  onClick="mailClick()" value="true">';
	strHTMLText += '</td>';
	strHTMLText += '<td width="15%">';
	strHTMLText += '<input type="checkbox" id="chkSendSMS2" name="chkSendSMS2" class="NoBorder"  onClick="enableSMSChk()" value="true">';
	strHTMLText += '</td>';
	strHTMLText += '<td width="15%">';
	strHTMLText += '<input type="checkbox" id="chkSendEmail2" name="chkSendEmail2" class="NoBorder"  onClick="" value="true">';
	strHTMLText += '</td>';
	strHTMLText += '</tr>';
	strHTMLText += '</table>';

	DivWrite("spnReSchedChk", strHTMLText);

}

function writeCancelCheck() {
	var strHTMLText = "";
	strHTMLText += '<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table12">';
	strHTMLText += '<tr>';
	strHTMLText += '<td width="40%">';
	strHTMLText += '<font>Cancelled Flights </font>';
	strHTMLText += '</td>';
	strHTMLText += '<td width="15%">';
	strHTMLText += '<input type="checkbox" id="chkGA1" name="chkGA1" class="NoBorder"  onClick="dataChanged()" value="true" checked>';
	strHTMLText += '</td>';
	strHTMLText += '<td width="15%">';
	strHTMLText += '<input type="checkbox" id="chkSE1" name="chkSE1" class="NoBorder"  onClick="mailClick()" value="true">';
	strHTMLText += '</td>';
	strHTMLText += '<td width="15%">';
	strHTMLText += '<input type="checkbox" id="chkSendSMS" name="chkSendSMS" class="NoBorder"  onClick="enableSMSChk()" value="true">';
	strHTMLText += '</td>';
	strHTMLText += '<td width="15%">';
	strHTMLText += '<input type="checkbox" id="chkSendEmail" name="chkSendEmail" class="NoBorder"  onClick="" value="true">';
	strHTMLText += '</td>';
	strHTMLText += '</tr>';
	strHTMLText += '</table>';

	DivWrite("spnCancelChk", strHTMLText);

}

function writeEmail() {
	var strHTMLText = "";
	strHTMLText += '<table width="100%" border="0" cellpadding="2" cellspacing="2">';
	strHTMLText += '	<tr>';
	strHTMLText += '		<td colspan="2"><hr></td>';
	strHTMLText += '	</tr>';
	strHTMLText += '	<tr>';
	strHTMLText += '		<td><font class="fntBold">EMail - To Call Center</font></td>';
	strHTMLText += '	</tr>';
	strHTMLText += '	<tr>';
	strHTMLText += '		<td><font>To</font></td>';
	strHTMLText += '		<td><input type="text" id="txtTo" style="width:150px;" maxlength="255" name="txtTo" value=""></td>';
	strHTMLText += '	</tr>';
	strHTMLText += '	<tr>';
	strHTMLText += '		<td><font>Subject</font></td>';
	strHTMLText += '		<td><input type="text" id="txtSubject" style="width:150px;" maxlength="255" name="txtSubject" value=""></td>';
	strHTMLText += '	</tr>';
	strHTMLText += '	<tr>';
	strHTMLText += '		<td><font>Contents</font></td>';
	strHTMLText += '		<td><textarea name="txtContents" id="txtContents" cols="50" rows="3"></textarea></td>';
	strHTMLText += '	</tr>';
	strHTMLText += '</table>';

	DivWrite("spnEmail", strHTMLText);
}

function cancelClick() {
	var cnf = confirm("Cancel or Update operation was not carried out. Do you wish to Continue?");
	if (cnf) {
		setPageEdited(false);
		if (strMode != "CANCEL") {
			opener.disableSaveResetButtons(false);
		}
		opener.closeChildWindows();
	}
}

function setPageEdited(isEdited) {
	blnPageedit = isEdited;
}

function dataChanged() {
	setPageEdited(true);
}

function mailClick() {
	dataChanged();
	if (getFieldByID("chkSE1").checked || getFieldByID("chkSE2").checked) {
		document.getElementById('divEmailDesc').style.display = 'block';
		window.resizeTo(720, 675);
		setVisible("divClearButton", true);
		if (arrData.length == 0) {
			window.resizeTo(720, 675);
		}
	} else {
		document.getElementById('divEmailDesc').style.display = 'none';
		setVisible("divClearButton", false);
		if (arrData.length == 0)
			window.resizeTo(720, 525);
	}

	if (!getFieldByID("chkSE1").checked) {
		window.resizeTo(720, 512);
	}

}

// Clear all input controls
function clearInputControls() {
	setField("txtTo", "");
	setField("txtSubject", "");
	setField("txtContents", "");

}

function enableInputControls() {
	Disable("txtTo", "");
	Disable("txtSubject", "");
	Disable("txtContents", "");

}

function clearClick() {
	clearInputControls();
	enableInputControls();
}

function validateError(msg) {
	objMsg.MessageText = msg;
	objMsg.MessageType = "Error";
	ShowPageMessage();
}

function writeScheduleHeading() {
	var strHTMLText = "";
	strHTMLText += '<table width="100%" border="0" cellpadding="0" cellspacing="0">';
	strHTMLText += '	<tr>';
	strHTMLText += '		<td>';
	strHTMLText += '			<font class="fntBold">Schedule ID</font>';
	strHTMLText += '			<font class="fntBold"><span id="spnSchd"></span><font>';
	strHTMLText += '		</td>';
	strHTMLText += '		<td>';
	strHTMLText += '			<font class="fntBold">Start Date</font>';
	strHTMLText += '			<font><span id="spnStartD" name="spnStartD"></span><font>';
	strHTMLText += '		</td>';
	strHTMLText += '		<td>';
	strHTMLText += '			<font class="fntBold">Stop Date</font>';
	strHTMLText += '			<font><span id="spnEndD" name="spnEndD"></span><font>';
	strHTMLText += '		</td>';
	strHTMLText += '	<tr>';
	strHTMLText += '	<tr>';
	strHTMLText += '		<td>';
	strHTMLText += '			<font class="fntBold">Flight Number</font>';
	strHTMLText += '			<font><span id="spnSchedFlightNo" name="spnSchedFlightNo"></span><font>';
	strHTMLText += '		</td>';
	strHTMLText += '		<td>';
	strHTMLText += '			<font class="fntBold">Origin</font>';
	strHTMLText += '			<font><span id="spnschedOrg"></span><font>';
	strHTMLText += '		</td>';
	strHTMLText += '		<td>';
	strHTMLText += '			<font class="fntBold">Destination</font>';
	strHTMLText += '			<font><span id="spnSchedDest"></span><font>';
	strHTMLText += '		</td>';
	strHTMLText += '		<td>';
	strHTMLText += '			<font class="fntBold">Frequency</font>';
	strHTMLText += '			<font><span id="spnFreq"></span><font>';
	strHTMLText += '		</td>';
	strHTMLText += '	<tr>';
	strHTMLText += '</table>';

	DivWrite("spnSchedHeader", strHTMLText);

}

function writeFlightHeading() {
	var strHTMLText = "";
	strHTMLText += '<table width="100%" border="0" cellpadding="0" cellspacing="0">';
	strHTMLText += '	<tr>';
	strHTMLText += '		<td>';
	strHTMLText += '			<font class="fntBold">Flight ID</font>';
	strHTMLText += '			<font class="fntBold"><span id="spnFltId"></span><font>';
	strHTMLText += '		</td>';
	strHTMLText += '		<td>';
	strHTMLText += '			<font class="fntBold">Depature Date</font>';
	strHTMLText += '			<font><span id="spnDepatDate" name="spnDepatDate"></span><font>';
	strHTMLText += '		</td>';
	strHTMLText += '	</tr>';
	strHTMLText += '	<tr>';
	strHTMLText += '		<td>';
	strHTMLText += '			<font class="fntBold">Flight Number</font>';
	strHTMLText += '			<font><span id="spnFlightNo" name="spnFlightNo"></span><font>';
	strHTMLText += '		</td>';
	strHTMLText += '		<td>';
	strHTMLText += '			<font class="fntBold">Origin</font>';
	strHTMLText += '			<font><span id="spnOrg" name="spnOrg"></span><font>';
	strHTMLText += '		</td>';
	strHTMLText += '		<td>';
	strHTMLText += '			<font class="fntBold">Destination</font>';
	strHTMLText += '			<font><span id="spnDest" name="spnDest"></span><font>';
	strHTMLText += '		</td>';
	strHTMLText += '	<tr>';
	strHTMLText += '</table>';

	DivWrite("spnFlightHeader", strHTMLText);

}

function focusChildWindow() {
	objWindow = opener.top[0].objReprotectWindow;
	if ((objWindow != "") && (objWindow != null) && (!objWindow.closed)) {
		objWindow.focus();
	}
}

function enableSMSChk() {

	if ((document.getElementById("chkSendSMS").checked == true)
			|| (document.getElementById("chkSendSMS2").checked == true)) {
		Disable("chkSmsCnf", false);
		Disable("chkSmsOnH", false);
	} else {
		Disable("chkSmsCnf", true);
		Disable("chkSmsOnH", true);
		if (document.getElementById("chkSmsCnf").checked == true) {
			document.getElementById("chkSmsCnf").checked = false;
		}
		if (document.getElementById("chkSmsOnH").checked == true) {
			document.getElementById("chkSmsOnH").checked = false;
		}
	}
}

// function writeSMSOrEmailCheck() {
// var strHTMLText = "";
// strHTMLText += '<table width="80%" border="0" cellpadding="0"
// cellspacing="4">';
// strHTMLText += '<tr>';
// strHTMLText += '<td width="5%" align="right">';
// strHTMLText += '<font> SMS PAX </font>';
// strHTMLText += '</td>';
// strHTMLText += '<td width="5%" align="left">';
// strHTMLText += '<input type="checkbox" id="chkSendSMS" name="chkSendSMS"
// class="NoBorder" onClick="enableSMSChk()" value="true">';
// strHTMLText += '</td>';
// strHTMLText += '<td width="5%" align="right">';
// strHTMLText += '<font> Email PAX </font>';
// strHTMLText += '</td>';
// strHTMLText += '<input type="checkbox" id="chkSendEmail" name="chkSendEmail"
// class="NoBorder" onClick="" value="true">';
// strHTMLText += '</td>';
// strHTMLText += '</tr>';
// strHTMLText += '</table>';
//	
// DivWrite("spnSMSOrEmailChk",strHTMLText);
//
// }

function writeSMSForCnfOrOnHCheck() {
	var strHTMLText = "";
	strHTMLText += '<table width="100%" border="0" cellpadding="0" cellspacing="4">';
	// strHTMLText += '<tr>';
	// strHTMLText += '<td colspan="5">';
	// strHTMLText += '<hr>';
	// strHTMLText += '</td>';
	// strHTMLText += '</tr>';
	strHTMLText += '<tr>';
	strHTMLText += '<td width="30%">';
	strHTMLText += '</td>';
	strHTMLText += '<td width="20%" align="center">';
	strHTMLText += '<font> SMS CNF PAX</font>';
	strHTMLText += '</td>';
	strHTMLText += '<td width="15%" align="center">';
	strHTMLText += '<input type="checkbox" id="chkSmsCnf" name="chkSmsCnf" class="NoBorder"  onClick="" value="true">';
	strHTMLText += '</td>';
	strHTMLText += '<td width="20%">';
	strHTMLText += '<font> SMS ON HOLD PAX </font>';
	strHTMLText += '</td>';
	strHTMLText += '<td width="15%">';
	strHTMLText += '<input type="checkbox" id="chkSmsOnH" name="chkSmsOnH" class="NoBorder"  onClick="" value="true">';
	strHTMLText += '</td>';
	strHTMLText += '</tr>';
	strHTMLText += '</table>';

	DivWrite("spnSMSForCnfOrOnHChk", strHTMLText);

}

function RPClick_V2(rowNo){
	setPageEdited(true);
	opener.closeReprotectChildWindow();
	var intLeft = (window.screen.width - 1000) / 2;
	var intTop = (window.screen.height - 765) / 2;
	var winWidth = 1000;
	var winheight = 750;

	var strFlightId = arrData[rowNo][2];
	var strActionUrl = "showFlightsToReprotect!execute_V2.action";
	strActionUrl += "?hdnFlightId=" + strFlightId;

	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
			+ winWidth
			+ ',height='
			+ winheight
			+ ',resizable=no,top='
			+ intTop
			+ ',left=' + intLeft;
	opener.top[0].objReprotectWindow = window.open(strActionUrl,
			"reprotectWindow", strProp);
}

function paxDetailsClick(){
	if(typeof(arrData) != 'undefined' &&  arrData != null){
		var intLeft = (window.screen.width - 1000) / 2;
		var intTop = (window.screen.height - 765) / 2;
		var winheight = 600;
		var winWidth = 970;

		var strFlightId = arrData[0][2] ;

		var strActionUrl = window.document.location.protocol;
		strActionUrl += "//"+window.document.location.host;
		strActionUrl += "/airadmin/private/master/";
		strActionUrl += "showFile!paxDetails.action";
		strActionUrl += "?hdnFlightId=" + strFlightId;

		var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
				+ winWidth+ ',height='+ winheight+ ',resizable=no,top='+ intTop+ ',left=' + intLeft;
		opener.top[0].objRollForwardWindow = window.open(strActionUrl,"paxDetials", strProp);
	}			
}

writeAlertHeader();
writeReSchedCheck();
writeCancelCheck();
writeEmail();
writeScheduleHeading();
writeFlightHeading();
// writeSMSOrEmailCheck();
writeSMSForCnfOrOnHCheck();
cancelOnLoad();
