function UI_reprotectPNR(){
	
	UI_reprotectPNR.agentWiseSeatMovementDataMap = null;
	UI_reprotectPNR.selectedPNRArray = null;
	UI_reprotectPNR.selectedAdultCount = null;
	UI_reprotectPNR.selectedSoldCount = 0;
	UI_reprotectPNR.selectedOhdCount = 0;
	UI_reprotectPNR.fltSegId = null;
	
	this.ready = function(){		
		$.ajax({
		    url:'selectPNRToReProtect.action',
		    beforeSend: ShowProgress(),
		    data:{"hdnFlightSegId":hidFlightID,"hdnCabinClass":hdnCabinClass},
		    dataType:"json",
		    complete: UI_reprotectPNR.pageLoadComplete,
		    error:UI_reprotectPNR.pageLoadError
		 });
	}
	this.pageLoadComplete = function(response){
		response = eval("("+response.responseText+")");		
		UI_reprotectPNR .agentWiseSeatMovementDataMap = response.pnrPaxMap;		
		UI_reprotectPNR.constructSeatMovementGrid(response.pnrPaxMap);
		
		UI_reprotectPNR.setCheckedItem();
		HideProgress();
		
	}
	this.pageLoadError = function(){
		HideProgress();
		alert("ERROR");
	}
	
	ShowProgress =function(){
		$("#divLoadMsg").show();
	}
	
	HideProgress =function(){
		$("#divLoadMsg").hide();
	}
	this.fillLocalGrid = function(p){
		var i = 0;
		p = $.extend({id:"",
					 data:null,
					 editable:false}, 
					p);
		$(p.id).clearGridData();
		if (p.data != null){
			$.each(p.data, function(){
				if ("D" != this.action){
					$(p.id).addRowData(i + 1, p.data[i]);
				}
				if ($(p.editable)){
					$(p.id).editRow(String(i+1));
				}
				i++;
			});
		}
	};

	
	this.setSelectedPnr = function(){
		var selected = new Array();
		var adultCount = 0;
		UI_reprotectPNR.selectedSoldCount = 0;
		UI_reprotectPNR.selectedOhdCount = 0;
        $("input:checkbox[name=pnr]:checked").each(function(){
            var val = $(this).val();
            selected.push(val);            
            try {
            	adultCount = adultCount + parseInt(String(val.split("#")[1]));
            	if(val.split("#")[2] == "CNF"){
            		UI_reprotectPNR.selectedSoldCount = UI_reprotectPNR.selectedSoldCount + parseInt(String(val.split("#")[1]));
            	} else {
            		UI_reprotectPNR.selectedOhdCount = UI_reprotectPNR.selectedOhdCount + parseInt(String(val.split("#")[1]));
            	}
            }catch(e) { }
        });
        UI_reprotectPNR.selectedPNRArray = selected;
        UI_reprotectPNR.checkAllOption();        
        try {
        	opener.objDGFrom.setCellValue(hdnSelRowNo, 4,adultCount);
        }catch(e) { }          
	}
		
	
	this.constructSeatMovementGrid = function(dataModel){
		$("#spnSeatMovementsInfo").empty();
		var gridTable = $("<table></table>").attr({"id": "tblGridSeatMove","align":"left"});
		
		setPassengerDetails = function(cellVal, options, rowObject){
			var strPax = "";
			strPax +="<ol>"
			$.each(cellVal, function(){
				strPax += "<li>"+this.firstName + " " + this.lastName + "(" +  this.paxType + ")</li>";
			});
			strPax +="</ol>"
			return strPax;
		}
		
		setArrivalFltNo = function(cellVal, options, rowObject){
			var strArrFltNo = "";
			strArrFltNo +="<ol>"
			$.each(cellVal, function(){
				if (this.arrivalIntlFlightNo != null && this.arrivalIntlFlightNo != "")  {
					strArrFltNo += "<li>"+this.arrivalIntlFlightNo +"</li>";
				}
				
			});
			strArrFltNo +="</ol>"
			return strArrFltNo;
		}
		
		setArrivalDateTime = function(cellVal, options, rowObject){
			var strArrTime = "";
			strArrTime +="<ol>"
			$.each(cellVal, function(){
				if (this.intlFlightArrivalDate != null && this.intlFlightArrivalDate != "")  {
					var tmpDate = this.intlFlightArrivalDate;
					var dateArr = tmpDate.split("T");
					var tmpDateArr = dateArr[0].split("-");
					strArrTime += "<li>"+tmpDateArr[2]+"/"+tmpDateArr[1]+"/"+tmpDateArr[0]+"  "+dateArr[1].substring(0,5) +"</li>";
				}
				
			});
			strArrTime +="</ol>"
			return strArrTime;
		}
		
		setDepartureFltNo = function(cellVal, options, rowObject){
			var strDeptFltNo = "";
			strDeptFltNo +="<ol>"
			$.each(cellVal, function(){
				if (this.departureIntlFlightNo != null && this.departureIntlFlightNo != "")  {
					strDeptFltNo += "<li>"+this.departureIntlFlightNo +"</li>";
				}
				
			});
			strDeptFltNo +="</ol>"
			return strDeptFltNo;
		}
		
		setDepartureDateTime = function(cellVal, options, rowObject){
			var strDeptTime = "";
			strDeptTime +="<ol>"
			$.each(cellVal, function(){
				if (this.intlFlightDepartureDate != null && this.intlFlightDepartureDate != "") {
					var tmpDate = this.intlFlightDepartureDate;
					var dateArr = tmpDate.split("T");
					var tmpDateDept = dateArr[0].split("-");
					strDeptTime += "<li>"+tmpDateDept[2]+"/"+tmpDateDept[1]+"/"+tmpDateDept[0]+"  "+dateArr[1].substring(0,5)+"</li>";
				}
				
			});
			strDeptTime +="</ol>"
			return strDeptTime;
		}
		
		setPnrPaxGroupId = function(cellVal, options, rowObject){
			var strPnrPaxGroup = "";
			strPnrPaxGroup +="<ol>"
			$.each(cellVal, function(){
				if (this.pnrPaxGroupId != null && this.pnrPaxGroupId != "")  {
					strPnrPaxGroup += "<li>"+this.pnrPaxGroupId +"</li>";
				}
			});
			strPnrPaxGroup +="</ol>"
			return strPnrPaxGroup;
		}
		
		setCheckBoxValue = function(cellVal, options, rowObject){
			var checked ='';
			var objIndex = options.rowId;
			var objName = options.colModel.name;				 
			
			return 	"<input type='checkbox' id='" + objIndex + "_" + objName +"' name='" + objName +
				"' "+ "  value='" + rowObject.pnr+"#"+rowObject.totalAdultCount+"#"+rowObject.status + "' onclick='UI_reprotectPNR.setSelectedPnr()' />";
		}
		
		setPaxSum = function(cellVal, options, rowObject){			
			return 	rowObject.totalAdultCount + "/" + rowObject.infantCount;
		}
	
		
		$("#spnSeatMovementsInfo").append(gridTable);
		
		var intlFlightDetailCaptureEnabled = opener.externalIntlFlightDetailCaptureEnabled;
		if (intlFlightDetailCaptureEnabled == "true") {
			var winWidth = 1130;
			var hideRow = false;
		} else {
			var winWidth = 650;
			var hideRow = true;
		}
		
		var temGrid = $("#tblGridSeatMove").jqGrid({
			datatype:"local",
			height: 650,
			width: winWidth,			  
			colNames:['<input type="checkbox" id="checkAll" name="checkAll" onclick="UI_reprotectPNR.setAllPNRSelected(event)"/>','PNR','Adult/Infant', 'Total PAX','Total Adult','Passenger','Status', 'Arrival Flight No', 'Intl Flight Arrival', 'Departure Flight No', 'Intl Flight Departure','Group Id'],
			colModel:[								
				{name:'pnr',index:'pnr', width:20, formatter : setCheckBoxValue,sortable:false },
			    {name:'pnr',index:'pnr', width:100, jsonmap:'pnrPaxMap.pnr'},				
			    {name:'paxSum',index:'paxSum', width:100, formatter : setPaxSum,sortable:false },
			    {name:'totalPaxCount',index:'totalPaxCount', width:100,hidden:true, jsonmap:'pnrPaxMap.totalPaxCount',sortable:false },
			    {name:'totalAdultCount',index:'totalAdultCount', width:100,hidden:true, jsonmap:'pnrPaxMap.totalAdultCount',sortable:false },
			    {name:'passengers',index:'passengers', width:250,formatter : setPassengerDetails,sortable:false },
			    {name:'status',index:'status', width:50,jsonmap:'pnrPaxMap.status',sortable:false },
			    {name:'passengers',index:'passengers', width:120,formatter : setArrivalFltNo,sortable:false, hidden : hideRow},
			    {name:'passengers',index:'passengers', width:120,formatter : setArrivalDateTime,sortable:false,  hidden :hideRow},
			    {name:'passengers',index:'passengers', width:120,formatter : setDepartureFltNo,sortable:false, hidden : hideRow},
			    {name:'passengers',index:'passengers', width:140,formatter : setDepartureDateTime,sortable:false,  hidden :hideRow},
			    {name:'passengers',index:'passengers', width:140,formatter : setPnrPaxGroupId,sortable:false }
			],
			imgpath: "",
			multiselect: false,
			viewrecords: true,
			altRows:true,
			altclass:"GridAlternateRow",
			rownumbers:true,			
			rowNum:0, 
			jsonReader: { 
				root: "rows",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
			onSelectRow: function(rowid){

			}
			
			
			
		});
		UI_reprotectPNR.fillLocalGrid({"id":temGrid, "data":dataModel, "editable":false});
	};
	
	this.confirm = function(){
		UI_reprotectPNR.setSelectedPnr();
		UI_reprotectPNR.updateSegByPNR();        
        UI_reprotectPNR.close();
	}
	
	this.close = function(){	
		window.close();
	}
	
	this.updateSegByPNR = function(){		
		var pnrList = UI_reprotectPNR.selectedPNRArray;
		var fltSegId = hidFlightID;
		
		var selected = opener.document.getElementById("hdnSelectedPNR").value;

		var pnrFullStr='';
		if(selected!=null && selected!=''){			
			var j=0;			
			var pnrByFltSeg = selected.split("@");
			$.each( pnrByFltSeg, function(){
				if(pnrByFltSeg[j]!=null){
					var fltSegArray = pnrByFltSeg[j].split(":");					
					if(fltSegId!=fltSegArray[0]) {
						pnrFullStr +=pnrByFltSeg[j]+"@";  
					} 
				}				
				j++;
			});		
			
		}
		
		if(pnrList!=null && pnrList!='' && fltSegId!=null && fltSegId!=''){
			pnrFullStr += fltSegId+':'+pnrList;
		}
		
		var objHidden = opener.document.getElementById("hdnSelectedPNR");
        objHidden.value = pnrFullStr;	
        
        var pnrSoldOhdCount = UI_reprotectPNR.selectedSoldCount+"#"+UI_reprotectPNR.selectedOhdCount;
        
        var objSoldOhdCount = opener.document.getElementById("hdnSelectedSoldOhdCount");
        objSoldOhdCount.value = pnrSoldOhdCount;
	}
	
	this.setCheckedItem = function(){
		var selected = opener.document.getElementById("hdnSelectedPNR").value;
		var fltSegId = hidFlightID;
		var isAllChecked = false;
		
		if(hdnTransferAll!=null && hdnTransferAll=='Y') {			
			UI_reprotectPNR.setAllItemChecked();				
			isAllChecked = true;
		}
		
		if(!isAllChecked && selected!=null && selected!=''){		
			var j=0;			
			var pnrByFltSeg = selected.split("@");
			$.each( pnrByFltSeg, function(){
				if(pnrByFltSeg[j]!=null){
					var fltSegArray = pnrByFltSeg[j].split(":");					
					if(fltSegId==fltSegArray[0]) {
						var i=0;
						var pnrArray = fltSegArray[1].split(",");
						$.each( pnrArray, function(){				
							$("input:checkbox[name=pnr]").each(function(){
								if(pnrArray[i]==$(this).val()){
									$(this).attr('checked',true);
								}						
							});
							i++;
						});						
					} 
				}				
				j++;
			});		
			
			UI_reprotectPNR.checkAllOption();			
		}		
	}
	
	
	this.setAllItemChecked = function(){			
		$("#checkAll").attr('checked',true);		
		$("#checkAll").attr('disabled',true); 
		$("input:checkbox[name=pnr]").each(function(){					
			$(this).attr('checked',true);
			$(this).attr('disabled',true); 
		});
	}
	
	this.setAllPNRSelected = function(e){
		e = e||event;/* get IE event ( not passed ) */ 
		e.stopPropagation? e.stopPropagation() : e.cancelBubble = true; 
		if($("#checkAll").is(':checked')){		
			$("input:checkbox[name=pnr]").each(function(){			
				$(this).attr('checked',true);			
			});
		}else{
			$("input:checkbox[name=pnr]").each(function(){			
				$(this).attr('checked',false);			
			});
		}		
	}
	
	this.checkAllOption = function(){
		if($("input:checkbox[name=pnr]").size()==$("input:checkbox[name=pnr]:checked").size() 
				&& $("input:checkbox[name=pnr]").size()!=0){
			$("#checkAll").attr('checked',true);				
		}else{
			$("#checkAll").attr('checked',false);				
		}
	}
}
UI_reprotectPNR = new UI_reprotectPNR();
UI_reprotectPNR.ready();