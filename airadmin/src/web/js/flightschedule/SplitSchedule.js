var blnPageedit = false;
var objWindow;
var strGridRow = opener.strGridRow;
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtSplitStart", strDate);
		break;
	case "1":
		setField("txtSplitStop", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 10;
	objCal1.left = 0;
	objCal1.showCalendar(objEvent);
	objCal1.onClick = "setDate";
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 10;
	if (strID == "0") {
		objCal1.left = 0;
	} else if (strID == "1") {
		objCal1.left = 220;
	}
	objCal1.showCalendar(objEvent);
	objCal1.onClick = "setDate";
}

function setPageEdited(isEdited) {
	blnPageedit = isEdited;
}

function dataChanged() {
	setPageEdited(true);
}

// On Page loading make sure that all the page input fields are cleared
function winOnLoad(strMsg, strMsgType) {
	clearInputControls();
	if (opener.blnZuluTime) {		
		setField("hdnStartD", opener.arrData[strGridRow][52]);
		setField("hdnStopD",  opener.arrData[strGridRow][53]);

	} else {
		setField("hdnStartD", opener.arrData[strGridRow][54]);
		setField("hdnStopD",  opener.arrData[strGridRow][55]);

	}

	setField("hdnScheduleId", opener.arrData[strGridRow][11]);
	enableFreq();
	setPageEdited(false);
	if (strMsg != null && strMsgType != null)
		showCommonError(strMsgType, strMsg);

	displayFrequency();
	var splscheduId = "Schedule ID " + opener.arrData[strGridRow][11];
	var spldaterange = "Choose a Split Date between (";

	var splToday = formatDate(getVal("hdnCurrentDate"), "dd/mm/yyyy",
			"dd-mmm-yyyy");
	var splStartD = formatDate(getVal("hdnStartD"), "dd/mm/yyyy", "dd-mmm-yyyy");

	if (compareDates(splToday, splStartD, '>=')) {
		setField("txtSplitStart", getFieldByID("hdnNextDate").value);
		spldaterange += getFieldByID("hdnNextDate").value + ") - (";
	} else {
		setField("txtSplitStart", getFieldByID("hdnStartD").value);
		spldaterange += getFieldByID("hdnStartD").value + ") - (";
	}

	setField("txtSplitStop", getFieldByID("hdnStopD").value);
	spldaterange += getFieldByID("hdnStopD").value + ")";
	document.getElementById('spnSplSchedid').innerHTML = splscheduId;
	document.getElementById('spnSplRange').innerHTML = spldaterange;
	getFieldByID("txtSplitStart").focus();
	if (opener.arrData[strGridRow][26] != ""
			&& opener.arrData[strGridRow][26] != "null") {
		setField("hdnOlapSchId", opener.arrData[strGridRow][26]);
		var sploSched = "Note : <br> Selected Flight Schedule is overlapping with another schedule "
				+ opener.arrData[strGridRow][26];
		document.getElementById('spnSplOverlap').innerHTML = sploSched;
	}

}

function beforeUnload() {
	if ((objWindow) && (!objWindow.closed)) {
		objWindow.close();
	}
}

function SplitConfirm() {
	var StopD = "";
	// Mandotory Fields
	if (isEmpty(getVal("txtSplitStart"))) {
		validateError(opener.splfromnull);
		getFieldByID("txtSplitStart").focus();
		return;
	}
	if (!isEmpty(getVal("txtSplitStop"))) {

		StopD = formatDate(getVal("txtSplitStop"), "dd/mm/yyyy", "dd-mmm-yyyy");
	}
	// compare start date and current date
	var Today = formatDate(getVal("hdnCurrentDate"), "dd/mm/yyyy",
			"dd-mmm-yyyy");
	var StartD = formatDate(getVal("txtSplitStart"), "dd/mm/yyyy",
			"dd-mmm-yyyy");
	var StopD = formatDate(getVal("txtSplitStop"), "dd/mm/yyyy", "dd-mmm-yyyy");
	var OriginalStartD = formatDate(getVal("hdnStartD"), "dd/mm/yyyy",
			"dd-mmm-yyyy");
	var OriginalStopD = formatDate(getVal("hdnStopD"), "dd/mm/yyyy",
			"dd-mmm-yyyy");

	if (!dateChk("txtSplitStart")) {
		validateError(opener.splinvaliddate);
		getFieldByID("txtSplitStart").focus();
		return;
	}

	if (!dateChk("txtSplitStop")) {
		validateError(opener.splinvaliddate);
		getFieldByID("txtSplitStop").focus();
		return;
	}
	if (compareDates(Today, StartD, '>=')) {
		validateError(opener.splfromlesscurr);
		getFieldByID("txtSplitStart").focus();
		return;
	}
	// compare start date and stop date
	if (!isEmpty(getVal("txtSplitStop"))) {
		if (compareDates(StartD, StopD, '>=')) {
			validateError(opener.spltolessfrom);
			getFieldByID("txtSplitStop").focus();
			return;
		}
	}

	// compare Date overlap

	if ((compareDates(StartD, OriginalStartD, '<'))
			&& (compareDates(StopD, OriginalStopD, '>'))) {
		validateError(opener.rangeclash);
		getFieldByID("txtSplitStart").focus();
		return;
	}

	if (StartD == OriginalStartD) {
		validateError(opener.splsamerange);
		getFieldByID("txtSplitStart").focus();
		return;
	}
	if ((compareDates(StartD, OriginalStartD, '<'))
			&& (compareDates(StopD, OriginalStartD, '<'))) {
		validateError(opener.splbothless);
		getFieldByID("txtSplitStart").focus();
		return;
	}

	if ((compareDates(StartD, OriginalStopD, '>'))
			&& (compareDates(StopD, OriginalStopD, '>'))) {
		validateError(opener.splboyhgreat);
		getFieldByID("txtSplitStart").focus();
		return;
	}

	if (compareDates(StartD, OriginalStartD, '<')) {
		validateError(opener.splstless);
		getFieldByID("txtSplitStart").focus();
		return;
	}

	if (compareDates(StopD, OriginalStopD, '>')) {
		validateError(opener.splstpgreat);
		getFieldByID("txtSplitStop").focus();
		return;
	}

	// check Frequency all null
	if (document.getElementById("chkSunday").checked == false) {
		if (document.getElementById("chkMonday").checked == false) {
			if (document.getElementById("chkTuesday").checked == false) {
				if (document.getElementById("chkWednesday").checked == false) {
					if (document.getElementById("chkThursday").checked == false) {
						if (document.getElementById("chkFriday").checked == false) {
							if (document.getElementById("chkSaturday").checked == false) {
								validateError(opener.splfrequency);
								return;
							}
						}
					}
				}
			}
		}
	}

	var strFromD = getFieldByID("txtSplitStart").value;
	var strToD = "";
	if (trim(getVal("txtSplitStop")) == "") {
		strToD = getVal("hdnStopD");
	} else {
		strToD = getFieldByID("txtSplitStop").value;
	}

	var strSchdId = getFieldByID("hdnScheduleId").value;
	var strOlschdId = getFieldByID("hdnOlapSchId").value;
	var freqSunday = document.getElementById("chkSunday").checked;
	var freqMonday = document.getElementById("chkMonday").checked;
	var freqTuesday = document.getElementById("chkTuesday").checked;
	var freqWednesday = document.getElementById("chkWednesday").checked;
	var freqThursday = document.getElementById("chkThursday").checked;
	var freqFriday = document.getElementById("chkFriday").checked;
	var freqSaturday = document.getElementById("chkSaturday").checked;

	var strSearchCriteria = opener.top[1].objTMenu.tabGetValue("SC_SCHD_001")
			.split("^")[1];
	var strSearch = strSearchCriteria.split(",");
	var searchstd = strSearch[0];
	var searchstpd = strSearch[1];
	var searchfltno = strSearch[2];
	var searchfrm = strSearch[3];
	var searchto = strSearch[4];
	var searchoptype = strSearch[5];
	var searchstat = strSearch[6];
	var searchbldst = strSearch[7];
	var searchFltstart = strSearch[8];
	var searchtime = strSearch[9];

	opener.disableCommonButtons("false");

	if ((trim(strOlschdId) != "") && (trim(strOlschdId) != "null")) {
		var strconfirm = confirm("The Schedule overlaps with "
				+ strOlschdId
				+ " click'ok' to split Both schedules or click 'cancel' to cancel");
		if (strconfirm == true) {
			opener.location = "showSchedule.action?&hdnMode=SPLIT&hdnScheduleId="
					+ strSchdId
					+ "&hdnSplitStartD="
					+ strFromD
					+ "&hdnSplitStopD="
					+ strToD
					+ "&chkSunday="
					+ freqSunday
					+ "&chkMonday="
					+ freqMonday
					+ "&chkTuesday="
					+ freqTuesday
					+ "&chkWednesday="
					+ freqWednesday
					+ "&chkThursday="
					+ freqThursday
					+ "&chkFriday="
					+ freqFriday
					+ "&chkSaturday="
					+ freqSaturday
					+ "&txtStartDateSearch="
					+ searchstd
					+ "&txtStopDateSearch="
					+ searchstpd
					+ "&txtFlightNoSearch="
					+ searchfltno
					+ "&selFromStn6="
					+ searchfrm
					+ "&selToStn6="
					+ searchto
					+ "&selOperationTypeSearch="
					+ searchoptype
					+ "&selStatusSearch="
					+ searchstat
					+ "&selBuildStatusSearch="
					+ searchbldst
					+ "&txtFlightNoStartSearch=" + searchFltstart;
			opener.top[0].objWindow.close();
			opener.top[2].ShowProgress();
		}

	} else {

		opener.location = "showSchedule.action?&hdnMode=SPLIT&hdnScheduleId="
				+ strSchdId + "&hdnSplitStartD=" + strFromD + "&hdnSplitStopD="
				+ strToD + "&chkSunday=" + freqSunday + "&chkMonday="
				+ freqMonday + "&chkTuesday=" + freqTuesday + "&chkWednesday="
				+ freqWednesday + "&chkThursday=" + freqThursday
				+ "&chkFriday=" + freqFriday + "&chkSaturday=" + freqSaturday
				+ "&txtStartDateSearch=" + searchstd + "&txtStopDateSearch="
				+ searchstpd + "&txtFlightNoSearch=" + searchfltno
				+ "&selFromStn6=" + searchfrm + "&selToStn6=" + searchto
				+ "&selOperationTypeSearch=" + searchoptype
				+ "&selStatusSearch=" + searchstat + "&selBuildStatusSearch="
				+ searchbldst + "&txtFlightNoStartSearch=" + searchFltstart;
		opener.top[0].objWindow.close();
		opener.top[2].ShowProgress();

	}

}

// Clear all input controls
function clearInputControls() {
	setField("txtSplitStart", "");
	setField("txtSplitStop", "");
}

function validateError(msg) {
	objMsg.MessageText = msg;
	objMsg.MessageType = "Error";
	ShowPageMessage();
}

function enableFreq() {
	Disable("chkSunday", "");
	Disable("chkMonday", "");
	Disable("chkTuesday", "");
	Disable("chkWednesday", "");
	Disable("chkThursday", "");
	Disable("chkFriday", "");
	Disable("chkSaturday", "");

}

function cancelClick() {
	if (blnPageedit) {
		var cnf = confirm("Changes will be lost! Do you wish to Continue?");
		if (cnf) {
			setPageEdited(false);
			window.close();
		}
	} else {
		setPageEdited(false);
		window.close();
	}
}

function displayFrequency() {
	if (opener.blnZuluTime) {
		if (opener.arrData[strGridRow][17] != "true")
			Disable("chkSunday", "false");
		else
			document.getElementById("chkSunday").checked = true;

		if (opener.arrData[strGridRow][18] != "true")
			Disable("chkMonday", "false");
		else
			document.getElementById("chkMonday").checked = true;

		if (opener.arrData[strGridRow][19] != "true")
			Disable("chkTuesday", "false");
		else
			document.getElementById("chkTuesday").checked = true;

		if (opener.arrData[strGridRow][20] != "true")
			Disable("chkWednesday", "false");
		else
			document.getElementById("chkWednesday").checked = true;

		if (opener.arrData[strGridRow][21] != "true")
			Disable("chkThursday", "false");
		else
			document.getElementById("chkThursday").checked = true;

		if (opener.arrData[strGridRow][22] != "true")
			Disable("chkFriday", "false");
		else
			document.getElementById("chkFriday").checked = true;

		if (opener.arrData[strGridRow][16] != "true")
			Disable("chkSaturday", "false");
		else
			document.getElementById("chkSaturday").checked = true;

	} else {
		if (opener.arrData[strGridRow][34] != "true")
			Disable("chkSunday", "false");
		else
			document.getElementById("chkSunday").checked = true;

		if (opener.arrData[strGridRow][35] != "true")
			Disable("chkMonday", "false");
		else
			document.getElementById("chkMonday").checked = true;

		if (opener.arrData[strGridRow][36] != "true")
			Disable("chkTuesday", "false");
		else
			document.getElementById("chkTuesday").checked = true;

		if (opener.arrData[strGridRow][37] != "true")
			Disable("chkWednesday", "false");
		else
			document.getElementById("chkWednesday").checked = true;

		if (opener.arrData[strGridRow][38] != "true")
			Disable("chkThursday", "false");
		else
			document.getElementById("chkThursday").checked = true;

		if (opener.arrData[strGridRow][39] != "true")
			Disable("chkFriday", "false");
		else
			document.getElementById("chkFriday").checked = true;

		if (opener.arrData[strGridRow][33] != "true")
			Disable("chkSaturday", "false");
		else
			document.getElementById("chkSaturday").checked = true;

	}
}
var spldayoffset = opener.dayOffset;
function writeFrequency() {

	if (spldayoffset == "")
		spldayoffset = "0";
	var dayArr = new Array();
	dayArr[0] = new Array("chkSunday", "Su", "Sunday");
	dayArr[1] = new Array("chkMonday", "Mo", "Monday");
	dayArr[2] = new Array("chkTuesday", "Tu", "Tuesday");
	dayArr[3] = new Array("chkWednesday", "We", "Wednesday");
	dayArr[4] = new Array("chkThursday", "Th", "Thirsday");
	dayArr[5] = new Array("chkFriday", "Fr", "Friday");
	dayArr[6] = new Array("chkSaturday", "Sa", "Saturday");

	var strHTMLText = "";
	var i = parseInt(spldayoffset) % 7;

	strHTMLText += '<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table10" align="center">';
	strHTMLText += '<tr>';
	strHTMLText += '	<td colspan="6">';
	for ( var j = 0; j < 7; j++) {
		if (i == 7)
			i = 0;
		strHTMLText += '<font>' + dayArr[i][1]
				+ '</font>&nbsp;<input type="checkbox" id=' + dayArr[i][0]
				+ ' name=' + dayArr[i][0]
				+ ' class="NoBorder" onchange="dataChanged()" title='
				+ dayArr[i][2] + '>&nbsp;&nbsp;';

		i++
	}

	strHTMLText += '		<font class="mandatory"> &nbsp;* </font>';
	strHTMLText += '	</td>';
	strHTMLText += '</tr>';
	strHTMLText += '</table>';
	DivWrite("spnSplFrequency", strHTMLText);

}
writeFrequency();