var varPageNoRec = 20; // number of records to display on the page
var blnRowClick = false;
var intrecno = 1;
if (top[1].objTMenu.tabGetValue("SC_SCHD_001").split("^")[0] != "")
	intrecno = top[1].objTMenu.tabGetValue("SC_SCHD_001").split("^")[0];

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "6%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Flight No";
objCol1.headerText = "Flight";
objCol1.itemAlign = "center"
objCol1.sort = true;

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "6%";
objCol2.arrayIndex = 2;
objCol2.toolTip = "From Date";
objCol2.headerText = "From Date<br>zulu";
objCol2.itemAlign = "center"
objCol2.sort = true;

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "6%";
objCol3.arrayIndex = 3;
objCol3.toolTip = "To Date";
objCol3.headerText = "To Date<br>zulu";
objCol3.itemAlign = "center"
objCol3.sort = true;

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "11%";
objCol4.arrayIndex = 4;
// objCol4.toolTip = "Frequency" ;
objCol4.headerText = "Frequency";
objCol4.itemAlign = "center"

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "16%";
objCol5.arrayIndex = 5;
// objCol5.toolTip = "Segments" ;
objCol5.headerText = "Segments";
objCol5.itemAlign = "left"

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "6%";
objCol6.arrayIndex = 6;
// objCol6.toolTip = "Operation Type" ;
objCol6.headerText = "Operation <br> Type";
objCol6.itemAlign = "center"

var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "5%";
objCol7.arrayIndex = 7;
objCol7.toolTip = "Available Seat Kilometers in 000's";
objCol7.headerText = "ASK'<br>000";
objCol7.itemAlign = "right"

var objCol8 = new DGColumn();
objCol8.columnType = "label";
objCol8.width = "4%";
objCol8.arrayIndex = 8;
objCol8.toolTip = "Overlap schedule";
objCol8.headerText = "Olap<br>Sched.";
objCol8.itemAlign = "center"

var objCol9 = new DGColumn();
objCol9.columnType = "label";
objCol9.width = "5%";
objCol9.arrayIndex = 9;
objCol9.toolTip = "Number of flights.";
objCol9.headerText = "Total<br>Flts";
objCol9.itemAlign = "right"

var objCol10 = new DGColumn();
objCol10.columnType = "label";
objCol10.width = "6%";
objCol10.arrayIndex = 10;
// objCol10.toolTip = "Build Status" ;
objCol10.headerText = "Build <br> Status";
objCol10.itemAlign = "center"

var objCol11 = new DGColumn();
objCol11.columnType = "label";
objCol11.width = "5%";
objCol11.arrayIndex = 43;
objCol11.toolTip = "Number of Open Flights";
objCol11.headerText = "Open<br>Flts";
objCol11.itemAlign = "right"

var objCol12 = new DGColumn();
objCol12.columnType = "label";
objCol12.width = "6%";
objCol12.arrayIndex = 12;
objCol12.toolTip = "Schedule status";
objCol12.headerText = "Sched.<br>Status";
objCol12.itemAlign = "center"

var objCol13 = new DGColumn();
objCol13.columnType = "checkbox";
objCol13.width = "3%";
objCol13.arrayIndex = 51;
// objCol13.toolTip = "Select All" ;
objCol13.checkAll = false;
objCol13.linkOnClick = "chkvalidate";
objCol13.headerText = "";
objCol13.itemAlign = "center"

var objCol14 = new DGColumn();
objCol14.columnType = "label";
objCol14.width = "5%";
objCol14.arrayIndex = 29;
objCol14.toolTip = "Depature Time in ZULU";
objCol14.headerText = "D.Time<br>Zulu";
objCol14.itemAlign = "center"

var objCol15 = new DGColumn();
objCol15.columnType = "label";
objCol15.width = "5%";
objCol15.arrayIndex = 28;
objCol15.toolTip = "Depature Time in LOCAL";
objCol15.headerText = "D.Time<br>Local";
objCol15.itemAlign = "center"

var objCol16 = new DGColumn();
objCol16.columnType = "label";
objCol16.width = "8%";
objCol16.arrayIndex = 47;
objCol16.toolTip = "Flight Type";
objCol16.headerText = "Flight<br/>Type";
objCol16.itemAlign = "center"

// ---------------- Grid
var objDG = new DataGrid("spnFlights");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol15);
objDG.addColumn(objCol14);
objDG.addColumn(objCol6);
objDG.addColumn(objCol7);
objDG.addColumn(objCol8);
objDG.addColumn(objCol9);
// objDG.addColumn(objCol12); // removed the schedule status
objDG.addColumn(objCol10);
objDG.addColumn(objCol11);
objDG.addColumn(objCol16);
objDG.addColumn(objCol13);

objDG.width = "99%";
objDG.height = "140px";
objDG.headerHeight = "32px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = arrData;
objDG.seqNo = true;
objDG.seqNoWidth = "3%";
objDG.seqStartNo = intrecno;
objDG.pgnumRecTotal = totalNoOfRecords; // remove as per return record size
objDG.paging = true
objDG.pgnumRecPage = 20;
objDG.rowClick = "RowClick";
objDG.pgonClick = "gridNavigations";
objDG.controlValue = 11; // control Value to return the selected row IDS;
objDG.displayGrid();

function objOnFocus() {
	top[2].HidePageMessage();
}

// function to get Schedules needed to build
function chkvalidate() {
	disableCommonButtons("false");
	var strArray = new Array();
	var count = 0;
	var selscedtobld = objDG.getSelectedColumn(14);
	for ( var i = 0; i < selscedtobld.length; i++) {
		if ((selscedtobld[i][1]) && (arrData[i][10] != "BLT")) {
			if (validateBuild(arrData[i])) {
				strArray[count] = arrData[i][11];
				count++
			}
		}

	}
	if (strArray.length > 0) {
		Disable("btnBuild", "");
	}

}

// Function validate the Build enable or not
function validateBuild(dataRow) {

	var StartD;
	var Today = formatDate(getVal("hdnCurrentDate"), "dd/mm/yyyy",
			"dd-mmm-yyyy");
	var StopD;
	if (blnZuluTime) {
		StartD = formatDate(dataRow[2], "dd/mm/yyyy", "dd-mmm-yyyy");
		StopD = formatDate(dataRow[3], "dd/mm/yyyy", "dd-mmm-yyyy");
	} else {
		StartD = formatDate(dataRow[30], "dd/mm/yyyy", "dd-mmm-yyyy");
		StopD = formatDate(dataRow[31], "dd/mm/yyyy", "dd-mmm-yyyy");
	}

	if (compareDates(Today, StartD, '>=')
			|| (!(dataRow[12] == "ACT") && !(dataRow[12] == "ERR"))) {
		return false;
	}
	if (compareDates(Today, StopD, '>=')
			|| (!(dataRow[12] == "ACT") && !(dataRow[12] == "ERR"))) {
		return false;
	}
	if (dataRow[10] == "BLT") {
		return false;
	}
	if (dataRow[12] == "ERR") {
		return false;
	}
	return true;
}

// function for select all click
function SelectAll() {
	blnRowClick = false;
	if (document.forms[0].chkSelectAll.checked) {
		var intLength = arrData.length;
		for ( var i = 0; i < intLength; i++) {
			objDG.setCellValue(i, 14, "true");
		}
		disableCommonButtons("false");
		disablePrintBuildButtons("");
	} else {
		var intLength = arrData.length;
		for ( var i = 0; i < intLength; i++) {
			objDG.setCellValue(i, 14, "");
		}
		disableCommonButtons("flase");
		disablePrintBuildButtons("false");
	}
	clearInputSection();
	clearLegSection();
	disableInputSection("false");
	disableLegSection("false");
	disableSaveResetButtons("false");

}

// Function write selecct all in absolute position
function writeSelectAll() {
	var strHTMLText = "";

	strHTMLText += '<table width="100" border="0" cellpadding="0" cellspacing="0" ID="Table10" align="right" >';//style="position:absolute;left:805px;top:300px;"
	strHTMLText += '<tr>';
	strHTMLText += '	<td align="right">';
	strHTMLText += '	<input type="checkbox" name="chkSelectAll" id="chkSelectAll" class="NoBorder" onClick="SelectAll()" tabindex="11">';
	strHTMLText += '	</td>';
	strHTMLText += '	<td align="left" width="135">';
	strHTMLText += '	<font>Select All</font>';
	strHTMLText += '	</td>';
	strHTMLText += '	</tr>';
	strHTMLText += '	</table>';

	DivWrite("spn_SA_spnFlights", strHTMLText);

}

// Function writes frequency dynamically
function writeFrequency() {
	if (dayOffset == "")
		dayOffset = "0";
	var dayArr = new Array();
	dayArr[0] = new Array("chkSunday", "Su", "Sunday");
	dayArr[1] = new Array("chkMonday", "Mo", "Monday");
	dayArr[2] = new Array("chkTuesday", "Tu", "Tuesday");
	dayArr[3] = new Array("chkWednesday", "We", "Wednesday");
	dayArr[4] = new Array("chkThursday", "Th", "Thirsday");
	dayArr[5] = new Array("chkFriday", "Fr", "Friday");
	dayArr[6] = new Array("chkSaturday", "Sa", "Saturday");

	var strHTMLText = "";
	var i = parseInt(dayOffset) % 7;

	strHTMLText += '<table width="99%" border="0" cellpadding="0" cellspacing="0" ID="Table10" align="center">';
	strHTMLText += '<tr>';
	strHTMLText += '	<td><font>Frequency</font></td>';
	strHTMLText += '	<td align="right"><input type="checkbox" id="chkAll" name="chkAll" class="NoBorder" onClick="selectFrequency()" onchange="dataChanged()" tabindex="20" title="All"></td>';
	strHTMLText += '	<td align="left" style="padding-right:3px;"><font>All</font></td>';

	for ( var j = 0; j < 7; j++) {
		if (i == 7)
			i = 0;
		strHTMLText += '<td align="right"><input type="checkbox" id='
				+ dayArr[i][0]
				+ ' name='
				+ dayArr[i][0]
				+ ' class="NoBorder" onchange="dataChanged()" tabindex="27" title='
				+ dayArr[i][2] + '></td>';
		strHTMLText += '<td align="left" style="padding-right:3px;"><font>' + dayArr[i][1] + '</font></td>';
		i++
	}

	strHTMLText += '		<font class="mandatory"> &nbsp;* </font>';
	strHTMLText += '	</td>';
	strHTMLText += '</tr>';
	strHTMLText += '</table>';
	DivWrite("spnFrequency", strHTMLText);

}


writeFrequency();
