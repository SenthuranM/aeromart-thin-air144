
var seatCharge = new Array();
var arrSelected = new Array();
var blnPageedit = false;
var logicalCCCode = "";

function drawSeatMap() {

	var strSTMap = "";
	var rowName = "";
	var colName = "";
	var seatId = "";
	var seatCode = "";
	var seatDescp = "";
	var seatstatus = "";
	var cbnRow = 0;
	var cablength = 0;
	var classlength = 0;
	var rowno = 1;
	var exitTable = "";
	var seatType = "";
	var exitMargin = 0;
	var arrColname = new Array();
	var seatVisibility = "";
	var seatVisibilityTag = "";
	var rowStatus = new Array();

	strSTMap += '<table width="100%" cellpadding="0" cellspacing="1" border="1" class="BGColor" align="center">';
	strSTMap += '<tr>';

	for ( var clsint = 0; clsint < stMap.length; clsint++) {
		classlength = 0;
		strSTMap += '			<td>';
		strSTMap += '			<table width="100%" border="0" cellpadding="0" cellspacing="0">';
		strSTMap += '				<tr>';
		strSTMap += '					<td width="100%" align="left" valign="middle">';

		if (stMap[clsint].length > 0) {
			strSTMap += '					<table width="100%" height="124" border="0" cellpadding="0" cellspacing="4">';
			strSTMap += '						<tr>';

			for ( var flgrp = 0; flgrp < stMap[clsint].length; flgrp++) {
				// clomn change
				strSTMap += '						<td>';
				strSTMap += '						<table width="100%" cellpadding="2">';
				for (rwgp = stMap[clsint][flgrp].length - 1; rwgp >= 0; rwgp--) {
					
					for ( var ftrw = stMap[clsint][flgrp][rwgp].length - 1; ftrw >= 0; ftrw--) {

						rowName = stMap[clsint][flgrp][rwgp][ftrw][0][1];
						rowName = rowName.substr(rowName.length - 1, 1);						
						if (classlength < stMap[clsint][flgrp][rwgp][ftrw].length)
							classlength = stMap[clsint][flgrp][rwgp][ftrw].length;
						strSTMap += '				<tr>';
						strSTMap += '					<td align="center" valign="middle" width="12" height="12"><font	class="mandatory"><b>' + rowName + '</b></font></td>';
						for ( var ftst = 0; ftst < stMap[clsint][flgrp][rwgp][ftrw].length; ftst++) {
							seatId = stMap[clsint][flgrp][rwgp][ftrw][ftst][0];
							seatCode = stMap[clsint][flgrp][rwgp][ftrw][ftst][1];
							seatDescp = stMap[clsint][flgrp][rwgp][ftrw][ftst][3];
							seatstatus = stMap[clsint][flgrp][rwgp][ftrw][ftst][2];
							seatType = stMap[clsint][flgrp][rwgp][ftrw][ftst][6];
							logicalCCCode = stMap[clsint][flgrp][rwgp][ftrw][ftst][7];
							seatVisibility = stMap[clsint][flgrp][rwgp][ftrw][ftst][8];
							
							var rowIndex = seatCode.substr(0,seatCode.length-1); 
							if(rowStatus[rowIndex] == undefined){
								rowStatus[rowIndex] = true;	
							}
							if(seatVisibility == "N"){
								seatVisibilityTag = "style=visibility:hidden;";
							} else if(seatVisibility == "Y"){
								rowStatus[rowIndex] = false;
							}
							
							exitTable = "";
							exitMargin = -70;
							arrColname[ftst] = seatCode.substr(0, seatCode.length - 1);
							
							if ((ftrw == stMap[clsint][flgrp][rwgp].length - 1
									&& rwgp == stMap[clsint][flgrp].length - 1 && seatType == 'EXIT')
									|| (ftrw == 0 && rwgp == 0 && seatType == 'EXIT')) {

								if (ftrw == 0 && rwgp == 0)
									exitMargin = 10;
								exitTable = '<span style="position:absolute;visibility:visible;padding-top:0px;margin-left:-10px;margin-top:' + exitMargin + 'px;width:15px;"><table border="0" cellpadding="0" cellspacing="0" width="100%" height="30px" ><tr><td><img src="../../images/AircraftExit_no_cache.jpg" ></td></tr></table></span>';
							}
							if (seatstatus == 'VAC') {
								strSTMap += '	           <td width="12" height="12" '+ seatVisibilityTag +' id="'
										+ seatId
										+ '" onmouseover="rollSeatImage(this)" onmouseout="rollSeatImage(this, \'no\',\''
										+ seatId
										+ '\')"  onClick = "seatClick(\''
										+ seatId
										+ '\',\''
										+ seatCode
										+ '\',\''
										+ seatstatus
										+ '\',this)" bgColor="green" align="center"> <A HREF="javascript:void(0);"><font class="fntDefaultWhite"><div id="spn'
										+ seatId
										+ '"></div></font></A><font class="fntDefaultWhite"><div id="spn'
										+ seatId + "_"
										+ logicalCCCode
										+ '">'+logicalCCCode+'</div></font>'
										+ exitTable + '</td>';
							} else if (seatstatus == 'INA') {
								strSTMap += '	           <td width="12" height="12" '+ seatVisibilityTag +' id="'
										+ seatId
										+ '" onmouseover="rollSeatImage(this)" onmouseout="rollSeatImage(this, \'no\',\''
										+ seatId
										+ '\')"  onClick = "seatClick(\''
										+ seatId
										+ '\',\''
										+ seatCode
										+ '\',\''
										+ seatstatus
										+ '\',this)" bgColor="black" align="center"> <A HREF="javascript:void(0);"><font class="fntDefaultWhite"><div id="spn'
										+ seatId
										+ '"></div></font></A><font class="fntDefaultWhite"><div id="spn'
										+ seatId + "_"
										+ logicalCCCode
										+ '">'+logicalCCCode+'</div></font>'
										+ exitTable + '</td>';
							} else {
								strSTMap += '	           <td width="12" height="12" id="'
										+ seatId
										+ '" bgColor="gray" align="center"><font class="fntDefaultWhite"><span id="spn'
										+ seatId
										+ '"></span></font>'
										+ exitTable + '</td>';
							}
							seatVisibilityTag = "";
						}						
						strSTMap += '         	</tr>';
						if (ftrw == 0 && rwgp != 0) {
							strSTMap += '			<tr>';
							strSTMap += '				<td align="center" valign="middle" width="12" height="12"><font color="#FFFFFF">&nbsp;</font></td>';
							for ( var ftil = 0; ftil < arrColname.length; ftil++) {
								var rowNumber = arrColname[ftil];
								var isEmptyRow = rowStatus[rowNumber];
								if(isEmptyRow){
									rowNumber = '';
								}
								strSTMap += '			<td align="center" valign="middle" width="12" height="12"><font class="mandatory"><b>' + (rowNumber) + '</b></font></td>';
							}
							strSTMap += '			</tr>';
						}
					}
				}
				strSTMap += '						</table>';
				strSTMap += '						</td>';
				rowno = rowno + classlength;
			}

			strSTMap += '						</tr>';
			strSTMap += '     			</table>';
		}
		strSTMap += '					</td>';
		strSTMap += '				</tr>';
		strSTMap += '			</table>';
		strSTMap += '			</td>';
	}
	strSTMap += '</tr>';
	strSTMap += '</table>';
	DivWrite("spnSeatMap", strSTMap);
	setVisible("spnSeatMap", true);
}

function seatClick(stId, stCode, ststatus, obj1) {
	disblecontrols(false);
	if (arrSelected[stId] && arrSelected[stId][0] != 0) {
		if (arrSelected[stId][1] != "A") {
			setTDColor(obj1, "black");
		} else {
			setTDColor(obj1, "green");
		}
		arrSelected[stId][0] = 0

	} else {
		for ( var ss = 0; ss < seatCharge.length; ss++) {
			if (seatCharge[ss][1] == stId) {
				ststatus = seatCharge[ss][4];
			}
		}
		arrSelected[stId] = new Array(stId, ststatus);
		setTDColor(obj1, "red");
	}
}

function setTDColor(obj, color) {
	obj.bgColor = color;
}

function clearTdColor(strSeatID, strColor) {
	document.getElementById(strSeatID).bgColor = strColor;
}

function rollSeatImage(imgEle, instruction, strSeatID) {
	if (instruction) {
		if (!isInSelected(strSeatID)) {
			for ( var sl = 0; sl < seatCharge.length; sl++) {
				if (seatCharge[sl][1] == imgEle.id) {
					if (seatCharge[sl][4].substr(0, 1) != "V") {
						imgEle.bgColor = "black";
						break;
					} else {
						imgEle.bgColor = "green";
					}
				}
			}
		}
	} else {
		imgEle.bgColor = "red";

	}
}

function isInSelected(seatID) {
	var inSelected = false;
	var curSeatId = "";
	if (arrSelected[seatID] && arrSelected[seatID][0] != 0) {
		inSelected = true;
	}

	return inSelected;
}

function disblecontrols(cond) {
	Disable("chkStatus", cond);
	Disable("txtSeatCharge", cond);
	Disable("btnReset", cond);
	Disable("btnConfirmed", cond);
}

function intilizeSeats() {
	arrayClone(seatCharges, seatCharge);
	for ( var dl = 0; dl < seatCharge.length; dl++) {
		document.getElementById("spn" + seatCharge[dl][1]).innerHTML = seatCharge[dl][3];
	}
}

function assignStatusClick() {
	setPageEdited(true);
	var status = getText("selStatus");
	status = status.substr(0, 1);
	var seatId = "";
	var seatCode = "";

	for ( var sl = 0; sl < seatCharge.length; sl++) {
		seatId = seatCharge[sl][1];
		seatCode = seatCharge[sl][2];
		if (arrSelected[seatId] && arrSelected[seatId][0] != 0) {
			document.getElementById("spn" + seatId).innerHTML = seatCharge[sl][3];
			seatCharge[sl][4] = status;
			arrSelected[seatId][1] = status;
			if (status != "V")
				clearTdColor(seatCharge[sl][1], "black");
			else
				clearTdColor(seatCharge[sl][1], "green");
		}
	}
	arrSelected = new Array();
	Disable("btnReset", false);
	Disable("btnConfirmed", false);
}

function validateHandlingChrg() {
	setPageEdited(true);
	var strCR = getText("txtSeatCharge");
	var strLen = strCR.length;
	var blnVal = currencyValidate(strCR, 5, 2);
	var wholeNumber;
	if (!blnVal) {
		if (strCR.indexOf(".") != -1) {
			wholeNumber = strCR.substr(0, strCR.indexOf("."));
			if (wholeNumber.length > 6) {
				setField("txtSeatCharge", strCR.substr(0,
						wholeNumber.length - 1));
			} else {
				setField("txtSeatCharge", strCR.substr(0, strLen - 1));
			}
		} else {
			setField("txtSeatCharge", strCR.substr(0, strLen - 1));
		}
		getFieldByID("txtSeatCharge").focus();
	}
}

function resetClick() {
	intilizeSeats();
	inializeField();
	arrayClone(seatCharges, seatCharge);
	Disable("btnReset", true);
	Disable("btnConfirmed", true);
}

function setPageEdited(isEdited) {
	blnPageedit = isEdited;
}

function winOnLoad() {
	drawSeatMap();
	intilizeSeats();
	inializeField();
	disblecontrols(true);
}

function inializeField() {
	arrSelected = new Array();
	setField("txtSeatCharge", "0");
	setField("txtTemplate", templateName);

}

function ConfirmSeat() {	
	var soldSeats = "";
	setField("hdnData", seatCharge);
	var objSeatForm = document.getElementById("frmSeatMap");
	objSeatForm.target = "CWindow";

	objSeatForm.action = "loadSegments.action?hdnMode=EDITSAVE&templateId="
			+ templateId
			+ "&fltSegId="
			+ fltSegId
			+ "&fltId="
			+ flightId
			+ "&modelNo="
			+ opener.strModelNo;
	objSeatForm.submit();
	window.close();
	opener.ShowPopProgress();
}

function validateError(msg) {
	objMsg.MessageText = msg;
	objMsg.MessageType = "Error";
	ShowPageMessage();
}

function clearClick() {
	for ( var sl = 0; sl < arrSelected.length; sl++) {
		if (arrSelected[sl] && arrSelected[sl][0] != 0) {
			if (arrSelected[sl][1].substr(0, 1) != "V")
				clearTdColor(arrSelected[sl][0], "black");
			else
				clearTdColor(arrSelected[sl][0], "green");
		}
	}
	arrSelected = new Array();
}

function cancelClick() {
	if (blnPageedit) {
		var cnf = confirm("Changes will be lost! Do you wish to Continue?");
		if (cnf) {
			setPageEdited(false);
			window.close();
		}
	} else {
		setPageEdited(false);
		window.close();
	}
}
