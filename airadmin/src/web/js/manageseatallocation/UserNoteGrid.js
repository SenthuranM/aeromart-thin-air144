var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "10%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Date";
objCol1.headerText = "Date";
objCol1.itemAlign = "center";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "40%";
objCol2.arrayIndex = 2;
objCol2.toolTip = "User Note";
objCol2.headerText = "User Note";
objCol2.itemAlign = "center";

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "30%";
objCol3.arrayIndex = 3;
objCol3.toolTip = "User";
objCol3.headerText = "User";
objCol3.itemAlign = "center";

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "20%";
objCol4.arrayIndex = 4;
objCol4.toolTip = "Segment";
objCol4.headerText = "Segment";
objCol4.itemAlign = "center";

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "10%";
objCol5.arrayIndex = 5;
objCol5.toolTip = "Logical CC Code";
objCol5.headerText = "COS";
objCol5.itemAlign = "center";

// ---------------- Grid

var objDG = new DataGrid("spnUnGrid");
objDG.addColumn(objCol1);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);

objDG.width = "99%";
objDG.height = "380px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = arrUsernotes;
objDG.seqNo = true;
objDG.seqNoWidth = "4%";
objDG.backGroundColor = "#ECECEC";
objDG.seqStartNo = 1;
objDG.pgnumRecTotal = totalNoOfRecords; // remove as per return record size
objDG.paging = false
objDG.displayGrid();

function cancelClick() {
	window.close();
}
