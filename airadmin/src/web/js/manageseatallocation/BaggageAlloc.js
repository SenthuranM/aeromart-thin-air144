var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "35%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Segment";
objCol1.headerText = "Segment";
objCol1.itemAlign = "left";

var objCol2 = new DGColumn();
objCol2.columnType = "CUSTOM";
objCol2.width = "45%";
objCol2.arrayIndex = 3;
objCol2.toolTip = "Baggage Template";
objCol2.headerText = "Baggage Template <font class='mandatory'>*<\/font>";
objCol2.ID = 'selTemplate';
objCol2.htmlTag = "<select  id='selTemplate' name='selTemplate' style='width:200px' :CUSTOM:><option value=''></option>"
		+ strOpt + "</select>";
objCol2.onChange = "changetemplate";
objCol2.itemAlign = "center";
	
var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "35%";
objCol3.arrayIndex = 1;
objCol3.toolTip = "Segment";
objCol3.headerText = "Segment";
objCol3.itemAlign = "left";
	
var objCol4 = new DGColumn();
objCol4.columnType = "custom";
objCol4.width = "17%";
objCol4.arrayIndex = 5;
objCol4.ID = "txtInfantCapacity";
objCol4.htmlTag = "<input :CUSTOM: type='text' id='txtInfantCapacity' name='txtInfantCapacity' style='width:40px;' maxlength='3' onkeyUp='parent.capacityValidate(this)' onkeyPress='parent.capacityValidate(this)'>";
objCol4.toolTip = "Baggage Allowance Remarks";
objCol4.headerText = "Baggage Allowance Remarks";
objCol4.onChange = "changeButtonStatus";
objCol4.itemAlign = "center"


// ---------------- Grid
 

if(blnBaggage) {
	var objDG = new DataGrid("spnBaggages");
	objDG.addColumn(objCol1);
	objDG.addColumn(objCol2);
	objDG.width = "99%";
	objDG.height = "90px";
	objDG.headerBold = false;
	objDG.arrGridData = arrBaggageSegData;
	objDG.rowOver = false;
	objDG.seqNo = false;
	objDG.backGroundColor = "#ECECEC";
	objDG.displayGrid();
}

if(blnBaggageAllowance && !blnBaggage) {
	var objDG2 = new DataGrid("spnBaggageAllowance");
	objDG2.addColumn(objCol3);
	objDG2.addColumn(objCol4);
	objDG2.width = "99%";
	objDG2.height = "90px";
	objDG2.headerBold = false;
	objDG2.arrGridData = arrBaggageSegData;
	objDG2.rowOver = false;
	objDG2.seqNo = false;
	objDG2.backGroundColor = "#ECECEC";
	objDG2.displayGrid();
}


var blnPageedit = false;
var arrFlt = opener.sa;
var cabinCode = "";
var intRowNo = 0;

function setPageEdited(isEdited) {
	blnPageedit = isEdited;
}

function cancelClick() {
	if (blnPageedit) {
		var cnf = confirm("Changes will be lost! Do you wish to Continue?");
		if (cnf) {
			setPageEdited(false);
			window.close();
		}
	} else {
		setPageEdited(false);
		window.close();
	}
}

function rollClick() {
	var blnFound = false;
	var cnf = false;
	for ( var sl = 0; sl < arrBaggageSegData.length; sl++) {

		if (arrBaggageSegData[sl][3] != "") {
			blnFound = true;
			break;
		}

	}
	if (!blnFound) {
		cnf = confirm("This will Remove all the Templates from selected Flights. Do you wish to Continue ?");

	}
	if (blnFound || cnf) {
		var intLeft = (window.screen.width - 800) / 2;
		var intTop = (window.screen.height - 620) / 2;

		var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=800,height=450,resizable=no,top='
				+ intTop + ',left=' + intLeft;
		if ((opener.top[0].objMealRoll) && (!opener.top[0].objMealRoll.closed)) {
			opener.top[0].objMealRoll.close();
			opener.top[0].objMealRoll = window.open("about:blank", "CMealWin",
					strProp);
		} else {
			opener.top[0].objMealRoll = window.open("about:blank", "CMealWin",
					strProp);
		}
		var objMealAllocationForm = document.getElementById("frmBaggageAlloc");
		objMealAllocationForm.target = "CMealWin";

		objMealAllocationForm.action = "showBaggageRoll.action?fltId="
				+ strFlightId+ "&rollType=UPDATE&cos=" + strClassOfService;
		objMealAllocationForm.submit();
	}

}

function removeTemplateRollForward(){
	var cnf = false;
	
	cnf = confirm("This will Remove all the Templates from selected Flights Schedules. Do you wish to Continue ?");
	
	if(cnf){
		var intLeft = (window.screen.width - 800) / 2;
		var intTop = (window.screen.height - 620) / 2;
		
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=800,height=450,resizable=no,top='
					  + intTop + ',left=' + intLeft;
		
		if((opener.top[0].objMealRoll) && (!opener.top[0].objMealRoll.closed)){
			opener.top[0].objMealRoll.close();
			opener.top[0].objMealRoll = window.open("about:blank", "RMealRFWin", strProp);
		} else {
			opener.top[0].objMealRoll = window.open("about:blank", "RMealRFWin", strProp);
		}
		
		var objMealRmvRollForwardForm = document.getElementById("frmBaggageAlloc");
		objMealRmvRollForwardForm.target = "RMealRFWin";
		
		objMealRmvRollForwardForm.action = "showBaggageRoll.action?fltId="+ strFlightId+ "&rollType=REMOVE&cos=" + strClassOfService;
		objMealRmvRollForwardForm.submit();
		
	}
	
}

function showEdit(segId, templateId, flightId) {

	var intLeft = (window.screen.width - 800) / 2;
	var intTop = (window.screen.height - 620) / 2;
	var templName = "";
	var status = "";

	for ( var xl = 0; xl < arrTemp.length; xl++) {
		if (arrTemp[xl][0] == templateId) {
			templName = arrTemp[xl][1];
			status = arrTemp[xl][2];
			break;
		}
	}

	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=900,height=550,resizable=no,top='
			+ intTop + ',left=' + intLeft;
	if (trim(templName) != "") {
		if ((opener.top[0].objMealRoll) && (!opener.top[0].objMealRoll.closed)) {
			opener.top[0].objMealRoll.close();
			opener.top[0].objMealRoll = window.open("about:blank", "CMealWin",
					strProp);
		} else {
			opener.top[0].objMealRoll = window.open("about:blank", "CMealWin",
					strProp);
		}
		var objMealAllocationForm = document.getElementById("frmBaggageAlloc");
		objMealAllocationForm.target = "CMealWin";

		objMealAllocationForm.action = "loadMealSegments.action?hdnMode=EDIT&segId="
				+ segId
				+ "&templateId="
				+ templateId
				+ "&flightId="
				+ flightId
				+ "&templateName=" + templName;
		objMealAllocationForm.submit();
	}

}

function SaveClick() {

	if(blnBaggage) {
		var blnFound = false;
		var cnf = false;
		var status = "";
		for ( var sl = 0; sl < arrBaggageSegData.length; sl++) {
			if (arrBaggageSegData[sl][3] != "") {
				blnFound = true;
				for ( var xl = 0; xl < arrTemp.length; xl++) {
					if (arrTemp[xl][0] == arrBaggageSegData[sl][3]) {
						status = arrTemp[xl][2];
						break;
					}
				}
				if (trim(status).toUpperCase() != 'ACT') {
					alert("Please select an Active Template");
					return;
				}
				break;
			}
		}

		if (!blnFound) {
			cnf = confirm("This will Remove the Templates from the selected Flight. Do you wish to Continue ?");
		}

		if (blnFound || cnf) {
			var objMealForm = document.getElementById("frmBaggageAlloc");
			objMealForm.target = "CWindow";
			objMealForm.action = "loadBaggageSegments.action?hdnMode=SAVE&hdnData="
					+ arrBaggageSegData + "&fltId=" + strFlightId + "&modelNo="
					+ opener.strModelNo + "&ccCode=" + cabinCode + "&cos=" + strClassOfService + "&blnBag="+ blnBaggage.toString() + "&blnBgRmk="+ blnBaggageAllowance.toString();
			objMealForm.submit();
			ShowPopProgress();
		}
	}
	
	if (blnBaggageAllowance && !blnBaggage) {
		var blnValid = true;
		for ( var sl = 0; sl < arrBaggageSegData.length; sl++) {
			if (arrBaggageSegData[sl][5] != "") {
				if (!validateForNumbers(arrBaggageSegData[sl][5])) {
					blnValid = false;
					break;
				}
			}
		}
		
		if (!blnValid) {
			alert("Please enter a valid amount for baggage remarks");
		} else {
			var objMealForm = document.getElementById("frmBaggageAlloc");
			objMealForm.target = "CWindow";
			objMealForm.action = "loadBaggageSegments.action?hdnMode=SAVE&hdnData="
					+ arrBaggageSegData + "&fltId=" + strFlightId + "&modelNo="
					+ opener.strModelNo + "&ccCode=" + cabinCode + "&cos=" + strClassOfService + "&blnBag="+ blnBaggage.toString() + "&blnBgRmk="+ blnBaggageAllowance.toString();
			objMealForm.submit();
			ShowPopProgress();
		}
	}
	

}

function validateForNumbers(value) {
	var Numbers = '0123456789';
	var blnValidNumber = true;
	
	for (i=0; i < value.length; i++)
	{
		if (Numbers.indexOf(value.charAt(i),0) == -1)
		{
			blnValidNumber = false;
			break;
		}
	}
	
	return blnValidNumber;
}

function changetemplate(obj, rowNo, value) {	
	setPageEdited(true);
	/*var templateId = arrBaggageSegData[rowNo][3];
	for ( var i = 0; i < arrTemp.length; i++) {		
		if (templateId == arrTemp[i][0]) {
			cabinCode = arrTemp[i][3];			
		}
	}*/
	cabinCode = arrBaggageSegData[rowNo][4];	
	disableControls(false);	
	if (getFieldByID("btnRollForward"))
		disableRmvRollForward();

}

function changeButtonStatus(obj, rowNo, value) {
	Disable("btnSave", false);
}

function showpopMssage(msg, msgType) {
	objMsg.MessageText = msg;
	objMsg.MessageType = msgType;
	ShowPageMessage();
}

function disableControls(cond) {
	if(blnBaggage) {
		Disable("btnRollForward", !cond);
	}
	Disable("btnSave", cond);
	
}

function checkBnkTemplate(){	
	var isBlank = true;
	for(var i=0; i<arrBaggageSegData.length; i++){
		if(arrBaggageSegData[i][3] != ''){
			isBlank = false;
			break;
		}
	}	
	return isBlank;
}

function disableRmvRollForward(){
	var templatExist = checkBnkTemplate();	
	if(document.getElementById('btnSave').disabled && 
			document.getElementById('btnRollForward').disabled && templatExist) {
		Disable("btnRmvRollForward", false);	
	} else{
		Disable("btnRmvRollForward", true);
	}
}


function winOnLoad(message, msgType) {
	disableControls(true);	
	//Disable("btnRmvRollForward", (blnIsSced));
	if (message != "" && msgType != "") {
		showpopMssage(message, msgType);
	}
	if (trim(errString) != "") {
		var errFormArray = new Array();
		var arrErrStr = errString.split(",");
		var noofsegs = arrErrStr.length / 5;
		var erIndex = 0;
		for ( var el = 0; el < noofsegs; el++) {
			erIndex = 5 * el;
			errFormArray[el] = new Array();
			errFormArray[el][0] = arrErrStr[erIndex + 0];
			errFormArray[el][1] = arrErrStr[erIndex + 1];
			errFormArray[el][2] = arrErrStr[erIndex + 2];
			errFormArray[el][3] = arrErrStr[erIndex + 3];
			errFormArray[el][4] = arrErrStr[erIndex + 4];
		}
		arrayClone(errFormArray, arrBaggageSegData);
		objDG.arrGridData = arrBaggageSegData;
		objDG.refresh();
		objDG2.arrGridData = arrBaggageSegData;
		objDG2.refresh();
		disableControls(false);
	}
	if (getFieldByID("btnRollForward"))
		Disable("btnRollForward", !(blnIsSced));
	
	if (blnBaggage) {
		disableRmvRollForward();
	}
	
	if (blnBaggage) {
		document.getElementById("divBaggage").disabled = !(blnBaggage);
	} else if (blnBaggageAllowance && !blnBaggage) {
		document.getElementById("divBaggage").disabled = blnBaggage;
		document.getElementById("divBaggageAllowance").disabled = !(blnBaggageAllowance);
	}
	
	if ((message == trim("Record Successfully updated"))
			|| (message == trim("Save/Update operation is successful"))) {
		alert("Record Successfully Saved!");
	}
	HidePopProgress();
}

function ShowPopProgress() {
	setVisible("divLoadMsg", true);
}

function HidePopProgress() {
	setVisible("divLoadMsg", false);
}