var showFlightResult = true;
var showSeatMOve = true;
var selectedSegIndex = 0;
var strSelectedCOS = "";
var strFlightCCCodes = "";
var strDeletedBCInventoryIDs = "";// comma seperated fccsbInvIds corresponding
// to deleted rows
var strFlightDataRowIndex = "";
var strSelectedOversellVal = "";
var strSelectedCurtailedVal = "";
var strSelectedAllocatedVal = "";
var strChkSegGridLoaded;
var strChkBCGridLoaded;
var strChkBCFareGridLoaded;
var screenId = "SC_INVN_002";
var valueSeperator = "^";
var callFrom = "";
var blnEditSegment = false;
var blnseatMVSel = false;
var depDate = "";
var strflightId = "";
var depAirport = "";
var strModelNo = "";
var showRollFWDdAudits = true; // Roll Forward Audit
var dataObject = [];

function makeSegGridDisabled() {
	if (objSegInvDG.loaded) {
		clearInterval(strChkSegGridLoaded);
		objSegInvDG.setDisable("", "", true);
	}
}

function makeBCGridsDisabled() {
	if (objBCInvDG.loaded) {
		clearInterval(strChkBCGridLoaded);
		objBCInvDG.setDisable("", "", true);
		// calculateTotals();
		top[2].HideProgress();
	}
}

function calculateTotals() {
	var totBCAllocation = 0;
	var totFixedAllocation = 0;

	for ( var i = 0; i < ba[selectedSegIndex].length; i++) {
		totBCAllocation += Number(objBCInvDG.getCellValue(i, 5));
		if (objBCInvDG.getCellValue(i, 2) == 'Y') {
			totFixedAllocation += Number(objBCInvDG.getCellValue(i, 5));
		}
	}
	document.getElementById("idTotBC").innerHTML = totBCAllocation;
	document.getElementById("idFixAllo").innerHTML = totFixedAllocation;
}

function editSegmentAllocation() {
	blnEditSegment = true;
	objSegInvDG.setDisable(selectedSegIndex, "", false);
	Disable('btnSaveBC', false);
}

function editByBookingCode() {
	objBCInvDG.setDisable("", "", false);
	var baNew = objBCInvDG.arrGridData;
	for ( var i = 0; i < baNew.length; i++) {
		// ba[0][i][33] = bc type
		if (baNew[i][33] == 'OPENRT') { 
		// if the bc type is 'OPENRT' it is not allowed to edit coloumns
			objBCInvDG.setDisable(i, "", true);
		}
	}
	Disable('btnSaveBC', false);

	document.getElementById('checkAllClosed').disabled = false;
	document.getElementById('checkAllPriority').disabled = false;
}

function BackClicked() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		if (callFrom == 'fromAllocation') {
			screenId = "SC_INVN_001";
		} else {
			screenId = "SC_FLGT_001";
		}
		if ((top[1].objTMenu.tabIsAvailable(screenId) && top[1].objTMenu
				.tabRemove(screenId))
				|| (!top[1].objTMenu.tabIsAvailable(screenId))) {

			if (callFrom == 'fromAllocation') {
				var strSearchArr = getTabValues("SC_INVN_002", valueSeperator,
						"strFlightSearchCriteria").split("#");
				setTabValues("SC_INVN_002", valueSeperator,
						"strFlightSearchCriteria", strSearchArr[0] + "#"
								+ "SearchBack");
				setTabValues("SC_INVN_002", valueSeperator, "intLastRec", 1);
				top[1].objTMenu.tabSetTabInfo("SC_INVN_002", "SC_INVN_001",
						"showMISearch.action", "Allocate", false);
				location.replace("showMISearch.action");
			} else {
				top[1].objTMenu.tabSetTabInfo("SC_INVN_002", "SC_FLGT_001",
						"showFlight.action", "Flights", false);
				var strFlightSearch = top[1].objTMenu.tabGetValue(screenId)
						.split(valueSeperator);
				location
						.replace("showFlight.action?hdnFromPage=INVENTORY&hdnMode=SEARCH&hdnRecNo="
								+ strFlightSearch[0]
								+ "&searchCriteria="
								+ strFlightSearch[1]
								+ "&hdnFromFromPage="
								+ hdnFromFromPage);
				top[2].ShowProgress();
			}
		}
	}
}

function showFlightSearchResult() {
	if (showFlightResult) {
		objFltSearchDG.arrGridData = fa;
		objFltSearchDG.refresh();
		setVisible("spnFlightSearchResults", true);
		showFlightResult = false;
	} else {
		setVisible("spnFlightSearchResults", false);
		showFlightResult = true;
	}
}

function showSeatMovement() {
	if (!blnseatMVSel) {
		objSeatMvDG.arrGridData = sma[0];
		objSeatMvDG.refresh();
	}

	var windowDiv = getFieldByID("seatMovementInfoOptionsWindow");
	
	if (showSeatMOve) {				
		windowDiv.style.display = "block";
		document.getElementById("radAgentOptionOrigin").checked = false ;
		document.getElementById("radAgentOptionOwner").checked = false ;
		showSeatMOve = false;
		
	} else {
		setVisible("spnSeatMovementsInfo", false);
		showSeatMOve = true;
		windowDiv.style.display = "none";
	}
	
	
}

function setSeatMovmntInfoWithOriginAgent () {
	
	objSeatMvDG.arrGridData = sma[0];
	objSeatMvDG.refresh();
	setVisible("spnSeatMovementsInfo", true);
	closeSeatMvmntInfoOptionWindow();
}

function setSeatMovmntInfoWithOwnerAgent () {
	objSeatMvDG.arrGridData = smaOwnerAgetnWise[0];
	objSeatMvDG.refresh();
	setVisible("spnSeatMovementsInfo", true);
	closeSeatMvmntInfoOptionWindow();
}

function closeSeatMvmntInfoOptionWindow (){
	var windowDiv = getFieldByID("seatMovementInfoOptionsWindow");
	windowDiv.style.display = "none";	 
}

function getClassOfService(strRowNo, strCOS) {
	strFlightCCCodes = fa[strRowNo][8].split("#");
	if (strCOS == "") {
		if (strFlightCCCodes.length > 0) {
			strSelectedCOS = strFlightCCCodes[0];
		}

		// Checking whether the CC Code is default CC. then make it as defaulty
		// selected
		var defaultCCCode = fa[strRowNo][9];
		for ( var i = 0; i < strFlightCCCodes.length; i++) {
			if (strFlightCCCodes[i] == defaultCCCode) {
				strSelectedCOS = defaultCCCode;
			}
		}
	} else {
		strSelectedCOS = strCOS;
	}
	createCabinClassCombo();
}

function createCabinClassCombo() {
	var htmlCOS = "<select id='selCOS' name='selCOS' onChange='loadDataforCOS()' size='1' style='width:100px'>";
	var objCLSBombo = getFieldByID("selCOS");
	var rowAndCos = getTabValues(screenId, valueSeperator, "tempSearch").split(
			"#");

	for ( var i = 0; i < objCLSBombo.length; i++) {
		for ( var j = 0; j < strFlightCCCodes.length; j++) {
			if (strFlightCCCodes[j] == objCLSBombo.options[i].value) {
				htmlCOS += "<option value='" + objCLSBombo.options[i].value
						+ "'>" + objCLSBombo.options[i].text + "</option>";
			}
		}
	}
	htmlCOS += "</select>";
	getFieldByID("COSList").innerHTML = htmlCOS;

	if (rowAndCos[2] == "") {
		setField("selCOS", strSelectedCOS);
	} else {
		setField("selCOS", rowAndCos[2]);
	}

}

function loadDataforCOS() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		var rowAndCos = getTabValues(screenId, valueSeperator, "tempSearch")
				.split("#");
		var objCLSBombo = getFieldByID("selCOS");

		setField("hdnFlightID", fa[rowAndCos[0]][10]);
		setField("hdnClassOfService",
				objCLSBombo.options[objCLSBombo.selectedIndex].value);
		setField("hdnOlFlightId", fa[rowAndCos[0]][12]);
		setField("hdnModelNo", fa[rowAndCos[0]][11]);
		setVisible("spnFlightSearchResults", false);
		setVisible("spnSeatMovementsInfo", false);
		setTabValues(screenId, valueSeperator, "tempSearch", rowAndCos[0] + "#"
				+ 0 + "#"
				+ objCLSBombo.options[objCLSBombo.selectedIndex].value);
		strSelectedCOS = objCLSBombo.options[objCLSBombo.selectedIndex].value;
		top[1].objTMenu.tabPageEdited(screenId, false);

		var strAction = "showMIAllocation.action";
		getFieldByID("formManageSeatInventory").action = strAction;
		getFieldByID("formManageSeatInventory").target = "_self";
		getFieldByID("formManageSeatInventory").submit();
		top[2].ShowProgress();
	}
}

function fltSearhResultDGRowClick(strRowNo) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {

		setField("hdnFlightID", fa[strRowNo][10]);
		getClassOfService(strRowNo, "");
		setField("hdnClassOfService", strSelectedCOS);
		setField("hdnOlFlightId", fa[strRowNo][12]);
		setField("hdnModelNo", fa[strRowNo][11]);
		setVisible("spnFlightSearchResults", false);
		setVisible("spnSeatMovementsInfo", false);
		setTabValues(screenId, valueSeperator, "tempSearch", strRowNo + "#" + 0
				+ "#" + "");
		setTabValues(screenId, valueSeperator, "strInventoryFlightDataRow",
				fa[strRowNo][0] + "~" + fa[strRowNo][1] + "~" + fa[strRowNo][2]
						+ "~" + fa[strRowNo][3] + "~" + fa[strRowNo][4] + "~"
						+ fa[strRowNo][11] + "~" + fa[strRowNo][14]);
		top[1].objTMenu.tabPageEdited(screenId, false);

		var strAction = "showMIAllocation.action";
		getFieldByID("formManageSeatInventory").action = strAction;
		getFieldByID("formManageSeatInventory").target = "_self";
		getFieldByID("formManageSeatInventory").submit();
		top[2].ShowProgress();
	}
}

function segInvDGRowClick(strRowNo) {
	var refreshGrid = true;
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		if (top[1].objTMenu.tabGetPageEidted(screenId)) {
			objSegInvDG.setCellValue(selectedSegIndex, 3,
					strSelectedOversellVal);
			objSegInvDG.setCellValue(selectedSegIndex, 4,
					strSelectedCurtailedVal);
			objSegInvDG.setCellValue(selectedSegIndex, 5,
					strSelectedAllocatedVal);
			objSegInvDG.setDisable(selectedSegIndex, "", true);
			top[1].objTMenu.tabPageEdited(screenId, false);
		}

		// DivWrite("spnSegmentCode", sa[strRowNo][2]);
		DivWrite("spnBCSegmentCode", sa[strRowNo][2]);
		strSelectedOversellVal = sa[strRowNo][4];
		strSelectedCurtailedVal = sa[strRowNo][5];
		strSelectedAllocatedVal = sa[strRowNo][6];

		if (tempBa != null) {
			ba = cloneObj(tempBa);
			if (ba != null && ba.length > strRowNo) {
				objBCInvDG.arrGridData = ba[strRowNo];
				objBCInvDG.refresh();
			}
		}

		if ((sa != null) && (sa.length == 1)) {
			refreshGrid = false;
		}

		if (refreshGrid) {
			if (ba != null && ba.length > strRowNo) {
				objBCInvDG.arrGridData = ba[strRowNo];
				objBCInvDG.refresh();
			}
		}

		if (sma != null && sma.length > strRowNo) {
			blnseatMVSel = true;
			objSeatMvDG.arrGridData = sma[strRowNo];
			objSeatMvDG.refresh();
		}
		selectedSegIndex = strRowNo;
		strChkBCFareGridLoaded = setInterval("createFareLinks()", 30);
		objBCInvDG.setDisable("", "", true);
		var rowAndCos = getTabValues(screenId, valueSeperator, "tempSearch")
				.split("#");
		if (rowAndCos[2] != "") {
			setTabValues(screenId, valueSeperator, "tempSearch", rowAndCos[0]
					+ "#" + selectedSegIndex + "#" + rowAndCos[2]);
		}

		calculateTotals();
	}
	setTimeout('setPriorityAndClosedAll()', 1000);
}

function setPriorityAndClosedAll() {
	var closedItems = document.getElementById('frm_spnBookingCodeAllocations').contentDocument
			.getElementsByName('CHK_spnBookingCodeAllocations_0_12');
	var priorities = document.getElementById('frm_spnBookingCodeAllocations').contentDocument
			.getElementsByName('CHK_spnBookingCodeAllocations_0_4');

	var isAllCheckedPriority = true;
	var isAllCkeckedClosed = true;

	for ( var i = 0; i < closedItems.length; ++i) {
		var closedItemID = closedItems[i].id
		var checked = document.getElementById('frm_spnBookingCodeAllocations').contentDocument
				.getElementById(closedItemID).checked;
		if (!checked) {
			isAllCkeckedClosed = false;
			break;
		}
	}

	for ( var i = 0; i < priorities.length; ++i) {
		var priorityID = priorities[i].id;
		var checked = document.getElementById('frm_spnBookingCodeAllocations').contentDocument
				.getElementById(priorityID).checked;
		if (!checked) {
			isAllCheckedPriority = false;
			break;
		}
	}

	if (isAllCheckedPriority) {
		document.getElementById('checkAllPriority').checked = true;
	} else {
		document.getElementById('checkAllPriority').checked = false;
	}

	if (isAllCkeckedClosed) {
		document.getElementById('checkAllClosed').checked = true;
	} else {
		document.getElementById('checkAllClosed').checked = false;
	}
	document.getElementById('checkAllClosed').disabled = true;
	document.getElementById('checkAllPriority').disabled = true;
}

function bcInvDGRowClick(strRowNo, value) {

}

function changeOversellAdt(objCol, rowNo) {
	top[1].objTMenu.tabPageEdited(screenId, true);
	objSegInvDG.setCellValue(selectedSegIndex, 3, objCol.value);
}

function blurOversellAdt(objCol) {
	top[1].objTMenu.tabPageEdited(screenId, true);
	objSegInvDG.setCellValue(selectedSegIndex, 3, objCol.value);
}

function changeCurtaiedAdt(objCol, rowNo) {
	top[1].objTMenu.tabPageEdited(screenId, true);
	objSegInvDG.setCellValue(selectedSegIndex, 4, objCol.value);
}

function blurCurtaiedAdt(objCol) {
	objSegInvDG.setCellValue(selectedSegIndex, 4, objCol.value);
}

function changeInfantAlloc(objCol, rowNo) {
	top[1].objTMenu.tabPageEdited(screenId, true);
	objSegInvDG.setCellValue(selectedSegIndex, 5, objCol.value);
}

function blurInfantAlloc(objCol) {
	objSegInvDG.setCellValue(selectedSegIndex, 5, objCol.value);
}

function piorityOnClick(strRowNo) {
	if (ba[selectedSegIndex][strRowNo][17] != 'A') {
		ba[selectedSegIndex][strRowNo][17] = 'E';
	}

	/*
	 * if (ba[selectedSegIndex][strRowNo][4] == true){
	 * ba[selectedSegIndex][strRowNo][4] = false; }else{
	 * ba[selectedSegIndex][strRowNo][4] = true; }
	 */
	top[1].objTMenu.tabPageEdited(screenId, true);
}

function changeBcAllocation(obj, strRowNo, strColNo) {
	if (ba[selectedSegIndex][strRowNo][17] != 'A') {
		ba[selectedSegIndex][strRowNo][17] = 'E';
	}
	objBCInvDG.setCellValue(strRowNo, 5, obj.value);
	top[1].objTMenu.tabPageEdited(screenId, true);
}

function changeBcAcquired(obj, strRowNo, strColNo) {
	if (ba[selectedSegIndex][strRowNo][17] != 'A') {
		ba[selectedSegIndex][strRowNo][17] = 'E';
	}
	objBCInvDG.setCellValue(strRowNo, 9, obj.value);
	top[1].objTMenu.tabPageEdited(screenId, true);
}

function blurBcAlloc(objCol, rowNo) {
	if (ba[selectedSegIndex][rowNo][17] != 'A') {
		ba[selectedSegIndex][rowNo][17] = 'E';
	}
	objBCInvDG.setCellValue(rowNo, 5, objCol.value);
}

function blurBcAcquired(objCol, rowNo) {
	if (ba[selectedSegIndex][rowNo][17] != 'A') {
		ba[selectedSegIndex][rowNo][17] = 'E';
	}
	objBCInvDG.setCellValue(rowNo, 9, objCol.value);
}

function closedOnClick(strRowNo) {
	if (ba[selectedSegIndex][strRowNo][17] != 'A') {
		ba[selectedSegIndex][strRowNo][17] = 'E';
	}

	if (ba[selectedSegIndex][strRowNo][11] == true) {
		// ba[selectedSegIndex][strRowNo][11] = false;
		ba[selectedSegIndex][strRowNo][15] = 'M';// manually open
	} else {
		// ba[selectedSegIndex][strRowNo][11] = true;
		ba[selectedSegIndex][strRowNo][15] = 'M';// manually closed
	}

	// check the open status and keep it
	if (ba[selectedSegIndex][strRowNo][26] != ba[selectedSegIndex][strRowNo][15]) {

		if (ba[selectedSegIndex][strRowNo][11] == false
				&& ba[selectedSegIndex][strRowNo][25] == false
				&& ba[selectedSegIndex][strRowNo][26] == 'C') {
			ba[selectedSegIndex][strRowNo][15] = 'C';
		} else if (ba[selectedSegIndex][strRowNo][11] == false
				&& ba[selectedSegIndex][strRowNo][25] == false
				&& ba[selectedSegIndex][strRowNo][26] == 'R') {
			ba[selectedSegIndex][strRowNo][15] = 'R';
		} else if (ba[selectedSegIndex][strRowNo][11] == false
				&& ba[selectedSegIndex][strRowNo][25] == false
				&& ba[selectedSegIndex][strRowNo][26] == 'B') {
			ba[selectedSegIndex][strRowNo][15] = 'B';
		}
	}
	top[1].objTMenu.tabPageEdited(screenId, true);
}

function CalculateClick() {

	// Checking segment Allocation
	if (sa.length > 0) {
		var strSoldAduInf = sa[selectedSegIndex][7].split("/");
		var strSoldAdu = strSoldAduInf[0];
		var strSoldInf = strSoldAduInf[1];

		var strTotAlloc = sa[selectedSegIndex][3];

		var strOnholdAduInf = sa[selectedSegIndex][8].split("/");
		var strOnholdAdu = strOnholdAduInf[0];
		var strOnholdInf = strOnholdAduInf[1];

		var strAvaiAduInf = sa[selectedSegIndex][9].split("/");
		var strAvaiAdu = strAvaiAduInf[0];
		var strAvaiInf = strAvaiAduInf[1];

		var strActavail = Number(strTotAlloc) - Number(strSoldAdu)
				- Number(strOnholdAdu);
		var strSeats = Number(strActavail)
				+ Number(objSegInvDG.getCellValue(selectedSegIndex, 3))
				- Number(objSegInvDG.getCellValue(selectedSegIndex, 4));

		if (blnEditSegment) {

			if ((objSegInvDG.getCellValue(selectedSegIndex, 5) == '')
					|| Number(objSegInvDG.getCellValue(selectedSegIndex, 5)) < (Number(strSoldInf) + Number(strOnholdInf))) {
				showERRMessage(infantLess);
				objSegInvDG.setColumnFocus(selectedSegIndex, 5);
				return false;
			} else if (objSegInvDG.getCellValue(selectedSegIndex, 3) == '') {
				showERRMessage(oversellInvaid);
				objSegInvDG.setColumnFocus(selectedSegIndex, 3);
				return false;
			} else if (Number(strSelectedOversellVal) != Number(objSegInvDG
					.getCellValue(selectedSegIndex, 3))) {
				if (Number(strSeats) < 0) {
					showERRMessage(oversellInvaid);
					objSegInvDG.setColumnFocus(selectedSegIndex, 3);
					return false;
				}
			} else if (objSegInvDG.getCellValue(selectedSegIndex, 4) == '') {
				showERRMessage(curtaildInvaid);
				objSegInvDG.setColumnFocus(selectedSegIndex, 4);
				return false;
			} else if (Number(strSelectedCurtailedVal) != Number(objSegInvDG
					.getCellValue(selectedSegIndex, 4))) {
				if (Number(strSeats) < 0) {
					showERRMessage(curtaildInvaid);
					objSegInvDG.setColumnFocus(selectedSegIndex, 4);
					return false;
				}
			}
		}
	}

	// Checking BC Allocation
	for ( var i = 0; i < ba[selectedSegIndex].length; i++) {
		if (ba[selectedSegIndex][i][17] != 'N') {
			if (objBCInvDG.getCellValue(i, 9) != '0'
					&& objBCInvDG.getCellValue(i, 9) == '') {
				showERRMessage(acquirednull);
				objBCInvDG.setColumnFocus(i, 9);
				return false;
			} else if ((objBCInvDG.getCellValue(i, 5) == '0')
					|| (objBCInvDG.getCellValue(i, 5) == '')
					|| ((Number(objBCInvDG.getCellValue(i, 5)) + Number(objBCInvDG
							.getCellValue(i, 9))) < (Number(ba[selectedSegIndex][i][19]) + Number(ba[selectedSegIndex][i][20])))) {
				showERRMessage(bcLess);
				objBCInvDG.setColumnFocus(i, 5);
				return false;
			} else {
				objOnFocus();
			}
			if (ba[selectedSegIndex][i][17] == 'A'
					&& (Number(objBCInvDG.getCellValue(i, 9)) != 0)) {
				showERRMessage(acquiredzero);
				objBCInvDG.setColumnFocus(i, 9);
				return false;
			}
		}
	}
	return true;
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function saveInventories() {
	var segSaveDataRow = "";
	var bcSaveDataRow = "";
	var blnStatusAction = "";
	var rowAndCos = getTabValues(screenId, valueSeperator, "tempSearch").split(
			"#");

	if (CalculateClick()) {
		segSaveDataRow = sa[selectedSegIndex][12] + "~" + // fccsInvId
		sa[selectedSegIndex][10] + "~" + // flightId
		sa[selectedSegIndex][2] + "~" + // segCode
		strSelectedCOS + "~" + // cos
		sa[selectedSegIndex][4] + "~" + // oversell
		sa[selectedSegIndex][5] + "~" + // curtailed
		sa[selectedSegIndex][6] + "~" + // infantalloc
		sa[selectedSegIndex][13]; // version

		for ( var i = 0; i < ba[selectedSegIndex].length; i++) {
			if (ba[selectedSegIndex][i][17] == 'A') {
				bcSaveDataRow += ba[selectedSegIndex][i][17] + "~"; // Record
				// Added
				bcSaveDataRow += ba[selectedSegIndex][i][1] + "~"; // Bc
				bcSaveDataRow += ba[selectedSegIndex][i][4] + "~"; // Priority
				bcSaveDataRow += ba[selectedSegIndex][i][5] + "~"; // Allocation
				bcSaveDataRow += ba[selectedSegIndex][i][11] + "~"; // Status
				bcSaveDataRow += ba[selectedSegIndex][i][15] + "~"; // Status
				// Change
				// Action
				bcSaveDataRow += ba[selectedSegIndex][i][9] + "~"; // Acquired
				bcSaveDataRow += ba[selectedSegIndex][i][16] + "@"; // Record
				// Added
			} else if (ba[selectedSegIndex][i][17] == 'E') {
				bcSaveDataRow += ba[selectedSegIndex][i][14] + "~"; // ffcsbInvId
				bcSaveDataRow += ba[selectedSegIndex][i][1] + "~"; // Bc
				bcSaveDataRow += ba[selectedSegIndex][i][4] + "~"; // Priority
				bcSaveDataRow += ba[selectedSegIndex][i][5] + "~"; // Allocation
				bcSaveDataRow += ba[selectedSegIndex][i][11] + "~"; // Status

				bcSaveDataRow += ba[selectedSegIndex][i][15] + "~"; // Status
				// Change
				// Action
				bcSaveDataRow += ba[selectedSegIndex][i][9] + "~"; // Acquired
				bcSaveDataRow += ba[selectedSegIndex][i][16] + "@"; // Record
				// Added
			}
		}
		if (strDeletedBCInventoryIDs != "") {
			bcSaveDataRow += strDeletedBCInventoryIDs + "@";
		}

		setField("hdnSaveSegData", segSaveDataRow);
		setField("hdnSaveBCData", bcSaveDataRow.substr(0,
				(bcSaveDataRow.length - 1)));

		if (sa[selectedSegIndex][10] == fa[rowAndCos[0]][12]) {
			setField("hdnFlightID", sa[selectedSegIndex][10]);
			setField("hdnOlFlightId", fa[rowAndCos[0]][10]);
			setField("hdnMode", "SAVEOL");
		} else {
			setField("hdnFlightID", sa[selectedSegIndex][10]);
			setField("hdnOlFlightId", fa[rowAndCos[0]][12]);
			setField("hdnMode", "SAVE");
		}

		setField("hdnClassOfService", strSelectedCOS);
		setField("hdnModelNo", fa[rowAndCos[0]][11]);

		setField("hdnSeatFactorMin", seatFactorMin);
		setField("hdnSeatFactorMax", seatFactorMax);

		setTabValues(screenId, valueSeperator, "tempSearch", rowAndCos[0] + "#"
				+ selectedSegIndex + "#" + strSelectedCOS);
		top[1].objTMenu.tabPageEdited(screenId, false);
		getFieldByID("formManageSeatInventory").action = "showMIAllocation.action";
		getFieldByID("formManageSeatInventory").target = "_self";
		getFieldByID("formManageSeatInventory").submit();
		top[2].ShowProgress();
	}
}

function addBookingClassInvnentory() {

	var selectedBC = getFieldByID("selBookingCode").value.split(",");
	objOnFocus();
	// check if booking code already added
	var bcInvCount = ba[selectedSegIndex].length;
	for ( var i = 0; i < bcInvCount; ++i) {
		if (ba[selectedSegIndex][i][1] == selectedBC[0]) {
			showCommonError("Error", "Booking Code already exists");
			return;
		}
		if (selectedBC[0] == 'OPENRT') {
			showCommonError("Error", "Cannot add Open Return BC manually");
			return;
		}
	}
	Disable('btnSaveBC', false);
	// add row to grid
	ba[selectedSegIndex][bcInvCount] = new Array('', selectedBC[0],
			selectedBC[1], selectedBC[2], 'false', 0, 0, 0, 0, 0, 0, 'false',
			'', 0, 0, 'C', 0, 'A', '', 0, 0, '', '', '', '', 'false', '', '', 0, 0, 0, 0,
			0, 0, 0);
	top[1].objTMenu.tabPageEdited(screenId, true);
	objBCInvDG.refresh();
}

function deleteBookingClassInvnentories() {
	var selectedBookingCodes = objBCInvDG.getSelectedRows();

	if (selectedBookingCodes == null || selectedBookingCodes.length == 0) {
		// error, no bc inv selected
		showCommonError("Error",
				"Please select Booking Class Inventories to delete");
		return;
	}

	var bcInvCount = ba[selectedSegIndex].length;

	// check whether delete is allowable; sold, onhold, cancelled must be 0
	var errorMsg = "";
	var cannotDelete = false;
	for ( var i = 0; i < selectedBookingCodes.length; ++i) {
		for ( var j = 0; j < bcInvCount; ++j) {
			if (selectedBookingCodes[i] == ba[selectedSegIndex][j][1]) {
				if (ba[selectedSegIndex][j][33] == 'OPENRT') {
					errorMsg = "Cannot delete [" + ba[selectedSegIndex][j][1]
							+ "]. System generated BC.";
					cannotDelete = true;
				} else {
					if ((ba[selectedSegIndex][j][32]) != '0') {
						errorMsg = "Cannot delete ["
								+ ba[selectedSegIndex][j][1]
								+ "]. It Linked with GDS";
						cannotDelete = true;
					} else {
						errorMsg = "Cannot delete ["
								+ ba[selectedSegIndex][j][1] + "]. Has ";
						var tmpMsg = "";
						cannotDelete = false;
						if ((ba[selectedSegIndex][j][6]) > 0) {
							cannotDelete = true;
							tmpMsg += "Sold";
						}
						if ((ba[selectedSegIndex][j][7]) > 0) {
							cannotDelete = true;
							if (tmpMsg == "") {
								tmpMsg += "Onhold";
							} else {
								tmpMsg += ", Onhold";
							}
						}
						if ((ba[selectedSegIndex][j][8]) > 0) {
							cannotDelete = true;
							if (tmpMsg == "") {
								tmpMsg += "Cancelled";
							} else {
								tmpMsg += ", Cancelled";
							}
						}
						errorMsg += tmpMsg + " seats."
					}// ---
				}
				if (cannotDelete) {
					showCommonError("Error", errorMsg);
					return;
				}
			}
		}
	}

	// purge deleted rows from ba array
	var tmpSwappingArr = new Array();

	var newBCIndex = 0;
	for ( var i = 0; i < bcInvCount; ++i) {
		var bookingCode = ba[selectedSegIndex][i][1];
		var deleted = false;
		for ( var j = 0; j < selectedBookingCodes.length; ++j) {
			if (selectedBookingCodes[j] == bookingCode) {
				deleted = true;
				// keep track of deleted bc inventories
				if (ba[selectedSegIndex][i][14] != "") {// to identify those
					// records which are not
					// loaded from database
					if (strDeletedBCInventoryIDs == "") {
						strDeletedBCInventoryIDs += ba[selectedSegIndex][i][14];
					} else {
						strDeletedBCInventoryIDs += "@"
								+ ba[selectedSegIndex][i][14];
					}
				}
				break;
			}
		}

		if (!deleted) {
			tmpSwappingArr[newBCIndex] = ba[selectedSegIndex][i];
			++newBCIndex;
		}
	}

	ba[selectedSegIndex] = tmpSwappingArr;
	objBCInvDG.arrGridData = ba[selectedSegIndex];
	objBCInvDG.refresh();
	Disable('btnSaveBC', false);
}

function showSearchInventory() {
}

var objInvRollforwardWindow;
function loadRollForward() {
	if (sa == null || sa.length == 0) {
		showCommonError("Error", "No segment inventories for rollforwarding");
	}

	if ((objInvRollforwardWindow) && (!objInvRollforwardWindow.closed)) {
		objInvRollforwardWindow.close();
	}

	var intLeft = (window.screen.width - 800) / 2;
	var intTop = (window.screen.height - 620) / 2;

	var strSearch = getTabValues(screenId, valueSeperator,
			"strInventoryFlightDataRow").split("~");
	var rowAndCos = getTabValues(screenId, valueSeperator, "tempSearch").split(
			"#");
	var strFCCInventoryInfo = "";
	strFCCInventoryInfo += fa[rowAndCos[0]][10];
	strFCCInventoryInfo += "," + strSearch[0];
	strFCCInventoryInfo += "," + strSearch[1];
	strFCCInventoryInfo += "," + strSearch[3];
	strFCCInventoryInfo += "," + strSelectedCOS;
	strFCCInventoryInfo += "," + strSearch[2].substr(0, 2) + "/"
			+ strSearch[2].substr(3, 2) + "/" + strSearch[2].substr(6, 4);
	strFCCInventoryInfo += "," + strSearch[4].substr(0, 2) + "/"
			+ strSearch[4].substr(3, 2) + "/" + strSearch[4].substr(6, 4);

	strFCCInventoryInfo += "," + seatFactorMin;
	strFCCInventoryInfo += "," + seatFactorMax;

	setField("hdnFCCInventoryInfo", strFCCInventoryInfo);
	setField("hdnMode", "LOAD");

	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=800,height=600,resizable=no,top='
			+ intTop + ',left=' + intLeft;
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
	} else {
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
	}

	var objSeatAllocationForm = document
			.getElementById("formManageSeatInventory");
	objSeatAllocationForm.target = "CWindow";

	objSeatAllocationForm.action = "mIShowRollforward.action";
	objSeatAllocationForm.submit();
}

function loadSeatCharges() {

	if ((objInvRollforwardWindow) && (!objInvRollforwardWindow.closed)) {
		objInvRollforwardWindow.close();
	}
	var intLeft = (window.screen.width - 800) / 2;
	var intTop = (window.screen.height - 620) / 2;

	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=800,height=350,resizable=no,top='
			+ intTop + ',left=' + intLeft;
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
	} else {
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
	}

	var objSeatAllocationForm = document
			.getElementById("formManageSeatInventory");
	objSeatAllocationForm.target = "CWindow";

	objSeatAllocationForm.action = "loadSegments.action?fltId=" + sa[0][10]
			+ "&modelNo=" + strModelNo;
	objSeatAllocationForm.submit();
}

function CloseClicked() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		top[1].objTMenu.tabRemove(screenId);
	}
}

function createFareLinks() {
	arrTempArray = ba[selectedSegIndex];
	if (objBCInvDG.loaded) {
		clearInterval(strChkBCFareGridLoaded);
		if (arrTempArray != null && arrTempArray.length > 0) {
			var arrFares = new Array();
			arrFares.length = 0;

			for ( var rowNo = 0; rowNo < arrTempArray.length; rowNo++) {
				if (arrTempArray[rowNo][12] != ""
						&& arrTempArray[rowNo][12] != null) {
					arrFares = arrTempArray[rowNo][12].split("|");

					var link = "";
					var space = "";
					var fareCount = 0;
					for ( var n = 0; n < arrFares.length; n++) {
						var arrFare = arrFares[n].split(":");
						fareCount += arrFare[1].length;
						if (fareCount > 10) {
							space = '<BR>';
							fareCount = 0;
						} else {
							space = '&nbsp;&nbsp;';
						}
						link += "<a href = 'javascript:void(0)' onclick='parent.showFare("
								+ arrFare[0]
								+ ")'>"
								+ arrFare[1]
								+ "</a>"
								+ space;
						fareCount++;
					}
					objBCInvDG.setCellValue(rowNo, 17, link);
					link = "";
				} else {
					objBCInvDG.setCellValue(rowNo, 17, "&nbsp;");
				}
			}
		}
		objBCInvDG.setDisable("", "", true);
	}
}

function beforeUnload() {
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	}
}

function showFare(varFareId) {
	var intLeft = (window.screen.width - 800) + 60;
	var intTop = (window.screen.height - 620);
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=400,height=350,resizable=no,top='
			+ intTop + ',left=' + intLeft;
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
		top[0].objWindow = window.open('about:blank', 'Fares', strProp);
	} else {
		top[0].objWindow = window.open('about:blank', 'Fares', strProp);
	}

	getFieldByID("formManageSeatInventory").action = 'showMIFare.action?hdnMode=GRIDCLICKED&fareId='
			+ varFareId + '';
	getFieldByID("formManageSeatInventory").target = "Fares";
	getFieldByID("formManageSeatInventory").submit();
}

function onLoadSegInvDGRowClick(strRowNo) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		if (top[1].objTMenu.tabGetPageEidted(screenId)) {
			objSegInvDG.setCellValue(selectedSegIndex, 3,
					strSelectedOversellVal);
			objSegInvDG.setCellValue(selectedSegIndex, 4,
					strSelectedCurtailedVal);
			objSegInvDG.setCellValue(selectedSegIndex, 5,
					strSelectedAllocatedVal);
			objSegInvDG.setDisable(selectedSegIndex, "", true);
			top[1].objTMenu.tabPageEdited(screenId, false);
		}

		strSelectedOversellVal = sa[strRowNo][4];
		strSelectedCurtailedVal = sa[strRowNo][5];
		strSelectedAllocatedVal = sa[strRowNo][6];

		selectedSegIndex = strRowNo;
		var rowAndCos = getTabValues(screenId, valueSeperator, "tempSearch")
				.split("#");
		if (rowAndCos[2] != "") {
			setTabValues(screenId, valueSeperator, "tempSearch", rowAndCos[0]
					+ "#" + selectedSegIndex + "#" + rowAndCos[2]);
		}
	}
}

function showCarrierSummary() {
	var intHeight = 400;
	var intWidth = 600;
	var strSearchArrTem = getTabValues("SC_INVN_002", valueSeperator,
			"strFlightSearchCriteria").split("#");
	var strSearch = strSearchArrTem[0];
	var strSearchArr = strSearch.split(",");

	var timeZone = strSearchArr[strSearchArr.length - 1];

	if (timeZone == "L") {
		timeZone = "LOCAL";
	} else {
		timeZone = "ZULU";
	}

	var strFilename = "showMarketingCarrierSummary.action?flightId="
			+ strflightId + "&depDate=" + depDate + "&cabinClass="
			+ strCabinClass + "&timeZone=" + timeZone + "&depAirPort="
			+ depAirport;
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
			+ intWidth
			+ ',height='
			+ intHeight
			+ ',resizable=no,top='
			+ ((window.screen.height - intHeight) / 2)
			+ ',left='
			+ (window.screen.width - intWidth) / 2;

	objWindow1 = window.open(strFilename, "CWindow1", strProp);
}

function addUserNote() {
	var intHeight = 330;
	var intWidth = 600;
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
			+ intWidth
			+ ',height='
			+ intHeight
			+ ',resizable=no,top='
			+ ((window.screen.height - intHeight) / 2)
			+ ',left='
			+ (window.screen.width - intWidth) / 2;
	var strFilename = "showINVNotes.action?hdnMode='ADD'";
	objWindow1 = window.open(strFilename, "CWindow1", strProp);

}

function viewUserNote() {
	var intHeight = 600;
	var intWidth = 800;
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
			+ intWidth
			+ ',height='
			+ intHeight
			+ ',resizable=no,top='
			+ ((window.screen.height - intHeight) / 2)
			+ ',left='
			+ (window.screen.width - intWidth) / 2;
	var strFilename = "showINVNotes.action?hdnMode=VIEW&flightId="
			+ fa[strFlightDataRowIndex][10];
	objWindow1 = window.open(strFilename, "CWindow1", strProp);
}

function loadMealCharges() {

	if ((objInvRollforwardWindow) && (!objInvRollforwardWindow.closed)) {
		objInvRollforwardWindow.close();
	}
	var intLeft = (window.screen.width - 800) / 2;
	var intTop = (window.screen.height - 620) / 2;

	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=800,height=350,resizable=no,top='
			+ intTop + ',left=' + intLeft;
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
	} else {
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
	}

	var objSeatAllocationForm = document
			.getElementById("formManageSeatInventory");
	objSeatAllocationForm.target = "CWindow";

	objSeatAllocationForm.action = "loadMealSegments.action?fltId=" + sa[0][10]
			+ "&modelNo=" + strModelNo + "&cos=" + getField('selCOS').value;
	objSeatAllocationForm.submit();
}

function loadBaggageCharges() {

	if ((objInvRollforwardWindow) && (!objInvRollforwardWindow.closed)) {
		objInvRollforwardWindow.close();
	}
	var intLeft = (window.screen.width - 800) / 2;
	var intTop = (window.screen.height - 620) / 2;

	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=800,height=350,resizable=no,top='
			+ intTop + ',left=' + intLeft;
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
	} else {
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
	}

	var objSeatAllocationForm = document
			.getElementById("formManageSeatInventory");
	objSeatAllocationForm.target = "CWindow";

	objSeatAllocationForm.action = "loadBaggageSegments.action?fltId="
			+ sa[0][10] + "&modelNo=" + strModelNo + "&cos=" + getField('selCOS').value;
	objSeatAllocationForm.submit();
}


function loadBookingClassPrint() {	
	if ((objInvRollforwardWindow) && (!objInvRollforwardWindow.closed)) {
		objInvRollforwardWindow.close();
	}
	var intLeft = (window.screen.width - 800) / 2;
	var intTop = (window.screen.height - 620) / 2;

	var strProp = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,width=850,height=600,resizable=yes,top='
			+ intTop + ',left=' + intLeft;
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
	} else {
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
	}

	var objSeatAllocationForm = document
			.getElementById("formManageSeatInventory");
	objSeatAllocationForm.target = "CWindow";
	
	var rowAndCos = getTabValues(screenId, valueSeperator, "tempSearch").split("#");	
	objSeatAllocationForm.action = "flightBookingClassPrint.action?fltId="
			+ sa[0][10] + "&olFlightId=" + fa[rowAndCos[0]][12];
	objSeatAllocationForm.submit();
}



var tempBa = null;

function cloneObj(obj) {
	if (obj == null) {
		return null;
	}
	var clone = (obj instanceof Array) ? [] : {};
	for ( var i in obj) {
		if (typeof (obj[i]) == "object")
			clone[i] = cloneObj(obj[i]);
		else
			clone[i] = obj[i];
	}
	return clone;
}

function winOnLoad(strMsg, strMsgType) {
	// alert("varFlightSummary length " + varFlightSummary.length);
	// alert(varFlightSummary);
	if (saveSuccess != '' && saveSuccess == 1) {
		alert("Record Successfully saved!");
		saveSuccess = 0;
	}
	callFrom = getTabValues(screenId, valueSeperator, "status");
	if (getTabValues(screenId, valueSeperator, "strInventoryFlightDataRow") != ""
			&& getTabValues(screenId, valueSeperator,
					"strInventoryFlightDataRow") != null) {
		var strSearch = getTabValues(screenId, valueSeperator,
				"strInventoryFlightDataRow").split("~");
		depDate = strSearch[2];
		strflightId = strSearch[0];
		depAirport = strSearch[1];
		if (strSearch[0] == '') {
			document.getElementById("idFlightLable").innerHTML = '&nbsp;';
		} else {
			document.getElementById("idFlightLable").innerHTML = '<B>Flight No</B> : '
					+ strSearch[0];
		}
		if (strSearch[1] == '') {
			document.getElementById("idDepature").innerHTML = '&nbsp;';
		} else {
			document.getElementById("idDepature").innerHTML = '<B>Departure</B> : '
					+ strSearch[1] + ' ' + strSearch[2];
		}
		if (strSearch[3] == '') {
			document.getElementById("idArrival").innerHTML = '&nbsp;';
		} else {
			document.getElementById("idArrival").innerHTML = '<B>Arrival</B>  : '
					+ strSearch[3] + ' ' + strSearch[4];
		}
		if (strSearch[5] == '') {
			document.getElementById("idModel").innerHTML = '&nbsp;';
		} else {
			document.getElementById("idModel").innerHTML = '<B>Model</B> : '
					+ strSearch[5];
			strModelNo = strSearch[5];
		}
		document.getElementById("idCapacity").innerHTML = '<B>Default Capacity (Adult/Infant)</B> : '
				+ varFlightDefCap;
		if (strSearch[6] == '') {
			document.getElementById("idAsk").innerHTML = '&nbsp;';
		} else {
			document.getElementById("idAsk").innerHTML = "<B>ASK'000 : </B>"
					+ (Number(strSearch[6]) / 1000) + "";
		}
	}
	var rowAndCos = getTabValues(screenId, valueSeperator, "tempSearch").split(
			"#");
	if (sa != null && sa.length > 0) {
		// segInvDGRowClick(rowAndCos[1]);
		onLoadSegInvDGRowClick(rowAndCos[1]);
		DivWrite("spnBCSegmentCode", sa[0][2]);
		strChkSegGridLoaded = setInterval("makeSegGridDisabled()", 30);
		strChkBCGridLoaded = setInterval("makeBCGridsDisabled()", 30);
	} else {
		top[2].HideProgress();
	}

	if (blnSeatMap) {
		if (document.getElementById("btnSeatCharge"))
			setVisible("btnSeatCharge", blnSeatMap);
	}
	if (blnShowMeal) {
		if (document.getElementById("btnMealAllocation"))
			setVisible("btnMealAllocation", blnShowMeal);
	}
	
	if (blnShowBaggageAllowanceRemarks) {
		if (document.getElementById("btnBaggageAllocation"))
			setVisible("btnBaggageAllocation", blnShowBaggageAllowanceRemarks);
	}

	if (blnShowBaggage) {
		if (document.getElementById("btnBaggageAllocation"))
			setVisible("btnBaggageAllocation", blnShowBaggage);
	}
	
	

	setVisible("spnFlightSearchResults", false);
	setVisible("spnSeatMovementsInfo", false);
	// DivWrite("spnSegmentCode", '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
	var varFlightSummRow = varFlightSummary.split("@");
	var varFlightSummCol = "";

	for ( var i = 0; i < varFlightSummRow.length; i++) {
		fa[i] = new Array();
		varFlightSummCol = varFlightSummRow[i].split("|");
		for ( var j = 0; j < varFlightSummCol.length; j++) {
			fa[i][j] = varFlightSummCol[j];
		}
	}

	getClassOfService(rowAndCos[0], rowAndCos[2]);
	strFlightDataRowIndex = rowAndCos[0];

	setField("hdnFlightSumm", varFlightSummary);

	// Reser roll forward audits
	if(varFlightRollForwardAuditSummary !=null && varFlightRollForwardAuditSummary != ""){
		var varFlightRollForwardAuditSummRow = varFlightRollForwardAuditSummary.split("@");
		var varFlightRollForwardAuditSummCol = "";
	
		for ( var i = 0; i < varFlightRollForwardAuditSummRow.length; i++) {
			dataObject[i] = new Array();
			varFlightRollForwardAuditSummCol = varFlightRollForwardAuditSummRow[i].split("|");
			for ( var j = 0; j < varFlightRollForwardAuditSummCol.length; j++) {
				dataObject[i][j] = varFlightRollForwardAuditSummCol[j];
			}
		}
		setField("hdnFlightRollForwardAuditSummary", varFlightRollForwardAuditSummary);
	}
	if (sa != null && sa.length > 0) {

		for ( var i = 0; i < sa.length; i++) {
			if (ba[i].length > 0) {
				Disable('btnRollForward', false);
				break;
			} else {
				Disable('btnRollForward', true);
			}
		}
	}

	if (fa[rowAndCos[0]][12] != 'null') {
		// Do nothing
	} else {
		document.getElementById("idOverlap").innerHTML = '&nbsp;';
	}

	if ((fa[rowAndCos[0]][13]) == 'N') {
		Disable('btnSegEdit', true);
		Disable('btnAddBC', true);
		Disable('btnEditBC', true);
		Disable('btnDeleteBC', true);

		Disable('btnRollForward', true);
		//if (blnSeatMap)
		Disable('btnSeatCharge', true);
		//if (blnShowMeal) {
		if (document.getElementById("btnMealAllocation"))
			Disable('btnMealAllocation', true);
		//}

		//if (blnShowBaggage) {
		if (document.getElementById("btnBaggageAllocation"))
			Disable('btnBaggageAllocation', true);
		//}
	} else {
		Disable('btnSegEdit', false);
		Disable('btnAddBC', false);
		Disable('btnEditBC', false);
		Disable('btnDeleteBC', false);
		if (blnSeatMap && blnSeatMapTmlt) {
			Disable('btnSeatCharge', false);
		} else {
			Disable('btnSeatCharge', true);
		}
		if (blnShowMeal) {
			if (document.getElementById("btnMealAllocation"))
				Disable('btnMealAllocation', false);
		} else {
			if (document.getElementById("btnMealAllocation"))
				Disable('btnMealAllocation', true);
		}

		if (blnShowBaggage || blnShowBaggageAllowanceRemarks) {
			if (document.getElementById("btnBaggageAllocation"))
				Disable('btnBaggageAllocation', false);
		} else {
			if (document.getElementById("btnBaggageAllocation"))
				Disable('btnBaggageAllocation', true);
		}

	}
	// if ((fa[rowAndCos[0]][15]) == 'N') {
	// Disable('btnRollForward', true);
	// }

	top.setFID(screenId);
	top[1].objTMenu.tabPageEdited(screenId, false);
	objOnFocus();

	if (strMsg != null && strMsgType != null) {
		showCommonError(strMsgType, strMsg);
	}
	Disable('btnSaveBC', true);
	setField("hdnFromFromPage", hdnFromFromPage);

	if (ba != null) {
		tempBa = cloneObj(ba);
	}
}

// Roll Forward Audits scripts

function showRollForwardAudits() {
	if (showRollFWDdAudits) {
		// TODO uncomment following 2 lines after attaching dataObject width
		// data
		objRollFWDAuditDG.arrGridData = dataObject;
		objRollFWDAuditDG.refresh();
		setVisible("spnRollForwardAudits", true);
		showRollFWDdAudits = false;
	} else {
		setVisible("spnRollForwardAudits", false);
		showRollFWDdAudits = true;
	}
}

function rollFWDAuditRowClick(strRowNo) {
	setField("hdnFlightID", dataObject[strRowNo][0]);
	top[1].objTMenu.tabPageEdited(screenId, false);
	setField("hdnModelNo", dataObject[strRowNo][5]);
	setField("hdnClassOfService", dataObject[strRowNo][6]);
	
	setTabValues(screenId, valueSeperator, "strInventoryFlightDataRow",
			dataObject[strRowNo][1] + "~" + dataObject[strRowNo][3] + "~" + dataObject[strRowNo][2]
					+ "~" + fa[0][3] + "~" + dataObject[strRowNo][7] + "~"
					+ dataObject[strRowNo][5] + "~" + fa[0][14]);
	// Note : have used fa array information and that will not affect any issue as destination/ask 
	// will not change in the roll forward screen
	
	var strAction = "showMIAllocation.action";
	getFieldByID("formManageSeatInventory").action = strAction;
	getFieldByID("formManageSeatInventory").target = "_self";
	getFieldByID("formManageSeatInventory").submit();
	top[2].ShowProgress();
}

function winUnLoad(rollForwardAudit) {
	if (rollForwardAudit != "") {
		var varFlightSummRow = rollForwardAudit.split("@");
		var varFlightSummCol = "";
		// user will be able to roll forward operations many times with out
		// coming back to the original page. So we should not clear the list.
		var currentLength = dataObject.length;
		for ( var i = 0; i < varFlightSummRow.length; i++) {
			dataObject[i + currentLength] = new Array();
			varFlightSummCol = varFlightSummRow[i].split("|");
			for ( var j = 0; j < varFlightSummCol.length; j++) {
				dataObject[i + currentLength][j] = varFlightSummCol[j];
			}
		}
		setField("hdnFlightRollForwardAuditSummary", rollForwardAudit);
	}
}