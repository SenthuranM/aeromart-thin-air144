var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "35%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Segment";
objCol1.headerText = "Segment";
objCol1.itemAlign = "left"

var objCol2 = new DGColumn();
objCol2.columnType = "CUSTOM";
objCol2.width = "45%";
objCol2.arrayIndex = 3;
objCol2.toolTip = "Charge Template";
objCol2.headerText = "Charge Template <font class='mandatory'>*<\/font>";
objCol2.ID = 'selTemplate';
objCol2.htmlTag = "<select id='selTemplate' name='selTemplate'  style='width:200px' :CUSTOM:><option value=''></option>"
		+ strOpt + "</select>";
objCol2.onChange = "changetemplate";
objCol2.itemAlign = "center"

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "20%";
objCol3.arrayIndex = 4;
objCol3.toolTip = "Edit Template";
objCol3.headerText = "";
objCol3.itemAlign = "center"

// ---------------- Grid
var objDG = new DataGrid("spnSeats");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.width = "99%";
objDG.height = "90px";
objDG.headerBold = false;
objDG.arrGridData = arrSeatSegData;
objDG.rowOver = false;
objDG.seqNo = false;
objDG.backGroundColor = "#ECECEC";
objDG.displayGrid();

var blnPageedit = false;
var arrFlt = opener.sa;
var arrSegs = new Array();
arrayClone(arrSeatSegData, arrSegs);

function setPageEdited(isEdited) {
	blnPageedit = isEdited;
}

function cancelClick() {
	if (blnPageedit) {
		var cnf = confirm("Changes will be lost! Do you wish to Continue?");
		if (cnf) {
			setPageEdited(false);
			window.close();
		}
	} else {
		setPageEdited(false);
		window.close();
	}
}

function rollClick() {

	var blnFound = false;
	var cnf = false;
	for ( var sl = 0; sl < arrSeatSegData.length; sl++) {

		if (arrSeatSegData[sl][3] != "") {
			blnFound = true;
			break;
		}

	}
	if (!blnFound) {
		cnf = confirm("This will Remove all the Templates from selected Flights. Do you wish to Continue ?");

	}
	if (blnFound || cnf) {
		var intLeft = (window.screen.width - 800) / 2;
		var intTop = (window.screen.height - 620) / 2;

		var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=800,height=480,resizable=no,top='
				+ intTop + ',left=' + intLeft;
		if ((opener.top[0].objSeatRoll) && (!opener.top[0].objSeatRoll.closed)) {
			opener.top[0].objSeatRoll.close();
			opener.top[0].objSeatRoll = window.open("about:blank", "CSeatWin",
					strProp);
		} else {
			opener.top[0].objSeatRoll = window.open("about:blank", "CSeatWin",
					strProp);
		}
		var objSeatAllocationForm = document.getElementById("frmSeatAlloc");
		objSeatAllocationForm.target = "CSeatWin";

		objSeatAllocationForm.action = "showSeatRoll.action?fltId="
				+ strFlightId;
		objSeatAllocationForm.submit();
	}

}

function showEdit(segId, templateId, flightId) {

	var intLeft = (window.screen.width - 800) / 2;
	var intTop = (window.screen.height - 620) / 2;
	var templName = "";
	var status = "";

	for ( var xl = 0; xl < arrTemp.length; xl++) {
		if (arrTemp[xl][0] == templateId) {
			templName = arrTemp[xl][0]; //templName = arrTemp[xl][1];
			//status = arrTemp[xl][2];
			break;
		}
	}

	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=900,height=650,resizable=no,top='
			+ intTop + ',left=' + intLeft;
	if (trim(templName) != "") {
		if ((opener.top[0].objSeatRoll) && (!opener.top[0].objSeatRoll.closed)) {
			opener.top[0].objSeatRoll.close();
			opener.top[0].objSeatRoll = window.open("about:blank", "CSeatWin",
					strProp);
		} else {
			opener.top[0].objSeatRoll = window.open("about:blank", "CSeatWin",
					strProp);
		}
		var objSeatAllocationForm = document.getElementById("frmSeatAlloc");
		objSeatAllocationForm.target = "CSeatWin";

		objSeatAllocationForm.action = "loadSegments.action?hdnMode=EDIT&segId="
				+ segId
				+ "&templateId="
				+ templateId
				+ "&flightId="
				+ flightId
				+ "&templateName=" + templName;
		objSeatAllocationForm.submit();
	}

}

function SaveClick() {	
		var objSeatForm = document.getElementById("frmSeatAlloc");
		objSeatForm.target = "CWindow";
		objSeatForm.action = "loadSegments.action?hdnMode=SAVE&hdnData="
				+ arrSeatSegData + "&fltId=" + strFlightId + "&modelNo="
				+ opener.modelNo;
		objSeatForm.submit();
		ShowPopProgress();
}

function changetemplate() {
	setPageEdited(true);
	disableControls(false);

}

function showpopMssage(msg, msgType) {
	objMsg.MessageText = msg;
	objMsg.MessageType = msgType;
	ShowPageMessage();
}

function disableControls(cond) {
	Disable("btnSave", cond);
}

function winOnLoad(message, msgType) {

	disableControls(true);		
	if (message != "" && msgType != "") {
		showpopMssage(message, msgType);
	}
	if (trim(errString) != "") {
		var errFormArray = new Array();
		var arrErrStr = errString.split(",");
		var noofsegs = arrErrStr.length / 5;
		var erIndex = 0;
		for ( var el = 0; el < noofsegs; el++) {
			erIndex = 5 * el;
			errFormArray[el] = new Array();
			errFormArray[el][0] = arrErrStr[erIndex + 0];
			errFormArray[el][1] = arrErrStr[erIndex + 1];
			errFormArray[el][2] = arrErrStr[erIndex + 2];
			errFormArray[el][3] = arrErrStr[erIndex + 3];
			errFormArray[el][4] = arrErrStr[erIndex + 4];
		}
		arrayClone(errFormArray, arrSeatSegData);
		objDG.arrGridData = arrSeatSegData;
		objDG.refresh();
		disableControls(false);
	}
	if(getFieldByID("btnRollForward"))
		Disable("btnRollForward", !(blnIsSced));
	

	if ((message == trim("Record Successfully updated"))
			|| (message == trim("Save/Update operation is successful"))) {
		alert("Record Successfully Saved!");
	}
	HidePopProgress();
}

function ShowPopProgress() {
	setVisible("divLoadMsg", true);
}

function HidePopProgress() {
	setVisible("divLoadMsg", false);
}