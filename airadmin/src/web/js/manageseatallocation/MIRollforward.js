var sysdate;

var count = 1;

	function addCalendar(){
		if(count>4){
			showWindowCommonError("Error", rollForwardMorethanFiveDateRanges);
		}else{
			count++;
			$(".dateTable").append('<tr class="hideAdvance"><td width="15%"><font>From Date</font></td><td width="25%" align="left"><input type="text" onkeypress="return isValidDate(event)" id="txtFromDate_'+count+'"  style="width: 75px;" maxlength="10" name="txtFromDate_'+count+'" onBlur="dateChk(\'txtFromDate_'+count+'\')"> <a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="View Calendar"><img SRC="../../images/calendar_no_cache.gif" border="0"></a>'+'<font class="mandatory">&nbsp;*</font></td><td width="15%"><font>To Date</font></td><td width="25%" align="left">'+'<input type="text" onkeypress="return isValidDate(event)" id="txtToDate_'+count+'" style="width: 75px;" maxlength="10" name="txtToDate_'+count+'" onBlur="dateChk(\'txtToDate_'+count+'\')"> <a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="View Calendar"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory">&nbsp;*</font></td><td width="20%"><input type="button"   class="Button removeCal" value="Remove"></td></tr>');
			reIndexCalender();
		}
	}
	
	$(document).on('click', '.removeCal', function() {
		count--;
		$(this).parent().parent().remove();
		reIndexCalender();
	});
	$(document).on('click','#radBasicMode',function(){

		$(".hideAdvance").hide();
		$("#spnFlightNoList").hide();
		$("#btnLog").show();
		$("#spnFlightNo").show();
	});
	$(document).on('click','#radAdvanceMode',function(){

		$(".hideAdvance").show();
		$("#spnFlightNoList").show();
		$("#btnLog").hide();
		$("#spnFlightNo").hide();
	});

	function reIndexCalender(){

		//alert('reIndexCalender');
		var sdates = $('[id^="txtFromDate"]');
		$( '[id^="txtFromDate"]').each(function(index,sdate) {
		
		$(sdate).attr('id','txtFromDate_'+(index+1));
		$(sdate).attr('name','txtFromDate_'+(index+1));
		$($(sdate).parent().find("a")).attr('onclick','LoadCalendar('+((index+1)+'0,event); return false;'));
		});

		var edates = $('[id^="txtToDate"]');
		$( '[id^="txtToDate"]').each(function(index,edates) {
		$(edates).attr('id','txtToDate_'+(index+1));
		$(edates).attr('name','txtToDate_'+(index+1));
		$($(edates).parent().find("a")).attr('onclick','LoadCalendar('+((index+1)+'1,event); return false;'));
		});
	}
	



function winOnLoad(strMsg, strMsgType) {
	var strFCCInventoryInfo = getValue("hdnFCCInventoryInfo");

	var fccInventoryInfoArr = strFCCInventoryInfo.split(",");
	
	DivWrite("spnFlightNo", fccInventoryInfoArr[1]);
	DivWrite("spnOrigin", fccInventoryInfoArr[2]);
	DivWrite("spnDestination", fccInventoryInfoArr[3]);

	lspm.selectedData(fccInventoryInfoArr[1]);

	getFieldByID("txtFromDate_1").focus();
	
	//Logic for Advance mode pop up

	if (isAdvance == "true") {
	    var intWidth = 720;
	    var intHeight = 350; 
	    var intLeft = (window.screen.width - 1000) / 2;
	    var intTop = (window.screen.height - 400) / 2;
	    var    strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=' + intWidth + ',height=' + intHeight + ',resizable=no,top=' + intTop + ',left=' + intLeft;
		var strURL = "showFile!advanceRollForwardResult.action?savedBatchId="+batchId;
	    top[0].objCancelWindow = window.open(strURL,"advanceResultWindow", strProp);
	    $("#btnLog").hide();
	}
	

	if(isLogicalCCEnabled == "false"){
		getFieldByID("selRollForwardOptions").style.visibility = 'hidden';
	}
	
	if (reqMode == "display") {
		var depDate = fccInventoryInfoArr[5]
		var dtD = depDate.substring(0, 2);
		var dtM = depDate.substring(3, 5);
		var dtY = depDate.substring(6, 10);
		var strYear = dtD + "/" + dtM + "/" + dtY;

		var arrStDt = strYear.split("/");
		var dtStart = new Date(arrStDt[2], Number(arrStDt[1]) - 1, arrStDt[0]);
		var dtEnd = DateToString(addDays(dtStart, 1));

		setField("txtFromDate_1", dtEnd);

		if(frequncy != ""){
			chkFrequency(frequncy, reqMode);
		} 

		if (!getFieldByID("chkDeleteTargetInventories").checked) {
			Disable("chkDeleteTargetInventories", true);
			Disable("chkDelOddBCs", false);
		} else if (!getFieldByID("chkOverrideLevel").checked) {
			Disable("chkOverrideLevel", true);
			Disable("chkDelOddBCs", true);
		}
		
		setField("seatFactorMin", fccInventoryInfoArr[7]);
		setField("seatFactorMax", fccInventoryInfoArr[8]);
		
		if(fccInventoryInfoArr.length > 9){
			sysdate = fccInventoryInfoArr[9];
		}

		ls.group1 = segmentCodes;
		ls.list1 = bookingCodes;

		ls.group2 = new Array();
		ls.list2 = new Array();
	} else {
		// after rollforwarding
		var selectedDataArr = strSelectedData.split(",");
		setField("txtFromDate_1", selectedDataArr[0]);
		setField("txtToDate", selectedDataArr[1]);
		setField("radOverrideLevel", selectedDataArr[2]);

		if(selectedFrequency != ""){		
			chkFrequency(selectedFrequency, "");
		}

		if (selectedDataArr[3] == "on") {
			getFieldByID("chkDeleteTargetInventories").checked = true;
			disableOverrideLevel();
		} else {
			getFieldByID("chkOverrideLevel").checked = true;
			Disable("chkDeleteTargetInventories", true);
		}
		
		if (selectedDataArr[4] == "on") {
			getFieldByID("chkRollFwdSegAllocs").checked = true;
		}

		// prepare not selected segment BCs
		var notSelectedSegs = new Array();
		var notSelectedBCs = new Array();
		if (strNotSelectedSegmentsAndBCs != null
				&& strNotSelectedSegmentsAndBCs != "") {

			var notSelectedSegBCsArr = strNotSelectedSegmentsAndBCs.split('|');
			for ( var x = 0; x < notSelectedSegBCsArr.length; ++x) {
				var segments = notSelectedSegBCsArr[x].split(':');
				notSelectedSegs[x] = segments[0];
				notSelectedBCs[x] = new Array();
				var bcs = segments[1].split(',');
				for (y = 0; y < bcs.length; ++y) {
					notSelectedBCs[x][y] = new Array();
					notSelectedBCs[x][y][0] = bcs[y];
					notSelectedBCs[x][y][1] = bcs[y];
				}
			}
		}

		// prepare selected segment BCs
		var selectedSegs = new Array();
		var selectedBCs = new Array();
		if (strSelectedSegmentsAndBCs != null
				&& strSelectedSegmentsAndBCs != "") {
			var selectedSegBCsArr = strSelectedSegmentsAndBCs.split('|');
			for ( var x = 0; x < selectedSegBCsArr.length; ++x) {
				var segments = selectedSegBCsArr[x].split(':');
				selectedSegs[x] = segments[0];
				selectedBCs[x] = new Array();
				var bcs = segments[1].split(',');
				for (y = 0; y < bcs.length; ++y) {
					selectedBCs[x][y] = new Array();
					selectedBCs[x][y][0] = bcs[y];
					selectedBCs[x][y][1] = bcs[y];
				}
			}
		}

		ls.group1 = notSelectedSegs;
		ls.list1 = notSelectedBCs;

		ls.group2 = selectedSegs;
		ls.list2 = selectedBCs;
	}

	// create and display the list boxes
	ls.width = "150px";
	ls.height = "100px";
	ls.drawListBox();

	// HideProgress();
	if (strMsg != null && strMsgType != null) {
		showWindowCommonError(strMsgType, strMsg);
		createLog();
		createLogData();
	}
 
}

function createLogData() {
	if(strLog != ""){
		DivWrite("spnPopupData",strLog);
	}else {
		DivWrite("spnPopupData", "Log is empty");
	}
}

function viewLog(){
	showPopUp(1);
}

function validateRollForward() {

	var selecteddays = [];

	for(var txtDateIndex=1;txtDateIndex<=count;txtDateIndex++){

		if (getValue("txtFromDate_"+txtDateIndex) == null || getValue("txtFromDate_"+txtDateIndex) == "") {
			showWindowCommonError("Error", rollForwardFromDateRqrd);
			return;
		}

		if (getValue("txtToDate_"+txtDateIndex) == null || getValue("txtToDate_"+txtDateIndex) == "") {
			showWindowCommonError("Error", rollForwardToDateRqrd);
			return;
		}
		if(sysdate != undefined && sysdate != "" && !CheckDates(sysdate,getValue("txtFromDate_"+txtDateIndex))){
			showWindowCommonError("Error", rollForwardFromDateLessthanCurrentDate);
			return;
		}
	
		var toDateString = getValue("txtToDate_"+txtDateIndex);
		var fromDateString = getValue("txtFromDate_"+txtDateIndex);

		var toDateStrArr = toDateString.split('/');
		var fromDateSrtArr = fromDateString.split('/');
	
		var toDate = new Date(toDateStrArr[2],Number(toDateStrArr[1])-1,toDateStrArr[0]);
		var fromDate = new Date(fromDateSrtArr[2],Number(fromDateSrtArr[1])-1,fromDateSrtArr[0]);
		
		if (toDate < fromDate) {
			showWindowCommonError("Error", rollForwardFromDateLessthanFromDate);
			return;
		}
		//Checking date range validation
		for (var d = fromDate; d <= toDate; d.setDate(d.getDate() + 1)) {
			if(($.inArray(fromDate.getTime(), selecteddays) > -1) || ($.inArray(toDate.getTime(), selecteddays) > -1)){
				showWindowCommonError("Error", rollForwardFromDateToDateOverlapping);
				return;
			}else{
   				selecteddays.push(new Date(d).getTime());
			}
		}

	}

	if (!getFieldByID("chkSunday").checked
			&& !getFieldByID("chkMonday").checked
			&& !getFieldByID("chkTuesday").checked
			&& !getFieldByID("chkWednesday").checked
			&& !getFieldByID("chkThursday").checked
			&& !getFieldByID("chkFriday").checked
			&& !getFieldByID("chkSaturday").checked) {
		showWindowCommonError("Error", rollForwardFrequencyRqrd);
		return;
	}
	
	var varOverSell = getValue("radRfO");
	var varCurtail = getValue("radRfC");
	var varWaitlisting = getValue("radRfW");
	
	if (ls.getSelectedDataWithGroup() == "" && varOverSell == "0" && varCurtail == "0" && varWaitlisting == "0") {
		showWindowCommonError("Error", rollForwardSegmentRqrd);
		return;
	}
	
	
	
	var isValidSeatFactor = seatFactorValidate();
	if(isValidSeatFactor==false){
		return false;
	}

	return true;
}

// seat factor validation
function seatFactorValidate(){
	var seatFactorMin = getFieldByID("seatFactorMin").value;
	var seatFactorMax = getFieldByID("seatFactorMax").value;
	
	if(seatFactorMin == "" && seatFactorMax == ""){
		return true;
	}else if(seatFactorMin != "" && seatFactorMax == ""){
		if(parseInt(seatFactorMin,10) < 0){				
			showWindowCommonError("Error", rollForwardSFMin);
			getFieldByID("seatFactorMin").focus();
			return false;
		}
	}else if(seatFactorMin == "" && seatFactorMax != ""){
		if(parseInt(seatFactorMax,10) < 0){				
			showWindowCommonError("Error", rollForwardSFMax);
			getFieldByID("seatFactorMax").focus();
			return false;
		}
	}else if(seatFactorMin != "" && seatFactorMax != ""){
		if(parseInt(seatFactorMin,10) < 0){				
			showWindowCommonError("Error", rollForwardSFMin);
			getFieldByID("seatFactorMin").focus();
			return false;
		}
		if(parseInt(seatFactorMax,10) < 0){				
			showWindowCommonError("Error", rollForwardSFMax);
			getFieldByID("seatFactorMax").focus();
			return false;
		}
		if(parseInt(seatFactorMin,10) > parseInt(seatFactorMax,10) ){				
			showWindowCommonError("Error", rollForwardSFMinMax);
			getFieldByID("seatFactorMin").focus();
			return false;
		}
	}
		
	return true;
}

function VaidateSeatFactor(){
	
	var seatFactorMin = getFieldByID("seatFactorMin").value;
	var seatFactorMax = getFieldByID("seatFactorMax").value;
	if(seatFactorMin!= ""){
		var strCC = seatFactorMin;
		var strLen = strCC.length;
		var blnVal = isPositiveInt(strCC)
		if (!blnVal) {
			setField("seatFactorMin", strCC.substr(0, strLen - 1));
			getFieldByID("seatFactorMin").focus();
		}
	}
	
	if(seatFactorMax!= ""){
		var strCC = seatFactorMax;
		var strLen = strCC.length;
		var blnVal = isPositiveInt(strCC)
		if (!blnVal) {
			setField("seatFactorMax", strCC.substr(0, strLen - 1));
			getFieldByID("seatFactorMax").focus();
		}
	}
}

function onClickDeleteTargetInventories(CheckBox) {
	if (!CheckBox.checked) {
		Disable("chkOverrideLevel", false);
		Disable("radOverrideLevel", false);
		Disable("chkDeleteTargetInventories", true);
		Disable("chkDelOddBCs", false);
		getFieldByID("chkOverrideLevel").checked = true;
		setField("radOverrideLevel", "None");
	}
}

function onClickOverrideLevel(CheckBox) {
	if (!CheckBox.checked) {
		Disable("chkDeleteTargetInventories", false);
		getFieldByID("chkDeleteTargetInventories").checked = true;
		getFieldByID("chkDelOddBCs").checked = false; 
		disableOverrideLevel();
	}
}

function onClickRemoveOddBC(CheckBox){
	if (CheckBox.checked) {
		getFieldByID("chkDeleteTargetInventories").checked = false;
		Disable("chkDeleteTargetInventories", true);
		Disable("chkOverrideLevel", false);
		Disable("radOverrideLevel", false);
		getFieldByID("chkOverrideLevel").checked = true;
		setField("radOverrideLevel", "None");
	}else{
		Disable("chkOverrideLevel", false);
		Disable("radOverrideLevel", false);
		Disable("chkDeleteTargetInventories", true);
		 
		getFieldByID("chkOverrideLevel").checked = true;
		setField("radOverrideLevel", "None");
	}
}

function disableOverrideLevel() {
	getFieldByID("chkOverrideLevel").checked = false;
	Disable("chkOverrideLevel", true);
	Disable("radOverrideLevel", true);
	setField("radOverrideLevel", "");
}

function setDate(strDate, strID) {
	
	switch (strID) {
		case "10":
			setField("txtFromDate_1", strDate);
			break;
		case "11":
			setField("txtToDate_1", strDate);
			break;
		case "20":
			setField("txtFromDate_2", strDate);
			break;
		case "21":
			setField("txtToDate_2", strDate);
			break;
		case "30":
			setField("txtFromDate_3", strDate);
			break;
		case "31":
			setField("txtToDate_3", strDate);
			break;
		case "40":
			setField("txtFromDate_4", strDate);
			break;
		case "41":
			setField("txtToDate_4", strDate);
			break;
		case "50":
			setField("txtFromDate_5", strDate);
			break;
		case "51":
			setField("txtToDate_5", strDate);
			break;
		case "0":
			setField("txtFromDate_1", strDate);
			break;
		case "1":
			setField("txtToDate_1", strDate);
			break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 10;
	if (strID == "0") {
		objCal1.left = 0;
	} else {
		objCal1.left = 210;
	}
	objCal1.showCalendar(objEvent);
	objCal1.onClick = "setDate";
}

function isValidDate(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 47 || charCode > 57)) {
        return false;
    }
    return true;
}

function cbkBCA_Click() {
	var objC = getFieldByID("chkBCodeAllocation");
	setVisible("spnBKA", false);
	if (objC.checked) {
		setVisible("spnBKA", true);
	}
}

function saveRollForward() {

	if (!validateRollForward())
		return;

	setField("hdnMode", "SAVE");
	var fccInventoryInfoArr = getValue("hdnFCCInventoryInfo").split(",");

	setField("hdnFlightIDRF", fccInventoryInfoArr[0]);
	setField("hdnFlightNumber", fccInventoryInfoArr[1]);
	setField("hdnOrigin", fccInventoryInfoArr[2]);
	setField("hdnDestination", fccInventoryInfoArr[3]);
	setField("hdnCabinClassCode", fccInventoryInfoArr[4]);

	// ls.disable(false);
	// setField("hdnAssignedSegments",ls.getselectedData());
	setField("hdnSelectedSegsAndBCs", ls.getSelectedDataWithGroup());
	setField("hdnNotSelectedSegsAndBCs", ls.getNotSelectedDataWithGroup());

	setField("hdnSelectedFlights", lspm.getSelectedDataWithGroup());
	setField("hdnNotSelectedFlights", lspm.getNotSelectedDataWithGroup());
	
	// setVisible("spnRollFwdStatusReport", false);

	showWindowCommonError("success", "Roll forwarding in progress...");
	getFieldByID("formRollAllocation").action = "mIShowRollforward.action";
	getFieldByID("formRollAllocation").submit();

	// ShowProgress();
}

function chkFrequency(strWeek, mode) {
	var arrFrequncyWeek = strWeek.split(",");

	for ( var i = 0; i < arrFrequncyWeek.length; i++) {
		if (arrFrequncyWeek[i] == "Sun") {
			getFieldByID("chkSunday").checked = true;
		}
		if (arrFrequncyWeek[i] == "Mon") {
			getFieldByID("chkMonday").checked = true;
		}
		if (arrFrequncyWeek[i] == "Tue") {
			getFieldByID("chkTuesday").checked = true;
		}
		if (arrFrequncyWeek[i] == "Wed") {
			getFieldByID("chkWednesday").checked = true;
		}
		if (arrFrequncyWeek[i] == "Thu") {
			getFieldByID("chkThursday").checked = true;
		}
		if (arrFrequncyWeek[i] == "Fri") {
			getFieldByID("chkFriday").checked = true;
		}
		if (arrFrequncyWeek[i] == "Sat") {
			getFieldByID("chkSaturday").checked = true;
		}
	}

	if (mode == "display") {
		if (!getFieldByID("chkSunday").checked) {
			Disable("chkSunday", true);
		}
		if (!getFieldByID("chkMonday").checked) {
			Disable("chkMonday", true);
		}
		if (!getFieldByID("chkTuesday").checked) {
			Disable("chkTuesday", true);
		}
		if (!getFieldByID("chkWednesday").checked) {
			Disable("chkWednesday", true);
		}
		if (!getFieldByID("chkThursday").checked) {
			Disable("chkThursday", true);
		}
		if (!getFieldByID("chkFriday").checked) {
			Disable("chkFriday", true);
		}
		if (!getFieldByID("chkSaturday").checked) {
			Disable("chkSaturday", true);
		}
	}
	
	if (privOverrideFrequency!= undefined 
			&& privOverrideFrequency!= null && privOverrideFrequency=='true') {
				
		Disable("chkSunday", false);
		Disable("chkMonday", false);
		Disable("chkTuesday", false);
		Disable("chkWednesday", false);
		Disable("chkThursday", false);
		Disable("chkFriday", false);
		Disable("chkSaturday", false);
	}
}