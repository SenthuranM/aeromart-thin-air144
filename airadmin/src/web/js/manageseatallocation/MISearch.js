var screenId = "SC_INVN_001";
var valueSeperator = "^";

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "6%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Flight No";
objCol1.headerText = "Flight<br>No";
objCol1.itemAlign = "left"
objCol1.sort = "true";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "5%";
objCol2.arrayIndex = 2;
objCol2.toolTip = "Departure";
objCol2.headerText = "Dep.";
objCol2.itemAlign = "Center"

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "12%";
objCol3.arrayIndex = 3;
objCol3.toolTip = "Departure Date & Time";
objCol3.headerText = "Departure <br>Date & Time";
objCol3.itemAlign = "center"

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "5%";
objCol4.arrayIndex = 4;
objCol4.toolTip = "Arrival";
objCol4.headerText = "Arr.";
objCol4.itemAlign = "Center"

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "12%";
objCol5.arrayIndex = 5;
objCol5.toolTip = "Arrival Date & Time";
objCol5.headerText = "Arrival <br>Date & Time";
objCol5.itemAlign = "center"

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "8%";
objCol6.arrayIndex = 6;
objCol6.toolTip = "Via Points";
objCol6.headerText = "Via Points";
objCol6.itemAlign = "Center"

var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "7%";
objCol7.arrayIndex = 7;
objCol7.toolTip = "Aircraft Model";
objCol7.headerText = "Aircraft Model";
objCol7.itemAlign = "left"

var objCol8 = new DGColumn();
objCol8.columnType = "label";
objCol8.width = "10%";
objCol8.arrayIndex = 8;
objCol8.toolTip = "Allocated Adults/ Infants (F/J/Y)";
objCol8.headerText = "Allocated Adults/ Infants<br>(F/J/Y)";
objCol8.itemAlign = "right"

var objCol9 = new DGColumn();
objCol9.columnType = "label";
objCol9.width = "10%";
objCol9.arrayIndex = 9;
objCol9.toolTip = "Seats Sold Adult/ Infant(F/J/Y)";
objCol9.headerText = "Seats Sold <br>Adult/ Infant<br>(F/J/Y)";
objCol9.itemAlign = "right"

var objCol10 = new DGColumn();
objCol10.columnType = "label";
objCol10.width = "10%";
objCol10.arrayIndex = 10;
objCol10.toolTip = "Seats on Hold Adult/ Infant(F/J/Y)";
objCol10.headerText = "Seats on Hold Adult/ Infant<br>(F/J/Y)";
objCol10.itemAlign = "right"

var objCol12 = new DGColumn();
objCol12.columnType = "label";
objCol12.width = "6%";
objCol12.arrayIndex = 17;
objCol12.toolTip = "Flight Status";
objCol12.headerText = "Flight Status";
objCol12.itemAlign = "Center"
	
var objCol13= new DGColumn();
objCol13.columnType = "label";
objCol13.width = "3%";
objCol13.arrayIndex = 23;
objCol13.toolTip = "Seat Factor";
objCol13.headerText = "%";
objCol13.itemAlign = "right";


// ---------------- Grid
var objDG = new DataGrid("spnSearchFlights");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol12);
objDG.addColumn(objCol6);
objDG.addColumn(objCol7);
objDG.addColumn(objCol8);
objDG.addColumn(objCol9);
objDG.addColumn(objCol10);
objDG.addColumn(objCol13);

objDG.width = "99%";
objDG.height = "275px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = f;
objDG.seqNo = true;
objDG.seqStartNo = getTabValues(screenId, valueSeperator, "intLastRec");
objDG.pgnumRecTotal = totalRecords;
objDG.paging = true;
objDG.pgnumRecPage = 20;
objDG.pgonClick = "gridNavigations";
objDG.rowClick = "RowClick";
objDG.displayGrid();

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtDepartureDateFrom", strDate);
		break;
	case "1":
		setField("txtDepartureDateTo", strDate);
		break;
	case "3":
		frames["frm_DE"].setDate(strDate, strID);
		break;
	case "4":
		frames["frm_DE"].setDate(strDate, strID);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 0;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}


function loadSeatFactorSelection(){
	$("#txtMinSeatFactor").val(0);
	$("#txtMaxSeatFactor").val(100);
}

loadSeatFactorSelection();

function seatFactorMinOnChng() {
}


function seatFactorMaxOnChng() {
}


function KPValidateDecimel(objCon, s, f) {
	 
	var strText = objCon.value;
	var length = strText.length;
	var blnVal = isLikeDecimal(strText);
	var blnVal2 = currencyValidate(strText, s, f);
	var blnVal3 = isDecimal(strText);

	if (!blnVal) {
		customValidate(objCon, /[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/=:,;_^~]/g);
	} else if (!blnVal2) {
		customValidate(objCon, /[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/=:,;_^~]/g);
		objCon.focus();
	}
	if (!blnVal2) {
		if (strText.indexOf(".") != -1) {
			wholeNumber = strText.substr(0, strText.indexOf("."));
			if (wholeNumber.length > 13) {
				objCon.value= strText.substr(0, wholeNumber.length - 1);
			} else {
				objCon.value= strText.substr(0, length - 1);
			}
		} else {
			objCon.value= strText.substr(0, length - 1);
		}
		
	}
}