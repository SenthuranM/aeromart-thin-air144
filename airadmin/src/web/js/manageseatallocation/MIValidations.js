var intLastRec;
var strRowData;
var strGridRow;
var strFlightRow;
var strFlightSumm;
var screenId = "SC_INVN_001";
var valueSeperator = "^";
var objProgressCheck = "";
var isPostBacked = false;

// On Page loading make sure that all the page input fields are cleared
function winOnLoad(strMsg, strMsgType) {
	if (top[1].objTMenu.focusTab == screenId) {
		getFieldByID("txtDepartureDateFrom").focus();
	}

	if (strMsg != null && strMsgType != null)
		showCommonError(strMsgType, strMsg);
	
	setDefaultTimeMode();
	
	if (getTabValues(screenId, valueSeperator, "strFlightSearchCriteria") != ""
			&& getTabValues(screenId, valueSeperator, "strFlightSearchCriteria") != null) {
		strFlightSumm = "";

		for ( var i = 0; i < f.length; i++) {
			strFlightRow = f[i];
			strFlightSumm += strFlightRow[1] + "|"; // 00FlightNumber
			strFlightSumm += strFlightRow[2] + "|"; // 1 Departure
			strFlightSumm += strFlightRow[3] + "|"; // 2 DepartureDateTime
			strFlightSumm += strFlightRow[4] + "|"; // 3 Arrival
			strFlightSumm += strFlightRow[5] + "|"; // 4 ArrivalDateTime
			strFlightSumm += strFlightRow[6] + "|"; // 5 ViaPoints
			strFlightSumm += strFlightRow[17] + "|"; // 6 FlightStatus
			if (strFlightRow[19] == 'null') { // 7
				strFlightSumm += 'N' + "|";
			} else {
				strFlightSumm += 'Y' + "|";
			}
			strFlightSumm += strFlightRow[13] + "|"; // 8 CabinClassCodes
			strFlightSumm += strFlightRow[18] + "|"; // 9 DefaultCabinClass
			strFlightSumm += strFlightRow[11] + "|"; // 10 FlightId
			strFlightSumm += strFlightRow[7] + "|"; // 11 ModelNumber
			strFlightSumm += strFlightRow[19] + "|"; // 12
														// OverlappingFlightId
			strFlightSumm += strFlightRow[20] + "|"; // 13
			strFlightSumm += strFlightRow[21] + "|"; // 14 Ask Value
			strFlightSumm += strFlightRow[22] + "|"; // 15 Has schedule Id
														// Y/N
			strFlightSumm += "@";
		}
		setField("hdnFlightSumm", strFlightSumm);

		var strSearchArr = getTabValues(screenId, valueSeperator,
				"strFlightSearchCriteria").split("#");
		var strSearch = strSearchArr[0].split(",");
		setFieldValues("txtDepartureDateFrom", strSearch[0]);
		setFieldValues("txtDepartureDateTo", strSearch[1]);
		setFieldValues("txtFlightNumber", strSearch[2]);
		setFieldValues("selFltStatus", strSearch[3]);
		setFieldValues("selDeparture", strSearch[4]);
		setFieldValues("selArrival", strSearch[5]);
		setFieldValues("selCabinClass", strSearch[6]);
		setFieldValues("selVia1", strSearch[7]);
		setFieldValues("selVia2", strSearch[8]);
		setFieldValues("selVia3", strSearch[9]);
		setFieldValues("selVia4", strSearch[10]);
		setFieldValues("radTime", strSearch[11]);
		var minSFC = strSearch[12];
		var maxSFC = strSearch[13];
		setFieldValues("txtMinSeatFactor", minSFC);
		setFieldValues("txtMaxSeatFactor", maxSFC);
		//var values = $( "#seatFactor" ).slider( "option", "values" );
		//$("#seatFactor").slider("values", 0, minSFC);
		//$("#seatFactor").slider("values", 1, maxSFC);

		if (strSearchArr[1] == 'SearchBack') // Not required since flights
												// are available in second
												// screen
		{
			var strSearchArr = getTabValues(screenId, valueSeperator,
					"strFlightSearchCriteria").split("#");
			setTabValues(screenId, valueSeperator, "strFlightSearchCriteria",
					strSearchArr[0] + "#" + "");

			// document.forms[0].hdnMode.value="SEARCH";
			// document.forms[0].submit();
			// top[2].ShowProgress();
		} else {
			objProgressCheck = setInterval("ClearProgressbar()", 300);
		}
	} else {
		objProgressCheck = setInterval("ClearProgressbar()", 300);
	}
		
}

var setDefaultTimeMode = function() {
	if(isTimeInZulu){
		document.formSearchSeatInventory.radTime[1].checked = true;
	}else{
		document.formSearchSeatInventory.radTime[0].checked = true;
	}
}


function ClearProgressbar() {
	if (objDG.loaded) {
		clearTimeout(objProgressCheck);
		top[2].HideProgress();
	}
}

function changeCase(varFlightNo) {
	setField("txtFlightNumber", varFlightNo.value.toUpperCase());
}

function clearInventory() {
	setField("txtDepartureDateFrom", "");
	setField("txtDepartureDateTo", "");
	setField("txtFlightNumber", "");
	setField("selDeparture", "-1");
	setField("selFltStatus", "-1");
	setField("selArrival", "-1");
	setField("selVia1", "-1");
	setField("selVia2", "-1");
	setField("selVia3", "-1");
	setField("selVia4", "-1");
	setField("selCabinClass", "-1");
	setField("radTime", "Z");

	getFieldByID("txtDepartureDateFrom").focus();
}

function setFieldValues(strFieldName, strValue) {
	setField(strFieldName, strValue);
}

function validateSearchSeatInventory() {
	if (validateTextField(getValue("txtDepartureDateFrom"),
			departureDateFromRqrd)) {
		getFieldByID("txtDepartureDateFrom").focus();
		return;
	}
	if (validateTextField(getValue("txtDepartureDateTo"), departureDateToRqrd)) {
		getFieldByID("txtDepartureDateTo").focus();
		return;
	}
	if (!dateValidDate(getValue("txtDepartureDateTo"))) {
		showCommonError("Error", departureDateToRqrd);
		setField("txtDepartureDateTo", "");
		getFieldByID("txtDepartureDateTo").focus();
		return;
	}

	var strFromDate = document.getElementById("txtDepartureDateTo").value;
	var strToDate = document.getElementById("txtDepartureDateFrom").value;
	var isToDateLessthanFromDate = CheckDates(strFromDate, strToDate);

	if (!(strFromDate == strToDate)) {
		if (isToDateLessthanFromDate) {
			showCommonError("Error", departureDateToBnsRleVltd);
			return;
		}
	}

	if (document.getElementById("selDeparture").value != "-1"
			&& document.getElementById("selArrival").value != "-1") {
		if (document.getElementById("selDeparture").value == document
				.getElementById("selArrival").value) {
			showCommonError("Error", departureAndArrivalBnsRleVltd);
			return;
		}
	}

	var strDeparture = document.getElementById("selDeparture").value;
	var strArrival = document.getElementById("selArrival").value;

	var strVia1 = document.getElementById("selVia1").value;
	var strVia2 = document.getElementById("selVia2").value;
	var strVia3 = document.getElementById("selVia3").value;
	var strVia4 = document.getElementById("selVia4").value;

	var arrSegments = new Array();
	arrSegments[0] = strDeparture;
	arrSegments[1] = strArrival;
	arrSegments[2] = strVia1;
	arrSegments[3] = strVia2;
	arrSegments[4] = strVia3;
	arrSegments[5] = strVia4;

	var blnDuplicate = false;
	for ( var i = 0; i < arrSegments.length; i++) {
		if (arrSegments[i] != "-1") {
			for ( var m = i; m < arrSegments.length; m++) {
				if (arrSegments[m] != "-1") {
					if (arrSegments[m] == arrSegments[i]) {
						if (m != i) {
							blnDuplicate = true;
							break;
						}
					}
				}
			}
		}
		if (blnDuplicate) {
			break;
		}
	}

	if (blnDuplicate) {
		showCommonError("Error", viapointsinvalid);
		return;
	}

	var schcarrier = trim(getVal("txtFlightNumber")).substr(0, 2);
	var blnValid = false;
	for ( var cl = 0; cl < arrCarriers.length; cl++) {
		if (arrCarriers[cl] == schcarrier) {
			blnValid = true;
			break;
		}
	}
	if (schcarrier != "" && !blnValid) {
		showERRMessage(invalidcarrier);
		getFieldByID("txtFlightNumber").focus();
		return false;
	}
	return true;
}

// on Grid Row click
function RowClick(strRowNo) {
	if ((top[1].objTMenu.tabIsAvailable("SC_INVN_002") && top[1].objTMenu
			.tabRemove("SC_INVN_002"))
			|| (!top[1].objTMenu.tabIsAvailable("SC_INVN_002"))) {

		top.strLastPage = "";
		setTabValues(screenId, valueSeperator, "tempSearch", strRowNo + "#" + 0
				+ "#" + "");
		setTabValues(screenId, valueSeperator, "status", "fromAllocation");
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;
		setTabValues(screenId, valueSeperator, "strInventoryFlightDataRow",
				f[strRowNo][1] + "~" + f[strRowNo][2] + "~" + f[strRowNo][3]
						+ "~" + f[strRowNo][4] + "~" + f[strRowNo][5] + "~"
						+ f[strRowNo][7] + "~" + f[strRowNo][21]);

		var strCCCodes = f[strRowNo][13].split("#");
		var strCode = "";
		if (strCCCodes.length > 0) {
			strCode = strCCCodes[0];
		}

		// Checking whether the CC Code is default CC. then make it as defaulty
		// selected
		var defaultCCCode = f[strRowNo][18];
		for ( var i = 0; i < strCCCodes.length; i++) {
			if (strCCCodes[i] == defaultCCCode) {
				strCode = defaultCCCode;
			}
		}

		setField("hdnFlightID", f[strRowNo][11]);
		setField("hdnClassOfService", strCode);
		setField("hdnOlFlightId", f[strRowNo][19]);
		setField("hdnModelNo", f[strRowNo][7]);
				
		setField("hdnSeatFactorMin", getFieldByID("txtMinSeatFactor").value);
		setField("hdnSeatFactorMax", getFieldByID("txtMaxSeatFactor").value);

		var strAction = "showMIAllocation.action";
		top[1].objTMenu.tabSetTabInfo("SC_INVN_001", "SC_INVN_002",
				"showMIAllocation.action", "Allocate", false);
		getFieldByID("formSearchSeatInventory").action = strAction;
		getFieldByID("formSearchSeatInventory").submit();
		top[2].ShowProgress();
	}
}

function viewAllocationDetails(strRowNo) {
	RowClick(strRowNo);
}

function searchInventory() {
	if (!validateSearchSeatInventory())
		return;

	var strTempFrom = getValue("txtDepartureDateFrom");
	var strTempTo = getValue("txtDepartureDateTo");
	var strTempFlightNumber = getValue("txtFlightNumber");
	var strTempFltStatus = getValue("selFltStatus");
	var strTempDeparture = getValue("selDeparture");
	var strTempArrival = getValue("selArrival");
	var strTempCabinClass = getValue("selCabinClass");
	var strTempVia1 = getValue("selVia1");
	var strTempVia2 = getValue("selVia2");
	var strTempVia3 = getValue("selVia3");
	var strTempVia4 = getValue("selVia4");
	var strTempTime = getValue("radTime");
	var strMinSeatFactor = getFieldByID("txtMinSeatFactor").value;
	var strMaxSeatFactor = getFieldByID("txtMaxSeatFactor").value;
    
	var strFlightSearchCriteriaTemp = strTempFrom + "," + strTempTo + ","
			+ strTempFlightNumber + "," + strTempFltStatus + ","
			+ strTempDeparture + "," + strTempArrival + "," + strTempCabinClass
			+ "," + strTempVia1 + "," + strTempVia2 + "," + strTempVia3 + ","
			+ strTempVia4 + "," + strTempTime + "," + strMinSeatFactor+ "," + strMaxSeatFactor;

	setTabValues(screenId, valueSeperator, "strFlightSearchCriteria",
			strFlightSearchCriteriaTemp + "#" + "");
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
	document.forms[0].hdnMode.value = "SEARCH";
	document.forms[0].submit();
	top[2].ShowProgress();
}

// to show error messages
function myRecFunction(intRecNo, intErrMsg) {
	top[2].HidePageMessage();
	if (intErrMsg != "") {
		top[2].objMsg.MessageText = intErrMsg;
		top[2].objMsg.MessageType = "Error";
		top[2].ShowPageMessage();
	}
}

// To Handle Paging
function gridNavigations(intRecNo, intErrMsg) {
	setField("hdnMode", "SEARCH");
	objOnFocus();
	setTabValues(screenId, valueSeperator, "intLastRec", intRecNo);

	setField("hdnRecNo", getTabValues(screenId, valueSeperator, "intLastRec"));
	if (intErrMsg != "") {
		top[2].objMsg.MessageText = intErrMsg;
		top[2].objMsg.MessageType = "Error";
		top[2].ShowPageMessage();
	} else {
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function KPValidate(control) {
	var strCC = getText(control);
	var strLen = strCC.length;
	var blnVal = isAlpha(strCC)
	if (blnVal) {
		setField(control, strCC.substr(0, strLen - 1));
		getFieldByID(control).focus();
	}
}
