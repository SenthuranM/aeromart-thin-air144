var blnPageedit = false;

function setPageEdited(isEdited) {
	blnPageedit = isEdited;
}
function validateTA(objControl, length) {
	var strValue = objControl.value;
	var strLength = strValue.length;
	var blnVal = isEmpty(strValue);
	if (blnVal) {
		setField("txtUsetNotes", strValue.substr(0, strLength - 1));
		getFieldByID("txtUsetNotes").focus();
	}
	if (strLength > length) {
		strValue = strValue.substr(0, length);
		objControl.value = strValue;
	}
}
function clickChange() {
	setPageEdited(true);
}

function addClick() {
	var note = getFieldByID("txtUsetNotes").value;
	var patt = /['|"`~]+/g;
	note = note.replace(patt,'');
	patt = /(?:\r\n|\r|\n)/g; // pattern to get the newline of the text
	note = note.replace(patt, ' '); // replace newline with a white space
	if (trim(note) == "") {
		validateError("User Note cannot be Blank")
		getFieldByID("txtUsetNotes").focus();
		return;
	}
	setField("hdnMode", "SAVE");
	setField("txtUsetNotes",note);
	setField("hdnflightSegId", opener.manageInvantory.selectedFlightSegInvID);
	document.frminvADUN.submit();
}

function cancelClick() {
	if (blnPageedit) {
		var cnf = confirm("Changes will be lost! Do you wish to Continue?");
		if (cnf) {
			setPageEdited(false);
			window.close();
		}
	} else {
		setPageEdited(false);
		window.close();
	}
}

function validateError(msg) {
	objMsg.MessageText = msg;
	objMsg.MessageType = "Error";
	ShowPageMessage();
}

function winOnLoad(strMsg, strMsgType) {
	getFieldByID("txtUsetNotes").focus();
	if (strMsg != null && strMsg != "" && strMsgType != null
			&& strMsgType != "") {
		objMsg.MessageText = strMsg;
		objMsg.MessageType = strMsgType;
		ShowPageMessage();
	}
	if (strMsg == trim("Record Successfully Saved!")) {
		alert("User Note Successfully added!");
	}
}