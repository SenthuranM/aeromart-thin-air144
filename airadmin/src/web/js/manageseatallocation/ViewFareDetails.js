function window_onload() {
	getFieldByID("idFareRuleId").innerHTML = (fareDetails[0] == 'null') ? ' '
			: fareDetails[0];
	getFieldByID("idEffectiveFrom").innerHTML = (fareDetails[1] == 'null') ? ' '
			: fareDetails[1];
	getFieldByID("idEffectiveTo").innerHTML = (fareDetails[2] == 'null') ? ' '
			: fareDetails[2];
	getFieldByID("idFareBasisCode").innerHTML = (fareDetails[3] == 'null') ? ' '
			: fareDetails[3];
	getFieldByID("idFareRuleDescription").innerHTML = (fareDetails[4] == 'null') ? ' '
			: fareDetails[4];
	getFieldByID("idRefundable").innerHTML = (fareDetails[5] == 'null') ? ' '
			: fareDetails[5];
	getFieldByID("idReturn").innerHTML = (fareDetails[6] == 'Y') ? 'Yes' : 'No';
	getFieldByID("idAdvanceBookingDays").innerHTML = (fareDetails[7] == 'null') ? ' '
			: fareDetails[7];
	getFieldByID("idFareId").innerHTML = (fareDetails[8] == 'null') ? ' '
			: fareDetails[8];
	getFieldByID("idAFareRuleCode").innerHTML = (fareDetails[9] == 'null') ? ' '
			: fareDetails[9];
	getFieldByID("idAgentCodes").innerHTML = (fareDetails[10] == 'null') ? ' '
			: fareDetails[10];
	getFieldByID("idVisibleChannels").innerHTML = (fareDetails[11] == 'null') ? ' '
			: fareDetails[11];
}