function winOnLoad(strMsg, strMsgType) {

	DivWrite("spnFlightNo", flightData[0]);
	DivWrite("spnOrigin", flightData[1]);
	DivWrite("spnDestination", flightData[2]);

	getFieldByID("txtFromDate").focus();

	if (reqMode == "display") {

		var depDate = flightData[3]
		var dtD = depDate.substring(0, 2);
		var dtM = depDate.substring(3, 5);
		var dtY = depDate.substring(6, 10);
		var strYear = dtD + "/" + dtM + "/" + dtY;

		var arrStDt = strYear.split("/");
		var dtStart = new Date(arrStDt[2], Number(arrStDt[1]) - 1, arrStDt[0]);
		var dtEnd = DateToString(addDays(dtStart, 1));

		setField("txtFromDate", dtEnd);
		chkFrequency(frequncy, reqMode);
		ls.group1 = segmentCodes;
		ls.list1 = new Array();
		ls.group2 = new Array();
		ls.list2 = new Array();
		ls.width = "150px";
		ls.height = "100px";
		ls.drawListBox();
	} else {
		// after rollforwarding
		var selectedDataArr = strSelectedData.split(",");
		setField("txtFromDate", selectedDataArr[0]);
		setField("txtToDate", selectedDataArr[1]);

		chkFrequency(selectedFrequency, "");

		// prepare not selected segment
		var notSelectedSegs = new Array();

		if (strNotSelectedSegmentsAndBCs != null
				&& strNotSelectedSegmentsAndBCs != "") {

			var notSelectedSegBCsArr = strNotSelectedSegmentsAndBCs.split(',');
			for ( var x = 0; x < notSelectedSegBCsArr.length; ++x) {
				notSelectedSegs[x] = notSelectedSegBCsArr[0];
			}
		}
		var notSelectedlen = notSelectedSegs.length;

		// prepare selected segment
		var selectedSegs = new Array();
		if (strSelectedSegmentsAndBCs != null
				&& strSelectedSegmentsAndBCs != "") {
			var selectedSegBCsArr = strSelectedSegmentsAndBCs.split(',');
			for ( var x = 0; x < selectedSegBCsArr.length; ++x) {
				selectedSegs[x] = selectedSegBCsArr[0];
				notSelectedSegs[notSelectedlen + x] = selectedSegBCsArr[0];
			}
		}

		ls.group1 = notSelectedSegs;
		ls.list1 = new Array();
		ls.group2 = new Array();
		ls.list2 = new Array();
		ls.width = "150px";
		ls.height = "100px";
		ls.drawListBox();
		ls.selectedData(strSelectedSegmentsAndBCs);
	}

	// create and display the list boxes

	// HideProgress();
	if (strMsg != null && strMsgType != null) {
		
		//Enable disabled save button on load
		Disable("btnSave", false);
		showWindowCommonError(strMsgType, strMsg);
	}

}

function validateRollForward() {
	if (getValue("txtFromDate") == null || getValue("txtFromDate") == "") {
		showWindowCommonError("Error", rollForwardFromDateRqrd);
		return;
	}

	if (getValue("txtToDate") == null || getValue("txtToDate") == "") {
		showWindowCommonError("Error", rollForwardToDateRqrd);
		return;
	}

	if (getValue("txtFromDate") == null || getValue("txtFromDate") == "") {
		showWindowCommonError("Error", rollForwardFromDateLessthanCurrentDate);
		return;
	}

	if (getValue("txtToDate") < getValue("txtToDate")) {
		showWindowCommonError("Error", rollForwardFromDateLessthanFromDate);
		return;
	}

	if (!getFieldByID("chkSunday").checked
			&& !getFieldByID("chkMonday").checked
			&& !getFieldByID("chkTuesday").checked
			&& !getFieldByID("chkWednesday").checked
			&& !getFieldByID("chkThursday").checked
			&& !getFieldByID("chkFriday").checked
			&& !getFieldByID("chkSaturday").checked) {
		showWindowCommonError("Error", rollForwardFrequencyRqrd);
		return;
	}

	if (ls.getSelectedDataWithGroup() == "") {
		showWindowCommonError("Error", rollForwardSegmentRqrd);
		return;
	}
	return true;
}

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 10;
	if (strID == "0") {
		objCal1.left = 0;
	} else {
		objCal1.left = 210;
	}
	objCal1.showCalendar(objEvent);
	objCal1.onClick = "setDate";
}

function saveRollForward() {

	if (!validateRollForward())
		return;

	//Disable save button until rollforward is done
	Disable("btnSave", true);
	setField("hdnMode", "SAVE");
	setField("hdnFlightIDRF", flightData[4]);
	setField("hdnFlightNumber", flightData[0]);
	setField("hdnOrigin", flightData[1]);
	setField("hdnDestination", flightData[2]);

	setField("hdnSelectedSegs", ls.getSelectedDataWithGroup());
	setField("hdnNotSelectedSegs", ls.getNotSelectedDataWithGroup());

	showWindowCommonError("Warning", "Roll forwarding in progress...");
	getFieldByID("formSeatRoll").action = "showSeatRoll.action?fltId="
			+ flightData[4]+"&modelNo="+opener.opener.modelNo;
	getFieldByID("formSeatRoll").submit();

}

function chkFrequency(strWeek, mode) {
	var arrFrequncyWeek = strWeek.split(",");

	for ( var i = 0; i < arrFrequncyWeek.length; i++) {
		if (arrFrequncyWeek[i] == "Sun") {
			getFieldByID("chkSunday").checked = true;
		}
		if (arrFrequncyWeek[i] == "Mon") {
			getFieldByID("chkMonday").checked = true;
		}
		if (arrFrequncyWeek[i] == "Tue") {
			getFieldByID("chkTuesday").checked = true;
		}
		if (arrFrequncyWeek[i] == "Wed") {
			getFieldByID("chkWednesday").checked = true;
		}
		if (arrFrequncyWeek[i] == "Thu") {
			getFieldByID("chkThursday").checked = true;
		}
		if (arrFrequncyWeek[i] == "Fri") {
			getFieldByID("chkFriday").checked = true;
		}
		if (arrFrequncyWeek[i] == "Sat") {
			getFieldByID("chkSaturday").checked = true;
		}
	}

	if (mode == "display") {
		if (!getFieldByID("chkSunday").checked) {
			Disable("chkSunday", true);
		}
		if (!getFieldByID("chkMonday").checked) {
			Disable("chkMonday", true);
		}
		if (!getFieldByID("chkTuesday").checked) {
			Disable("chkTuesday", true);
		}
		if (!getFieldByID("chkWednesday").checked) {
			Disable("chkWednesday", true);
		}
		if (!getFieldByID("chkThursday").checked) {
			Disable("chkThursday", true);
		}
		if (!getFieldByID("chkFriday").checked) {
			Disable("chkFriday", true);
		}
		if (!getFieldByID("chkSaturday").checked) {
			Disable("chkSaturday", true);
		}
	}
	
	if (privOverrideFrequency!= undefined 
			&& privOverrideFrequency!= null && privOverrideFrequency=='true') {
				
		Disable("chkSunday", false);
		Disable("chkMonday", false);
		Disable("chkTuesday", false);
		Disable("chkWednesday", false);
		Disable("chkThursday", false);
		Disable("chkFriday", false);
		Disable("chkSaturday", false);
	}
}