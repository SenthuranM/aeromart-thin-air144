//On Page loading make sure that all the page input fields are cleared
var screenId = "SC_ADMN_004";
var valueSeperator = "~";

var rowNo;
var strRowData;
var isLoadCal = "NO";
var isChangedDST = "NO";
var btnSeq = "btnAdd,btnEdit,btnDelete";

function winOnLoad(strMsg, strMsgType) {
	if (saveSuccess == 5) {
		alert("Record Successfully saved, Alerts and Emails sent");
		isChangedDST = "YES";
		saveSuccess = 0;
	} else if (saveSuccess == 4) {
		alert("Record Successfully saved and Emails sent");
		isChangedDST = "YES";
		saveSuccess = 0;
	} else if (saveSuccess == 3) {
		alert("Record Successfully saved and Alerts sent");
		isChangedDST = "YES";
		saveSuccess = 0;
	} else if (saveSuccess == 1) {
		alert("Record Successfully saved!");
		isChangedDST = "YES";
		saveSuccess = 0;
	}
	clearAirportDst();
	disableInputControls();
	opener.setPageEdited(false, true);
	buttonSetFocus("btnAdd", btnSeq);
	if (strMsg != null && strMsgType != null)
		showWindowCommonError(strMsgType, strMsg);

	Disable("btnEdit", true);
	Disable("btnDisableRollback", true);
	Disable("txtEmailID", true);
	setVisible("divEmailAddr", false);
	setVisible("spnNotifyNote", false);
	
	if(sendEmailForPax == "Y"){
		Disable("chkEmailPax", false);
	} else {
		Disable("chkEmailPax", true);
	}
	
	if(sendSMSForPax == "Y"){
		Disable("chkSmsPax", false);
	} else {
		Disable("chkSmsPax", true);
	}
	
	Disable("chkSmsCNF", true);
	Disable("chkSmsOnhold", true);

	var values = opener.getTabValues(screenId, valueSeperator,
			"airportDstCompination");
	values = values.split(",");
	var airportCode = values[0];
	var gmtOffsetTime = values[1];

	document.getElementById("spnDSTHeader").innerHTML = airportCode
			+ " GMTOffset " + gmtOffsetTime;

	setVisible("spnDSTHeader", true);
	getFieldByID("chkEmails").checked = false;

	if (arrFormData != null && arrFormData.length > 0) {

		opener.setPageEdited(true, true);
		enableInputControls();
		isLoadCal = "YES";
		setVisible("spnNotifyNote", true);
		setField("txtStart", arrFormData[0][1]);
		setField("txtStartTime", arrFormData[0][2]);
		setField("txtEnd", arrFormData[0][3]);
		setField("txtEndTime", arrFormData[0][4]);
		setField("txtAdjTime", arrFormData[0][5]);
		setField("selOperator", arrFormData[0][6]);
		setField("txtEmailID", arrFormData[0][8]);
		if (arrFormData[0][7] == "on" || arrFormData[0][7] == "ON")
			getFieldByID("chkEmails").checked = true;
		else
			getFieldByID("chkEmails").checked = false;

		if (arrFormData[0][9] == "on" || arrFormData[0][9] == "ON")
			getFieldByID("chkAlerts").checked = true;
		else
			getFieldByID("chkAlerts").checked = false;
		if (arrFormData[0][10] == "Active" || arrFormData[0][10] == "active")
			getFieldByID("chkStatus").checked = true;
		else
			getFieldByID("chkStatus").checked = false;

		if (isAddMode) {
			setField("hdnMode", "ADD");
			getFieldByID("txtStart").focus();
			// getFieldByID("chkStatus").checked = "true";
		} else {
			setField("hdnMode", "EDIT");
			// If date less or equal to current date, Disable Start date/time in
			// edit mode
			if (checkValidStartDate(getValue("txtStart"))) {
				Disable("txtStart", false);
				Disable("txtStartTime", false);
				getFieldByID("txtStart").focus();
			} else {
				Disable("txtStart", true);
				Disable("txtStartTime", true);
				getFieldByID("selOperator").focus();
			}
			Disable("btnEdit", false);
			// getFieldByID("selOperator").focus();
			setField("hdnVersion", prevVersion);
			setField("hdnDSTId", prevDSTId);
			setField("hdnGridRow", prevGridRow);
			strGridRow = prevGridRow;
		}
	}

}

function checkValidStartDate(strDate) {
	if (strDate != null && strDate != "") {
		try {
			var checkDate = formatDate(strDate, "dd/mm/yyyy", "dd-mmm-yyyy");
			var today = formatDate(getVal("hdnCurrentDate"), "dd/mm/yyyy",
					"dd-mmm-yyyy");
			return compareDates(checkDate, today, '>');
		} catch (ex) {
			return false;
		}
	} else {
		return false;
	}
}

function validateAirportDst() {
	if (getValue("hdnMode") != "EDIT") {
		// Validate Start Date only in Add mode
		if (getValue("txtStart") == null || getValue("txtStart") == "") {
			showWindowCommonError("Error", airportDstStartDateRqrd);
			getFieldByID("txtStart").focus();
			return false;
		}
		if (getValue("txtStart") != null || getValue("txtStart") != "") {
			var status = dateValidDate(getValue("txtStart"));
			if (!status) {
				showWindowCommonError("Error", airportDstStartDateFormatInvalid);
				getFieldByID("txtStart").focus();
				return false;
			}
		}
	}
	// compare start date and stop date
	var StartD = formatDate(getVal("txtStart"), "dd/mm/yyyy", "dd-mmm-yyyy");
	var EndD = formatDate(getVal("txtEnd"), "dd/mm/yyyy", "dd-mmm-yyyy");
	var Today = formatDate(getVal("hdnCurrentDate"), "dd/mm/yyyy",
			"dd-mmm-yyyy");

	if (getValue("hdnMode") != "EDIT") {
		// Validate Start Date only in Add mode
		if (compareDates(Today, StartD, '>')) {
			showWindowCommonError("Error", airportDstStartLessthanCurrentDate);
			getFieldByID("txtStart").focus();
			return false;
		}

		if (getValue("txtStartTime") == null || getValue("txtStartTime") == "") {
			showWindowCommonError("Error", airportDstStartTimeRqrd);
			getFieldByID("txtStartTime").focus();
			return false;
		}

		if (checkStartTime()) {
			showWindowCommonError("Error", airportDstStartTimeInvalid);
			getFieldByID("txtStartTime").focus();
			return false;
		}
	}
	if (getValue("txtAdjTime") == null || getValue("txtAdjTime") == "") {
		showWindowCommonError("Error", airportDstTimeRqrd);
		getFieldByID("txtAdjTime").focus();
		return false;
	}
	if (checkAdjTime()) {
		showWindowCommonError("Error", airportDstTimeInvalid);
		getFieldByID("txtAdjTime").focus();
		return false;
	}
	if (getValue("txtEnd") == null || getValue("txtEnd") == "") {
		showWindowCommonError("Error", airportDstEndDateRqrd);
		getFieldByID("txtEnd").focus();
		return false;
	}
	if (getValue("txtEnd") != null || getValue("txtEnd") != "") {
		var status = dateValidDate(getValue("txtEnd"));
		if (!status) {
			showWindowCommonError("Error", airportDstEndDateFormatInvalid);
			getFieldByID("txtEnd").focus();
			return false;
		}
	}

	if (compareDates(StartD, EndD, '>')) {
		showWindowCommonError("Error", airportDstEndLessthanStartDate);
		getFieldByID("txtEnd").focus();
		return false;
	}

	if (getValue("txtEndTime") == null || getValue("txtEndTime") == "") {
		showWindowCommonError("Error", airportDstEndTimeRqrd);
		getFieldByID("txtEndTime").focus();
		return false;
	}
	if (checkEndTime()) {
		showWindowCommonError("Error", airportDstEndTimeInvalid);
		getFieldByID("txtEndTime").focus();
		return false;
	}
	if (StartD == EndD) {
		if ((getValue("txtEndTime").replace(":", ".")) <= (getValue("txtStartTime")
				.replace(":", "."))) {
			showWindowCommonError("Error", airportDstEndTimelessForSameDay);
			getFieldByID("txtEndTime").focus();
			return false;
		}

	}
	if(validateAdjTime(getValue('txtAdjTime'))){
			showWindowCommonError("Error", airportAdjTimeInvalid);
			getFieldByID("txtAdjTime").focus();
			return false;			
	}
	if (getFieldByID("chkEmails").checked == true) {
		if ((getValue("txtEmailID") == null) || (getValue("txtEmailID") == "")) {
			showWindowCommonError("Error", airportDstEmailIdRqrd);
			getFieldByID("txtEmailID").focus();
			return false;
		} else {
			if (!checkEmail(getValue("txtEmailID"))) {
				showWindowCommonError("Error", airportDstEmailIdInvalid);
				getFieldByID("txtEmailID").focus();
				return false;
			}
		}
	}
	return true;
}


function clearAirportDst() {
	setField("hdnVersion", "");
	setField("txtStart", "");
	setField("txtStartTime", "");
	setField("txtEnd", "");
	setField("txtEndTime", "");
	setField("txtAdjTime", "");
	setField("txtEmailID", "");
	getFieldByID("chkEmails").checked = false;
	setField("selOperator", "+");

	getField("txtStart").focus();
}

function disableInputControls() {
	Disable("txtStart", true);
	Disable("txtEnd", true);
	Disable("txtStartTime", true);
	Disable("txtEndTime", true);
	Disable("txtAdjTime", true);
	Disable("selOperator", true);
	Disable("chkStatus", true);
	Disable("btnSave", true);
	Disable("btnReset", true);

}

function enableInputControls() {
	Disable("txtStart", false);
	Disable("txtEnd", false);
	Disable("txtStartTime", false);
	Disable("txtEndTime", false);
	Disable("txtStartTime", false);
	Disable("txtEndTime", false);
	Disable("selOperator", false);
	Disable("txtAdjTime", false);
	Disable("btnSave", false);
	Disable("btnReset", false);

}

// on Grid Row click
function RowClick(strRowNo) {
	objOnFocus();
	if (chkChanges()) {

		rowNo = strRowNo;
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;
		setField("hdnGridRow", strRowNo);
		setField("hdnDSTId", arrData[strRowNo][9]);
		var arrStartDate = strRowData[0].split(" ");
		var arrEndDate = strRowData[1].split(" ");
		setField("txtStart", arrStartDate[0]);
		setField("txtEnd", arrEndDate[0]);
		setField("txtStartTime", arrStartDate[1]);
		setField("txtEndTime", arrEndDate[1]);
		document.getElementById("btnSave").value = "Save";
		setField("txtEmailID", "");
		getFieldByID("chkEmails").checked = false;
		// getFieldByID("chkAlerts").checked = false;
		isLoadCal = "NO";
		if ((arrData[strRowNo][7]) > 0) {
			setField("selOperator", "+");
			setField("txtAdjTime", arrData[strRowNo][3].replace("+", ""));
		} else {
			setField("selOperator", "-");
			setField("txtAdjTime", arrData[strRowNo][3].replace("-", ""));
		}

		setField("hdnVersion", arrData[strRowNo][5]);
		if (arrData[strRowNo][6] == "Active") {
			getFieldByID("chkStatus").checked = true;
		} else {
			getFieldByID("chkStatus").checked = false;
			Disable("btnEdit", true);
		}
		// Enable Rollback button if Start Date > System Date and Status =
		// Active
		var StartD = formatDate(arrStartDate[0], "dd/mm/yyyy", "dd-mmm-yyyy");
		var EndD = formatDate(arrEndDate[0], "dd/mm/yyyy", "dd-mmm-yyyy");
		var Today = formatDate(getVal("hdnCurrentDate"), "dd/mm/yyyy",
				"dd-mmm-yyyy");
		if ((compareDates(Today, StartD, '<'))
				&& (arrData[strRowNo][6] == "Active")) {
			Disable("btnDisableRollback", false);
		} else
			Disable("btnDisableRollback", true);

		// Enable Edit button if End Date > System Date
		if (compareDates(Today, EndD, '>')
				|| (arrData[strRowNo][6] == "In-Active")) {
			Disable("btnEdit", true);
		} else {
			Disable("btnEdit", false);
		}
		Disable("btnAdd", false);
		opener.setPageEdited(false, true);
		disableInputControls();
		setVisible("spnNotifyNote", false);
	}
}

function saveAirportDst() {
	if (!validateAirportDst())
		return;
	enableInputControls();
	var strCountry = getFieldByID("selCountry").value;
	var strAirportCode = opener.document.getElementById("txtAirportCode").value;
	Disable("chkStatus", false);
	Disable("btnSave", true);
	Disable("btnReset", true);	
	isChangedDST = "YES";
	document.forms[0].action = "showAirportDST.action?txtAirportCode="
			+ strAirportCode + "&strCountry=" + strCountry;
	document.forms[0].submit();
	Disable("chkStatus", false);
}

function resetAirportDst() {
	opener.setPageEdited(false, true);
	setVisible("spnNotifyNote", true);
	var strMode = getText("hdnMode");
	document.getElementById("btnSave").value = "Save";
	setField("txtEmailID", "");
	getFieldByID("chkEmails").checked = false;
	getFieldByID("chkAlerts").checked = true;
	getFieldByID("chkEmailPax").checked = false;
	getFieldByID("chkSmsPax").checked = false;
	clickSmsPax(getFieldByID("chkSmsPax"));
	setVisible("divEmailAddr", false);
	isLoadCal = "YES";
	if (strMode == "ADD") {
		clearAirportDst();
		getField("txtStart").focus();
		getFieldByID("chkStatus").checked = "true";
		setField("selOperator", "+");
	}
	if (strMode == "EDIT") {

		if (arrData[strGridRow][6] != "Active") {
			clearAirportDst();
			disableInputControls();
			opener.setPageEdited(false, true);

			buttonSetFocus("btnAdd", btnSeq);

			Disable("btnEdit", true);
			Disable("btnDisableRollback", true);
			Disable("txtEmailID", true);
			setVisible("spnNotifyNote", false);

			setVisible("spnDSTHeader", true);
			getFieldByID("chkEmails").checked = false;
		} else {

			enableInputControls();

			// If date less or equal to current date, Disable Start date/time in
			// edit mode
			if (checkValidStartDate(getValue("txtStart"))) {
				Disable("txtStart", false);
				Disable("txtStartTime", false);
				getFieldByID("txtStart").focus();
			} else {
				Disable("txtStart", true);
				Disable("txtStartTime", true);
				getFieldByID("selOperator").focus();
			}

			var arrStartDate = arrData[strGridRow][1].split(" ");
			var arrEndDate = arrData[strGridRow][2].split(" ");
			setField("txtStart", arrStartDate[0]);
			setField("txtEnd", arrEndDate[0]);
			setField("txtStartTime", arrStartDate[1]);
			setField("txtEndTime", arrEndDate[1]);

			if ((arrData[strGridRow][7]) > 0) {
				setField("selOperator", "+");
				setField("txtAdjTime", arrData[strGridRow][3].replace("+", ""));
			} else {
				setField("selOperator", "-");
				setField("txtAdjTime", arrData[strGridRow][3].replace("-", ""));
			}
			setField("hdnVersion", arrData[strGridRow][5]);
			if (arrData[strGridRow][6] == "Active") {
				getFieldByID("chkStatus").checked = true;
			} else {
				getFieldByID("chkStatus").checked = false;
			}
		}
		//
	}
	Disable("btnAdd", false);
	objOnFocus();
}

function addClick() {
	if (chkChanges()) {
		enableInputControls();
		clearAirportDst();
		opener.setPageEdited(false, true);
		setVisible("spnNotifyNote", true);
		Disable("btnEdit", true);
		Disable("btnDisableRollback", true);
		Disable("btnSave", false);
		Disable("btnReset", false);
		setField("hdnMode", "ADD");
		isLoadCal = "YES";
		setField("hdnDSTId", 0);
		setField("selOperator", "+");
		getFieldByID("chkStatus").checked = "true";
		document.getElementById("btnSave").value = "Save";
	}
	objOnFocus();
}
function editClick() {
	objOnFocus();
	setVisible("spnNotifyNote", true);
	enableInputControls();

	// If date less or equal to current date, Disable Start date/time in edit
	// mode
	if (checkValidStartDate(getValue("txtStart"))) {
		Disable("txtStart", false);
		Disable("txtStartTime", false);
		getFieldByID("txtStart").focus();
	} else {
		Disable("txtStart", true);
		Disable("txtStartTime", true);
		getFieldByID("selOperator").focus();
	}

	Disable("btnDisableRollback", true);
	Disable("btnSave", false);
	Disable("btnReset", false);
	setField("hdnMode", "EDIT");
	isLoadCal = "YES";
	document.getElementById("btnSave").value = "Save";
}

function disableRollbackClick() {
	objOnFocus();
	isLoadCal = "NO";
	Disable("txtStart", true);
	Disable("txtEnd", true);
	Disable("txtStartTime", true);
	Disable("txtEndTime", true);
	Disable("txtAdjTime", true);
	Disable("selOperator", true);
	Disable("chkStatus", true);

	setVisible("spnNotifyNote", true);
	setField("hdnMode", "DISABLEROLLBACK");
	getFieldByID("chkStatus").checked = "false";
	Disable("btnSave", false);
	Disable("btnReset", false);
	document.getElementById("btnSave").value = "Confirm";
}

function objOnFocus() {
	HidePageMessage();
}

function StartTimePress(objTextBox) {
	objOnFocus();
	opener.setPageEdited(true, true);
	var strCR = getText("txtStartTime");
	var strLen = strCR.length;
	var blnEmpty = isEmpty(strCR);
	if (blnEmpty) {
		setField("txtStartTime", strCR.substr(0, strLen - 1));
		getFieldByID("txtStartTime").focus();
	}

}

function EndTimePress(objTextBox) {
	objOnFocus();
	opener.setPageEdited(true, true);
	var strCD = getText("txtEndTime");
	var strLen = strCD.length;
	var blnEmpty = isEmpty(strCD);
	if (blnEmpty) {
		setField("txtEndTime", strCD.substr(0, strLen - 1));
		getFieldByID("txtEndTime").focus();
	}
}

function AdjTimePress(objTextBox) {
	objOnFocus();
	opener.setPageEdited(true, true);
	var strCB = getText("txtAdjTime");
	var strLen = strCB.length;
	var blnEmpty = isEmpty(strCB);
	if (blnEmpty) {
		setField("txtAdjTime", strCB.substr(0, strLen - 1));
		getFieldByID("txtAdjTime").focus();
	}
}

function checkStartTime() {
	var strCR = getText("txtStartTime");
	var blnVal = IsValidTime(trim(strCR));
	if (!blnVal) {
		return true;
	}
}

function checkEndTime() {
	var strCR = getText("txtEndTime");
	var blnVal = IsValidTime(trim(strCR));
	if (!blnVal) {
		return true;
	}
}

function checkAdjTime() {
	var strCR = getText("txtAdjTime");
	var blnVal = IsValidTime(trim(strCR));
	if (!blnVal) {
		return true;
	}
}

function clickEmail(field) {
	pageOnChange();
	
	if(field.checked){
		setVisible("divEmailAddr", true);
		Disable("txtEmailID", false);
	} else {
		setVisible("divEmailAddr", false);
		setField("txtEmailID", "");
		Disable("txtEmailID", true);
	}
}

function clickEmailPax(){
	pageOnChange();
}

function clickSmsPax(field){
	pageOnChange();
	if(field.checked){
		Disable("chkSmsCNF", false);
		Disable("chkSmsOnhold", false);
		setField("chkSmsCNF", true);
		setField("chkSmsOnhold", true);
	} else {
		setField("chkSmsCNF", false);
		setField("chkSmsOnhold", false);
		Disable("chkSmsCNF", true);
		Disable("chkSmsOnhold", true);
	}
}

function pageOnChange() {
	opener.setPageEdited(true, true);
}
function chkChanges() {
	// if (opener.top.pageEdited) {
	if (opener.top[1].objTMenu.tabGetPageEidted(screenId)) {
		return confirm("Changes will be lost! Do you wish to Continue?");
	} else {
		return true;
	}
}

function isTimeFormatValid(objTime) {
	// Checks if time is in HH:MM format - HH can be more than 24 hours
	var timePattern = /^(\d{1,2}):(\d{2})$/;
	var matchArray = getValueTrimed(objTime).match(timePattern);
	if (matchArray == null) {
		return false;
	}
	var hour = matchArray[1];
	var min = matchArray[2];
	if ((min < 0) || (min > 59)) {
		return false;
	}
	if (hour.length < 2) {
		setField(objTime, "0" + getValueTrimed(objTime));
	}
	return true;
}

function getValueTrimed(strValue) {
	return trim(getValue(strValue));
}

// Calender
var objCal1 = new Calendar("spnCalendarDG1");

objCal1.onClick = "setDate";
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtStart", strDate);
		break;
	case "1":
		setField("txtEnd", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	if (isLoadCal == "YES") {
		objCal1.ID = strID;
		objCal1.top = 130;
		objCal1.left = 0;
		objCal1.onClick = "setDate";
		objCal1.showCalendar(objEvent);
	}

}

function windowclose() {
	if (opener.isAirportDstEdited) {
		// opener.closeParentAndChildWindow();
		var cfrm = confirm("Changes will be lost! Do you wish to Continue?");
		if (cfrm) {
			opener.setPageEdited(false, false);
			opener.isAirportEdited = false;
			opener.isAirportDstEdited = false;
			checkDstChanged();
			// window.close();
		}
	} else {
		checkDstChanged();
		// window.close();
	}
}

function checkDstChanged() {
	if (isChangedDST == "YES") {
		var selCountry = getFieldByID("selCountry").value;
		opener.location = "showAirport.action?hdnMode=SEARCH" + "&selCountry="
				+ selCountry + "&hdnRecNo="
				+ opener.getTabValues(screenId, valueSeperator, "intLastRec");
		window.close();
	} else {
		window.close();
	}
}

function validateAdjTime(adjTime){
	if(adjTime != null){
		var times = adjTime.split(':');
		if(times[0] == '00' || times[0] == '0' ){
			if(times[1] == '00' ||
					times[1] == '01' ||
					times[1] == '02' ||
					times[1] == '03' ||
					times[1] == '04' ){
				return true;
			} 
		} else return false;
		
	}	
}