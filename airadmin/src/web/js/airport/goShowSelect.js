function comboConfig() {

	objCmb.onChange = "pageOnChange";

	if (window.XMLHttpRequest) { // Non-IE browsers
		objCmb.top = "540";
		objCmb.left = "692";
		objCmb.width = "170";
		objCmb.height = "48";
		objCmb.tabIndex = 27;
	} else if (window.ActiveXObject) { // IE
		objCmb.top = "530";
		objCmb.left = "722";
		objCmb.width = "170";
		objCmb.height = "48";
		objCmb.tabIndex = 27;
	}
}

function getEditComboData() {
	var stationCode = getValue("selFormStation");
	var arr = new Array();
	arr[0] = new Array("N-A", "");

	if (agentsOfAllAirports.length > 0) {
		var tempTerritory = "";
		for ( var v = 0; v < agentsOfAllAirports.length; v++) {
			if (stationCode == agentsOfAllAirports[v][3]) {
				tempTerritory = agentsOfAllAirports[v][2];
				break;
			}
		}

		var y = 1;
		for ( var x = 0; x < agentsOfAllAirports.length; x++) {
			if (tempTerritory == agentsOfAllAirports[x][2]
					|| (agentsOfAllAirports[x][2] == "" && agentsOfAllAirports[x][3] == stationCode)) {
				arr[y] = new Array(agentsOfAllAirports[x][0],
						agentsOfAllAirports[x][1]);
				y++;
			}
		}
	}
	return arr;
}

function stationChange() {
	objCmb.arrData = getEditComboData();
	objCmb.getText('');
	setField('hdnGoShowAgent', '');
	pageOnChange();
}
