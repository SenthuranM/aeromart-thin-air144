var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "30%";
objCol1.arrayIndex = 1;
objCol1.headerText = "Terminal Code";
objCol1.itemAlign = "center"
objCol1.sort = true;

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "30%";
objCol2.arrayIndex = 2;
objCol2.headerText = "Terminal Name";
objCol2.itemAlign = "Center"
objCol2.sort = true;

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "20%";
objCol3.arrayIndex = 3;
objCol3.headerText = "Default ";
objCol3.itemAlign = "Center"
objCol3.sort = true;

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "20%";
objCol4.arrayIndex = 4;
objCol4.headerText = "Active ";
objCol4.itemAlign = "Center"
objCol4.sort = true;

// ---------------- Grid
var objDG = new DataGrid("spnTerminal");

objDG.addColumn(objCol2);
objDG.addColumn(objCol1);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);



objDG.width = "99%";
objDG.height = "125px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = parent.arrData;
objDG.seqNo = true;
objDG.seqNoWidth = "5%";
objDG.rowClick = "parent.RowClick";
objDG.displayGrid();
objDG.shortCutKeyFunction = "Body_onKeyDown";

// ----------------------------------------
