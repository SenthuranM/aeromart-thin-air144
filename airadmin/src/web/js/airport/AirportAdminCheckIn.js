var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "20%";
objCol1.arrayIndex = 1;
objCol1.headerText = "Carrier Code";
objCol1.itemAlign = "center"
objCol1.sort = true;

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "20%";
objCol2.arrayIndex = 2;
objCol2.headerText = "Flight Number";
objCol2.itemAlign = "Center"
objCol2.sort = true;

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "30%";
objCol3.arrayIndex = 3;
objCol3.headerText = "Check In time gap (HH:MM) ";
objCol3.itemAlign = "Center"
objCol3.sort = true;

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "30%";
objCol4.arrayIndex = 6;
objCol4.headerText = "Check In closing time(HH:MM) ";
objCol4.itemAlign = "Center"
objCol4.sort = true;


// ---------------- Grid
var objDG = new DataGrid("spnCI");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);


objDG.width = "99%";
objDG.height = "125px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = parent.arrData;
objDG.seqNo = true;
objDG.seqNoWidth = "5%";
objDG.rowClick = "parent.RowClick";
objDG.displayGrid();
objDG.shortCutKeyFunction = "Body_onKeyDown";

// ----------------------------------------
