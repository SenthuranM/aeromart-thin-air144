//*
//* Author : K.Srikanth
//*/

var strRowData;
var strGridRow = -1;	
var intLastRec;
var screenId = "SC_ADMN_004";
var valueSeperator = "~";
var objCmb1 = new combo();
var objCmb2 = new combo();
var objCmb3 = new combo();
var begin = [["ALL", "ALL", "ALL"]];
var begin1 = [["*", "ALL"]];

var isAirportEdited = false;
var isAirportDstEdited = false;
var blnEngVisible = (engineVisiblity == 'true') ? true : false;
var blnLCCEnable = (lccEnableStatus == 'true') ? true : false;
var arrAirportInOL = new Array();

var ssHtmlStr = '';
function validateAirport(){

	if (document.getElementById("radOnline2").checked && objCmb.getText() != "") {
		showCommonError("Error", goShowAgentCannotExist);
		objCmb.disable(false);
		objCmb.focus();
		return false;
	}
	if (isEmpty(getText("txtAirportCode"))) {
		showCommonError("Error", airportIdRqrd);
		getFieldByID("txtAirportCode").focus();
		return false;
	}
	if (document.getElementById("selFormStation").value == "-1") {
		showCommonError("Error", stationRqrd);
		getFieldByID("selFormStation").focus();
		return false;
	}

	if (getValue("txtAirportCode") != null || getValue("txtAirportCode") != "") {
		if (getValue("txtAirportCode").length < 3) {
			showCommonError("Error", airportIdMinLength);
			getFieldByID("txtAirportCode").focus();
			return false;
		}
	}

	if (isEmpty(getText("txtAirportName"))) {
		showCommonError("Error", airportNameRqrd);
		getFieldByID("txtAirportName").focus();
		return false;
	}
/*	if (validateAirportTranslation() == false) {		
		showCommonError("Error",airportNameRqrd);
		getFieldByID("translate").focus();
		return false;
	}*/
	
	//GMT off set action validations

	if (isEmpty(getText("txtGmtOffSetAction"))) {
		showCommonError("Error", airportGmtoffsetctionRqrd);
		getFieldByID("txtGmtOffSetAction").focus();
		return false;
	}

	var gmtOffSetAction = getValue("txtGmtOffSetAction").split(":");

	if (checkGmtOffSet()) {
		showCommonError("Error", airportGmtOffSetInvalid);
		getFieldByID("txtGmtOffSetAction").focus();
		return false;
	}
	
	//minimum stop overtime validations

	if (isEmpty(getText("txtMinStopOvrTime"))) {		
		showCommonError("Error",airportMinstopovrtimeRqrd);
		getFieldByID("txtMinStopOvrTime").focus();
		return false;
	}

	if (getValue("txtMinStopOvrTime") == null
			|| getValue("txtMinStopOvrTime") == "") {
		showCommonError("Error", airportMinstopovrtimeRqrd);
		getFieldByID("txtMinStopOvrTime").focus();
		return false;
	}
	if (checkMinStopOvrTime()) {
		showCommonError("Error", airportMinstopovrtimeInvalid);
		getFieldByID("txtMinStopOvrTime").focus();
		return false;
	}
	var minStopTime = getValue("txtMinStopOvrTime").replace(":", ".");
	var defaultMinStopTime = formatTimeWithColon(defaultMinStopOverTime);
	defaultMinStopTime = defaultMinStopTime.replace(":", ".");

	if (minStopTime < defaultMinStopTime) {
		showCommonError("Error", airportMinstopovrtimeBnsRleVltd);
		getFieldByID("txtMinStopOvrTime").focus();
		return false;
	}
	
	//Notification Start Cutover time Validation
	if(checkNotifStartCutOverTime()){
		showCommonError("Error", nofiCutoverStartTimeInvalid);
		getFieldByID("anciStartCutoverTime").focus();
		return false;
	}
	
	//Meal notification timing validation
	if (getFieldByID('chkMealNotification').checked) {
		setField("chkMealNotification","ACT");	
		if (getValue("txtNotifyEmail") == null || trim(getValue("txtNotifyEmail")) == "") {
			showCommonError("Error", notificationEmailRequired);
			getFieldByID("txtNotifyEmail").focus();
			return false;
		} else {
			if (!validateBillingEmail()) {
				showCommonError("Error", notificationEmailInvalid);
				getFieldByID("txtNotifyEmail").focus();
				return false;
			}
		}
		
		if (getValue("mealNotifyStartTime") == null || trim(getValue("mealNotifyStartTime")) == "") {
			showCommonError("Error", notificationStartTimeRequired);
			getFieldByID("mealNotifyStartTime").focus();
			return false;
		} else {
			var startTime = trim(getValue("mealNotifyStartTime")).split(":");
			var startTimeMin = startTime[1];
			if (parseInt(startTimeMin) > 59 || (parseInt(startTime[0])+parseInt(startTime[1]) == 0)) {
				showCommonError("Error", notificationStartTimeInvalid);
				getFieldByID("mealNotifyStartTime").focus();
				return false;
			}
			if (startTime[0].length > 3) {
				showCommonError("Error", notificationStartTimeInvalid);
				getFieldByID("mealNotifyStartTime").focus();
				return false;
			}
		}

		if (getValue("notificationFrequency") == null || trim(getValue("notificationFrequency")) == "") {
			showCommonError("Error", notificationFrequencyRequired);
			getFieldByID("notificationFrequency").focus();
			return false;
		} else {
			if (trim(getValue("notificationFrequency")) == 0) {
				showCommonError("Error", notificationFrequencyInvalid);
				getFieldByID("notificationFrequency").focus();
				return false;
			}
		}

		if (getValue("lastNotifyTime") == null || trim(getValue("lastNotifyTime")) == "") {
			showCommonError("Error", lastNotificationTimeRequired);
			getFieldByID("lastNotifyTime").focus();
			return false;
		} else {
			var lastTime = trim(getValue("lastNotifyTime")).split(":");
			var lastTimeMin = lastTime[1];
			if (parseInt(lastTimeMin) > 59) {
				showCommonError("Error", lastNotificationTimeInvalid);
				getFieldByID("lastNotifyTime").focus();
				return false;
			}
			if (lastTime[0].length > 3) {
				showCommonError("Error", lastNotificationTimeInvalid);
				getFieldByID("lastNotifyTime").focus();
				return false;
			}
		}

		if ((getValue("mealNotifyStartTime") != null || trim(getValue("mealNotifyStartTime")) != "") && (getValue("lastNotifyTime") != null || trim(getValue("lastNotifyTime")) != "")) {
			var sTime = trim(getValue("mealNotifyStartTime")).split(":");
			var lTime = trim(getValue("lastNotifyTime")).split(":");
			if ((parseInt(lTime[0]) +parseInt(lTime[1])) >= (parseInt(sTime[0]) + parseInt(sTime[1]))) {
				showCommonError("Error", lastNotificationTimeWrong);
				getFieldByID("lastNotifyTime").focus();
				return false;
			}
		}
	}
		
	//Notification End Cutover time Validation
	if(checkNotifEndCutOverTime()){
		showCommonError("Error", nofiCutoverEndTimeInvalid);
		getFieldByID("anciEndCutoverTime").focus();
		return false;
	}
	
	if(validateCutOverStartEndTime()){
		showCommonError("Error", nofiCutoverStartEndTimeInvalid);
		getFieldByID("anciStartCutoverTime").focus();
		return false;
	}
	
	if (!isSelectCarriers()) {
		getFieldByID("selCarrierCodes").focus();
		showCommonError("Error", carrierSelectionRequired);
		return false;
	}
	if (getValue("selLccPublishStatus") == "-1" && blnLCCEnable) {
		getFieldByID("selLccPublishStatus").focus();
		showCommonError("Error", lccPublishStatusSelectionRequired);
		return false;
	}	
	if(validateAirportTranslation() == false){
		return false;
	}
	
	if (isGroundServiceEnabled) {
		if(getFieldByID('chkSurfaceSegment').checked){
			if(getValue('selConnectingAirport') == ''){
				showCommonError("Error", connectingAirportRqrd);
				return false;
			} else if(getValue('selConnectingAirport') == getValue("txtAirportCode")){
				showCommonError("Error", connectingAirportInvalid);
				return false;
			}
		}
	}
	
	top[2].HideProgress();

	return true;
}

var objForm;

function ViewDetail(){

	if(document.forms[0].hdnMode.value=="ADD"){
		alert("Airport details should be saved first, to View/Edit DST");	
	}else if(top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))){
		//Save Data before display Airport DSt page
		
		if(document.getElementById("txtAirportCode").value == null || document.getElementById("txtAirportCode").value==""){
			showCommonError("Error","Please select a airport to modify!");
			return;
		}
		if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
			top[0].objWindow.close();
		}

		var intLeft = (window.screen.width - 800) / 2;
		var intTop = (window.screen.height - 570) / 2;
		var strAirportCode = getFieldByID("txtAirportCode").value;
		var strGMTOffset = getFieldByID("hdnGMTOffset").value;
		var strCountry = objCmb1.getComboValue(); //getFieldByID("selCountryName").value;
		enableInputControls();
		document.forms[0].hdnMode.value="";
		
		top[0].objWindow = window.open("about:blank","CWindow",'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=800,height=570,resizable=no,top=' + intTop + ',left=' + intLeft);
		objForm  = document.getElementById("frmAirport");
		objForm.target = "CWindow";
		objForm.action = "showAirportDST.action?strCountry=" + strCountry
				+ "&strGridRow=" + strGridRow;
		objForm.submit();
		disableInputControls();

	}
}
function ViewDetailCIT(){

	if(document.forms[0].hdnMode.value=="ADD"){
		alert("Airport details should be saved first, to View/Edit CIT");	
	}else if(top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))){
		//Save Data before display Airport DSt page
		
		if(document.getElementById("txtAirportCode").value == null || document.getElementById("txtAirportCode").value==""){
			showCommonError("Error","Please select a airport to modify!");
			return;
		}
		if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
			top[0].objWindow.close();
		}

		var intLeft = (window.screen.width - 800) / 2;
		var intTop = (window.screen.height - 500) / 2;
		var strAirportCode = getFieldByID("txtAirportCode").value;
		var strGMTOffset = getFieldByID("hdnGMTOffset").value;
		var strCountry = objCmb1.getComboValue(); //getFieldByID("selCountryName").value;
		enableInputControls();
		document.forms[0].hdnMode.value="";
		
		top[0].objWindow = window.open("about:blank","CWindow",'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=800,height=500,resizable=no,top=' + intTop + ',left=' + intLeft);
		objForm  = document.getElementById("frmAirport");
		objForm.target = "CWindow";
		objForm.action = "showAirportCIT.action?" + "&strGridRow=" + strGridRow;
		objForm.submit();
		disableInputControls();

	}
}
function ViewDetailTerminal(){

	if(document.forms[0].hdnMode.value=="ADD"){
		alert("Airport details should be saved first, to View/Edit CIT");	
	}else if(top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))){
		//Save Data before display Airport DSt page
		
		if(document.getElementById("txtAirportCode").value == null || document.getElementById("txtAirportCode").value==""){
			showCommonError("Error","Please select a airport to view Terminals!");
			return;
		}
		if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
			top[0].objWindow.close();
		}

		var intLeft = (window.screen.width - 800) / 2;
		var intTop = (window.screen.height - 500) / 2;
		var strAirportCode = getFieldByID("txtAirportCode").value;
		var strGMTOffset = getFieldByID("hdnGMTOffset").value;
		var strCountry = objCmb1.getComboValue(); //getFieldByID("selCountryName").value;
		enableInputControls();
		document.forms[0].hdnMode.value="";
		
		top[0].objWindow = window.open("about:blank","CWindow",'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=800,height=500,resizable=no,top=' + intTop + ',left=' + intLeft);
		objForm  = document.getElementById("frmAirport");
		objForm.target = "CWindow";
		objForm.action = "showAirportTerminal.action?" + "&strGridRow=" + strGridRow;
		objForm.submit();
		disableInputControls();

	}
}
// on Grid Row click
function RowClick(strRowNo) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		if (isGroundServiceEnabled) {
			sspopup1.CloseWindow();
		}
		objOnFocus();
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow=strRowNo;
		setField("hdnGridRow",strRowNo);
		setField("hdnMode","");
		setField("txtAirportCode",strRowData[0]);
		setField("txtAirportName",strRowData[1]);
		setField("selFormStation",strRowData[3]);	
		setAirpotTranslationList();
	
		objCmb.arrData =getEditComboData();


	
		if(arrData[strRowNo][22] == null || arrData[strRowNo][22] == "" || arrData[strRowNo][22] == "null" ){
			setField("selClosestAirport","-1");	
		}else
			setField("selClosestAirport",arrData[strRowNo][22]);			
		if(arrData[strRowNo][4]=="Online"){
			document.getElementById("radOnline1").checked = true;
			document.getElementById("radOnline2").checked = false;
		} else {
			document.getElementById("radOnline2").checked = true;
			document.getElementById("radOnline1").checked = false;
		}

		setField("chkActive",arrData[strRowNo][5]);		
		if(arrData[strRowNo][5]=="Active" || arrData[strRowNo][5]=="active")
			document.getElementById("chkActive").checked = true;
		else
			document.getElementById("chkActive").checked = false;
		
		
		setField("hdnVersion", arrData[strRowNo][7]);
		setField("txtContact", arrData[strRowNo][8]);
		setField("txtFax", arrData[strRowNo][9]);
		setField("selGmtOffSetAction", arrData[strRowNo][10]);
		setField("txtGmtOffSetAction", arrData[strRowNo][11]);

		if (arrData[strRowNo][13] == 'N')
			document.getElementById("rndLatitude1").checked = true;
		if (arrData[strRowNo][13] == 'S')
			document.getElementById("rndLatitude2").checked = true;

		setField("txtLatitude1", arrData[strRowNo][12].substring(0, 3));
		setField("txtLatitude2", arrData[strRowNo][12].substring(3, 6));
		setField("txtLatitude3", arrData[strRowNo][12].substring(6, 9));

		if (arrData[strRowNo][15] == 'E')
			document.getElementById("rndLongitude1").checked = true;
		if (arrData[strRowNo][15] == 'W')
			document.getElementById("rndLongitude2").checked = true;


		setField("txtLongitude1",arrData[strRowNo][14].substring(0,3));	
		setField("txtLongitude2",arrData[strRowNo][14].substring(3,6));
		setField("txtLongitude3",arrData[strRowNo][14].substring(6,9));

		setField("txtPhone",arrData[strRowNo][16]);		
		setField("txtaRemarks",arrData[strRowNo][17]);	
		
		setField("txtMinStopOvrTime",arrData[strRowNo][18]);	
		if(arrData[strRowNo][21]=="Yes" && arrData[strRowNo][25] != null) {
			setField("lblStarts",arrData[strRowNo][24]);			
			setField("lblEnds",arrData[strRowNo][25]);			
		}else{
			setField("lblStarts","");			
			setField("lblEnds","");				
		}

		setField("hdnGMTOffset", arrData[strRowNo][26]);

		if (blnEngVisible) {
			if (arrData[strRowNo][28] == "Y") {
				getFieldByID("chkIBEVisibililty").checked = true;
			} else {
				getFieldByID("chkIBEVisibililty").checked = false;
			}
			if (arrData[strRowNo][29] == "Y") {
				getFieldByID("chkXBEVisibililty").checked = true;
			} else {
				getFieldByID("chkXBEVisibililty").checked = false;
			}

		}

		setPageEdited(false, false);
		disableInputControls();

		Disable("btnAdd", false);
		Disable("btnEdit", false);		
		Disable("btnDelete", false);		
		Disable("btnAirportFD", false);	
		if((arrData[strRowNo][4]=="Online")&& (arrData[strRowNo][5]=="Active" )){
	
			Disable("btnDST",false);
		} else {
	
			Disable("btnDST",true);
		}	
		if((arrData[strRowNo][4]=="Online")&& (arrData[strRowNo][5]=="Active" )){
			
			Disable("btnCIT",false);
			Disable("btnTerminals",false);
		} else {
	
			Disable("btnCIT",true);
			Disable("btnTerminals",true);
		}
		var strCompination = strRowData[0]+ "," + arrData[strRowNo][10]+ arrData[strRowNo][11];		
		setTabValues(screenId, valueSeperator, "airportDstCompination", strCompination);
		
		if(arrData[strRowNo][30]==null||arrData[strRowNo][30]==""){
			objCmb.setText("");
			objCmb.setComboValue("A-N");
			document.getElementById("hdnGoShowAgent").value = "";
		} else {
			objCmb.setComboValue(arrData[strRowNo][30]);
		}
		
		setField("chkManualCheckIn", arrData[strRowNo][33]);
		if(arrData[strRowNo][33]=="Y" || arrData[strRowNo][33]=="y")
			document.getElementById("chkManualCheckIn").checked = true;
		else
			document.getElementById("chkManualCheckIn").checked = false;
		
		setField("hdnLCCVisiblity",arrData[strRowNo][34]);
		deselectCarriers();
		var strCarrCodes = arrData[strRowNo][35].replace("<br>",",")+",";
		if (trim(strCarrCodes)!=""){
			var strCarrCode = strCarrCodes.split(",");
			for ( var x=0; x<strCarrCode.length ; x++){
				setSelectedCarrier(strCarrCode[x]);
			}
		}
		setField("hdnCarrierCode",arrData[strRowNo][35].replace("<br>",",")+",");
		if(blnLCCEnable) { setField("selLccPublishStatus",arrData[strRowNo][36]); } 
		setField("anciStartCutoverDay",arrData[strRowNo][37]);	
		setField("anciStartCutoverTime",arrData[strRowNo][38]);	
		setField("anciEndCutoverDay",arrData[strRowNo][39]);	
		setField("anciEndCutoverTime",arrData[strRowNo][40]);	
		setField("chkOnlineCheckin", arrData[strRowNo][41]);
		setField("enableCFG", arrData[strRowNo][52]);
		setField("selFormCity",arrData[strRowNo][53]);	

		if(arrData[strRowNo][41]=="Y" || arrData[strRowNo][41]=="y")
			document.getElementById("chkOnlineCheckin").checked = true;
		else
			document.getElementById("chkOnlineCheckin").checked = false;
		
		if (isGroundServiceEnabled) {		
			if(arrData[strRowNo][42] == "Y" || arrData[strRowNo][42] == "y"){
				getFieldByID('chkSurfaceSegment').checked = true;
				Disable('btnShowSubStations', false);
			} else {
				getFieldByID('chkSurfaceSegment').checked = false;
				Disable('btnShowSubStations', true);
			}
			
			setField('selConnectingAirport', arrData[strRowNo][43]);
		}
		
		if(arrData[strRowNo][52]=="Y" || arrData[strRowNo][52]=="y")
			document.getElementById("enableCFG").checked = true;
		else
			document.getElementById("enableCFG").checked = false;
		
		setField("chkMealNotification",arrData[strRowNo][46]);		
		if (arrData[strRowNo][46]=="ACT" || arrData[strRowNo][46]=="act") {
			document.getElementById("chkMealNotification").checked = true;
		}else {
			document.getElementById("chkMealNotification").checked = false;
		}	
		setField("txtNotifyEmail",arrData[strGridRow][45]);	
		setField("mealNotifyStartTime",arrData[strGridRow][47]);	
		setField("notificationFrequency",arrData[strGridRow][49]);	
		setField("lastNotifyTime",arrData[strGridRow][48]);
		
		if(arrData[strRowNo][50] == "Y" || arrData[strRowNo][50] == "y"){
			getFieldByID('chkETLProcessEnabled').checked = true;
		} else {
			getFieldByID('chkETLProcessEnabled').checked = false;
		}
		
		
		if(arrData[strRowNo][51] == "Y" || arrData[strRowNo][51] == "y"){
            getFieldByID('chkAutoFlownProcessEnabled').checked = true;
        } else {
            getFieldByID('chkAutoFlownProcessEnabled').checked = false;
        }
	}
}

/*
 * Function call for SEARCH Button Click
 */
function searchAirport() {
	objOnFocus();	
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setField("hdnMode", "SEARCH");
		
		setField("hdnRecNo", 1);
		setField("hdnSearchData", getSearchData());
		getFieldByID("frmAirport").target = "_self";
		getFieldByID("frmAirport").action = "showAirport.action"
		getFieldByID("frmAirport").submit();
		top[2].ShowProgress();
	}

}

function getSearchData(){
	var tmpCountryName;
	var tmpAirCode;
	var tmpAirName;
	
	if(objCmb1.getText() != "" && objCmb1.getText() != "ALL"){ // this is for refresh the selected value
		tmpCountryName = objCmb1.getComboValue();
	}
	else {
		tmpCountryName = "*";
	}
	if(objCmb2.getText() != "" && objCmb2.getText() != "ALL"){ // this is for refresh the selected value
		tmpAirCode = objCmb2.getComboValue();
	} else {
		tmpAirCode = "*";
	}	
	if(objCmb3.getText() != "" && objCmb3.getText() != "ALL"){ // this is for refresh the selected value
		tmpAirName = objCmb3.getComboValue();
	} else {
		tmpAirName = "*";
	}	
	
	return tmpCountryName + "," + tmpAirCode + "," + tmpAirName;
}

var objDelayFocus = null;

//On Page loading make sure that all the page input fields are cleared
function winOnLoad(strMsg,strMsgType) {
	if (saveSuccess == 1) {
		alert("Record Successfully saved!");
		saveSuccess = 0;
	} else if (saveSuccess == 3) {
		alert("Record Successfully saved!");
		alert(newAirportAdded);
		saveSuccess = 0;
	}

	if(isGroundServiceEnabled){
		setVisible('btnShowSubStations',true);
		setVisible('spnSurfaceSegment',true);
		setVisible('spnConnectingAirport',true);
		setVisible('chkSurfaceSegment',true);
		setVisible('selConnectingAirport',true);
		
		createSubStationPopup(); 
	} else {
		setVisible('btnShowSubStations',false);
		setVisible('spnSurfaceSegment',false);
		setVisible('spnConnectingAirport',false);
		setVisible('chkSurfaceSegment',false);
		setVisible('selConnectingAirport',false);
	}
	
	createPopUps();
	PopUpData();
	clearInputControls();
	disableInputControls();
	setPageEdited(false, false);		
	// to set the Country after search	
	var strTmpSearch = searchData;
	if (strTmpSearch != "" && strTmpSearch != null) { // sets the search data
		var strSearch = strTmpSearch.split(",");
		objCmb1.setComboValue(strSearch[0]);
		objCmb2.setComboValue(strSearch[1]);
		objCmb3.setComboValue(strSearch[2]);
	}
	filterAirportCodeAndName();
	if(strMsg != null && strMsgType != null)
		showCommonError(strMsgType,strMsg);
	
	
	Disable("btnEdit", true);		
	Disable("btnDelete", true);		
	Disable("btnSave", true);		
	Disable("btnAirportFD", true);	
	if (isGroundServiceEnabled) {
		Disable('btnShowSubStations', true);		
	}
	Disable("btnReset", true);		
	Disable("btnDST", true);
	Disable("btnCIT", true);
	Disable("btnTerminals",true);
	objCmb.disable(true);

	if (top[1].objTMenu.focusTab == screenId) {
		objCmb1.focus();
	}
	document.getElementById("rndLatitude1").checked = true;
	document.getElementById("rndLongitude1").checked = true;

	if(arrFormData != null && arrFormData.length > 0) {
		setPageEdited(true,false);	
		strGridRow = getTabValues(screenId, valueSeperator, "strGridRow"); //Get saved grid Row
		if((arrFormData[0][1]=="Online")||(arrFormData[0][1]=="1")){
			document.getElementById("radOnline1").checked = true;
			document.getElementById("radOnline2").checked = false;
		} else {
			document.getElementById("radOnline2").checked = true;
			document.getElementById("radOnline1").checked = false;
		}

		setField("txtAirportCode", arrFormData[0][2]);
		setField("txtAirportName", arrFormData[0][3]);
		setField("txtContact", arrFormData[0][4]);
		setField("txtPhone", arrFormData[0][5]);
		setField("txtFax", arrFormData[0][6]);

		setField("txtaRemarks", arrFormData[0][7]);
		setField("lblStarts", arrFormData[0][8]);
		setField("lblEnds", arrFormData[0][9]);

		setField("selFormStation", arrFormData[0][12]);
		setField("selClosestAirport", arrFormData[0][13]);

		setField("selGmtOffSetAction", arrFormData[0][14]);
		setField("txtGmtOffSetAction", arrFormData[0][15]);
		setField("txtMinStopOvrTime", arrFormData[0][16]);
		setField("txtLatitude1", arrFormData[0][19]);
		setField("txtLatitude2", arrFormData[0][23]);
		setField("txtLatitude3", arrFormData[0][24]);
		if (arrFormData[0][18] == 'N')
			document.getElementById("rndLatitude1").checked = true;
		if (arrFormData[0][18] == 'S')
			document.getElementById("rndLatitude2").checked = true;
		if (arrFormData[0][20] == 'E')
			document.getElementById("rndLongitude1").checked = true;
		if (arrFormData[0][20] == 'W')
			document.getElementById("rndLongitude2").checked = true;
		setField("txtLongitude1", arrFormData[0][21]);
		setField("txtLongitude2", arrFormData[0][25]);
		setField("txtLongitude3", arrFormData[0][26]);
		setField("txtLongitude3", arrFormData[0][26]);
		
		
		setField("anciStartCutoverDay", arrFormData[0][30]);
		setField("anciStartCutoverTime", arrFormData[0][31]);
		setField("anciEndCutoverDay", arrFormData[0][32]);
		setField("anciEndCutoverTime", arrFormData[0][33]);
		setField("selLccPublishStatus", arrFormData[0][36]);
		
		setSelectedCarrierCodes(arrFormData[0][37]);
		setField("selFormCity", arrFormData[0][38]);
		setField("hdnLCCVisiblity", arrFormData[0][39]);

		if (arrFormData[0][11] == "Active" || arrFormData[0][11] == "active"
				|| arrFormData[0][11] == "on")
			getFieldByID("chkActive").checked = true;
		else
			getFieldByID("chkActive").checked = false;

		if (blnEngVisible) {
			if (arrFormData[0][27] == "Y") {
				getFieldByID("chkIBEVisibililty").checked = true;
			} else {
				getFieldByID("chkIBEVisibililty").checked = false;
			}
			if (arrFormData[0][28] == "Y") {
				getFieldByID("chkXBEVisibililty").checked = true;
			} else {
				getFieldByID("chkXBEVisibililty").checked = false;
			}			
		}	

		enableInputControls();			
	
		if(isAddMode){
			setField("hdnMode","ADD");
			if (top[1].objTMenu.focusTab == screenId){
				getFieldByID("txtAirportCode").focus();
			}
		}else{
			setField("hdnMode","EDIT");
			Disable("txtAirportCode", true);			
			
			//Check whether Airport is used. If so disable GMT offset setting.
			var airportUsed = getTabValues(screenId, valueSeperator, "tempSearch");						
			if (airportUsed != null && airportUsed != "" ){
				Disable("selGmtOffSetAction",airportUsed);
				Disable("txtGmtOffSetAction",airportUsed);
			}

			setField("hdnVersion", prevVersion);
			setField("hdnGridRow", prevGridRow);
			strGridRow = prevGridRow;
		}
		if (isStationFocus) {
			if (top[1].objTMenu.focusTab == screenId) {
				getFieldByID("selFormStation").focus();
			}
		} else {
			if (isAirportFocus) {
				if (top[1].objTMenu.focusTab == screenId) {
					getFieldByID("selClosestAirport").focus();
				}
			}
		}
		if(arrFormData[0][36]=="PUB" && blnLCCEnable){
			Disable("selLccPublishStatus", true);
		}

		objCmb.arrData = getEditComboData();
		objCmb.setComboValue(arrFormData[0][29]);
		
		if (isGroundServiceEnabled) {
			if(arrFormData[0][34] == "Y" || arrFormData[0][34] == "y"){
				getFieldByID('chkSurfaceSegment').checked = true;
			} else {
				getFieldByID('chkSurfaceSegment').checked = false;
			}
			
			setField('selConnectingAirport', arrFormData[0][35]);
		}
		
	} else {
		objCmb.arrData = getEditComboData();
		deselectCarriers();
	}
	
	if (isCityFocus) {
		if (top[1].objTMenu.focusTab == screenId) {
			getFieldByID("selFormCity").focus();
		}
	}

	if (isGoShowFocus) {
		objCmb.disable(false);
		objCmb.focus();
	}
	
	if(!blnLCCEnable){
		setVisible("fntLCCPublish", false);
		setVisible("selLccPublishStatus", false);
	} else {
		setVisible("fntLCCPublish", true);
	}
	

}

function selectChange(strCountryCode) {
	objCmb1.setComboValue(strCountryCode);
}

function enableInputControls() {
	objCmb.disable(false);
	Disable("txtAirportCode", false);
	Disable("txtAirportName", false);
	Disable("txtContact", false);
	Disable("txtLatitude1", false);
	Disable("txtLatitude2", false);
	Disable("txtLatitude3", false);

	Disable("txtContact", false);
	Disable("txtPhone", false);
	Disable("selGmtOffSetAction", false);
	Disable("txtGmtOffSetAction", false);
	Disable("txtFax", false);

	Disable("txtMinStopOvrTime", false);
	Disable("txtaRemarks", false);
	Disable("txtLongitude1", false);		
	Disable("txtLongitude2", false);		
	Disable("txtLongitude3", false);				
	Disable("chkActive", false);	
	Disable("chkManualCheckIn", false);

	Disable("selClosestAirport", false);		
	Disable("selFormStation", false);	
	Disable("selFormCity", false);		
	Disable("rndLatitude", false);		
	Disable("rndLongitude", false);			
	Disable("radOnline1", false);		
	Disable("radOnline2", false);		
	Disable("btnSave",false);
	Disable("btnReset",false);
	if (blnEngVisible) {
		Disable("chkIBEVisibililty",false); 
		Disable("chkXBEVisibililty",false);
	}
	Disable("selLccPublishStatus",false);
	Disable("selCarrierCodes",false);
	Disable("anciStartCutoverDay",false);
	Disable("anciStartCutoverTime",false);
	Disable("anciEndCutoverDay",false);
	Disable("anciEndCutoverTime",false);
	Disable("chkOnlineCheckin",false);	
	Disable("chkETLProcessEnabled", false);
	Disable("chkAutoFlownProcessEnabled", false);
	Disable("enableCFG",false);
	
	Disable("chkMealNotification",false);
	
	if (isGroundServiceEnabled) {
		Disable('chkSurfaceSegment',false);
		
		if(getFieldByID('chkSurfaceSegment').checked){
			Disable('selConnectingAirport', false);
		} else {
			Disable('selConnectingAirport', true);
		}
	}
	
	
}

function disableInputControls() {
	objCmb.disable(true);
	Disable("txtAirportCode", true);
	Disable("txtAirportName", true);
	Disable("txtContact", true);
	Disable("txtLatitude1", true);
	Disable("txtLatitude2", true);
	Disable("txtLatitude3", true);

	Disable("txtContact", true);
	Disable("txtPhone", true);
	Disable("selGmtOffSetAction", true);
	Disable("txtGmtOffSetAction", true);
	Disable("txtFax", true);

	Disable("txtMinStopOvrTime", true);
	Disable("txtaRemarks", true);
	Disable("txtLongitude1", true);
	Disable("txtLongitude2", true);
	Disable("txtLongitude3", true);
	Disable("chkActive", true)
	Disable("chkManualCheckIn", true);
	//Disable("btnCIT", true);
	Disable("btnSave", true);
	Disable("btnReset", true);

	Disable("selClosestAirport", true);
	Disable("selFormStation", true);
	Disable("selFormCity", true);
	Disable("rndLatitude", true);
	Disable("rndLongitude", true);
	Disable("radOnline1", true);
	Disable("radOnline2", true);
	if (blnEngVisible) {
		Disable("chkIBEVisibililty", true);
		Disable("chkXBEVisibililty", true);
	}
	Disable("selLccPublishStatus",true);
	Disable("selCarrierCodes",true);
	Disable("anciStartCutoverDay",true);
	Disable("anciStartCutoverTime",true);
	Disable("anciEndCutoverDay",true);
	Disable("anciEndCutoverTime",true);
	Disable("chkOnlineCheckin",true);
	Disable("enableCFG",true);
	
	Disable("chkMealNotification",true);
	Disable("txtNotifyEmail",true);
	Disable("mealNotifyStartTime",true);
	Disable("notificationFrequency",true);
	Disable("lastNotifyTime",true);
	Disable("chkETLProcessEnabled", true);
	Disable("chkAutoFlownProcessEnabled", true);
	Disable("enableCFG",true);
	
	if (isGroundServiceEnabled) {
		Disable('chkSurfaceSegment',true);
		Disable('selConnectingAirport',true);
	}
	
}

function clearInputControls() {
	enableInputControls();
	objCmb.setText("");
	objCmb.setComboValue("");
	setField("hdnVersion", "");
	setField("txtAirportCode", "");
	setField("lblStarts", "");
	setField("lblEnds", "");
	setField("txtAirportName", "");
	setField("txtContact", "");
	setField("txtPhone", "");
	setField("txtFax", "");
	setField("txtaRemarks", "");
	setField("selFormStation", "-1");
	setField("selFormCity", "-1");
	setField("selClosestAirport", "-1");
	setField("selGmtOffSetAction", "+");
	setField("txtGmtOffSetAction", "");
	setField("txtMinStopOvrTime", "");
	setField("txtLatitude1", "");
	setField("txtLatitude2", "");
	setField("txtLatitude3", "");
	setField("txtLongitude1", "");
	setField("txtLongitude2", "");
	setField("txtLongitude3", "");
	setField("anciStartCutoverDay", "");
	setField("anciStartCutoverTime", "00:00");
	setField("anciEndCutoverDay", "");
	setField("anciEndCutoverTime", "00:00");
	setField("txtNotifyEmail", "");
	setField("mealNotifyStartTime", "00:00");
	setField("notificationFrequency", "0");
	setField("lastNotifyTime", "00:00");
		
	Disable("txtAirportCode", false);
	document.getElementById("rndLatitude1").checked = true;
	document.getElementById("rndLongitude1").checked = true;
	if (blnEngVisible) {
		document.getElementById("chkIBEVisibililty").checked = true;
		document.getElementById("chkXBEVisibililty").checked = true;
	}
	deselectCarriers();
	
	if (isGroundServiceEnabled) {
		getFieldByID('chkSurfaceSegment').checked = false;
		Disable('selConnectingAirport',true);
		setField('selConnectingAirport', '');
	}
	document.getElementById("chkETLProcessEnabled").checked = false;
	document.getElementById("chkAutoFlownProcessEnabled").checked = false;
	
}

function enableMealNotifyDetails() {
	if (getFieldByID('chkMealNotification').checked) {
		Disable("txtNotifyEmail",false);
		Disable("mealNotifyStartTime",false);
		Disable("notificationFrequency",false);
		Disable("lastNotifyTime",false);
		setField("chkMealNotification","ACT");
	} else {
		Disable("txtNotifyEmail",true);
		Disable("mealNotifyStartTime",true);
		Disable("notificationFrequency",true);
		Disable("lastNotifyTime",true);
		setField("chkMealNotification","INA");
	}
}

function saveAirport() {
	
	if (!validateAirport())
		return;
	Disable("txtAirportCode", false);
	Disable("selGmtOffSetAction", false);
	Disable("txtGmtOffSetAction", false);
	Disable("hdnGoShowAgent", false);

	var tempName = trim(getText("txtaRemarks"));
	tempName=replaceall(tempName,"'" , "`");
	tempName=replaceEnter(tempName);
	setField("txtaRemarks",tempName);		
	//setField("hdnRecNo",(top[0].intLastRec));
	setField("hdnRecNo",getTabValues(screenId, valueSeperator, "intLastRec"));
	setHdnAirportForDisplay();
	if(objCmb.getText()==null || objCmb.getText()==""||objCmb.getText()=="null"){
		setField("hdnGoShowAgent","");
	}else{
		setField("hdnGoShowAgent",objCmb.getComboValue());
	}
	
	var strVisibile="";
	if(getFieldByID("selCarrierCodes")){
		var intCount = getFieldByID("selCarrierCodes").length ;
		 for (var i = 0; i < intCount; i++){
		 	if (getFieldByID("selCarrierCodes").options[i].selected){
		   	  strVisibile+=getFieldByID("selCarrierCodes").options[i].value+",";
		   	}
		 }
	}	

	setField("hdnCarrierCode", strVisibile);
	if(blnLCCEnable){
		setField("hdnLccPublishStatus",document.getElementById("selLccPublishStatus").value);
	} else {
		setField("hdnLccPublishStatus", "SKP");
	}
	if (blnEngVisible) {
		if(document.getElementById("chkIBEVisibililty").checked) {
			document.getElementById("chkIBEVisibililty").value = "Y";
		} else {
			document.getElementById("chkIBEVisibililty").value = "N";
		}
		if(document.getElementById("chkXBEVisibililty").checked) {
			document.getElementById("chkXBEVisibililty").value = "Y";
		} else {
			document.getElementById("chkXBEVisibililty").value = "N";
		}
	}
	
/*	alert("hdnGoShowAgent-"+
			document.getElementById("hdnGoShowAgent").value+
		  ", getComboValue()-"+
		    objCmb.getComboValue()+
		  ", getComboText()-"+
		    objCmb.getComboText());
*/
	
	getFieldByID("frmAirport").target="_self";
	getFieldByID("frmAirport").action = "showAirport.action"
	setTabValues(screenId, valueSeperator, "strGridRow", strGridRow); //Save the grid Row
	Disable("btnSave",true);
	getFieldByID("frmAirport").submit();
	top[2].ShowProgress();
}

// on Add Button click
function addClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		if (isGroundServiceEnabled) {	
			sspopup1.CloseWindow(); 
		}
		clearInputControls();
		objCmb.arrData = getEditComboData();
		// set the default business system parameters
		getFieldByID("txtAirportCode").focus();
		setField("txtMinStopOvrTime", defaultMinStopOverTime);
		document.getElementById("radOnline1").checked = true;
		document.getElementById("radOnline2").checked = false;
		document.getElementById("rndLatitude1").checked = true;
		document.getElementById("rndLongitude1").checked = true;
		if (document.getElementById("radOnline1").checked == true) {

			Disable("btnDST", false);
		} else {
	
			Disable("btnDST",true);
		}	
		if (document.getElementById("radOnline1").checked == true) {

			Disable("btnCIT", false);
			Disable("btnTerminals",false);
		} else {
	
			Disable("btnCIT",true);
			Disable("btnTerminals",true);
		}	
		setField("hdnMode","ADD");
		getFieldByID("chkActive").checked=true;
		getFieldByID("chkManualCheckIn").checked=false;
		getFieldByID("chkOnlineCheckin").checked=false;	
		getFieldByID("chkMealNotification").checked=false;	
		getFieldByID("chkETLProcessEnabled").checked=false;
		getFieldByID("chkAutoFlownProcessEnabled").checked=false;
		getFieldByID("enableCFG").checked=false;	
		
		Disable("txtAirportCode",false);
		Disable("btnSave",false);	
		Disable("btnReset",false);		
		Disable("btnEdit",true);
		Disable("btnDelete",true);
		Disable("btnCIT",true);
		Disable("btnTerminals",true);
		Disable("btnAirportFD", true);		
		objOnFocus();
		setPageEdited(false, false);
		Disable("selLccPublishStatus",false);
		Disable("selCarrierCodes",false);
		if (blnEngVisible) {
			document.getElementById("chkIBEVisibililty").checked = true;
			document.getElementById("chkXBEVisibililty").checked = true;
		}
		document.getElementById("selLccPublishStatus").value = "TBP"
	}
}

// on Edit Button click
function editClick() {
	if (getFieldByID("txtAirportCode").value == ""
			|| getFieldByID("txtAirportCode").value == null) {
		showCommonError("Error", "Please select a airport to modify!");
		Disable("txtAirportCode", true);
		disableInputControls();
		return;
	}
	if (isGroundServiceEnabled) {	
		sspopup1.CloseWindow(); 
	}
	Disable("btnAirportFD", true);		
	objOnFocus();
	setField("hdnMode", "EDIT");
	setPageEdited(false, false);
	enableInputControls();

	// Check whether Airport is used. If so disable GMT offset setting.
	if (arrData[strGridRow][27] != null && arrData[strGridRow][27] != "") {
		Disable("selGmtOffSetAction", arrData[strGridRow][27]);
		Disable("txtGmtOffSetAction", arrData[strGridRow][27]);
	}

	Disable("txtAirportCode", true);
	getFieldByID("txtAirportName").focus();
	if((document.getElementById("radOnline1").checked == true)&&(getFieldByID("chkActive").checked==true)){
	
		Disable("btnDST",false);
	} else {

		Disable("btnDST", true);
	}
	if((document.getElementById("radOnline1").checked == true)&&(getFieldByID("chkActive").checked==true)){
		
		Disable("btnCIT",false);
	} else {

		Disable("btnCIT", true);
	}
	Disable("btnDelete", true);
	Disable("txtAirportCode", true);
	Disable("btnCIT", true);
	Disable("btnTerminals",true);
	if (document.getElementById("radOnline1").checked == false
			&& document.getElementById("radOnline2").checked == true) {
		objCmb.disable(true);
	}
	if (arrData[strGridRow][36] == "PUB"){
		document.getElementById("selLccPublishStatus").value = "TBR"; 
	}
	Disable("selLccPublishStatus",false);
	Disable("selCarrierCodes",false);
	
	if (getFieldByID('chkMealNotification').checked) {
		Disable("txtNotifyEmail",false);
		Disable("mealNotifyStartTime",false);
		Disable("notificationFrequency",false);
		Disable("lastNotifyTime",false);	
	} else {
		Disable("txtNotifyEmail",true);
		Disable("mealNotifyStartTime",true);
		Disable("notificationFrequency",true);
		Disable("lastNotifyTime",true);
	}
}

function resetAirport() {
	if (isGroundServiceEnabled) {	
		sspopup1.CloseWindow(); 
	}
	objOnFocus();

	getFieldByID("chkActive").checked = true;
	getFieldByID("chkManualCheckIn").checked = false;
	getFieldByID("chkOnlineCheckin").checked = false;
	getFieldByID("chkETLProcessEnabled").checked = false;
	getFieldByID("chkAutoFlownProcessEnabled").checked = false;
	

	setPageEdited(false, false);
	var strMode = getText("hdnMode");
	if (strMode == "ADD") {
		getFieldByID("txtAirportCode").focus();

		Disable("txtAirportCode", false);
		clearInputControls();

		// set the default business system parameters
		setField("txtMinStopOvrTime", defaultMinStopOverTime);
		document.getElementById("radOnline1").checked = true;
		document.getElementById("radOnline2").checked = false;
		Disable("btnAdd", false);
		Disable("btnSave", false);
		Disable("btnAirportFD", true);		
		Disable("btnReset", false);		
		Disable("btnEdit", true);		
		Disable("btnDelete", true);		
		setField("hdnMode","ADD");
		getFieldByID("chkActive").checked=true;
		getFieldByID("chkManualCheckIn").checked = false;
		getFieldByID("chkOnlineCheckin").checked = false;
		getFieldByID("chkActive").focus();
		document.getElementById("selLccPublishStatus").value = "TBP"
		Disable("selLccPublishStatus",false);
		Disable("selCarrierCodes",false);
		if (blnEngVisible) {
			document.getElementById("chkIBEVisibililty").checked = true;
			document.getElementById("chkXBEVisibililty").checked = true;
		}
	}

	if (strMode == "EDIT") {
		if (strGridRow < arrData.length
				&& arrData[strGridRow][1] == getText("txtAirportCode")) {
			setPageEdited(false, false);
			enableInputControls();
			getFieldByID("txtAirportName").focus();
			setField("txtAirportCode", arrData[strGridRow][1]);
			setField("txtAirportName", arrData[strGridRow][2]);
			setField("selFormStation", arrData[strGridRow][23]);

			if (arrData[strGridRow][22] == null
					|| arrData[strGridRow][22] == ""
					|| arrData[strGridRow][22] == "null") {
				setField("selClosestAirport", "-1");
			} else
				setField("selClosestAirport", arrData[strGridRow][22]);

			if (arrData[strGridRow][4] == "Online") {
				document.getElementById("radOnline1").checked = true;
				document.getElementById("radOnline2").checked = false;
			} else {
				document.getElementById("radOnline2").checked = true;
				document.getElementById("radOnline1").checked = false;
			}
			setField("chkActive", arrData[strGridRow][5]);
			if (arrData[strGridRow][5] == "Active"
					|| arrData[strGridRow][5] == "active")
				document.getElementById("chkActive").checked = true;
			else
				document.getElementById("chkActive").checked = false;
			setField("hdnVersion", arrData[strGridRow][7]);
			setField("txtContact", arrData[strGridRow][8]);
			setField("txtFax", arrData[strGridRow][9]);
			setField("selGmtOffSetAction", arrData[strGridRow][10]);
			setField("txtGmtOffSetAction", arrData[strGridRow][11]);
			if (arrData[strGridRow][13] == 'N')
				document.getElementById("rndLatitude1").checked = true;
			if (arrData[strGridRow][13] == 'S')
				document.getElementById("rndLatitude2").checked = true;
			setField("txtLatitude1", arrData[strGridRow][12].substring(0, 3));
			setField("txtLatitude2", arrData[strGridRow][12].substring(3, 6));
			setField("txtLatitude3", arrData[strGridRow][12].substring(6, 9));
			if (arrData[strGridRow][15] == 'E')
				document.getElementById("rndLongitude1").checked = true;
			if (arrData[strGridRow][15] == 'W')
				document.getElementById("rndLongitude2").checked = true;
			setField("txtLongitude1", arrData[strGridRow][14].substring(0, 3));
			setField("txtLongitude2", arrData[strGridRow][14].substring(3, 6));
			setField("txtLongitude3", arrData[strGridRow][14].substring(6, 9));
			setField("txtPhone", arrData[strGridRow][16]);
			setField("txtaRemarks", arrData[strGridRow][17]);
			setField("txtMinStopOvrTime", arrData[strGridRow][18]);
			objCmb.setText(arrData[strGridRow][31]);
			objCmb.setComboValue(arrData[strGridRow][30]);
			//ADDED NEW FIELDS IN AIRPORT
			setField("chkManualCheckIn", arrData[strGridRow][33]);
			if(arrData[strGridRow][33]=="Y" || arrData[strGridRow][33]==""){
				document.getElementById("chkManualCheckIn").checked = true;
			} else {
			    document.getElementById("chkManualCheckIn").checked = false;
			}
			
			if(arrData[strGridRow][50]=="Y"){
			    document.getElementById("chkETLProcessEnabled").checked = true;
			} else {
			    document.getElementById("chkETLProcessEnabled").checked = false;
			}
			
			if(arrData[strGridRow][51]=="Y"){
                document.getElementById("chkAutoFlownProcessEnabled").checked = true;
            } else {
                document.getElementById("chkAutoFlownProcessEnabled").checked = false;
            }
			
			
			setField("hdnLCCVisiblity",arrData[strGridRow][34]);
			setField("hdnCarrierCode",arrData[strGridRow][35]);
			
			deselectCarriers();
            setSelectedCarrierCodes(arrData[strGridRow][35]);
			
			setField("anciStartCutoverDay",arrData[strGridRow][37]);	
			setField("anciStartCutoverTime",arrData[strGridRow][38]);	
			setField("anciEndCutoverDay",arrData[strGridRow][39]);	
			setField("anciEndCutoverTime",arrData[strGridRow][40]);	
			
			setField("chkOnlineCheckin",arrData[strGridRow][41]);
			if(arrData[strGridRow][41]=="Y" || arrData[strGridRow][41]=="Y")
				document.getElementById("chkOnlineCheckin").checked = true;
			else
				document.getElementById("chkOnlineCheckin").checked = false;
			
			if (arrData[strGridRow][21] == "Yes"
					&& arrData[strGridRow][25] != null) {
				setField("lblStarts", arrData[strGridRow][24]);
				setField("lblEnds", arrData[strGridRow][25]);
			} else {
				setField("lblStarts", "");
				setField("lblEnds", "");
			}
			if (blnEngVisible) {
				if (arrData[strGridRow][28] == "Y") {
					getFieldByID("chkIBEVisibililty").checked = true;
				} else {
					getFieldByID("chkIBEVisibililty").checked = false;
				}
				if (arrData[strGridRow][29] == "Y") {
					getFieldByID("chkXBEVisibililty").checked = true;
				} else {
					getFieldByID("chkXBEVisibililty").checked = false;
				}
			}
			Disable("txtAirportCode", true);
			if (arrData[strGridRow][4] == "Online") {

				Disable("btnDST", false);
			} else {

				Disable("btnDST", true);
			}
			if (arrData[strGridRow][4] == "Online") {

				Disable("btnCIT", false);
				Disable("btnTerminals",false);
			} else {

				Disable("btnCIT", true);
				Disable("btnTerminals",true);
			}
			Disable("btnDelete", true);
			document.getElementById("selLccPublishStatus").value = arrData[strGridRow][36];
			if (arrData[strGridRow][36] == "PUB") {
				Disable("selLccPublishStatus", true);
			}
			setField("selFormCity", arrData[strGridRow][53]);
		} else {
			// Grid row not found. Clear it off.
			clearInputControls();
			disableInputControls();
			setPageEdited(false, false);
			// to set the Country after search
			if (strCountryCode != null && strCountryCode != "undefined") {
				selectChange(strCountryCode);
			}	

			Disable("btnEdit", true);		
			Disable("btnDelete", true);		
			Disable("btnSave", true);
			Disable("btnAirportFD", true);		
			Disable("btnReset", true);		
			Disable("btnDST", true);
			Disable("btnCIT", true);
			Disable("btnTerminals",true);
			getFieldByID("selCountryName").focus();	
			document.getElementById("rndLatitude1").checked = true;
			document.getElementById("rndLongitude1").checked = true;
			Disable("selLccPublishStatus",false);
			Disable("selCarrierCodes",false);
			if (blnEngVisible) {
				document.getElementById("chkIBEVisibililty").checked = true;
				document.getElementById("chkXBEVisibililty").checked = true;
			}
			document.getElementById("selLccPublishStatus").value = "TBP"
		}
	}

}

// on Delete Button click
function deleteClick() {
	if (isGroundServiceEnabled) {	
		sspopup1.CloseWindow(); 
	}
	var confirmStr = confirm(deleteRecoredCfrm);
	if (!confirmStr)
		return;
	if (strRowData[0] == null) {
		showCommonError("Error", "Please select a row to delete!");
	} else {
		enableInputControls();

		setField("txtAirportCode", strRowData[0]);
		setField("hdnMode", "DELETE");
		getFieldByID("frmAirport").target = "_self";
		getFieldByID("frmAirport").action = "showAirport.action"
		setTabValues(screenId, valueSeperator, "intLastRec", 1); // Delete
		setField("hdnRecNo", getTabValues(screenId, valueSeperator,
				"intLastRec"));
		getFieldByID("frmAirport").submit();
	}
}

function myRecFunction(intRecNo, intErrMsg) {
	top[2].HidePageMessage();
	if (intErrMsg != "") {
		top[2].objMsg.MessageText = intErrMsg;
		top[2].objMsg.MessageType = "Error";
		top[2].ShowPageMessage();
	}
}

// To Handle Paging
function gridNavigations(intRecNo, intErrMsg) {

	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setField("hdnMode", "PAGING");
		objOnFocus();
		if (intRecNo <= 0)
			intRecNo = 1;
		setTabValues(screenId, valueSeperator, "intLastRec", intRecNo);
		setField("hdnRecNo", getTabValues(screenId, valueSeperator,
				"intLastRec"));
		setField("hdnSearchData", getSearchData());
		if (intErrMsg != "") {
			top[2].objMsg.MessageText = "<li> " + intErrMsg;
			top[2].objMsg.MessageType = "Error";
			top[2].ShowPageMessage();
		} else {
			getFieldByID("frmAirport").target = "_self";
			document.forms[0].action = "showAirport.action";
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	}
}

function filterAirportCodeAndName() {	
	var tempAirArray = new Array();
	var airCount = 0;
	var strCountrycode;
	var strCountryName = objCmb1.getText();
	if(strCountryName != "ALL" && strCountryName.equals != ""){
		strCountrycode = objCmb1.getComboValue();
		for (var k = 0; k < airportArr.length; k++){
			if(strCountrycode == airportArr[k][2]){
				tempAirArray[airCount] = airportArr[k];
				airCount++;
			}			
		}
		objCmb2.arrData = begin.concat(tempAirArray);
		objCmb3.arrData = begin.concat(tempAirArray);
	} else {
		objCmb2.arrData = begin.concat(airportArr);		
		objCmb3.arrData = begin.concat(airportArr.sort(by(1)));
	}	
	
	var strAirportCode= objCmb2.getText();
	var tempANameArray = new Array();
	var airNCount = 0;
	if(strAirportCode != "ALL" && strAirportCode.equals != ""){
		for (var j = 0; j < airportArr.length; j++){
			if(strAirportCode == airportArr[j][0]){
				tempANameArray[airNCount] = airportArr[j];
				airNCount++;
			}			
		}		
		objCmb3.arrData = begin.concat(tempANameArray);
	} 	
}

function setAirportCodeAndName() {	
	if(objCmb1.selectedValue != ""){
		filterAirportCodeAndName();
	}			
	top[2].HidePageMessage();
}

function pageOnChange() {
	setPageEdited(true, false);
}

function setPageEdited(isEdited, isDst) {

	var isEitherOneEdited = false;

	if (isDst) {
		isAirportDstEdited = isEdited;
	} else {
		isAirportEdited = isEdited;
	}

	if (isAirportDstEdited || isAirportEdited) {
		isEitherOneEdited = true;
	}
	top[1].objTMenu.tabPageEdited(screenId, isEitherOneEdited);
}

function formatTime(objTxt) {
	var strTime = objTxt.value;
	if (strTime != null && strTime.length == 5 && strTime)
		var strTimes = strTime.split("-");
	setField("txtMinStopOvrTime", strTimes[0] + ":" + strTimes[1]);
}

function MinStopOvrTimePress(objTextBox) {
	objOnFocus();
	setPageEdited(true, false);
	pageOnChange();
	var strCR = getText("txtMinStopOvrTime");
	var strLen = strCR.length;
	var blnEmpty = isEmpty(strCR);
	if (blnEmpty) {
		setField("txtMinStopOvrTime", strCR.substr(0, strLen - 1));
		getFieldByID("txtMinStopOvrTime").focus();
	}
}
function checkMinStopOvrTime() {
	var strCR = getText("txtMinStopOvrTime");
	var blnVal = isTimeFormatValid("txtMinStopOvrTime");
	if (!blnVal) {
		return true;
	}
}

function GmtOffSetPress(objTextBox) {
	objOnFocus();
	setPageEdited(true, false);
	pageOnChange();
	var strCR = getText("txtGmtOffSetAction");
	var strLen = strCR.length;
	var blnEmpty = isEmpty(strCR);
	if (blnEmpty) {
		setField("txtGmtOffSetAction", strCR.substr(0, strLen - 1));
		getFieldByID("txtGmtOffSetAction").focus();
	}
}
function checkGmtOffSet() {
	var strCR = getText("txtGmtOffSetAction");
	var blnVal = IsValidTime(trim(strCR)); // 24 hour clock check
	if (!blnVal) {
		return true;
	}
}

function notifStartCutoverTimePress(objTextBox) {
	objOnFocus();
	setPageEdited(true, false);
	pageOnChange();
	var strCR = getText("anciStartCutoverTime");
	var strLen = strCR.length;
	var blnEmpty = isEmpty(strCR);
	if (blnEmpty) {
		setField("anciStartCutoverTime", strCR.substr(0, strLen - 1));
		getFieldByID("anciStartCutoverTime").focus();
	}
}
function checkNotifStartCutOverTime() {
	var strCR = getText("anciStartCutoverTime");
	var blnVal = isTimeFormatValid("anciStartCutoverTime");
	if (!blnVal) {
		return true;
	}
}


function notifEndCutoverTimePress(objTextBox) {
	objOnFocus();
	setPageEdited(true, false);
	pageOnChange();
	var strCR = getText("anciEndCutoverTime");
	var strLen = strCR.length;
	var blnEmpty = isEmpty(strCR);
	if (blnEmpty) {
		setField("anciEndCutoverTime", strCR.substr(0, strLen - 1));
		getFieldByID("anciEndCutoverTime").focus();
	}
}

function mealNotifyTiming(timeVal) {
	objOnFocus();
	setPageEdited(true, false);
	pageOnChange();
	var strCR = getText(timeVal);
	var strLen = strCR.length;
	var blnEmpty = isEmpty(strCR);
	if (blnEmpty) {
		setField(timeVal, strCR.substr(0, strLen - 1));
		getFieldByID(timeVal).focus();
	}
}

function checkNotifEndCutOverTime() {
	var strCR = getText("anciEndCutoverTime");
	var blnVal = isTimeFormatValid("anciEndCutoverTime");
	if (!blnVal) {
		return true;
	}
}


function isTimeFormatValid(objTime) {
	// Checks if time is in HH:MM format - HH can be more than 24 hours
	var timePattern = /^(\d{1,2}):(\d{2})$/;
	var matchArray = getValueTrimed(objTime).match(timePattern);
	if (matchArray == null) {
		return false;
	}
	var hour = matchArray[1];
	var min = matchArray[2];
	if ((min < 0) || (min > 59)) {
		return false;
	}
	if (hour.length < 2) {
		setField(objTime, "0" + getValueTrimed(objTime));
	}
	return true;
}

function rad1Checked() {
	objCmb.disable(false);
	document.getElementById("radOnline2").checked = false;
	if(document.getElementById("chkActive").checked){
		if(document.getElementById("selLccPublishStatus").value == "PUB"){
			document.getElementById("selLccPublishStatus").value = "TBP"
		} else {
			document.getElementById("selLccPublishStatus").value = "TBR"
		}
	} else {
		document.getElementById("selLccPublishStatus").value = "SKP"
	}
}

function rad2Checked() {
	Disable("btnDST", true);
	Disable("btnCIT", true);
	objCmb.disable(true);
	document.getElementById("radOnline1").checked = false;
	document.getElementById("selLccPublishStatus").value = "SKP"
}

function statusChange() {
	if(document.getElementById("chkActive").checked && document.getElementById("radOnline1").checked){
		if(document.getElementById("selLccPublishStatus").value == "PUB"){
			document.getElementById("selLccPublishStatus").value = "TBP"
		} else {
			document.getElementById("selLccPublishStatus").value = "TBR"
		}
	} else {
		document.getElementById("selLccPublishStatus").value = "SKP"
	}
}

function getValueTrimed(strValue) {
	return trim(getValue(strValue));
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function valAirportName(objTextBox) {
	setPageEdited(true, false);
	objOnFocus();
	commaValidate(objTextBox);
}
function valContact(objTextBox) {
	setPageEdited(true, false);
	objOnFocus();
	var strCC = objTextBox.value;
	var strLen = strCC.length;
	var blnVal = isEmpty(strCC);
	if (blnVal) {
		setField("txtContact", strCC.substr(0, strLen - 1));
		getFieldByID("txtContact").focus();
	} else
		alphaNumericValidate(objTextBox);
}

function formatTimeWithColon(strTime) {
	var timePattern = /^(\d{1,2}):(\d{2})$/;
	var matchArray = trim(strTime).match(timePattern);
	var hour = matchArray[1];
	var min = matchArray[2];
	if (hour.length < 2) {
		strTime = "0" + trim(strTime);
	}

	if (trim(strTime) != "") {
		var index = strTime.length - 2;
		var hourindex = strTime.search(":");
		if (hourindex != -1) {
			strTime = strTime;
		} else {
			var mn = "00";
			if (strTime.length == 3 || strTime.length == 4)
				mn = strTime.substr(index, 2);

			var hr = 0;
			if (strTime.length == 3) {
				hr = strTime.substr(0, 1);
			} else {
				hr = strTime.substr(0, 2);
			}
			var timecolon = hr + ":" + mn;
			strTime = timecolon;
		}
	}
	return strTime;
}

function beforeUnload() {
	if (!isAirportDstEdited) {
		if ((top[0].objWindow) && (!top[0].objWindow.closed) && 'SEARCH' != getText("hdnMode")) {
			top[0].objWindow.close();
		}
	} else if (!isAirportEdited && isAirportDstEdited) {
		var cfrm = confirm("Changes will be lost! Do you wish to Continue?");
		if (cfrm) {
			setPageEdited(false, false);
			isAirportEdited = false;
			isAirportDstEdited = false;
			
			
			if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
				top[0].objWindow.close();
			}
		} else {
			top[0].objWindow.focus();
		}
	}
}

function closeParentAndChildWindow() {
	if (isAirportEdited) {
		top[1].objTMenu.tabRemove(screenId);
		beforeUnload();
	} else if (isAirportDstEdited) {
		beforeUnload();
	} else {
		setPageEdited(false, false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function resetVariables() {
	setPageEdited(false, false);
	isAirportEdited = false;
	isAirportDstEdited = false;	
	setPageEdited(false, false);
}

function writeVisibilityTable() {
	var strHTMLText = "";
	strHTMLText += '<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table10" align="center"';
	strHTMLText += '<tr>';
	strHTMLText += '	<td width="30%"><font>IBE Visibility</font></td>';
	strHTMLText += '	<td width="20%"><input type="checkbox" name= "chkIBEVisibililty" id="chkIBEVisibililty" value="" onClick="pageOnChange()" tabindex="25" class="NoBorder"></td>';
	strHTMLText += '	<\/td>';
	strHTMLText += '	<td width="30%"><font>XBE Visibility</font></td>';
	strHTMLText += '	<td><input type="checkbox" name= "chkXBEVisibililty" id="chkXBEVisibililty" value="" onClick="pageOnChange()" tabindex="26" class="NoBorder"></td>';
	strHTMLText += '	<\/td>';
	strHTMLText += '<\/tr>';
	strHTMLText += '<\/table>';
	DivWrite("spnVisibility", strHTMLText);
}
if(blnEngVisible)
	writeVisibilityTable();
function PopUpData(){
	var html = '<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0"> <tr><td> <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4"> <tr><td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td> <td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader"> Airport Name in Other Languages </font></td> <td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td> </tr><tr><td class="FormBackGround"></td><td class="FormBackGround" valign="top"> <table><tr><td width="663" align="left"><table width="98%" height="105"  border="0" cellpadding="0" cellspacing="0"> <tr align="left" valign="middle" class="fntMedium"> <td width="19%" class="fntMedium"><font>Language</font></td> <td width="33%" align="left" style="padding-bottom:3px; ">';
		html += '<select name="language" id="language"  style="width:150px; "> ';
		html += '<option value="-1"></option>'+languageHTML;
		html += '</select></td> <td width="13%" rowspan="2" align="center" valign="top"><input name="add" type="button" class="Button" id="add" style="width:50px; " title="Add Item"  onclick="addAirport();" value="&gt;&gt;" /> <input name="remove" type="button" class="Button" id="remove" style="width:50px;" title="Remove Item" onclick="removeAirport();" value="&lt;&lt;" /></td> <td width="35%" rowspan="2" align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0"> <tr> <td><select name="translation" size="5" id="translation" style="width:175px; height:100px; "> </select></td> </tr> <tr> </tr> </table></td> </tr> <tr align="left" valign="middle" class="fntMedium"> <td height="67" valign="top" class="fntMedium"><font>Airport Name* </font></td> <td valign="top"><input name="txtAirportNameOL" type="text" id="txtAirportNameOL" size="32" maxlength="255" /></td> </tr> </table></td> </tr> <tr><td align="top"><span id="spnSta"></span></td></tr> <tr><td align="right"><input type="button" id="btnUpdate" value= "Save" class="Button" onClick="saveAirportTranslation();"> &nbsp;<input type="button" id="btnPopupClose" value= "Close" class="Button" onClick="return POSdisable(true);"></td></tr></table> </td><td class="FormBackGround"></td></tr><tr> <td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td> <td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td> <td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td> </tr></table> </td></tr></table>';

	DivWrite("spnPopupData",html);
}

function POSenable(){
	if(!listForDisplayEnabled)
		return;
	if(getFieldByID("txtAirportCode").value=="" || getFieldByID("txtAirportCode").value==null){
			alert("No airport is selected, please select airport and try again");
			return;
		}
		setField("hdnMode", "SAVELISTFORDISP");
		//setAirpotTranslationList();
		showPopUp(1);
}

function POSdisable(bFromClose){
	if(!listForDisplayEnabled)
		return;
	if(bFromClose)
		setField("hdnMode","");
	 hidePopUp(1);
}

function addAirport(){
	if(!listForDisplayEnabled)
		return;
	if(getValue('txtAirportNameOL').length ==0){
		return false;
	}

	var selLang = getValue("language");
	if(selLang == '-1')
		return false;
	
	if(findValueInList(selLang, "translation") == true){
		alert("Airport name for the selected language already exist");
		return false;
	}

	var selLangName = getCurSelectedLabel("language");
	var airportName = getValue("txtAirportNameOL");
	addToList("translation", selLangName+"=>"+airportName, selLang);	
	setField("txtAirportNameOL", "");
	setField("language","-1");
}

function removeAirport(){
	if(!listForDisplayEnabled)
		return;
	var airportList = getFieldByID("translation");
	var selected = airportList.selectedIndex;
	if(selected != -1){
		var langCode = airportList.options[selected].value;
		var label = airportList.options[selected].text.split("=>");
		setField("language", langCode);
		setField("txtAirportNameOL", label[1]);
		airportList.remove(selected);
	}
}
function saveAirportTranslation(){
	if(!listForDisplayEnabled)
		return;
	if(getValue('txtAirportNameOL').length>0 && getValue("language")!='-1'){
		if(confirm("The current edited language is not added to the list, do you want to continue?")==false)
			return false;
	}

	var airportList = getFieldByID("translation");
	var size = airportList.length;
	var langCode;
	var airportName;
	var label;
	var str="";
	var size;
	var i=0;
	var j=0;
	
	var newData = new Array();
	var count = 0;
	for(i=0;i<airportList.length;i++){
		langCode = airportList.options[i].value;
		label = airportList.options[i].text.split("=>");
		airportName = label[1];
		size = arrData[strGridRow][32].length;
		var found = false;
		for(j=0;j<size;j++){
			if(arrData[strGridRow][32][j][0] == langCode){
				newData[count++] = new Array(langCode, airportName, arrData[strGridRow][32][j][2], arrData[strGridRow][32][j][3]);
				found = true;
				break;
			}
		}
		if(!found){
			newData[count++] = new Array(langCode, airportName, "-1", "-1");//langCode,airportName,id,ver
			//arrData[strGridRow][32][size] = new Array(langCode,airportName,'-1','-1');//langCode,airportName,id,ver
		}
	}
	var langCode2;
	var size1 = arrData[strGridRow][32].length;
	var size2 = newData.length;
	for(i=0;i<size1;i++){//original list
		found = false;
		for(j=0;j<size2;j++){//new data
			langCode = arrData[strGridRow][32][i][0];
			langCode2 = newData[j][0];
			if(langCode == langCode2){//the lang code exist in the new data
				found = true;
			}
		}
		if(!found){
			newData[newData.length] = new Array(langCode,"null",arrData[strGridRow][32][i][2],arrData[strGridRow][32][i][3]);
		}
	}
	arrData[strGridRow][32] = newData;
	saveAirport();
	return POSdisable(false);
}

function setHdnAirportForDisplay(){
	if(!listForDisplayEnabled)
		return;
	var strMode = getText("hdnMode");
	var airportList = getFieldByID("translation");
	var langCode;
	var airportName;
	var label;
	var ver;
	var id;
	var str="";
	var i=0;
	if(strMode == "ADD"){
		airportName = getUnicode(getValue("txtAirportName"));
		var size2=getFieldByID("language").length;
		for(i=1;i<size2;i++){
			langCode = getFieldByID("language").options[i].value;
			if(str.length>0)
				str += "|";
			str=str + langCode+"^"+airportName+"^-1^-1^"+getValue("txtAirportCode");
		}
	}else{
		var size = arrData[strGridRow][32].length;
		for(i=0;i<size;i++){
			langCode = arrData[strGridRow][32][i][0];
			label = arrData[strGridRow][32][i][1];
			airportName = getUnicode(label);
			id = arrData[strGridRow][32][i][2];
			ver=arrData[strGridRow][32][i][3];
			if(str.length>0)
				str += "|";
			str=str + langCode+"^"+airportName+"^"+id+"^"+ver+"^"+arrData[strGridRow][1];
		}
	}
	setField("hdnAirportForDisplay", str);
}
function setAirpotTranslationList(){
	if(!listForDisplayEnabled)
		return;
	if(strGridRow>=0 ){
		setField("txtAirportNameOL", "");
		setField("language","-1");
		var airportList = getFieldByID("translation");
		airportList.options.length = 0;
		var langCode ;
		var airportName ;
		var langName;
		var i=0;
		for(i=0;i<arrData[strGridRow][32].length;i++){
			langCode = arrData[strGridRow][32][i][0];
			airportName = arrData[strGridRow][32][i][1];
			if(airportName == "null")
				continue;
			langName = getListLabel("language", langCode);
			addToList("translation", langName+"=>"+airportName, langCode);
		}
	}
}
function validateAirportTranslation(){
	if(!listForDisplayEnabled)
		return;
	var strMode = getText("hdnMode");
	var size2=getFieldByID("language").length;
	var valid = true;
	var size1;
	var i;
	var j;
	var langCode;
	var currName;
	var found = false;
	if(strMode == "ADD")
		valid = false;
	else{
		size1 = arrData[strGridRow][32].length;
		if(size1 != (size2-1))//-1 to ignore the first empty item
			valid = false;
		else{
			for(i=0;i<size1;i++){//original list
				airportName = arrData[strGridRow][32][i][1];
				if(airportName == "null"){
					valid = false;
					break;
				}
			}
		}
	}
	if(!valid){
		alert("Airport name for display is missing for all or some languages, original airport name will copied to all languages") ;
		if(strMode != "ADD"){
				var descr = getValue("txtAirportName");
				for(i=1;i<size2;i++){//lang  list, start from 1 to ignore the first item (empty)
					found = false;
					langCode = getFieldByID("language").options[i].value;
					for(j=0;j<size1;j++){
						airportName = arrData[strGridRow][32][j][1];
						if(airportName == "null"){
							arrData[strGridRow][32][j][1] = descr;
						}
						if(langCode == arrData[strGridRow][32][j][0]){
							found = true;
							continue;
						}
					}
					if(!found)
						arrData[strGridRow][32][arrData[strGridRow][32].length] = new Array(langCode,descr,"-1","-1");
				}
		}
	}
	return true;
}

buildCountryCombos(arrCountry);

function buildCountryCombos(arrName) {	
	objCmb1.id = "selCountryName";
	objCmb1.top = "26";
	objCmb1.left = "70";
	objCmb1.width = "165";
	objCmb1.textIndex = 1;
	objCmb1.valueIndex = 0;
	objCmb1.tabIndex = 1;
	objCmb1.selectedText = "ALL";
	objCmb1.selectedValue = "*";
	objCmb1.arrData = begin1.concat(arrName);
	objCmb1.onChange = "setAirportCodeAndName";	
}

buildAirportCodeCombos(airportArr);

function buildAirportCodeCombos(arrName) {	
	objCmb2.id = "selAirportCode";
	objCmb2.top = "26";
	objCmb2.left = "360";
	objCmb2.width = "100";
	objCmb2.textIndex = 0;
	objCmb2.valueIndex = 0;
	objCmb2.tabIndex = 1;
	objCmb2.selectedText = "ALL";
	objCmb2.selectedValue = "*";
	objCmb2.arrData = begin.concat(arrName); // add "ALL" element begin of the list	
	objCmb2.onChange = "filterAirportCodeAndName";	
	
	
}

buildAirportNameCombos(airportArr);

function buildAirportNameCombos(arrName) {		
	objCmb3.id = "selAirportName";
	objCmb3.top = "26";
	objCmb3.left = "580";
	objCmb3.width = "215";
	objCmb3.textIndex = 1;
	objCmb3.valueIndex = 1;
	objCmb3.tabIndex = 1;
	objCmb3.selectedText = "ALL";
	objCmb3.selectedValue = "*";
	objCmb3.arrData = begin.concat(airportArr.sort(by(1)));
	objCmb3.onChange = "filterAirportCodeAndName";
	objCmb3.buildCombo();	
}

function by(i) {
	return function(a,b){a = a[i];b = b[i];return a.toLowerCase() == b.toLowerCase() ? 0 : (a.toLowerCase() < b.toLowerCase() ? -1 : 1)}
	}

function setSelectedCarrier(code){
	
	if(getFieldByID("selCarrierCodes")){
		var intCount = getFieldByID("selCarrierCodes").length ;
		 for (var i = 0; i < intCount; i++){
		 	if (trim(getFieldByID("selCarrierCodes").options[i].value)==trim(code)){
		   	  getFieldByID("selCarrierCodes").options[i].selected=true;
		   	}
		}
	}	
}

function setSelectedCarrierCodes(codes){
    var arrCarriers = codes.split(",");
    for(var i=0;i<arrCarriers.length;i++){               
            setSelectedCarrier(arrCarriers[i]);                   
    }
}

function deselectCarriers(){
	if(getFieldByID("selCarrierCodes")){
		var intCount = getFieldByID("selCarrierCodes").length ;
		 for (var i = 0; i < intCount; i++){
		   	  getFieldByID("selCarrierCodes").options[i].selected=false;
		}		
	}	
}

function isSelectCarriers(){
	if(getFieldByID("selCarrierCodes")){
		var intCount = getFieldByID("selCarrierCodes").length ;
		 for (var i = 0; i < intCount; i++){
		   	  if(getFieldByID("selCarrierCodes").options[i].selected){
		   	  	return true;
		   	  }
		}
	}
	return false;
}

function setDefaultStartCutoverTime() {
	if(getFieldByID("anciStartCutoverTime").value == ''){
		setField("anciStartCutoverTime", "00:00");
	}
}


function setDefaultEndCutoverTime() {
	if(getFieldByID("anciEndCutoverTime").value == ''){
		setField("anciEndCutoverTime", "00:00");
	}
}

function getStrtCutOverInMiliSec(){	
	var strtCutDate = new Date();	
	var startCutOverDays = getFieldByID("anciStartCutoverDay").value;	
	var startCutOverTime = getFieldByID("anciStartCutoverTime").value;	
	var startCutOverTimeArr = startCutOverTime.split(":");	
	var totHours = (Number(startCutOverDays*24) + Number(startCutOverTimeArr[0]));	
	strtCutDate.setHours(totHours);
	strtCutDate.setMinutes(startCutOverTimeArr[1]);
	strtCutDate.setSeconds(0,0);
	return strtCutDate.getTime();
}

function getEndCutOverInMiliSec(){	
	var endCutDate = new Date();	
	endCutOverDays = getFieldByID("anciEndCutoverDay").value;	
	var endCutOverTime = getFieldByID("anciEndCutoverTime").value;	
	var endCutOverTimeArr = endCutOverTime.split(":");	
	var totalHour = (Number(endCutOverDays*24) + Number(endCutOverTimeArr[0]));	
	endCutDate.setHours(totalHour);	
	endCutDate.setMinutes(endCutOverTimeArr[1]);
	endCutDate.setSeconds(0,0);
	return endCutDate.getTime();
}

function validateCutOverStartEndTime(){	
	if(getStrtCutOverInMiliSec() < getEndCutOverInMiliSec()){
		return true;
	}
}

function changeConnectingAirportStatus(){
	if(getFieldByID('chkSurfaceSegment').checked){
		Disable('selConnectingAirport', false);
	} else {
		setField('selConnectingAirport', '');
		Disable('selConnectingAirport', true);
	}
}

function valueValidate(objControl) {
	setPageEdited(true);
	if (objControl.value == null || objControl.value == "") {
		return false;
	}
	return positiveWholeNumberValidate2(objControl);
}

function valueValidateWithoutColon(objControl) {
	setPageEdited(true);
	if (objControl.value == null || objControl.value == "") {
		return false;
	}
	return positiveWholeNumberValidate(objControl);
}

function validateBillingEmail(){
	setPageEdited(true);
	objOnFocus();
	var tempVar;
	var mailaddrs = trim(getText("txtNotifyEmail"));
	if(mailaddrs.indexOf(",") != -1) {
		var mailarray = mailaddrs.split(",");
		for(var i=0;i < mailarray.length; i++) {
			tempVar = checkEmail(mailarray[i]);
			if(!tempVar) break;
			tempVar = checkEmailFormat(mailarray[i]);
		}
	}else {
		tempVar = checkEmail(trim(getText("txtNotifyEmail")));
		if (tempVar) {
			tempVar = checkEmailFormat(trim(getText("txtNotifyEmail")));
		}	
	}	
	if(!tempVar){
		setField("txtNotifyEmail","");
		getFieldByID("txtNotifyEmail").focus();		
	}
	return tempVar;
}

function checkEmailFormat(email) {
	var status = true;
	var mailWords = email.split("@");
	for (var i=0;i < mailWords.length; i++) {
		var wordLength = mailWords[i].length;
		if (trim(mailWords[i]).charAt(0) == "." || trim(mailWords[i]).charAt(wordLength-1) == ".") {
			status = false;
		}
	}
	return status;
}

function showSubStations(){
	var ssCount = arrData[strGridRow][44].length;
	if( ssCount== 0){
		alert("No sub Stations avaliable for selected airport.");
	} else {
		subStationPopupData();
		for(var i=0;i<ssCount;i++){
			var ssObj = arrData[strGridRow][44][i];
			ssHtmlStr += '<tr><td width="10"></td><td class="fntMedium" align="left"><font>' + ssObj[0] + '</font></td>';
			ssHtmlStr += '<td class="fntMedium" align="left"><font>' + ssObj[1] + '</font></td>';
			ssHtmlStr += '<td class="fntMedium" align="left"><font>' + ((ssObj[3]=='ACT')?'Active':'In-Active') + '</font></td><td width="11"></td></tr>';
		}
		DivWrite("ssPopupData",ssHtmlStr);
		sspopup1.OpenWindow();
	}
}

function subStationPopupData(){
	ssHtmlStr = '<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0"><tr><td> <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table5">';
	ssHtmlStr += '<tr><td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td><td width="30%" height="20" class="FormHeadBackGround" align="left"><font class="FormHeader">Sub Station Code</font></td><td width="50%" height="20" class="FormHeadBackGround" align="left"><font class="FormHeader">Description</font></td>';
	ssHtmlStr += '<td width="20%" height="20" class="FormHeadBackGround" align="left"><font class="FormHeader">Status</font></td><td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td></tr>';
}
