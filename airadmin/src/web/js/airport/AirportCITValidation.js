//On Page loading make sure that all the page input fields are cleared
var screenId = "SC_ADMN_004";
var valueSeperator = "~";

var rowNo;
var strRowData;
var isLoadCal = "NO";
var isChangedDST = "NO";
var btnSeq = "btnAdd,btnEdit,btnDelete";

function winOnLoad(strMsg, strMsgType) {
	
	clearAirportCit();
	disableInputControls();
	opener.setPageEdited(false, true);
	buttonSetFocus("btnAdd", btnSeq);
	if (strMsg != null && strMsgType != null)
		showWindowCommonError(strMsgType, strMsg);

	Disable("btnEdit", true);
	Disable("btnDelete", true);
	Disable("selCarrierCode", true);
	Disable("txtFlightNo", true);
	Disable("txtCIT", true);
	Disable("txtCIClosingTime", true);
	
	
	var values = opener.getTabValues(screenId, valueSeperator,
			"airportDstCompination");
	values = values.split(",");
	var airportCode = values[0];
	setField("hdnAirportCode",airportCode);

	document.getElementById("spnCITHeader").innerHTML = airportCode;
			

	

	if (arrFormData != null && arrFormData.length > 0) {

		opener.setPageEdited(true, true);
		enableInputControls();
		isLoadCal = "YES";
		
		if (isAddMode) {
			setField("hdnMode", "ADD");
			
		} else {
			setField("hdnMode", "EDIT");
			// If date less or equal to current date, Disable Start date/time in
			// edit mode
			
			Disable("btnEdit", false);
			
			setField("hdnVersion", prevVersion);
			setField("hdnCITId", prevCITId);
			setField("hdnGridRow", prevGridRow);
			strGridRow = prevGridRow;
		}
	}
	if(isOnlyOneCarrier()){
		getFieldByID("selCarrierCode").options[1].selected=false;
	   	Disable("selCarrierCode", true);
	}
}



function validateAirportCit() {
	
	if ( isFlightNoAndCarrierCodeNotNull() && getValue("selCarrierCode") != getValue("txtFlightNo").substring(0,2)) {
		
		showWindowCommonError("Error", airportCitCarrierFlightInvalid);
		getFieldByID("selCarrierCode").focus();
		return false;
	
	}	
	else if ((getValue("txtCIT") == null || getValue("txtCIT") == "") && (getValue("txtCIClosingTime") == null || getValue("txtCIClosingTime") == "")) {
		showWindowCommonError("Error", airportCitRqrd);
		getFieldByID("txtCIT").focus();
		return false;
	}
	//else if(!isNumeric( getValue("txtCIT") )){
	//	showWindowCommonError("Error", airportCitInvalid);
	//	getFieldByID("txtCIT").focus();
	//	return false;
	//}
	else if ((getValue("txtCIT") != null && getValue("txtCIT") != "") && checkCheckInTime()) {
		showWindowCommonError("Error", airportCitInvalid);
		getFieldByID("txtCIT").focus();
		return false;
	}
	else if ((getValue("txtCIClosingTime") != null && getValue("txtCIClosingTime") != "") && checkCheckInClosingTime()) {
		showWindowCommonError("Error", airportCIClosingTimeInvalid);
		getFieldByID("txtCIClosingTime").focus();
		return false;
	}
	else if (compareTime(getValue("txtCIClosingTime"), getValue("txtCIT"))<= 0) {
		showWindowCommonError("Error",airportCIFromTimeGraterThanToTime );
		getFieldByID("txtCIClosingTime").focus();
		return false;
	}
	//else if ((getValue("selCarrierCode") == null || getValue("selCarrierCode") == "") && getValue("txtFlightNo") != "") {
	//	showWindowCommonError("Error", airportCarrierCodeRqrd);
	//	getFieldByID("selCarrierCode").focus();
	//	return false;
	//}
	else if (getFieldByID("hdnMode").value == "ADD" || getFieldByID("hdnMode").value == "EDIT") {
		for ( var i = 0; i < arrData.length; i++) {                        
			if(getFieldByID("hdnMode").value == "EDIT" && rowNo != i){
				if (arrData[i][1] == getFieldByID("selCarrierCode").value && arrData[i][2] == getFieldByID("txtFlightNo").value) {
                                        showWindowCommonError("Error", airportCitCarrierFlightExists);
				        getFieldByID("selCarrierCode").focus();
				        return false;
				}
			}
			else if (getFieldByID("hdnMode").value == "ADD" && (arrData[i][1] == getFieldByID("selCarrierCode").value && arrData[i][2] == getFieldByID("txtFlightNo").value)) {                               
				showWindowCommonError("Error", airportCitCarrierFlightExists);
				getFieldByID("selCarrierCode").focus();
				return false;
			}
		}
		return true;
	}
	else{
		return true;
	}
}

function isFlightNoAndCarrierCodeNotNull(){
	if((getValue("selCarrierCode") != null && getValue("selCarrierCode") != "") && (getValue("txtFlightNo") != null && getValue("txtFlightNo") != "")){
		return true;
	}
	else{
		return false;
	}
	
}
function clearAirportCit() {
	setField("hdnVersion", "");
	setField("selCarrierCode", "");
	setField("txtFlightNo", "");
	setField("txtCIT", "");
	setField("txtCIClosingTime", "");
	setField("hdnCITId", "");
	
	
}
function CheckInTimePress(objTextBox) {
	objOnFocus();
	opener.setPageEdited(true, false);
	pageOnChange();
	var strCR = getText("txtCIT");
	var strLen = strCR.length;
	var blnEmpty = isEmpty(strCR);
	if (blnEmpty) {
		setField("txtCIT", strCR.substr(0, strLen - 1));
		getFieldByID("txtCIT").focus();
	}
}
function CheckInClosingTimePress(objTextBox) {
	objOnFocus();
	opener.setPageEdited(true, false);
	pageOnChange();
	var strCR = getText("txtCIClosingTime");
	var strLen = strCR.length;
	var blnEmpty = isEmpty(strCR);
	if (blnEmpty) {
		setField("txtCIClosingTime", strCR.substr(0, strLen - 1));
		getFieldByID("txtCIClosingTime").focus();
	}
}
function checkCheckInTime() {
	var strCR = getText("txtCIT");
	var blnVal = isTimeFormatValid("txtCIT");
	if (!blnVal) {
		return true;
	}
}

function checkCheckInClosingTime() {
	var strCR = getText("txtCIClosingTime");
	var blnVal = isTimeFormatValid("txtCIClosingTime");
	if (!blnVal) {
		return true;
	}
}

function isTimeFormatValid(objTime) {
	// Checks if time is in HH:MM format - HH can be more than 24 hours
	var timePattern = /^(\d{1,2}):(\d{2})$/;
	var matchArray = getValueTrimed(objTime).match(timePattern);
	if (matchArray == null) {
		return false;
	}
	var hour = matchArray[1];
	var min = matchArray[2];
	if ((min < 0) || (min > 59)) {
		return false;
	}
	if (hour.length < 2) {
		setField(objTime, "0" + getValueTrimed(objTime));
	}
	return true;
}
function disableInputControls() {
	
	Disable("btnSave", true);
	Disable("btnReset", true);

}

function enableInputControls() {
	
	Disable("btnSave", false);
	Disable("btnReset", false);
	Disable("selCarrierCode", false);
	Disable("txtFlightNo", false);
	Disable("txtCIT", false);
	Disable("txtCIClosingTime", false);

}

// on Grid Row click
function RowClick(strRowNo) {
	objOnFocus();
	if (chkChanges()) {

		rowNo = strRowNo;
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;
		setField("hdnGridRow", strRowNo);
		setField("hdnCITId", arrData[strRowNo][5]);
		
		setField("selCarrierCode", strRowData[0]);
		setField("txtFlightNo", strRowData[1]);
		setField("txtCIT", strRowData[2]);
		setField("txtCIClosingTime", strRowData[3]);
		
		document.getElementById("btnSave").value = "Save";
		
		
		setField("hdnVersion", arrData[strRowNo][4]);
		
		
		Disable("btnAdd", false);
		Disable("btnEdit", false);
		Disable("btnDelete", false);
		opener.setPageEdited(false, true);
		disableInputControls();
		
	}
}
function isOnlyOneCarrier(){
	if(getFieldByID("selCarrierCode")){
		var intCount = getFieldByID("selCarrierCode").length ;
		if(intCount == 2){
			return true;		   	  
		}		
	}
}
function saveAirportCit() {
	if (!validateAirportCit())
		return;
	enableInputControls();
	
	var strAirportCode = opener.document.getElementById("txtAirportCode").value;
	
	isChangedDST = "YES";
	document.forms[0].action = "showAirportCIT.action?txtAirportCode="
			+ strAirportCode;
	document.forms[0].submit();
	Disable("chkStatus", false);
}

function resetAirportCit() {
	opener.setPageEdited(false, true);
	
	var strMode = getText("hdnMode");
	document.getElementById("btnSave").value = "Save";
	
	isLoadCal = "YES";
	if (strMode == "ADD") {
		clearAirportCit();
		getField("selCarrierCode").focus();
		
	}
	if (strMode == "EDIT") {		
		clearAirportCit();
		disableInputControls();
		opener.setPageEdited(false, true);

		buttonSetFocus("btnAdd", btnSeq);

		Disable("btnEdit", true);		
	}
	Disable("btnAdd", false);
	objOnFocus();
}

function addClick() {
	if (chkChanges()) {
		enableInputControls();
		clearAirportCit();
		opener.setPageEdited(false, true);
		
		Disable("btnEdit", true);
		Disable("btnDelete", true);
		Disable("btnSave", false);
		Disable("btnReset", false);
		Disable("selCarrierCode", false);
		Disable("txtFlightNo", false);
		Disable("txtCIT", false);
		Disable("txtCIClosingTime", false);
		setField("hdnMode", "ADD");
		isLoadCal = "YES";
		setField("hdnCITId", 0);		
		document.getElementById("btnSave").value = "Save";
	}
	objOnFocus();
	if(isOnlyOneCarrier()){
		getFieldByID("selCarrierCode").options[1].selected=false;
	   	Disable("selCarrierCode", true);
	}
}
function editClick() {
	objOnFocus();
	
	//enableInputControls();

	Disable("btnSave", false);
	Disable("btnReset", false);	
	Disable("txtCIT", false);
	Disable("txtCIClosingTime", false);
	Disable("selCarrierCode", false);
	Disable("txtFlightNo", false);
	setField("hdnMode", "EDIT");	
	document.getElementById("btnSave").value = "Save";
	if(isOnlyOneCarrier()){
		getFieldByID("selCarrierCode").options[1].selected=false;
	   	Disable("selCarrierCode", true);
	}
}
function deleteClick() {
	
	var confirmStr = confirm(deleteRecoredCfrm);
	if (!confirmStr)
		return;
	if (strRowData[0] == null) {
		showCommonError("Error", "Please select a row to delete!");
	} else {
		enableInputControls();

		var strAirportCode = opener.document.getElementById("txtAirportCode").value;
		setField("hdnMode", "DELETE");
		document.forms[0].target = "_self";
		
		document.forms[0].action = "showAirportCIT.action?txtAirportCode="
				+ strAirportCode;
		
		
		document.forms[0].submit();
	}
}



function objOnFocus() {
	HidePageMessage();
}


function pageOnChange() {
	opener.setPageEdited(true, true);
}
function chkChanges() {
	// if (opener.top.pageEdited) {
	if (opener.top[1].objTMenu.tabGetPageEidted(screenId)) {
		return confirm("Changes will be lost! Do you wish to Continue?");
	} else {
		return true;
	}
}



function getValueTrimed(strValue) {
	return trim(getValue(strValue));
}



function windowclose() {
	if (opener.isAirportDstEdited) {
		// opener.closeParentAndChildWindow();
		var cfrm = confirm("Changes will be lost! Do you wish to Continue?");
		if (cfrm) {
			opener.setPageEdited(false, false);
			opener.isAirportEdited = false;
				
			window.close();
		}
	} else {		
		window.close();
	}
}



