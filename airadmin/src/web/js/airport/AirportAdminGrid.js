var screenId = "SC_ADMN_004";
var valueSeperator = "~";

if (getTabValues(screenId, valueSeperator, "intLastRec") == "") {
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
}

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "7%";
objCol1.arrayIndex = 1;
objCol1.headerText = "Airport Code";
objCol1.itemAlign = "left"
objCol1.sort = true;

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "22%";
objCol2.arrayIndex = 2;
objCol2.headerText = "Airport Name";
objCol2.itemAlign = "left"
objCol2.sort = true;

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "7%";
objCol3.arrayIndex = 26;
objCol3.headerText = "GMT Offset";
objCol3.itemAlign = "center"

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "7%";
objCol4.arrayIndex = 23;
objCol4.headerText = "Station";
objCol4.itemAlign = "left"

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "7%";
objCol5.arrayIndex = 4;
objCol5.headerText = "Type";
objCol5.itemAlign = "left"

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "7%";
objCol6.arrayIndex = 5;
objCol6.headerText = "Status";
objCol6.itemAlign = "left"

var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "7%";
objCol7.arrayIndex = 21;
objCol7.headerText = "DST";
objCol7.itemAlign = "left"

var objCol8 = new DGColumn();
objCol8.columnType = "label";
objCol8.width = "25%";
objCol8.arrayIndex = 31;
objCol8.headerText = "GoShow Agent";
objCol8.itemAlign = "left"

var objCol9 = new DGColumn();
objCol9.columnType = "label";
objCol9.width = "3%";
objCol9.arrayIndex = 29;
objCol9.headerText = "XBE";
objCol9.itemAlign = "left"

var objCol10 = new DGColumn();
objCol10.columnType = "label";
objCol10.width = "3%";
objCol10.arrayIndex = 28;
objCol10.headerText = "IBE";
objCol10.itemAlign = "left"
	
var objCol11 = new DGColumn();
objCol11.columnType = "label";
objCol11.arrayIndex = 35;
objCol11.headerText = "Carriers";
objCol11.itemAlign = "left"

// ---------------- Grid
var objDG = new DataGrid("spnAirports");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol6);
objDG.addColumn(objCol7);
objDG.addColumn(objCol8);
objDG.addColumn(objCol9);
objDG.addColumn(objCol10);
objDG.addColumn(objCol11);

objDG.width = "99%";
objDG.height = "143px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = arrData;
objDG.seqNo = true;
objDG.seqStartNo = getTabValues(screenId, valueSeperator, "intLastRec"); // Grid
objDG.pgnumRecTotal = totalRecords; // remove as per return record size
objDG.paging = true
objDG.pgnumRecPage = 20;
objDG.rowClick = "RowClick";
objDG.pgonClick = "gridNavigations";
objDG.displayGrid();
objDG.shortCutKeyFunction = "Body_onKeyDown";
