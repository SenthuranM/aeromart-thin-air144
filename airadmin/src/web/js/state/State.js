

var stateScreenId = "SC_ADMN_028";

jQuery(document).ready(function(){  //the page is ready				
			
			$("#divSearch").decoratePanel("Search State");
			$("#divResultsPanel").decoratePanel("State");
			$("#divDispState").decoratePanel("Add/Modify State");			
			$('#btnAdd').decorateButton();
			$('#btnEdit').decorateButton();
			$('#btnDelete').decorateButton();
			$('#btnClose').decorateButton();
			$('#btnReset').decorateButton();
			$('#btnSave').decorateButton();
			$('#btnSearch').decorateButton();
			disableStateButton();
			enableSearch();	
			
			var grdArray = new Array();	
			jQuery("#listState").jqGrid({ 
				url:'showState!searchState.action',
				datatype: "json",
				postData: {
					selCountry: function() { return $("#selCountry").val(); }
				},
				jsonReader : {
					  root: "rows", 
					  page: "page",
					  total: "total",
					  records: "records",
					  repeatitems: false,					 
					  id: "0"				  
					},													
				colNames:['&nbsp;','StateId' ,'Code','Name', 'country','Status','address1','address2','city','TaxNo', 'Signature', 'version','createdBy','createdDate','modifiedBy','modifiedDate'], 
				colModel:[ 	{name:'Id', width:20, jsonmap:'id'},   
				           	{name:'stateId',index:'stateId', align:"center",hidden:true, width:220, jsonmap:'state.stateId'}, 
						 	{name:'stateCode',index:'stateCode', width:220, jsonmap:'state.stateCode'}, 
						   	{name:'stateName' ,index:'stateName' ,width:225, align:"center", jsonmap:'state.stateName'},
						   	{name:'stateCountry',index:'stateCountry' ,width:90, align:"center",jsonmap:'state.countryCode'},
						   	{name:'status',index:'status' ,width:90, align:"center", jsonmap:'state.status'},
							{name:'arlOffStAdress1', index:'arlOffStAdress1' ,width:225, align:"center",hidden:true,  jsonmap:'state.airlineOfficeSTAddress1'},	
							{name:'arlOffStAdress2', index:'arlOffStAdress2' ,width:225, align:"center",hidden:true,  jsonmap:'state.airlineOfficeSTAddress2'},	
							{name:'arlOffcity', index:'arlOffcity' ,width:225, align:"center",hidden:true,  jsonmap:'state.airlineOfficeCity'},	
							{name:'arlOffTaxReg', index:'arlOffTaxReg' ,width:225, align:"center",hidden:true,  jsonmap:'state.airlineOfficeTaxRegNo'},	
							{name:'stateDigiSig', index:'stateDigiSig' ,width:225, align:"center",hidden:true,  jsonmap:'state.digitalSignature'},	
						   	{name:'version', index:'version' ,width:225, align:"center",hidden:true,  jsonmap:'state.version'},						   							   	
						   	{name:'createdBy', index:'createdBy',width:225, align:"center",hidden:true,  jsonmap:'state.createdBy'},
						   	{name:'createdDate', index:'createdDate',width:225, align:"center",hidden:true,  jsonmap:'state.createdDate'},
						   	{name:'modifiedBy', index:'modifiedBy',width:225, align:"center",hidden:true,  jsonmap:'state.modifiedBy'},
						   	{name:'modifiedDate', index:'modifiedDate',width:225, align:"center",hidden:true,  jsonmap:'state.modifiedDate'}
						 ], 
				imgpath: '../../themes/default/images/jquery', 
				multiselect:false,
			    onSelectRow: function(rowid){
								fillForm(rowid);
								disableControls(true);
								enableSearch();			
								enableGridButtons();
								$("#rowNo").val(rowid);

				},		
				pager: jQuery('#listStatepager'),
				rowNum:20,						
				viewrecords: true,
				height:210,
				width:930,
				loadui:'block',
				  }).navGrid("#listStatepager",{refresh: true, edit: false, add: false, del: false, search: true});
			

		    
			// pre-submit callback 
			function showRequest(formData, jqForm, options) { 
			    top[2].ShowProgress();
			    return true; 
			} 			
			
			function showResponse(responseText, statusText)  {
				top[2].HideProgress();
//				clearForm();
				var response = JSON.parse(responseText);
				showCommonError(response.msgType, response.succesMsg);
			    if(response.msgType != "Error") {
			    	$('#frmState').clearForm();
			    	jQuery("#listState").trigger("reloadGrid");
			    	$("#btnAdd").attr('disabled', false); 
			    	$("#btnSave").disableButton();
			    	disableForm(true);
			    	disableStateButton();
					clearForm();
			    }
			} 	
			
			function showError(responseText, statusText) { 
				    showCommonError("Error", sysError);
				    return true; 
			} 

			function showDelete(formData, jqForm, options) {				
				return confirm(deleteRecoredCfrm); 
			} 
			
			function disableForm(cond){
				$("#stateCode").attr('disabled',cond);
				$("#stateName").attr('disabled',cond);
				$("#stateCountry").attr('disabled',cond);
				$("#status").attr('disabled',cond);
				$("#btnSave").attr('disabled',cond); 	
			}


			$('#btnSearch').click(function() {
				 jQuery("#listState").trigger("reloadGrid");
				 clearForm();
			});
			
			$('#btnSave').click(function() {
				if(validateFormDate()){
					disableControls(false);
					 var data = {};
					 data['stateDTO.stateId'] = $('#stateId').val();
					 data['stateDTO.stateCode'] =$('#stateCode').val();
					 data['stateDTO.stateName'] =$('#stateName').val();
					 data['stateDTO.status'] = $('#status').val();
					 data['stateDTO.countryCode'] =$('#stateCountry').val();
					 data['stateDTO.airlineOfficeSTAddress1'] =$('#arlOffStAdress1').val();
					 data['stateDTO.airlineOfficeSTAddress2'] =$('#arlOffStAdress2').val();
					 data['stateDTO.airlineOfficeCity']= $('#arlOffcity').val();
					 data['stateDTO.airlineOfficeTaxRegNo']= $('#arlOffTaxReg').val();
					 data['stateDTO.stateDigitalSignature']= $('#stateDigiSig').val();					 
					 data['hdnMode']= $('#hdnMode').val();
			         $.ajax({
                         url : 'showState.action',
                         beforeSend : top[2].ShowProgress(),
                         data : data,
                         type : "POST",
                         dataType : "json",
                         complete : function(resp) {
                        	 showResponse(resp.responseText, null );
                         }
                     });
				}

						
			});
			 
			$('#btnAdd').click(function() {
				disableControls(false);
				$('#frmState').clearForm();		
				$("#version").val('-1');
				$("#rowNo").val('');
				$('#btnEdit').disableButton();
				$('#btnDelete').disableButton();
				$("#btnSave").enableButton();
				setField("hdnMode","ADD");
				setField("hdnModel","ADD");
				$("#status").val('ACT');
			}); 	
			
			$('#btnClose').click(function() {
				closeState();
			}); 	


			
			$('#btnEdit').click(function() {
				disableControls(false);	
				$("#btnSave").enableButton();
				$("#stateCode").attr('disabled',true);
				setField("hdnMode","EDIT");
			}); 
			
			$('#btnReset').click(function() {
				if($('#rowNo').val() == '') {
					$('#frmState').resetForm();
					clearForm();
				}else {
					rowid = $("#rowNo").val();
					mode = $("#hdnMode").val();
					fillForm($("#rowNo").val());
					$("#rowNo").val(rowid);
					$("#hdnMode").val(mode);
				}
					
			});
			$('#btnDelete').click(function() {
				if(confirm(deleteRecoredCfrm)){
					disableControls(false);
					$('#hdnMode').val("DELETE");
	
					if($('#stateId').val() != undefined && $('#stateId').val() != "" ){
						 var data = {};
						 data['stateId'] = $('#stateId').val();
						 data['hdnMode']= $('#hdnMode').val();
				         $.ajax({
	                         url : 'showState!deleteState.action',
	                         beforeSend : top[2].ShowProgress(),
	                         data : data,
	                         type : "POST",
	                         dataType : "json",
	                         complete : function(resp) {
	                        	 showResponse(resp.responseText, null );
	                         }
	                     });
					}else{
						showCommonError("Error",selectRecord);
					}
					disableForm(true);
					return false;	
				}
			}); 
			
			function fillForm(rowid) {
				setField("hdnMode","");
				clearForm();
				jQuery("#listState").GridToForm(rowid, '#frmState');
				if(jQuery("#listState").getCell(rowid,'status') == 'ACT'){
					$("#status").val("ACT");								
				}else {																
					$("#status").val("INA");								
				}														
			}
			
			
			function  disableControls(cond){
				$("input").each(function(){ 
				      $(this).attr('disabled', cond); 
				});	
				$("textarea").each(function(){ 
				      $(this).attr('disabled', cond); 
				});	
				$("select").each(function(){ 
				      $(this).attr('disabled', cond); 
				});
				$("#btnClose").enableButton();
				$('input[type=checkbox]').each(function () {
					 $(this).attr('disabled', false);
				});
				enableSearch();
			}
			
			function enableSearch(){
				$('#selCountry').attr("disabled", false); 	
				$('#btnSearch').enableButton();
			}
			
			function disableStateButton(){
				$("#btnEdit").disableButton();
				$("#btnDelete").disableButton();
				$("#btnReset").disableButton();
				$("#btnSave").disableButton();

			}

			function enableGridButtons(){
				$("#btnAdd").enableButton();
				$("#btnEdit").enableButton();
				$("#btnDelete").enableButton();
				$("#btnReset").enableButton();
			}
			
			function validateFormDate(){
				
				if(trim($("#stateCode").val()) == ""){
					getFieldByID("stateCode").focus();
					showCommonError("Error", stateCodeRequire);
					return false;
				} else if(trim($('#stateName').val()) == ""){
					getFieldByID("stateName").focus();
					showCommonError("Error", stateNameRequire);
					return false;
				} else if(trim($('#arlOffStAdress1').val()) == ""){
					getFieldByID("arlOffStAdress1").focus();
					showCommonError("Error", arlOffStAdressfieldsRequire);
					return false;
				} else if(trim($('#arlOffStAdress2').val()) == ""){
					getFieldByID("arlOffStAdress2").focus();
					showCommonError("Error", arlOffStAdressfieldsRequire);
					return false;
				} else if( trim($('#arlOffcity').val()) == ""){
					getFieldByID("arlOffcity").focus();
					showCommonError("Error", arlOffcityfieldsRequire);
					return false;
				} else if(trim($('#arlOffTaxReg').val()) == ""){
					getFieldByID("arlOffTaxReg").focus();
					showCommonError("Error", arlOffTaxRegfieldsRequire);
					return false;
				}  else if($('#stateCountry').val() == null){
					getFieldByID("stateCountry").focus();
					showCommonError("Error", stateCountryRequire);
					return false;
				}  else if(trim($('#stateDigiSig').val()).length > 200){
					getFieldByID("stateDigiSig").focus();
					showCommonError("Error", signatureLengthExceeds);
					return false;
				}    
				
				return true;				
			}
			
			disableControls(true);
			$("#btnAdd").enableButton();
			$('#btnSearch').enableButton();
			
});

		
		function setPageEdited(isEdited) {
			top[1].objTMenu.tabPageEdited(stateScreenId, isEdited);
		}	

	
		function clearForm() {
			$('#frmState').resetForm();
		}
		
		function closeState() {
			top[1].objTMenu.tabRemove(stateScreenId);
		}

	
		
		
