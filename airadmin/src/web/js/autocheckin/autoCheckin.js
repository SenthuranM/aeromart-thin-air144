var selectedACT='';

jQuery(document).ready(function(){  //the page is ready			

			var grdArray = new Array();	
			
			$("#divSearchPanel").decoratePanel("Search Template");
			$("#divDispAutoCheckinTemp").decoratePanel("Template Details");
			$("#divResultsPanel").decoratePanel("Automatic Check In Templates");			
			$('#btnAdd').decorateButton();
			$('#btnEdit').decorateButton();
			$('#btnDelete').decorateButton();
			$('#btnClose').decorateButton();
			$('#btnSave').decorateButton();
			$('#btnSearch').decorateButton();
			$('#amount').numeric({allowDecimal:true});
			enableGridButtons(false);
			enbleFormButtons(false);
			
			jQuery("#listACITemp").jqGrid({ 
				url:'showAutoCheckInTemplate!searchACITemplates.action',
				datatype: "json",
				jsonReader : {
					  root: "rows", 
					  page: "page",
					  total: "total",
					  records: "records",
					  repeatitems: false,					 
					  id: "0"				  
					},													
				colNames:['&nbsp;','Template ID', 'Airports', 'Status','Amount'], 
				colModel:[ 	{name:'id', width:40, jsonmap:'id'},
					   	{name:'automaticCheckinTemplateId',index:'automaticCheckinTemplateId', width:125, jsonmap:'autoCheckin.templateId'}, 
					   	{name:'airports', index:'airports', width:175, jsonmap:'autoCheckin.airportlist'},						   
						{name:'status', width:150, align:"center", jsonmap:'autoCheckin.status'},
						{name:"amount", index: 'amount', hidden:true, jsonmap:'autoCheckin.amount' }
					  ], imgpath: '../../themes/default/images/jquery', multiselect:false,
				pager: jQuery('#temppager'),
				rowNum:20,						
				viewrecords: true,
				height:175,
				width:925,
				loadui:'block',
				onSelectRow: function(rowid){
					if(isPageEdited()) {
						clearForm();
						hideMessage();
						setPageEdited(false);
						$("#rowNo").val(rowid);
						fillForm(rowid);
						disableControls(true);
						enableSearch();
						enableGridButtons(true);
						enbleFormButtons(false);
					}
			}}).navGrid("#temppager",{refresh: true, edit: false, add: false, del: false, search: false});

			var options = { 
				cache: false,
				beforeSubmit:  showRequest,  // pre-submit callback - this can be used to do the validation part 
				success: showResponse,   // post-submit callback 			 
				dataType:  'json'        // 'xml', 'script', or 'json' (expected server response type) 
			 
			}; 

			 function fillForm(rowid){
			 	jQuery("#listACITemp").GridToForm(rowid,"#frmAutoCheckin");
			 	
				if(jQuery("#listACITemp").getCell(rowid,'status') == 'ACT'){
					$("#status").attr('checked', true);
				} else {
					$("#status").attr('checked', false);
				}
				
				$("#automaticCheckinTemplateId").val(jQuery("#listACITemp").getCell(rowid,'automaticCheckinTemplateId'));
				var selectedAirportList = jQuery("#listACITemp").getCell(rowid,'airports');
				stls.selectedData(selectedAirportList);
			}

		    
			// pre-submit callback 
			function showRequest(formData, jqForm, options) { 
				if(	trim($("#amount").val()) == "") {
					showCommonError("Error", baseCurrency);
					return false; 
				}
			
				if (trim(stls.getselectedData()) == "") {
					showCommonError("Error", chargeGroup);
					setPageEdited(true);
					return false;
				}
			
				top[2].ShowProgress();
				return true;
			} 
			
			function showResponse(responseText, statusText)  { 
				top[2].HideProgress();
				if (responseText['msgType'] == "Error") {
			    	showCommonError(responseText['msgType'], responseText['errorMsg']);
			    } else {
			    	showCommonError(responseText['msgType'], responseText['succesMsg']);
				    if(responseText['msgType'] != "Error"){
				    	alert(responseText['succesMsg']);
						setPageEdited(false);
				    	clearForm();
						jQuery("#listACITemp").trigger("reloadGrid");
						enableGridButtons(false);
						enbleFormButtons(false);
						$("select#selTemplate").html(responseText['templOption']);
				    }
			    }			 
			} 

			$('#btnSave').click(function() {
				hideMessage();
				disableControls(false);
				if($("#status").attr('checked')){
					$("#status").val('ACT');
				}else {
					$("#status").val('INA');
				}
				
				$("#selAirportLst").val(stls.getselectedData()); 
				$('#frmAutoCheckin').ajaxSubmit(options);
				return false;	
			});

			$('#btnSearch').click(function() {
				hideMessage();
				var BaseUrl = "showAutoCheckInTemplate!searchACITemplates.action";
				var strUrl = BaseUrl+"?selStatus="+$("#selStatus").val()
								+"&airportCode="+$("#selAirportCode").val();
				selStatus = $("#selStatus").val();
				jQuery("#listACITemp").setGridParam({url:strUrl});
				jQuery("#listACITemp").trigger("reloadGrid");
				enbleFormButtons(false);
				enableGridButtons(false);				
				clearForm();
				jQuery("#listACITemp").setGridParam({url:BaseUrl});
				grdArray = new Array();
				disableControls(true);
				enableSearch();
				$("#btnAdd").attr('disabled', false);
			});
			 
			$('#btnAdd').click(function() {
				hideMessage();
				disableControls(false);
				clearForm();
				grdArray = new Array();
				enbleFormButtons(true);
			}); 	

			$('#btnEdit').click(function() {
				hideMessage();
				disableControls(false);
				enbleFormButtons(true);			
			}); 

			function  disableControls(cond){
				$("input").each(function(){ 
		      		$(this).attr('disabled', cond); 
				});	
				$("textarea").each(function(){ 
					  $(this).attr('disabled', cond); 
				});	
				$("select").each(function(){ 
					  $(this).attr('disabled', cond); 
				});	
				$("#btnClose").attr('disabled', false);
			}

			function enableGridButtons(cond){
				if(cond) {
					$("#btnEdit").enableButton();
					$("#btnDelete").enableButton();					
				} else {
					$("#btnEdit").disableButton();
					$("#btnDelete").disableButton();					
				}				
				
			}
			
			function enbleFormButtons(cond){
				if(cond) {
					$("#btnReset").enableButton();
					$("#btnSave").enableButton();
				} else {
					$("#btnReset").disableButton();
					$("#btnSave").disableButton();
				}
				
			}
			
			function enableSearch(){
				$("#btnSearch").attr('disabled', false);
				$("#selAirportCode").attr('disabled', false);		
				$("#selStatus").attr('disabled', false);
			}

			disableControls(true);
			$("#btnAdd").attr('disabled', false);			
			enableSearch(); 	


			function setPageEdited(isEdited) {
				top[1].objTMenu.tabPageEdited(screenId, isEdited);
				hideMessage();
			}

			function isPageEdited() {
				return top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId));
			}
			function hideMessage() {
				top[2].HidePageMessage();
			}							
			function clearForm(){
				$("#automaticCheckinTemplateId").val('');
				$("#amount").val('');
				stls.selectedData('');
				$('#status').prop('checked', false);
			}
			autoCompleteAirportList();
			$('#airportList_1,#airportList_2,#airportList_3,#airportList_4').click(function() {
				autoCompleteAirportList();	
			}); 
			
			function autoCompleteAirportList(){
				jQuery.fn.filterByText = function(textbox) {
					  return this.each(function() {
					    var select = this;
					    var options = [];
					    $(select).find('option').each(function() {
					      options.push({
					        value: $(this).val(),
					        text: $(this).text()
					      });
					    });
					    $(select).data('options', options);
	
					    $(textbox).bind('change keyup', function() {
					      var options = $(select).empty().data('options');
					      var search = $.trim($(this).val());
					      var regex = new RegExp(search, "gi");
					      var myarray = new Array();
					      var tempOptions =[];
					      var countryList =[];
					      
					      $.each(options, function(index,option) {
					       
					        if ((option.value.match(regex) !== null) || (option.text.match(regex) !== null) ) {
					        	var country =option.value;
					        	if(country.indexOf(":") != -1){
					        		if(countryList.indexOf(country.split(":")[0]) > -1){
					        			tempOptions.push(option);
					        		}else{
					        			tempOptions.push({
									        value: country.split(":")[0],
									        text: country.split(":")[0]
									      });
					        			countryList.push(country.split(":")[0]);
					        			tempOptions.push(option);
					        		}
					        		
					        	}else{
					        		if(countryList.indexOf(country) > -1){
					        			tempOptions.push(option);
					        		}else{
					        			countryList.push(country);
					        			tempOptions.push(option);
					        		}
					        	}
					        }
					      });
					      $.each(tempOptions, function(index,option) {
				    		  $(select).append(
					            $('<option>').text(option.text).val(option.value)
					          );
					    	  
					      });
					    });
					  });
					};
	
					$(function() {
					  $('select#lstSTC').filterByText($('#searchItem'));
					});
				}
		}); 