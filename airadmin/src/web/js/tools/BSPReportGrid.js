var screenId = "SC_SHDS_008";
var valueSeperator = "~";

if (getTabValues(screenId, valueSeperator, "intLastRec") == "") {
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
}

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "7%";
objCol1.arrayIndex = 1;
objCol1.headerText = "Tnx ID";
objCol1.itemAlign = "left"
objCol1.sort = true;

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "7%";
objCol2.arrayIndex = 2;
objCol2.headerText = "Agent Code";
objCol2.itemAlign = "left"
objCol2.sort = true;

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "7%";
objCol3.arrayIndex = 3;
objCol3.headerText = "Country Code";
objCol3.itemAlign = "center"

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "22%";
objCol4.arrayIndex = 4;
objCol4.headerText = "Description";
objCol4.itemAlign = "left"

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "7%";
objCol5.arrayIndex = 5;
objCol5.headerText = "LCC ID";
objCol5.itemAlign = "left"

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "7%";
objCol6.arrayIndex = 6;
objCol6.headerText = "Modified By";
objCol6.itemAlign = "left"

var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "7%";
objCol7.arrayIndex = 7;
objCol7.headerText = "Modified Date";
objCol7.itemAlign = "left"

var objCol8 = new DGColumn();
objCol8.columnType = "label";
objCol8.width = "20%";
objCol8.arrayIndex = 8;
objCol8.headerText = "Status";
objCol8.itemAlign = "left"

var objCol9 = new DGColumn();
objCol9.columnType = "label";
objCol9.width = "3%";
objCol9.arrayIndex = 9;
objCol9.headerText = "version";
objCol9.itemAlign = "left"

var objCol10 = new DGColumn();
objCol10.columnType = "label";
objCol10.width = "8%";
objCol10.arrayIndex = 10;
objCol10.headerText = "BSP Txn Log ID";
objCol10.itemAlign = "left"
	
var objCol11 = new DGColumn();
objCol11.columnType = "label";
objCol11.arrayIndex = 11;
objCol11.headerText = "Pax Sequence";
objCol11.itemAlign = "left"

// ---------------- Grid
var objDG = new DataGrid("spnAirports");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol6);
objDG.addColumn(objCol7);
objDG.addColumn(objCol8);
objDG.addColumn(objCol9);
objDG.addColumn(objCol10);
objDG.addColumn(objCol11);

objDG.width = "99%";
objDG.height = "143px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = arrData;
objDG.seqNo = true;
objDG.seqStartNo = getTabValues(screenId, valueSeperator, "intLastRec"); // Grid
objDG.pgnumRecTotal = totalRecords; // remove as per return record size
objDG.paging = true
objDG.pgnumRecPage = 20;
objDG.rowClick = "RowClick";
objDG.pgonClick = "gridNavigations";
objDG.displayGrid();
objDG.shortCutKeyFunction = "Body_onKeyDown";
