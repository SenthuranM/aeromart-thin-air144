var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.buildCalendar();
var begin1 = [["ALL", "ALL"]];

jQuery(document).ready(function(){  //the page is ready				
			
			$("#divSearch").decoratePanel("Search PNRGOV Reports");
			$("#divResultsPanel").decoratePanel("PNRGOV Reports");
			$("#divDispPNRGOV").decoratePanel("Add/Modify PNRGOV Reports");			
			$('#btnClose').decorateButton();
			$('#btnEdit').decorateButton();
			$('#btnResubmit').decorateButton();
			$('#btnSearch').decorateButton();
			disablePNRGOVButton();
			createPopUps();
			var grdArray = new Array();	
			jQuery("#listPNRGOVReport").jqGrid({ 
				url:'showPnrGovReport!searchPNRGOVReport.action',
				datatype: "json",
				jsonReader : {
					  root: "rows", 
					  page: "page",
					  total: "total",
					  records: "records",
					  repeatitems: false,					 
					  id: "0"				  
					},													
				colNames:['&nbsp;','Log ID','Flight Seg ID', 'Status', 'Description', 'User ID', 'Transmission Time', 'Country','Airport','In/Out','Time Period'], 
				colModel:[ 	{name:'Id', width:40, jsonmap:'id'},   
						 	{name:'pnrGovTxHistoryID',index:'pnrGovTxHistoryID', width:70, jsonmap:'pnrgovreport.pnrGovTxHistoryID'}, 
						   	{name:'fltSegID',index:'fltSegID', width:100,  jsonmap:'pnrgovreport.fltSegID'}, 
						   	{name:'status', width:90, align:"center", jsonmap:'pnrgovreport.status'},
						   	{name:'description', width:360, align:"center",  jsonmap:'pnrgovreport.description'},
						   	{name:'userId', width:90, align:"center",  jsonmap:'pnrgovreport.userId'},
							{name:'transmissionTimeStamp', width:125, align:"right",  jsonmap:'pnrgovreport.transmissionTimeStamp'},
							{name:'countryCode', width:70, align:"center",  jsonmap:'pnrgovreport.countryCode'},
							{name:'airportCode', width:70, align:"center",  jsonmap:'pnrgovreport.airportCode'},						   							   	
						   	{name:'inboundOutbound', width:100, align:"center",  jsonmap:'pnrgovreport.inboundOutbound'},
						   	{name:'timePeriod', width:90, align:"center",  jsonmap:'pnrgovreport.timePeriod'},
						   	
						   
						  ], imgpath: '../../themes/default/images/jquery', multiselect:false,
				pager: jQuery('#pnrgovreportpager'),
				rowNum:20,						
				viewrecords: true,
				height:210,
				width:930,
				loadui:'block',
				onSelectRow: function(rowid){
						$("#rowNo").val(rowid);
						$('#dvUpLoad').html($('#dvUpLoad').html());
						$('#dvUpLoadThum').html($('#dvUpLoadThum').html());
						fillForm(rowid);

						enableSearch();
						disablePNRGOVButton();
						var dbStatusID = jQuery("#listPNRGOVReport").getCell(rowid,'status');
						if (dbStatusID == "ERROR"){
							enableGridButtons();
						} else {
							disableGridButtons();
						}

			}}).navGrid("#pnrgovreportpager",{refresh: true, edit: false, add: false, del: false, search: false});
			
			
			function showDelete(formData, jqForm, options) {				
			    return confirm(deleteRecoredCfrm); 
			} 
			$('#btnResubmit').click(function() {

				setField("hdnMode","SENDPNRGOV");
				var data = {};
				data['pnrGovTxHistoryID'] = $("#pnrGovTxHistoryID").val();
				data['hdnMode'] = "SENDPNRGOV";
				var url = "showPnrGovReport.action";
				$.ajax({type: "POST", dataType: 'json', data: data , url:url,		
					success: savePNRGOVSuccess, error:savePNRGOVError, cache : false});

				return false;	
			});
			 
			$('#btnEdit').click(function() {				

				$("#btnResubmit").enableButton();
				setField("hdnMode","SENDPNRGOV");
			}); 
			
			$('#btnSearch').click(function() {
				searchData();			
			});
			
			function searchData(){
				
				var newUrl = 'showPnrGovReport!searchPNRGOVReport.action?';
			 	newUrl += 'pnrGovReportSrch.countryName=' + $("#selCountry").val();
			 	newUrl += '&pnrGovReportSrch.fltSegID=' + $("#txtFltSegId").val();
			 	newUrl += '&pnrGovReportSrch.status=' + $("#selStatus").val();
			 	newUrl += '&pnrGovReportSrch.frmDate=' + $("#txtDateFrom").val();
			 	newUrl += '&pnrGovReportSrch.toDate=' + $("#txtDateTo").val();
			 	
			 	$("#listPNRGOVReport").clearGridData();
			 	$.getJSON(newUrl, function(response){
					$("#listPNRGOVReport")[0].addJSONData(response);
				
			 	});	
			}
			
			function savePNRGOVSuccess(response){
				if (response.success == true){
					if (response.hdnMode == "SENDPNRGOV"){
						searchData();
						//jQuery("#listPNRGOVReport").trigger("reloadGrid");
						disablePNRGOVButton();
						$('#frmPNRGOVReport').resetForm();
						alert( "Message Sent Successfully!!!");
					}
				} else {
					alert( response.txtMsg);
				}
			}
			
			function savePNRGOVError(response){
				alert (response);
			}

			function fillForm(rowid) {
				setField("hdnMode","");
				jQuery("#listPNRGOVReport").GridToForm(rowid, '#frmPNRGOVReport');					
				var dbStatusID = jQuery("#listPNRGOVReport").getCell(rowid,'status');

				if (dbStatusID == "ERROR"){
					enableGridButtons();
				} else {
					disableGridButtons();
				}
				
			}

			
			function enableSearch(){
				$('#selCountry').attr("disabled", false); 
				$('#selStatus').attr("disabled", false);
				$('#selCabinClass').attr("disabled", false); 
				$('#btnSearch').enableButton();
			}
			
			
			function disablePNRGOVButton(){
				$("#btnEdit").disableButton();
				$("#btnResubmit").disableButton();
			}

			function enableGridButtons(){
				$("#btnEdit").enableButton();

			}
			
			function disableGridButtons(){
				$("#btnEdit").disableButton();
			}
			
			$("#btnAdd").enableButton();
			$('#selCountry').attr("disabled", false); 
			$('#selStatus').attr("disabled", false); 
			$('#btnSearch').enableButton();
			
			

		
		
			
		});	

				
		function LoadCalendar(strID, objEvent) {
			objCal1.ID = strID;
			objCal1.top = 0;
			objCal1.left = 0;
			objCal1.onClick = "setDate";
			objCal1.showCalendar(objEvent);
		}
		
		function setDate(strDate, strID) {
			switch (strID) {
			case "0":
				setField("txtDateFrom", strDate);
				break;
			case "1":
				setField("txtDateTo", strDate);
				break;
			case "3":
				setDate(strDate, strID);
				break;
			case "4":
				setDate(strDate, strID);
				break;
			}
		}
		

