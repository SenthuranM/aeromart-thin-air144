var SSR_CATEGORY = {INFLIGHT_SERVICE:1, AIRPORT_SERVICE:2, AIRPORT_TRANSFER:3};

jQuery(document)
		.ready(
				function() { // the page is ready

					$("#divSearch").decoratePanel("Search SSR Charges");
					$("#divResultsPanel").decoratePanel("SSR Charges");
					$("#divDispSSRCharges")
							.decoratePanel("Add/Modify SSR Charges");
					$('#btnAdd').decorateButton();
					$('#btnEdit').decorateButton();
					$('#btnDelete').decorateButton();
					$('#btnClose').decorateButton();
					$('#btnReset').decorateButton();
					$('#btnSave').decorateButton();
					$('#btnSearch').decorateButton();

					disableSSRChargeButton();

					var grdArray = new Array();
					jQuery("#listSSRCharges").jqGrid(
							{
								url : 'showSSRCharges!searchSSRCharges.action',
								datatype : "json",
								jsonReader : {
									root : "rows",
									page : "page",
									total : "total",
									records : "records",
									repeatitems : false,
									id : "0"
								},
								colNames : [ '&nbsp;', 'ChargeId',
										'Charge Code', 'Description',
										'ApplicablityType', 'AdultAmount',
										'ChildAmount', 'InfantAmount',
										'ReservationAmount','SSR Code', 'Status',
										'SsrId', 'SsrInfoDescription',
										'SsrInfoCatId', 'SsrInfoCategory',
										'SsrInfoSubCatId',
										'SsrInfoSubCategory', 'version',
										'createdBy', 'createdDate',
										'modifiedBy', 'modifiedDate', 'serviceCharge', 'cabinClassCode','logicalCabinClass','ChargeLocalCurrencyCode'],
								colModel : [ {
									name : 'Id',
									width : 10,
									jsonmap : 'id'
								}, {
									name : 'chargeId',
									index : 'chargeId',
									width : 30,
									align : "center",
									hidden : true,
									jsonmap : 'ssrCharge.chargeId'
								}, {
									name : 'chargeCode',
									index : 'chargeCode',
									width : 30,
									align : "center",
									jsonmap : 'ssrCharge.ssrChargeCode'
								}, {
									name : 'description',
									index : 'description',
									width : 60,
									jsonmap : 'ssrCharge.description'
								}, {
									name : 'applyBy',
									index : 'applyBy',
									width : 30,
									hidden : true,
									jsonmap : 'ssrCharge.applicablityType'
								}, {
									name : 'adultAmount',
									index : 'adultAmount',
									width : 30,
									hidden : true,
									jsonmap : 'ssrCharge.localCurrAdultAmount'
								}, {
									name : 'childAmount',
									index : 'childAmount',
									width : 30,
									hidden : true,
									jsonmap : 'ssrCharge.localCurrChildAmount'
								}, {
									name : 'infantAmount',
									index : 'infantAmount',
									width : 30,
									hidden : true,
									jsonmap : 'ssrCharge.localCurrInfantAmount'
								}, {
									name : 'reservationAmount',
									index : 'reservationAmount',
									width : 30,
									hidden : true,
									jsonmap : 'ssrCharge.localCurrReservationAmount'
								}, {
									name : 'ssrInfoCode',
									index : 'ssrInfoCode',
									width : 30,
									jsonmap : 'ssrCharge.ssrInfoCode'
								}, {
									name : 'status',
									index : 'status',
									width : 30,
									hidden : true,
									jsonmap : 'ssrCharge.status'
								}, {
									name : 'ssrId',
									index : 'ssrId',
									width : 30,
									align : "center",
									hidden : true,
									jsonmap : 'ssrCharge.ssrId'
								}, {
									name : 'ssrInfoDescription',
									index : 'ssrInfoDescription',
									width : 30,
									align : "center",
									hidden : true,
									jsonmap : 'ssrCharge.ssrInfoDescription'
								}, {
									name : 'ssrInfoCatId',
									index : 'ssrInfoCatId',
									width : 30,
									align : "center",
									hidden : true,
									jsonmap : 'ssrCharge.ssrInfoCatId'
								}, {
									name : 'ssrInfoCategory',
									index : 'ssrInfoCategory',
									width : 30,
									align : "center",
									hidden : true,
									jsonmap : 'ssrCharge.ssrInfoCategory'
								}, {
									name : 'ssrSubCatId',
									index : 'ssrSubCatId',
									width : 30,
									align : "center",
									hidden : true,
									jsonmap : 'ssrCharge.ssrInfoSubCatId'
								}, {
									name : 'ssrInfoSubCategory',
									index : 'SsrInfoSubCategory',
									width : 30,
									align : "center",
									hidden : true,
									jsonmap : 'ssrCharge.ssrInfoSubCategory'
								}, {
									name : 'version',
									width : 225,
									align : "center",
									hidden : true,
									jsonmap : 'ssrCharge.version'
								}, {
									name : 'createdBy',
									width : 30,
									align : "center",
									hidden : true,
									jsonmap : 'ssrCharge.createdBy'
								}, {
									name : 'createdDate',
									width : 30,
									align : "center",
									hidden : true,
									jsonmap : 'ssrCharge.createdDate'
								}, {
									name : 'modifiedBy',
									width : 30,
									align : "center",
									hidden : true,
									jsonmap : 'ssrCharge.modifiedBy'
								}, {
									name : 'modifiedDate',
									width : 30,
									align : "center",
									hidden : true,
									jsonmap : 'ssrCharge.modifiedDate'
								}, {
									name : 'serviceCharge',
									width : 30,
									align : "center",
									hidden : true,
									jsonmap : 'ssrCharge.localCurrChargeAmount'
								}, {
									name : 'cabinClassCode',
									width : 15,
									align : "center",
									hidden : true,
									jsonmap : 'ccList'
								},{
									name : 'logicalCabinClass',
									width : 15,
									align : "center",
									hidden : true,
									jsonmap : 'lccList'
								},{
									name : 'chargeLocalCurrencyCode', 
									jsonmap : 'ssrCharge.chargeLocalCurrencyCode',
									width : 150, 
									align : "center",
									hidden : true
								}					

								],
								imgpath : '../../themes/default/images/jquery',
								multiselect : false,
								pager : jQuery('#ssrchargepager'),
								rowNum : 20,
								viewrecords : true,
								height : 210,
								width : 930,
								loadui : 'block',
								onSelectRow : function(rowid) {									
									$("#rowNo").val(rowid);									
									fillForm(rowid);
									disableControls(true);
									enableSearch();
									disableSSRChargeButton();
									enableGridButtons();
									applyByChange();									
								}
							}).navGrid("#ssrchargepager", {
						refresh : true,
						edit : false,
						add : false,
						del : false,
						search : false
					});

					var options = {
						cache : false,
						beforeSubmit : showRequest, // pre-submit callback -
						// this can be used to do
						// the validation part
						success : showResponse, // post-submit callback
						//error: showErrorTest,
						dataType : 'json' // 'xml', 'script', or 'json'
					// (expected server response type)
					};
					
					function showErrorTest(responseText, statusText){
						alert(statusText);
					}

					var delOptions = {
						cache : false,
						beforeSubmit : showDelete, // pre-submit callback -
						// this can be used to do
						// the validation part
						success : showResponse, // post-submit callback
						url : 'showSSRCharges!deleteSSRCharge.action', // override
						// for
						// form's
						// 'action'
						// attribute
						dataType : 'json', // 'xml', 'script', or 'json'
						// (expected server response type)
						resetForm : true
					// reset the form after successful submit

					};

					// pre-submit callback
					function showRequest(formData, jqForm, options) {						

						if (trim($("#chargeCode").val()) == "") {
							showCommonError("Error", chargCodeReq);
							return false;
						}

						if (trim($("#description").val()) == "") {
							showCommonError("Error", chargDescReq);
							return false;
						}

						var ssrId = trim($("#ssrId").val());
						if (ssrId == "" || ssrId == "-1") {
							showCommonError("Error", ssrCodeReq);
							return false;
						}
						
						var isSelectCOS = false;
						$('#cos option').each(function(i, option){ 
							var selVal = $(this).val();
							if($('#cos option[value$="' + selVal + '"]').attr('selected')){
								isSelectCOS = true;
							}					
						});
						if(isSelectCOS == false){
						  	showCommonError("Error", "Class of Service Required");
						   	return false;
						}
						
						var ssrCatId = ssrCatMap[ssrId];
						$("#ssrInfoCatId").val(ssrCatId);
						
						if(ssrCatId == SSR_CATEGORY.INFLIGHT_SERVICE){
							if (!checkValidAmount("serviceCharge")) {
								showCommonError("Error", serChgAmtErr);
								return false;
							}
						} else if(ssrCatId == SSR_CATEGORY.AIRPORT_SERVICE || ssrCatId == SSR_CATEGORY.AIRPORT_TRANSFER){							
							// applyBy
							if (trim($("#applyBy").val()) != ""
									&& trim($("#applyBy").val()) == "P") {
								if (!checkValidAmount("adultAmount")) {
									showCommonError("Error", adultAmtErr);
									return false;
								}
	
								if (!checkValidAmount("childAmount")) {
									showCommonError("Error", childAmtErr);
									return false;
								}
	
								if (!checkValidAmount("infantAmount")) {
									showCommonError("Error", infantAmtErr);
									return false;
								}
	
							} else {
								if (!checkValidAmount("reservationAmount")) {
									showCommonError("Error", reserAmtErr);
									return false;
								}
							}					
						}
						 if($("#localCurrCode").val() == null){
								showCommonError("Error", localCurrencyCodeRqrd);
								return false;
							}
						top[2].ShowProgress();
						return true;
					}

					function showResponse(responseText, statusText) {
						top[2].HideProgress();
						showCommonError(responseText['msgType'],
								responseText['succesMsg']);
						if (responseText['msgType'] != "Error") {
							$('#frmSSRCharges').clearForm();

							jQuery("#listSSRCharges").trigger("reloadGrid");
							disableSSRChargeButton();
							$("#btnAdd").attr('disabled', false);
							$("#btnSave").enableButton();
							
							alert(responseText['succesMsg']);
						}

					}

					function showDelete(formData, jqForm, options) {
						return confirm(deleteRecoredCfrm);
					}
										
					$("#cos").change(function () {				
						$("#cos option:selected").each(function () {
							var selVal = $(this).val();
							var selArr = selVal.split("_");
							if (selArr[0] == selArr[1]){
								$('#cos option[value!="' + selVal + '"][value$="_' + selArr[0] + '"]').attr('selected', false);
						    }
						});
					});
					
					$('#btnSave').click(function() {
						disableControls(false);
						
						$('#frmSSRCharges').ajaxSubmit(options);
						return false;
					});

					$('#btnAdd').click(function() {
						disableControls(false);
						$('#frmSSRCharges')[0].reset();
						$("#version").val('-1');
						$("#rowNo").val('');
						$('#btnEdit').disableButton();
						$('#btnDelete').disableButton();
						$("#btnSave").enableButton();
						$(".airportServiceCharge").hide();
						$(".infligtServiceCharge").hide();
						setField("hdnMode", "ADD");
						applyByChange();
						updateSSRList();						
						$("#btnReset").enableButton();
					});

					$('#btnEdit').click(function() {
						disableControls(false);
						$('#btnDelete').disableButton();
						$('#btnUpload').enableButton();
						$("#btnSave").enableButton();						
						setField("hdnMode", "EDIT");			
						updateSSRList();
						$('#chargeCode').attr('disabled', true);		
						
					});
					$('#btnReset').click(function() {
						if ($("#rowNo").val() == '') {
							$('#frmSSRCharges')[0].reset();
						} else {

							fillForm($("#rowNo").val());
						}

					});
					$('#btnDelete').click(function() {
						disableControls(false);
						$('#frmSSRCharges').ajaxSubmit(delOptions);
						return false;
					});

					$('#btnSearch')
							.click(
									function() {
										// srchChargeCode,selSsrCode,selCategory,selStatus
										var newUrl = 'showSSRCharges!searchSSRCharges.action?';
										newUrl += 'srchChargeCode='
												+ $("#srchChargeCode").val();
										newUrl += '&selSsrCode='
												+ $("#selSsrCode").val();
										newUrl += '&page=1';

										$("#listSSRCharges").clearGridData();
										$.getJSON(newUrl, function(response) {
											$("#listSSRCharges")[0]
													.addJSONData(response);

										});
									});
									
					$("#ssrId").change(function(){
						var selValue = $(this).val();
						
						if(selValue == null || selValue == "" || selValue == -1){
							$(".infligtServiceCharge").hide();
							$(".airportServiceCharge").hide();
							$("#ssrInfoCatId").val(0);
						} else {
							var ssrCatId = ssrCatMap[selValue];							
							handleSSRChargeType(ssrCatId);
						}
						
					});

					function fillForm(rowid) {
						jQuery("#listSSRCharges").GridToForm(rowid, '#frmSSRCharges');							
						var selectedSSrCatId = $("#listSSRCharges").getCell(rowid, 'ssrInfoCatId');
						
						code = jQuery("#listSSRCharges").getCell(rowid,'chargeLocalCurrencyCode');
						$("#localCurrCode").val(code);
						$("#lblLocalCurrencyCode").html(code);
												
						if(selectedSSrCatId != null && selectedSSrCatId != "" && selectedSSrCatId != "-1"){
							handleSSRChargeType(parseInt(selectedSSrCatId));		
						}
						setSelectedCOSList(rowid);
						enableGridButtons();
					}
					
					function setSelectedCOSList(rowid){
						$('#cos option').each(function(i, option){ 
							var selVal = $(this).val();
							$('#cos option[value$="' + selVal + '"]').attr('selected', false);
						});
						var cabinClass = "";
						var logocalCC = "";
						var cc = jQuery("#listSSRCharges").getCell(rowid,'cabinClassCode');
						var lcc = jQuery("#listSSRCharges").getCell(rowid,'logicalCabinClass');
						if(cc != ""){
							cabinClass = cc.split(",");
						}
						if(lcc != ""){
							logocalCC = lcc.split(",");
						}
						
						$('#cos option').each(function(i, option){ 
							var selVal = $(this).val();
							var selArr = selVal.split("_");
							if(lcc != ""){
								for(var j = 0;j < logocalCC.length;j++){
									if (trim(selArr[0]) == trim(logocalCC[j])){
										$('#cos option[value$="' + selVal + '"]').attr('selected', true);
								    }
								}
							}
							if(cc != ""){
								for(var j = 0;j < cabinClass.length;j++){
									if (trim(selArr[0]) == trim(cabinClass[j])){
										$('#cos option[value$="' + selVal + '"]').attr('selected', true);
								    }
								}
							}
						});
					}
					function handleSSRChargeType(selSSRCatId){
						if(selSSRCatId == SSR_CATEGORY.INFLIGHT_SERVICE){
							$(".infligtServiceCharge").show();
							$(".airportServiceCharge").hide();
						} else if(selSSRCatId == SSR_CATEGORY.AIRPORT_SERVICE || selSSRCatId == SSR_CATEGORY.AIRPORT_TRANSFER){
							$(".airportServiceCharge").show();
							$(".infligtServiceCharge").hide();
						}
						
						$("#ssrInfoCatId").val(selSSRCatId);
					}
					
					function disableControls(cond) {
						$("input").each(function() {
							$(this).attr('disabled', cond);
						});
						$("textarea").each(function() {
							$(this).attr('disabled', cond);
						});
						$("select").each(function() {
							$(this).attr('disabled', cond);
						});

						if (cond == true) {
							blnCalander = false;
						} else {
							blnCalander = true;
						}

						$("#btnClose").enableButton();

					}

					function enableSearch() {
						$('#srchChargeCode').attr("disabled", false);
						$('#selSsrCode').attr("disabled", false);
						$('#selCategory').attr("disabled", false);
						$('#btnSearch').enableButton();
					}

					function disableSSRChargeButton() {
						$("#btnEdit").disableButton();
						$("#btnDelete").disableButton();
						$("#btnReset").disableButton();
						$("#btnSave").disableButton();
					}

					function enableGridButtons() {
						$("#btnAdd").enableButton();
						$("#btnEdit").enableButton();
						$("#btnDelete").enableButton();
						$("#btnReset").enableButton();
					}

					disableControls(true);
					$("#btnAdd").enableButton();
					$('#srchChargeCode').attr("disabled", false);
					$('#selSsrCode').attr("disabled", false);
					$('#selCategory').attr("disabled", false);
					$('#btnSearch').enableButton();

					function setPageEdited(isEdited) {
						top[1].objTMenu.tabPageEdited(screenId, isEdited);
					}

				});

function applyByChange() {
	var applyBy = getValue("applyBy");	
	
	if (applyBy != null && applyBy == "R") {
		ReadOnly("reservationAmount", false);
		ReadOnly("adultAmount", true);
		ReadOnly("childAmount", true);
		ReadOnly("infantAmount", true);
		setField("adultAmount", "0");
		setField("childAmount", "0");
		setField("infantAmount", "0");		
		getFieldByID("reservationAmount").focus();
	} else {
		ReadOnly("adultAmount", false);
		ReadOnly("childAmount", false);
		ReadOnly("infantAmount", false);
		ReadOnly("reservationAmount", true);
		setField("reservationAmount", "0");
		if($("#adultAmount").val() == ""){
			setField("adultAmount", "0");
		}
		if($("#childAmount").val() == ""){
			setField("childAmount", "0");	
		}
		if($("#infantAmount").val() == ""){
			setField("infantAmount", "0");
		}
		if($("#serviceCharge").val() == ""){
			setField("serviceCharge", "0");	
		}
		if($("#ssrInfoCatId").val() == ""){
			setField("ssrInfoCatId", "0");
		}	
		getFieldByID("adultAmount").focus();		
	}
}

function checkValidAmount(fieldId) {
	var checkIntparam = trim($("#" + fieldId).val());
	
	if (checkIntparam==null || checkIntparam =="") {
		return false;
	}
	
	blnVal2 = currencyValidate(checkIntparam, 8, 2);
	
	if(!blnVal2){
		return false;
	}


	return true;
}

function alphaCustomValidate(val){
	customValidate(val,/[0-9`!@#$%\s&*?\[\]{}()|\\\/+=:.,;^~_-]/g)
}

function validateAmount(objTxt) {
	var strCR = objTxt.value;
	var strLen = strCR.length;
	var blnVal = currencyValidate(strCR, 8, 2);
	var wholeNumber;
	if (!blnVal) {
		if (strCR.indexOf(".") != -1) {
			wholeNumber = strCR.substr(0, strCR.indexOf("."));
			if (wholeNumber.length > 9) {				
				objTxt.value = strCR.substr(0,wholeNumber.length - 1);
			} else {
				objTxt.value = strCR.substr(0, strLen - 1);
			}
		} else {
			objTxt.value = strCR.substr(0, strLen - 1);
		}
		objTxt.focus();
	}
}



function updateSSRList() {
	var data = {};
	
	$("#frmSSRCharges").ajaxSubmit({
	dataType: 'json', 
	data:data, 
	url:"showSSRCharges!getAvailableSSR.action",							
	success: loadSuccess});			
}

loadSuccess = function(response){
	if (response.updatedSsrCodes != null && trim(response.updatedSsrCodes) != "") {
		$("#ssrId").html(response.updatedSsrCodes);		
		
		var hdnMode = getValue("hdnMode");	
		
		if (jQuery("#listSSRCharges").getCell($("#rowNo").val()) >= 0) {
			selectedVal = jQuery("#listSSRCharges").getCell($("#rowNo").val(), 'ssrId');
			$("#ssrId").val(selectedVal);
		}
			
		if(hdnMode!=null && hdnMode=="ADD"){
			$("#ssrId").val("-1");
		}
		
	}
}