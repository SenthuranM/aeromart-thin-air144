var newSearch = false;
var isNewConfig = true;
var bundledFareConfig = {};
var selectedBundleFareConfig = null;
var panelsEditable= true;

var bundledCategoryMap = {};
var bundleDescriptionTemplate = null;

var dataMap = {	
	FlightNo: [],
	Agents: [],
	BookingClass: [],	
	SearchTags:[],
	Countries : []
}

var allFlightNos = [];
var selRoute='';
var routeArray = [];

var freeServices = {
		SEAT : "SEAT_MAP",
		MEAL : "MEAL",
		BAGGAGE : "BAGGAGE",
		HALA : "AIRPORT_SERVICE",
		FLEXI : "FLEXI_CHARGES"
}

var BASE_SERVICE_RANKING = ['BAGGAGE', 'MEAL', 'SEAT', 'FLEXI', 'HALA'];
var MARKER_TEXT = "href=";

var IMAGE_UPLOAD_TYPE = {BUNDLE : 'bundle', STRIPE : 'stripe'};
var lblBundleImage = "Upload Bundled Fare Images";
var lblStripeImage = "Upload Bundled Fare Stripe Images";
var uploadingType = '';
var strBundleUploads = {};
var strStripeUploads = {};
var imageSuffix = '.png';
var currentEditingServiceName = '';
var currentEditingService = null;
var selectedServices = { baseService:'', services:[]};
var selectedServicesCopy = { baseService:'', services:[]};
var currentlyEditingDisplaySetting = null;
var displaySettings = { periodKey:'', translations:[], thumbnails:[], displayTemplate:''};


$(document).ready(function(){
	$("#divBundledFaresSearch").decoratePanel("Search Bundled Services");
	$("#bundledFaresDetails").decoratePanel("Add/Edit Bundled Services");
	$("#divSelectedServiceTemplate").decoratePanel("Add/Edit Selected Services");
	$("#divSelectedServiceTemplateGrid").decoratePanel("Selected Services Periods");
	$("#divBundleDisplayTemplate").decoratePanel("Add/Edit Bundle Description Template");
	$("#divDisplayThumbnailsTemplate").decoratePanel("Upload Bundle Thumbnail Images");
	$("#divDisplayTranslationsTemplate").decoratePanel("Add/Edit Bundle Translations");
	$("#divTranslationsTemplateGrid").decoratePanel("Bundle Translations");
	$("#bundledFaresDetails").hide();
	$("#divBundleDescTemplateSampleDisplay").hide();
	
	$("#bundledFareName").alphaNumericWhiteSpace();
	$("#txtBundledFee").numeric({allowDecimal:true});
	
	loadInitdata();
	
	$("#btnAdd").click(function(){
		panelsEditable= true;
		addBundleFares();
	});
	
	$("#btnEdit").click(function(){
		panelsEditable=true;
		editBundleFares();
		OverrideFieldsForEdit();
	});
	
	$("#bundleTemplateTabs").tabs().css({'resize':'none','min-height':'250px'});
	$("#displaySettingsTabs").tabs().css({'resize':'none','min-height':'420px'});
	
    prepareServiceTroggleEditButtons();

    prepareServiceToggleCheckBoxes();

    prepareServiceTemplatePopulator();
    
    prepareDescriptionTemplatePopulator();

    prepareSelectedServiceTab();
	
	$("#btnBack").click(function(){
		resetBundledFares();
		$('#jqGridBundledFaresContainer').show();
		$("#bundledFaresDetails").hide();
	});
	
	$(".addItem").click(function(e){
		addItemsToSelect(e.target.id);
	});
	$(".removeItem").click(function(e){
		removeItemsFromSelect(e.target.id);
	});
	
	$("#btnSearch").click(searchBundledFares);
	$("#btnSave").click(saveBundledFare);

	$("#btnReset").click(function(e){
		resetBundledFares();
		isNewConfig = true;
		panelsEditable= true;
	});
	
	$("#flightPeriodFrom, #flightPeriodTo, #serviceFromDate, #serviceToDate" ).datepicker({
		dateFormat: 'dd/mm/yy',
		minDate: new Date()
	}).keyup(function(e) {
	    if(e.keyCode == 8 || e.keyCode == 46) {
	        $.datepicker._clearDate(this);
	    }
	});
	
	$('#addHala').click(function(){addToSelectPaneCustom('halaAvailable', 'halaAssigned', true)});
    $('#removeHala').click(function(){addToSelectPaneCustom('halaAssigned', 'halaAvailable', true)});
    
    $('#addOndCodes').click(function(){
    	addToSelectPaneCustom('sel_GeneratedRoutes', 'sel_Routes', true); 
    	removeDuplicationFromSelectBox('sel_Routes');
    });
    
    $('#removeOndCodes').click(function(){addToSelectPaneCustom('sel_Routes', 'sel_GeneratedRoutes', true)});
    
    $('#addAllOndCodes').click(function(){
    	addToSelectPaneCustom('sel_GeneratedRoutes', 'sel_Routes', false);
    	removeDuplicationFromSelectBox('sel_Routes');
    });
    $('#removeAllOndCodes').click(function(){addToSelectPaneCustom('sel_Routes', 'sel_GeneratedRoutes', false)});
    
    $('#addBookingClasses').click(function(){addToSelectPaneCustom('sel_BookingClass', 'sel_BookingClassAssigned', true)});
    $('#removeBookingClasses').click(function(){addToSelectPaneCustom('sel_BookingClassAssigned', 'sel_BookingClass', true)});
    
    $('#add_FlightNo').click(function(){addToSelectPaneCustom('sel_FlightNo', 'sel_FlightNoAssigned', true)});
    $('#del_FlightNo').click(function(){addToSelectPaneCustom('sel_FlightNoAssigned', 'sel_FlightNo', true)});
    
    $('#addCountry').click(function(){addToSelectPaneCustom('sel_Country', 'sel_CountryAssigned', true)});
    $('#removeCountry').click(function(){addToSelectPaneCustom('sel_CountryAssigned', 'sel_Country', true)});
         
    
    $('#btnImageDialogClose').click(function() {
    	$('#imageUploadDialog').dialog('close');
    }); 
	
    createCategoryMap();
	createGrid();
	$("#divBundledFaresSearch").show();
	$("#bundledFaresDetails").show(); 
	
	$("#samrtSearchHelp").dialog({
        autoOpen: false, 
        hide: "fade",
        show : "slide",
        height: 550,
        width: 650,
        draggable: true,
        resizable: true,
     });
	$("#btnSmartSearchHelp").click(function() {
        $("#samrtSearchHelp").dialog( "open" );
    });
	
	$("#btnSmartSearchClear").click(function(){
		clearSmartSearch();
	});
	
	$("#selChanel").change(function() {
		setupWEBPointOfSales();
	});
	
	
});

function setupWEBPointOfSales(selectedCountryCodes) {
	validateWEBPointOfSaleSelection();
	if (selectedCountryCodes != null) {
		selecteOptionsFromValues("sel_Country", selectedCountryCodes);
		addToSelectPaneCustom('sel_Country', 'sel_CountryAssigned', true);	
	}
}

function validateWEBPointOfSaleSelection() {
	var  salesChannels = getSelectedSelectBoxData("selChanel");
	var  WEBSALES_CHANNEL = '4';
	var  includeWEBPOS = false;
	
	if ($.inArray(WEBSALES_CHANNEL, salesChannels) != -1) {
		includeWEBPOS = true;
		enableWEBPointOfSalesTab();
	} else {
		disableWEBPointOfSalesTab();
	}
	
	return includeWEBPOS;
	
}

/*
 * values - [SL, Srilanka], 
 */
var addOptionsToSelectFromStringMap = function(selectID,values){
	if(values != null && values.length > 0){
		$("#" + selectID +" option[value='']").remove();
	}
	$.each(values,function(index,value){
		var displayText = replaceall(value[1],"***","All");
		$("#"+selectID).append('<option value='+value[0]+'>'+displayText+'</option>');
	});
}

var selecteOptionsFromValues = function(selectID,values){
	if(values != null && values.length > 0){
		$("#" + selectID +" option[value='']").remove();
	}	
	$("#"+selectID).val(values);
	
}

function getCountryName(countryCode) {
	var countryData;
	for (countryData = 0; countryData < dataMap.Countries.length; countryData++) {
		if (dataMap.Countries[countryData][0] == countryCode) {
			return dataMap.Countries[countryData][1];
		}
	}
		
}

function enableWEBPointOfSalesTab() {
	$("#bundleTemplateTabs").tabs("enable", 6);
}

function disableWEBPointOfSalesTab() {
	$("#bundleTemplateTabs").tabs("disable", 6);
}

/**
 * Due to slow performance(page stuck) of existing select box functionality,
 * handling large number of data, introduce the new functionality
 * 
 */
function addToSelectPaneCustom(src, dst, isSelected) {
	var selected = '';
	if (isSelected) {
		selected = ':selected';
	}
	 var options = $('#'  + src + ' option' + selected).sort().clone();
	 $('#' + dst ).append(options);	
	 $('#' + src + ' option' + selected).remove();	 
}

function removeDuplicationFromSelectBox(src) {
	$('#'+ src + ' option').each(function() {
		  $(this).prevAll('option[value="' + this.value + '"]').remove();
	});
}

function clearSmartSearch() {
	$("#txtOndSearch").val('');
	clearSearchBox();
	if(isNewConfig) { 
		$('#sel_Routes option').remove();
	}
}

function generateOndCodes() {
	var searchTxt = $("#txtOndSearch").val();
	showProgressBar();
	if (validateSearchText(searchTxt)) {
		$.ajax({ 
			url : 'smartSearchServices!search.action',
			data : {
				'searchTO.searchText' : function(){return  searchTxt;},
				'searchTO.searchType' : function(){return  "OND_CODE";}
			 },	
		    beforeSend : function() {
		    	clearSearchBox();
		    },
			dataType : "json",
			cache:false,
			success:function(response){
				createONDSelectBox(response.searchTO.searchResult);
			}
		});
	}
	
	hideProgressBar();
	
}

function clearSearchBox() {
	$('#sel_GeneratedRoutes option').remove();
}

function validateSearchText(searchText) {
	var valid = true;
	if (searchText.length == 0) {
		clearSearchBox();
		valid = false;
	}
	
	return valid;
}

function createONDSelectBox(listOfOndCodes) {
	if (validateSearchText) {
		addOptionsToSelect("sel_GeneratedRoutes", listOfOndCodes);
	}
}

function createCategoryMap() {
	bundledCategoryMap = $("#selBundleCategory > option").map(function() {
	    var arr = [];
	    arr.push([$(this).val(), $(this).text()]);
	    return arr;
	}).get();	
}

function getBundleCategory(categoryID) {
	for (var i = 0;  i < bundledCategoryMap.length; i++) {
		if (bundledCategoryMap[i][0] == categoryID) {
			return bundledCategoryMap[i][1];
		}
	}
	return "";
}

// enable/disable selected services tab based on flight period
var prepareSelectedServiceTab = function () {
	
	$("#bundleTemplateTabs").tabs("option", "disabled", [5,6] );

	$("#flightPeriodFrom").change(function () {
		enableSelectedServicesTab();
	});

	$("#flightPeriodTo").change(function () {
		enableSelectedServicesTab();
	});
}

var enableSelectedServicesTab = function () {
	if ($("#flightPeriodFrom").val() && $("#flightPeriodTo").val()) {
		$("#bundleTemplateTabs").tabs("enable", 5);
	}
}

var isNotEmptyTextbox = function (textboxId) {
	if ($(textboxId).val() && $(textboxId).val().trim() !== "") {
		return true;
	}
	return false;
}

var getInputParametersForServiceSearch = function() {
	var data = {};
	data['templateId'] = $("#textServiceTemplateId").val();
	data['serviceType'] = currentEditingServiceName;
	return data;
} 

var showServiceTemplateSearchLoader = function() {
	$("#serviceTemplateLoader").css("display", "inline");
	$("#serviceTemplateName").text("")
}

var prepareServiceTemplatePopulator = function () {
	// change event handler for textServiceTemplateId text for SEAT, MEAL and BAGGAGE
	$("#textServiceTemplateId").change(function () {
		if (isNotEmptyTextbox("#textServiceTemplateId")) {
			showServiceTemplateSearchLoader();
			$.ajax({
				type: "POST",
				url: 'findBundledServiceTemplate!search.action',
				data: getInputParametersForServiceSearch(),
				success: function (response) {
					populateSelectedServiceAjaxResponse(response);
				},
				dataType: 'json'
			});
		} 
	});

	// change event handler for flexiServiceTemplateId dropdown for FLEXI
	$("#flexiServiceTemplateId").change(function () {
		var slectedTID = $("#flexiServiceTemplateId").val();
		if ('INC' === slectedTID) {
			$("#serviceTemplateName").text("Included");
		} else {
			$("#serviceTemplateName").text("Template Name");
		}
	});

	// click event handlers for addHalaTemplate, removeHalaTemplate buttons for HALA 
	$("#addHalaTemplate").click(function () { addToSelectPaneCustom('halaAvailableTemplateIds', 'halaAssignedTemplateIds', true) });
	$("#removeHalaTemplate").click(function () { addToSelectPaneCustom('halaAssignedTemplateIds', 'halaAvailableTemplateIds', true) });

	// click event handler for add service period, only base service can add new periods
	$("#btnAddServicePeriod").click(function () {
		if (selectedServices.baseService === currentEditingServiceName && validateServiceTemplatePeriod(true)) {
			var selectedPeriodService = getCurrentSelectedPeriodService();
			if (!selectedPeriodService) {
				selectedPeriodService = {
					sortingNumber: convertToDateNumber($("#serviceFromDate").val()),
					fromDate: $("#serviceFromDate").val(),
					toDate: $("#serviceToDate").val()
				}
			}

			var key = getCurrentSelectedPeriodServiceKey();
			var addingSelectedServiceDetail = {
				fromDate: $("#serviceFromDate").val(),
				toDate: $("#serviceToDate").val()
			};			
			selectedPeriodService[currentEditingServiceName] = addingSelectedServiceDetail;
			selectedServices.services[key] = selectedPeriodService;

			// update tid, template name and free services ids
			updateSelectedServiceRecord(selectedPeriodService);
			// sort selected services by period start date
			sortSelectedServices();
			fillPopupDialogServicesGrid();
		}
	});

	// click event handler for delete service period, only base service can delete existing periods
	$("#btnDeleteAllServicePeriod").click(function () {
		var deletingKey = getSeletedServiceKeyFromGrid();
		delete selectedServices.services[deletingKey];
		fillPopupDialogServicesGrid();
	});

	// click event for editing the selected service period
	$("#btnEditServicePeriod").click(function () {
		if (validateServiceChangeOperation() && validateServiceTemplatePeriod(false)) {
			var selectedService = getCurrentSelectedPeriodService();
			updateSelectedServiceRecord(selectedService);
			fillPopupDialogServicesGrid();
		}
	});

	// click event to apply changes for all the records in selected services dialog
	$("#btnApplyAllServicePeriod").click(function () {
		if (validateServiceTemplatePeriod(false)) {
			Object.keys(selectedServices.services).forEach(function (key) {
				var selectedService = selectedServices.services[key];
				updateSelectedServiceRecord(selectedService);
			});
			fillPopupDialogServicesGrid();
		}
	});

	// click event to confirm changes done for the selected services dialog
	$("#btnConfirmServicePeriod").click(function () {
		$("#addServicesDialog").dialog('close');
		populateSelectedServiceGrid();
	});

	// click event to clear the selected services dialog
	$("#btnClearServicePeriod").click(function () {
		resetAddServicesDialog(true);
	});	

	// click event to close selected services dialog with out applying changes
	$("#btnCloseServicePeriod").click(function () {
		$("#addServicesDialog").dialog('close');
		restoreCopyOfSelectedServices();
	});

	// click event handler for tranlations add
	$("#btnAddTranslationTemplate").click(function () {
		addTranslationToGrid();
	});

	// click event handler for translations reset
	$("#btnResetTranslationTemplate").click(function () {
		resetTranlationEditPanel();
	});

	// click event handler for translations delete
	$("#btnDeleteTranslationTemplate").click(function () {
		deleteTranlationRecord();
	});

	// click event hander for display content close button
	$("#btnCloseDisplaySettings").click(function () {
		$("#displaySettingsDialog").dialog('close');
		clearBundledDisplaySettings();
	});

	// click event hander for display content save button
	$("#btnConfirmDisplaySettings").click(function () {
		saveBundleDisplaySettings();
	});

	// click event hander for thumbnail upload
	$("#btnUploadTumbnailImage").click(function () {
    	var data = {};
    	data['mode'] = IMAGE_UPLOAD_TYPE.BUNDLE;
    	data['lang'] = $('#selThumbnailImageLanguage').val();
    	
		$('#frmThumbnailUpload').ajaxSubmitWithFileInputs({				 
			beforeSubmit:  beforeUploadingThumbnail,  
			success: afterUploadingThumbnail,
			data: data,
			url: 'showUpload.action',
			dataType:  'script'     
		});
		return false;
	});
}

var prepareDescriptionTemplatePopulator = function(){
	// click event hander for display content bundle description template, add button
	$("#displayTemplateId").change(function () {

		if (isNotEmptyTextbox("#displayTemplateId")) {
			var paramTID = $("#displayTemplateId").val();
			var data = {};
			data['templateID'] = paramTID;
			$.ajax({
				type: "POST",
				url: 'showBundleDescription!retrieveBundleTemplateById.action',
				data:data,
				success: function (response) {
					cleanBundleDescTemplateGrid();
					populateSelectedBundleDescTemplate(response);
				},
				dataType: 'json'
			});
		} 
	
	});
	
}

var getLanguageName = function(code) {
	var languageName = null;
	$("select#selThumbnailImageLanguage option").each(function () {
		if (code === $(this).val()) {
			languageName = $(this).text();
		}
	});
	return languageName;
}

var afterUploadingThumbnail = function (responseText, statusText)  {
	top[2].HideProgress();
    eval(responseText);		    		
    showMsg(responseText.msgType, responseText.succesMsg);
    if(responseText.msgType !== "Error"){
    	var selectedLang = responseText.lang;
    	var imageName = '';
    	$('#dvUpLoad').html($('#dvUpLoad').html());

    	if(uploadingType == IMAGE_UPLOAD_TYPE.STRIPE){
    		imageName = 'bundledFareStripe_test_' + selectedLang + imageSuffix;
    		strStripeUploads[selectedLang] = true;
    	} else {    		
    		imageName = 'bundledFare_test_' + selectedLang + imageSuffix;
    		strBundleUploads[selectedLang] = true;
		}

		displaySettings.thumbnails[selectedLang] = {
			imageUrl: bundledFareImagePath + imageName + '?rel=' + new Date().getTime(),
			code: selectedLang,
			languageName: getLanguageName(selectedLang)
		}

		populateBundleTumbnails();
    }
}

var populateBundleTumbnails = function () {
	clearTumbnails();
	var tblThumbnailImages = $("#tblThumbnailImages");
	var rowSample = $("#rowThunbnailImages");
	if (Object.keys(displaySettings.thumbnails).length == 0) {
		rowSample.show();
	} else {
		rowSample.hide();
		var count = 0;
		var rowClone = null;
		Object.keys(displaySettings.thumbnails).forEach(function (code) {			
			var index = count%4;
			var thumbnail = displaySettings.thumbnails[code];
			if(index === 0){
				rowClone = rowSample.clone(true);
				tblThumbnailImages.append(rowClone);
				rowClone.show();
			}
	
			var thumbnailImageSample = rowClone.find(".thumbnailImage_" + index);
			thumbnailImageSample.attr("src", thumbnail.imageUrl);
			var thumbnailLanguageSample = rowClone.find(".thumbnailLanguage_" + index);
			thumbnailLanguageSample.text(thumbnail.languageName);

			count++;
		});	
	}
}

var clearTumbnails = function () {
	var tblThumbnailImages = $("#tblThumbnailImages");
	var rowThunbnailImages = $("#rowThunbnailImages");
	tblThumbnailImages.empty();
	tblThumbnailImages.append(rowThunbnailImages);
}
	

var beforeUploadingThumbnail = function(formData, jqForm, options) {
	var immagePath = $(".thumbnailImageFile").val();
	if(immagePath == null || immagePath == ''){
		showMsg("Error", "Please select an image to upload");
		return false;
	} else {
		var uploadingImage = new Image();
		uploadingImage.src = $("#imgBundle").attr('src');	
		if(uploadingImage.width > 1024 || uploadingImage.height > 700){
			showMsg("Error", "Image is too large ");
			return false;
		}
		top[2].ShowProgress();
	}
}

var addTranslationToGrid = function() {
	var languageCode = $("#selTanslationTemplateLanguage").val();
	var translation = {
		code: languageCode,
		language: $( "#selTanslationTemplateLanguage option:selected" ).text() ,
		bundleName: $("#displayTranslationName").val(),
		budleDescription: $("#displayTranslationDiscription").val()
	}
	displaySettings.translations[languageCode] = translation;
	populateBundleTranslationsGrid();
}

var selectTranlationToEdit = function() {
	var rowId = $("#jqGridBundledFareTranslations").getGridParam("selrow");
	var languageCode = $("#jqGridBundledFareTranslations").getCell(rowId, "code");
	var language = displaySettings.translations[languageCode];
	$("#selTanslationTemplateLanguage").val(languageCode);
	$("#displayTranslationName").val(language.bundleName);
	$("#displayTranslationDiscription").val(language.budleDescription);
}

var resetTranlationEditPanel = function() {
	$("#jqGridBundledFareTranslations").resetSelection();
	$("#displayTranslationName").val("");
	$("#displayTranslationDiscription").val("");
}

var deleteTranlationRecord = function() {
	var rowId = $("#jqGridBundledFareTranslations").getGridParam("selrow");
	if(rowId) {
		var languageCode = $("#jqGridBundledFareTranslations").getCell(rowId, "code");
		delete displaySettings.translations[languageCode];
	}
	populateBundleTranslationsGrid();
}

var restoreCopyOfSelectedServices = function() {
	// user is closing without confirming changes, restore the original before editing  
	selectedServices = selectedServicesCopy;
	populateSelectedServiceGrid();
}

var getCurrentSelectedPeriodService = function () {
	var key = getCurrentSelectedPeriodServiceKey();
	return selectedServices.services[key];
}

var getCurrentSelectedPeriodServiceKey = function () {
	return $("#serviceFromDate").val() + "_" + $("#serviceToDate").val();
}

var updateSelectedServiceRecord = function (selectedService) {
	if (selectedService) {
		var tid = getSelectedServiceTID();
		selectedService[currentEditingServiceName].id = tid;
		selectedService[currentEditingServiceName].templateName = getSelectedServiceTemplateName(tid);
		if ('HALA' === currentEditingServiceName) {
			setHalaFreeServicesIds(selectedService);
		} else if('MEAL' === currentEditingServiceName) {
			setMultiMealSelectionFlag(selectedService);
		}
	}
}

var setMultiMealSelectionFlag = function(selectedService) {
	if ($("#chkMleMultiple").attr('checked')) {
		selectedService[currentEditingServiceName].allowMultipleSelect = true;
	} else {
		selectedService[currentEditingServiceName].allowMultipleSelect = false;
	}
}

var setHalaFreeServicesIds = function (selectedService) {
	selectedService[currentEditingServiceName].freeServiceIds = [];
	$("select#halaAssignedTemplateIds option").each(function () {
		selectedService[currentEditingServiceName].freeServiceIds.push(parseInt($(this).val(), 10));
	});
}

var isSelectedServiceIDValid = function() {
    var valid = true;
    if (selectedServices.baseService === currentEditingServiceName && currentEditingServiceName !== 'FLEXI' && currentEditingServiceName !== 'HALA') {
        valid = $("#textServiceTemplateId").val().trim() !== "";
    }
    return valid;
}

var getSelectedServiceTID = function () {
	var slectedServiceTid = null;
	if ('FLEXI' === currentEditingServiceName) {
		slectedServiceTid = $("#flexiServiceTemplateId").val();
	} else if ('HALA' === currentEditingServiceName && $("select#halaAssignedTemplateIds option").length > 0) {
		slectedServiceTid = "ACT";
	} else {
		slectedServiceTid = $("#textServiceTemplateId").val();
	}
	return slectedServiceTid;
}

var getSelectedServiceTemplateName = function (tid) {
	var selectedServiceTemplateName = null;
	if (tid !== null && tid !== "") {
		if ('HALA' === currentEditingServiceName) {
			$("select#halaAssignedTemplateIds option").each(function () {
				selectedServiceTemplateName += ", " + $(this).text();
			});
			selectedServiceTemplateName = selectedServiceTemplateName.substring(6);
		} else {
			selectedServiceTemplateName = $("#serviceTemplateName").text();
			if('MEAL' === currentEditingServiceName && $("#chkMleMultiple").attr('checked')) {
				selectedServiceTemplateName += " (M)";	
			}
		}
	}
	return selectedServiceTemplateName;
}

var populateSelectedServiceAjaxResponse = function (response) {
	if (response.success) {
		currentEditingService = response;
		$("#serviceTemplateLoader").css("display", "none");
		$("#serviceTemplateName").text(response.templateName);
		$("#noteTemplateValidity").show();
		populateFromToDateAjaxResponse(response);
		populateTemplateValidityNoteAjaxResponse(response);
	} else {
		currentEditingService = null;
		showMsg("Error", response.messageTxt);
		resetAddServicesDialog(false);
	}
}

var populateFromToDateAjaxResponse = function(response) {
	// none base service periods should not be editable
	if (currentEditingServiceName == selectedServices.baseService && response.fromDate && response.toDate) {
		var fromDateAsNumber = convertToDateNumber(response.fromDate);
		var toDateAsNumber = convertToDateNumber(response.toDate);
		var flightValidityFromNumber = convertToDateNumber($("#flightPeriodFrom").val());
		var flightValidityToNumber = convertToDateNumber($("#flightPeriodTo").val());

		if(fromDateAsNumber < flightValidityFromNumber && flightValidityFromNumber < toDateAsNumber ) {
			$("#serviceFromDate").val($("#flightPeriodFrom").val());
		} else {
			$("#serviceFromDate").val(response.fromDate);
		}

		if(fromDateAsNumber < flightValidityToNumber && flightValidityToNumber < toDateAsNumber ) {
			$("#serviceToDate").val($("#flightPeriodTo").val());
		} else {
			$("#serviceToDate").val(response.toDate);
		}		
	}
}

var populateTemplateValidityNoteAjaxResponse = function (response) {
	if (response.fromDate) {
		$("#noteTemplateValidityFrom").text(response.fromDate);
	} else {
		$("#noteTemplateValidityFrom").text($("#flightPeriodFrom").val());
	}

	if (response.toDate) {
		$("#noteTemplateValidityTo").text(response.toDate);
	} else {
		$("#noteTemplateValidityTo").text($("#flightPeriodTo").val());
	}
}

var getSeletedServiceKeyFromGrid = function () {
	var rowId = $("#jqGridBundledFareServices").getGridParam("selrow");
	var rowFromDate = $("#jqGridBundledFareServices").getCell(rowId, "fromDate");
	var rowToDate = $("#jqGridBundledFareServices").getCell(rowId, "toDate");
	var selectedKey = rowFromDate + "_" + rowToDate;
	return selectedKey;
}

var sortSelectedServices = function () {
	var selectedServicesArray = [];
	Object.keys(selectedServices.services).forEach(function (key) {
		var selectedService = selectedServices.services[key];
		selectedServicesArray.push(selectedService);
		delete selectedServices.services[key];
	});

	selectedServicesArray.sort(function (a, b) {
		return a.sortingNumber - b.sortingNumber;
	});

	selectedServicesArray.forEach(function (service) {
		var serviceKey = service.fromDate + "_" + service.toDate;
		selectedServices.services[serviceKey] = service;
	});
}

var resetAddServicesDialog = function (withSelectedRow) {
	if (withSelectedRow) {
		$("#jqGridBundledFareServices").clearGridData();
	}
	$("#serviceTemplateLoader").css("display", "none");
	$("#serviceTemplateName").text("Template Name");
	$("#noteTemplateValidity").hide();
	$("#serviceFromDate").val("");
	$("#serviceToDate").val("");
	$("#textServiceTemplateId").val("");
	$("#flexiServiceTemplateId").val("");
	$("#chkMleMultiple").attr('checked', false);
	resetAllToAvailablePane('halaAssignedTemplateIds', 'halaAvailableTemplateIds');
}

var populateSelectedServiceGrid = function () {
	clearSelectedServiceGrid();
	var selectedServiceRow = $("#rowSelectedServices");
	var cellSample = $("#cellSelectedServicesSample");

	if (Object.keys(selectedServices.services).length == 0) {
		cellSample.show();
	} else {
		cellSample.hide();
		Object.keys(selectedServices.services).forEach(function (key) {
			var selectedService = selectedServices.services[key];
			var cellClone = cellSample.clone(true);

			var timePeriodSample = cellClone.find(".timePeriodSample");
			var timePeriodTitle = selectedService.fromDate + " - " + selectedService.toDate;
			timePeriodSample.text(timePeriodTitle);
			
			var displayKey = selectedService.bundleFarePeriodId;

			attachDetailsToSampleCellClone('BAGGAGE', cellClone, selectedService);
			attachDetailsToSampleCellClone('MEAL', cellClone, selectedService);
			attachDetailsToSampleCellClone('SEAT', cellClone, selectedService);
			attachDetailsToSampleCellClone('FLEXI', cellClone, selectedService);
			attachDetailsToSampleCellClone('HALA', cellClone, selectedService);
			attachBundleFeeToCellClone(key, cellClone, selectedService);
			attachDisplaySettings(key, cellClone, timePeriodTitle);

			selectedServiceRow.append(cellClone);
			cellClone.show();
		});
	}
}

var attachBundleFeeToCellClone = function (key, cellClone, selectedService) {
	var bundleFeeSample = cellClone.find(".bundleFeeSample");
	bundleFeeSample.attr("name", key);
	bundleFeeSample.css("display", "inline");
	bundleFeeSample.numeric({allowDecimal:true});
		
	if(selectedService.bundleFee != null) {
		bundleFeeSample.val(selectedService.bundleFee);
	}

	bundleFeeSample.change(function () {
		bundleFeePeriodValue = $(this).val();
		bundleFeePeriodNameAsKey = $(this).attr('name');
		slectedServiceForFee = selectedServices.services[bundleFeePeriodNameAsKey];
		slectedServiceForFee.bundleFee = bundleFeePeriodValue;
	});
}

var attachDetailsToSampleCellClone = function (serviceName, cellClone, selectedService) {
	var serviceDetailSample = cellClone.find("." + serviceName.toLowerCase() + "DetailSample");
	if (selectedService[serviceName] && selectedService[serviceName].id) {
		var formattedTemplateName = selectedService[serviceName].templateName;
		if (formattedTemplateName && formattedTemplateName.length > 45) {
			formattedTemplateName = formattedTemplateName.substring(0, 40) + " ....";
		}
		serviceDetailSample.text(formattedTemplateName + " (" + selectedService[serviceName].id + ")");
	}
}

var clearSelectedServiceGrid = function () {
	var selectedServiceRow = $("#rowSelectedServices");
	var cellSample = $("#cellSelectedServicesSample");
	selectedServiceRow.empty();
	selectedServiceRow.append(cellSample);
}

var validateServiceChangeOperation = function(){
	var servicePeriodSelected = $("#jqGridBundledFareServices").getGridParam("selrow");
	if(!servicePeriodSelected){
		showMsg("Error", "Please select the required service period from the 'Selected Services Periods'");
		return false;
	}
	return true;
}

var validateServiceTemplatePeriod = function (withOverlapping) {
	var serviceFromDate = convertToDateNumber($("#serviceFromDate").val());
	var serviceToDate = convertToDateNumber($("#serviceToDate").val());

	var flightPeriodFrom = convertToDateNumber($("#flightPeriodFrom").val());
	var flightPeriodTo = convertToDateNumber($("#flightPeriodTo").val());

	var templateFromDate = convertToDateNumber($("#noteTemplateValidityFrom").text());
	var templateToDate = convertToDateNumber($("#noteTemplateValidityTo").text());

	var validation = true;
	
	if(!isSelectedServiceIDValid()){
		showMsg("Error", "Please fill the required template Id (TID)");
	    validation = false;
	    
	}else if (!isNotEmptyTextbox("#serviceFromDate")) {
	    showMsg("Error", "From Date is required");
	    validation = false;

	} else if (!isNotEmptyTextbox("#serviceToDate")) {
	    showMsg("Error", "To Date is required");
	    validation = false;

	} else if (serviceToDate < serviceFromDate) {
	    showMsg("Error", "Invalid time period");
	    validation = false;

	} else if (serviceFromDate < flightPeriodFrom || flightPeriodTo < serviceToDate) {
	    showMsg("Error", "Mismatching time period");
	    validation = false;

	} else if (serviceFromDate < templateFromDate || templateToDate < serviceToDate) {
	    showMsg("Error", "Invalid time period");
	    validation = false;

	} else if (withOverlapping && isOverlappingPeriod()) {
	    showMsg("Error", "Overlapping time period");
	    validation = false;
	}

	return validation;
}

var fillPopupDialogServicesGrid = function () {
	var i = 0;
	$("#jqGridBundledFareServices").clearGridData();
	Object.keys(selectedServices.services).forEach(function (key) {
		i++;
		$("#jqGridBundledFareServices").addRowData(i, selectedServices.services[key][currentEditingServiceName]);
	});
}

var isOverlappingPeriod = function () {
	var serviceFromDate = convertToDateNumber($("#serviceFromDate").val());
	var serviceToDate = convertToDateNumber($("#serviceToDate").val());
	var isOverlapping = false;
	Object.keys(selectedServices.services).forEach(function (key) {
		var selectedService = selectedServices.services[key];
		var existingFromDate = convertToDateNumber(selectedService.fromDate);
		var existingToDate = convertToDateNumber(selectedService.toDate);
		if (existingFromDate <= serviceToDate && serviceFromDate <= existingToDate) {
			isOverlapping = true;
		}
	});
	return isOverlapping;
}

var convertToDateNumber = function (dateAsString) {
	var day = dateAsString.substring(0, 2);
	var month = dateAsString.substring(3, 5);
	var year = dateAsString.substring(6);
	return parseInt(year + month + day);
}

var prepareServiceTroggleEditButtons = function () {
	$("#btnEditBgd").disableButton();
	$("#btnEditMle").disableButton();
	$("#btnEditSte").disableButton();
	$("#btnEditFlx").disableButton();
	$("#btnEditHla").disableButton();

	$("#btnEditBgd").click(function () {
		selectedServiceEditButtonOnClick("BAGGAGE");
	});

	$("#btnEditMle").click(function () {
		selectedServiceEditButtonOnClick("MEAL");
	});

	$("#btnEditSte").click(function () {
		selectedServiceEditButtonOnClick("SEAT");
	});

	$("#btnEditFlx").click(function () {
		selectedServiceEditButtonOnClick("FLEXI");
	});

	$("#btnEditHla").click(function () {
		selectedServiceEditButtonOnClick("HALA");
	});
}

var selectedServiceEditButtonOnClick = function (serviceName) {
	currentEditingServiceName = serviceName;
	populateSelectedServicesData(serviceName);
	initAddServiceDialog(serviceName);	
}

var initAddServiceDialog = function (serviceName) {
	$("#addServicesDialog").dialog({ 
		height: 590, 
		width: 890, 
		title: "Add/Edit Selected Services [" + serviceName + "]", 
		modal: true
	});

	$("#jqGridBundledFareServices").jqGrid({
		datatype: "local",
		height: 200,
		colNames: ['ID', 'Template Name', 'From Date', 'To Date'],
		colModel: [
			{ name: 'id', index: 'id', width: 70, sorttype: "int", align: "center" },
			{ name: 'templateName', index: 'templateName', width: 400 },
			{ name: 'fromDate', index: 'fromDate', width: 190, sorttype: "date" },
			{ name: 'toDate', index: 'toDate', width: 190, sorttype: "date" }
		],
		multiselect: false,
		onSelectRow: function (rowid) {
			selectSelectedServiceForEdit();
		}
	});
	resetAddServicesDialog(true);
	$("#jqGridBundledFareServices").clearGridData();
	$("#noteFlightValidityFrom").text($("#flightPeriodFrom").val());
	$("#noteFlightValidityTo").text($("#flightPeriodTo").val());
	$("#noteTemplateServiceName").text(serviceName);
	fillPopupDialogServicesGrid();

	// keep the copy of current selected services if user desides to cancel the changes done in popup dialog
	selectedServicesCopy = $.extend(true, {}, selectedServices);

	if (selectedServices.baseService === currentEditingServiceName) {
		$("#btnAddServicePeriod").css("display", "inline");
		$("#btnDeleteAllServicePeriod").css("display", "inline");
		$("#btnEditServicePeriod").css("display", "none");
		$("#serviceFromDate,#serviceToDate").attr("disabled", false);
	} else {
		$("#btnAddServicePeriod").css("display", "none");
		$("#btnDeleteAllServicePeriod").css("display", "none");
		$("#btnEditServicePeriod").css("display", "inline");
		$("#serviceFromDate,#serviceToDate").attr("disabled", true);
	}

	if('MEAL' === currentEditingServiceName) {
		$("#divAllowMultipleMeal").css("display", "block");
	} else {
		$("#divAllowMultipleMeal").css("display", "none");
	}

	if ('FLEXI' === currentEditingServiceName) {
		$("#flexiServiceTemplateId").css("display", "inline");
		$("#textServiceTemplateId").css("display", "none");
		$("#tblServiceTemplate").css("display", "inline");
		$("#tblHalaServiceTemplate").css("display", "none");
	} else if ('HALA' === currentEditingServiceName) {
		$("#tblServiceTemplate").css("display", "none");
		$("#tblHalaServiceTemplate").css("display", "inline");
	} else { 
		// for all other service types
		$("#flexiServiceTemplateId").css("display", "none");
		$("#textServiceTemplateId").css("display", "inline");
		$("#tblServiceTemplate").css("display", "inline");
		$("#tblHalaServiceTemplate").css("display", "none");
	}
}


var selectSelectedServiceForEdit = function () {
	if (selectedServices.baseService === currentEditingServiceName) {
		// base service cannot be edited, to simplyfy the workflow.
		// if different periods required for base service it should done via add/delete
		resetAddServicesDialog(false);
	} else {
		var serviceKeyToEdit = getSeletedServiceKeyFromGrid();
		var serviceTemplateToEdit = selectedServices.services[serviceKeyToEdit][currentEditingServiceName];

		if ('HALA' === currentEditingServiceName) {
			selectHalaServicesForEdit(serviceTemplateToEdit.freeServiceIds);
		} else {
			$("#textServiceTemplateId").val(serviceTemplateToEdit.id);
			$("#serviceTemplateName").text(serviceTemplateToEdit.templateName);
			if('MEAL' === currentEditingServiceName && serviceTemplateToEdit.allowMultipleSelect) {
				$("#chkMleMultiple").attr('checked', true)
			} else {
				$("#chkMleMultiple").attr('checked', false)
			}
		}

		$("#serviceFromDate").val(serviceTemplateToEdit.fromDate);
		$("#serviceToDate").val(serviceTemplateToEdit.toDate);
	}
}

var selectHalaServicesForEdit = function (freeServiceIds) {
	resetAllToAvailablePane('halaAssignedTemplateIds', 'halaAvailableTemplateIds');
	$("select#halaAvailableTemplateIds option").map(function () {
		optVal = $(this).val();
		optText = $(this).text();
		freeServiceIds.forEach(function (selectedFreeServiceId) {
			if (selectedFreeServiceId == (parseInt(optVal, 10))) {
				addValuesToSelect(optVal, optText, 'halaAvailableTemplateIds', 'halaAssignedTemplateIds');
			}
		});
	});
}

var calculateBaseService = function () {
	if ($("#chkBgdService").attr('checked')) {
		selectedServices.baseService = 'BAGGAGE';
	} else if ($("#chkMleService").attr('checked')) {
		selectedServices.baseService = 'MEAL';
	} else if ($("#chkSteService").attr('checked')) {
		selectedServices.baseService = 'SEAT';
	} else if ($("#chkFlxService").attr('checked')) {
		selectedServices.baseService = 'FLEXI';
	} else if ($("#chkHlaService").attr('checked')) {
		selectedServices.baseService = 'HALA';
	}
}

/**
 * Service toggles work-flows
 */
var prepareServiceToggleCheckBoxes = function () {
	$("#chkBgdService").click(function () {
		selectedServiceCheckBoxClick(this.id, this.checked, 'BAGGAGE');
	});

	$("#chkMleService").click(function () {
		selectedServiceCheckBoxClick(this.id, this.checked, 'MEAL');
	});

	$("#chkSteService").click(function () {
		selectedServiceCheckBoxClick(this.id, this.checked, 'SEAT');
	});

	$("#chkFlxService").click(function () {
		selectedServiceCheckBoxClick(this.id, this.checked, 'FLEXI');
	});

	$("#chkHlaService").click(function () {
		selectedServiceCheckBoxClick(this.id, this.checked, 'HALA');
	});
}

var selectedServiceCheckBoxClick = function(checkBox, selected, serviceName) {
    if (validateCheckBoxTransition(checkBox, selected, serviceName)) {
        calculateBaseService();
        toggleServiceEditButton(selected, serviceName);
        troggleSelectedServicesData(selected, serviceName);
    }
}


/**
 * service removal==> control upon user confirmation
 */
var validateCheckBoxTransition = function(checkBox, selected, serviceName) {
    var proceed = true;
    if (!selected && isServiceLinked(serviceName)) {
    	var userMessage = formatPlaceHolder(delinkConfirmation, serviceName);
         proceed = confirm(userMessage);
        if (!proceed) {
            $("#" + checkBox).attr('checked', true);
        }
    }
    return proceed;
}

var isServiceLinked = function(serviceName) {
	var servicesLinked = false;
	var availableServices = getSelectedServicesNames();
	if (availableServices && availableServices[serviceName]) {
		servicesLinked = true;
	}
	return servicesLinked;
}

var toggleServiceEditButton = function (selected, serviceName) {
	var serviceButton = null;

	if ('BAGGAGE' === serviceName) {
		serviceButton = $("#btnEditBgd");
	} else if ('MEAL' === serviceName) {
		serviceButton = $("#btnEditMle");
	} else if ('SEAT' === serviceName) {
		serviceButton = $("#btnEditSte");
	} else if ('FLEXI' === serviceName) {
		serviceButton = $("#btnEditFlx");
	} else if ('HALA' === serviceName) {
		serviceButton = $("#btnEditHla");
	}

	if (serviceButton) {
		if (selected) {
			serviceButton.enableButton();
		} else {
			serviceButton.disableButton();
		}
	}
}

var troggleSelectedServicesData = function (selected, serviceName) {
	if (selected) {
		populateSelectedServicesData(serviceName);
	} else {
		clearSelectedServicesData(serviceName);
	}
}

var clearSelectedServicesData = function (serviceName) {
	Object.keys(selectedServices.services).forEach(function (key) {
		var selectedService = selectedServices.services[key];
		selectedService[serviceName] = null;
	});
	populateSelectedServiceGrid();
}

var populateSelectedServicesData = function (serviceName) {
	Object.keys(selectedServices.services).forEach(function (key) {
		var selectedService = selectedServices.services[key];
		if (!selectedService[serviceName]) {
			selectedService[serviceName] = {};
			selectedService[serviceName].freeServiceIds = [];
			selectedService[serviceName].allowMultipleSelect = false;
		}
		selectedService[serviceName].fromDate = selectedService.fromDate;
		selectedService[serviceName].toDate = selectedService.toDate;
	});
	populateSelectedServiceGrid();
}

var addToSelectPane = function (src, dest) {
    var optVal;
    var optText;

    $("select#" + src + " option:selected").each(
        function () {
            optVal = $(this).val();
            optText = $(this).text();
            addValuesToSelect(optVal, optText, src, dest);
        });

}

var removeFromSelectPane = function (dest, src) {
    var optVal;

    $("select#" + dest + " option:selected").each(
        function () {
            optVal = $(this).val();
            optText = $(this).text();
            removeValuesFromSelect(optVal, optText, dest, src);
        });
}

var resetAllToAvailablePane = function(dest, src){
	$("select#" + dest + " option").map(function() {
		optVal = $(this).val();
        optText = $(this).text();
        removeValuesFromSelect(optVal, optText, dest, src);
	});
}

var addValuesToSelect = function(optVal, optText, src, dest){	
	if ($("select#" + dest + " option[value='" + optVal + "']").length == 0) {
    	if ( $("#"+dest+" option[value='"+optVal+"']").length == 0 ){                                		
    		$("#" + dest).append("<option value=\"" + optVal + "\">" + optText + "</option>");
    	}
        $("select#" + src + " option").remove("[value='" + optVal + "']");
    }
}
var removeValuesFromSelect = function(optVal, optText, dest, src){
	if ( $("#"+src+" option[value='"+optVal+"']").length == 0 ){                                	  
    	$("#" + src).append("<option value=\"" + optVal + "\">" + optText + "</option>");
    }
    $("select#" + dest + " option").remove("[value='" + optVal + "']");
}

function createGrid(){
	
	listFormatter = function(cellVal, options, rowObject){
		var returnStr = "";
		
		if (cellVal != null && cellVal.length > 0){
			var arrLength = cellVal.length;
			for(var i=0;i<arrLength;i++){
					var value = replaceall(cellVal[i],"***","All");
					if(i == (arrLength - 1)){
						returnStr += value;
					} else {					
						returnStr += value +"<br/>";
					}			
			}
		} else{
			
			returnStr = "&nbsp;";
			
		}
		return returnStr;
	}
	
	listCountFormater = function(cellVal, options, rowObject){
		var returnStr = "";
		
		if (cellVal != null && cellVal.length > 0){
			var arrLength = cellVal.length;
			if (options.colModel.jsonmap == 'ondCodes') {
				returnStr += arrLength;
			} else {
				returnStr = "&nbsp;";	
			}
		} else {			
			returnStr = "0";			
		}
		return returnStr;
	}
		
	
	serviceFormatter = function(cellVal, options, rowObject){
		var returnStr = "";
		if (cellVal != null && cellVal.length > 0){
			var arrLength = cellVal.length;
			for(var i=0;i<arrLength;i++){
				if(i == (arrLength - 1)){
					returnStr += getSubString(cellVal[i].serviceName, 4).toUpperCase();
				} else {					
					returnStr += getSubString(cellVal[i].serviceName, 4).toUpperCase() +"/";
				}
			}
		}
		return returnStr;
	}
	
	getSubString = function(stringValue, numberOfLetters) {
		var value = '';
		if (stringValue != null || stringValue != "") {
			if (stringValue.length > numberOfLetters){
				value = stringValue.substring(0,numberOfLetters);
			} else {
				value = stringValue;
			}
		}
		return value;
	}
	
	
	statusFormatter = function(cellVal, options, rowObject){
		if(cellVal){
			return "ACT";
		} else {
			return "INA";
		}
	}
	
	categoryFormatter = function(cellVal, options, rowObject){
		return getBundleCategory(cellVal)
	}
	
	var channelFomatter = function(cellVal, options, rowObject){
		var treturn = "&nbsp;";
		if (cellVal!=null){
			var arrLength = dataMap.Channels.length;
			for(var i=0;i<arrLength;i++){
				if ($.inArray(dataMap.Channels[i][0], cellVal)>-1){					
					treturn += dataMap.Channels[i][1] +"/";					
				}
			}
			if (isMatchLastCharcter(treturn, '/')) {
				treturn = removeLastChar(treturn);
			}
		}
		return treturn;
	}
	
	removeLastChar = function(stringValue) {
		return stringValue.substring(0, stringValue.length-1);
	}
	
	isMatchLastCharcter = function(stringValue, matchChar) {
		var match = true;
		if (stringValue.charAt(stringValue.length - 1) != matchChar) {
			match = false;
		}
		return match;
	}
	
	openClickedTab = function(columnID) {
		switch(columnID) {
			case 1 : $( "#bundleTemplateTabs" ).tabs({ active: 0 });break;
			case 2 : $( "#bundleTemplateTabs" ).tabs({ active: 0 });break;
			case 3 : $( "#bundleTemplateTabs" ).tabs({ active: 1 });break;
			case 4 : $( "#bundleTemplateTabs" ).tabs({ active: 3 });break;
			case 5 : $( "#bundleTemplateTabs" ).tabs({ active: 2 });break;
			case 6 : $( "#bundleTemplateTabs" ).tabs({ active: 2 });break;
			case 7 : $( "#bundleTemplateTabs" ).tabs({ active: 5 });break;
			case 8 : $( "#bundleTemplateTabs" ).tabs({ active: 0 });break;
		
		}
	}
	
	$('#jqGridBundledFaresData').jqGrid({
		url : "manageBundledFaresConfiguration!search.action",
		postData : {
			'searchCriteria.bundledFareName' : function(){return $("#selBundledFareNameSearch").val()},
			'searchCriteria.bundledCategoryID' : function(){return $("#selBundleCategory").val()},
			'searchCriteria.ondCode' : function(){return $("#selBundleOND").val()},
			'searchCriteria.bookingClass' :function(){return $("#selBundleBookingClasses").val()},
			'searchCriteria.status' :function(){return $("#selBundleStatus").val()},
			'newSearch' : function() { return newSearch; }
		 },
		datatype : 'json',
		jsonReader : {
			root: 'bundledFareCriterias', 
			page: "page",
			total: "totalPages",
			records: "totalRecords",
			repeatitems: false,	
			id: 'id'	
		},
		colNames:['ID','Fare Class','Category','OND(s)','BC','Agents', 'Channels','Services','Status'],
		colModel:[
		{name:'id', jsonmap:"id",align:"center", width:20},
		{name:'bundledFareName',  jsonmap:'bundledFareName'},
		{name:'bundleCategory',  jsonmap:'bundledCategoryID', width:50, formatter: categoryFormatter},		
		{name:'ondCodes',  jsonmap:'ondCodes',formatter:listCountFormater,width:50 },
		{name:'bookingClasses', jsonmap:'bookingClasses', width:50, align:"center",formatter:listFormatter},
		{name:'Agents', jsonmap:'agents', formatter:listCountFormater,width:50 },
		{name:'channels', jsonmap:"applicableSalesChannels" ,formatter:channelFomatter},
		{name:'Services', jsonmap:'freeServices', formatter:serviceFormatter, hidden:true},
		{name:'status',  jsonmap:'status', width:40, formatter: statusFormatter}
		],
		rowNum : 10,
		viewRecords : true,
		height : 150,
		width : 900,
		pager : '#jqGridBundledFaresPages',
		afterInsertRow : function(rowid,rowdata,rowelement ){
			bundledFareConfig[rowelement.id] = rowelement;
		},
		onSelectRow : function(rowid,status,e){
			selectedBundleFareConfig = bundledFareConfig[rowid];
			panelsEditable= false;
			editBundleFares();
		},
		onCellSelect : function(rowId, iCol, content, event) {
			openClickedTab(iCol);
		},
		loadComplete : function(data){
			if(data != null && data.responseText != null && data.responseText != ''){				
				var responseObj = $.parseJSON(data.responseText);
				if(responseObj.bundledFareNamesHtml != null && responseObj.bundledFareNamesHtml != ''){
					$("#selBundledFareNameSearch").html(responseObj.bundledFareNamesHtml);
				}
			}
		}
	});
}

searchBundledFares = function(){
	newSearch = true;
	panelsEditable= true;
	$('#jqGridBundledFaresData').trigger("reloadGrid");
	newSearch = false;
	selectedBundleFareConfig = null;
	resetBundledFares();
	$('#jqGridBundledFaresContainer').show();
	$("#bundledFaresDetails").hide();
}

saveBundledFare = function(){
	if(validateData()){
		var submitURL=isNewConfig?"manageBundledFaresConfiguration!add.action":"manageBundledFaresConfiguration!edit.action";
		var data = getBundleFareData(isNewConfig);
		
		$.ajax({
			type: "POST",
			url: submitURL,
			data: data,
			success: successFunction,
			dataType: 'json'
		});
	}
}

successFunction = function(response){
	if(response.success){
		showMsg("Success",response.messageTxt);
		searchBundledFares();
	}else{
		showMsg("Error",response.messageTxt);
	}
	hideProgressBar();
}

getBundleFareData = function(isNewConfig){
	var data = {};
	
	data['flightDateFrom'] = transformDate($('#flightPeriodFrom').val(), true);
	data['flightDateTo'] = transformDate($("#flightPeriodTo").val(), false);
	data['ondCodes'] = getSelectBoxData('sel_Routes');
	data['agents'] = getSelectBoxData('sel_Agents');
	data['bookingClasses'] = getSelectBoxData('sel_BookingClassAssigned');
	data['flightNumbers'] = getSelectBoxData('sel_FlightNoAssigned');
	data['serviceTemplates'] = JSON.stringify(transformServiceTemplates());
	data["applicableSalesChannels"] = getSelectedSelectBoxData("selChanel");
	data["pointOfSales"] = getSelectBoxData("sel_CountryAssigned");
	
	var bundledFareData = {};	
	bundledFareData['id'] = isNewConfig ? null : selectedBundleFareConfig.id;
	bundledFareData['bundledFareName'] = $("#bundledFareName").val();
	bundledFareData['bookingClass'] = null; //TODO:RW remove this $("#sel_BookingClass").val();	
	bundledFareData['chargeCode'] = $("#selChargeCode").val();
	bundledFareData['bundleFee'] = $("#txtBundledFee").val();
	bundledFareData['cutOffTime'] = $("#txtCutOffTime").val();	
	bundledFareData['defaultBundled'] = $("#chkDefault").is(':checked') == true ? 'Y' : 'N';
	bundledFareData['version'] = isNewConfig ? -1 : selectedBundleFareConfig.version;
	bundledFareData['bundledCategoryID'] = $('#selCategory').val();	
	bundledFareData['status'] = $("input[name='radStatus']:checked").val() == 'ACT' ? true : false;
	
	data['bundledFare'] = JSON.stringify(bundledFareData);
	return data;
}

//Transfrom (FE structures) to (BE structures)
var transformServiceTemplates = function() {
    var transformedTemplates = new Array();
    Object.keys(selectedServices.services).forEach(function(key) {
        var transformedTemplate = {};
        var selectedTemplate = selectedServices.services[key];
        
        transformPackageConstraints(transformedTemplate, selectedTemplate);
        transformBaagge(transformedTemplate, selectedTemplate['BAGGAGE']);
        transformMeal(transformedTemplate, selectedTemplate['MEAL']);
        transformSeat(transformedTemplate, selectedTemplate['SEAT']);
        transformFlexi(transformedTemplate, selectedTemplate['FLEXI']);
        transformHala(transformedTemplate, selectedTemplate['HALA']);

        transformedTemplates.push(transformedTemplate);
    });
    return transformedTemplates;
}

var transformPackageConstraints = function(transformedTemplate, selectedTemplate) {
    transformedTemplate['flightFrom'] = transformDate(selectedTemplate.fromDate, true);
    transformedTemplate['flightTo'] =  transformDate(selectedTemplate.toDate, false);
    transformedTemplate['bundleFee'] = selectedTemplate.bundleFee;
    transformedTemplate['version'] = -1;
    transformedTemplate['bundledServices'] = new Array();
}

var transformBaagge = function(transformedTemplate, baggage) {
    if (baggage && baggage.id) {
        var transformedBaggage = transformObject(freeServices.BAGGAGE, baggage.id, null, false);
        transformedTemplate['bundledServices'].push(transformedBaggage);
    }
}


var transformMeal = function(transformedTemplate, meal) {
    if (meal && meal.id) {
        var transformedMeal = transformObject(freeServices.MEAL, meal.id, null, meal.allowMultipleSelect); 
        transformedTemplate['bundledServices'].push(transformedMeal);
    }
}


var transformSeat = function(transformedTemplate, seat) {
    if (seat && seat.id) {
        var transformedSeat = transformObject(freeServices.SEAT, seat.id, null, false);
        transformedTemplate['bundledServices'].push(transformedSeat);
    }
}


var transformFlexi = function(transformedTemplate, flexi) {
    if (flexi && flexi.id && flexi.id === 'INC' ) {
        var transformedFlexi = transformObject(freeServices.FLEXI, null, null, false);
        transformedTemplate['bundledServices'].push(transformedFlexi);
    }
}

var transformHala = function(transformedTemplate, hala) {
    if (hala && hala.id && hala.id === 'ACT') {
        var halaSelected = !$.isEmptyObject(hala.freeServiceIds);
        if (halaSelected) {
            var transformedHala = transformObject(freeServices.HALA, null, hala.freeServiceIds, false);
            transformedTemplate['bundledServices'].push(transformedHala);
        }
    }
}

var transformObject = function(serviceName, id, includedFreeServices, allowMultipleSelect) {
    return {
        'id': null,
        'serviceName': serviceName,
        'templateId': id,
        'includedFreeServices': includedFreeServices,
        'version': -1,
        'allowMultipleSelect': allowMultipleSelect
    };
}

var transformDate = function(strDate, start) {
    var day = strDate.substring(0, 2);
    var month = strDate.substring(3, 5);
    var year = strDate.substring(6);
    var time = start ? 'T00:00:00' : 'T23:59:59'
    return (year + "-" + month + "-" + day + time);
}
	
validateData = function(){	
	
	if($("#bundledFareName").val()==""){
		showERRMessage(bundledFareNameRequired);
		return false;
	}
	
	if ($("#sel_Routes>option").length==0){
		showERRMessage(ondCodeRequired);
		return false;
	}
	
	if(($('#flightPeriodFrom').val()!="") &&  ($("#flightPeriodTo").val()!="")){	
		var applicableFrom = $("#flightPeriodFrom").datepicker("getDate");
		var applicableTo = $("#flightPeriodTo").datepicker("getDate");
		if(applicableFrom>applicableTo){
			showERRMessage(toCannotLessThanFromDate);
			return false;
		}
	}
	
	if(Object.keys(selectedServices.services).length == 0){
		showERRMessage(bundledFareFreeServiceRequired);
		return false;
	}
	
	var msg = complianceForCheckBoxesAndSelectedServices();
	if(!msg.valid && msg.errorMsg !== ""){
		showERRMessage(msg.errorMsg);
		return false;
	}
		
	if($("#selChargeCode").val()==""){
		showERRMessage(chargeCodeRequired);
		return false;
	}
	
	if($("#txtBundledFee").val()==""){
		showERRMessage(bundledFeeRequired);
		return false;
	}
	
	if(($("#txtCutOffTime").val()=="") || ($("#txtCutOffTime").val() < 0) || ($("#txtCutOffTime").val() > 99)){
		showERRMessage(cutOffTimeInvalid);
		return false;
	}
	
	if($("#selChanel").val()==null){
		showERRMessage(promoCodeChlSelectionRequired);
		return false;
	}
	
	return true;
}

var loadInitdata = function(){
	$.ajax({ 
		url : 'loadBundledFaresInitData.action',
		beforeSend : showProgressBar(),
		type : "GET",
		async:false,
		dataType : "json",
		complete:function(response){
			 var response = eval("(" + response.responseText + ")");			
			 dataMap.FlightNo = response.flightNos;
			 allFlightNos = response.flightNos;
			 dataMap.Agents = objectFormat(response.agents, true);			
			 dataMap.BookingClass = response.availableBC;
			 dataMap.Channels = objectToArray(response.availableChannels, false);
			 dataMap.SearchTags = response.searchTags;	
			 dataMap.Countries = response.countries;
			 fillInitData(null);
			 hideProgressBar();
		}
	});
}


var fillInitData = function(assignedData){	
	$("#selChanel").fillDropDown({firstEmpty:false, dataArray:dataMap.Channels , keyIndex:0, valueIndex:1});
    
	$("#Agents").autocomplete({
		source:  function( request, response ) {
			    var matches = $.map( dataMap['Agents'], function(tag) {
			      if ( tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0 ) {
			        return tag;
			      }
			    });
			    response(matches);
			  },
        select: function( event, ui ) {
            $( "#Agents" ).val( ui.item.label.split("|")[0] );
            $( "#Agents" ).attr("rel", ui.item.label.split("|")[1] );
            return false;
        }
    });
	
	var unassingedObjects = getUnassignedObjects(assignedData);

    addOptionsToSelect("sel_BookingClass", unassingedObjects.bookingClasses);
	addOptionsToSelect("sel_FlightNo",  unassingedObjects.flightNumbers);	
	addOptionsToSelectFromStringMap("sel_Country",  dataMap.Countries);	
}

/**
 * Determine Available options
 * - If no assignemnts=> will consider alloptions
 * - If assignments => will consider effective unassigned =  (alloptions - assinged)
 */
getUnassignedObjects = function(assignedData) {
    var bookingClasses = null;
    var flightNumbers = null;
    if (assignedData) {
        if (assignedData.bookingClasses) {
            bookingClasses = removeElements(dataMap['BookingClass'], assignedData.bookingClasses);
        }
        if (assignedData.flightNumbers) {
            flightNumbers = removeElements(dataMap['FlightNo'], assignedData.flightNumbers);
        }
    } else {
        bookingClasses = dataMap['BookingClass'];
        flightNumbers = dataMap['FlightNo'];
    }
    return {
        'bookingClasses': bookingClasses,
        'flightNumbers': flightNumbers
    };
}

addBundleFares = function(){
	$("#bundledFaresDetails").show();
	isNewConfig = true;
	panelsEditable= true;
	resetBundledFares();
	fillInitData(null);	
	$('input[name=radStatus][value=ACT]').prop('checked',true);	
	changeEnableDisableStatus(false);
	agentSelectionWidget();
	clearONDSearchData();
    $( "#bundleTemplateTabs" ).tabs( "disable", 5 );
}

clearONDSearchData = function() {
	$("#txtOndSearch").val('');
	clearSearchBox();
}

editBundleFares = function(){
	viewSelectedBundleFare();	
	enableDisableFieldsForEdit();
    $( "#bundleTemplateTabs" ).tabs( "enable", 5 );
}

viewSelectedBundleFare = function(){
	if(selectedBundleFareConfig == null){
		alert("Please select a bundled fare to edit");
		return false;
	}
	
	isNewConfig = false;
	$("#bundledFaresDetails").show();	
	resetBundledFares();	
	fillInitData(selectedBundleFareConfig);

	$('#divMultiMealSelection').hide();
	$('#selCategory').val(selectedBundleFareConfig.bundledCategoryID);
	$("#bundledFareName").val(selectedBundleFareConfig.bundledFareName);	
	$('#chkDefault').attr('checked', selectedBundleFareConfig.defaultBundled == 'Y' ? true : false);
	
	addOptionsToSelect("sel_BookingClassAssigned", selectedBundleFareConfig.bookingClasses);
	addOptionsToSelect("sel_Routes", selectedBundleFareConfig.ondCodes);
	addOptionsToSelect("sel_Agents", selectedBundleFareConfig.agents);
	addOptionsToSelect("sel_FlightNoAssigned", selectedBundleFareConfig.flightNumbers);
	
	if(selectedBundleFareConfig.flightDateFrom != null && selectedBundleFareConfig.flightDateFrom != ""){	
		var frmDate = new Date(selectedBundleFareConfig.flightDateFrom);
		recreateDatePicker($("#flightPeriodFrom"), frmDate);
		$("#flightPeriodFrom").val(selectedBundleFareConfig.flightDateFrom);
		$("#flightPeriodFrom").datepicker("setDate", frmDate);
	}

	if(selectedBundleFareConfig.flightDateTo != null && selectedBundleFareConfig.flightDateTo != ""){	
		var toDate = new Date(selectedBundleFareConfig.flightDateTo);
		recreateDatePicker($("#flightPeriodTo"), toDate);
		$("#flightPeriodTo").val(selectedBundleFareConfig.flightDateTo);
		$("#flightPeriodTo").datepicker( "setDate", toDate);
	}
	
	$("#selChargeCode").val(selectedBundleFareConfig.chargeCode);
	$("#txtBundledFee").val(selectedBundleFareConfig.bundleFee);	
	$('input[name=radStatus][value=' + statusFormatter(selectedBundleFareConfig.status) + ']').prop('checked',true);	
	$("#selChanel").val(selectedBundleFareConfig.applicableSalesChannels);
	$("#txtCutOffTime").val(selectedBundleFareConfig.cutOffTime);
	clearONDSearchData();
	setupWEBPointOfSales(selectedBundleFareConfig.pointOfSale);
	
	convertBackendStructuresToFrontEndStructures(selectedBundleFareConfig.bundledFarePeriods);
	enableTabPaneSelectedServices();
    populateSelectedServiceGrid();
	
	agentSelectionWidget();
}

//convert (BE model structures) to (FE structures)
var convertBackendStructuresToFrontEndStructures = function (bundledFarePeriods){
	if(bundledFarePeriods){
		 for (var i = 0; i < selectedBundleFareConfig.bundledFarePeriods.length; i++) {
     	        var convertedService = {};
		        var template = selectedBundleFareConfig.bundledFarePeriods[i];
		        var key = getServiceKey(template);
		        convertPeriodConstraints(convertedService, template);

		        var modelServices = template.bundledServices;
		        if (modelServices) {
		            for (var j = 0; j < modelServices.length; j++) {
		                var modelService = modelServices[j];
		                switch (modelService.serviceName) {
		                    case freeServices.SEAT:
		                        convertSeatService(convertedService, modelService);
		                        break;
		                    case freeServices.MEAL:
		                        convertMealService(convertedService, modelService);
		                        break;
		                    case freeServices.BAGGAGE:
		                        convertBaggageService(convertedService, modelService);
		                        break;
		                    case freeServices.HALA:
		                        convertHalaService(convertedService, modelService);
		                        break;
		                    case freeServices.FLEXI:
		                        convertFlexiService(convertedService, modelService);
		                        break;
		                    default:
		                        break;
		                }
		            }
		        }
		        selectedServices.services[key] = convertedService;
		    }
		    selectedServices.baseService = getBaseServiceName();
		    sortSelectedServices();
	}
}

var convertPeriodConstraints = function (convertedService, template){
     var fromDate = convertDate(template.flightFrom);
     var toDate = convertDate(template.flightTo);
     var bundleFee = template.bundleFee;
     var bundleFarePeriodId = template.bundleFarePeriodId;

     var updatedService = {
         fromDate: fromDate,
         toDate: toDate,
         sortingNumber: convertToDateNumber(fromDate),
         bundleFee: bundleFee,
         bundleFarePeriodId: bundleFarePeriodId
     };
     
     $.extend(convertedService, updatedService);
}

var getServiceKey = function(template) {
    return convertDate(template.flightFrom) + "_" + convertDate(template.flightTo);
}

var convertDate = function(rawDate) {
    var year = rawDate.substring(0, 4);
    var month = rawDate.substring(5, 7);
    var day = rawDate.substring(8, 10);
    return day + "/" + month + "/" + year;
}

var convertSeatService = function(convertedService, service) {
    convertedService['SEAT'] = {
        id: service.templateId,
        fromDate: service.fromDate,
        toDate: service.toDate,
        templateName: getTemplateName("selSeatTemplate", service.templateId),
        allowMultipleSelect : false,
        freeServiceIds : null
    }
}

var convertMealService = function(convertedService, service) {
    convertedService['MEAL'] = {
        id: service.templateId,
        fromDate: service.fromDate,
        toDate: service.toDate,
        templateName: getTemplateName("selMealTemplate", service.templateId),
        allowMultipleSelect : service.allowMultipleSelect,
        freeServiceIds : null
    }
}

var convertBaggageService  = function(convertedService, service) {
    convertedService['BAGGAGE'] = {
        id: service.templateId,
        fromDate: service.fromDate,
        toDate: service.toDate,
        templateName: getTemplateName("selBaggageTemplate", service.templateId),
        allowMultipleSelect : false,
        freeServiceIds : null
    }
}

var convertHalaService = function(convertedService, service) {
	if (service.includedFreeServices
			&& !$.isEmptyObject(service.includedFreeServices)) {
		convertedService['HALA'] = {
			id : 'ACT',
			fromDate : service.fromDate,
			toDate : service.toDate,
			templateName : getHalaServiceName(service),
			allowMultipleSelect : false,
			freeServiceIds : service.includedFreeServices
		}
	}
}

var convertFlexiService= function(convertedService, service) {
		convertedService['FLEXI'] = {
	            id: 'INC',
	            fromDate: service.fromDate,
	            toDate: service.toDate,
	            allowMultipleSelect : false,
	            templateName: 'Included'
   }
}

var getBaseServiceName = function() {
	var baseService = "";
	var selectedServicesNames = getSelectedServicesNames();

	for (var i = 0; i < BASE_SERVICE_RANKING.length; i++) {
		if (selectedServicesNames[BASE_SERVICE_RANKING[i]]) {
			baseService = BASE_SERVICE_RANKING[i];
			break;
		}
	}
	return baseService;
}

var getSelectedServicesNames = function() {
    var availableServices = {};
    Object.keys(selectedServices.services).forEach(function(key) {
        var availableService = selectedServices.services[key];
        if (availableService) {
            if (availableService['BAGGAGE'] && availableService['BAGGAGE'].id) {
                availableServices['BAGGAGE'] = true;
            }
            if (availableService['MEAL'] && availableService['MEAL'].id) {
                availableServices['MEAL'] = true;
            }
            if (availableService['SEAT'] && availableService['SEAT'].id) {
                availableServices['SEAT'] = true;
            }
            if (availableService['FLEXI'] && availableService['FLEXI'].id && availableService['FLEXI'].id === 'INC') {
                availableServices['FLEXI'] = true;
            }
            if (availableService['HALA'] && availableService['HALA'].id && availableService['HALA'].id === 'ACT') {
                availableServices['HALA'] = true;
            }
        }
    });
    return availableServices;
}

var getTemplateName = function(elementId, value) {
	var templateName = "";
	$("#" + elementId).val(value);
	templateName = $("#" + elementId + " option:selected").text();
	if (templateName == "") {
		templateName = "Inactive Template";
	}
	return templateName;
}

var getHalaServiceName = function(service) {
   var templateName = "";
   var includedFreeServices = service.includedFreeServices;
   if(includedFreeServices && !$.isEmptyObject(includedFreeServices)){
	   selectHalaServicesForEdit(includedFreeServices);
	    $("select#halaAssignedTemplateIds option").each(function() {
	        var ssrName = $(this).text();
	        if (ssrName == '') {
	            ssrName = 'Inactive SSR';
	        }
	        if(templateName.length > 0){
	        	templateName += ",";
	        }
	        templateName += ssrName;
	    });
   }
    return templateName;
}

var enableTabPaneSelectedServices = function() {
	var enabledServices = getSelectedServicesNames();

	if (enabledServices['BAGGAGE']) {
		enableTabPaneSelectedService("chkBgdService", "btnEditBgd");
	}

	if (enabledServices['MEAL']) {
		enableTabPaneSelectedService("chkMleService", "btnEditMle");
	}

	if (enabledServices['SEAT']) {
		enableTabPaneSelectedService("chkSteService", "btnEditSte");
	}

	if (enabledServices['FLEXI']) {
		enableTabPaneSelectedService("chkFlxService", "btnEditFlx");
	}

	if (enabledServices['HALA']) {
		enableTabPaneSelectedService("chkHlaService", "btnEditHla");
	}
}

var complianceForCheckBoxesAndSelectedServices = function() {
    var valid = true;
    var errorMsg = "";
    var checkedServices = getSelectedServicesNames();
    
    if($.isEmptyObject(checkedServices)){
    	 valid = false;
         errorMsg = bundledFareFreeServiceRequired;
    }

    if ($("#chkBgdService").attr('checked')) {
        if (!checkedServices['BAGGAGE']) {
            valid = false;
            errorMsg = formatPlaceHolder(selectAtleastOne, 'BAGGAGE');
        }
    }

    if (valid && $("#chkMleService").attr('checked')) {
        if (!checkedServices['MEAL']) {
            valid = false;
            errorMsg = formatPlaceHolder(selectAtleastOne, 'MEAL');
        }
    }

    if (valid && $("#chkSteService").attr('checked')) {
        if (!checkedServices['SEAT']) {
            valid = false;
            errorMsg = formatPlaceHolder(selectAtleastOne, 'SEAT');
        }
    }

    if (valid && $("#chkFlxService").attr('checked')) {
        if (!checkedServices['FLEXI']) {
            valid = false;
            errorMsg = formatPlaceHolder(selectAtleastOne, 'FLEXI');
        }
    }

    if (valid && $("#chkHlaService").attr('checked')) {
        if (!checkedServices['HALA']) {
            valid = false;
            errorMsg = formatPlaceHolder(selectAtleastOne, 'HALA');
        }
    }

    var result = {
        'valid': valid,
        'errorMsg': errorMsg
    };

    return result;

}

var enableTabPaneSelectedService = function(checkBox, button) {
    $("#" + checkBox).attr('checked', true);
    $("#" + button).enableButton();
}

recreateDatePicker = function(datePickerObj, minDate){
	
	var toDay = new Date();
	
	if(minDate > toDay){
		minDate = toDay;
	}
	
	datePickerObj.datepicker('destroy');
	datePickerObj.datepicker({
		dateFormat: 'dd/mm/yy',
		minDate: minDate
	}).keyup(function(e) {
		if(e.keyCode == 8 || e.keyCode == 46) {
			$.datepicker._clearDate(this);
		}
	});
	
}

enableDisableFieldsForEdit = function() {
	if (selectedBundleFareConfig != null) {
		if(panelsEditable){
			changeEnableDisableStatus(false);
		}else{
			changeEnableDisableStatus(true);
		}
	}
}

OverrideFieldsForEdit = function() {
	$("#bundledFareName").val('');
}

changeEnableDisableStatus = function(status){
	$("#bundledFareName,#flightPeriodFrom,#flightPeriodTo,#txtBundledFee,#txtCutOffTime,#txtOndSearch").attr("disabled", status);
	$("#selCategory,#sel_BookingClass,#sel_BookingClassAssigned,#selChargeCode,#sel_Routes,#sel_FlightNo,#sel_FlightNoAssigned,#sel_Agents,#selChanel,#sel_GeneratedRoutes,#sel_Country, #sel_CountryAssigned").attr("disabled", status);
	$(".addItem,.removeItem,.portSel").attr("disabled", status);
	$("#Agents,#FlightNo").attr("disabled", status);
	$("#chkDefault").attr("disabled", status);
	$("#chkSeat,#chkMeal,#chkBaggage,#chkFlexi,#chkHala,#chkMultiMeal").attr('disabled', status);
}

resetBundledFares = function(){
	$("#bundledFareName").val('');
	$('#selFrom,#selTo,#selVia1,#selVia2,#selVia3,#selVia4').empty();
	$('#selSeatTemplate,#selMealTemplate,#selBaggageTemplate,#selChargeCode').val('');
	$('#sel_Routes,#sel_FlightNoAssigned,#sel_Agents,#sel_BookingClassAssigned, #sel_CountryAssigned').empty();
	$('#FlightNo').val('');
	$('#flightPeriodFrom').val('');
	$('#flightPeriodTo').val('');
	$('#txtBundledFee').val('');
	$('#chkSeat,#chkMeal,#chkBaggage,#chkFlexi,#chkHala,#chkActive,#chkMultiMeal').attr('checked', false);
    $('#chkBgdService,#chkMleService,#chkSteService,#chkFlxService,#chkHlaService').attr('checked', false);
	$('#divMultiMealSelection').hide();
	resetAllToAvailablePane('halaAssigned', 'halaAvailable');
	$('#halaAvailable,#halaAssigned').attr('disabled', true);
	$("#selChanel").empty();
	$('#btnUpload').enableButton();
	$('#dvUpLoad').html($('#dvUpLoad').html());
	$('#imgBundledFare').attr('src', '');
	$('#btnUpload2').enableButton();
	$('#dvUpLoad2').html($('#dvUpLoad2').html());
	$('#imgBundledFareStripe').attr('src', '');
	$('#txtCutOffTime').val('');	
	resetImagesUploaded();
	$('#selCategory').val('');
    $("#btnEditBgd").disableButton();
    $("#btnEditMle").disableButton();
    $("#btnEditSte").disableButton();
    $("#btnEditFlx").disableButton();
	$("#btnEditHla").disableButton();
	resetAddServicesDialog(true);
	clearSelectedServices();
	clearSelectedServiceGrid();
	setupWEBPointOfSales();
}

var clearSelectedServices = function (){
	selectedServices.services = [];
	selectedServices.baseService = '';
}

resetImagesUploaded = function(){
	strBundleUploads = {};
	strStripeUploads = {};
}

addItemsFunction=function(tID, checkList){
	var boolAdd = true;
	var addValue = $("#"+tID).val();
	if (tID=="Agents"){
		addValue +=  "|" + $("#"+tID).attr("rel");
	}
	var appenItem = $("<option>"+addValue+"</option>").attr("value", addValue);
	if (tID=="Agents"){
		appenItem = $("<option>"+addValue.split("|")[0]+"</option>").attr("value", addValue.split("|")[1]);
	}
	alloption = $("#sel_"+tID).find("option");
	if (addValue==""){
		boolAdd = false;
	}else if(checkList && $.inArray(addValue ,dataMap[tID])<0){
		alert("Not a valied item!..");
		boolAdd = false;
	}
	
	for(var i = 0; i < alloption.length; i++){
		if (alloption[i].value == addValue){
			alert("Already added!..");
			boolAdd = false;
			break;
		}
	}
	
	
	if(boolAdd){
		if(alloption[0].value==""){ 
			$("#sel_"+tID).empty();
		}
		$("#sel_"+tID).append(appenItem);
		$("#"+tID).val("");
	}
};

addItemsToSelect = function(id){
	
	var tID = id.split("_")[1];
	var boolAdd = true;
	switch (tID){
	case "Routes":
	
		var route = getViaPoints();
		if(route!=""){			
			var displayText = replaceall(route,"***","All");
			var appenRouteItem = $("<option>"+displayText+"</option>").attr("value", route);
			if (route==""){
				boolAdd = false;
				break;
			}
			alloption = $("#sel_"+tID).find("option");
			for(var i = 0; i < alloption.length; i++){
				if (alloption[i].value == route){
					boolAdd = false;
					break;
				}
			}
			if(boolAdd){
				if(alloption.length > 0 && alloption[0].value==""){ 
					$("#sel_"+tID).empty();
				}
				$("#sel_"+tID).append(appenRouteItem);
			
			
				var routeData={};
				var routeVal='';
				for (var i = 0; i < routeArray.length; i++) {
			       if(i==0){
			    	   routeVal= routeArray[i];
			       }
			       else{
			    	   routeVal=routeVal+","+ routeArray[i];
			       }
			           		        
			    }
				
				routeData['selectedRoute']=routeVal;				
				loadApplicableFlightNumbers(routeData);
			}
		}
		break;
	default:
		addItemsFunction(tID, true)
	}
};

removeItemsFromSelect = function(id){
	var tID = id.split("_")[1];
	alloption = $("#sel_"+tID).find("option");
	
	if(alloption[0].value!=""){ 	
		var selItem=$("#sel_"+tID).find("option:selected")[0].value;
		$("#sel_"+tID).find("option:selected").remove();
		
		if(tID == "Routes"){			
			var splitItem=selItem.split("/");
			var arrayElement=[];
			for(var i=0;i<splitItem.length-1;i++){
				if(splitItem[i+1]!=""){
					arrayElement[i]="'"+splitItem[i]+"/"+splitItem[i+1]+"'";
				}
			}
			for(var a=0;a<arrayElement.length;a++){
				for(var i=0; i<routeArray.length;i++ )
				{ 
					if(routeArray[i]==arrayElement[a]){
						routeArray.splice(i,1);
						break;
					} 
				}
			}
			var routeVal='';
			for (var i = 0; i < routeArray.length; i++) {
				if(i==0){
					routeVal= routeArray[i];
				}
				else{
					routeVal=routeVal+","+ routeArray[i];
				}
				
			}
			var routeData={};
			routeData['selectedRoute']=routeVal;
			
			if(routeVal!=''){
				loadApplicableFlightNumbers(routeData);
			} else {
				dataMap.FlightNo = allFlightNos;
				fillFlightData();
			}
		}
	}
	
	if(tID != "Routes"){		
		if($("#sel_"+tID).find("option").length==0){
			var appenItem = $("<option>"+"All"+"</option>").attr("value", "");
			$("#sel_"+tID).append(appenItem);
		}
	}
};


loadApplicableFlightNumbers = function(routeData){
	$.ajax({ 
		url : 'loadPromotionFormInitData!getSeletedFlights.action',
		beforeSend : showProgressBar(),
		data:routeData,
		type : "GET",
		async:false,
		dataType : "json",
		complete:function(response){
			 var response = eval("(" + response.responseText + ")");
			 dataMap.FlightNo = response.selectedFlights;
			 fillFlightData();
			 hideProgressBar();
		}
	});
}

/**
 * ########## Translations related methods ##########
 */
validateKeyPress = function (objCon) {	
	var strCC = objCon.value;
	var strLen = strCC.length;
	var blnVal = isAlphaNumeric(strCC);
	if (!blnVal) {
		objCon.value=strCC.substr(0, strLen - 1);
		objCon.focus();
	}
} 

objectFormat = function(obj,bind){
	rObj = [];
	$.each(obj, function(key, val){
		var t = val
		if (bind){
			t = val + "|" + key;
		}
		rObj[rObj.length] = t;
	})
	return rObj
}

objectToArray = function(obj){
	rObj = [];
	$.each(obj, function(key, val){
		t = [];
		t[0] = key;
		t[1] = val;
		rObj[rObj.length] = t;
	})
	return rObj
}

showMsg = function(strType, strMsg) {
	top[2].objMsg.MessageText = strMsg;
	top[2].objMsg.MessageType = strType ;
	top[2].ShowPageMessage();
}

showProgressBar = function(){
	top[2].ShowProgress();
}

hideProgressBar = function(){
	top[2].HideProgress();
}

getSelectBoxData = function(selectBoxID){
	var selector="#"+selectBoxID+" option"
	var selectedItems=[];
	$(selector).each(function () {
		if(this.value!=""){
			selectedItems.push(this.value);
		}
	});
	return JSON.stringify(selectedItems);
}

getSelectedSelectBoxData = function(selectBoxID) {
	var selector="#"+selectBoxID+" option:selected"
	var selectedItems=[];
	$(selector).each(function () {
		selectedItems.push(this.value);
	});
	return JSON.stringify(selectedItems);
}

addOptionsToSelect = function(selectID,values){
	$("#" + selectID + " option").remove();
	$.each(values,function(index,value){
		var displayText = replaceall(value,"***","All");
		$("#"+selectID).append('<option value='+value+'>'+displayText+'</option>');
	});
}

agentSelectionWidget = function(){
	var agents = $("#selChanel").val();
	var disableAgentWidget = true;
	if(agents!=null){
		$.each(agents,function(index,value){
			if(value=='3'){
				disableAgentWidget = false;
			}
		});		
	}	
	$("#Agents,#sel_Agents").attr("disabled", disableAgentWidget);
}

validateAndPopulate = function(cntfield){
	if (!dateChk(cntfield))	{
		showERRMessage(invalidDate);		
		getFieldByID(cntfield).focus();		
		return;
	}
}

/**
 * Display Settings ==> Related functions
 */
var attachDisplaySettings = function(key, cellClone, title) {
    var btnContentSample = cellClone.find(".btnContentSample");
    btnContentSample.attr("id", key);
    btnContentSample.attr("name", title);

    //Event handler for the Display Settings button
    btnContentSample.click(function() {
    	clearDisplaySettingsData();
    	currentlyEditingDisplaySetting = { tId: this.id, title: this.name};
        prepareDisplaySettings();
    });
    btnContentSample.css("display", "inline");
    
    //disable display settings on add/edit flows
    btnContentSample.attr("disabled", panelsEditable);

}

var prepareDisplaySettings = function() {
    var tId = currentlyEditingDisplaySetting.tId;
    if (tId && tId !== "") {
        showServiceTemplateSearchLoader();
        $.ajax({
            type: "POST",
            url: 'bundledFaresDisplaySettings!search.action',
            data: getParametersForDisplaySearch(),
            success: function(response) {
                populateDisplaySettingsAjaxResponse(response);
            },
            dataType: 'json'
        });
    }
}

var getParametersForDisplaySearch = function() {
    var data = {};
    var tId = currentlyEditingDisplaySetting.tId;
    data['bundledFarePeriodId'] = getBundledFarePeriodId(tId);
    return data;
}

var getBundledFarePeriodId = function(tID) {
    var bundleFarePeriodId = "";
    Object.keys(selectedServices.services).forEach(function(key) {
        if (key === tID) {
            bundleFarePeriodId = selectedServices.services[key].bundleFarePeriodId;
        }
    });
    return bundleFarePeriodId;
}

var populateDisplaySettingsAjaxResponse = function(response) {
	if (response.success) {
		$("#serviceTemplateLoader").css("display", "none");
		populateDisplaySettingsData(response.displayDTO);
		initDisplaySettingsDialog();
	} else {
		showMsg("Error", response.messageTxt);
	}
}

/**
 * Display Settings => Convert BE model data structures to FE friendly data
 * 
 */
var populateDisplaySettingsData = function(data) {
    displaySettings.periodKey = data.bundleFarePeriodId;
    convertTranslations(displaySettings.translations, data.translations);
    convertThumbnails(displaySettings.thumbnails, data.thumbnails);
    convertDisplayTemplate(displaySettings, data);  
}

var convertDisplayTemplate = function (displaySettings, data) {
	displaySettings.displayTemplate = data.displayTemplateId;
    bundleDescriptionTemplate = data.descriptionTemplateDTO;
}

var convertTranslations = function(displayTranslations, translations) {
    $.each(translations, function(key, value) {
        var contents = translations[key];
        var convertedTranslation = {
            code: key,
            language: getLanguageName(key),
            bundleName: contents.name,
            budleDescription: contents.description
        };
        displayTranslations[key] = convertedTranslation;
    });
}

var convertThumbnails = function(displayThumbnails, thumbnails) {
    $.each(thumbnails, function(key, value) {
        var imageUrl = thumbnails[key];
        var convertedThumbnails = {
            code: key,
            languageName: getLanguageName(key),
            imageUrl: imageUrl + '?rel=' + new Date().getTime(),
        }
        displayThumbnails[key] = convertedThumbnails;
    });
}


/**
 * Display Settings => Initialize the display settings Dialog
 */
var initDisplaySettingsDialog = function() {
    var title = currentlyEditingDisplaySetting.title;
    $("#displaySettingsDialog").dialog({
        height: 590,
        width: 890,
        title: "Bundle Display Settings [" + title + "]",
        modal: true
    });

    $("#jqGridBundledFareTranslations").jqGrid({
        datatype: "local",
        height: 190,
        colNames: ['Code', 'Language', 'Bundle Name', 'Description'],
        colModel: [{
                name: 'code',
                index: 'code',
                width: 47,
                align: "center"
            },
            {
                name: 'language',
                index: 'language',
                width: 100
            },
            {
                name: 'bundleName',
                index: 'bundleName',
                width: 210
            },
            {
                name: 'budleDescription',
                index: 'budleDescription',
                width: 470
            }
        ],
        multiselect: false,
        onSelectRow: function(rowid) {
            selectTranlationToEdit();
        }
    });
    

    $("#btnAddDisplayTemplate").click(function() {
        displaySettings.displayTemplate = bundleDescriptionTemplate.templateID;
        populateBundleDescriptionTemplate();
    });
    
    $("#btnChangeDisplayTemplate").click(function() {
        //Better to get a confirmation
        displaySettings.displayTemplate = bundleDescriptionTemplate.templateID;
        populateBundleDescriptionTemplate();
      
    });
    
    enableDisplayTemplateButtons();
    fillPopupDialogDisplayGrid();
}

var enableDisplayTemplateButtons = function() {
	var displayTemplateLinked = displaySettings.displayTemplate
			&& displaySettings.displayTemplate !== '';
	if (displayTemplateLinked) {
		$("#btnAddDisplayTemplate").hide();
		$("#btnChangeDisplayTemplate").show();
	} else {
		$("#btnAddDisplayTemplate").show();
		$("#btnChangeDisplayTemplate").hide();
	}
}

/**
 * Display Settings => Poulate contents from already converted FE structures
 * 
 * 1.populate description template
 * 2.poulate thumbnail images
 * 3.poulate translations
 * 
 */
var fillPopupDialogDisplayGrid = function() {
	populateBundleTranslationsGrid();
    populateBundleTumbnails();
    populateBundleDescriptionTemplate();
}

var populateBundleDescriptionTemplate = function() {
	clearBundleDescTemplateGrid();
    if (bundleDescriptionTemplate) {
    	$("#displayTemplateId").val(bundleDescriptionTemplate.templateID);
        $("#displayTemplateName").text(bundleDescriptionTemplate.templateName);
        $("#divDescTempFreeContext").html(sanitizeContent(bundleDescriptionTemplate.defaultFreeServicesContent))
        $("#divDescTempPaidContext").html(sanitizeContent(bundleDescriptionTemplate.defaultPaidServicesContent));
        $("#divBundleDescTemplateSampleDisplay").show();//review
        if (!jQuery.isEmptyObject(bundleDescriptionTemplate.translationTemplates)) {
            var traslation = bundleDescriptionTemplate.translationTemplates;
            $.each(traslation, function(i, item) {
                var languageName = getLanguageName(item.languageCode);
                var div = $("#divBundleDescTemplateSampleDisplay").clone().prop('id', 'divBundleDescTemplateDisplay_' + item.languageCode);
                div.addClass(".divDescriptionTemplates");
                div.find("#bundleDescTrasLanguage").text(languageName);
                
                var freeContentId = "divDescTempFreeContext_" + item.languageCode;
                var paidContentId = "divDescTempPaidContext_" + item.languageCode;                
                
                div.find("#divDescTempFreeContext").attr('id', freeContentId);
                div.find("#divDescTempPaidContext").attr('id', paidContentId);
                
                div.find("#"+ freeContentId).html(item.freeServicesContent);
                div.find("#"+ paidContentId).html(item.paidServicesContent);
          
                $('#divBundleDescTemplateDisplayParent').append(div);
            });
            
        }
    }
    
    var descTemplatesLinked = displaySettings.displayTemplate && displaySettings.displayTemplate !== "";
    if(descTemplatesLinked){
    	 $("#bundleDisplayWarningMsgTable").hide();
    }else{
    	 $("#bundleDisplayWarningMsgTable").show();
    }
}

/**
 * Display Settings ==> Description Template Ajax calls 
 */
var prepareDescriptionTemplatePopulator = function() {
    $("#displayTemplateId").change(function() {
        if (isNotEmptyTextbox("#displayTemplateId")) {
            var paramTID = $("#displayTemplateId").val();
            var data = {};
            data['templateID'] = paramTID;
            $.ajax({
                type: "POST",
                url: 'showBundleDescription!retrieveBundleTemplateById.action',
                data: data,
                success: function(response) {
                    clearBundleDescTemplateGrid();
                    displayTemplateAjaxResponse(response);
                },
                dataType: 'json'
            });
        }
    });
}

var displayTemplateAjaxResponse = function(response) {
    if (response.success) {
        bundleDescriptionTemplate = response.updatedBundleTemplate;
        if (bundleDescriptionTemplate.templateID) {
            populateBundleDescriptionTemplate();
        } else {
            clearBundleDescTemplateGrid();
            showMsg("Error", "There is no such template defined in the system");
        }
    } else {
        showMsg("Error", response.messageTxt);
    }
}

/**
 * Display Settings ==> Translations
 */
var populateBundleTranslationsGrid = function() {
	var i = 0;
	$("#jqGridBundledFareTranslations").clearGridData();
	Object.keys(displaySettings.translations).forEach(function (key) {
		i++;
		$("#jqGridBundledFareTranslations").addRowData(i, displaySettings.translations[key]);
	});	
}

/**
 * Display Settings ==> Sanitary Tasks
 */
var clearBundledDisplaySettings = function (){
	clearDisplaySettingsData();
	clearTumbnails();
	clearBundleTranslationsGrid();
	clearBundleDescTemplateGrid();
	resetTranlationEditPanel();
}

var sanitizeContent = function(content) {
	if (content) {
		content = replaceall(content, MARKER_TEXT, '');
	}
	return content;
}

var clearDisplaySettingsData = function() {
    displaySettings = {
        periodKey: '',
        translations: [],
        thumbnails: [],
        displayTemplate: ''
    };
    currentlyEditingDisplaySetting = null;
}

var clearBundleDescTemplateGrid = function() {
	
	//TODO unify this
	$('#divBundleDescTemplateDisplayParent').find('div').filter(function () {
        var regex = new RegExp('divBundleDescTemplateDisplay_*');
        var matches = regex.test(this.id);
        var myNa= this.id;
        console.log(myNa + " - Matches" + matches)
        return matches;
    }).remove();
	
	if(!jQuery.isEmptyObject(bundleDescriptionTemplate)){
		var traslation = bundleDescriptionTemplate.translationTemplates;
		$.each(traslation, function (i, item) {
			$( '#divBundleDescTemplateDisplay_' + item.languageCode).remove();
		});
	}
	$(".divDescriptionTemplates").remove();

    $("#displayTemplateName").text("");
    $("#displayTemplateId").val("");
    $("#divBundleDescTemplateSampleDisplay").hide();
}


var clearBundleTranslationsGrid = function (){
	$("#jqGridBundledFareTranslations").clearGridData();
}

/**
 * Display Settings ==> Saving Functions
 */
var saveBundleDisplaySettings = function (){
	$.ajax({
		type: "POST",
		url: 'bundledFaresDisplaySettings!add.action',
		data: getTransformedDisplaySettingsData(),
		success: saveBundleDisplaySettingsAjaxReponse,
		dataType: 'json'
	});
}

var saveBundleDisplaySettingsAjaxReponse = function (response){
	if (response.success) {
		clearBundledDisplaySettings();
		$("#displaySettingsDialog").dialog('close');
		showMsg("Success",response.messageTxt);
	} else {
		showMsg("Error", response.messageTxt);
	}
}

/**
 * Display Settings ==> Transform FE objects to BE object structures
 */
var getTransformedDisplaySettingsData = function() {
    var data = {};
    if (strBundleUploads != null && strBundleUploads != '') {
        data['uploadImage'] = JSON.stringify(strBundleUploads);
    }
    data['bundledFarePeriodId'] = JSON.stringify(displaySettings.periodKey);

    var descriptionTemplateId = displaySettings.displayTemplate;

    if (descriptionTemplateId !== '') {
        data['displayTemplateId'] = JSON.stringify(displaySettings.displayTemplate);
    }
    transformTranslations(data);

    return data;
}

var transformTranslations = function(data) {
    var translations = displaySettings.translations;
    var nameTranslation = {};
    var descTranslation = {};
    if (translations) {
        Object.keys(translations).forEach(function(lang) {
            var translation = translations[lang];
            if (isNotEmpty(lang)) {
                if (isNotEmpty(translation.bundleName)) {
                    nameTranslation[lang] = translation.bundleName;
                }

                if (isNotEmpty(translation.budleDescription)) {
                    descTranslation[lang] = translation.budleDescription;
                }
            }
        });

        if (nameTranslation) {
            data['nameTranslations'] = JSON.stringify(nameTranslation);
        }

        if (descTranslation) {
            data['descTranslations'] = JSON.stringify(descTranslation);
        }
    }
}

var isNotEmpty = function (field){
	if(field && field.trim() && field.trim() !== ""){
		return true;
	}
	return false;
}

var capitalizeFirstLetter = function (str){
	if(str){
		str = str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
	}
	return str;
}

var formatPlaceHolder = function (placeHolder, value ){
	 if(placeHolder){
		 var formatedValue = capitalizeFirstLetter(value);
		 placeHolder = placeHolder.replace("{0}", formatedValue);
	 }
	return placeHolder;
}

/**
 * Remove matching elements from an array
 * and returns a new array
 */
var removeElements = function(sourceArray, elementsToRemove) {
    var filteredArray = null;
    if (sourceArray) {
        filteredArray = sourceArray.slice(0);
        if (elementsToRemove) {
            filteredArray = sourceArray.filter(function(val) {
                return (elementsToRemove.indexOf(val) == -1) ? true : false;
            });
        }
    }
    return filteredArray;
}


// Auto complete plugin
//========================
$( function() {	
    $("#txtOndSearch")
      // don't navigate away from the field on tab when selecting an item
      .on( "keydown", function( event ) {
        if (event.keyCode == 13) {        	
        	//$('#txtOndSearch').autocomplete('close');
        	generateOndCodes(event);        	
        }
      })
      /*.autocomplete({    	 
    	  source: dataMap.SearchTags
      })*/;
  } );
