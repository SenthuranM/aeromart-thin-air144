var templatesData = {};
var generateChargesPane = false;
var isEditClick = false;
var isAddClick = false;
var nextSeatMinSeats=1; //max seats allowed through pnl is 2.
var nextSeatMaxSeats=1;
var selectedGridRowId = "";

jQuery(document).ready(function(){	
	$("#divSearch").decoratePanel("Search Promotions");
	$("#divResultsPanel").decoratePanel("Promotions");
	$('#btnAdd').decorateButton();
	$('#btnEdit').decorateButton();
	$('#btnDelete').decorateButton();
    $('#promotionsTemplateDetails :input').attr('disabled', true);
    $("#btnClose").attr('disabled', false);
    
	enableGridButtons(false);
	enbleFormButtons(false);

	loadDisplayData();
	clearPanel('promotionsTemplateDetails')
	
	$("#promotionsTabs").tabs({
		   select: function(event, ui) {
			   if (ui.index == 2 && generateChargesPane){
				   generateChargesAndRewardsParamHtml();
				   generateChargesPane =false;
				   if(isEditClick || isAddClick ){
					   $('#chargesRewardsPane :input').attr('disabled', false);
				   }else{
					   $('#chargesRewardsPane :input').attr('disabled', true); 
				   }
			   }
		   }
	});

	promotionTypeChange();
	constructPromoTemplateGrid();
	
	$("#txtStartDateSearch, #txtStopDateSearch, #txtStartDateDetail, #txtEndDateDetail").datepicker({
		showOn: "button",
		buttonImage: "../../images/calendar_no_cache.gif",
		buttonImageOnly: true,
		dateFormat: 'dd/mm/yy'
	});

    $("#txtStartDateDetail, #txtEndDateDetail")
        .datepicker('disable');


    $('#btnAdd').click(function() {
    	selectedGridRowId = "";
        isAddClick = true;
        isEditClick = false;
		hideMessage();
		//disableControls(false);
		clearPanel('promotionsTemplateDetails');
		generateChargesPane = true;
		//$('#frmPromotion').clearForm();
		$('#promotionType').attr('disabled', false);
		/*$("#version").val('-1');*/
		//jQuery("#jqGridPromoTemplatesData").clearGridData();
		grdArray = new Array();
		enbleFormButtons(true);
		promotionTypeChange();
		$('#promotionsTemplateDetails :input').attr('disabled', false);
		$('#chargesRewardsPane :input').attr('disabled', false);

        $("#txtStartDateDetail, #txtEndDateDetail")
            .datepicker('enable');
	});
	
	$('#btnEdit').click(function() {
		isEditClick = true;
		isAddClick = false;
		generateChargesPane = false;
		hideMessage();
		$('#promotionType').attr('disabled', true);
		//disableControls(false);
		enbleFormButtons(true);
		$('#promotionsTemplateDetails :input').attr('disabled', false);
		$('#chargesRewardsPane :input').attr('disabled', false);

        $("#txtStartDateDetail, #txtEndDateDetail")
            .datepicker('enable');
	});
	

	

});


function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
	hideMessage();
}

function isPageEdited() {
	return top.loadCheck(top[1].objTMenu
			.tabGetPageEidted(screenId));
}

function enbleFormButtons(cond) {
	if (cond) {
		$("#btnReset").enableButton();
		$("#btnSave").enableButton();
	} else {
		$("#btnReset").disableButton();
		$("#btnSave").disableButton();
	}

}

function enableGridButtons(cond) {
	if (cond) {
		$("#btnEdit").enableButton();
		$("#btnDelete").enableButton();
		$("#btnAddOND").enableButton();
	} else {
		$("#btnEdit").disableButton();
		$("#btnDelete").disableButton();
		$("#btnAddOND").disableButton();
	}

}

function hideMessage() {
	top[2].HidePageMessage();
}

function disableControls(cond) {
	$("input").each(function() {
		$(this).attr('disabled', cond);
	});
	$("textarea").each(function() {
		$(this).attr('disabled', cond);
	});
	$("select").each(function() {
		$(this).attr('disabled', cond);
	});
	$("#btnClose").attr('disabled', false);
}

function constructPromoTemplateGrid(strUrl) {
	$("#jqGridPromoTemplatesData")
			.jqGrid(
					{
						url : 'promotionTemplatesManagement!searchPromotionTemplates.action',
						datatype : "json",
						mtype : 'POST',
						jsonReader : {
							root : "rows",
							page : "page",
							total : "total",
							records : "records",
							repeatitems : false,
							id : "0"
						},
						rowNum : 20,
						height : 160,
						width : 930,
						loadui : 'block',
						colNames : [ '&nbsp;', 'Promotion Type', 'Start Date',
								'End Date', 'Status', 'params', 'routes' ],
						colModel : [ {
							name : 'id',
							index : 'id',
							width : 40
						}, {
							name : 'template.promotionTypeName',
							index : 'promotionTypeName',
							width : 120,
							align : "center"
						}, {
							name : 'template.promoStartDate',
							index : 'promoStartDate',
							width : 120,
							align : "center",
							formatter : dateFormatter
						}, {
							name : 'template.promoEndDate',
							index : 'promoEndDate',
							width : 120,
							align : "center",
							formatter : dateFormatter
						}, {
							name : 'template.status',
							index : 'status',
							width : 100,
							align : "center"
						}, {
							name : 'template.promotionTemplateParams',
							index : 'promotionTemplateParams',
							hidden : true
						}, {
							name : 'template.routes',
							index : 'routes',
							hidden : true
						} ],
						multiselect : false,
						viewrecords : true,
						onSelectRow : function(rowid) {
							if (isPageEdited()) {
								selectedGridRowId = rowid;
								performOperationsOnRowClick(rowid);
							}
						},
						loadComplete : function (response) {
							var resp = eval("(" + response.responseText + ")");
							templatesData = resp.rows;
						},
						pager : jQuery('#jqGridPromoTemplatesPages')
					}).navGrid("#mealpager", {
				refresh : false,
				edit : false,
				add : false,
				del : false,
				search : false
			});
}

function loadDisplayData() {
	$.ajax({
		url : 'loadPromotionTemplatesManagement.action',
		beforeSend : top[2].ShowProgress(),
		data : $.extend({}, {}),
		dataType : "json",
		complete : function(resp) {
			resp = eval("("+resp.responseText+")");
			
			$("#originAirport, #destAirport,#viaPoint1,#viaPoint2").fillDropDown({
				firstEmpty : false,
				dataArray : resp.airportsList,
				keyIndex : 0,
				valueIndex : 1
			});
			
			top[2].HideProgress();
		}
	});
}

function loadRoutes() {
	
	hideMessage();
	
	var origin = $("#originAirport").val();
	var dest = $("#destAirport").val();
	var viaPoint1 = $("#viaPoint1").val();
	var viaPoint2 = $("#viaPoint2").val();
	var ondCode = "";
	
	
	if(origin == dest && origin != "ALL"){
		showERRMessage(depatureArriavlSame);
		$("#originAirport").focus();
		return false;
	} else if(origin == viaPoint1 || origin == viaPoint2){
		showERRMessage(arriavlViaSame);
		$("#originAirport").focus();
		return false;
	} else if(dest == viaPoint1 || dest == viaPoint2){
		showERRMessage(depatureViaSame);
		$("#destAirport").focus();
		return false;
	} else if (viaPoint2 != "" && viaPoint1 == "" ){
		showERRMessage(vianotinSequence);
		$("#viaPoint1").focus();
		return false;
	} else if((viaPoint1 != "" && viaPoint2 != "") && (viaPoint1==viaPoint2)){
		showERRMessage(viaEqual);
		$("#viaPoint1").focus();
		return false;
	} else if((viaPoint1 != "" || viaPoint2 != "") && (origin == "ALL" || dest == "ALL") ){
		showERRMessage(incorrectAllOriginDest);
		$("#origin").focus();
		return false;
	} else{
		if(viaPoint1 != "" && viaPoint2 != ""){
			ondCode = origin + "/" + viaPoint1 + "/" + viaPoint2 + "/" + dest;
		}else if(viaPoint1 != ""){
			ondCode = origin + "/" + viaPoint1 + "/" + dest;
		}else{
			ondCode = origin + "/" + dest;
		}
		
		$("#availableRoutes").html('');
		$("#availableRoutes").fillDropDown({
			firstEmpty : false,
			dataArray : [[ondCode]],
			keyIndex : 0,
			valueIndex : 0
		});
	}
}

function addRoutes() {
	var optVal;
	var optText;

	$("select#availableRoutes option:selected").each(
			function() {
				optVal = $(this).val();
				optText = $(this).text();

				if ($("select#applicableRoutes_1 option[value='" + optVal + "']").length == 0) {
					$("#applicableRoutes_1").append("<option value=\"" + optVal + "\">" + optText + "</option>");
					$("#applicableRoutes_2").append("<option value=\"" + optVal + "\">" + optText + "</option>");
				}
			});
}

function removeRoutes() {
	var optVal;

	$("select#applicableRoutes_1 option:selected").each(
			function() {
				optVal = $(this).val();

				$("select#applicableRoutes_1 option").remove("[value='" + optVal + "']");
				$("select#applicableRoutes_2 option").remove("[value='" + optVal + "']");
			});
}

function searchPromotionTemplates() {
	
	if(validateFlightSearch()){
		var strUrl = "promotionTemplatesManagement!searchPromotionTemplates.action?";
	
		strUrl += "promotionsSearchTo.promotionType=" + $("#searchPromotionType").val();
		strUrl += "&promotionsSearchTo.promotionStatus=" + $("#promoSearchStatus").val();
	
		var searchDate;
		searchDate = $("#txtStartDateSearch").val();
		if(searchDate != '') { 
			strUrl += "&startDate=" + searchDate;
		}
		searchDate = $("#txtStopDateSearch").val();
		if(searchDate != '') {
			strUrl += "&endDate=" + searchDate;
		}
	
		$("#jqGridPromoTemplatesData").setGridParam({
			url : strUrl,
			page : 1
		});
		$("#jqGridPromoTemplatesData").trigger("reloadGrid");
		$("#btnAdd").attr('disabled', false);
		enableGridButtons(false);
		$('#promotionsTemplateDetails :input').attr('disabled', true);
		$("#btnClose").attr('disabled', false);
		clearPanel('promotionsTemplateDetails');
		selectedGridRowId = "";
	}
}

function validateFlightSearch(){
	 hideMessage();
	 if ($("#txtStartDateSearch").val() == ""){
			showERRMessage(startDateRequired);
			$("#txtStartDateDetail").focus();
			return false;
	 }
	 
	 if ($("#txtStopDateSearch").val() == ""){
			showERRMessage(endDateRequired);
			$("#txtEndDateDetail").focus();
			return false;
	 }
	 
	if (!dateChk("txtStartDateSearch")){
			showERRMessage(invalidDate);
			getFieldByID("txtStartDateSearch").focus();
			return false;
	}
	
	if (!dateChk("txtStopDateSearch")){
			showERRMessage(invalidDate);
			getFieldByID("txtStopDateSearch").focus();
			return false;
	 }
	
	var schStartD = formatDate($("#txtStartDateSearch").val(), "dd/mm/yyyy","dd-mmm-yyyy");
	var schStopD = formatDate($("#txtStopDateSearch").val(), "dd/mm/yyyy","dd-mmm-yyyy");

	if (!CheckDates(schStartD,schStopD)){
		showERRMessage(enddategreat);
		return false;
	}
	 
		isEditClick = false;
		isAddClick = false;
	 return true;
	
}

function performOperationsOnRowClick(rowid){
	 clearPanel('promotionsTemplateDetails');
     hideMessage();
     $('#promotionType').attr('disabled', true);
	 setPageEdited(false);
	 loadPromotionTemplateToDetailsPane(rowid);
	 enableGridButtons(true);
	 $("#btnClose").attr('disabled', false);
}

function loadPromotionTemplateToDetailsPane(rowid) {
	
	isEditClick = false;
	isAddClick = false;
	var promotionTemplate = templatesData[parseInt(rowid) - 1].template;
	
	var parsedDate = $.datepicker.parseDate('yy-mm-dd', formatDate(promotionTemplate.promoStartDate));
	$('#txtStartDateDetail').datepicker('setDate', parsedDate);
	
	parsedDate = $.datepicker.parseDate('yy-mm-dd', formatDate(promotionTemplate.promoEndDate));
	$('#txtEndDateDetail').datepicker('setDate', parsedDate);
	
	$("#promotionType").val(promotionTemplate.promotionTypeId);
	$("#promotionId").val(promotionTemplate.promotinId);
	$("#selStatus").val(promotionTemplate.status)
	
	promotionTypeChange();
	
	if (promotionTemplate.promotionTypeId == "1") {
		$("#txtFreeSeatMinSeats").val(nextSeatMinSeats);
		$("#txtFreeSeatMaxSeats").val(nextSeatMaxSeats);
		//$("#txtFreeSeatMinSeats").val(promotionTemplate.promotionTemplateParams[PromoTemplateParams['FreeSeat.SeatsMin']]);
		//$("#txtFreeSeatMaxSeats").val(promotionTemplate.promotionTemplateParams[PromoTemplateParams['FreeSeat.SeatsMax']]);
		$("#txtMinVacantSeats").val(promotionTemplate.promotionTemplateParams[PromoTemplateParams['FreeSeat.MinVacant']]);
		$("#txtPreserveSeats").val(promotionTemplate.promotionTemplateParams[PromoTemplateParams['FreeSeat.SeatsPreserve']]);
	} else if (promotionTemplate.promotionTypeId == "2") {
		$("#txtFlexiDateLowerBound").val(promotionTemplate.promotionTemplateParams[PromoTemplateParams['FlexiDate.DaysLowerBound']]);
		$("#txtFlexiDateUpperBound").val(promotionTemplate.promotionTemplateParams[PromoTemplateParams['FlexiDate.DaysUpperBound']]);	
	}
	
	$("select#applicableRoutes_1 option").remove();
	$("select#applicableRoutes_2 option").remove();
	$.each(promotionTemplate.routes, function(index, value) { 
		$("#applicableRoutes_1").append("<option value=\"" + value + "\">" + value + "</option>");
		$("#applicableRoutes_2").append("<option value=\"" + value + "\">" + value + "</option>"); 
	});

	generateChargesAndRewardsParamHtml();
	$('#chargesRewardsPane :input').attr('disabled', true);
	$('#promotionsTemplateDetails :input').attr('disabled', true);
    $("#txtStartDateDetail, #txtEndDateDetail").datepicker('disable');
	$.each(promotionTemplate.promoCharges, function(index, value) { 
		 $('#' + value.chargeCode).val(value.charge); 
	});
	
	generateChargesPane = false;
}

function promotionTypeChange() {
	var promoType = $("#promotionType").val();
	
	$("div#promoParamsPane > table").hide();
	
	if(promoType == "1") {
		$("table#paramsNextSeatFree").show();
		//hack for allowing 1 seat, until pnl/adl is allowed to 2 extra seats
		$("#txtFreeSeatMinSeats").val(nextSeatMinSeats);
		$("#txtFreeSeatMaxSeats").val(nextSeatMaxSeats);
		generateChargesAndRewardsParamHtml();
		generateChargesPane =true;
	} else if (promoType == "2") {
		$("table#paramsFlexiDate").show();
		generateChargesPane =true;
	}
	
}

function submitPromotionTemplate() {
	
	if(validatePromotion()){
		var data1 = {};
		var promoId = $("#promotionId").val();
		var promoType = parseInt($("#promotionType").val());
		var strUrl;
	
		data1['startDate'] = $("#txtStartDateDetail").val();
		data1['endDate'] = $("#txtEndDateDetail").val();
		data1['promotionTemplateTo.promotionTypeId'] = promoType;
		data1['promotionTemplateTo.promotinId'] = promoId;
		data1['promotionTemplateTo.status'] = $("#selStatus").val();
		
		if(promoType == 1) {
			data1['promotionTemplateTo.promotionTemplateParams.' + PromoTemplateParams['FreeSeat.SeatsMin']] = nextSeatMinSeats;
			data1['promotionTemplateTo.promotionTemplateParams.' + PromoTemplateParams['FreeSeat.SeatsMax']] = nextSeatMaxSeats;
			//data1['promotionTemplateTo.promotionTemplateParams.' + PromoTemplateParams['FreeSeat.SeatsMin']] = $("#txtFreeSeatMinSeats").val();
			//data1['promotionTemplateTo.promotionTemplateParams.' + PromoTemplateParams['FreeSeat.SeatsMax']] = $("#txtFreeSeatMaxSeats").val();
			data1['promotionTemplateTo.promotionTemplateParams.' + PromoTemplateParams['FreeSeat.MinVacant']] = $("#txtMinVacantSeats").val();
			data1['promotionTemplateTo.promotionTemplateParams.' + PromoTemplateParams['FreeSeat.SeatsPreserve']] = $("#txtPreserveSeats").val();
		} else if (promoType == 2) {
			data1['promotionTemplateTo.promotionTemplateParams.' + PromoTemplateParams['FlexiDate.DaysLowerBound']] = $("#txtFlexiDateLowerBound").val();
			data1['promotionTemplateTo.promotionTemplateParams.' + PromoTemplateParams['FlexiDate.DaysUpperBound']] = $("#txtFlexiDateUpperBound").val();		
		}
		
		// charges
		var chargeId;
		var chargeMap = {};
		$("input[is_promo_charge=\"true\"]").each(function() {
			chargeMap[$(this).attr("id")] = $(this).val();	
		});
		
		
		// routes
		var r = 0;
		var c = 0;
		var ondCode;
		$("select#applicableRoutes_1 option").each(function() {
			ondCode = $(this).val();
			data1['promotionTemplateTo.routes[' + (r++) + ']'] = ondCode;
			
			for (var key in chargeMap) {
				data1['promotionTemplateTo.promoCharges[' + c + '].ondCode'] = ondCode;
				data1['promotionTemplateTo.promoCharges[' + c + '].chargeCode'] = key;
				data1['promotionTemplateTo.promoCharges[' + c + '].charge'] = chargeMap[key];
				c++;
			}
		});
		
		if (promoId != '') {
			strUrl = 'promotionTemplatesManagement!updatePromotionTemplate.action' ;
		} else {
			strUrl = 'promotionTemplatesManagement!savePromotionTemplate.action' ;
		}
		
		
		var success;
		var message;
		$.ajax({ 
			url : strUrl,
			beforeSend : top[2].ShowProgress(),
			data : data1,
			type : "POST",
			dataType : "json",
			complete : function(resp) {
				clearPanel('promotionsTemplateDetails');
				resp = resp = eval("("+resp.responseText+")");
				if (resp.success) {
                    searchPromotionTemplates();
					alert("Promotion Template Successfully Saved.");
				} else if (resp.message == 'promotion.ond.overlap') {
					alert("Promotion's Add/Update Unsuccessful. ONDs are overlapped.");
				} else if (resp.message == 'promotion.edit.failed.active.requests') {
                    alert("Promotion's Add/Update Unsuccessful. Subscribed promotions exist.");
                }
				
				top[2].HideProgress();
			}
		});
		
	}
	
}

function generateChargesAndRewardsParamHtml () {
	var promoType = $("#promotionType").val();
	var paramsHtml = "";
	
	paramsHtml += "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"2\" id=\"chargesNextSeatFree\">";
	hideMessage();
	if(promoType == "1") {

//        var minSeats = $("#txtFreeSeatMinSeats").val();
//        var maxSeats = $("#txtFreeSeatMaxSeats").val();
//
//		if($("#txtFreeSeatMinSeats").val() == ""){
//			showERRMessage(minseatsRequired);
//			$("#txtFreeSeatMinSeats").focus();
//			return false;
//		} else if(isNaN($("#txtFreeSeatMinSeats").val())){
//			showERRMessage(minseatsIsNumber);
//			$("#txtFreeSeatMinSeats").focus();
//			return false;
//		} else if($("#txtFreeSeatMinSeats").val() > 2){
//			showERRMessage("Next seat free seat offer cannot be greater than 2");
//			$("#txtFreeSeatMinSeats").focus();
//			return false;
//		}
//		
//		if($("#txtFreeSeatMaxSeats").val() == "") {
//			showERRMessage(maxseatsRequired);
//			$("#txtFreeSeatMaxSeats").focus();
//			return false;
//		} else if(isNaN($("#txtFreeSeatMaxSeats").val())){
//			showERRMessage(maxseatsIsNumber);
//			$("#txtFreeSeatMaxSeats").focus();
//			return false;
//		} else if($("#txtFreeSeatMaxSeats").val() > 2){
//			showERRMessage("Next seat free seat offer cannot be greater than 2");
//			$("#txtFreeSeatMaxSeats").focus();
//			return false;
//		}

//        if (minSeats > maxSeats) {
//            showERRMessage("Next seat free: minimum seats cannot be greater than the maximum seats.");
//            $("#txtFreeSeatMaxSeats").focus();
//            return false;
//        }
		
		
		var htmlRegCharge = "<tr><td colspan=\"2\"><font>Registration Charge : Non-Refundable</font></td></tr>";
		var htmlSeatCharge = "<tr><td colspan=\"2\"><font>Seat Allocation Charge : Refundable</font></td></tr>";
		
		var seatsMin = parseInt($("#txtFreeSeatMinSeats").val());
		var seatsMax = parseInt($("#txtFreeSeatMaxSeats").val());
		var c;
		
		for (c = seatsMin; c <= seatsMax; c++) {
			if (c == 1) {
				htmlRegCharge += "<tr><td style=\"padding-left: 40px; width: 140px;\"><font>Per Seat</font></td>";
				htmlRegCharge += "<td><input type=\"text\" is_promo_charge=\"true\" name=\"" + PromoRewardsAndCharges['FreeSeat.RegistrationCharge'] + c + "\"" + 
				" id=\"" + PromoRewardsAndCharges['FreeSeat.RegistrationCharge'] + c + "\"" + 
				" style=\"width: 100px;\" maxlength=\"10\" align=\"middle\"><font class=\"mandatory\">&nbsp;*</font></td></tr>";	
				
				htmlSeatCharge += "<tr><td style=\"padding-left: 40px\"><font>Per Seat</font></td>";
				htmlSeatCharge += "<td><input type=\"text\" is_promo_charge=\"true\" name=\"" + PromoRewardsAndCharges['FreeSeat.PrefixChargeOnSeats'] + c + "\"" + 
				" id=\"" + PromoRewardsAndCharges['FreeSeat.PrefixChargeOnSeats'] + c + "\"" + 
				" style=\"width: 100px;\" maxlength=\"10\" align=\"middle\"><font class=\"mandatory\">&nbsp;*</font></td></tr>";				
			} else {
//				htmlRegCharge += "<td style=\"padding-left: 40px\"><font>Per " + c + " Seats</font></td>";
//				htmlRegCharge += "<td><input type=\"text\" is_promo_charge=\"true\" name=\"" + PromoRewardsAndCharges['FreeSeat.RegistrationCharge'] + c + "\"" + 
//				" id=\"" + PromoRewardsAndCharges['FreeSeat.RegistrationCharge'] + c + "\"" + 
//				" style=\"width: 100px;\" maxlength=\"10\" align=\"middle\"><font class=\"mandatory\">&nbsp;*</font></td></tr>";	
//				
//				htmlSeatCharge += "<td style=\"padding-left: 40px\"><font>Per " + c + " Seats</font></td>";
//				htmlSeatCharge += "<td><input type=\"text\" is_promo_charge=\"true\" name=\"" + PromoRewardsAndCharges['FreeSeat.PrefixChargeOnSeats'] + c + "\"" + 
//				" id=\"" + PromoRewardsAndCharges['FreeSeat.PrefixChargeOnSeats'] + c + "\"" + 
//				" style=\"width: 100px;\" maxlength=\"10\" align=\"middle\"><font class=\"mandatory\">&nbsp;*</font></td></tr>";				
			}
		}
		
		paramsHtml = htmlRegCharge + htmlSeatCharge;
	} else if (promoType == "2") {
		if($("#txtFlexiDateLowerBound").val() == ""){
			showERRMessage(lowerboundryRequired);
			$("#txtFlexiDateLowerBound").focus();
			return false;
		} else if(isNaN($("#txtFlexiDateLowerBound").val())){
			showERRMessage(lowerboundryIsNumber);
			$("#txtFlexiDateLowerBound").focus();
			return false;
		} 
		if($("#txtFlexiDateUpperBound").val() == ""){
			showERRMessage(upperboundryRequired);
			$("#txtFlexiDateUpperBound").focus();
			return false;
		} else if(isNaN($("#txtFlexiDateUpperBound").val())){
			showERRMessage(upperboundryIsNumber);
			$("#txtFlexiDateUpperBound").focus();
			return false;
		} 
		
		paramsHtml += "<tr><td colspan=\"2\"><font>Flexibility Rewards</font></td></tr>";
		var flexLowerBound = parseInt($("#txtFlexiDateLowerBound").val()) ;
		var flexUpperBound = parseInt($("#txtFlexiDateUpperBound").val()) ;
		var c;
		
		for (c = 1; c <= (flexUpperBound + flexLowerBound); c++) {
			if(c == 1) {
				paramsHtml += "<tr><td style=\"padding-left: 40px\"><font>Per Day</font></td>";
				paramsHtml += "<td><input type=\"text\" is_promo_charge=\"true\" name=\"" + PromoRewardsAndCharges['FlexiDate.PrefixRewardOnDays'] + c + "\"" + 
				" id=\"" + PromoRewardsAndCharges['FlexiDate.PrefixRewardOnDays'] + c + "\"" + 
				" style=\"width: 100px;\" maxlength=\"10\" align=\"middle\"><font class=\"mandatory\">&nbsp;*</font></td></tr>";
			} else {
				paramsHtml += "<td style=\"padding-left: 40px\"><font>Per " + c + " Days</font></td>";
				paramsHtml += "<td><input type=\"text\" is_promo_charge=\"true\" name=\"" + PromoRewardsAndCharges['FlexiDate.PrefixRewardOnDays'] + c + "\"" + 
				" id=\"" + PromoRewardsAndCharges['FlexiDate.PrefixRewardOnDays'] + c + "\"" + 
				" style=\"width: 100px;\" maxlength=\"10\" align=\"middle\"><font class=\"mandatory\">&nbsp;*</font></td></tr>";
			}
		}
		
	}
	paramsHtml += "</table>";
	$("#chargesRewardsPane").html(paramsHtml);
	
}

function dateFormatter(cellvalue, options, rowObject) {
	return formatDate(cellvalue);
}

function formatDate(value) {
	return value.substr(0, 10);
}

function resetClick() {
	clearPanel('promotionsTemplateDetails');
	if(isEditClick && selectedGridRowId != ""){
		performOperationsOnRowClick(selectedGridRowId);
		$('#btnEdit').trigger('click');
	}
}

function clearPanel(panelId) {
	$("#" + panelId + " input[type=\"text\"]").val('');
	$("#" + panelId + " input[type=\"hidden\"]").val('');
	$("#" + panelId + " textarea").val('');
	$("#" + panelId + " select[id^=\"applicableRoutes\"]").html("");
	$("#" + panelId + " select[id=\"availableRoutes\"]").html("");
	$("#" + panelId + " input[type=\"checkbox\"]").attr('checked', false);
    $("#" + panelId + " select[id=\"originAirport\"] option:first-child").attr("selected", "selected");
    $("#" + panelId + " select[id^=\"viaPoint\"] option:first-child").attr("selected", "selected");
    $("#" + panelId + " select[id=\"destAirport\"] option:first-child").attr("selected", "selected");

	generateChargesPane = false;
    clearERRMessage();

}

function hideMessage() {
	top[2].HidePageMessage();
}

function validatePromotion() {
	
	var StrCurrentDate = DateToString(new Date());

    if ($("#promotionType").val() == null) {
        showERRMessage(promotionTypeRequired);
        $("#promotionType").focus();
        return false;
    } else if ($("#txtStartDateDetail").val() == "") {
        showERRMessage(startDateRequired);
        $("#txtStartDateDetail").focus();
        return false;
    } else if (!CheckDates(formatDate(StrCurrentDate, "dd/mm/yyyy","dd-mmm-yyyy"),
    					formatDate($("#txtStartDateDetail").val(), "dd/mm/yyyy","dd-mmm-yyyy"))) {
    	showERRMessage(buildError(lowerCurrentDate, 'Start date '));
    	$("#txtStartDateDetail").focus();
    	return false;
    } else if ($("#txtEndDateDetail").val() == "") {
        showERRMessage(endDateRequired);
        $("#txtEndDateDetail").focus();
        return false;
    } else if (!CheckDates(formatDate(StrCurrentDate, "dd/mm/yyyy","dd-mmm-yyyy"),
                        formatDate($("#txtEndDateDetail").val(), "dd/mm/yyyy","dd-mmm-yyyy"))) {
    	showERRMessage(buildError(lowerCurrentDate, 'End date '));
    	$("#txtEndDateDetail").focus();
    	return false;
    } else if (!CheckDates(formatDate($("#txtStartDateDetail").val(), "dd/mm/yyyy","dd-mmm-yyyy"),
                            formatDate($("#txtEndDateDetail").val(), "dd/mm/yyyy","dd-mmm-yyyy"))) {
        showERRMessage(enddategreat);
        $("#txtEndDateDetail").focus();
        return false;
    } else if ($("#selStatus").val() == null) {
        showERRMessage(statusRequired);
        $("#selStatus").focus();
        return false;
    } else if ($("#promotionType").val() == 1) {
//        if ($("#txtFreeSeatMinSeats").val() == "") {
//            showERRMessage(minseatsRequired);
//            $("#txtFreeSeatMinSeats").focus();
//            return false;
//        } else if (isNaN($("#txtFreeSeatMinSeats").val())) {
//            showERRMessage(minseatsIsNumber);
//            $("#txtFreeSeatMinSeats").focus();
//            return false;
//        } else if ($("#txtFreeSeatMinSeats").val() < 0) {
//            showERRMessage(buildError(negativeNumber, 'Min Seats '));
//            $("#txtFreeSeatMinSeats").focus();
//            return false;
//        }
    	
//        if ($("#txtFreeSeatMaxSeats").val() == "") {
//            showERRMessage(maxseatsRequired);
//            $("#txtFreeSeatMaxSeats").focus();
//            return false;
//        } else if (isNaN($("#txtFreeSeatMaxSeats").val())) {
//            showERRMessage(maxseatsIsNumber);
//            $("#txtFreeSeatMaxSeats").focus();
//            return false;
//        } else if ($("#txtFreeSeatMaxSeats").val() < 0) {
//            showERRMessage(buildError(negativeNumber, 'Max Seats '));
//            $("#txtFreeSeatMaxSeats").focus();
//            return false;
//        }
        if ($("#txtMinVacantSeats").val() == "") {
            showERRMessage(minvacantseatsRequired);
            $("#txtMinVacantSeats").focus();
            return false;
        } else if (isNaN($("#txtMinVacantSeats").val())) {
            showERRMessage(minvacantseatsIsNumber);
            $("#txtMinVacantSeats").focus();
            return false;
        } else if ($("#txtMinVacantSeats").val() < 0) {
            showERRMessage(buildError(negativeNumber, 'Min Vacant Seats '));
            $("#txtMinVacantSeats").focus();
            return false;
        }
        if ($("#txtPreserveSeats").val() == "") {
            showERRMessage(preservRequired);
            $("#txtPreserveSeats").focus();
            return false;
        } else if (isNaN($("#txtPreserveSeats").val())) {
            showERRMessage(preserveIsNumber);
            $("#txtPreserveSeats").focus();
            return false;
        } else if ($("#txtPreserveSeats").val() < 0) {
            showERRMessage(buildError(negativeNumber, 'Preserve Seats '));
            $("#txtPreserveSeats").focus();
            return false;
        }

    } else if ($("#promotionType").val() == 2) {
        if ($("#txtFlexiDateLowerBound").val() == "") {
            showERRMessage(lowerboundryRequired);
            $("#txtFlexiDateLowerBound").focus();
            return false;
        } else if (isNaN($("#txtFlexiDateLowerBound").val())) {
            showERRMessage(lowerboundryIsNumber);
            $("#txtFlexiDateLowerBound").focus();
            return false;
        } else if ($("#txtFlexiDateLowerBound").val() < 0) {
            showERRMessage(buildError(negativeNumber, 'Lower Boundary (Days) '));
            $("#txtFlexiDateLowerBound").focus();
            return false;
        }
        if ($("#txtFlexiDateUpperBound").val() == "") {
            showERRMessage(upperboundryRequired);
            $("#txtFlexiDateUpperBound").focus();
            return false;
        } else if (isNaN($("#txtFlexiDateUpperBound").val())) {
            showERRMessage(upperboundryIsNumber);
            $("#txtFlexiDateUpperBound").focus();
            return false;
        } else if ($("#txtFlexiDateUpperBound").val() < 0) {
            showERRMessage(buildError(negativeNumber, 'Upper Boundary (Days) '));
            $("#txtFlexiDateUpperBound").focus();
            return false;
        }
    }

    if ($("#applicableRoutes_1")[0].length == 0) {
        showERRMessage(applicableRouteRequired);
        $("#applicableRoutes_1").focus();
        return false;
    } else if ($("#chargesRewardsPane").find("input:text").length == 0) {
        showERRMessage(chargesrewardsRequired);
        $(this).focus();
        return false;
    }

    var chargesContainsError = false;
    $("#chargesRewardsPane").find("input:text").each(function () {
        if ($(this).val() == "") {
            showERRMessage(chargesrewardsRequired);
            $(this).focus();
            chargesContainsError = true;
            return false;
        } else if (isNaN($(this).val())) {
            showERRMessage(chargesrewardsIsNumber);
            $("#txtFlexiDateUpperBound").focus();
            chargesContainsError = true;
            return false;
        } else if ($(this).val() < 0) {
            showERRMessage(buildError(negativeNumber, 'Charges/Rewards '));
            $("#txtFlexiDateUpperBound").focus();
            chargesContainsError = true;
            return false;
        }
    });

    if (chargesContainsError) {
        return false;
    } else {
        return true;
    }

}