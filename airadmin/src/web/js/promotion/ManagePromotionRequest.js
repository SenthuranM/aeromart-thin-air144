var promoRequests = {};
var selectedFlight = "";
var openedInPopUp = false;

jQuery(document).ready(function(){
	
	$("#divSearch").decoratePanel("Search Flights");
	$("#divDispPromoReqs").decoratePanel("Flexi-Date Promotion Requests");
	$("#btnApproveReqs").disableButton();
	$('#txtTotalRevenue').attr('readonly','true');
	
	if(cameFrom != "ALLOC"){
		constructFlightsGrid();
		$("#txtStartDateSearch, #txtStopDateSearch").datepicker({
			showOn: "button",
			buttonImage: "../../images/calendar_no_cache.gif",
			buttonImageOnly: true,
			dateFormat: 'dd/mm/yy'
		});
	}
	constructPromoReqsGrid(null);
	
	if(cameFrom == "ALLOC"){
		openedInPopUp = true;
		$("#divSearch").parent().hide();
		$("#divResultsPanel").hide();
		selectedFlight = fl_ID;
		searchPromoRequests(fl_ID);
	}
	
});

function searchFlights() {
	if(validateFlightSearch()){
		selectedFlight = "";
		var strUrl = "promotionRequestsAdmin!searchFlights.action?";
	
		strUrl += "flightNumber=" + $("#txtFlightNumber").val();
	
		var searchDate;
		searchDate = $("#txtStartDateSearch").val();
		if(searchDate != '') { 
			strUrl += "&flightDepStart=" + searchDate;
		}
		searchDate = $("#txtStopDateSearch").val();
		if(searchDate != '') {
			strUrl += "&flightDepEnd=" + searchDate;
		}
	
		$("#jqGridFlightsData").setGridParam({
			url : strUrl,
			page : 1
		});
		$("#jqGridFlightsData").trigger("reloadGrid");
		constructPromoReqsGrid(null);
	 }
}

function validateFlightSearch(){
	top[2].HidePageMessage();
	 if ($("#txtStartDateSearch").val() == ""){
			showERRMessage(startDateRequired);
			$("#txtStartDateDetail").focus();
			return false;
	 }
	 
	 if ($("#txtStopDateSearch").val() == ""){
			showERRMessage(endDateRequired);
			$("#txtEndDateDetail").focus();
			return false;
	 }
	 
	if (!dateChk("txtStartDateSearch")){
			showERRMessage(invalidDate);
			getFieldByID("txtStartDateSearch").focus();
			return false;
	}
	
	if (!dateChk("txtStopDateSearch")){
			showERRMessage(invalidDate);
			getFieldByID("txtStopDateSearch").focus();
			return false;
	 }
	
//	var schStartD = formatDate($("#txtStartDateSearch").val(), "dd/mm/yyyy","dd-mmm-yyyy");
//	var schStopD = formatDate($("#txtStopDateSearch").val(), "dd/mm/yyyy","dd-mmm-yyyy");
	
//	if (compareDates(schStartD,schStopD,'>')){
//		showERRMessage(stopdategreat);
//		return false;
//	}
	 
	 return true;
}

function HideProgress(){
	setVisible("divLoadMsg", false);
}

function ShowProgress(){
	setVisible("divLoadMsg", true);
}

function searchPromoRequests(flightId) {
	var strUrl = "promotionRequestsAdmin!retrievePromotionApprovalView.action?";
	strUrl += "flightId=" + flightId;
	constructPromoReqsGrid(strUrl);
}

function constructFlightsGrid() {
	$("#jqGridFlightsData")
	.jqGrid(
			{
				url : 'promotionRequestsAdmin!initSearch.action',
				datatype : "json",
				mtype : 'POST',
				jsonReader : {
					root : "rows",
					page : "page",
					total : "total",
					records : "records",
					repeatitems : false,
					id : "0"
				},
				rowNum : 20,
				height : 160,
				width : 930,
				loadui : 'block',
				colNames : [ '&nbsp;', 'Flight Number', 'Departure Date',
						'Origin', 'Destination', 'Segments', 'Seats Alloc', 'Seats Sold', 'Flight Id' ],
				colModel : [ {
					name : 'id',
					index : 'id',
					width : 40
				}, {
					name : 'flight.flight.flightNumber',
					index : 'flightNumber',
					width : 80,
					align : "center"
				}, {
					name : 'flight.flight.departureDate',
					index : 'promoStartDate',
					width : 120,
					align : "center"
				}, {
					name : 'flight.flight.originAptCode',
					index : 'originAptCode',
					width : 60,
					align : "center"
				}, {
					name : 'flight.flight.destinationAptCode',
					index : 'destinationAptCode',
					width : 60,
					align : "center"
				}, {
					name : 'flight.flight.flightSegements',
					index : 'flightSegements',
					width : 120,
					align : "center",
					formatter : function(cellvalue, options, rowObject) {
						var strSegs = "";
						
						$.each(cellvalue, function(index, value) { 
							strSegs += value.segmentCode  + " ";
						});
						
						return strSegs;
					}
				}, {
					name : 'flight.allocated',
					index : 'allocated',
					width : 80,
					align : "center"
				}, {
					name : 'flight.seatsSold',
					index : 'seatsSold',
					width : 80,
					align : "center"
				}, {
					name : 'flight.flight.flightId',
					index : 'flightId',
					width : 0,
					hidden : true
				} ],
				multiselect : false,
				viewrecords : true,
				onSelectRow : function(rowid) {
					var flightId = $("#jqGridFlightsData").getCell(rowid,
							'flight.flight.flightId')
					selectedFlight = flightId;
					searchPromoRequests(flightId);
					$('#txtTotalRevenue').removeAttr('readonly').val("");
					$('#txtTotalRevenue').attr('readonly','true');
				},
				pager : jQuery('#jqGridFlightsPages')
			});
}

function constructPromoReqsGrid(actionUrl) {
	
	if(actionUrl == null){
		actionUrl = 'promotionRequestsAdmin!initSearch.action';
	}
	$("#jqGridPromoReqsContainer").empty();
	$("#jqGridPromoReqsContainer").append('<table width="100%" border="0" cellpadding="0" cellspacing="0" id="jqGridPromoReqsData"></table><div id="jqGridPromoReqsPages"></div>')
	$("#jqGridPromoReqsData").jqGrid(
			{
				url : actionUrl,
				datatype : "json",
				mtype : 'POST',
				jsonReader : {
					root : "rows",
					page : "page",
					total : "total",
					records : "records",
					repeatitems : false,
					id : "0"
				},
				rowNum : 20,
				height : 160,
				width : 930,
				loadui : 'block',
				colNames : [ '&nbsp;', 'PNR', 'Passenger Count', 'Target Flight Number', 'Target Departure Date',
						'Revenue', 'Transferring', 'Rewards', 'Current Selling', 'Approve', 'Version', 'ResSegId',
						'FltSegId', 'FltSegCode', 'PromoReqId', 'SegTransfer' ],
				colModel : [ {
					name : 'id',
					index : 'id',
					width : 40
				}, {
					name : 'view.pnr',
					index : 'pnr',
					width : 80,
					align : "center"
				}, {
					name : 'view.passengerCount',
					index : 'totalPassengers',
					width : 100,
					align : "center"
				}, {
					name : 'view.flightNumber',
					index : 'flightNumber',
					width : 120,
					align : "center"
				}, {
					name : 'view.departureDate',
					index : 'departureDate',
					width : 120,
					align : "center"
				}, {
					name : 'view.revenue',
					index : 'revenueFromTransfer',
					width : 100,
					align : "center"
				},  {
					name : 'view.transferedSegmentsFare',
					index : 'transferedSegmentsFare',
					width : 100,
					align : "center"
				},  {
					name : 'view.rewards',
					index : 'rewards',
					width : 100,
					align : "center"
				}, {
					name : 'view.currentSellingFare',
					index : 'currentSellingFare',
					width : 100,
					align : "center"
				}, {
					name : 'view.approveReq',
					index : 'approveReq',
					width : 40,
					align : "center",
					formatter : function(cellvalue, options, rowObject) {
						return "<input id=\"approve_req_" + options.rowId + "\" type=\"checkbox\" onclick=\"calculateTotalRevenue()\" />";
					}
				}, {
					name : 'view.versionId',
					index : 'versionId',
					width : 0,
					hidden : true
				}, {
					name : 'view.resSegId',
					index : 'resSegId',
					width : 0,
					hidden : true
				}, {
					name : 'view.flightSegId',
					index : 'flightSegId',
					width : 0,
					hidden : true
				}, {
					name : 'view.flightSegCode',
					index : 'flightSegCode',
					width : 0,
					hidden : true
				}, {
					name : 'view.promotionReqId',
					index : 'promotionReqId',
					width : 0,
					hidden : true
				}, {
					name : 'view.segmentTransferData',
					index : 'segmentTransferData',
					width : 0,
					hidden : true
				} ],
				multiselect : false,
				viewrecords : true,
				onSelectRow : function(rowid) {
					var flightNumber = $("#jqGridPromoReqsData").getCell(rowid,
					'view.flightNumber')
					if(flightNumber == "No match / Same Flight"){
						$("#btnApproveReqs").disableButton();
					}else{
						$("#btnApproveReqs").enableButton();
					}
				},
				loadComplete : function (response) {
					var resp = eval("(" + response.responseText + ")");
					promoRequests = resp.rows;
				},
				pager : jQuery('#jqGridPromoReqsPages')
			});
}

function approvePromoRequests() {
	
	var data1 = {};
	var promoReqs = new Array();
	var promoReq = {};
	var index = 0;
	var promoRequestsGrid = $("#jqGridPromoReqsData");
	
	$.each(promoRequestsGrid.getDataIDs(), function(i, value) {
		if ($('#approve_req_' + value).is(':checked')) {
			promoReq = {
					'PNR' : promoRequestsGrid.getCell(value, 'view.pnr'),
					'RESERVATION_SEGMENT_ID' : promoRequestsGrid.getCell(value, 'view.resSegId'),
					'FLIGHT_SEGMENT_ID' : promoRequestsGrid.getCell(value, 'view.flightSegId') ,
					'VERSION_ID' : promoRequestsGrid.getCell(value, 'view.versionId'),
					'FLIGHT_SEGMENT_CODE' : promoRequestsGrid.getCell(value, 'view.flightSegCode'),
					'PROMOTION_REQUEST_ID' : promoRequestsGrid.getCell(value, 'view.promotionReqId'),
					'TRANSFER_SEGMENTS' : promoRequests[i].view.segmentTransferData,
					'PROMOTION_ID' : promoRequests[i].view.promotionId.toString(),
					'FLIGHT_SEGMENT_CODES' : promoRequests[i].view.approvingSegs
			};
			
			promoReqs[ index ] = promoReq;
			index++;
		}
	});
	
	data1 = {'promoApprovalData' : JSON.stringify(promoReqs)};
	
	$.ajax({ 
		url : 'promotionRequestsAdmin!approvePromotionRequests.action',
		beforeSend : ShowProgress(),
		data : data1,
		type : "POST",
		dataType : "json",
		complete : function(response) {
			var resp = eval("(" + response.responseText + ")");
			if(openedInPopUp){
				//showWindowCommonError(resp.msgType, resp.succesMsg);
				alert(resp.succesMsg);
			}else{
				showCommonError(resp.msgType, resp.succesMsg);
			}
			$('#txtTotalRevenue').removeAttr('readonly').val("");
			$('#txtTotalRevenue').attr('readonly','true');
			HideProgress();
			searchPromoRequests(selectedFlight);
		}
	});
}

function calculateTotalRevenue() {
	var promoRequestsGrid = $("#jqGridPromoReqsData");
	var totalRevenue = 0.0;
	
	$.each(promoRequestsGrid.getDataIDs(), function(index, value) {
		if ($('#approve_req_' + value).is(':checked')) {
			if(!(promoRequestsGrid.getCell(value, 'view.revenue') == "")){
				totalRevenue += parseFloat(promoRequestsGrid.getCell(value, 'view.revenue'));
			}
		}
	});
	
	$('#txtTotalRevenue').removeAttr('readonly').val(totalRevenue);
	$('#txtTotalRevenue').attr('readonly','true');
}

