
jQuery(document).ready(function(){
	
	$("#divSearch").decoratePanel("Search Flights");
	
	constructFlightsGrid();
	
	$("#txtStartDateSearch, #txtStopDateSearch").datepicker({
		showOn: "button",
		buttonImage: "../../images/calendar_no_cache.gif",
		buttonImageOnly: true,
		dateFormat: 'dd/mm/yy'
	});	
});

function HideProgress(){
	setVisible("divLoadMsg", false);
}

function ShowProgress(){
	setVisible("divLoadMsg", true);
}

function searchFlights() {
	
	if(validateFlightSearch()){
		var strUrl = "promotionRequestsSummary.action?";

        strUrl += "&requestStatus=" + $("#requestStatus").val();

		var searchDate;
		searchDate = $("#txtStartDateSearch").val();
		if(searchDate != '') { 
			strUrl += "&startDate=" + searchDate;
		}
		searchDate = $("#txtStopDateSearch").val();
		if(searchDate != '') {
			strUrl += "&endDate=" + searchDate;
		}
		
		var flightNo = $("#txtFlightNo").val();
		if(flightNo != '') {
				strUrl += "&flightNo=" + flightNo;
		}
	
		$("#jqGridFlightsData").setGridParam({
			url : strUrl,
			page : 1
		});
			$("#jqGridFlightsData").trigger("reloadGrid");
	}
}

function validateFlightSearch(){
	top[2].HidePageMessage();
	 if ($("#txtStartDateSearch").val() == ""){
			showERRMessage(startIsRequired);
			$("#txtStartDateDetail").focus();
			return false;
	 }
	 
	 if ($("#txtStopDateSearch").val() == ""){
			showERRMessage(endIsRequired);
			$("#txtEndDateDetail").focus();
			return false;
	 }
	 
	 if (!dateChk("txtStartDateSearch")){
			showERRMessage(invalidDate);
			getFieldByID("txtStartDateSearch").focus();
			return false;
	}
	
	if (!dateChk("txtStopDateSearch")){
			showERRMessage(invalidDate);
			getFieldByID("txtStopDateSearch").focus();
			return false;
	 }
	
//	var schStartD = formatDate($("#txtStartDateSearch").val(), "dd/mm/yyyy","dd-mmm-yyyy");
//	var schStopD = formatDate($("#txtStopDateSearch").val(), "dd/mm/yyyy","dd-mmm-yyyy");
//
//	if (compareDates(schStartD,schStopD,'>')){
//		showERRMessage(stopdategreat);
//		return false;
//	}
	 
	 return true;
	
}

function constructFlightsGrid() {
	$("#jqGridFlightsData")
	.jqGrid(
			{
				url : 'promotionRequestsSummary!initSearch.action',
				datatype : "json",
				mtype : 'POST',
				jsonReader : {
					root : "rows",
					page : "page",
					total : "total",
					records : "records",
					repeatitems : false,
					id : "0"
				},
				rowNum : 20,
				height : 435,
				width : 930,
				loadui : 'block',
				colNames : [ '&nbsp;', 'Flight Number', 'Departure Date',
						'Segment Code', 'Total Requests', 'Active Req', 'Approved Req', 'Rejected Req', 'Refunds' ],
				colModel : [ {
					name : 'id',
					index : 'id',
					width : 40
				}, {
					name : 'view.flightNumber',
					index : 'flightNumber',
					width : 80,
					align : "center"
				}, {
					name : 'view.departureDate',
					index : 'departureDate',
					width : 120,
					align : "center"
				}, {
					name : 'view.segmentCode',
					index : 'segmentCode',
					width : 60,
					align : "center"
				}, {
					name : 'view.totalRequests',
					index : 'totalRequests',
					width : 60,
					align : "center"
				}, {
					name : 'view.activeRequests',
					index : 'activeRequests',
					width : 60,
					align : "center"
				}, {
					name : 'view.approvedRequests',
					index : 'approvedRequests',
					width : 60,
					align : "center"
				}, {
					name : 'view.rejectedRequests',
					index : 'rejectedRequests',
					width : 60,
					align : "center"
				}, {
					name : 'view.refundAmount',
					index : 'refundAmount',
					width : 0,
					width : 80,
					align : "center"
				} ],
				multiselect : false,
				viewrecords : true,
				onSelectRow : function(rowid) {
				},
				pager : jQuery('#jqGridFlightsPages')
			});
}





