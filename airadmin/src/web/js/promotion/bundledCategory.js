var isNewConfig = true;
var allLanguages = [];
var allPriorities = [];
var bundledCategories = {};
var defaultLanguage = '';


$(document).ready(function(){

    $("#divBundledCategorySearch").decoratePanel("Search Bundled Categories");
    $("#divBundledCategoryEdit").decoratePanel("Add/Modify Bundled Categories");

    $("#bundledCategoryTabs").tabs();

    $("#btnSave").disableButton();
    $("#btnReset").disableButton();
    $("#btnAdd").enableButton();
    $("#btnEdit").disableButton();
    $("#btnDelete").disableButton();
    $("#txtName").attr("disabled", true);
    $("#selStatusInDetail").attr("disabled", true);
    $("#txtPriority").attr("disabled", true);
    $("#txtDescription").attr("disabled", true);

    loadInitdata();

    $("#btnAdd").click(function(){
        addBundleCategory();
    });

    $("#btnEdit").click(function(){
        editBundleCategory();
    });

    $("#btnSearch").click(function(){
        $('#bundledCategoryGrid').trigger("reloadGrid");
    });

    $("#btnSave").click(function(){
        saveBundledCategory();
        setTimeout(function(){ $('#bundledCategoryGrid').trigger("reloadGrid"); }, 500);
        resetClicked();
    });

    $("#btnReset").click(function(){
        resetClicked();
    });

    $("#btnDelete").click(function(){
        deleteBundledCategory();
        setTimeout(function(){ $('#bundledCategoryGrid').trigger("reloadGrid"); }, 500);
    });




    $("#bundledCategoryGrid").jqGrid({
        url : 'showBundledCategory!searchCategories.action',
        postData : {
            'searchCriteria.status' : function(){return $("#selStatus").val()},
            'searchCriteria.priority' : function(){return $("#selPriority").val()}
        },
        datatype : "json",
        jsonReader : {
            root : "rows",
            page : "page",
            total : "total",
            records : "records",
            repeatitems : false,
            id : "0"
        },
        colNames : [ '#', 'Category name',
            'Status', 'Priority' , 'Description','Translations', 'Version'
        ],
        colModel : [
            {
                name : 'bundledCategoryId',
                width : 40,
                jsonmap : 'bundledCategoryId',
                hidden:true
            },
            {
                name : 'categoryName',
                width : 125,
                index : 'categoryName'
            },
            {
                name : 'status',
                width : 150,
                align : "center",
                index : 'status'
            },
            {
                name : 'priority',
                width : 150,
                align : "center",
                index : 'priority'

            },
            {
                name : 'description',
                width : 275,
                index : 'description'

            },
            {
                name : 'translations',
                width : 225,
                align : "center",
                index : 'translations',
                hidden : true
            },
            {
                name : 'version',
                width : 225,
                align : "center",
                index : 'version',
                hidden : true
            }

        ],
        imgpath : '../../themes/default/images/jquery',
        multiselect : false,
        pager : jQuery('#divPager'),
        rowNum : 10,
        viewrecords : true,
        height : 165,
        width : 910,
        loadui : 'block',
        afterInsertRow : function(rowid,rowdata,rowelement ) {
            bundledCategories[rowelement.bundledCategoryId] = rowelement;
        },
        onSelectRow : function(rowid) {
            rowClicked(rowid);
        }
    })
        .navGrid("#divPager", {
            refresh : true,
            edit : false,
            add : false,
            del : false,
            search : false
        });
});


loadInitdata = function(){
    $.ajax({
        url : 'showBundledCategory.action',
        beforeSend : showProgressBar(),
        type : "GET",
        async:false,
        dataType : "json",
        complete:function(response) {

            var res = eval("(" + response.responseText + ")");

            if(res.defaultLanguage) {
                defaultLanguage = res.defaultLanguage;
            }

            if(res.allLanguages) {
                allLanguages = res.allLanguages;

                var populatedLangs = [];

                allLanguages.forEach(function(language) {
                    if((language.ibeContent === 'Y' || language.xbeContent === 'Y') && defaultLanguage !== language.languageCode) {
                        populatedLangs.push({"code": language.languageCode , "languageName": language.languageName});
                    }
                });

                populatedLangs.sort(compareLanguage);

                $("#translationsTemplate").iterateTemplete({templeteName:"translationsTemplate", data:populatedLangs, dtoName:"translation"});

            }

            if(res.allPriorities) {
                allPriorities = res.allPriorities;
                allPriorities.sort();

                allPriorities.forEach(function (priority) {
                    $('#selPriority').append($('<option>', {
                        value: priority,
                        text : priority
                    }));
                });
            }

            hideProgressBar();
        }
    });
}


rowClicked = function(rowid) {

    $("#btnEdit").enableButton();
    $("#btnDelete").enableButton();
    $("#btnSave").disableButton();
    $("#btnReset").disableButton();

    $("#frmBundleCategory").disableForm();

    $("#bundledCategoryGrid").GridToForm(rowid,"#frmBundleCategory");
    var id = $("#bundledCategoryGrid").getCell(rowid,'bundledCategoryId');
    var bdlCat = bundledCategories[id];
    var populatedLangs = [];

    if(allLanguages) {
        allLanguages.forEach(function(language) {
            if((language.ibeContent === 'Y' || language.xbeContent === 'Y') && defaultLanguage !== language.languageCode) {
                populatedLangs.push({"code": language.languageCode , "languageName": language.languageName});
            }
        });

        populatedLangs.sort(compareLanguage);

        $("#translationsTemplate").iterateTemplete({templeteName:"translationsTemplate", data:populatedLangs, dtoName:"translation"});
    }
}


addBundleCategory = function(){
    $("#btnEdit").disableButton();
    $("#btnReset").enableButton();
    $("#btnSave").enableButton();
    $("#btnDelete").disableButton();
    $("#bundledCategoryGrid").resetSelection();
    $("#frmBundleCategory").clearForm();
    $("#frmBundleCategory").enableForm();

    isNewConfig = true;
}


resetClicked = function(){

    $("#btnEdit").disableButton();
    $("#btnDelete").disableButton();
    $("#btnAdd").enableButton();
    $("#btnSave").disableButton();
    $("#btnReset").disableButton();

    $("#tblAirline").resetSelection();
    $("#frmBundleCategory").clearForm();
    $("#frmBundleCategory").disableForm();
}

editBundleCategory = function() {

    $("#btnEdit").disableButton();
    $("#btnReset").enableButton();
    $("#btnSave").enableButton();
    $("#frmBundleCategory").enableForm();

    isNewConfig = false;
}

saveBundledCategory = function(){
    if(validateData()){
        var submitURL=isNewConfig?"showBundledCategory!add.action":"showBundledCategory!edit.action";
        var data = getBundleCategoryData(isNewConfig);

        $.ajax({
            type: "POST",
            url: submitURL,
            data: data,
            success: successFunction,
            dataType: 'json'
        });
    }
}

deleteBundledCategory = function(){

    var submitURL= "showBundledCategory!delete.action";
    var data = {};
    data['bundleFareCategory.bundledCategoryId'] = $('#hdnBundledCategoryId').val();

    $.ajax({
        type: "POST",
        url: submitURL,
        data: data,
        success: successFunction,
        dataType: 'json'
    });
}

successFunction = function(response){
    if(response.success){
        showMsg("Success",response.messageTxt);
        searchBundledFares();
    }else{
        showMsg("Error",response.messageTxt);
    }
    hideProgressBar();
}

getBundleCategoryData = function(isNewConfig) {

    var data = {};

    data['bundleFareCategory.categoryName'] = $('#txtName').val();
    data['bundleFareCategory.priority'] = $("#txtPriority").val();
    data['bundleFareCategory.status'] = $("#selStatusInDetail").val();
    data['bundleFareCategory.description'] = $("#txtDescription").val();

    if(!isNewConfig) {
        data['bundleFareCategory.bundledCategoryId'] = $('#hdnBundledCategoryId').val();
        data['bundleFareCategory.version'] = $('#hdnVersion').val();
    }

    return data;
}

validateData = function(){

    if($("#txtName").val() === ""){
        showERRMessage("Invalid bundle category name");
        return false;
    }

    if ($("#txtPriority").val() === ""){
        showERRMessage("Invalid bundle category priority");
        return false;
    }

    if ($("#selStatusInDetail").val() === ""){
        showERRMessage("Invalid bundle category status");
        return false;
    }

    return true;
}

compareLanguage = function(a,b) {
    if (a.languageName < b.languageName)
        return -1;
    if (a.languageName > b.languageName)
        return 1;
    return 0;
}

showProgressBar = function(){
    top[2].ShowProgress();
}

hideProgressBar = function(){
    top[2].HideProgress();
}