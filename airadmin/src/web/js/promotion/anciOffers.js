var anciCriterias = {};
var selectedAnciCriteria = null;
var translationSeperator="=>";
var newSearch = false;
var translationsInitialized = false;
var isNewCriteria = true;

$(document).ready(function(){
	
	$("#divAnciOfferSearch").decoratePanel("Search Anci Offers");
	$("#anciOfferDetails").decoratePanel("Add/Edit Anci Offers");
	$("#anciOfferDetails").hide();
	$("#selApplicableDaysInBound").disable();
	
	$("#selAnciTemplateType").change();
	
	$("#btnAdd").click(function(){
		addAnciOffers();
	});
	
	$("#btnEdit").click(function(){
		editAnciOffers();
	});
	
	$("#btnTranslations").click(function(){
		initTranslationDialog();
	});
	
	$("#btnAddTranslations").click(function(){
		addTranslation();
		resetTranslations();
	});
	
	$("#btnRemoveTranslations").click(function(){
		removeTranslation();
	});
	
	$("#btnTranslationReset").click(function(){
		resetTranslations();
		resetTranslationsSet();
	});
	
	$("#btnTranslationSubmit").click(function(){
		$("#internationalizationDialog").dialog('close');
	});
	
	$("#eligibleReturn").click(function(){
		if($("#eligibleReturn").is(':checked')){
			$("#selApplicableDaysInBound").enable();
			$("#applReturn").enable();
			$("#applReturn").attr('checked', true);
		}else{
			$("#selApplicableDaysInBound").disable();
			$("#applReturn").disable();
			$("#applReturn").attr('checked', false);
			$("#selApplicableDaysInBound option:selected").removeAttr("selected");
		}
	});
	
	$("#btnBack").click(function(){
		resetAnciOffers();
		$('#jqGridAnciOfferContainer').show();
		$("#anciOfferDetails").hide();
	});
	
	$(".addItem").click(function(e){
		addItemsToSelect(e.target.id);
	});
	$(".removeItem").click(function(e){
		removeItemsFromSelect(e.target.id);
	});
	
	$("#btnSearch").click(searchAnciOffers);
	$("#btnSave").click(saveAnciOffer);
	
	$("#btnClose").click(function(){
		top[1].objTMenu.tabRemove("SC_PROM_05");
	});
	
	$("#btnReset").click(function(e){
		resetAnciOffers();
		translationsInitialized = false;
		isNewCriteria = true;
	});
	
	
	$("#txtStartDateSearch, #txtStopDateSearch" ).datepicker({
		dateFormat: 'mm/dd/yy',
		minDate: null
	}).keyup(function(e) {
	    if(e.keyCode == 8 || e.keyCode == 46) {
	        $.datepicker._clearDate(this);
	    }
	});
	
	$("#applicableFrom, #applicableTo" ).datepicker({
		dateFormat: 'dd/mm/yy',
		minDate: new Date()
	}).keyup(function(e) {
	    if(e.keyCode == 8 || e.keyCode == 46) {
	        $.datepicker._clearDate(this);
	    }
	});
	
	$.each(anciTemplateTypes,function(key,value){
		$("#selTemplateType").append('<option value='+key+'>'+key+'</option>');
	});
	
	$("#selTemplateType").change(function(){
		templateTypeChange();
	});
	
	$.extend(jQuery.jgrid.defaults, {
		prmNames: {
			id: "rowid", page: "page", rows: "rows",
			oper: "oper", sort: "searchCriteria.sortColumn", order: "searchCriteria.sortOrder"
		}
	}); 
	
	createGrid();
	
	$("#adultPCount, #childPCount, #infantPCount").numeric();
	
	$("#txtBCAutoComplete").autocomplete({
		source: bcJSONList
	});
	
	templateTypeChange();
});

/*
 * Function creating the display grid for existing data.
 */
function createGrid(){
	$('#jqGridAnciOfferData').jqGrid({
		url : "manageAnciOfferCriteria!search.action",
		postData : {
			'searchCriteria.OND' : function() { 
					var ond = $("#selFrom").val()+"/"+$("#selTo").val();
					return ond == "/" ? "" : ond;
				},
			'searchCriteria.from' : function() { return $("#txtStartDateSearch").val(); },
			'searchCriteria.to' : function() { return $("#txtStopDateSearch").val(); },
			'searchCriteria.active' : function() { return $("#selStatus").val() == "" ? null :$("#selStatus").val(); },
			'searchCriteria.anciOfferType' : function() { return $("#selOfferType").val() == "" ? null :$("#selOfferType").val(); },
			'newSearch' : function() { return newSearch; }
			},
		datatype : 'json',
		jsonReader : {
			root: 'anciOfferCriterias', 
			page: "page",
			total: "totalPages",
			records: "totalRecords",
			repeatitems: false,	
			id: 'id'	
		},
		colNames:['ID','Anci Offer Type','Applicable From','Applicable To'],
		colModel:[
		{name:'id', jsonmap:"id",align:"center"},
		{name:'anciTemplateType',  jsonmap:'anciTemplateType'},
		{name:'applicableFrom',  jsonmap:'applicableFrom',formatter:'date'},
		{name:'applicableTo', jsonmap:'applicableTo', formatter:'date', align:"center"}
	],
		rowNum : 20,
		viewRecords : true,
		height : 400,
		width : 920,
		pager : '#jqGridAnciOfferPages',
		afterInsertRow : function(rowid,rowdata,rowelement ){
			anciCriterias[rowelement.id] = rowelement;
		},
		onSelectRow : function(rowid,status,e){
			selectedAnciCriteria = anciCriterias[rowid];
		}
	});
}

/*
 * This method will reload the display grid and search with the given parameters.
 */
searchAnciOffers = function(){
	var applicableFrom = $("#txtStartDateSearch").datepicker("getDate");
	var applicableTo = $("#txtStopDateSearch").datepicker("getDate");
	
	if(applicableFrom > applicableTo){
		alert("Start date cannot be greater than End date.");
		return false;
	}
	
	newSearch = true;
	$('#jqGridAnciOfferData').trigger("reloadGrid");
	newSearch = false;
	
	resetAnciOffers();
	$('#jqGridAnciOfferContainer').show();
	$("#anciOfferDetails").hide();
	selectedAnciCriteria = null;
}

/*
 * Method handling creqating and updating anci offer criteria.
 */
saveAnciOffer = function(){
	if(validateData()){
		var submitURL=isNewCriteria?"manageAnciOfferCriteria!create.action":"manageAnciOfferCriteria!update.action";
		var data = {"anciOffer":$.toJSON(getAnciOfferData(isNewCriteria))};
		var translationData = getTranslationsForName();
		
		$.extend(data,translationData);
		
		showProgressBar();
		
		$.ajax({
			type: "POST",
			url: submitURL,
			data: data,
			success: successFunction,
			dataType: 'json'
		});
	}
}

/*
 * Method handling ajax request callback.
 */
successFunction = function(data, textStatus, jqXHR){
	if(data.success){
		showMsg("Success",data.messageTxt);
		searchAnciOffers();
	}else{
		showMsg("Error",data.messageTxt);
	}
	hideProgressBar();
}

/*
 * Method doing the preprocessing required to add a new anci offer.
 */
addAnciOffers = function(){
	$('#jqGridAnciOfferContainer').hide();
	$("#anciOfferDetails").show();
	translationsInitialized = false;
	isNewCriteria = true;
	resetAnciOffers();
}

/*
 * Method setting the data to be edited and doing the necessary pre-processing
 */
editAnciOffers = function(){
	if(selectedAnciCriteria == null){
		alert("Please select an Anci Offer to edit.");
		return;
	}
	
	translationsInitialized = true;
	isNewCriteria = false;
	
	$('#jqGridAnciOfferContainer').hide();
	$("#anciOfferDetails").show();
	
	resetAnciOffers();
	
	$('#jqGridAnciOfferContainer').hide();
	
	$("#selTemplateType").val(selectedAnciCriteria.anciTemplateType);
	templateTypeChange();
	
	$("#selTemplateID").val(selectedAnciCriteria.templateID);
	
	$("#applicableFrom").val(selectedAnciCriteria.applicableFrom);
	$("#applicableTo").val(selectedAnciCriteria.applicableTo);
	
	$("#applicableFrom").datepicker("setDate", new Date(selectedAnciCriteria.applicableFrom));
	$("#applicableTo").datepicker( "setDate", new Date(selectedAnciCriteria.applicableTo));
	
	$("#applReturn").attr('checked', selectedAnciCriteria.applicableForReturn);
	$("#applOneway").attr('checked', selectedAnciCriteria.applicableForOneWay);
	
	$("#eligibleReturn").attr('checked', selectedAnciCriteria.eligibleForReturn);
	$("#eligibleOneway").attr('checked', selectedAnciCriteria.eligibleForOneWay);
	
	$("#chkActive").attr('checked', selectedAnciCriteria.active);
	$("#chkApplyForFlexi").attr('checked', selectedAnciCriteria.applyToFlexiOnly);
	
	addOptionsToSelect("selPaxCount",selectedAnciCriteria.eligiblePaxCounts);
	addOptionsToSelect("selONDs",selectedAnciCriteria.onds);
	
	$("#selLCCs").val(selectedAnciCriteria.lccs);
	$("#selFlexiCodes").val(selectedAnciCriteria.flexiRuleIDs);
	$("#selApplicableDaysOutBound").val(selectedAnciCriteria.applicableOutboundDaysOfTheWeek);
	$("#selApplicableDaysInBound").val(selectedAnciCriteria.applicableInboundDaysOfTheWeek);
	
	if(selectedAnciCriteria.applicableForReturn){
		$("#selApplicableDaysInBound").enable();
	}
	
	$('#selBookingClasses').empty();
	addOptionsToSelect("selBookingClasses",selectedAnciCriteria.bookingClasses);
	
	setSelectedCriteriaTranslations();
	
	$("#txtName").disable();
	$("#txtDescription").disable();
}

/*
 * Sets the translations details in an existing (currently selected) anci offer.
 */
setSelectedCriteriaTranslations = function(){
	//Set the translations in the UI.
	$.each(selectedAnciCriteria.nameTranslations,function(key,value){
		var language = key;
		var name = value;
		var description = selectedAnciCriteria.descriptionTranslations[language];
		
		var representation=language + translationSeperator + name + translationSeperator + description;
		$("#selTranslations").append('<option value="'+language+'">'+representation+'</option>');
		$("#selTranslations").find("option[value='"+language+"']").text(representation);
		//Set English name and description on text boxes
		if(language == "en"){
			$("#txtName").val(name);
			$("#txtDescription").val(description);
		}
	});
	
	
}

/*
 * Resets the input fields and selects boxes.
 */
resetAnciOffers = function(){
	
	$("#selLanguage, #selOrigin, #selDestination, #selTemplateType, #selTemplateID, " +
			"#selVia1, #selVia2, #selVia3, #selVia4").prop('selectedIndex', '-1');
	
	$("#selApplicableDaysOutBound option:selected, #selApplicableDaysInBound option:selected, " +
			"#selLCCs option:selected, #selFlexiCodes option:selected").removeAttr("selected");
	
	$('#selBookingClasses').empty().append('<option value="ALL">ALL</option>');
	
	$("#applOneway, #applReturn, #chkApplyForFlexi, #eligibleReturn, #eligibleOneway").attr('checked', false);
	$("#chkActive").attr('checked', true);
	
	$("#txtName, #applicableFrom, #applicableTo, #txtDescription, #adultPCount, #childPCount, #infantPCount, #txtTranslatedName, " +
			"#txtTranslatedDesc, #txtBCAutoComplete").val("");
	
	$("#selPaxCount").children().remove();
	$("#selONDs").children().remove();
	$("#selTranslations").children().remove();
	
	$("#selApplicableDaysInBound").disable();
	
	$("#txtName").enable();
	$("#txtDescription").enable();
}

/*
 * Adds the items to their respective select elements
 */
addItemsToSelect = function(id){
	if(id=="btnAddOND"){
		
		var origin = $("#selOrigin").val();
		var destination = $("#selDestination").val();
		var via1 = $("#selVia1").val();
		var via2 = $("#selVia2").val();
		var via3 = $("#selVia3").val();
		var via4 = $("#selVia4").val();
		
		var viaPoints=[];
		viaPoints.push(via1);
		viaPoints.push(via2);
		viaPoints.push(via3);
		viaPoints.push(via4);
		
		if(origin == null || destination == null){
			alert("Origin and Destination need to be selected");
			return false;
		}else if(origin != "ALL" && destination != null && origin == destination){
			alert("Origin and Destination can't be the same");
			return false;
		}
		
		var wasPrevViaPointEmpty = false;
		
		for(var i=0; i<viaPoints.length; i++) {
			var value = viaPoints[i];
			if(wasPrevViaPointEmpty && value != null && value != ""){
				alert("Via points should be selected in order");
				return false;
			}
			if((value==null || value == "") && !wasPrevViaPointEmpty){
				wasPrevViaPointEmpty = true;
			}
		}
		
		var sortedViaPoints = viaPoints.sort();
		var prevValue = null;
		for(var i=0; i<sortedViaPoints.length; i++) {
			var value = sortedViaPoints[i];
			if(prevValue == value && value != "ALL" && value != null && value != ""){
				alert("Duplicate via points");
				return false;
			}
			prevValue = value;
		}
		
		var concatVal = origin + '/' 
		if(via1!=null && via1 != ""){ concatVal += via1 + '/'; }
		if(via2!=null && via2 != ""){ concatVal += via2 + '/'; }
		if(via3!=null && via3 != ""){ concatVal += via3 + '/'; }
		if(via4!=null && via4 != ""){ concatVal += via4 + '/'; }
		concatVal += destination;
		
		if($("#selONDs option[value='"+concatVal+"']").length > 0){
			alert("Origin and Destination already exist");
			return false;
		}
		
		$('#selONDs').append($("<option/>", {
	        value: concatVal,
	        text: concatVal
	    }));
		
	}else if(id=="addPaxCount"){
		var adultCount = $("#adultPCount").val();
		var childCount = $("#childPCount").val();
		var infantCount = $("#infantPCount").val();
		
		if(adultCount == ""){
			adultCount="-1";
		}
		if(childCount == ""){
			childCount="-1";
		}
		if(infantCount == ""){
			infantCount="-1";
		}
		
		if(parseInt(adultCount,10) < parseInt(infantCount,10)){
			alert("Infant count cannot be higher than adult count.");
			return false;
		}
		
		var concatVal = adultCount + ',' + childCount + ',' + infantCount;
		
		if($("#selPaxCount option[value='"+concatVal+"']").length > 0){
			alert("Pax count combination already exist");
			return false;
		}
		
		$('#selPaxCount').append($("<option/>", {
	        value: concatVal,
	        text: concatVal
	    }));
	}else if(id=="addBC"){
		var bcVal = $("#txtBCAutoComplete").val();
		
		if($("#selBookingClasses option[value='"+bcVal+"']").length > 0){
			alert("Booking class already exist");
			return false;
		}else if($.inArray(bcVal, bcJSONList) == -1){
			alert("Invalid BC detected. Please verify. If a new BC was created please reload the anci offer page.");
			return false;
		}
		
		$("#selBookingClasses option[value='ALL']").remove();
		$('#selBookingClasses').append($("<option/>", {
	        value: bcVal,
	        text: bcVal
	    }));
	}
}

/*
 * Add options to the gives select element. selectID should be passed on without any alterations
 */
addOptionsToSelect = function(selectID,values){
	$.each(values,function(index,value){
		$("#"+selectID).append('<option value='+value+'>'+value+'</option>');
	});
}

/*
 * Remove the selected option from the given select element.
 */
removeItemsFromSelect = function(id){
	if(id=="btnRemoveOND"){
		$('option:selected', $("#selONDs")).remove();
	}else if(id=="removePaxCount"){
		$('option:selected', $("#selPaxCount")).remove();
	}else if(id=="removeBC"){
		$('option:selected', $("#selBookingClasses")).remove();
		
		if($('#selBookingClasses option').length < 1){
			$('#selBookingClasses').append($("<option/>", {
		        value: "ALL",
		        text: "ALL"
		    }));
		}
	}
}

/*
 * Retrieves the data from the inputs. Depending on the isNew parameter id and version 
 * are set afresh or from the selected offer criteria.
 */
getAnciOfferData = function(isNew){
	var data = {};
	data["id"] = isNew ? 0 : selectedAnciCriteria.id;
	data["anciTemplateType"]=$("#selTemplateType").val();
	data["templateID"]=$("#selTemplateID").val();
	data["applicableFrom"]=$("#applicableFrom").val();
	data["applicableTo"]=$("#applicableTo").val();
	data["applicableForReturn"]=$("#applReturn").is(':checked');
	data["applicableForOneWay"]=$("#applOneway").is(':checked');
	data["eligibleForReturn"] = $("#eligibleReturn").is(':checked');
	data["eligibleForOneWay"] = $("#eligibleOneway").is(':checked');
	data["applyToFlexi"]=$("#chkApplyForFlexi").is(':checked');
	data["active"]=$("#chkActive").is(':checked');
	data["version"]=isNew ? -1 : selectedAnciCriteria.version;
	
	data["eligiblePaxCounts"]=getNonSelectedOptions("selPaxCount");
	
	var bcArr = [];
	$('#selBookingClasses option').each(function() {
		bcArr.push($(this).val());
	});
	
	data["bookingClasses"]=bcArr;
	data["lccs"]=$("#selLCCs").val();
	data["onds"]=getNonSelectedOptions("selONDs");
	data["flexiRuleIDs"]=$("#selFlexiCodes").val();
	data["applicableOutboundDaysOfTheWeek"]=$("#selApplicableDaysOutBound").val();
	data["applicableInboundDaysOfTheWeek"]=$("#selApplicableDaysInBound").val();
	
	return data;
}

/*
 * Retrieves all the options from a select list. id should be passed without additional modifiers like #id
 */
getNonSelectedOptions = function(id){
	var valueList=new Array();
	$("#"+id+" option").each(function()
	{
	    valueList.push($(this).val());
	});
	
	return valueList;
}

/*
 * Sets the initial translations from the english name and description fields if necessary and displays the translations
 * popup.
 */
initTranslationDialog = function(){
	if($("#txtName").val() == "" || $("#txtDescription").val() == ""){
		alert("Offer Name and Description can't be empty, please enter those details to enter transltions");
		return false;
	}
	
	$("#internationalizationDialog").dialog({ height: 400, width: 500, title: "Name And Description Translations", modal: true });
	
	if(!translationsInitialized){
		pupulateTranslationsFromInitialData();
	}
	
	translationsInitialized = true;
}

/*
 * Pupulates all language translations from english name and descriptions entered intitially.
 */
pupulateTranslationsFromInitialData = function(){
	$("#selLanguage option").each(function()
	{
		$("#selLanguage").val($(this).val());
		$("#txtTranslatedName").val($("#txtName").val());
		$("#txtTranslatedDesc").val($("#txtDescription").val());
		addTranslation();
	});
}

/*
 * Add the translation to the translations select element.
 */
addTranslation = function(){
	var language=$("#selLanguage").val();
	var name=$("#txtTranslatedName").val();
	var description=$("#txtTranslatedDesc").val();
	
	if(name == "" || description == ""){
		alert("Name or Description cannot be blank");
		return false;
	}
	
	var representation=language + translationSeperator + name + translationSeperator + description;
	
	if($("#selTranslations option[value='"+language+"']").length > 0){
		
		$("#selTranslations option").each(function()
		{
			if($(this).val() == language){
				$(this).text(representation);
			}
			
		});
		
	}else{
		$("#selTranslations").append('<option value="'+language+'">'+representation+'</option>');
	}
}

/*
 * Remove the selected translation and set it's values to be edited.
 */
removeTranslation = function(){
	var representation=$("#selTranslations option:selected").text();
	var detailArr=representation.split(translationSeperator);
	
	var language=detailArr[0];
	var name=detailArr[1];
	var description=detailArr[2];
	
	$("#selLanguage").val(language);
	$("#txtTranslatedName").val(name);
	$("#txtTranslatedDesc").val(description);
}

/*
 * Resets the translation inputs and sets the language select to the fist option.
 */
resetTranslations = function(){
	$("#selLanguage").val($("#selLanguage option:first").val());
	$("#txtTranslatedName").val("");
	$("#txtTranslatedDesc").val("");
}

/*
 * Removes the existing edited translations and reenters the original translation set. 
 */
resetTranslationsSet = function(){
	$("#selTranslations").children().remove();
	if(isNewCriteria){
		pupulateTranslationsFromInitialData();
	}else{
		setSelectedCriteriaTranslations();
	}
}

/*
 * Method selecting the the displayed templates per the template type selected.
 */
templateTypeChange = function(){
	$("#selTemplateID").children().remove();
	$.each(anciTemplateTypes[$("#selTemplateType").val()],function(key,value){
		$("#selTemplateID").append('<option value='+key+'>'+value+'</option>');
	});
}

/*
 * Retrieve the translation data.
 */
getTranslationsForName = function(){
	var nameTranslations={};
	var descTranslations={};
	$("#selTranslations option").each(function()
	{
		var detailArr=$(this).text().split(translationSeperator);
		
		var language=detailArr[0];
		var name=detailArr[1];
		var description=detailArr[2];
		
		nameTranslations[language]=name;
		descTranslations[language]=description;
		
	});
	var results={};
	results["nameTranslations"]=$.toJSON(nameTranslations);
	results["descriptionTranslations"]=$.toJSON(descTranslations);
	
	return results;
}

/*
 * Method validating the inputs.
 */
validateData = function(){
	var templateType = $("#selTemplateType").val();
	var templateID = $("#selTemplateID").val();
	
	var applicableFrom = $("#applicableFrom").datepicker("getDate");
	var applicableTo = $("#applicableTo").datepicker("getDate");
	
	var applicableForReturn = $("#applReturn").is(':checked');
	var applicableForOneWay = $("#applOneway").is(':checked');
	
	if(templateType == null){
		alert("Please select a Template Type.")
		return false;
	}else if(templateID == null){
		alert("Please select a Template ID")
		return false;
	}else if(applicableFrom == null || applicableTo == null){
		alert("Please select a time period for the offer.")
		return false;
	}else if(applicableFrom > applicableTo){
		alert("Applicable From date cannot be greater than Applicable To date");
		return false;
	}else if(!applicableForOneWay && !applicableForReturn){
		alert("Please select at least one option from Applicable For Oneway, Applicable For Return");
		return false;
	}else if(!translationsInitialized){
		alert("Please enter the appropriate translations");
		return false;
	}else if($("#selApplicableDaysOutBound :selected").length < 1){
		alert("Please select at lease one Applicable Day");
		return false;
	}else if(applicableForReturn && $("#selApplicableDaysInBound :selected").length < 1){
		alert("Please select at lease one Applicable Day");
		return false;
	}else if($("#selLCCs :selected").length < 1){
		alert("Please select at least one Applicable LCC");
		return false;
	}else if($("#selPaxCount option").length < 1){
		alert("Please add at least one passenger count combination");
		return false;
	}else if($("#selONDs option").length < 1){
		alert("Please add at least one OND");
		return false;
	}
	
	return true;
}

/*
 * Show message in the AirAdmin interface.
 */
showMsg = function(strType, strMsg) {
	top[2].objMsg.MessageText = strMsg;
	top[2].objMsg.MessageType = strType ;
	top[2].ShowPageMessage();
}

showProgressBar = function(){
	top[2].ShowProgress();
}

hideProgressBar = function(){
	top[2].HideProgress();
} 