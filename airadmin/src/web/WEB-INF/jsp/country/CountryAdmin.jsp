<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Template Page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
<LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css">

<script
	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script  src="../../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script
	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script
	src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
</head>
<body class="tabBGColor" scroll="no"
	onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false"
	onkeydown='return Body_onKeyDown(event)' ondrag='return false'
	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">

<form method="post" action="showCountry.action"><script
	type="text/javascript">
				var searchData = "<c:out value="${requestScope.reqCountrySearchData}" escapeXml="false" />";
				<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
				<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
				var arrFormData = new Array();
				<c:out value="${requestScope.reqFormData}" escapeXml="false" />
				var isBSPEnabledForAirLine= "<c:out value="${requestScope.reqBSPEnableForAirLine}" escapeXml="false" />";

				<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
				<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
				<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />		
				var arrCountry = new Array();
				<c:out value='${requestScope.reqCountryList}' escapeXml='false' />				
			</script>
<table width="99%" align="center" border="0" cellpadding="0"
	cellspacing="0">
	<td><%@ include file="../common/IncludeFormTop.jsp"%>
	Search Countries <%@ include file="../common/IncludeFormHD.jsp"%>
	<table width="100%" border="0" cellpadding="0" cellspacing="2"
		ID="Table2">
		<tr>
			<td width="15%"><font>Country Description</td>
			<td width="23%"><font>&nbsp;</font></td>
			<td width="12%" align="right"><input name="btnSearch"
				type="button" class="button" id="btnSearch" onClick="searchCountry()"
				value="Search"></td>
		</tr>
	</table>
	<%@ include file="../common/IncludeFormBottom.jsp"%>
	</td>
	<tr>
		<td><%@ include file="../common/IncludeFormTop.jsp"%>Countries<%@ include
			file="../common/IncludeFormHD.jsp"%>
		<table width="100%" border="0" cellpadding="0" cellspacing="4"
			ID="Table9">
			<tr>
				<td><font class="fntSmall">&nbsp;</font></td>
			</tr>
			<tr>
				<td><span id="spnCountries"></span></td>
			</tr>
			<tr>
				<td><u:hasPrivilege privilegeId="sys.mas.country.add">
					<input name="btnAdd" type="button" class="Button" id="btnAdd"
						value="Add" onclick="addClick()">
				</u:hasPrivilege> <u:hasPrivilege privilegeId="sys.mas.country.edit">
					<input name="btnEdit" type="button" class="Button" id="btnEdit"
						value="Edit" onclick="editClick()">
				</u:hasPrivilege> <u:hasPrivilege privilegeId="sys.mas.country.delete">
					<input name="btnDelete" type="button" class="Button" id="btnDelete"
						value="Delete" onclick="deleteClick()">
				</u:hasPrivilege></td>
			</tr>
		</table>
		<%@ include file="../common/IncludeFormBottom.jsp"%>
		</td>
	</tr>
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeFormTop.jsp"%>Add/Modify
		Country<%@ include file="../common/IncludeFormHD.jsp"%>
		<br>
		<table width="98%" border="0" cellpadding="0" cellspacing="2"
			align="center" ID="Table1">
			<tr>
				<td width="15%"><font>Country ID</font></td>
				<td><input name="txtCountryId" type="text" id="txtCountryId"
					style="width: 75px;" class="UCase" maxlength="2"
					onkeyUp="alphaValidate(this)" onkeyPress="alphaValidate(this)"
					onChange="setPageEdited(true)"><font class="mandatory">&nbsp*</font></td>
			</tr>
			<tr>
				<td><font>Country Description</font></td>
				<td><input name="txtCountryDes" type="text" id="txtCountryDes"
					size="50" maxlength="30" onkeyUp="alphaWhiteSpaceValidate(this)"
					onkeyPress="alphaWhiteSpaceValidate(this)"
					onChange="setPageEdited(true)"><font class="mandatory">&nbsp*</font></td>
			</tr>
			<tr>
				<td><font>Currency</font></td>
				<td><select name="selCurrencyCode" id="selCurrencyCode"
					size="1" style="width: 105;" onChange="setPageEdited(true)">
					<option value="-1"></option>
					<c:out value="${requestScope.reqCurrencyList}" escapeXml="false" />
				</select><font class="mandatory">&nbsp*</font></td>
			</tr>
			<tr>
				<td><font>Region</font></td>
				<td><select name="selRegionCode" id="selRegionCode"
					size="1" style="width: 125;" onChange="setPageEdited(true)">
					<option value="-1"></option>
					<c:out value="${requestScope.reqRegionList}" escapeXml="false" />
				</select><font class="mandatory">&nbsp*</font></td>
			</tr>
			<tr>
				<td valign="top"><font>Remarks</font></td>
				<td><textarea name="txtRemarks" id="txtRemarks" cols="47"
					rows="3" onkeyUp="validateTA(this,255);commaValidate(this);"
					onkeyPress="validateTA(this,255);commaValidate(this);"
					onChange="setPageEdited(true)"
					title="Can enter only up to 255 charactors"></textarea></td>
			</tr>
			<tr>
				<td><font>Allow On-Hold Bookings</font></td>
				<td><input type="checkbox" name="chkOhdEnabled" id="chkOhdEnabled"
					onChange="setPageEdited(true)"></td>
			</tr>
			<tr>
				<td><font>Active</font></td>
				<td><input type="checkbox" name="chkStatus" id="chkStatus"
					onChange="setPageEdited(true)"></td>
			</tr>
			<tr id = "BSPEnableRw">
				<td><font>BSP Enabled</font></td>
				<td><input type="checkbox" name="chkBSPEnabled" id="chkBSPEnabled"
					onChange="setPageEdited(true)"></td>
			</tr>
            <tr>
                <td width="15%"><font>ISO Code Alpha3</font></td>
                <td><input name="txtIsoCodeAlpha3" type="text" id="txtIsoCodeAlpha3"
                           style="width: 75px;" class="UCase" maxlength="3"
                           onkeyUp="alphaValidate(this)" onkeyPress="alphaValidate(this)"
                           onChange="setPageEdited(true)"><font class="mandatory">&nbsp*</font></td>
            </tr>
			<tr>
				<td colspan="2" style="height: 42px;" valign="bottom"><input
					name="btnClose" type="button" class="Button" id="btnClose"
					value="Close" onclick="top[1].objTMenu.tabRemove(screenId)">
				<input name="btnReset" type="button" class="Button" id="btnReset"
					value="Reset" onClick="resetCountry()"></td>
				<td align="right" valign="bottom"><input name="btnSave"
					type="button" class="Button" id="btnSave" value="Save"
					onClick="saveCountry()"></td>
			</tr>
		</table>
		<%@ include file="../common/IncludeFormBottom.jsp"%>
		</td>
	</tr>
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
</table>

<input type="hidden" name="hdnVersion" id="hdnVersion" value="" /> 
<input type="hidden" name="hdnMode" id="hdnMode" /> 
<input type="hidden" name="hdnRecNo" id="hdnRecNo">
<input type="hidden" name="hdnSearchData" id="hdnSearchData"/> 
</form>
<script
	src="../../js/country/CountryAdminGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/country/CountryValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
</body>

<script type="text/javascript">
  	<!--
  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
  	function ClearProgressbar(){  		
  		if (typeof(objDG) == "object"){
	  		if (objDG.loaded){
	  			clearTimeout(objProgressCheck);
	  			top[2].HideProgress();
	  		}
	  	}	  	
  	}
  	 
  	//-->
  </script>

</html>