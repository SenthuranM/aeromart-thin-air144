<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Marketing Carrier Summary</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
</head>
<body class="Body">
  <table align="center">
  		<tr bgcolor="#E1E1E1">
  			<th width="25%" class="GridNoBorder">Marketing Carrier Code</th>
  			<th width="18%" class="GridNoBorder">Allocated Seats</th>
  			<th width="18%" class="GridNoBorder">Sold Seats</th>
  			<th width="18%" class="GridNoBorder">OnHold Seats</th>
  			<th width="18%" class="GridNoBorder">Available Seats</th>
  		</tr>
	 <c:forEach items="${requestScope.CarrierList}" var="inventoryAllocationDTO">
        <tr class="GridNoBorder">
  			<td align="center"><c:out value="${inventoryAllocationDTO.markettingCarrierCode}"/></td>
  			<td align="center"><c:out value="${inventoryAllocationDTO.allocation}"/></td>
  			<td align="center"><c:out value="${inventoryAllocationDTO.sold_seat}"/></td>
  			<td align="center"><c:out value="${inventoryAllocationDTO.onHoldSeat}"/></td>
  			<td align="center"><c:out value="${inventoryAllocationDTO.availableSeat}"/></td>
  		</tr>		
	</c:forEach>	
  </table>
</body>
</html>