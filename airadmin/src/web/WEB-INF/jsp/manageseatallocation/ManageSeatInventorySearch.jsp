<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/jquery.ui_no_cache.css">
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> 	
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.js"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js"></script>

	
  </head><!--oncontextmenu="return false"-->
  <body oncontextmenu="return false" class="tabBGColor" scroll="no" onkeypress='return Body_onKeyPress(event)' onkeydown='return Body_onKeyDown(event)' ondrag='return false' onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">

   <form method="post"  id="formSearchSeatInventory" name="formSearchSeatInventory" action="showMISearch.action">
		<script type="text/javascript">
			<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
			<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />		
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			<c:out value="${requestScope.reqFormFieldValues}" escapeXml="false" />		
			<c:out value="${requestScope.PostBack}" escapeXml="false" />
			
			var arrCarriers = new Array();
			<c:out value="${requestScope.requsercrriercode}" escapeXml="false" />
			<c:out value="${requestScope.reqTimeIn}" escapeXml="false" />
		</script>

			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Seat Inventory - Search Flights for Seat Allocation<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
				  			<tr>
				  				<td width="15%"><font>&nbsp;</font></td>
				  				<td width="10%"><font>&nbsp;</font></td>

				  				<td width="8%"><font>&nbsp;</font></td>
				  				<td width="15%"><font>&nbsp;</font></td>

				  				<td width="8%"><font>&nbsp;</font></td>
				  				<td width="6%"><font>&nbsp;</font></td>

				  				<td width="8%"><font>&nbsp;</font></td>
				  				<td width="15%"><font>&nbsp;</font></td>				  				

				  				<td width="8%"><font>&nbsp;</font></td>
				  				<td width="20%"><font>&nbsp;</font></td>				  				
				  			</tr>
							<tr>
								<td>
									<font>Departure Date - From</font>
								</td>
								<td><input name="txtDepartureDateFrom" type="text" id="txtDepartureDateFrom" style="width:78px;" maxlength="11" tabindex="1" onBlur="dateChk('txtDepartureDateFrom')" onKeyUp="KPValidate('txtDepartureDateFrom')" onKeyPress="KPValidate('txtDepartureDateFrom')"></td>
								<td align="left"><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory">&nbsp;*</font></td>
								<td>
									<font>Departure Date - To</font>
								</td>
								<td><input name="txtDepartureDateTo" type="text" id="txtDepartureDateTo" style="width:78px;" maxlength="11"  tabindex="2"  onBlur="dateChk('txtDepartureDateTo')" onKeyUp="KPValidate('txtDepartureDateTo')" onKeyPress="KPValidate('txtDepartureDateTo')"></td>
								<td align="left"><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory">&nbsp;*</font></td>
								<td>
									<font>Flight No</font>
								</td>
								<td>
									<input name="txtFlightNumber" type="text" id="txtFlightNumber" style="width:90px;" maxlength="7"  tabindex="3" onkeyUp="alphaNumericValidate(this)" onkeyPress="alphaNumericValidate(this)" onBlur="changeCase(this)" title="Carrier code or Flight number">
								</td>

								<td>
									<font>Flight Status</font>
								</td>
								<td colspan="2">
									<select name="selFltStatus" size="1" id="selFltStatus"  tabindex="4" style="width:100px;">
										<option value="-1">All</option>
										<c:out value="${requestScope.reqFlightStatusList}" escapeXml="false" />
								  </select>
								</td>
							</tr>
							<tr>
								<td>
									<font>Departure</font>
								</td>
								<td colspan="2">
									<select name="selDeparture" size="1" id="selDeparture"  tabindex="5" title="Original departure location" style="width:100px;">
										<option value="-1"></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
								  </select>
								</td>
								<td>
									<font>Arrival</font>
								</td>
								<td colspan="2">
									<select name="selArrival" size="1" id="selArrival"  tabindex="6" title=" Final Destination" style="width:100px;">
										<option value="-1"></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
								  </select>
								</td>


								<td>
									<font>Cabin Class</font>
								</td>
								<td>
									<select name="selCabinClass" size="1" id="selCabinClass"  tabindex="7" style="width:100px;">
										<option value="-1">All</option>
										<c:out value="${requestScope.reqCabinClassList}" escapeXml="false" />
								  </select>
								</td>
							</tr>
							<tr>
								<td>
									<font>Via 1</font>
								</td>
								<td colspan="2">
									<select name="selVia1" size="1" id="selVia1"  tabindex="8" style="width:100px;">
										<option value="-1"></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
								  </select>
								</td>
								<td>
									<font>Via 2</font>
								</td>
								<td colspan="2">
									<select name="selVia2" size="1" id="selVia2"  tabindex="9" style="width:100px;">
										<option value="-1"></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
								  </select>
								</td>
								<td>
									<font>Via 3</font>
								</td>
								<td>
									<select name="selVia3" size="1" id="selVia3"  tabindex="10" style="width:100px;">
										<option value="-1"></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
								  </select>
								</td>
								<td>
									<font>Via 4</font>
								</td>
								<td>
									<select name="selVia4" size="1" id="selVia4"  tabindex="11" style="width:100px;">
										<option value="-1"></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
								  </select>
								</td>																																	
							</tr>
							<tr>
								<td>
									<font>Time in Airport</font>
								</td>
								<td colspan="2">
									<font>
									<input type="radio" id="radTime" name="radTime" value="L"  tabindex="12" class="NoBorder">
									Local
									<input type="radio" id="radTime" name="radTime" value="Z"  tabindex="13" class="NoBorder" checked="checked">
									Zulu
									</font>
								</td>	
								<td><font>Seat Factor</font></td>
								<td colspan = "4">
										<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
										<tr><td>
											<font>min</font>&nbsp;
											<input type="text" id="txtMinSeatFactor" name="txtMinSeatFactor" size="5" tabindex="14" onkeyup="seatFactorMinOnChng()" class="inputSm">
											&nbsp;<font>%</font>
										<!--  <td align="center"><div id="seatFactor" style="width:160px;height:5px"></div></td> -->
										&nbsp;<font>max</font>
										&nbsp;<input type="text" id="txtMaxSeatFactor" name="txtMaxSeatFactor" size="5" tabindex="15" onkeyup="seatFactorMaxOnChng()" class="inputSm">
										&nbsp;<font>%</font>
										</td>
										</tr>
									</table>
								</td>
							
								<td colspan="2" align="right">
									<input name="btnClear" type="button" class="Button" id="btnClear" value="Reset"  tabindex="16" onClick="clearInventory()">
									<input name="btnSearch" type="button" class="Button" id="btnSearch" value="Search"  tabindex="17" onclick="searchInventory()">
								</td>
								
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Flights<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td style="height:275;" valign="top">
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td>
												<span id="spnSearchFlights"></span>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									<input type="button" id="btnClose" class="Button" name="btnClose" value="Close"  onclick="top[1].objTMenu.tabRemove('SC_INVN_001')">
								</td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
    	<script src="../../js/manageseatallocation/MISearch.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/manageseatallocation/MIValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<input type="hidden" name="hdnVersion"/>	
 		<input type="hidden" name="hdnMode"  id="hdnMode" value="SEARCH"/>	
	 	<input type="hidden" name="hdnRecNo" id="hdnRecNo" >		
		<input type="hidden" name="hdnFlightID" id="hdnFlightID">
		<input type="hidden" name="hdnClassOfService" id="hdnClassOfService">
		<input type="hidden" name="hdnOlFlightId" id="hdnOlFlightId">
		<input type="hidden" name="hdnModelNo" id="hdnModelNo">
		<input type="hidden" name="hdnFlightSumm" id="hdnFlightSumm" style="width:500px;">		
		<input type="hidden" name="hdnSeatFactorMin" id="hdnSeatFactorMin">
		<input type="hidden" name="hdnSeatFactorMax" id="hdnSeatFactorMax">		
  </form>		
 </body>
</html>