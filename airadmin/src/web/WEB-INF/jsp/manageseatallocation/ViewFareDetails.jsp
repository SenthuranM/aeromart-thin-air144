<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Fares & Rules</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
 </head>

<body class="tabBGColor" scroll="yes" onkeypress='return Body_onKeyPress(event)' onkeydown='return Body_onKeyDown(event)' ondrag='return false'	onLoad="window_onload()">
	<form method="post" name="fareDetails"  id="fareDetails" action="">
		<script type="text/javascript">
			<c:out value="${requestScope.reqFareDetails}" escapeXml="false" />
		</script>
			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td><font class="Header">Fares &amp; Rules</font></td>
				</tr>
				<br>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Fares &amp; Rules<%@ include file="../common/IncludeFormHD.jsp"%>
							<table width="100%" border="0" cellpadding="2" cellspacing="2" ID="Table2">
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td width="10%"></td>
									<td width="40%" align="left"><font><b>Fare Id </b></font></td>
									<td width="50%" align="left"><font><span id="idFareId"></span></font></td>
								</tr>
								<tr>
									<td></td>
									<td align="left"><font><b>Fare Rule Id </b></font></td>
									<td align="left"><font><span id="idFareRuleId"></span></font></td>
								</tr>
								<tr>
									<td></td>
									<td align="left"><font><b>Effective From </b></font></td>
									<td align="left"><font><span id="idEffectiveFrom"></span></font></td>
								</tr>
								<tr>
									<td></td>
									<td align="left"><font><b>Effective To </b></font></td>
									<td align="left"><font><span id="idEffectiveTo"></span></font></td>
								</tr>
								<tr>
									<td></td>
									<td align="left"><font><b>Fare Basis Code </b></font></td>
									<td align="left"><font><span id="idFareBasisCode"></span></font></td>
								</tr>
								<tr>
									<td></td>
									<td align="left"><font><b>Fare Rule Description </b></font></td>
									<td align="left"><font><span id="idFareRuleDescription"></span></font></td>
								</tr>
								<tr>
									<td></td>
									<td align="left"><font><b>Refundable AD/CH/IN</b></font></td>
									<td align="left"><font><span id="idRefundable"></span></font></td>
								</tr>
								<tr>
									<td></td>
									<td align="left"><font><b>Return </b></font></td>
									<td align="left"><font><span id="idReturn"></span></font></td>
								</tr>
								<tr>
									<td></td>
									<td align="left"><font><b>Advance Booking Time(Days:Hours:Mins) </b></font></td>
									<td align="left"><font><span id="idAdvanceBookingDays"></span></font></td>
								</tr>

								<tr>
									<td></td>
									<td align="left"><font><b>Master FareRule Code </b></font></td>
									<td align="left"><font><span id="idAFareRuleCode"></span></font></td>
								</tr>
								<tr>
									<td></td>
									<td align="left"><font><b>Agent Codes </b></font></td>
									<td align="left"><font><span id="idAgentCodes"></span></font></td>
								</tr>
								<tr>
									<td></td>
									<td align="left"><font><b>Visible Channels </b></font></td>
									<td align="left"><font><span id="idVisibleChannels"></span></font></td>
								</tr>
								<tr>
									<td colspan="3" align="right">
										<input type="button" id="btnCancel" value="Cancel" class="Button" onclick="window.close();" name="btnCancel">
									</td>
								</tr>
							</table>
							<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
			</table>
	<script src="../../js/manageseatallocation/ViewFareDetails.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	</form>
</body>