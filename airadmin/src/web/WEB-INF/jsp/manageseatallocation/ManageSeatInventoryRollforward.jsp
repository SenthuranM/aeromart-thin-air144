<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>RollForward</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css"
	href="../../css/Style_no_cache.css">
<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script
	src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script
	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/manageseatallocation/MIRollforward.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/manageseatallocation/RollFwdLog.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
</head>

<body scroll="no" onkeydown='return Body_onKeyDown(event)'
	ondrag='return false' onkeypress='return Body_onKeyPress(event)'
	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')"
	onunload="opener.winUnLoad('<c:out value="${requestScope.rollForwardAudit}"/>')">

<%@ include file="../common/IncludeWindowTop.jsp"%>
<form method="post" name="formRollAllocation" id="formRollAllocation"
	action="saveRollAllocation.action"><!--c:out value="${requestScope.reqRollFwdStatusData}" escapeXml="false"/-->
<script type="text/javascript">
<!--
	var rollFwdStatusReportData = new Array();
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />

	<c:out value="${requestScope.reqFrequncy}" escapeXml="false" />
	<c:out value="${requestScope.reqSelectedDataJS}" escapeXml="false" />
	<c:out value="${requestScope.reqSegmentList}" escapeXml="false" />
	var strLog = "";
	strLog = "<c:out value="${requestScope.reqLog}" escapeXml="false" />";	
	var isLogicalCCEnabled = '<c:out value="${requestScope.reqIsLogicalCCEnabled}" escapeXml="false" />';
	var privOverrideFrequency = '<c:out value="${requestScope.reqOverrideFrequency}" escapeXml="false" />';
	var privAdvanceRollforward = '<c:out value="${requestScope.reqAdvanceRollforward}" escapeXml="false" />';
//-->	
</script>


<table width="100%" cellpadding="0" cellspacing="0" border="0"
	class="PageBorder" ID="Table7" style="height: 350px;">
	<tr>
		<td width="30"><img src="../../images/spacer_no_cache.gif"
			width="100%" height="1"></td>
		<td valign="top" align="left" class="PageBackGround"><!-- Your Form start here -->
		<font class="Header">Seat Allocation - Roll Forward</font> <font
			class="fntSmall">&nbsp;</font> <br><%@ include
			file="../common/IncludeMandatoryText.jsp"%> <br>
		<table width="98%" cellpadding="0" cellspacing="0" border="0"
			ID="Table7">
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>
				Seat Allocation - Roll Forward <%@ include
					file="../common/IncludeFormHD.jsp"%> <br>
				<table width="100%" border="0" cellpadding="1" cellspacing="0"
					ID="Table2">
					<c:if test="${reqAdvanceRollforward}">
						<u:hasPrivilege privilegeId="plan.invn.alloc.bc.roll.advance">
						<tr>
						<td style="white-space: nowrap;">
							<input type="radio" id="radBasicMode"
									name="radMode" value="Basic" class="NoBorder" checked>
								<font>&nbsp;Basic Mode</font></td>
						<td><input type="radio" id="radAdvanceMode"
									name="radMode" value="Advance" class="NoBorder">
								<font>&nbsp;Advance Mode</font></td>
						</tr>
						</u:hasPrivilege>
					</c:if>
					<tr>
						<td width="20%" style="vertical-align:top; padding: 12 0 0 12;"><font style="margin-top: 10px;">Flight</font></td>
						<td width="10%">
							<font class="fntBold">
							<span id="spnFlightNo"></span>
							</font>
							<span id="spnFlightNoList" style="display:none"></span>
						</td>

						<td width="10%" style="vertical-align:top; padding: 12 0 0 12;">
							<font style="margin-top: 10px;">Origin</font>
						</td>
						<td width="15%" style="vertical-align:top; padding: 12 0 0 12;">
							<font style="margin-top: 10px;" class="fntBold">
							<span id="spnOrigin"></span></font>
						</td>
						<td width="10%" style="vertical-align:top; padding: 12 0 0 12;">
							<font style="margin-top: 10px;">Destination</font>
							</td>
						<td width="15%" style="vertical-align:top; padding: 12 0 0 12;">
							<font style="margin-top: 10px;" class="fntBold">
								<span id="spnDestination"></span>
							</font>
						</td>
						<td width="10%"><font>&nbsp;</font></td>
					</tr>
					<tr class="seatAllocation">
						<td colspan="7">
						<table width="70%" border="0" cellpadding="1" cellspacing="0"
							ID="Table2" class="dateTable">
							<tr>
								<td width="15%"><font>From Date</font></td>
								<td width="25%" align="left"><input type="text" onkeypress="return isValidDate(event)"
									id="txtFromDate_1" style="width: 75px;" maxlength="10"
									name="txtFromDate_1" onBlur="dateChk('txtFromDate_1')"> <a
									href="javascript:void(0)"
									onclick="LoadCalendar(0,event); return false;"
									title="View Calendar"><img
									SRC="../../images/calendar_no_cache.gif" border="0"></a><font
									class="mandatory">&nbsp;*</font></td>
								<td width="15%"><font>To Date</font></td>
								<td width="25%" align="left"><input type="text" onkeypress="return isValidDate(event)"
									id="txtToDate_1" style="width: 75px;" maxlength="10"
									name="txtToDate_1" onBlur="dateChk('txtToDate_1')"> <a
									href="javascript:void(0)"
									onclick="LoadCalendar(1,event); return false;"
									title="View Calendar"><img
									SRC="../../images/calendar_no_cache.gif" border="0"></a><font
									class="mandatory">&nbsp;*</font></td>
								<td with="20%">
									<input type="button" id="btnSave" name="btnSave" class="Button hideAdvance" style="display:none" value="Add" onclick="addCalendar()">
								</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td><font>Segment</font></td>
						<td valign="top"><span id="spn1" class="FormBackGround"></span></td>
						<td><font class="mandatory">&nbsp;*</font></td>
					</tr>


					<tr>
						<td><font>Frequency</font></td>
						<td colspan="6"><font>M</font>&nbsp;<input type="checkbox"
							id="chkMonday" name="chkMonday" class="NoBorder"> <font>Tu</font>&nbsp;<input
							type="checkbox" id="chkTuesday" name="chkTuesday"
							class="NoBorder"> <font>W</font>&nbsp;<input
							type="checkbox" id="chkWednesday" name="chkWednesday"
							class="NoBorder"> <font>Th</font>&nbsp;<input
							type="checkbox" id="chkThursday" name="chkThursday"
							class="NoBorder"> <font>F</font>&nbsp;<input
							type="checkbox" id="chkFriday" name="chkFriday" class="NoBorder">
						<font>Sa</font>&nbsp;<input type="checkbox" id="chkSaturday"
							name="chkSaturday" class="NoBorder"> <font>Su</font>&nbsp;<input
							type="checkbox" id="chkSunday" name="chkSunday" class="NoBorder">
						<font class="mandatory">&nbsp;*</font></td>
					</tr>

					<tr>

						<td colspan="7"><input type="checkbox" id="chkOverrideLevel"
							name="chkOverrideLevel" class="NoBorder" checked
							onclick="onClickOverrideLevel(this)"> <font>Booking
						Class Inventory overriding options</font>
					</tr>
					<tr>
						<td colspan="1"></td>
						<td colspan="6"><input type="radio" id="radOverrideLevel"
							name="radOverrideLevel" value="None" class="NoBorder" checked>
						<font>&nbsp;Never Override</font><br>
						<input type="radio" id="radOverrideLevel" name="radOverrideLevel"
							value="NoResOnly" class="NoBorder"> <font>&nbsp;Override,
						if no reservations against booking class inventory</font><br>
						<input type="radio" id="radOverrideLevel" name="radOverrideLevel"
							value="All" class="NoBorder"> <font>&nbsp;Override
						All</font></td>

					</tr>
					<tr>

						<td colspan="7"><input type="checkbox"
							id="chkDeleteTargetInventories" name="chkDeleteTargetInventories"
							class="NoBorder" onclick="onClickDeleteTargetInventories(this)">
						<font>Delete <b>all</b> booking class inventories in target
						flight segments first</font><br>
						<font class="fntSmall"><b>Note:</b> Deleting inventories
						succeeds, only if there is no reservations in target flight
						segment</font></td>
					</tr>
					<tr>
						<td colspan="7" align="left">
						<table width="70%" border="0" cellpadding="0" cellspacing="0"
							ID="tblRmvAdditoinal">
							<tr>
								<td width="3%"><input type="checkbox" id="chkDelOddBCs"
								name="chkDelOddBCs" class="NoBorder"
								onclick="onClickRemoveOddBC(this)"></td>
								<td width="22%" align="left"><font>Remove additional booking classes</font></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan="7" align="left">
						<table width="90%" borderfunction winOnunload(t){
		alert(t);
	}="0" cellpadding="0" cellspacing="0"
							ID="tblCurtail">
							<tr>
								<td width="10%"><font>Curtail &nbsp;- </font></td>
								<td width="25%" align="left"><font><select
									id="radRfC" name="radRfC">
									<option value="2">Override and Update</option>
									<option value="1">If target is zero</option>
									<option value="0" selected="selected">Ignore</option>
								</select></font></td>
								<td width="10%"><font>&nbsp;&nbsp;Oversell - &nbsp;</font></td>
								<td width="25%" align="left"><font><select
									id="radRfO" name="radRfO">
									<option value="2">Override and Update</option>
									<option value="1">If target is zero</option>
									<option value="0" selected="selected">Ignore</option>
								</select></font></td>
							</tr>
							<u:hasPrivilege privilegeId="plan.invn.alloc.waitlist.roll">
							<tr>
								<td width="name="txtFromDate"10%"><font>Waitlisting - &nbsp;</font></td>
								<td width="25%" align="left" ><font><select
									id="radRfW" name="radRfW">
									<option value="2">Override and Update</option>
									<option value="1">If target is zero</option>
									<option value="0" selected="selected">Ignore</option>
								</select></font>
								</td>
								<td></td>
								<td></td>
							</tr>
							</u:hasPrivilege>						
						</table>
						</td>
					</tr>
					<tr>
						<td colspan="7" align="left">
						<table width="70%" border="0" cellpadding="0" cellspacing="0"
							ID="tblOverSell">
							<tr>
								<td width="5%"><input type="checkbox" id="chkCopyPriority"
									name="chkCopyPriority" class="NoBorder"></td>
								<td width="20%" align="left"><font>Copy Priority
								Flag</font></td>
								<td width="5%" align="left"><input type="checkbox"
									id="chkBCStatus" name="chkBCStatus" class="NoBorder"></td>
								<td width="20%" align="left"><font>Copy Status Flag</font></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan="7" align="left">
						<table width="60%" border="0" cellpadding="0" cellspacing="0"
							ID="tblOverSell">
							<tr>
								<td width="25%"><font>Seat Factor Min %</font></td>
								<td width="2%" align="right"><input type="text"
									id="seatFactorMin" name="seatFactorMin" maxlength="3" size="3"
									onKeyPress="VaidateSeatFactor()" onKeyUp="VaidateSeatFactor()"></td>
								<td width="6%"></td>
								<td width="25%%"><font>Seat Factor Max %</font></td>
								<td width="2%" align="right"><input type="text"
									id="seatFactorMax" name="seatFactorMax" maxlength="3" size="3"
									onKeyPress="VaidateSeatFactor()" onKeyUp="VaidateSeatFactor()"></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr id="selRollForwardOptions">
						<td colspan="7" align="left">
						<table width="77.5%" border="0" cellpadding="0" cellspacing="0"
							ID="tblCpySegAlocs">
							<tr>
								<td width="25%"><font>Copy segment allocations - </font></td>
								<td width="40%"><font><select
									id="selRollFwdSegAllocs" name="selRollFwdSegAllocs">
									<option value="0" selected="selected">Booking Class Rollforward</option>
									<option value="1">Logical class rollforward without overriding</option>
									<option value="2">Logical class rollforward with overriding</option>
								</select></font></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan="3"><input type="button" id="btnClose"
							class="Button" value="Close" onclick="javascript:window.close();">
						</td>
						<td colspan="2" align="left"><input type="button" id="btnLog"
							class="Button" value="View Log" onclick="viewLog();"></td>
						<td colspan="3" align="right"><input type="button"
							id="btnSave" name="btnSave" class="Button" value="Save"
							onclick="saveRollForward()"></td>
					</tr>
					<!-- Stable -->
				</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>
		</table>
		<br>
		</td>
		<td width="30"><img src="../../images/spacer_no_cache.gif"
			width="100%" height="1"></td>
	</tr></table>

	<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
</table>
<input type="hidden" name="hdnVersion" id="hdnVersion" /> <input
	type="hidden" name="hdnMode" id="hdnMode" /> <input type="hidden"
	name="hdnFlightIDRF" id="hdnFlightIDRF" /> <input type="hidden"
	name="hdnFlightNumber" id="hdnFlightNumber" /> <input type="hidden"
	name="hdnCabinClassCode" id="hdnCabinClassCode" /> <input type="hidden"
	name="hdnOrigin" id="hdnOrigin" /> <input type="hidden"
	name="hdnDestination" id="hdnDestination" /> <input type="hidden"
	name="hdnSelectedSegsAndBCs" id="hdnSelectedSegsAndBCs" /> <input
	type="hidden" name="hdnNotSelectedSegsAndBCs"
	id="hdnNotSelectedSegsAndBCs" /> <input type="hidden"
	name="hdnFCCInventoryInfo" id="hdnFCCInventoryInfo"
	value='<c:out value="${requestScope.reqFCCInventoryInfo}" escapeXml="false" />' />
	<input type="hidden" name="hdnSelectedFlights" id="hdnSelectedFlights" /> 
	<input type="hidden" name="hdnNotSelectedFlights" id="hdnNotSelectedFlights" /> 

</form>
</body>
<script type="text/javascript">
	<c:out value="${requestScope.reqFlightNoList}" escapeXml="false" />

	var isAdvance = "<c:out value="${requestScope.advanceFlag}" escapeXml="false" />";
	var batchId = "<c:out value="${requestScope.savedBatchId}" escapeXml="false" />";
<!--
	var objCal1 = new Calendar("spnCalendarDG1");
	objCal1.onClick = "setDate";
	objCal1.buildCalendar();
	
//-->

</script>
<script src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
</html>