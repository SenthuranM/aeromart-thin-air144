<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
	<head>
		<title>User assign stations</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="-1">
			   
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>
<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>
<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/jquery.ui.autocomplete.css"/>


<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery-1.4.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery-ui-1.8.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	
	<style>
	.innerPanel{
		width:98%;
		margin:0 auto;
	}
	.portSel{
		width:50px
	}
	select{
		margin:1px 1px;
	}
	.iconBtn{
		width:25px
	}
	#ui-datepicker-div{
		display:none;
	}
	.ui-autocomplete { height: 200px; overflow-y: scroll; overflow-x: hidden;}
	</style>
	
	</head>
  <body class="tabBGColor" scroll="no"  ondrag='return false'>
  <br/>
  			<div id="divUserStation" style="text-align: left;background-color: #ECECEC;" >
				<div class="innerPanel divsearch">
					<table width="80%" border="0" cellpadding="0" cellspacing="0" >
							<tr>
								<td valign="top" width="25%"><font>User ID</font><font class="mandatory">&nbsp;*</font></td>
									
												<td width="75%">
												<select id="selUser" class="portSel" style="width:80%">
												<option value=""></option>																					
												<c:out value="${requestScope.reqUserIdList}" escapeXml="false" />
												</select>
												</td>
							</tr>
							<tr>
							<td valign="top" width="25%"><font>Assigned stations :</font></td>
								<td >
												 <span id="spnStations" visible="false"/>
								</td>
							 
							</tr>
							<tr>
									<td valign="top" width="25%"><font>Stations</font><font class="mandatory">&nbsp;*</font></td>
											
										<td width="25%" valign="top"><select size="10" id="selStation" style="height:75px;width:80%" multiple="multiple">
										
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
										</select></td>
										</tr>
										<tr>
										<td valign="top" width="25%"><input name="btnSave" type="button" id="btnSave" value="Save" class="Button"></td>
										<td>
										</td>
										</tr>
						</table>
						<br/>
					</div>
				</div>
				<script type="text/javascript" src="../../js/GroupBooking/userAssignStations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> 
  					<script>
	
		
		var screenId = 'SC_SHDS_0011';
					
		
		top[2].HideProgress();
		
	</script>
	
	</body>
	
</html>