<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
	<head>
		<title>Group Request</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="-1">
			   
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>
<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>
<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/jquery.ui.autocomplete.css"/>


<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/moment/moment-with-langs.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery-1.4.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery-ui-1.8.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	
	<style>
	.innerPanel{
		width:98%;
		margin:0 auto;
	}
	.portSel{
		width:50px
	}
	select{
		margin:1px 1px;
	}
	.iconBtn{
		width:25px
	}
	#ui-datepicker-div{
		display:none;
	}
	.ui-autocomplete { height: 200px; overflow-y: scroll; overflow-x: hidden;}
	</style>
	
	</head>
   
  
    <body class="tabBGColor" scroll="no"  ondrag='return false'>
     <input type="hidden" name="requestID" id="requestID" value="-1">
    	<div id="divGrpBSearch" style="text-align: left;background-color: #ECECEC;" >
		<div class="innerPanel divsearch" style="border-bottom:1px solid #bbb">
		<table width="96%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
			<tr>
				<td><font>Request ID</font>
					 <input type="text" name="requestID" id="txtReqId" value="" style="width: 80px;" maxlength="10" >
				</td>
				<td><font>From</font>
					<select name="selFrom" id="selFrom" style="width: 80px;">
						<option value="">All</option>
					</select>
				</td>
				<td><font>To</font>
					<select name="selTo"  id="selTo" style="width: 80px;">
						<option value="">All</option>
					</select>
				</td>
				<td>
					<font>Agent</font>
					<select name="travelAgent" id="travelAgent" style="width: 120px;">
						<option value="">All</option>
					</select>
				</td>
				<td>
					<font>Station</font>
					<select name="station" id="station" style="width: 80px;">
						<option value="">All</option>
					</select>
				</td>
				<td>
					<input type="checkbox" id="chkMainReqestOnly"><label>Main Request Only</label>
				</td>
				
				<td>
				<input type="checkbox" id="chkShared"> <label>Shared Stations</label>
				</td>
				<td>
					<input name="btnSearch" type="button" id="btnSearch" value="search"  class="Button">
				</td>
			</tr>
		
		</table>
		</div>
		<div class="innerPanel divResults" style="border-top:1px solid #fff">
			<div id="jqGridGrpBContainer" style="margin-top: 5px;overflow:auto;">
				<div id="groubBookingTabs">
					<ul>
						<li><a href="#groupBookingPA">Pending Approval</a></li>
						<li><a href="#groupBookingReq">Requote</a></li>
						<li><a href="#groupBookingRej">Rejected</a></li>
						<li><a href="#groupBookingAppr">Approved</a></li>
						<li><a href="#groupBookingRel">Release Seats</a></li>
						<li><a href="#groupBookingOth">Others</a></li>
					</ul>
					<div id="groupBookingPA">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="jqGridGrpPAData" >
						</table>
						<div id="jqGridGrpPAPages"></div>
					</div>
					<div id="groupBookingReq">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="jqGridGrpReqData" >
						</table>
						<div id="jqGridGrpReqPages"></div>
					</div>
					<div id="groupBookingRej">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="jqGridGrpRejData" >
						</table>
						<div id="jqGridGrpRejPages"></div>
					</div>
					<div id="groupBookingAppr">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="jqGridGrpApprData" >
						</table>
						<div id="jqGridGrpApprPages"></div>
					</div>
					<div id="groupBookingRel">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="jqGridGrpRelData" >
						</table>
						<div id="jqGridGrpRelPages"></div>
					</div>
					<div id="groupBookingOth">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="jqGridGrpOthData" >
						</table>
						<div id="jqGridGrpOthPages"></div>
					</div>
				</div>
			</div>
	   		<div style="padding:3px 6px">
	   			<input name="btnEdit" type="button" id="btnEdit" value="View" class="Button" style="width:auto">
	   		</div>
	</div>
	</div>
 
    
   <div id="grpBookingDetails" style="text-align: left; background-color: #ECECEC;">
		<form method="post" action="manageGroupBookingRequest!save.action" id="frmGrpB">
			<table width="100%" border="0" cellpadding="2" cellspacing="2">

				<tr>
					<td><font>Request Made by</font></td>
					<td colspan="4"><span id="spanAgentDetails"></span></td>
				</tr>

				<tr>
					<td colspan="5">
						<div id="subRequestContainer">
							<table id="subRequestGrid"></table>
						</div>
					</td>
				</tr>

				<tr>
					<td><font>Approver remarks</font><font class="mandatory">&nbsp;*</font>
					</td>
					<td colspan="4"><textarea name="adminRemarks" id="adminRemarks" cols="40"
							rows="2" class="leftAlign" maxlength="99" readonly="readonly">
	  </textarea></td>
				</tr>
				<tr>
					<td><font>Minimum Payment Required</font><font
						class="mandatory">&nbsp;*</font></td>
					<td width="3%"><input id="rdo_Val" type="radio"
						name="minPayReq" checked="checked"></td>
					<td><font>Value </font>
						<div id="val">
							<input type="text" id="minPayReq_Val" name="minPayReq_Val"
								style="width: 60px" class="rightAlign" /> <span
								id="spnCurrencyMR" style="font-weight: bold;" />
						</div></td>
					<td><input id="rdo_Pct" type="radio" name="minPayReq">
					</td>
					<td><font>Precentage</font>
						<div id="pct">
							<input type="text" id="minPayReq_Pct" name="minPayReq_Pct"
								style="width: 60px" class="rightAlign" /> %
						</div></td>
				</tr>
				<tr>
					<td><font>Target Payment Date</font><font class="mandatory">&nbsp;*</font>
					</td>
					<td colspan="4"><input type="text" id="tagetPayDate" style="width: 20%"
						class="leftAlign" readonly="readonly" /></td>
				</tr>
				<tr>
					<td><font>Fare valid till</font><font class="mandatory">&nbsp;*</font>
					</td>
					<td colspan="4"><input type="text" id="fareValidDate" style="width: 20%"
						class="leftAlign" readonly="readonly" /></td>
				</tr>
				<tr>
					<td colspan="2"><span id="spnPayment" style="font-weight: bold;" /></td>
					<td colspan="2"><span id="spnPNR" style="font-weight: bold;" /></td>
					<td></td>
				</tr>
				<tr>
					<td><input name="btnApprove" type="button" id="btnApprove"
						value="Approve" class="Button"> <input name="btnReject"
						type="button" id="btnReject" value="Reject" class="Button">
						<input name="btnRequote" type="button" id="btnRequote"
						value="Re-Quote" class="Button"> <input
						name="btnReleasedSeat" type="button" id="btnReleasedSeat"
						value="Seat Released" class="Button"> <input
						name="btnShare" type="button" id="btnShare" value="Share"
						class="Button" style="width: auto" /> <input name="btnHistory"
						type="button" id="btnHistory" value="History" class="Button"
						style="width: auto" /></td>
				</tr>

			</table>

		</form>
    </div>
    	
     <div id="jqGridGrpBHistoryContainer" >
				<table width="100%" border="0" cellpadding="0" cellspacing="0" id="jqGridGrpBHistoryData">
				</table>
				<div id="jqGridGrpBHistoryData"></div>
			</div>	
	   	
    <div>
    
   
     <table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
				<tr>
					<td colspan="3" style="height: 42px;" valign="bottom"><input name="btnClose" class="Button" type="button" id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault">
					</td>
					
				</tr>
			</table>
    </div>
    <script type="text/javascript" src="../../js/GroupBooking/groupBooking.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> 
  <script>
	
		
		var screenId = 'SC_SHDS_0010';
					
		
		
		
	</script>
	
    </body>
   
</html>