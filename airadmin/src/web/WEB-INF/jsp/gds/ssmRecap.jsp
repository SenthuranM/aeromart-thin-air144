<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
     
    <LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css">
     
    <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery.ui_no_cache.css" />
	<link rel="stylesheet" media="all" type="text/css" href="../../css/timepicker/jquery-ui-timepicker-addon.css" /> 
    
    <script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
 	<script type="text/javascript" src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
    <script type="text/javascript" src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
    <script type="text/javascript" src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>

	
	<script type="text/javascript" src="../../js/v2/common/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="../../js/v2/common/jquery-ui-sliderAccess.js"></script>
	<script type="text/javascript" src="../../js/gds/ssmRecap.js"></script>
	
     
   
      
      
<title>SSM Recap</title> 
<%@ include file='../v2/common/pageHD.jsp'%>
</head>

<body class="tabBGColor">

        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" >
  			<tr>
  				<td>
  					<%@ include file="../v2/common/includeWindowAdminPanelTop.jsp"%>Schedule SSM Recap<%@ include file="../v2/common/includeAdminPanelHeader.jsp"%>

				<form method="post" action = "sendSSMRecap.action">
					<table width="100%" border="0" cellpadding="0" cellspacing="0" id="divScheduleSSMRecap">						
						<tr>
						     <td><font>GDS Code</font></td>
						     <td><font><c:out value="${requestScope.gdsCode}" escapeXml="false" /></font></td>
						</tr>
						
						<tr style="margin: 4px"> 
						    <td><font>Schedule Date</font></td>
							<td>
							   <input type="text" value="" id="scheduledDate" name="scheduledDate"	 style="margin: 4px"/>
							</td>
						</tr>
												
						<tr> 
						    <td><font>Schedule Time</font></td>
							<td>
							   <input type="text" value="" id="scheduledTime" name="scheduledTime" style="margin: 4px" onfocus="removeNow()" />
							</td>
						</tr>						
						
						<tr> 
						<td>
						<input type="radio" name="selectedScheduleRange" id="allSchedulesRadio" value="all" selected="true" style="margin: 4px" onclick="onClickAllSchedules()"> <font>All Schedules</font>
						</td>											
						</tr>
																		
						<tr> 						
                        <td>
                        <input type="radio" name="selectedScheduleRange" id="selectedScheduleRadio" value="selected" style="margin: 4px" onclick="onClickSelectedSchedules()"> <font>Select Range</font>		
                        </td>
                        </tr>  
                                        						
                        <tr> 
                        <td><font>From Date:</font></td>
							<td title="From Date Here"><input type="text" id="scheduleStartDate" name="scheduleStartDate" style="margin: 4px" ></td>							
                        <td><font>To Date:</font></td>
							<td title="To Date Here"><input type="text" id="scheduleEndDate" name="scheduleEndDate" style="margin: 4px" ></td>							                  										
						</tr>
						
						<tr>
						<td><font>Email</font></td> 						
                        <td>
                        <input type="text" name="emailAddress" id="emailAddress" style="margin: 4px">	
                        </td>
                        </tr> 						
											
						<tr> 
						<td>						
						<input type="button" value ="Schedule" id = "btnScheduleRecap" class="Button" onclick="onClickBtnSchedule()"> 
						</td>						
						</tr>
					</table>
					<input type="hidden" name="hdnMode" id="hdnMode"/>
					<input type="hidden" name="gdsID" id="gdsID" value="<c:out value="${requestScope.gdsID}" escapeXml="false" />"/>
					<input type="hidden" name="gdsCode" id="gdsCode" value="<c:out value="${requestScope.gdsCode}" escapeXml="false" />"/>
				</form>
				
			 <%@ include file="../v2/common/includeAdminPanelBottom.jsp"%>
  				</td>
  			</tr>
  		</table>
  		
      <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" >
  			<tr>
  				<td>
  					<%@ include file="../v2/common/includeWindowAdminPanelTop.jsp"%>SSM Recap History<%@ include file="../v2/common/includeAdminPanelHeader.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="divSSMRecapHistory">

							<tr>
								<td colspan="3">
									<table id="spnFC"></table>
									<div id="spnFCPager"></div>
									<font>&nbsp;** ssm recap history</font>
								</td>											
							</tr>	
															
						</table>
					<%@ include file="../v2/common/includeAdminPanelBottom.jsp"%>
  				</td>
  			</tr>
  		</table>
</body>
</html>