<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
     
    <LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css">
     
    <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery.ui_no_cache.css" />
	<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>
	<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/jquery.ui.autocomplete.css"/>
    
    <script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
 	<script type="text/javascript" src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
    <script type="text/javascript" src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
    <script type="text/javascript" src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
	<script type="text/javascript" src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
	
	<script src="../../js/gds/PublishRoutes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/common/jquery-ui-sliderAccess.js"></script>
	<style>
	.portSel{
		width:50px
	}
	
	.iconBtn{
	border:1px solid #B2B2B2; font-family:tahoma;
	font-size:11px;
	text-align:center;
	text-transform: capitalize;
	font-weight: bold;
	color: black;
	background-image: url('../../css/../images/AA111_no_cache.jpg');
	height: 20px;
	width:40px;
	cursor:pointer;
	}
	</style>
	     
<title>Publish Onds for GDS</title> 
<%@ include file='../v2/common/pageHD.jsp'%>
</head>

<body class="tabBGColor">
	<form action="showPublishRoutes.action"
		id="gdsRouteWiseAutoPublishForm" method="post"
		name="gdsRouteWiseAutoPublishForm">
		<table align="center" border="0" cellpadding="0" cellspacing="0"
			width="100%" height="100%">

			<tbody>
				<tr>
					<td><%@ include
							file="../v2/common/includeWindowAdminPanelTop.jsp"%>GDS
						Auto Publish Routes<%@ include
							file="../v2/common/includeAdminPanelHeader.jsp"%></td>
				</tr>
				<tr>
						     <td width="25%"><font>GDS Code:&nbsp;<c:out value="${requestScope.gdsCode}" escapeXml="false" /></font></td>
	
				</tr>
				<tr>
					<td valign="top" width="100%"><br> <input
						id="routeWiseAutoPublish" name="option_routeWiseAutoPublish"
						type="checkbox" value="true"> &nbsp;Enable route wise auto
						publish</td>
				</tr>
				<tr>
					<td valign="top" width="100%"><br> OND<span
						class="mandatory">&nbsp;*</span></td>
				</tr>
				<tr>
				<td width="14%">&nbsp;</td>
				</tr>
				<tr>
					<td valign="top" width="100%">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td width="25%">From<br>
									</td>
									<td width="25%">To<br>
									</td>
								</tr>
								<tr>
									<td width="25%"><select class="portSel" id="selFrom">
									</select><span class="mandatory">&nbsp;*</span></td>
									<td width="25%"><select class="portSel" id="selTo">
									</select><span class="mandatory">&nbsp;*</span></td>
								</tr>
								<tr>
									<td width="14%">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="2"><select id="sel_Onds" multiple="multiple"
										size="10" style="height: 75px; width: 100%;">
											<option value="">All</option>
									</select></td>
									<td>
										<table width="100%">
											<tr>
												<td colspan="2"><input class="iconBtn addItem"
													id="add_Onds" type="button" value=" + "></td>
											</tr>
											<tr>
												<td colspan="2"><input class="iconBtn removeItem"
													id="del_Onds" type="button" value=" - "></td>
											</tr>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
					<td width="14%">&nbsp;</td>
				</tr>
			<!-- 
				<tr>
					<td valign="top" width="100%"><br> Operating Carrier<span
						class="mandatory">&nbsp;*</span></td>
				</tr>
				<tr>
				<td width="14%">&nbsp;</td>
				</tr>
				<tr>
					<td valign="top">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td colspan="2"><select class="portSel" id="carrierCodes"
									multiple="multiple" name="carrierCodes" size="10"
									style="height: 100px; width: 100%;">
								</select></td>
								<td width="8%">
									<table width="100%">
										<tr>
											<td width="10%"><input class="Button" id="add_Carrier"
												type="button" value="&gt;&gt;"></td>
										</tr>
										<tr>
											<td width="10%"><input class="Button" id="del_Carrier"
												type="button" value="&lt;&lt;"></td>
										</tr>
									</table>
								</td>
								<td colspan="2"><select id="sel_Carrier"
									multiple="multiple" name="sel_Carrier" size="10"
									style="height: 100px; width: 100%;">
								</select></td>
								<td width="2%">&nbsp;</td>
							</tr>
						</table>
					</td> -->
					<td width="14%">&nbsp;</td>
				</tr>
				<tr>
				<td width="14%">&nbsp;</td>
				</tr>
				<tr>
					<td valign="top">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tbody>
								<tr>
									<td align="left"><input type="button"
										id="btnClosePublishRoutes" class="Button" value="Close">
										<input type="button" class="Button" id="btnResetPublishRoutes"
										value="Reset"></td>
									<td align="right">
									<u:hasPrivilege privilegeId="sys.mas.gds.autopublish.edit">
									<input name="btnSavePublishRoutes"
										type="button" class="Button" id="btnSavePublishRoutes" value="Save"></u:hasPrivilege>
									</td>
								</tr>
							</tbody>
							<%@ include file="../v2/common/includeAdminPanelBottom.jsp"%>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</body>

<script type="text/javascript">
$(document).ready(function() {
	var reset = 'false';
	loadData(reset);
	
});


$("#btnEditPublishRoutes").click(function() {
	onClickEditButton();
	
});

$(".addItem").click(function(e){
	addItemsToSelect(e.target.id);
});

$(".removeItem").click(function(e){
	removeItemsFromSelect(e.target.id);
});

$("#add_Carrier").click(function(){
	addToSelectPane('carrierCodes', 'sel_Carrier')
	});
$("del_Carrier").click(function(){
	removeFromSelectPane('sel_Carrier', 'carrierCodes')
	});

$("#btnSavePublishRoutes").click(function() {
	updateOrSaveData();
	
});

$("#btnClosePublishRoutes").click(function() {
	window.close();
	
});

$("#btnResetPublishRoutes").click(function() {
	var reset = 'true';
	resetWindow(reset);
	
});

</script>
</html>