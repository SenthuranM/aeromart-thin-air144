 <%-- 
	 @Author 	: 	Haider 
	 @Date		:	13Oct08
	 @Copyright : 	ISA
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>GDS Admin</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">	
	
	<script type="text/javascript">
	var selectedGdsIDForPublish;
	</script>
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>		
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

	<script src="../../js/gds/GDSValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
  </head>

  <body class="tabBGColor" scroll="no"  oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown='return Body_onKeyDown(event)' ondrag='return false' 
  	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">

  <form method="post" action="showGDS.action">
  			<script type="text/javascript">
				<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
				<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />		
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
				var arrFormData = new Array();
				<c:out value="${requestScope.reqFormData}" escapeXml="false" />
				
				<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
				<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
				<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />						
			</script>

			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>		
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>GDS<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									  <span id="spnGDS"></span> 
								</td>
							</tr>
							<tr>
								<td>
									<u:hasPrivilege privilegeId="sys.mas.gds.add">
										<input type="button" id="btnAdd" class="Button" value="Add" onClick="addClick()">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.mas.gds.edit">
										<input type="button" id="btnEdit" class="Button" value="Edit" onClick="editClick()">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.mas.gds.edit">
										<input type="button" id="btnSSMRecap" class="Button" value="SSM Recap" onClick="ssmRecapClick()">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.mas.gds.autopublish.view">
										<input type="button" id="btnPublishRoutes" class="Button" value="Publish Routes" onClick="publishRoutesOnClick()">
									</u:hasPrivilege>
								</td>
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						Add/Modify GDS 
						<%@ include file="../common/IncludeFormHD.jsp"%>			
							<br>
                        <table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
                            <tr>
                                <td width="15%"><font>GDS Code</font></td>
                                <td width="40%"><input name="txtGDSCode" type="text" id="txtGDSCode" maxlength="12"
                                                       size="12" class="UCase" onChange="clickChange()"> <font
                                        class="mandatory">&nbsp;*</font></td>
                                <td width="20%"><font>Type A Version</font></td>
                                <td><input name="typeAVersion" type="text" id="typeAVersion" maxlength="10" size="10"
                                           onChange="clickChange()"> <font class="mandatory">&nbsp;*</font></td>
                            </tr>
                            <tr>
                                <td><font>GDS Description</font></td>
                                <td><input name="txtDesc" type="text" id="txtDesc" maxlength="30" size="50"
                                           onkeyUp="valDescription(this)" onkeyPress="valDescription(this)"
                                           onChange="clickChange()"> <font class="mandatory">&nbsp*</font></td>
                                <td width="20%"><font>Type A Sender ID</font></td>
                                <td><input name="typeASenderId" type="text" id="typeASenderId" maxlength="10" size="10"
                                           onChange="clickChange()"> <font class="mandatory">&nbsp;*</font></td>
                            </tr>
                            <tr>
                                <td valign="top"><font>Comments</font></td>
                                <td><input name="txtRemarks" type="text" id="txtRemarks" cols="47" size="50" rows="4"
                                           maxlength="255" onkeyUp="validateTA(this,255);commaValidate(this);"
                                           onkeyPress="validateTA(this,255);commaValidate(this);"
                                           onChange="clickChange()"></td>
                                <td width="20%"><font>Type A Syntax ID</font></td>
                                <td><input name="typeASyntaxId" type="text" id="typeASyntaxId" maxlength="10" size="10"
                                           onChange="clickChange()"> <font class="mandatory">&nbsp;*</font></td>
                            </tr>
                            <tr>
                                <td><font>Publish Mechanism</font></td>
                                <td>
                                    <select name="selPublishMechanism" size="1" id="selPublishMechanism"
                                            style="width:100px;">
                                        <option value="-1"></option>
                                        <c:out value="${requestScope.reqPublishMechList}" escapeXml="false"/>
                                    </select>
                                    <font class="mandatory">&nbsp*</font></td>

                                <td width="20%"><font>Type A Syntax Version Number</font></td>
                                <td><input name="typeASyntaxVersionNumber" type="text" id="typeASyntaxVersionNumber"
                                           maxlength="10" size="10" onChange="clickChange()"> <font class="mandatory">
                                    &nbsp;*</font></td>
                            </tr>
                            <tr>
                                <td><font>Active</font></td>
                                <td><input type="checkbox" name="chkStatus" id="chkStatus" onChange="clickChange()">
                                </td>

                                <td width="20%" style="display: none;"><font>Type A Controlling Agency</font></td>
                                <td style="display: none;"><input name="typeAControllingAgency" type="text" id="typeAControllingAgency" maxlength="10" size="10" onChange="clickChange()"> <font class="mandatory">&nbsp;*</font> </td>

                                <td valign="top"><font>Enable NAVS</font></td>
                                <td valign="top"><input type="checkbox" name="chkEnableNAVS" id="chkEnableNAVS"
                                                        onChange="clickChange()"></td>
                            </tr>
                            <tr>
                                <td width="20%"><font>AVS Avalible Threshold</font></td>
                                <td><input name="txtAVSAvailThreshold" type="text" id="txtAVSAvailThreshold"
                                           maxlength="2" size="2" onkeyUp="positiveWholeNumberValidate(this)"
                                           onkeyPress="positiveWholeNumberValidate(this)" onChange="clickChange()"></td>

                                <td><font>Air Line Reservation Modifiable</font></td>
                                <td><input type="checkbox" name="airlineResModifiable" id="airlineResModifiable"
                                           onChange="clickChange()"> <font class="mandatory">&nbsp;*</font></td>
                            </tr>
                            <tr>
                                <td width="20%"><font>AVS Closuer Threshold</font></td>
                                <td><input name="txtAVSClosureThreshold" type="text" id="txtAVSClosureThreshold"
                                           maxlength="2" size="2" onkeyUp="positiveWholeNumberValidate(this)"
                                           onkeyPress="positiveWholeNumberValidate(this)" onChange="clickChange()"></td>
                            </tr>
                            <tr>
                                <td style="height:42px;" colspan="4" valign="bottom">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <input type="button" id="btnClose" class="Button" value="Close"
                                                       onclick="top[1].objTMenu.tabRemove(screenId)">
                                                <input name="btnReset" type="button" class="Button" id="btnReset"
                                                       onClick="resetGDS()" value="Reset">
                                            </td>
                                            <td align="right">
                                                <input name="btnSave" type="button" class="Button" id="btnSave"
                                                       onClick="saveGDS()" value="Save">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
							  <%@ include file="../common/IncludeFormBottom.jsp"%>
						 </td>
					</tr>
				  <tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
			

		<input type="hidden" name="hdnVersion"  id="hdnVersion" value=""/>	
		<input type="hidden" name="hdnMode" id="hdnMode"/>
		<input type="hidden" name="hdnGDSId" id="hdnGDSId"/>
		<input type="hidden" name="hdnAction" id="hdnAction"/>			
		<input type="hidden" name="hdnRecNo" id="hdnRecNo">		
		<script>
		//	fillCombos();
		</script>
	</form>	
	<script src="../../js/gds/GDSAdminGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
 	<script src="../../js/gds/PublishRoutes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </body>

    <script type="text/javascript">
  	<!--
  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
  	function ClearProgressbar(){  		
  		if (typeof(objDG) == "object"){
	  		if (objDG.loaded){
	  			clearTimeout(objProgressCheck);
	  			top[2].HideProgress();
	  		}
	  	}	  	
  	}
  	 
  	//-->
  </script>
  
</html>