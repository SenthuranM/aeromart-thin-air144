
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Re-Protect Flights</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </head>
  <body oncontextmenu="return false" ondrag="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" scroll="yes"
  		onLoad="reprotectOnLoad()">
   <form id="frmFlightReprotect" name="frmFlightReprotect">

  <script type="text/javascript">
  </script>

   <%@ include file="../common/IncludeWindowTop.jsp"%><!-- Page Background Top page -->
   <table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td><font class="Header">Re-Protect Flights</font></td>
		</tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td>
				<%@ include file="../common/IncludeMandatoryText.jsp"%>
			</td>
		</tr>				
		<tr>
			<td>
				<%@ include file="../common/IncludeFormTop.jsp"%>Flight Details<%@ include file="../common/IncludeFormHD.jsp"%>
		  	<table width="100%" border="0" cellpadding="0" cellspacing="0">
  				<tr>
					<td>
						<font class="fntBold">Schedule ID</font>
						<font class="fntBold"><span id="spnSchd"></span><font>
					</td>
					<td>
						<font class="fntBold">Start Date</font>
						<font><span id="spnStartD"></span><font>
					</td>
					<td>
						<font class="fntBold">Stop Date</font>
						<font><span id="spnEndD"></span><font>
					</td>
				<tr>
				<tr>
					<td>
						<font class="fntBold">Flight Number</font>
						<font><span id="spnFlightNo"></span><font>
					</td>
					<td>
						<font class="fntBold">Destination</font>
						<font><span id="spnDest"></span><font>
					</td>
					<td>
						<font class="fntBold">Origin</font>
						<font><span id="spnOrg"></span><font>
					</td>
					<td>
						<font class="fntBold">Frequency</font>
						<font><span id="spnFreq"></span><font>
					</td>
				<tr>
		  	</table>
			<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td>
			<%@ include file="../common/IncludeFormTop.jsp"%> <%@ include file="../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">
					<tr>
						<td height="200px">
							<span id="spnReprotect"></span>
						</td>
					</tr>
					<tr>
						<td align="right">
							<input name="btnReturn" type="button" class="Button" id="btnReturn"  title="Return to Schedules" value="Return" onclick="returnClick()">
						</td>
					</tr>
				</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
	</table>
	<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
<input type="hidden" name="hdnMode" id="hdnMode" value="">
  </form>
  	<script src="../../js/flightschedule/validateFlightReprotect.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </body>
</html>