<%@ page language="java"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Edit Segments</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
	<script type="text/javascript" src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script> 	
  </head>
  <body oncontextmenu="return false" ondrag="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" scroll="no"  onLoad="onLoad()">
  <%@ include file="../common/IncludeWindowTop.jsp"%><!-- Page Background Top page -->
  	    <script type="text/javascript">
			<c:out value="${requestScope.reqSegmentValidationData}" escapeXml="false" />	
			<c:out value="${requestScope.reqOverLapSchedDataArray}" escapeXml="false" />
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />	
		</script>	
				<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBorder" ID="Table7">
					
					<tr>
						<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="8"></td>
						<td valign="top" align="center" class="PageBackGround">
						<!-- Your Form start here -->
							<form method="post" action="showSchedule.action">
							<br>
							<table width="98%" cellpadding="0" cellspacing="0" border="0" ID="Table7">
								<tr>
									<td><font class="Header">Segments Validation for the Schedules</font></td>
								</tr>
								<tr>
									<td>
									<%@ include file="../common/IncludeFormTop.jsp"%>
									Edit Segments<%@ include file="../common/IncludeFormHD.jsp"%>
										<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td width="48%"><font class="fntBold">Select Segments for the Schedule  </font><span id="spnFirstSched"></span></td>
												<td width="2%"><font>&nbsp;</font></td>
												<td width="48%">
													<div id="divOLap1">
														<font class="fntBold" title="Schedule Overlapping ">Select Overlap Schedule</font>
														<select id="selOLSch" size="1" name="selOLSch"  title="'Flight Schedule ID" onchange="spnGridOLapSeg_onChange();">
															<option value="">&nbsp;</option>
															<c:out value="${requestScope.reqOverLapSchedData}" escapeXml="false" />	
														</select>
													</div>
												</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td><font>&nbsp;</font></td>
												<td>
													<div id="divOLap2">
														<font>Overlapping with Flight Schedule </font><span id="spnOlapId"></span>
													</div>
												</td>
											</tr>
											<tr>
												<td></td>
												<td><font>&nbsp;</font></td>
												<td>
													<div id="divOLap3">
														<font class="fntBold">Overlapped Segments</font>
													</div>
												</td>
											</tr>
											<tr>	
												<td valign="top"><span id="spnValidSeg"></span></td>
												<td>&nbsp;</td>
												<td valign="top"><span id="spnGridOLapSeg"></span></td>
											</tr>
											<tr>
												<td ><font>Overlap with another Flight Schedule</font>&nbsp;&nbsp;&nbsp;							
												
													<u:hasPrivilege privilegeId="plan.flight.sch.seg.overlap">
														<input type="button" id="btnOLap" name="btnOLap" class="button"  value="Select Overlap" style="width:100px" onclick="ViewOverLap()" title="Select Overlap Flight Schedule ">
													</u:hasPrivilege>
												</td>
												<td>&nbsp;</td>
												<td> 
													<div id="divRemove">
														<input type="button" id="btnOLapRemove" name="btnOLapRemove" class="button"  value="Remove OverLap" style="width:100px" onclick="removeOverLap()" title="Remove Overlap Flight Schedule">
													</div>										
												
												</td>
											</tr>
											<tr>
												<td colspan="3"><hr></td>
											</tr>
											<tr>
												<td>
													<input type="button" id="btnCancel"  name="btnCancel" value="Cancel" class="Button" onclick="cancelClick()">
													
												</td>
												<td><font>&nbsp;</font></td>
												<td align="right">&nbsp;
													<input type="button" id="btnConfirm"  name="btnConfirm" value="Confirm" class="Button" onClick="confirmValidSegment()">
												</td>
											</tr>
										</table>
									<%@ include file="../common/IncludeFormBottom.jsp"%>
									</td>
								</tr>
							</table>							
					<input type="hidden" name="hdnMode" id="hdnMode"  value="<%=request.getParameter("strSaveMode")%>"/>
					<input type="hidden" name="hdnLegs" id="hdnLegs"   value="<%=request.getParameter("strLegs")%>"/>
					<input type="hidden" name="hdnLegDtls" id="hdnLegDtls"   value="<%=request.getParameter("strLegDtls")%>"/>
					<input type="hidden" name="hdnOverlapId" id="hdnOverlapId"  value="<%=request.getParameter("hdnOverlapSchID")%>"/>
					<input type="hidden" name="hdnSegNOFlag" id="hdnSegNOFlag"   value=""/>					
					</form>
				<!-- Your Form ends here -->
							</td>
							<td width="15"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
						</tr>
					</table>						
	<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
  	<script src="../../js/flightschedule/ValidateSegments.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </body>
</html>
