
<%-- 
	 @Author 	: 	Asiri Wijayasinghe
	 @Copyright : 	ISA
--%>

<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<%@ page language="java"%>
<%@ page import="java.util.*,java.text.*"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Configure Onhold Time Limits</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">

<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">		
<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
 
 
<script
	src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
<script
	src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/scheduledservices/PFSEditContentPopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/scheduledservices/PFSAlternateFlight.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script type="text/javascript"
	src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.jqGridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.form_last_worked_ver.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/flightschedule/ConfigTimeLimits.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>


</head>

<body class="tabBGColor" scroll="no" onkeypress=''
	oncontextmenu="return false" onkeydown='' ondrag='return false'>

	
		<script type="text/javascript">
			var arrOnlineAirports = new Array();
			<c:out value="${requestScope.reqOnlineAirportList}" escapeXml="false" />
		</script>
		<script type="text/javascript">
			var arrBookingClassCode= new Array();
			<c:out value="${requestScope.reqBookingCodeList}" escapeXml="false" />
		</script>
		<table width="99%" align="center" border="0" cellpadding="0"
			cellspacing="0">
			<tr>
				<td>
					<table width="99%">
						<tr>
							<td width="88%" align="left"><font>All times are in
									local</font></td>
							<!--td  align="right">
							<font class="fntSmall">All times in local</font>
					</td-->
						</tr>
					</table>
				</td>
			</tr>
			<!--tr>
					<td>
						<font >All times in local</font>
				</td>
				</tr-->
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>
					Search Onhold Config <%@ include file="../common/IncludeFormHD.jsp"%>
					
					<form method="post" action="showConfigOnholdTimeLimits.action"
						id="frmConfigTimeLmts">
					<table width="100%" border="0" cellpadding="0" cellspacing="2"
						ID="Table2">
						<tr>
							<td align="left"><font>From</font></td>
							<td><select name="selFromSrch" id="selFromSrch" size="1"
								maxlength="30" style="width: 60px" title="Origin"
								tabIndex="10">
							</select></td>
							
							<td align="left"><font>To</font></td>
							<td><select name="selToSrch" id="selToSrch" size="1"
								maxlength="30" style="width: 60px" title="Destination"
								tabIndex="10">
							</select></td>
									
							<td align="left"><font>Booking Class</font></td>
							<td><select name="selBookingClassSrch" size="1" id="selBookingClassSrch">
									<option value=""></option>
									<option value="ANY">ANY</option>
									<c:out value="${requestScope.reqAllBookingCodes}" escapeXml="false" />
							</select></td>
							
							<td align="left"><font>Flight Type</font></td>
							<td><select name="setFltTypeSrch" size="1" id="setFltTypeSrch">
									<option value=""></option>
									<c:out value="${requestScope.reqFlightTypesList}" escapeXml="false" />
							</select></td>
							
							<td align="right"><input tabindex="5" name="btnSearchConfigTimeLmts"
								type="button" class="button" id="btnSearchConfigTimeLmts"
								 value="Search"></td>
						</tr>
						<tr>
							
							<td align="left"><font>Module Code</font></td>
							<td><select name="selModuleCodeSrch" id="selModuleCodeSrch">
									<option value=""></option>
									<option value="ANY">ANY</option>
									<option value="XBE">XBE</option>
									<option value="IBE">IBE</option>
									<option value="API">API</option>
							</select></td>
							
							<td align="left"><font>Rank</font></td>
							<td><select name="selRankSrch" id="selRankSrch">
									<option value=""></option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
							</select></td>
							
							<td align="left"><font>Agent Code</font></td>
							<td colspan="5"><select name="selAgentCodeSrch" size="1" id="selAgentCodeSrch">
									<option value=""></option>
									<option value="ANY">ANY</option>
									<c:out value="${requestScope.reqAgentCodeList}" escapeXml="false" />
							</select></td>
							
							<td align="left"><font>&nbsp;</font></td>
							<td align="left">&nbsp;</td>
							<td align="right">&nbsp;</td>
						</tr>
					</table> 
					</form>
					
					<%@ include file="../common/IncludeFormBottom.jsp"%>	
				</td>
			</tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Received
					Onhold Configs<%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="4"
						ID="Table9">
						<!--tr><td><font class="fntSmall">&nbsp;</font></td></tr-->
						<tr>
							<td>
								<!-- <span id="spnDepartureData"></span>  -->
								<table id="tblConfigOnholdTimeLimits" width='100%' border='0' cellpadding='0' cellspacing='0'></table>
								<div id="divConfigOnholdTimeLimitsPager" class="scroll" style="text-align:center;"></div>
							</td>
						</tr>
						<tr>
							<td><input type="button" id="btnAddConfigTimeLmts"
								name="btnAddConfigTimeLmts" class="Button" value="Add"
								tabindex="6">  <input type="button"
								id="btnEditConfigTimeLmts" name="btnEditConfigTimeLmts"
								class="Button" value="Edit" tabindex="7">
								<input type="button"
								id="btnDeleteConfigTimeLmts" name="btnDeleteConfigTimeLmts"
								class="Button" value="Delete"></td>
						</tr>
					</table> <%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>

			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>
					Add/Modify Onhold Configs <%@ include
						file="../common/IncludeFormHD.jsp"%>
					<table width="98%" border="0" cellpadding="0" cellspacing="0"
						align="center" ID="Table1">
						<tr>
							<td>
								<form method='post' action="showConfigOnholdTimeLimits.action" id="frmAddModifyConfigs"> 
									<table width="98%" border="0" cellpadding="0" cellspacing="0"
										align="left" id="Table2">
										<tr>
											<td width="75%" valign="top">
												<table>
												
												<!-- 													
													<tr>
														<td align="left"><font>Config ID</font></td>
														<td><font class="fntBold"><span id="configID"></span></font></td>
														<td></td>
													</tr>   -->
	
													<tr>
														<td align="left"><font>From</font></td>
														<td><select name="selFromOpt" id="selFromOpt" size="1"
															maxlength="30" style="width: 60px" title="Origin"
															tabIndex="10">
														</select></td>
														<td></td>
													</tr>
	
													<tr>
														<td align="left"><font>To</font></td>
														<td><select name="selToOpt" id="selToOpt" size="1"
															maxlength="30" style="width: 60px" title="Destination"
															tabIndex="10">
														</select></td>
														<td></td>
													</tr>
	
													<tr>
														<td align="left"><font>Cabin Class</font></td>
														<td><select name="selCabinClass" size="1" id="selCabinClass">
																<option value="ANY">ANY</option>
																<c:out value="${requestScope.reqCabinClassList}" escapeXml="false" />
														</select></td>
														<td></td>
													</tr>
													
													<tr>
														<td align="left"><font>Booking Class</font></td>
														<td><select name="selBookingClass" size="1" id="selBookingClass">
																<option value="ANY">ANY</option>
<%-- 															var arrBookingClassCode=<c:out value="${requestScope.reqBookingCodeList}" escapeXml="false" /> --%>
														</select></td>
														<td></td>
													</tr>
													
													<tr>
														<td align="left"><font>Flight Type</font></td>
														<td><select name="selFltType" size="1" id="selFltType">
																<option value=""></option>
																<c:out value="${requestScope.reqFlightTypesList}" escapeXml="false" />
														</select><font class="mandatory"> &nbsp;* </font></td>
														<td></td>
	
													</tr>
	
													<tr>
														<td align="left"><font>Module Code </font></td>
														<td><select name="selModuleCode" id="selModuleCode">
																<option value="ANY">ANY</option>
																<option value="XBE">XBE</option>
																<option value="IBE">IBE</option>
																<option value="API">API</option>
														</select></td>
														<td></td>
													</tr>
	
													<tr>
														<td align="left"><font>Rank</font></td>
														<td><select name="selRank" id="selRank">
																<option value="1">1</option>
																<option value="2">2</option>
																<option value="3">3</option>
																<option value="4">4</option>
																<option value="5" selected="selected">5</option>
														</select></td>
														<td></td>
													</tr>
	
													<tr>
														<td align="left"><font>Start Cutover Time</font></td>
														<td><input name="startCutoverTime" type="text"
															id="startCutoverTime" maxlength="30" size="15"
															tabindex="14"><font>in Minutes</font><font class="mandatory"> &nbsp;* </font></td>
														<td></td>
													</tr>
													<tr>
														<td align="left" valign="top"><font>End Cutover Time</font></td>
														<td><input name="endCutoverTime" type="text"
															id="endCutoverTime" maxlength="30" size="15"
															tabindex="14"><font>in Minutes</font><font class="mandatory"> &nbsp;* </font></td>
														<td></td>
													</tr>
													<tr>
														<td align="left" valign="top"><font>Release Time</font></td>
														<td><input name="releaseTime" type="text"
															id="releaseTime" maxlength="30" size="15"
															tabindex="14"> <font class="mandatory"> &nbsp;* </font> </td>
														<td></td>
													</tr>
													<tr>
														<td align="left" valign="top"><font>Time With Respect To</font></td>
														<td><select name="selRelTimeRespectTo" id="selRelTimeRespectTo">
															<option value=""></option>
															<option value="BKG_DATE">Booking Date</option>
															<option value="DEP_DATE">Departure Date</option>
														</select></td>
														<td></td>
													</tr>
													<tr>
														<td align="left"><font>Agent Code</font></td>
														<td><select name="selAgentCode" size="1" id="selAgentCode">
																<option value="ANY">ANY</option>
																<c:out value="${requestScope.reqAgentCodeList}" escapeXml="false" />
														</select></td>
														<td></td>
													</tr>
													
												</table>
											</td>
											<td width="50%" valign="top">
												<table>
													<tr>
														<td align="left"><b><u><font></font></u></b></td>
													</tr>
													<tr>
														<td><font><span id="spnContent"
																name="spnContent"
																STYLE="overflow: scroll; width: 350; height: 100; border: 1 #000000 solid; text-align: left; padding: 2px"></span></font>
														</td>
														<!--textarea name="txtaRulesCmnts" id="txtaRulesCmnts" cols="70" rows="11" maxlength="225" onkeyUp="validateTextArea(this)" onkeyPress="validateTextArea(this)" title="Can enter only up to 255 charactors" readOnly="true" ></textarea-->
														</td>
													</tr>
												</table> 
											</td>
										</tr>
									</table>
										<input type="text" name="version"  id="version" style="display: none;" />
										<input type="hidden" name="hdnId" id="hdnId" /> 
										<input type="hidden" name="hdnVersion" id="hdnVersion" /> 
										<input type="hidden" name="hdnGridRow" id="hdnGridRow" /> 
										<input type="hidden" name="hdnMode" id="hdnMode" />
									
								</form>
							</td>
						</tr>

						<tr>
							<td style="height: 42px;" colspan="2" valign="bottom">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td>
										<input tabindex="16" type="button" name="btnCloseConfigTimeLmts"
								
											id="btnCloseConfigTimeLmts" class="Button" value="Close"> 
										<input tabindex="17"
											name="btnResetConfigTimeLmts" type="button" class="Button"
											id="btnResetConfigTimeLmts" value="Reset"></td>
										<td colspan="5" align="center"><input tabindex="21"
											name="btnSaveConfigTimeLmts" type="button" class="Button"
											id="btnSaveConfigTimeLmts" value="Save"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table> <%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>

		</table>


</body>


<script>
		var screenId = 'SC_SHDS_0036';
		top[2].HideProgress();
</script>

</html>

