<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;
	Date dStopDate = null;
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date
	cal.add(Calendar.MONTH,1);
	dStopDate = cal.getTime(); //Next Month Date
	String CurrentDate =  formatter.format(dStartDate);
	String NextMonthDate = formatter.format(dStopDate);
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Copy Flight Schedule</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="-1">    
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">

	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
	<script src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>			
	</head>
	<body onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="no" onLoad="winOnLoad()">
	<%@ include file="../common/IncludeWindowTop.jsp" %><!-- Page Background Top page -->		
					<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBorder" ID="Table7" style="height:300px;">
						<tr>
							<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
							<td valign="top" align="center" class="PageBackGround">
								<!-- Your Form start here -->
								<form method="post"  id="frmSchdCopy" name="frmSchdCopy"action="showSchedule.action">
								<br>
								<table width="98%" cellpadding="0" cellspacing="0" border="0" ID="Table7">
								<tr>
									<td><font class="Header">Copy Flight Schedule</font></td>
								</tr>
								<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
								<tr>
									<td><%@ include file="../common/IncludeMandatoryText.jsp"%>
									</td>
								</tr>
									<tr>
										<td>
										<%@ include file="../common/IncludeFormTop.jsp"%>
										Copy Schedule<%@ include file="../common/IncludeFormHD.jsp"%>
											<table width="100%" border="0" cellpadding="2" cellspacing="2" ID="Table2">
												<tr>
													<td width="160"><font>From Date</font></td>
													<td width="100"><input type="text" id="txtFromDate"  name="txtFromDate" style="width:75px;" maxlength="10"  name="txtFromDate" invalidText="true"  onBlur="dateChk('txtFromDate')" onchange="dataChanged()">
													</td>
													<td><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="View Calendar"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font></td>
												</tr>
												<tr>
													<td><font>To Date</font></td>
													<td><input type="text" id="txtToDate"  name="txtToDate" style="width:75px;" maxlength="11" invalidText="true" name="txtToDate"  onBlur="dateChk('txtToDate')" onchange="dataChanged()">
													</td>
													<td><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="View Calendar"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font></td>
												</tr>												
												<tr>
													<td colspan="3"><hr></td>
												</tr>
												<tr>
													<td colspan="2">
														<input type="button" id="btnCancel" value="Cancel" class="Button" onclick="cancelClick()" name="btnCancel">
													</td>
													<td align="right">
														<input type="button" id="btnConfirmed" value="Confirm" class="Button" name="btnConfirmed" onclick="ConfirmCopy()">
														
													</td>
												</tr>
											</table>
										<%@ include file="../common/IncludeFormBottom.jsp"%>
										</td>
									</tr>
								</table>
								<input type="hidden" name="hdnMode" id="hdnMode"  value=""/>
								<input type="hidden" name="hdnCurrentDate" id="hdnCurrentDate" value="<%=CurrentDate %>">
								<input type="hidden" name="hdnStartD" id="hdnStartD" value=""/>
								<input type="hidden" name="hdnStopD" id="hdnStopD"  value=""/>
								<input type="hidden" name="hdnCopyStartD" id="hdnCopyStartD"  value=""/>
								<input type="hidden" name="hdnCopyStopD" id="hdnCopyStopD"  value=""/>
								<input type="hidden" name="hdnScheduleId" id="hdnScheduleId" value=""/>

								</form>
							<!-- Your Form ends here -->
							</td>
							<td width="15"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
						</tr>
					</table>				
		<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
		<script src="../../js/flightschedule/CopySchedule.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	</body>
</html>
