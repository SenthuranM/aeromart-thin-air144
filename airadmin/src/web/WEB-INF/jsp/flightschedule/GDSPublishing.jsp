 <%-- 
	 @Author 	: 	Srikanth
	 @Copyright : 	ISA
  --%>
<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date
	cal.add(Calendar.MONTH,1);
	String CurrentDate =  formatter.format(dStartDate);
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>GDS Publishing</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  	    <script type="text/javascript">
  	    	var version = '<c:out value="${version}" escapeXml="false" />';
  	    	var popupmessage = "<c:out value="${popupmessage}" escapeXml="false" />";
			<c:out value="${requestScope.sesGDSListData}" escapeXml="false" />
		</script>
  </head>
  <body scroll="no" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown='return Body_onKeyDown(event)' ondrag='return false' 
  	  onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
	<%@ include file="../common/IncludeWindowTop.jsp"%>	
	<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround" ID="Table1">
		<tr>
		  
			<td width="13" class="BannerBorderLeft"><img src="../../images/spacer_no_cache.gif" width="13" height="1"></td>
			<td valign="top">
				<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBorder" ID="Table7">
					<tr>
						<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
						<td valign="top" align="center" class="PageBackGround">
							
						</td>
						<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
					</tr>
				</table>
			</td>
			<td width="12" class="BannerBorderRight"><img src="../../images/spacer_no_cache.gif" width="12" height="1"></td>
		</tr>
	</table>  

<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td><font class="Header">GDS Publishing&nbsp;&nbsp;<span id="spnDSTHeader"></span></font></td>
	</tr>
	<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
	<tr>
		<td>
			<%@ include file="../common/IncludeFormTop.jsp"%>
			GDS Publishing<%@ include file="../common/IncludeFormHD.jsp"%>	
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr><td><font>&nbsp;</font></td></tr>
				<tr>
					<td style="height:200px;" valign="top">
						<span id="spnGDS"></span>
					</td>
				</tr>
				<tr><td><font>&nbsp;</font></td></tr>
				<tr>
					<td>
						<input type="button" id="btnClose" class="Button" value="Close" onclick="closeClick()">
						<input name="btnSave" type="button" class="Button" id="btnSave" value="Save" onClick="saveGDSPublishing()">
					</td>
				</tr>
			</table>
			<%@ include file="../common/IncludeFormBottom.jsp"%>
		</td>
	</tr>
	<tr><td><font class="fntSmall"></font></td></tr>							
</table>
	<%@ include file="../common/IncludeWindowBottom.jsp"%>
	<form method="post"  id="GDSData" name="GDSData" action="showGDSPublishing.action">
	<input type="hidden" name="hdnScheduleId"  id="hdnScheduleId" value='<c:out value="${scheduleID}" escapeXml="false" />'/>	
	<input type="hidden" name="hdnViewModeOnly"  id="hdnViewModeOnly" value='<c:out value="${viewModeOnly}" escapeXml="false" />'/>	
	<input type="hidden" name="hdnVersion" id="hdnVersion" value=""/>	
	<input type="hidden" name="hdnGridRow" id="hdnGridRow" value=""/>
	<input type="hidden" name="hdnGDSPublishing" id="hdnGDSPublishing" value='<c:out value="${gdsList}" escapeXml="false" />'/>
	<input type="hidden" name="hdnCurrentDate" id="hdnCurrentDate" value="<%=CurrentDate %>">
	<input type="hidden" name="hdnMode" id="hdnMode" />
	<input type="hidden" name="hdnHaveMCFlight" id="hdnHaveMCFlight" value='<c:out value="${haveMCFlight}" escapeXml="false" />'/>


	</form>
  </body>
	<script src="../../js/flightschedule/GDSPublishing.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>		
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>	
	<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/flightschedule/GDSPublishingGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
</html>
