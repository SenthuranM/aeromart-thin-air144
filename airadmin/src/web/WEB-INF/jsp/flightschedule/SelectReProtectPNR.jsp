<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	    <title>Select PNR to Re-protect Page</title>
	    <meta http-equiv="pragma" content="no-cache"/>
	    <meta http-equiv="cache-control" content="no-cache"/>
	    <meta http-equiv="expires" content="-1"/>
		<link rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>		
		<link rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>
		
		
		<style>
			.tipBase{
				background: #e4e4e4;
			}
			.paxClass{
				background: #333;
				height:15px;
				width:15px;
			}
			.seatDrop{
				border:1px solid #fff;
			}
			.ui-state-active, .ui-state-hover{
				border:1px dashed #ff9988;
			}
			#dragHElper{
				position:absolute;
				width:100%;
				height:100%;
				z-index:-1;
			}
			.select-to-drag{
				background:#ff0000;
			}
		</style>
		<script type="text/javascript">
			var hidFlightID = '<c:out value="${requestScope.hdnFlightSegId}" escapeXml="false" />';
			var hdnCabinClass = '<c:out value="${requestScope.hdnCabinClass}" escapeXml="false" />';
			var hdnTransferAll = '<c:out value="${requestScope.hdnTransferAll}" escapeXml="false" />';
			var hdnSelRowNo = '<c:out value="${requestScope.hdnSelRowNo}" escapeXml="false" />';
			
			
		</script>
		
 </head>
 
 <body class="tabBGColor">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.airutil.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
  	<script src="../../js/flightschedule/selectReprotectPNR.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  	<%@ include file="../common/IncludeFormTop.jsp"%>Select PNR to Re-protect<%@ include file="../common/IncludeFormHD.jsp"%>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" height="100%" >
		<tr>
			<td id="fromFlightProtect" class="" width="25%" valign="top" align="center" >&nbsp;</td>
			<td id="fromFlightProtect" class="" width="25%" valign="top" align="center" >&nbsp;</td>
			<td id="fromFlightProtect" class="" width="25%" valign="top" align="center" >&nbsp;</td>
			<td id="fromFlightProtect" class="" width="25%" valign="top" align="center" >&nbsp;</td>				
				
		</tr>
		<tr>
			<td id="fromFlightProtect" class="" width="25%" valign="top" align="left" colspan="3" height="675">
				<span id="spnSeatMovementsInfo"  style="position:absolute;z-index:5"> </span>
			</td>			
		</tr>
		<tr>
			<td id="fromFlightProtect" class="" width="25%" valign="top" align="center" >&nbsp;</td>
			<td id="fromFlightProtect" class="" width="25%" valign="top" align="center" >&nbsp;</td>
			<td id="fromFlightProtect" class="" width="25%" valign="top" align="center" >&nbsp;</td>
			<td id="fromFlightProtect" class="" width="25%" valign="top" align="center" >
				
				<input type="button" id="btnconfirm" name="btnconfirm" value="Confirm" onclick="UI_reprotectPNR.confirm()"/>
				<input type="button" id="btnclose" name="btnclose" value="Close" onclick="UI_reprotectPNR.close()"/>
			</td>
		</tr>
	</table>
	<div id="divLoadMsg" style="z-index: 1000; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; display: block; background-position: initial initial; background-repeat: initial initial;position: absolute;top:45%;left:30% ">
		<iframe src="LoadMsg" id="frmLoadMsg" name="frmLoadMsg" frameborder="0" scrolling="no" style="position:absolute; top:50%; left:40%; width:260px; height:40px;"></iframe>
	</div>
 </body>
</html>