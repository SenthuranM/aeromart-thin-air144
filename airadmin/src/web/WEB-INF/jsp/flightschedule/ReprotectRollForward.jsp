 <%-- 
	 @Author 	: 	Srikanth
  --%>
  
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Reprotect RollForward</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </head>
  <body scroll="no"  oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown='return Body_onKeyDown(event)' ondrag='return false' 
  	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
   <form method="post" id="frmFlightRollforward" name="frmFlightRollforward"  action="saveReprotectRollForward.action">
   <%@ include file="../common/IncludeWindowTop.jsp"%><!-- Page Background Top page -->
   <table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td><font class="Header">Re-Protect Roll Forward</font></td>
		</tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td>
				<%@ include file="../common/IncludeMandatoryText.jsp"%>
			</td>
		</tr>				
		<tr>
			<td>
				<%@ include file="../common/IncludeFormTop.jsp"%>Roll Forward<%@ include file="../common/IncludeFormHD.jsp"%>
		  	<table width="100%" border="0" cellpadding="2" cellspacing="2">
  				<tr>
					<td width="15%">
						<font class="fntBold">Reprotect From</font>
					</td>
					<td width="10%">
						<font><span id="spnFlightNo"></span></font>
					</td>
					<td width="6%">
						<font class="fntBold">Origin</font>
					</td>
					<td width="10%">
						<font><span id="spnOrigin"></span></font>
					</td>
					<td width="11%">
						<font class="fntBold">Destination</font>
					</td>
					<td>
						<font><span id="spnDest"></span></font>
					</td>
				</tr>
				<tr>
					<td>
						<font class="fntBold">Reprotect To</font>
					</td>
					<td>
						<font><span id="spnFlightNoRe"></span></font>
					</td>
					<td>
						<font class="fntBold">Origin</font>
					</td>
					<td>
						<font><span id="spnOriginRe"></span></font>
					</td>
					<td>
						<font class="fntBold">Destination</font>
					</td>
					<td>
						<font><span id="spnDestRe"></span></font>
					</td>
				</tr>
				<tr><td colspan="6"><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td colspan="6">
						<table width="100%" border="0" cellpadding="2" cellspacing="0">
							<tr>
								<td width="20%">
									<font>Departure Date - From</font>
								</td>
								<td width="20%">
									<input type="text" id="txtFromDate" style="width:75px;" maxlength="10" name="txtFromDate"  onBlur="dateChk('txtFromDate')" invalidText="true" onChange="dataChanged()" disabled>
									<!--a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0"></a-->
								</td>
								<td width="20%">
									<font>Departure Date - Till</font>
								</td>
								<td>
									<input type="text" id="txtTillDate" style="width:75px;" maxlength="10" name="txtTillDate"  onBlur="dateChk('txtTillDate')" invalidText="true" onChange="dataChanged()">
									<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0"></a>
								</td>
							</tr>
							<tr><td colspan="4"><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<!--td>
									<font>All Occurences</font>
									<input type="checkbox" class="NoBorder" name="chkAllOccur" id="chkAllOccur"  onclick="disableFrequncies(this)">
								</td-->
								<td>
									<font>Frequency</font>
								</td>
								<td colspan="3">
									<font>Mo</font><input type="checkbox" class="NoBorder" name="chkMonday" id="chkMonday" onClick="dataChanged()" checked>
									<font>Tu</font><input type="checkbox" class="NoBorder" name="chkTuesday" id="chkTuesday" onClick="dataChanged()" checked>
									<font>We</font><input type="checkbox" class="NoBorder" name="chkWednesday" id="chkWednesday" onClick="dataChanged()" checked>
									<font>Th</font><input type="checkbox" class="NoBorder" name="chkThursday" id="chkThursday" onClick="dataChanged()" checked>
									<font>Fr</font><input type="checkbox" class="NoBorder" name="chkFriday" id="chkFriday" onClick="dataChanged()" checked>
									<font>Sa</font><input type="checkbox" class="NoBorder" name="chkSaturday" id="chkSaturday" onClick="dataChanged()" checked> 
									<font>Su</font><input type="checkbox" class="NoBorder" name="chkSunday" id="chkSunday" onClick="dataChanged()" checked>
								</td>
							</tr>
							<tr><td colspan="4"><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td colspan="4">
									<table width="100%  border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td width="10%">			
												<font>Auto-fit</font>
												<input type="radio" class="NoBorder" name="optAllocMode" id="optAllocMode" value="AUTO_FIT"  checked onClick="dataChanged()">
											</td>
											<td width="14%">
												<font>Over Allocate</font>
												<input type="radio" class="NoBorder" name="optAllocMode" id="optAllocMode" value="OVER_ALLOC" onClick="dataChanged()">
											</td>
											<td>
												<font>Over Allocate Seats</font>
												<input type="text" name="txtOverAllocSeats" id="txtOverAllocSeats" maxlength="3" size="4" onkeyUp="positiveWholeNumberValidate(this)" onkeyPress="positiveWholeNumberValidate(this)" onChange="dataChanged()">
											</td>
											<td align="right">
												<font class="fntSmall">Note: Over allocate may cause minus availability. Auto fit will fit the available capacity</font>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr><td colspan="4"><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td colspan="4">
									<font>Include Closed Flights</font>&nbsp;&nbsp;<input type="checkbox" class="NoBorder" name="chkClosedFlights" id="chkClosedFlights" onClick="dataChanged()">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="6"><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>					
					<td colspan="3">
						<input type="button" class="Button" name="btnCancel" id="btnCancel" value="Close" onClick="closeClick()">
					</td>					
					<td colspan="3" align="right">
						<input type="button" class="Button" name="btnRoll" id="btnRoll" value="Confirm" onClick="rollForwardReprotect()">
					</td>
				</tr>
		  	</table>
			<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
	</table>
	<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
    <input type="hidden" name="hdnMode" id="hdnMode" value="">
	<!-- Used in ReprotectRollForwardRequestHandler.java file -->
	<input type="hidden" name="hdnLogicalCabinClassCode" id="hdnLogicalCabinClassCode">
	<input type="hidden" name="hdnTargetFlightId" id="hdnTargetFlightId">
	<input type="hidden" name="hdnTargetSegmentCode" id="hdnTargetSegmentCode">
	<input type="hidden" name="hdnTargetFlightSegmentId" id="hdnTargetFlightSegmentId">
	<input type="hidden" name="hdnSourceFlightDistributions" id="hdnSourceFlightDistributions">
	<input type="hidden" name="hdnFlightId" id="hdnFlightId">
	<input type="hidden" name="hdnlAlert" id="hdnAlert" value="false"/>	
	<input type="hidden" name="hdnSMSAlert" id="hdnSMSAlert" value=""/>	
	<input type="hidden" name="hdnEmailAlert" id="hdnEmailAlert" value=""/>	
	<input type="hidden" name="hdnSmsCnf" id="hdnSmsCnf" value=""/>	
	<input type="hidden" name="hdnSmsOnH" id="hdnSmsOnH" value=""/>
	<input type="hidden" name="hdnEmailOriginAgent" id="hdnEmailOriginAgent" value=""/>	
	<input type="hidden" name="hdnEmailOwnerAgent" id="hdnEmailOwnerAgent" value=""/>	
  </form>
  	<script src="../../js/flightschedule/validateRollForward.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </body>
</html>