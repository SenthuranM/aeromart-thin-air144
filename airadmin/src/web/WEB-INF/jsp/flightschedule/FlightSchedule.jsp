 <%-- 
	 @Author 	: 	Menaka
	 @Copyright : 	ISA
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;
	Date dStopDate = null;
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date
	cal.add(Calendar.MONTH,1);
	dStopDate = cal.getTime(); //Next Month Date
	String CurrentDate =  formatter.format(dStartDate);
	String NextMonthDate = formatter.format(dStopDate);
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	    <title>Template Page</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="-1">
	    <LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">	
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
		<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">	
		
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script  src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
 		<script type="text/javascript" src="../../js/v2/jquery/jquery.js"></script>
 		<script src="../../js/v2/jquery/jquery-migrate.js" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js "></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
  
		<script src="../../js/tools/PopUpData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		
		<script  src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	</head>
  	<body oncontextmenu="return false" ondrag="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" scroll="no"
  			onload="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" onunload="beforeUnload()" class="tabBGColor">
  		<form method="post" name="frmSchedule" id="frmSchedule" action="showSchedule.action" target="_self">
	
  	    <script type="text/javascript">
  	  		var hdnBuildDefaultInvTemplate = "<c:out value="${requestScope.reqBuildDefaultInvTemplate}" escapeXml="false" />";	
  	    	var arrData = new Array();
			<c:out value="${requestScope.reqFlightScheduleData}" escapeXml="false" />
			<c:out value="${requestScope.sesOnlineActiveAirportCodeListData}" escapeXml="false" />
			<c:out value="${requestScope.sesAircraftModelDetails}" escapeXml="false" />						
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			<c:out value="${requestScope.sesSchedData}" escapeXml="false" />
			<c:out value="${requestScope.reqSeatsToBeReprotected}" escapeXml="false" />
			<c:out value="${requestScope.durationError}" escapeXml="false" />			
			<c:out value="${requestScope.sesMinStopOverTimeData}" escapeXml="false" />
             var hasdurError = "<c:out value="${requestScope.hasdurationError}" escapeXml="false" />";			
			var arrOnlineAirports = new Array();
			<c:out value="${requestScope.sesOnlineAirportCodeListData}" escapeXml="false" />
						
			var defaultOperationType = "<c:out value="${requestScope.reqDefaultOperationType}" escapeXml="false" />";
			var defaultStatus = "<c:out value="${requestScope.reqScheduleStatus}" escapeXml="false" />";
			var defCarrCode = "<c:out value="${requestScope.reqcrriercode}" escapeXml="false" />";			
			var totalNoOfRecords = "<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />";
			var dayOffset = "<c:out value="${requestScope.reqDayOffSet}" escapeXml="false" />";
			
			var reprotect = "<c:out value="${requestScope.reqcancel}" escapeXml="false" />";
			var reproScheduleid = "<c:out value="${requestScope.reqrepshid}" escapeXml="false" />";
			var reprostdate = "<c:out value="${requestScope.reqrepstd}" escapeXml="false" />";
			var reprostpdate = "<c:out value="${requestScope.reqrepstpd}" escapeXml="false" />";
			var reprofltno = "<c:out value="${requestScope.reqrepfltno}" escapeXml="false" />";
			var reprodestination = "<c:out value="${requestScope.reqrepdest}" escapeXml="false" />";
			var reprofreq = "<c:out value="${requestScope.reqrepfreq}" escapeXml="false" />";
			var reproorg = "<c:out value="${requestScope.reqreporg}" escapeXml="false" />";			
			var popupmessage = "<c:out value="${requestScope.popupmessage}" escapeXml="false" />";			
			var arrCarriers = new Array();
			<c:out value="${requestScope.requsercrriercode}" escapeXml="false" />
			<c:out value="${requestScope.reqTimeIn}" escapeXml="false" />
			<c:out value="${requestScope.reqMultiFltTypesAvailability}" escapeXml="false" />
			
			var seatTemplates = "<c:out value="${requestScope.reqSeatTempl}" escapeXml="false" />";
			var isCodeShareEnable = "<c:out value="${requestScope.reqCodeShareEnable}" escapeXml="false" />"; 
			var gdsCarrierCodeArray = new Array();
			<c:out value="${requestScope.gdsCarrierCodeArray}" escapeXml="false" />	

		</script>
		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">			
			<tr>
				<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
			</tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Flight Schedule Search<%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table2">
						<tr>
							<td style="width:50px;"><font>Start Date</font></td>
							<td><font class="mandatory"> &nbsp;* </font></td>
							<td style="width:50px;"><font>Stop Date</font></td>
							<td><font class="mandatory"> &nbsp;* </font></td>
							<td valign="bottom" width="20"><a href="javascript:PreviousMonth()" title="Previous month"><img src="../../images/UArrow_no_cache.gif" border="0"></a></td>
							<td><font>Flight No</font></td>
							<td><font>From</font></td>
							<td><font>To</font></td>
							<td><font>Operation Type</font> <font class="mandatory"> &nbsp;* </font></td>
							<td><font>Status</font> <font class="mandatory"> &nbsp;* </font></td>
							<td><div id="flightTypeLbl"><font>Flight Type</font> <font class="mandatory"> &nbsp;* </font> </div> </td>	
							<td><font>Build Status</font> <font class="mandatory"> &nbsp;* </font></td>
								
						</tr>
						<tr>
							<td><input type="text" name="txtStartDateSearch" id="txtStartDateSearch"  onBlur="settingValidation('txtStartDateSearch')" invalidText="true" value="<%=CurrentDate %>" style="width:62px;" maxlength="10" tabindex="1" align="middle" onChange="hdeErrmessages()"></td>
							<td><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a></td>
							<td><input type="text" name="txtStopDateSearch" id="txtStopDateSearch"  onBlur="settingValidation('txtStopDateSearch')" invalidText="true" value="<%=NextMonthDate %>" style="width:62px;" maxlength="10" tabindex="2" align="middle" onChange="hdeErrmessages()"></td>
							<td>
								<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a>
							</td>
							<td valign="top">
								<a href="javascript:NextMonth()" title="Next month"><img src="../../images/DArrow_no_cache.gif" border="0" ></a> </td>
							<td>
								<input type="text" id="txtFlightNoStartSearch" name="txtFlightNoStartSearch" maxlength="2" style="width:20px" value="" tabindex="3" align="left" onChange="hdeErrmessages()">
								<input type="text" id="txtFlightNoSearch" name="txtFlightNoSearch" maxlength="<c:out value="${requestScope.reqFltNumberLength}" escapeXml="false" />" style="width:40px" tabindex="4" title="Key in Flight No." align="left" onChange="hdeErrmessages()">
							</td>
							<td>
								<select name="selFromStn6" id="selFromStn6" size="1" style="width:60px" tabindex="5" title="Origin" onChange="hdeErrmessages()">
								</select>
							</td>
							<td>
								<select name="selToStn6" id="selToStn6" size="1" style="width:60px" tabindex="6" title="Destination" onChange="hdeErrmessages()">
								</select>
							</td>
							<td>
								<select name="selOperationTypeSearch" id="selOperationTypeSearch" size="1" tabindex="7" style="width:75;" onChange="hdeErrmessages()">
									<c:out value="${requestScope.sesOperationTypeListData}" escapeXml="false" />
									<option>All</option>
								</select>
							</td>
							<td>
								<select name="selStatusSearch" id="selStatusSearch" size="1" tabindex="8" style="width:75;" onChange="hdeErrmessages()">
									<c:out value="${requestScope.sesStatusListData}" escapeXml="false" />
								</select>
							</td>
							<td>
								<select name="selFlightTypeSearch" id="selFlightTypeSearch" size="1" title="Flight Type" tabIndex="10">
									<option value="All">All</option>
									<c:out value="${requestScope.reqFlightTypes}" escapeXml="false" />
								</select>
							</td>
							<td>
								<select name="selBuildStatusSearch" id="selBuildStatusSearch" size="1" style="width:90px" tabindex="9" onChange="hdeErrmessages()">
									<option value="-1">All </option>
								    <c:out value="${requestScope.sesBuildStatusListData}" escapeXml="false" />
								</select>
							</td>							
							<td>
								<input type="button" id="btnSearch"  name="btnSearch" value="Search" class="Button" onclick="SearchData()" onChange="hdeErrmessages()">
							</td>
						</tr>
						<tr><td colspan="13"></td></tr>														
					</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>
			<tr>
				<td style="height:2px;" colspan="8"></td>
			</tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Flight Schedule Search Results<%@ include file="../common/IncludeFormHD.jsp"%>
				  	<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">							
						<tr>
							<td><span id="spnFlights"></span>
								<span id="spnChkAll"></span>
							</td>
						</tr>
						<tr>
							<td>
								<u:hasPrivilege privilegeId="plan.flight.sch.add">
									<input type="button" name="btnAdd" id="btnAdd" class="Button" value="Add" onclick="AddData()" >
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="plan.flight.sch.edit">
									<input type="button" name="btnEdit" id="btnEdit" class="Button" value="Edit Schedule" onclick="EditData()" style="width:120px;">
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="plan.flight.sch.edit">
									<input type="button" name="btnEditLeg" id="btnEditLeg" class="Button" value="Edit Leg" onclick="EditLegData()" >
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="plan.flight.sch.cancel">
									<input type="button" name="btnCancel" id="btnCancel" class="Button" value="Cancel" onclick="CancelData()" >
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="plan.flight.sch.split">
									<input type="button" name="btnSplit" id="btnSplit" class="Button" value="Split" onclick="SplitData()" >
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="plan.flight.sch.build">
									<input type="button" name="btnBuild" id="btnBuild" class="Button" value="Build"  onclick="BuildData()">
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="plan.flight.sch.copy">
									<input type="button" name="btnCopy" id="btnCopy" class="Button" value="Copy" onclick="CopyData()">
								</u:hasPrivilege>									
								<u:hasPrivilege privilegeId="plan.flight.sch.showflights">
									<input type="button" name="btnFlight" id="btnFlight" class="Button" value="Show Flight" onClick="FlightInstance()" style="width:100px;">
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="plan.flight.sch.gds">
									<input type="button" name="btnGDS" id="btnGDS" value="GDS" class="Button"  onclick="GDSPublishDetails()" tabindex="58">
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="plan.flight.flt.mgt">
									<input type="button" name="btnFlightMnt" id="btnFlightMnt" class="Button" value="Manage Flight" onClick="FlightManage()" style="width:100px;">
								</u:hasPrivilege>				
							</td>
						</tr>
					</table>
				  	<%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>
			<tr>
				<td style="height:2px;" colspan="8"></td>
			</tr>
			<tr>
				<td>					
					<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table5" align="center">
						<tr>
							<td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td>
							<td width="442" height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif">
								<font class="FormHeader">Add/Modify Flight Schedule</font></td>
							<td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td>
							<td width="7" height="20"></td>
							<td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td>
							<td width="250" height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif">
							<font class="FormHeader">&nbsp;&nbsp;Leg Details</font></td>
							<td class="FormHeadBackGround"><font class="FormHeader">Time In</font> </td>
							<td class="FormHeadBackGround"><input type="radio" id="radTZ" name="radTZ" value="LOCAL" class="NoBorder" onClick="setLocalTime()" tabindex="12"></td>				
							<td class="FormHeadBackGround"><font  class="FormHeader">Local</font></td>
							<td class="FormHeadBackGround"><input type="radio" id="radTZ" name="radTZ" value="ZULU" class="NoBorder" onClick="setZuluTime()" tabindex="13"></td>
							<td class="FormHeadBackGround"><font  class="FormHeader">Zulu</font></td>
							<td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td>
						</tr>
						<tr>
							<td colspan="3" valign="bottom" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="7"></td>
							<td></td>
							<td colspan="8" valign="bottom" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="7"></td>
						</tr>
						<tr>
							<td colspan="3" class="FormBackGround" valign="middle">
								<!--  Left Pane Start -->
								<table width="95%" border="0" cellpadding="0" cellspacing="1" align="center" ID="Table1">
									<tr>
										<td><font>Schedule ID</font></td>
										<td colspan="2"><font><span id="spnSchedId"></span></font></td>
										<td><font>Status</font></td>
										<td colspan="2"><font><span id="spnStatus"></span></font></td>
										
									</tr>
									<tr>
										<td width="65"><font>Start Date</font></td>
										<td width="80"><input type="text" name="txtStartDate" id="txtStartDate"  invalidText="true" style="width:75px;" maxlength="10" tabindex="14" onchange="dataChanged()"  onBlur="settingValidation('txtStartDate')" value="<%=CurrentDate %>" align="middle"></td>
										<td width="51"><a href="javascript:void(0)" onclick="LoadCalendar(3,event); return false;" title="View Calendar"><img SRC="../../images/calendar_no_cache.gif" border="0" ></a><font class="mandatory"> &nbsp;* </font></td>
										<td width="55"><font>Stop Date</font></td>
										<td width="80"><input type="text" name="txtStopDate" id="txtStopDate" invalidText="true" value="<%=NextMonthDate %>" style="width:75px;" maxlength="10"  tabindex="15"  onBlur="settingValidation('txtStopDate')" onchange="dataChanged()" align="middle"></td>
										<td width="50"><a href="javascript:void(0)" onclick="LoadCalendar(4,event); return false;" title="View Calendar"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font></td>
									</tr>					
									<tr>
										<td><font>Operation Type</font></td>
										<td colspan="2">
											<select id="selOperationType" name="selOperationType" size="1" style="width:105;" tabindex="16" onchange="dataChanged()">
												<option></option>
												<c:out value="${requestScope.sesOperationTypeListData}" escapeXml="false" />
											</select><font class="mandatory"> &nbsp;* </font>
										</td>
										<td><font>Flight No</font><font id="fntOpBy" style="display: none">/Op. By</font></td>
										<td colspan="2">
											<input type="text" id="txtFlightNoStart" name="txtFlightNoStart" maxlength="2" style="width:20px"  tabindex="17" onchange="dataChanged()" value="" align="left">							
											<input type="text" id="txtFlightNo" name="txtFlightNo" maxlength="<c:out value="${requestScope.reqFltNumberLength}" escapeXml="false" />" style="width:50px"  tabindex="18" onchange="dataChanged()" align="left">
											<%-- This is only for demo purpose --%>
												<select id="selOpBy" style="width:40px;display: none;">
													<option value="D0">D0</option>
													<option value="G9">G9</option>
													<option value="3O">G9</option>
													<option value="E5">G9</option>
												</select>
											<%-- This is only for demo purpose --%>
											<font class="mandatory"> &nbsp;* </font>
										</td>
									</tr>
									<tr>
										<td><font>Aircraft Model </font></td>
										<td colspan="2">
											<select name="selAircraftModel" id="selAircraftModel" size="1" style="width:105;" tabindex="19" onchange="loadAircraftModelCapacity()">	
												<option></option>							
												<c:out value="${requestScope.sesAircraftModelListData}" escapeXml="false" />
											</select><font class="mandatory"> &nbsp;* </font>
										</td>										
										<td colspan="3"><font><span id="spnCapacity"></span></font></td>
									</tr>	
									<tr>
												<td valign="top"><font>Seat Template</font></td>
												<td colspan="5" valign="top">
													<select id="selSeatChargeTemplate" name="selSeatChargeTemplate" size="1" >
														<option></option>
														<c:out value="${requestScope.reqSeatTempl}" escapeXml="false" />
													</select>
												</td>
											</tr>	
											<tr>
												<td valign="top"><font>Meal Template</font></td>
												<td colspan="5" valign="top">
													<select id="selMealTemplate" name="selMealTemplate" size="1">
														<option></option>								
														<c:out value="${requestScope.reqMealTempl}" escapeXml="false" />
													</select>
												</td>
											</tr>		
									<tr id="selectFltType">
										<td><font>Flight Type</font></td>
										<td colspan="5" valign="top">
											<select name="selFlightType" id="selFlightType" size="1" title="Flight Type" tabIndex="20" style="width:105;">
												<option></option>
												<c:out value="${requestScope.reqFlightTypes}" escapeXml="false" />
											</select><font class="mandatory"> &nbsp;* </font>
										</td>
									</tr>				
									<tr>
										<td colspan="5"><span id="spnFrequency"></span></td>
									</tr>
									<tr>
										<td colspan="6"><font>&nbsp;</font></td>
									</tr>
									<tr>
										<td colspan="6">
											<table width="100%" border="0" cellpadding="2" cellspacing="0">
												<tr>
													<td width="16%">
														<font>Flights Exist</font>
													</td>
													<td width="3%" id="tdFltExists" class="fltStatus01"></td>
													<td width="25%" align="right">
														<font>Flights Changed</font>
													</td>
													<td width="3%" id="tdFltChanged" class="fltStatus01"></td>
													<td width="30%" align="right">
														<font>Schedule Overlapped</font>
													</td>
													<td width="3%" id="tdSchdOverlap" class="fltStatus01">									
													</td>
													<td><font>&nbsp;</font></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td valign="top"><font>Remarks </font></td>
										<td colspan="5" valign="top" align="left">
											<textarea name="txtRemarks" style="resize:none" id="txtRemarks" cols="47" rows="4" maxlength="260" onkeyUp="validateTA(this,255);commaValidate(this);spclCharsVali(this);" onkeyPress="validateTA(this,255);commaValidate(this);spclCharsVali(this);"></textarea>
										</td>
									</tr>
									<tr id="CodeShareOC">
											<td width="40%"><font>CS OC Carrier</font></td>
											<td width="20%"><input type="text"
												id="txtCsOCCarrierCode" name="txtCsOCCarrierCode"
												maxlength="2" style="width: 20px"
												onchange="dataChanged()" value="" tabIndex="32"
												readonly="readonly" disabled="disabled"></td>
											<td width="30%"><font>CS OC Flight No</font></td>
											<td width="10%"><input type="text"
												id="txtCsOCFlightNo" name="txtCsOCFlightNo"
												style="width: 50px;"
												maxlength="<c:out value="${requestScope.reqFltNumberLength}" escapeXml="false" />"
												tabIndex="33" onchange="dataChanged()"
												readonly="readonly" disabled="disabled"></td>
										</tr>
								</table>
								<!--  Left Pane End -->
							</td>
							<td></td>
							<td colspan="8" valign="top" class="FormBackGround">
							<!--  right Pane Start -->
								<table width="95%" border="0" cellpadding="0" cellspacing="1" align="center" ID="Table2">					
									<tr>
										<td></td>
										<td><font>Leg</font></td>
										<td></td>
										<td><font>D.Time</font></td>
										<td><font>D.Day</font></td>
										<td><font>A.Time</font></td>
										<td><font>A.Day</font></td>						
										<td><font>Duration</font></td>
										<td><font>Err</font></td>
									</tr>
									<tr>
										<td><font>1.</font></td>
										<td>
											<select name="selFromStn1" id="selFromStn1" size="1"  tabindex="28"style="width:60px" onchange="selFromStn1_Onchange()">
												
											</select>
										</td>
										<td>
											<select name="selToStn1" id="selToStn1" size="1" tabindex="29" style="width:60px" onchange="selToStn1_Onchange()">
											</select>
										</td>
										<td><input type="text" name="txtD_Time1" id="txtD_Time1" maxlength="5"  tabindex="30" style="width:50px" onchange="dataChanged()" onblur="setTimeWithColon('txtD_Time1', document.forms[0].txtD_Time1.value)" align="middle"></td>
										<td>
										<select name="selD_Day1" id="selD_Day1" size="1" style="width:40px"  tabindex="31" onchange="selDaysOnchange()" title="0 = same day +n = No. of days from Flight Date -n = No.of days before flight date">
										</select>
										</td>
										<td><input type="text" name="txtA_Time1" id="txtA_Time1" maxlength="5" style="width:50px"  tabindex="32" onchange="dataChanged()" onblur="setTimeWithColon('txtA_Time1', document.forms[0].txtA_Time1.value)" align="middle"></td>
										<td>
										<select name="selA_Day1" id="selA_Day1" size="1"  style="width:40px"  tabindex="33"onchange="selDaysOnchange()" title="0 = same day +n = No. of days from Flight Date -n = No.of days before flight date">
										</select><font class="mandatory"> &nbsp;* </font>
										</td>						
										<td><font><span id="spnDuration1"></span></font></td>
										<td><font><span id="spnError1" ></span></font></td>
									</tr>
									<tr>
										<td><font>2.</font></td>
										<td>
											<select name="selFromStn2" id="selFromStn2" size="1" style="width:60px"  tabindex="34" onchange="selFromStn2_Onchange()">
											</select>
										</td>
										<td>
											<select name="selToStn2" id="selToStn2" size="1" style="width:60px"  tabindex="35" onchange="selToStn2_Onchange()">							
											</select>
										</td>
										<td><input type="text" name="txtD_Time2" id="txtD_Time2" maxlength="5" style="width:50px"  tabindex="36"onchange="dataChanged()" onblur="setTimeWithColon('txtD_Time2',document.forms[0].txtD_Time2.value)" align="middle"></td>
										<td>
										<select name="selD_Day2" id="selD_Day2" size="1" style="width:40px"  tabindex="37" onchange="selDaysOnchange()" title="0 = same day +n = No. of days from Flight Date -n = No.of days before flight date">
										</select>
										</td>
										<td><input type="text" name="txtA_Time2" id="txtA_Time2" maxlength="5" style="width:50px"  tabindex="38" onchange="dataChanged()" onblur="setTimeWithColon('txtA_Time2', document.forms[0].txtA_Time2.value)" align="middle"></td>
										<td>
										<select name="selA_Day2" id="selA_Day2" size="1" style="width:40px"  tabindex="39" onchange="selDaysOnchange()" title="0 = same day +n = No. of days from Flight Date -n = No.of days before flight date">
										</select>
										</td>						
										<td><font><span id="spnDuration2"></span></font></td>
										<td><font><span id="spnError2"></span></font></td>
									</tr>
									<tr>
										<td><font>3.</font></td>
										<td>
											<select name="selFromStn3" id="selFromStn3" size="1" style="width:60px"  tabindex="40" onchange="selFromStn3_Onchange()">								
											</select>
										</td>
										<td>
											<select name="selToStn3" id="selToStn3" size="1" style="width:60px"  tabindex="41" onchange="selToStn3_Onchange()">							
											</select>
										</td>
										<td><input type="text" name="txtD_Time3" id="txtD_Time3" maxlength="5" style="width:50px"  tabindex="42" onchange="dataChanged()" onblur="setTimeWithColon('txtD_Time3', document.forms[0].txtD_Time3.value)" align="middle"></td>
										<td>
										<select name="selD_Day3" id="selD_Day3" size="1" style="width:40px"  tabindex="43" onchange="selDaysOnchange()" title="0 = same day +n = No. of days from Flight Date -n = No.of days before flight date">
										</select>
										</td>
										<td><input type="text" name="txtA_Time3" id="txtA_Time3" maxlength="5" style="width:50px"  tabindex="44" onchange="dataChanged()" onblur="setTimeWithColon('txtA_Time3', document.forms[0].txtA_Time3.value)" align="middle"></td>
										<td>
										<select name="selA_Day3" id="selA_Day3" size="1" style="width:40px"  tabindex="45" onchange="selDaysOnchange()" title="0 = same day +n = No. of days from Flight Date -n = No.of days before flight date">
										</select></td>
										<td><font><span id="spnDuration3"></span></font></td>
										<td><font><span id="spnError3"></span></font></td>
									</tr>
									<tr>
										<td><font>4.</font></td>
										<td>
											<select name="selFromStn4" id="selFromStn4" size="1" style="width:60px"  tabindex="46" onchange="selFromStn4_Onchange()">
											</select>
										</td>
										<td>
											<select name="selToStn4" id="selToStn4" size="1" style="width:60px"  tabindex="47" onchange="selToStn4_Onchange()">
											</select>
										</td>
										<td><input type="text" name="txtD_Time4" id="txtD_Time4" maxlength="5" style="width:50px"  tabindex="48" onchange="dataChanged()" onblur="setTimeWithColon('txtD_Time4', document.forms[0].txtD_Time4.value)" align="middle"></td>
										<td>
										<select name="selD_Day4" id="selD_Day4" size="1" style="width:40px"  tabindex="49" onchange="selDaysOnchange()" title="0 = same day +n = No. of days from Flight Date -n = No.of days before flight date">
										</select>
										</td>
										<td><input type="text" name="txtA_Time4" id="txtA_Time4" maxlength="5" style="width:50px"  tabindex="50" onchange="dataChanged()" onblur="setTimeWithColon('txtA_Time4', document.forms[0].txtA_Time4.value)" align="middle"></td>
										<td>
										<select name="selA_Day4" id="selA_Day4" size="1" style="width:40px"  tabindex="51" onchange="selDaysOnchange()" title="0 = same day +n = No. of days from Flight Date -n = No.of days before flight date">
										</select>
										</td>						
										<td><font><span id="spnDuration4" ></span></font></td>
										<td><font><span id="spnError4"></span></font></td>
									</tr>
									<tr>
										<td><font>5.</font></td>
										<td>
											<select name="selFromStn5" id="selFromStn5" size="1"  style="width:60px" tabindex="52" onchange="selFromStn5_Onchange()">								
											</select>
										</td>
										<td>
											<select name="selToStn5" id="selToStn5" size="1" style="width:60px"  tabindex="53" onchange="selToStn5_Onchange()" >
											</select>
										</td>
										<td><input type="text" name="txtD_Time5" id="txtD_Time5" maxlength="5" style="width:50px"  tabindex="54" onchange="dataChanged()" onblur="setTimeWithColon('txtD_Time5',document.forms[0].txtD_Time5.value)" align="middle"></td>
										<td>
										<select name="selD_Day5" id="selD_Day5" size="1" style="width:40px"  tabindex="55" onchange="selDaysOnchange()" title="0 = same day +n = No. of days from Flight Date -n = No.of days before flight date">
										</select>
										</td>
										<td><input type="text" name="txtA_Time5" id="txtA_Time5" maxlength="5" style="width:50px"  tabindex="56" onchange="dataChanged()" onblur="setTimeWithColon('txtA_Time5',document.forms[0].txtA_Time5.value)" align="middle"></td>
										<td>
										<select name="selA_Day5" id="selA_Day5" size="1" style="width:40px"  tabindex="57" onchange="selDaysOnchange()" title="0 = same day +n = No. of days from Flight Date -n = No.of days before flight date">
										</select>
										</td>						
										<td><font><span id="spnDuration5"></span></font></td>
										<td><font><span id="spnError5"></span></font></td>
									</tr>					
									<tr>
										<td colspan="18">
											<input type="button" 
											name="btnValidseg" id="btnValidseg" value="Edit Segment" class="Button"  style="width:110px;" onclick="ValidSegments()" tabindex="58">
											<u:hasPrivilege privilegeId="sys.mas.airport.term">
												<input type="button" name="btnValidTerminal" id="btnValidTerminal" value="Edit Terminals" class="Button" style="width:110px;" onclick="validTerminal()" tabIndex="59">
											</u:hasPrivilege>
										</td>
									</tr>
								</table>

								<!-- CordShare Start -->
								
								<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" id="Table3">
						  				<tbody>
								 <tr>
							  			  <td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td>
							  	  <td width="442" height="20" class="FormHeadBackGround"><img src="../../images//bullet_small_no_cache.gif">
																	<font class="FormHeader"> CodeShare MC Flight</font></td>
							    	<td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td> 
							  </tr>
								  <tr>
							 	   <td></td><td width="100%">
									<table cellspacing="0" cellpadding="2" border="0" align="center" width="100%">
												<tbody><tr>
													<td width="22%">
														<font>Carrier Code</font><br>
														<select name="carrier" id="carrier" size="1" style="width:105;" tabindex="19">	
												<option></option>							
												<c:out value="${requestScope.sesCodeShareListData}" escapeXml="false" />
													</select>
													</td>
													<td width="22%">
													<td width="22%">
														<font>Flight No</font><br>
														<input type="text" class="rightAlign portSel" id="flightnum" style="text-transform: uppercase;" >
												
													</td><td width="22%">
														<input type="button" class="iconBtn addItem" value=" + " id="add_CodeShare"  onclick="addCodeShareFlight()">
													</td>
												</tr>
												<tr>
													<tr>
														<td colspan="4">
															<select size="10" id="sel_CodeShare" style="height:75px;width:98%" multiple="multiple"></select>
														</td>
														<td valign="top"><input type="button" id="del_CodeShare" value=" - " class="iconBtn removeItem" onclick="removeFromList()"></td>
													</tr>
											</tbody></table></td>
    
									    <td></td>
									   </tr>
									  </tbody></table>
									  
								<!-- CordShare End -->
								<!--  right Pane End -->
							</td>
						</tr>
						<tr >
							<td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td>
							<td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
							<td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td>
							<td width="7" height="12"></td>							
							<td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td>
							<td colspan="6" height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
							<td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td>
						</tr>
						<tr>
							<td style="height:2px;" colspan="8">				
							</td>
						</tr>
						<tr>
							<td colspan="12">
								<%@ include file="../common/IncludeFormTopFrameLess.jsp"%>
									<table width="100%" border="0" cellpadding="0">
										<tr>
											<td>
												<input type="button" id="btnClose" value="Close" class="Button" onclick="closeClick()">								
												<input type="button" id="btnReset"  name="btnReset" value="Reset" class="Button" onclick="resetClick()" >
											</td>
											<td align="right">
												<input type="button" id="btnPubMessages" name="btnPubMessages" value="Published Messages" class="Button" onclick="viewEnterDatesClick('PUB_MSG')" tabindex="67" style="width:150px">
												<input type="button" id="btnMsgHistory" name="btnMsgHistory" value="Message History" class="Button" onclick="viewEnterDatesClick('IN_MSG')" tabindex="67" style="width:110px">
												<input type="button" id="btnSave"  name="btnSave"value="Save" class="Button" onclick="SaveData()" >
											</td>
										</tr>
									</table>
								 <%@ include file="../common/IncludeFormBottom.jsp"%>
							</td>
						</tr>
					</table>  
					<!-- AA001_03 End here -->						
				</td>
			</tr>
		</table>
		
		<input type="hidden" name="hdnVersion" id="hdnVersion"  value=""/>	
		<input type="hidden" name="hdnMode" id="hdnMode"  value="<%=request.getParameter("strMode")%>"/>
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">	
		<input type="hidden" name="hdnCurrentDate" id="hdnCurrentDate" value="<%=CurrentDate %>">
		<input type="hidden" name="hdnTimeMode" id="hdnTimeMode" value=""/>
		<input type="hidden" name="hdnScheduleId" id="hdnScheduleId" value="<%=request.getParameter("strSchdId")%>"/>
		<input type="hidden" name="hdnSelectedSchedules" id="hdnSelectedSchedules" value=""/>
		<input type="hidden" name="hdnArrivalStn" id="hdnArrivalStn" value=""/>
		<input type="hidden" name="hdnDepartStn" id="hdnDepartStn" value=""/>
		<input type="hidden" name="hdnLegArray" id="hdnLegArray" value=""/>
		<input type="hidden" name="hdnSegArray" id="hdnSegArray" value=""/>
		<input type="hidden" name="hdnTerminalArray" id="hdnTerminalArray" value=""/>
		<input type="hidden" name="hdnOverLapSeg" id="hdnOverLapSeg" value=""/>
		<input type="hidden" name="hdnOverLapSegId" id="hdnOverLapSegId" value=""/>
		<input type="hidden" name="hdnOverLapSegSchedId" id="hdnOverLapSegSchedId" value=""/>
		<input type="hidden" name="hdnOverlapSchID" id="hdnOverlapSchID" value=""/>	
		<input type="hidden" name="hdnStatus" id="hdnStatus" value=""/>
		<input type="hidden" name="hdnBulidStatus" id="hdnBulidStatus" value=""/>
		<input type="hidden" name="hdnGridRow" id="hdnGridRow" value=""/>

		<input type="hidden" name="hdnCancelAlert" id="hdnCancelAlert" value=""/>	
		<input type="hidden" name="hdnReSchedAlert" id="hdnReSchedAlert" value=""/>	
		<input type="hidden" name="hdnCancelEmail" id="hdnCancelEmail" value=""/>	
		<input type="hidden" name="hdnReSchedEmail" id="hdnReSchedEmail" value=""/>	
		<input type="hidden" name="hdnRescheduleAll" id="hdnRescheduleAll" value=""/>
		<input type="hidden" name="hdnEmailTo" id="hdnEmailTo" value=""/>
		<input type="hidden" name="hdnEmailSubject" id="hdnEmailSubject" value=""/>
		<input type="hidden" name="hdnEmailContent" id="hdnEmailContent" value=""/>	
		<input type="hidden" name="hdnReprotect" id="hdnReprotect" value="false"/>	
		<input type="hidden" name="hdnGDSPublishing" id="hdnGDSPublishing" />
		<input type="hidden" name="hdnViewModeOnly"  id="hdnViewModeOnly">		
		
		<input type="hidden" name="hdnSMSAlert" id="hdnSMSAlert" value=""/>	
		<input type="hidden" name="hdnEmailAlert" id="hdnEmailAlert" value=""/>	
		<input type="hidden" name="hdnSmsCnf" id="hdnSmsCnf" value=""/>	
		<input type="hidden" name="hdnSmsOnH" id="hdnSmsOnH" value=""/>
		<input type="hidden" name="hdnSMSAlert2" id="hdnSMSAlert2" value=""/>	
		<input type="hidden" name="hdnEmailAlert2" id="hdnEmailAlert2" value=""/>
		<input type="hidden" name="hndCodeShare" id="hndCodeShare" value=""/>
		<input type="hidden" name="hndCsOCCarrierCode" id="hndCsOCCarrierCode" value=""/>
		<input type="hidden" name="hndCsOCFlightNo" id="hndCsOCFlightNo" value=""/>
		<input type="hidden" name="hdnHaveMCFlight" id="hdnHaveMCFlight" />
		<input type="hidden" name="hdnBuildDefaultInventory" id="hdnBuildDefaultInventory" value=""/>	
		<input type="hidden" name="hdnAuditType" id="hdnAuditType" value=""/>
		
		<input type="text" id="fromDate" name="fromDate" style="display: none;">
		<input type="text" id="toDate" name="toDate" style="display: none;">		
		
	</form>
	
	
			<div id="popup"  style="display:none;" title="Select the Date Range">
				<div>
					<table id="hitoryDuration">
						<tr>
							<td><label>From Date:</label></td>
							<td title="From Date Here"><input type="text" id="fromDatePicker" name="fromDatePicker" onblur="settingValidation('fromDatePicker');"></td>
							<td>
								<font class="mandatory"> &nbsp;* </font>
							</td>
						</tr>
						<tr>
							<td><label>To Date:</label></td>
							<td title="To Date Here"><input type="text" id="toDatePicker" name="toDatePicker" onblur="settingValidation('toDatePicker');"></td>
							<td>
								<font class="mandatory"> &nbsp;* </font>
							</td>
						</tr>
						<tr>
							<td><input type="button" id="btnCancel" value="Cancel" onclick="$('#popup').dialog('close');" class="Button"></td>
							<td><input type="button" id="btnContinue" value="Continue" onclick="viewHistoryClick()" class="Button"></td>
						</tr>
					</table>
				</div>
			</div>	
	
	
	<script src="../../js/flightschedule/FlightSchedule.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  	<script src="../../js/flightschedule/FlightScheduleGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript">
		<!--
			var objProgressCheck = setInterval("ClearProgressbar()", 300);
			function ClearProgressbar() {
		    	if (objDG.loaded) {
					clearTimeout(objProgressCheck);
					top[2].HideProgress();
				}
			}
	    
	   	//-->
	</script>	
	<div id="popUp" class="myPopup"></div>								
  </body>
</html>