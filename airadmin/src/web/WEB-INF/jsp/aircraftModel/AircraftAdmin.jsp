<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	    <title>Template Page</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="-1">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
		<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
		
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../../js/aircraftModel/aircraftValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript">
			var strVar = "";
			strVar = <c:out value="${requestScope.reqCabinClasses}" escapeXml="false" />
			var arrClass = strVar.split(",");
		</script>
		<style type="text/css">
        span.f1 { display: block; float: left; clear: left; width: 60px; }
    	
    	</style>
		
 	</head>
 	<body class="tabBGColor" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="no" 
		onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
	  
		<form name="frmPage" id="frmPage" action="showAircraftModel.action" method="post">
			<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">
		   	<script type="text/javascript">
				<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
				<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
				<c:out value="${requestScope.reqFormData}" escapeXml="false" />
	
				<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
				<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
				<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />	
				<c:out value="${requestScope.isEnableDowngrade}" escapeXml="false" />
				<c:out value="${requestScope.isNeedChecked}" escapeXml="false" />
				<c:out value="${requestScope.isIATAAircraftTypeMandatory}" escapeXml="false" />
			</script>

			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Aircraft Model<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									  <span id="spnAircrafts"></span> 
								</td>
							</tr>
							<tr>
								<td>
								  	<u:hasPrivilege privilegeId="sys.mas.airmodel.add">
										<input name="btnAdd" type="button" id="btnAdd" class="Button" value="Add" onClick="addClick()" >
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.mas.airmodel.edit">
										<input name="btnEdit" type="button" id="btnEdit" class="Button" value="Edit" onClick="editClick()" >
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.mas.airmodel.delete">
										<input name="btnDelete" type="button" id="btnDelete" class="Button" value="Delete" onClick="deleteClick()" >
									</u:hasPrivilege>	
								</td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td><%@ include file="../common/IncludeFormTop.jsp"%>Add/Modify Aircraft Model<%@ include file="../common/IncludeFormHD.jsp"%>
						
						<table width="98%"border="0" cellpadding="0" cellspacing="2" align="center">
							<tr>
								<td width="20%"><font>Fleet/Model</font></td>
								<td>
									<table width="100%"  border="0" cellspacing="0" cellpadding="0">
		                                  <tr>
												<td width="29%"><input name="txtFleet" type="text" id="txtFleet" size="10" onKeyUp="codePress()" onKeyPress="codePress()" class="UCase" maxlength="20" ><font class="mandatory">&nbsp;*</font></td>
												<td width="16%"><font>IATA Aircraft Type</font></td>
												<td width="55%"><input name="iataAircraftType" type="text" id="iataAircraftType" size="15" onKeyUp="descPress(this)" onKeyPress="descPress(this)" maxlength="5" >
												<font name="iataAircraftTypeMand" id="iataAircraftTypeMand"  class="mandatory">&nbsp;*</font></td>
		                                  </tr>
	                                </table>
								</td>
							</tr>
							<tr>
								<td><font>Model Description</font></td>
								<td>
									<table width="100%"  border="0" cellspacing="0" cellpadding="0">
	                                  <tr>
										<td width="29%"><input name="txtDesc" type="text" id="txtDesc" size="40" onKeyUp="descPress(this)" onKeyPress="descPress(this)" maxlength="30" onblur="copyDescription()" ><font class="mandatory">&nbsp;*</font></td>
										<td width="21%"><font>Default IATA Aircraft</font></td>
										<td width="50%"><input name="defaultIataAircraft" type="checkbox" id="defaultIataAircraft"></td>
	                                  </tr>
	                                </table>
                                </td>
								
							</tr>
							<tr>
								<td><font>Itinerary Description</font></td>
								<td>
									<table width="100%"  border="0" cellspacing="0" cellpadding="0">
	                                  <tr>
										<td width="29%"><input name="txtItinDesc" type="text" id="txtItinDesc" size="40" onKeyUp="descPress(this)" onKeyPress="descPress(this)" maxlength="255" ><font class="mandatory">&nbsp;*</font></td>
	                                  </tr>
	                                </table>
                                </td>								
							</tr>
							<tr>
								<td><font>Aircaraft Type Code</font></td>
								<td>
									<table width="100%"  border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="35%">
												<select name="selAircraftTypeCode" size="1" id="selAircraftTypeCode" style="width:150px">
													<c:out value="${requestScope.reqAircraftTypeCodesAll}" escapeXml="false"/>
												</select>
												<font class="mandatory">&nbsp;*</font>
											</td>
										</tr>
										
									</table>
								</td>
							</tr>
							<tr>
								<td><font>Active</font></td>
								<td>
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td align="left" width="29%">
												<input name="chkStatus" type="checkbox" id="chkStatus" onFocus="objOnFocus()" onClick="statusChanged()"  >
											</td>
											<td width="11%"><font>SSR Template</font></td>
											<td width="60%">
												<select name="selSSRTemplate" size="1" id="selSSRTemplate" style="width:150px">		
													<c:out value="${requestScope.reqSSRTempl}" escapeXml="false" />
												</select>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top">
									<font>Capacity</font>
								</td>
								<td align="left"><span id="spnCOSs"></span></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<table>
										<tr>
											<td><font>Class of Service</font></td>
											<td>
												<select  id="selClassOfService" name="selClassOfService" size="1" style="width:100px"  onChange="statusChanged()">
													<c:out  value="${requestScope.reqCabinClassList}" escapeXml="false" />
												</select>
											</td>
											<td>									
												<input name="btnAddCOS" type="button" id="btnAddCOS" class="Button" value="Add" onClick="addByClassOfService()" >
												<input name="btnDeleteCOS" type="button" id="btnDeleteCOS" class="Button" value="Delete" onClick="deleteByClassOfService()" >
											</td>
										</tr>
									</table>																		
								</td>
							</tr>	
							<tr>
								
								<td align="left" colspan="2"><span id="spnFlights" width="150px" ></span></td>
							</tr>						
							<tr>
				                <td>									
									<input name="btnClose" type="button" id="btnClose" class="Button" value="Close" onclick="closeClick()">
					                <input name="btnReset" type="button" class="Button" id="btnReset" value="Reset" onClick="resetClick()" >
					            </td>
					            <td align="right" valign="bottom">									
									<input name="btnSave" type="button" class="Button" id="btnSave" value="Save" onClick="saveClick()" >
				  				</td>
				             </tr>
				         </table>			  
						<!--  Left Pane End -->
					  <%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
		
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="Mode">
		<input type="hidden" name="hdnModel" id="hdnModel" value="">
		<input type="hidden" name="hdnMainMode" id="hdnMainMode" value="">
		<input type="hidden" name="hdnVersion" id="hdnVersion" value="">
		<input type="hidden" name="hdnCabinClasses" id="hdnCabinClasses" value="">
		<input type="hidden" name="hdnInfantCabinClasses" id="hdnInfantCabinClasses" value="">
		<input type="hidden" name="hdnCOSCapacities" id="hdnCOSCapacities" value="">
  </form>
	<script src="../../js/aircraftModel/AircraftCOSGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  	<script src="../../js/aircraftModel/AircraftAdminGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </body>
  <script type="text/javascript">
  	<!--
  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
  	function ClearProgressbar(){  		
  		if (typeof(objDG) == "object"){
	  		if (objDG.loaded){
	  			clearTimeout(objProgressCheck);
	  			top[2].HideProgress();
	  		}
	  	}	  	
  	}
  	 
  	//-->
  </script>
</html>