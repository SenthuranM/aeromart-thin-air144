<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Admin Fee Agents</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">

	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>		
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

	<script src="../../js/adminfeeagent/AdminFeeAgentValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </head>

  <body class="tabBGColor" scroll="no" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" ondrag='return false'
  	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">

  <form method="post" action="showAdminFeeAgent.action">
  	<script type="text/javascript">
		<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
		<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />		
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
		<c:out value="${requestScope.reqFormData}" escapeXml="false" />
		<c:out value="${requestScope.reqFormFieldValues}" escapeXml="false" />		

		<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
		<c:out value="${requestScope.reqOperationUIMode}" escapeXml="false" />
		<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
		<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />		

	</script>

			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Search Agents<%@ include file="../common/IncludeFormHD.jsp"%>					  
						<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table2">
							<tr>
								<td width="10%"><font>Agents</font></td>
								<td width="18%">
									<input name="txtAdminFeeAgent" type="text" id="txtAdminFeeAgent" maxlength="20" size="15"/>
	                            </td>

								<td  width="11%" align="right"><input name="btnSearch" type="button" class="button" id="btnSearch" onClick="searchAgent()" value="Search"></td>
								<td width="61%"></td>
							</tr>
					  	</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>		
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Agents<%@ include file="../common/IncludeFormHD.jsp"%>
							<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
							<td width="30%">
							<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table20" align="left">
								<tr>
									<td>
									  <span id="spnAgents"></span>
									</td>
								</tr>
							</table>
							</td>
							<td width="10%">
							<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table21" align="left">
								<tr>
									<td>
									</td>
								</tr>
								<tr>
									<td>
									  <input type="button" id="btnAddAgent" class="Button" value=">"  onclick="addToList()">	
									</td>
								</tr>
								<tr>
									<td>
									  <input type="button" id="btnRemoveAgent" class="Button" value="<"  onclick="removeFromList()">	
									</td>
								</tr>
								<tr>
									<td>
									</td>
								</tr>
							</table>
							</td>
							<td width="30%">
							<table width="100%"  height="100%" border="0" cellpadding="0" cellspacing="2" ID="Table22" align="left">
								<td>
									<select multiple style="width:100%; height:150px;" class="ListBox" id="selAgents">
										<c:out value="${requestScope.reqAdminFeeAgentList}" escapeXml="false" />
									</select>
								</td>
							</table>
							</td>
							<td width="30%">
							<table width="100%"  height="100%" border="0" cellpadding="0" cellspacing="2" ID="Table23" align="left">
								<tr>
									<td>
										<select id="sel_Admin_Fee_Agent_Initial" name="sel_Admin_Fee_Agent_Initial" style="width: 0; height: 0; display:none;" multiple >
											<c:out value="${requestScope.reqAdminFeeAgentList}" escapeXml="false" />
										</select>
									</td>
								</tr>
							</table>
							</td>
							</tr>
							<tr> 
										<td style="height:44px;" colspan="4" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td>
													<input type="button" id="btnClose" class="Button" value="Close"  onclick="top[1].objTMenu.tabRemove(screenId)">	
												 </td>
												 <td align="right">
													<input name="btnSave" type="button" class="Button" id="btnSave" onClick="saveAgent()" value="Save">
												 </td>
											</tr>
										</table>
									  	</td>
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>

		<input type="hidden" name="hdnVersion" id="hdnVersion"/>	
		<input type="hidden" name="hdnGridRow" id="hdnGridRow"/>
		<input type="hidden" name="hdnMode" id="hdnMode"/>	
		<input type="hidden" name="hdnUIMode" id="hdnUIMode"/>
		<input type="hidden" name="hdnRecNo" id="hdnRecNo">	
		<input type="hidden" name="hdnAddAgentCodes" id="hdnAddAgentCodes"/>
		<input type="hidden" name="hdnDeleteAgentCodes" id="hdnDeleteAgentCodes"/>
	</form>	
	<script src="../../js/adminfeeagent/AdminFeeAgentAdminGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </body>

  <script type="text/javascript">
  	<!--
  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
  	function ClearProgressbar(){  		
  		if (typeof(objDG) == "object"){
	  		if (objDG.loaded){
	  			clearTimeout(objProgressCheck);
	  			top[2].HideProgress();
	  		}
	  	}	  	
  	}
  	 
  	//-->
  </script>

</html>