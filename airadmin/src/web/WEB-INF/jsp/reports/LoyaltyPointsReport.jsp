<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Loyalty Points Report</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">	
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";	
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
</script>
</head>
<body class="tabBGColor" onunload="beforeUnload()" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" scroll="no">
<form action="" method="post" id="frmLoyaltyPointsReport"
	name="frmLoyaltyPointsReport">
	
	<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">	
		
		<tr>
			
      <td> 
        <%@ include file="../common/IncludeFormTop.jsp"%>Loyalty Points<%@ include file="../common/IncludeFormHD.jsp"%>
        <table width="100%" border="0" cellpadding="0" cellspacing="2"
					ID="tblHead">	
																			
					
				<tr>				
					<td><font class="fntBold">Output Option</font></td>							
				</tr>
				<tr>
					<td><table  width="100%" border="0" cellpadding="0" cellspacing="2"><tr>
					<td width="15%"><input type="radio" name="radReportOption" id="radReportOption"
						value="HTML" class="noBorder"  checked><font>HTML</font></td>
					<td width="15%"><input type="radio" name="radReportOption" id="radReportOption" 
						value="PDF" class="noBorder"><font>PDF</font></td>
					<td width="15%"><input type="radio" name="radReportOption" id="radReportOption"
						value="EXCEL" class="noBorder"><font>EXCEL</font></td>					
					<td><input type="radio" name="radReportOption" id="radReportOption" 
						value="CSV" class="noBorder"><font>CSV</font></td>
				</tr>									
				</table>
				<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
				</td>									
			</tr>
			<tr><td>&nbsp;</td></tr>	
			<tr>				
				<td width=90%><input type="button" id="btnClose"  name="btnClose" value="Close" class="Button" onclick="closeClick()"></td>
				<td width=10% align="right"><input type="button" id="btnView"  name="btnView" value="View" class="Button" onclick="viewClick()"></td>	
			</tr>		
		</table>
		<%@ include file="../common/IncludeFormBottom.jsp"%></td>
	</tr>
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
</table>
	<input type="hidden" name="hdnMode" id="hdnMode">
	<input type="hidden" name="hdnReportView" id="hdnReportView">
	<input type="hidden" name="hdnLive" id="hdnLive">
	
</form>
<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
</body>
<script src="../../js/reports/LoyaltyPointsReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
    
   //-->
  </script>
</html>