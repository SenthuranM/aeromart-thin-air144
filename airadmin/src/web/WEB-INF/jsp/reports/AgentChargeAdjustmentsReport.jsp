
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<link href="../../themes/default/css/ui.all_no_cache.css" rel="stylesheet" type="text/css" />
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/v2/schedrept/schedrept.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

  </head>
  <body class="tabBGColor" onload="load()" onbeforeunload="beforeUnload()" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" scroll="no">
  	<form name="frmOnHoldPag" id="frmOnHoldPag" action="" method="post">
		<script type="text/javascript">
			var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
			var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			var displayAgencyMode = 0;
			<c:out value="${requestScope.reqHtmlDetails}" escapeXml="false" />	
			
		</script>
		<table width="99%" border="0" cellpadding="0" cellspacing="0">				
								<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Agent Charge Adjustments<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
					  		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>							
							<tr>
							<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="2">
									<tr>
										<td><font class="fntBold">Date Range</font></td>				
									</tr>
									<tr>
										<td>
										<table width="70%" border="0" cellpadding="0" cellspacing="2">	
											<tr>
												<td width="17%" align="left"><font>From </font></td>
												<td width="30%"><input name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFromDate')">
													<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
												<td width="6%"><font>To </font></td>
												<td><input name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtToDate')">
													<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
											</tr>
										</table>
										</td>
									</tr>
									
								<td>
									<div id="divAgencies">
										<table>
											<tr>
												<td>
													<font>Agencies</font>
													<select id="selAgencies" size="1" name="selAgencies" onClick="clickAgencies()" onChange="changeAgencies()" style="width:76px">
														<option value=""></option>
														<option value="All">All</option>
														<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />					
													</select>
												</td>
												
												<td><font>&nbsp;</font></td>
												<td>
													<input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()">
												</td>
											</tr>
										
										</table>
									</div>
								</td>
							</tr>	
								<tr>		
									<td width="40%">
										<font>Type:&nbsp;&nbsp;</font>
		  								<input name="chkTypeR" id="chkTypeR" maxlength="1" type="checkbox" value="R" title="Reservation" class="noBorder">
		  								<font>R</font>
	  									<input name="chkTypeC" id="chkTypeC" maxlength="1" type="checkbox" value="C" title="Credit" class="noBorder">
		  								<font>C</font>
		  								<input name="chkTypeD" id="chkTypeD" maxlength="1" type="checkbox" value="D" title="Debit" class="noBorder">
		  								<font>D</font>
		  								<input name="chkTypeRR" id="chkTypeRR" maxlength="1" type="checkbox" value="RR" title="Reservation Refund" class="noBorder">
		  								<font>RR</font>
		  								<input name="chkTypeA" id="chkTypeA" maxlength="1" type="checkbox" value="A" title="Cash" class="noBorder">
		  								<font>CH</font>
		  								<input name="chkTypeQ" id="chkTypeQ" maxlength="1" type="checkbox" value="Q" title="Cheque" class="noBorder">
		  								<font>CQ</font>
		  								<input name="chkTypeRV" id="chkTypeRV" maxlength="1" type="checkbox" value="RV" title="Adjustment" class="noBorder">
		  								<font>A</font>
		  								<input name="chkTypeBSP" id="chkTypeBSP" maxlength="1" type="checkbox" value="BSP" title="BSP Settlement" class="noBorder">
		  								<font>BSP</font>
		  								<input name="chkTypeCG" id="chkTypeCG" maxlength="1" type="checkbox" value="CG" title="Commision Granted" class="noBorder">
  										<font>CG</font>
  										<input name="chkTypeCR" id="chkTypeCR" maxlength="1" type="checkbox" value="CR" title="Commision Removed" class="noBorder">
  										<font>CR</font>
	  							</td>
							</tr>					
							<tr>
								<td>
									<div id="divAgents">
										<table>
											<tr>
												<td>
													<font class="fntBold">Agents</font>
												</td>
											</tr>
											<tr><td>
											<table width="62%" border="0" cellpadding="0" cellspacing="2">	
											<tr>
												<td valign="top" width="2%"><span id="spn1"></span></td>
												<td altgn="left" valign="bottom"> <font class="mandatory"><b>*</b></font></td></tr>
												</table></td>
											</tr>
											<tr>
												<td align="right" valign="bottom" height="30px">
													<input name="btnLoadUsers" type="button" class="Button" id="btnLoadUsers" value=" Load Users " onClick="loadUsersClick()">
												</td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div id="divUsers">
										<table>
											<tr>
												<td>
													<font class="fntBold">Users</font>
												</td>
											</tr>
											<tr><td>
											<table width="62%" border="0" cellpadding="0" cellspacing="2">	
											<tr>
												<td valign="top" width="2%"><span id="spn2"></span></td>
												<td altgn="left" valign="bottom"> </td></tr>
												</table></td>
											</tr>
											
										</table>
									</div>
								</td>
							</tr>

									<tr>				
										<td><font class="fntBold">Output Option</font></td>							
									</tr>
									<tr>
										<td>
											<table width="70%" border="0" cellpadding="0" cellspacing="2">
												<tr>
												<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionHTML"
													value="HTML" class="noBorder" checked><font>HTML</font></td>
												<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" 
													value="PDF"  class="noBorder"><font>PDF</font></td>
												<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
													value="EXCEL"  class="noBorder"><font>EXCEL</font></td>
												<td width="40%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
													value="CSV"  class="noBorder"><font>CSV</font></td>
												</tr>									
											</table>
										</td>									
									</tr>
									<tr><td>&nbsp;</td></tr>
									<tr>
										<td>
											<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
										</td>
										<td align="right">
											<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
									
										</td>
										
										<td align="right">
											<div id="divSchedFrom"></div>
											<input name="btnSchedule" type="button" class="Button" id="btnSchedule" value="Schedule" onClick="scheduleClick()">
										</td>
									</tr>
								</table>
								</td>
							</tr>
							</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>					  		
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		</table>
		<script type="text/javascript">
				<c:out value="${requestScope.reqGSAList}" escapeXml="false" />				
				<c:out value="${requestScope.reqAgentUserList}" escapeXml="false" />
				if (displayAgencyMode == 1 ) {
					document.getElementById('divAgencies').style.display= 'block';
					document.getElementById('divAgents').style.display= 'block';
				} else if (displayAgencyMode == 2 ) {
					document.getElementById('divAgents').style.display= 'block';
					document.getElementById('divAgencies').style.display= 'none';
				} else {
					document.getElementById('divAgencies').style.display= 'none';
					document.getElementById('divAgents').style.display= 'none';
				}				
		</script>	

		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnModel" id="hdnModel" value="">
		<input type="hidden" name="hdnVersion" id="hdnVersion" value="">
		<input type="hidden" name="hdnLive" id="hdnLive" value="">
		<input type="hidden" name="hdnAgents" id="hdnAgents" value="">
		<input type="hidden" name="hdnUsers" id="hdnUsers" value="">
		<input type="hidden" name="hdnRptType" id="hdnRptType" value="">
		<input type="hidden" name="hdnNominals id="hdnNominals" value="">
		
	</form>
  </body>
<script src="../../js/reports/validateAgentChargeAdjs.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
    
   //-->
  </script>
</html>
