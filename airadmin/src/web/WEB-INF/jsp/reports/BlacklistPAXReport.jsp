<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>BlackListed Passenges</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css"
	href="../../css/Style_no_cache.css">
<script
	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

</head>
<body class="tabBGColor" onload="load()" onbeforeunload="beforeUnload()"
	onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false"
	onkeydown="return Body_onKeyDown(event)" scroll="no">
	<form name="frmBlPaxPag" id="frmBlPaxPag" action="" method="post">
		<script type="text/javascript">
			var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
			var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			var displayAgencyMode = 0;
			<c:out value="${requestScope.reqHtmlDetails}" escapeXml="false" />
		</script>
		<table width="99%" border="0" cellpadding="0" cellspacing="0">
			<tbody>
				<tr>
					<td><font class="fntSmall">Field(s) highlighted with <font
							class="mandatory"><b>*</b></font> are mandatory
					</font></td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0"
							align="center" id="Table4">
							<tbody>
								<tr>
									<td width="10" height="20"><img
										src="../../images/form_top_left_corner_no_cache.gif"></td>
									<td height="20" class="FormHeadBackGround"><img
										src="../../images/bullet_small_no_cache.gif"> <font
										class="FormHeader">Blacklisted Passengers </font></td>
									<td width="11" height="20"><img
										src="../../images/form_top_right_corner_no_cache.gif"></td>
								</tr>
								<tr>
									<td class="FormBackGround"></td>
									<td class="FormBackGround" valign="top">
										<table width="100%" border="0" cellpadding="0" cellspacing="2"
											id="tblReservations">
											<tbody>
												<tr>
													<td><font class="fntSmall">&nbsp;</font></td>
												</tr>
												<tr>
													<td>
														<table width="100%" border="0" cellpadding="3"
															cellspacing="2">
															<tbody>
																<tr>
																	<td width="5%"><font>Flight Number </font></td>
																	<td width="15%"><input name="txtFlightNumber"
																		type="text" id="txtFlightNumber" size="7"
																		style="width: 75px;" class="UCase" maxlength="7"><!-- <font
																		class="mandatory"><b>*</b></font> --></td>
																	<td>&nbsp;</td></tr>
																	<tr><td width="6%" align="left"><font>Start
																			Date </font></td>
																	<td width="20%"><input name="txtFromDate"
																		type="text" id="txtFromDate" size="10"
																		style="width: 75px;" maxlength="10"
																		onblur="dateChk('txtFromDate')"> <a
																		href="javascript:void(0)"
																		onclick="LoadCalendar(0,event); return false;"
																		title="Date From"><img
																			src="../../images/calendar_no_cache.gif" border="0"></a><font
																		class="mandatory"><b>*</b></font></td></tr>
																	<tr><td width="6%"><font>End Date </font></td>
																	<td><input name="txtToDate" type="text"
																		id="txtToDate" size="10" style="width: 75px;"
																		maxlength="10" onblur="dateChk('txtToDate')">
																		<a href="javascript:void(0)"
																		onclick="LoadCalendar(1,event); return false;"
																		title="Date To"><img
																			src="../../images/calendar_no_cache.gif" border="0"></a><font
																		class="mandatory"><b>*</b></font></td></tr>
																	<tr><td><font>Blacklist Type</font></td>
																	<td><select name="blTypeSearch" id="blTypeSearch"
																		style="width: 80px;">
																			<option value="T">Tempory</option>
																			<option value="P">Permanent</option>
																	</select></td>
																</tr>
																<tr>
																	<td colspan="2"><font class="fntBold">Output Option</font></td>
																</tr>
																<tr>
																	<td colspan="2">
																		<table width="100%" border="0" cellpadding="0"
																			cellspacing="2">
																			<tbody>
																				<tr>
																					<td width="25%"><input type="radio"
																						name="radReportOption" id="radReportOptionHTML"
																						value="HTML" class="noBorder" checked=""><font>HTML</font></td>
																					<td></td>
																					<td width="25%"><input type="radio"
																						name="radReportOption" id="radReportOptionPDF"
																						value="PDF" class="noBorder"><font>PDF</font></td>
																					<td></td>
																					<td width="25%"><input type="radio"
																						name="radReportOption" id="radReportOptionEXCEL"
																						value="EXCEL" class="noBorder"><font>EXCEL</font></td>
																					<td></td>
																					<td width="25%"><input type="radio"
																						name="radReportOption" id="radReportOptionEXCEL"
																						value="CSV" class="noBorder"><font>CSV</font></td>
																				</tr>
																			</tbody>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td>&nbsp;</td>
																</tr>
																<tr>
																	<td><input name="btnClose" type="button"
																		class="Button" id="btnClose" value="Close"
																		onclick="closeClick()"></td>
																	<td align="right"><input name="btnView"
																		type="button" class="Button" id="btnView" value="View"
																		onclick="viewClick()"></td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
											</tbody>
										</table>




									</td>
									<td class="FormBackGround"></td>
								</tr>
								<tr>
									<td height="12"><img
										src="../../images/form_bottom_left_corner_no_cache.gif"></td>
									<td height="12" class="FormBackGround"><img
										src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
									<td height="12"><img
										src="../../images/form_bottom_right_corner_no_cache.gif"></td>
								</tr>
							</tbody>
						</table> <span id="spnQuickLaunch"
						style="position: absolute; top: -55px; width: 912px; visibility: hidden; align: right;"></span>
						<script language="javascript"
							src="../../js/common/QuickLaunch.js?relversion=20090501120000"></script>

					</td>
				</tr>
				<tr>
					<td><font class="fntSmall">&nbsp;</font></td>
				</tr>
			</tbody>
		</table>
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnModel" id="hdnModel" value=""> <input
			type="hidden" name="hdnVersion" id="hdnVersion" value=""> <input
			type="hidden" name="hdnLive" id="hdnLive" value=""> <input
			type="hidden" name="hdnAgents" id="hdnAgents" value=""> <input
			type="hidden" name="hdnUsers" id="hdnUsers" value=""> <input
			type="hidden" name="hdnRptType" id="hdnRptType" value="">

	</form>
</body>
<script
	src="../../js/reports/validateBlacklist.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
    
   //-->
  </script>
</html>
