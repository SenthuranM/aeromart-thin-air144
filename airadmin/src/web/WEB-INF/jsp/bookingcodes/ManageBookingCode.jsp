<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	    <title>Template Page</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="-1">
	    
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
		<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
		
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>		
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/bookingcodes/BookingCodeValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

  	</head>
	<body scroll="no"  onkeypress='return Body_onKeyPress(event)' oncontextmenu="return showContextMenu()" onkeydown='return Body_onKeyDown(event)' ondrag='return false' 
  			class="tabBGColor" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">

  		<form method="post" action="showBookingCodes.action" name="frmBookingCodes" id="frmBookingCodes">
  			<script type="text/javascript">
				<c:out value="${requestScope.BookingCode}" escapeXml="false" />
				<c:out value="${requestScope.COS}" escapeXml="false" />
				<c:out value="${requestScope.LogicalCC}" escapeXml="false" />
				<c:out value="${requestScope.BCType}" escapeXml="false" />
				<c:out value="${requestScope.ALLOCType}" escapeXml="false" />
				<c:out value="${requestScope.Status}" escapeXml="false" />

				<c:out value="${requestScope.reqBookingCodes}" escapeXml="false" />
				<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />		
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
				
				var arrFormData = new Array();
				<c:out value="${requestScope.reqFormData}" escapeXml="false" />				
				<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
				<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />
				<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
			</script>
		 
			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">				
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>	
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Search Booking Classes<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table9">
							<tr>
								<td width="10%"><font>Booking Class</font></td>
								<td width="17%">
									<select id="selBookingClass" name="selBookingClass" size="1" style="width:80px" onChange="disableSelections('selBookingClass')">
										<OPTION value="-1">All</OPTION>
										<c:out value="${requestScope.reqBookingCodesList}" escapeXml="false" />	
									</select>
								</td>
								
								<td width="15%"><font>BC Category Type </font></td>
								<td width="20%">
									<select id="selBCType" name="selBCType" size="1" style="width:100px">
										<OPTION value="-1">All</OPTION>
										<c:out value="${requestScope.reqBCTypeList}" escapeXml="false" />	
									</select>
								</td>

								
								<td width="10%">
									<font>Status</font>
								</td>
								<td width="20%">
									<select id="selStatusSearch" name="selStatusSearch" size="1" onChange="disableSelections('selStatusSearch')" >
										<option value="All">All</option>
										<option value="ACT">Active</option>
										<option value="INA">Inactive</option>
									</select>
								</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><font>COS</font></td>
								<td >
									<select id="selCOS" name="selCOS" size="1" style="width:80px" onChange="listLogicalCabinClasses('<c:out value="${requestScope.ccWiseLogicalCabinClassList}"/>', 'selCOS', 'selLCC')">
										<OPTION value="-1">All</OPTION>
										<c:out value="${requestScope.reqClassOfServicesList}" escapeXml="false" />	
									</select>
								</td>
								<td >
									<font>Allocation Type</font>
								</td>
								<td >
									<select id="selAllocSearch" name="selAllocSearch" size="1">
										<option value="-1">All</option>
										<c:out value="${requestScope.reqAllocationTypeList}" escapeXml="false" />
									</select>
								</td>
								<td ><font>Logical CC</font></td>
								<td >
									<select id="selLCC" name="selLCC" size="1" style="width:80px">
										<OPTION value="-1">All</OPTION>
										<c:out value="${requestScope.reqLogicalCabinClassList}" escapeXml="false" />	
									</select>
								</td>
								
								
								<td align="right">	
									<input type="button" id="btnSearch" value="Search" name="btnSearch" class="button" onclick="searchBookingClass()">
								</td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				
				<tr>
					<td>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table9">
							<tr>
								<td width="100%" valign="top">
									<%@ include file="../common/IncludeFormTop.jsp"%>Booking Classes<%@ include file="../common/IncludeFormHD.jsp"%>
									<table width="100%" border="0" cellpadding="0" cellspacing="1" ID="Table9">
										<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
										<tr>
											<td>
												  <span id="spnBkgClass"></span>
											</td>
										</tr>
										<tr>
											<td>
												<u:hasPrivilege privilegeId="plan.invn.bc.add">
													<input type="button" id="btnAdd" name="btnAdd" class="Button" value="Add" onClick="addBookingClass()">
												</u:hasPrivilege>
												<u:hasPrivilege privilegeId="plan.invn.bc.edit">
													<input type="button" id="btnEdit" name="btnEdit" class="Button" value="Edit" onClick="editBookingClass()">
												</u:hasPrivilege>
												<u:hasPrivilege privilegeId="plan.invn.bc.delete">
													<input type="button" id="btnDelete" name="btnDelete" class="Button" value="Delete" onClick="deleteBookingClass()">
												</u:hasPrivilege>
											</td>
										</tr>
									</table>
							  		<%@ include file="../common/IncludeFormBottom.jsp"%>
								</td>
							</tr>
						</table>
					</td>
				</tr>				
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						Add/Modify Booking Classes
						<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="98%"border="0" cellpadding="0" cellspacing="1" align="center">
							<tr>
								<td width="20%"><font>Booking Class</font></td>
								<td width="50%">
									<input name="txtBookingClass" style="width=68px;" type="text" id="txtBookingClass" class="UCase" maxlength="5"  onkeyUp="alphaNumericWithoutSpaceValidate(this); removeWhiteSpaces(this)" onkeyPress="alphaNumericWithoutSpaceValidate(this); removeWhiteSpaces(this)" onChange="required_changed()" onClick="clearError()"><font class="mandatory" >&nbsp;*&nbsp;&nbsp;&nbsp;&nbsp;</font>
									<input name="txtBookingClassDescription" style="width=180px;" type="text" id="txtBookingClassDescription" maxlength="100"  title="Booking Class Description" onkeyUp="alphaNumericValidate(this)" onkeyPress="alphaNumericValidate(this)" onChange="required_changed()" onClick="clearError()"><font class="mandatory" >&nbsp;*</font>
								</td>
								<td width="10%"><font>Active</font></td>
								<td width="20%">
									<input type="checkBox" id="chkStatus" name="chkStatus" class="NoBorder" onClick="required_changed();clearError()">
								</td>
							</tr>
							<tr>
								<td><font>Class of Service</font></td>
								<td>
									<select id="selClassofService" name="selClassofService" size="1" style="width:100px" onChange="listLogicalCabinClasses('<c:out value="${requestScope.ccWiseLogicalCabinClassList}"/>', 'selClassofService', 'selLogicalCC')" onClick="clearError()">
										<option></option>
										<c:out value="${requestScope.reqClassOfServicesList}" escapeXml="false" />
									</select><font class="mandatory">&nbsp;*</font>
									
								</td>
								<td width="10%"><font>Pax Category</font></td>
								<td width="20%">
									<select id="selPaxCat" name="selPaxCat" size="1" style="width:100px">											
										<c:out value="${requestScope.reqPaxCatList}" escapeXml="false" />
									</select>
								</td>								
							</tr>
							
							<tr id="logicalCCData">
								<td><font>Logical Cabin Class</font></td>
								<td>
									<select id="selLogicalCC" name="selLogicalCC" size="1" style="width:100px" onChange="required_changed()" onClick="clearError()">
										<option></option>
										<c:out value="${requestScope.reqLogicalCabinClassList}" escapeXml="false" />
									</select><font class="mandatory">&nbsp;*</font>
									
								</td>
							</tr>

							<tr>
								<td valign="top">
									<font>Standard  Class</font>
								</td>
								<td valign="top">
									<input type="checkBox" id="chkStandardClass" name="chkStandardClass" size="1" onclick="disableNestRank(this);required_changed();clearError()" class="NoBorder">
								</td>
				
							</tr>

							<tr>
								<td valign="top">
									<font>Nest Rank</font>
								</td>
								<td valign="top">
									<input type="text" id="txtNestRank" name="txtNestRank" maxlength="3" style="width=40px;" onkeyUp="positiveWholeNumberValidate(this)" onkeyPress="positiveWholeNumberValidate(this)" onChange="required_changed()" onClick="clearError()"> &nbsp;
									<font>If already existing, insert and shift existing ranks</font> <!--</td> -->
									<input type="checkBox" id="chkNestShift" name="chkNestShift" class="NoBorder" onChange="required_changed()" onClick="clearError()">
								</td>

								<td valign="top" rowspan="3"><font>Rules &amp; Comments</font></td>
								<td valign="top" rowspan="3" ><textarea name="txtRulesComments" id="txtRulesComments" cols="40" rows="3" onkeyUp="validateTextArea(this)" onkeyPress="validateTextArea(this)" onChange="required_changed()" onClick="clearError()"></textarea></td>
							</tr>
							<tr>
								<td valign="top">
									<font>Fixed Class</font>
								</td>
								<td valign="top">
									<input type="checkBox" id="chkFixedClass" name="chkFixedClass" size="1" onclick="disableStandardClass(this);required_changed();clearError()" class="NoBorder">
								</td>
							</tr>
							<tr>
								<td colspan='4'>
									<table width="100%"border="0" cellpadding="0" cellspacing="1" align="center">
										<tr>
											<td valign="top" ><font>Charge Group</font>
											</td>
											<td>
												<span id="spnMDrop"></span>									
											</td>
											<td valign="top" width="10%" align="left"><font>GDS Code</font></td>
											<td align="left" ><select id="selGDSPublishing" name="selGDSPublishing" size="1" style="width:100; height: 50" multiple>
													<c:out value="${requestScope.sesGDSListData}" escapeXml="false" />
												</select></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<!-- FIXME: Programtically set BC Type -->
								<td colspan='4'>
									<table width="100%"border="0" cellpadding="0" cellspacing="1" align="center">
										<tr>
											<td valign="top" width="10%"><font>BC Type</font>
											</td>
											<td valign="top" width="20%" align="left">
												<select name="sltBCType" id="sltBCType" size="1" style="width:150px">
													<option value=""></option>													
													<c:out value="${requestScope.reqBookingClassTypeList}" escapeXml="false" />
												</select><font class="mandatory">&nbsp;*</font>
											</td>
											<td valign="top" width="25%" ><font>Fare Category</font></td>
											<td valign="top" width="17%">
												<select id="selFareCat" name="selFareCat" size="1" style="width:100px">										
													<c:out value="${requestScope.reqFareCatList}" escapeXml="false" />
												</select><font class="mandatory">&nbsp;*</font>
											</td>
											<td  width="17%"><font>Prevent On hold</font></td>
											<td align="left">
												<input type="checkBox" id="chkOnHold" name="chkOnHold" size="1" onclick="required_changed();clearError()" class="NoBorder">
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan='4'>
									<table width="100%"border="0" cellpadding="0" cellspacing="1" align="center">
										<tr>
											<td valign="top" width="16%"><font>Allocation Type</font></td>
											<td valign="top" width="15%">
												<select id="selAllocationType" name="selAllocationType" size="1"  onChange="required_changed()" onClick="clearError()">
													<option value=""></option>
													<c:out value="${requestScope.reqAllocationTypeList}" escapeXml="false" />
												</select><font class="mandatory">&nbsp;*</font>
											</td>
											<td valign="top" width="19%"><font>Seat Release Cut-Over Time</font></td>
											<td valign="top" width="17%"><input type="text" id="txtCutOverTime" name="txtCutOverTime" maxlength="7" style="width:50px;" 
											onBlur="setTimeWithColonWithMax(this, 7)" onChange="required_changed()" onClick="clearError()" onkeyUp="positiveWholeNumberValidate(this)" onkeyPress="positiveWholeNumberValidate(this)" > &nbsp;<font>Hours:Minutes</font></td>
											<td valign="top"  width="17%"><font>Activate Release Time</font></td>
											<td valign="top" align="left"><input type="checkBox" id="chkReleaseTime" name="chkReleaseTime" size="1" onclick="required_changed();clearError()" class="NoBorder"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
				                <td style="height:30px" valign="bottom" colspan="2">
									<input type="button" id="btnClose" class="Button" name="btnClose" value="Close"  onclick="closeClick()">
					                <input name="btnReset" type="button" class="Button" id="btnReset" value="Reset" onClick="resetBookingClass()">					               
					            </td>
					            <td align="right" valign="bottom" colspan="2">
									  <input name="btnSave" type="button" class="Button" id="btnSave" value="Save" onClick="saveBookingClass()">

				  				</td>
				             </tr>
				         </table>			  
						<!--  Left Pane End -->
					  <%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
		
		<input type="hidden" name="hdnRecNo" id="hdnRecNo"  value="1">		
		<input type="hidden" id="hdnMode" name="hdnMode"  value="SAVE"/>
		<input type="hidden" id="hdnVersion" name="hdnVersion"/>
		<input type="hidden" id="hdnPAXType" name="hdnPAXType"/>
		<input type="hidden" id="hdnCCOdes" name="hdnCCOdes"/>
		<input type="hidden" id="hdnRowNum" name="hdnRowNum"/>
		<input type="hidden" name="hdnGroupId" id="hdnGroupId"  value="0">	
		</form>
		<script src="../../js/bookingcodes/BookingCodes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </body>

<script type="text/javascript">
	 <!--
	 var arrGroup1 = new Array('Charge Groups');
	 <c:out value="${requestScope.reqGroupList}" escapeXml="false" />
	var ls = new Listbox('lstAgents', 'lstAssignedAgents', "spnMDrop");
	ls.group1 = arrGroup1;
	ls.list1 = arrData;
	ls.width = "150px";
	ls.height = "77px";
	ls.drawListBox();
//-->
</script>  
  <script type="text/javascript">	
   <!--   
   var objProgressCheck = setInterval("ClearProgressbar()", 300);

   function ClearProgressbar(){  		


		if (typeof(objDG) == "object"){
	  		if (objDG.loaded){
	  			clearTimeout(objProgressCheck);
	  			top[2].HideProgress();
	  		}
	  	}	  	
 	}
   //-->
  </script>
</html>