<%@ page language="java"%>
<%@ include  file="../../common/Directives.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Role Maintenance</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	
	<script  src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script  src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../../js/common/MultiDropDown_2.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script  src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../../js/security/role/RolePrivValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </head>
  <body class="tabBGColor" scroll="no"  onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false' 
  	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
   <form name="frmRoles" id="frmRoles" method="post" action="showRole.action">
	   	<script type="text/javascript">
	   		var crragent = new Array();
			<c:out value="${requestScope.reqRoleHtmlData}" escapeXml="false" />
			var totalNoOfRecords =<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			<c:out value="${requestScope.reqFormData}" escapeXml="false" />
			<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
			<c:out value="${requestScope.reqServiceChannel}" escapeXml="false" />
			<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
			<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />	
			<c:out value="${requestScope.reqCarrierAgent}" escapeXml="false" />		
			<c:out value="${requestScope.reqDefaultAirlineCode}" escapeXml="false" />		
			
		</script>

		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			<tr>
				<td>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="50%" valign="top" style="height:510px">
								<table width="99%" border="0" cellpadding="0" cellspacing="0">
								   <tr>
										 <td>
											<%@ include file="../../common/IncludeFormTop.jsp"%>
												Search Role	&amp; Privilege					
												<%@ include file="../../common/IncludeFormHD.jsp"%>
												<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table2">
													<tr>
														<td width="15%"><font>Role Name</font></td>
														<td>
															<input type="text" id="txtRoleNameSearch"  name="txtRoleNameSearch" maxlength="50" size="30" onkeyUp="alphaNumericValidate(this)" onkeyPress="alphaNumericValidate(this)" >
														</td>
														<td width="15%" align="right">
															<input name="btnSearch" type="button" class="button" id="btnSearch" onClick="searchRolePrivilege()" value="Search">
														</td>
													</tr>
											  </table>
												<%@ include file="../../common/IncludeFormBottom.jsp"%>
											</td>
										</tr>
										<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
										<tr>
											<td valign="top">
												<%@ include file="../../common/IncludeFormTop.jsp"%>
													Roles &amp; Privileges
												<%@ include file="../../common/IncludeFormHD.jsp"%>
											
												<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">
													<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
													<tr>
														<td align="center">
															  <span id="spnRoles"></span> 
														</td>
													</tr>
													<tr>
														<td>
															<u:hasPrivilege privilegeId="sys.security.role.add">
																<input name="btnAdd" type="button" class="Button" id="btnAdd" value="Add" onClick="addRolePrivilege()">
															</u:hasPrivilege>
															<u:hasPrivilege privilegeId="sys.security.role.edit">
																<input name="btnEdit" type="button" class="Button" id="btnEdit" value="Edit" onClick="editRolePrivilege()">
															</u:hasPrivilege>
															<u:hasPrivilege privilegeId="sys.security.role.delete">
																<input name="btnDelete" type="button" class="Button" id="btnDelete" value="Delete" onClick="deleteRolePrivilege()">
															</u:hasPrivilege>
															<u:hasPrivilege privilegeId="sys.security.role.copy">
															<input name="btnCopy" type="button" class="Button" id="btnCopy" value="Copy" onClick="copyRolePrivilege()">
															</u:hasPrivilege>
														</td>
													</tr>
												</table>
												<%@ include file="../../common/IncludeFormBottom.jsp"%>
											</td>
										</tr>
								   </table>
							</td>
							<td width="50%"  valign="top">
								<%@ include file="../../common/IncludeFormTop.jsp"%>
									Add/Modify Roles &amp; Privileges						
								<%@ include file="../../common/IncludeFormHD.jsp"%>
									<table width="100%" border="0" cellpadding="2" cellspacing="2" id="Table2">
												<tr>
												<td>
													<table>
														<tr>
															<td><font>Role ID&nbsp;&nbsp;</font></td>
														  	<td>
																<input name="txtRoleId" type="text" class="InputBorder" id="txtRoleId" align="right" size="8"></td>
															<td><font>Role Name</font></td>
												 			 <td><input name="txtRoleName" type="text" class="InputBorder" id="txtRoleName" size="32"  maxlength="50"  onKeyPress="nameVAalidate()" onKeyUp="nameVAalidate()" onChange="clickChange()"> <font class="mandatory"> &nbsp;* </font> </td>	
														</tr>
													</table>
												</td>											  											  
											  </tr>
											  <tr>
											  	<td>
											  		<table>
														<tr>
															<td><font>Inclusion/Exclusion&nbsp;&nbsp;</font></td>
														  	<td>
																<select size="1" id="selIncExc" name="selIncExc" onchange="setApplicableAirline(this.value);">
																<c:out value="${requestScope.reqIncExc}" escapeXml="false" />		
																</select> <font class="mandatory"> &nbsp;* </font>
															<td><font>Applicable Airline&nbsp;&nbsp;</font></td>
												 			 <td>
												 			 	<select size="1" id="selApplicableAirline" name="selApplicableAirline" >
																<c:out value="${requestScope.reqApplicableAirline}" escapeXml="false" />		
																</select><font class="mandatory"> &nbsp;* </font>
												 			 </td>	
														</tr>
													</table>
											  	</td>
											  </tr>
												<tr>
												<td>
													<table>
														<tr>
															<td valign="top"><font>Remarks</font></td>  
												  			<td colspan="3"><textarea name="txtRemarks" id="txtRemarks" cols="50" rows="1" onKeyPress="validateTA(this,255)" onKeyUp="validateTA(this,255)" onChange="clickChange()"></textarea></td>
															
												  			<td><font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Active</font></td>
												  			<td><font> &nbsp;&nbsp;</font><input name="chkStatus" id="chkStatus" maxlength="1" type="checkbox" value="Active" onChange="clickChange()"></td>
														</tr>
													</table>												
												</td>													
												</tr>
												<tr>
												<td>												
													<table>
														<tr>
															<td valign="top" align="right">
															<u:hasPrivilege privilegeId="sys.security.role.visible.type">
																<font>Visible&nbsp;&nbsp;&nbsp;<br />  Agent Type&nbsp;&nbsp;&nbsp;</font>
															</u:hasPrivilege>
															</td>
																<td rowspan="3" >
																<u:hasPrivilege privilegeId="sys.security.role.visible.type">
																	<select multiple style="width:40px; height:80px;" class="ListBox" id="selAgentType" name="selAgentType" disabled>
																	<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />		
																	</select> <font class="mandatory"> &nbsp;* </font>
																</u:hasPrivilege> 
															</td>

															<td valign="top" align="right">
																<u:hasPrivilege privilegeId="sys.security.role.assign.type">
																<font>Assignable&nbsp;&nbsp;&nbsp;<br />  Agent Type&nbsp;&nbsp;&nbsp;</font>
																</u:hasPrivilege>
																</td>
																<td rowspan="3" >
																<u:hasPrivilege privilegeId="sys.security.role.assign.type">
																	<select multiple style="width:40px; height:80px;" class="ListBox" id="selVisibleAgentType" name="selVisibleAgentType" disabled>
																	<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />		
																	</select> <font class="mandatory"> &nbsp;* </font>
																</u:hasPrivilege> 
															</td>	
															
															<td valign="top" align="right">
																<u:hasPrivilege privilegeId="sys.security.role.servicechannel.view">
																<font>Service&nbsp;&nbsp;&nbsp;<br /> Channel&nbsp;&nbsp;&nbsp;</font>
																</u:hasPrivilege>
															</td>
															<td rowspan="3" valign="top" >
																<u:hasPrivilege privilegeId="sys.security.role.servicechannel.view">
																	<select size="1"  id="selServiceChannel" name="selServiceChannel" disabled>
																	<c:out value="${requestScope.reqServiceChannels}" escapeXml="false" />
																	</select> <font class="mandatory"> &nbsp;* </font>
																</u:hasPrivilege> 
															</td>	
														</tr>
													</table>
												</td>												  
												</tr>
		  										 <tr>
													<td valign="top">
														<span id="spn1" class="FormBackGround"></span>
													</td>
												 </tr>
												</table> 
												<%@ include file="../../common/IncludeFormBottom.jsp"%>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<%@ include file="../../common/IncludeFormTopFrameLess.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<input name="btnClose" type="button" align="right"class="Button" id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" >
									<input name="btnReset" type="button" class="Button" align="left" id="btnReset"  onClick="resetClick()" value="Reset">
								</td>
								<td align="right">
									<input name="btnSave" type="button" align="left" class="Button" id="btnSave" onclick="saveData()" value="Save">
								</td>
										</tr>
									</table>					
									<%@ include file="../../common/IncludeFormBottom.jsp"%> 

				</td>
			</tr>
		</table>


	<script type="text/javascript">
		<c:out value="${requestScope.reqDependancyArray}" escapeXml="false" />
		<c:out value="${requestScope.reqHtmlPrivilegeData}" escapeXml="false" />			
	</script>
	<input type="hidden" name="hdnVersion" id="hdnVersion"/> 
	<input type="hidden" name="hdnMode" id="hdnMode" /> 
	<input type="hidden" name="hdnModel" id="hdnModel" /> 
	<input type="hidden" name="hdnAssignPrivileges" id="hdnAssignPrivileges" /> 
	<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1"/> 
	<input type="hidden" name="hdnAction" id="hdnAction"/>
	<input type="hidden" name="hdnRoleId" id="hdnRoleId"/>
	<input type="hidden" name="hdnNoAgent" id="hdnNoAgent"/>
	</form>
	<script  src="../../js/security/role/RolePrivAdminGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </body>		

  <script type="text/javascript">
  	<!--
  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
  	function ClearProgressbar(){  		
  		if (typeof(objDG) == "object"){
	  		if (objDG.loaded){
	  			clearTimeout(objProgressCheck);
	  			top[2].HideProgress();
	  		}
	  	}	  	
  	}
  	 
  	//-->
  </script>

</html>