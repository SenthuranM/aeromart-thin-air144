<%-- 
	 @Author 	: 	Srikanth
	 @Copyright : 	ISA
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Territory Admin</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">

	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>		
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
    	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.formfileupload.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
	<script src="../../js/station/StationValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </head>

  <body class="tabBGColor" scroll="no"  oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown='return Body_onKeyDown(event)' ondrag='return false' 
  	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">

  <form method="post" action="showStation.action">
  			<script type="text/javascript">
				var arrCountry = new Array();
				var countryStateArry = {};
  				<c:out value="${requestScope.CountryCode}" escapeXml="false" />
				<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
				<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />		
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
				<c:out value="${requestScope.reqFormData}" escapeXml="false" />
				<c:out value="${requestScope.reqCountryCombo}" escapeXml="false" />			
				
				<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
				<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
				<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />
				<c:out value="${requestScope.reqAllowExtendedNameModificaion}" escapeXml="false" />
				<c:out value="${requestScope.reqCountryStateList}" escapeXml="false" />

			</script>

			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Search Station<%@ include file="../common/IncludeFormHD.jsp"%>	 			  
						<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table2">
						  <tr>       
								<td width="10%"><font>Country</font></td>
								<td>								
									<select name="selCountry1" id="selCountry1">
										<option value="-1">All </option> 
										<c:out value="${requestScope.reqCountryList}" escapeXml="false" />
	                               </select>
	                             </td> 
								<td  width="11%" align="right">
									<input name="btnSearch" type="button" class="button" id="btnSearch"  value="Search">
								</td> 
							</tr> 
					  	</table> 
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>		
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Station<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									  	<span id="spnStations"></span> 
								</td>
							</tr>
							<tr>
								<td>
									<u:hasPrivilege privilegeId="sys.mas.station.add">
										<input type="button" id="btnAdd" class="Button" value="Add" onClick="addClick()">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.mas.station.edit">
										<input type="button" id="btnEdit" class="Button" value="Edit" onClick="editClick()">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.mas.station.delete">
										<input type="button" id="btnDelete" class="Button" value="Delete" onClick="deleteClick()">
									</u:hasPrivilege>
								</td>
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						Add/Modify Station
						
						<%@ include file="../common/IncludeFormHD.jsp"%>			
							<!-- <br> -->
							  <table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
									<tr>
										<td width="15%"><font>Station Code</font></td>
										<td><input name="txtStationId" type="text" id="txtStationId" maxlength="3" size="10" onkeyUp="alphaNumericWithoutSpaceValidate(this)" class ="UCase" onkeyPress="alphaNumericWithoutSpaceValidate(this)"onChange="clickChange()" ><font class="mandatory">&nbsp*</font></td>
									</tr>
									<tr>
										<td><font>Station Name</font></td>
										<td><input name="txtDesc" type="text" id="txtDesc" maxlength="50" size="50" onkeyUp="alphaNumericValidate(this)" onkeyPress="alphaNumericValidate(this)" onChange="clickChange()"><font class="mandatory">&nbsp*</font></td>
									</tr>
									<tr>
										<td><font>Territory</font></td>
										<td>
											<select name="selTerritory" id="selTerritory" size="1" style="width:150;" onChange="clickChange()">
												<option value="-1"></option> 
												<c:out value="${requestScope.reqTerritoryList}" escapeXml="false" />
											</select><font class="mandatory">&nbsp*</font>
										</td>
								  
									</tr>
									<tr>
										<td width="10%"><font>Country</font></td>
										<td> 
								 	  <select name="selCountry" id="selCountry" style="width:150;">
										<option value="-1"></option> 
										<c:out value="${requestScope.reqCountryList}" escapeXml="false" />
	                                 		</select><font class="mandatory">&nbsp*</font>
	                              		</td>
									</tr>
									<tr id='stateRow'>
										<td width="10%"><font>State</font></td>
										<td> 
								 	  <select name="selState" id="selState" style="width:150;" > 
	                                 	</select><font class="mandatory">&nbsp*</font>
	                              		</td>
									</tr>
									<tr>
										<td width="10%"><font>&nbsp</font></td>
										<td>
										<input type="radio" id="radOnline1" name="radOnline" value="1" class="NoBorder" checked="checked">
					  					<font>Online</font>
					  					<input type="radio" id="radOnline2" name="radOnline" value="0" class="NoBorder">
					  					<font>Offline</font>&nbsp;&nbsp;	
										</td>
									</tr>
									
									<tr>
										<td><font>Contact</font></td>
										<td>
											<input type="text" id="txtContact" name="txtContact" size="25" onkeyUp="valContact(this)" onkeyPress="valContact(this)"  maxlength="225" onChange="pageOnChange()"><!--<font class="mandatory">&nbsp*</font> -->
										</td>
									</tr>
									
									<tr>
										<td><font>Phone</font></td>
						  				<td>
							  				<input type="text" id="txtPhone" name="txtPhone" size="25" maxlength="20" onkeyUp="positiveWholeNumberValidate(this)" onkeyPress="positiveWholeNumberValidate(this)" onChange="pageOnChange()" ><!--<font class="mandatory">&nbsp*</font> -->
						  				</td>
									</tr>
									
									<tr>
										<td><font>Fax</font></td>
						  				<td>
							  				<input type="text" id="txtFax" name="txtFax" size="25"  maxlength="20" onkeyUp="positiveWholeNumberValidate(this)" onkeyPress="positiveWholeNumberValidate(this)" onChange="pageOnChange()"><!--<font class="mandatory">&nbsp*</font> -->
						  				</td>
									</tr>
									
									<tr>
										<td valign="top"><font>Remarks</font></td>
										<td><textarea name="txtRemarks" id="txtRemarks" cols="94" rows="2" maxlength="255" onkeyUp="validateTextArea(this,255);commaValidate(this);" onkeyPress="validateTextArea(this,255);commaValidate(this);" onChange="clickChange()" ></textarea></td>
									</tr>
                                 <tr>
								   <td><font>Active</font></td>
								   <td align="left"><input type="checkbox" name= "chkStatus" id="chkStatus"></td>
							     </tr>
							     <tr>
							     	<td id="thresholdTime"><font>Threshold Time</font></td>
							     	<td id="thresholdTimeInput">
							     		<input type="text" id="txtThresholdTime" name="txtThresholdTime" size="10"  maxlength="5" onChange="pageOnChange()" onblur="setTimeWithColon(document.forms[0].txtThresholdTime)" onkeyUp="positiveWholeNumberValidate(this)" >
							     		<font>(hh:mm)</font>
						  			</td>
							     </tr>
                                  	<tr>
									  <td style="height:42px;" colspan="2" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td>
													<input type="button" id="btnClose" class="Button" value="Close"  onclick="closeStation()">
													<input name="btnReset" type="button" class="Button" id="btnReset"  onClick="resetStation()" value="Reset">
												 </td>
												 <td align="right">
													<input name="btnSave" type="button" class="Button" id="btnSave" onClick="saveStation()" value="Save">
												 </td>
											</tr>
										</table>
									   </td>
									</tr>
							  </table>
						  <%@ include file="../common/IncludeFormBottom.jsp"%>	
						 </td>
					</tr>
				  <tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>

		<input type="hidden" name="hdnVersion"  id="hdnVersion" value=""/>	
		<input type="hidden" name="hdnMode" id="hdnMode"/>
		<input type="hidden" name="hdnAction" id="hdnAction"/>			
		<input type="hidden" name="hdnRecNo" id="hdnRecNo">		
		<script>
		//	fillCombos();
		</script>
	</form>	
	<script src="../../js/station/StationAdminGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </body>
  
  <script type="text/javascript">
  	<!--
  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
  	function ClearProgressbar(){  		
  		if (typeof(objDG) == "object"){
	  		if (objDG.loaded){
	  			clearTimeout(objProgressCheck);
	  			top[2].HideProgress();
	  		}
	  	}	  	
  	}
  	 
  	//-->
  </script>
</html>