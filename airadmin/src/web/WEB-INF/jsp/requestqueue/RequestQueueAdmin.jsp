    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <title>Request Queue Management</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
    <LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/requestQueueManagement.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
</head>

<body>
<script type="text/javascript">	
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
</script>
<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">

	<tr>
				<td colspan="3">
				<%@ include file="../common/IncludeFormTop.jsp"%>Search Request Queue<%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table1">
						<tr>
							<td width="10%"><font>Queue Name</font></td>								
							<td width="10%"><font>Applicable Page</font></td>
							<td width="10%" align="right" rowspan="2">
								<input type="button" id="btnSearch"  name="btnSearch" class="Button"  value="Search"  tabIndex="9">
							</td>
						</tr>						
						<tr>
							<td width="15%">
								<input name="searchQueueType" type="text" id="searchQueueType" style="width:75px;" class="UCase" maxlength="5"  tabindex="1">
							</td>
							<td colspan="2" valign="top">
									<select id="applicablePage" name="applicablePage" size="1">
										<option value="">All</option>	
										<c:out value="${requestScope.reqQueuePages}" escapeXml="false" />							
									</select>
							</td>					
						</tr>
					</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
	</tr>
	<tr>
		<td id="requestsGridContainer">
	    </td>
	</tr>
	<tr>
		<td>
		<input type="button" id="btnAdd"  name="btnSeatAllocate" class="Button"  value="Add"  tabIndex="20">
		<input type="button" id="btnEdit"  name="btnSeatAllocate" class="Button"   value="Edit"  tabIndex="20">
		<input type="button" id="btnDelete"  name="btnSeatAllocate" class="Button"  value="Delete"  tabIndex="20">
		</td>
	</tr>	
	<tr>
		<td>
			<div id='editQueue'>
				<input type="hidden" id="selectedData" value="">
			<%@ include file="../common/IncludeFormTop.jsp"%>Add/Edit Request Queue<%@ include file="../common/IncludeFormHD.jsp"%>
			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="30%"><font>Queue Name</font></td>
					<td width="50%">
						<input type="text" id="queueName" name="queueName" style="width:150px;"  invalidText="true"   maxlength="10" tabIndex="24">
					</td>
				</tr>
				<tr>
					<td width="30%"><font>Description</font></td>
					<td colspan="4" valign="top" align="left">
							<textarea style="resize:none" name="queueDescription" id="queueDescription" cols="47" rows="4" maxlength="260" onkeyUp="validateTA(this,255);commaValidate(this);spclCharsVali(this);" onkeyPress="validateTA(this,255);commaValidate(this);spclCharsVali(this);"></textarea>
					</td>
				</tr>
				<tr>
						<td><input type="text" id="pageNameInput" name="pageNameInput" style="width:150px;"  invalidText="true"   maxlength="10" tabIndex="24"></td>
						<td>
							<input type="button" id="btnSearchPage"  name="btnSearchPage" class="Button"  onClick="searchForPages()" value="Search"  tabIndex="20">
						</td>					
				</tr>
				<tr>
					<td align="left" valign="center" width="15%" rowspan="4">
						<font>Assign Pages for Request Queue</font>
					</td>
					<td align="left" width="20%" rowspan="4">
						<select id="selectablePages" name="selectablePages" style="width: 150px; height: 150px" multiple >
						</select>
					</td>					
					<td align="left"  width="5%" rowspan="4">
						<table>
							  	<tr>
							  		<td align="center" onclick="addPage()">
										<div class="Button" style="width:23px;height: 17px">></div>
									</td>
								</tr>
								<tr>
									<td align="center" onclick="removePage()">
										<div class="Button" style="width:23px;height: 17px"><</div>
									</td>
							  	</tr>
						</table>
					</td>
					<td align="left"  width="20%" rowspan="4">
						<select id="selectedPages" size="8" name="selectedPages" style="width: 150px; height: 150px" multiple >
						</select>
						<font class="mandatory">&nbsp;*</font>
					</td>
				</tr>
				<tr>
						<td colspan="2"><td>
						<td><input type="text" id="userNameInput" name="userNameInput" style="width:150px;"  invalidText="true"   maxlength="10" tabIndex="24"></td>
						<td>
							<input type="button" id="btnSearchUser"  name="btnSearchUser" class="Button"  onClick="searchForUsers()" value="Search"  tabIndex="20">
						</td>					
				</tr>
				<tr>
					<td align="left" valign="center" width="15%" rowspan="4">
						<font>Assign Users for Request Queue</font>
					</td>
					<td align="left" width="20%" rowspan="4">
						<select id="selectableUsers" name="selectableUsers" style="width: 150px; height: 150px" multiple >
						</select>
					</td>							
					<td align="left"  width="5%" rowspan="4">
						<table>
							  	<tr>
							  		<td align="center" onclick="addUsers()">
										<div class="Button" style="width:23px;height: 17px">></div>
									</td>
								</tr>
								<tr>
									<td align="center" onclick="removeUsers()">
										<div class="Button" style="width:23px;height: 17px"><</div>
									</td>
							  	</tr>
						</table>
					</td>
					<td align="left"  width="20%" rowspan="4">
						<select id="selectedUsers" size="8" name="selectedUsers" style="width: 150px; height: 150px" multiple >
						</select>
						<font class="mandatory">&nbsp;*</font>
					</td>
				</tr>
			</table>
			<%@ include file="../common/IncludeFormBottom.jsp"%>
			</div>
		</td>
	</tr>
	<tr>
		<td>
		<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
				<tr>
					<td colspan="3" style="height: 42px;" valign="bottom">						
						<input type="button" id="btnClose"  name="btnClose" class="Button"  onClick="top[1].objTMenu.tabRemove(screenId)" value="Close"  tabIndex="20">
						<input type="button" id="btnReset"  name="btnReset" class="Button"  onClick="resetClick()" value="Reset"  tabIndex="20">					
					</td>
					<td align="right" valign="bottom">						
						<input type="button" id="btnSave"  name="btnSave" class="Button"  onclick="submitRequestQueue()" value="Save"  tabIndex="20">
					</td>
				</tr>
		</table>
		</td>
	</tr>
</table>
<script>
		var screenId = "SC_SHDS_0037";
</script>
</body>
</html>