 <%-- 
	 @Author 	: 	Srikanth
	 @Copyright : 	ISA
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>SITA Address Admin</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">

	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>		
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

	<script src="../../js/sitaaddress/SitaAddressValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
	
	<!--  Jquery stuff -->
	<script type="text/javascript" src="../../js/v2/jquery/jquery.js "></script>
 	<script src="../../js/v2/jquery/jquery-migrate.js" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js "></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
	
	<!--  external js library -->
	<script type="text/javascript" src="../../js/v2/common/pair-select.min.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
  </head>

  <body class="tabBGColor" scroll="no" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" ondrag='return false'
  	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">

  <form method="post" action="showSitaAddress.action">
  	<script type="text/javascript">
  		<c:out value="${requestScope.AirportCode}" escapeXml="false" />
		<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
		<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />		
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
		<c:out value="${requestScope.reqFormData}" escapeXml="false" />
		<c:out value="${requestScope.reqFormFieldValues}" escapeXml="false" />		

		<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
		<c:out value="${requestScope.reqOperationUIMode}" escapeXml="false" />
		<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
		<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />		
		<c:out value="${requestScope.reqAirportFocus}" escapeXml="false" />
		<c:out value="${requestScope.reqDeliveryMethodDisplayStatus}" escapeXml="false" />
		<c:out value="${requestScope.reqPalCalDisplayStatus}" escapeXml="false" />
		
		//<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	    //<c:out value="${requestScope.reqBcHtmlData}" escapeXml="false" />
	</script>

			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Search SITA Address<%@ include file="../common/IncludeFormHD.jsp"%>					  
						<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table2">
							<tr>
								<td width="10%"><font>Airport Code</font></td>
								<td>
									<select name="selAirport" id="selAirport">
										<option value="-1">All </option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
	                                </select>
	                            </td>

								<td width="10%" align="center"><font>Carrier Code</font></td>
								<td>
									<select name="selCarrierCode" id="selCarrierCode">
										<option value="-1">All </option>
										<c:out value="${requestScope.reqCarrierList}" escapeXml="false" />
	                                </select>
	                            </td>
	                            
								<td width="10%" align="center"><font>Status</font></td>
								<td>
									<select name="selActiveStatus" id="selActiveStatus">
										<option value="-1">All </option>
										<c:out value="${requestScope.reqActiveStatusList}" escapeXml="false" />
	                                </select>
	                            </td>

								<td width="10%"><font>SITA Address</font></td>
								<td>
									<input name="txtSitaAddress" type="text" id="txtSitaAddress" maxlength="7" size="15"/>
	                            </td>

								<td  width="11%" align="right"><input name="btnSearch" type="button" class="button" id="btnSearch" onClick="searchSitaAddress()" value="Search"></td>
							</tr>
					  	</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>		
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>SITA Addresses<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									  <span id="spnSitaAddress"></span>
								</td>
							</tr>
							<tr>
								<td>
									<u:hasPrivilege privilegeId="sys.mas.sita.add">
										<input type="button" id="btnAdd" class="Button" value="Add" onClick="addClick()">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.mas.sita.edit">
										<input type="button" id="btnEdit" class="Button" value="Edit" onClick="editClick()">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.mas.sita.delete">
										<input type="button" id="btnDelete" class="Button" value="Delete" onClick="deleteClick()">
									</u:hasPrivilege>
								</td>
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						Add/Modify SITA Address 
						
						<%@ include file="../common/IncludeFormHD.jsp"%>			
							<br>
							  <table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
									<tr>
										
													<td width="15%"><font>SITA Address</font></td>
												  <td width="35%"><input class="UCase" name="txtFormSitaAddress" type="text" id="txtFormSitaAddress" maxlength="7" size="12" onkeyUp="valAddress(this)" onkeyPress="valAddress(this)" onChange="pageOnChange()"></td>													
												  <td width="10%"><font>Carrier Code</font></td>
					  <td width="40%" rowspan="3" ><select multiple style="width:60px; height:70px;" class="ListBox" id="selCarrierCodes">
														<c:out value="${requestScope.reqCarrierList}" escapeXml="false" />		
														</select> <font class="mandatory"> &nbsp;* </font>
									  </td>
  </tr>
									
												<tr>										
													<td>
														<font> Email Id</font> 
													</td>										
													<td>																					
														<input type="text" name="txtEmail" id="txtEmail" onChange="">											
													</td>
												</tr>
												
												<tr>
													<td><font>Departure Airport Code</font></td>
													<td>				
														<select name="selFormAirport" id="selFormAirport" size="1" style="width:105;" onChange="fromAirportOnChange()">
															<option></option> 
															<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
														</select><font class="mandatory">&nbsp;*</font>
														</td>
											
												</tr>
												
												
									
							<tr>		
				  				<td>
				  					<input tabindex="2" type="radio" id="radRoute" name="radRoute" value="0" class="NoBorder"  onChange="pageOnChange()" onClick="radRouteChecked()" />
				  					<font>OnD</font>&nbsp;&nbsp;				  					
								</td>
								<td width="78px;"></td>
							</tr>
							<tr>
								<td><font>&nbsp;&nbsp;Arrival</font></td>
								<td>	
									<select name="selArrival" size="1" id="selArrival" style="width:55px;">
										<option></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
									</select>
								</td>
								<td><font>&nbsp;</font></td>
								<td>
								</td>
								<td></td>
								<td></td>
				  			</tr> 
							<tr> 
								<td></td>
								<td></td>
								<td>
								</td>
								<td></td>
								<td></td>
				  			</tr> 
							<tr> 
								<td></td>
								<td></td>
								<td>
								</td>
								<td></td>
								<td></td>
				  			</tr>
				  			<tr>
								<td width="15%"></td>
								<td style="padding-left: 89px">
									<table width="50%">
											<tr>
												<td width="20%">
												</td>
												<td width="10%">
													<table width="100%">
														<tr>
															<td width="10%" id="add_Segment_td">
																<input name="add_Segment" type="button" class="Button" id="add_Segment" onClick="addToList()" value=">" style="width: 23px; height: 17px">
															</td>
														</tr>
														<tr>
															<td width="10%" id="delete_Segment_td">
																<input name="delete_Segment" type="button" class="Button" id="delete_Segment" onClick="removeFromList()" value="<" style="width: 23px; height: 17px">
															</td>
														</tr>
													</table>
												</td>
												<td width="20%">
													<select id="selSegment" name="selSegment" multiple size="1" style="width:200px;height:85px">
													</select>
													<select id="sel_Ond_Data" name="sel_Ond_Data" style="width: 0; height: 0; display:none;" multiple >
														<c:out value="${requestScope.reqOndListData}" escapeXml="false" />
													</select>
												</td>
											</tr>
										</table>
									</td>
							</tr>
				  			<tr>		
				  				<td>
				  					<input tabindex="1" type="radio" id="radFlightNum" name="radFlightNum" value="1" class="NoBorder" onClick="radFlightNumChecked()" onChange="pageOnChange()">
				  					<font>Flight Number(s)</font>		  					
								</td>
								<td width="78px;"></td>
							</tr>
							<tr>
								<td width="15%"><font>Flight Number(s)</font></td>
								<td style="padding-left: 7px">
									<table width="50%">
										<tr>
											<td width="20%">
												<select id="sel_FlightNo" name="sel_FlightNo" style="width: 80; height: 180" multiple>
													<c:out value="${requestScope.reqFlightListData}" escapeXml="false" />
												</select>
												<select id="sel_FlightNo_Initial" name="sel_FlightNo_Initial" style="width: 0; height: 0; display:none;" multiple >
													<c:out value="${requestScope.reqFlightListData}" escapeXml="false" />
												</select>
												<select id="sel_FlightNo_Origin_Initial" name="sel_FlightNo_Origin_Initial" style="width: 0; height: 0; display:none;" multiple >
													<c:out value="${requestScope.reqFlightWithOriginListData}" escapeXml="false" />
												</select>
											</td>
												<td width="10%">
													<table width="100%">
														<tr>
															<td width="10%" id="add_FlightNo_td">
																<input name="add_FlightNo" type="button" class="Button" id="add_FlightNo" value=">" style="width: 23px; height: 17px">
															</td>
														</tr>
														<tr>
															<td width="10%" id="del_FlightNo_td">
																<input name="del_FlightNo" type="button" class="Button" id="del_FlightNo" value="<" style="width: 23px; height: 17px">
															</td>
														</tr>
													</table>
												</td>
											<td width="20%">
												<select id="sel_FlightNoAssigned" size="8" name="sel_FlightNoAssigned" style="width: 80; height: 180" multiple>
												</select>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							 
							
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									<tr>
										<td width="15%"><font>Location Description</font></td>
										<td colspan="3">
											<input name="txtLocation" id="txtLocation" type="text" maxlength="50" size="75" onChange="pageOnChange()" onkeyUp=" valLocation(this)" onkeyPress=" valLocation(this)" >
											<font class="mandatory">&nbsp;*</font></td>
									</tr>
									<tr>
										<td><font>Active</font></td>
										<td colspan="3"><input type="checkbox" name= "chkStatus" id="chkStatus" onChange="pageOnChange()"></td>
									</tr>
									
									<tr id="deliveryMethod">
										<td><font>Delivery Method</font></td>
										<td>				
											<select name="selDeliveryMethod" id="selDeliveryMethod" size="1" style="width:105;" onChange="pageOnChange();enableDisableAddressFields()">
												<option></option>
												<option value="SITATEX">SITATEX</option>
												<option value="M-CONNECT">M-CONNECT</option>
												<option value="ARINC">ARINC</option> 															
											</select><font class="mandatory">&nbsp;*</font>
										</td>												
									<tr>

									<tr>
										<td><font>Allow For PNL/ADL</font></td>
										<td colspan="3"><input type="checkbox" name= "allowPnlAdl" id="allowPnlAdl" onChange="pageOnChange()"></td>
									</tr>
									<tr id="allowPalCalRow" style="">
										<td><font>Allow For PAL/CAL</font></td>
										<td colspan="3"><input type="checkbox" name= "allowPalCal" id="allowPalCal" onChange="pageOnChange()"></td>
									</tr>

									<tr>
									 
												<td style="height:42px;" colspan="4" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td>
													<input type="button" id="btnClose" class="Button" value="Close"  onclick="top[1].objTMenu.tabRemove(screenId)">	
													<input name="btnReset" type="button" class="Button" id="btnReset"  onClick="resetSitaAddress()" value="Reset">
												 </td>
												 <td align="right">
													<input name="btnSave" type="button" class="Button" id="btnSave" onClick="saveSitaAddress()" value="Save">
												 </td>
											</tr>
										</table>
									   </td>
											</tr>
										
							  </table>
						  <%@ include file="../common/IncludeFormBottom.jsp"%>	
						 </td>
					</tr>
				  <tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>

		<input type="hidden" name="hdnSitaId" id="hdnSitaId"/>	
		<input type="hidden" name="hdnVersion" id="hdnVersion"/>	
		<input type="hidden" name="hdnGridRow" id="hdnGridRow"/>
		<input type="hidden" name="hdnMode" id="hdnMode"/>	
		<input type="hidden" name="hdnUIMode" id="hdnUIMode"/>
		<input type="hidden" name="hdnRecNo" id="hdnRecNo">	
		<input type="hidden" name="hdnCarrierCodes" id="hdnCarrierCodes"/>	
		<input type="hidden" name="hdnOndCodes" id="hdnOndCodes"/>
		<input type="hidden" name="hdnFlightNumbers" id="hdnFlightNumbers"/>
	</form>	
	<script src="../../js/sitaaddress/SitaAddressAdminGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </body>

  <script type="text/javascript">
  	<!--
  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
  	function ClearProgressbar(){  		
  		if (typeof(objDG) == "object"){
	  		if (objDG.loaded){
	  			clearTimeout(objProgressCheck);
	  			top[2].HideProgress();
	  		}
	  	}	  	
  	}
  	 
  	//-->
  </script>

</html>