<%-- 
	 @Author 	: 	Sumudu
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Template Page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">

<script	src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script src="../../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 
<script	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/fareClasses/ManageFareClasses.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/fareClasses/FareClassValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
</head>
<script type="text/javascript">
	var chidVisibility = "<c:out value="${requestScope.childVisibility}" escapeXml="false" />";
	var showNoFareSplitOption = "<c:out value="${requestScope.isDisplayNoFareSplitOption}" escapeXml="false" />";
	<c:out value="${requestScope.reqFareGrid}" escapeXml="false" />
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />	
	<c:out value="${requestScope.reqSysParam}" escapeXml="false" />
	var model= new Array();
	<c:out value="${requestScope.reqModelData}" escapeXml="false" />
	var fixedBCFareRuleArr= new Array();
	<c:out value="${requestScope.reqFixedBCFareRuleData}" escapeXml="false" />
	<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />
	<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
	<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />
	var isDelete=false;
	<c:out value="${requestScope.deleteMsg}" escapeXml="false" />
	<c:out value="${requestScope.reqPaxFareData}" escapeXml="false" />
	var isOpenRetSupport = "<c:out value="${requestScope.openReturnSupport}" escapeXml="false" />";
	<c:out value="${requestScope.reqFlexiJourneyList}" escapeXml="false" />
	<c:out value="${requestScope.reqFlexiCodeList}" escapeXml="false"/>
	<c:out value="${requestScope.fareDiscountVisibility}" escapeXml="false" />
	<c:out value="${requestScope.agentInstructionsVisibility}" escapeXml="false" />
	<c:out value="${requestScope.agentFareCommissionVisibility}" escapeXml="false" />
	<c:out value="${requestScope.reqMultiFltTypesAvailability}" escapeXml="false" />
</script>
<body scroll="yes" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return showContextMenu()"	onkeydown='return Body_onKeyDown(event)' onunload="beforeUnload()"	ondrag='return true' class="tabBGColor"
	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">

<form action="showFare.action" name="frmFareClasses" id="frmFareClasses" method="post">
<table width="99%" align="center" border="0" cellpadding="0"
	cellspacing="0">
	<tr>
		<td></td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeMandatoryText.jsp"%>
		</td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeFormTop.jsp"%>Search	Fare Rules<%@ include file="../common/IncludeFormHD.jsp"%>


		<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table9">
			<tr>
				<td width="15%"><font>Fare Rule Code</font></td>
				<td width="15%"><select id="selFareRuleCode"
					name="selFareRuleCode" size="1" style="width: 100px"
					onChange="controlStatSearch('selFareRuleCode')">
					<OPTION value="All">All</OPTION>
					<c:out value="${requestScope.reqFareClassList}" escapeXml="false" />

				</select></td>
				<td width="10%"><font>Status</font></td>
				<td width="12%"><select id="selStatus" name="selStatus"
					size="1" style="width: 60px"
					onChange="controlStatSearch('selStatus')">
					<OPTION value="All">All</OPTION>
					<OPTION value="ACT">Active</OPTION>
					<OPTION value="INA">Inactive</OPTION>
				</select></td>

				<td align="right"><input type="button" id="btnSearch" name="btnSearch" value="Search" class="button" onClick="ctrl_Search_click()"></td>
			</tr>
		</table>
		</td>
		<td class="FormBackGround"></td>
	</tr>
	<tr>
		<td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td>
		<td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
		<td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4">
	<tr>
		<td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td> 
		<td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader">Add/Edit
		Fare Rules </font></td>
		<td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td>
	</tr>
	<tr>
		<td class="FormBackGround"></td>
		<td class="FormBackGround" valign="top">
		<table width="100%" border="0" cellpadding="0" cellspacing="0"
			ID="Table9">
			<tr>
				<td><font class="fntSmall">&nbsp;</font></td>
			</tr>
			<tr>
				<td><span id="spnBkgCodes"></span></td>
			</tr>
			<tr>
				<td><u:hasPrivilege privilegeId="plan.fares.rule.add">
					<input type="button" id="btnAdd" name="btnAdd" class="Button" value="Add" onClick="ctrl_Addclick()">
				</u:hasPrivilege> <u:hasPrivilege privilegeId="plan.fares.rule.edit">
					<input type="button" id="btnEdit" name="btnEdit" class="Button" value="Edit" onClick="ctrl_Editclick()">
				</u:hasPrivilege> <u:hasPrivilege privilegeId="plan.fares.rule.delete">
					<input type="button" id="btnDelete" name="btnDelete" class="Button" value="Delete" onClick="ctrl_Deleteclick()">
				</u:hasPrivilege> <u:hasPrivilege privilegeId="plan.fares.rule.agent">
					<input type="button" id="btnViewAgntFC" name="btnViewAgntFC" class="Button" value="View Agent Wise FR" style="width: 140px;" onclick="view_AgentWiseFR()">
				</u:hasPrivilege>
				</td>
			</tr>
		</table>
		</td>
		<td class="FormBackGround"></td>
	</tr>
	<tr>
		<td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td>
		<td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
		<td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0" border="0"	align="center" ID="Table4">
	<tr>
		<td><%@ include file="../common/IncludeFormTop.jsp"%>Fare Rule<%@ include file="../common/IncludeFormHD.jsp"%>
		<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table9">
			<tr>
				<td colspan="2">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="12%" align="left"><font>Fare Rule Code</font></td>
						<td width="11%" align="left"><input type="text" id="txtFc" name="txtFc" maxLength="7" size="7" onChange="required_changed()" 
							onkeyPress="kpAlphaNumeric('txtFc')" onkeyUp="kpAlphaNumeric('txtFc')"> <font class="mandatory"><b>*</b></font></td>
						<td width="27%"><input type="text" id="txtDesc" name="txtDesc" maxlength="30" size="30"
							title="Fare Rule Description" onChange="required_changed()"><font class="mandatory"><b>*</b></font></td>
						
						<td align="left" width="10%"><font>Basis Code</font></td>
						<td width="18%" align="left"><font><input type="text" id="txtFBC" name="txtFBC" size=16 maxlength="15"></font></td>
						<td width="7%" align="left"><font>Active</font></td>
						<td width="5%" align="left"><input type="checkbox" id="chkStatus" name="chkStatus" class="NoBorder" onChange="required_changed()" value="ON"></td>
						<td >&nbsp;</td>
						
					</tr>
				</table>
				</td>
			</tr>
			<!--   Re-struct start -->
			
			<tr>
				<td colspan="2">
				<table border="0" width="100%">
				<tr><td width="50%">
				<table border="0" width="100%">
					<tr>
					<td colspan="7">
					<table border="0" width="100%">
					<tr>
					<td>
												
												<input type="radio" id="radCurrFareRule" name="radCurrFareRule" value="BASE_CURR"  
												class="NoBorder"onClick="fareRuleCurrClick();required_changed()" checked="checked">
												<font>Base Currency</font>
											</td>
											
											<c:if test="${requestScope.isDisplaySelectedCurrencyOption == true }">
											<td>
												
												<input type="radio" id=radSelCurrFareRule name="radCurrFareRule" value="SEL_CURR"  
												class="NoBorder"onClick="fareRuleCurrClick();required_changed()">
												<font>Selected Currency</font>
											</td>
											</c:if>
											
											<td>
												<select name="selCurrencyCode" id="selCurrencyCode" size="1" style="width:105;" onChange="setPageEdited(true)">
													<option value="-1"></option> 
													<c:out value="${requestScope.reqCurrencyList}" escapeXml="false" />
												</select><font class="mandatory"></font>
											</td>
					
					</tr>
					</table>
					</td>
					
					</tr>					
					<tr>
					<td width="10%" valign="top" align="left"><font>Adv.Bkg Days:Hrs:Mins</font></td>
						<td width="1%" align="left"><input type="checkbox" id="chkAdvance" name="chkAdvance" class="NoBorder"
							onClick="DisableAdnvanceBKDays()" onChange="required_changed()" value="ON"></TD>
						<td width="5%" align="left"><input type="text" id="txtAdvBookDays" name="txtAdvBookDays" maxlength="3" size="3"
							onKeyUp="KPValidatePositiveInteger('txtAdvBookDays')" onKeyPress="KPValidatePositiveInteger('txtAdvBookDays')"
							class="rightText" onChange="required_changed()"></td>	
						<td width="5%"><input type="text" id="txtAdvBookHours" name="txtAdvBookHours" maxlength="2" size="2"
							onKeyUp="KPValidatePositiveInteger('txtAdvBookHours', 'HRS')" onKeyPress="KPValidatePositiveInteger('txtAdvBookHours', 'HRS')"
							class="rightText" onChange="required_changed()" tabindex="28"></td>
						<td width="5%"><input type="text" id="txtAdvBookMins" name="txtAdvBookMins" maxlength="2" size="2"
							onKeyUp="KPValidatePositiveInteger('txtAdvBookMins', 'MIN')" onKeyPress="KPValidatePositiveInteger('txtAdvBookMins', 'MIN')"
							class="rightText" onChange="required_changed()" tabindex="28"></td>
											
						
						<td width="13%" align="left"><font>Charge Restrictions</font></td>
						<td width="2%" align="left"><input type="checkbox" id="chkChrgRestrictions" name="chkChrgRestrictions" 
							class="NoBorder" onClick="DisableRestrictions()" onChange="required_changed()" value="ON"></td>
					</tr>
				</table>		
				
				
				</td>	
				<td>
				<table border="0" width="100%">
					<tr>
					<td align="left"  width="8%" align="left"><font>MOD</font></td>
						<td width="62%">
							<select id="selDefineModType" name="selDefineModType" size="1" style="width: 40px;" onChange="required_changed();clearModTypeOnClick();" tabindex="14">
								<option value=""></option>
								<c:out value="${requestScope.reqFareModCancTypeList}" escapeXml="false" />							
							</select>
						<input type="text" id="txtModification" name="txtModification" size="7"
							onKeyPress="KPValidateDecimel(this)" onKeyUp="KPValidateDecimel(this)" class="rightText" onChange="required_changed()" maxLength="13">
						<c:if test="${requestScope.isDisplaySelectedCurrencyOption == true }">
							<span id="spModLocal">
							<font>&nbsp;Sel. Curr</font>
							<input type="text" id="txtModificationInLocal" name="txtModificationInLocal" size="7"
								onKeyPress="KPValidateDecimel(this)" onKeyUp="KPValidateDecimel(this)" class="rightText" onChange="required_changed()" maxLength="13">
							</span>		
						</c:if>

						</td>						
						<td width="7%"><font>Min.</font></td>
						<td width="8%"><input type="text" id="txtMin_mod" name="txtMin_mod" class="rightText" onClick="required_changed()" onKeyPress="KPValidateDecimel(this)" onKeyUp="KPValidateDecimel(this)" tabindex="15" size="3">
						</td>
						<td width="7%"><font>Max.</font></td>
						<td width="8%"><input type="text" id="txtMax_mod" name="txtMax_mod" class="rightText" onClick="required_changed()" onKeyPress="KPValidateDecimel(this)" onKeyUp="KPValidateDecimel(this)" tabindex="16" size="3">
						</td>
					</tr>
					<tr>
					<td align="left" ><font>CNX</font></td>
						<td >
						<select id="selDefineCanType" name="selDefineCanType" size="1" style="width: 40px;" onChange="required_changed();clearCancTypeOnClick();" tabindex="18">
								<option value=""></option>
								<c:out value="${requestScope.reqFareModCancTypeList}" escapeXml="false" />
							</select>
						<input type="text" id="txtCancellation" name="txtCancellation" size="7"
							onKeyPress="KPValidateDecimel(this)" onKeyUp="KPValidateDecimel(this)" class="rightText" onChange="required_changed()" maxLength="17">
						<c:if test="${requestScope.isDisplaySelectedCurrencyOption == true }">
							<span id="spCancelLocal">
								<font>&nbsp;Sel. Curr</font>	
								<input type="text" id="txtCancellationInLocal" name="txtCancellationInLocal" size="7"
								onKeyPress="KPValidateDecimel(this)" onKeyUp="KPValidateDecimel(this)" class="rightText" onChange="required_changed()" maxLength="17">	
							</span>	
						</c:if>	
						</td>
						<td ><font>Min.</font></td>
						<td ><input type="text" id="txtMin_canc" name="txtMin_canc" class="rightText" onClick="required_changed()" onKeyPress="KPValidateDecimel(this)" onKeyUp="KPValidateDecimel(this)" tabindex="19" size="3">
						</td>
						<td><font>Max.</font></td>
						<td ><input type="text" id="txtMax_canc" name="txtMax_canc" class="rightText" onClick="required_changed()" onKeyPress="KPValidateDecimel(this)" onKeyUp="KPValidateDecimel(this)" tabindex="20" size="3">
						</td>
					</tr>
					</table>
				</td>	</tr>				
					
				</table>
				</td>
			</tr>
			
			<!--  Re-struct end -->
			
			
			<%-- 
			<tr>
				<td colspan="2">
				<table border="0" width="100%">

					<tr>
						<td width="10%" valign="top" align="left"><font>Adv.Bkg Days:Hrs:Mins</font></td>
						<td width="1%" align="left"><input type="checkbox" id="chkAdvance" name="chkAdvance" class="NoBorder"
							onClick="DisableAdnvanceBKDays()" onChange="required_changed()" value="ON"></TD>
						<td width="5%" align="left"><input type="text" id="txtAdvBookDays" name="txtAdvBookDays" maxlength="3" size="3"
							onKeyUp="KPValidatePositiveInteger('txtAdvBookDays')" onKeyPress="KPValidatePositiveInteger('txtAdvBookDays')"
							class="rightText" onChange="required_changed()"></td>	
						<td width="5%"><input type="text" id="txtAdvBookHours" name="txtAdvBookHours" maxlength="2" size="2"
							onKeyUp="KPValidatePositiveInteger('txtAdvBookHours', 'HRS')" onKeyPress="KPValidatePositiveInteger('txtAdvBookHours', 'HRS')"
							class="rightText" onChange="required_changed()" tabindex="28"></td>
						<td width="5%"><input type="text" id="txtAdvBookMins" name="txtAdvBookMins" maxlength="2" size="2"
							onKeyUp="KPValidatePositiveInteger('txtAdvBookMins', 'MIN')" onKeyPress="KPValidatePositiveInteger('txtAdvBookMins', 'MIN')"
							class="rightText" onChange="required_changed()" tabindex="28"></td>
											
						
						<td width="13%" align="left"><font>Charge Restrictions</font></td>
						<td width="2%" align="left"><input type="checkbox" id="chkChrgRestrictions" name="chkChrgRestrictions" 
							class="NoBorder" onClick="DisableRestrictions()" onChange="required_changed()" value="ON"></td>
						<td align="left"  width="4%" align="left"><font>Mod</font></td>
						<td width="15%">
							<select id="selDefineModType" name="selDefineModType" size="1" style="width: 40px;" onChange="required_changed();clearModTypeOnClick();" tabindex="14">
								<option value=""></option>
								<c:out value="${requestScope.reqFareModCancTypeList}" escapeXml="false" />							
							</select>
						<input type="text" id="txtModification" name="txtModification" size="6"
							onKeyPress="KPValidateDecimel(this)" onKeyUp="KPValidateDecimel(this)" class="rightText" onChange="required_changed()" maxLength="13">
						<input type="text" id="txtModificationInLocal" name="txtModificationInLocal" size="6"
							onKeyPress="KPValidateDecimel(this)" onKeyUp="KPValidateDecimel(this)" class="rightText" onChange="required_changed()" maxLength="13">	

						</td>						
						<td width="3%"><font>Min.</font></td>
						<td width="3%"><input type="text" id="txtMin_mod" name="txtMin_mod" class="rightText" onClick="required_changed()" onKeyPress="KPValidateDecimel(this)" onKeyUp="KPValidateDecimel(this)" tabindex="15" size="3">
						</td>
						<td width="3%"><font>Max.</font></td>
						<td width="3%"><input type="text" id="txtMax_mod" name="txtMax_mod" class="rightText" onClick="required_changed()" onKeyPress="KPValidateDecimel(this)" onKeyUp="KPValidateDecimel(this)" tabindex="16" size="3">
						</td>
						<td align="left" width="6%"><font>Cancellation</font></td>
						<td width="15%">
						<select id="selDefineCanType" name="selDefineCanType" size="1" style="width: 40px;" onChange="required_changed();clearCancTypeOnClick();" tabindex="18">
								<option value=""></option>
								<c:out value="${requestScope.reqFareModCancTypeList}" escapeXml="false" />
							</select>
						<input type="text" id="txtCancellation" name="txtCancellation" size="6"
							onKeyPress="KPValidateDecimel(this)" onKeyUp="KPValidateDecimel(this)" class="rightText" onChange="required_changed()" maxLength="17">
						<input type="text" id="txtCancellationInLocal" name="txtCancellationInLocal" size="6"
							onKeyPress="KPValidateDecimel(this)" onKeyUp="KPValidateDecimel(this)" class="rightText" onChange="required_changed()" maxLength="17">	
								
						</td>
						<td width="3%"><font>Min.</font></td>
						<td width="3%"><input type="text" id="txtMin_canc" name="txtMin_canc" class="rightText" onClick="required_changed()" onKeyPress="KPValidateDecimel(this)" onKeyUp="KPValidateDecimel(this)" tabindex="19" size="3">
						</td>
						<td width="3%"><font>Max.</font></td>
						<td width="3%"><input type="text" id="txtMax_canc" name="txtMax_canc" class="rightText" onClick="required_changed()" onKeyPress="KPValidateDecimel(this)" onKeyUp="KPValidateDecimel(this)" tabindex="20" size="3">
						</td>
					</tr>
				</table>
				</td>
			</tr>
			 --%>
			<tr>
				<td height="22">
				<table border="0" width="100%">
					<tr>
						<td width="2%" align="left" valign="middle"> 
         				   <input type="radio" id="radFare" name="radFare" value="OneWay"
							onClick="DisableReturnDays();required_changed()" class="NoBorder"></td>
						<td width="7%"><font>One Way &nbsp;</font></td>	
						<td align="left" valign="middle" width="2%">
							<input type="radio" id="radReturn" name="radFare" value="Return" onClick="DisableReturnDays();required_changed()"
							class="NoBorder" checked></td>
						<td width="9%"><font> Return</font></td>
						<td id="halfReturnLabel" width="7%"><font> Half Return</font> </td>
						<td id="halfReturnCheck" width="2%"> <input type="checkbox" id="chkHRT" name="chkHRT" class="NoBorder" onChange="required_changed()"></td>
						<td width="6%" id="tdLblChkOpenRT"><font>Open RT</font></td>				
						<td valign="middle" width="4%" id="tdChkOPenRT">   
							<input type="checkbox" id="chkOpenRT" name="chkOpenRT" class="NoBorder" onChange="required_changed()" onclick="EnableConfirmPeriod(this)"></td>	
						<td width="9%"><font>Print Expiry</font></td>				
						<td valign="middle" width="13%" align="left"> <input type="checkbox" id="chkPrintExp" name="chkPrintExp"
							class="NoBorder" onChange="required_changed()" value="on"></td>	
						<td valign="middle" width="10%"><font>Pax Category&nbsp;</font></td>
						<td width="12%" valign="middle"><select id="selPaxCat" name="selPaxCat" size="1" style="width: 90px">
							<c:out value="${requestScope.reqPaxCatList}" escapeXml="false" /></select></td>
						<td valign="middle" width="10%"><font>Fare Category&nbsp;</font></td>
						<td valign="middle" width="10%"><select id="selFareCat" name="selFareCat" size="1" style="width: 90px" onChange="selFareOnchange()">
							<c:out value="${requestScope.reqFareCatList}" escapeXml="false" /></select></td>
						<td width="14%"><font>Flexi Code</font></font></td>
						<td width="10%"><select id="selFlexiCode" name="selFlexiCode" size="1" style="width: 100px" tabindex="41" 
							></select></td>			
					</tr>
				</table>	
				</td>
			</tr>			
			<tr height="22">
				<td colspan="2">
				<table border="0" width="100%">
					<tr>			
						<td width="21%">
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td height="20">
										<font >Fare Visibility</font><font class="mandatory">*</font>
									</td>				
								</tr>
								<tr>
									<td align="left" width="108" valign="top"><select id="selFareVisibility" size="4" name="selFareVisibility"
										style="width: 100; height: 70" multiple onChange="ctrl_visibility_Select();ctrl_visibilityAgents()"
										onBlur="ctrl_visibilityAgents()">
										<c:out value="${requestScope.reqVisibilityList}" escapeXml="false" /></select></td>						
								</tr>
							</table>
						</td>
						<td width="45%"><%@ include file="../common/IncludeFareTimeDurations.jsp"%></td>		
						<td colspan="4" rowspan="2" valign="middle" width="38%"><%@ include file="../common/includePaxFareConditions.jsp"%></td>						
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td><font class="fntSmall">&nbsp;</font></td>
			</tr>
			<tr>
			<!-- fare rule modifications and load factor -->
			<td colspan="2">
				<table border="0" width="100%">

					<tr>
						<td width="3%"><font>Load Factor</font></td>
						<td width="1%" align="left"><input type="checkbox" id="loadFactorEnabled" name="loadFactorEnabled" 
							class="NoBorder" onClick="loadFactorValidation()" onChange="required_changed()" value="Y"></td>	
						<td width="4%"><font>Min.%</font></td>
						<td width="4%"><input type="text" id="loadFactorMin" name="loadFactorMin" class="rightText" onClick="required_changed()" onKeyPress="VaidateFactor()" onKeyUp="VaidateFactor()" tabindex="15" size="3" maxlength="3">
						</td>
						<td width="4%"><font>Max.%</font></td>
						<td width="4%"><input type="text" id="loadFactorMax" name="loadFactorMax" class="rightText" onClick="required_changed()" onKeyPress="VaidateFactor()" onKeyUp="VaidateFactor()" tabindex="16" size="3" maxlength="3">
						</td>
						
						<td width="5%" ></td>
						
						
						<td width="13%" valign="left" align="left"><font>Allow Modify Date</font></td>
						<td width="1%" align="left"><input type="checkbox" id="modifyByDate" name="modifyByDate" class="NoBorder"
							onClick="" onChange="required_changed()" value="ON" checked="checked"></TD>
						<td width="2%"></td>					
						<td width="13%" align="left"><font>Allow Modify OnD</font></td>
						<td width="1%" align="left"><input type="checkbox" id="modifyByOND" name="modifyByOND" 
							class="NoBorder" onClick="" onChange="required_changed()" value="ON" checked="checked"></td>
							
						<td width="5%" ></td>
						
						<td colspan="6"> 	
						 	<table id="tblFareDiscount">
							 	 <tr>
								 	 	<td width="10%"><font>Fare Discount</font></td>
										<td width="1%" align="left"><input type="checkbox" id="fareDiscount" name="fareDiscount" 
											class="NoBorder" onClick="enableDisableFareDiscount()" onChange="required_changed()" value="Y"></td>	
										<td width="4%"><font>Min.%</font></td>
										<td width="4%"><input type="text" id="fareDiscountMin" name="fareDiscountMin" class="rightText" onClick="required_changed()" onKeyPress="allowOnlyPositiveInt('fareDiscountMin')" onKeyUp="allowOnlyPositiveInt('fareDiscountMin')" tabindex="15" size="3" maxlength="3">
										</td>
										<td width="4%"><font>Max.%</font></td>
										<td width="4%"><input type="text" id="fareDiscountMax" name="fareDiscountMax" class="rightText" onClick="required_changed()" onKeyPress="allowOnlyPositiveInt('fareDiscountMax')" onKeyUp="allowOnlyPositiveInt('fareDiscountMax')" tabindex="16" size="3" maxlength="3">
										</td>	
							 	 </tr>
						 	</table>
						</td>
						
						
						<td width="5%"></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td colspan="3">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="50%">
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td>
									<font class="fntBold">Valid Days of week </font></td>
								</tr>
								<tr>
									<td><font>Out Bound&nbsp;&nbsp;&nbsp;All</font>&nbsp;
										<input type="checkbox" id="chkOutAll" name="chkOutAll" class="NoBorder" onClick="required_changed();selectAll(true)"> <font>Mo</font>&nbsp;
										<input type="checkbox" id="chkOutMon" name="chkOutMon" class="NoBorder" onClick="required_changed()"> <font>Tu</font>&nbsp;
										<input type="checkbox" id="chkOutTues" name="chkOutTues" class="NoBorder" onClick="required_changed()"> <font>We</font>&nbsp;
										<input type="checkbox" id="chkOutWed" name="chkOutWed" class="NoBorder" onClick="required_changed()"> <font>Th</font>&nbsp;
										<input type="checkbox" id="chkOutThu" name="chkOutThu" class="NoBorder" onClick="required_changed()"> <font>Fr</font>&nbsp;
										<input type="checkbox" id="chkOutFri" name="chkOutFri" class="NoBorder" onClick="required_changed()"> <font>Sa</font>&nbsp;
										<input type="checkbox" id="chkOutSat" name="chkOutSat" class="NoBorder" onClick="required_changed()"> <font>Su</font>&nbsp;
										<input type="checkbox" id="chkOutSun" name="chkOutSun" class="NoBorder" onClick="required_changed()"></td>	
								</tr>
								<tr>
									<td><font>In Bound&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All</font>&nbsp;
										<input type="checkbox" id="chkInAll" name="chkInAll" class="NoBorder" onClick="required_changed();selectAll(false)"> <font>Mo</font>&nbsp;
										<input type="checkbox" id="chkInbMon" name="chkInbMon" class="NoBorder" onChange="required_changed()"> <font>Tu</font>&nbsp;
										<input type="checkbox" id="chkInbTues" name="chkInbTues" class="NoBorder" onChange="required_changed()"> <font>We</font>&nbsp;
										<input type="checkbox" id="chkInbWed" name="chkInbWed" class="NoBorder" onChange="required_changed()"> <font>Th</font>&nbsp;
										<input type="checkbox" id="chkInbThu" name="chkInbThu" class="NoBorder" onChange="required_changed()"> <font>Fr</font>&nbsp;
										<input type="checkbox" id="chkInbFri" name="chkInbFri" class="NoBorder" onChange="required_changed()"> <font>Sa</font>&nbsp;
										<input type="checkbox" id="chkInbSat" name="chkInbSat" class="NoBorder" onChange="required_changed()"> <font>Su</font>&nbsp;
										<input type="checkbox" id="chkInbSun" name="chkInbSun" class="NoBorder" onChange="required_changed()"></td>
								</tr>
								<tr>
									<td>
					                    	<table id="tblAgentCommission" width="90%">
												<tr>
												 	<td><font class="fntBold"> Agent Commission</font></td>
												</tr>
												<tr>
													<td width="25%">
														<font>Apply By</font>
													</td>
											    	<td>
											    		<select id="selAgentComType" name="selAgentComType" size="1" style="width: 40px;" onChange="required_changed();clearModTypeOnClick();" tabindex="30">
															<option value="V">V</option>
															<option value="PF">PF</option>
														</select>
											    	</td>
											    	<td width="10%">
														<td><font>Commission Value</font>
													</td>
											    	<td>
											    		<input type="text" id="txtAgentCommision" name="txtAgentCommision" size="6" onKeyPress="KPValidateDecimel(this,10,2)" onKeyUp="KPValidateDecimel(this,10,2)" class="rightText" onChange="required_changed()" tabindex="33" maxLength="13">
											    	   <font class="mandatory"><b>*</b></font>
											    	</td>
												</tr>
											</table>
							    	</td>
								</tr>
							</table>
						</td>
					<td width="15%"> 
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td valign="middle" align="center">	<font class="fntBold">Local Time</font></td>
							</tr>
							<tr>
								<td><font>From</font></td>
								<td><font>To</font></td>
							</tr>
							<tr>
								<td width="50%" align="left"><input type="text" id="txtOutTmFrom" name="txtOutTmFrom" size="5" maxlength="5"
									onChange="required_changed()" onblur="setTimeWithColon(document.forms[0].txtOutTmFrom)"></td>
								<td width="50%" align="left"><input type="text" id="txtOutTmTo" name="txtOutTmTo" size="5" maxlength="5"
									onChange="required_changed()" onblur="setTimeWithColon(document.forms[0].txtOutTmTo)"></td>
							</tr>
							<tr>		
								<td><font class="fntSmall">&nbsp;</font></td>
							</tr>
							<tr>
								<td width="50%" align="left"><input type="text" id="txtInbTmFrom" name="txtInbTmFrom" size="5" maxlength="5"
									onChange="required_changed()" onblur="setTimeWithColon(document.forms[0].txtInbTmFrom)"></td>
								<td width="50%" align="left"><input type="text" id="txtInbTmTo" name="txtInbTmTo" size="5" maxlength="5"
									onChange="required_changed()" onblur="setTimeWithColon(document.forms[0].txtInbTmTo)"></td>
							</tr>
						</table>
					</td>
					<td width="35%">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<font>&nbsp;&nbsp;Rules &amp; Comments</font></td>
							</tr>
							<tr>
								<td width="100%"><textarea name="txtaRulesCmnts" id="txtaRulesCmnts" cols="45" rows="3"
							onkeyUp="validateTextArea(this);" onkeyPress="validateTextArea(this);" title="Enter text to be displayed in Fare Quote"
							onChange="required_changed()" onblur="removeAvoidableChar(this);"></textarea></td>
							</tr>
							<tr rowspan="2">
								<td>
									<table id="tblTextInstructions" >
										<tr>
											<td><font>&nbsp;&nbsp;Agent Instructions</font></td>
										</tr>
										<tr>
											<td width="100%">
												<textarea name="txtaInstructions" id="txtaInstructions" cols="45" rows="1"
												onkeyUp="validateTextArea(this);removeAvoidableChar(this)" onkeyPress="validateTextArea(this);removeAvoidableChar(this)" title="Enter Agent Instructions to be displayed in Fare Quote"
												onChange="required_changed()"></textarea></td>
										</tr>
									</table>
								</td>			
							</tr>	
						</table>
					</td>
				</tr>
			</table>
			</td>
				
		</tr>
		<tr>
			<td colspan="1">
				<table width="70%" border="0" cellpadding="0" cellspacing="0" align="left">
					<tr>
						<td style="height: 20px" valign="bottom" colspan="5">
							<input type="button" id="btnClose" class="Button" value="Close" onclick="closeClick()">
							<input name="btnReset" type="button" class="Button" id="btnReset" value="Reset" onClick="resetClick()">
							<input name="btnLnkAgent" type="button" class="Button" id="btnLnkAgent" value="Link Agents" onclick="linkAgents()">
							<input type="button" id="btnOverWriteRule" name="btnOverWriteRule" class="Button" value="View & Overwrite Fee" style="width: 140px;" onclick="viewAndOverwriteFee()">
						</td>
					</tr>
				</table>
				<table width="30%" border="0" cellpadding="0" cellspacing="0" align="right">
				<tr>
					<td style="height: 20px;text-align: right" valign="bottom" >
						<font>
							<label id=lblUpdateCommentsOnly style="visibility:hidden" title="If ticked won't create new Fares. Only allows updating comments and agent instructions">
								No Fare Split
							</label>
						</font>
						<input type="checkbox" name="chkUpdateCommentsOnly" id="chkUpdateCommentsOnly" class="NoBorder" title="If ticked won't create new Fares. Only allows updating comments and agent instructions"
							onChange="updateCommentAndDescription()" style="visibility:hidden;">
					
						<input name="btnSave" type="button" class="Button" id="btnSave" value="Save" onClick="save_click()">
					</td>
				</tr>
				</table>
				</td>
			</tr>
		</table>
		<!--  Left Pane End --></td>
		<td class="FormBackGround"></td>
	</tr>
	<tr>
		<td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td>
		<td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
		<td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td>
	</tr>
</table>
<input type="hidden" id="hdnMode" name="hdnMode" value="Mode">
<input type="hidden" id="hdnVersion" name="hdnVersion" value="">
<input type="hidden" id="hdnDependent" name="hdnDependent" value="Dependent">
<input type="hidden" id="hdnRecNo" name="hdnRecNo" value="1">
<input type="hidden" id="hdnarrDays" name="hdnarrDays" value=""> 
<input type="hidden" id="hdnFareID" name="hdnFareID" value=""> 
<input type="hidden" id="hdndeptDays" name="hdndeptDays" value=""> 
<input type="hidden" id="hdnRuleCode" name="hdnRuleCode" value=""> 
<input type="hidden" id="hdnVisibility" name="hdnVisibility" value="">
<input type="hidden" id="hdnSearchCriteria" name="hdnSearchCriteria" value=""> 
<input type="hidden" id="hdnAgents" name="hdnAgents" value=""> 
<input type="hidden" id="hdnFareVisibilityIDs" name="hdnFareVisibilityIDs" value=""> 
<input type="hidden" id="hdnArrIntDays" name="hdnArrIntDays" value=""> 
<input type="hidden" id="hdnDeptIntDays" name="hdnDeptIntDays" value="">

<input type="hidden" id="hdnPaxTypeAD" name="hdnPaxTypeAD" value="">
<input type="hidden" id="hdnPaxTypeCH" name="hdnPaxTypeCH" value="">
<input type="hidden" id="hdnPaxTypeIN" name="hdnPaxTypeIN" value="">

<input type="hidden" id="hdnVersionAD" name="hdnVersionAD" value="">
<input type="hidden" id="hdnVersionCH" name="hdnVersionCH" value="">
<input type="hidden" id="hdnVersionIN" name="hdnVersionIN" value="">

<input type="hidden" id="hdnFRPAXIDAD" name="hdnFRPAXIDAD" value="">
<input type="hidden" id="hdnFRPAXIDCH" name="hdnFRPAXIDCH" value="">
<input type="hidden" id="hdnFRPAXIDIN" name="hdnFRPAXIDIN" value="">
<!--fare rule modifications and load factor-->
<input type="hidden" id="hdnLoadFactorMax" name="hdnLoadFactorMax" value="">
<input type="hidden" id="hdnLoadFactorMin" name="hdnLoadFactorMin" value="">
<input type="hidden" id="hdnFareDiscountMin" name="hdnFareDiscountMin" value="">
<input type="hidden" id="hdnFareDiscountMax" name="hdnFareDiscountMax" value="">
<input type="hidden" id="isFlexiCodeRemoved" name="isFlexiCodeRemoved" value="">
<!--  <input type="hidden" id="hdnLocalCurrencySelected" name="hdnLocalCurrencySelected" value=""> -->
</form> 
<script src="../../js/fareClasses/FareClass.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" 	type="text/javascript"></script>
</body>
<script type="text/javascript">   <!--
      
   		var objProgressCheck = setInterval("ClearProgressbar()", 300);

   		function ClearProgressbar(){  		
	 		if (typeof(objDG) == "object"){
		  		if (objDG.loaded){
		  			clearTimeout(objProgressCheck);
		  			top[2].HideProgress();
		  		}
	  		}	  	
 		}
    
   //-->
    </script>

</html>