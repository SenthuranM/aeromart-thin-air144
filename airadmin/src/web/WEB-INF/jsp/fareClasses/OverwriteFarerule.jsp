<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Overwrite Fare Rule</title>
<meta http-equiv="pragma" content="no-cache"/>
<meta http-equiv="cache-control" content="no-cache"/>
<meta http-equiv="expires" content="-1"/>
<link rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>
<link rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>

<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	
<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
<script	src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

<script type="text/javascript">
var ofrcode = '<c:out value="${requestScope.fareruleCode}" escapeXml="false" />';
var ofrId = '<c:out value="${requestScope.fareruleId}" escapeXml="false" />';
var requestFrom = '<c:out value="${requestScope.requestFrom}" escapeXml="false" />';
var ruleVersion = '<c:out value="${requestScope.ruleVersion}" escapeXml="false" />';
var fareRuleLocalCurr = '<c:out value="${requestScope.ruleLocalCurrency}" escapeXml="false" />';
//TODO Fill the dorpdown boxs lobj list
var listCompareAgainst = [{key:"BEFDEP", value:"Before Departure"},{key:"AFTDEP", value:"After Departure"}];
var listTimeType = [{key:"MON", value:"Months"},{key:"WEK", value:"Weeks"},{key:"DAY", value:"Days"},{key:"HRS", value:"Hours"}, {key:"MIN", value:"Minutes"}];
var listChargeType = [{key:"MOD", value:"MOD"},{key:"CNX", value:"CNX"}, {key:"NCC", value:"NCC"}];
var listChargeAmtType = [{key:"V", value:"V"},{key:"PF", value:"PF"}, {key:"PFS", value:"PFS"}];
</script>
</head>
	<body class="tabBGColor">
		<table width="99%" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround">
		<tr>
			<td><font class="fntSmall">&nbsp;</font></td>
		</tr>
		<tr>
			<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
		</tr>
		<tr>
			<td><font class="fntSmall">&nbsp;</font></td>
		</tr>
		<tr>
			<td><%@ include file="../common/IncludeFormTop.jsp"%>Search	Fare Rules<%@ include file="../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="2" cellspacing="2">
					<tr>
						<td colspan="7"><font class="fntSmall">&nbsp;</font></td>
					</tr>
					<tr>
						<td width="12%"><font>Fare Rule Code</font></td>
						<td width="12%"><label id="fareRuleCode" class="fntBold"></label></td>
						<td width="12%"><font>Charge Type</font></td>
						<td width="15%"><select id="selChargeTypeSH" name="selChargeTypeSH"
							size="1" style="width: 100px">
							<option value="All" selected="selected">All</option>
						</select></td>
						<td width="14%"><font>Compare Against</font></td>
						<td width="15%"><select id="selCompareAgainstSH" name="selCompareAgainstSH"
							size="1" style="width: 100px">
							<option value="All" selected="selected">All</option>
						</select></td>
						<td width="10%"><font>Time Type</font></td>
						<td width="15%"><select id="selTimeTypeSH" name="selTimeTypeSH"
							size="1" style="width: 60px">
							<option value="All" selected="selected">All</option>

						</select></td>
						<td align="right"><input type="button" id="btnSearch" name="btnSearch" value="Search" class="button"/></td>
					</tr>
				</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%></td>
			</tr>
		</table>

		<table width="99%" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround">
		<tr>
			<td><font class="fntSmall">&nbsp;</font></td>
		</tr>
		<tr>
			<td class="FormBackGround"><%@ include file="../common/IncludeFormTop.jsp"%>Overwritten Fare Rules<%@ include file="../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td><font class="fntSmall">&nbsp;</font></td>
					</tr>
					<tr>
						<td>
							<table id="tblOWFarerule"></table>
							<div id="feePager"></div>
						</td>
					</tr>
					<tr>
						<td>
						<u:hasPrivilege privilegeId="plan.fares.rule.add">
							<input type="button" id="btnAdd" name="btnAdd" class="Button" value="Add" />
						</u:hasPrivilege> <u:hasPrivilege privilegeId="plan.fares.rule.edit">
							<input type="button" id="btnEdit" name="btnEdit" class="Button" value="Edit" />
						</u:hasPrivilege> <u:hasPrivilege privilegeId="plan.fares.rule.delete">
							<input type="button" id="btnDelete" name="btnDelete" class="Button" value="Delete" />
						</u:hasPrivilege>
						</td>
					</tr>
				</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%></td>
			</tr>
			</table>
			
			<table width="99%" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround">
		<tr>
			<td><font class="fntSmall">&nbsp;</font></td>
		</tr>
		<tr>
			<td class="FormBackGround"><%@ include file="../common/IncludeFormTop.jsp"%>Add/Edit Overwritten Fare Rules<%@ include file="../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td><font class="fntSmall">&nbsp;</font></td>
					</tr>
					<tr>
						<td>
							<table width="100%" cellpadding="2" cellspacing="2" border="0" id="tblFormData">
								<tr>
									<td width="20%" align="left"><font>Charge Type</font></td>
									<td width="25%" align="left">
										<select id="selChargeType" name="selChargeType"
											size="1" style="width: 60px">
											<!--<option value="-"  selected="selected"></option>

										--></select>
										<font class="mandatory"><b>*</b></font>
									</td>
									<td width="5%">&nbsp;</td>
									<td width="20%" align="left"><font>Compare Against</font></td>
									<td width="25%" align="left">
										<select id="selCompareAgainst" name="selCompareAgainst"
											size="1" style="width: 100px">
											<!--<option value="-"  selected="selected"></option>
										--></select>
										<font class="mandatory"><b>*</b></font>
									</td>
								</tr>
								
								<tr>
									<td width="20%" align="left"><font>Charge Amount Type</font></td>
									<td width="25%" align="left">
										<select id="selChargeAmountType" name="selChargeAmountType"
											size="1" style="width: 60px">
											<!--<option value="-"  selected="selected"></option>
										--></select>
									 <font class="mandatory"><b>*</b></font></td>
									<td width="5%">&nbsp;</td>
									<td width="20%" align="left"><font>Time Type</font></td>
									<td width="25%" align="left">
										<select id="selTimeType" name="selTimeType"
											size="1" style="width: 60px">
											<option value="-"  selected="selected"></option>
										</select>
										<font class="mandatory"><b>*</b></font>
									</td>
								</tr>
								
								<tr>
									<td width="20%" align="left"><font>Charge Amount</font></td>
									<td width="25%" align="left"><input type="text" id="txtChargeAmount" name="txtChargeAmount" /> <font class="mandatory"><b>*</b></font></td>
									<td width="5%">&nbsp;</td>
									<td width="20%" align="left"><font>Time Value</font></td>
									<td width="25%" align="left"><input type="text" id="txtTimeValue" name="txtTimeValue" /><font class="mandatory"><b>*</b></font>
									</td>
								</tr>
								<tr>
									<td width="20%" align="left"><font>Charge Amount in (<c:out value="${requestScope.ruleLocalCurrency}" escapeXml="false" />)</font></td>
									<td width="25%" align="left"><input type="text" id="txtChargeAmountInLocal" name="txtChargeAmountInLocal" /> <font class="mandatory"><b>*</b></font></td>
									<td width="5%">&nbsp;</td>
									<td width="20%" align="left"><font>Status</font></td>
									<td width="25%" align="left">
										<input type="checkbox" id="chkStatus" name="chkStatus" class="NoBorder" value=""/>
									</td>
								</tr>
								
								<tr>
									<td width="20%" align="left"><font>Min Pert Charge Amount</font></td>
									<td width="25%" align="left"><input type="text" id="txtMinPChargeAmount" name="txtMinPChargeAmount" /></td>
									<td width="5%">&nbsp;</td>
									<td width="20%" align="left"><font>Max Pert Charge Amount</font></td>
									<td width="25%" align="left"><input type="text" id="txtMaxPChargeAmount" name="txtMaxPChargeAmount" /></td>
								</tr>
								
								<tr>
									<td width="20%" align="left">&nbsp;</td>
									<td width="25%" align="left">&nbsp;</td>
									<td width="5%">&nbsp;</td>
									<td width="20%" align="left">&nbsp;</td>
									<td width="25%" align="left">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align="right">
							<input type="hidden" id="fareFeeId" name="fareFeeId" value="" /> 
							<input type="button" id="btnSave" name="btnSave" class="Button" value="Save" />
							<input type="button" id="btnReset" name="btnReset" class="Button" value="Reset" />
							<input type="button" id="btnCancel" value="Close" class="Button" onclick="overWriteFareRule.closeMe()"/>	
						</td>
					</tr>
					<tr>
						<td><font class="fntSmall">&nbsp;</font></td>
					</tr>
				</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%></td>
			</tr>
			</table>
			<table width="99%" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround">
							
				<tr>
					<td><font class="fntSmall">&nbsp;</font></td>
				</tr>
				<tr>
						<td><%@ include file="../common/IncludeFormTop.jsp"%>Cutover Time Applicability<%@ include file="../common/IncludeFormHD.jsp"%>
							<table width="100%" border="0" cellpadding="2" cellspacing="2">
								<tr>
									<td colspan="7"><font class="fntSmall">&nbsp;</font></td>
								</tr>
						<tr>
	
							<td width="20%"><font>Apply 12 hrs fee cutover time</font></td>
							<td width="3%"><input type="checkbox" id="chkTwlHrsCutOver" name="chkTwlHrsCutOver" class="NoBorder" value=""/></td>
							<td width="4%" align="left"><input type="button" id="btnTwlCutOverEdit" name="btnTwlCutOverEdit" value="Edit" class="button"/></td>
							<td align="left"><input type="button" id="btnTwlCutOverApply" name="btnTwlCutOverApply" value="Apply" class="button"/></td>
						</tr>
					</table>
					<%@ include file="../common/IncludeFormBottom.jsp"%></td>
				</tr>
		</table>
	<div class="PageBottomColor" style="margin-top:10px">
		<table width="100%" cellspacing="1" cellpadding="0" border="0" id="Table6">
			<tbody><tr>
				<td width="20">&nbsp;</td>
				<td width="20" valign="bottom"><img id="imgError" border="0" src="../../images/Err_no_cache.gif" name="imgError" style="visibility: hidden; display: none;" /></td>
				<td width="1"><img src="../../images/StatusBar_no_cache.jpg" /></td>
				<td id="tdSTBar" class="StatsBar"><span id="spnDate">&nbsp;</span></td>
				<td width="1"><img src="../../images/StatusBar_no_cache.jpg" /></td>
				<td width="150" align="center"><span id="spnUID"></span></td>
				<td width="1"><img src="../../images/StatusBar_no_cache.jpg" /></td>
				<td width="200" align="center"><span id="spnScreenID"><font class="fontPGInfo"></font></span></td>
				<td width="20">&nbsp;</td>
			</tr>
		</tbody></table>
	</div>
		<!--<form action="" name="frmOverwritefare" id="frmOverwritefare" method="post">
		</form>
	--></body>
	<script src="../../js/fareClasses/overWriteFare.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
</html>
