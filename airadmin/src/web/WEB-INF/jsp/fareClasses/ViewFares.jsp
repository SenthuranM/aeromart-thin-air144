<%-- 
	 @Author 	: 	Sumudu
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Template Page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
<LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css">
<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" 	type="text/javascript"></script>
<script	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>
<script	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script	src="../../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" 	type="text/javascript"></script>

<script type="text/javascript">
	var chidVisibility = "<c:out value="${requestScope.childVisibility}" escapeXml="false" />";
	<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	<c:out value="${requestScope.reqViewFareGrid}" escapeXml="false" />
	<c:out value="${requestScope.reqFareSummaryGrid}" escapeXml="false" />
	<c:out value="${requestScope.reqSystemDate}" escapeXml="false" />
	<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
	<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />
	<c:out value="${requestScope.recNo}" escapeXml="false" />
	<c:out value="${requestScope.reqLinkedFaresVisibility}" escapeXml="false" />
	<c:out value="${requestScope.reqSameFareModificationVisibility}" escapeXml="false" />
	
	var totalRes=0;
	var totalChargeRecs=0;
	var baseCurrency ="<c:out value="${requestScope.reqBaseCurrency}" escapeXml="false" />";
	var showFareDefByDepCountryCurrency = "<c:out value="${requestScope.showFareDefByDepCountryCurrency}" escapeXml="false" />";
	var model= new Array();
	<c:out value="${requestScope.reqModelData}" escapeXml="false" />

	var arrFareChargeData= new Array();
	var arrAirports= new Array();
	<c:out value="${requestScope.reqViaList}" escapeXml="false" />

</script>

</head>
<body class="tabBGColor" onkeydown='return Body_onKeyDown(event)' scroll="no" oncontextmenu="return showContextMenu()" onkeypress='return Body_onKeyPress(event)' 
		ondrag='return false' onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
<form action="showFares.action" name="frmViewFares" id="frmViewFares" method="post">
<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeFormTop.jsp"%>Search<%@ include file="../common/IncludeFormHD.jsp"%>
		<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table9">
		<tr>
			<td>
				<table>
					<tr>
						<td align="left"><font>&nbsp;&nbsp;Origin</font></td>
						<td align="center"><img src="../../images/spacer_no_cache.gif" width="75" height="20"/></td>
						<td align="left"><font class="mandatory">*</font></td>
						<td align="center"><img src="../../images/spacer_no_cache.gif" width="50" height="20"/></td>
						<td align="left" width="70"><font>Destination</font></td>
						<td align="center"><img src="../../images/spacer_no_cache.gif" width="75" height="20"/></td>
						<td align="left"><font class="mandatory">*</font></td>
						<td align="center"><img src="../../images/spacer_no_cache.gif" width="5" height="20"/></td>
						<td align="left" width="50"><font>Via 1</font></td>
						<td align="left"><img src="../../images/spacer_no_cache.gif" width="75" height="20"/></td>
						<td align="left" width="40"><font>Via 2</font></td>
						<td ><img src="../../images/spacer_no_cache.gif" width="75" height="20"/></td>
						<td align="left" width="70"><font>Via 3</font></td>
						<td ><img src="../../images/spacer_no_cache.gif" width="75" height="20"/></td>
						<td  align="left" width="60"><font>Via 4</font></td>
						<td ><img src="../../images/spacer_no_cache.gif" width="40" height="20"/></td>
					</tr>
				</table>
		</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0">
					<tr>
						<td align="left"><input type="radio" id="radSOD1" name="radSOD1" class="NoBorder" value="AllComb" tabindex="7"><font>Show All O &amp; D Combinations</font></td>
						<td align="left"><input type="radio" id="radSelected" name="radSOD1" class="NoBorder" value="selectedComb" tabindex="8"><font>Selected O&amp; D Only</font></td>
						<td><font>COS</font></td>
						<td align="left"><select id="selCOS" name="selCOS" size="1" tabindex="9">
							<option value=""></option>
							<c:out value="${requestScope.reqCOSList}" escapeXml="false" />
						</select><font class="mandatory">*</font></td>
						<td><font>Status</font></td>
						<td><select id="selStatus" name="selStatus" size="1" tabindex="10">
							<option value="ACT">Active</option>
							<option value="INA">Inactive</option>
							<option value="All">All</option>
						</select><font class="mandatory">*</font></td>
						<td><font>Fare Basis Code</font></td>
						<td><select id="selBasisCode" name="selBasisCode" size="1" tabindex="11">
							<option value=""></option>
							<c:out value="${requestScope.reqBasisList}" escapeXml="false" />
						</select></td>
						<td><font>Booking Class</font></td>
						<td align="center"><input type="text" id="txtBkingClass" name="txtBkingClass" style="width: 50px;" maxlength="10"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0">			
					<tr>
					
						<td valign="top"><input type="radio" id="rasSF1" name="rasSF1" class="NoBorder" value="EffectiveAll" onBlur="radEffective_click()" tabindex="12"> <font>All Fares</font></td>
						<td valign="top"><input type="radio" id="radAllFares" name="rasSF1" class="NoBorder" value="From" onClick="radEffective_click()" tabindex="13"><font>Effective During</font></td>
						<td><font>From</font></td>
						<td><input type="text" id="txtFromDate" name="txtFromDate" style="width: 75px;" maxlength="10" onBlur="dateChk('txtFromDate')" invalidText="true" tabindex="14"></td>
						<td>
							<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a>
						</td>
						<td><font>To</font></td>
						<td>
						 <input type="text" id="txtToDate" name="txtToDate" style="width: 75px;" maxlength="10" onBlur="dateChk('txtToDate')" invalidText="true" tabindex="15">
						</td>
						<td>	
							<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a></td>
						<td><input type="radio" id="radDt" name="radDt" checked="checked" class="NoBorder" value="VALID" onClick="radClick()"> <font>Dep Date(Out)</font></td>
						<td><input type="radio" id="radDt" name="radDt" class="NoBorder" value="SALES" onClick="radClick()"> <font>Sales Date</font></td>
						<td><input type="radio" id="radDt" name="radDt" class="NoBorder" value="VALIDRT" onClick="radClick()"> <font>Dep Date(In)</font></td>
						<td><font>Fare Rule</font></td>
						<td><input type="text" id="txtFareRule" name="txtFareRule" style="width: 50px;" maxlength="10"></td>
						<td align="right"><input type="button" id="btnSearch" name="btnSearch" class="Button" value="Search" onClick="search_click()" title="Search" tabindex="16"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
		<%@ include file="../common/IncludeFormBottom.jsp"%>
		</td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeFormTop.jsp"%>Fares<%@ include
			file="../common/IncludeFormHD.jsp"%>
		<table width="100%" border="0" cellpadding="0" cellspacing="0"
			ID="Table9">
			<tr>
				<td><font class="fntSmall">&nbsp;</font></td>
			</tr>
			<tr>
				<td><span id="spnSearch"></span></td>
			</tr>
		</table>
		<%@ include file="../common/IncludeFormBottom.jsp"%>
		</td>
	</tr>
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
	<tr>
		<td colspan="10">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="3"><font>Amend Date of Selected Fares </font></td>
				<td>&nbsp;&nbsp;<font>Valid From </font>&nbsp;&nbsp;<input type="text" id="txtExtendFrom" name="txtExtendFrom" style="width: 75px;" maxlength="10"
					onBlur="dateChk('txtExtendFrom')" invalidText="true" tabindex="17"></td>
				<td><a href="javascript:void(0)" onclick="LoadNewCalendar(3,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif"
					border="0"></a></td>
				<td><font>&nbsp;&nbsp;Valid To </font>&nbsp;&nbsp;<input type="text" id="txtExtendTo" name="txtExtendTo" style="width: 75px;" maxlength="10" onBlur="dateChk('txtExtendTo')"
					invalidText="true" tabindex="17"></td>
				<td><a href="javascript:void(0)" onclick="LoadNewCalendar(5,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a></td>
				<td>&nbsp;&nbsp; <input type="button" id="btnExtend" value="Amend Validity" class="Button" style="width: 110px" onClick="extend_click()" title="Amend Validity" tabindex="18">
				</td>
				<td>&nbsp;</td>
				
				<td colspan="4">
					<table id="tblFareTypes">
						<tr>
							<td width="14%" id="tdFareColorMaster" class="ondFareMaster"></td>
							<td><font>&nbsp;Master Fare</font></td>
							<td width="14%" id="tdFareColorLink" class="ondFareLinked"></td>
							<td><font>&nbsp;Linked Fare</font></td>
							<td></td>
						</tr>
					</table>
				</td>
				
				<!-- <td width="3%" id="tdBCAutCls" class="bcStatus03"></td>
				<td><font>&nbsp;System Closed</font></td>
				<td width="3%" id="tdBCAutOpen" class="bcStatus04"></td>
				<td><font>&nbsp;System Opened</font></td>-->
			
				
			</tr>
			
		</table>
		</td>
	</tr>
	<tr>
		<td colspan="10">
		<table width="100%">
			<tr>
				<td colspan="10" width="80%"><u:hasPrivilege privilegeId="plan.fares.setup.add">
					<input type="button" id="btnAdd" name="btnAdd" value="Add Fare" class="Button" onclick="add_click();rule_click()" title="Add New Fare" tabindex="20">
				
				<u:hasPrivilege privilegeId="plan.fares.setup.upload">
					<input style="width:100px;" type="button" id="btnUpload" name="btnUpload" value="Upload Fare(s)" class="Button" onclick="upload_click();" title="Upload New Fare" tabindex="20">				
				</u:hasPrivilege>
				</u:hasPrivilege> <u:hasPrivilege privilegeId="plan.fares.setup.edit">
					<input type="button" id="btnEdit" name="btnEdit" value="Edit" class="Button" onclick="edit_click('Edit')" title="Edit" tabindex="21">
				</u:hasPrivilege> <u:hasPrivilege privilegeId="plan.fares.setup.editrule">
					<input type="button" id="btnEditRule" name="btnEditRule" value="Edit Rule" class="Button" onclick="editRule_click();rule_click('Edit')" title="Edit Fare Rule" tabindex="22">
				</u:hasPrivilege> 
					<input name="btnSave" type="button" class="Button" id="btnSave" value="Save" onClick="save_click()" title="Save" tabindex="23">
					<input type="button" id="btnReset" value="Reset" class="Button" onClick="reset_click()" title="Reset" tabindex="19">
				<td><font>POS</font></td>
				<td width="8%"><select id="selPOS" name="selPOS" size="1" style="width: 60px;">
					<option value=""></option>
					<c:out value="${requestScope.reqStationList}" escapeXml="false" />
				</select></td>
				<td><input type="button" id="btnGetCharges" name="btnGetCharges" value="Get Charges" class="Button" title="Edit Fare Rule" tabindex="22" onClick="searchCharges_click()"></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td width="100%"><%@ include file="../common/IncludeFormTop.jsp"%>Charges<%@ include file="../common/IncludeFormHD.jsp"%> <span id="spnONDFare"></span></td>
		<%@ include file="../common/IncludeFormBottom.jsp"%>
	</tr>
	<tr>
		<td colspan="11" align="right"><input type="button" id="btnClose" class="Button" value="Close" onclick="closeClick()" tabindex="25">
		</td>
	</tr>
</table>
<input type="hidden" id="hdnFareMode" name="hdnFareMode" value="">
<input type="hidden" id="hdnMode" name="hdnMode" value=""> 
<input type="hidden" id="hdnFareRules" name="hdnFareRules" value="">
<input type="hidden" id="hdnRecNo" name="hdnRecNo" value="1"> 
<input type="hidden" id="hdnField" name="hdnField" value=""> 
<input type="hidden" id="hdnFareIDs" name="hdnFareIDs" value=""> 
<input type="hidden" id="hdnVersion" name="hdnVersion" value=""> 
<input type="hidden" id="hdnFaresData" name="hdnFaresData" value="">
<input type="hidden" id="hdnStatus" name="hdnStatus" value="">
<input type="hidden" id="hdnChargeRecNo" name="hdnChargeRecNo" value="">
<input type="hidden" id="hdnOrigin" name="hdnOrigin" value="">
<input type="hidden" id="hdnDestination" name="hdnDestination" value="">
<input type="hidden" id="hdnVia1" name="hdnVia1" value=""> 
<input type="hidden" id="hdnVia2" name="hdnVia2" value=""> 
<input type="hidden" id="hdnVia3" name="hdnVia3" value=""> 
<input type="hidden" id="hdnVia4" name="hdnVia4" value=""> 
<input type="hidden" id="hdnState" name="hdnState" value=""> 
<input type="hidden" id="hdnBk" name="hdnBk" value=""> 
<input type="hidden" id="hdnFID" name="hdnFID" value="">
</form>
<script src="../../js/fareClasses/ValidateViewFares.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
</body>
<script type="text/javascript">
	<!--
		var objProgressCheck = setInterval("ClearProgressbar()", 300);
		function ClearProgressbar(){
			if(objDG!=null){
				if (objDG.loaded){
					clearTimeout(objProgressCheck);
				 	top[2].HideProgress();
				}
			}
		}	    
	   //-->
	</script>
</html>