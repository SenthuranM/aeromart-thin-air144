<%@ page language="java"%>
<%@ include  file="../common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Optimize Seat Allocation</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </head>
  <body class="tabBGColor" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" scroll="no">
	<form name="frmOptimize" id="frmOptimize" method="post" action="showOptimize.action">
			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Optimize Seat Allocation<%@ include file="../common/IncludeFormHD.jsp"%>
					  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
				   			<tr>
				  				<td width="14%"><font>&nbsp;</font></td>
				  				<td width="11%"><font>&nbsp;</font></td>
				  				<td width="14%"><font>&nbsp;</font></td>	
				  				<td width="15%"><font>&nbsp;</font></td>
				  				<td width="11%"><font>&nbsp;</font></td>
				  				<td width="14%"><font>&nbsp;</font></td>
								<td width="14%"><font>&nbsp;</font></td>	
				  			</tr>
							<tr>
				  				<td>
									<font>&nbsp;From Date</font>
								</td>
								<td><input type="text" id="txtFromDate" name="txtFromDate" style="width:62px;" maxlength="10" onBlur="dateChk('txtFromDate')" onKeyUp="KPValidate('txtFromDate');setPageEdit()" onKeyPress="KPValidate('txtFromDate');setPageEdit()"></td>
								<td><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory">&nbsp;*</font></td>
								
								<td>
									<font>&nbsp;To Date</font>
								</td>
								<td><input type="text" id="txtToDate" name="txtToDate" style="width:62px;" maxlength="10" onBlur="dateChk('txtToDate')" onKeyUp="KPValidate('txtToDate');setPageEdit()" onKeyPress="KPValidate('txtToDate');setPageEdit()"></td>
								<td><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory">&nbsp;*</font></td>
								
								<td><font>Flight Number</font></td>
								<td>
								    <input type="text" id="txtFlightp1" name="txtFlightp1" size="6" maxlength="2" style="width:20px;" value="" onKeyUp="setPageEdit();alphaNumericValidate(this);" onKeyPress="setPageEdit();alphaNumericValidate(this);">
									<input type="text" id="txtFlightp2" name="txtFlightp2" size="6" maxlength="4" style="width:35px;" onKeyUp="setPageEdit();alphaNumericValidate(this);" onKeyPress="setPageEdit();alphaNumericValidate(this);">
								</td>
				  			</tr>
							<tr>
								<td><font class="fntBold">&nbsp;&nbsp;&nbsp;O &amp; D</font></td>
								<td><font>Departure</font></td>
								<td>	
									<select name="selDeparture" size="1" id="selDeparture" style="width:55px;">
										<option value="All">All</option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
									</select>
								</td>
								<td><font>&nbsp;&nbsp;Arrival</font></td>
								<td>	
									<select name="selArrival" size="1" id="selArrival" style="width:55px;">
										<option value="All">All</option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
									</select>
								</td>
								<td><font>&nbsp;</font></td>
								<td colspan="2" rowspan="3">
									<select id="selSegment" name="selSegment" multiple size="1" style="width:200px;height:85px">
									</select>
								</td>
				  			</tr> 
							<tr> 
								<td>&nbsp;</td>
								<td><font>Via 1</font></td>
								<td>	
									<select name="selVia1" size="1" id="selVia1" style="width:55px;">
										<option value=""></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
									</select>
								</td>
								<td><font>&nbsp;&nbsp;Via 2</font></td>
								<td>	
									<select name="selVia2" size="1" id="selVia2" style="width:55px;">
										<option value=""></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
									</select>
								</td>
								<td align="center"><a href="javascript:void(0)" onclick="addToList()" title="Add to list"><img src="../../images/AA115_no_cache.gif" border="0"></a></td>
				  			</tr> 
							<tr> 
								<td>&nbsp;</td>
								<td><font>Via 3</font></td>
								<td>	
									<select name="selVia3" size="1" id="selVia3" style="width:55px;">
										<option value=""></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
									</select>
								</td>
								<td><font>&nbsp;&nbsp;Via 4</font></td>
								<td>	
									<select name="selVia4" size="1" id="selVia4" style="width:55px;">
										<option value=""></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
									</select>
								</td>
								<td align="center"><a href="javascript:void(0)" onclick="removeFromList()" title="Remove from list"><img src="../../images/AA114_no_cache.gif" border="0"></a></td> 
				  			</tr> 
				  			<tr>
								<td colspan="2"><font>Logical Cabin Class </font></td>
								<td>	
									<select name="selLogicalCC" size="1" id="selLogicalCC" style="width:120px;">
										<option value=""></option>
										<c:out value="${requestScope.reqLogicalCabinClassList}" escapeXml="false" />
									</select> <font class="mandatory">&nbsp;*</font>
								</td>
				  			</tr>
							<tr>
								<td colspan="8">
									<hr>
								</td>
							</tr>
							<tr>
								<td colspan="7">
									<font class="fntBold">Booking Classes to Display</font>
								</td>
							</tr>
							<tr>
				  				<td>
									<input type="radio" id="radAllBc" name="radAllBc" value="A" class="NoBorder"  onClick="displayBC()">
									<font>All</font>
								</td>
								<td colspan="2" align="center">
									<input type="radio" id="radStandardBc"  name="radAllBc" value="S"  class="NoBorder" onClick="displayBC()" >
									<font>Standard Classes Only</font>
								</td>
								<td colspan="2" align="center">
									<input type="radio" id="radSelBc" name="radAllBc" value="C"  class="NoBorder" onClick="displayBC()">
									<font>Selected Classes Only</font>
								</td>
								<td colspan ="3" rowspan="7" valign="top">
									<span id="spnBC" class="FormBackGround"></span>
								</td>
				  			</tr>
							<tr>
								<td colspan="7">
									<font class="fntbold">&nbsp;</font>
								</td>
				  			</tr>
							<tr>
								<td colspan="7">
									<font class="fntbold">&nbsp;</font>
								</td>
				  			</tr>
							<tr>
								<td colspan="7">
									<font class="fntbold">&nbsp;</font>
								</td>
				  			</tr>
							<tr>
								<td colspan="7">
									<font class="fntbold">&nbsp;</font>
								</td>
				  			</tr>

				  			<tr>
								<td colspan="7" valign="bottom" style="height:40;">
									<font>&nbsp;</font>
								</td>
				  			</tr>
							<tr>
								<td colspan="7">
									<font class="fntbold">Seat Availability</font>
								</td>
				  			</tr>


							<tr>
								<td align="right">
									<input type="radio" id="radAvailable" name="radAvailable" value="AA" class="NoBorder" onClick="chkAvailability()">
								</td>
								<td colspan="2">
									<font>Availability with</font>
								</td>
								<td>
									<select id="selMoreLess" name="selMoreLess" size="1" style="width:120px;">
										<option value="More than">More than</option>
										<option value="Less than">Less than</option>
										<option value="More than or equal">More than or equal</option>
										<option value="Less than or equal">Less than or equal</option>
									</select>
								</td>
								<td colspan="2" align="center">
									<font>Seats</font>
									<input type="text" style='text-align:right;width:40px;' id="txtSeats" name="txtSeats" value="0" size="5" maxlength="3" onKeyUp="KPValidatePositiveInteger('txtSeats')" onKeyPress="KPValidatePositiveInteger('txtSeats')" onKeyUp="setPageEdit()" onKeyPress="setPageEdit()">&nbsp;&nbsp;
								</td>
								<td colspan="2" align="left">
									<input type="radio" id="radSegLevel" name="radSegLevel" value="A" class="NoBorder">
									<font>At Flight/Segment Level</font>

									<input type="radio" id="radBCLevel" name="radSegLevel" value="B" class="NoBorder">
									<font>At BC Level</font>
								</td>
							</tr>
							<tr>
								<td align="right" title="Overloaded/Over booked Flights">
									<input type="radio" id="radNegAvailable" name="radAvailable" value="NA" class="NoBorder" onClick="chkAvailability()">
								</td>
								<td colspan="2" title="Overloaded/Over booked Flights">
									<font>Availability with Negative</font>
								</td>
								<td><font>&nbsp;</font></td>
							</tr>
							<tr>
								<td colspan="7">
									<font class="fntbold">View Allocations by</font>
								</td>
				  			</tr>
				  			<tr>
				  				
								<td colspan="1" align="right">
									<input type="radio" id="radShowBc" name="radShowBc" value="BC" class="NoBorder" onClick="viewAllocations()">
								</td>
								<td colspan="6">
									<font>Booking Class</font>
								</td>
				  			</tr>
				  			<tr>
				  				
								<td colspan="1" align="right">
									<input type="radio" id="radAgent" name="radShowBc"  value="Agent" class="NoBorder" onClick="viewAllocations()">
								</td>
								<td colspan="6">
									<font>In relation to Agent</font>
									<select id="selAgent" name="selAgent" size="1" style="width:150px">
										<option value=""></option>
										<c:out value="${requestScope.reqAgentList}" escapeXml="false" />
									</select>
								</td>
				  			</tr>
				  			<tr>
								<td colspan="7" valign="bottom" style="height:20;">
									<font>&nbsp;</font>
								</td>
				  			</tr>
							<tr>
								<td colspan="7">
									<font class="fntbold">Note : Maximum number of <c:out value="${requestScope.reqOptimizeMaxFlights}" escapeXml="false" /> flights information would be retrieved.</font>
								</td>
				  			</tr>
				  			<tr>
								<td colspan="7" valign="bottom">
									<font>&nbsp;</font>
								</td>
				  			</tr>
				  			<tr>
								<td colspan="5">
									<input type="button" id="btnClose" class="Button" value="Close"  onclick="top[1].objTMenu.tabRemove('SC_INVN_004');">									
								</td>
								<td colspan="3" align="right">
									<u:hasPrivilege privilegeId="plan.invn.opt">
										<input type="button" id="btnContinue" class="Button" value="Continue" onclick="ContinueClick()">
									</u:hasPrivilege>
								</td>
								
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
	<script src="../../js/optimizeseatallocation/OptimizeSeatAllocation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript">
		var defCarrCode = "<c:out value="${requestScope.reqcrriercode}" escapeXml="false" />";
		<c:out value="${requestScope.reqBcHtmlData}" escapeXml="false" />
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
		var arrCarriers = new Array();
			<c:out value="${requestScope.requsercrriercode}" escapeXml="false" />
		if(bcls) {
			bcls.height = '165px';
			bcls.width = '170px';				
			bcls.drawListBox();
		}
		
	</script> 
	<script type="text/javascript">
   	   <!--
	   var objProgressCheck = setInterval("ClearProgressbar()", 300);
	   function ClearProgressbar(){
		    clearTimeout(objProgressCheck);
		    top[2].HideProgress();
	   }
      //-->
    </script>
		<input type="hidden" name="hdnVersion" id="hdnVersion"/> 
		<input type="hidden" name="hdnMode" id="hdnMode"/> 
		<input type="hidden" name="txtFlightNo" id="txtFlightNo"/>
		<input type="hidden" name="hdnAssignOnD" id="hdnAssignOnD"/>	
		<input type="hidden" name="hdnAssignBC" id="hdnAssignBC"/> 	
		<input type="hidden" name="hdnAllocType" id="hdnAllocType"/> 
		<input type="hidden" name="selMoreLess" id="selMoreLess"/> 
		<input type="hidden" name="hdnBCDisType" id="hdnBCDisType"/> 
		<input type="hidden" name="hdnLogicalCC" id="hdnLogicalCC"/>
	</form>
  </body>
</html>