 <%-- 
	 @Author 	: 	Srikanth
	 @Copyright : 	ISA
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Territory Admin</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">	

	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>		
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </head>

  <body class="tabBGColor" scroll="no"  oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown='return Body_onKeyDown(event)' ondrag='return false' 
  	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">

  <form method="post" action="showTerritory.action">
  			<script type="text/javascript">
				<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
				<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />		
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
				var arrFormData = new Array();
				<c:out value="${requestScope.reqFormData}" escapeXml="false" />
				
				var searchData = "<c:out value="${requestScope.reqTerritorySearchData}" escapeXml="false" />";
				<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
				<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
				<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />	

				var arrTerriID = new Array();
				<c:out value='${requestScope.reqTerritoryList}' escapeXml='false' />
				var arrTerriDesc = new Array();
				<c:out value='${requestScope.reqTerritoryList}' escapeXml='false' />
				var defaultAirlineCode = "<c:out value="${requestScope.reqDefaultAirlineCode}" escapeXml="false" />";
				var isRemoteUser = 	"<c:out value="${requestScope.reqIsRemoteUser}" escapeXml="false" />";						
			</script>

			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<td><%@ include file="../common/IncludeFormTop.jsp"%>
					Search Territories <%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="2"
						ID="Table2">
						<tr>
							<td width="15%"><font>Territory ID</td>
							<td width="23%"><font>&nbsp;</font></td>
							<td width="15%"><font>Territory Description</td>
							<td width="23%"><font>&nbsp;</font></td>
							<td width="12%" align="right"><input name="btnSearch"
								type="button" class="button" id="btnSearch" onClick="searchTerritory()"
								value="Search"></td>
						</tr>
					</table>
					<%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>		
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Territories<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									  <span id="spnTerritories"></span>
								</td>
							</tr>
							<tr>
								<td>
									<u:hasPrivilege privilegeId="sys.mas.territory.add">
										<input type="button" id="btnAdd" class="Button" value="Add" onClick="addClick()">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.mas.territory.edit">
										<input type="button" id="btnEdit" class="Button" value="Edit" onClick="editClick()">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.mas.territory.delete">
										<input type="button" id="btnDelete" class="Button" value="Delete" onClick="deleteClick()">
									</u:hasPrivilege>
								</td>
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						Add/Modify Territory 
						<%@ include file="../common/IncludeFormHD.jsp"%>			
							<br>
							  <table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
									<tr>
										<td width="15%"><font>Territory ID</font></td>
										<td><span id="spnCntAirlineCode" style="display: none"><font><span id="spnAirlineCode"></span>&nbsp;&nbsp;</font> </span> <input name="txtTerritoryId" type="text" id="txtTerritoryId" maxlength="10" size="10" class ="UCase" onkeyUp="alphaNumericWithoutSpaceValidate(this)" onkeyPress="alphaNumericWithoutSpaceValidate(this)" onChange="clickChange()"> <font class="mandatory">&nbsp*</font></td>
									</tr>
									<tr>
										<td><font>Territory Description</font></td>
										<td><input name="txtDesc" type="text" id="txtDesc" maxlength="30" size="50" onkeyUp="valDescription(this)" onkeyPress="valDescription(this)" onChange="clickChange()"> <font class="mandatory">&nbsp*</font></td>
									</tr>
									
									<tr>
										<td valign="top"><font>Remarks</font></td>
										<td><textarea name="txtRemarks" id="txtRemarks" cols="47" rows="4" maxlength="255" onkeyUp="validateTA(this,255);commaValidate(this);" onkeyPress="validateTA(this,255);commaValidate(this);"onChange="clickChange()"></textarea></td>
									</tr>
									
									<tr>
										<td><font>Bank</font></td>
										<td><input name="txtBank" type="text" id="txtBank" maxlength="50" size="50"></td>
									</tr>
									
									<tr>
										<td valign="top"><font>Bank Address</font></td>
										<td><textarea name="txtBankAddress" id="txtBankAddress" cols="47" rows="2" maxlength="255" ></textarea></td>
									</tr>
									
									<tr>
										<td><font>Account Number</font></td>
										<td><input name="txtAccountNumber" type="text" id="txtAccountNumber" maxlength="20" size="30" ></td>
									</tr>
									
									<tr>
										<td><font>Swift Code</font></td>
										<td><input name="txtSwiftCode" type="text" id="txtSwiftCode" maxlength="11" size="11" ></td>
									</tr>
									
									<tr>
										<td><font>IBAN Code</font></td>
										<td><input name="txtIBANNo" type="text" id="txtIBANNo" maxlength="30" size="30" ></td>
									</tr>
									
									<tr>
										<td><font>Beneficiary Name</font></td>
										<td><input name="txtBeneficiaryName" type="text" id="txtBeneficiaryName" maxlength="50" size="50" ></td>
									</tr>
									
									<tr>
										<td><font>Active</font></td>
										<td><input type="checkbox" name= "chkStatus" id="chkStatus" "onChange="clickChange()"></td>
									</tr>
									<tr>
									  <td style="height:42px;" colspan="2" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td>
													<input type="button" id="btnClose" class="Button" value="Close"  onclick="top[1].objTMenu.tabRemove(screenId)">
													<input name="btnReset" type="button" class="Button" id="btnReset"  onClick="resetTerritory()" value="Reset">
												 </td>
												 <td align="right">
													<input name="btnSave" type="button" class="Button" id="btnSave" onClick="saveTerritory()" value="Save">
												 </td>
											</tr>
										</table>
									   </td>
									</tr>
							  </table>
							  <%@ include file="../common/IncludeFormBottom.jsp"%>
						 </td>
					</tr>
				  <tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
			

		<input type="hidden" name="hdnVersion"  id="hdnVersion" value=""/>	
		<input type="hidden" name="hdnMode" id="hdnMode"/>
		<input type="hidden" name="hdnAction" id="hdnAction"/>			
		<input type="hidden" name="hdnRecNo" id="hdnRecNo">
		<input type="hidden" name="hdnSearchData" id="hdnSearchData"/> 		
		<script>
		//	fillCombos();
		</script>
	</form>	
	<script src="../../js/territory/TerritoryAdminGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/territory/TerritoryValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </body>

    <script type="text/javascript">
  	<!--
  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
  	function ClearProgressbar(){  		
  		if (typeof(objDG) == "object"){
	  		if (objDG.loaded){
	  			clearTimeout(objProgressCheck);
	  			top[2].HideProgress();
	  		}
	  	}	  	
  	}
  	 
  	//-->
  </script>
  
</html>