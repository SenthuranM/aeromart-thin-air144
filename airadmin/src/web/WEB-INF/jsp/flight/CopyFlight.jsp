<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;
	Date dStopDate = null;
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date
	cal.add(Calendar.MONTH,1);
	dStopDate = cal.getTime(); //Next Month Date
	String CurrentDate =  formatter.format(dStartDate);
	String NextMonthDate = formatter.format(dStopDate);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Copy Flight</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="-1">    
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">

	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
 
	</head>
	<body  oncontextmenu="return false" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="no" onLoad="winOnLoad()">
	<%@ include file="../common/IncludeWindowTop.jsp"%><!-- Page Background Top page -->		
					<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBorder" ID="Table7" style="height:300px;">
						<tr>
							<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
							<td valign="top" align="center" class="PageBackGround">
								<!-- Your Form start here -->								
								<br>
								<table width="98%" cellpadding="0" cellspacing="0" border="0" ID="Table7">
								<tr>
									<td><font class="Header">Copy Flight </font></td>
								</tr>
								<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
								<tr>
									<td><%@ include file="../common/IncludeMandatoryText.jsp"%>
									</td>
								</tr>
									<tr>
										<td>
										<%@ include file="../common/IncludeFormTop.jsp"%>
										Copy Flight<%@ include file="../common/IncludeFormHD.jsp"%>
											<table width="100%" border="0" cellpadding="2" cellspacing="2" ID="Table2">
											
												<tr>
													<td><font>Flight Id</font></td>
													<td><font>From/To</font></td>
													<td><font>Departure Date</font></td>
													<td><font>Overlap Flight Id</font></td>
												</tr>
												<tr>
													<td><font><span id="spnFlightId"></span></font></td>	
													<td><font><span id="spnFlightseg"></span></font></td>	
													<td><font><span id="spnDeptDate"></span></font></td>	
													<td><font><span id="spnOlapFlightId"></span></font></td>	
												</tr>
												<tr>
													<td colspan="4">&nbsp;</td>
												</tr>
												<tr>
													<td ><font>Copy To Date</font></td>
													<td  width="80"><input type="text" id="txtCopyToDate" style="width:75px;"  invalidText="true" maxlength="10" name="txtCopyToDate" onBlur="dateChk('txtCopyToDate')" onchange="dataChanged()"></td>
													<td colspan="2"><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="View Calendar"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font></td>
												</tr>												
												
												<tr>
													<td colspan="4"><hr></td>
												</tr>
												<tr>
													<td colspan="3">
														<input type="button" id="btnCancel" value="Cancel" class="Button" onclick="cancelClick()" name="btnCancel">
													</td>
													<td align="right">
														<input type="button" id="btnConfirm" value="Confirm" class="Button" name="btnConfirm" onclick="confirmCopy()">
													</td>
												</tr>
											</table>
										<%@ include file="../common/IncludeFormBottom.jsp"%>
										</td>
									</tr>
								</table>
								<input type="hidden" name="hdnMode" id="hdnMode"  value=""/>
								<input type="hidden" name="hdnCurrentDate" id="hdnCurrentDate" value="<%=CurrentDate %>">
								<input type="hidden" name="hdnStartD" id="hdnStartD" value="<%=request.getParameter("strFltDate")%>"/>
								<input type="hidden" name="hdnCopyStartD" id="hdnCopyStartD"  value=""/>								
								<input type="hidden" name="hdnFlightId" id="hdnFlightId" value="<%=request.getParameter("strFlightId")%>"/>
								<input type="hidden" name="hdnTrip" id="hdnTrip" value="<%=request.getParameter("strTrip")%>"/>
								<input type="hidden" name="hdnTrip" id="hdnOverlapFlightId" value="<%=request.getParameter("strOverlapFltID")%>"/>
								
							<!-- Your Form ends here -->
							</td>
							<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
						</tr>
					</table>				
		<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
		<script src="../../js/flight/CopyFlight.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	</body>
</html>
