 <%-- 
	 @Author 	: 	Shakir
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </head>
  <body oncontextmenu="return false" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="yes">
   <form id="frmFlightCancel" name="frmFlightCancel">

   <%@ include file="../common/IncludeWindowTop.jsp"%><!-- Page Background Top page -->
   <table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td><font class="Header">Confirm Flight Updation</font></td>
		</tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td>
			<%@ include file="../common/IncludeFormTop.jsp"%>&nbsp;<%@ include file="../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table9">
					<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
					<tr>
						<td>
							<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table10">
							<tr>								
								<td>
									<font class="fntBold">Send Alerts - Select Options</font>
								</td>
								<td>
									<font class="fntBold">Alerts</font>
								</td>								
								<td>
									<font class="fntBold">Group Mail</font>
								</td>
								<td>
									<font class="fntBold">Email PAX</font>
								</td>
								<td>
									<font class="fntBold">SMS PAX</font>
								</td>
							</tr>
							<tr>
								<td>
									<font>Rescheduled Flights</font>
								</td>
								<td>
									<input type="checkbox" id="chkGA1" name="chkGA1" class="NoBorder" onClick="checkClick()">
								</td>								
								<td>
									<input type="checkbox" id="chkSE1" name="chkSE1" class="NoBorder" onClick="checkClick()">
								</td>
								<td>
									<input type="checkbox" id="chkSendEmail" name="chkSendEmail" class="NoBorder"  onClick="" value="true">
								</td>
								<td>
									<input type="checkbox" id="chkSendSMS" name="chkSendSMS" class="NoBorder"  onClick="enableSMSChk()" value="true">
								</td>
								
							</tr>
							
							<!-- <tr>
								<td> <font>SMS PAX</font> </td>
								<td><input type="checkbox" id="chkSendSMS" name="chkSendSMS" class="NoBorder"  onClick="enableSMSChk()" value="true"></td>
								<td><font> Email PAX </font></td>
								<td><input type="checkbox" id="chkSendEmail" name="chkSendEmail" class="NoBorder"  onClick="" value="true"></td>
							</tr> -->
						
							<tr>
								<td><font></font> </td>
								<td> <font>SMS CNF PAX</font> </td>
								<td><input type="checkbox" id="chkSmsCnf" name="chkSmsCnf" class="NoBorder"  onClick="" value="true"></td>
								<td><font> SMS ON HOLD PAX</font></td>
								<td><input type="checkbox" id="chkSmsOnH" name="chkSmsOnH" class="NoBorder"  onClick="" value="true"></td>
							</tr>
								<c:if test="${requestScope.showETOpeningSection == true}">
									<tr id="chkETOpen">
										<td></td>
										<td>
											<input type="checkbox" id="chkOpenEtickets"
												   name="chkOpenEtickets"
												   class="NoBorder"
												   value="true" />&nbsp;<font class="font">OPN All
											Etickets </font>
										</td>
									</tr>
								</c:if>
							<tr>
								<td colspan="3">
									<div id="divEmailDesc">
									<span id="spnEmail"></span>
									</div>
								</td>
							</tr>						
							</table>
						</td>
					</tr>
					<tr><td colspan="3">&nbsp;</td></tr>
					<tr>
						<td colspan="3">
							<table width="100%" border="0" cellpadding="0" cellspacing="4">
								<tr>
									<td width="15%">
										<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
									</td>
									<td>
										<div id="divClearButton">
											<input type="button" id="btnClear" name="btnClear" value="Clear Email" class="Button" onclick="clearClick()">
										</div>
									</td>
									<td align="right">
										<input name="btnConfirm" type="button" class="Button" id="btnConfirm" value="Confirm"  title="Confirm Updation" onClick="confirmClick()">
									</td>
								</tr>
							</table>	
						</td>
					</tr>
				</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
	</table>
	<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
	<input type="hidden" name="hdnMode" id="hdnMode" value=""/>
	<input type="hidden" name="hdnFromPage" id="hdnFromPage" value="<%=request.getParameter("strFromPage")%>"/>
  </form>
  	<script src="../../js/flight/validateAmendFlight.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </body>
</html>