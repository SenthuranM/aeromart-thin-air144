 <%-- 
	 @Author 	: 	Shakir
  --%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </head>
  <body oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="no"
  		onLoad="cancelOnLoad()">
   <form id="frmFlightCancel" name="frmFlightCancel">

  

   <%@ include file="../common/IncludeWindowTop.jsp"%><!-- Page Background Top page -->
   <table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td><font class="Header">Confirm Flight Cancellation</font></td>
		</tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td>
				<%@ include file="../common/IncludeMandatoryText.jsp"%>
			</td>
		</tr>				
		<tr>
			<td>
				<%@ include file="../common/IncludeFormTop.jsp"%>Flight Details<%@ include file="../common/IncludeFormHD.jsp"%>
		  	<table width="100%" border="0" cellpadding="0" cellspacing="0">
  				<tr>
					<td>
						<font class="fntBold">Flight ID</font>
						<font><span id="spnFlightID"></span><font>
					</td>
					<td>
						<font class="fntBold">Start Date</font>
						<font><span id="spnDepDate"></span><font>
					</td>
					<td>&nbsp;
						<!--font class="fntBold">Stop Date</font>
						<font><span id="spnEndD" name="spnEndD"></span><font-->
					</td>
				<tr>
				<tr>
					<td>
						<font class="fntBold">Flight Number</font>
						<font><span id="spnFlightNo"></span><font>
					</td>
					<td>
						<font class="fntBold">Destination</font>
						<font><span id="spnDest"></span><font>
					</td>
					<td>
						<font class="fntBold">Origin</font>
						<font><span id="spnOrg"></span><font>
					</td>
				<tr>
		  	</table>
			<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td>
			<%@ include file="../common/IncludeFormTop.jsp"%>List of Flight Reservations to be Cancelled
			 <%@ include file="../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table9">
					<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
					<tr>
						<td colspan="2">
							<span id="spnCancel" style="height:210px;"></span>
						</td>
					</tr>
					<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
					<tr>
						<td colspan="2">
							<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table10">
							<tr>								
								<td>
									<font class="fntBold">Send Alerts - Select Options</font>
								</td>
								<td>
									<font class="fntBold">Generate Alerts</font>
								</td>								
								<td>
									<font class="fntBold">Send EMail</font>
								</td>
							</tr>
							<tr>
								<td>
									<font>Cancelled Flights</font>
								</td>
								<td>
									<input type="checkbox" id="chkGA1" name="chkGA1" class="NoBorder" onClick="checkClick()">
								</td>								
								<td>
									<input type="checkbox" id="chkSE1" name="chkSE1" class="NoBorder" onClick="checkClick()">
								</td>
							</tr>							
							</table>
						</td>
					</tr>
					<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
					<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
					<tr>
						<td>
							<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
						</td>
						<td align="right">
							<input name="btnReprotect" type="button" class="Button" id="btnReprotect" style="width:130px"  title="Confirm Cancellation and proceed to re-protect" value="Confirm & Re-protect" onClick="reprotectClick()">
							<input name="btnConfirm" type="button" class="Button" id="btnConfirm" value="Confirm"  title="Confirm Cancellation without re-protecting" onClick="confirmClick()">
						</td>
					</tr>
				</table>
				<%@ include file="../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
	</table>
	<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
	<input type="hidden" name="hdnMode" id="hdnMode" value=""/>
	<input type="hidden" name="hdnFromPage" id="hdnFromPage" value="<%=request.getParameter("strFromPage")%>"/>
  </form>
  	<script src="../../js/flight/validateFlightCancel.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </body>
</html>