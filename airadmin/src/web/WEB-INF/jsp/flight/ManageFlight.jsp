<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Manage Flights</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </head>
  <body oncontextmenu="return false" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="no">
  <script type="text/javascript">
  var hdnScheduleID = "";
  var hdnNoOptions = false;
  hdnScheduleID = "<c:out value="${requestScope.reqSchdId}" escapeXml="false" />";
  hdnNoOptions  = "<c:out value="${requestScope.reqNoOptions}" escapeXml="false" />";
  </script>
  <form method="post" name="frmFlight" id="frmFlight" action="manageFlight.action">
	<table width="100%" cellpadding="0" cellspacing="0" border="0" ID="Table7">
					<tr>
						 
						<td valign="top" align="center" class="tabBGColor">
						<!-- Your Form start here -->
							<form method="post" action="showFlight.action">
							 
							<table width="100%" cellpadding="0" cellspacing="2" border="0" ID="Table7">
								 
								<tr>
									<td valign="top" width="50%">
									<%@ include file="../common/IncludeFormTop.jsp"%>Schedule Information<%@ include file="../common/IncludeFormHD.jsp"%>
										<table width="98%" border="0" cellpadding="0" cellspacing="0">
											<!--  # -->
											<c:if test="${requestScope.reqBelongstoShed == 'true'}">		 
			 									<tr><td><font>Schedule ID&nbsp;</font><font class="fntBold"><c:out value="${requestScope.reqSchdId}" escapeXml="false" /></font></td></tr>
			 								</c:if>
			 								<c:if test="${requestScope.reqBelongstoShed != 'true'}">
			 								<tr><td><font class="fntBold">Modifying Individual Flight(s)</font></td></tr>
			 								</c:if>		
											<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
											<tr>
												<td valign="top">
												
													<table width="98%" border="0" cellpadding="0" cellspacing="0" ID="Table9">
																<tr><td align="center"><font>
																<table width="98%" border="0" cellpadding="0" cellspacing="1">
																	<tr>
																		<td width="25%" class="GridItemRow GridItem GridItemHover">
																			<font></font>
																		</td>
																		<td width="20%" align="center" class="GridItemRow GridItem GridItemHover">
																			<font class="fntBold"><img src="../../images/act_status_no_cache.gif">&nbsp;Active</font>
																		</td>
																		<td width="20%" align="center" class="GridItemRow GridItem GridItemHover">
																			<font class="fntBold"><img src="../../images/cls_status_no_cache.gif">&nbsp;Closed</font>
																		</td>
																		<td width="20%" align="center" class="GridItemRow GridItem GridItemHover">
																			<font class="fntBold"><img src="../../images/cnx_status_no_cache.gif">&nbsp;Canceled</font>
																		</td>
																	</tr>
																	<tr>
																		<td align="left" class="GridItemRow GridItem GridItemHover">
																			<font class="fntBold">Selected Flights</font>
																		</td>
																		<td align="right" class="GridItemRow GridItem"> 
																			<font><c:out value="${requestScope.reqActCounts}" escapeXml="false" />&nbsp;</font>
																		</td>
																		<td align="right" class="GridItemRow GridItem">
																			<font><c:out value="${requestScope.reqInaCounts}" escapeXml="false" />&nbsp;</font>
																		</td>
																		<td align="right" class="GridItemRow GridItem">
																			<font><c:out value="${requestScope.reqCnxCounts}" escapeXml="false" />&nbsp;</font>
																		</td>
																	</tr>
																	<c:if test="${requestScope.reqBelongstoShed == 'true'}">		
																		<tr>
																			<td align="left" class="GridItemRow GridItem GridItemHover">
																				<font class="fntBold">Total in Schedule</font>
																			</td>
																			<td align="right" class="GridItemRow GridItem">
																				<font><font><c:out value="${requestScope.reqTotalActCounts}" escapeXml="false" />&nbsp;</font></font>
																			</td>
																			<td align="right" class="GridItemRow GridItem">
																				<font><font><c:out value="${requestScope.reqTotalInaCounts}" escapeXml="false" />&nbsp;</font></font>
																			</td>
																			<td align="right" class="GridItemRow GridItem">
																				<font><font><c:out value="${requestScope.reqTotalCnxCounts}" escapeXml="false" />&nbsp;</font></font>
																			</td>
																		</tr>
																	</c:if>
																</table>
																</font></td></tr>
																
													 </table>
															</td>
											</tr>
											<tr><td>&nbsp;</td></tr>
												<tr><td><span id="spnMngFlightDetails"></span></td></tr>
											<tr>
												<td>
													
													
												</td>
											</tr>
										</table>
									<%@ include file="../common/IncludeFormBottom.jsp"%>
									</td>
									<td></td>
									<td valign="top" width="50%">
									<%@ include file="../common/IncludeFormTop.jsp"%>Options<%@ include file="../common/IncludeFormHD.jsp"%>
									<table width="95%" cellspacing="2" height="100%">
																<c:if test="${requestScope.reqNoOptions == 'true'}">
																<tr><td align="center"><br>
																	<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="15%" valign="middle" align="right">
																	<img src="../../images/alert_no_cache.png" border="0"></td><td width="85%" align="center"><font class="fntBold">&nbsp;<c:out value="${requestScope.reqUserMsg}" escapeXml="false" /></font></td>
																	</tr></table><br>
																</td></tr>
																</c:if>
																 
																<c:if test="${requestScope.reqCanAct == 'true'}">
																<!-- opt 1 make flights ACT -->
																	<tr><td><font><input type="checkbox" name="radActOpnSel" id="radActOpnSel" value="1" onclick="handleSimpleDependnt('radActOpnSel')">&nbsp;Open selected closed flights</font></td></tr>
																 
																</c:if>
																
																<!-- opt 1 make flights INA -->
																<c:if test="${requestScope.reqCanCls == 'true'}">
																	<tr><td><font><input type="checkbox" name="radActClsSel" id="radActClsSel" value="3" onclick="handleSimpleDependnt('radActClsSel')">&nbsp;Close selected open flights</font></td></tr>
																 
																</c:if>
																<c:if test="${requestScope.reqCanActScd == 'true'}">
																<tr><td><font><input type="checkbox" name="radActOpenAll" id="radActOpenAll" value="2" onclick="handleDependancies('radActOpenAll')">&nbsp;Open all closed flights in schedule</font></td></tr>
																 
																</c:if>
																<c:if test="${requestScope.reqCanClsScd == 'true'}">
																<tr><td><font><input type="checkbox" name="radActClsAll" id="radActClsAll" value="4" onclick="handleDependancies('radActClsAll')">&nbsp;Close all open flights in schedule</font></td></tr>
																 
																</c:if>
																<c:if test="${requestScope.reqCanSkip == 'true'}">
																
																</c:if>
															 
																 
																<c:if test="${requestScope.reqCanActCnx == 'true'}">
																	<u:hasPrivilege privilegeId="plan.flight.reactivate">			
																		<tr><td><font><input type="checkbox" name="chkreinst" id="chkreinst" value="1">Re-Instate selected canceled flight(s) </font></td></tr>	
																    </u:hasPrivilege>
																</c:if>
																
																<c:if test="${requestScope.reqOpnClsFlt == 'true'}">
																	<tr><td><font><input type="checkbox" name="radActSysClsSel" id="radActSysClsSel" value="1" onclick="handleSimpleDependnt('hdnradActSysClsSel')">&nbsp;Open selected closed flights</font></td></tr>
																</c:if>
																			
									</table>
									<%@ include file="../common/IncludeFormBottom.jsp"%>
									</td>
								</tr>
								<tr>
									<td colspan="3" valign="top">
									<%@ include file="../common/IncludeFormTopFrameLess.jsp"%>
										<table width="100%" border="0" cellpadding="0" cellspacing="3">
														<tr>
															<td width="15%">
																<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
															</td>
															<td>
																
															</td>
															<td align="right">
																<input name="btnConfirm" type="button" class="Button" id="btnConfirm" value="Confirm"  title="Confirm Updation" onClick="confirmClick()">
															</td>
														</tr>
										</table>	
										 <%@ include file="../common/IncludeFormBottom.jsp"%>
									</td>
								</tr>
							</table>
					<input type="hidden" name="hdnMode" id="hdnMode"  value=""/>
					
						</td>
						
					</tr>
				</table>	
				</form>
	<input type="hidden" name="hdnMode" id="hdnMode" value=""/>
	<input type="hidden" name="hdnFromPage" id="hdnFromPage" value="<%=request.getParameter("strFromPage")%>"/>
  
  	<script src="../../js/flight/manageFlight.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </body>
</html>