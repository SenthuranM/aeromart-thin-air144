<%@ page language="java"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;
	Date dStopDate = null;
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date
	cal.add(Calendar.MONTH,1);
	dStopDate = cal.getTime(); //Next Month Date
	String StartDate =  formatter.format(dStartDate);
	String StopDate = formatter.format(dStopDate);
%>
<html>
  <head>
  <meta http-equiv="pragma" content="no-cache">
  <meta http-equiv="cache-control" content="no-cache">
  <meta http-equiv="expires" content="-1">  
  <LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">	
  <LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
  <LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">	
  
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
 
 	<!-- JQUERY STUFF -->
 	
 	<script type="text/javascript" src="../../js/v2/jquery/jquery.js "></script>
 	<script src="../../js/v2/jquery/jquery-migrate.js" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js "></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
  	<script src="../../js/flight/UserNoteRollFwd.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  	
<script type="text/javascript">
	$(function() {
		$("#tabs").tabs(); 
		$(".ui-tabs-panel").css ('padding', 4);
		$(".ui-tabs").css ('padding', 0);
		
		$("#tabs").tabs( "disable" , 1);
	});
	<c:out value="${requestScope.isNeedChecked}" escapeXml="false" />
	</script>
 
 
 </head>
	<body class="tabBGColor" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" ondrag="return false" onkeydown="return Body_onKeyDown(event)" onbeforeunload="beforeUnload()"
	style='overflow-y:auto;overflow-x:hidden'>
		<form method="post" name="frmFlight" id="frmFlight" action="showFlight.action">
	 
	  		<script type="text/javascript">
	  			var isFlightUserNoteEnabled = <c:out value="${requestScope.reqUserNote}" escapeXml="false" />;
		  		var flightData = new Array();
				<c:out value="${requestScope.reqFlightData}" escapeXml="false" />
				<c:out value="${requestScope.sesOnlineActiveAirportCodeListData}" escapeXml="false" />
				<c:out value="${requestScope.sesMinStopOverTimeData}" escapeXml="false" />				
				<c:out value="${requestScope.sesAircraftModelDetails}" escapeXml="false" />
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
				<c:out value="${requestScope.sesFltData}" escapeXml="false" />
				var reqFlightsToBeReprotected = <c:out value="${requestScope.reqFlightsToBeReprotected}" escapeXml="false" />;
				var arrOnlineAirports = new Array();
				<c:out value="${requestScope.sesOnlineAirportCodeListData}" escapeXml="false" />
				
				var totalNoOfRecords = "<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />";			
				
				var defaultOperationType = "<c:out value="${requestScope.reqDefaultOperationType}" escapeXml="false" />";
				var defaultStatus = "<c:out value="${requestScope.reqFlightDefStatus}" escapeXml="false" />";
				var defaultclassofservice = "<c:out value="${requestScope.reqclassofservice}" escapeXml="false" />";
				var defCarrCode = "<c:out value="${requestScope.reqcrriercode}" escapeXml="false" />";
				var minTimeallowed = "<c:out value="${requestScope.reqMinTineForSched}" escapeXml="false" />";
				var popupmessage = "<c:out value="${requestScope.popupmessage}" escapeXml="false" />";
				var durError = "<c:out value="${requestScope.durationError}" escapeXml="false" />";
				var hideLogicalCC = "<c:out value="${requestScope.hideLogicalCC}" escapeXml="false" />";
				var hideWaitListing = "<c:out value="${requestScope.hideWaitListing}" escapeXml="false" />";
	
				<c:out value="${requestScope.reqSeatsToBeReprotected}" escapeXml="false" />
				var isFlightNeedReprotect = "<c:out value="${requestScope.reqcancel}" escapeXml="false" />";
				
				var reproFlightId = "<c:out value="${requestScope.reqRepFltId}" escapeXml="false" />";
				var reproFltNum = "<c:out value="${requestScope.reqRepFltNum}" escapeXml="false" />";
				var reproFltDepDate = "<c:out value="${requestScope.reqRepDepDate}" escapeXml="false" />";
				var reproFltDest = "<c:out value="${requestScope.reqRepFltDest}" escapeXml="false" />";
				var reproFltOrg = "<c:out value="${requestScope.reqRepFltOrg}" escapeXml="false" />";
				var privAddOnlyOpFlights = "<c:out value="${requestScope.reqAddOperationsOnly}" escapeXml="false" />";
				
				var isInManageFltMode ="";
				isInManageFltMode = "<c:out value="${requestScope.reqMngFlt}" escapeXml="false" />";
				var arrCarriers = new Array();
				<c:out value="${requestScope.requsercrriercode}" escapeXml="false" />
				var isEditFlightNo = "<c:out value="${requestScope.reqEditFltNumber}" escapeXml="false" />"; 
				var isDelFlownFlts = "<c:out value="${requestScope.reqDelFlownFlt}" escapeXml="false" />"; 
				<c:out value="${requestScope.reqTimeIn}" escapeXml="false" />
				<c:out value="${requestScope.reqMultiFltTypesAvailability}" escapeXml="false" />	
				<c:out value="${requestScope.sesOperationTypeDetails}" escapeXml="false" />
				var privActivatePastFlights = "<c:out value="${requestScope.reqActivatePastFlights}" escapeXml="false" />";
				var pastFlightActivationDurationInDays = "<c:out value="${requestScope.reqPastFlightActivationDurationInDays}" escapeXml="false" />";
				var modificationForPastFlight = <c:out value="${requestScope.reqTimeChangeCancellationAndReprotectionForPastFlights}" escapeXml="false" />;
				var seatTemplates = "<c:out value="${requestScope.reqSeatTempl}" escapeXml="false" />";
				var isCodeShareEnable = "<c:out value="${requestScope.reqCodeShareEnable}" escapeXml="false" />";
				var gdsCarrierCodeArray = new Array();
				<c:out value="${requestScope.gdsCarrierCodeArray}" escapeXml="false" />				

			</script>
  	
			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">				
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>

				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Flight Search<%@ include file="../common/IncludeFormHD.jsp"%>
							<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table2">
								<tr>
									<td colspan="2"><font>Start Date - Zulu</font></td>									
									<td colspan="2"><font>Stop Date - Zulu</font></td>									
									<td valign="bottom" width="20"><a href="javascript:PreviousMonth()" title="Previous month"><img src="../../images/UArrow_no_cache.gif" border="0"></a></td>
									<td><font>Flight No</font></td>
									<td><font>From</font></td>
									<td><font>To</font></td>
									<td><font>Operation Type</font></td>
									<td><font>Status</font></td>	
									<td><div id="lblFlightType"><font>Flight Type</font></div></td>							
								</tr>
								<tr>
									<td style="width:75px;">
										<input type="text" name="txtStartDateSearch" id="txtStartDateSearch"  invalidText="true"  onBlur="settingValidation('txtStartDateSearch')" value=<%=StartDate %> style="width:70px;"  maxlength="10" tabIndex="1">
									</td>
									<td>
										<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="View Calendar" tabIndex="2"><img src="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font>
									</td>
									<td style="width:75px;">
										<input type="text" name="txtStopDateSearch" id="txtStopDateSearch" invalidText="true" onBlur="settingValidation('txtStopDateSearch')" value=<%=StopDate %> style="width:70px;" maxlength="10" tabIndex="3">
									</td>
									<td>
										<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font>
									</td>
									<td valign="top">
										<a href="javascript:NextMonth()" title="Next month"><img src="../../images/DArrow_no_cache.gif" border="0"></a>
									</td>
									<td>
										<input type="text" id="txtFlightNoStartSearch" name="txtFlightNoStartSearch" maxlength="2" style="width:20px;text-transform:uppercase" value="" tabIndex="7">						
										<input type="text" id="txtFlightNoSearch" name="txtFlightNoSearch" maxlength="<c:out value="${requestScope.reqFltNumberLength}" escapeXml="false" />" style="width:50px" tabIndex="8">									
									</td>
									<td>
										<select name="selFromStn6" id="selFromStn6" size="1" style="width:60px" title="Origin" tabIndex="9">
										</select>
									</td>
									<td>
										<select name="selToStn6" id="selToStn6" size="1" style="width:60px" title="Destination" tabIndex="10">										
										</select>
									</td>
									<td>
										<select name="selOperationTypeSearch" id="selOperationTypeSearch" size="1" tabIndex="11">
											<c:out value="${requestScope.sesOperationTypeListData}" escapeXml="false" />
											<option>All</option>
										</select><font class="mandatory"> &nbsp;* </font>
									</td>
									<td>
										<select name="selStatusSearch" id="selStatusSearch" size="1" title="Flight Status" tabIndex="12">
											<c:out value="${requestScope.sesFlightStatusData}" escapeXml="false" />
											<option value="All">All</option>										
										</select><font class="mandatory"> &nbsp;* </font>
									</td>
									<td>
										<div id="divFlightTypeSearch">
											<select name="selFlightTypeSearch" id="selFlightTypeSearch" size="1" title="Flight Type" tabIndex="13">
												<option value="All">All</option>
												<c:out value="${requestScope.reqFlightTypes}" escapeXml="false" />
											</select><font class="mandatory"> &nbsp;* </font>
										</div>	
									</td>
									<td>
										<input type="button" id="btnSearch"  name="btnSearch" value="Search" class="button" onclick="SearchData()" tabIndex="14">
									</td>
								</tr>								
								<tr>
									<table width="60%" border="0" cellpadding="0" cellspacing="2" ID="tblOptSearch">
									<tr>
									<td colspan="5"><span id="spnFrequency"></span></td>	
																			
<!--											<td style="width:100px;">Start Time (HH:MM)</td>-->
<!--											<td>-->
<!--												<input type="text" name="txtStartTime" id="txtStartTime"  invalidText="true"  onBlur="setTimeWithColon(document.forms[0].txtStartTime)" value='00:00' style="width:40px"  maxlength="10" tabIndex="12">-->
<!--											</td>-->
<!--										-->
<!--										-->
<!--											<td style="width:90px;">End Time (HH:MM)</td>-->
<!--											<td>-->
<!--												<input type="text" name="txtEndTime" id="txtEndTime"  invalidText="true"  onBlur="setTimeWithColon(document.forms[0].txtEndTime)" value='23:59' style="width:40px"  maxlength="10" tabIndex="13">-->
<!--											</td>-->
										
									
									<td style="width:70px;">Include Only Scheduled</td>
									<td><input name="chkOnlySche" type="checkbox" id="chkOnlySche"></td>														
									</tr>
									</table>
								</tr>														
							</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
						</td>
					</tr>	
					<tr>
						<td style="height:2px;" colspan="8">						
						</td>
					</tr>					
					<tr>	
						<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Flight Search Results<%@ include file="../common/IncludeFormHD.jsp"%>
				  			<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">
								<tr>
									<td>									
	  									<span id="spnInstances"></span>  									
									</td>
								</tr>
								<tr>
									<td>
										<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td>
													<c:if test="${requestScope.isManageFlt != 'true'}">
													</c:if>
														<u:hasPrivilege privilegeId="plan.flight.flt.add,plan.flight.flt.add.optype.operations">
															<input type="button" id="btnAdd"  name="btnAdd" class="Button" value="Add" onClick="addClick()" tabIndex="16">
														</u:hasPrivilege>
														<u:hasPrivilege privilegeId="plan.flight.flt.edit">
															<input type="button" id="btnEdit"  name="btnEdit" class="Button" value="Edit" onClick="editClick()" tabIndex="17">
														</u:hasPrivilege>
													
													<u:hasPrivilege privilegeId="plan.flight.flt.cancel">
														<input type="button" id="btnCancel"  name="btnCancel" class="Button" value="Cancel" onclick="cancelClick()" tabIndex="18">
													</u:hasPrivilege>
													<u:hasPrivilege privilegeId="plan.flight.flt.copy">
														<input type="button" id="btnCopy"  name="btnCopy" class="Button" value="Copy" onclick="copyClick()" tabIndex="19">
													</u:hasPrivilege>
													 <c:if test="${requestScope.isManageFlt != 'true'}">								
														<u:hasPrivilege privilegeId="plan.invn.alloc">
															<input type="button" id="btnSeatAllocate"  name="btnSeatAllocate" class="Button"  onClick="seatAllocation()" value="Seat Allocation" style="width:105px;" tabIndex="20">
														</u:hasPrivilege>
													</c:if>
													<u:hasPrivilege privilegeId="plan.flight.flt.transfer">
														<input type="button" id="btnReprotectPAX"  name="btnReprotectPAX" class="Button" value="Re-Protect/Transfer PAX" style="width:180px;" onclick="reprotectClick_v2()" tabIndex="21">				
													</u:hasPrivilege>
													<u:hasPrivilege privilegeId="plan.flight.flt.gds">
														<input type="button" name="btnGDS" id="btnGDS" value="GDS" class="Button"  onclick="GDSPublishDetails()" tabindex="58">
													</u:hasPrivilege>
												 
												 	<u:hasPrivilege privilegeId="plan.flight.flt.mgt">
														<input type="button" id="btnManageFlight"  name="btnManageFlight" class="Button" value="Manage Flights" onclick="manageFlight()" tabIndex="59" style="width:110px;">
												 	</u:hasPrivilege>
													 
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
				  			<%@ include file="../common/IncludeFormBottom.jsp"%>
						</td>
					</tr>
					<tr>
						<td style="height:1px;" colspan="8">						
						</td>
					</tr>
					<!--  new row -->
					 
					<tr> 
						<td>
							<div id="tabs">
							    <ul>
									<li><a href="#tabs-1">Flight Details</a></li>
									<u:hasPrivilege privilegeId="plan.flight.flt.mgt">
										<li><a href="#tabs-2">Manage Flights</a></li>
								 	</u:hasPrivilege>
								</ul>
							    <div id="tabs-1">
								      <!--  start normal data -->
								      <!-- start new row -->
					 <table cellspacing="0" align="center">
					<tr>
						<td>
						
							<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table5" align="center">
								<tr>
									<td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td>
									<td width="442" height="20" class="FormHeadBackGround"><img src="../../images//bullet_small_no_cache.gif">
										<font class="FormHeader">Add/Modify Flight</font></td>
									<td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td>
									<td width="7" height="20"></td>
									<td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td>
									<td width="242" height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif">
										<font class="FormHeader">Leg Details</font></td>
									<td class="FormHeadBackGround"><font class="FormHeader">Time In</font> </td> 
									<td class="FormHeadBackGround"><input type="radio" id="radTZ" name="radTZ" value="LOCAL" class="NoBorder" onClick="setLocaTime()" tabindex="22"></td>
									<td class="FormHeadBackGround"><font  class="FormHeader">Local</font></td>
									<td class="FormHeadBackGround"><input type="radio" id="radTZ" name="radTZ" value="ZULU" class="NoBorder" checked="checked" onClick="setZuluTime()" tabindex="23"></td>
									<td class="FormHeadBackGround"><font  class="FormHeader">Zulu</font></td>
									<td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td>
								</tr>
								<tr>
									<td colspan="3" class="FormBackGround" valign="middle">
										<!--  Left Pane Start -->
										<table width="95%" border="0" cellpadding="0" cellspacing="1" align="center" ID="Table1">
											<tr>
												<td><font>Schedule ID</font></td>
												<td colspan="2"><font><span id="spnSchedId"></span></font></td>					
												<td><font>Flight ID</font></td>
												<td><font><span id="spnFlightId"></span></font></td>					
											</tr>	
											<tr><td colspan="8"><font class="fntSmall">&nbsp;</font></td></tr>
											<tr>
												<td width="30%"><font>Departure Date</font></td>
												<td width="19%"><input type="text" id="txtDepatureDate" name="txtDepatureDate" style="width:75px;"  invalidText="true"  onBlur="settingValidation('txtDepatureDate')" maxlength="10" tabIndex="24"></td>
												<td width="11%"><a href="javascript:void(0)" onclick="LoadCalendar(3,event); return false;" title="View Calendar"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font></td>
												<td width="20%"><font>Flight No</font></td>
												<td width="20%">
													<input type="text" id="txtFlightNoStart" name="txtFlightNoStart" maxlength="2" style="width:20px"  onchange="dataChanged()" value="" tabIndex="26">							
													<input type="text" id="txtFlightNo" name="txtFlightNo" style="width:50px;" maxlength="<c:out value="${requestScope.reqFltNumberLength}" escapeXml="false" />" tabIndex="27" onchange="dataChanged()"><font class="mandatory"> &nbsp;* </font>
												</td>
												
											</tr>
											<tr>
												<td><font>Operation Type</font></td>
												<td colspan="2">
													<select id="selOperationType" name="selOperationType" size="1" style="width:105;" tabIndex="28" onchange="dataChanged();onChangeLoadAircraftModels()">
														<option></option>
														<c:out value="${requestScope.sesOperationTypeListData}" escapeXml="false" />
													</select><font class="mandatory"> &nbsp;* </font>
												</td>					
												<td><font>Status</font></td>
												<td>
													<select id="selStatus" name="selStatus" size="1" style="width:105;" onChange="setStatusChange()" tabIndex="29">
														<option></option>								
														<c:out value="${requestScope.sesFlightStatusData}" escapeXml="false" />
													</select>
												</td>
												
											</tr>
											<tr>
												<td valign="top"><font>Aircraft Model</font></td>
												<td valign="top" colspan="4">
													<select name="selModelNo" id="selModelNo" size="1" style="width:300;" onchange="loadAircraftModelCapacity()" tabIndex="30">
														<option></option>
														<c:out value="${requestScope.sesAircraftModelListData}" escapeXml="false" />
													</select><font class="mandatory"> &nbsp;* </font>
												
												</td>
											
											</tr>							
											
											<tr>
												<td valign="top"><font>Default Seat Template</font></td>
												<td colspan="2" valign="top">
													<select id="selSeatChargeTemplate" name="selSeatChargeTemplate" size="1" >
														<option></option>
													<!-- 	<c:out value="${requestScope.reqSeatTempl}" escapeXml="false" /> -->
													</select>
												</td>
											</tr>	
											<tr>
												<td valign="top"><font>Default Meal Template</font></td>
												<td colspan="2" valign="top">
													<select id="selMealTemplate" name="selMealTemplate" size="1">
														<option></option>								
														<c:out value="${requestScope.reqMealTempl}" escapeXml="false" />
													</select>
												</td>
											</tr>		
										
					
											<tr>
												<td colspan="3"><font><span id="spnCapacity"></span></font></td>
											</tr>
											<tr>
												<td colspan="5"></td>
											</tr>
											<tr id="selectFltType">
												<td valign="top"><font>Flight Type</font></td>
												<td colspan="4" valign="top" align="left">
													<select name="selFlightType" id="selFlightType" size="1" style="width:105;" tabIndex="31">
														<option value=""></option>	
														<c:out value="${requestScope.reqFlightTypes}" escapeXml="false" />
													</select><font class="mandatory"> &nbsp;* </font>
												</td>
											 		
											</tr>
											<tr>
												<td colspan="5"></td>
											</tr>
											<tr>
												<td colspan="5">
													<table width="100%" border="0" cellpadding="1" cellspacing="0">
														<tr>
															<td width="22%">
																<font>Flights Changed</font>
															</td>
															<td width="3%" id="tdFltChanged" class="fltStatus01"></td>
															<td width="25%" align="right">
																<font>Overlap Flights</font>
															</td>
															<td width="3%" id="tdFltOverLapped" class="fltStatus01"></td>
															<td width="30%" align="right">
																<font>Reservation Exists</font>
															</td>
															<td width="3%" id="tdReservExcist" class="fltStatus01">
															</td>
															<td><font>&nbsp;</font></td>
														</tr>								
													</table>
												</td>
											</tr>
											<tr>
												<td colspan="5"></td>
											</tr>
											<tr>
												<td valign="top"><font>Remarks </font></td>
												<td colspan="4" valign="top" align="left">
													<textarea style="resize:none" name="txtRemarks" id="txtRemarks" cols="47" rows="4" maxlength="260" onkeyUp="validateTA(this,255);commaValidate(this);spclCharsVali(this);" onkeyPress="validateTA(this,255);commaValidate(this);spclCharsVali(this);"></textarea>
												</td>
											</tr>
											<tr>
												<td valign="top"><font>User Notes </font></td>
												<td colspan="4" valign="top" align="left">
													<textarea style="resize:none" name="txtUserNotes" id="txtUserNotes" cols="47" rows="4" maxlength="260"></textarea>
												</td>
											</tr>
											<tr id="CodeShareOC">
												<td width="40%"><font>CS OC Carrier</font></td>
												<td width="20%"><input type="text"
													id="txtCsOCCarrierCode" name="txtCsOCCarrierCode"
													maxlength="2" style="width: 20px"
													onchange="dataChanged()" value="" tabIndex="32"
													readonly="readonly" disabled="disabled"></td>
												<td width="30%"><font>CS OC Flight No</font></td>
												<td width="10%"><input type="text"
													id="txtCsOCFlightNo" name="txtCsOCFlightNo"
													style="width: 50px;"
													maxlength="<c:out value="${requestScope.reqFltNumberLength}" escapeXml="false" />"
													tabIndex="33" onchange="dataChanged()"
													readonly="readonly" disabled="disabled"></td>
											</tr>
										</table>
										<!--  Left Pane End -->
									</td>
									<td></td>
									<td colspan="12" valign="top" class="FormBackGround">
										<!--  right Pane Start -->
										<table width="100%" border="0" cellpadding="0" cellspacing="1" align="center" ID="Table2">					
											
											<tr>
												<td></td>
												<td><font>Leg</font></td>
												<td></td>
												<td><font>D.Time</font></td>
												<td><font>D.Day</font></td>
												<td><font>A.Time</font></td>
												<td><font>A.Day</font></td>						
												<td><font>Duration</font></td>
												<td><font>Err</font></td>
											</tr>
											<tr>
												<td><font>1.</font></td>
												<td>
													<select name="selFromStn1" id="selFromStn1" size="1" style="width:60px" onchange="checkSameLeg('selFromStn1','selToStn1')" tabIndex="32">								
													</select>
												</td>
												<td>
													<select name="selToStn1" id="selToStn1" size="1" style="width:60px" onchange="checkSameLeg('selFromStn1','selToStn1')" tabIndex="33">
													</select>
												</td>
												<td><input type="text" name="txtDepTime1" id="txtDepTime1" maxlength="5" style="width:50px" onchange="dataChanged()" onblur="setTimeWithColon(document.forms[0].txtDepTime1)" tabIndex="34"></td>
												<td>
												<select name="selDepDay1" id="selDepDay1" size="1" style="width:40px">
												</select>
												</td>
												<td><input type="text" name="txtArrTime1" id="txtArrTime1" maxlength="5" style="width:50px" onchange="dataChanged()" onblur="setTimeWithColon(document.forms[0].txtArrTime1)" tabIndex="35"></td>
												<td>
												<select name="selArrDay1" id="selArrDay1" size="1" style="width:40px" tabIndex="36">
												</select><font class="mandatory"> &nbsp;* </font>
												</td>
												<td><font><span id="spnDuration1"></span></font></td>
												<td><font><span id="spnError1"></span></font></td>
											</tr>
											<tr>
												<td><font>2.</font></td>
												<td>
													<select name="selFromStn2" id="selFromStn2" size="1" style="width:60px" onchange="checkSameLeg('selFromStn2','selToStn2')" tabIndex="37">
													</select>
												</td>
												<td>
													<select name="selToStn2" id="selToStn2" size="1" style="width:60px" onchange="checkSameLeg('selFromStn2','selToStn2')" tabIndex="38">							
													</select>
												</td>
												<td><input type="text" name="txtDepTime2" id="txtDepTime2" maxlength="5" style="width:50px" onchange="dataChanged()" onblur="setTimeWithColon(document.forms[0].txtDepTime2)" tabIndex="39"></td>
												<td>
												<select name="selDepDay2" id="selDepDay2" size="1" style="width:40px" tabIndex="40">
												</select>
												</td>
												<td><input type="text" name="txtArrTime2" id="txtArrTime2" maxlength="5" style="width:50px" onchange="dataChanged()" onblur="setTimeWithColon(document.forms[0].txtArrTime2)" tabIndex="41"></td>
												<td>
												<select name="selArrDay2" id="selArrDay2" size="1" style="width:40px" tabIndex="42">
												</select>
												</td>
												<td><font><span id="spnDuration2"></span></font></td>
												<td><font><span id="spnError2"></span></font></td>
											</tr>
											<tr>
												<td><font>3.</font></td>
												<td>
													<select name="selFromStn3" id="selFromStn3" size="1" style="width:60px" onchange="checkSameLeg('selFromStn3','selToStn3')" tabIndex="43">								
													</select>
												</td>
												<td>
													<select name="selToStn3" id="selToStn3" size="1" style="width:60px" onchange="checkSameLeg('selFromStn3','selToStn3')" tabIndex="44">							
													</select>
												</td>
												<td><input type="text" name="txtDepTime3" id="txtDepTime3" maxlength="5" style="width:50px" onchange="dataChanged()" onblur="setTimeWithColon(document.forms[0].txtDepTime3)" tabIndex="45"></td>
												<td>
												<select name="selDepDay3" id="selDepDay3" size="1" style="width:40px" tabIndex="44">
												</select>
												</td>
												<td><input type="text" name="txtArrTime3" id="txtArrTime3" maxlength="5" style="width:50px" onchange="dataChanged()" onblur="setTimeWithColon(document.forms[0].txtArrTime3)" tabIndex="46"></td>
												<td>
												<select name="selArrDay3" id="selArrDay3" size="1" style="width:40px" tabIndex="47">
												</select></td>
												<td><font><span id="spnDuration3"></span></font></td>
												<td><font><span id="spnError3"></span></font></td>
											</tr>
											<tr>
												<td><font>4.</font></td>
												<td>
													<select name="selFromStn4" id="selFromStn4" size="1" style="width:60px" onchange="checkSameLeg('selFromStn4','selToStn4')" tabIndex="48">
													</select>
												</td>
												<td>
													<select name="selToStn4" id="selToStn4" size="1" style="width:60px" onchange="checkSameLeg('selFromStn4','selToStn4')" tabIndex="49">
													</select>
												</td>
												<td><input type="text" name="txtDepTime4" id="txtDepTime4" maxlength="5" style="width:50px" onchange="dataChanged()" onblur="setTimeWithColon(document.forms[0].txtDepTime4)" tabIndex="50"></td>
												<td>
												<select name="selDepDay4" id="selDepDay4" size="1" style="width:40px" tabIndex="51">
												</select>
												</td>
												<td><input type="text" name="txtArrTime4" id="txtArrTime4" maxlength="5" style="width:50px" onchange="dataChanged()" onblur="setTimeWithColon(document.forms[0].txtArrTime4)" tabIndex="52"></td>
												<td>
												<select name="selArrDay4" id="selArrDay4" size="1" style="width:40px" tabIndex="53">
												</select>
												</td>
												<td><font><span id="spnDuration4"></span></font></td>
												<td><font><span id="spnError4"></span></font></td>
											</tr>
											<tr>
												<td><font>5.</font></td>
												<td>
													<select name="selFromStn5" id="selFromStn5" size="1" style="width:60px" onchange="checkSameLeg('selFromStn5','selToStn5')" tabIndex="54">								
													</select>
												</td>
												<td>
													<select name="selToStn5" id="selToStn5" size="1" style="width:60px" onchange="checkSameLeg('selFromStn5','selToStn5')" tabIndex="55">						
													</select>
												</td>
												<td><input type="text" name="txtDepTime5" id="txtDepTime5" maxlength="5" style="width:50px" onchange="dataChanged()" onblur="setTimeWithColon(document.forms[0].txtDepTime5)" tabIndex="56"></td>
												<td>
												<select name="selDepDay5" id="selDepDay5" size="1" style="width:40px" tabIndex="57">
												</select>
												</td>
												<td><input type="text" name="txtArrTime5" id="txtArrTime5" maxlength="5" style="width:50px" onchange="dataChanged()" onblur="setTimeWithColon(document.forms[0].txtArrTime5)" tabIndex="58"></td>
												<td>
												<select name="selArrDay5" id="selArrDay5" size="1" style="width:40px" tabIndex="59">
												</select></td>
												<td><font><span id="spnDuration5"></span></font></td>
												<td><font><span id="spnError5"></span></font></td>
											</tr>					
											<tr>
												<td colspan="18">
													<input type="button" name="btnValidseg" id="btnValidseg" value="Edit Segment" class="Button" style="width:110px;" onclick="validSegment()" tabIndex="60">
													<input type="button" name="btnValidTerminal" id="btnValidTerminal" value="Edit Terminals" class="Button" style="width:110px;" onclick="validTerminal()" tabIndex="61">
												</td>
												
											</tr>
										</table>
								<!-- CordShare Start -->
								
								<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" id="Table3">
						  				<tbody>
								 <tr>
							  			  <td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td>
							  	  <td width="442" height="20" class="FormHeadBackGround"><img src="../../images//bullet_small_no_cache.gif">
																	<font class="FormHeader"> CodeShare MC Flight</font></td>
							    	<td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td> 
							  </tr>
								  <tr>
							 	   <td></td><td width="100%">
									<table cellspacing="0" cellpadding="2" border="0" align="center" width="100%">
												<tbody><tr>
													<td width="22%">
														<font>Carrier Code</font><br>
														<select name="carrier" id="carrier" size="1" style="width:105;" tabindex="19">	
												<option></option>							
												<c:out value="${requestScope.sesCodeShareListData}" escapeXml="false" />
													</select>
													</td>
													<td width="22%">
													<td width="22%">
														<font>Flight No</font><br>
														<input type="text" class="rightAlign portSel" id="flightnum" style="text-transform: uppercase;">
												
													</td><td width="22%">
														<input type="button" class="iconBtn addItem" value=" + " id="add_CodeShare"  onclick="addCodeShareFlight()">
													</td>
												</tr>
												<tr>
													<tr>
														<td colspan="4">
															<select size="10" id="sel_CodeShare" style="height:75px;width:98%" multiple="multiple"></select>
														</td>
														<td valign="top"><input type="button" id="del_CodeShare" value=" - " class="iconBtn removeItem" onclick="removeFromList()"></td>
													</tr>
											</tbody></table></td>
    
									    <td></td>
									   </tr>
									  </tbody></table>
  
								<!-- CordShare End -->
										<!--  right Pane End -->
									</td>
								</tr>
								<tr>
									<td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td>
									<td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
									<td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td>
									<td width="7" height="12" ></td>							
									<td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td>
									<td colspan="2" height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
									<td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td>
								</tr>
								<tr>
									<td style="height:2px;" colspan="18">
													<span id="spnFlights" width="150px" ></span>
									</td>
								</tr>
								<tr>
									<td colspan="18">
										<%@ include file="../common/IncludeFormTopFrameLess.jsp"%>
											<table width="100%" border="0" cellpadding="0">
												<tr>
													<td>
														<input type="button" id="btnClose"  name="btnClose" value="Close" class="Button" onClick="closeClick()" tabIndex="60">
														<input type="button" id="btnReset" name="btnReset" value="Reset" class="Button" onClick="resetClick()" tabIndex="61">								
													</td>
													<td align="right">
														<input type="button" id="btnPubMessages" name="btnPubMessages" value="Published Messages" class="Button" onclick="viewEnterDatesClick('PUB_MSG')" tabindex="67" style="width:150px">
														<input type="button" id="btnMsgHistory" name="btnMsgHistory" value="Message History" class="Button" onclick="viewEnterDatesClick('IN_MSG')" tabindex="67" style="width:110px">
														<input type="button" id="btnNoteRollForwardSearch"  name="btnNoteRollForwardSearch" value="Roll Forward User Note" class="Button" onclick="UI_usrNoteRolFwd.showNoteRollForward()" tabIndex="64" style="width:170px">														
														<input type="button" id="btnViewHistory" name="btnViewHistory" value="Show History" class="Button" onclick="viewEnterDatesClick()" tabindex="67" style="width:93px">
														<input type="button" id="btnSave"  name="btnSave" value="Save" class="Button" onclick="saveClick()" tabIndex="63">
													</td>
												</tr>
											</table>
										 <%@ include file="../common/IncludeFormBottom.jsp"%>
									</td>
								</tr>
							</table>		
						</td>
					</tr>
					</table>
					<!-- end first row-->
								      <!--  end normal data -->
								</div>
								<u:hasPrivilege privilegeId="plan.flight.flt.mgt">
							    <div id="tabs-2" align="center">
							    <table cellpadding="0" width="100%">
									 <tr><td><iframe name="mngFltPopup" id="mngFltPopup" width="100%" height="210px" align="top" style="border:0px"></iframe>
								  	</td></tr></table>
							    </div>
							    </u:hasPrivilege>
							</div>
						</td>
					</tr>
					
				</table>
				
			<%--@ include file="../common/IncludeBottom.jsp"--%><!-- Page Background Bottom page -->
			<input type="hidden" name="hdnVersion" id="hdnVersion"  value=""/>	
			<input type="hidden" name="hdnMode" id="hdnMode"  value=""/>
			<input type="hidden" name="hdnMainMode" id="hdnMainMode"  value=""/>
			<input type="hidden" name="hdnCurrentDate" id="hdnCurrentDate" value="<%=StartDate %>">
			<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">	
			<input type="hidden" name="hdnTimeMode" id="hdnTimeMode" value=""/>
			<input type="hidden" name="hdnScheduleId" id="hdnScheduleId" value=""/>
			<input type="hidden" name="hdnSelectedSchedules" id="hdnSelectedSchedules" value=""/>
			<input type="hidden" name="hdnFlightId" id="hdnFlightId" value=""/>
			<input type="hidden" name="hdnOverlapFlightId" id="hdnOverlapFlightId" value=""/>
			<input type="hidden" name="hdnFromPage" id="hdnFromPage" value=""/>		
			<input type="hidden" name="hdnOverLapSeg" id="hdnOverLapSeg" value=""/>
			<input type="hidden" name="hdnOverLapSegId" id="hdnOverLapSegId" value=""/>
			<input type="hidden" name="hdnSegArray" id="hdnSegArray" value=""/>
			<input type="hidden" name="hdnTerminalArray" id="hdnTerminalArray" value=""/>
			<input type="hidden" name="hdnAllowConf" id="hdnAllowConf" value=""/>
			<input type="hidden" name="hdnAllowDuration" id="hdnAllowDuration" value=""/>
			<input type="hidden" name="hdnAllowModel" id="hdnAllowModel" value="true"/>
			<input type="hidden" name="hdnAllowSegment" id="hdnAllowSegment" value="true"/>
			<input type="hidden" name="hdnAllowOverlap" id="hdnAllowOverlap" value=""/>
			<input type="hidden" name="hdnCopyToDay" id="hdnCopyToDay" value=""/>
			<input type="hidden" name="hdnPrevStatus" id="hdnPrevStatus" value=""/>
			<input type="hidden" name="hdnGridRow" id="hdnGridRow" value=""/>
			<input type="hidden" name="hdnFlightSumm" id="hdnFlightSumm" />
			<input type="hidden" name="hdnCancelAlert" id="hdnCancelAlert" value=""/>	
			<input type="hidden" name="hdnReSchedAlert" id="hdnReSchedAlert" value=""/>	
			<input type="hidden" name="hdnCancelEmail" id="hdnCancelEmail" value=""/>	
			<input type="hidden" name="hdnReSchedEmail" id="hdnReSchedEmail" value=""/>	
			<input type="hidden" name="hdnEmailTo" id="hdnEmailTo" value=""/>
			<input type="hidden" name="hdnEmailSubject" id="hdnEmailSubject" value=""/>
			<input type="hidden" name="hdnEmailContent" id="hdnEmailContent" value=""/>	
			<input type="hidden" name="hdnReprotect" id="hdnReprotect" value="false"/>	
			<input type="hidden" name="hdnViewModeOnly"  id="hdnViewModeOnly">		
			<input type="hidden" name="hdnGDSPublishing" id="hdnGDSPublishing" />
			<input type="hidden" name="hdnFlightDate" id="hdnFlightDate" value=""/>
			
			<input type="hidden" name="hdnSMSAlert" id="hdnSMSAlert" value=""/>	
			<input type="hidden" name="hdnEmailAlert" id="hdnEmailAlert" value=""/>	
			<input type="hidden" name="hdnSmsCnf" id="hdnSmsCnf" value=""/>	
			<input type="hidden" name="hdnSmsOnH" id="hdnSmsOnH" value=""/>			
			<input type="hidden" name="hdnManageFltIds" id="hdnManageFltIds" value=""/>			
		    <input type="hidden" name="hdnManageOption" id="hdnManageOption" value=""/>			
		    <input type="hidden" name="hdnReInstate" id="hdnReInstate" value=""/>
		   
		    <input type="hidden" name="hdnradActOpnSel" id="hdnradActOpnSel" value=""/>	
		    <input type="hidden" name="hdnradActClsSel" id="hdnradActClsSel" value=""/>		
		    <input type="hidden" name="hdnradActOpenAll" id="hdnradActOpenAll" value=""/>		
		    <input type="hidden" name="hdnradActClsAll" id="hdnradActClsAll" value=""/>
		    <input type="hidden" name="hdnradActSysClsSel" id="hdnradActSysClsSel" value=""/>			
		    <input type="hidden" name="hdnstartDate" id="hdnstartDate" value=""/>				
		    <input type="hidden" name="hdnHasFrq" id="hdnHasFrq" value=""/>
		    <input type="hidden" name="hdnFromFromPage" id="hdnFromFromPage" value=""/>
		    <input type="hidden" name="hdnFrequency" id="hdnFrequency" value=""/>
<!--		    <input type="hidden" name="hdnStartTime" id="hdnStartTime" value=""/>-->
<!--		    <input type="hidden" name="hdnEndTime" id="hdnEndTime" value=""/>-->
		    <input type="hidden" name="hdnIncludeScheduled" id="hdnIncludeScheduled" value=""/>
		    <input type="hidden" name="hdnFlightNote" id="hdnFlightNote" value=""/>
		    
		    <input type="hidden" name="hdnStartDateSearch" id="hdnStartDateSearch" value=""/>
			<input type="hidden" name="hdnStopDateSearch" id="hdnStopDateSearch" value=""/>
			<input type="hidden" name="hdnFlightNoStartSearch" id="hdnFlightNoStartSearch" value=""/>
			<input type="hidden" name="hdnFlightNoSearch" id="hdnFlightNoSearch" value=""/>
			<input type="hidden" name="hdnSelFromStn6" id="hdnSelFromStn6" value=""/>
			<input type="hidden" name="hdnSelToStn6" id="hdnSelToStn6" value=""/>
			<input type="hidden" name="hdnSelOperationTypeSearch" id="hdnSelOperationTypeSearch" value=""/>
			<input type="hidden" name="hdnSelStatusSearch" id="hdnSelStatusSearch" value="<c:out value='${param.hdnSelStatusSearch}' escapeXml='false' />"/>
			<input type="hidden" name="hdnSelFlightTypeSearch" id="hdnSelFlightTypeSearch" value="<c:out value='${param.hdnSelFlightTypeSearch}' escapeXml='false' />"/>
			<input type="hidden" name="hdnFlightNo" id="hdnFlightNo" value=""/>
			<input type="hidden" name="hdnTxtUserNotes" id="hdnTxtUserNotes" value=""/>
			<input type="hidden" name="hdnModelNo" id="hdnModelNo" value="" />
			<input type="text" id="fromDate" name="fromDate" style="display: none;">
			<input type="text" id="toDate" name="toDate" style="display: none;">
		    <input type="hidden" name="hdnLegArray" id="hdnLegArray" value=""/>
		    <input type="hidden" name="displayWarning" id="displayWarning" value=""/>
		    <input type="hidden" name="hndCodeShare" id="hndCodeShare" value=""/>	
		    <input type="hidden" name="hndCsOCCarrierCode" id="hndCsOCCarrierCode" value=""/>
			<input type="hidden" name="hndCsOCFlightNo" id="hndCsOCFlightNo" value=""/>
			<input type="hidden" name="hdnShowResHistory" id="hdnShowResHistory" value=""/>			
			<input type="hidden" name="hdnHaveMCFlight" id="hdnHaveMCFlight" />
			<input type="hidden" name="hdnAuditType" id="hdnAuditType" value="HISTORY"/>
			<input type="hidden" name="hdnOpenEtStatus" id="hdnOpenEtStatus" value=""/>
					
		    <c:if test="${requestScope.isManageFlt != 'true'}">
				<input type="hidden" id="btnManageFlight"  name="btnManageFlight"> 
			</c:if>		
			</form>
			<div id="popup"  style="display:none;" title="Select the Date Range">
				<div>
					<table id="hitoryDuration">
						<tr>
							<td><label>From Date:</label></td>
							<td title="From Date Here"><input type="text" id="fromDatePicker" name="fromDatePicker" onblur="settingValidation('fromDatePicker');"></td>
							<td>
								<font class="mandatory"> &nbsp;* </font>
							</td>
						</tr>
						<tr>
							<td><label>To Date:</label></td>
							<td title="To Date Here"><input type="text" id="toDatePicker" name="toDatePicker" onblur="settingValidation('toDatePicker');"></td>
							<td>
								<font class="mandatory"> &nbsp;* </font>
							</td>
						</tr>
						<tr>
							<td><input type="button" id="btnCancel" value="Cancel" onclick="$('#popup').dialog('close');" class="Button"></td>
							<td><input type="button" id="btnContinue" value="Continue" onclick="viewHistoryClick()" class="Button"></td>
						</tr>
					</table>
				</div>
			</div>	

		<script src="../../js/flight/FlightInstance.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/flight/FlightInstanceGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script>
			winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>' ,'<c:out value="${requestScope.warningmessage}"/>');
		</script>
		<script type="text/javascript">
		<!--
			var objProgressCheck = setInterval("ClearProgressbar()", 300);
			function ClearProgressbar() {
		    	if (objDG.loaded) {
					clearTimeout(objProgressCheck);
					top[2].HideProgress();
				}
			}
 
			 
	   	//-->
		</script>
				<%@ include file="../v2/tools/inc_UserNoteRollForward.jsp"%>
				<div id="page-mask" style="display:none; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: rgb(220, 220, 220); position: absolute; z-index: 1000; top: 0px; left: 0px; width: 100%; height: 690px; opacity: 0.1; background-position: initial initial; background-repeat: initial initial; "></div>
				
  </body>
</html>