<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title></title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
    <meta http-equiv='lcc-xbe-login' content='adfsdfj123dfa893435#@!bnyufUYR345245!RirasdfnM#djf' />
	<link rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>
	<link rel="stylesheet" type="text/css" href="../../themes/isa.commonError_no_cache.css">
	<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/login/Login.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script>
		var reOpen=false;
		var request=new Array();
		
		<c:out value='${requestScope.reqRequestData}' escapeXml='false' />		
			fullScreen2();
			var erroImgPath = "../../images/error_icon_no_cache.gif";
	</script>
  </head>
  <body style="visibility:hidden" onkeypress='return Body_onKeyPress(event)' scroll="no" oncontextmenu="return true" onkeydown='return Body_onKeyDown(event)' ondrag='return false' >
	<table width='999' cellpadding='0' cellspacing='0' border='0' align="center" class="TBLBackGround">
		<td width="13" class="BannerBorderLeft"><img src="../../images/spacer_no_cache.gif" width="13" height="1"></td>
		<td width='974px;' style='background-image:url(../../images/spacer_no_cache.gif);background-repeat:repeat-y;background-color:white;'>
			<table width='100%' border='0' cellpadding='0' cellspacing='0'>
				<tr>
					<td style="height:670px;background-image:url(../../images/M037_no_cache.jpg);background-repeat:no-repeat;' class="PageBackGround" align='center'>
						<table width='100%' border='0' cellpadding='0' cellspacing='0'>
							<tr>
								<td style="height:670px;background-image:url(../../images/spacer_no_cache.gif);background-repeat:no-repeat;background-position:bottom right;" align='center'>
									<table width='50%' border='0' cellpadding='0' cellspacing='0'>
										<tr>
											<td colspan='3' style='height:110px;'></td>
										</tr>
										<tr>
											<td><img src='../../images/M038_no_cache.jpg'></td>
											<td style="background-image:url(../../images/M039_no_cache.jpg);background-repeat:repeat-x;">
												<form id="formLogin" name="formLogin" method="post" action="../public/j_security_check">
													<table width="90%" border="0" cellpadding="1" cellspacing="1" align='center'>
														<tr>
															<td colspan="2" style='height:20px;'>
															</td>
														</tr>
														<tr>
															<td colspan="2">
																<font class="fntBold">Welcome !</font>
															</td>
														</tr>
														<!--  <tr>
															<td colspan="2" style='height:10px;'>
															</td>
														</tr> -->
														<tr>
															<td colspan="2"><font><span id="spnWelcome"></span></font>		
																
															</td>
														</tr>
														<tr hidden><td colspan="2"><font><input type="hidden" id="j_token" name="j_token" value="0">&nbsp;</font></td></tr>
														<tr>
															<td width="53%" align="right">
																<font>User ID :</font>
															</td>
															<td align="right">
																<input type="text" id="j_username" name="j_username" size="20" maxlength="20" onkeydown="Login_onKeyDown(event)" onChange="HidePageMessage()">
															</td>
														</tr>
														<tr>
															<td align="right">
																<font>Password :</font>
															</td>
															<td align="right">
																<input type="password" id="j_password" name="j_password" size="20" maxlength="12" onkeydown="Login_onKeyDown(event)" onChange="HidePageMessage()">
															</td>
														</tr>
														<tr class="captcha" style="display: none;">
															<td align="right">
																<img src="../../private/jcaptcha_new" align="top" style="cursor: pointer;" name="imgCPT" id="imgCPT" width="130px" height="50px" onclick="refreshCaptcha()"/>
															</td>
															<td align="right">														
																<input type="text" size="20" name="captcha_txt" id="captcha_txt" onkeydown="Login_onKeyDown(event)" onChange="HidePageMessage()"/>
															</td>
														</tr>
														<!--  <tr>
															<td align="right" colspan="2" style='height:10px;'>
															</td>
														</tr> -->
														<tr>
															<td align="right" colspan="2">
																<input type="button" id="btnClose" value="Close" class="Button" onclick="closeLogin()">
																<input type="button" id="btnLogin" value="Login" class="Button" onclick="login()">
															</td>
														</tr>
													</table>
													<input type="hidden" id="hdnUserStatus" name="hdnUserStatus" value="<c:out value="${sessionScope.isInactiveUserLogin}" escapeXml="false" />" />
												</form>
											</td>
											<td><img src='../../images/M040_no_cache.jpg'></td>
										</tr>
										<tr>
											<td colspan='3'>
												<font style="color:#E4E4E4">&nbsp;Powered by <b>ISA</b></font>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><img src="../../images/bottom6_no_cache.jpg"></td>
				</tr>
				<tr>
					<td>	
						<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBottomColor" style="height:100%;" ID="Table5">
							<tr>
								<td width="30" class="PageBottomShadow"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
								<td width="914" align="left" height="25" valign="middle">
									<table width="100%" border="0" cellpadding="0" cellspacing="1" ID="Table6">
										<tr>
											<td width="20" valign="bottom"><a href="javascript:void(0);" onclick="ShowPageMessage()" title="View Message(s)"><img id="imgError" name="imgError" src="../../images/spacer_no_cache.gif" border="0"></a></td>
											<td width="1"><img src="../../images/StatusBar_no_cache.jpg"></td>
											<td class="StatsBarDef" id="tdSTBar"><span id="spnDate">&nbsp;</span></td>
											<td width="1"><img src="../../images/StatusBar_no_cache.jpg"></td>
											<td width="265" align="right">
												<!--font class="fontPGInfo">This site best view in IE 5+ and 1024 x 768 resolution.</font-->
											</td>
										</tr>
									</table>
								</td>
								<td width="30" class="PageBottomShadow"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td width="12" class="BannerBorderRight"><img src="../../images/spacer_no_cache.gif" width="12" height="1"></td>	
	</table>
	<div id="errorMSGouter" style="display: none;">
			<div class="errorBox">
				<span id="errorMSG" style=""></span>
				<span class="errorClose">&nbsp;</span>
			</div>
		</div>
		<div id="errorPopup" style="display: none;">
			<div class="errorPopupHeader">
				<span class="text">Error Console</span>
				<span class="errorClose">&nbsp;</span>
				<div style="clear:both"></div>
			</div>
			<div class="errorPopupButton" style="">
				<a href="javascript:void(0)" class="errorClear">Clear All</a>
			</div>
				<div class="errorPopupbody" style="">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<div>
									<table id="errorPopupTable" border="0" width="100%" cellspacing="0" cellpadding="4">
									
									</table>
								</div>
							</td>
						</tr>
					</table>
				</div>
		</div>
	<script src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script language="javascript">
		<c:out value='${requestScope.reqClientMessages}' escapeXml='false' />
		var carrier = "<c:out value='${requestScope.reqCarrName}' escapeXml='false' />";
		var defaultAirlineId = "<c:out value='${requestScope.defaultAirlineId}' escapeXml='false' />";
		document.title = ": : "+carrier;
	<!--
	var arrError		= new Array();
	var logStatus='<%=request.getParameter("hdnMode")%>';	
	var serverErrorMsg = "<c:out value='${requestScope.reqServerErrors}' escapeXml='false' />"; 
	if(!reOpen){
		document.body.style.visibility="visible";
		onLoad(logStatus);
	}
	function reloadLoginAction(){
		return true;
	}
	
	function refreshCaptcha() {
	    $("#txtCaptcha").val("");
	    document['imgCPT'].src = '../../private/jcaptcha_new?relversion=' + (new Date()).getTime();
	}
	
	function showCaptcha(){
		
		var data = {};
		$.ajax({
			type : "POST",
			url : "../../public/master/showCaptcha.action",
			dataType : "json",
			data : data,
			async : false,
			success: function (response) {
				
				if (response.captchaEnabled){
					$("tr.captcha").show();
				}
 
			}
		});
		
	}
	
	$(document).ready(function() {
		showCaptcha();
	});
	
	//-->
	</script>
</body>
</html>
