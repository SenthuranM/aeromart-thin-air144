<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css">
	
	<script  src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script  src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script  src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </head>
  <body class="tabBGColor" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
   <form name="frmBizParam" id="frmBizParam" method="post" action="showParam.action">

		
		<script type="text/javascript">			
			<c:out value="${requestScope.reqParamData}" escapeXml="false" />
			var totalNoOfRecords =<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />
			var searchData = "<c:out value="${requestScope.reqParamSearchData}" escapeXml="false" />";
			var initialrec = "<c:out value="${requestScope.recordNo}" escapeXml="false" />";
			var svdGird = "<c:out value="${requestScope.reqParamGridData}" escapeXml="false" />";
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			<c:out value="${requestScope.reqFormData}" escapeXml="false" />
			<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
			var request=new Array();
			<c:out value='${requestScope.reqRequest}' escapeXml='false' /> 
			var paramNameArr=new Array();
			<c:out value='${requestScope.reqParamNameList}' escapeXml='false' />
			var paramKeyArr=new Array();
			<c:out value='${requestScope.reqParamKeyList}' escapeXml='false' />
		</script>
		
			<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						Search Parameters						<%@ include file="../common/IncludeFormHD.jsp"%>					  
						<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table2">
							<tr>								
								<td width="12%"><font>Parameter Name </font></td>
								<td width="2%"><td><input name="txtParamName" type="text" id="txtParamName" size="40"  maxlength="40"></td>
								<td width="8%"><font>Param Key </font></td>
								<td width="2%"><td><input name="txtParameterKey" type="text" id="txtParameterKey" size="15"  maxlength="10"></td>
						    	<td width="6%" id="fntCarrierCode"><font>Carrier Code</font></td>
								<td width="8%">
									<select name="selCarrierCode"  id="selCarrierCode" size="1">	
										<option VALUE="ALL">All</option>								 
										<c:out value="${requestScope.reqParamCCList}" escapeXml="false" />									  
								    </select>
								</td>
								<td width="12%" align="right">
								  <input name="btnSearch" type="button" class="button" id="btnSearch" onClick="searchUser()" value="Search"> 
							    </td>
							</tr>
	 				    </table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>				
				<tr>
					<td>
						<br>
						<%@ include file="../common/IncludeFormTop.jsp"%>Parameters<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">							
							<tr>
								<td>
									  <span id="spnParameters"></span> 
								</td>
							</tr>
							<tr>
								<td>
									<u:hasPrivilege privilegeId="sys.param.admin.add">
										<input type="button"  name="btnAdd" id="btnAdd" class="Button" value="Add" onClick="addParam()">
									</u:hasPrivilege>
									<input type="button"  name="btnEdit" id="btnEdit" class="Button" value="Edit" onClick="editParam()">
									<u:hasPrivilege privilegeId="sys.param.admin.edit">
										<input type="button"  name="btnAdminEdit" id="btnAdminEdit" class="Button" value="AdminEdit" onClick="adminEditParam()">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.param.admin.sync.all.cache">
										<input type="button"  name="btnAdminSyncAll" id="btnAdminSyncAll" class="Button" value="AdminSyncAll" onClick="adminSyncAllParams()">
									</u:hasPrivilege>
								</td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>			
			<tr>
				<td>
					<%@ include file="../common/IncludeFormTop.jsp"%>
					Modify Parameters
					<%@ include file="../common/IncludeFormHD.jsp"%>		
					
					<table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
				       <tr>
				          <td width="60%" valign="top">
				               <table width="100%" border="0" cellpadding="0" cellspacing="2" align="center">  
				                  <tr>
				                    <td width="25%"><font>Carrier Code</font></td>
				                    <td>
										<select name="selCCode"  id="selCCode" size="1">	
											<option VALUE=""></option>								 
											<c:out value="${requestScope.reqParamCCList}" escapeXml="false" />									  
										</select> <font class="mandatory"> * </font> 
				                    </td>
				                   </tr>
				                   <tr>
				                   	<td valign="top"><font>Parameter Name</font></td>				                   
				                    <td><input name="txtParameterName" type="text" id="txtParameterName" size="50"  maxlength="70"> <font class="mandatory"> * </font> </td>
									</tr>									
				                   <tr>
				                    <td><font>Parameter Key</font></td>
									<td><input name="txtParamKey" type="text" id="txtParamKey" size="20" maxlength="20"> <font class="mandatory"> * </font> </td>
				                  </tr>								
				                  <tr>
				                    <td><font>Parameter Value</font></td>
				                     <td><span id="spnParamValue"><input name="txtParameterValue" type="text" id="txtParameterValue" maxlength="100" size="50" onChange="clickChange()"></span><font class="mandatory"> &nbsp;* </font></td>
				                   </tr>								
								  <tr>
									<td><font>Type</font></td>
									<td>
										<select name="selType"  id="selType" size="1">
											<option VALUE=""></option>								 
											<c:out value="${requestScope.reqParamTypeList}" escapeXml="false" />									  
									    </select> <font class="mandatory"> * </font> 
									</td>
				                  </tr>
				                </table>
				              </td>
					          <td colspan="2" valign="top">
								<font><br><b>Note : </b><br>The server must be restarted, for the changes to be effective.<br>Pending Parameter changes are marked with&nbsp;&nbsp;**&nbsp;&nbsp;against the<br>Parameter Name.</font>
					          </td>
				            </tr>
				            <tr><td colspan="2">&nbsp;</td></tr>					
							<tr>
			                    <td colspan="2" valign="bottom">
									<input type="button" id="btnClose" class="Button" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)">				                    
				                    <input name="btnReset" type="button" class="Button" id="btnReset"  onClick="resetClick()" value="Reset">
				                </td>
				                <td align="right" valign="bottom">
									<input name="btnSave" type="button" class="Button" id="btnSave" onClick="saveData()" value="Save">
								</td>
			                  </tr>
			                  <tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			           </table>
					<%@ include file="../common/IncludeFormBottom.jsp"%>  
			    </td>
		    </tr>			
			</table>

		<input type="hidden" name="hdnMode" id="hdnMode"/> 
		<input type="hidden" name="hdnRecNo" id="hdnRecNo"/> 	
		<input type="hidden" name="hdnAction" id="hdnAction"/>
		<input type="hidden" name="hdnEditable" id="hdnEditable"/>
		<input type="hidden" name="hdnSearchData" id="hdnSearchData"/> 
		<input type="hidden" name="hdnGridData" id="hdnGridData"/> 
	</form>	

	<script src="../../js/bizsysparameters/BusinessSystemParametersValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/bizsysparameters/BusinessSystemParametersAdminGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </body>

  

  <script type="text/javascript">
  	<!--
  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
  	function ClearProgressbar(){  		
  		if (typeof(objDG) == "object"){
	  		if (objDG.loaded){
	  			clearTimeout(objProgressCheck);
	  			top[2].HideProgress();
	  		}
	  	}	  	
  	}
  	 
  	//-->
  </script>

</html>