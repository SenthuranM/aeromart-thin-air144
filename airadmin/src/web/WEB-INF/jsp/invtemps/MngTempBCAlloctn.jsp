<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Default Inventory Template Page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
<LINK rel="stylesheet" type="text/css"
	href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css"
	href="../../themes/default/css/ui.all_no_cache.css">

<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script src="../../js/tools/PopUpData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/invtemps/MngTempBCAlloctn.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>


</head>
<body class="tabBGColor" scroll="no"
	onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false"
	onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
	<form method="post" name="frmInvTemp" id="frmInvTemp" target="_self">
		<div id="divSearchAC" class="FormBackGround"
			style="height: 22px; text-align: left;">
			<table width="96%" border="0" cellpadding="0" cellspacing="2"
				ID="Table9">

				<tr>

					<td width="16%"><font>AirCraft Model Name</font></td>
					<td colspan="2"><select name="selAircraftModel"
						id="selAircraftModel" size="1" style="width: 250;" tabindex="19">
							<option></option>
					</select></td>

					<td><font>Status:</font></td>
					<td><input type="checkbox" id="chkStatus" name="chkStatus"
						class="NoBorder" tabindex="27" title="Status" value="ACT" checked onchange ="UI_mngBCAllo.StatusChanged(this)"></td>
				</tr>
			</table>
		</div>
		<div id="divAddRoute" class="FormBackGround"
			style="height: 47px; text-align: left;">
			<table width="100%" border="0" cellpadding="0" cellspacing="2"
				ID="Table2">

				<tr>
					<td><font>Segment Code:</font></td>
					<td><font>Origin :</font></td>
					<td><select name="selOrigin" id="selOrigin" size="1"
						style="width: 100;" tabindex="19">
							
					</select></td>
					<td><font>Destination :</font></td>
					<td><select name="selDestination" id="selDestination" size="1"
						style="width: 100;" tabindex="19">
							
					</select></td>
					<td><font>Via 1:</font></td>
					<td><select name="selVia1" id="selVia1" size="1"
						style="width: 100;" tabindex="19">
							
					</select></td>
					<td><font>Via 2:</font></td>
					<td><select name="selVia2" id="selVia2" size="1"
						style="width: 100;" tabindex="19">
							
					</select></td>
					<td><font>Via 3:</font></td>
					<td><select name="selVia3" id="selVia3" size="1"
						style="width: 100;" tabindex="19">
							
					</select></td>
				</tr>
				<tr>
					<td><input tabindex="2" type="text" name="txtSegment"
						id="txtSegment" size="25" maxlength="20"></td>
					<td></td>
					<td width="8%"><input name="btnSegment" type="button"
						id="btnSegment" value="Add Segment" class="Button"></td>
				</tr>

			</table>
		</div>
		<script type="text/javascript">
			var showRW = "<c:out value="${requestScope.showRW}" escapeXml="false" />";
			if (showRW == "Y") {
				$("#divAddRoute").show();
				$("#divAddRoute").decoratePanel("Add Route");

			} else {
				$("#divAddRoute").hide();
			}
		</script>
		<div id="divCabinClsSel"
			style="height: 165px; text-align: left; background-color: #ECECEC;">
			<table width="100%" border="0" cellpadding="0" cellspacing="4">
			<tr>
				<td id="CCSelDetailsGridContainer"></td>
			</tr>
			<tr>
					<td width="8%"><input name="btnSplit" type="button"
						id="btnSplit" value="Split" class="Button"></td>

			</tr>
			</table>

		</div>
		<div id="divBCAlloc" style="height: 250px; text-align: left; background-color: #ECECEC;">
			<table id="listBCAllocId" class="scroll" cellpadding="0"
				cellspacing="0">
				<tr>
					<td id="BCAllocGridContainer"></td>
				</tr>
			</table>

			<table width="100%" border="0" cellpadding="0">
				<tr>
					<td width="10%"><font>Total BC Alloc:</font></td>
					<td id = "totalBCAllocs"><font></font></td>
					<td width="10%"></td>
					<td width="10%"><font>Fixed Alloc:0</font></td>
					<td width="12%"></td>
					<td width="12%"><font>Check/Uncheck All Prio:</font></td>
					<td ><input type="checkbox" id="chkPrio" name="chkPrio" class="NoBorder" tabindex="27" title="chkPrio" ></td>
					<td width="14%"></td>
					<td width="10%"><font>Check/Uncheck All Closed:</font></td>
					<td><input type="checkbox" id="chkClosed" name="chkClosed" class="NoBorder" tabindex="27" title="chkClosed"></td>
				</tr>
			</table>

		</div>
		<div id="divBCAllocOpts"
			style="height: 50px; text-align: left; background-color: #ECECEC;">
			<table width="100%" border="0" cellpadding="0" cellspacing="4">
				<tr>
					<td width="5%"><font>Booking Class</font></td>
					<td><select name="selBC" id="selBC" size="1"
						style="width: 100;" tabindex="19">
							<option></option>
					</select></td>
					<td width="15%">Allocation</td>
					<td><input tabindex="2" type="text" name="txtAlloc"
						id="txtAlloc" size="10" maxlength="8"></td>
				</tr>
				<tr>
					<td width="10%"><input name="btnAdd" type="button" id="btnAdd"
						value="Add" class="Button"></td>
					<td width="5%"><input name="btnEdit" type="button"
						id="btnEdit" value="Edit" class="Button">
					<td width="5%"><input name="btnDelete" type="button"
						id="btnDelete" value="Delete" class="Button"></td>
				</tr>

			</table>

		</div>

		<div id="divFinish">

			<table width="100%" border="0" cellpadding="0" cellspacing="4">
				<tr>
					<td width="90%"><input name="btnBack" type="button"
						id="btnBack" value="Back" class="Button"></td>
					<td><input name="btnSave" type="button" id="btnSave"
						value="Save" class="Button"></td>
				</tr>
			</table>
		</div>
		<table>
			<tr style="display: none">
				<td><input type="hidden" id="hdnAirCraftModel"
					value='<c:out value="${requestScope.airCraftModel}" escapeXml="false" />'>
				</td>
				<td><input type="hidden" id="hdnStatus"
					value='<c:out value="${requestScope.status}" escapeXml="false" />'>
				</td>
				<td><input type="hidden" id="hdnSegment"
					value='<c:out value="${requestScope.segment}" escapeXml="false" />'>
				</td>	
				<td><input type="hidden" id="hdnTmplateID"
					value='<c:out value="${requestScope.templateID}" escapeXml="false" />'>
				</td>
				<td><input type="hidden" id="hdnShowRW"
				    value='<c:out value="${requestScope.showRW}" escapeXml="false" />'>
				</td>	
			</tr>
		</table>
	</form>
	<div id="popUp" class="myPopup"></div>
	<script>
		var screenId = 'SC_INVN_006';
		top[2].HideProgress();
	</script>

</body>
</html>
