<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Default Inventory Template Page</title>
<meta http-equiv="pragma" content="no-cache"/>
	    <meta http-equiv="cache-control" content="no-cache"/>
	    <meta http-equiv="expires" content="-1"/>
		<link rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>		
		<link rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>
		
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
        <script src="../../js/invtemps/MngInvTemps.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

</head>
<body class="tabBGColor" scroll="no"
	onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false"
	onkeydown='return Body_onKeyDown(event)' ondrag='return false'>

	<div id="divSearch" class="FormBackGround" style="height: 30px; text-align: left;">
		<table width="96%" border="0" cellpadding="0" cellspacing="2"
			ID="Table9">
			<tr>

				<td width="16%"><font>AirCraft Model Name</font></td>
				<td colspan="2"><select name="selAircraftModel"
					id="selAircraftModel" size="1" style="width: 250;" tabindex="19">
						<option></option>
				</select></td>

				<td width="8%"><input name="btnSearch" type="button"
					id="btnSearch" value="Search" class="Button"></td>
			</tr>
		</table>
	</div>

	<div id="divResultsPanel"
		style="height: 350px; text-align: left; background-color: #ECECEC;">
		<table id="listInvTempId" class="scroll" cellpadding="0"
			cellspacing="0">
			<tr>
				<td id="InvTemplDetailsGridContainer"></td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="4">
			<tr>
			<u:hasPrivilege privilegeId="inventory.template.add">
				<td width="20%"><input type="button" id="btnAddDefaultIvn"
					class="ButtonLong" value="Add Default Inventory Template">
				</td>

				<td width="20%" align="right"><input type="button"
					id="btnAddRWIvn" class="ButtonLong"
					value="Add Route wise Inventory Template"></td>
			</u:hasPrivilege>
			<u:hasPrivilege privilegeId="inventory.template.edit">
				<td width="60%" align="right"><input name="btnEdit"
					type="button" id="btnEdit" value="Edit" class="Button"></td>
			</u:hasPrivilege>
			</tr>
		</table>

	</div>
	<script>
		var screenId = 'SC_INVN_005';
		top[2].HideProgress();
	</script>
	
</body>
</html>
