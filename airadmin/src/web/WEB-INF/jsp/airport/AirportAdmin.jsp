
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<html>
	<head>
	    <title>Airport Admin</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="-1">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
		<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">	
		<LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css">
	
		<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>	
	<script src="../../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/airport/goShowSelect.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>	
	<script src="../../js/airport/PopUpData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
	
  </head>
  <body class="tabBGColor" scroll="Yes" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return true" onkeydown="return Body_onKeyDown(event)" ondrag='return false' 
  	onbeforeunload="beforeUnload()" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">

  		<form method="post" action="showAirport.action" id="frmAirport" name="frmAirport">  		
  			<script type="text/javascript">
  				var searchData = "<c:out value="${requestScope.reqAirportSearchData}" escapeXml="false" />";
  				<c:out value="${requestScope.reqCountryCode}" escapeXml="false" />
				<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
				<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
				<c:out value="${requestScope.reqFormData}" escapeXml="false" />
				<c:out value="${requestScope.reqDefaultMinStopOverTime}" escapeXml="false" />
					
				<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
				<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
				<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />	
				<c:out value="${requestScope.reqAirportFocus}" escapeXml="false" />
				<c:out value="${requestScope.reqStationFocus}" escapeXml="false" />	
				<c:out value="${requestScope.reqGoShowFocus}" escapeXml="false" />	
				<c:out value="${requestScope.reqGoShoreAgentList}" escapeXml="false" />	
				<c:out value="${requestScope.reqIsGroundServiceEnabled}" escapeXml="false"/>
				var engineVisiblity = "<c:out value="${requestScope.engineVisibility}" escapeXml="false" />";
				var agentsOfAirport = null;
				var objCmb = null;
				var arrCountry = new Array();
				<c:out value="${requestScope.reqCountryList}" escapeXml="false" />
				var airportArr = new Array();
				<c:out value="${requestScope.reqAirportCodeNameList}" escapeXml="false" />
				var lccEnableStatus = "<c:out value="${requestScope.reqLCCEnableStatus}" escapeXml="false" />";

			</script>

			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td><%@ include file="../common/IncludeFormTop.jsp"%>Search Airports<%@ include file="../common/IncludeFormHD.jsp"%>							
						<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table2">
							<tr>
								<td width="10%"><font>Country</font></td>
								<td width="18%"><font>&nbsp;</font></td>
								<td width="10%"><font>Airport Code</font></td>
								<td width="15%"><font>&nbsp;</font></td>
								<td width="10%"><font>Airport Name</font></td>
								<td width="11%"><font>&nbsp;</font></td>
								<td align="right"><input name="btnSearch" type="button" class="button" id="btnSearch" onClick="searchAirport()" value="Search"></td>
			  			</tr>
					  </table>
				  	  <%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Airports<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									<span id="spnAirports"></span> 
								</td>
							</tr>
							<tr>
								<td>
									<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td>
												<u:hasPrivilege privilegeId="sys.mas.airport.add">
													<input name="btnAdd" type="button" class="Button" id="btnAdd" onClick="addClick()" value="Add">
												</u:hasPrivilege>	
												<u:hasPrivilege privilegeId="sys.mas.airport.edit">
													<input name="btnEdit" type="button" class="Button" id="btnEdit" value="Edit"  onClick="editClick()">
												</u:hasPrivilege>
												<u:hasPrivilege privilegeId="sys.mas.airport.fordisp">
													<input type="button" value= "Add Name For Display" class="ButtonLong" onClick="return POSenable();" id="btnAirportFD" style="width:150px;">

												</u:hasPrivilege>
												<u:hasPrivilege privilegeId="sys.mas.airport.delete">	
													<input name="btnDelete" type="button" class="Button" id="btnDelete" value="Delete"  onClick="deleteClick()">
												</u:hasPrivilege>
												<u:hasPrivilege privilegeId="sys.mas.airport.cit">
													<input type="button" id="btnCIT" name="btnCIT" class="Button" value="Check in time"  style="width:100px;" onclick="ViewDetailCIT()">
												</u:hasPrivilege>
												<u:hasPrivilege privilegeId="sys.mas.airport.term">
													<input type="button" id="btnTerminals" name="btnTerminals" class="Button" value="Terminals"  style="width:90px;" onclick="ViewDetailTerminal()">
												</u:hasPrivilege>
												<!--input name="btnPrint" type="button" class="Button" id="btnPrint" value="Print"  onClick=""-->
												<input name="btnShowSubStations" type="button" class="ButtonLong" id="btnShowSubStations" value="View Sub Stations" onClick="showSubStations()" style="width:140px;"/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td><%@ include file="../common/IncludeFormTop.jsp"%>Add / Modify Airport<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="0">				  			
				  			<tr>
								<td width="78px;"><font>Type</font></td>
				  				<td>
				  					<input tabindex="1" type="radio" id="radOnline1" name="radOnline1" value="1" class="NoBorder" onClick="rad1Checked()" onChange="pageOnChange()">
				  					<font>Online</font>
				  					<input tabindex="2" type="radio" id="radOnline2" name="radOnline2" value="0" class="NoBorder" onClick="rad2Checked()" onChange="pageOnChange()">
				  					<font>Offline</font>&nbsp;&nbsp;				  					
								</td>
								<td><font>Active</font></td>
								<td>
									<input tabindex="9" type="checkbox" id="chkActive" name="chkActive" onChange="pageOnChange()" onclick="statusChange()">
								</td>
								<td colspan="2">
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><font>Manual CheckIn</font></td>
											<td>
												<input tabindex="9" type="checkbox" id="chkManualCheckIn" name="chkManualCheckIn" onChange="pageOnChange()">
											</td>
											<td><font>Online CheckIn</font></td>
											<td>
												<input tabindex="9" type="checkbox" id="chkOnlineCheckin" name="chkOnlineCheckin" onChange="pageOnChange()">
											</td>
											<td><font>Enable CFG in PNL/ADL</font></td>
											<td>
												<input tabindex="9" type="checkbox" id="enableCFG" name="enableCFG" onChange="pageOnChange()">
											</td>
										</tr>
									</table>
								</td>
									
				  			</tr>
				  			<tr>
				  				<td><font>Airport Code</font></td>
				  				<td>
				  					<input tabindex="3" type="text" title="Airport Code" id="txtAirportCode" name="txtAirportCode" size="3"   class="UCase"  maxlength="3"  onkeyUp="alphaValidate(this)" onkeyPress="alphaValidate(this)" onChange="pageOnChange()"><font class="mandatory">&nbsp;*</font>
				  				</td>

				  				<td><font>Station</font></td>
				  				<td>
					  				<select tabindex="10"  name="selFormStation" size="1" id="selFormStation" onChange="stationChange()" style="width:60px;">
										<option value="-1"></option>
										<c:out value="${requestScope.reqStationList}" escapeXml="false" />
				  				  	</select>
									<font class="mandatory">&nbsp;*</font> 
				  				</td>

				  			</tr>
				  			<tr>
				  				<td>
				  					<font>Airport Name</font>
				  				</td>
				  				<td colspan="3">
					  				<input tabindex="4" title="Airport Name" type="text" id="txtAirportName" name="txtAirportName" onkeyUp="valAirportName(this)" onkeyPress="valAirportName(this)" size="44" maxlength="255" onChange="pageOnChange()"><font class="mandatory">&nbsp;*</font>
				  				</td>				  				
				  				<td><font>Latitude</font></td>
								<td colspan="2" rowspan="7" valign="top">
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td>
												<fieldset>
													<table width="100%" border="0" cellpadding="0" cellspacing="0">
														<tr>
															<td><font>&nbsp;&nbsp;DST</font></td>
															<td><font class="fntBold">Current/Next </font></td>
														</tr>
														<tr>
															<td>
																<font>&nbsp;&nbsp;Start&nbsp;&nbsp;&nbsp;</font>
															</td>
															<td>
																<input type="text" id="lblStarts" name="lblStarts" size="18" readonly="readonly" disabled="disabled">
															</td>				  				
														</tr>
														<tr>
															<td><font>&nbsp;&nbsp;End</font></td>
															<td>
																<input type="text" id="lblEnds" name="lblEnds" size="18" readonly="readonly" disabled="disabled">
															</td>		
														</tr>														
														<tr>
															<td><font>&nbsp;</font></td>
															<td align="left">
																<input tabindex="28" type="button" id="btnDST" name="btnDST" value="View/Edit DST" class="Button" style="width:100px;" onclick="ViewDetail()">
															</td>		
														</tr>
													</table>
												</fieldset>
											</td>
										</tr>
										<tr><td><font>&nbsp;</font></td></tr>
										<tr><td><font>&nbsp;GoShow Agent</font></td></tr>
										<tr>
											<td>
												<script type="text/javascript">
													objCmb = new combo();
													objCmb.id = "selGoShoreAgent";
													comboConfig();
													objCmb.arrData = getEditComboData();
												</script>												
											</td>
										</tr>
										<tr><td><font>&nbsp;</font></td></tr>
										<tr><td><font>&nbsp;</font></td></tr>
										<tr><td><font>&nbsp;</font></td></tr>
										<tr><td><font>&nbsp;Operated by Carrier</font></td></tr>
					  					<tr>
					  						<td width="40%" rowspan="3" >
					  							<select multiple style="width:60px; height:45px;margin-left: 11px;" class="ListBox" id="selCarrierCodes"  onChange="pageOnChange()">
													<c:out value="${requestScope.reqCarrierList}" escapeXml="false" />		
												</select> <font class="mandatory"> &nbsp;* </font>
											</td>
										</tr>
									</table>									
								</td>
				  			</tr>
				  			<tr>
				  				<td><font>City</font></td>
				  				<td colspan="3">
					  				<select tabindex="43"  name="selFormCity" size="1" id="selFormCity" style="width:60px;">
										<option value="-1"></option>
										<c:out value="${requestScope.reqCityList}" escapeXml="false" />
				  				  	</select>									
				  				</td>
				  				<td>
				  					<input tabindex="16" type="radio" title="Latitude" id="rndLatitude1" name="rndLatitude" value="N" class="NoBorder" checked="checked" onChange="pageOnChange()">
				  					<font>North</font>
				  					<input tabindex="17" type="radio" title="Latitude" id="rndLatitude2" name="rndLatitude" value="S" class="NoBorder" onChange="pageOnChange()">
				  					<font>South</font>
				  				</td>
				  			</tr>
				  			<tr>
				  				<td>
				  					<font>Contact</font>
				  				</td>
				  				<td>
					  				<input tabindex="5" type="text" id="txtContact" name="txtContact" size="25" onkeyUp="valContact(this)" onkeyPress="valContact(this)"  maxlength="225" onChange="pageOnChange()"><!--<font class="mandatory">&nbsp*</font> -->
				  				</td>
				  				<td>
				  					<font>Closest Airport</font>
				  				</td>
				  				<td>
					  				<select tabindex="11" name="selClosestAirport" size="1" id="selClosestAirport" style="width:60px;" onChange="pageOnChange()">
										<option value="-1"></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
				  				  </select>
				  				</td>
				  				<td>
				  					<input tabindex="18" name="txtLatitude1" title="Latitude1" type="text" id="txtLatitude1" size="3" maxlength="3" onkeyUp="positiveWholeNumberValidate(this)" onkeyPress="positiveWholeNumberValidate(this)" onChange="pageOnChange()"><!--<font class="mandatory">&nbsp*</font> -->
				  					<input tabindex="19" name="txtLatitude2" title="Latitude2" type="text" id="txtLatitude2" size="3" maxlength="3" onkeyUp="positiveWholeNumberValidate(this)" onkeyPress="positiveWholeNumberValidate(this)" onChange="pageOnChange()">
				  					<input tabindex="20" name="txtLatitude3" title="Latitude3" type="text" id="txtLatitude3" size="3" maxlength="3" onkeyUp="positiveWholeNumberValidate(this)" onkeyPress="positiveWholeNumberValidate(this)" onChange="pageOnChange()">				  									  					
				  				</td>


				  			</tr>
							<tr>
				  				<td>
				  					<font>Phone</font>
				  				</td>
				  				<td>
					  				<input tabindex="6" type="text" id="txtPhone" name="txtPhone" size="25" maxlength="20" onkeyUp="positiveWholeNumberValidate(this)" onkeyPress="positiveWholeNumberValidate(this)" onChange="pageOnChange()" ><!--<font class="mandatory">&nbsp*</font> -->
				  				</td>
				  				<td>
				  					<font>GMT Offset</font><font>&nbsp;&nbsp;(HH:MM)</font>
				  				</td>
				  				<td>
					  				<select tabindex="12" name="selGmtOffSetAction" size="1" id="selGmtOffSetAction" onChange="pageOnChange()">
					  					<option value="+">+</option>
										<option value="-">-</option>
				  				  </select>
					  				
									<input tabindex="13" name="txtGmtOffSetAction" type="text" id="txtGmtOffSetAction" size="3"  maxlength="5" onkeyUp="GmtOffSetPress(this)" onkeyPress="GmtOffSetPress(this)" onblur="setTimeWithColon(document.forms[0].txtGmtOffSetAction)" onChange="pageOnChange()">
									<font class="mandatory"> &nbsp;*</font> 
				  				</td>
				  				<td>
				  					<font>Longitude</font>
				  				</td>
				  						  				
				  			</tr>		
							
				  			<tr>
				  				<td>
				  					<font>Fax</font>
				  				</td>
				  				<td>
					  				<input tabindex="7" type="text" id="txtFax" name="txtFax" size="25"  maxlength="20" onkeyUp="positiveWholeNumberValidate(this)" onkeyPress="positiveWholeNumberValidate(this)" onChange="pageOnChange()"><!--<font class="mandatory">&nbsp*</font> -->
				  				</td>
				  				<td>
				  					<font>Min Stopover Time</font>
									<font>&nbsp;&nbsp;(HH:MM)</font>
				  				</td>
				  				<td>
					  				
									<input tabindex="14" name="txtMinStopOvrTime" type="text" id="txtMinStopOvrTime" size="5"  maxlength="5" onkeyUp="MinStopOvrTimePress(this)" onkeyPress="MinStopOvrTimePress(this)" onblur="setTimeWithColon(document.forms[0].txtMinStopOvrTime)" onChange="pageOnChange()">
									<font class="mandatory"> &nbsp;*</font>
				  				</td>
				  				<td>
				  					<input tabindex="21" title="Longitude" type="radio" id="rndLongitude1" name="rndLongitude" value="E" class="NoBorder" checked="checked" onChange="pageOnChange()">
				  					<font>East</font>
				  					<input tabindex="22" title="Longitude" type="radio" id="rndLongitude2" name="rndLongitude" value="W" class="NoBorder" onChange="pageOnChange()">
				  					<font>West</font>
				  				</td>											

				  			</tr>		
				  			<tr>
				  				<td>
				  					<font>Remarks</font>
				  				</td>
				  				<td colspan="3">
					  				<textarea tabindex="8" name="txtaRemarks" id="txtaRemarks" cols="25" rows="5" style="height:18px;width:320px;" onkeyUp="validateTextArea(this,255);commaValidate(this);" onkeyPress="validateTextArea(this,255);commaValidate(this);" title="Can enter only up to 255 charactors" onChange="pageOnChange()"></textarea>
				  				</td>

				  				<td>
				  					<input tabindex="23" name="txtLongitude1" title="Longitude1" type="text" id="txtLongitude1" size="3" maxlength="3" onkeyUp="positiveWholeNumberValidate(this)" onkeyPress="positiveWholeNumberValidate(this)" onChange="pageOnChange()">
				  					<input tabindex="24" name="txtLongitude2" title="Longitude2" type="text" id="txtLongitude2" size="3" maxlength="3" onkeyUp="positiveWholeNumberValidate(this)" onkeyPress="positiveWholeNumberValidate(this)" onChange="pageOnChange()">
				  					<input tabindex="25" name="txtLongitude3" title="Longitude3" type="text" id="txtLongitude3" size="3" maxlength="3" onkeyUp="positiveWholeNumberValidate(this)" onkeyPress="positiveWholeNumberValidate(this)" onChange="pageOnChange()">				  									  					
				  				</td>
				  			</tr>
				  			<tr>
				  				<td colspan='2'><span id="spnVisibility"></span></td>
				  				 
				  				<td><span id="fntLCCPublish"><font>LCC Publish Status</font></span></td>
				  				<td colspan="3">
					  				<select tabindex="15"  name="selLccPublishStatus" size="1" id="selLccPublishStatus" onChange="stationChange()" style="width:90px;">
										<option value="-1"></option>
										<option value="PUB" disabled="disabled">PUBLISHED</option>
										<option value="TBP">TO BE PUBLISHED</option>
										<option value="TBR" disabled="disabled">TO BE REPUBLISHED</option>
										<option value="SKP">SKIP</option>
				  				  	</select>
				  				</td>
				  			</tr>
				  			<tr>				  				
				  				<td colspan="2"><font>Ancillary Notification Start Cutover</font></td>
				  				<td align="left" nowrap>
					  				<input tabindex="29" title="Anci Notification Start Cutover Day" type="text"  id="anciStartCutoverDay" name="anciStartCutoverDay" size="2"   class="UCase"  maxlength="2" onblur="setDefaultStartCutoverTime();" ><font>&nbsp;(Day)&nbsp;</font>
					  				<input tabindex="30" title="Anci Notification Start Cutover Time(HH:MM)" type="text"  id="anciStartCutoverTime" name="anciStartCutoverTime" size="5" class="UCase"  maxlength="5" onkeyUp="notifStartCutoverTimePress(this)" onkeyPress="notifStartCutoverTimePress(this)" onblur="setTimeWithColon(document.forms[0].anciStartCutoverTime),setDefaultStartCutoverTime()" onChange="pageOnChange()" ><font>&nbsp;(HH:MM)</font> 
				  				</td>
				  				<td colspan="2" align="right"><span id="spnSurfaceSegment"><font>Surface/Bus Station&nbsp;</font></span></td>
				  				<td>
				  					<input tabindex="31" type="checkbox" name="chkSurfaceSegment" id="chkSurfaceSegment" onchange="changeConnectingAirportStatus()"/>
				  				</td>
				  				<td colspan="2" align="right"><font>Enable Auto Flown Processing </font></td>
								<td><input tabindex="35" type="checkbox" id="chkAutoFlownProcessEnabled" name="chkAutoFlownProcessEnabled" onChange="pageOnChange()" >		
								</td>
				  			</tr>
				  			<tr>			  				
				  				<td colspan="2" align="left"><font>Ancillary Notification End Cutover</font></td>
				  				<td align="left" nowrap>
					  				<input tabindex="33" title="Anci Notification End Cutover Day" type="text" id="anciEndCutoverDay" name="anciEndCutoverDay" size="2"   class="UCase"  maxlength="2" onblur="setDefaultEndCutoverTime();"  ><font>&nbsp;(Day)&nbsp;</font>
					  				<input tabindex="34" title="Anci Notification End Cutover Time(HH:MM)" type="text"  id="anciEndCutoverTime" name="anciEndCutoverTime" size="5" class="UCase"  maxlength="5"  onkeyUp="notifEndCutoverTimePress(this)" onkeyPress="notifEndCutoverTimePress(this)" onblur="setTimeWithColon(document.forms[0].anciEndCutoverTime),setDefaultEndCutoverTime()" onChange="pageOnChange()" ><font>&nbsp;(HH:MM)</font>
				  				</td>	
				  				<td colspan="2" align="right"><font>Enable ETL Processing </font></td>
								<td><input tabindex="35" type="checkbox" id="chkETLProcessEnabled" name="chkETLProcessEnabled" onChange="pageOnChange()" >		
								</td>
				  							  				
				  				<td colspan="2" align="right"><span id="spnConnectingAirport"><font>Bus Connection Hub&nbsp;</font></span></td>
				  				<td>
				  					<select tabindex="36" name="selConnectingAirport" id="selConnectingAirport">
				  						<option value=""></option>
				  						<c:out value="${requestScope.reqActiveAirportList}" escapeXml="false" />
				  					</select>
				  				</td>
				  			</tr>
							<tr>			  				
				  				<td colspan="5">&nbsp;</td>
				  			</tr>
				  			<tr>			  				
				  				<td colspan="2" align="left">
									<fieldset>
						  				<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td colspan="2"> 
													<font>Enable Meal Notification</font>
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input tabindex="36" type="checkbox" id="chkMealNotification" name="chkMealNotification" onChange="enableMealNotifyDetails()">		
												</td>
											</tr>
						  					<tr>
						  						<td width="45%"><font>Notification Email(s)</font></td>
						  						<td width="65%">
						  							<input tabindex="37" type="text" id="txtNotifyEmail" name="txtNotifyEmail" maxlength="120" onchange="setPageEdited(true)"/>
						  							<font class="mandatory">&nbsp;*</font>
												</td>
						  					</tr>
						  					<tr>
						  						<td width="45%"><font>Notification Start Time</font></td>
						  						<td width="65%">
						  							<input tabindex="38" title="Meal Notification Start Time(HH:MM)" type="text"  id="mealNotifyStartTime" name="mealNotifyStartTime" size="6" class="UCase"  maxlength="6" onblur="setTimeWithColon2(document.forms[0].mealNotifyStartTime)" onkeyUp='valueValidate(this)' onkeyPress='valueValidate(this)' onChange="pageOnChange()" ><font>&nbsp;(HH:MM)</font>
						  							<font class="mandatory">&nbsp;*</font>
						  						</td>
						  					</tr>
						  					<tr>
						  						<td width="45%"><font>Notification Frequency</font></td>
						  						<td width="65%">
						  							<input tabindex="39" title="Notification Frequency(MM)" type="text"  id="notificationFrequency" name="notificationFrequency" size="3" class="UCase"  maxlength="3" onkeyUp='valueValidateWithoutColon(this)' onkeyPress='valueValidateWithoutColon(this)' onChange="pageOnChange()" ><font>&nbsp;(MM)</font>
						  							<font class="mandatory">&nbsp;*</font>
						  						</td>
						  					</tr>
						  					<tr>
						  						<td width="45%"><font>Last Notification Time</font></td>
						  						<td width="65%">
						  							<input tabindex="40" title="Meal Notification End Time(HH:MM)" type="text"  id="lastNotifyTime" name="lastNotifyTime" size="6" class="UCase"  maxlength="6"  onblur="setTimeWithColon2(document.forms[0].lastNotifyTime)" onkeyUp='valueValidate(this)' onkeyPress='valueValidate(this)' onChange="pageOnChange()" ><font>&nbsp;(HH:MM)</font> 							
						  							<font class="mandatory">&nbsp;*</font>
						  						</td>
						  					</tr>
						  				</table>
						  			</fieldset>
								</td>
				  				<td>&nbsp;</td>				  				
				  				<td colspan="2">&nbsp;</td>
				  				<td>&nbsp;</td>
				  			</tr>
							<tr>			  				
				  				<td colspan="5">&nbsp;</td>
				  			</tr>
				  			<tr>
				  				<td colspan="5">				                   
				                </td>
				                <td align="right" colspan="2" nowrap>									
								</td>
				  			</tr>				
				  			<tr>
				  				<td colspan="5">
									<input tabindex="40" type="button" id="btnClose" class="Button" value="Close"  onclick="closeParentAndChildWindow()">
				                    <input tabindex="41" name="btnReset" type="button" class="Button" id="btnReset" onClick="resetAirport()" value="Reset">
				                </td>
				                <td align="right" colspan="2" nowrap>
									<input tabindex="42" name="btnSave" type="button" class="Button" id="btnSave" onClick="saveAirport()" value="Save">
								</td>
				  			</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>		
		<input type="hidden" name="hdnMode" id="hdnMode">
		<input type="hidden" name="hdnVersion" id="hdnVersion" value="">			
		<input type="hidden" name="hdnRecNo" id="hdnRecNo">		
		<input type="hidden" name="hdnGMTOffset" id="hdnGMTOffset" value="">
		<input type="hidden" name="hdnGridRow" id="hdnGridRow" value="">
		<input type="hidden" name="hdnGoShowAgent" id="hdnGoShowAgent" value="">
		<input type="hidden" name="hdnAirportForDisplay" id="hdnAirportForDisplay" value="">
		<input type="hidden" name="hdnLCCVisiblity" id="hdnLCCVisiblity" value="">
		<input type="hidden" name="hdnCarrierCode" id="hdnCarrierCode" value="">
		<input type="hidden" name="hdnSearchData" id="hdnSearchData"/> 
		<input type="hidden" name="hdnLccPublishStatus" id="hdnLccPublishStatus" value="">

   </form>	
  </body>
  <script type="text/javascript">
    	 var languageHTML = "<c:out value="${requestScope.reqLanguageList}" escapeXml="false" />";
  		 var listForDisplayEnabled = <c:out value="${requestScope.reqListForDisplayEnabled}" escapeXml="false" />;
  </script>
<script src="../../js/airport/AirportValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>	
<script src="../../js/airport/AirportAdminGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
  <script type="text/javascript">
  	<!--
  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
  	function ClearProgressbar(){
  		if (typeof(objDG) == "object"){
	  		if (objDG.loaded){
	  			clearTimeout(objProgressCheck);
	  			top[2].HideProgress();
	  		}
	  	}	  	
  	}
  	//-->
  </script>

</html>