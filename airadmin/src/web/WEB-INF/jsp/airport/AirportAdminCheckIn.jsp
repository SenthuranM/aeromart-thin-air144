 <%-- 
	 @Author 	: 	Srikanth
	 @Copyright : 	ISA
  --%>
<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<%@ page import="java.util.*, java.text.*" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 	<head>
	    <title>Airport Admin Check In Times</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="-1">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
		<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
		
		<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> 
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
		<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script> 	

 	</head>
  	<body scroll="no" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false' 
  			onUnload="opener.resetVariables()"  onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
		<%@ include file="../common/IncludeWindowTop.jsp"%>	
		<form method="post"  id="AirportAdminCITFrom" action="showAirportCIT.action">
		<script type="text/javascript">
			<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			<c:out value="${requestScope.reqFormData}" escapeXml="false" />
			<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
		</script>
		<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround" ID="Table1">
			<tr>
			  
				<td width="13" class="BannerBorderLeft"><img src="../../images/spacer_no_cache.gif" width="13" height="1"></td>
				<td valign="top">
					<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBorder" ID="Table7">
						<tr>
							<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
							<td valign="top" align="center" class="PageBackGround">
								
							</td>
							<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
						</tr>
					</table>
				</td>
				<td width="12" class="BannerBorderRight"><img src="../../images/spacer_no_cache.gif" width="12" height="1"></td>
			</tr>
		</table>  

		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
			<tr>				
				<td><font class="Header">Check In Time - Airport&nbsp;&nbsp;<span id="spnCITHeader"></span></font></td>
			</tr>
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Check In Time<%@ include file="../common/IncludeFormHD.jsp"%>	
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr><td><font>&nbsp;</font></td></tr>
						<tr>
							<td style="height:125px;" valign="top">
								<span id="spnCI"></span>
							</td>
						</tr>
						<tr><td><font>&nbsp;</font></td></tr>
						<tr>
							<td>
								<u:hasPrivilege privilegeId="sys.mas.airport.cit.add">
									<input name="btnAdd" type="button" class="Button" id="btnAdd" value="Add" onClick="addClick()">
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="sys.mas.airport.cit.edit">
									<input name="btnEdit" type="button" class="Button" id="btnEdit" value="Edit"  onClick="editClick()">
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="sys.mas.airport.cit.delete">
									<input name="btnDelete" type="button" class="Button" id="btnDelete" value="Delete"  onClick="deleteClick()">
								</u:hasPrivilege>
								
							</td>
						</tr>
					</table>
					<%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Add / Modify Check In Time<%@ include file="../common/IncludeFormHD.jsp"%>	
					<br>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="30%"><font>Carrier Code</font></td>
							<td width="12%">
								<!-- <input tabindex="1" type="text" name="txtCarrierCode" id="txtCarrierCode" size="20"  "> -->
								<select style="width:60px;" class="ListBox" id="selCarrierCode" name="selCarrierCode" onChange="pageOnChange()">
									<option value=""></option>
									<c:out value="${requestScope.reqCarrierList}" escapeXml="false" />		
								</select>
							</td>
							<td width="8%">
								
							</td>												
							<td width="15%" align="right"></td>					
							<td width="18%" align="left"></td>
							<td width="11%" >								
							</td>															
						</tr>
						<tr>
							<td colspan="8"></td>
						</tr>
						<tr>
							<td width="30%" align="left"><font>Flight Number</font></td>
							<td width="12%">
								<!--  <input tabindex="1" type="text" name="txtFlightNo" id="txtFlightNo" size="20" onChange="pageOnChange()" /> -->
								<select style="width:90px;" class="ListBox" id="txtFlightNo" name="txtFlightNo" onChange="pageOnChange()">
									<option value=""></option>
									<c:out value="${requestScope.reqFlightListData}" escapeXml="false" />		
								</select>
							</td>
							<td width="5%">
								
							</td>												
							<td width="15%" align="right">
							</td>
							<td width="10%" align="left"></td>
							<td width="11%"></td>
						</tr>
						<tr>
							<td colspan="8"></td>
						</tr>
						<tr>
							<td width="30%" align="left"><font>Check In starting time(HH:MM Before departure) </font></td>
							<td width="12%">
								<input tabindex="1" type="text" name="txtCIT" id="txtCIT" size="5" onkeyUp="CheckInTimePress(this)" onkeyPress="CheckInTimePress(this)" onblur="setTimeWithColon(document.forms[0].txtCIT)" onChange="pageOnChange()" maxlength="5"/>
								<font class="mandatory">&nbsp;*</font>
							</td>
							<td width="5%">
								
							</td>												
							<td width="15%" align="right">
							</td>
							<td width="10%" align="left"></td>
							<td width="11%"></td>
						</tr>
						<tr>
							<td colspan="8"></td>
						</tr>
						<tr>
							<td width="50%" align="left"><font>Check In closing time(HH:MM Before departure) </font></td>
							<td width="12%">
								<input tabindex="1" type="text" name="txtCIClosingTime" id="txtCIClosingTime" size="5" onkeyUp="checkCheckInClosingTime(this)" onkeyPress="checkCheckInClosingTime(this)" onblur="setTimeWithColon(document.forms[0].txtCIClosingTime)" onChange="pageOnChange()" maxlength="5"/>
							</td>
							<td width="5%">
								
							</td>												
							<td width="15%" align="right">
							</td>					
							<td width="10%" align="left"></td>
							<td width="11%"></td>
						</tr>
						<tr>
							<td colspan="8"></td>
						</tr>
						<tr>
							<td colspan="8">&nbsp;</td>
						</tr>
				 		
						<tr>
							<td colspan="8">&nbsp;</td>
						</tr>
						</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td colspan="4">									
								<input tabindex="14" type="button" id="btnClose" class="Button" value="Close" onclick="windowclose()">	
								<input tabindex="15" name="btnReset" type="button" class="Button" id="btnReset" onClick="resetAirportCit()" value="Reset" >
							</td>
							<td align="right" colspan="4">
								<input tabindex="16"  name="btnSave" type="button" class="Button" id="btnSave" onClick="saveAirportCit()" value="Save">
							</td>
						</tr>
					</table>
					<%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>	
			<tr><td><font class="fntSmall"></font></td></tr>							
		</table>
		<%@ include file="../common/IncludeWindowBottom.jsp"%>
		<script src="../../js/airport/AirportAdminCheckIn.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>		
		<input type=hidden name="hdnAirportCode"  id="hdnAirportCode">	
		<input type=hidden name="hdnCITId"  id="hdnCITId">	
		<input type=hidden name="hdnMode" id="hdnMode">
		<input type="hidden" name="hdnVersion" id="hdnVersion" value="">	
		<input type="hidden" name="hdnGridRow" id="hdnGridRow" value="">
		
	</form>	
  </body>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>	
	<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/airport/AirportCITValidation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
</html>
