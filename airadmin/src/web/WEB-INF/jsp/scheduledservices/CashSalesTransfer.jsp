 <%-- 
	 @Author 	: 	Menaka P. Wedige
	 @Copyright : 	ISA
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
 <%@ page language="java"%>

<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date
	cal.add(Calendar.MONTH,1);
	String CurrentDate =  formatter.format(dStartDate);
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>View Cash Sales Transfer Status</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">

	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> 
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>	
	<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </head>

  <body scroll="no" class="tabBGColor" scroll="no" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" ondrag='return false'
  	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">

  	<form method="post" action="showCashSalesTransfer.action" id="frmCashSales">
  		<script type="text/javascript">
			var arrData = new Array();
			var arrCashData = new Array();		
			<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
			<c:out value="${requestScope.reqOperationUIMode}" escapeXml="false" />
			<c:out value="${requestScope.reqFormData}" escapeXml="false" />
			<c:out value="${requestScope.reqFormFieldValues}" escapeXml="false" />
			<c:out value="${requestScope.reqViewReportStatus}" escapeXml="false" />
			var totString = "<c:out value="${requestScope.total_cash}" escapeXml="false" />";
		</script>
		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Search Cash Sales<%@ include file="../common/IncludeFormHD.jsp"%>					  
					<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table2">
						<tr>
							<td align="left" width="5%"><font>From</font></td>
							<td align="left" width="30%">
								<input tabindex="1"  type="text" name="txtFrom" id="txtFrom" size="12" maxlength="10" invalidText="true" onBlur="fromDateValidation('txtFrom')" onChange="pageOnChange()">
								<a href="javascript:void(0)"  onclick="LoadCalendar(0,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory">&nbsp*</font> 
	                        </td>
							<td width="60%" rowspan="3" width="75%"><span tabindex="3" id="spn1" class="FormBackGround"></span></td>
						</tr>
						<tr>
							<td align="left" width="5%"><font>To</font></td>
							<td align="left" width="30%">
								<input tabindex="2"  type="text" name="txtTo" id="txtTo" size="12" maxlength="10" invalidText="true" onBlur="toDateValidation('txtTo')" onChange="pageOnChange()"> 
								<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory">&nbsp*</font>
							</td>
							<td align="left" width="65%">&nbsp;</td>
						</tr>
						<tr>								
							<td align="left" width="5%"><font>Agent</font></td>
							<td align="left" width="30%">
								<select tabindex="3"  name="selAgent" id="selAgent" onChange="AgentChange()">
									<option value="-1"></option>
									<c:out value="${requestScope.reqAgentList}" escapeXml="false" />
	                            </select><font class="mandatory">&nbsp;*</font>
	                         </td>
								<td colspan="3" width="65%" align="right" valign="bottom"><input tabindex="4"  name="btnSearch" type="button" class="button" id="btnSearch" onClick="searchData()" value="Search"></td>
							</tr>
					  	</table>						
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>		
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Cash Sales<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
								<span id="spnCashSalesTransfer"></span>
								<span id="spnTotalCashSales"></span>
								</td>
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td><%@ include file="../common/IncludeFormTopFrameLess.jsp"%>	
						<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table">
							<tr>
								<td style="height:42px;" colspan="2" valign="bottom">
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td>
												<input tabindex="6" type="button" id="btnClose" class="Button" value="Close"  onclick="top[1].objTMenu.tabRemove(screenId)">	
											 </td>
											 <td align="right">
											 	<input tabindex="7" style="width:130px;" name="btnViewReport" type="button" class="Button" id="btnViewReport" onClick="viewReport()" value="Download">
											 	<input tabindex="8" style="width:130px;" name="btnGenTransfer" type="button" class="Button" id="btnGenTransfer" onClick="generateOrTransfer()" value="Generate/Transfer">
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>	
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
			<script src="../../js/scheduledservices/CashSalesGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
			<input type="hidden" name="hdnInvId" id="hdnInvId"/>	
			<input type="hidden" name="hdnVersion" id="hdnVersion"/>	
			<input type="hidden" name="hdnGridRow" id="hdnGridRow"/>
			<input type="hidden" name="hdnDateOfSale" id="hdnDateOfSale"/>	
			<input type="hidden" name="hdnMode" id="hdnMode"/>	
			<input type="hidden" name="hdnUIMode" id="hdnUIMode"/>
			<input type="hidden" name="hdnRecNo" id="hdnRecNo">	
			<input type="hidden" name="hdnFromDate" id="hdnFromDate"/>	
			<input type="hidden" name="hdnToDate" id="hdnToDate"/>	
			<input type="hidden" name="hdnAction" id="hdnAction"/>	
			<input type="hidden" name="hdnUsers" id="hdnUsers"/>	
			<input type="hidden" name="hdnSelectedDates" id="hdnSelectedDates"/>
			<input type="hidden" name="hdnSelectedRows" id="hdnSelectedRows"/>
			<input type="hidden" name="hdnStaffId" id="hdnStaffId"/>
			<input type="hidden" name="hdnCurrentDate" id="hdnCurrentDate" value="<%=CurrentDate %>">

		</form>	
		 <script type="text/javascript">
			<c:out value="${requestScope.reqUserData}" escapeXml="false" />
		 </script>
  	</body>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/scheduledservices/CashSalesValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  <script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
    if (objDG.loaded){
     clearTimeout(objProgressCheck);
     top[2].HideProgress();
    }
   }
    
   //-->
  </script>

</html>