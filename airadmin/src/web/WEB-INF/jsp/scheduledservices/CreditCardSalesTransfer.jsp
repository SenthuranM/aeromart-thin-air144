 <%-- 
	 @Author 	: 	Menaka P. Wedige
	 @Copyright : 	ISA
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<%@ page language="java"%>
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date
	cal.add(Calendar.MONTH,1);
	String CurrentDate =  formatter.format(dStartDate);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>View Credit sales Transfer Status</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">

	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> 
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script  src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>	
	<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
  </head>

  	<body scroll="no" class="tabBGColor" scroll="no"  onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'
  		onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">

  	<form method="post" action="showCreditCardSalesTransfer.action">
		<script type="text/javascript">
			var arrData = new Array();
			var arrCreditData = new Array();
			<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
			<c:out value="${requestScope.reqOperationUIMode}" escapeXml="false" />
			<c:out value="${requestScope.reqFormData}" escapeXml="false" />
			<c:out value="${requestScope.reqFormFieldValues}" escapeXml="false" />
			<c:out value="${requestScope.reqViewReportStatus}" escapeXml="false" />
			var totString = "<c:out value="${requestScope.total_credit}" escapeXml="false" />";
		</script>
		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Search Credit Sales Transfer Status<%@ include file="../common/IncludeFormHD.jsp"%>					  
					<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table2">
						<tr>
							<td align="left" width="5%"><font>From</font></td>
							<td align="left" width="20%">
								<input tabindex="1"  type="text" name="txtFrom" id="txtFrom" size="12" maxlength="10" invalidText="true" onBlur="fromDateValidation('txtFrom')" onChange="pageOnChange()">
								<a href="javascript:void(0)"  onclick="LoadCalendar(0,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory">&nbsp*</font> 
	                        </td>
							<td width="60%" rowspan="3" width="75%"><span id="spn1" class="FormBackGround"></span></td>
						</tr>
						<tr>
							<td align="left" valign="top" width="5%"><font>To</font></td>
							<td align="left" valign="top" width="20%">
								<input tabindex="2"  type="text" name="txtTo" id="txtTo" size="12" maxlength="10" invalidText="true" onBlur="toDateValidation('txtTo')" onChange="pageOnChange()"> 
								<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory">&nbsp*</font>
							</td>
							<td align="left" width="75%">&nbsp;</td>
						</tr>
						<tr>								
							<td align="left" width="5%" >&nbsp;</td>
							<td align="left" width="20%">&nbsp;</td>
							<td colspan="3" width="75%" align="right" valign="bottom"><input tabindex="4"  name="btnSearch" type="button" class="button" id="btnSearch" onClick="searchData()" value="Search"></td>
						</tr>
					  </table>
				  	<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>		
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Credit Sales<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
								<span id="spnCreditCardSalesTransfer"></span>
								<span id="spnTotalCreditSales"></span>
								</td>
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>

				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td><%@ include file="../common/IncludeFormTopFrameLess.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table">
							<tr>
							  <td style="height:42px;" colspan="2" valign="bottom">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td>
											<input tabindex="5" type="button" id="btnClose" class="Button" value="Close"   onclick="top[1].objTMenu.tabRemove(screenId)">	
										 </td>
										 <td align="right">
										 	<input tabindex="6" style="width:130px;" name="btnViewReport" type="button" class="Button" id="btnViewReport" onClick="viewReport()" value="Download">
										 	<input  tabindex="7"  style="width:130px;" name="btnGenTransfer" type="button" class="Button" id="btnGenTransfer" onClick="generateOrTransfer()" value="Generate/Transfer">
										</td>
									</tr>
								</table>
						   	</td>
						</tr>
					</table>							
						  <%@ include file="../common/IncludeFormBottom.jsp"%>	
				</td>
			</tr>
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
		<script src="../../js/scheduledservices/CreditCardSalesGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<input type="hidden" name="hdnVersion" id="hdnVersion"/>	
		<input type="hidden" name="hdnGridRow" id="hdnGridRow"/>
		<input type="hidden" name="hdnMode" id="hdnMode"/>	
		<input type="hidden" name="hdnUIMode" id="hdnUIMode"/>
		<input type="hidden" name="hdnRecNo" id="hdnRecNo">	
		<input type="hidden" name="hdnDateOfSale" id="hdnDateOfSale"/>	
		<input type="hidden" name="hdnCardType" id="hdnCardType"/>	
		<input type="hidden" name="hdnAmount" id="hdnAmount"/>	
		<input type="hidden" name="hdnTransTimestamp" id="hdnTransTimestamp"/>
		<input type="hidden" name="hdnTransStatus" id="hdnTransStatus"/>
		<input type="hidden" name="hdnCardTypes" id="hdnCardTypes"/>
		<input type="hidden" name="hdnSelectedRows" id="hdnSelectedRows"/>
		<input type="hidden" name="hdnSelectedDates" id="hdnSelectedDates"/>
		<input type="hidden" name="hdnSelectedCardTypes" id="hdnSelectedCardTypes"/>
		<input type="hidden" name="hdnIndex" id="hdnIndex"/>
		<input type="hidden" name="hdnCurrentDate" id="hdnCurrentDate" value="<%=CurrentDate %>">

	</form>	
<script type="text/javascript">
	<c:out value="${requestScope.reqCardTypeData}" escapeXml="false" />
</script>

  </body>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/scheduledservices/CreditCardSalesValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
   <script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
    if (objDG.loaded){
     clearTimeout(objProgressCheck);
     top[2].HideProgress();
    }
   }
    
   //-->
  </script>


</html>