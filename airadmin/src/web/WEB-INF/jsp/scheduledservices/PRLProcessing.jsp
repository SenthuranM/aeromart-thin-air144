<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<%@ page language="java"%>
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	DateFormat timeFormatter = new SimpleDateFormat("HH:mm");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date
	cal.add(Calendar.MONTH,1);
	String CurrentDate =  formatter.format(dStartDate);
	String currentTime= timeFormatter.format(dStartDate);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>View PRL Processing</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">

	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> 
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
    <script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/scheduledservices/PFSEditContentPopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/scheduledservices/PFSAlternateFlight.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

  </head>

  <body scroll="no" class="tabBGColor" scroll="no" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" ondrag='return false' onbeforeunload="beforeUnload()"
  	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">

  <form method="post" action="showPRLProcessing.action" id=frmPRL>
  			<script type="text/javascript">
				var arrData = new Array();
				var arrPRLData = new Array();
				var currentRow="";
				var totalRes;
				var arrFormData= null;
				var popupError = "";
				//currentRow=<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />;
				<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
				<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
				<c:out value="${requestScope.reqOperationUIMode}" escapeXml="false" />
				<c:out value="${requestScope.reqFormData}" escapeXml="false" />
				<c:out value="${requestScope.reqFormFieldValues}" escapeXml="false" />
				<!--c:out value="${requestScope.reqSystemDate}" escapeXml="false" /-->
				
				//createPopUps1();
				//var arrFLData = new Array();
				//<c:out value="${requestScope.reqAlternateFlight}" escapeXml="false" />	
				
				popupError = "<c:out value="${requestScope.reqPopupMessage}" escapeXml="false" />"
			</script>
			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr><td>
				<table width="99%">
				<tr>
					<td width="88%" align="left">
							<font>All times are in local</font> 
					</td>
					<!--td  align="right">
							<font class="fntSmall">All times in local</font>
					</td-->
				</tr>
				</table>
				</td>
				</tr>
				<!--tr>
					<td>
						<font >All times in local</font>
				</td>
				</tr-->
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						Search PRL							
						<%@ include file="../common/IncludeFormHD.jsp"%>					  
						<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table2">
							<tr>
								<td align="left"><font>From Date </font></td>
								<td align="left">
									<input tabindex="1" type="text" name="txtFrom" id="txtFrom" size="12" maxlength="10" invalidText="true" onBlur="fromDateValidation('txtFrom')" >
									<a href="javascript:void(0)"  onclick="LoadCalendar(0,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a>
	                            </td>
								<td align="left"><font>To Date</font></td>
								<td align="left">
									<input tabindex="2" type="text" name="txtTo" id="txtTo" size="12" maxlength="10" invalidText="true" onBlur="toDateValidation('txtTo')" > 
									<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a>
								</td>
								<td align="left"><font>Airport</font></td>
								<td align="left">
									<select tabindex="3" name="selAirport" id="selAirport" >
										<option value=""> </option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
	                                </select>
	                            </td>
								<td align="left"><font>Processing Status</font></td>
								<td align="left">
									<select tabindex="4" name="selProcessType" id="selProcessType" >
										
	                                </select>
	                            </td>
								<td align="right">
								<input tabindex="5" name="btnSearch" type="button" class="button" id="btnSearch" onClick="searchData()" value="Search"></td>
							</tr>
					  	</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr>		
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Received PRL<%@ include file="../common/IncludeFormHD.jsp"%> 
						<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">
							<!--tr><td><font class="fntSmall">&nbsp;</font></td></tr-->
							<tr>
								<td>
								<span id="spnPRLData"></span>
								</td>
							</tr>
							<tr>
							<td>
								<input type="button" id="btnDelete" class="Button" value="Delete" onClick="deleteClick()">
							</td>
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>

				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
							
						<%@ include file="../common/IncludeFormHD.jsp"%>			
							  <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center" ID="Table1">
								<tr><td>
									<table width="98%" border="0" cellpadding="0" cellspacing="0" align="left" id="Table2">
											<tr>
												<td width="50%" valign="top">
													<table>
													 		<tr>
																<td  align="left"><font>PRL ID</font></td>
																<td ><font class="fntBold"><span id="spnPRLID"></span></font></td>
																<td ></td>															
															</tr>
												
															<tr>
																<td align="left"><font>Download Date </font></td>
																<td><input  name="txtDownloadTS" type="text" invalidText="true" id="txtDownloadTS" onBlur="dateChk('txtDownloadTS')" onchange="pageOnChange()" size="10" maxlength="10"  tabindex="8"><a href="javascript:void(0)" onclick="LoadCalendarForAdd(0,event); return false;" title="View Calendar">&nbsp;&nbsp;<img src="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory" >&nbsp*</font>
																</td>
																<td><font>&nbsp;&nbsp;Time <input  name="txtDownloadTime" type="text"  id="txtDownloadTime"  onblur="setTimeWithColon(document.forms[0].txtDownloadTime)" onchange="pageOnChange()" size="5" maxlength="5"  tabindex="9"></font>
																</td>	
															</tr>														
						
															<tr>
															    <td align="left"><font>Flight Number</font></td>
																<td ><input type="text" name="txtFlightNo" id="txtFlightNo" size="7" maxLength="7" onchange="pageOnChange()" tabindex="10"></td>
																<td >
																</td>
																	
															</tr>
						
															<tr>
																<td align="left"><font>Flight Date</font></td>
																<td ><input type="text" name="txtFlightDateTime" invalidText="true" id="txtFlightDateTime" size="10" maxlength="10"  onchange="pageOnChange()" onBlur="dateChk('txtFlightDateTime')"  tabindex="11"><a href="javascript:void(0)" onclick="LoadCalendarForAdd(1,event); return false;" title="View Calendar">&nbsp;&nbsp;<img src="../../images/calendar_no_cache.gif" border="0"></a>
																<td ><font>&nbsp;&nbsp;Time <input  name="txFlightTime" type="text"  id="txFlightTime" onblur="setTimeWithColon(document.forms[0].txFlightTime)" onchange="pageOnChange()" size="5" maxlength="5"  tabindex="12"></font>
																</td>																				
															</tr>
																					
															<tr>
																<td align="left"><font>Airport </font>
																<td><select  name="selDest" id="selDest" onChange="pageOnChange()" tabindex="13">
																<option value="-1"> </option>
																<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
																</select><!--font class="mandatory">&nbsp*</font--></td>
																<td ></td>																
															</tr>
															
															<tr>
																<td align="left"><font>SITA Address</font></td>
																<td ><input  name="txFromAddress" type="text" id="txFromAddress" maxlength="30" size="15" onchange="pageOnChange()" tabindex="14"><!--font class="mandatory">&nbsp*</font--></td>
																<td"></td>																
															</tr>
															
															 <tr>
																<td align="left"><font>Processing Status</font></td>
																<td><select  name="selStatus" id="selStatus"  onchange="pageOnChange()" tabindex="15" style="width:80px;"></select></td>
																<td></td>																
															</tr>	
														</table>
													</td>
													<td width="50%" valign="top">
														<table>
															<tr>
																<td align="left"><b><u><font>PRL Content View</font></u></b></td>
															</tr>	
															<tr>
																<td>
																	<font><span id="spnContent" name="spnContent"  STYLE="overflow: scroll; width:350; height:100; border:1 #000000 solid; text-align: left;  padding: 2px"></span></font>
																</td>
																<!--textarea name="txtaRulesCmnts" id="txtaRulesCmnts" cols="70" rows="11" maxlength="225" onkeyUp="validateTextArea(this)" onkeyPress="validateTextArea(this)" title="Can enter only up to 255 charactors" readOnly="true" ></textarea--></td>						
															</tr>					
														</table>
													</td>
											</tr>
									</table>
								</td></tr>
								
							  </table>
								<!--/span-->
								</td></tr>

								<tr>
								  <td style="height:42px;" colspan="2" valign="bottom">
										<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td>
													<input tabindex="16" type="button" id="btnClose" class="Button" value="Close"  onclick="closeParentAndChildWindow()">
													<input tabindex="17" name="btnReset" type="button" class="Button" id="btnReset"  onClick="resetPRL()" value="Reset">
												 </td>
												<td align="right">
													<input tabindex="20" name="btnProcess" type="button" class="Button"
														   id="btnProcess" onClick="processSavePRL('PROCESS')"
														   value="Process">
													&nbsp;
													<input tabindex="21" name="btnPRLDetails" type="button"
														   class="Button" id="btnPRLDetails" onClick="viewPRLDetails()"
														   value="PRL Details">
													&nbsp;
												</td>
											</tr>
										</table>
								   </td>
								</tr>
							  </table>
							
						 
						 </td>
					</tr>
				 
			</table>

		<script src="../../js/scheduledservices/PRLProcessingGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<input type="hidden" name="hdnId" id="hdnId"/>	
		<input type="hidden" name="hdnVersion" id="hdnVersion"/>	
		<input type="hidden" name="hdnGridRow" id="hdnGridRow"/>
		<input type="hidden" name="hdnMode" id="hdnMode"/>	
		<input type="hidden" name="hdnUIMode" id="hdnUIMode"/>
		<input type="hidden" name="hdnRecNo" id="hdnRecNo"  value="1">	
		<input type="hidden" name="hdnFromDate" id="hdnFromDate"/>	
		<input type="hidden" name="hdnToDate" id="hdnToDate"/>	
		<input type="hidden" name="hdnAirport" id="hdnAirport"/>	
		<input type="hidden" name="hdnText" id="hdnText"/>
		<input type="hidden" name="hdnPRLData" id="hdnPRLData"/>
		<input type="hidden" name="hdnPRLID" id="hdnPRLID"/>
		<input type="hidden" name="hdnPaxVersion" id="hdnPaxVersion"/>
		<input type="hidden" name="hdnPaxID" id="hdnPaxID"/>
		<input type="hidden" name="hdnCurrentPrl" id="hdnCurrentPrl"/>
		<input type="hidden" name="hdnCurrentRowNum" id="hdnCurrentRowNum"/>
		<input type="hidden" name="hdnPRLAddedData" id="hdnPRLAddedData"/>
		<input type="hidden" name="txtaRulesCmnts" id="txtaRulesCmnts"/>
		<input type="hidden" name="hdnPaxCount" id="hdnPaxCount"/>


		<input type="hidden" name="hdnCurrentDate" id="hdnCurrentDate" value="<%=CurrentDate %>">
		<input type="hidden" name="hdnCurrentTime" id="hdnCurrentTime" value="<%=currentTime %>">
		
	</form>	
  </body>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>	
	<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/scheduledservices/PRLProcessingValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
  
	
  <script type="text/javascript">
   <!--
   createPopUps();
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
    if (objDG.loaded){
     clearTimeout(objProgressCheck);
     top[2].HideProgress();
    }
   }

   //-->
  </script>

</html>

