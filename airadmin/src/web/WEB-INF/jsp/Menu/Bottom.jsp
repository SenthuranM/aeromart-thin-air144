<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  language="javascript" ></script>
	<script src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  language="javascript" ></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
  </head>
	 <body scroll="no"  onmousewheel="return false;" onkeypress='return Body_onKeyPress(event)' onkeydown='return Body_onKeyDown(event)' scroll="no" oncontextmenu="return true"  ondrag='return false'>
		<table width="999" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround" ID="Table1">
			<tr>
				<td width="13" class="BannerBorderLeft"><img src="../../images/spacer_no_cache.gif" width="13" height="1"></td>
				<td width="974" height="24" align="left" valign="bottom"><img src="../../images/bottom6_no_cache.jpg"></td>
				<td width="12" class="BannerBorderRight"><img src="../../images/spacer_no_cache.gif" width="12" height="1"></td>
			</tr>
			<tr>
				<td width="13" class="BannerBorderLeft"><img src="../../images/spacer_no_cache.gif" width="13" height="1"></td>
				<td height="20" align="left" valign="top">
					<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBottomColor" style="height:100%;" ID="Table5">
						<tr>
							<td width="30" class="PageBottomShadow"><img  id='imgEditMode' name='imgEditMode' src="../../images/spacer_no_cache.gif"></td>
							<td width="914" align="center" height="25" valign="middle">
								<table width="100%" border="0" cellpadding="0" cellspacing="1" ID="Table6">
									<tr>
										<td width="20" valign="bottom"><a href="javascript:void(0);" onclick="ShowPageMessage();" title="View Message(s)"><img id="imgError" name="imgError" src="../../images/spacer_no_cache.gif" border="0"></a></td>
										<td width="1"><img src="../../images/StatusBar_no_cache.jpg"></td>
										<td class="StatsBarDef" id="tdSTBar"><span id="spnDate">&nbsp;</span></td>
										<td width="1"><img src="../../images/StatusBar_no_cache.jpg"></td>
										<td width="150" align="center"><span id="spnUID"></span></td>
										<td width="1"><img src="../../images/StatusBar_no_cache.jpg"></td>
										<td width="200" align="center"><span id="spnScreenID"></span></td>
									</tr>
								</table>
							</td>
							<td width="30" class="PageBottomShadow"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
						</tr>
					</table>
				</td>
				<td width="12" class="BannerBorderRight"><img src="../../images/spacer_no_cache.gif" width="12" height="1"></td>
			</tr>
		</table>
		<span style='position:absolute;top:0px;left:30px;'><img src='../../images/M003_no_cache.gif' alt='AirLine Logo'></span>
		<div id="errorMSGouter" style="display: none;">
			<div class="errorBox">
				<span id="errorMSG" style=""></span>
				<span class="errorClose">&nbsp;</span>
			</div>
		</div>
		<div id="errorPopup" style="display: none;">
			<div class="errorPopupHeader">
				<span class="text">Error Console</span>
				<span class="errorClose">&nbsp;</span>
				<div style="clear:both"></div>
			</div>
			<div class="errorPopupButton" style="">
				<a href="javascript:void(0)" class="errorBtn showAll errorSelected">All</a>
				<a href="javascript:void(0)" class="errorBtn showErrors"><span>&nbsp;</span>Errors</a>
				<a href="javascript:void(0)" class="errorBtn showWarnings"><span>&nbsp;</span>Warnings</a>
				<a href="javascript:void(0)" class="errorBtn showConfirm"><span>&nbsp;</span>Confirms</a> 
				<span class="errorSep">|</span>
				<a href="javascript:void(0)" class="errorBtn errorClear">Clear</a>
				<div style="clear:both"></div>
			</div>
				<div class="errorPopupbody" style="">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<div>
									<table id="errorPopupTable" border="0" width="100%" cellspacing="0" cellpadding="4">
									
									</table>
								</div>
							</td>
						</tr>
					</table>
				</div>
		</div>
	
	<script type="text/javascript">
<!--
	function getthedate(){
		var arrDay= new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
		var arrMonth = new Array("January","February","March","April","May","June","July","August","September","October","November","December")
		
		var dtCurrent = new Date()
		var intYear = dtCurrent.getYear()
		if (intYear < 1000)
			intYear+=1900
			var intDay = dtCurrent.getDay()
			var intMonth = dtCurrent.getMonth()
			var intDayM = dtCurrent.getDate()
			
		if (intDayM<10){intDayM="0"+intDayM;}
		var cdate="<b><font class='fontPGInfo'>"+arrDay[intDay]+", "+arrMonth[intMonth]+" "+intDayM+", "+intYear+"<\/font><\/b>"
		DivWrite("spnDate", cdate);	
	}	
	
	// Write to Divs / span
	function DivWrite(strDivID, strText){
		var objControl ;
		if (document.getElementById)
		{	objControl = document.getElementById(strDivID);
			objControl.innerHTML = "";
			objControl.innerHTML = strText;
		}
		else if (document.all)
		{
			objControl = document.all[strDivID];
			objControl.innerHTML = strText;
		}
		else if (document.layers)
		{
			objControl = document.layers[strDivID];
			objControl.document.open();
			objControl.document.write(strText);
			objControl.document.close();
		}
	}
	
	var erroImgPath = "../../images/error_icon_no_cache.gif";
//getthedate();	
//-->
</script>
<script src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

  	</body>
</html>
</html>