<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title></title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<script type="text/javascript" src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script language="javascript">
		var errorLog = [];
		var pageEdited = false;
		var strLastPage = "";
		var isPWDReseted = false;
		var arrQuickLaunch  = new Array();
		//the state of the report schedule app parameter
		var logicalCCVisibility =  "<c:out value='${requestScope.reqLogicalCCVisibility}' escapeXml='false' />";
		var strReportScheduleEnabled = "<c:out value='${requestScope.reqReportSchedularAppParam}' escapeXml='false' />";
		
		var arrPrivi		= new Array();
		<c:out value='${requestScope.reqHtmlPrivilegeData}' escapeXml='false' />
		
		var arrMenu 		= new Array();
		<c:out value='${requestScope.menuList}' escapeXml='false' />;
		var carrier = "<c:out value='${requestScope.reqCarrName}' escapeXml='false' />";
		document.title = ": : "+carrier;
		
		var gConfig=new Array();
		<c:out value='${requestScope.reqGlobalConfig}' escapeXml='false' />
		isPWDReseted = "<c:out value='${requestScope.userPwdReseted}' escapeXml='false' />";

		function doUnload(){			
			makePOST("../../public/master/logout.action","");
			
		}
	
	</script>		
	<script language="javascript" src="../../js/common/menu-path.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script language="javascript" src="../../js/common/menuG5LoaderFSX.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>	
  </head>
	<frameset rows="27, 648,*" border="0" frameborder="0" onUnload="doUnload()">
		<frame src="../../private/master/showTop" border="0" NoResize frameborder="0" scrolling="no"></frame>
		<frame src="../../private/master/showMain" name="main"  NoResize scrolling="no"></frame>
		<frame src="../../private/master/showBottom" name="bottom" NoResize scrolling="no"></frame>	
	</frameset>
</html>