<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
   	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<script language="javascript" src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>	   	
    <script language="javascript" src="../../js/common/menuG5FX.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
    <script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  language="javascript" ></script>	
    <script language="javascript">
    	<!--
    		var intLastRec = 1;  
    		var strSearchCriteria = "";

			var strFlightSearchCriteria = ""; // for Inventory Allocation
			var strInventoryFlightDataRow = ""; // for Inventory Allocation 
			var strInventoryFlightData = ""; // for Inventory Allocation

    		var arrayData=""; // variable agents data for fares
    		var status=""; // status of a fares
    		var ruleStatus=""; // status of a fare rule
    		var objWindow;
			var tempSearch="";
			var airportDstVal;

			//Note: variables defined for Reprotect functionality. Please do not delete.
			var arrReprotectData;//to hold the reprotect data array
			var strReprotectFrom;//to identify in which page the reprotect button was clicked
			var objCancelWindow; //variable to hold the reference of cancel window
			var objReprotectWindow; //variable to hold the reference of reprotect window
			var objRollForwardWindow; //variable to hold the reference of roll forward window
			var objSeatRoll; //variable to hold the reference of roll forward window
			var objMealRoll; //variable to hold the reference of Meal roll forward window
			
			var objBaggageRoll; //variable to hold the reference of Baggage roll forward window
			
			var TAMode;
			// To hold travel agent mode
    		
    		// Variabl initialization onload of the every page 
    		// from the menu link 
    		function initializeVar(){
	    		intLastRec = 1 ;
	    		strSearchCriteria = "";
				strFlightSearchCriteria = "";
				strInventoryFlightDataRow = "";
				strInventoryFlightData = "";
				airportDstVal = "";
				arrReprotectData = "";
				strSearchCriteria = "";
				strFlightSearchCriteria = "";
    			arrayData=""; // variable agents data for fares
	    		status=""; // status of a fares
    			ruleStatus=""; // status of a fare rule
				tempSearch="";
				airportDstVal = "";
				strReprotectFrom = "";	
				TAMode = "";			
			}
			
			//Function to store the Current Date
			function getClientDate(){
				var dtC = new Date();
				var dtCM = dtC.getMonth() + 1;
				var dtCD = dtC.getDate();
				if (dtCM < 10){dtCM = "0" + dtCM}
				if (dtCD < 10){dtCD = "0" + dtCD}
			 
				var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear();
				return strSysDate;
			}
			
			function getServerTime() {			
 
			var intYY = 2005;
			var intDD = 12;
			var intMM = 01;
			var intHH = 10 ;
			var intmm = 01 ;
			var intSS = 00;
			var mydate=new Date(intYY, intMM, intDD, intHH, intmm, intSS)

			function getthedate(){

 			var year=mydate.getYear()
			 if (year < 1000)
				  year+=1900
				  var day=mydate.getDay()
				  var month=mydate.getMonth()
				  var daym=mydate.getDate()
  
			 if (daym<10)
				  daym="0"+daym
				  var hours=mydate.getHours()
				  var minutes=mydate.getMinutes()
				  var seconds=mydate.getSeconds()
				 
			
			 if (hours==0)
				  hours=12
			 if (minutes<=9)
				  minutes="0"+minutes
			 if (seconds<=9)
				  seconds="0"+seconds
		 //change font size here

			 mydate=new Date(intYY, intMM, intDD, intHH, intmm, intSS)
			 intSS++
			 if (intSS >= 60)	 {
				 intSS = 0;
				 intmm++;
			 }

			 if (intmm >= 60) {
				 intmm = 0;
				 intHH++;
			 }
			 
			 if (intHH >= 24) {
			 	intHH++
			 	intDD++
			}
				goforit(); 
			}
		}
			function goforit(){			
				  setInterval("getthedate()",1000)
			}
    	//-->
    </script>
  </head>
	<body onload="initMenu('AAMenu','top'); setSubFrame('AAMenu',parent.main);" onkeypress='return Body_onKeyPress(event)' onmousewheel="return false;" onkeydown='return Body_onKeyDown(event)' scroll="no" oncontextmenu="return true"  ondrag='return false'>
		<table width="999" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround" ID="Table1">
			<tr>
				<td width="13" class="BannerBorderLeft"><img src="../../images/spacer_no_cache.gif" width="13" height="1"></td>
				<td height="27" width="974" align="right" valign="top" style="background-image:url(../../images/AA126_no_cache.jpg)">
					<table border="0" cellpadding="0" cellspacing="0" align="right">
						<tr>
							<td width="35" align="center">
								<span id="spnErrorLog" style="display:none;cursor: pointer;" title="View Error Messages">
									<img id="imgMM1" border="0" src="../../images/error_icon_no_cache.gif" style="margin-top: 4px">
								</span>
							</td>
							<td valign="top" align="left">
								<a href="javascript:void(0)" onmouseover="javascript:top[1].ShowQuickLaunch();" title="Show Quick Launch"><img id="imgQL" border="0" src="../../images/AA160_no_cache.gif"></a>
							</td>
							<td valign="top" align="left">
								<a href="javascript:void(0)" onclick="window.blur()" title="Minimize Window"><img id="imgMM" border="0" src="../../images/AA180_no_cache.gif"></a>
							</td>
							<td style='width:10px;'>
							</td>
						</tr>
					</table>
				</td>
				<td width="12" class="BannerBorderRight"><img src="../../images/spacer_no_cache.gif" width="12" height="1"></td>
			</tr>
		</table>
		<script type="text/javascript">
		<!--
			if (top.strQuickLaunch == ""){
				setVisible("imgQL", false);
			}
			
			function CloseApplication(){
				getFieldByID("frmUnload").submit();
			}
		//-->
		</script>
<form id="frmUnload" method="post" action="../../public/master/logout.action" target="_parent">
</form>
	</body>
</html>