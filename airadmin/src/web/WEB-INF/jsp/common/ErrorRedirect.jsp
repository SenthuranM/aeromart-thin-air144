<%@ page language="java" isErrorPage="true"%>
<%@ include file="../common/Directives.jsp" %>

<%

String errMsgDef = "";

if(exception != null){
	errMsgDef = exception.getMessage(); 
}

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title></title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
  </head>
	<body scroll="no"  class="PageBorder" onkeydownx='return Body_onKeyDown(event)' ondragx='return false' oncontextmenux="return showContextMenu()">
		<span style="color:red"><c:out value='${requestScope.reqServerErrors}' escapeXml='false'/></span>
							<br><br><br><br>
							<table width="80%" border="0" cellpadding="2" cellspacing="5" align="center">
								<tr>
									<td align="center">
										<img src="../../images/AA173_no_cache.jpg">
									</td>
								</tr>
								<tr>
									<td align="center">
										<font class="mandatory">
											System error has occurred.
										</font>
									</td>
								</tr>
								<tr>
									<td align="center">
										<font class="mandatory">
											Please contact the Administrator. Sorry for the inconvenience.
										</font>
									</td>
								</tr>
								<tr>
									<td colspan="2" align="center">
										<font class="mandatory fntBold"><span id="spnErrorMsg"></span></font>
									</td>
								</tr>
							</table>
			<!--
			<table width="999" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround" ID="Table1">
			<tr>
				<td width="13" class="BannerBorderLeft"><img src="../../images/spacer_no_cache.gif" width="13" height="1"></td>
				<td width="974" valign="top">
					<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBorder" ID="Table7">
						<tr>
							<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
							<td width="914" valign="top" align="center" class="PageBackGround" style="height:628">
								<br>
							</td>
							<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
						</tr>
					</table>
				</td>
				<td width="12" class="BannerBorderRight"><img src="../../images/spacer_no_cache.gif" width="12" height="1"></td>
			</tr>
			<tr>
				<td width="13" class="BannerBorderLeft"><img src="../../images/spacer_no_cache.gif" width="13" height="1"></td>
				<td width="974" height="24" align="left" valign="bottom"><img src="../../images/bottom6_no_cache.jpg"></td>
				<td width="12" class="BannerBorderRight"><img src="../../images/spacer_no_cache.gif" width="12" height="1"></td>
			</tr>
			<tr>
				<td width="13" class="BannerBorderLeft"><img src="../../images/spacer_no_cache.gif" width="13" height="1"></td>
				<td height="20" align="left" valign="top">
					<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBottomColor" style="height:100%;" ID="Table5">
						<tr>
							<td width="30" class="PageBottomShadow"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
							<td width="914" align="left" height="25" valign="middle">
								<table width="100%" border="0" cellpadding="0" cellspacing="1" ID="Table6">
									<tr>
										<td width="20" valign="bottom"><a href="javascript:void(0);" onclick="ShowPageMessage()" title="View Message(s)"><img id="imgError" name="imgError" src="../../images/spacer_no_cache.gif" border="0"></a></td>
										<td width="1"><img src="../../images/StatusBar_no_cache.jpg"></td>
										<td class="StatsBarDef" id="tdSTBar"><span id="spnDate">&nbsp;</span></td>
										<td width="1"><img src="../../images/StatusBar_no_cache.jpg"></td>
										<td width="265" align="right">
											<font class="fontPGInfo">This site best view in IE 5+ and 1024 x 768 resolution.</font>
										</td>
									</tr>
								</table>
							</td>
							<td width="30" class="PageBottomShadow"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
						</tr>
					</table>
				</td>
				<td width="12" class="BannerBorderRight"><img src="../../images/spacer_no_cache.gif" width="12" height="1"></td>
			</tr>
		</table>
		-->
	</span>
   <script language="javascript">

   <!--
   var errFrame="<c:out value='${requestScope.reqErrorFrame}' escapeXml='false'/>";
   var errMsg="<c:out value='${requestScope.reqServerErrors}' escapeXml='false'/>";
   var errRedirect="<c:out value='${requestScope.reqErrorRedirect}' escapeXml='false'/>";
   var errMsgDef="<%=errMsgDef%>";

	if(errMsgDef!=""){
		errMsg=errMsgDef;
		errFrame="top";
	}
   if (errFrame == ""){
   	  	try {
			top.showServerErrors(errMsg,errRedirect);
		}catch (ex) {
			//setVisible("spnErrorView",true);
			DivWrite("spnErrorMsg","<font class='Mandatory fntMedium'>"+errMsg+"<\/font>");
		}
   }else{
		//setVisible("spnErrorView",true);
		DivWrite("spnErrorMsg","<font class='Mandatory fntMedium'>"+errMsg+"<\/font>");
	}

	try{
		top[2].HideProgress(); 
	}catch(e){
		//alert(e)
	}
   //-->


   <!--
    //if ("<c:out value='${requestScope.reqErrorFrame}' escapeXml='false'/>" == ""){
	//   	top.showServerErrors("<c:out value='${requestScope.reqServerErrors}' escapeXml='false'/>",
	//					     "<c:out value='${requestScope.reqErrorRedirect}' escapeXml='false'/>")
	//}else{
	//	setVisible("spnErrorView", true);
	//	DivWrite("spnErrorMsg", "<font class='Mandatory'>" + "<c:out value='${requestScope.reqServerErrors}' escapeXml='false'/>" + "<\/font>")
	//}
   //-->
   </script>	
	</body>
</html>