 <%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
 <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse  border-style: inset;" width="100%"><tr><td>
	<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse">
	
	  <!-- tr>
	  <td ><font>&nbsp;</font></td>
	  	<td width="8%" align="center" title="Fare Applicability"><font>&nbsp;App</font></td>
	    <td width="8%" align="center" title="Refundable"><font>&nbsp;Ref&nbsp;</font></td>													    
	    <td width="8%" align="center" title="Modification Charge Applicability"><font>&nbsp;Mod</font></td>
	    <td width="8%" align="center" title="Cancellation Charge Applicability"><font>&nbsp;CNX</font></td>
	    <td width="8%" align="center" title="Cancellation Charge Applicability"><font>&nbsp;Type</font></td>
	    <td width="15%" align="center" title="No Show Charge Amount"><font>&nbsp;NOSHOW</font></td>
	    <td width="15%" align="center" title="No Show Charge Amount in Local"><font>&nbsp;Local</font></td>
	    <td width="15%" align="center" title="No Show Charge Break Point"><div id="breakPointDiv"><font>&nbsp;Breakpoint</font></div></td>
	    <td width="15%" align="center" title="No Show Charge Boundary"><div id="boundaryDiv"><font>&nbsp;Boundary</font></div></td>
	  </tr -->
	
	  <tr>
	  <td ><font>&nbsp;</font></td>
	  	<td width="5%" align="center" title="Fare Applicability"><font>&nbsp;App</font></td>
	    <td width="5%" align="center" title="Refundable"><font>&nbsp;Ref&nbsp;</font></td>													    
	    <td width="5%" align="center" title="Modification Charge Applicability"><font>&nbsp;Mod</font></td>
	    <td width="5%" align="center" title="Cancellation Charge Applicability"><font>&nbsp;CNX</font></td>
	    <td width="8%" align="center" title="Cancellation Charge Applicability"><font>&nbsp;Type</font></td>
	    <td width="15%" align="center" title="No Show Charge Amount"><font>&nbsp;NOSHOW</font></td>
	    <c:if test="${requestScope.isDisplaySelectedCurrencyOption == true }">
	    	<td width="15%" align="center" title="No Show Charge Amount in Local"><font>&nbsp;Sel. Curr</font></td>
	    </c:if>
	    <td width="15%" align="center" title="No Show Charge Break Point"><div id="breakPointDiv"><font>&nbsp;Breakpoint</font></div></td>
	    <td width="15%" align="center" title="No Show Charge Boundary"><div id="boundaryDiv"><font>&nbsp;Boundary</font></div></td>
	  </tr>
	  <tr>
	    <td align="center"><font>&nbsp;<span id="tag1"></span></font></td>
	    <td align="center"><input type="checkbox" id="chkADApp" name="chkADApp" onClick="validateAdultDependancy('chkADApp','chkINApp');"></td>
	    <td align="center"><input type="checkbox" id="chkADRef" name="chkADRef" onClick="validateAdultDependancy('chkADRef','chkINRef');"></td>
	    <td align="center"><input type="checkbox" id="chkADMod" name="chkADMod" onClick="validateAdultDependancy('chkADMod','chkINMod');"></td>
	    <td align="center"><input type="checkbox" id="chkADCNX" name="chkADCNX" onClick="validateAdultDependancy('chkADCNX','chkINCNX');"></td>
        <td align="center"><select id="selADCGTYP"  name="selADCGTYP" onchange="setDisplayBoundaryAndBreakPoints(this);"><option value="V">V</option><option value="PF">PF</option><option value="PFS">PFS</option><option value="PTF">PTF</option></select></td>
	    <td align="center"><input type="text" name="txtADNoShoCharge"     id="txtADNoShoCharge" size="5" onKeyUp="KPValidateDecimel(this,10,2)" onKeyPress="KPValidateDecimel(this,10,2)"  class="rightText" onChange="required_changed()"></td>
	   	<c:if test="${requestScope.isDisplaySelectedCurrencyOption == true }">
	   	 <td align="center"><input type="text" name="txtADNoShoChargeInLocal"   id="txtADNoShoChargeInLocal" size="5" onKeyUp="KPValidateDecimel(this,10,2)" onKeyPress="KPValidateDecimel(this,10,2)"  class="rightText" onChange="required_changed()"></td>
	    </c:if>
	    <td align="center"><input type="text" name="txtADNoShoBreakPoint" id="txtADNoShoBreakPoint" size="5" onKeyUp="KPValidateDecimel(this,10,2)" onKeyPress="KPValidateDecimel(this,10,2)"  class="rightText" onChange="required_changed()"></td>
	    <td align="center"><input type="text" name="txtADNoShoBoundary"   id="txtADNoShoBoundary" size="5" onKeyUp="KPValidateDecimel(this,10,2)" onKeyPress="KPValidateDecimel(this,10,2)"  class="rightText" onChange="required_changed()"></td>
	  </tr>
	  <tr>
	    <td align="center"><font>&nbsp;<span id="tag2"></span></font></td>
	    <td align="center"><input type="checkbox" id="chkCHApp" name="chkCHApp" onclick="validateChild('chkCHApp')"></td>
	    <td align="center"><input type="checkbox" id="chkCHRef" name="chkCHRef"></td>
		<td align="center"><input type="checkbox" id="chkCHMod" name="chkCHMod"></td>
	    <td align="center"><input type="checkbox" id="chkCHCNX" name="chkCHCNX"></td>
		<td align="center"><select id="selCHCGTYP"  name="selCHCGTYP" onchange="setDisplayBoundaryAndBreakPoints(this);"><option value="V">V</option><option value="PF">PF</option><option value="PFS">PFS</option><option value="PTF">PTF</option></select></td>
        <td align="center"><input type="text" name="txtCHNoShoCharge"     id="txtCHNoShoCharge" size="5" onKeyUp="KPValidateDecimel(this,10,2)" onKeyPress="KPValidateDecimel(this,10,2)"  class="rightText" onChange="required_changed()"></td>
   		<c:if test="${requestScope.isDisplaySelectedCurrencyOption == true }">
        	<td align="center"><input type="text" name="txtCHNoShoChargeInLocal"     id="txtCHNoShoChargeInLocal" size="5" onKeyUp="KPValidateDecimel(this,10,2)" onKeyPress="KPValidateDecimel(this,10,2)"  class="rightText" onChange="required_changed()"></td>
		</c:if>
		<td align="center"><input type="text" name="txtCHNoShoBreakPoint" id="txtCHNoShoBreakPoint" size="5" onKeyUp="KPValidateDecimel(this,10,2)" onKeyPress="KPValidateDecimel(this,10,2)"  class="rightText" onChange="required_changed()"></td>
        <td align="center"><input type="text" name="txtCHNoShoBoundary"   id="txtCHNoShoBoundary" size="5" onKeyUp="KPValidateDecimel(this,10,2)" onKeyPress="KPValidateDecimel(this,10,2)"  class="rightText" onChange="required_changed()"></td>
	  </tr>
	  <tr>
	    <td align="center"><font>&nbsp;<span id="tag3"></span></font></td>
	    <td align="center"><input type="checkbox" id="chkINApp" name="chkINApp" onClick="validateInfant('chkINApp','chkADApp')"></td>
	    <td align="center"><input type="checkbox" id="chkINRef" name="chkINRef" onClick="validateInfant('chkINRef','chkADRef')"></td>
	    <td align="center"><input type="checkbox" id="chkINMod" name="chkINMod" onClick="validateInfant('chkINMod','chkADMod')"></td>
	    <td align="center"><input type="checkbox" id="chkINCNX" name="chkINCNX" onClick="validateInfant('chkINCNX','chkADCNX')"></td>
		<td align="center"><select id="selINCGTYP"  name="selINCGTYP" s onchange="setDisplayBoundaryAndBreakPoints(this);"><option value="V">V</option><option value="PF">PF</option><option value="PFS">PFS</option><option value="PTF">PTF</option></select></td>
		<td align="center"><input type="text" name="txtINNoShoCharge"     id="txtINNoShoCharge" size="5" onKeyUp="KPValidateDecimel(this,10,2)" onKeyPress="KPValidateDecimel(this,10,2)"  class="rightText" onChange="required_changed()"></td>
		<c:if test="${requestScope.isDisplaySelectedCurrencyOption == true }">
			<td align="center"><input type="text" name="txtINNoShoChargeInLocal"     id="txtINNoShoChargeInLocal" size="5" onKeyUp="KPValidateDecimel(this,10,2)" onKeyPress="KPValidateDecimel(this,10,2)"  class="rightText" onChange="required_changed()"></td>
        </c:if>
        <td align="center"><input type="text" name="txtINNoShoBreakPoint" id="txtINNoShoBreakPoint" size="5" onKeyUp="KPValidateDecimel(this,10,2)" onKeyPress="KPValidateDecimel(this,10,2)"  class="rightText" onChange="required_changed()"></td>
        <td align="center"><input type="text" name="txtINNoShoBoundary"   id="txtINNoShoBoundary" size="5" onKeyUp="KPValidateDecimel(this,10,2)" onKeyPress="KPValidateDecimel(this,10,2)"  class="rightText" onChange="required_changed()"></td>
	  </tr>
	</table></td>
    </tr>
</table>