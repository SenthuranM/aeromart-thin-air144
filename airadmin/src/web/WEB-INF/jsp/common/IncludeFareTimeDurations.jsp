 	<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse  border-style: inset;" width="100%">
		<tr>
			<td width="100%">
				<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse">
					<tr>
						<td width="115" align="left" height="16"><font >&nbsp;</font></td>
						<td width="50" align="left" ><font>Months</font></td>
						<td width="50" align="left" ><font>Days</font></td>
						<td width="50" align="left" ><font>Hours</font></td>
						<td width="50" align="left" ><font>Minutes</font></td>					
					</tr>
					<tr>
						<td height="18" align="left"><font>Min Stay Over</font></td>
						<td align="left" ><input type="text"
							id="txtMinStayMonths" name="txtMinStayMonths" size="4" maxlength="2" onKeyUp="KPValidatePositiveInteger('txtMinStayMonths','MON')"
							onKeyPress="KPValidatePositiveInteger('txtMinStayMonths','MON')" class="rightText" onChange="required_changed()">
							</td>
						<td align="left" ><input type="text"
							id="txtMinStayDays" name="txtMinStayDays" size="4" maxlength="2" onKeyUp="KPValidatePositiveInteger('txtMinStayDays','DAY')"
							onKeyPress="KPValidatePositiveInteger('txtMinStayDays','DAY')" class="rightText" onChange="required_changed()">
							</td>
						<td align="left" ><input type="text"
							id="txtMinStayHours" name="txtMinStayHours" size="4" maxlength="2" onKeyUp="KPValidatePositiveInteger('txtMinStayHours','HRS')"
							onKeyPress="KPValidatePositiveInteger('txtMinStayHours','HRS')" class="rightText" onChange="required_changed()">
							</td>
						<td align="left" ><input type="text" id="txtMinStayMins" name="txtMinStayMins" size="4" maxlength="2"
							onKeyUp="KPValidatePositiveInteger('txtMinStayMins','MIN')"onKeyPress="KPValidatePositiveInteger('txtMinStayMins','MIN')"
							class="rightText">
							</td>		
					</tr>
					<tr>
						<td height="18" align="left"><font>Max Stay Over</font></td>
						<td align="left"><input type="text" id="txtMaxStayMonths" name="txtMaxStayMonths" size="4" maxlength="2"
							onKeyUp="KPValidatePositiveInteger('txtMaxStayMonths','MON')" onKeyPress="KPValidatePositiveInteger('txtMaxStayMonths','MON')"
							class="rightText" onchange="valueChanged()">
							</td>
						
						<td align="left"><input type="text" id="txtMaxStayDays" name="txtMaxStayDays" size="4" maxlength="2"
							onKeyUp="KPValidatePositiveInteger('txtMaxStayDays','DAY')" onKeyPress="KPValidatePositiveInteger('txtMaxStayDays','DAY')"
							class="rightText" onchange="valueChanged()">
							</td>

						<td align="left"><input type="text"
							id="txtMaxStayHours" name="txtMaxStayHours" size="4" maxlength="2" onKeyUp="KPValidatePositiveInteger('txtMaxStayHours','HRS')"
							onKeyPress="KPValidatePositiveInteger('txtMaxStayHours','HRS')" class="rightText" onchange="valueChanged()">
							</td>
						<td align="left"><input type="text" id="txtMaxStayMins" name="txtMaxStayMins" size="4" maxlength="2"
							onKeyUp="KPValidatePositiveInteger('txtMaxStayMins','MIN')" onKeyPress="KPValidatePositiveInteger('txtMaxStayMins','MIN')"
							class="rightText" onchange="valueChanged()">
							</td>
					</tr>
					<tr>
						<td height="18" align="left" id="tdLblOpenRTConf"><font>Open Ret. Confirm</font></td>		
						<td id="tdOpenRTConfMnth"><input type="text" id="txtConfirmMonths" name="txtConfirmMonths" size="4" maxlength="2" 
							onKeyUp="KPValidatePositiveInteger('txtConfirmMonths','MON')" onKeyPress="KPValidatePositiveInteger('txtConfirmMonths','MON')" 
							class="rightText" onChange="required_changed()">
							</td>
										
						<td id="tdOpenRTConfDays"><input type="text" id="txtConfirmDays" name="txtConfirmDays" size="4" maxlength="2" 
							onKeyUp="KPValidatePositiveInteger('txtConfirmDays','DAY')" onKeyPress="KPValidatePositiveInteger('txtConfirmDays','DAY')"
							class="rightText" onChange="required_changed()">
							</td>
												
						<td id="tdOpenRTConfHrs"><input type="text" id="txtConfirmHours" name="txtConfirmHours" size="4" maxlength="2" 
							onKeyUp="KPValidatePositiveInteger('txtConfirmHours','HRS')" onKeyPress="KPValidatePositiveInteger('txtConfirmHours','HRS')"
							class="rightText" onChange="required_changed()"></td>
						
					
						<td id="tdOpenRTConfMins"><input type="text" id="txtConfirmMins" name="txtConfirmMins" size="4" maxlength="2" 
							onKeyUp="KPValidatePositiveInteger('txtConfirmMins','MIN')" onKeyPress="KPValidatePositiveInteger('txtConfirmMins','MIN')"
							class="rightText" onChange="required_changed()"></td>
				
								
					</tr>
					<tr id="trModBuffTimeInternational">
						<td height="18" align="left"><font> Mod Buffer Time <span id="spanModBuffTimeInternationl">International</span>&nbsp; </font></td>
						<td><font>&nbsp;</font></td>
						<td><input type="text" id="txtModBufDays" name="txtModBufDays" size="4" maxlength="4" class="rightText"
							onChange="required_changed()" onkeyup="KPValidatePositiveInteger('txtModBufDays')"
							onkeypress="KPValidatePositiveInteger('txtModBufDays')">
							</td>
						<td><input type="text" id="txtModBufHours"  name="txtModBufHours" size="4" maxlength="2" class="rightText"onchange="required_changed()"
							onkeyup="KPValidatePositiveInteger('txtModBufHours','HRS')" onkeypress="KPValidatePositiveInteger('txtModBufHours','HRS')">
							</td>
						<td><input type="text" id="txtModBufMins" name="txtModBufMins" size="4" maxlength="2" class="rightText" onchange="required_changed()"
							onkeyup="KPValidatePositiveInteger('txtModBufMins','MIN')" onkeypress="KPValidatePositiveInteger('txtModBufMins','MIN')">
							</td>
					</tr>
					<tr id="trModBuffTimeDomestic">
						<td height="18" align="left"><font> Mod Buffer Time <span id="spanModBuffTimeDomestic">Domestic</span>&nbsp; </font></td>
						<td><font>&nbsp;</font></td>
						<td><input type="text" id="txtModBufDaysDom" name="txtModBufDaysDom" size="4" maxlength="4" class="rightText"
							onChange="required_changed()" onkeyup="KPValidatePositiveInteger('txtModBufDaysDom','DAY')"
							onkeypress="KPValidatePositiveInteger('txtModBufDaysDom','DAY')">
							</td>
						<td><input type="text" id="txtModBufHoursDom"  name="txtModBufHoursDom" size="4" maxlength="2" class="rightText"onchange="required_changed()"
							onkeyup="KPValidatePositiveInteger('txtModBufHoursDom','HRS')" onkeypress="KPValidatePositiveInteger('txtModBufHoursDom','HRS')">
							</td>
						<td><input type="text" id="txtModBufMinsDom" name="txtModBufMinsDom" size="4" maxlength="2" class="rightText" onchange="required_changed()"
							onkeyup="KPValidatePositiveInteger('txtModBufMinsDom','MIN')" onkeypress="KPValidatePositiveInteger('txtModBufMinsDom','MIN')">
							</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>