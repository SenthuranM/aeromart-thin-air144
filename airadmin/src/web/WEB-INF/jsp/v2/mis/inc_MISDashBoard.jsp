								<table width='100%' border='0' cellpadding='0' cellspacing='0'>
									<tr>
										<td valign='top' width='50%'>
											<table width='100%' border='0' cellpadding='0' cellspacing='2'>
												<tr>
													<td>
														<%@ include file="inc_paneTop.jsp" %>
															<table width='100%' border='0' cellpadding='1' cellspacing='1'>
																<tr>
																	<td align='center'>
																		<font class='fntMedium'><b>Bookings for the period</b></font>
																	</td>
																</tr>
																<tr>
																	<td align='center'>
																		<img style='display:none;' src='../../images/M022_no_cache.jpg' alt=''>
																	</td>
																</tr>
																<tr>
																	<td align='center'>
																		<table style='width:330px;' border='0' cellpadding='0' cellspacing='0'>
																			<tr>
																				<td style='height:20px;'>
																				</td>
																			</tr>
																			<tr>
																				<td class='thinBorder' style='height:23px;background-image:url(../../images/M054_no_cache.jpg);background-repeat:repeat-x;' valign='top'>
																					<div id='div='divActBar' style='position:absolute;z-index:3;'>
																						<table border='0' cellpadding='0' cellspacing='0'>
																							<tr>
																								<td id='tdYTDAct' style='width:0px;height:22px;background-image:url(../../images/M055_no_cache.jpg);background-repeat:repeat-x;border-right: solid 1px black;z-index:11'>
																									<font style='font-size:xx-small'>&nbsp;</font>
																								</td>
																							</tr>
																						</table>
																					</div>
																					<font>&nbsp;</font>
																				</td>
																			</tr>
																			<tr>
																				<td style='height:5px;'>
																					<div id='divActualValue' style='position:absolute;top:172px;width:245px;z-index:4;'></div>
																				</td>
																			</tr>
																			<tr>
																				<td><img src='../../images/M053_no_cache.jpg' alt=''></td>
																			</tr>
																			<tr>
																				<td>
																					<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																						<tr>
																							<td width='30%'><font>0</font></td>
																							<td width='20%' align='right'><div id='divBudgM'><font>0</font></div></td>
																							<td width='50%' align='right'><div id='divBudgF'><font>0</font></div></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																			<tr>
																				<td style='height:10px;'>
																				</td>
																			</tr>
																			<tr>
																				<td align='center'>
																					<table border='0' cellpadding='2' cellspacing='2'>
																						<tr>
																							<td>
																								<img src='../../images/M056_no_cache.jpg' alt=''>
																							</td>
																							<td>
																								<font>No. of Passenger Segments</font>
																							</td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														<%@ include file="inc_paneBottom.jsp" %>
													</td>
												</tr>
												<tr>
												
													<td>
														<%@ include file="inc_paneTop.jsp" %>		
															<table width='100%' border='0' cellpadding='1' cellspacing='1'>
															<tr>
															
															<td><font><b><input type="radio"  name="displayOption" value="0"  checked="checked" onchange="changeDisplayOption()">Display By Value</b></font></td>
															<td><font><b><input type="radio"  name="displayOption" value="1" onchange="changeDisplayOption()">Display By %</b></font></td>
															</tr>
																<tr>
																	<td align='center' colspan="3">
																		<font><b>Sales by Channel</b></font>
																	</td>
																	<td>
																		<input type="image" name="btnEmailGraph" id="btnEmailGraph" src="../../images/M027_no_cache.jpg" title="E-mail" border="0" />
																	</td>
																	<td align='center'>
																		<img style='display:none;' src='../../images/M023_no_cache.jpg' alt='' border='0'>
																	</td>
																</tr>
																
																<tr>
																	<td style='height:110px;' colspan="3">
																		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																			<tr>
																				<td width='100%'>
																					<img id="imgDashSalesB" src="../../images/spacer_no_cache.gif" usemap ="#AAMap" border='0'>		
																				</td>
																				
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																<td align='center' colspan="3">
																	<font><b>Passenger Segment Count by Channel</b></font>
																</td>
																</tr>
																<tr>
																	<td style='height: 130px;'  colspan="3">
																				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																					<tr>
																					<td width='100%'><img id="imgDashSalesByPax"
																					src="../../images/spacer_no_cache.gif" border='0'>
																					</td>
																	
																				</tr>
																				</table>
																	</td>
																</tr>
															</table>
															
														<%@ include file="inc_paneBottom.jsp" %>
													</td>
													
												</tr>
												
											</table>
										</td>
										<td valign='top' width='50%'>
											<table width='100%' border='0' cellpadding='0' cellspacing='2'>
												<tr>
													<td>
														<%@ include file="inc_paneTop.jsp"%>
															<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																<tr>
																	<td align='center'>
																		<font class='fntMedium'><b>Sales Breakdown</b></font>
																	</td>
																</tr>
																<tr>
																	<td style='height:115px;' valign='top'>
																		<table width='90%' border='0' cellpadding='0' cellspcing='0' align='center'>
																			<tr>
																				<td width='2%' class='thinBorder' style='height:22px;background-image:url(../../images/M042_no_cache.jpg);background-repeat:repeat-x;'>
																					<font>&nbsp;</font>
																				</td>
																				<td width='3%'>
																					<font>&nbsp;</font>
																				</td>
																				<td width='45%'>
																					<font><b>Total Sales</b></font>
																				</td>
																				<td width='50%' class='thinBorder' align='right' style='background-color:white;'>
																					<div id='divRevenueD'></div>
																				</td>
																			</tr>
																			<tr>
																				<td colspan='4' style='height:2px;'>
																				</td>
																			</tr>
																			<tr>
																				<td class='thinBorder' style='height:22px;background-image:url(../../images/M043_no_cache.jpg);background-repeat:repeat-x;' >
																					<font>&nbsp;</font>
																				</td>
																				<td>
																					<font>&nbsp;</font>
																				</td>
																				<td>
																					<font><b>Total Fare</b></font>
																				</td>
																				<td class='thinBorder' align='right' style='background-color:white;'>
																					<div id='divFareD'></div>
																				</td>
																			</tr>
																			<tr>
																				<td colspan='4' style='height:2px;'>
																				</td>
																			</tr>
																			<tr>
																				<td width='2%' class='thinBorder' style='height:22px;background-image:url(../../images/M044_no_cache.jpg);background-repeat:repeat-x;'>
																					<font>&nbsp;</font>
																				</td>	
																				<td>
																					<font>&nbsp;</font>
																				</td>
																				<td>
																					<font><b>Total Taxes</b></font>
																				</td>
																				<td class='thinBorder' align='right' style='background-color:white;'>
																					<div id='divTaxD'></div>
																				</td>
																			</tr>
																			<tr>
																				<td colspan='4' style='height:2px;'>
																				</td>
																			</tr>
																			<tr>
																				<td class='thinBorder' style='height:22px;background-image:url(../../images/M045_no_cache.jpg);background-repeat:repeat-x;'>
																					<font>&nbsp;</font>
																				</td>	
																				<td>
																					<font>&nbsp;</font>
																				</td>
																				<td>
																					<font><b>Total Surcharges</b></font>
																				</td>
																				<td class='thinBorder' align='right' style='background-color:white;'>
																					<div id='divSurchargeD'></div>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td style='height:335px;' align='center'>
																		<img src='../../images/M046_no_cache.jpg' alt='' border='0' style='display:none'>
																		<div id="imgDashRevB"></div>
																	</td>
																</tr>
															</table>
														<%@ include file="inc_paneBottom.jsp" %>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
