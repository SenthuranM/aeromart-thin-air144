<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Reports</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="-1">    
    <%@ include file="inc_pgHD.jsp" %>
		
	<script src="../../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
	<script type='text/javascript' src='../../js/v2/mis/ua.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />'></script>  	
	<script type='text/javascript' src='../../js/v2/mis/ftiens4.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />'></script>  	
	<script type='text/javascript' src='../../js/v2/mis/nodes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />'></script>  	
	
	<style type='text/css'>
	SPAN.TreeviewSpanArea A {
		FONT-SIZE: 11px; COLOR: black; FONT-FAMILY: tahoma,helvetica; TEXT-DECORATION: none
	}
	SPAN.TreeviewSpanArea A:hover {
		COLOR: #820082
	}
	</style>
  </head>
  <body class="tabBGColor" scroll="no" onkeypress='return Body_onKeyPress(event)' onkeydown='return Body_onKeyDown(event)'>
	<table width='100%' border='0' cellpadding='0' cellspacing='2'>
  		<tr>
			<td width='20%'>
				<%@ include file="../../common/IncludeFormTopFrameLess.jsp"%>			
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td style='height:570px;' valign='top'>
								<table width='100%' border='0' cellpadding='2' cellspacing='1'>
									<tr>
										<td style='display:none;'>
											<table width='100%' border='0' cellpadding='2' cellspacing='1'>
												<tr>
													<td class='lgndBd cursorPointer' id='tdMis0' onclick='misReportsOnClick(0)' style='background-Color:#B0C0C0'><font id='fntClr0'>Dashboard</font></td>
												</tr>
												<tr>
													<td class='lgndBd cursorPointer' id='tdMis1' onclick='misReportsOnClick(1)' style='background-Color:#B0C0C0'><font id='fntClr1'>Distribution</font></td>
												</tr>
												<tr>
													<td class='lgndBd cursorPointer' id='tdMis2' onclick='misReportsOnClick(2)' style='background-Color:#B0C0C0'><font id='fntClr2'>Regional</font></td>
												</tr>
												<tr>
													<td class='lgndBd cursorPointer' id='tdMis3' onclick='misReportsOnClick(3)' style='background-Color:#B0C0C0'><font id='fntClr3'>Agents</font></td>
												</tr>
												<tr>
													<td class='lgndBd cursorPointer' id='tdMis4' onclick='misReportsOnClick(4)' style='background-Color:#B0C0C0'><font id='fntClr4'>Accounts</font></td>
												</tr>
												<tr>
													<td class='lgndBd cursorPointer' id='tdMis5' onclick='misReportsOnClick(5)' style='background-Color:#B0C0C0'><font id='fntClr5'>Misc</font></td>
												</tr>
											</table>												
										</td>
									</tr>
									<tr>
										<td style='height:460px;' valign='top'>
											<A href="http://www.treemenu.net/" style="visibility:hidden;"></A></FONT><span class='TreeviewSpanArea' style='position:absolute;height:460px;width:164px;overflow-X:auto; overflow-Y:auto;'>
											<script type='text/javascript'>initializeDocument();</script>
										</td>
									</tr>
									<tr>
										<td>
											<table width='167px;' border='0' cellpadding='0 cellspacing='0'>
												<tr>
													<td style='background-image:url(../../images/M011_no_cache.jpg);background-repreat:no-repeat;background-position:center;height:96px;' align='center'>
														<table width='85%' border='0' cellpadding='1' cellspacing='0'>
															<tr>
																<td>
																	<font><b>Quick Search</b></font>
																</td>
															</tr>
															<tr>
																<td style='height:5px;'>
																</td>
															</tr>
															<tr>
																<td>
																	<input type='textbox' id='txtQSrch' name='txtQSrch' style='width:135px'>
																</td>
															</tr>
															<tr>
																<td align='right'>
																	<input type='button' id='btnQSrch' name='btnQSrch' value='Go' style='width:auto;' class='button' disabled="disabled">
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				<%@ include file="../../common/IncludeFormBottom.jsp"%>
			</td>
			<td width='80%' valign='top' align='right'>
				<iframe id='frmReport' name='frmReport' src="about:blank" width="730px;" height="600px;" frameBorder='No' scrolling='no'></iframe>
			</td>
		</tr>
	</table>
	<script type='text/javascript' src='../../js/v2/common/commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />'></script>  	
	<script type='text/javascript' src='../../js/v2/mis/reports.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />'></script>
  </body>
</html>