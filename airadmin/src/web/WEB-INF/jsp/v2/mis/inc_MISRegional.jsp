
<table width='100%' border='0' cellpadding='0' cellspacing='2'>
	<tr>
		<td width='23%' valign='top'><%@ include file="inc_paneTop.jsp"%>
		<table width='100%' border='0' cellpadding='1' cellspacing='1'>
			<tr>
				<td align='center'><font class='fntMedium'><b>Yearly
				Sales</b></font></td>
			</tr>
			<tr>
				<td align='center' style='height: 260px'><img
					style='display: none;' src='../../images/M028_no_cache.jpg' alt=''
					border='0'> <div id="imgRegiYearlySales"/></td>
			</tr>
		</table>
		<%@ include file="inc_paneBottom.jsp"%></td>
		<td width='44%' valign='top'><%@ include file="inc_paneTop.jsp"%>
		<table width='100%' border='0' cellpadding='1' cellspacing='1'>
			<tr>
				<td align='center'><font class='fntMedium'><b>6
				Months Rolling Sales Trend</b></font></td>
			</tr>
			<tr>
				<td align='center'><font class='fntSmall'><b>
				(Passenger segment count for all Regions)</b></font></td>
			</tr>
			<tr>
				<td align='center' style='height: 245px'><img
					style='display: none;' src='../../images/M029_no_cache.jpg' alt=''
					border='0'><div id="imgRegi6MonthsRoll"></div></td>
			</tr>
			<tr>
				<td style='display: none;'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
						<td width='33%'>
						<table border='0' cellpadding='1' cellspacing='1'>
							<tr>
								<td style='background-color: #FE9900; width: 5px;'></td>
								<td><font>Middle East</font></td>
							</tr>
						</table>
						</td>
						<td width='23%'>
						<table border='0' cellpadding='1' cellspacing='1'>
							<tr>
								<td style='background-color: #0198FF; width: 5px;'></td>
								<td><font>Asian</font></td>
							</tr>
						</table>
						</td>
						<td width='44%'>
						<table border='0' cellpadding='1' cellspacing='1'>
							<tr>
								<td style='background-color: #FE0000; width: 5px;'></td>
								<td><font>Europe</font></td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		<%@ include file="inc_paneBottom.jsp"%></td>
		<td width='33%' valign='top'><%@ include file="inc_paneTop.jsp"%>
		<div id="divAgentPerformance"  style="overflow:auto;height:282px;">
		<table width='100%' border='0' cellpadding='1' cellspacing='1'>
			<tr>
				<td align='center'><font><b>Agents -&nbsp;</b></font> <select
					id='selTAgnts' name='selTAgnts' size='1' style='width: 105px;'>
					<option value='REGIONAL_BP'>Best Performance</option>
					<option value='REGIONAL_LP'>Non Performing</option>
				</select></td>
			</tr>
			<tr>
				<td align='center' style='height: 217px' valign='top'>
				<table id='tblReginalPerformance' width='100%' border='0'
					cellpadding='1' cellspacing='1' style='border:1px solid grey;'>
					<tbody></tbody>
					<tfoot>
						<tr>
							<td colspan='3' class='thinTop' style='height: 5px;'><font>&nbsp;</font>
							</td>
						</tr>
						<tr>
							<td colspan='2'><font><b>Average Agents Sales</b></font></td>
							<td align='right'>
							<div id='divAgntsSales'></div>
							</td>
						</tr>
					</tfoot>
				</table>
				</td>
			</tr>
		</table>
		</div>
		<%@ include file="inc_paneBottom.jsp"%></td>
	</tr>
	<tr>
		<td colspan='3'><%@ include file="inc_paneTop.jsp"%>
		<table width='100%' border='0' cellpadding='1' cellspacing='1'>
			<tr>
				<td align='center'>
				<table width='100%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
						<td width='40%'><input type="image" name="btnemail"
							id="btnemail" src="../../images/M027_no_cache.jpg" title="E-mail"
							border="0" /> <input type="image" name="btnExcel" id="btnExcel"
							src="../../images/M032_no_cache.jpg" title="Dowanload as Excel" border="0" />
						<input type="image" name="btnPrint" id="btnPrint"
							src="../../images/M033_no_cache.jpg" title="Print" border="0" /> <input
							type="image" name="btnGraph" id="btnGraph"
							src="../../images/M034_no_cache.jpg" border="0" title="View as Graph"
							border="0" /></td>
						<td width='60%'><font class='fntMedium'><b>Product
						Sales Analysis</b></font></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td align='center'>
				<div id="divProdSalesAnalysis" style="overflow: auto; height: 94px;border:1px solid grey;">
				<table id='tblRegDisAna' width='100%' border='0' cellpadding='1'
					cellspacing='1'>
					<tbody></tbody>
				</table>
				</div>

				</td>
			</tr>
			<tr><td><font class='fntSmall'>Note: Product Sales Data is the total sales value for each agent in <b>all regions</b> for the selected time period.</font></td></tr>
		</table>
		<%@ include file="inc_paneBottom.jsp"%></td>
	</tr>
</table>
<div class="printable" id="divPintSalesData">
<table id='tblProdSale' width='100%' class="sample">
	<tbody></tbody>
</table>
</div>
