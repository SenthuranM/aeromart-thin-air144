							<table width="100%" border="0" cellpadding="0" cellspacing="1">
								<tr>
									<td width='25%' valign='top'>
										<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td>
													<%@ include file="inc_paneTop.jsp"%>
														<table width="100%" border="0" cellpadding="0" cellspacing="0">
															<tr>
																<td style='height:221px;' valign='top'>
																	<div id='divCntry' name='divCntry' style='position:absolute;height:220px;width:160px;overflow-X:auto; overflow-Y:auto;'></div>
																</td>
															</tr>
														</table>
													<%@ include file="inc_paneBottom.jsp" %>
												</td>
											</tr>
											<tr>
												<td valign='top'>
													<%@ include file="inc_paneTop.jsp"%>	
														<table width="100%" border="0" cellpadding="0" cellspacing="0">
															<tr>
																<td valign='top'>
																	<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																		<tr>
																			<td>
																				<font><b>Comparative overview of all agents</b></font>
																			</td>
																		</tr>
																		<tr>
																			<td>	
																				<font>Filter :</font>
																				<select id='selAgentsFilter' name='selAgentsFilter' size='1' style='width:90px;' onchange='UI_tabMISAgents.createAgentsList();'>
																					<option value='AGENTS_A'>All</option>
																					<option value='AGENTS_T'>Top 10</option>
																					<option value='AGENTS_N'>Non Performing</option>
																				</select>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
															<tr>
																<td valign='top' style='height:173px;' >
																	<div id='divAgntsList' name='divAgntsList' style='position:absolute;height:170px;width:160px;overflow-X:hidden; overflow-Y:auto;'>
																		<table id='tblAgentsList' width='100%' border='0' cellpadding='1' cellspacing='1' style='border:1px solid grey;'>
																		</table>
																	</div>
																</td>
															</tr>
														</table>
													<%@ include file="inc_paneBottom.jsp" %>
												</td>
											</tr>
										</table>
									</td>
									<td width='75%' valign='top'>
										<div id='divBranch' name='divBranch' style='position:absolute;height:490px;width:545px;overflow-X:auto; overflow-Y:auto;'>
											<table width='100%' border='0' cellpadding='0' cellspacing='0'>
												<tr>
													<td>
														<%@ include file="inc_paneTop.jsp"%>	
															<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																<tr>	
																	<td style='height:202px;'>
																		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																			<tr>
																				<td width='44%' align='center'>
																					<div id="map_canvas" style="width: 240px; height: 200px; border: 1px solid black;"></div> 
																				</td>
																				<td width='3%' valign='top'>&nbsp;</td>
																				<td width='53%' valign='top'>
																					<div id='divAgentInfo'></div>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														<%@ include file="inc_paneBottom.jsp" %>
													</td>
												</tr>
												<tr>
													<td valign='top'>
														<table width='100%' border='0' cellpadding='0' cellspacing='0'>
															<tr>
																<td width='49%' valign='top'>
																	<%@ include file="inc_paneTop.jsp"%>	
																		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																			<tr>
																				<td align='center'>
																					<font><b>Sub Agents Sales Summary</b></font>
																				</td>
																			</tr>
																			<tr>
																				<td valign='top' style='height:140px;'>
																					<div id='divBranchSumm' name='divBranchSumm' style='position:absolute;height:140px;width:235px;overflow-X:auto; overflow-Y:auto;border-bottom:1px solid lightgrey;'>
																					<table id='tblBranchSumm' width='219px' border='0' cellpadding='1' cellspacing='1'>
																						<tbody></tbody>
																					</table>
																					</div>																																																																																																																																																																																																
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<div style='margin-top:5px;'><font style='fntSmall'>Note: Based on the given date range.</font></div>
																				</td>
																			</tr>
																		</table>
																	<%@ include file="inc_paneBottom.jsp" %>
																</td>
																<td width='51%' valign='top'>
																	<%@ include file="inc_paneTop.jsp"%>	
																		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																			<tr>
																				<td align='center'>
																					<font><b>Recent Payment History</b></font>
																				</td>
																			</tr>
																			<tr>
																				<td style='height:140px;' valign='top'>
																					<div id='divPayHistory' name='divPayHistory' style='position:absolute;height:140px;width:235px;overflow-X:auto; overflow-Y:auto;margin-top:0px;border-bottom:1px solid lightgrey;'>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<div style='margin-top:5px;'><font style='fntSmall'>Note: Based on the given date range.</font></div>
																				</td>
																			</tr>
																		</table>
																	<%@ include file="inc_paneBottom.jsp" %>
																</td>
															</tr>
															<tr>
																<td colspan='2'>
																	<%@ include file="inc_paneTopDark.jsp"%>	
																		<table border='0' cellpadding='0' cellspacing='0' align='right'>
																			<tr>
																				<td>
																					<font><b>Branches :&nbsp;</b></font>
																				</td>
																				<td>
																					<select id='selAgentBranch' name='selAgentBranch' size='1' style='width:240px;'>
																					</select>
																				</td>
																			</tr>
																		</table>
																	<%@ include file="inc_paneBottomDark.jsp" %>
																</td>
															</tr>
															<tr id="trRouteAndStaffWiseInfo" style="display:none">
																<td valign='top'>
																	<%@ include file="inc_paneTop.jsp"%>	
																		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																			<tr>
																				<td align='center'>
																					<font><b>Route wise Performance</b></font>
																				</td>
																			</tr>
																			<tr>
																				<td style='height:120px;' valign='top'>
																					<div id='divRoutePerf' name='divBranchSumm' style='position:absolute;height:120px;width:235px;overflow-X:auto; overflow-Y:auto;border-bottom:1px solid lightgrey;'>
																					<table id='tblRoutePerf' width='100%' border='0' cellpadding='1' cellspacing='1'>
																						<tbody></tbody>
																					</table>
																					<div>
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<div style='margin-top:5px;'><font style='fntSmall'>Note: Based on the given date range.</font></div>
																				</td>
																			</tr>
																		</table>
																	<%@ include file="inc_paneBottom.jsp" %>
																</td>
																<td valign='top'>
																	<%@ include file="inc_paneTop.jsp"%>	
																		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																			<tr>
																				<td align='center'>
																					<font><b>Staff wise Performance</b></font>
																				</td>
																			</tr>
																			<tr>
																				<td style='height:120px;' valign='top'>
																					<div id='divStaffPerf' name='divBranchSumm' style='position:absolute;height:120px;width:235px;overflow-X:auto; overflow-Y:auto;border-bottom:1px solid lightgrey;'>
																					<table id='tblStaffPerf' width='100%' border='0' cellpadding='1' cellspacing='1'>
																						<tbody></tbody>
																					</table>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<div style='margin-top:5px;'><font style='fntSmall'>Note: Based on the given date range.</font></div>
																				</td>
																			</tr>
																		</table>
																	<%@ include file="inc_paneBottom.jsp" %>
																</td>
															</tr>
															<tr id="trRouteAnalysis" style="display:none">
																<td colspan='2'>
																	<%@ include file="inc_paneTop.jsp"%>	
																		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																			<tr>
																				<td align='center'>
																					<font><b>Route wise Analysis</b></font>
																				</td>
																			</tr>
																			<tr>
																				<td align='center'>
																					<div id="imgAgentFareWise"></div>
																				</td>
																			</tr>																	
																		</table>
																	<%@ include file="inc_paneBottom.jsp" %>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
							</table>
