	<script type="text/javascript">
									   function rad1Checked() {										   
										   document.getElementById('divImgDistReveByChannelWise2').style.visibility='hidden';
										   document.getElementById('divImgDistReveByChannelWise2').style.display='none';
										   document.getElementById('divImgDistReveByChannelWise1').style.visibility='visible';
										   document.getElementById('divImgDistReveByChannelWise1').style.display='block';
									           refreshGraph("1");
									   }
									   
									   function rad2Checked() {										   
										   document.getElementById('divImgDistReveByChannelWise1').style.visibility='hidden';
										   document.getElementById('divImgDistReveByChannelWise1').style.display='none';
										   document.getElementById('divImgDistReveByChannelWise2').style.visibility='visible';
										   document.getElementById('divImgDistReveByChannelWise2').style.display='block';
										   refreshGraph("2");
									   }
									</script>								

<table width='100%' border='0' cellpadding='0' cellspacing='2'>
									<tr>
										<td>
											<%@ include file="inc_paneTop.jsp"%>
												<table width='100%' border='0' cellpadding='0' cellspacing='0'>
													<tr>
														<td valign='top' width='100%'>
															<div id="divBkgChnl">
															</div>
														</td>
													</tr>
													<tr>
														<td colspan="2">
														<table id='tblGraphLegend' width='100%' border='0' cellpadding='0' cellspacing='1'>
														</table>
														</td>
													</tr>
													
												</table>
											<%@ include file="inc_paneBottom.jsp" %>
										</td>
									</tr>
									
									<tr>
											<td colspan='2' class='thinTop'>
												<img style='display:none;' src='../../images/M025_no_cache.jpg' alt='' border='0'>
											</td>
									</tr>	
									<tr>
										<td>
											<%@ include file="inc_paneTop.jsp"%>
												<table width='100%' border='0' cellpadding='1' cellspacing='1'>
													<tr>
														<td align='center'>
															<font class='fntMedium'><b>Total Passenger Segment Count by Channel</b></font>
														</td>
													</tr>
													<tr>
														<td align='center'>
															<img style='display:none;' src='../../images/M024_no_cache.jpg' alt='' border='0'>
														</td>
													</tr>
													<tr>
														<td>
															<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																<tr>
																	<td width='95%' valign='top' align='left'>
																		<div id="imgDistReveByChannelWise"></div>		
																	</td>
																	
																	<td width='5%'><font>&nbsp;</font></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											<%@ include file="inc_paneBottom.jsp" %>														
										</td>
									</tr>
									
									
									<tr>
										<td>
											<%@ include file="inc_paneTop.jsp"%>
												<table width='100%' border='0' cellpadding='1' cellspacing='1'>
													<tr>
														<td align='center'>
															<font class='fntMedium'><b></b></font>
														</td>
													</tr>
													<tr>
														<td align='center'>
															<img style='display:none;' src='../../images/M024_no_cache.jpg' alt='' border='0'>
														</td>
													</tr>
													<tr>
														<td>
															<table width='100%' border='0' cellpadding='0' cellspacing='0'>
																<tr>
																	<td width='95%' valign='top' align='left'>
																		<div id="imgDistReveByChannelWiseRdo">View&nbsp;&nbsp;
																		  <input tabindex="1" type="radio" id="radOnline1" name="radOnline1" value="1" class="NoBorder" onClick="rad1Checked()" onChange="">
														  					<font>SF%</font>&nbsp;&nbsp;
														  					<input tabindex="2" type="radio" id="radOnline1" name="radOnline1" value="0" class="NoBorder" onClick="rad2Checked()" onChange="">
														  					<font>Yield </font>&nbsp;&nbsp;				  					
																		</div>		
																	</td>
																	
																	<td width='5%'><font>&nbsp;</font></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											<%@ include file="inc_paneBottom.jsp" %>														
										</td>
									</tr>
									
									<tr>									  
										<td id="divImgDistReveByChannelWise1"  style="visibility:hidden;">
											<%@ include file="inc_paneTop.jsp"%>
												<table width='100%' border='0' cellpadding='1' cellspacing='1'>
													<tr>
														<td align='center'>
															<font class='fntMedium'><b>SF% Analysis</b></font>
														</td>
													</tr>
													<tr>
														<td align='center'>
															<img style='display:none;' src='../../images/M024_no_cache.jpg' alt='' border='0'>
														</td>
													</tr>
													<tr>
														<td>
															<table width='90%' border='0' cellpadding='0' cellspacing='0'>
																<tr>

																    <td width='5%'><font>&nbsp;</font></td>
																	<td width='95%' valign='top' align='center'>
																		<div id="imgDistReveByChannelWise1"></div>		
																	</td>
																	
																	<td width='5%'><font>&nbsp;</font></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											<%@ include file="inc_paneBottom.jsp" %>														
										</td>									
									</tr>
									
									
									<tr>
										<td id="divImgDistReveByChannelWise2"  style="visibility:hidden;">
											<%@ include file="inc_paneTop.jsp"%>
												<table width='100%' border='0' cellpadding='1' cellspacing='1'>
													<tr>
														<td align='center'>
															<font class='fntMedium'><b>Yield Analysis</b></font>
														</td>
													</tr>
													<tr>
														<td align='center'>
															<img style='display:none;' src='../../images/M024_no_cache.jpg' alt='' border='0'>
														</td>
													</tr>
													<tr>
														<td>
															<table width='80%' border='0' cellpadding='0' cellspacing='0'>
																<tr>

																	<td width='5%'><font>&nbsp;</font></td>
																	<td width='95%' valign='top' align='center'>
																		<div id="imgDistReveByChannelWise2"></div>		
																	</td>
																	
																	<td width='5%'><font>&nbsp;</font></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											<%@ include file="inc_paneBottom.jsp" %>														
										</td>
									</tr>
									
									<tr>					
										<td id="tdPerformanceOfPortals">
											<%@ include file="inc_paneTop.jsp"%>
												<table width='100%' border='0' cellpadding='0' cellspacing='2'>
													<tr>
														<td align='center'>
															<font class='fntMedium'><b>Performance of Portals</b></font>
														</td>
													</tr>
													<tr>
														<td>
															<table id='tblDisPerformance' width='100%' border='0' cellpadding='1' cellspacing='1'>
																<tbody></tbody>
															</table>
														</td>
													</tr>
												</table>
											<%@ include file="inc_paneBottom.jsp" %>														
										</td>
									
									</tr>
								</table>