<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Product Sales Analysis</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="-1">    
    <%@ include file="inc_pgHD.jsp" %>
	<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">	
	<script src="../../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/ToolTip.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

	<script src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jQuery.print.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/v2/jquery/jquery.popupwindow.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="http://www.google.com/jsapi"></script>
 	<script type="text/javascript">
 		google.load('visualization', '1', {packages: ['barchart','piechart','columnchart','linechart']});

		
		$(document).ready(function(){
			var mydata = opener.UI_tabMISRegional.productSalesData;
			var data = new google.visualization.DataTable();
			data.addColumn('string', 'Station');
			data.addColumn('number', 'Sales');
			var i=0;
			for(region in  mydata){
				var objRegionalSales = mydata[region];
				for (city in objRegionalSales) {  
					var objCitySales = objRegionalSales[city];
					data.addRow();
					data.setValue(i, 0, city);
					data.setValue(i, 1,Number(objCitySales.actualSales));
					i++;
				} 
			}
			new google.visualization.ColumnChart(document.getElementById('divPopupGraph')).draw(data, 
					{ width:670, height:350 ,
				is3D:true,
				colors:[{color:'#669933', darker:'#55802b'}],
				legend: 'none', backgroundColor:'#f6f6f6'
					}                   
			);
		});
    	</script>
</head>
<body class="tabBGColor" scroll="no" onkeypress='return Body_onKeyPress(event)' onkeydown='return Body_onKeyDown(event)' style='background-color:#f6f6f6'>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr><td align="center"><div style="margin-top:2px;margin-bottom:2px;"><font><b>Product Sales Analysis</b></font></div></td></tr>
<tr><td align="center">
<div id="divPopupGraph"></div>
</td></tr>
</table>
</body>
</html>