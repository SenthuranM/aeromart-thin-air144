									<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
											<td width='100%'  colspan='2' valign='top'>
												<%@ include file="inc_paneTopDark.jsp" %>
													<table width='100%' border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td align='right'>
																<font><b>Display Ancillary Data : </b></font>
															</td>
															<td width='80px;' align='left'>
																<font>&nbsp;<select id='selAnciMode'>
																<option value="Sales" selected="selected">By Sales</option>
																<option value="Count" >By Count</option>
																</select></font>
															</td>
															
														</tr>
													</table>
												<%@ include file="inc_paneBottomDark.jsp" %>
											</td>
										</tr>
										<tr>
											<td width='50%' valign='top'>
												<%@ include file="inc_paneTop.jsp"%>
													<table width="100%" border="0" cellpadding="0" cellspacing="0">
														<tr>
															<td valign='top'>
																<font><b>Sales Information </b></font>
															</td>
														</tr>
														<tr>
															<td style='height:205px;' align='center'>
																<div id='imgAnciSales'></div>
															</td>
														</tr>
													</table>
												<%@ include file="inc_paneBottom.jsp" %>
											</td>
											<td width='50%' valign='top'>
												<%@ include file="inc_paneTop.jsp"%>
													<table width='100%' border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td colspan='2'>
																<font><b>KPI Viewer</b></font>
															</td>
														</tr>
														<tr>
															<td align='right'><font>Product Category :</font></td>
															<td width='25%' align='right'>
																<select id='selAnsiPrdCat' name='selAnsiPrdCat' size='1'>
																	
																</select>
															</td>
														</tr>
														<tr>
															<td colspan='2' valign='top' style='height:188px;'>
																<div id="divAnciByRoute" style="overflow:auto; height:185px;">
																	<table id='tblAnsiSeat' width='100%' border='0' cellpadding='1' cellspacing='1' style='border:1px solid grey;'>
																	<tbody></tbody>
																	</table>
																</div>
															</td>
														</tr>
													</table>
												<%@ include file="inc_paneBottom.jsp" %>
											</td>
										</tr>
										<tr>
											<td colspan='2'>
												<%@ include file="inc_paneTop.jsp"%>
													<table width='100%' border='0' cellpadding='0' cellspacing='0'>
														<tr>
															<td colspan='2'>
																<font><b>By Channel</b></font>
															</td>
														</tr>
														<tr>
															<td colspan='2' valign='top' style='height:160px;'>
																<table id='tblAnsiChannel' width='100%' border='0' cellpadding='1' cellspacing='1' style='border:1px solid grey;'>
																	<tbody></tbody>
																</table>
															</td>
														</tr>
													</table>
												<%@ include file="inc_paneBottom.jsp" %>
											</td>
										</tr>
									</table>
									
