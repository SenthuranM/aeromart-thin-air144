<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Passenger Blacklist</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
<LINK rel="stylesheet" type="text/css"
	href="../../css/Style_no_cache.css" />
<LINK rel="stylesheet" type="text/css"
	href="../../themes/default/css/ui.all_no_cache.css" />
<LINK rel="stylesheet" type="text/css"
	href="../../themes/default/css/jquery.ui.autocomplete.css" />


<script
	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.watermark.min.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<!-- <script type="text/javascript"
	src="../../js/v2/jquery/jquery.ui.dialog.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script> -->

<script
	src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/promotion/popupData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/v2/tools/BlacklistPAXManagement.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script type="text/javascript">
	var languageHTML = "<c:out value="${requestScope.reqLanguageList}" escapeXml="false" />";
</script>
<style>
.innerPanel {
	width: 98%;
	margin: 0 auto;
}

.portSel {
	width: 50px
}

select {
	margin: 1px 1px;
}

.iconBtn {
	width: 25px
}

#ui-datepicker-div {
	display: none;
}

.ui-autocomplete {
	height: 200px;
	overflow-y: scroll;
	overflow-x: hidden;
}

.input.uppercase {
	text-transform: uppercase;
}

.period {
	padding-top: 5px;
	text-decoration: underline;
}

#blacklistPaxDetails {
	padding-top: 10px;
	padding-bottom: 10px;
	margin-top: 5px;
	border: solid 1px #aaaaaa;
}
</style>
</head>
<body class="tabBGColor" scroll="no"
	onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false"
	onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
	<input type="hidden" name="hdnSelectID" id="hdnSelectID" value=0>
	<input type="hidden" name="hdnPromoCodeID" id="hdnPromoCodeID" value=0>
	<input type="hidden" name="hdnPromoCodeDesTrans" id="hdnPromoCodeDesTrans" value=0>
	<script type="text/javascript">
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	</script>

	<div id="divBlPaxSearch"
		style="text-align: left; background-color: #ECECEC;padding:5px;">
		<div class="innerPanel divsearch"
			style="">
			<table width="96%" border="0" cellpadding="0" cellspacing="2"
				ID="Table9">
				<!-- <tr>
					<td><font>First Name </font></td>
					<td><input name="txtFnameSearch" size="40" id="txtFnameSearch"
						style="width: 120px;"/></td>

					<td><font>Last Name</font></td>
					<td><input name="txtLnameSearch" size="40" id="txtLnameSearch"
						style="width: 120px;"/></td>

					<td><font>Passport No</font></td>
					<td><input name="txtPassportNo" size="40" id="txtPassportNo"
						style="width: 120px;"/></td>
					<td></td>
				</tr> -->
				<tr>
					<td><font>Passenger Name </font></td>
					<td colspan='3'><input name="txtFullnameSearch" size="40" id="txtFullnameSearch"
						style="width: 403px;"/></td>

					<td><font>Passport No</font></td>
					<td><input name="txtPassportNo" size="40" id="txtPassportNo"
						style="width: 120px;"/></td>
					<td></td>
				</tr>
				<tr>
					<td><font>Blacklist Type</font></td>
					<td><select name="blTypeSearch" id="blTypeSearch" style="width: 118px;">
							<option value="T">Tempory</option>
							<option value="P">Permanent</option>
					</select></td>
					<td align="left"><font>Nationality</font></td>
					<td><select name="nationalitySearch" size="1" id="nationalitySearch">
							<option value="-">All</option>
							<c:out value="${requestScope.reqNationalityList}"
								escapeXml="false" />
					</select></td>
					<td><font>Date of Birth</font></td>
					<td><input type="text" id="txtdobSearch" style="width: 98px"
							maxlength="10" readonly="readonly" /></td>
					<td></td>
				</tr>
				<tr>
					<td><font>Status </font></td>
					<td><select name="blStatusSearch" id="blStatusSearch" style="width: 118px;">
							<option value="ACT">Active</option>
							<option value="INA">Inactice</option>
					</select></td>	
					<td colspan='4'></td>
					<td align="right"><input name="btnSearch" type="button"
						id="btnSearch" value="Search" class="button"></td>
				</tr>

			</table>
		</div>
		</div>
		<div id="divBlPaxList" style="text-align: left; background-color: #ECECEC;">
		<div class="innerPanel divResults" style="border-top: 1px solid #fff">
			<div id="jqGridblacklistedPAXContainer" align="center"
				style="margin-top: 5px;">
				<table width="100%" border="0" cellpadding="0" cellspacing="0"
					id="jqGridblacklistedPAXData">
				</table>
				<div id="jqGridblacklistedPAXPages"></div>
			</div>
			<u:hasPrivilege privilegeId="pax.blacklist.add">
				<input name="btnAdd" type="button" id="btnAdd" value="Add"	class="btnDefault button" style="width: auto">
			</u:hasPrivilege>
			<u:hasPrivilege privilegeId="pax.blacklist.edit"> 
				<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault button" style="width: auto"> 
			</u:hasPrivilege>
			<u:hasPrivilege privilegeId="pax.blacklist.remove">
				<input name="btnDelete" type="button" id="btnDelete" value="Remove" class="btnDefault button" style="width: auto">
			</u:hasPrivilege>
		</div>
	</div>

	<div id="BlPaxDetails"
		style="text-align: left; background-color: #ECECEC;padding:5px;">
		<form method="post" action="" id="frmBlPaxDetails">
			<div id="tabBlPaxDetails" class="innerPanel">
				<div style="display: block;"></div>
				<table width="100%" border="0" cellpadding="0" cellspacing="2">
					<!-- <tr>
						<td valign="top"><font>First Name</font><font
							class="mandatory">&nbsp;*</font></td>
						<td valign="top"><input type="text" id="txtFnameDetails" size="40"
							style="width: 166px;" /></td>
						<td valign="top"><font>Last Name</font><font
							class="mandatory">&nbsp;*</font></td>
						<td valign="top"><input type="text" id="txtLnameDetails" size="40"
							style="width: 166px;" /></td>
						<td valign="top"><font>Passport No</font><font
							class="mandatory">&nbsp;*</font></td>
						<td valign="top"><input type="text" id="txtPassportNoDetails"
							size="40" style="width: 167px;" /></td>
					</tr> -->
					<tr>
						<td valign="top"><font>Passenger Name</font><font
							class="mandatory">&nbsp;*</font></td>
						<td colspan='3' valign="top"><input type="text" id="txtFullnameDetails" size="40"
							style="width: 465px;" /></td>
						

						<td valign="top"><font>Passport No</font><font
							class="mandatory">&nbsp;*</font></td>
						<td valign="top"><input type="text" id="txtPassportNoDetails"
							size="40" style="width: 167px;" /></td>
					</tr>

					<tr>
						<td align="left"><font>Nationality</font><font class="mandatory">&nbsp;*</font></td>
						<td><select name="nationalityDetails" size="1" id="nationalityDetails">
								<c:out value="${requestScope.reqNationalityList}"
								 	escapeXml="false" />
						</select></td>
						<td><font>Date of Birth</font></td>
						<td><input type="text" id="txtdobDetails" style="width: 145px;"
							maxlength="10" readonly="readonly" /></td>
						<td><font>Remark</font></td>
						<td><textarea id="txtRemarksDetails" name="txtRemarksDetails"></textarea>
						</td>
					</tr>
					<tr>
						<td><font>Blacklist Type</font><font class="mandatory">&nbsp;*</font></td>
						<td><select name="blTypeDetails" id="blTypeDetails" style="width: 165px;">
								<option value="T">Tempory</option>
								<option value="P">Permanent</option>
						</select></td>
						<td><font>Passenger Status</font><font class="mandatory">&nbsp;*</font></td>
						<td><select name="blStatusDetails" id="blStatusDetails" style="width: 165px;">
								<option value="ACT">Active</option>
								<option value="INA">Inactive</option>
						</select></td>
					</tr>

					<tr>
						<td><font class="period">Applicable Period</font></td>
					</tr>

					<tr>
						<td><font>From Date</font></td>

						<td><input type="text" id="blockPeriodFromDetails"
							style="width: 145px" maxlength="10" readonly="readonly" /></td>
						<td><font>To Date</font></td>
						<td><input type="text" id="blockPeriodToDetails" style="width: 145px"
							maxlength="10" readonly="readonly" /></td>
					</tr>
					<tr>
						<td colspan="5"></td>
						<td align="right" valign="bottom">
						<input name="btnReset" type="button" id="btnReset" value="Reset" class="btnDefault button" style="width: auto"> 
						<input name="btnSave" type="button" id="btnSave" value="Save" class="btnDefault button" style="width: auto">
									
						</td>						
					</tr>

				</table>

			</div>
			</form>
	</div>
	
	<table width="98%" border="0" cellpadding="0" cellspacing="2"
		align="center" ID="Table1">
		<tr>
			<td colspan="3" style="height: 42px;" valign="bottom"><input
				name="btnClose" type="button" id="btnClose" value="Close"
				onclick="top[1].objTMenu.tabRemove(screenId)"
				class="btnDefault button"></td>

		</tr>
	</table>
	

	<script type="text/javascript"
		src="../../js/v2/tools/BlacklistPAXManagement.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
		type="text/javascript"></script>

	<script>
		var screenId = 'SC_BLPAX_001';
		
		
		
		top[2].HideProgress();
	</script>
	
	<div id="dialog-remove" title="Remove passenger from blacklist" style="display:none; text-align: left; background-color: #ECECEC;">
	  <div id="addReasonForRemove">
	  <form>
	    <table>
	    <tr>
	    <td><font>Removal Effective from</font><font
							class="mandatory">&nbsp;*</font></td>
		</tr><tr>				
		<td><input type="text" id="validuntil" style="width: 70%"
							maxlength="10" readonly="readonly" /></td>
	    </tr>
	    <tr>
	    <td><font>Please enter a valid reason for removal</font><font
							class="mandatory">&nbsp;*</font></td>
		</tr><tr>
		<td><textarea id="removalReason" name="removalReason" rows="5" cols="50"></textarea>
						</td>
	    </tr>
	    <tr>
		<td align="right" valign="bottom"><input name="btnDeletePopup"
							type="button" id="btnDeletePopup" value="Remove"
							class="btnDefault button" style="width: auto">
	    </tr>
	    </table>
	  </form>
	  </div>
	  <div id="divConfirmDelete">
	  	<table>
	    <tr>
	    <td>&nbsp</td>
		</tr>
		<tr>
	    <td><font>Black List Passenger currently doesn,t have reservations. Do you want to permenently remove the blacklisted passenger ?</font></td>
		</tr>
		<tr><td>&nbsp</td></tr>
		<tr><td>&nbsp</td></tr>
		<td align="right" valign="bottom">
			<input name="permentlyDelete" type="button" id="permentlyDelete" value="Remove" class="btnDefault button" style="width: auto">
			<input name="permentlyDeleteCancel" type="button" id="permentlyDeleteCancel" value="Cancel" class="btnDefault button" style="width: auto">
		<tr><td>&nbsp</td></tr>
		</table>
	  </div>
	</div>
</body>
</html>