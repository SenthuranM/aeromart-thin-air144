<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	    <title>BSP HOT File Management</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">		
		<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
		
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
				
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
    	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.formfileupload.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		
		<script src="../../js/tools/PopUpData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>		
 	</head>
  	<body class="tabBGColor" scroll="no"  onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
		 		
  			<script type="text/javascript">			
				//<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />										
			</script>
			
			<div id="divSearch" style="height:100px;text-align:left;">
				<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
					<tr>
						<td width="12%"><font>File Name</font></td>
						<td width="20%">
							<input type="text" id="txtFileName" name="txtFileName" style="width:160px;" maxlength="50" >
						</td>
						<td width="8%"><font>Status</font></td>
						<td width="10%">
							<select name="selStatus" size="1" id="selStatus">
								<option value="">All</option>										
								<option value="UPLOADED">UPLOADED</option>
								<option value="PROCESSED">PROCESSED</option>
							  </select>
						</td><td width="10%"></td><td width="10%"></td>
						<td width="20%"></td>
</tr><tr>	
						<td width="10%"><font>Uploaded From</font></td>
						<td width="15%"><input name="txtUpDateFrom" type="text" id="txtUpDateFrom" size="10" style="width:80px;" maxlength="10"></td>
						<td width="10%"><font>Uploaded To</font></td>
						<td width="15%"><input name="txtUpDateTo" type="text" id="txtUpDateTo" size="10" style="width:80px;" maxlength="10"></td>
						<td width="7%"><font>Uploaded By</font></td>
						<td width="15%" colspan="2">
							<input type="text" id="txtUploadedBy" name="txtUploadedBy" style="width:120px;" maxlength="50">
						</td>								
					</tr>
					<tr>
<td><font>Processed From</font></td>
						<td><input name="txtProcDateFrom" type="text" id="txtProcDateFrom" size="10" style="width:80px;" maxlength="10"></td>
						<td><font>Processed To</font></td>
						<td><input name="txtProcDateTo" type="text" id="txtProcDateTo" size="10" style="width:80px;" maxlength="10"></td>
						<td><font>Processed By</font></td>
						<td colspan="2">
							<input type="text" id="txtProcessedBy" name="txtProcessedBy" style="width:120px;" maxlength="50">
						</td>						</tr>
					<tr>
						
						<td><font>DPC Processed From</font></td>
						<td><input name="txtDpcDateFrom" type="text" id="txtDpcDateFrom" size="10" style="width:80px;" maxlength="10"></td>
						<td><font>DPC Processed To</font></td>
						<td><input name="txtDpcDateTo" type="text" id="txtDpcDateTo" size="10" style="width:80px;" maxlength="10"></td>
<td colspan="2">
						<td align="center" >									
							<input name="btnSearch" type="button" id="btnSearch" value="Search">
						</td>
															
			</tr>
				</table>			
			</div>
			
			<div id="divResultsPanel" style="height:300px;text-align:left;background-color:#ECECEC;">
				<table id="listBSPHOTFiles" class="scroll" cellpadding="0" cellspacing="0"></table>
				<div id="bspHOTFilesPager" class="scroll" style="text-align:center;"></div>
					
					<input name="btnUpload" type="button" id="btnUpload" value="Upload" class="btnDefault">
					<input name="btnProcess" type="button" id="btnProcess" value="Process" class="btnDefault">
					<input name="btnDelete" type="button" id="btnDelete" value="Delete" class="btnDefault">
					
			</div>			  		
			<div id="divDispBSP" style="height:110px;text-align:left;background-color:#ECECEC;">
				<form method="post" action="ShowBSPHOTFileManagement.action" id="frmBSPHOTFiles" enctype="multipart/form-data"> 
				<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">	
<tr><td>&nbsp;</td></tr>
						<tr>
						<td width="20%"><font>HOT File Location</font></td>
						<td width="80%">
							<input type="file" id="fileHOTFile" name="fileHOTFile"/>&nbsp;[Only text files are accepted]
						</td>
						<td></td>
						</tr>
						<td>
							<input name="btnClose" type="button"  id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault">
							<input name="btnReset" type="button"  id="btnReset" value="Reset" class="btnDefault">
						</td>
						<td><input name="btnUploadNew" type="button" id="btnUploadNew" value="Upload" class="btnDefault">
						<input name="btnUploadNProcess" type="button" id="btnUploadNProcess" value="Upload & Process" class="btnDefault" style="width:130px;"></td>


						<tr><td>&nbsp;</td></tr></table>		
			  <input type="hidden" name="hdnMode" id="hdnMode">
			  <input type="hidden" name="hdnHOTFileLogId" id="hdnHOTFileLogId">
			  <input type="hidden" name="hdnHOTFileName" id="hdnHOTFileName">
			  </form>			 
			</div>	
	<script>
		var screenId = 'SC_SHDS_010';
		<c:out value="${requestScope.reqBSPAirlineCode}" escapeXml="false" />	
		top[2].HideProgress();
	</script>
	<script type="text/javascript" src="../../js/tools/BSPHOTFileManagement.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </body>
</html> 