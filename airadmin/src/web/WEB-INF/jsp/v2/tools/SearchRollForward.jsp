<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Search Roll Forward</title>
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<link rel="stylesheet" type="text/css"
	href="../../css/Style_no_cache.css" />
<link rel="stylesheet" type="text/css"
	href="../../themes/default/css/ui.all_no_cache.css" />
<link rel="stylesheet" type="text/css" href="../../css/ui.jqgrid.css" />

<script
	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.jqGrid.grouping.min.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script
	src="../../js/v2/tools/SearchRollForward.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script type="text/javascript">
	var languageHTML = "<c:out value="${requestScope.reqLanguageList}" escapeXml="false" />";
	var hasRollCancleBatchPrivilege="<c:out value="${requestScope.hasRollCancleBatchPrivilege}" escapeXml="false" />";
</script>
<style>
.innerPanel {
	width: 98%;
	margin: 0 auto;
}

.portSel {
	width: 50px
}

select {
	margin: 1px 1px;
}

.iconBtn {
	width: 25px
}

#ui-datepicker-div {
	display: none;
}

.ui-autocomplete {
	height: 200px;
	overflow-y: scroll;
	overflow-x: hidden;
}

.input.uppercase {
	text-transform: uppercase;
}

.period {
	padding-top: 5px;
	text-decoration: underline;
}

#blacklistPaxDetails {
	padding-top: 10px;
	padding-bottom: 10px;
	margin-top: 5px;
	border: solid 1px #aaaaaa;
}
</style>
</head>
<body class="tabBGColor" scroll="no"
	onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false"
	onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
	<input type="hidden" name="hdnSelectID" id="hdnSelectID" value=0 />
	<script type="text/javascript">
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	</script>

	<div id="divBlPaxSearch"
		style="text-align: left; background-color: #ECECEC;">
		<div class="innerPanel divsearch"
			style="border-bottom: 1px solid #bbb">
			<table width="96%" border="0" cellpadding="0" cellspacing="2"
				ID="Table9">
				<tr>

					<td><font>Flight No </font></td>
					<td><input name="txtFlightNoSearch" size="40"
						id="txtFlightNoSearch" style="width: 38%" /></td>

					<td></td>
				</tr>
				<tr>
					<td><font>From</font></td>
					<td><input type="text" id="txtFromSearch" size="40" invalidText="true" 
						style="width: 38%" maxlength="10"  onBlur="dateChk('txtFromSearch')"/> <font
						class="mandatory">&nbsp;*</font></td>
					<td><font>To</font></td>
					<td><input type="text" id="txtToSearch" size="40" invalidText="true"
						style="width: 38%" maxlength="10" onBlur="dateChk('txtToSearch')"/> <font
						class="mandatory">&nbsp;*</font></td>
					<td></td>
				</tr>
				<tr>
					<td><font>Batch Id</font></td>
					<td><input name="txtBatchIdSearch" size="40"
						id="txtBatchIdSearch" style="width: 120px;" /></td>
				</tr>
				<tr>

					<td align="left"><font>Status</font></td>
					<td><select name="statusSearch" size="1" id="statusSearch">
							<option value="-">All</option>
							<c:out value="${requestScope.reqStatusList}" escapeXml="false" />
					</select></td>

					<td align="right"><input name="btnSearch" type="button"
						id="btnSearch" value="Search" class="button"></td>
				</tr>

			</table>
		</div>
		<div class="innerPanel divResults" style="border-top: 1px solid #fff">
			<div id="jqGridsearchRollForwardContainer" align="center"
				style="margin-top: 5px;">
				<table width="100%" border="0" cellpadding="0" cellspacing="0"
					id="jqGridsearchRollForwardData">
				</table>
				<div id="jqGridsearchRollForwardPages"></div>
			</div>
		</div>
		<div align="center" style="margin-top: 5px;">
			<table width="98%" border="0" cellpadding="0" cellspacing="2"
				align="center" ID="Table1">
				<tr>
					<td colspan="3" style="height: 42px;" valign="bottom"><input
						name="btnClose" type="button" id="btnClose" value="Close"
						onclick="top[1].objTMenu.tabRemove(screenId)"
						class="btnDefault button"></td>
				</tr>
			</table>
		</div>
	</div>

	<div id="popUp" class="myPopup"></div>
	<script>
		var screenId = 'SC_RFSC_001';
		top[2].HideProgress();
	</script>

	<script type="text/javascript"
		src="../../js/v2/tools/SearchRollForward.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
		type="text/javascript"></script>

</body>
</html>