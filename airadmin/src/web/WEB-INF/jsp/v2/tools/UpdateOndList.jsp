<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
function showStatus(){
	var screenId = 'SC_SHDS_007';
	top[2].HideProgress();
	var status = <c:out value="${requestScope.reqUpdateStatus}" escapeXml="false" />

	if(status){
		alert("Ond list update successful.");
	} else {
		alert("Ond list update failed.");
	}
	top[1].objTMenu.tabRemove(screenId);
}
</script>
<head>
<body onLoad="showStatus()">
</body>
</html>