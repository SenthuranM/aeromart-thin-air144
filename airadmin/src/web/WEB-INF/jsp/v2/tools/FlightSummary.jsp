<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Flight Summary</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">

	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
	<script src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
   	<script src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/tools/FlightSummary.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
 	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
 	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
 </head>

 <body>	
 <script type="text/javascript">
 <c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
 var screenId = "SC_SHDS_009";
 </script>
 <div id="flightSearchPanel">		
	<form method="post" name="frmFlightSummary"  id="frmFlightSummary" action="showFlightSummary.action">
	<table width="99%" align="center" border="0" cellpadding="1" cellspacing="0">
		<tr>
			<td>				
				<table width="85%" border="0" cellpadding="0" cellspacing="2" ID="Table2">
					<tr>
						<td colspan="2"><font>Departure Date</font></td>		
						<td style="width:115px;">
							<input type="text" name="departureDate" id="txtDepartureDate" style="width:70px;"  maxlength="10" tabIndex="1">
							<font class="mandatory"> &nbsp;*</font>
						</td>			
						<td><font>Flight No</font></td>		
						<td>
							<input type="text" id="txtFlightNoSearch" name="flightNumber" maxlength="7" style="width:50px" tabIndex="8">
							<font class="mandatory"> &nbsp;* </font>									
						</td>				
						<td>
							<input type="button" id="btnSearch"  name="btnSearch" value="Search" class="button" tabIndex="14">
						</td>
					</tr>																				
				</table>
			</td>
		</tr>	
	</table>
	</form>
	</div>
 <div id="flightSummary">
 <table id="tblFlightSummary" ></table>
 <div id="temppager" class="scroll" style="text-align:center;"></div>
 </div>
 <div id="flightBCSummary">
 <table id="tblFlightBCSummary">
 </table>
 <div id="popUp" class="myPopup"></div>
 <div id="temppager" class="scroll"></div>
 <table>
 <tr>
 <td colspan="2" style="height:25px;"  valign="bottom">
	<input name="btnClose" type="button"  id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault">
</td>
 </tr>
 </table>
 </div>
</body>
</html>