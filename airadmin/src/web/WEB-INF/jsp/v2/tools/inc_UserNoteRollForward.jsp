
	<div id='divNoteRollForward' style='position:absolute;top:200px;left:300px;width:450px;z-index:1001;display:none;' class="popupXBE ui-corner-all">
		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td class="ui-widget ui-widget-content ui-corner-all popupXBEborder">
					<table width='100%' border='0' cellpadding='0' cellspacing='0'>
						<tr>
							<td class='paneHD'>
								<p class='txtWhite txtBold'> &nbsp; User Notes Roll Forward</p>
							</td>
						</tr>
						<tr>
							<td  class='popBGColor singleGap'></td>
						</tr>
						<tr id='trAncillaryOpt'>
							<td class='popBGColor'  align='center'>
								<table width='95%' border='0' cellpadding='1' cellspacing='0'>
									<tr>
										<td align='left'>From Date&nbsp;</td>
										<td colspan="2" align='left'>
											<input id="rollForwardStartDate" name="rollForwardStartDate" type="text" 
												readonly="readonly" class="aa-input" style="width:80px;" maxlength="10"/>
										</td>
									</tr>
									<tr>
										<td align='left'>To Date&nbsp;</td>
										<td align='left'>
											<input id="rollForwardEndDate" name="rollForwardEndDate" type="text" 
												readonly="readonly" class="aa-input" style="width:80px;" maxlength="10"/>
										</td>
										<td align="left">
											<button id="btnNoteRollForwardSearchAction" type="button" class="Button">Search</button>
											&nbsp; &nbsp;
											<button id="btnNoteRollForwardCancel" type="button" class="Button">Close</button>
										</td>
									</tr>
																	
								</table>
							</td>							
						</tr>
						<tr>
							<td  class='popBGColor singleGap' style='height: 15px'></td>
						</tr>
						<tr>
							<td class='popBGColor' align='center'>
								<div style="height: 200px;overflow-y:auto;overflow-x:hidden;" id="divRFAvailFlights">
								
									<table width='95%' border='0' cellpadding='1' cellspacing='0'>
										<tr >
											<td>
												<input type="checkbox" id="chkNoteRFAll"/>
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<label class='txtBold' >All</label>
											</td>
										</tr>
									</table>
									<table width='95%' border='0' cellpadding='1' cellspacing='0'>
										<tr id="trNoteRollForwardTemp" style="display: none;">
											<td>
												<input type="checkbox" name="chkOhdRF"/>
											</td>
											<td align="left">
												<label class='txtBold' ></label>
											</td>
										</tr>
									</table>
								</div>
							</td>							
						</tr>
						<tr>
							<td  class='popBGColor singleGap'></td>
						</tr>
						<tr id='trNoteRFConfirm' style="display: none;">
							<td>
								<button id="btnNoteRollForwardConfirm" type="button" class="Button">Confirm</button>
							</td>
						</tr>
						<tr>
							<td  class='popBGColor singleGap'></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>