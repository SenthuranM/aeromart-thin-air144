<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
 
    <title>Dashboard Messages</title>
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
<LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css">

	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">		
		<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
				
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script type="text/javascript" src="../../js/v2/jquery/grid.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/grid.inlinedit.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> 
    	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.RESCRIPTED.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jquery.popupwindow.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jqValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/isa.airadmin.jq.plugin.config.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	

	<script src="../../js/v2/isalibs/isa.jquery.multi-selector.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
  	<script type="text/javascript">  		
  		var arrError = new Array();  
  		var agentMulti = new Array();
  		var allAgents = null; 	
  		var allAgents_act = null;			 
  		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			
  		var allStations = <c:out value="${requestScope.reqStationList}" escapeXml="false" />
  		var allStations_Act = <c:out value="${requestScope.reqActStationList}" escapeXml="false" />
  		var allAgentTypes =	<c:out value="${requestScope.reqagentType}" escapeXml="false" />
  		//var allAgents =<c:out value="${requestScope.reqAgentCombo}" escapeXml="false" />  		
  		//var allAgentsStr =<c:out value="${requestScope.agentSTR}" escapeXml="false" /> 		
  		//var allItemsArr =<c:out value="${requestScope.itemsAllArr}" escapeXml="false" /> 	
  </script>
  </head>
  <body class='tabBGColor'>

<div style="margin-top:0px;height:570px;">

	<div id="divMessagesSearch" style="height:60px;text-align:left;background-color:#ECECEC;">
  	<form name="frmMessagesSearch" id="frmMessagesSearch" action="" method="post">
		<table width="100%" border="0" cellpadding="2" cellspacing="2">
			<tr>
				<td align="left" class="fntBold" width = '15%'><font>Message Type</font></td>
				<td align="left" width = '20%'>
				<select id="selSearchMsgType" name="selSearchMsgType" class="aa-input" style="width:150px">
				<option value="">All</option>																				
				<c:out value="${requestScope.msgTypeList}" escapeXml="false" />		</select>
				</td>
				<td align="left" width = '15%'><font class="fntBold">Message</font></td>
				<td align="left"><input name="searchMessage" type="text" id="searchMessage" class = "" size="20"  maxlength="20" />						
				</td>
				<td align="left"><font class="fntBold">User</font></td>
				<td align="left"><input name="searchUser" type="text" id="searchUser" class = "" size="20"  maxlength="20" />						
				</td>
			</tr>
			<tr>
				<td align="left"><font class="fntBold">Effective From</font></td>
				<td align="left"><input name="searchFrom" type="text" id="searchFrom" class = "" size="10"  maxlength="10"  onBlur="dateChk('searchFrom')" />						
				</td>
				<td align="left"><font class="fntBold">Effective To</font></td>
				<td align="left"><input name="searchTo" type="text" id="searchTo" class = "" size="10"  maxlength="10"  onBlur="dateChk('searchTo')"/>						
				</td>

				<td align="right" colspan = "2">
					<input name="btnSearch" type="button" class="btnDefault" id="btnSearch" value="Search"/>
				</td>
			</tr>			
		</table>
	</form>
	</div>
	<div id="divResultsPanel" style="height:140px;text-align:left;background-color:#ECECEC;">
			<table id="tblDashboardMsg" class="scroll" cellpadding="0" cellspacing="0"></table>
			<div id="divDashboardMsgPager" class="scroll" style="text-align:center;"></div>
					<u:hasPrivilege privilegeId="tool.dashboard.add">	
						<input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault"/>
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="tool.dashboard.edit">
						<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault"/>
					</u:hasPrivilege>		
								
	</div>	
	<form id = "frmModify" method="post" action="dashBoardMessageManagement!addEditMessage.action">
	
	<div id="divDisplayDashboardMsg" style="height:325px;text-align:left;background-color:#ECECEC;">
		<table width="98%" border="0" cellpadding="0" cellspacing="0" align="center" >
			<tr>
				<td width = '50%' valign="top">
					<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" >
						<tr><td>&nbsp;</td></tr>	
						<tr>
							<td width="20%"><font  class="fntBold">Message Type</font></td>
							<td  width = "20%">
								<select id="selMsgType" name="dashboardMsgTO.messageType" class="aa-input frm_editable" style="width:150px">
								<c:out value="${requestScope.msgTypeList}" escapeXml="false" />		</select>					
							<font class="mandatory">*</font>
							</td>											
						</tr>
						<tr><td>&nbsp;</td></tr>	
						<tr>
							<td ><font  class="fntBold">From Date</font></td>
							<td>
								<input name="dashboardMsgTO.fromDate" type="text" class = "aa-input frm_editable" id="fromDate" size="10"  maxlength="20"  onBlur="dateChk('fromDate')"/>						
							<font class="mandatory">*</font>
							</td>
						</tr>	
						<tr><td>&nbsp;</td></tr>	
						<tr>
							<td width = "10%"><font  class="fntBold">To Date</font></td>
							<td>
								<input name="dashboardMsgTO.toDate" type="text" id="toDate" class = "aa-input frm_editable" size="10"  maxlength="20"  onBlur="dateChk('toDate')"/>						
							<font class="mandatory">*</font>
							</td>
						</tr>	
						<tr><td>&nbsp;</td></tr>			
						<tr>
							<td width = "10%"><font  class="fntBold">Priority</font></td>
							<td width="75%">
							<input name="dashboardMsgTO.priority" type="text"id="txtEditPriority" size="10" maxlength="10" class = "aa-input frm_editable" onkeyPress="JqValidations.kpNumeric('txtEditPriority')" onKeyUp="JqValidations.kpNumeric('txtEditPriority')"/>
							<font class="mandatory">*</font>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>	
						<tr>
							<td width = "10%"><font  class="fntBold">Active</font></td>
							<td width="75%">
							<input name="chkStatus" type="checkbox"id="chkStatus" size="2" class = "chkStatus aa-input frm_editable" value="ACT"/>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>	
						<tr>
							<td><font  class="fntBold">Message</font></td>
							<td rowspan="2"><textarea name='dashboardMsgTO.messageContent'
									id='txtMessage' class="aa-input frm_editable"
									style='height: 50px; width: 300px;' onkeyPress="UI_DashboardMsg.kpValidateContentDynamic('txtMessage')" onKeyUp="UI_DashboardMsg.kpValidateContentDynamic('txtMessage')"></textarea>
							<font class="mandatory">*</font>		
							</td>
									
						</tr>
						<tr><td>&nbsp;</td></tr>	
					</table>				
				</td>
				<td height="100%"  valign="top">
					<table width="100%" border="0" cellpadding="2" cellspacing="2" align="center" height="100%">
						<tr>
							<td colspan = "6" valign="top"><font class="fntBold">User Visibility</font></td>
						</tr>
						<tr>
							<td colspan = "6" width="100%" valign="top">
							<div id = "divTabbedVisiblity" style = "background-color:#ECECEC;">
								<ul>
									<li><a href="#divSearchPOS">By POS</a></li>
									<li><a href="#divSearchAgent">By Agent</a></li>
									<li><a href="#divSearchAgentType">By Agent Type</a></li>
									<li><a href="#divUserSelectionWrapper">By User</a></li>
								</ul>
									<%@ include file="inc_dashboardSearchPOS.jsp" %>
									<%@ include file="inc_dashboardSearchAgentType.jsp" %>
									<%@ include file="inc_dashboardSearchAgent.jsp" %>
									<%@ include file="inc_dashboardMsgUserSearch.jsp" %>
							</div>
							</td>
						</tr>								
					</table>	
				</td>
			</tr>						
		</table>
		<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center">
						
					<tr>
						<td colspan="3" style="height:42px;"  valign="bottom">
							<input name="btnClose" type="button"  id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId);" class="btnDefault" />
						    <input name="btnReset" type="button"  id="btnReset" value="Reset" class="btnDefault" />
						</td>
						<td align="right" valign="bottom">
							<input name="btnSave" type="button"  id="btnSave" value="Save" class="btnDefault" />
						</td>
					</tr>
		</table>
	</div>
			<input type="hidden" name="dashboardMsgTO.messageID" id="hdnMessageID" value="">
			<input type="hidden" name="dashboardMsgTO.version" id="hdnVersion" value="">
			<input type="hidden" name="dashboardMsgTO.status" id="hdnStatus" value="">
			<input type="hidden" name="dashboardMsgTO.agentInclusionStatus" id="hdnAgentInclusionStatus" value="">
			<input type="hidden" name="dashboardMsgTO.userInclusionStatus" id="hdnUserInclusionStatus" value="">
			<input type="hidden" name="dashboardMsgTO.agentTypeInclusionStatus" id="hdnAgentTypeInclusionStatus" value="">
			<input type="hidden" name="dashboardMsgTO.posInclusionStatus" id="hdnPosInclusionStatus" value="">
			<input type="hidden" name="dashboardMsgTO.selectedAgents" id="hdnSelectedAgents" value="">
			<input type="hidden" name="dashboardMsgTO.selectedAgentTypes" id="hdnSelectedAgentTypes" value="">
			<input type="hidden" name="dashboardMsgTO.selectedPOS" id="hdnSelectedPOS" value="">
			<input type="hidden" name="dashboardMsgTO.userList" id="hdnUserList" value="">
	</form>
	</div>

<script type="text/javascript">
  	<!--
	var screenId = 'SC_SHDS_006';
	top[2].HideProgress();
  	//-->
  </script>
  		<script type="text/javascript">
				<c:out value="${requestScope.reqGSAList}" escapeXml="false" />	
				<c:out value="${requestScope.reqAgentList}" escapeXml="false" />			
		</script>
  	<script type="text/javascript" src="../../js/v2/tools/dashboardMsgAgent.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  	 <script type="text/javascript" src="../../js/v2/tools/dashboardMsgUser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  		 <script type="text/javascript" src="../../js/v2/tools/dashboardMsgAgentType.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  		 <script type="text/javascript" src="../../js/v2/tools/dashboardMsgPOS.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  
	 <script type="text/javascript" src="../../js/v2/tools/dashboardMessageManagement.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	 
  </body>
</html>
