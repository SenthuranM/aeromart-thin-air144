<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	    <title>PNRGOV Report Page</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">		
		<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
		
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
				
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
    	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		
		<script src="../../js/tools/PopUpData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		   
		<script type="text/javascript">
			var languageHTML = "<c:out value="${requestScope.reqLanguageList}" escapeXml="false" />";
		</script>
 	</head>
  	<body class="tabBGColor" scroll="no"  onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
		 		
  			<script type="text/javascript">			
				//<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />										
			</script>
			
			<div id="divSearch" style="height:30px;text-align:left;">
				<table width="96%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
					<tr>
						<td width="12%"><font>Country</font></td>
						<td width="10%">
							<select name="selCountry" size="1" id="selCountry">
								<option value="">All</option>
								<c:out value="${requestScope.reqPNRGOVCountryList}" escapeXml="false" />
							</select>
						</td>
						<td width="2%">&nbsp;</td>
						<td width="8%"><font>Status</font></td>
						<td width="7%">
							<select name="selStatus" size="1" id="selStatus">
								<option value="">All</option>										
								<option value="ERROR">ERROR</option>
								<option value="SUBMITTED">SUBMITTED</option>
							  </select>
						</td>
						<td width="10%"><font>Flight Seg ID</font></td>
						<td><input type="text" id="txtFltSegId" name="txtFltSegId" style="width:75px;" maxlength="12" invalidText="true" ></td>	
						<td width="10%"><font>From Date</font></td>
						<td><input type="text" id="txtDateFrom" name="txtDateFrom" style="width:75px;" maxlength="12" onblur="dateChk('txtDateFrom')"  invalidText="true" ></td>
						<td>
							<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</td>
						<td width="10%"><font>To Date</font></td>
						<td>
							<input type="text" id="txtDateTo" name="txtDateTo" style="width:75px;" maxlength="12" onblur="dateChk('txtDateTo')" invalidText="true">
						</td>
						<td>
							<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a>
						</td>
						<td width="2%">&nbsp;</td>
														
						<td align="right" width="8%">									
							<input name="btnSearch" type="button" id="btnSearch" value="Search">
						</td>
					</tr>
				</table>			
			</div>
			
			<div id="divResultsPanel" style="height:300px;text-align:left;background-color:#ECECEC;">
				<table id="listPNRGOVReport" class="scroll" cellpadding="0" cellspacing="0"></table>
				<div id="pnrgovreportpager" class="scroll" style="text-align:center;"></div>
					
					<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault">
					
			</div>			  		
			<div id="divDispPNRGOV" style="height:330px;text-align:left;background-color:#ECECEC;">
				<form method="post" action="ShowPnrGovReport.action" id="frmPNRGOVReport" enctype="multipart/form-data"> 
				<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
						<tr><td>&nbsp;</td></tr>	
						<tr>
						<td><font>Log ID</font></td>
						<td>
							<input type="text" id="pnrGovTxHistoryID" name="pnrGovTxHistoryID" size="18" readonly="readonly">
						</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr>
				  			<td colspan="3" style="height:42px;"  valign="bottom">
								<input name="btnClose" type="button"  id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault">
						    	<input name="btnResubmit" type="button"  id="btnResubmit" value="Resend" class="btnDefault">
							</td>
				                
				  		</tr>		
					
			 </table>		
			  <input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
			  </form>			 
			</div>	
	<script>
		var screenId = 'SC_SHDS_008';
		top[2].HideProgress();
	</script>
	<script type="text/javascript" src="../../js/tools/PNRGOVReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </body>
</html> 
