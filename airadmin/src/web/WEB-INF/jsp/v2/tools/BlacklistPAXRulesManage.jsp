<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	    <title></title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">		
		<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
		
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
				
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
    	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jquery.popupwindow.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jqValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/isa.airadmin.jq.plugin.config.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
			

		<script src="../../js/v2/isalibs/isa.jquery.multi-selector.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
		
		<script src="../../js/tools/PopUpData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		   
		<script type="text/javascript">
			var arrError = [];
			<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />
			//var strCnfMessageType = "<c:out value="${requestScope.reqClientErrors}" escapeXml="false" />";				   
	</script>
 	</head>
  	<body class="tabBGColor" scroll="no"  onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
		 		
  			<script type="text/javascript">			
				//<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />										
			</script>
			
			<div id="divSearch" style="height:30px;text-align:left;">
				<table width="100%" border="0" cellpadding="2" cellspacing="2">
							<tr>
								<td valign="top" >
									<font>Label </font>
								</td>
								<td >
									<input  name="txtLabelSearch" type="text" id="txtLabelSearch" size="30"/>
								</td>
								<td valign="top" >
									<font>Status</font>
								</td>
								<td><select name="statusSearch" id="statusSearch" style="width: 80px;">
										<option value="">All</option>
										<option value="ACT">Active</option>
										<option value="INA">Inactive</option>
								</select></td>
								<td>&nbsp;</td>						
						        <td align="left" colspan="3">
								  	<input name="btnSearch" type="button" class="Button" id="buttonSearch" value="Search" /> 
								</td>
							</tr>
							
	 				    </table>			
			</div>
			
			<div id="divResultsPanel" style="height:300px;text-align:left;background-color:#ECECEC;">
				<table width="100%" border="0" cellpadding="0" cellspacing="4">							
							<tr><td id="BlacklistRulesGridContainer"></td>
							
							</tr>
							<tr><td>
								<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
								<tr><td>
											<u:hasPrivilege privilegeId="pax.blacklist.res.rule.add">
												<input type="button" id="btnAdd"  name="btnAdd" class="Button" value="Add" tabindex="4"/>
												</u:hasPrivilege>
												<u:hasPrivilege privilegeId="pax.blacklist.res.rule.edit">
												<input type="button" id="btnEdit"  name="btnEdit" class="Button" value="Edit" tabindex="5"/>
												</u:hasPrivilege>
											</td>
										</tr>
									</table>
								</td>
							</tr>							
						</table>
					
			</div>			  		
			<div id="BlPaxRulesDetails" style="text-align: left; background-color: #ECECEC;">
			<form method="post" action="" id="frmBlPaxDetails">
				<div id="tabBlPaxRulesDetails" class="innerPanel">
					<div style="display: block;"></div>
					<table width="100%" border="0" cellpadding="0">
						<tr>
							<!-- <td colspan="2"> -->
								<!-- <table width="100%" border="0" cellpadding="0" cellspacing="2"> -->
									<!-- <tr> -->
							<td valign="top" width="10%"><font>Label</font><font
									class="mandatory">&nbsp;*</font></td>
										<td valign="top"><input type="text" id="txtLabelDetails" size="50"
									style="width: 170px;" /></td>
						</tr>
						<tr>
								<td><font>Status </font>	</td>
								<td><select name="statusDetails" id="statusDetails" style="width: 170px;">
											<option value="ACT">Active</option>
											<option value="INA">Inactive</option>
									</select></td>
								<!-- </tr> -->
								<!-- </table> -->
							<!-- </td>	 -->						
						</tr>
						
						<tr>
							<!-- listview span -->
							<td><font>Data Element </font>
							<font class="mandatory">&nbsp;*</font></td>
							<td valign="top"><span id="spn3"></span></td>
						</tr>
	
						<tr>							
							<td colspan="5" align="right" valign="bottom" >
								<input type="button" id="btnReset"  name="btnReset" class="Button" value="Reset" tabindex="6"/>
								<input type="button" id="btnSave"  name="btnSave" class="Button" value="Save" tabindex="7"/>
								<input type="button" id="btnClose"  name="btnClose" class="Button" value="Close" tabindex="8"/>
							</td>
						</tr>
	
					</table>
	
				</div>
				</form>
			</div>	
	<script>
		var screenId = '';
		top[2].HideProgress();	
 		<c:out value="${requestScope.reqBlRulesDataElementList}" escapeXml="false" />
	</script>
	<script type="text/javascript" src="../../js/v2/tools/BlacklistPAXRulesManagement.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  </body>
</html> 