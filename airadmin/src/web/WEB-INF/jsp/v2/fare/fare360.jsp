<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Fare360</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    
	<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>
	<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/jquery.multiselect2side_no_cache.css"/>
	<LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css"/>
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>
	<LINK rel="stylesheet" type="text/css" href="../../css/fare360_no_cache.css"/>
	
	<script type="text/javascript" src="../../js/v2/fare/countryCity.js"></script>
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.json.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.treeview.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.multiselect2side.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.fares.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.fareslider.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.test.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="http://www.google.com/jsapi"></script>
		
	<script src="../../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
	<script type="text/javascript">
      google.load('visualization', '1', {packages: ['table','corechart']});
	    function drawVisualization() {
	      // Create and populate the data table.
	      var JSONObject = {
	          cols: [{id: 'period', label: '&nbsp', type: 'string'},
	              {id: 'RAK', label: 'Qatar Airways', type: 'number'},
	              {id: 'IND', label: 'Emirates', type: 'number'},
	              {id: 'FLY', label: 'TAROM', type: 'number'},
	              {id: 'EMI', label: 'Turkish', type: 'number'}],
	          rows: [{c:[{v: 'High'}, {v: 340}, {v: 492}, {v: 410}, {v: 460}]},
	              {c:[{v: 'Medium'}, {v: 313}, {v: 492}, {v: 410}, {v: 460}]},
	              {c:[{v: 'Low'}, {v: 313}, {v: 492}, {v: 410}, {v: 460}]}]};
	    
	      var data = new google.visualization.DataTable(JSONObject, 0.5);
	    
			var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
	       chart.draw(data, {width: 480, height: 120, title: 'DXB - CDG'});
	
	      // Create and draw the visualization.
	      visualization = new google.visualization.Table(document.getElementById('table-data'));
	      visualization.draw(data, {'allowHtml': true,width: 420, height: 96});
	    }
	    google.setOnLoadCallback(drawVisualization);


   $(function () {
  	    
       var $tabs = $('#tabs1').tabs({
   		tabTemplate: '<li><a href="#{href}">#{label}</a> <span class="ui-icon ui-icon-close">Remove Tab</span></li>',
   		add: function(event, ui) {
   			//var tab_content = $tab_content_input.val() || 'Tab '+tab_counter+' content.';
   			//$(ui.panel).append('<p>'+tab_content+'</p>');
   			$.ajax({
   				url: uri,
   				dataType:"html",
   				context: document.body,
   				success: function(result){
   					$(ui.panel).html(result);
   		      	},
   		      	error: function (error){
   		      		alert(error);
   		      	},
   		      	complete:function (){
   		      		$( "#tabs1" ).tabs( "select" , tab_counter-1 );
   		      	}
   			});
   		}
   	});
           
       	$(document).on('click','.showFares', function(){
       		if ($(this).attr("disabled"))
       			return false;
       		else
       			addTab($(this).text());
       		
       		$(this).attr("disabled","disabled");
       	});
       
           
       function addTab(tabId) {
   		var tab_title = tabId;
   		uri = "showLoadJsp!loadFare360Data.action?myobj="+tabId;
   		$tabs.tabs('add', '#load-'+tabId, tab_title);
   		tab_counter++;
   		$("#tabs1 ul").css("border-bottom","1px solid #AAAAAA");
   	};

   	$(document).on('click','#tabs1 span.ui-icon-close' function() {
   		var takeing = $(this).prev("a").attr("href");
   		if ($(takeing+" .fares").hasClass("selected"))
   			alert("Please un-select the fare(s) before close the tab");
   		else{
   		var index = $('li',$tabs).index($(this).parent());
   		$tabs.tabs('remove', index);
   		tab_counter--;
   		var tk = takeing.split("#load-")
   		$("#"+tk[1]).attr("disabled","");
   		}
   		if ($("#tabs1").tabs("length") == 0)
   			$("#tabs1 ul").css("border-bottom","0px");
   		
   	});
   	
       });
    </script>
</head>
  <body>

	<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" id="Table4">
		<tbody>
		<tr>
			<td height="20" width="10"><img src="../../images/form_top_left_corner_no_cache.gif"></td>
			<td height="20" class="FormHeadBackGround">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr><td width="880"><font class="FormHeader"><img src="../../images/upArrow_no_cache.png" align="absmiddle" class="searchTriger"/> Search</font></td>
				<td align="right"></td>
				</tr></table>
				
			</td>
			<td height="20" width="11"><img src="../../images/form_top_right_corner_no_cache.gif"></td>
		</tr>
		<tr>
			<td class="FormBackGround"></td>
			<td valign="top" class="FormBackGround">

			<div class="searchCri" style="height:265px">
			<table cellspacing="0" cellpadding="2" border="0" align="center" width="100%" id="Table4" height="200">
					<tbody>
					<tr>
						<td colspan="3" >
						<div id="ondselection">
						<table cellspacing="0" cellpadding="2" border="0" align="center" width="100%" >
							<tbody>
							<tr>
							<td>
							Origin
								<select id="origin">
									
								</select>
							</td>
							<td>
							Destination
								<select id="destination">
									
								</select>
							</td>
							<td>
							Via 1
								<select id="via1">
									
								</select>
							</td>
							<td>
							Via 2
								<select id="via2">
									
								</select>
							</td>
							<td>
							Via 3
								<select id="via3">
									
								</select>
							</td>
							<td>
							Via 4
								<select id="via4">
									
								</select>
							</td>
							</tr>
							</tbody>
						</table>
						</div>
						</td>
					</tr>
					<tr>
					<td width="33%">
					<table cellspacing="0" cellpadding="2" border="0" align="center" width="100%" id="Table4">
					<tbody>
					<tr>
						<td colspan="2">Select Multiple O&Ds:</td>
					</tr>
					<!-- <tr>
						<td colspan="2">
						<input type="radio" checked="checked" name="ondType" id="ondDirect" /> Direct &nbsp;&nbsp; 
						<input type="radio" name="ondType" id="ondViaHub" /> Via Hubs &nbsp;&nbsp; 
						<input type="radio" name="ondType" id="ondAll" /> All O&Ds
						</td>
					</tr> --> 
					<tr>
					<td valign="top">
					<select id="ondlist" name="ondlist[]" multiple='multiple' size='10'>
						
					</select>
					</td>
					</tr>
					</tbody>
				</table>
					
					</td>
					<td width="34%" align="center">
					<table cellspacing="0" cellpadding="2" border="0" align="center" width="100%" id="Table4">
						<tbody>
						<tr>
							<td>Select Multiple Fare Rules:</td>
						</tr>
						<tr>
						<td rowspan="2" valign="top">
							<select id="fareRule" name="fareRule[]" multiple='multiple' size='10'>
									<option value="RT:01OCT">YK3M</option>
									<option value="RT:02APR">YE1M</option>
									<option value="RT:02OCT">YK1M</option>
									<option value="OW:0YSTBY">YE3M</option>
									<option value="OW:10OCT">YK4M</option>
									<option value="OW:15AUG">YE4M</option>
							</select>
						</td>
						</tr>
						</tbody>
					</table>
					</td>
					<td width="34%">
					<table cellspacing="0" cellpadding="2" border="0" align="center" width="100%" id="Table4">
						<tbody>
						<tr>
							<td>Select Multiple Booking Class:</td>
						</tr>
						<tr>
							<td>
								<select id="bookingClass" name="bookingClass[]" multiple='multiple' size='10'>
									<optgroup label="Return">
										<option value="-1">All</option>
										<option value="0Y">0Y</option>
										<option value="0YCN1">0YCN1</option>
										<option value="0YY">0YY</option>
										<option value="3OF">3OF</option>
										<option value="3OY">3OY</option>
									</optgroup>
									<optgroup label="Return">
										<option value="A6">A6</option>
										<option value="A7">A7</option>
										<option value="A8">A8</option>
										<option value="AA">AA</option>
										<option value="AB">AB</option>
										<option value="AD">AD</option>
										<option value="AF">AF</option>
										<option value="AG">AG</option>
										<option value="AK">AK</option>
										<option value="AL">AL</option>
										<option value="AM">AM</option>
									</optgroup>
									<optgroup label="Return">
										<option value="AM1">AM1</option>
										<option value="AN">AN</option>
										<option value="AR">AR</option>
										<option value="AS">AS</option>
										<option value="AT">AT</option>
										<option value="B">B</option>
										<option value="B1">B1</option>
										<option value="B2">B2</option>
										<option value="B3">B3</option>
										<option value="B4">B4</option>
										<option value="B5">B5</option>
										<option value="B6">B6</option>
									</optgroup>
								</select>
							</td>
						</tr>
						</tbody>
					</table>
					</td>
					</tr>
					<tr>
					<td colspan="4">
						<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" id="Table4">
							<tbody>
								<tr>
								<td>
								<table cellspacing="0" cellpadding="2" border="0" align="center" width="100%" class="dateTable">
									<tbody>
										<tr>
											<td>&nbsp;</td>
											<td><input type="checkbox" id="svcheck" name="svcheck" checked="checked"> Sales Validity:</td>
											<td><input type="checkbox" id="dvcheck" name="dvcheck"> Departure Validity:</td>
											<td><input type="checkbox" id="rvcheck" name="rvcheck"> Return Validity:</td>
										</tr>
										<tr>
											<td>From Date: </td>
											<td><input type="text" id="fromSalesDate" name="fromSalesDate" /> </td>
											<td><input type="text" id="fromDepartureDate" name="fromDepartureDate" disabled="disabled"/></td>
											<td><input type="text" id="fromReturnDate" name="fromReturnDate" disabled="disabled"/></td>
										</tr>
										<tr>
											<td>To Date: </td>
											<td><input type="text" id="toSalesDate" name="toSalesDate" /> </td>
											<td><input type="text" id="toDepartureDate" name="toDepartureDate" disabled="disabled"/></td>
											<td><input type="text" id="toReturnDate" name="toReturnDate" disabled="disabled"/></td>
										</tr>
									</tbody>
									</table>
								</td>
								<td>
								<table cellspacing="0" cellpadding="2" border="0" align="center" width="100%" class="bottomTable">
									<tbody>
										<tr>
											<td></td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td></td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											
										</tr>
										<tr>
											<td>COS:</td>
											<td>
											<select tabindex="9" size="1" name="selCOS" id="selCOS">
												<option value=""></option>
												<option value="Y" selected="selected">Economy Class</option>
											</select>
											</td>
											<td>Status:</td>
											<td>
											<select tabindex="10" size="1" name="selStatus" id="selStatus">
												<option value="All">All</option>
												<option value="ACT">Active</option>
												<option value="INA">Inactive</option>
											</select>
											</td>
											<td>Min Fare:</td>
											<td>
											<input type="text" id="minFare" name="minFare" value="166.00"/>
											</td>
											<td><!--Fare Rule:</td>-->
											<td>
											<!--<input type="text" id="fareRule" name="fareRule"/>
											--></td>
										</tr>
										<tr>
											<td>Fare Basis Code:</td>
											<td>
											<select tabindex="11" size="1" name="selBasisCode" id="selBasisCode">
												<option value=""></option>
												<option value="PO0">PO0</option>
												<option value="POWDR">POWDR</option>
												<option value="POWK0R">POWK0R</option>
												<option value="POWKR">POWKR</option>
												<option value="PRWKR">PRWKR</option>
												<option value="SOR">SOR</option>
												<option value="SOWDR">SOWDR</option>
												<option value="SOWK0NR">SOWK0NR</option>
												<option value="SOWK0R">SOWK0R</option>
												<option value="SOWK2R">SOWK2R</option>
												<option value="SOWKR">SOWKR</option>
												<option value="SRR">SRR</option>
												<option value="SRWK0R">SRWK0R</option>
												<option value="SRWK2R">SRWK2R</option>
												<option value="SRWKR">SRWKR</option>
											</select>
											</td>
											<td><!--Booking Class:</td>-->
											<td>
											<!--<input type="text" id="maxFare" name="maxFare"/>
											--></td>
											<td>Max Fare:</td>
											<td>
											<input type="text" id="maxFare" name="maxFare" value="176.00"/>
											</td>
											<td colspan="2" align="right">
											<button tabindex="15" type="button" id="btnSearch" class="ui-state-default ui-corner-all">Search</button>
											</td>
										</tr>
									</tbody>
									</table>
								</td>
								</tr>
							</tbody>
						</table>
					</td>
					
					</tr>
					</tbody>
			</table>
			</div>
			<td class="FormBackGround"></td>
		</tr>
		<tr>
			<td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td>
			<td height="12" class="FormBackGround"><img height="1" width="100%" src="../../images/spacer_no_cache.gif"></td>
			<td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td>
		</tr>
	</tbody></table>
	<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" id="Table4">
		<tbody>
				<tr>
					<td height="20" width="10"><img src="../../images/form_top_left_corner_no_cache.gif"></td>
					<td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader">Fares</font></td>
					<td height="20" width="11"><img src="../../images/form_top_right_corner_no_cache.gif"></td>
				</tr>
				<tr>
					<td class="FormBackGround"></td>
					<td valign="top" class="FormBackGround" style="padding-top: 5px;height: 554px">
					<div class="search-res" style="display:none">
					<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" id="Table4">
						<tbody>
						<tr>
						<td width="20%" valign="top">
						<div class="FormHeadBackGround" style="padding: 4px"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader">Available Fares</font> </div>
							<div id="treeView" style="height: 125px">
								<ul id="tree"></ul>
							</div>
								
						<div>
						<div style="padding: 3px;cursor: pointer;color: #fff;font-weight: bold;" class="banchmark FormHeadBackGround" >
							<img src="../../images/downArrow_no_cache.png" align="absmiddle" class="banchmarkTriger"/> BanchMarking
						</div>
						<table cellspacing="0" cellpadding="0" border="0" align="center" width="179" id="tableBanchmark" 
						style="display:none;position: absolute;">
						<tbody>
						<tr>
							<td width="100%" valign="top" style="background-color:#BCD1ED;">
								<div class="bSearch" style="border-right: 0;height: 235px">
									<table cellspacing="0" cellpadding="2" border="0" align="center" width="100%">
										<tr>
										<td colspan="3"><label style="width:20px">From:</label><input type="text" id="txtFrom" name="txtFrom" size="26" value="DXB" /></td>
										</tr>
										<tr>
										<td colspan="3"><label style="display: block">To:</label><input type="text" id="txtTo" name="txtTo" size="26" value="CDG" /></td>
										</tr>
										<tr>
										<td colspan="3">
										Departing Date:
											<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
											<tr>
											<td>
												<select id="cmbDepMonth" name="cmbDepMonth" size="1" tabindex="9" style="width: 80px">
													<option>Jan</option><option>Feb</option><option>Mar</option><option>Apr</option>
													<option>May</option><option selected="selected">Jun</option><option>Jul</option><option>Aug</option>
													<option>Sep</option><option>Oct</option><option>Nov</option><option>Dec</option>
												</select>
											</td>
											<td>
												<select id="cmbRetDate" name="cmbRetDate" size="1" tabindex="9">
													<option>1</option><option>2</option><option>3</option><option>4</option>
													<option>5</option><option>6</option><option>7</option><option>8</option>
													<option>9</option><option>10</option><option>11</option><option>12</option>
													<option>12</option><option>14</option><option>15</option><option>16</option>
													<option>17</option><option>18</option><option>19</option><option>20</option>
													<option>21</option><option>22</option><option>23</option><option>24</option>
													<option>25</option><option>26</option><option>27</option><option>28</option>
													<option>29</option><option>30</option><option>31</option>
												</select>
											</td>
											<td>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;</td>
											</tr>
											</table>
										</td>
										</tr>
										<tr>
										<td colspan="3">
										Returning Date:
											<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
											<tr>
											<td>
												<select id="cmbRetMonth" name="cmbRetMonth" size="1" tabindex="9" style="width: 80px">
													<option>Jan</option><option>Feb</option><option>Mar</option><option>Apr</option>
													<option>May</option><option selected="selected">Jun</option><option>Jul</option><option>Aug</option>
													<option>Sep</option><option>Oct</option><option>Nov</option><option>Dec</option>
												</select>
											</td>
											<td>
												<select id="cmbRetDate" name="cmbRetDate" size="1" tabindex="9">
													<option>1</option><option>2</option><option>3</option><option>4</option>
													<option>5</option><option>6</option><option>7</option><option>8</option>
													<option>9</option><option>10</option><option>11</option><option>12</option>
													<option>12</option><option>14</option><option>15</option><option>16</option>
													<option>17</option><option>18</option><option>19</option><option>20</option>
													<option>21</option><option>22</option><option>23</option><option>24</option>
													<option>25</option><option>26</option><option>27</option><option>28</option>
													<option>29</option><option>30</option><option>31</option>
												</select>
											</td>
											<td><input type="checkbox" id="return"></input></td>
											</tr>
											</table>
										</td>
										<tr>
										<td>Passenger:<input type="text" id="txtTickets" name="txtTickets" size="6" value="01"/></td>
										<td colspan="2">Class:
											<select id="cmbClass" name="cmbClass" size="1" tabindex="9" style="width: 60px">
												<option value=""></option>
												<option selected="selected" value="Y">Economy Class</option>
											</select>
										</td>
										</tr>
										<tr>
										<td colspan="3">Site:
											<select id="cmbSitestoCap" name="cmbSitestoCap" size="1" tabindex="9" style="width: 148px">
												<option value=""></option>
												<option selected="selected" value="expedia">expedia.com</option>
												<option  value="orbit">orbit.com</option>
											</select></td>
										</tr>
										<tr>
										<td colspan="1">Airlines:
											<select id="cmbAirlines" name="cmbAirlines" size="1" tabindex="9" style="width: 80px">
												<option value="" selected="selected">All</option>
												<option  value="Qutar">Qutar</option>
												<option  value="Emirates">Emirates</option>
												<option  value="TAROM">TAROM</option>
												<option  value="TAROM">Turkish</option>
											</select></td>
											<td colspan="2" valign="bottom">
											&nbsp;
											<button class="ui-state-default ui-corner-all" id="bmSearch">GO</button>
										</td>
										</tr>
									</table>
								</div>
								<div class="recordItems">
								<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
									<tr><td style="background: #B8BDB7;padding: 3px;color: #000" ><b>Track Fares</b></td></tr>
										<tr><td  style="padding: 3px">
											<table cellspacing="0" cellpadding="2" border="0" align="center" width="100%">
												<tr>
												<td>
													<input type="checkbox" name="WeeklyRecord" id="WeeklyRecord" checked="checked"/> <b>Weekly</b>
												</td>
												</tr><tr>
												<td>
													<input type="checkbox" name="SuRecord" id="SuRecord" checked="checked"/> Su
													<input type="checkbox" name="MoRecord" id="MoRecord" checked="checked"/> Mo
													<input type="checkbox" name="TuRecord" id="TuRecord" checked="checked"/> Tu
													<input type="checkbox" name="WeRecord" id="WeRecord" checked="checked"/> We
													<input type="checkbox" name="ThRecord" id="ThRecord" checked="checked"/> Th
													<input type="checkbox" name="FrRecord" id="FrRecord" checked="checked"/> Fr
													<input type="checkbox" name="SaRecord" id="SaRecord" checked="checked"/> Sa
												</td>
												</tr>
												<tr><td style="height: 4px"><hr/></td></tr>
												<tr>
												<td>
													<input type="checkbox" name="WeeklyRecord" id="WeeklyRecord" checked="checked"/> <b>Daily</b>
													<select id="cmbAirlines" name="cmbAirlines" size="1" tabindex="9" style="width: 80px">
														<option value="" selected="selected">Midnight</option>
														<option  value="Qutar">Morning</option>
													</select>
												</td>
												</tr>
												<tr>
												<td>
													<font style="color:#CA944B;font-weight: bold;">Record Untill</font>
													<select id="cmbRecorduntil" name="cmbRecorduntil" size="1" tabindex="9" style="width: 60px">
															<option>Jan</option><option>Feb</option><option>Mar</option><option>Apr</option>
															<option>May</option><option selected="selected">Jun</option><option>Jul</option><option>Aug</option>
															<option>Sep</option><option>Oct</option><option>Nov</option><option>Dec</option>
													</select>
													<input type="button" id="record" class="recordbtn"/>
												</td>
												</tr>
												</table>
										</td></tr>
								</table>
								</div>
							</td>
						</tr>
						</tbody></table>
						</div>
							</td>
							<td width="60%" valign="top">
								<div id="tabs1">
									<ul>
									
									</ul>
								</div>
								<div class="buttonSet" style="display:none">
									<span class="icons menu"></span>
									<span class="icons save"></span>
									<span class="icons copy"></span>
								</div>
								
								<div class="bDetails" style="border-left: 0;width: 1px;display: none;height:180px">
								<div style="width:100%"><a href="javascript:void(0)" style="outline:none"><span title="Save" class="icons save _save" style="float: right;display:none"></span></a><div style="clear:both"></div></div>
								
									<div id="tabs" class="tabs-bottom" style="display: none">
										<ul>
											<li><a href="#tabs-1">Fares</a></li>
											<li><a href="#tabs-2">Graph</a></li>
											<li class="radios" style="float: right;margin: 2px">
												<div>
													<input type="radio" id="currentHistory" name="currentHistory" checked="checked"/> Current
													<input type="radio" id="currentHistory" name="currentHistory"/> History
												</div>
											</li>
										</ul>
										<div id="tabs-1" align="center">
										<div id="mask" style="position: fixed;z-index: 100;background: #fff;width:510px;display: none">
											<table align="center" width="100%" height="118">
												<tr><td valign="middle" align="center" style="text-align: center;">
													<img alt="loading..." src="../../images/ajax-loader-white_no_cache.gif" 
													 style="width: 24px;margin: 45px auto" align="middle">
												</td></tr>
											</table>
										</div>
											<div style="float: left">
											 	<select id="selCurrency">
											 		<option value="GBP" selected="selected">GBP</option>
											 		<%--<option value="USD">USD</option>
											 		<option value="EUR">EUR</option>
											 		<option value="AED">AED</option>--%>
											 	</select>
											 </div>
											 <div style="float: left"><div id="table-data"></div></div>
											 <div style="clear:both"></div>
											 <div>
											 	<table border="0">
											 		<tr>
											 			<td width="20%"><input type="checkbox" id="chkExpedia" /> Expedia</td>
											 			<td width="5%">&nbsp;</td>
											 			<td width="20%"><input type="checkbox" id="chkOrbitz" /> Orbitz</td>
											 			<td width="5%">&nbsp;</td>
											 			<td width="20%"><input type="checkbox" checked="checked" id="chkTravelfusion" /> Travelfusion</td>
											 			<td width="5%">&nbsp;</td>
											 			<td width="15%"><input type="checkbox" id="chkAvarage" /> Avarage</td>
											 		</tr>
											 	</table>
											 </div>
										</div>
										<div id="tabs-2">
											<div id='chart_div'></div>
											<%--<div>
											 	<table border="0">
											 		<tr>
											 			<td width="20%"><input type="checkbox" id="chkExpedia" /> Expedia</td>
											 			<td width="10%">&nbsp;</td>
											 			<td width="20%"><input type="checkbox" id="chkOrbitz" /> Orbitz</td>
											 			<td width="10%">&nbsp;</td>
											 			<td width="20%"><input type="checkbox" checked="checked" id="chkTravelfusion" /> Travelfusion</td>
											 			<td width="10%">&nbsp;</td>
											 			<td width="15%"><input type="checkbox" id="chkAvarage" /> Avarage</td>
											 		</tr>
											 	</table>
											</div>--%>
										</div>
									</div>	
								</div>
							</td>
							
							<td width="20%" valign="top">
								<div class="properties" >
									<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
									<tr><td class="FormHeadBackGround" style="padding: 4px;font-weight: bold;color: #fff" id="pro">
									<img src="../../images/bullet_small_no_cache.gif"/> Properties
									</td></tr>
									<tr><td  valign="top" style="">
										<div class="addProperty" style="border-bottom:1px solid #777;border-right:1px solid #777777;overflow: auto;max-height: 350px;">
							
										</div>
									</td></tr>
									<tr>
									<td class="FormHeadBackGround" style="padding: 4px;font-weight: bold;color: #fff" id="qAction">
										<img src="../../images/bullet_small_no_cache.gif"/> Quick Adjustment
									</td></tr>
									<tr>
									<td class="quickTable">
										<div class="quickTableClass">
										<table cellspacing="0" cellpadding="0" align="center" width="100%"><tbody>
										<tr>
										<td width="30">&nbsp;</td>
										<td><input type="radio" name="Change" id="Change" checked="checked">Selected
										</td>
										<td><input type="radio" name="Change" id="Change"> All
										</td>
										</tr>
										<tr>
										<td colspan="3">
											<div id="tabs2">
												<ul>
													<li><a href="#tabs2-1">Value</a></li>
													<li><a href="#tabs2-2">%</a></li>
												</ul>
												<div id="tabs2-1">
													<table cellspacing="1" width="97%">
													<tr>
														<td>Old</td>
														<td><input type="text" class="oldfare" size="10" readonly="readonly" style="text-align: right" /></td>
													</tr>
													<tr>
														<td>New</td>
														<td><input type="text" size="10" style="text-align: right" /></td>
													</tr>
													<tr>
														<td></td>
														<td><button class="ui-state-default ui-corner-all" id="ddh">Save</button></td></td>
													</tr>
													</table>
												</div>
												<div id="tabs2-2">
													<table cellspacing="1" width="97%">
													<tr>
														<td>Old</td>
														<td><input type="text" class="oldfare" size="10" readonly="readonly" style="text-align: right" /></td>
													</tr>
													<tr>
														<td>New</td>
														<td><input type="text" size="10" style="text-align: right" /></td>
													</tr>
													<tr>
														<td></td>
														<td><button class="ui-state-default ui-corner-all" id="ddh">Save</button></td></td>
													</tr>
													</table>
												</div>
											</div>
											<input type="hidden" id="selectedFare" name="selectedFare"/>
											<input type="hidden" id="selectedSeg" name="selectedSeg"/>
											<input type="hidden" id="selectedClass" name="selectedClass"/>
											<input type="hidden" id="selectedBasis" name="selectedBasis"/>
											<div align="right"><a href="javascript:void(0);" onclick="CWindowOpen(0)">Advanced Adjustment</a></div>
										</td>
										</tr>
										</tbody></table>
										</div>
									</td>
									</tr>
									<tr>
										<td class="FormHeadBackGround" style="padding: 4px;font-weight: bold;color: #fff" id="proAuto">
										<img src="../../images/bullet_small_no_cache.gif"/> Auto Actions
										</td>
									</tr>
									<tr ><td valign="top" style="">
										<div id="proAutoDetails" style="border-bottom: 1px solid rgb(119, 119, 119); border-right: 1px solid rgb(119, 119, 119); overflow: auto; max-height: 400px;display: none;" class="addAuto">
											<div class="singleProp collepse">
											<div class="icon"></div><b id="autoRoot">DXB-CDG</b></div>
											<div class="prop" style="display: block;">
											<table cellspacing="0" cellpadding="0" align="center" width="100%"><tbody>
												<tr><td>From</td><td><input type="text" id="fromRecodrdDate" style="width: 80px"></td></tr>
												<tr><td>To</td><td><input type="text" id="toRecodrdDate" style="width: 80px"></td></tr>
												<tr><td>Day of Op</td><td>
													<input type="checkbox" name="SuRecord1" id="SuRecord1" checked="checked"/>Su
													<input type="checkbox" name="MoRecord1" id="MoRecord1" checked="checked"/>Mo
													<input type="checkbox" name="TuRecord1" id="TuRecord1" checked="checked"/>Tu
													<input type="checkbox" name="WeRecord1" id="WeRecord1" checked="checked"/>We
													<input type="checkbox" name="ThRecord1" id="ThRecord1" checked="checked"/>Th
													<input type="checkbox" name="FrRecord1" id="FrRecord1" checked="checked"/>Fr
													<input type="checkbox" name="SaRecord1" id="SaRecord1" checked="checked"/>Sa
												</td></tr>
												<tr><td>Dep Time (From)</td><td><input type="text" id="fromDepDate" style="width: 80px"></td></tr>
												<tr><td>Dep Time (To)</td><td><input type="text" id="toDepDate" style="width: 80px"></td></tr>
												<tr><td>Flights</td><td>
													<select id="flightRecord" style="width: 80px">
														<option>AB 091</option>
														<option>AB 093</option>
													</select>
												</td></tr>
												<tr><td>Follow</td><td>
													<select id="followRecord" style="width: 80px">
														<option>Lowest Fare</option>
														<option>CR</option>
													</select>
												</td></tr>
												<tr><td>Carrier</td><td>
													<select id="carrierRecord" style="width: 80px">
														<option>EK</option>
														<option>BA</option>
														<option>UA</option>
														<option>QR</option>
														<option>CX</option>
													</select>
												</td></tr>
												<tr><td>Site</td><td>
													<select id="siteRecord" size="1" tabindex="9" style="width: 80px">
														<option value=""></option>
														<option selected="selected" value="expedia">expedia.com</option>
														<option  value="orbit">orbit.com</option>
													</select>
												</td></tr>
												<tr><td>Adjust</td><td>
													<table cellspacing="0" cellpadding="0" align="center" width="100%" border="0"><tr>
													<td>
														<select id="Adjust1Record" style="width: 40px">
															<option>Value</option>
															<option>%</option>
														</select>
													</td>
													<td>
														<select id="Adjust2Record" style="width: 40px">
															<option>High</option>
															<option>Low</option>
													</select>
													</td>
													</tr></table>
												</td></tr>
												<tr><td>Value</td><td><input type="text" id="valueRecord" style="width: 80px"/> </td></tr>
												<tr><td>Curtial BC</td><td>
													<table cellspacing="0" cellpadding="0" align="center" width="100%" border="0"><tr>
													<td>
														<select id="Curtial1Record" style="width: 40px">
															<option>C</option>
															<option>K</option>
															<option>V</option>
															<option>Y</option>
														</select>
													</td>
													<td>
														<select id="Curtial2Record" style="width: 40px">
															<option>C</option>
															<option>K</option>
															<option>V</option>
															<option>Y</option>
														</select>
													</td>
													</tr></table>
												</td></tr>
												<tr><td>Visibility</td>
												<td>
														<select id="VisibilityRecord" style="width: 80px">
															<option>XBE</option>
															<option>IBE</option>
														</select>
												</td></tr>
												</tbody>
											</table>
										</div>
									</div>
									</td></tr>
									
									</table>
								</div>
							</td>
						</tr>
						</tbody>
					</table>
					</div>
					</td>
					<td class="FormBackGround"></td>
					</tr>
		</tbody>
	</table>
</body>
	<script type="text/javascript">

	
		top[2].HideProgress();

		var objWindow1;

		function beforeUnload() {
			if ((objWindow1) && (!objWindow1.closed)) {
				objWindow1.close();
			}
		}
		
		function CWindowOpen(strIndex) {
			
			var segment = $("#selectedSeg").val();
			var farebase = $("#selectedBasis").val();
			var nclass = $("#selectedClass").val();
			var currentFare = $("#selectedFare").val();
			
			if ((objWindow1) && (!objWindow1.closed)) {
				objWindow1.close();
			}
			var intWidth = 0;
			var intHeight = 0;
			var strProp = '';

			var strFilename = ""
			switch (strIndex) {
			case 0:
				strFilename = "showLoadJsp!loadFare360Edit.action?seg="+ segment +"&fbase="+ farebase +"&nclass="+ nclass +"&cfare="+ currentFare;
				intHeight = 480
				intWidth = 800
				break;
			}
			strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
					+ intWidth
					+ ',height='
					+ intHeight
					+ ',resizable=no,top='
					+ ((window.screen.height - intHeight) / 2)
					+ ',left='
					+ (window.screen.width - intWidth) / 2;
			objWindow1 = window.open(strFilename, "CWindow1", strProp);
		}
	</script>
</html>
	