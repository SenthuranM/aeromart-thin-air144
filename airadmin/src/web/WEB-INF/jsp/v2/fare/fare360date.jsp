<%
	String name = request.getParameter("myobj");
%>
<html>
<head>
</head>
<body>
<div style="border: 1px solid #8a8ff2; background: #eff0fb">
<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
	<tbody>
	<tr>
		<td></td>
		<td><input type="checkbox" id="<%= name %>_svcheck" name="<%= name %>_svcheck" class="_chk" checked="checked"> Sales Validity:</td>
		<td><input type="checkbox" id="<%= name %>_dvcheck" name="<%= name %>_dvcheck" class="_chk"> Departure Validity:</td>
		<td><input type="checkbox" id="<%= name %>_rvcheck" name="<%= name %>_rvcheck" class="_chk"> Return Validity:</td>
		<td width="80">&nbsp;</td>						
	</tr>
	<tr>
	<th height="18"><div style="border: 1px solid #777; border-right: 0;height: 18px;background: #99b4d1">F. Level</div></th>
	<td rowspan="2" colspan="4" valign="top">
	<div style="overflow-y: hidden;overflow-x: auto;width:445px;">
	 <table cellspacing="0" cellpadding="0" border="0" align="center" width="1200">
		<tbody>
		<tr>
			<th class="monthsLine"><div style="border: 1px solid #777; border-right: 0;height: 18px;background: #99b4d1">&nbsp;</div></th>
		</tr>
		<tr>
		
		<td class="fareDisplay">
		
			<div class="fares" id="<%= name %>" title="YE3M">
				<span>SVS,2/15/2010, SVE,7/15/2010, DVS,5/6/2010, DVE,8/10/2010, RVS,5/15/2010, RVE,7/15/2010,FV,370</span>
			</div>
			<%--
			<div class="fares" id="<%= name %>FB11" title="FB11">
				<span>SVS,4/15/2010, SVE,9/15/2010, DVS,5/6/2010, DVE,7/19/2010, RVS,5/15/2010, RVE,8/15/2011,FV,170</span>
			</div>
			<div class="fares" id="<%= name %>FB08" title="FB08">
				<span>SVS,1/15/2010, SVE,8/10/2010, DVS,5/6/2010, DVE,12/15/2010, RVS,5/15/2010, RVE,6/15/2010,FV,175</span>
			</div>
			<div class="fares" id="<%= name %>FB03" title="FB03">
				<span>SVS,6/15/2010, SVE,7/30/2010, DVS,5/6/2010, DVE,11/15/2010, RVS,5/15/2010, RVE,9/15/2010,FV,173</span>
			</div>
			<div class="fares" id="<%= name %>FB02" title="FB02">
				<span>SVS,5/15/2010, SVE,10/15/2010, DVS,5/6/2010, DVE,9/30/2010, RVS,5/15/2010, RVE,8/15/2010,FV,172</span>
			</div>
			<div class="fares" id="<%= name %>FB15" title="FB15">
				<span>SVS,3/15/2010, SVE,10/01/2010, DVS,5/6/2010, DVE,7/10/2010, RVS,5/15/2010, RVE,8/15/2010,FV,171</span>
			</div>
			<div class="fares" id="<%= name %>FB31" title="FB31">
				<span>SVS,2/15/2010, SVE,8/02/2010, DVS,5/6/2010, DVE,7/15/2010, RVS,5/15/2010, RVE,6/15/2010,FV,168</span>
			</div>
			<div class="fares" id="<%= name %>FB10" title="FB10">
				<span>SVS,6/15/2010, SVE,8/10/2010, DVS,5/6/2010, DVE,8/15/2010, RVS,5/15/2010, RVE,9/15/2010,FV,166</span>
			</div>
			--%>
		</td></tr>
		</tbody></table>
	</div>
	
	</td>
	</tr>
	<tr>
	<td width="70" class="fareScaler" valign="top"></td>
	</tr>
	</tbody>
	</table>
	</div>
	<div class="cabinClass">
		<span class="select"><a href="javascript:void(0)">F</a></span>
		<span><a href="javascript:void(0)">J</a></span>
		<span><a href="javascript:void(0)">Y</a></span>
	</div>	
	<script type="text/javascript">

	function dateDiff( str1 ) {
	    var diff = Date.parse( str1 );
	   // return diff / 86400000;
	    return isNaN( diff ) ? NaN : {
	    	diff : diff,
	    	ms : Math.floor( diff            % 1000 ),
	    	s  : Math.floor( diff /     1000 %   60 ),
	    	m  : Math.floor( diff /    60000 %   60 ),
	    	h  : Math.floor( diff /  3600000 %   24 ),
	    	d  : Math.floor( diff / 86400000 		)
	    };
	}
	
	function valDate(str1) {
	    var d = new Date();
	    d.setTime(str1);
	    var str = Number(d.getMonth()+1) + "/" + d.getDate() + "/" + d.getFullYear();
	    return str;
   };
	
	//alert(valDate(14753 * 86400000))
	
	var pixelPerDay = 2;
	var dayPerYear = 360;
	var dayPerMonth = 30;
	var borderSpace = 1;
	
	var range = 0;
	var noOfUnits = 10;
	var minFareVal = 310;
	var maxFareVal = 410;

	
	function monthsLine(space) {
		var pixPreMonth = (dayPerMonth * pixelPerDay);
		var avaiMon = parseInt((parseInt(space))/pixPreMonth-borderSpace);
		var htm = "";
		var dat = new Date();
		var thismonth = Number(dat.getMonth()-1)
		for (var i = 0; i< avaiMon; i++){
			if (jsYears[thismonth].month == "Dec"){
				thismonth = 0;
			}else{
				thismonth = thismonth + 1;
			}
				if (i == avaiMon-1)
					htm += "<div id='"+jsYears[thismonth].month+"' class='monthLineClass lastMonth' style='width:"+pixPreMonth+"px'>"+jsYears[thismonth].month+"</div>";
				else
					htm += "<div id='"+jsYears[thismonth].month+"' class='monthLineClass' style='width:"+pixPreMonth+"px'>"+jsYears[thismonth].month+"</div>";
			
		}
		return htm;
	}

	function fareScale(height ,w) {
		range = (maxFareVal - minFareVal)/noOfUnits;
		var unitsPerPix = parseInt((parseInt(height))/noOfUnits);
		var htm = "";
		for (var i = noOfUnits; i >= 0; i--){
			if (w == "fareScal")
				htm += "<div id='"+w+Number(minFareVal+(i*range))+"' class='"+w+"border' style='height:"+unitsPerPix+"px'>"
				+Number(minFareVal+(i*range))+"</div>";
			else
				htm += "<div id='"+w+Number(minFareVal+(i*range))+"' class='"+w+"border' style='height:"+unitsPerPix+"px'></div>";
		}
		return htm;
	}

	$("#load-<%= name %> .monthsLine").prepend(monthsLine('1250px'));
	$("#load-<%= name %> td.fareScaler").html(fareScale('210px', 'fareScal'));
	$("#load-<%= name %> td.fareDisplay").prepend(fareScale('210px', 'fareVal'));
	//alert(Math.round(months(parseInt($(".monthsLine").css("width")))));
	$("#load-<%= name %> .fares").fareslider({
		parent: '<%= name %>',
		breaker:'span',
		background:"red,green,bule",
		stDate:"5/1/2010",
		edDate:"12/30/2011"
	});
	
	
	</script>
</body>
</html>