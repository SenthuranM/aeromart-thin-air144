<%
	String segment = request.getParameter("seg");
	String fareBase = request.getParameter("fbase");
	String nClass = request.getParameter("nclass");
	String currentFare = request.getParameter("cfare");
%>
<html>
<head>
<title>Advanced Fare Adjustment</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    
	<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>
	<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/jquery.multiselect2side_no_cache.css"/>
	<LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css"/>
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>
	<LINK rel="stylesheet" type="text/css" href="../../css/fare360_no_cache.css"/>
	
	<script type="text/javascript" src="../../js/v2/fare/countryCity.js"></script>
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="http://www.google.com/jsapi"></script>
	
	<style type="text/css">
	.label{text-align: left;font-weight: bold;}
	th{padding-left: 15px}
	</style>
	<script type="text/javascript">
	google.load('visualization', '1', {packages: ['table','corechart']});

	    //google.setOnLoadCallback(drawVisualization);
	    $(function () {

	    	function hideLoading(){
	    		$("#mask").hide();
	    	};

	    	function showLoading(){
	    		hideLoading();
	    		$("#mask").show();
	    		setTimeout("$('#mask').hide();",2000);
	    	};
	    	
	    	$('#txtStartDate').datepicker({
				changeMonth: true,
				changeYear: true,
				showButtonPanel: true
			});

			$('#txtEnddate').datepicker({
				changeMonth: true,
				changeYear: true,
				showButtonPanel: true
			});
			$('#txtAdjustment').keypress(function(e){
				var code = (e.keyCode ? e.keyCode : e.which);
				var typ = $('#selAdjustedBy').val()
				 if(code == 13)
					 changeFare(typ);
			});

			function changeFare(t){
				var curre = parseFloat($('#txtCuttentFare').val());
				var adjs = parseFloat($('#txtAdjustment').val());
				var newf = null;
				if (t == 'percentage')
					newf = curre + ((curre * adjs)/100);
				else
					newf = curre +  adjs;
			
				$('#lblNewFare').html(addDecimal(newf));
			};
			
			function addDecimal(val){
				var decimal = String(val).split(".");
				if (decimal[1]== undefined)
					val = String(val) + ".00"
				else if (decimal[1].length == 1)
					val = String(val) + "0"
					
				return val;
			};
			$("#bmSearch1, #bmSearchv").click(function(){
				window.close();
			});
			
			$("input[type='radio']").click(function(){
				//alert($("input[name='chkChannelRegion']:checked").val())
				var channel = $("input[name='chkChannelRegion']:checked").val()
				var dailyMonthly = $("input[name='chkDailyMonthly']:checked").val()
				var title = null;
				if (channel == 'Channel' && dailyMonthly == 'Monthly'){
					title = 'Monthly View';
					monthlyChannel(title);
				}else if (channel == 'Channel' && dailyMonthly == 'Daily'){
					title = 'Daily View';
					dailyChannel(title);
				}else if (channel == 'Region' && dailyMonthly == 'Monthly'){
					title = 'Monthly View';
					MonthlyRegion(title);
				}else{
					title = 'Daily View';
					dailyRegion(title);
				}
			});

			function drawVisualization(data, view) {
			      // Create and populate the data table.
				  var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
			       chart.draw(data, {width: 520, height: 180, title: view});
		    };

	    	function drawVisualizationMonthly(data, view) {
		      // Create and populate the data table.
			  var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
		       chart.draw(data, {width: 520, height: 180, title: view, hAxis:{title : '%'}});
		    };
			
			function dailyRegion(title){
				showLoading();
				var dataDaily = new google.visualization.DataTable();
      			dataDaily.addColumn('string', 'Region');
		        dataDaily.addColumn('number', 'GDS');
		        dataDaily.addColumn('number', 'WEB');
		        dataDaily.addColumn('number', 'XBE');
		        dataDaily.addRows(4);
		        dataDaily.setValue(0, 0, 'Europe');
		        dataDaily.setValue(0, 1, 68);
		        dataDaily.setValue(0, 2, 80);
		        dataDaily.setValue(0, 3, 70);
		        
		        dataDaily.setValue(1, 0, 'Asia');
		        dataDaily.setValue(1, 1, 55);
		        dataDaily.setValue(1, 2, 69);
		        dataDaily.setValue(1, 3, 58);
		        
		        dataDaily.setValue(2, 0, 'M.E.');
		        dataDaily.setValue(2, 1, 66);
		        dataDaily.setValue(2, 2, 85);
		        dataDaily.setValue(2, 3, 45);
		        
		        dataDaily.setValue(3, 0, 'Africa');
		        dataDaily.setValue(3, 1, 59);
		        dataDaily.setValue(3, 2, 68);
		        dataDaily.setValue(3, 3, 75);
		        drawVisualization(dataDaily,title);
			};
			
			function dailyChannel(title){
				showLoading();
				var dataDaily = new google.visualization.DataTable();
      			dataDaily.addColumn('string', 'Channel');
		        dataDaily.addColumn('number', 'Europe');
		        dataDaily.addColumn('number', 'Asia');
		        dataDaily.addColumn('number', 'M.E.');
		        dataDaily.addColumn('number', 'Africa');
		        
		        dataDaily.addRows(3);
		        dataDaily.setValue(0, 0, 'GDS');
		        dataDaily.setValue(0, 1, 80);
		        dataDaily.setValue(0, 2, 78);
		        dataDaily.setValue(0, 3, 75);
		        dataDaily.setValue(0, 4, 35);
		        
		        dataDaily.setValue(1, 0, 'WEB');
		        dataDaily.setValue(1, 1, 55);
		        dataDaily.setValue(1, 2, 65);
		        dataDaily.setValue(1, 3, 68);
		        dataDaily.setValue(1, 4, 48);
		        
		        dataDaily.setValue(2, 0, 'XBE');
		        dataDaily.setValue(2, 1, 15);
		        dataDaily.setValue(2, 2, 84);
		        dataDaily.setValue(2, 3, 35);
		        dataDaily.setValue(2, 4, 80);

		        drawVisualization(dataDaily,title);
				
			};
			
			function MonthlyRegion(title){
				showLoading();
				var dataDaily = new google.visualization.DataTable();
      			dataDaily.addColumn('string', 'Region');
      			dataDaily.addColumn('number', 'GDS');
		        dataDaily.addColumn('number', 'WEB');
		        dataDaily.addColumn('number', 'XBE');
		        dataDaily.addRows(4);
		        dataDaily.setValue(0, 0, 'Europe');
		        dataDaily.setValue(0, 1, 10);
		        dataDaily.setValue(0, 2, 60);
		        dataDaily.setValue(0, 3, 30);
		        
		        dataDaily.setValue(1, 0, 'Asia');
		        dataDaily.setValue(1, 1, 66);
		        dataDaily.setValue(1, 2, 22);
		        dataDaily.setValue(1, 3, 12);
		        
		        dataDaily.setValue(2, 0, 'M.E.');
		        dataDaily.setValue(2, 1, 45);
		        dataDaily.setValue(2, 2, 15);
		        dataDaily.setValue(2, 3, 40);
		        
		        dataDaily.setValue(3, 0, 'Africa');
		        dataDaily.setValue(3, 1, 40);
		        dataDaily.setValue(3, 2, 10);
		        dataDaily.setValue(3, 3, 50);
		        drawVisualizationMonthly(dataDaily,title);
			};
			
			function monthlyChannel(title){
				showLoading();
				var dataDaily = new google.visualization.DataTable();
      			dataDaily.addColumn('string', 'Channel');
		        dataDaily.addColumn('number', 'Europe');
		        dataDaily.addColumn('number', 'Asia');
		        dataDaily.addColumn('number', 'M.E.');
		        dataDaily.addColumn('number', 'Africa');
		        
		        dataDaily.addRows(3);
		        dataDaily.setValue(0, 0, 'GDS');
		        dataDaily.setValue(0, 1, 10);
		        dataDaily.setValue(0, 2, 20);
		        dataDaily.setValue(0, 3, 30);
		        dataDaily.setValue(0, 4, 50);
		        
		        dataDaily.setValue(1, 0, 'WEB');
		        dataDaily.setValue(1, 1, 20);
		        dataDaily.setValue(1, 2, 30);
		        dataDaily.setValue(1, 3, 30);
		        dataDaily.setValue(1, 4, 20);
		        
		        dataDaily.setValue(2, 0, 'XBE');
		        dataDaily.setValue(2, 1, 15);
		        dataDaily.setValue(2, 2, 25);
		        dataDaily.setValue(2, 3, 30);
		        dataDaily.setValue(2, 4, 30);

		        drawVisualizationMonthly(dataDaily,title);
			};
			dailyChannel('Daily View');

			
	    });
	</script>
</head>
<body>
	<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" id="Table4">
		<tbody>
		<tr>
			<td height="20" width="10"><img src="../../images/form_top_left_corner_no_cache.gif"></td>
			<td height="20" class="FormHeadBackGround">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr><td width="880"><font class="FormHeader"><img src="../../images/bullet_small_no_cache.gif"> Fare Assessment</font></td>
				<td align="right"></td>
				</tr></table>
				
			</td>
			<td height="20" width="11"><img src="../../images/form_top_right_corner_no_cache.gif"></td>
		</tr>
		<tr>
		<td class="FormBackGround"></td>
		<td class="FormBackGround">
			<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
				<tr><td style="height:10px">&nbsp;</td></tr>
				<tr>
					<td width="30%">
						<table cellspacing="0" cellpadding="2" border="0" align="center" width="100%" style="margin: 3px 1px">
							<tr>
								<th colspan="2" align="left">Summary</th>
							</tr>
							<tr>
								<td width="37%" class="label">Segment : </td>
								<td width="62%"><%= segment %></td>
							</tr>
							<tr>
								<td class="label">Fare Basis : </td>
								<td><%= fareBase %></td>
							</tr>
							<tr>
								<td class="label">Nested Class : </td>
								<td><%= nClass %></td>
							</tr>
						</table>
						
						<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="margin: 3px 1px">
							<tr>
								<th align="left">Forwarded Bookings</th>
							</tr>
							<tr>
								<td><input type="radio" name="chkDailyMonthly" value="Daily" checked="checked"/> Daily  </td>
							</tr>
							
							<tr>
								<td><input type="radio" name="chkDailyMonthly" value="Monthly"/> Monthly</td>
							</tr>
						</table>
						
						<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="margin: 3px 1px">
							<tr>
								<th align="left">Filter Sales By</th>
							</tr>
							<tr>
								<td><input type="radio" name="chkChannelRegion" value="Channel" checked="checked"/> Channel  </td>
							</tr>
							
							<tr>
								<td><input type="radio" name="chkChannelRegion" value="Region"/> Region/Channel</td>
							</tr>
						</table>
					</td>
					<td width="70%">
					<div id="mask" style="position: fixed;z-index: 100;background: #fff;width:520px;display: none">
							<table align="center" width="100%" height="180">
								<tr><td valign="middle" align="center" style="text-align: center;">
									<img alt="loading..." src="../../images/ajax-loader-white_no_cache.gif" 
									 style="width: 24px;margin: 80px auto" align="middle">
								</td></tr>
							</table>
					</div>
					<div id='chart_div'></div></td>
				</tr>
			</table> 
		</td>
		<td class="FormBackGround"></td>
		</tr>
		<tr>
			<td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td>
			<td height="12" class="FormBackGround"><img height="1" width="100%" src="../../images/spacer_no_cache.gif"></td>
			<td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td>
		</tr>
	</tbody></table>
	
	<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" id="Table4">
		<tbody>
		<tr>
			<td height="20" width="10"><img src="../../images/form_top_left_corner_no_cache.gif"></td>
			<td height="20" class="FormHeadBackGround">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr><td width="880"><font class="FormHeader"><img src="../../images/bullet_small_no_cache.gif"> Fare Adjustment</font></td>
				<td align="right"></td>
				</tr></table>
				
			</td>
			<td height="20" width="11"><img src="../../images/form_top_right_corner_no_cache.gif"></td>
		</tr>
		<tr>
		<td class="FormBackGround"></td>
		<td class="FormBackGround">
			<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
			<tr>
				<td>
				
					<table cellspacing="0" cellpadding="2" border="0" align="center" width="100%" style="margin: 3px 1px">
						<tr>
							<td width="15%"> Current Fare : </td>
							<td width="8%"><input type="text" id="txtCuttentFare" readonly="readonly" style="text-align: right;" size="15" value="<%= currentFare %>"/>  </td>
							<td width="2%">&nbsp;</td>
							<td width="15%"> Adjusted By : </td>
							<td width="10%">
								<select id="selAdjustedBy">
									<option value="value">Value</option>
									<option value="percentage">Percentage (%)</option>
								</select>
							</td>
							<td width="2%">&nbsp;</td>
							<td width="15%"> Adjustment : </td>
							<td width="10%"><input type="text" id="txtAdjustment" style="text-align: right;" size="15"/> </td>
							<td width="2%">&nbsp;</td>
							<td width="15%"> New Fare :  </td>
							<td width="10%"><span id="lblNewFare" style="text-align: right;"><%= currentFare %></span>  </td>
						</tr>
						
						<tr>
							<td> Start Date :</td>
							<td><input type="text" id="txtStartDate" name="txtStartDate" size="15" readonly="readonly"/></td>
							<td width="2%">&nbsp;</td>
							<td> End Date :</td>
							<td><input type="text" id="txtEnddate" name="txtStartDate" size="15" readonly="readonly"/></td>
							<td width="2%">&nbsp;</td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td colspan="3"> Visibility</td>
							<td colspan="7"> Comments:</td>
							<td></td>
						</tr>
						<tr>
							<td colspan="1">
								<input type="checkbox" id="chkAll" checked="checked"/> ALL </br>
								<input type="checkbox" id="chkWeb" checked="checked"/> WEB </br>
								<input type="checkbox" id="chkXBE" checked="checked"/> XBE </br>
								<input type="checkbox" id="API" checked="checked"/> API </br>
								<input type="checkbox" id="GDS" checked="checked"/> GDS </br>
							</td>
							<td valign="top" colspan="2">
								<input type="checkbox" id="chkSABRE" checked="checked"/> SABRE </br>
								<input type="checkbox" id="GALILEO" checked="checked"/> GALILEO </br>
								<input type="checkbox" id="ABACUS" checked="checked"/> ABACUS </br>
							
							</td>
							<td colspan="7">
								<textarea rows="5" cols="65"></textarea>
							</td>
							<td></td>
						</tr>
						<tr>
							<td colspan="4">
								<input type="checkbox" id="chkNotifyXBE" checked="checked"/> Notify change by e-mail to XBE users
							</td>
							<td colspan="4">
								<input type="checkbox" id="chkPdf" checked="checked"/> Include PDF latter
							</td>
							<td colspan="3"></td>
						</tr>
						<tr>
						<td colspan="9"></td>
						<td colspan="2">
							<button id="bmSearch1" class="ui-state-default ui-corner-all">Cancel</button>
							<button id="bmSearchv" class="ui-state-default ui-corner-all">Save</button>
						</td>
						</tr>
					</table>
					
				</td>
			</tr>
			</table> 
		</td>
		<td class="FormBackGround"></td>
		</tr>
		<tr>
			<td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td>
			<td height="12" class="FormBackGround"><img height="1" width="100%" src="../../images/spacer_no_cache.gif"></td>
			<td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td>
		</tr>
	</tbody></table>
</body>
</html>