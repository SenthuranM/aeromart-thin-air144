<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	    <title>Template Page</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="-1">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">		
		<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
		
		<script type="text/javascript">
			var arrMealCategory = new Array();
			<c:out value="${requestScope.reqMeaLCatlist}" escapeXml="false" />
			<c:out value="${requestScope.reqBaseCurrency}" escapeXml="false"/>
			
		</script>
		
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
   	 	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	    <script type="text/javascript" src="../../js/meal/MealTemplate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
 	
 		<script src="../../js/meal/PopUpData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		
 	</head>
  	<body class="tabBGColor" scroll="no"  onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
		 		
  			<script type="text/javascript">			
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />										
			</script>
			
			<div id="divSearchPanel" style="height:30px;text-align:left;">
				<table width="96%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
					<tr>
						<td><font>Meal Template</font></td>
						<td>
							<select name="selTemplate" size="1" id="selTemplate">
								<option value="">All</option>										
								<c:out value="${requestScope.reqMealTempl}" escapeXml="false" />
							  </select>
						</td>
						<td><font>Status</font></td>
						<td>
							<select name="selStatus" size="1" id="selStatus">
								<option value="ACT">Active</option>
								<option value="INA">Inactive</option>
								<option value="ALL">All</option>
							</select>
						</td>									
						<td align="right">									
							<input name="btnSearch" type="button" id="btnSearch" value="Search">
						</td>
					</tr>
				</table>			
			</div>
			<div id="divResultsPanel" style="height:250px;text-align:left;background-color:#ECECEC;">
				<table id="listMealTemp" class="scroll" cellpadding="0" cellspacing="0"></table>
				<div id="temppager" class="scroll" style="text-align:center;"></div>
					
					<u:hasPrivilege privilegeId="sys.mas.mealtempl.add">
						<input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="sys.mas.mealtempl.edit">
						<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="sys.mas.mealtempl.delete">	
						<input name="btnDelete" type="button" id="btnDelete" value="Delete" class="btnDefault">
					</u:hasPrivilege>				
				
			</div>			  		
			<div id="divDispMealTemp" style="height:250px;text-align:left;background-color:#ECECEC;">
				<form method="post" action="showMealTemplate.action" id="frmMeal"> 
				<table width="100%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
					<tr>
						<td width="12%"><font>Template Code</font></td>
						<td><input name="templateCode" type="text" id="templateCode" size="20"  class="UCase" maxlength="20"><font class="mandatory">&nbsp;*</font></td>
					</tr>
					<tr>
						<td><font>Description</font></td>
						<td width="35%"><input name="description" type="text" id="description" size="30" maxlength="30"><font class="mandatory">&nbsp;*</font></td>
						<td width="15%" align="left"><font>Active</font></td>
						<td width="15%" align="left"><input type="checkbox" name="status" id="status" value="ACT"></td>
					</tr>
					<tr>
						<td><font>Currency Code</font>	</td>
						<td>
						<c:choose>
							<c:when test="${requestScope.reqPrivMealLoclCUrr == true}">
								<select name="localCurrCode" size="1" id="localCurrCode" >
										<c:out value="${requestScope.reqCurrencyCombo}" escapeXml="false"/>
							    </select><font class="mandatory">&nbsp;*</font> 
							    <label id="lblLocalCurrencyCode" style="display:none;"></label>
							</c:when>
							<c:otherwise>
								<label id="lblLocalCurrencyCode"></label>
								<input type="text" name="localCurrCode" id="localCurrCode" value="${requestScope.reqBaseCurrency}" style="display: none;" />
							</c:otherwise>						
						</c:choose>    				
								</td>
								<td></td>
								<td></td>
						</tr>					
					<tr>
						<td></td>
						<td valign="top" colspan="4"><table id="listMealChg" class="scroll" cellpadding="0" cellspacing="0"></table>
						</td>
						
					</tr>
					<tr>
						<td></td>
						<td colspan="4">
							<table>
								<tr>
								<td><font>COS</font></td>
								<td colspan="2">
									<select name="cos" size="1" id="cos" style="width: 100px" >	
									<option value=""></option>	
										<c:out value="${requestScope.reqCOSList}" escapeXml="false" />
									</select>
									<font class="mandatory">&nbsp;*</font>
								</td>
								<td valign="top"><font>Meal</font></td>
								<td><select name="selMeal" id="selMeal" size="1" style="width:95;">
									</select><font class="mandatory">&nbsp;*</font>
								</td>
								<td><font>Price</font>
								</td>
								<td><input name="price" id="price" type="text" size="5"
									maxlength="10"><font class="mandatory">&nbsp;*</font>
								</td>
								<td><font>Quantity</font></td>
								<td><input name="allocatedMeal" type="text" id="allocatedMeal" size="3" maxlength="3" ><font class="mandatory">&nbsp;*</font></td>
								<td><font>Popularity</font></td>
								<td><input name="popularity" type="text" id="popularity" size="3" maxlength="3" ><font class="mandatory">&nbsp;*</font></td>
								<td><font>Status</font></td>								
								<td><select name="mstatus" id="mstatus" size="1" style="width:40;">
										<option value="ACT">Active</option>
										<option value="INA">In-Active</option>										
									</select><font class="mandatory">&nbsp;*</font>
								</td>
								<td>		
									<input name="btnChgAdd" type="button" id="btnChgAdd" value="Add" class="btnDefault">				
									<input name="btnChgEdit" type="button" id="btnChgEdit" value="Change" class="btnDefault">									
								</td>
								</tr>
							
							</table>
						</td>						
					</tr>				
			 		<tr>
						<td colspan="2" style="height:25px;"  valign="bottom">
							<input name="btnClose" type="button"  id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault">
						    <input name="btnReset" type="button"  id="btnReset" value="Reset" class="btnDefault">
						</td>
						<td align="right" valign="bottom" colspan="3">							
							<input name="btnRestrict" type="button"  id="btnRestrict" value="Meal Restriction" class="btnDefault" style="width:150px;">
							<input name="btnEdtPop" type="button"  id="btnEdtPop" value="Edit Popularity" class="btnMedium">
							<input name="btnSave" type="button"  id="btnSave" value="Save" class="btnDefault">
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="version"  id="version" style="display: none;" />
							<input type="text" name="templateId"  id="templateId" style="display: none;" />							
							<input type="text" name="createdBy"  id="createdBy" style="display: none;" />
							<input type="text" name="createdDate"  id="createdDate" style="display: none;" />
							<input type="text" name="mealCharge"  id="mealCharge" style="display: none;" />
							<input type="text" name="mealCharges"  id="mealCharges" style="display: none;" />
							<input type="text" name="mealId"  id="mealId" style="display: none;" />
							<input type="text" name="meal"  id="meal" style="display: none;" />
							<input type="text" name="chargeId"  id="chargeId" style="display: none;" />							
							<input type="text" name="mId"  id="mId" style="display: none;" />
							<input type="text" name="amount"  id="amount" style="display: none;" />
							<input type="text" name="allocatedMeal"  id="allocatedMeal" style="display: none;" />
							<input type="text" name="mversion"  id="mversion" style="display: none;" />
							<input type="text" name="rowNo"  id="rowNo" style="display: none;" />
							<input type="text" name="multiRestrictions"  id="multiRestrictions" style="display: none;" />							
						</td>
				   </tr>
			  </table>					
			  </form>
			</div>	
	<script>
		var screenId = 'SC_ADMN_031';
		top[2].HideProgress();
	</script>
  </body>
</html>