<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	    <title>Meal Category Page</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">		
		<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
		
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
				
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
    	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.formfileupload.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		
		<script type="text/javascript" src="../../js/meal/MealCategory.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		
		<script src="../../js/meal/PopUpData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		   
		<script type="text/javascript">	
		    var mlCategoryImagePath = "";
			var languageHTML = "<c:out value="${requestScope.reqLanguageList}" escapeXml="false" />";
			var listForDisplayEnabled = <c:out value="${requestScope.reqListForDisplayEnabled}" escapeXml="false" />;
		</script>
 	</head>
  	<body class="tabBGColor" scroll="no"  onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
		 		
  			<script type="text/javascript">			
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />										
			</script>
			
			<div id="divSearch" style="height:30px;text-align:left;">
				<table width="96%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
					<tr>
						<td width="10%"><font>Meal Category Name</font></td>
						<td width="12%"><input name="srchMealCategoryName" type="text" id="srchMealCategoryName" size="15"  maxlength="50" onkeyUp="alphaWhiteSpaceValidate(this)" onkeyPress="alphaWhiteSpaceValidate(this)"><font class="mandatory">&nbsp;*</font></td>
						<td width="2%">&nbsp;</td>							
						<td align="right" width="8%">									
							<input name="btnSearch" type="button" id="btnSearch" value="Search">
						</td>
					</tr>
				</table>			
			</div>
			
			<div id="divResultsPanel" style="height:300px;text-align:left;background-color:#ECECEC;">
				<table id="listMealCategory" class="scroll" cellpadding="0" cellspacing="0"></table>
				<div id="mealCategoryPager" class="scroll" style="text-align:center;"></div>
					
					<u:hasPrivilege privilegeId="sys.mas.meal.category.add">
						<input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="sys.mas.meal.category.edit">
						<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="sys.mas.meal.category.delete">	
						<input name="btnDelete" type="button" id="btnDelete" value="Delete" class="btnDefault">
					</u:hasPrivilege>		
					<u:hasPrivilege privilegeId="sys.mas.meal.category.add">	
						<input name="btnAddDescription" type="button" id="btnAddDescription" value="Add Description" class="btnDefault">				
					</u:hasPrivilege>






				
			</div>			  		
			<div id="divDispMealCategory" style="height:170px;text-align:left;background-color:#ECECEC;">
				<form method="post" action="showMealCategory.action" id="frmMealCategory" enctype="multipart/form-data"> 
				<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
						<tr><td>&nbsp;</td></tr>					
						<tr>
						<td width="15%"><font>Meal Category Name</font></td>
						<td colspan="2"><input name="mealCategoryName" type="text" id="mealCategoryName" size="20"  maxlength="50" onkeyUp="alphaWhiteSpaceValidate(this)" onkeyPress="alphaWhiteSpaceValidate(this)"><font class="mandatory">&nbsp;*</font></td>
						<td rowspan="6"><img  name="imgMealCategoryThamb" id= "imgMealCategoryThamb" src="" alt="" height="150"> &nbsp;&nbsp;  <img  name="imgMealCategory" id= "imgMealCategory" src="" alt="" height="150"> </td>
					</tr>
					<tr style="display:none">
						<td><font>Image</font></td>
						<td width="20%"><div id="dvUpLoad"><input type="file" name="mealCategoryImage">&nbsp;</div></td>
						<td align="left" valign="top"> &nbsp;&nbsp;<input name="btnUpload" type="button"  id="btnUpload" value="Upload" ></td>
						
					</tr>
					<tr style="display:none">
						<td><font> Thumbnail image  </font></td>
						<td width="20%"><div id="dvUpLoadThum"><input type="file" name="mealCategoryThumbnailImage">&nbsp;</div></td>
						<td align="left" valign="top"> &nbsp;&nbsp;<input name="btnUploadThumbnail" type="button"  id="btnUploadThumbnail" value="Upload" ></td>
						
					</tr>
			 		<tr>
						<td colspan="3" style="height:42px;"  valign="bottom">
							<input name="btnClose" type="button"  id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault">
						    <input name="btnReset" type="button"  id="btnReset" value="Reset" class="btnDefault">
						</td>
						<td align="right" valign="bottom">
							<input name="btnSave" type="button"  id="btnSave" value="Save" class="btnDefault">
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="version"  id="version" style="display: none;" />
							<input type="text" name="mealCategoryId"  id="mealCategoryId" style="display: none;" />	
							<input type="text" name="createdBy"  id="createdBy" style="display: none;" />
							<input type="text" name="createdDate"  id="createdDate" style="display: none;" />						
							<input type="text" name="uploadImage"  id="uploadImage" style="display: none;" />
							<input type="text" name="uploadImageThumbnail"  id="uploadImageThumbnail" style="display: none;" />
							<input type="text" name="rowNo"  id="rowNo" style="display: none;" />						
						</td>
				   </tr>
			  </table>		
			  <input type="hidden" name="hdnMode" id="hdnMode" value="Mode">	
			  <input type="hidden" name="hdnModel" id="hdnModel" value="">		
			  <input type="hidden" name="mealCatDescForDisplay" id="mealCatDescForDisplay" value="">  
			  </form>			 
			</div>	
	<script>
		var screenId = 'SC_ADMN_035';
		top[2].HideProgress();
	</script>
  </body>
</html>