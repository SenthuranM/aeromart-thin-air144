<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>OND Baggage Template Page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css"
	href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css"
	href="../../themes/default/css/ui.all_no_cache.css">

<script
	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script
	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script	src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
    type="text/javascript"></script>
    <script type="text/javascript"
	src="../../js/baggage/ONDBaggageTemplate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

</head>
<body class="tabBGColor" scroll="no"
	onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false"
	onkeydown='return Body_onKeyDown(event)' ondrag='return false'>

	<script type="text/javascript">
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
		var baseCurrency = '<c:out value="${requestScope.reqBaseCurrency}" escapeXml="false" />';
		var showInLocalCurrency = '<c:out value="${requestScope.reqPrivBgLoclCUrr}" escapeXml="false" />';
	</script>

	<div id="divSearchPanel" style="height: 25px; text-align: left;padding-top:5px">
		<table width="96%" border="0" cellpadding="0" cellspacing="2"
			ID="Table9">
			<tr>
				<td colspan="2"><font>OND Baggage Template</font>&nbsp;
                <select name="selTemplate" size="1" id="selTemplate">
						<option value="">All</option>
						<c:out value="${requestScope.reqONDBaggageTempl}" escapeXml="false" />
				</select>
				</td>
				<td colspan="2"><font>Status</font>&nbsp;<select name="selStatus" size="1" id="selStatus">
						<option value="ACT">Active</option>
						<option value="INA">Inactive</option>

						<option value="ALL">All</option>
				</select>
				</td>
				<td align="right"><input name="btnSearch" type="button"
					id="btnSearch" value="Search"></td>
			</tr>
		</table>
	</div>
	<div id="divResultsPanel"
		style="height: 250px; text-align: left; background-color: #ECECEC;">
		<table id="listBaggageTemp" class="scroll" cellpadding="0"
			cellspacing="0"></table>
		<div id="temppager" class="scroll" style="text-align: center;"></div>
		
		<!--TODO New privileges should be added to the privileges table-->
		<u:hasPrivilege privilegeId="sys.mas.ondbaggagetempl.add">
			<input name="btnAdd" type="button" id="btnAdd" value="Add"
				class="btnDefault">
		</u:hasPrivilege>
		<u:hasPrivilege privilegeId="sys.mas.ondbaggagetempl.edit">
			<input name="btnEdit" type="button" id="btnEdit" value="Edit"
				class="btnDefault">
		</u:hasPrivilege>
		<u:hasPrivilege privilegeId="sys.mas.ondbaggagetempl.delete">
			<input name="btnDelete" type="button" id="btnDelete" value="Delete"
				class="btnDefault">
		</u:hasPrivilege>
		<u:hasPrivilege privilegeId="sys.mas.ondbaggagetempl.copy">
			<input name="btnCopy" type="button" id="btnCopy" value="Copy" class="btnDefault">
		</u:hasPrivilege>
	</div>
	<div id="divDispBaggageTemp"
		style="text-align: left; background-color: #ECECEC;">
		<form method="post" action="showONDBaggageTemplate.action"
			id="frmBaggage">
			<table width="98%" border="0" cellpadding="0" cellspacing="2"
				align="center" ID="Table1">
				<tr>
					<td width="15%"><font>Template Code</font>
					</td>
					<td><input name="templateCode" type="text" id="templateCode"
						size="20" class="UCase" maxlength="20"><font
						class="mandatory">&nbsp;*</font>
					</td>
				</tr>
				<tr>
					<td><font>Description</font>
					</td>
					<td><input name="description" type="text" id="description"
						size="30" maxlength="30"><font class="mandatory">&nbsp;*</font>
					</td>
					
					<td><font>Active</font>
					</td>
					<td><input type="checkbox" name="status" id="status"
						value="ACT">
					</td>
				</tr>
				
				<tr>
					<td align="left"><font>From Date </font></td>
								<td align="left">
									<input tabindex="1" type="text" name="fromDateTmp" id="fromDateTmp" size="12" maxlength="10" invalidText="true"  onBlur="validateAndPopulate('fromDateTmp')" />
									<font class="mandatory">&nbsp;*</font>
	                            </td>
								<td align="left"><font>To Date</font></td>
								<td align="left">
									<input tabindex="2" type="text" name="toDateTmp" id="toDateTmp" size="12" maxlength="10" invalidText="true"  onBlur="validateAndPopulate('toDateTmp')"/>
									<font class="mandatory">&nbsp;*</font> 
								</td>
				</tr>
				<tr>
					<td><font>Currency Code</font>	</td>
					<td>
					<c:choose>
						<c:when test="${requestScope.reqPrivBgLoclCUrr == true}">
							<select name="localCurrCode" size="1" id="localCurrCode" >
									<c:out value="${requestScope.reqCurrencyCombo}" escapeXml="false"/>
						    </select><font class="mandatory">&nbsp;*</font> 
						    <label id="lblLocalCurrencyCode" style="display:none;"></label>
						</c:when>
						<c:otherwise>
							<label id="lblLocalCurrencyCode"></label>
							<input type="text" name="localCurrCode" id="localCurrCode" style="display: none;" ></input>              
						</c:otherwise>						
					</c:choose>    				
							</td>
							<td></td>
							<td></td>
							</tr>
						
								
				<tr>
					<td></td>
					<td valign="top" colspan="3"><table id="listBaggageChg"
							class="scroll" cellpadding="0" cellspacing="0"></table></td>

				</tr>
				<tr>
					<td>&nbsp;</td>
					<td colspan="3">
						<table border="0" cellpadding="0" cellspacing="2" width="100%">
							<tr>
								<td><font>Class of Service</font></td>
								<td>
									<select name="cos" size="1" id="cos" style="width: 100px" >	
										<c:out value="${requestScope.reqCOSList}" escapeXml="false" />
									</select>
									<font class="mandatory">&nbsp;*</font>
								</td>
								<td valign="top"><font>Baggage</font>
								</td>
								<td><select name="selBaggage" id="selBaggage" size="1"
									style="width: 105;">
										<option value=""></option>										
								</select><font class="mandatory">&nbsp;*</font></td>
								<td><font>Amount</font>
								</td>
								<td><input name="amount" id="amount" type="text" size="5"
									maxlength="10"><font class="mandatory">&nbsp;*</font>
								</td>
								<!-- td><font>Status</font></td>
								<td><select name="mstatus" id="mstatus" size="1"
									style="width: 70px;">
										<option value=""></option>
										<option value="ACT">Active</option>
										<option value="INA">In-Active</option>
								</select><font class="mandatory">&nbsp;*</font></td-->
								<td><font>Default</font>&nbsp;<input type="checkbox" name="mdefault" id="mdefault" value="Y"></td>						
								<td></td>
								<td></td>
							</tr>		
							<tr>
								<td><font>Seat Factor</font></td>
								<td><input name="seatFactor" id="seatFactor" type="text" size="5"
									maxlength="10"><font class="mandatory">&nbsp;*</font></td>
								<td><font>Total Weight</font></td>
								<td><input name="totalWeight" id="totalWeight" type="text" size="5"
                                    maxlength="10"><font class="mandatory">&nbsp;*</font></td>
								<td><font>Seat Factor Apply For</font></td>
								<td>
									<select name="seatFactorApplyFor" size="1" id="seatFactorApplyFor" style="width: 100px" >	
										<c:out value="${requestScope.reqSeatFactorApplyFor}" escapeXml="false" />
									</select>
									<font class="mandatory">&nbsp;*</font>
								</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>																
								<td style="text-align:right"><input name="btnChgAdd" type="button" id="btnChgAdd"
									value="Add" class="btnDefault"> <input
									name="btnChgEdit" type="button" id="btnChgEdit" value="Change"
									class="btnDefault"></td>
							</tr>						
						</table></td>
				</tr>
				<tr>
					<td colspan="3" style="height: 25px;" valign="bottom"><input
						name="btnClose" type="button" id="btnClose" value="Close"
						onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault">
						<input name="btnReset" type="button" id="btnReset" value="Reset"
						class="btnDefault"></td>
					<td align="right" valign="bottom"><input name="btnSave"
						type="button" id="btnSave" value="Save" class="btnDefault">
					</td>
				</tr>
				<tr>
					<td><input type="text" name="createdBy" id="createdBy"
						style="display: none;" /> <input type="text" name="createdDate"
						id="createdDate" style="display: none;" /> <input type="text"
						name="chargeId" id="chargeId" style="display: none;" /> <input
						type="text" name="rowNo" id="rowNo" style="display: none;" /> <input
						type="text" name="recId" id="recId" style="display: none;" /> <input
						type="text" name="baggage" id="baggage" style="display: none;" />
						<input type="text" name="chargeId" id="chargeId"
						style="display: none;" /> <input type="text" name="templateId"
						id="templateId" style="display: none;" /> <input type="text"
						name="baggageId" id="baggageId" style="display: none;" /> <input
						type="text" name="bagAmount" id="bagAmount" style="display: none;" />
						<input type="text" name="allocPieces" id="allocPieces"
						style="display: none;" /> <input type="text" name="version"
						id="version" style="display: none;" /> <input type="text"
						name="bagVersion" id="bagVersion" style="display: none;" /> <input
						type="text" name="bagStatus" id="bagStatus" style="display: none;" />
						<input type="text" name="cabinClass" id="cabinClass"
						style="display: none;" /> <input type="text"
						name="baggageCharges" id="baggageCharges" style="display: none;" />
						<input type="text" name="baggageCharge" id="baggageCharge"
						style="display: none;" />
						<input type="text" name="defaultBaggage" id="defaultBaggage"
						style="display: none;" />
					</td>
				</tr>
			</table>
		</form>
	</div>
	<script>
		var screenId = 'SC_ADMN_039';
		top[2].HideProgress();
	</script>
</body>
</html>