<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Baggage Page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
<LINK rel="stylesheet" type="text/css"
	href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css"
	href="../../themes/default/css/ui.all_no_cache.css">

<script
	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script
	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script type="text/javascript"
	src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/baggage/Baggage.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/baggage/PopUpData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript">	
			var languageHTML = "<c:out value="${requestScope.reqLanguageList}" escapeXml="false" />";
			var listForDisplayEnabled = <c:out value="${requestScope.reqListForDisplayEnabled}" escapeXml="false" />;			
		</script>
</head>
<body class="tabBGColor" scroll="no"
	onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false"
	onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
	<script type="text/javascript">			
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />										
			</script>
	<div id="divSearch" style="height: 30px; text-align: left;">
		<table width="96%" border="0" cellpadding="0" cellspacing="2"
			ID="Table9">
			<tr>
				<td width="15%"><font>Baggage Name</font></td>
				<td width="20%"><input name="srchBaggageName" type="text"
					id="srchBaggageName" size="20" maxlength="50"><font
					class="mandatory">&nbsp;*</font></td>

				<td width="10%"><font>IATA</font></td>
				<td width="10%"><select name="selIATACode" size="1"
					id="selIATACode">
						<option value="">All</option>
						<c:out value="${requestScope.reqIATACodeList}" escapeXml="false" />
				</select>
				</td>
				<td width="10%"><font>Status</font></td>
				<td width="10%"><select name="selStatus" size="1"
					id="selStatus">
						<option value="ALL">All</option>
						<option value="ACT">Active</option>
						<option value="INA">Inactive</option>
				</select>
				</td>
				<td width="10%">&nbsp;</td>
				<td align="right"><input name="btnSearch" type="button"
					id="btnSearch" value="Search">
				</td>
				<td width="2%">&nbsp;</td>
			</tr>
		</table>
	</div>

	<div id="divResultsPanel"
		style="height: 300px; text-align: left; background-color: #ECECEC;">
		<table id="listBaggage" class="scroll" cellpadding="0" cellspacing="0"></table>
		<div id="baggagepager" class="scroll" style="text-align: center;"></div>

		<u:hasPrivilege privilegeId="sys.mas.baggage.add">
			<input name="btnAdd" type="button" id="btnAdd" value="Add"
				class="btnDefault">
		</u:hasPrivilege>
		<u:hasPrivilege privilegeId="sys.mas.baggage.edit">
			<input name="btnEdit" type="button" id="btnEdit" value="Edit"
				class="btnDefault">
		</u:hasPrivilege>
		<u:hasPrivilege privilegeId="sys.mas.baggage.delete">
			<input name="btnDelete" type="button" id="btnDelete" value="Delete"
				class="btnDefault">
		</u:hasPrivilege>
		<u:hasPrivilege privilegeId="sys.mas.baggage.add">
			<input type="button" value="Add Description For Display"
				class="btnDefault" onClick="return POSenable();" id="btnBaggageFD"
				style="width: 200px;">
		</u:hasPrivilege>

	</div>
	<div id="divDispBaggage"
		style=" text-align: left; background-color: #ECECEC;">
		<form method="post" action="showBaggage.action" id="frmBaggage"
			enctype="multipart/form-data">
			<table width="98%" border="0" cellpadding="0" cellspacing="2"
				align="center" ID="Table1">
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width="15%"><font>Baggage Name</font></td>
					<td colspan="2"><input name="baggageName" type="text"
						id="baggageName" size="20" maxlength="50"><font
						class="mandatory">&nbsp;*</font></td>
					<td rowspan="6">&nbsp;</td>
				</tr>
				<tr>
					<td><font>Baggage Description</font></td>
					<td colspan="2"><input name="description" type="text"
						id="description" size="30" maxlength="125"><font
						class="mandatory">&nbsp;*</font></td>
				</tr>
				<tr>
					<td><font>Baggage Weight</font></td>
					<td colspan="2"><input name="baggageWeight" type="text"
						id="baggageWeight" size="6" maxlength="3">&nbsp;Kg<font
						class="mandatory">&nbsp;*</font>
					</td>
				</tr>
				<tr>
					<td><font>Class of Service</font>
					</td>
					<td colspan="2"><select name="cos" size="6" id="cos"
						multiple="multiple" style="height: 70px;">
							<c:out value="${requestScope.reqCOSList}" escapeXml="false" />
					</select> <font class="mandatory">&nbsp;*</font></td>
				</tr>
				<tr>
					<td><font>IATA Code</font></td>
					<td colspan="2"><select name="iataCode" size="1" id="iataCode">
							<option value="-1"></option>
							<c:out value="${requestScope.reqIATACodeList}" escapeXml="false" />
					</select> <font class="mandatory">&nbsp;*</font>
					</td>
				</tr>

				<tr>
					<td><font>Active</font></td>
					<td colspan="2"><input type="checkbox" name="status"
						id="status" value="ACT"></td>
				</tr>
				<tr>
					<td colspan="3" style="height: 42px;" valign="bottom"><input
						name="btnClose" type="button" id="btnClose" value="Close"
						onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault">
						<input name="btnReset" type="button" id="btnReset" value="Reset"
						class="btnDefault">
					</td>
					<td align="right" valign="bottom"><input name="btnSave"
						type="button" id="btnSave" value="Save" class="btnDefault">
					</td>
				</tr>
				<tr>
					<td><input type="text" name="version" id="version"
						style="display: none;" /> <input type="text" name="baggageId"
						id="baggageId" style="display: none;" /> <input type="text"
						name="createdBy" id="createdBy" style="display: none;" /> <input
						type="text" name="createdDate" id="createdDate"
						style="display: none;" /> <input type="text" name="rowNo"
						id="rowNo" style="display: none;" /></td>
				</tr>
			</table>
			<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
			<input type="hidden" name="hdnModel" id="hdnModel" value="">
			<input type="hidden" name="baggagesForDisplay"
				id="baggagesForDisplay" value="">

		</form>
	</div>
	<script>
		var screenId = 'SC_ADMN_033';
		top[2].HideProgress();
	</script>
</body>
</html>
