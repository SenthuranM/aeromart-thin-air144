<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>City Admin</title>
   	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">		
		<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">

		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
				
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
    	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.formfileupload.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/city/City.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/city/PopUpData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
		  
</head>
 	<body class="tabBGColor" scroll="no"  onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
		 		
  			<script type="text/javascript">			
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
				var cityArr = new Array();
				<c:out value="${requestScope.reqCityCountryList}" escapeXml="false" />
				var listForDisplayEnabled = <c:out value="${requestScope.reqListForDisplayEnabled}" escapeXml="false" />;
				var languageHTML = "<c:out value="${requestScope.reqLanguageList}" escapeXml="false" />";
				var lccEnableStatus = "<c:out value="${requestScope.reqLCCEnableStatus}" escapeXml="false" />";
			</script>
			
			<div id="divSearch" style="height:30px;text-align:left;">
				<table width="96%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
					<tr>
						<td width="10%"><font>Country</font></td>
						<td>								
							<select name="selCountry" id="selCountry">
							<option value="-1">All </option> 
								<c:out value="${requestScope.reqCountryList}" escapeXml="false" />
	                         </select>
	                    </td>
	                    <td width="10%"><font>City Code</font></td>
	                    <td>								
							<select name="selCity" id="selCity">
								<option value="-1">   </option> 
	                        </select>
	                    </td>
	                    <td width="10%"><font>Status</font></td>
	                    <td>
							<select name="selStatus" size="1" id="selStatus">
							    <option value="-1">   </option> 										
								<option value="ACT" selected>Active</option>
								<option value="INA">Inactive</option>
							</select>
						</td>  				
						<td align="right" width="8%">									
							<input name="btnSearch" type="button" id="btnSearch" value="Search">
						</td>
					</tr>
				</table>			
			</div>
			
			<div id="divResultsPanel" style="height:300px;text-align:left;background-color:#ECECEC;">
				<table width="98%" id="listCity" class="scroll" cellpadding="0" cellspacing="0"></table>
				<div id="listCitypager" class="scroll" style="text-align:center;"></div>
				<input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault">
				<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault">
				<input name="btnDelete" type="button" id="btnDelete" value="Delete" class="btnDefault">
				<input name="btnCityFD" type="button" id="btnCityFD" value= "Add Name For Display" class="btnDefault" onClick="return POSenable();" style="width:150px">
			</div>			  		
			<div id="divDispCity" style="height:215px;text-align:left;background-color:#ECECEC;">
				<form method="post" action="showCity.action" id="frmCity" enctype="multipart/form-data"> 
				<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
					<tr><td>&nbsp;</td></tr>					
					<tr>
						<td width="18%"><font>City Code</font></td>
						<td colspan="2"><input name="cityCode" type="text" id="cityCode" size="25"  maxlength="50"><font class="mandatory">&nbsp;*</font></td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td width="18%"><font>City Name</font></td>
						<td colspan="3"><input name="cityName" type="text" id="cityName" size="60"  maxlength="100"><font class="mandatory">&nbsp;*</font></td>
					</tr>					
					<tr><td>&nbsp;</td></tr>
					<tr>
					<td><font>Country</font></td>
						<td colspan="2">
							<select name="cityCountry" size="1" id="cityCountry">	
								<c:out value="${requestScope.reqCountryList}" escapeXml="false" />
							</select>
							<font class="mandatory">&nbsp;*</font>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td width="8%"><font>Status</font></td>
							<td width="7%">
								<select name="status" size="1" id="status">										
									<option value="ACT" selected>Active</option>
									<option value="INA">Inactive</option>
								 </select>
							</td>
					</tr>	
					<tr><td>&nbsp;</td></tr>
					<tr>	 
				  		<td><span id="fntLCCPublish"><font>LCC Publish Status</font></span></td>
				  		<td colspan="3">
					  		<select name="selLccPublishStatus" size="1" id="selLccPublishStatus" >
								<option value="-1"></option>
								<option value="PUB" disabled="disabled">PUBLISHED</option>
								<option value="TBP">TO BE PUBLISHED</option>
								<option value="TBR" disabled="disabled">TO BE REPUBLISHED</option>
								<option value="SKP">SKIP</option>
				  			</select>
				  		</td>
				  	</tr>						
			 		<tr>
						<td colspan="3" style="height:42px;"  valign="bottom">
							<input name="btnClose" type="button"  id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault">
						    <input name="btnReset" type="button"  id="btnReset" value="Reset" class="btnDefault">
						</td>
						<td align="right" valign="bottom">
							<input name="btnSave" type="button"  id="btnSave" value="Save" class="btnDefault">
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="cityId"  id="cityId" style="display: none;" />
							<input type="text" name="version"  id="version" style="display: none;" />
							<input type="text" name="createdDate"  id="createdDate" style="display: none;" />
							<input type="text" name="createdBy"  id="createdBy" style="display: none;" />
							<input type="text" name="rowNo"  id="rowNo" style="display: none;" />					
						</td>
				   </tr>
			  </table>		
			  <input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
			  <input type="hidden" name="hdnModel" id="hdnModel" value="">
			  </form>			 
			</div>	
	<script>
		var screenId = 'SC_ADMN_029';
		top[2].HideProgress();
	</script>
  </body>
</html>