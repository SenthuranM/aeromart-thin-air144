<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	    <title>Advertisement Page</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">		
		<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
		
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
				
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
    	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.formfileupload.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/advertisement/Advertisement.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		
		<script src="../../js/advertisement/PopUpData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		  
		   
		<script type="text/javascript">	
			var advertisementImagePath = "";
			<c:out value="${requestScope.reqAdvertisementImgPath}" escapeXml="false" />
			var advertisementImageMaxHeight = "<c:out value="${requestScope.reqAdvertisementImgMaxHeight}" escapeXml="false" />";
			var advertisementImageMinHeight = "<c:out value="${requestScope.reqAdvertisementImgMinHeight}" escapeXml="false" />";
			var advertisementImageMaxWidth = "<c:out value="${requestScope.reqAdvertisementImgMaxWidth}" escapeXml="false" />";
			var advertisementImageMinWidth = "<c:out value="${requestScope.reqAdvertisementImgMinWidth}" escapeXml="false" />";
			var advertisementImageSize = "<c:out value="${requestScope.reqAdvertisementImgSize}" escapeXml="false" />";
			var languageHTML = "<c:out value="${requestScope.reqLanguageList}" escapeXml="false" />";
			var viaPointHTML = "<c:out value="${requestScope.reqViaList}" escapeXml="false" />";
		</script>
 	</head>
  	<body class="tabBGColor" scroll="no"  onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
		 		
  			<script type="text/javascript">			
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />										
			</script>
			
			<div id="divSearch" style="height:30px;text-align:left;">
				<table width="96%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
					<tr>
						<td width="10%"><font>Title</font></td>  
						<td width="14%"><input name="srchAdvertisementTitle" type="text" id="srchAdvertisementTitle" size="15"  maxlength="50" onkeyUp="alphaNumericValidate(this)" onkeyPress="alphaNumericValidate(this)"><font class="mandatory">&nbsp;</font></td>
						
 						<td width='8%'><font>Origin</font></td>
						<td width="10%">
							<select id='selOriginSearch' name='selOriginSearch' size='1' tabindex="2">
									<option value=""></option>
<!-- 									<option value="***">ALL</option>               -->
									<c:out value="${requestScope.reqViaList}" escapeXml="false" />
							</select>				
						</td>
						<td width='8%'><font>Destination</font></td>
						<td width="10%">
							<select id='selDestinationSearch' name='selDestinationSearch' size='1' tabindex="2">
									<option value=""></option>
<!-- 									<option value="***">ALL</option>                           -->
									<c:out value="${requestScope.reqViaList}" escapeXml="false" />
							</select>				
						</td>
						
						<td width='8%'><font>Language</font></td>
						<td width="10%">
							<select id='selLanguageSearch' name='selLanguageSearch' size='1' tabindex="2">
									<option value=""></option>
									<c:out value="${requestScope.reqLanguageList}" escapeXml="false" />
							</select>					
						</td>
				
						<td align="right" width="8%">									
							<input name="btnSearch" type="button" id="btnSearch" value="Search">
						</td>
					</tr>
				</table>			
			</div>
			
			<div id="divResultsPanel" style="height:300px;text-align:left;background-color:#ECECEC;">
				<table width="98%" id="listAdvertisement" class="scroll" cellpadding="0" cellspacing="0"></table>
				<div id="Advertisementpager" class="scroll" style="text-align:center;"></div>
					
					<u:hasPrivilege privilegeId="sys.mas.itin.advertisement.add">
						<input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="sys.mas.itin.advertisement.edit">
						<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="sys.mas.itin.advertisement.delete">	
						<input name="btnDelete" type="button" id="btnDelete" value="Delete" class="btnDefault">
					</u:hasPrivilege>			
				
			</div>			  		
			<div id="divDispAdvertisement" style="height:330px;text-align:left;background-color:#ECECEC;">
				<form method="post" action="showAdvertisement.action?mode=advertisementImage" id="frmAdvertisement" enctype="multipart/form-data"> 
				<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
						<tr><td>&nbsp;</td></tr>					
						<tr>
						<td width="15%"><font>Title</font></td>
						<td colspan="2"><input name="advertisementTitle" type="text" id="advertisementTitle" size="60"  maxlength="100"><font class="mandatory">&nbsp;*</font></td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
					<td><font>Language</font></td>
						<td colspan="2">
							<select name="advertisementLanguage" size="1" id="advertisementLanguage">	
								<option value="-1"></option>
								<c:out value="${requestScope.reqLanguageList}" escapeXml="false" />
							</select>
							<font class="mandatory">&nbsp;*</font>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td colspan='5'>
								<table border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td align="center"><font>O &amp; D &nbsp;</font></td>				
										<td align="center"><font> Origin</font><font>&nbsp;&nbsp;</font></td>
										<td>	
											<select name="advertisementOrigin" size="1" id="advertisementOrigin" style="width:55px;">
												<option value=""></option>
												<option value="***">ALL</option>
												<c:out value="${requestScope.reqViaList}" escapeXml="false" />
											</select>
										</td>
										<td align="center"><font>&nbsp;&nbsp;</font><font> Destination</font><font>&nbsp;&nbsp;</font></td>
										<td>	
											<select name="advertisementDestination" size="1" id="advertisementDestination" style="width:55px;">
												<option value=""></option>
												<option value="***">ALL</option>
												<c:out value="${requestScope.reqViaList}" escapeXml="false" />
											</select>
										</td>
										<td><font>&nbsp;</font></td>
										<td colspan="2" rowspan="3">
											<select id="advertisementSegmentCode" name="advertisementSegmentCode" multiple size="1" style="width:200px;height:85px">
											</select>
										</td>
										<td>
											<font class="mandatory">&nbsp;*</font>
										</td>
									</tr> 
									<tr> 	
										<td><font>&nbsp;</font></td>									
										<td align="center" ><font> Via 1</font></td>
										<td>	
											<select name="advertisementVia1" size="1" id="advertisementVia1" style="width:55px;">
												<option value=""></option>
												<c:out value="${requestScope.reqViaList}" escapeXml="false" />
											</select>
										</td>
										<td align="center"><font> Via 2</font></td>
										<td>	
											<select name="advertisementVia2" size="1" id="advertisementVia2" style="width:55px;">
												<option value=""></option>
												<c:out value="${requestScope.reqViaList}" escapeXml="false" />
											</select>
										</td>
										<td align="center">
											<a id="addAnchor" href="javascript:void(0)" onclick="addToList()" title="Add to list"><img src="../../images/AA115_no_cache.gif" border="0"></a>
											<font id=addAnchorSpace>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font>
										</td>
									</tr> 
									<tr> 
										<td><font>&nbsp;</font></td>							
										<td align="center"><font> Via 3</font></td>
										<td>	
											<select name="advertisementVia3" size="1" id="advertisementVia3" style="width:55px;">
												<option value=""></option>
												<c:out value="${requestScope.reqViaList}" escapeXml="false" />
											</select>
										</td>
										<td align="center"><font> Via 4</font></td>
										<td>	
											<select name="advertisementVia4" size="1" id="advertisementVia4" style="width:55px;">
												<option value=""></option>
												<c:out value="${requestScope.reqViaList}" escapeXml="false" />
											</select>
										</td>
										<td align="center">
										<a id="removeAnchor" href="javascript:void(0)" onclick="removeFromList()" title="Remove from list"><img src="../../images/AA114_no_cache.gif" border="0"></a>
										<font id=removeAnchorSpace>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font>
										</td> 
								  	</tr> 							
								</table>
							</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td><font>Advertisement Banner</font></td>
						<td><span id="dvUpLoad" style="width: 77px"> <input
							type="file" style="width: 77px" id="uploadImage"
							name="uploadImage" data-max-size="2048"
							onchange="imageValidate(this.id);setFileName(this.id)"> <font
							class="mandatory">&nbsp;*</font>&nbsp;
					</span> <span id="dvEditUpLoad" style="display: none; width: 77px;">
							<input style="width: 77px" type="file" id="editUploadImage"
							name="editUploadImage" data-max-size="2048"
							onchange="imageValidate(this.id);setFileName(this.id)">&nbsp;
					</span> <label id=lblFileName>file name</label></td>

						<td >
							<input name="btnView" type="button" id="btnView" value="Show Image" class="" width="100px" height="15px" onclick="showImage()">
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td width="8%"><font>Status</font></td>
							<td width="7%">
								<select name="status" size="1" id="status">										
									<option value="ACT" selected>Active</option>
									<option value="INA">Inactive</option>
								  </select>
						</td>
						
					</tr>	
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td width="8%"><font>Remarks</font></td>
						<td width="7%">
							<textarea name="advertisementRemarks" id="advertisementRemarks" cols="60" title="Can enter only up to 255 charactors"></textarea>
						</td>
					</tr>							
			 		<tr>
						<td colspan="3" style="height:42px;"  valign="bottom">
							<input name="btnClose" type="button"  id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault">
						    <input name="btnReset" type="button"  id="btnReset" value="Reset" class="btnDefault">
						</td>
						<td align="right" valign="bottom">
							<input name="btnSave" type="button"  id="btnSave" value="Save" class="btnDefault">
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="advertisementId"  id="advertisementId" style="display: none;" />
							<input type="text" name="version"  id="version" style="display: none;" />
							<input type="text" name="createdDate"  id="createdDate" style="display: none;" />
							<input type="text" name="createdBy"  id="createdBy" style="display: none;" />
							<input type="text" name="advertisementImage"  id="advertisementImage" style="display: none;" />
							<input type="text" name="editAdvertisementImage"  id="editAdvertisementImage" style="display: none;" />
							<input type="text" name="rowNo"  id="rowNo" style="display: none;" />
							<input type="text" name="advertisementDeleteId"  id="advertisementDeleteId" style="display: none;" />						
						</td>
				   </tr>
			  </table>		
			  <input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
			  <input type="hidden" name="hdnModel" id="hdnModel" value="">
			  <img src="" id="validateImage" name="validateImage" style="display: none;" >
			  </form>			 
			</div>	
	<script>
		var screenId = 'SC_ADMN_044';
		top[2].HideProgress();
	</script>
  </body>
</html>