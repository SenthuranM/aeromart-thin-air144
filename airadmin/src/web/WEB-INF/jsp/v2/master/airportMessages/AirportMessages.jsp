<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
 
<title>Airport Messages</title>
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
<LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css">

	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">		
		<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
				
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script type="text/javascript" src="../../js/v2/jquery/grid.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/grid.inlinedit.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> 
    	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.RESCRIPTED.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.formfileupload.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>			
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jquery.popupwindow.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jqValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/isa.airadmin.jq.plugin.config.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	

	<script src="../../js/v2/isalibs/isa.jquery.multi-selector.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript" ></script>
  	<script type="text/javascript">  
  	
  	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
  		var allSalesChannels = null; 	
  		var allFlightsNumbers = null; 		
			
  		var allSalesChannels = <c:out value="${requestScope.reqSalesChannel}" escapeXml="false" />  		
  		var allFlightsNumbers = <c:out value="${requestScope.reqFltNumber}" escapeXml="false" />  
  		var languageHTML = "<c:out value="${requestScope.reqLanguageList}" escapeXml="false" />";
  </script>
  </head>
  <body class='tabBGColor'>

<div style="margin-top:0px;height:570px;">

	<div id="divMessagesSearch" style="height:60px;text-align:left;background-color:#ECECEC;">
  	<form name="frmMessagesSearch" id="frmMessagesSearch" action="" method="post">
		<table width="100%" border="0" cellpadding="2" cellspacing="2">
			<tr>
				 <td width="100%">
					 <table>
						  <tr>
						    <td align="left" class="fntBold" width="10%" ><font>Airport</font></td>
							<td align="left" width="10%">
							<select id="searchAirport" name="searchAirport" class="aa-input" style="width:80px">	
							<option value=""></option>																					
							<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" /></select>
							
							</td>
							
							<td align="left" width="10%"><font class="fntBold">Dep Arr Type</font></td>
							<td align="left" width="10%">
							<select id="searchDepArrType" name="searchDepArrType" class="aa-input" style="width:80px">	
							<option value=""></option>	
							<option value="ANY">Any</option>
							<option value="DEP">Departure</option>
							<option value="ARR">Arrival</option>																	
							</select>
							</td>
							
							<td align="left" width="10%"><font class="fntBold">Message</font></td>
							<td align="left" width="39%"><input name="searchMessage" type="text" id="searchMessage" class="aa-input" maxlength="20" />				
										
						  </tr>
					 </table>
				<td>	
			</tr>
			
			<tr>			
				<td width="100%">
					<table> 
						<tr>
							<td align="left" width="10%"><font class="fntBold">Dispaly From</font></td>
							<td align="left" width="10%"><input name="searchDisplayFrom" type="text" id="searchDisplayFrom" style="width:60px" class="aa-input" maxlength="10"  onBlur="dateChk('searchFrom')" />						
							</td>
							<td align="left" width="10%"><font class="fntBold">Dispaly To</font></td>
							<td align="left" width="10%"><input name="searchDisplayTo" type="text" id="searchDisplayTo" style="width:60px" class="aa-input"   maxlength="10"  onBlur="dateChk('searchTo')"/>						
							</td>				
							<td align="left" width="10%"><font class="fntBold">Valid From</font></td>
							<td align="left" width="10%"><input name="searchValidFrom" type="text" id="searchValidFrom" style="width:60px" class="aa-input"   maxlength="10"  onBlur="dateChk('searchFrom')" />						
							</td>
							<td align="left" width="10%"><font class="fntBold">Valid To</font></td>
							<td align="left" width="10%"><input name="searchValidTo" type="text" id="searchValidTo" style="width:60px" class="aa-input" maxlength="10"  onBlur="dateChk('searchTo')"/>						
							</td>
							
							<td align="right" width="10%">
								<input name="btnSearch" type="button" class="btnDefault" id="btnSearch" value="Search"/>
							</td>
							
						</tr>					
					</table>
				</td>			
			</tr>				
		</table>
	</form>
	</div>
	
	<div id="divResultsPanel" style="height:140px;text-align:left;background-color:#ECECEC;">
			<table id="tblAirportMsg" class="scroll" cellpadding="0" cellspacing="0"></table>
			<div id="divAirportMsgPager" class="scroll" style="text-align:center;"></div>
						<u:hasPrivilege privilegeId="master.airportmsg.add">			
						<input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault"/>
						</u:hasPrivilege>
						<u:hasPrivilege privilegeId="master.airportmsg.edit">					
						<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault"/>					
						</u:hasPrivilege>
						<u:hasPrivilege privilegeId="master.airportmsg.translation">
						<input id="btnMsgDisplayDes" type="button" id="btnMsgDisplayDes" value= "Add Content For Display" class="btnDefault" style="width:200px;">
						</u:hasPrivilege>		
	</div>
		
	<form id = "frmModify" method="post" action="airportMessages!addEditMessage.action">
	
	<div id="divDisplayAirportMsg" style="height:325px;text-align:left;background-color:#ECECEC;">
		<table width="98%" border="0" cellpadding="0" cellspacing="0" align="center" >
			<tr>
				<td width = '50%' valign="top">
					<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" >
						<tr><td>&nbsp;</td></tr>	
						<tr>
							<td width="20%"><font  class="fntBold">Airport</font></td>
							<td  width = "20%">
								<select id="selMsgAirport" name="airport" class="aa-input frm_editable" style="width:150px">
								<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
								</select>					
							<font class="mandatory">*</font>
							</td>											
						</tr>
						<tr><td>&nbsp;</td></tr>	
						
						<tr>
							<td width="20%"><font  class="fntBold"> Dep Arr Type</font></td>
							<td  width = "20%">
								<select id="selDepArrType" name="depArrType" class="aa-input frm_editable" style="width:150px">
								<option value="ANY">Any</option>
								<option value="DEP">Departure</option>
								<option value="ARR">Arrival</option>
								</select>
								<font class="mandatory">*</font>							
							</td>							
						</tr>
						
						<tr><td>&nbsp;</td></tr>	
						
						<tr>
							<td width="20%"><font  class="fntBold">Message Type</font></td>
							<td  width = "20%">
								<select id="selMsgType" name="messageType" class="aa-input frm_editable" style="width:150px">								
								<option value="SEARCH_RESULT">SEARCH RESULT</option>
								<option value="CARD_PAYMENT">CARD PAYMENT</option>
								<option value="ITINERARY">ITINERARY</option>
								</select>
								<font class="mandatory">*</font>							
							</td>											
						</tr>
						
						<tr><td>&nbsp;</td></tr>
							
						<tr>
							<td ><font  class="fntBold">Display From </font></td>
							<td>
								<input name="displayFrom" type="text" class = "aa-input frm_editable" id="msgDisplayFromDate" size="10"  maxlength="20"  onBlur="dateChk('msgDisplayFromDate')"/>						
							<font class="mandatory">*</font>
							</td>
						</tr>	
						
						<tr><td>&nbsp;</td></tr>
							
						<tr>
							<td width = "10%"><font  class="fntBold">Display To</font></td>
							<td>
								<input name="dispalyTo" type="text" id="msgDisplaytoDate" class = "aa-input frm_editable" size="10"  maxlength="20"  onBlur="dateChk('msgDisplaytoDate')"/>						
							<font class="mandatory">*</font>
							</td>
						</tr>	
						
						<tr><td>&nbsp;</td></tr>			
						
						<tr>
							<td ><font  class="fntBold"> Valid From </font></td>
							<td>
								<input name="validFrom" type="text" class = "aa-input frm_editable" id="validFromDate" size="10"  maxlength="20"  onBlur="dateChk('validFromDate')"/>						
							<font class="mandatory">*</font>
							</td>
						</tr>	
						<tr><td>&nbsp;</td></tr>	
						<tr>
							<td width = "10%"><font  class="fntBold">Valid To </font></td>
							<td>
								<input name="validTo" type="text" id="validToDate" class = "aa-input frm_editable" size="10"  maxlength="20"  onBlur="dateChk('validToDate')"/>						
							<font class="mandatory">*</font>
							</td>
						</tr>	
						
						<tr><td>&nbsp;</td></tr>	
						
						<tr>
							<td><font  class="fntBold">Message</font></td>
							<td rowspan="2"><textarea name='message'
									id='txtMessage' class="aa-input frm_editable"
									style='height: 50px; width: 300px;' ></textarea>
							<font class="mandatory">*</font>		
							</td>
									
						</tr>
						<tr><td>&nbsp;</td></tr>	
					</table>				
				</td>
				<td height="100%"  valign="top">
					<table width="100%" border="0" cellpadding="2" cellspacing="2" align="center" height="100%">
						<tr>
							<td colspan = "6" valign="top"><font class="fntBold"></font></td>
						</tr>
						<tr>
							<td colspan = "6" width="100%" valign="top">
							<div id = "divTabbedVisiblity" style = "background-color:#ECECEC;">
								<ul>
									<li><a href="#divONDs">ONDs</a></li>
									<li><a href="#divFlighNumbers">Flights</a></li>
									<li><a href="#divsalesChannels">Sales Channels</a></li>									
								</ul>
								    <%@ include file="inc_ONDs.jsp" %>
								    <%@ include file="inc_Flights.jsp" %>								   
									<%@ include file="inc_salesChannels.jsp" %>									
							</div>
							</td>
						</tr>								
					</table>	
				</td>
			</tr>						
		</table>
		<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center">
						
					<tr>
						<td colspan="3" style="height:42px;"  valign="bottom">
							<input name="btnClose" type="button"  id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId);" class="btnDefault" />
						    <input name="btnReset" type="button"  id="btnReset" value="Reset" class="btnDefault" />
						</td>
						<td align="right" valign="bottom">
							<input name="btnSave" type="button"  id="btnSave" value="Save" class="btnDefault" />
						</td>
					</tr>
		</table>
	</div>
			<input type="hidden" name="messageId" id="hdnMessageID" value="" >
			<input type="hidden" name="version" id="hdnVersion" value="">
	
	</form>
	</div>

<script type="text/javascript">
  	<!--
	var screenId = 'SC_ADMN_032';
	top[2].HideProgress();
  	//-->
  </script>
  	 <script type="text/javascript" src="../../js/v2/master/airportMessages/ONDs.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  	 <script type="text/javascript" src="../../js/v2/master/airportMessages/flightNumbersMultiSelect.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  	 <script type="text/javascript" src="../../js/v2/master/airportMessages/salesChannelsMultiSelect.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	 <script type="text/javascript" src="../../js/v2/master/airportMessages/airportMessages.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	 
	 <script type="text/javascript" src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	 <script type="text/javascript" src="../../js/v2/master/airportMessages/popupData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	 <script type="text/javascript" src="../../js/v2/master/airportMessages/languageTranslationPopup.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	 
  </body>
</html>
