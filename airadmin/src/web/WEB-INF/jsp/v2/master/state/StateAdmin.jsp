<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>State Admin</title>
   	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">		
		<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">

		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
				
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
    	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.formfileupload.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/state/State.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		
	<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		  
</head>
 	<body class="tabBGColor" scroll="no"  onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
		 		
  			<script type="text/javascript">			
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />										
			</script>
			
			<div id="divSearch" style="height:30px;text-align:left;">
				<table width="96%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
					<tr>
						<td width="10%"><font>Country</font></td>
						<td>								
							<select name="selCountry" id="selCountry">
							<option value="-1">All </option> 
								<c:out value="${requestScope.reqCountryList}" escapeXml="false" />
	                         </select>
	                    </td> 				
						<td align="right" width="8%">									
							<input name="btnSearch" type="button" id="btnSearch" value="Search">
						</td>
					</tr>
				</table>			
			</div>
			
			<div id="divResultsPanel" style="height:300px;text-align:left;background-color:#ECECEC;">
				<table width="98%" id="listState" class="scroll" cellpadding="0" cellspacing="0"></table>
				<div id="listStatepager" class="scroll" style="text-align:center;"></div>
					
					<u:hasPrivilege privilegeId="sys.mas.state.add">
						<input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="sys.mas.state.edit">
						<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="sys.mas.state.delete">	
						<input name="btnDelete" type="button" id="btnDelete" value="Delete" class="btnDefault">
					</u:hasPrivilege>			
				
			</div>			  		
			<div id="divDispState" style="height:330px;text-align:left;background-color:#ECECEC;">
				<form method="post" action="showState.action" id="frmState" enctype="multipart/form-data"> 
				<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
					<tr><td>&nbsp;</td></tr>					
					<tr>
						<td width="18%"><font>State Code</font></td>
						<td colspan="2"><input name="stateCode" type="text" id="stateCode" size="25"  maxlength="50"><font class="mandatory">&nbsp;*</font></td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td width="18%"><font>State Name</font></td>
						<td colspan="3"><input name="stateName" type="text" id="stateName" size="60"  maxlength="100"><font class="mandatory">&nbsp;*</font></td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td width="18%"><font>AirLine Office Street Address 1</font></td>
						<td colspan="3"><input name="arlOffStAdress1" type="text" id="arlOffStAdress1" size="60"  maxlength="100"><font class="mandatory">&nbsp;*</font></td>
					</tr>
					<tr>
						<td width="18%"><font>AirLine Office Street Address 2</font></td>
						<td colspan="3"><input name="arlOffStAdress2" type="text" id="arlOffStAdress2" size="60"  maxlength="100"><font class="mandatory">&nbsp;*</font></td>
					</tr>
					<tr>
						<td width="18%"><font>AirLine Office City</font></td>
						<td colspan="3"><input name="arlOffcity" type="text" id="arlOffcity" size="60"  maxlength="100"><font class="mandatory">&nbsp;*</font></td>
					</tr>
					<tr>
						<td width="18%"><font>AirLine Office Tax Register No</font></td>
						<td colspan="3"><input name="arlOffTaxReg" type="text" id="arlOffTaxReg" size="60"  maxlength="100"><font class="mandatory">&nbsp;*</font></td>
					</tr>
					<tr>
						<td width="18%"><font>State Digital Signature</font></td>
						<td colspan="3"><input name="stateDigiSig" type="text" id="stateDigiSig" size="60"  maxlength="200"></td>
					</tr>
					
					<tr><td>&nbsp;</td></tr>
					<tr>
					<td><font>Country</font></td>
						<td colspan="2">
							<select name="stateCountry" size="1" id="stateCountry">	
								<c:out value="${requestScope.reqCountryList}" escapeXml="false" />
							</select>
							<font class="mandatory">&nbsp;*</font>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td width="8%"><font>Status</font></td>
							<td width="7%">
								<select name="status" size="1" id="status">										
									<option value="ACT" selected>Active</option>
									<option value="INA">Inactive</option>
								  </select>
						</td>
						
					</tr>	
					<tr><td>&nbsp;</td></tr>						
			 		<tr>
						<td colspan="3" style="height:42px;"  valign="bottom">
							<input name="btnClose" type="button"  id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault">
						    <input name="btnReset" type="button"  id="btnReset" value="Reset" class="btnDefault">
						</td>
						<td align="right" valign="bottom">
							<input name="btnSave" type="button"  id="btnSave" value="Save" class="btnDefault">
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="stateId"  id="stateId" style="display: none;" />
							<input type="text" name="version"  id="version" style="display: none;" />
							<input type="text" name="createdDate"  id="createdDate" style="display: none;" />
							<input type="text" name="createdBy"  id="createdBy" style="display: none;" />
							<input type="text" name="rowNo"  id="rowNo" style="display: none;" />					
						</td>
				   </tr>
			  </table>		
			  <input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
			  <input type="hidden" name="hdnModel" id="hdnModel" value="">
			  </form>			 
			</div>	
	<script>
		var screenId = 'SC_ADMN_044';
		top[2].HideProgress();
	</script>
  </body>
</html>