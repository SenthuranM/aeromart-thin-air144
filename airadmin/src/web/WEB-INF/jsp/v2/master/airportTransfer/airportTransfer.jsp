<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Airport Transfer</title>
        <LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
        <LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
        <LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css">

	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
	    
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">		
		<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
				
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script type="text/javascript" src="../../js/v2/jquery/grid.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/grid.inlinedit.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> 
    	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.RESCRIPTED.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jquery.popupwindow.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jqValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/isa.airadmin.jq.plugin.config.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	

  	   <script type="text/javascript">  		
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />		
      </script>
  </head>
  
<body class='tabBGColor'>
<!-- <div style="margin-top:0px;:570px;"> -->
<div style="margin-top:0px;">
	<div id="divSearch" style="height:30px;text-align:left;">
				<table width="96%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
					<tr>
						 	<td align="left" class="fntBold" width="10%" ><font>Airport</font></td>
							<td align="left" width="10%">
								<select id="searchAirport" name="searchAirport" size="1" style="width:80px">	
								     <option value=""></option>																			
									 <c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
							   </select>
							</td>
						
							<td width="8%"><font>Airport Transfer Status</font></td>
							<td width="7%">
								<select name="selStatus" size="1" id="selStatus">
									<option value="ALL">All</option>										
									<option value="ACT">Active</option>
									<option value="INA">Inactive</option>
							  	</select>
							</td>								
							<td align="right" width="8%">									
								<input name="btnSearch" type="button" id="btnSearch" value="Search">
							</td>
					</tr>
				 </table>
	</div>
	
	<div id="divResultsPanel" style="height:250px;text-align:left;background-color:#ECECEC;">
				<table id="tblAirportTransfers" class="scroll" cellpadding="0" cellspacing="0"></table>
				<div id="transferpager" class="scroll" style="text-align:center;"></div>
					
					<u:hasPrivilege privilegeId="sys.mas.airport.transfer.add">
						<input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="sys.mas.airport.transfer.edit">
						<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault">
					</u:hasPrivilege>
	</div>			
  	
	
	<div id="divDispTransfer" style="height:330px;text-align:left;background-color:#ECECEC;">
	<form method="post"  action="showAirportTransfer!save.action" id="frmAirportTransfer"  enctype="multipart/form-data"> 
		<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" >
			  <tr>
			  	  <td>&nbsp;</td>
			  </tr>
			  <tr>
				  <td width="15%"><font>Airport Code</font></td>
				  <td colspan="2">
				        <select id="airportCode" name="transfer.airportCode" class="aa-input" style="width: 80px">
						<option value=""></option>
					    <c:out value="${requestScope.reqActiveViaList}"	escapeXml="false" />
						</select> <font class="mandatory">&nbsp;*</font>
				  </td>
		     </tr>

					<tr><td>&nbsp;</td></tr>	
						<tr>
							<td>Provider Name</td>
							
							<td>
								<input name="transfer.providerName" type="text" class = "aa-input frm_editable"  id="providerName"  size="26" maxlength="30" onkeyUp="customAlphaWhiteSpaceValidate(this)" onkeyPress="customAlphaWhiteSpaceValidate(this)" />						
								<font class="mandatory">*</font> 
							</td>
						</tr>	
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td>Provider Address</td>
							<td>
								<input name="transfer.providerAddess" type="text" class = "aa-input frm_editable" id="providerAddess"  size="26" maxlength="80" onkeyUp="validateWithComma(this)" onkeyPress="validateWithComma(this)"/>						
								<font class="mandatory">*</font>
							</td>
						</tr>	
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td >Provider Number</td>
							<td>
							   <table>
							   	<tr>
							   	  <td>
							   	  	<input type="text" class = "aa-input frm_editable" id="countryCode"  size="4" maxlength="4" onkeyUp="positiveNumberCustomValidate(this)" onkeyPress="positiveNumberCustomValidate(this)"/>
							   	  </td>
							   	  <td>
							   	  	<input type="text" class = "aa-input frm_editable" id="areaCode"  size="4" maxlength="4" onkeyUp="positiveNumberCustomValidate(this)" onkeyPress="positiveNumberCustomValidate(this)"/>
							   	  </td>
							   	  <td>
							   	  	<input type="text" class = "aa-input frm_editable" id="contactNo"  size="10" maxlength="10" onkeyUp="positiveNumberCustomValidate(this)" onkeyPress="positiveNumberCustomValidate(this)"/> <font class="mandatory">*</font>
							   	  </td>
							   	</tr>
							   </table>
								
							</td>
						</tr>	
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td>Provider Email</td>
							<td>
								<input name="transfer.providerEmail" type="text" class = "aa-input frm_editable" id="providerEmail" maxlength="25" size="26"/>						
								<font class="mandatory">*</font>
							</td>
						</tr>	
						<tr><td>&nbsp;</td></tr>	
						<tr>
							   <td>Active</td>
								<td colspan="2"><input type="checkbox" name="chkStatus" id="chkStatus"></td>
					    </tr>
		 </table>				
				
		<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center">
						
					<tr>
						<td colspan="3" style="height:42px;"  valign="bottom">
							<input name="btnClose" type="button"  id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId);" class="btnDefault" />
						    <input name="btnReset" type="button"  id="btnReset" value="Reset" class="btnDefault" />
						</td>
						<td align="right" valign="bottom">
							<input name="btnSave" type="button"  id="btnSave" value="Save" class="btnDefault" />
						</td>
					</tr>
		</table>
		    <input type="hidden" name="transfer.status" id="status" value="INA">
		    <input type="hidden" name="transfer.providerNumber" id="providerNumber" value="" />
		    <input type="hidden" name="transfer.version" id="version" value="">
			<input type="hidden" name="transfer.createdBy" id="createdBy" value="">
			<input type="hidden" name="transfer.createdDate" id="createdDate" value="">
			<input type="hidden" name="transfer.modifiedBy" id="modifiedBy" value="">
			<input type="hidden" name="transfer.modifiedDate" id="modifiedDate" value="">
			<input type="hidden" name="transfer.airportTransferId" id="airportTransferId" value="">
		</form>
	</div>
	
</div>

<script type="text/javascript">
  	var screenId = 'SC_ADMN_042';
	top[2].HideProgress();
</script>
	 <script type="text/javascript" src="../../js/v2/master/airportTransfer/airportTransfer.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	 
	 <script type="text/javascript" src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	 <script type="text/javascript" src="../../js/v2/master/airportMessages/popupData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	 
  </body>
</html>
