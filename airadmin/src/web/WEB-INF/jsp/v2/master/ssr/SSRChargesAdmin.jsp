<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>SSR Charges Page</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
<LINK rel="stylesheet" type="text/css"
	href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css"
	href="../../themes/default/css/ui.all_no_cache.css">

<script
	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script
	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script type="text/javascript"
	src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script type="text/javascript"
	src="../../js/ssr/SSRCharges.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script
	src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

</head>
<body class="tabBGColor" scroll="no"
	onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false"
	onkeydown='return Body_onKeyDown(event)' ondrag='return false'
	onload="applyByChange()">
	<script type="text/javascript">			
				var ssrCatMap = <c:out value="${requestScope.reqSSRCatMap}" escapeXml="false" />;
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />			
			</script>
	<div id="divSearch" style="height: 30px; text-align: left;">
		<table width="96%" border="0" cellpadding="0" cellspacing="2"
			ID="Table9">
			<tr>
				<td width="15%"><font>Charge Code</font>
				</td>
				<td width="20%"><input name="srchChargeCode" type="text"
					id="srchChargeCode" size="15" maxlength="10" class="UCase">
				</td>

				<td width="15%"><font>SSR Code</font>
				</td>
				<td width="20%"><select name="selSsrCode" size="1"
					id="selSsrCode">
						<option value="ALL">All</option>
						<c:out value="${requestScope.reqSsrCodeList}" escapeXml="false" />

				</select>
				</td>

				<%--
				<td width="15%"><font>Service Category</font>
				</td>
				<td width="20%"><select name="selCategory" size="1"
					id="selCategory">
						<option value="ALL">All</option>
						<c:out value="${requestScope.reqCategoryList}" escapeXml="false" />

				</select>
				</td>				 
				<td width="10%"><font>Status</font>
				</td>
				<td width="10%"><select name="selStatus" size="1"
					id="selStatus">
						<option value="ALL">All</option>
						<option value="ACT">Active</option>
						<option value="INA">Inactive</option>
				</select></td>
				--%>
				<td width="10%">&nbsp;</td>
				<td align="right"><input name="btnSearch" type="button"
					id="btnSearch" value="Search"></td>
				<td width="2%">&nbsp;</td>
			</tr>
		</table>
	</div>

	<div id="divResultsPanel"
		style="height: 300px; text-align: left; background-color: #ECECEC;">
		<table id="listSSRCharges" class="scroll" cellpadding="0"
			cellspacing="0"></table>
		<div id="ssrchargepager" class="scroll" style="text-align: center;"></div>

		<u:hasPrivilege privilegeId="sys.mas.ssrcharge.add">
			<input name="btnAdd" type="button" id="btnAdd" value="Add"
				class="btnDefault">
		</u:hasPrivilege>
		<u:hasPrivilege privilegeId="sys.mas.ssrcharge.edit">
			<input name="btnEdit" type="button" id="btnEdit" value="Edit"
				class="btnDefault">
		</u:hasPrivilege>
		<u:hasPrivilege privilegeId="sys.mas.ssrcharge.delete">
			<input name="btnDelete" type="button" id="btnDelete" value="Delete"
				class="btnDefault">
		</u:hasPrivilege>

	</div>
	<div id="divDispSSRCharges"
		style="height: 225px; text-align: left; background-color: #ECECEC;">
		<form method="post" action="showSSRCharges.action" id="frmSSRCharges"
			enctype="multipart/form-data">
			<table width="98%" border="0" cellpadding="0" cellspacing="2"
				align="center" ID="Table1">
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<table width="98%" border="0" cellpadding="0" cellspacing="5"
							align="center">
							<tr>
								<td width="15%"><font>Charge Code</font>
								</td>
								<td colspan="2"><input name="chargeCode" type="text"
									id="chargeCode" size="20" maxlength="10" class="UCase"
									onkeyUp="alphaCustomValidate(this)"
									onkeyPress="alphaCustomValidate(this)"><font
									class="mandatory">&nbsp;*</font>
								</td>
							</tr>
							<tr>
								<td><font>Description</font>
								</td>
								<td colspan="2"><input name="description" type="text"
									id="description" size="40" maxlength="125"><font
									class="mandatory">&nbsp;*</font>
								</td>
							</tr>
							<tr>
								<td><font>SSR Code</font>
								</td>
								<td colspan="2"><select name="ssrId" size="1" id="ssrId">
										<option value="-1"></option>
										<c:out value="${requestScope.reqSsrCodeList}"
											escapeXml="false" />
								</select> <font class="mandatory">&nbsp;*</font>
								</td>

							</tr>
							<tr class="airportServiceCharge" style="display: none;">
								<td><font>Apply To</font>
								</td>
								<td colspan="2"><select name="applyBy" size="1"
									id="applyBy" onchange="applyByChange()">
										<option value="P">Passenger</option>
										<option value="R">Reservation</option>
								</select><font class="mandatory">&nbsp;*</font>
								</td>
							</tr>
							<tr class="airportServiceCharge" style="display: none;">
								<td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;ADL&nbsp;<input
									name="adultAmount" type="text" id="adultAmount" size="9"
									maxlength="8" onkeyUp="validateAmount(this)"
									onkeyPress="validateAmount(this)">
									&nbsp;&nbsp;CHD&nbsp;<input name="childAmount" type="text"
									id="childAmount" size="9" maxlength="8"
									onkeyUp="validateAmount(this)"
									onkeyPress="validateAmount(this)">
									&nbsp;&nbsp;INF&nbsp;<input name="infantAmount" type="text"
									id="infantAmount" size="9" maxlength="8"
									onkeyUp="validateAmount(this)"
									onkeyPress="validateAmount(this)">
									&nbsp;&nbsp;Reservation&nbsp;<input name="reservationAmount"
									type="text" id="reservationAmount" size="9" maxlength="8"
									onkeyUp="validateAmount(this)"
									onkeyPress="validateAmount(this)"></td>
							</tr>	
							<tr class="infligtServiceCharge" style="display: none;">
								<td><font>Charge</font></td>
								<td colspan="2">
									<input type="text" name="serviceCharge" id="serviceCharge" size="9" maxlength="8">
									<font class="mandatory">&nbsp;*</font>
								</td>
							</tr>			
							<tr>
								<td><font>Class of Service</font></td>
								<td colspan="2">
									<select name="cos" size="4" id="cos" multiple="multiple" style="height: 58px; width: 150px" >	
									<c:out value="${requestScope.reqCOSList}" escapeXml="false" />
									</select>
								<font class="mandatory">&nbsp;*</font>
								</td>
							</tr>
							<tr>
						<td><font>Currency Code</font>	</td>
						<td>
						<c:choose>
							<c:when test="${requestScope.reqPrivSsrLoclCUrr == true}">
								<select name="localCurrCode" size="1" id="localCurrCode" >
										<c:out value="${requestScope.reqCurrencyCombo}" escapeXml="false"/>
							    </select><font class="mandatory">&nbsp;*</font> 
							    <label id="lblLocalCurrencyCode" style="display:none;"></label>
							</c:when>
							<c:otherwise>
								<label id="lblLocalCurrencyCode"></label>
								<input type="text" name="localCurrCode" id="localCurrCode" value="${requestScope.reqBaseCurrency}" style="display: none;" />
							</c:otherwise>						
						</c:choose>    				
								</td>								
						</tr>
							
						</table>
					</td>
				</tr>

				<tr>
					<td colspan="3" style="height: 42px;" valign="bottom"><input
						name="btnClose" type="button" id="btnClose" value="Close"
						onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault">
						<input name="btnReset" type="button" id="btnReset" value="Reset"
						class="btnDefault"></td>
					<td align="right" valign="bottom"><input name="btnSave"
						type="button" id="btnSave" value="Save" class="btnDefault">
					</td>
				</tr>
				<tr>
					<td><input type="text" name="version" id="version"
						style="display: none;" /> <input type="text" name="createdBy"
						id="createdBy" style="display: none;" /> <input type="text"
						name="createdDate" id="createdDate" style="display: none;" /> <input
						type="text" name="rowNo" id="rowNo" style="display: none;" />
					</td>
				</tr>
			</table>

			<input type="hidden" name="chargeId" id="chargeId">  
			<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">			

			<input type="hidden" name="ssrInfoCode" id="ssrInfoCode"> <input
				type="hidden" name="ssrInfoDescription" id="ssrInfoDescription">
			<input type="hidden" name="ssrInfoCatId" id="ssrInfoCatId"> <input
				type="hidden" name="ssrInfoCategory" id="ssrInfoCategory"> <input
				type="hidden" name="ssrSubCatId" id="ssrSubCatId"> <input
				type="hidden" name="ssrInfoSubCategory" id="ssrInfoSubCategory">
				<input type="hidden" name="status" id="status"> 
				
		</form>
	</div>
	<script>	
		var screenId = 'SC_ADMN_037';
		top[2].HideProgress();
	</script>
</body>
</html>
