<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	    <title>Template Page</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="-1">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">		
		<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
		
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
   	 	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	    <script type="text/javascript" src="../../js/ssr/SSRTemplate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
 	
 	</head>
  	<body class="tabBGColor" scroll="no"  onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
		 		
  			<script type="text/javascript">			
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />										
			</script>
			
			<div id="divSearchPanel" style="height:30px;text-align:left;">
				<table width="96%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
					<tr>
						<td><font>SSR Template</font></td>
						<td>
							<select name="selTemplate" size="1" id="selTemplate">
								<option value="">All</option>										
								<c:out value="${requestScope.reqSSRTempl}" escapeXml="false" />
							  </select>
						</td>
						<td><font>Status</font></td>
						<td>
							<select name="selStatus" size="1" id="selStatus">
								<option value="ACT">Active</option>
								<option value="INA">Inactive</option>
								<option value="ALL">All</option>
							</select>
						</td>									
						<td align="right">									
							<input name="btnSearch" type="button" id="btnSearch" value="Search">
						</td>
					</tr>
				</table>			
			</div>
			<div id="divResultsPanel" style="height:250px;text-align:left;background-color:#ECECEC;">
				<table id="listSSRTemp" class="scroll" cellpadding="0" cellspacing="0"></table>
				<div id="temppager" class="scroll" style="text-align:center;"></div>
					
					<u:hasPrivilege privilegeId="sys.mas.ssrtempl.add">
						<input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="sys.mas.ssrtempl.edit">
						<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="sys.mas.ssrtempl.delete">	
						<input name="btnDelete" type="button" id="btnDelete" value="Delete" class="btnDefault">
					</u:hasPrivilege>				
				
			</div>			  		
			<div id="divDispSSRTemp" style="height:250px;text-align:left;background-color:#ECECEC;">
				<form method="post" action="showSSRTemplate.action" id="frmSSR"> 
				<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
					<tr>
						<td width="15%"><font>Template Code</font></td>
						<td><input name="templateCode" type="text" id="templateCode" size="20"  class="UCase" maxlength="20"><font class="mandatory">&nbsp;*</font></td>
					</tr>
					<tr>
						<td><font>Description</font></td>
						<td><input name="description" type="text" id="description" size="30" maxlength="30"><font class="mandatory">&nbsp;*</font></td>
						<td><font>Active</font></td>
						<td><input type="checkbox" name="status" id="status" value="ACT"></td>
					</tr>					
					<tr>
						<td></td>
						<td valign="top" colspan="3"><table id="listSSRQty" class="scroll" cellpadding="0" cellspacing="0" style="width:650px"></table>
						</td>
						
					</tr>
					<tr>
						<td></td>
						<td colspan="3">
							<table>
								<tr>
								<td><font>Class of Service</font></td>
								<td colspan="2">
									<select name="cos" size="1" id="cos" style="width: 90px" >	
										<c:out value="${requestScope.reqCOSList}" escapeXml="false" />
									</select>
									<font class="mandatory">&nbsp;*</font>
								</td>
								<td valign="top"><font>SSR</font></td>
								<td><select name="selSSR" id="selSSR" size="1" style="width:105;">
									</select><font class="mandatory">&nbsp;*</font>
								</td>
								<td><font>Max Quantity</font></td>
								<td><input name="maxQty" type="text" id="maxQty" size="5" maxlength="3" ><font class="mandatory">&nbsp;*</font></td>
								<td><font>Status</font></td>								
								<td><select name="ssrStatus" id="ssrStatus" size="1" style="width:105;">
										<option value="ACT">Active</option>
										<option value="INA">In-Active</option>										
									</select><font class="mandatory">&nbsp;*</font>
								</td>
								<td>		
									<input name="btnChgAdd" type="button" id="btnChgAdd" value="Add" class="btnDefault">				
									<input name="btnChgEdit" type="button" id="btnChgEdit" value="Change" class="btnDefault">									
								</td>
								</tr>
							
							</table>
						</td>						
					</tr>				
			 		<tr>
						<td colspan="2" style="height:25px;"  valign="bottom">
							<input name="btnClose" type="button"  id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault">
						    <input name="btnReset" type="button"  id="btnReset" value="Reset" class="btnDefault">
						</td>
						<td align="right" valign="bottom">
							<input name="btnSave" type="button"  id="btnSave" value="Save" class="btnDefault">
						</td>
					</tr>
					<tr>
						<td>
							<input type="hidden" name="version"  id="version"/>
							<input type="hidden" name="templateId"  id="templateId"/>							
							<input type="hidden" name="createdBy"  id="createdBy"/>
							<input type="hidden" name="createdDate"  id="createdDate"/>
							<input type="hidden" name="ssrCabinQtys" id="ssrCabinQtys"/>
							<input type="hidden" name="sId"  id="sId"/>
							<input type="hidden" name="templCabinQty"  id="templCabinQty"/>
							<input type="hidden" name="ssrId"  id="ssrId"/>
							<input type="hidden" name="ssr"  id="ssr"/>
							<input type="hidden" name="maxQty"  id="ssr"/>
							<input type="hidden" name="ssrVersion"  id="ssrVersion"/>
							<input type="hidden" name="rowNo"  id="rowNo"/>							
						</td>
				   </tr>
			  </table>					
			  </form>
			</div>	
	<script>
		var screenId = 'SC_ADMN_038';
		top[2].HideProgress();
	</script>
  </body>
</html>