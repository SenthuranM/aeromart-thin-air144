<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	    <title>Template Page</title>
	    <meta http-equiv="pragma" content="no-cache"/>
	    <meta http-equiv="cache-control" content="no-cache"/>
	    <meta http-equiv="expires" content="-1"/>
		<link rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>		
		<link rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>
		<link rel="stylesheet" type="text/css" href="../../css/ui.jqgrid.css"/>
		
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.grouping.min.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript">
			var hidFromPage = '<c:out value="${requestScope.reqFromPage}" escapeXml="false" />';
			var hidFlightSearch = eval(<c:out value="${requestScope.reqHidString}" escapeXml="false" />);
			var hidFlightSumm = '<c:out value="${requestScope.hdnFlightSumm}" escapeXml="false" />';
			var isLogicalCCEnabled = '<c:out value="${requestScope.reqIsLogicalCCEnabled}" escapeXml="false" />';
			var hasBCStatusChangePrivilege = '<c:out value="${requestScope.hasBCStatusChangePrivilege}" escapeXml="false" />';
            var isPromotionEnabled = '<c:out value="${requestScope.reqIsPromotionEnabled}" escapeXml="false" />';
            var csFlightInvModifiable = '<c:out value="${requestScope.hdnCsFlightInvModifiable}" escapeXml="false" />';
            var hideOneWayReturnPaxCount = eval(<c:out value="${requestScope.hideOneWayReturnPaxCount}" escapeXml="false" />);
		</script>
		<style>
		#spnFlightSearchResults .ui-jqgrid-bdiv{
			overflow-y:scroll;
		}
		#spnFlightSearchResults .ui-jqgrid{
			float:left;
		}
		#cb_jqg{
			display:none;
		}
		#spnRollForwardAudits .ui-jqgrid-bdiv{
			overflow-y:scroll;
		}
		#spnRollForwardAudits .ui-jqgrid{
			float:left;
		}
		.GridAlternateRow{
			background:#F1F7F7;
		}
		.cbox{
		display:none;
		}
		</style>
 </head>
 <body class="tabBGColor">
	<div>
		<%@ include file="../../common/IncludeMandatoryText.jsp"%>
	</div>	
	<div id="SeatInventory" class="FormBackGround">
	<table width="100%" border="0" cellpadding="0" cellspacing="2" id="Table9">
		<tr>
			<td width="12%"><font>&nbsp;</font></td>
			<td width="13%"><font>&nbsp;</font></td>

			<td width="5%"><font>&nbsp;</font></td>
			<td width="10%"><font>&nbsp;</font></td>

			<td width="13%"><font>&nbsp;</font></td>
			<td width="4%"><font>&nbsp;</font></td>

			<td width="8%"><font>&nbsp;</font></td>
			<td width="15%"><font>&nbsp;</font></td>				  				

			<td width="8%"><font>&nbsp;</font></td>
			<td width="20%"><font>&nbsp;</font></td>				  				
		</tr>
		<tr>
			<td>
				<font>Departure Date - From</font>
			</td>
			<td><input name="txtDepartureDateFrom" type="text" id="txtDepartureDateFrom" style="width:78px;" maxlength="11" tabindex="1" onblur="dateChk('txtDepartureDateFrom')" /></td>
			<td align="left"><font class="mandatory">&nbsp;*</font></td>
			<td>
				<font>Departure Date - To</font>
			</td>
			<td><input name="txtDepartureDateTo" type="text" id="txtDepartureDateTo" style="width:78px;" maxlength="11"  tabindex="2"  onblur="dateChk('txtDepartureDateTo')" /></td>
			<td align="left"><font class="mandatory">&nbsp;*</font></td>
			<td>
				<font>Flight No</font>
			</td>
			<td>
				<input name="txtFlightNumber" type="text" id="txtFlightNumber" style="width:90px;" maxlength="7"  tabindex="3" onkeyup="alphaNumericValidate(this)" onkeypress="alphaNumericValidate(this)" title="Carrier code or Flight number"/>
			</td>
		
			<td>
				<font>Flight Status</font>
			</td>
			<td colspan="2">
			  	<select name="selFltStatus" size="1" id="selFltStatus"  tabindex="4" style="width:100px;">
	     	  	</select>
			</td>
		</tr>
		<tr>
			<td>
				<font>Departure</font>
			</td>
			<td colspan="2">
				<select name="selDeparture" size="1" id="selDeparture"  tabindex="5" title="Original departure location" style="width:100px;">
			    </select>
			</td>
			<td>
				<font>Arrival</font>
			</td>
			<td colspan="2">
			  <select name="selArrival" size="1" id="selArrival"  tabindex="6" title=" Final Destination" style="width:100px;">
			  </select>
			</td>
			<td>
				<font>Cabin Class</font>
			</td>
			<td>
			  <select name="selCabinClass" size="1" id="selCabinClass"  tabindex="7" style="width:100px;">
			  </select>
			</td>
		</tr>
		<tr>
			<td>
				<font>Via 1</font>
			</td>
			<td colspan="2">
			  <select name="selVia1" size="1" id="selVia1"  tabindex="8" style="width:100px;">
			  </select>
			</td>
			<td>
				<font>Via 2</font>
			</td>
			<td colspan="2">
			  <select name="selVia2" size="1" id="selVia2"  tabindex="9" style="width:100px;">
			  </select>
			</td>
			<td>
				<font>Via 3</font>
			</td>
			<td>
				<select name="selVia3" size="1" id="selVia3"  tabindex="10" style="width:100px;">
			  </select>
			</td>
			<td>
				<font>Via 4</font>
			</td>
			<td>
			  <select name="selVia4" size="1" id="selVia4"  tabindex="11" style="width:100px;">
			  </select>
			</td>																																	
		</tr>
		<tr>
			<td>
				<font>Time in Airport</font>
			</td>
			<td colspan="2">
				<font>
				<input type="radio" id="radTime_L" name="radTime" value="L"  tabindex="12" class="NoBorder"/>
				Local
				<input type="radio" id="radTime_Z" name="radTime" value="Z"  tabindex="13" class="NoBorder"/>
				Zulu
				</font>
			</td>	
			<td><font>Seat Factor</font></td>
			<td colspan="6">
				<table width="100%" border="0" cellpadding="0" cellspacing="2" id="Table9">
					<tr><td>
						<font>min</font>&nbsp;
						<input type="text" id="txtMinSeatFactor" name="txtMinSeatFactor" size="5" tabindex="14" class="inputSm"/>
						&nbsp;<font>%</font>
					<!--  <td align="center"><div id="seatFactor" style="width:160px;height:5px"></div></td> -->
					&nbsp;<font>max</font>
					&nbsp;<input type="text" id="txtMaxSeatFactor" name="txtMaxSeatFactor" size="5" tabindex="15" class="inputSm"/>
					&nbsp;<font>%</font>
					</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<font>Frequency	</font>
			</td>			
			<td colspan="4">
				<table width="99%">
					<tr>
						<td align="left">
							<input type="checkbox" id="chkSunday" name="chkSunday" class="NoBorder" tabindex="16" title="Sunday"/>
							<font>Su</font>
							<input type="checkbox" id="chkMonday" name="chkMonday" class="NoBorder" tabindex="17" title="Monday"/>
							<font>Mo</font>
							<input type="checkbox" id="chkTueday" name="chkTueday" class="NoBorder" tabindex="18" title="Tuesday"/>
							<font>Tu</font>
							<input type="checkbox" id="chkWedday" name="chkWedday" class="NoBorder" tabindex="19" title="Wednesday"/>
							<font>We</font>
							<input type="checkbox" id="chkThuday" name="chkThuday" class="NoBorder" tabindex="20" title="Thursday"/>
							<font>Th</font>
							<input type="checkbox" id="chkFriday" name="chkFriday" class="NoBorder" tabindex="21" title="Friday"/>
							<font>Fr</font>
							<input type="checkbox" id="chkSatday" name="chkSatday" class="NoBorder" tabindex="22" title="Saturday"/>
							<font>Sa</font>
						</td>
					</tr>
				</table>
			</td>
			<td colspan="5" align="right">
				<input name="btnClear" type="button" class="Button" id="btnClear" value="Reset"  tabindex="23"/>
				<input name="btnSearch" type="button" class="Button" id="btnSearch" value="Search"  tabindex="24"/>
			</td>
		</tr>
	</table>
	</div>
	<div id="SeatAllocation" class="FormBackGround">
		<table width="100%" border="0" cellpadding="0" cellspacing="1" >
			<tr><td style="font-size:2px" colspan="5">&nbsp;</td></tr>
			<tr>
				<td width="15%"><font><span id="idFlightLable"></span></font></td>
				<td width="30%"><font><span id="idDepature"></span></font></td>
				<td width="25%"><font><span id="idArrival"></span></font></td>
				<td>&nbsp;</td>
				<td align="right"><a href="javascript:void(0)" id="showFlightonPanel" title="Show/Hide flight search result"><img src="../../images/AU_no_cache.gif" border="0"/><font> Flight Search Results</font></a>
				<span id="spnFlightSearchResults" style="position:absolute; z-index:20;top:60px;left:5px;border:1px solid;width:926px;height:369px;overflow-y:auto;overflow-x:hidden;text-align:left"></span></td> 
			</tr>
			<tr><td style="font-size:2px" colspan="5">&nbsp;</td></tr>
			<tr>
				<td><font><span id="idModel"></span></font></td>
				<td><font><span id="idCapacity"></span></font></td>
				<td><font><span id="idAsk"></span></font></td>
				<td><b><font style="color:#FF0000"><span id="idOverlap" name="idOverlap" style="display:none">Overlapping Flight</span></font></b></td>
				<td align="right" id="COSList">
					<select id="selCOS" name="selCOS" size="1" style="width:100px">
					</select>
				</td>
			</tr>
		</table>
	</div>
	<div id="segmentAllocation" class="FormBackGround">
		<div id="tblSegmentAllocations">
			
		</div>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<u:hasPrivilege privilegeId="plan.invn.alloc.bc.segupdate">
						<input type="button" name="btnSegEdit" id="btnSegEdit" class="Button" value="Edit" />
						<input type="button" name="btnUnAdd" id="btnUnAdd" class="Button" value="Add Note" />	
						<input type="button" name="btnSplit" id="btnSplit" class="Button" value="Split" />
					</u:hasPrivilege>				
					
					<u:hasPrivilege privilegeId="plan.invn.alloc.bc.viewNotes,plan.invn.alloc.bc.segupdate">
						<input type="button" name="btnUnView" id="btnUnView" class="Button" value="View Note" />
					</u:hasPrivilege>			
				</td>
				<td width='49%'>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="3%" id="tdBCOverload" class="fltStatus03"></td>
							<td><font>&nbsp;Overloaded Flight</font></td>
							<td width="3%" id="tdBCOverLap" class="fltStatus04"></td>
							<td><font>&nbsp;Overlapped Flight</font></td>
							<td width="3%" id="tdBCClsSeg" class="fltStatus02"></td>
							<td><font>&nbsp;Closed Segment</font></td>
							
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<div id="cabinClassTop" style="display:none">
		<table width="40%" border="0" cellpadding="0" cellspacing="0">
			<tr><td style="font-size:4px">&nbsp;</td></tr>
			<tr>
				<td align='right'>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="3%" id="tdBCManClose" class="bcStatus01"></td>
							<td><font>&nbsp;Manually Closed</font></td>
							<td width="3%" id="tdBCManOpen" class="bcStatus02"></td>
							<td><font>&nbsp;Manually Opened</font></td>
							<td width="3%" id="tdBCAutCls" class="bcStatus03"></td>
							<td><font>&nbsp;System Closed</font></td>
							<td width="3%" id="tdBCAutOpen" class="bcStatus04"></td>
							<td><font>&nbsp;System Opened</font></td>
							<td width="3%" id="tdBCLwrSTDCls" class="bcStatus05"></td>
							<td><font>&nbsp;STD Closure</font></td>
							<td width="3%" id="tdBCGroupCls" class="bcStatus06"></td>
							<td><font>&nbsp;Group Closure</font></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<div id="cabinClassSection" style="height:320px;overflow-y:auto">
		<div id="cabinCodeAllocTemplate" class="ui-widget ui-widget-content ui-corner-all cabinCodeAllocTemplateC " style="margin-top: 5px; display: none;background: #ECECEC">
			<div class="ui-dialog-titlebar ui-widget-header ui-corner-all panelHeader ui-helper-clearfix" style="height:15px;padding-top:2px;padding-left:5px;">
				<table  width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width= '30%' align='left'><b> Booking Code Allocations ::<span id="spnBCSegmentCode"></span></b>
						</td>
						<td width= '30%' align='center'><c:if test="${requestScope.interLineDisplay == 'true'}"><a href="javascript:void(0)"  title="Description to be shown" id="marketingCarrierSummary" name="marketingCarrierSummary">Marketing Carrier Summary ::<img src="../../images/AD_no_cache.gif" border="0" align="absmiddle"/></a></c:if></td>
						<td width="40%" align='right'><a href="javascript:void(0)" title="Show/Hide Seat movement" title="Show/Hide Seat movement" id="seatMovementOptionInfo" name="seatMovementOptionInfo">Seat Movement Information ::</a>
						<span id="spnSeatMovementsInfo"  style="position:absolute; right:100px;display:none; z-index:5"> -->
						</span>
						</td>
					</tr>
				</table>	
			</div>
			<div>
				<span id="BookingCodeAllocationGrid">
				
				</span>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr><td colspan="4" style="font-size:2px">&nbsp;</td></tr>
					<tr>
						<td width="25%"><font><b>Total BC Alloc : </b></font>
							<font><span id="idTotBC"></span></font>&nbsp;&nbsp;&nbsp;&nbsp;
							<font><b>Fixed Alloc : </b></font>
							<font><span id="idFixAllo"></span></font>
						</td>
						<td align="right" width="20%">
							<font>Check/Uncheck All Priority <input type="checkbox" id="checkAllPriority" name="checkAllPriority"/></font>
						</td>
						<td align="right" width="20%">
							<u:hasPrivilege privilegeId="plan.invn.alloc.bc.status">
								<font>Check/Uncheck All Closed <input type="checkbox" id="checkAllClosed" name="checkAllClosed"/></font>
							</u:hasPrivilege>
						</td>
						<td align="right" width="35%">
								<font>Booking Class</font>
								<select id="selBookingCode" name="selBookingCode" size="1" style="width:90px">
								</select>
								<u:hasPrivilege privilegeId="plan.invn.alloc.bc.add">
									<input type="button" name="btnAddBC" id="btnAddBC" class="Button" value="Add" style="width:40px"/>							
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="plan.invn.alloc.bc.update">	
									<input type="button" name="btnEditBC" id="btnEditBC" class="Button" value="Edit" style="width:40px" />
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="plan.invn.alloc.bc.delete">	
									<input type="button" name="btnDeleteBC" id="btnDeleteBC" class="Button" value="Delete" style="width:50px" />
								</u:hasPrivilege>
						</td>	
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div id="buttonSetTable" class="">
		<table width="100%" border="0" cellpadding="0" cellspacing="2">
			<tr>
				<td>
					<input type="button" id="btnClose_Aloc" id="btnClosebtnClose_Aloc" class="Button" value="Close"/>
					<input type="button" id="btnBack" id="btnBack" class="Button" value="Back"/>
				</td>
				<td>
				<a href="javascript:void(0)" id="showRollforwardAudit" title="Show/Hide roll forward audits"><img src="../../images/AU_no_cache.gif" border="0"/><font> Roll Forward Audits</font></a>
				<span id="spnRollForwardAudits" style="position:absolute; top:460px;left:200px;border:1px solid;"></span>
					<div id="tblRollFwdAudit">
					</div>
				</td>
				<td align="right">
					<input type="button" id="btnPrintBookingClasses" name="btnPrintBookingClasses" class="Button" value="Print"  style="visibility:visible;" />
					<u:hasPrivilege privilegeId="sys.mas.mgmt.promo.request">
						<input type="button" id="btnPromotion" name="btnPromotion" class="Button" value="Check Promotion"  style="visibility:visible;width:100px;" />
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="plan.invn.alloc.baggage">
						<input type="button" id="btnBaggageAllocation" name="btnBaggageAllocation" class="Button" value="Link Baggage"  style="visibility:show;width:105px;" />
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="plan.invn.alloc.meal">
						<input type="button" id="btnMealAllocation" name="btnMealAllocation" class="Button" value="Link Meals"  style="visibility:visible;" />
					</u:hasPrivilege>								
					<u:hasPrivilege privilegeId="plan.invn.alloc.seat">
						<input type="button" id="btnSeatCharge" name="btnSeatCharge" class="Button" value="Seat Charge"  style="width:90px" style="visibility:visible;" />
					</u:hasPrivilege>								
					<u:hasPrivilege privilegeId="plan.invn.alloc.bc.roll">
						<input type="button" id="btnRollForward" name="btnRollForward" class="Button" value="Roll Forward"  style="visibility:visible;width:90px" />
					</u:hasPrivilege>
					<input type="button" name="btnSaveBC" id="btnSaveBC" class="Button" value="Save" />
				</td>
			</tr>
		</table>
	</div>
	<div id="flightResults" class="FormBackGround">
		<div id="tblSearchFlights">
			
		</div>
		<input type="button" id="btnClose" class="Button" name="btnClose" value="Close"  onclick="top[1].objTMenu.tabRemove('SC_INVN_001')"/>
	</div>
   	<script src="../../js/v2/manageseatallocation/mISearch.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
   	<div id="popUp" class="myPopup"></div>
   	<div id="spnSeatMovementOptionInfo" class="myPopup"></div>
   	<form id="frmManageSeatAllocate" action="" method="post"></form>
   	<input type="hidden" name="flightRollForwardAuditSummary" id="flightRollForwardAuditSummary" value="" style="width:500px;"/>
   	
 </body>
</html>