<%-- 
	 @Author 	: 	Baladewa Welathanthri
--%>
<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
    <title>Setup Charges</title>
    <%@ include file='../common/pageHD.jsp' %>		
  	</head>
	<body class="tabBGColor" style="overflow-X:hidden; overflow-Y:auto;" oncontextmenu="return false" ondrag='return false'>
  		<div id='pop' name='pop' style='position: absolute; z-index:1000; left:30%; top:25%; height: 200px; width: 500; visibility:hidden;'></div>
		<div id='popagent' name='popagent' style='position: absolute; z-index:1000; left:30%; top:25%; height: 200px; width: 500; visibility:hidden;'></div>
		<form action="showCharges.action" name="frmManageCharges" id="frmManageCharges" method="post">
			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td><%@ include file="../../common/IncludeMandatoryText.jsp"%></td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/includeAdminPanelTop.jsp"%>
						<img src="../../images/bullet_small_no_cache.gif"/> Search Charges<%@ include file="../common/includeAdminPanelHeader.jsp"%>
						<table width="100%" border="0" cellpadding="1" cellspacing="0" ID="Table9">
							<tr style="height:1px">
								<td width="10%"><font>Charge Code</font></td>
								<td width="10%"><font>Group</font></td>
								<td width="10%"><font>Category</font></td>
								<td colspan="4"><font>Show Charge With Rates</font> </td>
								<td width="10%"><font>From Date</font></td>
								<td width="2%"><font>&nbsp;</font></td>
								<td width="10%"><font>To Date</font></td>
								<td width="2%"><font>&nbsp;</font></td>
								<td align="right" rowspan="2" valign="bottom">	
									<input type="button" id="btnSearch" name="btnSearch" value="Search" class="button"/>
								</td>
							</tr>
							<tr>
								<td>
									<select id="selChargeCode" name="selChargeCode" style="width:74px">
										<OPTION value="All">All</OPTION>
										<c:out value="${requestScope.reqChargeCodeList}" escapeXml="false" />
									</select>
								</td>
								<td>
									<select id="selGroup" name="selGroup" style="width:80px" >
										<OPTION value="All">All</OPTION>
										<c:out value="${requestScope.reqGroupList}" escapeXml="false" />
									</select>
								</td>
								<td>
									<select id="selCategory" name="selCategory" style="width:80px">
										<OPTION value="All">All</OPTION>
										<c:out value="${requestScope.reqCategoryList}" escapeXml="false" />
									</select>
								</td>	
								<td>
									<input type="radio" id="radRateCat" name="radRateCat"   class="NoBorder" value="NoCharges" >
								</td>
								<td ><font>Without rates</font></td>
								<td>
									<input type="radio" id="radEffective" name="radRateCat"   class="NoBorder"  value="WithDateRange" ></td><td><font>Effective During</font>
								</td>
								<td colspan="2">
									<input type="text" id="txtDateFrom" name="txtDateFrom" style="width:75px;" maxlength="12" invalidText="true">
								</td>
								<td colspan="2">
									<input type="text" id="txtDateTo" name="txtDateTo" style="width:75px;" maxlength="12" invalidText="true">
								</td>
								
							</tr>
						</table>
					  	<%@ include file="../common/includeAdminPanelBottom.jsp"%>
					</td>
				</tr>			
				<tr>
					<td>
						<%@ include file="../common/includeAdminPanelTop.jsp"%>
						<img src="../../images/bullet_small_no_cache.gif"/> Charges<%@ include file="../common/includeAdminPanelHeader.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="1" ID="Table9">
							<tr>
								<td>
							  		<table id="spnMC"></table> 
							  		<div id="spnMCPager"></div> 
								</td>
							</tr>
							<tr>
								<td>
									<u:hasPrivilege privilegeId="plan.fares.charge.add">
										<input type="button" id="btnAdd" name="btnAdd" class="Button" value="Add" />
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="plan.fares.charge.edit">
										<input type="button" id="btnEdit" name="btnEdit" class="Button" value="Edit" />
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="plan.fares.charge.delete">
										<input type="button" id="btnDelete" name="btnDelete" class="Button" value="Delete" />
									</u:hasPrivilege>
								</td>
							</tr>
						</table>
					  	<%@ include file="../common/includeAdminPanelBottom.jsp"%>
					</td>
				</tr>
				<tr>
				<td class="formSection">
				<div id="tabs" class="admin-panel ui-border-topDis" style="display:none">
					<ul class="ui-border-leftDis ui-border-rightDis">
						<li id="tabAddModChargeTop"><a href="#tabAddModCharge">Add/Modify Charges</a></li>
						<li id="tabAddModChargeRateTop"><a href="#tabAddModChargeRate">Add/Modify Charge Rate Values</a></li>
						<li id="tabReportingTop"><a href="#tabReporting">For Reporting </a></li>
						<li id="tabPSOTop"><a href="#tabPOS">POS</a></li>
						<li id="tabAgentTop"><a href="#tabAgent">Agent</a></li>
					</ul>
					<div id="tabAddModCharge" style="height: 200px;overflow-y: auto;overflow-x: hidden">
						<table width="100%" border="0" cellpadding="0" cellspacing="2" align="left">
							<tr>
								<td width="18%" height="20"><font>Charge Code</font></td>	
								<td width="18%" align="left">
									<input type="text" id="txtChargeCode" name="txtChargeCode" size="7" maxLength="7"/><font class="mandatory">*</font>
								</td>
								<td>
									<div id="divCurrSelect">
										<table border="0" cellpadding="0" cellspacing="0" align="left">
											<tr>
												<td><font class="fntBold">Show Charges In</font> </td>
												<td >
													<input type="radio" id="radCurr" name="radCurr" value="BASE_CURR"  
													class="NoBorder" checked="checked" />
												</td>
												<td><font>&nbsp;Base Currency</font></td>
												<td>&nbsp;</td>
												<td>
													<input type="radio" id="radSelCurr" name="radCurr" value="SEL_CURR"  
													class="NoBorder" />
												</td>
												<td><font>&nbsp;Selected Currency</font></td>
												<td>&nbsp;</td>
												<td>
													<select name="selCurrencyCode" id="selCurrencyCode" size="1" style="width:105;">
													<option value="-1"></option> 
													</select><font class="mandatory"></font>
												</td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
							<tr>
								<td width="18%" height="20"><font>Group</font></td>
								<td width="18%">
									<select id="AddSegselGroup" size="1" name="AddSegselGroup" style="width:65px"  >
									<option value=""></option>
									</select><font class="mandatory">*</font>
								</td>
								<td width="64%" rowspan="2">
								<fieldset style="border:1px solid #666; padding: 3px 6px"> 
								<legend><font>Transits</font></legend>
									<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
										<tr>
											<td width="17%" align="center">
												<font>Inward Transit</font>
											</td>
											<td width="17%" align="center">
												<font>Outward Transit</font>
											</td>
											<td width="17%" align="center">
												<font>Ond Transit</font>
											</td>
											<td width="23%" align="center">
												<font>Min Transit Duration</font>
											</td>
											<td width="23%" align="center">
												<font>Max Transit Duration</font>
											</td>
										</tr>
										<tr>
											<td align="center">
											<select name="chkInwardTrn" id="chkInwardTrn" onchange = "controlTrnsitInputs()">
												<option value="N"></option>
												<option value="I">INCLUDE</option>
												<option value="E">EXCLUDE</option>																									
											</select>
											
											</td>
											<td align="center">
											<select name="chkOutwardTrn" id="chkOutwardTrn" onchange = "controlTrnsitInputs()">
												<option value="N"></option>
												<option value="I">INCLUDE</option>
												<option value="E">EXCLUDE</option>															
											</select>
											</td>
											<td align="center">
											<select name="chkAllTrn" id="chkAllTrn" onchange = "controlTrnsitInputs()">
												<option value="N"></option>
												<option value="I">INCLUDE</option>
												<option value="E">EXCLUDE</option>															
											</select>
											</td>
											<td align = "center"><input type="text" maxLength="7" name="hdnMinTrnDuration" id="hdnMinTrnDuration" size="5" onblur="setTimeWithColon(document.forms[0].hdnMinTrnDuration)" onchange = "controlTrnsitInputs()">
											<td align = "center"><input type="text" maxLength="7" name="hdnMaxTrnDuration" id="hdnMaxTrnDuration" size="5" onblur="setTimeWithColon(document.forms[0].hdnMaxTrnDuration)" onchange = "controlTrnsitInputs()">
											</td>
										</tr>
									</table>
								</fieldset>
								</td>
							</tr>
							<tr>
								<td width="18%" height="20"><font>Description</font></td>
								<td width="18%">
									<input type="text" id="txtDesc" name="txtDesc" width="140px" maxLength="30"/><font class="mandatory">*</font>
								</td>
							</tr>
							<tr>
								<td align ="left" width="18%" height="20">
									<font>Journey Type</font>
								</td>
								<td align="left">
									<select id="selJourney"  name="selJourney" size="1" style="width: 130px">
										<option value="A">All</option>
										<option value="E">All Excluding Short Transit</option>
										</select><font class="mandatory">*</font>
								</td>
								<td>
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr>
											<td align="left" width="15%">
												<font class="fntBold">Category</font><font class="mandatory">*</font>
											</td>
											<td align="left"  width="5%">
												<input type="radio" id="radCat1" name="radCat1" value="GLOBAL"  class="NoBorder" />
											</td>
											<td align="left" width="10%"><font>GLOBAL</font></td>
											<td  width="5%">
												<input type="radio" id="radPos" name="radCat1" value="POS"  class="NoBorder" / >
											</td>
											<td align="left" width="10%"><font>POS</font></td>
											<td width="5%">
												<input type="radio" id="radSegment" name="radCat1" value="OND"  class="NoBorder" />
											</td>
											<td align="left" width="10%"><font>OND</font></td>
											<td align="left" width="40%">
												<!--<span id="spnPOS">
													<input type="button" value= "POS" class="Button" style="height: 19px" />
												</span>
												 <span id="spnONDAgent">
													<input type="button" value= "Agents" class="Button" style="height: 19px" />
												 </span>
											--></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="18%" height="20"><font>Refundable</font></td>
								<td width="18%">
								<input type="checkbox" id="chkRefundable" name="chkRefundable" class="NoBorder">
								</td>
								<td rowspan="5">
									<span id="spnSegment">
										<table border="0" width="100%">
											<tr>
												<td width="17%"><font>Departure</font></td>
												<td >
										  			<select id="selDepature" name="selDepature" size="1" style="width:60px;">
														<option value="All">All</option>
													</select>
												</td>
												<td width="16%"><font>Arrival</font></td>
												<td>
													<select id="selArrival" name="selArrival" size="1" style="width:60px;">
														<option value="All">All</option>
													</select>
												</td>
											</tr>
											<tr>
												<td width="17%"><font>Via 1</font></td>
												<td colspan="2">
													<select id="selVia1" name="selVia1" size="1" style="width:60px;">
														<OPTION value=""></OPTION>
													</select>
												</td>
												<td rowspan="4">
													<select id="selSelected" size="3" name="selSelected" style="width:150;height:70" multiple>
													</select>
												</td>
											</tr>
											<tr>
												<td width="17%"><font>Via 2</font></td>
												<td ><select id="selVia2" name="selVia2" size="1" style="width:60px;">
														<OPTION value=""></OPTION>
													</select>
												</td>
												<td align="center"><input type="button" id="btnAddSeg" name="btnAddSeg" class="Button" value=">" style="width:40px;height:17px" > </td>
												<td></td>
											</tr>
											<tr>
												<td width="17%"><font>Via 3</font></td>
												<td ><select id="selVia3" name="selVia3" size="1" style="width:60px;">
														<OPTION value=""></OPTION>
													</select>
												</td>
												<td align="center"><input type="button" id="btnRemSeg" name="btnRemSeg" class="Button" value="<" style="width:40px;height:17px" > </td>
												<td></td>
											</tr>
											<tr>
												<td width="17%"><font>Via 4</font></td>
												<td colspan="3"><select id="selVia4" name="selVia4" size="1" style="width:60px;">
														<OPTION value=""></OPTION>
													</select>
												</td>												
											</tr>
										</table>
									</span>
								</td>
							</tr>
							<tr id="refundabileForMOD">
								<td width="18%" height="20"><font>Refundable only for modifications</font></td>
								<td width="18%">
									<input type="checkbox" id="chkRefundableOnlyMOD" name="chkRefundableOnlyMOD" class="NoBorder">
								</td>
							</tr>
							<tr>
								<td width="18%" align="left" height="20"><font>Active</font></td>
								<td width="18%" align="left">
									<input type="checkbox" id="chkStatus" name="chkStatus" class="NoBorder">
								</td>
							</tr>
							<tr>
								<td align="left" width="18%" height="20"><font>Apply To</font></td>
								<td align="left" width="18%">
									<select name="selApplyTo" id="selApplyTo">
									</select>
								</td>
							</tr>
							<tr>
								<td align="left" width="18%" height="20">
									<font>Apply OND Seg.</font>
								</td>
								<td align="left">
									<select name="selOndSeg" id="selOndSeg">
										<option value="A">All</option>
										<option value="F">First</option>
										<option value="L">Last</option>
									</select>
								</td>									
							</tr>
							<tr>
								<td align="left" width="18%" height="20">
									<font>Sales channel</font>
								</td>
								<td align="left">
									<select name="selChannel" id="selChannel">
										<option value="-1">All</option>
										<option value="4">IBE</option>
										<option value="3">XBE</option>
									</select>
								</td>									
							</tr>
							<tr>
								<td colspan="3" height="20">&nbsp;</td>
							</tr>
					</table>			  
					</div>
					<div id="tabAddModChargeRate" style="height: 200px;overflow-y: auto;overflow-x: hidden">
					
						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
							<tr>
								<td style="height:80px;" align="left">
									<table width="720" border="0" cellpadding="0" cellspacing="0" align="left">
										<thead>
											<tr>
												<td width="15" align="center">&nbsp;</td>
												<td width="90" align="center"><font>Effect From</font></td>
												<td width="90" align="center"><font>Effect To</font></td>
												<td width="50" align="center"><font>Flag</font></td>
												<td width="60" align="center"><font>Value %</font></td>
												<td width="80" align="center"><font>Min Value</font></td>
												<td width="80" align="center"><font>Max Value</font></td>
												<td width="80" align="center"><font>Make Value</font></td>
												<td width="80" align="center"><font>Modify Value</font></td>
												<td width="80" align="center"><font>Loc. Curr.</font></td>
												<td width="25" align="center"><font>Act</font></td>
											</tr>
										</thead>
										<tbody>
											<tr><td colspan="11">
												<div>
												<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
													<tr id="ChargeRateTemplate" style="display:none">
														<td width="15" align="center"><label id="reqid"></label></td>
														<td width="90" align="center"><input type="text" id="effFrom" name="effFrom" style="width:80px"/></td>
														<td width="90" align="center"><input type="text" id="effTo" name="effTo" style="width:80px"/></td>
														<td width="50" align="center"><select id="selPer" name="selPer" style="width:40px"></select></td>
														<td width="60" align="center"><input type="text" id="ratevalue" name="ratevalue" style="width:50px"/></td>
														<td width="80" align="center"><input type="text" id="minval" name="minval" style="width:50px"/></td>
														<td width="80" align="center"><input type="text" id="maxval" name="maxval" style="width:50px"/></td>
														<td width="80" align="center"><input type="text" id="makeVal" name="makeVal" style="width:50px"/></td>
														<td width="80" align="center"><input type="text" id="modifyVal" name="modifyVal" style="width:50px"/></td>
														<td width="80" align="center" ><input type="text" id="localCurr" name="localCurr" style="width:50px"/></td>
														<td width="25" align="center"><input type="checkbox" id="status" name="status" /></td>
													</tr>
												</table>
												</div>
											</td></tr>
										</tbody>
										<tfoot>
											<tr>
												<td colspan="9">
													<u:hasPrivilege privilegeId="plan.fares.charge.rate.add">
														<input type="button" id="btnGAdd" value="Add" class="Button" />
													</u:hasPrivilege>
													<u:hasPrivilege privilegeId="plan.fares.charge.rate.add">
														<input type="button" id="btnGEDit" name="btnGEDit" value="Edit" class="Button" />
													</u:hasPrivilege>													
												</td>
											</tr>
										</tfoot>
									</table>
								</td>
							</tr>									
						</table>
							
					</div>
					<div id="tabReporting" style="height: 200px;overflow-y: auto;overflow-x: hidden">
						<table width="100%" border="0" cellpadding="0" cellspacing="1" align="center">											
							<tr>										
								<td align="left" width="18%"><font>Charge Group</font></td>
								<td align="left">										
									<select id="selReportingGroup"  name="selReportingGroup" size="1" style="width:130px" onChange="required_changed()">
										<OPTION value=""></OPTION>
									</select>
								</td>
								<td width="62%">&nbsp;</td>
							</tr>
							<tr>
							<td colspan="2">
								<fieldset style="border:1px solid #666; padding: 3px 6px"> 
								<legend><font>Charges Applicable Journey</font></legend>
									<table width="100%" cellspacing="1" cellpadding="0">
										<tr>
											<td align="left">
											<input type="radio" id="radDept1" name="radDept1" value="D"  class="NoBorder"  onClick="depatureClicked()"><font>&nbsp;Departure</font>												
											</td>
											<td align="left">
											<input type="radio" id="radArr" name="radDept1" value="A"  class="NoBorder"onClick="depatureClicked()"  ><font>&nbsp;Arrival</font>
											</td>
										</tr>
									</table>
								</fieldset>
							</td>
							<td width="62%"></td>	
							</tr>
							<tr>	
								<td colspan="2">														
									<span id="spnRevenue">
										<table  width="100%" border="0" cellpadding="0" cellspacing="1" align="left">
											<tr><td align="left" width="49%"> <font>Revenue&nbsp;</font></td>
												<td align="left"><input type="checkbox" id="chkRevenue" name="chkRevenue" class="NoBorder">
												</td>
											</tr>
											<tr>
												<td align ="left" width="49%">	<font>Revenue %&nbsp;</font></td>
													<td align="left" >
													<input type="text" id="txtRevenue" name="txtRevenue" style="width:75px;" size="5" class="rightText" maxlength="5" onChange="required_changed()" onKeyPress="RevenuePress(this)" onKeyUp="RevenuePress(this)" tabindex="19">
												</td>
											</tr>
										</table>
									</span>
								</td>
								<td width="62%"></td>																														
							</tr>
						</table>
					</div>
					<div id="tabPOS" style="height: 200px;overflow-y: auto;overflow-x: hidden">
						<table>
							<tbody>
								<tr>
									<td align="left">
										<input type="radio" class="NoBorder" value="Include" name="radInc1" id="radInc1"><font>Include / </font>
										<input type="radio" class="NoBorder" value="Exclude" name="radInc1" id="radInc2"><font>Exclude the selected agents</font>
									</td>
								</tr>
								<tr>
									<td align="top">
										<span id="spnAgents">
										<select multiple="" style="width: 170px; height: 100px;" class="ListBox" id="lstStations">
										</select>
									</span>
								</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div id="tabAgent" style="height: 200px;overflow-y: auto;overflow-x: hidden">
						<table>
							<tbody>
								<tr>
									<td align="left">
										<input type="radio" class="NoBorder" value="Include" name="radAgtInc1" id="radAgtInc1"><font>Include / </font>
										<input type="radio" class="NoBorder" value="Exclude" name="radAgtInc1" id="radAgtInc2"><font>Exclude the selected agents</font>
									</td>
								</tr>
								<tr>
									<td align="top">
										<span id="spnAgents">
											<select multiple="" style="width: 170px; height: 100px;" class="ListBox" id="lstAgents">
											</select>
									</span>
								</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="admin-inner-panel">		
						<table width="97%" border="0" cellpadding="0" cellspacing="0" align="center">
							<tr>									
								<td class="FormBackGround">						
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
				    						<td>
				          							<input type="button" id="btnClose" class="Button" value="Close" onclick="closeClick()">
				              						<input name="btnReset" type="button" class="Button" id="btnReset" value="Reset" onClick="resetClick()">					               
				          						</td>
				          						<td align="right" colspan="2">
					 							<input name="btnSave" type="button" class="Button" id="btnSave" value="Save">
											</td>
												</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
			</div>
		</td>
	</tr>
	</table>	
	<input type="hidden" id="hdnRateIDs" name="hdnRateIDs"  value="" />
	<input type="hidden" id="hdnMode" name="hdnMode"  value="" />
	<input type="hidden" id="hdnRateMode" name="hdnRateMode"  value="" />
	<input type="hidden" id="hdnVersion" name="hdnVersion"  value="" />
	<input type="hidden" id="hdnRateVersion" name="hdnRateVersion"  value="" />
	<input type="hidden" id="hdnRecNo" name="hdnRecNo"  value="1" />
	<input type="hidden" id="hdnData" name="hdnData"  value="" />
	<input type="hidden" id="hdnChargeData" name="hdnChargeData"  value="" />
	<input type="hidden" id="hdnChargeCode" name="hdnChargeCode"  value="" />
	<input type="hidden" id="hdnSegments" name="hdnSegments"  value="" />
	<input type="hidden" id="hdnStation" name="hdnStation"  value="" />
	<input type="hidden" id="hdnGroup" name="hdnGroup"  value="" />
	<input type="hidden" id="hdnStatus" name="hdnStatus"  value="" />
	<input type="hidden" id="hdnDesc" name="hdnDesc"  value="" />
	<input type="hidden" id="hdnCat" name="hdnCat"  value="" />
	<input type="hidden" id="hdnApplyTo" name="hdnApplyTo"  value="" />
	<input type="hidden" id="hdnRefund" name="hdnRefund"  value="" />
	<input type="hidden" id="hdnRefundOnlyForMOD" name="hdnRefundOnlyForMOD"  value="" />
	<input type="hidden" id="hdnSelSelected" name="hdnSelSelected"  value="" />
	<input type="hidden" id="hdnpayType" name="hdnpayType"  value="" />			
	<input type="hidden" id="hdnRowNum" name="hdnRowNum"/>						
	<input type="hidden" id="hdnOndSeg" name="hdnOndSeg"  value="" />
	<input type="hidden" id="hdnChannel" name="hdnChannel"  value="" />
	<input type="hidden" id="hdnJourney" name="hdnJourney"  value="" />
	<input type="hidden" id="hdnStations" name="hdnStations"  value="" />
	<input type="hidden" id="hdnInc" name="hdnInc"  value="" />
	<input type="hidden" id="hdnAgents" name="hdnAgents"  value="" />
	<input type="hidden" id="hdnIncOnd" name="hdnIncOnd"  value="" />
	<input type="hidden" id="hdnAgent" name="hdnAgent"  value="" />
	</form>
	</body>
	<script src="../../js/v2/manageCharges/ChargesValidate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/manageCharges/ManageCharges.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript">

 <c:out value="${requestScope.reqActStationList}" escapeXml="false"></c:out>

 <c:out value="${requestScope.reqAgentList}" escapeXml="false"></c:out>

 <!--
   objDG = {};
   var objProgressCheck = setInterval("ClearProgressbar()", 300);

   function ClearProgressbar(){
   	
		if(typeof(objDG) == "object"){
			if (objDG.loaded){
				clearTimeout(objProgressCheck);
				top[2].HideProgress();
		 	}
	 	}
    
   }
   //-->
  </script>


</html>