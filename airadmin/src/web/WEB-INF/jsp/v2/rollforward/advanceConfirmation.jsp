<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Roll Forward Confirmation</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
<LINK rel="stylesheet" type="text/css"
	href="../../css/Style_no_cache.css" />
<LINK rel="stylesheet" type="text/css"
	href="../../themes/default/css/ui.all_no_cache.css" />
<LINK rel="stylesheet" type="text/css"
	href="../../themes/default/css/jquery.ui.autocomplete.css" />


<script
	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript"
	src="../../js/v2/jquery/jquery.watermark.min.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<!-- <script type="text/javascript"
	src="../../js/v2/jquery/jquery.ui.dialog.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script> -->

<script
	src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/promotion/popupData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/v2/rollforward/advanceConfirmation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script type="text/javascript">
	var languageHTML = "<c:out value="${requestScope.reqLanguageList}" escapeXml="false" />";
</script>
<style>
.innerPanel {
	width: 98%;
	margin: 0 auto;
}

.portSel {
	width: 50px
}

select {
	margin: 1px 1px;
}

.iconBtn {
	width: 25px
}

#ui-datepicker-div {
	display: none;
}

.ui-autocomplete {
	height: 200px;
	overflow-y: scroll;
	overflow-x: hidden;
}

.input.uppercase {
	text-transform: uppercase;
}

.period {
	padding-top: 5px;
	text-decoration: underline;
}

#blacklistPaxDetails {
	padding-top: 10px;
	padding-bottom: 10px;
	margin-top: 5px;
	border: solid 1px #aaaaaa;
}
</style>
</head>
<body class="tabBGColor" scroll="no"
	onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false"
	onkeydown='return Body_onKeyDown(event)' ondrag='return false' onload="constructRollForwardBatchGrid()">
	<script type="text/javascript">
		var batchId = "<c:out value="${param.savedBatchId}" escapeXml="false" />";
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	</script>
		
	<table width="98%" border="0" cellpadding="0" cellspacing="2"
		align="center" ID="TableGRIDResults">
		<tr>
		<td width="30"><img src="../../images/spacer_no_cache.gif"
			width="100%" height="1"></td>
		<td>
			<div style="border-bottom: 1px solid #bbb">
			
				<font class="Header"><span id="statusMsg">Roll forward Batch processing in progress....</span></font><br>
				<font class="Header">Roll-Forward Batch ID : <c:out value="${param.savedBatchId}" escapeXml="false" /></font>
			</div>
			<div id="divRollForwardConfirm"
			style="text-align: left; background-color: #ECECEC;">
				<div class="innerPanel divsearch" style="border-bottom: 1px solid #bbb">
				</div>
				<div class="innerPanel divResults" style="border-top: 1px solid #fff">
					<div id="jqGridsearchRollForwardContainer" align="center"
						style="margin-top: 5px;">
						<table width="100%" border="0" cellpadding="0" cellspacing="0"
							id="jqGridsearchRollForwardData">
						</table>
						<div id="jqGridsearchRollForwardPages"></div>
					</div>
				</div>
			</div>
			<table width="98%" border="0" cellpadding="0" cellspacing="2"
			align="center" ID="Table1">
				<tr>
					<td colspan="3" style="height: 42px;" valign="bottom"><input
						name="btnClose" type="button" id="btnClose" value="Close"
						onclick="window.close()"
						class="btnDefault button"></td>
		
				</tr>
			</table>
		</td>
		<td width="30"><img src="../../images/spacer_no_cache.gif"
			width="100%" height="1"></td>
	</tr>
	</table>
	<div id="popUp" class="myPopup"></div>
	<script type="text/javascript"
		src="../../js/v2/rollforward/advanceConfirmation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
		type="text/javascript"></script>

</body>
</html>