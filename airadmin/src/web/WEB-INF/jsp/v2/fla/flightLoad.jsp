<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Flight Load</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="-1">    
    <LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
    <%@ include file="inc_pgHD.jsp" %>
    <script src="../../js/common/OVERLIB.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/ToolTip.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.json.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
 <script type="text/javascript">
      google.load('visualization', '1', {packages: ['barchart']});
    </script>
    <script type="text/javascript">
	<!--

	var DATA_MIS = new Array();
	DATA_MIS["initialParams"] = eval('(' + '<c:out value="${requestScope.req_InitialParams}" escapeXml="false" />' + ')');
	//-->
	</script>
  </head>
  <body class="tabBGColor" scroll="no" onkeypress='return Body_onKeyPress(event)' onkeydown='return Body_onKeyDown(event)'><form id='frmAction' name='frmAction' method='post' action=''>
	<table width='100%' border='0' cellpadding='0' cellspacing='0'>
  		<tr>
			<td>
				<%@ include file="../../common/IncludeFormTop.jsp"%></td>
			<td class="FormHeadBackGround">
				<table  width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width= '30%' align='left'><font class="FormHeader">Search Flights</font>
						</td>
						<td width= '30%' align='center'></td>
						<td width= '40%' align='right'><font class="FormHeader"> All times in Local</font>
						</td>
					</tr>
				</table>				
				<%@ include file="../../common/IncludeFormHD.jsp"%>
			</td>
			<td> 			  
				<table width="100%" border="0" cellpadding="1" cellspacing="0">	
					<tr>
						<td colspan='5' style='height:5px;'></td>
					</tr>
					<tr>       
						<td><font>From Location :</font></td>
						<td>
							<select id='selFrom' name='selFrom' size='1'></select>
						</td>
						<td><font>To Location :</font></td>
						<td>
							<select id='selTo' name='selTo' size='1'></select>
						</td>
						<td><font>From Date :</font></td>
						<td>								
							<table border='0' cellpadding='0' cellspacing='1'>
								<tr>
									<td><input type='text' id='txtFDate' name='txtFDate' style='width:70px;' onblur='dateChange(0)'/></td>
									<td><a href="javascript:void(0)" onclick="loadCalendar(0,event); return false;" title="View Calendar" tabIndex="2"><img src='../../images/calendar_no_cache.gif' alt='Calendar' border='0'/></a></td>
									<td><%@ include file="inc_Mandatory.jsp" %></td>
								</tr>
							</table>
		                </td> 
		                <td><font>To Date : </font></td>
						<td>								
							<table border='0' cellpadding='0' cellspacing='1'>
								<tr>
									<td><input type='text' id='txtTDate' name='txtTDate' style='width:70px;' onblur='dateChange(1)'/></td>
									<td><a href="javascript:void(0)" onclick="loadCalendar(1,event); return false;" title="View Calendar" tabIndex="2"><img src='../../images/calendar_no_cache.gif' alt='Calendar' border='0'/></a></td>
									<td><%@ include file="inc_Mandatory.jsp" %></td>
								</tr>
							</table>
		                </td>
						<td align="right">
						<input name="btnCancel" type="button" class="button" id="btnCancel" onClick="pgButtonClick(1)" value="Close">
						<input name="btnReset" type="button" class="button" id="btnReset" onClick="pgButtonClick(2)" value="Reset">
						<input name="btnSearch" type="button" class="button" id="btnSearch" onClick="UI_FLA.flightSearch()" value="Search"></td> 
					</tr> 
			  	</table> 
		  		<%@ include file="../../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
		<tr>
			<td style='height:5px;'></td>
		</tr>
		<tr>
			<td><span id='spnSrchResuls' style='display:none'>
				<%@ include file="../../common/IncludeFormTop.jsp"%>Search Results<%@ include file="../../common/IncludeFormHD.jsp"%>	 			  
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>       
						<td id='tdSrchResults' style='height:465px;' valign='top'>
							<table width='100%' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td>
										<span id='spnHD'></span>
									</td>
								</tr>
								<tr>
									<td id='tdDetails' style='height:400px;' valign='top'>
										<span id='spnDT' style='position:absolute;overflow-X:hidden; overflow-Y:scroll;width:920px;height:400px;'></span>
									</td>
								</tr>
								<tr>
									<td style='height:10px;'>
									</td>
								</tr>
								<tr>
									<td>
										<table width='100%' border='0' cellpadding='0' cellspacing='0'>
											<tr>
												<td width='30%'>
													<span id='spnRec'></span>
												</td>
												<td width='70%'>
													<span id='spnLegend'></span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td> 
					</tr> 
			  	</table> 
		  		<%@ include file="../../common/IncludeFormBottom.jsp"%>
		  		</span>
			</td>
		</tr>
  	</table></form>
	<span id='spnDetails' style='position:absolute;top:580px;width:932px;display:none'>
		<table width='100%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td style='background-image:url(../../images/M004_no_cache.jpg);background-repeat:repeat-x;background-position:bottom;'>
					<table border='0' cellpadding='0' cellspacing='0' width='100%'>
						<tr>
							<td width='240px;'>
								<span id='spnDetailInfoTab'>
									<table border='0' cellpadding='0' cellspacing='0'>
										<tr>
											<td id='tdTab1' class='cursorPointer' style='width:66px;height:22px;background-image:url(../../images/M007_no_cache.jpg);background-repeat:no-repeat' align='center' onclick='tblDetailClick(0)'><font class="FormHeader">Analyze</font></td>
											<td style='width:2px;'></td>							
											<td id='tdTab2' class='cursorPointer' style='width:66px;height:22px;background-image:url(../../images/M008_no_cache.jpg);background-repeat:no-repeat' align='center' onclick='tblDetailClick(1)'><font class="FormHeader">Optimize</font></td>
											<td style='width:2px;'></td>
											<td id='tdTab4' class='cursorPointer' style='width:66px;height:22px;background-image:url(../../images/M008_no_cache.jpg);background-repeat:no-repeat' align='center' onclick='tblDetailClick(3)'><font class="FormHeader">Alerts</font></td>
											<td style='width:2px;'></td> 
											<!--
											<td id='tdTab3' style='width:66px;height:22px;background-image:url(../../images/M008_no_cache.jpg);background-repeat:no-repeat' align='center' onclick='tblDetailClick(2)'><font class="FormHeader">Fare Req.</font></td> -->
										</tr>
									</table>
								</span>
							</td>
							<td>
								<span id='spnSltdInfo'></span>
							</td>
							<td align='right'>
								<table border='0' cellpadding='0' cellspacing='0'>
									<tr>
										<td><img src='../../images/form_top_left_corner_no_cache.gif'></td>
										<td class="FormHeadBackGround cursorPointer" align='center' onclick='showDetailsClick()'>
											<font class="FormHeader"><span id='spnDetailInfoDesc'>Show Details</span></font>
										</td>
										<td><img src='../../images/form_top_right_corner_no_cache.gif'></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
				<span id='spnAnalyzeInfo'></span>
				<span id='spnOptimizeInfo' style='display:none;'>
					<%@ include file="inc_FLA_Optimize.jsp" %>
				</span>
				</td>
			</tr>
			<tr>
				<td><span id='spnAlertInfo'></span></td>
			</tr>
			<tr>
				<td><span id='spnFareReqInfo'></span></td>
			</tr>
			<div id='graphPopup'></div>
			<div id='emailPopup'></div>
		</table>
	</span>
	<!-- <span id='spnGridOtimiL' style='position:absolute;top:335px;width:400px;visibility:hidden;'></span>
	<span id='spnGridOtimiR' style='position:absolute;top:335px;width:500px;left:410px;visibility:hidden;'></span> -->
	<span id='spnGridFareReq' style='position:absolute;top:335px;width:920px;left:10px;visibility:hidden;'></span>
	<span id='spnGridAlert' style='position:absolute;top:335px;width:920px;left:10px;visibility:hidden;'></span>
	<script type='text/javascript' src='../../js/v2/common/commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />'></script>  	
	<script type='text/javascript'>
	<!--
	var arrAirports = new Array();
	var jsFlghtPerformance = [{}];
	
	// Airports
	<c:out value='${requestScope.req_Airports}' escapeXml='false' />
	<c:out value='${requestScope.req_fla_performance}' escapeXml='false' />
	<c:out value='${requestScope.req_fla_startDay}' escapeXml='false' />	
	
	//-->
	</script>
	<script type='text/javascript' src='../../js/v2/common/simpleEmailSender.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />'></script>
	<script type='text/javascript' src='../../js/v2/fla/flightLoad.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />'></script>
	<script type='text/javascript' src='../../js/v2/fla/flaOptimize.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />'></script>
	
  </body>
</html>
