
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<html>
	<head>
	    <title>FLA Email Alert</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="-1">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
		<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">	
		<LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css">
	
		
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>	
	
  </head>
  <body class="tabBGColor" scroll="no" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" ondrag='return false' 
  	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">

  		<form method="post" action="showFlaAlertEmail.action" id="frmFlaEmail" name="frmFlaEmail">  		
  			<script type="text/javascript">
	  			<c:out value="${requestScope.reqCountryCode}" escapeXml="false" />
	  			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />			
			</script>

			<table width="100%" cellpadding="0" cellspacing="0" border="0"
	class="PageBorder" ID="Table7" style="height: 350px;">
	<tr>
		<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%"
			height="1"></td>
		<td valign="top" align="left" class="PageBackGround"><!-- Your Form start here -->
		<font class="Header">FLA - Email Alert</font> <font
			class="fntSmall">&nbsp;</font> <br><%@ include
			file="../../common/IncludeMandatoryText.jsp"%> <br>
		<table width="98%" cellpadding="0" cellspacing="0" border="0"
			ID="Table7">
			<tr>
				<td><%@ include file="../../common/IncludeFormTop.jsp"%>FLA - Email Alert<%@ include
					file="../../common/IncludeFormHD.jsp"%> <br>
				<table width="100%" border="0" cellpadding="0" cellspacing="2">	
				  			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>			  			
				  			<tr>
								<td><font>To :</font></td>
				  				<td>
				  					<input type="text" id="txtToAddress" name="txtToAddress" size="40"><font class="mandatory">&nbsp;*</font>				  					
								</td>
							</tr>	
							
							<tr>	
								<td><font>Subject :</font></td>
								<td>
									<input type="text" id="txtSubject" name="txtSubject" size="45"><font class="mandatory">&nbsp;*</font>
								</td>
							</tr>
							
							<tr>	
								<td><font>&nbsp;</font></td>
								<td>
					  				<textarea name="txtMsgBody" id="txtMsgBody" cols="25" rows="4" style="height:72px;width:360px;" onkeyUp="" onkeyPress="" title="Can enter only up to 255 charactors"></textarea>
				  				</td>
				  			</tr>
				  			<tr>	
								<td><font>&nbsp;</font></td>
				  			</tr>
				  			<tr>	
								<td><font>&nbsp;</font></td>
				  			</tr>
				  			<tr>	
								<td><font>&nbsp;</font></td>
				  			</tr>
				  			<tr>
				  				<td colspan="5">
									<input type="button" id="btnClose" class="Button" value="Close"  onclick="javascript:window.close();">
				                    
				                </td>
				                <td align="right" colspan="2">
									<input name="btnSend" type="button" class="Button" id="btnSend" onClick="sendClicked()" value="Send">
								</td>
				  			</tr>
						</table>
				<%@ include file="../../common/IncludeFormBottom.jsp"%>
				<!-- Stable -->
			</tr>
		</table>
		<br>
		</td>
		<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%"
			height="1"></td>
	</tr>

	<%@ include file="../../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
</table>	
		<input type="hidden" name="hdnMode" id="hdnMode">
		

   </form>	
  </body>
  <script type="text/javascript">
    	 var languageHTML = "<c:out value="${requestScope.reqLanguageList}" escapeXml="false" />";
  		
  </script>
<script src="../../js/v2/fla/FlaAlertEmail.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>	
<script type="text/javascript">
  	<!--
  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
  	function ClearProgressbar(){  		
  		if (typeof(objDG) == "object"){
	  		if (objDG.loaded){
	  			clearTimeout(objProgressCheck);
	  			top[2].HideProgress();
	  		}
	  	}	  	
  	}
  	//-->
  </script>

</html>

