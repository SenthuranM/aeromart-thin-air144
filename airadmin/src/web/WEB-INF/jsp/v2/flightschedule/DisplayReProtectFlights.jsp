 <%-- 
	 @Author 	: 	Srikanth
  --%>
  
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Search Flight to Re-Protect</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
	<style type="text/css" media="screen">
	    th.ui-th-column div{
	        white-space:normal !important;
	        height:auto !important;
	        font-size:0.575em !important;
	        padding:3px;
	    }
	</style>

	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

	<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
    <script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

 	<script src="../../js/v2/flightschedule/flightToReprotect.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  

  </head>
  <body oncontextmenu="return false" scroll="yes" onkeypress='return Body_onKeyPress(event)' onkeydown='return Body_onKeyDown(event)' ondrag='return false' 
	onLoad="UI_flightToReprotect.winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" >
  	
   <form method="post"  id="frmFlightReprotectSearch" name="frmFlightReprotectSearch" action="showFlightsToReprotect!reprotect.action">
			  <script type="text/javascript">
					<!-- c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" /-->		
					<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />	
					<c:out value="${requestScope.PostBack}" escapeXml="false" />		
					<c:out value="${requestScope.reqReprotectFlightDataByCCCode}" escapeXml="false" />		
					<c:out value="${requestScope.reqCabinClassData}" escapeXml="false" />
					var popupmessage = "<c:out value="${requestScope.popupmessage}" escapeXml="false" />";	
					var agentReprotectEmailEnabled = "<c:out value="${requestScope.agentEmailEnabled}" escapeXml="false" />"
					var isSelfReprotectionAllowed = "<c:out value="${requestScope.isSelfReprotectionAllowed}" escapeXml="false" />"
					var externalIntlFlightDetailCaptureEnabled = "<c:out value="${requestScope.externalIntlFlightDetailCaptureEnabled}" escapeXml="false" />"
					var autoDateFillEnabled = "<c:out value="${requestScope.dateAutoFillEnabled}" escapeXml="false" />"
					<c:out value="${requestScope.reqSystemDate}" escapeXml="false" />
					var selfReprotectionEnable = "<c:out value="${requestScope.selfReprotectionEnable}" escapeXml="false" />";	
			  </script>

   <%@ include file="../../common/IncludeWindowTop.jsp"%><!-- Page Background Top page -->
   <table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
	   <tr>
			<td colspan="3"><font class="Header">Search Flights to Re-protect</font></td>
		</tr>
		<tr><td colspan="3"><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td colspan="3">
				<%@ include file="../../common/IncludeMandatoryText.jsp"%>
			</td>
		</tr>				
		<tr>
			<td colspan="3">
				<%@ include file="../../common/IncludeFormTop.jsp"%>Flight Details<%@ include file="../../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="2" class="FormBackGround">
							<tr valign="middle">
								<td width="10%" >
									<font>Departure Date - From</font>
								</td>
								<td width="13%">
<!-- 									<input type="text" name="txtDepartureDateFrom" id="txtDepartureDateFrom" maxlength="10" size="8" onBlur="dateChk('txtDepartureDateFrom')" invalidText="true"> -->
<!-- 									<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font> -->
									<input type="text" name="txtDepartureDateFrom" id="txtDepartureDateFrom" style="width: 72px;" maxlength="10"><font class="mandatory">&nbsp;*</font>
								</td>
								<td width="10%">
									<font>Departure Date - To</font>
								</td>
								<td width="13%">
<!-- 									<input type="text" name="txtDepartureDateTo" id="txtDepartureDateTo" maxlength="10" size="8" onBlur="dateChk('txtDepartureDateTo')" invalidText="true"> -->
<!-- 									<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font> -->
									<input type="text" name="txtDepartureDateTo" id="txtDepartureDateTo" style="width: 72px;" maxlength="10"><font class="mandatory">&nbsp;*</font>
								</td>
								<td width="10%">
									<font>Flight Number</font>
								</td>
								<td>
									<input type="text" id="txtFlightNumber" name="txtFlightNumber" maxlength="7" size="7">
								</td>
								<td>
									<font>Departure</font>
								</td>
								<td>
									<select name="selDeparture" size="1" id="selDeparture"  tabindex="5" title="Original departure location" style="width:80px;">
										<option value="-1"></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
						   			</select>
								</td>
								<td>
									<font>Arrival</font>
								</td>
								<td>
									<select name="selArrival" size="1" id="selArrival"  tabindex="6" title=" Final Destination" style="width:80px;">
										<option value="-1"></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
							 		</select>
								</td>
							</tr>
							<tr>								
								<td>
									<font>Via1</font>
								</td>
								<td>
									<select name="selVia1" size="1" id="selVia1"  tabindex="8" style="width:80px;">
										<option value="-1"></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
						   			</select>
								</td>
								<td>
									<font>Via2</font>
								</td>
								<td>
									<select name="selVia2" size="1" id="selVia2"  tabindex="9" style="width:80px;">
										<option value="-1"></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
							 		</select>
								</td>
								<td>
									<font>Via3</font>
								</td>
								<td>
									<select name="selVia3" size="1" id="selVia3"  tabindex="10" style="width:80px;">
										<option value="-1"></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
							 		</select>
								</td>
								<td>
									<font>Via4</font>
								</td>
								<td>
									<select name="selVia4" size="1" id="selVia4"  tabindex="11" style="width:80px;">
										<option value="-1"></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
							 		</select>
								</td>
								<td align="right" colspan="2">
									<input type="Button" class="button" name="btnSearch" id="btnSearch" value="Search">
								</td>
							</tr>
					</table>
			<%@ include file="../../common/IncludeFormBottom.jsp"%>
			</td>
		</tr>
		<tr><td colspan="3"><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td colspan="3">
			<%@ include file="../../common/IncludeFormTop.jsp"%>Flight Details Search Results<%@ include file="../../common/IncludeFormHD.jsp"%>
				<span id="spnSearch" style="height:160px"></span>
				<div id="divResultsPanel" style="height:250px;text-align:left;background-color:#ECECEC;">
				<table width="98%" id="listFlightRP" class="scroll" cellpadding="0" cellspacing="0"></table>
				<div id="FlightRPpager" class="scroll" style="text-align:center;"></div>		
				</div>
				<%@ include file="../../common/IncludeFormBottom.jsp"%>
			</td>
		<tr>
		<tr><td colspan="3"><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td width="47%" valign="top">
			<%@ include file="../../common/IncludeFormTop.jsp"%>Re-protect From<%@ include file="../../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="2" cellspacing="2">
					<tr>
						<td><font class="fntBold">Flight No</font>&nbsp;<font><span id="spnFlightNo"></span></font></td>
						<td><font class="fntBold">Departure</font>&nbsp;<font><span id="spnDeparture"></span></font></td>
						<td><font class="fntBold">Arrival</font>&nbsp;<font><span id="spnArrival"></span></font></td>
					</tr>
					<tr>
						<td>
							<font class="fntBold">COS</font>&nbsp;<span id="spnCOSFrom"></span>
						</td>
						<td colspan="2">
							<font class="fntBold">Dep. Date</font>&nbsp;<font><span id="spnDepDate"></span></font>
						</td>
					</tr>
					<tr>
						<td colspan="3" valign="top">
							<span id="spnReprotectFrom" style="height:150px"></span>
							<table width="98%" id="listFlightRPFROM" class="scroll" cellpadding="0" cellspacing="0"></table>
							<div id="FlightRPpagerFROM" class="scroll" style="text-align:center;"></div>	
						</td>
					</tr>
				</table>
			<%@ include file="../../common/IncludeFormBottom.jsp"%>
			</td>
			<td width="4%" align="center">
				<input type="button" class="Button" name="btnMove" id="btnMove" value=">>" style="width:25px" onClick="UI_flightToReprotect.reprotectPassenger()">
			</td>
		   <td width="49%">
			<span id="spnReprotectTo" style="visibility:hidden;" style="width:430px">
			<%@ include file="../../common/IncludeFormTop.jsp"%>Re-protect To<%@ include file="../../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="2" cellspacing="2">
					<tr>
						<td><font class="fntBold">Flight No</font>&nbsp;<font><span id="spnFlightNoOther"></span></font></td>
						<td><font class="fntBold">Departure</font>&nbsp;<font><span id="spnDepartureOther"></span></font></td>
						<td><font class="fntBold">Arrival</font>&nbsp;<font><span id="spnArrivalOther"></span></font></td>
					</tr>
					<tr>
						<td><font class="fntBold">COS</font>&nbsp;<span id="spnCOSOther"></span></td>
						<td>
							<font class="fntBold">Dep. Date</font>&nbsp;<font><span id="spnDepDateOther"></span></font>
						</td>
						<td>
							<font class="fntBold">Model</font>&nbsp;<font><span id="spnModelCap"></span></font>
						</td>
					</tr>
					<tr>
						<td colspan="3" valign="top">
							<span id="spnOther" style="height:80px"></span>
							<table width="98%" id="listFlightRPTO" class="scroll" cellpadding="0" cellspacing="0"></table>
							<div id="FlightRPpagerTO" class="scroll" style="text-align:center;"></div>	
						</td>
						
					</tr>
					<tr>						
						<td colspan="2">
							<table border="0" width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td width="5%" >
									<input type="checkbox" id="chkGA" name="chkGA" class="NoBorder" onClick="setPageEdited(true)" checked>
									</td>
									<td width="20%" align="left">
									<font class="font">Generate Alerts</font>
									</td>
									<td width="5%" >
									<input type="checkbox" id="chkSendSMS" name="chkSendSMS" class="NoBorder" onClick="enableSMSChk()">
									</td>
									<td width="20%" align="left">
									<font class="font">SMS Pax</font>
									</td>
									<td width="5%">
									<input type="checkbox" id="chkSendEmail" name="chkSendEmail" class="NoBorder" onClick="setPageEdited(true)">
									</td>
									<td width="20%" align="left">
									<font class="font">Email Pax</font>
									</td>
								</tr>
								<tr>
									<td width="5%">
										<input type="checkbox" id="chkSmsCnf" name="chkSmsCnf" class="NoBorder" onClick="setPageEdited(true)">
									</td>
									<td width="20%" align="left">
										<font class="font">SMS CNF Pax</font>
									</td>
									<td width="5%" >
										<input type="checkbox" id="chkSmsOnH" name="chkSmsOnH" class="NoBorder" onClick="setPageEdited(true)">
									</td>
									<td width="20%" align="left">
										<font class="font">SMS ON HOLD Pax</font>
									</td>
								</tr>
								<tr id="agentEmailRow">
									<td width="5%">
										<input type="checkbox" id="chkSendEmailOriginAgent" name="chkSendEmailOriginAgent" class="NoBorder" onClick="setPageEdited(true)">
									</td>
									<td width="20%" align="left">
										<font class="font">Origin Agent Email</font>
									</td>
									<td width="5%">
										<input type="checkbox" id="chkSendEmailOwnerAgent" name="chkSendEmailOwnerAgent" class="NoBorder" onClick="setPageEdited(true)">
									</td>
									<td width="20%" align="left">
										<font class="font">Owner Agent Email</font>
									</td>
								</tr>
								<tr id="selfReprotectRow">
									<td width="5%">
										<input type="checkbox" id="chkIbeTrasferFlights" name="chkIbeTrasferFlights" class="NoBorder" onClick="setPageEdited(true)">
									</td>
									<td width="25%" align="left">
										<font class="font">IBE Transfer Flight Email</font>
									</td>
								</tr>
							</table>
						</td>
						<td align="right">
							<input type="Button" class="Button" name="btnReset" id="btnReset" value="Reset" onClick="UI_flightToReprotect.resetReprotect()">
						</td>
					</tr>
					
					
				</table>
			<%@ include file="../../common/IncludeFormBottom.jsp"%>
			</span>
			</td>
		</tr>
		<tr><td colspan="4"><font class="fntSmall">&nbsp;</font></td></tr>
		<tr>
			<td>
				<input type="Button" class="Button" name="btnClose" id="btnClose" value="Close" onCLick="closeClick()" style="width:100px">
			</td>
			<td colspan="2" align="right">
				<span id="spnViewRollForwardBtn" style="visibility:show;">
					<input type="Button" class="Button" name="btnCRF" id="btnCRF" value="Roll Forward" style="width:120px" onClick="UI_flightToReprotect.confirmReprotectRollForward()">
				</span>
				<input type="Button" class="button" name="btnPaxDetals" id="btnPaxDetals" value="Show Pax Details" onCLick="UI_flightToReprotect.showPaxDetails()" style="width:auto">
				<input type="Button" class="button" name="btnConfirm" id="btnConfirm" value="Reprotect" style="width:auto" onClick="UI_flightToReprotect.confirmReprotectPAX()">				
			</td>
		</tr>
		<tr>
			<td>
				<div id='divLoadMsg' style='visibility: hidden; z-index: 1000; background: transparent;'>
										<iframe src="showFlightsToReprotect!loadMsg.action" id='frmLoadMsg'
											name='reprotect.frmLoadMsg' frameborder='0' scrolling='no'
											style='position: absolute; top: 50%; left: 40%; width: 260; height: 40;'></iframe>
				</div>
			</td>
		</tr>
		<tr><td colspan="3"><font class="fntSmall">&nbsp;</font></td></tr>
		</table>
	<%@ include file="../../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
	<!-- Used in FlightsToReprotectRequestHandler.java file -->
	<input type="hidden" name="hdnMode" id="hdnMode">
	<input type="hidden" name="hdnLogicalCabinClassCode" id="hdnLogicalCabinClassCode">
	<input type="hidden" name="hdnTargetCabinClassCode" id="hdnTargetCabinClassCode">
	<input type="hidden" name="hdnTargetCabinClassCode" id="hdnTargetCabinClassCode">
	<input type="hidden" name="hdnTargetFlightId" id="hdnTargetFlightId">
	<input type="hidden" name="hdnTargetSegmentCode" id="hdnTargetSegmentCode">
	<input type="hidden" name="hdnTargetFlightSegmentId" id="hdnTargetFlightSegmentId">
	<input type="hidden" name="hdnSourceFlightDistributions" id="hdnSourceFlightDistributions">
	<input type="hidden" name="hdnFlightId" id="hdnFlightId">
	<input type="hidden" name="hdnAlert" id="hdnAlert" value="false"/>	
	
	<input type="hidden" name="hdnSMSAlert" id="hdnSMSAlert" value=""/>	
	<input type="hidden" name="hdnEmailAlert" id="hdnEmailAlert" value=""/>	
	<input type="hidden" name="hdnSmsCnf" id="hdnSmsCnf" value=""/>	
	<input type="hidden" name="hdnSmsOnH" id="hdnSmsOnH" value=""/>
	<input type="hidden" name="hdnEmailOriginAgent" id="hdnEmailOriginAgent" value=""/>	
	<input type="hidden" name="hdnEmailOwnerAgent" id="hdnEmailOwnerAgent" value=""/>
	<input type="hidden" name="hdnSelectedPNR" id="hdnSelectedPNR" value=""/>	
	<input type="hidden" name="hdnSelectedSoldOhdCount" id="hdnSelectedSoldOhdCount" value=""/>				
	<input type="hidden" name="hdnSelectedSoldOhdInfantCount" id="hdnSelectedSoldOhdInfantCount" value=""/>
	<input type="hidden" name="hdnIBETransferFlights" id="hdnIBETransferFlights" value="false"/>	
	<input type="hidden" name="hdnReprotectSegmentIDs" id="hdnReprotectSegmentIDs" value=""/>
       <u:hasPrivilege privilegeId="plan.flight.flt.transfer.pnr.list">
           <input type="hidden" id="hiddenPrivilege" name="hiddenPrivilege" value="1" style="visibility: hidden"/>
       </u:hasPrivilege>
        <u:hasPrivilege privilegeId="plan.flight.flt.cancel">
           <input type="hidden" id="flightCanclePrivilege" name="flightCanclePrivilege" value="1" style="visibility: hidden"/>
       </u:hasPrivilege>      

  </form>
  	
  <script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  <script src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
</body>
</html>