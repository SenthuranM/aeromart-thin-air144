<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	    <title>Template Page</title>
	    <meta http-equiv="pragma" content="no-cache"/>
	    <meta http-equiv="cache-control" content="no-cache"/>
	    <meta http-equiv="expires" content="-1"/>
		<link rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>		
		<link rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>
		
		
		<style>
			.tipBase{
				background: #e4e4e4;
			}
			.paxClass{
				background: #333;
				height:15px;
				width:15px;
			}
			.seatDrop{
				border:1px solid #fff;
			}
			.ui-state-active, .ui-state-hover{
				border:1px dashed #ff9988;
			}
			#dragHElper{
				position:absolute;
				width:100%;
				height:100%;
				z-index:-1;
			}
			.select-to-drag{
				background:#ff0000;
			}
		</style>
		<script type="text/javascript">
			var hidFlightID = '<c:out value="${requestScope.hidFlightId}" escapeXml="false" />';
			var reqReprotectFlightData = {
					"ccCode": "Y",
					"ccName": "Promo Fare",
					"cabinClasses": [{"Y": "Economy"}],
					"flNumber": "G90101",
					"fromName": "BAH",
					"toName": "SHJ",
					"segments": [{
							"segIndex": "0",
							"segCode": "SHJ/BAH",
							"soldADIF": "1/0",
							"ohdADIF": "0/0",
							"flightID": "491427",
							"flSegmentID": "518795",
						}],
					"flightID": "491427",
					"departureDate": "02/12/2012",
					"intNumber": "4374"
					};
		</script>
 </head>
 <body class="tabBGColor">
	<%--  --%>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" height="100%" >
	<tr>
	<td id="fromFlightProtect" class="" width="32%" valign="top" align="center" >
		<table width="97%" border="0" cellpadding="0" cellspacing="0" style="margin:5px" class="ui-widget  ui-corner-all FormBackGround">
			<tr><td  colspan="2"><div class="ui-widget-header ui-corner-all" style='height:23px;line-height: 27px;padding:0px 5px'>From Flight</div></td></tr>
			<tr>
				<td style="" width="55%" aligh="right">
					<table><tbody id="tbdyExtSeatMap_FROM"></tbody></table>
				</td>
				<td width="45%">
					<table width="100%" border="0" cellpadding="2" cellspacing="1" height="100%">
						<tr><td></td></tr>
						<tr><td height="30">
							<input type="checkbox" id="selctAll_From" />
							Select all Passanges
						</td></tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
	<td id="flightSearch" class="" width="35%" valign="top">
		<table width="97%" border="0" cellpadding="2" cellspacing="0" style="margin:5px" class="ui-widget  ui-corner-all FormBackGround">
			<tr><td  ><div class="ui-widget-header ui-corner-all" style='height:23px;line-height: 27px;padding:0px 5px'>Flight Search</div></td></tr>
			<tr><td>
				<table width="100%" border="0" cellpadding="2" cellspacing="1" >
					<tr>
						<td width="60%">
							Departure Date<br/>
							<input type="text" id="departureDate" name="departureDate"/>
						</td>
						<td width="40%">
							Departure<br/>
							<select id="departure" style="width:70px">
								<option value='-'></option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="100%" border="0" cellpadding="0" cellspacing="1" >
								<tr>
									<td width="25%">
										Via1<br/>
										<select id="via1" style="width:50px">
											<option value='-'></option>
										</select>
									</td>
									<td width="25%">
										Via1<br/>
										<select id="via2" style="width:50px">
											<option value='-'></option>
										</select>
									</td>
									<td width="25%">
										Via1<br/>
										<select id="via3" style="width:50px">
											<option value='-'></option>
										</select>
									</td>
									<td width="25%">
										Via1<br/>
										<select id="via4" style="width:50px">
											<option value='-'></option>
										</select>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							Arrival Date<br/>
							<input type="text" id="arrivalDate" name="arrivalDate" />
						</td>
						<td>
							Arrival<br/>
							<select id="arrival" style="width:70px">
								<option value='-'></option>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							Flight Number<br/>
							<input type="text" id="flNumber" name="flNumber"/>
						</td>
						<td>
							Number of Days (+/-)<br/>
							<input type="text" id="dayVarient" name="dayVarient" size="4" value="0"/>
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
						<td align="right">
							<input type="button" id="btnsubmit" name="btnsubmit" value="Search"/>
						</td>
					</tr>
				</table>
			</td></tr>
		</table>
		<table width="97%" border="0" cellpadding="0" cellspacing="0" style="margin:5px" class="ui-widget  ui-corner-all FormBackGround">
			<tr><td  ><div class="ui-widget-header ui-corner-all" style='height:23px;line-height: 27px;padding:0px 5px'>Search results - Flights</div></td></tr>
			<tr>
			<td>
				<table id="flResults"></table>
				<div id="flResultsPager"></div>
			</td>
			</tr>
		</table>
	</td>
	<td id="toFlightProtect" class="" width="32%" valign="top">
		<table width="97%" border="0" cellpadding="0" cellspacing="0" style="margin:5px" class="ui-widget ui-corner-all FormBackGround">
			<tr><td colspan="2"><div class="ui-widget-header ui-corner-all" style='height:23px;line-height: 27px;padding:0px 5px'>To Flight</div></td></tr>
			<tr>
				<td style=""  width="55%" aligh="right">
					<table><tbody id="tbdyExtSeatMap_TO"></tbody></table>
				</td>
				<td width="45%">
					<table width="100%" border="0" cellpadding="2" cellspacing="1" height="100%">
						<tr><td></td></tr>
						<tr><td height="30">
							<input type="checkbox" id="selctAll_From" />
							Select all Passanges
						</td></tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
	</tr>
	</table>
	<%@ include file="../../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
	<div style="clear:both"></div>
   	<div id="popUp" class="myPopup" style='display: none'></div>
   		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.airutil.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/flightschedule/isa.seatmap.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
   		<script src="../../js/v2/flightschedule/reProtec.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
 </body>
</html>