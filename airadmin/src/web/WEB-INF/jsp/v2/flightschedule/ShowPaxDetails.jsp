<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	    <title>Pax Details</title>
	    <meta http-equiv="pragma" content="no-cache"/>
	    <meta http-equiv="cache-control" content="no-cache"/>
	    <meta http-equiv="expires" content="-1"/>
		<link rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>		
		<link rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>
		<link rel="stylesheet" type="text/css" href="../../css/ui.jqgrid.css"/>

		
		<style>
			.tipBase{
				background: #e4e4e4;
			}
			.paxClass{
				background: #333;
				height:15px;
				width:15px;
			}
			.seatDrop{
				border:1px solid #fff;
			}
			.ui-state-active, .ui-state-hover{
				border:1px dashed #ff9988;
			}
			#dragHElper{
				position:absolute;
				width:100%;
				height:100%;
				z-index:-1;
			}
			.select-to-drag{
				background:#ff0000;
			}
 		body{ overflow-x:hidden;overflow-y:hidden; } 
		</style>
		<script type="text/javascript">
			var hidFlightID = '<c:out value="${requestScope.hdnFlightId}" escapeXml="false" />';
		</script>
		<style type="text/css" media="screen">
		    th.ui-th-column div{
		        white-space:normal !important;
		        height:auto !important;
		        font-size:0.9em !important;
		        padding:2px;
		    }
		</style>
		
 </head>
 
 <body class="tabBGColor">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.grouping.min.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.airutil.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
  	<script src="../../js/v2/flightschedule/paxDetails.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  	<%@ include file="../../common/IncludeFormTop.jsp"%>Pax Details<%@ include file="../../common/IncludeFormHD.jsp"%>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" height="100%" >
		<tr>
			<td id="fromFlightProtect&flightCancel" class="" width="25%" valign="top" align="center" >&nbsp;</td>
			<td id="fromFlightProtect&flightCancel" class="" width="25%" valign="top" align="center" >&nbsp;</td>
			<td id="fromFlightProtect&flightCancel" class="" width="25%" valign="top" align="center" >&nbsp;</td>
			<td id="fromFlightProtect&flightCancel" class="" width="25%" valign="top" align="center" >&nbsp;</td>					
		</tr>
		<tr>
			<td class="" width="25%" valign="top" align="left" colspan="3" height="530">
				<span id="spnPaxDetailsInfo"  style="position:absolute;z-index:5"> </span>
				<div id="divResultsPanel" style="height:250px;text-align:left;background-color:#ECECEC;">
				<table width="98%" id="listPaxDetails" class="scroll" cellpadding="0" cellspacing="0"></table>
				<div id="pagingPaxDetails" class="scroll" style="text-align:center;"></div>
				</div>
			</td>			
		</tr>
		<tr>
			<td id="fromFlightProtect&flightCancel" class="" width="25%" valign="top" align="center" >&nbsp;</td>
			<td id="fromFlightProtect&flightCancel" class="" width="25%" valign="top" align="center" >&nbsp;</td>
			<td id="fromFlightProtect&flightCancel" class="" width="25%" valign="top" align="right" >&nbsp;
				<input type="button" id="btnclose" name="btnclose" value="Close" onclick="UI_paxDetails.close()" align="right"/>
			</td>
			<td id="fromFlightProtect&flightCancel" class="" width="25%" valign="top" align="center" >&nbsp;</td>
			<td id="fromFlightProtect&flightCancel" class="" width="25%" valign="top" align="right" >
				&nbsp;
			</td>
		</tr>
	</table>
 </body>
</html>