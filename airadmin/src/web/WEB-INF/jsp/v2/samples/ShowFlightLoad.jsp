<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Flight Load</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="-1">
	<script type="text/javascript" src="../../js/v2/jquery/jquery.js "></script>
	<script src="../../js/v2/jquery/jquery-migrate.js" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js "></script>
	<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js"></script>	
    <script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js"></script>

	<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js "></script>
	<script type="text/javascript">
		$(function(){
			var flightRes = [];
			$.themes._setTheme('default','default','../../themes/default/css/ui.all_no_cache.css');
			
			$("#divSearchPanel").decoratePanel("Flight Search");
			$("#divResultsPanel").decoratePanel("Search Results");
			$("#divDispPanel").decoratePanel("Flight Detail");

			$("#fromdate").datepicker({ changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true' });
			$("#todate").datepicker({ changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true' });

			$('#btnSearch').decorateButton();
			$('#btnSearch').click(function() {
				mydata = [];
				$.getJSON("../samples/jsonFlightSearch.action?startDate="+$("#fromdate").val()+"&stoptDate="+$("#todate").val(),
					function(result){
						flightRes = result["flights"];					
						for(var i=0;i<flightRes.length;i++) $("#list4").addRowData(i+1,flightRes[i]["flight"]); 

					 });
			});

			$('#selThemeSelect').change(function() {
				$.themes._setTheme(this.value,this.value,'../../themes/'+ this.value +'/css/ui.all_no_cache.css');
			});

			$("#list4").jqGrid({ datatype: "local", height: 250, colNames:['Flt No','Date', 'mod no', 'origin','destination','Status','DayNo'], 
				colModel:[ {name:'flightNumber',index:'flightNumber', width:60}, 
						   {name:'departureDate',index:'departureDate', width:90, sorttype:"date"}, 
						   {name:'modelNumber',index:'modelNumber', width:100, align:"center"}, 
						   {name:'originAptCode',index:'originAptCode', width:80, align:"center"}, 
						   {name:'destinationAptCode',index:'destinationAptCode', width:80, align:"center"}, 
						   {name:'status',index:'status', width:80,align:"center"}, 
						   {name:'dayNumber',index:'dayNumber', width:150, sortable:false} 
						  ], imgpath: '../../themes/default/images/jqgrid', multiselect:false,
							onSelectRow: function(rowid){
											var flt = flightRes[rowid-1]["flight"]
											$("#fltNo").val(flt.flightNumber);
											$("#fltDate").val(flt.departureDate);
											$("#modelNo").html(flt.modelNumber);
											$("#fltOrg").val(flt.originAptCode);
											$("#fltDest").val(flt.destinationAptCode);
											
										}});			
		});
	</script>
</head>
<body style="overflow:hidden;" class="softBody">
<div id="divRoorWrapper">

			<div id="divSearchPanel">
				<table width="922px" cellpadding="5" cellspacing="0" border="0">
					<tr>
						<td>
							From Date: <input id="fromdate" type="text" class="aa-input">
						</td>
						<td>
							To Date: <input id="todate" type="text" class="aa-input">
						</td>						
						<td align="right">				
						<button id="btnSearch" type="button">Search</button>
						</td>
					</tr>
				</table>
			</div>

			<div id="divResultsPanel" style="height:375px;">
				<table id="list4" class="scroll" cellpadding="0" cellspacing="0"></table>
			</div>
			<div id="divDispPanel" style="height:100px;">
				<table width="922px" cellpadding="5" cellspacing="0" border="0">
					<tr>
						<td>
							Flight Date: <input id="fltDate" type="text" class="aa-input">
						</td>
						<td>
							Flight No: <input id="fltNo" type="text" class="aa-input">
						</td>						
					</tr>
					<tr>
						<td>
							Model NO <span id="modelNo"></span>
						</td>											
					</tr>
					<tr>
						<td>
							Origin <input id="fltOrg" type="text" class="aa-input">
						</td>
						<td>
							Destination <input id="fltDest" type="text" class="aa-input">
						</td>												
					</tr>
				</table>
			</div>
			
			<div id="themeRoll" style="float:right">Theme : 
				<select id="selThemeSelect">
					<option value="default">default</option>
					<option value="bluesky">bluesky</option>
					<option value="humanity">humanity</option>
				</select>
			</div>
</div>
<script>
	top[2].HideProgress();
</script>
</body>
</html>