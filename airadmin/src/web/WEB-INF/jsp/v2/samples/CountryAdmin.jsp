<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	    <title>Template Page</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="-1">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
		<LINK rel="stylesheet" type="text/css" href="../../themes/bluesky/css/ui.all_no_cache.css">
		
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js "></script>
		<script src="../../js/v2/jquery/jquery-migrate.js" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js"></script>	
	    <script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js"></script>
	    <script type="text/javascript" src="../../js/v2/jquery/jquery.form.js"></script>
	    
	    <script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js "></script>
	    <script type="text/javascript">

		jQuery(document).ready(function(){  //the page is ready	

			$.themes._setTheme('bluesky','bluesky','../../themes/bluesky/css/ui.all_no_cache.css');			
			
			$("#divResultsPanel").decoratePanel("Countries");
			$("#divDispCountry").decoratePanel("Add/Modify Country");			
			$('#btnAdd').decorateButton();
			$('#btnEdit').decorateButton();
			$('#btnDelete').decorateButton();
			$('#btnClose').decorateButton();
			$('#btnReset').decorateButton();
			$('#btnSave').decorateButton();
			
			jQuery("#listCountry").jqGrid({ 
				url:'../samples/showCountryAdmin!searchCountry.action',
				datatype: "json",
				jsonReader : {
					  root: "rows", 
					  page: "page",
					  total: "total",
					  records: "records",
					  repeatitems: false,					 
					  id: "0"				  
					},													
				ccolNames:['&nbsp;','Country ID','Country Description', 'Currency','Remarks', 'Status','Version','createdBy','createdDate','modifiedBy','modifiedDate'], 
				colModel:[ 	{name:'id', width:40, jsonmap:'id'},   
						 	{name:'countryCode',index:'countryCode', width:150, jsonmap:'country.countryCode'}, 
						   	{name:'countryName',index:'countryName', width:350,  jsonmap:'country.countryName'}, 
						   	{name:'currencyCode', width:120, align:"center",  jsonmap:'country.currencyCode'}, 
						   	{name:'remarks', width:120, align:"center",hidden:true,  jsonmap:'country.remarks'},
						   	{name:'status', width:225, align:"center",  jsonmap:'country.status'},
						   	{name:'version', width:225, align:"center",hidden:true,  jsonmap:'country.version'},
						   	{name:'createdBy', width:225, align:"center",hidden:true,  jsonmap:'country.createdBy'},
						   	{name:'createdDate', width:225, align:"center",hidden:true,  jsonmap:'country.createdDate'},
						   	{name:'modifiedBy', width:225, align:"center",hidden:true,  jsonmap:'country.modifiedBy'},
						   	{name:'modifiedDate', width:225, align:"center",hidden:true,  jsonmap:'country.modifiedDate'}
						   
						  ], imgpath: '../../themes/bluesky/images/jqgrid', multiselect:false,
				pager: jQuery('#pager'),
				rowNum:20,						
				viewrecords: true,
				height:210,
				loadui:'block',
				onSelectRow: function(rowid){												
					jQuery("#listCountry").GridToForm(rowid,frmCountry);					
					if(jQuery("#listCountry").getCell(rowid,'status') == 'ACT'){
						$("#status").attr('checked', true);
					}else {
						$("#status").attr('checked', false);
					}
					enableGridButtons();
			}}).navGrid("#pager",{refresh: true, edit: false, add: false, del: false, search: false});

			var options = { 
			//	target:        '#output2',   // target element(s) to be updated with server response 
				beforeSubmit:  showRequest,  // pre-submit callback - this can be used to do the validation part 
				success: showResponse,   // post-submit callback 
			 
				// other available options: 
				//url:       url         // override for form's 'action' attribute 
				//type:      type        // 'get' or 'post', override for form's 'method' attribute 
				dataType:  'json'        // 'xml', 'script', or 'json' (expected server response type) 
				//clearForm: true        // clear all form fields after successful submit 
				//resetForm: true        // reset the form after successful submit 
			 
				// $.ajax options can be used here too, for example: 
				//timeout:   3000 
			    }; 

			var delOptions = {				 
				beforeSubmit:  showDelete,  // pre-submit callback - this can be used to do the validation part 
				success: showResponse,   // post-submit callback 				 
				url: '../samples/showCountryAdmin!deleteCountry.action',         // override for form's 'action' attribute 
				dataType:  'json',        // 'xml', 'script', or 'json' (expected server response type) 
				resetForm: true        // reset the form after successful submit 
				 
			 }; 

		    
			// pre-submit callback 
			function showRequest(formData, jqForm, options) { 
			    // formData is an array; here we use $.param to convert it to a string to display it 
			    // but the form plugin does this for you automatically when it submits the data 
			   // var queryString = $.param(formData); 
			 
			    // jqForm is a jQuery object encapsulating the form element.  To access the 
			    // DOM element for the form do this: 
			    // var formElement = jqForm[0]; 
			 
			    //alert('About to submit: \n\n' + queryString); 
			 
			    // here we could return false to prevent the form from being submitted; 
			    // returning anything other than false will allow the form submit to continue 
			    return true; 
			} 
			
			function showResponse(responseText, statusText)  { 
			    // for normal html responses, the first argument to the success callback 
			    // is the XMLHttpRequest object's responseText property 
			 
			    // if the ajaxSubmit method was passed an Options Object with the dataType 
			    // property set to 'xml' then the first argument to the success callback 
			    // is the XMLHttpRequest object's responseXML property 
			 
			    // if the ajaxSubmit method was passed an Options Object with the dataType 
			    // property set to 'json' then the first argument to the success callback 
			    // is the json data object returned by the server 
			 
			    alert(responseText['succesMsg']); 
			} 

			function showDelete(formData, jqForm, options) {				
			    return confirm('Do you wish to Delete the record ?'); 
			} 
			$('#btnSave').click(function() {
				disableControls(false);
				if($("#status").attr('checked')){
					$("#status").val('ACT');
				}else {
					$("#status").val('INA');
				}							
				$('#frmCountry').ajaxSubmit(options);
				return false;	
			});
			 
			$('#btnAdd').click(function() {
				disableControls(false);
				$('#frmCountry').clearForm();				
			}); 	

			$('#btnEdit').click(function() {
				disableControls(false);				
			}); 
			$('#btnReset').click(function() {
				$('#frmCountry').resetForm();			
			});
			$('#btnDelete').click(function() {
				disableControls(false);				
				$('#frmCountry').ajaxSubmit(delOptions);
				return false;			
			});  				
			
			function  disableControls(cond){
				$("input").each(function(){ 
				      $(this).attr('disabled', cond); 
				});	
				$("textarea").each(function(){ 
				      $(this).attr('disabled', cond); 
				});	
				$("select").each(function(){ 
				      $(this).attr('disabled', cond); 
				});	
				$("#btnClose").attr('disabled', false);
			}

			function enableGridButtons(){
				$("#btnAdd").attr('disabled', false);
				$("#btnEdit").attr('disabled', false);
				$("#btnDelete").attr('disabled', false);
				$("#btnReset").attr('disabled', false);
			}
			disableControls(true);
			
		}); 

	
		</script>
 	
 	</head>
  	<body class="tabBGColor" scroll="no"  onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
		 		
  			<script type="text/javascript">			
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />										
			</script>
			
			<div id="divResultsPanel" style="height:300px;text-align:left;">
				<table id="listCountry" class="scroll" cellpadding="0" cellspacing="0"></table>
				<div id="pager" class="scroll" style="text-align:center;"></div>
					
					<u:hasPrivilege privilegeId="sys.mas.country.add">
						<input name="btnAdd" type="button" id="btnAdd" value="Add">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="sys.mas.country.edit">
						<input name="btnEdit" type="button" id="btnEdit" value="Edit">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="sys.mas.country.delete">	
						<input name="btnDelete" type="button" id="btnDelete" value="Delete">
					</u:hasPrivilege>				
				
			</div>			  		
			<div id="divDispCountry" style="height:220px;text-align:left;">
				<form method="post" action="../samples/showCountryAdmin.action" id="frmCountry"> 
				<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
					<tr>
						<td width="15%"><font>Country ID</font></td>
						<td><input name="countryCode" type="text" id="countryCode" style="width:75px;"  class="UCase" maxlength="2" onkeyUp="alphaValidate(this)" onkeyPress="alphaValidate(this)"><font class="mandatory">&nbsp;*</font></td>
					</tr>
					<tr>
						<td><font>Country Description</font></td>
						<td><input name="countryName" type="text" id="countryName" size="50" maxlength="30" onkeyUp="alphaWhiteSpaceValidate(this)" onkeyPress="alphaWhiteSpaceValidate(this)"><font class="mandatory">&nbsp;*</font></td>
					</tr>
					<tr>
						<td><font>Currency</font></td>
						<td>
							<select name="currencyCode" id="currencyCode" size="1" style="width:105;">
								<option value="-1"></option> 
								<c:out value="${requestScope.reqCurrencyList}" escapeXml="false" />
							</select><font class="mandatory">&nbsp;*</font>
						</td>
					</tr>
					<tr>
						<td valign="top"><font>Remarks</font></td>
						<td><textarea name="remarks" id="remarks" cols="47" rows="5" title="Can enter only up to 255 charactors"></textarea></td>
					</tr>
					<tr>
						<td><font>Active</font></td>
						<td><input type="checkbox" name="status" id="status" value="ACT"></td>
					</tr>
			 		<tr>
						<td colspan="2" style="height:42px;"  valign="bottom">
							<input name="btnClose" type="button"  id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)">
						    <input name="btnReset" type="button"  id="btnReset" value="Reset"  onClick="resetCountry()">
						</td>
						<td align="right" valign="bottom">
							<input name="btnSave" type="button"  id="btnSave" value="Save">
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="version"  id="version" style="display: none;" />
						</td>
				   </tr>
			  </table>					
			  </form>
			</div>	
	<script>
		var screenId = 'SC_FLGT_003';
		top[2].HideProgress();
	</script>
  </body>
</html>