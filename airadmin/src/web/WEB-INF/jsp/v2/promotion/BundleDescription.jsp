<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
    <LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
    <LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/v2/tiny_mce/tiny_mce.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>

    <script src="../../js/promotion/bundleDescription.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Edit Bundle Description Templates</title>
</head>
<body class='tabBGColor'>
<div id="tabTemplatesGrid" style="background-color: rgb(236, 236, 236);">
    <form>
        <div id="tabSearchTerms">
            <table id="termsSearch" style="height: 30px; padding: 10px; padding-bottom: 5px; padding-top: 5px;">
                <tr>
                    <td width="790px">
                        <label id="lblName">Template Name :</label>
                        <input id="templateNameSearchText" type="text" style="width: 355px;"/>
                    </td>
                    <td width="100px" align="right">
                        <input type="button" id="btnSearch" class="btnDefault Button ui-button ui-widget ui-corner-all" value="Search"/>
                    </td>
                </tr>
            </table>
        </div>
    </form>
    <div style="padding-left:8px;">   
        <table width="100%" border="0" cellpadding="0" cellspacing="0"  id="bundleTemplatesTable"></table>
        <div id="bundleTemplatesTablePager"></div>
    </div>
    <div style="padding-left:8px;">
        <input type="button" id="btnAdd" class="btnDefault Button ui-button ui-widget ui-corner-all" value="Add"/>
        <input type="button" id="btnEdit" class="btnDefault Button ui-button ui-widget ui-corner-all" value="Edit"/>
    </div>
</div>
<div style="min-height:7px;"></div>
<div id="addEditDisctiptionPanel" style="background-color: rgb(236, 236, 236);">
    <div style="padding-left:8px;padding-right: 8px;">
        <div id="descriptionTabs" class="tabTranslationDiv">
        <ul id="tabs_ul">
            <li id="tabs_settings"><a href='#templateManagement' id='tab_settings'>Settings</a></li>  
            <li id="tabs_li_en"><a href='#tabs_en' id='tab_en'>English</a></li>       
        </ul>
        <div>
            <table id="templateManagement" style="height: 30px; padding: 10px">
                <tbody>
                <tr>
                    <td width="500px" style="padding-top: 20px;">
                        <label id="lblTemplateName">Template Name: </label>
                        <input id="txtTemplateName" type="text" style="width: 355px;" disabled/>
                    <td>
                    <td width="100px">
                        
                    <td>
                    <td width="100px">
                    <td>
                    <td width="100px">
                        &nbsp;
                    </td>
                    </td>
                </tr>
                </tbody>
            </table>        
        </div>    
        <div id="tabs_en">
            <table class="traslationContext_table" >
                <tbody>
                <tr>
                    <td width="10px"></td>
                    <td width="430px">
                        <div style="width: 100%; padding: 5px">Free Items Text : (Only Unordered Lists)</div>
                        <textarea class="freeContext" id="freeContext_en" disabled></textarea>
                    </td>
                    <td width="10px"></td>
                    <td width="430px">
                        <div style="width:100%; padding: 5px">Paid Items Text : (Only Unordered Lists)</div>
                        <textarea class="paidContext" id="paidContext_en" disabled></textarea>
                    </td>
                    <td width="10px"></td>
                </tr>
                </tbody>
            </table>
            </div>
        </div>
    </div>
    <div style="padding:8px;">
        <input type="button" id="btnClose" onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault Button ui-button ui-widget ui-corner-all" value="Close"/>
        <input type="button" id="btnReset" class="btnDefault Button ui-button ui-widget ui-corner-all" value="Reset"/>
        <input type="button" id="btnSave" class="btnDefault Button ui-button ui-widget ui-corner-all" value="Save" style="float: right;"/>
    </div>
</div>    
	<script>
		var screenId = 'SC_PROM_10';

		top[2].HideProgress();
	</script>
</body>
</html>