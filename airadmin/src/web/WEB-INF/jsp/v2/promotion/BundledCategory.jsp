<%@ page language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
    <LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
    <LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
    <LINK rel="stylesheet" type="text/css" href="../../css/flexboxgrid.css">

    <script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>

    <script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/manageCharges/ManageChargesValidation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script type="text/javascript"
            src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script type="text/javascript"
            src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script type="text/javascript"
            src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script type="text/javascript"
            src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script type="text/javascript"
            src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script type="text/javascript"
            src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script type="text/javascript"
            src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script src="../../js/v2/isalibs/isa.jquery.templete.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <script type="text/javascript"
            src="../../js/promotion/bundledCategory.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
            type="text/javascript"></script>
    <style>
        .block {
            padding: 15px 0 0 0;
        }

        .fullWidth {
            width: 100%;
        }
        .border{
            border: dotted;
        }
        .parent{
            height: 100px;
        }
        .high{
            height: 100% !important;
        }

    </style>

</head>
<body class="tabBGColor" scroll="no" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false"
      onkeydown='return Body_onKeyDown(event)' ondrag='return false'>


<!-- --------------------SEARCH PANEL------------------------------------------------------------------------------>
<div id="divBundledCategorySearch" style="text-align: left; background-color: #ECECEC; padding-left:10px; padding-right:10px;">
    <table width="100%" class="scroll" cellpadding="5" cellspacing="5" style="padding-top:5px; padding-bottom:5px;">
        <tr>
            <td>
                <span style="padding-right:5px;">Category Name</span>
                <input id="txtBundleName" type="text" style="width:200px;"/>
            </td>
            <td>
                <span style="padding-right:5px;">Status</span>
                <select id="selStatus">
                    <option></option>
                    <option>ACT</option>
                    <option>INC</option>
                </select>
            </td>
            <td>
                <span style="padding-right:5px;">Priority</span>
                <select id="selPriority">
                    <option></option>
                </select>
            </td>
            <td align="right">
                <input name="btnSearch" type="button" id="btnSearch" value="Search" class="Button ui-button ui-widget ui-corner-all">
            </td>
        </tr>
    </table>
    <table id="bundledCategoryGrid" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="divPager" class="scroll" style="text-align:center;"></div>
    <div style="padding-top:5px; padding-bottom:5px;">
        <input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault Button ui-button ui-widget ui-corner-all">
        <input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault Button ui-button ui-widget ui-corner-all">
        <input name="btnDelete" type="button" id="btnDelete" value="Delete" class="btnDefault Button ui-button ui-widget ui-corner-all" />
    </div>
</div>
<!-- --------------------EDIT/VIEW DETAILS-------------------------------------------------------------------------->
<div id="divBundledCategoryEdit" style="height:260px;text-align:left; background-color: #ECECEC;">
    <form method="post" action="showBundledCategory!saveCategory.action" id="frmBundleCategory">
        <input type="hidden" id="hdnBundledCategoryId" name="bundledCategoryId"/>
        <input type="hidden" id="hdnVersion" name="version"/>
        <table border="0" cellpadding="0" cellspacing="0" ID="Table9">
            <tr>
                <td>
                    <div id="bundledCategoryTabs" class="admin-panel-smaller" style="height:210px;">
                        <ul class="ui-border-leftDis ui-border-rightDis">
                            <li id="tabBookingClasses"><a href="#modifyCategoryTabId">Category Detail</a></li>
                            <li id="tabSalesChannels"><a href="#modifyCategoryTranslations">Translations</a></li>
                        </ul>
                        <div id="modifyCategoryTabId">
                            <table width="100%">
                                <tr>
                                    <td valign="top">
                                        <table width="100%">
                                            <tr>
                                                <td colspan="2">
                                                    <div>Category Name</div>
                                                    <div><input id="txtName" name="categoryName" type="text" style="width:90%"></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="50%">
                                                    <div style="padding-top:10px;">Status</div>
                                                    <div>
                                                        <select id="selStatusInDetail" name="status" style="width:80%">
                                                            <option></option>
                                                            <option value="ACT">Active</option>
                                                            <option value="INC">Inactive</option>
                                                            <option value="DEL">Deleted</option>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div style="padding-top:10px;">Priority</div>
                                                    <div><input id="txtPriority" type="text" name="priority" style="width:80%"></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width:55%">
                                        <div>Description</div>
                                        <div><textarea id="txtDescription" name="description" style="width:100%" rows="8"></textarea></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="modifyCategoryTranslations">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <div style="overflow-y: auto; height:150px; ">
                                            <table width="100%">
                                                <tr>
                                                    <td height="30">Languages</td>
                                                    <td height="30">Category Name</td>
                                                    <td height="30" width="60%">Description</td>
                                                </tr>
                                                <tr id="translationsTemplate" >
                                                    <td><label id="languageName">English</span></td>
                                                    <td><input id="txtCatName" name="txtCatName" type="text" disabled></td>
                                                    <td width="60%">
                                                        <input id="txtCatDesc" name="description" type="text" disabled style="width:90%">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" style="padding-left:5px; padding-right:5px;">
                        <tr>
                            <td colspan="3" style="height: 25px;" valign="bottom"><input name="btnClose" type="button" id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault Button ui-button ui-widget ui-corner-all"> <input name="btnReset" type="button" id="btnReset"
                                                                                                                                                                                                                                                                     value="Reset" class="btnDefault Button ui-button ui-widget ui-corner-all"></td>
                            <td align="right" valign="bottom"><input name="btnSave" type="button" id="btnSave" value="Save" class="btnDefault Button ui-button ui-widget ui-corner-all"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</div>

<script>
    var screenId = 'SC_PROM_06';
    top[2].HideProgress();
</script>
</body>
</html>