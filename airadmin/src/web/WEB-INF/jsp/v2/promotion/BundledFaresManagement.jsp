<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Create Bundled Fare</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css" />
<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css" />
<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/jquery.ui.autocomplete.css" />


<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<!-- script type="text/javascript" src="../../js/v2/jquery/jquery-ui-1.8.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script-->
<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery.json.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/promotion/popupData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/MultiDropDown1.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery.formfileupload.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript">
	var languageHTML = "<c:out value="${requestScope.reqLanguageList}" escapeXml="false" />";
	<c:out value="${requestScope.reqImgPath}" escapeXml="false" />
	<c:out value="${requestScope.reqImagePrefix}" escapeXml="false" />
	<c:out value="${requestScope.reqImageStripePrefix}" escapeXml="false" />
</script>
<style>
.innerPanel {
	width: 98%;
	margin: 0 auto;
}

.portSel {
	width: 50px
}

select {
	margin: 1px 1px;
}

.iconBtn {
	width: 25px
}

#ui-datepicker-div {
	display: none;
}

.ui-autocomplete {
	height: 200px;
	overflow-y: scroll;
	overflow-x: hidden;
}

.multipleSelect {
	height: 100px;
	width: 150px
}
</style>
</head>
<body class="tabBGColor" scroll="no" oncontextmenu="return false" ondrag='return false'>
	<script type="text/javascript">
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	</script>

	<div id="divBundledFaresSearch" style="text-align: left; background-color: #ECECEC; display: none">
		<div class="innerPanel divsearch" style="border-bottom: 1px solid #bbb">
			<table border="0" cellpadding="0" cellspacing="2" align="left" style="width: 97%; margin: 0 auto">
				<tr>
					<td><font>Fare Class Name</font></td>
					<td><select id="selBundledFareNameSearch" name="selBundledFareNameSearch" style="width: 100px;">
							<option value="" selected="selected">All</option>
							<c:out value="${requestScope.reqBundledFareNameList}" escapeXml="false" />
					</select></td>
					<td align="left"><font>Category</font></td>
					<td><select id="selBundleCategory">
							<option value="" selected="selected">All</option>
							<c:out value="${requestScope.reqBundledCategoryList}" escapeXml="false" />
					</select></td>
					<td><font>OND</font></td>
					<td><select id="selBundleOND">
							<option value="" selected="selected">All</option>
							<c:out value="${requestScope.reqBundledONDList}" escapeXml="false" />
					</select></td>
					<td><font>Booking Class</font></td>
					<td><select id="selBundleBookingClasses">
							<option value="" selected="selected">All</option>
							<c:out value="${requestScope.reqBundledBookingClassesList}" escapeXml="false" />
					</select></td>
					<td><font>Status</font></td>
					<td><select id="selBundleStatus">
							<option value="" selected="selected">All</option>
							<c:out value="${requestScope.reqBundledStatusList}" escapeXml="false" />
					</select></td>

					<td align="right"><input name="btnSearch" type="button" id="btnSearch" value="Search" class="Button ui-button ui-widget ui-corner-all"></td>
				</tr>

			</table>
		</div>
		<div class="innerPanel divResults" style="border-top: 1px solid #fff">
			<div id="jqGridBundledFaresContainer" style="margin-top: 30px;">
				<table width="100%" border="0" cellpadding="0" cellspacing="0" id="jqGridBundledFaresData">
				</table>
				<div id="jqGridBundledFaresPages"></div>
			</div>
			<input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault Button ui-button ui-widget ui-corner-all">
			<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault Button ui-button ui-widget ui-corner-all"> 
		</div>
	</div>

	<div id="bundledFaresDetails" style="display: none; background-color: rgb(236, 236, 236);">
		<form method="post" action="manageBundledFaresConfiguration.action" id="frmBundledFares" enctype="multipart/form-data">

			<table width="100%">
				<tr>
					<td>
						<div id="bundleTemplateTabs" class="ui-border-topDis">
							<ul class="ui-border-leftDis ui-border-rightDis">
								<li id="tabBundleDetail"><a href="#tabPaneBundleDetail">Bundle Details</a></li>
								<li id="tabONDCodes"><a href="#tabPaneONDCodes">OND Codes</a></li>
								<li id="tabChannelAgents"><a href="#tabPaneChannelAgents">Channels & Agents</a></li>
								<li id="tabFlightPeriodNo"><a href="#tabPaneFlightPeriodNo">Booking Classes & Flight No.</a></li>
								<li id="tabServices" style="display: none;"><a href="#tabPaneServices">Services</a></li>
								<li id="tabSelectedServices"><a href="#tabPaneSelectedServices">Selected Services</a></li>
								<li id="tabLinkWEBPOS"><a href="#tabPaneLinkWEBPOS">Link Country/POS</a></li>
							</ul>

							<div id="tabPaneBundleDetail">
								<table width="97%" border="0" cellpadding="0" cellspacing="2">
									<tr>
										<td width="35%" valign="top">
											<table border="0" cellpadding="0" cellspacing="2">
												<tr>
													<td>Category<font class="mandatory">&nbsp;*&nbsp;</font></td>
													<td style="padding-left: 7px"><select id="selCategory">
															<option value=""></option>
															<c:out value="${requestScope.reqBundledCategoryList}" escapeXml="false" />
													</select></td>
												<tr>
												<tr>
													<td colspan="2" style="height: 5px;" valign="top"></td>
												</tr>
												<tr>
													<td>Fare Class Name <font class="mandatory">&nbsp;*&nbsp;</font></td>
													<td style="padding-left: 7px"><input type="text" id="bundledFareName" maxlength="60" /></td>
												</tr>
												<tr>
													<td colspan="2" style="height: 5px;" valign="top"></td>
												</tr>
												<tr>
													<td valign="top">Flight Period From</td>
													<td valign="top" style="padding-left: 7px">
														 <input type="text" id="flightPeriodFrom" style="width: 70%" maxlength="10" onblur="validateAndPopulate('flightPeriodFrom')" name="flightPeriodFrom" />
													</td>
												</tr>
												<tr>
													<td colspan="2" style="height: 5px;" valign="top"></td>
												</tr>
												<tr>
													<td valign="top">Flight Period To</td>
													<td valign="top" style="padding-left: 7px">															
														<input type="text" id="flightPeriodTo" style="width: 70%" maxlength="10" onblur="validateAndPopulate('flightPeriodTo')" name="flightPeriodTo" />
													</td>
												</tr>
												<tr>
													<td colspan="2" style="height: 5px;" valign="top"></td>
												</tr>
											    <tr>
													<td>Set Default</td>
													<td style="padding-left: 7px"><input type="checkbox" id=chkDefault /></td>
												</tr>
												<tr>
													<td colspan="2" style="height: 5px;" valign="top"></td>
												</tr>
											</table>
										</td>
										<td width="35%" valign="top">
											<table border="0" cellpadding="0" cellspacing="2">
												<tr>
													<td colspan="2" style="height: 5px;" valign="top"></td>
												</tr>
												<tr>
													<td>Status</td>
													<td style="padding-left: 7px">
													   <input type="radio" name="radStatus" value="ACT">Active
													   <input type="radio" name="radStatus" value="INA"> Inactive
													</td>
												</tr>
											
												<tr>
													<td colspan="2" style="height: 5px;" valign="top"></td>
												</tr>
												<tr>
													<td>Default Bundle Fee<font class="mandatory">&nbsp;*&nbsp;</font></td>
													<td style="padding-left: 7px"><input id="txtBundledFee" type="text" maxlength="10"></td>
												</tr>
												<tr>
													<td colspan="2" style="height: 5px;" valign="top"></td>
												</tr>
												<tr>
													<td>Default Charge Code<font class="mandatory">&nbsp;*</font></td>
													<td style="padding-left: 7px"><select id="selChargeCode" style="width: 112px;">
														<option value="" selected="selected"></option>
														<c:out value="${requestScope.reqChargeCodeList}" escapeXml="false" />
													</select></td>
												</tr>
												<tr>
													<td colspan="2" style="height: 5px;" valign="top"></td>
												</tr>
												<tr>
													<td>Cutoff Time (hours)</td>
													<td><input id="txtCutOffTime" type="text" maxlength="2" size="8" value="0"></td>
												</tr>
												<tr>
													<td colspan="2" style="height: 5px;" valign="top"></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</div>
							<div id="tabPaneONDCodes">
								<table width="100%">
									<tr>
										<td valign="top">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td valign="top" width="40%">Filter OND's<br /> <br />
														<div class="ui-widget">
															<input type="text" style="width: 80%" id="txtOndSearch" />
														</div>
													</td>
													<td width="20%">Results<br /> <br> <select size="10" id="sel_GeneratedRoutes" style="height: 150px; width: 70%" multiple="multiple">
													</select>
													</td>
													<td valign="middle" width="10%">
														<table width="100%">
														    <tr>
																<td width="10%" id="addAllOndCodes">
																	<div class="Button" style="width: 30px; height: 20px">&gt;&gt;</div>
																</td>
															</tr>
															<tr>
																<td width="10%" id="addOndCodes">
																	<div class="Button" style="width: 30px; height: 20px">&gt;</div>
																</td>
															</tr>
															<tr>
																<td></td>
															</tr>
															<tr>
																<td width="10%" id="removeOndCodes">
																	<div class="Button" style="width: 30px; height: 20px">&lt;</div>
																</td>
															</tr>
															<tr>
																<td width="10%" id="removeAllOndCodes">
																	<div class="Button" style="width: 30px; height: 20px">&lt;&lt;</div>
																</td>
															</tr>
														</table>
													<td width="20%">OND Codes List<br /> <br> <select size="10" id="sel_Routes" style="height: 150px; width: 70%" multiple="multiple">
													</select>
													</td>
													<td width="10%"></td>
												</tr>
												<tr>
													<td colspan="2" style="height: 5px;" valign="top"></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td align="left">
											<input id="btnSmartSearchHelp" type="button" style="width:60px" value="Help" class="ui-button ui-widget ui-corner-all" /> 
											<input id="btnSmartSearchClear" type="button" style="width:60px" value="Clear" class="ui-button ui-widget ui-corner-all" /> 
										</td>
									<tr>
								</table>
							</div>
							<div id="tabPaneChannelAgents">
								<table width="90%">
									<tr>
										<td valign="top">Channel <font class="mandatory">&nbsp;*</font></td>
										<td valign="top"><select size="10" id="selChanel" style="height: 75px; width: 90%" multiple="multiple" onchange="agentSelectionWidget()"></select></td>

										<td valign="top">Agents</td>
										<td valign="top"><input type="text" id="Agents" style="width: 99%" /> <br /> <select size="10" id="sel_Agents" style="height: 55px; width: 99%" multiple="multiple">
												<option value="">All</option>
										</select></td>
										<td width="25" valign="top"><input type="button" id="add_Agents" value=" + " class="iconBtn addItem" /><br /> <input type="button" id="del_Agents" value=" - " class="iconBtn removeItem" /></td>
									</tr>

								</table>
							</div>

							<div id="tabPaneFlightPeriodNo">
								<table width="100%">
									<tr>
										<td valign="top"><br />Booking Classes</td>
										<td style="padding-left: 7px">
											<table width="50%">
												<tr>
													<td width="20%"><select id="sel_BookingClass" name="sel_BookingClass" style="width: 80; height: 180" multiple>
													</select></td>
													<td width="10%">
														<table width="100%">
															<tr>
																<td width="10%" id="addBookingClasses">
																	<div class="Button" style="width: 23px; height: 17px">&gt;</div>
																</td>
															</tr>
															<tr>
																<td width="10%" id="removeBookingClasses">
																	<div class="Button" style="width: 23px; height: 17px">&lt;</div>
																</td>
															</tr>
														</table>
													</td>
													<td width="20%"><select id="sel_BookingClassAssigned" size="8" name="sel_BookingClassAssigned" style="width: 80; height: 180" multiple>
															<option value="">All</option>
													</select></td>
												</tr>
											</table>
										</td>
										<td width="10%" valign="top"></br> Flight No</td>
										<td style="padding-left: 7px">
											<table width="50%">
												<tr>
													<td width="20%"><select id="sel_FlightNo" name="sel_FlightNo" style="width: 80; height: 180" multiple>
													</select></td>
													<td width="10%">
														<table width="100%">
															<tr>
																<td width="10%" id="add_FlightNo">
																	<div class="Button" style="width: 23px; height: 17px">&gt;</div>
																</td>
															</tr>
															<tr>
																<td width="10%" id="del_FlightNo">
																	<div class="Button" style="width: 23px; height: 17px">&lt;</div>
																</td>
															</tr>
														</table>
													</td>
													<td width="20%"><select id="sel_FlightNoAssigned" size="8" name="sel_FlightNoAssigned" style="width: 80; height: 180" multiple>
															<option value="">All</option>
													</select></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</div>

							<div id="tabPaneServices" style="text-align: left; display: none;">
								<table>
									<tr>
										<td valign="top"><br /> <font>Free Services </font><font class="mandatory">&nbsp;*</font></td>
										<td valign="top" colspan="3">
											<table border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td><input id="chkSeat" name="freeAnci" type="checkbox" value="SEAT_MAP">&nbsp;Seat</td>
													<td>&nbsp;&nbsp; <select id="selSeatTemplate" style="width: 175px;" disabled="disabled">
															<option value="" selected="selected"></option>
															<c:out value="${requestScope.reqSeatTempl}" escapeXml="false" />
													</select>
													</td>
												</tr>
												<tr>
													<td><input id="chkMeal" name="freeAnci" type="checkbox" value="MEAL">&nbsp;Meal</td>
													<td>&nbsp;&nbsp; <select id="selMealTemplate" style="width: 175px;" disabled="disabled">
															<option value="" selected="selected"></option>
															<c:out value="${requestScope.reqMealTempl}" escapeXml="false" />
													</select>

														<div id="divMultiMealSelection" style="display: none;">
															&nbsp;&nbsp; <input id="chkMultiMeal" name="multiMealSelection" type="checkbox" title="Allow user to select multiple meals in create/modify reservation for this bundle" value="MULTI_MEAL"> &nbsp;Allow Multiple Meal Selection
														</div>
													</td>
												</tr>
												<tr>
													<td><input id="chkBaggage" name="freeAnci" type="checkbox" value="BAGGAGE">&nbsp;Baggage</td>
													<td>&nbsp;&nbsp; <select id="selBaggageTemplate" style="width: 175px;" disabled="disabled">
															<option value="" selected="selected"></option>
															<c:out value="${requestScope.reqBaggageTempl}" escapeXml="false" />
													</select>
													</td>
												</tr>
												<tr>
													<td><input id="chkFlexi" name="freeAnci" type="checkbox" value="FLEXI_CHARGES">&nbsp;Flexi</td>
													<td></td>
												</tr>
												<tr>
													<td valign="top"><input id="chkHala" name="freeAnci" type="checkbox" value="AIRPORT_SERVICE">&nbsp;Hala</td>
													<td valign="top">
														<table width="100%">
															<tr>
																<td width="20%"><select id="halaAvailable" name="halaAvailable" style="width: 150; height: 110" multiple disabled="disabled">
																		<c:out value="${requestScope.reqSSRList}" escapeXml="false" />
																</select></td>
																<td width="8%">
																	<table width="100%">
																		<tr>
																			<td width="10%" id="addHala">
																				<div class="Button" style="width: 23px; height: 17px">&gt;</div>
																			</td>
																		</tr>
																		<tr>
																			<td width="10%" id="removeHala">
																				<div class="Button" style="width: 23px; height: 17px">&lt;</div>
																			</td>
																		</tr>
																	</table>
																</td>
																<td width="20%"><select id="halaAssigned" size="8" name="halaAssigned" style="width: 150; height: 110" multiple disabled="disabled">
																</select></td>
																<td width="2%">&nbsp;</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>

								</table>
							</div>

							<div id="tabPaneSelectedServices" style="text-align: left">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td valign="top" style="min-width: 100px">
											<div style="border: 1px solid gray; border-bottom: none; padding-left: 18px; padding-top: 10px; min-height: 20px; ">
												<b>Service</b>
											</div>
											<div style="border: 1px solid gray; border-bottom: none; padding: 4px; min-height: 17px;">
												<input id="chkBgdService" name="chkBgdService" type="checkbox" value="BGD"/>
												<span>Baggage</span>
											</div>
											<div style="border: 1px solid gray; border-bottom: none; padding: 4px; min-height: 17px;">
												<input id="chkMleService" name="chkMleService" type="checkbox" value="MLE"/>
												<span>Meal</span>
											</div>
											<div style="border: 1px solid gray; border-bottom: none; padding: 4px; min-height: 17px;">
												<input id="chkSteService" name="chkSteService" type="checkbox" value="STE"/>
												<span>Seat</span>
											</div>
											<div style="border: 1px solid gray; border-bottom: none; padding: 4px; min-height: 17px;">
												<input id="chkFlxService" name="chkFlxService" type="checkbox" value="FLX"/>
												<span>Flexi</span>
											</div>
											<div style="border: 1px solid gray; border-bottom: none; padding: 4px; min-height: 17px;">
												<input id="chkHlaService" name="chkHlaService" type="checkbox" value="HLA"/>
												<span>Hala</span>
											</div>
											<div style="border: 1px solid gray; padding: 6px; padding-left: 17px; padding-bottom: 8px;">
												<span>Bundle Fee</span>
											</div>
										</td>
										<td valign="top" style="min-width: 720px">
											<div style="max-width:720px; min-height: 205px; display: block; overflow-x: scroll;">
												<table width="100%" border="0" cellpadding="0" cellspacing="0">
													<tr id="rowSelectedServices">
														<td id="cellSelectedServicesSample" style="min-width:330px;">
															<div style="border: 1px solid gray; border-bottom: none; padding-left: 18px; padding-top: 10px; min-height: 20px; padding-right: 5px;">
																<b class="timePeriodSample">&nbsp;</b>
																<img src="../../images/Warn_no_cache.gif" style="float:right; padding-left:2px; display: none;">
																<img src="../../images/Conf_no_cache.gif" style="float:right; padding-left:2px; display: none;">
																<input type="button" value="Display Settings" class="btnContentSample btnDefault Button ui-button ui-widget ui-corner-all" style="max-height: 17px; width: 110px; float: right; display: none;">
															</div>
															<div style="border: 1px solid gray; border-bottom: none; padding: 4px; min-height: 17px; padding-left: 18px;">
																<label class="baggageDetailSample">&nbsp;</label>
															</div>
															<div style="border: 1px solid gray; border-bottom: none; padding: 4px; min-height: 17px; padding-left: 18px;">
																<label class="mealDetailSample">&nbsp;</label>
															</div>
															<div style="border: 1px solid gray; border-bottom: none; padding: 4px; min-height: 17px; padding-left: 18px;">
																<label class="seatDetailSample">&nbsp;</label>
															</div>
															<div style="border: 1px solid gray; border-bottom: none; padding: 4px; min-height: 17px; padding-left: 18px;">
																<label class="flexiDetailSample">&nbsp;</label>
															</div>
															<div style="border: 1px solid gray; border-bottom: none; padding: 4px; min-height: 17px; padding-left: 18px;">
																<label class="halaDetailSample">&nbsp;</label>
															</div>
															<div style="border: 1px solid gray; padding: 6px; padding-left: 17px; min-height: 15px;">
																<input class="bundleFeeSample" type="text" maxlength="10" style="text-align:right; width: 80px; display: none;" />
															</div>
														</td>																														
													</tr>
												</table> 
											</div>
										</td>
										<td valign="top" style="min-width: 70px">
											<div style="border: 1px solid gray; border-bottom: none; padding-left: 18px; padding-top: 10px; min-height: 20px;">
												<b>&nbsp;</b>
											</div>
											<div style="border: 1px solid gray; border-bottom: none; padding: 4px; min-height: 17px;">
												<input name="btnEditBgd" type="button" id="btnEditBgd" value="Edit" class="btnDefault Button ui-button ui-widget ui-corner-all" style="max-height: 17px;">
											</div>
											<div style="border: 1px solid gray; border-bottom: none; padding: 4px; min-height: 17px;">
												<input name="btnEditMle" type="button" id="btnEditMle" value="Edit" class="btnDefault Button ui-button ui-widget ui-corner-all" style="max-height: 17px;">
											</div>
											<div style="border: 1px solid gray; border-bottom: none; padding: 4px; min-height: 17px;">
												<input name="btnEditSte" type="button" id="btnEditSte" value="Edit" class="btnDefault Button ui-button ui-widget ui-corner-all" style="max-height: 17px;">
											</div>
											<div style="border: 1px solid gray; border-bottom: none; padding: 4px; min-height: 17px;">
												<input name="btnEditFlx" type="button" id="btnEditFlx" value="Edit" class="btnDefault Button ui-button ui-widget ui-corner-all" style="max-height: 17px;">
											</div>
											<div style="border: 1px solid gray; border-bottom: none; padding: 4px; min-height: 17px;">
												<input name="btnEditHla" type="button" id="btnEditHla" value="Edit" class="btnDefault Button ui-button ui-widget ui-corner-all" style="max-height: 17px;">
											</div>
											<div style="border: 1px solid gray; padding: 6px; padding-left: 17px; padding-bottom: 8px;">
												<span>&nbsp;</span>
											</div>
										</td>
									</tr>
								</table>
							</div>
							
							<div id="tabPaneLinkWEBPOS">
								<table width="90%">
									<tr>										
										<td style="padding-left: 7px">
											<table width="60%">
												<tr>
													<td width="20%">
														<select id="sel_Country" name="sel_Country" style="width: 175; height: 180" multiple> </select>
													</td>
													<td width="15%" align="center">
														<table width="100%" style="padding-left: 15px">
															
															<tr>
																<td width="10%" id="addCountry">
																	<div class="Button" style="width: 30px; height: 20px">&gt;</div>
																</td>
															</tr>
															<tr>
																<td></td>
															</tr>
															<tr>
																<td width="10%" id="removeCountry">
																	<div class="Button" style="width: 30px; height: 20px">&lt;</div>
																</td>
															</tr>
															
														</table>
													</td>
													<td width="20%">
														<select id="sel_CountryAssigned" size="8" name="sel_CountryAssigned" style="width: 175; height: 180" multiple>
															<option value="">All</option>
														</select>
													</td>
												</tr>
											</table>
										</td>
									</tr>

								</table>
							</div>

						</div>

					</td>
				</tr>
				<tr>
					<td>
						<table width="98%">
							<tr>
							<tr>
								<td colspan="3" style="height: 25px;" valign="bottom"><input name="btnClose" type="button" id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault Button ui-button ui-widget ui-corner-all"> <input name="btnReset" type="button" id="btnReset"
									value="Reset" class="btnDefault Button ui-button ui-widget ui-corner-all"></td>
								<td align="right" valign="bottom"><input name="btnSave" type="button" id="btnSave" value="Save" class="btnDefault Button ui-button ui-widget ui-corner-all"></td>
							</tr>
							<tr></tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</div>
	<div id="imageUploadDialog" style="display: none;">
		<form id="frmImageUpload">
			<table>
				<tr>
					<td>Language :</td>
					<td colspan="2"><select id="selImageLanguage"><c:out value="${requestScope.reqLanguageList}" escapeXml="false" /></select></td>
				</tr>
				<tr>
					<td><label id="lblImgUpload">Bundled Fare Image :</label></td>
					<td>
						<div id="dvUpLoad">
							<input type="file" name="fileHOTFile" id="fileHOTFile">&nbsp;
						</div>
					</td>
					<td align="left" valign="top">&nbsp;&nbsp;<input name="btnUpload" type="button" id="btnUpload" value="Upload">
					</td>
				</tr>
				<tr>
					<td valign="top" colspan="3">
						<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center">
							<tr>
								<td><img name="imgBundle" id="imgBundle" src="" alt=""></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td></td>
					<td align="right" colspan="2"><input id="btnImageDialogClose" type="button" value="Close" class="btnDefault Button" /></td>
				</tr>
			</table>
		</form>
	</div>

	<div id="displaySettingsDialog" style="display: none;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td style="padding-top:5px;">		
					<div id="displaySettingsTabs" class="ui-border-topDis">
						<ul class="ui-border-leftDis ui-border-rightDis">
							<li id="tabDisplaySettings"><a href="#tabPaneDisplaySettings">Bundle Description Template</a></li>
							<li id="tabDisplayThumbnails"><a href="#tabPaneDisplayThumbnails">Thumbnail Images</a></li>
							<li id="tabDisplayTranslations"><a href="#tabPaneDisplayTranslations">Translations</a></li>
						</ul>
						<div id="tabPaneDisplaySettings">
							<div id="divBundleDisplayTemplate">
								<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding:10px;">
									<tr>
										<td style="width:70px;">Display TID</td>								
										<td style="width:100px;">
											<input id="displayTemplateId" type="text" maxlength="5" style="width:50px;">
										</td>
										<td>
											<img id="displayTemplateLoader" src="../../images/icons/loading_circle_no_cache.gif" height="20px" width="20px" style="display: none;"/>
											<label id="displayTemplateName"></label>
										</td>
										<td style="width:80px;" align="right">
											<input id="btnAddDisplayTemplate" type="button" value="Add" class="btnDefault Button ui-button ui-widget ui-corner-all" />
											<input id="btnChangeDisplayTemplate" type="button" value="Change" class="btnDefault Button ui-button ui-widget ui-corner-all" style="display: none;"/>
										</td>										
									</tr>
								</table>								
							</div>
							
							<div id="divBundleDescTemplateDisplayParent" style="overflow-y: scroll; max-height: 330px;padding-top: 7px;">
								<div id="divBundleDescTemplateSampleDisplay" style="padding-left:20px;padding-right:20px;padding-bottom:20px;width:230px;height:320px;display:inline-block;">
									<fieldset style="border: 1px solid rgb(153, 153, 153);width:200;height:310px;padding-left: 5px;padding-top: 5px;padding-right: 5px;">
										<legend><label id="bundleDescTrasLanguage">English</label></legend>
										<fieldset style="border: 1px solid rgb(153, 153, 153);width:190;height:135px;padding-left: 25px;padding-top: 10px;">
											<legend><label>Free Items</label></legend>
											<div id="divDescTempFreeContext"></div>
										</fieldset>
										<fieldset style="border: 1px solid rgb(153, 153, 153);width:190;height:135px;padding-left: 25px;padding-top: 10px;">
											<legend><label>Paid Items</label></legend>
											<div id="divDescTempPaidContext"></div>
										</fieldset>
									</fieldset>						
								</div>
							</div>							
								
						</div>
						<div id="tabPaneDisplayThumbnails">
							<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding:10px;">
								<tr>
									<td>
										<div id="divDisplayThumbnailsTemplate">
											<form id="frmThumbnailUpload">
												<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding:10px;">
													<tr>
														<td style="width:60px;">Language</td>
														<td style="width:120px;">
															<select id="selThumbnailImageLanguage"><c:out value="${requestScope.reqLanguageList}" escapeXml="false" /></select>
														</td>
														<td style="width:100px;">Thumbnail Image</td>
														<td>
															<input type="file" name="fileHOTFile" id="fileHOTFile" class="thumbnailImageFile">
														</td>
														<td align="right">
																<input id="btnUploadTumbnailImage" type="button" value="Upload" class="btnDefault Button ui-button ui-widget ui-corner-all" />
														</td>										
													</tr>
												</table>
											</form>							
										</div>
									</td>
								</tr>
								<tr>
									<td style="padding-top:20px;">
										<div style="overflow-y: scroll; max-height: 290px;">
											<table id="tblThumbnailImages" width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr id="rowThunbnailImages">
													<td style="min-width:200px;padding-bottom:20px;" valign="top">
														<img class="thumbnailImage_0" style="width:180px; height:260px;"/>
														<div class="thumbnailLanguage_0" style="text-align: center; margin-top: 5px;"></div>
													</td>
													<td style="min-width:200px;padding-bottom:20px;">
														<img class="thumbnailImage_1" style="width:180px; height:260px;"/>
														<div class="thumbnailLanguage_1" style="text-align: center; margin-top: 5px;"></div>													
													</td>
													<td style="min-width:200px;padding-bottom:20px;">
														<img class="thumbnailImage_2" style="width:180px; height:260px;"/>
														<div class="thumbnailLanguage_2" style="text-align: center; margin-top: 5px;"></div>													
													</td>
													<td style="min-width:200px;padding-bottom:20px;">
														<img class="thumbnailImage_3" style="width:180px; height:260px;"/>
														<div class="thumbnailLanguage_3" style="text-align: center; margin-top: 5px;"></div>													
													</td>																																								
												</tr>
											</table>
										</div>										
									</td>
								</tr>								
							</table>
														
						</div>
						<div id="tabPaneDisplayTranslations">
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<div id="divDisplayTranslationsTemplate">
											<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding:10px;">
												<tr>
													<td style="width:100px;">Language</td>
													<td style="width:150px;">Bundle Name</td>
													<td>Description</td>									
												</tr>
												<tr>
													<td>
														<select id="selTanslationTemplateLanguage"><c:out value="${requestScope.reqLanguageList}" escapeXml="false" /></select>
													</td>
													<td>
														<input id="displayTranslationName" type="text" size="10" style="width:120px;">
													</td>
													<td>
														<input id="displayTranslationDiscription" type="text" style="width:400px;">
													</td>
													<td align="right">
														<input id="btnAddTranslationTemplate" type="button" value="Add" class="btnDefault Button ui-button ui-widget ui-corner-all" />
														<input id="btnChangeTranslationTemplate" type="button" value="Change" class="btnDefault Button ui-button ui-widget ui-corner-all" style="display: none;"/>
													</td>																			
												</tr>									
											</table>								
										</div>
									</td>
								</tr>
								<tr>
									<td style="padding-top:10px;">
										<div id="divTranslationsTemplateGrid" style="background-color: rgb(236, 236, 236);">
											<table width="100%" border="0" cellpadding="0" cellspacing="0" id="jqGridBundledFareTranslations">
											</table>
											<input id="btnDeleteTranslationTemplate" type="button" value="Delete" class="btnDefault Button ui-button ui-widget ui-corner-all"/>
											<input id="btnResetTranslationTemplate" type="button" value="Reset" class="btnDefault Button ui-button ui-widget ui-corner-all" style="float:right"/>
										</div>											
									</td>
								</tr>								
							</table>						
						</div>								
					</div>
				</td>
			</tr>
			<tr>
				<td align="left" style="padding-top:10px;">
					<table id="bundleDisplayWarningMsgTable" style="display: none">
						<tr>
							<td style="width:25px;">
								<img src="../../images/Warn_no_cache.gif" style="padding:0; margin: 0;">
							</td>
							<td>
								<span>No bundle display template assigned yet.</span>
							</td>
						</tr>
					</table>
				</td>
			</tr>			
			<tr>				
				<td style="padding-top:15px;">
					<input id="btnConfirmDisplaySettings" type="button" value="Save Changes" class="btnDefault Button ui-button ui-widget ui-corner-all" style="width:160px; float:right;" />
					<input id="btnCloseDisplaySettings" type="button" value="Close" class="btnDefault Button ui-button ui-widget ui-corner-all" />
				</td>
			</tr>
		</table>
	</div>	

	<div id="internationalizationDialog" style="display: none;">
		<table>
			<tr>
				<td>Language :</td>
				<td><select id="selLanguage"><c:out value="${requestScope.reqLanguageList}" escapeXml="false" /></select></td>
			</tr>
			<tr>
				<td>Bundle Fare Name :</td>
				<td><input id="txtTranslatedBundledName" type="text" /></td>
			</tr>
			<tr>
				<td>Description :</td>
				<td><input id="txtTranslatedDesc" type="text" /></td>
			</tr>
			<tr>
				<td></td>
				<td align="right"><input id="btnAddTranslations" type="button" value=" + " class="btnDefault" style="width: auto" /> <input id="btnRemoveTranslations" type="button" value=" - " class="btnDefault" style="width: auto" /></td>
			</tr>
			<tr>
				<td colspan="2"><select id="selTranslations" size="10" multiple="multiple" style="height: 150px; width: 460px"></select></td>
			</tr>
			<tr>
				<td></td>
				<td align="right"><input id="btnTranslationReset" type="button" value="Reset" class="btnDefault Button" /> <input id="btnTranslationSubmit" type="button" value="Save" class="btnDefault Button" /></td>
			</tr>
		</table>
	</div>

	<div id="addServicesDialog" style="display: none;">
		<table>
			<tr>
				<td style="padding-top:5px;">
					<div id="divSelectedServiceTemplateGrid" style="background-color: rgb(236, 236, 236);">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="jqGridBundledFareServices">
						</table>
						<input id="btnDeleteAllServicePeriod" type="button" value="Delete" class="btnDefault Button ui-button ui-widget ui-corner-all" style="display: none;"/>
					</div>
				</td>
			</tr>			
			<tr>
				<td style="padding-top:10px;">
					<div id="divSelectedServiceTemplate">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding:10px;">
							<tr>
								<td>
									<table id="tblServiceTemplate">
										<tr>
											<td style="width:25px;">TID</td>								
											<td style="width:60px;">
												<input id="textServiceTemplateId" type="text" maxlength="5" style="width:50px;">
												<select id="flexiServiceTemplateId" style="width:50px;">
													<option value=""></option>
													<option value="INC">INC</option>
												</select>
											</td>
											<td style="width: 200px;">
												<img id="serviceTemplateLoader" src="../../images/icons/loading_circle_no_cache.gif" height="20px" width="20px" style="display: none;"/>
												<label id="serviceTemplateName">Best Template</label>
											</td>
										</tr>
										<tr>
											<td colspan="3">
												<div id="divAllowMultipleMeal"  style="padding-top:10px; display: none;">
													<input id="chkMleMultiple" name="chkMleMultiple" type="checkbox" value="allowMultipeMle"/>
													<span>Allow multiple meal selection<span></span>
												</div>
											</td>
										</tr>										
									</table>									
									<table id="tblHalaServiceTemplate">
										<tr>
											<td>
												<select id="halaAvailableTemplateIds" name="halaAvailableTemplateIds" style="width: 180; height: 80" multiple>
													<c:out value="${requestScope.reqSSRList}" escapeXml="false" />
												</select>
											</td>
											<td>
												<table width="100%">
													<tr>
														<td width="10%" id="addHalaTemplate">
															<div class="Button" style="width: 23px; height: 17px">&gt;</div>
														</td>
													</tr>
													<tr>
														<td width="10%" id="removeHalaTemplate">
															<div class="Button" style="width: 23px; height: 17px">&lt;</div>
														</td>
													</tr>
												</table>
											</td>
											<td>
												<select id="halaAssignedTemplateIds" size="8" name="halaAssignedTemplateIds" style="width: 180; height: 80" multiple>
												</select>
											</td>
										</tr>										
									</table>
								</td>

								<td valign="top" style="padding-right:25px;" align="right">From Date</td>
								<td valign="top">
									<input id="serviceFromDate" type="text" maxlength="10" name="serviceFromDate" onblur="validateAndPopulate('serviceFromDate')" />
								</td>
								<td valign="top" style="padding-right:25px;" align="right">To Date</td>
								<td valign="top" style="width: 80px;" align="right">
									<input id="serviceToDate" type="text" maxlength="10" name="serviceToDate" onblur="validateAndPopulate('serviceToDate')" />
								</td>																						
							</tr>
							<tr>
								<td style="padding-top:15px;" align="right" colspan="5">
									<input id="btnClearServicePeriod" type="button" value="Reset" class="btnDefault Button ui-button ui-widget ui-corner-all" style="float:left;"/>
									<input id="btnAddServicePeriod" type="button" value="Add" class="btnDefault Button ui-button ui-widget ui-corner-all" />
									<input id="btnEditServicePeriod" type="button" value="Change" class="btnDefault Button ui-button ui-widget ui-corner-all" style="display: none;"/>
									<input id="btnApplyAllServicePeriod" type="button" value="Apply All" class="btnDefault Button ui-button ui-widget ui-corner-all" style="width:80px;" />
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td align="left" style="padding-top:10px;">
								<span>BUNDLE validity period is from </span>
								<b><label id="noteFlightValidityFrom"></label></b>
								<span> to </span>
								<b><label id="noteFlightValidityTo"></label></b>
							</td>
						</tr>
						<tr>
							<td align="left">				
								<span id="noteTemplateValidity">
									<label id="noteTemplateServiceName"></label>
									<span> validity period is from </span>
									<b><label id="noteTemplateValidityFrom"></label></b>
									<span> to </span>
									<b><label id="noteTemplateValidityTo"></label></b>
								</span>
							</td>
						</tr>
						<tr>
							<td align="left" style="padding-top:15px;">								
								<input id="btnCloseServicePeriod" type="button" value="Close" class="btnDefault Button ui-button ui-widget ui-corner-all" />
								<input id="btnConfirmServicePeriod" type="button" value="Confirm Changes" class="btnDefault Button ui-button ui-widget ui-corner-all" style="width:160px; float:right;" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	
	  <div id = "samrtSearchHelp" title = "Smart Search Help" style="display: none">
	  		<br/>
	  		<br/>
	  		<ul>
	  			<li> 
	  			  Valid search texts are airport code, country code, country name and alias
	  			  <br/>
	  			  Examples
	  			  <br> 
	  			  Airport code - SHJ
	  			  <br>
	  			  Country code - AE
	  			  <br>
	  			  Country name - United Arab Emirates 
	  			  <br/>
	  			  Alias - The alias should be configured in the system. Support team will help you, to configure the alias
	  			  <br/>
	  			  Eg   Alias - Famous
	  			  <br> Linked airport codes - CMB, DEL, KWI, JED 
	  			   <br/>
	  			   <br/>   
	  			</li>
	  			<li>
	  				Search text patterns are below which are not case sensitive 
	  				<br/>
	  				1. ALL ALL ALL ALL ALL<br/>	  				
	  				2. CMB SHJ<br/>
	  				3. CMB SHJ BAH<br/>
	  				4. ALL SHJ<br/>
	  				5. ALL shj all<br/>
	  				6. IN shj<br/>
	  				7. India shj Saudi Arabia<br/>
	  				8. Famous shj
	  			   <br/>
	  			   <br/> 
	  			</li>
	  			<li>
	  				Sample search snapshot is below	
	  				<br/> 
	  				<br/>  
	  				<img style="max-width: 100%; height: auto;" width="1000" height="350" src="../../images/help/Bundle_Smart_Search.jpg"/>
	  				<br/>
	  				<br/>				
	  			</li>
	  		</ul>
	  </div>
      


	<script type="text/javascript" src="../../js/promotion/bundledFares.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script>
		var screenId = 'SC_PROM_07';

		top[2].HideProgress();
	</script>
</body>
</html>