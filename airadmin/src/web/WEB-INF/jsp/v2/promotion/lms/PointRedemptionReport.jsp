<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Point Redemption Detailed Report</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>
<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>
<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/jquery.ui.autocomplete.css"/>

<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript" src="../../js/v2/jquery/jquery.js?relversion=20090501120000" type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery-migrate.js?relversion=20090501120000" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?relversion=20090501120000" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?relversion=20090501120000" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?relversion=20090501120000" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?relversion=20090501120000" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?relversion=20090501120000" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?relversion=20090501120000" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?relversion=20090501120000" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.watermark.min.js?relversion=20090501120000" type="text/javascript"></script>
<script	src="../../js/v2/schedrept/schedrept.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"	type="text/javascript"></script>


<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var defCarrCode = "<c:out value="${requestScope.reqcrriercode}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" /> 
	<c:out value="${requestScope.sesLoyaltyProductCodesList}" escapeXml="false" /> 
	var arrOnlineAirports = new Array();
	<c:out value="${requestScope.sesOnlineAirportCodeListData}" escapeXml="false" />
</script>
</head>
<body onload="pageLoadRR()" class="tabBGColor" onbeforeunload="beforeUnload()" oncontextmenu="return false">
<form action="" method="post" id="frmPointRedemptionReport"	name="frmPointRedemptionReport">
<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">	
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
	<tr>
		<td><%@ include file="/WEB-INF/jsp/common/IncludeMandatoryText.jsp"%></td>
	</tr>
	<tr>
		<td><%@ include file="/WEB-INF/jsp/common/IncludeFormTop.jsp"%>Point Redemption Detailed Report<%@ include file="/WEB-INF/jsp/common/IncludeFormHD.jsp"%>
		<table width="100%" border="0" cellpadding="0" cellspacing="2"	ID="tblHead">
			
			<tr>
			<td width="90%">
				<table width="98%" border="0" cellpadding="0" cellspacing="2">	
					<tr>
					<td width="29%" align="left"><font>From Date &nbsp;</font>
						<input tabindex="10" name="txtFromDate" type="text"  onBlur="settingValidation('txtFromDate')" id="txtFromDate" size="10" style="width:55px;margin-right:2px;" maxlength="10" />&nbsp;
						<font class="mandatory hasDatepicker">&nbsp;*</font>
					</td>
					
					<td width="28%"><font>To Date &nbsp;</font>
						<input tabindex="20" name="txtToDate" type="text" onBlur="settingValidation('txtToDate')" id="txtToDate" size="10" style="width:55px;margin-right:2px;" maxlength="10" />&nbsp;
						<font class="mandatory hasDatepicker">&nbsp;*</font>
					</td>
					
					<td width="25%"><font>Flight No </font>
						<input tabindex="25" name="txtFlightNo" type="text" id="txtFlightNo" size="10" style="width:65px;" maxlength="10" />
					</td>
					
					<td width="5%">
						<font>Sector </font>
						<select name="selFromStn" id="selFromStn" size="1" style="width:55px" title="Origin" tabindex="30"></select>
					</td>
					<td>
						<select name="selVia1" id="selVia1" size="1" style="width:55px; margin-left:-15px;" title="First Intermediate Airport" tabindex="40"></select>
					</td>
					<td>
						<select name="selVia2" id="selVia2" size="1" style="width:55px;" title="Second Intermediate Airport" tabindex="50"></select>
					</td>
					<td>
						<select name="selToStn" id="selToStn" size="1" style="width:55px;" title="Destination" tabindex="60"></select>
					</td>
					</tr>
					
					<tr><td>&nbsp;</td></tr>
					
					<tr>
					<td width="25%"><font>PNR </font>
						<input tabindex="70"	name="txtPnr" type="text" id="txtPnr" size="10" style="width:75px;" maxlength="10" />
					</td>
					
					<td width="25%"><font>PAX Name </font>
						<input tabindex="80" name="txtPaxName" type="text" id="txtPaxName" size="10" style="width:75px;" maxlength="20" />
					</td>
					
					<td width="25%"><font>FFID </font>
						<input tabindex="90" name="txtFFID" type="text" id="txtFFID" size="10" style="width:95px;" />
					</td>
					
					<td width="25%"><font>Mobile </font>
						<input tabindex="100" name="txtMobileNo" type="text" id="txtMobileNo" size="10" style="width:65px;" maxlength="15" />
					</td>
					
					<td width="25%">
						<font>Product </font>
					</td>
					
					<td>
						<select tabindex="110" name="selProducts" id="selProducts" size="1" style="width:60px" title="Loyalty Product Codes" ></select>
					</td>
					</tr>
				</table>
			</td>
			</tr>
			
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			
			<tr>
			    <td><font class="fntBold">Display Currency</font></td>
			</tr>
			
			<tr>  
			<td>
				<input type="radio" name="radRptCurrency" id="radRptCurrency" value="USD" class="noBorder" checked="checked">
				<font>USD</font>
				<input type="radio" name="radRptCurrency" id="radRptCurrency" value="Base Currency" class="noBorder" >
				<font>Base Currency</font>
			</td>
			</tr>
			
			<tr><td>&nbsp;</td></tr>		
								
			<tr>				
				<td><font class="fntBold">Output Option</font></td>							
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>
					<table width="60%" border="0" cellpadding="0" cellspacing="2">
					<tr>
						<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
							value="HTML" class="noBorder" checked="checked"><font>HTML</font></td>
						<td width="20%"><input type="radio" name="radReportOption" id="radReportOption" 
							value="PDF" class="noBorder"><font>PDF</font></td>
						<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
							value="EXCEL" class="noBorder"><font>EXCEL</font></td>
						<td width="40%"><input type="radio" name="radReportOption" id="radReportOption"
							value="CSV" class="noBorder"><font>CSV</font></td>
						<td>&nbsp;</td>
					</tr>									
					</table>
						<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
				</td>									
			</tr>
			<tr><td>&nbsp;</td></tr>	
			<tr>	
				<div id="divSchedFrom"></div>			
				<td width="30%"><input type="button" id="btnClose"  name="btnClose" tabindex="130" value="Close" class="Button" onclick="closeClick()"></td>
				<td width="30%" align="right"><input name="btnSched" type="button" class="Button schdReptButton" id="btnSched" value="Schedule" onClick="viewClick(true)"> </td>
				<td width="30%" align="right"><input type="button" id="btnView"  tabindex="120" name="btnView" value="View" class="Button" onclick="viewClick()"></td>	
			</tr>		
		</table>
		<%@ include file="/WEB-INF/jsp/common/IncludeFormBottom.jsp"%></td>
	</tr>
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
</table>
<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
<input type="hidden" name="hdnLive" id="hdnLive" value="">
</form>
</body>
<script src="../../js/reports/PointRedemptionReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
    
   //-->
  </script>
</html>
