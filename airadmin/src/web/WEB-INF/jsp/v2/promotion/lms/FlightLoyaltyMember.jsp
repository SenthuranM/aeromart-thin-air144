<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Flight Loyalty Member Report</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>
<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>
<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/jquery.ui.autocomplete.css"/>

<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/fillDD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript" src="../../js/v2/jquery/jquery.js?relversion=20090501120000" type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery-migrate.js?relversion=20090501120000" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?relversion=20090501120000" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?relversion=20090501120000" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?relversion=20090501120000" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?relversion=20090501120000" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?relversion=20090501120000" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?relversion=20090501120000" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?relversion=20090501120000" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.watermark.min.js?relversion=20090501120000" type="text/javascript"></script>


<script type="text/javascript">
	var defCarrCode = "<c:out value="${requestScope.reqcrriercode}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" /> 
	var arrOnlineAirports = new Array();
	<c:out value="${requestScope.sesOnlineAirportCodeListData}" escapeXml="false" />
</script>
</head>
<body onload="pageLoadRR()" class="tabBGColor" onbeforeunload="beforeUnload()" oncontextmenu="return false">
<form action="" method="post" id="frmFlightLoyaltyMemberReport"
	name="frmModeofPayments">
<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">	
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
	<tr>
		<td><%@ include file="/WEB-INF/jsp/common/IncludeMandatoryText.jsp"%></td>
	</tr>
	<tr>
		<td><%@ include file="/WEB-INF/jsp/common/IncludeFormTop.jsp"%>Flight Loyalty Member Report<%@ include file="/WEB-INF/jsp/common/IncludeFormHD.jsp"%>
		<table width="100%" border="0" cellpadding="0" cellspacing="2"	ID="tblHead">
			
			<tr>
			<td width="90%">
				<table width="98%" border="0" cellpadding="0" cellspacing="2">	
					<tr>
					<td width="28%" align="left"><font>From Date &nbsp;</font><input tabindex="10"
					name="txtFromDate" type="text" id="txtFromDate" onBlur="settingValidation('txtFromDate')" size="10" style="width:75px;margin-right:5px;" maxlength="10" />
					<font class="mandatory hasDatepicker">&nbsp;*</font></td>
					
					<td width="28%"><font>To Date &nbsp;</font><input tabindex="20"
						name="txtToDate" type="text" id="txtToDate" onBlur="settingValidation('txtToDate')" size="10" style="width:75px;margin-right:5px;" maxlength="10" />
						<font class="mandatory hasDatepicker">&nbsp;*</font></td>
					
					
					<td width="25%"><font>Flight No </font>
						<input tabindex="30"	name="txtFlightNo" type="text" id="txtFlightNo" size="10" style="width:75px;" maxlength="10" >
					</td>
					
					<td width="5%">
						<font>Sector </font>
					</td>
					<td>	
						<select name="selFromStn" id="selFromStn" size="1" style="width:60px" title="Origin" tabindex="40"></select>
					</td>
					<td>
						<select name="selVia1" id="selVia1" size="1" style="width:60px" title="Intermediate Airport 1" tabindex="50">
							<option value="">Via 1</option>
						</select>
					</td>
					<td>
						<select name="selVia2" id="selVia2" size="1" style="width:60px" title="Intermediate Airport 2" tabindex="60">
							<option value="">Via 2</option>
						</select>
					</td>
					<td>
						<select name="selToStn" id="selToStn" size="1" style="width:60px" title="Destination" tabindex="70"></select>
					</td>
					
					</tr>
				</table>
			</td>
			
			</tr>
			
			<tr><td>&nbsp;</td></tr>							
			<tr>				
				<td><font class="fntBold">Output Option</font></td>							
			</tr>
			<tr>
				<td>
					<table width="60%" border="0" cellpadding="0" cellspacing="2">
					<tr>
						<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
							value="HTML" class="noBorder" checked="checked"><font>HTML</font></td>
						<td width="20%"><input type="radio" name="radReportOption" id="radReportOption" 
							value="PDF" class="noBorder"><font>PDF</font></td>
						<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
							value="EXCEL" class="noBorder"><font>EXCEL</font></td>
						<td width="40%"><input type="radio" name="radReportOption" id="radReportOption"
							value="CSV" class="noBorder"><font>CSV</font></td>
						<td>&nbsp;</td>
					</tr>									
					</table>
						<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
				</td>									
			</tr>
			<tr><td>&nbsp;</td></tr>	
			<tr>				
				<td width="70%"><input type="button" id="btnClose"  name="btnClose" tabindex="90" value="Close" class="Button" onclick="closeClick()"></td>
				<td width="30%" align="right"><input type="button" id="btnView"  tabindex="80" name="btnView" value="View" class="Button" onclick="viewClick()"></td>	
			</tr>		
		</table>
		<%@ include file="/WEB-INF/jsp/common/IncludeFormBottom.jsp"%></td>
	</tr>
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
</table>
<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
</form>
</body>
<script src="../../js/reports/FlightLoyaltyMember.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
    
   //-->
  </script>
</html>
