<%@ page language="java"%>
<%@ include file="../../../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Create Campaign</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
    <LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>
    <LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>
    <LINK rel="stylesheet" type="text/css" href="../../themes/default/css/jquery.ui.autocomplete.css"/>

    <script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script src="../../js/v2/jquery/jquery.json.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

    <script src="../../js/v2/tiny_mce/tiny_mce.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script type="text/javascript">
        var promoCriteriaId = "<c:out value="${requestScope.pcID}" escapeXml="false" />";
    </script>
    <style>
        .innerPanel{
            width:98%;
            margin:0 auto;
        }
        .portSel{
            width:50px
        }
        select{
            margin:1px 1px;
        }
        .pad-btm td{
            padding-bottom: 3px;
        }
        .iconBtn{
            width:25px
        }
        #ui-datepicker-div{
            display:none;
        }
        .ui-autocomplete { height: 200px; overflow-y: scroll; overflow-x: hidden;}
        .multipleSelect { height:100px; width:150px }


    </style>
</head>
<body class="tabBGColor" scroll="no" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
<script type="text/javascript">
    <c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
    var bcJSONList=<c:out value="${requestScope.reqBCList}" escapeXml="false" />
</script>


<div id="campaignCriteria" style="text-align: left; background-color: #ECECEC;">
    <table class="innerPanel">
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td width="20%">Campaign Name : <font class="mandatory">&nbsp;*&nbsp;</font></td>
                        <td>
                            <input id="txtOfferName" type="text" style="width: 95%"/>
                        </td>
                        <td width="60%">
                            <table width="100%">
                                <tr>
                                    <td width="20%"></td>
                                    <td>From</td>
                                    <td>To</td>
                                </tr>
                                <tr>
                                    <td>Booking Date : <font class="mandatory">&nbsp;*&nbsp;</font></td>
                                    <td><input type="text" id="bookingDateFrom"></input></td>
                                    <td><input type="text" id="bookingDateTo"></input></td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                    <tr>
                        <td width="20%">Campaign Description : </td>
                        <td>
                            <textarea id="txtCampaignDescription" style="width:95%" class="htmlsup"></textarea>
                        </td>
                        <td width="50%">
                            <table width="100%">
                                <tr>
                                    <td width="20%"></td>
                                    <td>From</td>
                                    <td>To</td>
                                </tr>
                                <tr>
                                    <td>Travel Date : <font class="mandatory">&nbsp;*&nbsp;</font></td>
                                    <td><input type="text" id="travelDateFrom"></input></td>
                                    <td><input type="text" id="travelDateTo"></input></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="90%">
                                <tr>
                                    <td> <label>Origin : <font class="mandatory">&nbsp;*&nbsp;</font></label> </td>
                                    <td>
                                        <select id="selOrigin">
                                            <option value=""></option>
                                            <option value="ALL">ALL</option>
                                        </select>
                                    </td>
                                    <td> <label>Destination : <font class="mandatory">&nbsp;*&nbsp;</font></label> </td>
                                    <td>
                                        <select id="selDestination">
                                            <option value=""></option>
                                            <option value="ALL">ALL</option>
                                        </select>

                                    </td>
                                    <td>
                                        <input id="btnAddOND" type="button" value= " + " class="iconBtn addItem"></input>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="60%">
                            <table width="100%">
                                <tr>
                                    <td width="20%"></td>
                                    <td>From</td>
                                    <td>To</td>
                                </tr>
                                <tr>
                                    <td>Age Range : </td>
                                    <td><input type="text" id="ageFrom"></input></td>
                                    <td><input type="text" id="ageTo"></input></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="95%">
                                <tr>
                                    <td>
                                        <label>Via 1</label><br>
                                        <select class="portSel" id="selVia1">
                                            <option value=""></option>
                                        </select>
                                    </td>
                                    <td>
                                        <label>Via 2</label><br>
                                        <select class="portSel" id="selVia2">
                                            <option value=""></option>
                                        </select>
                                    </td>
                                    <td>
                                        <label>Via 3</label><br>
                                        <select class="portSel" id="selVia3">
                                            <option value=""></option>
                                        </select>
                                    </td>
                                    <td>
                                        <label>Via 4</label><br>
                                        <select class="portSel" id="selVia4">
                                            <option value=""></option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <select id="selONDs" size="10" multiple="multiple" class="multipleSelect" style="width: 100%"></select>
                                    </td>
                                    <td valign="top">
                                        <input id="btnRemoveOND" type="button" value= " - " class="iconBtn removeItem"></input>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" style="padding-top: 5px">
                            <table width="100%">
                                <tr>
                                    <td width="50%" valign="top">
                                        <table width="100%">
                                            <tr>
                                                <td valign="top" width="30%">
                                                    Location :
                                                </td>
                                                <td>
                                                    <select id="selLocation" style="width: 100%">
                                                        <option value="ALL">ALL</option>
                                                    </select>
                                                </td>
                                                <td width="10%" valign="middle">
                                                    <input id="btnAddLocation" type="button" value= " + " class="iconBtn addItem"></input>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <select id="selectedLocations" size="10" multiple="multiple" class="multipleSelect" style="width: 100%"></select>
                                                </td>
                                                <td valign="top">
                                                    <input id="btnRemoveLocation" type="button" value= " - " class="iconBtn removeItem"></input>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td valign="top" width="30%">
                                                    Fare Type :
                                                </td>
                                                <td>
                                                    <select id="selFare" style="width: 100%">
                                                        <option value="One Way">One Way</option>
                                                		<option value="Return">Return</option>                                              
                                                    </select>
                                                </td>
                                                <td width="10%" valign="middle">
                                                    <input id="btnAddFare" type="button" value= " + " class="iconBtn addItem"></input>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <select id="selectedFares" size="10" multiple="multiple" class="multipleSelect" style="width: 100%"></select>
                                                </td>
                                                <td valign="top">
                                                    <input id="btnRemoveFare" type="button" value= " - " class="iconBtn removeItem"></input>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 5px; padding-bottom: 10px">
                <input name="btnSetTargets" type="button" id="btnSetTargets" value="Create" class="btnDefault Button"/>
                <label>   Promo Codes to Send : </label>           	
                <input name="noOfPromoCodes" type="text" id="noOfPromoCodes" disabled="disabled" style="width:40px"/>            
            </td>           
        </tr>
    </table>
</div>

<div id="campaignContent" style="text-align: left; background-color: #ECECEC;display: none">
    <div style="width: 100%;">
        <textarea id="campaignEditor" class="campaignEditor"></textarea>
    </div>
    <div style="width:98%;padding: 10px 5px;" >
        <table width="100%">
<!--             <tr> -->
<!--                 <td width="25%">Frequency : </td> -->
<!--                 <td width="25%">Months</td> -->
<!--                 <td width="25%">Days</td> -->
<!--                 <td width="25%">Hours</td> -->
<!--             </tr> -->
<!--             <tr> -->
<!--                 <td></td> -->
<!--                 <td> -->
<!--                     <table width="100%"> -->
<!--                         <tr> -->
<!--                             <td> -->
<!--                                 <select id="selMonth" style="width: 100%"> -->
<!--                                     <option value="ALL">ALL</option> -->
<!--                                 </select> -->
<!--                             </td> -->
<!--                             <td width="10%" valign="middle"> -->
<!--                                 <input id="btnAddMonth" type="button" value= " + " class="iconBtn addItem"></input> -->
<!--                             </td> -->
<!--                         </tr> -->
<!--                         <tr> -->
<!--                             <td> -->
<!--                                 <select id="selectedMonths" size="10" multiple="multiple" class="multipleSelect" style="width: 100%"></select> -->
<!--                             </td> -->
<!--                             <td valign="top"> -->
<!--                                 <input id="btnRemoveMonth" type="button" value= " - " class="iconBtn removeItem"></input> -->
<!--                             </td> -->
<!--                         </tr> -->
<!--                     </table> -->
<!--                 </td> -->
<!--                 <td> -->
<!--                     <table width="100%"> -->
<!--                         <tr> -->
<!--                             <td> -->
<!--                                 <select id="selDay" style="width: 100%"> -->
<!--                                     <option value="ALL">ALL</option> -->
<!--                                 </select> -->
<!--                             </td> -->
<!--                             <td width="10%" valign="middle"> -->
<!--                                 <input id="btnAddDay" type="button" value= " + " class="iconBtn addItem"></input> -->
<!--                             </td> -->
<!--                         </tr> -->
<!--                         <tr> -->
<!--                             <td> -->
<!--                                 <select id="selectedDays" size="10" multiple="multiple" class="multipleSelect" style="width: 100%"></select> -->
<!--                             </td> -->
<!--                             <td valign="top"> -->
<!--                                 <input id="btnRemoveDay" type="button" value= " - " class="iconBtn removeItem"></input> -->
<!--                             </td> -->
<!--                         </tr> -->
<!--                     </table> -->
<!--                 </td> -->
<!--                 <td> -->
<!--                     <table width="100%"> -->
<!--                         <tr> -->
<!--                             <td> -->
<!--                                 <select id="selHour" style="width: 100%"> -->
<!--                                     <option value="ALL">ALL</option> -->
<!--                                 </select> -->
<!--                             </td> -->
<!--                             <td width="10%" valign="middle"> -->
<!--                                 <input id="btnAddHour" type="button" value= " + " class="iconBtn addItem"></input> -->
<!--                             </td> -->
<!--                         </tr> -->
<!--                         <tr> -->
<!--                             <td> -->
<!--                                 <select id="selectedHours" size="10" multiple="multiple" class="multipleSelect" style="width: 100%"></select> -->
<!--                             </td> -->
<!--                             <td valign="top"> -->
<!--                                 <input id="btnRemoveHour" type="button" value= " - " class="iconBtn removeItem"></input> -->
<!--                             </td> -->
<!--                         </tr> -->
<!--                     </table> -->
<!--                 </td> -->
<!--             </tr> -->

            <tr>
                <td colspan="4">
                    <table>
                        <tr>
                            <td>
                                <input name="btnSubmitCampaign" type="button" id="btnSubmitCampaign" value="Submit" class="btnDefault Button"/>
                            </td>
                             <td>
                             	<div id="divSchedFrom"></div>
                                <input name="btnScheduleCampaign" type="button" id="btnScheduleCampaign" value="Schedule" class="btnDefault Button"/>
                            </td>
                            <td>
                                <input name="btnCancelSubmit" type="button" id="btnCancelSubmit" value="Cancel" class="btnDefault Button"/>
                            </td>
                        </tr>
                    </table>


                </td>
            </tr>
        </table>
    </div>
<!--     <input type="hidden" id="promoCriteriaIdHdn" name="promoCriteriaIdHdn"> -->
</div>
<div class="PageBottomColor" style="margin-top:10px">
		<table width="100%" cellspacing="1" cellpadding="0" border="0" id="Table6">
			<tbody><tr>
				<td width="20">&nbsp;</td>
				<td width="20" valign="bottom"><img id="imgError" border="0" src="../../images/Err_no_cache.gif" name="imgError" style="visibility: hidden; display: none;" /></td>
				<td width="1"><img src="../../images/StatusBar_no_cache.jpg" /></td>
				<td id="tdSTBar" class="StatsBarDef"><span id="spnDate">&nbsp;</span></td>
				<td width="1"><img src="../../images/StatusBar_no_cache.jpg" /></td>
				<td width="150" align="center"><span id="spnUID"></span></td>
				<td width="1"><img src="../../images/StatusBar_no_cache.jpg" /></td>
				<td width="200" align="center"><span id="spnScreenID"><font class="fontPGInfo"></font></span></td>
				<td width="20">&nbsp;</td>
			</tr>
		</tbody></table>
	</div>
<script type="text/javascript" src="../../js/promotion/CreateCampaign.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/promotion/ScheduleCampaign.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script>
    var screenId = 'SC_PROM_09';
    top[2].HideProgress();
</script>
<script type="text/javascript">			
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />										
</script>
</body>
</html>