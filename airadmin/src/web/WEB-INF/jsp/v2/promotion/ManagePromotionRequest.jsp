<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<%@ page import="com.isa.thinair.promotion.api.to.constants.PromoTemplateParam"%>
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;
	Date dStopDate = null;
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date
	cal.add(Calendar.DATE,3);
	dStopDate = cal.getTime(); // three days after current date
	String StartDate =  formatter.format(dStartDate);
	String StopDate = formatter.format(dStopDate);
%> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Felxidate Promotion Requests</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script>
		var screenId = 'SC_PROM_02';
		var cameFrom = "<c:out value='${requestScope.cameFrom}' escapeXml='false' />";
		var fl_ID = "<c:out value='${requestScope.flID}' escapeXml='false' />";
</script>
<script type="text/javascript" src="../../js/promotion/ManagePromotionRequest.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript">	

</script>
</head>
<script type="text/javascript">	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" /></script>

<body class="tabBGColor" scroll="no" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
	<div id="divSearch" style="height: 30px; text-align: left;">
		<table width="96%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
			<tr>
				<td><font>Flight Number</font>
				</td>
				<td><input type="text" name="txtFlightNumber" id="txtFlightNumber" value="" style="width: 80px;" maxlength="10"></td>
				<td><font>Dep Start Date</font>
				</td>
				<td><input type="text" name="txtStartDateSearch" id="txtStartDateSearch" value=<%=StartDate%> style="width: 72px;" maxlength="10"><font class="mandatory">&nbsp;*</font>
				</td>
				<td><font>Dep End Date</font>
				</td>
				<td><input type="text" name="txtStopDateSearch" id="txtStopDateSearch" value=<%=StopDate %> style="width: 72px;" maxlength="10"><font class="mandatory">&nbsp;*</font></td>
				<td align="right"><input name="btnSearch" type="button" id="btnSearch" value="Search" onclick="searchFlights()" class="button"></td>
			</tr>
		</table>
	</div>
	<div id="divResultsPanel" style="height: 240px; text-align: left; background-color: #ECECEC;">
		<div id="jqGridFlightsContainer" style="margin-top: 5px;">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" id="jqGridFlightsData">
			</table>
			<div id="jqGridFlightsPages"></div>
		</div>
	</div>
	<div id="divDispPromoReqs" style="height: 275px; text-align: left; background-color: #ECECEC;">
		<div id="divPromoReqsPanel" style="height: 240px; text-align: left; background-color: #ECECEC;">
			<div id="jqGridPromoReqsContainer" style="margin-top: 5px;">
				
			</div>
		</div>		
		<table  width="100%">
			<tr>
				<td width="15%">Total Revenue : </td>
				<td width="25%"><input type="text" name="txtTotalRevenue" id="txtTotalRevenue" value="" style="width: 80px;" maxlength="10"></td>
				<td width="60%"  align="right"><input name="btnApproveReqs" type="button" id="btnApproveReqs" value="Approve Requests" class="btnLarge button" onclick="approvePromoRequests()"></td>
			</tr>
		</table>
	</div>
	<div id='divLoadMsg'
		style='visibility: hidden; z-index: 1000; background: transparent;'>
		<iframe src="LoadMsg" id='frmLoadMsg' name='frmLoadMsg'
		frameborder='0' scrolling='no'
		style='position: absolute; top: 50%; left: 40%; width: 260; height: 40;'></iframe>
	</div>
	<script>
		// Promotion template params
		var PromoTemplateParams = {};
		PromoTemplateParams['FlexiDate.DaysLowerBound'] = "<%=PromoTemplateParam.TemplateConfig.FLEXI_DATE_LOWER_BOUND%>";
		PromoTemplateParams['FlexiDate.DaysUpperBound'] = "<%=PromoTemplateParam.TemplateConfig.FLEXI_DATE_UPPER_BOUND%>";
		PromoTemplateParams['FreeSeat.SeatsMin'] = "<%=PromoTemplateParam.TemplateConfig.NEXT_SEAT_FREE_SEATS_MIN%>";
		PromoTemplateParams['FreeSeat.SeatsMax'] = "<%=PromoTemplateParam.TemplateConfig.NEXT_SEAT_FREE_SEATS_MAX%>";
		PromoTemplateParams['FreeSeat.MinVacant'] = "<%= PromoTemplateParam.TemplateConfig.NEXT_SEAT_FREE_MIN_VACANT_SEATS %>";
		PromoTemplateParams['FreeSeat.SeatsPreserve'] = "<%= PromoTemplateParam.TemplateConfig.NEXT_SEAT_FREE_PRESERVE_SEATS %>";
		
		// Promotion charges/rewards params
		var PromoRewardsAndCharges = {};
		PromoRewardsAndCharges['FlexiDate.PrefixRewardOnDays'] = "<%=PromoTemplateParam.ChargeAndReward.FLEXI_DATE_DAYS_REWARD.getPrefix()%>" ;
		PromoRewardsAndCharges['FreeSeat.RegistrationCharge'] = "<%=PromoTemplateParam.ChargeAndReward.NEXT_SEAT_FREE_REG_CHRG.getPrefix()%>" ;
		PromoRewardsAndCharges['FreeSeat.PrefixChargeOnSeats'] = "<%=PromoTemplateParam.ChargeAndReward.NEXT_SEAT_FREE_SEAT_CHRG.getPrefix()%>" ;
		if (top[2]!=undefined){
			top[2].HideProgress();
		}else{
			HideProgress();
		}
		
	</script>
</body>
</html>