<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="com.isa.thinair.promotion.api.to.constants.PromoTemplateParam" %> 
<table width="100%" border="0" cellpadding="0" cellspacing="2" id="chargesNextSeatFree">
	<tr>
		<td colspan="2"><font>Flexibility Rewards</font>
		</td>
	</tr>
	<c:forEach var="noOfDays" begin="${requestScope.flexLowerBound}" end="${requestScope.flexUpperBound}" step="1">
		<c:choose>
			<c:when test="${noOfDays == 1}">
				<td style="padding-left: 40px"><font>Per Day</font>
				</td>
			</c:when>
			<c:otherwise>
				<td style="padding-left: 40px"><font>Per ${noOfDays} Days</font>
				</td>
			</c:otherwise>
		</c:choose>
			<td><input type="text" name="${PromoTemplateParam.FlexiDate.ChargeAndReward.getFlexibilityRewardParam(noOfDays)}" 
				id="${PromoTemplateParam.FlexiDate.ChargeAndReward.getFlexibilityRewardParam(noOfDays)}" value="-" 
				style="width: 100px;" maxlength="10" align="middle"></td>
				
	</c:forEach>
</table>