<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ page import="com.isa.thinair.promotion.api.to.constants.PromoTemplateParam"%>
<table width="100%" border="0" cellpadding="0" cellspacing="2" id="chargesNextSeatFree">
	<tr>
		<td width="40%"><font>Refundable Charge</font></td>
		<td><input type="text" name="txtStartDateSearch" id="txtStartDateSearch" onBlur="settingValidation('txtStartDateSearch')" invalidText="true" value="-" style="width: 100px;" maxlength="10"
			tabindex="1" align="middle" onChange="hdeErrmessages()"></td>
	</tr>
	<tr>
		<td colspan="2"><font>Non-Refundable Charges</font></td>
	</tr>
	<c:forEach var="noOfDays" begin="${requestScope.seatsLowerBound}" end="${requestScope.seatsUpperBound}" step="1">
		<c:choose>
			<c:when test="${noOfDays == 1}">
				<td style="padding-left: 40px"><font>Per Seat</font></td>
			</c:when>
			<c:otherwise>
				<td style="padding-left: 40px"><font>Per ${noOfDays} Seats</font></td>
			</c:otherwise>
		</c:choose>
		<td><input type="text" name="${PromoTemplateParam.FreeSeat.ChargeAndReward.getFreeSeatChargeParam(noOfDays)}" id="txtStartDateSearch" onBlur="settingValidation('txtStartDateSearch')"
			invalidText="true" value="-" style="width: 100px;" maxlength="10" tabindex="1" align="middle">
		</td>
	</c:forEach>
</table>