<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Create Promotion Templates</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>
<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>
<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/jquery.ui.autocomplete.css"/>


<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.watermark.min.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/promotion/popupData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/v2/common/jQuery.multiselect.js?relversion=20160501120000" type="text/javascript"></script>
	
<script type="text/javascript">	
var languageHTML = "<c:out value="${requestScope.reqLanguageList}" escapeXml="false" />";
</script>
<style>
	.innerPanel{
		width:98%;
		margin:0 auto;
	}
	.portSel{
		width:50px
	}
	select{
		margin:1px 1px;
	}
	.iconBtn{
		width:25px
	}
	#ui-datepicker-div{
		display:none;
	}
	.ui-autocomplete { height: 200px; overflow-y: scroll; overflow-x: hidden;}
	.input.uppercase{  
		text-transform: uppercase;  
	}  
</style>
</head>
<body class="tabBGColor" scroll="no" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
 <input type="hidden" name="hdnSelectID" id="hdnSelectID" value=0>
 <input type="hidden" name="hdnPromoCodeID" id="hdnPromoCodeID" value=0>
 <input type="hidden" name="hdnPromoCodeDesTrans" id="hdnPromoCodeDesTrans" value=0>
<script type="text/javascript">			
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />										
			</script>
			
	<div id="divPromeSearch" style="text-align: left;background-color: #ECECEC;" >
		<div class="innerPanel divsearch" style="border-bottom:1px solid #bbb">
		<table width="96%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
			<tr>
				<td><font>From</font></td>
				<td>
					<select name="selFrom" id="selFrom" style="width: 80px;">
					<option value="">All</option>
					</select>
				</td>
				<td><font>To</font></td>
				<td>
					<select name="selTo"  id="selTo" style="width: 80px;">
					<option value="">All</option>
					</select>
				</td>
				<td><font>Promotion Codes</font></td>
				<td><input name="txtPromotionCodes" size="40" id="txtPromotionCodes" style="width: 120px;" onkeyUp="return ValidateFlagText(this,event);" onkeyPress="return ValidateFlagText(this,event);"/></td>
				<td></td>
			</tr>
		
			<tr>
				<td><font>Start Date</font></td>
				<td><input type="text" name="txtStartDateSearch" id="txtStartDateSearch" value="" style="width: 62px;" maxlength="10"  readonly="readonly"></td>
				<td><font>End Date</font></td>
				<td><input type="text" name="txtStopDateSearch" id="txtStopDateSearch" value="" style="width: 62px;" maxlength="10"  readonly="readonly"> </td>
				<td><font>Status</font></td>
				<td>
					<select name="selStatus" id="selStatus" style="width: 80px;">
						<option value="-">Any</option>
						<option value="ACT">Active</option>
						<option value="INA">Inactive</option>
					</select>
				</td>
				<td align="right"><input name="btnSearch" type="button" id="btnSearch" value="Search"  class="button">
				</td>
			</tr>
			
		</table>
		</div>
		<div class="innerPanel divResults" style="border-top:1px solid #fff">
			<div id="jqGridPromoCodesContainer" style="margin-top: 5px;">
				<table width="100%" border="0" cellpadding="0" cellspacing="0" id="jqGridPromoCodesData">
				</table>
				<div id="jqGridPromoCodesPages"></div>
			</div>
		<input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault button"   style="width:auto">
		<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault button"   style="width:auto">
		<input name="btnDelete" type="button" id="btnDelete" value="Delete" class="btnDefault button"  style="width:auto">
		<input type="hidden" name="promoStatus" value="Y" id="promoStatus" class="button" style="width:auto"/>
		
	<input type="button" value= "Add Description For Display" onClick="return POSenable();" id="btnPromoFD" class="btnDefault button"  style="width:auto">
	<input name="btnExport" type="button" id="btnExport" value="Export" class="btnDefault button"  style="width:auto">
	<input name="btnCopy" type="button" id="btnCopy" value="Copy Settings and Create New" class="btnDefault button"  style="width:auto">
	</div>
	</div>

	<div id="promoCodeDetails" style="text-align: left; background-color: #ECECEC;">
		<form method="post" action="promotionTemplatesManagementAction.action" id="frmPromotion">
				<div id="tabPromoDetails" class="innerPanel">
					<div style="display: none;">
					</div>
								<table width="100%" border="0" cellpadding="0" cellspacing="2" >
										<br><tr>
											<td colspan="1" style="padding-right:10px" valign="top"><font>Enable</font><br/>
												<input style="margin-top:8px;margin-left:10px" type="checkbox" id="enblePromoCode" />
											</td>
											<td colspan="1" valign="top"><font>Promocode</font><br/>
												<input id="promoCode" style="width:85%;margin-top:8px" class="uppercase" maxlength="40" type="text" >
												<font align="center">  OR  </font>
											</td>
											<!-- <td colspan="1" valign="top"><br><font>OR</font></td> -->
											<td colspan="3" valign="top">
											<table align="center"><tbody><tr>
											<td id="beforeGenerate"><font>Generate Multiple Promo Codes</font></td>
											<td id="afterGenerate" style="display: none;"><font>Multiple Promo Codes generated</font></td>
											</tr>
												<tr>
												<td><input id="numOfCodes" class="rightAlign portSel" onkeypress="KPValidateDecimelDup(this)" onkeyup="KPValidateDecimelDup(this);handleSingleOrMultiPromocode();" style="width:30%;margin-top:2px;margin-right:10px" type="text">
												<input id="btnGenerate" name="btnGenerate" value="Generate" class="btnDefault button" type="button">
												<input id="btnView" name="btnView" value="View" class="btnDefault button" style="display: none;" type="button"></td></tr>
												</tbody></table>
											</td>
											<td colspan="1" valign="top"><font>Promo Name</font><font class="mandatory">&nbsp;*</font><br>
												<input id="promoName" class="leftAlign" style="width:90%;margin-top:8px" type="text">
									 		</td>
											<td colspan="3" style="width:200px;"></td>
										</tr>
										<tr><td colspan="10" style="height:15px;"></td></tr>
										<tr>
											<td colspan="2" valign="top"><font>Promo Type</font><font class="mandatory">&nbsp;*</font><br/>
												<select id="selpromoType" style="width:90%">
													<option value="Buy And Get Free">Buy And Get Free</option>
													<option value="Discount">Discount</option>
													<option value="Free Service">Free Service</option>
												</select>
											</td>
											<td valign="top"><font>Status</font><br/>
												<select id="selStatus_1" style="width: 80px;">
													<option value="ACT">Active</option>
													<option value="INA">Inactive</option>
												</select>
											</td>
											<td valign="top" colspan="4"><font>Restrictions</font><br/>
												<input id="chkModification" type="checkbox"> <label>Modification</label>
												<input id="chkCancelation" type="checkbox"> <label>Cancelation</label>
												<input id="chkSplitting" checked="checked" disabled="disabled" type="checkbox"> <label>Splitting</label>
												<input id="chkRemovePax" type="checkbox"> <label>Remove Pax</label>
											</td>
										</tr>
									<tr><td colspan="10" style="height:15px;"></td></tr>
									<tr id="trPromoCodeType">
								<td colspan="10" >
									<fieldset style="border: 1px solid rgb(153, 153, 153);" id="trPromoCodePanel">
										<legend><label>Promo Type Related Panel</label></legend>
									<fieldset style="border:1px solid #999;margin:5px;display:none" id="f_Discount">
										<legend><label >Discount</label></legend>
										<table width="70%" align="center">
											<tr>
												<td width="10%"><font>Discount</font><font class="mandatory">&nbsp;*</font></td>
												<td width="30%">
													<select id="selpromoTypeDiscount" style="width:80%">
													</select>
													
												</td>
												<td width="10%">
													<font>Value</font><font class="mandatory">&nbsp;*</font>
												</td>
												<td width="10%">
													<input type="text" id="disValue"  size="10" class="rightAlign" onKeyPress="KPValidateDecimelDup(this,10,2)" onKeyUp="KPValidateDecimelDup(this,10,2)"/>
													
												</td>
												<td  width="18%"><font>Bank ID No (BIN)</font></td>
												<td rowspan="3" width="30%"><br>
												<table width="90%" border="0" cellpadding="0" cellspacing="0" ><tr><td>
													<input type="text" id="BIN" style="width:99%" />
													<br />
													<select size="10" id="sel_BIN" style="height:75px;width:99%" multiple="multiple"></select>
												<td width="25" valign="top"><input type="button" id="add_BIN" value=" + " class="iconBtn addItem"/><br />
													<input type="button" id="del_BIN" value=" - " class="iconBtn removeItem"/>
												</td></tr></table>
												</td>
											</tr>
											<tr><td style="height:5px;" colspan="3"></td></tr>
											<tr>
												<td><font>Apply to</font><font class="mandatory">&nbsp;*</font></td>
												<td>
													<select id="selApplyToDiscount" style="width:80%">
													</select>
													
												</td>
												<td>
													<font>Apply As</font><font class="mandatory">&nbsp;*</font>
												</td>
												<td>
													<select id="selApplyToType" style="width:80%">
													</select>
													
												</td>
											</tr>
											<tr>
											<td colspan="6">
											
												<label>Credit Constraints</label>
												<table border="0" width="98%">
												<tr>
													<td width="3%">&nbsp;</td>
													<td width="14%"><input id="constPaxName" type="checkbox" value="Y" name="constPaxName"> <font>Pax name</font></td>
													<td width="3%">&nbsp;</td>
													<td width="14%"><input id="constSector" type="checkbox" value="Y" name="constSector"> <font>Sector</font></td>
													<td colspan="5"></td>
												</tr>
												<tr>
													<td colspan="4" ><font>Flight Period</font></td>
													<td width="3%">&nbsp;</td>
													<td colspan="4"><font>Booking Period </font></td>
												</tr>
												<tr>
													<td width="3%"><font> From</font></td>
													<td width="14%"><input type="text" id="constFlightPeriodFrom" style="width:70%" maxlength="10"  readonly="readonly"/></td>
													<td width="3%"><font>To</font></td>
													<td width="14%"><input type="text" id="constFlightPeriodTo"  style="width:70%" maxlength="10"  readonly="readonly"/></td>
													<td width="10%">&nbsp;</td>
													<td width="3%"><font> From</font></td>
													<td width="14%"><input type="text" id="constBookingPeriodFrom" style="width:70%" maxlength="10"  readonly="readonly"/>
													</td>
													<td width="3%"><font>To</font></td>
													<td  width="14%">	<input type="text" id="constBookingPeriodTo"  style="width:70%" maxlength="10"  readonly="readonly"/>
												</td>
												</tr>
												</table>
													
											</td>
											</tr>
											<tr><td style="height:5px;" colspan="5"></td></tr>
										</table>
									</fieldset>
									<fieldset style="border:1px solid #999;margin:5px;display:none" id="f_Free Service">
										<legend><label >Free Service</label></legend>
										<table width="70%" align="center">
											<tr>
												<td rowspan="3" valign="top" width="20%"><font>Ancillaries</font><font class="mandatory">&nbsp;*</font></td>
												<td rowspan="3" width="30%">
													<!-- <table width="90%" border="0" cellpadding="0" cellspacing="0" ><tr><td> -->
														<select size="10" id="sel_Anicillaries" style="height:95px;width:99%" multiple="multiple"></select>
													<!-- <td width="25" valign="top"><input type="button" id="add_Anicillaries" value=" + " class="iconBtn addItem"/><br />
														<input type="button" id="del_Anicillaries" value=" - " class="iconBtn removeItem"/>
													</td></tr></table> -->
												</td>
												<td width="10%"><font>Discount</font><font class="mandatory">&nbsp;*</font></td>
												<td width="30%">
													<select id="selpromoTypeAncillary" style="width:80%">
													</select>
													
												</td>
												<td width="10%">
													<font>Value<font class="mandatory">&nbsp;*</font></font>
												</td>
												<td>
													<input type="text" id="disValueAncillary" class="rightAlign" size="10" onKeyPress="KPValidateDecimelDup(this,10,2)" onKeyUp="KPValidateDecimelDup(this,10,2)"/>
													
												</td> 
											</tr>
											<tr><td style="height:5px;" colspan="3"></td></tr>
											<tr>
												<td id="tdAplAnc"><font>Apply to</font><font class="mandatory">&nbsp;*</font></td>
												<td id="tdAplAnc1">
													<select id="selApplyToAncillary" style="width:80%" >
													</select>
													
												</td>
												<td>
													<font>Apply As</font><font class="mandatory">&nbsp;*</font>
												</td>
												<td>
													<select id="selApplyAsAncillary" style="width:80%">
													</select>
													
												</td>
												<td></td>
											</tr>
												<tr>
											<td colspan="6">
												<label>Credit Constraints</label>
												<table border="0" width="98%">
													<tr>
														<td width="3%">&nbsp;</td>
														<td width="14%"><input id="fsconstPaxName" type="checkbox" value="Y" name="fsconstPaxName"> <font>Pax name</font></td>
														<td width="3%">&nbsp;</td>
														<td width="14%"><input id="fsconstSector" type="checkbox" value="Y" name="fsconstSector"> <font>Sector</font></td>
														<td colspan="5"></td>
													</tr>
													<tr>
														<td colspan="4" ><font>Flight Period</font></td>
														<td width="3%">&nbsp;</td>
														<td colspan="4"><font>Booking Period </font></td>
													</tr>
												
													<tr>
														<td width="3%"><font> From</font></td>
														<td width="14%"><input type="text" id="fsconstFlightPeriodFrom" style="width:70%" maxlength="10"  readonly="readonly"/></td>
														<td width="3%"><font>To</font></td>
														<td width="14%"><input type="text" id="fsconstFlightPeriodTo"  style="width:70%" maxlength="10"  readonly="readonly"/></td>
														<td width="10%">&nbsp;</td>
														<td width="3%"><font> From</font></td>
														<td width="14%"><input type="text" id="fsconstBookingPeriodFrom" style="width:70%" maxlength="10"  readonly="readonly"/>
														</td>
														<td width="3%"><font>To</font></td>
														<td  width="14%">	<input type="text" id="fsconstBookingPeriodTo"  style="width:70%" maxlength="10"  readonly="readonly"/></td>
													</tr>
												</table>
											</td>
											</tr>
											<tr><td style="height:5px;" colspan="5"></td></tr>
										</table>
									</fieldset>
									<fieldset style="border:1px solid #999;margin:5px;display:none" id="f_Buy And Get Free">
										<legend><label >Buy and Get Free</label></legend>
										<table width="80%" align="center"><tr><td width="60%">
											<table width="100%" align="center" border="0" cellpadding="2" cellspacing="0">
												<tr>
													<td  width="22%"><br /><font>Pax Count</font></td>
													<td  width="22%">
														<font>Adult</font><br/>
														<input type="text" id="adultPCount" class="rightAlign portSel">
													</td>
													<td  width="22%">
														<font>Child</font><br/>
														<input type="text" id="childPCount" class="rightAlign portSel">
													</td>
													<td  width="22%">
														<font>Infant</font><br/>
														<input type="text" id="infantPCount" class="rightAlign portSel">
													</td>
													<td width="12%">
														
													</td>
												</tr>
												<tr>
													<td valign="top" width="20%"><font>Free Count</font></td>
													<td >
														<input type="text" id="adultFCount" class="rightAlign portSel">
													</td>
													<td >
														<input type="text" id="childFCount" class="rightAlign portSel">
													</td>
													<td >
														<input type="text" id="infantFCount" class="rightAlign portSel">
													</td>
													<td>
														<input type="button" id="add_getFree" value = " + " class="iconBtn addItem"/>
													</td>
												</tr>
												<tr>
													<td colspan="4">
														<table width="100%" border="0" cellpadding="2" cellspacing="0" style="border:1px solid #AAA;">
															<thead style="background: #898989;">
																<tr>
																	<td width="50%" class="paddingTD"><label >Pax Count to Buy (AD,CH,INF)</label><font class="mandatory">&nbsp;*</font></td>
																	<td width="50%" class="paddingTD"><label >Free Pax Count (AD,CH,INF)</label></td>
																</tr>
															</thead>
															<tbody  >
																<tr><td colspan="2">
																	<div style="height:50px;overflow-y:auto;">
																	<table  width="100%" border="0" cellpadding="2" cellspacing="0" >
																		<thead>
																			<tr>
																				<td width="50%" style="height:1px;"></td>
																				<td width="50%" style="height:1px;"></td>
																			</tr>
																		</thead>
																		<tbody id="buyNGetBody"></tbody>
																	</table>
																	</div>
																</td></tr>
															</tbody>
														</table>
													</td>
													<td width="20%">
														<input type="button" id="del_getFree" value=" - " class="iconBtn removeItem"/>
													</td>
												</tr>
											</table></td>
											<td valign="top"><br/>
												<table width="100%" align="center">
												<tr>
													<td width="40%"><font>Limiting load factor</font><font class="mandatory">&nbsp;*</font></td>
													<td ><input type="text" id="limitedLoadFactor" class="rightAlign portSel" onKeyPress="KPValidateDecimelDup(this,10,2)" onKeyUp="KPValidateDecimelDup(this,10,2)"></td>
												</tr>
												<tr>
													<td><font>Apply to</font><font class="mandatory">&nbsp;*</font></td>
													<td>
														<select id="selApplyToDiscountForBuyAndGetFree" style="width:80%">
														</select>
														
													</td>
												</tr>
												</table>
											</td>
											</tr></table>
									</fieldset></fieldset>
								</td>
								
							</tr>
							<tr><td colspan="10" style="height:15px;"></td></tr>
							<tr>
								<td colspan="5" valign="bottom"><font>Eligibility</font><font class="mandatory">&nbsp;*&nbsp;</font><br/>
									<input id="applOneway" value="Y" name="applDeparture" type="checkbox"><label>One Way</label>&ensp;
									<input id="applReturn" value="Y" name="applArrival" type="checkbox"><label>Return</label>&ensp;
									<input id="appliciOutBound" value="IB" name="appliciOutBound" type="checkbox"><label>OutBound</label>&ensp;
									<input id="appliciInBound" value="OB" name="appliciInBound" type="checkbox"><label>InBound</label>	
								</td>
							</tr>
							<tr><td colspan="10" style="height:15px;"></td></tr>
							<tr>
								<td valign="top" colspan="5" style="padding-right:15px">
									<fieldset style="border: 1px solid rgb(153, 153, 153);">
										<legend><label>Registration Validity Period</label></legend>
										<table>
											<tr>
												<td valign="top" colspan="2"><font>Start</font>&ensp;
													<input type="text" id="registrationFrom" style="width:60%"  maxlength="10" readonly="readonly"/>
												</td>
												<td valign="top" colspan="2"><font>End</font>&ensp;
													<input type="text" id="registrationTo" style="width:60%" maxlength="10"  readonly="readonly"/>
												</td>
												<td valign="top"><input id="registrationDateAdd" type="button" value="Add" class="btnDefault button"></td>
											</tr>
											<tr><td valign="top" colspan="5" style="height:10px;"></td></tr>
											<tr>
												<td valign="top" colspan="5" style="margin:10px">
													<div id="multipleRegisterDateGrid">
														<table id="multipleRegisterDateTable">			
														</table>
													</div>
												</td>
											</tr>
											<tr>
												<td valign="bottom" colspan="2"></td>
												<td align="right" colspan="3" valign="bottom">
													<input id="registrationDateClear" type="button" value="Clear" class="btnDefault button">
													<input id="registrationDateDel" type="button" value="Delete" class="btnDefault button">
												</td>
											</tr>
										</table>
									</fieldset>
								</td>
								<td valign="top" colspan="5" style="padding-left:15px">
									<fieldset style="border: 1px solid rgb(153, 153, 153);">
										<legend><label>Flight Validity Period</label></legend>
										<table>
											<tr>
												<td valign="top" colspan="2"><font>Start</font>&ensp;
													<input type="text" id="flightPeriodFrom" style="width:60%" maxlength="10"  readonly="readonly"/>
												</td>
												<td valign="top" colspan="2"><font>End</font>&ensp;
													<input type="text" id="flightPeriodTo"  style="width:60%" maxlength="10"  readonly="readonly"/>
												</td>
												<td valign="top"><input id="flightDateAdd" type="button" value="Add" class="btnDefault button"></td>
											</tr>
											<tr><td valign="top" colspan="5" style="height:10px;"></td></tr>
											<tr>
												<td valign="top" colspan="5" style="margin:10px">
													<div id="multipleFlightDateGrid">
														<table id="multipleFlightDateTable">			
														</table>
													</div>
												</td>
											</tr>
											<tr>
												<td valign="bottom" colspan="2"></td>
												<td align="right" colspan="3" valign="bottom">
													<input id="flightDateClear" type="button" value="Clear" class="btnDefault button">
													<input id="flightDateDel" type="button" value="Delete" class="btnDefault button">
												</td>
											</tr>
										</table>
									</fieldset>
								</td>
							</tr>
							<tr><td colspan="10" style="height:15px;"></td></tr>
							<tr>

								<td colspan="2" style="padding-right:15px" valign="top">
									<fieldset style="border: 1px solid rgb(153, 153, 153);">
										<legend><label>Channels<font class="mandatory">&nbsp;*</font></label></legend>
										<select size="15" id="selChanel" style="height:90%;width:90%" multiple="multiple"></select>
									</fieldset>
								</td>
										<td colspan="7" style="padding-left:40px" valign="top">
										<fieldset style="border: 1px solid rgb(153, 153, 153);">
										<legend><label>Customize</label></legend>
										<table width="100%">
											<tbody>
												<tr><td colspan="7" style="height:15px;"></td></tr>
												<tr>
													<td valign="top"><font>Agents</font><font class="mandatory">&nbsp;*</font><br/>
														<input id="Agents" style="width:99%" class="ui-autocomplete-input" autocomplete="off" type="text"><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><br/>
														<select size="10" id="sel_Agents" style="height:75px;width:99%" multiple="multiple">
															<option value="">All</option>
														</select>
													</td>
													<td width="25" valign="top"><br/>
														<input type="button" id="add_Agents" value=" + " class="iconBtn addItem"/><br/>
														<input type="button" id="del_Agents" value=" - " class="iconBtn removeItem"/>
													</td>
													<td valign="top"><font>Booking Classes</font><font class="mandatory">&nbsp;*</font><br/>
														<input id="BookinClasses" style="width:99%" class="ui-autocomplete-input" autocomplete="off" type="text"><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><br/>
														<select size="10" id="sel_BookinClasses" style="height:75px;width:99%" multiple="multiple">
															<option value="">All</option>
														</select>
													</td>
													<td width="25" valign="top"><br/>
														<input type="button" id="add_BookinClasses" value=" + " class="iconBtn addItem"/><br/>
														<input type="button" id="del_BookinClasses" value=" - " class="iconBtn removeItem"/>
													</td>
													<td valign="top"><font>Flights</font><font class="mandatory">&nbsp;*</font><br/>
														<input id="FlightNo" style="width:99%" class="ui-autocomplete-input" autocomplete="off" type="text"><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><br>
														<select size="10" id="sel_FlightNo" style="height:75px;width:99%" multiple="multiple">
															<option value="">All</option>
														</select>
													</td>
													<td width="25" valign="top"><br/>
														<input type="button" id="add_FlightNo" value=" + " class="iconBtn addItem"/><br/>
														<input type="button" id="del_FlightNo" value=" - " class="iconBtn removeItem"/>
													</td>
												</tr>
												<tr><td colspan="7" style="height:15px;"></td></tr>
												<tr>
													<td><font>COS</font><font class="mandatory">&nbsp;*</font><br/>
														<input id="COS" style="width:99%" class="ui-autocomplete-input" autocomplete="off" type="text"><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><br>
														<select size="10" id="sel_COS" style="height:75px;width:99%" multiple="multiple">
															<option value="">All</option>
														</select>
													</td>
													<td width="25" valign="top"><br/>
														<input type="button" id="add_COS" value=" + " class="iconBtn addItem"/><br/>
														<input type="button" id="del_COS" value=" - " class="iconBtn removeItem"/>
													</td>
													<td><font>LCC</font><font class="mandatory">&nbsp;*</font><br/>
														<input id="LCC" style="width:99%" class="ui-autocomplete-input" autocomplete="off" type="text"><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span><br>
														<select size="10" id="sel_LCC" style="height:75px;width:99%" multiple="multiple">
															<option value="">All</option>
														</select>
													</td>
													<td width="25" valign="top"><br/>
														<input type="button" id="add_LCC" value=" + " class="iconBtn addItem"/><br/>
														<input type="button" id="del_LCC" value=" - " class="iconBtn removeItem"/>
													</td>
												</tr>
												<tr><td colspan="7" style="height:15px;"></td></tr>
												<tr>
													<td id="applicability" style="display:none" colspan="7" valign="bottom"><font>Applicability</font><font class="mandatory">&nbsp;*</font><br/><br/>
														<input id="applLoyaltyMember" value="Y" name="applLoyaltyMember" type="checkbox"><label>Loyalty Members</label>&emsp;
														<input id="applRegtMember" value="Y" name="applRegtMember" type="checkbox"><label>Registered Members</label>&emsp;
														<input id="applGuest" value="Y" name="applGuest" type="checkbox"><label>Guests</label>	
													</td>
												</tr>
												<tr><td colspan="7" style="height:15px;"></td></tr>
												<tr>
												<td id="webPOS" style="display:none" colspan="7" valign="bottom">
													<font>WEB POS</font><font class="mandatory">&nbsp;*</font><br/><br/>
																<div style="height: 182px;overflow-x: hidden;overflow-y: auto">
																	<table width="80%" border="0" cellpadding="2" cellspacing="0">
																		<tr>
																			<td colspan="7">
																				<span id="spnMDropPOS">
																					<select multiple="" style="width: 260px; height: 140px;" class="ListBox" id="lstPOS">
																					</select>
																				</span>
																			</td>											
																		</tr>				
																	</table>
																</div>
												</td>
											</tr>
											<tr><td colspan="7" style="height:5px;"></td></tr>
											</tbody>
										</table>
									</fieldset>
								</td>
							</tr>
							<tr><td colspan="10" style="height:15px;"></td></tr>
							<tr>
								<td colspan="10">
									<fieldset style="border:1px solid #999">
										<legend><label>Limit</label></legend>
										<table style="margin:5px" width="100%" border="0" cellpadding="2" cellspacing="0" align="center" >
											<tr>
												<td valign="top"><font>Per Flight</font>
													<input type="text" id="limitPerFlight" style="width:90%" class="rightAlign"/>
												</td>
												<td width="10%">&nbsp;</td>
										
												<td valign="top"><font>Per agent</font>
												<input type="text" id="numByAgent" style="width:90%" class="rightAlign"/></td>
												<td width="10%"></td>
											
												<td valign="top"><font>Per sales channel</font>
													<input type="text" id="numBySalesChannel" style="width:90%" class="rightAlign"/>
												</td>
												<td width="10%"></td>
											
												<td valign="top"><font>Total</font>
													<input type="text" id="limitTotal" style="width:90%" class="rightAlign"/>
												</td>
												<td width="10%"></td>
											</tr>
										</table>
									</fieldset>
								</td>
							</tr>
							<tr><td colspan="10" style="height:15px;"></td></tr>
							
							<tr>			
								<td colspan="10">
								<fieldset style="border:1px solid #999; width:100%">
									<legend><label>Route &amp; Destinations<font class="mandatory">&nbsp;*</font></label></legend>
									<table width="100%">
									<tbody><tr>
										<td valign="top">
											<table width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody><tr>
													<td colspan="9" width="40%" valign="top">Filter OND's<br> 
														<br><div class="ui-widget">
															<input id="txtOndSearch" style="width: 100%" type="text">
														</div>
														<br>
													</td>
													</tr>
													<br><tr>
												
													<td width="15%">Results<br> <br> <select size="10" id="sel_GeneratedRoutes" multiple="multiple" style="height: 150px; width: 90%">
													</select>
													</td>
													<td width="10%" valign="middle">
														<table width="100%">
														    <tbody><tr>
																<td id="addAllOndCodes" width="10%">
																	<div class="Button" style="width: 100px; height: 20px">&gt;&gt;</div>
																</td>
															</tr>
															<tr>
																<td id="addOndCodes" width="10%">
																	<div class="Button" style="width: 100px; height: 20px">&gt;</div>
																</td>
															</tr>
															<tr>
																<td></td>
															</tr>
															<tr>
																<td id="removeOndCodes" width="10%">
																	<div class="Button" style="width: 100px; height: 20px">&lt;</div>
																</td>
															</tr>
															<tr>
																<td id="removeAllOndCodes" width="10%">
																	<div class="Button" style="width: 100px; height: 20px">&lt;&lt;</div>
																</td>
															</tr>
														</tbody></table>
													</td><td width="50%">OND Codes List<br> <br> <select size="10" id="sel_Routes" multiple="multiple" style="height: 150px; width: 100%"><option value="">CMB/SHJ</option></select>
													</td>
													
												</tr>
												<tr>
													<td colspan="2" style="height: 5px;" valign="top"></td>
												</tr>
											</tbody></table>
										</td>
									</tr>
									</tbody></table>
												</fieldset>
											</td>
								
							</tr>
											
							<tr>
								<td colspan="5" style="height: 30px;" valign="bottom"></td>
									<td colspan="5" align="right" valign="bottom">
									<input style="margin:5px" name="btnReset" type="button" id="btnReset" value="Reset" class="btnDefault button"  style="width:auto">
									<input style="margin:5px" name="btnSave" type="button" id="btnSave" value="Save" class="btnDefault button" style="width:auto">
									<u:hasPrivilege privilegeId="sys.mas.mgmt.promo.campaign">
										<input style="margin:5px" name="btnSend" type="button" id="btnSend" value="Send" class="btnDefault button" style="width:auto">
									</u:hasPrivilege>
								</td>
							</tr>
					</table>
					
				</div>
				
				
			</div>
			
			<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
				<tr>
					<td colspan="3" style="height: 42px;" valign="bottom"><input name="btnClose" type="button" id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault button">
					</td>
					
				</tr>
			</table>
		</form>
	</div>
	<!-- multiple promocodes popup for search & edit ui -->
	<div id="viewMultiPromoCodePopup" style="display: none;">
		<table>
			<tr>
				<td>Following are the Multi Generated Promo Codes. </td>				
			</tr>
			<tr><td height="5px"></td></tr>
			<tr>
				<td><table id="codeDetails"></table></td>
			</tr>
			<tr></tr>
			<tr>
				<td></td>
				<td align="right">
				<input id="closeBtn" type="button" value="Close" style = "width:auto" class="btnDefault Button"/></td>
			</tr>
		</table>
	</div>
	
	<div id="multiPromoCodeDialog" style="display: none;">
		<table>
			<tr>
				<td>Following are the system generated Promo Codes. </td>				
			</tr>
			</br>
			<tr></tr>
			<tr>
				<td><table id="codeSelection"></table></td>
			</tr>
			<tr></tr>
			<tr>
				<td></td>
				<td align="right">
				<input id="btnEnableCodes" type="button" value="Close" style = "width:auto" class="btnDefault Button"/></td>
			</tr>
		</table>
	</div>
	<div class="PageBottomColor" style="margin-top:10px">
		<table width="100%" cellspacing="1" cellpadding="0" border="0" id="Table6">
			<tbody><tr>
				<td width="20">&nbsp;</td>
				<td width="20" valign="bottom"><img id="imgError" border="0" src="../../images/Err_no_cache.gif" name="imgError" style="visibility: hidden; display: none;" /></td>
				<td width="1"><img src="../../images/StatusBar_no_cache.jpg" /></td>
				<td id="tdSTBar" class="StatsBarDef"><span id="spnDate">&nbsp;</span></td>
				<td width="1"><img src="../../images/StatusBar_no_cache.jpg" /></td>
				<td width="150" align="center"><span id="spnUID"></span></td>
				<td width="1"><img src="../../images/StatusBar_no_cache.jpg" /></td>
				<td width="200" align="center"><span id="spnScreenID"><font class="fontPGInfo"></font></span></td>
				<td width="20">&nbsp;</td>
			</tr>
		</tbody></table>
	</div>
<!--  	<div id='divLoadMsg'
		style='visibility: hidden; z-index: 1000; background: transparent;'>
		<iframe src="LoadMsg" id='frmLoadMsg' name='frmLoadMsg'
		frameborder='0' scrolling='no'
		style='position: absolute; top: 50%; left: 40%; width: 260; height: 40;'></iframe>
	</div>-->
	<script type="text/javascript" src="../../js/promotion/promoCodes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	
	<script>
		var screenId = 'SC_PROM_04';		
		top[2].HideProgress();
	</script>
</body>
</html>