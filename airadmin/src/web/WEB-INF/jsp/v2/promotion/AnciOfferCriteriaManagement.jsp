<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Create Promotion Templates</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>
<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>
<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/jquery.ui.autocomplete.css"/>


<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery.json.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript">	
	var anciTemplateTypes=$.parseJSON('<c:out value="${requestScope.reqAnciOfferTypes}" escapeXml="false" />');
</script>
<style>
	.innerPanel{
		width:98%;
		margin:0 auto;
	}
	.portSel{
		width:50px
	}
	select{
		margin:1px 1px;
	}
	.iconBtn{
		width:25px
	}
	#ui-datepicker-div{
		display:none;
	}
	.ui-autocomplete { height: 200px; overflow-y: scroll; overflow-x: hidden;}
	.multipleSelect { height:100px; width:150px }
	
</style>
</head>
<body class="tabBGColor" scroll="no" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
<script type="text/javascript">			
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	var bcJSONList=<c:out value="${requestScope.reqBCList}" escapeXml="false" />
</script>
			
	<div id="divAnciOfferSearch" style="text-align: left;background-color: #ECECEC;" >
		<div class="innerPanel divsearch" style="border-bottom:1px solid #bbb">
		<table width="96%" border="0" cellpadding="0" cellspacing="2"  style="width:97%;margin:0 auto">
			<tr>
				<td width="15%"><font>From</font></td>
				<td width="15%">
					<select name="selFrom" id="selFrom" style="width: 80px;">
					<option value="">All</option>
					<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
					</select>
				</td>
				<td width="15%"><font>To</font></td>
				<td width="15%">
					<select name="selTo"  id="selTo" style="width: 80px;">
					<option value="">All</option>
					<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
					</select>
				</td>
				<td width="15%"><font>Status</font></td>
				<td width="15%">
					<select name="selStatus"  id="selStatus" style="width: 80px;">
					<option value="">All</option>
					<option value="Y">Active</option>
					<option value="N">Inactive</option>
					</select>
				</td>
				<td width="10%">&nbsp;</td>
			</tr>
		
			<tr>
				<td><font>Start Date</font></td>
				<td><input type="text" name="txtStartDateSearch" id="txtStartDateSearch" value="" style="width: 80px;" maxlength="10"  readonly="readonly"></td>
				<td><font>End Date</font></td>
				<td><input type="text" name="txtStopDateSearch" id="txtStopDateSearch" value="" style="width: 80px;" maxlength="10"  readonly="readonly"> </td>
				<td><font>Offer Type</font></td>
				<td>
					<select name="selOfferType"  id="selOfferType" style="width: 80px;">
						<option value="">All</option>
						<option value="BAGGAGE">Baggage</option>
						<option value="MEAL">Meal</option>
						<option value="SEAT">Seat</option>
						<option value="INSURANCE">Insurance</option>
					</select>
				</td>
				<td align="right"><input name="btnSearch" type="button" id="btnSearch" value="Search" class="Button">
				</td>
			</tr>
			
		</table>
		</div>
		<div class="innerPanel divResults" style="border-top:1px solid #fff">
			<div id="jqGridAnciOfferContainer" style="margin-top: 5px;">
				<table width="100%" border="0" cellpadding="0" cellspacing="0" id="jqGridAnciOfferData">
				</table>
				<div id="jqGridAnciOfferPages"></div>
			</div>
			<input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault Button">
			<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault Button">
			<input type="button" id="btnClose" value="Close" class="btnDefault Button"/>
		</div>
	
	</div>

	<div id="anciOfferDetails" style="text-align: left; background-color: #ECECEC;">
		<table class="innerPanel">
			<tr>
				<td width="15%">Anci Offer Type :</td>
				<td width="18%">
					<select id="selTemplateType"></select>
				</td>
				<td width="15%">Template ID :</td>
				<td width="18%">
					<select id="selTemplateID"></select>
				</td>
				<td width="15%">Eligibility :<font class="mandatory">&nbsp;*&nbsp;</font></td>
				<td width="18%">One way<input id="eligibleOneway" type="checkbox" value="Y" name="applDeparture"/>&nbsp;&nbsp;
					Return<input id="eligibleReturn" type="checkbox" value="Y" name="applArrival"/>
				</td>
			</tr>
			<tr>
				<td>Offer Name : <font class="mandatory">&nbsp;*&nbsp;</font></td>
				<td>
					<input id="txtName" type="text"/>
				</td>
				<td> <label>Eligible From :</label> <font class="mandatory">&nbsp;*&nbsp;</font></td>
				<td> <input type="text" id="applicableFrom"></input> </td>
				<td> <label>Eligible To :</label> <font class="mandatory">&nbsp;*&nbsp;</font></td>
				<td> <input type="text" id="applicableTo"></input> </td>
			</tr>		
			<tr>
				<td>Offer Description : <font class="mandatory">&nbsp;*&nbsp;</font></td>
				<td colspan="5">
					<textarea id="txtDescription" style="width:95%" class="htmlsup"></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<table width="97%">
						<tr>
							<td> <label>Origin</label> </td>
							<td> 
								<select id="selOrigin">
								<option value="ALL">ALL</option>
								<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
								</select>
							 </td>
							<td> <label>Destination</label> </td>
							<td>
								<select id="selDestination">
									<option value="ALL">ALL</option>
									<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
								</select> 
								<font class="mandatory">&nbsp;*&nbsp;</font>
							</td>
							<td>
								<input id="btnAddOND" type="button" value= " + " class="iconBtn addItem"></input>
							</td>
						</tr>
						<tr>
							<td>
								<label>Via 1</label><br>
								<select class="portSel" id="selVia1">
									<option value=""></option>
									<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
								</select>
							</td>
							<td>
								<label>Via 2</label><br>
								<select class="portSel" id="selVia2">
									<option value=""></option>
									<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
								</select>
							</td>
							<td>
								<label>Via 3</label><br>
								<select class="portSel" id="selVia3">
									<option value=""></option>
									<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
								</select>
							</td>
							<td>
								<label>Via 4</label><br>
								<select class="portSel" id="selVia4">
									<option value=""></option>
									<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="4" > 
								<select id="selONDs" size="10" multiple="multiple" class="multipleSelect" style="width: 100%"></select> 
							</td>
							<td valign="top">
								<input id="btnRemoveOND" type="button" value= " - " class="iconBtn removeItem"></input>
							</td>							
						</tr>
					</table>
					</td><td colspan="2" valign="bottom">
					<table width="95%" >
						<tr>
							<td valign="top" width="95%"> 
								<label>Eligible Days Of The Week</label> <font class="mandatory">&nbsp;*&nbsp;</font>
							</td>
							<td></td>
						</tr>
						<tr>
							<td>
								<label>Outbound : </label>
							</td>
							<td>
								<label>Inbound : </label>
							</td>
						</tr>
						<tr>
							<td width="47.5%">
								<select id="selApplicableDaysOutBound" size="10" multiple="multiple" class="multipleSelect">
									<!-- These values correspond to the DAY_OF_THE_WEEK values for each day defined in java.util.Calender -->
									<option value="1">Sunday</option>
									<option value="2">Monday</option>
									<option value="3">Tuesday</option>
									<option value="4">Wednsday</option>
									<option value="5">Thursday</option>
									<option value="6">Friday</option>
									<option value="7">Saturday</option>
								</select>
							</td>
							<td width="47.5%">
								<select id="selApplicableDaysInBound" size="10" multiple="multiple" class="multipleSelect">
									<!-- These values correspond to the DAY_OF_THE_WEEK values for each day defined in java.util.Calender -->
									<option value="1">Sunday</option>
									<option value="2">Monday</option>
									<option value="3">Tuesday</option>
									<option value="4">Wednsday</option>
									<option value="5">Thursday</option>
									<option value="6">Friday</option>
									<option value="7">Saturday</option>
								</select>
							</td>
						</tr>
					</table>
					</td><td colspan="2" valign="bottom">
					<table width="95%">
						<tr>
							<td>
								 <label>Adult</label>
							</td>
							<td>
								<input type="text" id="adultPCount" size="3"/>
							</td>
							<td>
								 <label>Child</label>
							</td>
							<td>
								<input type="text" id="childPCount" size="3"/>
							</td>
							<td>
								 <label>Infant</label>
							</td>
							<td>
								<input type="text" id="infantPCount" size="3"/>
								<font class="mandatory">&nbsp;*&nbsp;</font>
							</td>
							<td>
								<input type="button" id="addPaxCount" value = " + " class="iconBtn addItem"/>
							</td>
						</tr>
						<tr>
							<td colspan="6"> 
								<select id="selPaxCount" size="10" multiple="multiple" class="multipleSelect" style="width:100%"></select> 
							</td>
							<td valign="top">
								<input type="button" id="removePaxCount" value = " - " class="iconBtn removeItem"/>
							</td>
						</tr>
					</table>
				</td>
			</tr>

			<tr>
				<td colspan="2">
					<table width="95%"><tr>
						<td valign="top"> <label>Eligible Booking Classes :</label> </td>
						</tr>
						<tr>
							<td><input type="text" size="5" id="txtBCAutoComplete" name="txtBCAutoComplete"></td>
							<td><input type="button" id="addBC" value = " + " class="iconBtn addItem"/></td>
							<td><input type="button" id="removeBC" value = " - " class="iconBtn removeItem"/></td>
						</tr>
						<tr>					
						<td>
							<select id="selBookingClasses" size="10" multiple="multiple" class="multipleSelect" style="width:95%">
								<option value="ALL">ALL</option>
							</select>
						</td>
					</tr></table>
				</td><td colspan="2">
					<table width="95%"><tr>
					<td valign="top"> <label>Eligible LCCs :</label> <font class="mandatory">&nbsp;*&nbsp;</font></td>
					</tr><tr>	
					<td>
						<select id="selLCCs" size="10" multiple="multiple" class="multipleSelect" style="width:95%">
							<c:out value="${requestScope.reqLogicalCabinClassList}" escapeXml="false" />
						</select>
					</td>
					</tr></table>
				</td><td colspan="2">
					<table width="95%">
						<tr>
							<td valign="top"> <label>Apply for flexi fares :</label> </td>
							<td> <input type="checkbox" id="chkApplyForFlexi"/> </td>
						</tr>
						<tr>
							<td>Applicability : <font class="mandatory">&nbsp;*&nbsp;</font></td>
							<td>One way
								<input id="applOneway" type="checkbox" value="Y" name="applDeparture"/>&nbsp;&nbsp;
								Return
								<input id="applReturn" type="checkbox" value="Y" name="applArrival"/>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td><font>Active : </font><td>
				<td>
					<input id="chkActive" type="checkbox" value="Y" name="chkActive">
				<td>
			</tr>
			<tr>
				<td colspan="6"> 
					<input name="btnReset" type="button" id="btnReset" value="Reset" class="btnDefault Button"/> 
					<input name="btnSave" type="button" id="btnSave" value="Save" class="btnDefault Button" /> 
					<input name="btnBack" type="button" id="btnBack" value="Back" class="btnDefault Button">
					<input name="btnSave" type="button" id="btnTranslations" value="Translations" class="btnDefault Button" style="width:90"/>
				</td>
			</tr>
		</table>
	</div>
	<div id="internationalizationDialog" style="display: none;">
		<table>
			<tr>
				<td>Language :</td>
				<td>
					<select id="selLanguage"><c:out value="${requestScope.reqLanguageList}" escapeXml="false" /></select>
				</td>
			</tr>
			<tr>
				<td>Anci Offer Name :</td>
				<td>
					<input id="txtTranslatedName" type="text"/>
				</td>
			</tr>
			<tr>
				<td>Anci Offer Description :</td>
				<td>
					<textarea id="txtTranslatedDesc" cols="50" rows="5" class="htmlsup"></textarea>
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">
					<input id="btnAddTranslations" type="button" value=" + " class="btnDefault" style="width:auto"/>
					<input id="btnRemoveTranslations" type="button" value=" - " class="btnDefault" style="width:auto"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<select id="selTranslations" size="10" multiple="multiple" style="height:150px; width:460px"></select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="right">
				<input id="btnTranslationReset" type="button" value="Reset" class="btnDefault Button" />
				<input id="btnTranslationSubmit" type="button" value="Save" class="btnDefault Button"/></td>
			</tr>
		</table>
	</div>
	<script type="text/javascript" src="../../js/promotion/anciOffers.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script>
		var screenId = 'SC_PROM_05';
		
		top[2].HideProgress();
	</script>
</body>
</html>