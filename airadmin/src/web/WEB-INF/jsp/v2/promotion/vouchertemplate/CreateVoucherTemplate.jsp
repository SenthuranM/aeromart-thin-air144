<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<%@ page import="com.isa.thinair.promotion.api.to.constants.PromoTemplateParam" %>
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;
	Date dStopDate = null;
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date
	cal.add(Calendar.MONTH,1);
	dStopDate = cal.getTime(); //Next Month Date
	String StartDate =  formatter.format(dStartDate);
	String StopDate = formatter.format(dStopDate);
%> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Create Gift Voucher Templates</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/VoucherTemplate/CreateVoucherTemplate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript">	

</script>
</head>
<body class="tabBGColor" scroll="no" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>

<script type="text/javascript">			
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />										
</script>
	<div id="divSearch" style="height: 50px; text-align: left;">
		<table width="80%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
			<tr>
				<td width="20%"><font>Voucher Template Name</font></td>
				<td width="20%">
					<input type="text" id="searchVoucherName"  maxlength="30">
				</td>
				<td width="15%" align="center"><font>Status</font></td>
				<td width="20%"><select name="statusSrch" id="statusSrch">
						<option value="ACT">Active</option>
						<option value="INA">In-Active</option>
				</select></td>				
				<div style="display: none;">
						<select name="voucherNames" size="1" id="voucherNames" style="width: 100px;">
							<option value="-1">All</option>
							<c:out value="${requestScope.voucherTypesList}" escapeXml="false" />
						</select>
				</div>
				<td align="right"><input name="btnSearch" type="button" id="btnSearch" value="Search" onclick="searchVoucherTemplates()" class="button">
				</td>
			</tr>
		</table>
	</div>
	<div id="divResultsPanel" style="height: 240px; text-align: left; background-color: #ECECEC;">
		<div id="jqGridVoucherTemplatesContainer" style="margin-top: 5px;">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" id="jqGridVoucherTemplatesData">
			</table>
			<div id="jqGridVoucherTemplatesPages"></div>
		</div>
		 
		<input name="btnAdd" type="button" id="btnAdd" value="Add" class="btnDefault">
		<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault">
		
	</div>
	<div id="voucherTemplateDetails" style="height: 275px; text-align: left; background-color: #ECECEC;">
		<form method="post" action="voucherTemplatesManagementAction.action" id="frmVoucher">
			
			<!--<div id="vouchersTabs" class="admin-panel ui-border-topDis" class="FormHeadBackGround">
				 <ul class="ui-border-leftDis ui-border-rightDis">
					<li id="tabAddModChargeTop"><a href="#tabVoucherDetails">Gift Voucher Details</a></li>					
				</ul> -->
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="10" height="20">
							<img src="../../images/form_top_left_corner_no_cache.gif">
						</td>
						<td height="20" class="FormHeadBackGround" width="300%" align="left">
							<img src="../../images/bullet_small_no_cache.gif"> 
							<font class="FormHeader">
								Gift Voucher Template Details 				
							</font></td>
						<td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td>
					</tr>
				</table>
				<div id="tabVoucherDetails">
					<div style="display: none;">
						<input type="hidden" id="voucherId" name="voucherTemplateRequest.voucherId" value="">
					</div>
					<table width="96%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
						<tr>
							<td width="50%">
								<div>
									<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table1">
										<tr>
											<td width="25%"><font>Name</font></td>
											<td width="25%">
												<input type="text" id="txtVoucherName" style="width: 90px;" maxlength="30"><font class="mandatory">&nbsp;*</font>												
											</td>
											
										</tr>
											<td width="25%"><font>Amount</font></td>
											<td width="25%">
												<input type="text" id="txtAmount" style="width: 90px;" onkeypress="return isNumberKey(event)"><font class="mandatory">&nbsp;*</font>												
											</td>
											<td width="25%" align="right"><font>Currency</font></td>
											<td width="25%">
												<select name="selectCurrency" size="1" id="selectCurrency" style="width: 90px;" >																								
												</select><font class="mandatory">&nbsp;*</font>
											</td>										
										<tr>
											<td width="25%"><font>Validity Period</font></td>
											<td width="25%">
												<select name="validity" id="validity" size="1" style="width:90;" class="UCase">
													<option value="30">30 Days</option>
													<option value="90">90 Days</option>
													<option value="180">180 Days</option>	
													<option value="365">1 Year</option>			
												</select><font class="mandatory">&nbsp;*</font>
										   </td>
										<tr>
										<tr>
											<td width="25%"><font>Status</font></td>
											<td width="25%">
												<select name="status" id="status" size="1" style="width:90;" class="UCase">
													<option value="ACT">Active</option>
													<option value="INA">In-Active</option>	
												</select><font class="mandatory">&nbsp;*</font>
										   </td>
										<tr>
											<td width="25%"><font>Sales Validity   From</font></td>
											<td width="25%""><input type="text" name="txtStartDateDetail" id="txtStartDateDetail"  value=<%=StopDate %> style="width: 72px;" maxlength="10"><font class="mandatory">&nbsp;*</font></td>
											<td width="25%"" align="right"><font>To</font></td>
											<td width="25%""><input type="text" name="txtEndDateDetail" id="txtEndDateDetail"  value=<%=StopDate %> style="width: 72px;" maxlength="10"><font class="mandatory">&nbsp;*</font></td>
										</tr>											
										<tr>
											<td valign="top">
												<font>Remarks</font>
											</td>
											<td align="left" valign="top" colspan="4">
												<textarea id="txtRemarks" onkeypress="validateTA(this,255);commaValidate(this);spclCharsVali(this);" onkeyup="validateTA(this,255);commaValidate(this);spclCharsVali(this);" maxlength="260" rows="4" cols="47" name="txtRemarks" style="resize:none" disabled=""></textarea>
											</td>
										</tr>
												
									</table>
								</div>
							</td>
							<td width="50%"></td>							
						</tr>
					</table>
				</div>
				
			<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
				<tr>
					<td colspan="3" style="height: 42px;" valign="bottom"><input name="btnClose" type="button" id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" class="button">
					<input name="btnReset" type="button" id="btnReset" value="Reset" onclick="resetClick()" class="button"></td>
					<td align="right" valign="bottom">
						<input name="btnSave" type="button" id="btnSave" value="Save" class="button" onclick="submitVoucherTemplate()">
						<input name="btnUpdate" type="button" id="btnUpdate" value="Update" class="button"></td>
				</tr>
			</table>
		</form>
	</div>
	<script>
		var screenId = "SC_PROM_08";		
		top[2].HideProgress();
	</script>
</body>
</html>