<div id="tabs" class="admin-panel ui-border-topDis" style="display:none">
		<ul class="ui-border-leftDis ui-border-rightDis">
			<li id="tabFareRuleTop"><a href="#tabFareRule">Fare Rule</a></li>
			<li id="tabValidityDataTop"><a href="#tabValidityData">Validity Data</a></li>
			<li id="tabChargesTop"><a href="#tabCharges">Charges</a></li>
			<li id="tabLinkAgentTop"><a href="#tabLinkAgent">Link Agent</a></li>
			<li id="tabLinkPOSTop"><a href="#tabLinkPOS">Link Country/POS</a></li>
			<li id="tabFareRuleCommentsTop"><a href="#tabFareRuleComments">Comments and Rules</a></li>
		</ul>
		<div id="tabFareRule" style="height: 210px;overflow-y: auto;overflow-x: hidden">
			<table width="96%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="2">
						<input type="hidden" id="hdnRuleCode" name="hdnRuleCode"/>
						<input type="hidden" id="hdnRuleId" name="hdnRuleId"/>
 						<input type="hidden" id="hdnVersion" name="hdnVersion"/>
 						<!-- <input type="hidden" id="radCurrFareRule" name="radCurrFareRule" value="BASE_CURR"/> -->
 						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr id="fareRuleOnly">
								<td width="13%" align="left"><font>Fare Rule Code</font></td>
								<td width="14%" align="left">
									<input type="text" id="ruleCode" name="ruleCode" maxLength="7" size="7" onkeyup="alphaNumericValidate(this)" onkeypress="alphaNumericValidate(this)" />
									<font class="mandatory"><b>*</b></font></td>
								<td width="10%"><font>Description</font></td>
								<td width="21%">
									<input type="text" id="description" name="description" maxlength="30" size="25"	title="Fare Rule Description" onkeyup="alphaNumericValidate(this)" onkeypress="alphaNumericValidate(this)" />
									<font class="mandatory"><b>*</b></font>
								</td>
								<td align="left" width="12%"><font>Basis Code</font></td>
								<td width="17%" align="left"><font><input type="text" id="fareBasisCode" name="fareBasisCode" size=20 maxlength="15"/></font></td>
								<td width="10%" align="left"><font>Active</font></td>
								<td width="7%" align="left"><input type="checkbox" id="chkStatusActive" name="chkStatusActive" class="NoBorder"/></td>
							</tr>
							<tr>
								<td width="12%" align="left" style="height:1px"></td>
								<td width="14%" align="left">
								</td>
								<td width="10%"></td>
								<td width="21%">
								</td>
								<td align="left" width="12%"></td>
								<td width="12%" align="left"></td>
								<td width="12%" align="left"></td>
								<td width="10%" align="left"></td>
							</tr>
							<tr>
								<td align="left"><font>Adv.Bkg Days:Hrs:Mins</font></td>
								<td  align="left">
									<table width="50%" cellpadding="0" cellspacing="0" border="0"><tr>
										<td><input type="checkbox" id="chkAdvance" name="chkAdvance" class="NoBorder" value="ON"/></td>
										<td><input type="text" id="advanceBookingDays" name="advanceBookingDays" maxlength="3" size="3" class="rightText numaricApply" /></td>
										<td><input type="text" id="advanceBookingHrs" name="advanceBookingHrs" maxlength="3" size="3" class="rightText numaricApply" /></td>
										<td><input type="text" id="advanceBookingMins" name="advanceBookingMins" maxlength="3" size="3" class="rightText numaricApply" /></td>
									</tr></table>
								</td>
								<td  align="left" valign="top" >
									<font>Fare Visibility</font><font class="mandatory">*</font>
								</td>
								<td valign="top" rowspan="2">
										<select id="visibility" size="4" name="visibility" style="width: 100; height: 70" multiple="multiple" >											
										</select>
								</td>
								<td colspan="4" rowspan="5" valign="top">
								<fieldset style="border:1px solid #666; padding: 3px 6px"> 
									<legend><font>Group Applicability</font></legend>
									<table width="100%" cellpadding="2" cellspacing="1" border="0" class="groupClass">
									<tr>
										<td width="43%"><font></font></td>
										<td width="19%"><font>Adult</font></td>
										<td width="19%"><font>Child</font></td>
										<td width="19%"><font>Infant</font></td>
									</tr>
									<tr>
										<td width="43%" align="left"><font>Fare Applicable</font></td>
										<td width="19%"><input type="checkbox" name="chkADApp" id="chkADApp" disabled="disabled"/></td>
										<td width="19%"><input type="checkbox" name="chkCHApp" id="chkCHApp" disabled="disabled"/></td>
										<td width="19%"><input type="checkbox" name="chkINApp" id="chkINApp" disabled="disabled"/></td>
									</tr>
									<tr>
										<td width="43%" align="left"><font>Fare Refundable</font></td>
										<td width="19%"><input type="checkbox" name="chkADRef" id="chkADRef" disabled="disabled"/></td>
										<td width="19%"><input type="checkbox" name="chkCHRef" id="chkCHRef" disabled="disabled"/></td>
										<td width="19%"><input type="checkbox" name="chkINRef" id="chkINRef" disabled="disabled"/></td>
									</tr>
									<tr>
										<td width="43%" align="left"><font>Modification Applicable</font></td>
										<td width="19%"><input type="checkbox" name="chkADMod" id="chkADMod" disabled="disabled"/></td>
										<td width="19%"><input type="checkbox" name="chkCHMod" id="chkCHMod" disabled="disabled"/></td>
										<td width="19%"><input type="checkbox" name="chkINMod" id="chkINMod" disabled="disabled"/></td>
									</tr>
									<tr>
										<td width="43%" align="left"><font>Cancellation Applicable</font></td>
										<td width="19%"><input type="checkbox" name="chkADCNX" id="chkADCNX" disabled="disabled"/></td>
										<td width="19%"><input type="checkbox" name="chkCHCNX" id="chkCHCNX" disabled="disabled"/></td>
										<td width="19%"><input type="checkbox" name="chkINCNX" id="chkINCNX" disabled="disabled"/></td>
									</tr>																	
									</table>
									</fieldset>
								</td>
							</tr>
							<tr>
								<td  align="left" colspan="3">
									<fieldset style="border:1px solid #666; padding: 0px 6px;width: 90%"> 
										<legend><font>Journey Type</font></legend>
										<table width="100%" cellpadding="0" cellspacing="2" border="0" class="groupClass"><tr>
											<td width="6%"><input type="radio" id="radOneWay" name="radFare" value="oneway" class="NoBorder" checked="checked"/></td><td><font>One way</font></td>
											<td width="6%"><input type="radio" id="radReturn" name="radFare" value="return" class="NoBorder" /></td><td><font> Return</font></td>
											<td width="6%" class="half-return-sup"><input type="checkbox" id="radHalfReturn" name="radFare" value="return" class="NoBorder" disabled="disabled"/></td><td class="half-return-sup"><font> Half Return</font></td>
											<td width="6%" class="open-return-sup"><input type="checkbox" id="radOpReturn" name="radFare" value="oReturn" class="NoBorder" disabled="disabled"/></td><td class="open-return-sup"><font> Open RT</font></td>
										</tr></table>
									</fieldset>
								</td>
								<td></td>
							</tr>
							<tr>
								<td  align="left" colspan="4" style="padding-bottom:5px">
									<fieldset style="border:1px solid #666; padding: 0px 6px;width: 90%"> 
										<legend><font>Bulk Ticket</font></legend>
										<table width="100%" cellpadding="0" cellspacing="2" border="0" class="groupClass">
											<tr>
												<td class="bulk-ticket" width="25%"><font>Bulk Ticket Rule</font></td>
												<td class="bulk-ticket" width="10%"><input type="checkbox" id="chkBulkTicket" name="chkBulkTicket" class="NoBorder"></td>
												<td class="bulk-ticket" width="35%"><font>Min Requested Seats</font></td>
												<td class="bulk-ticket" width="30%"><input type="text" id="minBTSeatCount" name="minBTSeatCount" maxlength="3" ><font class="mandatory"><b>&nbsp;*</b></font></td>
											</tr>
										</table>
									</fieldset>
								</td>
							</tr>
							<tr>
								<td align="left" valign="top"><font>Min Stay Over</font></td>
								<td align="left" valign="top" colspan="2">
									<input type="text" id="minStayOver_MM" name="minStayOver_MM"  maxlength="3" style="width:30px" class="rightText numaricApply" onkeyup="KPValidatePositiveInteger(this,'MON')"/>
									<input type="text" id="minStayOver_DD" name="minStayOver_DD"  maxlength="3" style="width:30px" class="rightText numaricApply" onkeyup="KPValidatePositiveInteger(this,'DAY')"/>
									<input type="text" id="minStayOver_HR" name="minStayOver_HR"  maxlength="3" style="width:30px" class="rightText numaricApply" onkeyup="KPValidatePositiveInteger(this,'HRS')"/>
									<input type="text" id="minStayOver_MI" name="minStayOver_MI"  maxlength="3" style="width:30px" class="rightText numaricApply" onkeyup="KPValidatePositiveInteger(this,'MIN')"/>
								</td>
								<td><font>(MM : DD : HH : MI)</font></td>
							</tr>
							
							<tr>
								<td align="left" valign="top"><font>Max Stay Over</font></td>
								<td align="left" valign="top" colspan="2">
									<input type="text" id="maxStayOver_MM" name="maxStayOver_MM"  maxlength="3" style="width:30px" class="rightText numaricApply" onkeyup="KPValidatePositiveInteger(this,'MON')"/>
									<input type="text" id="maxStayOver_DD" name="maxStayOver_DD"  maxlength="3" style="width:30px" class="rightText numaricApply" onkeyup="KPValidatePositiveInteger(this,'DAY')"/>
									<input type="text" id="maxStayOver_HR" name="maxStayOver_HR"  maxlength="3" style="width:30px" class="rightText numaricApply" onkeyup="KPValidatePositiveInteger(this,'HRS')"/>
									<input type="text" id="maxStayOver_MI" name="maxStayOver_MI"  maxlength="3" style="width:30px" class="rightText numaricApply" onkeyup="KPValidatePositiveInteger(this,'MIN')"/>
								</td>
								<td><font>(MM : DD : HH : MI)</font></td>
							</tr>
							
							<tr class="open-return-sup">
								<td align="left" valign="top"><font>Open RT. Confirm</font></td>
								<td align="left" valign="top" colspan="2">
									<input type="text" id="ortConfirm_MM" name="ortConfirm_MM"  maxlength="3" style="width:30px" class="rightText numaricApply" onkeyup="KPValidatePositiveInteger(this,'MON')" />
									<input type="text" id="ortConfirm_DD" name="ortConfirm_DD"  maxlength="3" style="width:30px" class="rightText numaricApply" onkeyup="KPValidatePositiveInteger(this,'DAY')"/>
									<input type="text" id="ortConfirm_HR" name="ortConfirm_HR"  maxlength="3" style="width:30px" class="rightText numaricApply" onkeyup="KPValidatePositiveInteger(this,'HRS')"/>
									<input type="text" id="ortConfirm_MI" name="ortConfirm_MI"  maxlength="3" style="width:30px" class="rightText numaricApply" onkeyup="KPValidatePositiveInteger(this,'MIN')"/>
								</td>
								<td><font>(MM : DD : HH : MI)</font></td>
							</tr>
							
							<tr>
								<td valign="top"><font>Valid Days of week </font></td>
								<td align="left" valign="top" colspan="8">
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr id="outBoundType">
											<td width="60%"><font>Out Bound&nbsp;&nbsp;&nbsp;All</font>&nbsp;
												<input type="checkbox" id="chkOutAll" name="chkOutAll" class="NoBorder"/> <font>Mo</font>&nbsp;
												<input type="checkbox" id="chkOutMon" name="chkOutMon" class="NoBorder" /> <font>Tu</font>&nbsp;
												<input type="checkbox" id="chkOutTues" name="chkOutTues" class="NoBorder" /> <font>We</font>&nbsp;
												<input type="checkbox" id="chkOutWed" name="chkOutWed" class="NoBorder" /> <font>Th</font>&nbsp;
												<input type="checkbox" id="chkOutThu" name="chkOutThu" class="NoBorder" /> <font>Fr</font>&nbsp;
												<input type="checkbox" id="chkOutFri" name="chkOutFri" class="NoBorder" /> <font>Sa</font>&nbsp;
												<input type="checkbox" id="chkOutSat" name="chkOutSat" class="NoBorder" /> <font>Su</font>&nbsp;
												<input type="checkbox" id="chkOutSun" name="chkOutSun" class="NoBorder" />
											</td>
											<td><font>From</font></td>
											<td width="8%"><input type="text" id="outBoundFrom" name="outBoundFrom" size="5" maxlength="5"/></td>
											<td><font>To</font></td>
											<td width="8%"><input type="text" id="outBoundTo" name="outBoundTo" size="5" maxlength="5"/></td>
											<td width="14%"><font>(Time)</font></td>	
										</tr>
										<tr id="inBoundType">
											<td width="60%"><font>In Bound&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All</font>&nbsp;
												<input type="checkbox" id="chkInAll" name="chkInAll" class="NoBorder" /> <font>Mo</font>&nbsp;
												<input type="checkbox" id="chkInbMon" name="chkInbMon" class="NoBorder" /> <font>Tu</font>&nbsp;
												<input type="checkbox" id="chkInbTues" name="chkInbTues" class="NoBorder" /> <font>We</font>&nbsp;
												<input type="checkbox" id="chkInbWed" name="chkInbWed" class="NoBorder" /> <font>Th</font>&nbsp;
												<input type="checkbox" id="chkInbThu" name="chkInbThu" class="NoBorder" /> <font>Fr</font>&nbsp;
												<input type="checkbox" id="chkInbFri" name="chkInbFri" class="NoBorder" /> <font>Sa</font>&nbsp;
												<input type="checkbox" id="chkInbSat" name="chkInbSat" class="NoBorder" /> <font>Su</font>&nbsp;
												<input type="checkbox" id="chkInbSun" name="chkInbSun" class="NoBorder" />
											</td>
											<td><font>From</font></td>
											<td width="8%"><input type="text" id="inBoundFrom" name="inBoundFrom" size="5" maxlength="5"/></td>
											<td><font>To</font></td>
											<td width="8%"><input type="text" id="inBoundTo" name="inBoundTo" size="5" maxlength="5"/></td>
											<td width="14%"><font>(Time)</font></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<div id="tabValidityData" style="height: 200px;overflow-y: auto;overflow-x: hidden">
			<table width="96%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td width="13%"><font>Load Factor</font></td>
								<td colspan="1">
									<table width="60%" cellpadding="0" cellspacing="0" border="0"><tr>
										<td width="1%" align="left">
											<input type="checkbox" id="loadFactorEnabled" name="loadFactorEnabled" class="NoBorder" value="ON">
										</td>	
										<td width="4%"><font>Min.%</font></td>
										<td width="4%">
											<input type="text" id="loadFactorMin" name="loadFactorMin" class="rightText numaricApply" tabindex="15" size="3" maxlength="3">
										</td>
										<td width="4%"><font>Max.%</font></td>
										<td width="4%">
											<input type="text" id="loadFactorMax" name="loadFactorMax" class="rightText numaricApply" tabindex="16" size="3" maxlength="3">
										</td>
									</tr></table>
								</td>
								<td width="10%"></td>
								<td align="left" valign="top" width="12%"><font>Print Expiry</font></td>
								<td align="left" valign="top">
									<input type="checkbox" id="chkPrintExp" name="chkPrintExp" class="NoBorder"value="on"/>
								</td>
								<td width="10%"></td>
							</tr>
							<tr>
								<td align="left" width="13%"><font>Fare Category</font></td>
								<td align="left">
									<select id="selFareCat" name="selFareCat" size="1" style="width: 90px" >
									</select>
								</td>
								<td></td>
								<td id="lblFlexiCode" align="left"><font>Flexi Code</font></td>
								<td align="left">
									<select id="selFlexiCode" name="selFlexiCode" size="1" style="width: 100px" tabindex="41">
									</select>
								</td>
								<td width="10%"></td>
							</tr>
							<tr>
								<td><font>Pax Category</font></td>
								<td>
									<select id="selPaxCat" name="selPaxCat" size="1" style="width: 90px">
										</select>
								</td>
								<td></td>
								<td></td>
								<td></td>
								<td width="10%"></td>
							</tr>
							<tr>
								<td><font>Allow Modify Date</font></td>
								<td>
									<input type="checkbox" id="modifyByDate" name="modifyByDate" class="NoBorder" value="ON" checked="checked"/>
								</td>
								<td></td>
								<td><font>Allow Modify OnD</font></td>
								<td>
									<input type="checkbox" id="modifyByOND" name="modifyByOND" class="NoBorder" checked="checked"/>
								</td>
								<td width="10%"></td>
							</tr>
							<tr>
								<td><font> <span class="domestic-show" style="display:none">International</span> Mod Buffer Time</font></td>
								<td>
									<input type="text" id="intnModBuffertime_DD" name="intnModBuffertime_DD"  maxlength="3" class="leftText numaricApply" onkeyup="KPValidatePositiveInteger(this,'DAY')" style="width:30px"/>
									<input type="text" id="intnModBuffertime_HR" name="intnModBuffertime_HR"  maxlength="3" class="leftText numaricApply" onkeyup="KPValidatePositiveInteger(this,'HRS')" style="width:30px"/>
									<input type="text" id="intnModBuffertime_MI" name="intnModBuffertime_MI"  maxlength="3" class="leftText numaricApply" onkeyup="KPValidatePositiveInteger(this,'MIN')" style="width:30px"/>
									<font>(DD : HH : MI)</font>
								</td>
								<td></td>
								<td></td>
								<td align="left">
								</td>
								<td width="10%"></td>
							</tr>
							<tr class="domestic-show" style="display:none">
								<td><font> Domestic Mod Buffer Time</font></td>
								<td>
									<input type="text" id="domModBuffertime_DD" name="domModBuffertime_DD"  maxlength="3" class="leftText numaricApply" onkeyup="KPValidatePositiveInteger(this,'DAY')" style="width:30px"/>
									<input type="text" id="domModBuffertime_HR" name="domModBuffertime_HR"  maxlength="3" class="leftText numaricApply" onkeyup="KPValidatePositiveInteger(this,'HRS')" style="width:30px"/>
									<input type="text" id="domModBuffertime_MI" name="domModBuffertime_MI"  maxlength="3" class="leftText numaricApply" onkeyup="KPValidatePositiveInteger(this,'MIN')" style="width:30px"/>
									<font>(DD : HH : MI)</font>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<table id="tblFareDiscount" border="0" cellpadding="0" cellspacing="0" width="60%">
									 	 <tr>
										 	 	<td width="15%"><font>Fare Discount</font></td>
												<td width="1%" align="left"><input type="checkbox" value="Y" class="NoBorder" name="fareDiscount" id="fareDiscount"></td>	
												<td width="4%"><font>Min.%</font></td>
												<td width="4%"><input type="text" maxlength="3" size="3" tabindex="15" onkeyup="allowOnlyPositiveInt('fareDiscountMin')" onkeypress="allowOnlyPositiveInt('fareDiscountMin')" class="rightText" name="fareDiscountMin" id="fareDiscountMin" disabled="">
												</td>
												<td width="4%"><font>Max.%</font></td>
												<td width="4%"><input type="text" maxlength="3" size="3" tabindex="16" onkeyup="allowOnlyPositiveInt('fareDiscountMax')" onkeypress="allowOnlyPositiveInt('fareDiscountMax')" class="rightText" name="fareDiscountMax" id="fareDiscountMax" disabled="">
												</td>	
									 	 </tr>
							 		</table>
								</td>
							</tr>
													
						</table>
					</td>
				</tr>
			</table>
		</div>
		
		<div id="tabCharges" style="height: 200px;overflow: hidden">
			<div style="height: 195px;overflow-x: hidden;overflow-y: auto">
				<table width="90%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>
						<table border="0" width="100%">
							
							<tr id="trCurrencySelect">
								<td colspan="6">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td><font>Charges in: </font></td>
											<td>
												<input type="radio" class="NoBorder" value="BASE_CURR" name="radCurrFareRule" id="radCurrFareRule" checked="checked"/>
												<font>Base Currency</font>
											</td>
											
											<td class="selected-currency">
												<input type="radio" class="NoBorder" value="SEL_CURR" name="radCurrFareRule" id="radSelCurrFareRule" />
												<font>Selected Currency</font>
											</td>
											
											<td class="selected-currency">
												<select style="width: 75px; visibility: hidden;" size="1" id="selCurrencyCode" name="selCurrencyCode" >
												<option></option>
												</select>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<input type="checkbox" id="chkChrgRestrictions" name="chkChrgRestrictions" class="NoBorder" value=""> <font>Charge Restrictions</font>
								</td>
							</tr>
							<tr>
							<td align="left"  width="8%" align="left"><font>MOD</font></td>
								<td width="15%">
									<select id="selDefineModType" name="selDefineModType" size="1" style="width: 40px;" tabindex="14">
									<option></option>
									</select>
								<input type="text" id="txtModification" name="txtModification" size="7"
									 class="rightText numaricApply baseCurr"  maxLength="13">
								
								</td>
								<td class="currency-loacal" width="15%">
									<span id="spModLocal">
									<font>&nbsp;Sel. Curr</font>
									<input type="text" id="txtModificationInLocal" name="txtModificationInLocal" size="7"
										class="rightText numaricApply selCurr" maxLength="13" readonly="readonly">
									</span>		
		
								</td>						
								<td align="left" width="4%"><font>Min.</font></td>
								<td align="left" width="4%"><input type="text" id="txtMin_mod" name="txtMin_mod" class="rightText numaricApply" tabindex="15" size="3" disabled="disabled">
								</td>
								<td align="left" width="4%"><font>Max.</font></td>
								<td align="left" width="15%"><input type="text" id="txtMax_mod" name="txtMax_mod" class="rightText numaricApply" tabindex="16" size="3" disabled="disabled">
								</td>
								<td>
								</td>
							</tr>
							<tr>
							<td align="left" ><font>CNX</font></td>
								<td >
								<select id="selDefineCanType" name="selDefineCanType" size="1" style="width: 40px;"  tabindex="18">
								<option></option>
									</select>
								<input type="text" id="txtCancellation" name="txtCancellation" size="7"
									 class="rightText numaricApply baseCurr"  maxLength="17">
								</td>
								<td class="currency-loacal" >
								<span id="spCancelLocal">
								<font>&nbsp;Sel. Curr</font>	
								<input type="text" id="txtCancellationInLocal" name="txtCancellationInLocal" size="7"
									class="rightText numaricApply selCurr"  maxLength="17" readonly="readonly">	
								</span>		
								</td>
								<td align="left" ><font>Min.</font></td>
								<td align="left" ><input type="text" id="txtMin_canc" name="txtMin_canc" class="rightText numaricApply"  tabindex="19" size="3" disabled="disabled">
								</td>
								<td><font>Max.</font></td>
								<td ><input type="text" id="txtMax_canc" name="txtMax_canc" tabindex="20" size="3" class="rightText numaricApply"disabled="disabled" >
								</td>
								<td></td>
							</tr>
							<tr id="trFareRuleNCC">
							<td align="left" ><font>Name Change Charge</font></td>
								<td >
								<select id="selDefineNCCType" name="selDefineNCCType" size="1" style="width: 40px;"  tabindex="18">
								<option></option>
									</select>
								<input type="text" id="txtNCC" name="txtNCC" size="7"
									 class="rightText numaricApply baseCurr"  maxLength="17">
								</td>
								<td class="currency-loacal" >
								<span id="spCancelLocal">
								<font>&nbsp;Sel. Curr</font>	
								<input type="text" id="txtNCCInLocal" name="txtNCCInLocal" size="7"
									class="rightText numaricApply selCurr"  maxLength="17" readonly="readonly">	
								</span>		
								</td>
								<td align="left" ><font>Min.</font></td>
								<td align="left" ><input type="text" id="txtMin_NCC" name="txtMin_canc" class="rightText numaricApply"  tabindex="19" size="3" disabled="disabled">
								</td>
								<td><font>Max.</font></td>
								<td ><input type="text" id="txtMax_NCC" name="txtMax_NCC" tabindex="20" size="3" class="rightText numaricApply"disabled="disabled" >
								</td>
								<td></td>
							</tr>
							<tr><td colspan="6" style="height:8px;"></td></tr>
							<tr><td colspan="6">
								<table border="0" cellpadding="0" cellspacing="0">
									<tr>
									  <td width="12%"><font>&nbsp;</font></td>
									  	<td width="15%" align="left" title="Charge Type"><font>&nbsp;Type</font></td>
									    <td width="15%" align="left" title="No Show Charge Amount"><font>&nbsp;NOSHOW</font></td>
									    <td width="15%" align="left" title="No Show Charge Amount in Local" class="currency-loacal"><font>&nbsp;Sel. Curr</font></td>
									    <td width="15%" align="left" title="No Show Charge Break Point"><div id="breakPointDiv"><font>&nbsp;Breakpoint</font></div></td>
									    <td width="23%" align="left" title="No Show Charge Boundary"><div id="boundaryDiv"><font>&nbsp;Boundary</font></div></td>
									  </tr>
									  <tr>
									    <td align="left"><font>Adult</font></td>
									    <td align="left"><select id="selADCGTYP"  name="selADCGTYP" class="preferd" ></select></td>
									    <td align="left"><input type="text" name="txtADNoShoCharge"     id="txtADNoShoCharge" size="7" class="rightText desimalApply baseCurr" ></td>
									    <td align="left" class="currency-loacal"><input type="text" name="txtADNoShoChargeInLocal"   id="txtADNoShoChargeInLocal" size="7" class="rightText desimalApply selCurr" ></td>
									    <td align="left"><input type="text" name="txtADNoShoBreakPoint" id="txtADNoShoBreakPoint" size="7" class="rightText desimalApply " ></td>
									    <td align="left"><input type="text" name="txtADNoShoBoundary"   id="txtADNoShoBoundary" size="7" class="rightText desimalApply " ></td>
									  </tr>
									  <tr>
									    <td align="left"><font>Child</font></td>
									    <td align="left"><select id="selCHCGTYP"  name="selCHCGTYP" class="preferd"></select></td>
								        <td align="left"><input type="text" name="txtCHNoShoCharge"     id="txtCHNoShoCharge" size="7" class="rightText desimalApply baseCurr" ></td>
								        <td align="left" class="currency-loacal"><input type="text" name="txtCHNoShoChargeInLocal" id="txtCHNoShoChargeInLocal" size="7" class="rightText desimalApply selCurr" ></td>
										<td align="left"><input type="text" name="txtCHNoShoBreakPoint" id="txtCHNoShoBreakPoint" size="7" class="rightText desimalApply " ></td>
								        <td align="left"><input type="text" name="txtCHNoShoBoundary"   id="txtCHNoShoBoundary" size="7" class="rightText desimalApply " ></td>
									  </tr>
									  <tr>
									    <td align="left"><font>Infant</font></td>
									    <td align="left"><select id="selINCGTYP"  name="selINCGTYP" class="preferd"></select></td>
										<td align="left"><input type="text" name="txtINNoShoCharge"     id="txtINNoShoCharge" size="7" class="rightText desimalApply baseCurr" ></td>
										<td align="left" class="currency-loacal"><input type="text" name="txtINNoShoChargeInLocal"     id="txtINNoShoChargeInLocal" size="7" class="rightText desimalApply selCurr" ></td>
								        <td align="left"><input type="text" name="txtINNoShoBreakPoint" id="txtINNoShoBreakPoint" size="7"   class="rightText desimalApply " ></td>
								        <td align="left"><input type="text" name="txtINNoShoBoundary"   id="txtINNoShoBoundary" size="7" class="rightText desimalApply " ></td>
									  </tr>	
								</table>
						</td>
					</tr>
				</table></td></tr></table>
				<input type="button" style="width:auto;" value="View &amp; Overwrite Fee" class="Button" name="btnOverWriteRule" id="btnOverWriteRule">
				<input type="button" style="width:auto;" value="Override On-Hold" class="Button" name="btnOverRideOHD" id="btnOverRideOHD">
			</div>
		</div>
		<div id="tabLinkAgent" style="height: 200px;overflow: hidden">
			<div style="height: 182px;overflow-x: hidden;overflow-y: auto">
				<%@ include file="LinkAgent.jsp"%>
				<table width="95%">
					<tr>
						<td width="45%">
					        <table id="tblAgentCommission" width="40%">
					        	<tr>
									<td style="height:5px;"></td>
								</tr>
								<tr>
									<td width="50%">
										<font>Agent Commission: Apply By</font>
									</td>
									<td width="10%">
										<select id="selAgentComType" name="selAgentComType" size="1" style="width: 40px;" onChange="required_changed();clearModTypeOnClick();" tabindex="30">
												   <option value="EMPTY"></option>
												   <option value="V">V</option>
												   <option value="PF">PF</option>
										</select>
									</td>
									<td width="10%">
									<td width="15%">
									    <font>Amount</font>
									</td>
									<td width="20%">
										<input type="text" id="txtAgentCommision" name="txtAgentCommision" size="6" onKeyPress="KPValidateDecimel(this,10,2)" onKeyUp="KPValidateDecimel(this,10,2)" class="rightText" tabindex="33" maxLength="15">
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td width="60%">
							<table id="tblTextInstructions">
								<tr>
									<td style="height:5px;"></td>
								</tr>
								<tr>
									<td><font>Agent Instructions</font></td>
								</tr>
								<tr>
									<td width="100%">
										<textarea title="Enter Agent Instructions to be displayed in Fare Quote" rows="1" cols="60" id="txtaInstructions" name="txtaInstructions" style="width:420px;"></textarea>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				
			</div>
		</div>
		<div id="tabLinkPOS" style="height: 200px;overflow: hidden">
			<div style="height: 182px;overflow-x: hidden;overflow-y: auto">
				<table width="100%" border="0" cellpadding="2" cellspacing="0">
					<tr>
						<td colspan="2">
							<span id="spnMDropPOS">
								<select multiple="" style="width: 260px; height: 140px;" class="ListBox" id="lstPOS">
								</select>
							</span>
						</td>											
					</tr>				
					<tr>
						<td colspan="2">
							<input type="hidden" id="hdnPOSMode" name="hdnPOSMode"  value="Mode" />
							<input type="hidden" id="hdnPOSAssigned" name="hdnPOSAssigned"  value="" />
						</td>
					</tr>							
				</table>
			</div>
		</div>
		<div id="tabFareRuleComments" style="height: 200px;overflow-y: auto;overflow-x: hidden; display: none;">
			<table width="96%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="2">
 						<table width="100%" cellpadding="2" cellspacing="3" border="0">
							<tr>
								<td colspan="2">
				  					<table id="listFareRuleComment" height="50px" class="scroll" cellpadding="0" cellspacing="0">										
									</table>
									<div id="temppager" class="scroll" style="text-align:center;"></div>
								</td>
							</tr>
							<tr>
								<td colspan="2"><span id="spnFareRuleComments"></span></td>
							</tr>
							<tr>
								<td align="left"  width="8%" align="left">
									<font>Language</font>
								</td>
								<td>
									<select id="selFareRuleLanguage" name="selFareRuleLanguage" size="1" style="width: 80px;" tabindex="14">
										<c:out value="${requestScope.languagesList}" escapeXml="false" />
									</select>
								</td>
							</tr>
							<tr>
								<td width="13%" valign="top">
									<font>Comments &amp; Rules</font>
								</td>
								<td >
									<textarea name="commentsLang" id="commentsLang" cols="155" rows="5" title="Enter text to be displayed in Fare Quote"></textarea>
								</td>
							</tr>
							<tr>
								<td style="height: 20px" valign="bottom" colspan="5">
									<input name="btnRemoveFareRuleComment" type="button" id="btnRemoveFareRuleComment" class="Button" value="Remove">
									<input name="btnEditFareRuleComment" type="button" id="btnEditFareRuleComment" class="Button" value="Edit"/>
									<input name="btnAddFareRuleComment" type="button" id="btnAddFareRuleComment" class="Button" value="Add">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		
		<div class="admin-inner-panel">
		<!-- <input type="checkbox" id="chkOpenRT" name="chkOpenRT" class="NoBorder" /> -->
			
		<table width="96%" border="0" cellpadding="0" cellspacing="0">
			<tr class="rulesAndComments">
				<td width="13%" valign="top">
					<font>&nbsp;&nbsp;Rules &amp; Comments</font>
				</td>
				<td ><textarea name="comments" id="comments" cols="115" rows="1" 
				title="Enter text to be displayed in Fare Quote"></textarea></td>
			</tr>
			<tr><td colspan="2" style="height: 5px"></td></tr>
			<tr><td colspan="2">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td style="height: 20px" valign="bottom" colspan="5">
							<input type="button" id="btnClose" class="Button" value="Close">
							<input type="button" id="btnBack" name="btnBack" class="Button" value="Back" style="display:none"/>
							<input name="btnReset" type="button" class="Button" id="btnReset" value="Reset">
							<!--<input name="btnLnkAgent" type="button" class="Button" id="btnLnkAgent" value="Link Agents" onclick="CWindowOpen(0)">
						--></td>
						<td align="right" valign="bottom" height="27">
							<input name="btnEditRule" type="button" class="Button" id="btnEditRule" value="Edit Rule" style="display: none;">
							<font>
							<label id="lblUpdateCommentsOnly" style="visibility: visible; " title="If ticked won't create new Fares. Only allows updating comments and agent instructions">
								No Fare Split
							</label>
							</font>
							<input type="checkbox" name="chkUpdateCommentsOnly" id="chkUpdateCommentsOnly" class="NoBorder" title="If ticked won't create new Fares. Only allows updating comments and agent instructions" style="visibility: visible; ">
							<input name="btnSave" type="button" class="Button" id="btnSave" value="Save">
						</td>
					</tr>
				</table>
			</td></tr>
		</table>
	</div>	
</div>
<input type="hidden" name="version" id="version" />
<input type="hidden" name="fareRuleID" id="fareRuleID" />
<input type="hidden" name="versionAD" id="versionAD" />
<input type="hidden" name="versionCH" id="versionCH" />
<input type="hidden" name="versionIN" id="versionIN" />
<input type="hidden" name="paxIDAD" id="paxIDAD" />
<input type="hidden" name="paxIDCH" id="paxIDCH" />
<input type="hidden" name="paxIDIN" id="paxIDIN" />
<input type="hidden" name="fareID" id="fareID" value="<c:out value='${requestScope.fareReqID}' escapeXml='false' />" />
<input type="hidden" name="versionFare" id="versionFare" >
