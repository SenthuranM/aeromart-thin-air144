<%-- 
	 @Author 	: 	Baladewa Welathanthri
--%>
<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	    <title>Manage Fare</title>
	    <%@ include file='../common/pageHD.jsp' %>		
 	
	 	<script type="text/javascript">
			var screeMode = "<c:out value='${requestScope.mode}' escapeXml='false' />";
			var fareID = "<c:out value='${requestScope.fareReqID}' escapeXml='false' />";
			var isFlexiEnabled = "<c:out value='${requestScope.flexiEnabled}' escapeXml='false' />";
			var isPosWebFaresEnabled = "<c:out value='${requestScope.posWebFaresEnabled}' escapeXml='false' />";
			var isMultilingualCommentEnabled = <c:out value='${requestScope.multilingualCommentEnabled}' escapeXml='false' />;
		</script>
		<style type="text/css">
	    	#ui-datepicker-div{display:none}
	    </style>
	</head>
  	<body scroll="no" ondrag='return true' class="tabBGColor" >
	<div style="width:100%;overflow-x: hidden;">
	<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td><%@ include file="../../common/IncludeMandatoryText.jsp"%></td>
	</tr>
	<tr id="newFareRow">
	<td><%@ include file="CreateFare.jsp"%></td>
	</tr> 
	<tr id="fareSearchRow">
	<td>
		<%@ include file="../common/includeAdminPanelTop.jsp"%>
		<img src="../../images/bullet_small_no_cache.gif"/> Search Fare Rules<%@ include file="../common/includeAdminPanelHeader.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="0" id="divSearch">
						<tr>
							<td width="15%"><font>Fare Rule Code</font></td>
							<td width="15%">
								<select id="selFareRuleCode"
								name="selFareRuleCode" size="1" style="width: 100px">
								
								<OPTION value="All">All</OPTION>
								<c:out value="${requestScope.reqFareClassList}" escapeXml="false" />
								</select>
							</td>
							<td width="10%"><font>Status</font></td>
							<td width="12%"><select id="selStatus" name="selStatus"
								size="1" style="width: 60px">
								<OPTION value="All">All</OPTION>
								<OPTION value="ACT" selected="selected">Active</OPTION>
								<OPTION value="INA">Inactive</OPTION>
							</select></td>
							<td align="right"><input type="button" id="btnSearch" name="btnSearch" value="Search" class="button" ></td>
						</tr>
					</table>			
			<%@ include file="../common/includeAdminPanelBottom.jsp"%>
			
			<%@ include file="../common/includeAdminPanelTop.jsp"%>
			<img src="../../images/bullet_small_no_cache.gif"/> Add/Edit Fare Rules <%@ include file="../common/includeAdminPanelHeader.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="0"
						ID="Table9">
						<tr>
							<td colspan="2">
								<div id="fareRuleGridContainer">
									<table id="fareRuleGrid"></table>
									<div id="fareRulePager"></div>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2"><span id="spnBkgCodes"></span></td>
						</tr>
						<tr>
							<td><u:hasPrivilege privilegeId="plan.fares.rule.add">
								<input type="button" id="btnAddFare" name="btnAdd" class="Button" value="Add" />
							</u:hasPrivilege> <u:hasPrivilege privilegeId="plan.fares.rule.edit">
								<input type="button" id="btnEdit" name="btnEdit" class="Button" value="Edit" />
							</u:hasPrivilege>
							 <u:hasPrivilege privilegeId="plan.fares.rule.delete">
								<input type="button" id="btnDelete" name="btnDelete" class="Button" value="Delete" />
							</u:hasPrivilege> <u:hasPrivilege privilegeId="plan.fares.rule.agent">
								<input type="button" id="btnViewAgntFC" name="btnViewAgntFC" class="Button" value="View Agent Wise FR" style="width: 140px;">
							</u:hasPrivilege></td>
							<td align="right"></td>
						</tr>
					</table>
			<%@ include file="../common/includeAdminPanelBottom.jsp"%>
		</td></tr>
		<tr><td>
			<form name="frmFareClasses" id="frmFareClasses" method="post">
			<%@ include file="FareClass.jsp"%>
			</form>
		<input type="hidden" id="hdnMode" name="hdnMode"/> 
		</td>
		</tr></table>
		</div>
	<script>
	var screenId = 'SC_ADMN_018';
	if (screeMode != "CREATEFARE"){
		screenId = 'SC_ADMN_019';
	}
		
		top[2].HideProgress();
	</script>
	<script	src="../../js/v2/fareClasses/ManageFareClasses.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script	src="../../js/v2/fareClasses/CreateFareValidation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script	src="../../js/v2/fareClasses/FareClassValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script	src="../../js/fareClasses/FareRuleComment.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  </body>
</html>