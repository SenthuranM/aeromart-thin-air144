<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE html>
<html>
	<head>
	    <title>Setup Fare</title>
	    <%@ include file='../common/pageHD.jsp' %>
	    <style type="text/css">
	    	#ui-datepicker-div{display:none}
	    </style>
 </head>
 <body class="tabBGColor">
	<div>
		<%@ include file="../../common/IncludeMandatoryText.jsp"%>
	</div>
	
	<%@ include file="../common/includeAdminPanelTop.jsp"%>
	<img src="../../images/bullet_small_no_cache.gif"/> Search Fares<%@ include file="../common/includeAdminPanelHeader.jsp"%>
	<table width="100%" border="0" cellpadding="0" cellspacing="2" id="Table9">
		<tr>
			<td width="7%">
				<font>Origin</font>
			</td>
			<td width="10%">
				<select name="selDeparture" size="1" id="selDeparture"  tabindex="5" title="Original departure location" style="width:60px;">
			    </select> <font class="mandatory">*</font>
			</td>
			<td width="10%">
				<font>Destination</font>
			</td>
			<td width="10%">
			  <select name="selArrival" size="1" id="selArrival"  tabindex="6" title=" Final Destination" style="width:60px;">
			  </select> <font class="mandatory">*</font>
			</td>
			<td width="6%">
				<font>Via 1</font>
			</td>
			<td width="10%">
			  <select name="selVia1" size="1" id="selVia1"  tabindex="8" style="width:60px;">
			  </select>
			</td>
			<td width="6%">
				<font>Via 2</font>
			</td>
			<td width="10%">
			  <select name="selVia2" size="1" id="selVia2"  tabindex="9" style="width:60px;">
			  </select>
			</td>
			<td width="6%">
				<font>Via 3</font>
			</td>
			<td width="12%">
				<select name="selVia3" size="1" id="selVia3"  tabindex="10" style="width:60px;">
			  </select>
			</td>
			<td width="9%">
				<font>Via 4</font>
			</td>
			<td width="10%">
			  <select name="selVia4" size="1" id="selVia4"  tabindex="11" style="width:60px;">
			  </select>
			</td>
		</tr>
		<tr>
			<td colspan="5">
				<input id="radSOD1" class="NoBorder" type="radio" tabindex="7" value="AllComb" name="radSOD1" checked="checked">
				<font>Show All O&D Combinations</font>&nbsp; &nbsp;
				<input id="radSelected" class="NoBorder" type="radio" tabindex="8" value="selectedComb" name="radSOD1">
				<font>Selected O&D Only</font>
			</td>
			<td><font>COS</font></td>
			<td colspan="2">
				<select id="selCOS" tabindex="9" size="1" name="selCOS">
				</select> <font class="mandatory">*</font>
			</td>
			<td><font>Status</font></td>
			<td>
				<select id="selStatus" tabindex="9" size="1" name="selStatus">
					<option value="ACT">Active</option>
					<option value="INA">Inactive</option>
					<option value="All">All</option>
				</select> <font class="mandatory">*</font>
			</td>
			<td><font>Fare Rule</font></td>
			<td>
				<input type="text" maxlength="10" style="width: 80%;" name="txtFareRule" id="txtFareRule">
			</td>
			
		</tr>
		<tr>
			<td colspan="3">
				<input type="radio" tabindex="12" value="EffectiveAll" class="NoBorder" name="rasSF1" id="rasSF1"/>
				<font>All Fares</font>
				&nbsp; &nbsp;
				<input type="radio" tabindex="13" value="From" class="NoBorder" name="rasSF1" id="radAllFares"/>
				<font>Effective During</font>
			</td>
			<td><font>From</font></td>
			<td colspan="2"><input type="text" tabindex="14" maxlength="10" name="txtFromDate" id="txtFromDate" style="width:100px"/></td>
			<td colspan="3">
				<font>To</font> &nbsp; &nbsp; &nbsp;
				<input type="text" tabindex="14" maxlength="10" name="txtToDate" id="txtToDate" style="width:100px"/>
			</td>
			<td><font>Booking Class</font></td>
			<td colspan="2">
				<input type="text" maxlength="10" style="width: 50px;" name="txtBkingClass" id="txtBkingClass">
			</td>
		</tr>
		<tr>
			<td colspan="5">
				<input id="radValid" class="NoBorder" type="radio" value="VALID" checked="checked" name="radDt">
				<font>Dep Date(Out)</font>
				&nbsp; &nbsp;
				<input id="radSales" class="NoBorder" type="radio"  value="SALES" name="radDt" checked="checked">
				<font>Sales Date</font>
				&nbsp; &nbsp;
				<input id="radReturn" class="NoBorder" type="radio" value="VALIDRT" name="radDt">
				<font>Dep Date(In)</font>
			</td>
			<td colspan="3">
				<font>Fare Basis Code</font>
				&nbsp; &nbsp;
				<select id="selBasisCode" tabindex="9" size="1" name="selBasisCode"></select>
				
			</td>
			<td colspan="2"></td>
			<td align="right" colspan="2">
				<input type="button" tabindex="16" title="Search" value="Search" class="Button" name="btnSearch" id="btnSearch">
			</td>
		</tr>
	</table>
	<%@ include file="../common/includeAdminPanelBottom.jsp"%>
	
	<%@ include file="../common/includeAdminPanelTop.jsp"%>
	<img src="../../images/bullet_small_no_cache.gif"/>Fares<%@ include file="../common/includeAdminPanelHeader.jsp"%>
	<div>
		<div id="fareGridContainer">
			<table id="fareGrid"></table>
			<div id="fareGridPager"></div>
		</div>
		
		<table cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td><font>Amend Date of Selected Fares </font></td>
			<td><font>Valid From </font>&nbsp;&nbsp;<input type="text" tabindex="17" maxlength="10" style="width: 75px;" name="txtExtendFrom" id="txtExtendFrom" ></td>
			<td><font>Valid To </font>&nbsp;&nbsp;<input type="text" tabindex="17" maxlength="10" style="width: 75px;" name="txtExtendTo" id="txtExtendTo" ></td>
			<td><input type="button" tabindex="18" title="Amend Validity" style="width: 110px" class="Button" value="Amend Validity" id="btnExtend" />
			</td>
			<td colspan="3" width="25%">
				<table id="tblFareTypes" style="display: none;">
					<tbody><tr>
						<td width="14%" class="ondFareMaster" id="tdFareColorMaster"></td>
						<td><font>&nbsp;Master Fare</font></td>
						<td width="14%" class="ondFareLinked" id="tdFareColorLink"></td>
						<td><font>&nbsp;Linked Fare</font></td>
						<td></td>
					</tr>
				</tbody></table>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<input type="button" tabindex="20" title="Add New Fare"  class="Button" value="Add Fare" name="btnAdd" id="btnAdd">
				<u:hasPrivilege privilegeId="plan.fares.setup.upload">
					<input type="button" tabindex="20" title="Upload New Fare" class="Button" value="Upload Fare(s)" name="btnUpload" id="btnUpload" style="width:100px;">				
				</u:hasPrivilege>
				<input type="button" tabindex="21" title="Edit" class="Button" value="Edit" name="btnEdit" id="btnEdit" >
				<input type="button" tabindex="22" title="Edit Fare Rule" class="Button" value="Edit Rule" name="btnEditRule" id="btnEditRule" >
				<input type="button" tabindex="23" title="Save" value="Save" id="btnSave" name="btnSave" class="Button">
				<input type="button" tabindex="19" title="Reset"  value="Reset" id="btnReset" class="Button">
			</td><td><font>POS</font></td>
			<td><select style="width: 60px;" size="1" name="selPOS" id="selPOS">
			</select></td>
			<td>
				<input type="button" tabindex="22" title="Edit Fare Rule" class="Button" value="Get Charges" name="btnGetCharges" id="btnGetCharges">
				
			</td>
		</tr>
	</table>
	</div>
	
	<%@ include file="../common/includeAdminPanelBottom.jsp"%>
	
	<%@ include file="../common/includeAdminPanelTop.jsp"%>
	<img src="../../images/bullet_small_no_cache.gif"/>Charges<%@ include file="../common/includeAdminPanelHeader.jsp"%>
	<table width="100%" border="0" cellpadding="0" cellspacing="2">
	<tr>
		<td>
			<table id="fareChargerGrid"></table>
		</td>
	</tr>
	<tr>
		<td align="right">
			<input type="button" id="btnClose" class="Button" value="Close">
		</td>
	</tr>
	</table>
	<%@ include file="../common/includeAdminPanelBottom.jsp"%>
	
	<script	src="../../js/v2/fareClasses/setupFares.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
</body>