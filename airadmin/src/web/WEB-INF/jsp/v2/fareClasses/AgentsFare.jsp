<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	    <title>View Fare Rules by Agent</title>
	    <%@ include file='../common/pageHD.jsp' %>		
 	</head>
<body scroll="no" ondrag='return true' class="tabBGColor" >
  		<form action="showAgentRules.action" name="frmAgentFC" id="frmAgentFC" method="post">
  		<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
  			<tr>
  				<td style="height:300px;" valign="top">
  					<%@ include file="../common/includeWindowAdminPanelTop.jsp"%>View Agent Wise Fare Rules<%@ include file="../common/includeAdminPanelHeader.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" id="divSearch">
							<tr>
								<td width="15%">
									<font>Agent Name</font>
								</td>
								<td width="70%">
									<select id="selAgent" name="selAgent" style="width:200px">
									<option value=""></option>
									<option value="All">All</option>
									</select>
								</td>
								<td align="right">
									<input type="button" id="btnSearch" name="btnSearch" value="Search" class="Button"/>
								</td>
							</tr>
							<tr><td colspan="3" style="height: 10px"></td></tr>
							<tr>
								<td colspan="3">
									<table id="spnFC"></table>
									<div id="spnFCPager"></div>
									<font>&nbsp;** indicates the relevant fare rule is linked to a fare</font>
								</td>											
							</tr>
							<tr>
								<td colspan="3" align="right">
									<input type="button" id="btnCancel" value="Close" class="Button" onclick="javascript:window.close()">																
									<input type="hidden" name="hdnMode" id="hdnMode" value="AgentFares">
									<input type="hidden" id="hdnSearchCriteria" name="hdnSearchCriteria"  value="" >
									<input type="hidden" id="hdnRecNo" name="hdnRecNo"  value="" >
								</td>
							</tr>			
						</table>
					<%@ include file="../common/includeAdminPanelBottom.jsp"%>
  				</td>
  			</tr>
  		</table>
  		</form>
		<script src="../../js/v2/fareClasses/AgentsFare.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>		
	</body>
</html>
