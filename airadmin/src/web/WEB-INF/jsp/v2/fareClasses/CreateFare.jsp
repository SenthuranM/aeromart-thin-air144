<%@ include file="../common/includeAdminPanelTop.jsp"%>Fare<%@ include file="../common/includeAdminPanelHeader.jsp"%>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" >
	<tbody><tr>
			<td>
			<table width="98%" border="0" cellpadding="0" cellspacing="2">
				<tbody><tr>
				<td width="12%"><font>Fare ID</font></td>
				<td><font class="fntBold"><span id="spnId"></span></font>
				</td></tr>
			</tbody></table>
			</td>
			</tr>
			<tr>
				<td>
					<table width="90%" border="0" cellpadding="0" cellspacing="2">
						<tbody><tr>					
							<td width="12%"><font>Origin</font></td>
							<td align="left" width="8%">
								<select id="selOrigin">
									<option value=""></option>
								</select>
							</td>
							<td width="3%"><font class="mandatory">*</font></td>
			
							<td width="5%"><font>Destination</font></td>
							<td align="left" width="12%">
								<select id="selDestination">
									<option value=""></option>
								</select>
							</td>
							<td width="3%"> <font class="mandatory">*</font></td>
			
							<td width="6%"><font>Via 1</font></td>
							<td width="7%">
								<select id="selVia1">
									<option value=""></option>
								</select>
							</td>
			
							<td align="right" width="6%"><font>Via 2</font></td>
							<td width="7%">
								<select id="selVia2">
									<option value=""></option>
								</select>
							</td>
			
							<td align="right" width="6%"><font>Via 3</font></td>
							<td width="7%">
								<select id="selVia3">
									<option value=""></option>
								</select>
							</td>
			
							<td align="right" width="6%"><font>Via 4</font></td>
							<td>
								<select id="selVia4">
									<option value=""></option>
								</select>
							</td>		
							<td align="left" width="20%"><!-- <font class="fntBold"><span id="spnFixed"></span></font> --></td>	
							<td><input type="button" tabindex="27" class="Button" style="width: 120px;" value="Add Proration" id="btnAddProration"></td>
						</tr>
					</tbody></table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="90%" border="0" cellpadding="0" cellspacing="2">
						<tbody><tr>			
							<td width="12%"><font>Booking Class</font></td>
							<td width="10%"><select tabindex="7" style="width: 60px;" size="1" name="selBc" id="selBc">
								<option value=""></option>
							</select><font class="mandatory">&nbsp;*</font></td>			
							<td align="left" width="47%"><font class="fntBold"><span id="spnFixed"></span></font></td>			
							<td width="10%"><font>Active</font></td>
							<td><input type="checkbox" tabindex="8" class="NoBorder" id="chkStatus" name="chkStatus"></td>
							
						</tr>
					</tbody></table>
				</td>
			</tr>
			<tr>
				<td colspan="12"><font class="fntBold">Fare Rules</font></td>
			</tr>
			<tr>
				<td>
					<table width="90%" border="0" cellpadding="0" cellspacing="2">
						<tbody><tr>
							<td align="left" width="12%"><font>Fare Rule Code</font></td>
							<td width="12%"><select tabindex="24" style="width: 100px;" size="1" name="selFC" id="selFC">
								<option value=""></option>
							</select></td>
							<td><input type="button" tabindex="25" class="Button" value="Add" id="btnAddFareRule">
							    <input type="button" tabindex="26" style="width: 100px;" class="Button" value="Ad-Hoc Rule" id="btnAddHocRule" >
							</td>			
						</tr>
					</tbody></table>
				</td>
			</tr>
			<tr id="divDateOutbound">
				<td>					
					<table width="90%" border="0" cellpadding="0" cellspacing="2">
						<tbody><tr>
							<td width="12%"><font>From Date (outbound)</font></td>
							<td width="14%"><input type="text" tabindex="9" style="width:70%" maxlength="10" name="txtFromDt" id="txtFromDt"> 
							</td>
							<td width="7%"><font class="mandatory">&nbsp;*</font></td>
			
							<td align="left" width="12%"><font>To Date (outbound)</font></td>
							<td width="14%"><input type="text" tabindex="10" style="width:70%" maxlength="10" name="txtToDt" id="txtToDt">
							</td>
							<td><font class="mandatory">&nbsp;*</font></td>	
							<td>
								<table>
									<tr>
									<td width="98%"><font>Apply 10 year difference between from dates and to dates</font></td>
									<td align="left" width="2%"><input type="checkBox" class="noBorder" id="chkExtendToDate" name="chkExtendToDate"></td>
									</tr>
								</table>
							</td>			
						</tr>
					</tbody></table>
				</td>
			</tr>
			<tr id="divDateInbound">
				<td>					
					<table width="90%" border="0" cellpadding="0" cellspacing="2">
						<tbody><tr>
							<td width="12%"><font>From Date (inbound)</font></td>
							<td width="14%"><input type="text" tabindex="11" style="width:70%" maxlength="10" name="txtFromDtInbound" id="txtFromDtInbound" disabled=""> 
							</td>
							<td width="7%"><font class="mandatory">&nbsp;*</font></td>
			
							<td align="left" width="12%"><font>To Date (inbound)</font></td>
							<td width="14%"><input type="text" tabindex="11" style="width:70%" maxlength="10" name="txtToDtInbound" id="txtToDtInbound" disabled="">
							</td>
							<td><font class="mandatory">&nbsp;*</font></td>				
						</tr>
					</tbody></table>
				</td>
			</tr>
			<tr id="divSalesDates">
				<td>
					<table width="90%" border="0" cellpadding="0" cellspacing="2">
						<tbody><tr>
							
							<td width="12%"><font>Sales Start Date</font></td>
							<td width="14%"><input type="text" tabindex="12" style="width:70%" maxlength="10" name="txtSlsStDate" id="txtSlsStDate"> 
								</td>
							<td width="7%"><font class="mandatory">&nbsp;*</font></td>
			
							<td align="left" width="12%"><font>Sales End Date</font></td>
							<td width="14%"><input type="text" tabindex="12" style="width:70%" maxlength="10" name="txtSlsEnDate" id="txtSlsEnDate" >
							</td>
							<td><font class="mandatory">&nbsp;*</font></td>
							
						</tr>
					</tbody></table>
				</td>
			</tr>
			
			<tr id="divCurrSelect" style="display: none;"  class="selected-currency">
				<td>
					<table width="90%" border="0" cellpadding="0" cellspacing="2">
							<tbody><tr>
								<td width="12%"><font>Show Fares In</font> </td>
								<td width="20%">
									<input type="radio" checked="checked" onblur="" onchange="" class="NoBorder" value="BASE_CURR" name="radCurr" id="radCurr">
									<font>Base Currency</font>
								</td>
								<td width="25%">
									<input type="radio" onchange="" class="NoBorder" value="SEL_CURR" name="radCurr" id="radSelCurr">
									<font>Departing Airport Currency</font>
								</td>
								<td width="45%"></td>
							</tr>
						</tbody></table>
				</td>
			</tr>
			
			<tr id="divFareAmounts">
				<td>
					<table border="0" width="90%" border="0" cellpadding="0" cellspacing="2">
						<tbody><tr>
							<td width="12%"><font>Base Fare</font></td>
							<td width="10%"><input type="text" tabindex="13" class="rightText" size="13" maxlength="13"  id="txtAED" name="txtAED" style="width:65px"/><font class="mandatory" >*</font></td>
							<td width="42%">
								<table>
									<tbody><tr>
										<td align="right"><font>Child Fare</font></td>
										<td><select tabindex="14" style="width: 35px; height: 15px;" size="1" name="selCamt" id="selCamt">
											<option value="V">V</option>
											<option value="P">P</option>
											</select>
										</td>
										<td align="center"><input type="text" tabindex="15" class="rightText" size="13" maxlength="13" id="txtCHL" name="txtCHL" style="width:65px"/></td>
										<td>&nbsp;&nbsp;&nbsp;</td>
										<td align="right"><font>Infant Fare</font></td>
										<td><select tabindex="16" onchange="setFareType(this)" style="width: 35px; height: 15px;" size="1" name="selIamt" id="selIamt">
											<option value="V">V</option>
											<option value="P">P</option>
										</select></td>
										<td align="center"><input type="text" tabindex="17" class="rightText" size="13" maxlength="13" id="txtINF" name="txtINF" style="width:65px"/></td>
									</tr>
								</tbody></table>
							</td>
							<td width="13%"><font>Create Return Fare </font></td>
							<td align="left" width="1%"><input type="checkBox" onchange="required_changed()" tabindex="18" class="noBorder" id="chkAllowReturn" name="chkAllowReturn" >
							</td><td width="13%"><font>Fare Basis Code</font></td>
							<td><font class="fntBold"><input type="text" maxlength="7" size="8" name="txtFBC" id="txtFBC" disabled=""></font></td>
						</tr>
					</tbody></table>				
				</td>
			</tr>
			
			<tr>
				<td>
					<div id="divFare" style="display: none;"  class="selected-currency">
						<table width="90%" border="0" cellpadding="0" cellspacing="2">
							<tbody><tr>
								<td width="12%"><font>Base Fare In (</font>
								<label id="airportCurrency" name="airportCurrency"></label><font>)</font></td>
								<td width="10%"><input type="text" tabindex="19" class="rightText" size="8" maxlength="13" id="txtBaseInLocal" name="txtBaseInLocal" style="width:65px"/><font class="mandatory">*</font></td>
								
								<td width="42%">
								<table>
									<tbody><tr>
										<td align="right"><font>Child Fare</font></td>
										<td><select tabindex="20" onchange="setFareType(this)" style="width: 35px; height: 15px;" size="1" name="selCamtInLocal" id="selCamtInLocal">
											<option value="V">V</option>
											<option value="P">P</option>
										</select></td> 
										<td align="center"><input type="text" tabindex="21" class="rightText" id="txtCHFareInLocal" name="txtCHFareInLocal" style="width:65px"/></td>
										<td>&nbsp;&nbsp;&nbsp;</td>
										<td align="right"><font>Infant Fare</font></td>
										<td><select tabindex="22" onchange="setFareType(this)" style="width: 35px; height: 15px;" size="1" name="selIamtInLocal" id="selIamtInLocal">
											<option value="V">V</option>
											<option value="P">P</option>
										</select></td>
										<td align="center"><input type="text" tabindex="23" class="rightText" size="13" maxlength="13" id="txtINFInLocal" name="txtINFInLocal" style="width:65px" /></td>
									</tr>
								</tbody></table>
								</td>
								<td width="10%"><font></font></td>
								<td align="left" width="1%">
								</td><td width="10%"><font></font></td>
								<td></td>				
							</tr>
						</tbody></table>
					</div>
				</td>
			</tr>
			
			<tr>
				<td>
					<table width="auto" border="0" cellpadding="0" cellspacing="2" id="linkFareDiv" style="display: none">
						<tbody>
							<tr>
								<td width="12%"><font>Master Fare</font></td>
								<td width="10%"><input type="checkbox" tabindex="19" class="noBorder" id="chkMasterFare" name="chkMasterFare" /></td>
								
								<td width="15%">
									<font>Master Fare Reference Code</font>
								</td>
								<td width="10%"><input type="text" class="rightText" maxlength="10" size="10" name="masterFareRefCode" id="masterFareRefCode"></td>
							</tr>
							<tr>
								<td width="9%"><font>Linked Fare</font></td>
								<td align="left" width="1%"><input type="checkBox" tabindex="18" class="noBorder" id="chkLinkFare" name="chkLinkFare">						
								</td><td width="10%"><input type="button" style="width: 140px;" value="Link Fares" class="Button" name="btnLinkFares" id="btnLinkFares" disabled=""></td>
								<td width="15%"><font>Master Fare ID</font></td>
								<td width="15%"><select style="width: 100px" size="1" name="selectedMasterFareID" id="selectedMasterFareID" disabled="">
								<option selected="selected" value=""></option>
								</select></td>
								<td width="20%"><font>Link Fare Percentage</font></td>
								<td width="10%"><input type="text" maxlength="3" size="3" class="rightText" name="linkFarePercentage" id="linkFarePercentage" disabled=""></td>							
								<td></td>														
							</tr>
					</table>
				</td>				
				<td>
					<table width="auto" border="0" cellpadding="0" cellspacing="2" id="divModToSameFare" style="display: none">
					<tr>
						<td width="12%"><font>Modify To Same Fare</font></td>
						<td width="10%"><input type="checkbox" tabindex="20" class="noBorder" id="chkModToSameFare" name="chkModToSameFare" /></td>
					</tr>
					</table>			
				</td>				
			</tr>
		</tbody>
	</table>			
<%@ include file="../common/includeAdminPanelBottom.jsp"%>