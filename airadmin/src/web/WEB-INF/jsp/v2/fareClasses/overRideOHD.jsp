<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Overwrite Fare Rule</title>
<meta http-equiv="pragma" content="no-cache"/>
<meta http-equiv="cache-control" content="no-cache"/>
<meta http-equiv="expires" content="-1"/>
<link rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>
<link rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>

<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	
<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>

<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
<script	src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

<script type="text/javascript">
var screeMode = "<c:out value='${requestScope.mode}' escapeXml='false' />",
fareRuleCode = "<c:out value='${requestScope.fareRuleCode}' escapeXml='false' />",
fareRuleID = "<c:out value='${requestScope.fareRuleID}' escapeXml='false' />",
version = "<c:out value='${requestScope.version}' escapeXml='false' />";
</script>
</head>
	<body class="tabBGColor">
		<table width="99%" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround">
		<tr>
			<td><font class="fntSmall">&nbsp;</font></td>
		</tr>
		<tr>
			<td><%@ include file="../../common/IncludeMandatoryText.jsp"%></td>
		</tr>
		<%-- <tr>
			<td><%@ include file="../../common/IncludeFormTop.jsp"%>Search	On-Hold Configs<%@ include file="../../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="2" cellspacing="2">
					<tr>
						<td colspan="7"><font class="fntSmall">&nbsp;</font></td>
					</tr>
					<tr>
						<td width="12%"><font>Fare Rule Code</font></td>
						<td width="12%"><label id="fareRuleCode" class="fntBold"></label></td>
						<td width="10%"><font>Release Time</font></td>
						<td width="15%">
							<input type="text" id="releaseTime" id="releaseTime" value="" />
						</td>
						<td width="14%"><font>Status</font></td>
						<td width="15%">
							<select id="status" >
								<option value="ACT">Active</option>
								<option value="INACT">In-Active</option>
							</select>
						</td>
						<td align="right"> <input type="button" id="btnSearch" name="btnSearch" value="Search" class="Button" /> </td>
					</tr>
				</table>
				<%@ include file="../../common/IncludeFormBottom.jsp"%></td>
			</tr> --%>
		</table>

		<table width="99%" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround">
		<tr>
			<td><font class="fntSmall">&nbsp;</font></td>
		</tr>
		<tr>
			<td class="FormBackGround"><%@ include file="../../common/IncludeFormTop.jsp"%>On-Hold Configs<%@ include file="../../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td><font class="fntSmall">&nbsp;</font></td>
					</tr>
					<tr>
						<td><font>Fare Rule Code</font> &nbsp;&nbsp; <label id="fareRuleCode" class="fntBold"></label></td>
					</tr>
					<tr>
						<td id="wrapperGrid">
							<table id="tblOverRideOHD"></table>
							<div id="OverRidePager"></div>
						</td>
					</tr>
					<tr>
						<td>
							<input type="button" id="btnAdd" name="btnAdd" class="Button" value="Add" />
							<input type="button" id="btnEdit" name="btnEdit" class="Button" value="Edit" />
							<input type="button" id="btnDelete" name="btnDelete" class="Button" value="Delete" />
						</td>
					</tr>
				</table>
				<%@ include file="../../common/IncludeFormBottom.jsp"%></td>
			</tr>
			</table>
			
		<table width="99%" cellpadding="0" cellspacing="0" border="0" align="center" class="TBLBackGround">
		<tr>
			<td><font class="fntSmall">&nbsp;</font></td>
		</tr>
		<tr>
			<td class="FormBackGround"><%@ include file="../../common/IncludeFormTop.jsp"%>Add/Edit Override On-Hold Configs<%@ include file="../../common/IncludeFormHD.jsp"%>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td><font class="fntSmall">&nbsp;</font></td>
					</tr>
					<tr>
						<td>
							<table width="100%" cellpadding="2" cellspacing="2" border="0" id="tblFormData">
								<tr>
									<td width="20%" align="left"><font>Start Cutover Time(w.r.t. Dep.Time)</font></td>
									<td width="25%" align="left">
										<input type="text" name="startCutOTimeDD" id="startCutOTimeDD" size="2" />
										<input type="text" name="startCutOTimeHH" id="startCutOTimeHH" size="2" />
										<input type="text" name="startCutOTimeMM" id="startCutOTimeMM" size="2" />
										<font class="mandatory"><b>*</b></font> <font>(DD:HH:MM)</font>
									</td>
									<td width="5%">&nbsp;</td>
									<td width="20%" align="left"><font>End Cutover Time(w.r.t. Dep.Time)</font></td>
									<td width="25%" align="left">
										<input type="text" name="endCutOTimeDD" id="endCutOTimeDD" size="2" />
										<input type="text" name="endCutOTimeHH" id="endCutOTimeHH" size="2" />
										<input type="text" name="endCutOTimeMM" id="endCutOTimeMM" size="2" />
										<font class="mandatory"><b>*</b></font> <font>(DD:HH:MM)</font>
									</td>
								</tr>
								
								<tr>
									<td width="20%" align="left"><font>Release Time</font></td>
									<td width="25%" align="left">
										<input type="text" id="releaseTimeDD" name="releaseTimeDD" size="2" />
										<input type="text" id="releaseTimeHH" name="releaseTimeHH" size="2" />
										<input type="text" id="releaseTimeMM" name="releaseTimeMM" size="2" />
									 <font class="mandatory"><b>*</b></font> <font>(DD:HH:MM)</font></td>
									<td width="5%">&nbsp;</td>
									<td width="20%" align="left"></td>
									<td width="25%" align="left">
									</td>
								</tr>
								
								<tr>
									<td width="20%" align="left"><font>Release Time with Respect To</font></td>
									<td width="25%" align="left">
										<select id="timeWithRespect" name="timeWithRespect">
											<option value=""></option>
											<option value="BKG_DATE">BKG_DATE</option>
											<option value="DEP_DATE">DEP_DATE</option>
										</select>
									<font class="mandatory"><b>*</b></font></td>
									<td width="5%">&nbsp;</td>
									<td width="20%" align="left"></td>
									<td width="25%" align="left"></font>
									</td>
								</tr>
								<tr>
									<td width="20%" align="left"><font>Module Code</font></td>
									<td width="25%" align="left">
										<select id="moduleCode" name="moduleCode">
											<option value="ANY">ANY</option>
											<option value="XBE">XBE</option>
											<option value="IBE">IBE</option>
										</select>
									</td>
									<td width="5%">&nbsp;</td>
									<td width="20%" align="left"></td>
									<td width="25%" align="left">
									</td>
								</tr>
								<tr>
									<td align="right" colspan="5">
										<input type="hidden" id="ohdRelId" name="ohdRelId" value="" />
										<input type="hidden" id="version" name="version" value="" />  
										<input type="button" id="btnSave" name="btnSave" class="Button" value="Save" />
										<!-- <input type="button" id="btnReset" name="btnReset" class="Button" value="Reset" /> -->
										<input type="button" id="btnCancel" value="Close" class="Button" onclick="javascript:window.close()"/>	
									</td>
								</tr>
							</table>
						</td>
					</tr>
					
					<tr>
						<td><font class="fntSmall">&nbsp;</font></td>
					</tr>
				</table>
				<%@ include file="../../common/IncludeFormBottom.jsp"%></td>
			</tr>
			</table>
			
	<div class="PageBottomColor" style="margin-top:10px">
		<table width="100%" cellspacing="1" cellpadding="0" border="0" id="Table6">
			<tbody><tr>
				<td width="20">&nbsp;</td>
				<td width="20" valign="bottom"><img id="imgError" border="0" src="../../images/Err_no_cache.gif" name="imgError" style="visibility: hidden; display: none;" /></td>
				<td width="1"><img src="../../images/StatusBar_no_cache.jpg" /></td>
				<td id="tdSTBar" class="StatsBar"><span id="spnDate">&nbsp;</span></td>
				<td width="1"><img src="../../images/StatusBar_no_cache.jpg" /></td>
				<td width="150" align="center"><span id="spnUID"></span></td>
				<td width="1"><img src="../../images/StatusBar_no_cache.jpg" /></td>
				<td width="200" align="center"><span id="spnScreenID"><font class="fontPGInfo"></font></span></td>
				<td width="20">&nbsp;</td>
			</tr>
		</tbody></table>
	</div>
	<div id='divLoadMsg' style='visibility:hidden;z-index:1000; background:transparent;'>
		<iframe src="LoadMsg" id='frmLoadMsg' name='frmLoadMsg'  frameborder='0' scrolling='no' style='position:absolute; top:50%; left:40%; width:260; height:40;'></iframe>
	</div>
	</body>
	<script src="../../js/v2/fareClasses/overRideOHD.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
</html>
