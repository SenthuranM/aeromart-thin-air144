<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Manage Schedule Reports</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
<LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css">

	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">		
		<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
		
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		<script	src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
				
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
    	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/commonSystem.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/common/isa.airadmin.jq.plugin.config.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/reports/scheduling/scheduleReportsManager.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/reports/scheduling/scheduledRptSentItems.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		

</head>

<body class="tabBGColor" scroll="no"  onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false'>
  			<script type="text/javascript">			
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />										
			</script>
	
	<form id = "frmSearch" method="post" action="">

			<div id="divSearch" style="height:30px;text-align:left;">
				<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
					<tr>
						<td width="10%"><font>Report Name</font></td>
						
						<td width="15%">
							<select name="serchSelReportName" size="1" id="serchSelReportName">
								<option value="">All</option>																				
								<c:out value="${requestScope.reqScheduledReportNames}" escapeXml="false" />
							</select>
							</td>						
						<td width="8%"><font>From</font></td>
						<td width="10%"><input name="srchPeriodFrom" type="text" id="srchPeriodFrom" size="10"  maxlength="50" onkeyUp="alphaWhiteSpaceValidate(this)" onkeyPress="alphaWhiteSpaceValidate(this)"></td>
					
						<td width="8%"><font>To</font></td>
						<td width="10%"><input name="srchPeriodTo" type="text" id="srchPeriodTo" size="10"  maxlength="50" onkeyUp="alphaWhiteSpaceValidate(this)" onkeyPress="alphaWhiteSpaceValidate(this)"></td>
						<td width="15%"><font>Scheduled By</font></td>
						<td width="10%"><input name="srchScheduledBy" type="text" id="srchScheduledBy" size="15"  maxlength="50"></td>

						<td align="right">									
							<input name="btnSearch" type="button" id="btnSearch" value="Search">
						</td>
					</tr>
				</table>			
			</div>
	</form>
	<div id="divResultsPanel" style="height:240px;text-align:left;background-color:#ECECEC;">
				<table id="tblScheduled" class="scroll" cellpadding="0" cellspacing="0"></table>
				<div id="scheduledReportsPager" class="scroll" style="text-align:center;"></div>
					
					<u:hasPrivilege privilegeId="rpt.scheduling.edit">
						<input name="btnEdit" type="button" id="btnEdit" value="Edit" class="btnDefault">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="rpt.scheduling.manage">	
						<input name="btnDelete" type="button" id="btnDelete" value="Act/Deactivate" class="btnMedium">
					</u:hasPrivilege>
					<u:hasPrivilege privilegeId="rpt.scheduling.edit">	
						<input name="btnSentItems" type="button" id="btnSentItems" value="View Emails" class="btnMedium">
					</u:hasPrivilege>
				
	</div>	
<div id="divTabbed" style="height:260px;text-align:left;background-color:#ECECEC;">
	<ul>
		<li><a href="#divDisplayScheduled">Modify Scheduled</a></li>
		<li><a href="#divSentItems">View Sent Items</a></li>
	</ul>

<form id = "frmModify" method="post" action="showScheduledReports!saveScheduledReport.action">
			<div id="divDisplayScheduled" style="height:150px;text-align:left;background-color:#ECECEC;">
				<table width="98%" border="0" cellpadding="0" cellspacing="0" align="center" ID="Table1">
					<tr><td>&nbsp;</td></tr>	
					<tr>
						<td width="20%"><font>Report Name</font></td>
						<td  width = "20%"><label id = "lblReportDisplayName"></label>
						</td>
						<td colspan = "2"></td>						
					</tr>
					<tr><td>&nbsp;</td></tr>	
					<tr>
						<td ><font>From</font></td>
						<td>
							<input name="reportsViewTO.startDate" type="text" class = "shpt_rpt_editable" id="fromDate" size="20"  maxlength="50" />						
						</td>
						<td width = "10%"><font>To</font></td>
						<td>
							<input name="reportsViewTO.endDate" type="text" id="toDate" class = "shpt_rpt_editable" size="20"  maxlength="50" />						
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>	
					<tr>
						<td><font>Scheduled By</font></td>
							<td><label id = "lblScheduledBy"></label>
						</td>
						<td colspan = "2">
						<table width="98%" border="0" cellpadding="0" cellspacing="0" align="center" ID="Table3">
							<tr>
								<td ><font>Send Email To</font></td>
								<td  rowspan="2">
								<textarea name='reportsViewTO.emailIDs' id='txtSendTo' class = "shpt_rpt_editable"  style='height:50px;width:300px;'></textarea>
								</td>
							</tr>
						</table>
						</td>	
					</tr>
					<tr><td>&nbsp;</td></tr>	
					<tr>
						<td><font>Cron Expression</font></td>
						<td><label id = "lblCornExp"></label>
						</td>
						<td colspan = "2"></td>		
					</tr>
						</table>
					</div>	
					<div id = "divSentItems" style="height:150px;text-align:left;background-color:#ECECEC;">
								<table id="tblScheduledSent" class="scroll" cellpadding="0" cellspacing="0"></table>
								<div id="sentItemsPager" class="scroll" style="text-align:center;"></div>
					</div>	
					<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table3">
						
					<tr>
						<td colspan="3" style="height:42px;"  valign="bottom">
							<input name="btnClose" type="button"  id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" class="btnDefault">
						    <input name="btnReset" type="button"  id="btnReset" value="Reset" class="btnDefault">
						</td>
						<td align="right" valign="bottom">
							<input name="btnSave" type="button"  id="btnSave" value="Save" class="btnDefault">
						</td>
					</tr>
				</table>
						

<input type="hidden" name="reportsViewTO.version" id="version" value="" /> 
<input type="hidden" name="reportsViewTO.scheduledReportID" id="scheduledReportID" value="" /> 
<input type="hidden" name="reportsViewTO.status" id="status" value="" /> 

</form>

</div>	

<script type="text/javascript">
  	<!--
	var screenId = 'UC_REPM_076';
	top[2].HideProgress();
  	//-->
  </script>
	</body>
</html>