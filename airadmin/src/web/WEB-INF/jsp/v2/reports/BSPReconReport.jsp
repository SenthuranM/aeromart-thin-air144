<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>
	<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>
	<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/jquery.ui.autocomplete.css"/>
	
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/MultiDropDownDup.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  	    	
	<script src="../../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery-1.4.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery-ui-1.8.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript">
 	 var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
 	</script>

  </head>
  <body onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" scroll="no" class="tabBGColor">
	<div id="divBSPReconReport" style="height:725px;text-align:left;background-color:#ECECEC;">
  	<form name="frmBSPReconReport" id="frmBSPReconReport" action="" method="post">
		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblBSPReconReport">
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>	
			<tr>
				<td width="15%" align="left"><font class="fntBold">BSP Country</font></td>
				<td width="5%" align="left"><font class="fntBold"></font></td>
				<td width="15%" align="left"><select name="selBspCountryCode" id="selBspCountryCode" style="width: 80px;">
					    <option value="All">All</option>
						<c:out value="${requestScope.reqBSPCountryList}" escapeXml="false" />
					</select></td>
				<td width="5%" align="left"><font class="fntBold"></font></td>
				<td width="60%" align="left"><font class="fntBold"></font></td>
					
			</tr>
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>					
			<tr>
				<td width="15%" align="left"><font class="fntBold">Transaction Date</font></td>
				<td width="5%" align="right"><font class="fntBold">From</font></td>
				<td width="15%" align="left">
					<input name="tnxDateFrom" type="text" id="tnxDateFrom" size="10" maxlength="10">
					<font class="mandatory"><b>*</b></font>
				</td>
				<td width="5%" align="right"><font class="fntBold">To</font></td>
				<td width="60%" align="left">
					<input name="tnxDateTo" type="text" id="tnxDateTo" size="10"  maxlength="10">
					<font class="mandatory"><b>*</b></font>
				</td>			
			</tr>
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>		
			<tr>
				<td width="15%" align="left"><font class="fntBold">DPC Processed Date</font></td>
				<td width="5%" align="right"><font class="fntBold">From</font></td>
				<td width="15%" align="left"><input name="dpcProcessedDateFrom" type="text" id="dpcProcessedDateFrom" size="10" maxlength="10"></td>
				<td width="5%" align="right"><font class="fntBold">To</font></td>
				<td width="60%" align="left"><input name="dpcProcessedDateTo" type="text" id="dpcProcessedDateTo" size="10"  maxlength="10"></td>			
				
			</tr>
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			<tr>
				<td width="15%" align="left"><font class="fntBold">Transaction Type</font></td>
				<td width="5%" align="left"><font class="fntBold"></font></td>
				<td width="15%" align="left"><select name="selBspTnxType" id="selBspTnxType" style="width: 80px;">
						<option value="">All</option>
						<c:out value="${requestScope.reqBSPTnxTypesList}" escapeXml="false" />
					</select></td>
				<td width="5%" align="left"><font class="fntBold"></font></td>
				<td width="60%" align="left"><font class="fntBold"></font></td>	
			</tr>
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>	
			<tr>
				<td width="15%" align="left"><font class="fntBold">Reconcilied</font></td>
				<td width="5%" align="left"><font class="fntBold"></font></td>
				<td width="15%" align="left"><select name="selReconStatus" id="selReconStatus" style="width: 80px;">
						<option value="">All</option>
						<option value="Y">Yes</option>
						<option value="N">No</option>
					</select></td>
				<td width="5%" align="left"><font class="fntBold"></font></td>
				<td width="60%" align="left"><font class="fntBold"></font></td>		
			</tr>	
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>	
			<tr>
				<td width="15%" align="left"><font class="fntBold">Entity</font></td>
				<td width="5%" align="left"><font class="fntBold"></font></td>
				<td width="15%" align="left"><select name="selEntity" id="selEntity" style="width: 100px;">
						<c:out value="${requestScope.reqEntities}" escapeXml="false" />
					</select></td>
				<td width="5%" align="left"><font class="fntBold"></font></td>
				<td width="60%" align="left"><font class="fntBold"></font></td>		
			</tr>				
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>	
			<tr>				
				<td><font class="fntBold">Output Option</font></td>
			</tr>	
			<tr>
				<td colspan="5">
					<table  width="100%" border="0" cellpadding="0" cellspacing="2">
						<tr>
							<td width="18%"><input type="radio" name="radReportOption" id="radReportOption"
								value="HTML" class="noBorder"  checked><font>HTML</font></td>
							<td width="18%"><input type="radio" name="radReportOption" id="radReportOption" 
								value="PDF" class="noBorder"><font>PDF</font></td>
							<td width="18%"><input type="radio" name="radReportOption" id="radReportOption"
								value="EXCEL" class="noBorder"><font>EXCEL</font></td>					
							<td width="46%"><input type="radio" name="radReportOption" id="radReportOption" 
								value="CSV" class="noBorder"><font>CSV</font></td>
						</tr>									
					</table>
				</td>
			</tr>								
			<tr>
				<td colspan="5">
					<input name="btnClose" type="button" class="Button" id="btnClose" value="Close">
				</td>
				<td align="right">
					<input name="btnView" type="button" class="Button" id="btnView" value="View">
				</td>
			</tr>
		</table>
		<input type="hidden" name="hdnLive" id="hdnLive" value="">
		<input type="hidden" name="hdnEntityText" id="hdnEntityText" value="">
	</form>
	</div>

  </body>
	<script type="text/javascript">
	<!--
		var arrError = new Array();
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
		top[2].HideProgress();
  	//-->
  	</script>
  	<script type="text/javascript" src="../../js/v2/reports/BSPReconReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

</html>