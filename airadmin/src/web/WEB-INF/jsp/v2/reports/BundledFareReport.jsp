<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>
	<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>
	<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/jquery.ui.autocomplete.css"/>
	
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/MultiDropDownDup.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  	    	
	<script src="../../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery-1.4.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery-ui-1.8.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	
	<script type="text/javascript">
		var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
    </script>

  </head>
  <body onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" scroll="no" class="tabBGColor">
   
	<div id="divBundledFareReport" style="height:725px;text-align:left;background-color:#ECECEC;">
  	<form name="frmBundledFareReport" id="frmBundledFareReport" action="" method="post">
		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblBundledFareReport">
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			<tr>
				<td colspan="4">
					<table>
						<tr>
							<td> <font class="fntBold">Report Option</font> </td>
						</tr>
						<tr>													
							<td><input type="radio" id="radOptionSummary" name="radOption" value="SUMMARY" checked="checked"> <font> Summary</font></td>
							<td><input type="radio" id="radOption" name="radOption" value="DETAIL"> <font> Detail</font></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>		
			<tr>
				<td colspan="4">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="20%" align="left"><font class="fntBold">Sales Date From</font></td>
							<td width="20%" valign="top">
								<input name="salesDateFrm" type="text" id="salesDateFrm" size="10" style="width:80px;" maxlength="10">
								<font class="mandatory"><b>*</b></font>
							</td>
							<td width="5%" align="left"><font class="fntBold">&nbsp;&nbsp;&nbsp;To</font></td>
							<td valign="top">
								<input name="salesDateTo" type="text" id="salesDateTo" size="10" style="width:80px;" maxlength="10">
								<font class="mandatory"><b>*</b></font>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>		
			<tr>
				<td colspan="4">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="20%" align="left"><font class="fntBold">Flight Date From</font></td>
							<td width="20%" valign="top"><input name="flightDateFrm" type="text" id="flightDateFrm" size="10" style="width:80px;" maxlength="10"></td>
							<td width="5%" align="left"><font class="fntBold">&nbsp;&nbsp;&nbsp;To</font></td>
							<td valign="top"><input name="flightDateTo" type="text" id="flightDateTo" size="10" style="width:80px;" maxlength="10"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>		
			<tr>
				<td width="8%" valign="top"  align="left"><font class="fntBold">Sector</font></td>
				<td width="35%" valign="top">
					<table width="95%" border="0" cellpadding="0" cellspacing="0" >
						<tr>
							<td colspan="2"><font>From</font><br/>
								<select id="selFrom_1" class="portSel">
									<option value="" selected="selected"></option>
									<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
								</select>
							</td>
							
							<td colspan="2"><font>To</font><br/>
								<select id="selTo_1" class="portSel">
									<option value="" selected="selected"></option>
									<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
								</select>
							</td>							
						</tr>
						<tr>
							<td><font>via 1</font><br/>
								<select id="selVia1" class="portSel">
									<option value="" selected="selected"></option>
									<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
								</select>
								
							</td>
							<td><font>via 2</font><br/>
								<select id="selVia2" class="portSel">
									<option value="" selected="selected"></option>
									<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
								</select>
								
							</td>
							<td><font>via 3</font><br/>
								<select id="selVia3" class="portSel">
									<option value="" selected="selected"></option>
									<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
								</select>
								
							</td>
							<td><font>via 4</font><br/>
								<select id="selVia4" class="portSel">
									<option value="" selected="selected"></option>
									<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
								</select>
								
							</td>
							<td><br/>
								<input type="button" id="add_Routes" value=" + " class="iconBtn addItem"/>
							</td>
						</tr>
						<tr>
							<td colspan="4">
								<select size="10" id="sel_Routes" style="height:75px;width:98%" multiple="multiple"></select>
							</td>
							<td valign="top"><input type="button" id="del_Routes" value=" - " class="iconBtn removeItem" /></td>
						</tr>
					</table>
				</td>
			</tr>	
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			<tr>
				<td align="left" colspan="4"><font class="fntBold">Channel Inclusion</font></td>
			</tr>
			<tr>
				<td align="left" colspan="4"><span id="spn6">&nbsp; </span></td>
			</tr>	
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			<tr>
				<td align="left" colspan="4"><font class="fntBold">Bundled Services</font></td>
			</tr>
			<tr>
				<td align="left" colspan="4"><span id="spnBundledFare">&nbsp; </span></td>
			</tr>
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>	
			<tr>				
				<td><font class="fntBold">Output Option</font></td>
			</tr>
				
			<tr>
				<td colspan="4">
					<table  width="100%" border="0" cellpadding="0" cellspacing="2">
						<tr>
							<td width="18%"><input type="radio" name="radReportOption" id="radReportOption"
								value="HTML" class="noBorder"  checked><font>HTML</font></td>
							<td width="18%"><input type="radio" name="radReportOption" id="radReportOption" 
								value="PDF" class="noBorder"><font>PDF</font></td>
							<td width="18%"><input type="radio" name="radReportOption" id="radReportOption"
								value="EXCEL" class="noBorder"><font>EXCEL</font></td>					
							<td width="46%"><input type="radio" name="radReportOption" id="radReportOption" 
								value="CSV" class="noBorder"><font>CSV</font></td>
						</tr>									
					</table>
				</td>
			</tr>								
			<tr>
				<td colspan="3">
					<input name="btnClose" type="button" class="Button" id="btnClose" value="Close">
				</td>
				<td align="right">
					<input name="btnView" type="button" class="Button" id="btnView" value="View">
				</td>
			</tr>
		</table>
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnChannels" id="hdnChannels" value="">
		<input type="hidden" name="hdnBundledFares" id="hdnBundledFares" value="">
		<input type="hidden" name="hdnSectors" id="hdnSectors" value=""/>
		<input type="hidden" name="hdnReportView" id="hdnReportView" value="">
		<input type="hidden" name="hdnLive" id="hdnLive" value="">
	</form>
	</div>

  </body>
	<script type="text/javascript">
	<!--
		var arrError = new Array();
		var stns = new Array();
 		var bundledFaresArr = new Array(); 
 		var chnArr = new Array();
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
		<c:out value="${requestScope.reqBundledFares}" escapeXml="false" />
		<c:out value="${requestScope.reqChannels}" escapeXml="false" />
		top[2].HideProgress();
  	//-->
  	</script>
  	<script type="text/javascript" src="../../js/v2/reports/common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  	<script type="text/javascript" src="../../js/v2/reports/BundledFareReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

</html>
