<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css"/>
	<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css"/>
	<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/jquery.ui.autocomplete.css"/>
	
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>	
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>  	    	
	<script src="../../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery-1.4.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery-ui-1.8.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
      <script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	
	<script type="text/javascript">
		<!--
		var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
		var modDetailsEnabled = "<c:out value="${requestScope.reqModDetailsEnabled}" escapeXml="false" />";
		var repShowpay = "<c:out value="${requestScope.reqSowPay}" escapeXml="false" />";
		var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			var selectBox;
			var displayAgencyMode = 0;
			<c:out value="${requestScope.reqHtmlDetails}" escapeXml="false" />
			var systemDate = "<c:out value="${requestScope.systemDate}" escapeXml="false" />";
			var offlineReportParams = "<c:out value="${requestScope.offlineReportParams}" escapeXml="false" />";
			
	-->
    </script>

  </head>
  <body oncontextmenu="return false"  scroll="no" class="tabBGColor">
   
	<div id="divPaxStatusReport" style="height:625px;text-align:left;background-color:#ECECEC;">
  	<form name="frmPage" id="frmPage" action="" method="post">
		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblPaxStatusReport">
            <tr><td><label class="fntSmall">&nbsp;</label></td></tr>
            <tr>
                <td width="1%"></td>
                <td width="97%">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
						<td width="100%"><font class="fntBold" colspan="8">Departure Date Range</font></td>
						
					</tr>
					<tr>
						<td colspan="7">
							<table width="100%" border="0" cellpadding="0" cellspacing="2">	
								<tr><td width="10%" align="left"><label class="fntBold">From </label></td>
									<td width="15%" align="left"><input name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onBlur="dateChk('txtFromDate')" invalidText="true">
									<label class="mandatory"><b>*</b></label>
									</td>
									<td width="10%"><label class="fntBold">To </label></td>
									<td width="15%" align="left"><input	name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onBlur="dateChk('txtToDate')" invalidText="true">
									<label class="mandatory"><b>*</b></label>
									</td>
									<td width="30%"><input type="checkbox" name="chkLocalTime" id="chkLocalTime" value="on" class="noBorder" align="left"><font>In Local Time</font></td>
									<td width="10%"></td>
									<td width="10%"></td>
								</tr>
							</table>
						</td>
					<td></td>
					</tr>	
             		<tr>
						<td width="100%"><font class="fntBold" colspan="8">Booked Date Range</font></td>
					</tr>
					<tr>
						<td colspan="7">
							<table width="100%" border="0" cellpadding="0" cellspacing="2">	
								<tr><td width="10%" align="left"><label class="fntBold">From</label></td>
									<td width="15%" align="left"><input name="txtBookFromDate" type="text" id="txtBookFromDate" size="10" style="width:75px;" maxlength="10" onBlur="dateChk('txtBookFromDate')" invalidText="true"></td>
									<td width="10%"><label class="fntBold">To </label></td>
									<td width="15%" align="left"><input	name="txtBookToDate" type="text" id="txtBookToDate" size="10" style="width:75px;" maxlength="10" onBlur="dateChk('txtBookToDate')" invalidText="true"></td>
									<td width="30%"></td>
									<td width="10%"></td>
									<td width="10%"></td>
									
								</tr>
							</table>
						</td>
					<td></td>
					</tr>
                    <tr><td colspan="8"><label class="fntSmall">&nbsp;</label></td></tr>
                    <tr>
                    	<td colspan="7">
	                    	<table width="100%" border="0" cellpadding="0" cellspacing="2">	
		                        <td width="10%" align="left"><label class="fntBold">Flight Numbers</label></td>
		                        <td valign="top" width="15%">
		                            <input name="flightNo" type="text" id="flightNo" size="10" style="width:100px;" maxlength="100"
		                            title="Enter Comma Seperated Values for Multiple Entries">
		                        </td>
		                        <td align="left" width="10%"><label class="fntBold">PFS Status</label></td>
		                        <td valign="top" width="15%">
		                            <select id="pfsStatus" style="width:80%;" name="pfsStatus">
		                                <c:out value="${requestScope.reqPfsStatusList}" escapeXml="false" />
		                            </select>
		                        </td>
		                        <td align="left"><label class="fntBold" width="10%">Booking Class</label></td>
		                        <td valign="top" width="15%">
		                            <select id="bookingClass" style="width:80%;" name="bookingClass">
		                                <option value="All" selected="selected">All</option>
		                                <c:out value="${requestScope.reqBCList}" escapeXml="false" />
		                            </select>
		                            <label class="mandatory"><b></b></label>
		                        </td>
		                        <td width="10%"></td>
								<td width="10%"></td>
	                        </table>
                        </td><td></td>
                    </tr>
                    <tr><td colspan="8"><label class="fntSmall">&nbsp;</label></td></tr>
                    <tr>
	                    <td colspan="7">
			                    <table width="100%" border="0" cellpadding="0" cellspacing="2">	
			                        <td align="left"><label class="fntBold" width="10%">Fare Basis</label></td>
			                        <td valign="top" width="15%">
			                            <select id="fareBasis" style="width:80%;" name="fareBasis">
			                                <option value="All" selected="selected">All</option>
			                                <c:out value="${requestScope.reqBasisList}" escapeXml="false" />
			                            </select>
			                        </td>
			                        <td align="left"><label class="fntBold" width="10%">Fare Rule</label></td>
			                        <td valign="top" width="15%">
			                            <select id="fareRule" style="width:80%;" name="fareRule">
			                                <option value="All" selected="selected">All</option>
			                                <c:out value="${requestScope.reqFareClasses}" escapeXml="false" />
			                            </select>
			                        </td>
			                        <td width="25%"></td>
									<td width="25%"></td>
		                        </table>
	                        </td><td></td>
                    </tr>
                    <tr><td colspan="8"><label class="fntSmall">&nbsp;</label></td></tr>
                    <tr>
                    	<td colspan="7">
			            	 <table width="100%" border="0" cellpadding="0" cellspacing="2">	
		                        <td width="10%"><label class="fntBold">OnD(Flight)</label> </td>
		                        <td width="90%" colspan="2">
		                            <table width="auto" border="0" cellpadding="0" cellspacing="0">
		                                <tr>
		                                    <td width="40%">
		                                        <label>Origin</label><br/>
		                                        <select id="origin" class="portSel" name="origin">
		                                            <option value="" selected="selected"></option>
		                                            <c:out value="${requestScope.reqAirportList}" escapeXml="false" />
		                                        </select>
		                                    </td>
		                                    <td width="1%"></td>
		                                    <td width="40%">
		                                        <label>Destination</label><br/>
		                                        <select id="destination" class="portSel" name="destination">
		                                            <option value="" selected="selected"></option>
		                                            <c:out value="${requestScope.reqAirportList}" escapeXml="false" />
		                                        </select>
		                                    </td>
		                                </tr>
		                            </table>
		                        </td>
		                        <td></td>
		                        <td></td>
		                        </table>
                        </td><td></td>
                    </tr>
                    <tr><td colspan="8"><label class="fntSmall">&nbsp;</label></td></tr>
                    <tr>
                        <td colspan="8">
                            <div id="divAgencies">
                                <table>
                                    <tr>
                                        <td>
                                            <font>Agencies</font>
                                            <select id="selAgencies" size="1" name="selAgencies" style="width:76px">
                                                <option value=""></option>
                                                <option value="All">All</option>
                                                <c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />
                                            </select>
                                        </td>
                                        <td>
                                            <input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr><td colspan="8"><label class="fntSmall">&nbsp;</label></td></tr>
                    <tr>
                        <td colspan="8">
                            <div id="divAgents">
                                <table>
                                    <tr>
                                        <td>
                                            <label class="fntBold">Agents(Owner)</label>
                                        </td>
                                    </tr>
                                    <tr><td>
                                        <table width="62%" border="0" cellpadding="0" cellspacing="2">
                                            <tr>
                                                <td valign="top" width="2%"><span id="spn1"></span></td>
                                                <td align="left" valign="bottom"> </td></tr>
                                        </table></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr><td colspan="8"><label class="fntSmall">&nbsp;</label></td></tr>
                    <tr><td colspan="8"><label class="fntSmall">&nbsp;</label></td></tr>
                    <tr><td colspan="8"><label class="fntBold">Output Option</label></td></tr>
                    <tr>
                    	<td colspan="7">
			            	 <table width="100%" border="0" cellpadding="0" cellspacing="2">	
		                        <td width="10%"><input type="radio" name="radReportOption" id="radReportOption"	value="HTML"  class="noBorder" checked><label>HTML</label></td>
		                        <td width="10%"><input type="radio" name="radReportOption" id="radReportOptionPDF" value="PDF"  class="noBorder"><label>PDF</label></td>
		                        <td width="10%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL" value="EXCEL"  class="noBorder"><label>EXCEL</label></td>
		                        <td width="70%"><input type="radio" name="radReportOption" id="radReportOptionCSV" value="CSV"  class="noBorder"><label>CSV</label></td>
                        	</table>
                        </td><td></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="left">
                            <input name="btnClose" type="button" class="Button" id="btnClose" value="Close" />
                        </td>
                        <td colspan="4" align="right">
                            <input name="btnView" type="button" class="Button" id="btnView" value="View" />
                        </td>
                    </tr>
                    </table>
                </td>
                <td width="1%"></td>
            </tr>

		</table>
		<input type="hidden" name="hdnAgents" id="hdnAgents" value="">
        <input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnLive" id="hdnLive" value="">

	</form>
	</div>

  </body>
	<script type="text/javascript">
	<!--
		var arrError = new Array();
        <c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
        <c:out value="${requestScope.reqGSAList}" escapeXml="false" />
        top[2].HideProgress();
  	//-->
 
		if (displayAgencyMode == 1) {
			document.getElementById('divAgencies').style.display = 'block';
			document.getElementById('divAgents').style.display = 'block';
		} else if (displayAgencyMode == 2) {
			document.getElementById('divAgents').style.display = 'block';
			document.getElementById('divAgencies').style.display = 'none';
		} else {
			document.getElementById('divAgencies').style.display = 'none';
			document.getElementById('divAgents').style.display = 'none';
		}
	</script>
  	<script type="text/javascript" src="../../js/v2/reports/common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
  	<script type="text/javascript" src="../../js/v2/reports/PaxStatusAndRevenueReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

</html>
