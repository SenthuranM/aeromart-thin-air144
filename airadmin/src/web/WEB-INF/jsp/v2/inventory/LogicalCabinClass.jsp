<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	    <title>Template Page</title>
	    <meta http-equiv="pragma" content="no-cache">
	    <meta http-equiv="cache-control" content="no-cache">
	    <meta http-equiv="expires" content="-1">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">		
		<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
		
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
		
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery.json.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
   	 	<script type="text/javascript" src="../../js/v2/jquery/jquery.jqGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script type="text/javascript" src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	    <script type="text/javascript" src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/v2/jquery/jquery.formfileupload.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	    <script src="../../js/cabinclass/LogicalCabinClass.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		
  	</head>
  	<body style='overflow-y:auto;overflow-x:hidden'>
  	<div id="divSearchPanel" style="height: 50px; text-align: center;background-color:#ECECEC;" align="center">
  	<form method="post" action="loadLogicalCabinClass!searchCabinClass.action" id="frmSearchLogicalCabinClass"> 
  	<script type="text/javascript">			
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />	
				var languageHTML = "<c:out value="${requestScope.reqLanguageList}" escapeXml="false" />";
				<c:out value="${requestScope.reqImgPath}" escapeXml="false" />
				<c:out value="${requestScope.reqLCCImagePrefix}" escapeXml="false" />
				<c:out value="${requestScope.reqWithFlexiImagePrefix}" escapeXml="false" />
	</script>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table9">
	<tr>
	<td class="UCase">Logical CC Code</td>
	<td>
		<select id="selLogicalCabinClass" name="searchParams.logicalCCCode" size="1" style="width:80px" class="UCase">
			<OPTION value="All">All</OPTION>
			<c:out value="${requestScope.reqLogicalCabinClassList}" escapeXml="false" />	
		</select>
	</td>
	<td class="UCase">Cabin Class Code</td>
	<td>
		<select id="selCabinClass" name="searchParams.cabinClassCode" size="1" style="width:80px" class="UCase" >
			<OPTION value="All">All</OPTION>
			<c:out value="${requestScope.reqCabinClassList}" escapeXml="false" />	
		</select>
	</td>
	<td class="UCase">Status</td>
	<td>
		<select name="searchParams.status" id="selStatus" size="1" style="width:105;" class="UCase">
			<option value="All">All</option>
			<option value="ACT">Active</option>
			<option value="INA">In-Active</option>										
		</select>
	</td>
	<td align="right" class="UCase">									
		<input name="btnSearch" type="button" class="Button" id="btnSearch" value="Search">
	</td>
	</tr>				
	
	</table>  	
	</form>
  	</div>
  	<div id="divResultsPanel" style="height:250px;text-align:left;background-color:#ECECEC;">
		<table id="listCabinClass" class="scroll" cellpadding="0" cellspacing="0"></table>
		<div id="temppager" class="scroll" style="text-align:center;"></div>
		<table>
			<tr>
			<td>	
			<u:hasPrivilege privilegeId="plan.invn.log.cc.add">	
			<input name="btnAdd" type="button" class="Button" id="btnAdd" value="Add">			
			</u:hasPrivilege>
			<u:hasPrivilege privilegeId="plan.invn.log.cc.edit">	
			<input name="btnEdit" type="button" class="Button" id="btnEdit" value="Edit">	
			</u:hasPrivilege>
			<u:hasPrivilege privilegeId="plan.invn.log.cc.delete">
			<input name="btnDelete " type="button" class="Button" id="btnDelete" value="Delete">		
			</u:hasPrivilege>
			<u:hasPrivilege privilegeId="plan.invn.log.cc.add">
				<input name="btnInternationalize " type="button" class="Button" id="btnInternationalize" value="Translations">		
			</u:hasPrivilege>							
			</td>
			</tr>
		</table>
 	</div>
 	<div id="divDispResultsPanel" style="height: 300px; text-align: center;background-color:#ECECEC;" align="center">
 	<form method="post" action="loadLogicalCabinClass.action" id="frmCabinClass"> 
	<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="tblDispResult">
	<tr>
	<td><font>Logical Cabin Class Code</font></td>
	<td><input name="logicalCCCode" style="width=68px;" type="text" id="logicalCCCode" class="UCase" maxlength="5">
	<font class="mandatory">&nbsp;*</font>
	</td>
	<td width ="35%"><font>Cabin Class</font></td>
	<td width ="15%" align="left">
		<select id="cabinClassCode" name="cabinClassCode" size="1" style="width:80px" class="UCase">
			<OPTION value=""></OPTION>
			<c:out value="${requestScope.reqCabinClassList}" escapeXml="false" />	
		</select>
		<font class="mandatory">&nbsp;*</font>
	</td>
	</tr>
	<tr>
	<td><font>Logical Cabin Class Name</font></td>
	<td>
		<input type="text" name="description" id="description" maxlength="200" size="30"/>
		<font class="mandatory">&nbsp;*</font>
	</td>
	<td width ="35%"><font>Status</font></td>
	<td width ="10%" ><input type="checkbox" name="clsStatus" id="status" value="ACT"></td>
	</tr>
	<tr>
	<td><font>Comment</font></td>
	<td>
	<textarea name="comments" id="comments" rows="3" cols="28"></textarea>
	<font class="mandatory">&nbsp;*</font>
	</td>
	<c:choose>
		<c:when test='${requestScope.reqBundleDescTemplateEnabled == true}'>
			<td width ="35%"><font>Bundle Description Template </font></td>
			<td width ="15%" align="left">
				<select id="bundleDescId" name="bundleDescId" size="1" style="width:80px" class="UCase">
					<OPTION value=""></OPTION>
					<c:out value="${requestScope.reqBundleDescTemplateList}" escapeXml="false" />	
				</select>
			</td>
		</c:when>
	</c:choose>
	</tr>
	<tr><td></td>
	<td><font>(This comment will be shown in IBE/XBE)</font></td>
	</tr>
	<tr>
	<td><font>Rank</font></td>
	<td><input name="nestRank" type="text" id="nestRank" size="30" maxlength="30">
	<font class="mandatory">&nbsp;*</font></td>	
	<td width =35%">
	<font>If already existing, insert and shift existing ranks</font></td>
	<td><input type="checkBox" id="chkNestShift" name="chkNestShift" value="ACT"></td>
	<td><input type="text" name="rowNo"  id="rowNo" style="display: none;" />
	</td>
	</tr>
	<tr>
		<td><font>Allow Single Meal Only</font></td>
		<td>
			<input type="checkBox" id="chkAllowSingleMeal" name="allowSingleMealOnly" value="Y"/>
		</td>	
		<td width="35%"><font>Enable Free Seat</font></td>
		<td>
			<input type="checkBox" id="chkEnableFreeSeat" name="freeSeatEnabled" value="Y" 
				title="If checked, seat will be included free of charge for this logical cabin class"/>
		</td>
		<td></td>
	</tr>
	<tr>
		<td width="35%"><font>Enable Free Flexi</font></td>
		<td>
			<input type="checkBox" id="chkEnableFreeFlexi" name="freeFlexiEnabled" value="Y" 
				title="If checked, flexi will be added free of charge for this logical cabin class"/>
		</td>
		<td><font>With Flexi Name</font></td>
		<td>
			<input type="text" name="flexiDescription" id="flexiDescription"
				title="This will be used as the logical cabin class name, if system enabled flexi for this logical cabin class" maxlength="200" size="17"/>
			<font class="mandatory">&nbsp;*</font>
		</td>	
		<td></td>
	</tr>
	<tr>
	<td><font>Flexi Comment</font></td>
	<td>
		<textarea name="flexiComment" id="flexiComment" rows="3" cols="35"
			title='This will be displayed as the comment if the logical cabin class with flexi option is available'></textarea>
	</td>
	<td><font class="mandatory">&nbsp;*</font></td>
	</tr>
	<tr><td></td>
	<td><font>(This comment will be shown in IBE/XBE)</font></td>
	</tr>
	<tr>
	<td><font>IBE Tooltip Images</font></td>
	<td>
		<input name="btnToolTipImage" type="button" id="btnToolTipImage" value="Upload Images" class="Button" style="width:120">
	</td>
	<td><font>With Flexi Images</font></td>
	<td>
		<input name="btnWithFlexiToolTipImage" type="button" id="btnWithFlexiToolTipImage" value="Upload Images" class="Button" style="width:120">
	</td>	
	<td></td>
	</tr>
	<tr>
	<td colspan="3" style="height:42px;"  valign="bottom">
	<input name="btnClose" type="button" class="Button"  id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)">
	<input name="btnReset" type="button" class="Button" id="btnReset" value="Reset">
	</td>
	<td align="right" valign="bottom">
	<input name="btnSave" type="button" class="Button" id="btnSave" value="Save">
	</td>
	</tr>
	</table>
	<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
	<input type="hidden" name="lccVersion"  id="version" style="display: none;" />
	
	<input type="hidden" name="descriptionI18n"  id="descriptionI18n" style="display: none;" />
	<input type="hidden" name="flexiDescriptionI18n"  id="flexiDescriptionI18n" style="display: none;" />
	<input type="hidden" name="flexiCommentI18n"  id="flexiCommentI18n" style="display: none;" />
	<input type="hidden" name="commentsI18n"  id="commentsI18n" style="display: none;" />
	
	<input type="hidden" name="descriptionForDisplay"  id="descriptionForDisplay" style="display: none;" />
	<input type="hidden" name="flexiDescriptionForDisplay"  id="flexiDescriptionForDisplay" style="display: none;" />
	<input type="hidden" name="flexiCommentForDisplay"  id="flexiCommentForDisplay" style="display: none;" />
	<input type="hidden" name="commentsForDisplay"  id="commentsForDisplay" style="display: none;" />
	
	<input type="hidden" name="strLccImageUploads" id="strLccImageUploads"/>
	<input type="hidden" name="strWithFlexiImageUploads" id="strWithFlexiImageUploads"/>
	</form>
 	</div>
 	<div id="imageUploadDialog" style="display: none;">
		<form id="frmImageUpload">
		<table>
			<tr>
				<td>Language :</td>
				<td colspan="2">
					<select id="selImageLanguage"><c:out value="${requestScope.reqLanguageList}" escapeXml="false" /></select>
				</td>
			</tr>
			<tr>
				<td><label id="lblImgUpload">Image :</label></td>
				<td>
					<div id="dvUpLoad"><input type="file" name="fileHOTFile" id="fileHOTFile">&nbsp;</div>
				</td>
				<td align="left" valign="top">&nbsp;&nbsp;<input
					name="btnUpload" type="button" id="btnUpload" value="Upload">
				</td>
			</tr>
			<tr>
				<td valign="top" colspan="3">
					<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" >
						<tr>
							<td><img name="imgLcc" id="imgLcc" src="" alt="">
							</td>
						</tr>
					</table>
				</td>	
			</tr>	
			<tr>
				<td></td>
				<td align="right" colspan="2">
				<input id="btnImageDialogClose" type="button" value="Close" class="btnDefault Button"/></td>
			</tr>
		</table>
		</form>
	</div>
 	<span id="displayData"></span>
 	<script>
		var screenId = 'SC_ADMN_002';
		top[2].HideProgress();
	</script>
  	</body>
</html>