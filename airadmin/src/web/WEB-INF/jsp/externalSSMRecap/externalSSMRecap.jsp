 <%-- 
	 @Author 	: 	Haider 
	 @Date		:	13Oct08
	 @Copyright : 	ISA
  --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>External SSM Recap</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css">
	
	<link rel="stylesheet" media="all" type="text/css" href="../../css/jquery.ui_no_cache.css" />
	<link rel="stylesheet" media="all" type="text/css" href="../../css/timepicker/jquery-ui-timepicker-addon.css" /> 
	
	<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
 	<script type="text/javascript" src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
    <script type="text/javascript" src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
    <script type="text/javascript" src="../../js/v2/isalibs/isa.commonError.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" ></script>
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/common/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="../../js/v2/common/jquery-ui-sliderAccess.js"></script>
	<script type="text/javascript" src="../../js/externalSSMRecap/externalSSMRecap.js"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<%@ include file='../v2/common/pageHD.jsp'%>
  </head>

  <body class="tabBGColor">

  <form method="post">

			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						Schedule External SSM Recap 
						<%@ include file="../common/IncludeFormHD.jsp"%>			
							<br>
                        <table width="60%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
                           <tr style="margin: 4px"> 
						    <td><font>Schedule Date</font></td>
							<td>
							   <input type="text" value="" id="scheduledDate" name="scheduledDate"	 style="margin: 4px"/>
							</td>
						</tr>
												
						<tr> 
						    <td><font>Schedule Time</font></td>
							<td>
							   <input type="text" value="" id="scheduledTime" name="scheduledTime" style="margin: 4px" onfocus="removeNow()"  />
							</td>
						</tr>						
						
						<tr> 
						<td>
						<input type="radio" name="selectedScheduleRange" id="allSchedulesRadio" value="all" selected="true" style="margin: 4px" onclick="onClickAllSchedules()"> <font>All Schedules</font>
						</td>											
						</tr>
																		
						<tr> 						
                        <td>
                        <input type="radio" name="selectedScheduleRange" id="selectedScheduleRadio" value="selected" style="margin: 4px" onclick="onClickSelectedSchedules()"> <font>Select Range</font>		
                        </td>
                        </tr>  
                                        						
                        <tr> 
                        <td><font>From Date:</font></td>
							<td title="From Date Here"><input type="text" id="scheduleStartDate" name="scheduleStartDate" style="margin: 4px" ></td>							
                        <td><font>To Date:</font></td>
							<td title="To Date Here"><input type="text" id="scheduleEndDate" name="scheduleEndDate" style="margin: 4px" ></td>							                  										
						</tr>
						
						<tr>
						<td>
						<input type="radio" name="selectedEmail" id="emailRadio" value="email" selected="true" style="margin: 4px" onclick="onClickEmail()"> <font>Email</font>
						</td>
						</tr>
						<tr>
						<td>
                        <input type="radio" name="selectedSITA" id="sitaRadio" value="sita" style="margin: 4px" onclick="onClickSita()"> <font>Sita</font>		
                        </td>											
						</tr>
						<tr>
						<td><font>Email</font></td> 						
                        <td>
                        <input type="text" name="emailAddress" id="emailAddress" style="margin: 4px">	
                        </td>
                        </tr> 
                        
						<tr>
						<td><font>SITA Address</font></td> 						
                        <td>
                        <input type="text" name="sitaAddress" id="sitaAddress" style="margin: 4px">	
                        </td>
                        </tr> 							
											
						<tr> 
						<td>						
						<input type="button" value ="Schedule" id = "btnScheduleRecap" class="Button" onclick="onClickBtnSchedule()"> 
						</td>						
						</tr>
                        </table>
							  <%@ include file="../common/IncludeFormBottom.jsp"%>
						 </td>
					</tr>
				  <tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
		<script>
		//	fillCombos();
		</script>
	</form>	
  </body>
  
</html>