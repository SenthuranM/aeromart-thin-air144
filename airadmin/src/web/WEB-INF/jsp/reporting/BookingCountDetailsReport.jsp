<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Booking Count Details Report</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">	
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
<script type="text/javascript">
	//var stns = new Array();  
	//var agentsArr = new Array();
	//var repLive = "<c:out value="${requestScope.hdnLive}" escapeXml="false" />";
	//var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	//<c:out value="${requestScope.reqClientMessages}" escapeXml="false" /> 
	
	//<c:out value="${requestScope.reqGSAList}" escapeXml="false" />
	//<c:out value="${requestScope.reqAgentUserList}" escapeXml="false" />
</script>

</head>
<body class="tabBGColor" onunload="beforeUnload()" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" scroll="no">
<form method="post" id="frmBookingCountDetailsReport" name="frmBookingCountDetailsReport">

<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">

	<tr>
		<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
	</tr>
	
	<tr>
		<td>
			<%@ include file="../common/IncludeFormTop.jsp"%>Booking Count Details<%@ include file="../common/IncludeFormHD.jsp"%>
			<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblHead">
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			<tr></tr>
				
			<tr>					
				<td width="100%">
					<table width="100%" border="0" cellpadding="0" cellspacing="2">	
						<tr><td width="15%" align="left" colspan="2"><font class="fntBold">From</font></td><td width="10%"><input
								name="txtBookingFromDate" type="text" id="txtBookingFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtBookingFromDate')"></td>
							<td width="15%" align="left">&nbsp;&nbsp;<a href="javascript:void(0)"
					onclick="LoadCalendar(0,event); return false;" title="Date From"><img
					SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory">&nbsp;*</font></td>
							<td width="20%" colspan="2"><font class="fntBold">To &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><input
								name="txtBookingToDate" type="text" id="txtBookingToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtBookingToDate')"></td>
							<td width="20%"><a href="javascript:void(0)"
					onclick="LoadCalendar(1,event); return false;" title="Date To"><img
					SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory">&nbsp;*</font></td>
							<td width="10%"></td>
						</tr>
					</table>
				</td>
			</tr>	
			<tr><td>&nbsp;</td></tr>
			<tr>
			<td>
			<table width="70%" border="0" cellpadding="0" cellspacing="2">
			  <tr>
				<td>
					<table width="50%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td>
								<font class="fntBold">Agencies</font>
									<select id="selAgencies" size="1" name="selAgencies" onClick="clickAgencies()" onChange="changeAgencies()" style="width:76px">
									<option value=""></option>
									<option value="All">All</option>
									<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />					
								</select>
							</td>
							<td>
								<input type="checkbox" name="chkTAs" id="chkTAs" class="noborder"><font>With reporting TA's</font>					
							</td>
							<td>
								<input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()"><!-- <font class="mandatory"><b>&nbsp;*</b></font>  --> 
							</td>
						</tr>
						<tr>
							<td><font>&nbsp;</font></td>
							<td>
								<input type="checkbox" name="chkCOs" id="chkCOs" class="noborder"><font>With reporting CO's</font>
							</td>												
						</tr>
				   </table>
			     </td>
			  </tr>			
			  <tr>
				<td>
					<div id="divAgents">
						<table>
							<tr>
								<td>
									<font class="fntBold">Agents</font>
								</td>
							</tr>
							<tr><td>
							<table width="62%" border="0" cellpadding="0" cellspacing="2">	
							<tr>
								<td valign="top" width="2%"><span id="spn1"></span></td>
								<td altgn="left" valign="bottom"> <font class="mandatory"><b>*</b></font></td></tr>
								</table></td>
							</tr>						
						</table>
					</div>
				</td>
			  </tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</table>
			</td>
			</tr>
			
			<tr>				
				<td><font class="fntBold">Output Option</font></td>							
			</tr>
			<tr>
				<td><table  width="100%" border="0" cellpadding="0" cellspacing="2"><tr>
				<td width="15%"><input type="radio" name="radReportOption" id="radReportOption"
					value="HTML" class="noBorder"  checked><font>HTML</font></td>
				<td width="15%"><input type="radio" name="radReportOption" id="radReportOption" 
					value="PDF" class="noBorder"><font>PDF</font></td>
				<td width="15%"><input type="radio" name="radReportOption" id="radReportOption"
					value="EXCEL" class="noBorder"><font>EXCEL</font></td>
				<td><input type="radio" name="radReportOption" id="radReportOption" 
					value="CSV" class="noBorder"><font>CSV</font></td>
				</tr>									
				</table>
				
				</td>									
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>				
				<td width=90%><input type="button" id="btnClose"  name="btnClose" value="Close" class="Button" onclick="closeClick()"></td>
				<td width=10% align="right"><input type="button" id="btnView"  name="btnView" value="View" class="Button" onclick="viewClick()"></td>	
			</tr>
				
			</table>
			<script type="text/javascript">
				<c:out value="${requestScope.reqGSAList}" escapeXml="false" />
				//<c:out value="${requestScope.reqAgentUserList}" escapeXml="false" />
		    </script>
			<%@ include file="../common/IncludeFormBottom.jsp"%>
		</td>
	</tr>
</table>
	<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
	<input type="hidden" name="hdnAgents" id="hdnAgents" value="">
	<input type="hidden" name="hdnUsers" id="hdnUsers" value="">
	<input type="hidden" name="hdnReportView" id="hdnReportView">
	<input type="hidden" name="hdnLive" id="hdnLive">
	<input type="hidden" name="hdnSegments" id="hdnSegments" value="">
	
</form>
</body>
<script src="../../js/reports/BookingCountDetailsReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>

<script type="text/javascript">
	var objProgressCheck = setInterval("ClearProgressbar()", 300);
	function ClearProgressbar(){
	   clearTimeout(objProgressCheck);
	   top[2].HideProgress();
	}
</script>

</html>