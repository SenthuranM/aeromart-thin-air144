<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Airport Tax Report</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">	
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript">
	var stns = new Array();  
	var agentsArr = new Array();
	var repLive = "<c:out value="${requestScope.hdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	
</script>
</head>
<body class="tabBGColor" onunload="beforeUnload()" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" scroll="no">
<form action="" method="post" id="frmAirportTaxReport"
	name="frmAirportTaxReport">
	
	<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">	
		
		<tr>
			<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
		</tr>
		<tr>
			
      <td> 
        <%@ include file="../common/IncludeFormTop.jsp"%>Airport Tax<%@ include file="../common/IncludeFormHD.jsp"%>
        <table width="100%" border="0" cellpadding="0" cellspacing="2"
					ID="tblHead">
				<tr><td><font class="fntSmall">&nbsp;</font></td>
				</tr>	
				<tr>
					<td colspan='3'>
						<table width="70%" border="0" cellpadding="0" cellspacing="0">
					 		<tr>
								<td><font class="fntBold">O &amp; D</font></td>					
								<td><font>Departure</font></td>
								<td>	
									<select name="selDeparture" size="1" id="selDeparture" style="width:55px;">
										<option value=""></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
									</select>
								</td>
								<td><font>Arrival</font></td>
								<td>	
									<select name="selArrival" size="1" id="selArrival" style="width:55px;">
										<option value=""></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
									</select>
								</td>
								<td><font>&nbsp;</font></td>
								<td colspan="2" rowspan="3">
									<select id="selSegment" name="selSegment" multiple size="1" style="width:200px;height:85px" class="NoBorder">
									</select>
								</td>
							</tr> 
							<tr> 
								<td><font>&nbsp;</font></td>								
								<td><font>Via 1</font></td>
								<td>	
									<select name="selVia1" size="1" id="selVia1" style="width:55px;">
										<option value=""></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
									</select>
								</td>
								<td><font>Via 2</font></td>
								<td>	
									<select name="selVia2" size="1" id="selVia2" style="width:55px;">
										<option value=""></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
									</select>
								</td>
								<td align="center"><a href="javascript:void(0)" onclick="addToList()" title="Add to list"><img src="../../images/AA115_no_cache.gif" border="0"></a></td>
							</tr> 
							<tr> 
								<td><font>&nbsp;</font></td>							
								<td><font>Via 3</font></td>
								<td>	
									<select name="selVia3" size="1" id="selVia3" style="width:55px;">
										<option value=""></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
									</select>
								</td>
								<td><font>Via 4</font></td>
								<td>	
								<select name="selVia4" size="1" id="selVia4" style="width:55px;">
										<option value=""></option>
										<c:out value="${requestScope.reqAirportList}" escapeXml="false" />
										</select>
								</td>
								<td align="center"><a href="javascript:void(0)" onclick="removeFromList()" title="Remove from list"><img src="../../images/AA114_no_cache.gif" border="0"></a></td> 
							</tr> 							
						</table>
					</td>
				</tr>
				<tr>				
					<td><font class="fntBold">Estimated Departure Time(Local)</font></td>							
				</tr>
					<tr>					
						<td width="100%">
							<table width="100%" border="0" cellpadding="0" cellspacing="2">	
								<tr><td width="15%" align="left" colspan="2"><font>From</font></td><td width="10%"><input
										name="txtDepFromDate" type="text" id="txtDepFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtDepFromDate')"></td>
									<td width="15%" align="left">&nbsp;&nbsp;<a href="javascript:void(0)"
							onclick="LoadCalendar(0,event); return false;" title="Date From"><img
							SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory">&nbsp;*</font></td>
									<td width="20%" colspan="2"><font>To &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><input
										name="txtDepToDate" type="text" id="txtDepToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtDepToDate')"></td>
									<td width="20%"><a href="javascript:void(0)"
							onclick="LoadCalendar(1,event); return false;" title="Date To"><img
							SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory">&nbsp;*</font></td>
									<td width="10%"></td>
								</tr>
							</table>
						</td>
					</tr>					
					<tr>					
						<td width="100%">
							<table width="100%" border="0" cellpadding="0" cellspacing="2">	
								<tr><td width="15%" align="left" colspan="2"><font>Booked From Date</font></td><td width="10%"><input
										name="txtBookFromDate" type="text" id="txtBookFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtBookFromDate')"></td>
									<td width="15%" align="left">&nbsp;&nbsp;<a href="javascript:void(0)"
							onclick="LoadCalendar(2,event); return false;" title="Date From"><img
							SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory">&nbsp;*</font></td>
									<td width="20%" colspan="2"><font>Booked To Date</font><input
										name="txtBookToDate" type="text" id="txtBookToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtBookToDate')"></td>
									<td width="20%"><a href="javascript:void(0)"
							onclick="LoadCalendar(3,event); return false;" title="Date To"><img
							SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a></td>
									<td width="10%"></td>
								</tr>
							</table>
						</td>
					</tr>															
					
				<tr>				
					<td><font class="fntBold">Output Option</font></td>							
				</tr>
				<tr>
					<td><table  width="100%" border="0" cellpadding="0" cellspacing="2"><tr>
					<td width="15%"><input type="radio" name="radReportOption" id="radReportOption"
						value="HTML" class="noBorder"  checked><font>HTML</font></td>
					<td width="15%"><input type="radio" name="radReportOption" id="radReportOption" 
						value="PDF" class="noBorder"><font>PDF</font></td>
					<td width="15%"><input type="radio" name="radReportOption" id="radReportOption"
						value="EXCEL" class="noBorder"><font>EXCEL</font></td>
					<td><input type="radio" name="radReportOption" id="radReportOption" 
						value="CSV" class="noBorder"><font>CSV</font></td>
				</tr>									
				</table>
				
				</td>									
			</tr>
			<tr><td>&nbsp;</td></tr>	
			<tr>				
				<td width=90%><input type="button" id="btnClose"  name="btnClose" value="Close" class="Button" onclick="closeClick()"></td>
				<td width=10% align="right"><input type="button" id="btnView"  name="btnView" value="View" class="Button" onclick="viewClick()"></td>	
			</tr>		
		</table>
		<%@ include file="../common/IncludeFormBottom.jsp"%></td>
	</tr>
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
</table>
	<input type="hidden" name="hdnMode" id="hdnMode">
	<input type="hidden" name="hdnReportView" id="hdnReportView">
	<input type="hidden" name="hdnLive" id="hdnLive">
	<input type="hidden" name="hdnSegments" id="hdnSegments" value="">
	
</form>
<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
</body>
<script src="../../js/reports/AirportTaxReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript">
   <!--
   <c:out value="${requestScope.reqAgentStation}" escapeXml="false" />
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
    
   //-->
  </script>
</html>