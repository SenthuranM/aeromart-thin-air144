 <%-- 
	 @Author 	: 	Chamindap
  --%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	var priviSearchFullAllowed = "<c:out value="${requestScope.priviSearchFullAllowed}" escapeXml="false" />";
	var displayAgencyMode = 0;	
	var currentAgentCode = "<c:out value="${requestScope.currentAgentCode}" escapeXml="false" />";
	var gsaV2Enabled= "<c:out value="${requestScope.reqGSAStructureV2Enabled}" escapeXml="false" />";

	<c:out value="${requestScope.reqHtmlDetails}" escapeXml="false" />


</script>
  </head>
  <body class="tabBGColor" onbeforeunload="beforeUnload()" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" scroll="no">
  	<form name="frmAgenTransDetails" id="frmAgenTransDetails" method="post">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">  		
		 <script type="text/javascript">
		</script>

		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Agent/GSA Transaction Details<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
					  		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
						
							<tr>							
								<td>
									<div id="divAgencies">
										<table width="50%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td>
													<font class="fntBold">Agencies</font>
														<select id="selAgencies" size="1" name="selAgencies" onClick="clickAgencies()" onChange="changeAgencies()" style="width:76px">
														<option value=""></option>
														<option value="All">All</option>
														<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />					
													</select>
												</td>
												<td>
													<input type="checkbox" name="chkTAs" id="chkTAs" class="noborder"><font>With reporting TA's</font>					
												</td>
												<td>
													<input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()">
												</td>
											</tr>
											<tr>
												<td><font>&nbsp;</font></td>
												<td>
													<input type="checkbox" name="chkCOs" id="chkCOs" class="noborder"><font>With reporting CO's</font>
												</td>												
											</tr>
										</table>
									</div>
								</td>
							</tr>

							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									<div id="divAgents">
										<table>
											<tr>
												<td>
													<font class="fntBold">Agents</font>
												</td>
											</tr>
												
											<tr>
												<td>
													<table width="62%" border="0" cellpadding="0" cellspacing="2">	
														<tr>
															<td valign="top" width="2%"><span id="spn1"></span></td>
															<td altgn="left" valign="bottom"> <font class="mandatory"><b>*</b></font></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
								</td>
							</tr>						
								
								<tr>
									<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="20%"><font>Transactions From</font></td>
										<td width="55%"><input type="text" name="txtTransFrom" id="txtTransFrom" size="10" onBlur="settingValidation('txtTransFrom')" maxlength="10">
											<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Created Date From"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"><b>*</b></font></td>			
										<td width="20%"></td>	
										<td></td>	
										</tr>
										</table>
									</td>	
								</tr>
								<tr>
									<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="20%"><font>Transactions To</font></td>
										<td width="55%"><input type="text" name="txtTransTo" id="txtTransTo" size="10" onBlur="settingValidation('txtTransTo')" maxlength="10">
											<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Created Date To"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"><b>*</b></font></td>			
										<td width="20%"></td>	
										<td></td>	
										</tr>
										</table>
									</td>	
								</tr>	
								<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
					  				<table width="100%" border="0" cellpadding="0" cellspacing="2">
										<tr>
											<td width="20%"><font>Report Option</font></td>
											
											<td width="55%"><select id="selRepOption" name="selRepOption" onchange="changeOption()">
													<option value="Summary">Summary Report</option>
													<u:hasPrivilege privilegeId="rpt.ta.atx.all">
													<option value="AgentDetail">Detail Report</option> 
													</u:hasPrivilege>
												</select>
												<font class="mandatory"><b>*</b></font>
											</td>
											<td width="20%"></td>	
											
										</tr>
									</table>
					  			</td>
							</tr>
								<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									<table width="100%" border="0" cellpadding="0" cellspacing="2">
										<tr>
											<td width="20%"><font>Transactions Type</font></td>
											<td>
  												<input name="chkTypeR" id="chkTypeR" maxlength="1" type="checkbox" value="R" title="Reservation"  class="noBorder">
  													<font>R</font>
  												<input name="chkTypeC" id="chkTypeC" maxlength="1" type="checkbox" value="C" title="Credit"  class="noBorder">
  													<font>C</font>
  												<input name="chkTypeD" id="chkTypeD" maxlength="1" type="checkbox" value="D" title="Debit"  class="noBorder">
  													<font>D</font>
  												<input name="chkTypeRR" id="chkTypeRR" maxlength="1" type="checkbox" value="RR" title="Reservation Refund"  class="noBorder">
  													<font>RR</font>
  												<input name="chkTypeA" id="chkTypeA" maxlength="1" type="checkbox" value="A" title="Cash"  class="noBorder">
  													<font>CH</font>
  												<input name="chkTypeQ" id="chkTypeQ" maxlength="1" type="checkbox" value="Q" title="Cheque"  class="noBorder">
  													<font>CQ</font>
  												<input name="chkTypeRV" id="chkTypeRV" maxlength="1" type="checkbox" value="RV" title="Adjustment"  class="noBorder">
  													<font>A</font>
  												<input name="chkTypeCG" id="chkTypeCG" maxlength="1" type="checkbox" value="CG" title="Commision Granted" class="noBorder">
  													<font>CG</font>
  												<input name="chkTypeCR" id="chkTypeCR" maxlength="1" type="checkbox" value="CR" title="Commision Removed" class="noBorder">
  													<font>CR</font>	
  												<input name="chkTypeAO" id="chkTypeAO" maxlength="1" type="checkbox" value="AO" title="Sub-Agent Operations (Creation/Credit Limit Adjustment)" class="noBorder">
  													<font>AO</font>												
  												<input name="chkTypeCRTopUp" id="chkTypeCRTopUp" maxlength="1" type="checkbox" value="CRTP" title="Commision Removed" class="noBorder">
  												<input name="chkTypeCRTopUp" id="chkTypeCRTopUp" maxlength="1" type="checkbox" value="CRTP" title="Credit Card" class="noBorder">
  													<font>CC</font>															
  											</td>
  										</tr>
  									</table>
								</td>
							</tr>				
								
							
							
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>				
							<td><font class="fntBold">Output Option</font></td>							
							</tr>
							<tr>
								<td><table width="70%" border="0" cellpadding="0" cellspacing="2"><tr>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
									value="HTML"  class="noBorder" checked><font>HTML</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" 
									value="PDF"  class="noBorder"><font>PDF</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
									value="EXCEL"  class="noBorder"><font>EXCEL</font></td>
								<td width="40%"><input type="radio" name="radReportOption" id="radReportOptionCSV"
									value="CSV"  class="noBorder"><font>CSV</font></td>
								</tr>									
								</table>
								<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
								</td>									
							</tr>
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
									
									<!--input name="btnPrint" type="button" class="Button" id="btnPrint" value="Print" onClick="printClick()"-->
								</td>
								<td align="right">
									<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
								</td>
							</tr>
						</table>
				  		
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
	
<%@ include file="../common/IncludeFormBottom.jsp"%>		
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnAgents" id="hdnAgents" value="">
		<input type="hidden" name="hdnRptType" id="hdnRptType" value="Summary">
		<input type="hidden" name="hdnLive" id="hdnLive" value="">
		<input type="hidden" name="hdnAgentName" id="hdnAgentName" value="">

	</form>
  </body>
	<script src="../../js/reports/AgentTransactionDetailsReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript">
		<c:out value="${requestScope.reqGSAList}" escapeXml="false" />
		var maxDaysForTrans = "<c:out value="${requestScope.reqGSAList}" escapeXml="false" />";
		
				
		if (displayAgencyMode == 1 ) {
			document.getElementById('divAgencies').style.display= 'block';
			document.getElementById('divAgents').style.display= 'block';
		} else if (displayAgencyMode == 2 ) {
			document.getElementById('divAgents').style.display= 'block';
			document.getElementById('divAgencies').style.display= 'none';
		} else {
			document.getElementById('divAgencies').style.display= 'none';
			document.getElementById('divAgents').style.display= 'none';
		}	

	</script>
	<script type="text/javascript">
   	<!--
   		var objProgressCheck = setInterval("ClearProgressbar()", 300);
   		function ClearProgressbar(){
      		clearTimeout(objProgressCheck);
      		top[2].HideProgress();
   		}
    
   //-->
  </script>
</html>

