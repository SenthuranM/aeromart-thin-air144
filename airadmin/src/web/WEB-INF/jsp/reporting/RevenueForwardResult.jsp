<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>revenue Report</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="-1">    
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">

	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script type="text/javascript">
		var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	</script>	
	</head>
	<body oncontextmenu="return false" ondrag="return false" onkeydown="return Body_onKeyDown(event)" scroll="no">
	<%@ include file="../common/IncludeWindowTop.jsp" %><!-- Page Background Top page -->		
		<table width="100%" cellpadding="0" cellspacing="0" border="0" class="PageBorder" ID="Table7" style="height:300px;">
			<tr>
				<td width="30"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
				<td valign="top" align="center" class="PageBackGround">
				<!-- Your Form start here -->
				<form method="post"  id="frmrevRes" name="frmrevRes"action="showRevenueForwardReport.action"  ENCTYPE="multipart/form-data">
				<br>
					<table width="98%" cellpadding="0" cellspacing="0" border="0" ID="Table7">
						<tr>
							<td><font class="Header">Forward Sales Results</font></td>
						</tr>
						<tr><td><font class="fntSmall">&nbsp;</font></td></tr>								
						<tr>
							<td>
								<%@ include file="../common/IncludeFormTop.jsp"%>
									Forward Sales Report<%@ include file="../common/IncludeFormHD.jsp"%>
								<table width="100%" border="0" cellpadding="2" cellspacing="2" ID="Table2">
									<tr>
										<td align="center">											
											<a href="javaScript:openErr('test.csv')">Forward Sales Report</a>
										</td>
									</tr>
									<tr><td align="left"><input type="button" id="btnCancel" value="Close" class="Button" onclick="cancelClick()" name="btnCancel"></td></tr>
								</table>
								<%@ include file="../common/IncludeFormBottom.jsp"%>
							</td>
						</tr>
					</table>								
				</form>
				<INPUT TYPE="hidden" name="fileName">
				<input type="hidden" name="hdnMode" id="hdnMode">
				<input type="hidden" name="hdnLive" id="hdnLive" value="">
				<!-- Your Form ends here -->
			</td>
			<td width="15"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>
		</tr>
	</table>				
	<%@ include file="../common/IncludeWindowBottom.jsp"%><!-- Page Background Bottom page -->
	<script src="../../js/reports/revenueForwardResult.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
</body>
</html>
