<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Lcc Collections</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
</script>
</head>
<body onload="winOnLoad()" class="tabBGColor" onbeforeunload="beforeUnload()" oncontextmenu="return false">
<form action="" method="post" id="frmLccCol" name="frmLccCol">
<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeFormTop.jsp"%>Payable / Receivable Report<%@ include file="../common/IncludeFormHD.jsp"%>
		<table width="100%" border="0" cellpadding="0" cellspacing="2"
			ID="tblHead">
			<tr>
				<td><font class="fntBold">Date Range</font></td>				
			</tr>
			<tr>
			<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="2">	
				<tr><td width="25%" align="left"><font>From &nbsp;&nbsp;</font><input
					name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFromDate')"></td>
				<td width="20%"><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory">&nbsp;*</font></td>
				<td width="20%"><font>To &nbsp;&nbsp;</font><input
					name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtToDate')"></td>
				<td><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"><font class="mandatory">&nbsp;*</font></a></td>
				</tr>
			</table>
			</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
				<td width="20%"><font class="fntBold">Collection Type</font></td>
				<td><select id="selColType" name="selColType">
						<c:out value="${requestScope.reqAirlineWiseColType}" escapeXml="false" />
					</select></td>
				<td>
				<u:hasPrivilege privilegeId="rpt.rev.new.view">
				<font>New View</font>
				</u:hasPrivilege>
				</td>
				<td>
				<u:hasPrivilege privilegeId="rpt.rev.new.view">			
				<input type="checkbox" name="chkNewview" id="chkNewview" value="on" class="NoBorder">
				</u:hasPrivilege>
				</td>
				</tr>				
				</table>
				</td>			
			</tr>
						
			<tr><td>&nbsp;</td></tr>				
			<tr>				
				<td><font class="fntBold">Output Option</font></td>							
			</tr>
			<tr>
				<td><table width="70%" border="0" cellpadding="0" cellspacing="2"><tr>
				<td width="10%"><input type="radio" name="radReportOption" id="radReportOptionHTML"
					value="HTML" class="noBorder" checked="checked"><font>HTML</font></td>
				<td width="10%"><input type="radio" name="radReportOption" id="radReportOptionPDF" 
					value="PDF" class="noBorder"><font>PDF</font></td>
				<td width="10%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
					value="EXCEL" class="noBorder"><font>EXCEL</font></td>
				<td width="40%"><input type="radio" name="radReportOption" id="radReportOptionCSV"
					value="CSV" class="noBorder"><font>CSV</font></td>
				</tr>									
				</table>
				<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />
				</td>									
			</tr>
			<tr><td>&nbsp;</td></tr>	
			<tr>				
				<td width=70%><input type="button" id="btnClose"  name="btnClose" value="Close" class="Button" onclick="closeClick()"></td>
				<td width=30% align="right"><input type="button" id="btnView"  name="btnView" value="View" class="Button" onclick="viewClick()"></td>	
			</tr>		
		</table>
		<%@ include file="../common/IncludeFormBottom.jsp"%></td>
	</tr>
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
</table>
<input type="hidden" name="hdnMode" id="hdnMode" value="Mode"> 
<input type="hidden" name="hdnLive" id="hdnLive" value="">
</form>
</body>
<script src="../../js/reports/LccAndDryCollectionReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress(); 
   }
    
   //-->
  </script>
</html>