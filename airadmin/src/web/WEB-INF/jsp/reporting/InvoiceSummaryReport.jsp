<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<link href="../../themes/default/css/ui.all_no_cache.css" rel="stylesheet" type="text/css" />	
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/v2/schedrept/schedrept.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repShowpay = "<c:out value="${requestScope.reqSowPay}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	<c:out value="${requestScope.reqVarIsTrnInv}" escapeXml="false" />
	var isWeeklyInvoiceEnabled = "<c:out value="${requestScope.isWeeklyInvoiceEnabled}" escapeXml="false" />";
	
</script>
  </head>
  <body class="tabBGColor" onbeforeunload="beforeUnload()" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" scroll="no">
  	<form name="frmInvoiceSummaryReport" id="frmInvoiceSummaryReport" action="" method="post">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">		
		 <table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">				
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Invoice Summary<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">					  		
							<tr>
								<td width="60%"></td>
								<td width="20%"></td>
								<td></td>
							</tr>
							<tr>
							<c:if test="${requestScope.reqIsTrnWiseInv == 'false'}">
									
										<td>
											<table width="62%" border="0" cellpadding="0" cellspacing="2">	
												<tr><td width="10%" align="left"><font>Year &nbsp;&nbsp;&nbsp; </font></td>
												<td width="20%"><select name="selYear" id="selYear" size="1" style="width:100px" title="Year">
												<OPTION value="" selected></OPTION>	
												<c:out value="${requestScope.reqBasePrevious}" escapeXml="false" />
												</select><font class="mandatory"><b>*</b></font></td>
													<td width="7%"><font>Month </font></td>
													<td width="10%" align="left"><select name="selMonth" id="selMonth" size="1" style="width:100px" title="Month">
										<OPTION value=""></OPTION>	
										<OPTION value=01>January</OPTION>
										<OPTION value=02>February</OPTION> 
		                                <OPTION value=03>March</OPTION> 
		                                <OPTION value=04>April</OPTION> 
		                                <OPTION value=05>May</OPTION> 
		                                <OPTION value=06>June</OPTION> 
		                                <OPTION value=07>July</OPTION>
		                                <OPTION value=08>August</OPTION> 
		                                <OPTION value=09>September</OPTION> 
		                                <OPTION value=10>October</OPTION> 
		                                <OPTION value=11>November</OPTION> 
		                                <OPTION value=12>December</OPTION>
									</select><font class="mandatory"><b>*</b></font></td>
												</tr>
											</table>
										</td>
										<td></td>
									 
							</c:if>
							<c:if test="${requestScope.reqIsTrnWiseInv == 'true'}">
								<td>
								<table width="65%" border="0" cellpadding="0" cellspacing="2"><tr><td>
							 
									<font>From&nbsp;</font><input name="txtInvoiceFrom" type="text" id="txtInvoiceFrom" size="10" style="width:80px;" maxlength="10" onblur="dateChk('txtInvoiceFrom')"></td><td align="left">
									<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="From Date"><img src="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>&nbsp;*</b></font></td><td>
									<font>To&nbsp;</font><input name="txtInvoiceTo" type="text" id="txtInvoiceTo" size="10" style="width:80px;" maxlength="10" onblur="dateChk('txtInvoiceTo')"></td><td align="left">
									<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="To Date"><img src="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>&nbsp;*</b></font></td>
									<input type="hidden" id="selYear" name="selYear">
									<input type="hidden" id="selMonth" name="selMonth">
									<input type="hidden" id="selBP" name="selBP">
								</td>
								</tr>
								</table>
								</td>
							</c:if>
							</tr>
							<c:if test="${requestScope.reqIsTrnWiseInv == 'false'}">
								<tr>
								<td><table width="34%" border="0" cellpadding="0" cellspacing="2"><tr>
								<td width="20%"><font>Invoice Period</font></td>
								<td width="10%" align="left">
									<select name="selBP" id="selBP" size="1" style="width:50px" onChange="objOnFocus()">
										<c:out value="${requestScope.reqInvPeriods}" escapeXml="false" />
									</select>
								</td>			
								<td></td>
								</tr>	
								</table>
								</td>
								</tr>
							</c:if>
							<tr>
								<td>
									<table width="75%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td>
												<font class="fntBold">Agencies</font>
													<select id="selAgencies" size="1" name="selAgencies" onClick="clickAgencies()" onChange="changeAgencies()" style="width:76px">
													<option value=""></option>
													<option value="All">All</option>
													<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />					
												</select>
											</td>
											<td>
												<input type="checkbox" name="chkTAs" id="chkTAs" class="noborder"><font>With reporting TA's</font>					
											</td>
											<td>
												<input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()">
											</td>
										</tr>
										<tr>
											<td><font>&nbsp;</font></td>
											<td>
												<input type="checkbox" name="chkCOs" id="chkCOs" class="noborder"><font>With reporting CO's</font>
											</td>												
										</tr>
									</table>
								</td>
							</tr>							
							<tr>
								<td>
									<font class="fntBold">Agents</font>
								</td>
							</tr>
							<tr>
								<td colspan="2" valign="top"><span id="spn1"></span></td>
								<td valign="bottom" ><font class="mandatory"><b>*</b></font></td>
							</tr>
							
							<tr>							
								<td>
								<div id="divPay">
									<table width="60%" border="0" cellpadding="0" cellspacing="2">
										<tr>
											<td><font>Payment Source</font></td>
											<td><select id="selPaySource" name="selPaySource">
													<option value="INTERNAL">Reservation System</option>
													<option value="EXTERNAL">External</option>
													<option value="BOTH">Both</option>  
												</select>
											</td>											
										</tr>
									</table>
									</div>	
								</td>
								<td>
								<table width="40%" border="0" cellpadding="0" cellspacing="2">
								<tr>
								<td>			
									<u:hasPrivilege privilegeId="rpt.ta.comp.base">					
										<font>Include Base Currency</font>
									</u:hasPrivilege>									
								</td>
								<td>						
									<u:hasPrivilege privilegeId="rpt.ta.comp.base">			
										<input type="checkbox" name="chkBase" id="chkBase" value="on" class="NoBorder">
									</u:hasPrivilege>									
								</td>
								<td>
								<u:hasPrivilege privilegeId="rpt.rev.new.view">
								<font>New View</font>
								</u:hasPrivilege>
								</td>
								<td>
								<u:hasPrivilege privilegeId="rpt.rev.new.view">
								<input type="checkbox" name="chkNewview" id="chkNewview" value="on" class="NoBorder">
								</u:hasPrivilege>
								</td>
								</tr>
								</table>
								</td>							
							</tr>
							<tr>
								<td>
									<table width="60%" border="0" cellpadding="0" cellspacing="2">
										<tr>
											<td width="40%">
												<font>Entity</font>
											</td>
											<td>
												<select id="selEntity" size="1" name="selEntity"  style="width:100px">
													<c:out value="${requestScope.reqEntities}" escapeXml="false" />					
												</select>
											</td>
										</tr>
									</table>
								</td>
							</tr>							
							<tr>
								<td><table width="80%" border="0" cellpadding="0" cellspacing="2"><tr>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionHTML"	value="HTML"  class="noBorder" checked><font>HTML</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" value="PDF"  class="noBorder"><font>PDF</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL" value="EXCEL"  class="noBorder"><font>EXCEL</font></td>
								<td><input type="radio" name="radReportOption" id="radReportOptionCSV" value="CSV"  class="noBorder"><font>CSV</font></td>
							</tr>											
						</table>
							<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />
					</td>							
						<tr>
							<td >
									<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
								</td>
								<td align="right" colspan ="2">
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td align="right">
											<u:hasPrivilege privilegeId="rpt.sch.invoicedetail">	
											<div id="divSchedFrom"></div>
											<input name="btnSched" type="button" class="Button schdReptButton" id="btnSched"
												value="Schedule" onClick="viewClick(true)">
											</u:hasPrivilege>
										</td>
										<td align="right">
											<input name="btnExport" type="button" class="Button" id="btnExport" value="Export" onClick="exportClick()">
											<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick(false)">
										</td>
									</tr>
									</table>
								</td>
							</tr>
						</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
			<%@ include file="../common/IncludeFormBottom.jsp"%>
		<script type="text/javascript">
				<c:out value="${requestScope.reqGSAList}" escapeXml="false" />
				
		</script>		
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnAgents" id="hdnAgents" value="">
		<input type="hidden" name="hdnAgentName" id="hdnAgentName" value="">
		<input type="hidden" name="hdnInvoice" id="hdnInvoice" value="">
		<input type="hidden" name="hdnRpt" id="hdnRpt" value="Summary">
		<input type="hidden" name="hdnLive" id="hdnLive" value="">
		<input type="hidden" name="hdnEntityText" id="hdnEntityText" value="">
	</form>
  </body>
 <script src="../../js/reports/InvoiceSummary.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  <script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
   //-->
  </script>
</html>
