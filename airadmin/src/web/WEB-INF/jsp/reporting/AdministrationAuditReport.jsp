<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

	<script type="text/javascript">
		var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
		var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	</script>
	
  </head>
  
  <body class="tabBGColor" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" onbeforeunload="beforeUnload()"  scroll="no">
  	<form name="frmAdminAudit" id="frmAdminAudit" action="" method="POST">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">		
		 <table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">				
								
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Admin Module Audit Report<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblAdminAudit">					  		
							<tr>
								<td width="60%"></td>
					
							</tr>
							<tr>
								<td colspan="2">
									<table border="0" cellpadding="0" cellspacing="2" width="100%">
										<tr><td colspan="4"><font class="fntBold">Date Range</font></td><td></td></tr>
										<tr><td width="13%" align="left"><font>From &nbsp;&nbsp;</font>
											<input name="txtFromDate" type="text" id="txtFromDate" size="10"  maxlength="10" onBlur="settingValidation('txtFromDate')" onchange="dataChanged()"></td>
											<td width="8%" align="left"><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"> &nbsp;* </font></td>
											<td width="11%"><font>To &nbsp;&nbsp;</font>
											<input	name="txtToDate" type="text" id="txtToDate" size="10" maxlength="10" onBlur="settingValidation('txtToDate')" onchange="dataChanged()"></td>
											<td align="left"><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"> &nbsp;* </font></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							
							<tr>					
									<td><table>
									<tr>
									<td align="left"><font class="fntBold">Task Group&nbsp;&nbsp; </font>
											<select name="selTaskGroup" id="selTaskGroup" onClick="clickTaskGroup()" onchange="changeTaskGroup()" size="1"  title="Task Group">
												<option value=""></option>																					
												<option value="All">All</option>
												<c:out value="${requestScope.reqTaskGroupList}" escapeXml="false" />
											</select>
											<font class="mandatory"> &nbsp;* </font>
										</td>	
								
										<td><font class="fntBold">&nbsp;&nbsp;Task&nbsp;&nbsp;</font>
											<select name="selTask" id="selTask" size="1" title="Task">
												<option value=""></option>
												<option value="All">All</option>
												<c:out value="${requestScope.reqTaskList}" escapeXml="false" />
											</select>								
										</td>
									</tr>
									</table></td>
											
							</tr>							
							
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							
							<tr>
								<td align="left" width="20%"><font class="fntBold">User ID&nbsp;&nbsp;&nbsp; </font>
									<select name="selUserId" id="selUserId" size="1" title="User Id">
										<option value=""></option>																					
										<option value="All">All</option>
										<c:out value="${requestScope.reqUserIdList}" escapeXml="false" />
									</select>
									
								</td>	
							</tr>
							
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							
							<tr>
								<td>
									<table width="50%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td>
												<font class="fntBold">Agencies</font>
													<select id="selAgencies" size="1" name="selAgencies" onClick="clickAgencies()" onChange="changeAgencies()" style="width:76px">
														<option value=""></option>
														<option value="All">All</option>
														<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />					
													</select>
											</td>
											<td>
												<input type="checkbox" name="chkTAs" id="chkTAs" class="noborder"><font>With reporting TA's</font>					
											</td>
											<td>
												<input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()">
											</td>
										</tr>
										<tr>
											<td><font>&nbsp;</font></td>
											<td>
												<input type="checkbox" name="chkCOs" id="chkCOs" class="noborder"><font>With reporting CO's</font>
											</td>												
										</tr>
									</table>
								</td>
								<td></td>
							</tr>							
							
							<tr>
								<td>
									<font class="fntBold">Agents</font>
								</td>																
							</tr>
							
							<tr>
								<td valign="top"><span id="spn1"></span></td>
								<td valign="bottom"><font class="mandatory"> &nbsp;</font></td>								
							</tr>
							
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>

							<tr>
								<td>
									<table>
										<tr>
											<td> <font class="fntBold">Search For&nbsp;&nbsp;</font></td>
											<td><textarea name="txtSearch" id="txtSearch" cols="47" rows="2" onkeyUp="validateTextArea(this,255)" onkeyPress="validateTextArea(this,255)" onChange="dataChanged()" title="Can enter only up to 255  charactors"></textarea></td>
										</tr>
									</table>
									
								</td>
							</tr>
							
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>								
								<td>
									<font class="fntBold">Output Option</font>
								</td>
							</tr>	
								
							<tr>								
								<td>
									<table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
											<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionHTML"	value="HTML" onClick="chkClick()" class="noBorder" checked><font>HTML</font></td>
											<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" value="PDF" onClick="chkClick()" class="noBorder"><font>PDF</font></td>
											<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL" value="EXCEL" onClick="chkClick()" class="noBorder"><font>EXCEL</font></td>
											<td width="40%"><input type="radio" name="radReportOption" id="radReportOptionCSV" value="CSV" onClick="chkClick()" class="noBorder"><font>CSV</font></td>
										</tr>															
									</table>
									<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />
								</td>	
							</tr>	
							
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
								</td>
								<td >
									<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
								</td>
							</tr>
							
						</table>
						
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
			
		<script type="text/javascript">
				<c:out value="${requestScope.reqGSAList}" escapeXml="false" />				
		</script>	
			
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnAgents" id="hdnAgents" value="">
		<input type="hidden" name="hdnPaymentMode" id="hdnPaymentMode" value="">
		<input type="hidden" name="hdnLive" id="hdnLive" value="">
	</form>
  </body>
  <script src="../../js/reports/validateAdministrationAuditReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  <script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
   //-->
  </script>
</html>
