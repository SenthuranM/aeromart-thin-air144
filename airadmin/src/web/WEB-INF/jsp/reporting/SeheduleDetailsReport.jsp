 <%-- 
	 @Author 	: 	Chamindap
  --%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
</script>
  </head>
  <body class="tabBGColor" oncontextmenu="return false" onbeforeunload="beforeUnload()">
  	<form name="frmScheduleDetails" id="frmScheduleDetails" action="showScheduleDetailsReport.action" method="post">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">  		
		 <script type="text/javascript">
		</script>

		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<!--tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr -->
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Schedule Details<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
					  		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
						
							<tr><td><font>&nbsp;</font></td></tr>
							
								<td>
								<table width="70%" border="0" cellpadding="0" cellspacing="2">
								<tr>
									<td><table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="30%"><font>Start Date </font></td>
										<td><input type="text" name="txtFromDate" id="txtFromDate" size="10" onBlur="settingValidation('txtFromDate')" onchange="dataChanged()"  maxlength="10">
											<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font></td>					
										</tr>
										</table>
									</td>	
								</tr>
								<tr>
									<td><table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="30%"><font>Stop Date </font></td>
										<td><input type="text" name="txtToDate" id="txtToDate" maxlength="10"  onBlur="settingValidation('txtToDate')" onchange="dataChanged()" size="10">
											<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font></td>					
										</tr>
										</table>
									</td>	
								</tr>
								<tr>
									<td><table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="30%"><font>Flight Number </font></td>
										<td><input type="text" name="FlightNo" id="FlightNo"></td>					
										</tr>
										</table>
									</td>	
								</tr>
								<tr>
								<td><table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="30%"><font>Operation Type </font></td>
										<td><select name="selOperationType" id="selOperationType" size="1" style="width:120px" title="Operation Type">																				
											<OPTION value="Select"></OPTION>	
										<c:out value="${requestScope.sesOperationTypeListData}" escapeXml="false" />
									</select></td>					
										</tr>
										</table>
								
								</td>
								</tr>
								<tr>
								<td><table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="30%">
									<font>Origin </font></td>
										<td>
									<select name="selFrom" id="selFrom" size="1" style="width:120px" title="Select From">
										<OPTION value="Select"></OPTION>										
										<c:out value="${requestScope.reqStationList}" escapeXml="false" />
									</select></td>					
										</tr>
										</table>
								</td>
								</tr>
								<tr>
								<td><table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="30%">
									<font>Destination </font></td>
										<td>
									<select name="selTo" id="selTo" size="1" style="width:120px" title="Select To">
										<OPTION value="Select"></OPTION>										
										<c:out value="${requestScope.reqStationList}" escapeXml="false" />
									</select></td>					
										</tr>
										</table>
								</td>
								</tr>
								<tr>
								<td><table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="30%">
									<font>Status </font></td>
										<td>
									<select name="selStatus" id="selStatus" size="1" style="width:120px" title="Select Status">																				
										<c:out value="${requestScope.sesFlightStatusData}" escapeXml="false" />
									</select></td>					
										</tr>
										</table>
								</td>
								</tr>
								<tr>
									<td>
										<table width="70%" border="0" cellpadding="0" cellspacing="2">
											<tr>
												<td width="30%">
													<font>Time in</font>
												</td>
												<td width="5%">
													<input type="radio" name="radTimeIn" id="radTimeIn" value="Local"  class="noBorder">
												</td>
												<td width="20%"><font>Local</font></td>
												<td width="5%">
													<input type="radio" name="radTimeIn" id="radTimeInZulu" value="Zulu" class="noBorder" CHECKED>
												</td>
												<td><font>Zulu</font></td>
											</tr>
										</table>
									</td>
								</tr>
							
								
							<tr><td><font>&nbsp;</font></td></tr>
							
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>				
							<td><font class="fntBold">Output Option</font></td>							
							</tr>
							<tr>
								<td><table width="80%" border="0" cellpadding="0" cellspacing="2"><tr>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
									value="HTML" onClick="chkClick()" class="noBorder" checked><font>HTML</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" 
									value="PDF" onClick="chkClick()" class="noBorder"><font>PDF</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
									value="EXCEL" onClick="chkClick()" class="noBorder"><font>EXCEL</font></td>
								<td><input type="radio" name="radReportOption" id="radReportOptionCSV"
									value="CSV" onClick="chkClick()" class="noBorder"><font>CSV</font></td>
								</tr>									
								</table>
								<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
								</td>									
							</tr>
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
									
									<!--input name="btnPrint" type="button" class="Button" id="btnPrint" value="Print" onClick="printClick()"-->
								</td>
								<td align="right">
									<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
								</td>
							</tr>
						</table>
				  		</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>	
				<%@ include file="../common/IncludeFormBottom.jsp"%>	
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnModel" id="hdnModel" value="">
		<input type="hidden" name="hdnVersion" id="hdnVersion" value="">
		<input type="hidden" name="hdnLive" id="hdnLive" value="">

	</form>
  </body>
	<script src="../../js/reports/SeheduleDetailsReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
       clearTimeout(objProgressCheck);
      top[2].HideProgress();
 
   }
    
   //-->
  </script>
</html>

