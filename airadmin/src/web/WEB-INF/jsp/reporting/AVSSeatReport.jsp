 <%-- 
	 @Author 	: 	Chamindap
  --%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
		
</script>
  </head>
  <body class="tabBGColor" oncontextmenu="return false" onbeforeunload="beforeUnload()">
  	<form name="frmAVSSeat" id="frmAVSSeat"  method="post">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">  		
		 <script type="text/javascript">
		</script>

		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<!--tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr -->
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>AVS Seats<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
					  		<tr>			
							
								<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="2">
									<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="15%"></td>
										<td width="55%"></td>		
										<td width="10%"></td>
										<td width="20%"></td>										
										</tr>
										</table>
								
								</td>
								<tr>

							<tr><td><font>&nbsp;</font></td></tr>
							
								<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="2">
									<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="15%"><font>GDS Code </font></td>
										<td width="55%"><select name="selGDSCode" id="selGDSCode" size="1" style="width:120px" title="GDS Code">												
										<OPTION value="Select"></OPTION>								
										<c:out value="${requestScope.reqGdsCodeList}" escapeXml="false" />
									</select></td>							
										<td width="10%"></td>
										<td width="20%"></td>	
										</tr>
										</table>
								
								</td>
								
								<tr>
								<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="15%">
									<font>Booking Code </font></td>
										<td width="55%">
									<select name="selBC" id="selBC" size="1" style="width:120px" title="Select Booking Code">
										<OPTION value="Select"></OPTION>										
										<c:out value="${requestScope.reqBookingCodeList}" escapeXml="false" />
									</select></td>
										<td width="10%"></td>
										<td width="20%"></td>									
										</tr>
										</table>
								</td>
								</tr>
								<tr>
								<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="15%">
									<font>Event Code </font></td>
										<td width="55%">
									<select name="selEventCode" id="selEventCode" size="1" style="width:120px" title="Select Event Code">
										<OPTION value="Select"></OPTION>
										<c:out value="${requestScope.reqEventCodeList}" escapeXml="false" />										
										
									</select></td>	
									<td width="10%"></td>
									<td width="20%"></td>
										</tr>
										</table>
								</td>
								</tr>
								<tr>
									<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="15%"><font>Flight Number </font></td>
										<td width="55%"><input type="text" name="txtFlightNum" id="txtFlightNum" size="10" maxlength="10">
													
										<td width="20%"></td>	
										<td width="20%"></td>	
										</tr>
										</table>
									</td>	
								</tr>
								<tr>
									<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="15%"><font>Departure Date </font></td>
										<td width="55%"><input type="text" name="txtDepTimeLocal" id="txtDepTimeLocal" maxlength="10"  onBlur="settingValidation('txtDepTimeLocal')" onchange="dataChanged()"  size="10">
											<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Departure Date"><img SRC="../../images/calendar_no_cache.gif" border="0"></a>					
										
										<td width="10%"></td>
										<td width="20%"></td> 
										</tr>
										</table>
									</td>	
								</tr>
								
								<tr>
							
								</tr>
								<tr>
								<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="15%">
									<font>Segment Code From </font></td>
										<td width="55%">
									<select name="selSegmentFrom" id="selSegmentFrom" size="1" style="width:120px" title="Select Segment From">	<OPTION value="Select"></OPTION>														
										<c:out value="${requestScope.reqStationList}" escapeXml="false" />
									</select></td>
									<td width="15%">
									<font>Segment Code To </font></td>
										<td width="55%">
									<select name="selSegmentTo" id="selSegmentTo" size="1" style="width:120px" title="Select Segment To">	<OPTION value="Select"></OPTION>														
										<c:out value="${requestScope.reqStationList}" escapeXml="false" />
									</select></td>
										
									<td width="10%"></td>
										<td width="20%"></td>
										</tr>
										</table>
								</td>
								</tr>
								
								<tr><td>
								</td>
							</table>
								</td>
							</tr>
								<tr>				
							<td><font class="fntBold">Output Option</font></td>							
							</tr>							
							<tr>
								<td><table width="80%" border="0" cellpadding="0" cellspacing="2"><tr>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
									value="HTML" class="noBorder" checked><font>HTML</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" 
									value="PDF" class="noBorder"><font>PDF</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
									value="EXCEL" class="noBorder"><font>EXCEL</font></td>
								<td><input type="radio" name="radReportOption" id="radReportOptionCSV"
									value="CSV" class="noBorder"><font>CSV</font></td>
								</tr>									
								</table>
								</td>									
							</tr>
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
									
									<!--input name="btnPrint" type="button" class="Button" id="btnPrint" value="Print" onClick="printClick()"-->
								</td>
								<td align="right">
									<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
								</td>
							</tr>
						</table>
				  		
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>	
				<%@ include file="../common/IncludeFormBottom.jsp"%>	
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnModel" id="hdnModel" value="">
		<input type="hidden" name="hdnVersion" id="hdnVersion" value="">
		<input type="hidden" name="hdnOpType" id="hdnOpType" value="">
		<input type="hidden" name="hdnLive" id="hdnLive" value="">

	</form>
  </body>
	<script src="../../js/reports/AVSSeatReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){ 
      clearTimeout(objProgressCheck);
      top[2].HideProgress();   
   }
    
   //-->
  </script>
</html>

