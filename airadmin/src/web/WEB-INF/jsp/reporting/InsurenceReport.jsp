<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Insurence Report</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">	
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

	<script type="text/javascript">
		var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
		var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	</script>
  </head>
  <body class="tabBGColor" onunload="beforeUnload()" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" scroll="no">
  	<form name="frmINRPage" id="frmINRPage" action="" method="post">				
		 <table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">				
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			<tr>
				<td>
					<%@ include file="../common/IncludeFormTop.jsp"%>Insurance Details<%@ include file="../common/IncludeFormHD.jsp"%>
				  	<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
						<tr>
							<td><font class="fntBold">Date Range</font></td>				
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="2">	
									<tr><td align="left" width="13%"><font>From &nbsp;&nbsp;</font><input name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onBlur="dateChk('txtFromDate')" invalidText="true"></td>
										<td align="left" width="10%"><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>&nbsp;*</b></font></td>
										<td width="11%"><font>To &nbsp;&nbsp;</font><input	name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onBlur="dateChk('txtToDate')" invalidText="true"></td>
										<td align="left"><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>&nbsp;*</b></font></td>
									</tr>
								</table>
							</td>
							<td></td>
						</tr>							
						<tr><td width="60%"><font class="fntBold">Output Option</font></td></tr>
						<tr><td><table width="70%" border="0" cellpadding="0" cellspacing="2"><tr>
							<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"	value="HTML"  class="noBorder" checked><font>HTML</font></td>
							<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" value="PDF"  class="noBorder"><font>PDF</font></td>
							<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL" value="EXCEL"  class="noBorder"><font>EXCEL</font></td>
							<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionCSV" value="CSV"  class="noBorder"><font>CSV</font></td>
						</tr>
						<tr><td>
						<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
						</td></tr>
						<tr>
							<td colspan="2">
								<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
							</td>
							<td align="right">
								<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
							</td>
						</tr>
						<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>					
			 		<%@ include file="../common/IncludeFormBottom.jsp"%>									
					</table>
				</td>
			</tr>	
		</table>				
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnLive" id="hdnLive" value="">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">
	</form>
	<script src="../../js/reports/InsurenceReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  	<script type="text/javascript">
	   <!--
	   var objProgressCheck = setInterval("ClearProgressbar()", 300);
	   function ClearProgressbar(){
	      clearTimeout(objProgressCheck);
	      top[2].HideProgress();
	   }
	   //-->
  	</script>
  </body>  
</html>
