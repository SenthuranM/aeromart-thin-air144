<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.isa.thinair.airadmin.core.web.constants.WebConstants"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>OverBook Details</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">	
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript">
	var stns = new Array();  
	var agentsArr = new Array();
	var repLive = "<c:out value="${requestScope.hdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	
</script>
</head>
<body class="tabBGColor" onunload="beforeUnload()" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" scroll="no">
<form action="" method="post" id="frmOverBookReport"
	name="frmPromotionsReport">
	
	<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">	
		
		<tr>
			<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
		</tr>
		<tr>
			
      <td> 
        <%@ include file="../common/IncludeFormTop.jsp"%>Flight OverBook Details Report<%@ include file="../common/IncludeFormHD.jsp"%>
        <table width="100%" border="0" cellpadding="0" cellspacing="2"
					ID="tblHead">	
					<tr>					
						<td width="100%">
							<table width="80%" border="0" cellpadding="0" cellspacing="2">	
							
								<tr> 
								  	<td width="5%" ><font>From Date</font></td>
									<td width="5%"><input name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFromDate')"></td>
								   
								    <td width="10%" >&nbsp;&nbsp;<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From">
								    <img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory">&nbsp;*</font></td>
										
									<td width="5%" colspan="2"><font>To Date</font></td>
									<td width="5%"><input name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtToDate')"></td>
									
									<td width="10%" ><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To">
									<img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory">&nbsp;*</font></td>
									
								</tr>
								<tr>
									<td width="5%"><font>Considering Fixed Seats</font></td>
									<td><input type="checkbox" name="chkWithFixedSeats" id="chkWithFixedSeats" /></td>
								</tr>
							</table>
						</td>
					</tr>															
				
							
				<tr>				
					<td><font class="fntBold">Output Option</font></td>							
				</tr>
				<tr>
					<td><table  width="100%" border="0" cellpadding="0" cellspacing="2"><tr>
					<td width="15%"><input type="radio" name="radReportOption" id="radReportOption"
						value="HTML" class="noBorder"  checked><font>HTML</font></td>
					<td width="15%"><input type="radio" name="radReportOption" id="radReportOption" 
						value="PDF" class="noBorder"><font>PDF</font></td>
					<td width="15%"><input type="radio" name="radReportOption" id="radReportOption"
						value="EXCEL" class="noBorder"><font>EXCEL</font></td>
					<td><input type="radio" name="radReportOption" id="radReportOption" 
						value="CSV" class="noBorder"><font>CSV</font></td>
				</tr>									
				</table>
				</td>									
			</tr>
			<tr><td>&nbsp;</td></tr>	
			<tr>				
				<td width=90%><input type="button" id="btnClose"  name="btnClose" value="Close" class="Button" onclick="closeClick()"></td>
				<td width=10% align="right"><input type="button" id="btnView"  name="btnView" value="View" class="Button" onclick="viewClick()"></td>	
			</tr>		
		</table>
		<%@ include file="../common/IncludeFormBottom.jsp"%></td>
	</tr>
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
</table>
	<input type="hidden" name="hdnMode" id="hdnMode">
	<input type="hidden" name="hdnReportView" id="hdnReportView">
	<input type="hidden" name="hdnLive" id="hdnLive">
	<input type="hidden" name="selFlightOverbookType" id="selFlightOverbookType">
</form>
<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
</body>
<script src="../../js/reports/FlightOverbookReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
</html>