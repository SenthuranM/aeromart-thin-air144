<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Outstanding Balance Report</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<!--For Calendar-->
<title>Outstanding Balance Report</title>
<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
</script>
</head>
<body onload="pageLoadOB()" class="tabBGColor" onbeforeunload="beforeUnload()" oncontextmenu="return false">
<form action="" method="post" id="frmOutStandingReport"
	name="frmOutStandingReport">
<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">	
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
	</tr>
	<tr>
		<td><%@ include file="../common/IncludeFormTop.jsp"%>Outstanding
		Balance Report<%@ include file="../common/IncludeFormHD.jsp"%>
		<table width="100%" border="0" cellpadding="0" cellspacing="2"
			ID="tblHead">
			<tr>
				<td><font class="fntBold">Date Range</font></td>				
			</tr>
			<tr><td>
			<table width="70%" border="0" cellpadding="0" cellspacing="2">	
				<tr><td width="27%" align="left"><font>From  </font><input
					name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFromDate')"></td>
				<td width="20%"><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory">&nbsp;*</font></td>
				<td width="24%"><font>To  </font><input
					name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtToDate')"></td>
				<td width="29%"><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory">&nbsp;*</font></td>
				</tr>
			</table>
			</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td><font class="fntBold">Agents</font></td>				
			</tr>
			<tr>				
				<td>
					<table width="70%" border="0" cellpadding="0" cellspacing="2">
						<tr>
							<td width="20%"><input type="radio" name="radAgentOption" id="radAgentOptionALL"
								value="All" class="noBorder" checked="checked" onClick="agentsClick()"><font>All</font></td>
							<td width="20%"><input type="radio" name="radAgentOption" id="radAgentOptionAgent" 
								value="AGENT" class="noBorder" onClick="agentsClick()"><font>Select</font></td>
							<td><select name="selagent" id="selagent">
									<option></option>
									<c:out value="${requestScope.reqGSAList}" escapeXml="false" />
								</select>
							</td> 
						</tr>
					</table>
			<tr><td>&nbsp;</td></tr>				
			<tr>				
				<td><font class="fntBold">Report Options</font></td>							
			</tr>
			<tr>
				<td><table width="70%" border="0" cellpadding="0" cellspacing="2"><tr>
				<td width="30%"><input type="radio" name="radDebitCredit" id="radDebitCredit"
					value="DebitCredit" onClick="chkClick()" class="noBorder" checked="checked"><font>Credit &amp; Debit</font></td>
				<td width="20%"><input type="radio" name="radDebitCredit" id="radDebit" 
					value="Debit" onClick="chkClick()" class="noBorder"><font>Debit only</font></td>
				<td width="50%"><input type="radio" name="radDebitCredit" id="radCredit"
					value="Credit" onClick="chkClick()" class="noBorder"><font>Credit only</font></td>
				</tr>									
				</table>
				</td>									
			</tr>
			
			<tr><td>&nbsp;</td></tr>				
			<tr>				
				<td><font class="fntBold">Output Option</font></td>							
			</tr>
			<tr>
				<td><table width="70%" border="0" cellpadding="0" cellspacing="2"><tr>
				<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionHTML"
					value="HTML" class="noBorder" checked="checked"><font>HTML</font></td>
				<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" 
					value="PDF" class="noBorder"><font>PDF</font></td>
				<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
					value="EXCEL" class="noBorder"><font>EXCEL</font></td>				
				<td><input type="radio" name="radReportOption" id="radReportOptionCSV"
					value="CSV" class="noBorder"><font>CSV</font></td>
				</tr>	
				</table>
				<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
				</td>									
			</tr>
			<tr><td>&nbsp;</td></tr>	
			<tr>				
				<td width="70%"><input type="button" id="btnClose"  name="btnClose" value="Close" class="Button" onclick="closeClick()"></td>
				<td width="30%" align="right"><input type="button" id="btnView"  name="btnView" value="View" class="Button" onclick="viewClick()"></td>	
			</tr>		
		</table>
		<%@ include file="../common/IncludeFormBottom.jsp"%></td>
	</tr>
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
</table>
<input type="hidden" name="hdnMode" id="hdnMode" value="Mode"> 
<input type="hidden" name="hdnAgentTypeCode" id="hdnAgentTypeCode"/>
<input type="hidden" name="hdnLive" id="hdnLive" value="">
</form>
</body>
<script type="text/javascript">			
</script>
<script src="../../js/reports/OutStandingReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
    
   //-->
  </script>
</html>
