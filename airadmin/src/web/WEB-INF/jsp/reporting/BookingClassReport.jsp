 <%-- 
	 @Author 	: 	Ruchith Arambepola
  --%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	
</script>
  </head>
  <body class="tabBGColor" onbeforeunload="beforeUnload()" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
  	
  	<form name="frmBookingClassReport" id="frmBookingClassReport" a method="post">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">  		
		 <script type="text/javascript">
		</script>

		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Booking Class Report<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
					  								
							<tr>							
								<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="2">
								<tr>
								<td>
									<table width="70%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td width="50%">
											<font class="fntBold">Class of Service</font>
											<select id="selCOS" size="1" name="selCOS" style="width:110px"
													onchange="filterBookingClass(this.value, 'cos')">
												<option value=""></option>												
												<c:out value="${requestScope.reqCabinClassList}" escapeXml="false" />					
											</select>
										</td>
										
										<td id="logicalCC">
											<font class="fntBold">Logical Cabin Class</font>
											<select id="selLogicalCC" size="1" name="selLogicalCC" style="width:129px"
													onchange="filterBookingClass(this.value, 'lcc')">
												<option value=""></option>												
												<c:out value="${requestScope.reqLogicalCabinClassList}" escapeXml="false" />					
											</select>
										</td>
										</tr>
										</table>
										</td>
									</tr>
									<tr></tr>
									<tr>
										<td>								
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr>	
												<td colspan ="3" rowspan="7" valign="top">
													<span id="spnBC" class="FormBackGround"></span>
												</td>
										  	</tr>
							  				<tr></tr>
											</table>
										</td>
									</tr>
									<tr></tr>
									<tr></tr>
								<tr>
								<td>
								<table>
								
									<tr>
										<td><font>&nbsp;</font></td>
										<td>
											
										</td>												
									</tr>
								</table>
							 </td>
							</tr>							
							
							
							
							<tr>
							<td></td>
							</tr>
							<tr>
							
								<td valign="top"><span id="spnBCCat"></span></td>
								<td valign="bottom" width="1px"><font class="mandatory"></font></td>
							</tr>
													
							<tr></tr>
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr><td><font class="fntBold">Booking Class Type</font>
											<select id="selBCType" size="1" name="selBCType" style="width:76px" class="pos_left1">
												<option value=""></option>
												<option value="NORMAL">Normal</option>
												<option value="STANDBY">StandBy</option>
												<option value="OPENRT">OpenReturn</option>
											</select>
										</td>
										</tr>
										<tr></tr>
										<tr></tr>
										<tr>
										<td><font class="fntBold">Allocation Type</font>
												<select id="selAllocationType" size="1" name="selAllocationType"  style="width:76px" class="pos_left2">
												<option value=""></option>
												
												<c:out value="${requestScope.reqAllocationTypeList}" escapeXml="false" />					
											</select>
										</td>
										</tr>
										<tr></tr>
										<tr></tr>
									<tr>
									<td><font class="fntBold">Status</font>
												<select id="selStatus" size="1" name="selStatus"  style="width:76px" class="pos_left3">
												<option value=""></option>
												<option value="ACT">ACT</option>
												<option value="INA">INA</option>
																	
											</select>
											</td>
									</tr>
									<tr></tr>
									<tr></tr>
							<tr><td><font class="fntBold">Output Option</font></td>	</tr>
							<tr>
								<td><table width="70%" border="0" cellpadding="0" cellspacing="2"><tr>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
									value="HTML"  class="noBorder" checked><font>HTML</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" 
									value="PDF"  class="noBorder"><font>PDF</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
									value="EXCEL"  class="noBorder"><font>EXCEL</font></td>
								<td width="40%"><input type="radio" name="radReportOption" id="radReportOptionCSV"
									value="CSV"  class="noBorder"><font>CSV</font></td>
								</tr>									
								</table>
								<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
								</td>									
							</tr>
							
							<tr>
								<td>
									<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
									
									<!--input name="btnPrint" type="button" class="Button" id="btnPrint" value="Print" onClick="printClick()"-->
								</td>
								
								<td align="right">
									<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
								</td>
							</tr>
						</table>
				  		
					</td>
				</tr>
				
			</table>
	
<%@ include file="../common/IncludeFormBottom.jsp"%>		
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnBCs" id="hdnBCs" value="">
		<input type="hidden" name="hdnBCCat" id="hdnBCCat" value="">		
		<input type="hidden" name="hdnLive" id="hdnLive" value="">
		<input type="hidden" name="hdnRptType" id="hdnRptType" value="">
		

	</form>
  </body>
	<script src="../../js/reports/BookingClassReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
	<c:out value="${requestScope.reqBcHtmlData}" escapeXml="false" />
	<c:out value="${requestScope.reqBcCategoryData}" escapeXml="false" />
		
	if(bcls) {
		bcls.height = '125px';
		bcls.width = '160px';				
		bcls.drawListBox();
	}
   
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
    }
    
   //-->
  </script>
</html>

