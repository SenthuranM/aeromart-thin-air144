<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>Enplanement Report</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
	<script type="text/javascript">
		var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
		var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	</script>
</head>
	<body class="tabBGColor" onLoad="winOnLoad()" onbeforeunload="beforeUnload()" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" scroll="no">
		<form action="" method="post" id="frmEnplanementReport" name="frmModeofPayments">
			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">	
				<tr>
					<td><font class="fntSmall">&nbsp;</font></td>
				</tr>
				<tr>
					<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
				</tr>
				<tr>
					<td><%@ include file="../common/IncludeFormTop.jsp"%>Enplanement Report<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="2"	ID="tblHead">
							<tr>
								<td><font class="fntBold">Date Range</font></td>				
							</tr>
							<tr>
								<td>
									<table width="70%" border="0" cellpadding="0" cellspacing="2">	
										<tr>
											<td width="20%" align="left"><font>From&nbsp;&nbsp;&nbsp; </font><input
												name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFromDate')" invalidText="true"></td>
											<td width="10%"><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
											<td width="15%"><font>To </font><input
												name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtToDate')" invalidText="true"></td>
											<td width="29%"><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr><td>&nbsp;</td></tr>
							<tr>
								<td>
									<table width="100%" border="0" cellpadding="0" cellspacing="2">	
										<tr>
											<td ><font >Airport</font>
												<select id="selAirport" size="1" name="selAirport" style="width:75px" tabindex="1">
													<option value=""></option>
													<c:out value="${requestScope.reqStationList}" escapeXml="false" />
									    		 </select>
									    	</td>
										</tr>						
			 						</table>
			 					</td>
							</tr>		
							<tr><td>&nbsp;</td></tr>
							
							
							
							
							
							
							<tr>
										<td><table width="100%" border="0" cellpadding="0"
												cellspacing="2">
												<tr>
													<td width="15%"><font class="fntBold">Flight
															Number </font></td>
													<td width="55%"><input type="text"
														name="txtFlightNumber" id="txtFlightNumber" maxlength="10"
														onchange="" size="10"></td>
													<td width="10%">&nbsp;</td>
													<td width="20%">&nbsp;</td>
												</tr>
											</table></td>
							</tr>
							<tr>
										<td colspan="3">&nbsp;</td>
							</tr>
							<tr>
				
								<td colspan='3'>
											<table width="70%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td><font class="fntBold">O &amp; D</font></td>
													<td><font>Departure</font></td>
													<td><select name="selDeparture" size="1"
														id="selDeparture" style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
													<td><font>Arrival</font></td>
													<td><select name="selArrival" size="1" id="selArrival"
														style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
													<td><font>&nbsp;</font></td>
													<td colspan="2" rowspan="3"><select id="selSegment"
														name="selSegment" multiple size="1"
														style="width: 200px; height: 85px">
													</select></td>
												</tr>
												<tr>
													<td><font>&nbsp;</font></td>
													<td><font>Via 1</font></td>
													<td><select name="selVia1" size="1" id="selVia1"
														style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
													<td><font>Via 2</font></td>
													<td><select name="selVia2" size="1" id="selVia2"
														style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
													<td align="center"><a href="javascript:void(0)"
														onclick="addToList()" title="Add to list"><img
															src="../../images/AA115_no_cache.gif" border="0"> </a>
													</td>
												</tr>
												<tr>
													<td><font>&nbsp;</font></td>
													<td><font>Via 3</font></td>
													<td><select name="selVia3" size="1" id="selVia3"
														style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
													<td><font>Via 4</font></td>
													<td><select name="selVia4" size="1" id="selVia4"
														style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqStationList}"
																escapeXml="false" />
													</select></td>
													<td align="center"><a href="javascript:void(0)"
														onclick="removeFromList()" title="Remove from list"><img
															src="../../images/AA114_no_cache.gif" border="0"> </a>
													</td>
												</tr>
											</table></td>
								
							</tr>
							<tr><td>&nbsp;</td></tr>				
							<tr>				
								<td><font class="fntBold">Output Option</font></td>							
							</tr>
							<tr>
								<td>
									<table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
											<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
												value="HTML"  class="noBorder" checked><font>HTML</font></td>
											<td width="20%"><input type="radio" name="radReportOption" id="radReportOption" 
												value="PDF"  class="noBorder"><font>PDF</font></td>
											<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
												value="EXCEL"  class="noBorder"><font>EXCEL</font></td>
											<td width="40%"><input type="radio" name="radReportOption" id="radReportOption"
												value="CSV"  class="noBorder"><font>CSV</font>
											</td>
										</tr>									
									</table>
									
									<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
								</td>									
							</tr>
							<tr><td>&nbsp;</td></tr>
							<tr>
								<td>
									<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
								</td>
								<td align="right">
									<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
								</td>
							</tr>				
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr>
					<td><font class="fntSmall">&nbsp;</font></td>
				</tr>
			</table>
		<input type="hidden" name="hdnMode" id="hdnMode">
		<input type="hidden" name="hdnLive" id="hdnLive" value="">
		<input type="hidden" name="hdnSegments" id="hdnSegments" value="">
		
		</form>
</body>
<script src="../../js/reports/EnplanementReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){ 
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
 
   }
    
   //-->
  </script>
</html>