<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Booked PAX Segment</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script type="text/javascript"><!--
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repShowpay = "<c:out value="${requestScope.reqSowPay}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
		var selectBox;
		var currentAgentCode = "";
		var displayAgencyMode = 0;
		<c:out value="${requestScope.reqHtmlDetails}" escapeXml="false" />
		
		
--></script>
  </head>
  <body class="tabBGColor" onbeforeunload="beforeUnload()" oncontextmenu="return showContextMenu()" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" scroll="no">  	
	<form name="frmBookedPAXSegment" id="frmBookedPAXSegment" action="" method="post">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">		
		 <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">				
				
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Booked PAX Segment<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
					  		<tr>
									<td>
										<table width="100%" border="0" cellpadding="0" cellspacing="0">	
											<tr>
												<td width="25%"><font class="fntBold">Date Range</font></td>						
											
												<td width="30%" align="left"><font>From </font>
													<input name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10"  onblur="dateChk('txtFromDate')" invalidText="true">
												</td>
												<td width="10%">
													<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font>
												</td>
												<td width="25%"><font>To </font>
													<input name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10"  onblur="dateChk('txtToDate')" invalidText="true">
												</td>
												<td width="10%"><a href="javascript:void(0)" onclick="LoadCalendarTo(1,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
											</tr>
										</table>
								</td>
							</tr>							
							<tr></tr>
							<tr>
								<td>
									<font class="fntBold">Sales Channel</font>
								</td>
								<!--<td>
									<font class="fntBold">Mode of Payment</font>
								</td>	-->							
							</tr>
							
							<tr>
							<td>
							<table width="62%" border="0" cellpadding="0" cellspacing="2">	
							<tr>
								<td valign="top" width="15%"><span id="spn6"></span></td>
								<td valign="bottom"><font class="mandatory"><b>*</b></font></td>								
								</tr>
								</table>
								</td>
							<!--<td>
							<table width="62%" border="0" cellpadding="0" cellspacing="2">	
							<tr>
								<td valign="top" width="15%"><span id="spn2"></span></td>
								<td valign="bottom"><font class="mandatory"><b>*</b></font></td>								
								</tr>
								</table>
								</td>								
							--></tr>							
							<tr>
								<td>
									<div id="divAgencies">
										<table>
											<tr>
												<td>
													<font>Agencies</font>
													<select id="selAgencies" size="1" name="selAgencies" onClick="clickAgencies()" onChange="changeAgencies()" style="width:76px">
														<option value=""></option>
														<option value="All">All</option>
														<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />					
													</select>
												</td>
												<td>
													<input type="checkbox" name="chkTAs" id="chkTAs" class="noborder"><font>With reporting TA's</font>					
												</td>
												<td>
													<input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()">
												</td>
											</tr>
											
											<tr>
												<td><font>&nbsp;</font></td>
												<td>
													<input type="checkbox" name="chkCOs" id="chkCOs" class="noborder"><font>With reporting CO's</font>
												</td>												
											</tr>
										</table>
									</div>
								</td>
							</tr>							
							<tr>
								<td colspan="2">
									<div id="divAgents">
										<table>
											<tr>
												<td>
													<font class="fntBold">Origin Agents</font>
												</td>
											</tr>
											<tr><td>
											<table width="62%" border="0" cellpadding="0" cellspacing="2">	
											<tr>
												<td valign="top" width="2%"><span id="spn1"></span></td>
												<!--<td align="left" valign="bottom"> <font class="mandatory"><b>*</b></font></td>--></tr>
												</table></td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<font class="fntBold">Origin Station</font>
								</td>	
								<td>
									<font class="fntBold">Marketing Carrier Code</font>
								</td>														
							</tr>
							<tr>
							<td>
							<table width="62%" border="0" cellpadding="0" cellspacing="2">	
							<tr>
								<td valign="top" width="15%"><span id="spnSTC"></span></td>
								<!--<td valign="bottom"><font class="mandatory"><b>*</b></font></td>								-->
								</tr>
								</table>
								</td>
								<td>
							<table width="62%" border="0" cellpadding="0" cellspacing="2">	
							<tr>
								<td valign="top" width="25%"><span id="spn4"></span></td>
								<td valign="bottom"><font class="mandatory"><b>*</b></font></td>								
								</tr>
								</table>
								</td>
							</tr>
							<tr><td width="60%">
									<font class="fntBold">Output Option</font>
								</td>
								<td width="60%">
									<font class="fntBold">Report Option</font>
								</td></tr>

							<tr><td><table width="100%" border="0" cellpadding="0" cellspacing="2">
							<tr>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"	value="HTML"  class="noBorder"><font>HTML</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" value="PDF"  class="noBorder"><font>PDF</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL" value="EXCEL"  class="noBorder"><font>EXCEL</font></td>
								<td width="40%"><input type="radio" name="radReportOption" id="radReportOptionCSV" value="CSV"  class="noBorder"  checked><font>CSV</font></td>
							</tr>
							</table>
								
							<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />															
								
							</td>
							<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
							
								
								<tr><td><input type="radio" name="radReportOutOption" id="radReportOptionDet"	value="Detail"  class="noBorder" checked><font>Detail Report</font></td></tr>
								<tr><td><input type="radio" name="radReportOutOption" id="radReportOptionSum" value="Summary"  class="noBorder"><font>Summary Report</font></td></tr>
								<tr><td>&nbsp</td></tr>
								<tr><td>
								<input type="checkbox" name="chkOnhold" id="chkOnhold" value="on" class="NoBorder">
								<font>Include Onhold PAX Segments</font>								
								</td></tr>
							
							</table>
								
							</td>
							</tr>							
							<tr>
								<td colspan="2">
									<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
								</td>
								<td align="right">
									<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
								</td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
		<script type="text/javascript">
				<c:out value="${requestScope.reqGSAList}" escapeXml="false" />
				<c:out value="${requestScope.reqPaymentType}" escapeXml="false" />
				<c:out value="${requestScope.reqSalesChannel}" escapeXml="false" />
				<c:out value="${requestScope.reqStationCountry}" escapeXml="false" />
				<c:out value="${requestScope.reqCarrierCodes}" escapeXml="false" />
				
				if (displayAgencyMode == 1 ) {
					document.getElementById('divAgencies').style.display= 'block';
					document.getElementById('divAgents').style.display= 'block';
				} else if (displayAgencyMode == 2 ) {
					document.getElementById('divAgents').style.display= 'block';
					document.getElementById('divAgencies').style.display= 'none';
				} else {
					document.getElementById('divAgencies').style.display= 'none';
					document.getElementById('divAgents').style.display= 'none';
				}						
		</script>	
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnAgents" id="hdnAgents" value="">
		<input type="hidden" name="hdnPayments" id="hdnPayments" value="">
		<input type="hidden" name="hdnRptType" id="hdnRptType" value="">
		<input type="hidden" name="hdnAgentName" id="hdnAgentName" value="">
		<!--<input type="hidden" name="hdnPaymentDesc" id="hdnPaymentDesc" value="">-->
		<input type="hidden" name="hdnReportView" id="hdnReportView" value="">
		<input type="hidden" name="hdnSalesChannels" id="hdnSalesChannels" value="">
		<input type="hidden" name="hdnStations" id="hdnStations" value="">
		<input type="hidden" name="hdnCarrierCode" id="hdnCarrierCode" value="">
		<input type="hidden" name="hdnOnholdStatus" id="hdnOnholdStatus" value="">
		<input type="hidden" name="hdnLive" id="hdnLive" value="">


	</form>
  </body>
  <script src="../../js/reports/BookedPAXSegmentReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  <script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
   //-->
   
   
  </script>
</html>