<%@ page contentType="text/html;charset=ISO-8859-1" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Booked SSR Details Report</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css"
	href="../../css/Style_no_cache.css">
<link href="../../themes/default/css/ui.all_no_cache.css"
	rel="stylesheet" type="text/css" />
<script
	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script
	src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/v2/schedrept/schedrept.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
</script>
<!--For Calendar -->
</head>
<body class="tabBGColor" onbeforeunload="beforeUnload()"
	onkeypress='return Body_onKeyPress(event)' onLoad="winOnLoad()"
	oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)"
	scroll="yes">
	<form action="showBookedSSRDetailsReport.action" method="post"
		id="frmBookedSSR" name="frmBookedSSR">
		<table width="100%" align="center" border="0" cellpadding="0"
			cellspacing="0">
			<tr>
				<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
			</tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Booked
					SSR Details Report<%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="100%" border="0" cellpadding="0" cellspacing="2"
						ID="tblHead">
						<tr>
							<td colspan="2">
								<table width="90%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td width="15%"><font class="fntBold">Service
												Category</font>
										</td>
										<td width="20%"><select name="selCategory" size="1"
											id="selCategory" onchange="filterSSRCodeByCategory()">
												<option value=""></option>
												<c:out value="${requestScope.reqSSRCategoryList}"
													escapeXml="false" />

										</select> <font class="mandatory"><b>*</b> </font>
										</td>

										<td width="15%"><font class="fntBold">Report Type</font>
										</td>
										<td width="15%"><select name="selReportType" size="1"
											id="selReportType">
												<option value=""></option>
												<option value="DETAIL">Details</option>
												<option value="SUMMARY">Summary</option>
										</select><font class="mandatory"><b>*</b> </font>
										</td>
										<td>&nbsp;</td>
									</tr>
								</table></td>
						</tr>
						<tr>
							<td colspan="3">
								<hr></td>
						</tr>


						<tr>
							<td colspan="2">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td>
											<table width="80%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td width="25%"><font class="fntBold">Departure
															Date</font></td>

													<td width="20%" align="left"><font>From </font> <input
														name="txtFromDate" type="text" id="txtFromDate" size="10"
														style="width: 75px;" maxlength="10"
														onblur="dateChk('txtFromDate')" invalidText="true">
													</td>
													<td width="20%"><a href="javascript:void(0)"
														onclick="LoadCalendar(0,event); return false;"
														title="Date From"><img
															SRC="../../images/calendar_no_cache.gif" border="0"
															border="0"> </a><font class="mandatory"><b>*</b>
													</font></td>
													<td width="18%"><font>To </font> <input
														name="txtToDate" type="text" id="txtToDate" size="10"
														style="width: 75px;" maxlength="10"
														onblur="dateChk('txtToDate')" invalidText="true">
													</td>
													<td width="17%"><a href="javascript:void(0)"
														onclick="LoadCalendarTo(1,event); return false;"
														title="Date From"><img
															SRC="../../images/calendar_no_cache.gif" border="0"
															border="0"> </a><font class="mandatory"><b>*</b>
													</font></td>
												</tr>
											</table></td>
									</tr>
									<tr>
										<td>
											<table width="80%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td width="25%"><font class="fntBold">Booked
															Date</font></td>

													<td width="20%" align="left"><font>From </font> <input
														name="txtBookedFromDate" type="text"
														id="txtBookedFromDate" size="10" style="width: 75px;"
														maxlength="10" onblur="dateChk('txtBookedFromDate')"
														invalidText="true"></td>
													<td width="20%"><a href="javascript:void(0)"
														onclick="LoadCalendar(2,event); return false;"
														title="Date From"><img
															SRC="../../images/calendar_no_cache.gif" border="0"
															border="0"> </a></td>
													<td width="18%"><font>To </font> <input
														name="txtBookedToDate" type="text" id="txtBookedToDate"
														size="10" style="width: 75px;" maxlength="10"
														onblur="dateChk('txtBookedToDate')" invalidText="true">
													</td>
													<td width="17%"><a href="javascript:void(0)"
														onclick="LoadCalendarTo(3,event); return false;"
														title="Date From"><img
															SRC="../../images/calendar_no_cache.gif" border="0"
															border="0"> </a></td>
												</tr>
											</table></td>
									</tr>
									<tr>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr>
										<td><table width="100%" border="0" cellpadding="0"
												cellspacing="2">
												<tr>
													<td width="15%"><font class="fntBold">Flight
															Number </font></td>
													<td width="55%"><input type="text"
														name="txtFlightNumber" id="txtFlightNumber" maxlength="10"
														onchange="" size="10"></td>
													<td width="10%">&nbsp;</td>
													<td width="20%">&nbsp;</td>
												</tr>
											</table></td>
									</tr>
									<tr>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr>
										<td colspan='3'>
											<table width="70%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td><font class="fntBold">O &amp; D</font></td>
													<td><font>Departure</font></td>
													<td><select name="selDeparture" size="1"
														id="selDeparture" style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqAirportList}"
																escapeXml="false" />
													</select></td>
													<td><font>Arrival</font></td>
													<td><select name="selArrival" size="1" id="selArrival"
														style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqAirportList}"
																escapeXml="false" />
													</select></td>
													<td><font>&nbsp;</font></td>
													<td colspan="2" rowspan="3"><select id="selSegment"
														name="selSegment" multiple size="1"
														style="width: 200px; height: 85px">
													</select></td>
												</tr>
												<tr>
													<td><font>&nbsp;</font></td>
													<td><font>Via 1</font></td>
													<td><select name="selVia1" size="1" id="selVia1"
														style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqAirportList}"
																escapeXml="false" />
													</select></td>
													<td><font>Via 2</font></td>
													<td><select name="selVia2" size="1" id="selVia2"
														style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqAirportList}"
																escapeXml="false" />
													</select></td>
													<td align="center"><a href="javascript:void(0)"
														onclick="addToList()" title="Add to list"><img
															src="../../images/AA115_no_cache.gif" border="0"> </a>
													</td>
												</tr>
												<tr>
													<td><font>&nbsp;</font></td>
													<td><font>Via 3</font></td>
													<td><select name="selVia3" size="1" id="selVia3"
														style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqAirportList}"
																escapeXml="false" />
													</select></td>
													<td><font>Via 4</font></td>
													<td><select name="selVia4" size="1" id="selVia4"
														style="width: 55px;">
															<option value=""></option>
															<c:out value="${requestScope.reqAirportList}"
																escapeXml="false" />
													</select></td>
													<td align="center"><a href="javascript:void(0)"
														onclick="removeFromList()" title="Remove from list"><img
															src="../../images/AA114_no_cache.gif" border="0"> </a>
													</td>
												</tr>
											</table></td>
									</tr>
									<tr>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr>
										<td>
											<table width="55%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td valign="top"><span id="spnSSR"
														class="FormBackGround"></span></td>
													<td valign="bottom"><font class="mandatory"><b>*</b>
													</font></td>
												</tr>
											</table></td>
										<td></td>
										<td>
											<table width="100%" border="0" cellpadding="0"
												cellspacing="0">
												<tr>
													<td colspan="3" rowspan="7" valign="top"></td>
												</tr>
											</table></td>
									</tr>
									<tr>
									<td>
											<table width="55%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td valign="top"><span id="spnPFSStatus"
														class="FormBackGround"></span></td>
													<td valign="bottom"><font class="mandatory">
													</font></td>
												</tr>
											</table></td>
										<td></td>
										<td>
											<table width="100%" border="0" cellpadding="0"
												cellspacing="0">
												<tr>
													<td colspan="3" rowspan="7" valign="top"></td>
												</tr>
											</table></td>
									</tr>


									<tr>
										<td><font>&nbsp;</font></td>
									</tr>

									<tr>
										<td>
											<div id="divReportViewOpts">
												<table width="100%" border="0" cellpadding="0"
													cellspacing="2">
													<tr>
														<td colspan='3'><font class="fntBold">Output
																Option</font></td>
													</tr>
													<tr>
														<td colspan='3' align="left">
															<table width="50%" border="0" cellpadding="0"
																cellspacing="2">
																<tr>
																	<td width="20%"><input type="radio"
																		name="radReportOption" id="radReportOption"
																		value="HTML" class="noBorder" checked><font>HTML</font>
																	</td>
																	<td width="20%"><input type="radio"
																		name="radReportOption" id="radReportOptionPDF"
																		value="PDF" class="noBorder"><font>PDF</font>
																	</td>
																	<td width="20%"><input type="radio"
																		name="radReportOption" id="radReportOptionEXCEL"
																		value="EXCEL" class="noBorder"><font>EXCEL</font>
																	</td>
																	<td width="40%"><input type="radio"
																		name="radReportOption" id="radReportOptionCSV"
																		value="CSV" class="noBorder"><font>CSV</font>
																	</td>
																</tr>
															</table> <c:out value="${requestScope.rptFormatOption}"
																escapeXml="false" /></td>
													</tr>
												</table>
											</div></td>
									</tr>
									<tr>
										<td colspan='2'><input name="btnClose" type="button"
											class="Button" id="btnClose" value="Close"
											onclick="closeClick()"></td>
										<td align="right"><input name="btnView" type="button"
											class="Button" id="btnView" value="View"
											onClick="viewClick()"></td>
									</tr>
								</table></td>
						</tr>
					</table> <%@ include file="../common/IncludeFormBottom.jsp"%></td>
			</tr>

		</table>
		<input type="hidden" name="hdnMode" id="hdnMode"> <input
			type="hidden" name="hdnSegments" id="hdnSegments"> <input
			type="hidden" name="hdnLive" id="hdnLive" value=""> <input
			type="hidden" name="hdnChargesCode" id="hdnChargesCode"> <input
			type="hidden" name="hdnCarrierCode" id="hdnCarrierCode"> <input
			type="hidden" name="hdnChargeGroupCodes" id="hdnChargeGroupCodes">
		<input type="hidden" name="hdnSsrCodes" id="hdnSsrCodes"> <input
			type="hidden" name="hdnAllSSRByCategory" id="hdnAllSSRByCategory"
			value="<c:out value='${requestScope.reqSsrAndCategoryList}' escapeXml='false' />">
			<input type="hidden" name="hdnPfsStatus" id="hdnPfsStatus"> 

	</form>

</body>
<script
	src="../../js/reports/BookedSSRDetailsReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript">
	
   <c:out value="${requestScope.reqSSRCodeList}" escapeXml="false" />
   if(ssrs) {
	    ssrs.height = '125px';
	    ssrs.width = '160px';				
	    ssrs.drawListBox();
	}
   
   <c:out value="${requestScope.reqPfsStatusList}" escapeXml="false" />
   pfsStatuss.height = '125px';
   pfsStatuss.width = '160px';				
   pfsStatuss.drawListBox();
   
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){ 
      clearTimeout(objProgressCheck);
      top[2].HideProgress();   
   }    
  
</script>
</html>