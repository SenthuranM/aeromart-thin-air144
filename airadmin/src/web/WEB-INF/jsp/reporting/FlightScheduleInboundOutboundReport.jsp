 <%-- 
	 @Author 	: 	Chamindap
  --%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<%@ page import="java.util.*, java.text.*" %>
<%
	Date dStartDate = null;
	Date dStopDate = null;
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dStartDate = cal.getTime(); // Current date
	cal.add(Calendar.MONTH,1);
	dStopDate = cal.getTime(); //Next Month Date
	String StartDate =  formatter.format(dStartDate);
	String StopDate = formatter.format(dStopDate);
%>
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
</script>
  </head>
  <body class="tabBGColor" oncontextmenu="return false" onbeforeunload="beforeUnload()">
  	<form name="frmPage" id="frmPage" action="" method="">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">  		
		 <script type="text/javascript">
		</script>

		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<!--tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr -->
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Inbound/Outbound Schedules<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
					  		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
							<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="2">
									<tr>
										<td width="100%">
											<font class="fntBold">Date Range</font>
										</td>
									</tr>
									<tr>
										<td><table width="100%" border="0" cellpadding="0" cellspacing="2"><tr>
											<td width="15%" align="left"><font>From </font></td>
											<td width="13%"><input type="text" name="txtFromDate" id="txtFromDate" size="10" onBlur="dateChk('txtFromDate')" value=<%=StartDate %> maxlength="10"></td>
											<td width="15%"><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font></td>											
											<td width="18%"><font>To </font>
											<input type="text" name="txtToDate" id="txtToDate" maxlength="10"  onBlur="dateChk('txtToDate')" value=<%=StopDate %> size="10"></td>
											<td width="8%"><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font></td>											
											<td valign="top">											
											<a href="javascript:PreviousMonth()" title="Previous month"><img src="../../images/UArrow_no_cache.gif" border="0" tabIndex="5"></a>
											<a href="javascript:NextMonth()" title="Next month"><img src="../../images/DArrow_no_cache.gif" border="0" tabIndex="6"></a>
											</td>
											</tr>									
											</table>
										</td>
									</tr>
							    </table>
							</td>
							</tr>
							<tr><td><font>&nbsp;</font></td></tr>
							<tr>
								<td>
								<table width="70%" border="0" cellpadding="0" cellspacing="2">
								<tr>
									<td><table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="30%"><font>Flight No </font></td>
										<td>
										<input type="text" name="selFlightNumber" id="selFlightNumber" maxlength="10" onchange="dataChanged()"  size="10">
										<!-- select name="selFlightNumber" id="selFlightNumber" size="1" style="width:120px" title="Flight No">
										<OPTION value=""></OPTION>										
										<c:out value="${requestScope.reqFlightNoList}" escapeXml="false" />									
										</select -->
										</td>					
										</tr>
										</table>
									</td>	
								</tr>
								<tr>
								<td><table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="30%"><font>Operation Type </font></td>
										<td><select name="selOperationType" id="selOperationType" size="1" style="width:120px" title="Operation Type">												
											<OPTION value=""></OPTION>																		
										<c:out value="${requestScope.sesOperationTypeListData}" escapeXml="false" />
									</select></td>					
										</tr>
										</table>
								
								</td>
								</tr>
								<tr>
								<td><table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="30%">
									<font>From </font></td>
										<td>
									<select name="selFrom" id="selFrom" size="1" style="width:120px" title="Select From">
										<OPTION value=""></OPTION>										
										<c:out value="${requestScope.reqStationList}" escapeXml="false" />
									</select></td>					
										</tr>
										</table>
								</td>
								</tr>
								<tr>
								<td><table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="30%">
									<font>To </font></td>
										<td>
									<select name="selTo" id="selTo" size="1" style="width:120px" title="Select To">
										<OPTION value=""></OPTION>										
										<c:out value="${requestScope.reqStationList}" escapeXml="false" />
									</select></td>					
										</tr>
										</table>
								</td>
								</tr>
								<tr>
								<td><table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="30%">
									<font>Status </font></td>
										<td>
									<select name="selStatus" id="selStatus" size="1" style="width:120px" title="Select Status">																				
										<OPTION value=""></OPTION>	
										<c:out value="${requestScope.reqFlightBuildStatus}" escapeXml="false" />
									</select></td>					
										</tr>
										</table>
								</td>
							</tr>								
							<tr><td><font>&nbsp;</font></td></tr>							
							</table>
								</td>
							</tr>
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>				
							<td><font class="fntBold">Output Option</font></td>							
							</tr>
							<tr>
								<td><table width="80%" border="0" cellpadding="0" cellspacing="2"><tr>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
									value="HTML" class="noBorder" checked><font>HTML</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" 
									value="PDF" class="noBorder"><font>PDF</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
									value="EXCEL" class="noBorder"><font>EXCEL</font></td>
								<td><input type="radio" name="radReportOption" id="radReportOptionCSV"
									value="CSV" class="noBorder"><font>CSV</font></td>
								</tr>									
								</table>
								<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />								
								</td>									
							</tr>
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
									<!--input name="btnPrint" type="button" class="Button" id="btnPrint" value="Print" onClick="printClick()"-->
								</td>
								<td align="right">
									<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">									
								</td>
							</tr>
						</table>
				  		<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>		
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnModel" id="hdnModel" value="">
		<input type="hidden" name="hdnVersion" id="hdnVersion" value="">
		<input type="hidden" name="hdnLive" id="hdnLive" value="">

	</form>
  </body>
	<script src="../../js/reports/FlightScheduleInboundOutbound.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
    
   //-->
  </script>
</html>
