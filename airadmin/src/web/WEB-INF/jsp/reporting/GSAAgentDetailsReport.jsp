 <%-- 
	 @Author 	: 	Chamindap
  --%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	var displayAgencyMode = 0;
	<c:out value="${requestScope.reqHtmlDetails}" escapeXml="false" />		

</script>
  </head>
  <body class="tabBGColor" onbeforeunload="beforeUnload()" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" scroll="no">
  	<form name="frmAgentGsaDetails" id="frmAgentGsaDetails" a method="post">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">  		
		 <script type="text/javascript">
		</script>

		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeMandatoryText.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Agent/GSA Details<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
					  		<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
						
							<tr>							
								<td>
								<table width="70%" border="0" cellpadding="0" cellspacing="2">
								<tr>
								<td>
									<div id="divAgencies">
										<table width="50%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td>
													<font class="fntBold">Agencies</font>
														<select id="selAgencies" size="1" name="selAgencies" onClick="clickAgencies()" onChange="changeAgencies()" style="width:76px">
														<option value=""></option>
														<option value="All">All</option>
														<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />					
													</select>
												</td>
												<td>
													<input type="checkbox" name="chkTAs" id="chkTAs" class="noborder"><font>With reporting TA's</font>					
												</td>
												<td>
													<input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()"><font class="mandatory"><b>&nbsp;*</b></font> 
												</td>
											</tr>
											<tr>
												<td><font>&nbsp;</font></td>
												<td>
													<input type="checkbox" name="chkCOs" id="chkCOs" class="noborder"><font>With reporting CO's</font>
												</td>												
											</tr>
										</table>
									</div>
							 	</td>
							</tr>							
							<tr>
								<td>
									<div id="divAgents">
										<table>
											<tr>
												<td>
													<font class="fntBold">Agents</font>
												</td>
											</tr>
											<tr>
												<td valign="top"><span id="spn1"></span></td>								
											</tr>
										</table>
									</div>								
								</td>
							</tr>							
							
								<tr>
									<td><table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="30%"><font>Station </font></td>
										<td><select name="selStation" id="selStation" size="1" style="width:120px" title="Station">
										<OPTION value=""></OPTION>										
											<c:out value="${requestScope.reqStationList}" escapeXml="false" />
										</select></td>					
										</tr>
										</table>
									</td>	
								</tr>
								<tr>
									<td><table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="30%"><font>Territory </font></td>
										<td><select name="selTerritory" id="selTerritory" size="1" style="width:120px" title="Territory">
										<OPTION value=""></OPTION>										
										<c:out value="${requestScope.reqterritory}" escapeXml="false" />									
										</select></td>					
										</tr>
										</table>
									</td>	
								</tr>
								
								<tr>
								<td><table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="30%">
									<font>Country </font></td>
										<td>
									<select name="selCountry" id="selCountry" size="1" style="width:120px" title="Select From">
										<OPTION value=""></OPTION>										
										<c:out value="${requestScope.reqCountryList}" escapeXml="false"  />
									</select></td>					
										</tr>
										</table>
								</td>
								</tr>
								<tr>
									<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="20%"><font>Created Date From</font></td>
										<td width="55%"><input type="text" name="txtCreatedDate" id="txtCreatedDate" size="10" onBlur="settingValidation('txtCreatedDate')" maxlength="10">
											<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Created Date From"><img SRC="../../images/calendar_no_cache.gif" border="0"></a></td>			
										<td width="20%"></td>	
										<td></td>	
										</tr>
										</table>
									</td>	
								</tr>
								<tr>
									<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="20%"><font>Created Date To</font></td>
										<td width="55%"><input type="text" name="txtCreatedDateTo" id="txtCreatedDateTo" size="10" onBlur="settingValidation('txtCreatedDateTo')" maxlength="10">
											<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Created Date To"><img SRC="../../images/calendar_no_cache.gif" border="0"></a></td>			
										<td width="20%"></td>	
										<td></td>	
										</tr>
										</table>
									</td>	
								</tr>
								<tr>
								<td><table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="30%">
									<font>Status </font></td>
										<td>
									<select name="selStatus" id="selStatus" size="1" style="width:120px" title="Select From">
										<OPTION value=""></OPTION>										
										<option value="ACT">Active</option>
										<option value="INA">In-Active</option>
										<option value="BLK">Black Listed</option>
										<option value="NEW">New</option>
									</select></td>					
										</tr>
										</table>
								</td>
								</tr>
								
								
							<tr><td><font>&nbsp;</font></td></tr>
							
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>				
							<td><font class="fntBold">Output Option</font></td>							
							</tr>
							<tr>
								<td><table width="70%" border="0" cellpadding="0" cellspacing="2"><tr>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
									value="HTML"  class="noBorder" checked><font>HTML</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" 
									value="PDF"  class="noBorder"><font>PDF</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
									value="EXCEL"  class="noBorder"><font>EXCEL</font></td>
								<td width="40%"><input type="radio" name="radReportOption" id="radReportOptionCSV"
									value="CSV"  class="noBorder"><font>CSV</font></td>
								</tr>									
								</table>
								<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
								</td>									
							</tr>
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
									
									<!--input name="btnPrint" type="button" class="Button" id="btnPrint" value="Print" onClick="printClick()"-->
								</td>
								<td align="right">
									<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
								</td>
							</tr>
						</table>
				  		
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
	<script type="text/javascript">
				<c:out value="${requestScope.reqGSAList}" escapeXml="false" />
				
				if (displayAgencyMode == 1 ) {
					document.getElementById('divAgencies').style.display= 'block';
					document.getElementById('divAgents').style.display= 'block';
				} else if (displayAgencyMode == 2 ) {
					document.getElementById('divAgents').style.display= 'block';
					document.getElementById('divAgencies').style.display= 'none';
				} else {
					document.getElementById('divAgencies').style.display= 'none';
					document.getElementById('divAgents').style.display= 'none';
				}	
				
		</script>
<%@ include file="../common/IncludeFormBottom.jsp"%>		
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnAgents" id="hdnAgents" value="">
		<input type="hidden" name="hdnRptType" id="hdnRptType" value="Summary">
		<input type="hidden" name="hdnLive" id="hdnLive" value="">

	</form>
  </body>
	<script src="../../js/reports/GSAAgentDetailsReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
    }
    
   //-->
  </script>
</html>

