<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>IBE Exit Details Report</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css"
	href="../../css/Style_no_cache.css">
<script
	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script type="text/javascript">
	var stns = new Array();
	var agentsArr = new Array();
	var repLive = "<c:out value="${requestScope.hdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
</script>
</head>
<body class="tabBGColor" onunload="beforeUnload()"
	oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)'
	onkeydown="return Body_onKeyDown(event)"
	scroll="no">
 <form action="" method="post" id="frmIbeExitDetailseport"
	name="frmIbeExitDetailseport">

<table width="99%" align="center" border="0" cellpadding="0"
	cellspacing="0">

	<tr>
		<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
	</tr>

	<tr>

		<td><%@ include file="../common/IncludeFormTop.jsp"%>IBE
		Exit Details<%@ include file="../common/IncludeFormHD.jsp"%>
		<table width="100%" border="0" cellpadding="0" cellspacing="2"
			ID="tblHead">
			<tr>
				<td><font class="fntSmall">&nbsp;</font></td>
			</tr>
			<tr>
				<td colspan='4'>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
				
					<tr>
						<td width="24%" align="left"><font>Flight departure From </font><input
									name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFromDate')"  ><font class="mandatory">&nbsp;*</font>
								<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a>&nbsp;					
								</td>	
						<td width="24%" align="left"><font>Flight departure To </font><input name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtToDate')"  ><font class="mandatory">&nbsp;*</font>&nbsp;
								<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a>					
								</td>
					</tr>

				</table>
				</td>
			</tr>

			<tr>
				<td><font>&nbsp;</font></td>
			</tr>
			<tr>
				<td><font>&nbsp;</font></td>
			</tr>

			<tr>
				<td><font class="fntBold">Output Option</font></td>
			</tr>
			<tr>
				<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="2">
					<tr>
						<td width="15%"><input type="radio" name="radReportOption"
							id="radReportOption" value="HTML" class="noBorder" checked><font>HTML</font></td>
						<td width="15%"><input type="radio" name="radReportOption"
							id="radReportOption" value="PDF" class="noBorder"><font>PDF</font></td>
						<td width="15%"><input type="radio" name="radReportOption"
							id="radReportOption" value="EXCEL" class="noBorder"><font>EXCEL</font></td>
						<td><input type="radio" name="radReportOption"
							id="radReportOption" value="CSV" class="noBorder"><font>CSV</font></td>
					</tr>
				</table>

				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width=90%><input type="button" id="btnClose"
					name="btnClose" value="Close" class="Button" onclick="closeClick()"></td>
				<td width=10% align="right"><input type="button" id="btnView"
					name="btnView" value="View" class="Button" onclick="viewClick()"></td>
			</tr>
		</table>
		<%@ include file="../common/IncludeFormBottom.jsp"%></td>
	</tr>
	<tr>
		<td><font class="fntSmall">&nbsp;</font></td>
	</tr>
</table>
<input type="hidden" name="hdnMode" id="hdnMode"> <input
	type="hidden" name="hdnReportView" id="hdnReportView"> <input
	type="hidden" name="hdnLive" id="hdnLive"> <input type="hidden"
	name="hdnSegments" id="hdnSegments" value="">
 </form> 
<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
</body>
<script
	src="../../js/reports/IbeExitDetailsReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript">
<!--
	var objProgressCheck = setInterval("ClearProgressbar()", 300);
	function ClearProgressbar() {
		clearTimeout(objProgressCheck);
		top[2].HideProgress();
	}
	
//-->
</script>
</html>