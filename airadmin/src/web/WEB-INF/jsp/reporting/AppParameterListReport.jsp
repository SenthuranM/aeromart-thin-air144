<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Application Prameter List Report</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css"
	href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css"
	href="../../css/Grid_no_cache.css">
<link href="../../themes/default/css/ui.all_no_cache.css"
	rel="stylesheet" type="text/css" />

<script
	src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script
	src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
<script
	src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script
	src="../../js/v2/schedrept/schedrept.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>

<script type="text/javascript">
	var stns = new Array();
	var agentsArr = new Array();
	var repLive = "<c:out value="${requestScope.hdnLive}" escapeXml="false" />";	
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
</script>
</head>

<body class="tabBGColor" onunload="beforeUnload()"
	oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)'
	onkeydown="return Body_onKeyDown(event)"
	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')"
	scroll="no">
	<form action="" method="post" id="frmAppParametersReport"
		name="frmAppParametersReport">
		<table width="99%" align="center" border="0" cellpadding="0"
			cellspacing="0">

			<tr>
				<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
			</tr>
			<tr>
				<td><%@ include file="../common/IncludeFormTop.jsp"%>Application
					Prameters<%@ include file="../common/IncludeFormHD.jsp"%>
					<table width="40%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td><font>&nbsp;</font></td>
						</tr>
						<tr>
							<td>
								<table width="75%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td><font class="fntBold">Select By</font></td>
										<td><select id="selFields" size="1" name="selFields"
											onClick="" onChange="changeAgencies()" style="width: 100px">
												<option value=""></option>
												<option value="Key">Key</option>
												<option value="Description">Description</option>
										</select></td>
										<td><input name="btnGetParams" type="button"
											class="Button" id="btnGetParams" value="List"
											onClick="getParamsClick()"></td>
										<td>&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
								<td colspan="2">
									<div id="divAgents">
										<table>
											<tr><td>
											<table width="62%" border="0" cellpadding="0" cellspacing="2">	
											<tr>
												<td valign="top" width="2%"><span id="spn1"></span></td>
												<td align="left" valign="bottom"> <font class="mandatory"><b>*</b></font></td></tr>
												</table></td>
											</tr>
										</table>
									</div>
								</td>
							</tr>	
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><font class="fntBold">Output Option</font></td>
						</tr>
						<tr>
							<td>


								<table width="70%" border="0" cellpadding="0" cellspacing="2">
									<tr>
										<td width="15%"><input type="radio"
											name="radReportOption" id="radReportOption" value="HTML"
											class="noBorder" checked><font>HTML</font></td>
										<td width="15%"><input type="radio"
											name="radReportOption" id="radReportOption" value="PDF"
											class="noBorder"><font>PDF</font></td>
										<td width="15%"><input type="radio"
											name="radReportOption" id="radReportOption" value="EXCEL"
											class="noBorder"><font>EXCEL</font></td>
										<td><input type="radio" name="radReportOption"
											id="radReportOption" value="CSV" class="noBorder"><font>CSV</font>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td width=90%><input type="button" id="btnClose"
								name="btnClose" value="Close" class="Button"
								onclick="closeClick()"></td>
							<td width=10% align="right"><input type="button"
								id="btnView" name="btnView" value="View" class="Button"
								onclick="viewClick()"></td>
						</tr>
					</table> <%@ include file="../common/IncludeFormBottom.jsp"%>
				</td>
			</tr>
			<tr>
				<td><font class="fntSmall">&nbsp;</font></td>
			</tr>
		</table>
		<script type="text/javascript">
			<c:out value="${requestScope.reqdata}" escapeXml="false" />
		</script>
		<input type="hidden" name="hdnMode" id="hdnMode"> <input
			type="hidden" name="hdnLive" id="hdnLive"> <input
			type="hidden" name="hdnParams" id="hdnParams" value=""> <input
			type="hidden" name="hdnParamsType" id="hdnParamsType" value="">
	</form>
	<%@ include file="../common/IncludeBottom.jsp"%><!-- Page Background Bottom page -->
</body>

<script
	src="../../js/reports/AppParameterListReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
	type="text/javascript"></script>
<script type="text/javascript">
<!--
	var objProgressCheck = setInterval("ClearProgressbar()", 300);
	function ClearProgressbar() {
		clearTimeout(objProgressCheck);
		top[2].HideProgress();
	}
//-->
</script>
</html>