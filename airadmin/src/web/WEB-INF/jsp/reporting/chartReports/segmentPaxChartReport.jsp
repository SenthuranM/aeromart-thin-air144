<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

	<script type="text/javascript">
		//var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
		//var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	</script>
	
  </head>
  
  <body class="tabBGColor" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" onbeforeunload="beforeUnload()"  scroll="no">
  	<form name="frmChart" id="frmChart" action="" method="POST">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">		
		 <table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>				
					<td>					
						<%@ include file="../../common/IncludeFormTop.jsp"%>Top Performing Segment Chart<%@ include file="../../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2"">					  		
							<tr>
								<td width="60%"></td>
					
							</tr>
							<tr>
								<td colspan="2">
									<table border="0" cellpadding="0" cellspacing="2" width="100%">
										<tr><td colspan="4"><font class="fntBold">Date Range</font></td><td></td></tr>
										
										<tr><td width="13%" align="left"><font>From &nbsp;&nbsp;</font>
											<input name="txtFromDate" type="text" id="txtFromDate" size="10"  maxlength="10" onBlur="settingValidation('txtFromDate')" onchange="dataChanged()"></td>
											<td width="8%" align="left"><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"> &nbsp;* </font></td>
											<td width="11%"><font>To &nbsp;&nbsp;</font>
											<input	name="txtToDate" type="text" id="txtToDate" size="10" maxlength="10" onBlur="settingValidation('txtToDate')" onchange="dataChanged()"></td>
											<td align="left"><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"> &nbsp;* </font></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td colspan="2">
								<span id = "imgSpn1">								
								 <img id = "chartImg1"/>
								 </span>
								</td>					
							</tr>
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>								
								<td colspan="2">
									<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
								</td>
							</tr>
							
						</table>
						
				  		<%@ include file="../../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>
	
			
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnFromDate" id="hdnFromDate" value="">
		<input type="hidden" name="hdnToDate" id="hdnToDate" value="">

	</form>
  </body>
  <script src="../../js/reports/chartReports/segmentPaxChartReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  <script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
   //-->
  </script>
</html>
