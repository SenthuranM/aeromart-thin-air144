<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>Mode of Payments</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<link href="../../themes/default/css/ui.all_no_cache.css" rel="stylesheet" type="text/css" />
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/v2/schedrept/schedrept.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

	
	<script type="text/javascript">
		var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
		var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
	</script>
</head>
	<body class="tabBGColor" onbeforeunload="beforeUnload()" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad()" scroll="no">
		<form action="" method="post" id="frmMonitorPerformance" name="frmMonitorPerformance">
			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">	
				<tr>
					<td><font class="fntSmall">&nbsp;</font></td>
				</tr>
				<tr>
					<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
				</tr>
				<tr>
					<td><%@ include file="../common/IncludeFormTop.jsp"%>Performance of staff<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblHead">
							<tr>
								<td><font class="fntBold">Report Period</font></td>				
							</tr>
							<tr>
								<td>
									<table width="80%" border="0" cellpadding="0" cellspacing="2">
										<tr>
											<td width="15%"><input type="radio" name="radReportPeriod" id="radReportPeriodT"
												value="Today" onClick="chkClickPeriod()" class="noBorder" checked><font>Today</font></td>
											<td width="20%"><input type="radio" name="radReportPeriod" id="radReportPeriodTW"
												value="ThisWeek" onClick="chkClickPeriod()" class="noBorder"><font>This Week</font></td>
											<td width="22%"><input type="radio" name="radReportPeriod" id="radReportPeriodTM"
												value="ThisMonth" onClick="chkClickPeriod()" class="noBorder"><font>This Month</font></td>
											<td width="43%"><input type="radio" name="radReportPeriod" id="radReportPeriodTY"
												value="ThisYear" onClick="chkClickPeriod()" class="noBorder"><font>This Year</font></td>
										</tr>													
									</table>
								</td>
							</tr>				
							<tr>
								<td>
									<table width="80%" border="0" cellpadding="0" cellspacing="2">	
										<tr>
											<td width="16%"><input type="radio" name="radReportPeriod" id="radReportPeriodO"
												value="Other" onClick="chkClickPeriod()" class="noBorder"><font>Other</font></td>
											<td width="24%" align="left"><font>From </font><input
												name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtFromDate')"  ></td>
											
											<td width="19%"><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a></td>
											
											<td width="21%"><font>To </font><input
												name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onblur="dateChk('txtToDate')"></td>
											<td width="35%"><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr><td>&nbsp;</td></tr>			
							<tr>
								<td><font class="fntBold"></font></td>
								<td><font class="fntBold"></font></td>			
							</tr>
							<tr>
								<td><font>Agencies</font>
									<select id="selAgencies" size="1" name="selAgencies" style="width:100px" onChange="AgentOnChange()">
									<option value=""></option>
									<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />					
									</select><font class="mandatory">&nbsp*</font>&nbsp;								
								<input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()"></td>
							</tr>
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
									<font class="fntBold">Agents</font>
								</td>
							</tr>
							<tr>
								<td valign="top"><span id="spn1"></span></td>
							</tr>
							<tr><td>&nbsp;</td></tr>	
							<tr>
								<td>
									<font class="fntBold">Select Payment Type</font>
								</td>
							</tr>
							<tr>
								<td valign="top"><span id="spnPaymentTypes"></span></td>
							</tr>						
							<tr>				
								<td><font class="fntBold">Output Option</font></td>							
							</tr>
							<tr>
								<td>
									<table width="70%" border="0" cellpadding="0" cellspacing="2">
										<tr>
											<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
												value="HTML" class="noBorder" checked><font>HTML</font></td>
											<td width="20%"><input type="radio" name="radReportOption" id="radReportOption" 
												value="PDF"  class="noBorder"><font>PDF</font></td>
											<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
												value="EXCEL" class="noBorder"><font>EXCEL</font></td>
											<td width="40%"><input type="radio" name="radReportOption" id="radReportOption"
												value="CSV" class="noBorder"><font>CSV</font></td>
										</tr>									
									</table>
									<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
								</td>									
							</tr>
							<tr><td>&nbsp;</td></tr>	
							<tr>	
								<td>
									<table border="0" cellpadding="0" cellspacing="2">						
										<tr>
											<td width="80%"><input type="button" id="btnClose"  name="btnClose" value="Close" class="Button" onclick="closeClick()"></td>
											<td width="10%" align="right">
												<u:hasPrivilege privilegeId="rpt.sch.staffperformance">	
													<div id="divSchedFrom"></div>
													<input name="btnSched" type="button" class="Button schdReptButton" id="btnSched" value="Schedule" onClick="viewClick(true)">
												</u:hasPrivilege>
											</td>
											<td width="10%" align="right">
												<input type="button" id="btnView"  name="btnView" value="View" class="Button" onclick="viewClick()">
											</td>
										</tr>
									</table>
								</td>	
							</tr>	
						</table>		
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr>
					<td><font class="fntSmall">&nbsp;</font></td>
				</tr>
			</table>
			<script type="text/javascript">			
					<c:out value="${requestScope.reqAgentUser}" escapeXml="false" />			
					<c:out value="${requestScope.reqBookingType}" escapeXml="false" />
			</script>
			<input type="hidden" name="hdnMode" id="hdnMode">
			<input type="hidden" name="hdnUserCode" id="hdnUserCode"/>
			<input type="hidden" name="hdnBookingPaymentType" id="hdnBookingPaymentType"/>
			<input type="hidden" name="hdnLive" id="hdnLive" value="">			
		</form>
	</body>
	<script src="../../js/reports/MonitorPerformanceOfSalesStaff.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript">
		<!--
		   var objProgressCheck = setInterval("ClearProgressbar()", 300);
		   function ClearProgressbar(){
		      clearTimeout(objProgressCheck);
		      top[2].HideProgress();
		   }
		//-->
	</script>
</html>