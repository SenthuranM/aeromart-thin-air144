<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Mode Of Payment</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<link href="../../themes/default/css/ui.all_no_cache.css" rel="stylesheet" type="text/css" />
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/DynaTab.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  	    	
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/v2/schedrept/schedrept.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
	<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
</script>
  </head>
  <body class="tabBGColor" onbeforeunload="beforeUnload()" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)" onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" scroll="no">
  	<form name="frmPage" id="frmPage" action="" method="post">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">		
		 <table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">				
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Mode Of Payment<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
					  		<tr>
								<td><font class="fntBold">Date Range</font></td>				
							</tr>
							<tr>
								<td>
									<table width="62%" border="0" cellpadding="0" cellspacing="2">	
										<tr><td width="25%" align="left"><font>From &nbsp;</font><input name="txtFromDate" type="text" id="txtFromDate" size="10" style="width:75px;" maxlength="10" onBlur="dateChk('txtFromDate')" invalidText="true"></td>
											<td width="10%" align="left"><a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
											<td width="20%"><font>To </font><input	name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;" maxlength="10" onBlur="dateChk('txtToDate')" invalidText="true"></td>
											<td width="10%" align="left"><a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0" border="0"></a><font class="mandatory"><b>*</b></font></td>
										</tr>
									</table>
								</td>
								<td></td>
							</tr>
							<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
							<tr>
								<td>
									<font class="fntBold">Mode of Payment</font>
								</td>
							</tr>
							<tr>
							<td>
							<table width="70%" border="0" cellpadding="0" cellspacing="2"><tr>
								<td valign="top"><span id="spn2"></span></td>
								<td align="left" valign="bottom"><font class="mandatory"><b>*</b></font></td>
								</tr>
							</table>
							</td>
								</tr>
							<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
							<tr>
								<td>
								<table width="70%" border="0" cellpadding="0" cellspacing="2">
									<tr>
										<td><font class="fntBold">Entity</font></td>
										<td><select id="selEntity" size="1" name="selEntity"  style="width:100px">									
												<c:out value="${requestScope.reqEntities}" escapeXml="false" />					
											</select>
										</td>
									</tr>
								</table>								
								</td>
							</tr>								
							<tr>
								<td><font class="fntBold">Report Options</font></td>
								
							</tr>
							<tr>
								<td>
								<input type="radio" name="chkOption" id="chkOption" value="PM"  class="noBorder" onClick="radioReportChanged()" checked><font>By Payment Modes</font>
									<font class="fntSmall">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font>
								<input type="radio" name="chkOption" id="chkOption1" value="AC" class="noBorder" onClick="radioReportChanged()"><font>By Users</font>
									<font class="fntSmall">&nbsp;&nbsp;&nbsp;&nbsp;</font>
								<input type="radio" name="chkOption" id="chkOption2" value="AN" class="noBorder" onClick="radioReportChanged()"><font>By Agent Users</font>
								<!-- 	<font class="fntSmall">&nbsp;&nbsp;&nbsp;</font>
								<input type="radio" name="chkOption" id="chkOption3" value="EP" class="noBorder" onClick="radioReportChanged()"><font>External Payments</font>  -->
									<font class="fntSmall">&nbsp;&nbsp;&nbsp;</font>
								<input type="radio" name="chkOption" id="chkOption4" value="BC" class="noBorder" onClick="radioReportChanged()"><font>By Currency</font>
							</tr>
							<tr>
								<td><font class="fntBold">Time Options</font></td>
								
							</tr>
							<tr>
								<td>
									<input type="radio" name="chkZulu" id="chkZulu" value="ZULU"  class="noBorder" onClick="zuluClick()" checked><font>Zulu</font>
									<font class="fntSmall">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font>
									<input type="radio" name="chkZulu" id="chkZulu1" value="LOCAL" class="noBorder" onClick="localClick()"><font>Local</font>
								</td>
							</tr>							
							<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>

							<tr>
								<td width="70%">
									<font class="fntBold">Output Option</font>
								</td>
							</tr>
							<tr>
								<td><table width="70%" border="0" cellpadding="0" cellspacing="2"><tr>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"	value="HTML"  class="noBorder" checked><font>HTML</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" value="PDF"  class="noBorder"><font>PDF</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL" value="EXCEL"  class="noBorder"><font>EXCEL</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionCSV" value="CSV"  class="noBorder"><font>CSV</font></td>
							</tr>
							<tr>
							<td colspan="2" >
							<u:hasPrivilege privilegeId="rpt.rev.new.view">
								<font>New View</font>
								</u:hasPrivilege>
							</td>
							<td colspan="2" align='left'>
								<u:hasPrivilege privilegeId="rpt.rev.new.view">
								<input type="checkbox" name="chkNewview" id="chkNewview" value="on" class="NoBorder">
								</u:hasPrivilege>
							</td>
							</tr>									
						</table>
						<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
					</td>						 
				</tr>
				<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
				<tr>
					<td align="left">
						<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
					</td>
					<td align="right">
						<u:hasPrivilege privilegeId="rpt.sch.modeofPay">	
						<div id="divSchedFrom"></div>
						<input name="btnSched" type="button" class="Button schdReptButton" id="btnSched"
							value="Schedule" onClick="viewClick(true)">
						</u:hasPrivilege>	
					</td>
					<td align="right">
						<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick(false)">
					</td>
				</tr>
				<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
			</table>
	  		<%@ include file="../common/IncludeFormBottom.jsp"%>
		</td>
	</tr>
	<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
</table>
	<script type="text/javascript">
		<c:out value="${requestScope.reqGSAList}" escapeXml="false" />
		<c:out value="${requestScope.reqPaymentType}" escapeXml="false" />
	</script>		
	<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
	<input type="hidden" name="hdnAgents" id="hdnAgents" value="">
	<input type="hidden" name="hdnPayments" id="hdnPayments" value="">
	<input type="hidden" name="hdnRptType" id="hdnRptType" value="">
	<input type="hidden" name="hdnAgentName" id="hdnAgentName" value="">
	<input type="hidden" name="hdnPaymentDesc" id="hdnPaymentDesc" value="">
	<input type="hidden" name="hdnTime" id="hdnTime" value="">
	<input type="hidden" name="hdnLive" id="hdnLive" value="">
	<input type="hidden" name="hdnEntity" id="hdnEntity" value="">
</form>
  </body>
  <script src="../../js/reports/ModeOfPayment.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
  <script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
   }
   //-->
  </script>
</html>
