<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>Ticket wise Forward Sales Report</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
</head>
<body class="tabBGColor" style="align:left" onkeypress='return Body_onKeyPress(event)' onkeydown="return Body_onKeyDown(event)"
	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')" >
	<%@ include file="../common/IncludeTop.jsp"%><!-- Page Background Top page -->
  	<form name="frmTicketWiseForwardSalesReport" id="frmTicketWiseForwardSalesReport" action="" method="post">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">
		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td><font class="fntSmall">&nbsp;</font></td>
				</tr>
				<tr>
					<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Ticket wise Forward Sales Report<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
					  		<tr>			
								<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="2">
								<tr>
									<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="15%"></td>
										<td width="55%"></td>		
										<td width="10%"></td>
										<td width="20%"></td>										
										</tr>
										</table>							
									</td>
								</tr>								
								
								<tr>
									<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="15%"><font class="fntBold">From Sales Date </font></td>
										<td width="55%"><input type="text" name="txtFromDate" id="txtFromDate" size="10" maxlength="10">
											<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0"><font class="mandatory"> &nbsp;* </font></a></td>			
										<td width="20%"></td>	
										<td width="20%"></td>	
										</tr>
										</table>
									</td>	
								</tr>
								<tr>
									<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="15%"><font class="fntBold">To Sales Date </font></td>
										<td width="55%"><input type="text" name="txtToDate" id="txtToDate" maxlength="10" size="10">
											<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="Date To"><img SRC="../../images/calendar_no_cache.gif" border="0"></a><font class="mandatory"> &nbsp;* </font></td>					
										
										<td width="20%"></td>
										<td width="20%"></td> 
										</tr>
										</table>
									</td>	
								</tr>											
								<tr>
									<td>
										<table width="100%" border="0" cellpadding="0" cellspacing="2">
											<tr>
											<td width = 30%><font class="fntBold">Country</font></td>
											<td width = 30%>
												<select name="selCountry" size="1" id="selCountry" style="width:55px;">
													<option value=""></option>		
													<c:out value="${requestScope.reqCountryList}" escapeXml="false" />
												</select>
											</td>
											<td width = 20%><font class="fntBold">Stations</font></td>
											<td width = 20%>	
												<select name="selStations" size="1" id="selStations" style="width:55px;">
												<option value=""></option>		
												<c:out value="${requestScope.reqStationList}" escapeXml="false" />
												</select>
											</td>
											</tr>									
										</table>
									</td>									
								</tr>
								<tr><td>
									<table width="100%" border="0" cellpadding="0" cellspacing="2">
										<tr>
											<td width = 30%><font class="fntBold">From E-Ticket Number</font>
											</td>
											<td width="20%"><input type="text" name="txtFromETktNumber" id="txtFromDate" size="13"  maxlength="13">
											</td>
											<td width = 30%><font class="fntBold">To E-Ticket Number</font></td>
											<td width = 20%"><input type="text" name="txtToETktNumber" id="txtFromDate" size="13"  maxlength="13">
											</td>
											</tr>									
									</table></td>
								</tr>
								</table>
								<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
								</td>									
							</tr>
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td width="60%">
									<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">										
								</td>
								<td width="40%">
									<input name="btnView" type="button" align="left" class="Button" id="btnView" value="View" onClick="viewClick()">
								</td>		
							</tr>
						</table>
				  		
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>	
				<%@ include file="../common/IncludeFormBottom.jsp"%>	
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnLive" id="hdnLive" value="">
</form>		
</body>		
<script src="../../js/reports/TicketWiseForwardSalesReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript">
</script>
</html>
