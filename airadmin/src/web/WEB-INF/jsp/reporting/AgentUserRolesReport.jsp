<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Insert title here</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">

<LINK rel="stylesheet" type="text/css"
	href="../../css/Style_no_cache.css">
<LINK rel="stylesheet" type="text/css"
	href="../../themes/default/css/ui.all_no_cache.css">


	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>	
	<script src="../../js/common/combo.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

<script type="text/javascript"src="../../js/common/stringRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' /> "type="text/javascript"></script>
<script type="text/javascript"src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"type="text/javascript"></script>
<script type="text/javascript"src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' /> "type="text/javascript"></script>
<script type="text/javascript"src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"type="text/javascript"></script>
<script type="text/javascript"src="../../js/v2/jquery/jquery.themes.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"type="text/javascript"></script>
<script type="text/javascript"src="../../js/v2/jquery/i18n/grid.locale-en.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"type="text/javascript"></script>
<script type="text/javascript"src="../../js/v2/jquery/jquery.jqGridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"type="text/javascript"></script>
<script type="text/javascript"src="../../js/v2/jquery/jquery.form_last_worked_ver.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"type="text/javascript"></script>
<script type="text/javascript"src="../../js/v2/isalibs/isa.jquery.decorator.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"type="text/javascript"></script>

</head>
<body class="tabBGColor" scroll="no" onkeypress=''
	oncontextmenu="return false" onkeydown='' ondrag='return false'>
	<div style="margin-top: 0px;">
		<div id="divSearch" style="height: 60px; text-align: left;">
			<form method="post" action="showAgentUserRolesReport.action"
				id="frmAgentUserRolesReport">
				<table width="96%" border="0" cellpadding="0" cellspacing="18"
					ID="Table9">
					<tr>
						<td align="center" class="fntBold" width="2%"><font>Privilege</font></td>
						<td colspan="2" align="left" width="10%"><select
							id="searchPrivilege" name="searchPrivilege" size="1"
							style="width: 200px" tabindex="1">
								<option value="-1" hidden>Select Privilege</option>
								<c:out value="${requestScope.actPrivilegeData}"
									escapeXml="false" />
						</select></td>

						<td align="right" width="10%"><input name="btnSearch"
							type="button" id="btnSearch" value="Search"
							class="Button" tabindex="2"></td>
					</tr>
				</table>
			</form>
		</div>

		<div id="divResultsPanel"
			style="height: 420px; text-align: left; background-color: #ECECEC;">
			<table id="tblAgentUserRolesReport" class="scroll" cellpadding="0"
				cellspacing="0" width='98%'></table>
			<div id=divAirportMsgPager class="scroll" style="text-align: center;"></div>

		</div>

		<table width="98%" border="0" cellpadding="0" cellspacing="2"
			align="center">
			<tr>
				<td colspan="3" style="height: 42px;" valign="bottom"><input
					name="btnClose" type="button" id="btnClose" value="Close"
					onclick="top[1].objTMenu.tabRemove(screenId);"
					class="Button" tabindex="3" />
			</tr>
		</table>

	</div>
	<script type="text/javascript">
		var screenId = 'UC_REPM_096';
		top[2].HideProgress();
	</script>
	<script
		src="../../js/reports/AgentUserRolesReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
		type="text/javascript"></script>


</body>
</html>