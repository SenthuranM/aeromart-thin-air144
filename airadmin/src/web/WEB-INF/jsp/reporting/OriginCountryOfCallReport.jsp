<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<html>
<head>
    <title>Origin Country Of Call</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
    <LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
    <LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
    <link href="../../themes/default/css/ui.all_no_cache.css" rel="stylesheet" type="text/css" />
    <script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
    <script src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
    <script src="../../js/v2/schedrept/schedrept.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

    <script type="text/javascript">
        var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
        var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
        <c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
    </script>
</head>

<body onload="pageLoadSC()" class="tabBGColor" onbeforeunload="beforeUnload()" oncontextmenu="return false" scroll="no">
<div id='Country' name='Country' style='position: absolute; z-index:1000; left:30%; top:25%; height: 200px; width: 500px; visibility:hidden;'></div>
<form name="frmOriginCountry" id="frmOriginCountry" action="" method="post">
    <input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">
    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">

        <tr>
            <td>
                <%@ include file="../common/IncludeMandatoryText.jsp" %>
            </td>
        </tr>
        <tr>
            <td>
                <%@ include file="../common/IncludeFormTop.jsp" %>
                Origin Country Of Call
                <%@ include file="../common/IncludeFormHD.jsp" %>
                <table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="25%"><font class="fntBold">Date Range</font></td>

                                    <td width="30%" align="left"><font>From </font>
                                        <input name="txtFromDate" type="text" id="txtFromDate" size="10"
                                               style="width:75px;" maxlength="10" onblur="dateChk('txtFromDate')"
                                               invalidText="true">
                                    </td>
                                    <td width="10%">
                                        <a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;"
                                           title="Date From"><img SRC="../../images/calendar_no_cache.gif" border="0"
                                                                  border="0"></a><font class="mandatory"><b>*</b></font>
                                    </td>
                                    <td width="25%"><font>To </font>
                                        <input name="txtToDate" type="text" id="txtToDate" size="10" style="width:75px;"
                                               maxlength="10" onblur="dateChk('txtToDate')" invalidText="true">
                                    </td>
                                    <td width="10%"><a href="javascript:void(0)"
                                                       onclick="LoadCalendarTo(1,event); return false;"
                                                       title="Date From"><img SRC="../../images/calendar_no_cache.gif"
                                                                              border="0" border="0"></a><font
                                            class="mandatory"><b>*</b></font></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr>
                        <td>
                            <font class="fntBold">Origin Country</font>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width =%60 >
                                <tr>
                                    <td width =%40 >
                                        <input id="btnCountries" type="button" value= "Select Countries" class="Button" onClick="return showCountryMultiSelect(true);" style="height:20px ;width:160px; " />
                                    </td>
                                    <td width =%60 >
                                        <select id="selectedCntries" size="3" name="selectedCntries" style="height:70px; width:150px;" multiple ="multiple">
                                        </select>
                                    </td>
                                    <td valign="bottom"><font class="mandatory">&nbsp;*</font></td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr>
                        <td width="60%">
                            <font class="fntBold">Output Option</font>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="2">
                                <tr>
                                    <td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
                                                           value="HTML" class="noBorder"><font>HTML</font></td>
                                    <td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF"
                                                           value="PDF" class="noBorder"><font>PDF</font></td>
                                    <td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
                                                           value="EXCEL" class="noBorder"><font>EXCEL</font></td>
                                    <td width="40%"><input type="radio" name="radReportOption" id="radReportOptionCSV"
                                                           value="CSV" class="noBorder" checked><font>CSV</font></td>
                                </tr>
                            </table>

                            <c:out value="${requestScope.rptFormatOption}" escapeXml="false"/>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input name="btnClose" type="button" class="Button" id="btnClose" value="Close"
                                   onclick="closeClick()">
                        </td>
                        <td align="right" width="50%">

                            <div id="divSchedFrom"></div>
                            <input name="btnSched" type="button" class="Button schdReptButton" id="btnSched"
                                   value="Schedule" onClick="viewClick(true)">
                        </td>
                        <td width="50%" align="right">
                            <input name="btnView" type="button" class="Button" id="btnView" value="View"
                                   onClick="viewClick(false)">
                        </td>
                    </tr>
                </table>

                <%@ include file="../common/IncludeFormBottom.jsp"%>
            </td>
        </tr>
        <tr>
            <td><font class="fntSmall">&nbsp;</font></td>
        </tr>
    </table>
    <script type="text/javascript">
        <c:out value="${requestScope.reqCountryList}" escapeXml="false"></c:out>

    </script>
    <input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
    <input type="hidden" name="hdnCountries" id="hdnCountry" value="">


</form>


</body>

<script src="../../js/reports/OriginCountryOfCallReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"
        type="text/javascript"></script>
<script type="text/javascript">
    <!--
    var objProgressCheck = setInterval("ClearProgressbar()", 300);
    function ClearProgressbar() {
        clearTimeout(objProgressCheck);
        top[2].HideProgress();
    }
    //-->


</script>
</html>
