
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
	

<script type="text/javascript">
	var repLive = "<c:out value="${requestScope.reqHdnLive}" escapeXml="false" />";
	var repStartDate = "<c:out value="${requestScope.reqStartDay}" escapeXml="false" />";
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
		var selectBox;		
		
</script>
  </head>
  <body class="tabBGColor" oncontextmenu="return false" onbeforeunload="beforeUnload()" onload="winOnLoad()">
  	<form name="frmFareRuleDetails" id="frmFareRuleDetails"  method="post">
		<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">  		
		 <script type="text/javascript">
		</script>

		<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Fare Rule Details<%@ include file="../common/IncludeFormHD.jsp"%>
				  		<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="tblReservations">
					  		<tr>			
							
								<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="2">
									<td><table width="100%" border="0" cellpadding="0" cellspacing="2">
										<tr>
										<td width="15%"></td>
										<td width="55%"></td>		
										<td width="10%"></td>
										<td width="20%"></td>										
										</tr>
										</table>
								
								</td>
								<tr>
															
								<td>
								<table width="100%" border="0" cellpadding="0" cellspacing="2">
								 <tr>
								 		<td>
								 			<table width="100%" border="0" cellpadding="0" cellspacing="2">
												<tr>
													<td align="left"><font class="fntBold">Fare Rule Selection  <font class="mandatory"> &nbsp;* </font></font></td>
												</tr>
												<tr>
													<td valign="top" align="center">
															<span id="spnFareRules"></span>
													</td>
												</tr>
											</table>
										
										</td>
										<td>
											<table width="100%" border="0" cellpadding="0" cellspacing="2">
												<tr>
													<td align="left"><font class="fntBold">Visibility </font></font></td>
												</tr>
												<tr>
													<td valign="top" align="center">																									
														<span id="spnFareVisibility"></span>
													</td>
												</tr>
											</table>
										
										</td>
								 </tr>
								 
								 <tr><td><font>&nbsp;</font></td></tr>								 
								 
								 <tr>
								 		<td >
								 			<table width="100%" border="0" cellpadding="0" cellspacing="2">
												<tr>
												<td width="15%"><font class="fntBold">Status </font></td>
												<td width="55%">													
													<select name="selFareRuleStatus" id="selFareRuleStatus" size="1" style="width:120px" title="Select Status">
														<OPTION value="ACTIVE" selected="selected">Active</OPTION>
														<OPTION value="INACTIVE">In-Active</OPTION>
														<OPTION value="ALL">All</OPTION>														
													</select>
												</td>							
												<td width="10%"></td>
												<td width="20%"></td>	
												</tr>
												</table>
										
										</td>
										<td >
								 			<table width="100%" border="0" cellpadding="0" cellspacing="2">
												<tr>
												<td width="15%"><font class="fntBold">Oneway/Return </font></font></td>
												<td width="55%">
													<select name="selFlightWay" id="selFareRuleWay" size="1" style="width:120px" title="Select Way">
														<OPTION value=""></OPTION>
														<OPTION value="ALL">All</OPTION>
														<OPTION value="ONEWAY">Oneway</OPTION>
														<OPTION value="RETURN">Return</OPTION>														
													</select>
												</td>							
												<td width="10%"></td>
												<td width="20%"></td>	
												</tr>
												</table>
										
										</td>
								 </tr>				
								 
								<tr><td><font>&nbsp;</font></td></tr>
								 
								<tr>
								<td colspan="2">
									<table width="75%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td>
												<font class="fntBold">Agencies</font>
													<select id="selAgencies" size="1" name="selAgencies" onClick="clickAgencies()" onChange="changeAgencies()" style="width:76px">
													<option value=""></option>
													<option value="All">All</option>
													<c:out value="${requestScope.reqAgentTypeList}" escapeXml="false" />					
												</select>
											</td>
											<td>
												<input type="checkbox" name="chkTAs" id="chkTAs" class="noborder"><font>With reporting TA's</font>					
											</td>
											<td>
												<input name="btnGetAgent" type="button" class="Button" id="btnGetAgent" value="List" onClick="getAgentClick()">
											</td>
										</tr>
										<tr>
											<td><font>&nbsp;</font></td>
											<td>
												<input type="checkbox" name="chkCOs" id="chkCOs" class="noborder"><font>With reporting CO's</font>
											</td>												
										</tr>
									</table>
								</td>
							</tr>							
							<tr>
								<td colspan="2">
									<font class="fntBold">Agents</font>
								</td>
							</tr>
							<tr>
								<td colspan="2" valign="top"><span id="spn1"></span></td>								
							</tr>						 
								
							</table>
								</td>
							</tr>
								<tr>				
							<td><font class="fntBold">Output Option</font></td>							
							</tr>							
							<tr>
								<td><table width="40%" border="0" cellpadding="0" cellspacing="2"><tr>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOption"
									value="HTML" class="noBorder" checked><font>HTML</font></td>
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionPDF" 
									value="PDF" class="noBorder"><font>PDF</font></td>
								<%--
								<td width="20%"><input type="radio" name="radReportOption" id="radReportOptionEXCEL"
									value="EXCEL" class="noBorder"><font>EXCEL</font></td>
								<td><input type="radio" name="radReportOption" id="radReportOptionCSV"
									value="CSV" class="noBorder"><font>CSV</font></td>								
								 --%>									
							</tr>	
								</table>
								<c:out value="${requestScope.rptFormatOption}" escapeXml="false" />	
								</td>									
							</tr>							
							<tr>
								<td>
									<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
									
									<!--input name="btnPrint" type="button" class="Button" id="btnPrint" value="Print" onClick="printClick()"-->
								</td>
								<td align="right">
									<input name="btnView" type="button" class="Button" id="btnView" value="View" onClick="viewClick()">
								</td>
							</tr>
						</table>
						
				  		
					</td>
				</tr>				
			</table>	
		<%@ include file="../common/IncludeFormBottom.jsp"%>	
		<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
		<input type="hidden" name="hdnModel" id="hdnModel" value="">
		<input type="hidden" name="hdnVersion" id="hdnVersion" value="">		
		<input type="hidden" name="hdnLive" id="hdnLive" value="">
		<input type="hidden" name="hdnAgents" id="hdnAgents" value="">
		<input type="hidden" name="hdnRptType" id="hdnRptType" value="">
		<input type="hidden" name="hdnAgentName" id="hdnAgentName" value="">
		
		<!-- Hidden Values for the Multi Select boxes -->
		<input type="hidden" name="hdnFareRules" id="hdnFareRules" >
		<input type="hidden" name="hdnFareRuleVisibility" id="hdnFareRuleVisibility" >
		<input type="hidden" name="hdnFareAgents" id="hdnFareAgents" >
		
		    <script type="text/javascript">
				    <c:out value="${requestScope.reqGSAList}" escapeXml="false" />
					<c:out value="${requestScope.reqPaymentType}" escapeXml="false" />
					
					<c:out value="${requestScope.reqFareRuleMultiList}" escapeXml="false" />
					<c:out value="${requestScope.reqFareVisibilityMultiList}" escapeXml="false" />

					<c:out value="${requestScope.reqAgentList}" escapeXml="false" />					
			</script>

	</form>
  </body>
	<script src="../../js/reports/FareRuleDetailsReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
<script type="text/javascript">
   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);
   function ClearProgressbar(){
      clearTimeout(objProgressCheck);
      top[2].HideProgress();
 
   }
    
   //-->
  </script>
</html>

