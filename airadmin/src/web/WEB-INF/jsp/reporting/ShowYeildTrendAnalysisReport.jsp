 <%-- 
	 @Author 	: 	Chamindap
  --%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>

<html>
  <head>
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">	
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

	<script src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.form.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jQuery.print.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/v2/jquery/jquery.popupwindow.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
   
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript">
 	google.load('visualization', '1', {packages: ['barchart','piechart','columnchart','linechart','table']});
    </script>
<style type='text/css'>
  Body {background :#F7F7F7;margin :0px 0px 0px 0px;}

  .large-font {
    font-size: 12px;
    text-align: center;
  }

  .headerRow-font {
    font-style: normal;
    color: black;
  }

  .headerCell-border {
     border: 1px solid #8B8C8B;
     background-color: #D9DBD9;
     width : 20;
  }

  .selectedTableRow-background {
    background-color: #868786;
  }

  .tableRow-background {
    background-color: #F0F2F0;
    border: 1px solid #8B8C8B;
  }

  .oddTableRow-background {
    background-color: #DFE0DE;
  }

</style>

  </head>

  <body>
  
        <div id='divLoadMsg' style='visibility:hidden;z-index:1000; background:transparent;'>
		<iframe src="LoadMsg" id='frmLoadMsg' name='frmLoadMsg'  frameborder='0' scrolling='no' style='position:absolute; top:50%; left:40%; width:260; height:40;'></iframe>
	</div>


<div id="main_tbl" style="display:none;">

<table width='100%' align='center' border='0' cellpadding='0' cellspacing='0' style='background-color:#F7F7F7'>
   <tr>
     <td colspan='3'>
       &nbsp;&nbsp;&nbsp;
     </td>
   </tr>
   <tr>
     <td colspan='3'>
       <!--div id="rpt_name"/-->
     </td>
   </tr>

   <tr>
     <td colspan='3'>
       
								<table width='100%' border='0' cellpadding='0' cellspacing='0'>
									<tr>	
										<td style='height:12px;background-image:url(../../images/M015_no_cache.jpg);background-repeat:repeat-x' valign='top'><img src='../../images/M014_no_cache.jpg'></td>
										<td style='background-image:url(../../images/M015_no_cache.jpg);background-repeat:repeat-x'></td>
										<td style='background-image:url(../../images/M015_no_cache.jpg);background-repeat:repeat-x' valign='top' align='right'><img src='../../images/M016_no_cache.jpg'></td>
									</tr>
									<tr>	
										<td style='height:12px;background-image:url(../../images/M017_no_cache.jpg);background-repeat:repeat-y;background-color:#F7F7F7;' valign='top'></td>
										<td width='100%' style='background-color:#F7F7F7;'>

       <table width='100%' align='center' class='fntSmall' border='0' cellpadding='0' cellspacing='0' style='background-color:#F7F7F7'>
	   <tr>
	     <td colspan='3'>
	       <div id="rpt_name"/>
	     </td>
	   </tr>
          <tr>
                <td width="20%">&nbsp;
                </td> 
                <td>
                    <font class="fntSmall">&nbsp;<div id="chart_div"></font></div>
    		</td>
                <td width="20%">&nbsp;
                </td> 
          </tr>
	  <tr>
                <td  colspan="3">&nbsp;
                </td> 
          </tr>
	 <tr>
                <td  colspan="3">&nbsp;
                </td> 
          </tr>
	  <tr>
                <td  colspan="3">&nbsp;
		<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()">
		</td>
	  </tr>
	  <tr>
                <td  colspan="3">&nbsp;
                </td> 
          </tr>
      </table>

										</td>
										<td style='background-image:url(../../images/M018_no_cache.jpg);background-repeat:repeat-y;background-position:right;background-color:#F7F7F7;' valign='top' align='right'></td>
									</tr>
									<tr>	
										<td style='height:12px;background-image:url(../../images/M020_no_cache.jpg);background-repeat:repeat-x' valign='top'><img src='../../images/M019_no_cache.jpg'></td>
										<td style='background-image:url(../../images/M020_no_cache.jpg);background-repeat:repeat-x'></td>
										<td style='background-image:url(../../images/M020_no_cache.jpg);background-repeat:repeat-x' valign='top' align='right'><img src='../../images/M021_no_cache.jpg'></td>
									</tr>
								</table>

     </td>
  </tr>

  <tr>
      <td>&nbsp;</td>
  </tr>

  <tr>
      <td>
	<table width="70%" border="0" cellpadding="0" cellspacing="2">
	  <tr>
	    <td align="left" >
               <!--input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="closeClick()"-->
            </td>
	 </tr>
        </table>
     </td>
 </tr>
</table>
</div>

 <script type='text/javascript'
	src='../../js/reports/ShowYieldTrendAnalysisReport.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />'></script>	
  </body>
</html>