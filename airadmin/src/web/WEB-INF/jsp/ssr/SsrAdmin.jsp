<%@ page language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>SSR Admin</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/htmlRoutines.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		
  </head>
 <body class="tabBGColor" scroll="no"  onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false' 
  	onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
 
  <form method="post" action="showSsr.action" id="frmSSR" name="frmSSR">
  		
  			<script type="text/javascript">
  				var arrData = new Array();
				<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
				<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />
				<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
				
				var arrFormData = new Array();
				<c:out value="${requestScope.reqFormData}" escapeXml="false" />

				<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
				<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
				<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />	

				var selectedSsrCategory = "<c:out value="${requestScope.reqSsrCategory}" escapeXml="false" />";
				 					
			</script>
			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
							<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						Search Special Service Requests<%@ include file="../common/IncludeFormHD.jsp"%>							
						<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table2">
							<tr>
								<td width="10%"><font>SSR Category</font></td>
								<td width="11%">
									<select name="selSSRCat" id="selSSRCat">
										<option value="All">All</option>
										<c:out value="${requestScope.reqSSRCategoryList}" escapeXml="false" />
									</select>
	                            </td>
								<td align="right"><input name="btnSearch" type="button" class="button" id="btnSearch" onClick="searchSSR()" value="Search"></td>
			  			</tr>
					  </table>
				  	  <%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr><td>
				<%@ include file="../common/IncludeFormTop.jsp"%>Special Service Requests<%@ include file="../common/IncludeFormHD.jsp"%>
			  		<table width="100%" border="0" cellpadding="0" cellspacing="4" ID="Table9">
						<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
						<tr>
							<td>
								  <span id="spnSSR"></span> 
							</td>
						</tr>
						<tr>
							<td>
								<u:hasPrivilege privilegeId="sys.mas.ssr.add">
									<input name="btnAdd" type="button" class="Button" id="btnAdd" value="Add" onclick="addClick()">
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="sys.mas.ssr.edit">
									<input name="btnEdit" type="button" class="Button" id="btnEdit" value="Edit" onclick="editClick()">
								</u:hasPrivilege>
								<u:hasPrivilege privilegeId="sys.mas.ssr.delete">	
									<input name="btnDelete" type="button" class="Button" id="btnDelete" value="Delete" onclick="deleteClick()">
								</u:hasPrivilege>	
							</td>
						</tr>
					</table>
		  		<%@ include file="../common/IncludeFormBottom.jsp"%>
		  		</td></tr>
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
				<tr><td>		
				<%@ include file="../common/IncludeFormTop.jsp"%>
				Add/Modify SSR
				<%@ include file="../common/IncludeFormHD.jsp"%>
					<br>
					  <table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
							<tr>
								<td width="15%"><font>SSR code</font></td>
								<td><input name="txtSSRCode" type="text" id="txtSSRCode" style="width:75px;"  class="UCase" maxlength="4" onkeyUp="alphaValidate(this)" onkeyPress="alphaValidate(this)" onChange="setPageEdited(true)"><font class="mandatory">&nbsp;*</font></td>
							</tr>
							<tr>
								<td><font>SSR Description</font></td>
								<td><input name="txtSSRDesc" type="text" id="txtSSRDesc" size="50" maxlength="255" onkeyUp="alphaWhiteSpaceValidate(this)" onkeyPress="alphaWhiteSpaceValidate(this)" onChange="setPageEdited(true)"><font class="mandatory">&nbsp;*</font></td>
								
								<td valign="top" rowspan=3><font>Visible Sales <br>Channels</font></td>
								<td rowspan=3>
									<select multiple style="width:120px; height:80px;" class="ListBox" name="selSSRVisibility" id="selSSRVisibility" onChange="setPageEdited(true)">
									<c:out value="${requestScope.reqSalesChannels}" escapeXml="false" />
								</td>
							
							</tr>
							<tr>
								<td><font>Charge</font></td>
								<td><input name="txtDefaultCharge" type="text" id="txtDefaultCharge" style="width:75px;"  maxlength="3" onkeyUp="positiveWholeNumberValidate(this)" onkeyPress="positiveWholeNumberValidate(this)" onChange="setPageEdited(true)"><font class="mandatory">&nbsp;*</font></td>
							</tr>
							<tr>
								<td><font>Category</font></td>
								<td>
									<select name="selSSRCategory" id="selSSRCategory" size="1" style="width:105;" onChange="setPageEdited(true)">
										<option value="-1"></option> 
										<c:out value="${requestScope.reqSSRCategoryList}" escapeXml="false" />
									</select><font class="mandatory">&nbsp;*</font>
								</td>
							</tr>
							<tr>
								<td><font>Active</font></td>
								<td><input type="checkbox" name="chkStatus" id="chkStatus" onChange="setPageEdited(true)"></td>
							</tr>
							
					 		<tr>
								<td colspan="2" style="height:42px;"  valign="bottom">
									<input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)">
								    <input name="btnReset" type="button" class="Button" id="btnReset" value="Reset"  onClick="resetSSR()">
								</td>
								<td colspan="2" align="right" valign="bottom">
									<input name="btnSave" type="button" class="Button" id="btnSave" value="Save"  onClick="saveSSR()">
								</td>
							</tr>
					  </table>
				 <%@ include file="../common/IncludeFormBottom.jsp"%>
				</td></tr>				 
				<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
			</table>

		<input type="hidden" name="hdnVersion"  id="hdnVersion" value=""/>	
		<input type="hidden" name="hdnMode" id="hdnMode"/>			
		<input type="hidden" name="hdnRecNo" id="hdnRecNo">
		<input type="hidden" name="hdnSSRId" id="hdnSSRId">		
	</form>
	<script src="../../js/ssr/SsrValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/ssr/SsrAdminGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
  </body>

  <script type="text/javascript">
  	<!--
  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
  	function ClearProgressbar(){
  		
  		if (typeof(objDG) == "object"){
	  		if (objDG.loaded){
	  			clearTimeout(objProgressCheck);
	  			top[2].HideProgress();
	  		}
	  	}
  	}
  	 
  	//-->
  </script>

</html>
				