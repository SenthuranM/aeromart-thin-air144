<!--
	*********************************************************
		Description		: Hub Details
		Author			: Nadika Dhanusha Mahatantila
		Created on		: Feb 19, 2008  
	*********************************************************	
-->

<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/Directives.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>
		</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="-1">
		<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">
		<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
		
		<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script> 
		<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
		<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
		
	</head>
	<body class="tabBGColor" scroll="no" onkeypress='return Body_onKeyPress(event)' oncontextmenu="return false" onkeydown='return Body_onKeyDown(event)' ondrag='return false' onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">

		<form name="frmPage" id="frmPage" action="showHubDetails.action" method="post">
			<input type="hidden" name="hdnRecNo" id="hdnRecNo" value="1">
			<script type="text/javascript">
			var arrFormData = new Array();
			var isSaveTransactionSuccessful = false;
			<c:out value="${requestScope.reqHtmlRows}" escapeXml="false" />
			<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />
			<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
			<c:out value="${requestScope.reqFormData}" escapeXml="false" />
			<c:out value="${requestScope.reqOperationMode}" escapeXml="false" />
			<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
			<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />
		</script>

			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						Search Hub Details
						<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table2">
							<tr>
								<td width="10%">
									<font>
										Hub
									</font>
								</td>
								<td width="10%">
									<select name="selSearchHub" id="selSearchHub" tabindex="1">
										<option value="">
										</option>
										<c:out value="${requestScope.reqHubs}" escapeXml="false" />
									</select>
								</td>
								<td width="10%">
									<font>
										Inward Carrier
									</font>
								</td>
								<td width="10%">
									<select name="selSearchInCarrier" id="selSearchInCarrier" tabindex="2">
										<option value="">
										</option>
										<c:out value="${requestScope.reqCarrierCodes}" escapeXml="false" />
									</select>
								</td>
								<td width="10%">
									<font>
										Outward Carrier
									</font>
								</td>
								<td width="10%">
									<select name="selSearchOutCarrier" id="selSearchOutCarrier" tabindex="3">
										<option value="">
										</option>
										<c:out value="${requestScope.reqCarrierCodes}" escapeXml="false" />
									</select>
								</td>
								<td align="right">									
									<input type='button' id='btnSearch' name='btnSearch' value='Search' onclick='pgBtnClick(6)' class='button' tabindex='4'>
								</td>
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<br>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						Hubs
						<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table9">
							<tr>
								<td>
									<font class="fntSmall">
										&nbsp;
									</font>
								</td>
							</tr>
							<tr>
								<td>
									<span id="spnHubDetails"></span>
								</td>
							</tr>
							<tr>
								<td>
									<u:hasPrivilege privilegeId="sys.mas.hub.add">
										<!-- input name="btnAdd" type="button" class="Button" id="btnAdd" value="Add" onClick="addClick()" tabindex="5" -->
										<input type="button" id="btnAdd" name="btnAdd" value="Add" onClick='pgBtnClick(1)' class='button' tabindex="5">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.mas.hub.edit">
										<!-- input type="button" id="btnEdit" class="Button" value="Edit" onClick="editClick()" tabindex="6"-->
										<input type="button" id="btnEdit" name="btnEdit" value="Edit" onClick='pgBtnClick(5)' class='button' tabindex="20">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="sys.mas.hub.delete">
										<!-- input name="btnDelete" type="button" class="Button" id="btnDelete" value="Delete" onClick="deleteClick()" tabindex="7" -->
										<input type="button" id="btnDelete" name="btnDelete" value="Delete" onClick='pgBtnClick(3)' class='button' tabindex="7">
									</u:hasPrivilege>
								</td>
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<font class="fntSmall">
							&nbsp;
						</font>
					</td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>
						Add/Modify Hub
						<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="98%" border="0" cellpadding="0" cellspacing="2" align="center" ID="Table1">
							<tr>
								<td width="25%">
									<font>
										Hub
									</font>
								</td>
								<td>
									<select name="selHub" id="selHub" onChange="clickChange()" tabindex="8">
										<option value="">
										</option>
										<c:out value="${requestScope.reqHubs}" escapeXml="false" />
									</select>
									<font class="mandatory">
										&nbsp;*
									</font>
								</td>
							</tr>
							<tr>
								<td>
									<font>
										Min Connection Time (HH:MM)
									</font>
								</td>
								<td>
									<input type="text" name="txtMinConnectTime" id="txtMinConnectTime" style="width:75px;" maxlength="5" onChange="clickChange()" onblur="timeChk(txtMinConnectTime)" onKeyPress="MinConnectTimePress()"
										onKeyUp="MinConnectTimePress()" tabindex="9">
									<font class="mandatory">
										&nbsp;*
									</font>
								</td>
							</tr>
							<tr>
								<td>
									<font>
										Max Connection Time (HH:MM)
									</font>
								</td>
								<td>
									<input type="text" name="txtMaxConnectTime" id="txtMaxConnectTime" style="width:75px;" maxlength="5" onChange="clickChange()" onblur="timeChk(txtMaxConnectTime)" onKeyPress="MaxConnectTimePress()"
										onKeyUp="MaxConnectTimePress()" tabindex="10">
									<font class="mandatory">
										&nbsp;*
									</font>
								</td>
							</tr>
							<tr>
								<td>
									<font>
										Inward Carrier
									</font>
								</td>
								<td>
									<select name="selInCarrier" id="selInCarrier" onChange="clickChange()" tabindex="11">
										<option value="">
										</option>
										<c:out value="${requestScope.reqCarrierCodes}" escapeXml="false" />
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<font>
										Outward Carrier
									</font>
								</td>
								<td>
									<select name="selOutCarrier" id="selOutCarrier" onChange="clickChange()" tabindex="12">
										<option value="">
										</option>
										<c:out value="${requestScope.reqCarrierCodes}" escapeXml="false" />
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<font>
										Active
									</font>
								</td>
								<td>
									<input type="checkbox" name="chkStatus" id="chkStatus" size="1" value="Active" onChange="clickChange()" tabindex="13">
								</td>
							</tr>
							<tr>
								<td colspan="2">
									&nbsp;
								</td>
							</tr>
							<tr>
								<td colspan="2" valign="bottom">
									<!-- input name="btnClose" type="button" class="Button" id="btnClose" value="Close" onclick="top[1].objTMenu.tabRemove(screenId)" tabindex="13" -->
									<input type="button" id="btnCancel" name="btnCancel" value="Close" onClick='pgBtnClick(4)' class='button' tabindex='14'>
									<!-- input name="btnReset" type="button" class="Button" id="btnReset" onClick="resetClick()" value="Reset" tabindex="14" -->
									<input type="button" id="btnReset" name="btnReset" value="Reset" onClick='pgBtnClick(0)' class='button' tabindex='15'>
								</td>
								<td align="right" valign="bottom">
									<!-- input name="btnSave" type="button" class="Button" id="btnSave" onClick="saveHubDetails()" value="Save" tabindex="15" -->
									<input type="button" id="btnSave" name="btnSave" value="Save" onClick='pgBtnClick(2)' class='button' tabindex='16'>
								</td>
							</tr>
						</table>
						<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				<tr>
					<td>
						<font class="fntSmall">
							&nbsp;
						</font>
					</td>
				</tr>
			</table>
			<input type="hidden" name="hdnMode" id="hdnMode" value="Mode">
			<input type="hidden" name="hdnModel" id="hdnModel" value="">
			<input type="hidden" name="hdnVersion" id="hdnVersion" value="">
			<input type="hidden" name="hdnId" id="hdnId" />
			<input type="hidden" name="hdnHubCode" id="hdnHubCode" />
		</form>
	</body>
	<script src="../../js/hubdetails/HubDetailsValidations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script src="../../js/hubdetails/HubDetailsGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script type="text/javascript">
  	<!--
  	var objProgressCheck = setInterval("ClearProgressbar()", 300);
  	function ClearProgressbar(){  		
  		if (typeof(objDG) == "object"){
	  		if (objDG.loaded){
	  			clearTimeout(objProgressCheck);
	  			top[2].HideProgress();
	  		}
	  	}	  	
  	}
  	
  	// Search Controls
  	var arrSearch = new Array();
	arrSearch[0]	= new Array("selSearchHub", "Hub");
	arrSearch[1]	= new Array("selSearchInCarrier", "Inward Carrier");
	arrSearch[2]	= new Array("selSearchOutCarrier", "Outward Carrier");
	
	// Data Controls
	var arrControls = new Array();
	arrControls[0]	= new Array("selHub", "Hub", "Y");
	arrControls[1]	= new Array("txtMinConnectTime", "Min Connection Time");
	arrControls[2]	= new Array("txtMaxConnectTime", "Max Connection Time");
	arrControls[3]	= new Array("selInCarrier", "Inward Carrier");
	arrControls[4]	= new Array("selOutCarrier", "Outward Carrier");
	arrControls[5]	= new Array("chkStatus", "Active");
  	 
  	//-->
  </script>

</html>
