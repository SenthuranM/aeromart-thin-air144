<%--
	 @Author 	: 	Sumudu
  --%>
<%@ page language="java"%>
<%@ include file="../common/Directives.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Template Page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="-1">
	<LINK rel="stylesheet" type="text/css" href="../../themes/default/css/ui.all_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Grid_no_cache.css">
	<LINK rel="stylesheet" type="text/css" href="../../css/Style_no_cache.css">

	<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
	<script src="../../js/common/Drag.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/Message.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/recNav.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/Calendar.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>	
	<script src="../../js/common/validations.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/manageCharges/ManageChargesValidation.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/autoDate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>  
	<script src="../../js/common/MultiDropDown.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/manageCharges/PopUpData.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/manageCharges/PopUpDataAgt.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/manageCharges/PopUpDataCharges.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/PopUp.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/common/GridNew.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	
	<script type="text/javascript" src="../../js/v2/jquery/jquery.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/v2/jquery/jquery-migrate.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript" src="../../js/v2/jquery/jquery.ui.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script	src="../../js/v2/common/jQuery.common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />" type="text/javascript"></script>
	<script type="text/javascript">
		
		<c:out value="${requestScope.totalNoOfRecords}" escapeXml="false" />
		<c:out value="${requestScope.reqClientMessages}" escapeXml="false" />
		<c:out value="${requestScope.reqChargesGrid}" escapeXml="false" />
		<c:out value="${requestScope.reqModifyChargesGrid}" escapeXml="false" />
		var model= new Array();
		<c:out value="${requestScope.reqModelData}" escapeXml="false" />
		<c:out value="${requestScope.reqSystemDate}" escapeXml="false" />
		<c:out value="${requestScope.reqTransactionStatus}" escapeXml="false" />
		<c:out value="${requestScope.reqDisplayTransactionStatus}" escapeXml="false" />
		
		
		var isDelete=false;
		var currentSystemDateAndTime ="<c:out value="${requestScope.reqChargesSysDate}" escapeXml="false" />";
		var enableExcludeChargesFromFQ ="<c:out value="${requestScope.enableExcludeChargesFromFQ}" escapeXml="false" />";
		<c:out value="${requestScope.deleteMsg}" escapeXml="false" />
		var strVar = <c:out value="${requestScope.reqCabinClasses}" escapeXml="false" />
		var cabinClassCodes = strVar.split(",");
		<c:out value="${requestScope.reqAllowAssCookieCharges}" escapeXml="false" />
		<c:out value="${requestScope.reqBundledFaresEnabled}" escapeXml="false"></c:out>
		
		var countryStateArry = {};
		<c:out value="${requestScope.reqCountryStateList}" escapeXml="false" />
		<c:out value="${requestScope.reqServiceTaxReportCategory}" escapeXml="false" />
	</script>    
  </head>
	<body class="tabBGColor" style="overflow-X:hidden; overflow-Y:auto;" oncontextmenu="return false" onkeypress='return Body_onKeyPress(event)' onkeydown='return Body_onKeyDown(event)' ondrag='return false'  
  			onLoad="winOnLoad('<c:out value="${requestScope.reqMessage}"/>','<c:out value="${requestScope.reqMsgType}"/>')">
  		<div id='pop' name='pop' style='position: absolute; z-index:1000; left:30%; top:25%; height: 200px; width: 500; visibility:hidden;'></div>
		<div id='popagent' name='popagent' style='position: absolute; z-index:1000; left:30%; top:25%; height: 200px; width: 500; visibility:hidden;'></div>
		<div id='popCharges' name='popCharges' style='position: absolute; z-index:1000; left:30%; top:25%; height: 200px; width: 500; visibility:hidden;'></div>
		<form action="showCharges.action" name="frmManageCharges" id="frmManageCharges" method="post">
			<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td><%@ include file="../common/IncludeMandatoryText.jsp"%></td>
				</tr>
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Search Charges<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="1" cellspacing="0" ID="Table9">
							<tr style="height:1px">
								<td width="10%"><font>Charge Code</font></td>
								<td width="10%"><font>Group</font></td>
								<td width="10%"><font>Category</font></td>
								<td colspan="4"><font>Show Charge With Rates</font> </td>
								<td width = "10%"><font>Status</font> </td>
								<td width="8%"><font>From Date</font></td>
								<td width="2%"><font>&nbsp;</font></td>
								<td width="8%"><font>To Date</font></td>
								<td width="2%"><font>&nbsp;</font></td>
								<td align="right" rowspan="2" valign="bottom">	
									<input type="button" id="btnSearch" name="btnSearch" value="Search" class="button" onClick="search_click()">
								</td>
							</tr>
							<tr>
								<td>
									<select id="selChargeCode" name="selChargeCode" style="width:74px">
										<OPTION value="All">All</OPTION>
										<c:out value="${requestScope.reqChargeCodeList}" escapeXml="false" />
									</select>
								</td>
								<td>
									<select id="selGroup" name="selGroup" style="width:70px" >
										<OPTION value="All">All</OPTION>
										<c:out value="${requestScope.reqGroupList}" escapeXml="false" />
									</select>
								</td>
								<td>
									<select id="selCategory" name="selCategory" style="width:80px">
										<OPTION value="All">All</OPTION>
										<c:out value="${requestScope.reqCategoryList}" escapeXml="false" />
									</select>
								</td>	
								<td width = "2%">
									<input type="radio" id="radRateCat" name="radRateCat"   class="NoBorder" value="NoCharges" onClick="setWithoutCharges()" >
								</td>
								<td width = "9%"><font>Without rates</font></td>
								<td width = "2%">
									<input type="radio" id="radEffective" name="radRateCat"   class="NoBorder"  value="WithDateRange" onClick="setWithoutCharges()" ></td>
								<td width = "12%"><font>Effective During</font>
								</td>
								<td><select id="selStatus" name="selStatus" size="1">
									<option value="ACT">Active</option>
									<option value="INA">Inactive</option>
									<option value="All">All</option>
								</select></td>
								<td>
									<input type="text" id="txtDateFrom" name="txtDateFrom" style="width:75px;" maxlength="12" onblur="dateChk('txtDateFrom')"  invalidText="true">
								</td>
								<td>
									<a href="javascript:void(0)" onclick="LoadCalendar(0,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</td>
								<td>
									<input type="text" id="txtDateTo" name="txtDateTo" style="width:75px;" maxlength="12" onblur="dateChk('txtDateTo')" invalidText="true">
								</td>
								<td>
									<a href="javascript:void(0)" onclick="LoadCalendar(1,event); return false;" title="View Calendar"><img src="../../images/calendar_no_cache.gif" border="0"></a>
								</td>
							</tr>
						</table>
					  	<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>			
				<tr>
					<td>
						<%@ include file="../common/IncludeFormTop.jsp"%>Charges<%@ include file="../common/IncludeFormHD.jsp"%>
						<table width="100%" border="0" cellpadding="0" cellspacing="1" ID="Table9">
							<tr><td><font class="fntSmall">&nbsp;</font></td></tr>
							<tr>
								<td>
							  			<span id="spnMC"></span> 
								</td>
							</tr>
							<tr>
								<td>
									<u:hasPrivilege privilegeId="plan.fares.charge.add">
										<input type="button" id="btnAdd" name="btnAdd" class="Button" value="Add" onClick="ctrl_Addclick()">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="plan.fares.charge.edit">
										<input type="button" id="btnEdit" name="btnEdit" class="Button" value="Edit" onClick="ctrl_Editclick()">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="plan.fares.charge.delete">
										<input type="button" id="btnDelete" name="btnDelete" class="Button" value="Delete" onClick="ctrl_Deleteclick()">
									</u:hasPrivilege>
								</td>
							</tr>
						</table>
					  	<%@ include file="../common/IncludeFormBottom.jsp"%>
					</td>
				</tr>
				
			<tr>
				<td>
				<div id="tabs" class="admin-panel ui-border-topDis">
					<ul class="ui-border-leftDis ui-border-rightDis">
						<li id="tabAddModChargeTop"><a href="#tabAddModCharge">Add/Modify Charges</a></li>
						<li id="tabAddModChargeRateTop"><a href="#tabAddModChargeRate">Add/Modify Charge Rate Values</a></li>
						<li id="tabReportingTop"><a href="#tabReporting">For Reporting </a></li>
					</ul>
					<div id="tabAddModCharge" style="height: 208px;overflow-y: auto;overflow-x: hidden">
					<table width="100%" border="0" cellpadding="0" cellspacing="2" align="left">
						<tr id="excludeFarequote" style="display:none;">
							<td width="18%" height="20">
								<font>Exclude from Fare Quote</font>
							</td>
							<td width="18%">
								<input type="checkbox" id="chkExcludeFromFareQuote" name="chkExcludeFromFareQuote" class="NoBorder" />
							</td>
							<td></td>
						</tr>
						<tr>
							<td width="18%" height="20">
								<font>Charge Code</font>
							</td>
							<td width="18%">
								<input type="text" id="txtChargeCode" name="txtChargeCode" size="7" maxLength="7" onChange="required_changed()" 
								onkeyPress="kpAlphaNumeric('txtChargeCode')" onKeyUp="kpAlphaNumeric('txtChargeCode')"><font class="mandatory">*</font>
							</td>
							<td width="63%">
								<div id="divCurrSelect">
									<table>
										<tr>
											<td><font class="fntBold">Show Charges In</font> </td>
											
											<td>
												
												<input type="radio" id="radCurr" name="radCurr" value="BASE_CURR"  
												class="NoBorder"onClick="currencyClick();required_changed()" checked="checked">
												<font>Base Currency</font>
											</td>
											
											<td>
												
												<input type="radio" id="radSelCurr" name="radCurr" value="SEL_CURR"  
												class="NoBorder"onClick="currencyClick();required_changed()">
												<font>Selected Currency</font>
											</td>
											
											<td>
												<select name="selCurrencyCode" id="selCurrencyCode" size="1" style="width:105;" onChange="setPageEdited(true)">
													<option value="-1"></option> 
													<c:out value="${requestScope.reqCurrencyList}" escapeXml="false" />
												</select><font class="mandatory"></font>
											</td>
											
										</tr>
									</table>
								</div>
							</td>
						</tr>
						<tr>
							<td width="18%" height="20">
								<font>Group</font>
							</td>
							<td width="18%" align="left">
								<select id="AddSegselGroup" size="1" name="AddSegselGroup" style="width:65px" onChange="getSelGroup()" >
								<option value=""></option>
								<c:out value="${requestScope.reqGroupStatusList}" escapeXml="false" />
								</select><font class="mandatory">*</font>
							</td>
							<td >
								<table>
									<tr>
										<td align="left" width="20%">
											<font class="fntBold">Airport Tax</font>
										</td>
										<td align="left"  width="5%">
											<input type="checkbox" id="chkAirportTax" name="chkAirportTax" class="NoBorder" onClick="checkAirportTax()">
										</td>
										<td align="left"  width="15%">
											<select id="selAirportTaxType"  name="selAirportTaxType" size="1" width="130px" onChange="airportTaxTypeChanged()">
												<option value="DEP">Departure</option>
												<option value="ARR">Arrival</option>
												<option value="TRANSIT">Transit</option>
											</select>
										</td>
										<td align="left" width="55%">
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td height="20" width="18%">
								<font>Description</font>
							</td>
							<td width="18%">
								<input type="text" id="txtDesc" name="txtDesc" width="140px" maxLength="30" onChange="required_changed()" 
								onkeyPress="replaceInvalids('txtDesc')" onKeyUp="replaceInvalids('txtDesc')"><font class="mandatory">*</font>
							</td>
							<td rowspan="2">
							<fieldset style="border:1px solid #666; padding: 3px 6px"> 
									<legend><font>Transits</font></legend>
									<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
											<tr>
												<td width="17%" align="center">
													<font>Inward Transit</font>
												</td>
												<td width="17%" align="center">
													<font>Outward Transit</font>
												</td>
												<td width="17%" align="center">
													<font>Ond Transit</font>
												</td>
												<td width="23%" align="center" title="(DD:HR:MI)">
													<font>Min Transit Duration</font>
												</td>
												<td width="23%" align="center" title="(DD:HR:MI)">
													<font>Max Transit Duration</font>
												</td>
											</tr>
											
											<tr>
												<td width="17%" align="center">
												<select name="chkInwardTrn" id="chkInwardTrn" onchange = "controlTrnsitInputs()">
													<option value="N"></option>
													<option value="I">INCLUDE</option>
													<option value="E">EXCLUDE</option>																									
												</select>
												
												</td>
												<td width="17%" align="center">
												<select name="chkOutwardTrn" id="chkOutwardTrn" onchange = "controlTrnsitInputs()">
													<option value="N"></option>
													<option value="I">INCLUDE</option>
													<option value="E">EXCLUDE</option>															
												</select>
												</td>
												<td width="17%" align="center">
												<select name="chkAllTrn" id="chkAllTrn" onchange = "controlTrnsitInputs()">
													<option value="N"></option>
													<option value="I">INCLUDE</option>
													<option value="E">EXCLUDE</option>															
												</select>
												</td>
												<td align = "center">
												<input type="text" id="minTranDuration_DD" name="minTranDuration_DD"  maxlength="3" class="leftText" style="width:30px;color:#888;" value="DD" onkeyup="KPValidatePositiveInteger('minTranDuration_DD')" onfocus="inputFocus(this ,'DD')" onblur="inputBlur(this , 'DD')"/>
												<input type="text" id="minTranDuration_HR" name="minTranDuration_HR"  maxlength="3" class="leftText" style="width:30px;color:#888;" value="hh" onkeyup="KPValidatePositiveInteger('minTranDuration_HR')" onfocus="inputFocus(this ,'hh')" onblur="inputBlur(this , 'hh')"/>
												<input type="text" id="minTranDuration_MI" name="minTranDuration_MI"  maxlength="3" class="leftText" style="width:30px;color:#888;" value="mm" onkeyup="KPValidatePositiveInteger('minTranDuration_MI')" onfocus="inputFocus(this ,'mm')" onblur="inputBlur(this , 'mm')"/>
												</td>
												<td align = "center">
												<input type="text" id="maxTranDuration_DD" name="maxTranDuration_DD"  maxlength="3" class="leftText" style="width:30px;color:#888;" value="DD" onkeyup="KPValidatePositiveInteger('maxTranDuration_DD')" onfocus="inputFocus(this ,'DD')" onblur="inputBlur(this , 'DD')"/>
												<input type="text" id="maxTranDuration_HR" name="maxTranDuration_HR"  maxlength="3" class="leftText" style="width:30px;color:#888;" value="hh" onkeyup="KPValidatePositiveInteger('maxTranDuration_HR')" onfocus="inputFocus(this ,'hh')" onblur="inputBlur(this , 'hh')"/>
												<input type="text" id="maxTranDuration_MI" name="maxTranDuration_MI"  maxlength="3" class="leftText" style="width:30px;color:#888;" value="mm" onkeyup="KPValidatePositiveInteger('maxTranDuration_MI')" onfocus="inputFocus(this ,'mm')" onblur="inputBlur(this , 'mm')"/>												
												</td>
											</tr>
										</table>
									</fieldset>
								
							</td>
						</tr>
						<tr>
							<td align="left" width="18%" height="20">
								<font>Journey Type</font>
							</td>
							<td align="left">
								<select id="selJourney"  name="selJourney" size="1" width="130px" onChange="required_changed()">
									<option value="A">All</option>
									<option value="E">All Excluding Short Transit</option>
									<!-- c:out value="${requestScope.reqJourney}" escapeXml="false"  -->
									</select><font class="mandatory">*</font>
							</td>
							<td></td>
						</tr>
						<tr>
							<td width="18%" height="20">
								<font>Refundable</font>
							</td>
							<td width="18%" height="20">
								<input type="checkbox" id="chkRefundable" name="chkRefundable" class="NoBorder" onchange="changeRefOnlyForMOD()">
							</td>
							<td>
								<table>
									<tr>
										<td align="left" width="15%">
											<font class="fntBold">Category</font><font class="mandatory">*</font>
										</td>
										<td align="left"  width="5%">
											<input type="radio" id="radCat1" name="radCat1" value="GLOBAL"  class="NoBorder"  onClick="categoryClick();required_changed()">
										</td>
										<td align="left" width="10%"><font>GLOBAL</font></td>
										<td  width="5%">
											<input type="radio" id="radPos" name="radCat1" value="POS"  class="NoBorder" onClick="categoryClick();required_changed()"  >
										</td>
										<td align="left" width="10%"><font>POS</font></td>
										<td width="5%">
											<input type="radio" id="radSegment" name="radCat1" value="OND"  class="NoBorder" onClick="categoryClick();required_changed()">
										</td>
										<td align="left" width="10%"><font>OND</font></td>
										
										<td width="5%">
											<input type="radio" id="radServiceTax" name="radCat1" value="SERVICE_TAX"  class="NoBorder" onClick="categoryClick();required_changed()">
										</td>
										<td align="left" width="10%"><font>Service Tax</font></td>
										
										<td width="5%">
											<input type="radio" id="radCookieBased" name="radCat1" value="COOKIE_BASED"  class="NoBorder" onClick="categoryClick();required_changed()">
										</td>
										<td id="lblCookieBased" align="left" width="10%"><font>Cookie Based</font></td>
										


										
										<td align="left" width="40%">
											
											<span id="spnPOS">
												<input type="button" value= "POS" 
												class="Button" onClick="return POSenable();" style="height:20px" />
											</span>
											
											<span id="spnONDAgent">
												<input type="button" value= "Agents" class="Button" 
												onClick="return ONDAgentenable();" style="height:20px" />
										 	</span>
										 	
										 	<span id="spnCharge">
												<input type="button" value= "Charge" 
												class="Button" onClick="return ChargeEnable();" style="height:20px" />
											</span>
										 	
											
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr id="refundabileForMOD">
							<td width="18%" height="20"><font>Refundable only for modifications</font></td>
							<td width="18%">
								<input type="checkbox" id="chkRefundableOnlyMOD" name="chkRefundableOnlyMOD" class="NoBorder" />
							</td>
							<td id="searchDays" width="10%">Date range applicable for cookie based surcharge : Min
							<input type="text" id="txtMinNoOfDays" name="txtMinNoOfDays" size="3" maxLength="3" onChange="required_changed()" onkeyUp="positiveWholeNumberValidate(this)" onkeyPress="positiveWholeNumberValidate(this)" /><font class="mandatory">*</font>
							<font>- Max</font>
							<input type="text" id="txtMaxNoOfDays" name="txtMaxNoOfDays" size="3" maxLength="3" onChange="required_changed()" onkeyUp="positiveWholeNumberValidate(this)" onkeyPress="positiveWholeNumberValidate(this)" /><font class="mandatory">*</font>
							<font>(Days)</font>
							</td>
						</tr>
						<tr>
								<td width="18%" height="20"><font>Refundable when exchange</font></td>
								<td width="18%">
									<input type="checkbox" id="chkRefundableWhenExchange" name="chkRefundableWhenExchange" class="NoBorder">
								</td>
									<td valign="top">
									<span id="spnServiceTax">
											<table border="0" width="80%">
												<tr>
													<td width="17%"><font>Country</font></td>
													<td >
											  			<select id="selCountry" name="selCountry" size="1" style="width:60px;">
											  				<OPTION value=""></OPTION>
															<c:out value="${requestScope.reqCountryList}" escapeXml="false" />
														</select>
													</td>
												</tr>
												<tr id='rowSelState'>
													<td width="17%"><font>State</font></td>
													<td colspan="2">
														<select id="selState" name="selState" size="1" style="width:60px;">
															<OPTION value=""></OPTION>
														</select>
													</td>
												</tr>
												<tr id='rowInterState'>
													<td width="17%"><font>Inter State</font></td>
													<td >
														<input type="checkbox" id="chkServiceTaxInterState" name="chkServiceTaxInterState" class="NoBorder">
													</td>
												</tr>
												<tr id='rowTicketing'>
													<td width="17%"><font>Ticketing Service Tax</font></td>
													<td >
														<input type="checkbox" id="chkServiceTaxTicketing" name="chkServiceTaxTicketing" class="NoBorder">
													</td>
												</tr>
												<tr>
													<td width="17%"><font>Invoice Mapping</font></td>
													<td >
											  			<select id="selGenerateReport" name="selGenerateReport" size="1" style="width:60px;">
											  				<OPTION value="TAX1">TAX1</OPTION>
															<OPTION value="TAX2">TAX2</OPTION>
															<OPTION value="TAX3">TAX3</OPTION>
														</select>
													</td>
												</tr>
											</table>
										</span>																
								</td>
						</tr>						
						<tr>							
							<u:hasPrivilege privilegeId="xbe.res.exclude.charges">
							Exclude from Fare Quote	<input type="checkbox" id="chkExcludeFromFareQuote" name="chkExcludeFromFareQuote" class="NoBorder">
							</u:hasPrivilege>							
							<td></td>
						</tr>
						<tr id='trBundledFare'>
							<td width="18%" height="20"><font>Bundle Fare Charge</font></td>	
							<td width="18%">
								<input type="checkbox" id="chkBundledFareCharge" name="chkBundledFareCharge" class="NoBorder" onclick="clickBundleFareCharge()">
							</td>	
							<td width="63%"></td>		
						</tr>
						
						<tr>
							<td width="18%" height="20"> 
								<font>Active</font>		
							</td>
							<td width="18%" height="20">
								<input type="checkbox" id="chkStatus" name="chkStatus" class="NoBorder">
							</td>
							<td rowspan="5" valign="top">
								<span id="spnSegment">
										<table border="0" width="80%">
											<tr>
												<td width="17%"><font>Departure</font></td>
												<td >
										  			<select id="selDepature" name="selDepature" size="1" style="width:60px;">
														<option value="All">All</option>
														<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
													</select>
												</td>
												<td width="16%"><font>Arrival</font></td>
												<td>
													<select id="selArrival" name="selArrival" size="1" style="width:60px;">
														<option value="All">All</option>
														<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
													</select>
												</td>
											</tr>
											<tr>
												<td width="17%"><font>Via 1</font></td>
												<td colspan="2">
													<select id="selVia1" name="selVia1" size="1" style="width:60px;">
														<OPTION value=""></OPTION>
														<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
													</select>
												</td>
												<td rowspan="4">
													<select id="selSelected" size="3" name="selSelected" style="width:150;height:70" multiple>
													</select>
												</td>
											</tr>
											<tr>
												<td width="17%"><font>Via 2</font></td>
												<td ><select id="selVia2" name="selVia2" size="1" style="width:60px;">
														<OPTION value=""></OPTION>
														<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
													</select>
												</td>
												<td align="center"><input type="button" id="btnAddSeg" name="btnAddSeg" class="Button" value=">" style="width:40px;height:17px" onClick="btnAddSegment_click()"> </td>
												<td></td>
											</tr>

											<tr>
												<td width="17%"><font>Via 3</font></td>
												<td ><select id="selVia3" name="selVia3" size="1" style="width:60px;">
														<OPTION value=""></OPTION>
														<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
													</select>
												</td>
												<td align="center"><input type="button" id="btnRemSeg" name="btnRemSeg" class="Button" value="<" style="width:40px;height:17px" onClick="btnRemoveSegment_click()"> </td>
												<td></td>
											</tr>
											<tr>
												<td width="17%"><font>Via 4</font></td>
												<td colspan="3"><select id="selVia4" name="selVia4" size="1" style="width:60px;">
														<OPTION value=""></OPTION>
														<c:out value="${requestScope.reqActiveViaList}" escapeXml="false" />
													</select>
												</td>												
											</tr>
										</table>
									</span>
							</td>
						</tr>
						<tr>
							<td width="18%" height="20"> 
								<font>Apply To</font>
							</td>
							<td width="18%" height="20">
								<select name="selApplyTo" id="selApplyTo">
									<option value="">-</option>									
								</select>
							</td>
							<td></td>
						</tr>
						
						<tr>
							<td width="18%" height="20"> 
								<font>Apply OND Seg.</font>
							</td>
							<td width="18%" height="20">
								<select name="selOndSeg" id="selOndSeg">
									<option value="A">All</option>
									<option value="F">First</option>
									<option value="L">Last</option>
								</select>
							</td>
							<td></td>
						</tr>
		
						<tr>
							<td align="left" width="18%" height="20">
								<font>Sales channel</font>
							</td>
							<td align="left">
								<select name="selChannel" id="selChannel">
									<option value="-1">All</option>
									<option value="4">IBE</option>
									<option value="3">XBE</option>										
									<option value="12">API</option>																		
								</select>
							</td>									
						</tr>
						
						<tr>
							<td width="18%" height="20"> 
								<font></font>
							</td>
							<td width="18%" height="20">
							</td>
							<td></td>
						</tr>
						<tr> 
							<td colspan="3"   valign="top">
								
							</td>
						</tr>
						</table>
					</div>
					<div id="tabAddModChargeRate" style="height: 208px;overflow-y: auto;overflow-x: hidden">
					<table width="75%" border="0" cellpadding="0" cellspacing="0" align="left"><tr><td>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
							<tr>
								<td style="height:150px;" valign="top">
									<div style="width:900px;overflow-x: auto;">
										<span id="spnTax"></span> 
									</div>
								</td>
							</tr>									
							<tr>
								<td>
									<u:hasPrivilege privilegeId="plan.fares.charge.rate.add">
										<input type="button" id="btnGAdd" value="Add" class="Button" onClick="ctrl_GAdd_click()">
									</u:hasPrivilege>
									<u:hasPrivilege privilegeId="plan.fares.charge.rate.add">
										<input type="button" id="btnGEDit" name="btnGEDit" value="Edit" class="Button" onClick="ctrl_GEdit_click()">
									</u:hasPrivilege>													
								</td>
							</tr>
						</table></td></tr></table>
					</div>
					<div id="tabReporting" style="height: 208px;overflow-y: auto;overflow-x: hidden">
						<table width="100%" border="0" cellpadding="0" cellspacing="1" align="center">
							<tr>
								<td width="40%">
									<table>
										<tr>
											<td align="left" width="20%"><font class="fntBold">Charge Group</font></td>
											<td align="left" width="5%"><input type="radio" id="radRptChgCode" name="radRptChgCode" value="EXST" class="NoBorder" onClick="reportChargeGroupOnChange();"></td>
											<td align="left" width="10%"><font>Existing Charge Group Code-Description</font></td>
											<td width="15%" colspan="2" align="left">
												<select id="selReportingGroup" name="selReportingGroup" size="1" style="width: 130px" onChange="required_changed()">
													<OPTION value=""></OPTION>
													<c:out value="${requestScope.reqReportingGroup}" escapeXml="false" />
												</select></td>
										</tr>
									</table>
								</td>
								<td width="60%">
									<table width="100%">
										<tr>
										<td align="right" width="3%"><input type="radio" id="radNewRptCHCode" name="radRptChgCode" value="NEW" class="NoBorder" onClick="reportChargeGroupOnChange();"></td>
										<td align="left" width="22%"><font>&nbsp;New Charge Group</font></td>
										<td width="75%"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<fieldset style="border:1px solid #666; padding: 3px 6px"> 
									<legend><font>Charges Applicable Journey</font></legend>
										<table width="100%" cellspacing="1" cellpadding="0">
											<tr>
												<td align="left">
													<input type="radio" id="radDept1" name="radDept1" value="D"  class="NoBorder"  onClick="depatureClicked()"><font>&nbsp;Departure</font>												
												</td>
												<td align="left">
													<input type="radio" id="radArr" name="radDept1" value="A"  class="NoBorder"onClick="depatureClicked()"  ><font>&nbsp;Arrival</font>
												</td>
											</tr>
										</table>
									</fieldset>
								</td>
								<td width="60%">
									<table width="100%">
										<tr>
											<td width="25%"><font>&nbsp;&nbsp;Charge Group Code</font></td>
											<td width="15%"><input type="text" id="txtRptChargeGroupCode" name="txtRptChargeGroupCode" style="width:70px;" maxlength="10" onkeyPress="kpAlphaNumericWhiteSpace('txtRptChargeGroupCode')" onKeyUp="kpAlphaNumericWhiteSpace('txtRptChargeGroupCode')"></td>
											<td width="20%"><font>&nbsp;Charge Group Description</font></td>
											<td width="40%"><input type="text" id="txtRptChargeGroupDesc" name="txtRptChargeGroupDesc" style="width:200px;" maxlength="100" onkeyPress="kpAlphaNumericWhiteSpace('txtRptChargeGroupDesc')" onKeyUp="kpAlphaNumericWhiteSpace('txtRptChargeGroupDesc')"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<span id="spnRevenue">
										<table  width="100%" border="0" cellpadding="0" cellspacing="1" align="right">
											<tr>
												<td align="left" width="48%">
													<font>Revenue&nbsp;</font></td>
												<td align="left">
													<input type="checkbox" id="chkRevenue" name="chkRevenue" class="NoBorder" onClick="checkRevenue()">
												</td>
											</tr>
											<tr>
												<td align ="left" width="48%">
													<font>Revenue %&nbsp;</font>
												</td>
												<td align="left" >
													<input type="text" id="txtRevenue" name="txtRevenue" style="width:75px;" size="5" class="rightText" maxlength="5" onChange="required_changed()" onKeyPress="RevenuePress(this)" onKeyUp="RevenuePress(this)" tabindex="19">
												</td>
											</tr>
										</table>
									</span>
								</td>
								<td width="60%"></td>																															
							</tr>
						</table>
					</div>
					<div class="admin-inner-panel" style="padding: 1px .7em;">		
						<table width="97%" border="0" cellpadding="0" cellspacing="0" align="center">
							<tr>
	    						<td>
	          							<input type="button" id="btnClose" class="Button" value="Close" onclick="closeClick()">
	              						<input name="btnReset" type="button" class="Button" id="btnReset" value="Reset" onClick="resetClick()">					               
	          						</td>
	          						<td align="right" colspan="2">
		 							<input name="btnSave" type="button" class="Button" id="btnSave" value="Save" onClick="save_click()">
								</td>
									</tr>
						</table>
					</div>
				</div>
				
				
					<%--<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="19%" valign="top">
								<%@ include file="../common/IncludeFormTop.jsp"%>Add/Modify Charges	<%@ include file="../common/IncludeFormHD.jsp"%>
											  
										<!--  Left Pane End -->
								  <%@ include file="../common/IncludeFormBottom.jsp"%>
							</td>
							<td width="1%"><font>&nbsp;</font></td>
							<td width="80%" valign="top">
							<%@ include file="../common/IncludeFormTop.jsp"%>Add/Modify Charge Rate Values<%@ include file="../common/IncludeFormHD.jsp"%>
								<%@ include file="../common/IncludeFormBottom.jsp"%>
								<table width="100%" border="0" cellpadding="0" cellspacing="1" align="center">
									<tr>
										<td>
											<%@ include file="../common/IncludeFormTop.jsp"%>For Reporting<%@ include file="../common/IncludeFormHD.jsp"%>
											<%@ include file="../common/IncludeFormBottom.jsp"%>
										</td>
									</tr>
												

									<tr>
										<td>								
											<table width="100%" border="0" cellpadding="0" cellspacing="1" align="center">
												<tr>									
													<td><%@ include file="../common/IncludeFormTopFrameLess.jsp"%>						
														<%@ include file="../common/IncludeFormBottom.jsp"%>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>							
						</tr>
			<tr><td><font class="fntSmall">&nbsp;</font></td></tr>				
		</table> --%>		
	<input type="hidden" id="hdnRateIDs" name="hdnRateIDs"  value="" >
	<input type="hidden" id="hdnMode" name="hdnMode"  value="" >
	<input type="hidden" id="hdnRateMode" name="hdnRateMode"  value="" >
	<input type="hidden" id="hdnVersion" name="hdnVersion"  value="" >
	<input type="hidden" id="hdnRateVersion" name="hdnRateVersion"  value="" >
	<input type="hidden" id="hdnRecNo" name="hdnRecNo"  value="1" >
	<input type="hidden" id="hdnData" name="hdnData"  value="" >
	<input type="hidden" id="hdnChargeData" name="hdnChargeData"  value="" >
	<input type="hidden" id="hdnChargeCode" name="hdnChargeCode"  value="" >
	<input type="hidden" id="hdnSegments" name="hdnSegments"  value="" >
	<input type="hidden" id="hdnStation" name="hdnStation"  value="" >
	<input type="hidden" id="hdnGroup" name="hdnGroup"  value="" >
	<input type="hidden" id="hdnStatus" name="hdnStatus"  value="" >
	<input type="hidden" id="hdnDesc" name="hdnDesc"  value="" >
	<input type="hidden" id="hdnCat" name="hdnCat"  value="" >
	<input type="hidden" id="hdnApplyTo" name="hdnApplyTo"  value="" >
	<input type="hidden" id="hdnRefund" name="hdnRefund"  value="" >
	<input type="hidden" id="hdnRefundOnlyForMOD" name="hdnRefundOnlyForMOD"  value="" />
	<input type="hidden" id="hdnRefundWhenExchange" name="hdnRefundWhenExchange"  value="" />
	<input type="hidden" id="hdnExcludeFromFareQuote" name="hdnExcludeFromFareQuote"  value="" />
	<input type="hidden" id="hdnBundledFareCharge" name="hdnBundledFareCharge"  value="" />
	<input type="hidden" id="hdnSelSelected" name="hdnSelSelected"  value="" >
	<input type="hidden" id="hdnpayType" name="hdnpayType"  value="" >			
	<input type="hidden" id="hdnRowNum" name="hdnRowNum"/>						
	<input type="hidden" id="hdnOndSeg" name="hdnOndSeg"  value="" >
	<input type="hidden" id="hdnChannel" name="hdnChannel"  value="" />
	<input type="hidden" id="hdnJourney" name="hdnJourney"  value="" >
	<input type="hidden" id="hdnStations" name="hdnStations"  value="" >
	<input type="hidden" id="hdnInc" name="hdnInc"  value="" >
	<input type="hidden" id="hdnAgents" name="hdnAgents"  value="" >
	<input type="hidden" id="hdnIncOnd" name="hdnIncOnd"  value="" >
	<input type="hidden" id="hdnAgent" name="hdnAgent"  value="" >
	<input type="hidden" id="hdnAirportTax" name="hdnAirportTax"  value="" >
	<input type="hidden" id="hdnAirportTaxType" name="hdnAirportTaxType"  value="" >
	<input type="hidden" id="hdnRptChargeGroupType" name="hdnRptChargeGroupType"  value="" >
	<input type="hidden" id="hdnMinTrnDuration" name="hdnMinTrnDuration"  value="" >
	<input type="hidden" id="hdnMaxTrnDuration" name="hdnMaxTrnDuration"  value="" >
	<input type="hidden" id="hdnCharges" name="hdnCharges"  value="" >
	<input type="hidden" id="hdnServiceTaxInterState" name="hdnServiceTaxInterState"  value="" >
	<input type="hidden" id="hdnServiceTaxTicketing" name="hdnServiceTaxTicketing"  value="" >
	
	<input type="hidden" name="hdnAppliesToList" id="hdnAppliesToList" 
	value="<c:out value='${requestScope.reqChargeAppliesToList}' escapeXml='false' />">
	
	</form>
  </body>
	<script src="../../js/manageCharges/ManageCharges.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
	<script src="../../js/manageCharges/ManageChargesGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script> 
	<script src="../../js/manageCharges/ModifyChargesGrid.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>

 <script type="text/javascript">
  
 <c:out value="${requestScope.reqActStationList}" escapeXml="false"></c:out>

 <c:out value="${requestScope.reqAgentList}" escapeXml="false"></c:out>
 
 <c:out value="${requestScope.reqChargesList}" escapeXml="false"></c:out>

   <!--
   var objProgressCheck = setInterval("ClearProgressbar()", 300);

   function ClearProgressbar(){
   	
		if(typeof(objDG) == "object"){
			if (objDG.loaded){
				clearTimeout(objProgressCheck);
				top[2].HideProgress();
		 	}
	 	}
    
   }
   $("#tabs").tabs();
   //-->
  </script>


</html>