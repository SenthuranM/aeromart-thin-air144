<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:common="http://www.isa.com/typea/common" xmlns:ns1="http://www.isa.com/typea/tkcreq"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs"
                xmlns:converter="com.isa.thinair.gdsservices.core.typeA.helpers.XmlDataConverter">

    <xsl:import href="/resources/transform/03.1/COMMON_03.1_EDI_TO_AA.xslt"/>
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>

    <xsl:template match="/">
        <xsl:for-each select="IATA">
            <ns1:AA_TKCREQ>

                <xsl:call-template name="UNB">
                    <xsl:with-param name="element_name">common:header</xsl:with-param>
                </xsl:call-template>

                <xsl:for-each select="TKCREQ">

                    <message>
                        <xsl:call-template name="UNH">
                            <xsl:with-param name="element_name">common:messageHeader</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="MSG">
                            <xsl:with-param name="element_name">messageFunction</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="ORG">
                            <xsl:with-param name="element_name">originatorInformation</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="TAI">
                            <xsl:with-param name="element_name">ticketingAgentInformation</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="RCI">
                            <xsl:with-param name="element_name">reservationControlInformation</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="EQN">
                            <xsl:with-param name="element_name">numberOfUnits</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="TVL">
                            <xsl:with-param name="element_name">flightInformation</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="TIF">
                            <xsl:with-param name="complex"/>
                            <xsl:with-param name="element_name">travellerInformation</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="FTI">
                            <xsl:with-param name="element_name">frequentTravellerInformation</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="FOP">
                            <xsl:with-param name="element_name">formOfPayment</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="RPI">
                            <xsl:with-param name="element_name">relatedProductInfo</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="IFT">
                            <xsl:with-param name="element_name">text</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="CRI">
                            <xsl:with-param name="element_name">consumerRefInformation</xsl:with-param>
                        </xsl:call-template>

                        <xsl:for-each select="GROUP_1">
                            <tickets>

                                <xsl:call-template name="TKT">
                                    <xsl:with-param name="complex">true</xsl:with-param>
                                    <xsl:with-param name="element_name"/>
                                </xsl:call-template>

                                <xsl:call-template name="TIF">
                                    <xsl:with-param name="complex"/>
                                    <xsl:with-param name="element_name">common:travellerInformation</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="TAI">
                                    <xsl:with-param name="element_name">common:ticketingAgentInformation</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="RCI">
                                    <xsl:with-param name="element_name">common:reservationControlInformation</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="FTI">
                                    <xsl:with-param name="element_name">common:frequentTravellerInformation</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="MON">
                                    <xsl:with-param name="element_name">monetaryInformation</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="FOP">
                                    <xsl:with-param name="element_name">common:formOfPayment</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="PTK">
                                    <xsl:with-param name="element_name">common:pricingTicketingDetails</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="TXD">
                                    <xsl:with-param name="element_name">common:taxDetails</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="ODI">
                                    <xsl:with-param name="element_name">common:originDestinationInfo</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="ORG">
                                    <xsl:with-param name="element_name">common:originatorInformation</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="DAT">
                                    <xsl:with-param name="element_name">common:dateAndTime</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="ATI">
                                    <xsl:with-param name="element_name">common:additionalTourInfo</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="EQN">
                                    <xsl:with-param name="element_name">common:numberOfUnits</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="IFT">
                                    <xsl:with-param name="element_name">common:text</xsl:with-param>
                                </xsl:call-template>

                                <xsl:call-template name="DID">
                                </xsl:call-template>

                                <xsl:call-template name="CRI">
                                    <xsl:with-param name="element_name">common:consumerRefInformation</xsl:with-param>
                                </xsl:call-template>

                                <xsl:for-each select="GROUP_2">
                                    <common:ticketCoupon>

                                        <xsl:call-template name="CPN">
                                            <xsl:with-param name="complex">true</xsl:with-param>
                                            <xsl:with-param name="element_name"/>
                                        </xsl:call-template>

                                        <xsl:call-template name="TVL">
                                            <xsl:with-param name="element_name">common:flightInfomation</xsl:with-param>
                                        </xsl:call-template>

                                        <xsl:call-template name="PTS">
                                            <xsl:with-param name="element_name">common:pricingTicketingSubsequent</xsl:with-param>
                                        </xsl:call-template>

                                        <xsl:call-template name="DAT">
                                            <xsl:with-param name="element_name">common:dateAndTimeInformation</xsl:with-param>
                                        </xsl:call-template>

                                        <xsl:call-template name="RPI">
                                            <xsl:with-param name="element_name">common:relatedProductInfo</xsl:with-param>
                                        </xsl:call-template>

                                        <xsl:call-template name="IFT">
                                            <xsl:with-param name="element_name">common:text</xsl:with-param>
                                        </xsl:call-template>

                                        <xsl:call-template name="EBD">
                                            <xsl:with-param name="element_name">common:excessBaggageInformation</xsl:with-param>
                                        </xsl:call-template>

                                        <xsl:call-template name="PTK">
                                            <xsl:with-param name="element_name">common:pricingTicketingDetails</xsl:with-param>
                                        </xsl:call-template>

                                        <xsl:call-template name="FTI">
                                            <xsl:with-param name="element_name">common:frequentTravellerInformation</xsl:with-param>
                                        </xsl:call-template>

                                    </common:ticketCoupon>
                                </xsl:for-each>

                            </tickets>
                        </xsl:for-each>

                    </message>
                </xsl:for-each>
            </ns1:AA_TKCREQ>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>