<xsl:stylesheet version="1.0"
                xmlns:common="http://www.isa.com/typea/common"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:converter="com.isa.thinair.gdsservices.core.typeA.helpers.XmlDataConverter">

    <xsl:template name="UNB" priority="0">
        <xsl:param name="element_name"/>
        <xsl:if test="UNB">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
            <common:interchangeHeader>
                <common:syntaxId>
                    <xsl:value-of select="string(UNB/UNB01/UNB0101)"/>
                </common:syntaxId>
                <common:versionNo>
                    <xsl:value-of select="string(UNB/UNB01/UNB0102)"/>
                </common:versionNo>
                <common:senderInfo>
                    <common:interchangeId>
                        <xsl:value-of select="string(UNB/UNB02/UNB0201)"/>
                    </common:interchangeId>
                    <common:interchangeCode>
                        <xsl:value-of select="string(UNB/UNB02/UNB0202)"/>
                    </common:interchangeCode>
                    <common:internalIdentificationCode>
                        <xsl:value-of select="string(UNB/UNB02/UNB0203)"/>
                    </common:internalIdentificationCode>
                </common:senderInfo>
                <common:receiverInfo>
                    <common:interchangeId>
                        <xsl:value-of select="string(UNB/UNB03/UNB0301)"/>
                    </common:interchangeId>
                    <common:interchangeCode>
                        <xsl:value-of select="string(UNB/UNB03/UNB0302)"/>
                    </common:interchangeCode>
                    <common:internalIdentificationCode>
                        <xsl:value-of select="string(UNB/UNB03/UNB0303)"/>
                    </common:internalIdentificationCode>
                </common:receiverInfo>
                <common:interchangeControlRef>
                    <xsl:value-of select="string(UNB/UNB05)"/>
                </common:interchangeControlRef>
                <common:recipientRef>
                    <xsl:value-of select="string(UNB/UNB06/UNB0601)"/>
                </common:recipientRef>
                <common:associationCode>
                    <xsl:value-of select="string(UNB/UNB08)"/>
                </common:associationCode>
            </common:interchangeHeader>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="UNH" priority="0">
        <xsl:param name="element_name"/>
        <xsl:if test="UNH">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
            <common:referenceNo>
                <xsl:value-of select="string(UNH/UNH01)"/>
            </common:referenceNo>
            <common:messageId>
                <common:messageType>
                    <xsl:value-of select="string(UNH/UNH02/UNH0201)"/>
                </common:messageType>
                <common:version>
                    <xsl:value-of select="string(UNH/UNH02/UNH0202)"/>
                </common:version>
                <common:releaseNumber>
                    <xsl:value-of select="string(UNH/UNH02/UNH0203)"/>
                </common:releaseNumber>
                <common:controllingAgency>
                    <xsl:value-of select="string(UNH/UNH02/UNH0204)"/>
                </common:controllingAgency>
            </common:messageId>
            <common:commonAccessReference>
                <xsl:value-of select="string(UNH/UNH03)"/>
            </common:commonAccessReference>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
        </xsl:if>
    </xsl:template>

    <!-- ==== ==== ==== ==== ==== ==== ==== ==== -->

    <xsl:template name="TIF">
        <xsl:param name="complex"/>
        <xsl:param name="element_name"/>
        <xsl:choose>
            <xsl:when test="$complex">
                <common:travellerSurname>
                    <xsl:value-of select="string(TIF/TIF01/TIF0101)"/>
                </common:travellerSurname>
                <common:travellerQualifier>
                    <xsl:value-of select="string(TIF/TIF01/TIF0102)"/>
                </common:travellerQualifier>
                <common:travellerCount>
                    <xsl:value-of select="string(TIF/TIF01/TIF0103)"/>
                </common:travellerCount>
                <xsl:for-each select="TIF/TIF02">
                    <common:travellers>
                        <common:title>
                            <xsl:value-of select="converter:getTitle(string(TIF0201))"/>
                        </common:title>
                        <common:givenName>
                            <xsl:value-of select="converter:getGivenName(string(TIF0201))"/>
                        </common:givenName>
                        <common:passengerType>
                            <xsl:value-of select="string(TIF0202)"/>
                        </common:passengerType>
                        <common:travellerReferenceNumber>
                            <xsl:value-of select="string(TIF0203)"/>
                        </common:travellerReferenceNumber>
                        <common:hasInfant>
                            <xsl:value-of select="string(TIF0204)"/>
                        </common:hasInfant>
                        <common:otherNames>
                            <xsl:value-of select="string(TIF0205)"/>
                        </common:otherNames>
                    </common:travellers>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <xsl:for-each select="TIF">
                    <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
                    <common:travellerSurname>
                        <xsl:value-of select="string(TIF01/TIF0101)"/>
                    </common:travellerSurname>
                    <common:travellerQualifier>
                        <xsl:value-of select="string(TIF01/TIF0102)"/>
                    </common:travellerQualifier>
                    <common:travellerCount>
                        <xsl:value-of select="string(TIF01/TIF0103)"/>
                    </common:travellerCount>
                    <xsl:for-each select="TIF02">
                        <common:travellers>
                            <common:givenName>
                                <xsl:value-of select="string(TIF0201)"/>
                            </common:givenName>
                            <common:passengerType>
                                <xsl:value-of select="string(TIF0202)"/>
                            </common:passengerType>
                            <common:travellerReferenceNumber>
                                <xsl:value-of select="string(TIF0203)"/>
                            </common:travellerReferenceNumber>
                            <common:hasInfant>
                                <xsl:value-of select="string(TIF0204)"/>
                            </common:hasInfant>
                            <common:otherNames>
                                <xsl:value-of select="string(TIF0205)"/>
                            </common:otherNames>
                        </common:travellers>
                    </xsl:for-each>
                    <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>

                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="TKT">
        <xsl:param name="complex"/>
        <xsl:param name="element_name"/>
        <xsl:choose>
            <xsl:when test="$complex">
                <common:ticketNumber>
                    <xsl:value-of select="string(TKT/TKT01/TKT0101)"/>
                </common:ticketNumber>
                <common:documentType>
                    <xsl:value-of select="string(TKT/TKT01/TKT0102)"/>
                </common:documentType>
                <common:totalItems>
                    <xsl:value-of select="string(TKT/TKT01/TKT0103)"/>
                </common:totalItems>
                <common:dataIndicator>
                    <xsl:value-of select="string(TKT/TKT01/TKT0104)"/>
                </common:dataIndicator>
            </xsl:when>
            <xsl:otherwise>
                <xsl:for-each select="TKT">
                    <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
                    <common:ticketNumber>
                        <xsl:value-of select="string(TKT01/TKT0101)"/>
                    </common:ticketNumber>
                    <common:documentType>
                        <xsl:value-of select="string(TKT01/TKT0102)"/>
                    </common:documentType>
                    <common:totalItems>
                        <xsl:value-of select="string(TKT01/TKT0103)"/>
                    </common:totalItems>
                    <common:dataIndicator>
                        <xsl:value-of select="string(TKT01/TKT0104)"/>
                    </common:dataIndicator>
                    <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>

                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="CPN">
        <xsl:param name="complex"/>
        <xsl:param name="element_name"/>
        <xsl:choose>
            <xsl:when test="$complex">
                <common:couponNumber>
                    <xsl:value-of select="string(CPN/CPN01/CPN0101)"/>
                </common:couponNumber>
                <common:status>
                    <xsl:value-of select="string(CPN/CPN01/CPN0102)"/>
                </common:status>
                <common:couponValue>
                    <xsl:value-of select="string(CPN/CPN01/CPN0103)"/>
                </common:couponValue>
                <common:exchangeMedia>
                    <xsl:value-of select="string(CPN/CPN01/CPN0104)"/>
                </common:exchangeMedia>
                <common:settlementAuthCode>
                    <xsl:value-of select="string(CPN/CPN01/CPN0105)"/>
                </common:settlementAuthCode>
                <common:voluntaryInvoluntaryIndicator>
                    <xsl:value-of select="string(CPN/CPN01/CPN0106)"/>
                </common:voluntaryInvoluntaryIndicator>
                <common:previousStatus>
                    <xsl:value-of select="string(CPN/CPN01/CPN0107)"/>
                </common:previousStatus>
            </xsl:when>
            <xsl:otherwise>
                <xsl:for-each select="CPN">
                    <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
                    <common:couponNumber>
                        <xsl:value-of select="string(CPN01/CPN0101)"/>
                    </common:couponNumber>
                    <common:status>
                        <xsl:value-of select="string(CPN01/CPN0102)"/>
                    </common:status>
                    <common:couponValue>
                        <xsl:value-of select="string(CPN01/CPN0103)"/>
                    </common:couponValue>
                    <common:exchangeMedia>
                        <xsl:value-of select="string(CPN01/CPN0104)"/>
                    </common:exchangeMedia>
                    <common:settlementAuthCode>
                        <xsl:value-of select="string(CPN01/CPN0105)"/>
                    </common:settlementAuthCode>
                    <common:voluntaryInvoluntaryIndicator>
                        <xsl:value-of select="string(CPN01/CPN0106)"/>
                    </common:voluntaryInvoluntaryIndicator>
                    <common:previousStatus>
                        <xsl:value-of select="string(CPN01/CPN0107)"/>
                    </common:previousStatus>
                    <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- ==== ==== ==== ==== ==== ==== ==== ==== -->


    <xsl:template name="ORG">
        <xsl:param name="element_name"/>
        <xsl:if test="ORG">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
            <common:senderSystemDetails>
                <common:companyCode>
                    <xsl:value-of select="string(ORG/ORG01/ORG0101)"/>
                </common:companyCode>
                <common:location>
                    <common:locationCode>
                        <xsl:value-of select="string(ORG/ORG01/ORG0102)"/>
                    </common:locationCode>
                    <common:locationName>
                        <xsl:value-of select="string(ORG/ORG01/ORG0103)"/>
                    </common:locationName>
                </common:location>
            </common:senderSystemDetails>
            <common:agentId>
                <xsl:value-of select="string(ORG/ORG02/ORG0201)"/>
            </common:agentId>
            <common:officeId>
                <xsl:value-of select="string(ORG/ORG02/ORG0202)"/>
            </common:officeId>
            <common:systemId>
                <xsl:value-of select="string(ORG/ORG02/ORG0203)"/>
            </common:systemId>
            <common:agentLocation>
                <common:locationCode>
                    <xsl:value-of select="string(ORG/ORG03/ORG0301)"/>
                </common:locationCode>
                <common:locationName>
                    <xsl:value-of select="string(ORG/ORG03/ORG0302)"/>
                </common:locationName>
            </common:agentLocation>
            <common:initiatorSystemDetails>
                <common:companyCode>
                    <xsl:value-of select="string(ORG/ORG04/ORG0401)"/>
                </common:companyCode>
                <common:location>
                    <common:locationCode>
                        <xsl:value-of select="string(ORG/ORG04/ORG0402)"/>
                    </common:locationCode>
                    <common:locationName>
                        <xsl:value-of select="string(ORG/ORG04/ORG0403)"/>
                    </common:locationName>
                </common:location>
            </common:initiatorSystemDetails>
            <common:originatorTypeCode>
                <xsl:value-of select="string(ORG/ORG05)"/>
            </common:originatorTypeCode>
            <common:agentCountryCode>
                <xsl:value-of select="string(ORG/ORG06/ORG0601)"/>
            </common:agentCountryCode>
            <common:agentCurrencyCode>
                <xsl:value-of select="string(ORG/ORG06/ORG0602)"/>
            </common:agentCurrencyCode>
            <common:agentLanguageCode>
                <xsl:value-of select="string(ORG/ORG06/ORG0603)"/>
            </common:agentLanguageCode>
            <common:originatorAuthorityCode>
                <xsl:value-of select="string(ORG/ORG07)"/>
            </common:originatorAuthorityCode>
            <common:communicationNumber>
                <xsl:value-of select="string(ORG/ORG08)"/>
            </common:communicationNumber>
            <common:partyId>
                <xsl:value-of select="string(ORG/ORG09)"/>
            </common:partyId>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="MSG">
        <xsl:param name="element_name"/>
        <xsl:if test="MSG">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
            <common:serviceType>
                <xsl:value-of select="string(MSG/MSG01/MSG0101)"/>
            </common:serviceType>
            <common:messageFunction>
                <xsl:value-of select="string(MSG/MSG01/MSG0102)"/>
            </common:messageFunction>
            <common:subMessageFunction>
                <xsl:value-of select="string(MSG/MSG01/MSG0104)"/>
            </common:subMessageFunction>
            <common:responseType>
                <xsl:value-of select="string(MSG/MSG02)"/>
            </common:responseType>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="RCI">
        <xsl:param name="element_name"/>
        <xsl:for-each select="RCI/RCI01">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
            <common:airlineCode>
                <xsl:value-of select="string(RCI0101)"/>
            </common:airlineCode>
            <common:reservationControlNumber>
                <xsl:value-of select="string(RCI0102)"/>
            </common:reservationControlNumber>
            <common:reservationControlType>
                <xsl:value-of select="string(RCI0103)"/>
            </common:reservationControlType>
            <common:recordCreateTimestamp>
                <xsl:if test="RCI0104 and RCI0105">
                    <xsl:value-of select="converter:toXmlDateTime(RCI0104, 'ddMMyy', RCI0105, 'HHmmss')"/>
                </xsl:if>
            </common:recordCreateTimestamp>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="IFT">
        <xsl:param name="element_name"/>
        <xsl:for-each select="IFT">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
            <common:textSubjectQualifier>
                <xsl:value-of select="IFT01/IFT0101"/>
            </common:textSubjectQualifier>
            <common:informationType>
                <xsl:value-of select="IFT01/IFT0102"/>
            </common:informationType>
            <common:status>
                <xsl:value-of select="IFT01/IFT0103"/>
            </common:status>
            <xsl:for-each select="IFT02">
                <common:freeText>
                    <xsl:value-of select="."/>
                </common:freeText>
            </xsl:for-each>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="MON">
        <xsl:param name="element_name"/>
        <xsl:if test="MON">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
            <common:amountTypeQualifier>
                <xsl:value-of select="MON/MON01/MON0101"/>
            </common:amountTypeQualifier>
            <common:amount>
                <xsl:value-of select="MON/MON01/MON0102"/>
            </common:amount>
            <common:currencyCode>
                <xsl:value-of select="MON/MON01/MON0103"/>
            </common:currencyCode>
            <common:departureLocation>
                <xsl:value-of select="MON/MON01/MON0104"/>
            </common:departureLocation>
            <common:arrivalLocation>
                <xsl:value-of select="MON/MON01/MON0105"/>
            </common:arrivalLocation>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
            <xsl:for-each select="MON/MON02">
                <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
                <common:amountTypeQualifier>
                    <xsl:value-of select="MON0201"/>
                </common:amountTypeQualifier>
                <common:amount>
                    <xsl:value-of select="MON0202"/>
                </common:amount>
                <common:currencyCode>
                    <xsl:value-of select="MON0203"/>
                </common:currencyCode>
                <common:departureLocation>
                    <xsl:value-of select="MON0204"/>
                </common:departureLocation>
                <common:arrivalLocation>
                    <xsl:value-of select="MON0205"/>
                </common:arrivalLocation>
                <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="FOP">
        <xsl:param name="element_name"/>
        <xsl:for-each select="FOP/FOP01">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
            <common:formOfPaymentType>
                <xsl:value-of select="FOP0101"/>
            </common:formOfPaymentType>
            <common:dataIndicator>
                <xsl:value-of select="FOP0102"/>
            </common:dataIndicator>
            <common:paymentAmount>
                <xsl:value-of select="FOP0103"/>
            </common:paymentAmount>
            <common:referenceNumber>
                <xsl:value-of select="FOP0105"/>
            </common:referenceNumber>
            <common:additionalCreditCardDetails>
                <common:vendorCode>
                    <xsl:value-of select="FOP0104"/>
                </common:vendorCode>
                <common:expirationDate>
                    <xsl:value-of select="FOP0106"/>
                </common:expirationDate>
                <common:approvalCode>
                    <xsl:value-of select="FOP0107"/>
                </common:approvalCode>
                <common:sourceOfApproval>
                    <xsl:value-of select="FOP0108"/>
                </common:sourceOfApproval>
                <common:authorizedAmount>
                    <xsl:value-of select="FOP0109"/>
                </common:authorizedAmount>
                <common:addressVerificationCode>
                    <xsl:value-of select="FOP0110"/>
                </common:addressVerificationCode>
                <common:extendedPaymentCode>
                    <xsl:value-of select="FOP0112"/>
                </common:extendedPaymentCode>
                <common:membershipStatus>
                    <xsl:value-of select="FOP0114"/>
                </common:membershipStatus>
                <common:creditCardTransactionInformation>
                    <xsl:value-of select="FOP0115"/>
                </common:creditCardTransactionInformation>
            </common:additionalCreditCardDetails>
            <common:accountHolderNumber>
                <xsl:value-of select="FOP0111"/>
            </common:accountHolderNumber>
            <common:text>
                <xsl:value-of select="FOP0113"/>
            </common:text>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="PTK">
        <xsl:param name="element_name"/>
        <xsl:if test="PTK">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
            <xsl:for-each select="PTK/PTK01/*">
                <common:pricingTicketingIndicators>
                    <common:key>
                        <!-- PTK0101 : extracts last 2 digits -->
                        <xsl:value-of select="substring(local-name(.), (string-length(local-name(.)) - 1))"/>
                    </common:key>
                    <common:value>
                        <xsl:value-of select="."/>
                    </common:value>
                </common:pricingTicketingIndicators>
            </xsl:for-each>
            <xsl:if test="PTK/PTK03/PTK0301">
                <common:ticketingTime>
                    <xsl:value-of select="converter:toXmlDateTime(PTK/PTK03/PTK0301, 'ddMMyy', '0000', 'HHmm')"/>
                </common:ticketingTime>
            </xsl:if>
            <common:ticketingLocation>
                <common:countryCode>
                    <xsl:value-of select="PTK/PTK06/PTK0602"/>
                </common:countryCode>
            </common:ticketingLocation>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="ODI">
        <xsl:param name="element_name"/>
        <xsl:if test="ODI">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
            <common:departureAirportCityCode>
                <xsl:value-of select="string(ODI/ODI01[1])"/>
            </common:departureAirportCityCode>
            <common:arrivalAirportCityCode>
                <xsl:value-of select="string(ODI/ODI01[2])"/>
            </common:arrivalAirportCityCode>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="TVL">
        <xsl:param name="element_name"/>
        <xsl:if test="TVL">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
            <xsl:if test="TVL/TVL01/TVL0101">
                <common:departureDateTime>
                    <xsl:choose>
                        <xsl:when test="TVL/TVL01/TVL0102">
                            <xsl:value-of select="converter:toXmlDateTime(TVL/TVL01/TVL0101, 'ddMMyy', TVL/TVL01/TVL0102, 'HHmm')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="converter:toXmlDateTime(TVL/TVL01/TVL0101, 'ddMMyy', '0000', 'HHmm')"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </common:departureDateTime>
            </xsl:if>
            <xsl:if test="TVL/TVL01/TVL0103">
                <common:arrivalDateTime>
                    <xsl:choose>
                        <xsl:when test="TVL/TVL01/TVL0104">
                            <xsl:value-of select="converter:toXmlDateTime(TVL/TVL01/TVL0103, 'ddMMyy', TVL/TVL01/TVL0104, 'HHmm')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="converter:toXmlDateTime(TVL/TVL01/TVL0103, 'ddMMyy', '0000', 'HHmm')"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </common:arrivalDateTime>
            </xsl:if>
            <common:departureLocation>
                <common:locationCode>
                    <xsl:value-of select="string(TVL/TVL02/TVL0201)"/>
                </common:locationCode>
                <common:locationName>
                    <xsl:value-of select="string(TVL/TVL02TVL0202)"/>
                </common:locationName>
            </common:departureLocation>
            <common:arrivalLocation>
                <common:locationCode>
                    <xsl:value-of select="string(TVL/TVL03/TVL0301)"/>
                </common:locationCode>
                <common:locationName>
                    <xsl:value-of select="string(TVL/TVL03/TVL0302)"/>
                </common:locationName>
            </common:arrivalLocation>
            <common:companyInfo>
                <xsl:for-each select="TVL/TVL04">
                    <common:marketingAirlineCode>
                        <xsl:value-of select="string(TVL0401)"/>
                    </common:marketingAirlineCode>
                    <common:operatingAirlineCode>
                        <xsl:value-of select="string(TVL0402)"/>
                    </common:operatingAirlineCode>
                </xsl:for-each>
            </common:companyInfo>
            <common:flightNumber>
                <xsl:value-of select="string(TVL/TVL05/TVL0501)"/>
            </common:flightNumber>
            <common:flightPreference>
                <common:bookingClass>
                    <xsl:value-of select="string(TVL/TVL05/TVL0502)"/>
                </common:bookingClass>
                <common:bookingClassModifier>
                    <xsl:value-of select="string(TVL/TVL05/TVL0504)"/>
                </common:bookingClassModifier>
            </common:flightPreference>
            <xsl:for-each select="TVL/TVL06">
                <common:flightRemarks>
                    <xsl:value-of select="string(TVL0601)"/>
                </common:flightRemarks>
            </xsl:for-each>
            <common:lineItemNumber>
                <xsl:value-of select="string(TVL/TVL07/TVL0701)"/>
            </common:lineItemNumber>

            <common:travellerCount>
                <xsl:value-of select="string(RPI/RPI01)"/>
            </common:travellerCount>
            <common:actionCode>
                <xsl:value-of select="string(RPI/RPI02)"/>
            </common:actionCode>

            <common:sellType>
                <xsl:value-of select="string(SDT/SDT01/SDT0101)"/>
            </common:sellType>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="TAI">
        <xsl:param name="element_name"/>
        <xsl:if test="TAI">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
            <common:companyIdentifier>
                <xsl:value-of select="string(TAI/TAI01)"/>
            </common:companyIdentifier>
            <xsl:for-each select="TAI/TAI02">
                <common:internalIdentificationDetails>
                    <common:requesterId>
                        <xsl:value-of select="string(TAI0201)"/>
                    </common:requesterId>
                    <common:identificationType>
                        <xsl:value-of select="string(TAI0202)"/>
                    </common:identificationType>
                    <common:agentId>
                        <xsl:value-of select="string(TAI0203)"/>
                    </common:agentId>
                </common:internalIdentificationDetails>
            </xsl:for-each>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="FTI">
        <xsl:param name="element_name"/>
        <xsl:if test="FTI">
            <xsl:for-each select="FTI/FTI01">
                <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
                <common:airlineDesignator>
                    <xsl:value-of select="string(FTI0101)"/>
                </common:airlineDesignator>
                <common:frequentTravellerNumber>
                    <xsl:value-of select="string(FTI0102)"/>
                </common:frequentTravellerNumber>
                <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="ATI">
        <xsl:param name="element_name"/>
        <xsl:if test="ATI">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
            <common:productId>
                <xsl:value-of select="string(ATI/ATI01/ATI0101)"/>
            </common:productId>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="EQN">
        <xsl:param name="element_name"/>
        <xsl:for-each select="EQN/EQN01">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
            <common:quantity>
                <xsl:value-of select="string(EQN0101)"/>
            </common:quantity>
            <common:qualifier>
                <xsl:value-of select="string(EQN0102)"/>
            </common:qualifier>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="TXD">
        <xsl:param name="element_name"/>
        <xsl:if test="TXD">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
            <common:indicator>
                <xsl:value-of select="string(TXD/TXD01)"/>
            </common:indicator>
            <xsl:for-each select="TXD/TXD02">
                <common:tax>
                    <common:amount>
                        <xsl:value-of select="string(TXD0201)"/>
                    </common:amount>
                    <common:countryCode>
                        <xsl:value-of select="string(TXD0202)"/>
                    </common:countryCode>
                    <common:currencyCode>
                        <xsl:value-of select="string(TXD0203)"/>
                    </common:currencyCode>
                    <common:type>
                        <xsl:value-of select="string(TXD0204)"/>
                    </common:type>
                    <common:taxFiledAmount>
                        <xsl:value-of select="string(TXD0205)"/>
                    </common:taxFiledAmount>
                    <common:taxFiledCurrencyCode>
                        <xsl:value-of select="string(TXD0206)"/>
                    </common:taxFiledCurrencyCode>
                    <common:taxFiledType>
                        <xsl:value-of select="string(TXD0207)"/>
                    </common:taxFiledType>
                    <common:filedConversionRate>
                        <xsl:value-of select="string(TXD0208)"/>
                    </common:filedConversionRate>
                    <common:taxQualifier>
                        <xsl:value-of select="string(TXD0209)"/>
                    </common:taxQualifier>
                </common:tax>
            </xsl:for-each>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="PTS">
        <xsl:param name="element_name"/>
        <xsl:if test="PTS">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
            <common:fareBasis>
                <xsl:value-of select="string(PTS/PTS02/PTS0201)"/>
            </common:fareBasis>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="EBD">
        <xsl:param name="element_name"/>
        <xsl:if test="EBD">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
            <common:quantity>
                <xsl:value-of select="string(EBD/EBD02/EBD0201)"/>
            </common:quantity>
            <common:chargeQualifier>
                <xsl:value-of select="string(EBD/EBD02/EBD0203)"/>
            </common:chargeQualifier>
            <common:measurementUnit>
                <xsl:value-of select="string(EBD/EBD02/EBD0204)"/>
            </common:measurementUnit>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="DAT">
        <xsl:param name="element_name"/>
        <xsl:for-each select="DAT/DAT01">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
            <common:qualifier>
                <xsl:value-of select="string(DAT0101)"/>
            </common:qualifier>
            <xsl:if test="DAT0102">
                <common:date>
                    <xsl:choose>
                        <xsl:when test="DAT0103">
                            <xsl:value-of select="converter:toXmlDateTime(DAT0102, 'ddMMyy', DAT0103, 'HHmm')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="converter:toXmlDateTime(DAT0102, 'ddMMyy', '0000', 'HHmm')"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </common:date>
            </xsl:if>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="CRI">
        <xsl:param name="element_name"/>
        <xsl:if test="CRI">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
            <common:referenceQualifier>
                <xsl:value-of select="string(CRI/CRI01/CRI0101)"/>
            </common:referenceQualifier>
            <common:referenceNumber>
                <xsl:value-of select="string(CRI/CRI01/CRI0102)"/>
            </common:referenceNumber>
            <common:partyId>
                <xsl:value-of select="string(CRI/CRI01/CRI0103)"/>
            </common:partyId>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="RPI">
        <xsl:param name="element_name"/>
        <xsl:if test="RPI">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
            <common:quantity>
                <xsl:value-of select="string(RPI/RPI01)"/>
            </common:quantity>
            <common:qualifier>
                <xsl:value-of select="string(RPI/RPI02)"/>
            </common:qualifier>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="ERC">
        <xsl:param name="element_name"/>
        <xsl:if test="ERC">
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;', $element_name, '&gt;')"/>
                <common:applicationErrorCode>
                    <xsl:value-of select="string(ERC/ERC0101)"/>
                </common:applicationErrorCode>
            <xsl:value-of disable-output-escaping="yes" select="concat('&lt;/', $element_name, '&gt;')"/>
        </xsl:if>
    </xsl:template>

    <!-- ================ -->

    <xsl:template name="DID">
    </xsl:template>

</xsl:stylesheet>