<xsl:stylesheet version="1.0"
                xmlns:common="http://www.isa.com/typea/common"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:converter="com.isa.thinair.gdsservices.core.typeA.helpers.XmlDataConverter">

    <xsl:template name="Header">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <xsl:for-each select="$elements">
                <UNB>
                    <UNB01>
                        <UNB0101>
                            <xsl:value-of select="common:interchangeHeader/common:syntaxId"/>
                        </UNB0101>
                        <UNB0102>
                            <xsl:value-of select="common:interchangeHeader/common:versionNo"/>
                        </UNB0102>
                    </UNB01>
                    <UNB02>
                        <UNB0201>
                            <xsl:value-of select="common:interchangeHeader/common:senderInfo/common:interchangeId"/>
                        </UNB0201>
                         <xsl:if test="string(common:interchangeHeader/common:senderInfo/common:interchangeCode) != ''">
                            <UNB0202>
                                <xsl:value-of
                                    select="common:interchangeHeader/common:senderInfo/common:interchangeCode"/>
                            </UNB0202>
                        </xsl:if> 
                        <xsl:if test="string(common:interchangeHeader/common:senderInfo/common:internalIdentificationCode) != ''">
                            <UNB0203>
                                <xsl:value-of
                                    select="common:interchangeHeader/common:senderInfo/common:internalIdentificationCode"/>
                            </UNB0203>
                        </xsl:if>                     
                    </UNB02>
                    <UNB03>
                        <UNB0301>
                            <xsl:value-of select="common:interchangeHeader/common:receiverInfo/common:interchangeId"/>
                        </UNB0301>
                         <xsl:if test="string(common:interchangeHeader/common:receiverInfo/common:interchangeCode) != ''">
                            <UNB0302>
                                <xsl:value-of
                                    select="common:interchangeHeader/common:receiverInfo/common:interchangeCode"/>
                            </UNB0302>
                        </xsl:if>
                        <xsl:if test="string(common:interchangeHeader/common:receiverInfo/common:internalIdentificationCode) != ''">
                            <UNB0303>
                                <xsl:value-of
                                    select="common:interchangeHeader/common:receiverInfo/common:internalIdentificationCode"/>
                            </UNB0303>
                        </xsl:if>                      
                    </UNB03>
                    <UNB05>
                        <xsl:value-of select="common:interchangeHeader/common:interchangeControlRef"/>
                    </UNB05>
                    <UNB06>
                        <UNB0601>
                            <xsl:value-of select="common:interchangeHeader/common:recipientRef"/>
                        </UNB0601>
                    </UNB06>
                    <UNB08>
                        <xsl:value-of select="common:interchangeHeader/common:associationCode"/>
                    </UNB08>
                </UNB>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="MessageHeader">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <xsl:for-each select="$elements">
                <UNH>
                    <UNH01>
                        <xsl:value-of select="common:referenceNo"/>
                    </UNH01>
                    <UNH02>
                        <UNH0201>
                            <xsl:value-of select="common:messageId/common:messageType"/>
                        </UNH0201>
                        <UNH0202>
                            <xsl:value-of select="common:messageId/common:version"/>
                        </UNH0202>
                        <UNH0203>
                            <xsl:value-of select="common:messageId/common:releaseNumber"/>
                        </UNH0203>
                        <UNH0204>
                            <xsl:value-of select="common:messageId/common:controllingAgency"/>
                        </UNH0204>
                    </UNH02>
                    <UNH03>
                        <xsl:value-of select="common:commonAccessReference"/>
                    </UNH03>
                </UNH>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="ErrorInformation">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <xsl:for-each select="$elements">
                <ERC>
                    <ERC0101>
                        <xsl:value-of select="common:applicationErrorCode"/>
                    </ERC0101>
                </ERC>
                <!--<xsl:if test="common:subjectQualifier and common:text">-->
                    <!--<IFT>-->
                        <!--<IFT01>-->
                            <!--<IFT0101>-->
                                <!--<xsl:value-of select="common:subjectQualifier"/>-->
                            <!--</IFT0101>-->
                        <!--</IFT01>-->
                        <!--<xsl:for-each select="common:text">-->
                            <!--<IFT02>-->
                                <!--<xsl:value-of select="."/>-->
                            <!--</IFT02>-->
                        <!--</xsl:for-each>-->
                    <!--</IFT>-->
                <!--</xsl:if>-->
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="MessageFunction">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <xsl:for-each select="$elements">
                <MSG>
                    <MSG01>
                        <MSG0101>
                            <xsl:value-of select="string(common:serviceType)"/>
                        </MSG0101>
                        <MSG0102>
                            <xsl:value-of select="string(common:messageFunction)"/>
                        </MSG0102>
                        <xsl:if test="common:subMessageFunction != ''">
                            <MSG0104>
                                <xsl:value-of select="string(common:subMessageFunction)"/>
                            </MSG0104>
                        </xsl:if>
                    </MSG01>
                    <xsl:if test="common:responseType != ''">
                        <MSG02>
                            <xsl:value-of select="string(common:responseType)"/>
                        </MSG02>
                    </xsl:if>
                </MSG>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="TravellerInformation">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <xsl:for-each select="$elements">
                <TIF>
                    <TIF01>
                        <TIF0101>
                            <xsl:value-of select="string(common:travellerSurname)"/>
                        </TIF0101>
                        <TIF0102>
                            <xsl:value-of select="string(common:travellerQualifier)"/>
                        </TIF0102>
                        <xsl:if test="common:travellerCount != null">
                            <TIF0103>
                                <xsl:value-of select="string(common:travellerCount)"/>
                            </TIF0103>
                        </xsl:if>
                    </TIF01>
                    <xsl:for-each select="common:travellers">
                        <TIF02>
                            <xsl:choose>
                                <xsl:when test="common:title != ''">
                                    <TIF0201>
                                        <xsl:value-of select="concat(common:givenName, ' ', common:title)"/>
                                    </TIF0201>
                                </xsl:when>
                                <xsl:otherwise>
                                    <TIF0201>
                                        <xsl:value-of select="string(common:givenName)"/>
                                    </TIF0201>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:if test="common:passengerType != ''">
                                <TIF0202>
                                    <xsl:value-of select="string(common:passengerType)"/>
                                </TIF0202>
                            </xsl:if>
                            <xsl:if test="common:travellerReferenceNumber != ''">
                                <TIF0203>
                                    <xsl:value-of select="string(common:travellerReferenceNumber)"/>
                                </TIF0203>
                            </xsl:if>
                            <xsl:if test="common:hasInfant != ''">
                                <TIF0204>
                                    <xsl:value-of select="string(common:hasInfant)"/>
                                </TIF0204>
                            </xsl:if>
                            <xsl:if test="common:otherNames != ''">
                                <TIF0205>
                                    <xsl:value-of select="string(common:otherNames)"/>
                                </TIF0205>
                            </xsl:if>
                        </TIF02>
                    </xsl:for-each>
                </TIF>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="ReservationControlInformation">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <RCI>
                <xsl:for-each select="$elements">
                    <RCI01>
                        <RCI0101>
                            <xsl:value-of select="string(common:airlineCode)"/>
                        </RCI0101>
                        <RCI0102>
                            <xsl:value-of select="string(common:reservationControlNumber)"/>
                        </RCI0102>
                        <xsl:if test="common:reservationControlType != ''">
                            <RCI0103>
                                <xsl:value-of select="string(common:reservationControlType)"/>
                            </RCI0103>
                        </xsl:if>
                        <xsl:if test="common:recordCreateTimestamp != ''">
                            <RCI0104>
                                <xsl:value-of select="converter:formatDate(common:recordCreateTimestamp, 'ddMMyy')"/>
                            </RCI0104>
                            <RCI0105>
                                <xsl:value-of select="converter:formatDate(common:recordCreateTimestamp, 'HHmmss')"/>
                            </RCI0105>
                        </xsl:if>
                    </RCI01>
                </xsl:for-each>
            </RCI>
        </xsl:if>
    </xsl:template>

    <xsl:template name="OriginatorInformation">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <xsl:for-each select="$elements">
                <ORG>
                    <ORG01>
                        <xsl:if test="common:senderSystemDetails/common:companyCode != ''">
                            <ORG0101>
                                <xsl:value-of select="string(common:senderSystemDetails/common:companyCode)"/>
                            </ORG0101>
                        </xsl:if>
                        <xsl:if test="common:senderSystemDetails/common:location/common:locationCode != ''">
                            <ORG0102>
                                <xsl:value-of
                                    select="string(common:senderSystemDetails/common:location/common:locationCode)"/>
                            </ORG0102>
                        </xsl:if>
                        <xsl:if test="common:senderSystemDetails/common:location/common:locationName != ''">
                            <ORG0103>
                                <xsl:value-of
                                    select="string(common:senderSystemDetails/common:location/common:locationName)"/>
                            </ORG0103>
                        </xsl:if>
                    </ORG01>
                    <ORG02>
                        <xsl:if test="common:agentId != ''">
                            <ORG0201>
                                <xsl:value-of select="string(common:agentId)"/>
                            </ORG0201>
                        </xsl:if>
                        <xsl:if test="common:officeId != ''">
                            <ORG0202>
                                <xsl:value-of select="string(common:officeId)"/>
                            </ORG0202>
                        </xsl:if>
                        <xsl:if test="common:systemId != ''">
                            <ORG0203>
                                <xsl:value-of select="string(common:systemId)"/>
                            </ORG0203>
                        </xsl:if>
                    </ORG02>
                    <ORG03>
                        <xsl:if test="common:agentLocation/common:locationCode != ''">
                            <ORG0301>
                                <xsl:value-of select="string(common:agentLocation/common:locationCode)"/>
                            </ORG0301>
                        </xsl:if>
                        <xsl:if test="common:agentLocation/common:locationName != ''" xml:base="">
                            <ORG0302>
                                <xsl:value-of select="string(common:agentLocation/common:locationName)"/>
                            </ORG0302>
                        </xsl:if>
                    </ORG03>
                    <ORG04>
                        <xsl:if test="common:initiatorSystemDetails/common:iataAgentCode != ''">
                            <ORG0401>
                                <xsl:value-of select="string(common:initiatorSystemDetails/common:iataAgentCode)"/>
                            </ORG0401>
                        </xsl:if>
                        <xsl:if test="common:initiatorSystemDetails/common:agentCountryCode != ''">
                            <ORG0402>
                                <xsl:value-of select="string(common:initiatorSystemDetails/common:agentCountryCode)"/>
                            </ORG0402>
                        </xsl:if>
                        <xsl:if test="common:initiatorSystemDetails/common:agentCurrencyCode != ''">
                            <ORG0403>
                                <xsl:value-of select="string(common:initiatorSystemDetails/common:agentCurrencyCode)"/>
                            </ORG0403>
                        </xsl:if>
                    </ORG04>
                    <xsl:if test="common:originatorTypeCode != ''">
                        <ORG05>
                            <xsl:value-of select="string(common:originatorTypeCode)"/>
                        </ORG05>
                    </xsl:if>
                    <ORG06>
                        <xsl:if test="common:agentCountryCode != ''">
                            <ORG0601>
                                <xsl:value-of select="string(common:agentCountryCode)"/>
                            </ORG0601>
                        </xsl:if>
                        <xsl:if test="common:agentCurrencyCode != ''">
                            <ORG0602>
                                <xsl:value-of select="string(common:agentCurrencyCode)"/>
                            </ORG0602>
                        </xsl:if>
                        <xsl:if test="common:agentLanguageCode != ''">
                            <ORG0602>
                                <xsl:value-of select="string(common:agentLanguageCode)"/>
                            </ORG0602>
                        </xsl:if>
                    </ORG06>
                    <xsl:if test="common:originatorAuthorityCode != ''">
                        <ORG07>
                            <xsl:value-of select="string(common:originatorAuthorityCode)"/>
                        </ORG07>
                    </xsl:if>
                    <xsl:if test="common:communicationNumber != ''">
                        <ORG08>
                            <xsl:value-of select="string(common:communicationNumber)"/>
                        </ORG08>
                    </xsl:if>
                    <xsl:if test="common:partyId != ''">
                        <ORG09>
                            <xsl:value-of select="string(common:partyId)"/>
                        </ORG09>
                    </xsl:if>
                </ORG>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="MonetaryInformation">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <MON>
                <xsl:for-each select="$elements">
                    <xsl:choose>
                        <xsl:when test="position() = 1">
                            <MON01>
                                <xsl:if test="common:amountTypeQualifier != ''">
                                    <MON0101>
                                        <xsl:value-of select="string(common:amountTypeQualifier)"/>
                                    </MON0101>
                                </xsl:if>
                                <xsl:if test="common:amount != ''">
                                    <MON0102>
                                        <xsl:value-of select="string(common:amount)"/>
                                    </MON0102>
                                </xsl:if>
                                <xsl:if test="common:currencyCode != ''">
                                    <MON0103>
                                        <xsl:value-of select="string(common:currencyCode)"/>
                                    </MON0103>
                                </xsl:if>
                                <!--<MON0104>-->
                                <!--<xsl:value-of select="string(common:departureLocation)"/>-->
                                <!--</MON0104>-->
                                <!--<MON0105>-->
                                <!--<xsl:value-of select="string(common:arrivalLocation)"/>-->
                                <!--</MON0105>-->
                            </MON01>
                        </xsl:when>
                        <xsl:otherwise>
                            <MON02>
                                <xsl:if test="common:amountTypeQualifier != ''">
                                    <MON0201>
                                        <xsl:value-of select="string(common:amountTypeQualifier)"/>
                                    </MON0201>
                                </xsl:if>
                                <xsl:if test="common:amount != ''">
                                    <MON0202>
                                        <xsl:value-of select="string(common:amount)"/>
                                    </MON0202>
                                </xsl:if>
                                <xsl:if test="common:currencyCode != ''">
                                    <MON0203>
                                        <xsl:value-of select="string(common:currencyCode)"/>
                                    </MON0203>
                                </xsl:if>
                                <!--<MON0204>-->
                                <!--<xsl:value-of select="string(common:departureLocation)"/>-->
                                <!--</MON0204>-->
                                <!--<MON0205>-->
                                <!--<xsl:value-of select="string(common:arrivalLocation)"/>-->
                                <!--</MON0205>-->
                            </MON02>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </MON>
        </xsl:if>
    </xsl:template>

    <xsl:template name="FormOfPayment">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <FOP>
                <xsl:for-each select="$elements">
                    <FOP01>
                        <xsl:if test="common:formOfPaymentType != ''">
                            <FOP0101>
                                <xsl:value-of select="string(common:formOfPaymentType)"/>
                            </FOP0101>
                        </xsl:if>
                        <xsl:if test="common:dataIndicator != ''">
                            <FOP0102>
                                <xsl:value-of select="string(common:dataIndicator)"/>
                            </FOP0102>
                        </xsl:if>
                        <xsl:if test="common:paymentAmount != ''">
                            <FOP0103>
                                <xsl:value-of select="string(common:paymentAmount)"/>
                            </FOP0103>
                        </xsl:if>
                        <xsl:if test="common:referenceNumber != ''">
                            <FOP0105>
                                <xsl:value-of select="string(common:referenceNumber)"/>
                            </FOP0105>
                        </xsl:if>
                        <xsl:if test="common:additionalCreditCardDetails/common:vendorCode != ''">
                            <FOP0104>
                                <xsl:value-of select="string(common:additionalCreditCardDetails/common:vendorCode)"/>
                            </FOP0104>
                        </xsl:if>
                        <xsl:if test="common:additionalCreditCardDetails/common:expirationDate != ''">
                            <FOP0106>
                                <xsl:value-of
                                    select="string(common:additionalCreditCardDetails/common:expirationDate)"/>
                            </FOP0106>
                        </xsl:if>
                        <xsl:if test="common:additionalCreditCardDetails/common:approvalCode != ''">
                            <FOP0107>
                                <xsl:value-of select="string(common:additionalCreditCardDetails/common:approvalCode)"/>
                            </FOP0107>
                        </xsl:if>
                        <xsl:if test="common:additionalCreditCardDetails/common:sourceOfApproval != ''">
                            <FOP0108>
                                <xsl:value-of
                                    select="string(common:additionalCreditCardDetails/common:sourceOfApproval)"/>
                            </FOP0108>
                        </xsl:if>
                        <xsl:if test="common:additionalCreditCardDetails/common:authorizedAmount != ''">
                            <FOP0109>
                                <xsl:value-of
                                    select="string(common:additionalCreditCardDetails/common:authorizedAmount)"/>
                            </FOP0109>
                        </xsl:if>
                        <xsl:if test="common:additionalCreditCardDetails/common:addressVerificationCode != ''">
                            <FOP0110>
                                <xsl:value-of
                                    select="string(common:additionalCreditCardDetails/common:addressVerificationCode)"/>
                            </FOP0110>
                        </xsl:if>
                        <xsl:if test="common:additionalCreditCardDetails/common:extendedPaymentCode != ''">
                            <FOP0112>
                                <xsl:value-of
                                    select="string(common:additionalCreditCardDetails/common:extendedPaymentCode)"/>
                            </FOP0112>
                        </xsl:if>
                        <xsl:if test="common:additionalCreditCardDetails/common:membershipStatus != ''">
                            <FOP0114>
                                <xsl:value-of
                                    select="string(common:additionalCreditCardDetails/common:membershipStatus)"/>
                            </FOP0114>
                        </xsl:if>
                        <xsl:if test="common:additionalCreditCardDetails/common:creditCardTransactionInformation != ''">
                            <FOP0115>
                                <xsl:value-of
                                    select="string(common:additionalCreditCardDetails/common:creditCardTransactionInformation)"/>
                            </FOP0115>
                        </xsl:if>
                        <xsl:if test="common:accountHolderNumber != ''">
                            <FOP0111>
                                <xsl:value-of select="string(common:accountHolderNumber)"/>
                            </FOP0111>
                        </xsl:if>
                        <xsl:if test="common:text != ''">
                            <FOP0113>
                                <xsl:value-of select="string(common:text)"/>
                            </FOP0113>
                        </xsl:if>
                    </FOP01>
                </xsl:for-each>
            </FOP>
        </xsl:if>
    </xsl:template>

    <xsl:template name="PricingTicketingDetails">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <xsl:for-each select="$elements">
                <PTK>
                    <PTK01>
                        <xsl:for-each select="common:pricingTicketingIndicators">
                            <xsl:variable name="elementName" select="converter:buildElementName('PTK01', common:key)"/>
                            <xsl:value-of disable-output-escaping="yes"
                            select="concat('&lt;', $elementName, '&gt;')"/>
                            <xsl:value-of select="string(common:value)"/>
                            <xsl:value-of disable-output-escaping="yes"
                            select="concat('&lt;/', $elementName, '&gt;')"/>
                        </xsl:for-each>
                    </PTK01>
                    <xsl:if test="common:ticketingTime">
                        <PTK03>
                            <PTK0301>
                                <xsl:value-of select="converter:formatDate(common:ticketingTime, 'ddMMyy')"/>
                            </PTK0301>
                        </PTK03>
                    </xsl:if>
                    <PTK06>
                        <xsl:if test="common:ticketingLocation/common:countryCode != ''">
                            <PTK0602>
                                <xsl:value-of select="string(common:ticketingLocation/common:countryCode)"/>
                            </PTK0602>
                        </xsl:if>
                    </PTK06>
                </PTK>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="OriginDestinationInformation">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <xsl:for-each select="$elements">
                <ODI>
                    <xsl:if test="common:departureAirportCityCode != ''">
                        <ODI01>
                            <xsl:value-of select="string(common:departureAirportCityCode)"/>
                        </ODI01>
                    </xsl:if>
                    <xsl:if test="common:arrivalAirportCityCode != ''">
                        <ODI01>
                            <xsl:value-of select="string(common:arrivalAirportCityCode)"/>
                        </ODI01>
                    </xsl:if>
                </ODI>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="InteractiveFreeText">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <xsl:for-each select="$elements">
                <IFT>
                    <IFT01>
                        <xsl:if test="common:textSubjectQualifier != ''">
                            <IFT0101>
                                <xsl:value-of select="string(common:textSubjectQualifier)"/>
                            </IFT0101>
                        </xsl:if>
                        <xsl:if test="common:informationType != ''">
                            <IFT0102>
                                <xsl:value-of select="string(common:informationType)"/>
                            </IFT0102>
                        </xsl:if>
                        <xsl:if test="common:status != ''">
                            <IFT0103>
                                <xsl:value-of select="string(common:status)"/>
                            </IFT0103>
                        </xsl:if>
                    </IFT01>
                    <xsl:for-each select="common:freeText">
                        <xsl:if test=". != ''">
                            <IFT02>
                                <xsl:value-of select="string(.)"/>
                            </IFT02>
                        </xsl:if>
                    </xsl:for-each>
                </IFT>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="TicketNumberDetails">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <xsl:for-each select="$elements">
                <TKT>
                    <TKT01>
                        <xsl:if test="common:ticketNumber != ''">
                            <TKT0101>
                                <xsl:value-of select="common:ticketNumber"/>
                            </TKT0101>
                        </xsl:if>
                        <xsl:if test="common:documentType != ''">
                            <TKT0102>
                                <xsl:value-of select="common:documentType"/>
                            </TKT0102>
                        </xsl:if>
                        <xsl:if test="common:totalItems != ''">
                            <TKT0103>
                                <xsl:value-of select="common:totalItems"/>
                            </TKT0103>
                        </xsl:if>
                        <xsl:if test="common:dataIndicator != ''">
                            <TKT0104>
                                <xsl:value-of select="common:dataIndicator"/>
                            </TKT0104>
                        </xsl:if>
                    </TKT01>
                </TKT>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="TicketCoupon">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <xsl:for-each select="$elements">
                <CPN>
                    <CPN01>
                        <xsl:if test="common:couponNumber != ''">
                            <CPN0101>
                                <xsl:value-of select="common:couponNumber"/>
                            </CPN0101>
                        </xsl:if>
                        <xsl:if test="common:status != ''">
                            <CPN0102>
                                <xsl:value-of select="common:status"/>
                            </CPN0102>
                        </xsl:if>
                        <xsl:if test="common:couponValue != ''">
                            <CPN0103>
                                <xsl:value-of select="common:couponValue"/>
                            </CPN0103>
                        </xsl:if>
                        <xsl:if test="common:exchangeMedia != ''">
                            <CPN0104>
                                <xsl:value-of select="common:exchangeMedia"/>
                            </CPN0104>
                        </xsl:if>
                        <xsl:if test="common:settlementAuthCode != ''">
                            <CPN0105>
                                <xsl:value-of select="common:settlementAuthCode"/>
                            </CPN0105>
                        </xsl:if>
                        <xsl:if test="common:voluntaryInvoluntaryIndicator != ''">
                            <CPN0106>
                                <xsl:value-of select="common:voluntaryInvoluntaryIndicator"/>
                            </CPN0106>
                        </xsl:if>
                        <xsl:if test="common:previousStatus != ''">
                            <CPN0107>
                                <xsl:value-of select="common:previousStatus"/>
                            </CPN0107>
                        </xsl:if>
                    </CPN01>
                </CPN>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="FlightInformation">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <xsl:for-each select="$elements">
                <TVL>
                    <TVL01>
                        <TVL0101>
                            <xsl:value-of select="converter:formatDate(common:departureDateTime, 'ddMMyy')"/>
                        </TVL0101>
                        <TVL0102>
                            <xsl:value-of select="converter:formatDate(common:departureDateTime,'HHmm')"/>
                        </TVL0102>
                        <TVL0103>
                            <xsl:value-of select="converter:formatDate(common:arrivalDateTime, 'ddMMyy')"/>
                        </TVL0103>
                        <TVL0104>
                            <xsl:value-of select="converter:formatDate(common:arrivalDateTime,'HHmm')"/>
                        </TVL0104>
                    </TVL01>
                    <TVL02>
                        <xsl:if test="common:departureLocation/common:locationCode != ''">
                            <TVL0201>
                                <xsl:value-of select="string(common:departureLocation/common:locationCode)"/>
                            </TVL0201>
                        </xsl:if>
                        <xsl:if test="common:departureLocation/common:locationName != ''">
                            <TVL0202>
                                <xsl:value-of select="string(common:departureLocation/common:locationName)"/>
                            </TVL0202>
                        </xsl:if>
                    </TVL02>
                    <TVL03>
                        <xsl:if test="common:arrivalLocation/common:locationCode != ''">
                            <TVL0301>
                                <xsl:value-of select="string(common:arrivalLocation/common:locationCode)"/>
                            </TVL0301>
                        </xsl:if>
                        <xsl:if test="common:arrivalLocation/common:locationName != ''">
                            <TVL0302>
                                <xsl:value-of select="string(common:arrivalLocation/common:locationName)"/>
                            </TVL0302>
                        </xsl:if>
                    </TVL03>
                    <TVL04>
                        <xsl:if test="common:companyInfo/common:marketingAirlineCode != ''">
                            <TVL0401>
                                <xsl:value-of select="string(common:companyInfo/common:marketingAirlineCode)"/>
                            </TVL0401>
                        </xsl:if>
                        <xsl:if test="common:companyInfo/common:operatingAirlineCode != ''">
                            <TVL0402>
                                <xsl:value-of select="string(common:companyInfo/common:operatingAirlineCode)"/>
                            </TVL0402>
                        </xsl:if>
                    </TVL04>
                    <TVL05>
                        <xsl:if test="common:flightNumber != ''">
                            <TVL0501>
                                <xsl:value-of select="string(common:flightNumber)"/>
                            </TVL0501>
                        </xsl:if>
                        <xsl:if test="common:flightPreference/common:bookingClass != ''">
                            <TVL0502>
                                <xsl:value-of select="string(common:flightPreference/common:bookingClass)"/>
                            </TVL0502>
                        </xsl:if>
                        <xsl:if test="common:flightPreference/common:bookingClassModifier != ''">
                            <TVL0504>
                                <xsl:value-of
                                    select="string(common:flightPreference/common:bookingClassModifier)"/>
                            </TVL0504>
                        </xsl:if>
                    </TVL05>
                    <TVL06>
                        <xsl:for-each select="common:flightRemarks">
                            <xsl:if test=". != ''">
                                <xsl:value-of disable-output-escaping="yes"
                                              select="concat('&lt;TVL060', position(), '&gt;')"/>
                                <xsl:value-of select="string(.)"/>
                                <xsl:value-of disable-output-escaping="yes"
                                              select="concat('&lt;/TVL060', position(), '&gt;')"/>
                            </xsl:if>
                        </xsl:for-each>
                    </TVL06>
                </TVL>
                <!--<RPI>-->
                    <!--<xsl:if test="common:travellerCount != ''">-->
                        <!--<RPI01>-->
                            <!--<xsl:value-of select="common:travellerCount"/>-->
                        <!--</RPI01>-->
                    <!--</xsl:if>-->
                    <!--<xsl:if test="common:actionCode != ''">-->
                        <!--<RPI02>-->
                            <!--<xsl:value-of select="common:actionCode"/>-->
                        <!--</RPI02>-->
                    <!--</xsl:if>-->
                <!--</RPI>-->
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="DateAndTimeInformation">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <DAT>
                <xsl:for-each select="$elements">
                    <DAT01>
                        <xsl:if test="common:qualifier != ''">
                            <DAT0101>
                                <xsl:value-of select="common:qualifier"/>
                            </DAT0101>
                        </xsl:if>
                        <xsl:if test="common:date">
                            <DAT0102>
                                <xsl:value-of select="converter:formatDate(common:date, 'ddMMyy')"/>
                            </DAT0102>
                            <DAT0103>
                                <xsl:value-of select="converter:formatDate(common:date, 'HHmm')"/>
                            </DAT0103>
                        </xsl:if>
                    </DAT01>
                </xsl:for-each>
            </DAT>
        </xsl:if>
    </xsl:template>

    <xsl:template name="NumberOfUnit">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <EQN>
                <xsl:for-each select="$elements">
                    <EQN01>
                        <xsl:if test="common:quantity != ''">
                            <EQN0101>
                                <xsl:value-of select="common:quantity"/>
                            </EQN0101>
                        </xsl:if>
                        <xsl:if test="common:qualifier != ''">
                            <EQN0102>
                                <xsl:value-of select="common:qualifier"/>
                            </EQN0102>
                        </xsl:if>
                    </EQN01>
                </xsl:for-each>
            </EQN>
        </xsl:if>
    </xsl:template>

    <xsl:template name="FrequentTravellerInformation">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <FTI>
                <xsl:for-each select="$elements">
                    <FTI01>
                        <xsl:if test="common:airlineDesignator != ''">
                            <FTI0101>
                                <xsl:value-of select="common:airlineDesignator"/>
                            </FTI0101>
                        </xsl:if>
                        <xsl:if test="common:frequentTravellerNumber != ''">
                            <FTI0102>
                                <xsl:value-of select="common:frequentTravellerNumber"/>
                            </FTI0102>
                        </xsl:if>
                    </FTI01>
                </xsl:for-each>
            </FTI>
        </xsl:if>
    </xsl:template>

    <xsl:template name="PricingTicketingSubsequent">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <xsl:for-each select="$elements">
                <PTS>
                    <PTS02>
                        <xsl:if test="common:fareBasis != ''">
                            <PTS0201>
                                <xsl:value-of select="common:fareBasis"/>
                            </PTS0201>
                        </xsl:if>
                    </PTS02>
                </PTS>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="TicketingAgentInformation">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <xsl:for-each select="$elements">
                <TAI>
                    <xsl:if test="common:companyIdentifier != ''">
                        <TAI01>
                            <xsl:value-of select="common:companyIdentifier"/>
                        </TAI01>
                    </xsl:if>
                    <xsl:for-each select="common:internalIdentificationDetails">
                        <TAI02>
                            <xsl:if test="common:requesterId != ''">
                                <TAI0201>
                                    <xsl:value-of select="common:requesterId"/>
                                </TAI0201>
                            </xsl:if>
                            <xsl:if test="common:identificationType != ''">
                                <TAI0202>
                                    <xsl:value-of select="common:identificationType"/>
                                </TAI0202>
                            </xsl:if>
                            <!--<xsl:if test="common:agentId != ''">-->
                                <!--<TAI0203>-->
                                    <!--<xsl:value-of select="common:agentId"/>-->
                                <!--</TAI0203>-->
                            <!--</xsl:if>-->
                        </TAI02>
                    </xsl:for-each>
                </TAI>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="TaxDetails">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <xsl:for-each select="$elements">
                <TXD>
                    <xsl:if test="common:indicator != ''">
                        <TXD01>
                            <xsl:value-of select="common:indicator"/>
                        </TXD01>
                    </xsl:if>
                    <xsl:for-each select="common:tax">
                        <TXD02>
                            <xsl:if test="common:amount != ''">
                                <TXD0201>
                                    <xsl:value-of select="common:amount"/>
                                </TXD0201>
                            </xsl:if>
                            <xsl:if test="common:countryCode != ''">
                                <TXD0202>
                                    <xsl:value-of select="common:countryCode"/>
                                </TXD0202>
                            </xsl:if>
                            <xsl:if test="common:currencyCode != ''">
                                <TXD0203>
                                    <xsl:value-of select="common:currencyCode"/>
                                </TXD0203>
                            </xsl:if>
                            <xsl:if test="common:type != ''">
                                <TXD0204>
                                    <xsl:value-of select="common:type"/>
                                </TXD0204>
                            </xsl:if>
                            <xsl:if test="common:taxFiledAmount != ''">
                                <TXD0205>
                                    <xsl:value-of select="common:taxFiledAmount"/>
                                </TXD0205>
                            </xsl:if>
                            <xsl:if test="common:taxFiledCurrencyCode != ''">
                                <TXD0206>
                                    <xsl:value-of select="common:taxFiledCurrencyCode"/>
                                </TXD0206>
                            </xsl:if>
                            <xsl:if test="common:taxFiledType != ''">
                                <TXD0207>
                                    <xsl:value-of select="common:taxFiledType"/>
                                </TXD0207>
                            </xsl:if>
                            <xsl:if test="common:filedConversionRate != ''">
                                <TXD0208>
                                    <xsl:value-of select="common:filedConversionRate"/>
                                </TXD0208>
                            </xsl:if>
                            <xsl:if test="common:taxQualifier != ''">
                                <TXD0209>
                                    <xsl:value-of select="common:taxQualifier"/>
                                </TXD0209>
                            </xsl:if>
                        </TXD02>
                    </xsl:for-each>
                </TXD>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="AdditionalTourInformation">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <xsl:for-each select="$elements">
                <ATI>
                    <ATI01>
                        <xsl:if test="common:productId != ''">
                            <ATI0101>
                                <xsl:value-of select="common:productId"/>
                            </ATI0101>
                        </xsl:if>
                    </ATI01>
                </ATI>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="ConsumerReferenceInformation">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <xsl:for-each select="$elements">
                <CRI>
                    <CRI01>
                        <xsl:if test="common:referenceQualifier != ''">
                            <CRI0101>
                                <xsl:value-of select="common:referenceQualifier"/>
                            </CRI0101>
                        </xsl:if>
                        <xsl:if test="common:referenceNumber != ''">
                            <CRI0102>
                                <xsl:value-of select="common:referenceNumber"/>
                            </CRI0102>
                        </xsl:if>
                        <xsl:if test="common:partyId != ''">
                            <CRI0103>
                                <xsl:value-of select="common:partyId"/>
                            </CRI0103>
                        </xsl:if>
                    </CRI01>
                </CRI>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="RelatedProductInformation">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <xsl:for-each select="$elements">
                <RPI>
                    <xsl:if test="common:quantity != ''">
                        <RPI01>
                            <xsl:value-of select="common:quantity"/>
                        </RPI01>
                    </xsl:if>
                    <xsl:if test="common:qualifier != ''">
                        <RPI02>
                            <xsl:value-of select="common:qualifier"/>
                        </RPI02>
                    </xsl:if>
                </RPI>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="ExcessBaggageInformation">
        <xsl:param name="elements"/>
        <xsl:if test="count($elements) > 0">
            <xsl:for-each select="$elements">
                <EBD>
                    <EBD02>
                        <xsl:if test="common:quantity != ''">
                            <EBD0201>
                                <xsl:value-of select="common:quantity"/>
                            </EBD0201>
                        </xsl:if>
                        <xsl:if test="common:chargeQualifier != ''">
                            <EBD0203>
                                <xsl:value-of select="common:chargeQualifier"/>
                            </EBD0203>
                        </xsl:if>
                        <xsl:if test="common:measurementUnit != ''">
                            <EBD0204>
                                <xsl:value-of select="common:measurementUnit"/>
                            </EBD0204>
                        </xsl:if>
                    </EBD02>
                </EBD>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <xsl:template name="DocumentInformationDetails">
        <xsl:param name="elements"/>
        <!--<xsl:if test="count($elements) > 0">-->
        <!--<xsl:for-each select="$elements">-->
        <!--<DID>-->
        <!--</DID>-->
        <!--</xsl:for-each>-->
        <!--</xsl:if>-->
    </xsl:template>

</xsl:stylesheet>