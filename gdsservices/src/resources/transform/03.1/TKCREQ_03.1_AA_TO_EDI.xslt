<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ns0="http://www.isa.com/typea/common" xmlns:ns1="http://www.isa.com/typea/tkcreq"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="ns0 ns1 xs"
                xmlns:converter="com.isa.thinair.gdsservices.core.typeA.helpers.XmlDataConverter">

    <!--<xsl:import href="/resources/transform/xx.x/COMMON_XX.X_AA_TO_EDI.xslt"/>-->
    <xsl:import href="/resources/transform/03.1/COMMON_03.1_AA_TO_EDI.xslt"/>
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>

    <xsl:template match="/">
        <IATA>

            <xsl:for-each select="ns1:AA_TKCREQ">

                <xsl:call-template name="Header">
                    <xsl:with-param name="elements" select="ns0:header"/>
                </xsl:call-template>

                <TKCREQ>
                    <xsl:for-each select="message">

                        <xsl:call-template name="MessageHeader">
                            <xsl:with-param name="elements" select="ns0:messageHeader"/>
                        </xsl:call-template>

                        <xsl:call-template name="MessageFunction">
                            <xsl:with-param name="elements" select="messageFunction"/>
                        </xsl:call-template>

                        <xsl:call-template name="OriginatorInformation">
                            <xsl:with-param name="elements" select="originatorInformation"/>
                        </xsl:call-template>

                        <xsl:call-template name="TicketingAgentInformation">
                            <xsl:with-param name="elements" select="ticketingAgentInformation"/>
                        </xsl:call-template>

                        <xsl:call-template name="ReservationControlInformation">
                            <xsl:with-param name="elements" select="reservationControlInformation"/>
                        </xsl:call-template>

                        <xsl:call-template name="NumberOfUnit">
                            <xsl:with-param name="elements" select="numberOfUnits"/>
                        </xsl:call-template>

                        <xsl:call-template name="FlightInformation">
                            <xsl:with-param name="elements" select="flightInformation"/>
                        </xsl:call-template>

                        <xsl:call-template name="TravellerInformation">
                            <xsl:with-param name="elements" select="travellerInformation"/>
                        </xsl:call-template>

                        <xsl:call-template name="FrequentTravellerInformation">
                            <xsl:with-param name="elements" select="frequentTravellerInformation"/>
                        </xsl:call-template>

                        <xsl:call-template name="FormOfPayment">
                            <xsl:with-param name="elements" select="formOfPayment"/>
                        </xsl:call-template>

                        <!--<xsl:call-template name="RelatedProductInformation">-->
                            <!--<xsl:with-param name="elements" select="relatedProductInfo"/>-->
                        <!--</xsl:call-template>-->

                        <xsl:call-template name="InteractiveFreeText">
                            <xsl:with-param name="elements" select="text"/>
                        </xsl:call-template>

                        <xsl:call-template name="ConsumerReferenceInformation">
                            <xsl:with-param name="elements" select="consumerRefInformation"/>
                        </xsl:call-template>

                        <xsl:if test="tickets">
                            <xsl:for-each select="tickets">

                                <GROUP_1>
                                    <xsl:call-template name="TicketNumberDetails">
                                        <xsl:with-param name="elements" select="."/>
                                    </xsl:call-template>

                                    <xsl:call-template name="TravellerInformation">
                                        <xsl:with-param name="elements" select="ns0:travellerInformation"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="TicketingAgentInformation">
                                        <xsl:with-param name="elements" select="ns0:ticketingAgentInformation"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="ReservationControlInformation">
                                        <xsl:with-param name="elements" select="ns0:reservationControlInformation"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="FrequentTravellerInformation">
                                        <xsl:with-param name="elements" select="ns0:frequentTravellerInformation"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="MonetaryInformation">
                                        <xsl:with-param name="elements" select="ns0:monetaryInformation"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="FormOfPayment">
                                        <xsl:with-param name="elements" select="ns0:formOfPayment"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="PricingTicketingDetails">
                                        <xsl:with-param name="elements" select="ns0:pricingTicketingDetails"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="TaxDetails">
                                        <xsl:with-param name="elements" select="ns0:taxDetails"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="OriginDestinationInformation">
                                        <xsl:with-param name="elements" select="ns0:originDestinationInfo"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="OriginatorInformation">
                                        <xsl:with-param name="elements" select="ns0:originatorInformation"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="DateAndTimeInformation">
                                        <xsl:with-param name="elements" select="ns0:dateAndTime"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="AdditionalTourInformation">
                                        <xsl:with-param name="elements" select="ns0:additionalTourInfo"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="NumberOfUnit">
                                        <xsl:with-param name="elements" select="ns0:numberOfUnits"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="InteractiveFreeText">
                                        <xsl:with-param name="elements" select="ns0:text"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="DocumentInformationDetails">
                                        <xsl:with-param name="elements" select="ns0:documentInfoDetails"/>
                                    </xsl:call-template>

                                    <xsl:call-template name="ConsumerReferenceInformation">
                                        <xsl:with-param name="elements" select="ns0:consumerRef"/>
                                    </xsl:call-template>

                                    <xsl:for-each select="ns0:ticketCoupon">
                                        <GROUP_2>
                                            <xsl:call-template name="TicketCoupon">
                                                <xsl:with-param name="elements" select="."/>
                                            </xsl:call-template>

                                            <xsl:call-template name="FlightInformation">
                                                <xsl:with-param name="elements" select="ns0:flightInfomation"/>
                                            </xsl:call-template>

                                            <xsl:call-template name="PricingTicketingSubsequent">
                                                <xsl:with-param name="elements" select="ns0:pricingTicketingSubsequent"/>
                                            </xsl:call-template>

                                            <xsl:call-template name="DateAndTimeInformation">
                                                <xsl:with-param name="elements" select="ns0:dateAndTimeInformation"/>
                                            </xsl:call-template>

                                            <xsl:call-template name="RelatedProductInformation">
                                                <xsl:with-param name="elements" select="ns0:relatedProductInfo"/>
                                            </xsl:call-template>

                                            <xsl:call-template name="InteractiveFreeText">
                                                <xsl:with-param name="elements" select="ns0:text"/>
                                            </xsl:call-template>

                                            <xsl:call-template name="ExcessBaggageInformation">
                                                <xsl:with-param name="elements" select="ns0:excessBaggageInformation"/>
                                            </xsl:call-template>

                                            <xsl:call-template name="PricingTicketingDetails">
                                                <xsl:with-param name="elements" select="ns0:pricingTicketingDetails"/>
                                            </xsl:call-template>

                                            <xsl:call-template name="FrequentTravellerInformation">
                                                <xsl:with-param name="elements" select="ns0:frequentTravellerInformation"/>
                                            </xsl:call-template>

                                        </GROUP_2>
                                    </xsl:for-each>

                                </GROUP_1>
                            </xsl:for-each>
                        </xsl:if>
                    </xsl:for-each>

                    <UNT>
                    </UNT>
                </TKCREQ>
            </xsl:for-each>

            <UNZ>
            </UNZ>
        </IATA>
    </xsl:template>
</xsl:stylesheet>