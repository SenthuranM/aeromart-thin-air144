<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ns0="http://www.isa.com/typea/common" xmlns:ns1="http://www.isa.com/typea/hwpres"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="ns0 ns1 xs"
                xmlns:converter="com.isa.thinair.gdsservices.core.typeA.helpers.XmlDataConverter">

    <xsl:import href="/resources/transform/96.2/COMMON_96.2_AA_TO_EDI.xslt"/>
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>

    <xsl:template match="/">
        <IATA>

            <xsl:for-each select="ns1:AA_HWPRES">

                <xsl:call-template name="Header">
                    <xsl:with-param name="elements" select="ns0:header"/>
                </xsl:call-template>

                <HWPRES>
                    <xsl:for-each select="ns1:message">

                        <xsl:call-template name="MessageHeader">
                            <xsl:with-param name="elements" select="ns0:messageHeader"/>
                        </xsl:call-template>

						<xsl:call-template name="MessageFunction">
                            <xsl:with-param name="elements" select="ns1:messageFunction"/>
                        </xsl:call-template>

                        <xsl:call-template name="ReservationControlInformation">
                            <xsl:with-param name="elements" select="ns1:reservationControlInformation"/>
                        </xsl:call-template>

                        <xsl:for-each select="ns1:errorInformation">
							<GROUP_1>
								<xsl:call-template name="ErrorInformation">
                                        <xsl:with-param name="elements" select="."/>
                                    </xsl:call-template>
							</GROUP_1>
                        </xsl:for-each>

                    </xsl:for-each>
                    <UNT>
                    </UNT>
                </HWPRES>
            </xsl:for-each>

            <UNZ>
            </UNZ>
        </IATA>
    </xsl:template>
</xsl:stylesheet>