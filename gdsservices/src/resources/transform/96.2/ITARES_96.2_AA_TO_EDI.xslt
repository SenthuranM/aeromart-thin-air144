<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ns0="http://www.isa.com/typea/common" xmlns:ns1="http://www.isa.com/typea/itares"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="ns0 ns1 xs"
                xmlns:converter="com.isa.thinair.gdsservices.core.typeA.helpers.XmlDataConverter">

    <xsl:import href="/resources/transform/96.2/COMMON_96.2_AA_TO_EDI.xslt"/>
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>

    <xsl:template match="/">
        <IATA>

            <xsl:for-each select="ns1:AA_ITARES">

                <xsl:call-template name="Header">
                    <xsl:with-param name="elements" select="ns0:header"/>
                </xsl:call-template>

                <ITARES>
                    <xsl:for-each select="ns1:message">

                        <xsl:call-template name="MessageHeader">
                            <xsl:with-param name="elements" select="ns0:messageHeader"/>
                        </xsl:call-template>

                        <xsl:call-template name="MessageFunction">
                            <xsl:with-param name="elements" select="ns1:messageFunction"/>
                        </xsl:call-template>

                        <xsl:for-each select="ns1:errorInformation">
                            <GROUP_1>
                                <xsl:call-template name="ErrorInformation">
                                    <xsl:with-param name="elements" select="."/>
                                </xsl:call-template>
                            </GROUP_1>
                        </xsl:for-each>

                        <xsl:for-each select="ns1:originDestinationInfo">
                            <GROUP_2>
                                <xsl:call-template name="OriginDestinationInformation">
                                    <xsl:with-param name="elements" select="."/>
                                </xsl:call-template>

                                <xsl:for-each select="ns0:errorInformation">
                                    <GROUP_3>
                                        <xsl:call-template name="ErrorInformation">
                                            <xsl:with-param name="elements" select="."/>
                                        </xsl:call-template>
                                    </GROUP_3>
                                </xsl:for-each>

                                <xsl:for-each select="ns0:flightInfomation">
                                    <GROUP_4>
                                        <xsl:call-template name="FlightInformation">
                                            <xsl:with-param name="elements" select="."/>
                                        </xsl:call-template>

                                        <xsl:for-each select="ns0:errorInformation">
                                            <GROUP_5>
                                                <xsl:call-template name="ErrorInformation">
                                                    <xsl:with-param name="elements" select="."/>
                                                </xsl:call-template>
                                            </GROUP_5>
                                        </xsl:for-each>
                                    </GROUP_4>
                                </xsl:for-each>

                            </GROUP_2>
                        </xsl:for-each>

                    </xsl:for-each>
                    <UNT>
                    </UNT>
                </ITARES>
            </xsl:for-each>

            <UNZ>
            </UNZ>
        </IATA>
    </xsl:template>
</xsl:stylesheet>