<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:common="http://www.isa.com/typea/common" xmlns:ns1="http://www.isa.com/typea/hwpreq"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs"
                xmlns:converter="com.isa.thinair.gdsservices.core.typeA.helpers.XmlDataConverter">

    <xsl:import href="/resources/transform/96.2/COMMON_96.2_EDI_TO_AA.xslt"/>
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>

    <xsl:template match="/">
        <xsl:for-each select="IATA">

            <ns1:AA_HWPREQ>

                <xsl:call-template name="UNB">
                    <xsl:with-param name="element_name">common:header</xsl:with-param>
                </xsl:call-template>

                <xsl:for-each select="HWPREQ">

                    <message>
                        <xsl:call-template name="UNH">
                            <xsl:with-param name="element_name">common:messageHeader</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="MSG">
                            <xsl:with-param name="element_name">messageFunction</xsl:with-param>
                        </xsl:call-template>

                        <xsl:for-each select="GROUP_1">
                            <xsl:call-template name="ORG">
                                <xsl:with-param name="element_name">originatorInformation</xsl:with-param>
                            </xsl:call-template>

                            <!--<xsl:call-template name="ABI"/>-->
                        </xsl:for-each>

                        <xsl:call-template name="RCI">
                            <xsl:with-param name="element_name">reservationControlInformation</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="LTS">
                            <xsl:with-param name="element_name">freeText</xsl:with-param>
                        </xsl:call-template>


                    </message>

                </xsl:for-each>

            </ns1:AA_HWPREQ>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>