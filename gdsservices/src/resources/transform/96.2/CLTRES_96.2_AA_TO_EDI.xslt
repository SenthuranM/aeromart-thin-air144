<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ns0="http://www.isa.com/typea/common" xmlns:ns1="http://www.isa.com/typea/cltres"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="ns0 ns1 xs"
                xmlns:converter="com.isa.thinair.gdsservices.core.typeA.helpers.XmlDataConverter">

    <xsl:import href="/resources/transform/96.2/COMMON_96.2_AA_TO_EDI.xslt"/>
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>

    <xsl:template match="/">
        <IATA>

            <xsl:for-each select="ns1:AA_CLTRES">
                <xsl:call-template name="Header">
                    <xsl:with-param name="elements" select="ns0:header"/>
                </xsl:call-template>

                <CLTRES>
                    <xsl:for-each select="ns1:message">
                        <xsl:call-template name="MessageHeader">
                            <xsl:with-param name="elements" select="ns0:messageHeader"/>
                        </xsl:call-template>

                        <CLT>
                            <CLT01>
                                <xsl:value-of select="ns1:clearTerminateInfo/ns0:actionRequest"/>
                            </CLT01>
                        </CLT>

                    </xsl:for-each>

                    <UNT>
                    </UNT>
                </CLTRES>
            </xsl:for-each>
            <UNZ>
            </UNZ>
        </IATA>
    </xsl:template>
</xsl:stylesheet>