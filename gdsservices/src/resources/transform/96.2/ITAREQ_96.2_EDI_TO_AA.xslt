<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:common="http://www.isa.com/typea/common" xmlns:ns1="http://www.isa.com/typea/itareq"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs"
                xmlns:converter="com.isa.thinair.gdsservices.core.typeA.helpers.XmlDataConverter">

    <xsl:import href="/resources/transform/96.2/COMMON_96.2_EDI_TO_AA.xslt"/>
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>

    <xsl:template match="/">
        <xsl:for-each select="IATA">
            <ns1:AA_ITAREQ>

                <xsl:call-template name="UNB">
                    <xsl:with-param name="element_name">common:header</xsl:with-param>
                </xsl:call-template>

                <xsl:for-each select="ITAREQ">

                    <message>
                        <xsl:call-template name="UNH">
                            <xsl:with-param name="element_name">common:messageHeader</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="MSG">
                            <xsl:with-param name="element_name">messageFunction</xsl:with-param>
                        </xsl:call-template>

                        <xsl:for-each select="GROUP_1">
                            <xsl:call-template name="ORG">
                            	<xsl:with-param name="element_name">originatorInformation</xsl:with-param>
                        	</xsl:call-template>

                            <!--<xsl:call-template name="ABI"/>-->
                        </xsl:for-each>

                        <xsl:call-template name="RCI">
                            <xsl:with-param name="element_name">reservationControlInformation</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="EQN">
                            <xsl:with-param name="element_name">numberOfUnits</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="RPI">
                        	<xsl:with-param name="element_name">relatedProductInfo</xsl:with-param>
                        </xsl:call-template>

                        <xsl:call-template name="FTI">
                        	<xsl:with-param name="element_name">frequentTravellerInformation</xsl:with-param>
                       	</xsl:call-template>

                        <!--<xsl:call-template name="SDT"/>-->

                        <!--<xsl:call-template name="SSR"/>-->

                        <xsl:for-each select="GROUP_2">
                            <travellerInformation>
                                <xsl:call-template name="TIF">
                                    <xsl:with-param name="complex">true</xsl:with-param>
                                    <xsl:with-param name="element_name"/>
                                </xsl:call-template>

                                <!--<xsl:call-template name="FTI"/>-->

                                <!--<xsl:call-template name="SSR"/>-->

                            </travellerInformation>
                        </xsl:for-each>

                        <xsl:for-each select="GROUP_3">
                            <originDestinationInfo>
                                <xsl:call-template name="ODI">
                                    <xsl:with-param name="complex">true</xsl:with-param>
                                    <xsl:with-param name="element_name"/>
                                </xsl:call-template>

                                <!--<xsl:call-template name="MSG"/>-->

                                <xsl:for-each select="GROUP_4">
                                    <common:flightInfomation>
                                        <xsl:call-template name="TVL">
                                            <xsl:with-param name="complex">true</xsl:with-param>
                                        	<xsl:with-param name="element_name"/>
                                        </xsl:call-template>

                                        <!--<xsl:call-template name="APD"/>-->

                                        <!--<xsl:call-template name="RPI"/>-->

                                        <!--<xsl:call-template name="RCI"/>-->

                                        <!--<xsl:call-template name="EQN"/>-->

                                        <!--<xsl:call-template name="FTI"/>-->

                                        <!--<xsl:call-template name="PDI"/>-->

                                        <!--<xsl:call-template name="SDT"/>-->

                                        <!--<xsl:call-template name="SSR"/>-->

                                        <!-- GROUP_5 -->
                                        <!-- GROUP_6 -->
                                    </common:flightInfomation>
                                </xsl:for-each>

                            </originDestinationInfo>
                        </xsl:for-each>

                    </message>
                </xsl:for-each>
            </ns1:AA_ITAREQ>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>