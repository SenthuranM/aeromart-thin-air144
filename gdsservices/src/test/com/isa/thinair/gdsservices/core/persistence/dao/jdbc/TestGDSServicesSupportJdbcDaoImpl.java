package com.isa.thinair.gdsservices.core.persistence.dao.jdbc;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSServicesSupportJDBCDAO;
import com.isa.thinair.gdsservices.core.util.GDSTestCase;

public class TestGDSServicesSupportJdbcDaoImpl extends GDSTestCase {

	public void testGetPNRNo() throws ModuleException{
		String originatorPnr = "10000023";
		
		String pnr = getGDSServicesSupportDAO().getPnr(originatorPnr );
		
		assertNotNull(pnr, "PNR cannot be null");
		System.out.println("PNR:" + pnr);
	}
	
	
	private GDSServicesSupportJDBCDAO getGDSServicesSupportDAO(){
        return GDSServicesDAOUtils.DAOInstance.GDSSERVICES_SUPPORT_JDBC_DAO;
    }
}
