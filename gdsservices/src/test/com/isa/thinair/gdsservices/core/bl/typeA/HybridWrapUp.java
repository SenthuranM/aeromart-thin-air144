package com.isa.thinair.gdsservices.core.bl.typeA;

public class HybridWrapUp extends BaseTestUtil {

	public static void main(String[] args) {
		HybridWrapUp hybridWrapUp = new HybridWrapUp();
		hybridWrapUp.sendMessage();
	}

	@Override
	protected String getEDIMessage() {
		StringBuilder sb = new StringBuilder();
		sb.append("UNA:+.? '");
		sb.append("UNB+IATA:1+SR+DL+890701:0830+841F60'");
		sb.append("UNH+1+HWPREQ:96:2:IA+12345678'");
		sb.append("MSG+:56'");
		sb.append("MSG+:64'");
		sb.append("ORG+1A:ATL+12345678:0GK5+++T++BR'");
		sb.append("RCI+1A:ABCDEF'");
		sb.append("LTS+%");
		sb.append("HDQRMAA%");
		sb.append(".QTSRM1A 150208%");
		sb.append("PDM%");
		sb.append("MUC1ARLOC12/150208%");
		sb.append("1HENDERSON/HMR%");
		sb.append("AA100F28MARARLJFKSS1/09401230%");
		sb.append("OSIAA CTCT ATL7707153384%;'");
		sb.append("UNT+7+1'");
		return sb.toString();
	}
}