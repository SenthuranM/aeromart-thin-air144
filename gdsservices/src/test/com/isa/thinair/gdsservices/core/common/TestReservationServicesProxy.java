package com.isa.thinair.gdsservices.core.common;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.gdsservices.api.dto.internal.AddSegmentRequest;
import com.isa.thinair.gdsservices.api.dto.internal.Adult;
import com.isa.thinair.gdsservices.api.dto.internal.ConfirmBookingRequest;
import com.isa.thinair.gdsservices.api.dto.internal.ContactDetail;
import com.isa.thinair.gdsservices.api.dto.internal.CreateReservationRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.LocateReservationRequest;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.internal.RecordLocator;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.dto.internal.SegmentDetail;
import com.isa.thinair.gdsservices.api.dto.internal.SingleSegment;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.command.AddSegmentAction;
import com.isa.thinair.gdsservices.core.command.ConfirmBookingAction;
import com.isa.thinair.gdsservices.core.command.CreateReservationAction;
import com.isa.thinair.gdsservices.core.command.LocateReservationAction;
import com.isa.thinair.gdsservices.core.util.GDSTestCase;

public class TestReservationServicesProxy extends GDSTestCase {

	private static Log log = LogFactory.getLog(TestReservationServicesProxy.class);

	public void ntestProcessRequest() {
		//fail("Not yet implemented"); // TODO
	}

	public void ntestCancelSegmentAction() {
		//fail("Not yet implemented"); // TODO
	}

	public void testConfirmBookingAction() {
		try {
			ConfirmBookingRequest request = new ConfirmBookingRequest();
			GDSCredentialsDTO gdsCredentialsDTO = new GDSCredentialsDTO();
			gdsCredentialsDTO.setAgentCode("GSA1");
			gdsCredentialsDTO.setAgentStation("BGW");
			gdsCredentialsDTO.setUserId("SYSTEM");
			gdsCredentialsDTO.setSalesChannelCode(3);
			gdsCredentialsDTO.setDefaultCarrierCode("G9");

			request.setGdsCredentialsDTO(gdsCredentialsDTO);

			RecordLocator originatorRecordLocator = new RecordLocator();

			originatorRecordLocator.setPnrReference("LDQRVG22");
			request.setOriginatorRecordLocator(originatorRecordLocator);

			request.setTicketNo("F123456");

			request = ConfirmBookingAction.processRequest(request);
			if (request.isSuccess()) {
				log.info("Reservation Test Success :: " + request.getResponseMessage());
			} else {
				log.info("Reservation Test Fail    :: " + request.getResponseMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void testAddSegmentAction() {
		try {
			AddSegmentRequest addSegmentRequest = new AddSegmentRequest();
			GDSCredentialsDTO gdsCredentialsDTO = new GDSCredentialsDTO();
			gdsCredentialsDTO.setAgentCode("GSA1");
			gdsCredentialsDTO.setAgentStation("BGW");
			gdsCredentialsDTO.setUserId("SYSTEM");
			gdsCredentialsDTO.setSalesChannelCode(3);
			gdsCredentialsDTO.setDefaultCarrierCode("G9");

			addSegmentRequest.setGdsCredentialsDTO(gdsCredentialsDTO);

			RecordLocator originatorRecordLocator = new RecordLocator();

			originatorRecordLocator.setPnrReference("JJYPPO");
			addSegmentRequest.setOriginatorRecordLocator(originatorRecordLocator);

			List<SegmentDetail> segmentCollection = new ArrayList<SegmentDetail>();
			Segment seg = new Segment();
			seg.setDepartureStation("BGW");
			seg.setArrivalStation("DXB");

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

			//cal.add(Calendar.DATE, 3);
			seg.setDepartureDate(dateFormat.parse("01/08/2008"));
			seg.setDepartureDayOffset(0);
			SingleSegment simpleSeg = new SingleSegment();
			simpleSeg.setSegment(seg);
			segmentCollection.add(simpleSeg);

			addSegmentRequest.setSegmentDetails(segmentCollection);
			addSegmentRequest.addReservationAction(GDSInternalCodes.ReservationAction.ADD_SEGMENT);

			addSegmentRequest = AddSegmentAction.processRequest(addSegmentRequest);
			if (addSegmentRequest.isSuccess()) {
				log.info("Reservation Test Success :: " + addSegmentRequest.getResponseMessage());
				log.info("Reservation PNR          :: " + addSegmentRequest.getResponderAddress());
			} else {
				log.info("Reservation Test Fail    :: " + addSegmentRequest.getResponseMessage());
				log.info("Reservation PNR          :: " + addSegmentRequest.getResponderAddress());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void ntestCreateReservationAction() {
		try {
			CreateReservationRequest reservationRequest = new CreateReservationRequest();

			GDSCredentialsDTO gdsCredentialsDTO = new GDSCredentialsDTO();
			gdsCredentialsDTO.setAgentCode("GSA1");
			gdsCredentialsDTO.setAgentStation("BGW");
			gdsCredentialsDTO.setUserId("SYSTEM");
			gdsCredentialsDTO.setSalesChannelCode(3);
			gdsCredentialsDTO.setDefaultCarrierCode("G9");

			reservationRequest.setGdsCredentialsDTO(gdsCredentialsDTO);

			RecordLocator originatorRecordLocator = new RecordLocator();

			originatorRecordLocator.setPnrReference("a10000");
			reservationRequest.setOriginatorRecordLocator(originatorRecordLocator);

			List<SegmentDetail> segmentCollection = new ArrayList<SegmentDetail>();
			Segment seg = new Segment();
			seg.setDepartureStation("BGW");
			seg.setArrivalStation("DXB");

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

			//cal.add(Calendar.DATE, 3);
			seg.setDepartureDate(dateFormat.parse("01/08/2008"));
			seg.setDepartureDayOffset(0);
			SingleSegment simpleSeg = new SingleSegment();
			simpleSeg.setSegment(seg);
			segmentCollection.add(simpleSeg);

			Adult adt = new Adult();
			adt.setFirstName("Dhanushka");
			adt.setLastName("Ranatunga");
			adt.setTitle("MR");
			List<Passenger> passengers = new ArrayList<Passenger>();
			passengers.add(adt);
			reservationRequest.setPassengers(passengers);

			ContactDetail contact = new ContactDetail();
			contact.setTitle("MR");
			contact.setFirstName("Dhanu");
			contact.setLastName("Rana");
			contact.setStreetAddress("435/44, Nungamugoda");
			contact.setCity("Kelaniya");
			contact.setCountry("LK");
			contact.setMobilePhoneNumber("094716808300");
			contact.setLandPhoneNumber("094112917777");
			contact.setFaxNumber("0941234567");
			contact.setEmailAddress("dnranatunga@yahoo.com");

			reservationRequest.setNewSegmentDetails(segmentCollection);
			reservationRequest.setContactDetail(contact);

			reservationRequest.addReservationAction(GDSInternalCodes.ReservationAction.CREATE_BOOKING);

			reservationRequest = CreateReservationAction.processRequest(reservationRequest);
			if (reservationRequest.isSuccess()) {
				log.info("Reservation Test Success :: " + reservationRequest.getResponseMessage());
				log.info("Reservation PNR          :: " + reservationRequest.getResponderAddress());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void ntestLocateReservationActionAction() {
		try {
			LocateReservationRequest locateResRequest = new LocateReservationRequest();

			GDSCredentialsDTO gdsCredentialsDTO = new GDSCredentialsDTO();
			gdsCredentialsDTO.setAgentCode("GSA1");
			gdsCredentialsDTO.setAgentStation("BGW");
			gdsCredentialsDTO.setUserId("SYSTEM");
			gdsCredentialsDTO.setSalesChannelCode(3);
			gdsCredentialsDTO.setDefaultCarrierCode("G9");

			locateResRequest.setGdsCredentialsDTO(gdsCredentialsDTO);

			RecordLocator originatorRecordLocator = new RecordLocator();

			originatorRecordLocator.setPnrReference("10000163");
			locateResRequest.setOriginatorRecordLocator(originatorRecordLocator);

			locateResRequest.addReservationAction(GDSInternalCodes.ReservationAction.LOCATE_RESERVATION);

			locateResRequest = LocateReservationAction.processRequest(locateResRequest);
			if (locateResRequest.isSuccess()) {
				log.info("Reservation Test Success :: " + locateResRequest.getResponseMessage());
				log.info("Reservation PNR          :: " + locateResRequest.getResponderAddress());
				log.info(locateResRequest.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void ntestCancelReservationAction() {
		//		try{
		//			CancelReservationRequest request = new CancelReservationRequest();
		//		
		//			GDSCredentialsDTO gdsCredentialsDTO = new GDSCredentialsDTO();
		//			gdsCredentialsDTO.setAgentCode("GSA1");
		//			gdsCredentialsDTO.setAgentStation("BGW");
		//			gdsCredentialsDTO.setUserId("SYSTEM");
		//			gdsCredentialsDTO.setSalesChannelCode(3);
		//			gdsCredentialsDTO.setDefaultCarrierCode("G9");
		//		
		//			request.setGdsCredentialsDTO(gdsCredentialsDTO);
		//		
		//			RecordLocator originatorRecordLocator = new RecordLocator();
		//			originatorRecordLocator.setPnrReference("a10000d");
		//			request.setOriginatorRecordLocator(originatorRecordLocator);
		//		
		//			request.addReservationAction(GDSInternalCodes.ReservationAction.CANCEL_RESERVATION);
		//			request = CancelReservationAction.processRequest(request);
		//		if(request.isSuccess()){
		//			log.info("Reservation Test Success :: "+request.getResponseMessage());	
		//			log.info("Reservation PNR          :: "+request.getResponderAddress());
		//		}
		//		
		//		
		//		}catch(Exception e){e.printStackTrace();}
	}
}