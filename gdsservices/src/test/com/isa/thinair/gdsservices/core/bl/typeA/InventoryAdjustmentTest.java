package com.isa.thinair.gdsservices.core.bl.typeA;

public class InventoryAdjustmentTest extends BaseTestUtil {

	public static void main(String[] args) {
		InventoryAdjustmentTest inventoryAdjustmentTest = new InventoryAdjustmentTest();
		inventoryAdjustmentTest.sendMessage();
	}

	@Override
	protected String getEDIMessage() {
		StringBuilder sb = new StringBuilder();
		sb.append("UNA:+.? '");
		sb.append("UNB+IATA:2+SR+DL+890701:0830+841F60'");
		sb.append("UNH+1+ITAREQ:96:2:IA+10402'");
		sb.append("ORG+1A+NCE'");
		sb.append("ODI+SHJ+CMB'");
		sb.append("TVL+200610:2155:210610:0435+SHJ+CMB+G9+505:R2'");
		sb.append("RPI+1+XX'");
		sb.append("TVL+300610:0535:300610:0730+CMB+SHJ+G9+506:R2'");
		sb.append("RPI+1+XX'");
		sb.append("UNT+8+1'");
		return sb.toString();
	}
}