package com.isa.thinair.gdsservices.core.util;



import com.isa.thinair.login.util.ForceLoginInvoker;

public class CredentialInvokerUtil {

	public static void invokeCredentials() {
		// It is wrong to have username and password in module config.
		// This credentials should be passed by the client (Message Broker)
		// Since you guys need to test. No worries of hard coding in test classes.

		ForceLoginInvoker.login("SYSTEM", "password");
	}

}
