package com.isa.thinair.gdsservices.core.bl.typeA;

public class TransferControlTest extends BaseTestUtil {

	public static void main(String[] args) {
		TransferControlTest transferControlTest = new TransferControlTest();
		transferControlTest.sendMessage();
	}

	@Override
	protected String getEDIMessage() {

		StringBuffer message = new StringBuffer();
		message.append("UNB+IATB:1+1AHET::HR+1PCET::G9+111021:1415+00DFIMQCNT0001+++O'");
		message.append("UNH+1+TKCUAC:03:1:IA+099421300004DE'");
		message.append("MSG+:107'");
		message.append("ORG+HR:MUC++++++ETS'");
		message.append("EQN+2:TD'");
		message.append("TIF+HAPPY:A+MISTER MR'");
		message.append("TAI+7906+0001AA/SU:B'");
		message.append("RCI+1A:3S7TDN:1'");
		message.append("MON+B:7561.00:USD+T:5501.08:EUR+E:5227.00:EUR+D:0.691300'");
		message.append("FOP+CC:3:5531.08:VI:4012129999992220:0812'");
		message.append("PTK+N::I::::NE++230911'");
		message.append("TXD++4.17:::YQ+66.39:::YR+203.52:::XT'");
		message.append("ODI+CMB+SHJ'");
		message.append("ORG+1A:MUC+23497902:043704+SHJ+HR+A+DE:EUR:EN+A0001AASU'");
		message.append("EQN+5:TF'");
		message.append("IFT+4:5+49610350130'");
		message.append("IFT+4:39+TBA+HR'");
		message.append("IFT+4:15:0+CMB G9 SHJ1250.00G9 CMB1250.00G9 SHJ1500.00LH CPH1780.65LH SHJ1780.65N+UC7561.30END ROE1.000000XT72.00YQ16.00OY50.33RA13.08DE23.52YK2.78UD5.5+4UA20.27ZO'");
		message.append("TKT+1692409420099:T:2'");
		message.append("CPN+1:I::::::1'");
		message.append("TVL+251111:2245+CMB+SHJ+G9+0506:N2+J+1'"); // in local time
		message.append("PTS++CIF'");
		message.append("RPI++OK'");
		message.append("EBD++40::W:K'");
		message.append("CPN+2:I::::::2'");
		message.append("TVL+011211:0005+SHJ+CMB+G9+0503:N2+J+2'");
		message.append("PTS++CIF'");
		message.append("RPI++OK'");
		message.append("EBD++40::W:K'");
		message.append("CPN+3:I::::::3'");
		message.append("TVL+051211:0635+CMB+SHJ+G9+0504:N2+J+3'");
		message.append("PTS++CIF'");
		message.append("RPI++OK'");
		message.append("EBD++40::W:K'");
		message.append("CPN+4:701::::::4'");
		message.append("TVL+101211:1335+SHJ+CPH+LH+824:Y+J+4'");
		message.append("PTS++Y77'");
		message.append("RPI++OK'");
		message.append("EBD++1::N'");
		message.append("FTI+LH:51001228'");
		message.append("TKT+1692409420100:T:2'");
		message.append("CPN+1:701::::::5'");
		message.append("TVL+201211:0905+CPH+SHJ+LH+823:Y+J+5'");
		message.append("PTS++Y77'");
		message.append("RPI++OK'");
		message.append("EBD++1::N'");
		message.append("FTI+LH:51001228'");
		message.append("UNT+47+1'");
		message.append("UNZ+1+00DFIMQCNT0001'");
		return message.toString();
	}

}
