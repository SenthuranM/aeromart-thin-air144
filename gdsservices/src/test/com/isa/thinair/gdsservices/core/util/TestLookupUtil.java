package com.isa.thinair.gdsservices.core.util;

import com.isa.thinair.commons.api.client.BDLookupUtil;
import com.isa.thinair.gdsservices.api.service.GDSServicesBD;
import com.isa.thinair.gdsservices.api.utils.GdsservicesConstants;

public class TestLookupUtil {
	public static GDSServicesBD getGDSServicesBD() {
		return (GDSServicesBD) BDLookupUtil.getInstance().getBDInstance(
				GdsservicesConstants.MODULE_NAME,
				GdsservicesConstants.BDKeys.GDSSERVICES_GDSPUBLISHING);
	}
	
}
