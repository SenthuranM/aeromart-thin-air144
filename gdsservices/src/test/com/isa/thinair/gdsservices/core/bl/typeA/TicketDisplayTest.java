package com.isa.thinair.gdsservices.core.bl.typeA;

public class TicketDisplayTest extends BaseTestUtil {

	public static void main(String args[]) {
		TicketDisplayTest ticketDisplay = new TicketDisplayTest();
		ticketDisplay.sendMessage();
	}

	@Override
	protected String getEDIMessage() {
		StringBuffer message = new StringBuffer();
		message.append("UNB+IATB:1+1PETS::PS+1AETH::HR+111024:1247+20440029390001+++O'");
		message.append("UNH+1+TKCREQ:03:1:IA+09D0F10900284E'");
		message.append("MSG+:142'");
		message.append("ORG+PS:HDQ+00000000:CRC+++A++SW'");
		message.append("EQN+1:TD'");
		message.append("TKT+1692409430037:T:1'");
		message.append("CPN+1:B'");
		message.append("UNT+7+1'");
		message.append("UNZ+1+20440029390001'");
		return message.toString();
	}
}
