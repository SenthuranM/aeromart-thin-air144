package com.isa.thinair.gdsservices.core.util;

import java.util.Date;

import com.isa.thinair.commons.core.util.CalendarUtil;



public class TestCalanderUtil {
	public static void main(String[] args) {
		testAddDateTime();
		testTruncateTime();
	}

	private static void testAddDateTime() {
		Date d = CalendarUtil.addDateAndTime(new Date(), new Date());
		System.out.println(d);
	}
	
	private static void testTruncateTime() {
		System.out.println(CalendarUtil.truncateTime(new Date()));		
	}
}
