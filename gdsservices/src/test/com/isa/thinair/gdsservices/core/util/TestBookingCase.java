package com.isa.thinair.gdsservices.core.util;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestSuite;
import junit.textui.TestRunner;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.service.GDSServicesBD;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * Global Test Case for booking scenarios
 * @author Nilindra Fernando
 */
public class TestBookingCase extends GDSTestCase {
	
	public void testBookingProcess() {
		String sampleMessage = GDSMessageTemplates.getSampleBookingXML();
		XStream xstream = new XStream(new DomDriver()); 
		GDSServicesBD gdsServicesBD = TestLookupUtil.getGDSServicesBD();
		BookingRequestDTO bookingRequestDTO = (BookingRequestDTO) xstream.fromXML(sampleMessage);
		
		System.out.println("########### START REQUEST XML ###########");
		System.out.println(xstream.toXML(bookingRequestDTO));
		System.out.println("########### END REQUEST XML ###########");

		try {
			String uniqueAAId = bookingRequestDTO.getUniqueAccelAeroRequestId();
			Map<String, BookingRequestDTO> bookingRequestDTOMap = new HashMap<String, BookingRequestDTO>();
			bookingRequestDTOMap.put(uniqueAAId, bookingRequestDTO);
			bookingRequestDTOMap = gdsServicesBD.processRequests(bookingRequestDTOMap);
			bookingRequestDTO = bookingRequestDTOMap.get(uniqueAAId);
			
			System.out.println("########### START RESPONSE XML ###########");
			System.out.println(xstream.toXML(bookingRequestDTO));
			System.out.println("########### END RESPONSE XML ###########");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		TestRunner.run(new TestSuite(TestBookingCase.class));
	}
}
