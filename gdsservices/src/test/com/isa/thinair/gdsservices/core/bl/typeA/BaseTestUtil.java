package com.isa.thinair.gdsservices.core.bl.typeA;

import java.util.Hashtable;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

public abstract class BaseTestUtil {

	private final static String PROVIDER_URL = "127.0.0.1:1199";
	private final static String QUEUE_NAME = "queue/isaGDSTypeAQueue";
	
	protected abstract String getEDIMessage();

	public void sendMessage() {
		try {
			Hashtable<String, String> props = new Hashtable<String, String>();
			props.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
			props.put(Context.PROVIDER_URL, PROVIDER_URL);
			props.put("java.naming.rmi.security.manager", "yes");
			props.put(Context.URL_PKG_PREFIXES, "org.jboss.naming");

			Context context = new InitialContext(props);

			ConnectionFactory cf = (ConnectionFactory) context.lookup("ConnectionFactory");
			Queue queue = (Queue) context.lookup(QUEUE_NAME);

			Connection myConn = cf.createConnection();
			Session mySess = myConn.createSession(false, Session.AUTO_ACKNOWLEDGE);

			MessageProducer myMsgProducer = mySess.createProducer(queue);

			TextMessage myTextMsg = mySess.createTextMessage();
			myTextMsg.setText(getEDIMessage());
			System.out.println("Sending Message: \n" + myTextMsg.getText().replace("'", "'\n"));
			myMsgProducer.send(myTextMsg);

			myConn.start();
			mySess.close();
			myConn.close();

		} catch (Exception jmse) {
			System.out.println("Exception occurred : " + jmse.toString());
			jmse.printStackTrace();
		}
	}
}