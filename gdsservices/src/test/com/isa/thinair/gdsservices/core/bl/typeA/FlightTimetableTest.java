package com.isa.thinair.gdsservices.core.bl.typeA;

public class FlightTimetableTest extends BaseTestUtil {

	public static void main(String[] args) {
		FlightTimetableTest flightTimetableTest = new FlightTimetableTest();
		flightTimetableTest.sendMessage();
	}

	@Override
	protected String getEDIMessage() {
		String ediMessage = "UNA:+.? 'UNB+IATA:1+SR+DL+890701:0830+841F60'UNH+1+PAOREQ:96:2:IA'MSG+:51'ORG+1A+NCE'ODI'TVL++SHJ+CMB+G9'APD+:::::280510310510'UNT+7+1'";
		return ediMessage;
	}
}