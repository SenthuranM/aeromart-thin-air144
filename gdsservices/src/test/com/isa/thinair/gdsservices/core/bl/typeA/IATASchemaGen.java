package com.isa.thinair.gdsservices.core.bl.typeA;

import java.io.File;

import javax.xml.transform.stream.StreamResult;

import com.ddtek.xmlconverter.ConverterFactory;
import com.ddtek.xmlconverter.SchemaGenerator;
import com.ddtek.xmlconverter.XHTMLGenerator;
import com.ddtek.xmlconverter.exception.ConverterException;

public class IATASchemaGen {

	private static final String VERSION = "03.1";
	private static final String SCHEMA_BASE_PATH = "./resources/schema";
	private static final String HTML_BASE_PATH = "./resources/html";

	private static final String EDI_URL = "EDI:dialect=IATA:doc=yes:tbl=yes:version=" + VERSION + ":message=";

	enum MessageTypes {
		TKCREQ, TKCRES, TKCUAC, PAOREQ, PAORES, ITAREQ, ITARES
	}

	public static void main(String[] args) {
		try {
			ConverterFactory factory = new ConverterFactory();
			for (MessageTypes messageTypes : MessageTypes.values()) {

				String messageUri = EDI_URL + messageTypes.toString();

				// schema generation
				SchemaGenerator schemaGenerator = factory.newSchemaGenerator(messageUri);
				String filePath = SCHEMA_BASE_PATH + "/" + messageTypes + "-" + VERSION + ".xsd";
				System.out.println("Generation xml schema for " + messageTypes + " in " + filePath);
				StreamResult sr = new StreamResult(new File(filePath));
				schemaGenerator.getSchema(sr);

				// html generation
				XHTMLGenerator newXHTMLGenerator = factory.newXHTMLGenerator(messageUri);
				filePath = HTML_BASE_PATH + "/" + messageTypes + "-" + VERSION + ".html";
				System.out.println("Generation html for " + messageTypes + " in " + filePath);
				sr = new StreamResult(new File(filePath));
				newXHTMLGenerator.getXHTML(sr);
			}

		} catch (ConverterException e) {
			e.printStackTrace();
		}
	}

}
