package com.isa.thinair.gdsservices.core.util;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;

public class GDSMessageTemplates {
	
	private static String getHeaders() {
		BookingRequestDTO bookingRequestDTO = new BookingRequestDTO();
		bookingRequestDTO.setGds(GDSExternalCodes.GDS.SABRE);
		
		StringBuilder xml = new StringBuilder();
		xml.append("  <uniqueAccelAeroRequestId>" + bookingRequestDTO.getUniqueAccelAeroRequestId() + "</uniqueAccelAeroRequestId>	 ");
		xml.append("  <gds>" + bookingRequestDTO.getGds() + "</gds>							");
		xml.append("  <messageReceivedDateStamp>2008-08-01 10:10:20.0 IST</messageReceivedDateStamp>	");
		
		return xml.toString();
	}
	
	public static String getSampleBookingXML() {
		StringBuilder xml = new StringBuilder();
		xml.append(" <com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO> 		");
		xml.append("  <messageIdentifier>BKG</messageIdentifier>							");
		xml.append("  <originatorAddress>HDQBB1S</originatorAddress>						");
		xml.append(getHeaders());
		xml.append("  <originatorRecordLocator>												");
		xml.append("    <owner>SEN</owner>													");
		xml.append("    <bookingOffice>HDQ1S </bookingOffice>								");
		xml.append("    <pnrReference>JJYPPS</pnrReference>									");
		xml.append("    <taOfficeCode>PD5C</taOfficeCode>									");
		xml.append("    <userID>99999999</userID>											");
		xml.append("  </originatorRecordLocator>											");
		xml.append("  <communicationReference>310355</communicationReference>				");
		xml.append("  <changedNameDTOs/>													");
		xml.append("  <newNameDTOs>															");
		xml.append("    <com.isa.thinair.gdsservices.api.dto.external.NameDTO>				");
		xml.append("      <paxTitle>Mr</paxTitle>										");
		xml.append("      <lastName>HARRIES</lastName>										");
		xml.append("      <firstName>JHON</firstName>										");
		xml.append("      <groupMagnitude>0</groupMagnitude>								");
		xml.append("    </com.isa.thinair.gdsservices.api.dto.external.NameDTO>				");
		xml.append("  </newNameDTOs>														");
		xml.append("  <bookingSegmentDTOs>													");
		xml.append("    <com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO>	");
		xml.append("      <flightNumber>506</flightNumber>									");
		xml.append("      <departureDate>2008-08-26 00:00:00.0 IST</departureDate>			");
		xml.append("      <departureStation>SHJ</departureStation>							");
		xml.append("      <destinationStation>CMB</destinationStation>						");
		xml.append("      <bookingCode>Y</bookingCode>										");
		xml.append("      <departureTime>1970-01-01 00:22:00.0 IST</departureTime>			");
		xml.append("      <arrivalTime>1970-01-01 00:03:15.0 IST</arrivalTime>				");
		xml.append("      <actionOrStatusCode>NN</actionOrStatusCode>						");
		xml.append("      <noofPax>1</noofPax>												");
		xml.append("      <dayOffSet>1</dayOffSet>											");
		xml.append("      <carrierCode>G9</carrierCode>										");
		xml.append("    </com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO>	");
		xml.append("  </bookingSegmentDTOs>													");
		xml.append("  <ssrDTOs/>															");
		xml.append("  <osiDTOs>																");
		xml.append("    <com.isa.thinair.gdsservices.api.dto.external.OtherServiceInfoDTO>	");
		xml.append("      <codeOSI>CTCP</codeOSI>											");
		xml.append("      <carrierCode>G9</carrierCode>										");
		xml.append("      <osiValue>CTCP CMB4444</osiValue>									");
		xml.append("      <fullElement>OSI G9 CTCP CMB4444</fullElement>					");
		xml.append("      <serviceInfo>CMB4444</serviceInfo>								");
		xml.append("    </com.isa.thinair.gdsservices.api.dto.external.OtherServiceInfoDTO>	");
		xml.append("  </osiDTOs>															");
		xml.append("  <groupBooking>false</groupBooking>									");
		xml.append(" </com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO>		");
		
		return xml.toString();
	}
}
