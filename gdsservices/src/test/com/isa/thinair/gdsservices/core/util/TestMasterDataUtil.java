package com.isa.thinair.gdsservices.core.util;

import java.util.Collection;
import java.util.Iterator;

public class TestMasterDataUtil extends GDSTestCase{
	
	public void testGetTitles() {
		Collection<String> titles = MasterDataUtil.getTitles();
		
		for (Iterator<String> iterator = titles.iterator(); iterator.hasNext();) {
			String title = (String) iterator.next();
			
			System.out.println(title);
		}
	}
}
