package com.isa.thinair.gdsservices.core.util;

import com.isa.thinair.login.client.ClientLoginModule;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Nilindra Fernando
 */
public class GDSTestCase extends PlatformTestCase {
	@Override
	protected void setUp() throws Exception {
		System.setProperty("repository.home", "c:/isaconfig-test");
		
		try {
			new ClientLoginModule().login("SYSTEM","password", 
					"c:/isaconfig-test/client/login-config/client_jass_login.config", "JbossClientLogin");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
