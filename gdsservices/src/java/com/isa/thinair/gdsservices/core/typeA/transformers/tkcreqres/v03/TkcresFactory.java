package com.isa.thinair.gdsservices.core.typeA.transformers.tkcreqres.v03;

import static com.isa.thinair.gdsservices.core.util.IATADataExtractionUtil.setValue;
import iata.typea.v031.tkcres.MSG;
import iata.typea.v031.tkcres.ObjectFactory;
import iata.typea.v031.tkcres.Type0029;
import iata.typea.v031.tkcres.Type0051;
import iata.typea.v031.tkcres.Type0065;
import iata.typea.v031.tkcres.UNB;
import iata.typea.v031.tkcres.UNH;
import iata.typea.v031.tkcres.UNT;
import iata.typea.v031.tkcres.UNZ;

import java.util.Date;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.config.TypeAMessageConfig;
import com.isa.thinair.gdsservices.core.typeA.model.InterchangeHeader;
import com.isa.thinair.gdsservices.core.typeA.model.InterchangeInfo;
import com.isa.thinair.gdsservices.core.typeA.model.MessageHeader;
import com.isa.thinair.gdsservices.core.util.IATADataExtractionUtil;
import com.isa.thinair.gdsservices.core.util.RandomANGen;

/**
 * The singleton Tkcres factory class.
 * 
 * @author sanjaya
 * 
 */
public class TkcresFactory {

	private static final TkcresFactory factory = new TkcresFactory();
	private static final ObjectFactory objectFactory = new ObjectFactory();

	private TkcresFactory() {
		super();
	}

	/**
	 * @return The singleton instance of the {@link TkcresFactory}.
	 */
	public static final TkcresFactory getInstance() {
		return factory;
	}

	/**
	 * @throws ModuleException
	 */
	public UNB createUNB(InterchangeHeader interchangeHeader, String interchangeControllRef) throws InteractiveEDIException {

		TypeAMessageConfig typeAMessageConfig = GDSServicesModuleUtil.getConfig().getTypeAMessageConfig();

		UNB unb = objectFactory.createUNB();
		// syntax id and version

		setValue(objectFactory, unb, typeAMessageConfig.getSyntaxIdentifier(), 1, 1);
		setValue(objectFactory, unb, typeAMessageConfig.getSyntaxVersion(), 1, 2);

		InterchangeInfo replySender = interchangeHeader.getRecipient();
		InterchangeInfo recipient = interchangeHeader.getSender();

		// sender info
		setValue(objectFactory, unb, replySender.getInterchangeId(), 2, 1);
		setValue(objectFactory, unb, replySender.getInterchangeCode(), 2, 3);

		// recipient info
		setValue(objectFactory, unb, recipient.getInterchangeId(), 3, 1);
		setValue(objectFactory, unb, recipient.getInterchangeCode(), 3, 3);

		// This date and time need to be given in GMT
		String[] dateAsString = IATADataExtractionUtil.getDateAsString(new Date(), true);
		setValue(objectFactory, unb, dateAsString[0], 4, 1);
		setValue(objectFactory, unb, dateAsString[1], 4, 2);

		// interchange controll ref of sender

		setValue(objectFactory, unb, interchangeControllRef, 5);

		// recipient ref
		setValue(objectFactory, unb, interchangeHeader.getRecipientRefAssociationId(), 6, 1);
		setValue(objectFactory, unb, interchangeHeader.getRecipientRefSequenceNo(), 6, 2);

		// set association codeunb
		setValue(objectFactory, unb, Type0029.T, 8);

		return unb;
	}

	/**
	 * @return : The {@link UNH}
	 * @throws ModuleException
	 */
	public UNH createUNH(MessageHeader messageHeader) throws InteractiveEDIException {

		IATAOperation messageType = messageHeader.getMessageIdentifier().getMessageType().getResponse();
		TypeAMessageConfig typeAMessageConfig = GDSServicesModuleUtil.getConfig().getTypeAMessageConfig();

		UNH unh = objectFactory.createUNH();

		// Message ref number . Allways 1
		setValue(objectFactory, unh, String.valueOf(1), 1);

		// Message type and version
		setValue(objectFactory, unh, Type0065.valueOf(messageType.toString()), 2, 1);
		setValue(objectFactory, unh, typeAMessageConfig.getMessageVersoins(messageType.toString()), 2, 2);
		setValue(objectFactory, unh, typeAMessageConfig.getMessageRelease(messageType.toString()), 2, 3);
		setValue(objectFactory, unh, Type0051.IA, 2, 4);

		// common access reference
		String carRemote = messageHeader.getCommonAccessReference();
		String carOwn = new RandomANGen(14).nextString("");
		String car = carRemote.concat("/").concat(carOwn);
		setValue(objectFactory, unh, car, 3);

		return unh;
	}

	/**
	 * @throws ModuleException
	 */
	public MSG createMSG(String functionCode) throws InteractiveEDIException {
		MSG msg = objectFactory.createMSG();

		setValue(objectFactory, msg, functionCode, 1, 2);
		setValue(objectFactory, msg, String.valueOf(3), 2); // FIXME make it programeticle

		return msg;
	}

	/**
	 * @throws ModuleException
	 */
	public UNT createUNT(MessageHeader messageHeader) throws InteractiveEDIException {
		UNT unt = objectFactory.createUNT();
		setValue(objectFactory, unt, messageHeader.getMessageReferenceNumber(), 2);
		return unt;
	}

	@Deprecated
	public UNZ createUNZ(InterchangeHeader interchangeHeader, String interchangeControllRef) throws InteractiveEDIException {
		UNZ unz = objectFactory.createUNZ();
		setValue(objectFactory, unz, String.valueOf(1), 1);
		setValue(objectFactory, unz, interchangeControllRef, 2);
		return unz;
	}

	public ObjectFactory getObjectFactory() {
		return objectFactory;
	}

}
