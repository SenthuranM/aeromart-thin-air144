package com.isa.thinair.gdsservices.core.bl.old.typeA;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSTypeAServicesSupportJDBCDAO;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * This command is responsible for consume the inventory (block seat) / creating a dummy reservation if PAX detail
 * existing.
 * 
 * @author mekanayake
 * 
 * @isa.module.command name="createPassengerRecord.old"
 */
public class CreatePassengerRecord extends DefaultBaseCommand {

	private GDSTypeAServicesSupportJDBCDAO gdsTypeAServicesJDBCDAO = null;

	public CreatePassengerRecord() {
		gdsTypeAServicesJDBCDAO = GDSServicesDAOUtils.DAOInstance.GDS_TypeA_JDBC_DAO;

	}

	@Override
	public ServiceResponce execute() throws ModuleException {
		Object parameter = this.getParameter("");

		// constructing and return command response
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		return responce;
	}
}
