package com.isa.thinair.gdsservices.core.typeA.transformers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientBalancePayment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegmentETicket;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.TypeANotes;
import com.isa.typea.common.FormOfPayment;
import com.isa.typea.common.FormOfPaymentIndicator;
import com.isa.typea.common.MonetaryInformation;
import com.isa.typea.common.TravellerTicketingInformation;
import com.isa.typea.tktreq.TKTREQMessage;

public class ReservationDTOsTransformer {

	public static LCCClientBalancePayment getLCCClientBalancePayment(TKTREQMessage message, String pnr,
	                                                                 LCCClientReservation reservation) throws GdsTypeAException {
		LCCClientPaymentAssembler paymentAssembler;
		PayCurrencyDTO payCurrencyDTO;
		LCCClientReservationPax reservationPax;
		List<FormOfPayment> formsOfPayment;

		Date exchangeRateByDate = CalendarUtil.getCurrentSystemTimeInZulu();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(exchangeRateByDate);

		LCCClientBalancePayment balancePayment = new LCCClientBalancePayment();
		balancePayment.setGroupPNR(pnr);
		balancePayment.setOtherCarrierPaxCreditUser(false);
		String paymentAmount = null;
		String totalPaymentAmount = null;
		String additionalPaymentAmount = null;
		String selectedCurrencyCode = null;

		// TODO -- externalChargesDto map
		Map externalChargesDtos = null;
		balancePayment.setExternalChargesMap(externalChargesDtos);

		for (TravellerTicketingInformation ticketingInfo : message.getTravellerTicketingInformation()) {
			paymentAssembler = new LCCClientPaymentAssembler();
			// only one pax per Traveller-Information element
			if (ticketingInfo.getTravellers().size() > 1) {
				// TODO -- exceptions define
				throw new GdsTypeAException();
			}

			reservationPax = GDSDTOUtil.resolveReservationPax(ticketingInfo,
					ticketingInfo.getTravellers().get(0), reservation);

			formsOfPayment = ticketingInfo.getFormOfPayment();
			for (FormOfPayment formOfPayment : formsOfPayment) {
				if (formOfPayment.getFormOfPaymentType() == FormOfPaymentIndicator.CC) {
					// TODO -- CC payments details save 'em as remarks
				}
			}


			for (MonetaryInformation monetaryInfo : ticketingInfo.getMonetaryInformation()) {
				selectedCurrencyCode = monetaryInfo.getCurrencyCode();

				if (monetaryInfo.getAmountTypeQualifier().equals(TypeAConstants.MonetaryType.TICKET_TOTAL)) {

					if (monetaryInfo.getAmount().endsWith(TypeAConstants.TypeAKeywords.ADDITION_COLLECTION_POSTFIX)) {
						additionalPaymentAmount = monetaryInfo.getAmount()
								.substring(0, monetaryInfo.getAmount().length() - TypeAConstants.TypeAKeywords.ADDITION_COLLECTION_POSTFIX.length());
					} else {
						totalPaymentAmount = monetaryInfo.getAmount();
					}
				} else if (monetaryInfo.getAmountTypeQualifier().equals(TypeAConstants.MonetaryType.ADDITIONAL_COLLECTION_TOTAL)) {
					additionalPaymentAmount = monetaryInfo.getAmount();
				}
			}

			if (totalPaymentAmount != null) {
				paymentAmount = totalPaymentAmount;
			} else if (additionalPaymentAmount != null) {
				paymentAmount = additionalPaymentAmount;
			}

			if (paymentAmount != null) {
				CurrencyExchangeRate currExRate;
				try {
					currExRate = exchangeRateProxy.getCurrencyExchangeRate(selectedCurrencyCode);
				} catch (ModuleException e) {
					throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.UNABLE_TO_CONVERT_TO_TICKETING_CURRENCY, TypeANotes.ErrorMessages.NO_MESSAGE);
				}

				payCurrencyDTO = new PayCurrencyDTO(currExRate.getCurrency().getCurrencyCode(),
						currExRate.getMultiplyingExchangeRate(), currExRate.getCurrency().getBoundryValue(), currExRate.getCurrency()
						.getBreakPoint());

				paymentAssembler.addCashPayment(new BigDecimal(paymentAmount), payCurrencyDTO, exchangeRateProxy.getExecutionDate(),
						null, null, null, null);

				balancePayment.addPassengerPayments(reservationPax.getPaxSequence(), paymentAssembler);
			}
		}

		return balancePayment;


	}

	public static List<EticketTO> toETicketTos(List<LccClientPassengerEticketInfoTO> coupons) {
		ArrayList<EticketTO> eticketTOs = new ArrayList<EticketTO>();
		EticketTO eticketTO;

		for (LccClientPassengerEticketInfoTO coupon : coupons) {
			eticketTO = new EticketTO();
			eticketTO.setPnrPaxFareSegId(coupon.getPpfsId());
			eticketTO.setEticketId(coupon.getEticketId());
			eticketTO.setEticketNumber(coupon.getPaxETicketNo());
			eticketTO.setCouponNo(coupon.getCouponNo());
			eticketTO.setExternalEticketNumber(coupon.getExternalPaxETicketNo());
			eticketTO.setExternalCouponNo(coupon.getExternalCouponNo());
			eticketTO.setExternalCouponStatus(coupon.getExternalCouponStatus());
			eticketTO.setExternalCouponControl(ReservationPaxFareSegmentETicket.ExternalCouponControl.VALIDATING_CARRIER);

			eticketTOs.add(eticketTO);
		}

		return eticketTOs;
	}
}
