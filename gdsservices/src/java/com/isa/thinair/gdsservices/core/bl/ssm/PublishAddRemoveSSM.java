/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.core.bl.ssm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.NotifyFlightEventRQ;
import com.isa.thinair.airinventory.api.dto.NotifyFlightEventsRQ;
import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.SSMSplitFlightSchedule;
import com.isa.thinair.airschedules.api.utils.FlightEventCode;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.airschedules.api.utils.GDSScheduleEventCollector;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.XMLStreamer;
import com.isa.thinair.gdsservices.api.model.GDSPublishMessage;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSServicesDAO;
import com.isa.thinair.msgbroker.api.dto.SSIMessegeDTO;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for integrate create schedule with gds
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="publishAddRemoveSSM"
 */
public class PublishAddRemoveSSM extends DefaultBaseCommand {

	private BookingClassBD bookingClassBD;

	private FlightInventoryBD flightInventoryBD;

	// Dao's
	private GDSServicesDAO gdsServiceDao;

	/**
	 * constructor of the publishAddRemoveSSM command
	 */
	public PublishAddRemoveSSM() {

		// looking up BDs
		bookingClassBD = GDSServicesModuleUtil.getBookingClassBD();

		flightInventoryBD = GDSServicesModuleUtil.getFlightInventoryBD();

		// lokking up DAOs
		gdsServiceDao = GDSServicesDAOUtils.DAOInstance.GDSSERVICES_DAO;
	}

	/**
	 * execute method of the PublishUpdateASM command
	 * 
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce execute() throws ModuleException {

		GDSScheduleEventCollector sGdsEvents = (GDSScheduleEventCollector) this
				.getParameter(GDSSchedConstants.ParamNames.GDS_EVENT_COLLECTOR);
		boolean isExternalSSMRecap = sGdsEvents.isExternalSSMRecap();

		HashMap flightIdMap = (HashMap) this.getParameter(GDSSchedConstants.ParamNames.FLIGHT_IDS_MAP_FOR_SCHEDULES);

		Map aircraftTypeMap = GDSServicesModuleUtil.getGlobalConfig().getIataAircraftCodes();

		String airLineCode = GDSServicesModuleUtil.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
		// String airLineCode = (String) this.getParameter(GDSSchedConstants.ParamNames.CARRIER_CODE);
		Collection flightIdsList = (Collection) flightIdMap.get(sGdsEvents.getUpdatedSchedule().getScheduleId());

		if (!isExternalSSMRecap) {

			Map gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
			Collection addedList = (sGdsEvents.getAddedGDSList() != null) ? sGdsEvents.getAddedGDSList() : new ArrayList();
			Collection deletedList = (sGdsEvents.getDeletedGDSList() != null) ? sGdsEvents.getDeletedGDSList() : new ArrayList();

			Collection gdsIdsForRbd = new ArrayList();
			gdsIdsForRbd.addAll(addedList);
			gdsIdsForRbd.addAll(deletedList);

			Map bcMap = bookingClassBD.getBookingClassCodes(gdsIdsForRbd);

			if (sGdsEvents.containsAction(GDSScheduleEventCollector.GDS_DELETED)) {

				sendSSMCancel(gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(), sGdsEvents.getUpdatedSchedule(),
						airLineCode, deletedList, bcMap, flightIdsList, sGdsEvents.getSupplementaryInfo());
			}

			if (sGdsEvents.containsAction(GDSScheduleEventCollector.GDS_ADDED)) {

				sendSSMNew(gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(), sGdsEvents.getUpdatedSchedule(),
						airLineCode, addedList, bcMap, flightIdsList, sGdsEvents.getSupplementaryInfo());
			}
		} else {

			if (sGdsEvents.containsAction(GDSScheduleEventCollector.EXTERNAL_SSM_RECAP)) {
				String externalEmail = sGdsEvents.getExternalEmailAddress();
				String externalSita = sGdsEvents.getExternalSitaAddress();
				sendSSMExternal(aircraftTypeMap, sGdsEvents.getServiceType(), sGdsEvents.getUpdatedSchedule(), airLineCode,
						flightIdsList, externalEmail, externalSita);
			}
		}

		// constructing response
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		// return command response
		return responce;
	}

	private void sendSSM(String gdsCode, String airLineCode, SSIMessegeDTO newSSM, GDSPublishMessage pubMessage) {

		try {

			// ReservationToGDSMessageSenderBD gdsSender = CommonPublishUtil.getGDSSender();
			if (gdsCode != null) {
				GDSServicesModuleUtil.getSSMASMServiceBD().sendStandardScheduleMessage(gdsCode, airLineCode, newSSM);
			} else {
				GDSServicesModuleUtil.getSSMASMServiceBD().sendStandardScheduleMessageForExternalSystem(airLineCode, newSSM);
			}
			gdsServiceDao.saveGDSPublisingMessage(pubMessage);

		} catch (Exception e) {

			pubMessage.setRemarks("Message broker invocation failed");
			pubMessage.setStatus(GDSPublishMessage.MESSAGE_FAILED);
			gdsServiceDao.saveGDSPublisingMessage(pubMessage);
		}
	}

	private void nofigyInventory(FlightEventCode eventcode, Collection flightIds, Collection gdsIDs) throws ModuleException {

		if (flightIds != null && !flightIds.isEmpty()) {
			// notifiy flight events to inventory to send corresponding avs messages
			NotifyFlightEventsRQ notifyFlightEventsRQ = new NotifyFlightEventsRQ();
			NotifyFlightEventRQ newFlightEnvent = new NotifyFlightEventRQ();
			newFlightEnvent.setFlightEventCode(eventcode);
			newFlightEnvent.addFlightIds(flightIds);

			if (FlightEventCode.FLIGHT_CREATED.equals(eventcode)) {
				newFlightEnvent.setGdsIdsAdded(gdsIDs);
			} else {
				newFlightEnvent.setGdsIdsRemoved(gdsIDs);
			}

			notifyFlightEventsRQ.addNotifyFlightEventRQ(newFlightEnvent);
			flightInventoryBD.notifyFlightEvent(notifyFlightEventsRQ);
		}
	}

	private void sendSSMNew(Map gdsStatsMap, Map aircraftTypeMap, String serviceType, FlightSchedule schedulex,
			String airLineCode, Collection gdsList, Map bcMap, Collection flightIds, String supplementaryInfo)
			throws ModuleException {

		if (gdsList != null && gdsList.size() > 0) {
			ArrayList activeGdses = new ArrayList();
			Collection<SSMSplitFlightSchedule> scheduleColl = schedulex.getSsmSplitFlightSchedule();
			if (scheduleColl != null && !scheduleColl.isEmpty()) {
				for (SSMSplitFlightSchedule subSchedule : scheduleColl) {
					generateSSMNewMsg(gdsStatsMap, aircraftTypeMap, schedulex, airLineCode, gdsList, bcMap, subSchedule,
							activeGdses, supplementaryInfo);
				}
			} else {
				generateSSMNewMsg(gdsStatsMap, aircraftTypeMap, schedulex, airLineCode, gdsList, bcMap, null, activeGdses,
						supplementaryInfo);
			}

			if (activeGdses.size() > 0)
				nofigyInventory(FlightEventCode.FLIGHT_CREATED, flightIds, activeGdses);
		}
	}

	private void sendSSMCancel(Map gdsStatsMap, Map aircraftTypeMap, String serviceType, FlightSchedule schedulex,
			String airLineCode, Collection gdsList, Map bcMap, Collection flightIds, String supplementaryInfo)
			throws ModuleException {

		if (gdsList != null && gdsList.size() > 0) {
			ArrayList activeGdses = new ArrayList();
			Collection<SSMSplitFlightSchedule> scheduleColl = schedulex.getSsmSplitFlightSchedule();
			if (scheduleColl != null && !scheduleColl.isEmpty()) {
				for (SSMSplitFlightSchedule subSchedule : scheduleColl) {
					generateSSMCancelMsg(gdsStatsMap, aircraftTypeMap, schedulex, airLineCode, gdsList, bcMap, subSchedule,
							activeGdses, supplementaryInfo);
				}
			} else {
				generateSSMCancelMsg(gdsStatsMap, aircraftTypeMap, schedulex, airLineCode, gdsList, bcMap, null, activeGdses,
						supplementaryInfo);
			}

			if (activeGdses.size() > 0)
				nofigyInventory(FlightEventCode.FLIGHT_GDS_PUBLISHED_CHANGED, flightIds, activeGdses);
		}
	}

	private void generateSSMNewMsg(Map gdsStatsMap, Map aircraftTypeMap, FlightSchedule schedule, String airLineCode,
			Collection gdsList, Map bcMap, SSMSplitFlightSchedule subSchedule, ArrayList activeGdses, String supplementaryInfo)
			throws ModuleException {

		SSIMessegeDTO newSSM = new SSIMessegeDTO();
		SchedulePublishUtil.populateSSIMessageDTOforNEW(newSSM, schedule, null,
				(String) aircraftTypeMap.get(schedule.getModelNumber()), subSchedule, supplementaryInfo);

		Iterator itGdsIds = gdsList.iterator();
		if (activeGdses == null) {
			activeGdses = new ArrayList();
		}

		while (itGdsIds.hasNext()) {

			Integer gdsId = (Integer) itGdsIds.next();
			SchedulePublishUtil.addRBDInfo(newSSM, (Collection) bcMap.get(gdsId.toString()));
			GDSStatusTO gdsStatus = (GDSStatusTO) gdsStatsMap.get(gdsId.toString());

			GDSPublishMessage pubMessage = SchedulePublishUtil.createSSMSuccessPublishLog(gdsId, XMLStreamer.compose(newSSM),
					newSSM.getReferenceNumber(), schedule.getScheduleId());

			if (GDSInternalCodes.GDSStatus.ACT.getCode().equals(gdsStatus.getStatus())) {
				activeGdses.add(gdsId);
				sendSSM(gdsStatus.getGdsCode(), airLineCode, newSSM, pubMessage);
			} else {
				pubMessage.setStatus(GDSPublishMessage.GDS_INACTIVE);
				gdsServiceDao.saveGDSPublisingMessage(pubMessage);
			}
		}

	}

	private void generateSSMCancelMsg(Map gdsStatsMap, Map aircraftTypeMap, FlightSchedule schedule, String airLineCode,
			Collection gdsList, Map bcMap, SSMSplitFlightSchedule subSchedule, ArrayList activeGdses, String supplementaryInfo)
			throws ModuleException {

		SSIMessegeDTO newSSM = new SSIMessegeDTO();
		SchedulePublishUtil.populateSSIMessageDTOforCNL(newSSM, schedule, null,
				(String) aircraftTypeMap.get(schedule.getModelNumber()), subSchedule, supplementaryInfo);

		Iterator itGdsIds = gdsList.iterator();
		if (activeGdses == null) {
			activeGdses = new ArrayList();
		}

		while (itGdsIds.hasNext()) {
			Integer gdsId = (Integer) itGdsIds.next();
			SchedulePublishUtil.addRBDInfo(newSSM, (Collection) bcMap.get(gdsId.toString()));
			GDSStatusTO gdsStatus = (GDSStatusTO) gdsStatsMap.get(gdsId.toString());

			GDSPublishMessage pubMessage = SchedulePublishUtil.createSSMSuccessPublishLog(gdsId, XMLStreamer.compose(newSSM),
					newSSM.getReferenceNumber(), schedule.getScheduleId());

			if (GDSInternalCodes.GDSStatus.ACT.getCode().equals(gdsStatus.getStatus())) {
				activeGdses.add(gdsId);
				sendSSM(gdsStatus.getGdsCode(), airLineCode, newSSM, pubMessage);
			} else {
				pubMessage.setStatus(GDSPublishMessage.GDS_INACTIVE);
				gdsServiceDao.saveGDSPublisingMessage(pubMessage);
			}
		}

	}

	private void sendSSMExternal(Map aircraftTypeMap, String serviceType, FlightSchedule schedule, String airLineCode,
			Collection flightIds, String externalEmail, String externalSita) throws ModuleException {

		SSIMessegeDTO newSSM = new SSIMessegeDTO();
		SchedulePublishUtil.populateSSIMessageDTOforNEW(newSSM, schedule, serviceType,
				(String) aircraftTypeMap.get(schedule.getModelNumber()), null, null);

		newSSM.setPassengerResBookingDesignator(AppSysParamsUtil.getCommonBookingCodesForExternalSSMRecap());
		newSSM.setExternalEmailAddres(externalEmail);
		newSSM.setExternalSitaAddress(externalSita);

		GDSPublishMessage pubMessage = SchedulePublishUtil.createSSMSuccessPublishLog(null, XMLStreamer.compose(newSSM),
				newSSM.getReferenceNumber(), schedule.getScheduleId());
		sendSSM(null, airLineCode, newSSM, pubMessage);

	}
}
