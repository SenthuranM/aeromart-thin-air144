package com.isa.thinair.gdsservices.core.typeb.transformers;

import java.util.Map;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.ConfirmBookingRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ProcessStatus;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;

public class ConfirmBookingResponseTransformer {
	private static final ReservationAction RESERVATION_ACTION = ReservationAction.CONFIRM_BOOKING;

	public static BookingRequestDTO getResponse(BookingRequestDTO bookingRequestDTO,
			Map<ReservationAction, GDSReservationRequestBase> reservationResponseMap) {

		ConfirmBookingRequest confirmBookingRequest = (ConfirmBookingRequest) reservationResponseMap.get(RESERVATION_ACTION);

		if (confirmBookingRequest.isSuccess()) {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_SUCCESS);
			bookingRequestDTO = ResponseTransformerUtil.setSSRStatus(bookingRequestDTO);
			bookingRequestDTO = ValidatorUtils.addResponse(bookingRequestDTO, confirmBookingRequest);
		} else {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_FAILURE);
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, confirmBookingRequest);
		}

		return bookingRequestDTO;
	}

}
