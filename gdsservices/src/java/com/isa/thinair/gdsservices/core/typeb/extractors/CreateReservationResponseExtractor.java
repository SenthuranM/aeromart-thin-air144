package com.isa.thinair.gdsservices.core.typeb.extractors;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.CreateReservationResponse;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;

/**
 * @author Manoj Dhanushka
 */
public class CreateReservationResponseExtractor extends ValidatorBase {

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {
		this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
		return bookingRequestDTO;
	}
	
	public static CreateReservationResponse getRequest(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO) {
		CreateReservationResponse createReservationResponse = new CreateReservationResponse();
		createReservationResponse.setGdsCredentialsDTO(gdsCredentialsDTO);
		createReservationResponse = (CreateReservationResponse) RequestExtractorUtil.addBaseAttributes(createReservationResponse,
				bookingRequestDTO);
		return createReservationResponse;
	}

}
