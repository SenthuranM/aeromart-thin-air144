package com.isa.thinair.gdsservices.core.typeA.parser;

import java.util.HashSet;
import java.util.Set;

public interface TypeAParseConstants {

	interface Delimiter {
		String SEGMENT = "'";
		String ELEMENT = "+";
		String DATA = ":";
	}

	enum Segment {
		UNA,
		UNB,
		UNH,
		IFT_15,
		TIF,
		TVL;


		private Set<Segment> childSegments;

		private Segment(Segment ... childSegs) {
			childSegments = new HashSet<Segment>();

			for (Segment childSeg : childSegs) {
				childSegments.add(childSeg);
			}
		}
	}
}
