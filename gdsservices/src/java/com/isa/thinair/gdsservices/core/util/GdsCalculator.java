package com.isa.thinair.gdsservices.core.util;

import java.math.BigDecimal;

public class GdsCalculator {
	private GdsCalculator() { }

	public enum RoundingType {
		UP      (BigDecimal.ROUND_UP),
		DOWN    (BigDecimal.ROUND_DOWN),
		OFF     (BigDecimal.ROUND_DOWN);

		private int bigDecimalRoundingMode;

		RoundingType(int bigDecimalRoundingMode) {
			this.bigDecimalRoundingMode = bigDecimalRoundingMode;
		}

		public int getBigDecimalRoundingMode() {
			return bigDecimalRoundingMode;
		}
	}

	public static BigDecimal round(String amount, String roundingPattern) {
		return round(amount, roundingPattern, RoundingType.UP);
	}

	public static BigDecimal round(String originalValue, String roundingPattern, RoundingType roundingType) {
		BigDecimal rounded;

		BigDecimal value = new BigDecimal(originalValue);
		BigDecimal round = new BigDecimal(roundingPattern);

		Double scaleRaw = Math.log10(round.doubleValue());
		int scale = scaleRaw.intValue() * -1;

		BigDecimal trim = value.setScale(scale + 1, BigDecimal.ROUND_DOWN);

		BigDecimal multiple = trim.divide(round, value.scale(), BigDecimal.ROUND_UP).setScale(0, roundingType.getBigDecimalRoundingMode());
		rounded = round.multiply(multiple);

		return rounded;
	}
}
