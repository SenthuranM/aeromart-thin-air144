/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */

package com.isa.thinair.gdsservices.core.persistence.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.dto.internal.criteria.GDSMessageSearchCriteria;
import com.isa.thinair.gdsservices.api.model.GDSMessage;
import com.isa.thinair.gdsservices.api.util.DateUtil;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSServicesSupportJDBCDAO;
import com.isa.thinair.gdsservices.core.util.GDSConstants;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Nilindra Fernando
 */
public class GDSServicesSupportJdbcDAOImpl extends PlatformBaseJdbcDAOSupport implements GDSServicesSupportJDBCDAO {

	private static final String SQL_GDS = "gdsAllMessages";

	private static final String PAGE_ORDERBY = "gdsMessagesOrderBy";

	private static final String SQL_COUNT = "countOfMessages";

	private static final String SQL_PNR_FROM_EXT_REC_LOC = "pnrFromExternalRecordLocator";

	private static final String SQL_RESERVATION_VERSION_FROM_PNR = "versionFromPnr";

	private static final String SQL_PNR_FROM_EXT_TKT_NO = "pnrFromExternalTicketNumber";

	private static final String SQL_PNR_FROM_EXT_REC_LOC_AND_GDS_CODE = "pnrFromExternalRecordLocatorAndGdsCode";

	private Log log = LogFactory.getLog(getClass());

	public String getPnr(String originatorPnr) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		return (String) template.queryForObject(getQuery("PNRNO_BY_ORIGINATOR_PNRNO"), new Object[] { originatorPnr },
				String.class);
	}

	public Map<String, String> getSSRs() {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String queryString = getQuery("SSR_CODES");

		Map<String, String> ssrMap = (Map<String, String>) template.query(queryString, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, String> ssrMap = new HashMap<String, String>();
				if (rs != null) {
					while (rs.next()) {
						ssrMap.put(GDSApiUtils.maskNull(rs.getObject(1)), GDSApiUtils.maskNull(rs.getObject(2)));
					}
				}

				return ssrMap;
			}
		});

		return ssrMap;
	}

	public Collection<String> getTitles() {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		List<Map<String, Object>> titles = template.queryForList(getQuery("PAX_TITLES"));

		return getStringCollection(titles);
	}

	private Collection<String> getStringCollection(List<Map<String, Object>> titles) {
		Collection<String> strTitles = new ArrayList<String>();

		for (Iterator<Map<String, Object>> iter = titles.iterator(); iter.hasNext();) {
			Map<String, Object> dataMap = (Map<String, Object>) iter.next();

			String title = PlatformUtiltiies.nullHandler(dataMap.values().iterator().next());

			strTitles.add(title);
		}

		return strTitles;
	}

	public Page searchGDSMessages(GDSMessageSearchCriteria criteria, int startIndex, int noRecs) throws ModuleException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		GDSMessageResultsExtractor extractor = new GDSMessageResultsExtractor();

		int fromIndex = startIndex;
		int toIndex = startIndex + noRecs - 1;
		String sqlArray[] = constructSql(criteria);

		String sql = sqlArray[0];
		String countSql = sqlArray[1];

		jdbcTemplate.query(new GDSMessagePreparedStatementCreator(criteria, sql, fromIndex, toIndex), extractor);

		Collection<GDSMessage> messages = extractor.getMessages();

		GDSMessageCount gdsMessageCount = new GDSMessageCount(criteria, countSql, getDataSource());
		if (log.isDebugEnabled()) {
			log.debug(sql);
			log.debug(countSql);
		}
		return new Page(gdsMessageCount.getCountofGDSMessages(), startIndex, toIndex, messages);
	}

	private String[] constructSql(GDSMessageSearchCriteria criteria) throws ModuleException {
		StringBuilder sql = new StringBuilder(getQuery(SQL_GDS));
		StringBuilder sqlCount = new StringBuilder(getQuery(SQL_COUNT));
		String sqlPageOrderby = getQuery(PAGE_ORDERBY);

		String tempWhereStr = "";
		String andPrefix = " WHERE ";

		if (criteria.getMessageType() != null) {
			tempWhereStr += andPrefix + " MSG_TYPE LIKE '%" + criteria.getMessageType() + "%' ";
			andPrefix = "AND";
		}

		if (criteria.getMessageReceivedDateFrom() != null && criteria.getMessageReceivedDateTo() != null) {
			String[] strDateArr = DateUtil.constructDatesForSql(criteria.getMessageReceivedDateFrom(),
					criteria.getMessageReceivedDateTo());
			tempWhereStr += andPrefix + " MSG_RECEIVED_DATE BETWEEN TO_DATE(" + strDateArr[0] + ") AND TO_DATE(" + strDateArr[1]
					+ ") ";
			andPrefix = "AND";
		}

		if (criteria.getCommunicationReference() != null) {
			tempWhereStr += andPrefix + " COMMUNICATION_REFERENCE LIKE '%" + criteria.getCommunicationReference() + "%' ";
			andPrefix = "AND";
		}

		if (criteria.getGds() != null) {
			tempWhereStr += andPrefix + " GDS_NAME LIKE '%" + criteria.getGds() + "%' ";
			andPrefix = "AND";
		}

		if (criteria.getProcessStatus() != null) {
			tempWhereStr += andPrefix + " PROCESS_STATUS LIKE '%" + criteria.getProcessStatus() + "%' ";
			andPrefix = "AND";
		}

		if (criteria.getGdsPnr() != null) {
			tempWhereStr += andPrefix + " GDS_PNR LIKE '%" + criteria.getGdsPnr() + "%' ";
			andPrefix = "AND";
		}

		if (criteria.getMessageBrokerReferenceId() != null) {
			tempWhereStr += andPrefix + " MSG_BROKER_ID LIKE '%" + criteria.getMessageBrokerReferenceId() + "%' ";
			andPrefix = "AND";
		}

		if (criteria.getRequestXML() != null) {
			tempWhereStr += andPrefix + " REQUEST_XML LIKE '%" + criteria.getRequestXML() + "%' ";
			andPrefix = "AND";
		}

		if (criteria.getResponseXML() != null) {
			tempWhereStr += andPrefix + " RESPONSE_XML LIKE '%" + criteria.getResponseXML() + "%' ";
			andPrefix = "AND";
		}

		sqlCount.append(" " + tempWhereStr);
		sql.append(" " + tempWhereStr);
		sql.append(" " + sqlPageOrderby);

		return new String[] { sql.toString(), sqlCount.toString() };
	}

	@Override
	public String getPnrFromExternalRecordLocator(String externalRecordLocator) throws ModuleException {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		return (String) template.queryForObject(getQuery(SQL_PNR_FROM_EXT_REC_LOC),
				new Object[] { externalRecordLocator }, String.class);
	}

	public String getPnrFromExternalRecordLocator(String externalRecordLocator, String gdsCode) throws ModuleException {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		return (String) template.queryForObject(getQuery(SQL_PNR_FROM_EXT_REC_LOC_AND_GDS_CODE),
				new Object[] { externalRecordLocator, gdsCode }, String.class);
	}

	@Override
	public Long getReservationVersion(String pnr) throws ModuleException {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		return (Long) template.queryForObject(getQuery(SQL_RESERVATION_VERSION_FROM_PNR),
				new Object[] { pnr }, Long.class);
	}

	public String getPnrFromExternalTicketNumber(String externalTicketNumber) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		return (String) template.queryForObject(getQuery(SQL_PNR_FROM_EXT_TKT_NO),
				new Object[] { externalTicketNumber }, String.class);
	}

	// lock updating inventory for schedule messages related functions
	@Override
	public void lockUpdatingScheduleMessage(Integer scheduleMsgUnitId, String scheduleMsgUnitType) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO MB_T_IN_MSG_PROCESSING_QUEUE mpq");
		sql.append(" (mpq.MSG_PROCESSING_QUEUE_ID,mpq.SCHEDULE_FLIGHT_ID, mpq.LOCK_STATUS, mpq.SCHEDULE_TYPE,mpq.TIME_STAMP)");
		sql.append(" VALUES (MB_S_IN_MSG_PROCESSING_QUEUE.nextval,?,?,?,?)");

		template.update(sql.toString(), new Object[] { scheduleMsgUnitId, GDSConstants.InMessageQueueStatus.LOCK,
				scheduleMsgUnitType, new Date() });
	}

	@Override
	public void releasedLockedScheduleMessage(Integer inventoryId, String inventoryType) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM MB_T_IN_MSG_PROCESSING_QUEUE mpq WHERE ");
		sql.append(" mpq.SCHEDULE_FLIGHT_ID = ? and mpq.SCHEDULE_TYPE = ?");

		template.update(sql.toString(), new Object[] { inventoryId, inventoryType });

	}

	@Override
	public boolean isLockedScheduleMessage(Integer inventoryId, String inventoryType) {
		boolean isLocked = false;
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT count(mpq.SCHEDULE_FLIGHT_ID) as cnt");
		sql.append(" FROM MB_T_IN_MSG_PROCESSING_QUEUE mpq");
		sql.append(" WHERE mpq.SCHEDULE_TYPE = ? and mpq.SCHEDULE_FLIGHT_ID = ?");

		Integer count = (Integer) template.query(sql.toString(), new Object[] { inventoryType, inventoryId },
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						while (rs.next()) {
							return new Integer(rs.getInt("cnt"));
						}
						return null;
			}
				});

		if (count != null && count > 0) {
			isLocked = true;
		}
		return isLocked;
	}
}