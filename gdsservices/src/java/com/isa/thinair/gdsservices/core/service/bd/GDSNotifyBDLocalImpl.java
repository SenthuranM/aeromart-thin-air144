package com.isa.thinair.gdsservices.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.gdsservices.api.service.GDSNotifyBD;

@Local
public interface GDSNotifyBDLocalImpl extends GDSNotifyBD {
}
