package com.isa.thinair.gdsservices.core.typeA.transformers.itareqres.v03;

import static com.isa.thinair.gdsservices.core.util.IATADataExtractionUtil.getDate;
import static com.isa.thinair.gdsservices.core.util.IATADataExtractionUtil.getValue;
import iata.typea.v031.itareq.IATA;
import iata.typea.v031.itareq.ITAREQ;
import iata.typea.v031.itareq.Type0029;
import iata.typea.v031.itareq.UNB;
import iata.typea.v031.itareq.UNH;
import iata.typea.v031.itareq.UNH.UNH02;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import javax.xml.bind.JAXBElement;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.core.typeA.helpers.Constants;
import com.isa.thinair.gdsservices.core.typeA.model.InterchangeHeader;
import com.isa.thinair.gdsservices.core.typeA.model.InterchangeInfo;
import com.isa.thinair.gdsservices.core.typeA.model.MessageHeader;
import com.isa.thinair.gdsservices.core.typeA.model.MessageIdentifier;
import com.isa.thinair.gdsservices.core.typeA.model.RDD;
import com.isa.thinair.gdsservices.core.typeA.transformers.IATARequest;

/**
 * Extract the ODI info to be used to block seats.
 * 
 * @author mekanayake
 * 
 */
public class Itareq extends IATARequest {

	private ITAREQ itareq = null;;

	public Itareq(JAXBElement<?> jaxbRequest) {
		super(jaxbRequest);
		itareq = getMessage();
		customMessageBranchHandler = new InventoryAdjustment();
	}

	@Override
	public Map<String, Object> extractSpecificCommandParams() throws ModuleException {
		return customMessageBranchHandler.extractSpecificCommandParams(itareq);
	}

	public ITAREQ getMessage() {
		for (Object element : ((IATA) jaxbRequest.getValue()).getUNBAndUNGAndITAREQ()) {
			if (element instanceof ITAREQ) {
				return (ITAREQ) element;
			}
		}
		return null;
	}

	@Override
	public InterchangeHeader getInterchangeHeader() throws InteractiveEDIException {
		for (Object element : ((IATA) jaxbRequest.getValue()).getUNBAndUNGAndITAREQ()) {
			if (element instanceof UNB) {
				UNB unb = (UNB) element;

				String syntaxId = getValue(unb, RDD.M, 1, 1);

				String versionNo = getValue(unb, RDD.M, 1, 2);
				// sender information
				String senderId = getValue(unb, RDD.M, 2, 1);
				String senderInternalId = getValue(unb, RDD.C, 2, 3);
				InterchangeInfo sender = new InterchangeInfo(senderId, senderInternalId);

				// recipient information
				String recipientId = getValue(unb, RDD.M, 3, 1);
				String recipientCode = getValue(unb, RDD.C, 3, 3);
				InterchangeInfo recipient = new InterchangeInfo(recipientId, recipientCode);

				String datePart = getValue(unb, RDD.M, 4, 1);
				String timePart = getValue(unb, RDD.M, 4, 2);
				Date timeStamp = getDate(new BigDecimal(datePart), new BigDecimal(timePart));

				String interchangeControlRef = getValue(unb, RDD.C, 5);
				Type0029 associatioinCode = getValue(unb, RDD.C, 8);
				String associationCodeStr = null;
				if(associatioinCode != null){
					associationCodeStr = associatioinCode.toString();
				}
				return new InterchangeHeader(syntaxId, versionNo, sender, recipient, timeStamp, interchangeControlRef,
						associationCodeStr);
			}
		}
		return null;
	}

	@Override
	public MessageHeader getMessageHeader() {
		MessageHeader header = new MessageHeader();
		UNH unh = itareq.getUNH();
		header.setMessageReferenceNumber(unh.getUNH01());
		UNH02 unh02 = unh.getUNH02();
		MessageIdentifier identifier = new MessageIdentifier(unh02.getUNH0201().toString(), unh02.getUNH0202(),
				unh02.getUNH0203(), unh02.getUNH0204().toString());
		header.setMessageIdentifier(identifier);
		header.setCommonAccessReference(unh.getUNH03());
		return header;
	}

	@Override
	public String getBeanFromElement() {
		return Constants.CommandNames.INVENTORY_ADJUSTMENT;
	}

}