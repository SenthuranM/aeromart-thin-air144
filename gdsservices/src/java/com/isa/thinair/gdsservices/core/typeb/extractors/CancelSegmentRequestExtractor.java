package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.Iterator;
import java.util.List;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.internal.CancelSegmentRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

public class CancelSegmentRequestExtractor extends ValidatorBase {

	public static CancelSegmentRequest getRequest(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO) {

		CancelSegmentRequest cancelSegmentRequest = new CancelSegmentRequest();

		cancelSegmentRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
		cancelSegmentRequest = (CancelSegmentRequest) RequestExtractorUtil.addBaseAttributes(cancelSegmentRequest,
				bookingRequestDTO);

		cancelSegmentRequest = addSegments(cancelSegmentRequest, bookingRequestDTO);
		cancelSegmentRequest.setNotSupportedSSRExists(bookingRequestDTO.isNotSupportedSSRExists());

		return cancelSegmentRequest;
	}

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {

		RequestExtractorUtil.setResponderRecordLocator(bookingRequestDTO);
		String pnrReference = ValidatorUtils.getPNRReference(bookingRequestDTO.getResponderRecordLocator());

		if (pnrReference.length() == 0) {
			String message = MessageUtil.getMessage("gdsservices.extractors.emptyPNRResponder");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		} else {
			// There aren't any AddSegmentRequestExtractor specific validations.
			// The General Action codes are validated at the high level.
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
			return bookingRequestDTO;
		}
	}

	/**
	 * adds segments witch need to be canceled to the
	 * 
	 * @param cancelSegmentRequest
	 * @param bookingRequestDTO
	 * @return
	 */
	private static CancelSegmentRequest
			addSegments(CancelSegmentRequest cancelSegmentRequest, BookingRequestDTO bookingRequestDTO) {

		List<BookingSegmentDTO> segmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();
		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();
		BookingSegmentDTO segmentDTO;
		String gdsActionCode;

		for (Iterator<BookingSegmentDTO> iterator = segmentDTOs.iterator(); iterator.hasNext();) {
			segmentDTO = (BookingSegmentDTO) iterator.next();

			if (carrierCode.equals(segmentDTO.getCarrierCode())) {
				gdsActionCode = segmentDTO.getActionOrStatusCode();

				if (gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_IF_AVAILABLE.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_IF_HOLDING.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_RECOMMENDED.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CODESHARE_CANCEL.getCode())) {

					Segment segment = RequestExtractorUtil.getSegment(segmentDTO);
					cancelSegmentRequest.getSegments().add(segment);
				}
			}
		}

		return cancelSegmentRequest;
	}
}
