package com.isa.thinair.gdsservices.core.bl.typeA.common;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.api.model.GDSEdiMessages;
import com.isa.thinair.gdsservices.api.model.GdsEvent;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.core.dto.EDIMessageDTO;
import com.isa.thinair.gdsservices.core.typeA.converters.Convert;
import com.isa.thinair.platform.api.ServiceResponce;

public abstract class EdiMessageInitiator extends DefaultBaseCommand {

	protected IATAOperation iataOperation;
	protected GdsEvent.Request gdsEvent;
	protected List<EDIMessageDTO> ediMessageDTOs;
	protected Map<String, Object> respData;

	protected EdiMessageInitiator() {
		ediMessageDTOs = new ArrayList<EDIMessageDTO>();
		respData = new HashMap<String, Object>();
	}

	public ServiceResponce execute() throws ModuleException {

		iataOperation = (IATAOperation)getParameter(TypeACommandParamNames.IATA_OPERATION);
		gdsEvent = (GdsEvent.Request)getParameter(TypeACommandParamNames.IATA_EVENT);

		prepareMessages();
		sendMessage();
		processResponse();

		ServiceResponce sr = new DefaultServiceResponse();
		sr.addResponceParam(TypeACommandParamNames.EDI_PROCESS_STATUS, true);
		sr.addResponceParams(respData);

		return sr;
	}

	protected abstract void prepareMessages() throws ModuleException;

	private void sendMessage() throws ModuleException{
		String ediRequest;
		String ediResponse;

		GDSEdiMessages gdsMessage;


		for (EDIMessageDTO ediMessageDTO : ediMessageDTOs) {
			ediRequest = Convert.toEdiMessage(ediMessageDTO);
			ediMessageDTO.setRawRequest(ediRequest);

			gdsMessage = new GDSEdiMessages();
			gdsMessage.setReqMessage(ediMessageDTO.getRawRequest());
			gdsMessage.setReqTimestamp(new Date());
			gdsMessage.setGdsTransactionRef(ediMessageDTO.getRequestMetaDataDTO().getMessageReference());


			ediResponse = GDSServicesModuleUtil.getTypeAServiceBD().sendMessageToGDS(ediMessageDTO.getGdsCode(), ediRequest);

//			ediResponse = "";
//			try {
//				Path path = Paths.get("/home/chirantha/Desktop/edi/1.txt");
//				ediResponse = new String(Files.readAllBytes(path));
//			} catch (Exception e) {
//				e.printStackTrace();
//			}

			if (ediResponse != null) {
				ediMessageDTO.setRawResponse(ediResponse);
				Convert.processInboundMessage(ediMessageDTO);
			} else {
				ediMessageDTO.setInvocationError(true);
			}

			gdsMessage.setResMessage(ediMessageDTO.getRawResponse());
			gdsMessage.setResTimestamp(new Date());

			if (!ediMessageDTO.getErrors().isEmpty()) {
				gdsMessage.setErrors(ediMessageDTO.getErrors().toString());
			}

			GDSServicesModuleUtil.getGDSServicesBD(false).saveGdsEdiMessage(gdsMessage);
		}
	}

	protected abstract void processResponse() throws ModuleException;

}
