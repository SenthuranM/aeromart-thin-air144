package com.isa.thinair.gdsservices.core.dto;

import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.typea.common.TaxDetails;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class FinancialInformationEditor {

	public static FinancialInformationAssembler filter(FinancialInformationAssembler financialInformationAssembler, List<FilterSegment> filterSegments) {

		// TODO -- tax refunding

		FinancialInformationAssembler alteredFinancialInfoAssembler = new FinancialInformationAssembler();
		FinancialInformation alteredFinancialInfo = new FinancialInformation();
		FinancialInformation.Sector alteredSector;
		FinancialInformation.Segment alteredSegment;
		FinancialInformation.QuotedFare alteredQuotedFare;
		BigDecimal alteredFareAmount;
		BigDecimal alteredTotalFareAmount = BigDecimal.ZERO;

		boolean segmentMatches;
		boolean proceedNext;
		BigDecimal sectorFareTotal = BigDecimal.ZERO;
		BigDecimal sectorTotalWeight =  BigDecimal.ZERO;
		BigDecimal sectorFilteringWeight = BigDecimal.ZERO;

		FinancialInformation.Sector tempSector;
		List<FinancialInformation.Segment> tempSegments;
		FinancialInformation.Segment tempSegment;

		FinancialInformation financialInformation = financialInformationAssembler.getFinancialInformation();
		List<TaxDetails> taxDetails = financialInformationAssembler.getTaxDetails();

		int decimals = Integer.valueOf(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.DECIMALS));

		int sectorIndex = 0;
		int segmentIndex = 0;


		level_filter_segments:
		for (FilterSegment filterSegment : filterSegments) {

			level_sector:
			for (; sectorIndex < financialInformation.getSectors().size(); ) {

				tempSector = financialInformation.getSectors().get(sectorIndex);
				tempSegments = tempSector.getSegments();
				proceedNext = false;

				if (segmentIndex == 0) {
					alteredFinancialInfo.getSectors().add(new FinancialInformation.Sector());
				}
				alteredSector = alteredFinancialInfo.getSectors().get(alteredFinancialInfo.getSectors().size() - 1);


				level_segment:
				for (; segmentIndex < tempSegments.size(); ) {

					tempSegment = tempSector.getSegments().get(segmentIndex);

					String dep = CommonsServices.getGlobalConfig().getAirportInfo(filterSegment.departure, false)[4] == null ?
							filterSegment.departure : CommonsServices.getGlobalConfig().getAirportInfo(filterSegment.departure, false)[4].toString();

					String arr = CommonsServices.getGlobalConfig().getAirportInfo(filterSegment.arrival, false)[4] == null ?
							filterSegment.arrival : CommonsServices.getGlobalConfig().getAirportInfo(filterSegment.arrival, false)[4].toString();

					segmentMatches = dep.equals(tempSegment.getOrigin().getAirportOrCityCode()) &&
							arr.equals(tempSegment.getDestination().getAirportOrCityCode());

					segmentIndex++;
					sectorTotalWeight = sectorTotalWeight.add(filterSegment.getWeight());

					if (segmentMatches) {
						if (filterSegment.filter) {
							alteredSegment = new FinancialInformation.Segment();
							alteredSegment.setOrigin(tempSegment.getOrigin());
							alteredSegment.setCarrier(tempSegment.getCarrier());
							alteredSegment.setDestination(tempSegment.getDestination());
							alteredSector.getSegments().add(alteredSegment);

							sectorFilteringWeight = sectorFilteringWeight.add(filterSegment.getWeight());
						}

						proceedNext = true;
						break level_segment;
					}


				}

				// process fare quote in the end
				if (segmentIndex == tempSegments.size()) {
					for (FinancialInformation.QuotedFare quotedFare : tempSector.getQuotedFares()) {
						sectorFareTotal = sectorFareTotal.add(quotedFare.getAmount());
					}

					if (alteredSector.getSegments().size() > 0) {
						if (tempSector.getSegments().size() == alteredSector.getSegments().size()) {
							// entire sector applicable
							alteredSector.setQuotedFares(tempSector.getQuotedFares());
						} else {
							// part of the sector applicable
							alteredQuotedFare = new FinancialInformation.QuotedFare();
							alteredFareAmount = sectorFareTotal.multiply(sectorFilteringWeight)
									.divide(sectorTotalWeight, decimals, RoundingMode.FLOOR);
							alteredQuotedFare.setAmount(alteredFareAmount);
							alteredSector.getQuotedFares().add(alteredQuotedFare);
						}
					} else {
						alteredFinancialInfo.getSectors().remove(alteredFinancialInfo.getSectors().size() - 1);
					}

					sectorFareTotal = BigDecimal.ZERO;
					sectorTotalWeight = BigDecimal.ZERO;
					sectorFilteringWeight = BigDecimal.ZERO;

					sectorIndex++;
					segmentIndex = 0;
				}

				if (proceedNext) {
					continue level_filter_segments;
				}

			}
		}

		// ----------------------------------------
		// set missing values
		FinancialInformation.Carrier unFlownSeg = new FinancialInformation.Carrier();
		unFlownSeg.setFlown(false);
		unFlownSeg.setFareInclude(false);

		FinancialInformation.Location lastLocation = null;
		FinancialInformation.Segment dummySeg;
		for (int a = 0; a < alteredFinancialInfo.getSectors().size(); a++) {
			tempSector = alteredFinancialInfo.getSectors().get(a);

			for (int b = 0; b < tempSector.getSegments().size(); ) {
				tempSegment = tempSector.getSegments().get(b);

				if (lastLocation == null) {
					lastLocation = tempSegment.getOrigin();
					alteredFinancialInfo.setOrigin(lastLocation);
				} else {
					if (!lastLocation.getAirportOrCityCode().equals(tempSegment.getOrigin().getAirportOrCityCode())) {
						dummySeg = new FinancialInformation.Segment();
						dummySeg.setCarrier(unFlownSeg);
						dummySeg.setDestination(tempSegment.getOrigin());
						tempSector.getSegments().add(b++, dummySeg);
					}

					lastLocation = tempSector.getSegments().get(b++).getDestination();
				}
			}

			for (FinancialInformation.QuotedFare quotedFare : tempSector.getQuotedFares()) {
				alteredTotalFareAmount = alteredTotalFareAmount.add(quotedFare.getAmount());
			}
		}


		alteredFinancialInfo.setCurrency(financialInformation.getCurrency());
		alteredFinancialInfo.setTotalAmount(alteredTotalFareAmount);
		alteredFinancialInfo.setRateOfExchange(financialInformation.getRateOfExchange());


		alteredFinancialInfoAssembler.setFinancialInformation(alteredFinancialInfo);

		return alteredFinancialInfoAssembler;
	}

	public static FinancialInformationSummary aggregate(List<FinancialInformationAssembler> assemblers) {
		FinancialInformationSummary summary;

		summary = new FinancialInformationSummary();

		BigDecimal totalFare = BigDecimal.ZERO;
		BigDecimal totalTax = BigDecimal.ZERO;
		BigDecimal totalSurcharges = BigDecimal.ZERO;


		for (FinancialInformationAssembler assembler : assemblers) {
		 	totalFare = totalFare.add(assembler.getFareAmount());
			totalTax = totalTax.add(assembler.getTaxAmount());
			totalSurcharges = totalSurcharges.add(assembler.getSurcharge());
		}

		summary.setTotalFare(totalFare);
		summary.setTotalTax(totalTax);
		summary.setTotalSurcharge(totalSurcharges);

		return summary;
	}

	public static class FilterSegment {
		private String departure;
		private String arrival;
		private Integer travelSequence;
		private boolean filter;
		private BigDecimal weight;

		public String getDeparture() {
			return departure;
		}

		public void setDeparture(String departure) {
			this.departure = departure;
		}

		public String getArrival() {
			return arrival;
		}

		public void setArrival(String arrival) {
			this.arrival = arrival;
		}

		public Integer getTravelSequence() {
			return travelSequence;
		}

		public void setTravelSequence(Integer travelSequence) {
			this.travelSequence = travelSequence;
		}

		public boolean isFilter() {
			return filter;
		}

		public void setFilter(boolean filter) {
			this.filter = filter;
		}

		public BigDecimal getWeight() {
			return weight;
		}

		public void setWeight(BigDecimal weight) {
			this.weight = weight;
		}
	}
}
