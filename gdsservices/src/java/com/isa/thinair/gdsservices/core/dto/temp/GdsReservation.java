package com.isa.thinair.gdsservices.core.dto.temp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.isa.typea.common.ReservationControlInformation;

public class GdsReservation<T extends TravellerInformationWrapper> implements Serializable {

	private List<ReservationControlInformation> reservationControlInformation;
	private List<T> travellerInformation;

	public GdsReservation() {
		reservationControlInformation = new ArrayList<ReservationControlInformation>();
		travellerInformation = new ArrayList<T>();
	}

	public List<ReservationControlInformation> getReservationControlInformation() {
		return reservationControlInformation;
	}

	public List<T> getTravellerInformation() {
		return travellerInformation;
	}
}
