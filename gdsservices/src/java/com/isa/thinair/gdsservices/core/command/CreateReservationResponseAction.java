package com.isa.thinair.gdsservices.core.command;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.OperatingCarrierRecordLocator;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.internal.CreateReservationResponse;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

/**
 * @author Manoj Dhanushka
 */
public class CreateReservationResponseAction {
	
	private static Log log = LogFactory.getLog(CreateReservationResponseAction.class);
	
	public static CreateReservationResponse processRequest(CreateReservationResponse createReservationResponse) {
		try {
			String pnr = getOriginatorPNR(createReservationResponse);
			Reservation reservation = loadReservation(pnr, createReservationResponse.getGdsCredentialsDTO());
			String userNote = "";
			
			OperatingCarrierRecordLocator operatingCarrierRecordLocator = new OperatingCarrierRecordLocator();
			//operatingCarrierRecordLocator.setPnr(pnr);
			operatingCarrierRecordLocator.setOperatingCarrierRLOC(createReservationResponse.getResponderRecordLocator().getPnrReference());
			operatingCarrierRecordLocator.setGdsId(getGDSId(createReservationResponse.getGds().getCode()));
			reservation.addOperatingCarrierRecordLocator(operatingCarrierRecordLocator);

			userNote = createReservationResponse.getGds().getCode() + " PNR : " 
								+ createReservationResponse.getResponderRecordLocator().getPnrReference();
			
			CredentialsDTO credentialsDTO = getCredentialsDTO(createReservationResponse.getGdsCredentialsDTO());
			
			insertExtRecLocToUserNote(pnr, userNote, credentialsDTO);
			
			if (reservation != null) {
				GDSServicesModuleUtil.getReservationBD().updateReservationModifiability(reservation,
						ReservationInternalConstants.Modifiable.YES);
				createReservationResponse.setSuccess(true);
			}
		} catch (Exception e) {
			log.error(" CreateReservationResponse Failed for GDS PNR " + createReservationResponse.getOriginatorRecordLocator().getPnrReference(), e);
			createReservationResponse.setSuccess(false);
			createReservationResponse.setResponseMessage(MessageUtil.getDefaultErrorMessage());
			createReservationResponse.addErrorCode(MessageUtil.getDefaultErrorCode());
		}

		return createReservationResponse;
	}
	
	/**
	 * returns originator PNR
	 * 
	 * @param createReservationRequest
	 * @return
	 */
	private static String getOriginatorPNR(CreateReservationResponse createReservationResponse) {
		String pnr = "";

		if (createReservationResponse.getOriginatorRecordLocator() != null) {
			pnr = GDSApiUtils.maskNull(createReservationResponse.getOriginatorRecordLocator().getPnrReference());
		}

		return pnr;
	}
	
	/**
	 * try loads a reservation using originator pnr#
	 * 
	 * @param originatorPnr
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private static Reservation loadReservation(String pnr, GDSCredentialsDTO credentialsDTO) throws ModuleException {
		Reservation reservation = null;

		if (pnr.length() > 0) {
			reservation = CommonUtil.loadReservation(credentialsDTO, null, pnr);
		} else {
			throw new ModuleException("gdsservices.actions.originatorRequestEmpty");
		}

		return reservation;
	}
	
	private static Integer getGDSId(String gdsCode) {
		Map<String, GDSStatusTO> gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
		for (Entry<String, GDSStatusTO> entry : gdsStatusMap.entrySet()) {
			if (entry.getValue().getGdsCode().equals(gdsCode)) {
				return new Integer(entry.getKey());
			}
		}
		return null;
	}
	
	private static void insertExtRecLocToUserNote(String pnr, String userNote, CredentialsDTO credentialsDTO){
		
		String auditUserNote;
		AuditorBD auditor = GDSServicesModuleUtil.getAuditorBD();
		ReservationAudit reservationAudit = auditor.getLastReservationAudit(pnr);
		
		if(reservationAudit != null){
			auditUserNote = reservationAudit.getUserNote();
			
			if(!auditUserNote.isEmpty() && auditUserNote != null){
				userNote = auditUserNote + " , " + userNote; 
			}
			
			reservationAudit.setUserNote(userNote);
			
			GDSServicesModuleUtil.getAuditorBD().updateReservationAudit(reservationAudit);
			
		}else{
			reservationAudit = new ReservationAudit();
			constructAddUserNoteAudit(null , null , null ,reservationAudit);
			ReservationAudit.createReservationAudit(reservationAudit, pnr, userNote, credentialsDTO);
			try {
				auditor.audit(reservationAudit, reservationAudit.getContentMap());
			} catch (ModuleException e) {
				log.debug("Reservation audit for add user note failed");
				e.printStackTrace();
			}
		}
		
	}
	
	private static void constructAddUserNoteAudit(String ip , String originCarrierCode , String remoteUser , ReservationAudit reservationAudit){
		
		reservationAudit.setModificationType(AuditTemplateEnum.ADD_USER_NOTE.getCode());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddUserNote.IP_ADDDRESS, ip);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddUserNote.ORIGIN_CARRIER, originCarrierCode);
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.AddUserNote.REMOTE_USER , remoteUser);
		
	}
	
	private static CredentialsDTO getCredentialsDTO(GDSCredentialsDTO gdsCredentialsDTO){
		
		CredentialsDTO credentialsDTO = new CredentialsDTO();
		credentialsDTO.setUserId(gdsCredentialsDTO.getUserId());
		credentialsDTO.setSalesChannelCode(gdsCredentialsDTO.getSalesChannelCode());
		credentialsDTO.setDefaultCarrierCode(gdsCredentialsDTO.getDefaultCarrierCode());
		credentialsDTO.setAgentCode(gdsCredentialsDTO.getAgentCode());
		
		return credentialsDTO;
	}
}
