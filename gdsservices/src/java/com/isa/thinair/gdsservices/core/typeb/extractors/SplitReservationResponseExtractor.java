package com.isa.thinair.gdsservices.core.typeb.extractors;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.CreateReservationRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.SplitReservationResponse;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;

public class SplitReservationResponseExtractor extends ValidatorBase {

	public static CreateReservationRequest getRequest(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO) {
		CreateReservationRequest createReservationRequest = new SplitReservationResponse();
		CreateRequestExtractor.composeReservationRequest(gdsCredentialsDTO, bookingRequestDTO, createReservationRequest);
		return createReservationRequest;
	}

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {
		this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
		return bookingRequestDTO;
	}
}
