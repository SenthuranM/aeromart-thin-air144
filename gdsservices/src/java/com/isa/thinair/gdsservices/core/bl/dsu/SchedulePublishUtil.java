package com.isa.thinair.gdsservices.core.bl.dsu;

import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airmaster.api.model.AirportTerminal;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;

/**
 * @author Manoj Dhanushka
 */
public class SchedulePublishUtil {

	public static final String GMT_OFFSET_ACTION_MINUS = "-";

	public static final String GMT_OFFSET_ACTION_PLUS = "+";

	public static String getGMTOffset(Airport apt, Date localDate) throws ModuleException {

		int dstOffset = 0;

		// find GMT for the airport
		int gmtOffsetValue = getGMTOffsetOnly(apt);

		// calculate the zulu date without DST
		Date zuluDate = CalendarUtil.getZuluDate(new Date(), gmtOffsetValue, dstOffset);

		// find DST with zulu date
		AirportDST aptDST = GDSServicesModuleUtil.getAirportBD().getEffectiveAirportDST(apt.getAirportCode(), zuluDate);

		dstOffset = (aptDST != null) ? aptDST.getDstAdjustTime() : 0;

		int offset = gmtOffsetValue + dstOffset;

		String gmtOffset = "+0000";
		int minutesPerHour = 60;

		if (apt != null) {
			int timeHH = offset / minutesPerHour;
			int timeMM = offset % minutesPerHour;

			String timeHH_str = String.valueOf(timeHH);
			String timeMM_str = String.valueOf(timeMM);
			if (timeHH < 10)
				timeHH_str = "0" + timeHH_str;
			if (timeMM < 10)
				timeMM_str = "0" + timeMM_str;

			gmtOffset = timeHH_str + timeMM_str;

			gmtOffset = (apt.getGmtOffsetAction().equals(GMT_OFFSET_ACTION_MINUS)) ? GMT_OFFSET_ACTION_MINUS + gmtOffset
					: GMT_OFFSET_ACTION_PLUS + gmtOffset;
		}
		return gmtOffset;
	}

	public static String getFormattedOffset(int offSetInMinutes) throws ModuleException {

		String gmtOffset = "+0000";
		int minutesPerHour = 60;

		int timeHH = offSetInMinutes / minutesPerHour;
		int timeMM = offSetInMinutes % minutesPerHour;

		String timeHH_str = String.valueOf(timeHH);
		String timeMM_str = String.valueOf(timeMM);
		if (offSetInMinutes >= 0) {
			if (timeHH < 10)
				timeHH_str = "0" + timeHH_str;
			if (timeMM < 10)
				timeMM_str = "0" + timeMM_str;
		} else if (offSetInMinutes < 0) {
			if (timeHH < 10)
				timeHH_str = "0" + (timeHH_str.split("-"))[1];
			if (timeMM < 10)
				timeMM_str = "0" + timeMM_str;
		}

		gmtOffset = timeHH_str + timeMM_str;

		gmtOffset = (offSetInMinutes < 0) ? GMT_OFFSET_ACTION_MINUS + gmtOffset : GMT_OFFSET_ACTION_PLUS + gmtOffset;

		return gmtOffset;
	}

	public static int calculateEffectiveOffset(Airport apt, Date localDate) throws ModuleException {

		int dstOffset = 0;
		// find GMT for the airport
		int gmtOffsetValue = getGMTOffsetOnly(apt);

		// calculate the zulu date without DST
		Date zuluDate = CalendarUtil.getZuluDate(localDate, gmtOffsetValue, dstOffset);

		// find DST with zulu date
		AirportDST aptDST = GDSServicesModuleUtil.getAirportBD().getEffectiveAirportDST(apt.getAirportCode(), zuluDate);

		dstOffset = (aptDST != null) ? aptDST.getDstAdjustTime() : 0;

		int offset = gmtOffsetValue + dstOffset;

		return offset;
	}

	private static int getGMTOffsetOnly(Airport apt) throws ModuleException {
		int gmtOffset = 0;
		if (apt != null) {

			gmtOffset = (apt.getGmtOffsetAction().equals("-")) ? (-1 * apt.getGmtOffsetHours()) : apt.getGmtOffsetHours();
		}

		return gmtOffset;
	}

	public static String getRBDInfo(Collection bookingClassList) {

		String rbds = "";
		if (bookingClassList != null) {
			Iterator itRbd = bookingClassList.iterator();
			while (itRbd.hasNext()) {
				String code = (String) itRbd.next();
				rbds += code;
			}
		}
		return fillTailBlanks(20, rbds);
	}

	private static String fillTailBlanks(int targetLength, String source) {
		while (source.length() < targetLength) {
			source += " ";
		}
		if (source.length() > targetLength) {
			return source.substring(0, targetLength);
		}
		return source;
	}

	/**
	 * Handles null value and replace with a "" value. This also trims if a String value exist
	 * 
	 * @param string
	 * @return
	 */
	public static String nullHandler(Object object) {
		if (object == null) {
			return "";
		} else {
			return object.toString().trim();
		}
	}

	public static String getInternationalDomesticStatus(boolean status) {
		if (status) {
			return "DD";
		} else {
			return "II";
		}
	}

	public static String fillFrontZero(int targetLength, String source) {
		while (source.length() < targetLength) {
			source = "0" + source;
		}
		return source;
	}

	/**
	 * methood to get the start time of a given day
	 * 
	 * @param date
	 * @return
	 */
	public static Date getStartTimeOfDate(Date date) {

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(date);

		calander.set(GregorianCalendar.HOUR_OF_DAY, 0);
		calander.set(GregorianCalendar.MINUTE, 0);
		calander.set(GregorianCalendar.SECOND, 0);
		calander.set(GregorianCalendar.MILLISECOND, 0);

		return calander.getTime();
	}

	/**
	 * methood to check equality of the the start time of the given days
	 * 
	 * @param date
	 * @return
	 */
	public static boolean isEqualStartTimeOfDate(Date date1, Date date2) {

		GregorianCalendar calander1 = new GregorianCalendar();
		calander1.setTime(date1);
		GregorianCalendar calander2 = new GregorianCalendar();
		calander2.setTime(date2);
		return (calander1.get(GregorianCalendar.YEAR) == calander2.get(GregorianCalendar.YEAR)
				&& calander1.get(GregorianCalendar.MONTH) == calander2.get(GregorianCalendar.MONTH) && calander1
				.get(GregorianCalendar.DAY_OF_MONTH) == calander2.get(GregorianCalendar.DAY_OF_MONTH));
	}

	/**
	 * 
	 * @param parseDate
	 * @param dateFrom
	 * @param dateTo
	 * @return
	 */
	public static boolean isBetween(Date parseDate, Date fromDate, Date toDate) {

		boolean status = false;
		GregorianCalendar compareDate = getCalendar(parseDate);
		GregorianCalendar dateFrom = getCalendar(fromDate);
		GregorianCalendar dateTo = getCalendar(toDate);

		if (compareDate.after(dateFrom) && compareDate.before(dateTo)) {
			status = true;
		}
		return status;
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	private static GregorianCalendar getCalendar(Date date) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		return calendar;
	}

	public static Date getChangedDate(Date date, int value) {

		GregorianCalendar dateCalender = new GregorianCalendar();
		dateCalender.setTime(date);
		dateCalender.add(GregorianCalendar.DATE, value);
		return dateCalender.getTime();
	}

	public static String getIATAAirCraftType(Collection airCraftList, String model) {
		String iataCode = "";
		if (airCraftList != null) {
			Iterator ite = airCraftList.iterator();
			while (ite.hasNext()) {
				AircraftModel aircraft = (AircraftModel) ite.next();
				if (aircraft.getModelNumber().equals(model)) {
					iataCode = aircraft.getIataAircraftType();
					break;
				}
			}
		}
		if (iataCode == null || iataCode.equals("")) {
			return "000";
		} else if (iataCode.length() > 3) {
			return iataCode.substring(iataCode.length() - 3);
		} else {
			return fillFrontZero(3, iataCode);
		}
	}

	public static String getTerminalCode(AirportTerminal terminal) {
		String terminalCode = "  ";
		if (terminal != null && terminal.getTerminalCode() != null) {
			terminalCode = fillTailBlanks(2, terminal.getTerminalCode());
		}
		return terminalCode;
	}

}
