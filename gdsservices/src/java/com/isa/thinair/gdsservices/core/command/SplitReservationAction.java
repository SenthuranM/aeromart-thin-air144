package com.isa.thinair.gdsservices.core.command;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.ReservationStatus;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.internal.RecordLocator;
import com.isa.thinair.gdsservices.api.dto.internal.SplitReservationRequest;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.platform.api.ServiceResponce;

public class SplitReservationAction {
	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(SplitReservationAction.class);

	public static SplitReservationRequest processRequest(SplitReservationRequest splitReservationRequest) {
		try {
			Reservation reservation = CommonUtil.loadReservation(splitReservationRequest.getGdsCredentialsDTO(), null,
					splitReservationRequest.getResponderRecordLocator().getPnrReference());

			CommonUtil.validateReservation(splitReservationRequest.getGdsCredentialsDTO(), reservation);
			Collection<Integer> pnrPaxIds = CommonUtil.extractPaxIDs(reservation, splitReservationRequest.getPassengers());

			if (pnrPaxIds != null && pnrPaxIds.size() > 0) {
				// Remember when spliting we don't get the passenger type.
				// FIXME : Please make configurable if needed
				// CommonUtil.checkChildOnlyBookingExistWhenSpliting(pnrPaxIds, reservation);

				ServiceResponce serviceRes = GDSServicesModuleUtil.getReservationBD().splitReservation(reservation.getPnr(),
						pnrPaxIds, reservation.getVersion(), null, null,
						CommonUtil.getTrackingInfo(splitReservationRequest.getGdsCredentialsDTO()), null,
						splitReservationRequest.getOriginatorRecordLocator().getPnrReference());

				if (serviceRes != null && serviceRes.isSuccess()) {
					String strNewPNR = (String) serviceRes.getResponseParam(CommandParamNames.PNR);
					RecordLocator recordLocator = new RecordLocator();
					recordLocator.setPnrReference(strNewPNR);
					splitReservationRequest.setResponderRecordLocator(recordLocator);
					splitReservationRequest.setSuccess(true);
				} else {
					splitReservationRequest.setSuccess(false);
					splitReservationRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
					splitReservationRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
					Reservation modifiedReservation = CommonUtil.loadReservation(splitReservationRequest.getGdsCredentialsDTO(),
							splitReservationRequest.getOriginatorRecordLocator().getPnrReference(), null);
					if (modifiedReservation.getStatus().equals(ReservationStatus.OHD.toString())) {
						String message = "";
						Date releaseTimeStamp = modifiedReservation.getReleaseTimeStamps()[0];
						if (GDSServicesModuleUtil.getConfig().isShowPaymentDetailsInTTYMessage()) {
							message = CommonUtil.getFormattedMessage(GDSInternalCodes.ResponseMessageCode.CONFIRMED_ON_HOLD,
									modifiedReservation.getLastCurrencyCode(), modifiedReservation.getTotalAvailableBalance(),
									releaseTimeStamp);
						} else {
							message = CommonUtil.getFormattedMessage(GDSInternalCodes.ResponseMessageCode.CONFIRMED_ON_HOLD,
									modifiedReservation.getLastCurrencyCode(), modifiedReservation.getTotalAvailableBalance(),
									releaseTimeStamp);
						}
						if (splitReservationRequest.isNotSupportedSSRExists()) {
							message = message
									+ MessageUtil.getMessage(GDSInternalCodes.ResponseMessageCode.SSR_NOT_SUPPORTED.getCode());
						}
						splitReservationRequest.setResponseMessage(message);

					}
				}
			} else {
				splitReservationRequest.setResponseMessage(MessageUtil.getMessage("gdsservices.actions.invalidPassengers"));
				splitReservationRequest.addErrorCode("gdsservices.actions.invalidPassengers");
				splitReservationRequest.setSuccess(false);
			}
		} catch (ModuleException e) {
			log.error(" SplitReservationAction Failed for GDS PNR "
					+ splitReservationRequest.getOriginatorRecordLocator().getPnrReference(), e);
			splitReservationRequest.setSuccess(false);
			splitReservationRequest.setResponseMessage(e.getMessageString());
			splitReservationRequest.addErrorCode(e.getExceptionCode());
		} catch (Exception e) {
			log.error(" SplitReservationAction Failed for GDS PNR "
					+ splitReservationRequest.getOriginatorRecordLocator().getPnrReference(), e);
			splitReservationRequest.setSuccess(false);
			splitReservationRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
			splitReservationRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
		}

		return splitReservationRequest;
	}
}
