package com.isa.thinair.gdsservices.core.typeA.transformers;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.GdsUtil;
import com.isa.typea.common.TicketControlTicketNumberDetails;
import com.isa.typea.common.TicketCoupon;
import com.isa.typea.common.TicketNumberDetails;
import com.isa.typea.common.TravellerTicketControlInformation;
import com.isa.typea.common.TravellerTicketingInformation;

public class ReservationDtoMerger {

	public static List<LccClientPassengerEticketInfoTO> getMergedETicketCoupons(List<TravellerTicketingInformation> travellerTicketingInformation,
	                                                                                         LCCClientReservation reservation) throws GdsTypeAException {
		List<LccClientPassengerEticketInfoTO> coupons = new ArrayList<LccClientPassengerEticketInfoTO>();
		LCCClientReservationPax lccPax;
		LccClientPassengerEticketInfoTO eticketInfoTO;

		for (TravellerTicketingInformation traveller : travellerTicketingInformation) {

			lccPax = GDSDTOUtil.resolveReservationPax(traveller, traveller.getTravellers().get(0), reservation);

			coupons.addAll(getMergedETicketCoupons(traveller.getTickets(), lccPax));

		}

		return coupons;
	}

	public static List<LccClientPassengerEticketInfoTO> getTicketControlMergedETicketCoupons(List<TravellerTicketControlInformation> travellerTicketingInformation,
	                                                                                         LCCClientReservation reservation) throws GdsTypeAException {
		List<LccClientPassengerEticketInfoTO> coupons = new ArrayList<LccClientPassengerEticketInfoTO>();
		LCCClientReservationPax lccPax;
		LccClientPassengerEticketInfoTO eticketInfoTO;

		for (TravellerTicketControlInformation traveller : travellerTicketingInformation) {

			lccPax = GDSDTOUtil.resolveReservationPax(traveller, traveller.getTravellers().get(0), reservation);

			coupons.addAll(getTicketControlMergedETicketCoupons(traveller.getTickets(), lccPax));

		}

		return coupons;
	}

	public static List<LccClientPassengerEticketInfoTO> getMergedETicketCoupons(List<TicketNumberDetails> tickets,
	                                                                            LCCClientReservationPax lccPax) throws GdsTypeAException {
		List<LccClientPassengerEticketInfoTO> coupons = new ArrayList<LccClientPassengerEticketInfoTO>();

		for (TicketNumberDetails ticket : tickets) {
			for (TicketCoupon coupon : ticket.getTicketCoupon()) {
				LccClientPassengerEticketInfoTO  eticketInfoTO = new LccClientPassengerEticketInfoTO();
				eticketInfoTO = GdsUtil.lccPaxEticket(ticket, coupon, lccPax.geteTickets());

				if (eticketInfoTO != null) {
					eticketInfoTO.setExternalPaxETicketNo(ticket.getTicketNumber());

					if (coupon.getCouponNumber() != null) {
						eticketInfoTO.setExternalCouponNo(Integer.parseInt(coupon.getCouponNumber()));
					}
					eticketInfoTO.setExternalCouponStatus(coupon.getStatus());
					coupons.add(eticketInfoTO);
				} else {
					throw new GdsTypeAException();
				}
			}
		}

		return coupons;
	}

	public static List<LccClientPassengerEticketInfoTO> getTicketControlMergedETicketCoupons(List<TicketControlTicketNumberDetails> tickets,
	                                                                            LCCClientReservationPax lccPax) throws GdsTypeAException {
		List<LccClientPassengerEticketInfoTO> coupons = new ArrayList<LccClientPassengerEticketInfoTO>();
		LccClientPassengerEticketInfoTO eticketInfoTO;

		for (TicketNumberDetails ticket : tickets) {
			for (TicketCoupon coupon : ticket.getTicketCoupon()) {
				eticketInfoTO = GdsUtil.lccTicketControlPaxEticket(ticket, coupon, lccPax.geteTickets());

				if (eticketInfoTO != null) {
					eticketInfoTO.setExternalPaxETicketNo(ticket.getTicketNumber());

					if (coupon.getCouponNumber() != null) {
						eticketInfoTO.setExternalCouponNo(Integer.parseInt(coupon.getCouponNumber()));
					}
					eticketInfoTO.setExternalCouponStatus(TypeAConstants.CouponStatus.ORIGINAL_ISSUE.equals(coupon.getStatus()) ? EticketStatus.OPEN
									.code() : coupon.getStatus());
					coupons.add(eticketInfoTO);
				} else {
					throw new GdsTypeAException();
				}
			}
		}

		return coupons;
	}
}
