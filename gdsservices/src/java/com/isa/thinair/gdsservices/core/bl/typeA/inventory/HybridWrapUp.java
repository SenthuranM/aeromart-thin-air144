package com.isa.thinair.gdsservices.core.bl.typeA.inventory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.gdsservices.core.bl.typeA.common.MessageErrorAdaptor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.api.dto.internal.TempSegBcAllocDTO;
import com.isa.thinair.gdsservices.api.dto.typea.InventoryAdjustmentTypeBRQ;
import com.isa.thinair.gdsservices.api.dto.typea.InventoryAdjustmentTypeBRS;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBAnalyseRQ;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBAnalyseRS;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRQ;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.command.CommonUtil;
import com.isa.thinair.gdsservices.core.dto.EDIMessageDTO;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.typeb.unify.UnifierFactory;
import com.isa.thinair.gdsservices.core.typeb.unify.UnifyTypeBMessage;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.gdsservices.core.util.TypeAMessageUtil;
import com.isa.thinair.gdsservices.core.util.TypeANotes;
import com.isa.thinair.msgbroker.api.dto.BookingSegmentDTO;
import com.isa.thinair.msgbroker.api.service.TypeBServiceBD;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.typea.common.ErrorInformation;
import com.isa.typea.common.MessageFunction;
import com.isa.typea.common.ReservationControlInformation;
import com.isa.typea.hwpreq.AAHWPREQ;
import com.isa.typea.hwpreq.HWPREQMessage;
import com.isa.typea.hwpres.AAHWPRES;
import com.isa.typea.hwpres.HWPRESMessage;

/**
 * @isa.module.command name="hybridWrapUp"
 */
public class HybridWrapUp extends DefaultBaseCommand {

    private static Log log = LogFactory.getLog(HybridWrapUp.class);

    private EDIMessageDTO messageDTO;
    private AAHWPREQ hwpreq;
    private AAHWPRES hwpres;

    public ServiceResponce execute() {

	    MessageFunction messageFunction;
	    ErrorInformation errorInformation;

        messageDTO = (EDIMessageDTO) getParameter(TypeACommandParamNames.EDI_REQUEST_DTO);
        hwpreq = (AAHWPREQ) messageDTO.getRequest();

        hwpres = new AAHWPRES();
        HWPRESMessage hwpresMessage = new HWPRESMessage();
        hwpresMessage.setMessageHeader(GDSDTOUtil.createRespMessageHeader(hwpreq.getMessage().getMessageHeader(),
                messageDTO.getRequestMetaDataDTO().getMessageReference(), messageDTO.getRequestMetaDataDTO().getIataOperation().getResponse()));
        hwpres.setHeader(GDSDTOUtil.createRespEdiHeader(hwpreq.getHeader(), hwpresMessage.getMessageHeader(), messageDTO));
        hwpres.setMessage(hwpresMessage);

        messageDTO.setResponse(hwpres);

        try {
            processInventoryAdjustmentWrapUp();
        } catch (GdsTypeAException e) {
			log.error("processInventoryAdjustmentWrapUp ---- ", e);

	        messageFunction = new MessageFunction();
	        messageFunction.setServiceType(TypeAConstants.BusinessFunction.AIR_PROVIDER);
	        messageFunction.setMessageFunction(TypeAConstants.MessageFunction.CREATE);
	        messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
	        hwpresMessage.setMessageFunction(messageFunction);

	        errorInformation = new ErrorInformation();
	        errorInformation.setApplicationErrorCode(e.getEdiErrorCode());
	        hwpresMessage.setErrorInformation(errorInformation);

	        messageDTO.setInvocationError(true);

			if (!e.isNoRollback()) {
				messageDTO.setRollbackTransaction(true);
			}

        } catch (Exception e) {
			log.error("processInventoryAdjustmentWrapUp ---- ", e);

			messageFunction = new MessageFunction();
	        messageFunction.setServiceType(TypeAConstants.BusinessFunction.AIR_PROVIDER);
	        messageFunction.setMessageFunction(TypeAConstants.MessageFunction.CREATE);
	        messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
	        hwpresMessage.setMessageFunction(messageFunction);

	        errorInformation = new ErrorInformation();
	        errorInformation.setApplicationErrorCode(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR);
	        hwpresMessage.setErrorInformation(errorInformation);

	        messageDTO.setInvocationError(true);
	        messageDTO.setRollbackTransaction(true);

        }

        ServiceResponce serviceResponce = new DefaultServiceResponse();
        serviceResponce.addResponceParam(TypeACommandParamNames.EDI_RESPONSE_DTO, messageDTO.getResponse());

        return serviceResponce;
    }

    public void processInventoryAdjustmentWrapUp() throws GdsTypeAException {
        TypeBServiceBD typeBServiceBD = GDSServicesModuleUtil.getTypeBServiceBD();
		ReservationBD reservationBD;

        HWPREQMessage hwpreqMessage = hwpreq.getMessage();
        HWPRESMessage hwpresMessage = hwpres.getMessage();

        List<ReservationControlInformation> colRci;
        ReservationControlInformation rci;
        TypeBAnalyseRQ analyseRQ;
        TypeBAnalyseRS analyseRS;
        String pnr = null;
        MessageFunction messageFunction = null;
        InventoryAdjustmentTypeBRS invAdjRS = null;
        List<Integer> applicableTempSegBcAllocIds = new ArrayList<Integer>();
        UnifyTypeBMessage unifyTypeBMessage = UnifierFactory.getTypeBMessageUnifier(hwpreqMessage.getOriginatorInformation());
        int paxCount;
        String fccSegBcAllocKey;
        FCCSegBCInventory fccSegBCInventory;

        String modifiedText = unifyTypeBMessage.unifyMessage(hwpreqMessage.getFreeText());

        analyseRQ = new TypeBAnalyseRQ();
        analyseRQ.setTypeBMessage(modifiedText);
        analyseRQ.setOriginDestination(true);

        try {
            analyseRS = typeBServiceBD.analyseTypeBMessage(analyseRQ);
        } catch (ModuleException e) {
            throw new GdsTypeAException();
        }

        paxCount = analyseRS.getPaxCount();

        List<BookingSegmentDTO> segInfoDTOs = analyseRS.getSegments();
        List<BookingSegmentDTO> newSegs = GDSDTOUtil.getNewSegments(segInfoDTOs);

        List<ReservationSegmentDTO> actualResSegments = CommonUtil.getActualResSegments(analyseRS.getGdsCarrierCode(), newSegs);
        Map<String, Integer> tempSegBcAllocIds = (Map<String, Integer>) messageDTO.getMessagingSession().get(TypeAConstants.MessageSessionKey.BLOCKED_SEATS);

        for (ReservationSegmentDTO resSeg : actualResSegments) {
            fccSegBCInventory = GDSServicesModuleUtil.getFlightInventoryBD().getFCCSegBCInventory(resSeg.getFlightSegId(), resSeg.getBookingClass());
	        fccSegBcAllocKey = String.valueOf(fccSegBCInventory.getFccsbInvId());

	        if (tempSegBcAllocIds != null && tempSegBcAllocIds.containsKey(fccSegBcAllocKey)) {
		        applicableTempSegBcAllocIds.add(tempSegBcAllocIds.get(fccSegBcAllocKey));
//		        break;
	        }

        }


	    List<Integer> tempAllocsList;
	    try {
		    if (analyseRS.getInfantCount() > 0) {
			    Collection<TempSegBcAlloc> adjustedAllocs = GDSServicesModuleUtil.getFlightInventoryResBD().adjustSeatsBlocked(applicableTempSegBcAllocIds, 0, analyseRS.getInfantCount());

			    tempAllocsList = new ArrayList<Integer>();
			    for (TempSegBcAlloc adjustedAlloc : adjustedAllocs) {
				    tempAllocsList.add(adjustedAlloc.getId());
			    }
			    applicableTempSegBcAllocIds = tempAllocsList;
		    }
	    } catch (ModuleException e) {
		    throw new GdsTypeAException();
	    }

		InventoryAdjustmentTypeBRQ typeBRQ = new InventoryAdjustmentTypeBRQ();
		typeBRQ.setRequestType(TypeBRQ.RequestType.INVENTORY_ADJUSTMENT);

		if (applicableTempSegBcAllocIds != null && !applicableTempSegBcAllocIds.isEmpty()) {

			List<TempSegBcAlloc> tempSegBcAllocs = GDSServicesModuleUtil.getFlightInventoryBD().getTempSegBcAllocs(applicableTempSegBcAllocIds);

			if (tempSegBcAllocs.isEmpty()) {
				throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.UNABLE_TO_PROCESS, TypeANotes.ErrorMessages.NO_MESSAGE);
			}

			List<TempSegBcAllocDTO> tempSegBcAllocDTOs = new ArrayList<TempSegBcAllocDTO>();
			for (TempSegBcAlloc tempSegBcAlloc : tempSegBcAllocs) {
				tempSegBcAllocDTOs.add(new TempSegBcAllocDTO(tempSegBcAlloc));
			}
			typeBRQ.setBlockedSeats(tempSegBcAllocDTOs);
		}

		typeBRQ.setPaxCount(analyseRS.getPaxCount() + analyseRS.getInfantCount());

		typeBRQ.setTypeBMessage(modifiedText.trim());
		typeBRQ.setChannel(TypeBRQ.Channel.TYPE_A);

            try {
//				if (!applicableTempSegBcAllocIds.isEmpty()) {
//					tempSegBcAllocs = GDSServicesModuleUtil.getFlightInventoryBD().getTempSegBcAllocs(applicableTempSegBcAllocIds);
//
//					try {
//						reservationBD = GDSServicesModuleUtil.getReservationBD();
//						reservationBD.releaseBlockedSeats(tempSegBcAllocs);
//					} catch (ModuleException ex) {
//						throw  new GdsTypeAException(TypeAConstants.ApplicationErrorCode.UNABLE_TO_PROCESS, TypeANotes.ErrorMessages.NO_MESSAGE);
//					}
//				}

                invAdjRS = (InventoryAdjustmentTypeBRS) typeBServiceBD.processTypeBMessage(typeBRQ);
            } catch (Exception e) {
                log.error("Error occurred while processing Type-B Msg ", e);

                throw new GdsTypeAException();
            }

		if (invAdjRS.getMessageError() != null && invAdjRS.getMessageError().isErroneous()) {
			if (!hwpreqMessage.getReservationControlInformation().isEmpty()) {
				log.error("rloc ---- " + hwpreqMessage.getReservationControlInformation().get(0).getAirlineCode() +
						"---- " + hwpreqMessage.getReservationControlInformation().get(0).getReservationControlNumber());
			}

			if (!invAdjRS.getMessageError().getErrors().isEmpty()) {
				messageDTO.getErrors().addAll(invAdjRS.getMessageError().getErrorCodes());
				throw MessageErrorAdaptor.toGdsTypeAException(invAdjRS.getMessageError().getErrors());
			}

			throw new GdsTypeAException();
		} else {
			pnr = invAdjRS.getPnr();

			if (pnr == null || pnr.trim().isEmpty()) {
				throw new GdsTypeAException();
			}
		}



        colRci = hwpresMessage.getReservationControlInformation();
        rci = hwpreqMessage.getReservationControlInformation().get(0);
        colRci.add(rci);

        rci = new ReservationControlInformation();
        rci.setAirlineCode(GDSServicesUtil.getCarrierCode());
        rci.setReservationControlNumber(pnr);
        colRci.add(rci);

        messageFunction = new MessageFunction();
        messageFunction.setServiceType(TypeAConstants.BusinessFunction.AIR_PROVIDER);
        messageFunction.setResponseType(TypeAConstants.MessageFunction.CONFIRMATION);

        hwpresMessage.setMessageFunction(messageFunction);

    }

}
