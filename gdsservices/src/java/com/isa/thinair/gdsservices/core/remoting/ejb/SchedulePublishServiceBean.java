/*
 * 
==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
===============================================================================
 */
package com.isa.thinair.gdsservices.core.remoting.ejb;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import com.isa.thinair.gdsservices.core.bl.ssm.SchedulePublishBL;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airschedules.api.dto.ReScheduleRequiredInfo;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;

import com.isa.thinair.gdsservices.core.service.bd.SchedulePublishBDImpl;
import com.isa.thinair.gdsservices.core.service.bd.SchedulePublishBDLocalImpl;

/**
 * Session bean to provide GDS Schedule Publish Service related functionalities
 * 
 * @author M.Rikaz
 * @since 6.0
 */
@Stateless
@RemoteBinding(jndiBinding = "SchedulePublishService.remote")
@LocalBinding(jndiBinding = "SchedulePublishService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class SchedulePublishServiceBean extends PlatformBaseSessionBean implements SchedulePublishBDImpl,
		SchedulePublishBDLocalImpl {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(SchedulePublishServiceBean.class);

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void publishSSMMessageForScheduleChanges(ReScheduleRequiredInfo reScheduleInfo) throws ModuleException {
		SchedulePublishBL.sendSSMForReSchedules(reScheduleInfo);
	}
}