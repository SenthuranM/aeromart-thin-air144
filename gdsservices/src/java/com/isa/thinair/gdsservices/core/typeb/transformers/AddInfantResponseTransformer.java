package com.isa.thinair.gdsservices.core.typeb.transformers;

import java.util.List;
import java.util.Map;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRChildDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDocsDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRInfantDTO;
import com.isa.thinair.gdsservices.api.dto.internal.AddInfantRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.AdviceCode;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ProcessStatus;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;

public class AddInfantResponseTransformer {
	/**
	 * Transforms reservationResponseMap back to bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param reservationResponseMap
	 * @return
	 */
	public static BookingRequestDTO getResponse(BookingRequestDTO bookingRequestDTO,
			Map<ReservationAction, GDSReservationRequestBase> reservationResponseMap) {

		AddInfantRequest addInfantRequest = (AddInfantRequest) reservationResponseMap.get(ReservationAction.ADD_INFANT);

		if (addInfantRequest.isSuccess()) {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_SUCCESS);
			bookingRequestDTO = setSSRStatuses(bookingRequestDTO, addInfantRequest);
			bookingRequestDTO = ValidatorUtils.addResponse(bookingRequestDTO, addInfantRequest);
		} else {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_FAILURE);
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, addInfantRequest);
		}
		bookingRequestDTO.getErrors().addAll(addInfantRequest.getErrorCode());

		return bookingRequestDTO;
	}

	/**
	 * Adds infant SSR statuses to the bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param addInfanttRequest
	 * @param b
	 */
	private static BookingRequestDTO setSSRStatuses(BookingRequestDTO bookingRequestDTO, AddInfantRequest addInfanttRequest) {

		List<SSRDTO> ssrDTOs = bookingRequestDTO.getSsrDTOs();

		for (SSRDTO ssrDTO : ssrDTOs) {
			if (ssrDTO instanceof SSRInfantDTO) {
				ssrDTO.setAdviceOrStatusCode(AdviceCode.CONFIRMED.getCode());
			}
			if (ssrDTO instanceof SSRChildDTO) {
				ssrDTO.setAdviceOrStatusCode(AdviceCode.CONFIRMED.getCode());
			}
			if (ssrDTO instanceof SSRDocsDTO) {
				ssrDTO.setAdviceOrStatusCode(AdviceCode.CONFIRMED.getCode());
			}
		}

		return bookingRequestDTO;
	}
}
