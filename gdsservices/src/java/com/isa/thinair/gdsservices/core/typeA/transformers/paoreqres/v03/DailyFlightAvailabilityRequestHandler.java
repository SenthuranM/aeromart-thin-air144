package com.isa.thinair.gdsservices.core.typeA.transformers.paoreqres.v03;

import static com.isa.thinair.gdsservices.core.util.IATADataExtractionUtil.getDate;
import static com.isa.thinair.gdsservices.core.util.IATADataExtractionUtil.getValue;
import iata.typea.v031.paoreq.GROUP1;
import iata.typea.v031.paoreq.GROUP3;
import iata.typea.v031.paoreq.GROUP4;
import iata.typea.v031.paoreq.ODI;
import iata.typea.v031.paoreq.ORG;
import iata.typea.v031.paoreq.PAOREQ;
import iata.typea.v031.paoreq.PDI;
import iata.typea.v031.paoreq.PDI.PDI02;
import iata.typea.v031.paoreq.TVL;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.typea.ERCDetail;
import com.isa.thinair.gdsservices.api.dto.typea.TVLDetail;
import com.isa.thinair.gdsservices.api.dto.typea.TVLDto;
import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.core.typeA.model.RDD;
import com.isa.thinair.gdsservices.core.util.TypeAErrorUtill;

public class DailyFlightAvailabilityRequestHandler {

	public static Map<String, Object> extractCommandParams(PAOREQ paoreq) throws InteractiveEDIException {
		Map<String, Object> params = new HashMap<String, Object>();

		List<TVLDto> tvlDtos = new ArrayList<TVLDto>();

		String messageFunction = getValue(paoreq.getMSG(), RDD.C, 1, 2);

		GROUP1 group1 = paoreq.getGROUP1();

		// Group 2
		String pos = null;
		if (group1 != null) {
			ORG org = group1.getORG();
			pos = getValue(org, RDD.C, 1, 2);
		}

		// Group one and two are ignored as they are not relevant to daily flight availability

		// Group 3
		List<GROUP3> group3List = paoreq.getGROUP3();
		if (group3List == null) {
			TypeAErrorUtill.addAppError(params, CodeSetEnum.CS9321.AppError_114, 1); // invalid flight number
		}

		for (GROUP3 group3 : group3List) {

			// ODI is ignored at the moment.
			ODI odi = group3.getODI();
			if (odi == null) {
				// Invalid Origin and Destination Pair
				TypeAErrorUtill.addAppError(params, CodeSetEnum.CS9321.AppError_130, 1);
			}

			List<GROUP4> group4List = group3.getGROUP4();
			if (group4List != null) {
				for (GROUP4 group4 : group4List) {
					TVLDto tvlDto = new TVLDto();

					TVLDetail tvlDetail = new TVLDetail();
					AvailableFlightSearchDTO aviFlightSearchDTO = new AvailableFlightSearchDTO();

					// TVL processing begin.
					TVL tvl = group4.getTVL();
					if (tvl == null) {
						// invalid flight number
						TypeAErrorUtill.addAppError(params, CodeSetEnum.CS9321.AppError_114, 1);
					}

					String carrierCode = getValue(tvl, RDD.M, 4, 1);
					// we need to process TVL only for own segments
					if (carrierCode.equals(AppSysParamsUtil.getDefaultCarrierCode())) {

						// Capture start date and end date to departure
						String startDateString = getValue(tvl, RDD.M, 1, 1);
						BigDecimal startTimeString = getValue(tvl, RDD.M, 1, 2);
						Date departureStartDate = getDate(new BigDecimal(startDateString), startTimeString);

						String endDateString = getValue(tvl, RDD.C, 1, 3);
						BigDecimal endTimeString = getValue(tvl, RDD.C, 1, 4);
						Date departureEndDate = null;
						if (endDateString != null && endTimeString != null) {
							departureEndDate = getDate(new BigDecimal(endDateString), endTimeString);
						} else if (endTimeString != null) {
							departureEndDate = getDate(new BigDecimal(startDateString), endTimeString);
						} else if (endDateString == null && endTimeString == null) {
							// If end date is not given we take the start date itself as the end date
							departureEndDate = departureStartDate;
						}

						tvlDetail.setDepartureTimeStart(departureStartDate);
						tvlDetail.setDepartureTimeEnd(departureEndDate);

						aviFlightSearchDTO.setSelectedDepatureDateTimeStart(departureStartDate);
						aviFlightSearchDTO.setSelectedDepatureDateTimeEnd(departureEndDate);

						// from and to airports
						String fromAirport = getValue(tvl, RDD.M, 2, 1);
						String toAirport = getValue(tvl, RDD.M, 3, 1);
						if (fromAirport == null) {
							// Invalid Place of Departure Code
							ERCDetail ercDetail = new ERCDetail(CodeSetEnum.CS9321.AppError_100, 3);
							tvlDto.addErcDetails(ercDetail);
						}

						if (toAirport == null) {
							// Invalid Place of Destination Code
							ERCDetail ercDetail = new ERCDetail(CodeSetEnum.CS9321.AppError_101, 3);
							tvlDto.addErcDetails(ercDetail);
						}
						tvlDetail.setFrom(fromAirport);
						tvlDetail.setTo(toAirport);

						// pdi processign . If present it will carry the rdb information
						PDI pdi = group4.getPDI();
						if (pdi != null) {
							List<PDI02> pdi02 = pdi.getPDI02();
							String bookingClass = getValue(pdi02, RDD.C, 1);
							tvlDetail.setBookingClass(bookingClass);
						}

						tvlDetail.setCarrierCode(carrierCode);
					}

					tvlDetail.setAdultCount(1);
					tvlDetail.setChildCount(0);
					tvlDetail.setInfantCount(0);
					tvlDetail.setPosAirport(pos == null ? AppSysParamsUtil.getHubAirport() : pos);
					tvlDto.setTvlDetail(tvlDetail);

					tvlDtos.add(tvlDto);
				}
			}

		}
		params.put(TypeACommandParamNames.TVL_DTOS, tvlDtos);
		params.put(TypeACommandParamNames.MESSAGE_FUNCTION, messageFunction);

		return params;
	}
}
