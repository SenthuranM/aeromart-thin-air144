package com.isa.thinair.gdsservices.core.typeA.transformers.paoreqres.v03;

import static com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum.CS1225.SeamlessAvailabilityResponse;
import iata.typea.v031.paores.GROUP2;
import iata.typea.v031.paores.GROUP3;
import iata.typea.v031.paores.IATA;
import iata.typea.v031.paores.ODI;
import iata.typea.v031.paores.PAORES;
import iata.typea.v031.paores.PDI;
import iata.typea.v031.paores.TVL;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.JAXBElement;

import com.isa.thinair.airinventory.api.dto.seatavailability.CabinClassAvailabilityDTO;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.core.typeA.helpers.Constants;

public class SeamlessAvailabilityResponseHandler {

	public static JAXBElement<?> handleResponce(DefaultServiceResponse response) throws InteractiveEDIException {

		Map<String, List<CabinClassAvailabilityDTO>> odiCcdtoMap = (Map<String, List<CabinClassAvailabilityDTO>>) response
				.getResponseParam(TypeACommandParamNames.CABIN_CLASS_AVAILABILITY_DTO_LIST);

		PaoresFactory factory = PaoresFactory.getInstance();
		PAORES paores = factory.getObjectFactory().createPAORES();
		paores.setUNH(factory.createUNH(null));
		paores.setMSG(factory.createMSG(SeamlessAvailabilityResponse.getValue()));

		for (Entry<String, List<CabinClassAvailabilityDTO>> entry : odiCcdtoMap.entrySet()) {
			GROUP2 group2 = factory.getObjectFactory().createGROUP2();
			{
				// ODI
				ODI odiL1 = factory.createODI();
				String odiL1Str = entry.getKey();
				if (odiL1Str != null && odiL1Str.length() > 0) {
					odiL1.getODI01().addAll(Arrays.asList(odiL1Str.split(Constants.Separators.SEGMENT_SEPARATOR)));
				}
				group2.setODI(odiL1);

				for (CabinClassAvailabilityDTO cabinClassAvailabilityDTO : entry.getValue()) {
					GROUP3 group3 = factory.getObjectFactory().createGROUP3();
					{
						// TVL
						Date departureDate = cabinClassAvailabilityDTO.getDepartureDateTimeStart();
						Date arrivalDate = cabinClassAvailabilityDTO.getDepartureDateTimeStart();
						String from = cabinClassAvailabilityDTO.getOrigin();
						String to = cabinClassAvailabilityDTO.getDestination();
						String carrier = cabinClassAvailabilityDTO.getFlightNumber().substring(0, 2);
						String productId = cabinClassAvailabilityDTO.getFlightNumber().substring(2);
						String sequenceNumber = cabinClassAvailabilityDTO.getSequenceNumber();
						String lineItemNumber = cabinClassAvailabilityDTO.getLineItemNumber();
						TVL tvlL2 = factory.createTVL(departureDate, arrivalDate, from, to, carrier, productId, sequenceNumber,
								lineItemNumber);
						group3.setTVL(tvlL2);

						// PDI
						PDI pdiL2 = factory.createPDI(cabinClassAvailabilityDTO.getCabinClassAvailability());
						group3.setPDI(pdiL2);
					}
					group2.getGROUP3().add(group3);
				}
			}

			paores.getGROUP2().add(group2);
		}

		paores.setUNT(factory.createUNT(null));

		IATA iata = factory.getObjectFactory().createIATA();
		iata.getUNBAndUNGAndPAORES().add(factory.createUNB(null, null));
		iata.getUNBAndUNGAndPAORES().add(paores);
		return factory.getObjectFactory().createIATA(iata);
	}

}
