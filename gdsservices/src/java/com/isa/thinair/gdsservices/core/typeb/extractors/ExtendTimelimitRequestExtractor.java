package com.isa.thinair.gdsservices.core.typeb.extractors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.SsrTicketingTimeLimitDTO;
import com.isa.thinair.gdsservices.api.dto.internal.ExtendTimelimitRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.SSRData;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

/**
 * @author Manoj
 */
public class ExtendTimelimitRequestExtractor extends ValidatorBase {
	
	private static final Log log = LogFactory.getLog(ExtendTimelimitRequestExtractor.class);

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {

		String pnrReference = ValidatorUtils.getPNRReference(bookingRequestDTO.getResponderRecordLocator());

		if (pnrReference.length() == 0) {
			String message = MessageUtil.getMessage("gdsservices.extractors.emptyPNRResponder");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);
			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		} else {
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
			return bookingRequestDTO;
		}
	}

	/**
	 * returns ExtendTimelimitRequest
	 * 
	 * @param gdsCredentialsDTO
	 * @param ssrData
	 * @param bookingRequestDTO
	 * @return
	 */
	public static ExtendTimelimitRequest getRequest(GDSCredentialsDTO gdsCredentialsDTO, SSRData ssrData,
			BookingRequestDTO bookingRequestDTO) {
		ExtendTimelimitRequest extendTimelimitRequest = new ExtendTimelimitRequest();

		extendTimelimitRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
		extendTimelimitRequest = (ExtendTimelimitRequest) RequestExtractorUtil
				.addBaseAttributes(extendTimelimitRequest, bookingRequestDTO);

		extendTimelimitRequest = addTimelimit(extendTimelimitRequest, ssrData);
		extendTimelimitRequest.setNotSupportedSSRExists(bookingRequestDTO.isNotSupportedSSRExists());

		return extendTimelimitRequest;
	}

	/**
	 * adds  Extend Timelimit info to the ExtendTimelimitRequest
	 * 
	 * @param ExtendTimelimitRequest
	 * @param ssrData
	 * @return
	 */
	private static ExtendTimelimitRequest addTimelimit(ExtendTimelimitRequest extendTimelimitRequest, SSRData ssrData) {

		SsrTicketingTimeLimitDTO ssrTicketingTimeLimitDto = ssrData.getTicketingTimeLimit();

		if (ssrTicketingTimeLimitDto != null
				&& GDSExternalCodes.SSRCodes.EXTEND_TIMELIMIT.getCode().equals(ssrTicketingTimeLimitDto.getCodeSSR())
				&& ssrTicketingTimeLimitDto.getActionOrStatusCode().equals(GDSExternalCodes.ActionCode.SOLD.getCode())) {
			extendTimelimitRequest.setTimeLimit(ssrTicketingTimeLimitDto.getTicketingTime());
		}
		return extendTimelimitRequest;
	}

}
