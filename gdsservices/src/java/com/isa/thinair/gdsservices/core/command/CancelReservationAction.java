package com.isa.thinair.gdsservices.core.command;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.internal.CancelReservationRequest;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.platform.api.ServiceResponce;

public class CancelReservationAction {
	private static Log log = LogFactory.getLog(CancelReservationAction.class);
	
	public static CancelReservationRequest processRequest(CancelReservationRequest cancelReservationRequest) {
		try {
			Reservation reservation = null;
			String originatorPnr = GDSApiUtils.maskNull(cancelReservationRequest.getOriginatorRecordLocator().getPnrReference());
			reservation = CommonUtil.loadReservationFromExternalLocator(originatorPnr);
			if (reservation == null) {
				String bookingOffice = cancelReservationRequest.getOriginatorRecordLocator().getBookingOffice();
				if (bookingOffice != null
						&& bookingOffice.length() > 2
						&& AppSysParamsUtil.getDefaultCarrierCode().equals(
								bookingOffice.substring(bookingOffice.length() - 2, bookingOffice.length()))) {
					reservation = CommonUtil.loadReservation(cancelReservationRequest.getGdsCredentialsDTO(), null,
							originatorPnr);
				} 
			}

			CommonUtil.validateReservation(cancelReservationRequest.getGdsCredentialsDTO(), reservation);
			boolean isFlownSegmentCancelling = CommonUtil.checkAlreadyFlownSegmentsExists(reservation,
					cancelReservationRequest.getSegments());
			if (isFlownSegmentCancelling) {
				throw new ModuleException("gdsservices.validators.segments.flown.cannot.cancel");
			}

			if (!reservation.getStatus().equals("CNX")) {
				CustomChargesTO customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
				TrackInfoDTO trackInfoDTO = CommonUtil.getTrackingInfo(cancelReservationRequest.getGdsCredentialsDTO());

				ServiceResponce serviceRes = GDSServicesModuleUtil.getReservationBD().cancelReservation(reservation.getPnr(),
						customChargesTO, reservation.getVersion(), false, false, false, trackInfoDTO, false, null,
						GDSInternalCodes.GDSNotifyAction.NO_ACTION.getCode(), null);

				if (serviceRes != null && serviceRes.isSuccess()) {
					cancelReservationRequest.setSuccess(true);
					cancelReservationRequest = setCancelledStatus(cancelReservationRequest);
				} else {
					cancelReservationRequest.setSuccess(false);
				}
			} else {
				cancelReservationRequest.setSuccess(false);
				cancelReservationRequest.setResponseMessage(MessageUtil.getMessage("gdsservices.response.message.invalidPnrCancel"));
				cancelReservationRequest.addErrorCode("gdsservices.response.message.invalidPnrCancel");
			}
		} catch (ModuleException e) {
			log.error(" CancelReservationAction Failed for GDS PNR "
					+ cancelReservationRequest.getOriginatorRecordLocator().getPnrReference(), e);
			cancelReservationRequest.setSuccess(false);
			cancelReservationRequest.setResponseMessage(e.getMessageString());
			cancelReservationRequest.addErrorCode(e.getExceptionCode());
		} catch (Exception e) {
			log.error(" CancelReservationAction Failed for GDS PNR "
					+ cancelReservationRequest.getOriginatorRecordLocator().getPnrReference(), e);
			cancelReservationRequest.setSuccess(false);
			cancelReservationRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
			cancelReservationRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
		}

		return cancelReservationRequest;
	}
	
	private static CancelReservationRequest setCancelledStatus(CancelReservationRequest cancelReservationRequest) {
		Collection<Segment> segments = cancelReservationRequest.getSegments();

		for (Segment segment : segments) {
			segment.setStatusCode(GDSInternalCodes.StatusCode.CANCELLED);
		}

		return cancelReservationRequest;
	}
	

}
