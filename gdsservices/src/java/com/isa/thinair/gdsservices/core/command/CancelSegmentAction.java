package com.isa.thinair.gdsservices.core.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.ReservationStatus;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.dto.internal.CancelSegmentRequest;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.platform.api.ServiceResponce;

public class CancelSegmentAction {
	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(CancelSegmentAction.class);

	public static CancelSegmentRequest processRequest(CancelSegmentRequest cancelSegmentRequest) {
		try {
			ServiceResponce serviceRes;
			Reservation reservation = CommonUtil.loadReservation(cancelSegmentRequest.getGdsCredentialsDTO(),
					cancelSegmentRequest.getOriginatorRecordLocator().getPnrReference(), null);

			CommonUtil.validateReservation(cancelSegmentRequest.getGdsCredentialsDTO(), reservation);
			boolean isFlownSegmentCancelling = CommonUtil.checkAlreadyFlownSegmentsExists(reservation,
					cancelSegmentRequest.getSegments());
			if (isFlownSegmentCancelling) {
				throw new ModuleException("gdsservices.validators.segments.flown.cannot.cancel");
			}

			Collection<Integer> colPnrSegIds = CommonUtil.extractPnrSegIDs(reservation, cancelSegmentRequest.getSegments());

			if (!AppSysParamsUtil.isRequoteEnabled()) {
				if (colPnrSegIds != null && colPnrSegIds.size() > 0) {
					CustomChargesTO customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
					serviceRes = GDSServicesModuleUtil.getReservationSegmentBD().cancelSegments(reservation.getPnr(),
							colPnrSegIds, customChargesTO, reservation.getVersion(), false, null, false, null, true);

					if (serviceRes != null && serviceRes.isSuccess()) {
						cancelSegmentRequest.setSuccess(true);
						cancelSegmentRequest = setCancelledStatus(cancelSegmentRequest);
					} else {
						cancelSegmentRequest.setSuccess(false);
					}
				} else {
					cancelSegmentRequest.setSuccess(false);
					cancelSegmentRequest.setResponseMessage(MessageUtil.getMessage("gdsservices.actions.invalidSegments"));
					cancelSegmentRequest.addErrorCode("gdsservices.actions.invalidSegments");
				}
			} else {
				if (colPnrSegIds != null && colPnrSegIds.size() > 0) {
					FlightPriceRQ flightPriceRQ = createFlightPriceRQ(cancelSegmentRequest, reservation);
					FlightPriceRS flightPriceRS = GDSServicesModuleUtil.getAirproxyReservationQueryBD().quoteFlightPrice(
							flightPriceRQ, null);

					if (flightPriceRS != null) {
						PriceInfoTO priceInfoTO = flightPriceRS.getSelectedPriceFlightInfo();

						if (priceInfoTO != null && priceInfoTO.getFareTypeTO() != null) {
							int ondLength = flightPriceRS.getOriginDestinationInformationList().size();
							boolean faresAvailable = false;
							for (int i = 0; i < ondLength; i++) {
								if (priceInfoTO.getFareTypeTO().getOndWiseFareType().get(i) == FareTypes.NO_FARE) {
									faresAvailable = false;
									break;
								}
								faresAvailable = true;
							}
							if (faresAvailable) {

								CustomChargesTO customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
								RequoteModifyRQ requoteModifyRQ = new RequoteModifyRQ();
								requoteModifyRQ.setPnr(reservation.getPnr());
								requoteModifyRQ.setCustomCharges(customChargesTO);
								requoteModifyRQ.setGroupPnr(reservation.getOriginatorPnr());
								requoteModifyRQ.setVersion(Long.toString(reservation.getVersion()));
								Set<String> removedSegmentIds = new HashSet<String>(colPnrSegIds.size());
								for (Integer segId : colPnrSegIds)
									removedSegmentIds.add(segId.toString());
								requoteModifyRQ.setRemovedSegmentIds(removedSegmentIds);

								QuotedFareRebuildDTO fareInfo = new QuotedFareRebuildDTO(priceInfoTO.getFareSegChargeTO(),
										flightPriceRQ, null);
								requoteModifyRQ.setFareInfo(fareInfo);
								requoteModifyRQ.setLastFareQuoteDate(priceInfoTO.getLastFareQuotedDate());
								requoteModifyRQ.setFQWithinValidity(priceInfoTO.isFQWithinValidity());
								requoteModifyRQ
										.setPaymentType(ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS);
								
								ReservationBalanceTO reservationBalanceTO = GDSServicesModuleUtil.getAirproxyReservationBD()
										.getRequoteBalanceSummary(requoteModifyRQ,
												CommonUtil.getTrackingInfo(cancelSegmentRequest.getGdsCredentialsDTO()));
								if (!reservationBalanceTO.hasBalanceToPay()) {
									requoteModifyRQ.setNoBalanceToPay(true);
								}
								
								serviceRes = GDSServicesModuleUtil.getReservationBD().requoteModifySegments(
										requoteModifyRQ, CommonUtil.getTrackingInfo(cancelSegmentRequest.getGdsCredentialsDTO()));
								if (serviceRes != null && serviceRes.isSuccess()) {
									cancelSegmentRequest.setSuccess(true);
									cancelSegmentRequest = setCancelledStatus(cancelSegmentRequest);
									Reservation modifiedReservation = CommonUtil.loadReservation(cancelSegmentRequest
											.getGdsCredentialsDTO(), cancelSegmentRequest.getOriginatorRecordLocator()
											.getPnrReference(), null);
									if (modifiedReservation.getStatus().equals(ReservationStatus.OHD.toString())) {
										String message = "";
										Date releaseTimeStamp = modifiedReservation.getReleaseTimeStamps()[0];
										if (GDSServicesModuleUtil.getConfig().isShowPaymentDetailsInTTYMessage()) {
											message = CommonUtil.getFormattedMessage(
													GDSInternalCodes.ResponseMessageCode.CONFIRMED_ON_HOLD,
													modifiedReservation.getLastCurrencyCode(),
													modifiedReservation.getTotalAvailableBalance(), releaseTimeStamp);
										} else {
											message = CommonUtil.getFormattedMessage(
													GDSInternalCodes.ResponseMessageCode.CONFIRMED_ON_HOLD_NO_PAYMENT_DETAILS,
													releaseTimeStamp);
										}
										if (cancelSegmentRequest.isNotSupportedSSRExists()) {
											message = message
													+ MessageUtil
															.getMessage(GDSInternalCodes.ResponseMessageCode.SSR_NOT_SUPPORTED
																	.getCode());
										}
										cancelSegmentRequest.setResponseMessage(message);

									}
								} else {
									cancelSegmentRequest.setSuccess(false);
									cancelSegmentRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
								}

							}
						}

					}
				}
			}

		} catch (ModuleException e) {
			log.error(" CancelSegmentAction Failed for GDS PNR "
					+ cancelSegmentRequest.getOriginatorRecordLocator().getPnrReference(), e);
			cancelSegmentRequest.setSuccess(false);
			cancelSegmentRequest.setResponseMessage(e.getMessageString());
			cancelSegmentRequest.addErrorCode(e.getExceptionCode());
		} catch (Exception e) {
			log.error(" CancelSegmentAction Failed for GDS PNR "
					+ cancelSegmentRequest.getOriginatorRecordLocator().getPnrReference(), e);
			cancelSegmentRequest.setSuccess(false);
			cancelSegmentRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
			cancelSegmentRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
		}

		return cancelSegmentRequest;
	}

	/**
	 * sets segment statuses
	 * 
	 * @param cancelSegmentRequest
	 * @return
	 */
	private static CancelSegmentRequest setCancelledStatus(CancelSegmentRequest cancelSegmentRequest) {
		Collection<Segment> segments = cancelSegmentRequest.getSegments();

		for (Segment segment : segments) {
			segment.setStatusCode(GDSInternalCodes.StatusCode.CANCELLED);
		}

		return cancelSegmentRequest;
	}

	/**
	 * Transform OTA Air price request to Air Proxy Request
	 * 
	 * @param otaAirPriceRQ
	 * @return
	 * @throws ModuleException
	 */
	private static FlightPriceRQ createFlightPriceRQ(CancelSegmentRequest cancelSegmentRequest, Reservation reservation)
			throws ModuleException {
		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();
		Collection<Segment> segments = cancelSegmentRequest.getSegments();
		Collection<ReservationSegmentDTO> segmentsNotCancelling = CommonUtil.extractNonCancelingSegments(reservation, segments);

		Collection<String> bookingClassSet = new HashSet<String>();

		LinkedHashMap<Integer, List<ReservationSegmentDTO>> segmentListMap = new LinkedHashMap<Integer, List<ReservationSegmentDTO>>();
		for (ReservationSegmentDTO bookFlightSegment : segmentsNotCancelling) {
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(bookFlightSegment.getStatus())) {

				if (segmentListMap.get(bookFlightSegment.getJourneySeq()) != null) {
					List<ReservationSegmentDTO> segmentList = segmentListMap.get(bookFlightSegment.getJourneySeq());
					segmentList.add(bookFlightSegment);
					Collections.sort(segmentList);
				} else {
					List<ReservationSegmentDTO> segmentList = new ArrayList<ReservationSegmentDTO>();
					segmentList.add(bookFlightSegment);
					segmentListMap.put(bookFlightSegment.getJourneySeq(), segmentList);
				}
			}
		}

		for (Entry<Integer, List<ReservationSegmentDTO>> entry : segmentListMap.entrySet()) {
			List<ReservationSegmentDTO> segList = entry.getValue();
			if (segList != null && segList.size() > 0) {
				OriginDestinationInformationTO depatureOnD = flightPriceRQ.addNewOriginDestinationInformation();
				ReservationSegmentDTO firstFlightSegment = BeanUtils.getFirstElement(segList);
				ReservationSegmentDTO lastFlightSegment = BeanUtils.getFirstElement(segList);
				depatureOnD.setDepartureDateTimeStart(firstFlightSegment.getDepartureDate());
				depatureOnD.setOrigin(firstFlightSegment.getOrigin());
				depatureOnD.setDestination(lastFlightSegment.getDestination());
				depatureOnD.setPreferredClassOfService(firstFlightSegment.getCabinClassCode());
				depatureOnD.setPreferredLogicalCabin(firstFlightSegment.getLogicalCCCode());
				if (firstFlightSegment.getFareTO() != null && firstFlightSegment.getFareTO().getBookingClassCode() != null) {
					depatureOnD.setPreferredBookingClass(firstFlightSegment.getFareTO().getBookingClassCode());
				}
				Date depatureDate = depatureOnD.getDepartureDateTimeStart();
				Date depatureDateTimeStart = CalendarUtil.getStartTimeOfDate(depatureDate);
				Date depatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(depatureDate);
				depatureOnD.setPreferredDate(depatureDate);
				depatureOnD.setDepartureDateTimeStart(depatureDateTimeStart);
				depatureOnD.setDepartureDateTimeEnd(depatureDateTimeEnd);
				depatureOnD.getExistingFlightSegIds().add(firstFlightSegment.getFlightSegId());
				depatureOnD.getExistingPnrSegRPHs().add(FlightRefNumberUtil.composePnrSegRPH(firstFlightSegment));
				depatureOnD.getOrignDestinationOptions().add(CommonUtil.setFlightSegmentTo(segList));
			}
		}

		CommonUtil.getTravelInfomationSummary(reservation, flightPriceRQ);
		AvailPreferencesTO availPref = flightPriceRQ.getAvailPreferences();
		CommonUtil.setAvailPreference(availPref, cancelSegmentRequest.getGdsCredentialsDTO());

		TravelPreferencesTO travelerPref = flightPriceRQ.getTravelPreferences();
		CommonUtil.setTravelPreference(bookingClassSet, travelerPref);

		return flightPriceRQ;
	}

}
