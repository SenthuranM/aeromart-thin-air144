package com.isa.thinair.gdsservices.core.remoting.ejb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airreservation.api.dto.CreateTicketInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.dto.TransitionTo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.gdsservices.api.dto.typea.common.Coupon;
import com.isa.thinair.gdsservices.api.dto.typea.init.ReIssueTicketRq;
import com.isa.thinair.gdsservices.api.dto.typea.init.ReIssueTicketRs;
import com.isa.thinair.gdsservices.api.dto.typea.init.RecallTicketCouponControlRq;
import com.isa.thinair.gdsservices.api.dto.typea.init.RecallTicketCouponControlRs;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketChangeOfStatusRQ;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketChangeOfStatusRS;
import com.isa.thinair.gdsservices.api.model.GdsEvent;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.service.bd.GdsRequiredBDImpl;
import com.isa.thinair.gdsservices.core.service.bd.GdsRequiredBDLocalImpl;
import com.isa.thinair.gdsservices.core.typeA.model.IataOperationConfig;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.platform.api.ServiceResponce;

@Stateless
@RemoteBinding(jndiBinding = "GdsRequiredService.remote")
@LocalBinding(jndiBinding = "GdsRequiredService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class GdsRequiredServiceBean extends PlatformBaseSessionBean implements GdsRequiredBDImpl, GdsRequiredBDLocalImpl {

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public RecallTicketCouponControlRs recallTicketCouponControl(RecallTicketCouponControlRq rq) {
		RecallTicketCouponControlRs rs = new RecallTicketCouponControlRs();

		rs.setSuccess(true);

		List<Integer> temp = new ArrayList<Integer>();
		for(Integer id : rq.getPpfsETicketIds()) {
			temp.add(id);
		}
		rs.setGrantedPpfsETicketIds(temp);

		return rs;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ReIssueTicketRs reIssueTicket(ReIssueTicketRq reIssueTicketRq) throws ModuleException {
		ReIssueTicketRs reIssueTicketRs;
		Map<Integer, Map<Integer, TransitionTo<Coupon>>> transitions;
		TransitionTo<Coupon> transitionTo;
		Coupon coupon;
		Reservation reservation;
		LCCClientReservation lccReservation;
		Map<String, Integer> segSeqAgainstFltRef;
		Integer segmentSeq;
		TicketChangeOfStatusRQ ticketChangeOfStatusRQ;
		Map<Integer, String> couponStatus;

		lccReservation = GDSServicesUtil.loadLccReservation(reIssueTicketRq.getPnr());

		// ----------------
		// TODO -- send TKCREQ: recall control
		// ----------------

		couponStatus = new HashMap<Integer, String>();
		for (LCCClientReservationPax pax : lccReservation.getPassengers()) {
			for (LccClientPassengerEticketInfoTO eticketInfoTO : pax.geteTickets()) {
				// TODO -- only if not flown
				couponStatus.put(eticketInfoTO.getPpfsId(), TypeAConstants.CouponStatus.EXCHANGED);

			}

		}
		// --------------------------------
		ticketChangeOfStatusRQ = new TicketChangeOfStatusRQ();
		ticketChangeOfStatusRQ.setOverrideStatus(true);
		ticketChangeOfStatusRQ.setRequestType(GdsEvent.Request.TICKET_REISSUE);
		ticketChangeOfStatusRQ.setCouponStatus(couponStatus);

		String commandName = IataOperationConfig.getMessageInitiatorCommand(IATAOperation.TKCREQ);
		Command command = (DefaultBaseCommand) GDSServicesModuleUtil.getModule().getLocalBean(commandName);
		command.setParameter(TypeACommandParamNames.TICKET_CONTROL_DTO, ticketChangeOfStatusRQ);
		command.setParameter(TypeACommandParamNames.IATA_OPERATION, IATAOperation.TKCREQ);
		command.setParameter(TypeACommandParamNames.IATA_EVENT, GdsEvent.Request.TICKET_REISSUE);
		ServiceResponce response = command.execute();
		// --------------------------------

		TicketChangeOfStatusRS resp = (TicketChangeOfStatusRS) response.getResponseParam(TypeACommandParamNames.TICKET_CONTROL_DTO);
		reIssueTicketRs = new ReIssueTicketRs();

		if (resp.isSuccess()) {
			reservation = GDSServicesUtil.loadReservation(reIssueTicketRq.getPnr());

			int gdsId = reservation.getGdsId();
			Gds gds = GDSServicesModuleUtil.getGdsBD().getGds(gdsId);
			User user = GDSServicesModuleUtil.getSecurityBD().getUserBasicDetails(gds.getUserId());


			CreateTicketInfoDTO tktInfoDto = new CreateTicketInfoDTO();
			tktInfoDto.setAppIndicator(AppIndicatorEnum.APP_GDS);
			tktInfoDto.setPaymentAgent(user.getAgentCode());

			// TODO -- LCC
			Map<Integer, Map<Integer, EticketDTO>> etickets = GDSServicesModuleUtil.getReservationBD()
					.generateIATAETicketNumbersAsSubstitution(reservation, tktInfoDto);
			Map<Integer, EticketDTO> paxEtickets;

			segSeqAgainstFltRef = new HashMap<String, Integer>();
			for (LCCClientReservationSegment seg : lccReservation.getSegments()) {
				segSeqAgainstFltRef.put(seg.getFlightSegmentRefNumber(), seg.getSegmentSeq());
			}

			transitions = new HashMap<Integer, Map<Integer, TransitionTo<Coupon>>>();
			for (LCCClientReservationPax pax : lccReservation.getPassengers()) {
				transitions.put(pax.getPaxSequence(), new HashMap<Integer, TransitionTo<Coupon>>());
				if (etickets.containsKey(pax.getPaxSequence())) {
					paxEtickets = etickets.get(pax.getPaxSequence());

					for (LccClientPassengerEticketInfoTO eticket : pax.geteTickets()) {
						segmentSeq = segSeqAgainstFltRef.get(eticket.getFlightSegmentRef());
						if (paxEtickets.containsKey(segmentSeq)) {
							transitionTo = new TransitionTo<Coupon>();
							coupon = new Coupon();
							coupon.setTicketNumber(eticket.getPaxETicketNo());
							coupon.setCouponNumber(eticket.getCouponNo());
							coupon.setExternalTicketNumber(eticket.getExternalPaxETicketNo());
							coupon.setExternalCouponNumber(eticket.getExternalCouponNo());
							transitionTo.setOldVal(coupon);

							coupon = new Coupon();
							coupon.setTicketNumber(paxEtickets.get(segmentSeq).getEticketNo());
							coupon.setCouponNumber(paxEtickets.get(segmentSeq).getCouponNo());
							transitionTo.setNewVal(coupon);

							transitions.get(pax.getPaxSequence()).put(segSeqAgainstFltRef.get(eticket.getFlightSegmentRef()), transitionTo);
						}
					}

				}
			}

			reIssueTicketRs.setSuccess(true);
			reIssueTicketRs.setTransitions(transitions);
			reIssueTicketRs.setGdsId(gdsId);

		} else {
			reIssueTicketRs.setSuccess(false);
		}


		return reIssueTicketRs;
	}
}
