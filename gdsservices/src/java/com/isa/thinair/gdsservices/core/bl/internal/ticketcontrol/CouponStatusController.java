package com.isa.thinair.gdsservices.core.bl.internal.ticketcontrol;


import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.dto.CouponStatusTransitionTo;

public abstract class CouponStatusController {

	public abstract void receiveCouponStatusTransition(CouponStatusTransitionTo receive);

	public abstract void validateSendCouponStatusTransition(CouponStatusTransitionTo send);

	public abstract void callbackSendCouponStatusTransition(CouponStatusTransitionTo reply);

	public static CouponStatusController getCouponStatusController(boolean isValidatingCarrier) {
		return isValidatingCarrier ? new ValidatingCarrierCouponStatusController() : new OperatingCarrierCouponStatusController();
	}

	public static boolean isFinalStatus(String status) {
		return status.equals(TypeAConstants.CouponStatus.EXCHANGED) ||
				status.equals(TypeAConstants.CouponStatus.FLOWN) ||
				status.equals(TypeAConstants.CouponStatus.PRINTED) ||
				status.equals(TypeAConstants.CouponStatus.REFUNDED) ||
				status.equals(TypeAConstants.CouponStatus.PRINT_EXCHANGED) ||
				status.equals(TypeAConstants.CouponStatus.VOIDED) ||
				status.equals(TypeAConstants.CouponStatus.CLOSED);
	}

	public static boolean isInterimStatus(String status) {
		return status.equals(TypeAConstants.CouponStatus.AIRPORT_CONTROL) ||
				status.equals(TypeAConstants.CouponStatus.CHECKED_IN) ||
				status.equals(TypeAConstants.CouponStatus.IRREGULAR_OPERATIONS) ||
				status.equals(TypeAConstants.CouponStatus.BOARDED) ||
				status.equals(TypeAConstants.CouponStatus.ORIGINAL_ISSUE) ||
				status.equals(TypeAConstants.CouponStatus.SUSPENDED);
	}
}
