package com.isa.thinair.gdsservices.core.typeb.transformers;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.internal.CheckOldSegmentsRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ProcessStatus;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;

public class CheckOldSegmentsResponseTransformer {
	/**
	 * Transforms reservationResponseMap back to bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param reservationResponseMap
	 * @return
	 */
	public static BookingRequestDTO getResponse(BookingRequestDTO bookingRequestDTO,
			Map<ReservationAction, GDSReservationRequestBase> reservationResponseMap) {

		CheckOldSegmentsRequest checkOldSegmentsRequest = (CheckOldSegmentsRequest) reservationResponseMap
				.get(ReservationAction.CHECK_OLD_SEGMENTS);

		if (checkOldSegmentsRequest.isSuccess()) {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_SUCCESS);
			bookingRequestDTO = setSegmentStatuses(bookingRequestDTO, checkOldSegmentsRequest);
			bookingRequestDTO = ValidatorUtils.addResponse(bookingRequestDTO, checkOldSegmentsRequest);
		} else {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_FAILURE);
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, checkOldSegmentsRequest);
		}
		bookingRequestDTO.getErrors().addAll(checkOldSegmentsRequest.getErrorCode());

		return bookingRequestDTO;
	}

	/**
	 * Adds segment statuses to the bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param checkOldSegmentsRequest
	 */
	private static BookingRequestDTO setSegmentStatuses(BookingRequestDTO bookingRequestDTO,
			CheckOldSegmentsRequest checkOldSegmentsRequest) {

		List<BookingSegmentDTO> bookingSegmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();
		Collection<Segment> segments = checkOldSegmentsRequest.getSegments();

		Iterator<BookingSegmentDTO> iterSegDTO = bookingSegmentDTOs.iterator();

		while (iterSegDTO.hasNext()) {
			BookingSegmentDTO bookingSegmentDTO = (BookingSegmentDTO) iterSegDTO.next();

			for (Segment segment : segments) {
				if (GDSApiUtils.isEqual(bookingSegmentDTO, segment)) {
					String gdsActionOrStatusCode = GDSApiUtils.maskNull(bookingSegmentDTO.getActionOrStatusCode());

					if (gdsActionOrStatusCode.equals(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode())
							|| gdsActionOrStatusCode.equals(GDSExternalCodes.StatusCode.CODESHARE_CONFIRMED.getCode())) {
						String adviceStatusCode = ResponseTransformerUtil.getSegmentStatus(segment,
								bookingSegmentDTO.getActionOrStatusCode());
						bookingSegmentDTO.setAdviceOrStatusCode(adviceStatusCode);
					}
				}
			}
		}

		return bookingRequestDTO;
	}
}
