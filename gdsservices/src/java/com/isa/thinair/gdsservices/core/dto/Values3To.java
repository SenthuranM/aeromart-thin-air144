package com.isa.thinair.gdsservices.core.dto;

public class Values3To<M, N, O> extends Values2To<M, N> {
	private O o;

	public O getO() {
		return o;
	}

	public void setO(O o) {
		this.o = o;
	}
}
