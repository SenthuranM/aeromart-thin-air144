package com.isa.thinair.gdsservices.core.bl.typeA.ticket;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.dto.GDSChargeAdjustmentRQ;
import com.isa.thinair.airproxy.api.dto.GDSPaxChargesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketsAssembler;
import com.isa.thinair.gdsservices.api.model.ExternalReservation;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.GdsTypeACodes;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.bl.internal.ticket.TicketIssuanceHandler;
import com.isa.thinair.gdsservices.core.bl.internal.ticket.TicketIssuanceHandlerImpl;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageHandler;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.dto.FinancialInformationAssembler;
import com.isa.thinair.gdsservices.core.dto.temp.GdsReservation;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerTicketingInformationWrapper;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAConcurrencyException;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSTypeAServiceDAO;
import com.isa.thinair.gdsservices.core.typeA.transformers.DTOsTransformerUtil;
import com.isa.thinair.gdsservices.core.typeA.transformers.ReservationDTOsTransformer;
import com.isa.thinair.gdsservices.core.typeA.transformers.ReservationDtoMerger;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.gdsservices.core.util.GdsDtoTransformer;
import com.isa.thinair.gdsservices.core.util.GdsInfoDaoHelper;
import com.isa.thinair.gdsservices.core.util.GdsUtil;
import com.isa.thinair.gdsservices.core.util.KeyGenerator;
import com.isa.thinair.gdsservices.core.util.TypeANotes;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.typea.common.ErrorInformation;
import com.isa.typea.common.Header;
import com.isa.typea.common.MessageFunction;
import com.isa.typea.common.MessageHeader;
import com.isa.typea.common.TaxDetails;
import com.isa.typea.common.TicketCoupon;
import com.isa.typea.common.TicketNumberDetails;
import com.isa.typea.common.TravellerTicketingInformation;
import com.isa.typea.tktreq.AATKTREQ;
import com.isa.typea.tktreq.TKTREQMessage;
import com.isa.typea.tktres.AATKTRES;
import com.isa.typea.tktres.TKTRESMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ReIssueTicket extends EdiMessageHandler {

	private static final Log log = LogFactory.getLog(ReIssueTicket.class);

	private AATKTREQ tktReq;
	private AATKTRES tktRes;
	private LCCClientReservation reservation;

	private GdsReservation<TravellerTicketingInformationWrapper> gdsReservation;

	protected void handleMessage() {
		TKTRESMessage tktresMessage;
		MessageHeader messageHeader;
		Header header;
		MessageFunction messageFunction;

		tktReq = (AATKTREQ)messageDTO.getRequest();
		TKTREQMessage tktreqMessage = tktReq.getMessage();

		tktRes = new AATKTRES();
		tktresMessage = new TKTRESMessage();

		messageHeader = GDSDTOUtil.createRespMessageHeader(tktreqMessage.getMessageHeader(),
				messageDTO.getRequestMetaDataDTO().getMessageReference(), messageDTO.getRequestMetaDataDTO().getIataOperation().getResponse());
		header = GDSDTOUtil.createRespEdiHeader(tktReq.getHeader(), messageHeader, messageDTO);

		tktRes.setHeader(header);
		tktresMessage.setMessageHeader(messageHeader);
		tktRes.setMessage(tktresMessage);

		messageDTO.setResponse(tktRes);

		try {

			preHandleMessage();
			handleReIssuance();
			postHandleMessage();


		} catch (GdsTypeAException e) {
			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(TypeAConstants.MessageFunction.EXCHANGE);
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			tktresMessage.setMessageFunction(messageFunction);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(e.getEdiErrorCode());
			tktresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);

			log.error("Error Occurred ----", e);

		} catch (Exception e) {
			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(TypeAConstants.MessageFunction.EXCHANGE);
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			tktresMessage.setMessageFunction(messageFunction);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR);
			tktresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);

			log.error("Error Occurred ----", e);

		}
	}

	private void preHandleMessage() throws GdsTypeAException {

		String pnr = DTOsTransformerUtil.getOwnPnr(tktReq.getMessage().getReservationControlInformation());

		try {
			GDSServicesModuleUtil.getGDSNotifyBD().reservationLock(pnr);
		} catch (ModuleException e) {
			throw new GdsTypeAConcurrencyException();
		}

		try {
			reservation = GDSServicesUtil.loadLccReservation(pnr);
		   	if (reservation == null) {
			    throw new ModuleException(TypeANotes.ErrorMessages.NO_MESSAGE);
		    }
		} catch (ModuleException e) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.NO_PNR_MATCH, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		GDSTypeAServiceDAO gdsTypeAServiceDAO = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO;
		ExternalReservation externalReservation = gdsTypeAServiceDAO.gdsTypeAServiceGet(ExternalReservation.class, pnr);
		if (externalReservation.getReservationSynced().equals(CommonsConstants.NO)) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.OPERATION_NOT_AUTHORIZED_FOR_PNR, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		gdsReservation =  GdsInfoDaoHelper.getGdsReservationTicketView(pnr);

		if (gdsReservation == null) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.NO_PNR_MATCH, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

	}

	private void handleReIssuance() throws GdsTypeAException {
		TKTRESMessage tktresMessage;
		TKTREQMessage tktreqMessage;
		MessageFunction messageFunction;
		MessageFunction reqMessageFunction;


		tktreqMessage = tktReq.getMessage();
		tktresMessage = tktRes.getMessage();

		reqMessageFunction = tktreqMessage.getMessageFunction();
		messageFunction = new MessageFunction();
		messageFunction.setMessageFunction(reqMessageFunction.getMessageFunction());
		messageFunction.setResponseType(TypeAConstants.ResponseType.PROCESSED_SUCCESSFULLY);
		tktresMessage.setMessageFunction(messageFunction);

		tktRes.setMessage(tktresMessage);

		LCCClientReservationPax pax;

		TicketsAssembler ticketsAssembler = new TicketsAssembler(TicketsAssembler.Mode.PAYMENT);
		ticketsAssembler.setPnr(reservation.getPNR());
		ticketsAssembler.setIpAddress(messageDTO.getRequestIp());


		handleDiscrepancies();

		for (TravellerTicketingInformation traveller : tktreqMessage.getTravellerTicketingInformation()) {
		    pax = GDSDTOUtil.resolveReservationPax(traveller, traveller.getTravellers().get(0), reservation);
			ticketReIssue(traveller, pax);
		}

	}

	private void ticketReIssue(TravellerTicketingInformation reIssueTraveller, LCCClientReservationPax pax)
			throws GdsTypeAException {

		String settlementAuthCode;

		TravellerTicketingInformation traveller;
		List<TicketNumberDetails> filteredTickets;
		List<TicketNumberDetails> newTickets;
		TicketNumberDetails oldTicketImage;
		TicketCoupon oldCouponImage;
		LccClientPassengerEticketInfoTO eticketInfoTO;

		TKTRESMessage tktresMessage = tktRes.getMessage();

		settlementAuthCode = KeyGenerator.generateSettlementAuthorizationCode();

		TravellerTicketingInformationWrapper travellerWrapperImage = GDSDTOUtil.resolvePassengerByTraveller(gdsReservation, reIssueTraveller);
		TravellerTicketingInformation travellerImage = travellerWrapperImage.getTravellerInformation();

		traveller = new TravellerTicketingInformation();
		traveller.setTravellerSurname(travellerImage.getTravellerSurname());
		traveller.setTravellerCount(travellerImage.getTravellerCount());
		traveller.setTravellerQualifier(travellerImage.getTravellerQualifier());
		traveller.getTravellers().addAll(travellerImage.getTravellers());

		traveller.getReservationControlInformation().addAll(gdsReservation.getReservationControlInformation());
		traveller.getMonetaryInformation().addAll(reIssueTraveller.getMonetaryInformation());
		traveller.getFormOfPayment().addAll(reIssueTraveller.getFormOfPayment());
		traveller.setPricingTicketingDetails(reIssueTraveller.getPricingTicketingDetails());
		traveller.setOriginDestinationInfo(reIssueTraveller.getOriginDestinationInfo());
		traveller.setOriginatorInformation(travellerWrapperImage.getOriginatorInformation());
		traveller.setTicketingAgentInformation(travellerWrapperImage.getTicketingAgentInformation());
		// TODO -- EQN
		traveller.getTaxDetails().addAll(travellerImage.getTaxDetails());
		traveller.getText().addAll(travellerImage.getText());

		filteredTickets = GDSDTOUtil.filterTicketNumbers(reIssueTraveller.getTickets(), GdsTypeACodes.TicketDataIndicator.OLD);
		newTickets = GDSDTOUtil.filterTicketNumbers(reIssueTraveller.getTickets(), GdsTypeACodes.TicketDataIndicator.NEW);

		for (TicketNumberDetails oldTicket : filteredTickets) {

			oldTicketImage = GDSDTOUtil.resolveTicket(travellerImage, oldTicket.getTicketNumber());
			TicketNumberDetails ticket = new TicketNumberDetails();
			ticket.setTicketNumber(oldTicketImage.getTicketNumber());
			ticket.setDocumentType(oldTicketImage.getDocumentType());
			ticket.setDataIndicator(TypeAConstants.TicketDataIndicator.NEW);
			traveller.getTickets().add(ticket);

			for (TicketCoupon oldCoupon : oldTicket.getTicketCoupon()) {

				eticketInfoTO = GdsUtil.lccPaxEticket(oldTicket, oldCoupon, pax.geteTickets());

				if (!eticketInfoTO.getSegmentStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
					if (! GDSDTOUtil.containsTicketCoupon(newTickets, eticketInfoTO)) {
						throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.EXISTING_ITINERARY_INCOMPATIBLE, TypeANotes.ErrorMessages.NO_MESSAGE);
					}
				}

				oldCouponImage = GDSDTOUtil.resolveCoupon(oldTicketImage, oldCoupon.getCouponNumber());
				oldCouponImage.setStatus(TypeAConstants.CouponStatus.EXCHANGED);
				oldCouponImage.setSettlementAuthCode(settlementAuthCode);

				ticket.getTicketCoupon().add(oldCouponImage);
			}
		}

		tktresMessage.getTravellerTicketingInformation().add(traveller);

	}

	private void postHandleMessage() throws GdsTypeAException {

		TKTREQMessage tktreqMessage = tktReq.getMessage();

		TravellerTicketingInformationWrapper oldTravellerWrapper;
		TravellerTicketingInformationWrapper newTravellerWrapper;

		TravellerTicketingInformation oldTravellerImage;
		TravellerTicketingInformation newTravellerImage;

		List<TicketNumberDetails> newTickets;

		List<TicketNumberDetails> oldTransitions;
		List<TicketNumberDetails> newTransitions;

		TicketNumberDetails ticketImage;
		TicketCoupon couponImage;

		for (TravellerTicketingInformation ticketTraveller : tktreqMessage.getTravellerTicketingInformation()) {
			oldTravellerWrapper = GDSDTOUtil.resolvePassengerByTraveller(gdsReservation, ticketTraveller);

			newTickets = new ArrayList<TicketNumberDetails>();

			if (oldTravellerWrapper != null) {
				oldTravellerWrapper.setOriginatorInformation(tktreqMessage.getOriginatorInformation());
				oldTravellerWrapper.setTicketingAgentInformation(tktreqMessage.getTicketingAgentInformation());

				oldTravellerImage = oldTravellerWrapper.getTravellerInformation();

				oldTransitions = new ArrayList<TicketNumberDetails>();
				newTransitions = new ArrayList<TicketNumberDetails>();

				for (TicketNumberDetails ticket : ticketTraveller.getTickets()) {

					TicketNumberDetails ticketNumberDetails = new TicketNumberDetails();
					ticketNumberDetails.setTicketNumber(ticket.getTicketNumber());
					ticketNumberDetails.setDocumentType(ticket.getDocumentType());

					ticketImage = GDSDTOUtil.resolveTicket(oldTravellerImage, ticket.getTicketNumber());
					if (ticketImage != null) {
						// old ticket
						ticketImage.setDataIndicator(ticket.getDataIndicator());
						oldTransitions.add(ticketNumberDetails);
					} else {
						// new ticket
						ticketImage = ticket;
						newTickets.add(ticket);
						newTransitions.add(ticketNumberDetails);
					}


					for (TicketCoupon coupon : ticket.getTicketCoupon()) {
						couponImage = GDSDTOUtil.resolveCoupon(ticketImage, coupon.getCouponNumber());

						TicketCoupon ticketCoupon = new TicketCoupon();
						ticketCoupon.setStatus(couponImage.getStatus());
						ticketCoupon.setCouponNumber(couponImage.getCouponNumber());
						ticketCoupon.setSettlementAuthCode(couponImage.getSettlementAuthCode());
						ticketCoupon.setOriginatorInformation(tktreqMessage.getOriginatorInformation());

						if (ticket.getDataIndicator().equals(TypeAConstants.TicketDataIndicator.OLD)) {
							ticketCoupon.setFlightInfomation(couponImage.getFlightInfomation());
							ticketCoupon.setRelatedProductInfo(couponImage.getRelatedProductInfo());
						} else if (ticket.getDataIndicator().equals(TypeAConstants.TicketDataIndicator.NEW)) {
							ticketCoupon.setFlightInfomation(coupon.getFlightInfomation());
							ticketCoupon.setRelatedProductInfo(coupon.getRelatedProductInfo());
						}

						ticketNumberDetails.getTicketCoupon().add(ticketCoupon);
					}

				}

				GDSDTOUtil.mergeCouponTransitions(oldTravellerWrapper.getCouponTransitions(), oldTransitions,
						tktreqMessage.getOriginatorInformation());



				newTravellerImage = new TravellerTicketingInformation();
				newTravellerImage.setTravellerSurname(ticketTraveller.getTravellerSurname());
				newTravellerImage.setTravellerCount(ticketTraveller.getTravellerCount());
				newTravellerImage.setTravellerQualifier(ticketTraveller.getTravellerQualifier());
				newTravellerImage.getTravellers().addAll(ticketTraveller.getTravellers());

				newTravellerImage.setTicketingAgentInformation(ticketTraveller.getTicketingAgentInformation());
				newTravellerImage.getReservationControlInformation().addAll(gdsReservation.getReservationControlInformation());

				newTravellerImage.getMonetaryInformation().addAll(GDSDTOUtil.generateMonetoryInfo(oldTravellerImage.getMonetaryInformation(),
						ticketTraveller.getMonetaryInformation(), ticketTraveller.getTaxDetails()));      // TODO

				newTravellerImage.getFormOfPayment().addAll(ticketTraveller.getFormOfPayment());
				newTravellerImage.setPricingTicketingDetails(ticketTraveller.getPricingTicketingDetails());
				newTravellerImage.setOriginDestinationInfo(ticketTraveller.getOriginDestinationInfo());
				newTravellerImage.setOriginatorInformation(ticketTraveller.getOriginatorInformation());
				// TODO -- EQN
				newTravellerImage.getTaxDetails().addAll(ticketTraveller.getTaxDetails());
				for (TaxDetails taxDetails : newTravellerImage.getTaxDetails()) {
					taxDetails.setIndicator(null);
				}
				newTravellerImage.getText().addAll(ticketTraveller.getText());

				newTickets = GDSDTOUtil.filterTicketNumbers(ticketTraveller.getTickets(), GdsTypeACodes.TicketDataIndicator.NEW);
				newTravellerImage.getTickets().addAll(newTickets);

				newTravellerWrapper = new TravellerTicketingInformationWrapper(newTravellerImage);
				newTravellerWrapper.setOriginatorInformation(tktreqMessage.getOriginatorInformation());
				newTravellerWrapper.setTicketingAgentInformation(tktreqMessage.getTicketingAgentInformation());

				gdsReservation.getTravellerInformation().add(newTravellerWrapper);

				GDSDTOUtil.mergeCouponTransitions(newTravellerWrapper.getCouponTransitions(), newTransitions,
						tktreqMessage.getOriginatorInformation());

			} else {
				throw new GdsTypeAException();
			}
		}


		GdsInfoDaoHelper.saveGdsReservationTicketView(reservation.getPNR(), gdsReservation);

		try {
			reservation = GDSServicesUtil.loadLccReservation(reservation.getPNR());
		} catch (ModuleException e) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.NO_PNR_MATCH, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		List<LccClientPassengerEticketInfoTO> lccCoupons = ReservationDtoMerger.getMergedETicketCoupons(tktReq.getMessage().getTravellerTicketingInformation(), reservation);
		List<EticketTO> coupons = ReservationDTOsTransformer.toETicketTos(lccCoupons);

		ServiceResponce serviceResponce;
		try {
			serviceResponce = GDSServicesModuleUtil.getReservationBD().updateExternalEticketInfo(coupons);
			if (!serviceResponce.isSuccess()) {
				throw new GdsTypeAException();
			}

		} catch (ModuleException e) {
			throw new GdsTypeAException();
		}

	}

	// TODO -- put to a common place and reuse with ticket-issue
	private void handleDiscrepancies()
			throws GdsTypeAException {

		TKTREQMessage tktreqMessage = tktReq.getMessage();
		LCCClientReservationPax pax;
		int pnrPaxId;

		TicketsAssembler ticketsAssembler = new TicketsAssembler(TicketsAssembler.Mode.PAYMENT);
		ticketsAssembler.setPnr(reservation.getPNR());
		ticketsAssembler.setIpAddress(messageDTO.getRequestIp());


		Map<Integer, GDSPaxChargesTO> paxAdjustment = new HashMap<Integer, GDSPaxChargesTO>();
		GDSPaxChargesTO paxChargesTO;

		GDSChargeAdjustmentRQ gdsChargeAdjustmentRQ = new GDSChargeAdjustmentRQ();
		gdsChargeAdjustmentRQ.setPnr(reservation.getPNR());
		gdsChargeAdjustmentRQ.setGroupPNR(reservation.isGroupPNR());
		gdsChargeAdjustmentRQ.setVersion(reservation.getVersion());
		gdsChargeAdjustmentRQ.setGdsChargesByPax(paxAdjustment);


		for (TravellerTicketingInformation traveller : tktreqMessage.getTravellerTicketingInformation()) {
			pax = GDSDTOUtil.resolveReservationPax(traveller, traveller.getTravellers().get(0), reservation);

			pnrPaxId = PaxTypeUtils.getPnrPaxId(pax.getTravelerRefNumber());

			FinancialInformationAssembler financialInformationAssembler = new FinancialInformationAssembler();
			financialInformationAssembler.setFinancialInformation(traveller.getText());
			financialInformationAssembler.setTaxDetails(traveller.getTaxDetails());
			financialInformationAssembler.setMonetaryInformation(traveller.getMonetaryInformation());
			financialInformationAssembler.calculateAmounts();

			if (!paxAdjustment.containsKey(pnrPaxId)) {
				paxChargesTO = new GDSPaxChargesTO();
				paxChargesTO.setPaxType(pax.getPaxType());
				paxChargesTO.setPnrPaxId(pnrPaxId);
				paxChargesTO.setFareAmount(BigDecimal.ZERO);
				paxChargesTO.setTaxAmount(BigDecimal.ZERO);
				paxChargesTO.setSurChargeAmount(BigDecimal.ZERO);

				paxAdjustment.put(pnrPaxId, paxChargesTO);
			}

			paxChargesTO = paxAdjustment.get(pnrPaxId);

			paxChargesTO.setFareAmount(paxChargesTO.getFareAmount().add(financialInformationAssembler.getFareAmount()));
			paxChargesTO.setTaxAmount(paxChargesTO.getTaxAmount().add(financialInformationAssembler.getTaxAmount()));
			paxChargesTO.setSurChargeAmount(paxChargesTO.getSurChargeAmount().add(financialInformationAssembler.getSurcharge()));

			GdsDtoTransformer.appendToTicketsAssembler(ticketsAssembler, traveller, pax);

		}

		try {
			ticketsAssembler.setGdsChargeAdjustmentRQ(gdsChargeAdjustmentRQ);

			TicketIssuanceHandler ticketIssuanceHandler = new TicketIssuanceHandlerImpl();
			ticketIssuanceHandler.handleMonetaryDiscrepancies(ticketsAssembler);
		} catch (ModuleException e) {
			throw new GdsTypeAException();
		}
	}


}
