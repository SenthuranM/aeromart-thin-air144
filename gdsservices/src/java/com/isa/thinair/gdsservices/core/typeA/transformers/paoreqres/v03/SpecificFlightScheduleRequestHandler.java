package com.isa.thinair.gdsservices.core.typeA.transformers.paoreqres.v03;

import static com.isa.thinair.gdsservices.core.util.TypeADataConvertorUtill.getDate;
import static com.isa.thinair.gdsservices.core.util.TypeADataConvertorUtill.getValue;
import iata.typea.v031.paoreq.ABI;
import iata.typea.v031.paoreq.GROUP1;
import iata.typea.v031.paoreq.GROUP3;
import iata.typea.v031.paoreq.GROUP4;
import iata.typea.v031.paoreq.ODI;
import iata.typea.v031.paoreq.ORG;
import iata.typea.v031.paoreq.PAOREQ;
import iata.typea.v031.paoreq.TVL;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.gdsservices.api.dto.typea.SpecificFlightDetailRequestDTO;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;

public class SpecificFlightScheduleRequestHandler {

	public static Map<String, Object> getOwnMessageParams(PAOREQ paoreq) {

		Map<String, Object> params = new HashMap<String, Object>();
		List<SpecificFlightDetailRequestDTO> flightDetailRequestDTOs = new ArrayList<SpecificFlightDetailRequestDTO>();
		if (paoreq.getGROUP1() != null) {
			GROUP1 group1 = paoreq.getGROUP1();
			ORG org = group1.getORG();
			List<ABI> abis = group1.getABI();
			// TODO do we need to consider this info at all ?
		}

		List<GROUP3> group3List = paoreq.getGROUP3();
		for (GROUP3 group3 : group3List) {
			// We are getting an empty ODI for this branch. Will remove later.
			ODI odi = group3.getODI();

			for (GROUP4 group4 : group3.getGROUP4()) {

				/*
				 * This is a specific flight schedule.requet will ask for a flight number in specific date. So the end
				 * date is ignored.
				 */

				// TVL begin.
				TVL tvl = group4.getTVL();
				Date specificDate = getDate(tvl.getTVL01().getTVL0101());
				String carrierCode = (String) getValue(tvl, "getTVL04().getTVL0401()");
				String flightNumber = (String) getValue(tvl, "getTVL05().getTVL0501()");
				flightNumber = carrierCode.concat(flightNumber);
				// TVL ends.

				SpecificFlightDetailRequestDTO flightDetailRequestDTO = new SpecificFlightDetailRequestDTO();
				flightDetailRequestDTO.setFlightNumber(flightNumber);
				flightDetailRequestDTO.setSecificDate(specificDate);
				flightDetailRequestDTOs.add(flightDetailRequestDTO);
			}
		}

		params.put(TypeACommandParamNames.SPECIFIC_FLIGHT_DETAIL_LIST, flightDetailRequestDTOs);
		return params;
	}

}
