package com.isa.thinair.gdsservices.core.util;

import java.util.regex.Pattern;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;

public abstract class KeyGenerator {

	private static final String SEQUENCE_FORMAT = "%0" + TypeAConstants.SEQUENCE_NUMBER_FIXED_LENGTH + "d";

    public static String generateRespCommonAccessRefKey()  {
	    String respKey = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO.generateRespCommonAccessRefKey();
        return respKey;
    }

	public static String getInterchangeControlRef(String commonAccessRef, String sequenceNo) {
		int seqNo = Integer.parseInt(sequenceNo);

		String[] senderInitiatorRefs = commonAccessRef.split(Pattern.quote(TypeAConstants.DELIM_INITIATOR_RESPONDER_KEY));

		String senderRef = senderInitiatorRefs[senderInitiatorRefs.length - 1];
		return senderRef + String.format(SEQUENCE_FORMAT, seqNo);
	}

	public static String getNextInterchangeControlRef(String value) {
		String constant = value.substring(0, value.length() - TypeAConstants.SEQUENCE_NUMBER_FIXED_LENGTH);
		String incremental = value.substring(value.length() - TypeAConstants.SEQUENCE_NUMBER_FIXED_LENGTH);
		int  seqNumber = Integer.parseInt(incremental) + 1;

		return constant + String.format(SEQUENCE_FORMAT, seqNumber);
	}

	public static String generateSettlementAuthorizationCode() {
		String part1 = String.format("%4s", AppSysParamsUtil.getDefaultAirlineIdentifierCode());
		String part2 = String.format("%010d", GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO.getSettlementAuthorizationKey());

		return (part1 + part2);

	}
}
