package com.isa.thinair.gdsservices.core.typeb.validators;

import java.util.List;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.MessageIdentifier;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

/**
 * @author Nilindra Fernando
 */
public class ValidateSegmentActionCodes extends ValidatorBase {

	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {
		List<BookingSegmentDTO> bookingSegmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();

		if (bookingSegmentDTOs != null && bookingSegmentDTOs.size() > 0) {
			for (BookingSegmentDTO bookingSegmentDTO : bookingSegmentDTOs) {
				String actionOrStatusCode = GDSApiUtils.maskNull(bookingSegmentDTO.getActionOrStatusCode());

				if (actionOrStatusCode.length() > 0) {
					String derivedActionOrStatusCode = ValidatorUtils.getSupportedActionOrStatus(actionOrStatusCode);

					if (derivedActionOrStatusCode == null) {
						if (bookingRequestDTO.getMessageIdentifier().equals(MessageIdentifier.SCHEDULE_CHANGE.getCode()) 
								|| bookingRequestDTO.getMessageIdentifier().equals(MessageIdentifier.LINK_RECORD_LOCATOR.getCode())) {
							derivedActionOrStatusCode = ValidatorUtils.getSupportedAdviceCode(actionOrStatusCode);
						}
						if (derivedActionOrStatusCode == null) {
							String message = MessageUtil
									.getMessage("gdsservices.validators.unsupported.segment.actionstatus.codes")
									+ " "
									+ bookingSegmentDTO.getActionOrStatusCode();
							bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

							this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
							return bookingRequestDTO;
						}
					}
				} else {
					String message = MessageUtil.getMessage("gdsservices.validators.empty.segment.actionstatus.codes");
					bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

					this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
					return bookingRequestDTO;
				}
			}

			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
			return bookingRequestDTO;
		} else {
			String message = MessageUtil.getMessage("gdsservices.validators.empty.booking.segment.information");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		}
	}
}
