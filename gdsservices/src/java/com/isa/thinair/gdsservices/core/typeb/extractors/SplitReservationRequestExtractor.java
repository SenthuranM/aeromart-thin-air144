package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.external.OSIDTO;
import com.isa.thinair.gdsservices.api.dto.external.RecordLocatorDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.internal.SplitReservationRequest;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.msgbroker.core.bl.TypeBMessageConstants;

/**
 * @author Nilindra Fernando
 * 
 *         Notes: 1. Modification for a split booking always happens for the new booking 2. Under split operation there
 *         will be passenger(s) specified in order to split
 */
public class SplitReservationRequestExtractor extends ValidatorBase {
	
	private static final String DEFAULT_DATA_SEPARATOR = " ";
	
	/**
	 * Extracts reservation actions need to perform for a split requests
	 * 
	 * @param gdsCredentialsDTO
	 * @param bookingRequestDTO
	 * @return
	 */
	public static LinkedHashMap<ReservationAction, GDSReservationRequestBase> getMap(GDSCredentialsDTO gdsCredentialsDTO,
			BookingRequestDTO bookingRequestDTO) {
		LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap = new LinkedHashMap<ReservationAction, GDSReservationRequestBase>();

		SplitReservationRequest splitResRequest = getRequest(gdsCredentialsDTO, bookingRequestDTO);
		requestMap.put(GDSInternalCodes.ReservationAction.SPLIT_RESERVATION, splitResRequest);

		LinkedHashMap<ReservationAction, GDSReservationRequestBase> modifyMap = ModifyReservationRequestExtractor.getMap(
				gdsCredentialsDTO, bookingRequestDTO);

		for (ReservationAction reservationAction : modifyMap.keySet()) {
			GDSReservationRequestBase gdsReservationRequestBase = modifyMap.get(reservationAction);
			requestMap.put(reservationAction, gdsReservationRequestBase);
		}

		return requestMap;
	}

	/**
	 * extracts the split reservation request
	 * 
	 * @param gdsCredentialsDTO
	 * @param bookingRequestDTO
	 * @return
	 */
	private static SplitReservationRequest getRequest(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO) {

		SplitReservationRequest splitResRequest = new SplitReservationRequest();
		splitResRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
		splitResRequest = (SplitReservationRequest) RequestExtractorUtil.addBaseAttributes(splitResRequest, bookingRequestDTO);

		splitResRequest = setPassengers(splitResRequest, bookingRequestDTO);

		return splitResRequest;
	}

	/**
	 * sets passengers to split
	 * 
	 * @param splitResRequest
	 * @param bookingRequestDTO
	 * @return
	 */
	private static SplitReservationRequest setPassengers(SplitReservationRequest splitResRequest,
			BookingRequestDTO bookingRequestDTO) {

		Collection<Passenger> passengers = new ArrayList<Passenger>();
		List<NameDTO> namesDTOs = bookingRequestDTO.getNewNameDTOs();

		for (Iterator<NameDTO> iterator = namesDTOs.iterator(); iterator.hasNext();) {
			NameDTO nameDTO = (NameDTO) iterator.next();
			Passenger passenger = RequestExtractorUtil.getPassenger(new Passenger(), nameDTO);

			passengers.add(passenger);
		}

		splitResRequest.setPassengers(passengers);

		return splitResRequest;
	}

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {

		String pnrReference = ValidatorUtils.getPNRReference(getResponderRecordLocator(bookingRequestDTO));

		if (pnrReference.length() == 0) {
			String message = MessageUtil.getMessage("gdsservices.extractors.emptyPNRResponder");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		} else {
			List<NameDTO> namesDTOs = bookingRequestDTO.getNewNameDTOs();

			if (namesDTOs == null || namesDTOs.size() == 0) {
				String message = MessageUtil.getMessage("gdsservices.extractors.split.params.missing");
				bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

				this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
				return bookingRequestDTO;
			} else {
				ValidatorBase validatorBase = new ModifyReservationRequestExtractor();
				bookingRequestDTO = validatorBase.validate(bookingRequestDTO, gdsCredentialsDTO);

				this.setStatus(validatorBase.getStatus());
				return bookingRequestDTO;
			}
		}
	}
	
	private RecordLocatorDTO getResponderRecordLocator(BookingRequestDTO bookingRequestDTO) {
		if (bookingRequestDTO.getResponderRecordLocator() == null) {
			Collection<OSIDTO> osiDTOs = bookingRequestDTO.getOsiDTOs();
			if (osiDTOs != null && osiDTOs.size() > 0) {
				String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();
				for (Iterator<OSIDTO> iterator = osiDTOs.iterator(); iterator.hasNext();) {
					OSIDTO osiDTO = (OSIDTO) iterator.next();
					if ((carrierCode.equals(GDSApiUtils.maskNull(osiDTO.getCarrierCode())) ||
								GDSApiUtils.maskNull(osiDTO.getCarrierCode()).equals(TypeBMessageConstants.CommonCodes.LIEU_AIRLINE_DESIGNATOR))
							&& osiDTO.getCodeOSI().equals("RLOC")) {
						if (osiDTO.getOsiValue() != null && osiDTO.getOsiValue().length() > 0) {
							String[] values = osiDTO.getOsiValue().split(DEFAULT_DATA_SEPARATOR);
							if (values.length >= 2) {
								String pnr = null;
								try {
									pnr = GDSServicesModuleUtil.getReservationQueryBD().getPnrByExtRecordLocator(values[1], null,
											false);
								} catch (Exception ex) {
									// Do nothing
								}
								if (pnr != null) {
									RecordLocatorDTO recordLocatorDTO = new RecordLocatorDTO();
									recordLocatorDTO.setPnrReference(pnr);
									bookingRequestDTO.setResponderRecordLocator(recordLocatorDTO);
								}
							}
						}
					}
				}
			}
		}
		return bookingRequestDTO.getResponderRecordLocator();
	}
}
