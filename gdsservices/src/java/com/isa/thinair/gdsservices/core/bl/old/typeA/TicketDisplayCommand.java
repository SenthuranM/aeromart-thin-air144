package com.isa.thinair.gdsservices.core.bl.old.typeA;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.api.util.TypeAResponseCode;
import com.isa.thinair.gdsservices.core.typeA.transformers.tkcreqres.v03.ITicketDisplayRequest;
import com.isa.thinair.gdsservices.core.typeA.transformers.tkcreqres.v03.TicketDisplayRequestCriteria;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @isa.module.command name="displayTicket.old"
 * 
 * @author sanjaya
 */
public class TicketDisplayCommand extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(TicketDisplayCommand.class);

	@Override
	public ServiceResponce execute() throws ModuleException {

		TicketDisplayRequestCriteria displayCriteria = (TicketDisplayRequestCriteria) this
				.getParameter(TypeACommandParamNames.TICKET_DISPLAY_SEARCH_CRITERIA);
		ITicketDisplayRequest displayRequest = (ITicketDisplayRequest) this.getParameter(TypeACommandParamNames.TICKET_SEARCH);

		log.debug("Searching ticket display for " + displayCriteria);

		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		responce.addResponceParam(TypeAResponseCode.TICKET_DISPLAY_RESULTS, displayRequest.search());

		log.debug("Search ticket display finished");

		return responce;
	}

}
