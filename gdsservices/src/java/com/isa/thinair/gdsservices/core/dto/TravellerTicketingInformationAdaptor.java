package com.isa.thinair.gdsservices.core.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import com.isa.thinair.commons.api.dto.ets.FlightInformation;
import com.isa.thinair.commons.api.dto.ets.InteractiveFreeText;
import com.isa.thinair.commons.api.dto.ets.Location;
import com.isa.thinair.commons.api.dto.ets.MonetaryInformation;
import com.isa.thinair.commons.api.dto.ets.TaxDetails;
import com.isa.thinair.commons.api.dto.ets.TicketCoupon;
import com.isa.thinair.commons.api.dto.ets.TicketNumberDetails;
import com.isa.thinair.commons.api.dto.ets.TaxDetails.Tax;
import com.isa.thinair.commons.api.dto.ets.Traveller;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.typea.common.TravellerTicketControlInformation;
import com.isa.typea.common.TravellerTicketingInformation;

public class TravellerTicketingInformationAdaptor {

	public TravellerTicketControlInformation adapt(com.isa.thinair.commons.api.dto.ets.TravellerTicketingInformation traveller)
			throws Exception {
		TravellerTicketControlInformation target = new TravellerTicketControlInformation();
		target.setTravellerSurname(traveller.getTravellerSurname());
		target.setTravellerCount(traveller.getTravellerCount());
		target.setTravellerQualifier(traveller.getTravellerQualifier());
		target.getTravellers().addAll(adaptTravellerDetails(traveller.getTravellers()));

		target.getTaxDetails().addAll(adaptTaxDetails(traveller.getTaxDetails()));
		target.getText().addAll(adaptInteractiveFreeText(traveller.getText()));
		target.getMonetaryInformation().addAll(adaptMonetaryInformation(traveller.getMonetaryInformation()));

		target.getTickets().addAll(adaptTickets(traveller.getTickets()));
		return target;
	}

	private List<com.isa.typea.common.InteractiveFreeText> adaptInteractiveFreeText(List<InteractiveFreeText> text) {
		List<com.isa.typea.common.InteractiveFreeText> targetList = new ArrayList<>();
		if (text != null && !text.isEmpty()) {
			for (InteractiveFreeText src : text) {
				com.isa.typea.common.InteractiveFreeText target = new com.isa.typea.common.InteractiveFreeText();
				target.setInformationType(src.getInformationType());
				target.setTextSubjectQualifier(src.getTextSubjectQualifier());
				target.setStatus(src.getStatus());
				target.getFreeText().addAll(src.getFreeText());

				targetList.add(target);
			}
		}

		return targetList;

	}

	private List<com.isa.typea.common.Traveller> adaptTravellerDetails(List<Traveller> text) {
		List<com.isa.typea.common.Traveller> targetList = new ArrayList<>();
		if (text != null && !text.isEmpty()) {
			for (Traveller src : text) {
				com.isa.typea.common.Traveller target = new com.isa.typea.common.Traveller();
				target.setTitle(src.getTitle());
				target.setGivenName(src.getGivenName());
				target.setPassengerType(src.getPassengerType());
				target.setTravellerReferenceNumber(src.getTravellerReferenceNumber());
				target.setHasInfant(src.getHasInfant());
				target.setOtherNames(src.getOtherNames());

				targetList.add(target);
			}
		}

		return targetList;

	}

	private List<com.isa.typea.common.TaxDetails> adaptTaxDetails(List<TaxDetails> text) {
		List<com.isa.typea.common.TaxDetails> targetList = new ArrayList<>();
		if (text != null && !text.isEmpty()) {
			for (TaxDetails src : text) {
				com.isa.typea.common.TaxDetails target = new com.isa.typea.common.TaxDetails();
				target.setIndicator(src.getIndicator());
				target.getTax().addAll(adaptTax(src.getTax()));

				targetList.add(target);
			}
		}

		return targetList;

	}

	private List<com.isa.typea.common.MonetaryInformation> adaptMonetaryInformation(List<MonetaryInformation> text) {
		List<com.isa.typea.common.MonetaryInformation> targetList = new ArrayList<>();
		if (text != null && !text.isEmpty()) {
			for (MonetaryInformation src : text) {
				com.isa.typea.common.MonetaryInformation target = new com.isa.typea.common.MonetaryInformation();

				target.setAmount(src.getAmount());
				target.setAmountTypeQualifier(src.getAmountTypeQualifier());
				target.setCurrencyCode(src.getCurrencyCode());
				target.setDepartureLocation(src.getDepartureLocation());
				target.setArrivalLocation(src.getArrivalLocation());

				targetList.add(target);
			}
		}

		return targetList;

	}

	private List<com.isa.typea.common.TaxDetails.Tax> adaptTax(List<Tax> tax) {
		List<com.isa.typea.common.TaxDetails.Tax> targetList = new ArrayList<>();
		if (tax != null && !tax.isEmpty()) {
			for (Tax taxInfo : tax) {
				com.isa.typea.common.TaxDetails.Tax targetTaxInfo = new com.isa.typea.common.TaxDetails.Tax();

				targetTaxInfo.setAmount(convertAndScaleAmount(taxInfo.getAmount()));
				targetTaxInfo.setCurrencyCode(taxInfo.getCurrencyCode());
				targetTaxInfo.setCountryCode(taxInfo.getCountryCode());
				targetTaxInfo.setType(taxInfo.getType());
				targetTaxInfo.setTaxFiledType(taxInfo.getTaxFiledType());
				targetTaxInfo.setTaxFiledAmount(convertAndScaleAmount(taxInfo.getTaxFiledAmount()));
				targetTaxInfo.setTaxFiledCurrencyCode(taxInfo.getTaxFiledCurrencyCode());
				targetTaxInfo.setFiledConversionRate(convertAndScaleAmount(taxInfo.getFiledConversionRate()));
				targetTaxInfo.setTaxQualifier(taxInfo.getTaxQualifier());

				targetList.add(targetTaxInfo);

			}
		}

		return targetList;

	}

	private List<com.isa.typea.common.TicketControlTicketNumberDetails> adaptTickets(List<TicketNumberDetails> ticketNumber) throws Exception {
		List<com.isa.typea.common.TicketControlTicketNumberDetails> targetList = new ArrayList<>();
		if (ticketNumber != null && !ticketNumber.isEmpty()) {
			for (TicketNumberDetails src : ticketNumber) {
				com.isa.typea.common.TicketControlTicketNumberDetails targetTicketNumber = new com.isa.typea.common.TicketControlTicketNumberDetails();

				targetTicketNumber.setTicketNumber(src.getTicketNumber());
				targetTicketNumber.setDocumentType(src.getDocumentType());
				targetTicketNumber.setTotalItems(src.getTotalItems());
				targetTicketNumber.setTicketNumber(src.getTicketNumber());
				targetTicketNumber.setDataIndicator(src.getDataIndicator());
				targetTicketNumber.getText().addAll(adaptInteractiveFreeText(src.getText()));

				targetTicketNumber.getTicketCoupon().addAll(adaptTicketCoupon(src.getTicketCoupon()));

				targetList.add(targetTicketNumber);
			}
		}

		return targetList;

	}

	private List<com.isa.typea.common.TicketCoupon> adaptTicketCoupon(List<TicketCoupon> ticketCoupon) throws Exception {
		List<com.isa.typea.common.TicketCoupon> targetList = new ArrayList<>();
		if (ticketCoupon != null && !ticketCoupon.isEmpty()) {
			for (TicketCoupon taxInfo : ticketCoupon) {
				com.isa.typea.common.TicketCoupon targetTicketCoupon = new com.isa.typea.common.TicketCoupon();
				targetTicketCoupon.setCouponNumber(taxInfo.getCouponNumber());
				targetTicketCoupon.setStatus(taxInfo.getStatus());
				targetTicketCoupon.setCouponValue(taxInfo.getCouponValue());
				targetTicketCoupon.setExchangeMedia(taxInfo.getExchangeMedia());
				targetTicketCoupon.setSettlementAuthCode(taxInfo.getSettlementAuthCode());
				targetTicketCoupon.setVoluntaryInvoluntaryIndicator(taxInfo.getVoluntaryInvoluntaryIndicator());
				targetTicketCoupon.setPreviousStatus(taxInfo.getPreviousStatus());
				targetTicketCoupon.setFlightInfomation(adaptFlightInformation(taxInfo.getFlightInformation()));

				targetList.add(targetTicketCoupon);

			}
		}

		return targetList;

	}

	private com.isa.typea.common.FlightInformation adaptFlightInformation(FlightInformation flightInformation) throws Exception {
		com.isa.typea.common.FlightInformation targetFlightInformation = null;
		if (flightInformation != null) {
			targetFlightInformation = new com.isa.typea.common.FlightInformation();

			targetFlightInformation.setDepartureDateTime(convertAsXMLGregorianCalendar(flightInformation.getDepartureDateTime()));
			targetFlightInformation.setArrivalDateTime(convertAsXMLGregorianCalendar(flightInformation.getArrivalDateTime()));

			targetFlightInformation.setDepartureLocation(adaptLocation(flightInformation.getDepartureLocation()));
			targetFlightInformation.setArrivalLocation(adaptLocation(flightInformation.getArrivalLocation()));
			targetFlightInformation.setFlightNumber(flightInformation.getFlightNumber());
			targetFlightInformation.setLineItemNumber(flightInformation.getLineItemNumber());
			targetFlightInformation.setTravellerCount(flightInformation.getTravellerCount());
			targetFlightInformation.setActionCode(flightInformation.getActionCode());
			targetFlightInformation.setSellType(flightInformation.getSellType());
			targetFlightInformation.setSegmentIdentifier(flightInformation.getSegmentIdentifier());

			targetFlightInformation.setCompanyInfo(adaptCompanyInfo(flightInformation.getCompanyInfo()));

		}

		return targetFlightInformation;

	}

	private com.isa.typea.common.Location adaptLocation(Location location) {
		com.isa.typea.common.Location targetLocation = null;
		if (location != null) {
			targetLocation = new com.isa.typea.common.Location();
			targetLocation.setLocationCode(location.getLocationCode());
			targetLocation.setLocationName(location.getLocationName());
		}

		return targetLocation;

	}

	private com.isa.typea.common.FlightInformation.CompanyInfo adaptCompanyInfo(FlightInformation.CompanyInfo companyInfo) {
		com.isa.typea.common.FlightInformation.CompanyInfo targetCompanyInfo = null;
		if (companyInfo != null) {
			targetCompanyInfo = new com.isa.typea.common.FlightInformation.CompanyInfo();
			targetCompanyInfo.setMarketingAirlineCode(companyInfo.getMarketingAirlineCode());
			targetCompanyInfo.setOperatingAirlineCode(companyInfo.getOperatingAirlineCode());
		}

		return targetCompanyInfo;

	}

	private BigDecimal convertAndScaleAmount(String amount) {

		if (!StringUtil.isNullOrEmpty(amount)) {
			return AccelAeroCalculator.getTwoScaledBigDecimalFromString(amount);
		}
		return AccelAeroCalculator.getDefaultBigDecimalZero();

	}

	private XMLGregorianCalendar convertAsXMLGregorianCalendar(String date) throws Exception {
		if (!StringUtil.isNullOrEmpty(date)) {
			return CalendarUtil.asXMLGregorianCalendar(CalendarUtil.getParsedTime(date, "yyyy-MM-dd'T'HH:mm:ss"));
		}
		return null;

	}
}
