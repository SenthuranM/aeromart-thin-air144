package com.isa.thinair.gdsservices.core.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airschedules.api.criteria.FlightSearchCriteria;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchDto;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchRespDto;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.dto.internal.AdviceScheduleChangeRequest;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Manoj
 */
public class TransferSegmentAction {

	private static Log log = LogFactory.getLog(TransferSegmentAction.class);

	public static AdviceScheduleChangeRequest processRequest(AdviceScheduleChangeRequest adviceScheduleChangeRequest) {
		try {
			ServiceResponce serviceResponce;
			
			Reservation reservation = null;
			String bookingOffice = adviceScheduleChangeRequest.getOriginatorRecordLocator().getBookingOffice();
			if (bookingOffice != null
					&& bookingOffice.length() > 2
					&& !AppSysParamsUtil.getDefaultCarrierCode().equals(
							bookingOffice.substring(bookingOffice.length() - 2, bookingOffice.length()))) {
				reservation = CommonUtil.loadReservationFromExternalLocator(adviceScheduleChangeRequest
						.getOriginatorRecordLocator().getPnrReference());
			} else {
				reservation = CommonUtil.loadReservation(adviceScheduleChangeRequest.getGdsCredentialsDTO(), null,
						adviceScheduleChangeRequest.getOriginatorRecordLocator().getPnrReference());
			}
			CommonUtil.validateReservationStatus(adviceScheduleChangeRequest.getGdsCredentialsDTO(), reservation);

			List<Segment> transferingSegments = new ArrayList<Segment>();
			transferingSegments.addAll(adviceScheduleChangeRequest.getSourceSegments());
			boolean isFlownSegmentCancelling = CommonUtil.checkAlreadyFlownSegmentsExists(reservation, transferingSegments);
			if (isFlownSegmentCancelling) {
				throw new ModuleException("gdsservices.validators.segments.flown.cannot.cancel");
			}

			List<Integer> colPnrSegIds = CommonUtil.extractPnrSegIDs(reservation, transferingSegments);
			List<FlightSegmentTO> targetFlightSegmentTOs = new ArrayList<FlightSegmentTO>();
			
			List<Segment> targetSegments = adviceScheduleChangeRequest.getTargetSegments();
			for (Segment targetSegment : targetSegments) {
				WildcardFlightSearchDto flightSearchDto = new WildcardFlightSearchDto();
				flightSearchDto.setDepartureDateExact(true);
				flightSearchDto.setArrivalDateExact(true);
				flightSearchDto.setFlightNumber(targetSegment.getFlightNumber());
				flightSearchDto.setDepartureDate(targetSegment.getDepartureDateTime());
				flightSearchDto.setArrivalDate(targetSegment.getArrivalDateTime());
				String[] values = targetSegment.getSegmentCode().split("/");
				flightSearchDto.setDepartureAirport(values[0]);
				flightSearchDto.setArrivalAirport(values[1]);
				flightSearchDto.setAgentCode(adviceScheduleChangeRequest.getGdsCredentialsDTO().getAgentCode());
				WildcardFlightSearchRespDto resp = GDSServicesModuleUtil.getFlightBD().searchFlightsWildcardBased(flightSearchDto);
				if (resp.getBestMatch() != null) {
					targetFlightSegmentTOs.add(resp.getBestMatch());
				} else {
					Date d = new Date();
					if (targetSegment.getDepartureDateTime().getYear() == d.getYear() + 1) {
						Date modifiedDepartureDateLocal = CalendarUtil.add(targetSegment.getDepartureDateTime(), -1, 0, 0, 0, 0, 0);
						flightSearchDto.setDepartureDate(modifiedDepartureDateLocal);
						Date modifiedArrivalDateLocal = CalendarUtil.add(targetSegment.getArrivalDateTime(), -1, 0, 0, 0, 0, 0);
						flightSearchDto.setArrivalDate(modifiedArrivalDateLocal);
						resp = GDSServicesModuleUtil.getFlightBD().searchFlightsWildcardBased(flightSearchDto);
						if (resp.getBestMatch() != null) {
							targetFlightSegmentTOs.add(resp.getBestMatch());
						}
					}					
				}
			}
			Collections.sort(targetFlightSegmentTOs);
			Map<Integer, Integer> pnrSegIdAndNewFlgSegId = new HashMap<Integer, Integer>(); // {12189810=200051}
			if (colPnrSegIds != null && !targetFlightSegmentTOs.isEmpty()) {
				int maxCount = colPnrSegIds.size() > targetFlightSegmentTOs.size() ? colPnrSegIds.size() : targetFlightSegmentTOs
						.size();
				for (int count = 0; count < maxCount; count++) {
					if (colPnrSegIds.size() == targetFlightSegmentTOs.size()) {
						pnrSegIdAndNewFlgSegId.put(colPnrSegIds.get(count), targetFlightSegmentTOs.get(count).getFlightSegId());
					} else if (colPnrSegIds.size() > targetFlightSegmentTOs.size()) {
						pnrSegIdAndNewFlgSegId.put(colPnrSegIds.get(count),
								targetFlightSegmentTOs.get(targetFlightSegmentTOs.size()).getFlightSegId());
					} else {
						pnrSegIdAndNewFlgSegId.put(colPnrSegIds.get(colPnrSegIds.size()), targetFlightSegmentTOs.get(count)
								.getFlightSegId());
					}
				}
			}
			if (!pnrSegIdAndNewFlgSegId.isEmpty()) {
				serviceResponce = GDSServicesModuleUtil.getReservationSegmentBD().transferReservationSegment(
						reservation.getPnr(), pnrSegIdAndNewFlgSegId, true, false, reservation.getVersion(), false, null, null, null);
				if (serviceResponce != null && serviceResponce.isSuccess()) {
					adviceScheduleChangeRequest.setSuccess(true);
				} else {
					adviceScheduleChangeRequest.setSuccess(false);
				}
			} else {
				adviceScheduleChangeRequest.setSuccess(false);
			}
		} catch (ModuleException e) {
			log.error(" TransferSegmentAction Failed for GDS PNR "
					+ adviceScheduleChangeRequest.getOriginatorRecordLocator().getPnrReference(), e);
			adviceScheduleChangeRequest.setSuccess(false);
			adviceScheduleChangeRequest.setResponseMessage(e.getMessageString());
			adviceScheduleChangeRequest.addErrorCode(e.getExceptionCode());
		} catch (Exception e) {
			log.error(" TransferSegmentAction Failed for GDS PNR "
					+ adviceScheduleChangeRequest.getOriginatorRecordLocator().getPnrReference(), e);
			adviceScheduleChangeRequest.setSuccess(false);
			adviceScheduleChangeRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
			adviceScheduleChangeRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
		}

		return adviceScheduleChangeRequest;
	}

}
