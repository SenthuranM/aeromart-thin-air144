package com.isa.thinair.gdsservices.core.util;

import org.apache.commons.lang.StringUtils;

/**
 * Identifies the fields in RET file
 */
public class RETFileDecoder {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String[] RETSections = { "IT01 File Header Record Layout",
				"IT02 Basic Sale Transaction Records",
				// "IT0S Stock Control Numbers Records",
				"IT03 Related Ticket/Document Information Records", "IT04 Additional Sale Information Records",
				"IT05 Monetary Amounts Records", "IT06 Itinerary Records", "IT07 Fare Calculation Records",
				"IT08 Form of Payment Records", "IT09 Additional Information Records" };

		String[][] RETLayouts = {
				{ "1,S,RecordIdentifier,RCID,M,1,AN,1", "2,S,SystemProviderReportingPeriodEndingDate,SPED,M,6,N,2",
						"3,S,ReportingSystemIdentifier,RPSI,M,4,AN,8", "4,S,HandbookRevisionNumber,REVN,M,3,N,12",
						"5,S,Test_ProductionStatus,TPST,M,4,AN,15", "6,S,ProcessingDate,PRDA,M,6,N,19",
						"7,S,ProcessingTime,TIME,M,4,N,25", "8,S,ISOCountryCode,ISOC,M,2,A,29",
						"9,,ReservedSpace,RESD,M,6,AN,31", "10,S,FileType,FTYP,C,1,AN,37",
						"11,S,FileTypeSequenceNumber,FTSN,C,2,AN,38", "12,,ReservedSpace,RESD,M,216,AN,40" },

				{
						"1,S,RecordIdentifier,RCID,M,1,AN,1",
						"2,S,TransactionNumber,TRNN,M,6,N,2",
						"3,S,AgentNumericCode,AGTN,M,8,N,8",
						"4,S,ConjunctionTicketIndicator,CJCP,C,3,AN,16",
						"5,S,CouponUseIndicator,CPUI,C,4,AN,19",
						"6,S,DateofIssue,DAIS,M,6,N,23",
						"7,S,StatisticalCode,STAT,C,3,AN,29",
						"8,S,Ticket_DocumentNumber,TDNR,C,15,AN,32",
						"9,S,CheckDigit,CDGT,C,1,N,47",
						"10,S,TransactionCode,TRNC,M,4,AN,48",
						"11,X,TransmissionControlNumber,TCNR,C,15,AN,52",
						"12,X,TransmissionControlNumberCheckDigit,TCND,C,1,N,67",
						"13,S,TicketingAirlineCodeNumber,TACN,C,5,AN,68",
						"14,S,CheckDigit,CDGT,C,1,N,73",
						"15,S,FormatIdentifier,FORM,C,1,AN,74",
						"16,S,PassengerName,PXNM,C,49,AN,75",
						"17,S,ApprovedLocationType,ALTP,C,1,AN,124",
						// "S,,StockControlNumberor,SCNR,C,16,AN",
						"18,S,StockControlNumberFrom,SCNF,C,16,AN,125", "19,S,StockControlNumberTo,SCNT,C,4,N,141",
						"20,S,ApprovedLocationNumericCode,ALNC,C,8,N,145", "21,S,ApprovedLocationType,ALTP,C,1,AN,153",
						"22,S,StockControlNumberFrom,SCNF,C,16,AN,154", "23,S,StockControlNumberTo,SCNT,C,4,N,170",
						"24,S,ApprovedLocationNumericCode,ALNC,C,8,N,174", "25,S,ApprovedLocationType,ALTP,C,1,AN,182",
						"26,S,StockControlNumberFrom,SCNF,C,16,AN,183", "27,S,StockControlNumberTo,SCNT,C,4,N,199",
						"28,S,ApprovedLocationNumericCode,ALNC,C,8,N,203", "29,S,ApprovedLocationType,ALTP,C,1,AN,211",
						"30,S,StockControlNumberFrom,SCNF,C,16,AN,212", "31,S,StockControlNumberTo,SCNT,C,4,N,228",
						"32,S,SettlementAuthorisationCode,ESAC,C,14,AN,232", "33,S,DataInputStatusIndicator,DISI,C,1,AN,246",
						"34,S,ISOCountryCode,ISOC,M,2,A,247", "35,S,VendorISOCountryCode,VISO,C,2,A,249",
						"36,S,VendorIdentification,VNID,C,4,AN,251", "37,,ReservedSpace,RESD,M,1,AN,255" },

				/*
				 * {"1,X,RecordIdentifier,RCID,M,1,AN,1", "2,X,TransactionNumber,TRNN,M,6,N,2",
				 * "3,S,ApprovedLocationNumericCode,ALNC,M,8,N,8", "4,S,ApprovedLocationType,ALTP,M,1,AN,16",
				 * "5,S,StockcontrolNumberFrom,SCNF,M,16,AN,17", "6,S,StockControlNumberTo,SCNT,M,4,N,33",
				 * "7,S,ApprovedLocationNumericCode,ALNC,C,8,N,37", "8,S,ApprovedLocationType,ALTP,C,1,AN,45",
				 * "9,S,StockControlNumberFrom,SCNF,C,16,AN,46", "10,S,StockControlNumberTo,SCNT,C,4,N,62",
				 * "11,S,ApprovedLocationNumericCode,ALNC,C,8,N,66", "12,S,ApprovedLocationType,ALTP,C,1,AN,74",
				 * "13,S,StockControlNumberFrom,SCNF,C,16,AN,75", "14,S,StockControlNumberTo,SCNT,C,4,N,91",
				 * "15,S,ApprovedLocationNumericCode,ALNC,C,8,N,95", "16,S,ApprovedLocationType,ALTP,C,1,AN,103",
				 * "17,S,StockControlNumberFrom,SCNF,C,16,AN,104", "18,S,StockControlNumberTo,SCNT,C,4,N,120",
				 * "19,S,ApprovedLocationNumericCode,ALNC,C,8,N,124", "20,S,ApprovedLocationType,ALTP,C,1,AN,132",
				 * "21,S,StockControlNumberFrom,SCNF,C,16,AN,133", "22,S,StockControlNumberTo,SCNT,C,4,N,149",
				 * "23,S,ApprovedLocationNumericCode,ALNC,C,8,N,153", "24,S,ApprovedLocationType,ALTP,C,1,AN,161",
				 * "25,S,StockControlNumberFrom,SCNF,C,16,AN,162", "26,S,StockControlNumberTo,SCNT,C,4,N,178",
				 * "27,S,ApprovedLocationNumericCode,ALNC,C,8,N,182", "28,S,ApprovedLocationType,ALTP,C,1,AN,190",
				 * "29,S,StockControlNumberFrom,SCNF,C,16,AN,191", "30,S,StockControlNumberTo,SCNT,C,4,N,207",
				 * "31,S,ApprovedLocationNumericCode,ALNC,C,8,N,211", "32,S,ApprovedLocationType,ALTP,C,1,AN,219",
				 * "33,S,StockControlNumberFrom,SCNF,C,16,AN,220", "34,S,StockControlNumberTo,SCNT,C,4,N,236",
				 * "35,,ReservedSpace,RESD,M,16,AN,240"},
				 */

				{ "1,S,RecordIdentifier,RCID,M,1,AN,1", "2,S,TransactionNumber,TRNN,M,6,N,2",
						"3,S,RelatedTicket_DocumentCouponNumberIdentifier,RCPN,C,4,N,8",
						"4,S,RelatedTicket_DocumentNumber,RTDN,M,15,AN,12", "5,S,CheckDigit,CDGT,M,1,N,27",
						"6,S,RelatedTicket_DocumentCouponNumberIdentifier,RCPN,C,4,N,28",
						"7,S,RelatedTicket_DocumentNumber,RTDN,C,15,AN,32", "8,S,CheckDigit,CDGT,C,1,N,47",
						"9,S,RelatedTicket_DocumentCouponNumberIdentifier,RCPN,C,4,N,48",
						"10,S,RelatedTicket_DocumentNumber,RTDN,C,15,AN,52", "11,S,CheckDigit,CDGT,C,1,N,67",
						"12,S,RelatedTicket_DocumentCouponNumberIdentifier,RCPN,C,4,N,68",
						"13,S,RelatedTicket_DocumentNumber,RTDN,C,15,AN,72", "14,S,CheckDigit,CDGT,C,1,N,87",
						"15,S,RelatedTicket_DocumentCouponNumberIdentifier,RCPN,C,4,N,88",
						"16,S,RelatedTicket_DocumentNumber,RTDN,C,15,AN,92", "17,S,CheckDigit,CDGT,C,1,N,107",
						"18,S,RelatedTicket_DocumentCouponNumberIdentifier,RCPN,C,4,N,108",
						"19,S,RelatedTicket_DocumentNumber,RTDN,C,15,AN,112", "20,S,CheckDigit,CDGT,C,1,N,127",
						"21,S,RelatedTicket_DocumentCouponNumberIdentifier,RCPN,C,4,N,128",
						"22,S,RelatedTicket_DocumentNumber,RTDN,C,15,AN,132", "23,S,Check-Digit,CDGT,C,1,N,147",
						"24,S,DateofIssueRelatedDocument,DIRD,C,6,N,148", "25,S,TourCode,TOUR,C,15,AN,154",
						"26,S,WaiverCode,WAVR,C,14,AN,169", "27,,ReservedSpace,RESD,M,73,AN,183" },

				{ "1,S,RecordIdentifier,RCID,M,1,AN,1", "2,S,TransactionNumber,TRNN,M,6,N,2",
						"3,X,PNRReferenceand/orAirlineData,PNRR,C,13,AN,8",
						"4,X,TrueOrigin/DestinationCityCodes,TODC,C,14,AN,21", "5,,ReservedSpace,RESD,M,4,AN,35",
						"6,X,TicketingModeIndicator,TKMI,M,1,AN,39", "7,X,OriginalIssueInformation,ORIN,C,32,AN,40",
						"8,S,TourCode,TOUR,C,15,AN,72", "9,X,Fare,FARE,M,11,AN,87", "10,X,EquivalentFarePaid,EQFR,C,11,AN,98",
						"11,X,Tax,TAXA,C,11,AN,109", "12,X,Tax,TAXA,C,11,AN,120", "13,X,Tax,TAXA,C,11,AN,131",
						"14,X,Total,TOTL,M,11,AN,142", "15,X,NeutralTicketingSystemIdentifier,NTSI,C,4,AN,153",
						"16,X,ServicingAirline/SystemProviderIdentifier,SASI,C,4,AN,157",
						"17,X,ClientIdentification,CLID,C,8,AN,161", "18,X,BookingAgentIdentification,BAID,C,6,AN,169",
						"19,X,PassengerSpecificData,PXDA,C,49,AN,175", "20,X,ValidatingLocationNumericCode,VLNC,C,8,N,224",
						"21,X,BookingAgency/LocationNumber,BOON,C,10,AN,232", "22,X,BookingEntityOutletType,BEOT,C,1,AN,242",
						"23,,ReservedSpace,RESD,M,13,AN,243" },

				{ "1,S,RecordIdentifier,RCID,M,1,AN,1", "2,S,TransactionNumber,TRNN,M,6,N,2",
						"3,S,AmountEnteredbyAgent,AEBA,C,11,N,8", "4,,ReservedSpace,RESD,M,11,N,19",
						"5,S,Tax/MiscellaneousFeeType,TMFT,C,8,AN,30", "6,S,Tax/MiscellaneousFeeAmount,TMFA,C,11,N,38",
						"7,S,Tax/MiscellaneousFeeType,TMFT,C,8,AN,49", "8,S,Tax/MiscellaneousFeeAmount,TMFA,C,11,N,57",
						"9,S,Tax/MiscellaneousFeeType,TMFT,C,8,AN,68", "10,S,Tax/MiscellaneousFeeAmount,TMFA,C,11,N,76",
						"11,S,Ticket/DocumentAmount,TDAM,M,11,N,87", "12,S,CurrencyType,CUTP,M,4,AN,98",
						"13,S,TaxonCommissionAmount,TOCA,C,11,N,102", "14,,ReservedSpace,RESD,M,1,AN,113",
						"15,S,Tax/MiscellaneousFeeType,TMFT,C,8,AN,114", "16,S,Tax/MiscellaneousFeeAmount,TMFA,C,11,N,122",
						"17,S,Tax/MiscellaneousFeeType,TMFT,C,8,AN,133", "18,S,Tax/MiscellaneouseeAmount,TMFA,C,11,N,141",
						"19,S,Tax/MiscellaneousFeeType,TMFT,C,8,AN,152", "20,S,Tax/MiscellaneousFeeAmount,TMFA,C,11,N,160",
						"21,S,CommissionType,COTP,C,6,AN,171", "22,S,CommissionRate,CORT,C,5,N,177",
						"23,S,CommissionAmount,COAM,C,11,N,182", "24,S,CommissionType,COTP,C,6,AN,193",
						"25,S,CommissionRate,CORT,C,5,N,199", "26,S,CommissionAmount,COAM,C,11,N,204",
						"27,S,CommissionType,COTP,C,6,AN,215", "28,S,CommissionRate,CORT,C,5,N,221",
						"29,S,CommissionAmount,COAM,C,11,N,226", "30,S,Net-ReportingIndicator,NRID,C,2,AN,237",
						"31,S,AmountPaidbyCustomer,APBC,C,11,N,239", "32,S,TaxonCommissionType,TCTP,C,6,AN,250" },

				{ "1,L,RecordIdentifier,RCID,M,1,AN,1", "2,L,TransactionNumber,TRNN,M,6,N,2",
						"3,L,OriginAirport/CityCode,ORAC,M,5,A,8", "4,L,DestinationAirport/CityCode,DSTC,M,5,A,13",
						"5,X,FrequentFlyerReference,FFRF,C,16,AN,18", "6,L,Carrier,CARR,C,4,AN,34",
						"7,,ReservedSpace,RESD,M,1,AN,38", "8,L,ReservationBookingDesignator,RBKD,M,2,AN,39",
						"9,L,FlightDate,FTDA,C,5,AN,41", "10,X,NotValidBeforeDate,NBDA,C,5,AN,46",
						"11,X,NotValidAfterDate,NADA,C,5,AN,51", "12,L,FareBasis/TicketDesignator,FBTD,M,15,AN,56",
						"13,L,FlightNumber,FTNR,C,5,AN,71", "14,X,FlightDepartureTime,FTDT,C,5,AN,76",
						"15,X,FreeBaggageAllowance,FBAL,C,3,AN,81", "16,X,FlightBookingStatus,FBST,C,2,A,84",
						"17,L,SegmentIdentifier,SEGI,M,1,N,86", "18,L,StopoverCode,STPO,M,1,A,87",
						"19,L,OriginAirport/CityCode,ORAC,M,5,A,88", "34,L,StopoverCode,STPO,M,1,A,167",
						"35,L,OriginAirport/CityCode,ORAC,M,5,A,168", "50,L,StopoverCode,STPO,M,1,A,247",
						"51,,ReservedSpace,RESD,M,8,AN,248" },

				{ "1,X,RecordIdentifier,RCID,M,1,AN,1", "2,X,TransactionNumber,TRNN,M,6,N,2",
						"3,X,FareCalculationArea,FRCA,C,87,AN,8", "4,X,FareCalculationSequenceNumber,FRCS,C,1,N,95",
						"5,X,FareCalculationArea,FRCA,C,87,AN,96", "6,X,FareCalculationSequenceNumber,FRCS,C,1,N,183",
						"7,X,FareCalculationModeIndicator,FCMI,M,1,AN,184", "8,,ReservedSpace,RESD,M,71,AN,185" },

				{ "1,S,RecordIdentifier,RCID,M,1,AN,1", "2,S,TransactionNumber,TRNN,M,6,N,2",
						"3,S,FormofPaymentAccountNumber,FPAC,C,19,AN,8", "4,S,FormofPaymentAmount,FPAM,M,11,N,27",
						"5,S,ApprovalCode,APLC,C,6,AN,38", "6,S,CurrencyType,CUTP,M,4,AN,44",
						"7,S,ExtendedPaymentCode,EXPC,C,2,AN,48", "8,S,FormofPaymentType,FPTP,M,10,AN,50",
						"9,S,ExpiryDate,EXDA,C,4,AN,60", "10,S,CustomerFileReference,CSTF,C,27,AN,64",
						"11,S,CreditCardCorporateContract,CRCC,C,1,AN,91", "12,S,AddressVerificationCode,AVCD,C,2,AN,92",
						"13,S,SourceofApprovalCode,SAPP,C,1,AN,94", "14,S,FormofPaymentTransactionInformation,FPTI,C,25,AN,95",
						"15,S,AuthorisedAmount,AUTA,C,11,AN,120", "16,S,FormofPaymentAccountNumber,FPAC,C,112,AN,131",
						"28,S,AuthorisedAmount,AUTA,C,11,AN,243", "29,,ReservedSpace,RESD,M,2,AN,254" },

				{ "1,X,RecordIdentifier,RCID,M,1,AN,1", "2,X,TransactionNumber,TRNN,M,6,N,2",
						"3,X,Endorsements/Restrictions,ENRS,C,147,AN,8", "4,X,FormofPaymentInformation,FPIN,M,50,AN,155",
						"5,X,FormofPaymentInformation,FPIN,M,50,AN,205", "6,,ReservedSpace,RESD,M,1,AN,255" } };

		String[] RETData = {
				"1091014WEBL201TEST0910150009WW                                                                                                                                                                                                                                 ",
				"2000001XXXXXXXX   FVVV091007D  9862100000001  1TKTT               0986  1XMARTIN/MARINA                                    A                000000000000                 000000000000                 000000000000                 0000               WW       ",
				"",
				"4000001ZZZZZ    /XB                   /                                               USD   70.00   00000000XT     8.40XT     1.50           USD   79.90NAME9999                                                               99999999          A             ",
				"500000100000000000           XT      00000000840XT      00000000150        0000000000000000007990USD200000000000         00000000000        00000000000        00000000000      0000000000000000      0000000000000000      0000000000000000  00000000000      ",
				"6000001YMQ  GVA                  XB   Y 15OCT     15OCTYOW            530  0930 PC OK1                                                                               0                                                                               0         ",
				"7000001YMQ XB GVA70.00USD70.00END                                                             1                                                                                       00                                                                       ",
				"8000001                   00000007990      USD2  CA                                                                    00000000000                   00000000000                                                                                               ",
				"9000001                                                                                                                                                   CASH                                                                                                 " };

		String[] sectionFields;
		String[] fieldsLayout;
		int fieldLength = -1;
		int fieldPosition = -1;

		try {
			for (int outter = 0; outter < RETSections.length; ++outter) {
				System.out.println("######## " + RETSections[outter]);
				System.out.println("######## " + RETData[outter]);
				sectionFields = RETLayouts[outter];
				for (int inner = 0; inner < RETData[outter].length(); ++inner) {
					fieldsLayout = sectionFields[inner].split(",");
					fieldLength = Integer.parseInt(fieldsLayout[5]);
					fieldPosition = Integer.parseInt(fieldsLayout[7]) - 1;
					System.out.println(fieldsLayout[2] + "="
							+ StringUtils.substring(RETData[outter], fieldPosition, fieldPosition + fieldLength));

					if (fieldPosition + fieldLength == 255) {
						break;
					}
				}
			}
		} catch (Exception e) {
			System.out.println("fieldPosition=" + fieldPosition + ", fieldLength=" + fieldLength);
			e.printStackTrace();
		}

	}

}
