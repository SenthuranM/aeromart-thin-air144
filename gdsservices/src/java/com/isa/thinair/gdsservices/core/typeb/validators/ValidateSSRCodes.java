package com.isa.thinair.gdsservices.core.typeb.validators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRChildDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRInfantDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRMinorDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSROTHERSDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

/**
 * @author Nilindra Fernando
 */
public class ValidateSSRCodes extends ValidatorBase {

	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {
		Collection<String> ssrCodes = extractSSRCodes(bookingRequestDTO);

		if (ssrCodes.size() > 0) {
			Map<String, String> ssrMap = GDSServicesDAOUtils.DAOInstance.GDSSERVICES_SUPPORT_JDBC_DAO.getSSRs();
			bookingRequestDTO = transform(bookingRequestDTO, ssrCodes, ssrMap);
		} else {
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
		}

		return bookingRequestDTO;
	}

	private BookingRequestDTO transform(BookingRequestDTO bookingRequestDTO, Collection<String> ssrCodes,
			Map<String, String> ssrMap) {
		if (ssrMap.keySet().containsAll(ssrCodes)) {
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
			return bookingRequestDTO;
		} else {
			ssrCodes.removeAll(ssrMap.keySet());

			String message = composeResponseMessage(ssrCodes);
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);
			bookingRequestDTO.getErrors().add("gdsservices.validators.specified.ssr.codes.notsupported");

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		}
	}

	private String composeResponseMessage(Collection<String> ssrCodes) {
		StringBuilder sb = new StringBuilder();
		for (String ssrCode : ssrCodes) {
			if (sb.length() == 0) {
				sb.append(ssrCode);
			} else {
				sb.append(", " + ssrCode);
			}
		}

		return MessageUtil.getMessage("gdsservices.validators.specified.ssr.codes.notsupported") + sb.toString();
	}

	private Collection<String> extractSSRCodes(BookingRequestDTO bookingRequestDTO) {
		List<SSRDTO> ssrDTOs = bookingRequestDTO.getSsrDTOs();
		Collection<String> ssrCodes = new ArrayList<String>();

		if (ssrDTOs != null && ssrDTOs.size() > 0) {
			String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();
			for (SSRDTO ssrdto : ssrDTOs) {
				if (carrierCode.equals(ssrdto.getCarrierCode())) {
					this.addSSRCode(ssrCodes, ssrdto);
				}
			}
		}

		return ssrCodes;
	}

	private void addSSRCode(Collection<String> ssrCodes, SSRDTO ssrdto) {
		// Children's and Infants are excluded from the Standard SSR List in AccelAero
		if (ssrdto instanceof SSRChildDTO || ssrdto instanceof SSRMinorDTO || ssrdto instanceof SSRInfantDTO || ssrdto instanceof SSROTHERSDTO) {
		} else {
			ssrCodes.add(ssrdto.getCodeSSR());
		}
	}
}
