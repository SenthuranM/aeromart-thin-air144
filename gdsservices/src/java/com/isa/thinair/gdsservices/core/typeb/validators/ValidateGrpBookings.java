package com.isa.thinair.gdsservices.core.typeb.validators;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

/**
 * @author Nilindra Fernando
 */
public class ValidateGrpBookings extends ValidatorBase {

	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {

		if (bookingRequestDTO.isGroupBooking()) {
			 String message = MessageUtil.getMessage("gdsservices.validators.groupbookings.notsupported");
			 bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);
			 this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			//this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
		} else {
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
		}

		return bookingRequestDTO;
	}
}
