package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.internal.AddSegmentRequest;
import com.isa.thinair.gdsservices.api.dto.internal.CreateReservationRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.SSRData;
import com.isa.thinair.gdsservices.api.dto.internal.SegmentDetail;
import com.isa.thinair.gdsservices.api.dto.internal.TempSegBcAllocDTO;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

public class AddSegmentRequestExtractor extends ValidatorBase {

	public static AddSegmentRequest getRequest(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO) {

		AddSegmentRequest addSegmentRequest = new AddSegmentRequest();
		SSRData ssrData = new SSRData(bookingRequestDTO.getSsrDTOs());

		addSegmentRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
		addSegmentRequest = (AddSegmentRequest) RequestExtractorUtil.addBaseAttributes(addSegmentRequest, bookingRequestDTO);

		addSegmentRequest = addNewSegments(addSegmentRequest, bookingRequestDTO);
		addSegmentRequest = addBlockSeatDetails(addSegmentRequest, bookingRequestDTO.getBlockedSeats());
		addSegmentRequest = addPaymentDetail(addSegmentRequest, ssrData);
		addSegmentRequest.setNotSupportedSSRExists(bookingRequestDTO.isNotSupportedSSRExists());

		return addSegmentRequest;
	}

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {

		String pnrReference = ValidatorUtils.getPNRReference(bookingRequestDTO.getResponderRecordLocator());

		if (pnrReference.length() == 0) {
			String message = MessageUtil.getMessage("gdsservices.extractors.emptyPNRResponder");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		} else {
			// There aren't any AddSegmentRequestExtractor specific validations.
			// The General Action codes are validated at the high level.
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
			return bookingRequestDTO;
		}
	}

	/**
	 * adds new segments to the AddSegmentRequest
	 * 
	 * @param addSegmentRequest
	 * @param bookingRequestDTO
	 * @return
	 */
	private static AddSegmentRequest addNewSegments(AddSegmentRequest addSegmentRequest, BookingRequestDTO bookingRequestDTO) {

		List<BookingSegmentDTO> segmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();
		Collection<SegmentDetail> segmentDetails = new ArrayList<SegmentDetail>();
		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();
		SegmentDetail segmentDetail = null;
		BookingSegmentDTO segmentDTO;
		String gdsActionCode;

		for (Iterator<BookingSegmentDTO> iterator = segmentDTOs.iterator(); iterator.hasNext();) {
			segmentDTO = (BookingSegmentDTO) iterator.next();

			if (carrierCode.equals(segmentDTO.getCarrierCode())) {
				gdsActionCode = segmentDTO.getActionOrStatusCode();

				if (gdsActionCode.equals(GDSExternalCodes.ActionCode.NEED.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.NEED_IFNOT_HOLDING.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.SOLD.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.SOLD_FREE.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.SOLD_IFNOT_HOLDING.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CODESHARE_SELL.getCode())) {

					segmentDetail = RequestExtractorUtil.getSegmentDetail(segmentDTO);
					segmentDetails.add(segmentDetail);
				}
			}
		}

		addSegmentRequest.setSegmentDetails(segmentDetails);

		return addSegmentRequest;
	}

	private static AddSegmentRequest addBlockSeatDetails(AddSegmentRequest addSegmentRequest,
			Collection<TempSegBcAllocDTO> blockedSeats) {
		if (blockedSeats != null && blockedSeats.size() > 0) {
			addSegmentRequest.setBlockedSeats(blockedSeats);
		}
		return addSegmentRequest;
	}


	/**
	 * adds credit card details to the AddSegmentRequest
	 * 
	 * @param addSegmentRequest
	 * @param ssrData
	 * @return
	 */
	private static AddSegmentRequest addPaymentDetail(AddSegmentRequest addSegmentRequest, SSRData ssrData) {
		addSegmentRequest.setPaymentDetail(SSRExtractorUtil.getPaymentDetail(ssrData));

		return addSegmentRequest;
	}
}
