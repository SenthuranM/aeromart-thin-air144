package com.isa.thinair.gdsservices.core.typeA.model;

import java.math.BigDecimal;

public class AirSegment {

	String airportFrom;
	String airportTo;
	BigDecimal fareAmount;
	String carrier;

	public AirSegment(String airportFrom, String airportTo, BigDecimal price, String carrier) {
		super();
		this.airportFrom = airportFrom;
		this.airportTo = airportTo;
		this.fareAmount = price;
		this.carrier = carrier;
	}

	/**
	 * @return the airportFrom
	 */
	public String getAirportFrom() {
		return airportFrom;
	}

	/**
	 * @param airportFrom
	 *            the airportFrom to set
	 */
	public void setAirportFrom(String airportFrom) {
		this.airportFrom = airportFrom;
	}

	/**
	 * @return the airportTo
	 */
	public String getAirportTo() {
		return airportTo;
	}

	/**
	 * @param airportTo
	 *            the airportTo to set
	 */
	public void setAirportTo(String airportTo) {
		this.airportTo = airportTo;
	}

	/**
	 * @return the fareAmount
	 */
	public BigDecimal getFareAmount() {
		return fareAmount;
	}

	/**
	 * @param fareAmount
	 *            the fareAmount to set
	 */
	public void setFareAmount(BigDecimal fareAmount) {
		this.fareAmount = fareAmount;
	}

	/**
	 * @return the carrier
	 */
	public String getCarrier() {
		return carrier;
	}

	/**
	 * @param carrier
	 *            the carrier to set
	 */
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	@Override
	public String toString() {
		return "AirSegment [airportFrom=" + airportFrom + ", airportTo=" + airportTo + ", price=" + fareAmount + ", carrier="
				+ carrier + "]";
	}

}