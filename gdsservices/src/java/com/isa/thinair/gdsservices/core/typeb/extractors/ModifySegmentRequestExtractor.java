package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.LinkedHashMap;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.AddSegmentRequest;
import com.isa.thinair.gdsservices.api.dto.internal.CancelSegmentRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.ModifySegmentRequest;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;

/**
 * 
 * @author Manoj Dhanushka
 */
public class ModifySegmentRequestExtractor extends ValidatorBase {
	
	public static LinkedHashMap<ReservationAction, GDSReservationRequestBase> getMap(
			LinkedHashMap<ReservationAction, GDSReservationRequestBase> currentMap, GDSCredentialsDTO gdsCredentialsDTO,
			BookingRequestDTO bookingRequestDTO) {

		if (AppSysParamsUtil.isRequoteEnabled()) {
			LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap = new LinkedHashMap<ReservationAction, GDSReservationRequestBase>();
			ModifySegmentRequest modifySegmentRequest = new ModifySegmentRequest();
			modifySegmentRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
			modifySegmentRequest = (ModifySegmentRequest) RequestExtractorUtil.addBaseAttributes(modifySegmentRequest,
					bookingRequestDTO);
			for (GDSInternalCodes.ReservationAction resAction : currentMap.keySet()) {
				if (resAction.equals(GDSInternalCodes.ReservationAction.ADD_SEGMENT)) {
					modifySegmentRequest.getNewSegmentDetails().addAll(
							((AddSegmentRequest) currentMap.get(GDSInternalCodes.ReservationAction.ADD_SEGMENT))
									.getSegmentDetails());
					modifySegmentRequest.setPaymentDetail(((AddSegmentRequest) currentMap
							.get(GDSInternalCodes.ReservationAction.ADD_SEGMENT)).getPaymentDetail());
					modifySegmentRequest.setNotSupportedSSRExists(((AddSegmentRequest) currentMap
							.get(GDSInternalCodes.ReservationAction.ADD_SEGMENT)).isNotSupportedSSRExists());
					modifySegmentRequest.setBlockedSeats(((AddSegmentRequest) currentMap.get(GDSInternalCodes.ReservationAction.ADD_SEGMENT))
						.getBlockedSeats());
				} else if (resAction.equals(GDSInternalCodes.ReservationAction.CANCEL_SEGMENT)) {
					modifySegmentRequest.setCancelingSegments(((CancelSegmentRequest) currentMap
							.get(GDSInternalCodes.ReservationAction.CANCEL_SEGMENT)).getSegments());
				}
			}
			requestMap.put(GDSInternalCodes.ReservationAction.MODIFY_SEGMENT, modifySegmentRequest);
			for (GDSInternalCodes.ReservationAction resAction : currentMap.keySet()) {
				if (!resAction.equals(GDSInternalCodes.ReservationAction.ADD_SEGMENT)
						&& !resAction.equals(GDSInternalCodes.ReservationAction.CANCEL_SEGMENT)) {
					requestMap.put(resAction, currentMap.get(resAction));
				}
			}
			return requestMap;
		} else {
			return currentMap;
		}
	}

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {
		// TODO Auto-generated method stub
		return null;
	}

}
