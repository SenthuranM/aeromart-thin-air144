package com.isa.thinair.gdsservices.core.typeb.transformers;

import java.util.Map;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.IssueEticketRequest;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ProcessStatus;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class IssueEticketResponseTransformer {

	private static final ReservationAction RESERVATION_ACTION = ReservationAction.ISSUE_ETICKET;

	/**
	 * Transforms reservationResponseMap back to bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param reservationResponseMap
	 * @return
	 */
	public static BookingRequestDTO getResponse(BookingRequestDTO bookingRequestDTO,
			Map<ReservationAction, GDSReservationRequestBase> reservationResponseMap) {

		IssueEticketRequest issueEticketRequest = (IssueEticketRequest) reservationResponseMap.get(RESERVATION_ACTION);

		if (issueEticketRequest.isSuccess()) {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_SUCCESS);
			bookingRequestDTO = ResponseTransformerUtil.setSSRStatus(bookingRequestDTO);
			bookingRequestDTO = ValidatorUtils.addResponse(bookingRequestDTO, issueEticketRequest);
			bookingRequestDTO.setSendResponse(issueEticketRequest.isSendResponse());
		} else {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_FAILURE);
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, issueEticketRequest);
		}

		return bookingRequestDTO;
	}

}
