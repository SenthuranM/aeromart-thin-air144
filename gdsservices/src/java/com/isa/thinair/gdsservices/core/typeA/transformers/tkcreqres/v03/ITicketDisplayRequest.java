package com.isa.thinair.gdsservices.core.typeA.transformers.tkcreqres.v03;

import java.io.Serializable;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.ReservationDisplayResult;

/**
 * Base interface for ticketDispay serach
 * 
 * @author malaka
 * 
 */
public interface ITicketDisplayRequest extends Serializable {

	ReservationDisplayResult search() throws ModuleException;

}
