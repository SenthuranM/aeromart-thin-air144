package com.isa.thinair.gdsservices.core.typeb.transformers;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.dto.internal.UpdateExternalSegmentsRequest;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ProcessStatus;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;

public class UpdateExternalSegmentsResponseTransformer {

	public static BookingRequestDTO getResponse(BookingRequestDTO bookingRequestDTO,
			Map<ReservationAction, GDSReservationRequestBase> reservationResponseMap) {

		UpdateExternalSegmentsRequest updateExternalSegmentsRequest = (UpdateExternalSegmentsRequest) reservationResponseMap
				.get(ReservationAction.UPDATE_EXTERNAL_SEGMENTS);

		if (updateExternalSegmentsRequest.isSuccess()) {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_SUCCESS);
			bookingRequestDTO = setSegmentStatuses(bookingRequestDTO, updateExternalSegmentsRequest);
			bookingRequestDTO = ResponseTransformerUtil.setSSRStatus(bookingRequestDTO);
		} else {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_FAILURE);
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, updateExternalSegmentsRequest);
		}
		bookingRequestDTO.getErrors().addAll(updateExternalSegmentsRequest.getErrorCode());

		return bookingRequestDTO;
	}

	/**
	 * Adds segment statuses to the bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param updateExternalSegmentsRequest
	 */
	private static BookingRequestDTO setSegmentStatuses(BookingRequestDTO bookingRequestDTO,
			UpdateExternalSegmentsRequest updateExternalSegmentsRequest) {

		List<BookingSegmentDTO> bookingSegmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();
		Collection<Segment> segmentDetails = updateExternalSegmentsRequest.getExternalSegments();

		Iterator<BookingSegmentDTO> iterSegDTO = bookingSegmentDTOs.iterator();

		while (iterSegDTO.hasNext()) {
			BookingSegmentDTO bookingSegmentDTO = (BookingSegmentDTO) iterSegDTO.next();

			for (Segment segment : segmentDetails) {

				if (GDSApiUtils.isEqual(bookingSegmentDTO, segment)) {
					String adviceStatusCode = ResponseTransformerUtil.getSegmentStatus(segment);
					bookingSegmentDTO.setAdviceOrStatusCode(adviceStatusCode);
				}
			}
		}

		return bookingRequestDTO;
	}
}
