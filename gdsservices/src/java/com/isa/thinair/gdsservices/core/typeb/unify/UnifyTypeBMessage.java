package com.isa.thinair.gdsservices.core.typeb.unify;

public interface UnifyTypeBMessage {
	public String unifyMessage(String rawMessage);
}
