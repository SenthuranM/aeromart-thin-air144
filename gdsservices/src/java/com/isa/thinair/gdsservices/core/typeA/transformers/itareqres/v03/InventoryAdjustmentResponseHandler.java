package com.isa.thinair.gdsservices.core.typeA.transformers.itareqres.v03;

import iata.typea.v031.itares.GROUP2;
import iata.typea.v031.itares.GROUP4;
import iata.typea.v031.itares.IATA;
import iata.typea.v031.itares.ITARES;
import iata.typea.v031.itares.ODI;
import iata.typea.v031.itares.RPI;
import iata.typea.v031.itares.TVL;

import javax.xml.bind.JAXBElement;

import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.api.dto.typea.ERCDetail;
import com.isa.thinair.gdsservices.api.dto.typea.InventoryAdjustmentResponseDTO;
import com.isa.thinair.gdsservices.api.dto.typea.ODIResponseDTO;
import com.isa.thinair.gdsservices.api.dto.typea.TVLDetail;
import com.isa.thinair.gdsservices.api.dto.typea.TVLDto;
import com.isa.thinair.gdsservices.api.util.TypeAResponseCode;

public class InventoryAdjustmentResponseHandler {

	public static JAXBElement<?> constructEDIResponce(DefaultServiceResponse response) {

		InventoryAdjustmentResponseDTO inventoryAdjustmentResponseDTO = (InventoryAdjustmentResponseDTO) response
				.getResponseParam(TypeAResponseCode.INVENTORY_ADJUSTMENT_RESPONSE);
		inventoryAdjustmentResponseDTO.rearangeERC();

		ItaresFactory factory = ItaresFactory.getInstance();
		ITARES itares = factory.createITARES();

		for (ERCDetail erc : inventoryAdjustmentResponseDTO.getErcDetails()) {
			factory.addERCIFT(itares, erc.getErrorCode().getValue(), erc.getErrorCode().getErrorText());
		}

		GROUP2 group2 = factory.getObjectfactory().createGROUP2();
		for (ODIResponseDTO odiResponse : inventoryAdjustmentResponseDTO.getOdiResponseDTOs()) {

			/* ODI */
			ODI odi = factory.createODI(odiResponse.getOdiFrom(), odiResponse.getOdiTo());
			group2.setODI(odi);

			/* GROUP3 ERC-IFT */
			for (ERCDetail ercift : odiResponse.getErcDetails()) {
				factory.addERCIFT(group2, ercift.getErrorCode().getValue(), ercift.getErrorCode().getErrorText());
			}

			/* Group 4 */
			for (TVLDto tvlResponse : odiResponse.getTvlResponses()) {
				GROUP4 group4 = factory.getObjectfactory().createGROUP4();

				TVLDetail tvlDetail = tvlResponse.getTvlDetail();
				TVL tvl = factory.createTVL(tvlDetail.getDepartureTimeStart(), tvlDetail.getArrivalTime(), tvlDetail.getFrom(),
						tvlDetail.getTo(), tvlDetail.getFlightNumber().substring(0, 2), tvlDetail.getFlightNumber().substring(2));
				RPI rpi = factory.createRPI(tvlResponse.getPaxCount(), tvlResponse.getResultCode().getValue());
				group4.setTVL(tvl);
				group4.setRPI(rpi);

				for (ERCDetail ercDetail : tvlResponse.getErcDetails()) {
					factory.addERCIFT(group4, ercDetail.getErrorCode().getValue(), ercDetail.getErrorCode().getErrorText());
				}
				group2.getGROUP4().add(group4);
			}
		}
		itares.getGROUP2().add(group2);

		IATA iata = factory.getObjectfactory().createIATA();
		iata.getUNBAndUNGAndITARES().add(factory.createUNB());
		iata.getUNBAndUNGAndITARES().add(itares);
		return factory.getObjectfactory().createIATA(iata);
	}
}
