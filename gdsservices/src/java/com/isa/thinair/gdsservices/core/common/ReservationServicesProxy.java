package com.isa.thinair.gdsservices.core.common;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.ejb.SessionContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.AddInfantRequest;
import com.isa.thinair.gdsservices.api.dto.internal.AddSegmentRequest;
import com.isa.thinair.gdsservices.api.dto.internal.AddSsrRequest;
import com.isa.thinair.gdsservices.api.dto.internal.AdviceScheduleChangeRequest;
import com.isa.thinair.gdsservices.api.dto.internal.CancelReservationRequest;
import com.isa.thinair.gdsservices.api.dto.internal.CancelSegmentRequest;
import com.isa.thinair.gdsservices.api.dto.internal.ChangeContactDetailRequest;
import com.isa.thinair.gdsservices.api.dto.internal.ChangePaxDetailsRequest;
import com.isa.thinair.gdsservices.api.dto.internal.CheckOldSegmentsRequest;
import com.isa.thinair.gdsservices.api.dto.internal.ConfirmBookingRequest;
import com.isa.thinair.gdsservices.api.dto.internal.CreateReservationRequest;
import com.isa.thinair.gdsservices.api.dto.internal.CreateReservationResponse;
import com.isa.thinair.gdsservices.api.dto.internal.ExchangeEticketRequest;
import com.isa.thinair.gdsservices.api.dto.internal.ExtendTimelimitRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.IssueEticketRequest;
import com.isa.thinair.gdsservices.api.dto.internal.LocateReservationRequest;
import com.isa.thinair.gdsservices.api.dto.internal.MakePaymentRequest;
import com.isa.thinair.gdsservices.api.dto.internal.ModifySegmentRequest;
import com.isa.thinair.gdsservices.api.dto.internal.RemovePaxRequest;
import com.isa.thinair.gdsservices.api.dto.internal.RemoveSsrRequest;
import com.isa.thinair.gdsservices.api.dto.internal.ReservationResponseProcessRequest;
import com.isa.thinair.gdsservices.api.dto.internal.SplitReservationRequest;
import com.isa.thinair.gdsservices.api.dto.internal.SplitReservationResponse;
import com.isa.thinair.gdsservices.api.dto.internal.UpdateExternalSegmentsRequest;
import com.isa.thinair.gdsservices.api.dto.internal.UpdateStatusRequest;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.core.command.AddInfantAction;
import com.isa.thinair.gdsservices.core.command.AddSegmentAction;
import com.isa.thinair.gdsservices.core.command.AddSsrAction;
import com.isa.thinair.gdsservices.core.command.CancelReservationAction;
import com.isa.thinair.gdsservices.core.command.CancelSegmentAction;
import com.isa.thinair.gdsservices.core.command.ChangeContactDetailsAction;
import com.isa.thinair.gdsservices.core.command.ChangePaxDetailsAction;
import com.isa.thinair.gdsservices.core.command.CheckOldSegmentsAction;
import com.isa.thinair.gdsservices.core.command.ConfirmBookingAction;
import com.isa.thinair.gdsservices.core.command.CreateReservationAction;
import com.isa.thinair.gdsservices.core.command.CreateReservationResponseAction;
import com.isa.thinair.gdsservices.core.command.ExchangeEticketAction;
import com.isa.thinair.gdsservices.core.command.ExtendTimelimitAction;
import com.isa.thinair.gdsservices.core.command.IssueEticketAction;
import com.isa.thinair.gdsservices.core.command.LocateReservationAction;
import com.isa.thinair.gdsservices.core.command.MakePaymentAction;
import com.isa.thinair.gdsservices.core.command.ModifySegmentAction;
import com.isa.thinair.gdsservices.core.command.RemovePaxAction;
import com.isa.thinair.gdsservices.core.command.RemoveSsrAction;
import com.isa.thinair.gdsservices.core.command.ReservationResponseProcessAction;
import com.isa.thinair.gdsservices.core.command.SplitReservationAction;
import com.isa.thinair.gdsservices.core.command.SplitReservationResponseAction;
import com.isa.thinair.gdsservices.core.command.TransferSegmentAction;
import com.isa.thinair.gdsservices.core.command.UpdateExternalSegmentAction;
import com.isa.thinair.gdsservices.core.command.UpdateStatusAction;
import com.isa.thinair.gdsservices.core.typeb.decorators.BookingRequestDTODecoratorProxy;
import com.isa.thinair.gdsservices.core.typeb.extractors.ReservationRequestExtractorProxy;
import com.isa.thinair.gdsservices.core.typeb.transformers.BookingRequestDTOTransformerProxy;
import com.isa.thinair.gdsservices.core.typeb.validators.BookingRequestDTOValidatorProxy;

/**
 * @author Nilindra Fernando
 */
public class ReservationServicesProxy {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ReservationServicesProxy.class);

	private SessionContext sessionContext;

	public ReservationServicesProxy(SessionContext sessionContext) {
		this.sessionContext = sessionContext;
	}

	/**
	 * Process the request
	 * 
	 * @param gdsCredentialsDTO
	 * @param bookingRequestDTO
	 * @return
	 * @throws ModuleException
	 */
	public BookingRequestDTO processRequest(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO) {
		bookingRequestDTO = BookingRequestDTOValidatorProxy.validate(gdsCredentialsDTO, bookingRequestDTO);

		if (bookingRequestDTO.getProcessStatus() == GDSExternalCodes.ProcessStatus.VALIDATE_SUCCESS) {
			LinkedHashMap<ReservationAction, GDSReservationRequestBase> reservationRequestMap = ReservationRequestExtractorProxy
					.extract(gdsCredentialsDTO, bookingRequestDTO);
			LinkedHashMap<ReservationAction, GDSReservationRequestBase> reservationResponseMap = processActions(reservationRequestMap);

			// We need to roll back cause if any error happens
			// it is wrong to save invalid data to the system
			performRollbackBeforeTransformation(reservationRequestMap, reservationResponseMap);
			bookingRequestDTO = BookingRequestDTOTransformerProxy.transform(reservationRequestMap, bookingRequestDTO,
					reservationResponseMap);
		}

		return BookingRequestDTODecoratorProxy.decorate(bookingRequestDTO);
	}

	/**
	 * Performs the rollback before the actual transformation
	 * 
	 * @param reservationRequestMap
	 * @param reservationResponseMap
	 */
	private void performRollbackBeforeTransformation(Map<ReservationAction, GDSReservationRequestBase> reservationRequestMap,
			Map<ReservationAction, GDSReservationRequestBase> reservationResponseMap) {

		if (reservationRequestMap.size() != reservationResponseMap.size()) {
			this.sessionContext.setRollbackOnly();
		} else {
			boolean isAllSuccess = true;
			for (GDSReservationRequestBase base : reservationResponseMap.values()) {
				if (!base.isSuccess()) {
					isAllSuccess = false;
					break;
				}
			}

			if (!isAllSuccess) {
				this.sessionContext.setRollbackOnly();
			}
		}
	}

	/**
	 * Processes the actions
	 * 
	 * @param reservationRequestMap
	 * @return
	 */
	private LinkedHashMap<ReservationAction, GDSReservationRequestBase> processActions(
			Map<GDSInternalCodes.ReservationAction, GDSReservationRequestBase> reservationRequestMap) {

		LinkedHashMap<ReservationAction, GDSReservationRequestBase> responseMap = new LinkedHashMap<GDSInternalCodes.ReservationAction, GDSReservationRequestBase>();
		GDSReservationRequestBase gdsRrequestBase;

		for (GDSInternalCodes.ReservationAction reservationAction : reservationRequestMap.keySet()) {
			gdsRrequestBase = reservationRequestMap.get(reservationAction);

			if (reservationAction == GDSInternalCodes.ReservationAction.CREATE_BOOKING) {
				gdsRrequestBase = CreateReservationAction.processRequest((CreateReservationRequest) gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.ADD_SEGMENT) {
				gdsRrequestBase = AddSegmentAction.processRequest((AddSegmentRequest) gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.MODIFY_SEGMENT) {
				gdsRrequestBase = ModifySegmentAction.processRequest((ModifySegmentRequest) gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.CANCEL_RESERVATION) {
				gdsRrequestBase = CancelReservationAction.processRequest((CancelReservationRequest)gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.CANCEL_SEGMENT) {
				gdsRrequestBase = CancelSegmentAction.processRequest((CancelSegmentRequest)gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.CHANGE_PAX_DETAILS) {
				gdsRrequestBase = ChangePaxDetailsAction.processRequest((ChangePaxDetailsRequest) gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.CHANGE_CONTACT_DETAILS) {
				gdsRrequestBase = ChangeContactDetailsAction.processRequest((ChangeContactDetailRequest) gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.SPLIT_RESERVATION) {
				gdsRrequestBase = SplitReservationAction.processRequest((SplitReservationRequest) gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.UPDATE_EXTERNAL_SEGMENTS) {
				gdsRrequestBase = UpdateExternalSegmentAction.processRequest((UpdateExternalSegmentsRequest) gdsRrequestBase,
						responseMap);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.LOCATE_RESERVATION) {
				gdsRrequestBase = LocateReservationAction.processRequest((LocateReservationRequest) gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.UPDATE_STATUS) {
				gdsRrequestBase = UpdateStatusAction.processRequest((UpdateStatusRequest) gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.CONFIRM_BOOKING) {
				gdsRrequestBase = ConfirmBookingAction.processRequest((ConfirmBookingRequest) gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.ADD_SSR) {
				gdsRrequestBase = AddSsrAction.processRequest((AddSsrRequest) gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.REMOVE_SSR) {
				gdsRrequestBase = RemoveSsrAction.processRequest((RemoveSsrRequest) gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.ISSUE_ETICKET) {
				gdsRrequestBase = IssueEticketAction.processRequest((IssueEticketRequest) gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.EXCHANGE_ETICKET) {
				gdsRrequestBase = ExchangeEticketAction.processRequest((ExchangeEticketRequest) gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.EXTEND_TIMELIMIT) {
				gdsRrequestBase = ExtendTimelimitAction.processRequest((ExtendTimelimitRequest) gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.ADD_INFANT) {
				gdsRrequestBase = AddInfantAction.processRequest((AddInfantRequest) gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.REMOVE_PAX) {
				gdsRrequestBase = RemovePaxAction.processRequest((RemovePaxRequest) gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.MAKE_PAYMENT) {
				gdsRrequestBase = MakePaymentAction.processRequest((MakePaymentRequest) gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.CHECK_OLD_SEGMENTS) {
				gdsRrequestBase = CheckOldSegmentsAction.processRequest((CheckOldSegmentsRequest) gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.TRANSFER_SEGMENT) {
				gdsRrequestBase = TransferSegmentAction.processRequest((AdviceScheduleChangeRequest) gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.SPLIT_RESERVATION_RESP) {
			 	gdsRrequestBase = SplitReservationResponseAction.processRequest((SplitReservationResponse)gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.CREATE_BOOKING_RESP) {
				gdsRrequestBase = CreateReservationResponseAction.processRequest((CreateReservationResponse) gdsRrequestBase);
			} else if (reservationAction == GDSInternalCodes.ReservationAction.RESERVATION_RESP) {
				gdsRrequestBase = ReservationResponseProcessAction.processRequest((ReservationResponseProcessRequest) gdsRrequestBase);
			} else {
				log.info(" Action not Supported " + reservationAction.toString());
			}

			responseMap.put(reservationAction, gdsRrequestBase);

			if (!gdsRrequestBase.isSuccess()) {
				break;
			}

		}

		return responseMap;
	}
}
