/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.core.bl.ssm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.NotifyFlightEventRQ;
import com.isa.thinair.airinventory.api.dto.NotifyFlightEventsRQ;
import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.SSMSplitFlightSchedule;
import com.isa.thinair.airschedules.api.utils.FlightEventCode;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.airschedules.api.utils.ScheduleBuildStatusEnum;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.XMLStreamer;
import com.isa.thinair.gdsservices.api.model.GDSPublishMessage;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSServicesDAO;
import com.isa.thinair.msgbroker.api.dto.SSIMessegeDTO;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for integrate create schedule with gds
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="publishSSMCancel"
 */
public class PublishSSMCancel extends DefaultBaseCommand {

	private BookingClassBD bookingClassBD;

	private FlightInventoryBD flightInventoryBD;

	// Dao's
	private GDSServicesDAO gdsServiceDao;

	/**
	 * constructor of the PublishSSMCancel command
	 */
	public PublishSSMCancel() {

		// looking up BDs
		bookingClassBD = GDSServicesModuleUtil.getBookingClassBD();

		flightInventoryBD = GDSServicesModuleUtil.getFlightInventoryBD();

		// lokking up DAOs
		gdsServiceDao = GDSServicesDAOUtils.DAOInstance.GDSSERVICES_DAO;
	}

	/**
	 * execute method of the IntegrateSSMNew command
	 * 
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce execute() throws ModuleException {

		FlightSchedule schedule = (FlightSchedule) this.getParameter(GDSSchedConstants.ParamNames.SCHEDULE);
		FlightSchedule overlappingSchedule = (FlightSchedule) this
				.getParameter(GDSSchedConstants.ParamNames.OVERLAPPING_SCHEDULE);
		String serviceType = (String) this.getParameter(GDSSchedConstants.ParamNames.SERVICE_TYPE);
		HashMap buildFlightIds = (HashMap) this.getParameter(GDSSchedConstants.ParamNames.FLIGHT_IDS_MAP_FOR_SCHEDULES);
		String supplementaryInfo = (String) this.getParameter(GDSSchedConstants.ParamNames.SUPPLEMENTARY_INFO);

		Map gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
		Map<String, String> aircraftTypeMap = GDSServicesModuleUtil.getGlobalConfig().getIataAircraftCodes();
		String airLineCode = GDSServicesModuleUtil.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);

		if (isSchedulePublishable(schedule)) {
			Collection flightIds = (Collection) buildFlightIds.get(schedule.getScheduleId());
			if(flightIds != null && !flightIds.isEmpty()){
				invokeMessageBroker(schedule, gdsStatusMap, serviceType, aircraftTypeMap, airLineCode, flightIds, supplementaryInfo);
			}
		}

		if (isSchedulePublishable(overlappingSchedule)) {
			Collection flightIds = (Collection) buildFlightIds.get(overlappingSchedule.getScheduleId());
			if(flightIds != null && !flightIds.isEmpty()){
				invokeMessageBroker(overlappingSchedule, gdsStatusMap, serviceType, aircraftTypeMap, airLineCode, flightIds,
						supplementaryInfo);
			}
		}

		// constructing responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		// return command responce
		return responce;
	}

	private boolean isSchedulePublishable(FlightSchedule schedule) {
		return ((schedule != null && schedule.getBuildStatusCode().equals(ScheduleBuildStatusEnum.BUILT.getCode()) && !(schedule
				.getGdsIds() == null || schedule.getGdsIds().size() == 0))) ? true : false;
	}

	private void invokeMessageBroker(FlightSchedule schedulex, Map gdsStatsMap, String serviceType,
			Map<String, String> aircraftTypeMap, String airLineCode, Collection flightIds, String supplementaryInfo)
			throws ModuleException {

		Collection gdsIds = schedulex.getGdsIds();

		if (gdsIds != null && gdsIds.size() > 0) {
			ArrayList<Integer> activeGdses = new ArrayList<Integer>();
			Collection<SSMSplitFlightSchedule> scheduleColl = schedulex.getSsmSplitFlightSchedule();
			Map<String, Collection<String>> bcMap = bookingClassBD.getBookingClassCodes(gdsIds);
			if (scheduleColl != null && !scheduleColl.isEmpty()) {
				for (SSMSplitFlightSchedule subSchedule : scheduleColl) {
					generateSSMCancelMsg(gdsStatsMap, aircraftTypeMap, schedulex, airLineCode, bcMap, subSchedule, activeGdses,
							supplementaryInfo);
				}
			} else {
				generateSSMCancelMsg(gdsStatsMap, aircraftTypeMap, schedulex, airLineCode, bcMap, null, activeGdses,
						supplementaryInfo);
			}

			// notifiy flight events to inventory to send corresponding avs messages
			if (activeGdses.size() > 0 && flightIds != null && !flightIds.isEmpty()) {
				NotifyFlightEventsRQ notifyFlightEventsRQ = new NotifyFlightEventsRQ();
				NotifyFlightEventRQ newFlightEnvent = new NotifyFlightEventRQ();
				newFlightEnvent.setFlightEventCode(FlightEventCode.FLIGHT_CANCELLED);
				newFlightEnvent.addFlightIds(flightIds);
				newFlightEnvent.setGdsIdsRemoved(activeGdses);
				notifyFlightEventsRQ.addNotifyFlightEventRQ(newFlightEnvent);

				flightInventoryBD.notifyFlightEvent(notifyFlightEventsRQ);
			}
		}
	}

	private void generateSSMCancelMsg(Map<String, GDSStatusTO> gdsStatsMap, Map<String, String> aircraftTypeMap,
			FlightSchedule schedule, String airLineCode, Map<String, Collection<String>> bcMap,
			SSMSplitFlightSchedule subSchedule, ArrayList<Integer> activeGdses, String supplementaryInfo) throws ModuleException {

		SSIMessegeDTO ssmDto = new SSIMessegeDTO();

		SchedulePublishUtil.populateSSIMessageDTOforCNL(ssmDto, schedule, null, aircraftTypeMap.get(schedule.getModelNumber()),
				subSchedule, supplementaryInfo);
		Collection<Integer> gdsIds = schedule.getGdsIds();
		Iterator<Integer> itGdsIds = gdsIds.iterator();

		while (itGdsIds.hasNext()) {

			Integer gdsId = itGdsIds.next();
			SchedulePublishUtil.addRBDInfo(ssmDto, bcMap.get(gdsId.toString()));
			GDSStatusTO gdsStatus = gdsStatsMap.get(gdsId.toString());

			GDSPublishMessage pubMessage = SchedulePublishUtil.createSSMSuccessPublishLog(gdsId, XMLStreamer.compose(ssmDto),
					ssmDto.getReferenceNumber(), schedule.getScheduleId());

			if (GDSInternalCodes.GDSStatus.ACT.getCode().equals(gdsStatus.getStatus())) {

				activeGdses.add(gdsId);

				try {

					// ReservationToGDSMessageSenderBD gdsSender = CommonPublishUtil.getGDSSender();
					GDSServicesModuleUtil.getSSMASMServiceBD().sendStandardScheduleMessage(gdsStatus.getGdsCode(), airLineCode,
							ssmDto);
					gdsServiceDao.saveGDSPublisingMessage(pubMessage);

				} catch (Exception e) {

					pubMessage.setRemarks("Message broker invocation failed");
					pubMessage.setStatus(GDSPublishMessage.MESSAGE_FAILED);
					gdsServiceDao.saveGDSPublisingMessage(pubMessage);
				}
			} else {
				pubMessage.setStatus(GDSPublishMessage.GDS_INACTIVE);
				gdsServiceDao.saveGDSPublisingMessage(pubMessage);
			}
		}

	}
}
