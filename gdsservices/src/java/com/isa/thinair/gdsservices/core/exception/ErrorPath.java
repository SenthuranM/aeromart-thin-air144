package com.isa.thinair.gdsservices.core.exception;

import com.isa.thinair.gdsservices.api.util.TypeAConstants;

public class ErrorPath {

	private TypeAConstants.MessageElement messageElement;

	private ErrorPath childElement;

	public ErrorPath(TypeAConstants.MessageElement messageElement) {
		this.messageElement = messageElement;
	}

	public ErrorPath(TypeAConstants.MessageElement messageElement, ErrorPath childElement) {
		this.messageElement = messageElement;
		this.childElement = childElement;
	}

	public TypeAConstants.MessageElement getMessageElement() {
		return messageElement;
	}

	public ErrorPath getChildElement() {
		return childElement;
	}
}
