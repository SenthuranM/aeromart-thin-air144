package com.isa.thinair.gdsservices.core.command;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.internal.BSPDetail;
import com.isa.thinair.gdsservices.api.dto.internal.CreditCardDetail;
import com.isa.thinair.gdsservices.api.dto.internal.MakePaymentRequest;
import com.isa.thinair.gdsservices.api.dto.internal.PaymentDetail;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.PaymentType;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.platform.api.ServiceResponce;

public class MakePaymentAction {
	private static Log log = LogFactory.getLog(MakePaymentAction.class);

	public static MakePaymentRequest processRequest(MakePaymentRequest paymentRequest) {
		try {

			// 1. Retrieve Reservation ---
			Reservation reservation = CommonUtil.loadReservation(paymentRequest.getGdsCredentialsDTO(), paymentRequest
					.getOriginatorRecordLocator().getPnrReference(), null);

			CommonUtil.validateReservation(paymentRequest.getGdsCredentialsDTO(), reservation);
			CommonUtil.validatePaymentOptions(reservation, paymentRequest.getPaymentDetail());

			PaymentDetail paymentDetail = paymentRequest.getPaymentDetail();
			CreditCardDetail cardDetail = null;
			Agent travelAgent = null;
			BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			Date onholdReleaseTime = null;

			if (paymentDetail != null) {
				if (paymentDetail.getPaymentType() == PaymentType.CREDIT_CARD) {
					cardDetail = (CreditCardDetail) paymentDetail;
				} else if (paymentDetail.getPaymentType() == PaymentType.BSP) {
					BSPDetail bspDetail = (BSPDetail) paymentDetail;
					travelAgent = GDSServicesModuleUtil.getTravelAgentBD().getAgentByIATANumber(bspDetail.getIATACode());
				}

				// 2. Do the payment
				IPayment iPayment = null;
				IPassenger iPassenger = new PassengerAssembler(null);

				Collection<ReservationPax> respassengers = reservation.getPassengers();

				for (ReservationPax pax : respassengers) {
					if (!ReservationApiUtils.isInfantType(pax)) {
						iPayment = new PaymentAssembler();
						BigDecimal amount = pax.getTotalAvailableBalance();
						// FIXME for pay currency
						if (cardDetail != null) {
							iPayment.addCardPayment(Integer.parseInt(cardDetail.getCardType()), cardDetail.getExpiryDate(),
									cardDetail.getCardNo(), cardDetail.getHolderName(), "", cardDetail.getSecureCode(), amount,
									AppIndicatorEnum.APP_XBE, TnxModeEnum.MAIL_TP_ORDER, null, getIPGIdentificationParamsDTO(),
									null, null, null, null, null, null);
						} else if (travelAgent != null) {
							iPayment.addAgentCreditPayment(travelAgent.getAgentCode(), amount, null, null, null, null, null, null, null);
						}

						iPassenger.addPassengerPayments(pax.getPnrPaxId(), iPayment);
						totalAmount = AccelAeroCalculator.add(totalAmount, amount);
					}
				}

				ServiceResponce serviceResponce = GDSServicesModuleUtil.getReservationBD().updateReservationForPayment(
						reservation.getPnr(), iPassenger, false, false, reservation.getVersion(), CommonUtil.getTrackingInfo(paymentRequest.getGdsCredentialsDTO()), false, false, true,
						false, true, false, null, false, null);

				if (serviceResponce != null && serviceResponce.isSuccess()) {
					paymentRequest.setSuccess(true);
				} else {
					paymentRequest.setSuccess(false);
					paymentRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
				}
			} else {
				onholdReleaseTime = reservation.getReleaseTimeStamps()[0];
			}

			paymentRequest.setResponseMessage(CommonUtil.getResponseMessage(paymentDetail, totalAmount, onholdReleaseTime));
		} catch (ModuleException e) {
			log.error(" MakePaymentAction Failed for GDS PNR " + paymentRequest.getOriginatorRecordLocator().getPnrReference(), e);
			paymentRequest.setSuccess(false);
			paymentRequest.setResponseMessage(e.getMessageString());
		} catch (Exception e) {
			log.error(" MakePaymentAction Failed for GDS PNR " + paymentRequest.getOriginatorRecordLocator().getPnrReference(), e);
			paymentRequest.setSuccess(false);
			paymentRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
		}

		return paymentRequest;
	}

	/**
	 * FIXME - Multi Currency Payment Gateway Temp solution.
	 * 
	 * @return
	 */
	private static IPGIdentificationParamsDTO getIPGIdentificationParamsDTO() throws ModuleException {

		String defaultPmtCurrency = AppSysParamsUtil.getDefaultPGCurrency();
		Integer ipgId = GDSServicesModuleUtil.getCommonMasterBD().getCurrency(defaultPmtCurrency).getDefaultXbePGId();
		return new IPGIdentificationParamsDTO(ipgId, AppSysParamsUtil.getBaseCurrency());
	}
}
