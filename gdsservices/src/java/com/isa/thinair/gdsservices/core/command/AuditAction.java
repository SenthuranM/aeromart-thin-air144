package com.isa.thinair.gdsservices.core.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.XMLStreamer;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.RecordLocatorDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSROTHERSDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.model.GDSMessage;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;

/**
 * @author Nilindra Fernando
 */
public class AuditAction {

	public static void perform(BookingRequestDTO bookingRequestDTO, String status, String processDesc,
			GDSCredentialsDTO gdsCredentialsDTO) throws ModuleException {
		Collection<GDSMessage> colGDSMessage = new ArrayList<GDSMessage>();

		GDSMessage oldGDSMessage = getGDSMessage(bookingRequestDTO.getUniqueAccelAeroRequestId());
		GDSMessage newGDSMessage = transform(bookingRequestDTO, oldGDSMessage, status, processDesc, gdsCredentialsDTO);
		colGDSMessage.add(newGDSMessage);

		if (colGDSMessage.size() > 0) {
			GDSServicesDAOUtils.DAOInstance.GDSSERVICES_DAO.saveOrUpdate(colGDSMessage);
		}
	}

	private static GDSMessage getGDSMessage(String uniqueAccelAeroRequestId) {
		return GDSServicesDAOUtils.DAOInstance.GDSSERVICES_DAO.getGDSMessage(uniqueAccelAeroRequestId);
	}

	private static String convertToXML(BookingRequestDTO base) {
		return XMLStreamer.compose(base);
	}

	private static GDSMessage action(BookingRequestDTO base, GDSMessage oldGDSMessage, String status, String processDesc) {
		String toXML = convertToXML(base);
		oldGDSMessage.setProcessDescription(processDesc);
		oldGDSMessage.setResponseXML(toXML);
		oldGDSMessage.setProcessStatus(status);
		oldGDSMessage.setProcessStatusHistory(GDSApiUtils.composeProcessHistory(base.getProcessStatusHistory()));

		return oldGDSMessage;
	}

	private static GDSMessage action_NOT_PROCESSED(BookingRequestDTO base, String status, GDSCredentialsDTO gdsCredentialsDTO) {
		String toXML = convertToXML(base);
		GDSMessage gdsMessage = new GDSMessage();
		gdsMessage.setMessageType(base.getMessageType().getValue());
		gdsMessage.setUniqueAccelAeroRequestId(base.getUniqueAccelAeroRequestId());
		gdsMessage.setMessageReceivedDate(new Date());
		gdsMessage.setCommunicationReference(base.getCommunicationReference());
		gdsMessage.setOriginatorAddress(base.getOriginatorAddress());
		gdsMessage.setResponderAddress(base.getResponderAddress());
		gdsMessage.setGds(base.getGds().getCode());
		gdsMessage.setRequestXML(toXML);
		gdsMessage.setResponseXML("");
		gdsMessage.setProcessDescription("");
		gdsMessage.setProcessStatus(GDSExternalCodes.ProcessStatus.NOT_PROCESSED.getValue());
		gdsMessage.setProcessStatusHistory(GDSApiUtils.composeProcessHistory(base.getProcessStatusHistory()));
		gdsMessage.setMessageBrokerReferenceId(base.getMessageBrokerReferenceId());

		if (base.getOriginatorRecordLocator() != null) {
			RecordLocatorDTO recordLocatorDTO = base.getOriginatorRecordLocator();

			String pnrReference = GDSApiUtils.maskNull(recordLocatorDTO.getPnrReference());
			if (pnrReference.length() > 0) {
				gdsMessage.setGdsPnr(pnrReference);
			}

			String gdsAgentCode = GDSApiUtils.maskNull(recordLocatorDTO.getTaOfficeCode());
			if (gdsAgentCode.length() > 0) {
				gdsMessage.setGdsAgentCode(gdsAgentCode);
			}

			String gdsUserId = GDSApiUtils.maskNull(recordLocatorDTO.getUserID());
			if (gdsUserId.length() > 0) {
				gdsMessage.setGdsUserId(gdsUserId);
			}
		}

		return gdsMessage;
	}

	private static GDSMessage action_ERROR_OCCURED(BookingRequestDTO base, GDSMessage oldGDSMessage, String processDesc) {
		base.getProcessStatusHistory().add(GDSExternalCodes.ProcessStatus.ERROR_OCCURED);

		oldGDSMessage.setProcessDescription(processDesc);
		oldGDSMessage.setResponseXML("");
		oldGDSMessage.setProcessStatus(GDSExternalCodes.ProcessStatus.ERROR_OCCURED.getValue());
		oldGDSMessage.setProcessStatusHistory(GDSApiUtils.composeProcessHistory(base.getProcessStatusHistory()));
		return oldGDSMessage;
	}

	private static GDSMessage transform(BookingRequestDTO base, GDSMessage oldGDSMessage, String status, String processDesc,
			GDSCredentialsDTO gdsCredentialsDTO) throws ModuleException {
		if (oldGDSMessage == null && GDSExternalCodes.ProcessStatus.NOT_PROCESSED.equals(status)) {
			return action_NOT_PROCESSED(base, status, gdsCredentialsDTO);
		} else if (oldGDSMessage != null && GDSExternalCodes.ProcessStatus.ERROR_OCCURED.equals(status)) {
			return action_ERROR_OCCURED(base, oldGDSMessage, processDesc);
		} else if (oldGDSMessage != null) {
			return action(base, oldGDSMessage, status, processDesc);
		}

		throw new ModuleException("gdsservices.framework.unsupportedRequest");
	}

	public static String getActionDescription(BookingRequestDTO bookingRequestDTO) {
		String responseMsg = GDSApiUtils.maskNull(bookingRequestDTO.getResponseMessage());

		if (responseMsg.length() > 0) {
			return responseMsg;
		} else {
			StringBuilder message = new StringBuilder("");
			List<SSRDTO> lstSSRDTOs = bookingRequestDTO.getSsrDTOs();
			String ssrValue;
			for (SSRDTO ssrDTO : lstSSRDTOs) {
				if (ssrDTO instanceof SSROTHERSDTO) {
					ssrValue = GDSApiUtils.maskNull(ssrDTO.getSsrValue());
					if (message.length() > 0) {
						message.append(" ").append(ssrValue);
					} else {
						message.append(ssrValue);
					}
				}
			}

			return message.toString();
		}
	}
}
