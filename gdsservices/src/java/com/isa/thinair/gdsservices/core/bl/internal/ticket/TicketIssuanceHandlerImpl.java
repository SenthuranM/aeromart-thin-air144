package com.isa.thinair.gdsservices.core.bl.internal.ticket;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.dto.GDSChargeAdjustmentRQ;
import com.isa.thinair.airproxy.api.dto.GDSPaxChargesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientBalancePayment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.dto.RefundInfoTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.dto.ClientCommonInfoDTO;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketDisplayRq;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketDisplayRs;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketsAssembler;
import com.isa.thinair.gdsservices.api.dto.typea.internal.TicketingEventRq;
import com.isa.thinair.gdsservices.api.dto.typea.internal.TicketingEventRs;
import com.isa.thinair.gdsservices.api.model.GdsEvent;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.typeA.model.IataOperationConfig;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.platform.api.ServiceResponce;

public class TicketIssuanceHandlerImpl implements TicketIssuanceHandler {

	private boolean refundsExist = false;
	private boolean paymentsExist = false;
	private LCCClientReservation reservation;
	private String refundNote = null;
	private boolean isManualRefund = false;
	private LCCClientBalancePayment balancePayment;
	private CommonReservationAssembler reservationAssembler;
	private Map<Integer, BigDecimal> effectivePaxTurnover;
	
	private static final Log log = LogFactory.getLog(TicketIssuanceHandlerImpl.class);

	public TicketingEventRs handleMonetaryDiscrepancies(TicketingEventRq ticketIssueRq) throws ModuleException {
		TicketingEventRs ticketIssueRs;

		DefaultBaseCommand command;
		String commandName;
		TicketDisplayRq ticketDisplayRq;
		ServiceResponce resp;

		ticketDisplayRq = new TicketDisplayRq();
		ticketDisplayRq.setRequestType(GdsEvent.Request.TICKET_DISPLAY);
		ticketDisplayRq.setGdsId(ticketIssueRq.getGdsId());
		ticketDisplayRq.setPaxTicketsCoupons(ticketIssueRq.getPaxTicketsCoupons());
		ticketDisplayRq.setPnr(ticketIssueRq.getPnr());
		ticketDisplayRq.setCalculateFinancialInfo(true);

		commandName = IataOperationConfig.getMessageInitiatorCommand(IATAOperation.TKCREQ);
		command = (DefaultBaseCommand) GDSServicesModuleUtil.getModule().getLocalBean(commandName);
		command.setParameter(TypeACommandParamNames.TICKET_CONTROL_DTO, ticketDisplayRq);
		command.setParameter(TypeACommandParamNames.IATA_OPERATION, IATAOperation.TKCREQ);
		command.setParameter(TypeACommandParamNames.IATA_EVENT, GdsEvent.Request.TICKET_DISPLAY);

		resp = command.execute();
		TicketDisplayRs ticketDisplayRs = (TicketDisplayRs) resp.getResponseParam(TypeACommandParamNames.TICKET_DTO);

		if (ticketDisplayRs.isSuccess()) {

			GDSChargeAdjustmentRQ gdsChargeAdjustmentRQ = ticketDisplayRs.getGdsChargeAdjustmentRq();
			GDSServicesModuleUtil.getReservationBD().syncAATotalGroupChargesWithCarrier(gdsChargeAdjustmentRQ, null);

			TicketIssuanceHandler ticketIssuanceHandler = new TicketIssuanceHandlerImpl();
			TicketsAssembler ticketsAssembler = ticketDisplayRs.getTicketsAssembler();
			ticketIssuanceHandler.handleMonetaryDiscrepancies(ticketsAssembler);

		}
		ticketIssueRs = new TicketingEventRs();
		ticketIssueRs.setSuccess(true);
		return ticketIssueRs;
	}

	public TicketingEventRs handleMonetaryDiscrepancies(TicketsAssembler ticketsAssembler) throws ModuleException {
		TicketingEventRs ticketIssueRs;

		// TODO -- pass reservation in ticket-assembler
		reservation = GDSServicesUtil.loadLccReservation(ticketsAssembler.getPnr());

		balancePayment = new LCCClientBalancePayment();
		balancePayment.setGroupPNR(reservation.getPNR());
		balancePayment.setOtherCarrierPaxCreditUser(false);
		balancePayment.setAcceptPaymentsThanPayable(true);

		reservationAssembler = new CommonReservationAssembler();

		effectivePaxTurnover = new HashMap<Integer, BigDecimal>();
		log.info("Inside handleMonetaryDiscrepancies");
		prepare(ticketsAssembler);
        sync(ticketsAssembler);
		settle(ticketsAssembler);
		log.info("Exit handleMonetaryDiscrepancies");
		ticketIssueRs = new TicketingEventRs();
		return ticketIssueRs;
	}

	public void prepare(TicketsAssembler ticketsAssembler) throws ModuleException {
		int lccPnrPaxId;
		BigDecimal adjustmentAmount = BigDecimal.ZERO;
		BigDecimal gdsAmount = BigDecimal.ZERO;
		BigDecimal gdsAmountInBaseCurr = BigDecimal.ZERO;
		BigDecimal aaAmountInBaseCurr = BigDecimal.ZERO;
		BigDecimal discrepancy = BigDecimal.ZERO;
		BigDecimal payment = BigDecimal.ZERO;


		for (TicketsAssembler.PassengerAssembler passengerAssembler : ticketsAssembler.getPassengerAssemblers()) {
			for (LCCClientReservationPax pax : reservation.getPassengers()) {
				lccPnrPaxId = PaxTypeUtils.getPnrPaxId(pax.getTravelerRefNumber());

				if (lccPnrPaxId == passengerAssembler.getPnrPaxId()) {

					switch (ticketsAssembler.getMode()) {
					case PAYMENT:
						refundNote = "GDS Excess Payment Adjustment";
						isManualRefund = false;

						if (passengerAssembler.getBalance() != null) {
							gdsAmount = passengerAssembler.getBalance();
							discrepancy = gdsAmount;
							aaAmountInBaseCurr = pax.getTotalAvailableBalance();
//							if (pax.getInfants() != null) {
//								for (LCCClientReservationPax infant : pax.getInfants()) {
//									aaAmountInBaseCurr = aaAmountInBaseCurr.subtract(infant.getTotalAvailableBalance());
//								}
//							}
						} else if (passengerAssembler.getTotalAmount() != null) {
							gdsAmount = passengerAssembler.getTotalAmount();
							aaAmountInBaseCurr = pax.getTotalPrice();

//							if (pax.getInfants() != null) {
//								for (LCCClientReservationPax infant : pax.getInfants()) {
//									aaAmountInBaseCurr = aaAmountInBaseCurr.subtract(infant.getTotalPrice());
//								}
//							}

							discrepancy = gdsAmount.subtract(pax.getTotalPaidAmount());
						} else {
							// TODO - throw exc
						}

						if (discrepancy.compareTo(BigDecimal.ZERO) >= 0) {
							prepareVirtualPayment(passengerAssembler, balancePayment, reservation, pax, discrepancy);
							paymentsExist = true;
						} else {
							prepareVirtualRefund(ticketsAssembler, passengerAssembler, reservationAssembler, reservation, pax, discrepancy);
							refundsExist = true;
						}


						break;
					case RECONCILE:
						refundNote = "GDS Excess Payment Refund";
						isManualRefund = false;

						if (passengerAssembler.getTotalAmount() != null) {
							gdsAmount = passengerAssembler.getTotalAmount();
							aaAmountInBaseCurr = pax.getTotalPrice();

						} else {
							// TODO - throw exc
						}

						adjustmentAmount = gdsAmount.subtract(aaAmountInBaseCurr);

						discrepancy = pax.getTotalAvailableBalance().add(adjustmentAmount);

						if (discrepancy.compareTo(BigDecimal.ZERO) > 0) {
							prepareVirtualPayment(passengerAssembler, balancePayment, reservation, pax, discrepancy);
							paymentsExist = true;
						} else if (discrepancy.compareTo(BigDecimal.ZERO) < 0) {
							prepareVirtualRefund(ticketsAssembler, passengerAssembler, reservationAssembler, reservation, pax, discrepancy);
							refundsExist = true;
						}

						break;
					case REFUND:
						refundNote = "GDS Payment Refund";
						isManualRefund = false;

						if (passengerAssembler.getNote() != null) {
							refundNote = passengerAssembler.getNote();
						}

						if (passengerAssembler.getTotalAmount() != null) {

//							if (discrepancy.compareTo(BigDecimal.ZERO) > 0) {
								discrepancy = passengerAssembler.getTotalAmount();

								prepareVirtualRefund(ticketsAssembler, passengerAssembler, reservationAssembler, reservation, pax, discrepancy);
								refundsExist = true;
//							}
						} else {
							if (pax.getTotalAvailableBalance().doubleValue() < 0) {

								discrepancy = pax.getTotalAvailableBalance();

								prepareVirtualRefund(ticketsAssembler, passengerAssembler, reservationAssembler, reservation, pax, discrepancy);
								refundsExist = true;
							}
						}

						break;
					}

				}
			}
		}
	}

	public void sync(TicketsAssembler ticketsAssembler)  throws ModuleException {

		GDSChargeAdjustmentRQ gdsChargeAdjustmentRQ = ticketsAssembler.getGdsChargeAdjustmentRQ();
		Map<Integer, GDSPaxChargesTO> gdsCharges;
		GDSPaxChargesTO gdsPaxCharges;
		BigDecimal totalCharges;
		BigDecimal totalPaid;
		BigDecimal discrepancy;
		int pnrPaxId;
		int infantPnrPaxId;

		if (gdsChargeAdjustmentRQ != null) {

			gdsCharges = gdsChargeAdjustmentRQ.getGdsChargesByPax();

			for (LCCClientReservationPax passenger : reservation.getPassengers()) {

				pnrPaxId = PaxTypeUtils.getPnrPaxId(passenger.getTravelerRefNumber());

				if (gdsCharges.containsKey(pnrPaxId)) {
					gdsPaxCharges = gdsCharges.get(pnrPaxId);

//					if (passenger.getPaxType().equals(PaxTypeTO.INFANT)) {
//						discrepancy = BigDecimal.ZERO;
//					} else {
						totalCharges = gdsPaxCharges.getTotalCharges();
						totalPaid = passenger.getTotalPaidAmount();

						if (effectivePaxTurnover.containsKey(pnrPaxId)) {
							totalPaid = totalPaid.add(effectivePaxTurnover.get(pnrPaxId));
						} else {
							throw new ModuleException("");
						}

//						for (LCCClientReservationPax infant : passenger.getInfants()) {
//							infantPnrPaxId = PaxTypeUtils.getPnrPaxId(infant.getTravelerRefNumber());
//
//							if (effectivePaxTurnover.containsKey(infantPnrPaxId) && gdsCharges.containsKey(infantPnrPaxId)) {
//
//								totalCharges = totalCharges.add(gdsCharges.get(infantPnrPaxId).getTotalCharges());
//								totalPaid = totalPaid.add(effectivePaxTurnover.get(infantPnrPaxId));
//
//							} else {
//								throw new ModuleException("");
//							}
//						}
						discrepancy = totalCharges.subtract(totalPaid);
//					}

					gdsPaxCharges.setSurChargeAmount(gdsPaxCharges.getSurChargeAmount().subtract(discrepancy));
				}
			}

			GDSServicesModuleUtil.getReservationBD().syncAATotalGroupChargesWithCarrier(gdsChargeAdjustmentRQ, null);
		}
	}



	public void settle(TicketsAssembler ticketsAssembler) throws ModuleException {

		ClientCommonInfoDTO clientInfoDTO = new ClientCommonInfoDTO();
		clientInfoDTO.setCarrierCode(GDSServicesUtil.getCarrierCode());
		clientInfoDTO.setIpAddress(ticketsAssembler.getIpAddress());

		LCCClientReservation lccClientReservation = reservationAssembler.getLccreservation();
		lccClientReservation.setPNR(reservation.getPNR());

		Long reservationVersion;
		if (paymentsExist) {

			// do the forced confirmation
			if (reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
				reservationVersion = GDSServicesDAOUtils.DAOInstance.GDSSERVICES_SUPPORT_JDBC_DAO.getReservationVersion(reservation.getPNR());

				LCCClientBalancePayment balPay = new LCCClientBalancePayment();
				balPay.setGroupPNR(reservation.getPNR());
				balPay.setOtherCarrierPaxCreditUser(false);
				balPay.setAcceptPaymentsThanPayable(true);
				balPay.setForceConfirm(true);

				GDSServicesModuleUtil.getAirproxyReservationBD().balancePayment(balPay,
						String.valueOf(reservationVersion), false, AppSysParamsUtil.isFraudCheckEnabled(SalesChannelsUtil.SALES_CHANNEL_AGENT),
						clientInfoDTO, null, true, false);
			}

			reservationVersion = GDSServicesDAOUtils.DAOInstance.GDSSERVICES_SUPPORT_JDBC_DAO.getReservationVersion(reservation.getPNR());
			log.info("calling balancePayment");
			GDSServicesModuleUtil.getAirproxyReservationBD().balancePayment(balancePayment,
					String.valueOf(reservationVersion), false, AppSysParamsUtil.isFraudCheckEnabled(SalesChannelsUtil.SALES_CHANNEL_AGENT),
					clientInfoDTO, null, true, false);
			log.info("End balancePayment");
		}

		if (refundsExist) {
			log.info("calling refundPassenger");
			reservationVersion = GDSServicesDAOUtils.DAOInstance.GDSSERVICES_SUPPORT_JDBC_DAO.getReservationVersion(reservation.getPNR());
			lccClientReservation.setVersion(String.valueOf(reservationVersion));

			GDSServicesModuleUtil.getAirproxyPassengerBD().refundPassenger(reservationAssembler, refundNote,
					false, null, null, null, true, isManualRefund);
			log.info("End refundPassenger");
		}


	}


	public void prepareVirtualPayment(TicketsAssembler.PassengerAssembler passengerAssembler,
	                                                       LCCClientBalancePayment balancePayment,
	                                                       LCCClientReservation reservation, LCCClientReservationPax pax,
	                                                       BigDecimal amount) throws ModuleException {

		LCCClientPaymentAssembler paymentAssembler;
		PayCurrencyDTO payCurrencyDTO;
		LCCClientPaymentInfo paymentInfo;

		Date exchangeRateByDate = CalendarUtil.getCurrentSystemTimeInZulu();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(exchangeRateByDate);

		int gdsId = reservation.getGdsId();
		Gds gds = GDSServicesModuleUtil.getGdsBD().getGds(gdsId);
		User user = GDSServicesModuleUtil.getSecurityBD().getUserBasicDetails(gds.getUserId());


		CurrencyExchangeRate currExRate = exchangeRateProxy.getCurrencyExchangeRate(passengerAssembler.getCurrencyCode());
		payCurrencyDTO = new PayCurrencyDTO(currExRate.getCurrency().getCurrencyCode(),
				currExRate.getMultiplyingExchangeRate(), currExRate.getCurrency().getBoundryValue(), currExRate.getCurrency()
				.getBreakPoint());


		int paxSequence;
		paxSequence = pax.getPaxSequence();

//		if (pax.getPaxType().equals(PaxTypeTO.INFANT)) {
//			paxSequence = pax.getParent().getPaxSequence();
//		} else {
//			paxSequence = pax.getPaxSequence();
//		}

		if (!balancePayment.getPaxSeqWisePayAssemblers().containsKey(paxSequence)) {
			paymentAssembler = new LCCClientPaymentAssembler();
			balancePayment.addPassengerPayments(paxSequence, paymentAssembler);
		}

		int pnrPaxId = PaxTypeUtils.getPnrPaxId(pax.getTravelerRefNumber());
		if (!effectivePaxTurnover.containsKey(pnrPaxId)) {
			effectivePaxTurnover.put(pnrPaxId, BigDecimal.ZERO);
		}
		effectivePaxTurnover.put(pnrPaxId, effectivePaxTurnover.get(pnrPaxId).add(amount));

		paymentAssembler = balancePayment.getPaxSeqWisePayAssemblers().get(paxSequence);

		if (paymentAssembler.getPayments().isEmpty()) {
			paymentInfo = paymentAssembler.addAgentCreditPayment(user.getAgentCode(), amount, payCurrencyDTO, new Date(), null,
					null, Agent.PAYMENT_MODE_ONACCOUNT, null);
			paymentInfo.setRemarks(passengerAssembler.getNote());
		} else {
			paymentInfo = paymentAssembler.getPayments().iterator().next();
			paymentAssembler.mergePayment(paymentInfo, amount, payCurrencyDTO);
		}






	}

	private void prepareVirtualRefund(TicketsAssembler ticketsAssembler, TicketsAssembler.PassengerAssembler passengerAssembler,
	                                  CommonReservationAssembler reservationAssembler, LCCClientReservation reservation,
	                                  LCCClientReservationPax reservationPax, BigDecimal amount) throws ModuleException {

		int gdsId = reservation.getGdsId();
		Gds gds = GDSServicesModuleUtil.getGdsBD().getGds(gdsId);
		User user = GDSServicesModuleUtil.getSecurityBD().getUserBasicDetails(gds.getUserId());
		boolean transactionMerged;

		LCCClientPaymentAssembler paymentAssembler;

		Date exchangeRateByDate = CalendarUtil.getCurrentSystemTimeInZulu();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(exchangeRateByDate);
		CurrencyExchangeRate currExRate;

		BigDecimal adjustmentAmount;

		currExRate = exchangeRateProxy.getCurrencyExchangeRate(passengerAssembler.getCurrencyCode());
		if (currExRate == null) {
			throw new ModuleException("");
		}

		adjustmentAmount = amount.abs();

		PayCurrencyDTO payCurrencyDTO = new PayCurrencyDTO(currExRate.getCurrency().getCurrencyCode(),
				currExRate.getMultiplyingExchangeRate(), currExRate.getCurrency().getBoundryValue(), currExRate.getCurrency()
				.getBreakPoint());

		LCCClientReservationPax applicablePax;
		if (reservationPax.getPaxType().equals(PaxTypeTO.INFANT)) {
			applicablePax = reservationPax.getParent();
		} else {
			applicablePax = reservationPax;
		}
		int pnrPaxId = com.isa.thinair.airproxy.api.utils.PaxTypeUtils.getPnrPaxId(applicablePax.getTravelerRefNumber());

		if (!effectivePaxTurnover.containsKey(pnrPaxId)) {
			effectivePaxTurnover.put(pnrPaxId, BigDecimal.ZERO);
		}
		effectivePaxTurnover.put(pnrPaxId, effectivePaxTurnover.get(pnrPaxId).subtract(adjustmentAmount));

		List<LCCClientPaymentInfo> filteredTransactions = new ArrayList<LCCClientPaymentInfo>();
		LCCClientPaymentInfo paymentInfo;
		for (LCCClientPaymentInfo payInfo : applicablePax.getLccClientPaymentHolder().getPayments()) {
			if (payInfo.getTotalAmount().compareTo(BigDecimal.ZERO) > 0) {
				filteredTransactions.add(payInfo);
			}
		}
		Collections.sort(filteredTransactions, new Comparator<LCCClientPaymentInfo>() {
			public int compare(LCCClientPaymentInfo o1, LCCClientPaymentInfo o2) {
				return o1.getTxnDateTime().compareTo(o2.getTxnDateTime());
			}
		});

		if (filteredTransactions.isEmpty()) {
			throw new ModuleException("No Payments"); // TODO -- error code
		}

		paymentInfo = filteredTransactions.iterator().next();

		RefundInfoTO refundInfoTO = new RefundInfoTO(paymentInfo.getOriginalPayReference(), paymentInfo.getLccUniqueTnxId(),
				paymentInfo.getTxnDateTime(), paymentInfo.getCarrierVisePayments(), paymentInfo.getCarrierCode() );

		LCCClientReservationPax mergingPax = null;
		if (reservationAssembler.getLccreservation().getPassengers() != null) {
			for (LCCClientReservationPax pax : reservationAssembler.getLccreservation().getPassengers()) {
				if (applicablePax.getPaxSequence().equals(pax.getPaxSequence())) {
					mergingPax = pax;
					break;
				}
			}
		}

		if (mergingPax != null && !mergingPax.getLccClientPaymentAssembler().getPayments().isEmpty()) {
			paymentAssembler = mergingPax.getLccClientPaymentAssembler();
			paymentInfo = paymentAssembler.getPayments().iterator().next();
			paymentAssembler.mergePayment(paymentInfo, adjustmentAmount, payCurrencyDTO);

			transactionMerged = true;
		} else {
			paymentAssembler = new LCCClientPaymentAssembler();
			paymentAssembler.addAgentCreditPayment(user.getAgentCode(), adjustmentAmount, payCurrencyDTO, new Date(), null,
					null, Agent.PAYMENT_MODE_ONACCOUNT, refundInfoTO);

			transactionMerged = false;
		}



		boolean isParent = (applicablePax.getInfants() != null && !applicablePax.getInfants().isEmpty());
		if (isParent) {

			if (!transactionMerged) {
				reservationAssembler.addParent(applicablePax.getFirstName(), applicablePax.getLastName(),
						applicablePax.getTitle(), applicablePax.getDateOfBirth(), applicablePax.getNationalityCode(),
						applicablePax.getPaxSequence(), applicablePax.getAttachedPaxId(), applicablePax.getTravelerRefNumber(),
						new PaxAdditionalInfoDTO(), applicablePax.getPaxCategory(), paymentAssembler, null, null, null, null, null,
						null, null, null, null);
			}
		} else if (PaxTypeTO.CHILD.equals(applicablePax.getPaxType())) {
			reservationAssembler.addChild(applicablePax.getFirstName(), applicablePax.getLastName(), applicablePax.getTitle(),
					applicablePax.getDateOfBirth(), applicablePax.getNationalityCode(), applicablePax.getPaxSequence(),
					applicablePax.getTravelerRefNumber(), new PaxAdditionalInfoDTO(), applicablePax.getPaxCategory(), paymentAssembler, null, null,
					null, null, null, null, null, null, null, null);
		} else {
			reservationAssembler
					.addSingle(applicablePax.getFirstName(), applicablePax.getLastName(), applicablePax.getTitle(),
							applicablePax.getDateOfBirth(), applicablePax.getNationalityCode(),
							applicablePax.getPaxSequence(), applicablePax.getTravelerRefNumber(), new PaxAdditionalInfoDTO(),
							applicablePax.getPaxCategory(), paymentAssembler, null, null, null, null, null, null, null, null,
							null, null);
		}

	}

}
