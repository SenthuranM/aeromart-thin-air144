package com.isa.thinair.gdsservices.core.typeA.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExternalReservationPaxDetail implements Serializable {

	private static final long serialVersionUID = -1767305154988832421L;

	private String firstName;

	private String lastName;

	private String title;

	private String paxType;

	private MonetaryInformation monetaryInformation;

	private List<PaxETicket> paxETickets;

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the paxType
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return the monetaryInformation
	 */
	public MonetaryInformation getMonetaryInformation() {
		return monetaryInformation;
	}

	/**
	 * @param monetaryInformation
	 *            the monetaryInformation to set
	 */
	public void setMonetaryInformation(MonetaryInformation monetaryInformation) {
		this.monetaryInformation = monetaryInformation;
	}

	/**
	 * @return the paxETickets
	 */
	public List<PaxETicket> getPaxETickets() {
		if (paxETickets == null) {
			paxETickets = new ArrayList<PaxETicket>();
		}
		return paxETickets;
	}

	/**
	 * @param paxETickets
	 *            the paxETickets to set
	 */
	public void setPaxETickets(List<PaxETicket> paxETickets) {
		this.paxETickets = paxETickets;
	}

	public void addPaxETicker(PaxETicket paxETicket) {
		getPaxETickets().add(paxETicket);
	}

	/**
	 * returns flat list of eticket segments belong to pax
	 * 
	 * @return
	 */
	public List<PaxETicketSegment> getAllEticketSegments() {
		List<PaxETicketSegment> allEtickets = new ArrayList<PaxETicketSegment>();
		for (PaxETicket paxETicket : getPaxETickets()) {
			allEtickets.addAll(paxETicket.getPaxEticketSegments());
		}
		return Collections.unmodifiableList(allEtickets);
	}

	/**
	 * return all own eticket segmetns
	 * 
	 * @return
	 */
	public List<PaxETicketSegment> getOwnEticketSegments() {
		List<PaxETicketSegment> ownEticketSegments = new ArrayList<PaxETicketSegment>();
		for (PaxETicketSegment eTicketSegment : getAllEticketSegments()) {
			if (!eTicketSegment.isExternalSegment()) {
				ownEticketSegments.add(eTicketSegment);
			}
		}
		return Collections.unmodifiableList(ownEticketSegments);
	}

	/**
	 * return external eticket segments
	 * 
	 * @return
	 */
	public List<PaxETicketSegment> getExternalEticketSegments() {
		List<PaxETicketSegment> externalEticketSegments = new ArrayList<PaxETicketSegment>();
		for (PaxETicketSegment eTicketSegment : getAllEticketSegments()) {
			if (eTicketSegment.isExternalSegment()) {
				externalEticketSegments.add(eTicketSegment);
			}
		}
		return Collections.unmodifiableList(externalEticketSegments);
	}

}
