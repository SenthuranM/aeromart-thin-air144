package com.isa.thinair.gdsservices.core.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegmentETicket;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.dto.internal.ExchangeEticketRequest;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceDetailRequest;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceRequest;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class ExchangeEticketAction {
	
	private static Log log = LogFactory.getLog(ExchangeEticketAction.class);

	public static ExchangeEticketRequest processRequest(ExchangeEticketRequest exchangeEticketRequest) {

		try {
			Reservation reservation = CommonUtil.loadReservation(exchangeEticketRequest.getGdsCredentialsDTO(), exchangeEticketRequest
					.getOriginatorRecordLocator().getPnrReference(), null);

			CommonUtil.validateReservation(exchangeEticketRequest.getGdsCredentialsDTO(), reservation);

			String nameKey = null;
			Passenger passenger = null;
			Map<String, Passenger> passengerMap = CommonUtil.getPassengerMap(exchangeEticketRequest.getPassengers());

			Collection<ReservationPax> resPassengers = reservation.getPassengers();
			ArrayList<EticketTO> eticketTOs = new ArrayList<EticketTO>();
			for (ReservationPax resPax : resPassengers) {
				nameKey = GDSApiUtils.getIATAName(resPax.getLastName(), resPax.getFirstName(), resPax.getTitle());
				passenger = passengerMap.get(nameKey);

				if (passenger != null) {
					eticketTOs.addAll(addEticketInfoForPax(resPax, passenger, exchangeEticketRequest, reservation));
				} else if (passengerMap.size() == 0) {
					eticketTOs.addAll(addEticketInfoForAllPax(resPax, exchangeEticketRequest.getCommonSSRs(), reservation));
				}
			}
			ServiceResponce serviceResponce = GDSServicesModuleUtil.getReservationBD().updateExternalEticketInfo(eticketTOs);

			if (serviceResponce != null && serviceResponce.isSuccess()) {
				exchangeEticketRequest.setSuccess(true);
			} else {
				exchangeEticketRequest.setSuccess(false);
				exchangeEticketRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
			}

		} catch (ModuleException e) {
			log.error(" ExchangeEticketAction Failed for GDS PNR "
					+ exchangeEticketRequest.getOriginatorRecordLocator().getPnrReference(), e);
			exchangeEticketRequest.setSuccess(false);
			exchangeEticketRequest.setResponseMessage(e.getMessageString());
		} catch (Exception e) {
			log.error(" ExchangeEticketAction Failed for GDS PNR "
					+ exchangeEticketRequest.getOriginatorRecordLocator().getPnrReference(), e);
			exchangeEticketRequest.setSuccess(false);
			exchangeEticketRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
		}

		return exchangeEticketRequest;
	}

	private static ArrayList<EticketTO> addEticketInfoForPax(ReservationPax resPax, Passenger passenger,
			ExchangeEticketRequest exchangeEticketRequest, Reservation reservation) {

		ArrayList<EticketTO> eticketTOs = new ArrayList<EticketTO>();
		Collection<SpecialServiceRequest> sSRs = passenger.getSsrCollection();

		if (sSRs != null) {
			for (SpecialServiceRequest ssr : sSRs) {
				if (ssr instanceof SpecialServiceDetailRequest) {
					SpecialServiceDetailRequest ssdr = (SpecialServiceDetailRequest) ssr;
					if (GDSExternalCodes.SSRCodes.ET_NOT_CHANGED.getCode().equals(ssdr.getCode())) {
						if (ssdr.getSegment() != null) {
							for (ReservationPaxFare reservationPaxFare : resPax.getPnrPaxFares()) {
								for (ReservationPaxFareSegment reservationPaxFareSegment : reservationPaxFare
										.getPaxFareSegments()) {
									for (ReservationSegmentDTO seg : reservation.getSegmentsView()) {
										if (seg.getPnrSegId().intValue() == reservationPaxFareSegment.getPnrSegId().intValue()) {
											if (seg.getDestination().equals(ssdr.getSegment().getArrivalStation())
													&& seg.getOrigin().equals(ssdr.getSegment().getDepartureStation())
													&& CalendarUtil.isEqualStartTimeOfDate(seg.getDepartureDate(), ssdr
															.getSegment().getDepartureDate())
													&& seg.getFlightNo().substring(2).equals(ssdr.getSegment().getFlightNumber())) {

												for (EticketTO eTicket : resPax.geteTickets()) {
													if (eTicket.getPnrPaxFareSegId().equals(
															reservationPaxFareSegment.getPnrPaxFareSegId())
															&& !eTicket.getTicketStatus().equals(EticketStatus.EXCHANGED.code())
															&& !eTicket.getTicketStatus().equals(EticketStatus.CLOSED.code())) {
														if (ssdr.getComment() != null && ssdr.getComment().length() > 0
																&& eTicket.getExternalEticketNumber() == null
																&& eTicket.getExternalCouponNo() == null) {
															eTicket.setExternalEticketNumber(ssdr.getComment());
															eTicket.setExternalCouponNo(eTicket.getCouponNo());
															eTicket.setExternalCouponStatus(EticketStatus.OPEN.code());
															eTicket.setExternalCouponControl(ReservationPaxFareSegmentETicket.ExternalCouponControl.VALIDATING_CARRIER);
															eticketTOs.add(eTicket);
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return eticketTOs;
	}

	private static ArrayList<EticketTO> addEticketInfoForAllPax(ReservationPax resPax,
			Collection<SpecialServiceRequest> commonSSRs, Reservation reservation) {

		ArrayList<EticketTO> eticketTOs = new ArrayList<EticketTO>();
		if (commonSSRs != null && commonSSRs.size() > 0) {
			for (SpecialServiceRequest ssr : commonSSRs) {
				if (ssr instanceof SpecialServiceDetailRequest) {
					SpecialServiceDetailRequest ssdr = (SpecialServiceDetailRequest) ssr;
					if (GDSExternalCodes.SSRCodes.ET_NOT_CHANGED.getCode().equals(ssdr.getCode()) && ssdr.getSegment() != null) {
						for (ReservationPaxFare reservationPaxFare : resPax.getPnrPaxFares()) {
							for (ReservationPaxFareSegment reservationPaxFareSegment : reservationPaxFare.getPaxFareSegments()) {
								for (ReservationSegmentDTO seg : reservation.getSegmentsView()) {
									if (seg.getPnrSegId().intValue() == reservationPaxFareSegment.getPnrSegId().intValue()) {
										if (seg.getDestination().equals(ssdr.getSegment().getArrivalStation())
												&& seg.getOrigin().equals(ssdr.getSegment().getDepartureStation())
												&& CalendarUtil.isEqualStartTimeOfDate(seg.getDepartureDate(), ssdr.getSegment()
														.getDepartureDate())
												&& seg.getFlightNo().substring(2).equals(ssdr.getSegment().getFlightNumber())) {

											for (EticketTO eTicket : resPax.geteTickets()) {
												if (eTicket.getPnrPaxFareSegId().equals(
														reservationPaxFareSegment.getPnrPaxFareSegId())) {
													if (ssdr.getComment() != null && ssdr.getComment().length() > 0
															&& ssdr.getComment().contains("C")) {
														String[] values = ssdr.getComment().split("C");
														if (values.length == 2 && values[0].length() > 0
																&& values[1].length() > 0) {
															eTicket.setExternalEticketNumber(values[0]);
															eTicket.setExternalCouponNo(Integer.parseInt(values[1]));
															eTicket.setExternalCouponStatus(EticketStatus.OPEN.code());
															eTicket.setExternalCouponControl(ReservationPaxFareSegmentETicket.ExternalCouponControl.OPERATING_CARRIER);
															eticketTOs.add(eTicket);
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return eticketTOs;
	}

}
