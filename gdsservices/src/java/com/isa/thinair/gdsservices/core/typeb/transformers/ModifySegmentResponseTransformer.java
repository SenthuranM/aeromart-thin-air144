package com.isa.thinair.gdsservices.core.typeb.transformers;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.ModifySegmentRequest;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.dto.internal.SegmentDetail;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ProcessStatus;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.StatusCode;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;

/**
 * @author Manoj Dhanushka
 */
public class ModifySegmentResponseTransformer {

	/**
	 * Transforms reservationResponseMap back to bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param reservationResponseMap
	 * @return
	 */
	public static BookingRequestDTO getResponse(BookingRequestDTO bookingRequestDTO,
			Map<ReservationAction, GDSReservationRequestBase> reservationResponseMap) {

		ModifySegmentRequest modifySegmentRequest = (ModifySegmentRequest) reservationResponseMap
				.get(ReservationAction.MODIFY_SEGMENT);

		if (modifySegmentRequest.isSuccess()) {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_SUCCESS);
			bookingRequestDTO = setSegmentStatuses(bookingRequestDTO, modifySegmentRequest);
			bookingRequestDTO = ResponseTransformerUtil.setSSRStatus(bookingRequestDTO);
			bookingRequestDTO = setScheduleChanges (bookingRequestDTO, modifySegmentRequest);
			bookingRequestDTO = ValidatorUtils.addResponse(bookingRequestDTO, modifySegmentRequest);
		} else {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_FAILURE);
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, modifySegmentRequest);
		}
		bookingRequestDTO.getErrors().addAll(modifySegmentRequest.getErrorCode());

		return bookingRequestDTO;
	}

	/**
	 * Adds segment statuses to the bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param cancelSegmentRequest
	 */
	private static BookingRequestDTO setSegmentStatuses(BookingRequestDTO bookingRequestDTO,
			ModifySegmentRequest modifySegmentRequest) {

		List<BookingSegmentDTO> bookingSegmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();
		Collection<Segment> cancellingSegments = modifySegmentRequest.getCancelingSegments();
		Collection<SegmentDetail> newSegmentDetails = modifySegmentRequest.getNewSegmentDetails();

		Iterator<BookingSegmentDTO> iterSegDTO = bookingSegmentDTOs.iterator();

		while (iterSegDTO.hasNext()) {
			BookingSegmentDTO bookingSegmentDTO = (BookingSegmentDTO) iterSegDTO.next();

			for (Segment cancelSegment : cancellingSegments) {
				if (modifySegmentRequest.isSuccess()) {
					cancelSegment.setStatusCode(StatusCode.CANCELLED);
				}
				if (GDSApiUtils.isEqual(bookingSegmentDTO, cancelSegment)) {
					String gdsActionCode = GDSApiUtils.maskNull(bookingSegmentDTO.getActionOrStatusCode());

					if (gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL.getCode())
							|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_IF_AVAILABLE.getCode())
							|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_IF_HOLDING.getCode())
							|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_RECOMMENDED.getCode())
							|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CODESHARE_CANCEL.getCode())) {

						String adviceStatusCode = ResponseTransformerUtil.getSegmentStatus(cancelSegment);

						bookingSegmentDTO.setAdviceOrStatusCode(adviceStatusCode);
					}
				}
			}
			for (SegmentDetail segmentDetail : newSegmentDetails) {
				Segment segment = segmentDetail.getSegment();
				
				if (modifySegmentRequest.isSuccess() && segmentDetail.isChangeInSceduleExists()) {
					segment.setStatusCode(StatusCode.CONFIRMED_SCHEDULE_CHANGED);
				} else if (modifySegmentRequest.isSuccess()) {
					segment.setStatusCode(StatusCode.CONFIRMED);
				}
				if (GDSApiUtils.isEqual(bookingSegmentDTO, segment)) {
					String gdsActionOrStatusCode = GDSApiUtils.maskNull(bookingSegmentDTO.getActionOrStatusCode());

					if (gdsActionOrStatusCode.equals(GDSExternalCodes.ActionCode.NEED.getCode())
							|| gdsActionOrStatusCode.equals(GDSExternalCodes.ActionCode.NEED_IFNOT_HOLDING.getCode())
							|| gdsActionOrStatusCode.equals(GDSExternalCodes.ActionCode.SOLD.getCode())
							|| gdsActionOrStatusCode.equals(GDSExternalCodes.ActionCode.SOLD_FREE.getCode())
							|| gdsActionOrStatusCode.equals(GDSExternalCodes.ActionCode.SOLD_IFNOT_HOLDING.getCode())
							|| gdsActionOrStatusCode.equals(GDSExternalCodes.ActionCode.CODESHARE_SELL.getCode())) {

						String adviceStatusCode = ResponseTransformerUtil.getSegmentStatus(segment,
								bookingSegmentDTO.getActionOrStatusCode());

						bookingSegmentDTO.setAdviceOrStatusCode(adviceStatusCode);
					}
				}
			}
		}
		return bookingRequestDTO;
	}
	
	private static BookingRequestDTO setScheduleChanges (BookingRequestDTO bookingRequestDTO,
			ModifySegmentRequest modifySegmentRequest) {
		
		List<BookingSegmentDTO> bookingSegmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();
		Collection<SegmentDetail> segmentDetails = modifySegmentRequest.getNewSegmentDetails();

		Iterator<BookingSegmentDTO> iterSegDTO = bookingSegmentDTOs.iterator();

		while (iterSegDTO.hasNext()) {
			BookingSegmentDTO bookingSegmentDTO = (BookingSegmentDTO) iterSegDTO.next();

			for (SegmentDetail segmentDetail : segmentDetails) {
				Segment segment = segmentDetail.getSegment();
				if (GDSApiUtils.isEqual(bookingSegmentDTO, segment)) {
					if (segmentDetail.isChangeInSceduleExists()) {
						bookingSegmentDTO.setChangeInScheduleExist(true);
						bookingSegmentDTO.setDepartureTime(segment.getDepartureTime());
						bookingSegmentDTO.setArrivalTime(segment.getArrivalTime());
						if (segment.getArrivalDayOffset() < 0) {
							bookingSegmentDTO.setDayOffSet(Math.abs(segment.getArrivalDayOffset()));
							bookingSegmentDTO.setDayOffSetSign("M");
						} else {
							bookingSegmentDTO.setDayOffSet(segment.getArrivalDayOffset());
						}
					}
				}
			}
		}
		
		return bookingRequestDTO;
		
	}
}
