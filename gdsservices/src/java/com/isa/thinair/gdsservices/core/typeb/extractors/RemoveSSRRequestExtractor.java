package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDetailDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.internal.RemoveSsrRequest;
import com.isa.thinair.gdsservices.api.dto.internal.SSRData;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceDetailRequest;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceRequest;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

public class RemoveSSRRequestExtractor extends ValidatorBase {
	private static final Log log = LogFactory.getLog(RemoveSSRRequestExtractor.class);

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {

		String pnrReference = ValidatorUtils.getPNRReference(bookingRequestDTO.getResponderRecordLocator());

		if (pnrReference.length() == 0) {
			String message = MessageUtil.getMessage("gdsservices.extractors.emptyPNRResponder");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		} else {
			List<SSRDTO> ssrDTOs = bookingRequestDTO.getSsrDTOs();

			if (ssrDTOs != null && ssrDTOs.size() > 0) {
				// Commented this because we are going to provide SSR remove facility
				// String message = MessageUtil.getMessage("gdsservices.extractors.ssr.cannot.modify");
				// bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);
				//
				// this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
				// return bookingRequestDTO;
				
				this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
				return bookingRequestDTO;
				
			} else {
				// There aren't any RemoveSSRRequestExtractor specific validations.
				// The General Action codes are validated at the high level.
				this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
				return bookingRequestDTO;
			}
		}
	}

	/**
	 * returns RemoveSsrRequest
	 * 
	 * @param gdsCredentialsDTO
	 * @param ssrData
	 * @param bookingRequestDTO
	 * @return
	 */
	public static RemoveSsrRequest getRequest(GDSCredentialsDTO gdsCredentialsDTO, SSRData ssrData,
			BookingRequestDTO bookingRequestDTO) {
		RemoveSsrRequest removeSSRRequest = new RemoveSsrRequest();

		removeSSRRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
		removeSSRRequest = (RemoveSsrRequest) RequestExtractorUtil.addBaseAttributes(removeSSRRequest, bookingRequestDTO);

		removeSSRRequest = addPassengersAndSSRs(removeSSRRequest, ssrData);
		removeSSRRequest.setNotSupportedSSRExists(bookingRequestDTO.isNotSupportedSSRExists());

		return removeSSRRequest;
	}

	/**
	 * adds Passengers and SSRs to the RemoveSsrRequest
	 * 
	 * @param removeSSRRequest
	 * @param ssrData
	 * @return
	 */
	private static RemoveSsrRequest addPassengersAndSSRs(RemoveSsrRequest removeSSRRequest, SSRData ssrData) {

		Map<String, Passenger> resPaxMap = new HashMap<String, Passenger>();
		Collection<SSRDetailDTO> ssrDetailDTOs = ssrData.getDetailSSRDTOs();
		Collection<SSRDTO> ssrDTOs = ssrData.getSsrDTOs();

		// add SSRDetailDTOs to paxs
		for (Iterator<SSRDetailDTO> iterator = ssrDetailDTOs.iterator(); iterator.hasNext();) {
			SSRDetailDTO ssrDetailDTO = (SSRDetailDTO) iterator.next();
			List<NameDTO> nameDTOs = ssrDetailDTO.getNameDTOs();
			String gdsActionCode = ssrDetailDTO.getActionOrStatusCode();

			// add SSRDetailDTOs to selected paxs
			if (!ssrDetailDTO.getCodeSSR().equals(GDSExternalCodes.SSRCodes.ETICKET_NO.getCode())) {
				if (gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_IF_AVAILABLE.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_IF_HOLDING.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_RECOMMENDED.getCode())) {
					if (nameDTOs != null && nameDTOs.size() > 0) {
						for (Iterator<NameDTO> iterNameDTO = nameDTOs.iterator(); iterNameDTO.hasNext();) {
							NameDTO nameDTO = (NameDTO) iterNameDTO.next();
							SpecialServiceDetailRequest ssr = new SpecialServiceDetailRequest();
							ssr = SSRExtractorUtil.addSSRDetailAttributes(ssr, ssrDetailDTO);
							String iataPaxName = nameDTO.getIATAName();

							Passenger resPax = resPaxMap.get(iataPaxName);

							if (resPax == null) {
								resPax = new Passenger();
								resPax = RequestExtractorUtil.getPassenger(resPax, nameDTO);
								resPaxMap.put(iataPaxName, resPax);
							}

							resPax.getSsrCollection().add(ssr);
							removeSSRRequest.getPassengers().add(resPax);
						}
					} else {
						// add to common SSRS
						SpecialServiceDetailRequest ssr = new SpecialServiceDetailRequest();
						ssr = SSRExtractorUtil.addSSRDetailAttributes(ssr, ssrDetailDTO);
						removeSSRRequest.getCommonSSRs().add(ssr);
					}
				}
			}
		}

		// add to common SSRS
		for (SSRDTO ssrDTO : ssrDTOs) {
			String gdsActionCode = ssrDTO.getActionOrStatusCode();

			if (gdsActionCode != null) {
				if (gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_IF_AVAILABLE.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_IF_HOLDING.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_RECOMMENDED.getCode())) {
					SpecialServiceRequest ssr = new SpecialServiceDetailRequest();

					ssr = SSRExtractorUtil.addBaseSSRAttributes(ssr, ssrDTO);
					removeSSRRequest.getCommonSSRs().add(ssr);
				}
			}

		}

		return removeSSRRequest;
	}
}
