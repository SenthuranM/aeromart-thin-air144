package com.isa.thinair.gdsservices.core.typeA.transformers.tkcuac.v03;

import iata.typea.v031.tkcres.IATA;
import iata.typea.v031.tkcres.MSG;
import iata.typea.v031.tkcres.ObjectFactory;
import iata.typea.v031.tkcres.TKCRES;
import iata.typea.v031.tkcres.UNB;
import iata.typea.v031.tkcres.UNH;
import iata.typea.v031.tkcres.UNT;

import javax.xml.bind.JAXBElement;

import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.core.typeA.model.InterchangeHeader;
import com.isa.thinair.gdsservices.core.typeA.model.MessageHeader;
import com.isa.thinair.gdsservices.core.typeA.transformers.tkcreqres.v03.TkcresFactory;
import com.isa.thinair.gdsservices.core.util.RandomANGen;

public class TransferControlResponseHandler {

	public JAXBElement<?> constructEDIResponce(DefaultServiceResponse response) throws InteractiveEDIException {

		TkcresFactory tkcresFactory = TkcresFactory.getInstance();
		ObjectFactory objectFactory = tkcresFactory.getObjectFactory();

		IATA iata = objectFactory.createIATA();

		InterchangeHeader iHeader = (InterchangeHeader) response.getResponseParam(TypeACommandParamNames.IC_HEADER);
		MessageHeader mHeader = (MessageHeader) response.getResponseParam(TypeACommandParamNames.MESSAGE_HEADER);
		String messageFuc = (String) response.getResponseParam(TypeACommandParamNames.MESSAGE_FUNCTION);

		// unb
		String interchangeControllRef = new RandomANGen(10).nextString("0001");
		UNB unb = tkcresFactory.createUNB(iHeader, interchangeControllRef);

		// tkcres
		TKCRES tkcres = objectFactory.createTKCRES();

		UNH unh = tkcresFactory.createUNH(mHeader);
		tkcres.setUNH(unh);

		MSG msg = tkcresFactory.createMSG(messageFuc);
		tkcres.setMSG(msg);

		UNT unt = tkcresFactory.createUNT(mHeader);
		tkcres.setUNT(unt);

		iata.getUNBAndUNGAndTKCRES().add(unb);
		iata.getUNBAndUNGAndTKCRES().add(tkcres);

		return objectFactory.createIATA(iata);
	}

}
