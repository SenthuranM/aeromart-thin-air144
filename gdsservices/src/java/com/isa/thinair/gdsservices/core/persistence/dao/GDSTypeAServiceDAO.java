package com.isa.thinair.gdsservices.core.persistence.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.gdsservices.api.dto.typea.DisplayTicketParams;
import com.isa.thinair.gdsservices.api.dto.typea.DisplayTicketTo;
import com.isa.thinair.gdsservices.api.model.GDSReservationInfo;
import com.isa.thinair.gdsservices.api.model.GDSReservationPaxInfo;
import com.isa.thinair.gdsservices.api.model.GDSTransactionStatus;

/**
 * GDS Type A related DAO
 * 
 * @author mekanayake
 * 
 */
public interface GDSTypeAServiceDAO {

	public <T> T gdsTypeAServiceGet(Class<T> e, Serializable id);

	public <T> void gdsTypeAServiceSaveOrUpdate(T t);

	public <T> void gdsTypeAServiceLock(T t, boolean wait);

	public GDSTransactionStatus getGDSTransactionStatus(long id);

	public GDSTransactionStatus getGDSTransactionStatusFromRef(String tnxRefId);

	public String generateRespCommonAccessRefKey();

	public Integer getSettlementAuthorizationKey();

	public List<GDSReservationInfo> getGDSReservationInfo(String pnr);

	public List<GDSReservationInfo> getGDSReservationInfo(List<String> pnrs);

	public List<GDSReservationPaxInfo> getGDSReservationPaxInfo(long pnrPaxId);

	public List<GDSReservationPaxInfo> getGDSReservationPaxInfo(List<Long> pnrPaxIds);

	public List<DisplayTicketTo> searchTickets(DisplayTicketParams params);

	public Map<String, Set<String>> getExternalETNumbersMap(List<String> extETicketNumbers);

	public Map<String, Map<Integer, List<String>>> groupTickets(List<String> ticketNumbers);
}
