package com.isa.thinair.gdsservices.core.bl.interceptor;

import java.util.Map;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.dto.EDIMessageDTO;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.typea.common.BaseEDIRequest;
import com.isa.typea.common.Header;

/**
 * @isa.module.command name="interactiveHeaderProcessor"
 */
public class InteractiveHeaderProcessor extends DefaultBaseCommand {

	public ServiceResponce execute() throws ModuleException {

		String newState = null;

		EDIMessageDTO messageDTO = (EDIMessageDTO) getParameter(TypeACommandParamNames.EDI_REQUEST_DTO);
		Map<String, Object> msgSession = messageDTO.getMessagingSession();

		String currentState = null;
		if (msgSession.containsKey(TypeAConstants.MessageSessionKey.MSG_CONTEXT_STATE)) {
			currentState = (String) msgSession.get(TypeAConstants.MessageSessionKey.MSG_CONTEXT_STATE);
		}

		BaseEDIRequest ediRequest = messageDTO.getRequest();
		Header header = ediRequest.getHeader();

		String requestedState = header.getInterchangeHeader().getAssociationCode();

		if(currentState == null) {
            if (requestedState.equals(TypeAConstants.AssociationCode.ESTABLISH)) {
	            newState = TypeAConstants.AssociationCode.ESTABLISHED;
	            msgSession.put(TypeAConstants.MessageSessionKey.MSG_CONTEXT_STATE,
			            TypeAConstants.AssociationCode.ESTABLISHED);
            } else {
	            // TODO ---- ERROR
            }
		} else if(currentState.equals(TypeAConstants.AssociationCode.ESTABLISH)
				&& (requestedState.equals(TypeAConstants.AssociationCode.ESTABLISHED)
					|| requestedState.equals(TypeAConstants.AssociationCode.RETRY))) {
			newState = TypeAConstants.AssociationCode.ESTABLISHED;
			if (requestedState.equals(TypeAConstants.AssociationCode.ESTABLISHED))    {
				msgSession.put(TypeAConstants.MessageSessionKey.MSG_CONTEXT_STATE,
						TypeAConstants.AssociationCode.ESTABLISHED);
			}

		} else if (currentState.equals(TypeAConstants.AssociationCode.ESTABLISHED)
				&& (requestedState.equals(TypeAConstants.AssociationCode.ESTABLISH)
					|| requestedState.equals(TypeAConstants.AssociationCode.RETRY))) {
			newState = TypeAConstants.AssociationCode.DUPLICATE;
		} else if (currentState.equals(TypeAConstants.AssociationCode.ESTABLISHED)
				&& requestedState.equals(TypeAConstants.AssociationCode.ESTABLISHED)) {
			newState = TypeAConstants.AssociationCode.ESTABLISHED;
		} else if (requestedState.equals(TypeAConstants.AssociationCode.TERMINATE)) {
			if (currentState.equals(TypeAConstants.AssociationCode.ESTABLISHED)) {
				msgSession.put(TypeAConstants.MessageSessionKey.MSG_CONTEXT_STATE,
						TypeAConstants.AssociationCode.TERMINATE);
			} else {
				// TODO -- ERROR
			}
		}


		DefaultServiceResponse resp = new DefaultServiceResponse();
		resp.addResponceParam(TypeACommandParamNames.EDI_RESP_ASSOCIATION_CODE, newState);
		return resp;
	}
}
