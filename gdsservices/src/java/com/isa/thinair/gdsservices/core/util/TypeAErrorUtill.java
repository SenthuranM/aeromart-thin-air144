package com.isa.thinair.gdsservices.core.util;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.gdsservices.api.dto.typea.ERCDetail;
import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;

/**
 * Utill to creat error response
 * 
 * @author malaka
 * 
 */
public class TypeAErrorUtill {

	public static Map<String, Object> addAppError(Map<String, Object> params, CodeSetEnum.CS9321 error, int level) {
		Set<ERCDetail> appErrors = (Set<ERCDetail>) params.get(TypeACommandParamNames.APPLICATION_ERROR);
		if (appErrors == null) {
			appErrors = new HashSet<ERCDetail>();
		}

		ERCDetail ercDetail = new ERCDetail(error, level);
		appErrors.add(ercDetail);
		return params;
	}

}
