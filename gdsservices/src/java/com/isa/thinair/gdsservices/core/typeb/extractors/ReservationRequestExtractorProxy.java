package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.LinkedHashMap;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.MessageIdentifier;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ProcessStatus;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

public class ReservationRequestExtractorProxy {

	/**
	 * Transform to a Map of GDS Reservation Requests
	 * 
	 * @param gdsCredentialsDTO
	 * @param bookingRequestDTO
	 * @return
	 */
	public static LinkedHashMap<GDSInternalCodes.ReservationAction, GDSReservationRequestBase> extract(
			GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO) {

		LinkedHashMap<ReservationAction, GDSReservationRequestBase> reservationRequestMap = null;
		GDSReservationRequestBase requestBase = null;
		String messageIdentifier = bookingRequestDTO.getMessageIdentifier();

		if (messageIdentifier.equals(MessageIdentifier.LOCATE_RESERVATION.getCode())) {
			requestBase = LocateReservationRequestExtractor.getRequest(gdsCredentialsDTO, bookingRequestDTO);
		} else if (messageIdentifier.equals(MessageIdentifier.PDM.getCode())) {
			requestBase = LocateReservationRequestExtractor.getRequest(gdsCredentialsDTO, bookingRequestDTO);
		} else if (messageIdentifier.equals(MessageIdentifier.NEW_BOOKING.getCode())) {
			requestBase = CreateRequestExtractor.getRequest(gdsCredentialsDTO, bookingRequestDTO);
		} else if (messageIdentifier.equals(MessageIdentifier.CANCEL_BOOKING.getCode())) {
			reservationRequestMap = CancelReservationRequestExtractor.getMap(gdsCredentialsDTO, bookingRequestDTO);
		} else if (messageIdentifier.equals(MessageIdentifier.NEW_CONTINUATION.getCode())
				|| messageIdentifier.equals(MessageIdentifier.NEW_ARRIVAL.getCode())
				|| messageIdentifier.equals(MessageIdentifier.ARRIVAL_UNKNOWN.getCode())) {
			requestBase = UpdateExternalSegmentsRequestExtractor.getRequest(gdsCredentialsDTO, bookingRequestDTO);
		} else if (messageIdentifier.equals(MessageIdentifier.AMEND_BOOKING.getCode())) {
			reservationRequestMap = ModifyReservationRequestExtractor.getMap(gdsCredentialsDTO, bookingRequestDTO);
		} else if (messageIdentifier.equals(MessageIdentifier.DEVIDE_BOOKING.getCode())) {
			reservationRequestMap = SplitReservationRequestExtractor.getMap(gdsCredentialsDTO, bookingRequestDTO);
		} else if (messageIdentifier.equals(MessageIdentifier.STATUS_REPLY.getCode())) {
			requestBase = UpdateStatusRequestExtractor.getRequest(gdsCredentialsDTO, bookingRequestDTO);
		} else if (messageIdentifier.equals(MessageIdentifier.TICKETING.getCode())) {
			requestBase = ConfirmBookingRequestExtractor.getRequest(gdsCredentialsDTO, bookingRequestDTO);
		} else if (messageIdentifier.equals(MessageIdentifier.SCHEDULE_CHANGE.getCode())) {
			reservationRequestMap = AdviceScheduleChangeRequestExtractor.getMap(gdsCredentialsDTO, bookingRequestDTO);
		} else if (messageIdentifier.equals(MessageIdentifier.DIVIDE_RESP.getCode())) {
			requestBase = SplitReservationResponseExtractor.getRequest(gdsCredentialsDTO, bookingRequestDTO);
		} else if (messageIdentifier.equals(MessageIdentifier.LINK_RECORD_LOCATOR.getCode())) {
			requestBase = CreateReservationResponseExtractor.getRequest(gdsCredentialsDTO, bookingRequestDTO);
		}else if (messageIdentifier.equals(MessageIdentifier.RESERVATION_RESP.getCode())) {
			requestBase = ReservationResponseExtractor.getRequest(gdsCredentialsDTO, bookingRequestDTO);
		}

		if (reservationRequestMap == null && requestBase != null) {
			reservationRequestMap = new LinkedHashMap<ReservationAction, GDSReservationRequestBase>();
			reservationRequestMap.put(requestBase.getActionType(), requestBase);
		}

		if (reservationRequestMap.keySet().contains(GDSInternalCodes.ReservationAction.ADD_SEGMENT)
				&& reservationRequestMap.keySet().contains(GDSInternalCodes.ReservationAction.CANCEL_SEGMENT)) {
			reservationRequestMap = ModifySegmentRequestExtractor.getMap(reservationRequestMap, gdsCredentialsDTO, bookingRequestDTO);
		}
		bookingRequestDTO.setProcessStatus(ProcessStatus.EXTRACT_SUCCESS);
		return reservationRequestMap;
	}
}
