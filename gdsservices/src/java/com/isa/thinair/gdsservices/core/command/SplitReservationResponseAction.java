package com.isa.thinair.gdsservices.core.command;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.GdsReservationAttributesDto;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.internal.SplitReservationResponse;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.msgbroker.api.util.MessageComposerApiUtils;

public class SplitReservationResponseAction {
	private static Log log = LogFactory.getLog(SplitReservationResponseAction.class);

	public static SplitReservationResponse processRequest(SplitReservationResponse splitReservationResponse) {
		GdsReservationAttributesDto gdsReservationAttributesDto;

		try {

			gdsReservationAttributesDto = new GdsReservationAttributesDto();
			gdsReservationAttributesDto.setPnr(splitReservationResponse.getResponderRecordLocator().getPnrReference());
			gdsReservationAttributesDto.setExternalRecLocator(splitReservationResponse.getOriginatorRecordLocator().getPnrReference());
			gdsReservationAttributesDto.setExternalPos(MessageComposerApiUtils.getExternalPos(splitReservationResponse.getOriginatorRecordLocator()));

			GDSServicesModuleUtil.getReservationBD().mergeGdsReservationAttributes(gdsReservationAttributesDto);

			splitReservationResponse.setSuccess(true);

		} catch (ModuleException e) {
			log.error(e.getMessage(), e);
			splitReservationResponse.setSuccess(false);
			splitReservationResponse.setResponseMessage(e.getMessageString());
			splitReservationResponse.addErrorCode(e.getExceptionCode());
		}

		return splitReservationResponse;
	}
}