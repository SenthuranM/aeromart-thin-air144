package com.isa.thinair.gdsservices.core.bl.typeA.common;

import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.typea.cltreq.AACLTREQ;
import com.isa.typea.cltreq.CLTREQMessage;
import com.isa.typea.cltres.AACLTRES;
import com.isa.typea.cltres.CLTRESMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *  @isa.module.command name="clearTerminateTransaction"
 */
public class ClearTerminateTransaction extends EdiMessageHandler {

	protected void handleMessage() {
		AACLTREQ aacltreq = (AACLTREQ) messageDTO.getRequest();
		CLTREQMessage cltreqMessage = aacltreq.getMessage();

		AACLTRES aacltres = new AACLTRES();
		CLTRESMessage cltresMessage = new CLTRESMessage();
		cltresMessage.setMessageHeader(GDSDTOUtil.createRespMessageHeader(cltreqMessage.getMessageHeader(),
				messageDTO.getRequestMetaDataDTO().getMessageReference(), messageDTO.getRequestMetaDataDTO().getIataOperation().getResponse()));
		aacltres.setHeader(GDSDTOUtil.createRespEdiHeader(aacltreq.getHeader(), cltresMessage.getMessageHeader(), messageDTO));

		try {
			releaseBlockSeats();
		} catch (Exception e) {

		}

		cltresMessage.setClearTerminateInfo(cltreqMessage.getClearTerminateInfo());

		aacltres.setMessage(cltresMessage);

		messageDTO.setResponse(aacltres);
	}

	private void releaseBlockSeats() throws GdsTypeAException {
		ReservationBD reservationBD;

		Map<String, Integer> tempSegBcAllocIds = (Map<String, Integer>) messageDTO.getMessagingSession().get(TypeAConstants.MessageSessionKey.BLOCKED_SEATS);
		List<TempSegBcAlloc> tempSegBcAllocs;
		List<Integer> segBcAllocIds = new ArrayList<>(tempSegBcAllocIds.values());

		if (!segBcAllocIds.isEmpty()) {
			tempSegBcAllocs = GDSServicesModuleUtil.getFlightInventoryBD().getTempSegBcAllocs(segBcAllocIds);

			try {
				reservationBD = GDSServicesModuleUtil.getReservationBD();
				reservationBD.releaseBlockedSeats(tempSegBcAllocs);
			} catch (ModuleException ex) {
				throw new GdsTypeAException();
			}
		}
	}
}
