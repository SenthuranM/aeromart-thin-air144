package com.isa.thinair.gdsservices.core.typeA.transformers.tkcreqres.v03;

import iata.typea.v031.tkcreq.TKCREQ;

import java.util.Map;

import javax.xml.bind.JAXBElement;

import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.core.typeA.transformers.EDIFACTMessageProcess;

public class ChangeStatus implements EDIFACTMessageProcess {

	@Override
	public Map<String, Object> extractSpecificCommandParams(Object message) {
		TKCREQ tkcreq = (TKCREQ) message;
		return ChangeStatusRequestHandler.getOwnMessageParams(tkcreq);
	}

	@Override
	public JAXBElement<?> constructEDIResponce(DefaultServiceResponse response) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCommandName() {
		// TODO Auto-generated method stub
		return null;
	}

}
