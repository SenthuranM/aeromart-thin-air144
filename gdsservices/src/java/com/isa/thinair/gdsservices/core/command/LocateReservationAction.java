package com.isa.thinair.gdsservices.core.command;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.internal.CreateReservationRequest;
import com.isa.thinair.gdsservices.api.dto.internal.LocateReservationRequest;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

public class LocateReservationAction {
	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(LocateReservationAction.class);

	public static LocateReservationRequest processRequest(LocateReservationRequest locateResRequest) {

		try {
			Reservation reservation = CommonUtil.loadReservationFromExternalLocator(locateResRequest
					.getOriginatorRecordLocator().getPnrReference());

			if (reservation == null) {
				locateResRequest = (LocateReservationRequest) CreateReservationAction
						.processRequest((CreateReservationRequest) locateResRequest);
			} else {
				locateResRequest = (LocateReservationRequest) CommonUtil.getExistingReservation(reservation, locateResRequest);
			}
		} catch (ModuleException e) {
			log.error(" LocateReservationActionAction Failed for GDS PNR "
					+ locateResRequest.getOriginatorRecordLocator().getPnrReference(), e);
			locateResRequest.setSuccess(false);
			locateResRequest.setResponseMessage(e.getMessageString());
		} catch (Exception e) {
			log.error(" LocateReservationActionAction Failed for GDS PNR "
					+ locateResRequest.getOriginatorRecordLocator().getPnrReference(), e);
			locateResRequest.setSuccess(false);
			locateResRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
		}

		return locateResRequest;
	}
}
