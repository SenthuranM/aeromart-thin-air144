package com.isa.thinair.gdsservices.core.bl.avs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateStatusDTO;
import com.isa.thinair.airinventory.api.dto.OperationStatusDTO;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airschedules.api.dto.ScheduleSearchDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.TTYMessageUtil;
import com.isa.thinair.gdsservices.api.dto.external.AVSRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.AVSRequestSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.external.AVSResponse;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.config.GDSServicesModuleConfig;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

public class ProcessAVSRequest {

	private final Log log = LogFactory.getLog(getClass());
	private static int DEFAULT_SEAT_ALLOCATION = 10;
	private static int MAX_ALLOWED_ENTRY = 30; // as per IATA standard it should be 10
	private static String ALL_BC = "All";
	public static final String FLIGHT_OPEN = "AS";
	public static final String L_AVAIL = "L";
	public static final String C_AVAIL = "C";
	public static final String A_AVAIL_S = "A";
	public static final String RE_OPENED_FOR_SALES = "LA";

	public ProcessAVSRequest() {

	}

	public AVSResponse beginProcessAVS(AVSRequestDTO avsRequestDTO) throws ModuleException {
		AVSResponse avsResponse = new AVSResponse();
		try {

			List<AVSRequestSegmentDTO> avsEntryList = avsRequestDTO.getAVSSegmentDTOs();

			log.info("ProcessAVSRequest beginProcessAVS");
			String gdsIdStr = avsRequestDTO.getGdsId();
			if (StringUtil.isNullOrEmpty(gdsIdStr) || StringUtil.isNullOrEmpty(avsRequestDTO.getOperatingCarrier())) {
				throw new ModuleException("gdsservices.airschedules.logic.bl.unknown.sender");
			}

			if (avsEntryList != null && avsEntryList.size() > MAX_ALLOWED_ENTRY) {
				throw new ModuleException("gdsservices.airschedules.leg.count.invalid");
			}

			Integer gdsId = new Integer(gdsIdStr);
			List<String> ignoreBookingClassList = avsIgnoreBookingClassList(gdsIdStr);
			List<String> actualBcList = avsProcessingAllowedActualBCList(gdsId, ignoreBookingClassList);
			for (AVSRequestSegmentDTO avsSegment : avsEntryList) {

				try {
					Collection<Flight> flightColl = new ArrayList<Flight>();

					flightColl = getAvailableFlights(avsSegment, avsRequestDTO.getOperatingCarrier());

					if (avsSegment.getSeatsAvailable() > 9) {
						throw new ModuleException("gdsservices.airschedules.logic.bl.rbd.invalid");
					}

					String gdsbookingCode = avsSegment.getRbd();
					Collection<String> rbdColl = new ArrayList<String>();

					String bookingCode = null;
					if (!ALL_BC.equalsIgnoreCase(gdsbookingCode)) {
						if (ignoreBookingClassList != null && ignoreBookingClassList.contains(gdsbookingCode)) {
							continue;
						}
						bookingCode = TTYMessageUtil.getActualBCForExternalCsBC(gdsId, gdsbookingCode);
						// only allowed CS BC should be processed, possible chances of these BCs having multiple mapping
						if (bookingCode == null || (actualBcList != null && !actualBcList.contains(bookingCode))) {
							// throw new ModuleException("gdsservices.airschedules.logic.bl.rbd.invalid");
							continue;
						}
						rbdColl.add(bookingCode);
					}

					if (ALL_BC.equalsIgnoreCase(gdsbookingCode)) {
						rbdColl.addAll(actualBcList);
					}

					updateFlightSegmentInventory(rbdColl, flightColl, avsSegment);

				} catch (Exception e) {
					addErrorResponse(avsResponse, e);
				}

			}
			avsResponse.setSuccess(true);
		} catch (Exception e) {
			log.error(" ERROR @ ProcessAVSRequest :: beginProcessAVS", e);
			addErrorResponse(avsResponse, e);
		}

		return avsResponse;

	}

	private static void addErrorResponse(AVSResponse avsResponse, Exception e) {
		avsResponse.setSuccess(false);
		if (e instanceof ModuleException) {
			ModuleException me = (ModuleException) e;
			if (avsResponse.getResponseMessage() == null || avsResponse.getResponseMessage() == "") {
				if (me.getModuleCode() != null) {
					String msg = MessageUtil.getMessage(me.getModuleCode());
					if (!StringUtil.isNullOrEmpty(msg)) {
						avsResponse.setResponseMessage(msg);
					} else {
						avsResponse.setResponseMessage(me.getMessageString() + me.getModuleCode());
					}

				} else {
					avsResponse.setResponseMessage(me.getMessageString());
				}
			}

		} else {
			avsResponse.setResponseMessage(MessageUtil.getDefaultErrorMessage());

		}
	}

	private static Collection<Flight> getAvailableFlights(AVSRequestSegmentDTO avsSegment, String operatingCarrier)
			throws ModuleException {

		String flightDesignator = avsSegment.getFlightDesignator();

		Collection<Flight> flightColl = new ArrayList<Flight>();

		ScheduleSearchDTO scheduleSearchDTO = new ScheduleSearchDTO();
		scheduleSearchDTO.setStartDate(avsSegment.getDepartureDate());
		scheduleSearchDTO.setCsOCFlightNumber(flightDesignator);
		scheduleSearchDTO.setCsOCCarrierCode(operatingCarrier);
		scheduleSearchDTO.setEndDate(CalendarUtil.getOfssetAddedTime(avsSegment.getDepartureDate(), 1));
		scheduleSearchDTO.setLocalTime(true);
		List<String> statusFilters = new ArrayList<String>();
		statusFilters.add(FlightStatusEnum.CREATED.toString());
		statusFilters.add(FlightStatusEnum.ACTIVE.toString());
		scheduleSearchDTO.setStatusFilter(statusFilters);
		scheduleSearchDTO.setEnableWildcard(true);
		flightColl = GDSServicesModuleUtil.getFlightBD().getAvailableAdhocFlights(scheduleSearchDTO);

		if (flightColl == null || flightColl.isEmpty()) {
			throw new ModuleException("gdsservices.airschedules.logic.bl.data.not.exist");
		}

		return flightColl;
	}

	private static boolean isValidSegment(String segmentCode, String origin, String destination, String status,
			Set<FlightSegement> flightSegements, String bc, String fullSegmentCode) {
		if (!StringUtil.isNullOrEmpty(segmentCode) && !StringUtil.isNullOrEmpty(origin) && !StringUtil.isNullOrEmpty(destination)
				&& !StringUtil.isNullOrEmpty(status) && !StringUtil.isNullOrEmpty(fullSegmentCode)) {

			String airportCodeArr[] = segmentCode.split("/");
			List<String> airports = Arrays.asList(airportCodeArr);
			if (status.startsWith(C_AVAIL)) {
				if ((airports.contains(origin) && airports.contains(destination))
						|| (airports.contains(origin) && !origin.equals(airportCodeArr[airportCodeArr.length - 1]))
						|| (airports.contains(destination) && !destination.equals(airportCodeArr[0]))) {
					return true;
				}

				List<String> effectiveCodeList = getEffectiveAirportCodes(fullSegmentCode, origin, destination);
				if (effectiveCodeList.contains(airportCodeArr[0])
						&& effectiveCodeList.contains(airportCodeArr[airportCodeArr.length - 1])) {
					return true;
				}

			} else if (status.startsWith(L_AVAIL)
					&& (origin.equals(airportCodeArr[0]) && destination.equals(airportCodeArr[airportCodeArr.length - 1]))) {
				return true;
			} else if (status.startsWith(A_AVAIL_S)) {
				List<String> effectiveCodeList = getEffectiveAirportCodes(fullSegmentCode, origin, destination);
				if (effectiveCodeList.contains(airportCodeArr[0])
						&& effectiveCodeList.contains(airportCodeArr[airportCodeArr.length - 1])) {
					return true;
				} else if (effectiveCodeList.contains(airportCodeArr[0])) {
					if (!StringUtil.isNullOrEmpty(bc) && flightSegements != null && !flightSegements.isEmpty()) {
						// also verify remaining segments Open, Ex: AB => ABC do we need to check status of BC?????
						/*
						 * Ex1: BAHDXB => BAH/DXB,BAH/DXB/BOM
						 */

						for (FlightSegement flightSegement : flightSegements) {

							String tempCodeArr[] = flightSegement.getSegmentCode().split("/");

							if (tempCodeArr[0].equals(destination)
									&& tempCodeArr[tempCodeArr.length - 1].equals(airportCodeArr[airportCodeArr.length - 1])) {
								FCCSegBCInventory fccSegBCInventory = GDSServicesModuleUtil.getFlightInventoryBD()
										.getFCCSegBCInventory(flightSegement.getFltSegId(), bc);
								if (FCCSegBCInventory.Status.OPEN.equals(fccSegBCInventory.getStatus())) {
									return true;
								} else {
									return false;
								}
							}
						}

					}
				}
			}
		}

		return false;
	}

	private static List<String> getEffectiveAirportCodes(String fullSegmentCode, String origin, String destination) {
		String fullJourneyAirport[] = fullSegmentCode.split("/");
		List<String> journeyAirportList = Arrays.asList(fullJourneyAirport);
		List<String> effectiveCodeList = new ArrayList<String>();
		boolean found = false;
		for (String code : journeyAirportList) {
			if (code.equals(origin)) {
				found = true;
			}

			if (found) {
				effectiveCodeList.add(code);
			}

			if (code.equals(destination)) {
				found = false;
			}

		}

		return effectiveCodeList;
	}

	private static String getSegment(Set<FlightSegement> set) {
		String strSeg = "";
		String strSegArray = "";
		int prevLen = 0;
		FlightSegement seg = null;
		if (set != null) {
			Iterator<FlightSegement> ite = set.iterator();

			if (ite != null) {
				while (ite.hasNext()) {
					seg = (FlightSegement) ite.next();
					strSeg = seg.getSegmentCode();
					if (prevLen < strSeg.length()) {
						strSegArray = strSeg;
						prevLen = strSeg.length();
					}
				}
			}
		}
		return strSegArray;
	}

	private static List<String> avsIgnoreBookingClassList(String gdsIdStr) {

		GDSServicesModuleConfig gdsModuleconfig = GDSServicesModuleUtil.getConfig();
		Map<Integer, List<String>> avsSkipProcessBookingCodes = gdsModuleconfig.getAvsSkipProcessBookingCodes();

		if (avsSkipProcessBookingCodes != null && !avsSkipProcessBookingCodes.isEmpty()
				&& avsSkipProcessBookingCodes.get(gdsIdStr) != null && !avsSkipProcessBookingCodes.get(gdsIdStr).isEmpty()) {
			return avsSkipProcessBookingCodes.get(gdsIdStr);
		}
		return null;
	}

	private static List<String> avsProcessingAllowedActualBCList(Integer gdsId, List<String> avsIgnoreExternalBCs) {

		List<String> avsIgnoreActualBCs = new ArrayList<String>();
		List<String> actualBcList = new ArrayList<String>();

		// avsSkipProcessBookingCodes defined needs to be skipped while
		if (avsIgnoreExternalBCs != null && !avsIgnoreExternalBCs.isEmpty()) {
			if (avsIgnoreExternalBCs != null && !avsIgnoreExternalBCs.isEmpty()) {
				Iterator<String> ignoreExternalBCsItr = avsIgnoreExternalBCs.iterator();
				while (ignoreExternalBCsItr.hasNext()) {
					String externalBc = ignoreExternalBCsItr.next();
					if (!StringUtil.isNullOrEmpty(externalBc)) {
						String bookingCode = TTYMessageUtil.getActualBCForExternalCsBC(gdsId, externalBc);
						if (!StringUtil.isNullOrEmpty(bookingCode)) {
							avsIgnoreActualBCs.add(bookingCode);
						}
					}

				}

			}

		}
		actualBcList = TTYMessageUtil.getSingleEntryMappedActualBCList(gdsId);

		if (actualBcList != null && !actualBcList.isEmpty() && avsIgnoreActualBCs != null && !avsIgnoreActualBCs.isEmpty()) {
			actualBcList.removeAll(avsIgnoreActualBCs);
		}

		return actualBcList;
	}

	private void updateBCInvStatus(FCCSegBCInventory bcInv, String statusCode, boolean isCreateMode) {
		if (statusCode.startsWith(A_AVAIL_S) || L_AVAIL.equals(statusCode) || RE_OPENED_FOR_SALES.equals(statusCode)) {
			bcInv.setStatus(FCCSegBCInventory.Status.OPEN);
			if (!isCreateMode) {
				bcInv.setStatusChangeAction(FCCSegBCInventory.StatusAction.SEAT_AQUISITION);
			}
		} else {
			bcInv.setStatus(FCCSegBCInventory.Status.CLOSED);
			bcInv.setStatusChangeAction(FCCSegBCInventory.StatusAction.AVAILABILITY_ZERO);
		}
	}

	private Integer getEffectiveSeatAllocation(FCCSegBCInventory fccSegBCInventory, AVSRequestSegmentDTO avsSegment) {

		int totalConsumed = (fccSegBCInventory.getSeatsSold() + fccSegBCInventory.getOnHoldSeats() + fccSegBCInventory
				.getSeatsCancelled()) - fccSegBCInventory.getSeatsAcquired();
		Integer seatsAvailable = avsSegment.getSeatsAvailable();
		Integer allocation = 0;
		if (seatsAvailable != null && avsSegment.isNumericAvailability()) {
			allocation = totalConsumed + seatsAvailable;
		} else if (RE_OPENED_FOR_SALES.equals(avsSegment.getStatusCode()) && !avsSegment.isNumericAvailability()) {
			allocation = totalConsumed + DEFAULT_SEAT_ALLOCATION;
		} else {
			allocation = fccSegBCInventory.getSeatsAllocated();
		}

		if (allocation < 0) {
			allocation = 0;
		}
		return allocation;
	}

	private void updateFlightSegmentInventory(Collection<String> rbdColl, Collection<Flight> flightColl,
			AVSRequestSegmentDTO avsSegment) throws ModuleException {

		if (rbdColl != null && !rbdColl.isEmpty()) {
			String origin = avsSegment.getDepartureStation();
			String destination = avsSegment.getDestinationStation();
			boolean isAllLeg = false;
			if (StringUtil.isNullOrEmpty(origin) && StringUtil.isNullOrEmpty(destination)) {
				isAllLeg = true;
			}
			Integer seatsAvailable = avsSegment.getSeatsAvailable();
			for (String bc : rbdColl) {
				if (flightColl != null && !flightColl.isEmpty()) {
					String logicalCC = TTYMessageUtil.getLogicalCCByActualBookingCode(bc);
					for (Flight flight : flightColl) {
						if (logicalCC != null && FlightStatusEnum.CREATED.toString().equals(flight.getStatus())
								|| FlightStatusEnum.ACTIVE.toString().equals(flight.getStatus())) {
							String fullSegmentCode = getSegment(flight.getFlightSegements());
							for (FlightSegement flightSegement : flight.getFlightSegements()) {

								boolean validSegment = isValidSegment(flightSegement.getSegmentCode(), origin, destination,
										avsSegment.getStatusCode(), flight.getFlightSegements(), bc, fullSegmentCode);

								if (isAllLeg || validSegment) {

									FCCSegBCInventory fccSegBCInventory = GDSServicesModuleUtil.getFlightInventoryBD()
											.getFCCSegBCInventory(flightSegement.getFltSegId(), bc);

									FCCSegInventory fccSegInventory = GDSServicesModuleUtil.getFlightInventoryBD()
											.getFCCSegInventory(flightSegement.getFltSegId(), logicalCC);

									FCCSegInventoryUpdateDTO fCCSegInventoryUpdateDTO = new FCCSegInventoryUpdateDTO();

									if (fccSegInventory != null) {
										if (fccSegBCInventory == null) {

											Set<FCCSegBCInventory> setAddedInventories = null;
											if (setAddedInventories == null) {
												setAddedInventories = new HashSet<FCCSegBCInventory>();
												fCCSegInventoryUpdateDTO.setAddedFCCSegBCInventories(setAddedInventories);
											}
											FCCSegBCInventory bcInv = new FCCSegBCInventory();
											bcInv.setfccsInvId(fccSegInventory.getFccsInvId());
											bcInv.setFlightId(flight.getFlightId());
											bcInv.setSegmentCode(flightSegement.getSegmentCode());
											bcInv.setLogicalCCCode(logicalCC);
											bcInv.setBookingCode(bc);
											bcInv.setPriorityFlag(true);
											bcInv.setAllocatedWaitListSeats(0);

											// we don't allow 0 allocation, as if same in admin screen as
											// well.
											if (seatsAvailable > 0) {
												bcInv.setSeatsAllocated(seatsAvailable);
											} else {
												bcInv.setSeatsAllocated(DEFAULT_SEAT_ALLOCATION);
											}
											boolean isCreateMode = false;
											if (FLIGHT_OPEN.equals(avsSegment.getStatusCode())) {
												bcInv.setStatusChangeAction(FCCSegBCInventory.StatusAction.CREATION);
												isCreateMode = true;
											}

											updateBCInvStatus(bcInv, avsSegment.getStatusCode(), isCreateMode);

											setAddedInventories.add(bcInv);
										} else {
											Set<FCCSegBCInventory> setEditedInventories = null;
											if (setEditedInventories == null) {
												setEditedInventories = new HashSet<FCCSegBCInventory>();
												fCCSegInventoryUpdateDTO.setUpdatedFCCSegBCInventories(setEditedInventories);
											}

											FCCSegBCInventory bcInv = new FCCSegBCInventory();
											bcInv.setFccsbInvId(fccSegBCInventory.getFccsbInvId());
											bcInv.setfccsInvId(fccSegBCInventory.getfccsInvId());
											bcInv.setFlightId(fccSegBCInventory.getFlightId());
											bcInv.setSegmentCode(fccSegBCInventory.getSegmentCode());
											bcInv.setLogicalCCCode(fccSegBCInventory.getLogicalCCCode());
											bcInv.setBookingCode(fccSegBCInventory.getBookingCode());
											bcInv.setPriorityFlag(fccSegBCInventory.getPriorityFlag());

											bcInv.setSeatsAllocated(getEffectiveSeatAllocation(fccSegBCInventory, avsSegment));

											bcInv.setAllocatedWaitListSeats(fccSegBCInventory.getAllocatedWaitListSeats());

											updateBCInvStatus(bcInv, avsSegment.getStatusCode(), false);

											bcInv.setSeatsAcquired(fccSegBCInventory.getSeatsAcquired());
											bcInv.setVersion(fccSegBCInventory.getVersion());
											bcInv.setSeatsSold(fccSegBCInventory.getSeatsSold());

											setEditedInventories.add(bcInv);
										}

										fCCSegInventoryUpdateDTO.setFlightId(flight.getFlightId());
										fCCSegInventoryUpdateDTO.setFccsInventoryId(fccSegInventory.getFccsInvId());
										fCCSegInventoryUpdateDTO.setLogicalCCCode(logicalCC);
										fCCSegInventoryUpdateDTO.setSegmentCode(flightSegement.getSegmentCode());
										fCCSegInventoryUpdateDTO.setInfantAllocation(fccSegInventory.getInfantAllocation());
										fCCSegInventoryUpdateDTO.setVersion(fccSegInventory.getVersion());

										FCCSegInventoryUpdateStatusDTO updateStatusDTO = GDSServicesModuleUtil
												.getFlightInventoryBD().updateFCCSegInventory(fCCSegInventoryUpdateDTO);

										if (updateStatusDTO.getStatus() == OperationStatusDTO.OPERATION_SUCCESS
												&& FlightStatusEnum.CREATED.toString().equals(flight.getStatus())) {

											int bcInvCount = GDSServicesModuleUtil.getFlightInventoryBD()
													.getFCCSegBCInventoriesCount(flight.getFlightId());
											Collection<Integer> flightIds = new ArrayList<Integer>();
											flightIds.add(new Integer(flight.getFlightId()));
											if (bcInvCount > 0) {// check status update is really required
												GDSServicesModuleUtil.getFlightBD().changeFlightStatus(flightIds,
														FlightStatusEnum.ACTIVE);
											}

										}
									}

								}

							}
							break;
						}

					}
				}
			}

		}
	}
}
