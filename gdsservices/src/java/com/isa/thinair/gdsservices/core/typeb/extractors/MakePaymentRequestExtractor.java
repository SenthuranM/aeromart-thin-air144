package com.isa.thinair.gdsservices.core.typeb.extractors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.BSPDetail;
import com.isa.thinair.gdsservices.api.dto.internal.CreditCardDetail;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.MakePaymentRequest;
import com.isa.thinair.gdsservices.api.dto.internal.PaymentDetail;
import com.isa.thinair.gdsservices.api.dto.internal.SSRData;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

public class MakePaymentRequestExtractor extends ValidatorBase {
	private static final Log log = LogFactory.getLog(MakePaymentRequestExtractor.class);

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {

		String pnrReference = ValidatorUtils.getPNRReference(bookingRequestDTO.getResponderRecordLocator());

		if (pnrReference.length() == 0) {
			String message = MessageUtil.getMessage("gdsservices.extractors.emptyPNRResponder");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		} else {
			SSRData ssrData = new SSRData(bookingRequestDTO.getSsrDTOs());
			PaymentDetail paymentDetail = SSRExtractorUtil.getPaymentDetail(ssrData);

			if (paymentDetail == null) {
				String message = MessageUtil.getMessage("gdsservices.extractors.empty.payment");
				bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

				this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
				return bookingRequestDTO;
			} else if (paymentDetail instanceof BSPDetail && ((BSPDetail) paymentDetail).isEmpty()) {
				String message = MessageUtil.getMessage("gdsservices.extractors.empty.bspdetails");
				bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

				this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
				return bookingRequestDTO;
			} else if (paymentDetail instanceof CreditCardDetail && ((CreditCardDetail) paymentDetail).isEmpty()) {
				String message = MessageUtil.getMessage("gdsservices.extractors.empty.ccdetails");
				bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

				this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
				return bookingRequestDTO;
			} else {
				// There aren't any MakePaymentRequestExtractor specific validations.
				// The General Action codes are validated at the high level.
				this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
				return bookingRequestDTO;
			}
		}
	}

	/**
	 * returns MakePaymentRequest
	 * 
	 * @param gdsCredentialsDTO
	 * @param ssrData
	 * @param bookingRequestDTO
	 * @return
	 */
	public static MakePaymentRequest getRequest(GDSCredentialsDTO gdsCredentialsDTO, SSRData ssrData,
			BookingRequestDTO bookingRequestDTO) {
		MakePaymentRequest makePaymentRequest = new MakePaymentRequest();

		makePaymentRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
		makePaymentRequest = (MakePaymentRequest) RequestExtractorUtil.addBaseAttributes(makePaymentRequest, bookingRequestDTO);

		makePaymentRequest.setPaymentDetail(SSRExtractorUtil.getPaymentDetail(ssrData));
		makePaymentRequest.setNotSupportedSSRExists(bookingRequestDTO.isNotSupportedSSRExists());

		return makePaymentRequest;
	}

}
