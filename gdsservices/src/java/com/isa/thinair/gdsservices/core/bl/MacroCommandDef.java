/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.core.bl;

import com.isa.thinair.commons.core.framework.Command;

/**
 * macro command definitions goes here
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command.macro-def
 */
public final class MacroCommandDef {

	private MacroCommandDef() {
	}

	/**
	 * @isa.module.command.macro-command name="publishAmmendScheduleMacro"
	 * @isa.module.command.macro-map order="1" inner-command="publishUpdateSSM"
	 */
	private Command publishAmmendScheduleMacro;

}
