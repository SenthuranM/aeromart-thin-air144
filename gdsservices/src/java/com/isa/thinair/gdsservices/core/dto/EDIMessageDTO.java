package com.isa.thinair.gdsservices.core.dto;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.typea.common.BaseEDIRequest;
import com.isa.typea.common.BaseEDIResponse;

public class EDIMessageDTO {

	private TypeAConstants.MessageFlow messageFlow;
	private String rawRequest;
	private EDIMessageMetaDataDTO requestMetaDataDTO;
	private String reqAssociationCode;
	private BaseEDIRequest request;
	private String rawResponse;
	private EDIMessageMetaDataDTO responseMetaDataDTO;
	private String respAssociationCode;
	private BaseEDIResponse response;
	private Map<String, Object> messagingSession;
	private String gdsCode;
	private boolean invocationError;
	private boolean rollbackTransaction;
	private boolean sendResponse;
	private String requestIp;
	private List<String> errors;

	public EDIMessageDTO(TypeAConstants.MessageFlow messageFlow) {
		this.messageFlow = messageFlow;
		this.messagingSession = new HashMap<String, Object>();
		this.invocationError = false;
		this.rollbackTransaction = false;
		this.sendResponse = true;
		this.errors = new ArrayList<>();
	}

	public TypeAConstants.MessageFlow getMessageFlow() {
		return messageFlow;
	}

	public void setMessageFlow(TypeAConstants.MessageFlow messageFlow) {
		this.messageFlow = messageFlow;
	}

	public String getRawRequest() {
		return rawRequest;
	}

	public void setRawRequest(String rawRequest) {
		this.rawRequest = rawRequest;
	}

	public EDIMessageMetaDataDTO getRequestMetaDataDTO() {
		return requestMetaDataDTO;
	}

	public void setRequestMetaDataDTO(EDIMessageMetaDataDTO messageMetaDataDTO) {
		this.requestMetaDataDTO = messageMetaDataDTO;
	}

	public String getReqAssociationCode() {
		return reqAssociationCode;
	}

	public void setReqAssociationCode(String reqAssociationCode) {
		this.reqAssociationCode = reqAssociationCode;
	}

	public BaseEDIRequest getRequest() {
		return request;
	}

	public void setRequest(BaseEDIRequest request) {
		this.request = request;
	}

	public String getRawResponse() {
		return rawResponse;
	}

	public void setRawResponse(String rawResponse) {
		this.rawResponse = rawResponse;
	}

	public EDIMessageMetaDataDTO getResponseMetaDataDTO() {
		return responseMetaDataDTO;
	}

	public void setResponseMetaDataDTO(EDIMessageMetaDataDTO responseMetaDataDTO) {
		this.responseMetaDataDTO = responseMetaDataDTO;
	}

	public String getRespAssociationCode() {
		return respAssociationCode;
	}

	public void setRespAssociationCode(String respAssociationCode) {
		this.respAssociationCode = respAssociationCode;
	}

	public BaseEDIResponse getResponse() {
		return response;
	}

	public void setResponse(BaseEDIResponse response) {
		this.response = response;
	}

	public Map<String, Object> getMessagingSession() {
		return messagingSession;
	}

	public void setMessagingSession(Map<String, Object> messagingSession) {
		this.messagingSession = messagingSession;
	}

	public void putMessagingSessionData(String key, Object data) {
		this.messagingSession.put(key, data);
	}

	public String getGdsCode() {
		return gdsCode;
	}

	public void setGdsCode(String gdsCode) {
		this.gdsCode = gdsCode;
	}

	public boolean isInvocationError() {
		return invocationError;
	}

	public void setInvocationError(boolean invocationError) {
		this.invocationError = invocationError;
	}

	public boolean isRollbackTransaction() {
		return rollbackTransaction;
	}

	public void setRollbackTransaction(boolean rollbackTransaction) {
		this.rollbackTransaction = rollbackTransaction;
	}

	public boolean isSendResponse() {
		return sendResponse;
	}

	public void setSendResponse(boolean sendResponse) {
		this.sendResponse = sendResponse;
	}

	public String getRequestIp() {
		return requestIp;
	}

	public void setRequestIp(String requestIp) {
		this.requestIp = requestIp;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void addErrors(String error) {
		this.errors.add(error);
	}
}
