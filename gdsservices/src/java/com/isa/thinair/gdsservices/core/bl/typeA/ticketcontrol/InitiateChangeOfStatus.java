package com.isa.thinair.gdsservices.core.bl.typeA.ticketcontrol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketChangeOfStatusRQ;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketChangeOfStatusRS;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageInitiator;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.dto.EDIMessageDTO;
import com.isa.thinair.gdsservices.core.dto.EDIMessageMetaDataDTO;
import com.isa.thinair.gdsservices.core.dto.temp.GdsReservation;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerTicketControlInformationWrapper;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerTicketingInformationWrapper;
import com.isa.thinair.gdsservices.core.typeA.parser.TypeAParseConstants;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.GdsInfoDaoHelper;
import com.isa.thinair.gdsservices.core.util.GdsUtil;
import com.isa.thinair.gdsservices.core.util.KeyGenerator;
import com.isa.thinair.gdsservices.core.util.TypeACodesMapper;
import com.isa.typea.common.FlightInformation;
import com.isa.typea.common.Header;
import com.isa.typea.common.Location;
import com.isa.typea.common.MessageFunction;
import com.isa.typea.common.MessageHeader;
import com.isa.typea.common.OriginatorInformation;
import com.isa.typea.common.TicketControlTicketNumberDetails;
import com.isa.typea.common.TicketCoupon;
import com.isa.typea.common.TicketNumberDetails;
import com.isa.typea.common.Traveller;
import com.isa.typea.common.TravellerInformation;
import com.isa.typea.common.TravellerTicketingInformation;
import com.isa.typea.tkcreq.AATKCREQ;
import com.isa.typea.tkcreq.TKCREQMessage;

public class InitiateChangeOfStatus extends EdiMessageInitiator {

	private TicketChangeOfStatusRQ ticketChangeOfStatusRQ;

	protected void prepareMessages() throws ModuleException {
		ticketChangeOfStatusRQ = (TicketChangeOfStatusRQ) getParameter(TypeACommandParamNames.TICKET_CONTROL_DTO);

		List<Integer> ppfsIds = new ArrayList<Integer>();
		ppfsIds.addAll(ticketChangeOfStatusRQ.getCouponStatus().keySet());
		List<LCCClientReservation> reservations = GDSServicesDAOUtils.DAOInstance.GDS_TypeA_JDBC_DAO.loadReservations(ppfsIds);

		handleChangeOfStatus(reservations);
		constructMessages(reservations);
	}


	private void constructMessages(List<LCCClientReservation> reservations) throws ModuleException {

		String carrierCode = AppSysParamsUtil.getCarrierCode();

		EDIMessageDTO ediMessageDTO;
		int gdsId;
		Map<String, TicketControlTicketNumberDetails> ticketNumbers;
		TicketControlTicketNumberDetails ticketNumberDetails;
		String couponStatus;

		Location location;
		FlightInformation.CompanyInfo companyInfo;
		FlightInformation.FlightPreference flightPreference;
		Map<String, GDSStatusTO> gdsStatusMap = CommonsServices.getGlobalConfig().getActiveGdsMap();
		GDSStatusTO gdsStatusTO;

		for (LCCClientReservation reservation : reservations) {

			ediMessageDTO = new EDIMessageDTO(TypeAConstants.MessageFlow.INITIATE);
			gdsId = reservation.getGdsId();
			gdsStatusTO = gdsStatusMap.get(String.valueOf(gdsId));

			if (!gdsStatusTO.isAaValidatingCarrier()) {
				ticketNumbers = new HashMap<String, TicketControlTicketNumberDetails>();

				AATKCREQ tkcReq = new AATKCREQ();
				MessageHeader messageHeader = GDSDTOUtil.createInitMessageHeader(IATAOperation.TKCREQ, gdsId, null);
				Header header = GDSDTOUtil.createInitEdiHeader(gdsId, messageHeader, null);
				tkcReq.setHeader(header);

				TKCREQMessage tkcreqMessage = new TKCREQMessage();
				tkcReq.setMessage(tkcreqMessage);
				tkcreqMessage.setMessageHeader(messageHeader);

				MessageFunction messageFunction = new MessageFunction();
				messageFunction.setMessageFunction(TypeAConstants.MessageFunction.CHANGE_OF_STATUS);
				tkcreqMessage.setMessageFunction(messageFunction);

				OriginatorInformation originatorInfo = new OriginatorInformation();
				tkcreqMessage.setOriginatorInformation(originatorInfo);

				LCCClientReservationSegment segment;
				for (LCCClientReservationPax pax : reservation.getPassengers()) {
					for (LccClientPassengerEticketInfoTO eticketInfoTO : pax.geteTickets()) {
						segment = null;
						for (LCCClientReservationSegment seg : reservation.getSegments()) {
							if (eticketInfoTO.getFlightSegmentRef().equals(seg.getFlightSegmentRefNumber())) {
								segment = seg;
								break;
							}
						}

						if (!ticketNumbers.containsKey(eticketInfoTO.getExternalPaxETicketNo())) {
							ticketNumberDetails = new TicketControlTicketNumberDetails();
							ticketNumberDetails.setTicketNumber(eticketInfoTO.getExternalPaxETicketNo());
							ticketNumberDetails.setDocumentType(TypeAConstants.DocumentType.TICKET);

							if (isRequired(TypeAParseConstants.Segment.TIF)) {
								TravellerInformation travellerInformation = new TravellerInformation();
								travellerInformation.setTravellerSurname(pax.getLastName());
								Traveller traveller = new Traveller();
								traveller.setGivenName(pax.getFirstName());
								travellerInformation.getTravellers().add(traveller);
								ticketNumberDetails.setTravellerInformation(travellerInformation);
							}


							tkcreqMessage.getTickets().add(ticketNumberDetails);

							ticketNumbers.put(eticketInfoTO.getExternalPaxETicketNo(), ticketNumberDetails);
						}

						ticketNumberDetails = ticketNumbers.get(eticketInfoTO.getExternalPaxETicketNo());

						TicketCoupon ticketCoupon = new TicketCoupon();
						ticketCoupon.setCouponNumber(String.valueOf(eticketInfoTO.getExternalCouponNo()));
						if (ticketChangeOfStatusRQ.isOverrideStatus()) {
							couponStatus = ticketChangeOfStatusRQ.getCouponStatus().get(eticketInfoTO.getPpfsId());
							ticketCoupon.setStatus(couponStatus);
						} else {
							ticketCoupon.setStatus(TypeACodesMapper.getCouponStatus(eticketInfoTO.getPaxETicketStatus()));
						}

						if (segment != null && isRequired(TypeAParseConstants.Segment.TVL)) {
							FlightInformation flightInfo = new FlightInformation();
							try {
								flightInfo.setDepartureDateTime(CalendarUtil.asXMLGregorianCalendar(segment.getDepartureDate()));
								flightInfo.setArrivalDateTime(CalendarUtil.asXMLGregorianCalendar(segment.getArrivalDate()));
							} catch (DatatypeConfigurationException e) {
								throw new ModuleException("");
							}


							location = new Location();
							location.setLocationCode(GdsUtil.getDepartingAirport(segment.getSegmentCode()));
							flightInfo.setDepartureLocation(location);

							location = new Location();
							location.setLocationCode(GdsUtil.getArrivalAirport(segment.getSegmentCode()));
							flightInfo.setArrivalLocation(location);

							companyInfo = new FlightInformation.CompanyInfo();
							companyInfo.setMarketingAirlineCode(carrierCode);
							flightInfo.setCompanyInfo(companyInfo);

							flightInfo.setFlightNumber(segment.getFlightNo().substring(carrierCode.length()));

							flightPreference = new FlightInformation.FlightPreference();
							flightPreference.setBookingClass(segment.getFareTO().getBookingClassCode());
							flightInfo.setFlightPreference(flightPreference);

							ticketCoupon.setFlightInfomation(flightInfo);
						}

						ticketNumberDetails.getTicketCoupon().add(ticketCoupon);

					}
				}

				tkcReq.setMessage(tkcreqMessage);

				ediMessageDTO.setRequest(tkcReq);

				EDIMessageMetaDataDTO metaDataDTO = new EDIMessageMetaDataDTO();
				metaDataDTO.setIataOperation(iataOperation);
				metaDataDTO.setMajorVersion(messageHeader.getMessageId().getVersion());
				metaDataDTO.setMinorVersion(messageHeader.getMessageId().getReleaseNumber());
				metaDataDTO.setMessageReference(messageHeader.getCommonAccessReference());
				ediMessageDTO.setRequestMetaDataDTO(metaDataDTO);

				ediMessageDTO.setReqAssociationCode(header.getInterchangeHeader().getAssociationCode());

				Gds gds = GdsUtil.getGds(gdsId);
				ediMessageDTO.setGdsCode(gds.getGdsCode());

				ediMessageDTOs.add(ediMessageDTO);

			}
		}
	}

	private void handleChangeOfStatus(List<LCCClientReservation> reservations) throws ModuleException {

		GdsReservation<TravellerTicketingInformationWrapper> gdsTicketReservation = null;
		GdsReservation<TravellerTicketControlInformationWrapper> gdsTicketControlReservation = null;

		Map<String, GDSStatusTO> gdsStatusMap = CommonsServices.getGlobalConfig().getActiveGdsMap();
		boolean aaValidatingCarrier;
		TicketCoupon ticketCoupon;
		TicketCoupon transitionTicketCoupon;
		String settlementAuthCode;
		String couponStatus;
		OriginatorInformation originatorInformation = GDSDTOUtil.createOriginatorInformation();


		TravellerTicketingInformationWrapper travellerWrapper;
		for (LCCClientReservation reservation : reservations)  {

			aaValidatingCarrier = gdsStatusMap.get(String.valueOf(reservation.getGdsId())).isAaValidatingCarrier();
			if (aaValidatingCarrier) {
				gdsTicketReservation = GdsInfoDaoHelper.getGdsReservationTicketView(reservation.getPNR());
			} else {
				gdsTicketControlReservation = GdsInfoDaoHelper.getGdsReservationTicketControlView(reservation.getPNR());
			}

			for (LCCClientReservationPax pax : reservation.getPassengers()) {

				settlementAuthCode = KeyGenerator.generateSettlementAuthorizationCode();
				for (LccClientPassengerEticketInfoTO eticketInfoTO : pax.geteTickets()) {
					ticketCoupon = null;

					if (aaValidatingCarrier && gdsTicketReservation != null) {
						ticketCoupon = GDSDTOUtil.resolveTicketCoupon(gdsTicketReservation.getTravellerInformation(), pax, eticketInfoTO);

						if (ticketCoupon != null) {
							ticketCoupon.setSettlementAuthCode(settlementAuthCode);
							if (ticketChangeOfStatusRQ.isOverrideStatus()) {
								couponStatus = ticketChangeOfStatusRQ.getCouponStatus().get(eticketInfoTO.getPpfsId());

							} else {
								couponStatus = TypeACodesMapper.getCouponStatus(eticketInfoTO.getPaxETicketStatus());
							}
							ticketCoupon.setStatus(couponStatus);

							travellerWrapper = GDSDTOUtil.resolvePassenger(gdsTicketReservation, pax);
							TravellerTicketingInformation travellerImage = travellerWrapper.getTravellerInformation();

							if (travellerWrapper != null) {

								List<TicketNumberDetails> transitions = new ArrayList<TicketNumberDetails>();

								for (TicketNumberDetails ticket : travellerImage.getTickets()) {

									if (ticket.getTicketNumber().equals(eticketInfoTO.getExternalPaxETicketNo())) {
										TicketNumberDetails ticketNumberDetails = new TicketNumberDetails();
										ticketNumberDetails.setTicketNumber(ticket.getTicketNumber());
										ticketNumberDetails.setDocumentType(ticket.getDocumentType());
										transitions.add(ticketNumberDetails);

										for (TicketCoupon coupon : ticket.getTicketCoupon()) {

											if (coupon.getCouponNumber().equals(String.valueOf(eticketInfoTO.getExternalCouponNo()))) {
												transitionTicketCoupon = new TicketCoupon();
												transitionTicketCoupon.setStatus(coupon.getStatus());
												transitionTicketCoupon.setCouponNumber(coupon.getCouponNumber());
												transitionTicketCoupon.setSettlementAuthCode(coupon.getSettlementAuthCode());
												transitionTicketCoupon.setOriginatorInformation(originatorInformation);
												transitionTicketCoupon.setFlightInfomation(coupon.getFlightInfomation());
												transitionTicketCoupon.setRelatedProductInfo(coupon.getRelatedProductInfo());

												ticketNumberDetails.getTicketCoupon().add(transitionTicketCoupon);
											}
										}
									}


								}

								GDSDTOUtil.mergeCouponTransitions(travellerWrapper.getCouponTransitions(), transitions, originatorInformation);

							}
						}

					} else if (!aaValidatingCarrier && gdsTicketControlReservation != null) {
						ticketCoupon = GDSDTOUtil.resolveTicketControlCoupon(gdsTicketControlReservation.getTravellerInformation(), pax, eticketInfoTO);
					}

					if (ticketCoupon != null) {

					}
				}
			}

			if (aaValidatingCarrier) {
				GdsInfoDaoHelper.saveGdsReservationTicketView(reservation.getPNR(), gdsTicketReservation);
			} else {
				GdsInfoDaoHelper.saveGdsReservationTicketControlView(reservation.getPNR(), gdsTicketControlReservation);

			}

		}
	}

	private boolean isRequired(TypeAParseConstants.Segment segment) throws ModuleException {
		boolean isRequired = false;
		switch (ticketChangeOfStatusRQ.getRequestType()) {
		case CHANGE_OF_STATUS_TO_FINAL:
			switch (segment) {
			case TIF:
			case TVL:
				isRequired = true;
				break;
			}
			break;
		case TICKET_REISSUE:
			switch (segment) {
			case TIF:
			case TVL:
				isRequired = false;
				break;
			}
			break;
		}

		return isRequired;
	}

	protected void processResponse() {
		TicketChangeOfStatusRS resp = new TicketChangeOfStatusRS();
		boolean success;
		if (!ediMessageDTOs.isEmpty()) {
			success = true;
			for (EDIMessageDTO edi : ediMessageDTOs) {
				if (edi.isInvocationError()) {
					success = false;
					break;
				}
			}
			resp.setSuccess(success);
		} else {
			resp.setSuccess(true);
		}

		respData.put(TypeACommandParamNames.TICKET_CONTROL_DTO, resp);
	}
}
