package com.isa.thinair.gdsservices.core.typeA.transformers.paoreqres.v03;

import static com.isa.thinair.gdsservices.core.util.TypeADataConvertorUtill.getDate;
import static com.isa.thinair.gdsservices.core.util.TypeADataConvertorUtill.getValue;
import iata.typea.v031.paoreq.ABI;
import iata.typea.v031.paoreq.EQN;
import iata.typea.v031.paoreq.FTI;
import iata.typea.v031.paoreq.GROUP1;
import iata.typea.v031.paoreq.GROUP2;
import iata.typea.v031.paoreq.GROUP3;
import iata.typea.v031.paoreq.GROUP4;
import iata.typea.v031.paoreq.MSG;
import iata.typea.v031.paoreq.ODI;
import iata.typea.v031.paoreq.ORG;
import iata.typea.v031.paoreq.PAOREQ;
import iata.typea.v031.paoreq.PDI;
import iata.typea.v031.paoreq.RCI;
import iata.typea.v031.paoreq.RPI;
import iata.typea.v031.paoreq.SSR;
import iata.typea.v031.paoreq.TIF;
import iata.typea.v031.paoreq.TVL;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.CabinClassAvailabilityDTO;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.core.typeA.helpers.Constants;

public class SeamlessAvailabilityRequestHandler {

	public static Map<String, Object> getOwnMessageParams(PAOREQ paoreq) {

		Map<String, Object> params = new HashMap<String, Object>();

		/* Group 1 */
		GROUP1 group1 = paoreq.getGROUP1();
		if (group1 != null) {
			ORG orgL1 = group1.getORG();

			for (ABI abiL2 : group1.getABI()) {

			}
		}

		PDI pdiL0 = paoreq.getPDI();
		EQN eqnL0 = paoreq.getEQN();
		RPI rpiL0 = paoreq.getRPI();
		RCI rciL0 = paoreq.getRCI();
		List<SSR> ssrL1 = paoreq.getSSR();

		/* Group 2 */
		for (GROUP2 group2 : paoreq.getGROUP2()) {
			TIF tifL1 = group2.getTIF();
			List<FTI> ftiL2 = group2.getFTI();
		}

		/* Group 3 */
		Map<String, List<CabinClassAvailabilityDTO>> odiCcdtoMap = new HashMap<String, List<CabinClassAvailabilityDTO>>();
		for (GROUP3 group3 : paoreq.getGROUP3()) {

			MSG msgL2 = group3.getMSG();
			String messageFunction = (String) getValue(msgL2,
					"getMSG01MessageFunctionBusinessDetails().getMSG0102MessageFunctionCoded()");
			if (CodeSetEnum.CS1225.NoActionRequired != CodeSetEnum.CS1225.getEnum(messageFunction)) {

				ODI odiL1 = group3.getODI();
				String odiL1Str = "";
				for (String placeLocationIdentification : odiL1.getODI01()) {
					odiL1Str += (Constants.Separators.SEGMENT_SEPARATOR + placeLocationIdentification);
				}
				odiL1Str = odiL1Str.replaceFirst(Constants.Separators.SEGMENT_SEPARATOR, "");

				/* Group 4 */
				List<CabinClassAvailabilityDTO> cabinClassAvailabilityDTOList = new ArrayList<CabinClassAvailabilityDTO>();
				for (GROUP4 group4 : group3.getGROUP4()) {
					TVL tvlL2 = group4.getTVL();
					if (tvlL2 != null && CodeSetEnum.CS7365.NoActionRequired != CodeSetEnum.CS7365.getEnum(tvlL2.getTVL08())) {
						CabinClassAvailabilityDTO cabinClassAvailabilityDTO = new CabinClassAvailabilityDTO();

						Date startDate = getDate(tvlL2.getTVL01().getTVL0101(), tvlL2.getTVL01().getTVL0102().toString());
						Date endDate = getDate(tvlL2.getTVL01().getTVL0101(), tvlL2.getTVL01().getTVL0102().toString());
						if (tvlL2.getTVL01().getTVL0102() != null) { // We have a specific date and time
							cabinClassAvailabilityDTO.setDepartureDateTimeStart(startDate);
							cabinClassAvailabilityDTO.setDepartureDateTimeEnd(endDate);
						} else { // Only the date is given. Need to consider the whole day as the date range.
							cabinClassAvailabilityDTO.setDepartureDateTimeStart(CalendarUtil.getStartTimeOfDate(startDate));
							cabinClassAvailabilityDTO.setDepartureDateTimeEnd(CalendarUtil.getEndTimeOfDate(endDate));
						}

						cabinClassAvailabilityDTO.setOrigin(tvlL2.getTVL02().getTVL0201());
						cabinClassAvailabilityDTO.setDestination(tvlL2.getTVL03().getTVL0301());

						cabinClassAvailabilityDTO.setFlightNumber(tvlL2.getTVL04().getTVL0401() + tvlL2.getTVL05().getTVL0501());

						cabinClassAvailabilityDTO.setSequenceNumber(tvlL2.getTVL06().getTVL0601());
						cabinClassAvailabilityDTO.setLineItemNumber(tvlL2.getTVL07().toString());

						PDI pdiL3 = group4.getPDI();
						RPI rpiL3 = group4.getRPI();
						for (SSR ssrL3 : group4.getSSR()) {

						}

						cabinClassAvailabilityDTOList.add(cabinClassAvailabilityDTO);
					}
				}

				odiCcdtoMap.put(odiL1Str, cabinClassAvailabilityDTOList);
			}
		}

		params.put(TypeACommandParamNames.CABIN_CLASS_AVAILABILITY_DTO_LIST, odiCcdtoMap);

		return params;
	}

}
