package com.isa.thinair.gdsservices.core.typeA.transformers.tkcreqres.v03;

import iata.typea.v031.tkcreq.GROUP1;
import iata.typea.v031.tkcreq.IATA;
import iata.typea.v031.tkcreq.TKCREQ;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

import com.ddtek.xmlconverter.exception.ConverterException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;
import com.isa.thinair.gdsservices.api.model.EticketInfoTO;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.core.typeA.converters.Convert;
import com.isa.thinair.gdsservices.core.typeA.marshallers.Marshaller;

/**
 * 
 * @author sanjaya
 * 
 */
public class TkcReqMessagingUtil {

	/**
	 * 
	 * @param eTicketList
	 * @throws ModuleException
	 * @throws JAXBException
	 */
	public static void sendTkcreqForFlownSegments(Collection<EticketInfoTO> eTicketList) throws ModuleException {

		if (eTicketList != null && eTicketList.size() > 0) {

			// As of now we are sending 1 etk per TKCREQ message. But this could be improved up to 99
			// per message. TODO - MALAKA
			for (EticketInfoTO eTicket : eTicketList) {

				Collection<EticketInfoTO> eTktList = new ArrayList<EticketInfoTO>();
				eTktList.add(eTicket);
				JAXBElement<?> message = buildTkcreqMessage(eTktList);
				try {
					String ediXMLOutput = Marshaller.javaToEdi(message, IATAOperation.TKCREQ);
					String ediResult;
					try {
						ediResult = Convert.xmlToEdi(ediXMLOutput);
						System.out.println(ediResult);
					} catch (ConverterException e) {
					}

				} catch (JAXBException e) {
					throw new ModuleException("", e);
				}
			}
		}
	}

	/**
	 * Building the {@link TKCREQ} message for a given list of eTickets.
	 * 
	 * @param eticketInfoTOs
	 * @return
	 * @throws ModuleException
	 */
	public static JAXBElement<?> buildTkcreqMessage(Collection<EticketInfoTO> eticketInfoTOs) throws ModuleException {

		if (eticketInfoTOs == null) {
			throw new ModuleException("gdsservices.framework.empty.argument");
		}

		TkcreqFactory factory = TkcreqFactory.getInstance();

		TKCREQ tkcreq = factory.getObjectFactory().createTKCREQ();

		tkcreq.setUNH(factory.createUNH());
		tkcreq.setMSG(factory.createMSG(CodeSetEnum.CS1225.ChangeStatus.getValue()));
		tkcreq.setORG(factory.createORG());

		Collection<GROUP1> group1Elements = factory.createGroup1(eticketInfoTOs);
		tkcreq.getGROUP1().addAll(group1Elements);

		tkcreq.setEQN(factory.createEQN(new BigDecimal(group1Elements.size())));
		tkcreq.setUNT(factory.createUNT());

		IATA iata = factory.getObjectFactory().createIATA();
		iata.getUNBAndUNGAndTKCREQ().add(factory.createUNB());
		iata.getUNBAndUNGAndTKCREQ().add(tkcreq);

		return factory.getObjectFactory().createIATA(iata);
	}
}
