package com.isa.thinair.gdsservices.core.util;

import java.math.BigDecimal;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketsAssembler;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.typea.common.MonetaryInformation;
import com.isa.typea.common.TravellerTicketControlInformation;
import com.isa.typea.common.TravellerTicketingInformation;
import com.isa.typea.tktres.TKTRESMessage;

public abstract class GdsDtoTransformer {


	public static void populatePassengerAssembler(TicketsAssembler.PassengerAssembler passengerAssembler, TKTRESMessage tktresMessage) {
		for ( TravellerTicketingInformation ticketingInformation : tktresMessage.getTravellerTicketingInformation() ) {
			for ( MonetaryInformation monetaryInfo : ticketingInformation.getMonetaryInformation() ) {
				if (monetaryInfo.getAmountTypeQualifier().equals(TypeAConstants.MonetaryType.TICKET_TOTAL)) {
					passengerAssembler.setCurrencyCode(monetaryInfo.getCurrencyCode());
					passengerAssembler.setTotalAmount(new BigDecimal(monetaryInfo.getAmount()));
				}
			}
		}
	}


	public static TicketsAssembler.PassengerAssembler appendToTicketsAssembler(TicketsAssembler ticketsAssembler, TravellerTicketingInformation traveller,
	                                                                           LCCClientReservationPax reservationPax) throws GdsTypeAException {

		String additionalPayCurrencyCode = null;
		String additionalPaymentAmount = null;
		String totalPayCurrencyCode = null;
		String totalPaymentAmount = null;

		BigDecimal amount = BigDecimal.ZERO;
		BigDecimal amountInBaseCurrency;

		TicketsAssembler.PassengerAssembler passengerAssembler;
		TicketsAssembler.Mode mode;


		for (MonetaryInformation monetaryInfo : traveller.getMonetaryInformation()) {
			if (monetaryInfo.getAmountTypeQualifier().equals(TypeAConstants.MonetaryType.TICKET_TOTAL)) {
				if (monetaryInfo.getAmount().endsWith(TypeAConstants.TypeAKeywords.ADDITION_COLLECTION_POSTFIX)) {
					additionalPaymentAmount = monetaryInfo.getAmount()
							.substring(0, monetaryInfo.getAmount().length() - TypeAConstants.TypeAKeywords.ADDITION_COLLECTION_POSTFIX.length());
					additionalPayCurrencyCode = monetaryInfo.getCurrencyCode();
				} else if (monetaryInfo.getAmount().equals(TypeAConstants.TypeAKeywords.NO_ADDITIONAL_COLLECTION)) {
					additionalPaymentAmount = BigDecimal.ZERO.toPlainString();
				} else {
					totalPayCurrencyCode = monetaryInfo.getCurrencyCode();
					totalPaymentAmount = monetaryInfo.getAmount();
				}
			} else if (monetaryInfo.getAmountTypeQualifier().equals(TypeAConstants.MonetaryType.ADDITIONAL_COLLECTION_TOTAL)) {
				additionalPayCurrencyCode = monetaryInfo.getCurrencyCode();
				additionalPaymentAmount = monetaryInfo.getAmount();
			}
		}

		passengerAssembler = new TicketsAssembler.PassengerAssembler();
		mode = ticketsAssembler.getMode();

		passengerAssembler.setPnrPaxId(PaxTypeUtils.getPnrPaxId(reservationPax.getTravelerRefNumber()));

		switch (mode) {
		case PAYMENT:
			if (totalPaymentAmount != null) {
				passengerAssembler.setCurrencyCode(totalPayCurrencyCode);
				amount = new BigDecimal(totalPaymentAmount);
				try {
					amountInBaseCurrency = GDSServicesUtil.convertToBaseCurrency(totalPayCurrencyCode, amount);
				} catch (ModuleException e) {
					throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.UNABLE_TO_CONVERT_TO_TICKETING_CURRENCY);
				}
				passengerAssembler.setTotalAmount(amountInBaseCurrency);
			} else if (additionalPaymentAmount != null) {
				amount = new BigDecimal(additionalPaymentAmount);
				try {
					if (amount.compareTo(BigDecimal.ZERO) == 0 && additionalPayCurrencyCode == null) {
						additionalPayCurrencyCode = AppSysParamsUtil.getBaseCurrency();
						amountInBaseCurrency = BigDecimal.ZERO;
					} else {
						amountInBaseCurrency = GDSServicesUtil.convertToBaseCurrency(additionalPayCurrencyCode, amount);
					}
				} catch (ModuleException e) {
					throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.UNABLE_TO_CONVERT_TO_TICKETING_CURRENCY);
				}
				passengerAssembler.setCurrencyCode(additionalPayCurrencyCode);
				passengerAssembler.setBalance(amountInBaseCurrency);
			}

			if (amount.compareTo(BigDecimal.ZERO) >= 0)  {
				ticketsAssembler.addPassengerAssembler(passengerAssembler);
			}

			break;
		case REFUND:
			if (totalPaymentAmount != null) {
				passengerAssembler.setCurrencyCode(totalPayCurrencyCode);
			} else if (additionalPaymentAmount != null) {
				passengerAssembler.setCurrencyCode(additionalPayCurrencyCode);
			}

			ticketsAssembler.addPassengerAssembler(passengerAssembler);

			break;
		}


		return passengerAssembler;
	}


	public static TicketsAssembler.PassengerAssembler appendNilRecord(TicketsAssembler ticketsAssembler, LCCClientReservationPax reservationPax) {

		TicketsAssembler.PassengerAssembler passengerAssembler = new TicketsAssembler.PassengerAssembler();
		TicketsAssembler.Mode mode = ticketsAssembler.getMode();

		passengerAssembler.setPnrPaxId(PaxTypeUtils.getPnrPaxId(reservationPax.getTravelerRefNumber()));

		switch (mode) {
		case PAYMENT:
			passengerAssembler.setBalance(BigDecimal.ZERO);
			passengerAssembler.setCurrencyCode(AppSysParamsUtil.getBaseCurrency());
			ticketsAssembler.addPassengerAssembler(passengerAssembler);

			break;
		case REFUND:
			break;
		}

		return passengerAssembler;
	}


	public static TicketsAssembler.PassengerAssembler appendToTicketsAssembler(TicketsAssembler ticketsAssembler, TravellerTicketControlInformation traveller,
	                                                                           LCCClientReservationPax reservationPax) throws GdsTypeAException {

		String totalPayCurrencyCode = null;
		String totalPaymentAmount = null;

		BigDecimal amount = BigDecimal.ZERO;
		BigDecimal amountInBaseCurrency;

		TicketsAssembler.PassengerAssembler passengerAssembler;
		TicketsAssembler.Mode mode;

		String additionalPayCurrencyCode = null;
		String additionalPaymentAmount = null;


		for (MonetaryInformation monetaryInfo : traveller.getMonetaryInformation()) {
			if (monetaryInfo.getAmountTypeQualifier().equals(TypeAConstants.MonetaryType.TICKET_TOTAL)) {
				if (monetaryInfo.getAmount().endsWith(TypeAConstants.TypeAKeywords.ADDITION_COLLECTION_POSTFIX)) {
					additionalPaymentAmount = monetaryInfo.getAmount()
							.substring(0, monetaryInfo.getAmount().length() - TypeAConstants.TypeAKeywords.ADDITION_COLLECTION_POSTFIX.length());
					additionalPayCurrencyCode = monetaryInfo.getCurrencyCode();
				} else if (monetaryInfo.getAmount().equals(TypeAConstants.TypeAKeywords.NO_ADDITIONAL_COLLECTION)) {
					additionalPaymentAmount = BigDecimal.ZERO.toPlainString();
				} else {
					totalPayCurrencyCode = monetaryInfo.getCurrencyCode();
					totalPaymentAmount = monetaryInfo.getAmount();
				}
			} else if (monetaryInfo.getAmountTypeQualifier().equals(TypeAConstants.MonetaryType.ADDITIONAL_COLLECTION_TOTAL)) {
				additionalPayCurrencyCode = monetaryInfo.getCurrencyCode();
				additionalPaymentAmount = monetaryInfo.getAmount();
			}
		}

		passengerAssembler = new TicketsAssembler.PassengerAssembler();
		mode = ticketsAssembler.getMode();

		passengerAssembler.setPnrPaxId(PaxTypeUtils.getPnrPaxId(reservationPax.getTravelerRefNumber()));

		switch (mode) {
		case PAYMENT:
			if (totalPaymentAmount != null) {
				passengerAssembler.setCurrencyCode(totalPayCurrencyCode);
				amount = new BigDecimal(totalPaymentAmount);
				try {
					amountInBaseCurrency = GDSServicesUtil.convertToBaseCurrency(totalPayCurrencyCode, amount);
				} catch (ModuleException e) {
					throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.UNABLE_TO_CONVERT_TO_TICKETING_CURRENCY);
				}
				passengerAssembler.setTotalAmount(amountInBaseCurrency);
			} else if (additionalPaymentAmount != null) {
				amount = new BigDecimal(additionalPaymentAmount);
				try {
					if (amount.compareTo(BigDecimal.ZERO) == 0 && additionalPayCurrencyCode == null) {
						additionalPayCurrencyCode = AppSysParamsUtil.getBaseCurrency();
						amountInBaseCurrency = BigDecimal.ZERO;
					} else {
						amountInBaseCurrency = GDSServicesUtil.convertToBaseCurrency(additionalPayCurrencyCode, amount);
					}
				} catch (ModuleException e) {
					throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.UNABLE_TO_CONVERT_TO_TICKETING_CURRENCY);
				}
				passengerAssembler.setCurrencyCode(additionalPayCurrencyCode);
				passengerAssembler.setBalance(amountInBaseCurrency);
			}

			if (amount.compareTo(BigDecimal.ZERO) >= 0)  {
				ticketsAssembler.addPassengerAssembler(passengerAssembler);
			}

			break;
		case RECONCILE:
			if (totalPaymentAmount != null) {
				passengerAssembler.setCurrencyCode(totalPayCurrencyCode);
				amount = new BigDecimal(totalPaymentAmount);
				try {
					amountInBaseCurrency = GDSServicesUtil.convertToBaseCurrency(totalPayCurrencyCode, amount);
				} catch (ModuleException e) {
					throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.UNABLE_TO_CONVERT_TO_TICKETING_CURRENCY);
				}
				passengerAssembler.setTotalAmount(amountInBaseCurrency);
			} else {
//				throw exc
			}

			if (amount.compareTo(BigDecimal.ZERO) != 0)  {
				ticketsAssembler.addPassengerAssembler(passengerAssembler);
			}

			break;
		}


		return passengerAssembler;
	}

}
