package com.isa.thinair.gdsservices.core.typeA.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.Pair;

public class MonetaryInformation implements Serializable {

	private static final long serialVersionUID = 3445246922559116209L;

	private PayCurrencyDTO baseFareAmount;

	private PayCurrencyDTO equivalantFareAmountInPay;

	private PayCurrencyDTO totalAmoutInPay;

	private BigDecimal exRate;

	private List<Pair<String, BigDecimal>> taxCharges = new ArrayList<Pair<String, BigDecimal>>();

	/**
	 * @return the baseFareAmount
	 */
	public PayCurrencyDTO getBaseFareAmount() {
		return baseFareAmount;
	}

	/**
	 * @param baseFareAmount
	 *            the baseFareAmount to set
	 */
	public void setBaseFareAmount(BigDecimal amount, String currencyCode) {
		PayCurrencyDTO currencyDTO = new PayCurrencyDTO(currencyCode, null);
		currencyDTO.setTotalPayCurrencyAmount(amount);
		this.baseFareAmount = currencyDTO;
	}

	/**
	 * @return the equivalantFareAmountInPay
	 */
	public PayCurrencyDTO getEquivalantFareAmountInPay() {
		return equivalantFareAmountInPay;
	}

	/**
	 * @param equivalantFareAmountInPay
	 *            the equivalantFareAmountInPay to set
	 */
	public void setEquivalantFareAmountInPay(BigDecimal amount, String currencyCode) {
		PayCurrencyDTO currencyDTO = new PayCurrencyDTO(currencyCode, null);
		currencyDTO.setTotalPayCurrencyAmount(amount);
		this.equivalantFareAmountInPay = currencyDTO;
	}

	/**
	 * @return the totalAmoutInPay
	 */
	public PayCurrencyDTO getTotalAmoutInPay() {
		return totalAmoutInPay;
	}

	/**
	 * @param totalAmoutInPay
	 *            the totalAmoutInPay to set
	 */
	public void setTotalAmoutInPay(BigDecimal amount, String currencyCode) {
		PayCurrencyDTO currencyDTO = new PayCurrencyDTO(currencyCode, null);
		currencyDTO.setTotalPayCurrencyAmount(amount);
		this.totalAmoutInPay = currencyDTO;
	}

	/**
	 * @return the exRate
	 */
	public BigDecimal getExRate() {
		return exRate;
	}

	/**
	 * @param exRate
	 *            the exRate to set
	 */
	public void setExRate(BigDecimal exRate) {
		this.exRate = exRate;
	}

	public void addTaxCharge(String code, BigDecimal amount) {
		Pair<String, BigDecimal> pair = Pair.of(code, amount);
		taxCharges.add(pair);
	}

	public BigDecimal getTotalTaxChargeAmount() {
		BigDecimal totalTaxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (Pair<String, BigDecimal> pair : taxCharges) {
			totalTaxCharge = AccelAeroCalculator.add(totalTaxCharge, pair.getRight());
		}
		return totalTaxCharge;
	}

}
