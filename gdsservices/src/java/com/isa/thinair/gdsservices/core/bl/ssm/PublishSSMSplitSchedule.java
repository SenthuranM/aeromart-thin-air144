/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.core.bl.ssm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airschedules.api.dto.ReScheduleRequiredInfo;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.SSMSplitFlightSchedule;
import com.isa.thinair.airschedules.api.service.ScheduleBD;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.airschedules.api.utils.GDSScheduleEventCollector;
import com.isa.thinair.airschedules.api.utils.ScheduleBuildStatusEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for sending SSM message for required schedules when Original schedule split.
 * 
 * @author M.Rikaz
 * @isa.module.command name="publishSSMSplitSchedule"
 */
public class PublishSSMSplitSchedule extends DefaultBaseCommand {

	private ScheduleBD scheduleBD;

	private BookingClassBD bookingClassBD;

	private static Log log = LogFactory.getLog(PublishSSMSplitSchedule.class);

	/**
	 * constructor of the PublishSplitSSM command
	 */
	public PublishSSMSplitSchedule() {
		scheduleBD = GDSServicesModuleUtil.getScheduleBD();
		bookingClassBD = GDSServicesModuleUtil.getBookingClassBD();
	}

	/**
	 * execute method of the IntegrateSSMNew command
	 * 
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce execute() throws ModuleException {

		GDSScheduleEventCollector sGdsEvents = (GDSScheduleEventCollector) this
				.getParameter(GDSSchedConstants.ParamNames.GDS_EVENT_COLLECTOR);
		log.info("Inside Split Schedule publish SSM ");

		if (AppSysParamsUtil.isEnableSSMforSplitSchedule()
				&& (sGdsEvents != null && sGdsEvents.containsAction(GDSScheduleEventCollector.SPLIT_SCHEDULE))) {

			Map gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
			Map aircraftTypeMap = GDSServicesModuleUtil.getGlobalConfig().getIataAircraftCodes();
			String airLineCode = GDSServicesModuleUtil.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);

			Collection<SSMSplitFlightSchedule> existingSubSchedules = sGdsEvents.getExistingSubSchedules();
			Collection<FlightSchedule> splitSchedules = sGdsEvents.getSplitSchedules();

			existingSubSchedules = getUniqueSubSchedules(existingSubSchedules);

			if (splitSchedules != null && !splitSchedules.isEmpty()) {
				FlightSchedule existingSchedule = sGdsEvents.getExistingSchedule();

				if (ScheduleBuildStatusEnum.BUILT.getCode().equals(existingSchedule.getBuildStatusCode())) {

					if (isEligibleToSendSubMessages(existingSubSchedules, splitSchedules)) {

						Map bcMap = bookingClassBD.getBookingClassCodes(existingSchedule.getGdsIds());
						ReScheduleRequiredInfo reScheduleInfo = new ReScheduleRequiredInfo();
						reScheduleInfo.addAction(GDSScheduleEventCollector.SPLIT_SCHEDULE);
						reScheduleInfo.setGdsStatusMap(gdsStatusMap);
						reScheduleInfo.setAircraftTypeMap(aircraftTypeMap);
						reScheduleInfo.setServiceType(sGdsEvents.getServiceType());
						reScheduleInfo.setAirLineCode(airLineCode);
						reScheduleInfo.setBcMap(bcMap);
						reScheduleInfo.setCancelExistingSubSchedule(false);
						reScheduleInfo.setSupplementaryInfo(sGdsEvents.getSupplementaryInfo());
						reScheduleInfo.setActions(sGdsEvents.getActions());
						reScheduleInfo.setPublishedGdsIds(sGdsEvents.getExistingSchedule().getGdsIds());
						reScheduleInfo.setExistingSchedule(sGdsEvents.getExistingSchedule());
						reScheduleInfo.setUpdatedSchedule(sGdsEvents.getUpdatedSchedule());
						reScheduleInfo.setSplitSchedules(splitSchedules);

						SchedulePublishBL.sendSsmSubMessagesForUpdate(reScheduleInfo);

					} else {
						List<SSMSplitFlightSchedule> cnxScheduleList = new ArrayList<SSMSplitFlightSchedule>();
						List<SSMSplitFlightSchedule> matchedSubScheduleList = new ArrayList<SSMSplitFlightSchedule>();
						List<SSMSplitFlightSchedule> matchedExistingSchedules = new ArrayList<SSMSplitFlightSchedule>();

						Map bcMap = bookingClassBD.getBookingClassCodes(existingSchedule.getGdsIds());

						existingSubSchedules = SchedulePublishUtil.addSubSchedule(existingSubSchedules, existingSchedule);

						for (FlightSchedule schedule : splitSchedules) {
							Collection<SSMSplitFlightSchedule> updatedScheduleColl = schedule.getSsmSplitFlightSchedule();

							updatedScheduleColl = SchedulePublishUtil.addSubSchedule(updatedScheduleColl, schedule);

							SchedulePublishBL.sendSSMForScheduleSplit(gdsStatusMap, aircraftTypeMap, null, airLineCode,
									schedule.getGdsIds(), bcMap, schedule, existingSchedule, existingSubSchedules,
									updatedScheduleColl, AppSysParamsUtil.isCancelPreviousScheduleOnDSTChanges(),
									cnxScheduleList, matchedSubScheduleList, matchedExistingSchedules,
									sGdsEvents.getSupplementaryInfo());

							log.info("Split Schedule publish SSM End");

						}

						if (cnxScheduleList != null && !cnxScheduleList.isEmpty() && matchedExistingSchedules != null
								&& !matchedExistingSchedules.isEmpty()) {
							cnxScheduleList.removeAll(matchedExistingSchedules);
							SchedulePublishBL.removeAlreadyMatchedSchedules(cnxScheduleList, matchedExistingSchedules);
						}

						// sending SSM-CNL for cancelled schedules
						if (AppSysParamsUtil.isSendSsmCnlMessageForReSchedule()) {
							SchedulePublishBL.sendSSMForCnxSchedules(gdsStatusMap, aircraftTypeMap, null, airLineCode,
									existingSchedule.getGdsIds(), bcMap, cnxScheduleList, existingSchedule,
									sGdsEvents.getSupplementaryInfo());
						}
					}

				}

			}

		}

		// constructing response
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		// return command response
		return responce;
	}

	private Collection<SSMSplitFlightSchedule> getUniqueSubSchedules(Collection<SSMSplitFlightSchedule> existingSubSchedules) {
		Collection<SSMSplitFlightSchedule> uniqueList = new ArrayList<SSMSplitFlightSchedule>();
		Set<SSMSplitFlightSchedule> set = null;
		if (existingSubSchedules != null && !existingSubSchedules.isEmpty()) {
			set = new TreeSet<SSMSplitFlightSchedule>(new Comparator<SSMSplitFlightSchedule>() {
				@Override
				public int compare(SSMSplitFlightSchedule o1, SSMSplitFlightSchedule o2) {
					if (o1.getScheduleId().intValue() == o2.getScheduleId().intValue()
							&& o1.getStartDate().compareTo(o2.getStartDate()) == 0
							&& o1.getStopDate().compareTo(o2.getStopDate()) == 0) {
						return 0;
					}
					return 1;
				}
			});
			set.addAll(existingSubSchedules);
			uniqueList = new ArrayList<SSMSplitFlightSchedule>(set);
		}
		return uniqueList;
	}

	private boolean isEligibleToSendSubMessages(Collection<SSMSplitFlightSchedule> existingSubSchedules,
			Collection<FlightSchedule> splitSchedules) {
		if ((AppSysParamsUtil.isEnableSendSsmAsmSubMessages() || splitSchedules.size() == 1)
				&& (!AppSysParamsUtil.isEnableSplitScheduleWhenDSTApplicable() && (existingSubSchedules == null || existingSubSchedules
						.isEmpty()))) {
			return true;
		}

		return false;
	}
}
