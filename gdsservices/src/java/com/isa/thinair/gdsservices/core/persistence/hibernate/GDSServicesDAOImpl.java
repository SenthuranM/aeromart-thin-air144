/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */

package com.isa.thinair.gdsservices.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.gdsservices.api.model.GDSEdiMessages;
import com.isa.thinair.gdsservices.api.model.GDSMessage;
import com.isa.thinair.gdsservices.api.model.GDSPublishMessage;
import com.isa.thinair.gdsservices.api.model.GDSSchedulePublishStatus;
import com.isa.thinair.gdsservices.api.model.GdsReservationImage;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSServicesDAO;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

/**
 * @author Nilindra Fernando
 * @isa.module.dao-impl dao-name="GDSServicesDAO"
 */
public class GDSServicesDAOImpl extends PlatformHibernateDaoSupport implements GDSServicesDAO {

	public void saveOrUpdate(Collection colGDSMessage) {
		super.hibernateSaveOrUpdateAll(colGDSMessage);
	}

	public GDSMessage getGDSMessage(long messageId) {
		return (GDSMessage) get(GDSMessage.class, new Long(messageId));
	}

	public GDSMessage getGDSMessage(String uniqueAccelAeroRequestId) {
		String hql = "SELECT gdsMessage FROM GDSMessage AS gdsMessage " + "WHERE gdsMessage.uniqueAccelAeroRequestId = ? ";

		Object[] params = { uniqueAccelAeroRequestId };

		return BeanUtils.getFirstElement(find(hql, params, GDSMessage.class));
	}

	public void saveGDSPublisingMessage(GDSPublishMessage publishMessage) {
		hibernateSaveOrUpdate(publishMessage);
	}

	public GDSPublishMessage getGDSPublisingMessage(int id) {
		return (GDSPublishMessage) get(GDSPublishMessage.class, new Integer(id));

	}

	public void saveGDSSchedulePublishStatus(GDSSchedulePublishStatus schedulePublishStatus) {
		hibernateSaveOrUpdate(schedulePublishStatus);
	}

	public void saveGdsEdiMessage(GDSEdiMessages message) {
		hibernateSaveOrUpdate(message);
	}

	public static DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	public Date getLastGDSSchedulePublishTime() {
		String queryString = null;
		queryString = "select max(gsps.PUBLISHED_TIME) from T_GDS_SCHEDULE_PUBLISH_STATUS gsps";

		JdbcTemplate templete = new JdbcTemplate(getDatasource());

		return (Date) templete.query(queryString, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Date lastPublishDate = null;
				if (rs != null) {
					while (rs.next()) {
						lastPublishDate = toDate(rs.getTimestamp("max(gsps.PUBLISHED_TIME)"));
					}
				}
				return lastPublishDate;
			}
		});
	}


	public GdsReservationImage getGDSReservationImage(String pnr, String imageContext) {
		GdsReservationImage image = null;
		List<GdsReservationImage> images;
		String hql = "SELECT gdsReservationImage FROM GdsReservationImage AS gdsReservationImage WHERE gdsReservationImage.pnr = ? AND gdsReservationImage.imageContext = ? ";
		Object[] params = { pnr, imageContext };

		images = find(hql, params, GdsReservationImage.class);
		if (!images.isEmpty()) {
			image = images.get(0);
		}

		return image;
	}

	private static Date toDate(Timestamp timestamp) {

		if (timestamp != null) {
			long milliseconds = timestamp.getTime() + (timestamp.getNanos() / 1000000);
			return new java.util.Date(milliseconds);
		} else {
			return timestamp;
		}
	}

}
