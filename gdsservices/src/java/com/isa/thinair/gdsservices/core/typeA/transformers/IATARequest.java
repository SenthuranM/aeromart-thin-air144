package com.isa.thinair.gdsservices.core.typeA.transformers;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBElement;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.core.config.TypeAMessageConfig;
import com.isa.thinair.gdsservices.core.typeA.model.InterchangeHeader;
import com.isa.thinair.gdsservices.core.typeA.model.InterchangeInfo;
import com.isa.thinair.gdsservices.core.typeA.model.MessageHeader;

/**
 * The Template method for all the GDS request handlers.
 * 
 */
public abstract class IATARequest {

	protected JAXBElement<?> jaxbRequest;

	protected EDIFACTMessageProcess customMessageBranchHandler;

	public IATARequest(JAXBElement<?> jaxbRequest) {
		super();
		this.jaxbRequest = jaxbRequest;
	}

	/**
	 * @return the jaxbRequest
	 */
	public JAXBElement<?> getJaxbRequest() {
		return jaxbRequest;
	}

	/**
	 * @param jaxbRequest
	 *            the jaxbRequest to set
	 */
	public void setJaxbRequest(JAXBElement<?> jaxbRequest) {
		this.jaxbRequest = jaxbRequest;
	}

	/**
	 * prepare the command params form the EDI request
	 * 
	 * @param messageHeader
	 * @param interchangeHeader
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Map<String, Object> getMessageParams(InterchangeHeader interchangeHeader, MessageHeader messageHeader)
			throws ModuleException {

		Map<String, Object> params = new HashMap<String, Object>();

		// validate header info
		validatHeaders(interchangeHeader, messageHeader);

		// adding common params to all the messages.
		params.put(TypeACommandParamNames.IC_HEADER, interchangeHeader);
		params.put(TypeACommandParamNames.MESSAGE_HEADER, messageHeader);

		// add the message specific params to the command.
		params.putAll(extractSpecificCommandParams());

		return params;
	}

	private void validatHeaders(InterchangeHeader interchangeHeader, MessageHeader messageHeader) throws InteractiveEDIException {

		TypeAMessageConfig typeAMessageConfig = GDSServicesModuleUtil.getConfig().getTypeAMessageConfig();

		// validate sysntax id
		if (!interchangeHeader.getSyntaxIdentifier().equals(typeAMessageConfig.getSyntaxIdentifier())) {
			throw new InteractiveEDIException(CodeSetEnum.CS0085.SYNTAX_OR_VERSION_LEVEL_NOT_SUPPORTED, "Invalide syntax found");
		}

		if (!interchangeHeader.getVersionNo().equals(typeAMessageConfig.getSyntaxVersion())) {
			throw new InteractiveEDIException(CodeSetEnum.CS0085.SYNTAX_OR_VERSION_LEVEL_NOT_SUPPORTED, "Invalide versoin found");
		}

		// recipientId
		InterchangeInfo recipient = interchangeHeader.getRecipient();
		if (!recipient.getInterchangeId().equals(typeAMessageConfig.getOwnId())) {
			throw new InteractiveEDIException(CodeSetEnum.CS0085.INVALID_OR_MISSING_RECIPIENT_REFERENCE_PASSWORD,
					"Invalid recipientId found");
		}

		if (!recipient.getInterchangeCode().equals(typeAMessageConfig.getOwnCode())) {
			throw new InteractiveEDIException(CodeSetEnum.CS0085.INVALID_OR_MISSING_RECIPIENT_REFERENCE_PASSWORD,
					"Invalid recipientCode found");
		}

		long diff = System.currentTimeMillis() - interchangeHeader.getTimestamp().getTime();
		if (diff > typeAMessageConfig.getMaxValidityPeriod()) {
			throw new InteractiveEDIException(CodeSetEnum.CS0085.TOO_OLD, "message is too old to be processed");
		}

	}

	public EDIFACTMessageProcess getMessageBranchHandler() {
		return customMessageBranchHandler;
	}

	public abstract Map<String, Object> extractSpecificCommandParams() throws ModuleException;

	/**
	 * returns the name of the command which is responsible for handling the request
	 * 
	 * @return
	 */
	public abstract String getBeanFromElement();

	/**
	 * Retrive the interactive header from message.
	 * 
	 * @return
	 * @throws InteractiveEDIException
	 */
	public abstract InterchangeHeader getInterchangeHeader() throws InteractiveEDIException;

	/**
	 * Retrive the message header from the edi message.
	 * 
	 * @return
	 * @throws InteractiveEDIException
	 */
	public abstract MessageHeader getMessageHeader() throws InteractiveEDIException;

}