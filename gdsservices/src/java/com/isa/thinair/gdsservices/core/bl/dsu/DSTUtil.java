package com.isa.thinair.gdsservices.core.bl.dsu;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.jfree.util.Log;

import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.to.AirportDstTO;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.dto.publishing.FlightScheduleLegDTO;
import com.isa.thinair.gdsservices.api.dto.publishing.ItineraryVariationScheduleMessageDTO;
import com.isa.thinair.gdsservices.api.dto.publishing.LegTimeSliceDTO;
import com.isa.thinair.gdsservices.api.dto.publishing.TimeSliceDTO;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;

/**
 * 
 * @author janandith jayawardena
 * 
 */
public class DSTUtil {

	public static List<TimeSliceDTO> getTimeSlices(Map<String, LegTimeSliceDTO> airportWiseEffectiveTimes, Date itenaryFromDate,
			Date itenaryToDate) {
		List<TimeSliceDTO> splitTimeRangeForLeg = new ArrayList<TimeSliceDTO>();
		itenaryFromDate = CalendarUtil.getStartTimeOfDate(itenaryFromDate);
		itenaryToDate = CalendarUtil.getEndTimeOfDate(itenaryToDate);

		List dstsPerAirport = new ArrayList();
		try {
			if (airportWiseEffectiveTimes != null && !airportWiseEffectiveTimes.isEmpty()) {
				for (String airportCode : airportWiseEffectiveTimes.keySet()) {
					Collection airportDSTCollection;
					airportDSTCollection = GDSServicesModuleUtil.getAirportBD().getAirportDSTs(airportCode);
					List<AirportDstTO> applicableDSTList = getApplicableDST(airportDSTCollection, itenaryFromDate, itenaryToDate);
					if (applicableDSTList.size() > 0) {
						List<AirportDstTO> sortedApplicableDSTList = sortDSTList(applicableDSTList);
						dstsPerAirport.add(sortedApplicableDSTList);
					}
				}

				splitTimeRangeForLeg = splitTimeRangeForLeg(dstsPerAirport, itenaryFromDate, itenaryToDate,
						airportWiseEffectiveTimes);
			}

		} catch (ModuleException e) {
			Log.error("DSTUtil.getTimeSlices : Error in getting timeslices for airport" + e.getMessage());
		}

		return splitTimeRangeForLeg;
	}

	private static List<TimeSliceDTO> getAllTimeSlices(List allTimeSlicesPerAirport, long itenaryEnd) {
		List<TimeSliceDTO> allTimeSlices = new ArrayList<TimeSliceDTO>();
		int smallestTimeSliceCount = 0;
		long endDateMilli = 0;

		for (int i = 0; i < allTimeSlicesPerAirport.size(); i++) {
			if (allTimeSlicesPerAirport.get(i) != null) {
				List<TimeSliceDTO> airportTimeSlices = (ArrayList<TimeSliceDTO>) allTimeSlicesPerAirport.get(i);

				if (!airportTimeSlices.isEmpty() && airportTimeSlices.get(0) != null) {
					if (airportTimeSlices.size() < smallestTimeSliceCount || smallestTimeSliceCount == 0) {
						smallestTimeSliceCount = airportTimeSlices.size();
					}
					
					if (((TimeSliceDTO) airportTimeSlices.get(0)).getEndDate().getTime() < endDateMilli || endDateMilli == 0) {
						endDateMilli = ((TimeSliceDTO) airportTimeSlices.get(0)).getEndDate().getTime();
					}
				}
			}
		}
		List<TimeSliceDTO> timeslices = new ArrayList<TimeSliceDTO>();

		for (int j = 0; j < smallestTimeSliceCount; j++) {
			List<TimeSliceDTO> tsforAirport = new ArrayList<TimeSliceDTO>();
			for (int k = 0; k < allTimeSlicesPerAirport.size(); k++) {
				timeslices = (ArrayList<TimeSliceDTO>) allTimeSlicesPerAirport.get(k);
				
				if (allTimeSlicesPerAirport.size() > 1 && timeslices.size() > j) {
					TimeSliceDTO slice = timeslices.get(j);
					if (endDateMilli <= slice.getEndDate().getTime()) {
						if (endDateMilli != slice.getStartDate().getTime()) {
							TimeSliceDTO sliceK = getTimeSlice(slice.getStartDate(), new Date(endDateMilli),
									slice.getdSTOffset(), slice.getAirportCode());
							timeslices.remove(j);
							timeslices.add(sliceK);
						}
						if (endDateMilli != slice.getEndDate().getTime()) {
							long adjust = 86400000;
							TimeSliceDTO sliceToInsert = getTimeSlice(new Date(endDateMilli+adjust), slice.getEndDate(),
									slice.getdSTOffset(), slice.getAirportCode());
							timeslices.add(sliceToInsert);
						}
						sortTimeSliceList(timeslices);
						smallestTimeSliceCount = smallestTimeSliceCount + 1;
						continue;
					}
				}
			}
			if ((j + 1) < smallestTimeSliceCount && !isSameDayByTimeStamp(endDateMilli, itenaryEnd)) {
				endDateMilli = getSmallestEndDateMilli(allTimeSlicesPerAirport, j + 1);
			}
		}

		return timeslices;
	}

	public static List<ItineraryVariationScheduleMessageDTO> getTimeSlicedItenaries(ItineraryVariationScheduleMessageDTO itenary,
			List<TimeSliceDTO> timeSlices) {
		List<ItineraryVariationScheduleMessageDTO> listOfItenaries = new ArrayList<ItineraryVariationScheduleMessageDTO>();

		List<FlightScheduleLegDTO> legs = itenary.getLegs();
		AirportBD airportBD = GDSServicesModuleUtil.getAirportBD();
		long currentTime = new Date().getTime();

		// 86400000 = 24*3600*1000 milliseconds for a day - avoiding multiplication
		long dateOffset = 0;

		try {
			for (int i = 0; i < timeSlices.size(); i++) {
				TimeSliceDTO ts = timeSlices.get(i);
				if (ts.getEndDate().getTime() >= currentTime) {
					ItineraryVariationScheduleMessageDTO it = (ItineraryVariationScheduleMessageDTO) itenary.clone();
					List<FlightScheduleLegDTO> flightLegs = new ArrayList<FlightScheduleLegDTO>();
					for (int j = 0; j < legs.size(); j++) {
						FlightScheduleLegDTO leg = legs.get(j);
						FlightScheduleLegDTO legI = (FlightScheduleLegDTO) leg.clone();
						if (i > 0 && i < timeSlices.size()) {
							legI.setLegStartDate(new Date(ts.getStartDate().getTime() + dateOffset));
						} else {
							legI.setLegStartDate(ts.getStartDate());
						}
						legI.setLegEndDate(ts.getEndDate());
						Date timeRangeDate = new Date((ts.getStartDate().getTime() + ts.getEndDate().getTime()) / 2);
						int depatureOffset = SchedulePublishUtil.calculateEffectiveOffset(
								airportBD.getAirport(legI.getDepartureStation()), timeRangeDate);
						int arrivalOffset = SchedulePublishUtil.calculateEffectiveOffset(
								airportBD.getAirport(legI.getArrivalStation()), timeRangeDate);
						legI.setLocalTimeVariationForDepatureSt(SchedulePublishUtil.getFormattedOffset(depatureOffset));
						legI.setLocalTimeVariationForArrivalSt(SchedulePublishUtil.getFormattedOffset(arrivalOffset));
						legI.setTimeOfDeparture(CalendarUtil.addMinutes(legI.getTimeOfDepartureZulu(), depatureOffset));
						legI.setTimeOfArrival(CalendarUtil.addMinutes(legI.getTimeOfArrivalZulu(), arrivalOffset));
						flightLegs.add(legI);
					}
					it.setLegs(flightLegs);
					listOfItenaries.add(it);
				}
			}
		} catch (ModuleException e) {
			Log.error("DSTUtil.getTimeSlicedItenaries : Error in splitting itenary" + e.getMessage());
		}
		return listOfItenaries;

	}

	private static long getSmallestEndDateMilli(List allTimeSlicesPerAirport, int index) {
		long endDateMilli = 0;
		for (int i = 0; i < allTimeSlicesPerAirport.size(); i++) {
			List<TimeSliceDTO> airportTimeSlices = (ArrayList<TimeSliceDTO>) allTimeSlicesPerAirport.get(i);
			if (airportTimeSlices != null && !airportTimeSlices.isEmpty()) {
				if (((TimeSliceDTO) airportTimeSlices.get(index)).getEndDate().getTime() < endDateMilli || endDateMilli == 0) {
					endDateMilli = ((TimeSliceDTO) airportTimeSlices.get(index)).getEndDate().getTime();
				}
			}
		}
		return endDateMilli;
	}

	private static TimeSliceDTO getTimeSlice(Date startDate, Date endDate, int dstOffset, String airportCode) {
		TimeSliceDTO timeSlice = new TimeSliceDTO();
		timeSlice.setStartDate(CalendarUtil.getStartTimeOfDate(startDate));
		timeSlice.setEndDate(CalendarUtil.getEndTimeOfDate(endDate));
		timeSlice.setdSTOffset(dstOffset);
		timeSlice.setAirportCode(airportCode);
		return timeSlice;
	}

	private static List<AirportDstTO> getApplicableDST(Collection dstCollection, Date startDate, Date endDate) {
		List<AirportDstTO> applicableDSTS = new ArrayList<AirportDstTO>();

		for (Iterator it = dstCollection.iterator(); it.hasNext();) {
			AirportDstTO airportDST = (AirportDstTO) it.next();
			if (airportDST.getAirportDST().getDSTStatus().equals(AirportDST.CURRENT_DST)) {
				long dstStartMilliseconds = airportDST.getAirportDST().getDstStartDateTime().getTime();
				long dstEndMilliseconds = airportDST.getAirportDST().getDstEndDateTime().getTime();

				long scheduleStartMilliseconds = startDate.getTime();
				long scheduleEndMilliseconds = endDate.getTime();

				if ((dstStartMilliseconds <= scheduleStartMilliseconds) && (dstEndMilliseconds >= scheduleEndMilliseconds)
						|| (dstStartMilliseconds <= scheduleStartMilliseconds) && (dstEndMilliseconds <= scheduleEndMilliseconds)
						&& (dstEndMilliseconds >= scheduleStartMilliseconds)
						|| (dstStartMilliseconds >= scheduleStartMilliseconds) && (dstEndMilliseconds <= scheduleEndMilliseconds)
						|| (dstStartMilliseconds >= scheduleStartMilliseconds) && (dstEndMilliseconds >= scheduleEndMilliseconds)
						&& (dstStartMilliseconds <= scheduleEndMilliseconds)) {
					applicableDSTS.add(airportDST);
				}
			}
		}
		return applicableDSTS;
	}

	private static List<AirportDstTO> sortDSTList(List<AirportDstTO> dstList) {

		Collections.sort(dstList, new Comparator<AirportDstTO>() {

			@Override
			public int compare(AirportDstTO o1, AirportDstTO o2) {
				return o1.getAirportDST().getDstEndDateTime().compareTo(o2.getAirportDST().getDstEndDateTime());
			}

		});
		return dstList;
	}

	private static List<TimeSliceDTO> sortTimeSliceList(List<TimeSliceDTO> timeSliceList) {

		Collections.sort(timeSliceList, new Comparator<TimeSliceDTO>() {

			@Override
			public int compare(TimeSliceDTO o1, TimeSliceDTO o2) {
				return o1.getEndDate().compareTo(o2.getEndDate());
			}

		});
		return timeSliceList;
	}
	
	public static Collection<FlightSchedule> getDSTBasedSchedules(FlightSchedule schedule) throws ModuleException {

		List<String> airports = getFlightLegAiportList(schedule);
		List<TimeSliceDTO> airportTimeSlices = getTimeSlices(getScheduleApplicableTimeByAiport(schedule), schedule.getStartDate(), schedule.getStopDate());
		Collection<FlightSchedule> flightSchedules = getTimeSlicedSchedules(schedule, airportTimeSlices, false);
		if (flightSchedules.isEmpty()) {
			flightSchedules.add(schedule);
		}

		return flightSchedules;
	}
	
	private static Collection<FlightSchedule> getTimeSlicedSchedules(FlightSchedule schedule, List<TimeSliceDTO> timeSlices,
			boolean isFutureSchedulesOnly) throws ModuleException {

		Collection<FlightSchedule> flightSchedules = new ArrayList<FlightSchedule>();
		Set<FlightScheduleLeg> flightScheduleLegs = schedule.getFlightScheduleLegs();
		AirportBD airportBD = GDSServicesModuleUtil.getAirportBD();
		long currentTime = new Date().getTime();

		try {

			for (TimeSliceDTO ts : timeSlices) {

				if (!isFutureSchedulesOnly || ts.getEndDate().getTime() >= currentTime) {
					FlightSchedule scheduleClone = cloneFlightSchedule(schedule);

					Set<FlightScheduleLeg> scheduleLegCopySet = new HashSet<FlightScheduleLeg>();

					scheduleClone.setStartDate(CalendarUtil.getStartTimeOfDate(ts.getStartDate()));
					scheduleClone.setStopDate(CalendarUtil.getStartTimeOfDate(ts.getEndDate()));

					for (FlightScheduleLeg flightScheduleLeg : flightScheduleLegs) {
						FlightScheduleLeg flightScheduleLegCopy = cloneFlightScheduleLeg(flightScheduleLeg);

						Date timeRangeDate = new Date((ts.getStartDate().getTime() + ts.getEndDate().getTime()) / 2);
						int depatureOffset = SchedulePublishUtil.calculateEffectiveOffset(
								airportBD.getAirport(flightScheduleLegCopy.getOrigin()), timeRangeDate);
						int arrivalOffset = SchedulePublishUtil.calculateEffectiveOffset(
								airportBD.getAirport(flightScheduleLegCopy.getDestination()), timeRangeDate);

						flightScheduleLegCopy.setEstDepartureTimeLocal(CalendarUtil.addMinutes(
								flightScheduleLegCopy.getEstDepartureTimeZulu(), depatureOffset));
						flightScheduleLegCopy.setEstArrivalTimeLocal(CalendarUtil.addMinutes(
								flightScheduleLegCopy.getEstArrivalTimeZulu(), arrivalOffset));

						scheduleLegCopySet.add(flightScheduleLegCopy);

					}
					scheduleClone.setFlightScheduleLegs(scheduleLegCopySet);

					flightSchedules.add(scheduleClone);
				}
			}
		} catch (ModuleException e) {
			Log.error("DSTUtil.getTimeSlicedSchedules : Error in splitting schedules" + e.getMessage());
		}
		return flightSchedules;

	}

	public static FlightSchedule cloneFlightSchedule(FlightSchedule schedule) throws ModuleException {
		FlightSchedule scheduleCopy = null;
		try {

			if (schedule != null) {
				scheduleCopy = (FlightSchedule) BeanUtils.cloneBean(schedule);
				scheduleCopy.setVersion(-1);
				scheduleCopy.setFlightScheduleSegments(cloneFlightScheduleSegments(schedule.getFlightScheduleSegments()));
				scheduleCopy.setCodeShareMCFlightSchedules(cloneCodeShareMCFlightSchedules(schedule.getCodeShareMCFlightSchedules()));
//				scheduleCopy.setSsmSplitFlightSchedule(getCopyOfSSMSplitFlightSchedules(schedule.getSsmSplitFlightSchedule()));
				scheduleCopy.setFlightScheduleLegs(cloneFlightScheduleLegs(schedule.getFlightScheduleLegs()));
				
				
			}

		} catch (Exception e) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		return scheduleCopy;
	}
	
	private static Set<FlightScheduleLeg> cloneFlightScheduleLegs(Set<FlightScheduleLeg> flightLegs)
			throws ModuleException {

		Set<FlightScheduleLeg> flightLegsCopySet = new HashSet<FlightScheduleLeg>();
		try {

			if (flightLegs != null) {
				for (FlightScheduleLeg seg : flightLegs) {
					FlightScheduleLeg segCopy = cloneFlightScheduleLeg(seg);					
					flightLegsCopySet.add(segCopy);
				}
			}

		} catch (Exception e) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		return flightLegsCopySet;
	}
	

	private static FlightScheduleLeg cloneFlightScheduleLeg(FlightScheduleLeg leg) throws ModuleException {

		FlightScheduleLeg legCopy = null;
		try {
			if (leg != null) {
				legCopy = (FlightScheduleLeg) BeanUtils.cloneBean(leg);
			}
		} catch (Exception e) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		return legCopy;
	}

	private static Set<FlightScheduleSegment> cloneFlightScheduleSegments(Set<FlightScheduleSegment> flightScheduleSegments)
			throws ModuleException {

		Set<FlightScheduleSegment> flightScheduleSegmentsCopySet = new HashSet<FlightScheduleSegment>();
		try {

			if (flightScheduleSegments != null) {
				for (FlightScheduleSegment seg : flightScheduleSegments) {
					FlightScheduleSegment segCopy = (FlightScheduleSegment) BeanUtils.cloneBean(seg);
					flightScheduleSegmentsCopySet.add(segCopy);
				}
			}

		} catch (Exception e) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		return flightScheduleSegmentsCopySet;
	}
	
	private static Set<CodeShareMCFlightSchedule> cloneCodeShareMCFlightSchedules(
			Set<CodeShareMCFlightSchedule> codeShareMCFlightSchedules) throws ModuleException {
		Set<CodeShareMCFlightSchedule> codeShareMCFlightSchedulesCopySet = new HashSet<CodeShareMCFlightSchedule>();
		try {

			if (codeShareMCFlightSchedules != null) {
				for (CodeShareMCFlightSchedule seg : codeShareMCFlightSchedules) {
					CodeShareMCFlightSchedule segCopy = (CodeShareMCFlightSchedule) BeanUtils.cloneBean(seg);
					codeShareMCFlightSchedulesCopySet.add(segCopy);
				}
			}

		} catch (Exception e) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		return codeShareMCFlightSchedulesCopySet;
	}
	
	private static List<String> getFlightLegAiportList(FlightSchedule schedule) {
		List<String> airports = new ArrayList<String>();
		if (schedule != null && schedule.getFlightScheduleLegs() != null && !schedule.getFlightScheduleLegs().isEmpty()) {
			Set<FlightScheduleLeg> flightScheduleLegs = schedule.getFlightScheduleLegs();

			for (FlightScheduleLeg leg : flightScheduleLegs) {
				if (!airports.contains(leg.getDestination())) {
					airports.add(leg.getDestination());
				}

				if (!airports.contains(leg.getOrigin())) {
					airports.add(leg.getOrigin());
				}
			}
		}
		return airports;
	}

	private static List<TimeSliceDTO> splitTimeRangeForLeg(List dstsPerAirport, Date itenaryFromDate, Date itenaryToDate,
			Map<String, LegTimeSliceDTO> airportWiseEffectiveTimes) {

		List allSplitsPerAirport = new ArrayList();
		long itenaryStartDate = itenaryFromDate.getTime();
		long itenaryEndDate = itenaryToDate.getTime();

		long splitLimit = itenaryStartDate;
		long dateOffset = 86400000;
		for (Object o : dstsPerAirport) {
			List<AirportDstTO> dsts = (ArrayList<AirportDstTO>) o;
			List<TimeSliceDTO> splitsPerAirport = new ArrayList<TimeSliceDTO>();
			boolean isDstApplied = false;
			for (int i = 0; i < dsts.size(); i++) {
				AirportDstTO dst = dsts.get(i);
				String airportCode = dst.getAirportDST().getAirportCode();
				long dstStart = dst.getAirportDST().getDstStartDateTime().getTime();
				long dstEnd = dst.getAirportDST().getDstEndDateTime().getTime();
				Date actualGroundTime = itenaryFromDate;
				LegTimeSliceDTO legTimeSliceDTO = null; 
				int dayOffset = 0;
				
				if (i > 0 && isDstApplied) {
					splitLimit = splitLimit + dateOffset;
				}				
				if (airportWiseEffectiveTimes != null && airportWiseEffectiveTimes.get(airportCode) != null) {
					legTimeSliceDTO = airportWiseEffectiveTimes.get(airportCode);
					if (LegTimeSliceDTO.ARRIVAL_ONLY.equals(legTimeSliceDTO.getAirportOperationMode())) {
						actualGroundTime = legTimeSliceDTO.getArrivalTime();
						dayOffset = legTimeSliceDTO.getArrivalDayOffset();
					}

					if (LegTimeSliceDTO.DEPARTURE_AND_ARRIVAL.equals(legTimeSliceDTO.getAirportOperationMode())
							|| LegTimeSliceDTO.DEPARTURE_ONLY.equals(legTimeSliceDTO.getAirportOperationMode())) {
						actualGroundTime = legTimeSliceDTO.getDepartureTime();
						dayOffset = legTimeSliceDTO.getDepartureDayOffset();
					}
				}

				dstStart = getEffectiveDstStartDate(dst, actualGroundTime, dayOffset);
				dstEnd = getEffectiveDstEndDate(dst, actualGroundTime, dayOffset);				
				isDstApplied = false;
				if (splitLimit >= dstStart && splitLimit <= dstEnd && (dstStart != dstEnd)) {
					if (dstEnd <= itenaryEndDate) {
						TimeSliceDTO tsDummyLeft = getTimeSlice(new Date(splitLimit), new Date(dstEnd), dst.getAirportDST()
								.getDstAdjustTime(), dst.getAirportDST().getAirportCode());
						splitsPerAirport.add(tsDummyLeft);
						splitLimit = dstEnd;
						if (i + 1 == dsts.size()) {
							TimeSliceDTO tsDummyRight = getTimeSlice(new Date(dstEnd + dateOffset), new Date(itenaryEndDate), dst
									.getAirportDST().getDstAdjustTime(), dst.getAirportDST().getAirportCode());
							splitsPerAirport.add(tsDummyRight);
							splitLimit = itenaryEndDate;
						}
						isDstApplied = true;
					} else if (dstEnd >= itenaryEndDate) {
						TimeSliceDTO tsDummy = getTimeSlice(new Date(splitLimit), new Date(itenaryEndDate), dst.getAirportDST()
								.getDstAdjustTime(), dst.getAirportDST().getAirportCode());
						splitsPerAirport.add(tsDummy);
						splitLimit = itenaryEndDate;
						isDstApplied = true;
					}
				} else if (splitLimit <= dstStart && splitLimit <= dstEnd && (dstStart != dstEnd)) {
					if (dstEnd <= itenaryEndDate) {
						TimeSliceDTO tsDummyLeft = getTimeSlice(new Date(splitLimit), new Date(dstStart - dateOffset), dst
								.getAirportDST().getDstAdjustTime(), dst.getAirportDST().getAirportCode());
						TimeSliceDTO tsDummyMiddle = getTimeSlice(new Date(dstStart), new Date(dstEnd), dst.getAirportDST()
								.getDstAdjustTime(), dst.getAirportDST().getAirportCode());
						splitsPerAirport.add(tsDummyLeft);
						splitsPerAirport.add(tsDummyMiddle);
						splitLimit = dstEnd;
						if (i + 1 == dsts.size() && !isSameDayByTimeStamp(dstEnd, itenaryEndDate)) {
							TimeSliceDTO tsDummyRight = getTimeSlice(new Date(dstEnd + dateOffset), new Date(itenaryEndDate), dst
									.getAirportDST().getDstAdjustTime(), dst.getAirportDST().getAirportCode());
							splitsPerAirport.add(tsDummyRight);
							splitLimit = itenaryEndDate;
						}
						isDstApplied = true;
					} else if (dstEnd >= itenaryEndDate) {
						TimeSliceDTO tsDummyLeft = getTimeSlice(new Date(splitLimit), new Date(dstStart - dateOffset), dst
								.getAirportDST().getDstAdjustTime(), dst.getAirportDST().getAirportCode());
						TimeSliceDTO tsDummyRight = getTimeSlice(new Date(dstStart), new Date(itenaryEndDate), dst
								.getAirportDST().getDstAdjustTime(), dst.getAirportDST().getAirportCode());
						splitsPerAirport.add(tsDummyLeft);
						splitsPerAirport.add(tsDummyRight);
						splitLimit = itenaryEndDate;
						isDstApplied = true;
					}
				}
			}
			splitLimit = itenaryStartDate;
			if(!splitsPerAirport.isEmpty()){
				allSplitsPerAirport.add(splitsPerAirport);
			}
		}
		removeInvalidTimeSlices(allSplitsPerAirport);
		return getAllTimeSlices(allSplitsPerAirport, itenaryEndDate);
	}

	private static void removeInvalidTimeSlices(List<List<TimeSliceDTO>> allSplitsPerAirport) {
		if (allSplitsPerAirport != null && !allSplitsPerAirport.isEmpty()) {

			for (List<TimeSliceDTO> splitsPerAirport : allSplitsPerAirport) {
				if (splitsPerAirport != null && !splitsPerAirport.isEmpty()) {
					Iterator<TimeSliceDTO> splitsPerAirportItr = splitsPerAirport.iterator();
					while (splitsPerAirportItr.hasNext()) {
						TimeSliceDTO timeSliceDTO = splitsPerAirportItr.next();
						if (timeSliceDTO.getStartDate().getTime() > timeSliceDTO.getEndDate().getTime()) {
							splitsPerAirportItr.remove();
						}
					}
				}

			}

		}
	}
	
	// TODO:verify multi-leg
	private static Map<String, LegTimeSliceDTO> getScheduleApplicableTimeByAiport(FlightSchedule schedule) {

		Map<String, LegTimeSliceDTO> airportWiseEffectiveTimes = new HashMap<String, LegTimeSliceDTO>();

		if (schedule != null && schedule.getFlightScheduleLegs() != null && !schedule.getFlightScheduleLegs().isEmpty()) {
			Set<FlightScheduleLeg> flightScheduleLegs = schedule.getFlightScheduleLegs();

			for (FlightScheduleLeg leg : flightScheduleLegs) {
				String departure = leg.getOrigin();
				String arrival = leg.getDestination();
				LegTimeSliceDTO arrivalLeg = airportWiseEffectiveTimes.get(arrival);
				LegTimeSliceDTO departureLeg = airportWiseEffectiveTimes.get(departure);

				if (arrivalLeg == null) {
					arrivalLeg = new LegTimeSliceDTO(arrival);
				}
				arrivalLeg.setArrivalTime(leg.getEstArrivalTimeZulu());
				arrivalLeg.setArrivalDayOffset(leg.getEstArrivalDayOffset());

				if (departureLeg == null) {
					departureLeg = new LegTimeSliceDTO(departure);
				}
				departureLeg.setDepartureTime(leg.getEstDepartureTimeZulu());
				departureLeg.setDepartureDayOffset(leg.getEstDepartureDayOffset());

				airportWiseEffectiveTimes.put(departure, departureLeg);
				airportWiseEffectiveTimes.put(arrival, arrivalLeg);

			}
		}
		return airportWiseEffectiveTimes;
	}
	
	private static boolean isSameDayByTimeStamp(long day1, long day2) {
		return CalendarUtil.isSameDay(new Date(day1), new Date(day2));
	}
	
	private static long getEffectiveDstStartDate(AirportDstTO dst, Date actualGroundTime, int dayOffset) {
		long dateOffset = 86400000;
		long dstStart = dst.getAirportDST().getDstStartDateTime().getTime();
		if (dayOffset == 0
				&& CalendarUtil.getOnlyTime(dst.getAirportDST().getDstStartDateTime()).getTime() > CalendarUtil.getOnlyTime(
						actualGroundTime).getTime()) {
			// flight depart before DST apply
			long actualDstStart = dst.getAirportDST().getDstStartDateTime().getTime() + dateOffset;
			dstStart = new Date(actualDstStart).getTime();
		} else if (dayOffset == 1
				&& CalendarUtil.getOnlyTime(actualGroundTime).getTime() >= CalendarUtil.getOnlyTime(
						dst.getAirportDST().getDstStartDateTime()).getTime()) {
			long actualDstStart = dst.getAirportDST().getDstStartDateTime().getTime() - dateOffset;
			dstStart = new Date(actualDstStart).getTime();
		}

		return dstStart;
	}

	private static long getEffectiveDstEndDate(AirportDstTO dst, Date actualGroundTime, int dayOffset) {
		long dateOffset = 86400000;
		long dstEnd = dst.getAirportDST().getDstEndDateTime().getTime();

		if ((dayOffset == 0 && CalendarUtil.getOnlyTime(dst.getAirportDST().getDstEndDateTime()).getTime() < CalendarUtil
				.getOnlyTime(actualGroundTime).getTime()) || (dayOffset == 1)) {

			if (dayOffset == 1
					&& CalendarUtil.getOnlyTime(actualGroundTime).getTime() > CalendarUtil.getOnlyTime(
							dst.getAirportDST().getDstEndDateTime()).getTime()) {
				dateOffset = dateOffset * 2;
			}

			// flight depart before DST apply
			long actualDstStart = dst.getAirportDST().getDstEndDateTime().getTime() - dateOffset;
			dstEnd = new Date(actualDstStart).getTime();
		}

		return dstEnd;
	}

}
