package com.isa.thinair.gdsservices.core.bl.typeA.ticketcontrol;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegmentETicket;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageHandler;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSServicesSupportJDBCDAO;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.gdsservices.core.util.GdsUtil;
import com.isa.thinair.gdsservices.core.util.TypeANotes;
import com.isa.typea.common.ErrorInformation;
import com.isa.typea.common.FlightInformation;
import com.isa.typea.common.MessageFunction;
import com.isa.typea.common.OriginDestinationInformation;
import com.isa.typea.common.ReservationControlInformation;
import com.isa.typea.common.TicketControlTicketNumberDetails;
import com.isa.typea.common.TicketCoupon;
import com.isa.typea.common.Traveller;
import com.isa.typea.common.TravellerTicketControlInformation;
import com.isa.typea.tkcres.AATKCRES;
import com.isa.typea.tkcres.TKCRESMessage;
import com.isa.typea.tkcuac.AATKCUAC;
import com.isa.typea.tkcuac.TKCUACMessage;

/**
 * @isa.module.command name="unsolicitedTicketControlHandler"
 */
public class UnsolicitedTicketControlHandler extends EdiMessageHandler {

	private AATKCUAC tkcUac;
	private AATKCRES tkcRes;

	private String pnr;
	private LCCClientReservation reservation;

	protected void handleMessage() {
		TKCUACMessage tkcuacMessage;
		TKCRESMessage tkcresMessage;
		MessageFunction messageFunction;

		tkcUac = (AATKCUAC)messageDTO.getRequest();
		tkcuacMessage = tkcUac.getMessage();

		tkcRes =  new AATKCRES();
		tkcresMessage = new TKCRESMessage();
		tkcresMessage.setMessageHeader(GDSDTOUtil.createRespMessageHeader(tkcuacMessage.getMessageHeader(),
				messageDTO.getRequestMetaDataDTO().getMessageReference(), messageDTO.getRequestMetaDataDTO().getIataOperation().getResponse()));
		tkcRes.setHeader(GDSDTOUtil.createRespEdiHeader(tkcUac.getHeader(), tkcresMessage.getMessageHeader(), messageDTO));
		tkcRes.setMessage(tkcresMessage);
		messageDTO.setResponse(tkcRes);

		try {
			//TODO -- check message function and do the delegation
			airportControlMessage();
		} catch (GdsTypeAException e) {
			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(TypeAConstants.MessageFunction.UNSOLICITED_AIRPORT_CONTROL);
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			tkcresMessage.setMessageFunction(messageFunction);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(e.getEdiErrorCode());
			tkcresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);
		} catch (Exception e) {
			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(TypeAConstants.MessageFunction.UNSOLICITED_AIRPORT_CONTROL);
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			tkcresMessage.setMessageFunction(messageFunction);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR);
			tkcresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);
		}
	}

	private void airportControlMessage() throws GdsTypeAException {

		TKCUACMessage tkcuacMessage;
		TKCRESMessage tkcresMessage;
		MessageFunction messageFunction;

		tkcuacMessage = tkcUac.getMessage();

		handleAirportControlMessage(tkcuacMessage);

		tkcresMessage =  tkcRes.getMessage();

		messageFunction = tkcuacMessage.getMessageFunction();
		messageFunction.setResponseType(TypeAConstants.ResponseType.PROCESSED_SUCCESSFULLY);
		tkcresMessage.setMessageFunction(messageFunction);
		tkcRes.setMessage(tkcresMessage);

		messageDTO.setResponse(tkcRes);
	}

	private void handleAirportControlMessage(TKCUACMessage tkcuacMessage) throws GdsTypeAException {
		String pnr;
		List<ReservationControlInformation> reservationControlInformationList;
		LCCClientReservation reservation;
		LCCClientReservationPax lccPax;
		List<EticketTO> eticketTOs;
		EticketTO eticketTO;
		OriginDestinationInformation originDestinationInfo;
		List<FlightInformation> flightInformationList;
		LccClientPassengerEticketInfoTO lccTicket;

		for (TravellerTicketControlInformation travellers : tkcuacMessage.getTravellers()) {
			eticketTOs = new ArrayList<EticketTO>();
			reservationControlInformationList = travellers.getReservationControlInformation();

			try {
				pnr = getOwnPnr(reservationControlInformationList);
			} catch (ModuleException e) {
				throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR, TypeANotes.ErrorMessages.NO_MESSAGE);
			}

			if (pnr != null) {
				try {
					reservation = GDSServicesUtil.loadLccReservation(pnr);
				} catch (ModuleException e) {
					throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.PASSENGER_NAME_MISMATCH_WITH_PNR, TypeANotes.ErrorMessages.NO_MESSAGE);
				}
				for (Traveller traveller : travellers.getTravellers()) {
					eticketTOs = new ArrayList<EticketTO>();
					lccPax = GdsUtil.getLccPax(travellers, traveller, reservation);

					if (lccPax != null) {

						for (TicketControlTicketNumberDetails ticketNumberDetails : travellers.getTickets()) {
							for (TicketCoupon  ticketCoupon : ticketNumberDetails.getTicketCoupon()) {
								lccTicket = GdsUtil.lccPaxEticket(ticketNumberDetails, ticketCoupon, lccPax.geteTickets());

								if (lccTicket != null) {
									eticketTO = new EticketTO();
									eticketTO.setPnrPaxFareSegId(lccTicket.getPpfsId());
									eticketTO.setEticketId(lccTicket.getEticketId());
									eticketTO.setExternalCouponStatus(ticketCoupon.getStatus());
									eticketTO.setExternalCouponControl(ReservationPaxFareSegmentETicket.ExternalCouponControl.OPERATING_CARRIER);

									eticketTOs.add(eticketTO);
								} else {
									// TODO -- error -- no matching segment in our side
								}
							}
						}
					} else {
						// TODO -- error -- no matching pax in our side
					}

					try {
						GDSServicesModuleUtil.getReservationBD().updateExternalEticketInfo(eticketTOs);
					} catch (ModuleException e) {
						throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR, TypeANotes.ErrorMessages.NO_MESSAGE);
					}
				}
			} else {
				// TODO -- error
			}

		}
	}

	private String getOwnPnr(List<ReservationControlInformation> colReservationControlInfo) throws ModuleException {

		String pnr = null;

		for (ReservationControlInformation reservationControlInfo : colReservationControlInfo) {
			if (reservationControlInfo.getAirlineCode().equals(GDSServicesUtil.getCarrierCode())) {
				pnr = reservationControlInfo.getReservationControlNumber();
			}
		}

		ReservationControlInformation resControlInfo = colReservationControlInfo.get(0);
		if(pnr == null){
			pnr = getGDSServicesSupportDAO()
					.getPnrFromExternalRecordLocator(resControlInfo.getReservationControlNumber(), resControlInfo.getAirlineCode());
		}
		return pnr;
	}

	private GDSServicesSupportJDBCDAO getGDSServicesSupportDAO(){
		return GDSServicesDAOUtils.DAOInstance.GDSSERVICES_SUPPORT_JDBC_DAO;
	}
}
