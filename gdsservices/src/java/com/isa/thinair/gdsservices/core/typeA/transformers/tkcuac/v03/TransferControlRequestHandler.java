package com.isa.thinair.gdsservices.core.typeA.transformers.tkcuac.v03;

import static com.isa.thinair.gdsservices.core.util.IATADataExtractionUtil.getValue;
import iata.typea.v031.tkcuac.CPN;
import iata.typea.v031.tkcuac.GROUP1;
import iata.typea.v031.tkcuac.GROUP2;
import iata.typea.v031.tkcuac.GROUP3;
import iata.typea.v031.tkcuac.IFT;
import iata.typea.v031.tkcuac.MON;
import iata.typea.v031.tkcuac.MON.MON02;
import iata.typea.v031.tkcuac.RCI;
import iata.typea.v031.tkcuac.RPI;
import iata.typea.v031.tkcuac.TIF;
import iata.typea.v031.tkcuac.TKCUAC;
import iata.typea.v031.tkcuac.TKT;
import iata.typea.v031.tkcuac.TVL;
import iata.typea.v031.tkcuac.TXD;
import iata.typea.v031.tkcuac.TXD.TXD02;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.ReconcilePassengersTO;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airreservation.api.dto.FlightReconcileDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.PassengerType;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.core.typeA.model.AirSegment;
import com.isa.thinair.gdsservices.core.typeA.model.ExternalReservationPaxDetail;
import com.isa.thinair.gdsservices.core.typeA.model.MonetaryInformation;
import com.isa.thinair.gdsservices.core.typeA.model.PaxETicket;
import com.isa.thinair.gdsservices.core.typeA.model.PaxETicketSegment;
import com.isa.thinair.gdsservices.core.typeA.model.RDD;
import com.isa.thinair.gdsservices.core.util.IATADataExtractionUtil;
import com.isa.thinair.gdsservices.core.util.TypeADataConvertorUtill;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class TransferControlRequestHandler {

	/** Holds the flight inventory res bd **/
	private FlightInventoryResBD flightInventoryResBD;
	/** Holds the flight segment bd */
	private SegmentBD segmentBD;
	private PassengerBD passengerBD;
	private TravelAgentBD agentBD;

	TransferControlRequestHandler() {
		super();
		flightInventoryResBD = GDSServicesModuleUtil.getFlightInventoryResBD();
		segmentBD = GDSServicesModuleUtil.getReservationSegmentBD();
		agentBD = GDSServicesModuleUtil.getTravelAgentBD();
		passengerBD = GDSServicesModuleUtil.getPassengerBD();
	}

	public Map<String, Object> extractCommandParams(TKCUAC tkcuac) throws ModuleException {
		// origin destination information.
		String marketingCarrier = getValue(tkcuac.getORG(), RDD.C, 1, 1);
		String messageFunction = getValue(tkcuac.getMSG(), RDD.C, 1, 2);

		// Per pax detail List
		List<GROUP1> group1List = tkcuac.getGROUP1();
		Map<String, Object> commandParams = null;
		if (group1List != null) {
			// Only one pax detail will be inclued in the TKCUAC
			GROUP1 group1 = PlatformUtiltiies.getFirstElement(group1List);
			commandParams = extractPerPaxInfo(group1, marketingCarrier);

		} else {
			throw new ModuleException("gdsservices.typea.validators.require.missing");
		}

		// add message function required to build the responce
		commandParams.put(TypeACommandParamNames.MESSAGE_FUNCTION, messageFunction);

		return commandParams;
	}

	private Map<String, Object> extractPerPaxInfo(GROUP1 group1, String marketingCarrier) throws ModuleException {
		Map<String, Object> commandParamMap = new HashMap<String, Object>();

		ExternalReservationPaxDetail reservationPaxDetail = new ExternalReservationPaxDetail();
		IReservation iReservation = new ReservationAssembler(null, null);
		iReservation.setModifiable('N');

		// pax detail
		TIF tif = group1.getTIF();
		String lastName = getValue(tif, RDD.M, 1, 1);
		String firstNameAndTitle = getValue(PlatformUtiltiies.getFirstElement(tif.getTIF02()), RDD.M, 1);
		String firstName = IATADataExtractionUtil.getSplitedElement(firstNameAndTitle.split(" "), 0, "TBA");
		String title = IATADataExtractionUtil.getSplitedElement(firstNameAndTitle.split(" "), 1, "MR");
		String paxType = getValue(tif, RDD.M, 1, 2);
		String aaPaxType = TypeADataConvertorUtill.convertPaxType(paxType);

		reservationPaxDetail.setFirstName(firstName);
		reservationPaxDetail.setLastName(lastName);
		reservationPaxDetail.setTitle(title);
		reservationPaxDetail.setPaxType(aaPaxType);
		commandParamMap.put(TypeACommandParamNames.PAX_TYPE, aaPaxType);

		// TAI is ignored as we will be linking a specific agent to all the bookings created from HR
		// Do we need to setup a mapping between 1A In-house identification no for agents and AA agentCode

		// linking the external reservation from RCI locator with the AA reservation
		RCI rci = group1.getRCI();

		// TODO validate external pnr and carrier code(company id)
		String companyId = getValue(PlatformUtiltiies.getFirstElement(rci.getRCI01()), RDD.M, 1);
		String extRecLoc = getValue(PlatformUtiltiies.getFirstElement(rci.getRCI01()), RDD.M, 2);
		iReservation.setExtRecordLocator(extRecLoc);

		// MonetaryInformation processign
		MonetaryInformation monetaryInformation = extractMonetaryInformation(group1.getMON(), group1.getTXD());
		reservationPaxDetail.setMonetaryInformation(monetaryInformation);

		// FOP is ignored as it contains the total payment including the external segments
		// PTK is ignored as noting to do from AA side
		// ODI is ignored as same information is available in segment informations
		// ORG is ignored as we are recording the reservatoin against a fix agent in AA side
		// EQN is ignored

		// Fare calculation area processing
		List<AirSegment> fareCalculationArea = extractFareCalculation(group1.getIFT());

		List<GROUP2> group2List = group1.getGROUP2();

		for (GROUP2 group2 : group2List) {
			PaxETicket paxETicket = extractPaxEticket(group2);
			reservationPaxDetail.addPaxETicker(paxETicket);
		}

		List<PaxETicketSegment> allETicketSegmentList = reservationPaxDetail.getAllEticketSegments();
		// No of segments in the fare calculation area and no segments in eticket should match
		if (!fareCalculationArea.isEmpty() && (fareCalculationArea.size() != allETicketSegmentList.size())) {
			throw new ModuleException("Invalide fare calculation area present");
		}

		// linking fare amount with segments
		int i = 0;
		for (AirSegment airSegment : fareCalculationArea) {
			PaxETicketSegment paxETicketSegment = allETicketSegmentList.get(i);
			if (paxETicketSegment.getFrom().equals(airSegment.getAirportFrom())
					&& paxETicketSegment.getTo().equals(airSegment.getAirportTo())
					&& paxETicketSegment.getCarrierCode().equals(airSegment.getCarrier())) {
				// We found the equal fare information for the given paxETicketSegment
				paxETicketSegment.setFareAmount(airSegment.getFareAmount());
			} else {
				// throw an exception ?
			}
			i++;
		}

		// getting the liked agent for external carrier.
		String externalLinkingAgentCode = AppSysParamsUtil.getGdsConnectivityAgent(marketingCarrier);
		Agent externalAgent = agentBD.getAgent(externalLinkingAgentCode);

		// Creating ondFareDto for each own segment onsideding them as individual segments
		Collection<OndFareDTO> ondFareDTOs = new ArrayList<OndFareDTO>();
		for (PaxETicketSegment paxETicketSegment : reservationPaxDetail.getOwnEticketSegments()) {

			FlightReconcileDTO flightReconcileDTO = segmentBD.getFlightSegmentsForReconcilation(
					paxETicketSegment.getFullFlightNumber(), paxETicketSegment.getDepartureDate(), paxETicketSegment.getFrom(),
					paxETicketSegment.getTo());

			if (flightReconcileDTO == null) {
				throw new ModuleException("gdsservices.typea.invalid.segments");
			}

			ReconcilePassengersTO reconcilePassengersTO = new ReconcilePassengersTO();
			trackNoOfPassengersForPaxTypes(reservationPaxDetail.getPaxType(), reconcilePassengersTO);

			reconcilePassengersTO.setCabinClassCode("Y"); // Hard coded at the moment. Can we derive it form the RBD
			reconcilePassengersTO.setFlightId(flightReconcileDTO.getFlightId().intValue());
			reconcilePassengersTO.setFlightNumber(paxETicketSegment.getFullFlightNumber());
			reconcilePassengersTO.setSegmentCode(flightReconcileDTO.getSegementCode());
			reconcilePassengersTO.setFlightSegId(flightReconcileDTO.getFlightSegId().intValue());
			reconcilePassengersTO.setDepartureDateTimeLocal(flightReconcileDTO.getDepartureDateTimeLocal());
			reconcilePassengersTO.setDepartureDateTimeZulu(flightReconcileDTO.getDepartureDateTimeZulu());
			reconcilePassengersTO.setPaxCategoryCode(AirPricingCustomConstants.BookingPaxType.ANY);
			reconcilePassengersTO.setBookingClassCode(paxETicketSegment.getRbd());

			reconcilePassengersTO.setAgentCode(externalAgent.getAgentCode());

			OndFareDTO ondFareDTO = flightInventoryResBD.quoteExternallyBookedReservationFares(reconcilePassengersTO);
			// overide fare if fare calculation is given present in the request otherwise we are using the internally
			// quated fare to save the reservation
			if (paxETicketSegment.getFareAmount() != null) {
				FareSummaryDTO fareSummaryDTO = ondFareDTO.getFareSummaryDTO();
				overideFareAmount(fareSummaryDTO, reservationPaxDetail.getPaxType(), paxETicketSegment.getFareAmount());
			}
			paxETicketSegment.setFltSegmentId(reconcilePassengersTO.getFlightSegId());
			ondFareDTOs.add(ondFareDTO);
		}

		// add ond fare dto to the reservation
		iReservation.addOndFareDTOs(ondFareDTOs);

		iReservation.setLastCurrencyCode(externalAgent.getCurrencyCode());
		iReservation.setLastModificationTimestamp(new Date());

		BigDecimal payedAmount = passengerBD.getPayedAmountFromOndFareDto(ondFareDTOs, reservationPaxDetail.getPaxType());
		IPayment iPaymentPassenger = new PaymentAssembler();

		// make mayCurrency as the anent currency code.
		PayCurrencyDTO payCurrencyDTO = null;
		if (AppSysParamsUtil.isStorePaymentsInAgentCurrencyEnabled()) {
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			CurrencyExchangeRate currExRate = exchangeRateProxy.getCurrencyExchangeRate(externalAgent.getCurrencyCode());
			payCurrencyDTO = new PayCurrencyDTO(currExRate.getCurrency().getCurrencyCode(),
					currExRate.getMultiplyingExchangeRate(), currExRate.getCurrency().getBoundryValue(), currExRate.getCurrency()
							.getBreakPoint());
		}

		iPaymentPassenger.addAgentCreditPayment(externalAgent.getAgentCode(), payedAmount, null, payCurrencyDTO, null, null, null);

		// adding segment/external segments to the reservation.
		Map<Integer, EticketDTO> eTicketSegInfo = assembleSegmentInfo(iReservation, reservationPaxDetail);

		// linking pax to reservation. If infant is pax type we need to add that to previously created reservation
		Map<Integer, Map<Integer, EticketDTO>> paxSegEticket = assemblePaxInfo(iReservation, iPaymentPassenger,
				reservationPaxDetail, eTicketSegInfo, commandParamMap);

		commandParamMap.put(TypeACommandParamNames.E_TICKET_MAP, paxSegEticket);

		// add contact information
		addContactInformation(iReservation, reservationPaxDetail, marketingCarrier);
		commandParamMap.put(TypeACommandParamNames.RESERVATION, iReservation);
		commandParamMap.put(TypeACommandParamNames.OND_FARE_DTOS, ondFareDTOs);
		commandParamMap.put(TypeACommandParamNames.EXTERNAL_COMPANY_ID, companyId);

		return commandParamMap;
	}

	private Map<Integer, EticketDTO> assembleSegmentInfo(IReservation iReservation,
			ExternalReservationPaxDetail reservationPaxDetail) {

		Map<Integer, EticketDTO> eticketSegmentMap = new HashMap<Integer, EticketDTO>();

		// adding own segments
		List<PaxETicketSegment> ownEticketSegments = reservationPaxDetail.getOwnEticketSegments();
		for (PaxETicketSegment ticketSegment : ownEticketSegments) {
			iReservation.addOutgoingSegment(ticketSegment.getSegmentSeq(), ticketSegment.getFltSegmentId(),
					ticketSegment.getSegmentSeq(), null, null, null, null, null);

			EticketDTO eticketDTO = new EticketDTO(ticketSegment.getEticket(), ticketSegment.getCoupunSeq());
			eticketSegmentMap.put(ticketSegment.getSegmentSeq(), eticketDTO);
		}

		// adding external segments
		List<PaxETicketSegment> externalEticketSegments = reservationPaxDetail.getExternalEticketSegments();
		for (PaxETicketSegment eTicketSegmet : externalEticketSegments) {
			String externalCarrierCode = eTicketSegmet.getCarrierCode();
			iReservation.addExternalPnrSegment(eTicketSegmet.getSegmentSeq(), externalCarrierCode,
					eTicketSegmet.getFullFlightNumber(), eTicketSegmet.getSegmentCode(), "Y", eTicketSegmet.getDepartureDate(),
					null);
		}

		return eticketSegmentMap;
	}

	private Map<Integer, Map<Integer, EticketDTO>> assemblePaxInfo(IReservation iReservation, IPayment iPaymentPassenger,
			ExternalReservationPaxDetail reservationPaxDetail, Map<Integer, EticketDTO> eTicketSegInfo,
			Map<String, Object> commandParamMap) throws ModuleException {

		SegmentSSRAssembler segmentSSRs = new SegmentSSRAssembler();
		Map<Integer, Map<Integer, EticketDTO>> eticketInfo = new HashMap<Integer, Map<Integer, EticketDTO>>();
		String paxType = reservationPaxDetail.getPaxType();
		PaxAdditionalInfoDTO paxAdditionalInfoDTO = new PaxAdditionalInfoDTO();

		int paxSequence = 1;
		if (paxType.equals(PassengerType.ADULT)) {

			iReservation.addSingle(reservationPaxDetail.getFirstName(), reservationPaxDetail.getLastName(),
					reservationPaxDetail.getTitle(), null, null, paxSequence, paxAdditionalInfoDTO, null, iPaymentPassenger,
					segmentSSRs, null, null, null, null, CommonsConstants.INDIVIDUAL_MEMBER, null, null, null,
					null, null);
			eticketInfo.put(paxSequence, eTicketSegInfo);

		} else if (paxType.equals(PassengerType.CHILD)) {
			iReservation.addChild(reservationPaxDetail.getFirstName(), reservationPaxDetail.getLastName(),
					reservationPaxDetail.getTitle(), null, null, paxSequence, paxAdditionalInfoDTO, null,
					iPaymentPassenger, segmentSSRs, null, null, null, null, CommonsConstants.INDIVIDUAL_MEMBER, null, null, null,
					null, null);
			eticketInfo.put(paxSequence, eTicketSegInfo);

		} else if (paxType.equals(PassengerType.INFANT)) {
			iReservation.addInfant(reservationPaxDetail.getFirstName(), reservationPaxDetail.getLastName(),
					reservationPaxDetail.getTitle(), null, null, paxSequence, -1, paxAdditionalInfoDTO, null, null, segmentSSRs,
					null, null, null, null, CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);
			// if the pax type is INF we need to add it to the linked with parent of previously created reservation
			eticketInfo.put(paxSequence, eTicketSegInfo);
			commandParamMap.put(TypeACommandParamNames.PAX_PAYMENT, iPaymentPassenger);

		} else {
			throw new ModuleException("airreservations.arg.unidentifiedPaxTypeDetected");
		}

		return eticketInfo;
	}

	private void addContactInformation(IReservation iReservation, ExternalReservationPaxDetail reservationPaxDetail,
			String marketingCarrier) {

		ReservationContactInfo contactInfo = new ReservationContactInfo();
		contactInfo.setTitle(reservationPaxDetail.getTitle());
		contactInfo.setFirstName(reservationPaxDetail.getFirstName());
		contactInfo.setLastName(reservationPaxDetail.getLastName());
		contactInfo.setPreferredLanguage(Locale.ENGLISH.toString());

		iReservation.addContactInfo(marketingCarrier + " Created External Reservation", contactInfo, null);

	}

	private void overideFareAmount(FareSummaryDTO fareSummaryDTO, String paxType, BigDecimal overriedFare) throws ModuleException {
		if (paxType.equals(PassengerType.ADULT)) {
			fareSummaryDTO.setAdultFare(overriedFare.doubleValue());
		} else if (paxType.equals(PassengerType.CHILD)) {
			fareSummaryDTO.setChildFare(overriedFare.doubleValue());
		} else if (paxType.equals(PassengerType.INFANT)) {
			fareSummaryDTO.setInfantFare(overriedFare.doubleValue());
		} else {
			throw new ModuleException("airreservations.arg.unidentifiedPaxTypeDetected");
		}
	}

	private PaxETicket extractPaxEticket(GROUP2 group2) throws ModuleException {

		PaxETicket paxETicket = new PaxETicket();

		// ETicket information processing.
		TKT tkt = group2.getTKT();
		String eticketNumber = getValue(tkt, RDD.M, 1, 1);
		paxETicket.setEticketNumber(eticketNumber);

		// per Eticket per segment prosessing.
		List<GROUP3> group3List = group2.getGROUP3();
		for (GROUP3 group3 : group3List) {

			CPN cpn = group3.getCPN();
			String eticketSeq = getValue(cpn, RDD.M, 1, 1);
			String segmentSeq = getValue(cpn, RDD.M, 1, 8);

			TVL tvl = PlatformUtiltiies.getFirstElement(group3.getTVL());
			String departureDatePart = getValue(tvl, RDD.M, 1, 1);
			BigDecimal departureTimePart = getValue(tvl, RDD.M, 1, 2);
			Date departureDate = IATADataExtractionUtil.getDate(new BigDecimal(departureDatePart), departureTimePart);

			String from = getValue(tvl, RDD.M, 2, 1);
			String to = getValue(tvl, RDD.M, 3, 1);
			String operatioinCarrier = getValue(tvl, RDD.M, 4, 1);
			String flightNumber = getValue(tvl, RDD.M, 5, 1);
			String rbd = getValue(tvl, RDD.M, 5, 2);

			RPI rpi = group3.getRPI();

			String status = PlatformUtiltiies.getFirstElement(rpi.getRPI02());

			PaxETicketSegment eTicketSegment = new PaxETicketSegment(Integer.valueOf(segmentSeq), Integer.valueOf(eticketSeq),
					from, to, departureDate, operatioinCarrier, flightNumber, rbd, status);
			paxETicket.addPaxEticketSegments(eTicketSegment);
		}

		return paxETicket;
	}

	private MonetaryInformation extractMonetaryInformation(MON mon, List<TXD> txdList) throws ModuleException {

		if (mon == null) {
			throw new ModuleException("gdsservices.typea.validators.require.missing");
		}

		MonetaryInformation monetaryInformation = new MonetaryInformation();

		String baseFareAmount = getValue(mon, RDD.M, 1, 2);
		String baseCurrency = getValue(mon, RDD.M, 1, 3);
		monetaryInformation.setBaseFareAmount(new BigDecimal(baseFareAmount), baseCurrency);

		List<MON02> mon02List = mon.getMON02();
		for (MON02 mon2 : mon02List) {

			String monType = getValue(mon2, RDD.M, 1);
			String amount = getValue(mon2, RDD.M, 2);
			String currency = getValue(mon2, RDD.C, 3);

			if (monType.equals("T")) {
				monetaryInformation.setTotalAmoutInPay(new BigDecimal(amount), currency);
			}

			if (monType.equals("E")) {
				monetaryInformation.setEquivalantFareAmountInPay(new BigDecimal(amount), currency);
			}
		}

		for (TXD txd : txdList) {
			List<TXD02> txd02List = txd.getTXD02();
			for (TXD02 txd02 : txd02List) {
				String amount = getValue(txd02, RDD.M, 1);
				String code = getValue(txd02, RDD.M, 4);
				monetaryInformation.addTaxCharge(code, new BigDecimal(amount));
			}
		}

		return monetaryInformation;
	}

	private List<AirSegment> extractFareCalculation(List<IFT> iftList) throws ModuleException {
		StringBuilder fareCalculationString = null;
		for (IFT ift : iftList) {
			String informationType = getValue(ift, RDD.M, 1, 2);
			if (informationType.equals("15")) { // we hit fare calculation area
				List<String> ift02List = ift.getIFT02();
				fareCalculationString = new StringBuilder();
				for (String iftSegment : ift02List) {
					fareCalculationString.append(iftSegment);
				}
			} else {
				continue;
			}
		}

		List<AirSegment> fareCalArea = new LinkedList<AirSegment>();
		;

		if (fareCalculationString != null) {

			// We have fare calculation area present in the TKCUAC
			// sample fare calculation area is given bellow.
			// IEV PS FRA1250.00PS IEV1250.00PS FRA1500.00LH CPH1780.65LH FRA1780.65NUC7561.30END
			// ROE1.000000XT72.00YQ16.00OY50.33RA13.08DE23.52YK2.78UD5.54UA20.27ZO

			String fareString = fareCalculationString.toString();
			int lastIndexOf = fareString.lastIndexOf("NUC");
			String farePart = fareString.substring(0, lastIndexOf - 1);
			int roeIndex = fareString.lastIndexOf("ROE");
			String roeAndTax = fareString.substring(roeIndex + 3);
			String[] split = farePart.split(" ");
			List<AirSegment> airSegments = new LinkedList<AirSegment>();
			AirSegment airSegment = new AirSegment(split[0], null, null, split[1]);
			airSegments.add(airSegment);
			for (int i = 2; i < split.length; i++) {
				String segment = split[i];
				int len = segment.length();
				String airport = segment.substring(0, 3);
				String carrier = segment.substring(len - 2, len);
				BigDecimal fare = new BigDecimal(segment.substring(3, len - 2));
				airSegments.add(new AirSegment(airport, null, fare, carrier));
			}

			for (int i = 0; i < airSegments.size() - 1; i++) {
				AirSegment from = airSegments.get(i);
				AirSegment to = airSegments.get(i + 1);
				AirSegment segment = new AirSegment(from.getAirportFrom(), to.getAirportFrom(), to.getFareAmount(),
						from.getCarrier());
				fareCalArea.add(segment);
			}

			// extract the rate of exchangerete from the fare calculation area.
			Pattern p = Pattern.compile("\\d{0,5}\\.\\d{0,6}");
			Matcher m = p.matcher(roeAndTax);
			if (m.find()) {
				BigDecimal ROE = new BigDecimal(m.group().toString());
				for (AirSegment airSeg : fareCalArea) {
					// converting to base currency
					airSeg.setFareAmount(AccelAeroCalculator.multiply(airSeg.getFareAmount(), ROE));
				}
			}
		}

		return fareCalArea;
	}

	/**
	 * set the pax count
	 * 
	 * @param paxType
	 * @param reconcilePassengersTO
	 */
	private void trackNoOfPassengersForPaxTypes(String paxType, ReconcilePassengersTO reconcilePassengersTO) {

		if (paxType.equals(PassengerType.ADULT)) {
			reconcilePassengersTO.setNoOfAdults(1);
		} else if (paxType.equals(PassengerType.CHILD)) {
			reconcilePassengersTO.setNoOfChildren(1);
		}

		// No Infants are not relevant for inventoty perpose
	}

}
