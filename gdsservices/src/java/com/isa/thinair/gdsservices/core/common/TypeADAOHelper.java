package com.isa.thinair.gdsservices.core.common;

import java.util.Date;

import com.isa.thinair.gdsservices.api.model.GDSTransactionStatus;
import com.isa.typea.common.BaseEDIMessage;

public class TypeADAOHelper {
	public static GDSTransactionStatus getGDSTransactionStatus(BaseEDIMessage ediMessage) {
		String tnxRefId = ediMessage.getMessageHeader().getCommonAccessReference();

		GDSTransactionStatus gdsTransactionStatus = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO
				.getGDSTransactionStatusFromRef(tnxRefId);

		if (gdsTransactionStatus == null) {
			gdsTransactionStatus = new GDSTransactionStatus();
			gdsTransactionStatus.setTxnStartTime(new Date());
			gdsTransactionStatus.setGdsTransactionRef(tnxRefId);
			gdsTransactionStatus.setTnxStage(ediMessage.getMessageHeader().getMessageId().getMessageType());
		}

		return gdsTransactionStatus;
	}
}
