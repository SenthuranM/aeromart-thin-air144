package com.isa.thinair.gdsservices.core.util;

public interface GDSConstants {

	String DELIM_SEGMENT_AIRPORTS = "/";

	enum MessageTransformationDirection {
		EDI_TO_AA, AA_TO_EDI
	}

	public static interface InMessageQueueStatus {
		public static final String LOCK = "L";
		public static final String UNLOCK = "U";
	}

	public static interface InMessageScheduleType {
		public static final String FLIGHT = "F";
		public static final String SCHEDULE = "S";
	}
}
