package com.isa.thinair.gdsservices.core.typeA.transformers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.typea.common.ReservationControlInformation;

public class DTOsTransformerUtil {

	// Assumptions -- in-bound route is reverse of out-bound
	public static Pair<List<FlightSegmentDTO>, List<FlightSegmentDTO>> seperateOutBoundAndInBoundSegs(
			List<FlightSegmentDTO> flightSegs) {
		List<FlightSegmentDTO> outBoundSegs = new ArrayList<FlightSegmentDTO>();
		List<FlightSegmentDTO> inBoundSegs = new ArrayList<FlightSegmentDTO>();

		Set<String> processedAirports = new HashSet<String>();
		boolean returnStarted = false;

		Collections.sort(flightSegs, new Comparator<FlightSegmentDTO>() {
			public int compare(FlightSegmentDTO o1, FlightSegmentDTO o2) {
				return o1.getDepartureDateTimeZulu().compareTo(o2.getDepartureDateTimeZulu());
			}
		});

		for (FlightSegmentDTO flightSegment : flightSegs) {
			if (!returnStarted) {
				if (processedAirports.contains(flightSegment.getToAirport())) {
					returnStarted = true;
					inBoundSegs.add(flightSegment);
				} else {
					processedAirports.add(flightSegment.getFromAirport());
					outBoundSegs.add(flightSegment);
				}
			} else {
				inBoundSegs.add(flightSegment);
			}
		}

		return Pair.of(outBoundSegs, inBoundSegs);
	}

	public static String getOwnPnr(List<ReservationControlInformation> colReservationControlInfo) {
		String pnr = null;
		for (ReservationControlInformation reservationControlInfo : colReservationControlInfo) {
			if (reservationControlInfo.getAirlineCode().equals(GDSServicesUtil.getCarrierCode())) {
				pnr = reservationControlInfo.getReservationControlNumber();
				break;
			}
		}
		return pnr;
	}

}
