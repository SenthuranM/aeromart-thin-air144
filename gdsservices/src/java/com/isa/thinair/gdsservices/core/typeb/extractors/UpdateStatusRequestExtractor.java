package com.isa.thinair.gdsservices.core.typeb.extractors;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.CreateReservationRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.UpdateStatusRequest;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

public class UpdateStatusRequestExtractor extends ValidatorBase {

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {
		String pnrReference = ValidatorUtils.getPNRReference(bookingRequestDTO.getResponderRecordLocator());

		if (pnrReference.length() == 0) {
			String message = MessageUtil.getMessage("gdsservices.extractors.emptyPNRResponder");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		} else {
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
			return bookingRequestDTO;
		}
	}

	public static GDSReservationRequestBase getRequest(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO) {

		CreateReservationRequest createReservationRequest = new UpdateStatusRequest();
		CreateRequestExtractor.composeReservationRequest(gdsCredentialsDTO, bookingRequestDTO, createReservationRequest);
		return (UpdateStatusRequest) createReservationRequest;
	}
}
