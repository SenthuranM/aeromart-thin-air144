package com.isa.thinair.gdsservices.core.typeA.transformers.tkcreqres.v03;

import java.util.Collection;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.ReservationDisplayResult;
import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class SearchByEticket implements ITicketDisplayRequest {

	private static final long serialVersionUID = -2569619675054143456L;

	private String eticket;

	public SearchByEticket(String eticket) {
		super();
		this.eticket = eticket;
	}

	@Override
	public ReservationDisplayResult search() throws ModuleException {
		ReservationDisplayResult result = new ReservationDisplayResult();
		ReservationSearchDTO searchDTO = new ReservationSearchDTO();
		searchDTO.setETicket(eticket);
		Collection<ReservationDTO> reservations = GDSServicesModuleUtil.getReservationQueryBD().getReservations(searchDTO);
		if (reservations == null || reservations.isEmpty()) {
			result.getAppErrors().add(CodeSetEnum.CS9321.AppError_364); // Invalid ticket number
			return result;
		}

		// Retrive the actual reservation if we have a match
		ReservationDTO firstReservatoin = PlatformUtiltiies.getFirstElement(reservations);
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(firstReservatoin.getPnr());
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadEtickets(true);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadOndChargesView(true);
		Reservation reservation = GDSServicesModuleUtil.getReservationBD().getReservation(pnrModesDTO, null);
		result.addReservation(reservation);

		return result;
	}

}
