package com.isa.thinair.gdsservices.core.bl.internal.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.gdsservices.api.dto.typea.internal.TicketingEventRq;
import com.isa.thinair.gdsservices.api.dto.typea.internal.TicketingEventRs;
import com.isa.thinair.gdsservices.api.model.ExternalReservation;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.config.GDSServicesModuleConfig;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.dto.GDSChargeAdjustmentRQ;
import com.isa.thinair.airproxy.api.dto.GDSPaxChargesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.dto.ets.AeroMartUpdateExternalCouponStatusRq;
import com.isa.thinair.commons.api.dto.ets.ReservationControlInformation;
import com.isa.thinair.commons.api.dto.ets.TravellerTicketingInformation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketsAssembler;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.bl.internal.ticket.TicketIssuanceHandler;
import com.isa.thinair.gdsservices.core.bl.internal.ticket.TicketIssuanceHandlerImpl;
import com.isa.thinair.gdsservices.core.command.CommonUtil;
import com.isa.thinair.gdsservices.core.command.IssueEticketAction;
import com.isa.thinair.gdsservices.core.dto.FinancialInformationAssembler;
import com.isa.thinair.gdsservices.core.dto.TravellerTicketingInformationAdaptor;
import com.isa.thinair.gdsservices.core.typeA.transformers.ReservationDTOsTransformer;
import com.isa.thinair.gdsservices.core.typeA.transformers.ReservationDtoMerger;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.gdsservices.core.util.GdsDtoTransformer;
import com.isa.thinair.gdsservices.core.util.TypeANotes;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.platform.api.ServiceResponce;

public class SyncReservationTotalTicketPrice {

	private AeroMartUpdateExternalCouponStatusRq externalCouponStatusRq;
	private String pnr;
	private LCCClientReservation reservation;
	private List<com.isa.typea.common.TravellerTicketControlInformation> travellers = new ArrayList<>();
	private static Log log = LogFactory.getLog(IssueEticketAction.class);
	private boolean isFareBackdownAvailable;
	
	public SyncReservationTotalTicketPrice(AeroMartUpdateExternalCouponStatusRq externalCouponStatusRq) throws ModuleException {
		this.externalCouponStatusRq = externalCouponStatusRq;
		this.pnr = getOwnPnr(externalCouponStatusRq.getReservationControlInformation());
		loadReservation();
	}

	public void updateExternalCouponStatus() throws Exception {

		/*
		ExternalReservation externalReservation =
				GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO.gdsTypeAServiceGet(ExternalReservation.class, reservation.getPNR());

		if (externalReservation.getPriceSynced().equals(CommonsConstants.NO)) {

			TicketingEventRq ticketIssueRq = new TicketingEventRq();
			ticketIssueRq.setGdsId(reservation.getGdsId());
			ticketIssueRq.setPnr(reservation.getPNR());
			ticketIssueRq.setCalculateFinancialInfo(true);

			Map<Integer, Map<String, List<Integer>>> paxTicketsCoupons = new HashMap<>();
			for (LCCClientReservationPax pax : reservation.getPassengers()) {
				paxTicketsCoupons.put(pax.getPaxSequence(), new HashMap<>());

				for (LccClientPassengerEticketInfoTO et : pax.geteTickets()) {
					if (!paxTicketsCoupons.get(pax.getPaxSequence()).containsKey(et.getExternalPaxETicketNo())) {
						paxTicketsCoupons.get(pax.getPaxSequence()).put(et.getExternalPaxETicketNo(), new ArrayList<>());
					}

					paxTicketsCoupons.get(pax.getPaxSequence()).get(et.getExternalPaxETicketNo()).add(et.getExternalCouponNo());
				}
			}
			ticketIssueRq.setPaxTicketsCoupons(paxTicketsCoupons);

			GDSServicesModuleConfig gdsServicesModuleConfig = GDSServicesModuleUtil.getConfig();

			if (gdsServicesModuleConfig != null && (gdsServicesModuleConfig.getMatipClientHost() != null && gdsServicesModuleConfig.getMatipClientPort() != 0)) {
				TicketingEventRs rs = GDSServicesModuleUtil.getGDSNotifyBD().onTicketIssue(ticketIssueRq);

				if (rs.isSuccess()) {
					externalReservation = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO
							.gdsTypeAServiceGet(ExternalReservation.class, reservation.getPNR());

					externalReservation.setPriceSynced(CommonsConstants.YES);

					GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO.gdsTypeAServiceSaveOrUpdate(externalReservation);
				}
			}

		}
		*/

		handleDiscrepancies();
		updateExternalTicketAndCouponDetails();
	}

	private void handleDiscrepancies() throws Exception {

		try {

			LCCClientReservationPax pax;
			int pnrPaxId;

			TicketsAssembler ticketsAssembler = new TicketsAssembler(TicketsAssembler.Mode.PAYMENT);
			ticketsAssembler.setPnr(reservation.getPNR());
			// ticketsAssembler.setIpAddress("10.20.11.190");

			Map<Integer, GDSPaxChargesTO> paxAdjustment = new HashMap<Integer, GDSPaxChargesTO>();
			GDSPaxChargesTO paxChargesTO;

			GDSChargeAdjustmentRQ gdsChargeAdjustmentRQ = new GDSChargeAdjustmentRQ();
			gdsChargeAdjustmentRQ.setPnr(reservation.getPNR());
			gdsChargeAdjustmentRQ.setGroupPNR(reservation.isGroupPNR());
			gdsChargeAdjustmentRQ.setVersion(reservation.getVersion());
			gdsChargeAdjustmentRQ.setGdsChargesByPax(paxAdjustment);
			boolean isHandleMonetaryDiscrepancies = false;
			for (TravellerTicketingInformation travellerTicketingInformation : this.externalCouponStatusRq.getTravellers()) {
				if (travellerTicketingInformation != null) {
					TravellerTicketingInformationAdaptor travellerTicketingInformationAdaptor = new TravellerTicketingInformationAdaptor();
					com.isa.typea.common.TravellerTicketControlInformation traveller = travellerTicketingInformationAdaptor
							.adapt(travellerTicketingInformation);
					travellers.add(traveller);

					if (isFinancialInformationAvailable(traveller)) {
						isHandleMonetaryDiscrepancies = true;
						pax = GDSDTOUtil.resolveReservationPax(traveller, traveller.getTravellers().get(0), reservation);

						pnrPaxId = PaxTypeUtils.getPnrPaxId(pax.getTravelerRefNumber());

						FinancialInformationAssembler financialInformationAssembler = new FinancialInformationAssembler();
						financialInformationAssembler.setFinancialInformation(traveller.getText());
						financialInformationAssembler.setTaxDetails(traveller.getTaxDetails());
						financialInformationAssembler.setMonetaryInformation(traveller.getMonetaryInformation());
						isFareBackdownAvailable = financialInformationAssembler.isFareBreakdownInTKCUACAvailable();
						if(isFareBackdownAvailable){
							financialInformationAssembler.calculateAmounts();
						}
		
						
						if (!paxAdjustment.containsKey(pnrPaxId)) {
							paxChargesTO = new GDSPaxChargesTO();
							paxChargesTO.setPaxType(pax.getPaxType());
							paxChargesTO.setPnrPaxId(pnrPaxId);
							paxChargesTO.setFareAmount(BigDecimal.ZERO);
							paxChargesTO.setTaxAmount(BigDecimal.ZERO);
							paxChargesTO.setSurChargeAmount(BigDecimal.ZERO);
							paxAdjustment.put(pnrPaxId, paxChargesTO);
						}

						paxChargesTO = paxAdjustment.get(pnrPaxId);

						if (isFareBackdownAvailable) {
							paxChargesTO.setFareAmount(
									paxChargesTO.getFareAmount().add(financialInformationAssembler.getFareAmount()));
							paxChargesTO.setTaxAmount(
									paxChargesTO.getTaxAmount().add(financialInformationAssembler.getTaxAmount()));
							paxChargesTO.setSurChargeAmount(paxChargesTO.getSurChargeAmount()
									.add(financialInformationAssembler.getSurcharge()));
						} else {

							Gds gds = GDSServicesModuleUtil.getGdsBD().getGds(reservation.getGdsId());
							GDSDTOUtil.injectAmounts(paxChargesTO, gds.getCarrierCode(), traveller.getMonetaryInformation(), traveller.getTaxDetails());

						}

						GdsDtoTransformer.appendToTicketsAssembler(ticketsAssembler, traveller, pax);
					}

				}
			}

			if (isHandleMonetaryDiscrepancies) {
				ticketsAssembler.setGdsChargeAdjustmentRQ(gdsChargeAdjustmentRQ);

				TicketIssuanceHandler ticketIssuanceHandler = new TicketIssuanceHandlerImpl();
				ticketIssuanceHandler.handleMonetaryDiscrepancies(ticketsAssembler);
				loadReservation();
			}

		} catch (ModuleException e) {
			throw new ModuleException("gdsservices.sync.gds.charges.handle.discrepancies.error", "ERROR @ handleDiscrepancies");
		}

	}

	private void updateExternalTicketAndCouponDetails() throws ModuleException {
		try {
			if (travellers != null && !travellers.isEmpty()) {
				List<LccClientPassengerEticketInfoTO> lccCoupons = ReservationDtoMerger.getTicketControlMergedETicketCoupons(travellers,
						reservation);
				List<EticketTO> coupons = ReservationDTOsTransformer.toETicketTos(lccCoupons);

				ServiceResponce serviceResponce = GDSServicesModuleUtil.getReservationBD().updateExternalEticketInfo(coupons);
				if (!serviceResponce.isSuccess()) {
					throw new ModuleException(TypeANotes.ErrorMessages.NO_MESSAGE);
				}
			}
		} catch (ModuleException ex) {
			log.error(" updateExternalTicketAndCouponDetails Failed for GDS PNR " + pnr, ex);
			throw new ModuleException("gdsservices.update.external.coupon.status.error", "ERROR @ update External coupon status");
		} catch (Exception e) {
			log.error(" updateExternalTicketAndCouponDetails Failed for GDS PNR " + pnr, e);
			throw new ModuleException("gdsservices.update.external.coupon.status.error", "ERROR @ update External coupon status");
		}
	}

	private void loadReservation() throws ModuleException {
		try {
			reservation = GDSServicesUtil.loadLccReservation(pnr);
			if (reservation == null) {
				throw new ModuleException("gdsservices.no.pnr.exist", "ERROR @ loadReservation");
			}

			if (reservation != null && reservation.getGdsId() == null) {
				throw new ModuleException("gdsservices.not.a.gds.reservation", "gdsservices.not.a.gds.reservation");
			}
		} catch (ModuleException e) {
			throw new ModuleException("gdsservices.no.pnr.exist", e.getMessage());
		}
	}

	private static String getOwnPnr(List<ReservationControlInformation> colReservationControlInfo) throws ModuleException {

		String pnr = null;
		
		for (ReservationControlInformation reservationControlInfo : colReservationControlInfo) {
			if (reservationControlInfo.getAirlineCode().equals(GDSServicesUtil.getCarrierCode())) {
				pnr = reservationControlInfo.getReservationControlNumber();
			}
		}

		ReservationControlInformation resControlInfo = colReservationControlInfo.get(0);
		if(pnr == null){
			pnr = GDSServicesDAOUtils.DAOInstance.GDSSERVICES_SUPPORT_JDBC_DAO
					.getPnrFromExternalRecordLocator(resControlInfo.getReservationControlNumber(), resControlInfo.getAirlineCode());
		}
		return pnr;
	}
	

	private boolean isFinancialInformationAvailable(com.isa.typea.common.TravellerTicketControlInformation traveller) {

		boolean financialInfoAvailability = false;

		if (traveller != null && ((traveller.getText() != null && !traveller.getText().isEmpty())
				|| (traveller.getTaxDetails() != null && !traveller.getTaxDetails().isEmpty())
				|| (traveller.getMonetaryInformation() != null && !traveller.getMonetaryInformation().isEmpty()))) {

			financialInfoAvailability = true;
		}

		return financialInfoAvailability;
	}
}
