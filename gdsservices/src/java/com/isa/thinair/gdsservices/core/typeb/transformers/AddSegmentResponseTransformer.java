package com.isa.thinair.gdsservices.core.typeb.transformers;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.internal.AddSegmentRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.dto.internal.SegmentDetail;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ProcessStatus;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;

public class AddSegmentResponseTransformer {
	/**
	 * Transforms reservationResponseMap back to bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param reservationResponseMap
	 * @return
	 */
	public static BookingRequestDTO getResponse(BookingRequestDTO bookingRequestDTO,
			Map<ReservationAction, GDSReservationRequestBase> reservationResponseMap) {

		AddSegmentRequest addSegmentRequest = (AddSegmentRequest) reservationResponseMap.get(ReservationAction.ADD_SEGMENT);

		if (addSegmentRequest.isSuccess()) {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_SUCCESS);
			bookingRequestDTO = setSegmentStatuses(bookingRequestDTO, addSegmentRequest);
			bookingRequestDTO = setScheduleChanges(bookingRequestDTO, addSegmentRequest);
			bookingRequestDTO = ResponseTransformerUtil.setSSRStatus(bookingRequestDTO);
			bookingRequestDTO = ValidatorUtils.addResponse(bookingRequestDTO, addSegmentRequest);
		} else {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_FAILURE);
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, addSegmentRequest);
		}
		bookingRequestDTO.getErrors().addAll(addSegmentRequest.getErrorCode());

		return bookingRequestDTO;
	}

	/**
	 * Adds segment statuses to the bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param addSegmentRequest
	 */
	private static BookingRequestDTO setSegmentStatuses(BookingRequestDTO bookingRequestDTO, AddSegmentRequest addSegmentRequest) {

		List<BookingSegmentDTO> bookingSegmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();
		Collection<SegmentDetail> segmentDetails = addSegmentRequest.getSegmentDetails();

		Iterator<BookingSegmentDTO> iterSegDTO = bookingSegmentDTOs.iterator();

		while (iterSegDTO.hasNext()) {
			BookingSegmentDTO bookingSegmentDTO = (BookingSegmentDTO) iterSegDTO.next();

			for (SegmentDetail segmentDetail : segmentDetails) {
				Segment segment = segmentDetail.getSegment();

				if (GDSApiUtils.isEqual(bookingSegmentDTO, segment)) {
					String gdsActionOrStatusCode = GDSApiUtils.maskNull(bookingSegmentDTO.getActionOrStatusCode());

					if (gdsActionOrStatusCode.equals(GDSExternalCodes.ActionCode.NEED.getCode())
							|| gdsActionOrStatusCode.equals(GDSExternalCodes.ActionCode.NEED_IFNOT_HOLDING.getCode())
							|| gdsActionOrStatusCode.equals(GDSExternalCodes.ActionCode.SOLD.getCode())
							|| gdsActionOrStatusCode.equals(GDSExternalCodes.ActionCode.SOLD_FREE.getCode())
							|| gdsActionOrStatusCode.equals(GDSExternalCodes.ActionCode.SOLD_IFNOT_HOLDING.getCode())
							|| gdsActionOrStatusCode.equals(GDSExternalCodes.ActionCode.CODESHARE_SELL.getCode())) {

						String adviceStatusCode = ResponseTransformerUtil.getSegmentStatus(segment,
								bookingSegmentDTO.getActionOrStatusCode());

						bookingSegmentDTO.setAdviceOrStatusCode(adviceStatusCode);
					}
				}
			}
		}

		return bookingRequestDTO;
	}
	
	private static BookingRequestDTO setScheduleChanges (BookingRequestDTO bookingRequestDTO,
			AddSegmentRequest addSegmentRequest) {
		
		List<BookingSegmentDTO> bookingSegmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();
		Collection<SegmentDetail> segmentDetails = addSegmentRequest.getSegmentDetails();

		Iterator<BookingSegmentDTO> iterSegDTO = bookingSegmentDTOs.iterator();

		while (iterSegDTO.hasNext()) {
			BookingSegmentDTO bookingSegmentDTO = (BookingSegmentDTO) iterSegDTO.next();

			for (SegmentDetail segmentDetail : segmentDetails) {
				Segment segment = segmentDetail.getSegment();
				if (GDSApiUtils.isEqual(bookingSegmentDTO, segment)) {
					if (segmentDetail.isChangeInSceduleExists()) {
						bookingSegmentDTO.setChangeInScheduleExist(true);
						bookingSegmentDTO.setDepartureTime(segment.getDepartureTime());
						bookingSegmentDTO.setArrivalTime(segment.getArrivalTime());
						if (segment.getArrivalDayOffset() < 0) {
							bookingSegmentDTO.setDayOffSet(Math.abs(segment.getArrivalDayOffset()));
							bookingSegmentDTO.setDayOffSetSign("M");
						} else {
							bookingSegmentDTO.setDayOffSet(segment.getArrivalDayOffset());
						}
					}
				}
			}
		}
		
		return bookingRequestDTO;
		
	}
}
