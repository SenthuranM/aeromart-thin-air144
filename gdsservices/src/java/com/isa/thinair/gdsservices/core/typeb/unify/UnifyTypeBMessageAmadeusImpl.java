package com.isa.thinair.gdsservices.core.typeb.unify;

import java.util.regex.Pattern;

import com.isa.thinair.gdsservices.api.util.TypeAConstants;

public class UnifyTypeBMessageAmadeusImpl implements UnifyTypeBMessage {
	public String unifyMessage(String rawMessage) {
		String unifiedMessage = rawMessage;

		if (unifiedMessage.endsWith(";")) {
			unifiedMessage = unifiedMessage.substring(0, unifiedMessage.length() - 1);
		}

//		unifiedMessage = unifiedMessage.replaceAll(Pattern.quote(TypeAConstants.NEW_LINE_DESIGNATOR_TYPE_B), "\n");
		unifiedMessage = unifiedMessage.trim();

		return unifiedMessage;

	}
}
