package com.isa.thinair.gdsservices.core.typeA.helpers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.gdsservices.api.util.TypeAConstants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class XmlDataConverter {

	public static final String XML_DATE_TIME_FORMAT = "yyyy-MM-dd\'T\'HH:mm:ss";
	private static Log log = LogFactory.getLog(XmlDataConverter.class);

	public static String toXmlDateTime(String date, String dateFormat, String time, String timeFormat) {
		String xmlDateTime = null;

		Date parsedDate = toDateTime(date, dateFormat, time, timeFormat);
		DateFormat format = new SimpleDateFormat(XML_DATE_TIME_FORMAT);
		xmlDateTime = format.format(parsedDate);

		return xmlDateTime;
	}

	public static Date toDateTime(String date, String dateFormat, String time, String timeFormat) {
		Date parsedDate = null;

		try {
			DateFormat format = new SimpleDateFormat(dateFormat + " " + timeFormat);
			parsedDate = format.parse(date + " " + time);
		} catch (ParseException e) {
			log.error("Can't parse date ---- " + date + time + " of format " + dateFormat + timeFormat);
		}

		return parsedDate;
	}

	public static String formatDate(String date, String format) {
		String formattedDate = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat(XML_DATE_TIME_FORMAT);
			Date parsedDate = dateFormat.parse(date);

			dateFormat = new SimpleDateFormat(format);
			formattedDate = dateFormat.format(parsedDate);

		} catch (ParseException e) {
			log.error("Can't parse date ---- " + date + " of " + XML_DATE_TIME_FORMAT + " to " + format);
		}

		return formattedDate;
	}

	public static String buildElementName(String parent, String elementIndex) {
		return (parent + String.format("%2s", elementIndex).replaceAll(" ", "0"));
	}

	public static String getGivenName(String text) {
		String givenName;
		String title = getTitle(text);

		if (title != null) {
			givenName = text.substring(0, text.length() - title.length());
		} else {
			givenName = text;
		}

		return givenName.trim();
	}

	public static String getTitle(String text) {
		String title = null;
		TypeAConstants.PaxTitle[] paxTitles = TypeAConstants.PaxTitle.values();

		for (TypeAConstants.PaxTitle paxTitle : paxTitles) {
			if (text.toUpperCase().endsWith(paxTitle.name())) {
				title = text.substring(text.length() - paxTitle.name().length());
			}
		}

		return title;
	}

}
