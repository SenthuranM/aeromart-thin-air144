package com.isa.thinair.gdsservices.core.command;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.dto.internal.ExtendTimelimitRequest;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;

/**
 * 
 * @author Manoj Dhanushka
 * 
 */
public class ExtendTimelimitAction {

	private static Log log = LogFactory.getLog(ExtendTimelimitAction.class);

	public static ExtendTimelimitRequest processRequest(ExtendTimelimitRequest extendTimelimitRequest) {

		try {
			Reservation reservation = CommonUtil.loadReservation(extendTimelimitRequest.getGdsCredentialsDTO(), extendTimelimitRequest
					.getOriginatorRecordLocator().getPnrReference(), null);

			CommonUtil.validateReservation(extendTimelimitRequest.getGdsCredentialsDTO(), reservation);
			
			ReservationPax reservationPax = null;
			Date releaseTimeStamp = null;
			Date newReleaseTimeStamp = null;
			long lDiff = 0;

			Iterator itReservationPax = reservation.getPassengers().iterator();
			while (itReservationPax.hasNext()) {
				reservationPax = (ReservationPax) itReservationPax.next();
				releaseTimeStamp = reservationPax.getZuluReleaseTimeStamp();
				if (releaseTimeStamp != null) {
					break;
				}
			}

			Calendar cal = new GregorianCalendar();
			cal.setTime(extendTimelimitRequest.getTimeLimit());
			newReleaseTimeStamp = cal.getTime();

			if (newReleaseTimeStamp.before(CalendarUtil.getCurrentSystemTimeInZulu())) {// Release time cannot be a past
																						// time
				throw new ModuleException("Reservation can not extend onhold for expired segment(s)");
			}

			Collection colFlightSegmentDTO = WebplatformUtil.getConfirmedDepartureSegments(reservation.getSegmentsView());
			Date maxAllowedReleaseTimeZulu = WebplatformUtil.getMaxAllowedReleaseTimeZulu(colFlightSegmentDTO);

			if (maxAllowedReleaseTimeZulu == null) {
				throw new ModuleException("Reservation can not extend onhold for expired segment(s)");
			}

			if (newReleaseTimeStamp.after(maxAllowedReleaseTimeZulu)) {
				newReleaseTimeStamp = maxAllowedReleaseTimeZulu;// Release time is allowed up to earliest of flight
																// closure and interline cut-over
			}

			try {
				lDiff = BeanUtils.getIdealReleaseDate(newReleaseTimeStamp, releaseTimeStamp);
			} catch (ParseException e) {
				throw new ModuleException("Reservation is not onhold or the extend onhold timestamp is invalid");
			}

			int intDiff = Math.round(lDiff / (60 * 1000));

			ServiceResponce serviceResponce = GDSServicesModuleUtil.getReservationBD().extendOnholdReservation(
					reservation.getPnr(), intDiff, reservation.getVersion(),
					CommonUtil.getTrackingInfo(extendTimelimitRequest.getGdsCredentialsDTO()));

			if (serviceResponce != null && serviceResponce.isSuccess()) {
				extendTimelimitRequest.setSuccess(true);
			} else {
				extendTimelimitRequest.setSuccess(false);
				extendTimelimitRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
				extendTimelimitRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
			}
		} catch (ModuleException e) {
			log.error(" ExtendTimelimitAction Failed for GDS PNR "
					+ extendTimelimitRequest.getOriginatorRecordLocator().getPnrReference(), e);
			extendTimelimitRequest.setSuccess(false);
			extendTimelimitRequest.setResponseMessage(e.getMessageString());
			extendTimelimitRequest.addErrorCode(e.getExceptionCode());
		} catch (Exception e) {
			log.error(" ExtendTimelimitAction Failed for GDS PNR "
					+ extendTimelimitRequest.getOriginatorRecordLocator().getPnrReference(), e);
			extendTimelimitRequest.setSuccess(false);
			extendTimelimitRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
			extendTimelimitRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
		}

		return extendTimelimitRequest;
	}
}
