package com.isa.thinair.gdsservices.core.persistence.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.model.EticketInfoTO;

/**
 * JDBC supprt serives for GDS Type A messaging.
 * 
 */
public interface GDSTypeAServicesSupportJDBCDAO {

	/**
	 * Retrieves the eticket information for a given collection of PPFS ID s (PNR_PAX_FARE_SEGMENT ids)
	 * 
	 * @param ppfsIds
	 *            : The list of PPFS_IDs to retrieve the eticket information.
	 * @return
	 * @throws ModuleException
	 */
	public Collection<EticketInfoTO> getEticketListForPPFSIds(Collection<Integer> ppfsIds, Integer gdsChannelCode)
			throws ModuleException;


	public List<LCCClientReservation> loadReservations(List<Integer> ppfsIds) throws ModuleException;

	public List<LCCClientReservation> loadReservations(Map<String, List<Integer>> ticketsAndCoupons) throws ModuleException;
}
