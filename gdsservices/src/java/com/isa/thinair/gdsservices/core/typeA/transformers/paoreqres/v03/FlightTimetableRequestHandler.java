package com.isa.thinair.gdsservices.core.typeA.transformers.paoreqres.v03;

import static com.isa.thinair.gdsservices.core.util.TypeADataConvertorUtill.getValue;
import static com.isa.thinair.gdsservices.core.util.TypeADataConvertorUtill.setValue;
import iata.typea.v031.paoreq.APD;
import iata.typea.v031.paoreq.CNX;
import iata.typea.v031.paoreq.GROUP3;
import iata.typea.v031.paoreq.GROUP4;
import iata.typea.v031.paoreq.PAOREQ;
import iata.typea.v031.paoreq.PDI;
import iata.typea.v031.paoreq.TVL;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.core.util.TypeADataConvertorUtill;

public class FlightTimetableRequestHandler {

	public static Map<String, Object> getOwnMessageParams(PAOREQ paoreq) {

		Map<String, Object> params = new HashMap<String, Object>();
		List<AvailableFlightSearchDTO> aviFlightSearchDTOList = new ArrayList<AvailableFlightSearchDTO>();

		// PDI begin
		PDI pdiL0 = paoreq.getPDI();
		String defaultCabinClass = setValue(
				(String) getValue(pdiL0, "getPDI02ProductDetails().get(0).getPDI0201CharacteristicIdentification()"), "Y");
		// PDI end.

		List<GROUP3> group3List = paoreq.getGROUP3();
		for (GROUP3 group3 : group3List) {
			// ODI is ignored at the moment.
			PDI pdiL3 = group3.getPDI();
			String cabinClassCode = setValue(
					(String) getValue(pdiL3, "getPDI02ProductDetails().get(0).getPDI0201CharacteristicIdentification()"),
					defaultCabinClass);

			List<GROUP4> group4List = group3.getGROUP4();
			if (group4List != null) {
				for (GROUP4 group4 : group4List) {
					AvailableFlightSearchDTO aviFlightSearchDTO = new AvailableFlightSearchDTO();

					// cabinClass code inherited form the level0
					aviFlightSearchDTO.getCabinClassSelection().put(cabinClassCode, null);

					// TVL begin.
					TVL tvl = group4.getTVL();
					aviFlightSearchDTO.setFromAirport(tvl.getTVL02().getTVL0201());
					aviFlightSearchDTO.setToAirport(tvl.getTVL03().getTVL0301());
					// TVL ends.

					// APD begin
					APD apd = group4.getAPD();
					String dateTimePerioud = apd.getAPD01().getAPD0106();
					Date startDate = TypeADataConvertorUtill.getDate(dateTimePerioud.substring(0, 6));
					Date endDate = TypeADataConvertorUtill.getDate(dateTimePerioud.substring(6));
					aviFlightSearchDTO.setSelectedDepatureDateTimeStart(startDate);
					aviFlightSearchDTO.setSelectedDepatureDateTimeEnd(endDate);
					aviFlightSearchDTO.setDepatureDateTimeStart(startDate);
					aviFlightSearchDTO.setDepatureDateTimeEnd(endDate);
					// APD end

					aviFlightSearchDTO.setReturnFlag(false);
					aviFlightSearchDTO.setAdultCount(1);
					aviFlightSearchDTO.setChildCount(0);
					aviFlightSearchDTO.setInfantCount(0);
					// TODO create a separate agent for GDS
					aviFlightSearchDTO.setPosAirport("SHJ");
					aviFlightSearchDTO.setAgentCode("SHJ001");
					aviFlightSearchDTO.setChannelCode(SalesChannelsUtil
							.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));

					// this will not change.
					aviFlightSearchDTO.setBookingType(BookingClass.BookingClassType.NORMAL);
					aviFlightSearchDTO.setAvailabilityRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED);

					// CNX begin
					CNX cnx = group4.getCNX();
					if (cnx != null) {
						aviFlightSearchDTO.setFlightsPerOndRestriction(AvailableFlightSearchDTO.CONNECTED_FLIGHTS_ONLY);
					} else {
						aviFlightSearchDTO.setFlightsPerOndRestriction(AvailableFlightSearchDTO.ALL_SINGLE_CONNECTED_FLIGHTS);
					}
					// CNX end
					aviFlightSearchDTOList.add(aviFlightSearchDTO);
				}
			}
		}

		params.put(TypeACommandParamNames.TVL_DTOS, aviFlightSearchDTOList);
		return params;
	}

}
