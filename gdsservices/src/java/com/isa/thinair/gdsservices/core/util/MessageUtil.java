package com.isa.thinair.gdsservices.core.util;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;

public class MessageUtil {

	private static final String DEFAULT_ERROR_CODE = "gdsservices.framework.default.error";
	private static String defaultErrorMeesage;

	static {
		init();
	}

	private static void init() {
		ModuleException me = new ModuleException(DEFAULT_ERROR_CODE);
		defaultErrorMeesage = me.getMessageString();
	}

	public static String getDefaultErrorMessage() {
		return defaultErrorMeesage;
	}

	public static String getDefaultErrorCode() {
		return DEFAULT_ERROR_CODE;
	}

	public static String getMessage(String messageCode) {
		return GDSApiUtils.maskNull(new ModuleException(messageCode).getMessageString());
	}
}
