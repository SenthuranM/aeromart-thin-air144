/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */

package com.isa.thinair.gdsservices.core.persistence.dao;

import java.util.Collection;
import java.util.Date;

import com.isa.thinair.gdsservices.api.model.GDSEdiMessages;
import com.isa.thinair.gdsservices.api.model.GDSMessage;
import com.isa.thinair.gdsservices.api.model.GDSPublishMessage;
import com.isa.thinair.gdsservices.api.model.GDSSchedulePublishStatus;
import com.isa.thinair.gdsservices.api.model.GdsReservationImage;

/**
 * @author Nilindra
 */
public interface GDSServicesDAO {
	public void saveOrUpdate(Collection colGDSMessage);

	public GDSMessage getGDSMessage(long messageId);

	public GDSMessage getGDSMessage(String uniqueAccelAeroRequestId);

	public void saveGDSPublisingMessage(GDSPublishMessage publishMessage);

	public GDSPublishMessage getGDSPublisingMessage(int id);

	public Date getLastGDSSchedulePublishTime();

	public void saveGDSSchedulePublishStatus(GDSSchedulePublishStatus schedulePublishStatus);

	public void saveGdsEdiMessage(GDSEdiMessages message);

	public GdsReservationImage getGDSReservationImage(String pnr, String imageContext);
	}
