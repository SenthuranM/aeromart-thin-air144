package com.isa.thinair.gdsservices.core.typeb.transformers;

import java.util.Map;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.SplitReservationRequest;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ProcessStatus;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;

public class SplitReservationResponseTransformer {

	/**
	 * Transforms reservationResponseMap back to bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param reservationResponseMap
	 * @return
	 */
	public static BookingRequestDTO getResponse(BookingRequestDTO bookingRequestDTO,
			Map<ReservationAction, GDSReservationRequestBase> reservationResponseMap) {

		SplitReservationRequest splitReservationRequest = (SplitReservationRequest) reservationResponseMap
				.get(ReservationAction.SPLIT_RESERVATION);

		if (splitReservationRequest.isSuccess()) {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_SUCCESS);
			bookingRequestDTO = ModifyReservationResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
			bookingRequestDTO.setResponseMessageIdentifier(GDSExternalCodes.MessageIdentifier.LINK_RECORD_LOCATOR.getCode());
		} else {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_FAILURE);
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, splitReservationRequest);
		}

		return bookingRequestDTO;
	}
	
	public static BookingRequestDTO setPnrReference(BookingRequestDTO bookingRequestDTO,
			GDSReservationRequestBase gdsReservationRequestBase) {
		if (gdsReservationRequestBase.getResponderRecordLocator() != null
				&& gdsReservationRequestBase.getResponderRecordLocator().getPnrReference() != null) {
			bookingRequestDTO.getResponderRecordLocator().setPnrReference(
					gdsReservationRequestBase.getResponderRecordLocator().getPnrReference());
		}
		bookingRequestDTO.getErrors().addAll(gdsReservationRequestBase.getErrorCode());

		return bookingRequestDTO;
	}
}
