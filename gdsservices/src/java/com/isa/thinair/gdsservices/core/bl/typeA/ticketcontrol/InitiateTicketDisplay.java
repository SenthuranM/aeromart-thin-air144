package com.isa.thinair.gdsservices.core.bl.typeA.ticketcontrol;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.airproxy.api.dto.GDSChargeAdjustmentRQ;
import com.isa.thinair.airproxy.api.dto.GDSPaxChargesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketDisplayRq;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketDisplayRs;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketsAssembler;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageInitiator;
import com.isa.thinair.gdsservices.core.dto.EDIMessageDTO;
import com.isa.thinair.gdsservices.core.dto.EDIMessageMetaDataDTO;
import com.isa.thinair.gdsservices.core.dto.FinancialInformationAssembler;
import com.isa.thinair.gdsservices.core.dto.temp.GdsReservation;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerTicketControlInformationWrapper;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.gdsservices.core.util.GdsDtoTransformer;
import com.isa.thinair.gdsservices.core.util.GdsInfoDaoHelper;
import com.isa.thinair.gdsservices.core.util.GdsUtil;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.typea.common.Header;
import com.isa.typea.common.Location;
import com.isa.typea.common.MessageFunction;
import com.isa.typea.common.MessageHeader;
import com.isa.typea.common.OriginatorInformation;
import com.isa.typea.common.SystemDetails;
import com.isa.typea.common.TicketControlTicketNumberDetails;
import com.isa.typea.common.TravellerTicketControlInformation;
import com.isa.typea.tkcreq.AATKCREQ;
import com.isa.typea.tkcreq.TKCREQMessage;
import com.isa.typea.tkcres.AATKCRES;
import com.isa.typea.tkcres.TKCRESMessage;

public class InitiateTicketDisplay extends EdiMessageInitiator {

	private TicketDisplayRq ticketDisplayRq;
	private boolean isFareBackdownAvailable;
	
	protected void prepareMessages() throws ModuleException {
		EDIMessageDTO ediMessageDTO ;

		ticketDisplayRq = (TicketDisplayRq) getParameter(TypeACommandParamNames.TICKET_CONTROL_DTO);
		Map<Integer, Map<String, List<Integer>>> paxTicketsCoupons = ticketDisplayRq.getPaxTicketsCoupons();
		Map<String, List<Integer>> ticketsCoupons;

		for (Integer paxSeq : paxTicketsCoupons.keySet()) {
			ticketsCoupons = paxTicketsCoupons.get(paxSeq);

			for (String ticketNumber : ticketsCoupons.keySet()) {
				
				OriginatorInformation originatorInformation = GDSDTOUtil.createOriginatorInformation();
				ediMessageDTO = new EDIMessageDTO(TypeAConstants.MessageFlow.INITIATE);

				MessageHeader messageHeader = GDSDTOUtil.createInitMessageHeader(IATAOperation.TKCREQ, ticketDisplayRq.getGdsId(), null);
				Header header = GDSDTOUtil.createInitEdiHeader(ticketDisplayRq.getGdsId(), messageHeader, null);

				AATKCREQ tktReq = new AATKCREQ();

				tktReq.setHeader(header);

				TKCREQMessage tkcreqMessage = new TKCREQMessage();

				tkcreqMessage.setMessageHeader(messageHeader);
				tktReq.setMessage(tkcreqMessage);
				MessageFunction messageFunction = new MessageFunction();
				messageFunction.setMessageFunction(TypeAConstants.MessageFunction.DISPLAY);
				tkcreqMessage.setMessageFunction(messageFunction);
				
				Location location = new Location();
				location.setLocationCode(AppSysParamsUtil.getHubAirport());
				
				SystemDetails systemDetails = new SystemDetails();
				systemDetails.setCompanyCode(AppSysParamsUtil.getDefaultCarrierCode());
				systemDetails.setLocation(location);
				originatorInformation.setSenderSystemDetails(systemDetails);
				originatorInformation.setAgentCurrencyCode(AppSysParamsUtil.getBaseCurrency());
				
				//system ?
				Gds gds = GdsUtil.getGds(ticketDisplayRq.getGdsId());
				originatorInformation.setOriginatorAuthorityCode("SYSTEM");
			
				originatorInformation.setAgentId(AppSysParamsUtil.getGdsConnectivityAgent(gds.getGdsCode()));
				
				tkcreqMessage.setOriginatorInformation(originatorInformation);
				

				List<TicketControlTicketNumberDetails> tickets = tkcreqMessage.getTickets();
				TicketControlTicketNumberDetails ticketNumberDetails = new TicketControlTicketNumberDetails();
				ticketNumberDetails.setTicketNumber(ticketNumber);
				ticketNumberDetails.setDocumentType(TypeAConstants.DocumentType.TICKET);
				tickets.add(ticketNumberDetails);

				ediMessageDTO.setRequest(tktReq);

				EDIMessageMetaDataDTO metaDataDTO = new EDIMessageMetaDataDTO();
				metaDataDTO.setIataOperation(iataOperation);
				metaDataDTO.setMajorVersion(messageHeader.getMessageId().getVersion());
				metaDataDTO.setMinorVersion(messageHeader.getMessageId().getReleaseNumber());
				metaDataDTO.setMessageReference(messageHeader.getCommonAccessReference());
				ediMessageDTO.setRequestMetaDataDTO(metaDataDTO);

				ediMessageDTO.setReqAssociationCode(header.getInterchangeHeader().getAssociationCode());

				ediMessageDTO.setGdsCode(gds.getGdsCode());

				ediMessageDTOs.add(ediMessageDTO);
			}
		}
	}


	// TODO -- test method -- delete this
	/*
	protected void processResponse() throws ModuleException {

		LCCClientReservation reservation = GDSServicesUtil.loadLccReservation(ticketDisplayRq.getPnr());

		Map<Integer, GDSPaxChargesTO> paxAdjustment = new HashMap<Integer, GDSPaxChargesTO>();
		GDSPaxChargesTO paxChargesTO;

		TicketsAssembler ticketsAssembler = new TicketsAssembler(TicketsAssembler.Mode.RECONCILE);
		ticketsAssembler.setPnr(ticketDisplayRq.getPnr());

		for (LCCClientReservationPax passenger : reservation.getPassengers()) {
			paxChargesTO = new GDSPaxChargesTO();
			paxChargesTO.setPnrPaxId(PaxTypeUtils.getPnrPaxId(passenger.getTravelerRefNumber()));
			paxChargesTO.setPaxType(passenger.getPaxType());

			paxChargesTO.setFareAmount(new BigDecimal(400));
			paxChargesTO.setSurChargeAmount(new BigDecimal(400));
			paxChargesTO.setTaxAmount(new BigDecimal(200));

			paxAdjustment.put(PaxTypeUtils.getPnrPaxId(passenger.getTravelerRefNumber()), paxChargesTO);


			TicketsAssembler.PassengerAssembler passengerAssembler = new TicketsAssembler.PassengerAssembler();
			passengerAssembler.setPnrPaxId(PaxTypeUtils.getPnrPaxId(passenger.getTravelerRefNumber()));
			passengerAssembler.setCurrencyCode("AED");
			passengerAssembler.setTotalAmount(new BigDecimal(1000));
			ticketsAssembler.addPassengerAssembler(passengerAssembler);

		}


		GDSChargeAdjustmentRQ gdsChargeAdjustmentRQ = new GDSChargeAdjustmentRQ();
		gdsChargeAdjustmentRQ.setPnr(reservation.getPNR());
		gdsChargeAdjustmentRQ.setGroupPNR(reservation.isGroupPNR());
		gdsChargeAdjustmentRQ.setVersion(reservation.getVersion());
		gdsChargeAdjustmentRQ.setGdsChargesByPax(paxAdjustment);

		TicketDisplayRs ticketDisplayRs = new TicketDisplayRs();
		ticketDisplayRs.setGdsChargeAdjustmentRq(gdsChargeAdjustmentRQ);
		ticketDisplayRs.setTicketsAssembler(ticketsAssembler);
		ticketDisplayRs.setSuccess(true);

		respData.put(TypeACommandParamNames.TICKET_DTO, ticketDisplayRs);
	}
    */

	protected void processResponse() throws ModuleException {

		TicketDisplayRs ticketDisplayRs = new TicketDisplayRs();
		AATKCRES aatktres;
		TKCRESMessage tktresMessage;
		Map<Integer, Map<String, List<Integer>>> paxTicketsCoupons = ticketDisplayRq.getPaxTicketsCoupons();
		LCCClientReservationPax pax;
		boolean invocationError = true;

		Map<Integer, Set<String>> processedTickets = new HashMap<Integer, Set<String>>();
		Map<Integer, List<TravellerTicketControlInformation>> travellers = new HashMap<Integer, List<TravellerTicketControlInformation>>();

		TravellerTicketControlInformationWrapper wrapper;
		List<TravellerTicketControlInformationWrapper> travellerWrappers;

		LCCClientReservation reservation = GDSServicesUtil.loadLccReservation(ticketDisplayRq.getPnr());

		boolean newRecord;
		for (EDIMessageDTO ediMessageDTO : ediMessageDTOs) {

			if (!ediMessageDTO.isInvocationError()) {

				aatktres = (AATKCRES) ediMessageDTO.getResponse();
				tktresMessage = aatktres.getMessage();

				for (TravellerTicketControlInformation traveller : tktresMessage.getTravellers()) {
					pax = GDSDTOUtil.resolveReservationPax(traveller, traveller.getTravellers().get(0), reservation);
					newRecord = false;

					if (!processedTickets.containsKey(pax.getPaxSequence())) {
						processedTickets.put(pax.getPaxSequence(), new HashSet<String>());
						travellers.put(pax.getPaxSequence(), new ArrayList<TravellerTicketControlInformation>());
					}

					for (TicketControlTicketNumberDetails ticket : traveller.getTickets()) {

						if (ticket.getDocumentType().equals(TypeAConstants.DocumentType.TICKET) &&
								!processedTickets.get(pax.getPaxSequence()).contains(ticket.getTicketNumber())) {
							processedTickets.get(pax.getPaxSequence()).add(ticket.getTicketNumber());
							newRecord = true;
						}

					}

					if (newRecord) {
						travellers.get(pax.getPaxSequence()).add(traveller);
					}
				}

				invocationError = false;
			} else {
				invocationError = true;
				break;
			}
		}

		if (!invocationError) {
			// save image
			GdsReservation<TravellerTicketControlInformationWrapper> gdsReservation =
					GdsInfoDaoHelper.getGdsReservationTicketControlView(ticketDisplayRq.getPnr());

			travellerWrappers = gdsReservation.getTravellerInformation();

			for (Integer paxSeq : travellers.keySet()) {
				for (TravellerTicketControlInformation traveller : travellers.get(paxSeq)) {
					wrapper = new TravellerTicketControlInformationWrapper(traveller);
					travellerWrappers.add(wrapper);
				}
			}
			GdsInfoDaoHelper.saveGdsReservationTicketControlView(ticketDisplayRq.getPnr(), gdsReservation);


			// reconcile financial details
			if (ticketDisplayRq.isCalculateFinancialInfo()) {
				Map<Integer, GDSPaxChargesTO> paxAdjustmentAgainstPaxSeq = new HashMap<Integer, GDSPaxChargesTO>();
				GDSPaxChargesTO paxChargesTO;

				TicketsAssembler ticketsAssembler = new TicketsAssembler(TicketsAssembler.Mode.RECONCILE);
				ticketsAssembler.setPnr(ticketDisplayRq.getPnr());
				ticketsAssembler.setIpAddress("127.0.0.1"); // triggered by system itself


				for (LCCClientReservationPax passenger : reservation.getPassengers()) {
					paxChargesTO = new GDSPaxChargesTO();
					paxChargesTO.setPnrPaxId(PaxTypeUtils.getPnrPaxId(passenger.getTravelerRefNumber()));
					paxChargesTO.setPaxType(passenger.getPaxType());

					paxChargesTO.setFareAmount(BigDecimal.ZERO);
					paxChargesTO.setSurChargeAmount(BigDecimal.ZERO);
					paxChargesTO.setTaxAmount(BigDecimal.ZERO);

					paxAdjustmentAgainstPaxSeq.put(passenger.getPaxSequence(), paxChargesTO);
				}

				FinancialInformationAssembler financialInformationAssembler;
				try {
					for (Integer paxSeq : travellers.keySet()) {
						for (TravellerTicketControlInformation traveller : travellers.get(paxSeq)) {

							financialInformationAssembler = new FinancialInformationAssembler();
							financialInformationAssembler.setFinancialInformation(traveller.getText());
							financialInformationAssembler.setTaxDetails(traveller.getTaxDetails());
							financialInformationAssembler.setMonetaryInformation(traveller.getMonetaryInformation());
							isFareBackdownAvailable = financialInformationAssembler.isFareBreakdownInTKCUACAvailable();
							if(isFareBackdownAvailable){
								financialInformationAssembler.calculateAmounts();
							}
			
							
							
							if (isFareBackdownAvailable) {
								paxChargesTO = paxAdjustmentAgainstPaxSeq.get(paxSeq);
								paxChargesTO.setFareAmount(paxChargesTO.getFareAmount().add(financialInformationAssembler.getFareAmount()));
								paxChargesTO.setTaxAmount(paxChargesTO.getTaxAmount().add(financialInformationAssembler.getTaxAmount()));
								paxChargesTO.setSurChargeAmount(paxChargesTO.getSurChargeAmount().add(financialInformationAssembler.getSurcharge()));
							} else {
								paxChargesTO = paxAdjustmentAgainstPaxSeq.get(paxSeq);
								paxChargesTO.setFareAmount(
										new BigDecimal(traveller.getMonetaryInformation().get(0).getAmount()));
							}
							
							pax = GDSDTOUtil.resolveReservationPax(traveller, traveller.getTravellers().get(0), reservation);
							GdsDtoTransformer.appendToTicketsAssembler(ticketsAssembler, traveller, pax);

						}
					}
				} catch (GdsTypeAException e) {
					throw new ModuleException("");
				}

				Map<Integer, GDSPaxChargesTO> paxAdjustmentAgainstPnrPaxId = new HashMap<Integer, GDSPaxChargesTO>();
				for (GDSPaxChargesTO paxCharges : paxAdjustmentAgainstPaxSeq.values()) {
					paxAdjustmentAgainstPnrPaxId.put(paxCharges.getPnrPaxId(), paxCharges);
				}


				GDSChargeAdjustmentRQ gdsChargeAdjustmentRQ = new GDSChargeAdjustmentRQ();
				gdsChargeAdjustmentRQ.setPnr(reservation.getPNR());
				gdsChargeAdjustmentRQ.setGroupPNR(reservation.isGroupPNR());
				gdsChargeAdjustmentRQ.setVersion(reservation.getVersion());
				gdsChargeAdjustmentRQ.setGdsChargesByPax(paxAdjustmentAgainstPnrPaxId);

				ticketDisplayRs.setGdsChargeAdjustmentRq(gdsChargeAdjustmentRQ);
				ticketDisplayRs.setTicketsAssembler(ticketsAssembler);
			}

			ticketDisplayRs.setSuccess(true);
		} else {
			ticketDisplayRs.setSuccess(false);
		}

		respData.put(TypeACommandParamNames.TICKET_DTO, ticketDisplayRs);

	}

}
