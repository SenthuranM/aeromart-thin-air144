package com.isa.thinair.gdsservices.core.typeA.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class PaxETicketSegment implements Serializable {

	private static final long serialVersionUID = -8068971231638156823L;

	private int segmentSeq;

	private int coupunSeq;

	private String eticket;

	private String from;

	private String to;

	private Date departureDate;

	private String carrierCode;

	private String flightNumber;

	private String rbd;

	private String status;

	private BigDecimal fareAmount;

	private int fltSegmentId;

	public PaxETicketSegment(int segmentSeq, int eticketSeq, String from, String to, Date departureDate, String carrierCode,
			String flightNumber, String rbd, String status) {
		super();
		this.segmentSeq = segmentSeq;
		this.coupunSeq = eticketSeq;
		this.from = from;
		this.to = to;
		this.departureDate = departureDate;
		this.carrierCode = carrierCode;
		this.flightNumber = flightNumber;
		this.rbd = rbd;
		this.status = status;
	}

	/**
	 * @return the segmentSeq
	 */
	public int getSegmentSeq() {
		return segmentSeq;
	}

	/**
	 * @return the eticketSeq
	 */
	public int getCoupunSeq() {
		return coupunSeq;
	}

	/**
	 * @param eticketSeq
	 *            the eticketSeq to set
	 */
	public void setCoupunSeq(int eticketSeq) {
		this.coupunSeq = eticketSeq;
	}

	/**
	 * @return the eticket
	 */
	public String getEticket() {
		return eticket;
	}

	/**
	 * @param eticket
	 *            the eticket to set
	 */
	public void setEticket(String eticket) {
		this.eticket = eticket;
	}

	/**
	 * @return the departureDate
	 */
	public Date getDepartureDate() {
		return departureDate;
	}

	/**
	 * @param departureDate
	 *            the departureDate to set
	 */
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * @param from
	 *            the from to set
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public String getTo() {
		return to;
	}

	/**
	 * @param to
	 *            the to to set
	 */
	public void setTo(String to) {
		this.to = to;
	}

	public String getSegmentCode() {
		return from + "/" + to;
	}

	/**
	 * @return the carrierCode
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            the carrierCode to set
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getFullFlightNumber() {
		return getCarrierCode() + getFlightNumber();
	}

	/**
	 * @return the fltSegmentId
	 */
	public int getFltSegmentId() {
		return fltSegmentId;
	}

	/**
	 * @param fltSegmentId
	 *            the fltSegmentId to set
	 */
	public void setFltSegmentId(int fltSegmentId) {
		this.fltSegmentId = fltSegmentId;
	}

	/**
	 * @return the rbd
	 */
	public String getRbd() {
		return rbd;
	}

	/**
	 * @param rbd
	 *            the rbd to set
	 */
	public void setRbd(String rbd) {
		this.rbd = rbd;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the fareAmount
	 */
	public BigDecimal getFareAmount() {
		return fareAmount;
	}

	/**
	 * @param fareAmount
	 *            the fareAmount to set
	 */
	public void setFareAmount(BigDecimal fareAmount) {
		this.fareAmount = fareAmount;
	}

	/**
	 * Return true if the segment represen a external segment
	 * 
	 * @return
	 */
	public boolean isExternalSegment() {
		String defaultCarrierCode = AppSysParamsUtil.getDefaultCarrierCode();
		return !(defaultCarrierCode.equals(carrierCode));
	}

}
