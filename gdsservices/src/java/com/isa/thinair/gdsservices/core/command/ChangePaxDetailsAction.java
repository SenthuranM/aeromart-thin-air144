package com.isa.thinair.gdsservices.core.command;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.internal.ChangePaxDetailsRequest;
import com.isa.thinair.gdsservices.api.dto.internal.OtherServiceInfo;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.internal.SSRTO;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.platform.api.ServiceResponce;

public class ChangePaxDetailsAction {

	private static Log log = LogFactory.getLog(ChangePaxDetailsAction.class);

	public static ChangePaxDetailsRequest processRequest(ChangePaxDetailsRequest changePaxDetailsRequest) {
		try {
			Reservation reservation = CommonUtil.loadReservation(changePaxDetailsRequest.getGdsCredentialsDTO(),
					changePaxDetailsRequest.getOriginatorRecordLocator().getPnrReference(), null);

			CommonUtil.validateReservation(changePaxDetailsRequest.getGdsCredentialsDTO(), reservation);
			Map<Passenger, Passenger> oldNewPassengerDetails = changePaxDetailsRequest.getOldNewPassengerDetails();
			Map<String, Passenger> oldNameNewPassengerMap = getNamesMap(oldNewPassengerDetails);

			Set<OtherServiceInfo> reservationOSI = new HashSet<OtherServiceInfo>();
			Collection<ReservationPax> colReservationPax = reservation.getPassengers();
			Passenger passenger;
			String posInfor = CommonUtil.getPOSInformation(changePaxDetailsRequest);

			for (Iterator<ReservationPax> iter = colReservationPax.iterator(); iter.hasNext();) {
				ReservationPax resPax = iter.next();
				String oldName = GDSApiUtils.getIATAName(resPax.getLastName(), resPax.getFirstName(), resPax.getTitle());
				passenger = oldNameNewPassengerMap.get(oldName);

				if (passenger != null) {
					if (passenger.getTitle() != null) {
						resPax.setTitle(passenger.getTitle());
					}
					resPax.setFirstName(passenger.getFirstName());
					resPax.setLastName(passenger.getLastName());
					if (passenger.getDocNumber() != null && passenger.getDocCountryCode() != null
							&& passenger.getDocExpiryDate() != null) {
						resPax.getPaxAdditionalInfo().setPassportNo(passenger.getDocNumber());
						resPax.getPaxAdditionalInfo().setPassportIssuedCntry(passenger.getDocCountryCode());
						resPax.getPaxAdditionalInfo().setPassportExpiry(passenger.getDocExpiryDate());
					}
					
					if (passenger.getTravelDocType() != null && passenger.getVisaApplicableCountry() != null &&passenger.getVisaDocIssueDate() != null
							&& passenger.getVisaDocNumber() != null && passenger.getVisaDocPlaceOfIssue() != null) {
						resPax.getPaxAdditionalInfo().setTravelDocumentType(passenger.getTravelDocType());
						resPax.getPaxAdditionalInfo().setVisaApplicableCountry(passenger.getVisaApplicableCountry());
						resPax.getPaxAdditionalInfo().setVisaDocIssueDate(passenger.getVisaDocIssueDate());
						resPax.getPaxAdditionalInfo().setVisaDocNumber(passenger.getVisaDocNumber());
						resPax.getPaxAdditionalInfo().setVisaDocPlaceOfIssue(passenger.getVisaDocPlaceOfIssue());
						resPax.getPaxAdditionalInfo().setPlaceOfBirth(passenger.getPlaceOfBirth());
					}

					List<SSRTO> processedSSRs = CommonUtil.getSSRInfo(passenger.getSsrCollection(), null, null);
					// FIXME: MULTI SSR SUPPORT
					// resPax.setSsrCode(processedSSRs[0]);
					// resPax.setSsrRemarks(processedSSRs[1]);

					reservationOSI.addAll(passenger.getOsiCollection());
				}
			}
			//We use same Action class to User note changes which comes in SSR OTHS element
			String userNote = reservation.getUserNote();
			if (!changePaxDetailsRequest.getUserNote().equals("")) {
				userNote = userNote + " [" + changePaxDetailsRequest.getUserNote() + "]";
			} else {
				userNote = CommonUtil.getAsUserNotes(reservationOSI, posInfor);
			}
			reservation.setUserNote(userNote);
			ServiceResponce serviceResponce = GDSServicesModuleUtil.getReservationBD().updateReservation(reservation,
					CommonUtil.getTrackingInfo(changePaxDetailsRequest.getGdsCredentialsDTO()), false, true, null, null);

			if (serviceResponce != null && serviceResponce.isSuccess()) {
				changePaxDetailsRequest.setSuccess(true);
			} else {
				changePaxDetailsRequest.setSuccess(false);
				changePaxDetailsRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
				changePaxDetailsRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
			}
		} catch (ModuleException e) {
			log.error(" ChangePaxDetailsAction Failed for GDS PNR "
					+ changePaxDetailsRequest.getOriginatorRecordLocator().getPnrReference(), e);
			changePaxDetailsRequest.setSuccess(false);
			changePaxDetailsRequest.setResponseMessage(e.getMessageString());
			changePaxDetailsRequest.addErrorCode(e.getExceptionCode());
		} catch (Exception e) {
			log.error(" ChangePaxDetailsAction Failed for GDS PNR "
					+ changePaxDetailsRequest.getOriginatorRecordLocator().getPnrReference(), e);
			changePaxDetailsRequest.setSuccess(false);
			changePaxDetailsRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
			changePaxDetailsRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
		}

		return changePaxDetailsRequest;
	}

	private static Map<String, Passenger> getNamesMap(Map<Passenger, Passenger> oldNewPassengerDetails) {
		Map<String, Passenger> namesMap = new HashMap<String, Passenger>();
		Passenger newPassenger;

		for (Passenger oldPassenger : oldNewPassengerDetails.keySet()) {
			newPassenger = oldNewPassengerDetails.get(oldPassenger);
			namesMap.put(oldPassenger.getIATAName(), newPassenger);
		}

		return namesMap;
	}
}
