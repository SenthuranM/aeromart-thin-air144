package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.internal.CancelReservationRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.dto.internal.UpdateExternalSegmentsRequest;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

public class CancelReservationRequestExtractor extends ValidatorBase {
	
	public static LinkedHashMap<ReservationAction, GDSReservationRequestBase> getMap(GDSCredentialsDTO gdsCredentialsDTO,
			BookingRequestDTO bookingRequestDTO) {

		LinkedHashMap<ReservationAction, GDSReservationRequestBase> requestMap = new LinkedHashMap<ReservationAction, GDSReservationRequestBase>();
		
		CancelReservationRequest cancelReservationRequest = new CancelReservationRequest();
		cancelReservationRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
		cancelReservationRequest = (CancelReservationRequest) RequestExtractorUtil.addBaseAttributes(cancelReservationRequest,
				bookingRequestDTO);
		cancelReservationRequest = addSegments(cancelReservationRequest, bookingRequestDTO);
		requestMap.put(GDSInternalCodes.ReservationAction.CANCEL_RESERVATION, cancelReservationRequest);
		
		RequestExtractorUtil.processExternalSegments(gdsCredentialsDTO, bookingRequestDTO, requestMap);

		return requestMap;
	}
	
	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {

		String pnrReference = ValidatorUtils.getPNRReference(bookingRequestDTO.getResponderRecordLocator());

		if (pnrReference.length() == 0) {
			String message = MessageUtil.getMessage("gdsservices.extractors.emptyPNRResponder");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		} else {
			// There aren't any AddSegmentRequestExtractor specific validations.
			// The General Action codes are validated at the high level.
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
			return bookingRequestDTO;
		}
	}
	
	/**
	 * adds segments witch need to be canceled to the
	 * 
	 * @param CancelReservationRQ
	 * @param bookingRequestDTO
	 * @return
	 */
	private static CancelReservationRequest
			addSegments(CancelReservationRequest cancelReservationRequest, BookingRequestDTO bookingRequestDTO) {

		List<BookingSegmentDTO> segmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();
		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();
		BookingSegmentDTO segmentDTO;
		String gdsActionCode;

		for (Iterator<BookingSegmentDTO> iterator = segmentDTOs.iterator(); iterator.hasNext();) {
			segmentDTO = (BookingSegmentDTO) iterator.next();
			gdsActionCode = segmentDTO.getActionOrStatusCode();

			if (gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL.getCode())
					|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_IF_AVAILABLE.getCode())
					|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_IF_HOLDING.getCode())
					|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_RECOMMENDED.getCode())
					|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CODESHARE_CANCEL.getCode())) {

				Segment segment = RequestExtractorUtil.getSegment(segmentDTO);
				cancelReservationRequest.getSegments().add(segment);
			}

		}

		return cancelReservationRequest;
	}

}
