package com.isa.thinair.gdsservices.core.typeA.transformers.tkcreqres.v03;

import static com.isa.thinair.gdsservices.core.util.IATADataExtractionUtil.setValue;
import iata.typea.v031.tkcreq.CPN;
import iata.typea.v031.tkcreq.EQN;
import iata.typea.v031.tkcreq.GROUP1;
import iata.typea.v031.tkcreq.GROUP2;
import iata.typea.v031.tkcreq.MSG;
import iata.typea.v031.tkcreq.ORG;
import iata.typea.v031.tkcreq.ObjectFactory;
import iata.typea.v031.tkcreq.TKCREQ;
import iata.typea.v031.tkcreq.TKT;
import iata.typea.v031.tkcreq.Type0029;
import iata.typea.v031.tkcreq.Type0051;
import iata.typea.v031.tkcreq.Type0065;
import iata.typea.v031.tkcreq.Type1159;
import iata.typea.v031.tkcreq.Type9972;
import iata.typea.v031.tkcreq.UNB;
import iata.typea.v031.tkcreq.UNH;
import iata.typea.v031.tkcreq.UNT;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.model.CouponInfoTO;
import com.isa.thinair.gdsservices.api.model.EticketInfoTO;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.config.TypeAMessageConfig;
import com.isa.thinair.gdsservices.core.util.IATADataExtractionUtil;
import com.isa.thinair.gdsservices.core.util.RandomANGen;

/**
 * The singleton Tkcreq factory class. When a new tkcreq is needed to be created please use thing class to initialize
 * the object.
 * 
 * @author sanjaya
 * 
 */
public class TkcreqFactory {

	private static final TkcreqFactory factory = new TkcreqFactory();
	private static final ObjectFactory objectFactory = new ObjectFactory();

	private static final String AIR_PROVIDER = "1";

	private TkcreqFactory() {
		super();
	}

	/**
	 * @return The singleton instance of the {@link TkcreqFactory}.
	 */
	public static TkcreqFactory getInstance() {
		return factory;
	}

	/**
	 * @return : The {@link UNB} for the {@link TKCREQ}.
	 * @throws ModuleException
	 */
	public UNB createUNB() throws ModuleException {
		TypeAMessageConfig typeAMessageConfig = GDSServicesModuleUtil.getConfig().getTypeAMessageConfig();
		UNB unb = objectFactory.createUNB();

		// Syntax Information
		setValue(objectFactory, unb, typeAMessageConfig.getSyntaxIdentifier(), 1, 1);
		setValue(objectFactory, unb, typeAMessageConfig.getSyntaxVersion(), 1, 2);

		// sender info
		setValue(objectFactory, unb, typeAMessageConfig.getOwnId(), 2, 1);
		setValue(objectFactory, unb, typeAMessageConfig.getOwnCode(), 2, 3);

		// recipient info
		setValue(objectFactory, unb, typeAMessageConfig.getRemoteId(), 3, 1);
		setValue(objectFactory, unb, typeAMessageConfig.getRemoteCode(), 3, 3);

		// This date and time need to be given in GMT
		// This date and time need to be given in GMT
		String[] dateAsString = IATADataExtractionUtil.getDateAsString(new Date(), true);
		setValue(objectFactory, unb, dateAsString[0], 4, 1);
		setValue(objectFactory, unb, dateAsString[1], 4, 2);

		// interchange controll ref of sender
		String carOwn = new RandomANGen(14).nextString("");
		setValue(objectFactory, unb, carOwn, 5);

		// set association codeunb
		setValue(objectFactory, unb, Type0029.O, 8);
		return unb;
	}

	/**
	 * @return : The {@link UNH} for the {@link TKCREQ}.
	 * @throws ModuleException
	 */
	public UNH createUNH() throws ModuleException {

		TypeAMessageConfig typeAMessageConfig = GDSServicesModuleUtil.getConfig().getTypeAMessageConfig();

		UNH unh = objectFactory.createUNH();

		setValue(objectFactory, unh, String.valueOf(1), 1);
		setValue(objectFactory, unh, Type0065.TKCREQ, 2, 1);
		setValue(objectFactory, unh, typeAMessageConfig.getMessageVersoins(Type0065.TKCREQ.toString()), 2, 2);
		setValue(objectFactory, unh, typeAMessageConfig.getMessageRelease(Type0065.TKCREQ.toString()), 2, 3);
		setValue(objectFactory, unh, Type0051.IA, 2, 4);

		String carOwn = new RandomANGen(14).nextString("");
		setValue(objectFactory, unh, carOwn, 3);

		return unh;
	}

	/**
	 * @return the {@link MSG} object for the {@link TKCREQ} message.
	 * @throws ModuleException
	 */
	public MSG createMSG(String functionCode) throws ModuleException {

		if (StringUtils.isEmpty(functionCode)) {
			throw new ModuleException("gdsservices.framework.empty.argument");
		}

		MSG msg = objectFactory.createMSG();

		setValue(objectFactory, msg, AIR_PROVIDER, 1, 1);
		setValue(objectFactory, msg, functionCode, 1, 2);
		return msg;
	}

	/**
	 * @return the {@link ORG} object for the {@link TKCREQ} message.
	 * @throws ModuleException
	 */
	public ORG createORG() throws ModuleException {
		TypeAMessageConfig typeAMessageConfig = GDSServicesModuleUtil.getConfig().getTypeAMessageConfig();

		ORG org = objectFactory.createORG();

		// Airline/Company identification.
		setValue(objectFactory, org, AppSysParamsUtil.getDefaultCarrierCode(), 1, 1);
		// Location identification.
		setValue(objectFactory, org, typeAMessageConfig.getLocationCode(), 1, 2);
		// Agent identification code.
		setValue(objectFactory, org, typeAMessageConfig.getAgentIdentificationCode(), 2, 1);
		// In house identification code
		setValue(objectFactory, org, typeAMessageConfig.getInHouseIdentificationCode(), 2, 2);
		// Originator type - air line
		setValue(objectFactory, org, Type9972.A, 5);
		// Originator authority request code
		setValue(objectFactory, org, typeAMessageConfig.getOriginatorAuthorityRequestCode(), 7);

		return org;
	}

	/**
	 * @return the {@link EQN} object for the {@link TKCREQ} message.
	 * @throws ModuleException
	 *             :
	 */
	public EQN createEQN(BigDecimal numberOfUnits) throws ModuleException {

		if (numberOfUnits == null) {
			throw new ModuleException("gdsservices.framework.empty.argument");
		}

		TypeAMessageConfig typeAMessageConfig = GDSServicesModuleUtil.getConfig().getTypeAMessageConfig();

		EQN eqn = objectFactory.createEQN();

		setValue(objectFactory, eqn, numberOfUnits, 1, 1);
		setValue(objectFactory, eqn, "TD", 1, 2);
		return eqn;
	}

	/**
	 * @return the {@link GROUP1} object for the {@link TKCREQ} message.
	 * @throws ModuleException
	 */
	public List<GROUP1> createGroup1(Collection<EticketInfoTO> eticketList) throws ModuleException {

		if (eticketList == null || eticketList.size() < 1) {
			throw new ModuleException("gdsservices.framework.empty.argument");
		}

		List<GROUP1> eTicketList = new ArrayList<GROUP1>();

		for (EticketInfoTO eticketInfo : eticketList) {
			GROUP1 group1 = objectFactory.createGROUP1();

			TKT tkt = objectFactory.createTKT();

			TKT.TKT01 tkt01 = objectFactory.createTKTTKT01();

			tkt01.setTKT0101(eticketInfo.getEticketNumber());
			tkt01.setTKT0102("T");
			tkt01.setTKT0103(new BigDecimal(eticketInfo.getNumberOfTickets()));
			tkt.setTKT01(tkt01);

			group1.setTKT(tkt);

			for (CouponInfoTO couponInfo : eticketInfo.getCouponList()) {
				GROUP2 group2 = objectFactory.createGROUP2();

				CPN cpn = objectFactory.createCPN();

				CPN.CPN01 cpn01 = objectFactory.createCPNCPN01();
				cpn01.setCPN0101(String.valueOf(couponInfo.getSegmentSquence()));
				cpn01.setCPN0102(couponInfo.getCouponStatus());
				cpn01.setCPN0104(Type1159.E);

				cpn.setCPN01(cpn01);

				group2.setCPN(cpn);

				group1.getGROUP2().add(group2);

			}

			eTicketList.add(group1);
		}
		return eTicketList;
	}

	/**
	 * @return the {@link UNT} object for the {@link TKCREQ} message.
	 */
	public UNT createUNT() {
		UNT unt = objectFactory.createUNT();
		return unt;
	}

	public ObjectFactory getObjectFactory() {
		return objectFactory;
	}

}
