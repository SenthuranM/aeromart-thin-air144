package com.isa.thinair.gdsservices.core.command;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.util.AvailableFlightSearchDTOBuilder;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchDto;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchRespDto;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.commons.api.dto.CabinClassWiseDefaultLogicalCCDetailDTO;
import com.isa.thinair.commons.api.dto.HubAirportTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.TTYMessageUtil;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * GDS Availability Search Assembler
 * 
 * @author Nilindra Fernando
 * @since 6:05 PM 9/19/2008
 */
public class GDSAASearchAssembler {

	private static Log log = LogFactory.getLog(GDSAASearchAssembler.class);

	/**
	 * Returns the quoted price
	 * 
	 * @param gdsCredentialsDTO
	 * @param segments
	 * @param totalPaxCountArr
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<OndFareDTO> getQuotedPrice(GDSCredentialsDTO gdsCredentialsDTO, Collection<Segment> segments,
			int[] totalPaxCountArr, Integer gdsId) throws ModuleException {

		Collection<OndFareDTO> prices = new ArrayList<OndFareDTO>();

		if (segments.size() > 1) {
			segments = GDSAASearchAssemblerUtil.getSegmentsOrdered(segments);
//			Map<String, Collection<Segment>> bookingClassWiseSegments = GDSAASearchAssemblerUtil
//					.getBookingClassWiseSegments(segments);

			// Checking for Return/Connection/Segment
			if (GDSAASearchAssemblerUtil.is360DegreesTrip(segments)) {
				boolean isAccelAeroReturnBooking = GDSAASearchAssemblerUtil.isAccelAeroReturnBooking(segments);

				// CMB/SHJ and SHJ/CMB case
				if (isAccelAeroReturnBooking) {
					calculatePrices(gdsCredentialsDTO, segments, totalPaxCountArr, true, prices, gdsId);
					// CMB/SHJ/KDM/MCT and MCT/KDM/XXX/SHJ This case is not treated as a AccelAero return booking
					// Even though it is a return booking
				} else {
					Collection[] segmentResults = GDSAASearchAssemblerUtil.getTurningPointSegments(segments);
					Collection<Segment> afterTurningPointSegments = segmentResults[0];
					Collection<Segment> tillTuriningPointSegments = segmentResults[1];

					calculatePrices(gdsCredentialsDTO, tillTuriningPointSegments, totalPaxCountArr, false, prices, gdsId);
					if (!afterTurningPointSegments.isEmpty()) {
						calculatePrices(gdsCredentialsDTO, afterTurningPointSegments, totalPaxCountArr, false, prices, gdsId);
					}
				}
				// Checking for Connection/Segment
			} else {
				calculatePrices(gdsCredentialsDTO, segments, totalPaxCountArr, false, prices, gdsId);
			}
			// One Segment price quotes
		} else {
			calculatePrices(gdsCredentialsDTO, segments, totalPaxCountArr, false, prices, gdsId);
		}

		if (prices.size() == 0) {
			String flightNumbers = "";
			int i = 0;
			for (Segment segment : segments) {
				if (i == 0) {
					flightNumbers += " " + segment.getFlightNumber();
				} else {
					flightNumbers += " , " + segment.getFlightNumber();
				}
				i++;
			}
			throw new ModuleException("airinventory.logic.bl.flights.not.available", flightNumbers);
		}

		return prices;
	}

	/**
	 * Calculate Prices
	 * 
	 * @param gdsCredentialsDTO
	 * @param segments
	 * @param totalPaxCountArr
	 * @param isReturnTrip
	 * @param prices
	 * @throws ModuleException
	 */
	private static void calculatePrices(GDSCredentialsDTO gdsCredentialsDTO, Collection<Segment> segments,
			int[] totalPaxCountArr, boolean isReturnTrip, Collection<OndFareDTO> prices, Integer gdsId) throws ModuleException {
		Collection<OndFareDTO> tmpPrices = GDSAASearchAssembler.getFares(gdsCredentialsDTO, segments, totalPaxCountArr,
				isReturnTrip, gdsId);

		if (tmpPrices == null || tmpPrices.size() == 0) {
			String flightNumbers = "";
			int i = 0;
			for (Segment segment : segments) {
				if (i == 0) {
					flightNumbers += " " + segment.getFlightNumber();
				} else {
					flightNumbers += " , " + segment.getFlightNumber();
				}
				i++;
			}
			throw new ModuleException("airinventory.logic.bl.fares.not.available", flightNumbers);
		} else {
			analyzeSegments(tmpPrices, segments);
			prices.addAll(tmpPrices);
		}
	}

	/**
	 * Analyze Segments
	 * 
	 * @param tmpPrices
	 * @param segments
	 * @throws ModuleException
	 */
	private static void analyzeSegments(Collection<OndFareDTO> tmpPrices, Collection<Segment> segments) throws ModuleException {

		for (Segment segment : segments) {
			boolean matchExists = isSegmentMatchExist(tmpPrices, segment);

			if (!matchExists) {
				throw new ModuleException("gdsservices.actions.idealflight.doesnot.exist");
			}
		}
	}

	/**
	 * Checks whether any segment match exist
	 * 
	 * @param tmpPrices
	 * @param segments
	 * @return
	 */
	private static boolean isSegmentMatchExist(Collection<OndFareDTO> tmpPrices, Segment segment) {

		Iterator itFlightSegmentDTO;
		FlightSegmentDTO flightSegmentDTO;
		Date depatureDateOwnSegment, depatureDateGDSSegment;
		Date arrivalDateOwnSegment, arrivalDateGDSSegment;

		for (OndFareDTO ondFareDTO : tmpPrices) {
			itFlightSegmentDTO = ondFareDTO.getSegmentsMap().values().iterator();

			while (itFlightSegmentDTO.hasNext()) {
				flightSegmentDTO = (FlightSegmentDTO) itFlightSegmentDTO.next();

				if (flightSegmentDTO.getFlightNumber().equals(segment.getFlightNumber())
						&& flightSegmentDTO.getFromAirport().equals(segment.getDepartureStation())
						&& flightSegmentDTO.getToAirport().equals(segment.getArrivalStation())) {

					depatureDateOwnSegment = CalendarUtil.truncateTime(flightSegmentDTO.getDepatureDate());
					depatureDateGDSSegment = CalendarUtil.truncateTime(segment.getDepartureDate());

					if (depatureDateOwnSegment.compareTo(depatureDateGDSSegment) == 0) {
						arrivalDateOwnSegment = CalendarUtil.truncateTime(flightSegmentDTO.getArrivalDateTime());
						arrivalDateGDSSegment = CalendarUtil.truncateTime(segment.getArrivalDate());

						// We have an ideal match. Now we need to see the times are having mismatches or not
						if (arrivalDateOwnSegment.compareTo(arrivalDateGDSSegment) == 0) {
							Date depatureDateTimeOwnSegment = flightSegmentDTO.getDepartureDateTime();
							Date depatureDateTimeGDSSegment = segment.getDepartureDateTime();

							Date arrivalDateTimeOwnSegment = flightSegmentDTO.getArrivalDateTime();
							Date arrivalDateTimeGDSSegment = segment.getArrivalDateTime();

							if (depatureDateTimeOwnSegment.compareTo(depatureDateTimeGDSSegment) == 0
									&& arrivalDateTimeOwnSegment.compareTo(arrivalDateTimeGDSSegment) == 0) {
								segment.setStatusCode(GDSInternalCodes.StatusCode.CONFIRMED);
							} else {
								segment.setStatusCode(GDSInternalCodes.StatusCode.CONFIRMED_SCHEDULE_CHANGED);
							}

							return true;
						}
					}
				}
			}
		}

		return false;
	}

	/**
	 * Get OND fares
	 * 
	 * @param gdsCredentialsDTO
	 * @param segment
	 * @param the
	 *            total passenger counts in an Integer Array : (0)-Adults, (1)-Children, (2)-Infants
	 * @return
	 * @throws ModuleException
	 */
	private static Collection<OndFareDTO> getFares(GDSCredentialsDTO gdsCredentialsDTO, Collection<Segment> segments,
			int[] paxCountArr, boolean isReturnTrip, Integer gdsId) throws ModuleException {

		GlobalConfig globalConfig = GDSServicesModuleUtil.getGlobalConfig();

		// 1. Flight Search ------
		AvailableFlightSearchDTOBuilder availableFlightSearchDTO = new AvailableFlightSearchDTOBuilder();
		availableFlightSearchDTO.setDepartureVariance(0);
		availableFlightSearchDTO.setArrivalVariance(0);

		availableFlightSearchDTO.setAdultCount(paxCountArr[0]);
		availableFlightSearchDTO.setChildCount(paxCountArr[1]);
		availableFlightSearchDTO.setInfantCount(paxCountArr[2]);

		Segment firstSegment = BeanUtils.getFirstElement(segments);
		availableFlightSearchDTO.setFromAirport(firstSegment.getDepartureStation());
		availableFlightSearchDTO.setDepartureDate(firstSegment.getDepartureDateTime());

//		if (isReturnTrip) {
//			Collection<Segment> turningFirstSegments = GDSAASearchAssemblerUtil.getTurningPointSegments(segments)[0];
//			Segment firstTurningSegment = BeanUtils.getFirstElement(turningFirstSegments);
//			availableFlightSearchDTO.setToAirport(firstTurningSegment.getDepartureStation());
//			availableFlightSearchDTO.setReturnDate(CalendarUtil.truncateTime(firstTurningSegment.getDepartureDate()));
//			availableFlightSearchDTO.setReturnFlag(true);
//		} else {
//			Segment lastSegment = (Segment) BeanUtils.getLastElement(segments);
//			availableFlightSearchDTO.setToAirport(lastSegment.getArrivalStation());
//			availableFlightSearchDTO.setReturnFlag(false);
//		}
//

//		Map<String, Collection<Segment>> bookingClassWiseSegments = new HashMap<String, Collection<Segment>>();
//		for (Segment segment : segments) {
//			if (!bookingClassWiseSegments.containsKey(segment.getBookingCode())) {
//				bookingClassWiseSegments.put(segment.getBookingCode(), new ArrayList<Segment>());
//			}
//
//			bookingClassWiseSegments.get(segment.getBookingCode()).add(segment);
//		}

		boolean isAccelAeroReturnBooking = GDSAASearchAssemblerUtil.isAccelAeroReturnBooking(segments);
        Collection<Collection<Segment>> outboundInboundSeperated = new ArrayList<Collection<Segment>>();
		if (isAccelAeroReturnBooking) {
			Collection[] segmentResults = GDSAASearchAssemblerUtil.getTurningPointSegments(segments);
			outboundInboundSeperated.add(segmentResults[1]);
			outboundInboundSeperated.add(segmentResults[0]);

		} else {
			outboundInboundSeperated.add(segments);
		}

		List<List<Segment>> onds = new ArrayList<List<Segment>>();
		List<Segment> ond;
		String departure;
		Date prevSegmentArrival;
		String maxConnectingTime;
		Date maxDepartingTime;

		for (Collection<Segment> segs : outboundInboundSeperated) {
			ond = null;
			prevSegmentArrival = null;
			for (Segment segment : segs) {
				departure = segment.getDepartureStation();


				// departs from a hub
				if (globalConfig.isHubAirport(departure)) {
					if (prevSegmentArrival != null) {

						maxConnectingTime = globalConfig.getMixMaxTransitDurations(departure, null, null)[1];
						maxDepartingTime = getAddedTransitTime(prevSegmentArrival, maxConnectingTime);

						if (maxDepartingTime.before(segment.getDepartureDateTime())) {
							ond = new ArrayList<Segment>();
							onds.add(ond);
						}
					} else {
						ond = new ArrayList<Segment>();
						onds.add(ond);
					}

					ond.add(segment);

				} else {
					ond = new ArrayList<Segment>();
					ond.add(segment);
					onds.add(ond);
				}
				prevSegmentArrival = segment.getArrivalDateTime();

			}
		}

		for (List<Segment> ondSegs : onds) {
			OriginDestinationInfoDTO ondInfoDTO = new OriginDestinationInfoDTO();

			List<Integer> flightSegmentIds = new ArrayList<Integer>();
			for (Segment segment : ondSegs) {

				Collection<FlightSegmentDTO> flightSegmentDTOs = new ArrayList<FlightSegmentDTO>();
				FlightSegmentDTO flightSegmentDTO = new FlightSegmentDTO();
				flightSegmentDTO.setFlightNumber(segment.getFlightNumber());
				flightSegmentDTO.setFromAirport(segment.getDepartureStation());
				flightSegmentDTO.setToAirport(segment.getArrivalStation());
				flightSegmentDTO.setDepartureDateTime(segment.getDepartureDateTime());
				flightSegmentDTO.setArrivalDateTime(segment.getArrivalDateTime());

				flightSegmentDTOs.add(flightSegmentDTO);

				log.error(" FltNum--" + segment.getFlightNumber() + "|Dep--" + segment.getDepartureStation() + "|Arr--" + segment.getArrivalStation() +
						"|DepDateTime--" + (segment.getDepartureDateTime() != null ? segment.getDepartureDateTime().toString() : "NULL") +
						"|ArrDateTime--" + (segment.getArrivalDateTime() != null ? segment.getArrivalDateTime().toString() : "NULL"));
				Collection<FlightSegmentDTO> filledFltSegDTOs = GDSServicesModuleUtil.getFlightBD().getFilledFlightSegmentDTOs(flightSegmentDTOs);

				if (filledFltSegDTOs != null) {
					log.error("filledFltSegDTOs.size ---- " + filledFltSegDTOs.size());
				}
				for (FlightSegmentDTO filledFlightSegDTO:filledFltSegDTOs) {
					if (filledFlightSegDTO.getSegmentId() == null) {
						log.error("SegmentId == NULL ---- onds.size -- " + onds.size() + " -- ondSegs.size -- " + ondSegs.size());
					}
					flightSegmentIds.add(filledFlightSegDTO.getSegmentId());
				}

			}


			ondInfoDTO.setOrigin(ondSegs.get(0).getDepartureStation());
			ondInfoDTO.setDestination(ondSegs.get(ondSegs.size() - 1).getArrivalStation());
			Date prefDTStart = CalendarUtil.getStartTimeOfDate(ondSegs.get(0).getDepartureDateTime());
			Date prefDTEnd = CalendarUtil.getEndTimeOfDate(ondSegs.get(ondSegs.size() - 1).getArrivalDateTime());

			ondInfoDTO.setDepartureDateTimeStart(prefDTStart);
			ondInfoDTO.setPreferredDateTimeStart(prefDTStart);
			ondInfoDTO.setDepartureDateTimeEnd(prefDTEnd);
			ondInfoDTO.setPreferredDateTimeEnd(prefDTEnd);

			String ownBookingClass = TTYMessageUtil.getActualBCForGdsMappedBC(gdsId, ondSegs.get(0).getBookingCode());
			BookingClass bookingClass = GDSServicesModuleUtil.getBookingClassBD().getLightWeightBookingClass(ownBookingClass);
			if (bookingClass == null) {
				throw new ModuleException("gdsservices.actions.gdsBookingClassDoesNotExist");
			}
			ondInfoDTO.setPreferredClassOfService(bookingClass.getCabinClassCode());
			ondInfoDTO.setPreferredBookingClass(ownBookingClass);
			ondInfoDTO.setPreferredBookingType(BookingClass.BookingClassType.NORMAL);
			ondInfoDTO.setPreferredLogicalCabin(bookingClass.getLogicalCCCode());
			ondInfoDTO.setFlightSegmentIds(flightSegmentIds);

			availableFlightSearchDTO.addOriginDestination(ondInfoDTO);
		}


		availableFlightSearchDTO.setPosAirport(gdsCredentialsDTO.getAgentStation());
		availableFlightSearchDTO.setAgentCode(gdsCredentialsDTO.getAgentCode());
		availableFlightSearchDTO.setChannelCode(SalesChannelsUtil
				.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));

		String actualBookingClass = TTYMessageUtil.getActualBCForGdsMappedBC(gdsId, firstSegment.getBookingCode());
		BookingClass bookingClass = GDSServicesModuleUtil.getBookingClassBD().getLightWeightBookingClass(actualBookingClass);

		if (bookingClass == null) {
			throw new ModuleException("gdsservices.actions.gdsBookingClassDoesNotExist");
		}

		availableFlightSearchDTO.setBookingClassCode(bookingClass.getBookingCode());
		availableFlightSearchDTO.setCabinClassCode(bookingClass.getCabinClassCode());
		availableFlightSearchDTO.setAvailabilityRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED);
		availableFlightSearchDTO.setBookingType(BookingClass.BookingClassType.NORMAL);
		availableFlightSearchDTO.setFareCategoryType(AirPricingCustomConstants.FareCategoryType.ANY);

		SelectedFlightDTO selectedFlightDTO = GDSServicesModuleUtil.getReservationQueryBD().getFareQuoteTnx(availableFlightSearchDTO.getSearchDTO(),
				null);

		Collection<OndFareDTO> fares = null;

		if (selectedFlightDTO != null) {
			fares = selectedFlightDTO.getOndFareDTOs(false);
		}

		return fares;
	}
	
	private static String getLogicalCabinClass(String cabinClass) {
		String defaultLogicalCCCode = null;
		if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
			for (CabinClassWiseDefaultLogicalCCDetailDTO defaultLogicalCCDetailDTO : CommonsServices.getGlobalConfig()
					.getDefaultLogicalCCDetails()) {
				if (cabinClass != null && cabinClass.equalsIgnoreCase(defaultLogicalCCDetailDTO.getCcCode())) {
					defaultLogicalCCCode = defaultLogicalCCDetailDTO.getDefaultLogicalCCCode();
					break;
				}
			}

		} else {
			defaultLogicalCCCode = cabinClass;
		}
		return defaultLogicalCCCode;
	}


	private static Date getAddedTransitTime(Date date, String timeInHhmm) {

		String hours = timeInHhmm.substring(0, timeInHhmm.indexOf(":"));
		String mins = timeInHhmm.substring(timeInHhmm.indexOf(":") + 1);

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(date);
		calander.add(GregorianCalendar.HOUR, Integer.parseInt(hours));
		calander.add(GregorianCalendar.MINUTE, Integer.parseInt(mins));

		return calander.getTime();
	}
}
