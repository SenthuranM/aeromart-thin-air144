package com.isa.thinair.gdsservices.core.typeb.validators;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;

public abstract class ValidatorBase {
	public abstract BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO);

	private GDSInternalCodes.ValidateConstants status;

	/**
	 * @return the status
	 */
	public GDSInternalCodes.ValidateConstants getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(GDSInternalCodes.ValidateConstants status) {
		this.status = status;
	}
}
