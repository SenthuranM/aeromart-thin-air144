/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */

package com.isa.thinair.gdsservices.core.persistence.dao;

import java.util.Collection;
import java.util.Map;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.internal.criteria.GDSMessageSearchCriteria;

/**
 * @author Nilindra
 * 
 */
public interface GDSServicesSupportJDBCDAO {
	/**
	 * loads originator pnr
	 * 
	 * @param pnr
	 * @return
	 */
	public String getPnr(String originatorPnr);

	/**
	 * Load SSR(s)
	 * 
	 * @return
	 */
	public Map getSSRs();

	/**
	 * Load Titles
	 * 
	 * @return
	 */
	public Collection getTitles();

	/**
	 * loads GDSMessage Pages
	 * 
	 * @param criteria
	 * @param startIndex
	 * @param noRecs
	 * @return
	 * @throws ModuleException
	 */
	public Page searchGDSMessages(GDSMessageSearchCriteria criteria, int startIndex, int noRecs) throws ModuleException;

	public String getPnrFromExternalRecordLocator(String externalRecordLocator) throws ModuleException;

	public String getPnrFromExternalRecordLocator(String externalRecordLocator, String gdsCode) throws ModuleException;

	public Long getReservationVersion(String pnr) throws ModuleException;

	public String getPnrFromExternalTicketNumber(String externalTicketNumber);

	public void lockUpdatingScheduleMessage(Integer scheduleMsgUnitId, String scheduleMsgUnitType);

	public void releasedLockedScheduleMessage(Integer scheduleMsgUnitId, String scheduleMsgUnitType);

	public boolean isLockedScheduleMessage(Integer scheduleMsgUnitId, String scheduleMsgUnitType);

}
