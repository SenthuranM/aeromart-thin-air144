/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.core.service.bd;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseServiceDelegate;
import com.isa.thinair.gdsservices.api.dto.publishing.Envelope;
import com.isa.thinair.gdsservices.api.service.GDSPublishingBD;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;

/**
 * local access implementation of GDSPublishingBD
 * 
 * @author Lasantha Pambagoda
 * @isa.module.bd-impl id="gdsservices.gdspublishing.local"
 */
public class GDSPublishingBDLocalImpl extends PlatformBaseServiceDelegate implements GDSPublishingBD {

	private final Log log = LogFactory.getLog(getClass());

	private static final java.lang.String DESTINATION_JNDI_NAME = "queue/isaGDSPublishQueue";
	private static final java.lang.String CONNECTION_FACTORY_JNDI_NAME = "ConnectionFactory";

	public void publishMessage(Envelope envilope) throws ModuleException {
		sendMessage(envilope, GDSServicesModuleUtil.getModule().getModuleConfig().getJndiProperties(), DESTINATION_JNDI_NAME,
				CONNECTION_FACTORY_JNDI_NAME);
	}
}
