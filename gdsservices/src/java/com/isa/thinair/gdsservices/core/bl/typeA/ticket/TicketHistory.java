package com.isa.thinair.gdsservices.core.bl.typeA.ticket;

import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageHandler;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.dto.temp.GdsReservation;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerTicketingInformationWrapper;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSServicesSupportJDBCDAO;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.GdsInfoDaoHelper;
import com.isa.typea.common.ErrorInformation;
import com.isa.typea.common.MessageFunction;
import com.isa.typea.common.TicketCoupon;
import com.isa.typea.common.TicketNumberDetails;
import com.isa.typea.common.TravellerTicketingInformation;
import com.isa.typea.tktreq.AATKTREQ;
import com.isa.typea.tktreq.TKTREQMessage;
import com.isa.typea.tktres.AATKTRES;
import com.isa.typea.tktres.TKTRESMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TicketHistory extends EdiMessageHandler {

	private static final Log log = LogFactory.getLog(TicketHistory.class);

	private AATKTREQ tktReq;
	private AATKTRES tktRes;

	private String ticketNumber;
	private GdsReservation<TravellerTicketingInformationWrapper> gdsReservation;

	protected void handleMessage() {

		TKTREQMessage tktreqMessage;
		TKTRESMessage tktresMessage;
		MessageFunction messageFunction;

		tktReq = (AATKTREQ) messageDTO.getRequest();

		tktreqMessage = tktReq.getMessage();

		tktRes = new AATKTRES();
		tktresMessage = new TKTRESMessage();
		tktresMessage.setMessageHeader(GDSDTOUtil.createRespMessageHeader(tktreqMessage.getMessageHeader(),
				messageDTO.getRequestMetaDataDTO().getMessageReference(), messageDTO.getRequestMetaDataDTO().getIataOperation().getResponse()));
		tktRes.setHeader(GDSDTOUtil.createRespEdiHeader(tktReq.getHeader(), tktresMessage.getMessageHeader(), messageDTO));
		tktRes.setMessage(tktresMessage);

		messageDTO.setResponse(tktRes);

		try {

			preHandleMessage();
			displayTicketHistory();

		} catch (GdsTypeAException e) {
			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(TypeAConstants.MessageFunction.HISTORY);
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			tktresMessage.setMessageFunction(messageFunction);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(e.getEdiErrorCode());
			tktresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);

			log.error("Error Occurred ----", e);

		} catch (Exception e) {
			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(TypeAConstants.MessageFunction.HISTORY);
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			tktresMessage.setMessageFunction(messageFunction);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR);
			tktresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);

			log.error("Error Occurred ----", e);

		}
	}

	private void preHandleMessage() throws GdsTypeAException {

		GDSServicesSupportJDBCDAO gdsServicesSupportDao = GDSServicesDAOUtils.DAOInstance.GDSSERVICES_SUPPORT_JDBC_DAO;
		String pnr;

		ticketNumber = tktReq.getMessage().getTickets().get(0).getTicketNumber();

		pnr = gdsServicesSupportDao.getPnrFromExternalTicketNumber(ticketNumber);

		gdsReservation =  GdsInfoDaoHelper.getGdsReservationTicketView(pnr);

	}

	private void displayTicketHistory() throws GdsTypeAException {

		TKTRESMessage tktresMessage = tktRes.getMessage();

		MessageFunction messageFunction = new MessageFunction();
		messageFunction.setMessageFunction(TypeAConstants.MessageFunction.HISTORY);
		messageFunction.setResponseType(TypeAConstants.ResponseType.PROCESSED_SUCCESSFULLY);
		tktresMessage.setMessageFunction(messageFunction);

		TravellerTicketingInformationWrapper travellerWrapper = resolvePassenger(ticketNumber);
		TicketNumberDetails couponTransitions = resolveCouponTransitions(ticketNumber, travellerWrapper);

		TravellerTicketingInformation travellerImage = travellerWrapper.getTravellerInformation();

		TravellerTicketingInformation traveller = new TravellerTicketingInformation();
		tktresMessage.getTravellerTicketingInformation().add(traveller);

		traveller.setTravellerSurname(travellerImage.getTravellerSurname());
		traveller.getTravellers().addAll(travellerImage.getTravellers());

		TicketNumberDetails ticketHistory = new TicketNumberDetails();
		traveller.getTickets().add(ticketHistory);

		ticketHistory.setTicketNumber(couponTransitions.getTicketNumber());
		ticketHistory.setDocumentType(couponTransitions.getDocumentType());

		TicketCoupon couponTransition;
		for (TicketCoupon transitionImage : couponTransitions.getTicketCoupon()) {
            couponTransition = new TicketCoupon();
			couponTransition.setCouponNumber(transitionImage.getCouponNumber());
			couponTransition.setStatus(transitionImage.getStatus());

			couponTransition.setFlightInfomation(transitionImage.getFlightInfomation());
			couponTransition.setRelatedProductInfo(transitionImage.getRelatedProductInfo());
			couponTransition.setDateAndTimeInformation(transitionImage.getDateAndTimeInformation());
			couponTransition.setOriginatorInformation(transitionImage.getOriginatorInformation());

			ticketHistory.getTicketCoupon().add(couponTransition);
		}

	}

	private TravellerTicketingInformationWrapper resolvePassenger(String ticketNumber) {
		TravellerTicketingInformationWrapper travellerWrapper = null;

		l0:
		for (TravellerTicketingInformationWrapper traveller : gdsReservation.getTravellerInformation()) {
			for (TicketNumberDetails ticket : traveller.getTravellerInformation().getTickets()) {
				if(ticketNumber.equals(ticket.getTicketNumber())) {
					travellerWrapper = traveller;
					break l0;
				}
			}
		}

		return travellerWrapper;
	}

	private TicketNumberDetails resolveCouponTransitions(String ticketNumber, TravellerTicketingInformationWrapper travellerWrapper) {
		TicketNumberDetails couponTransitions = null;

		for (TicketNumberDetails transitions : travellerWrapper.getCouponTransitions()) {
			if (ticketNumber.equals(transitions.getTicketNumber())) {
				couponTransitions = transitions;
				break;
			}
		}

		return  couponTransitions;
	}

}
