package com.isa.thinair.gdsservices.core.bl.internal.ticketcontrol;

import static com.isa.thinair.gdsservices.api.util.TypeAConstants.CouponStatus.AIRPORT_CONTROL;
import static com.isa.thinair.gdsservices.api.util.TypeAConstants.CouponStatus.NOTIFICATION;
import static com.isa.thinair.gdsservices.api.util.TypeAConstants.CouponStatus.NULL;
import static com.isa.thinair.gdsservices.api.util.TypeAConstants.CouponStatus.ORIGINAL_ISSUE;

import com.isa.thinair.gdsservices.core.dto.CouponStatusTransitionTo;

public class OperatingCarrierCouponStatusController extends CouponStatusController {

	public void receiveCouponStatusTransition(CouponStatusTransitionTo receive) {
		CouponStatusTransitionTo reply = new CouponStatusTransitionTo(CouponStatusTransitionTo.TransitionMode.SEND);

		String currentStatus = receive.getCurrentStatus();
		String receivingStatus = receive.getReceivingStatus();
		String newStatus = null;
		String sendingStatus = null;
		boolean sac = false;

		if (currentStatus.equals(NULL)) { // Validating carrier sends UAC
	   	    if (receivingStatus.equals(ORIGINAL_ISSUE)) {
		        newStatus = ORIGINAL_ISSUE;
		        sendingStatus = ORIGINAL_ISSUE;
	        }
		} else if (currentStatus.equals(ORIGINAL_ISSUE)) { // Validating carrier requests control returned
			if (receivingStatus.equals(NOTIFICATION)) {
				newStatus = NOTIFICATION;
				sendingStatus = ORIGINAL_ISSUE;
			}
		}

		reply.setNewStatus(newStatus);
		reply.setSendingStatus(sendingStatus);
		reply.setSettlementAuthCodeRequired(sac);

	}

	public void validateSendCouponStatusTransition(CouponStatusTransitionTo send) {
		String currentStatus = send.getCurrentStatus();
		String sendingStatus = send.getSendingStatus();
		String newStatus = null;
		boolean sac = false;
		boolean validTrans = false;

		if (currentStatus.equals(NULL)) { // Operating Carrier requests control
			if (sendingStatus.equals(AIRPORT_CONTROL)) {
				newStatus = ORIGINAL_ISSUE;
				validTrans = true;
			}
		} else if (currentStatus.equals(ORIGINAL_ISSUE)) { // Operating Carrier Returns control to Validating carrier
			if (sendingStatus.equals(ORIGINAL_ISSUE)) {
				newStatus = NULL;
				validTrans = true;
			}
		} else if (isInterimStatus(currentStatus)) {
			if (isInterimStatus(sendingStatus)) { // Operating carrier sends interim coupon status or Irregular Operations
				newStatus = sendingStatus;
				validTrans = true;
			} else if (isFinalStatus(sendingStatus)) { // Operating carrier sends final coupon status
				newStatus = sendingStatus;
				sac = true;
				validTrans = true;
			} else {
				validTrans = false;
			}
		}

		send.setNewStatus(newStatus);
		send.setSettlementAuthCodeRequired(sac);
		send.setInvalidTransition(!validTrans);
	}

	public void callbackSendCouponStatusTransition(CouponStatusTransitionTo reply) {
		String currentStatus = reply.getCurrentStatus();
		String sendingStatus = reply.getSendingStatus();
		String receivingStatus = reply.getReceivingStatus();
		boolean validTrans = false;

		if (currentStatus.equals(NULL)) { // Operating Carrier requests control
			if (sendingStatus.equals(AIRPORT_CONTROL)) {
				if (receivingStatus.equals(ORIGINAL_ISSUE)) {
					validTrans = true;
				}
			}
		} else if (currentStatus.equals(ORIGINAL_ISSUE)) { // Operating Carrier Returns control to Validating carrier
			if (sendingStatus.equals(ORIGINAL_ISSUE)) {
				if (receivingStatus.equals(ORIGINAL_ISSUE)) {
					validTrans = true;
				}
			}
		} else if (isInterimStatus(currentStatus)) {
			if (isInterimStatus(sendingStatus)) { // Operating carrier sends interim coupon status or Irregular Operations
				if (sendingStatus.equals(receivingStatus)) {
					validTrans = true;
				}
			} else if (isFinalStatus(sendingStatus)) { // Operating carrier sends final coupon status
				if (sendingStatus.equals(receivingStatus)) {
					validTrans = true;
				}
			} else {
				validTrans = false;
			}
		}

		reply.setInvalidTransition(!validTrans);

	}
}
