package com.isa.thinair.gdsservices.core.bl.typeA.ticket;

import com.isa.thinair.airproxy.api.dto.GDSChargeAdjustmentRQ;
import com.isa.thinair.airproxy.api.dto.GDSPaxChargesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketsAssembler;
import com.isa.thinair.gdsservices.api.model.ExternalReservation;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.bl.internal.ticket.TicketIssuanceHandler;
import com.isa.thinair.gdsservices.core.bl.internal.ticket.TicketIssuanceHandlerImpl;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageHandler;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.dto.temp.GdsReservation;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerTicketingInformationWrapper;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAConcurrencyException;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSTypeAServiceDAO;
import com.isa.thinair.gdsservices.core.typeA.transformers.ReservationDTOsTransformer;
import com.isa.thinair.gdsservices.core.typeA.transformers.ReservationDtoMerger;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.gdsservices.core.util.GdsDtoTransformer;
import com.isa.thinair.gdsservices.core.util.GdsInfoDaoHelper;
import com.isa.thinair.gdsservices.core.util.TypeANotes;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.typea.common.ErrorInformation;
import com.isa.typea.common.MessageFunction;
import com.isa.typea.common.TicketCoupon;
import com.isa.typea.common.TicketNumberDetails;
import com.isa.typea.common.TravellerTicketingInformation;
import com.isa.typea.tktreq.AATKTREQ;
import com.isa.typea.tktreq.TKTREQMessage;
import com.isa.typea.tktres.AATKTRES;
import com.isa.typea.tktres.TKTRESMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SystemCancelTicket extends EdiMessageHandler {

	private static final Log log = LogFactory.getLog(SystemCancelTicket.class);

	private AATKTREQ tktReq;
	private AATKTRES tktRes;

	private Map<String, Map<Integer, List<String>>> groupedTickets; // pnr -> pax-id -> tkt-no
	private Map<String, Map<Integer, List<String>>> groupedExchangedTickets;

	protected void handleMessage() {
		TKTREQMessage tktreqMessage;
		TKTRESMessage tktresMessage;
		MessageFunction messageFunction;
		MessageFunction rqMessageFunction;

		tktReq = (AATKTREQ) messageDTO.getRequest();
		tktreqMessage = tktReq.getMessage();

		tktRes =  new AATKTRES();
		tktresMessage = new TKTRESMessage();
		tktresMessage.setMessageHeader(GDSDTOUtil.createRespMessageHeader(tktreqMessage.getMessageHeader(),
				messageDTO.getRequestMetaDataDTO().getMessageReference(), messageDTO.getRequestMetaDataDTO().getIataOperation().getResponse()));
		tktRes.setHeader(GDSDTOUtil.createRespEdiHeader(tktReq.getHeader(), tktresMessage.getMessageHeader(), messageDTO));
		tktRes.setMessage(tktresMessage);
		messageDTO.setResponse(tktRes);

		rqMessageFunction = tktreqMessage.getMessageFunction();

		try {

			preSystemCancel();
			handleSystemCancel();

			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(rqMessageFunction.getMessageFunction());
			messageFunction.setSubMessageFunction(rqMessageFunction.getSubMessageFunction());
			messageFunction.setResponseType(TypeAConstants.ResponseType.PROCESSED_SUCCESSFULLY);
			tktresMessage.setMessageFunction(messageFunction);

		} catch (GdsTypeAConcurrencyException e) {
			messageDTO.setResponse(null);
			messageDTO.setSendResponse(false);
			messageDTO.setInvocationError(false);
			messageDTO.setRollbackTransaction(false);

			log.error("Error Occurred ----", e);

		} catch (GdsTypeAException e) {
			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(rqMessageFunction.getMessageFunction());
			messageFunction.setSubMessageFunction(rqMessageFunction.getSubMessageFunction());
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			tktresMessage.setMessageFunction(messageFunction);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(e.getEdiErrorCode());
			tktresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);

			log.error("Error Occurred ----", e);

		} catch (Exception e) {
			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(rqMessageFunction.getMessageFunction());
			messageFunction.setSubMessageFunction(rqMessageFunction.getSubMessageFunction());
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			tktresMessage.setMessageFunction(messageFunction);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR);
			tktresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);

			log.error("Error Occurred ----", e);

		}
	}

	private void preSystemCancel() throws GdsTypeAException {
		GDSTypeAServiceDAO gdsTypeAServiceDAO = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO;
		List<String> cancellingTicketNumbers = new ArrayList<String>();
		TKTREQMessage tktreqMessage = tktReq.getMessage();

		for (TicketNumberDetails ticket : tktreqMessage.getTickets()) {
			if (ticket.getDataIndicator() != null && ticket.getDataIndicator().equals(TypeAConstants.TicketDataIndicator.NEW)) {
				cancellingTicketNumbers.add(ticket.getTicketNumber());
			} else if (TypeAConstants.MessageFunction.VOID.equals(tktreqMessage.getMessageFunction().getSubMessageFunction())) {
				cancellingTicketNumbers.add(ticket.getTicketNumber());
			}
		}

		groupedTickets = gdsTypeAServiceDAO.groupTickets(cancellingTicketNumbers);

		Set<String> fetchedTickets = new HashSet<String>();
		for (Map<Integer, List<String>> resTickets : groupedTickets.values()) {
			for (List<String> paxTickets : resTickets.values()) {
				fetchedTickets.addAll(paxTickets);
			}
		}

		if (!fetchedTickets.containsAll(cancellingTicketNumbers)) {
			throw new GdsTypeAConcurrencyException();
		}
	}

	private boolean previouslyInvoked(String subMessageFunction, LCCClientReservation reservation ,
	                                  GdsReservation<TravellerTicketingInformationWrapper> gdsReservation) throws GdsTypeAException {
		boolean previouslyInvoked = false;
		Map<Integer, List<String>> resTickets;
		List<String> paxTickets;

		TicketNumberDetails ticketNumberDetails;

		resTickets = groupedTickets.get(reservation.getPNR());

		for (Integer pnrPaxId : resTickets.keySet()) {

			paxTickets = resTickets.get(pnrPaxId);

			for (String ticketNumber : paxTickets) {
				ticketNumberDetails = GDSDTOUtil.resolveTicket(gdsReservation, ticketNumber);

				for (TicketCoupon coupon : ticketNumberDetails.getTicketCoupon()) {

					if (subMessageFunction.equals(TypeAConstants.MessageFunction.ISSUE) ||
							subMessageFunction.equals(TypeAConstants.MessageFunction.EXCHANGE)) {
						if (coupon.getStatus().equals(TypeAConstants.CouponStatus.VOIDED)) {
							previouslyInvoked = true;
							break;
						}
					} else if (subMessageFunction.equals(TypeAConstants.MessageFunction.REFUND)) {
						if (coupon.getStatus().equals(TypeAConstants.CouponStatus.ORIGINAL_ISSUE)) {
							previouslyInvoked = true;
							break;
						}
					}
				}
			}
		}

		return previouslyInvoked;
	}

	private void handleSystemCancel() throws GdsTypeAException {
		TKTREQMessage tktreqMessage;
		MessageFunction messageFunction;

		tktreqMessage = tktReq.getMessage();
		messageFunction = tktreqMessage.getMessageFunction();

		String subMessageFunction = messageFunction.getSubMessageFunction();

		LCCClientReservation reservation;
		GdsReservation<TravellerTicketingInformationWrapper> gdsReservation;

		for (String pnr : groupedTickets.keySet()) {

			try {
				GDSServicesModuleUtil.getGDSNotifyBD().reservationLock(pnr);
			} catch (ModuleException e) {
				throw new GdsTypeAConcurrencyException();
			}

			try {
				reservation = GDSServicesUtil.loadLccReservation(pnr);
				if (reservation == null) {
					throw new ModuleException(TypeANotes.ErrorMessages.NO_MESSAGE);
				}

				gdsReservation = GdsInfoDaoHelper.getGdsReservationTicketView(pnr);
				if (gdsReservation == null) {
					throw new ModuleException(TypeANotes.ErrorMessages.NO_MESSAGE);
				}

			} catch (ModuleException e) {
				throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.NO_PNR_MATCH, TypeANotes.ErrorMessages.NO_MESSAGE);
			}


			if (!previouslyInvoked(subMessageFunction, reservation, gdsReservation)) {
				systemCancel(subMessageFunction, reservation, gdsReservation);
			}

		}

	}

	private void systemCancel(String subMessageFunction, LCCClientReservation reservation ,
	                          GdsReservation<TravellerTicketingInformationWrapper> gdsReservation) throws GdsTypeAException {
		if (subMessageFunction.equals(TypeAConstants.MessageFunction.ISSUE)) {
			cancelIssue(reservation, gdsReservation);
		} else if (subMessageFunction.equals(TypeAConstants.MessageFunction.EXCHANGE)) {
			preCancelReIssue();
			cancelReIssue(reservation, gdsReservation);
		} else if (subMessageFunction.equals(TypeAConstants.MessageFunction.REFUND)) {
			cancelRefund(reservation, gdsReservation);
		} else if (subMessageFunction.equals(TypeAConstants.MessageFunction.VOID)) {
			cancelVoid(reservation);
		}
	}

	private void cancelIssue(LCCClientReservation reservation, GdsReservation<TravellerTicketingInformationWrapper> gdsReservation)
			throws GdsTypeAException {

		TravellerTicketingInformationWrapper travellerWrapperImage;
		TravellerTicketingInformation travellerImage;

		LCCClientReservationPax passenger;
		Iterator<LCCClientPaymentInfo> transactions;
		LCCClientPaymentInfo transaction;

		Map<Integer, List<String>> cancellingPassengers;
		List<String> tickets;
		Set<String> processedTickets;
		List<TicketNumberDetails> updatedTickets;
		BigDecimal refundAmount;
		GDSPaxChargesTO paxChargesTO;

		TicketsAssembler.PassengerAssembler passengerAssembler;
		TicketsAssembler ticketsAssembler = new TicketsAssembler(TicketsAssembler.Mode.REFUND);
		ticketsAssembler.setPnr(reservation.getPNR());
		ticketsAssembler.setIpAddress(messageDTO.getRequestIp());


		GDSChargeAdjustmentRQ intermediateChgAdjRQ = new GDSChargeAdjustmentRQ();
		intermediateChgAdjRQ.setPnr(reservation.getPNR());
		intermediateChgAdjRQ.setGroupPNR(reservation.isGroupPNR());
		intermediateChgAdjRQ.setVersion(reservation.getVersion());

		GDSChargeAdjustmentRQ finalChgAdjRQ = new GDSChargeAdjustmentRQ();
		finalChgAdjRQ.setPnr(reservation.getPNR());
		finalChgAdjRQ.setGroupPNR(reservation.isGroupPNR());
		finalChgAdjRQ.setVersion(reservation.getVersion());

		TicketIssuanceHandler ticketIssuanceHandler;
		ReservationBD reservationBD;

		boolean skipMonetaryOperations;

		cancellingPassengers = groupedTickets.get(reservation.getPNR());
		for (Integer pnrPaxId : cancellingPassengers.keySet()) {
			processedTickets = new HashSet<String>();
			updatedTickets = new ArrayList<TicketNumberDetails>();
			transaction = null;

			tickets = cancellingPassengers.get(pnrPaxId);

			passenger = GDSDTOUtil.resolvePax(reservation, pnrPaxId);

			skipMonetaryOperations = passenger.getPaxType().equals(PaxTypeTO.INFANT);

			if (!skipMonetaryOperations) {
				transactions = getApplicableTransactions(passenger.getLccClientPaymentHolder().getPayments(), ReservationInternalConstants.TnxTypes.CREDIT);
				transaction = transactions.next();
			}

			paxChargesTO = new GDSPaxChargesTO();
			paxChargesTO.setFareAmount(BigDecimal.ZERO);
			paxChargesTO.setTaxAmount(BigDecimal.ZERO);
			paxChargesTO.setSurChargeAmount(BigDecimal.ZERO);
			paxChargesTO.setPaxType(passenger.getPaxType());
			paxChargesTO.setInactiveSegmentsApplicable(true);
			intermediateChgAdjRQ.addGdsCharges(PaxTypeUtils.getPnrPaxId(passenger.getTravelerRefNumber()), paxChargesTO);

			paxChargesTO = new GDSPaxChargesTO();
			paxChargesTO.setFareAmount(passenger.getTotalFare());
			paxChargesTO.setTaxAmount(passenger.getTotalTaxCharge());
			paxChargesTO.setSurChargeAmount(passenger.getTotalSurCharge());
			paxChargesTO.setPaxType(passenger.getPaxType());
			paxChargesTO.setInactiveSegmentsApplicable(true);
			finalChgAdjRQ.addGdsCharges(PaxTypeUtils.getPnrPaxId(passenger.getTravelerRefNumber()), paxChargesTO);

			for (String ticket : tickets) {
				// TODO -- reuse conjunction
				if (!processedTickets.contains(ticket)) {
					travellerWrapperImage = GDSDTOUtil.resolvePassengerByTicketNumber(gdsReservation, ticket);
					travellerImage = travellerWrapperImage.getTravellerInformation();

					if (!skipMonetaryOperations) {
						passengerAssembler = GdsDtoTransformer.appendToTicketsAssembler(ticketsAssembler, travellerImage, passenger);
						refundAmount = transaction.getTotalAmount();

						passengerAssembler.setTotalAmount(refundAmount);
						passengerAssembler.setNote("Payment Reversal Due To System-Cancel");
					}

					for (TicketNumberDetails ticketNumberDetails : travellerImage.getTickets()) {
						for (TicketCoupon ticketCoupon : ticketNumberDetails.getTicketCoupon()) {
							ticketCoupon.setStatus(TypeAConstants.CouponStatus.VOIDED);
						}
						updatedTickets.add(ticketNumberDetails);
						processedTickets.add(ticketNumberDetails.getTicketNumber());
					}

				}
			}

			updateTicketInfo(passenger, updatedTickets);
		}

		GdsInfoDaoHelper.saveGdsReservationTicketView(reservation.getPNR(), gdsReservation);

		try {
			reservationBD = GDSServicesModuleUtil.getReservationBD();
			ticketIssuanceHandler = new TicketIssuanceHandlerImpl();

			reservationBD.syncAATotalGroupChargesWithCarrier(intermediateChgAdjRQ, null);

			ticketIssuanceHandler.handleMonetaryDiscrepancies(ticketsAssembler);

			reservationBD.syncAATotalGroupChargesWithCarrier(finalChgAdjRQ, null);

		} catch (ModuleException e) {
			throw new GdsTypeAException();
		}

	}

	private void cancelReIssue(LCCClientReservation reservation, GdsReservation<TravellerTicketingInformationWrapper> gdsReservation)
			throws GdsTypeAException {

		Map<Integer, List<String>> cancellingPassengersNew;
		Map<Integer, List<String>> cancellingPassengersOld;
		List<String> ticketsNew;
		List<String> ticketsOld;

		LCCClientReservationPax passenger;
		Iterator<LCCClientPaymentInfo> transactions;
		LCCClientPaymentInfo transaction;

		BigDecimal refundAmount;
		GDSPaxChargesTO paxChargesTO;

		List<TicketNumberDetails> updatingTickets;
		TicketNumberDetails updatingTicket;
		TicketNumberDetails ticketImage;
		TravellerTicketingInformationWrapper travellerWrapper;
		TravellerTicketingInformation travellerImage;

		boolean skipMonetaryOperations;

		List<TravellerTicketingInformationWrapper> travellersByConjunctions;
		Map<Integer, Set<String>> conjunctiveTickets;
		Set<Integer> processedConjunctionGroups;
		Integer conjunctiveGroupId;

		TicketIssuanceHandler ticketIssuanceHandler;
		ReservationBD reservationBD;

		TicketsAssembler.PassengerAssembler passengerAssembler;
		TicketsAssembler ticketsAssembler = new TicketsAssembler(TicketsAssembler.Mode.REFUND);
		ticketsAssembler.setPnr(reservation.getPNR());
		ticketsAssembler.setIpAddress(messageDTO.getRequestIp());


		GDSChargeAdjustmentRQ intermediateChgAdjRQ = new GDSChargeAdjustmentRQ();
		intermediateChgAdjRQ.setPnr(reservation.getPNR());
		intermediateChgAdjRQ.setGroupPNR(reservation.isGroupPNR());
		intermediateChgAdjRQ.setVersion(reservation.getVersion());

		GDSChargeAdjustmentRQ finalChgAdjRQ = new GDSChargeAdjustmentRQ();
		finalChgAdjRQ.setPnr(reservation.getPNR());
		finalChgAdjRQ.setGroupPNR(reservation.isGroupPNR());
		finalChgAdjRQ.setVersion(reservation.getVersion());


		cancellingPassengersNew = groupedTickets.get(reservation.getPNR());

		if (!groupedExchangedTickets.containsKey(reservation.getPNR())) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.EXISTING_ITINERARY_INCOMPATIBLE, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		cancellingPassengersOld = groupedExchangedTickets.get(reservation.getPNR());

		for (Integer pnrPaxId : cancellingPassengersNew.keySet()) {

			transaction = null;

			passenger = GDSDTOUtil.resolvePax(reservation, pnrPaxId);
			skipMonetaryOperations = passenger.getPaxType().equals(PaxTypeTO.INFANT);

			travellersByConjunctions = GDSDTOUtil.resolvePassengers(gdsReservation, passenger);
			conjunctiveTickets = GDSDTOUtil.groupTicketsByConjunctions(travellersByConjunctions);
			processedConjunctionGroups = new HashSet<Integer>();

			if (!cancellingPassengersOld.containsKey(pnrPaxId)) {
				throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.EXISTING_ITINERARY_INCOMPATIBLE, TypeANotes.ErrorMessages.NO_MESSAGE);
			}

			updatingTickets = new ArrayList<TicketNumberDetails>();

			ticketsNew = cancellingPassengersNew.get(pnrPaxId);
			ticketsOld = cancellingPassengersOld.get(pnrPaxId);

			if (!skipMonetaryOperations) {
				transactions = getApplicableTransactions(passenger.getLccClientPaymentHolder().getPayments(), ReservationInternalConstants.TnxTypes.CREDIT);
				transaction = transactions.next();
			}


			paxChargesTO = new GDSPaxChargesTO();
			paxChargesTO.setFareAmount(BigDecimal.ZERO);
			paxChargesTO.setTaxAmount(BigDecimal.ZERO);
			paxChargesTO.setSurChargeAmount(BigDecimal.ZERO);
			paxChargesTO.setPaxType(passenger.getPaxType());
			paxChargesTO.setInactiveSegmentsApplicable(true);
			intermediateChgAdjRQ.addGdsCharges(PaxTypeUtils.getPnrPaxId(passenger.getTravelerRefNumber()), paxChargesTO);

			paxChargesTO = new GDSPaxChargesTO();
			paxChargesTO.setFareAmount(passenger.getTotalFare());
			paxChargesTO.setTaxAmount(passenger.getTotalTaxCharge());
			paxChargesTO.setSurChargeAmount(passenger.getTotalSurCharge());
			paxChargesTO.setPaxType(passenger.getPaxType());
			paxChargesTO.setInactiveSegmentsApplicable(true);
			finalChgAdjRQ.addGdsCharges(PaxTypeUtils.getPnrPaxId(passenger.getTravelerRefNumber()), paxChargesTO);


			for (String ticketNo : ticketsOld) {
				travellerWrapper = GDSDTOUtil.resolvePassengerByTicketNumber(gdsReservation, ticketNo);
				travellerImage = travellerWrapper.getTravellerInformation();
				ticketImage = GDSDTOUtil.resolveTicket(travellerImage, ticketNo);

				updatingTicket = new TicketNumberDetails();
				updatingTicket.setTicketNumber(ticketImage.getTicketNumber());
				updatingTicket.setDocumentType(ticketImage.getDocumentType());

				for (TicketCoupon coupon : ticketImage.getTicketCoupon()) {
					if (coupon.getStatus().equals(TypeAConstants.CouponStatus.EXCHANGED)) {
						coupon.setStatus(TypeAConstants.CouponStatus.ORIGINAL_ISSUE);
						coupon.setSettlementAuthCode(null);

						updatingTicket.getTicketCoupon().add(coupon);
					}
				}

				updatingTickets.add(updatingTicket);

			}

			for (String ticketNo : ticketsNew) {
				travellerWrapper = GDSDTOUtil.resolvePassengerByTicketNumber(gdsReservation, ticketNo);
				travellerImage = travellerWrapper.getTravellerInformation();
				ticketImage = GDSDTOUtil.resolveTicket(travellerImage, ticketNo);

				updatingTicket = new TicketNumberDetails();
				updatingTicket.setTicketNumber(ticketImage.getTicketNumber());
				updatingTicket.setDocumentType(ticketImage.getDocumentType());

				for (TicketCoupon coupon : ticketImage.getTicketCoupon()) {
					if (coupon.getStatus().equals(TypeAConstants.CouponStatus.ORIGINAL_ISSUE)) {
						coupon.setStatus(TypeAConstants.CouponStatus.VOIDED);

						updatingTicket.getTicketCoupon().add(coupon);
					}
				}

				updatingTickets.add(updatingTicket);

				conjunctiveGroupId = getConjunctiveGroupId(conjunctiveTickets, ticketNo);

				if (!processedConjunctionGroups.contains(conjunctiveGroupId)) {
					if (!skipMonetaryOperations) {
						if (transaction.getTotalAmount().compareTo(BigDecimal.ZERO) > 0) {
							passengerAssembler = GdsDtoTransformer.appendToTicketsAssembler(ticketsAssembler, travellerImage, passenger);
							refundAmount = transaction.getTotalAmount();

							passengerAssembler.setTotalAmount(refundAmount);
							passengerAssembler.setNote("Payment Reversal Due To System-Cancel");

						}
					}
					processedConjunctionGroups.add(conjunctiveGroupId);
				}

			}

			updateTicketInfo(passenger, updatingTickets);
		}


		GdsInfoDaoHelper.saveGdsReservationTicketView(reservation.getPNR(), gdsReservation);

		try {
			reservationBD = GDSServicesModuleUtil.getReservationBD();
			ticketIssuanceHandler = new TicketIssuanceHandlerImpl();

			reservationBD.syncAATotalGroupChargesWithCarrier(intermediateChgAdjRQ, null);

			ticketIssuanceHandler.handleMonetaryDiscrepancies(ticketsAssembler);

			reservationBD.syncAATotalGroupChargesWithCarrier(finalChgAdjRQ, null);

		} catch (ModuleException e) {
			throw new GdsTypeAException();
		}

	}

	private void preCancelReIssue() {
		GDSTypeAServiceDAO gdsTypeAServiceDAO = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO;
		TKTREQMessage tktreqMessage = tktReq.getMessage();

		List<String> cancellingTicketNumbers = new ArrayList<String>();
		for (TicketNumberDetails ticket : tktreqMessage.getTickets()) {
			if (ticket.getDataIndicator() != null && ticket.getDataIndicator().equals(TypeAConstants.TicketDataIndicator.OLD)) {
				cancellingTicketNumbers.add(ticket.getTicketNumber());
			}
		}

		groupedExchangedTickets = gdsTypeAServiceDAO.groupTickets(cancellingTicketNumbers);
	}

	private void cancelRefund(LCCClientReservation reservation, GdsReservation<TravellerTicketingInformationWrapper> gdsReservation)
			throws GdsTypeAException {

		List<TravellerTicketingInformationWrapper> travellersByConjunctions;
		Map<Integer, Set<String>> conjunctiveTickets;
		Set<Integer> processedConjunctionGroups;
		Integer conjunctiveGroupId;

		TravellerTicketingInformationWrapper travellerWrapperImage;
		TravellerTicketingInformation travellerImage;
		List<TicketNumberDetails> updatedTickets;


		LCCClientReservationPax passenger;
		Iterator<LCCClientPaymentInfo> transactions = null;
		LCCClientPaymentInfo transaction;

		Map<Integer, List<String>> cancellingPassengers;
		List<String> tickets;
		BigDecimal payAmount;
		boolean skipMonetaryOperations;

		TicketsAssembler.PassengerAssembler passengerAssembler;
		TicketsAssembler ticketsAssembler = new TicketsAssembler(TicketsAssembler.Mode.PAYMENT);
		ticketsAssembler.setPnr(reservation.getPNR());
		ticketsAssembler.setIpAddress(messageDTO.getRequestIp());


		cancellingPassengers = groupedTickets.get(reservation.getPNR());
		for (Integer pnrPaxId : cancellingPassengers.keySet()) {

			tickets = cancellingPassengers.get(pnrPaxId);

			passenger = GDSDTOUtil.resolvePax(reservation, pnrPaxId);
			skipMonetaryOperations = passenger.getPaxType().equals(PaxTypeTO.INFANT);

			if (!skipMonetaryOperations) {
				transactions = getApplicableTransactions(passenger.getLccClientPaymentHolder().getPayments(), ReservationInternalConstants.TnxTypes.DEBIT);
			}

			travellersByConjunctions = GDSDTOUtil.resolvePassengers(gdsReservation, passenger);
			conjunctiveTickets = GDSDTOUtil.groupTicketsByConjunctions(travellersByConjunctions);
			processedConjunctionGroups = new HashSet<Integer>();
			updatedTickets = new ArrayList<TicketNumberDetails>();

			if (!tickets.isEmpty()) {

				for (String ticket : tickets) {

					conjunctiveGroupId = getConjunctiveGroupId(conjunctiveTickets, ticket);

					travellerWrapperImage = GDSDTOUtil.resolvePassengerByTicketNumber(gdsReservation, ticket);
					travellerImage = travellerWrapperImage.getTravellerInformation();

					if (!processedConjunctionGroups.contains(conjunctiveGroupId)) {

						if (!skipMonetaryOperations) {
							transaction = transactions.next();
							passengerAssembler = GdsDtoTransformer.appendToTicketsAssembler(ticketsAssembler, travellerImage, passenger);
							payAmount = transaction.getTotalAmount();

							passengerAssembler.setBalance(payAmount.abs());
							passengerAssembler.setNote("Refund Reversal Due To System-Cancel");
						}

						processedConjunctionGroups.add(conjunctiveGroupId);
					}

					TicketNumberDetails ticketNumberDetails = GDSDTOUtil.resolveTicket(travellerImage, ticket);

					for (TicketCoupon ticketCoupon : ticketNumberDetails.getTicketCoupon()) {
						if (ticketCoupon.getStatus().equals(TypeAConstants.CouponStatus.REFUNDED)) {
							ticketCoupon.setStatus(TypeAConstants.CouponStatus.ORIGINAL_ISSUE);
							ticketCoupon.setSettlementAuthCode(null);
						}

						updatedTickets.add(ticketNumberDetails);
					}

				}
			} else {
				throw new GdsTypeAException();
			}

			updateTicketInfo(passenger, updatedTickets);

		}

		GdsInfoDaoHelper.saveGdsReservationTicketView(reservation.getPNR(), gdsReservation);

		TicketIssuanceHandler ticketIssuanceHandler = new TicketIssuanceHandlerImpl();
		try {
			ticketIssuanceHandler.handleMonetaryDiscrepancies(ticketsAssembler);
		} catch (ModuleException e) {
			throw new GdsTypeAException();
		}



	}

	// ----------------------------------------
	// TODO -- move to a util

	private Iterator<LCCClientPaymentInfo> getApplicableTransactions(Collection<LCCClientPaymentInfo> transactions, String tnxType)
			throws GdsTypeAException {

		List<LCCClientPaymentInfo> filteredTransactions = new ArrayList<LCCClientPaymentInfo>();
		for (LCCClientPaymentInfo payInfo : transactions) {
			if ((tnxType.equals(ReservationInternalConstants.TnxTypes.CREDIT) && payInfo.getTotalAmount().compareTo(BigDecimal.ZERO) >= 0) ||
					(tnxType.equals(ReservationInternalConstants.TnxTypes.DEBIT) && payInfo.getTotalAmount().compareTo(BigDecimal.ZERO) < 0)) {
				filteredTransactions.add(payInfo);
			}
		}

		if (filteredTransactions.isEmpty()) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.UNABLE_TO_PROCESS, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		Collections.sort(filteredTransactions, new Comparator<LCCClientPaymentInfo>() {
			public int compare(LCCClientPaymentInfo o1, LCCClientPaymentInfo o2) {
				return o2.getTxnDateTime().compareTo(o1.getTxnDateTime());
			}
		});

		return filteredTransactions.iterator();

	}

	private void updateTicketInfo(LCCClientReservationPax pax, List<TicketNumberDetails> tickets) throws GdsTypeAException {

		List<LccClientPassengerEticketInfoTO> lccCoupons = ReservationDtoMerger.getMergedETicketCoupons(tickets, pax);

		List<EticketTO> coupons = ReservationDTOsTransformer.toETicketTos(lccCoupons);

		try {
			GDSServicesModuleUtil.getReservationBD().updateExternalEticketInfo(coupons);
		} catch (ModuleException e) {
			throw new GdsTypeAException();
		}
	}

	private int getConjunctiveGroupId(Map<Integer, Set<String>> conjunctions, String ticketNumber) throws GdsTypeAException {
		Integer conjunctiveGroupId = null;

		for (Integer groupId : conjunctions.keySet()) {
			if (conjunctions.get(groupId).contains(ticketNumber)) {
				conjunctiveGroupId = groupId;
				break;
			}
		}

		if (conjunctiveGroupId == null) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.EXISTING_ITINERARY_INCOMPATIBLE, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		return conjunctiveGroupId;
	}
	
	private void cancelVoid(LCCClientReservation reservation) throws GdsTypeAException {
		TKTREQMessage tktreqMessage = tktReq.getMessage();
		List<Integer> pnrPaxIdUpdated = new ArrayList<Integer>();
		Map<Integer, TicketsAssembler.PassengerAssembler> passengerAssemblerByPaxId = new HashMap<Integer, TicketsAssembler.PassengerAssembler>();
		if (reservation != null) {
			for (TicketNumberDetails traveller : tktreqMessage.getTickets()) {
				Integer pnrPaxId = null;
				BigDecimal balancePayment = AccelAeroCalculator.getDefaultBigDecimalZero();
				PAX: for (LCCClientReservationPax passenger : reservation.getPassengers()) {
					pnrPaxId = PaxTypeUtils.getPnrPaxId(passenger.getTravelerRefNumber());
					if (pnrPaxIdUpdated.contains(pnrPaxId)) {
						continue;
					}

					for (LccClientPassengerEticketInfoTO paxEticketInfoTO : passenger.geteTickets()) {
						if (traveller.getTicketNumber().equals(paxEticketInfoTO.getExternalPaxETicketNo())) {
							pnrPaxIdUpdated.add(pnrPaxId);
							balancePayment = passenger.getTotalAvailableBalance();
							break PAX;
						}
					}
				}

				if (balancePayment.compareTo(BigDecimal.ZERO) > 0) {
					TicketsAssembler.PassengerAssembler passengerAssembler = new TicketsAssembler.PassengerAssembler();
					passengerAssembler.setPnrPaxId(pnrPaxId);
					passengerAssembler.setCurrencyCode(AppSysParamsUtil.getBaseCurrency());
					passengerAssembler.setBalance(balancePayment);
					passengerAssembler.setNote("VOID Reversal Due To System-Cancel");

					passengerAssemblerByPaxId.put(pnrPaxId, passengerAssembler);
				}
			}

			// make sure all passengers are considered
			if (reservation.getPassengers().size() != pnrPaxIdUpdated.size()) {
				throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.NUMBER_IN_PARTY_INVALID,
						TypeANotes.ErrorMessages.NO_MESSAGE);
			}

			restorePaymentsBack(passengerAssemblerByPaxId, reservation.getPNR());
		}
	}

	private void restorePaymentsBack(Map<Integer, TicketsAssembler.PassengerAssembler> passengerAssemblerByPaxId, String pnr)
			throws GdsTypeAException {
		try {
			if (passengerAssemblerByPaxId != null && !passengerAssemblerByPaxId.isEmpty()) {
				for (Integer pnrPaxId : passengerAssemblerByPaxId.keySet()) {
					TicketsAssembler.PassengerAssembler passengerAssembler = passengerAssemblerByPaxId.get(pnrPaxId);

					TicketsAssembler ticketsAssembler = new TicketsAssembler(TicketsAssembler.Mode.PAYMENT);
					ticketsAssembler.setPnr(pnr);
					ticketsAssembler.setIpAddress(messageDTO.getRequestIp());

					ticketsAssembler.addPassengerAssembler(passengerAssembler);
					TicketIssuanceHandler ticketIssuanceHandler = new TicketIssuanceHandlerImpl();
					ticketIssuanceHandler.handleMonetaryDiscrepancies(ticketsAssembler);

				}
			}
		} catch (ModuleException e) {
			throw new GdsTypeAException();
		}
	}
}
