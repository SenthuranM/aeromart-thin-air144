/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.core.bl.ssm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.NotifyFlightEventRQ;
import com.isa.thinair.airinventory.api.dto.NotifyFlightEventsRQ;
import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airschedules.api.dto.ReScheduleRequiredInfo;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.SSMSplitFlightSchedule;
import com.isa.thinair.airschedules.api.utils.FlightEventCode;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.airschedules.api.utils.GDSScheduleEventCollector;
import com.isa.thinair.airschedules.api.utils.ScheduleBuildStatusEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for integrate create schedule with gds
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="publishUpdateSSM"
 */
public class PublishUpdateSSM extends DefaultBaseCommand {

	private BookingClassBD bookingClassBD;

	private FlightInventoryBD flightInventoryBD;

	/**
	 * constructor of the PublishUpdateSSM command
	 */
	public PublishUpdateSSM() {

		// looking up BDs
		bookingClassBD = GDSServicesModuleUtil.getBookingClassBD();

		flightInventoryBD = GDSServicesModuleUtil.getFlightInventoryBD();
	}

	/**
	 * execute method of the IntegrateSSMNew command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		GDSScheduleEventCollector sGdsEvents = (GDSScheduleEventCollector) this
				.getParameter(GDSSchedConstants.ParamNames.GDS_EVENT_COLLECTOR);

		Map gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
		Map aircraftTypeMap = GDSServicesModuleUtil.getGlobalConfig().getIataAircraftCodes();

		String airLineCode = GDSServicesModuleUtil.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);

		Collection gdsIdsForRbd = new ArrayList();

		if (sGdsEvents.getExistingSchedule().getGdsIds() != null) {
			gdsIdsForRbd.addAll(sGdsEvents.getExistingSchedule().getGdsIds());
		}

		if ((sGdsEvents.getExistingOverlapSchedule() != null)
				&& (sGdsEvents.getUpdatedOverlapSchedule().getGdsIds() != null)) {
			gdsIdsForRbd.addAll(sGdsEvents.getExistingOverlapSchedule().getGdsIds());
		}

		Map bcMap = bookingClassBD.getBookingClassCodes(gdsIdsForRbd);

		if (isEligibleToSendSubMessages(sGdsEvents.getExistingSubSchedules(), sGdsEvents.getActions())) {
			// send sub-messages
			if (sGdsEvents.getExistingSchedule().getBuildStatusCode().equals(ScheduleBuildStatusEnum.BUILT.getCode())) {

				ReScheduleRequiredInfo reScheduleInfo = new ReScheduleRequiredInfo();

				reScheduleInfo.setGdsStatusMap(gdsStatusMap);
				reScheduleInfo.setAircraftTypeMap(aircraftTypeMap);
				reScheduleInfo.setServiceType(sGdsEvents.getServiceType());
				reScheduleInfo.setAirLineCode(airLineCode);
				reScheduleInfo.setBcMap(bcMap);
				reScheduleInfo.setCancelExistingSubSchedule(false);
				reScheduleInfo.setSupplementaryInfo(sGdsEvents.getSupplementaryInfo());
				reScheduleInfo.setActions(sGdsEvents.getActions());
				reScheduleInfo.setPublishedGdsIds(sGdsEvents.getExistingSchedule().getGdsIds());
				reScheduleInfo.setExistingSchedule(sGdsEvents.getExistingSchedule());
				reScheduleInfo.setUpdatedSchedule(sGdsEvents.getUpdatedSchedule());
				reScheduleInfo.setCancelExistingSchedule(isCancelOldSchedule(sGdsEvents.getActions()));

				SchedulePublishBL.sendSsmSubMessagesForUpdate(reScheduleInfo);
			}

			if (sGdsEvents.getExistingOverlapSchedule() != null && sGdsEvents.getExistingOverlapSchedule()
					.getBuildStatusCode().equals(ScheduleBuildStatusEnum.BUILT.getCode())) {
				ReScheduleRequiredInfo reScheduleInfo = new ReScheduleRequiredInfo();

				reScheduleInfo.setGdsStatusMap(gdsStatusMap);
				reScheduleInfo.setAircraftTypeMap(aircraftTypeMap);
				reScheduleInfo.setServiceType(sGdsEvents.getServiceType());
				reScheduleInfo.setAirLineCode(airLineCode);
				reScheduleInfo.setBcMap(bcMap);
				reScheduleInfo.setCancelExistingSubSchedule(false);
				reScheduleInfo.setSupplementaryInfo(sGdsEvents.getSupplementaryInfo());
				reScheduleInfo.setActions(sGdsEvents.getActions());
				reScheduleInfo.setPublishedGdsIds(sGdsEvents.getExistingOverlapSchedule().getGdsIds());
				reScheduleInfo.setExistingSchedule(sGdsEvents.getExistingOverlapSchedule());
				reScheduleInfo.setUpdatedSchedule(sGdsEvents.getUpdatedOverlapSchedule());
				reScheduleInfo.setExistingSubSchedules(null);
				reScheduleInfo.setUpdatedSubSchedules(null);
				reScheduleInfo.setCancelExistingSchedule(isCancelOldSchedule(sGdsEvents.getActions()));

				SchedulePublishBL.sendSSMForSchedulePeriodChanges(reScheduleInfo);
			}
		} else 
		if (isCancelAndComposeNew(sGdsEvents.getActions())) {

			if (sGdsEvents.getExistingSchedule().getBuildStatusCode().equals(ScheduleBuildStatusEnum.BUILT.getCode())) {
				ReScheduleRequiredInfo reScheduleInfo = new ReScheduleRequiredInfo();

				reScheduleInfo.setGdsStatusMap(gdsStatusMap);
				reScheduleInfo.setAircraftTypeMap(aircraftTypeMap);
				reScheduleInfo.setServiceType(sGdsEvents.getServiceType()); //null FIXME
				reScheduleInfo.setAirLineCode(airLineCode);
				reScheduleInfo.setBcMap(bcMap);
				reScheduleInfo.setCancelExistingSubSchedule(false);
				reScheduleInfo.setSupplementaryInfo(sGdsEvents.getSupplementaryInfo());
				reScheduleInfo.setActions(sGdsEvents.getActions());
				reScheduleInfo.setPublishedGdsIds(sGdsEvents.getExistingSchedule().getGdsIds());
				reScheduleInfo.setExistingSchedule(sGdsEvents.getExistingSchedule());
				reScheduleInfo.setUpdatedSchedule(sGdsEvents.getUpdatedSchedule()); //null
				reScheduleInfo.setExistingSubSchedules(null);
				reScheduleInfo.setUpdatedSubSchedules(null);
				reScheduleInfo.setCancelExistingSchedule(false);

				SchedulePublishBL.cancelOldScheduleAndPublishNewSchedule(reScheduleInfo);
			}
		} else {

			if (sGdsEvents.containsAction(GDSScheduleEventCollector.FLIGHT_NUMBER_CHANGED)) {
				if (sGdsEvents.getExistingSchedule().getBuildStatusCode()
						.equals(ScheduleBuildStatusEnum.BUILT.getCode())) {

					sendSSMFlightNumberChange(gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(),
							sGdsEvents.getUpdatedSchedule(), airLineCode, sGdsEvents.getExistingSchedule().getGdsIds(),
							bcMap, getUpdatedSsmSplitFlightSchedules(sGdsEvents), sGdsEvents.getSupplementaryInfo(),
							sGdsEvents.getExistingSchedule());
				}
			}

			// TODO: TimeMode should configurable - AARESAA-24335
			boolean isPeriodChanged = false;
			if (sGdsEvents.containsAction(GDSScheduleEventCollector.PERIOD_CHANGED_LOCAL_TIME)) {
				isPeriodChanged = true;
				// if
				// (sGdsEvents.containsAction(GDSScheduleEventCollector.PERIOD_CHANGED))
				// {
				// send REV - Revision of Period of Operation and/or Day(s)
				// of Operation
				ReScheduleRequiredInfo reScheduleInfo = new ReScheduleRequiredInfo();
				reScheduleInfo.addAction(GDSScheduleEventCollector.PERIOD_CHANGED_LOCAL_TIME);
				reScheduleInfo.setGdsStatusMap(gdsStatusMap);
				reScheduleInfo.setAircraftTypeMap(aircraftTypeMap);
				reScheduleInfo.setServiceType(sGdsEvents.getServiceType());
				reScheduleInfo.setAirLineCode(airLineCode);
				reScheduleInfo.setBcMap(bcMap);
				reScheduleInfo.setCancelExistingSubSchedule(false);
				reScheduleInfo.setSupplementaryInfo(sGdsEvents.getSupplementaryInfo());

				if (sGdsEvents.containsAction(GDSScheduleEventCollector.LEG_TIME_CHANGE)) {
					reScheduleInfo.addAction(GDSScheduleEventCollector.LEG_TIME_CHANGE);
				}

				if (ScheduleBuildStatusEnum.BUILT.getCode()
						.equals(sGdsEvents.getExistingSchedule().getBuildStatusCode())) {
					reScheduleInfo.setPublishedGdsIds(sGdsEvents.getExistingSchedule().getGdsIds());
					reScheduleInfo.setExistingSchedule(sGdsEvents.getExistingSchedule());
					reScheduleInfo.setUpdatedSchedule(sGdsEvents.getUpdatedSchedule());
					reScheduleInfo.setExistingSubSchedules(sGdsEvents.getExistingSubSchedules());
					reScheduleInfo.setUpdatedSubSchedules(sGdsEvents.getUpdatedSubSchedules());

					SchedulePublishBL.sendSSMForSchedulePeriodChanges(reScheduleInfo);

					notifyFlightInventory(FlightEventCode.FLIGHT_CREATED, sGdsEvents.getCreatedFlightIds(),
							sGdsEvents.getExistingSchedule().getGdsIds());

				}

				if (sGdsEvents.getExistingOverlapSchedule() != null && sGdsEvents.getExistingOverlapSchedule()
						.getBuildStatusCode().equals(ScheduleBuildStatusEnum.BUILT.getCode())) {
					reScheduleInfo.setPublishedGdsIds(sGdsEvents.getExistingOverlapSchedule().getGdsIds());
					reScheduleInfo.setExistingSchedule(sGdsEvents.getExistingOverlapSchedule());
					reScheduleInfo.setUpdatedSchedule(sGdsEvents.getUpdatedOverlapSchedule());
					reScheduleInfo.setExistingSubSchedules(null);
					reScheduleInfo.setUpdatedSubSchedules(null);

					SchedulePublishBL.sendSSMForSchedulePeriodChanges(reScheduleInfo);
				}
			}

			if (sGdsEvents.containsAction(GDSScheduleEventCollector.MODEL_CHANGED)
					&& !sGdsEvents.containsAction(GDSScheduleEventCollector.REPLACE_EXISTING_INFO)) {

				// cancel and create the existing and updated schedules
				if (sGdsEvents.getUpdatedSchedule().getBuildStatusCode()
						.equals(ScheduleBuildStatusEnum.BUILT.getCode())) {

					sendSSMModelChange(gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(),
							sGdsEvents.getUpdatedSchedule(), airLineCode, sGdsEvents.getExistingSchedule().getGdsIds(),
							bcMap, getUpdatedSsmSplitFlightSchedules(sGdsEvents), sGdsEvents.getSupplementaryInfo());
				}

				if (sGdsEvents.getUpdatedOverlapSchedule() != null && sGdsEvents.getUpdatedOverlapSchedule()
						.getBuildStatusCode().equals(ScheduleBuildStatusEnum.BUILT.getCode())) {

					sendSSMModelChange(gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(),
							sGdsEvents.getUpdatedOverlapSchedule(), airLineCode,
							sGdsEvents.getExistingOverlapSchedule().getGdsIds(), bcMap,
							sGdsEvents.getExistingOverlapSchedule().getSsmSplitFlightSchedule(),
							sGdsEvents.getSupplementaryInfo());
				}
			}

			if (sGdsEvents.containsAction(GDSScheduleEventCollector.LEG_ADDED_DELETED)
					|| sGdsEvents.containsAction(GDSScheduleEventCollector.REPLACE_EXISTING_INFO)) {

				if (sGdsEvents.getUpdatedSchedule().getBuildStatusCode()
						.equals(ScheduleBuildStatusEnum.BUILT.getCode())) {

					sendSSMLegsAddedDeleted(gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(),
							sGdsEvents.getUpdatedSchedule(), airLineCode, sGdsEvents.getExistingSchedule().getGdsIds(),
							bcMap, getUpdatedSsmSplitFlightSchedules(sGdsEvents), sGdsEvents.getSupplementaryInfo());
				}

				if (sGdsEvents.getUpdatedOverlapSchedule() != null && sGdsEvents.getUpdatedOverlapSchedule()
						.getBuildStatusCode().equals(ScheduleBuildStatusEnum.BUILT.getCode())) {

					sendSSMLegsAddedDeleted(gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(),
							sGdsEvents.getUpdatedOverlapSchedule(), airLineCode,
							sGdsEvents.getExistingOverlapSchedule().getGdsIds(), bcMap,
							sGdsEvents.getExistingOverlapSchedule().getSsmSplitFlightSchedule(),
							sGdsEvents.getSupplementaryInfo());
				}

			} else if (sGdsEvents.containsAction(GDSScheduleEventCollector.LEG_TIME_CHANGE) && !isPeriodChanged) {

				if (sGdsEvents.getUpdatedSchedule().getBuildStatusCode()
						.equals(ScheduleBuildStatusEnum.BUILT.getCode())) {

					ReScheduleRequiredInfo reScheduleInfo = new ReScheduleRequiredInfo();
					reScheduleInfo.addAction(GDSScheduleEventCollector.LEG_TIME_CHANGE);
					reScheduleInfo.setGdsStatusMap(gdsStatusMap);
					reScheduleInfo.setAircraftTypeMap(aircraftTypeMap);
					reScheduleInfo.setServiceType(sGdsEvents.getServiceType());
					reScheduleInfo.setAirLineCode(airLineCode);
					reScheduleInfo.setBcMap(bcMap);
					reScheduleInfo.setCancelExistingSubSchedule(false);

					reScheduleInfo.setPublishedGdsIds(sGdsEvents.getExistingSchedule().getGdsIds());
					reScheduleInfo.setExistingSchedule(sGdsEvents.getExistingSchedule());
					reScheduleInfo.setUpdatedSchedule(sGdsEvents.getUpdatedSchedule());
					reScheduleInfo.setExistingSubSchedules(sGdsEvents.getExistingSubSchedules());
					reScheduleInfo.setUpdatedSubSchedules(sGdsEvents.getUpdatedSubSchedules());
					reScheduleInfo.setSupplementaryInfo(sGdsEvents.getSupplementaryInfo());

					SchedulePublishBL.sendSSMForSchedulePeriodChanges(reScheduleInfo);

				}

				if (sGdsEvents.getUpdatedOverlapSchedule() != null && sGdsEvents.getUpdatedOverlapSchedule()
						.getBuildStatusCode().equals(ScheduleBuildStatusEnum.BUILT.getCode())) {

					sendSSMLegsTimeChange(gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(),
							sGdsEvents.getUpdatedOverlapSchedule(), airLineCode,
							sGdsEvents.getExistingOverlapSchedule().getGdsIds(), bcMap,
							sGdsEvents.getExistingOverlapSchedule().getSsmSplitFlightSchedule(),
							sGdsEvents.getSupplementaryInfo());
				}
			}

		}

		// constructing response
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		// return command response
		return responce;
	}

	@SuppressWarnings("rawtypes")
	private void sendSSMModelChange(Map gdsStatusMap, Map aircraftTypeMap, String serviceType, FlightSchedule schedule,
			String airLineCode, Collection updatedList, Map bcMap, Collection<SSMSplitFlightSchedule> scheduleColl,
			String supplementaryInfo) throws ModuleException {

		if (scheduleColl != null && !scheduleColl.isEmpty()) {
			for (SSMSplitFlightSchedule subSchedule : scheduleColl) {
				SchedulePublishBL.generateSSMAircraftModelChange(gdsStatusMap, aircraftTypeMap, serviceType, schedule,
						airLineCode, updatedList, bcMap, subSchedule, supplementaryInfo);
			}
		} else {
			SchedulePublishBL.generateSSMAircraftModelChange(gdsStatusMap, aircraftTypeMap, serviceType, schedule,
					airLineCode, updatedList, bcMap, null, supplementaryInfo);
		}

	}

	private void sendSSMLegsAddedDeleted(Map gdsStatusMap, Map aircraftTypeMap, String serviceType,
			FlightSchedule schedule, String airLineCode, Collection updatedList, Map bcMap,
			Collection<SSMSplitFlightSchedule> scheduleColl, String supplementaryInfo) throws ModuleException {

		if (scheduleColl != null && !scheduleColl.isEmpty()) {
			for (SSMSplitFlightSchedule subSchedule : scheduleColl) {
				SchedulePublishBL.generateSSMReplacementMsg(gdsStatusMap, aircraftTypeMap, serviceType, schedule,
						airLineCode, updatedList, bcMap, subSchedule, supplementaryInfo);
			}
		} else {
			SchedulePublishBL.generateSSMReplacementMsg(gdsStatusMap, aircraftTypeMap, serviceType, schedule,
					airLineCode, updatedList, bcMap, null, supplementaryInfo);
		}

	}

	private void sendSSMLegsTimeChange(Map gdsStatusMap, Map aircraftTypeMap, String serviceType,
			FlightSchedule schedulex, String airLineCode, Collection updatedList, Map bcMap,
			Collection<SSMSplitFlightSchedule> scheduleColl, String supplementaryInfo) throws ModuleException {

		if (scheduleColl != null && !scheduleColl.isEmpty()) {
			for (SSMSplitFlightSchedule subSchedule : scheduleColl) {
				SchedulePublishBL.generateSSMTimeChange(gdsStatusMap, aircraftTypeMap, serviceType, schedulex,
						airLineCode, updatedList, bcMap, subSchedule, supplementaryInfo);
			}
		} else {
			SchedulePublishBL.generateSSMTimeChange(gdsStatusMap, aircraftTypeMap, serviceType, schedulex, airLineCode,
					updatedList, bcMap, null, supplementaryInfo);
		}

	}

	private void sendSSMFlightNumberChange(Map gdsStatusMap, Map aircraftTypeMap, String serviceType,
			FlightSchedule updatedSchedule, String airLineCode, Collection updatedList, Map bcMap,
			Collection<SSMSplitFlightSchedule> scheduleColl, String supplementaryInfo, FlightSchedule existingSchedule)
			throws ModuleException {
		if (scheduleColl != null && !scheduleColl.isEmpty()) {
			for (SSMSplitFlightSchedule schedule : scheduleColl) {
				SchedulePublishBL.generateSSMFlightNumberChange(gdsStatusMap, aircraftTypeMap, serviceType,
						updatedSchedule, airLineCode, updatedList, bcMap, schedule, supplementaryInfo,
						existingSchedule);
			}
		} else {
			SchedulePublishBL.generateSSMFlightNumberChange(gdsStatusMap, aircraftTypeMap, serviceType, updatedSchedule,
					airLineCode, updatedList, bcMap, null, supplementaryInfo, existingSchedule);
		}
	}

	private void notifyFlightInventory(FlightEventCode eventcode, Collection<Integer> flightIds,
			Collection<Integer> gdsIDs) throws ModuleException {

		if (flightIds != null && !flightIds.isEmpty() && gdsIDs != null && !gdsIDs.isEmpty()) {

			// Notify flight events to inventory to send corresponding avs
			// messages
			NotifyFlightEventsRQ notifyFlightEventsRQ = new NotifyFlightEventsRQ();
			NotifyFlightEventRQ newFlightEnvent = new NotifyFlightEventRQ();
			newFlightEnvent.setFlightEventCode(eventcode);
			newFlightEnvent.addFlightIds(flightIds);
			newFlightEnvent.setGdsIdsAdded(gdsIDs);

			notifyFlightEventsRQ.addNotifyFlightEventRQ(newFlightEnvent);
			flightInventoryBD.notifyFlightEvent(notifyFlightEventsRQ);
		}

	}

	private Set<SSMSplitFlightSchedule> getUpdatedSsmSplitFlightSchedules(GDSScheduleEventCollector sGdsEvents) {
		Set<SSMSplitFlightSchedule> ssmSplitFlightSchedule = sGdsEvents.getExistingSchedule()
				.getSsmSplitFlightSchedule();
		if (sGdsEvents.containsAction(GDSScheduleEventCollector.PERIOD_CHANGED_LOCAL_TIME)
				&& sGdsEvents.getUpdatedSubSchedules() != null && !sGdsEvents.getUpdatedSubSchedules().isEmpty()) {
			ssmSplitFlightSchedule = new HashSet<SSMSplitFlightSchedule>(sGdsEvents.getUpdatedSubSchedules());
		}
		return ssmSplitFlightSchedule;
	}

	private boolean isEligibleToSendSubMessages(Collection<SSMSplitFlightSchedule> existingSubSchedules,
			Collection<String> actions) {
		boolean isSendSubMessage = false;
		if (actions != null && AppSysParamsUtil.isEnableSendSsmAsmSubMessages()
				&& (!AppSysParamsUtil.isEnableSplitScheduleWhenDSTApplicable()
						&& (existingSubSchedules == null || existingSubSchedules.isEmpty()))) {

			if (actions.contains(GDSScheduleEventCollector.PERIOD_CHANGED_LOCAL_TIME)
					&& (actions.contains(GDSScheduleEventCollector.MODEL_CHANGED)
							|| actions.contains(GDSScheduleEventCollector.REPLACE_EXISTING_INFO)
							|| actions.contains(GDSScheduleEventCollector.LEG_TIME_CHANGE)
							|| actions.contains(GDSScheduleEventCollector.FLIGHT_NUMBER_CHANGED))) {
				isSendSubMessage = true;
			} else if (actions.contains(GDSScheduleEventCollector.FLIGHT_NUMBER_CHANGED)
					&& (actions.contains(GDSScheduleEventCollector.MODEL_CHANGED)
							|| actions.contains(GDSScheduleEventCollector.REPLACE_EXISTING_INFO)
							|| actions.contains(GDSScheduleEventCollector.LEG_TIME_CHANGE)
							|| actions.contains(GDSScheduleEventCollector.PERIOD_CHANGED_LOCAL_TIME))) {
				isSendSubMessage = true;
			} else if (actions.contains(GDSScheduleEventCollector.REPLACE_EXISTING_INFO)
					&& (actions.contains(GDSScheduleEventCollector.FLIGHT_NUMBER_CHANGED)
							|| actions.contains(GDSScheduleEventCollector.LEG_TIME_CHANGE)
							|| actions.contains(GDSScheduleEventCollector.PERIOD_CHANGED_LOCAL_TIME))) {
				isSendSubMessage = true;
			}

		}

		return isSendSubMessage;
	}

	private boolean isCancelOldSchedule(Collection<String> actions) {
		if (actions != null && actions.contains(GDSScheduleEventCollector.PERIOD_CHANGED_LOCAL_TIME)
				&& (actions.contains(GDSScheduleEventCollector.LEG_TIME_CHANGE)
						|| actions.contains(GDSScheduleEventCollector.FLIGHT_NUMBER_CHANGED)
						|| actions.contains(GDSScheduleEventCollector.LEG_ADDED_DELETED))) {

			return true;
		}

		return false;
	}

	private boolean isCancelAndComposeNew(Collection<String> actions) {

		boolean cancelAndSendNew = false;
		if (actions != null) {
			if (actions.contains(GDSScheduleEventCollector.PERIOD_CHANGED_LOCAL_TIME)
					&& (actions.contains(GDSScheduleEventCollector.MODEL_CHANGED)
							|| actions.contains(GDSScheduleEventCollector.REPLACE_EXISTING_INFO)
							|| actions.contains(GDSScheduleEventCollector.LEG_TIME_CHANGE)
							|| actions.contains(GDSScheduleEventCollector.FLIGHT_NUMBER_CHANGED))) {
				cancelAndSendNew = true;
			} else if (actions.contains(GDSScheduleEventCollector.FLIGHT_NUMBER_CHANGED)
					&& (actions.contains(GDSScheduleEventCollector.MODEL_CHANGED)
							|| actions.contains(GDSScheduleEventCollector.REPLACE_EXISTING_INFO)
							|| actions.contains(GDSScheduleEventCollector.LEG_TIME_CHANGE)
							|| actions.contains(GDSScheduleEventCollector.PERIOD_CHANGED_LOCAL_TIME))) {
				cancelAndSendNew = true;
			}
		}

		return cancelAndSendNew;
	}
}
