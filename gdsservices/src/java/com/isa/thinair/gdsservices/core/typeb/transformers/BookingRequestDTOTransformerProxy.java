package com.isa.thinair.gdsservices.core.typeb.transformers;

import java.util.LinkedHashMap;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.MessageIdentifier;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

public class BookingRequestDTOTransformerProxy {

	/**
	 * Transforms to BookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * 
	 * @param reservationRequest
	 * @return
	 */
	public static BookingRequestDTO transform(
			LinkedHashMap<GDSInternalCodes.ReservationAction, GDSReservationRequestBase> reservationRequestMap,
			BookingRequestDTO bookingRequestDTO,
			LinkedHashMap<ReservationAction, GDSReservationRequestBase> reservationResponseMap) {

		String messageIdentifier = bookingRequestDTO.getMessageIdentifier();

		if (messageIdentifier.equals(MessageIdentifier.LOCATE_RESERVATION.getCode())) {
			bookingRequestDTO = LocateReservationResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
		} else if (messageIdentifier.equals(MessageIdentifier.PDM.getCode())) {
			bookingRequestDTO = LocateReservationResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
		} else if (messageIdentifier.equals(MessageIdentifier.NEW_BOOKING.getCode())) {
			bookingRequestDTO = CreateResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
		} else if (messageIdentifier.equals(MessageIdentifier.CANCEL_BOOKING.getCode())) {
			bookingRequestDTO = CancelReservationResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
		} else if (messageIdentifier.equals(MessageIdentifier.NEW_CONTINUATION.getCode())
				|| messageIdentifier.equals(MessageIdentifier.NEW_ARRIVAL.getCode())
				|| messageIdentifier.equals(MessageIdentifier.ARRIVAL_UNKNOWN.getCode())) {
			bookingRequestDTO = UpdateExternalSegmentsResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
		} else if (messageIdentifier.equals(MessageIdentifier.AMEND_BOOKING.getCode())) {
			bookingRequestDTO = ModifyReservationResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
		} else if (messageIdentifier.equals(MessageIdentifier.DEVIDE_BOOKING.getCode())) {
			bookingRequestDTO = SplitReservationResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
		} else if (messageIdentifier.equals(MessageIdentifier.STATUS_REPLY.getCode())) {
			bookingRequestDTO = UpdateStatusResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
		} else if (messageIdentifier.equals(MessageIdentifier.TICKETING.getCode())) {
			bookingRequestDTO = UpdateStatusResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
		} else if (messageIdentifier.equals(MessageIdentifier.SCHEDULE_CHANGE.getCode())) {
			bookingRequestDTO = NoResponseOnSuccessTransformer.getResponse(bookingRequestDTO, ReservationAction.TRANSFER_SEGMENT,
					reservationResponseMap);
		} else if (messageIdentifier.equals(MessageIdentifier.DIVIDE_RESP.getCode())) {
			bookingRequestDTO = NoResponseOnSuccessTransformer.getResponse(bookingRequestDTO, ReservationAction.SPLIT_RESERVATION_RESP,
					reservationResponseMap);
		} else if (messageIdentifier.equals(MessageIdentifier.LINK_RECORD_LOCATOR.getCode())) {
			bookingRequestDTO = NoResponseOnSuccessTransformer.getResponse(bookingRequestDTO, ReservationAction.CREATE_BOOKING_RESP,
					reservationResponseMap);
		} else if (messageIdentifier.equals(MessageIdentifier.RESERVATION_RESP.getCode())) {
			bookingRequestDTO = NoResponseOnSuccessTransformer.getResponse(bookingRequestDTO, ReservationAction.RESERVATION_RESP,
					reservationResponseMap);
		}

		return bookingRequestDTO;
	}
}
