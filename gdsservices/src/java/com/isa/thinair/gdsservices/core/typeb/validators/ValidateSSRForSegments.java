package com.isa.thinair.gdsservices.core.typeb.validators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airmaster.api.dto.SpecialServiceRequestDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDetailDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRInfantDTO;
import com.isa.thinair.gdsservices.api.dto.external.SegmentDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ActionCode;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

/**
 * @author Nilindra Fernando
 */
public class ValidateSSRForSegments extends ValidatorBase {

	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {
		List<SSRDTO> ssrDTOs = bookingRequestDTO.getSsrDTOs();

		Map<String, Collection<SegmentDTO>> mapSSRSegmentDTO = getSSRWiseSegmentDTOs(ssrDTOs);
		return checkSSRSegmentsWithBookingSegments(mapSSRSegmentDTO, bookingRequestDTO);
	}

	private BookingRequestDTO checkSSRSegmentsWithBookingSegments(Map<String, Collection<SegmentDTO>> mapSSRSegmentDTO,
			BookingRequestDTO bookingRequestDTO) {

		if (mapSSRSegmentDTO.size() > 0) {
			if (isSSRSegmentsAreBookingSegmentsCompliant(mapSSRSegmentDTO, bookingRequestDTO)) {
				if (!isRequestedSSRAvailable(bookingRequestDTO, mapSSRSegmentDTO)) {
					String message = MessageUtil.getMessage("gdsservices.validators.unsupported.ssr.actionstatus.codes");
					bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);
					this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
					return bookingRequestDTO;
				}
				this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
				return bookingRequestDTO;
			} else {
				String message = MessageUtil.getMessage("gdsservices.validators.ssrsegments.must.equals.to.itinerarysegments");
				bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

				this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
				return bookingRequestDTO;
			}
		} else {
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
			return bookingRequestDTO;
		}
	}

	private boolean isRequestedSSRAvailable(BookingRequestDTO bookingRequestDTO,
			Map<String, Collection<SegmentDTO>> mapSSRSegmentDTO) {

		boolean status = true;
		Map<String, List<String>> segmentWiseSSR = new HashMap<String, List<String>>();
		for (BookingSegmentDTO segDTO : bookingRequestDTO.getBookingSegmentDTOs()) {
			String origin = segDTO.getDepartureStation();
			String destination = segDTO.getDestinationStation();
			String flightNo = segDTO.getCarrierCode() + segDTO.getFlightNumber();
			Date departureDate = segDTO.getDepartureDate();
			Collection<FlightSegement> flightSegments;
			try {
				flightSegments = GDSServicesModuleUtil.getFlightBD().getFlightSegmentsForLocalDate(origin, destination, flightNo,
						departureDate, false);
				Map<Integer, ClassOfServiceDTO> flightSegCosMap = new HashMap<Integer, ClassOfServiceDTO>();
				ClassOfServiceDTO cos = new ClassOfServiceDTO();
				//TODO: Set cabin class here
				cos.setCabinClassCode(segDTO.getBookingCode());
				cos.setLogicalCCCode(segDTO.getBookingCode());
				Integer salesChannel = new Integer(ReservationInternalConstants.SalesChannel.GDS);

				for (FlightSegement seg : flightSegments) {
					cos.setSegmentCode(seg.getSegmentCode());
					flightSegCosMap.put(seg.getFltSegId(),cos);
				}				
			
				Map<Integer, List<SpecialServiceRequestDTO>> availableSSRs = GDSServicesModuleUtil.getSsrServiceBD()
						.getAvailableSSRs(flightSegCosMap, salesChannel.toString(), AppSysParamsUtil.isInventoryCheckForSSREnabled(),
								false, false);
				for (Entry<Integer, List<SpecialServiceRequestDTO>> entry : availableSSRs.entrySet()) {
					for (SpecialServiceRequestDTO ssr : entry.getValue()) {
						if (segmentWiseSSR.get(ssr.getOndCode()) == null) {
							segmentWiseSSR.put(ssr.getOndCode(), new ArrayList<String>());
						}
						if (!segmentWiseSSR.get(ssr.getOndCode()).contains(ssr.getSsrCode())) {
							segmentWiseSSR.get(ssr.getOndCode()).add(ssr.getSsrCode());
						}
					}
				}
			} catch (ModuleException e) {
				status = false;
			}
		}
		String ond = "";
		String codeSSR = "";
		if (status) {
			for (SSRDTO ssr : bookingRequestDTO.getSsrDTOs()) {
				codeSSR = ssr.getCodeSSR();
				if (ssr instanceof SSRDetailDTO
						&& (!codeSSR.equals(GDSExternalCodes.SSRCodes.ETICKET_NO.getCode()) && !codeSSR
								.equals(GDSExternalCodes.SSRCodes.ET_NOT_CHANGED.getCode()))) {
					Collection<SegmentDTO> segments = mapSSRSegmentDTO.get(ssr.getCodeSSR());
					if (segments != null) {
						for (SegmentDTO seg : segments) {
							ond = seg.getDepartureStation() + "/" + seg.getDestinationStation();
							if (segmentWiseSSR.get(ond) != null && !segmentWiseSSR.get(ond).contains(codeSSR)) {
								status = false;
							}
						}
					}
				}
			}
		}

		return status;
	}

	private boolean isSSRSegmentsAreBookingSegmentsCompliant(Map<String, Collection<SegmentDTO>> mapSSRSegmentDTO,
			BookingRequestDTO bookingRequestDTO) {
		List<BookingSegmentDTO> lstBookingSegmentDTO = getInternalSegments(bookingRequestDTO);

		for (String ssrCode : mapSSRSegmentDTO.keySet()) {
			Collection<SegmentDTO> segments = mapSSRSegmentDTO.get(ssrCode);
			boolean status = isSameSegments(segments, lstBookingSegmentDTO);

			if (!status) {
				return false;
			}
		}

		return true;
	}

	private List<BookingSegmentDTO> getInternalSegments(BookingRequestDTO bookingRequestDTO) {
		List<BookingSegmentDTO> bookingSegmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();
		List<BookingSegmentDTO> internalSegments = new ArrayList<BookingSegmentDTO>();
		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

		for (BookingSegmentDTO bookingSegmentDTO : bookingSegmentDTOs) {
			if (carrierCode.equals(bookingSegmentDTO.getCarrierCode())
					&& !bookingSegmentDTO.getActionOrStatusCode().equals(ActionCode.CANCEL.getCode())) {
				internalSegments.add(bookingSegmentDTO);
			}
		}

		return internalSegments;
	}

	private boolean isSameSegments(Collection<SegmentDTO> segments, List<BookingSegmentDTO> lstBookingSegmentDTO) {
		// SSR can be added to only one segment. No need for all segments. So, segment sizes can be unequal
		// if (segments.size() == lstBookingSegmentDTO.size()) {
		// return compareWhenSizesAreEqual(segments, lstBookingSegmentDTO);
		// } else {
		// return false;
		// }
		return checkSSRSegmentExists(segments, lstBookingSegmentDTO);
	}

	private boolean compareWhenSizesAreEqual(Collection<SegmentDTO> segments, List<BookingSegmentDTO> lstBookingSegmentDTO) {
		int i = 0;
		for (BookingSegmentDTO bookingSegmentDTO : lstBookingSegmentDTO) {

			for (SegmentDTO segmentDTO : segments) {
				if (GDSApiUtils.isEqual(bookingSegmentDTO, segmentDTO)) {
					i++;
				}
			}
		}

		if (i == lstBookingSegmentDTO.size()) {
			return true;
		} else {
			return false;
		}
	}
	
	private boolean checkSSRSegmentExists(Collection<SegmentDTO> segments, List<BookingSegmentDTO> lstBookingSegmentDTO) {
		int i = 0;

		for (SegmentDTO segmentDTO : segments) {
			for (BookingSegmentDTO bookingSegmentDTO : lstBookingSegmentDTO) {
				if (GDSApiUtils.isEqual(bookingSegmentDTO, segmentDTO)) {
					i++;
				}
			}
		}

		if (i == segments.size()) {
			return true;
		} else {
			return false;
		}
	}

	private Map<String, Collection<SegmentDTO>> getSSRWiseSegmentDTOs(List<SSRDTO> ssrDTOs) {
		Map<String, Collection<SegmentDTO>> mapSSRSegmentDTO = new HashMap<String, Collection<SegmentDTO>>();
		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

		if (ssrDTOs != null && ssrDTOs.size() > 0) {
			for (SSRDTO ssrdto : ssrDTOs) {
				if (carrierCode.equals(ssrdto.getCarrierCode())) {
					if (!ssrdto.getCodeSSR().equals(GDSExternalCodes.SSRCodes.ETICKET_NO.getCode())) {
						if (ssrdto instanceof SSRInfantDTO) {
							SSRInfantDTO ssrInfantDTO = (SSRInfantDTO) ssrdto;
							if (!ssrInfantDTO.isNameChangeRelated()) {
								addSegments(mapSSRSegmentDTO,
										ssrdto.getCodeSSR() + ssrInfantDTO.getIATAGuardianName() + ssrInfantDTO.getIATAInfantName(),
										ssrInfantDTO.getSegmentDTO());
							}
						} else if (ssrdto instanceof SSRDetailDTO) {
							addSegments(mapSSRSegmentDTO, ssrdto.getCodeSSR(), ((SSRDetailDTO) ssrdto).getSegmentDTO());
						}
					}
				}
			}
		}

		return mapSSRSegmentDTO;
	}

	private void addSegments(Map<String, Collection<SegmentDTO>> mapSegments, String codeSSR, SegmentDTO segmentDTO) {
		if (mapSegments.containsKey(codeSSR)) {
			Collection<SegmentDTO> segments = mapSegments.get(codeSSR);
			segments.add(segmentDTO);
		} else {
			Collection<SegmentDTO> segments = new ArrayList<SegmentDTO>();
			segments.add(segmentDTO);
			mapSegments.put(codeSSR, segments);
		}
	}
}
