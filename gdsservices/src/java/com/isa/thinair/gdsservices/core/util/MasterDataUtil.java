/**
 * 
 */
package com.isa.thinair.gdsservices.core.util;

import java.util.Collection;

import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSServicesSupportJDBCDAO;

/**
 * @author Sudheera
 * 
 */
public class MasterDataUtil {
	private static Collection<String> titles;

	/**
	 * get titles
	 * 
	 * @return
	 */
	public static Collection<String> getTitles() {
		if (titles == null) {
			loadTitles();
		}

		return titles;
	}

	/**
	 * loads titles
	 */
	private synchronized static void loadTitles() {
		if (titles != null)
			return;

		titles = getGDSServicesSupportDAO().getTitles();
	}

	/**
	 * gets GDSServicesSupportDAO
	 * 
	 * @return
	 */
	private static GDSServicesSupportJDBCDAO getGDSServicesSupportDAO() {
		return GDSServicesDAOUtils.DAOInstance.GDSSERVICES_SUPPORT_JDBC_DAO;
	}
}
