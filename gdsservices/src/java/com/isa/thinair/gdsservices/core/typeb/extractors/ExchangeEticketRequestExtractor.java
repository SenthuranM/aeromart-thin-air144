package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDetailDTO;
import com.isa.thinair.gdsservices.api.dto.internal.ExchangeEticketRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.internal.SSRData;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceDetailRequest;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

/**
 * @author Manoj
 */
public class ExchangeEticketRequestExtractor extends ValidatorBase {
	
	private static final Log log = LogFactory.getLog(ExchangeEticketRequestExtractor.class);

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {

		String pnrReference = ValidatorUtils.getPNRReference(bookingRequestDTO.getResponderRecordLocator());

		if (pnrReference.length() == 0) {
			String message = MessageUtil.getMessage("gdsservices.extractors.emptyPNRResponder");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);
			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		} else {
			boolean isETicketNumberExists = checkETicketNumberExists(bookingRequestDTO.getSsrDTOs());

			if (isETicketNumberExists) {
				this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
				return bookingRequestDTO;
			} else {
				String message = MessageUtil.getMessage("gdsservices.extractors.empty.eticket.number");
				bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

				this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
				return bookingRequestDTO;
			}
		}
	}
	
	private boolean checkETicketNumberExists(List<SSRDTO> ssrDTOs) {
		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

		for (SSRDTO ssrdto : ssrDTOs) {
			if (carrierCode.equals(ssrdto.getCarrierCode())
					&& GDSExternalCodes.SSRCodes.ET_NOT_CHANGED.getCode().equals(ssrdto.getCodeSSR())) {
				String eTicketNo = GDSApiUtils.maskNull(ssrdto.getSsrValue());

				if (eTicketNo.length() > 0) {
					return true;
				}
			}
		}

		return false;
	}

	public static ExchangeEticketRequest getRequest(GDSCredentialsDTO gdsCredentialsDTO, SSRData ssrData,
			BookingRequestDTO bookingRequestDTO) {
		ExchangeEticketRequest exchangeEticketRequest = new ExchangeEticketRequest();

		exchangeEticketRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
		exchangeEticketRequest = (ExchangeEticketRequest) RequestExtractorUtil
				.addBaseAttributes(exchangeEticketRequest, bookingRequestDTO);

		exchangeEticketRequest = addPassengersAndSSRs(exchangeEticketRequest, ssrData);
		exchangeEticketRequest.setNotSupportedSSRExists(bookingRequestDTO.isNotSupportedSSRExists());

		return exchangeEticketRequest;
	}
	
	private static ExchangeEticketRequest addPassengersAndSSRs(ExchangeEticketRequest exchangeEticketRequest, SSRData ssrData) {

		Map<String, Passenger> resPaxMap = new HashMap<String, Passenger>();
		Collection<SSRDetailDTO> ssrDetailDTOs = ssrData.getDetailSSRDTOs();

		// add SSRDetailDTOs to paxs
		for (Iterator<SSRDetailDTO> iterator = ssrDetailDTOs.iterator(); iterator.hasNext();) {
			SSRDetailDTO ssrDetailDTO = (SSRDetailDTO) iterator.next();

			if (GDSExternalCodes.SSRCodes.ET_NOT_CHANGED.getCode().equals(ssrDetailDTO.getCodeSSR())) {
				List<NameDTO> nameDTOs = ssrDetailDTO.getNameDTOs();
				String gdsActionCode = ssrDetailDTO.getActionOrStatusCode();

				if (gdsActionCode.equals(GDSExternalCodes.ActionCode.EXCHANGE_RECOMMENDED.getCode())) {
					
					if (nameDTOs != null && nameDTOs.size() > 0) {
						for (Iterator<NameDTO> iterNameDTO = nameDTOs.iterator(); iterNameDTO.hasNext();) {
							NameDTO nameDTO = (NameDTO) iterNameDTO.next();
							SpecialServiceDetailRequest ssr = new SpecialServiceDetailRequest();
							ssr = SSRExtractorUtil.addSSRDetailAttributes(ssr, ssrDetailDTO);
							String iataPaxName = nameDTO.getIATAName();
							Passenger resPax = resPaxMap.get(iataPaxName);

							if (resPax == null) {
								resPax = new Passenger();
								resPax = RequestExtractorUtil.getPassenger(resPax, nameDTO);
								resPaxMap.put(iataPaxName, resPax);
							}

							resPax.getSsrCollection().add(ssr);
							exchangeEticketRequest.getPassengers().add(resPax);
						}
					} else {
						// add to common SSRS
						SpecialServiceDetailRequest ssr = new SpecialServiceDetailRequest();
						ssr = SSRExtractorUtil.addSSRDetailAttributes(ssr, ssrDetailDTO);
						exchangeEticketRequest.getCommonSSRs().add(ssr);
					}
				}
			}
		}
		return exchangeEticketRequest;
	}
}
