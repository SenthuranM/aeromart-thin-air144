package com.isa.thinair.gdsservices.core.bl.typeA.ticketcontrol;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.gdsservices.api.dto.typea.init.InitBaseRq;
import com.isa.thinair.gdsservices.api.model.GdsEvent;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageInitiator;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @isa.module.command name="commonTicketControlInitiator"
 */
public class CommonTicketControlInitiator extends DefaultBaseCommand {

	public ServiceResponce execute() throws ModuleException {

		InitBaseRq initBaseRq = (InitBaseRq) getParameter(TypeACommandParamNames.TICKET_CONTROL_DTO);


		EdiMessageInitiator initiator = getTicketControlInitiator(initBaseRq.getRequestType());
		for (String paramName : getParameterNames()) {
			initiator.setParameter(paramName, getParameter(paramName));
		}
		ServiceResponce resp = initiator.execute();

		return resp;

	}

	private EdiMessageInitiator getTicketControlInitiator(GdsEvent.Request gdsEvent) {
		switch (gdsEvent) {
		case CHANGE_OF_STATUS_TO_FINAL:
		case TICKET_REISSUE:
			return new InitiateChangeOfStatus();
		case RECALL_CONTROL:
	   	    return new InitiateRecallControl();
		case TICKET_DISPLAY:
			return new InitiateTicketDisplay();
		}

		throw new UnsupportedOperationException("An unsupported event occurred");
	}
}
