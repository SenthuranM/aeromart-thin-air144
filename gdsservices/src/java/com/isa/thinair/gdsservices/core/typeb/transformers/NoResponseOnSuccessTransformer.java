package com.isa.thinair.gdsservices.core.typeb.transformers;

import java.util.Map;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;

public class NoResponseOnSuccessTransformer {
	public static BookingRequestDTO getResponse(BookingRequestDTO bookingRequestDTO, GDSInternalCodes.ReservationAction action,
	                                            Map<GDSInternalCodes.ReservationAction, GDSReservationRequestBase> reservationResponseMap) {

		GDSReservationRequestBase request = reservationResponseMap.get(action);
		if (request == null && action.equals(GDSInternalCodes.ReservationAction.TRANSFER_SEGMENT)
				&& reservationResponseMap.keySet().contains(GDSInternalCodes.ReservationAction.CANCEL_SEGMENT)) {
			request = reservationResponseMap.get(GDSInternalCodes.ReservationAction.CANCEL_SEGMENT);
		} else if (request == null && action.equals(GDSInternalCodes.ReservationAction.TRANSFER_SEGMENT)
				&& reservationResponseMap.keySet().contains(GDSInternalCodes.ReservationAction.ADD_SEGMENT)) {
			request = reservationResponseMap.get(GDSInternalCodes.ReservationAction.ADD_SEGMENT);
		} else if (request == null && action.equals(GDSInternalCodes.ReservationAction.TRANSFER_SEGMENT)
				&& reservationResponseMap.keySet().contains(GDSInternalCodes.ReservationAction.CANCEL_RESERVATION)) {
			request = reservationResponseMap.get(GDSInternalCodes.ReservationAction.CANCEL_RESERVATION);
		} else if (request == null && action.equals(GDSInternalCodes.ReservationAction.TRANSFER_SEGMENT)
				&& reservationResponseMap.keySet().contains(GDSInternalCodes.ReservationAction.UPDATE_EXTERNAL_SEGMENTS)) {
			request = reservationResponseMap.get(GDSInternalCodes.ReservationAction.UPDATE_EXTERNAL_SEGMENTS);
		}

		if (request.isSuccess()) {
			bookingRequestDTO.setProcessStatus(GDSExternalCodes.ProcessStatus.INVOCATION_SUCCESS_IGNORE_NOTIFICATION);

		} else {
			bookingRequestDTO.setProcessStatus(GDSExternalCodes.ProcessStatus.INVOCATION_FAILURE);
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, request);
			bookingRequestDTO.setResponseMessageIdentifier(null);
		}
		bookingRequestDTO.getErrors().addAll(request.getErrorCode());


		return bookingRequestDTO;
	}
}
