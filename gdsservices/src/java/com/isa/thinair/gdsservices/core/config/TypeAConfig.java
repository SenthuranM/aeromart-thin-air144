package com.isa.thinair.gdsservices.core.config;

import java.util.Map;

import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;

public class TypeAConfig {

	private Map<GDSExternalCodes.GDS, TypeAFunctionConfig> functions;
	private Map<GDSExternalCodes.GDS, Map<IATAOperation, String>> versions;

	public Map<GDSExternalCodes.GDS, TypeAFunctionConfig> getFunctions() {
		return functions;
	}

	public void setFunctions(Map<GDSExternalCodes.GDS, TypeAFunctionConfig> functions) {
		this.functions = functions;
	}

	public Map<GDSExternalCodes.GDS, Map<IATAOperation, String>> getVersions() {
		return versions;
	}

	public void setVersions(Map<GDSExternalCodes.GDS, Map<IATAOperation, String>> versions) {
		this.versions = versions;
	}

	public static class TypeAFunctionConfig {
		private GDSExternalCodes.GDS gdsCode;
		private Map<Category, Map<Function, ? extends FunctionConfig>> messageConfig;

		public GDSExternalCodes.GDS getGdsCode() {
			return gdsCode;
		}

		public void setGdsCode(GDSExternalCodes.GDS gdsCode) {
			this.gdsCode = gdsCode;
		}

		public Map<Category, Map<Function, ? extends FunctionConfig>> getMessageConfig() {
			return messageConfig;
		}

		public void setMessageConfig(Map<Category, Map<Function, ? extends FunctionConfig>> messageConfig) {
			this.messageConfig = messageConfig;
		}
	}

	public static class FunctionConfig {
		private boolean enabled;

		public boolean isEnabled() {
			return enabled;
		}

		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}
	}

	public enum Category {
		TICKET,
		TICKET_CONTROL
	}

	public enum Function {
		REFUND,
		VOID,
        HANDOVER_AIRPORT_CONTROL,
		OBTAIN_AIRPORT_CONTROL
	}

}
