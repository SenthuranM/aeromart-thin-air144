package com.isa.thinair.gdsservices.core.typeA.transformers.itareqres.v03;

import static com.isa.thinair.gdsservices.core.util.TypeADataConvertorUtill.getDate;
import static com.isa.thinair.gdsservices.core.util.TypeADataConvertorUtill.getValue;
import static com.isa.thinair.gdsservices.core.util.TypeADataConvertorUtill.setValue;
import iata.typea.v031.itareq.EQN;
import iata.typea.v031.itareq.FTI;
import iata.typea.v031.itareq.GROUP2;
import iata.typea.v031.itareq.GROUP3;
import iata.typea.v031.itareq.GROUP4;
import iata.typea.v031.itareq.ITAREQ;
import iata.typea.v031.itareq.MSG;
import iata.typea.v031.itareq.ODI;
import iata.typea.v031.itareq.ORG;
import iata.typea.v031.itareq.RCI;
import iata.typea.v031.itareq.RPI;
import iata.typea.v031.itareq.SDT;
import iata.typea.v031.itareq.SSR;
import iata.typea.v031.itareq.TIF;
import iata.typea.v031.itareq.TVL;
import iata.typea.v031.itareq.UNH;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.gdsservices.api.dto.typea.InventoryAdjestmentRequestDTO;
import com.isa.thinair.gdsservices.api.dto.typea.TVLDetail;
import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;

public class InventoryAdjustmentReqestHandler {

	public static Map<String, Object> getOwnMessageParams(ITAREQ itareq) {

		Map<String, Object> params = new HashMap<String, Object>();
		// we need to store the common access reference number against the PNR or block seat id.
		UNH unh = itareq.getUNH();

		/* Group 1 */
		ORG org = (ORG) getValue(itareq, "getGROUP1().getORG()");
		String companyName = null;
		String agentId = null;
		if (org != null) {
			companyName = org.getORG01().getORG0101();
			agentId = org.getORG02().getORG0202();
		}

		// EQN,FTI,RCI which are common for all the TVLs,
		// FIXME what is the functionality of RCI in the segment sell. Is this used in the booking modification.
		RCI rciL0 = itareq.getRCI();
		EQN eqnL0 = itareq.getEQN();
		FTI ftiL0 = itareq.getFTI();

		RPI rpiL0 = itareq.getRPI();
		SDT sdtL0 = itareq.getSDT();
		List<SSR> ssrL0List = itareq.getSSR();

		/* Group2 */
		TIF tifL1 = null;
		List<FTI> ftiL1List = null;
		List<SSR> ssrL1List = null;
		List<GROUP2> group2List = itareq.getGROUP2();
		for (GROUP2 group2 : group2List) {
			// common TIF, FTI, SSR on the level 1.
			tifL1 = group2.getTIF();
			ftiL1List = group2.getFTI();
			ssrL1List = group2.getSSR();
		}

		/* Group 3 */
		Map<String, InventoryAdjestmentRequestDTO> odiMap = new HashMap<String, InventoryAdjestmentRequestDTO>();

		List<GROUP3> group3List = itareq.getGROUP3();
		for (GROUP3 group3 : group3List) {
			// ODI represents the one journey which need to be match by a dummy reservation or block seats
			ODI odi = group3.getODI();
			String odiFrom = (String) getValue(odi, "getODI01().get(0)");
			String odiTo = (String) getValue(odi, "getODI01().get(1)");
			MSG msgL2 = group3.getMSG();
			String messageFunction = (String) getValue(msgL2, "getMSG01().getMSG0102()");
			// skipping the group if no action is required.
			if (CodeSetEnum.CS1225.NoActionRequired != CodeSetEnum.CS1225.getEnum(messageFunction)) {
				InventoryAdjestmentRequestDTO itareqDto = new InventoryAdjestmentRequestDTO();

				/* Group 4 */
				List<GROUP4> group4List = group3.getGROUP4();
				for (GROUP4 group4 : group4List) {

					TVL tvl = group4.getTVL();
					String from = tvl.getTVL02().getTVL0201();
					String to = tvl.getTVL03().getTVL0301();

					itareqDto.setOdiFrom(setValue(odiFrom, from));
					itareqDto.setOdiTo(setValue(odiTo, to));

					TVLDetail tvlDetail = new TVLDetail();
					tvlDetail.setDepartureTimeStart(getDate(tvl.getTVL01().getTVL0101(), null));
					tvlDetail.setArrivalTime(getDate(tvl.getTVL01().getTVL0103(), null));
					tvlDetail.setFrom(from);
					tvlDetail.setTo(to);
					tvlDetail.setRBD(tvl.getTVL05().getTVL0502());
					String carrierCode = (String) getValue(tvl, "getTVL04().getTVL0401()");
					String flightNumber = (String) getValue(tvl, "getTVL05().getTVL0501()");
					tvlDetail.setFlightNumber(carrierCode.concat(flightNumber));

					// default PRI/EQN/FTI overrides if they present in level3
					RCI pciL3 = (RCI) setValue(group4.getRCI(), rciL0);
					EQN eqnL3 = (EQN) setValue(group4.getEQN(), eqnL0);
					FTI ftiL3 = (FTI) setValue(group4.getFTI(), ftiL0);
					RPI rpiL3 = (RPI) setValue(group4.getRPI(), rpiL0);

					if (eqnL3 == null) { // No specific PAX type is given in the request.consider all as adults.
						Integer adultCount = (Integer) setValue(rpiL3.getRPI01().intValue(), Integer.valueOf(1));
						// tvlDetail.setPaxCount(new int[] { adultCount, 0, 0 });
					} else {
						int adultCount = 0, chlidCount = 0, infantCount = 0;

						Map<CodeSetEnum.CS6353, Integer> paxMap = new HashMap<CodeSetEnum.CS6353, Integer>();
						Integer count = (Integer) setValue(eqnL3.getEQN01().getEQN0101(), Integer.valueOf(1));
						CodeSetEnum.CS6353 paxType = CodeSetEnum.CS6353.getEnum(eqnL3.getEQN01().getEQN0102());
						paxMap.put(paxType, count);
						for (EQN.EQN02 numberOfUnitDetails : eqnL3.getEQN02()) {
							Integer count2 = (Integer) setValue(numberOfUnitDetails.getEQN0201(), Integer.valueOf(1));
							CodeSetEnum.CS6353 paxType2 = CodeSetEnum.CS6353.getEnum(numberOfUnitDetails.getEQN0202());
							paxMap.put(paxType2, count2);
						}

						for (CodeSetEnum.CS6353 pax : paxMap.keySet()) {

							// TODO Are we mapping all the pax type to AA domain
							if ((pax.equals(CodeSetEnum.CS6353.Infant) || pax.equals(CodeSetEnum.CS6353.InfantFemale) || pax
									.equals(CodeSetEnum.CS6353.InfantMale))) {
								infantCount = paxMap.get(pax);
							}

							if (pax.equals(CodeSetEnum.CS6353.Child)) {
								chlidCount = paxMap.get(pax);
							}

							int allCount = rpiL3.getRPI01().intValue();
							adultCount = allCount - (chlidCount + infantCount);
							// tvlDetail.setPaxCount(new int[] { adultCount, chlidCount, infantCount });
						}
					}
					itareqDto.addTVL(tvlDetail);
				}
				odiMap.put(itareqDto.getODIKey(), itareqDto);
			}
		}

		params.put(TypeACommandParamNames.INVENTORYTORY_ADJUSTMENT_DTO_LIST, odiMap);
		return params;
	}

}
