package com.isa.thinair.gdsservices.core.typeb.unify;

import com.isa.typea.common.OriginatorInformation;

public class UnifierFactory {
	private UnifierFactory() {
	}

	public static UnifyTypeBMessage getTypeBMessageUnifier(OriginatorInformation org) {
		UnifyTypeBMessage unifyTypeBMessage;

		unifyTypeBMessage = new UnifyTypeBMessageAmadeusImpl();

		return unifyTypeBMessage;
	}
}
