package com.isa.thinair.gdsservices.core.typeb.validators;

import java.util.List;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRInfantDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

/**
 * @author Nilindra Fernando
 */
public class ValidateInfantSeat extends ValidatorBase {

	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {
		List<SSRDTO> ssrDTOs = bookingRequestDTO.getSsrDTOs();

		boolean isInfantSeatRequired = isInfantSeatRequired(ssrDTOs);

		if (isInfantSeatRequired) {
			String message = MessageUtil.getMessage("gdsservices.validators.infantseats.notsupported");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		} else {
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
			return bookingRequestDTO;
		}
	}

	private boolean isInfantSeatRequired(List<SSRDTO> ssrDTOs) {
		if (ssrDTOs != null && ssrDTOs.size() > 0) {
			String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();
			for (SSRDTO ssrdto : ssrDTOs) {
				if (carrierCode.equals(ssrdto.getCarrierCode())) {
					if (ssrdto instanceof SSRInfantDTO) {
						SSRInfantDTO sSRInfantDTO = (SSRInfantDTO) ssrdto;

						if (sSRInfantDTO.isSeatRequired()) {
							return true;
						}
					}
				}
			}
		}

		return false;
	}
}
