package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.Collection;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.ChangeContactDetailRequest;
import com.isa.thinair.gdsservices.api.dto.internal.ContactDetail;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.OtherServiceInfo;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

public class ChangeContactDetailRequestExtractor extends ValidatorBase {

	public static ChangeContactDetailRequest getRequest(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO,
			Collection<OtherServiceInfo> osis) {

		ChangeContactDetailRequest contactChangeRequst = new ChangeContactDetailRequest();

		contactChangeRequst.setGdsCredentialsDTO(gdsCredentialsDTO);
		contactChangeRequst = (ChangeContactDetailRequest) RequestExtractorUtil.addBaseAttributes(contactChangeRequst,
				bookingRequestDTO);

		ContactDetail contactDetail = RequestExtractorUtil.getContactDetails(osis);
		contactChangeRequst.setContactDetail(contactDetail);
		contactChangeRequst.setNotSupportedSSRExists(bookingRequestDTO.isNotSupportedSSRExists());

		return contactChangeRequst;
	}

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {

		String pnrReference = ValidatorUtils.getPNRReference(bookingRequestDTO.getResponderRecordLocator());

		if (pnrReference.length() == 0) {
			String message = MessageUtil.getMessage("gdsservices.extractors.emptyPNRResponder");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		} else {
			boolean isContactDetailsExists = ValidatorUtils.isContactDetailsExists(bookingRequestDTO.getOsiDTOs(),
					bookingRequestDTO.getGds());

			if (isContactDetailsExists) {
				this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
				return bookingRequestDTO;
			} else {
				String message = MessageUtil.getMessage("gdsservices.extractors.emptyContactDetails");
				bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

				this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
				return bookingRequestDTO;
			}
		}
	}
}
