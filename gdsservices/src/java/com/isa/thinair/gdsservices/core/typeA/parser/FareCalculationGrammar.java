package com.isa.thinair.gdsservices.core.typeA.parser;


public interface FareCalculationGrammar {

	Grammar getGrammar();
}
