package com.isa.thinair.gdsservices.core.typeA.parser;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.gdsservices.core.typeA.parser.FareCalculationParserRegExImpl.FareCalculationConstants;

public class FareCalculationGrammarRegExTreeImpl implements FareCalculationGrammar {

	public Grammar getGrammar() {
		FareCalcGroupNode root = new FareCalcGroupNode(RegExSyntax.Operator.AND, RegExSyntax.Quantification.ONE);

		// First Airport Code or City Code of the journey
		root.addSibling(FareCalcUtil.createFareCalcAreaAirportCodeOrCityCodePart(false));

		FareCalcGroupNode fareCalcAreaGroup = FareCalcUtil.createFareCalcAreaGroup(false);
		root.addSibling(fareCalcAreaGroup);

		FareCalcRegExNode space =
				new FareCalcRegExNode(FareCalculationConstants.RegEx.SPACE, RegExSyntax.Quantification.ZERO_OR_ONE);
		root.addSibling(space);
		FareCalcRegExNode amountCurrency =
				new FareCalcRegExNode(FareCalculationConstants.RegEx.CURRENCY_CODE, RegExSyntax.Quantification.ONE);
		root.addSibling(amountCurrency);
		root.addSibling(space);
		FareCalcRegExNode fareQuoteAmount =
				new FareCalcRegExNode(FareCalculationConstants.RegEx.DECIMAL, RegExSyntax.Quantification.ONE);
		root.addSibling(fareQuoteAmount);
		root.addSibling(space);

		FareCalcRegExNode endOfFareCalc =
				new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.END_OF_FARE_CALC, RegExSyntax.Quantification.ONE);
		root.addSibling(endOfFareCalc);

		FareCalcRegExNode rateOfExchange =
				new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.RATE_OF_EXCHANGE, RegExSyntax.Quantification.ZERO_OR_ONE);
		root.addSibling(rateOfExchange);

		root.addSibling(FareCalcUtil.createTaxFeesChargesPart(false));


		FareCalcRegExGrammar grammar = new FareCalcRegExGrammar();
		grammar.setFareCalcNonDetailed(root.getRegEx());

		grammar.setAirportOrLocationDetailed(FareCalcUtil.createFareCalcAreaAirportCodeOrCityCodePart(true).getRegEx());
		grammar.setCarrierDetailed(FareCalcUtil.createFareCalcAreaCarrierCodePart(true).getRegEx());
		grammar.setJourneyUnitDetailed(FareCalcUtil.createJourneyUnit(true).getRegEx());
		grammar.setJourneySequenceDetailed(FareCalcUtil.createJourneySequencePart(true).getRegEx());
		grammar.setFareQuoteUnitDetailed(FareCalcUtil.createFareQuoteUnit(true).getRegEx());
		grammar.setFareQuoteGroupDetailed(FareCalcUtil.createFareQuoteGroup(true).getRegEx());
		grammar.setTaxFeeChargeDetailed(FareCalcUtil.createTaxFeesChargesPart(true).getRegEx());

		return grammar;
	}

	private abstract static class FareCalcNode {
		public abstract String getRegEx();
	}

	private static class FareCalcRegExNode extends FareCalcNode {
        private String regEx;
		private String quantification;
		private String quantifierType;

		private FareCalcRegExNode(String regEx, String quantification) {
			this(regEx, quantification, RegExSyntax.QuantifierType.GREEDY);
		}

		private FareCalcRegExNode(String regEx, String quantification, String quantifierType) {
			this.regEx = regEx;
			this.quantification = quantification;
			this.quantifierType = quantifierType;
		}

		public String getRegEx() {
			return "(" + regEx + ")" + quantification + quantifierType;
		}
	}

	private static class FareCalcCompositeNode extends FareCalcNode {
		private List<FareCalcNode> descendants;

		private FareCalcCompositeNode() {
			descendants = new ArrayList<FareCalcNode>();
		}

		public List<FareCalcNode> getDescendants() {
			return descendants;
		}

		public void addDescendant(FareCalcNode descendant) {
			descendants.add(descendant);
		}

		public String getRegEx() {
			return null;
		}
	}

	private static class FareCalcGroupNode extends FareCalcNode {
		private List<FareCalcNode> siblings;
		private String operator;
		private String quantification;
		private String quantifierType;

		private FareCalcGroupNode(String operator, String quantification) {
			this(operator, quantification, RegExSyntax.QuantifierType.GREEDY);
		}

		private FareCalcGroupNode(String operator, String quantification, String quantifierType) {
			this.operator = operator;
			this.quantification = quantification;
			this.quantifierType = quantifierType;
			siblings = new ArrayList<FareCalcNode>();
		}

		public List<FareCalcNode> getSiblings() {
			return siblings;
		}

		public String getQuantification() {
			return quantification;
		}

		public void addSibling(FareCalcNode sibling) {
			siblings.add(sibling);
		}

		public String getOperator() {
			return operator;
		}

		public String getRegEx() {
			String regEx = "";
			int siblingsLength = siblings.size();

			if (siblingsLength > 0) {
				regEx += "(";
				for (int a = 0; a < siblingsLength - 1; a++) {
					regEx += siblings.get(a).getRegEx() + operator;
				}
				regEx += siblings.get(siblingsLength - 1).getRegEx();
				regEx += ")" + quantification + quantifierType;
			}


			return regEx;
		}
	}

	public static class FareCalcUtil {

		public static FareCalcGroupNode createFareCalcAreaAirportCodeOrCityCodePart(boolean isDetailed) {
			FareCalcGroupNode airportCodeOrCityCode = new FareCalcGroupNode(RegExSyntax.Operator.AND, RegExSyntax.Quantification.ONE);

			if (isDetailed) {
				FareCalcGroupNode preAirportDecorators = new FareCalcGroupNode(RegExSyntax.Operator.AND, RegExSyntax.Quantification.ZERO_OR_MORE);
				airportCodeOrCityCode.addSibling(preAirportDecorators);
				// ==== ==== Before Airport
				preAirportDecorators.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.RegEx.SPACE_IF_ALPHABETIC_PRECEDE, RegExSyntax.Quantification.ONE));
				preAirportDecorators.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.INTERMEDIATE_TRANSFER_POINT, RegExSyntax.Quantification.ZERO_OR_ONE));
				preAirportDecorators.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.EXTRA_MILEAGE_ALLOWANCE, RegExSyntax.Quantification.ZERO_OR_ONE));
				preAirportDecorators.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.MAX_PERMITTED_MILEAGE_DEDUCTION, RegExSyntax.Quantification.ZERO_OR_ONE));
				preAirportDecorators.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.TICKETED_POINT_NOT_INCLUDED_IN_MILEAGE, RegExSyntax.Quantification.ZERO_OR_ONE));
			} else {
				airportCodeOrCityCode.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.NON_DETAILED_CAPTURE, RegExSyntax.Quantification.ONE));
			}
			// ==== ==== Before Airport

			airportCodeOrCityCode.addSibling(
					new FareCalcRegExNode(FareCalculationConstants.RegEx.AIRPORT_OR_CITY_CODE, RegExSyntax.Quantification.ONE));
			airportCodeOrCityCode.addSibling(
					new FareCalcRegExNode(FareCalculationConstants.RegEx.SPACE_IF_ALPHABETIC_SUCCEED, RegExSyntax.Quantification.ONE));

//			if (isDetailed) {
//				FareCalcGroupNode postAirportDecorators = new FareCalcGroupNode(RegExSyntax.Operator.OR, RegExSyntax.Quantification.ZERO_OR_ONE);
//				airportCodeOrCityCode.addSibling(postAirportDecorators);
//			} else {
//				airportCodeOrCityCode.addSibling(
//						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.NON_DETAILED_CAPTURE, RegExSyntax.Quantification.ONE));
//			}

			return airportCodeOrCityCode;
		}

		public static FareCalcGroupNode createFareCalcAreaCarrierCodePart(boolean isDetailed) {
			FareCalcGroupNode carrierCodeGroup = new FareCalcGroupNode(RegExSyntax.Operator.OR, RegExSyntax.Quantification.ONE);

			carrierCodeGroup.addSibling(
					new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.UNFLOWN_SECTOR, RegExSyntax.Quantification.ONE));

			FareCalcGroupNode flownSector = new FareCalcGroupNode(RegExSyntax.Operator.AND, RegExSyntax.Quantification.ONE);
			carrierCodeGroup.addSibling(flownSector);

			if (isDetailed) {
				FareCalcGroupNode preCarrierCodeDecorators = new FareCalcGroupNode(RegExSyntax.Operator.AND, RegExSyntax.Quantification.ZERO_OR_MORE);
				flownSector.addSibling(preCarrierCodeDecorators);
				preCarrierCodeDecorators.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.STOP_OVER_TRANSFER_CHARGE, RegExSyntax.Quantification.ZERO_OR_ONE));
				preCarrierCodeDecorators.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.SURCHARGE, RegExSyntax.Quantification.ZERO_OR_ONE));
			} else {
				flownSector.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.NON_DETAILED_CAPTURE, RegExSyntax.Quantification.ONE));
			}

			FareCalcGroupNode carrierCode = new FareCalcGroupNode(RegExSyntax.Operator.AND, RegExSyntax.Quantification.ONE);
			flownSector.addSibling(carrierCode);

			carrierCode.addSibling(
					new FareCalcRegExNode(FareCalculationConstants.RegEx.CARRIER_CODE_2_CHARS, RegExSyntax.Quantification.ONE));
			carrierCode.addSibling(
					new FareCalcRegExNode(FareCalculationConstants.RegEx.SPACE, RegExSyntax.Quantification.ONE));

//			if (isDetailed) {
//				FareCalcGroupNode postCarrierCodeDecorators = new FareCalcGroupNode(RegExSyntax.Operator.AND, RegExSyntax.Quantification.ZERO_OR_MORE);
//				flownSector.addSibling(postCarrierCodeDecorators);
//			} else {
//				carrierCodeGroup.addSibling(
//						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.NON_DETAILED_CAPTURE, RegExSyntax.Quantification.ONE));
//			}

			return carrierCodeGroup;
		}

		public static FareCalcGroupNode createJourneySequencePart(boolean isDetailed) {
            FareCalcGroupNode journeySequenceWrapper = new FareCalcGroupNode(RegExSyntax.Operator.AND, RegExSyntax.Quantification.ONE);

			FareCalcGroupNode journeySequence = new FareCalcGroupNode(RegExSyntax.Operator.AND, RegExSyntax.Quantification.ONE_OR_MORE);
            journeySequenceWrapper.addSibling(journeySequence);

			FareCalcGroupNode journeyUnit = createJourneyUnit(isDetailed);
            journeySequence.addSibling(journeyUnit);

			return journeySequenceWrapper;
		}

		public static FareCalcGroupNode createJourneyUnit(boolean isDetailed) {

			FareCalcGroupNode journeyUnit = new FareCalcGroupNode(RegExSyntax.Operator.AND, RegExSyntax.Quantification.ONE);

//		journeySequence.addSibling(FareCalcUtil.createSideTripNode());

			journeyUnit.addSibling(FareCalcUtil.createFareCalcAreaCarrierCodePart(isDetailed));

			journeyUnit.addSibling(FareCalcUtil.createFareCalcAreaAirportCodeOrCityCodePart(isDetailed));

			return journeyUnit;
		}

		public static FareCalcGroupNode createFareQuoteUnit(boolean isDetailed) {
			FareCalcGroupNode fareCalcAreaWrapper = new FareCalcGroupNode(RegExSyntax.Operator.AND, RegExSyntax.Quantification.ONE);

			// ==== ==== Before Amount
			if (isDetailed) {
				FareCalcGroupNode preFareQuoteDecorators = new FareCalcGroupNode(RegExSyntax.Operator.AND, RegExSyntax.Quantification.ZERO_OR_MORE);
				fareCalcAreaWrapper.addSibling(preFareQuoteDecorators);
				preFareQuoteDecorators.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.COMMON_POINT_MIN_CHECK, RegExSyntax.Quantification.ZERO_OR_ONE));
				preFareQuoteDecorators.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.EXTRA_MILEAGE_ALLOWANCE_ANY, RegExSyntax.Quantification.ZERO_OR_ONE));
				preFareQuoteDecorators.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.HIGHER_CLASS_DIFFERENTIAL, RegExSyntax.Quantification.ZERO_OR_ONE));
				preFareQuoteDecorators.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.HIGHER_INTERMEDIATE_POINT, RegExSyntax.Quantification.ZERO_OR_ONE));
				preFareQuoteDecorators.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.LOWEST_COMBINATION_PRINCIPLE, RegExSyntax.Quantification.ZERO_OR_ONE));
				preFareQuoteDecorators.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.MILEAGE_EQUALIZATION, RegExSyntax.Quantification.ZERO_OR_ONE));
				preFareQuoteDecorators.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.MINIMUMS, RegExSyntax.Quantification.ZERO_OR_ONE));
				preFareQuoteDecorators.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.ONE_WAY_SUB_JOURNEY_CHECK, RegExSyntax.Quantification.ZERO_OR_ONE));
				preFareQuoteDecorators.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.RETURN_SUB_JOURNEY_CHECK, RegExSyntax.Quantification.ZERO_OR_ONE));
				preFareQuoteDecorators.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.SURCHARGE_FARE, RegExSyntax.Quantification.ZERO_OR_ONE));
				preFareQuoteDecorators.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.MILEAGE_PRINCIPLE, RegExSyntax.Quantification.ZERO_OR_ONE));
				preFareQuoteDecorators.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.STOP_OVER_TRANSFER_CHARGE_SUMMARY, RegExSyntax.Quantification.ZERO_OR_ONE));
			} else {
				fareCalcAreaWrapper.addSibling(
						new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.NON_DETAILED_CAPTURE, RegExSyntax.Quantification.ONE));
			}
			// ==== ==== Before Amount


			FareCalcGroupNode fareCalcAmount = new FareCalcGroupNode(RegExSyntax.Operator.AND, RegExSyntax.Quantification.ONE);
			fareCalcAreaWrapper.addSibling(fareCalcAmount);
			fareCalcAmount.addSibling(
					new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.FARE_QUOTE_AMOUNT, RegExSyntax.Quantification.ONE));

			// ==== ==== After Amount
			fareCalcAreaWrapper.addSibling(
					new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.FARE_BASIS_CODE, RegExSyntax.Quantification.ZERO_OR_ONE));

			// ==== ==== After Amount

			return fareCalcAreaWrapper;
		}

		public static FareCalcGroupNode createFareQuoteGroup(boolean isDetailed) {
            FareCalcGroupNode fareQuoteGroupWrapper = new FareCalcGroupNode(RegExSyntax.Operator.AND, RegExSyntax.Quantification.ONE);

            FareCalcGroupNode fareQuoteGroup = new FareCalcGroupNode(RegExSyntax.Operator.AND, RegExSyntax.Quantification.ONE_OR_MORE);
            fareQuoteGroupWrapper.addSibling(fareQuoteGroup);

            FareCalcGroupNode fareQuoteUnit = createFareQuoteUnit(isDetailed);
            fareQuoteGroup.addSibling(fareQuoteUnit);

            return fareQuoteGroupWrapper;
		}

		public static FareCalcGroupNode createTaxFeesChargesPart(boolean isDetailed) {
			FareCalcGroupNode taxFeesChargesArea = new FareCalcGroupNode(RegExSyntax.Operator.AND, RegExSyntax.Quantification.ZERO_OR_MORE);
			taxFeesChargesArea.addSibling(
					new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.PASSENGER_FACILITY_CHARGE, RegExSyntax.Quantification.ZERO_OR_ONE));
			taxFeesChargesArea.addSibling(
					new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.US_DOMESTIC_FLIGHT_SEGMENT_TAX, RegExSyntax.Quantification.ZERO_OR_ONE));

			return taxFeesChargesArea;
		}

        public static FareCalcGroupNode createFareCalcAreaGroup(boolean isDetailed) {
            FareCalcGroupNode fareCalcArea = new FareCalcGroupNode(RegExSyntax.Operator.AND, RegExSyntax.Quantification.ONE_OR_MORE);

            FareCalcGroupNode journeySequenceWrapper = new FareCalcGroupNode(RegExSyntax.Operator.AND, RegExSyntax.Quantification.ONE);
            fareCalcArea.addSibling(journeySequenceWrapper);

            fareCalcArea.addSibling(FareCalcUtil.createJourneySequencePart(isDetailed));

            fareCalcArea.addSibling(FareCalcUtil.createFareQuoteGroup(isDetailed));

            return fareCalcArea;
        }

		// TODO -- need to test
		public static FareCalcGroupNode createSideTripNode(boolean isDetailed) {
			FareCalcGroupNode sideTripArea = new FareCalcGroupNode(RegExSyntax.Operator.AND, RegExSyntax.Quantification.ZERO_OR_ONE);

			sideTripArea.addSibling(
					new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.SIDE_TRIP_START, RegExSyntax.Quantification.ONE));

			FareCalcGroupNode journeySequence = new FareCalcGroupNode(RegExSyntax.Operator.AND, RegExSyntax.Quantification.ONE_OR_MORE);
			sideTripArea.addSibling(journeySequence);

			journeySequence.addSibling(createFareCalcAreaCarrierCodePart(isDetailed));
			journeySequence.addSibling(createFareCalcAreaAirportCodeOrCityCodePart(isDetailed));
			journeySequence.addSibling(createFareQuoteGroup(isDetailed));

			sideTripArea.addSibling(
					new FareCalcRegExNode(FareCalculationConstants.ElementPatterns.SIDE_TRIP_END, RegExSyntax.Quantification.ONE));

			return sideTripArea;
		}
	}


	private static interface RegExSyntax {
		interface Quantification {
			String ONE = "";
			String ZERO_OR_ONE = "?";
			String ZERO_OR_MORE = "*";
			String ONE_OR_MORE = "+";
		}

		interface QuantifierType {
			String GREEDY = "";
			String RELUCTANT = "?";
		}

		interface Operator {
			String AND = "";
			String OR = "|";
		}
	}
}
