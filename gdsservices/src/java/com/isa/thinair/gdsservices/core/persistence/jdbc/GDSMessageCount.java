package com.isa.thinair.gdsservices.core.persistence.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.gdsservices.api.dto.internal.criteria.GDSMessageSearchCriteria;

/**
 * 
 * @author Dhanushka Class for exctracting the Count of GDS Messages
 */
public class GDSMessageCount {

	private DataSource ds;

	private String queryString;

	private GDSMessageSearchCriteria gdsMessageSearchCriteria;

	/**
	 * Constructor
	 * 
	 * @param criteria
	 * @param sql
	 * @param ds
	 */
	public GDSMessageCount(GDSMessageSearchCriteria criteria, String sql, DataSource ds) {
		this.ds = ds;
		this.queryString = sql;
		this.gdsMessageSearchCriteria = criteria;
	}

	/**
	 * Method that uses the JdbcTemplate with prepared statement creator and extractor int
	 * 
	 * @return
	 */
	public int getCountofGDSMessages() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(ds);
		Integer count = (Integer) jdbcTemplate.query(new GDSMessagePrepStatCreator(gdsMessageSearchCriteria, queryString),
				new RetrieveGDSMessageCount());
		return count.intValue();
	}

	/**
	 * Class that passes the criteria to the prepared statement.
	 * 
	 * @author Dhanushka
	 * 
	 */
	public class GDSMessagePrepStatCreator implements PreparedStatementCreator {

		private String queryString;

		private GDSMessageSearchCriteria criteria;

		public GDSMessagePrepStatCreator(GDSMessageSearchCriteria criteria, String sql) {
			this.criteria = criteria;
			queryString = sql;
		}

		public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
			PreparedStatement prepStmt = con.prepareStatement(queryString);
			return prepStmt;
		}

	}

	/**
	 * Class that is used inside JdbcTemplate Will access a single colummn and row, to get the count value.
	 * 
	 * @author Dhanushka
	 * 
	 */
	public class RetrieveGDSMessageCount implements ResultSetExtractor {

		public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
			Integer count = null;
			while (rs.next()) {
				count = new Integer(rs.getInt("COUNT"));
			}
			return count;

		}
	}
}
