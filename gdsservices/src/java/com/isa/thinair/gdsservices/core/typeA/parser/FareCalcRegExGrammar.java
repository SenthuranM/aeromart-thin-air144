package com.isa.thinair.gdsservices.core.typeA.parser;

public class FareCalcRegExGrammar implements Grammar {

	private String fareCalcNonDetailed;

	private String airportOrLocationDetailed;
	private String carrierDetailed;
    private String journeyUnitDetailed;
    private String journeySequenceDetailed;
    private String fareQuoteUnitDetailed;
	private String fareQuoteGroupDetailed;
	private String taxFeeChargeDetailed;

    public String getFareCalcNonDetailed() {
        return fareCalcNonDetailed;
    }

    public void setFareCalcNonDetailed(String fareCalcNonDetailed) {
        this.fareCalcNonDetailed = fareCalcNonDetailed;
    }

    public String getAirportOrLocationDetailed() {
        return airportOrLocationDetailed;
    }

    public void setAirportOrLocationDetailed(String airportOrLocationDetailed) {
        this.airportOrLocationDetailed = airportOrLocationDetailed;
    }

    public String getCarrierDetailed() {
        return carrierDetailed;
    }

    public void setCarrierDetailed(String carrierDetailed) {
        this.carrierDetailed = carrierDetailed;
    }

    public String getJourneyUnitDetailed() {
        return journeyUnitDetailed;
    }

    public void setJourneyUnitDetailed(String journeyUnitDetailed) {
        this.journeyUnitDetailed = journeyUnitDetailed;
    }

    public String getJourneySequenceDetailed() {
        return journeySequenceDetailed;
    }

    public void setJourneySequenceDetailed(String journeySequenceDetailed) {
        this.journeySequenceDetailed = journeySequenceDetailed;
    }

    public String getFareQuoteUnitDetailed() {
        return fareQuoteUnitDetailed;
    }

    public void setFareQuoteUnitDetailed(String fareQuoteUnitDetailed) {
        this.fareQuoteUnitDetailed = fareQuoteUnitDetailed;
    }

    public String getFareQuoteGroupDetailed() {
        return fareQuoteGroupDetailed;
    }

    public void setFareQuoteGroupDetailed(String fareQuoteGroupDetailed) {
        this.fareQuoteGroupDetailed = fareQuoteGroupDetailed;
    }

    public String getTaxFeeChargeDetailed() {
        return taxFeeChargeDetailed;
    }

    public void setTaxFeeChargeDetailed(String taxFeeChargeDetailed) {
        this.taxFeeChargeDetailed = taxFeeChargeDetailed;
    }
}
