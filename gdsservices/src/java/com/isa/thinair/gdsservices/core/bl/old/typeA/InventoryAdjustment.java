package com.isa.thinair.gdsservices.core.bl.old.typeA;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.util.AvailableFlightSearchDTOBuilder;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.gdsservices.api.dto.typea.EDITypeAItareqException;
import com.isa.thinair.gdsservices.api.dto.typea.ERCDetail;
import com.isa.thinair.gdsservices.api.dto.typea.InventoryAdjestmentRequestDTO;
import com.isa.thinair.gdsservices.api.dto.typea.InventoryAdjustmentResponseDTO;
import com.isa.thinair.gdsservices.api.dto.typea.ODIResponseDTO;
import com.isa.thinair.gdsservices.api.dto.typea.TVLDetail;
import com.isa.thinair.gdsservices.api.dto.typea.TVLDto;
import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;
import com.isa.thinair.gdsservices.api.model.GDSTransactionStatus;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.api.util.TypeAResponseCode;
import com.isa.thinair.gdsservices.core.common.GDSTypeATransactionStatusUtils;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * This command is responsible for consume the inventory (block seat) / creating a dummy reservation if PAX detail
 * existing.
 * 
 * @author mekanayake
 * 
 * @isa.module.command name="inventoryAdjustment.old"
 */
public class InventoryAdjustment extends DefaultBaseCommand {

	public InventoryAdjustment() {

	}

	@SuppressWarnings("unchecked")
	@Override
	public ServiceResponce execute() throws ModuleException {
		InventoryAdjustmentResponseDTO inventoryAdjustmentResponseDTO = new InventoryAdjustmentResponseDTO();

		List<ODIResponseDTO> odiResponseDTOs = new ArrayList<ODIResponseDTO>();
		Map<String, InventoryAdjestmentRequestDTO> odiMap = (Map<String, InventoryAdjestmentRequestDTO>) this
				.getParameter(TypeACommandParamNames.EDI_REQUEST_DTO);
		String gdsTnxRef = (String) this.getParameter(TypeACommandParamNames.COMMON_ACCESS_REFERENCE);
		GDSTransactionStatus transactionStatus = GDSTypeATransactionStatusUtils.createOrUpdateTransactionStage(gdsTnxRef,
				IATAOperation.ITAREQ.name());

		for (String key : odiMap.keySet()) { // For each ODI
			InventoryAdjestmentRequestDTO adjestmentRequestDTO = odiMap.get(key);

			ODIResponseDTO odiResDTO = new ODIResponseDTO();
			odiResDTO.setOdiFrom(adjestmentRequestDTO.getOdiFrom());
			odiResDTO.setOdiTo(adjestmentRequestDTO.getOdiTo());

			// group the TVLs by action code because each action code defines the action needed to be perform on TVSs
			Set<CodeSetEnum.CS4405> actionsRequested = adjestmentRequestDTO.getRequestedActionCodes();
			for (CodeSetEnum.CS4405 action : actionsRequested) {

				// FIXME removed releaseBlockSeats method in version 1.15

				if (action.equals(CodeSetEnum.CS4405.NN)) { // Needed
					try {
						odiResDTO = handleInventoryConsumpsion(odiResDTO, adjestmentRequestDTO, action, transactionStatus);
					} catch (EDITypeAItareqException ex) {
						odiResDTO.addErcDetails(new ERCDetail(CodeSetEnum.CS9321.valueOf(ex.getErrorCode()), 1));
					} finally {
						odiResponseDTOs.add(odiResDTO);
					}
				}

			}
		}
		inventoryAdjustmentResponseDTO.setOdiResponseDTOs(odiResponseDTOs);

		// constructing and return command response
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		responce.addResponceParam(TypeAResponseCode.INVENTORY_ADJUSTMENT_RESPONSE, inventoryAdjustmentResponseDTO);
		return responce;
	}

	private ODIResponseDTO handleInventoryConsumpsion(ODIResponseDTO responseDTO, InventoryAdjestmentRequestDTO requestDTO,
			CodeSetEnum.CS4405 action, GDSTransactionStatus transactionStatus) throws ModuleException, EDITypeAItareqException {
		Map<String, List<TVLDetail>> tvlMap = requestDTO.getTVLDetailsByAction(action);

		for (String bcCode : tvlMap.keySet()) {

			/* FIXME need to map the RBD to Accelaero booking class. */
			BookingClass bookingClass = GDSServicesModuleUtil.getBookingClassBD().getLightWeightBookingClass(bcCode);
			List<TVLDetail> tvls = tvlMap.get(bcCode);
			CodeSetEnum.CS4405 resultAction = CodeSetEnum.CS4405.NO; // default:no action taken
			try {

				AvailableFlightSearchDTO searchDTOs = extractPriceQuoteReqParams(tvls, bookingClass);
				validateTVL(tvls, bookingClass);

				Collection<OndFareDTO> segsFaresSeatsColForBooking = new ArrayList<OndFareDTO>();
				SelectedFlightDTO selectedFlightDTO = GDSServicesModuleUtil.getReservationQueryBD()
						.getFareQuote(searchDTOs, null);
				Collection<OndFareDTO> quotedSegsFaresSeatsCol = new ArrayList<OndFareDTO>();

				if (selectedFlightDTO != null && selectedFlightDTO.getFareType() != FareTypes.NO_FARE) {
					quotedSegsFaresSeatsCol = selectedFlightDTO.getOndFareDTOs(false);
					resultAction = CodeSetEnum.CS4405.KK;
				} else { // Booking class already full
					resultAction = CodeSetEnum.CS4405.NO;
					// throw new EDITypeAItareqException(CS_9321.AppError_293);
				}

				/* Go ahead with the inventory blocking */
				segsFaresSeatsColForBooking.addAll(quotedSegsFaresSeatsCol);

				/* Block requested seat */
				// Audit parameters are not set.
				GDSServicesModuleUtil.getReservationBD().blockSeats(segsFaresSeatsColForBooking, null);
				createTVLResponse(responseDTO, tvls, resultAction);
			} catch (EDITypeAItareqException ex) {
				createTVLResponse(responseDTO, tvls, resultAction, ex);
				throw ex;
			} catch (ModuleException e) {
				// TODO result codes need to be verified.
				createTVLResponse(responseDTO, tvls, CodeSetEnum.CS4405.UU);
				throw e;
			}
		}

		return responseDTO;
	}

	private void createTVLResponse(ODIResponseDTO responseDTO, List<TVLDetail> tvls, CodeSetEnum.CS4405 resultAction,
			EDITypeAItareqException ex) {
		for (TVLDetail tvlDetail : tvls) {
			TVLDto tvlResponse = new TVLDto();
			tvlResponse.setTvlDetail(tvlDetail);
			tvlResponse.setPaxCount(tvlDetail.getAdultCount() + tvlDetail.getChildCount());
			tvlResponse.setResultCode(resultAction);
			tvlResponse.addErcDetails(new ERCDetail(CodeSetEnum.CS9321.valueOf(ex.getErrorCode()), 3));
			responseDTO.addTvlResponse(tvlResponse);
		}
	}

	private void createTVLResponse(ODIResponseDTO responseDTO, List<TVLDetail> tvls, CodeSetEnum.CS4405 actionCode) {
		for (TVLDetail tvlDetail : tvls) {
			TVLDto tvlResponse = new TVLDto();
			tvlResponse.setTvlDetail(tvlDetail);
			tvlResponse.setPaxCount(tvlDetail.getAdultCount() + tvlDetail.getChildCount());
			tvlResponse.setResultCode(actionCode);
			responseDTO.addTvlResponse(tvlResponse);
		}
	}

	/**
	 * Validations to be checked for PQ request. TODO move to a separate utill
	 * 
	 * @param list
	 * @param bookingClass
	 * @return
	 */
	private List<TVLDto> validateTVL(List<TVLDetail> list, BookingClass bookingClass) {
		// TODO Auto-generated method stub
		return null;
	}

	private AvailableFlightSearchDTO extractPriceQuoteReqParams(List<TVLDetail> tvlList, BookingClass bookingClass)
			throws ModuleException, EDITypeAItareqException {
		AvailableFlightSearchDTOBuilder availableFlightSearchDTO = new AvailableFlightSearchDTOBuilder();

		// return flag indicator.
		boolean returnSegments = false;
		Collection<String> obAirports = new ArrayList<String>();
		int[] paxCount = new int[3];

		// In-bound and Out-bound flightSegment lists
		Collection<FlightSegmentDTO> obFlightSegmentDTOs = new LinkedList<FlightSegmentDTO>();
		Collection<FlightSegmentDTO> ibFlightSegmentDTOs = new LinkedList<FlightSegmentDTO>();
		for (TVLDetail tvl : tvlList) {

			// departure date of the first TVL is taken as the departure date.
			if (availableFlightSearchDTO.getDepartureDate() == null) {
				availableFlightSearchDTO.setDepartureDate(tvl.getDepartureTimeStart());
				paxCount[0] = tvl.getAdultCount();
				paxCount[1] = tvl.getChildCount();
				paxCount[2] = tvl.getInfantCount();
			}

			if (obAirports.size() == 0) {
				availableFlightSearchDTO.setFromAirport(tvl.getFrom());
			}

			// we got a return segment.
			if (!returnSegments && (obAirports.contains(tvl.getFrom()) && obAirports.contains(tvl.getTo()))) {
				returnSegments = true;
				availableFlightSearchDTO.setReturnFlag(true);
			}

			// last OB is taken as the destination.
			if (!returnSegments) {
				availableFlightSearchDTO.setToAirport(tvl.getTo());
				obAirports.add(tvl.getFrom());
				obAirports.add(tvl.getTo());
			}

			// first IB departure date is taken as the return date
			if (returnSegments && availableFlightSearchDTO.getReturnDate() == null) {
				availableFlightSearchDTO.setDepartureDate(tvl.getDepartureTimeStart());
			}

			FlightSegmentDTO aaFlightSegmentDTO = new FlightSegmentDTO();
			aaFlightSegmentDTO.setFromAirport(tvl.getFrom());
			aaFlightSegmentDTO.setToAirport(tvl.getTo());
			aaFlightSegmentDTO.setFlightNumber(tvl.getFlightNumber());
			aaFlightSegmentDTO.setDepartureDateTime(tvl.getDepartureTimeStart());
			aaFlightSegmentDTO.setArrivalDateTime(tvl.getArrivalTime());

			if (!returnSegments) {
				obFlightSegmentDTOs.add(aaFlightSegmentDTO);
			} else {
				ibFlightSegmentDTOs.add(aaFlightSegmentDTO);
			}
		}

		Collection<FlightSegmentDTO> filledObFlightSegmentDTOs = GDSServicesModuleUtil.getFlightBD().getFilledFlightSegmentDTOs(
				obFlightSegmentDTOs);
		if (filledObFlightSegmentDTOs.size() != obFlightSegmentDTOs.size()) {
			// throw new EDITypeAItareqException(CS_9321.AppError_322);
		}

		// set outbound flight segment IDs
		Collection<Integer> obFlightSegmentIds = new ArrayList<Integer>();
		for (FlightSegmentDTO flightSegmentDTO : filledObFlightSegmentDTOs) {
			if (flightSegmentDTO.getSegmentId() != null) {
				obFlightSegmentIds.add(flightSegmentDTO.getSegmentId());
			} else {
				// throw new EDITypeAItareqException(CS_9321.AppError_322);
			}
		}
		availableFlightSearchDTO.setOutBoundFlights(obFlightSegmentIds);

		// set in-bound flights.
		if (availableFlightSearchDTO.getSearchDTO().isReturnFlag()) {
			Collection<FlightSegmentDTO> filledIbFlightSegmentDTOs = GDSServicesModuleUtil.getFlightBD()
					.getFilledFlightSegmentDTOs(ibFlightSegmentDTOs);
			if (filledObFlightSegmentDTOs.size() != obFlightSegmentDTOs.size()) {
				// throw new EDITypeAItareqException(CS_9321.AppError_322);
			}
			Collection<Integer> ibFlightSegmentIds = new ArrayList<Integer>();
			for (FlightSegmentDTO flightSegmentDTO : filledIbFlightSegmentDTOs) {
				if (flightSegmentDTO.getSegmentId() != null) {
					ibFlightSegmentIds.add(flightSegmentDTO.getSegmentId());
				} else {
					// throw new EDITypeAItareqException(CS_9321.AppError_322);
				}
			}
			availableFlightSearchDTO.setInBoundFlights(ibFlightSegmentIds);
		}

		// setting the PAX count
		if (paxCount[0] == 0 && paxCount[1] == 0) { // both the adult and children count cannot be zero
			// TODO find the suitable error code.
		}

		if (paxCount[0] < paxCount[2]) { // infants exceeds the adult count.
			// throw new EDITypeAItareqException(CS_9321.AppError_324);
		}

		availableFlightSearchDTO.setAdultCount(paxCount[0]);
		availableFlightSearchDTO.setChildCount(paxCount[1]);
		availableFlightSearchDTO.setInfantCount(paxCount[2]);

		// FIXME get the actual values.
		availableFlightSearchDTO.setPosAirport("SHJ");
		availableFlightSearchDTO.setAgentCode("SHJ001");

		availableFlightSearchDTO.setChannelCode(SalesChannelsUtil
				.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));
		availableFlightSearchDTO.setBookingType(BookingClass.BookingClassType.NORMAL);
		availableFlightSearchDTO.setAvailabilityRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED);

		if (bookingClass == null) {
			// throw new EDITypeAItareqException(CS_9321.AppError_104);

		}
		availableFlightSearchDTO.setBookingClassCode(bookingClass.getBookingCode());
		availableFlightSearchDTO.setCabinClassCode(bookingClass.getCabinClassCode());

		return availableFlightSearchDTO.getSearchDTO();
	}
}
