package com.isa.thinair.gdsservices.core.bl.ssm;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlight;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlightSchedule;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.Leg;
import com.isa.thinair.airschedules.api.model.SSMSplitFlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.commons.api.constants.CommonsConstants.TimeMode;
import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.FrequencyUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.XMLStreamer;
import com.isa.thinair.gdsservices.api.model.GDSPublishMessage;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.msgbroker.api.dto.ASMessegeDTO;
import com.isa.thinair.msgbroker.api.dto.SSIFlightLegDTO;
import com.isa.thinair.msgbroker.api.dto.SSIMessegeDTO;

public class SchedulePublishUtil {

	public static final String GDS_REF_SEQUENCE_NAME = "S_SSM_REF_NUMBER";

	public static final String GDS_REF_SUFFIX = "E001";

	public static void addDaysOfOperation(SSIMessegeDTO ssmDto, Frequency frequency) {

		Collection days = CalendarUtil.getDaysFromFrequency(frequency);

		Iterator itDays = days.iterator();

		while (itDays.hasNext()) {
			DayOfWeek day = (DayOfWeek) itDays.next();
			if (day.equals(DayOfWeek.MONDAY)) {
				ssmDto.addDayOfWeek(SSIMessegeDTO.DayOfWeek.MONDAY);
			} else if (day.equals(DayOfWeek.TUESDAY)) {
				ssmDto.addDayOfWeek(SSIMessegeDTO.DayOfWeek.TUESDAY);
			} else if (day.equals(DayOfWeek.WEDNESDAY)) {
				ssmDto.addDayOfWeek(SSIMessegeDTO.DayOfWeek.WEDNESDAY);
			} else if (day.equals(DayOfWeek.THURSDAY)) {
				ssmDto.addDayOfWeek(SSIMessegeDTO.DayOfWeek.THURSDAY);
			} else if (day.equals(DayOfWeek.FRIDAY)) {
				ssmDto.addDayOfWeek(SSIMessegeDTO.DayOfWeek.FRIDAY);
			} else if (day.equals(DayOfWeek.SATURDAY)) {
				ssmDto.addDayOfWeek(SSIMessegeDTO.DayOfWeek.SATURDAY);
			} else if (day.equals(DayOfWeek.SUNDAY)) {
				ssmDto.addDayOfWeek(SSIMessegeDTO.DayOfWeek.SUNDAY);
			}
		}
	}

	public static GDSPublishMessage
			createASMSuccessPublishLog(Integer gdsId, String content, String referenceKey, Integer typeId) {
		return createSuccessPublishLog(gdsId, content, referenceKey, GDSPublishMessage.MESSAGE_TYPE_ASM, typeId);
	}

	public static GDSPublishMessage
			createSSMSuccessPublishLog(Integer gdsId, String content, String referenceKey, Integer typeId) {
		return createSuccessPublishLog(gdsId, content, referenceKey, GDSPublishMessage.MESSAGE_TYPE_SSM, typeId);
	}

	public static GDSPublishMessage createSuccessPublishLog(Integer gdsId, String content, String referenceKey, String type,
			Integer typeId) {

		GDSPublishMessage pubMessage = new GDSPublishMessage();
		pubMessage.setGds(gdsId);

		pubMessage.setMessageContent(content);
		pubMessage.setMessageType(type);
		pubMessage.setMessageTypeId(typeId);
		pubMessage.setReferenceKey(referenceKey);

		pubMessage.setGenDate(new Date());
		pubMessage.setStatus(GDSPublishMessage.MESSAGE_SUCCESS);

		return pubMessage;
	}

	public static void addRBDInfo(SSIMessegeDTO ssmDto, Collection bookingClassList) {

		String rbds = "";
		if (bookingClassList != null) {
			Iterator itRbd = bookingClassList.iterator();
			while (itRbd.hasNext()) {
				String code = (String) itRbd.next();
				rbds += code;
			}
		}

		ssmDto.setPassengerResBookingDesignator(rbds);
	}

	public static void addRBDInfo(ASMessegeDTO asmDto, Collection bookingClassList) {

		String rbds = "";
		if (bookingClassList != null) {
			Iterator itRbd = bookingClassList.iterator();
			while (itRbd.hasNext()) {
				String code = (String) itRbd.next();
				rbds += code;
			}
		}

		asmDto.setPassengerResBookingDesignator(rbds);
	}

	public static void addLegsInfo(SSIMessegeDTO ssmDto, Collection legsList) {

		SimpleDateFormat format = new SimpleDateFormat("HHmm");

		for (int i = 0; i < legsList.size(); i++) {

			SSIFlightLegDTO ssiLeg = new SSIFlightLegDTO();

			Leg leg = null;
			Iterator itLegsIt = legsList.iterator();
			while (itLegsIt.hasNext()) {

				leg = (Leg) itLegsIt.next();
				if (leg.getLegNumber() == i + 1) {
					break;
				}
			}
			String depatureTime = null;
			int depatureOffSet = 0;
			String arrivalTime = null;
			int arrivalOffSet = 0;

			if (TimeMode.LOCAL_TIME_MODE.equalsIgnoreCase(ssmDto.getTimeMode())) {
				depatureTime = format.format(leg.getEstDepartureTimeLocal());
				depatureOffSet = leg.getEstDepartureDayOffsetLocal();

				arrivalTime = format.format(leg.getEstArrivalTimeLocal());
				arrivalOffSet = leg.getEstArrivalDayOffsetLocal();
			} else {

				depatureTime = format.format(leg.getEstDepartureTimeZulu());
				depatureOffSet = leg.getEstDepartureDayOffset();

				arrivalTime = format.format(leg.getEstArrivalTimeZulu());
				arrivalOffSet = leg.getEstArrivalDayOffset();
			}

			ssiLeg.setDepatureAirport(leg.getOrigin());
			ssiLeg.setDepatureTime(depatureTime);
			ssiLeg.setDepatureOffiset(depatureOffSet);

			ssiLeg.setArrivalAirport(leg.getDestination());
			ssiLeg.setArrivalTime(arrivalTime);
			ssiLeg.setArrivalOffset(arrivalOffSet);

			ssiLeg.setEticketCandidate(true);

			ssmDto.addLeg(ssiLeg);
		}
	}

	public static void addLegsInfo(ASMessegeDTO asmDto, Collection<FlightLeg> legsList) {

		SimpleDateFormat format = new SimpleDateFormat("HHmm");

		for (int i = 0; i < legsList.size(); i++) {

			SSIFlightLegDTO ssiLeg = new SSIFlightLegDTO();

			FlightLeg flighLeg = null;
			Iterator<FlightLeg> itLegsIt = legsList.iterator();
			while (itLegsIt.hasNext()) {

				flighLeg = itLegsIt.next();
				if (flighLeg.getLegNumber() == i + 1) {
					break;
				}
			}

			String depatureTime = null;
			int depatureOffSet = 0;
			String arrivalTime = null;
			int arrivalOffSet = 0;
			if (TimeMode.LOCAL_TIME_MODE.equalsIgnoreCase(asmDto.getTimeMode())) {
				depatureTime = format.format(flighLeg.getEstDepartureTimeLocal());
				depatureOffSet = flighLeg.getEstDepartureDayOffsetLocal();

				arrivalTime = format.format(flighLeg.getEstArrivalTimeLocal());
				arrivalOffSet = flighLeg.getEstArrivalDayOffsetLocal();
			} else {

				depatureTime = format.format(flighLeg.getEstDepartureTimeZulu());
				depatureOffSet = flighLeg.getEstDepartureDayOffset();

				arrivalTime = format.format(flighLeg.getEstArrivalTimeZulu());
				arrivalOffSet = flighLeg.getEstArrivalDayOffset();
			}

			ssiLeg.setDepatureAirport(flighLeg.getOrigin());
			ssiLeg.setDepatureTime(depatureTime);
			ssiLeg.setDepatureOffiset(depatureOffSet);

			ssiLeg.setArrivalAirport(flighLeg.getDestination());
			ssiLeg.setArrivalTime(arrivalTime);
			ssiLeg.setArrivalOffset(arrivalOffSet);
			
			ssiLeg.setEticketCandidate(true);

			asmDto.addLeg(ssiLeg);
		}
	}

	public static void populateSSIMessageDTOforNEW(SSIMessegeDTO ssmDto, FlightSchedule schedule, String serviceType,
			String aircraftType, SSMSplitFlightSchedule splitFlightSchedule, String supplementaryInfo) throws ModuleException {

		// Action Information
		ssmDto.setAction(SSIMessegeDTO.Action.NEW);

		// Add local timings to the schdule
		LocalZuluTimeAdder localZuluTimeAdder = new LocalZuluTimeAdder(getAirportBD());

		if (splitFlightSchedule != null) {
			localZuluTimeAdder.addLocalTimeDetailsToSubSchedule(schedule, splitFlightSchedule);
		} else {
			localZuluTimeAdder.addLocalTimeDetailsToFlightSchedule(schedule, false);
		}

		// fill common fields
		fillCommonSSMFields(ssmDto, schedule, serviceType, aircraftType);

		// Legs Information
		Collection legsList = schedule.getFlightScheduleLegs();
		SchedulePublishUtil.addLegsInfo(ssmDto, legsList);

		// Supplementary Information
		ssmDto.setSupplementaryInfo(supplementaryInfo);

	}

	public static void populateSSIMessageDTOforCNL(SSIMessegeDTO ssmDto, FlightSchedule schedule, String serviceType,
			String aircraftType, SSMSplitFlightSchedule subSchedule, String supplementaryInfo) throws ModuleException {

		// Action Information
		ssmDto.setAction(SSIMessegeDTO.Action.CNL);

		// Add local timings to the schdule
		LocalZuluTimeAdder localZuluTimeAdder = new LocalZuluTimeAdder(getAirportBD());
		if (subSchedule != null) {
			localZuluTimeAdder.addLocalTimeDetailsToSubSchedule(schedule, subSchedule);
		} else {
			localZuluTimeAdder.addLocalTimeDetailsToFlightSchedule(schedule, false);
		}

		// fill common fields
		fillCommonSSMFields(ssmDto, schedule, serviceType, aircraftType);

		// Legs Information
		Collection legsList = schedule.getFlightScheduleLegs();
		SchedulePublishUtil.addLegsInfo(ssmDto, legsList);

		// Supplementary Information
		ssmDto.setSupplementaryInfo(supplementaryInfo);
	}

	public static void populateSSIMessageDTOforEQT(SSIMessegeDTO ssmDto, FlightSchedule schedule, String serviceType,
			String aircraftType, SSMSplitFlightSchedule subSchedule, String supplementaryInfo) throws ModuleException {

		// Action Information
		ssmDto.setAction(SSIMessegeDTO.Action.EQT);

		// Add local timings to the schdule
		LocalZuluTimeAdder localZuluTimeAdder = new LocalZuluTimeAdder(getAirportBD());
		if (subSchedule != null) {
			localZuluTimeAdder.addLocalTimeDetailsToSubSchedule(schedule, subSchedule);
		} else {
			localZuluTimeAdder.addLocalTimeDetailsToFlightSchedule(schedule, false);
		}

		// fill common fields
		fillCommonSSMFields(ssmDto, schedule, serviceType, aircraftType);

		// Supplementary Information
		ssmDto.setSupplementaryInfo(supplementaryInfo);
	}

	public static void populateSSIMessageDTOforRPL(SSIMessegeDTO ssmDto, FlightSchedule schedule, String serviceType,
			String aircraftType, SSMSplitFlightSchedule subSchedule, String supplementaryInfo) throws ModuleException {

		// Action Information
		ssmDto.setAction(SSIMessegeDTO.Action.RPL);

		// Add local timings to the schdule
		LocalZuluTimeAdder localZuluTimeAdder = new LocalZuluTimeAdder(getAirportBD());
		if (subSchedule != null) {
			localZuluTimeAdder.addLocalTimeDetailsToSubSchedule(schedule, subSchedule);
		} else {
			localZuluTimeAdder.addLocalTimeDetailsToFlightSchedule(schedule, false);
		}

		// fill common fields
		fillCommonSSMFields(ssmDto, schedule, serviceType, aircraftType);

		// Legs Information
		Collection legsList = schedule.getFlightScheduleLegs();
		SchedulePublishUtil.addLegsInfo(ssmDto, legsList);

		// Supplementary Information
		ssmDto.setSupplementaryInfo(supplementaryInfo);
	}

	public static void populateSSIMessageDTOforTIM(SSIMessegeDTO ssmDto, FlightSchedule schedule, String serviceType,
			String aircraftType, SSMSplitFlightSchedule subSchedule, String supplementaryInfo) throws ModuleException {

		// Action Information
		ssmDto.setAction(SSIMessegeDTO.Action.TIM);

		// Add local timings to the schdule
		LocalZuluTimeAdder localZuluTimeAdder = new LocalZuluTimeAdder(getAirportBD());
		if (subSchedule != null) {
			localZuluTimeAdder.addLocalTimeDetailsToSubSchedule(schedule, subSchedule);
		} else {
			localZuluTimeAdder.addLocalTimeDetailsToFlightSchedule(schedule, false);
		}

		// fill common fields
		fillCommonSSMFields(ssmDto, schedule, serviceType, aircraftType);

		// Legs Information
		Collection legsList = schedule.getFlightScheduleLegs();
		SchedulePublishUtil.addLegsInfo(ssmDto, legsList);

		// Supplementary Information
		ssmDto.setSupplementaryInfo(supplementaryInfo);
	}

	public static void populateASMforNEW(ASMessegeDTO asmDto, Flight flight, String serviceType, String aircraftType,
			String supplementaryInfo)
			throws ModuleException {

		// Action Information
		asmDto.setAction(ASMessegeDTO.Action.NEW);

		// Add local timings to the schdule
		LocalZuluTimeAdder localZuluTimeAdder = new LocalZuluTimeAdder(getAirportBD());
		localZuluTimeAdder.addLocalTimeDetailsToFlight(flight);

		// fill common fields
		fillCommonASMFields(asmDto, flight, serviceType, aircraftType);

		// Legs Information
		Collection legsList = flight.getFlightLegs();
		SchedulePublishUtil.addLegsInfo(asmDto, legsList);

		// Supplementary Information
		asmDto.setSupplementaryInfo(supplementaryInfo);
	}

	public static void populateASMforCNL(ASMessegeDTO asmDto, Flight flight, String serviceType, String aircraftType,
			String supplementaryInfo)
			throws ModuleException {

		// Action Information
		asmDto.setAction(ASMessegeDTO.Action.CNL);

		// Add local timings to the schdule
		LocalZuluTimeAdder localZuluTimeAdder = new LocalZuluTimeAdder(getAirportBD());
		localZuluTimeAdder.addLocalTimeDetailsToFlight(flight);

		// fill common fields
		fillCommonASMFields(asmDto, flight, serviceType, aircraftType);

		// Legs Information
		Collection legsList = flight.getFlightLegs();
		SchedulePublishUtil.addLegsInfo(asmDto, legsList);

		// Supplementary Information
		asmDto.setSupplementaryInfo(supplementaryInfo);
	}

	public static void populateASMforEQT(ASMessegeDTO asmDto, Flight flight, String serviceType, String aircraftType,
			String supplementaryInfo)
			throws ModuleException {

		// Action Information
		asmDto.setAction(ASMessegeDTO.Action.EQT);

		// Add local timings to the flight
		LocalZuluTimeAdder localZuluTimeAdder = new LocalZuluTimeAdder(getAirportBD());
		localZuluTimeAdder.addLocalTimeDetailsToFlight(flight);

		// fill common fields
		fillCommonASMFields(asmDto, flight, serviceType, aircraftType);

		// Supplementary Information
		asmDto.setSupplementaryInfo(supplementaryInfo);
	}

	public static void populateASMforRPL(ASMessegeDTO asmDto, Flight flight, String serviceType, String aircraftType,
			String supplementaryInfo)
			throws ModuleException {

		// Action Information
		asmDto.setAction(ASMessegeDTO.Action.RPL);

		// Add local timings to the schdule
		LocalZuluTimeAdder localZuluTimeAdder = new LocalZuluTimeAdder(getAirportBD());
		localZuluTimeAdder.addLocalTimeDetailsToFlight(flight);

		// fill common fields
		fillCommonASMFields(asmDto, flight, serviceType, aircraftType);

		// Legs Information
		Collection legsList = flight.getFlightLegs();
		SchedulePublishUtil.addLegsInfo(asmDto, legsList);

		// Supplementary Information
		asmDto.setSupplementaryInfo(supplementaryInfo);
	}

	public static void populateASMforTIM(ASMessegeDTO asmDto, Flight flight, String serviceType, String aircraftType,
			String supplementaryInfo)
			throws ModuleException {

		// Action Information
		asmDto.setAction(ASMessegeDTO.Action.TIM);

		// Add local timings to the schdule
		LocalZuluTimeAdder localZuluTimeAdder = new LocalZuluTimeAdder(getAirportBD());
		localZuluTimeAdder.addLocalTimeDetailsToFlight(flight);

		// fill common fields
		fillCommonASMFields(asmDto, flight, serviceType, aircraftType);

		// Legs Information
		Collection legsList = flight.getFlightLegs();
		SchedulePublishUtil.addLegsInfo(asmDto, legsList);

		// Supplementary Information
		asmDto.setSupplementaryInfo(supplementaryInfo);
	}

	public static String getGeneratedReferenceNumber() {

		String nextVal = GDSServicesModuleUtil.getGlobalConfig().getNextValueFromSequence(GDS_REF_SEQUENCE_NAME).toString();
		GregorianCalendar calandar = new GregorianCalendar();
		SimpleDateFormat ddMMM = new SimpleDateFormat("ddMMM");

		int lenght = 5 - nextVal.length();
		for (int i = 0; i < lenght; i++) {
			nextVal = "0" + nextVal;
		}

		String referenceNumber = ddMMM.format(calandar.getTime()) + nextVal + GDS_REF_SUFFIX;

		return referenceNumber;
	}

	private static void
			fillCommonSSMFields(SSIMessegeDTO ssmDto, FlightSchedule schedule, String serviceType, String aircraftType) {

		// TODO: TimeMode should configurable - AARESAA-24335
		ssmDto.setTimeMode(TimeMode.LOCAL_TIME_MODE);
		// reference Number
		ssmDto.setReferenceNumber(getGeneratedReferenceNumber());

		// Action Information
		ssmDto.setWithdrawal(false);

		// Flight Information
		ssmDto.setFlightDesignator(schedule.getFlightNumber());

		// Period and Frequency
		Frequency frequency = null;
		if (TimeMode.LOCAL_TIME_MODE.equalsIgnoreCase(ssmDto.getTimeMode())) {
			ssmDto.setStartDate(schedule.getStartDateLocal());
			ssmDto.setEndDate(schedule.getStopDateLocal());
			frequency = schedule.getFrequencyLocal();
		} else {
			ssmDto.setStartDate(schedule.getStartDate());
			ssmDto.setEndDate(schedule.getStopDate());
			frequency = schedule.getFrequency();
		}

		SchedulePublishUtil.addDaysOfOperation(ssmDto, frequency);

		// update service type
		String iataServiceType = GDSApiUtils.getIATAServiceTypeForAAOperationId(schedule.getOperationTypeId(), false);
		ssmDto.setServiceType(iataServiceType);
		if (!StringUtil.isNullOrEmpty(iataServiceType)) {
			ssmDto.setServiceType(iataServiceType);
		} else {
			ssmDto.setServiceType(serviceType);
		}

		// Equipment Information
		ssmDto.setAircraftType(aircraftType);

		// TODO:in future need to support leg wise in order to support multi-leg code shared flights,temp we have added
		// Segment Information
		ssmDto.setExternalFlightNos(getCodeShareFlightScheduleNos(schedule.getCodeShareMCFlightSchedules()));
		if (ssmDto.getExternalFlightNos() != null && !ssmDto.getExternalFlightNos().isEmpty()) {
			ssmDto.setSegmentCode(getScheduleOriginDestination(schedule));
		}
	}

	private static void fillCommonASMFields(ASMessegeDTO asmDto, Flight flight, String serviceType, String aircraftType) {

		// TODO: TimeMode should configurable - AARESAA-24335
		asmDto.setTimeMode(TimeMode.LOCAL_TIME_MODE);

		// reference Number
		asmDto.setReferenceNumber(getGeneratedReferenceNumber());

		// set the flight identifier

		Date departureDate = flight.getDepartureDate();

		if (TimeMode.LOCAL_TIME_MODE.equalsIgnoreCase(asmDto.getTimeMode())) {
			departureDate = flight.getDepartureDateLocal();
		}
		SimpleDateFormat ddMMMyyy = new SimpleDateFormat("ddMMMyy");
		String flightIdentifier = flight.getFlightNumber() + "/" + ddMMMyyy.format(departureDate);
		asmDto.setFlightIdentifier(flightIdentifier);

		// update service type
		String iataServiceType = GDSApiUtils.getIATAServiceTypeForAAOperationId(flight.getOperationTypeId(), true);
		if (!StringUtil.isNullOrEmpty(iataServiceType)) {
			asmDto.setServiceType(iataServiceType);
		} else {
			asmDto.setServiceType(serviceType);
		}

		// Equipment Information
		asmDto.setAircraftType(aircraftType);

		// TODO:in future need to support leg wise in order to support multi-leg code shared flights,temp we have added
		asmDto.setExternalFlightNos(getCodeShareFlightNos(flight.getCodeShareMCFlights()));

		if (asmDto.getExternalFlightNos() != null && !asmDto.getExternalFlightNos().isEmpty()) {
			String segmentCode = flight.getOriginAptCode() + "" + flight.getDestinationAptCode();
			asmDto.setSegmentCode(segmentCode);
		}
	}

	private static AirportBD getAirportBD() {
		return GDSServicesModuleUtil.getAirportBD();
	}

	private static List<String> getCodeShareFlightNos(Set<CodeShareMCFlight> externalFlightNos) {
		List<String> list = new ArrayList<String>();
		if (externalFlightNos != null && !externalFlightNos.isEmpty()) {
			for (CodeShareMCFlight externalFlightNo : externalFlightNos)
				list.add(externalFlightNo.getCsMCFlightNumber());
		}
		return list;
	}

	private static List<String> getCodeShareFlightScheduleNos(Set<CodeShareMCFlightSchedule> externalFlightNos) {
		List<String> list = new ArrayList<String>();
		if (externalFlightNos != null && !externalFlightNos.isEmpty()) {
			for (CodeShareMCFlightSchedule externalFlightNo : externalFlightNos)
				list.add(externalFlightNo.getCsMCFlightNumber());
		}
		return list;
	}

	private static String getScheduleOriginDestination(FlightSchedule schedule) {
		String segmentCode = LegUtil.getFirstScheduleLeg(schedule.getFlightScheduleLegs()).getOrigin() + ""
				+ LegUtil.getLastScheduleLeg(schedule.getFlightScheduleLegs()).getDestination();
		return segmentCode;
	}

	public static void
			populateSSIMessageDTOforREV(SSIMessegeDTO ssmDto, FlightSchedule schedule, FlightSchedule existingSchedule,
 SSMSplitFlightSchedule updatedSubSchedule,
			SSMSplitFlightSchedule existingSubSchedule, String supplementaryInfo) throws ModuleException {

		// Action Information
		ssmDto.setAction(SSIMessegeDTO.Action.REV);

		// Add local timings to the schdule
		LocalZuluTimeAdder localZuluTimeAdder = new LocalZuluTimeAdder(getAirportBD());
		localZuluTimeAdder.addLocalTimeDetailsToSubSchedule(existingSchedule, existingSubSchedule);

		// Add local timings to the schdule
		LocalZuluTimeAdder localZuluTimeAdderForNewSchedule = new LocalZuluTimeAdder(getAirportBD());
		localZuluTimeAdderForNewSchedule.addLocalTimeDetailsToSubSchedule(schedule, updatedSubSchedule);

		// fill common fields
		fillCommonSSMFields(ssmDto, existingSchedule, null, null);

		// Updated Period & Frequency information
		if (TimeMode.LOCAL_TIME_MODE.equalsIgnoreCase(ssmDto.getTimeMode())) {
			ssmDto.setNewStartDate(schedule.getStartDateLocal());
			ssmDto.setNewEndDate(schedule.getStopDateLocal());

			ssmDto.setNewDaysOfOperation(getDaysOfOperation(schedule.getFrequencyLocal()));
		} else {
			ssmDto.setNewStartDate(schedule.getStartDate());
			ssmDto.setNewEndDate(schedule.getStopDate());

			ssmDto.setNewDaysOfOperation(getDaysOfOperation(schedule.getFrequency()));
		}

		// Supplementary Information
		ssmDto.setSupplementaryInfo(supplementaryInfo);
	}

	public static void populateSSIMessageDTOforFLT(SSIMessegeDTO ssmDto, FlightSchedule schedule, String serviceType,
			FlightSchedule existingSchedule, String aircraftType, SSMSplitFlightSchedule subSchedule, String supplementaryInfo)
			throws ModuleException {

		// Action Information
		ssmDto.setAction(SSIMessegeDTO.Action.FLT);

		// Add local timings to the schdule
		LocalZuluTimeAdder localZuluTimeAdder = new LocalZuluTimeAdder(getAirportBD());
		if (subSchedule != null) {
			localZuluTimeAdder.addLocalTimeDetailsToSubSchedule(schedule, subSchedule);
		} else {
			localZuluTimeAdder.addLocalTimeDetailsToFlightSchedule(schedule, false);
		}

		// fill common fields
		fillCommonSSMFields(ssmDto, existingSchedule, serviceType, aircraftType);

		ssmDto.setNewFlightDesignator(schedule.getFlightNumber());

		// Supplementary Information
		ssmDto.setSupplementaryInfo(supplementaryInfo);
	}

	public static void populateASMforFLT(ASMessegeDTO asmDto, Flight flight, String serviceType, Flight existingFlight,
			String aircraftType, String supplementaryInfo) throws ModuleException {

		// Action Information
		asmDto.setAction(ASMessegeDTO.Action.FLT);

		// Add local timings to the flight
		LocalZuluTimeAdder localZuluTimeAdder = new LocalZuluTimeAdder(getAirportBD());
		localZuluTimeAdder.addLocalTimeDetailsToFlight(flight);

		// fill common fields
		fillCommonASMFields(asmDto, existingFlight, serviceType, aircraftType);

		// Supplementary Information
		asmDto.setSupplementaryInfo(supplementaryInfo);

		// set the new flight identifier for FLT
		Date departureDate = flight.getDepartureDate();

		if (TimeMode.LOCAL_TIME_MODE.equalsIgnoreCase(asmDto.getTimeMode())) {
			departureDate = flight.getDepartureDateLocal();
		}
		SimpleDateFormat ddMMMyyy = new SimpleDateFormat("ddMMMyy");
		String newFlightIdentifier = flight.getFlightNumber() + "/" + ddMMMyyy.format(departureDate);
		asmDto.setNewFlightIdentifier(newFlightIdentifier);
	}

	private static List<SSIMessegeDTO.DayOfWeek> getDaysOfOperation(Frequency frequency) {
		List<SSIMessegeDTO.DayOfWeek> listOfWeek = new ArrayList<SSIMessegeDTO.DayOfWeek>();

		Collection<DayOfWeek> days = CalendarUtil.getDaysFromFrequency(frequency);
		Iterator<DayOfWeek> itDays = days.iterator();

		while (itDays.hasNext()) {
			DayOfWeek day = itDays.next();
			if (day.equals(DayOfWeek.MONDAY)) {
				listOfWeek.add(SSIMessegeDTO.DayOfWeek.MONDAY);
			} else if (day.equals(DayOfWeek.TUESDAY)) {
				listOfWeek.add(SSIMessegeDTO.DayOfWeek.TUESDAY);
			} else if (day.equals(DayOfWeek.WEDNESDAY)) {
				listOfWeek.add(SSIMessegeDTO.DayOfWeek.WEDNESDAY);
			} else if (day.equals(DayOfWeek.THURSDAY)) {
				listOfWeek.add(SSIMessegeDTO.DayOfWeek.THURSDAY);
			} else if (day.equals(DayOfWeek.FRIDAY)) {
				listOfWeek.add(SSIMessegeDTO.DayOfWeek.FRIDAY);
			} else if (day.equals(DayOfWeek.SATURDAY)) {
				listOfWeek.add(SSIMessegeDTO.DayOfWeek.SATURDAY);
			} else if (day.equals(DayOfWeek.SUNDAY)) {
				listOfWeek.add(SSIMessegeDTO.DayOfWeek.SUNDAY);
			}
		}
		return listOfWeek;
	}

	public static Collection<SSMSplitFlightSchedule> addSubSchedule(Collection<SSMSplitFlightSchedule> subScheduleColl,
			FlightSchedule schedule) throws ModuleException {

		if (subScheduleColl == null || subScheduleColl.isEmpty()) {
			LocalZuluTimeAdder localZuluTimeAdder = new LocalZuluTimeAdder(AirSchedulesUtil.getAirportBD());
			localZuluTimeAdder.addLocalTimeDetailsToFlightSchedule(schedule, false);
			subScheduleColl = new ArrayList<SSMSplitFlightSchedule>();
			SSMSplitFlightSchedule splitFlightSchedule = new SSMSplitFlightSchedule();
			splitFlightSchedule.setStartDate(schedule.getStartDate());
			splitFlightSchedule.setStopDate(schedule.getStopDate());
			splitFlightSchedule.setStartDateLocal(schedule.getStartDateLocal());
			splitFlightSchedule.setStopDateLocal(schedule.getStopDateLocal());
			splitFlightSchedule.setScheduleId(schedule.getScheduleId());
			splitFlightSchedule.setSsmSplitFlightScheduleLeg(LegUtil.adaptScheduleLegs(schedule.getFlightScheduleLegs()));

			subScheduleColl.add(splitFlightSchedule);
		}

		return subScheduleColl;
	}
}
