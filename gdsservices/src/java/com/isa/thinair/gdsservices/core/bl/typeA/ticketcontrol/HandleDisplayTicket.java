package com.isa.thinair.gdsservices.core.bl.typeA.ticketcontrol;

import java.util.List;

import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageHandler;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.dto.temp.GdsReservation;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerTicketControlInformationWrapper;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSServicesSupportJDBCDAO;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.GdsInfoDaoHelper;
import com.isa.thinair.gdsservices.core.util.TypeANotes;
import com.isa.typea.common.ErrorInformation;
import com.isa.typea.common.MessageFunction;
import com.isa.typea.common.TicketNumberDetails;
import com.isa.typea.common.TravellerTicketControlInformation;
import com.isa.typea.tkcreq.AATKCREQ;
import com.isa.typea.tkcreq.TKCREQMessage;
import com.isa.typea.tkcres.AATKCRES;
import com.isa.typea.tkcres.TKCRESMessage;

public class HandleDisplayTicket extends EdiMessageHandler {
	private AATKCREQ tkcReq;
	private AATKCRES tkcRes;

	protected void handleMessage() {

		tkcReq = (AATKCREQ) messageDTO.getRequest();
		TKCREQMessage tktreqMessage = tkcReq.getMessage();

		tkcRes = new AATKCRES();
		TKCRESMessage tktresMessage = new TKCRESMessage();
		tktresMessage.setMessageHeader(GDSDTOUtil.createRespMessageHeader(tktreqMessage.getMessageHeader(),
				messageDTO.getRequestMetaDataDTO().getMessageReference(), messageDTO.getRequestMetaDataDTO().getIataOperation().getResponse()));
		//tktresMessage.setOriginatorInformation(tktreqMessage.getOriginatorInformation());

		tkcRes.setHeader(GDSDTOUtil.createRespEdiHeader(tkcReq.getHeader(), tktresMessage.getMessageHeader(), messageDTO));
		tkcRes.setMessage(tktresMessage);

		MessageFunction messageFunction = new MessageFunction();
		messageFunction.setMessageFunction(TypeAConstants.MessageFunction.DISPLAY);
		messageFunction.setResponseType(TypeAConstants.ResponseType.PROCESSED_SUCCESSFULLY);
		tktresMessage.setMessageFunction(messageFunction);


		try {

			if (!tktreqMessage.getTickets().isEmpty()) {
				exactMatch();
			} else {
				throw new GdsTypeAException();
			}
		} catch (GdsTypeAException e) {
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(e.getEdiErrorCode());
			tktresMessage.setErrorInformation(errorInformation);
		} catch (Exception e) {
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR);
			tktresMessage.setErrorInformation(errorInformation);
		}

		messageDTO.setResponse(tkcRes);

	}

	private void exactMatch() throws GdsTypeAException {
		TravellerTicketControlInformation displayTicket;

		TKCRESMessage tkcresMessage = tkcRes.getMessage();
		List<TravellerTicketControlInformation> displayTickets = tkcresMessage.getTravellers();

		GdsReservation<TravellerTicketControlInformationWrapper> gdsReservation;
		GDSServicesSupportJDBCDAO gdsServicesSupportDao = GDSServicesDAOUtils.DAOInstance.GDSSERVICES_SUPPORT_JDBC_DAO;

		String ticketNumber = tkcReq.getMessage().getTickets().get(0).getTicketNumber();
		String pnr;

		try {
			pnr = gdsServicesSupportDao.getPnrFromExternalTicketNumber(ticketNumber);
		} catch (Exception e) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.TICKET_NUMBER_NOT_FOUND,
					TypeANotes.ErrorMessages.TICKET_NUMBER_DOES_NOT_EXIST);
		}

		gdsReservation = GdsInfoDaoHelper.getGdsReservationTicketControlView(pnr);

		TravellerTicketControlInformationWrapper travellerWrapper = resolvePassenger(gdsReservation, ticketNumber);
		TravellerTicketControlInformation travellerImage = travellerWrapper.getTravellerInformation();

		displayTicket = travellerImage;
		displayTickets.add(displayTicket);
	}


	private TravellerTicketControlInformationWrapper resolvePassenger(GdsReservation<TravellerTicketControlInformationWrapper> gdsReservation,
	                                                              String ticketNumber) {
		TravellerTicketControlInformationWrapper travellerWrapper = null;

		l0:
		for (TravellerTicketControlInformationWrapper traveller : gdsReservation.getTravellerInformation()) {
			for (TicketNumberDetails ticket : traveller.getTravellerInformation().getTickets()) {
				if(ticketNumber.equals(ticket.getTicketNumber())) {
					travellerWrapper = traveller;
					break l0;
				}
			}
		}

		return travellerWrapper;
	}

}
