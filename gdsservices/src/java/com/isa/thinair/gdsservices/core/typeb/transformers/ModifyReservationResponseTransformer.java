package com.isa.thinair.gdsservices.core.typeb.transformers;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ProcessStatus;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

public class ModifyReservationResponseTransformer {
	/**
	 * transforms reservationResponseMap back to bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param reservationResponseMap
	 * @return
	 */
	public static BookingRequestDTO getResponse(BookingRequestDTO bookingRequestDTO,
			Map<ReservationAction, GDSReservationRequestBase> reservationResponseMap) {

		Collection<GDSReservationRequestBase> reservationRequests = reservationResponseMap.values();

		for (Iterator<GDSReservationRequestBase> iterator = reservationRequests.iterator(); iterator.hasNext();) {
			GDSReservationRequestBase reservationRequest = (GDSReservationRequestBase) iterator.next();

			if (reservationRequest.getActionType() == ReservationAction.CHANGE_PAX_DETAILS) {
				bookingRequestDTO = ChangePaxDetailsResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
			} else if (reservationRequest.getActionType() == ReservationAction.CHANGE_CONTACT_DETAILS) {
				bookingRequestDTO = ChangeContactDetailsResponseTransformer
						.getResponse(bookingRequestDTO, reservationResponseMap);
			} else if (reservationRequest.getActionType() == ReservationAction.UPDATE_EXTERNAL_SEGMENTS) {
				bookingRequestDTO = UpdateExternalSegmentsResponseTransformer.getResponse(bookingRequestDTO,
						reservationResponseMap);
			} else if (reservationRequest.getActionType() == ReservationAction.ADD_SSR) {
				bookingRequestDTO = AddSSRResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
			} else if (reservationRequest.getActionType() == ReservationAction.REMOVE_SSR) {
				bookingRequestDTO = RemoveSSRResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
			} else if (reservationRequest.getActionType() == ReservationAction.ISSUE_ETICKET) {
				bookingRequestDTO = IssueEticketResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
			} else if (reservationRequest.getActionType() == ReservationAction.MODIFY_SEGMENT) {
				bookingRequestDTO = ModifySegmentResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
			} else if (reservationRequest.getActionType() == ReservationAction.CANCEL_SEGMENT) {
				bookingRequestDTO = CancelSegmentResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
			} else if (reservationRequest.getActionType() == ReservationAction.ADD_SEGMENT) {
				bookingRequestDTO = AddSegmentResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
			} else if (reservationRequest.getActionType() == ReservationAction.ADD_INFANT) {
				bookingRequestDTO = AddInfantResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
			} else if (reservationRequest.getActionType() == ReservationAction.REMOVE_PAX) {
				bookingRequestDTO = RemovePaxResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
			} else if (reservationRequest.getActionType() == ReservationAction.MAKE_PAYMENT) {
				bookingRequestDTO = MakePaymentResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
			} else if (reservationRequest.getActionType() == ReservationAction.CHECK_OLD_SEGMENTS) {
				bookingRequestDTO = CheckOldSegmentsResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
			} else if (reservationRequest.getActionType() == ReservationAction.SPLIT_RESERVATION) {
				bookingRequestDTO = SplitReservationResponseTransformer.setPnrReference(bookingRequestDTO, reservationRequest);
			} else if (reservationRequest.getActionType() == ReservationAction.CONFIRM_BOOKING) {
				bookingRequestDTO = ConfirmBookingResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
			} else if (reservationRequest.getActionType() == ReservationAction.EXTEND_TIMELIMIT) {
				bookingRequestDTO = ExtendTimelimitResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap);
			}

			if (!checkSuccess(bookingRequestDTO))
				break;
		}

		return bookingRequestDTO;
	}

	/**
	 * Checks whether the bookingRequestDTO in success state.
	 * 
	 * @param bookingRequestDTO
	 * @return
	 */
	private static boolean checkSuccess(BookingRequestDTO bookingRequestDTO) {
		return bookingRequestDTO.getProcessStatus() == ProcessStatus.INVOCATION_SUCCESS;
	}
}
