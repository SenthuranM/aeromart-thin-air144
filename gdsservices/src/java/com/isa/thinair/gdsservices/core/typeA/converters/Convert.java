package com.isa.thinair.gdsservices.core.typeA.converters;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ddtek.xmlconverter.Converter;
import com.ddtek.xmlconverter.ConverterFactory;
import com.ddtek.xmlconverter.adapter.edi.EDIConverterException;
import com.ddtek.xmlconverter.exception.ConverterException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.config.TypeAMessageConfig;
import com.isa.thinair.gdsservices.core.dto.EDIMessageDTO;
import com.isa.thinair.gdsservices.core.dto.EDIMessageMetaDataDTO;
import com.isa.thinair.gdsservices.core.service.ResourceLocator;
import com.isa.thinair.gdsservices.core.service.bd.ResourceLocatorImpl;
import com.isa.thinair.gdsservices.core.typeA.helpers.StringOutputStream;
import com.isa.thinair.gdsservices.core.typeA.model.IataOperationConfig;
import com.isa.thinair.gdsservices.core.util.GDSConstants;
import com.isa.thinair.gdsservices.core.util.TypeADataConvertorUtill;
import com.isa.typea.common.BaseEDIRequest;
import com.isa.typea.common.BaseEDIResponse;

public class Convert {

	private static Log log = LogFactory.getLog(Convert.class);
	// Schemas are generated using the following URL.This needs to go with the URL that is used in the converter
	private static final String DATA_DIRECT_XML_CONVERTER_URI = "converter:EDI:count=yes:dialect=IATA:decode=no:eol=no:newline=unix:seg=yes:strict=yes:typ=no:setup=never:clean=never:chr=REPLACE:invalid=\u0000:release=?"; // opt=yes:val=no:

	public static String ediToXML(String ediMessage) throws ConverterException {

		TypeAMessageConfig typeAMessageConfig = GDSServicesModuleUtil.getConfig().getTypeAMessageConfig();

		StreamSource converterSource = new StreamSource(new StringReader(ediMessage));
		StreamResult converterResult = new StreamResult(new StringOutputStream());

		// TODO make it singleton
		ConverterFactory factory = new ConverterFactory();
		factory.newConvertToXML(typeAMessageConfig.getDataDirectEdiXmlUrl()).convert(converterSource, converterResult);

		return converterResult.getOutputStream().toString();
	}

	public static String xmlToEdi(String ediXML) throws ConverterException {

		ConverterFactory factory = new ConverterFactory();

		// TODO make it singleton
		StreamSource converterSource = new StreamSource(new StringReader(ediXML));
		StreamResult converterResult = new StreamResult(new StringOutputStream());
		Converter fromXml = factory.newConvertFromXML(DATA_DIRECT_XML_CONVERTER_URI);
		fromXml.convert(converterSource, converterResult);

		return converterResult.getOutputStream().toString();
	}

	@Deprecated
	public static EDIMessageDTO toAccelAeroRequestDto(String ediMessage, TypeAConstants.MessageFlow messageFlow)
			throws ModuleException {

		// TODO -- need to handle messageFlow = INITIATE : currently doesn't process response

		ByteArrayInputStream aaDtoStream = null;
		EDIMessageDTO ediMessageDTO = new EDIMessageDTO(messageFlow);

		try {

			byte[] ediXmlMessage = parseEdiMessage(GDSConstants.MessageTransformationDirection.EDI_TO_AA, ediMessage.getBytes());

			EDIMessageMetaDataDTO metaDataDTO = TypeADataConvertorUtill.extractEdiOperationName(ediXmlMessage);
			String version = metaDataDTO.getFullyQualifiedVersion();
			IATAOperation operationType = metaDataDTO.getIataOperation();

			String transformerFile = IataOperationConfig.getTransformationFile(operationType,
					GDSConstants.MessageTransformationDirection.EDI_TO_AA, version);

			byte[] aaDtoXml = transformXml(transformerFile, ediXmlMessage);
			aaDtoStream = new ByteArrayInputStream(aaDtoXml);

			JAXBContext jaxbContext = getJaxbContext(operationType);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			BaseEDIRequest aaDto = (BaseEDIRequest) unmarshaller.unmarshal(aaDtoStream);

			ediMessageDTO.setRequestMetaDataDTO(metaDataDTO);
			ediMessageDTO.setRequest(aaDto);

		} catch (ModuleException e) {
			throw e;
		} catch (Exception e) {
			throw new ModuleException(e, "");
		} finally {
			if (aaDtoStream != null) {
				try {
					aaDtoStream.close();
				} catch (IOException e) {
				}
			}
		}
		return ediMessageDTO;
	}


	public static void processInboundMessage(EDIMessageDTO ediMessageDTO) throws ModuleException {

		ByteArrayInputStream aaDtoStream = null;
		TypeAConstants.MessageFlow messageFlow = ediMessageDTO.getMessageFlow();
		String message = null;

		switch (messageFlow) {
		case INITIATE:
			message =  ediMessageDTO.getRawResponse();
			break;
		case RESPOND:
			message = ediMessageDTO.getRawRequest();
			break;
		}


		try {

			byte[] ediXmlMessage = parseEdiMessage(GDSConstants.MessageTransformationDirection.EDI_TO_AA, message.getBytes());

			EDIMessageMetaDataDTO metaDataDTO = TypeADataConvertorUtill.extractEdiOperationName(ediXmlMessage);
			String version = metaDataDTO.getFullyQualifiedVersion();
			IATAOperation operationType = metaDataDTO.getIataOperation();

			String transformerFile = IataOperationConfig.getTransformationFile(operationType,
					GDSConstants.MessageTransformationDirection.EDI_TO_AA, version);

			byte[] aaDtoXml = transformXml(transformerFile, ediXmlMessage);
			aaDtoStream = new ByteArrayInputStream(aaDtoXml);

			JAXBContext jaxbContext = getJaxbContext(operationType);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			switch (messageFlow) {
			case INITIATE:
				ediMessageDTO.setResponseMetaDataDTO(metaDataDTO);
				BaseEDIResponse aaDtoRs = (BaseEDIResponse) unmarshaller.unmarshal(aaDtoStream);
				ediMessageDTO.setResponse(aaDtoRs);
				break;
			case RESPOND:
				ediMessageDTO.setRequestMetaDataDTO(metaDataDTO);
				BaseEDIRequest aaDtoRq = (BaseEDIRequest) unmarshaller.unmarshal(aaDtoStream);
				ediMessageDTO.setRequest(aaDtoRq);
				break;
			}


		} catch (ModuleException e) {
			throw e;
		} catch (Exception e) {
			throw new ModuleException(e, "");
		} finally {
			if (aaDtoStream != null) {
				try {
					aaDtoStream.close();
				} catch (IOException e) {
				}
			}
		}
	}



	public static String toEdiMessage(EDIMessageDTO messageDTO) throws ModuleException {
		String ediMessage = "";
		ByteArrayOutputStream aaDtoXmlStream = null;
		IATAOperation operationType = null ;
		Object aaDto = null;

		try {
			EDIMessageMetaDataDTO metaDataDTO = messageDTO.getRequestMetaDataDTO();
			String version = metaDataDTO.getFullyQualifiedVersion();

			switch (messageDTO.getMessageFlow()) {
			case INITIATE:
				operationType = metaDataDTO.getIataOperation();
				aaDto =  messageDTO.getRequest();
				break;
			case RESPOND:
				operationType = metaDataDTO.getIataOperation().getResponse();
				aaDto = messageDTO.getResponse();
                break;
			}

			aaDtoXmlStream = new ByteArrayOutputStream();
			JAXBContext jaxbContext = getJaxbContext(operationType);
			jaxbContext.createMarshaller().marshal(aaDto, aaDtoXmlStream);

			String transformerFile = IataOperationConfig.getTransformationFile(operationType,
					GDSConstants.MessageTransformationDirection.AA_TO_EDI, version);

			byte[] rawEdiXml = transformXml(transformerFile, aaDtoXmlStream.toByteArray());

			byte[] rawEdiMsg = parseEdiMessage(GDSConstants.MessageTransformationDirection.AA_TO_EDI, rawEdiXml);

			ediMessage = new String(rawEdiMsg);
		} catch (ModuleException e) {
			throw e;
		} catch (Exception e) {
			throw new ModuleException("", e);
		} finally {
			if (aaDtoXmlStream != null) {
				try {
					aaDtoXmlStream.close();
				} catch (IOException e) {
				}
			}
		}
		return ediMessage;

	}

	private static JAXBContext getJaxbContext(IATAOperation iataOperation) throws ModuleException {
		JAXBContext jaxbContext = null;
		String aaDtoPkg = "com.isa.typea." + iataOperation.name().toLowerCase();
		try {
			jaxbContext = JAXBContext.newInstance(aaDtoPkg + ":com.isa.typea.common");

		} catch (JAXBException e) {
			throw new ModuleException("", e);
		}

		return jaxbContext;
	}

	private static byte[] transformXml(String transformerFile, byte[] input) throws ModuleException {
		byte[] result = null;
		ByteArrayInputStream inputStream = null;
		ByteArrayOutputStream outputStream = null;
		final ResourceLocator resourceLocator = ResourceLocatorImpl.getInstance();

		try {

			InputStream transformerStream = new ByteArrayInputStream(resourceLocator.getContent(transformerFile));

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			transformerFactory.setURIResolver(new URIResolver() {
				public Source resolve(String href, String s) throws TransformerException {
					ByteArrayInputStream is = new ByteArrayInputStream(resourceLocator.getContent(href));
					Source source = new StreamSource(is);
					return source;
				}
			});
			Transformer transformer = transformerFactory.newTransformer(new StreamSource(transformerStream));

			inputStream = new ByteArrayInputStream(input);
			StreamSource inputSource = new StreamSource(inputStream);
			outputStream = new ByteArrayOutputStream();
			StreamResult aaXmlResult = new StreamResult(outputStream);
			transformer.transform(inputSource, aaXmlResult);

			result = outputStream.toByteArray();
		} catch (TransformerConfigurationException e) {
			log.error("Transform Config error", e);
		} catch (TransformerException e) {
			log.error("Transform error", e);
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
				}
			}

			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
				}
			}
		}

		return result;
	}

	private static byte[] parseEdiMessage(GDSConstants.MessageTransformationDirection direction, byte[] source)
			throws ModuleException {
		byte[] result = null;
		Converter converter = null;

		ByteArrayInputStream inStream = null;
		ByteArrayOutputStream outStream = null;

		try {
			ConverterFactory factory = new ConverterFactory();
			switch (direction) {
			case EDI_TO_AA:
				converter = factory.newConvertToXML(DATA_DIRECT_XML_CONVERTER_URI);
				break;
			case AA_TO_EDI:
				converter = factory.newConvertFromXML(DATA_DIRECT_XML_CONVERTER_URI);
				break;
			}

			inStream = new ByteArrayInputStream(source);
			StreamSource streamSource = new StreamSource(inStream);
			outStream = new ByteArrayOutputStream();
			StreamResult streamResult = new StreamResult(outStream);
			converter.convert(streamSource, streamResult);

			result = outStream.toByteArray();
		} catch (EDIConverterException e) {
			log.error("EDIConverterException error", e);
			CodeSetEnum.CS0085 cs0085 = CodeSetEnum.CS0085.getCS0085ByCode(Integer.parseInt(e.getNativeCode()));
			if (cs0085 == null) {
				cs0085 = CodeSetEnum.CS0085.UNSPECIFIED_ERROR;
			}
			throw new InteractiveEDIException(cs0085, e.getMessage());
		} catch (ConverterException e) {
			log.error("Converter error", e);
			throw new InteractiveEDIException(CodeSetEnum.CS0085.UNSPECIFIED_ERROR, "Unknown Error Occurred");
		} finally {
			if (inStream != null) {
				try {
					inStream.close();
				} catch (IOException e) {
				}
			}

			if (outStream != null) {
				try {
					outStream.close();
				} catch (IOException e) {
				}
			}
		}
		return result;
	}
}