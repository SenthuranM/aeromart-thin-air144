/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.core.remoting.ejb;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBElement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserDST;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.core.typeA.converters.Convert;
import com.isa.thinair.gdsservices.core.typeA.marshallers.Marshaller;
import com.isa.thinair.gdsservices.core.typeA.model.IataOperationConfig;
import com.isa.thinair.gdsservices.core.typeA.model.InterchangeHeader;
import com.isa.thinair.gdsservices.core.typeA.model.MessageHeader;
import com.isa.thinair.gdsservices.core.typeA.transformers.IATARequest;
import com.isa.thinair.gdsservices.core.typeA.transformers.Transformer;
import com.isa.thinair.login.util.DatabaseUtil;
import com.isa.thinair.login.util.ForceLoginInvoker;

@MessageDriven(name = "GDSTypeASubscriberMDB", activationConfig = {
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/isaGDSTypeAQueue"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge") })
public class GDSTypeASubscriberMDB implements MessageListener {
	private static final long serialVersionUID = 1L;
	private final Log log = LogFactory.getLog(GDSTypeASubscriberMDB.class);

	public void onMessage(Message message) {

		String ediMessage = "";
		String ediResult = "";

		IATARequest requestHandler = null;
		DefaultServiceResponse responseParams = new DefaultServiceResponse(true);
		InterchangeHeader iHeader = null;
		MessageHeader mHeader = null;
		IATAOperation ediOperation = null;
		JAXBElement<?> objectResponse = null;

		try {

			// FIXME
			String userID = "ABYSYSTEM";
			String password = "3O#pass123";
			ForceLoginInvoker.hahnAirLogin("ABYSYSTEM", "3O#pass123");

			// FIXME user of aaSession manager ones the connection details are finalized.
			User user = GDSServicesModuleUtil.getSecurityBD().getUserBasicDetails(userID);
			Agent agent = GDSServicesModuleUtil.getTravelAgentBD().getAgent(user.getAgentCode());
			Collection<UserDST> colUserDST = DatabaseUtil.getAgentLocalTime(agent.getStationCode());
			Collection userCarriers = DatabaseUtil.getCarrierCodes(userID);
			int salesChannelId = SalesChannelsUtil.SALES_CHANNEL_GDS;
			UserPrincipal userPrincipal = (UserPrincipal) UserPrincipal.createIdentity(userID, salesChannelId,
					user.getAgentCode(), agent.getStationCode(), null, userID, colUserDST, password, userCarriers,
					user.getDefaultCarrierCode(), user.getAirlineCode(), agent.getAgentTypeCode(), agent.getCurrencyCode(), null,
					null, null, null);

			TextMessage textMessage = (TextMessage) message;
			ediMessage = textMessage.getText();

			// (1)
			// EDI --> XML
			String ediXMLInput = Convert.ediToXML(ediMessage);

			// XML --> Java
			ediOperation = IATAOperation.extractEdiOperationName(ediMessage);
			JAXBElement<?> object = Marshaller.ediToJava(ediXMLInput, ediOperation);

			// Get BEAN name
			requestHandler = IataOperationConfig.getRequestHandler(ediOperation, object);
			String commandName = requestHandler.getBeanFromElement();

			// (*)
			Command command = (Command) GDSServicesModuleUtil.getModule().getLocalBean(commandName);
			if (command != null) {

				iHeader = requestHandler.getInterchangeHeader();
				mHeader = requestHandler.getMessageHeader();

				Map<String, Object> paramMap = Transformer.getMessageParams(requestHandler, iHeader, mHeader);
				paramMap.put(TypeACommandParamNames.USER_PRINCIPLE, userPrincipal);

				if (paramMap != null) {

					Iterator<String> itParams = paramMap.keySet().iterator();
					while (itParams.hasNext()) {
						String paramName = itParams.next();
						command.setParameter(paramName, paramMap.get(paramName));
					}
				}

				// Execute the command, parse response
				responseParams = (DefaultServiceResponse) command.execute();
				// add all input params to responce as well. WE need them to construct the responce
				responseParams.addResponceParams(paramMap);

				IATAOperation ediRes = ediOperation.getResponse();
				objectResponse = Transformer.tranformResponse(responseParams, ediRes, requestHandler);

				// (2)
				// Java --> XML
				String ediXMLOutput = Marshaller.javaToEdi(objectResponse, ediRes);

				// XML --> EDI
				ediResult = Convert.xmlToEdi(ediXMLOutput);

			}

		} catch (InteractiveEDIException e) {
			log.error("InteractiveEDIException occour [" + e.getDescription() + "]", e);
			responseParams.addResponceParam(TypeACommandParamNames.EXCEPTION_CODE, e.getErrorCode());
			ediResult = Transformer.tranformErrorResponse(responseParams, iHeader, mHeader);
		} catch (ModuleException e) {
			log.error("ModuleException occour", e);
			responseParams.addResponceParam(TypeACommandParamNames.EXCEPTION_CODE, CodeSetEnum.CS0085.UNSPECIFIED_ERROR);
			ediResult = Transformer.tranformErrorResponse(responseParams, iHeader, mHeader);
		} catch (JMSException e) {
			log.error("JMSException occour", e);
			responseParams.addResponceParam(TypeACommandParamNames.EXCEPTION_CODE, CodeSetEnum.CS0085.UNSPECIFIED_ERROR);
			ediResult = Transformer.tranformErrorResponse(responseParams, iHeader, mHeader);
		} catch (Exception e) {
			log.error("Exception occour", e);
			responseParams.addResponceParam(TypeACommandParamNames.EXCEPTION_CODE, CodeSetEnum.CS0085.UNSPECIFIED_ERROR);
			ediResult = Transformer.tranformErrorResponse(responseParams, iHeader, mHeader);
		} finally {
			ForceLoginInvoker.close();
		}
	}
}