package com.isa.thinair.gdsservices.core.typeA.transformers.contrl.v02;

import iata.typea.v021.contrl.CONTRL;
import iata.typea.v021.contrl.GROUP1;
import iata.typea.v021.contrl.IATA;
import iata.typea.v021.contrl.ObjectFactory;
import iata.typea.v021.contrl.UCI;
import iata.typea.v021.contrl.UCM;
import iata.typea.v021.contrl.UNB;
import iata.typea.v021.contrl.UNH;
import iata.typea.v021.contrl.UNT;

import javax.xml.bind.JAXBElement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.core.typeA.model.InterchangeHeader;
import com.isa.thinair.gdsservices.core.typeA.model.MessageHeader;
import com.isa.thinair.gdsservices.core.typeA.transformers.IATARequest;
import com.isa.thinair.gdsservices.core.typeA.transformers.IATAResponse;
import com.isa.thinair.gdsservices.core.util.RandomANGen;

/**
 * Used to generate erronus response for exceptional cases
 * 
 * @author malaka
 * 
 */
public class Contrl extends IATAResponse {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(Contrl.class);

	public Contrl(IATARequest iataRequest) {
		super(iataRequest);
	}

	@Override
	public JAXBElement<?> transform(DefaultServiceResponse response) throws InteractiveEDIException {

		InterchangeHeader iHeader = (InterchangeHeader) response.getResponseParam(TypeACommandParamNames.IC_HEADER);
		MessageHeader mHeader = (MessageHeader) response.getResponseParam(TypeACommandParamNames.MESSAGE_HEADER);
		CodeSetEnum.CS0085 error = (CodeSetEnum.CS0085) response.getResponseParam(TypeACommandParamNames.EXCEPTION_CODE);

		// TODO what will happen if the interchange and message headers are null

		ContrlFactory contrlFactory = ContrlFactory.getInstance();
		ObjectFactory objectfactory = contrlFactory.getObjectfactory();

		IATA iata = objectfactory.createIATA();


		// contrl
		CONTRL contrl = objectfactory.createCONTRL();
		UNH unh = contrlFactory.createUNH(mHeader);
		contrl.setUNH(unh);

		UCI uci = contrlFactory.createUCI(iHeader, error);
		contrl.setUCI(uci);

		// If application error occour, UCM is also required
		if (error.getAction() == 7) {
			GROUP1 group1 = objectfactory.createGROUP1();
			UCM ucm = contrlFactory.createUCM(mHeader, error);
			group1.setUCM(ucm);
			contrl.getGROUP1().add(group1);
		}

		UNT unt = contrlFactory.createUNT(mHeader);
		contrl.setUNT(unt);

		// unb
		if (iHeader != null) {
			String interchangeControllRef = new RandomANGen(10).nextString("0001");
			UNB unb = contrlFactory.createUNB(iHeader, interchangeControllRef);
			iata.getUNBAndUNGAndCONTRL().add(unb);
		}

		iata.getUNBAndUNGAndCONTRL().add(contrl);

		return objectfactory.createIATA(iata);

	}
}
