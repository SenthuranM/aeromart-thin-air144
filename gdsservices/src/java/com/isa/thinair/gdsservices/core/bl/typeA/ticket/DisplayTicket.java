package com.isa.thinair.gdsservices.core.bl.typeA.ticket;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.datatype.DatatypeConfigurationException;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.dto.typea.DisplayTicketParams;
import com.isa.thinair.gdsservices.api.dto.typea.DisplayTicketTo;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageHandler;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.dto.temp.GdsReservation;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerTicketingInformationWrapper;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSServicesSupportJDBCDAO;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.GdsInfoDaoHelper;
import com.isa.thinair.gdsservices.core.util.TypeANotes;
import com.isa.thinair.gdsservices.core.util.TypeASpecificConstants;
import com.isa.typea.common.ErrorInformation;
import com.isa.typea.common.FlightInformation;
import com.isa.typea.common.Location;
import com.isa.typea.common.MessageFunction;
import com.isa.typea.common.OriginatorInformation;
import com.isa.typea.common.ReservationControlInformation;
import com.isa.typea.common.TicketCoupon;
import com.isa.typea.common.TicketNumberDetails;
import com.isa.typea.common.Traveller;
import com.isa.typea.common.TravellerTicketingInformation;
import com.isa.typea.tktreq.AATKTREQ;
import com.isa.typea.tktreq.TKTREQMessage;
import com.isa.typea.tktres.AATKTRES;
import com.isa.typea.tktres.TKTRESMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DisplayTicket extends EdiMessageHandler {

	private static final Log log = LogFactory.getLog(DisplayTicket.class);

	private AATKTREQ tktReq;
	private AATKTRES tktRes;

	protected void handleMessage() {

		tktReq = (AATKTREQ) messageDTO.getRequest();
		TKTREQMessage tktreqMessage = tktReq.getMessage();

		tktRes = new AATKTRES();
		TKTRESMessage tktresMessage = new TKTRESMessage();
		tktresMessage.setMessageHeader(GDSDTOUtil.createRespMessageHeader(tktreqMessage.getMessageHeader(),
				messageDTO.getRequestMetaDataDTO().getMessageReference(), messageDTO.getRequestMetaDataDTO().getIataOperation().getResponse()));
		tktresMessage.setOriginatorInformation(tktreqMessage.getOriginatorInformation());

		tktRes.setHeader(GDSDTOUtil.createRespEdiHeader(tktReq.getHeader(), tktresMessage.getMessageHeader(), messageDTO));
		tktRes.setMessage(tktresMessage);

		MessageFunction messageFunction = new MessageFunction();
		messageFunction.setMessageFunction(TypeAConstants.MessageFunction.DISPLAY);
		messageFunction.setResponseType(TypeAConstants.ResponseType.PROCESSED_SUCCESSFULLY);
		tktresMessage.setMessageFunction(messageFunction);


		try {

			if (!tktreqMessage.getTickets().isEmpty()) {
				exactMatch();
			} else {
				approximateMatches();
			}
		} catch (GdsTypeAException e) {
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(e.getEdiErrorCode());
			tktresMessage.setErrorInformation(errorInformation);

			log.error("Error Occurred ---- ", e);
		} catch (Exception e) {
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR);
			tktresMessage.setErrorInformation(errorInformation);

			log.error("Error Occurred ---- ", e);
		}

		messageDTO.setResponse(tktRes);

	}

	private void approximateMatches() throws ModuleException {

		AATKTREQ tktReq = (AATKTREQ) messageDTO.getRequest();
		TKTREQMessage tktreqMessage = tktReq.getMessage();
		TravellerTicketingInformation ticketingInfo;
		List<TravellerTicketingInformation> respTicketingInfo;
		Traveller traveller;

		DisplayTicketParams params = new DisplayTicketParams();

		List<TravellerTicketingInformation> travellers = tktreqMessage.getTravellerTicketingInformation();
		TravellerTicketingInformation searchTraveller;
		if (travellers.size() > 0) {
			searchTraveller = travellers.get(0);
			if (searchTraveller.getTravellers().size() > 0) {
				params.setPassengerFirstName(searchTraveller.getTravellers().get(0).getGivenName());
			}
			params.setPassengerSurname(searchTraveller.getTravellerSurname());
		}

		FlightInformation flightInformation = tktreqMessage.getFlightInformation();
	    if (flightInformation != null) {
		    params.setFlightDepartureDate(flightInformation.getDepartureDateTime().toGregorianCalendar().getTime());
		    if (flightInformation.getDepartureLocation() != null) {
		        params.setOrigin(flightInformation.getDepartureLocation().getLocationCode());
		    }
		    if (flightInformation.getDepartureLocation() != null) {
			    params.setDestination(flightInformation.getDepartureLocation().getLocationCode());
		    }
		    params.setFlightNumber(AppSysParamsUtil.getCarrierCode() + flightInformation.getFlightNumber());
	    }

		Set<String> pnrs = new HashSet<String>();
		List<String> pnrList;
		List<TravellerTicketingInformation> travellerTicketingInfo = tktRes.getMessage().getTravellerTicketingInformation();

		List<DisplayTicketTo> matchedTickets = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO.searchTickets(params);

		for (DisplayTicketTo paxDTO : matchedTickets) {
			pnrs.add(paxDTO.getPnr());
		}

		pnrList = new ArrayList<String>(pnrs);
		Map<String, Map<String, List<Object>>> gdsInfo = GdsInfoDaoHelper.getGdsReservationDtoMap(pnrList);

		respTicketingInfo = toTravellerTicketingInformation(matchedTickets, gdsInfo);
		travellerTicketingInfo.addAll(respTicketingInfo);


	}

	private void exactMatch() throws GdsTypeAException {
		TravellerTicketingInformation displayTicket;

		TKTRESMessage tktresMessage = tktRes.getMessage();
		List<TravellerTicketingInformation> displayTickets = tktresMessage.getTravellerTicketingInformation();

		GdsReservation<TravellerTicketingInformationWrapper> gdsReservation;
		GDSServicesSupportJDBCDAO gdsServicesSupportDao = GDSServicesDAOUtils.DAOInstance.GDSSERVICES_SUPPORT_JDBC_DAO;

		String ticketNumber = tktReq.getMessage().getTickets().get(0).getTicketNumber();
		String pnr;

		try {
			pnr = gdsServicesSupportDao.getPnrFromExternalTicketNumber(ticketNumber);
		} catch (Exception e) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.TICKET_NUMBER_NOT_FOUND,
					TypeANotes.ErrorMessages.TICKET_NUMBER_DOES_NOT_EXIST);
		}

		gdsReservation = GdsInfoDaoHelper.getGdsReservationTicketView(pnr);

		TravellerTicketingInformationWrapper travellerWrapper = resolvePassenger(gdsReservation, ticketNumber);
		TravellerTicketingInformation travellerImage = travellerWrapper.getTravellerInformation();

		tktresMessage.setOriginatorInformation(travellerWrapper.getOriginatorInformation());
		// TODO - EQN

		displayTicket = new TravellerTicketingInformation();
		displayTicket.setTravellerSurname(travellerImage.getTravellerSurname());
		displayTicket.setTravellerCount(travellerImage.getTravellerCount());
		displayTicket.setTravellerQualifier(travellerImage.getTravellerQualifier());
		displayTicket.getTravellers().addAll(travellerImage.getTravellers());

		displayTicket.setTicketingAgentInformation(travellerWrapper.getTicketingAgentInformation());
		displayTicket.getReservationControlInformation().addAll(gdsReservation.getReservationControlInformation());
		displayTicket.getMonetaryInformation().addAll(travellerImage.getMonetaryInformation());
		displayTicket.getFormOfPayment().addAll(travellerImage.getFormOfPayment());
		displayTicket.setPricingTicketingDetails(travellerImage.getPricingTicketingDetails());
		displayTicket.setOriginatorInformation(travellerImage.getOriginatorInformation());
		displayTicket.setAdditionalTourInformation(travellerImage.getAdditionalTourInformation());
		// TODO -- ORG
		displayTicket.getTaxDetails().addAll(travellerImage.getTaxDetails());
		displayTicket.getText().addAll(travellerImage.getText());

		for (TicketNumberDetails ticket : travellerImage.getTickets()) {
			ticket.setDataIndicator(null);
			for (TicketCoupon coupon : ticket.getTicketCoupon()) {
				coupon.setRelatedProductInfo(null);
			}
			displayTicket.getTickets().add(ticket);
		}

		displayTickets.add(displayTicket);
	}


	private TravellerTicketingInformationWrapper resolvePassenger(GdsReservation<TravellerTicketingInformationWrapper> gdsReservation,
	                                                              String ticketNumber) {
		TravellerTicketingInformationWrapper travellerWrapper = null;

		l0:
		for (TravellerTicketingInformationWrapper traveller : gdsReservation.getTravellerInformation()) {
			for (TicketNumberDetails ticket : traveller.getTravellerInformation().getTickets()) {
				if(ticketNumber.equals(ticket.getTicketNumber())) {
					travellerWrapper = traveller;
					break l0;
				}
			}
		}

		return travellerWrapper;
	}


	private List<TravellerTicketingInformation> toTravellerTicketingInformation(List<DisplayTicketTo> displayTicketTos, Map<String, Map<String, List<Object>>> gdsInfo)
			throws ModuleException {
		Map<String, List<Object>> gdsResInfo;

		List<TravellerTicketingInformation> travellers = new ArrayList<TravellerTicketingInformation>();
		TravellerTicketingInformation travellerInfo;
		Traveller traveller;
		TicketNumberDetails ticketNumberDetails;
		FlightInformation flightInfo;
		TicketCoupon ticketCoupon;
		OriginatorInformation originatorInformation;
		List<ReservationControlInformation> resControlInfoList;
		ReservationControlInformation resControlInfo;
		Location location;
		FlightInformation.CompanyInfo companyInfo;
		FlightInformation.FlightPreference flightPreference;

		String carrierCode = AppSysParamsUtil.getCarrierCode();

		try {

			for (DisplayTicketTo displayTicketTo : displayTicketTos) {

				travellerInfo = new TravellerTicketingInformation();

				traveller = new Traveller();
				travellerInfo.setTravellerSurname(displayTicketTo.getPassengerSurname());
				traveller.setGivenName(displayTicketTo.getPassengerName());
				travellerInfo.getTravellers().add(traveller);

				originatorInformation = null;
				if (gdsInfo.containsKey(displayTicketTo.getPnr())) {

					gdsResInfo = gdsInfo.get(displayTicketTo.getPnr());
					if (gdsResInfo.containsKey(TypeASpecificConstants.TypeASegment.ORIGINATOR_DETAILS.getCode())) {
						originatorInformation =
								(OriginatorInformation) gdsResInfo.get(TypeASpecificConstants.TypeASegment.ORIGINATOR_DETAILS.getCode());
					}
				}

				resControlInfoList = new ArrayList<ReservationControlInformation>();
				resControlInfo = new ReservationControlInformation();
				resControlInfo.setReservationControlNumber(displayTicketTo.getPnr());
				resControlInfo.setAirlineCode(carrierCode);
				resControlInfoList.add(resControlInfo);

				resControlInfo = new ReservationControlInformation();
				resControlInfo.setReservationControlNumber(displayTicketTo.getExternalRecLocator());
				resControlInfo.setAirlineCode(displayTicketTo.getGdsCarrierCode());
				resControlInfoList.add(resControlInfo);

				travellerInfo.getReservationControlInformation().addAll(resControlInfoList);

				travellerInfo.setOriginatorInformation(originatorInformation);


				ticketNumberDetails = new TicketNumberDetails();
				ticketNumberDetails.setTicketNumber(String.valueOf(displayTicketTo.getTicketNumber()));
				ticketNumberDetails.setDocumentType(TypeAConstants.DocumentType.TICKET);

				ticketCoupon = new TicketCoupon();
				ticketCoupon.setCouponNumber(displayTicketTo.getTicketNumber());
				ticketCoupon.setStatus(displayTicketTo.getCouponStatus());


				flightInfo = new FlightInformation();
				flightInfo.setDepartureDateTime(CalendarUtil.asXMLGregorianCalendar(displayTicketTo.getFlightDepartureDate()));

				location = new Location();
				location.setLocationCode(displayTicketTo.getOrigin());
				flightInfo.setDepartureLocation(location);

				location = new Location();
				location.setLocationCode(displayTicketTo.getDestination());
				flightInfo.setArrivalLocation(location);

				companyInfo = new FlightInformation.CompanyInfo();
				companyInfo.setMarketingAirlineCode(carrierCode);
				flightInfo.setCompanyInfo(companyInfo);

				flightInfo.setFlightNumber(displayTicketTo.getFlightNumber().substring(carrierCode.length()));

				flightPreference = new FlightInformation.FlightPreference();
				flightPreference.setBookingClass(displayTicketTo.getBookingDesignator());
				flightInfo.setFlightPreference(flightPreference);

				ticketCoupon.setFlightInfomation(flightInfo);

				ticketNumberDetails.getTicketCoupon().add(ticketCoupon);

				travellerInfo.getTickets().add(ticketNumberDetails);

				travellers.add(travellerInfo);

			}

		} catch (DatatypeConfigurationException e) {
			throw new ModuleException(e, "Error occurred while Pax Details transformation");
		}

		return travellers;
	}
}
