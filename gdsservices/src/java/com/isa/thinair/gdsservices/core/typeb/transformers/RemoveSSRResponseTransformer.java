package com.isa.thinair.gdsservices.core.typeb.transformers;

import java.util.List;
import java.util.Map;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRChildDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRInfantDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.RemoveSsrRequest;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ActionCode;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.AdviceCode;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ProcessStatus;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.StatusCode;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;

public class RemoveSSRResponseTransformer {
	private static final ReservationAction RESERVATION_ACTION = ReservationAction.REMOVE_SSR;

	/**
	 * Transforms reservationResponseMap back to bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param reservationResponseMap
	 * @return
	 */
	public static BookingRequestDTO getResponse(BookingRequestDTO bookingRequestDTO,
			Map<ReservationAction, GDSReservationRequestBase> reservationResponseMap) {

		RemoveSsrRequest removeSsrRequest = (RemoveSsrRequest) reservationResponseMap.get(RESERVATION_ACTION);

		if (removeSsrRequest.isSuccess()) {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_SUCCESS);
			bookingRequestDTO = ResponseTransformerUtil.setCancelledSSRStatus(bookingRequestDTO);
			bookingRequestDTO = updateSSRElementStatus(bookingRequestDTO);
			bookingRequestDTO = ValidatorUtils.addResponse(bookingRequestDTO, removeSsrRequest);

		} else {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_FAILURE);
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, removeSsrRequest);
		}
		bookingRequestDTO.getErrors().addAll(removeSsrRequest.getErrorCode());

		return bookingRequestDTO;
	}
	
	private static BookingRequestDTO updateSSRElementStatus(BookingRequestDTO bookingRequestDTO) {
		List<SSRDTO> ssrDTOS = bookingRequestDTO.getSsrDTOs();
		for (SSRDTO ssrDto : ssrDTOS) {
			if (ssrDto instanceof SSRInfantDTO) {
				SSRInfantDTO ssrInfant = (SSRInfantDTO) ssrDto;
				String actionCode = ssrInfant.getActionOrStatusCode();
				if (actionCode.equals(ActionCode.CANCEL.getCode())) {
					ssrInfant.setAdviceOrStatusCode(AdviceCode.CANCELLED.getCode());
				} else if (actionCode.equals(ActionCode.NEED.getCode())) {
					ssrInfant.setAdviceOrStatusCode(AdviceCode.CONFIRMED.getCode());
				} else if (actionCode.equals(StatusCode.HOLDS_CONFIRMED.getCode())) {
					ssrInfant.setAdviceOrStatusCode(StatusCode.HOLDS_CONFIRMED.getCode());
				}
			}
			if (ssrDto instanceof SSRChildDTO) {
				SSRChildDTO ssrChild = (SSRChildDTO) ssrDto;
				String actionCode = ssrChild.getActionOrStatusCode();
				if (actionCode.equals(ActionCode.CANCEL.getCode())) {
					ssrChild.setAdviceOrStatusCode(AdviceCode.CANCELLED.getCode());
				} else if (actionCode.equals(ActionCode.NEED.getCode())) {
					ssrChild.setAdviceOrStatusCode(AdviceCode.CONFIRMED.getCode());
				} else if (actionCode.equals(StatusCode.HOLDS_CONFIRMED.getCode())) {
					ssrChild.setAdviceOrStatusCode(StatusCode.HOLDS_CONFIRMED.getCode());
				} else if (actionCode.equals(StatusCode.CODESHARE_CONFIRMED.getCode())) {
					ssrChild.setAdviceOrStatusCode(StatusCode.CODESHARE_CONFIRMED.getCode());
				}
			}

		}

		return bookingRequestDTO;
	}
}
