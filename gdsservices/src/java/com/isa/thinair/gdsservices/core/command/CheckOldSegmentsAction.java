package com.isa.thinair.gdsservices.core.command;

import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.internal.CheckOldSegmentsRequest;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

public class CheckOldSegmentsAction {

	private static Log log = LogFactory.getLog(CheckOldSegmentsAction.class);

	public static CheckOldSegmentsRequest processRequest(CheckOldSegmentsRequest checkOldSegmentsRequest) {
		try {
			Reservation reservation = CommonUtil.loadReservation(checkOldSegmentsRequest.getGdsCredentialsDTO(),
					checkOldSegmentsRequest.getOriginatorRecordLocator().getPnrReference(), null);
			Collection<Segment> segments = checkOldSegmentsRequest.getSegments();

			for (Iterator iterator = segments.iterator(); iterator.hasNext();) {
				Segment segment = (Segment) iterator.next();
				checkOldSegments(reservation.getSegments(), segment);
			}

			checkOldSegmentsRequest.setSuccess(true);
			if (checkOldSegmentsRequest.isNotSupportedSSRExists()) {
				String message = MessageUtil.getMessage(GDSInternalCodes.ResponseMessageCode.SSR_NOT_SUPPORTED.getCode());
				checkOldSegmentsRequest.setResponseMessage(message.toUpperCase());
			}
		} catch (ModuleException e) {
			log.error(" CheckOldSegmentsAction Failed for GDS PNR "
					+ checkOldSegmentsRequest.getOriginatorRecordLocator().getPnrReference(), e);
			checkOldSegmentsRequest.setSuccess(false);
			checkOldSegmentsRequest.setResponseMessage(e.getMessageString());
		} catch (Exception e) {
			log.error(" CheckOldSegmentsAction Failed for GDS PNR "
					+ checkOldSegmentsRequest.getOriginatorRecordLocator().getPnrReference(), e);
			checkOldSegmentsRequest.setSuccess(false);
			checkOldSegmentsRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
		}

		return checkOldSegmentsRequest;
	}

	/**
	 * checks segment for schedule time
	 * 
	 * @param resSegments
	 * @param segment
	 * @return
	 */
	private static void checkOldSegments(Collection<ReservationSegment> resSegments, Segment segment) {
		// FIXME: phase 2
		segment.setStatusCode(GDSInternalCodes.StatusCode.EXISTS);
	}

}
