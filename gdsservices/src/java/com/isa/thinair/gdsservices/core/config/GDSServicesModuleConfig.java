package com.isa.thinair.gdsservices.core.config;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * Holds GDS Services related module configurations
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @isa.module.config-bean
 */
public class GDSServicesModuleConfig extends DefaultModuleConfig {

	private String defaultPaymentBroker;

	private Properties msgBrokerParams;

	private String ejbAccessUsername;

	private String ejbAccessPassword;

	private String msgBrokerLoginConfigFile;

	private String msgBrokerLoginConfigName;

	private TypeAMessageConfig typeAMessageConfig;
	
	private boolean showPaymentDetailsInTTYMessage;

	private String matipClientHost;
	private int matipClientPort;

	private TypeAConfig typeAConfig;

	private Map<GDSExternalCodes.GDS, Set<String>> surchargeCodes;
	
	/**
	 * BCs mapped GDS wise to skip publish when send AVS
	 * */
	private Map<Integer,List<String>> avsSkipPublishBookingCodes;
	
	/**
	 * BCs mapped GDS wise to skip process when AVS receive
	 * */
	private Map<Integer,List<String>> avsSkipProcessBookingCodes;

	public Properties getMsgBrokerParams() {
		return msgBrokerParams;
	}

	public void setMsgBrokerParams(Properties msgBrokerParams) {
		this.msgBrokerParams = msgBrokerParams;
	}

	public String getEjbAccessUsername() {
		return ejbAccessUsername;
	}

	public void setEjbAccessUsername(String ejbAccessUsername) {
		this.ejbAccessUsername = ejbAccessUsername;
	}

	public String getEjbAccessPassword() {
		return ejbAccessPassword;
	}

	public void setEjbAccessPassword(String ejbAccessPassword) {
		this.ejbAccessPassword = ejbAccessPassword;
	}

	public String getMsgBrokerLoginConfigFile() {
		return msgBrokerLoginConfigFile;
	}

	public void setMsgBrokerLoginConfigFile(String msgBrokerLoginConfigFile) {
		this.msgBrokerLoginConfigFile = msgBrokerLoginConfigFile;
	}

	public String getMsgBrokerLoginConfigName() {
		return msgBrokerLoginConfigName;
	}

	public void setMsgBrokerLoginConfigName(String msgBrokerLoginConfigName) {
		this.msgBrokerLoginConfigName = msgBrokerLoginConfigName;
	}

	/**
	 * @return the defaultPaymentBroker
	 */
	public String getDefaultPaymentBroker() {
		return defaultPaymentBroker;
	}

	/**
	 * @param defaultPaymentBroker
	 *            the defaultPaymentBroker to set
	 */
	public void setDefaultPaymentBroker(String defaultPaymentBroker) {
		this.defaultPaymentBroker = defaultPaymentBroker;
	}

	/**
	 * @return the typeAMessageConfig
	 */
	public TypeAMessageConfig getTypeAMessageConfig() {
		return typeAMessageConfig;
	}

	/**
	 * @param typeAMessageConfig
	 *            the typeAMessageConfig to set
	 */
	public void setTypeAMessageConfig(TypeAMessageConfig typeAMessageConfig) {
		this.typeAMessageConfig = typeAMessageConfig;
	}

	/**
	 * @return the showPaymentDetailsInTTYMessage
	 */
	public boolean isShowPaymentDetailsInTTYMessage() {
		return showPaymentDetailsInTTYMessage;
	}

	/**
	 * @param showPaymentDetailsInTTYMessage the showPaymentDetailsInTTYMessage to set
	 */
	public void setShowPaymentDetailsInTTYMessage(boolean showPaymentDetailsInTTYMessage) {
		this.showPaymentDetailsInTTYMessage = showPaymentDetailsInTTYMessage;
	}

	public TypeAConfig getTypeAConfig() {
		return typeAConfig;
	}

	public void setTypeAConfig(TypeAConfig typeAConfig) {
		this.typeAConfig = typeAConfig;
	}

	public Map<Integer, List<String>> getAvsSkipPublishBookingCodes() {
		return avsSkipPublishBookingCodes;
	}

	public void setAvsSkipPublishBookingCodes(Map<Integer, List<String>> avsSkipPublishBookingCodes) {
		this.avsSkipPublishBookingCodes = avsSkipPublishBookingCodes;
	}

	public Map<Integer, List<String>> getAvsSkipProcessBookingCodes() {
		return avsSkipProcessBookingCodes;
	}

	public void setAvsSkipProcessBookingCodes(Map<Integer, List<String>> avsSkipProcessBookingCodes) {
		this.avsSkipProcessBookingCodes = avsSkipProcessBookingCodes;
	}

	public String getMatipClientHost() {
		return matipClientHost;
	}

	public void setMatipClientHost(String matipClientHost) {
		this.matipClientHost = matipClientHost;
	}

	public int getMatipClientPort() {
		return matipClientPort;
	}

	public void setMatipClientPort(int matipClientPort) {
		this.matipClientPort = matipClientPort;
	}

	public Map<GDSExternalCodes.GDS, Set<String>> getSurchargeCodes() {
		return surchargeCodes;
	}

	public void setSurchargeCodes(Map<GDSExternalCodes.GDS, Set<String>> surchargeCodes) {
		this.surchargeCodes = surchargeCodes;
	}
}
