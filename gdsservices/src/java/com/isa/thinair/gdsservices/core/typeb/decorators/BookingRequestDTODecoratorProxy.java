package com.isa.thinair.gdsservices.core.typeb.decorators;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRCreditCardDetailDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSROTHERSDTO;
import com.isa.thinair.msgbroker.api.service.MsgbrokerUtils;

public class BookingRequestDTODecoratorProxy {

	public static BookingRequestDTO decorate(BookingRequestDTO bookingRequestDTO) {
		bookingRequestDTO = externalSegmentsRemover(bookingRequestDTO);
		bookingRequestDTO = externalSSRSegmentsRemover(bookingRequestDTO);
		bookingRequestDTO = creditCardSSRRemover(bookingRequestDTO);

		// finally
		bookingRequestDTO = invalidCharactorRemover(bookingRequestDTO);
		return bookingRequestDTO;
	}

	private static BookingRequestDTO creditCardSSRRemover(BookingRequestDTO bookingRequestDTO) {
		List<SSRDTO> lstSSRDTOs = bookingRequestDTO.getSsrDTOs();

		if (lstSSRDTOs != null && lstSSRDTOs.size() > 0) {
			List<SSRDTO> removeAbleSSRs = new ArrayList<SSRDTO>();

			for (SSRDTO ssrDTO : lstSSRDTOs) {
				if (ssrDTO instanceof SSRCreditCardDetailDTO) {
					removeAbleSSRs.add(ssrDTO);
				}
			}

			if (removeAbleSSRs.size() > 0) {
				lstSSRDTOs.removeAll(removeAbleSSRs);
			}
		}

		return bookingRequestDTO;
	}

	private static BookingRequestDTO externalSSRSegmentsRemover(BookingRequestDTO bookingRequestDTO) {
		List<SSRDTO> lstSSRDTOs = bookingRequestDTO.getSsrDTOs();

		if (lstSSRDTOs != null && lstSSRDTOs.size() > 0) {
			List<SSRDTO> removeAbleSSRs = new ArrayList<SSRDTO>();
			String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

			for (SSRDTO ssrDTO : lstSSRDTOs) {
				if (!carrierCode.equals(ssrDTO.getCarrierCode())) {
					removeAbleSSRs.add(ssrDTO);
				}
			}

			if (removeAbleSSRs.size() > 0) {
				lstSSRDTOs.removeAll(removeAbleSSRs);
			}
		}

		return bookingRequestDTO;
	}

	private static BookingRequestDTO externalSegmentsRemover(BookingRequestDTO bookingRequestDTO) {
		List<BookingSegmentDTO> lstBookingSegmentDTO = bookingRequestDTO.getBookingSegmentDTOs();

		if (lstBookingSegmentDTO != null && lstBookingSegmentDTO.size() > 0) {
			List<BookingSegmentDTO> removeAbleSegments = new ArrayList<BookingSegmentDTO>();

			for (BookingSegmentDTO bookingSegmentDTO : lstBookingSegmentDTO) {
				Map<String, GDSStatusTO> gdsStatusMap = MsgbrokerUtils.getGlobalConfig().getActiveGdsMap();
				Set<String> csCarriers = new HashSet<String>();
				for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
					if (gdsStatusTO.isCodeShareCarrier()) {
						csCarriers.add(gdsStatusTO.getCarrierCode());
					}
				}
				csCarriers.add(AppSysParamsUtil.getDefaultCarrierCode());
				if (!csCarriers.contains(bookingSegmentDTO.getCarrierCode())) {
					removeAbleSegments.add(bookingSegmentDTO);
				}
			}

			if (removeAbleSegments.size() > 0) {
				lstBookingSegmentDTO.removeAll(removeAbleSegments);
			}
		}

		return bookingRequestDTO;
	}

	/**
	 * removes invalid SSR characters from the BookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @return
	 */
	private static BookingRequestDTO invalidCharactorRemover(BookingRequestDTO bookingRequestDTO) {
		List<SSRDTO> lstSSRDTOs = bookingRequestDTO.getSsrDTOs();

		if (lstSSRDTOs != null && lstSSRDTOs.size() > 0) {
			for (SSRDTO ssrDTO : lstSSRDTOs) {
				if (ssrDTO instanceof SSROTHERSDTO) {
					ssrDTO.setSsrValue(StringUtil.replaceInvalidSSRChars(ssrDTO.getSsrValue()));
				}
			}
		}

		return bookingRequestDTO;
	}
}
