package com.isa.thinair.gdsservices.core.bl.typeA.common;

import static com.isa.thinair.gdsservices.api.util.TypeAConstants.AssociationCode.DUPLICATE;
import static com.isa.thinair.gdsservices.api.util.TypeAConstants.AssociationCode.ESTABLISH;
import static com.isa.thinair.gdsservices.api.util.TypeAConstants.AssociationCode.ESTABLISHED;
import static com.isa.thinair.gdsservices.api.util.TypeAConstants.AssociationCode.INACTIVE;
import static com.isa.thinair.gdsservices.api.util.TypeAConstants.AssociationCode.ONE_OFF;
import static com.isa.thinair.gdsservices.api.util.TypeAConstants.AssociationCode.RETRY;
import static com.isa.thinair.gdsservices.api.util.TypeAConstants.AssociationCode.TERMINATE;

import java.util.Map;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.dto.EDIMessageDTO;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.typea.common.BaseEDIRequest;
import com.isa.typea.common.Header;

/**
 * @isa.module.command name="ediProtocolProcessor"
 */
public class EdiProtocolProcessor extends DefaultBaseCommand {

	public ServiceResponce execute() throws ModuleException {

		String respState = null;
		GDSExternalCodes.ProcessStatus processStatus = GDSExternalCodes.ProcessStatus.VALIDATE_SUCCESS;

		EDIMessageDTO messageDTO = (EDIMessageDTO) getParameter(TypeACommandParamNames.EDI_REQUEST_DTO);
		Map<String, Object> msgSession = messageDTO.getMessagingSession();

		String currentState = null;
		if (msgSession.containsKey(TypeAConstants.MessageSessionKey.MSG_CONTEXT_STATE)) {
			currentState = (String) msgSession.get(TypeAConstants.MessageSessionKey.MSG_CONTEXT_STATE);
		}

		BaseEDIRequest ediRequest = messageDTO.getRequest();
		Header header = ediRequest.getHeader();

		String requestedState = header.getInterchangeHeader().getAssociationCode();

		if (requestedState != null && !requestedState.isEmpty()) {
			if (currentState == null) {
				if (requestedState.equals(ESTABLISH) || requestedState.equals(ESTABLISHED)) {
					respState = ESTABLISHED;
					msgSession.put(TypeAConstants.MessageSessionKey.MSG_CONTEXT_STATE, ESTABLISHED);
				} else  if (requestedState.equals(ONE_OFF) || requestedState.equals(TERMINATE)) {
					respState = TERMINATE;
					msgSession.put(TypeAConstants.MessageSessionKey.MSG_CONTEXT_STATE, TERMINATE);
				} else {
					processStatus = GDSExternalCodes.ProcessStatus.VALIDATE_FAILURE;
					respState = INACTIVE;
				}
			} else if (currentState.equals(ESTABLISH)) {
				if (requestedState.equals(ESTABLISHED)) {
					respState = ESTABLISHED;
					msgSession.put(TypeAConstants.MessageSessionKey.MSG_CONTEXT_STATE, ESTABLISHED);
				} else if (requestedState.equals(ESTABLISH)) {
					processStatus = GDSExternalCodes.ProcessStatus.VALIDATE_FAILURE;
					respState = DUPLICATE;
				} else {
					processStatus = GDSExternalCodes.ProcessStatus.VALIDATE_FAILURE;
					respState = INACTIVE;
				}
			} else if (currentState.equals(ESTABLISHED)) {
				if (requestedState.equals(ESTABLISH) || requestedState.equals(RETRY)) {
					processStatus = GDSExternalCodes.ProcessStatus.VALIDATE_FAILURE;
					respState = DUPLICATE;
				} else if (requestedState.equals(TERMINATE)) {
					respState = TERMINATE;
					msgSession.put(TypeAConstants.MessageSessionKey.MSG_CONTEXT_STATE, TERMINATE);
				} else if (requestedState.equals(ESTABLISHED)) {
					respState = ESTABLISHED;
				}

			} else if (currentState.equals(TERMINATE)) {
				processStatus = GDSExternalCodes.ProcessStatus.VALIDATE_FAILURE;
				respState = INACTIVE;
			}
		}

		messageDTO.setRespAssociationCode(respState);

		DefaultServiceResponse resp = new DefaultServiceResponse();
		resp.addResponceParam(TypeACommandParamNames.EDI_PROCESS_STATUS, processStatus);
		return resp;
	}
}
