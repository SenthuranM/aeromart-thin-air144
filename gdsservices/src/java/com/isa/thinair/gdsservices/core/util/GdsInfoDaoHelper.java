package com.isa.thinair.gdsservices.core.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONPopulator;
import org.apache.struts2.json.JSONUtil;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.dto.temp.GdsReservation;
import com.isa.thinair.gdsservices.api.model.GDSReservationInfo;
import com.isa.thinair.gdsservices.api.model.GDSReservationPaxInfo;
import com.isa.thinair.gdsservices.api.model.GdsReservationImage;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.dto.temp.GdsReservation;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerTicketControlInformationWrapper;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerTicketingInformationWrapper;
import com.isa.typea.common.TravellerTicketingInformation;


public abstract class GdsInfoDaoHelper {

	private static Log log = LogFactory.getLog(GdsInfoDaoHelper.class);

	public static void saveSegmentOfReservation(Object segment, TypeASpecificConstants.TypeASegment segmentType, String pnr) throws ModuleException {
		String json = null;

		try {
			json = JSONUtil.serialize(segment);
		} catch (JSONException e) {
			throw new ModuleException("Error occurred while json serialization", e);
		}

		GDSReservationInfo reservationInfo = new GDSReservationInfo();
		reservationInfo.setPnr(pnr);
		reservationInfo.setSegmentType(segmentType.getCode());
		reservationInfo.setSegmentContent(json);
		GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO.gdsTypeAServiceSaveOrUpdate(reservationInfo);
	}

	public static void saveSegmentOfReservationPax(Object segment, TypeASpecificConstants.TypeASegment segmentType, Integer pnrPax) throws  ModuleException {
		String json = null;

		try {
			json = JSONUtil.serialize(segment);
		} catch (JSONException e) {
			throw new ModuleException("Error occurred while json serialization", e);
		}

		GDSReservationPaxInfo reservationPaxInfo = new GDSReservationPaxInfo();
		reservationPaxInfo.setPnrPaxId(pnrPax);
		reservationPaxInfo.setSegmentType(segmentType.getCode());
		reservationPaxInfo.setSegmentContent(json);
		GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO.gdsTypeAServiceSaveOrUpdate(reservationPaxInfo);
	}

	public static Map<String, List<Object>> getGdsReservationDtoMap(String pnr) throws ModuleException {
		Map<String, List<Object>> resDtoMap = new HashMap<String, List<Object>>();

		List<GDSReservationInfo> resInfoCol = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO
				.getGDSReservationInfo(pnr);
		List<Object> dtoList;

		try {
			for(GDSReservationInfo resInfo : resInfoCol) {
				if (!resDtoMap.containsKey(resInfo.getSegmentType())) {
					resDtoMap.put(resInfo.getSegmentType(), new ArrayList<Object>());
				}
				dtoList = resDtoMap.get(resInfo.getSegmentType());
				dtoList.add(JSONUtil.deserialize(resInfo.getSegmentContent()));
			}
		} catch (JSONException e) {
			throw new ModuleException("Error occurred while json de-serialization", e);
		}

		return resDtoMap;
	}

	public static Map<String, Map<String, List<Object>>> getGdsReservationDtoMap(List<String> pnrs) throws ModuleException {
		Map<String, Map<String, List<Object>>> resDtoMap = new HashMap<String, Map<String, List<Object>>>();
		JSONPopulator populator = new JSONPopulator();

		List<GDSReservationInfo> resInfoCol = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO
				.getGDSReservationInfo(pnrs);
		List<Object> dtoList;
		Map parsedJson;
		Class segType;
		Object convertedObject;

		try {
			for(GDSReservationInfo resInfo : resInfoCol) {
				if (!resDtoMap.containsKey(resInfo.getPnr())) {
					resDtoMap.put(resInfo.getPnr(), new HashMap<String, List<Object>>());
				}

				if (!resDtoMap.get(resInfo.getPnr()).containsKey(resInfo.getSegmentType())) {
					resDtoMap.get(resInfo.getPnr()).put(resInfo.getSegmentType(), new ArrayList<Object>());
				}
				dtoList = resDtoMap.get(resInfo.getPnr()).get(resInfo.getSegmentType());
				parsedJson = (Map)JSONUtil.deserialize(resInfo.getSegmentContent());
                segType = TypeASpecificConstants.TypeASegment.getTypeASegment(resInfo.getSegmentType()).getClazz();
				convertedObject = segType.newInstance();

				populator.populateObject(convertedObject, parsedJson);

				dtoList.add(convertedObject);
			}
		} catch (Exception e) {
			throw new ModuleException("Error occurred while json de-serialization", e);
		}

		return resDtoMap;
	}

	public static Map<String, List<Object>> getGdsReservationPaxDtoMap(long pnrPaxId) throws ModuleException {
		Map<String, List<Object>> resPaxDtoMap = new HashMap<String, List<Object>>();

		List<GDSReservationPaxInfo> resPaxInfoCol = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO
				.getGDSReservationPaxInfo(pnrPaxId);
		List<Object> dtoList;

		try {
			for(GDSReservationPaxInfo resPaxInfo : resPaxInfoCol) {
				if (!resPaxDtoMap.containsKey(resPaxInfo.getSegmentType())) {
					resPaxDtoMap.put(resPaxInfo.getSegmentType(), new ArrayList<Object>());
				}
				dtoList = resPaxDtoMap.get(resPaxInfo.getSegmentType());
				dtoList.add(JSONUtil.deserialize(resPaxInfo.getSegmentContent()));
			}
		} catch (JSONException e) {
			throw new ModuleException("Error occurred while json de-serialization", e);
		}

		return resPaxDtoMap;
	}

	public static Map<Integer, Map<String, List<Object>>> getGdsReservationPaxDtoMap(List<Long> pnrPaxIds) throws ModuleException {
		Map<Integer, Map<String, List<Object>>> resPaxDtoMap = new HashMap<Integer, Map<String, List<Object>>>();

		List<GDSReservationPaxInfo> resPaxInfoCol = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO
				.getGDSReservationPaxInfo(pnrPaxIds);
		List<Object> dtoList;

		try {
			for(GDSReservationPaxInfo resPaxInfo : resPaxInfoCol) {
				if (!resPaxDtoMap.containsKey(resPaxInfo.getPnrPaxId())) {
					resPaxDtoMap.put(resPaxInfo.getPnrPaxId(), new HashMap<String, List<Object>>());
				}

				if (!resPaxDtoMap.get(resPaxInfo.getPnrPaxId()).containsKey(resPaxInfo.getSegmentType())) {
					resPaxDtoMap.get(resPaxInfo.getPnrPaxId()).put(resPaxInfo.getSegmentType(), new ArrayList<Object>());
				}

				dtoList = resPaxDtoMap.get(resPaxInfo.getPnrPaxId()).get(resPaxInfo.getSegmentType());
				dtoList.add(JSONUtil.deserialize(resPaxInfo.getSegmentContent()));
			}
		} catch (JSONException e) {
			throw new ModuleException("Error occurred while json de-serialization", e);
		}

		return resPaxDtoMap;
	}

	public static GdsReservation<TravellerTicketingInformationWrapper> getGdsReservationTicketView(String pnr) {

		GdsReservation<TravellerTicketingInformationWrapper> gdsReservation;
		GdsReservationImage reservationImage = GDSServicesDAOUtils.DAOInstance.GDSSERVICES_DAO.getGDSReservationImage(pnr, GdsReservationImage.ImageContext.TICKET);

		if (reservationImage != null) {
			gdsReservation = (GdsReservation<TravellerTicketingInformationWrapper>) GDSApiUtils.toObject(reservationImage.getReservationImage());
		} else {
			gdsReservation = new GdsReservation<TravellerTicketingInformationWrapper>();
		}

		return gdsReservation;
	}

	public static void saveGdsReservationTicketView(String pnr, GdsReservation<TravellerTicketingInformationWrapper> gdsReservation) {
		GdsReservationImage reservationImage = GDSServicesDAOUtils.DAOInstance.GDSSERVICES_DAO.getGDSReservationImage(pnr, GdsReservationImage.ImageContext.TICKET);

		log.error("SaveGdsReservationTicketView ---- PNR: " + pnr + " ---- ReservationImage==NULL :"+ (reservationImage == null));

		if (reservationImage == null) {
			reservationImage = new GdsReservationImage();
			reservationImage.setPnr(pnr);
			reservationImage.setImageContext(GdsReservationImage.ImageContext.TICKET);
		}

		reservationImage.setReservationImage(GDSApiUtils.toBlob(gdsReservation));
		GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO.gdsTypeAServiceSaveOrUpdate(reservationImage);
	}


	public static GdsReservation<TravellerTicketControlInformationWrapper> getGdsReservationTicketControlView(String pnr) {

		GdsReservation<TravellerTicketControlInformationWrapper> gdsReservation;
		GdsReservationImage reservationImage = GDSServicesDAOUtils.DAOInstance.GDSSERVICES_DAO.getGDSReservationImage(pnr, GdsReservationImage.ImageContext.TICKET_CONTROL);

		if (reservationImage != null) {
			gdsReservation = (GdsReservation<TravellerTicketControlInformationWrapper>) GDSApiUtils.toObject(reservationImage.getReservationImage());
		} else {
			gdsReservation = new GdsReservation<TravellerTicketControlInformationWrapper>();
		}

		return gdsReservation;
	}

	public static void saveGdsReservationTicketControlView(String pnr, GdsReservation<TravellerTicketControlInformationWrapper> gdsReservation) {
		GdsReservationImage reservationImage = GDSServicesDAOUtils.DAOInstance.GDSSERVICES_DAO.getGDSReservationImage(pnr, GdsReservationImage.ImageContext.TICKET_CONTROL);

		if (reservationImage == null) {
			reservationImage = new GdsReservationImage();
			reservationImage.setPnr(pnr);
			reservationImage.setImageContext(GdsReservationImage.ImageContext.TICKET_CONTROL);
		}

		reservationImage.setReservationImage(GDSApiUtils.toBlob(gdsReservation));
		GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO.gdsTypeAServiceSaveOrUpdate(reservationImage);
	}

}
