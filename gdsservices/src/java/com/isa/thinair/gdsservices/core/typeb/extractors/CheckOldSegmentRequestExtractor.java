package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.internal.CheckOldSegmentsRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

public class CheckOldSegmentRequestExtractor extends ValidatorBase {

	public static CheckOldSegmentsRequest getRequest(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO) {

		CheckOldSegmentsRequest checkOldSegmentsRequest = new CheckOldSegmentsRequest();

		checkOldSegmentsRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
		checkOldSegmentsRequest = (CheckOldSegmentsRequest) RequestExtractorUtil.addBaseAttributes(checkOldSegmentsRequest,
				bookingRequestDTO);

		checkOldSegmentsRequest = addOldSegments(checkOldSegmentsRequest, bookingRequestDTO);
		checkOldSegmentsRequest.setNotSupportedSSRExists(bookingRequestDTO.isNotSupportedSSRExists());

		return checkOldSegmentsRequest;
	}

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {

		String pnrReference = ValidatorUtils.getPNRReference(bookingRequestDTO.getResponderRecordLocator());

		if (pnrReference.length() == 0) {
			String message = MessageUtil.getMessage("gdsservices.extractors.emptyPNRResponder");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		} else {
			// FIXME: HN ?
			this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
			return bookingRequestDTO;
		}
	}

	/**
	 * adds old segments to the CheckOldSegmentsRequest
	 * 
	 * @param checkOldSegmentsRequest
	 * @param bookingRequestDTO
	 * @return
	 */
	private static CheckOldSegmentsRequest addOldSegments(CheckOldSegmentsRequest checkOldSegmentsRequest,
			BookingRequestDTO bookingRequestDTO) {

		List<BookingSegmentDTO> segmentDTOs = bookingRequestDTO.getBookingSegmentDTOs();
		Collection<Segment> segments = new ArrayList<Segment>();
		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

		for (Iterator<BookingSegmentDTO> iterator = segmentDTOs.iterator(); iterator.hasNext();) {
			BookingSegmentDTO segmentDTO = (BookingSegmentDTO) iterator.next();

			if (carrierCode.equals(segmentDTO.getCarrierCode())) {
				String gdsActionCode = segmentDTO.getActionOrStatusCode();

				if (gdsActionCode.equals(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode())
						|| gdsActionCode.equals(GDSExternalCodes.StatusCode.CODESHARE_CONFIRMED.getCode())) {
					Segment segment = RequestExtractorUtil.getSegment(segmentDTO);
					segments.add(segment);
				}
			}
		}

		checkOldSegmentsRequest.setSegments(segments);

		return checkOldSegmentsRequest;
	}

}
