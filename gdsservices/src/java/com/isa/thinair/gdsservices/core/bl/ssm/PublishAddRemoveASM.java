/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.core.bl.ssm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.NotifyFlightEventRQ;
import com.isa.thinair.airinventory.api.dto.NotifyFlightEventsRQ;
import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.FlightEventCode;
import com.isa.thinair.airschedules.api.utils.GDSFlightEventCollector;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.XMLStreamer;
import com.isa.thinair.gdsservices.api.model.GDSPublishMessage;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSServicesDAO;
import com.isa.thinair.msgbroker.api.dto.ASMessegeDTO;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for integrate create schedule with gds
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="publishAddRemoveASM"
 */
public class PublishAddRemoveASM extends DefaultBaseCommand {

	private BookingClassBD bookingClassBD;

	private FlightInventoryBD flightInventoryBD;

	// Dao's
	private GDSServicesDAO gdsServiceDao;

	/**
	 * constructor of the publishAddRemoveASM command
	 */
	public PublishAddRemoveASM() {

		// looking up BDs
		bookingClassBD = GDSServicesModuleUtil.getBookingClassBD();

		flightInventoryBD = GDSServicesModuleUtil.getFlightInventoryBD();

		// lokking up DAOs
		gdsServiceDao = (GDSServicesDAO) GDSServicesDAOUtils.DAOInstance.GDSSERVICES_DAO;
	}

	/**
	 * execute method of the PublishUpdateASM command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		GDSFlightEventCollector sGdsEvents = (GDSFlightEventCollector) this
				.getParameter(GDSSchedConstants.ParamNames.GDS_EVENT_COLLECTOR);

		boolean isExternalSSMRecap = sGdsEvents.isExternalSSMRecap();
		String airLineCode = GDSServicesModuleUtil.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
		Map aircraftTypeMap = GDSServicesModuleUtil.getGlobalConfig().getIataAircraftCodes();

		if (!isExternalSSMRecap) {

			Map gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
			Collection addedList = (sGdsEvents.getAddedGDSList() != null) ? sGdsEvents.getAddedGDSList() : new ArrayList();
			Collection deletedList = (sGdsEvents.getDeletedGDSList() != null) ? sGdsEvents.getDeletedGDSList() : new ArrayList();

			Collection gdsIdsForRbd = new ArrayList();
			gdsIdsForRbd.addAll(addedList);
			gdsIdsForRbd.addAll(deletedList);

			Map bcMap = bookingClassBD.getBookingClassCodes(gdsIdsForRbd);

			if (sGdsEvents.containsAction(GDSFlightEventCollector.GDS_DELETED)) {

				// cancel flight for the removed gdses
				if (sGdsEvents.getUpdatedFlight().getScheduleId() == null) {

					sendASMCancel(gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(), sGdsEvents.getUpdatedFlight(),
							airLineCode, deletedList, bcMap, sGdsEvents.getSupplementaryInfo());
				}
			}

			if (sGdsEvents.containsAction(GDSFlightEventCollector.GDS_ADDED)) {

				// create flight for the added gdses
				if (sGdsEvents.getUpdatedFlight().getScheduleId() == null) {

					sendASMNew(gdsStatusMap, aircraftTypeMap, sGdsEvents.getServiceType(), sGdsEvents.getUpdatedFlight(),
							airLineCode, addedList, bcMap, sGdsEvents.getSupplementaryInfo());
				}
			}
		} else {
			if (sGdsEvents.containsAction(GDSFlightEventCollector.EXTERNAL_SSM_RECAP)) {
				String externalEmail = sGdsEvents.getExternalEmailAddress();
				String externalSita = sGdsEvents.getExternalSitaAddress();
				sendASMExternal(aircraftTypeMap, sGdsEvents.getServiceType(), sGdsEvents.getUpdatedFlight(), airLineCode,
						externalSita, externalEmail);
			}
		}

		// constructing response
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		// return command response
		return responce;
	}

	private void sendASM(String gdsCode, String airLineCode, ASMessegeDTO newASM, GDSPublishMessage pubMessage) {

		try {

			//ReservationToGDSMessageSenderBD gdsSender = CommonPublishUtil.getGDSSender();
			GDSServicesModuleUtil.getSSMASMServiceBD().sendAdHocScheduleMessage(gdsCode, airLineCode, newASM);
			gdsServiceDao.saveGDSPublisingMessage(pubMessage);

		} catch (Exception e) {

			pubMessage.setRemarks("Message broker invocation failed");
			pubMessage.setStatus(GDSPublishMessage.MESSAGE_FAILED);
			gdsServiceDao.saveGDSPublisingMessage(pubMessage);
		}
	}

	private void notifyInventory(FlightEventCode eventcode, Integer flightId, Collection gdsIDs) throws ModuleException {

		// notifiy flight events to inventory to send corresponding avs messages
		NotifyFlightEventsRQ notifyFlightEventsRQ = new NotifyFlightEventsRQ();
		NotifyFlightEventRQ newFlightEnvent = new NotifyFlightEventRQ();
		newFlightEnvent.setFlightEventCode(eventcode);
		newFlightEnvent.addFlightId(flightId);

		if (FlightEventCode.FLIGHT_CREATED.equals(eventcode)) {
			newFlightEnvent.setGdsIdsAdded(gdsIDs);
		} else {
			newFlightEnvent.setGdsIdsRemoved(gdsIDs);
		}

		notifyFlightEventsRQ.addNotifyFlightEventRQ(newFlightEnvent);
		flightInventoryBD.notifyFlightEvent(notifyFlightEventsRQ);
	}

	private void sendASMNew(Map gdsStatsMap, Map aircraftTypeMap, String serviceType, Flight flight, String airLineCode,
			Collection gdsList, Map bcMap, String supplementaryInfo) throws ModuleException {

		if (gdsList != null && gdsList.size() > 0) {

			ASMessegeDTO newASM = new ASMessegeDTO();
			SchedulePublishUtil.populateASMforNEW(newASM, flight, serviceType,
					(String) aircraftTypeMap.get(flight.getModelNumber()), supplementaryInfo);

			Iterator itGdsIds = gdsList.iterator();
			ArrayList activeGdses = new ArrayList();

			while (itGdsIds.hasNext()) {

				Integer gdsId = (Integer) itGdsIds.next();
				SchedulePublishUtil.addRBDInfo(newASM, (Collection) bcMap.get(gdsId.toString()));
				GDSStatusTO gdsStatus = (GDSStatusTO) gdsStatsMap.get(gdsId.toString());

				GDSPublishMessage pubMessage = SchedulePublishUtil.createASMSuccessPublishLog(gdsId, XMLStreamer.compose(newASM),
						newASM.getReferenceNumber(), flight.getFlightId());

				if (GDSInternalCodes.GDSStatus.ACT.getCode().equals(gdsStatus.getStatus())) {
					activeGdses.add(gdsId);
					sendASM(gdsStatus.getGdsCode(), airLineCode, newASM, pubMessage);
				} else {
					pubMessage.setStatus(GDSPublishMessage.GDS_INACTIVE);
					gdsServiceDao.saveGDSPublisingMessage(pubMessage);
				}
			}

			if (activeGdses.size() > 0)
				notifyInventory(FlightEventCode.FLIGHT_CREATED, flight.getFlightId(), activeGdses);
		}
	}

	private void sendASMCancel(Map gdsStatsMap, Map aircraftTypeMap, String serviceType, Flight flight, String airLineCode,
			Collection gdsList, Map bcMap, String supplementaryInfo) throws ModuleException {

		if (gdsList != null && gdsList.size() > 0) {

			ASMessegeDTO newASM = new ASMessegeDTO();
			SchedulePublishUtil.populateASMforCNL(newASM, flight, serviceType,
					(String) aircraftTypeMap.get(flight.getModelNumber()), supplementaryInfo);

			Iterator itGdsIds = gdsList.iterator();
			ArrayList activeGdses = new ArrayList();

			while (itGdsIds.hasNext()) {
				Integer gdsId = (Integer) itGdsIds.next();
				SchedulePublishUtil.addRBDInfo(newASM, (Collection) bcMap.get(gdsId.toString()));
				GDSStatusTO gdsStatus = (GDSStatusTO) gdsStatsMap.get(gdsId.toString());

				GDSPublishMessage pubMessage = SchedulePublishUtil.createASMSuccessPublishLog(gdsId, XMLStreamer.compose(newASM),
						newASM.getReferenceNumber(), flight.getFlightId());

				if (GDSInternalCodes.GDSStatus.ACT.getCode().equals(gdsStatus.getStatus())) {
					activeGdses.add(gdsId);
					sendASM(gdsStatus.getGdsCode(), airLineCode, newASM, pubMessage);
				} else {
					pubMessage.setStatus(GDSPublishMessage.GDS_INACTIVE);
					gdsServiceDao.saveGDSPublisingMessage(pubMessage);
				}
			}

			if (activeGdses.size() > 0)
				notifyInventory(FlightEventCode.FLIGHT_CANCELLED, flight.getFlightId(), activeGdses);
		}
	}

	private void sendASMExternal(Map aircraftTypeMap, String serviceType, Flight flight, String airLineCode, String sitaAddress,
			String emailAddress) throws ModuleException {

		ASMessegeDTO newASM = new ASMessegeDTO();
		SchedulePublishUtil.populateASMforNEW(newASM, flight, serviceType, (String) aircraftTypeMap.get(flight.getModelNumber()),
				null);
		newASM.setPassengerResBookingDesignator(AppSysParamsUtil.getCommonBookingCodesForExternalSSMRecap());
		newASM.setExternalEmailAddres(emailAddress);
		newASM.setExternalSitaAddress(sitaAddress);
		GDSPublishMessage pubMessage = SchedulePublishUtil.createASMSuccessPublishLog(null, XMLStreamer.compose(newASM),
				newASM.getReferenceNumber(), flight.getFlightId());
		sendASMToExternalSystem(airLineCode, newASM, pubMessage);

	}

	private void sendASMToExternalSystem(String airLineCode, ASMessegeDTO newASM, GDSPublishMessage pubMessage) {

		try {
			// ReservationToGDSMessageSenderBD gdsSender = CommonPublishUtil.getGDSSender();
			GDSServicesModuleUtil.getSSMASMServiceBD().sendAdHocScheduleMessageToExternalSystem(airLineCode, newASM);
			gdsServiceDao.saveGDSPublisingMessage(pubMessage);

		} catch (Exception e) {

			pubMessage.setRemarks("Message broker invocation failed");
			pubMessage.setStatus(GDSPublishMessage.MESSAGE_FAILED);
			gdsServiceDao.saveGDSPublisingMessage(pubMessage);
		}
	}
}
