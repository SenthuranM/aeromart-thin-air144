package com.isa.thinair.gdsservices.core.typeA.transformers.paoreqres.v03;

import iata.typea.v031.paoreq.PAOREQ;

import java.util.Map;

import javax.xml.bind.JAXBElement;

import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.core.typeA.helpers.Constants;
import com.isa.thinair.gdsservices.core.typeA.transformers.EDIFACTMessageProcess;

public class SeamlessAvailability implements EDIFACTMessageProcess {

	@Override
	public Map<String, Object> extractSpecificCommandParams(Object message) {
		PAOREQ paoreq = (PAOREQ) message;
		return SeamlessAvailabilityRequestHandler.getOwnMessageParams(paoreq);
	}

	@Override
	public JAXBElement<?> constructEDIResponce(DefaultServiceResponse response) throws InteractiveEDIException {
		return SeamlessAvailabilityResponseHandler.handleResponce(response);
	}

	@Override
	public String getCommandName() {
		return Constants.CommandNames.SEAMLESS_AVAILABILITY;
	}

}
