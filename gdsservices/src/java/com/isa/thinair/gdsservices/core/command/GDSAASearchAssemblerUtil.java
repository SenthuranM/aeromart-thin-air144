package com.isa.thinair.gdsservices.core.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;

/**
 * GDS Availability Search Assembler
 * 
 * @author Nilindra Fernando
 * @since 6:05 PM 9/19/2008
 */
public class GDSAASearchAssemblerUtil {

	/**
	 * Returns the reverse segment code
	 * 
	 * @param segmentCode
	 * @return
	 */
	public static String getReverseSegmentCode(String segmentCode) {
		String[] elements = segmentCode.split("/");
		String value = "";

		int i = elements.length - 1;
		while (i >= 0) {
			if (value.length() == 0) {
				value = elements[i];
			} else {
				value = value + "/" + elements[i];
			}
			i--;
		}

		return value;
	}

	/**
	 * Returns the segments ordered
	 * 
	 * @param segments
	 * @return
	 */
	public static Collection<Segment> getSegmentsOrdered(Collection<Segment> segments) {
		List<Segment> lstSegments = new ArrayList<Segment>();
		lstSegments.addAll(segments);

		Collections.sort(lstSegments);

		return lstSegments;
	}

	/**
	 * Returns the booking class wise segments
	 * 
	 * @param segments
	 * @return
	 */
	public static Map<String, Collection<Segment>> getBookingClassWiseSegments(Collection<Segment> segments) {
		Map<String, Collection<Segment>> bookingClassWiseSegments = new HashMap<String, Collection<Segment>>();
		Collection<Segment> perBookingSegments;

		for (Segment segment : segments) {
			perBookingSegments = bookingClassWiseSegments.get(segment.getBookingCode());

			if (perBookingSegments != null) {
				perBookingSegments.add(segment);
			} else {
				perBookingSegments = new ArrayList<Segment>();
				perBookingSegments.add(segment);
				bookingClassWiseSegments.put(segment.getBookingCode(), perBookingSegments);
			}
		}

		return bookingClassWiseSegments;
	}

	/**
	 * Returns till turning point segments
	 * 
	 * @param segments
	 * @return
	 */
	public static Collection[] getTurningPointSegments(Collection<Segment> segments) {
		Collection<String> segmentCodes = new ArrayList<String>();
		Collection<Segment> tillTurningPointSegments = new ArrayList<Segment>();
		Collection<Segment> afterTurningPointSegments = new ArrayList<Segment>();

		for (Segment segment : segments) {
			if (segmentCodes.size() == 0) {
				segmentCodes.add(segment.getSegmentCode());
				tillTurningPointSegments.add(segment);
			} else {
				if (afterTurningPointSegments.size() > 0
						|| segmentCodes.contains(GDSAASearchAssemblerUtil.getReverseSegmentCode(segment.getSegmentCode()))) {
					afterTurningPointSegments.add(segment);
				} else {
					segmentCodes.add(segment.getSegmentCode());
					tillTurningPointSegments.add(segment);
				}
			}
		}

		return new Collection[] { afterTurningPointSegments, tillTurningPointSegments };
	}

	/**
	 * Returns whether or not this is a return booking
	 * 
	 * @param bookingClassWiseSegments
	 * @return
	 */
	public static boolean isAccelAeroReturnBooking(Collection<Segment> segments) {
		Collection[] segmentResults = getTurningPointSegments(segments);
		Collection<Segment> afterTurningPointSegments = segmentResults[0];
		Collection<Segment> tillTuriningPointSegments = segmentResults[1];

		if (afterTurningPointSegments.size() == tillTuriningPointSegments.size()) {
			Collection<String> afterTurningPointONDCodes = getOndCodeList(afterTurningPointSegments);
			Collection<String> tillTuriningPointONDCodes = getOndCodeList(tillTuriningPointSegments);

			String ondCode1 = ReservationApiUtils.getOndCode(tillTuriningPointONDCodes);
			String ondCode2 = getReverseSegmentCode(ReservationApiUtils.getOndCode(afterTurningPointONDCodes));

			if (ondCode1.equals(ondCode2)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Returns the ond code list
	 * 
	 * @param segments
	 * @return
	 */
	private static Collection<String> getOndCodeList(Collection<Segment> segments) {
		Collection<String> ondCodes = new ArrayList<String>();
		for (Segment segment : segments) {
			ondCodes.add(segment.getSegmentCode());
		}

		return ondCodes;
	}

	/**
	 * Is a 360 Degree trip
	 * 
	 * @param segments
	 * @return
	 */
	public static boolean is360DegreesTrip(Collection<Segment> segments) {
		Segment firstSegment = (Segment) BeanUtils.getFirstElement(segments);
		Segment lastSegment = (Segment) BeanUtils.getLastElement(segments);

		if (firstSegment.getDepartureStation().equals(lastSegment.getArrivalStation())) {
			return true;
		} else {
			return false;
		}
	}

	public static void main(String[] args) {
		Collection<String> segmentCodes = new ArrayList<String>();
		segmentCodes.add("CMB/SHJ");
		segmentCodes.add("SHJ/KTM");
		segmentCodes.add("KTM/MCT");
		segmentCodes.add("MCT/KTM");
		segmentCodes.add("KTM/BAH");
		segmentCodes.add("BAH/CMB");
		System.out.println(ReservationApiUtils.getOndCode(segmentCodes));
		System.out.println(getReverseSegmentCode(ReservationApiUtils.getOndCode(segmentCodes)));
	}
}
