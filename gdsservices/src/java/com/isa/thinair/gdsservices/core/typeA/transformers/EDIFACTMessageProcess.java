package com.isa.thinair.gdsservices.core.typeA.transformers;

import java.util.Map;

import javax.xml.bind.JAXBElement;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;

/**
 * 
 * @author sanjaya
 * 
 */
public interface EDIFACTMessageProcess {

	/**
	 * @param message
	 * @return
	 * @throws ModuleException
	 */
	public Map<String, Object> extractSpecificCommandParams(Object message) throws ModuleException;

	public JAXBElement<?> constructEDIResponce(DefaultServiceResponse response) throws ModuleException;

	public String getCommandName();

}
