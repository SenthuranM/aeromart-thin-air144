package com.isa.thinair.gdsservices.core.bl.internal.reservation;

import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.TransitionTo;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.typea.internal.ChangePaxDetailsRq;
import com.isa.thinair.gdsservices.api.dto.typea.internal.RemoveSegmentRq;
import com.isa.thinair.gdsservices.api.dto.typea.internal.ReservationRq;
import com.isa.thinair.gdsservices.api.dto.typea.internal.ReservationRs;
import com.isa.thinair.gdsservices.api.dto.typea.internal.SplitResOrRemovePaxRq;
import com.isa.thinair.gdsservices.api.model.ExternalReservation;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.dto.temp.GdsReservation;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerTicketingInformationWrapper;
import com.isa.thinair.gdsservices.core.util.GdsInfoDaoHelper;
import com.isa.typea.common.TravellerTicketingInformation;

public class ReservationModificationHandlerImpl implements ReservationModificationHandler {

	public ReservationRs splitResOrRemovePax(ReservationRq reservationRq) {
		SplitResOrRemovePaxRq splitResOrRemovePaxRq = (SplitResOrRemovePaxRq) reservationRq;

		GdsReservation<TravellerTicketingInformationWrapper> oldGdsReservation =
				GdsInfoDaoHelper.getGdsReservationTicketView(splitResOrRemovePaxRq.getOldPnr());


		GdsReservation<TravellerTicketingInformationWrapper> newGdsReservation =
				GdsInfoDaoHelper.getGdsReservationTicketView(splitResOrRemovePaxRq.getNewPnr());
		newGdsReservation.getReservationControlInformation().addAll(oldGdsReservation.getReservationControlInformation());
		newGdsReservation.getTravellerInformation().addAll(oldGdsReservation.getTravellerInformation());

		GdsInfoDaoHelper.saveGdsReservationTicketView(splitResOrRemovePaxRq.getNewPnr(), newGdsReservation);

		ExternalReservation extReservation = new ExternalReservation();
		extReservation.setPnr(splitResOrRemovePaxRq.getNewPnr());
		extReservation.setExternalSystem(ExternalReservation.ExternalSystem.GDS);
		extReservation.setExternalRecordLocator(splitResOrRemovePaxRq.getNewExternalRecordLocator());
		extReservation.setGdsId(splitResOrRemovePaxRq.getGdsId());
		extReservation.setPriceSynced(CommonsConstants.NO);
		extReservation.setReservationSynced(CommonsConstants.YES);
		extReservation.setLockStatus(ExternalReservation.LockStatus.OPEN);
		GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO.gdsTypeAServiceSaveOrUpdate(extReservation);

		return new ReservationRs();
	}

	public ReservationRs removeSegments(ReservationRq reservationRq) {
		RemoveSegmentRq removeSegmentRq = (RemoveSegmentRq) reservationRq;



		return new ReservationRs();
	}

	public ReservationRs changePaxDetails(ReservationRq reservationRq) {
		ChangePaxDetailsRq changePaxDetailsRq = (ChangePaxDetailsRq) reservationRq;

		GdsReservation<TravellerTicketingInformationWrapper> gdsReservation =
				GdsInfoDaoHelper.getGdsReservationTicketView(changePaxDetailsRq.getPnr());

		TravellerTicketingInformation traveller;
		Passenger passenger;
		for (TransitionTo<Passenger> transition : changePaxDetailsRq.getNameChanges()) {
			passenger = transition.getOldVal();
			for (TravellerTicketingInformationWrapper wrapper : gdsReservation.getTravellerInformation()) {
                traveller = wrapper.getTravellerInformation();
				if ((passenger.getFirstName() != null && traveller.getTravellers().get(0).getGivenName() != null
						&& passenger.getFirstName().equals(traveller.getTravellers().get(0).getGivenName())) &&
						(passenger.getLastName() != null && traveller.getTravellerSurname() != null
								&& passenger.getLastName().equals(traveller.getTravellerSurname()))) {

					traveller.getTravellers().get(0).setGivenName(transition.getNewVal().getFirstName());
					traveller.setTravellerSurname(transition.getNewVal().getLastName());
				}
			}
		}

		GdsInfoDaoHelper.saveGdsReservationTicketView(changePaxDetailsRq.getPnr(), gdsReservation);


		return new ReservationRs();
	}
}
