package com.isa.thinair.gdsservices.core.typeA.model;

import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;

public enum RDD {

	C {
		@Override
		public void validate(Object object) {
			return;
		}
	},
	M {
		@Override
		public void validate(Object object) throws InteractiveEDIException {
			if (object == null) {
				throw new InteractiveEDIException(CodeSetEnum.CS0085.MISSING, "Mandetary value missing");
			}

		}
	},
	NA {
		@Override
		public void validate(Object object) {
			return;
		}
	};

	public abstract void validate(Object object) throws InteractiveEDIException;

}
