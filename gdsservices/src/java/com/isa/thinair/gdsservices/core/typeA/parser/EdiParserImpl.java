package com.isa.thinair.gdsservices.core.typeA.parser;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.xml.datatype.DatatypeConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.core.dto.FinancialInformation;
import com.isa.thinair.gdsservices.core.typeA.helpers.XmlDataConverter;
import com.isa.typea.common.Header;
import com.isa.typea.common.InterchangeHeader;
import com.isa.typea.common.InterchangeInfo;
import com.isa.typea.common.MessageHeader;
import com.isa.typea.common.MessageIdentifier;

public class EdiParserImpl implements EdiParser {
	private static Log log = LogFactory.getLog(EdiParserImpl.class);

	public Map<TypeAParseConstants.Segment, List<?>> parseEdiMessage(String ediMessage, Set<TypeAParseConstants.Segment> segsToParse) {

		Map<TypeAParseConstants.Segment, List<?>> parsedSegs = new HashMap<TypeAParseConstants.Segment, List<?>>();

		String[][][] splitMsg = splitMessage(ediMessage);

		for (TypeAParseConstants.Segment segment : segsToParse) {
			switch (segment) {
			case UNB:
				parsedSegs.put(segment, parseHeaders(splitMsg));
				break;
			case UNH:
				parsedSegs.put(segment, parseMessageHeaders(splitMsg));
				break;
			}
		}

		return parsedSegs;

	}

	@Override
	public FinancialInformation parseFinancialInformation(Map<TypeAParseConstants.Segment, String> rawFinancialInfo) {
		FinancialInformation financialInformation = null;
		long startTime = System.currentTimeMillis();

		for (TypeAParseConstants.Segment segment : rawFinancialInfo.keySet()) {
			switch (segment) {
			case IFT_15:
				financialInformation = parseFares(rawFinancialInfo.get(TypeAParseConstants.Segment.IFT_15));
				break;
			}
		}
		long endTime = System.currentTimeMillis();
		log.debug("EdiParserImpl - parseFinancialInformation ---- exec time (ms) : " + (endTime - startTime));

		return financialInformation;
	}

	public Map<TypeAParseConstants.Segment, String> generateFinancialInformation(FinancialInformation financialInformation) {
		FareCalculationParser parser = new FareCalculationParserRegExImpl();

		Map<TypeAParseConstants.Segment, String> financialInfoSegments = new HashMap<TypeAParseConstants.Segment, String>();
		financialInfoSegments.put(TypeAParseConstants.Segment.IFT_15, parser.buildFareInformationText(financialInformation));

		return financialInfoSegments;
	}

	private List<Header> parseHeaders(String[][][] splitMsg) {
		List<Header> headers = new ArrayList<Header>();
		Header header;

		InterchangeHeader interchangeHeader;
		InterchangeInfo interchangeInfo;
		String date = null;
		String time = null;
		Date timeStamp;

		try {
			for (String[][] seg : splitMsg) {
				if (seg[0][0].equals(TypeAParseConstants.Segment.UNB.name())) {
					header = new Header();
					interchangeHeader = new InterchangeHeader();
					if (seg.length > 1) {
						if (seg[1].length > 0)
							interchangeHeader.setSyntaxId(seg[1][0]);
						if (seg[1].length > 1)
							interchangeHeader.setVersionNo(seg[1][1]);
					}
					if (seg.length > 2) {
						interchangeInfo = new InterchangeInfo();
						if (seg[2].length > 0)
							interchangeInfo.setInterchangeId(seg[2][0]);
						if (seg[2].length > 1)
							interchangeInfo.setInterchangeCode(seg[2][1]);

						interchangeHeader.setSenderInfo(interchangeInfo);
					}
					if (seg.length > 3) {
						interchangeInfo = new InterchangeInfo();
						if (seg[3].length > 0)
							interchangeInfo.setInterchangeId(seg[3][0]);
						if (seg[3].length > 1)
							interchangeInfo.setInterchangeCode(seg[3][1]);

						interchangeHeader.setReceiverInfo(interchangeInfo);
					}
					if (seg.length > 4) {
						if (seg[4].length > 0)
							date = seg[4][0];
						if (seg[4].length > 1)
							time = seg[4][1];

						timeStamp = XmlDataConverter.toDateTime(date, "ddMMyy", time, "HHmm");
						interchangeHeader.setTimestamp(CalendarUtil.asXMLGregorianCalendar(timeStamp));
					}
					if (seg.length > 5 && seg[5].length > 0)
						interchangeHeader.setInterchangeControlRef(seg[5][0]);
					if (seg.length > 6 && seg[6].length > 0)
						interchangeHeader.setRecipientRef(seg[6][0]);
					if (seg.length > 8 && seg[8].length > 0)
						interchangeHeader.setAssociationCode(seg[8][0]);

					header.setInterchangeHeader(interchangeHeader);
					headers.add(header);
				}
			}
		} catch (DatatypeConfigurationException e) {
			log.error("Error while parsing EDI-Header", e);
		}

		return headers;
	}

	private List<MessageHeader> parseMessageHeaders(String[][][] splitMsg) {
		List<MessageHeader> messageHeaders = new ArrayList<MessageHeader>();
		MessageHeader messageHeader;

		MessageIdentifier messageIdentifier;

		for (String[][] seg : splitMsg) {
			if (seg[0][0].equals(TypeAParseConstants.Segment.UNH.name())) {
				messageHeader = new MessageHeader();

				if (seg.length > 1 && seg[1].length > 0)
					messageHeader.setReferenceNo(seg[1][0]);

				if (seg.length > 2) {
					messageIdentifier = new MessageIdentifier();

					if (seg[2].length > 0)
						messageIdentifier.setMessageType(seg[2][0]);
					if (seg[2].length > 1)
						messageIdentifier.setVersion(seg[2][1]);
					if (seg[2].length > 2)
						messageIdentifier.setReleaseNumber(seg[2][2]);
					if (seg[2].length > 3)
						messageIdentifier.setControllingAgency(seg[2][3]);

					messageHeader.setMessageId(messageIdentifier);
				}

				if (seg.length > 3 && seg[3].length > 0)
					messageHeader.setCommonAccessReference(seg[3][0]);

				messageHeaders.add(messageHeader);
			}
		}

		return messageHeaders;
	}

	private String[][][] splitMessage(String ediMessage) {
		String[][][] splitMessage;
		String[][] elements;

		ediMessage = ediMessage.replaceAll("\n","");

		String[] splitSegs = ediMessage.split(Pattern.quote(TypeAParseConstants.Delimiter.SEGMENT));
		String[] splitElements;
		String[] splitData;

		splitMessage = new String[splitSegs.length][][];

		for (int a = 0; a < splitSegs.length; a++) {
			splitElements = splitSegs[a].split(Pattern.quote(TypeAParseConstants.Delimiter.ELEMENT));
			elements = new String[splitElements.length][];

			for (int b = 0; b < splitElements.length; b++) {
				splitData = splitElements[b].split(Pattern.quote(TypeAParseConstants.Delimiter.DATA));
				elements[b] = splitData;
			}
			splitMessage[a] = elements;
		}

		return splitMessage;
	}

	private FinancialInformation parseFares(String fares) {
		FinancialInformation financialInformation;
		FareCalculationParser parser = new FareCalculationParserRegExImpl();

		financialInformation = parser.parseFinancialInformation(fares);

		return financialInformation;
	}

}
