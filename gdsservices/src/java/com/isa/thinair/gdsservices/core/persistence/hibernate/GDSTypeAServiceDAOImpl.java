package com.isa.thinair.gdsservices.core.persistence.hibernate;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.gdsservices.api.dto.typea.DisplayTicketParams;
import com.isa.thinair.gdsservices.api.dto.typea.DisplayTicketTo;
import com.isa.thinair.gdsservices.api.model.GDSReservationInfo;
import com.isa.thinair.gdsservices.api.model.GDSReservationPaxInfo;
import com.isa.thinair.gdsservices.api.model.GDSTransactionStatus;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSTypeAServiceDAO;

/**
 * Hibernate implementation of the GDSTypeAServiceDAO
 * 
 * @author mekanayake
 * @isa.module.dao-impl dao-name="GDSTypeAServiceDAO"
 */
public class GDSTypeAServiceDAOImpl extends PlatformHibernateDaoSupport implements GDSTypeAServiceDAO {

	private final Log log = LogFactory.getLog(GDSTypeAServiceDAOImpl.class);

	@Override
	public GDSTransactionStatus getGDSTransactionStatus(long id) {
		return (GDSTransactionStatus) get(GDSTransactionStatus.class, new Long(id));
	}

	public <T> T gdsTypeAServiceGet(Class<T> e, Serializable id) {
		return (T) get(e, id);
	}

	public <T> void gdsTypeAServiceSaveOrUpdate(T t) {
		hibernateSaveOrUpdate(t);
	}

	@Override
	public <T> void gdsTypeAServiceLock(T t, boolean wait) {
		LockMode lockMode;

		if (wait) {
			lockMode = LockMode.UPGRADE;
		} else {
			lockMode = LockMode.UPGRADE_NOWAIT;
		}
		lock(t, lockMode);
	}

	@Override
	public GDSTransactionStatus getGDSTransactionStatusFromRef(String tnxRefId) {
		String hql = "SELECT tnxStatus FROM GDSTransactionStatus AS tnxStatus WHERE tnxStatus.gdsTransactionRef = ? ";
		Object[] params = { tnxRefId };
		Collection<GDSTransactionStatus> collection = find(hql, params, GDSTransactionStatus.class);
		return BeanUtils.getFirstElement(collection);
	}

	@Override
	public String generateRespCommonAccessRefKey() {
		SQLQuery query = getSession().createSQLQuery(" SELECT S_TYPE_A_RESPONDER_KEY.NEXTVAL RESP_KEY FROM DUAL ");
		query.addScalar("RESP_KEY", StringType.INSTANCE);
		return (String) query.uniqueResult() + "";
	}

	public Integer getSettlementAuthorizationKey() {
		SQLQuery query = getSession().createSQLQuery(" SELECT S_SETTLEMENT_AUTH_CODE.NEXTVAL SAC_KEY FROM DUAL ");
		query.addScalar("SAC_KEY", IntegerType.INSTANCE);
		return (Integer) query.uniqueResult();
	}

	public List<GDSReservationInfo> getGDSReservationInfo(String pnr) {
		return find ("SELECT ri FROM GDSReservationInfo ri WHERE ri.pnr = ? ", pnr, GDSReservationInfo.class );
	}

	public List<GDSReservationInfo> getGDSReservationInfo(List<String> pnrs) {
		return getSession().createQuery("SELECT ri FROM GDSReservationInfo ri WHERE ri.pnr in ( :pnrs )")
				.setParameterList("pnrs", pnrs).list();
	}

	public List<GDSReservationPaxInfo> getGDSReservationPaxInfo(long pnrPaxId) {
		return find ("SELECT pi FROM GDSReservationPaxInfo pi WHERE pi.pnrPaxId = ? ", pnrPaxId, GDSReservationPaxInfo.class);
	}

	public List<GDSReservationPaxInfo> getGDSReservationPaxInfo(List<Long> pnrPaxIds) {
		return getSession().createQuery("SELECT pi FROM GDSReservationPaxInfo pi WHERE pi.pnrPaxId in ( :pnrPaxIds ) " )
				.setParameterList("pnrPaxIds", pnrPaxIds).list();
	}

	@Override
	public List<DisplayTicketTo> searchTickets(DisplayTicketParams params) {
		List<DisplayTicketTo> tickets = new ArrayList<DisplayTicketTo>();
		DisplayTicketTo ticket;

		String[] segAirports;

		StringBuilder queryContent = new StringBuilder();
		int criteriaCount = 0;
		queryContent
				.append("	SELECT p.first_name first_name,	")
				.append("	  p.last_name last_name,	")
				.append("	  p.pax_type_code pax_type_code,	")
				.append("	  r.pnr pnr,	")
				.append("	  r.external_rec_locator external_rec_locator,	")
				.append("	  g.carrier_code carrier_code,	")
				.append("	  c.ext_e_ticket_number ext_e_ticket_number,	")
				.append("	  c.ext_coupon_number ext_coupon_number,	")
				.append("	  c.ext_coupon_status ext_coupon_status,	")
				.append("	  fs.est_time_departure_local est_time_departure_local,	")
				.append("	  fs.segment_code segment_code,	")
				.append("	  f.flight_number flight_number,	")
				.append("	  rf.booking_code booking_code	")
				.append("	FROM t_reservation r,	")
				.append("	  t_pnr_passenger p,	")
				.append("	  t_pnr_segment s,	")
				.append("	  t_flight_segment fs,	")
				.append("	  t_flight f,	")
				.append("	  t_pax_e_ticket t,	")
				.append("	  t_gds g,	")
				.append("	  t_pnr_pax_fare_seg_e_ticket c,	")
				.append("	  t_pnr_pax_fare_segment rf	")
				.append("	WHERE r.pnr               = p.pnr	")
				.append("	AND r.pnr                 = s.pnr	")
				.append("	AND s.flt_seg_id          = fs.flt_seg_id	")
				.append("	AND fs.flight_id          = f.flight_id	")
				.append("	AND p.pnr_pax_id          = t.pnr_pax_id	")
				.append("	AND r.gds_id              = g.gds_id	")
				.append("	AND t.pax_e_ticket_id     = c.pax_e_ticket_id	")
				.append("	AND s.pnr_seg_id          = rf.pnr_seg_id	");

		if (params.getPassengerFirstName() != null) {
			queryContent.append(" AND p.first_name = '" + params.getPassengerFirstName() + "' ");
			criteriaCount++;
		}
		if (params.getPassengerSurname() != null) {
			queryContent.append(" AND p.last_name = '" + params.getPassengerSurname() + "' ");
			criteriaCount++;
		}
		if (params.getFlightNumber() != null) {
			queryContent.append(" AND f.flight_number = '" + params.getFlightNumber() + "' ");
			criteriaCount++;
		}
		if (params.getOrigin() != null) {
			queryContent.append(" AND fs.segment_code LIKE '" + params.getOrigin() + "/%' ");
			criteriaCount++;
		}
		if (params.getDestination() != null) {
			queryContent.append(" AND fs.segment_code LIKE '%/" + params.getDestination() + "' ");
			criteriaCount++;
		}
		if (params.getFlightDepartureDate() != null) {
			queryContent.append(" AND to_char(fs.est_time_departure_local, 'dd-mm-yyyy') = '" +
					new SimpleDateFormat("dd-MM-yyyy").format(params.getFlightDepartureDate()) + "' ");
			criteriaCount++;
		}


		if (criteriaCount > 0) {
			SQLQuery query = getSession().createSQLQuery(queryContent.toString())
					.addScalar("first_name", StringType.INSTANCE)
					.addScalar("last_name", StringType.INSTANCE)
					.addScalar("pax_type_code", StringType.INSTANCE)
					.addScalar("pnr", StringType.INSTANCE)
					.addScalar("external_rec_locator", StringType.INSTANCE)
					.addScalar("carrier_code", StringType.INSTANCE)
					.addScalar("ext_e_ticket_number", StringType.INSTANCE)
					.addScalar("ext_coupon_number", StringType.INSTANCE)
					.addScalar("ext_coupon_status", StringType.INSTANCE)
					.addScalar("est_time_departure_local", TimestampType.INSTANCE)
					.addScalar("segment_code", StringType.INSTANCE)
					.addScalar("flight_number", StringType.INSTANCE)
					.addScalar("booking_code", StringType.INSTANCE);

			List<Object[]> results = query.list();

			for (Object[] single : results) {
				ticket = new DisplayTicketTo();

				ticket.setPassengerName((String) single[0]);
				ticket.setPassengerSurname((String) single[1]);
				ticket.setPassengerType((String) single[2]);
				ticket.setPnr((String) single[3]);
				ticket.setExternalRecLocator((String) single[4]);
				ticket.setGdsCarrierCode((String) single[5]);
				ticket.setTicketNumber((String) single[6]);
				ticket.setCouponNumber((String) single[7]);
				ticket.setCouponStatus((String) single[8]);
				ticket.setFlightDepartureDate((Date) single[9]);

				segAirports = ((String) single[10]).split("/");
				ticket.setOrigin(segAirports[0]);
				ticket.setDestination(segAirports[segAirports.length - 1]);
				ticket.setFlightNumber((String) single[11]);
				ticket.setBookingDesignator((String) single[12]);

				tickets.add(ticket);
			}
		}
		return tickets;
	}

	public Map<String, Set<String>> getExternalETNumbersMap(List<String> extETicketNumbers) {
		Map<String, Set<String>> externalETNumbersMap;
		String pnr;

		String sql = "select p.pnr pnr, c.ext_e_ticket_number ext_e_ticket_number " +
				"from t_pnr_passenger p, t_pax_e_ticket t, t_pnr_pax_fare_seg_e_ticket c " +
				"where p.pnr_pax_id = t.pnr_pax_id and t.pax_e_ticket_id = c.pax_e_ticket_id " +
				"  and c.ext_e_ticket_number IN ( :ext_e_ticket_number )" ;


		Query query = getSession().createSQLQuery(sql)
				.addScalar("pnr", StringType.INSTANCE)
				.addScalar("ext_e_ticket_number", StringType.INSTANCE)
				.setParameterList("ext_e_ticket_number", extETicketNumbers, StringType.INSTANCE);


		List<Object[]> results = query.list();

		externalETNumbersMap = new HashMap<String, Set<String>>();
		for (Object[] ticket : results) {
			pnr = (String) ticket[0];
			if (!externalETNumbersMap.containsKey(pnr)) {
				externalETNumbersMap.put(pnr, new HashSet<String>());
			}
			externalETNumbersMap.get(pnr).add((String)ticket[1]);
		}

		return externalETNumbersMap;
	}

	public Map<String, Map<Integer, List<String>>> groupTickets(List<String> ticketNumbers) {
		Map<String, Map<Integer, List<String>>> groupedTickets = new HashMap<String, Map<Integer, List<String>>>();
		String pnr;
		Integer paxId;

		StringBuilder queryContent = new StringBuilder();
		queryContent
				.append(" SELECT DISTINCT p.pnr pnr,  p.pnr_pax_id pnr_pax_id, c.ext_e_ticket_number ext_e_ticket_number ")
				.append(" FROM t_pnr_passenger p, t_pax_e_ticket t, t_pnr_pax_fare_seg_e_ticket c ")
				.append(" WHERE p.pnr_pax_id = t.pnr_pax_id AND t.pax_e_ticket_id = c.pax_e_ticket_id ")
				.append("   AND c.ext_e_ticket_number IN ( :ticket_numbers ) ") ;

		try {
			Query query = getSession().createSQLQuery(queryContent.toString())
					.addScalar("pnr", StringType.INSTANCE)
					.addScalar("pnr_pax_id", IntegerType.INSTANCE)
					.addScalar("ext_e_ticket_number", StringType.INSTANCE)
					.setParameterList("ticket_numbers", ticketNumbers, StringType.INSTANCE);

			List<Object[]> results = query.list();
			for (Object[] single : results) {
				pnr = (String)single[0];
				paxId = (Integer)single[1];

				if (!groupedTickets.containsKey(pnr)) {
					groupedTickets.put(pnr, new HashMap<Integer, List<String>>());
				}
				if (!groupedTickets.get(pnr).containsKey(paxId)) {
					groupedTickets.get(pnr).put(paxId, new ArrayList<String>());
				}

				groupedTickets.get(pnr).get(paxId).add((String)single[2]);
			}

		} catch (HibernateException e) {
		 	// TODO -- throw exc
		}
		return groupedTickets;
	}
}
