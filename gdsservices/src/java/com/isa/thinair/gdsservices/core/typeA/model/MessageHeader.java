package com.isa.thinair.gdsservices.core.typeA.model;

import java.io.Serializable;

/**
 * Message header holder;
 * 
 * @author malaka
 * 
 */
public class MessageHeader implements Serializable {

	private static final long serialVersionUID = 496022460466674663L;

	private String messageReferenceNumber;

	private MessageIdentifier messageIdentifier;

	private String commonAccessReference;

	/**
	 * @return the messageReferenceNumber
	 */
	public String getMessageReferenceNumber() {
		return messageReferenceNumber;
	}

	/**
	 * @param messageReferenceNumber
	 *            the messageReferenceNumber to set
	 */
	public void setMessageReferenceNumber(String messageReferenceNumber) {
		this.messageReferenceNumber = messageReferenceNumber;
	}

	/**
	 * @return the messageIdentifier
	 */
	public MessageIdentifier getMessageIdentifier() {
		return messageIdentifier;
	}

	/**
	 * @param messageIdentifier
	 *            the messageIdentifier to set
	 */
	public void setMessageIdentifier(MessageIdentifier messageIdentifier) {
		this.messageIdentifier = messageIdentifier;
	}

	/**
	 * @return the commonAccessReference
	 */
	public String getCommonAccessReference() {
		return commonAccessReference;
	}

	/**
	 * @param commonAccessReference
	 *            the commonAccessReference to set
	 */
	public void setCommonAccessReference(String commonAccessReference) {
		this.commonAccessReference = commonAccessReference;
	}

}
