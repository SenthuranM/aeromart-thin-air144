package com.isa.thinair.gdsservices.core.typeA.parser;

import com.isa.thinair.gdsservices.core.dto.FinancialInformation;

public interface FareCalculationParser {

	FinancialInformation parseFinancialInformation(String fareInformationText);

	String buildFareInformationText(FinancialInformation financialInformation);
}
