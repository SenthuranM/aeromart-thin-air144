/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.core.remoting.ejb;

import java.util.HashMap;
import java.util.Iterator;

import javax.annotation.security.RunAs;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import com.isa.thinair.gdsservices.core.config.GDSServicesModuleConfig;
import com.isa.thinair.login.util.ForceLoginInvoker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.api.dto.publishing.Envelope;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;

/**
 * @author Lasantha Pambagoda
 */
@MessageDriven(name = "GDSPublishingMDB", activationConfig = {
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/isaGDSPublishQueue"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "maxSession", propertyValue = "5")})
@RunAs("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
public class GDSPublishingMDB implements MessageListener {
	private static final long serialVersionUID = -6583759698965963589L;
	private final Log logger = LogFactory.getLog(getClass());

	public void onMessage(Message message) {

		try {

			ObjectMessage objMessage = (ObjectMessage) message;
			Envelope envelope = (Envelope) objMessage.getObject();

			GDSServicesModuleConfig config = GDSServicesModuleUtil.getConfig();
			ForceLoginInvoker.login(config.getEjbAccessUsername(), config.getEjbAccessPassword());

			if (envelope != null) {

				Command command = (Command) GDSServicesModuleUtil.getModule().getLocalBean(envelope.getTargetLogic());

				if (command != null) {

					HashMap<String, Object> paramMap = envelope.getMessageParams();

					if (paramMap != null) {

						Iterator<String> itParams = paramMap.keySet().iterator();
						while (itParams.hasNext()) {

							String paramName = itParams.next();
							command.setParameter(paramName, paramMap.get(paramName));
						}
					}
					// execute the command, do nothing with response
					DefaultServiceResponse responce = (DefaultServiceResponse) command.execute();
				} else {
					logger.warn("Invalid Target Logic");
				}
			} else {
				logger.warn("null Envilope, nothing executed");
			}

		} catch (ModuleException e) {
			logger.error(e.getMessage(), e);
		} catch (JMSException e) {
			logger.error(e.getMessage(), e);
		}

	}
}
