package com.isa.thinair.gdsservices.core.command;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientBalancePayment;
import com.isa.thinair.commons.api.dto.ClientCommonInfoDTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.airproxy.api.utils.AuthorizationUtil;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.internal.ConfirmBookingRequest;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.platform.api.ServiceResponce;

public class ConfirmBookingAction {

	private static Log log = LogFactory.getLog(ConfirmBookingAction.class);
	public static final String E_TICKET_NUMBER = "ETICKETNUMBER";
	public static final String ON_ACCOUNT_PAYMENT = "ONACC";
	public static final String CASH_PAYMENT = "CASH";

	public static ConfirmBookingRequest processRequest(ConfirmBookingRequest confirmBookingRequest) {
		Long reservationVersion;

		try {
			// 1. Retrieve Reservation ---
			Reservation reservation = CommonUtil.loadReservation(confirmBookingRequest.getGdsCredentialsDTO(),
					confirmBookingRequest.getOriginatorRecordLocator().getPnrReference(), null);

			CommonUtil.validateReservation(confirmBookingRequest.getGdsCredentialsDTO(), reservation);


			ClientCommonInfoDTO clientInfoDTO = new ClientCommonInfoDTO();
			clientInfoDTO.setCarrierCode(GDSServicesUtil.getCarrierCode());
			clientInfoDTO.setIpAddress("127.0.0.1"); // todo -- ip

			// if on-hold - forced confirm reservation
			if (reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.ON_HOLD)) {
				reservationVersion = GDSServicesDAOUtils.DAOInstance.GDSSERVICES_SUPPORT_JDBC_DAO.getReservationVersion(reservation.getPnr());

				LCCClientBalancePayment balPay = new LCCClientBalancePayment();
				balPay.setGroupPNR(reservation.getPnr());
				balPay.setOtherCarrierPaxCreditUser(false);
				balPay.setAcceptPaymentsThanPayable(true);
				balPay.setForceConfirm(true);

				GDSServicesModuleUtil.getAirproxyReservationBD().balancePayment(balPay,
						String.valueOf(reservationVersion), false, AppSysParamsUtil.isFraudCheckEnabled(SalesChannelsUtil.SALES_CHANNEL_AGENT),
						clientInfoDTO, null, true, false);
			}

			IPayment iPayment;
			IPassenger iPassenger = new PassengerAssembler(null);

			Collection<ReservationPax> respassengers = reservation.getPassengers();

			String paymentType = GDSServicesUtil.getGDSPaymentType(confirmBookingRequest.getGds().getCode());

			for(Passenger passenger : confirmBookingRequest.getPassengers()) {
				if (paymentType != null) {

					ReservationPax pax = getReservationPax(passenger, respassengers);

					if (pax != null) {

						if (pax.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) != -1) {
							iPayment = new PaymentAssembler();

							if (ON_ACCOUNT_PAYMENT.equals(paymentType) && (confirmBookingRequest.getGdsCredentialsDTO() != null && AuthorizationUtil
									.hasPrivilege(confirmBookingRequest.getGdsCredentialsDTO().getPrivileges(),
											PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_ONACCOUNT_PAYMNETS))) {
								iPayment.addAgentCreditPayment(confirmBookingRequest.getGdsCredentialsDTO().getAgentCode(), pax.getTotalAvailableBalance(), null, null, null, null, null, null, null);
								iPassenger.addPassengerPayments(pax.getPnrPaxId(), iPayment);

							} else if (CASH_PAYMENT.equals(paymentType) && (confirmBookingRequest.getGdsCredentialsDTO() != null && AuthorizationUtil
									.hasPrivilege(confirmBookingRequest.getGdsCredentialsDTO().getPrivileges(),
											PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_CASH_PAYMNETS))) {
								iPayment.addCashPayment(pax.getTotalAvailableBalance(), confirmBookingRequest.getGdsCredentialsDTO().getAgentCode(),
										null, null, null, null, null);
								iPassenger.addPassengerPayments(pax.getPnrPaxId(), iPayment);
							} else {
								confirmBookingRequest.setSuccess(false);
								confirmBookingRequest
										.setResponseMessage(MessageUtil.getMessage("gdsservices.validators.payment.InsufficientPrivilege"));
							}
						}
					} else {
						confirmBookingRequest.setSuccess(false);
						confirmBookingRequest.setResponseMessage(MessageUtil.getMessage("gdsservices.validators.passengername.notfound"));
					}

				}
			}


			if(paymentType != null){

				reservationVersion = GDSServicesDAOUtils.DAOInstance.GDSSERVICES_SUPPORT_JDBC_DAO.getReservationVersion(reservation.getPnr());


				ServiceResponce serviceResponce = GDSServicesModuleUtil.getReservationBD().updateReservationForPayment(
						reservation.getPnr(), iPassenger, false, false, reservationVersion,
						CommonUtil.getTrackingInfo(confirmBookingRequest.getGdsCredentialsDTO()), false, false, true, false,
						true, false, null, false, null);

				if (serviceResponce != null && serviceResponce.isSuccess()) {
					confirmBookingRequest.setSuccess(true);
				} else {
					confirmBookingRequest.setSuccess(false);
					confirmBookingRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
				}
			} else {
				confirmBookingRequest.setSuccess(false);
				confirmBookingRequest.setResponseMessage(MessageUtil.getMessage("gdsservices.validators.payment.invalidConfig"));
			}


		} catch (ModuleException e) {
			log.error(" ConfirmBookingRequest Failed for GDS PNR "
					+ confirmBookingRequest.getOriginatorRecordLocator().getPnrReference(), e);

			confirmBookingRequest.setSuccess(false);
			confirmBookingRequest.setResponseMessage(e.getMessageString());
		} catch (Exception e) {
			log.error(" ConfirmBookingRequest Failed for GDS PNR "
					+ confirmBookingRequest.getOriginatorRecordLocator().getPnrReference(), e);
			confirmBookingRequest.setSuccess(false);
			confirmBookingRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
		}

		return confirmBookingRequest;
	}

	public static ReservationPax getReservationPax(Passenger passenger, Collection<ReservationPax> resPassengers) {
		ReservationPax reservationPax = null;

		for (ReservationPax pax : resPassengers) {

			if (passenger.getTitle().equals(pax.getTitle()) &&
					passenger.getFirstName().equals(pax.getFirstName()) &&
					passenger.getLastName().equals(pax.getLastName())) {
				reservationPax = pax;
				break;
			}

		}

		return reservationPax;
	}
}
