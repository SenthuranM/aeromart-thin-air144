package com.isa.thinair.gdsservices.core.typeb.extractors;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDetailDTO;
import com.isa.thinair.gdsservices.api.dto.internal.ConfirmBookingRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.internal.SSRData;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceDetailRequest;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.command.CommonUtil;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorBase;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

public class ConfirmBookingRequestExtractor extends ValidatorBase {

	@Override
	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {
		String pnrReference = ValidatorUtils.getPNRReference(bookingRequestDTO.getResponderRecordLocator());

		if (pnrReference.length() == 0) {
			String message = MessageUtil.getMessage("gdsservices.extractors.emptyPNRResponder");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		} else {
			boolean isETicketNumberExists = checkETicketNumberExists(bookingRequestDTO.getSsrDTOs());

			if (isETicketNumberExists) {
				this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
				return bookingRequestDTO;
			} else {
				String message = MessageUtil.getMessage("gdsservices.extractors.empty.eticket.number");
				bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

				this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
				return bookingRequestDTO;
			}
		}
	}

	private boolean checkETicketNumberExists(List<SSRDTO> ssrDTOs) {
		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

		for (SSRDTO ssrdto : ssrDTOs) {
			if (carrierCode.equals(ssrdto.getCarrierCode())
					&& GDSExternalCodes.SSRCodes.ETICKET_NO.getCode().equals(ssrdto.getCodeSSR())) {
				String eTicketNo = GDSApiUtils.maskNull(ssrdto.getSsrValue());

				if (eTicketNo.length() > 0) {
					return true;
				}
			}
		}

		return false;
	}

	public static ConfirmBookingRequest getRequest(GDSCredentialsDTO gdsCredentialsDTO, BookingRequestDTO bookingRequestDTO) {

		SSRData ssrData = new SSRData(bookingRequestDTO.getSsrDTOs());
		ConfirmBookingRequest confirmBookingRequest = new ConfirmBookingRequest();

		confirmBookingRequest.setGdsCredentialsDTO(gdsCredentialsDTO);
		confirmBookingRequest = (ConfirmBookingRequest) RequestExtractorUtil.addBaseAttributes(confirmBookingRequest,
				bookingRequestDTO);

		addPassengersAndSSRs(confirmBookingRequest, ssrData, bookingRequestDTO, gdsCredentialsDTO);

		return (ConfirmBookingRequest) confirmBookingRequest;
	}


	private static ConfirmBookingRequest addPassengersAndSSRs(ConfirmBookingRequest issueEticketRequest, SSRData ssrData, BookingRequestDTO bookingRequestDTO,
			GDSCredentialsDTO gdsCredentialsDTO) {
		try {
			String extPnrReference = ValidatorUtils.getPNRReference(bookingRequestDTO.getOriginatorRecordLocator());

			Reservation reservation = CommonUtil.loadReservation(gdsCredentialsDTO, extPnrReference, null);

			Map<String, Passenger> resPaxMap = new HashMap<>();
			Collection<SSRDetailDTO> ssrDetailDTOs = ssrData.getDetailSSRDTOs();

			// add SSRDetailDTOs to paxs
			for (Iterator<SSRDetailDTO> iterator = ssrDetailDTOs.iterator(); iterator.hasNext();) {
				SSRDetailDTO ssrDetailDTO = (SSRDetailDTO) iterator.next();

				if (GDSExternalCodes.SSRCodes.ETICKET_NO.getCode().equals(ssrDetailDTO.getCodeSSR())) {
					List<NameDTO> nameDTOs = ssrDetailDTO.getNameDTOs();
					String gdsActionCode = ssrDetailDTO.getActionOrStatusCode();

					if (gdsActionCode.equals(GDSExternalCodes.ActionCode.NEED.getCode())
							|| gdsActionCode.equals(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode())
							|| gdsActionCode.equals(GDSExternalCodes.StatusCode.CODESHARE_CONFIRMED.getCode())) {

						if (nameDTOs != null && nameDTOs.size() > 0) {
							for (Iterator<NameDTO> iterNameDTO = nameDTOs.iterator(); iterNameDTO.hasNext();) {
								NameDTO nameDTO = (NameDTO) iterNameDTO.next();
								SpecialServiceDetailRequest ssr = new SpecialServiceDetailRequest();
								ssr = SSRExtractorUtil.addSSRDetailAttributes(ssr, ssrDetailDTO);
								String iataPaxName = nameDTO.getIATAName();
								if (ssr.getComment() != null && ssr.getComment().startsWith("INF")) {
									ssr.setComment(ssr.getComment().substring(3));
									boolean infantFound = false;
									for (ReservationPax reservationPax : reservation.getPassengers()) {
										String iataResPaxName = GDSApiUtils.getIATAName(reservationPax.getLastName(),
												reservationPax.getFirstName(), reservationPax.getTitle());
										if (iataPaxName.equals(iataResPaxName)) {
											for (ReservationPax infant : reservationPax.getInfants()) {
												iataPaxName = GDSApiUtils.getIATAName(infant.getLastName(),
														infant.getFirstName(), infant.getTitle());
												nameDTO.setFirstName(infant.getFirstName());
												nameDTO.setLastName(infant.getLastName());
												nameDTO.setPaxTitle(infant.getTitle());
												nameDTO.setFamilyID(infant.getFamilyID());
												infantFound = true;
												break;
											}
										}
										if (infantFound) {
											break;
										}
									}
								}

								Passenger resPax = resPaxMap.get(iataPaxName);

								if (resPax == null) {
									resPax = new Passenger();
									resPax = RequestExtractorUtil.getPassenger(resPax, nameDTO);
									resPaxMap.put(iataPaxName, resPax);
								}

								resPax.getSsrCollection().add(ssr);
								issueEticketRequest.getPassengers().add(resPax);
							}
						}
					}
				}
			}
		} catch (ModuleException e) {
			e.printStackTrace();
		}
		return issueEticketRequest;
	}
}
