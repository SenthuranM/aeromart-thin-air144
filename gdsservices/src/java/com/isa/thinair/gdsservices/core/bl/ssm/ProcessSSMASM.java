package com.isa.thinair.gdsservices.core.bl.ssm;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMMessegeDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMResponse;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.MessageType;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.command.ProcessASMAction;
import com.isa.thinair.gdsservices.core.command.ProcessSSMAction;

public class ProcessSSMASM {

	private final Log log = LogFactory.getLog(getClass());

	public ProcessSSMASM() {

	}

	private boolean isEnableAttachDefaultAnciTemplate;

	public SSMASMResponse processSSMASMRequests(GDSCredentialsDTO gdsCredentialsDTO, SSMASMMessegeDTO ssIMessegeDTO)
			throws ModuleException {
		SSMASMResponse ssiMResponse = new SSMASMResponse();
		if (ssIMessegeDTO != null) {
			setEnableAttachDefaultAnciTemplate(ssIMessegeDTO.getSenderAddress(), ssIMessegeDTO.getMessageType().toString());

			if (MessageType.SSM.equals(ssIMessegeDTO.getMessageType())) {

				ProcessSSMAction processSSMAction = new ProcessSSMAction(ssIMessegeDTO, isEnableAttachDefaultAnciTemplate);
				ssiMResponse = processSSMAction.processSSMMessageAndUpdteSchedules();

			} else if (MessageType.ASM.equals(ssIMessegeDTO.getMessageType())) {

				ProcessASMAction processASMAction = new ProcessASMAction(ssIMessegeDTO, isEnableAttachDefaultAnciTemplate);
				ssiMResponse = processASMAction.processASMMessageAndUpdteSchedules();

			}
			ssiMResponse.setSsiMessegeDTO(ssIMessegeDTO);
		}
		return ssiMResponse;

	}

	public void setEnableAttachDefaultAnciTemplate(String tty, String requestType) throws ModuleException {
		this.isEnableAttachDefaultAnciTemplate = GDSServicesModuleUtil.getSSMASMServiceBD().isEnableAttachingDefaultAnciTemplate(
				tty, requestType);
	}

}
