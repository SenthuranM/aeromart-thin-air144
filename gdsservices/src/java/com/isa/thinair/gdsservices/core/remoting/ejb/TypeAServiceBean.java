package com.isa.thinair.gdsservices.core.remoting.ejb;

import java.util.Date;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.config.GDSServicesModuleConfig;
import com.isa.thinair.msgbroker.api.model.ServiceClient;
import com.isa.thinair.msgbroker.api.service.MessageBrokerServiceBD;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.gdsservices.api.service.GDSServicesBD;
import com.isa.thinair.gdsservices.core.service.bd.TypeAServiceBeanLocal;
import com.isa.thinair.gdsservices.core.service.bd.TypeAServiceBeanRemote;
import com.isa.typea.api.EDIMessage;
import com.isa.typea.api.Msg;
import com.isa.typea.exception.TypeAException;
import com.isa.typea.hth.cliconapi.ClientConnector;

@Stateless
@RemoteBinding(jndiBinding = "TypeAService.remote")
@LocalBinding(jndiBinding = "TypeAService.local")
// @RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
// @SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class TypeAServiceBean extends PlatformBaseSessionBean implements TypeAServiceBeanLocal, TypeAServiceBeanRemote {
	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(TypeAServiceBean.class);

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String sendMessageToGDS(String gdsCode, String message) throws ModuleException {

		String resp = null;
		GDSServicesModuleConfig gdsServicesModuleConfig = GDSServicesModuleUtil.getConfig();

		ClientConnector clientConnector = new ClientConnector();
		resp = clientConnector.getEDIResponse(message, false, gdsServicesModuleConfig.getMatipClientHost(),
				gdsServicesModuleConfig.getMatipClientPort());
		/*StringBuilder sb = new StringBuilder();
		

		sb.append("UNB+IATA:1+W5ET+1HET+170601:1606+1670001+76P012JWDS++'");
		sb.append("UNH+1+TKCRES:03:1:IA+76P012JWDS/222'");
		sb.append("MSG+:131+3'");
		sb.append("ORG+1A:MOW+00118580:002VN+MOW++T+RU:RU+111+MOVMVN'");
		sb.append("TIF+PEOP:+MLER'");
		sb.append("RCI+1A:95KZIJ:1+W5:15ALCG:1'");
		sb.append("MON+B:0.23:EUR+E:19.00:RUB+T:20.00:RUB'");
		sb.append("FOP+CA:3'");
		sb.append("PTK+N::I++190517+++:RU'");
		sb.append("TXD++1:::ZZ'");
		sb.append("IFT+4:15:1+MOW W5 THR234.92NUC234.92END ROE0.944989'");
		sb.append("IFT+4:10+NDSA/C0.00 NDSZZ28.22'");
		sb.append("IFT+4:13+FOID PP453678956'");
		sb.append("TKT+776090000151:T:1'");
		sb.append("CPN+1:O'");
		sb.append("TVL+110617:1200+DXB+IKA+W5+060:B+ET'");
		sb.append("PTS++F06'");
		sb.append("UNT+20+1'");
		sb.append("UNZ+1+1670001'");
		*/
	/*	resp = sb.toString();*/

		return resp;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public String sendMessage(String message) throws ModuleException {
		GDSServicesBD gdsServicesBD = GDSServicesModuleUtil.getGDSServicesBD(true);
		return gdsServicesBD.processEdifactMesssage(message, false);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ServiceClient getGdsServiceClient(Integer gdsId) throws ModuleException {

		GDSExternalCodes.GDS gds = GDSApiUtils.getGDSCode(gdsId);
		MessageBrokerServiceBD messageBrokerServiceBD = GDSServicesModuleUtil.getMessageBrokerServiceBD();
		ServiceClient serviceClient = messageBrokerServiceBD.getServiceClient(gds.getCode());
		return serviceClient;

	}

	@Override
	public String onMessage(String message) throws TypeAException {
		try { log.info("\n ********Received edi message: \n" + message);
			return sendMessage(message);
		} catch (ModuleException e) {
			log.error(e);
			throw new TypeAException("Message Cannot be processed");
		}
	}

	@Override
	public EDIMessage onMessage(Msg message) throws TypeAException {
		try { log.info("\n ********Received edi message: \n" + message.getMessage());
			String response = sendMessage(message.getMessage());
			return EDIMessage.getEDIMessage(response,false);
		} catch (ModuleException e) {
			log.error(e);
			throw new TypeAException("Message Cannot be processed");
		}
	}

	
}
