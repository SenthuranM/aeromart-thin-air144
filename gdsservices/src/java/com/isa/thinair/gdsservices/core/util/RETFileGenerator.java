package com.isa.thinair.gdsservices.core.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * Prepares content for a sample RET file
 * 
 */
public class RETFileGenerator {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Date processingDate = new Date();
		SimpleDateFormat dateSDF = new SimpleDateFormat("yyMMdd");
		SimpleDateFormat timeSDF = new SimpleDateFormat("HHmm");

		String[] dataNew =
		// {
		// "1", // 0 - TransactionNumber
		// "XXXXXXXX", // 1- AgentNumericCode
		// "091201", // 2 - DateofIssue YYMMDD
		// "5372100000001", // 3 - Ticket/DocumentNumber
		// "MARTIN/MARINA", // 4 - PassengerName
		//
		// "10000001", // 5 - PNRReference
		// "USD", // 6 - Currency Code
		// "70.00", // 7 - Fare
		// "9.90", // 8 - Tax
		// "79.90", // 9 - Total
		//
		// "990", // 10 - Tax
		// "7990", // 11 - Total
		// "USD", // 12 - CurrencyType
		//
		// "IKA", // 13 - OriginAirport/CityCode
		// "TEH", // 14 - DestinationAirport/CityCode
		// "Y", // 15 - ReservationBookingDesignator
		// "15DEC", // 16 - FlightDate
		// "YOW", // 17 - FareBasis/TicketDesignator
		// "530", // 18 - FlightNumber
		// "0930", // 19 - FlightDepartureTime
		//
		// "CA", // 20 - FormofPaymentType
		// "CASH" // 21 - FormofPaymentInformation
		// };
		{ "353", "BSP001", "091201", "5371000000135", "NASLY/YOOSUF", "10000089", "IRR", "180000.00", "70000.00", "400000.00",
				"7000000", "40000000", "IRR", "IKA", "DEL", "Y", "23DEC", "YOW", "5071", "2015", "CA", "CASH" };

		// String [][] data = {
		// {},
		// {"1", // TransactionNumber
		// "XXXXXXXX", // AgentNumericCode
		// "091201", // DateofIssue YYMMDD
		// "5372100000001", // Ticket/DocumentNumber
		// "MARTIN/MARINA" // PassengerName
		// },
		// {"10000001", // PNRReference
		// "USD", // Currency Code
		// "70.00", // Fare
		// "9.90", // Tax
		// "79.90" // Total
		// },
		// {"990", // Tax
		// "7990", // Total
		// "USD" // CurrencyType
		// },
		// {"IKA", // OriginAirport/CityCode
		// "TEH", // DestinationAirport/CityCode
		// "Y", // ReservationBookingDesignator
		// "15DEC", // FlightDate
		// "YOW", // FareBasis/TicketDesignator
		// "530", // FlightNumber
		// "0930" // FlightDepartureTime
		// },
		// {},
		// {"CA" // FormofPaymentType
		// },
		// {"CASH" // FormofPaymentInformation
		// }};

		// IT01 File Header Record Layout
		String line1 = "1" // RecordIdentifier
				+ "091231" // FIXME SystemProviderReportingPeriodEndingDate
				+ "WEBL" // ReportingSystemIdentifier
				+ "201" // HandbookRevisionNumber
				+ "TEST" // Test/ProductionStatus
				+ dateSDF.format(processingDate) // ProcessingDate YYMMDD
				+ timeSDF.format(processingDate) // ProcessingTime HH24MI
				+ "IR" // ISOCountryCode
				+ "      " // ReservedSpace
				+ " " // FileType
				+ "  " // FileTypeSequenceNumber
				+ "                                                                          "
				+ "                                                                        "
				+ "                                                                      "; // ReservedSpace

		// IT02 Basic Sale Transaction Records
		String line2 = "2" // RecordIdentifier
				+ getPaddedString(dataNew[0], "0", 6, false) // TransactionNumber
				+ getPaddedString(dataNew[1], " ", 8, true) // AgentNumericCode
				+ getPaddedString("", " ", 3, true) // ConjunctionTicketIndicator
				+ "FVVV" // CouponUseIndicator
				+ dataNew[2] // DateofIssue YYMMDD
				+ "D  " // StatisticalCode
				+ getPaddedString(dataNew[3], " ", 15, true) // Ticket/DocumentNumber
				+ "1" // CheckDigit
				+ "TKTT" // TransactionCode
				+ "               " // TransmissionControlNumber
				+ "0" // TransmissionControlNumberCheckDigit
				+ "537  " // TicketingAirlineCodeNumber
				+ "1" // CheckDigit
				+ "X" // FormatIdentifier
				+ getPaddedString(dataNew[4], " ", 49, true) // PassengerName
				+ "A" // ApprovedLocationType
				+ "                " // StockControlNumberFrom
				+ "0000" // StockControlNumberTo
				+ "00000000" // ApprovedLocationNumericCode
				+ " " // ApprovedLocationType
				+ "                " // StockControlNumberFrom
				+ "0000" // StockControlNumberTo
				+ "00000000" // ApprovedLocationNumericCode
				+ " " // ApprovedLocationType
				+ "                " // StockControlNumberFrom
				+ "0000" // StockControlNumberTo
				+ "00000000" // ApprovedLocationNumericCode
				+ " " // ApprovedLocationType
				+ "                " // StockControlNumberFrom
				+ "0000" // "StockControlNumberTo"
				+ "              " // SettlementAuthorisationCode
				+ " " // DataInputStatusIndicator
				+ "IR" // ISOCountryCode
				+ "  " // VendorISOCountryCode
				+ "    " // VendorIdentification
				+ " "; // ReservedSpace

		// IT03 Related Ticket/Document Information Records

		// IT04 Additional Sale Information Records
		String line3 = "4" // RecordIdentifier
				+ getPaddedString(dataNew[0], "0", 6, false) // TransactionNumber
				+ getPaddedString(dataNew[5], " ", 9, true) + "/W5 " // PNRReferenceand/orAirlineData
				+ "              " // TrueOrigin/DestinationCityCodes
				+ "    " // ReservedSpace
				+ "/" // TicketingModeIndicator
				+ "                                " // OriginalIssueInformation
				+ "               " // TourCode
				+ dataNew[6] + getPaddedString(dataNew[7], " ", 8, false) // Fare
				+ "   00000000" // EquivalentFarePaid
				+ "XT" + getPaddedString(dataNew[8], " ", 9, false) // Tax
				+ "           " // Tax
				+ "           " // Tax
				+ dataNew[6] + getPaddedString(dataNew[9], " ", 8, false) // Total
				+ "NAME" // NeutralTicketingSystemIdentifier
				+ "9999" // ServicingAirline/SystemProviderIdentifier
				+ "        " // ClientIdentification
				+ "      " // BookingAgentIdentification
				+ "                                                 " // PassengerSpecificData
				+ "99999999" // ValidatingLocationNumericCode
				+ "          " // BookingAgency/LocationNumber
				+ "" // BookingEntityOutletType
				+ "A" // BookingEntityOutletType
				+ "             "; // ReservedSpace

		// IT05 Monetary Amounts Records
		String line4 = "5" // RecordIdentifier
				+ getPaddedString(dataNew[0], "0", 6, false) // TransactionNumber
				+ "00000000000" // AmountEnteredbyAgent
				+ "           " // ReservedSpace
				+ "XT      " // Tax/MiscellaneousFeeType
				+ getPaddedString(dataNew[10], "0", 11, false) // Tax/MiscellaneousFeeAmount
				+ "        " // Tax/MiscellaneousFeeType
				+ "00000000000" // Tax/MiscellaneousFeeAmount
				+ "        " // Tax/MiscellaneousFeeType
				+ "00000000000" // Tax/MiscellaneousFeeAmount
				+ getPaddedString(dataNew[11], "0", 11, false) // Ticket/DocumentAmount
				+ dataNew[12] + "2" // CurrencyType
				+ "00000000000" // TaxonCommissionAmount
				+ " " // ReservedSpace
				+ "        " // Tax/MiscellaneousFeeType
				+ "00000000000" // Tax/MiscellaneousFeeAmount
				+ "        " // Tax/MiscellaneousFeeType
				+ "00000000000" // Tax/MiscellaneouseeAmount
				+ "        " // Tax/MiscellaneousFeeType
				+ "00000000000" // Tax/MiscellaneousFeeAmount
				+ "      " // CommissionType
				+ "00000" // CommissionRate
				+ "00000000000" // CommissionAmount
				+ "      " // CommissionType
				+ "00000" // CommissionRate
				+ "00000000000" // CommissionAmount
				+ "      " // CommissionType
				+ "00000" // CommissionRate
				+ "00000000000" // CommissionAmount
				+ "  " // Net-ReportingIndicator
				+ "00000000000" // AmountPaidbyCustomer
				+ "      "; // TaxonCommissionType

		// IT06 Itinerary Records
		String line5 = "6" // RecordIdentifier
				+ getPaddedString(dataNew[0], "0", 6, false) // TransactionNumber
				+ getPaddedString(dataNew[13], " ", 5, true) // OriginAirport/CityCode
				+ getPaddedString(dataNew[14], " ", 5, true) // DestinationAirport/CityCode
				+ "                " // FrequentFlyerReference
				+ "W5  " // Carrier
				+ " " // ReservedSpace
				+ getPaddedString(dataNew[15], " ", 2, true) // ReservationBookingDesignator
				+ dataNew[16] // FlightDate
				+ "     " // NotValidBeforeDate
				+ dataNew[16] // NotValidAfterDate
				+ getPaddedString(dataNew[17], " ", 15, true) // FareBasis/TicketDesignator
				+ getPaddedString(dataNew[18], " ", 5, true) // FlightNumber
				+ getPaddedString(dataNew[19], " ", 5, true) // FlightDepartureTime
				+ "PC " // FreeBaggageAllowance
				+ "OK" // FlightBookingStatus
				+ "1" // SegmentIdentifier
				+ " " // Stopover Code
				+ getPaddedString("0 ", " ", 80, false) // Origin Airport/City Code - Stopover Code
				+ getPaddedString("0 ", " ", 80, false) // Origin Airport/City Code - Stopover Code
				+ "        "; // ReservedSpace

		// IT07 Fare Calculation Records
		String line6 = "7" // RecordIdentifier
				+ getPaddedString(dataNew[0], "0", 6, false) // TransactionNumber
				+ getPaddedString(dataNew[13] + " XB " + dataNew[14] + dataNew[7] + dataNew[6] + dataNew[7] + "END", " ", 87,
						true) // FareCalculationArea
				+ "1" // FareCalculationSequenceNumber
				+ "                                                                                       " // FareCalculationArea
				+ "0" // FareCalculationSequenceNumber
				+ "0" // FareCalculationModeIndicator
				+ "                                                                       "; // ReservedSpace

		// IT08 Form of Payment Records
		String line7 = "8" // RecordIdentifier
				+ getPaddedString(dataNew[0], "0", 6, false) // TransactionNumber
				+ "                   " // FormofPaymentAccountNumber
				+ getPaddedString(dataNew[11], "0", 11, false) // FormofPaymentAmount
				+ "      " // ApprovalCode
				+ dataNew[12]
				+ "2" // CurrencyType
				+ "  " // ExtendedPaymentCode
				+ getPaddedString(dataNew[20], " ", 10, true) // FormofPaymentType
				+ "    " // ExpiryDate
				+ "                           " // CustomerFileReference
				+ " " // CreditCardCorporateContract
				+ "  " // AddressVerificationCode
				+ " " // SourceofApprovalCode
				+ "                         " // FormofPaymentTransactionInformation
				+ "00000000000" // AuthorisedAmount
				+ "                   00000000000                                                                                  " // FormofPaymentAccountNumber
				+ "           " // AuthorisedAmount
				+ "  "; // ReservedSpace

		// IT09 Additional Information Records
		String line8 = "9" // RecordIdentifier
				+ getPaddedString(dataNew[0], "0", 6, false) // TransactionNumber
				+ "                                                                                                                                                   " // Endorsements/Restrictions
				+ getPaddedString(dataNew[21], " ", 50, true) // FormofPaymentInformation
				+ "                                                  " // FormofPaymentInformation
				+ " "; // ReservedSpace

		System.out.println(line1);
		System.out.println(line2);
		System.out.println(line3);
		System.out.println(line4);
		System.out.println(line5);
		System.out.println(line6);
		System.out.println(line7);
		System.out.println(line8);
	}

	private static String getPaddedString(String data, String paddingChar, int width, boolean isAlignLeft) {
		String paddedStr = data;
		if (data == null)
			paddedStr = "";

		if (data.length() > width) {
			paddedStr = data.substring(0, width);
		} else if (paddingChar.length() > 0 && data.length() != width) {
			StringBuilder sb = new StringBuilder(paddedStr);
			if (isAlignLeft) {
				while (sb.length() < width) {
					sb.append(paddingChar);
				}
			} else {
				while (sb.length() < width) {
					sb.insert(0, paddingChar);
				}
			}
			paddedStr = sb.toString();
		}

		return paddedStr;
	}

}
