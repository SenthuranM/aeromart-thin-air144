package com.isa.thinair.gdsservices.core.command;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.ReservationStatus;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.decorator.ReservationMediator;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.gdsservices.api.dto.internal.AddInfantRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.dto.internal.Infant;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.internal.SSRTO;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.platform.api.ServiceResponce;

public class AddInfantAction {
	private static Log log = LogFactory.getLog(MakePaymentAction.class);

	private static GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

	private static String NEXT_SEQ = "NEXTSEQ";

	public static AddInfantRequest processRequest(AddInfantRequest addInfantRequest) {
		try {

			// 1. Retrieve Reservation ---
			Reservation reservation = CommonUtil.loadReservation(addInfantRequest.getGdsCredentialsDTO(), addInfantRequest
					.getOriginatorRecordLocator().getPnrReference(), null);

			CommonUtil.validateReservation(addInfantRequest.getGdsCredentialsDTO(), reservation);
			// Need to allow modifications to confirmed reservations as well. Balance payment will happen through
			// SSR TKNE. So comment the validation
			// CommonUtil.validatePaymentOptions(reservation, addInfantRequest.getPaymentDetail());
			ReservationMediator reservationMediator = new ReservationMediator(reservation);
			Collection<TempSegBcAlloc> blockIds = GDSServicesModuleUtil.getReservationBD().blockSeats(
					reservationMediator.getOndFareDTOs(addInfantRequest.getInfantCount()), null);
			// FIXME: MULTIPLE INFANT SUPPORT ???
			Collection<OndFareDTO> inventoryFares = getInfantFareQuote(addInfantRequest, reservation);

			IPassenger iPassenger = new PassengerAssembler(inventoryFares);

			Map<String, Object> adultMap = getPaxMap(reservation.getPassengers());

			Collection<Infant> infants = addInfantRequest.getInfants();
			String key = null;
			Passenger parent = null;
			ReservationPax resParent = null;
			IPayment iPayment = null;
			int nextPaxSequence = (Integer) adultMap.get(NEXT_SEQ);

			for (Infant infant : infants) {
				parent = infant.getParent();
				if (parent == null) {
					throw new ModuleException("gdsservices.actions.parentDetailsDoesNotExist");
				}

				key = GDSApiUtils.getIATAName(parent.getLastName(), parent.getFirstName(), parent.getTitle());
				resParent = (ReservationPax) adultMap.get(key);

				if (resParent.getInfants() != null && !resParent.getInfants().isEmpty()) {
					throw new ModuleException("gdsservices.actions.adult.more.infants");
				}

				// Add Infant
				List<SSRTO> processedSSRs = CommonUtil.getSSRInfo(infant.getSsrCollection(), null, null);
				SegmentSSRAssembler segmentSSRs = new SegmentSSRAssembler();
				String firstSsrCode = null;
				if (processedSSRs != null && processedSSRs.size() > 0) {
					firstSsrCode = processedSSRs.get(0).getFirstSSRCode();
					if (firstSsrCode != null) {
						Integer ssrId = SSRUtil.getSSRId(firstSsrCode);						
						if (ssrId != null) {
							segmentSSRs.addPaxSegmentSSR(null, ssrId, firstSsrCode);
						}
					}
				}
				iPassenger.addInfant(infant.getFirstName(), infant.getLastName(), infant.getTitle(), infant.getDateOfBirth(),
						CommonUtil.getNationalityCode(infant.getNationalityCode()), nextPaxSequence, resParent.getPaxSequence(),
						null, null, null, null, null, null, null, null, segmentSSRs, null, null, "", "", "", "", null);

				nextPaxSequence++;

				// Infant Payment
				iPayment = new PaymentAssembler();
				iPassenger.addPassengerPayments(resParent.getPnrPaxId(), iPayment);
			}

			Integer intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS;
			TrackInfoDTO trackInfoDTO = getTrackingInfo(addInfantRequest.getGdsCredentialsDTO());
			trackInfoDTO.setPaymentAgent(addInfantRequest.getGdsCredentialsDTO().getAgentCode());
			ServiceResponce serviceResponce = GDSServicesModuleUtil.getPassengerBD().addInfant(reservation.getPnr(), iPassenger,
					blockIds, intPaymentMode, reservation.getVersion(), null, trackInfoDTO, false, false, false, null);

			if (serviceResponce != null && serviceResponce.isSuccess()) {
				addInfantRequest.setSuccess(true);
				Reservation modifiedReservation = CommonUtil.loadReservation(addInfantRequest.getGdsCredentialsDTO(),
						addInfantRequest.getOriginatorRecordLocator().getPnrReference(), null);
				if (modifiedReservation.getStatus().equals(ReservationStatus.OHD.toString())) {
					String message = "";
					Date releaseTimeStamp = modifiedReservation.getReleaseTimeStamps()[0];
					if (GDSServicesModuleUtil.getConfig().isShowPaymentDetailsInTTYMessage()) {
						message = CommonUtil.getFormattedMessage(GDSInternalCodes.ResponseMessageCode.CONFIRMED_ON_HOLD,
								modifiedReservation.getLastCurrencyCode(), modifiedReservation.getTotalAvailableBalance(),
								releaseTimeStamp);
					} else {
						message = CommonUtil.getFormattedMessage(
								GDSInternalCodes.ResponseMessageCode.CONFIRMED_ON_HOLD_NO_PAYMENT_DETAILS, releaseTimeStamp);
					}
					if (addInfantRequest.isNotSupportedSSRExists()) {
						message = message
								+ MessageUtil.getMessage(GDSInternalCodes.ResponseMessageCode.SSR_NOT_SUPPORTED.getCode());
					}
					addInfantRequest.setResponseMessage(message);
				}
			} else {
				addInfantRequest.setSuccess(false);
				addInfantRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
				addInfantRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
			}

		} catch (ModuleException e) {
			log.error(" AddInfantAction Failed for GDS PNR " + addInfantRequest.getOriginatorRecordLocator().getPnrReference(), e);
			addInfantRequest.setSuccess(false);
			addInfantRequest.setResponseMessage(e.getMessageString());
			addInfantRequest.addErrorCode(e.getExceptionCode());
		} catch (Exception e) {
			log.error(" AddInfantAction Failed for GDS PNR " + addInfantRequest.getOriginatorRecordLocator().getPnrReference(), e);
			addInfantRequest.setSuccess(false);
			addInfantRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
			addInfantRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
		}

		return addInfantRequest;
	}

	private static Map<String, Object> getPaxMap(Set<ReservationPax> passengers) {
		Map<String, Object> paxMap = new HashMap<String, Object>();

		int lastSequence = 0;

		for (ReservationPax pax : passengers) {
			if (pax.getPaxType().equals(PaxTypeTO.ADULT)) {
				String key = GDSApiUtils.getIATAName(pax.getLastName(), pax.getFirstName(), pax.getTitle());
				paxMap.put(key, pax);
			}

			if (lastSequence < pax.getPaxSequence()) {
				lastSequence = pax.getPaxSequence();
			}
		}

		paxMap.put(NEXT_SEQ, ++lastSequence);

		return paxMap;
	}

	private static Collection<OndFareDTO> getInfantFareQuote(AddInfantRequest request, Reservation reservation)
			throws ModuleException {
		String stationCode = getUsersStation(request.getGdsCredentialsDTO());
		String agentCode = getUserAgent(request.getGdsCredentialsDTO());

		ReservationMediator rm = new ReservationMediator(reservation);
		Collection<OndFareDTO> ondFareDTOs = rm.getOndFareDTOs(request.getInfantCount());

		boolean privilegeExist = request.getGdsCredentialsDTO().hasPrivilege(PriviledgeConstants.PRIVI_ADD_INFANT);
		return GDSServicesModuleUtil.getFlightInventoryResBD()
				.getInfantQuote(ondFareDTOs, stationCode, privilegeExist, agentCode);
	}

	private static String getUsersStation(GDSCredentialsDTO credentialsDTO) throws ModuleException {
		if (credentialsDTO.getAgentStation() != null) {
			return credentialsDTO.getAgentStation();
		} else {
			return globalConfig.getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM);
		}
	}

	private static String getUserAgent(GDSCredentialsDTO credentialsDTO) throws ModuleException {
		if (credentialsDTO.getAgentCode() != null) {
			return credentialsDTO.getAgentCode();
		}
		return null;
	}

	private static TrackInfoDTO getTrackingInfo(GDSCredentialsDTO credentialsDTO) {

		TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
		trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_GDS);
		trackInfoDTO.setOriginUserId(credentialsDTO.getUserId());
		trackInfoDTO.setOriginAgentCode(credentialsDTO.getAgentCode());
		trackInfoDTO.setOriginChannelId(credentialsDTO.getSalesChannelCode());
		return trackInfoDTO;

	}
}
