package com.isa.thinair.gdsservices.core.typeb.transformers;

import java.util.Map;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

public class LocateReservationResponseTransformer extends CreateResponseTransformer {
	private static final ReservationAction RESERVATION_ACTION = ReservationAction.LOCATE_RESERVATION;

	/**
	 * Transforms reservationResponseMap back to bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param reservationResponseMap
	 *            reservationResponseMap to a BookingRequestDTO
	 * @return
	 */
	public static BookingRequestDTO getResponse(BookingRequestDTO bookingRequestDTO,
			Map<ReservationAction, GDSReservationRequestBase> reservationResponseMap) {
		bookingRequestDTO = CreateResponseTransformer.getResponse(bookingRequestDTO, reservationResponseMap, RESERVATION_ACTION);

		return bookingRequestDTO;
	}
}
