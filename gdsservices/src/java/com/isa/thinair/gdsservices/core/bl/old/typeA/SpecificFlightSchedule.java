package com.isa.thinair.gdsservices.core.bl.old.typeA;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegWithCCDTO;
import com.isa.thinair.airschedules.api.criteria.FlightSearchCriteria;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.api.dto.typea.SpecificFlightDetailRequestDTO;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.api.util.TypeAResponseCode;
import com.isa.thinair.gdsservices.core.common.GDSTypeATransactionStatusUtils;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @isa.module.command name="specificFlightSchedule.old"
 * @author mekanayake
 * 
 */
public class SpecificFlightSchedule extends DefaultBaseCommand {

	@SuppressWarnings("unchecked")
	@Override
	public ServiceResponce execute() throws ModuleException {

		List<SpecificFlightDetailRequestDTO> flightDetailRequestDTOs = (List<SpecificFlightDetailRequestDTO>) this
				.getParameter(TypeACommandParamNames.SPECIFIC_FLIGHT_DETAIL_LIST);
		List<FlightSegWithCCDTO> flightSegmentWithCabinClassDTOs = new ArrayList<FlightSegWithCCDTO>();

		String gdsTnxRef = (String) this.getParameter(TypeACommandParamNames.COMMON_ACCESS_REFERENCE);
		GDSTypeATransactionStatusUtils.createOrUpdateTransactionStage(gdsTnxRef, IATAOperation.PAOREQ.name());

		for (SpecificFlightDetailRequestDTO detailRequestDTO : flightDetailRequestDTOs) {
			FlightSearchCriteria searchCriteria = new FlightSearchCriteria();
			searchCriteria.setFlightNumber(detailRequestDTO.getFlightNumber());
			searchCriteria.setEstDepartureDate(detailRequestDTO.getSecificDate());
			Flight flight = GDSServicesModuleUtil.getFlightBD().getSpecificFlightDetail(searchCriteria);
			if (flight != null) {

				Set<FlightLeg> legs = flight.getFlightLegs();
				Set<String> segCodes = new HashSet<String>();
				for (FlightLeg leg : legs) {
					segCodes.add(leg.getModelRouteId());
				}

				Set<FlightSegement> segements = flight.getFlightSegements();
				for (FlightSegement seg : segements) {
					if (segCodes.contains(seg.getSegmentCode()) && !seg.isOverlapping()) { // non-overlapping direct
																							// segment
						FlightSegWithCCDTO segmentWithCabinClassDTO = GDSServicesModuleUtil.getFlightInventoryBD()
								.getFlightSegmentsWithCabinClass(seg.getFltSegId());
						flightSegmentWithCabinClassDTOs.add(segmentWithCabinClassDTO);
					}
				}
			}
		}

		// constructing and return command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(TypeAResponseCode.SPECIFICFLIGHT_SCHEDULE_SUCCSESSFULL, flightSegmentWithCabinClassDTOs);

		return response;
	}

}
