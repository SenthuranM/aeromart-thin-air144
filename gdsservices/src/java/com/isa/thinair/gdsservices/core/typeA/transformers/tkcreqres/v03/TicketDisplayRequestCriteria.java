package com.isa.thinair.gdsservices.core.typeA.transformers.tkcreqres.v03;

/**
 * 
 * @author sanjaya
 * 
 */
public enum TicketDisplayRequestCriteria {

	/**
	 * Search By E-ticket number
	 */
	BY_ETICKET,

	/**
	 * Search By Airline confirmation number.
	 */
	BY_AIRLINE_CONFIRMATION_NUMBER,

	/**
	 * Search By Date of Flight, Origin Airport, Destination Airport, Passenger Name.
	 */
	BY_OND_DOF_PAX_NAME,

	/**
	 * Search By Airline Designator (Marketing Carrier)/Flight Number/Date of Flight/Origin Airport/Destination
	 * Airport/Passenger Name/
	 * 
	 */
	BY_MC_FLIGHT_NUMBER_DOF_OND_PAX_NAME,

	/**
	 * Search By Frequent Flyer Reference/ Date of Flight
	 */
	BY_FFR_DOF,

	/**
	 * Search By Passenger Telephone Number/ Passenger Name/ Date of Flight
	 */
	BY_PAX_PHONE_PAX_NAME_DOF,

	/**
	 * Search By Credit Card Number/ Passenger Name/ Date of Flight
	 */
	BY_CC_NUMBER_PAX_NAME_DOF

}
