package com.isa.thinair.gdsservices.core.dto;

import com.isa.thinair.gdsservices.api.model.IATAOperation;

public class EDIMessageMetaDataDTO {
	private IATAOperation iataOperation;
	private String majorVersion;
	private String minorVersion;
	private String messageReference;

	public IATAOperation getIataOperation() {
		return iataOperation;
	}

	public void setIataOperation(IATAOperation iataOperation) {
		this.iataOperation = iataOperation;
	}

	public String getMajorVersion() {
		return majorVersion;
	}

	public void setMajorVersion(String majorVersion) {
		this.majorVersion = majorVersion;
	}

	public String getMinorVersion() {
		return minorVersion;
	}

	public void setMinorVersion(String minorVersion) {
		this.minorVersion = minorVersion;
	}

	public String getFullyQualifiedVersion() {
		return majorVersion + "." + minorVersion;
	}

	public String getMessageReference() {
		return messageReference;
	}

	public void setMessageReference(String messageReference) {
		this.messageReference = messageReference;
	}
}
