package com.isa.thinair.gdsservices.core.bl.old.typeA;

import java.util.List;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.gdsservices.api.dto.typea.ERCDetail;
import com.isa.thinair.gdsservices.api.dto.typea.TVLDto;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @isa.module.command name="dailyFlightAvailability.old"
 * @author mekanayake
 * 
 */
public class DailyFlightAvailability extends DefaultBaseCommand {

	@SuppressWarnings("unchecked")
	@Override
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		List<TVLDto> tvlDtos = (List) this.getParameter(TypeACommandParamNames.TVL_DTOS);
		Set<ERCDetail> errorDetails = (Set) this.getParameter(TypeACommandParamNames.APPLICATION_ERROR);

		if (errorDetails == null || !errorDetails.isEmpty()) {
			for (TVLDto tvlDto : tvlDtos) {
				if (!tvlDto.isErrorSegment()) {
					AvailableFlightSearchDTO flightSearchDTO = tvlDto.getAvailableFlightSearchDTO();
					flightSearchDTO.setReturnFlag(false);
					flightSearchDTO.getCabinClassSelection().put("Y", null);
					flightSearchDTO.setChannelCode(SalesChannelsUtil
							.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_GDS_Key));
					// this will not change.
					flightSearchDTO.setBookingType(BookingClass.BookingClassType.NORMAL);
					flightSearchDTO.setAvailabilityRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED);
					AvailableFlightDTO availableFlightDTO = GDSServicesModuleUtil.getReservationQueryBD()
							.getAvailableFlightsWithAllFares(flightSearchDTO, null);
					tvlDto.setAvailabilityResult(availableFlightDTO);
				}
			}
		}
		// constructing and return command response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		return response;
	}
}
