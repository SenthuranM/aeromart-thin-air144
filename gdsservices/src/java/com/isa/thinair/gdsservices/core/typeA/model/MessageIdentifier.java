package com.isa.thinair.gdsservices.core.typeA.model;

import java.io.Serializable;

import com.isa.thinair.gdsservices.api.model.IATAOperation;

public class MessageIdentifier implements Serializable {

	private static final long serialVersionUID = -7361564587872639508L;

	private IATAOperation messageType;

	private int version;

	private int relNo;

	private String controllingAgency;

	public MessageIdentifier(String messageType, String version, String relNo, String controllingAgency) {
		this.messageType = IATAOperation.valueOf(messageType);
		this.version = Integer.valueOf(version);
		this.relNo = Integer.valueOf(relNo);
		this.controllingAgency = controllingAgency;
	}

	/**
	 * @return the messageType
	 */
	public IATAOperation getMessageType() {
		return messageType;
	}

	/**
	 * @param messageType
	 *            the messageType to set
	 */
	public void setMessageType(IATAOperation messageType) {
		this.messageType = messageType;
	}

	/**
	 * @return the version
	 */
	public int getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(int version) {
		this.version = version;
	}

	/**
	 * @return the relNo
	 */
	public int getRelNo() {
		return relNo;
	}

	/**
	 * @param relNo
	 *            the relNo to set
	 */
	public void setRelNo(int relNo) {
		this.relNo = relNo;
	}

	/**
	 * @return the controllingAgency
	 */
	public String getControllingAgency() {
		return controllingAgency;
	}

	/**
	 * @param controllingAgency
	 *            the controllingAgency to set
	 */
	public void setControllingAgency(String controllingAgency) {
		this.controllingAgency = controllingAgency;
	}

}
