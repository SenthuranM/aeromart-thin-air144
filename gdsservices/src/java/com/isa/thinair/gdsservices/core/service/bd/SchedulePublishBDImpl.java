/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.gdsservices.api.service.SchedulePublishBD;

/**
 * Implementation of all the methods required to access the GDS Schedule Publish Services <strong> Remote Access
 * </strong>
 * 
 * @author M.Rikaz
 * @since 6.0
 */
@Remote
public interface SchedulePublishBDImpl extends SchedulePublishBD {

}
