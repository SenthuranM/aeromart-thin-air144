package com.isa.thinair.gdsservices.core.typeb.validators;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.ChangedNamesDTO;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRInfantDTO;
import com.isa.thinair.gdsservices.api.dto.external.UnchangedNamesDTO;
import com.isa.thinair.gdsservices.api.dto.internal.GDSCredentialsDTO;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

/**
 * @author Nilindra Fernando
 */
public class ValidateInfantParentMapping extends ValidatorBase {

	public BookingRequestDTO validate(BookingRequestDTO bookingRequestDTO, GDSCredentialsDTO gdsCredentialsDTO) {
		Collection<String> parentNamesFromInfantList = getParentNamesFromInfantList(bookingRequestDTO);
		Collection<String> passengerNames = getPassengerList(bookingRequestDTO);

		boolean isParentNamesAreBlank = checkIsPassengerNamesAreBlank(passengerNames, parentNamesFromInfantList);
		boolean isParentChildInValidMapping = checkIsParentInfantInValidMapping(passengerNames, parentNamesFromInfantList);

		if (isParentNamesAreBlank && passengerNames.size() != 1) {
			String message = MessageUtil.getMessage("gdsservices.validators.passengername.empty");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		}

		if (isParentChildInValidMapping && passengerNames.size() != 1) {
			String message = MessageUtil.getMessage("gdsservices.validators.invalid.infant.parent.mapping");
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, message);

			this.setStatus(GDSInternalCodes.ValidateConstants.FAILURE);
			return bookingRequestDTO;
		}

		this.setStatus(GDSInternalCodes.ValidateConstants.SUCCESS);
		return bookingRequestDTO;
	}

	private boolean
			checkIsParentInfantInValidMapping(Collection<String> parentNames, Collection<String> parentNamesFromInfantList) {

		for (String element : parentNamesFromInfantList) {
			if (!parentNames.contains(element)) {
				return true;
			}
		}

		return false;
	}

	private boolean checkIsPassengerNamesAreBlank(Collection<String>... parentNames) {
		Collection<String> names = new HashSet<String>();

		for (int i = 0; i < parentNames.length; i++) {
			names.addAll(parentNames[i]);
		}

		if (names.contains("")) {
			return true;
		} else {
			return false;
		}
	}

	private Collection<String> getPassengerList(BookingRequestDTO bookingRequestDTO) {
		Collection<String> colAdultNames = new HashSet<String>();
		List<NameDTO> newNames = bookingRequestDTO.getNewNameDTOs();
		List<ChangedNamesDTO> changedNames = bookingRequestDTO.getChangedNameDTOs();
		List<UnchangedNamesDTO> unChangedNames = bookingRequestDTO.getUnChangedNameDTOs();

		if (newNames != null && newNames.size() > 0) {
			for (NameDTO nameDTO : newNames) {
				colAdultNames.add(nameDTO.getIATAName());
			}
		} else if (changedNames != null && changedNames.size() > 0 && changedNames.get(0) != null
				&& changedNames.get(0).getOldNameDTOs() != null && changedNames.get(0).getOldNameDTOs().size() > 0) {
			List<NameDTO> oldNames = changedNames.get(0).getOldNameDTOs();
			for (NameDTO nameDTO : oldNames) {
				colAdultNames.add(nameDTO.getIATAName());
			}
		}
		if (unChangedNames != null && unChangedNames.size() > 0 && unChangedNames.get(0) != null
				&& unChangedNames.get(0).getUnChangedNames() != null && unChangedNames.get(0).getUnChangedNames().size() > 0) {
			List<NameDTO> names = unChangedNames.get(0).getUnChangedNames();
			for (NameDTO nameDTO : names) {
				colAdultNames.add(nameDTO.getIATAName());
			}
		}

		return colAdultNames;
	}

	private Collection<String> getParentNamesFromInfantList(BookingRequestDTO bookingRequestDTO) {
		Collection<String> parentNames = new HashSet<String>();
		List<SSRDTO> ssrDTOs = bookingRequestDTO.getSsrDTOs();

		if (ssrDTOs != null && ssrDTOs.size() > 0) {
			String iataGuardianName;
			SSRInfantDTO sSRInfantDTO;
			for (SSRDTO ssrdto : ssrDTOs) {
				if (ssrdto instanceof SSRInfantDTO) {
					sSRInfantDTO = (SSRInfantDTO) ssrdto;
					iataGuardianName = GDSApiUtils.maskNull(sSRInfantDTO.getIATAGuardianName());
					parentNames.add(iataGuardianName);
				}
			}
		}

		return parentNames;
	}
}
