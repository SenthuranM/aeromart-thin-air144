package com.isa.thinair.gdsservices.core.remoting.ejb;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.gdsservices.api.dto.typea.init.InitBaseRq;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketChangeOfStatusRQ;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.core.config.GDSServicesModuleConfig;
import com.isa.thinair.gdsservices.core.typeA.model.IataOperationConfig;
import com.isa.thinair.login.util.ForceLoginInvoker;

@MessageDriven(name = "GDSTypeANotifierMDB", activationConfig = {
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/isaGDSTypeANotifierQueue"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")})
public class GDSTypeANotifierMDB implements MessageListener {

	private static Log log = LogFactory.getLog(GDSTypeANotifierMDB.class);

	public void onMessage(Message message) {
		ObjectMessage objMessage;
		Object request;
		InitBaseRq initBaseRq;
		String commandName;
		DefaultBaseCommand command;

		GDSServicesModuleConfig config = GDSServicesModuleUtil.getConfig();
		ForceLoginInvoker.login(config.getEjbAccessUsername(), config.getEjbAccessPassword());

		objMessage = (ObjectMessage) message;

		try {

			request = objMessage.getObject();
			if (request instanceof TicketChangeOfStatusRQ) {
				initBaseRq = (InitBaseRq) request;
				commandName = IataOperationConfig.getMessageInitiatorCommand(IATAOperation.TKCREQ);
				command = (DefaultBaseCommand) GDSServicesModuleUtil.getModule().getLocalBean(commandName);
				command.setParameter(TypeACommandParamNames.TICKET_CONTROL_DTO, initBaseRq);
				command.setParameter(TypeACommandParamNames.IATA_OPERATION, IATAOperation.TKCREQ);
				command.setParameter(TypeACommandParamNames.IATA_EVENT, initBaseRq.getRequestType());

				command.execute();
			}

		} catch (Exception e) {
	   	    log.error("Error occurred while processing IATA-event " , e);
		}
	}
}
