package com.isa.thinair.gdsservices.core.bl.typeA.ticket;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.dto.GDSChargeAdjustmentRQ;
import com.isa.thinair.airproxy.api.dto.GDSPaxChargesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketsAssembler;
import com.isa.thinair.gdsservices.api.model.ExternalReservation;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.bl.internal.ticket.TicketIssuanceHandler;
import com.isa.thinair.gdsservices.core.bl.internal.ticket.TicketIssuanceHandlerImpl;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageHandler;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.config.TypeAConfig;
import com.isa.thinair.gdsservices.core.config.TypeAMessageConfig;
import com.isa.thinair.gdsservices.core.dto.FinancialInformation;
import com.isa.thinair.gdsservices.core.dto.FinancialInformationAssembler;
import com.isa.thinair.gdsservices.core.dto.FinancialInformationEditor;
import com.isa.thinair.gdsservices.core.dto.temp.GdsReservation;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerTicketingInformationWrapper;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAConcurrencyException;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSTypeAServiceDAO;
import com.isa.thinair.gdsservices.core.typeA.parser.EdiParser;
import com.isa.thinair.gdsservices.core.typeA.parser.EdiParserFactory;
import com.isa.thinair.gdsservices.core.typeA.parser.TypeAParseConstants;
import com.isa.thinair.gdsservices.core.typeA.transformers.ReservationDTOsTransformer;
import com.isa.thinair.gdsservices.core.typeA.transformers.ReservationDtoMerger;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.gdsservices.core.util.GdsCalculator;
import com.isa.thinair.gdsservices.core.util.GdsDtoTransformer;
import com.isa.thinair.gdsservices.core.util.GdsInfoDaoHelper;
import com.isa.thinair.gdsservices.core.util.GdsUtil;
import com.isa.thinair.gdsservices.core.util.KeyGenerator;
import com.isa.thinair.gdsservices.core.util.TypeAMessageUtil;
import com.isa.thinair.gdsservices.core.util.TypeANotes;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.typea.common.ErrorInformation;
import com.isa.typea.common.FlightInformation;
import com.isa.typea.common.InteractiveFreeText;
import com.isa.typea.common.MessageFunction;
import com.isa.typea.common.OriginatorInformation;
import com.isa.typea.common.TicketCoupon;
import com.isa.typea.common.TicketNumberDetails;
import com.isa.typea.common.TravellerTicketingInformation;
import com.isa.typea.tktreq.AATKTREQ;
import com.isa.typea.tktreq.TKTREQMessage;
import com.isa.typea.tktres.AATKTRES;
import com.isa.typea.tktres.TKTRESMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class RefundTicket extends EdiMessageHandler {

	private static final Log log = LogFactory.getLog(RefundTicket.class);

	private AATKTREQ tktReq;
	private AATKTRES tktRes;

	protected void handleMessage() {
		TKTREQMessage tktreqMessage;
		TKTRESMessage tktresMessage;
		MessageFunction messageFunction;

		tktReq = (AATKTREQ)messageDTO.getRequest();
		tktreqMessage = tktReq.getMessage();

		tktRes =  new AATKTRES();
		tktresMessage = new TKTRESMessage();
		tktresMessage.setMessageHeader(GDSDTOUtil.createRespMessageHeader(tktreqMessage.getMessageHeader(),
				messageDTO.getRequestMetaDataDTO().getMessageReference(), messageDTO.getRequestMetaDataDTO().getIataOperation().getResponse()));
		tktRes.setHeader(GDSDTOUtil.createRespEdiHeader(tktReq.getHeader(), tktresMessage.getMessageHeader(), messageDTO));
		tktRes.setMessage(tktresMessage);
		messageDTO.setResponse(tktRes);

        try {

	        preHandleMessage();
	        refundTicket();
	        postHandleMessage();

        } catch (GdsTypeAException e) {
	        messageFunction = new MessageFunction();
	        messageFunction.setMessageFunction(TypeAConstants.MessageFunction.REFUND);
	        messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
	        tktresMessage.setMessageFunction(messageFunction);

	        ErrorInformation errorInformation = new ErrorInformation();
	        errorInformation.setApplicationErrorCode(e.getEdiErrorCode());
	        tktresMessage.setErrorInformation(errorInformation);

	        messageDTO.setInvocationError(true);
	        messageDTO.setRollbackTransaction(true);

			log.error("Error Occurred ----", e);

		} catch (Exception e) {
	        messageFunction = new MessageFunction();
	        messageFunction.setMessageFunction(TypeAConstants.MessageFunction.REFUND);
	        messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
	        tktresMessage.setMessageFunction(messageFunction);

	        ErrorInformation errorInformation = new ErrorInformation();
	        errorInformation.setApplicationErrorCode(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR);
	        tktresMessage.setErrorInformation(errorInformation);

	        messageDTO.setInvocationError(true);
	        messageDTO.setRollbackTransaction(true);

			log.error("Error Occurred ----", e);

		}

	}

	private void preHandleMessage() throws GdsTypeAException {

		boolean functionSupported;
		TypeAConfig typeAConfig = GDSServicesModuleUtil.getConfig().getTypeAConfig();

		OriginatorInformation originatorInformation = tktReq.getMessage().getOriginatorInformation();
		String gdsCarrierCode = originatorInformation.getSenderSystemDetails().getCompanyCode();

		Map<String, GDSStatusTO> gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
		GDSExternalCodes.GDS gds = null;

		for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
			if (gdsStatusTO.getCarrierCode().equals(gdsCarrierCode)) {
				gds = GDSExternalCodes.GDS.valueOf(gdsStatusTO.getGdsCode());
			}
		}

		TypeAConfig.TypeAFunctionConfig typeAFunctionConfig = typeAConfig.getFunctions().get(gds);

		TypeAConfig.FunctionConfig functionConfig = typeAFunctionConfig.getMessageConfig().get(TypeAConfig.Category.TICKET).get(TypeAConfig.Function
				.REFUND);

		functionSupported = functionConfig.isEnabled();

		if (!functionSupported) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.MESSAGE_FUNCTION_NOT_SUPPORTED, TypeANotes.ErrorMessages.NO_MESSAGE);
		}

	}

	private void refundTicket() throws GdsTypeAException {
		handleRefundTicket();
	}

	private void postHandleMessage() throws GdsTypeAException {

	}


	private void handleRefundTicket() throws GdsTypeAException {

		TravellerTicketingInformation refundTraveller;
		TicketNumberDetails refundingTicket;
		TicketCoupon refundingCoupon;
		List<TravellerTicketingInformation> refundTravellers;
		TKTREQMessage tktreqMessage = tktReq.getMessage();
		String settlementAuthCode;
		List<TicketNumberDetails> couponTransitions;

		LCCClientReservation reservation;
		LCCClientReservationPax passenger;
		GdsReservation<TravellerTicketingInformationWrapper> gdsReservation;

		List<TravellerTicketingInformationWrapper> travellersByConjunctions;
		Map<Integer, Set<String>> conjunctiveTickets;

		EdiParser ediParser = EdiParserFactory.getEdiParser(null);
		Map<TypeAParseConstants.Segment, String> refundFareData;

		TKTRESMessage tktresMessage = tktRes.getMessage();

		MessageFunction messageFunction = new MessageFunction();
		messageFunction.setMessageFunction(TypeAConstants.MessageFunction.REFUND);
		messageFunction.setResponseType(TypeAConstants.ResponseType.PROCESSED_SUCCESSFULLY);
		tktresMessage.setMessageFunction(messageFunction);

		refundTravellers = tktresMessage.getTravellerTicketingInformation();

		List<String> refundingTicketNumbers = new ArrayList<String>();
		Map<String, Set<String>> refundingCoupons = new HashMap<String, Set<String>>();
		for(TicketNumberDetails ticket : tktreqMessage.getTickets()) {
			refundingTicketNumbers.add(ticket.getTicketNumber());

			if (!refundingCoupons.containsKey(ticket.getTicketNumber())) {
				refundingCoupons.put(ticket.getTicketNumber(), new HashSet<String>());
			}

			if (ticket.getTicketCoupon() != null) {
				for (TicketCoupon coupon : ticket.getTicketCoupon()) {
					refundingCoupons.get(ticket.getTicketNumber()).add(coupon.getCouponNumber());
				}
			}
		}

		// pnr -> pax-id -> tkt-no
		Map<String, Map<Integer, List<String>>> groupedTickets = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO.groupTickets(refundingTicketNumbers);
		Map<Integer, List<String>> passengers;
		List<String> paxTickets;
		TravellerTicketingInformationWrapper travellerWrapperImage;
		TravellerTicketingInformation travellerImage;
		TicketNumberDetails ticketImage, transitionTicket;
		TicketCoupon couponImage, transitionCoupon;
		Set<String> coupons;


		TicketsAssembler.PassengerAssembler passengerAssembler;
		TicketsAssembler ticketsAssembler;

		// tkt-no -> cpn-no
		Map<String, Set<Integer>> refundingTicketsCoupons;
		boolean refundApplicable;

		for (String pnr : groupedTickets.keySet()) {
			passengers = groupedTickets.get(pnr);

			try {
				try {
					GDSServicesModuleUtil.getGDSNotifyBD().reservationLock(pnr);
				} catch (ModuleException e) {
					throw new GdsTypeAConcurrencyException();
				}

				reservation = GDSServicesUtil.loadLccReservation(pnr);

				if (reservation == null) {
					throw new ModuleException(TypeANotes.ErrorMessages.NO_MESSAGE);
				}

				gdsReservation = GdsInfoDaoHelper.getGdsReservationTicketView(pnr);

				if (gdsReservation == null) {
					throw new ModuleException(TypeANotes.ErrorMessages.NO_MESSAGE);
				}

			} catch (ModuleException e) {
				throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.NO_PNR_MATCH, TypeANotes.ErrorMessages.NO_MESSAGE);
			}

			ticketsAssembler = new TicketsAssembler(TicketsAssembler.Mode.REFUND);
			ticketsAssembler.setPnr(reservation.getPNR());
			ticketsAssembler.setIpAddress(messageDTO.getRequestIp());


			GDSTypeAServiceDAO gdsTypeAServiceDAO = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO;
			ExternalReservation externalReservation = gdsTypeAServiceDAO.gdsTypeAServiceGet(ExternalReservation.class, pnr);
			if (externalReservation.getReservationSynced().equals(CommonsConstants.NO)) {
				throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.OPERATION_NOT_AUTHORIZED_FOR_PNR, TypeANotes.ErrorMessages.NO_MESSAGE);
			}

			for (Integer paxId : passengers.keySet()) {
				couponTransitions = new ArrayList<TicketNumberDetails>();
				refundingTicketsCoupons = new HashMap<String, Set<Integer>>();
				settlementAuthCode = KeyGenerator.generateSettlementAuthorizationCode();

				passenger = GDSDTOUtil.resolvePax(reservation, paxId);
				travellersByConjunctions = GDSDTOUtil.resolvePassengers(gdsReservation, passenger);
				conjunctiveTickets = GDSDTOUtil.groupTicketsByConjunctions(travellersByConjunctions);

				paxTickets = passengers.get(paxId);

				for (Set<String> refundingTickets : conjunctiveTickets.values()) {

					refundApplicable = false;
					for (String ticketNumber : paxTickets) {
						if (refundingTickets.contains(ticketNumber)) {
							refundApplicable = true;
							break;
						}
					}

					if (refundApplicable) {

						travellerWrapperImage = GDSDTOUtil.resolvePassengerByTicketNumber(gdsReservation, refundingTickets.iterator().next());
						travellerImage = travellerWrapperImage.getTravellerInformation();

						refundTraveller = new TravellerTicketingInformation();
						refundTraveller.setTravellerSurname(travellerImage.getTravellerSurname());
						refundTraveller.setTravellerCount(travellerImage.getTravellerCount());
						refundTraveller.setTravellerQualifier(travellerImage.getTravellerQualifier());
						refundTraveller.getTravellers().addAll(travellerImage.getTravellers());

						refundTraveller.setTicketingAgentInformation(travellerWrapperImage.getTicketingAgentInformation());
						refundTraveller.getReservationControlInformation().addAll(gdsReservation.getReservationControlInformation());
						refundTraveller.getFormOfPayment().addAll(travellerImage.getFormOfPayment());
						refundTraveller.setPricingTicketingDetails(travellerImage.getPricingTicketingDetails());
						refundTraveller.setOriginDestinationInfo(travellerImage.getOriginDestinationInfo());
						refundTraveller.setOriginatorInformation(travellerWrapperImage.getOriginatorInformation());
						// TODO -- EQN

						for (String ticketNumber : refundingTickets) {
							coupons = refundingCoupons.get(ticketNumber);
							ticketImage = GDSDTOUtil.resolveTicket(travellerImage, ticketNumber);

							refundingTicket = new TicketNumberDetails();
							refundingTicket.setTicketNumber(ticketImage.getTicketNumber());
							refundingTicket.setDocumentType(ticketImage.getDocumentType());
							refundTraveller.getTickets().add(refundingTicket);

							transitionTicket = new TicketNumberDetails();
							transitionTicket.setTicketNumber(ticketImage.getTicketNumber());
							transitionTicket.setDocumentType(ticketImage.getDocumentType());
							couponTransitions.add(transitionTicket);

							refundingTicketsCoupons.put(ticketNumber, new HashSet<Integer>());

							if (coupons != null && coupons.isEmpty()) {
								for (TicketCoupon cpnImage : ticketImage.getTicketCoupon())	{
									if (cpnImage.getStatus().equals(TypeAConstants.CouponStatus.ORIGINAL_ISSUE)) {
										coupons.add(cpnImage.getCouponNumber());
									}
								}
							}

							for (String couponNumber : coupons) {
								couponImage = GDSDTOUtil.resolveCoupon(ticketImage, couponNumber);

								couponImage.setSettlementAuthCode(settlementAuthCode);
								couponImage.setStatus(TypeAConstants.CouponStatus.REFUNDED);

								refundingTicket.getTicketCoupon().add(couponImage);

								refundingTicketsCoupons.get(ticketNumber).add(Integer.valueOf(couponNumber));

								transitionCoupon = new TicketCoupon();
								transitionCoupon.setCouponNumber(couponImage.getCouponNumber());
								transitionCoupon.setStatus(couponImage.getStatus());
								transitionCoupon.setSettlementAuthCode(couponImage.getSettlementAuthCode());
								transitionCoupon.setFlightInfomation(couponImage.getFlightInfomation());
								transitionTicket.getTicketCoupon().add(transitionCoupon);

							}

						}

						for (InteractiveFreeText text : travellerImage.getText()) {
							if (!text.getInformationType().equals(TypeAConstants.InformationType.FARE_CALCULATION)) {
								refundTraveller.getText().add(text);
							}
						}

						FinancialInformationAssembler refundFinancialInfoAssembler = handlePaxRefund(reservation, paxId, travellerImage, refundingTicketsCoupons, ticketsAssembler);
						FinancialInformation refundFinancialInfo = refundFinancialInfoAssembler.getFinancialInformation();

						refundFareData = ediParser.generateFinancialInformation(refundFinancialInfo);
						InteractiveFreeText freeText = new InteractiveFreeText();
						freeText.setTextSubjectQualifier(TypeAConstants.TextSubjectQualifier.CODED_AND_LITERAL_TEXT);
						freeText.setInformationType(TypeAConstants.InformationType.FARE_CALCULATION);
						freeText.getFreeText().addAll(TypeAMessageUtil.splitText(
								refundFareData.get(TypeAParseConstants.Segment.IFT_15),
								TypeAConstants.ElementMaxLength.INTERACTIVE_FREE_TEXT));
						refundTraveller.getText().add(freeText);

						refundTraveller.getMonetaryInformation().addAll(refundFinancialInfoAssembler.getMonetaryInformation());
						refundTraveller.getTaxDetails().addAll(refundFinancialInfoAssembler.getTaxDetails());

						refundTravellers.add(refundTraveller);

						GDSDTOUtil.mergeCouponTransitions(travellerWrapperImage.getCouponTransitions(), couponTransitions, tktreqMessage.getOriginatorInformation());
						travellerWrapperImage.setOriginatorInformation(tktreqMessage.getOriginatorInformation());

					}

				}

			}

			postHandleRefund(reservation, gdsReservation, ticketsAssembler);

		}

	}

	private FinancialInformationAssembler handlePaxRefund(LCCClientReservation reservation, Integer paxId, TravellerTicketingInformation traveller,
	                                             Map<String, Set<Integer>> refundingTicketsCoupons, TicketsAssembler ticketsAssembler) throws GdsTypeAException {
		int tempPaxId;
		FinancialInformation originalFareInfo = null;
		FinancialInformation refundFareInfo = null;
		List<FinancialInformationEditor.FilterSegment> filterSegments;
		FinancialInformationEditor.FilterSegment filterSegment;
		String flightSegRefNumber;
		FlightInformation flightInformation;

		FinancialInformationAssembler financialInformationAssembler;
		FinancialInformationAssembler filteredAssembler = null;
		TicketsAssembler.PassengerAssembler passengerAssembler;


		for (LCCClientReservationPax pax : reservation.getPassengers()) {
			tempPaxId = PaxTypeUtils.getPnrPaxId(pax.getTravelerRefNumber());
			if (paxId.intValue() == tempPaxId) {

				/*
				if (!pax.getStatus().equals(ReservationInternalConstants.ReservationPaxStatus.CANCEL)) {

					for (String extTicket : refundingTicketsCoupons.keySet()) {
						for (Integer extCoupon : refundingTicketsCoupons.get(extTicket)) {
							flightSegRefNumber = null;

							for (LccClientPassengerEticketInfoTO eticketInfoTO : pax.geteTickets()) {
								if (eticketInfoTO.getExternalPaxETicketNo().equals(extTicket) &&
										eticketInfoTO.getExternalCouponNo().equals(extCoupon)) {
									flightSegRefNumber = eticketInfoTO.getFlightSegmentRef();
								}
							}

							if (flightSegRefNumber == null) {
								throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.EXISTING_ITINERARY_INCOMPATIBLE, TypeANotes.ErrorMessages.NO_MESSAGE);
							}

							for (LCCClientReservationSegment segment : reservation.getSegments()) {
								if (segment.getFlightSegmentRefNumber().equals(flightSegRefNumber) &&
										!segment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
									throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.OPERATION_NOT_AUTHORIZED_FOR_PNR, TypeANotes.ErrorMessages.NO_MESSAGE);
								}
							}

						}
					}

				}
				*/

				financialInformationAssembler = new FinancialInformationAssembler();
				financialInformationAssembler.setFinancialInformation(traveller.getText());
				financialInformationAssembler.setTaxDetails(traveller.getTaxDetails());
				financialInformationAssembler.setMonetaryInformation(traveller.getMonetaryInformation());
				financialInformationAssembler.calculateAmounts();

				filterSegments = new ArrayList<FinancialInformationEditor.FilterSegment>();


				for (TicketNumberDetails ticket : traveller.getTickets()) {
					if (ticket.getDataIndicator().equals(TypeAConstants.TicketDataIndicator.NEW)) {
						for (TicketCoupon coupon : ticket.getTicketCoupon()) {
							flightInformation = coupon.getFlightInfomation();

							filterSegment = new FinancialInformationEditor.FilterSegment();
							filterSegment.setDeparture(flightInformation.getDepartureLocation().getLocationCode());
							filterSegment.setArrival(flightInformation.getArrivalLocation().getLocationCode());
							filterSegment.setWeight(BigDecimal.valueOf(1)); // TODO -- set mileage of the segment
							filterSegment.setFilter(
									refundingTicketsCoupons.containsKey(ticket.getTicketNumber()) &&
											refundingTicketsCoupons.get(ticket.getTicketNumber()).contains(Integer.valueOf(coupon.getCouponNumber())));

							filterSegments.add(filterSegment);
						}
					}
				}


				// TODO -- check whether all requested coupons are refunded

				filteredAssembler = FinancialInformationEditor.filter(financialInformationAssembler, filterSegments);
				String settleCurrency = GdsUtil.getSettleCurrency(traveller.getMonetaryInformation());
				filteredAssembler.setMonetaryInformation(traveller.getMonetaryInformation());
				filteredAssembler.setTaxDetails(traveller.getTaxDetails());
				
				filteredAssembler.constructElements(settleCurrency, GdsCalculator.RoundingType.DOWN, GDSServicesUtil.isGDSTaxRefundable(reservation.getGdsId()));

				paxRefund(reservation, pax, traveller, financialInformationAssembler, filteredAssembler);

				passengerAssembler = GdsDtoTransformer.appendToTicketsAssembler(ticketsAssembler, traveller, pax);
				BigDecimal refundAmount = filteredAssembler.getFareAmount()
						.add(filteredAssembler.getSurcharge())
						.add(filteredAssembler.getTaxAmount());

				passengerAssembler.setTotalAmount(refundAmount);


				break;
			}
		}

		return filteredAssembler;

	}


	private void paxRefund(LCCClientReservation reservation, LCCClientReservationPax pax,
	                       TravellerTicketingInformation traveller, FinancialInformationAssembler oldAssembler,
	                       FinancialInformationAssembler refundingAssembler) throws GdsTypeAException {

		// adjustment
		GDSChargeAdjustmentRQ gdsChargeAdjustmentRQ = new GDSChargeAdjustmentRQ();
		gdsChargeAdjustmentRQ.setPnr(reservation.getPNR());
		gdsChargeAdjustmentRQ.setGroupPNR(reservation.isGroupPNR());
		gdsChargeAdjustmentRQ.setVersion(reservation.getVersion());

		GDSPaxChargesTO paxChargesTO = new GDSPaxChargesTO();
		paxChargesTO.setFareAmount(oldAssembler.getFareAmount().subtract(refundingAssembler.getFareAmount()));
		paxChargesTO.setTaxAmount(oldAssembler.getTaxAmount().subtract(refundingAssembler.getTaxAmount()));
		paxChargesTO.setSurChargeAmount(oldAssembler.getSurcharge().subtract(refundingAssembler.getSurcharge()));
		paxChargesTO.setPaxType(pax.getPaxType());
		paxChargesTO.setInactiveSegmentsApplicable(true);

		gdsChargeAdjustmentRQ.addGdsCharges(com.isa.thinair.lccclient.api.util.PaxTypeUtils.getPnrPaxId(pax.getTravelerRefNumber()),
				paxChargesTO);

		try {
			GDSServicesModuleUtil.getReservationBD().syncAATotalGroupChargesWithCarrier(gdsChargeAdjustmentRQ, null);
		} catch (ModuleException e) {
			throw new GdsTypeAException();
		}

	}

	private void postHandleRefund(LCCClientReservation reservation,
	                              GdsReservation<TravellerTicketingInformationWrapper> gdsReservation, TicketsAssembler ticketsAssembler) throws GdsTypeAException {

		TKTREQMessage tktreqMessage = tktReq.getMessage();
		GdsInfoDaoHelper.saveGdsReservationTicketView(reservation.getPNR(), gdsReservation);

		TicketIssuanceHandler ticketIssuanceHandler = new TicketIssuanceHandlerImpl();
		try {
			ticketIssuanceHandler.handleMonetaryDiscrepancies(ticketsAssembler);
		} catch (ModuleException e) {
			throw new GdsTypeAException();
		}

		try {
			List<LccClientPassengerEticketInfoTO> lccCoupons = ReservationDtoMerger.getMergedETicketCoupons(tktReq.getMessage().getTravellerTicketingInformation(), reservation);
			List<EticketTO> coupons = ReservationDTOsTransformer.toETicketTos(lccCoupons);

			ServiceResponce serviceResponce = GDSServicesModuleUtil.getReservationBD().updateExternalEticketInfo(coupons);
			if (!serviceResponce.isSuccess()) {
				throw new ModuleException(TypeANotes.ErrorMessages.GENERIC_ERROR);
			}
		} catch (ModuleException e) {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR, TypeANotes.ErrorMessages.NO_MESSAGE);
		}
	}


}
