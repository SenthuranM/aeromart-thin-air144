package com.isa.thinair.gdsservices.core.typeA.transformers.tkcreqres.v03;

import iata.typea.v031.tkcreq.TKCREQ;

import java.util.Map;

import javax.xml.bind.JAXBElement;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.core.typeA.helpers.Constants;
import com.isa.thinair.gdsservices.core.typeA.transformers.EDIFACTMessageProcess;

/**
 * Implementation logic for the type A ticket display.
 * 
 * @author sanjaya
 * 
 */
public class TicketDisplay implements EDIFACTMessageProcess {

	@Override
	public Map<String, Object> extractSpecificCommandParams(Object message) throws ModuleException {
		TKCREQ tkcreq = (TKCREQ) message;
		return TicketDisplayRequestHandler.extractCommandParams(tkcreq);
	}

	@Override
	public JAXBElement<?> constructEDIResponce(DefaultServiceResponse response) throws ModuleException {
		return TicketDisplayResponseHandler.constructEDIResponce(response);
	}

	@Override
	public String getCommandName() {
		return Constants.CommandNames.DISPLAY_TICKET;
	}

}
