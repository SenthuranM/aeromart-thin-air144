package com.isa.thinair.gdsservices.core.typeA.model;

import java.math.BigDecimal;

import com.isa.thinair.commons.api.exception.ModuleException;

public enum DataType {

	AN {
		@Override
		public boolean validatType(Object object) throws ModuleException {
			if (object instanceof String) {
				return true;
			}

			throw new ModuleException("gdsservices.typea.validators.require.missing");
		}
	},
	N {
		@Override
		public boolean validatType(Object object) throws ModuleException {
			if (object instanceof BigDecimal) {
				return true;
			}

			throw new ModuleException("gdsservices.typea.validators.require.missing");
		}
	},
	A {
		@Override
		public boolean validatType(Object object) throws ModuleException {
			if (object instanceof String) {
				return true;
			}

			throw new ModuleException("gdsservices.typea.validators.require.missing");

		}
	};

	public abstract boolean validatType(Object object) throws ModuleException;

}
