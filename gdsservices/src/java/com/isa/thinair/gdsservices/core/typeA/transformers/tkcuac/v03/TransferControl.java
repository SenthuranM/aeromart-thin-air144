package com.isa.thinair.gdsservices.core.typeA.transformers.tkcuac.v03;

import iata.typea.v031.tkcuac.TKCUAC;

import java.util.Map;

import javax.xml.bind.JAXBElement;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.core.typeA.helpers.Constants;
import com.isa.thinair.gdsservices.core.typeA.transformers.EDIFACTMessageProcess;

/**
 * This message will be send from validating carrier to OC when the booking is made form VC Onese we receive the TKCUAC
 * , a reservation will be created in the AccelAero side which conseum the inventory Note. Each TKCUAC message will have
 * infromation of only one pax. So Each reservation created by TKCUAC will garenteeed to have only one pax
 * 
 * @author malaka
 * 
 */
public class TransferControl implements EDIFACTMessageProcess {

	public TransferControl() {

	}

	@Override
	public Map<String, Object> extractSpecificCommandParams(Object message) throws ModuleException {
		TKCUAC tkcuac = (TKCUAC) message;
		return new TransferControlRequestHandler().extractCommandParams(tkcuac);
	}

	@Override
	/**
	 * Responsible for generating the responce message for the EDIFACT request
	 */
	public JAXBElement<?> constructEDIResponce(DefaultServiceResponse response) throws ModuleException {
		return new TransferControlResponseHandler().constructEDIResponce(response);
	}

	@Override
	public String getCommandName() {
		return Constants.CommandNames.TRANSFER_CONTROL;
	}

}
