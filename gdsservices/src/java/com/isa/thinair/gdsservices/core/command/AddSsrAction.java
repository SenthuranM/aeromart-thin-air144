package com.isa.thinair.gdsservices.core.command;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.gdsservices.api.dto.internal.AddSsrRequest;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceDetailRequest;
import com.isa.thinair.gdsservices.api.dto.internal.SpecialServiceRequest;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.platform.api.ServiceResponce;

public class AddSsrAction {
	private static Log log = LogFactory.getLog(AddSsrAction.class);

	public static AddSsrRequest processRequest(AddSsrRequest addSsrRequest) {
		try {

			// 1. Retrieve Reservation ---
			Reservation reservation = CommonUtil.loadReservation(addSsrRequest.getGdsCredentialsDTO(), addSsrRequest
					.getOriginatorRecordLocator().getPnrReference(), null);

			CommonUtil.validateReservation(addSsrRequest.getGdsCredentialsDTO(), reservation);

			String nameKey = null;
			Passenger passenger = null;
			Map<String, Passenger> passengerMap = CommonUtil.getPassengerMap(addSsrRequest.getPassengers());

			Map<Integer, SegmentSSRAssembler> paxSSRMap = new HashMap<Integer, SegmentSSRAssembler>();
			Collection<ReservationPax> resPassengers = reservation.getPassengers();
			for (ReservationPax resPax : resPassengers) {
				nameKey = GDSApiUtils.getIATAName(resPax.getLastName(), resPax.getFirstName(), resPax.getTitle());
				passenger = passengerMap.get(nameKey);

				if (passenger != null) {
					addSsrInfo(resPax, passenger, reservation, paxSSRMap);
				} else if (passengerMap.size() == 0) {
					addSsrInfoForAllPax(resPax, addSsrRequest.getCommonSSRs(), reservation, paxSSRMap);
				}
			}

			IPassenger iPassenger = new PassengerAssembler(null);
			TrackInfoDTO trackInfoDTO = CommonUtil.getTrackingInfo(addSsrRequest.getGdsCredentialsDTO());
			if (paxSSRMap.size() == 0) {
				addSsrRequest.setSuccess(false);
				addSsrRequest.setResponseMessage(MessageUtil.getMessage("gdsservices.actions.addssr.doesnot.exist"));
				addSsrRequest.addErrorCode("gdsservices.actions.addssr.doesnot.exist");
			} else {
				ServiceResponce serviceResponce = GDSServicesModuleUtil.getReservationBD().modifySSRs(paxSSRMap,
						reservation.getPnr(), "", iPassenger, (int) reservation.getVersion(), true,
						EXTERNAL_CHARGES.INFLIGHT_SERVICES, trackInfoDTO, false);
	
				if (serviceResponce != null && serviceResponce.isSuccess()) {
					addSsrRequest.setSuccess(true);
				} else {
					addSsrRequest.setSuccess(false);
					addSsrRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
					addSsrRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
				}
			}

		} catch (ModuleException e) {
			log.error(" AddSsrAction Failed for GDS PNR " + addSsrRequest.getOriginatorRecordLocator().getPnrReference(), e);
			addSsrRequest.setSuccess(false);
			addSsrRequest.setResponseMessage(e.getMessageString());
			addSsrRequest.addErrorCode(e.getExceptionCode());
		} catch (Exception e) {
			log.error(" AddSsrAction Failed for GDS PNR " + addSsrRequest.getOriginatorRecordLocator().getPnrReference(), e);
			addSsrRequest.setSuccess(false);
			addSsrRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
			addSsrRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
		}

		return addSsrRequest;
	}

	private static void addSsrInfo(ReservationPax resPax, Passenger passenger, Reservation reservation,
			Map<Integer, SegmentSSRAssembler> paxSSRMap) {

		Collection<SpecialServiceRequest> sSRs = passenger.getSsrCollection();

		if (passenger != null) {
			for (SpecialServiceRequest ssr : sSRs) {
				if (ssr instanceof SpecialServiceDetailRequest) {
					SpecialServiceDetailRequest ssdr = (SpecialServiceDetailRequest) ssr;
					if (ssdr.getSegment() != null) {
						for (ReservationPaxFare reservationPaxFare : resPax.getPnrPaxFares()) {
							for (ReservationPaxFareSegment reservationPaxFareSegment : reservationPaxFare.getPaxFareSegments()) {
								for (ReservationSegmentDTO seg : reservation.getSegmentsView()) {
									if (seg.getPnrSegId().intValue() == reservationPaxFareSegment.getPnrSegId().intValue()) {
										if (seg.getDestination().equals(ssdr.getSegment().getArrivalStation())
												&& seg.getOrigin().equals(ssdr.getSegment().getDepartureStation())
												&& CalendarUtil.isEqualStartTimeOfDate(seg.getDepartureDate(), ssdr.getSegment()
														.getDepartureDate())) {
											if (seg.getFlightNo().substring(2).equals(ssdr.getSegment().getFlightNumber())
													|| ((ssdr.getSegment().getFlightNumber().length() - 1 == seg.getFlightNo()
															.substring(2).length()) && seg.getFlightNo().substring(2)
															.equals(ssdr.getSegment().getFlightNumber().substring(1)))) {

												SegmentSSRAssembler ipaxSSR = paxSSRMap.get(resPax.getPaxSequence());
												if (ipaxSSR == null) {
													paxSSRMap.put(resPax.getPaxSequence(), new SegmentSSRAssembler());
													ipaxSSR = paxSSRMap.get(resPax.getPaxSequence());
												}
												ipaxSSR.addPaxSegmentSSR(seg.getSegmentSeq(), SSRUtil.getSSRId(ssr.getCode()),
														ssdr.getComment(), null, BigDecimal.ZERO, ssr.getCode(), null, null,
														null, ReservationPaxSegmentSSR.APPLY_ON_SEGMENT, null, null, null, null);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private static void addSsrInfoForAllPax(ReservationPax resPax, Collection<SpecialServiceRequest> commonSSRs,
			Reservation reservation, Map<Integer, SegmentSSRAssembler> paxSSRMap) {

		if (commonSSRs != null && commonSSRs.size() > 0) {
			for (SpecialServiceRequest ssr : commonSSRs) {
				if (ssr instanceof SpecialServiceDetailRequest) {
					SpecialServiceDetailRequest ssdr = (SpecialServiceDetailRequest) ssr;
					if (ssdr.getSegment() != null) {
						for (ReservationPaxFare reservationPaxFare : resPax.getPnrPaxFares()) {
							for (ReservationPaxFareSegment reservationPaxFareSegment : reservationPaxFare.getPaxFareSegments()) {
								for (ReservationSegmentDTO seg : reservation.getSegmentsView()) {
									if (seg.getPnrSegId().intValue() == reservationPaxFareSegment.getPnrSegId().intValue()) {
										if (seg.getDestination().equals(ssdr.getSegment().getArrivalStation())
												&& seg.getOrigin().equals(ssdr.getSegment().getDepartureStation())
												&& CalendarUtil.isEqualStartTimeOfDate(seg.getDepartureDate(), ssdr.getSegment()
														.getDepartureDate())
												&& seg.getFlightNo().substring(2).equals(ssdr.getSegment().getFlightNumber())) {

											SegmentSSRAssembler ipaxSSR = paxSSRMap.get(resPax.getPaxSequence());
											if (ipaxSSR == null) {
												paxSSRMap.put(resPax.getPaxSequence(), new SegmentSSRAssembler());
												ipaxSSR = paxSSRMap.get(resPax.getPaxSequence());
											}
											ipaxSSR.addPaxSegmentSSR(seg.getSegmentSeq(), SSRUtil.getSSRId(ssr.getCode()),
													ssdr.getComment(), null, BigDecimal.ZERO, ssr.getCode(), null, null, null,
													ReservationPaxSegmentSSR.APPLY_ON_SEGMENT, null, null, null, null);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
