package com.isa.thinair.gdsservices.core.bl.typeA.ticket;

import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageHandler;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.TypeANotes;
import com.isa.typea.common.ErrorInformation;
import com.isa.typea.common.MessageFunction;
import com.isa.typea.tktreq.AATKTREQ;
import com.isa.typea.tktreq.TKTREQMessage;
import com.isa.typea.tktres.AATKTRES;
import com.isa.typea.tktres.TKTRESMessage;

/**
 * @isa.module.command name="commonTicketHandler"
 */
public class CommonTicketHandler extends EdiMessageHandler {

	private AATKTREQ tktreq;
	private AATKTRES tktres;

	protected void handleMessage() {

		MessageFunction messageFunction;
		TKTREQMessage tktreqMessage = null;
		TKTRESMessage tktresMessage = null;

		try {

			tktreq = (AATKTREQ) messageDTO.getRequest();
			tktreqMessage = tktreq.getMessage();

			tktres = new AATKTRES();
			tktresMessage = new TKTRESMessage();
			tktresMessage.setMessageHeader(GDSDTOUtil.createRespMessageHeader(tktreqMessage.getMessageHeader(),
					messageDTO.getRequestMetaDataDTO().getMessageReference(),
					messageDTO.getRequestMetaDataDTO().getIataOperation().getResponse()));

			tktres.setHeader(GDSDTOUtil.createRespEdiHeader(tktreq.getHeader(), tktresMessage.getMessageHeader(), messageDTO));
			tktres.setMessage(tktresMessage);

			messageDTO.setResponse(tktres);

			validateMessage();

			EdiMessageHandler ticketHandler = getTicketHandler();
			ticketHandler.setParameter(TypeACommandParamNames.EDI_REQUEST_DTO, messageDTO);
			ticketHandler.execute();

		} catch (GdsTypeAException e) {
			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(tktreqMessage.getMessageFunction().getMessageFunction());
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			tktresMessage.setMessageFunction(messageFunction);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(e.getEdiErrorCode());
			tktresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);
		} catch (Exception e) {
			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(tktreqMessage.getMessageFunction().getMessageFunction());
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			tktresMessage.setMessageFunction(messageFunction);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR);
			tktresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);
		}
	}

	private void validateMessage() throws GdsTypeAException {
		TKTREQMessage tktreqMessage = tktreq.getMessage();

		constraintsValidator.validateMessageFunction(tktreqMessage.getMessageFunction());

	}

	protected Set<String> getAllowedMessageFunctions() {
		Set<String> msgFunctions = new HashSet<String>();
		msgFunctions.add(TypeAConstants.MessageFunction.ISSUE);
		msgFunctions.add(TypeAConstants.MessageFunction.EXCHANGE);
		msgFunctions.add(TypeAConstants.MessageFunction.REFUND);
		msgFunctions.add(TypeAConstants.MessageFunction.VOID);
		msgFunctions.add(TypeAConstants.MessageFunction.DISPLAY);
		msgFunctions.add(TypeAConstants.MessageFunction.HISTORY);
		msgFunctions.add(TypeAConstants.MessageFunction.REVALIDATE);
		msgFunctions.add(TypeAConstants.MessageFunction.PRINT);
		msgFunctions.add(TypeAConstants.MessageFunction.CANCEL);
		return msgFunctions;
	}

	private EdiMessageHandler getTicketHandler() throws GdsTypeAException {

		EdiMessageHandler command = null;
		AATKTREQ tktReq = (AATKTREQ)messageDTO.getRequest();
		MessageFunction messageFunction = tktReq.getMessage().getMessageFunction();

		String msgFunction = messageFunction.getMessageFunction();

		if (msgFunction.equals(TypeAConstants.MessageFunction.ISSUE)) {
			command = new IssueTicket();
		} else if (msgFunction.equals(TypeAConstants.MessageFunction.EXCHANGE)) {
			command = new ReIssueTicket();
		} else if (msgFunction.equals(TypeAConstants.MessageFunction.REFUND)) {
			command = new RefundTicket();
		} else if (msgFunction.equals(TypeAConstants.MessageFunction.VOID)) {
			command = new VoidTicket();
		} else if (msgFunction.equals(TypeAConstants.MessageFunction.DISPLAY)) {
			command = new DisplayTicket();
		} else if (msgFunction.equals(TypeAConstants.MessageFunction.HISTORY)) {
			command = new TicketHistory();
		} else if (msgFunction.equals(TypeAConstants.MessageFunction.REVALIDATE)) {
			command = new ReValidateTicket();
		} else if (msgFunction.equals(TypeAConstants.MessageFunction.PRINT)) {
			command = new PrintTicket();
		} else if (msgFunction.equals(TypeAConstants.MessageFunction.CANCEL)) {
			command = new SystemCancelTicket();
		} else {
			throw new GdsTypeAException(TypeAConstants.ApplicationErrorCode.MESSAGE_FUNCTION_NOT_SUPPORTED,
					TypeANotes.ErrorMessages.NO_MESSAGE);
		}

		return command;
	}


}
