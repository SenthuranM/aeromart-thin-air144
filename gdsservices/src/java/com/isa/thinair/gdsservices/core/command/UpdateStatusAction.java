package com.isa.thinair.gdsservices.core.command;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.UpdateStatusRequest;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

public class UpdateStatusAction {
	private static Log log = LogFactory.getLog(UpdateStatusAction.class);

	public static GDSReservationRequestBase processRequest(UpdateStatusRequest updateStatusRequest) {
		try {
			String originatorPnr = GDSApiUtils.maskNull(updateStatusRequest.getOriginatorRecordLocator().getPnrReference());
			Reservation reservation = CommonUtil.loadReservation(updateStatusRequest.getGdsCredentialsDTO(), originatorPnr, null);

			CommonUtil.validateReservation(updateStatusRequest.getGdsCredentialsDTO(), reservation);

			Collection<GDSInternalCodes.StatusCode> segmentStatuses = CommonUtil.getSegmentStatusCodes(updateStatusRequest);
			if (!segmentStatuses.contains(GDSInternalCodes.StatusCode.NOT_OPERATED_PROVIDED)) {
				int intDiff = CommonUtil.getGDSExtendOnholdDiffInMinutes(reservation);
				GDSServicesModuleUtil.getReservationBD().extendOnholdReservation(reservation.getPnr(), intDiff,
						reservation.getVersion(), CommonUtil.getTrackingInfo(updateStatusRequest.getGdsCredentialsDTO()));
				
				CommonUtil.setSegmentStatusCodes(updateStatusRequest.getNewSegmentDetails(),
						GDSInternalCodes.StatusCode.CONFIRMED);
			}

			updateStatusRequest.setResponseMessage("UpdateStatusAction/processRequest:not implemented yet.");
			updateStatusRequest.setSuccess(false);
		} catch (ModuleException e) {
			log.error(" GDSReservationRequestBase Failed for GDS PNR "
					+ updateStatusRequest.getOriginatorRecordLocator().getPnrReference(), e);
			updateStatusRequest.setSuccess(false);
			updateStatusRequest.setResponseMessage(e.getMessageString());
		} catch (Exception e) {
			log.error(" GDSReservationRequestBase Failed for GDS PNR "
					+ updateStatusRequest.getOriginatorRecordLocator().getPnrReference(), e);
			updateStatusRequest.setSuccess(false);
			updateStatusRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
		}

		return updateStatusRequest;
	}
}
