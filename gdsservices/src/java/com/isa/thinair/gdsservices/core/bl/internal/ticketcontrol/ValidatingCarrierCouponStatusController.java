package com.isa.thinair.gdsservices.core.bl.internal.ticketcontrol;

import static com.isa.thinair.gdsservices.api.util.TypeAConstants.CouponStatus.AIRPORT_CONTROL;
import static com.isa.thinair.gdsservices.api.util.TypeAConstants.CouponStatus.NOTIFICATION;
import static com.isa.thinair.gdsservices.api.util.TypeAConstants.CouponStatus.ORIGINAL_ISSUE;

import com.isa.thinair.gdsservices.core.dto.CouponStatusTransitionTo;

public class ValidatingCarrierCouponStatusController extends CouponStatusController {

	public void receiveCouponStatusTransition(CouponStatusTransitionTo receive) {

		String currentStatus = receive.getCurrentStatus();
		String receivingStatus = receive.getReceivingStatus();
		String newStatus = null;
		String sendingStatus = null;
		boolean sac = false;
		boolean validTrans = false;

		if (currentStatus.equals(ORIGINAL_ISSUE)) { // Operating Carrier requests control
			if (receivingStatus.equals(AIRPORT_CONTROL)) {
				newStatus = AIRPORT_CONTROL;
				sendingStatus = ORIGINAL_ISSUE;
				validTrans = true;
			}
		} else if (currentStatus.equals(AIRPORT_CONTROL)) { // Operating Carrier Returns control to Validating carrier
			if (receivingStatus.equals(ORIGINAL_ISSUE)) {
				newStatus = ORIGINAL_ISSUE;
				sendingStatus = ORIGINAL_ISSUE;
				validTrans = true;
			}
		} else if (isInterimStatus(currentStatus)) {
			if (isInterimStatus(receivingStatus)) { // Operating carrier sends interim coupon status or Irregular Operations
				newStatus = receivingStatus;
				sendingStatus = receivingStatus;
				validTrans = true;
			} else if (isFinalStatus(receivingStatus)) { // Operating carrier sends final coupon status
				newStatus = receivingStatus;
				sendingStatus = receivingStatus;
				sac = true;
				validTrans = true;
			} else {
				validTrans = false;
			}
		}

		receive.setNewStatus(newStatus);
		receive.setSendingStatus(sendingStatus);
		receive.setSettlementAuthCodeRequired(sac);
		receive.setInvalidTransition(!validTrans);

	}

	public void validateSendCouponStatusTransition(CouponStatusTransitionTo send) {

		String currentStatus = send.getCurrentStatus();
		String sendingStatus = send.getSendingStatus();
		String newStatus = null;
		boolean validTrans = false;

		if (currentStatus.equals(ORIGINAL_ISSUE)) { // Validating carrier sends UAC
			if (sendingStatus.equals(ORIGINAL_ISSUE)) {
				newStatus = AIRPORT_CONTROL;
				validTrans = true;
			}
		} else if (currentStatus.equals(AIRPORT_CONTROL)) { // Validating carrier requests control returned
			if (sendingStatus.equals(NOTIFICATION)) {
				newStatus = ORIGINAL_ISSUE;
				validTrans = true;
			}
		}

		send.setNewStatus(newStatus);
		send.setInvalidTransition(!validTrans);

	}

	public void callbackSendCouponStatusTransition(CouponStatusTransitionTo reply) {

		String currentStatus = reply.getCurrentStatus();
		String sendingStatus = reply.getSendingStatus();
		String receivingStatus = reply.getReceivingStatus();
		boolean validTrans = false;

		if (currentStatus.equals(ORIGINAL_ISSUE)) { // Validating carrier sends UAC
			if (sendingStatus.equals(ORIGINAL_ISSUE)) {
				if (receivingStatus.equals(ORIGINAL_ISSUE)) {
					validTrans = true;
				}
			}
		} else if (currentStatus.equals(AIRPORT_CONTROL)) { // Validating carrier requests control returned
			if (sendingStatus.equals(NOTIFICATION)) {
				if (receivingStatus.equals(ORIGINAL_ISSUE)) {
					validTrans = true;
				}
			}
		}

		reply.setInvalidTransition(!validTrans);
	}
}
