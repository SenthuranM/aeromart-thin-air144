package com.isa.thinair.gdsservices.core.util;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.dto.Values2To;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerInformationWrapper;
import com.isa.thinair.msgbroker.api.model.ServiceClient;
import com.isa.typea.common.FlightInformation;
import com.isa.typea.common.MonetaryInformation;
import com.isa.typea.common.TicketCoupon;
import com.isa.typea.common.TicketNumberDetails;
import com.isa.typea.common.Traveller;
import com.isa.typea.common.TravellerInformation;
import com.isa.typea.common.TravellerTicketingInformation;

public abstract class GdsUtil {
	public static String getDepartingAirport(String segCode) {
		return segCode.split(GDSConstants.DELIM_SEGMENT_AIRPORTS)[0];
	}

	public static String getArrivalAirport(String segCode) {
		String[] airports = segCode.split(GDSConstants.DELIM_SEGMENT_AIRPORTS);
		return airports[airports.length - 1];
	}

	public static Gds getGds(int gdsId) throws ModuleException {
		return GDSServicesModuleUtil.getGdsBD().getGds(gdsId);
	}

	public static ServiceClient getGdsServiceClient(int gdsId) throws  ModuleException {
		return GDSServicesModuleUtil.getTypeAServiceBD().getGdsServiceClient(gdsId);
	}

	public static Traveller getTraveller(TravellerTicketingInformation travellerTicketingInformation,
	                                      LCCClientReservationPax pax) {
		Traveller traveller = null;

		if (travellerTicketingInformation.getTravellerSurname()
				.equalsIgnoreCase(pax.getLastName())) {
			for (Traveller tvl : travellerTicketingInformation.getTravellers()) {
				if (tvl.getGivenName().equalsIgnoreCase(pax.getFirstName())) {
				 	traveller = tvl;
					break;
				}
			}
		}

		return traveller;
	}

	public static LCCClientReservationPax getLccPax(TravellerInformation travellerInformation, Traveller traveller,
	                                      LCCClientReservation lccReservation) {
		LCCClientReservationPax lccPax = null;

		for (LCCClientReservationPax pax : lccReservation.getPassengers()) {
			if (traveller.getGivenName().equalsIgnoreCase(pax.getFirstName()) &&
					travellerInformation.getTravellerSurname().equalsIgnoreCase(pax.getLastName())) {
				lccPax = pax;
				break;
			}
		}

		return lccPax;
	}

	public static LccClientPassengerEticketInfoTO lccPaxEticket(TicketNumberDetails ticket, TicketCoupon ticketCoupon,
	                                                            Collection<LccClientPassengerEticketInfoTO> lccETickets) {
		LccClientPassengerEticketInfoTO lccPaxEticket = null;
		String flightNumber = null;

		if (ticket.getDataIndicator() == null || ticket.getDataIndicator().equals(TypeAConstants.TicketDataIndicator.OLD)) {
			for (LccClientPassengerEticketInfoTO eticketInfoTO : lccETickets) {
				if (ticket.getTicketNumber().equals(eticketInfoTO.getExternalPaxETicketNo())
						&& ticketCoupon.getCouponNumber().equals(String.valueOf(eticketInfoTO.getExternalCouponNo()))
						&& (ticketCoupon.getFlightInfomation() == null || CalendarUtil.isDateEqual(
								CalendarUtil.asDate(ticketCoupon.getFlightInfomation().getDepartureDateTime()),
								eticketInfoTO.getDepartureDate()))
						&& !eticketInfoTO.getExternalCouponStatus().equals(TypeAConstants.CouponStatus.VOIDED)) {
					lccPaxEticket = eticketInfoTO;
					break;
				}
			}
		} else if (ticket.getDataIndicator().equals(TypeAConstants.TicketDataIndicator.NEW)) {
			FlightInformation flightInformation = ticketCoupon.getFlightInfomation();
			if (flightInformation != null) {
				flightNumber = flightInformation.getCompanyInfo().getMarketingAirlineCode() + flightInformation.getFlightNumber();
			}

			for (LccClientPassengerEticketInfoTO eticketInfoTO : lccETickets) {
				if (flightNumber != null && flightNumber.equals(eticketInfoTO.getFlightNo()) &&
						flightInformation.getDepartureDateTime().toGregorianCalendar().getTime().equals(eticketInfoTO.getDepartureDate()) &&
						flightInformation.getDepartureLocation().getLocationCode().equals(getDepartingAirport(eticketInfoTO.getSegmentCode())) &&
						flightInformation.getArrivalLocation().getLocationCode().equals(getArrivalAirport(eticketInfoTO.getSegmentCode())) &&
						!eticketInfoTO.getPaxETicketStatus().equals(CommonsConstants.EticketStatus.EXCHANGED.code()) &&
						(eticketInfoTO.getExternalCouponStatus() == null || !eticketInfoTO.getExternalCouponStatus().equals(TypeAConstants.CouponStatus.VOIDED))) {
					lccPaxEticket = eticketInfoTO;
					break;
				}
			}
		}


		return lccPaxEticket;
	}

	public static LccClientPassengerEticketInfoTO lccTicketControlPaxEticket(TicketNumberDetails ticket, TicketCoupon ticketCoupon,
	                                                            Collection<LccClientPassengerEticketInfoTO> lccETickets) {
		LccClientPassengerEticketInfoTO lccPaxEticket = null;
		String flightNumber = null;

		if (ticket.getDataIndicator() == null ||
				ticket.getDataIndicator().equals(TypeAConstants.TicketDataIndicator.OLD)) {
			for (LccClientPassengerEticketInfoTO eticketInfoTO : lccETickets) {
				if (ticket.getTicketNumber().equals(eticketInfoTO.getExternalPaxETicketNo())
						&& ticketCoupon.getCouponNumber().equals(String.valueOf(eticketInfoTO.getExternalCouponNo()))
						&& !eticketInfoTO.getExternalCouponStatus().equals(TypeAConstants.CouponStatus.VOIDED)) {
					lccPaxEticket = eticketInfoTO;
					break;
				}
			}
		} else if (ticket.getDataIndicator().equals(TypeAConstants.TicketDataIndicator.NEW)) {
			FlightInformation flightInformation = ticketCoupon.getFlightInfomation();
			if (flightInformation != null) {
				flightNumber = flightInformation.getCompanyInfo().getMarketingAirlineCode() + flightInformation.getFlightNumber();
			}

			for (LccClientPassengerEticketInfoTO eticketInfoTO : lccETickets) {
				String csFlightNumber = eticketInfoTO.getCodeShareFlightNumber().substring(2);
				if (flightNumber != null && new Integer(flightInformation.getFlightNumber()).compareTo(new Integer(csFlightNumber)) == 0 &&
						flightInformation.getDepartureDateTime().toGregorianCalendar().getTime().equals(eticketInfoTO.getDepartureDate()) &&
						flightInformation.getDepartureLocation().getLocationCode().equals(getDepartingAirport(eticketInfoTO.getSegmentCode())) &&
						flightInformation.getArrivalLocation().getLocationCode().equals(getArrivalAirport(eticketInfoTO.getSegmentCode())) &&
						!eticketInfoTO.getPaxETicketStatus().equals(CommonsConstants.EticketStatus.EXCHANGED.code()) &&
						(eticketInfoTO.getExternalCouponStatus() == null || !eticketInfoTO.getExternalCouponStatus().equals(TypeAConstants.CouponStatus.VOIDED))) {
					lccPaxEticket = eticketInfoTO;
					break;
				}
			}
		}


		return lccPaxEticket;
	}


	public static LccClientPassengerEticketInfoTO lccPaxEticketByInternalData(TicketNumberDetails ticket, TicketCoupon ticketCoupon,
	                                                            Collection<LccClientPassengerEticketInfoTO> lccETickets) {
		LccClientPassengerEticketInfoTO lccPaxEticket = null;
		String flightNumber = null;
		boolean matchedByInternalData;

		for (LccClientPassengerEticketInfoTO eticketInfoTO : lccETickets) {

			FlightInformation flightInformation = ticketCoupon.getFlightInfomation();
			if (flightInformation != null) {
				flightNumber = flightInformation.getCompanyInfo().getMarketingAirlineCode() + flightInformation.getFlightNumber();
			}

			matchedByInternalData =
					flightNumber != null && flightNumber.equals(eticketInfoTO.getFlightNo()) &&
							flightInformation.getDepartureDateTime().toGregorianCalendar().getTime().equals(eticketInfoTO.getDepartureDate()) &&
							flightInformation.getDepartureLocation().getLocationCode().equals(getDepartingAirport(eticketInfoTO.getSegmentCode())) &&
							flightInformation.getArrivalLocation().getLocationCode().equals(getArrivalAirport(eticketInfoTO.getSegmentCode())) &&
							!eticketInfoTO.getPaxETicketStatus().equals(CommonsConstants.EticketStatus.EXCHANGED.code()) &&
							(eticketInfoTO.getExternalCouponStatus() == null || !eticketInfoTO.getExternalCouponStatus().equals(TypeAConstants.CouponStatus.VOIDED));

			if (matchedByInternalData) {
				lccPaxEticket = eticketInfoTO;
				break;
			}
		}

		return lccPaxEticket;
	}

	public static LccClientPassengerEticketInfoTO lccPaxEticketByExternalData(TicketNumberDetails ticket, TicketCoupon ticketCoupon,
	                                                            Collection<LccClientPassengerEticketInfoTO> lccETickets) {
		LccClientPassengerEticketInfoTO lccPaxEticket = null;
		boolean matchedByExternalData;

		for (LccClientPassengerEticketInfoTO eticketInfoTO : lccETickets) {

			matchedByExternalData =
					eticketInfoTO.getExternalPaxETicketNo() != null &&
							ticket.getTicketNumber().equals(eticketInfoTO.getExternalPaxETicketNo()) &&
							eticketInfoTO.getExternalCouponNo() != null &&
							ticketCoupon.getCouponNumber().equals(String.valueOf(eticketInfoTO.getExternalCouponNo())) &&
							!eticketInfoTO.getExternalCouponStatus().equals(TypeAConstants.CouponStatus.VOIDED);

			if (matchedByExternalData) {
				lccPaxEticket = eticketInfoTO;
				break;
			}
		}

		return lccPaxEticket;
	}


	public static <T extends TravellerInformationWrapper> Values2To<TravellerInformation, Traveller>
			resolveTraveller(TravellerInformation travellersGroup, Traveller traveller, Collection<T> travellersGroups) {
		Values2To<TravellerInformation, Traveller> matchingTraveller = null;
		TravellerInformation travellerInformation;

		for (TravellerInformationWrapper matchTravellerInformation : travellersGroups) {
			travellerInformation = matchTravellerInformation.getTravellerInformation();
			for (Traveller matchTraveller : travellerInformation.getTravellers()) {
				if (travellersGroup.getTravellerSurname().equals(travellerInformation.getTravellerSurname()) &&
						traveller.getGivenName().equals(matchTraveller.getGivenName())) {
					matchingTraveller = new Values2To<TravellerInformation, Traveller>();
					matchingTraveller.setM(travellerInformation);
					matchingTraveller.setN(matchTraveller);
				}
			}
		}

		return matchingTraveller;
	}


	public static String getSettleCurrency(List<MonetaryInformation> monetaryInformation) {
		String settleCurrency = null;

		String currencyTotal = null;
		String currencyAdditional = null;
		String currencyEquivalant = null;
		String currencyBase = null;

		if (monetaryInformation != null) {
			for (MonetaryInformation monetaryInfo : monetaryInformation) {
				if (monetaryInfo.getAmountTypeQualifier().equals(TypeAConstants.MonetaryType.TICKET_TOTAL)) {
					currencyTotal = monetaryInfo.getCurrencyCode();
				} else if (monetaryInfo.getAmountTypeQualifier().equals(TypeAConstants.MonetaryType.ADDITIONAL_COLLECTION_TOTAL)) {
					currencyAdditional = monetaryInfo.getCurrencyCode();
				} else if (monetaryInfo.getAmountTypeQualifier().equals(TypeAConstants.MonetaryType.EQUIVALENT_FARE)) {
					currencyEquivalant = monetaryInfo.getCurrencyCode();
				} else if (monetaryInfo.getAmountTypeQualifier().equals(TypeAConstants.MonetaryType.BASE_FARE)) {
					currencyBase = monetaryInfo.getCurrencyCode();
				}
			}
		}

		if (currencyTotal != null && !currencyTotal.isEmpty()) {
			settleCurrency = currencyTotal;
		} else if (currencyAdditional != null && !currencyAdditional.isEmpty()) {
			settleCurrency = currencyAdditional;
		} else if (currencyEquivalant != null && !currencyEquivalant.isEmpty()) {
			settleCurrency = currencyEquivalant;
		} else if (currencyBase != null && !currencyBase.isEmpty()) {
			settleCurrency = currencyBase;
		}

		return settleCurrency;
	}

	public static MonetaryInformation getMoneratoryAmount(List<MonetaryInformation> monetaryInformation, String amountQualifier) {
		MonetaryInformation monInfo = null;

		for (MonetaryInformation mon : monetaryInformation) {
			if (mon.getAmountTypeQualifier().equals(amountQualifier) && mon.getAmount() != null) {
				monInfo = mon;
			}
		}

		return monInfo;
	}

	public static boolean isPayment(List<MonetaryInformation> monetaryInformation){
		boolean isPayment = true;

		if (monetaryInformation != null) {
			for (MonetaryInformation monetaryInfo : monetaryInformation) {
				if (monetaryInfo.getAmountTypeQualifier().equals(TypeAConstants.MonetaryType.TICKET_TOTAL) &&
						monetaryInfo.getAmount().equals(TypeAConstants.TypeAKeywords.NO_ADDITIONAL_COLLECTION)) {
					isPayment = false;
				}
			}
		}

		return isPayment;
	}
}
