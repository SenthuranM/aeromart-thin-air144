package com.isa.thinair.gdsservices.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airproxy.api.model.reservation.commons.DisplayTickerRqDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketChangeOfStatusRQ;
import com.isa.thinair.gdsservices.api.dto.typea.init.TicketChangeOfStatusRS;
import com.isa.thinair.gdsservices.api.dto.typea.internal.ReservationRq;
import com.isa.thinair.gdsservices.api.dto.typea.internal.ReservationRs;
import com.isa.thinair.gdsservices.api.dto.typea.internal.TicketingEventRq;
import com.isa.thinair.gdsservices.api.dto.typea.internal.TicketingEventRs;
import com.isa.thinair.gdsservices.api.model.ExternalReservation;
import com.isa.thinair.gdsservices.core.bl.internal.reservation.ReservationModificationHandler;
import com.isa.thinair.gdsservices.core.bl.internal.reservation.ReservationModificationHandlerImpl;
import com.isa.thinair.gdsservices.core.bl.internal.ticket.TicketIssuanceHandler;
import com.isa.thinair.gdsservices.core.bl.internal.ticket.TicketIssuanceHandlerImpl;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSTypeAServiceDAO;
import com.isa.thinair.gdsservices.core.service.bd.GDSNotifyBDImpl;
import com.isa.thinair.gdsservices.core.service.bd.GDSNotifyBDLocalImpl;

@Stateless
@RemoteBinding(jndiBinding = "GDSNotifyService.remote")
@LocalBinding(jndiBinding = "GDSNotifyService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class GDSNotifyServiceBean extends PlatformBaseSessionBean implements GDSNotifyBDImpl, GDSNotifyBDLocalImpl {

	private static final String DESTINATION_JNDI_NAME_TYPEA_NOTIFIER = "queue/isaGDSTypeANotifierQueue";
	private static final String CONNECTION_FACTORY_JNDI_NAME = "ConnectionFactory";

	public TicketChangeOfStatusRS changeCouponStatus(TicketChangeOfStatusRQ ticketChangeOfStatusRQ) throws ModuleException {

		sendMessage(ticketChangeOfStatusRQ, DESTINATION_JNDI_NAME_TYPEA_NOTIFIER, CONNECTION_FACTORY_JNDI_NAME);

		TicketChangeOfStatusRS ticketChangeOfStatusRS = new TicketChangeOfStatusRS();
		return ticketChangeOfStatusRS;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public TicketingEventRs onTicketIssue(TicketingEventRq ticketIssueRq) throws ModuleException {
		TicketIssuanceHandler ticketIssuanceHandler;
		ticketIssuanceHandler = new TicketIssuanceHandlerImpl();
		return ticketIssuanceHandler.handleMonetaryDiscrepancies(ticketIssueRq);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ReservationRs onReservationModification(ReservationRq reservationRq) {
		ReservationRs modifyReservationRs = null;

		ReservationModificationHandler handler = new ReservationModificationHandlerImpl();

		switch (reservationRq.getEvent()) {
		case REMOVE_SEGMENT:
			modifyReservationRs = handler.removeSegments(reservationRq);
			break;
		case REMOVE_PASSENGER:
		case SPLIT_RESERVATION:
			modifyReservationRs = handler.splitResOrRemovePax(reservationRq);
			break;
		case CHANGE_PAX_DETAILS:
			modifyReservationRs = handler.changePaxDetails(reservationRq);
			break;
		}

		return modifyReservationRs;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void reservationLock(String pnr) throws ModuleException {

		GDSTypeAServiceDAO gdsTypeAServiceDAO = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO;

		ExternalReservation externalReservation = gdsTypeAServiceDAO.gdsTypeAServiceGet(ExternalReservation.class, pnr);

		try {
			gdsTypeAServiceDAO.gdsTypeAServiceLock(externalReservation, false);
		} catch (Exception e) {
			throw new ModuleException("cannot obtain lock");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void handleDisplayForModifiedReservation(DisplayTickerRqDTO displayTicketRq) throws ModuleException {
		
		
		GDSTypeAServiceDAO gdsTypeAServiceDAO = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO;
		ExternalReservation gdsExternalReservation = gdsTypeAServiceDAO.gdsTypeAServiceGet(ExternalReservation.class, displayTicketRq.getPnr());
		
		if(gdsExternalReservation.getPriceSynced().equals(CommonsConstants.NO)){
		
			TicketingEventRq ticketIssueRq = new TicketingEventRq();
			ticketIssueRq.setPnr(displayTicketRq.getPnr());
			ticketIssueRq.setGdsId(displayTicketRq.getGdsId());
			ticketIssueRq.setPaxTicketsCoupons(preparePaxTicketsCoupons(displayTicketRq));
			
			TicketIssuanceHandler ticketIssuanceHandler;
			ticketIssuanceHandler = new TicketIssuanceHandlerImpl();
			ticketIssuanceHandler.handleMonetaryDiscrepancies(ticketIssueRq);
			
		}
	}

	private Map<Integer, Map<String, List<Integer>>> preparePaxTicketsCoupons(DisplayTickerRqDTO displayTicketRq) {

		Map<Integer, Map<String, List<Integer>>> paxTicketCoupons = new HashMap<Integer, Map<String, List<Integer>>>();

		Set<LCCClientReservationPax> reservationPaxSet = displayTicketRq.getReservationPaxSet();
		for (LCCClientReservationPax reservationPax : reservationPaxSet) {

			Map<String, List<Integer>> ticketCouponMap = new HashMap<String, List<Integer>>();
			
			Collection<LccClientPassengerEticketInfoTO> eTickets = reservationPax.geteTickets();
			for (LccClientPassengerEticketInfoTO eTicket : eTickets) {
				List<Integer> coupons = new ArrayList<Integer>();
				coupons.add(eTicket.getExternalCouponNo());
				ticketCouponMap.put(eTicket.getPaxETicketNo(), coupons);
			}

			paxTicketCoupons.put(reservationPax.getPaxSequence(), ticketCouponMap);
		}

		return paxTicketCoupons;
	}

}
