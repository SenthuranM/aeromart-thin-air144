/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.core.bl.ssm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.AirportDSTHistory;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.dto.ReScheduleRequiredInfo;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.SSMSplitFlightSchedule;
import com.isa.thinair.airschedules.api.service.ScheduleBD;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.airschedules.api.utils.GDSScheduleEventCollector;
import com.isa.thinair.airschedules.api.utils.ScheduleBuildStatusEnum;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for sending SSM message for required schedules when Airport DST added/updated
 * 
 * @author M.Rikaz
 * @isa.module.command name="publishSSMDSTUpdate"
 */
public class PublishSSMDSTUpdate extends DefaultBaseCommand {

	private ScheduleBD scheduleBD;

	private AirportBD airportBD;

	private static Log log = LogFactory.getLog(PublishSSMDSTUpdate.class);

	private Map<String, GDSStatusTO> gdsStatusMap;

	private Map<String, String> aircraftTypeMap;

	private String airLineCode;

	private Map<String, Collection<String>> gdsMappedBookingClass;

	/**
	 * constructor of the PublishDSTUpdateSSM command
	 */
	public PublishSSMDSTUpdate() {

		scheduleBD = GDSServicesModuleUtil.getScheduleBD();

		airportBD = GDSServicesModuleUtil.getAirportBD();
	}

	@Override
	public ServiceResponce execute() throws ModuleException {

		GDSScheduleEventCollector sGdsEvents = (GDSScheduleEventCollector) this
				.getParameter(GDSSchedConstants.ParamNames.GDS_EVENT_COLLECTOR);

		Collection<String> airports = null;
		if (AppSysParamsUtil.isEnableSplitScheduleWhenDSTApplicable()
				&& (sGdsEvents != null && sGdsEvents.containsAction(GDSScheduleEventCollector.AIRPORT_DST_CHANGED))) {
			airports = sGdsEvents.getAirports();
			for (String airport : airports) {
				// load unprocessed dst
				Collection<AirportDSTHistory> airportDSTHistory = airportBD.getUnPublishedAirportDSTHistory(airport);
				airportDSTHistory = sortDSTList(new ArrayList<AirportDSTHistory>(airportDSTHistory));
				sendScheduleUpdateMessage(airportDSTHistory);
			}
		}

		// constructing response
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		// return command response
		return responce;
	}

	
	private void sendScheduleUpdateMessage(Collection<AirportDSTHistory> airportDSTHistory) throws ModuleException {
		if (airportDSTHistory != null && !airportDSTHistory.isEmpty()) {
			this.gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
			this.aircraftTypeMap = GDSServicesModuleUtil.getGlobalConfig().getIataAircraftCodes();
			this.airLineCode = GDSServicesModuleUtil.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
			this.gdsMappedBookingClass = getSingleEntryMappedGDSBookingClassesByGds();

			for (AirportDSTHistory airportDST : airportDSTHistory) {
				Collection<FlightSchedule> schedules = scheduleBD.getDSTApplicableFutureSchedules(airportDST);

				if (schedules != null && !schedules.isEmpty()) {
					for (FlightSchedule schedule : schedules) {
						splitScheduleAndPublishSSMForChanges(schedule);
					}
				}
				airportBD.updateAirportDSTHistoryStatus(airportDST.getDstCode(), AirportDSTHistory.PROCESSED);
			}
		}
	}

	private Collection<AirportDSTHistory> sortDSTList(List<AirportDSTHistory> dstList) {
		Collections.sort(dstList, new Comparator<AirportDSTHistory>() {
			public int compare(AirportDSTHistory o1, AirportDSTHistory o2) {
				return o1.getDstEndDateTime().compareTo(o2.getDstEndDateTime());
			}
		});
		return dstList;
	}

	@SuppressWarnings({ "unchecked" })
	private void splitScheduleAndPublishSSMForChanges(FlightSchedule schedule) throws ModuleException {

		Collection<SSMSplitFlightSchedule> existingSubSchedules = schedule.getSsmSplitFlightSchedule();
		ServiceResponce serviceResponce = GDSServicesModuleUtil.getScheduleBD().splitSubScheduleForSSM(schedule);
		Collection<SSMSplitFlightSchedule> updatedSubSchedules = null;
		if (serviceResponce.isSuccess()) {
			updatedSubSchedules = (Collection<SSMSplitFlightSchedule>) serviceResponce
					.getResponseParam(GDSSchedConstants.ParamNames.NEW_SSM_SUB_SCHEDULES);
			existingSubSchedules = (Collection<SSMSplitFlightSchedule>) serviceResponce
					.getResponseParam(GDSSchedConstants.ParamNames.EXISITNG_SSM_SUB_SCHEDULES);

			FlightSchedule existingSchedule = (FlightSchedule) serviceResponce
					.getResponseParam(GDSSchedConstants.ParamNames.FLIGHT_SCHEDULE);

			if (ScheduleBuildStatusEnum.BUILT.getCode().equals(existingSchedule.getBuildStatusCode())) {
				ReScheduleRequiredInfo reScheduleInfo = new ReScheduleRequiredInfo();
				reScheduleInfo.setGdsStatusMap(this.gdsStatusMap);
				reScheduleInfo.setAircraftTypeMap(this.aircraftTypeMap);
				reScheduleInfo.setServiceType(null);
				reScheduleInfo.setAirLineCode(this.airLineCode);
				reScheduleInfo.setPublishedGdsIds(schedule.getGdsIds());
				reScheduleInfo.setBcMap(this.gdsMappedBookingClass);
				reScheduleInfo.setExistingSchedule(existingSchedule);
				reScheduleInfo.setUpdatedSchedule(schedule);
				reScheduleInfo.setExistingSubSchedules(existingSubSchedules);
				reScheduleInfo.setUpdatedSubSchedules(updatedSubSchedules);
				reScheduleInfo.setCancelExistingSubSchedule(AppSysParamsUtil.isCancelPreviousScheduleOnDSTChanges());

				GDSServicesModuleUtil.getSchedulePublishBD().publishSSMMessageForScheduleChanges(reScheduleInfo);

			}

		}

	}

	private Map<String, Collection<String>> getSingleEntryMappedGDSBookingClassesByGds() {
		Map<Integer, List<String>> gdsMappedBookingClass = CommonsServices.getGlobalConfig()
				.getSingleEntryMappedGDSBookingClasses();
		Map<String, Collection<String>> bcMap = new HashMap<String, Collection<String>>();
		if (gdsMappedBookingClass != null && !gdsMappedBookingClass.isEmpty()) {
			for (Integer gdsId : gdsMappedBookingClass.keySet()) {
				bcMap.put(gdsId.toString(), gdsMappedBookingClass.get(gdsId));
			}
		}

		return bcMap;
	}

}
