package com.isa.thinair.gdsservices.core.typeb.transformers;

import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.NameDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRChildDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRInfantDTO;
import com.isa.thinair.gdsservices.api.dto.internal.ChangePaxDetailsRequest;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ActionCode;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.AdviceCode;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ProcessStatus;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.StatusCode;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.core.typeb.validators.ValidatorUtils;

public class ChangePaxDetailsResponseTransformer {

	/**
	 * transforms reservationResponseMap back to bookingRequestDTO
	 * 
	 * @param bookingRequestDTO
	 * @param reservationResponseMap
	 * @return
	 */
	public static BookingRequestDTO getResponse(BookingRequestDTO bookingRequestDTO,
			Map<ReservationAction, GDSReservationRequestBase> reservationResponseMap) {

		ChangePaxDetailsRequest changePaxDetailRequest = (ChangePaxDetailsRequest) reservationResponseMap
				.get(ReservationAction.CHANGE_PAX_DETAILS);

		if (changePaxDetailRequest.isSuccess()) {
			if (reservationResponseMap.size() < 3 && !changePaxDetailRequest.isNameChangeExists()) {
				bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_IGNORED);
			} else {
				bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_SUCCESS);
				bookingRequestDTO = removeChangeNamesDetails(bookingRequestDTO);
				bookingRequestDTO = updateSSRElementStatus(bookingRequestDTO);
			}
		} else {
			bookingRequestDTO.setProcessStatus(ProcessStatus.INVOCATION_FAILURE);
			bookingRequestDTO = ValidatorUtils.composeNoActionTakenResponse(bookingRequestDTO, changePaxDetailRequest);
		}
		bookingRequestDTO.getErrors().addAll(changePaxDetailRequest.getErrorCode());


		return bookingRequestDTO;
	}

	/**
	 * Remove ChangeNames Details
	 * 
	 * @param bookingRequestDTO
	 */
	private static BookingRequestDTO removeChangeNamesDetails(BookingRequestDTO bookingRequestDTO) {

		if (bookingRequestDTO.getChangedNameDTOs() != null && !bookingRequestDTO.getChangedNameDTOs().isEmpty()) {
			List<NameDTO> newNameDTOs = bookingRequestDTO.getChangedNameDTOs().get(0).getNewNameDTOs();
			bookingRequestDTO.setChangedNameDTOs(null);
			if (bookingRequestDTO.getNewNameDTOs() != null && !bookingRequestDTO.getNewNameDTOs().isEmpty()) {
				bookingRequestDTO.getNewNameDTOs().addAll(newNameDTOs);
			} else {
				bookingRequestDTO.setNewNameDTOs(newNameDTOs);
			}
		}
		return bookingRequestDTO;
	}
	
	private static BookingRequestDTO updateSSRElementStatus(BookingRequestDTO bookingRequestDTO) {
		List<SSRDTO> ssrDTOS = bookingRequestDTO.getSsrDTOs();
		for (SSRDTO ssrDto : ssrDTOS) {
			if (ssrDto instanceof SSRInfantDTO) {
				SSRInfantDTO ssrInfant = (SSRInfantDTO) ssrDto;
				String actionCode = ssrInfant.getActionOrStatusCode();
				if (actionCode.equals(ActionCode.CANCEL.getCode())) {
					ssrInfant.setAdviceOrStatusCode(AdviceCode.CANCELLED.getCode());
				} else if (actionCode.equals(ActionCode.NEED.getCode())) {
					ssrInfant.setAdviceOrStatusCode(AdviceCode.CONFIRMED.getCode());
				} else if (actionCode.equals(StatusCode.HOLDS_CONFIRMED.getCode())) {
					ssrInfant.setAdviceOrStatusCode(StatusCode.HOLDS_CONFIRMED.getCode());
				}
			} else if (ssrDto instanceof SSRChildDTO) {
				SSRChildDTO ssrChild = (SSRChildDTO) ssrDto;
				String actionCode = ssrChild.getActionOrStatusCode();
				if (actionCode.equals(ActionCode.CANCEL.getCode())) {
					ssrChild.setAdviceOrStatusCode(AdviceCode.CANCELLED.getCode());
				} else if (actionCode.equals(ActionCode.NEED.getCode())) {
					ssrChild.setAdviceOrStatusCode(AdviceCode.CONFIRMED.getCode());
				} else if (actionCode.equals(StatusCode.HOLDS_CONFIRMED.getCode())) {
					ssrChild.setAdviceOrStatusCode(StatusCode.HOLDS_CONFIRMED.getCode());
				} else if (actionCode.equals(StatusCode.CODESHARE_CONFIRMED.getCode())) {
					ssrChild.setAdviceOrStatusCode(StatusCode.CODESHARE_CONFIRMED.getCode());
				}
			} else {
				String gdsActionCode = ssrDto.getActionOrStatusCode();
				String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

				if (carrierCode.equals(ssrDto.getCarrierCode())) {
					if (gdsActionCode != null) {
						if (gdsActionCode.equals(GDSExternalCodes.ActionCode.NEED.getCode())
								|| gdsActionCode.equals(GDSExternalCodes.ActionCode.SOLD.getCode())
								|| gdsActionCode.equals(GDSExternalCodes.StatusCode.HOLDS_CONFIRMED.getCode())
								|| gdsActionCode.equals(GDSExternalCodes.StatusCode.CODESHARE_CONFIRMED.getCode())
								|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CODESHARE_SELL.getCode())) {
							ssrDto.setAdviceOrStatusCode(AdviceCode.CONFIRMED.getCode());
						} else if (gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL.getCode())
								|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_IF_AVAILABLE.getCode())
								|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_IF_HOLDING.getCode())
								|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CANCEL_RECOMMENDED.getCode())
								|| gdsActionCode.equals(GDSExternalCodes.ActionCode.CODESHARE_CANCEL.getCode())) {
							ssrDto.setAdviceOrStatusCode(AdviceCode.CANCELLED.getCode());
						} else if (gdsActionCode.equals(GDSExternalCodes.ActionCode.EXCHANGE_RECOMMENDED.getCode())) {
							ssrDto.setAdviceOrStatusCode(AdviceCode.CONFIRMED.getCode());
						}
					}
				}
			}

		}

		return bookingRequestDTO;
	}

}
