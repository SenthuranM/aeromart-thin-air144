/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.core.bl.ssm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.NotifyFlightEventRQ;
import com.isa.thinair.airinventory.api.dto.NotifyFlightEventsRQ;
import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.model.SSMSplitFlightSchedule;
import com.isa.thinair.airschedules.api.service.ScheduleBD;
import com.isa.thinair.airschedules.api.utils.FlightEventCode;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.XMLStreamer;
import com.isa.thinair.gdsservices.api.model.GDSPublishMessage;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSServicesDAO;
import com.isa.thinair.msgbroker.api.dto.SSIMessegeDTO;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for integrate create schedule with gds
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="publishSSMNew"
 */
public class PublishSSMNew extends DefaultBaseCommand {

	// BDs
	private ScheduleBD scheduleBD;

	private BookingClassBD bookingClassBD;

	private FlightInventoryBD flightInventoryBD;

	// Dao's
	private GDSServicesDAO gdsServiceDao;

	/**
	 * constructor of the IntegrateSSMNew command
	 */
	public PublishSSMNew() {

		// looking up BDs
		scheduleBD = GDSServicesModuleUtil.getScheduleBD();

		bookingClassBD = GDSServicesModuleUtil.getBookingClassBD();

		flightInventoryBD = GDSServicesModuleUtil.getFlightInventoryBD();

		// lokking up DAOs
		gdsServiceDao = GDSServicesDAOUtils.DAOInstance.GDSSERVICES_DAO;
	}

	/**
	 * execute method of the IntegrateSSMNew command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceResponce execute() throws ModuleException {

		// String serviceType = (String) this.getParameter(GDSSchedConstants.ParamNames.SERVICE_TYPE);
		HashMap buildFlightIds = (HashMap) this.getParameter(GDSSchedConstants.ParamNames.FLIGHT_IDS_MAP_FOR_SCHEDULES);
		StringBuilder supplementaryInfo = (StringBuilder) this.getParameter(GDSSchedConstants.ParamNames.SUPPLEMENTARY_INFO);

		Map<String, GDSStatusTO> gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
		Map<String, String> aircraftTypeMap = GDSServicesModuleUtil.getGlobalConfig().getIataAircraftCodes();
		String airLineCode = GDSServicesModuleUtil.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);

		if(supplementaryInfo==null){
			supplementaryInfo = new StringBuilder();
		}
		
		// building each schedule
		Iterator<Integer> it = buildFlightIds.keySet().iterator();
		while (it.hasNext()) {

			// get the flight schedule to build
			Integer scheduleId = it.next();
			FlightSchedule schedule = scheduleBD.getFlightSchedule(scheduleId);

			Collection<Integer> gdsIds = schedule.getGdsIds();
			Map<String, Collection<String>> bcMap = bookingClassBD.getBookingClassCodes(gdsIds);

			if (schedule.getGdsIds() != null && schedule.getGdsIds().size() > 0) {
				ArrayList<Integer> activeGdses = new ArrayList<Integer>();
				Collection<SSMSplitFlightSchedule> scheduleColl = schedule.getSsmSplitFlightSchedule();
				String siOverlapElement = getSupplementaryOverlapElement(schedule);
				if(siOverlapElement.length()>6){
					supplementaryInfo.append(siOverlapElement);
				}
				if (scheduleColl != null && !scheduleColl.isEmpty()) {
					for (SSMSplitFlightSchedule subSchedule : scheduleColl) {
						generateSSMNewMsg(gdsStatusMap, aircraftTypeMap, schedule, airLineCode, bcMap, subSchedule, activeGdses,
								supplementaryInfo.toString());
					}
				} else {
					generateSSMNewMsg(gdsStatusMap, aircraftTypeMap, schedule, airLineCode, bcMap, null, activeGdses,
							supplementaryInfo.toString());
				}

				// notifiy flight events to inventory to send corresponding avs messages
				if (activeGdses.size() > 0) {
					NotifyFlightEventsRQ notifyFlightEventsRQ = new NotifyFlightEventsRQ();
					NotifyFlightEventRQ newFlightEnvent = new NotifyFlightEventRQ();
					newFlightEnvent.setFlightEventCode(FlightEventCode.FLIGHT_CREATED);
					newFlightEnvent.addFlightIds((Collection) buildFlightIds.get(scheduleId));
					newFlightEnvent.setGdsIdsAdded(activeGdses);
					notifyFlightEventsRQ.addNotifyFlightEventRQ(newFlightEnvent);

					flightInventoryBD.notifyFlightEvent(notifyFlightEventsRQ);
				}
			}
		}

		// nothing to return, return empty response
		// constructing response
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		// return command response
		return response;
	}

	private void generateSSMNewMsg(Map<String, GDSStatusTO> gdsStatusMap, Map<String, String> aircraftTypeMap,
			FlightSchedule schedule, String airLineCode, Map<String, Collection<String>> bcMap,
			SSMSplitFlightSchedule subSchedule, ArrayList<Integer> activeGdses, String supplementaryInfo) throws ModuleException {

		SSIMessegeDTO ssmDto = new SSIMessegeDTO();
		String aircraftType = aircraftTypeMap.get(schedule.getModelNumber());

		SchedulePublishUtil.populateSSIMessageDTOforNEW(ssmDto, schedule, null, aircraftType, subSchedule, supplementaryInfo);
		Collection<Integer> gdsIds = schedule.getGdsIds();

		Iterator<Integer> itGdsIds = gdsIds.iterator();
		while (itGdsIds.hasNext()) {

			Integer gdsId = itGdsIds.next();
			SchedulePublishUtil.addRBDInfo(ssmDto, bcMap.get(gdsId.toString()));
			GDSStatusTO gdsStatus = gdsStatusMap.get(gdsId.toString());

			GDSPublishMessage pubMessage = SchedulePublishUtil.createSSMSuccessPublishLog(gdsId, XMLStreamer.compose(ssmDto),
					ssmDto.getReferenceNumber(), schedule.getScheduleId());

			if (GDSInternalCodes.GDSStatus.ACT.getCode().equals(gdsStatus.getStatus())) {
				activeGdses.add(gdsId);
				try {
					// publish the ssm message to GDS
					// ReservationToGDSMessageSenderBD gdsSender = CommonPublishUtil.getGDSSender();
					GDSServicesModuleUtil.getSSMASMServiceBD().sendStandardScheduleMessage(gdsStatus.getGdsCode(), airLineCode,
							ssmDto);
					gdsServiceDao.saveGDSPublisingMessage(pubMessage);
				} catch (Exception e) {
					pubMessage.setRemarks("Message broker invocation failed");
					pubMessage.setStatus(GDSPublishMessage.MESSAGE_FAILED);
					gdsServiceDao.saveGDSPublisingMessage(pubMessage);
				}
			} else {
				pubMessage.setStatus(GDSPublishMessage.GDS_INACTIVE);
				gdsServiceDao.saveGDSPublisingMessage(pubMessage);
			}
		}

	}
	
	private String getSupplementaryOverlapElement(FlightSchedule schedule) throws ModuleException {
		
		StringBuilder siOLAP = new StringBuilder();
		if (schedule.getOverlapingScheduleId() != null){
			FlightSchedule overlapPingSchedule = scheduleBD.getFlightSchedule(schedule.getOverlapingScheduleId());
			if(overlapPingSchedule.getGdsIds() != null ){
				
				Collection<FlightScheduleSegment> flightScheduleSegments = schedule.getFlightScheduleSegments();
				int i = 0;
				siOLAP.append("OLAP ");
				for (FlightScheduleSegment segment : flightScheduleSegments) {
					if (segment.getOverlapSegmentId()== null){
						if(i>0){
							siOLAP.append("#");
						}
						siOLAP.append(segment.getSegmentCode());
						i++;
					}
				}
			}
		}

		return siOLAP.toString();
	}
}
