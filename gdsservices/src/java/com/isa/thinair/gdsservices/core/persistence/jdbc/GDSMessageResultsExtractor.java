package com.isa.thinair.gdsservices.core.persistence.jdbc;

/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.gdsservices.api.model.GDSMessage;

/**
 * Class that JdbcTemplate will use.
 * 
 * @author Dhanushka
 * 
 */
public class GDSMessageResultsExtractor implements ResultSetExtractor {

	private static final String MSG_ID = "MSG_ID";

	private static final String MSG_TYPE = "MSG_TYPE";

	private static final String AA_OBJ_ID = "AA_OBJ_ID";

	private static final String MSG_RECEIVED_DATE = "MSG_RECEIVED_DATE";

	private static final String COMMUNICATION_REFERENCE = "COMMUNICATION_REFERENCE";

	private static final String ORIGINATOR_ADDRESS = "ORIGINATOR_ADDRESS";

	private static final String RESPONDER_ADDRESS = "RESPONDER_ADDRESS";

	private static final String GDS_NAME = "GDS_NAME";

	private static final String PROCESS_STATUS = "PROCESS_STATUS";

	private static final String STATUS_HISTORY = "STATUS_HISTORY";

	private static final String PROCESS_DESCRIPTION = "PROCESS_DESCRIPTION";

	private static final String VERSION = "VERSION";
	private static final String GDS_PNR = "GDS_PNR";
	private static final String GDS_AGENT_CODE = "GDS_AGENT_CODE";
	private static final String GDS_USER_ID = "GDS_USER_ID";
	private static final String MSG_BROKER_ID = "MSG_BROKER_ID";

	private Collection<GDSMessage> messages;

	/**
	 * @return the messages
	 */
	public Collection<GDSMessage> getMessages() {
		return messages;
	}

	/**
	 * Method that implements the ResultSetExtractor interface. This method is internally used by the JdbcTemplate.
	 * Method is used for accessing the resultSet and populating the DTO and adding it to the results LIst.
	 */
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

		messages = new ArrayList();
		GDSMessage gdsMessage;

		while (rs.next()) {
			gdsMessage = new GDSMessage();
			gdsMessage.setMessageId(rs.getLong(MSG_ID));
			gdsMessage.setMessageType(rs.getString(MSG_TYPE));
			gdsMessage.setUniqueAccelAeroRequestId(rs.getString(AA_OBJ_ID));

			Date tempDate = rs.getDate(MSG_RECEIVED_DATE);
			Time tempTime = rs.getTime(MSG_RECEIVED_DATE);

			Calendar cal = Calendar.getInstance();
			cal.setTime(tempDate);
			cal.set(Calendar.HOUR, tempTime.getHours());
			cal.set(Calendar.MINUTE, tempTime.getMinutes());
			cal.set(Calendar.SECOND, tempTime.getSeconds());

			gdsMessage.setMessageReceivedDate(cal.getTime());

			gdsMessage.setCommunicationReference(rs.getString(COMMUNICATION_REFERENCE));
			gdsMessage.setOriginatorAddress(rs.getString(ORIGINATOR_ADDRESS));
			gdsMessage.setResponderAddress(rs.getString(RESPONDER_ADDRESS));
			gdsMessage.setGds(rs.getString(GDS_NAME));
			gdsMessage.setProcessStatus(rs.getString(PROCESS_STATUS));
			gdsMessage.setProcessStatusHistory(rs.getString(STATUS_HISTORY));
			gdsMessage.setProcessDescription(rs.getString(PROCESS_DESCRIPTION));
			gdsMessage.setVersion(rs.getLong(VERSION));
			gdsMessage.setGdsPnr(rs.getString(GDS_PNR));
			gdsMessage.setGdsAgentCode(rs.getString(GDS_AGENT_CODE));
			gdsMessage.setGdsUserId(rs.getString(GDS_USER_ID));
			gdsMessage.setMessageBrokerReferenceId(rs.getString(MSG_BROKER_ID));

			messages.add(gdsMessage);
		}

		return messages;
	}
}