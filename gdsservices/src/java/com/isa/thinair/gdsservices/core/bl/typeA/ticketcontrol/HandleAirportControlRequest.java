package com.isa.thinair.gdsservices.core.bl.typeA.ticketcontrol;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegmentETicket;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageHandler;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.dto.temp.GdsReservation;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerTicketControlInformationWrapper;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.typeA.transformers.ReservationDTOsTransformer;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.gdsservices.core.util.GdsInfoDaoHelper;
import com.isa.thinair.gdsservices.core.util.GdsUtil;
import com.isa.thinair.gdsservices.core.util.TypeANotes;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.typea.common.AdditionalTourInformation;
import com.isa.typea.common.ConsumerRefInformation;
import com.isa.typea.common.DocumentInformationDetails;
import com.isa.typea.common.ErrorInformation;
import com.isa.typea.common.FormOfPayment;
import com.isa.typea.common.FrequentTravellerInformation;
import com.isa.typea.common.InteractiveFreeText;
import com.isa.typea.common.Location;
import com.isa.typea.common.MessageFunction;
import com.isa.typea.common.MonetaryInformation;
import com.isa.typea.common.NumberOfUnits;
import com.isa.typea.common.OriginDestinationInformation;
import com.isa.typea.common.OriginatorInformation;
import com.isa.typea.common.PricingTicketingDetails;
import com.isa.typea.common.ReservationControlInformation;
import com.isa.typea.common.SystemDetails;
import com.isa.typea.common.TaxDetails;
import com.isa.typea.common.TicketControlTicketNumberDetails;
import com.isa.typea.common.TicketCoupon;
import com.isa.typea.common.TicketNumberDetails;
import com.isa.typea.common.TicketingAgentInformation;
import com.isa.typea.common.TravellerTicketControlInformation;
import com.isa.typea.exception.TypeAException;
import com.isa.typea.tkcreq.AATKCREQ;
import com.isa.typea.tkcreq.TKCREQMessage;
import com.isa.typea.tkcres.AATKCRES;
import com.isa.typea.tkcres.TKCRESMessage;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HandleAirportControlRequest extends EdiMessageHandler {

	private AATKCREQ tkcReq;
	private AATKCRES tkcRes;

	protected void handleMessage() {

		tkcReq = (AATKCREQ) messageDTO.getRequest();
		TKCREQMessage tkcreqMessage = tkcReq.getMessage();

		tkcRes = new AATKCRES();
		TKCRESMessage tktresMessage = new TKCRESMessage();
		tktresMessage.setMessageHeader(GDSDTOUtil.createRespMessageHeader(tkcreqMessage.getMessageHeader(),
				messageDTO.getRequestMetaDataDTO().getMessageReference(), messageDTO.getRequestMetaDataDTO().getIataOperation().getResponse()));


		tkcRes.setHeader(GDSDTOUtil.createRespEdiHeader(tkcReq.getHeader(), tktresMessage.getMessageHeader(), messageDTO));
		tkcRes.setMessage(tktresMessage);

		MessageFunction messageFunction = new MessageFunction();
		messageFunction.setMessageFunction(TypeAConstants.MessageFunction.AIRPORT_CONTROL);
		messageFunction.setResponseType(TypeAConstants.ResponseType.PROCESSED_SUCCESSFULLY);
		tktresMessage.setMessageFunction(messageFunction);

		String pnr;
		String ticketNumber;
		Map<Integer, String> coupons;

		GdsReservation<TravellerTicketControlInformationWrapper> gdsReservation;

		List<TicketNumberDetails> errors;
		TicketNumberDetails errorDetails;
		List<TravellerTicketControlInformation> success;
		TravellerTicketControlInformation successDetails;

		try {

			OriginatorInformation originatorInformation = GDSDTOUtil.createOriginatorInformation();
			Location location = new Location();
			location.setLocationCode(AppSysParamsUtil.getHubAirport());

			SystemDetails systemDetails = new SystemDetails();
			systemDetails.setCompanyCode(AppSysParamsUtil.getDefaultCarrierCode());
			systemDetails.setLocation(location);
			originatorInformation.setSenderSystemDetails(systemDetails);
			originatorInformation.setAgentCurrencyCode(AppSysParamsUtil.getBaseCurrency());

			originatorInformation.setOriginatorAuthorityCode("SYSTEM");

			if (tkcreqMessage.getOriginatorInformation().getSenderSystemDetails().getCompanyCode() != null) {
				Gds gds = GDSServicesModuleUtil.getGdsBD().getGDSByCode(tkcreqMessage.getOriginatorInformation().getSenderSystemDetails().getCompanyCode());
				originatorInformation.setAgentId(AppSysParamsUtil.getGdsConnectivityAgent(gds.getGdsCode()));
			}

			tktresMessage.setOriginatorInformation(originatorInformation);

			errors = new ArrayList<>();
			success = new ArrayList<>();


			List<String> ticketNumbers = new ArrayList<>();
			Map<String, Map<Integer, String>> couponStates = new HashMap<>();
			for (TicketControlTicketNumberDetails tkt : tkcreqMessage.getTickets()) {
				ticketNumbers.add(tkt.getTicketNumber());
				couponStates.put(tkt.getTicketNumber(), new HashMap<>());

				for (TicketCoupon cpn : tkt.getTicketCoupon()) {
					couponStates.get(tkt.getTicketNumber()).put(Integer.valueOf(cpn.getCouponNumber()), cpn.getStatus());
				}

			}
			Map<String, Map<Integer, List<String>>> ticketGrouping =
					GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO.groupTickets(ticketNumbers);


			for (Map.Entry<String, Map<Integer, List<String>>> res : ticketGrouping.entrySet()) {

				pnr = res.getKey();
				gdsReservation = GdsInfoDaoHelper.getGdsReservationTicketControlView(pnr);

				for (Map.Entry<Integer, List<String>> pax : res.getValue().entrySet()) {

					for (String tkt : pax.getValue()) {
						TravellerTicketControlInformationWrapper travellerWrapper =
								GDSDTOUtil.resolveTicketControlPassengerByTicketNumber(gdsReservation, tkt);

						try {
							processStatusTransition(pnr, tkt, couponStates.get(tkt), travellerWrapper);
							successDetails = prepareSuccessResponse(travellerWrapper, gdsReservation.getReservationControlInformation(), tkt, couponStates.get(tkt));
							success.add(successDetails);
						} catch (GdsTypeAException e) {
							errorDetails = prepareErrorResponse(tkt, e);
							errors.add(errorDetails);
						}

					}

				}

				GdsInfoDaoHelper.saveGdsReservationTicketControlView(pnr, gdsReservation);

			}

			tktresMessage.getTicketNumberDetails().addAll(errors);
			tktresMessage.getTravellers().addAll(success);

			messageDTO.setResponse(tkcRes);
		} catch (Exception e) {

			// rectify this

		}
	}

	private void processStatusTransition(String pnr, String extETicketNumber, Map<Integer, String> couponNumbers,
			TravellerTicketControlInformationWrapper travellerWrapper) throws GdsTypeAException {

		Collection<Integer> processedCoupons = new HashSet<>();
		List<LccClientPassengerEticketInfoTO> updatedCoupons = new ArrayList<>();

		TicketCoupon ticketCoupon;
		ServiceResponce serviceResponce;
		List<EticketTO> coupons;

		try {
			LCCClientReservation reservation = GDSServicesUtil.loadLccReservation(pnr);

			for (LCCClientReservationPax pax : reservation.getPassengers()) {
				for (LccClientPassengerEticketInfoTO coupon : pax.geteTickets()) {
					if (coupon.getExternalPaxETicketNo().equals(extETicketNumber) &&
							couponNumbers.containsKey(coupon.getExternalCouponNo())) {

						processedCoupons.add(coupon.getExternalCouponNo());

						// todo ---- if !open && !seg_cnx && !exch -- throw exc
						if (true) {

							coupon.setExternalCouponStatus(couponNumbers.get(coupon.getExternalCouponNo()));
							coupon.setExternalCouponControl(ReservationPaxFareSegmentETicket.ExternalCouponControl.MARKETING_CARRIER);

							updatedCoupons.add(coupon);
						}
					}
				}
			}

			coupons = ReservationDTOsTransformer.toETicketTos(updatedCoupons);
			serviceResponce = GDSServicesModuleUtil.getReservationBD().updateExternalEticketInfo(coupons);

			for (Map.Entry<Integer, String> entry : couponNumbers.entrySet()) {
				ticketCoupon = GDSDTOUtil.resolveCoupon(travellerWrapper.getTravellerInformation().getTickets(), extETicketNumber, entry.getKey());
				ticketCoupon.setStatus(entry.getValue());
			}

			if (!serviceResponce.isSuccess()) {
				throw new ModuleException(TypeANotes.ErrorMessages.NO_MESSAGE);
			}

		} catch (ModuleException e) {

		}

	}

	private TravellerTicketControlInformation prepareSuccessResponse(TravellerTicketControlInformationWrapper travellerWrapper,
			Collection<ReservationControlInformation> pnrList, String eTicketNumber, Map<Integer, String> couponNumbers) {
		TravellerTicketControlInformation traveller = new TravellerTicketControlInformation();

		TravellerTicketControlInformation originalTraveller = travellerWrapper.getTravellerInformation();

		traveller.setTravellerSurname(originalTraveller.getTravellerSurname());
		traveller.setTravellerQualifier(originalTraveller.getTravellerQualifier());
		traveller.getTravellers().addAll(originalTraveller.getTravellers());
		traveller.setTicketingAgentInformation(originalTraveller.getTicketingAgentInformation());
		traveller.getReservationControlInformation().addAll(pnrList);
		traveller.getMonetaryInformation().addAll(originalTraveller.getMonetaryInformation());
		traveller.getFormOfPayment().addAll(originalTraveller.getFormOfPayment());
		traveller.setPricingTicketingDetails(originalTraveller.getPricingTicketingDetails());
		traveller.setOriginDestinationInfo(originalTraveller.getOriginDestinationInfo());
		traveller.setFrequentTravellerInfo(originalTraveller.getFrequentTravellerInfo());
		traveller.setAdditionalTourInfo(originalTraveller.getAdditionalTourInfo());
		traveller.setOriginatorInformation(originalTraveller.getOriginatorInformation());
		traveller.setNumberOfUnits(originalTraveller.getNumberOfUnits());
		traveller.getTaxDetails().addAll(originalTraveller.getTaxDetails());
		traveller.setDocumentInfo(originalTraveller.getDocumentInfo());
		traveller.getConsumerRef().addAll(originalTraveller.getConsumerRef());
		traveller.getText().addAll(originalTraveller.getText());

		TicketControlTicketNumberDetails ticketNumberDetails;
		TicketCoupon ticketCoupon;

		for (TicketControlTicketNumberDetails tkt : originalTraveller.getTickets()) {
			ticketNumberDetails = new TicketControlTicketNumberDetails();
			ticketNumberDetails.setDocumentType(tkt.getDocumentType());
			ticketNumberDetails.setTicketNumber(tkt.getTicketNumber());

			for (TicketCoupon cpn : tkt.getTicketCoupon()) {
				ticketCoupon = new TicketCoupon();
				ticketCoupon.setCouponNumber(cpn.getCouponNumber());

				if (tkt.getTicketNumber().equals(eTicketNumber) && couponNumbers.containsKey(Integer.valueOf(cpn.getCouponNumber()))) {
					ticketCoupon.setStatus(couponNumbers.get(Integer.valueOf(cpn.getCouponNumber())));
				} else {
					ticketCoupon.setStatus(TypeAConstants.CouponStatus.NOTIFIED);
				}

				ticketCoupon.setFlightInfomation(cpn.getFlightInfomation());
				ticketCoupon.setRelatedProductInfo(cpn.getRelatedProductInfo());
				ticketCoupon.setPricingTicketingSubsequent(cpn.getPricingTicketingSubsequent());
				ticketCoupon.setExcessBaggageInformation(cpn.getExcessBaggageInformation());
				ticketCoupon.setFrequentTravellerInformation(cpn.getFrequentTravellerInformation());
				ticketCoupon.setDateAndTimeInformation(cpn.getDateAndTimeInformation());
				ticketCoupon.setPricingTicketingDetails(cpn.getPricingTicketingDetails());

				ticketNumberDetails.getTicketCoupon().add(ticketCoupon);

			}
			traveller.getTickets().add(ticketNumberDetails);
		}

		return traveller;
	}

	private TicketNumberDetails prepareErrorResponse(String ticketNumber, GdsTypeAException e) {

		TicketNumberDetails errorTkt = new TicketNumberDetails();
		errorTkt.setDocumentType(TypeAConstants.DocumentType.TICKET);
		errorTkt.setTicketNumber(ticketNumber);

		ErrorInformation errorInformation  = new ErrorInformation();
		errorInformation.setApplicationErrorCode(e.getEdiErrorCode());
		errorTkt.setErrorInformation(errorInformation);

		return errorTkt;
	}
}
