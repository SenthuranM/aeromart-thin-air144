package com.isa.thinair.gdsservices.core.typeA.transformers;

import java.util.Map;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ddtek.xmlconverter.exception.ConverterException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.core.typeA.converters.Convert;
import com.isa.thinair.gdsservices.core.typeA.marshallers.Marshaller;
import com.isa.thinair.gdsservices.core.typeA.model.IataOperationConfig;
import com.isa.thinair.gdsservices.core.typeA.model.InterchangeHeader;
import com.isa.thinair.gdsservices.core.typeA.model.MessageHeader;
import com.isa.thinair.gdsservices.core.typeA.transformers.contrl.v02.Contrl;

public class Transformer {

	private final static Log log = LogFactory.getLog(Transformer.class);

	public static Map<String, Object> getMessageParams(IATARequest requestHandler, InterchangeHeader interchangeHeader,
			MessageHeader messageHeader) throws ModuleException {
		return requestHandler.getMessageParams(interchangeHeader, messageHeader);
	}

	public static JAXBElement<?> tranformResponse(DefaultServiceResponse response, IATAOperation ediOperation,
			IATARequest requestHandler) throws ModuleException {

		IATAResponse iataResponse = IataOperationConfig.getResponseHandler(ediOperation, requestHandler);
		return iataResponse.transform(response);
	}

	public static String tranformErrorResponse(DefaultServiceResponse response, InterchangeHeader iHeader, MessageHeader mHeader) {

		try {
			IATAResponse iataResponse = new Contrl(null);

			response.addResponceParam(TypeACommandParamNames.IC_HEADER, iHeader);
			response.addResponceParam(TypeACommandParamNames.MESSAGE_HEADER, mHeader);
			JAXBElement<?> transform = iataResponse.transform(response);
			String ediXMLOutput = Marshaller.javaToEdi(transform, IATAOperation.CONTRL);
			String ediResult = Convert.xmlToEdi(ediXMLOutput);
			return ediResult;
		} catch (ConverterException e) {
			log.error("ConverterException error", e);
			// This should not occour. nothing to do as we got exception while creating the error responce.
		} catch (JAXBException e) {
			log.error("JAXBException error", e);
			// This should not occour. nothing to do as we got exception while creating the error responce.
		} catch (InteractiveEDIException e) {
			log.error("InteractiveEDIException error", e);
			// This should not occour. nothing to do as we got exception while creating the error responce.
		} catch (ModuleException e) {
			log.error("ModuleException error", e);
		}

		return null;
	}
}