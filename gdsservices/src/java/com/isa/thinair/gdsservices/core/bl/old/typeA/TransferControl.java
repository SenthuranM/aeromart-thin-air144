package com.isa.thinair.gdsservices.core.bl.old.typeA;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.PassengerType;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @isa.module.command name="transferControl.old"
 * @author mekanayake
 */
public class TransferControl extends DefaultBaseCommand {

	private final ReservationBD reservationBD;
	private final PassengerBD passengerBD;

	public TransferControl() {
		super();
		reservationBD = GDSServicesModuleUtil.getReservationBD();
		passengerBD = GDSServicesModuleUtil.getPassengerBD();
	}

	@Override
	public ServiceResponce execute() throws ModuleException {

		IReservation iReservation = (IReservation) this.getParameter(TypeACommandParamNames.RESERVATION);
		Collection<OndFareDTO> ondFareDtos = (Collection) this.getParameter(TypeACommandParamNames.OND_FARE_DTOS);
		Map<Integer, Map<Integer, EticketDTO>> paxSegEticket = (Map) this.getParameter(TypeACommandParamNames.E_TICKET_MAP);
		UserPrincipal userPrincipal = (UserPrincipal) this.getParameter(TypeACommandParamNames.USER_PRINCIPLE);
		String paxType = (String) this.getParameter(TypeACommandParamNames.PAX_TYPE);
		String externalCompanyId = (String) this.getParameter(TypeACommandParamNames.EXTERNAL_COMPANY_ID);

		validateParams(paxType, iReservation, ondFareDtos, paxSegEticket);

		TrackInfoDTO trackInfoDTO = createTrackInfo(userPrincipal, externalCompanyId);

		ServiceResponce responce = null;
		if (paxType != PassengerType.INFANT) {
			// block seat before creating the reservation
			Collection blockSeats = reservationBD.blockSeats(ondFareDtos, null);
			responce = reservationBD.createGDSReservation(iReservation, blockSeats, paxSegEticket, trackInfoDTO, false);
		} else {

			IPayment iInfantPayment = (IPayment) this.getParameter(TypeACommandParamNames.PAX_PAYMENT);

			String extRecordLocator = iReservation.getExtRecordLocator();
			Reservation parentCandidateReservation = GDSServicesModuleUtil.getReservationBD()
					.getCandidateParentReservationFromExtLocator(extRecordLocator);
			if (parentCandidateReservation == null) {
				throw new ModuleException("gdsservices.typea.invalid.external.locator");
			}

			if (parentCandidateReservation.getTotalPaxInfantCount() >= 1) {
				throw new ModuleException("airreservations.adult.exceeds.infant");
			}
			ReservationAssembler rAssembler = (ReservationAssembler) iReservation;

			Integer intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED;
			ReservationPax parent = parentCandidateReservation.getPassengers().iterator().next();

			ReservationPax infant = rAssembler.getDummyReservation().getPassengers().iterator().next();
			Collection<OndFareDTO> ondFares = rAssembler.getInventoryFares();
			// assosiate pnr seg id with SegmentSeatDistsDTO
			assosiatPnrSegId(ondFareDtos, parentCandidateReservation.getSegments());

			IPassenger iPassenger = new PassengerAssembler(ondFares);
			iPassenger.addPassengerPayments(parent.getPnrPaxId(), iInfantPayment);
			Integer parentSeq = parent.getPaxSequence();
			int infantSeq = parentSeq + 1;
			iPassenger.addInfant(infant.getFirstName(), infant.getLastName(), infant.getTitle(), null, null, infantSeq,
					parentSeq, null, null, null, null, null, null, null, null, null, null, null, "", "", "", "", null);

			// correcting the pax sequence of infant
			Map<Integer, EticketDTO> firstElement = PlatformUtiltiies.getFirstElement(paxSegEticket.values());
			Map<Integer, Map<Integer, EticketDTO>> correctedPaxSegEticket = new HashMap<Integer, Map<Integer, EticketDTO>>();
			correctedPaxSegEticket.put(Integer.valueOf(infantSeq), firstElement);

			List blockIds = new ArrayList();
			responce = passengerBD.addInfantWithRequiresNew(parentCandidateReservation.getPnr(), iPassenger, blockIds,
					intPaymentMode, parentCandidateReservation.getVersion(), correctedPaxSegEticket, trackInfoDTO, false, false);

		}
		return responce;
	}

	/**
	 * assosiating the pnrSegId with SegmentSeatDis
	 * 
	 * @param ondFareDtos
	 * @param segments
	 */
	private void assosiatPnrSegId(Collection<OndFareDTO> ondFareDtos, Set<ReservationSegment> segments) {
		for (OndFareDTO fareDTO : ondFareDtos) {
			SegmentSeatDistsDTO seatDistsDTO = fareDTO.getSegmentSeatDistsDTO().iterator().next();
			int flightSegId = seatDistsDTO.getFlightSegId();
			Integer pnrSegId = null;
			for (ReservationSegment resSeg : segments) {
				if (resSeg.getFlightSegId().intValue() == flightSegId) {
					pnrSegId = resSeg.getPnrSegId();
					break;
				}
			}
			seatDistsDTO.setPnrSegId(pnrSegId);
		}

	}

	private TrackInfoDTO createTrackInfo(UserPrincipal userPrincipal, String externalCompanyId) {
		TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
		trackInfoDTO.setCarrierCode(externalCompanyId);

		trackInfoDTO.setMarketingUserId(BeanUtils.nullHandler(userPrincipal.getUserId()));
		trackInfoDTO.setMarketingAgentStationCode(BeanUtils.nullHandler(userPrincipal.getAgentStation()));
		trackInfoDTO.setMarketingAgentCode(BeanUtils.nullHandler(userPrincipal.getAgentCode()));
		return trackInfoDTO;
	}

	private void validateParams(String paxType, IReservation iReservation, Collection<OndFareDTO> ondFareDtos,
			Map<Integer, Map<Integer, EticketDTO>> paxSegEticket) throws ModuleException {

		if (iReservation == null || ondFareDtos == null || paxSegEticket == null) {
			throw new ModuleException("gdsservice.arg.invalid");
		}

	}

}
