package com.isa.thinair.gdsservices.core.command;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.ReservationResponseProcessRequest;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

/**
 * @author Manoj Dhanushka
 */
public class ReservationResponseProcessAction {
	
	private static Log log = LogFactory.getLog(ReservationResponseProcessAction.class);

	public static GDSReservationRequestBase processRequest(ReservationResponseProcessRequest reservationResponseProcessRequest) {
		try {
			String originatorPnr = GDSApiUtils.maskNull(reservationResponseProcessRequest.getOriginatorRecordLocator().getPnrReference());
			String bookingOffice = reservationResponseProcessRequest.getOriginatorRecordLocator().getBookingOffice();
			Reservation reservation = null;

			if (bookingOffice != null
					&& bookingOffice.length() > 2
					&& AppSysParamsUtil.getDefaultCarrierCode().equals(
							bookingOffice.substring(bookingOffice.length() - 2, bookingOffice.length()))) {
				reservation = CommonUtil.loadReservation(reservationResponseProcessRequest.getGdsCredentialsDTO(), null,
						originatorPnr);
			} else {
				reservation = CommonUtil.loadReservation(reservationResponseProcessRequest.getGdsCredentialsDTO(),
						originatorPnr, null);
			}
			//CommonUtil.validateReservation(reservationResponseProcessRequest.getGdsCredentialsDTO(), reservation);

			Collection<GDSInternalCodes.StatusCode> segmentStatuses = CommonUtil.getSegmentStatusCodes(reservationResponseProcessRequest);
			
			reservationResponseProcessRequest.setSuccess(true);
		} catch (ModuleException e) {
			log.error(" GDSReservationRequestBase Failed for GDS PNR "
					+ reservationResponseProcessRequest.getOriginatorRecordLocator().getPnrReference(), e);
			reservationResponseProcessRequest.setSuccess(false);
			reservationResponseProcessRequest.setResponseMessage(e.getMessageString());
		} catch (Exception e) {
			log.error(" GDSReservationRequestBase Failed for GDS PNR "
					+ reservationResponseProcessRequest.getOriginatorRecordLocator().getPnrReference(), e);
			reservationResponseProcessRequest.setSuccess(false);
			reservationResponseProcessRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
		}

		return reservationResponseProcessRequest;
	}

}
