package com.isa.thinair.gdsservices.core.bl.typeA.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.api.dto.typea.MessageError;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.api.util.GdsApplicationError;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.typeA.model.InterchangeHeader;
import com.isa.thinair.gdsservices.core.typeA.model.InterchangeInfo;
import com.isa.thinair.gdsservices.core.typeA.model.MessageIdentifier;
import com.isa.thinair.gdsservices.core.typeA.transformers.Transformer;
import com.isa.thinair.gdsservices.core.util.TypeANotes;
import com.isa.typea.common.Header;
import com.isa.typea.common.MessageHeader;

public abstract class MessageErrorAdaptor {
	public static String getErrorMessage(Header header, MessageHeader messageHeader, InteractiveEDIException e) {

		String ediErrorMsg = null;
		InterchangeHeader iHeader = null;
		com.isa.thinair.gdsservices.core.typeA.model.MessageHeader mHeader = null;

		if (header != null) {
			iHeader = toInterchangeHeader(header);
		}

		if (messageHeader != null) {
			mHeader = toMessageHeader(messageHeader);
		}

		DefaultServiceResponse responseParams = new DefaultServiceResponse(true);
		responseParams.addResponceParam(TypeACommandParamNames.EXCEPTION_CODE, e.getErrorCode());
		ediErrorMsg = Transformer.tranformErrorResponse(responseParams, iHeader, mHeader);

		return ediErrorMsg;
	}

	private static InterchangeHeader toInterchangeHeader(Header header) {
		com.isa.typea.common.InterchangeHeader interchangeHeader = header.getInterchangeHeader();
		String syntaxIdentifier = interchangeHeader.getSyntaxId();
		String versionNo = interchangeHeader.getVersionNo();
		InterchangeInfo sender = new InterchangeInfo(interchangeHeader.getSenderInfo().getInterchangeId(),
				interchangeHeader.getSenderInfo().getInterchangeCode());
		InterchangeInfo recipient = new InterchangeInfo(interchangeHeader.getReceiverInfo().getInterchangeId(),
				interchangeHeader.getReceiverInfo().getInterchangeCode());
		Date timestamp = interchangeHeader.getTimestamp().toGregorianCalendar().getTime();
		String recipientRef = interchangeHeader.getInterchangeControlRef();
		String associatioinCode = interchangeHeader.getAssociationCode();
		return new InterchangeHeader(syntaxIdentifier, versionNo, sender, recipient, timestamp, recipientRef, associatioinCode);
	}
	
	private static com.isa.thinair.gdsservices.core.typeA.model.MessageHeader toMessageHeader(MessageHeader messageHeader) {
		com.isa.thinair.gdsservices.core.typeA.model.MessageHeader mHeader =
				new com.isa.thinair.gdsservices.core.typeA.model.MessageHeader();
		mHeader.setMessageIdentifier(
				new MessageIdentifier(messageHeader.getMessageId().getMessageType(), "96", "2", messageHeader.getMessageId().getControllingAgency()));
		mHeader.setMessageReferenceNumber(messageHeader.getReferenceNo());
		mHeader.setCommonAccessReference(messageHeader.getCommonAccessReference());
		return mHeader;
	}

	public static GdsTypeAException toGdsTypeAException(List<GdsApplicationError> errorCode) {
		String error = TypeAConstants.ApplicationErrorCode.UNABLE_TO_PROCESS;

		for (GdsApplicationError e : errorCode) {
			switch (e) {
			case UNABLE_TO_SELL_TIME_LIMIT_REACHED:
				error = TypeAConstants.ApplicationErrorCode.UNABLE_TO_SELL_TIME_LIMIT_REACHED;
				break;
			case SSR_NOT_AVAILABLE:
				error = TypeAConstants.ApplicationErrorCode.SSR_NOT_AVAILABLE;
				break;
			case OPERATION_NOT_AUTHORIZED_FOR_PNR:
				error = TypeAConstants.ApplicationErrorCode.OPERATION_NOT_AUTHORIZED_FOR_PNR;
				break;
			case DUPLICATE_NAME:
				error = TypeAConstants.ApplicationErrorCode.DUPLICATE_NAME;
				break;
			}
		}

		return new GdsTypeAException(error);
	}
}
