package com.isa.thinair.gdsservices.core.typeA.transformers;

import javax.xml.bind.JAXBElement;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;

public abstract class IATAResponse {

	protected IATARequest iataRequest;

	public IATAResponse(IATARequest iataRequest) {
		super();
		this.iataRequest = iataRequest;
	}

	public JAXBElement<?> transform(DefaultServiceResponse response) throws ModuleException {
		return iataRequest.getMessageBranchHandler().constructEDIResponce(response);
	}

}