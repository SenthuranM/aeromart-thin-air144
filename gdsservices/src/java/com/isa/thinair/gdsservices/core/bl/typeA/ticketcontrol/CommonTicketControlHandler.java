package com.isa.thinair.gdsservices.core.bl.typeA.ticketcontrol;

import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.bl.typeA.common.EdiMessageHandler;
import com.isa.thinair.gdsservices.core.exception.GdsTypeAException;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.typea.common.ErrorInformation;
import com.isa.typea.common.MessageFunction;
import com.isa.typea.tkcreq.AATKCREQ;
import com.isa.typea.tkcreq.TKCREQMessage;
import com.isa.typea.tkcres.AATKCRES;
import com.isa.typea.tkcres.TKCRESMessage;

/**
 * @isa.module.command name="commonTicketControlHandler"
 */
public class CommonTicketControlHandler extends EdiMessageHandler {

	private AATKCREQ tkcreq;
	private AATKCRES tkcres;

	protected void handleMessage() {

		MessageFunction messageFunction;
		TKCREQMessage tkcreqMessage = null;
		TKCRESMessage tkcresMessage = null;

		try {

			tkcreq = (AATKCREQ) messageDTO.getRequest();
			tkcreqMessage = tkcreq.getMessage();

			tkcres = new AATKCRES();
			tkcresMessage = new TKCRESMessage();
			tkcresMessage.setMessageHeader(GDSDTOUtil.createRespMessageHeader(tkcreqMessage.getMessageHeader(),
					messageDTO.getRequestMetaDataDTO().getMessageReference(), messageDTO.getRequestMetaDataDTO().getIataOperation().getResponse()));

			tkcres.setHeader(GDSDTOUtil.createRespEdiHeader(tkcreq.getHeader(), tkcresMessage.getMessageHeader(), messageDTO));
			tkcres.setMessage(tkcresMessage);

			messageDTO.setResponse(tkcres);

			validateMessage();

			EdiMessageHandler ticketHandler = getTicketHandler();
			ticketHandler.setParameter(TypeACommandParamNames.EDI_REQUEST_DTO, messageDTO);
			ticketHandler.execute();

		} catch (GdsTypeAException e) {
			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(tkcreqMessage.getMessageFunction().getMessageFunction());
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			tkcresMessage.setMessageFunction(messageFunction);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(e.getEdiErrorCode());
			tkcresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);
		} catch (Exception e) {
			messageFunction = new MessageFunction();
			messageFunction.setMessageFunction(tkcreqMessage.getMessageFunction().getMessageFunction());
			messageFunction.setResponseType(TypeAConstants.ResponseType.RECEIVED_NOT_PROCESSED);
			tkcresMessage.setMessageFunction(messageFunction);

			ErrorInformation errorInformation = new ErrorInformation();
			errorInformation.setApplicationErrorCode(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR);
			tkcresMessage.setErrorInformation(errorInformation);

			messageDTO.setInvocationError(true);
			messageDTO.setRollbackTransaction(true);
		}
	}

	private EdiMessageHandler getTicketHandler() {

		EdiMessageHandler command = null;
		AATKCREQ tkcReq = (AATKCREQ)messageDTO.getRequest();
		MessageFunction messageFunction = tkcReq.getMessage().getMessageFunction();

		String msgFunction = messageFunction.getMessageFunction();

		if (msgFunction.equals(TypeAConstants.MessageFunction.DISPLAY)) {
			command = new HandleDisplayTicket();
		} else if (msgFunction.equals(TypeAConstants.MessageFunction.CHANGE_OF_STATUS)) {
			command = new HandleChangeOfStatus();
		} else if (msgFunction.equals(TypeAConstants.MessageFunction.AIRPORT_CONTROL)) {
			command = new HandleAirportControlRequest();
		} else {
			throw new UnsupportedOperationException("An unsupported message function was found in the EDI message");
		}

		return command;
	}

	private void validateMessage() throws GdsTypeAException {
		TKCREQMessage tktreqMessage = tkcreq.getMessage();

		constraintsValidator.validateMessageFunction(tktreqMessage.getMessageFunction());

	}

	protected Set<String> getAllowedMessageFunctions() {
		Set<String> msgFunctions = new HashSet<String>();
		msgFunctions.add(TypeAConstants.MessageFunction.DISPLAY);
		msgFunctions.add(TypeAConstants.MessageFunction.CHANGE_OF_STATUS);
		msgFunctions.add(TypeAConstants.MessageFunction.AIRPORT_CONTROL);
		return msgFunctions;
	}
}
