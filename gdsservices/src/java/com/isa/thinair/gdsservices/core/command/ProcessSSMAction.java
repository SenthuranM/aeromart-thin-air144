package com.isa.thinair.gdsservices.core.command;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleInfoDTO;
import com.isa.thinair.airschedules.api.dto.ScheduleSearchDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.api.utils.ScheduleStatusEnum;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.api.dto.FlightTypeDTO;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.dto.ScheduleRevisionTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.FrequencyUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.gdsservices.api.dto.external.AdHocScheduleProcessStatus;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMAuditDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMFlightLegDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMMessegeDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMResponse;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMSUBMessegeDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMSUBMessegeDTO.Action;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMSummaryAuditDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMGroupMessageDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMGroupMessageDTO.EXPECTED_ACTION;
import com.isa.thinair.gdsservices.api.dto.external.SSMMessageProcessStatus;
import com.isa.thinair.gdsservices.api.dto.external.SSMScheduleCompareInfo;
import com.isa.thinair.gdsservices.api.dto.external.ScheduleProcessStatus;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.bl.audit.ProcSSMASMAudit;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.msgbroker.core.util.Constants;
import com.isa.thinair.msgbroker.core.util.LookupUtil;
import com.isa.thinair.platform.api.ServiceResponce;

public class ProcessSSMAction {
	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ProcessSSMAction.class);
	private static String TIME_FORMAT = "HHmm";
	private static String LOCAL_TIME = "LT";
	private static String ZULU_TIME = "UTC";
	private SSMASMMessegeDTO ssIMessegeDTO = null;
	private boolean isLocalTimeMode = false;
	private AirportBD airportBD;
	private boolean isEnableAttachDefaultAnciTemplate;
	public static final String OVERLAP_SUPPLEMENTARY_INFO = "(SI)([ ])(OLAP)([ ])*([A-Z]{3}[\\/#A-Z]{4,30})?";
	private boolean enableAutoScheduleSplit = false;
	private boolean isDetailAuditEnabled = false;
	private boolean isRevExactMatchFound = false;

	public ProcessSSMAction(SSMASMMessegeDTO ssIMessegeDTO, boolean isEnableAttachDefaultAnciTemplate) {
		this.ssIMessegeDTO = ssIMessegeDTO;
		airportBD = AirSchedulesUtil.getAirportBD();
		this.isEnableAttachDefaultAnciTemplate = isEnableAttachDefaultAnciTemplate;
		this.enableAutoScheduleSplit = ssIMessegeDTO.isEnableAutoScheduleSplit();
		this.isDetailAuditEnabled = AppSysParamsUtil.isDetailAuditEnabledForProcessingScheduleMessages();

	}

	public SSMASMResponse processSSMMessageAndUpdteSchedules() throws ModuleException {
		SSMASMResponse ssiMResponse = new SSMASMResponse();

		if (isValidateSSMMessageReceived()) {
			applyDefaultTimeMode();
			List<SSMASMSUBMessegeDTO> ssmASMSubMsgsList = (List<SSMASMSUBMessegeDTO>) ssIMessegeDTO.getSsmASMSUBMessegeList();
			// SCHEDULE SHOULD BEGIN WITH VALID START & END DATE
			updateEffectiveValidSchedulePeriod(ssmASMSubMsgsList);

			if (this.isCancelScheduleDetectedWithUpdate(ssmASMSubMsgsList)) {
				ssiMResponse = this.processScheduleUpdateAndCancelMessage(ssIMessegeDTO);
			} else {
				ssiMResponse = this.processSSMSubMessages(ssmASMSubMsgsList, ssIMessegeDTO.getSupplementaryInfo());

			}
		} else {
			ssiMResponse.setSuccess(false);
			ssiMResponse.setResponseMessage("");
		}

		return ssiMResponse;
	}

	private boolean isValidateSSMMessageReceived() {
		if (ssIMessegeDTO == null || ssIMessegeDTO.getSsmASMSUBMessegeList() == null
				|| ssIMessegeDTO.getSsmASMSUBMessegeList().isEmpty()) {
			return false;
		}
		return true;
	}

	private SSMASMResponse processSSMSubMessages(List<SSMASMSUBMessegeDTO> ssmSubMsgsList, String supplementaryInfo)
			throws ModuleException {
		SSMASMResponse responseSummary = new SSMASMResponse();
		SSMASMResponse ssiMResponse = new SSMASMResponse();
		if (ssmSubMsgsList != null && ssmSubMsgsList.size() > 0) {
			responseSummary.setSuccess(true);
			for (SSMASMSUBMessegeDTO ssmASMSubMsgsDTO : ssmSubMsgsList) {
				log.info("ProcessSSMSubMessages => " + ssmASMSubMsgsDTO.getAction());
				ssiMResponse = processSSMSubMessage(ssmASMSubMsgsDTO, supplementaryInfo, ssiMResponse);
				log.info("Sub-Message Process Status => " + ssiMResponse.isSuccess() + " for " + ssmASMSubMsgsDTO.getAction());
				responseSummary.setSuccess(ssiMResponse.isSuccess());
				responseSummary.setResponseMessage(ssiMResponse.getResponseMessage());
				if (ssiMResponse.getScheduleProcessStatus() != null && !ssiMResponse.getScheduleProcessStatus().isEmpty()) {
					responseSummary.getScheduleProcessStatus().addAll(ssiMResponse.getScheduleProcessStatus());
				}
				responseSummary.setUpdatedScheduleCount(
						responseSummary.getUpdatedScheduleCount() + ssiMResponse.getUpdatedScheduleCount());

				responseSummary.setUpdatedAdhocScheduleCount(
						responseSummary.getUpdatedAdhocScheduleCount() + ssiMResponse.getUpdatedAdhocScheduleCount());

				if (!ssiMResponse.isAdHocFlightUpdate() || (SSMASMSUBMessegeDTO.Action.NEW.equals(ssmASMSubMsgsDTO.getAction())
						&& isDelayProcessingCancelAction())) {

					ProcessASMAction processASMAction = new ProcessASMAction(ssIMessegeDTO, isEnableAttachDefaultAnciTemplate);
					SSMASMResponse adHocScheduleUpdateResponse = processASMAction.processSSMAsAdhocMessageBulkUpdate(
							ssmASMSubMsgsDTO, isEnableAttachDefaultAnciTemplate, ssIMessegeDTO.getInMessegeID(),
							ssIMessegeDTO.getProcessingUserID(), this.isLocalTimeMode);

					if (!responseSummary.isSuccess() && adHocScheduleUpdateResponse.isSuccess()) {
						responseSummary.setResponseMessage(null);
						responseSummary.setSuccess(true);
					}

					Collection<SSMMessageProcessStatus> messageProcessStatus = adHocScheduleUpdateResponse
							.getScheduleProcessStatus();

					if (messageProcessStatus != null && !messageProcessStatus.isEmpty()) {
						for (SSMMessageProcessStatus processStatus : messageProcessStatus) {
							if (processStatus instanceof AdHocScheduleProcessStatus) {
								log.info((AdHocScheduleProcessStatus) processStatus);
							}

						}
						responseSummary.getScheduleProcessStatus().addAll(messageProcessStatus);
						responseSummary.setUpdatedAdhocScheduleCount(responseSummary.getUpdatedAdhocScheduleCount()
								+ adHocScheduleUpdateResponse.getUpdatedAdhocScheduleCount());
					}
				}

				if (!responseSummary.isSuccess()) {
					break;
				}
			}

		}
		addDetailMessageProcessedAudit(responseSummary);

		return responseSummary;
	}

	private SSMASMResponse processSSMSubMessage(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO, String supplementaryInfo,
			SSMASMResponse ssiMResponse) throws ModuleException {

		try {

			// validate schedule period & day of operation
			checkScheduleHasValidOperationDays(ssmASMSubMsgsDTO);

			if (isScheduleSplit(supplementaryInfo)) {
				ssiMResponse = this.splitExistingSchedule(ssmASMSubMsgsDTO);
			} else {
				switch (ssmASMSubMsgsDTO.getAction()) {
				case NEW:
					ssiMResponse = this.processRequestForNEW(ssmASMSubMsgsDTO);
					break;

				case CNL:
					ssiMResponse = this.processRequestForCNL(ssmASMSubMsgsDTO);
					break;

				case RPL:
					ssiMResponse = this.processRequestForRPL(ssmASMSubMsgsDTO);
					break;

				case EQT:
					ssiMResponse = this.processRequestForEQT(ssmASMSubMsgsDTO);
					break;

				case TIM:
					ssiMResponse = this.processRequestForTIM(ssmASMSubMsgsDTO);
					break;

				case REV:
					ssiMResponse = this.processRequestForREV(ssmASMSubMsgsDTO);
					break;

				case FLT:
					ssiMResponse = this.processRequestForFLT(ssmASMSubMsgsDTO);
					break;
				case SKD:
				case ACK:
				case NAC:
				case ADM:
					ssiMResponse = new SSMASMResponse();
					ssiMResponse.setResponseMessage("ACTION TYPE NOT SUPPORTED");
					break;

				}

				// perform AdHocFlight Update
				if (!ssiMResponse.isSuccess() && ssiMResponse.isAdHocFlightUpdate()) {
					ProcessASMAction processASMAction = new ProcessASMAction(ssIMessegeDTO, isEnableAttachDefaultAnciTemplate);
					ssiMResponse = processASMAction.processSSMAsAdhocMessage(ssmASMSubMsgsDTO, isEnableAttachDefaultAnciTemplate,
							ssIMessegeDTO.getInMessegeID(), ssIMessegeDTO.getProcessingUserID(), this.isLocalTimeMode);
					ssiMResponse.setAdHocFlightUpdate(true);
				}

			}

		} catch (Exception e) {
			log.error(" Exception @ ProcessSSMAction :: processSSMSubMessage", e);
			GDSApiUtils.addErrorResponse(ssiMResponse, e);
		}
		return ssiMResponse;
	}

	/**
	 * to recognize aeroMart Schedule Split operation
	 */
	private boolean isScheduleSplit(String supplementaryInfo) {

		List<String> scheduleSplitFreeText = new ArrayList<String>();
		scheduleSplitFreeText.add("SI SCHEDULE SPLIT");
		if (!StringUtil.isNullOrEmpty(supplementaryInfo) && scheduleSplitFreeText.contains(supplementaryInfo)) {
			return true;
		}

		return false;
	}

	private SSMASMResponse processRequestForNEW(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();
		ProcSSMASMAudit.Operation lastTask = ProcSSMASMAudit.Operation.CREATE;
		try {
			GDSApiUtils.validateSsmAsmRequest(ssmASMSubMsgsDTO);
			FlightSchedule schedule = new FlightSchedule();
			Date startDate = ssmASMSubMsgsDTO.getStartDate();
			Date stopDate = ssmASMSubMsgsDTO.getEndDate();

			String externalFlightNo = ssmASMSubMsgsDTO.getExternalFlightNo();

			if (!ssmASMSubMsgsDTO.isOwnSchedule() && !StringUtil.isNullOrEmpty(externalFlightNo)) {
				schedule.setFlightNumber(externalFlightNo);
				schedule.setCsOCCarrierCode(ssmASMSubMsgsDTO.getOperatingCarrier());
				schedule.setCsOCFlightNumber(ssmASMSubMsgsDTO.getFlightDesignator());

			} else {
				schedule.setFlightNumber(ssmASMSubMsgsDTO.getFlightDesignator());
			}

			int operationTypeId = GDSApiUtils.getAAOperationIdForIATAServiceType(ssmASMSubMsgsDTO.getServiceType());
			schedule.setOperationTypeId(operationTypeId);

			Frequency fqn = getFrequencyOfDays(ssmASMSubMsgsDTO.getDaysOfOperation());

			Collection<SSMASMFlightLegDTO> legs = ssmASMSubMsgsDTO.getLegs();

			Set<FlightScheduleLeg> legSet = new HashSet<FlightScheduleLeg>();
			int n = 1;
			if (legs != null && !legs.isEmpty()) {
				for (SSMASMFlightLegDTO legDTO : legs) {
					if (StringUtil.isNullOrEmpty(schedule.getDepartureStnCode())) {
						schedule.setDepartureStnCode(legDTO.getDepatureAirport());
					}

					schedule.setArrivalStnCode(legDTO.getArrivalAirport());

					FlightScheduleLeg fScheLeg = new FlightScheduleLeg();
					updateScheduleLeg(schedule, fScheLeg, legDTO, startDate, stopDate, fqn);

					fScheLeg.setLegNumber(n);
					n++;
					legSet.add(fScheLeg);

				}
			}

			schedule.setFlightScheduleLegs(legSet);
			updateScheduleSegments(schedule, legSet);

			String modelNo = getAircraftModelNo(ssmASMSubMsgsDTO.getAircraftType(), ssmASMSubMsgsDTO.getAircraftConfiguration());

			setFlightType(schedule.getDepartureStnCode(), schedule.getArrivalStnCode(), schedule);
			schedule.setModelNumber(modelNo);
			schedule.setViaScheduleMessaging(true);

			// set CodeShare details
			schedule.setCodeShareMCFlightSchedules(
					GDSApiUtils.getCodeShareMCFlightSchedules(ssmASMSubMsgsDTO.getMarketingFlightNos()));

			if (n > 2) {
				schedule.setOverlapingScheduleId(
						getPosibleOverlapScheduleId(schedule, ssmASMSubMsgsDTO.getSubSupplementaryInfo()));
			}

			Collection<Integer> cnxWaitingScheduleIds = new ArrayList<>();
			if (CalendarUtil.isSameDay(ssmASMSubMsgsDTO.getStartDate(), ssmASMSubMsgsDTO.getEndDate())) {
				throw new ModuleException("adhoc.flight.update");
			}

			boolean isCnxMsgWaitingToProcess = false;
			if (isDelayProcessingCancelAction()) {
				cnxWaitingScheduleIds = getWaitingFlightDesignatorChangeRequest(schedule, ssmASMSubMsgsDTO);

				Collection<Integer> cnxWaitingFlightIds = getCancelDelayedAdHocFlightsForFlightNoChange(ssmASMSubMsgsDTO,
						schedule);

				if ((cnxWaitingScheduleIds != null && !cnxWaitingScheduleIds.isEmpty())
						|| (cnxWaitingFlightIds != null && !cnxWaitingFlightIds.isEmpty())) {
					isCnxMsgWaitingToProcess = true;
				}

				// FIXME : temp fix for concurrent access, need to remove after message sequence is considered
				if (!isCnxMsgWaitingToProcess) {
					log.info("SSM NEW Action delayed ");
					Thread.sleep(5000);
					log.info("SSM NEW Action Resumed ");
				}
			}

			if (cnxWaitingScheduleIds != null && !cnxWaitingScheduleIds.isEmpty()) {
				ssimResponse = replaceFlightDesignatorInformation(ssmASMSubMsgsDTO, cnxWaitingScheduleIds);
			} else if (!isCnxMsgWaitingToProcess) {
				ServiceResponce serviceResponce = GDSServicesModuleUtil.getScheduleBD().createSchedule(schedule, true,
						isEnableAttachDefaultAnciTemplate);

				if (serviceResponce != null) {
					Integer scheduleId = (Integer) serviceResponce.getResponseParam(GDSSchedConstants.ParamNames.SCHEDULE_ID);
					auditSSMProc(ssmASMSubMsgsDTO, scheduleId, ProcSSMASMAudit.Operation.CREATE, serviceResponce.isSuccess(),
							serviceResponce.getResponseCode());

					if (serviceResponce.isSuccess()) {

						ssimResponse.setSuccess(true);
						ssimResponse.setResponseMessage(null);
						if (scheduleId != null) {

							ScheduleProcessStatus scheduleProcessStatus = null;
							if (this.isDetailAuditEnabled) {
								FlightSchedule scheduleNew = GDSServicesModuleUtil.getScheduleBD().getFlightSchedule(scheduleId);
								scheduleProcessStatus = adaptScheduleProcessStatus(scheduleNew);
								scheduleProcessStatus.setSuccessfullyProcessed(true);
								ssimResponse.setUpdatedScheduleCount(ssimResponse.getUpdatedScheduleCount() + 1);
								ssimResponse.addScheduleProcessStatus(scheduleProcessStatus);
							}

							Collection<Integer> scheduleIds = new ArrayList<Integer>();
							scheduleIds.add(scheduleId);
							lastTask = ProcSSMASMAudit.Operation.BUILD;
							serviceResponce = GDSServicesModuleUtil.getScheduleBD().buildSchedule(scheduleIds, true);
							auditSSMProc(ssmASMSubMsgsDTO, scheduleId, ProcSSMASMAudit.Operation.BUILD,
									serviceResponce.isSuccess(), serviceResponce.getResponseCode());

							if (!serviceResponce.isSuccess() || (!ResponceCodes.BUILD_SCHEDULE_SUCCESSFULL
									.equalsIgnoreCase(serviceResponce.getResponseCode())
									&& !StringUtil.isNullOrEmpty(serviceResponce.getResponseCode()))) {

								StringBuilder buildFailMsg = new StringBuilder();
								buildFailMsg.append("SCHEDULE BUILD FAIL MESSAGE FOR SCHEDULE ID : " + scheduleId + "\n\n");
								buildFailMsg.append("FLIGHT NO: " + schedule.getFlightNumber() + "\n\n");
								if (startDate != null && stopDate != null) {
									buildFailMsg
											.append(" Period From:" + CalendarUtil.formatDate(startDate, CalendarUtil.PATTERN1)
													+ " To:" + CalendarUtil.formatDate(stopDate, CalendarUtil.PATTERN1) + "\n\n");
								}

								if (fqn != null) {
									buildFailMsg.append(" " + fqn + "\n\n");
								}

								if (!StringUtil.isNullOrEmpty(this.ssIMessegeDTO.getRawMessage())) {
									buildFailMsg.append("SSM MESSAGE :\n\n" + this.ssIMessegeDTO.getRawMessage() + "\n\n");
								}

								log.info("SENDING SCHEDULE BUILD FAIL MESSAGE FOR SCHEDULE ID :" + scheduleId);
								sendSheduleBuildFailedEMail(buildFailMsg.toString());
							}

							// if (!serviceResponce.isSuccess()) {
							// lastTask = ProcSSMASMAudit.Operation.CANCEL;
							// serviceResponce = GDSServicesModuleUtil.getScheduleBD().cancelSchedule(scheduleId, true,
							// new FlightAlertDTO(), false, true);
							// auditSSMProc(ssmASMSubMsgsDTO, scheduleId, ProcSSMASMAudit.Operation.CANCEL,
							// serviceResponce.isSuccess(), serviceResponce.getResponseCode());
							// ssimResponse.setSuccess(false);
							// ssimResponse.setResponseMessage(
							// GDSApiUtils.getAAToGDSErrorMessages(ResponceCodes.WARNING_FOR_FLIGHT_UPDATE));
							// }
						}
					} else {
						//
						log.info("schedule creation failed :: " + serviceResponce.getResponseCode());
						boolean scheduleSplitAndUpdate = splitNewScheduleIfScheduleConflict(serviceResponce.getResponseCode(),
								ssmASMSubMsgsDTO);
						ssimResponse.setSuccess(scheduleSplitAndUpdate);
						if (!scheduleSplitAndUpdate) {
							ssimResponse
									.setResponseMessage(GDSApiUtils.getAAToGDSErrorMessages(serviceResponce.getResponseCode()));
						}
					}

				}
			}

		} catch (Exception e) {
			GDSApiUtils.addErrorResponse(ssimResponse, e);
			if (!ssimResponse.isAdHocFlightUpdate()) {
				log.error(" Exception @ ProcessSSMAction :: processRequestForNEW", e);
			}
			auditSSMProc(ssmASMSubMsgsDTO, null, lastTask, false, ssimResponse.getResponseMessage());
		}
		return ssimResponse;
	}

	private SSMASMResponse processRequestForCNL(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();
		Collection<Integer> updatedScheduleIds = new ArrayList<>();
		try {

			Collection<Integer> updateScheduleIds = getAvailableFlightSchedules(ssmASMSubMsgsDTO);

			if (updateScheduleIds != null && !updateScheduleIds.isEmpty()) {
				if (isDelayProcessingCancelAction()) {
					ssimResponse = delayProcessingScheduleCNLAction(updateScheduleIds, ssmASMSubMsgsDTO);
				} else {
					for (Integer scheduleId : updateScheduleIds) {
						ScheduleProcessStatus scheduleProcessStatus = null;
						if (this.isDetailAuditEnabled) {
							FlightSchedule schedule = GDSServicesModuleUtil.getScheduleBD().getFlightSchedule(scheduleId);
							scheduleProcessStatus = adaptScheduleProcessStatus(schedule);

						}

						ServiceResponce serviceResponce = GDSServicesModuleUtil.getScheduleBD().cancelSchedule(scheduleId, true,
								GDSApiUtils.generateFlightAlertDTO(true), false, true);

						auditSSMProc(ssmASMSubMsgsDTO, scheduleId, ProcSSMASMAudit.Operation.CANCEL, serviceResponce.isSuccess(),
								serviceResponce.getResponseCode());

						if (serviceResponce != null && serviceResponce.isSuccess()) {
							ssimResponse.setSuccess(true);
							updatedScheduleIds.add(scheduleId);
							if (this.isDetailAuditEnabled) {
								scheduleProcessStatus.setSuccessfullyProcessed(true);
							}
						} else {
							ssimResponse.setSuccess(false);
							ssimResponse
									.setResponseMessage(GDSApiUtils.getAAToGDSErrorMessages(serviceResponce.getResponseCode()));
						}
						if (this.isDetailAuditEnabled) {
							ssimResponse.addScheduleProcessStatus(scheduleProcessStatus);
						}

					}
					ssimResponse.setUpdatedScheduleCount(updatedScheduleIds.size());
				}

			}

		} catch (Exception e) {
			GDSApiUtils.addErrorResponse(ssimResponse, e);
			if (!ssimResponse.isAdHocFlightUpdate()) {
				log.error(" Exception @ ProcessSSMAction :: processRequestForCNL", e);
			}
			auditSSMProc(ssmASMSubMsgsDTO, null, ProcSSMASMAudit.Operation.CANCEL, false, ssimResponse.getResponseMessage());
			ssimResponse.setUpdatedScheduleCount(updatedScheduleIds.size());
		}

		return ssimResponse;
	}

	private SSMASMResponse processRequestForEQT(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();
		Collection<Integer> updatedScheduleIds = new ArrayList<>();
		try {

			Collection<Integer> updateScheduleIds = getAvailableFlightSchedules(ssmASMSubMsgsDTO);

			for (Integer scheduleId : updateScheduleIds) {
				FlightSchedule flightSchedule = GDSServicesModuleUtil.getScheduleBD().getFlightSchedule(scheduleId);
				String modelNo = getAircraftModelNo(ssmASMSubMsgsDTO.getAircraftType(),
						ssmASMSubMsgsDTO.getAircraftConfiguration());
				flightSchedule.setModelNumber(modelNo);
				ServiceResponce serviceResponce = GDSServicesModuleUtil.getScheduleBD().updateSchedule(flightSchedule, true,
						new FlightAlertDTO(), true, true, true, isEnableAttachDefaultAnciTemplate);

				auditSSMProc(ssmASMSubMsgsDTO, scheduleId, ProcSSMASMAudit.Operation.UPDATE, serviceResponce.isSuccess(),
						serviceResponce.getResponseCode());

				ScheduleProcessStatus scheduleProcessStatus = adaptScheduleProcessStatus(flightSchedule);

				if (serviceResponce != null && serviceResponce.isSuccess()) {
					updatedScheduleIds.add(scheduleId);
					ssimResponse.setSuccess(true);
					scheduleProcessStatus.setSuccessfullyProcessed(true);
				} else {
					ssimResponse.setSuccess(false);
					ssimResponse.setResponseMessage(GDSApiUtils.getAAToGDSErrorMessages(serviceResponce.getResponseCode()));
				}

				ssimResponse.addScheduleProcessStatus(scheduleProcessStatus);
			}

		} catch (Exception e) {
			GDSApiUtils.addErrorResponse(ssimResponse, e);
			if (!ssimResponse.isAdHocFlightUpdate()) {
				log.error(" Exception @ ProcessSSMAction :: processRequestForEQT", e);
			}
			auditSSMProc(ssmASMSubMsgsDTO, null, ProcSSMASMAudit.Operation.UPDATE, false, ssimResponse.getResponseMessage());
		}
		ssimResponse.setUpdatedScheduleCount(updatedScheduleIds.size());
		return ssimResponse;
	}

	private SSMASMResponse processRequestForTIM(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();
		Collection<Integer> updatedScheduleIds = new ArrayList<>();
		try {
			GDSApiUtils.validateSsmAsmRequest(ssmASMSubMsgsDTO);

			Collection<Integer> updateScheduleIds = getAvailableFlightSchedules(ssmASMSubMsgsDTO);

			for (Integer scheduleId : updateScheduleIds) {

				FlightSchedule schedule = GDSServicesModuleUtil.getScheduleBD().getFlightSchedule(scheduleId);

				Date startDate = schedule.getStartDate();
				Date stopDate = schedule.getStopDate();
				Frequency frq = schedule.getFrequency();
				if (isLocalTimeMode) {
					addLocalTimeDetailsToFlightSchedule(schedule);
					frq = schedule.getFrequencyLocal();
					startDate = schedule.getStartDateLocal();
					stopDate = schedule.getStopDateLocal();
				}

				Collection<SSMASMFlightLegDTO> legs = ssmASMSubMsgsDTO.getLegs();

				if (legs != null && !legs.isEmpty()) {
					for (SSMASMFlightLegDTO legDTO : legs) {
						String depAirport = legDTO.getDepatureAirport();
						String arrAirport = legDTO.getArrivalAirport();
						if (!StringUtil.isNullOrEmpty(depAirport) && !StringUtil.isNullOrEmpty(arrAirport)) {

							Set<FlightScheduleLeg> scheduleLegs = schedule.getFlightScheduleLegs();
							if (scheduleLegs != null && !scheduleLegs.isEmpty()) {
								for (FlightScheduleLeg fScheLeg : scheduleLegs) {
									if (depAirport.equalsIgnoreCase(fScheLeg.getOrigin())
											&& arrAirport.equalsIgnoreCase(fScheLeg.getDestination())) {

										updateScheduleLeg(schedule, fScheLeg, legDTO, startDate, stopDate, frq);
									}

								}
							}

						}

					}
				}

				validateScheduleLegs(schedule);

				updateScheduleSegments(schedule, schedule.getFlightScheduleLegs());

				ServiceResponce serviceResponce = GDSServicesModuleUtil.getScheduleBD().updateSchedule(schedule, true,
						GDSApiUtils.generateFlightAlertDTO(false), true, true, true, isEnableAttachDefaultAnciTemplate);

				auditSSMProc(ssmASMSubMsgsDTO, scheduleId, ProcSSMASMAudit.Operation.UPDATE, serviceResponce.isSuccess(),
						serviceResponce.getResponseCode());

				ScheduleProcessStatus scheduleProcessStatus = adaptScheduleProcessStatus(schedule);

				if (serviceResponce != null && serviceResponce.isSuccess()) {
					updatedScheduleIds.add(scheduleId);
					ssimResponse.setSuccess(true);
					scheduleProcessStatus.setSuccessfullyProcessed(true);
				} else {
					ssimResponse.setSuccess(false);
					ssimResponse.setResponseMessage(GDSApiUtils.getAAToGDSErrorMessages(serviceResponce.getResponseCode()));
				}

				ssimResponse.addScheduleProcessStatus(scheduleProcessStatus);
			}

		} catch (Exception e) {
			GDSApiUtils.addErrorResponse(ssimResponse, e);
			if (!ssimResponse.isAdHocFlightUpdate()) {
				log.error(" Exception @ ProcessSSMAction :: processRequestForTIM", e);
			}
			auditSSMProc(ssmASMSubMsgsDTO, null, ProcSSMASMAudit.Operation.UPDATE, false, ssimResponse.getResponseMessage());
		}
		ssimResponse.setUpdatedScheduleCount(updatedScheduleIds.size());
		return ssimResponse;
	}

	private String getAircraftModelNo(String aircraftType, String aircraftVersion) throws ModuleException {
		String modelNumber = GDSApiUtils.getActiveAircraftModelNumberByIataType(aircraftType, aircraftVersion);
		if (modelNumber == null) {
			throw new ModuleException("gdsservices.airschedules.logic.bl.aircraft.type.invalid");
		}
		return modelNumber;
	}

	private void updateScheduleSegments(FlightSchedule schedule, Set<FlightScheduleLeg> legSet) {
		HashSet<FlightScheduleSegment> segset = new HashSet<FlightScheduleSegment>();
		HashMap<String, FlightSegement> map = new HashMap<String, FlightSegement>();
		for (int i = 1; i < legSet.size() + 1; i++) {

			Iterator<FlightScheduleLeg> legIt = legSet.iterator();
			while (legIt.hasNext()) {

				FlightScheduleLeg leg1 = legIt.next();
				if (leg1.getLegNumber() == i) {

					String segmentCode = leg1.getOrigin() + "/" + leg1.getDestination();

					// create the segment
					FlightScheduleSegment seg1 = new FlightScheduleSegment();
					seg1.setSegmentCode(segmentCode);
					// This is to set terminal ids for the flight
					if (!map.isEmpty() && null != map.get(segmentCode)) {
						seg1.setArrivalTerminalId(map.get(segmentCode).getArrivalTerminalId());
						seg1.setDepartureTerminalId(map.get(segmentCode).getDepartureTerminalId());
					}
					segset.add(seg1);

					// creating following segments
					for (int j = i + 1; j < legSet.size() + 1; j++) {

						Iterator<FlightScheduleLeg> restLegIt = legSet.iterator();
						while (restLegIt.hasNext()) {

							FlightScheduleLeg leg2 = restLegIt.next();
							if (leg2.getLegNumber() == j) {

								segmentCode = segmentCode + "/" + leg2.getDestination();

								// create folllowing segment
								FlightScheduleSegment seg2 = new FlightScheduleSegment();
								seg2.setSegmentCode(segmentCode);
								// This is to set terminal ids for the flight
								if (!map.isEmpty() && null != map.get(segmentCode)) {
									seg2.setArrivalTerminalId(map.get(segmentCode).getArrivalTerminalId());
									seg2.setDepartureTerminalId(map.get(segmentCode).getDepartureTerminalId());
								}
								segset.add(seg2);

								break;
							}
						}
					}
					break;
				}
			}
		}
		schedule.setFlightScheduleSegments(segset);
	}

	private Collection<Integer> getAvailableFlightSchedules(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {

		ScheduleSearchDTO scheduleSearchDTO = composeScheduleSearchDTO(ssmASMSubMsgsDTO);

		Collection<FlightScheduleInfoDTO> scheduleInfoDTOs = GDSServicesModuleUtil.getScheduleBD()
				.getFlightSchedulesInfomation(scheduleSearchDTO);
		boolean isAdhocFlightUpdate = CalendarUtil.isSameDay(ssmASMSubMsgsDTO.getStartDate(), ssmASMSubMsgsDTO.getEndDate());

		if (scheduleInfoDTOs == null || scheduleInfoDTOs.isEmpty()) {
			if (isAdhocFlightUpdate) {
				// SPECIFIC ERROR THROWN TO, ADHOC SCHEDULE UPDATE FLOW TO BE TRIGGERED
				throw new ModuleException("adhoc.flight.update");
			}
			throw new ModuleException("gdsservices.airschedules.logic.bl.data.not.exist");
		}

		boolean isNewAction = Action.NEW.equals(ssmASMSubMsgsDTO.getAction())
				&& AppSysParamsUtil.isSplitAndUpdateScheduleForScheduleConflicts();

		Collection<Integer> updateScheduleIds = new ArrayList<>();
		if (!scheduleInfoDTOs.isEmpty()) {

			Frequency fqn = getFrequencyOfDays(ssmASMSubMsgsDTO.getDaysOfOperation());
			log.info("##############################  START SCHEDULE MATCH ##################################");
			for (FlightScheduleInfoDTO scheduleInfoDTO : scheduleInfoDTOs) {
				// Integer returnScheduleId = null;
				// Integer matchedScheduleId = null;
				// Date startDate = null;
				// Date endDate = null;
				if (ScheduleStatusEnum.CANCELLED.getCode().equals(scheduleInfoDTO.getStatusCode())) {
					continue;
				}
				Integer scheduleId = scheduleInfoDTO.getScheduleId();
				FlightSchedule schedule = GDSServicesModuleUtil.getScheduleBD().getFlightSchedule(scheduleId);

				SSMScheduleCompareInfo scheduleCompareInfo = compareAccelAeroScheduleWithSSM(ssmASMSubMsgsDTO.getStartDate(),
						ssmASMSubMsgsDTO.getEndDate(), fqn, schedule, isNewAction, isAdhocFlightUpdate);
				boolean exactMatch = scheduleCompareInfo.isExactMatch();
				Integer returnScheduleId = scheduleCompareInfo.getExactMatchScheduleId();

				updateScheduleCompareInfoForREVAction(scheduleCompareInfo, ssmASMSubMsgsDTO, schedule);

				if (scheduleCompareInfo.isSkipSchedule()) {
					continue;
				}

				if (!Action.REV.equals(ssmASMSubMsgsDTO.getAction())) {
					returnScheduleId = getSplitedOrExactMatchScheduleId(scheduleCompareInfo, ssmASMSubMsgsDTO);
				} else if (exactMatch && Action.REV.equals(ssmASMSubMsgsDTO.getAction())) {
					this.isRevExactMatchFound = true;
				}

				if (returnScheduleId != null) {
					printMatchedScheduleInfo(returnScheduleId, schedule, scheduleCompareInfo);
					updateScheduleIds.add(returnScheduleId);
					if (exactMatch) {
						break;
					}

				} else {
					printSkippedScheduleInfo(schedule);
				}

			}
			log.info("##############################  END OF SCHEDULE MATCH ##################################");
		}

		if (updateScheduleIds.isEmpty()) {
			if (isAdhocFlightUpdate) {
				// SPECIFIC ERROR THROWN TO, ADHOC SCHEDULE UPDATE FLOW TO BE TRIGGERED
				throw new ModuleException("adhoc.flight.update");
			}
			throw new ModuleException("gdsservices.airschedules.logic.bl.data.not.exist");
		}

		return updateScheduleIds;
	}

	private SSMASMResponse processRequestForRPL(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();
		Collection<Integer> updatedScheduleIds = new ArrayList<>();

		try {
			GDSApiUtils.validateSsmAsmRequest(ssmASMSubMsgsDTO);

			Collection<Integer> updateScheduleIds = getAvailableFlightSchedules(ssmASMSubMsgsDTO);

			for (Integer scheduleId : updateScheduleIds) {

				FlightSchedule schedule = GDSServicesModuleUtil.getScheduleBD().getFlightSchedule(scheduleId);

				Date startDate = schedule.getStartDate();
				Date stopDate = schedule.getStopDate();

				Frequency frq = schedule.getFrequency();
				if (isLocalTimeMode) {
					addLocalTimeDetailsToFlightSchedule(schedule);
					frq = schedule.getFrequencyLocal();
					startDate = schedule.getStartDateLocal();
					stopDate = schedule.getStopDateLocal();
				}

				if (!StringUtil.isNullOrEmpty(ssmASMSubMsgsDTO.getAircraftType())) {
					String modelNo = getAircraftModelNo(ssmASMSubMsgsDTO.getAircraftType(),
							ssmASMSubMsgsDTO.getAircraftConfiguration());
					schedule.setModelNumber(modelNo);
				}

				if (!StringUtil.isNullOrEmpty(ssmASMSubMsgsDTO.getServiceType())) {
					int operationTypeId = GDSApiUtils.getAAOperationIdForIATAServiceType(ssmASMSubMsgsDTO.getServiceType());
					schedule.setOperationTypeId(operationTypeId);
				}

				Collection<SSMASMFlightLegDTO> legs = ssmASMSubMsgsDTO.getLegs();

				Set<FlightScheduleLeg> legSet = new HashSet<FlightScheduleLeg>();
				int n = 1;
				if (legs != null && !legs.isEmpty()) {
					for (SSMASMFlightLegDTO legDTO : legs) {
						if (StringUtil.isNullOrEmpty(schedule.getDepartureStnCode())) {
							schedule.setDepartureStnCode(legDTO.getDepatureAirport());
						}
						schedule.setArrivalStnCode(legDTO.getArrivalAirport());
						FlightScheduleLeg fScheLeg = new FlightScheduleLeg();
						updateScheduleLeg(schedule, fScheLeg, legDTO, startDate, stopDate, frq);

						fScheLeg.setLegNumber(n);
						n++;
						legSet.add(fScheLeg);

					}
				}

				schedule.setFlightScheduleLegs(legSet);
				updateScheduleSegments(schedule, legSet);
				setFlightType(schedule.getDepartureStnCode(), schedule.getArrivalStnCode(), schedule);

				// set CodeShare details
				schedule.setCodeShareMCFlightSchedules(
						GDSApiUtils.getCodeShareMCFlightSchedules(ssmASMSubMsgsDTO.getMarketingFlightNos()));

				ServiceResponce serviceResponce = GDSServicesModuleUtil.getScheduleBD().updateSchedule(schedule, true,
						GDSApiUtils.generateFlightAlertDTO(false), true, true, true, isEnableAttachDefaultAnciTemplate);

				auditSSMProc(ssmASMSubMsgsDTO, scheduleId, ProcSSMASMAudit.Operation.UPDATE, serviceResponce.isSuccess(),
						serviceResponce.getResponseCode());

				ScheduleProcessStatus scheduleProcessStatus = adaptScheduleProcessStatus(schedule);

				if (serviceResponce != null && serviceResponce.isSuccess()) {
					scheduleProcessStatus.setSuccessfullyProcessed(true);
					ssimResponse.setSuccess(true);
					updatedScheduleIds.add(scheduleId);
				} else {
					ssimResponse.setSuccess(false);
					ssimResponse.setResponseMessage(GDSApiUtils.getAAToGDSErrorMessages(serviceResponce.getResponseCode()));
				}

				ssimResponse.addScheduleProcessStatus(scheduleProcessStatus);
			}

		} catch (Exception e) {
			GDSApiUtils.addErrorResponse(ssimResponse, e);
			if (!ssimResponse.isAdHocFlightUpdate()) {
				log.error(" Exception @ ProcessSSMAction :: processRequestForRPL", e);
			}
			auditSSMProc(ssmASMSubMsgsDTO, null, ProcSSMASMAudit.Operation.UPDATE, false, ssimResponse.getResponseMessage());
		}
		ssimResponse.setUpdatedScheduleCount(updatedScheduleIds.size());
		return ssimResponse;
	}

	private void setFlightType(String origin, String destination, FlightSchedule schedule) {
		boolean isDomesticRouteAvailable = false;
		List<FlightTypeDTO> lstFltTypes = AppSysParamsUtil.availableFlightTypes();
		for (FlightTypeDTO fltType : lstFltTypes) {
			if (FlightUtil.FLIGHT_TYPE_DOM.equals(fltType.getFlightType()) && "Y".equals(fltType.getFlightTypeStatus())) {
				isDomesticRouteAvailable = true;
			}
		}
		if (!StringUtil.isNullOrEmpty(origin) && !StringUtil.isNullOrEmpty(destination) && isDomesticRouteAvailable) {
			boolean isDomesticRoute = FlightUtil.isDomesticRoute(origin, destination);
			if (isDomesticRoute) {
				schedule.setFlightType(FlightUtil.FLIGHT_TYPE_DOM);
			} else {
				schedule.setFlightType(FlightUtil.FLIGHT_TYPE_INT);
			}
		} else {
			schedule.setFlightType(FlightUtil.FLIGHT_TYPE_INT);
		}

	}

	private Frequency getFrequencyOfDays(
			Collection<com.isa.thinair.gdsservices.api.dto.external.SSMASMSUBMessegeDTO.DayOfWeek> daysOfOperation) {
		Collection<DayOfWeek> dayList = new ArrayList<DayOfWeek>();

		if (daysOfOperation != null && !daysOfOperation.isEmpty()) {
			for (com.isa.thinair.gdsservices.api.dto.external.SSMASMSUBMessegeDTO.DayOfWeek dayOfWeek : daysOfOperation) {
				dayList.add(CalendarUtil.getIATAStandardDay(dayOfWeek.dayNumber()));
			}
		}
		Frequency fqn = CalendarUtil.getFrequencyFromDays(dayList);
		return fqn;
	}

	private SSMASMResponse processRequestForREV(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();
		Collection<Integer> updatedScheduleIds = new ArrayList<>();
		try {
			this.isRevExactMatchFound = false;
			Collection<Integer> updateScheduleIds = getAvailableFlightSchedules(ssmASMSubMsgsDTO);

			for (Integer scheduleId : updateScheduleIds) {
				FlightSchedule schedule = null;
				if (scheduleId != null) {
					schedule = GDSServicesModuleUtil.getScheduleBD().getFlightSchedule(scheduleId);
				}

				if (schedule != null) {
					Date startDate = ssmASMSubMsgsDTO.getNewStartDate();
					Date stopDate = ssmASMSubMsgsDTO.getNewEndDate();

					Frequency frq = getFrequencyOfDays(ssmASMSubMsgsDTO.getNewDaysOfOperation());

					if (this.isLocalTimeMode) {
						addLocalTimeDetailsToFlightSchedule(schedule);

						if (CalendarUtil.isSameDay(schedule.getStartDateLocal(), ssmASMSubMsgsDTO.getStartDate())
								&& CalendarUtil.isSameDay(schedule.getStopDateLocal(), ssmASMSubMsgsDTO.getEndDate())) {
							schedule.setStartDateLocal(startDate);
							schedule.setStopDateLocal(stopDate);
						}

						schedule.setFrequencyLocal(frq);
						schedule.setLocalChang(true);
						schedule.setStartDate(null);
						schedule.setStopDate(null);
						schedule.setFrequency(null);
					} else {
						if (CalendarUtil.isSameDay(schedule.getStartDate(), ssmASMSubMsgsDTO.getStartDate())
								&& CalendarUtil.isSameDay(schedule.getStopDate(), ssmASMSubMsgsDTO.getEndDate())) {
							schedule.setStartDate(startDate);
							schedule.setStopDate(stopDate);
						}

						schedule.setFrequency(frq);
						schedule.setLocalChang(false);
						schedule.setStartDateLocal(null);
						schedule.setStopDateLocal(null);
						schedule.setFrequencyLocal(null);
					}

					Set<FlightScheduleLeg> scheduleLegs = schedule.getFlightScheduleLegs();
					if (scheduleLegs != null && !scheduleLegs.isEmpty()) {
						for (FlightScheduleLeg fScheLeg : scheduleLegs) {

							if (this.isLocalTimeMode) {
								fScheLeg.setEstDepartureTimeLocal(fScheLeg.getEstDepartureTimeLocal());
								fScheLeg.setEstArrivalTimeLocal(fScheLeg.getEstArrivalTimeLocal());
								fScheLeg.setEstArrivalDayOffsetLocal(fScheLeg.getEstArrivalDayOffsetLocal());
								fScheLeg.setEstDepartureDayOffsetLocal(fScheLeg.getEstDepartureDayOffsetLocal());
								fScheLeg.setDuration(0);
								fScheLeg.setEstDepartureTimeZulu(null);
								fScheLeg.setEstArrivalTimeZulu(null);

							} else {
								fScheLeg.setEstDepartureTimeZulu(fScheLeg.getEstDepartureTimeZulu());
								fScheLeg.setEstArrivalTimeZulu(fScheLeg.getEstArrivalTimeZulu());
								fScheLeg.setEstArrivalDayOffset(fScheLeg.getEstArrivalDayOffset());
								fScheLeg.setEstDepartureDayOffset(fScheLeg.getEstDepartureDayOffset());
								fScheLeg.setEstDepartureTimeLocal(null);
								fScheLeg.setEstArrivalTimeLocal(null);

								schedule.setStartDate(startDate);
								schedule.setStopDate(stopDate);
								schedule.setFrequency(frq);

							}

						}
					}

					validateScheduleLegs(schedule);
					updateScheduleSegments(schedule, schedule.getFlightScheduleLegs());

					ServiceResponce serviceResponce = GDSServicesModuleUtil.getScheduleBD().updateSchedule(schedule, true,
							GDSApiUtils.generateFlightAlertDTO(false), true, true, true, isEnableAttachDefaultAnciTemplate);

					auditSSMProc(ssmASMSubMsgsDTO, scheduleId, ProcSSMASMAudit.Operation.UPDATE, serviceResponce.isSuccess(),
							serviceResponce.getResponseCode());

					ScheduleProcessStatus scheduleProcessStatus = adaptScheduleProcessStatus(schedule);

					if (serviceResponce != null && serviceResponce.isSuccess()) {
						updatedScheduleIds.add(scheduleId);
						ssimResponse.setSuccess(true);
						scheduleProcessStatus.setSuccessfullyProcessed(true);
					} else {
						ssimResponse.setSuccess(false);
						ssimResponse.setResponseMessage(GDSApiUtils.getAAToGDSErrorMessages(serviceResponce.getResponseCode()));
					}

					ssimResponse.addScheduleProcessStatus(scheduleProcessStatus);
				} else {
					throw new ModuleException("gdsservices.airschedules.logic.bl.data.not.exist");
				}

			}

			if (!this.isRevExactMatchFound) {
				// update missing flights for the given period
				updateMissingFlightsInRevisedSchedules(ssmASMSubMsgsDTO,ssimResponse);
			}


		} catch (Exception e) {
			GDSApiUtils.addErrorResponse(ssimResponse, e);
			if (!ssimResponse.isAdHocFlightUpdate()) {
				log.error(" Exception @ ProcessSSMAction :: processRequestForREV", e);
			}
			auditSSMProc(ssmASMSubMsgsDTO, null, ProcSSMASMAudit.Operation.UPDATE, false, ssimResponse.getResponseMessage());
		}
		ssimResponse.setUpdatedScheduleCount(updatedScheduleIds.size());

		return ssimResponse;
	}

	private SSMASMResponse splitExistingSchedule(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();

		try {

			this.enableAutoScheduleSplit = true;
			// when enableAutoScheduleSplit is true getAvailableFlightSchedules will do the required job

			Collection<Integer> updateScheduleIds = getAvailableFlightSchedules(ssmASMSubMsgsDTO);

			for (Integer scheduleId : updateScheduleIds) {
				log.info("splitExistingSchedule updated schedule part " + scheduleId);
				ssimResponse.setSuccess(true);
			}

		} catch (Exception e) {
			ssimResponse.setSuccess(false);
			log.error(" Exception @ ProcessSSMAction :: splitExistingSchedule", e);
			GDSApiUtils.addErrorResponse(ssimResponse, e);
		}

		return ssimResponse;
	}

	private SSMASMResponse processRequestForFLT(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();
		Collection<Integer> updatedScheduleIds = new ArrayList<>();
		try {

			Collection<Integer> updateScheduleIds = getAvailableFlightSchedules(ssmASMSubMsgsDTO);
			for (Integer scheduleId : updateScheduleIds) {
				FlightSchedule flightSchedule = GDSServicesModuleUtil.getScheduleBD().getFlightSchedule(scheduleId);

				if (!StringUtil.isNullOrEmpty(ssmASMSubMsgsDTO.getNewFlightDesignator())) {
					if (ssmASMSubMsgsDTO.isCodeShareCarrier() || !ssmASMSubMsgsDTO.isOwnSchedule()) {
						flightSchedule.setCsOCFlightNumber(ssmASMSubMsgsDTO.getNewFlightDesignator());
					} else {
						flightSchedule.setFlightNumber(ssmASMSubMsgsDTO.getNewFlightDesignator());
					}
				} else {
					throw new ModuleException("gdsservices.response.message.new.flight.no.required.flt");
				}
				ServiceResponce serviceResponce = GDSServicesModuleUtil.getScheduleBD().updateSchedule(flightSchedule, true,
						GDSApiUtils.generateFlightAlertDTO(false), true, true, true, isEnableAttachDefaultAnciTemplate);

				auditSSMProc(ssmASMSubMsgsDTO, scheduleId, ProcSSMASMAudit.Operation.UPDATE, serviceResponce.isSuccess(),
						serviceResponce.getResponseCode());

				ScheduleProcessStatus scheduleProcessStatus = adaptScheduleProcessStatus(flightSchedule);

				if (serviceResponce != null && serviceResponce.isSuccess()) {
					ssimResponse.setSuccess(true);
					scheduleProcessStatus.setSuccessfullyProcessed(true);
					updatedScheduleIds.add(scheduleId);
				} else {
					ssimResponse.setResponseMessage(GDSApiUtils.getAAToGDSErrorMessages(serviceResponce.getResponseCode()));
				}

				ssimResponse.addScheduleProcessStatus(scheduleProcessStatus);
			}

		} catch (Exception e) {
			GDSApiUtils.addErrorResponse(ssimResponse, e);
			if (!ssimResponse.isAdHocFlightUpdate()) {
				log.error(" Exception @ ProcessSSMAction :: processRequestForFLT", e);
			}
			auditSSMProc(ssmASMSubMsgsDTO, null, ProcSSMASMAudit.Operation.UPDATE, false, ssimResponse.getResponseMessage());
		}
		ssimResponse.setUpdatedScheduleCount(updatedScheduleIds.size());

		return ssimResponse;
	}

	private boolean isSchedulePeriodOverlap(Date start1, Date end1, Date start2, Date end2) {
		return start1.getTime() <= end2.getTime() && start2.getTime() <= end1.getTime();
	}

	@Deprecated
	private SSMASMResponse processScheduleUpdateAndCancelMessage(SSMASMMessegeDTO ssIMessegeDTO) throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();
		try {
			// order by CNL//NEW
			log.info("Inside ProcessScheduleUpdateAndCancelMessage ");
			List<SSMASMSUBMessegeDTO> independentMessages = new ArrayList<>();
			List<SSMASMSUBMessegeDTO> removedMessages = new ArrayList<>();
			List<SSMASMSUBMessegeDTO> ssmASMSubMsgsList = (List<SSMASMSUBMessegeDTO>) ssIMessegeDTO.getSsmASMSUBMessegeList();
			List<SSMGroupMessageDTO> flightWiseSSMList = new ArrayList<SSMGroupMessageDTO>();

			for (SSMASMSUBMessegeDTO ssmASMSubMsg : ssmASMSubMsgsList) {
				if (SSMASMSUBMessegeDTO.Action.CNL.equals(ssmASMSubMsg.getAction())) {

					List<SSMASMSUBMessegeDTO> subMsgList = getAllMsgForThisSchedule(ssmASMSubMsgsList, ssmASMSubMsg);
					if (subMsgList != null && !subMsgList.isEmpty()) {
						SSMGroupMessageDTO ssmMasterMessegeDTO = new SSMGroupMessageDTO();
						removedMessages.add(ssmASMSubMsg);
						removedMessages.addAll(subMsgList);
						ssmMasterMessegeDTO.setMasterScheduleMsg(ssmASMSubMsg);
						ssmMasterMessegeDTO.setSsmASMSUBMessegeList(subMsgList);
						flightWiseSSMList.add(ssmMasterMessegeDTO);
					} else {
						independentMessages.add(ssmASMSubMsg);
					}

				}

				if (!removedMessages.contains(ssmASMSubMsg) && !independentMessages.contains(ssmASMSubMsg)) {
					independentMessages.add(ssmASMSubMsg);
				}
			}

			// process multiple messages
			if (flightWiseSSMList != null && !flightWiseSSMList.isEmpty()) {
				log.info("process multiple messages");
				for (SSMGroupMessageDTO flightWiseSSMMaster : flightWiseSSMList) {
					SSMASMSUBMessegeDTO ssmMasterMessegeDTO = flightWiseSSMMaster.getMasterScheduleMsg();
					Collection<Integer> updateScheduleIds = getAvailableFlightSchedules(ssmMasterMessegeDTO);
					for (Integer scheduleId : updateScheduleIds) {

						if (EXPECTED_ACTION.UPDATE.equals(flightWiseSSMMaster.getExpectedOperationAction())) {
							log.info("UPDATE ONLY");
							replaceScheduleInformation(flightWiseSSMMaster.getSsmASMSUBMessegeList().get(0), scheduleId);
						} else if (EXPECTED_ACTION.SPLIT_UPDATE.equals(flightWiseSSMMaster.getExpectedOperationAction())) {
							log.info("SPLIT & UPDATE");
							Collection<Integer> macthedScheduledId = new ArrayList<>();
							Collection<Integer> newScheduleIdsFromSplit = null;
							if (scheduleId != null) {
								FlightSchedule schedule = GDSServicesModuleUtil.getScheduleBD().getFlightSchedule(scheduleId);

								Date startDate = schedule.getStartDate();
								Date stopDate = schedule.getStopDate();
								Frequency frequency = schedule.getFrequency();

								if (isLocalTimeMode) {
									addLocalTimeDetailsToFlightSchedule(schedule);
									startDate = schedule.getStartDateLocal();
									stopDate = schedule.getStopDateLocal();
									frequency = schedule.getFrequencyLocal();
								}

								List<SSMASMSUBMessegeDTO> ssmASMSUBMessegeList = flightWiseSSMMaster.getSsmASMSUBMessegeList();
								boolean scheduleSplitRequired = false;
								SSMASMSUBMessegeDTO lastSsmSUBMessege = null;
								List<SSMASMSUBMessegeDTO> schedulesNotMatched = new ArrayList<SSMASMSUBMessegeDTO>();
								for (SSMASMSUBMessegeDTO ssmASMSUBMessege : ssmASMSUBMessegeList) {
									lastSsmSUBMessege = ssmASMSUBMessege;
									boolean startDaySame = CalendarUtil.isSameDay(startDate, ssmASMSUBMessege.getStartDate());
									boolean endDaySame = CalendarUtil.isSameDay(stopDate, ssmASMSUBMessege.getEndDate());
									boolean splitStart = CalendarUtil.isGreaterThan(stopDate, ssmASMSUBMessege.getEndDate());
									boolean splitEnd = CalendarUtil.isGreaterThan(ssmASMSUBMessege.getStartDate(), startDate);
									boolean splitMiddle = CalendarUtil.isBetween(ssmASMSUBMessege.getStartDate(), startDate,
											stopDate)
											&& CalendarUtil.isBetween(ssmASMSUBMessege.getEndDate(), startDate, stopDate);

									Frequency fqn = getFrequencyOfDays(ssmASMSUBMessege.getDaysOfOperation());
									boolean isSameDays = FrequencyUtil.isEqualFrequency(fqn, frequency);
									boolean isSubSetDays = FrequencyUtil.chackIfSubFrequency(frequency, fqn);

									if (!isSameDays && !isSubSetDays) {
										// target schedule Frequency should exactly match or requested Frequency must be
										// a
										// sub-set of
										// schedule Frequency
										continue;
									}
									boolean splitFrequency = startDaySame && endDaySame && !isSameDays;

									// split
									// update
									// unmatched schedule-ids
									// matched schedule id

									if (splitStart || splitEnd || splitMiddle || splitFrequency) {
										scheduleSplitRequired = true;
									}

									if (newScheduleIdsFromSplit == null) {
										newScheduleIdsFromSplit = new ArrayList<>();
										newScheduleIdsFromSplit.add(scheduleId);
									}

									if (scheduleSplitRequired) {
										Integer updatedScheduleId = splitAndUpdateMatchedSchedule(newScheduleIdsFromSplit,
												ssmASMSUBMessege, macthedScheduledId);

										if (updatedScheduleId == null) {
											schedulesNotMatched.add(ssmASMSUBMessege);
										}
									}

								}

								newScheduleIdsFromSplit.removeAll(macthedScheduledId);

								// we assume all the remaining schedules which cannot be matched with any of the
								// schedules
								// as cancelled, at the same time we assume those schedules been canceled in operating
								// carrier side or scheduling system.
								if (newScheduleIdsFromSplit != null && !newScheduleIdsFromSplit.isEmpty()) {
									Set<Integer> set = new TreeSet<Integer>();
									set.addAll(newScheduleIdsFromSplit);
									newScheduleIdsFromSplit = new ArrayList<Integer>(set);
									log.info("schedules to be cancelled  " + schedulesNotMatched.size());
									for (Integer delScheduleId : newScheduleIdsFromSplit) {

										ServiceResponce serviceResponce = GDSServicesModuleUtil.getScheduleBD().cancelSchedule(
												delScheduleId, true, GDSApiUtils.generateFlightAlertDTO(true), false, true);
										auditSSMProc(lastSsmSUBMessege, delScheduleId, ProcSSMASMAudit.Operation.CANCEL,
												serviceResponce.isSuccess(), "Canceling in-active schedules in schedule split");
									}

								}

								if (!schedulesNotMatched.isEmpty()) {
									// Reservation which has booked these flights need to be re-protected to same flight
									// again.
									log.info("schedule cannot be matched " + schedulesNotMatched.size());
									this.processSSMSubMessages(schedulesNotMatched, null);
								}

							}

						} else {
							log.info("Expected Action => " + flightWiseSSMMaster.getExpectedOperationAction());
						}

					}

				}
			}

			if (independentMessages != null && !independentMessages.isEmpty()) {
				log.info("Processing Independent Messages");
				processSSMSubMessages(independentMessages, ssIMessegeDTO.getSupplementaryInfo());
			}
			ssimResponse.setSuccess(true);
		} catch (Exception e) {
			log.error(" Exception @ ProcessSSMAction :: ProcessScheduleUpdateAndCancelMessage", e);
			GDSApiUtils.addErrorResponse(ssimResponse, e);
		}
		return ssimResponse;

	}

	private List<SSMASMSUBMessegeDTO> getAllMsgForThisSchedule(List<SSMASMSUBMessegeDTO> ssmASMSubMsgsList,
			SSMASMSUBMessegeDTO cnxSSMMsg) {
		List<SSMASMSUBMessegeDTO> updateMsgList = new ArrayList<>();
		boolean continueMatch = false;
		for (SSMASMSUBMessegeDTO ssmASMSubMsg : ssmASMSubMsgsList) {

			if (ssmASMSubMsg == cnxSSMMsg) {
				continueMatch = true;
			}

			boolean isOverlapped = isSchedulePeriodOverlap(cnxSSMMsg.getStartDate(), cnxSSMMsg.getEndDate(),
					ssmASMSubMsg.getStartDate(), ssmASMSubMsg.getEndDate());

			log.info("isOverlapped :" + isOverlapped);
			if (continueMatch && isOverlapped && SSMASMSUBMessegeDTO.Action.NEW.equals(ssmASMSubMsg.getAction())
					&& ssmASMSubMsg.getFlightDesignator().equalsIgnoreCase(cnxSSMMsg.getFlightDesignator())) {

				updateMsgList.add(ssmASMSubMsg);

			}
		}
		return updateMsgList;
	}

	private void replaceScheduleInformation(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO, Integer scheduleId)
			throws ModuleException, ParseException {

		try {

			// Date startDate = ssmASMSubMsgsDTO.getStartDate();
			// Date stopDate = ssmASMSubMsgsDTO.getEndDate();

			FlightSchedule schedule = GDSServicesModuleUtil.getScheduleBD().getFlightSchedule(scheduleId);
			Date startDate = schedule.getStartDate();
			Date stopDate = schedule.getStopDate();

			Frequency frq = schedule.getFrequency();
			if (isLocalTimeMode) {
				addLocalTimeDetailsToFlightSchedule(schedule);
				frq = schedule.getFrequencyLocal();
				startDate = schedule.getStartDateLocal();
				stopDate = schedule.getStopDateLocal();
			}

			if (!StringUtil.isNullOrEmpty(ssmASMSubMsgsDTO.getAircraftType())) {
				String modelNo = getAircraftModelNo(ssmASMSubMsgsDTO.getAircraftType(),
						ssmASMSubMsgsDTO.getAircraftConfiguration());
				schedule.setModelNumber(modelNo);
			}

			if (!StringUtil.isNullOrEmpty(ssmASMSubMsgsDTO.getServiceType())) {
				int operationTypeId = GDSApiUtils.getAAOperationIdForIATAServiceType(ssmASMSubMsgsDTO.getServiceType());
				schedule.setOperationTypeId(operationTypeId);
			}

			Collection<SSMASMFlightLegDTO> legs = ssmASMSubMsgsDTO.getLegs();

			Frequency frqUpdated = getFrequencyOfDays(ssmASMSubMsgsDTO.getDaysOfOperation());

			if (!FrequencyUtil.isEqualFrequency(frq, frqUpdated)) {
				frq = frqUpdated;
			}

			Set<FlightScheduleLeg> legSet = new HashSet<FlightScheduleLeg>();
			int n = 1;
			if (legs != null && !legs.isEmpty()) {
				for (SSMASMFlightLegDTO legDTO : legs) {
					if (StringUtil.isNullOrEmpty(schedule.getDepartureStnCode())) {
						schedule.setDepartureStnCode(legDTO.getDepatureAirport());
					}
					schedule.setArrivalStnCode(legDTO.getArrivalAirport());
					FlightScheduleLeg fScheLeg = new FlightScheduleLeg();
					updateScheduleLeg(schedule, fScheLeg, legDTO, startDate, stopDate, frq);

					fScheLeg.setLegNumber(n);
					n++;
					legSet.add(fScheLeg);

				}
			}

			schedule.setFlightScheduleLegs(legSet);
			updateScheduleSegments(schedule, legSet);
			setFlightType(schedule.getDepartureStnCode(), schedule.getArrivalStnCode(), schedule);

			ServiceResponce serviceResponce = GDSServicesModuleUtil.getScheduleBD().updateSchedule(schedule, true,
					GDSApiUtils.generateFlightAlertDTO(false), true, true, true, isEnableAttachDefaultAnciTemplate);

			auditSSMProc(ssmASMSubMsgsDTO, scheduleId, ProcSSMASMAudit.Operation.UPDATE, serviceResponce.isSuccess(),
					serviceResponce.getResponseCode());

			if (serviceResponce != null && !serviceResponce.isSuccess()) {
				log.info("Schedule update operation failed " + scheduleId);
				log.info("Rejected Reason : " + GDSApiUtils.getAAToGDSErrorMessages(serviceResponce.getResponseCode()));
			}

		} catch (ModuleException me) {
			log.error(" ModuleException @ ProcessSSMAction :: replaceScheduleInformation", me);
			auditSSMProc(ssmASMSubMsgsDTO, scheduleId, ProcSSMASMAudit.Operation.UPDATE, false, me.getExceptionCode());
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());
		}
	}

	private Integer splitAndUpdateMatchedSchedule(Collection<Integer> newScheduleIdsFromSplit,
			SSMASMSUBMessegeDTO ssmASMSUBMessege, Collection<Integer> matchedScheduledIds)
			throws ModuleException, ParseException {

		Integer matchedScheduleId = null;
		boolean exactMatch = false;
		boolean isSplitSchedule = false;
		log.info("Inside splitAndUpdateMatchedSchedule");
		try {
			Frequency fqn = getFrequencyOfDays(ssmASMSUBMessege.getDaysOfOperation());
			for (Integer splitScheduleId : newScheduleIdsFromSplit) {

				if (matchedScheduledIds.contains(splitScheduleId)) {
					continue;
				}

				FlightSchedule splitSchedule = GDSServicesModuleUtil.getScheduleBD().getFlightSchedule(splitScheduleId);

				Date startDate = splitSchedule.getStartDate();
				Date stopDate = splitSchedule.getStopDate();
				Frequency frequency = splitSchedule.getFrequency();

				if (isLocalTimeMode) {
					addLocalTimeDetailsToFlightSchedule(splitSchedule);
					startDate = splitSchedule.getStartDateLocal();
					stopDate = splitSchedule.getStopDateLocal();
					frequency = splitSchedule.getFrequencyLocal();
				}

				boolean startDaySame = CalendarUtil.isSameDay(startDate, ssmASMSUBMessege.getStartDate());
				boolean endDaySame = CalendarUtil.isSameDay(stopDate, ssmASMSUBMessege.getEndDate());
				boolean splitStart = CalendarUtil.isGreaterThan(stopDate, ssmASMSUBMessege.getEndDate());
				boolean splitEnd = CalendarUtil.isGreaterThan(ssmASMSUBMessege.getStartDate(), startDate);
				boolean splitMiddle = CalendarUtil.isBetween(ssmASMSUBMessege.getStartDate(), startDate, stopDate)
						&& CalendarUtil.isBetween(ssmASMSUBMessege.getEndDate(), startDate, stopDate);
				boolean isSameDays = FrequencyUtil.isEqualFrequency(fqn, frequency);
				boolean isSubSetDays = FrequencyUtil.chackIfSubFrequency(frequency, fqn);

				if (!isSameDays && !isSubSetDays) {
					// target schedule Frequency should exactly match or requested Frequency must be a sub-set of
					// schedule Frequency
					continue;
				}
				boolean splitFrequency = startDaySame && endDaySame && !isSameDays;

				if (startDaySame && endDaySame && isSameDays) {
					// exact match no split
					exactMatch = true;
					matchedScheduleId = splitSchedule.getScheduleId();
					matchedScheduledIds.add(matchedScheduleId);

					break;
				} else if (splitFrequency) {
					isSplitSchedule = true;
					matchedScheduleId = splitSchedule.getScheduleId();
				} else if (startDaySame && splitStart) {
					// split start
					isSplitSchedule = true;
					matchedScheduleId = splitSchedule.getScheduleId();
				} else if (endDaySame && splitEnd) {
					// split end
					isSplitSchedule = true;
					matchedScheduleId = splitSchedule.getScheduleId();
				} else if (!startDaySame && !endDaySame && splitMiddle) {
					// split middle
					isSplitSchedule = true;
					matchedScheduleId = splitSchedule.getScheduleId();
				}

			}

			if (!exactMatch && isSplitSchedule) {

				ServiceResponce serviceResponce = GDSServicesModuleUtil.getScheduleBD().splitSchedule(matchedScheduleId,
						ssmASMSUBMessege.getStartDate(), ssmASMSUBMessege.getEndDate(), fqn, isLocalTimeMode, true, true);
				auditSSMProc(ssmASMSUBMessege, matchedScheduleId, ProcSSMASMAudit.Operation.SPLIT, serviceResponce.isSuccess(),
						serviceResponce.getResponseCode());
				if (serviceResponce.isSuccess()) {
					Collection<Integer> tempNewScheduleIdsFromSplit = (Collection<Integer>) serviceResponce
							.getResponseParam(GDSSchedConstants.ParamNames.NEW_SCHEDULE_IDS_FROM_SPLIT);

					newScheduleIdsFromSplit.addAll(tempNewScheduleIdsFromSplit);

				}

				return splitAndUpdateMatchedSchedule(newScheduleIdsFromSplit, ssmASMSUBMessege, matchedScheduledIds);

			}

			if (exactMatch) {
				replaceScheduleInformation(ssmASMSUBMessege, matchedScheduleId);
				matchedScheduledIds.add(matchedScheduleId);
				return matchedScheduleId;
			}

		} catch (ModuleException me) {
			log.error(" ModuleException @ ProcessSSMAction :: replaceScheduleInformation", me);
			auditSSMProc(ssmASMSUBMessege, matchedScheduleId, ProcSSMASMAudit.Operation.SPLIT, false, me.getExceptionCode());
			throw new ModuleException(me, me.getExceptionCode(), me.getModuleCode());

		}
		return null;

	}

	private void validateScheduleLegs(FlightSchedule schedule) throws ModuleException {
		if (schedule.getFlightScheduleLegs() != null && !schedule.getFlightScheduleLegs().isEmpty()) {
			String timeMode = null;
			for (FlightScheduleLeg fScheLeg : schedule.getFlightScheduleLegs()) {
				Date depTime = fScheLeg.getEstDepartureTimeLocal();
				Date arrTime = fScheLeg.getEstArrivalTimeLocal();

				Date depTimeZulu = fScheLeg.getEstDepartureTimeZulu();
				Date arrTimeZulu = fScheLeg.getEstArrivalTimeZulu();

				if (depTime != null && arrTime != null) {
					if (timeMode != null && ZULU_TIME.equals(timeMode)) {
						throw new ModuleException("gdsservices.airschedules.logic.bl.leg.data.conflict.with.exist.data");
					}
					timeMode = LOCAL_TIME;
				} else if (depTimeZulu != null && arrTimeZulu != null) {
					if (timeMode != null && LOCAL_TIME.equals(timeMode)) {
						throw new ModuleException("gdsservices.airschedules.logic.bl.leg.data.conflict.with.exist.data");
					}
					timeMode = ZULU_TIME;
				}

			}
		}
	}

	private boolean isCancelScheduleDetectedWithUpdate(List<SSMASMSUBMessegeDTO> ssmASMSubMsgsList) {
		// check same flight CNL & NEW
		boolean isReScheduled = false;
		for (SSMASMSUBMessegeDTO ssmASMSubMsg : ssmASMSubMsgsList) {
			if (SSMASMSUBMessegeDTO.Action.CNL.equals(ssmASMSubMsg.getAction())) {
				isReScheduled = isOverlappingSameScheduleExist(ssmASMSubMsgsList, ssmASMSubMsg);
				if (isReScheduled) {
					return true;
				}
			}
		}

		return false;
	}

	private boolean isOverlappingSameScheduleExist(List<SSMASMSUBMessegeDTO> ssmASMSubMsgsList, SSMASMSUBMessegeDTO cnxSSMMsg) {
		for (SSMASMSUBMessegeDTO ssmASMSubMsg : ssmASMSubMsgsList) {

			if (ssmASMSubMsg == cnxSSMMsg) {
				continue;
			}

			boolean isOverlapped = isSchedulePeriodOverlap(cnxSSMMsg.getStartDate(), cnxSSMMsg.getEndDate(),
					ssmASMSubMsg.getStartDate(), ssmASMSubMsg.getEndDate());
			log.info("isOverlappingSameScheduleExist :" + isOverlapped);
			if (SSMASMSUBMessegeDTO.Action.NEW.equals(ssmASMSubMsg.getAction())
					&& ssmASMSubMsg.getFlightDesignator().equalsIgnoreCase(cnxSSMMsg.getFlightDesignator())) {
				return true;
			}
		}
		return false;
	}

	private void applyDefaultTimeMode() {
		if (LOCAL_TIME.equals(ssIMessegeDTO.getTimeMode())) {
			this.isLocalTimeMode = true;
		}
	}

	private void updateScheduleLeg(FlightSchedule schedule, FlightScheduleLeg fScheLeg, SSMASMFlightLegDTO legDTO, Date startDate,
			Date stopDate, Frequency frq) throws ParseException {
		String depaturTime = legDTO.getDepatureTime();
		String arrivalTime = legDTO.getArrivalTime();
		fScheLeg.setOrigin(legDTO.getDepatureAirport());
		fScheLeg.setDestination(legDTO.getArrivalAirport());

		if (this.isLocalTimeMode) {
			fScheLeg.setEstDepartureTimeLocal(CalendarUtil.getParsedTime(depaturTime, TIME_FORMAT));
			fScheLeg.setEstArrivalTimeLocal(CalendarUtil.getParsedTime(arrivalTime, TIME_FORMAT));
			fScheLeg.setEstArrivalDayOffsetLocal(legDTO.getArrivalOffset());
			fScheLeg.setEstDepartureDayOffsetLocal(legDTO.getDepatureOffiset());
			fScheLeg.setDuration(0);
			fScheLeg.setEstDepartureTimeZulu(null);
			fScheLeg.setEstArrivalTimeZulu(null);

			schedule.setStartDateLocal(startDate);
			schedule.setStopDateLocal(stopDate);
			schedule.setFrequencyLocal(frq);
			schedule.setLocalChang(true);
			schedule.setStartDate(null);
			schedule.setStopDate(null);
			schedule.setFrequency(null);

		} else {
			fScheLeg.setEstDepartureTimeZulu(CalendarUtil.getParsedTime(depaturTime, TIME_FORMAT));
			fScheLeg.setEstArrivalTimeZulu(CalendarUtil.getParsedTime(arrivalTime, TIME_FORMAT));
			fScheLeg.setEstArrivalDayOffset(legDTO.getArrivalOffset());
			fScheLeg.setEstDepartureDayOffset(legDTO.getDepatureOffiset());
			fScheLeg.setEstDepartureTimeLocal(null);
			fScheLeg.setEstArrivalTimeLocal(null);

			schedule.setStartDate(startDate);
			schedule.setStopDate(stopDate);
			schedule.setFrequency(frq);
			schedule.setLocalChang(false);
			schedule.setStartDateLocal(null);
			schedule.setStopDateLocal(null);
			schedule.setFrequencyLocal(null);
		}
	}

	private void addLocalTimeDetailsToFlightSchedule(FlightSchedule schedule) throws ModuleException {
		LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(airportBD);
		schedule = timeAdder.addLocalTimeDetailsToFlightSchedule(schedule, false);
	}

	private void auditSSMProc(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO, Integer scheduleId, ProcSSMASMAudit.Operation operation,
			boolean success, String responseCode) throws ModuleException {

		SSMASMAuditDTO ssmAuditDTO = new SSMASMAuditDTO();

		ssmAuditDTO.setUserID(ssIMessegeDTO.getProcessingUserID());
		ssmAuditDTO.setInMessegeID(ssIMessegeDTO.getInMessegeID());
		ssmAuditDTO.setScheduleId(scheduleId);
		ssmAuditDTO.setSsmASMSubMsgsDTO(ssmASMSubMsgsDTO);
		ssmAuditDTO.setOperation(operation);
		ssmAuditDTO.setResponseCode(responseCode);
		ssmAuditDTO.setSuccess(success);
		ssmAuditDTO.setRawMessage(ssIMessegeDTO.getRawMessage());

		ProcSSMASMAudit.doAuditProcSSMASM(ssmAuditDTO);
	}

	private Integer getPosibleOverlapScheduleId(FlightSchedule schedule, String subSupplementaryInfo) {

		try {
			List<String> validSegmentCodes = new ArrayList<String>();
			boolean isCheckForScheduleOverlap = false;
			if (!StringUtil.isNullOrEmpty(subSupplementaryInfo) && subSupplementaryInfo.matches(OVERLAP_SUPPLEMENTARY_INFO)) {
				String overlappingSegmentCode = null;
				Pattern pattern = Pattern.compile(OVERLAP_SUPPLEMENTARY_INFO);
				Matcher matcher = pattern.matcher(subSupplementaryInfo);
				if (matcher.find()) {
					overlappingSegmentCode = matcher.group(5);
				}
				if (!StringUtil.isNullOrEmpty(overlappingSegmentCode)) {
					validSegmentCodes = Arrays.asList(overlappingSegmentCode.split("#"));
				}
				isCheckForScheduleOverlap = true;
			}

			Collection<FlightSchedule> possibleOverlappingSchedules = (GDSServicesModuleUtil.getScheduleBD()
					.getPossibleOverlappingSchedules(schedule));

			if (possibleOverlappingSchedules != null && !possibleOverlappingSchedules.isEmpty()) {
				FlightSchedule overlappingSchedule = getOverlappingSchedule(possibleOverlappingSchedules,
						isCheckForScheduleOverlap, schedule);

				allowOverlapValidSegments(overlappingSchedule, schedule, validSegmentCodes);

				if (overlappingSchedule != null && validOverlappingSegmentExist(schedule)) {
					return overlappingSchedule.getScheduleId();
				}
			}

		} catch (Exception e) {
			log.error("error while retreiving getPosibleOverlapScheduleId");
		}
		return null;
	}

	/**
	 * if a NEW action fails due to airschedules.logic.bl.schedule.schedule.overlapped new schedule will splited
	 * separate from conflicting schedule then continue with the update
	 */
	private boolean splitNewScheduleIfScheduleConflict(String responseCode, SSMASMSUBMessegeDTO ssmASMSubMsgsDTO)
			throws ModuleException, ParseException {

		if (!StringUtil.isNullOrEmpty(responseCode) && ResponceCodes.SCHEDULE_SCHEDULE_CONFLICT.equalsIgnoreCase(responseCode)) {
			log.info(" Requesting for schedule split & update info :: SCHEDULE_SCHEDULE_CONFLICT ");
			if (AppSysParamsUtil.isSplitAndUpdateScheduleForScheduleConflicts()) {
				try {
					this.enableAutoScheduleSplit = true;

					Collection<Integer> updateScheduleIds = getAvailableFlightSchedules(ssmASMSubMsgsDTO);

					for (Integer matchedScheduleId : updateScheduleIds) {
						isScheduleHasDelayedCancelMessage(matchedScheduleId, ssmASMSubMsgsDTO);

						log.info("splitNewScheduleIfScheduleConflict :: schedule split matched schedule id - "
								+ matchedScheduleId);
						// scheduleId
						replaceScheduleInformation(ssmASMSubMsgsDTO, matchedScheduleId);
						log.info("schedule split & update scheule information completed successfully!");
						return true;
					}

				} catch (ModuleException me) {
					throw new ModuleException(responseCode, "gdsservices :: SCHEDULE_SCHEDULE_CONFLICT");
				}
			} else {
				log.info("enable isSplitAndUpdateScheduleForScheduleConflicts to split & update");
			}

		}

		return false;

	}

	private void checkScheduleHasValidOperationDays(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		// validate schedule period & day of operation

		Frequency frequencyOfSchedule = getFrequencyOfDays(ssmASMSubMsgsDTO.getDaysOfOperation());
		Frequency validOperationDays = CalendarUtil.getValidFrequencyMatchesDateRange(ssmASMSubMsgsDTO.getStartDate(),
				ssmASMSubMsgsDTO.getEndDate(), getFrequencyOfDays(ssmASMSubMsgsDTO.getDaysOfOperation()));

		boolean isValidFrequency = FrequencyUtil.isEqualFrequency(frequencyOfSchedule, validOperationDays);

		if (validOperationDays.getDayCount(true) == 0 || !isValidFrequency) {
			throw new ModuleException("gdsservices.response.message.days.of.operation.invalid");
		}
	}

	private void allowOverlapValidSegments(FlightSchedule overlappingSchedule, FlightSchedule schedule,
			List<String> validSegmentCodes) {
		if (overlappingSchedule != null && overlappingSchedule.getFlightScheduleSegments() != null && schedule != null
				&& schedule.getFlightScheduleSegments() != null) {
			for (FlightScheduleSegment fltSegement : schedule.getFlightScheduleSegments()) {
				for (FlightScheduleSegment posbleFltSegement : overlappingSchedule.getFlightScheduleSegments()) {
					if (posbleFltSegement.getSegmentCode().equals(fltSegement.getSegmentCode())) {
						fltSegement.setValidFlag(false);
					}

					if (validSegmentCodes != null && !validSegmentCodes.isEmpty()
							&& !validSegmentCodes.contains(fltSegement.getSegmentCode())) {
						fltSegement.setValidFlag(false);
					}
				}

			}
		}

	}

	private boolean validOverlappingSegmentExist(FlightSchedule schedule) {
		for (FlightScheduleSegment fltSegement : schedule.getFlightScheduleSegments()) {
			if (fltSegement.getValidFlag()) {
				return true;
			}
		}
		return false;

	}

	private FlightSchedule getOverlappingSchedule(Collection<FlightSchedule> possibleOverlappingSchedules,
			boolean isCheckForOverlapSchedule, FlightSchedule schedule) {
		if (isCheckForOverlapSchedule || AppSysParamsUtil.isEnableOverlappingbySsmAsm()) {
			for (FlightSchedule overlappingFlight : possibleOverlappingSchedules) {
				// to allow same route creation, and skip same schedule when adding days of
				// oepration
				if (overlappingFlight.getOverlapingScheduleId() == null
						&& LegUtil.checkLegsAddedDeleted(overlappingFlight, schedule)
						&& overlappingFlight.getModelNumber() != null
						&& overlappingFlight.getModelNumber().equals(schedule.getModelNumber())) {
					return overlappingFlight;
				}

			}
		}
		return null;
	}

	private boolean isDelayProcessingCancelAction() {
		if (AppSysParamsUtil.isEnableDelayProcessingSsmAsmCnlAction()
				&& AppSysParamsUtil.getSsmAsmCnlActionProcessingHoldTimeInSeconds() > 0) {
			return true;
		}
		return false;
	}

	private SSMASMResponse delayProcessingScheduleCNLAction(Collection<Integer> scheduleIds, SSMASMSUBMessegeDTO ssmASMSubMsgsDTO)
			throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();
		Integer lastScheduleId = null;
		try {

			Date currentSystemTime = CalendarUtil.getCurrentZuluDateTime();
			Date holdTill = CalendarUtil.addSeconds(CalendarUtil.getCurrentZuluDateTime(),
					AppSysParamsUtil.getSsmAsmCnlActionProcessingHoldTimeInSeconds());

			GDSServicesModuleUtil.getScheduleBD().updateDelayCancelFlightSchedule(scheduleIds, true, holdTill, null);

			for (Integer scheduleId : scheduleIds) {
				lastScheduleId = scheduleId;
				auditSSMProc(ssmASMSubMsgsDTO, scheduleId, ProcSSMASMAudit.Operation.UPDATE, true,
						"SSM CNL Action delayed from " + CalendarUtil.formatForSQL(currentSystemTime, CalendarUtil.PATTERN7)
								+ " to " + CalendarUtil.formatForSQL(holdTill, CalendarUtil.PATTERN7));

				if (this.isDetailAuditEnabled) {
					FlightSchedule schedule = GDSServicesModuleUtil.getScheduleBD().getFlightSchedule(scheduleId);
					ScheduleProcessStatus scheduleProcessStatus = adaptScheduleProcessStatus(schedule);
					scheduleProcessStatus.setRemarks("CNL message delayed");
					scheduleProcessStatus.setSuccessfullyProcessed(true);
					ssimResponse.addScheduleProcessStatus(scheduleProcessStatus);
				}

			}
			ssimResponse.setUpdatedScheduleCount(scheduleIds.size());
			ssimResponse.setSuccess(true);

		} catch (Exception e) {
			log.error(" Exception @ ProcessSSMAction :: delayProcessingScheduleCNLAction", e);
			GDSApiUtils.addErrorResponse(ssimResponse, e);
			auditSSMProc(ssmASMSubMsgsDTO, lastScheduleId, ProcSSMASMAudit.Operation.UPDATE, false,
					"Unable to delay the SSM CNL Action");
		}

		return ssimResponse;
	}

	private void ignoreScheduleWaitingCancelMessage(Integer scheduleId, SSMASMSUBMessegeDTO ssmASMSubMsgsDTO)
			throws ModuleException {
		try {
			Collection<Integer> scheduleIds = new ArrayList<Integer>();
			scheduleIds.add(scheduleId);
			GDSServicesModuleUtil.getScheduleBD().updateDelayCancelFlightSchedule(scheduleIds, false, null, null);
			auditSSMProc(ssmASMSubMsgsDTO, scheduleId, ProcSSMASMAudit.Operation.UPDATE, true, "Schedule CNL message ignored");
		} catch (Exception e) {
			log.error(" Exception @ ProcessSSMAction :: ignoreScheduleWaitingCancelMessage", e);
			auditSSMProc(ssmASMSubMsgsDTO, scheduleId, ProcSSMASMAudit.Operation.UPDATE, false,
					"Unable to delay the SSM CNL Action");
		}

	}

	private boolean isScheduleHasDelayedCancelMessage(Integer scheduleId, SSMASMSUBMessegeDTO ssmASMSubMsgsDTO)
			throws ModuleException {
		FlightSchedule schedule = GDSServicesModuleUtil.getScheduleBD().getFlightSchedule(scheduleId);

		if (schedule.isCancelMsgWaiting()) {
			ignoreScheduleWaitingCancelMessage(scheduleId, ssmASMSubMsgsDTO);
		}

		return true;
	}

	private Collection<Integer> getWaitingFlightDesignatorChangeRequest(FlightSchedule composedSchedule,
			SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		Date startDate = null;
		Date stopDate = null;

		if (isLocalTimeMode) {
			startDate = composedSchedule.getStartDateLocal();
			stopDate = composedSchedule.getStopDateLocal();
		} else {
			startDate = composedSchedule.getStartDate();
			stopDate = composedSchedule.getStopDate();
		}

		String segmentCode = getFlightScheduleSegmentCode(composedSchedule);

		Collection<FlightSchedule> flightSchedules = GDSServicesModuleUtil.getScheduleBD()
				.getPossibleFlightDesignatorChange(startDate, stopDate, segmentCode, isLocalTimeMode);

		Collection<Integer> scheduleIds = new ArrayList<>();
		Integer scheduleId = null;
		if (flightSchedules != null && !flightSchedules.isEmpty()) {
			for (FlightSchedule flightSchedule : flightSchedules) {
				if (isSameScheduleFlightNoChange(flightSchedule, composedSchedule)) {
					scheduleId = flightSchedule.getScheduleId();
					scheduleIds.add(scheduleId);
				}
			}
		}

		return getSchedulesQualifiedForTheFlightNoChange(scheduleIds, ssmASMSubMsgsDTO);
	}

	private SSMASMResponse replaceFlightDesignatorInformation(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO,
			Collection<Integer> cnxWaitingScheduleIds) throws ModuleException {
		SSMASMResponse ssimResponse = new SSMASMResponse();
		Collection<Integer> updatedScheduleIds = new ArrayList<>();
		try {

			if (cnxWaitingScheduleIds != null && !cnxWaitingScheduleIds.isEmpty()) {
				for (Integer scheduleId : cnxWaitingScheduleIds) {
					ignoreScheduleWaitingCancelMessage(scheduleId, ssmASMSubMsgsDTO);
					FlightSchedule flightSchedule = GDSServicesModuleUtil.getScheduleBD().getFlightSchedule(scheduleId);

					if (!flightSchedule.getFlightNumber().equalsIgnoreCase(ssmASMSubMsgsDTO.getFlightDesignator())) {
						flightSchedule.setFlightNumber(ssmASMSubMsgsDTO.getFlightDesignator());
					}

					ServiceResponce serviceResponce = GDSServicesModuleUtil.getScheduleBD().updateSchedule(flightSchedule, true,
							GDSApiUtils.generateFlightAlertDTO(false), true, true, true, isEnableAttachDefaultAnciTemplate);

					auditSSMProc(ssmASMSubMsgsDTO, scheduleId, ProcSSMASMAudit.Operation.UPDATE, serviceResponce.isSuccess(),
							serviceResponce.getResponseCode());
					ScheduleProcessStatus scheduleProcessStatus = adaptScheduleProcessStatus(flightSchedule);
					if (serviceResponce != null && serviceResponce.isSuccess()) {
						updatedScheduleIds.add(scheduleId);
						ssimResponse.setSuccess(true);
					} else {
						ssimResponse.setSuccess(false);
						ssimResponse.setResponseMessage(GDSApiUtils.getAAToGDSErrorMessages(serviceResponce.getResponseCode()));
					}

					ssimResponse.addScheduleProcessStatus(scheduleProcessStatus);
				}
			}

		} catch (Exception e) {
			log.error(" Exception @ ProcessSSMAction :: replaceFlightDesignatorInformation", e);
			GDSApiUtils.addErrorResponse(ssimResponse, e);
			auditSSMProc(ssmASMSubMsgsDTO, null, ProcSSMASMAudit.Operation.UPDATE, false, ssimResponse.getResponseMessage());
		}
		ssimResponse.setUpdatedScheduleCount(updatedScheduleIds.size());

		return ssimResponse;
	}

	private boolean isSchedulePeriodExtended(Date startDateRq, Date endDateRq, Date startDate, Date endDate, boolean startDaySame,
			boolean endDaySame) {
		boolean endDayExtend = CalendarUtil.isGreaterThan(endDateRq, endDate);
		boolean startDayBroughtFwd = CalendarUtil.isGreaterThan(startDate, startDateRq);
		boolean isValidStartDate = CalendarUtil.isGreaterThan(startDateRq, CalendarUtil.getCurrentZuluDateTime());
		boolean isValidEndDate = CalendarUtil.isGreaterThan(endDateRq, startDateRq);

		if (isValidStartDate && isValidEndDate) {
			if (startDayBroughtFwd && endDayExtend) {
				return true;
			} else if (startDayBroughtFwd && endDaySame) {
				return true;
			} else if (endDayExtend && startDaySame) {
				return true;
			}
		}

		return false;
	}

	private boolean isSameScheduleFlightNoChange(FlightSchedule flightSchedule, FlightSchedule composedSchedule)
			throws ModuleException {

		// TODO: when Leg Time Validation is enabled can enable this
		// boolean isLegTimeChanged = true;
		// boolean isFreqMatches = false;
		// boolean isSubSetDays = false;
		// if (isLocalTimeMode) {
		// addLocalTimeDetailsToFlightSchedule(flightSchedule);
		//
		// isLegTimeChanged = LegUtil.isLegLocalTimeChanged(flightSchedule, composedSchedule);
		// isFreqMatches = FrequencyUtil.isEqualFrequency(flightSchedule.getFrequencyLocal(),
		// composedSchedule.getFrequencyLocal());
		// isSubSetDays = FrequencyUtil.chackIfSubFrequency(composedSchedule.getFrequencyLocal(),
		// flightSchedule.getFrequencyLocal());
		//
		// } else {
		// isLegTimeChanged = LegUtil.isLegTimeChanged(flightSchedule, composedSchedule);
		// isFreqMatches = FrequencyUtil.isEqualFrequency(flightSchedule.getFrequency(),
		// composedSchedule.getFrequency());
		// isSubSetDays = FrequencyUtil.chackIfSubFrequency(composedSchedule.getFrequency(),
		// flightSchedule.getFrequency());
		// }

		// TODO:MAKE IT CONFIGURABLE
		// Due to DST overlapped schedules cannot check the Leg Time may misses some schedules, when DST schedule split
		// is handled then we can enable this validation for flight number change

		// if (!isLegModified && (isFreqMatches || isSubSetDays) && !isLegTimeChanged) {
		if (!LegUtil.checkLegsAddedDeleted(flightSchedule, composedSchedule)) {
			if (!flightSchedule.getFlightNumber().equalsIgnoreCase(composedSchedule.getFlightNumber())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * as per IATA standard schedule period is starting from first flight date & last flight date of the schedule
	 */
	private void updateEffectiveValidSchedulePeriod(List<SSMASMSUBMessegeDTO> ssmASMSubMsgsList) {

		for (SSMASMSUBMessegeDTO ssmASMSubMsgsDTO : ssmASMSubMsgsList) {

			if (ssmASMSubMsgsDTO != null) {
				Date validDateRanges[] = getValidScheduleDateRange(ssmASMSubMsgsDTO.getStartDate(), ssmASMSubMsgsDTO.getEndDate(),
						ssmASMSubMsgsDTO.getDaysOfOperation());
				ssmASMSubMsgsDTO.setStartDate(validDateRanges[0]);
				ssmASMSubMsgsDTO.setEndDate(validDateRanges[1]);

				if (Action.REV.equals(ssmASMSubMsgsDTO.getAction())) {
					Date validRevDateRanges[] = getValidScheduleDateRange(ssmASMSubMsgsDTO.getNewStartDate(),
							ssmASMSubMsgsDTO.getNewEndDate(), ssmASMSubMsgsDTO.getNewDaysOfOperation());

					ssmASMSubMsgsDTO.setNewStartDate(validRevDateRanges[0]);
					ssmASMSubMsgsDTO.setNewEndDate(validRevDateRanges[1]);
				}
			}

		}

	}

	private ScheduleSearchDTO composeScheduleSearchDTO(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) {

		ScheduleSearchDTO scheduleSearchDTO = new ScheduleSearchDTO();

		Date scheduleSearchEndDate = ssmASMSubMsgsDTO.getEndDate();
		if (CalendarUtil.isSameDay(ssmASMSubMsgsDTO.getStartDate(), scheduleSearchEndDate)) {
			scheduleSearchEndDate = CalendarUtil.addDateVarience(scheduleSearchEndDate, 1);
		}

		scheduleSearchDTO.setStartDate(CalendarUtil.getStartTimeOfDate(ssmASMSubMsgsDTO.getStartDate()));
		scheduleSearchDTO.setEndDate(CalendarUtil.getEndTimeOfDate(scheduleSearchEndDate));
		scheduleSearchDTO.setLocalTime(isLocalTimeMode);

		if (ssmASMSubMsgsDTO.isCodeShareCarrier()) {
			scheduleSearchDTO.setCsOCFlightNumber(ssmASMSubMsgsDTO.getFlightDesignator());
			scheduleSearchDTO.setCsOCCarrierCode(ssmASMSubMsgsDTO.getOperatingCarrier());
		} else {
			scheduleSearchDTO.setFlightNumber(ssmASMSubMsgsDTO.getFlightDesignator());
		}

		List<String> statusFilters = new ArrayList<String>();
		statusFilters.add(ScheduleStatusEnum.ACTIVE.toString());
		statusFilters.add(ScheduleStatusEnum.CLOSED.toString());
		scheduleSearchDTO.setStatusFilter(statusFilters);

		return scheduleSearchDTO;
	}

	private SSMScheduleCompareInfo compareAccelAeroScheduleWithSSM(Date startDateRq, Date stopDateRq, Frequency frequencyRq,
			FlightSchedule schedule, boolean isNewAction, boolean isAdhocFlightUpdate) throws ModuleException {
		SSMScheduleCompareInfo scheduleCompareInfo = new SSMScheduleCompareInfo();

		Date startDate = schedule.getStartDate();
		Date endDate = schedule.getStopDate();
		Frequency frequency = schedule.getFrequency();

		if (isLocalTimeMode) {
			addLocalTimeDetailsToFlightSchedule(schedule);
			startDate = schedule.getStartDateLocal();
			endDate = schedule.getStopDateLocal();
			frequency = schedule.getFrequencyLocal();
		}

		if (CalendarUtil.isLessThan(endDate, startDateRq) || CalendarUtil.isGreaterThan(startDate, stopDateRq)) {
			// Invalid Schedule Picked
			scheduleCompareInfo.setSkipSchedule(true);
			return scheduleCompareInfo;
		}

		Integer scheduleId = schedule.getScheduleId();
		boolean startDaySame = CalendarUtil.isSameDay(startDate, startDateRq);
		boolean endDaySame = CalendarUtil.isSameDay(endDate, stopDateRq);
		boolean splitStart = CalendarUtil.isGreaterThan(endDate, stopDateRq);
		boolean splitEnd = CalendarUtil.isGreaterThan(startDateRq, startDate);
		boolean splitMiddle = CalendarUtil.isBetween(startDateRq, startDate, endDate)
				&& CalendarUtil.isBetween(stopDateRq, startDate, endDate);

		// ---------------------------------------------/
		// SSM RQ Frequency & schedule frequency are same
		boolean isSameDays = FrequencyUtil.isEqualFrequency(frequency, frequencyRq);
		// SSM RQ Frequency is a sub set of schedule frequency
		boolean isSubSetDays = FrequencyUtil.chackIfSubFrequency(frequency, frequencyRq);
		Frequency commonDays = FrequencyUtil.getTrueOverlapFrequency(frequencyRq, frequency);
		int commonDaysCount = commonDays.getDayCount(true);

		// period & days of operations same
		boolean exactMatch = false;
		// schedule needs to be split and get the exact matching one or sub-schedule
		boolean isSplitSchedule = false;
		// Treat as REV
		boolean isPossibleAddDays = false;
		boolean isSubSchedule = false;

		Integer returnScheduleId = null;
		Integer matchedScheduleId = null;
		// common days of operation
		Frequency overlapFrequency = frequencyRq;
		Date dateSplitStartDate = startDateRq;
		Date dateSplitStopDate = stopDateRq;

		// ---------------------------------------------/

		// FIXME 1//
		if (!isSameDays && !isSubSetDays) {
			// target schedule Frequency should exactly match or requested Frequency must be a sub-set of
			// schedule Frequency
			if (isNewAction && FrequencyUtil.chackIfSubFrequency(frequencyRq, frequency)) {
				// this is allowed only for day addition via NEW
				isPossibleAddDays = true;
			} else if (commonDaysCount == 0) {
				// not a valid schedule
				scheduleCompareInfo.setSkipSchedule(true);
				return scheduleCompareInfo;
			}
		}

		if (startDaySame && endDaySame && isSameDays) {
			// exact match no split
			exactMatch = true;
			returnScheduleId = scheduleId;

		} else if (isNewAction && !isAdhocFlightUpdate && startDaySame && endDaySame && !isSameDays && isPossibleAddDays) {
			// add operation days only
			exactMatch = true;
			returnScheduleId = scheduleId;

		} else if (isNewAction && !isAdhocFlightUpdate && (isSameDays || isPossibleAddDays)
				&& isSchedulePeriodExtended(startDateRq, stopDateRq, startDate, endDate, startDaySame, endDaySame)) {
			// extending period/increasing schedule period & add operation days
			exactMatch = true;
			returnScheduleId = scheduleId;

		} else if (!isAdhocFlightUpdate) {

			Frequency inValidDaysInSchedule = FrequencyUtil.getRestOfTheFrequency(frequency, frequencyRq);
			Frequency inValidDaysRq = FrequencyUtil.getRestOfTheFrequency(frequencyRq, frequency);
			boolean isFrequencySplit = false;
			boolean isOverlap = false;
			if (commonDaysCount > 0) {

				if (!FrequencyUtil.chackIfSubFrequency(frequencyRq, frequency)
						&& (inValidDaysInSchedule.getDayCount(true) > 0 || inValidDaysRq.getDayCount(true) > 0)) {
					isFrequencySplit = true;
					matchedScheduleId = scheduleId;
					overlapFrequency = commonDays;
				} else if (inValidDaysRq.getDayCount(true) > 0) {
					// matchedScheduleId = scheduleId;
					overlapFrequency = commonDays;
				}

				if ((startDaySame || CalendarUtil.isGreaterThan(startDate, startDateRq))
						&& (endDaySame || CalendarUtil.isGreaterThan(stopDateRq, endDate))) {

					if (!isFrequencySplit) {
						returnScheduleId = scheduleId;
					} else {
						matchedScheduleId = scheduleId;
						returnScheduleId = scheduleId;
					}
					isSubSchedule = true;
				} else if (CalendarUtil.isGreaterThan(startDateRq, startDate)
						|| CalendarUtil.isGreaterThan(endDate, stopDateRq)) {
					isOverlap = true;
					matchedScheduleId = scheduleId;

				}

				if (isOverlap || isFrequencySplit) {
					isSplitSchedule = true;

					if (CalendarUtil.isGreaterThan(startDateRq, startDate)) {
						dateSplitStartDate = startDateRq;
					}

					if (CalendarUtil.isGreaterThan(startDate, startDateRq)) {
						dateSplitStartDate = startDate;
					}

					if (CalendarUtil.isGreaterThan(endDate, stopDateRq)) {
						dateSplitStopDate = stopDateRq;
					}

					if (CalendarUtil.isGreaterThan(stopDateRq, endDate)) {
						dateSplitStopDate = endDate;
					}
				}

			}

			if (!isSubSchedule && (startDaySame && splitStart) || (endDaySame && splitEnd)
					|| (!startDaySame && !endDaySame && splitMiddle)) {
				// split start
				isSplitSchedule = true;
				matchedScheduleId = scheduleId;
				if (isPossibleAddDays) {
					overlapFrequency = frequency;
				}
			}

		}

		scheduleCompareInfo.setExactMatch(exactMatch);
		scheduleCompareInfo.setExactMatchScheduleId(returnScheduleId);
		scheduleCompareInfo.setSameDays(isSameDays);
		scheduleCompareInfo.setSubSetDays(isSubSetDays);
		scheduleCompareInfo.setCommonOperationDays(overlapFrequency);
		scheduleCompareInfo.setMatchedScheduleId(matchedScheduleId);
		scheduleCompareInfo.setSplitSchedule(isSplitSchedule);
		scheduleCompareInfo.setSubSchedule(isSubSchedule);
		scheduleCompareInfo.setPossibleAddDays(isPossibleAddDays);
		scheduleCompareInfo.setDateSplitStartDate(dateSplitStartDate);
		scheduleCompareInfo.setDateSplitStopDate(dateSplitStopDate);

		return scheduleCompareInfo;
	}

	/**
	 * returns the split schedule id which is required for update
	 */
	private Integer extractSplitMatchingSchedule(Collection<Integer> newScheduleIdsFromSplit, Date dateSplitStartDate,
			Date dateSplitStopDate, Frequency splitFrequencySrc) throws ModuleException {

		if (newScheduleIdsFromSplit != null && !newScheduleIdsFromSplit.isEmpty() && dateSplitStartDate != null
				&& dateSplitStopDate != null && splitFrequencySrc != null) {
			Frequency splitFrequency = FrequencyUtil.getCopyOfFrequency(splitFrequencySrc);
			for (Integer splitScheduleId : newScheduleIdsFromSplit) {
				FlightSchedule splitSchedule = GDSServicesModuleUtil.getScheduleBD().getFlightSchedule(splitScheduleId);

				SSMScheduleCompareInfo scheduleCompareInfo = compareAccelAeroScheduleWithSSM(dateSplitStartDate,
						dateSplitStopDate, splitFrequency, splitSchedule, false, false);
				if (scheduleCompareInfo.isScheduleToBeUpdate()) {
					return splitSchedule.getScheduleId();
				}

			}

		}
		return null;
	}

	private ScheduleProcessStatus adaptScheduleProcessStatus(FlightSchedule flightSchedule) {
		ScheduleProcessStatus scheduleProcessStatus = new ScheduleProcessStatus();
		Date startDate = flightSchedule.getStartDate();
		Date stopDate = flightSchedule.getStopDate();
		Frequency frequency = flightSchedule.getFrequency();
		String orginDestination = flightSchedule.getDepartureStnCode() + "/" + flightSchedule.getArrivalStnCode();

		if (this.isLocalTimeMode) {
			scheduleProcessStatus.setLocalTimeMode(true);
			startDate = flightSchedule.getStartDateLocal();
			stopDate = flightSchedule.getStopDateLocal();
			frequency = flightSchedule.getFrequencyLocal();
		}
		scheduleProcessStatus.setStartDate(startDate);
		scheduleProcessStatus.setEndDate(stopDate);
		scheduleProcessStatus.setScheduleId(flightSchedule.getScheduleId());
		scheduleProcessStatus.setStatus(flightSchedule.getStatusCode());
		scheduleProcessStatus.setFlightNumber(flightSchedule.getFlightNumber());
		scheduleProcessStatus.setFrequency(frequency);
		scheduleProcessStatus.setOriginDestination(orginDestination);

		return scheduleProcessStatus;
	}

	private String getFlightScheduleSegmentCode(FlightSchedule composedSchedule) {

		String validSegmentCode = null;
		if (composedSchedule != null) {
			int segmentLength = 0;
			String segmentCode = null;
			Set<FlightScheduleSegment> segSet = composedSchedule.getFlightScheduleSegments();
			if (segSet != null) {
				Iterator<FlightScheduleSegment> segIte = segSet.iterator();
				while (segIte.hasNext()) {
					FlightScheduleSegment segment = (FlightScheduleSegment) segIte.next();
					segmentCode = segment.getSegmentCode();
					if (segmentCode.length() > segmentLength) {
						segmentLength = segmentCode.length();
						validSegmentCode = segmentCode;
					}

				}
			}
		}

		return validSegmentCode;
	}

	public boolean isLegLocalTimeChanged(Flight existing, FlightSchedule updated) {

		boolean islegTimeChanged = false;

		Set<FlightScheduleLeg> updaLegs = updated.getFlightScheduleLegs();
		Iterator<FlightScheduleLeg> updaIt = null;

		Set<FlightLeg> exisLegs = existing.getFlightLegs();
		Iterator<FlightLeg> exisLegsIt = exisLegs.iterator();
		while (exisLegsIt.hasNext()) {
			FlightLeg exisLeg = (FlightLeg) exisLegsIt.next();

			updaIt = updaLegs.iterator();
			while (updaIt.hasNext()) {

				FlightScheduleLeg updaLeg = (FlightScheduleLeg) updaIt.next();
				if (exisLeg.getLegNumber() == updaLeg.getLegNumber()) {

					if (!(CalendarUtil.isSameTime(exisLeg.getEstArrivalTimeLocal(), updaLeg.getEstArrivalTimeLocal())
							&& CalendarUtil.isSameTime(exisLeg.getEstDepartureTimeLocal(), updaLeg.getEstDepartureTimeLocal())
							&& exisLeg.getEstArrivalDayOffsetLocal() == updaLeg.getEstArrivalDayOffsetLocal()
							&& exisLeg.getEstDepartureDayOffsetLocal() == updaLeg.getEstDepartureDayOffsetLocal())) {
						islegTimeChanged = true;
					}
					break;
				}
			}

			if (islegTimeChanged)
				break;
		}

		return islegTimeChanged;
	}

	public boolean isLegTimeChanged(Flight existing, FlightSchedule updated) {

		boolean islegTimeChanged = false;

		Set<FlightScheduleLeg> updaLegs = updated.getFlightScheduleLegs();
		Iterator<FlightScheduleLeg> updaIt = null;

		Set<FlightLeg> exisLegs = existing.getFlightLegs();
		Iterator<FlightLeg> exisLegsIt = exisLegs.iterator();
		while (exisLegsIt.hasNext()) {
			FlightLeg exisLeg = (FlightLeg) exisLegsIt.next();

			updaIt = updaLegs.iterator();
			while (updaIt.hasNext()) {

				FlightScheduleLeg updaLeg = (FlightScheduleLeg) updaIt.next();
				if (exisLeg.getLegNumber() == updaLeg.getLegNumber()) {

					if (!(CalendarUtil.isSameTime(exisLeg.getEstArrivalTimeZulu(), updaLeg.getEstArrivalTimeZulu())
							&& CalendarUtil.isSameTime(exisLeg.getEstDepartureTimeZulu(), updaLeg.getEstDepartureTimeZulu())
							&& exisLeg.getEstArrivalDayOffset() == updaLeg.getEstArrivalDayOffset()
							&& exisLeg.getEstDepartureDayOffset() == updaLeg.getEstDepartureDayOffset())) {
						islegTimeChanged = true;
					}
					break;
				}
			}

			if (islegTimeChanged)
				break;
		}

		return islegTimeChanged;
	}

	private Collection<Integer> getCancelDelayedAdHocFlightsForFlightNoChange(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO,
			FlightSchedule schedule) throws ModuleException {

		String segmentCode = getFlightScheduleSegmentCode(schedule);
		Collection<Integer> cnxWaitingFlightIds = GDSServicesModuleUtil.getFlightBD().getPossibleFlightDesignatorChange(
				ssmASMSubMsgsDTO.getStartDate(), segmentCode, this.isLocalTimeMode, ssmASMSubMsgsDTO.getEndDate(), true);

		if (cnxWaitingFlightIds != null && !cnxWaitingFlightIds.isEmpty()) {

			Iterator<Integer> cnxWaitingFlightItr = cnxWaitingFlightIds.iterator();

			while (cnxWaitingFlightItr.hasNext()) {
				Integer flightId = cnxWaitingFlightItr.next();
				boolean isPossibleFlightNumberChange = false;
				Flight flight = GDSServicesModuleUtil.getFlightBD().getFlight(flightId);

				boolean isLegTimeChanged = true;
				if (isLocalTimeMode) {
					LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(airportBD);
					flight = timeAdder.addLocalTimeDetailsToFlight(flight);

					isLegTimeChanged = this.isLegLocalTimeChanged(flight, schedule);

				} else {
					isLegTimeChanged = this.isLegTimeChanged(flight, schedule);

				}

				if (!isLegTimeChanged && !LegUtil.checkFlightVsScheduleLegsAddedDeleted(flight, schedule)) {
					if (!flight.getFlightNumber().equalsIgnoreCase(schedule.getFlightNumber())) {
						isPossibleFlightNumberChange = true;
					}
				}

				if (!isPossibleFlightNumberChange) {
					cnxWaitingFlightItr.remove();
				}

			}
		}

		return cnxWaitingFlightIds;
	}

	private void addDetailMessageProcessedAudit(SSMASMResponse ssiMResponse) {

		if (this.isDetailAuditEnabled) {
			try {
				SSMASMSummaryAuditDTO ssmAsmAudit = new SSMASMSummaryAuditDTO();
				ssmAsmAudit.setMessageType("SSM");
				if (this.ssIMessegeDTO != null) {
					ssmAsmAudit.setInMessegeID(this.ssIMessegeDTO.getInMessegeID());
					ssmAsmAudit.setRawMessage(this.ssIMessegeDTO.getRawMessage());

				}
				ssmAsmAudit.setUserID(ssIMessegeDTO.getProcessingUserID());
				ssmAsmAudit.setTotalUpdatedSchedules(ssiMResponse.getUpdatedScheduleCount());
				ssmAsmAudit.setTotalUpdatedFlights(ssiMResponse.getUpdatedAdhocScheduleCount());

				StringBuilder sb = new StringBuilder();

				Collection<SSMMessageProcessStatus> messageProcessStatus = ssiMResponse.getScheduleProcessStatus();
				if (messageProcessStatus != null && !messageProcessStatus.isEmpty()) {
					for (SSMMessageProcessStatus processStatus : messageProcessStatus) {
						if (processStatus instanceof AdHocScheduleProcessStatus) {
							sb.append("\n" + (AdHocScheduleProcessStatus) processStatus);
							sb.append("\n");

						} else if (processStatus instanceof ScheduleProcessStatus) {

							sb.append("\n" + (ScheduleProcessStatus) processStatus);
							sb.append("\n");
						}

					}

				}
				ssmAsmAudit.setSuccess(ssiMResponse.isSuccess());
				ssmAsmAudit.setResponseCode(ssiMResponse.getResponseMessage());
				ssmAsmAudit.setUpdatedScheduleDetails(sb.toString());

				ProcSSMASMAudit.addScheduleMessageProcessedSummary(ssmAsmAudit);

			} catch (Exception e) {
				log.error("ERROR @ addDetailMessageProcessedAudit" + e.getMessage());
			}
		}

	}

	private Integer getSplitedOrExactMatchScheduleId(SSMScheduleCompareInfo scheduleCompareInfo,
			SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		Integer returnScheduleId = null;
		if (scheduleCompareInfo.isScheduleToBeUpdate()) {
			returnScheduleId = scheduleCompareInfo.getExactMatchScheduleId();
		} else if (this.enableAutoScheduleSplit && scheduleCompareInfo.isScheduleToBeSplitted()) {

			Date dateSplitStartDate = scheduleCompareInfo.getDateSplitStartDate();
			Date dateSplitStopDate = scheduleCompareInfo.getDateSplitStopDate();
			Integer matchedScheduleId = scheduleCompareInfo.getMatchedScheduleId();
			Frequency splitFrequency = scheduleCompareInfo.getCommonOperationDays();

			ServiceResponce serviceResponce = GDSServicesModuleUtil.getScheduleBD().splitSchedule(matchedScheduleId,
					dateSplitStartDate, dateSplitStopDate, FrequencyUtil.getCopyOfFrequency(splitFrequency), isLocalTimeMode,
					true, true);

			auditSSMProc(ssmASMSubMsgsDTO, matchedScheduleId, ProcSSMASMAudit.Operation.SPLIT, serviceResponce.isSuccess(),
					serviceResponce.getResponseCode());

			if (serviceResponce.isSuccess()) {
				// load schedule again
				Collection<Integer> newScheduleIdsFromSplit = (Collection<Integer>) serviceResponce
						.getResponseParam(GDSSchedConstants.ParamNames.NEW_SCHEDULE_IDS_FROM_SPLIT);

				returnScheduleId = extractSplitMatchingSchedule(newScheduleIdsFromSplit, dateSplitStartDate, dateSplitStopDate,
						scheduleCompareInfo.getCommonOperationDays());
			}
		} else {
			returnScheduleId = null;
		}

		return returnScheduleId;
	}

	private Collection<Integer> getSchedulesQualifiedForTheFlightNoChange(Collection<Integer> scheduleIds,
			SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		Collection<Integer> updateScheduleIds = new ArrayList<>();
		if (scheduleIds != null && !scheduleIds.isEmpty()) {
			Frequency fqn = getFrequencyOfDays(ssmASMSubMsgsDTO.getDaysOfOperation());
			for (Integer scheduleId : scheduleIds) {

				FlightSchedule schedule = GDSServicesModuleUtil.getScheduleBD().getFlightSchedule(scheduleId);

				if (ScheduleStatusEnum.CANCELLED.getCode().equals(schedule.getStatusCode())) {
					continue;
				}

				SSMScheduleCompareInfo scheduleCompareInfo = compareAccelAeroScheduleWithSSM(ssmASMSubMsgsDTO.getStartDate(),
						ssmASMSubMsgsDTO.getEndDate(), fqn, schedule, false, false);
				boolean exactMatch = scheduleCompareInfo.isExactMatch();

				if (scheduleCompareInfo.isSkipSchedule()) {
					continue;
				}

				Integer returnScheduleId = getSplitedOrExactMatchScheduleId(scheduleCompareInfo, ssmASMSubMsgsDTO);

				if (returnScheduleId != null) {
					printMatchedScheduleInfo(returnScheduleId, schedule, scheduleCompareInfo);
					updateScheduleIds.add(returnScheduleId);
					if (exactMatch) {
						break;
					}

				} else {
					printSkippedScheduleInfo(schedule);
				}
			}

		}
		return updateScheduleIds;
	}

	private void printSkippedScheduleInfo(FlightSchedule schedule) {

		log.info("SKIPPED SCHEDULED ID:" + schedule.getScheduleId() + " ## FLIGHT_NO:" + schedule.getFlightNumber() + " ## START:"
				+ CalendarUtil.formatDate(schedule.getStartDate(), CalendarUtil.PATTERN1) + " ## END:"
				+ CalendarUtil.formatDate(schedule.getStopDate(), CalendarUtil.PATTERN1));

	}

	private void printMatchedScheduleInfo(Integer returnScheduleId, FlightSchedule schedule,
			SSMScheduleCompareInfo scheduleCompareInfo) {

		log.info("MATCHED SCHEDULED ID:" + returnScheduleId + " ## FLIGHT_NO:" + schedule.getFlightNumber() + " ## START:"
				+ CalendarUtil.formatDate(schedule.getStartDate(), CalendarUtil.PATTERN1) + " ## END:"
				+ CalendarUtil.formatDate(schedule.getStopDate(), CalendarUtil.PATTERN1) + " SCHEDULE SPLITTED:"
				+ scheduleCompareInfo.isScheduleToBeSplitted());

	}

	private void sendSheduleBuildFailedEMail(String inMessage) {
		try {
			ReservationBD reservationBD = AirSchedulesUtil.getReservationBD();
			List<String> officerEmilList = (List<String>) reservationBD.getOfficersEmailList();

			if (!StringUtil.isNullOrEmpty(inMessage) && officerEmilList != null && !officerEmilList.isEmpty()) {
				MessagingServiceBD messagingServiceBD = LookupUtil.getMessagingServiceBD();
				List<UserMessaging> messages = new ArrayList<UserMessaging>();
				HashMap<String, String> topicParamMap = new HashMap<String, String>();
				String error = "";

				for (String recipient : officerEmilList) {
					UserMessaging userMessaging = new UserMessaging();
					userMessaging.setToAddres(recipient);
					messages.add(userMessaging);
				}
				Topic topic = new Topic();
				topic.setTopicName(Constants.ScheduleMessageEmailTemplates.SCHEDULE_PROCESS_FAILED_EMAIL);
				topic.setAttachMessageBody(false);
				error = inMessage;
				topicParamMap.put("error", error);
				topic.setTopicParams(topicParamMap);

				MessageProfile messageProfile = new MessageProfile();
				messageProfile.setUserMessagings(messages);
				messageProfile.setTopic(topic);

				messagingServiceBD.sendMessage(messageProfile);

			}
		} catch (Exception e) {
			log.error("ERROR OCCURED WHILE SENDING SCHEDULE BUILD FAIL MESSAGE");
		}

	}

	// TODO: need to Refactor following code duplicate, due to an urgent fix done
	private Integer getPossibleFlightNoChangeSchedule(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO, FlightSchedule composedSchedule)
			throws ModuleException {
		Collection<FlightScheduleInfoDTO> scheduleInfoDTOs = new ArrayList<FlightScheduleInfoDTO>();

		ScheduleSearchDTO scheduleSearchDTO = new ScheduleSearchDTO();
		Date scheduleSearchEndDate = ssmASMSubMsgsDTO.getEndDate();
		if (CalendarUtil.isSameDay(ssmASMSubMsgsDTO.getStartDate(), scheduleSearchEndDate)) {
			scheduleSearchEndDate = CalendarUtil.addDateVarience(scheduleSearchEndDate, 1);
		}

		FlightScheduleLeg firstLeg = LegUtil.getFirstScheduleLeg(composedSchedule.getFlightScheduleLegs());
		String origin = firstLeg.getOrigin();
		if (!StringUtil.isNullOrEmpty(origin)) {
			scheduleSearchDTO.setFromAirport(origin);
		}
		FlightScheduleLeg lastLeg = LegUtil.getLastScheduleLeg(composedSchedule.getFlightScheduleLegs());
		String destination = lastLeg.getDestination();
		if (!StringUtil.isNullOrEmpty(destination)) {
			scheduleSearchDTO.setToAirport(destination);
		}

		scheduleSearchDTO.setStartDate(ssmASMSubMsgsDTO.getStartDate());
		scheduleSearchDTO.setEndDate(scheduleSearchEndDate);
		scheduleSearchDTO.setLocalTime(isLocalTimeMode);

		String flightNo = ssmASMSubMsgsDTO.getFlightDesignator();
		if (ssmASMSubMsgsDTO.isCodeShareCarrier()) {
			scheduleSearchDTO.setCsOCCarrierCode(ssmASMSubMsgsDTO.getOperatingCarrier());
		}
		List<String> statusFilters = new ArrayList<String>();
		statusFilters.add(ScheduleStatusEnum.ACTIVE.toString());
		statusFilters.add(ScheduleStatusEnum.CLOSED.toString());
		scheduleSearchDTO.setStatusFilter(statusFilters);

		scheduleInfoDTOs = GDSServicesModuleUtil.getScheduleBD().getFlightSchedulesInfomation(scheduleSearchDTO);
		boolean isAdhocFlightUpdate = CalendarUtil.isSameDay(ssmASMSubMsgsDTO.getStartDate(), ssmASMSubMsgsDTO.getEndDate());

		Integer matchedScheduleId = null;
		Integer addFrequencyScheduleId = null;
		Frequency overlapFrequency = null;
		Integer returnScheduleId = null;
		if (!scheduleInfoDTOs.isEmpty()) {
			boolean splitConflictingSchedule = AppSysParamsUtil.isSplitAndUpdateScheduleForScheduleConflicts();

			boolean isSplitSchedule = false;
			boolean exactMatch = false;
			Date dateSplitStartDate = null;
			Date dateSplitStopDate = null;

			Frequency fqn = getFrequencyOfDays(ssmASMSubMsgsDTO.getDaysOfOperation());
			for (FlightScheduleInfoDTO scheduleInfoDTO : scheduleInfoDTOs) {
				Date startDate = null;
				Date endDate = null;
				boolean isLegTimeChanged = false;
				if (ScheduleStatusEnum.CANCELLED.getCode().equals(scheduleInfoDTO.getStatusCode())) {
					continue;
				}
				Integer scheduleId = scheduleInfoDTO.getScheduleId();
				FlightSchedule schedule = GDSServicesModuleUtil.getScheduleBD().getFlightSchedule(scheduleId);

				startDate = schedule.getStartDate();
				endDate = schedule.getStopDate();
				Frequency frequency = schedule.getFrequency();

				if (schedule.getFlightNumber().equalsIgnoreCase(flightNo)) {
					continue;
				}

				if (isLocalTimeMode) {
					addLocalTimeDetailsToFlightSchedule(schedule);
					startDate = schedule.getStartDateLocal();
					endDate = schedule.getStopDateLocal();
					frequency = schedule.getFrequencyLocal();
					isLegTimeChanged = LegUtil.isLegLocalTimeChanged(schedule, composedSchedule);
				} else {
					isLegTimeChanged = LegUtil.isLegTimeChanged(schedule, composedSchedule);
				}

				boolean isLegModified = LegUtil.checkLegsAddedDeleted(schedule, composedSchedule);

				boolean startDaySame = CalendarUtil.isSameDay(startDate, ssmASMSubMsgsDTO.getStartDate());
				boolean endDaySame = CalendarUtil.isSameDay(endDate, ssmASMSubMsgsDTO.getEndDate());
				boolean splitStart = CalendarUtil.isGreaterThan(endDate, ssmASMSubMsgsDTO.getEndDate());
				boolean splitEnd = CalendarUtil.isGreaterThan(ssmASMSubMsgsDTO.getStartDate(), startDate);
				boolean splitMiddle = CalendarUtil.isBetween(ssmASMSubMsgsDTO.getStartDate(), startDate, endDate)
						&& CalendarUtil.isBetween(ssmASMSubMsgsDTO.getEndDate(), startDate, endDate);

				boolean isSameDays = FrequencyUtil.isEqualFrequency(frequency, fqn);
				boolean isSubSetDays = FrequencyUtil.chackIfSubFrequency(frequency, fqn);
				boolean isPossibleAddDays = false;

				if ((!isSameDays && !isSubSetDays) || isLegModified || isLegTimeChanged) {
					// target schedule Frequency should exactly match or requested Frequency must be a sub-set of
					// schedule Frequency
					continue;

				}

				if (startDaySame && endDaySame && isSameDays) {
					// exact match no split
					exactMatch = true;
					returnScheduleId = scheduleId;
					break;
				} else if (!isAdhocFlightUpdate) {
					if (startDaySame && splitStart) {
						// split start
						isSplitSchedule = true;
						matchedScheduleId = scheduleId;
						if (isPossibleAddDays) {
							addFrequencyScheduleId = scheduleId;
							overlapFrequency = frequency;
						}
					} else if (endDaySame && splitEnd) {
						// split end
						isSplitSchedule = true;
						matchedScheduleId = scheduleId;
						if (isPossibleAddDays) {
							addFrequencyScheduleId = scheduleId;
							overlapFrequency = frequency;
						}
					} else if (!startDaySame && !endDaySame && splitMiddle) {
						// split middle
						isSplitSchedule = true;
						matchedScheduleId = scheduleId;
						if (isPossibleAddDays) {
							addFrequencyScheduleId = scheduleId;
							overlapFrequency = frequency;
						}
					}

				}

			}

			if (splitConflictingSchedule && !exactMatch && isSplitSchedule && matchedScheduleId != null) {
				dateSplitStartDate = ssmASMSubMsgsDTO.getStartDate();
				dateSplitStopDate = ssmASMSubMsgsDTO.getEndDate();
				if (addFrequencyScheduleId != null) {
					matchedScheduleId = addFrequencyScheduleId;
					fqn = overlapFrequency;
				}

				ServiceResponce serviceResponce = GDSServicesModuleUtil.getScheduleBD().splitSchedule(matchedScheduleId,
						dateSplitStartDate, dateSplitStopDate, fqn, isLocalTimeMode, true, true);

				auditSSMProc(ssmASMSubMsgsDTO, matchedScheduleId, ProcSSMASMAudit.Operation.SPLIT, serviceResponce.isSuccess(),
						serviceResponce.getResponseCode());

				if (serviceResponce.isSuccess()) {
					// load schedule again
					Collection<Integer> newScheduleIdsFromSplit = (Collection<Integer>) serviceResponce
							.getResponseParam(GDSSchedConstants.ParamNames.NEW_SCHEDULE_IDS_FROM_SPLIT);

					if (newScheduleIdsFromSplit != null && !newScheduleIdsFromSplit.isEmpty()) {

						for (Integer splitScheduleId : newScheduleIdsFromSplit) {
							FlightSchedule splitSchedule = GDSServicesModuleUtil.getScheduleBD()
									.getFlightSchedule(splitScheduleId);

							Date startDate = splitSchedule.getStartDate();
							Date endDate = splitSchedule.getStopDate();
							Frequency frequency = splitSchedule.getFrequency();

							if (isLocalTimeMode) {
								addLocalTimeDetailsToFlightSchedule(splitSchedule);
								startDate = splitSchedule.getStartDateLocal();
								endDate = splitSchedule.getStopDateLocal();
								frequency = splitSchedule.getFrequencyLocal();
							}

							boolean startDaySame = CalendarUtil.isSameDay(startDate, ssmASMSubMsgsDTO.getStartDate());
							boolean endDaySame = CalendarUtil.isSameDay(endDate, ssmASMSubMsgsDTO.getEndDate());
							boolean isSameDays = FrequencyUtil.isEqualFrequency(frequency, fqn);

							if (startDaySame && endDaySame && isSameDays
									&& isSameScheduleFlightNoChange(splitSchedule, composedSchedule)) {
								returnScheduleId = splitSchedule.getScheduleId();
								break;
							}

						}

					}
				}
			} else if (!splitConflictingSchedule && !exactMatch) {
				returnScheduleId = null;
			}

		}

		return returnScheduleId;
	}

	private Date[] getValidScheduleDateRange(Date startDate, Date endDate,
			Collection<com.isa.thinair.gdsservices.api.dto.external.SSMASMSUBMessegeDTO.DayOfWeek> daysOfOperation) {

		Date validDateRanges[] = { startDate, endDate };
		Date firstFlightDate = null;
		Date lastFlightDate = null;

		if (startDate != null && endDate != null && daysOfOperation != null) {
			Frequency splitFrequency = getFrequencyOfDays(daysOfOperation);

			Frequency frequency = CalendarUtil.getValidFrequencyMatchesDateRange(startDate, endDate, splitFrequency);

			firstFlightDate = CalendarUtil.getEffectiveStartDate(startDate, frequency);
			lastFlightDate = CalendarUtil.getEffectiveStopDate(endDate, frequency);

			if (!CalendarUtil.isSameDay(startDate, firstFlightDate) || !CalendarUtil.isSameDay(endDate, lastFlightDate)) {

				validDateRanges[0] = firstFlightDate;
				validDateRanges[1] = lastFlightDate;

			}
		}
		return validDateRanges;
	}

	private void updateScheduleCompareInfoForREVAction(SSMScheduleCompareInfo scheduleCompareInfo,
			SSMASMSUBMessegeDTO ssmASMSubMsgsDTO, FlightSchedule schedule) {

		if (Action.REV.equals(ssmASMSubMsgsDTO.getAction()) && !scheduleCompareInfo.isExactMatch()) {

			Integer returnScheduleId = scheduleCompareInfo.getExactMatchScheduleId();

			Frequency frequency = schedule.getFrequency();
			Frequency fqn = getFrequencyOfDays(ssmASMSubMsgsDTO.getDaysOfOperation());

			if (isLocalTimeMode) {
				frequency = schedule.getFrequencyLocal();
			}
			Frequency inValidDaysInSchedule = FrequencyUtil.getRestOfTheFrequency(frequency, fqn);

			if ((scheduleCompareInfo.isSubSchedule() && !(inValidDaysInSchedule.getDayCount(true) > 0))
					&& ((!FrequencyUtil.isEqualFrequency(fqn, getFrequencyOfDays(ssmASMSubMsgsDTO.getNewDaysOfOperation()))))) {
				if (returnScheduleId == null && scheduleCompareInfo.getMatchedScheduleId() != null) {
					returnScheduleId = scheduleCompareInfo.getMatchedScheduleId();
					scheduleCompareInfo.setExactMatchScheduleId(returnScheduleId);
					scheduleCompareInfo.setSplitSchedule(false);
				}

			} else {
				// schedule should be skipped from update
				scheduleCompareInfo.setSkipSchedule(true);
			}
		}

	}

	/**
	 * function to update missing flights, when revising schedule period/ days of operation
	 */
	private void updateMissingFlightsInRevisedSchedules(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO, SSMASMResponse ssimResponse) {
		try {

			ScheduleRevisionTO scheduleRevisionTO = new ScheduleRevisionTO();
			scheduleRevisionTO.setFlightNumber(ssmASMSubMsgsDTO.getFlightDesignator());
			scheduleRevisionTO.setStartDate(ssmASMSubMsgsDTO.getStartDate());
			scheduleRevisionTO.setEndDate(ssmASMSubMsgsDTO.getEndDate());
			scheduleRevisionTO.setUpdatedStartDate(ssmASMSubMsgsDTO.getNewStartDate());
			scheduleRevisionTO.setUpdatedEndDate(ssmASMSubMsgsDTO.getNewEndDate());
			scheduleRevisionTO.setFrequency(getFrequencyOfDays(ssmASMSubMsgsDTO.getDaysOfOperation()));
			scheduleRevisionTO.setUpdatedFrequency(getFrequencyOfDays(ssmASMSubMsgsDTO.getNewDaysOfOperation()));
			scheduleRevisionTO.setLocalTimeMode(this.isLocalTimeMode);
			scheduleRevisionTO.setEnableAttachDefaultAnciTemplate(this.isEnableAttachDefaultAnciTemplate);

			ServiceResponce serviceResponce = GDSServicesModuleUtil.getScheduleBD().updateMissingFlightsWithinPeriod(
					scheduleRevisionTO, GDSApiUtils.generateFlightAlertDTO(true), GDSApiUtils.getFlightAllowActionDTO());

			if (serviceResponce != null && serviceResponce.isSuccess()) {
				Collection<AdHocScheduleProcessStatus> updatedFlightStatus = generateCreatedCanceledFlightStatus(serviceResponce);
				ssimResponse.getScheduleProcessStatus().addAll(updatedFlightStatus);
				ssimResponse
						.setUpdatedAdhocScheduleCount(ssimResponse.getUpdatedAdhocScheduleCount() + updatedFlightStatus.size());
			}

		} catch (Exception e) {
			log.error("ERROR @ REV updateMissingFlightsInRevisedSchedules");
		}
	}

	@SuppressWarnings("unchecked")
	private Collection<AdHocScheduleProcessStatus> generateCreatedCanceledFlightStatus(ServiceResponce serviceResponce) {

		Collection<AdHocScheduleProcessStatus> updatedFlightStatus = new ArrayList<>();
		Collection<Date> createdFlightDates = (Collection<Date>) serviceResponce
				.getResponseParam(CommandParamNames.CREATED_FLIGHT_DATES);
		Collection<Date> canceledFlightDates = (Collection<Date>) serviceResponce
				.getResponseParam(CommandParamNames.CANCELED_FLIGHT_DATES);

		Flight flightCopy = (Flight) serviceResponce.getResponseParam(CommandParamNames.FLIGHT);
		Collection<AdHocScheduleProcessStatus> createdFlightStatus = adaptAdHocScheduleProcessStatus(flightCopy,
				createdFlightDates, true);
		if (!createdFlightStatus.isEmpty()) {
			updatedFlightStatus.addAll(createdFlightStatus);
		}

		Collection<AdHocScheduleProcessStatus> canceledFlightStatus = adaptAdHocScheduleProcessStatus(flightCopy,
				canceledFlightDates, false);
		if (!canceledFlightStatus.isEmpty()) {
			updatedFlightStatus.addAll(canceledFlightStatus);
		}

		return updatedFlightStatus;

	}

	private Collection<AdHocScheduleProcessStatus> adaptAdHocScheduleProcessStatus(Flight flight, Collection<Date> flightDates,
			boolean isFlightCreated) {
		Collection<AdHocScheduleProcessStatus> flightProcessStatusResults = new ArrayList<>();
		if (flightDates != null && !flightDates.isEmpty()) {
			for (Date departureStartDate : flightDates) {
				AdHocScheduleProcessStatus adHocScheduleProcessStatus = new AdHocScheduleProcessStatus();
				adHocScheduleProcessStatus.setFlightNumber(flight.getFlightNumber());
				adHocScheduleProcessStatus.setLocalTimeMode(this.isLocalTimeMode);
				adHocScheduleProcessStatus.setDepartureDate(departureStartDate);
				if (isFlightCreated) {
					adHocScheduleProcessStatus.setStatus(FlightStatusEnum.CREATED.toString());
				} else {
					adHocScheduleProcessStatus.setStatus(FlightStatusEnum.CANCELLED.toString());
				}

				String originDestination = flight.getOriginAptCode() + "/" + flight.getDestinationAptCode();
				adHocScheduleProcessStatus.setOriginDestination(originDestination);
				adHocScheduleProcessStatus.setSuccessfullyProcessed(true);
				flightProcessStatusResults.add(adHocScheduleProcessStatus);
			}

		}

		return flightProcessStatusResults;

	}
}
