package com.isa.thinair.gdsservices.core.bl.internal.ticket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.TransitionTo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.common.Coupon;
import com.isa.thinair.gdsservices.api.dto.typea.internal.TicketingEventRq;
import com.isa.thinair.gdsservices.api.model.ExternalReservation;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.common.GDSServicesDAOUtils;
import com.isa.thinair.gdsservices.core.dto.temp.GdsReservation;
import com.isa.thinair.gdsservices.core.dto.temp.TravellerTicketingInformationWrapper;
import com.isa.thinair.gdsservices.core.persistence.dao.GDSTypeAServiceDAO;
import com.isa.thinair.gdsservices.core.util.GDSDTOUtil;
import com.isa.thinair.gdsservices.core.util.GDSServicesUtil;
import com.isa.thinair.gdsservices.core.util.GdsInfoDaoHelper;
import com.isa.thinair.gdsservices.core.util.KeyGenerator;
import com.isa.typea.common.TicketCoupon;
import com.isa.typea.common.TicketNumberDetails;
import com.isa.typea.common.TravellerTicketingInformation;

public class ValidatingCarrierTicketOperationsHandler extends TicketOperationsHandler {
	public void internalTicketReIssue(TicketingEventRq ticketingEventRq) throws ModuleException {

		LCCClientReservation reservation;
		GdsReservation<TravellerTicketingInformationWrapper> gdsReservation;
		String pnr = ticketingEventRq.getPnr();
		Map<Integer, Map<Integer, TransitionTo<Coupon>>> transitions = ticketingEventRq.getTransitions();

		GDSTypeAServiceDAO gdsTypeAServiceDAO = GDSServicesDAOUtils.DAOInstance.GDSTYPEASERVICES_DAO;

		gdsReservation =  GdsInfoDaoHelper.getGdsReservationTicketView(pnr);
		reservation = GDSServicesUtil.loadLccReservation(pnr);

		LCCClientReservationPax pax;
		TravellerTicketingInformationWrapper ticketTravellerImage;
		TravellerTicketingInformation ticketTraveller;
		TransitionTo<Coupon> transition;

		TicketNumberDetails ticketTransition;
		TicketCoupon couponTransition;
		Map<String, TicketNumberDetails> ticketTransitionsMap = new HashMap<String, TicketNumberDetails>();
		List<TicketNumberDetails> ticketTransitions = new ArrayList<TicketNumberDetails>();
		TicketNumberDetails ticketImage;
		TicketCoupon couponImage;
		String settlementAuthCode;

		for (Integer paxSeq : transitions.keySet()) {

			pax = getLccClientReservationPax(reservation, paxSeq);
			ticketTravellerImage = GDSDTOUtil.resolvePassenger(gdsReservation, pax);
			ticketTraveller = ticketTravellerImage.getTravellerInformation();

			settlementAuthCode = KeyGenerator.generateSettlementAuthorizationCode();

			for (Integer segSeq : transitions.get(paxSeq).keySet()) {

				transition = transitions.get(paxSeq).get(segSeq);

				ticketImage = GDSDTOUtil.resolveTicket(ticketTraveller, transition.getOldVal().getExternalTicketNumber());
				couponImage = GDSDTOUtil.resolveCoupon(ticketImage, String.valueOf(transition.getOldVal().getExternalCouponNumber()));

				couponImage.setStatus(TypeAConstants.CouponStatus.EXCHANGED);
				couponImage.setSettlementAuthCode(settlementAuthCode);

				if (!ticketTransitionsMap.containsKey(ticketImage.getTicketNumber())) {
					ticketTransition = new TicketNumberDetails();
					ticketTransition.setTicketNumber(ticketImage.getTicketNumber());
					ticketTransition.setDocumentType(ticketImage.getDocumentType());

					ticketTransitionsMap.put(ticketTransition.getTicketNumber(), ticketTransition);
					ticketTransitions.add(ticketTransition);
				}

				ticketTransition = ticketTransitionsMap.get(ticketImage.getTicketNumber());

				couponTransition = new TicketCoupon();
				couponTransition.setStatus(couponImage.getStatus());
				couponTransition.setCouponNumber(couponImage.getCouponNumber());
				couponTransition.setSettlementAuthCode(couponImage.getSettlementAuthCode());
				couponTransition.setOriginatorInformation(GDSDTOUtil.createOriginatorInformation());

				couponTransition.setFlightInfomation(couponImage.getFlightInfomation());
				couponTransition.setRelatedProductInfo(couponImage.getRelatedProductInfo());

				ticketTransition.getTicketCoupon().add(couponTransition);

				GDSDTOUtil.mergeCouponTransitions(ticketTravellerImage.getCouponTransitions(), ticketTransitions,
						GDSDTOUtil.createOriginatorInformation());

			}

		}

		GdsInfoDaoHelper.saveGdsReservationTicketView(pnr, gdsReservation);

		ExternalReservation externalReservation = gdsTypeAServiceDAO.gdsTypeAServiceGet(ExternalReservation.class, pnr);
		externalReservation.setReservationSynced(CommonsConstants.NO);
		gdsTypeAServiceDAO.gdsTypeAServiceSaveOrUpdate(externalReservation);

	}

	private LCCClientReservationPax getLccClientReservationPax(LCCClientReservation reservation, Integer paxSeq) {
		LCCClientReservationPax pax = null;

		for (LCCClientReservationPax lccPax : reservation.getPassengers()) {
			if (lccPax.getPaxSequence().equals(paxSeq)) {
				pax = lccPax;
				break;
			}
		}

		return pax;
	}
}
