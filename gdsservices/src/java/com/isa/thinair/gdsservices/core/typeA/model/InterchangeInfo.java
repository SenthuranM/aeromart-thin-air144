package com.isa.thinair.gdsservices.core.typeA.model;

public class InterchangeInfo {

	private String interchangeId;

	private String interchangeCode;

	public InterchangeInfo(String interchangeId, String interchangeCode) {
		super();
		this.interchangeId = interchangeId;
		this.interchangeCode = interchangeCode;
	}

	/**
	 * @return the interchangeId
	 */
	public String getInterchangeId() {

		return interchangeId;
	}

	/**
	 * @param interchangeId
	 *            the interchangeId to set
	 */
	public void setInterchangeId(String interchangeId) {
		this.interchangeId = interchangeId;
	}

	/**
	 * @return the interchangeInternalId
	 */
	public String getInterchangeCode() {
		return interchangeCode;
	}

	/**
	 * @param interchangeInternalId
	 *            the interchangeInternalId to set
	 */
	public void setInterchangeCode(String interchangeCode) {
		this.interchangeCode = interchangeCode;
	}

}
