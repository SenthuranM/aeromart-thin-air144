package com.isa.thinair.gdsservices.core.util;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.bl.common.ReservationProxy;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

public abstract class GDSServicesUtil {

	public static String getCarrierCode() {
		return AppSysParamsUtil.getDefaultCarrierCode();
	}

	public static LCCClientReservation loadLccReservation(String pnr) throws ModuleException {

		LCCClientReservation reservation = null;

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadLastUserNote(true);
		pnrModesDTO.setLoadLocalTimes(true);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setRecordAudit(false);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true);
		pnrModesDTO.setAppIndicator(ApplicationEngine.WS);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
		pnrModesDTO.setLoadExternalPaxPayments(true);
		pnrModesDTO.setLoadEtickets(true);
		pnrModesDTO.setLoadExternalPaxPayments(true);

		ModificationParamRQInfo paramRQInfo = new ModificationParamRQInfo();
		paramRQInfo.setAppIndicator(AppIndicatorEnum.APP_WS);

		reservation = GDSServicesModuleUtil.getAirproxyReservationBD().searchResByPNR(pnrModesDTO,
				paramRQInfo, null);

		return reservation;

	}

	public static Reservation loadReservation(String pnr) throws ModuleException {

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);

		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		return reservation;

	}

	public static long getReservationVersion(String pnr) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		Reservation reservation = ReservationProxy.getReservation(pnrModesDTO);

		return reservation.getVersion();
	}

	public static boolean allGdsETicketsIssued(ReservationPax pax) {
		boolean allGdsETicketsIssued = true;

		for (EticketTO eticketTO : pax.geteTickets()) {
			if (eticketTO.getExternalEticketNumber() == null || eticketTO.getExternalCouponNo() == null) {
				allGdsETicketsIssued = false;
				break;
			}
		}

		return allGdsETicketsIssued;
	}

	public static BigDecimal convertToBaseCurrency(String currencyCode, BigDecimal amount) throws ModuleException {
		Date exchangeRateByDate = CalendarUtil.getCurrentSystemTimeInZulu();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(exchangeRateByDate);
		CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(currencyCode);

		CurrencyExchangeRate baseCurrencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(AppSysParamsUtil.getBaseCurrency());

//		if (AppSysParamsUtil.isCurrencyRoundupEnabled()) {
			Currency currency = baseCurrencyExchangeRate.getCurrency();
			return AccelAeroRounderPolicy.convertAndRound(currencyExchangeRate.getExrateBaseToCurNumber(), amount,
					currency.getBoundryValue(), currency.getBreakPoint());
//		} else {
//			return AccelAeroCalculator.multiply(currencyExchangeRate.getExrateBaseToCurNumber(), amount);
//		}
	}

	public static BigDecimal convertFromBaseCurrency(String currencyCode, BigDecimal amount) throws ModuleException {

		Date exchangeRateByDate = CalendarUtil.getCurrentSystemTimeInZulu();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(exchangeRateByDate);
		CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(currencyCode);

//		if (AppSysParamsUtil.isCurrencyRoundupEnabled()) {
			Currency currency = currencyExchangeRate.getCurrency();
			return AccelAeroRounderPolicy.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(), amount,
					currency.getBoundryValue(), currency.getBreakPoint());
//		} else {
//			return AccelAeroCalculator.multiply(currencyExchangeRate.getMultiplyingExchangeRate(), amount);
//		}
	}

	public static String getBookingCode(String gdsCarrierCode, String gdsBookingCode) {
		Map<String, GDSStatusTO> gdsStatusMap = CommonsServices.getGlobalConfig().getActiveGdsMap();
		String bookingClass = null;
		for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
			if (gdsStatusTO.getCarrierCode() != null && gdsStatusTO.getCarrierCode().equals(gdsCarrierCode)) {
				bookingClass = getBookingCode(gdsStatusTO.getGdsId(), gdsBookingCode);
			}
		}
		if(bookingClass != null) {
			return bookingClass;
		}
		return gdsBookingCode;
	}

	public static String getGdsBookingCode(String gdsCarrierCode, String bookingCode) {
		Map<String, GDSStatusTO> gdsStatusMap = CommonsServices.getGlobalConfig().getActiveGdsMap();
		String gdsBookingClass = null;
		Map<Integer, Map<String, String>> gdsBookingClassMap;

		for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
			if (gdsStatusTO.getCarrierCode() != null && gdsStatusTO.getCarrierCode().equals(gdsCarrierCode)) {

				gdsBookingClassMap = CommonsServices.getGlobalConfig().getGdsBookingClassMap();

				if (gdsBookingClassMap.containsKey(gdsStatusTO.getGdsId()) &&
						gdsBookingClassMap.get(gdsStatusTO.getGdsId()).containsKey(bookingCode)) {
					gdsBookingClass = gdsBookingClassMap.get(gdsStatusTO.getGdsId()).get(bookingCode);
				}
			}
		}
		if(gdsBookingClass != null) {
			return gdsBookingClass;
		}
		return bookingCode;
	}

	private static String getBookingCode(Integer gdsId, String gdsBookingCode) {
		Map<Integer, Map<String, String>> gdsBookingClassMap = CommonsServices.getGlobalConfig().getGdsBookingClassMap();
		String bookingClass = null;
		if (!gdsBookingClassMap.get(gdsId).isEmpty()) {
			Map<String, String> bookingClassMap = gdsBookingClassMap.get(gdsId);
			for (Map.Entry<String, String> bcEntry : bookingClassMap.entrySet()) {
				if (bcEntry.getValue().equals(gdsBookingCode)) {
					bookingClass = bcEntry.getKey();
				}
			}
		}
		if (bookingClass != null) {
			return bookingClass;
		}
		return gdsBookingCode;
	}

	public static String getGDSPaymentType(String gdsCode) {
		Map<String, GDSStatusTO> gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
		for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
			if (gdsStatusTO.getGdsCode().equals(gdsCode)) {
				return gdsStatusTO.getPaymentType();
			}
		}
		return null;
	}
	
	public static boolean isGDSTaxRefundable(Integer gdsId) {
		if (gdsId != null) {
			Map<String, GDSStatusTO> gdsStatusMap = GDSServicesModuleUtil.getGlobalConfig().getActiveGdsMap();
			if (gdsStatusMap != null && !gdsStatusMap.isEmpty()) {
				return gdsStatusMap.get(gdsId.toString()).isGDSTaxRefundable();
			}
		}
		return false;
	}	
}

