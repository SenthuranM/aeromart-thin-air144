package com.isa.thinair.gdsservices.core.exception;

import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.gdsservices.core.util.TypeANotes;

public class GdsTypeAException extends Exception {

	private boolean attachError;
	private String ediErrorCode;
	private String errorMessageKey;
	private boolean noRollback;

	public GdsTypeAException() {
		this(TypeAConstants.ApplicationErrorCode.SYSTEM_ERROR,
				TypeANotes.ErrorMessages.GENERIC_ERROR);
	}

	public GdsTypeAException(String ediErrorCode) {
		this.attachError = true;
		this.ediErrorCode = ediErrorCode;
		this.errorMessageKey = TypeANotes.ErrorMessages.NO_MESSAGE;
	}

	public GdsTypeAException(String ediErrorCode, String errorMessageKey) {
		this.attachError = true;
		this.ediErrorCode = ediErrorCode;
		this.errorMessageKey = errorMessageKey;
	}

	public boolean attachError() {
		return attachError;
	}

	public void setAttachError(boolean attachError) {
		this.attachError = attachError;
	}

	public String getEdiErrorCode() {
		return ediErrorCode;
	}

	public void setEdiErrorCode(String ediErrorCode) {
		this.ediErrorCode = ediErrorCode;
	}

	public String getErrorMessageKey() {
		return errorMessageKey;
	}

	public void setErrorMessageKey(String errorMessageKey) {
		this.errorMessageKey = errorMessageKey;
	}

	public boolean isNoRollback() {
		return noRollback;
	}

	public void setNoRollback(boolean noRollback) {
		this.noRollback = noRollback;
	}
}
