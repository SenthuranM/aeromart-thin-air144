package com.isa.thinair.gdsservices.core.command;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.baggage.BaggageExternalChgDTO;
import com.isa.thinair.gdsservices.core.util.AncillaryUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.ReservationStatus;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.ISegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.TTYMessageUtil;
import com.isa.thinair.gdsservices.api.dto.internal.AddSegmentRequest;
import com.isa.thinair.gdsservices.api.dto.internal.RecordLocator;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.dto.internal.SegmentDetail;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.StatusCode;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.platform.api.ServiceResponce;

public class AddSegmentAction {

	private static Log log = LogFactory.getLog(AddSegmentAction.class);

	public static AddSegmentRequest processRequest(AddSegmentRequest addSegmentRequest) {
		try {
			// 1. Retrieve Reservation ---
			Reservation reservation = CommonUtil.loadReservation(addSegmentRequest.getGdsCredentialsDTO(), addSegmentRequest
					.getOriginatorRecordLocator().getPnrReference(), null);

			CommonUtil.validateReservationOwner(addSegmentRequest.getGdsCredentialsDTO(), reservation);
			// Need to allow modifications to confirmed reservations as well. Balance payment will happen through
			// SSR TKNE. So comment the validation
			// CommonUtil.validatePaymentOptions(reservation, addSegmentRequest.getPaymentDetail());

			List<FlightSegement> suggestedFlightSegments = null;

			for (SegmentDetail segmentDetail : addSegmentRequest.getSegmentDetails()) {
				String segmentCode = segmentDetail.getSegment().getSegmentCode();
				Date departureDateLocal = segmentDetail.getSegment().getDepartureDateTime();
				Date arrivalDateLocal = segmentDetail.getSegment().getArrivalDateTime();
				String flightNum = segmentDetail.getSegment().getFlightNumber();
				int flightSegCount = GDSServicesModuleUtil.getFlightBD().getFlightSegmentCount(flightNum, departureDateLocal,
						arrivalDateLocal, segmentCode, addSegmentRequest.getGdsCredentialsDTO().getAgentCode());
				if (flightSegCount == 0) {
					String strDepartureDate = CalendarUtil.formatSQLDate(departureDateLocal);
					suggestedFlightSegments = GDSServicesModuleUtil.getFlightBD().getSuggestedSegmentListForADate(flightNum,
							strDepartureDate, segmentCode);

					if (suggestedFlightSegments != null && suggestedFlightSegments.size() != 0) {
						String departureTime = CalendarUtil.getSegmentTime(suggestedFlightSegments.get(0)
								.getEstTimeDepatureLocal());
						String arrivalTime = CalendarUtil.getSegmentTime(suggestedFlightSegments.get(0).getEstTimeArrivalLocal());
						Date depTime = CalendarUtil.getSegmentTime(departureTime);
						Date arrTime = CalendarUtil.getSegmentTime(arrivalTime);
						segmentDetail.getSegment().setDepartureTime(depTime);
						segmentDetail.getSegment().setArrivalTime(arrTime);
						segmentDetail.getSegment().setArrivalDayOffset(CalendarUtil.getTimeDifferenceInDays(suggestedFlightSegments.get(0)
								.getEstTimeDepatureLocal(),suggestedFlightSegments.get(0).getEstTimeArrivalLocal()));
						segmentDetail.setChangeInSceduleExists(true);
					}
				}
			}

			Collection<SegmentDetail> segmentDetails = addSegmentRequest.getSegmentDetails();
			Collection<Segment> segments = CommonUtil.getSegments(segmentDetails);

			CommonUtil.checkForDuplicateSegments(segments, reservation);
			ServiceResponce serviceResponce = null;

			if (!AppSysParamsUtil.isRequoteEnabled()) {

				int[] totalPaxCountArr = new int[] { reservation.getTotalPaxAdultCount(), reservation.getTotalPaxChildCount(),
						reservation.getTotalPaxInfantCount() };

				Collection<OndFareDTO> collFares = GDSAASearchAssembler.getQuotedPrice(addSegmentRequest.getGdsCredentialsDTO(),
						segments, totalPaxCountArr, reservation.getGdsId());

				// 3. Block Seats ------------
				Collection collBlockID = GDSServicesModuleUtil.getReservationBD().blockSeats(collFares, null);

				// 4. Set Segment Information ---
				ISegment iSegmentAsm = getNewSegments(collFares, reservation);

				// 5. Payments -----------

				Map<String, BigDecimal> paxFareMap = CommonUtil.getTotalPaxPayments(collFares);
				Collection<ReservationPax> respassengers = reservation.getPassengers();

				for (ReservationPax pax : respassengers) {
					IPayment iPayment = new PaymentAssembler();

					BigDecimal amount = pax.getTotalAvailableBalance();

					if (pax.getPaxType().equals(PaxTypeTO.ADULT)) {
						if (pax.getAccompaniedPaxId() != null) {
							amount = amount.add((paxFareMap.get(PaxTypeTO.ADULT)).add(paxFareMap.get(PaxTypeTO.INFANT)));
						} else {
							amount = amount.add(paxFareMap.get(PaxTypeTO.ADULT));
						}
					} else if (pax.getPaxType().equals(PaxTypeTO.CHILD)) {
						amount = amount.add(paxFareMap.get(PaxTypeTO.CHILD));
					}

					iSegmentAsm.addPassengerPayments(pax.getPnrPaxId(), iPayment);
				}

				Integer intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS;
				TrackInfoDTO trackInfoDTO = CommonUtil.getTrackingInfo(addSegmentRequest.getGdsCredentialsDTO());
				trackInfoDTO.setPaymentAgent(addSegmentRequest.getGdsCredentialsDTO().getAgentCode());
				serviceResponce = GDSServicesModuleUtil.getReservationSegmentBD().addSegments(reservation.getPnr(), iSegmentAsm,
						collBlockID, intPaymentMode, reservation.getVersion(), trackInfoDTO, false, false, null, null, false,
						false, true);

			} else {

				FlightPriceRQ flightPriceRQ = createFlightPriceRQ(addSegmentRequest, reservation);
				FlightPriceRS flightPriceRS = GDSServicesModuleUtil.getAirproxyReservationQueryBD().quoteFlightPrice(
						flightPriceRQ, null);
				if (flightPriceRS != null) {
					PriceInfoTO priceInfoTO = flightPriceRS.getSelectedPriceFlightInfo();

					if (priceInfoTO != null && priceInfoTO.getFareTypeTO() != null) {
						int ondLength = flightPriceRS.getOriginDestinationInformationList().size();
						boolean faresAvailable = false;
						if (ondLength == priceInfoTO.getFareTypeTO().getOndWiseFareType().size()) {
							for (int i = 0; i < ondLength; i++) {
								if (priceInfoTO.getFareTypeTO().getOndWiseFareType().get(i) == FareTypes.NO_FARE) {
									faresAvailable = false;
									break;
								}
								faresAvailable = true;
							}
						}
						if (faresAvailable) {

							CustomChargesTO customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
							RequoteModifyRQ requoteModifyRQ = new RequoteModifyRQ();
							requoteModifyRQ.setPnr(reservation.getPnr());
							requoteModifyRQ.setCustomCharges(customChargesTO);
							requoteModifyRQ.setGroupPnr(reservation.getOriginatorPnr());
							requoteModifyRQ.setVersion(Long.toString(reservation.getVersion()));

							QuotedFareRebuildDTO fareInfo = new QuotedFareRebuildDTO(priceInfoTO.getFareSegChargeTO(),
									flightPriceRQ, null);
							requoteModifyRQ.setFareInfo(fareInfo);
							requoteModifyRQ.setLastFareQuoteDate(priceInfoTO.getLastFareQuotedDate());
							requoteModifyRQ.setFQWithinValidity(priceInfoTO.isFQWithinValidity());
							requoteModifyRQ
									.setPaymentType(ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS);							
							requoteModifyRQ.setCodeShareFlightDTOs(CommonUtil.createCodeShareFlightInfo(segments, reservation.getSegmentsView()));
							ReservationBalanceTO reservationBalanceTO = GDSServicesModuleUtil.getAirproxyReservationBD()
									.getRequoteBalanceSummary(requoteModifyRQ,
											CommonUtil.getTrackingInfo(addSegmentRequest.getGdsCredentialsDTO()));
							if (!reservationBalanceTO.hasBalanceToPay()) {
								requoteModifyRQ.setNoBalanceToPay(true);
							}

							addAncillaries(requoteModifyRQ, flightPriceRS, addSegmentRequest.getSegmentDetails(), reservation);

							serviceResponce = GDSServicesModuleUtil.getReservationBD().requoteModifySegments(
									requoteModifyRQ, CommonUtil.getTrackingInfo(addSegmentRequest.getGdsCredentialsDTO()));

						} else {
							throw new ModuleException("airinventory.logic.bl.fares.not.available");
						}
					} else {
						String flightNumbers = "";
						int i = 0;
						for (Segment segment : segments) {
							if (i == 0) {
								flightNumbers += " " + segment.getFlightNumber();
							} else {
								flightNumbers += " , " + segment.getFlightNumber();
							}
							i++;
						}
						throw new ModuleException("airinventory.logic.bl.fares.not.available", flightNumbers);
					}
				}
			}
			if (serviceResponce != null && serviceResponce.isSuccess()) {
				String strPNR = (String) serviceResponce.getResponseParam(CommandParamNames.PNR);
				RecordLocator recordLocator = new RecordLocator();
				recordLocator.setPnrReference(strPNR);
				addSegmentRequest.setResponderRecordLocator(recordLocator);
				addSegmentRequest.setSuccess(true);
				updateSegmentStatus(addSegmentRequest.getSegmentDetails());
				Reservation modifiedReservation = CommonUtil.loadReservation(addSegmentRequest.getGdsCredentialsDTO(),
						addSegmentRequest.getOriginatorRecordLocator().getPnrReference(), null);
				if (modifiedReservation.getStatus().equals(ReservationStatus.OHD.toString())) {
					String message = "";
					Date releaseTimeStamp = modifiedReservation.getReleaseTimeStamps()[0];
					if (GDSServicesModuleUtil.getConfig().isShowPaymentDetailsInTTYMessage()) {
						message = CommonUtil.getFormattedMessage(GDSInternalCodes.ResponseMessageCode.CONFIRMED_ON_HOLD,
								modifiedReservation.getLastCurrencyCode(), modifiedReservation.getTotalAvailableBalance(),
								releaseTimeStamp);
					} else {
						message = CommonUtil.getFormattedMessage(
								GDSInternalCodes.ResponseMessageCode.CONFIRMED_ON_HOLD_NO_PAYMENT_DETAILS, releaseTimeStamp);
					}
					if (addSegmentRequest.isNotSupportedSSRExists()) {
						message = message
								+ MessageUtil.getMessage(GDSInternalCodes.ResponseMessageCode.SSR_NOT_SUPPORTED.getCode());
					}
					addSegmentRequest.setResponseMessage(message);
				}
			} else {
				addSegmentRequest.setSuccess(false);
				addSegmentRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
				addSegmentRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
			}
		} catch (ModuleException e) {
			log.error(" AddSegmentAction Failed for GDS PNR " + addSegmentRequest.getOriginatorRecordLocator().getPnrReference(),
					e);
			CommonUtil.setSegmentStatusCodes(addSegmentRequest.getSegmentDetails(), GDSInternalCodes.StatusCode.NO_ACTION);
			addSegmentRequest.setSuccess(false);
			if (e.getModuleCode() != null) {
				addSegmentRequest.setResponseMessage(e.getMessageString() + e.getModuleCode());
			} else {
				addSegmentRequest.setResponseMessage(e.getMessageString());
			}
			addSegmentRequest.addErrorCode(e.getExceptionCode());
		} catch (Exception e) {
			log.error(" AddSegmentAction Failed for GDS PNR " + addSegmentRequest.getOriginatorRecordLocator().getPnrReference(),
					e);
			CommonUtil.setSegmentStatusCodes(addSegmentRequest.getSegmentDetails(), GDSInternalCodes.StatusCode.NO_ACTION);
			addSegmentRequest.setSuccess(false);
			addSegmentRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
			addSegmentRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
		}

		return addSegmentRequest;
	}

	/**
	 * Transform OTA Air price request to Air Proxy Request
	 * 
	 * @param otaAirPriceRQ
	 * @return
	 * @throws ModuleException
	 */
	private static FlightPriceRQ createFlightPriceRQ(AddSegmentRequest addSegmentRequest, Reservation reservation)
			throws ModuleException {

		Collection<SegmentDetail> segmentDetails = addSegmentRequest.getSegmentDetails();
		Collection<Segment> segments = CommonUtil.getSegments(segmentDetails);
		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();

		Collection<String> bookingClassSet = new HashSet<String>();

		Collection<ReservationSegmentDTO> resSegments = reservation.getSegmentsView();
		LinkedHashMap<Integer, List<ReservationSegmentDTO>> segmentListMap = new LinkedHashMap<Integer, List<ReservationSegmentDTO>>();
		for (ReservationSegmentDTO bookFlightSegment : resSegments) {
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(bookFlightSegment.getStatus())) {

				if (segmentListMap.get(bookFlightSegment.getJourneySeq()) != null) {
					List<ReservationSegmentDTO> segmentList = segmentListMap.get(bookFlightSegment.getJourneySeq());
					segmentList.add(bookFlightSegment);
					Collections.sort(segmentList);
				} else {
					List<ReservationSegmentDTO> segmentList = new ArrayList<ReservationSegmentDTO>();
					segmentList.add(bookFlightSegment);
					segmentListMap.put(bookFlightSegment.getJourneySeq(), segmentList);
				}
			}
		}

		for (Entry<Integer, List<ReservationSegmentDTO>> entry : segmentListMap.entrySet()) {
			List<ReservationSegmentDTO> segList = entry.getValue();
			if (segList != null && segList.size() > 0) {
				OriginDestinationInformationTO depatureOnD = flightPriceRQ.addNewOriginDestinationInformation();
				ReservationSegmentDTO firstFlightSegment = BeanUtils.getFirstElement(segList);
				ReservationSegmentDTO lastFlightSegment = BeanUtils.getFirstElement(segList);
				depatureOnD.setDepartureDateTimeStart(firstFlightSegment.getDepartureDate());
				depatureOnD.setOrigin(firstFlightSegment.getOrigin());
				depatureOnD.setDestination(lastFlightSegment.getDestination());
				depatureOnD.setPreferredClassOfService(firstFlightSegment.getCabinClassCode());
				depatureOnD.setPreferredLogicalCabin(firstFlightSegment.getLogicalCCCode());
				String bookingCode = null;
				if (firstFlightSegment.getFareTO() != null && firstFlightSegment.getFareTO().getBookingClassCode() != null) {
					bookingCode = firstFlightSegment.getFareTO().getBookingClassCode();
					String actualBookingCode = TTYMessageUtil.getActualBCForGdsMappedBC(reservation.getGdsId(), bookingCode);
					depatureOnD.setPreferredBookingClass(actualBookingCode);
				}
				Date depatureDate = depatureOnD.getDepartureDateTimeStart();
				Date depatureDateTimeStart = CalendarUtil.getStartTimeOfDate(depatureDate);
				Date depatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(depatureDate);
				depatureOnD.setPreferredDate(depatureDate);
				depatureOnD.setDepartureDateTimeStart(depatureDateTimeStart);
				depatureOnD.setDepartureDateTimeEnd(depatureDateTimeEnd);
				depatureOnD.getExistingFlightSegIds().add(firstFlightSegment.getFlightSegId());
				depatureOnD.getExistingPnrSegRPHs().add(FlightRefNumberUtil.composePnrSegRPH(firstFlightSegment));
				depatureOnD.getOrignDestinationOptions().add(CommonUtil.setFlightSegmentTo(segList));
			}
		}

		// setting the new segments information
		for (Segment segment : segments) {
			Date returnDate = segment.getDepartureDateTime();
			OriginDestinationInformationTO returnOnD = flightPriceRQ.addNewOriginDestinationInformation();
			Date returnDateTimeStart = CalendarUtil.getStartTimeOfDate(returnDate);

			Date returnDateTimeEnd = CalendarUtil.getEndTimeOfDate(returnDate);
			returnOnD.setPreferredDate(returnDate);
			returnOnD.setDepartureDateTimeStart(returnDateTimeStart);
			returnOnD.setDepartureDateTimeEnd(returnDateTimeEnd);
			returnOnD.setOrigin(segment.getDepartureStation());
			returnOnD.setDestination(segment.getArrivalStation());
			String bookingCode = TTYMessageUtil.getActualBCForGdsMappedBC(reservation.getGdsId(), segment.getBookingCode());
			returnOnD.setPreferredBookingClass(bookingCode);
			String cabinClass = GDSServicesModuleUtil.getBookingClassBD().getCabinClassForBookingClass(bookingCode);
			returnOnD.setPreferredClassOfService(cabinClass);
			returnOnD.setPreferredLogicalCabin(cabinClass);

			returnOnD.getOrignDestinationOptions().add(CommonUtil.setFlightSegmentTo(segment));
		}

		// Set pax information
		CommonUtil.getTravelInfomationSummary(reservation, flightPriceRQ);
		AvailPreferencesTO availPref = flightPriceRQ.getAvailPreferences();
		CommonUtil.setAvailPreference(availPref, addSegmentRequest.getGdsCredentialsDTO());

		// Setting Travel preference information such as booking class and booking type
		TravelPreferencesTO travelerPref = flightPriceRQ.getTravelPreferences();
		CommonUtil.setTravelPreference(bookingClassSet, travelerPref);

		// TODO set modify reservation information
		return flightPriceRQ;
	}

	/**
	 * gets new segments
	 * 
	 * @param colOndFareDTO
	 * @param oldReservation
	 * @param strFareQuotes
	 * @return
	 * @throws ModuleException
	 */
	private static ISegment getNewSegments(Collection<OndFareDTO> colOndFareDTO, Reservation reservation) throws ModuleException {

		ISegment iSegmentAsm = new SegmentAssembler(colOndFareDTO);

		int iNextFareGroupId = getNextONDGroupId(reservation);
		int iNextSegSequanceId = getNextSegSequanceId(reservation);

		for (OndFareDTO ondFare : colOndFareDTO) {
			Set tempSegIds = ondFare.getSegmentsMap().keySet();

			for (Iterator iterator = tempSegIds.iterator(); iterator.hasNext();) {
				FlightSegmentDTO element = ondFare.getSegmentsMap().get(iterator.next());

				if (ondFare.isInBoundOnd()) {
					iSegmentAsm.addReturnSegment((OndSequence.IN_BOUND + 1), iNextSegSequanceId, element.getSegmentId(),
							iNextFareGroupId, null, null, null, ondFare.getSelectedBundledFarePeriodId(), null, null);
				} else {
					iSegmentAsm.addOutgoingSegment((OndSequence.OUT_BOUND + 1), iNextSegSequanceId, element.getSegmentId(),
							iNextFareGroupId, null, null, ondFare.getSelectedBundledFarePeriodId(), null, null);
				}
				iNextSegSequanceId++;
			}
			iNextFareGroupId++;
		}

		return iSegmentAsm;
	}

	/**
	 * gets a next fare group id
	 * 
	 * @param Reservation
	 * @return
	 */
	private static int getNextONDGroupId(Reservation oldReservation) {
		int nextONDGroupId = 0;

		Set<ReservationSegment> colSegs = oldReservation.getSegments();

		for (ReservationSegment reservationSegment : colSegs) {
			Integer ondGroupId = reservationSegment.getOndGroupId();
			if (ondGroupId != null && nextONDGroupId < ondGroupId.intValue()) {
				nextONDGroupId = reservationSegment.getOndGroupId().intValue();
			}
		}

		nextONDGroupId++;

		return nextONDGroupId;
	}

	/**
	 * gets the next segment sequence id
	 * 
	 * @param Reservation
	 * @return
	 */
	private static int getNextSegSequanceId(Reservation oldReservation) {
		int iNextSegSequanceId = 0;

		Set<ReservationSegment> colSegs = oldReservation.getSegments();

		for (ReservationSegment reservationSegment : colSegs) {
			Integer segSeqId = reservationSegment.getSegmentSeq();
			if (segSeqId != null && iNextSegSequanceId < segSeqId.intValue()) {
				iNextSegSequanceId = segSeqId.intValue();
			}
		}

		iNextSegSequanceId++;

		return iNextSegSequanceId;
	}

	private static void updateSegmentStatus(Collection<SegmentDetail> segments) {
		for (SegmentDetail segment : segments) {
			if (segment.isChangeInSceduleExists()) {
				segment.getSegment().setStatusCode(StatusCode.CONFIRMED_SCHEDULE_CHANGED);
			} else {
				segment.getSegment().setStatusCode(StatusCode.CONFIRMED);
			}
		}
	}

	private static void addAncillaries(RequoteModifyRQ requoteModifyRQ, FlightPriceRS flightPriceRS, Collection<SegmentDetail> segmentDetails, Reservation reservation) throws ModuleException {

		BookingClassBD bookingClassBD = GDSServicesModuleUtil.getBookingClassBD();
		BookingClass bookingClass;


		List<FlightSegmentTO> flightSegments = new ArrayList<FlightSegmentTO>();
		FlightSegmentTO flightSegment;

		for (SegmentDetail newSegment : segmentDetails) {
			flightSegment = new FlightSegmentTO();

			l1:
			for (OriginDestinationInformationTO ond : flightPriceRS.getOriginDestinationInformationList()) {
				for(OriginDestinationOptionTO ondOption : ond.getOrignDestinationOptions()) {
					for (FlightSegmentTO flightSegmentTO : ondOption.getFlightSegmentList()) {

						if (newSegment.getSegment().getDepartureDateTime().compareTo(flightSegmentTO.getDepartureDateTime()) == 0 &&
								newSegment.getSegment().getArrivalDateTime().compareTo(flightSegmentTO.getArrivalDateTime()) == 0 &&
								newSegment.getSegment().getFlightNumber().equals(flightSegmentTO.getFlightNumber()) &&
								newSegment.getSegment().getSegmentCode().equals(flightSegmentTO.getSegmentCode())) {

							flightSegment.setFlightNumber(flightSegmentTO.getFlightNumber());
							flightSegment.setDepartureDateTime(flightSegmentTO.getDepartureDateTime());
							flightSegment.setDepartureDateTimeZulu(flightSegmentTO.getDepartureDateTimeZulu());
							flightSegment.setArrivalDateTime(flightSegmentTO.getArrivalDateTime());
							flightSegment.setArrivalDateTimeZulu(flightSegmentTO.getArrivalDateTimeZulu());
							flightSegment.setSegmentCode(flightSegmentTO.getSegmentCode());
							flightSegment.setFlightSegId(flightSegmentTO.getFlightSegId());
							flightSegment.setOperatingAirline(AppSysParamsUtil.getDefaultCarrierCode());
							flightSegment.setOndSequence(flightSegmentTO.getOndSequence());
							flightSegment.setReturnFlag(false);
							flightSegment.setBookingClass(ond.getPreferredBookingClass());

							bookingClass = bookingClassBD.getBookingClass(ond.getPreferredBookingClass());

							flightSegment.setCabinClassCode(bookingClass.getCabinClassCode());
							flightSegment.setLogicalCabinClassCode(bookingClass.getLogicalCCCode());
							// flightSegment.setOndSequence(OndSequence.OUT_BOUND);
							flightSegment.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentTO));
							flightSegment.setAirportCode(flightSegmentTO.getAirportCode());

							flightSegments.add(flightSegment);

							break l1;
						}
					}
				}
			}

		}


		List<Integer> adultChildList = new ArrayList<Integer>();
		for (ReservationPax pax : reservation.getPassengers()) {
			if (pax.getPaxType().equals(PaxTypeTO.ADULT) || pax.getPaxType().equals(PaxTypeTO.CHILD)) {
				adultChildList.add(pax.getPaxSequence());
			}

		}

		Map<Integer, List<ExternalChgDTO>> baggageExternalCharges = AncillaryUtil
				.getPaxwiseBaggageExternalCharges(flightSegments, adultChildList, reservation.getGdsId());


		if (baggageExternalCharges != null) {
			BaggageExternalChgDTO baggageExternalChgDTO = new BaggageExternalChgDTO();
			baggageExternalChgDTO.setExternalChargesEnum(ReservationInternalConstants.EXTERNAL_CHARGES.BAGGAGE);
			Map<ReservationInternalConstants.EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap = new HashMap<>();
			externalChargesMap.put(ReservationInternalConstants.EXTERNAL_CHARGES.BAGGAGE, baggageExternalChgDTO);
			requoteModifyRQ.setExternalChargesMap(externalChargesMap);

			Map<String, LCCClientPaymentAssembler> lccPassengerPayments = new HashMap<>();
			requoteModifyRQ.setLccPassengerPayments(lccPassengerPayments);

			Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = new HashMap<>();
			requoteModifyRQ.setPaxExtChgMap(paxExtChgMap);

			for (ReservationPax pax : reservation.getPassengers()) {

				LCCClientPaymentAssembler lccClientPaymentAssembler = new LCCClientPaymentAssembler();
				lccClientPaymentAssembler.setPaxType(pax.getPaxType());
				lccPassengerPayments.put(PaxTypeUtils.travelerReference(pax), lccClientPaymentAssembler);

				paxExtChgMap.put(pax.getPaxSequence(), new ArrayList<>());

				if (baggageExternalCharges.containsKey(pax.getPaxSequence())) {
					Collection<BaggageExternalChgDTO> externalChgDTOs = (Collection)baggageExternalCharges.get(pax.getPaxSequence());

					for (BaggageExternalChgDTO externalChgDTO : externalChgDTOs) {
						LCCClientExternalChgDTO lccClientExternalChgDTO = new LCCClientExternalChgDTO();
						lccClientExternalChgDTO.setExternalCharges(ReservationInternalConstants.EXTERNAL_CHARGES.BAGGAGE);
						lccClientExternalChgDTO.setAmount(externalChgDTO.getAmount());
						lccClientExternalChgDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());

						for (FlightSegmentTO flightSegmentTO : flightSegments) {
							if (flightSegmentTO.getFlightSegId().equals(externalChgDTO.getFlightSegId())) {
								lccClientExternalChgDTO.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
								break;
							}
						}
						lccClientExternalChgDTO.setCode(externalChgDTO.getBaggageCode());
						lccClientExternalChgDTO.setOndBaggageChargeGroupId(externalChgDTO.getOndBaggageChargeGroupId());
						lccClientExternalChgDTO.setOndBaggageGroupId(externalChgDTO.getOndBaggageGroupId());

						lccClientPaymentAssembler.addExternalCharges(lccClientExternalChgDTO);

						paxExtChgMap.get(pax.getPaxSequence()).add(lccClientExternalChgDTO);

					}
				}
			}
		}

	}


}
