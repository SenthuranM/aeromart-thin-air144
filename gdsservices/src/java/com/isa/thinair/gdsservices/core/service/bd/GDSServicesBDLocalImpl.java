/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.gdsservices.api.service.GDSServicesBD;

/**
 * Implementation of all the methods required to access the GDS Services <strong> Local Access </strong>
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
@Local
public interface GDSServicesBDLocalImpl extends GDSServicesBD {

}
