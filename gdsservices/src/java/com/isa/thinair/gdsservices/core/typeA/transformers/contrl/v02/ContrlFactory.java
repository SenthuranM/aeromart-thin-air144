package com.isa.thinair.gdsservices.core.typeA.transformers.contrl.v02;

import static com.isa.thinair.gdsservices.core.util.IATADataExtractionUtil.setValue;
import iata.typea.v021.contrl.ObjectFactory;
import iata.typea.v021.contrl.Type0029;
import iata.typea.v021.contrl.Type0051;
import iata.typea.v021.contrl.Type0065;
import iata.typea.v021.contrl.UCI;
import iata.typea.v021.contrl.UCM;
import iata.typea.v021.contrl.UNB;
import iata.typea.v021.contrl.UNH;
import iata.typea.v021.contrl.UNT;
import iata.typea.v021.contrl.UNZ;

import java.util.Date;

import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;
import com.isa.thinair.gdsservices.api.exception.InteractiveEDIException;
import com.isa.thinair.gdsservices.api.model.IATAOperation;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.config.TypeAMessageConfig;
import com.isa.thinair.gdsservices.core.typeA.model.InterchangeHeader;
import com.isa.thinair.gdsservices.core.typeA.model.InterchangeInfo;
import com.isa.thinair.gdsservices.core.typeA.model.MessageHeader;
import com.isa.thinair.gdsservices.core.util.IATADataExtractionUtil;
import com.isa.thinair.gdsservices.core.util.RandomANGen;
import com.isa.thinair.gdsservices.core.util.TypeAMessageUtil;

public class ContrlFactory {

	private static final ObjectFactory objectFactory = new ObjectFactory();
	private static final ContrlFactory contrlFactory = new ContrlFactory();

	private ContrlFactory() {
		super();
	}

	public static ContrlFactory getInstance() {
		return contrlFactory;
	}

	public UNB createUNB(InterchangeHeader interchangeHeader, String interchangeControllRef) throws InteractiveEDIException {
		TypeAMessageConfig typeAMessageConfig = GDSServicesModuleUtil.getConfig().getTypeAMessageConfig();

		UNB unb = objectFactory.createUNB();
		// syntax id and version

		setValue(objectFactory, unb, typeAMessageConfig.getSyntaxIdentifier(), 1, 1);
		setValue(objectFactory, unb, typeAMessageConfig.getSyntaxVersion(), 1, 2);

		// sender info
		if (interchangeHeader.getRecipient().getInterchangeId() != null) {
			setValue(objectFactory, unb, interchangeHeader.getRecipient().getInterchangeId(), 2, 1);
		} else {
			setValue(objectFactory, unb, typeAMessageConfig.getOwnId(), 2, 1);
			setValue(objectFactory, unb, typeAMessageConfig.getOwnCode(), 2, 3);
		}

		// recipient info
		if (interchangeHeader.getSender().getInterchangeId() != null) {
			setValue(objectFactory, unb, interchangeHeader.getSender().getInterchangeId(), 3, 1);
		} else {
			setValue(objectFactory, unb, typeAMessageConfig.getRemoteId(), 3, 1);
			setValue(objectFactory, unb, typeAMessageConfig.getRemoteCode(), 3, 3);
		}

		// This date and time need to be given in GMT
		String[] dateAsString = IATADataExtractionUtil.getDateAsString(new Date(), true);
		setValue(objectFactory, unb, dateAsString[0], 4, 1);
		setValue(objectFactory, unb, dateAsString[1], 4, 2);

		// interchange controll ref of sender
		setValue(objectFactory, unb, interchangeControllRef, 5);

		// recipient ref
		setValue(objectFactory, unb, interchangeHeader.getRecipientRef(), 6, 1);

		// set association code for errouns response
		setValue(objectFactory, unb, Type0029.I, 8);

		return unb;

	}

	public UNH createUNH(MessageHeader messageHeader) throws InteractiveEDIException {
		String car;
		TypeAMessageConfig typeAMessageConfig = GDSServicesModuleUtil.getConfig().getTypeAMessageConfig();

		UNH unh = objectFactory.createUNH();

		// Message ref number . Allways 1
		setValue(objectFactory, unh, String.valueOf(1), 1);

		// Message type and version
		String opCode = IATAOperation.CONTRL.toString();
		setValue(objectFactory, unh, Type0065.valueOf(opCode), 2, 1);
		setValue(objectFactory, unh, String.valueOf(messageHeader.getMessageIdentifier().getVersion()), 2, 2);
		setValue(objectFactory, unh, String.valueOf(messageHeader.getMessageIdentifier().getRelNo()), 2, 3);
		setValue(objectFactory, unh, Type0051.IA, 2, 4);

		// common access reference
		String carRemote = messageHeader.getCommonAccessReference();
		if (TypeAMessageUtil.containsResponseKey(carRemote)) {
			car = carRemote;
		} else {
			String carOwn = new RandomANGen(14).nextString("");
			car = carRemote.concat("/").concat(carOwn);
		}
		setValue(objectFactory, unh, car, 3);

		return unh;

	}

	public UCI createUCI(InterchangeHeader interchangeHeader, CodeSetEnum.CS0085 error) throws InteractiveEDIException {
		UCI uci = objectFactory.createUCI();

		if (interchangeHeader != null) {
			// original Recipient ref from sender
			setValue(objectFactory, uci, interchangeHeader.getRecipientRef(), 1);

			InterchangeInfo originalSender = interchangeHeader.getSender();
			InterchangeInfo originalRecipient = interchangeHeader.getRecipient();

			// original sender
			setValue(objectFactory, uci, originalSender.getInterchangeId(), 2, 1);
			setValue(objectFactory, uci, originalSender.getInterchangeCode(), 2, 3);

			// original recipient
			setValue(objectFactory, uci, originalRecipient.getInterchangeId(), 3, 1);
			setValue(objectFactory, uci, originalRecipient.getInterchangeCode(), 3, 3);
		}

		// action
		setValue(objectFactory, uci, String.valueOf(error.getAction()), 4);

		return uci;
	}

	public UCM createUCM(MessageHeader messageHeader, CodeSetEnum.CS0085 error) throws InteractiveEDIException {
		UCM ucm = objectFactory.createUCM();

		// original Recipient ref from sender
		setValue(objectFactory, ucm, String.valueOf(1), 1);

		IATAOperation messageType = messageHeader.getMessageIdentifier().getMessageType().getResponse();
		TypeAMessageConfig typeAMessageConfig = GDSServicesModuleUtil.getConfig().getTypeAMessageConfig();
		// message identifier
		setValue(objectFactory, ucm, Type0065.valueOf(messageType.toString()), 2, 1);
		setValue(objectFactory, ucm, String.valueOf(messageHeader.getMessageIdentifier().getVersion()), 2, 2);
		setValue(objectFactory, ucm, String.valueOf(messageHeader.getMessageIdentifier().getRelNo()), 2, 3);
		setValue(objectFactory, ucm, Type0051.IA, 2, 4);

		// action
		setValue(objectFactory, ucm, Integer.valueOf(4).toString(), 3);
		// syntax error
		setValue(objectFactory, ucm, String.valueOf(error.getAction()), 4);

		return ucm;
	}

	public UNT createUNT(MessageHeader messageHeader) throws InteractiveEDIException {
		UNT unt = objectFactory.createUNT();
		setValue(objectFactory, unt, messageHeader.getMessageReferenceNumber(), 2);
		return unt;
	}

	@Deprecated
	public UNZ createUNZ(InterchangeHeader interchangeHeader, String interchangeControllRef) throws InteractiveEDIException {
		UNZ unz = objectFactory.createUNZ();
		setValue(objectFactory, unz, String.valueOf(1), 1);
		setValue(objectFactory, unz, interchangeControllRef, 2);
		return unz;
	}

	/**
	 * @return the objectfactory
	 */
	public ObjectFactory getObjectfactory() {
		return objectFactory;
	}

}
