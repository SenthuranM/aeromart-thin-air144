package com.isa.thinair.gdsservices.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.gdsservices.api.service.GDSNotifyBD;

@Remote
public interface GDSNotifyBDImpl extends GDSNotifyBD {
}
