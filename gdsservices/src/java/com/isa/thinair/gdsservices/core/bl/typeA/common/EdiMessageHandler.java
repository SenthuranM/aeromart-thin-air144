package com.isa.thinair.gdsservices.core.bl.typeA.common;

import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.core.dto.EDIMessageDTO;
import com.isa.thinair.platform.api.ServiceResponce;

public abstract class EdiMessageHandler extends DefaultBaseCommand {

	protected EDIMessageDTO messageDTO;
	protected ConstraintsValidator constraintsValidator;

	public ServiceResponce execute() {

		messageDTO = (EDIMessageDTO) getParameter(TypeACommandParamNames.EDI_REQUEST_DTO);
		constraintsValidator = new ConstraintsValidator(this);

		handleMessage();

		ServiceResponce sr = new DefaultServiceResponse();
		sr.addResponceParam(TypeACommandParamNames.EDI_RESPONSE_DTO, messageDTO.getResponse());
		sr.addResponceParam(TypeACommandParamNames.ROLLBACK_TRANSACTION, messageDTO.isRollbackTransaction());
		return sr;
	}

	protected abstract void handleMessage();

	protected Set<String> getAllowedMessageFunctions() {
		return new HashSet<String>();
	}



}
