package com.isa.thinair.gdsservices.core.typeA.transformers.tkcreqres.v03;

import static com.isa.thinair.gdsservices.core.util.IATADataExtractionUtil.setValue;
import iata.typea.v031.tkcres.EQN;
import iata.typea.v031.tkcres.GROUP3;
import iata.typea.v031.tkcres.IATA;
import iata.typea.v031.tkcres.MON;
import iata.typea.v031.tkcres.MON.MON02;
import iata.typea.v031.tkcres.MSG;
import iata.typea.v031.tkcres.ObjectFactory;
import iata.typea.v031.tkcres.RCI;
import iata.typea.v031.tkcres.RCI.RCI01;
import iata.typea.v031.tkcres.TIF;
import iata.typea.v031.tkcres.TIF.TIF02;
import iata.typea.v031.tkcres.TKCRES;
import iata.typea.v031.tkcres.UNB;
import iata.typea.v031.tkcres.UNH;
import iata.typea.v031.tkcres.UNT;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBElement;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.dto.typea.ReservationDisplayResult;
import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.api.util.TypeACommandParamNames;
import com.isa.thinair.gdsservices.api.util.TypeAResponseCode;
import com.isa.thinair.gdsservices.core.typeA.model.InterchangeHeader;
import com.isa.thinair.gdsservices.core.typeA.model.MessageHeader;
import com.isa.thinair.gdsservices.core.util.RandomANGen;
import com.isa.thinair.gdsservices.core.util.TypeADataConvertorUtill;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class TicketDisplayResponseHandler {

	public static JAXBElement<?> constructEDIResponce(DefaultServiceResponse response) throws ModuleException {

		ReservationDisplayResult displayResults = (ReservationDisplayResult) response
				.getResponseParam(TypeAResponseCode.TICKET_DISPLAY_RESULTS);
		InterchangeHeader iHeader = (InterchangeHeader) response.getResponseParam(TypeACommandParamNames.IC_HEADER);
		MessageHeader mHeader = (MessageHeader) response.getResponseParam(TypeACommandParamNames.MESSAGE_HEADER);
		String msgFunctionCode = (String) response.getResponseParam(TypeACommandParamNames.MESSAGE_FUNCTION);

		if (displayResults.isErrorResponse()) {
			return constructErrorResponse(iHeader, mHeader, displayResults.getAppErrors());
		}

		List<Reservation> reservations = displayResults.getReservations();
		// we got a exactMatch
		if (reservations.size() == 1) {
			Reservation reservation = PlatformUtiltiies.getFirstElement(reservations);
			return constructDetailResponse(iHeader, mHeader, msgFunctionCode, reservation);

		}// Multiple reseults fount . responding back with list display
		else {
			return constructListRespomse(iHeader, mHeader, reservations);
		}
	}

	/**
	 * construct detail response for exact match
	 * 
	 * @param mHeader
	 * @param iHeader
	 * @param msgFunctionCode
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	private static JAXBElement<?> constructDetailResponse(InterchangeHeader iHeader, MessageHeader mHeader,
			String msgFunctionCode, Reservation reservation) throws ModuleException {

		TkcresFactory tkcresFactory = TkcresFactory.getInstance();
		ObjectFactory objectFactory = tkcresFactory.getObjectFactory();

		IATA iata = objectFactory.createIATA();

		// unb
		String interchangeControllRef = new RandomANGen(10).nextString("0001");
		UNB unb = tkcresFactory.createUNB(iHeader, interchangeControllRef);

		// tkcres
		TKCRES tkcres = objectFactory.createTKCRES();

		UNH unh = tkcresFactory.createUNH(mHeader);
		tkcres.setUNH(unh);

		MSG msg = tkcresFactory.createMSG(msgFunctionCode);
		tkcres.setMSG(msg);

		// calculation the equvalant fare in agents currency
		String originAgentCode = reservation.getAdminInfo().getOriginAgentCode();
		Agent originAgent = GDSServicesModuleUtil.getTravelAgentBD().getAgent(originAgentCode);
		String agentCurrency = originAgent.getCurrencyCode();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		CurrencyExchangeRate currExRate = exchangeRateProxy.getCurrencyExchangeRate(agentCurrency);

		// construct the reservation response
		Iterator<ReservationPax> passengerIterator = reservation.getPassengers().iterator();
		while (passengerIterator.hasNext()) {
			ReservationPax pax = passengerIterator.next();
			if (pax.getPaxType().equals(PaxTypeTO.ADULT)) {
				Collection<EticketTO> eTickets = pax.geteTickets();

				// no of tickets assosiated with the pax
				EQN eqn = objectFactory.createEQN();
				setValue(objectFactory, unh, BigDecimal.valueOf(eTickets.size()), 1, 1);
				setValue(objectFactory, unh, CodeSetEnum.CS6353.NumberOfTicket.toString(), 1, 1);
				tkcres.setEQN(eqn);

				// group 3 begin
				GROUP3 group3 = objectFactory.createGROUP3();

				// Traveler information
				TIF tif = objectFactory.createTIF();
				TIF02 tif02 = objectFactory.createTIFTIF02();

				setValue(objectFactory, tif, pax.getFirstName(), 1, 1);
				setValue(objectFactory, tif, TypeADataConvertorUtill.convertToPaxType(pax.getPaxType()), 1, 2);
				setValue(objectFactory, tif02, pax.getLastName(), 1);
				setValue(objectFactory, tif02, pax.getTitle(), 2);
				tif.getTIF02().add(tif02);
				group3.setTIF(tif);

				// TAI is ignored at the moment coz we dont have the amedues agent code for correcponding agnets

				// RCI with external pnr
				RCI rci = objectFactory.createRCI();
				RCI01 rci01 = objectFactory.createRCIRCI01();
				setValue(objectFactory, rci01, reservation.getAdminInfo().getOriginCarrierCode(), 1);
				setValue(objectFactory, rci01, reservation.getExternalRecordLocator(), 2);
				setValue(objectFactory, rci01, "1", 3);
				rci.getRCI01().add(rci01);
				group3.setRCI(rci);

				// Monitary information creation
				MON mon = createMonitaryInformation(objectFactory, reservation, currExRate);

			}
		}

		UNT unt = tkcresFactory.createUNT(mHeader);
		tkcres.setUNT(unt);

		iata.getUNBAndUNGAndTKCRES().add(unb);
		iata.getUNBAndUNGAndTKCRES().add(tkcres);

		return objectFactory.createIATA(iata);
	}

	private static MON createMonitaryInformation(ObjectFactory objectFactory, Reservation reservation,
			CurrencyExchangeRate currExRate) throws ModuleException {

		MON mon = objectFactory.createMON();

		// base fare taken from reservation.
		BigDecimal baseFare = reservation.getTotalTicketFare();

		BigDecimal exchangeRate = currExRate.getMultiplyingExchangeRate();
		Currency currency = currExRate.getCurrency();
		BigDecimal equvalantFare = AccelAeroRounderPolicy.convertAndRound(baseFare, exchangeRate, currency.getBoundryValue(),
				currency.getBreakPoint());
		BigDecimal totalAmount = reservation.getTotalChargeAmount();

		// base fare
		setValue(objectFactory, mon, "B", 1, 1);
		setValue(objectFactory, mon, baseFare, 1, 2);
		setValue(objectFactory, mon, AppSysParamsUtil.getBaseCurrency(), 1, 3);

		// equavalant fare
		MON02 mon02Equvalant = objectFactory.createMONMON02();
		setValue(objectFactory, mon02Equvalant, "E", 1);
		setValue(objectFactory, mon02Equvalant, equvalantFare, 2);
		setValue(objectFactory, mon02Equvalant, currency.getCurrencyCode(), 3);

		return mon;
	}

	/**
	 * Construct list display for multiple matches
	 * 
	 * @param mHeader
	 * @param iHeader
	 * @param reservations
	 * @return
	 */
	private static JAXBElement<?> constructListRespomse(InterchangeHeader iHeader, MessageHeader mHeader,
			List<Reservation> reservations) {
		// TODO Auto-generated method stub
		return null;
	}

	private static JAXBElement<?> constructErrorResponse(InterchangeHeader iHeader, MessageHeader mHeader,
			List<CodeSetEnum.CS9321> appErrors) {
		// TODO Auto-generated method stub
		return null;
	}

}
