package com.isa.thinair.gdsservices.core.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.ExternalSegmentTO;
import com.isa.thinair.airreservation.api.dto.OtherAirlineSegmentTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.internal.GDSReservationRequestBase;
import com.isa.thinair.gdsservices.api.dto.internal.RemovePaxRequest;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.dto.internal.UpdateExternalSegmentsRequest;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.StatusCode;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.platform.api.ServiceResponce;

public class UpdateExternalSegmentAction {

	private static Log log = LogFactory.getLog(UpdateExternalSegmentAction.class);

	public static UpdateExternalSegmentsRequest processRequest(UpdateExternalSegmentsRequest updateExternalSegmentsRequest,
			LinkedHashMap<ReservationAction, GDSReservationRequestBase> responseMap) {
		try {
			Reservation reservation = null;
			if (!responseMap.isEmpty() && responseMap.containsKey(GDSInternalCodes.ReservationAction.REMOVE_PAX)) {
				RemovePaxRequest removePaxRequest = (RemovePaxRequest) responseMap
						.get(GDSInternalCodes.ReservationAction.REMOVE_PAX);
				String removePaxPnr = removePaxRequest.getCancelledPnr();
				if (removePaxPnr != null) {
					reservation = CommonUtil.loadReservation(updateExternalSegmentsRequest.getGdsCredentialsDTO(), null,
							removePaxPnr);
				}
			}

			if (reservation == null) {
				reservation = CommonUtil.loadReservation(updateExternalSegmentsRequest.getGdsCredentialsDTO(),
						updateExternalSegmentsRequest.getOriginatorRecordLocator().getPnrReference(), null);
			}
			CommonUtil.validateReservationOwner(updateExternalSegmentsRequest.getGdsCredentialsDTO(), reservation);

			Map<String, Segment> gdsExtSegMap = getGDSExternalSegments(updateExternalSegmentsRequest.getExternalSegments());
			Collection<OtherAirlineSegmentTO> externalSegmentTOs = getOtherAirlineSegments(gdsExtSegMap);

			if (externalSegmentTOs != null && externalSegmentTOs.size() > 0) {
				ServiceResponce serviceRes = GDSServicesModuleUtil.getReservationSegmentBD().changeOtherAirlineSegments(
						reservation.getPnr(), externalSegmentTOs, reservation.getVersion(),
						CommonUtil.getTrackingInfo(updateExternalSegmentsRequest.getGdsCredentialsDTO()));

				if (serviceRes != null && serviceRes.isSuccess()) {
					updateExternalSegmentsRequest.setSuccess(true);
				} else {
					updateExternalSegmentsRequest.setSuccess(false);
					updateExternalSegmentsRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
					updateExternalSegmentsRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
				}
			} else {
				updateExternalSegmentsRequest.setSuccess(false);
				updateExternalSegmentsRequest.setResponseMessage(MessageUtil.getMessage("gdsservices.actions.invalidESegments"));
				updateExternalSegmentsRequest.addErrorCode("gdsservices.actions.invalidESegments");
			}
		} catch (ModuleException e) {
			log.error(" CancelSegmentAction Failed for GDS PNR "
					+ updateExternalSegmentsRequest.getOriginatorRecordLocator().getPnrReference(), e);
			updateExternalSegmentsRequest.setSuccess(false);
			updateExternalSegmentsRequest.setResponseMessage(e.getMessageString());
			updateExternalSegmentsRequest.addErrorCode(e.getExceptionCode());
		} catch (Exception e) {
			log.error(" CancelSegmentAction Failed for GDS PNR "
					+ updateExternalSegmentsRequest.getOriginatorRecordLocator().getPnrReference(), e);
			updateExternalSegmentsRequest.setSuccess(false);
			updateExternalSegmentsRequest.setResponseMessage(MessageUtil.getDefaultErrorMessage());
			updateExternalSegmentsRequest.addErrorCode(MessageUtil.getDefaultErrorCode());
		}

		return updateExternalSegmentsRequest;
	}

	private static Collection<ExternalSegmentTO> getExternalSegments(Map<String, Segment> gdsExtSegMap) throws ModuleException {
		Collection<ExternalSegmentTO> externalSegments = new ArrayList<ExternalSegmentTO>();

		for (Segment segment : gdsExtSegMap.values()) {
			ExternalSegmentTO externalSegmentTO = new ExternalSegmentTO();
			externalSegmentTO.setSegmentCode(segment.getSegmentCode());
			externalSegmentTO.setDepartureDate(segment.getDepartureDateTime());
			externalSegmentTO.setArrivalDate(segment.getArrivalDateTime());
			externalSegmentTO.setFlightNo(segment.getFlightNumber());
			// TODO set return flag, flight ref no and operating carrier

			if (segment.getStatusCode() == StatusCode.CONFIRMED) {
				externalSegmentTO.setStatus(ReservationInternalConstants.ReservationExternalSegmentStatus.CONFIRMED);
				externalSegments.add(externalSegmentTO);
			} else if (segment.getStatusCode() == StatusCode.CANCELLED) {
				externalSegmentTO.setStatus(ReservationInternalConstants.ReservationExternalSegmentStatus.CANCEL);
				externalSegments.add(externalSegmentTO);
			}
		}

		return externalSegments;
	}
	
	private static Collection<OtherAirlineSegmentTO> getOtherAirlineSegments(Map<String, Segment> gdsExtSegMap) throws ModuleException {
		Collection<OtherAirlineSegmentTO> externalSegments = new ArrayList<OtherAirlineSegmentTO>();
		
		for (Segment segment : gdsExtSegMap.values()) {
			OtherAirlineSegmentTO externalSegmentTO = new OtherAirlineSegmentTO();
			externalSegmentTO.setSegmentCode(segment.getSegmentCode());
			externalSegmentTO.setDepartureDateTimeLocal(segment.getDepartureDateTime());
			externalSegmentTO.setArrivalDateTimeLocal(segment.getArrivalDateTime());
			externalSegmentTO.setFlightNumber(segment.getFlightNumber());
			externalSegmentTO.setBookingCode(segment.getBookingCode());
			externalSegmentTO.setMarketingBookingCode(segment.getCodeShareBc());
			externalSegmentTO.setMarketingFlightNumber(segment.getCodeShareFlightNo());

			if (segment.getStatusCode() == StatusCode.CONFIRMED) {
				externalSegmentTO.setStatus(ReservationInternalConstants.ReservationOtherAirlineSegmentStatus.CONFIRMED);
				externalSegments.add(externalSegmentTO);
			} else if (segment.getStatusCode() == StatusCode.CANCELLED) {
				externalSegmentTO.setStatus(ReservationInternalConstants.ReservationOtherAirlineSegmentStatus.CANCEL);
				externalSegments.add(externalSegmentTO);
			}
		}
		return externalSegments;
	}

	private static Map<String, Segment> getGDSExternalSegments(Collection<Segment> externalSegments) {
		Map<String, Segment> map = new HashMap<String, Segment>();
		String key;

		for (Segment segment : externalSegments) {
			key = ReservationApiUtils.getUniqueSegmentKey(segment.getFlightNumber(), segment.getSegmentCode(),
					segment.getDepartureDateTime(), segment.getBookingCode(), null);
			map.put(key, segment);
		}

		return map;
	}

	private static String getSegMapKey(String flightNo, String segmentCode, Date departureDate) {
		return ReservationApiUtils.getUniqueSegmentKey(flightNo, segmentCode, departureDate, null, null);
	}
}
