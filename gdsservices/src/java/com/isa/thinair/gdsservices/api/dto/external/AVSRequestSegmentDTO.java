package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author M.Rikaz
 * 
 */
public class AVSRequestSegmentDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String flightDesignator;

	private Date departureDate;

	private String departureStation;

	private String destinationStation;

	private Integer seatsAvailable;

	private String rbd;

	private String statusCode;

	private boolean numericAvailability;

	public String getDepartureStation() {
		return departureStation;
	}

	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}

	public String getDestinationStation() {
		return destinationStation;
	}

	public void setDestinationStation(String destinationStation) {
		this.destinationStation = destinationStation;
	}

	public String getFlightDesignator() {
		return flightDesignator;
	}

	public void setFlightDesignator(String flightDesignator) {
		this.flightDesignator = flightDesignator;
	}

	public Integer getSeatsAvailable() {
		return seatsAvailable;
	}

	public void setSeatsAvailable(Integer seatsAvailable) {
		this.seatsAvailable = seatsAvailable;
	}

	public String getRbd() {
		return rbd;
	}

	public void setRbd(String rbd) {
		this.rbd = rbd;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public boolean isNumericAvailability() {
		return numericAvailability;
	}

	public void setNumericAvailability(boolean numericAvailability) {
		this.numericAvailability = numericAvailability;
	}

}
