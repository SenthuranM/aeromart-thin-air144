package com.isa.thinair.gdsservices.api.dto.internal;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;

public class Child extends Passenger {

	private static final long serialVersionUID = -5640076664055073712L;

	@Override
	public String getPaxType() {
		return ReservationInternalConstants.PassengerType.CHILD;
	}
}
