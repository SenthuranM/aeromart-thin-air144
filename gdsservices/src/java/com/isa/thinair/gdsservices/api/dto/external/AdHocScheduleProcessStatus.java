/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * @author M.Rikaz
 * 
 */
public class AdHocScheduleProcessStatus extends SSMMessageProcessStatus implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6213438435959278397L;

	private int flightId;

	private Date departureDate;

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append("AdHocScheduleProcessStatus ");
		sb.append(super.toString());
		sb.append(" Flight Id:" + flightId);
		if (departureDate != null) {
			sb.append(" Departure Date:" + CalendarUtil.formatDate(departureDate, CalendarUtil.PATTERN1));
		}

		return sb.toString();
	}

}
