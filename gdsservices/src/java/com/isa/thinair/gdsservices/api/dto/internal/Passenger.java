package com.isa.thinair.gdsservices.api.dto.internal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.builder.HashCodeBuilder;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;

public class Passenger extends GDSBase {
	private static final long serialVersionUID = -2927328084536289817L;

	private String title;
	private String lastName;
	private String firstName;
	private Date dateOfBirth;
	private int familyID;
	private String docCountryCode;	
	private String docNumber;	
	private Date docExpiryDate;
	private String nationalityCode;
	private String travelDocType;
	private String visaDocNumber;
	private String visaDocPlaceOfIssue;
	private Date visaDocIssueDate;
	private String visaApplicableCountry;
	private Date issueDate; 
	private Collection<SpecialServiceRequest> ssrCollection;
	private Collection<OtherServiceInfo> osiCollection;
	private String placeOfBirth;

	public Passenger() {
		ssrCollection = new ArrayList<SpecialServiceRequest>();
		osiCollection = new ArrayList<OtherServiceInfo>();
	}

	public String getPaxType() {
		return "";
	}

	/**
	 * @return Returns the firstName.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the lastName.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the osiCollection.
	 */
	public Collection<OtherServiceInfo> getOsiCollection() {
		return osiCollection;
	}

	/**
	 * @param osiCollection
	 *            The osiCollection to set.
	 */
	public void setOsiCollection(Collection<OtherServiceInfo> osiCollection) {
		this.osiCollection = osiCollection;
	}

	/**
	 * @return Returns the ssrCollection.
	 */
	public Collection<SpecialServiceRequest> getSsrCollection() {
		return ssrCollection;
	}

	/**
	 * @param ssrCollection
	 *            The ssrCollection to set.
	 */
	public void setSsrCollection(Collection<SpecialServiceRequest> ssrCollection) {
		this.ssrCollection = ssrCollection;
	}

	/**
	 * @return Returns the passengerTitle.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @param passengerTitle
	 *            The passengerTitle to set.
	 */

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIATAName() {
		return GDSApiUtils.getIATAName(this.getLastName(), this.getFirstName(), this.getTitle());
	}

	public int getFamilyID() {
		return familyID;
	}

	public void setFamilyID(int familyID) {
		this.familyID = familyID;
	}

	/**
	 * @return the docCountryCode
	 */
	public String getDocCountryCode() {
		return docCountryCode;
	}

	/**
	 * @param docCountryCode the docCountryCode to set
	 */
	public void setDocCountryCode(String docCountryCode) {
		this.docCountryCode = docCountryCode;
	}

	/**
	 * @return the docNumber
	 */
	public String getDocNumber() {
		return docNumber;
	}

	/**
	 * @param docNumber the docNumber to set
	 */
	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

	/**
	 * @return the docExpiryDate
	 */
	public Date getDocExpiryDate() {
		return docExpiryDate;
	}

	/**
	 * @param docExpiryDate the docExpiryDate to set
	 */
	public void setDocExpiryDate(Date docExpiryDate) {
		this.docExpiryDate = docExpiryDate;
	}

	public String getNationalityCode() {
		return nationalityCode;
	}

	public void setNationalityCode(String nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	public String getTravelDocType() {
		return travelDocType;
	}

	public void setTravelDocType(String travelDocType) {
		this.travelDocType = travelDocType;
	}

	public String getVisaDocNumber() {
		return visaDocNumber;
	}

	public void setVisaDocNumber(String visaDocNumber) {
		this.visaDocNumber = visaDocNumber;
	}

	public String getVisaDocPlaceOfIssue() {
		return visaDocPlaceOfIssue;
	}

	public void setVisaDocPlaceOfIssue(String visaDocPlaceOfIssue) {
		this.visaDocPlaceOfIssue = visaDocPlaceOfIssue;
	}

	public Date getVisaDocIssueDate() {
		return visaDocIssueDate;
	}

	public void setVisaDocIssueDate(Date visaDocIssueDate) {
		this.visaDocIssueDate = visaDocIssueDate;
	}

	public String getVisaApplicableCountry() {
		return visaApplicableCountry;
	}

	public void setVisaApplicableCountry(String visaApplicableCountry) {
		this.visaApplicableCountry = visaApplicableCountry;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}
	
	// equals and hashCode methods are override to be used in ChangePaxDetailsRequestExtractor class (getRequest method)
	@Override
	public boolean equals(Object passengerObj){
		Passenger passenger;
		if(passengerObj instanceof Passenger){
			passenger = (Passenger) passengerObj;
		}else{
			return false;
		}
		
		if(passenger.getTitle().equals(this.title) && passenger.getFirstName().equals(this.firstName)
				&& passenger.getLastName().equals(this.lastName)){
			return true;
		}
		
		return false;
		
	}
	
	@Override 
	public int hashCode(){
		return new HashCodeBuilder(17 , 31).append(this.title).append(this.firstName).append(this.lastName).toHashCode();
	}
}
