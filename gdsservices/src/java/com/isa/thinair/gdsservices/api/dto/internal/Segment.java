package com.isa.thinair.gdsservices.api.dto.internal;

import java.util.Date;

import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ActionCode;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.StatusCode;

public class Segment extends GDSBase implements Comparable {
	private static final long serialVersionUID = -4828974577822205303L;

	private String flightNumber;
	private Date departureDate;
	private Date departureTime;
	private Date arrivalDate;
	private Date arrivalTime;

	// +/-
	private int departureDayOffset;
	// +/-
	private int arrivalDayOffset;

	private String departureStation;
	private String arrivalStation;

	private String bookingCode;

	private ActionCode actionCode;

	private StatusCode statusCode;
	
	private String codeShareFlightNo;
	
	private String codeShareBc;

	/**
	 * @return Returns the arrivalDate.
	 */
	public Date getArrivalDate() {
		return arrivalDate;
	}

	/**
	 * @param arrivalDate
	 *            The arrivalDate to set.
	 */
	public void setArrivalDate(Date arrivalDateTime) {
		this.arrivalDate = arrivalDateTime;
	}

	/**
	 * @return the arrivalTime
	 */
	public Date getArrivalTime() {
		return arrivalTime;
	}

	/**
	 * @param arrivalTime
	 *            the arrivalTime to set
	 */
	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	/**
	 * @return Returns the arrivalDayOffset.
	 */
	public int getArrivalDayOffset() {
		return arrivalDayOffset;
	}

	/**
	 * @param arrivalDayOffset
	 *            The arrivalDayOffset to set.
	 */
	public void setArrivalDayOffset(int arrivalDayOffset) {
		this.arrivalDayOffset = arrivalDayOffset;
	}

	/**
	 * @return Returns the arrivalStation.
	 */
	public String getArrivalStation() {
		return arrivalStation;
	}

	/**
	 * @param arrivalStation
	 *            The arrivalStation to set.
	 */
	public void setArrivalStation(String arrivalStation) {
		this.arrivalStation = arrivalStation;
	}

	/**
	 * @return Returns the bookingCode.
	 */
	public String getBookingCode() {
		return bookingCode;
	}

	/**
	 * @param bookingCode
	 *            The bookingCode to set.
	 */
	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	/**
	 * @return Returns the departureDate.
	 */
	public Date getDepartureDate() {
		return departureDate;
	}

	/**
	 * @param departureDate
	 *            The departureDate to set.
	 */
	public void setDepartureDate(Date departureDateTime) {
		this.departureDate = departureDateTime;
	}

	/**
	 * @return the departureTime
	 */
	public Date getDepartureTime() {
		return departureTime;
	}

	/**
	 * @param departureTime
	 *            the departureTime to set
	 */
	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	/**
	 * @return Returns the departureDayOffset.
	 */
	public int getDepartureDayOffset() {
		return departureDayOffset;
	}

	/**
	 * @param departureDayOffset
	 *            The departureDayOffset to set.
	 */
	public void setDepartureDayOffset(int departureDayOffset) {
		this.departureDayOffset = departureDayOffset;
	}

	/**
	 * @return Returns the departureStation.
	 */
	public String getDepartureStation() {
		return departureStation;
	}

	/**
	 * @param departureStation
	 *            The departureStation to set.
	 */
	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}

	/**
	 * @return Returns the flightNumber.
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the actionCode
	 */
	public ActionCode getActionCode() {
		return actionCode;
	}

	/**
	 * @param actionCode
	 *            the actionCode to set
	 */
	public void setActionCode(ActionCode actionCode) {
		this.actionCode = actionCode;
	}

	/**
	 * @return the statusCode
	 */
	public StatusCode getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode
	 *            the statusCode to set
	 */
	public void setStatusCode(StatusCode statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return Returns the segmentCode.
	 */
	public String getSegmentCode() {
		return GDSApiUtils.getSegmentCode(this.getDepartureStation(), this.getArrivalStation());
	}

	/**
	 * Returns departure date time
	 * 
	 * @return
	 */
	public Date getDepartureDateTime() {
		return CalendarUtil.addDateAndTime(this.getDepartureDate(), this.getDepartureTime());
	}

	/**
	 * Returns arrival date time
	 * 
	 * @return
	 */
	public Date getArrivalDateTime() {
		return CalendarUtil.addDateAndTime(this.getArrivalDate(), this.getArrivalTime());
	}

	/**
	 * Compare zulu departure date time
	 * 
	 * @param o
	 * @return 1, 0, -1
	 */
	public int compareTo(Object o) {
		return this.getDepartureDateTime().compareTo(((Segment) o).getDepartureDateTime());
	}

	public String getCodeShareFlightNo() {
		return codeShareFlightNo;
	}

	public String getCodeShareBc() {
		return codeShareBc;
	}

	public void setCodeShareFlightNo(String codeShareFlightNo) {
		this.codeShareFlightNo = codeShareFlightNo;
	}

	public void setCodeShareBc(String codeShareBc) {
		this.codeShareBc = codeShareBc;
	}
}
