package com.isa.thinair.gdsservices.api.dto.internal;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

public class AddSsrRequest extends GDSReservationRequestBase {
	private static final long serialVersionUID = -893740256287561460L;

	private Collection<Passenger> passengers;
	private Collection<SpecialServiceRequest> commonSSRs;

	public AddSsrRequest() {
		passengers = new ArrayList<Passenger>();
		commonSSRs = new ArrayList<SpecialServiceRequest>();
	}

	@Override
	public ReservationAction getActionType() {
		return GDSInternalCodes.ReservationAction.ADD_SSR;
	}

	/**
	 * @return the passengers
	 */
	public Collection<Passenger> getPassengers() {
		return passengers;
	}

	/**
	 * @param passengers
	 *            the passengers to set
	 */
	public void setPassengers(Collection<Passenger> passengers) {
		this.passengers = passengers;
	}

	/**
	 * @return the commonSSRs
	 */
	public Collection<SpecialServiceRequest> getCommonSSRs() {
		return commonSSRs;
	}

	/**
	 * @param commonSSRs
	 *            the commonSSRs to set
	 */
	public void setCommonSSRs(Collection<SpecialServiceRequest> ssrs) {
		this.commonSSRs = ssrs;
	}

}
