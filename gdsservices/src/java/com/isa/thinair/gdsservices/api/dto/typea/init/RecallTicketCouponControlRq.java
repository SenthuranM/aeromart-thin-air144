package com.isa.thinair.gdsservices.api.dto.typea.init;

import java.util.List;

public class RecallTicketCouponControlRq extends InitBaseRq {
	private List<Integer> ppfsETicketIds;

	public List<Integer> getPpfsETicketIds() {
		return ppfsETicketIds;
	}

	public void setPpfsETicketIds(List<Integer> ppfsETicketIds) {
		this.ppfsETicketIds = ppfsETicketIds;
	}
}
