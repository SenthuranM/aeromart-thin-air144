package com.isa.thinair.gdsservices.api.dto.typea.internal;

import java.util.List;

public class SplitResOrRemovePaxRq extends ReservationRq {

	private String oldPnr;
	private String newPnr;
	private String newExternalRecordLocator;
	private int gdsId;
	private List<Integer> paxIds;

	public SplitResOrRemovePaxRq(Event event) {
		super(event);
	}

	public String getOldPnr() {
		return oldPnr;
	}

	public void setOldPnr(String oldPnr) {
		this.oldPnr = oldPnr;
	}

	public String getNewPnr() {
		return newPnr;
	}

	public void setNewPnr(String newPnr) {
		this.newPnr = newPnr;
	}

	public String getNewExternalRecordLocator() {
		return newExternalRecordLocator;
	}

	public void setNewExternalRecordLocator(String newExternalRecordLocator) {
		this.newExternalRecordLocator = newExternalRecordLocator;
	}

	public int getGdsId() {
		return gdsId;
	}

	public void setGdsId(int gdsId) {
		this.gdsId = gdsId;
	}

	public List<Integer> getPaxIds() {
		return paxIds;
	}

	public void setPaxIds(List<Integer> paxIds) {
		this.paxIds = paxIds;
	}
}
