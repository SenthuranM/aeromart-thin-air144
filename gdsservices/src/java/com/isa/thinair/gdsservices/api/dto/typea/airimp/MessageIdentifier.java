package com.isa.thinair.gdsservices.api.dto.typea.airimp;

public enum MessageIdentifier {

	AAK("AAK"), AKD("AKD"), AKI("AKI"), AKK("AKK"), ASA("ASA"), ASC("ASC"), AVA("AVA"), AVC("AVC"), AVH("AVH"), AVS("AVS"), AVT(
			"AVT"), BPR("BPR"), CHG("CHG"), CRA("CRA"), DEC("DEC"), DVD("DVD"), INC("INC"), IRP("IRP"), MAS("MAS"), MED("MED"), NAC(
			"NAC"), NAR("NAR"), NCO("NCO"), NRC("NRC"), NRL("NRL"), RQR("RQR"), RVR("RVR"), SKD("SKD"), UPD("UPD"), COR("COR"), PDM(
			"PDM"), ACK("ACK"), ADC("ADC"), ADV("ADV"), PRF("PRF"), RFD("RFD"), RFI("RFI"),

	/** Undefined */
	UNDEFINED("");

	private final String value;

	private MessageIdentifier(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public static MessageIdentifier getEnum(String value) {
		if (value != null) {
			for (MessageIdentifier enumSingle : MessageIdentifier.values()) {
				if (enumSingle.value.compareTo(value) == 0)
					return enumSingle;
			}
		}

		return UNDEFINED;
	}
}