package com.isa.thinair.gdsservices.api.dto.typea;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Main DTO to hold the response form inventory adjustment.
 * 
 * @author mekanayake
 * 
 */
public class ODIResponseDTO {

	private String OdiFrom;

	private String OdiTo;

	private List<TVLDto> tvlResponses;

	private Set<ERCDetail> ercDetails;

	public String getOdiFrom() {
		return OdiFrom;
	}

	public void setOdiFrom(String odiFrom) {
		OdiFrom = odiFrom;
	}

	public String getOdiTo() {
		return OdiTo;
	}

	public void setOdiTo(String odiTo) {
		OdiTo = odiTo;
	}

	public List<TVLDto> getTvlResponses() {
		if (tvlResponses == null) {
			tvlResponses = new ArrayList<TVLDto>();
		}
		return tvlResponses;
	}

	public void setTvlResponses(List<TVLDto> tvlResponses) {
		this.tvlResponses = tvlResponses;
	}

	public void addTvlResponse(TVLDto response) {
		getTvlResponses().add(response);
	}

	/**
	 * @return the ercDetails
	 */
	public Set<ERCDetail> getErcDetails() {
		return ercDetails;
	}

	/**
	 * @param ercDetails
	 *            the ercDetails to set
	 */
	public void setErcDetails(Set<ERCDetail> ercDetails) {
		if (ercDetails == null) {
			ercDetails = new HashSet<ERCDetail>();
		}
		this.ercDetails = ercDetails;
	}

	public void addErcDetails(ERCDetail ercDetail) {
		this.getErcDetails().add(ercDetail);
	}

	public void rearangeERC() {
		if (!getTvlResponses().isEmpty()) {
			Set<ERCDetail> intersection = new HashSet<ERCDetail>(tvlResponses.get(0).getErcDetails());
			for (int i = 1; i < getTvlResponses().size(); i++) {
				intersection.retainAll(tvlResponses.get(i).getErcDetails());
			}

			// removing ERC segments common to all TVLs and move up to the ODI level.
			if (!intersection.isEmpty()) {
				for (int i = 0; i < getTvlResponses().size(); i++) {
					Set<ERCDetail> erciftResponses = getTvlResponses().get(i).getErcDetails();
					erciftResponses.removeAll(intersection);
				}
				getErcDetails().addAll(intersection);
			}
		}
	}

}
