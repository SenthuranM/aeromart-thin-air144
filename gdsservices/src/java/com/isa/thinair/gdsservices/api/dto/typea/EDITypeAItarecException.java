package com.isa.thinair.gdsservices.api.dto.typea;

import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;

/**
 * Exception to propagate validation exceptions.
 * 
 * @author mekanayake
 * 
 */
public class EDITypeAItarecException extends Exception {

	private static final long serialVersionUID = 1L;

	private String errorCode;

	private String errorText;

	public EDITypeAItarecException() {
		super();
	}

	public EDITypeAItarecException(String errorCode, String errorText) {
		super();
		this.errorCode = errorCode;
		this.errorText = errorText;
	}

	public EDITypeAItarecException(CodeSetEnum.CS9321 message) {
		this.errorCode = message.getValue();
		this.errorText = message.getErrorText();
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorText() {
		return errorText;
	}

}
