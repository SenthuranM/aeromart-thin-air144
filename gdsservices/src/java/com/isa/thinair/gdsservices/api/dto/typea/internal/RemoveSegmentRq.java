package com.isa.thinair.gdsservices.api.dto.typea.internal;

import java.util.List;

public class RemoveSegmentRq extends ReservationRq {

	private String pnr;
	private List<Integer> pnrSegments;

	public RemoveSegmentRq(Event event) {
		super(event);
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public List<Integer> getPnrSegments() {
		return pnrSegments;
	}

	public void setPnrSegments(List<Integer> pnrSegments) {
		this.pnrSegments = pnrSegments;
	}
}
