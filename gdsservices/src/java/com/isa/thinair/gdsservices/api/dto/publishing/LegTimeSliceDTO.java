package com.isa.thinair.gdsservices.api.dto.publishing;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author M.Rikaz
 * 
 */
public class LegTimeSliceDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3617879434103691032L;

	public static final String DEPARTURE_ONLY = "D";

	public static final String ARRIVAL_ONLY = "A";

	public static final String DEPARTURE_AND_ARRIVAL = "B";

	private Date departureTime;

	private Date arrivalTime;

	private int departureDayOffset;

	private int arrivalDayOffset;

	private String airportCode;

	public LegTimeSliceDTO(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public Date getDepartureTime() {
		return departureTime;
	}

	public Date getArrivalTime() {
		return arrivalTime;
	}

	public int getDepartureDayOffset() {
		return departureDayOffset;
	}

	public int getArrivalDayOffset() {
		return arrivalDayOffset;
	}

	public void setArrivalDayOffset(int arrivalDayOffset) {
		this.arrivalDayOffset = arrivalDayOffset;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public void setDepartureDayOffset(int departureDayOffset) {
		this.departureDayOffset = departureDayOffset;
	}

	public String getAirportOperationMode() {
		String operationMode = null;
		if (departureTime != null && arrivalTime != null) {
			operationMode = DEPARTURE_AND_ARRIVAL;
		}

		if (departureTime != null && arrivalTime == null) {
			operationMode = DEPARTURE_ONLY;
		}

		if (departureTime == null && arrivalTime != null) {
			operationMode = ARRIVAL_ONLY;
		}
		return operationMode;
	}

}
