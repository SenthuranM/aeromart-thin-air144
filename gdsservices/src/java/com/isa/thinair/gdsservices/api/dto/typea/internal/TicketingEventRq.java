package com.isa.thinair.gdsservices.api.dto.typea.internal;

import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.api.dto.TransitionTo;
import com.isa.thinair.gdsservices.api.dto.typea.common.Coupon;

public class TicketingEventRq extends InternalBaseRq {
	private int gdsId;
	private String pnr;
	private Map<Integer, Map<String, List<Integer>>> paxTicketsCoupons; // pax-seq -> tkt-no -> cpn-no
	private Map<Integer, Map<Integer, TransitionTo<Coupon>>> transitions; // pax-sequence -> segment-sequence -> transition
	private String ticketEvent;
	private boolean calculateFinancialInfo;

	public interface Event {
		String ISSUE = "ISSUE";
		String RE_ISSUE = "RE_ISSUE";
		String CANCEL = "CANCEL";
	}

	public int getGdsId() {
		return gdsId;
	}

	public void setGdsId(int gdsId) {
		this.gdsId = gdsId;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Map<Integer, Map<String, List<Integer>>> getPaxTicketsCoupons() {
		return paxTicketsCoupons;
	}

	public void setPaxTicketsCoupons(Map<Integer, Map<String, List<Integer>>> paxTicketsCoupons) {
		this.paxTicketsCoupons = paxTicketsCoupons;
	}

	public Map<Integer, Map<Integer, TransitionTo<Coupon>>> getTransitions() {
		return transitions;
	}

	public void setTransitions(Map<Integer, Map<Integer, TransitionTo<Coupon>>> transitions) {
		this.transitions = transitions;
	}

	public String getTicketEvent() {
		return ticketEvent;
	}

	public void setTicketEvent(String ticketEvent) {
		this.ticketEvent = ticketEvent;
	}

	public boolean isCalculateFinancialInfo() {
		return calculateFinancialInfo;
	}

	public void setCalculateFinancialInfo(boolean calculateFinancialInfo) {
		this.calculateFinancialInfo = calculateFinancialInfo;
	}
}
