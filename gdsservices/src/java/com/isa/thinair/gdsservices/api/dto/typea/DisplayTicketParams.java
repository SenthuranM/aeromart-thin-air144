package com.isa.thinair.gdsservices.api.dto.typea;

import java.util.Date;

public class DisplayTicketParams {
	private String passengerFirstName;
	private String passengerSurname;
    private String phoneNumber;//TODO
	private String frequentTravellerNumber;//TODO
	private String creditCardNumber;//TODO
	private String confirmationId;//TODO
	private String formOfIdentification;//TODO

	private String flightNumber;
	private String origin;
	private String destination;
	private Date flightDepartureDate;

	public String getPassengerFirstName() {
		return passengerFirstName;
	}

	public void setPassengerFirstName(String passengerFirstName) {
		this.passengerFirstName = passengerFirstName;
	}

	public String getPassengerSurname() {
		return passengerSurname;
	}

	public void setPassengerSurname(String passengerSurname) {
		this.passengerSurname = passengerSurname;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getFrequentTravellerNumber() {
		return frequentTravellerNumber;
	}

	public void setFrequentTravellerNumber(String frequentTravellerNumber) {
		this.frequentTravellerNumber = frequentTravellerNumber;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getConfirmationId() {
		return confirmationId;
	}

	public void setConfirmationId(String confirmationId) {
		this.confirmationId = confirmationId;
	}

	public String getFormOfIdentification() {
		return formOfIdentification;
	}

	public void setFormOfIdentification(String formOfIdentification) {
		this.formOfIdentification = formOfIdentification;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Date getFlightDepartureDate() {
		return flightDepartureDate;
	}

	public void setFlightDepartureDate(Date flightDepartureDate) {
		this.flightDepartureDate = flightDepartureDate;
	}
}
