package com.isa.thinair.gdsservices.api.dto.external;

import java.util.ArrayList;
import java.util.List;

public class SSRDetailDTO extends SSRDTO {

	private static final long serialVersionUID = -4954087453833851281L;

	private int noOfPax;
	private SegmentDTO segmentDTO;
	private List<NameDTO> nameDTOs;

	public SSRDetailDTO() {
		nameDTOs = new ArrayList<NameDTO>();
	}

	public int getNoOfPax() {
		return noOfPax;
	}

	public void setNoOfPax(int noOfPax) {
		this.noOfPax = noOfPax;
	}

	public SegmentDTO getSegmentDTO() {
		return segmentDTO;
	}

	public void setSegmentDTO(SegmentDTO segmentDTO) {
		this.segmentDTO = segmentDTO;
	}

	/**
	 * @return the nameDTOs
	 */
	public List<NameDTO> getNameDTOs() {
		return nameDTOs;
	}

	/**
	 * @param nameDTOs
	 *            the nameDTOs to set
	 */
	public void setNameDTOs(List<NameDTO> nameDTOs) {
		this.nameDTOs = nameDTOs;
	}
}
