package com.isa.thinair.gdsservices.api.dto.external;

import java.util.Date;
import java.util.List;

/**
 * @author Manoj Dhanushka
 */
public class SSRDocsDTO extends SSRDTO {
	
	private static final long serialVersionUID = -3206921056149140122L;
	
	private int noOfPax;
	
	private String docType;

	private String docCountryCode;
	
	private String docNumber;
	
	private Date docExpiryDate;
	
	private String docFamilyName;

	private String docGivenName;
	
	private String passengerNationality;

	private Date passengerDateOfBirth;

	/** gender of passenger, use m-male,f-female,i-infant,fi,mi */
	private String passengerGender;

	/** denote whether this is a multi passenger passport */
	private boolean multiPassengerPassport;

	/** PNR name dto */
	private List<NameDTO> pnrNameDTOs;

	/**
	 * @return the noOfPax
	 */
	public int getNoOfPax() {
		return noOfPax;
	}

	/**
	 * @param noOfPax the noOfPax to set
	 */
	public void setNoOfPax(int noOfPax) {
		this.noOfPax = noOfPax;
	}

	/**
	 * @return the docType
	 */
	public String getDocType() {
		return docType;
	}

	/**
	 * @param docType the docType to set
	 */
	public void setDocType(String docType) {
		this.docType = docType;
	}

	/**
	 * @return the docCountryCode
	 */
	public String getDocCountryCode() {
		return docCountryCode;
	}

	/**
	 * @param docCountryCode the docCountryCode to set
	 */
	public void setDocCountryCode(String docCountryCode) {
		this.docCountryCode = docCountryCode;
	}

	/**
	 * @return the docNumber
	 */
	public String getDocNumber() {
		return docNumber;
	}

	/**
	 * @param docNumber the docNumber to set
	 */
	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

	/**
	 * @return the docExpiryDate
	 */
	public Date getDocExpiryDate() {
		return docExpiryDate;
	}

	/**
	 * @param docExpiryDate the docExpiryDate to set
	 */
	public void setDocExpiryDate(Date docExpiryDate) {
		this.docExpiryDate = docExpiryDate;
	}

	/**
	 * @return the docFamilyName
	 */
	public String getDocFamilyName() {
		return docFamilyName;
	}

	/**
	 * @param docFamilyName the docFamilyName to set
	 */
	public void setDocFamilyName(String docFamilyName) {
		this.docFamilyName = docFamilyName;
	}

	/**
	 * @return the passengerNationality
	 */
	public String getPassengerNationality() {
		return passengerNationality;
	}

	/**
	 * @param passengerNationality the passengerNationality to set
	 */
	public void setPassengerNationality(String passengerNationality) {
		this.passengerNationality = passengerNationality;
	}

	/**
	 * @return the docGivenName
	 */
	public String getDocGivenName() {
		return docGivenName;
	}

	/**
	 * @param docGivenName the docGivenName to set
	 */
	public void setDocGivenName(String docGivenName) {
		this.docGivenName = docGivenName;
	}

	/**
	 * @return the passengerDateOfBirth
	 */
	public Date getPassengerDateOfBirth() {
		return passengerDateOfBirth;
	}

	/**
	 * @param passengerDateOfBirth the passengerDateOfBirth to set
	 */
	public void setPassengerDateOfBirth(Date passengerDateOfBirth) {
		this.passengerDateOfBirth = passengerDateOfBirth;
	}

	/**
	 * @return the passengerGender
	 */
	public String getPassengerGender() {
		return passengerGender;
	}

	/**
	 * @param passengerGender the passengerGender to set
	 */
	public void setPassengerGender(String passengerGender) {
		this.passengerGender = passengerGender;
	}

	/**
	 * @return the multiPassengerPassport
	 */
	public boolean isMultiPassengerPassport() {
		return multiPassengerPassport;
	}

	/**
	 * @param multiPassengerPassport the multiPassengerPassport to set
	 */
	public void setMultiPassengerPassport(boolean multiPassengerPassport) {
		this.multiPassengerPassport = multiPassengerPassport;
	}

	/**
	 * @return the pnrNameDTOs
	 */
	public List<NameDTO> getPnrNameDTOs() {
		return pnrNameDTOs;
	}

	/**
	 * @param pnrNameDTOs the pnrNameDTOs to set
	 */
	public void setPnrNameDTOs(List<NameDTO> pnrNameDTOs) {
		this.pnrNameDTOs = pnrNameDTOs;
	}

}
