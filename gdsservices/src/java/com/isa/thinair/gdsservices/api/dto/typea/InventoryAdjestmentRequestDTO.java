package com.isa.thinair.gdsservices.api.dto.typea;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;

public class InventoryAdjestmentRequestDTO {

	private String odiFrom;

	private String odiTo;

	private List<TVLDetail> tvls;

	public String getOdiFrom() {
		return odiFrom;
	}

	public void setOdiFrom(String odiFrom) {
		this.odiFrom = odiFrom;
	}

	public String getOdiTo() {
		return odiTo;
	}

	public void setOdiTo(String odiTo) {
		this.odiTo = odiTo;
	}

	public String getODIKey() {
		return new StringBuffer().append(odiFrom).append("/").append(odiTo).toString();
	}

	public List<TVLDetail> getTvls() {
		if (tvls == null) {
			tvls = new ArrayList<TVLDetail>();
		}
		return tvls;
	}

	public void setTvls(List<TVLDetail> tvls) {
		this.tvls = tvls;
	}

	public void addTVL(TVLDetail tvlDetails) {
		getTvls().add(tvlDetails);
	}

	public Set<CodeSetEnum.CS4405> getRequestedActionCodes() {
		Set<CodeSetEnum.CS4405> actinCodes = new HashSet<CodeSetEnum.CS4405>();
		if (tvls != null) {
			for (TVLDetail tvlDetail : tvls) {
				if (!actinCodes.contains(tvlDetail.getActionCoded())) {
					actinCodes.add(tvlDetail.getActionCoded());
				}
			}
		}
		return actinCodes;
	}

	public Map<String, List<TVLDetail>> getTVLDetailsByAction(CodeSetEnum.CS4405 actionRequested) {
		Map<String, List<TVLDetail>> tvlDetailMap = new HashMap<String, List<TVLDetail>>();
		if (tvls != null) {
			for (TVLDetail detail : tvls) {
				if (detail.getActionCoded().equals(actionRequested)) {
					List<TVLDetail> tvlDetails = tvlDetailMap.get(detail.getRbd());
					if (tvlDetails == null) {
						tvlDetails = new ArrayList<TVLDetail>();
						tvlDetailMap.put(detail.getRbd(), tvlDetails);
					}
					tvlDetails.add(detail);
				}
			}
		}

		// sorting the tvls according to the departure time.
		for (String key : tvlDetailMap.keySet()) {
			List<TVLDetail> tvls = tvlDetailMap.get(key);
			Collections.sort(tvls, new TVLComparator());
		}

		return tvlDetailMap;
	}

	private class TVLComparator implements Comparator<TVLDetail> {
		@Override
		public int compare(TVLDetail o1, TVLDetail o2) {
			return o1.getDepartureTimeStart().compareTo(o2.getDepartureTimeStart());
		}
	}
}
