package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;

public class AVSResponse implements Serializable {

	private static final long serialVersionUID = -3719337365430743297L;

	private boolean isSuccess;

	private String responseMessage;

	private AVSRequestDTO avsRequestDTO;

	/** Creates a new instance of SegmentSellMessageDTO */
	public AVSResponse() {

	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public AVSRequestDTO getAvsRequestDTO() {
		return avsRequestDTO;
	}

	public void setAvsRequestDTO(AVSRequestDTO avsRequestDTO) {
		this.avsRequestDTO = avsRequestDTO;
	}
}
