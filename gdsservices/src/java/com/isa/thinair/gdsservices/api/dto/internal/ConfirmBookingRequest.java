package com.isa.thinair.gdsservices.api.dto.internal;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

import java.util.ArrayList;
import java.util.Collection;

public class ConfirmBookingRequest extends GDSReservationRequestBase {

	private static final long serialVersionUID = -2747787714908455861L;

	private Collection<Passenger> passengers;

	public ConfirmBookingRequest() {
		passengers = new ArrayList<>();
	}

	@Override
	public ReservationAction getActionType() {
		return GDSInternalCodes.ReservationAction.CONFIRM_BOOKING;
	}

	public Collection<Passenger> getPassengers() {
		return passengers;
	}

	public void setPassengers(Collection<Passenger> passengers) {
		this.passengers = passengers;
	}
}
