package com.isa.thinair.gdsservices.api.dto.typea.internal;

public class CreateReservationRq extends ReservationRq {

	private String pnr;
	private String externalRecordLocator;
	private Integer gdsId;
	private Boolean priceSynced;
	private Boolean reservationSynced;

	public CreateReservationRq() {
		super(Event.CREATE);
	}


	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getExternalRecordLocator() {
		return externalRecordLocator;
	}

	public void setExternalRecordLocator(String externalRecordLocator) {
		this.externalRecordLocator = externalRecordLocator;
	}

	public Integer getGdsId() {
		return gdsId;
	}

	public void setGdsId(Integer gdsId) {
		this.gdsId = gdsId;
	}

	public Boolean getPriceSynced() {
		return priceSynced;
	}

	public void setPriceSynced(Boolean priceSynced) {
		this.priceSynced = priceSynced;
	}

	public Boolean getReservationSynced() {
		return reservationSynced;
	}

	public void setReservationSynced(Boolean reservationSynced) {
		this.reservationSynced = reservationSynced;
	}
}
