package com.isa.thinair.gdsservices.api.dto.config;

import java.io.Serializable;

public abstract class BaseFunction implements Serializable {

	private boolean isEnabled;

	public boolean isEnabled() {
		return isEnabled;
	}

	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}
}
