package com.isa.thinair.gdsservices.api.dto.publishing;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.gdsservices.core.bl.dsu.SchedulePublishUtil;

/**
 * @author Manoj Dhanushka
 */
public class ScheduleUpdateMessageDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String timeMode;

	private String airlineDesignator;

	private Date fromDateOfPeriodValidity;

	private Date toDateOfPeriodValidity;

	private Date creationDateTime;

	private int recordSerialNumber;

	private int serialNumberCheckReference;

	private String endCode;

	public static final String END = "E";

	public static final String CONTINUATION = "C";

	public String getTimeMode() {
		return timeMode;
	}

	public void setTimeMode(String timeMode) {
		this.timeMode = timeMode;
	}

	public String getAirlineDesignator() {
		if (airlineDesignator.length() == 2) {
			airlineDesignator += " ";
		}
		return airlineDesignator;
	}

	public void setAirlineDesignator(String airlineDesignator) {
		this.airlineDesignator = airlineDesignator;
	}

	public String getFromDateOfPeriodValidityString() {
		return getDateString(this.fromDateOfPeriodValidity);
	}

	public String getToDateOfPeriodValidityString() {
		if (toDateOfPeriodValidity != null) {
			return getDateString(this.toDateOfPeriodValidity);
		} else {
			return "00XXX00";
		}
	}

	public Date getFromDateOfPeriodValidity() {
		return fromDateOfPeriodValidity;
	}

	public void setFromDateOfPeriodValidity(Date fromDateOfPeriodValidity) {
		this.fromDateOfPeriodValidity = fromDateOfPeriodValidity;
	}

	public Date getToDateOfPeriodValidity() {
		return toDateOfPeriodValidity;
	}

	public void setToDateOfPeriodValidity(Date toDateOfPeriodValidity) {
		this.toDateOfPeriodValidity = toDateOfPeriodValidity;
	}

	public Date getCreationDateTime() {
		return creationDateTime;
	}

	public void setCreationDateTime(Date creationDateTime) {
		this.creationDateTime = creationDateTime;
	}

	public String getCreationDateString() {
		return getDateString(this.creationDateTime);
	}

	public String getCreationTimeString() {
		return getTimeString(this.creationDateTime);
	}

	/**
	 * Method will return String representation of date. date format : ddMMMyy eg: 08JUN05
	 * 
	 * @param date
	 * @return
	 */
	public static String getDateString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyy");
		return sdf.format(date).toUpperCase().trim();
	}

	/**
	 * Method will return String representation of time. time format : HHmm eg: 0954
	 * 
	 * @param date
	 * @return
	 */
	public static String getTimeString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
		return sdf.format(date).toUpperCase().trim();
	}

	public int getRecordSerialNumber() {
		return recordSerialNumber;
	}

	public void setRecordSerialNumber(int recordSerialNumber) {
		this.recordSerialNumber = recordSerialNumber;
	}

	public String getRecordSerialNumberString() {
		return SchedulePublishUtil.fillFrontZero(6, String.valueOf(recordSerialNumber));
	}

	public int getSerialNumberCheckReference() {
		return serialNumberCheckReference;
	}

	public void setSerialNumberCheckReference(int serialNumberCheckReference) {
		this.serialNumberCheckReference = serialNumberCheckReference;
	}

	public String getSerialNumberCheckReferenceString() {
		return SchedulePublishUtil.fillFrontZero(6, String.valueOf(serialNumberCheckReference));
	}

	public String getEndCode() {
		return endCode;
	}

	public void setEndCode(String endCode) {
		this.endCode = endCode;
	}
}
