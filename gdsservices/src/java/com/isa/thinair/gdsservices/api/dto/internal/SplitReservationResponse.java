package com.isa.thinair.gdsservices.api.dto.internal;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;

public class SplitReservationResponse extends CreateReservationRequest {
	@Override
	public GDSInternalCodes.ReservationAction getActionType() {
		return GDSInternalCodes.ReservationAction.SPLIT_RESERVATION_RESP;
	}
}
