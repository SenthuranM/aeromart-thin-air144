package com.isa.thinair.gdsservices.api.dto.internal;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.airinventory.api.model.TempSegBcAlloc;

/**
 * @author Manoj Dhanushka
 */
public class TempSegBcAllocDTO implements Serializable {
	
	private static final long serialVersionUID = 8561486118788210412L;
	
	private Integer id;

	private Integer fccaId;

	private Integer fccsbaId;

	private Integer seatsBlocked;

	private Date timestamp;

	private String bookingClassType;

	private String prevStatusChgAction;

	private Integer nestRecordId;

	private String userId;

	private String status;

	private String direction;

	private String skipSegInvUpdate;
	
	private long version = -1;

	public TempSegBcAllocDTO() {
	}

	public TempSegBcAllocDTO(TempSegBcAlloc bcAlloc) {
		id = bcAlloc.getId();
		fccaId = bcAlloc.getFccaId();
		fccsbaId = bcAlloc.getFccsbaId();
		seatsBlocked = bcAlloc.getSeatsBlocked();
		timestamp = bcAlloc.getTimestamp();
		bookingClassType = bcAlloc.getBookingClassType();
		prevStatusChgAction = bcAlloc.getPrevStatusChgAction();
		nestRecordId = bcAlloc.getNestRecordId();
		userId = bcAlloc.getUserId();
		status = bcAlloc.getStatus();
		direction = bcAlloc.getDirection();
		skipSegInvUpdate = bcAlloc.getSkipSegInvUpdate();
		version = bcAlloc.getVersion();
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the fccaId
	 */
	public Integer getFccaId() {
		return fccaId;
	}

	/**
	 * @param fccaId the fccaId to set
	 */
	public void setFccaId(Integer fccaId) {
		this.fccaId = fccaId;
	}

	/**
	 * @return the fccsbaId
	 */
	public Integer getFccsbaId() {
		return fccsbaId;
	}

	/**
	 * @param fccsbaId the fccsbaId to set
	 */
	public void setFccsbaId(Integer fccsbaId) {
		this.fccsbaId = fccsbaId;
	}

	/**
	 * @return the seatsBlocked
	 */
	public Integer getSeatsBlocked() {
		return seatsBlocked;
	}

	/**
	 * @param seatsBlocked the seatsBlocked to set
	 */
	public void setSeatsBlocked(Integer seatsBlocked) {
		this.seatsBlocked = seatsBlocked;
	}

	/**
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the bookingClassType
	 */
	public String getBookingClassType() {
		return bookingClassType;
	}

	/**
	 * @param bookingClassType the bookingClassType to set
	 */
	public void setBookingClassType(String bookingClassType) {
		this.bookingClassType = bookingClassType;
	}

	/**
	 * @return the prevStatusChgAction
	 */
	public String getPrevStatusChgAction() {
		return prevStatusChgAction;
	}

	/**
	 * @param prevStatusChgAction the prevStatusChgAction to set
	 */
	public void setPrevStatusChgAction(String prevStatusChgAction) {
		this.prevStatusChgAction = prevStatusChgAction;
	}

	/**
	 * @return the nestRecordId
	 */
	public Integer getNestRecordId() {
		return nestRecordId;
	}

	/**
	 * @param nestRecordId the nestRecordId to set
	 */
	public void setNestRecordId(Integer nestRecordId) {
		this.nestRecordId = nestRecordId;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the direction
	 */
	public String getDirection() {
		return direction;
	}

	/**
	 * @param direction the direction to set
	 */
	public void setDirection(String direction) {
		this.direction = direction;
	}

	/**
	 * @return the skipSegInvUpdate
	 */
	public String getSkipSegInvUpdate() {
		return skipSegInvUpdate;
	}

	/**
	 * @param skipSegInvUpdate the skipSegInvUpdate to set
	 */
	public void setSkipSegInvUpdate(String skipSegInvUpdate) {
		this.skipSegInvUpdate = skipSegInvUpdate;
	}

	/**
	 * @return the version
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(long version) {
		this.version = version;
	}

}
