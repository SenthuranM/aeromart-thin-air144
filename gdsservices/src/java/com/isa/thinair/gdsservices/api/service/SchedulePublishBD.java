/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2008 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.api.service;

import com.isa.thinair.airschedules.api.dto.ReScheduleRequiredInfo;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * Interface to define all the methods required to GDS Schedule Publish.
 * 
 * @author M.Rikaz
 * @since 6.0
 */
public interface SchedulePublishBD {
	public static final String SERVICE_NAME = "SchedulePublishService";

	public void publishSSMMessageForScheduleChanges(ReScheduleRequiredInfo reScheduleInfo) throws ModuleException;

}
