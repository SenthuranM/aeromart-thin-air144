package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;

import com.isa.thinair.airschedules.api.utils.ProcStdScheduleAuditBase.Operation;

public class SSMASMAuditDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String userID;
	private Integer inMessegeID;
	private Integer scheduleId;
	private SSMASMSUBMessegeDTO ssmASMSubMsgsDTO;
	private Operation operation;
	private String responseCode;
	private boolean success;
	private String rawMessage;

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public Integer getInMessegeID() {
		return inMessegeID;
	}

	public void setInMessegeID(Integer inMessegeID) {
		this.inMessegeID = inMessegeID;
	}

	public Integer getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	public SSMASMSUBMessegeDTO getSsmASMSubMsgsDTO() {
		return ssmASMSubMsgsDTO;
	}

	public void setSsmASMSubMsgsDTO(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) {
		this.ssmASMSubMsgsDTO = ssmASMSubMsgsDTO;
	}

	public Operation getOperation() {
		return operation;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getRawMessage() {
		return rawMessage;
	}

	public void setRawMessage(String rawMessage) {
		this.rawMessage = rawMessage;
	}

}
