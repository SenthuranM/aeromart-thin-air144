package com.isa.thinair.gdsservices.api.dto.typea.internal;

import java.util.List;

import com.isa.thinair.commons.api.dto.TransitionTo;
import com.isa.thinair.gdsservices.api.dto.internal.Passenger;

public class ChangePaxDetailsRq extends ReservationRq {

	private String pnr;
	private List<TransitionTo<Passenger>> nameChanges;

	public ChangePaxDetailsRq (ReservationRq.Event event) {
		super(event);
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public List<TransitionTo<Passenger>> getNameChanges() {
		return nameChanges;
	}

	public void setNameChanges(List<TransitionTo<Passenger>> nameChanges) {
		this.nameChanges = nameChanges;
	}

}
