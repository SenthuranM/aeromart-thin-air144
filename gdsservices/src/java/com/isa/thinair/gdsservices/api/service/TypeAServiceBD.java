package com.isa.thinair.gdsservices.api.service;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.msgbroker.api.model.ServiceClient;
import com.isa.typea.api.consumer.TypeADataConsumer;

public interface TypeAServiceBD extends TypeADataConsumer {
	public static final String SERVICE_NAME = "TypeAService";

	/**
	 * Send message to the specified GDS
	 * 
	 * @param gdsCode
	 * @param message
	 * @return
	 * @throws ModuleException
	 */
	public String sendMessageToGDS(String gdsCode, String message) throws ModuleException;

	/**
	 * Send message to AccelAero
	 * 
	 * @param message
	 * @return
	 * @throws ModuleException
	 */
	public String sendMessage(String message) throws ModuleException;


	public ServiceClient getGdsServiceClient(Integer gdsId) throws ModuleException;
}
