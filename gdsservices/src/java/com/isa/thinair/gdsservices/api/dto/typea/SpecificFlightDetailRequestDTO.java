package com.isa.thinair.gdsservices.api.dto.typea;

import java.util.Date;

public class SpecificFlightDetailRequestDTO {

	private String flightNumber;

	private Date secificDate;

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getSecificDate() {
		return secificDate;
	}

	public void setSecificDate(Date secificDate) {
		this.secificDate = secificDate;
	}

}
