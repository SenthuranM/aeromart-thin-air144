package com.isa.thinair.gdsservices.api.dto.internal;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;

public class Adult extends Passenger {
	private static final long serialVersionUID = 7126081674164405119L;

	private Infant infant;

	@Override
	public String getPaxType() {
		return ReservationInternalConstants.PassengerType.ADULT;
	}

	/**
	 * @return Returns the infant.
	 */
	public Infant getInfant() {
		return infant;
	}

	/**
	 * @param infant
	 *            The infant to set.
	 */
	public void setInfant(Infant infant) {
		this.infant = infant;
	}
}
