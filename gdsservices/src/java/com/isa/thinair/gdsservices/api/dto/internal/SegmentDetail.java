package com.isa.thinair.gdsservices.api.dto.internal;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.SegmentType;

public abstract class SegmentDetail {
	public abstract SegmentType getSegmentType();

	private Segment segment;
	
	private boolean changeInSceduleExists;
	private boolean alternateFlightNumber;

	/**
	 * @return the segment
	 */
	public Segment getSegment() {
		return segment;
	}

	/**
	 * @param segment
	 *            the segment to set
	 */
	public void setSegment(Segment segment) {
		this.segment = segment;
	}

	public boolean isChangeInSceduleExists() {
		return changeInSceduleExists;
	}

	public void setChangeInSceduleExists(boolean changeInSceduleExists) {
		this.changeInSceduleExists = changeInSceduleExists;
	}

	public boolean isAlternateFlightNumber() {
		return alternateFlightNumber;
	}

	public void setAlternateFlightNumber(boolean alternateFlightNumber) {
		this.alternateFlightNumber = alternateFlightNumber;
	}
}
