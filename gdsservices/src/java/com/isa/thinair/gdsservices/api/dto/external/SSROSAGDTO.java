package com.isa.thinair.gdsservices.api.dto.external;

/**
 * @author Sudheera
 * 
 */
public class SSROSAGDTO extends SSRDTO {
	private static final long serialVersionUID = 4454644143389402570L;

	private String iATACode;

	/**
	 * @return the iATACode
	 */
	public String getIATACode() {
		return iATACode;
	}

	/**
	 * @param code
	 *            the iATACode to set
	 */
	public void setIATACode(String code) {
		iATACode = code;
	}

}
