package com.isa.thinair.gdsservices.api.dto.external;

public class OSIAddressDTO extends OSIDTO {

	private static final long serialVersionUID = -5964097038543641548L;
	
	private String addressLineOne;
	
	private String addressLineTwo;
	
	private String city;

	public String getAddressLineOne() {
		return addressLineOne;
	}

	public void setAddressLineOne(String addressLineOne) {
		this.addressLineOne = addressLineOne;
	}

	public String getAddressLineTwo() {
		return addressLineTwo;
	}

	public void setAddressLineTwo(String addressLineTwo) {
		this.addressLineTwo = addressLineTwo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
}
