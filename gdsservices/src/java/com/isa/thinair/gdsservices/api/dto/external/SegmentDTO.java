package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;
import java.util.Date;

public class SegmentDTO implements Serializable {

	private static final long serialVersionUID = 3204620358519883520L;

	private String flightNumber;
	private Date departureDate;
	private String departureStation;
	private String destinationStation;
	private String bookingCode;
	private String csFlightNumber;
	private String csBookingCode;
	private Date arrivalDate;

	public String getStrDepDateLocal() {
		return strDepDateLocal;
	}

	public void setStrDepDateLocal(String strDepDateLocal) {
		this.strDepDateLocal = strDepDateLocal;
	}

	public String getStrArrivalLocal() {
		return strArrivalLocal;
	}

	public void setStrArrivalLocal(String strArrivalLocal) {
		this.strArrivalLocal = strArrivalLocal;
	}

	private String strDepDateLocal;
	private String strArrivalLocal;

	private Date departureZulu;
	private Date arrivalZulu;

	public Date getDepartureZulu() {
		return departureZulu;
	}

	public void setDepartureZulu(Date departureZulu) {
		this.departureZulu = departureZulu;
	}

	public Date getArrivalZulu() {
		return arrivalZulu;
	}

	public void setArrivalZulu(Date arrivalZulu) {
		this.arrivalZulu = arrivalZulu;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public SegmentDTO() {
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getDepartureStation() {
		return departureStation;
	}

	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}

	public String getDestinationStation() {
		return destinationStation;
	}

	public void setDestinationStation(String destinationStation) {
		this.destinationStation = destinationStation;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public String getCsFlightNumber() {
		return csFlightNumber;
	}

	public String getCsBookingCode() {
		return csBookingCode;
	}

	public void setCsFlightNumber(String csFlightNumber) {
		this.csFlightNumber = csFlightNumber;
	}

	public void setCsBookingCode(String csBookingCode) {
		this.csBookingCode = csBookingCode;
	}
}
