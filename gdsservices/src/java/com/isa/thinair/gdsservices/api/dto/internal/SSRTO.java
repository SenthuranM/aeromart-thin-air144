package com.isa.thinair.gdsservices.api.dto.internal;

public class SSRTO {

	private static final long serialVersionUID = 8561486118788210412L;

	String firstSSRCode = null;

	String otherSSRInfo = null;

	Integer segSequence = null;

	/**
	 * @return the firstSSRCode
	 */
	public String getFirstSSRCode() {
		return firstSSRCode;
	}

	/**
	 * @param firstSSRCode
	 *            the firstSSRCode to set
	 */
	public void setFirstSSRCode(String firstSSRCode) {
		this.firstSSRCode = firstSSRCode;
	}

	/**
	 * @return the segSequence
	 */
	public Integer getSegSequence() {
		return segSequence;
	}

	/**
	 * @param segSequence
	 *            the segSequence to set
	 */
	public void setSegSequence(Integer segSequence) {
		this.segSequence = segSequence;
	}

	/**
	 * @return the otherSSRInfo
	 */
	public String getOtherSSRInfo() {
		return otherSSRInfo;
	}

	/**
	 * @param otherSSRInfo
	 *            the otherSSRInfo to set
	 */
	public void setOtherSSRInfo(String otherSSRInfo) {
		this.otherSSRInfo = otherSSRInfo;
	}

}
