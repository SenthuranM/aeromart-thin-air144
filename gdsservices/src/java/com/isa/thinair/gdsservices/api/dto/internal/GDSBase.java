package com.isa.thinair.gdsservices.api.dto.internal;

import java.io.Serializable;

/**
 * Marker base for GDS objects
 * 
 * @author Nilindra Fernando
 */
public class GDSBase implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2751510085930436547L;
}
