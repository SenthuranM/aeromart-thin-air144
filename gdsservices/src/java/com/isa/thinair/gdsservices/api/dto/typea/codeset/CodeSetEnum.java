package com.isa.thinair.gdsservices.api.dto.typea.codeset;

public class CodeSetEnum {

	public enum CS0085 {

		// For communication errors
		SYNTAX_OR_VERSION_LEVEL_NOT_SUPPORTED(4, 2),
		INVALID_OR_MISSING_RECIPIENT_REFERENCE_PASSWORD(4, 11),
		UNKNOWN_INTERCHANGE_SENDER(4, 23),
		TOO_OLD(4, 24),
		DUPLICATE_DETECTED(4, 26),
		REFERENCES_DO_NOT_MATCH(4, 28),
		CONTROL_COUNT_DOES_NOT_MATCH_NUMBER_OF_INSTANCES_REPORTED(4, 29),
		PERMANENT_COMMUNICATION_NETWORK_ERROR(4, 41),
		TEMPORARY_COMMUNICATION_NETWORK_ERROR(4, 42),
		UNKNOWN_INTERCHANGE_RECIPIENT(4, 43),
		// For application errors
		INVALID_VALUE(7, 12),
		MISSING(7, 13),
		VALUE_NOT_SUPPORTED_IN_THIS_POSITION(7, 14),
		NOT_SUPPORTED_IN_THIS_POSITION(7, 15),
		UNSPECIFIED_ERROR(7, 18),
		INVALID_TYPE_OF_CHARACTER(7, 37),
		DATA_ELEMENT_TOO_LONG(7, 39),
		DATA_ELEMENT_TOO_SHORT(7, 40);

		/**
		 * 4 for commiunication error 7 for application notification with UCM
		 */
		private int action;

		/**
		 * Refer Type0085 for original codes
		 */
		private int code;

		private CS0085(int action, int code) {
			this.action = action;
			this.code = code;
		}

		/**
		 * @return the action
		 */
		public int getAction() {
			return action;
		}

		/**
		 * @return the code
		 */
		public int getCode() {
			return code;
		}

		public static CS0085 getCS0085ByCode(int code) {
			for (CS0085 cs0085 : values()) {
				if (code == cs0085.getCode())
					return cs0085;
			}
			return UNSPECIFIED_ERROR;
		}

	}

	public enum CS1225 {

		NoActionRequired("F"),
		ResourceAvailability("40"),
		ResourceAllocation("41"),
		NonFlightMovementRelatedEvent("42"),
		FlightReport("43"),
		DailyFlightAvailabilityRequest("44"),
		DailyFlightAvailabilityResponse("45"),
		SeamlessAvailabilityRequest("46"),
		SeamlessAvailabilityResponse("47"),
		DailyFlightScheduleRequest("48"),
		DailyFlightScheduleResponse("49"),
		SpecificFlightScheduleRequest("50"),
		FlightTimetableRequest("51"),
		FlightTimetableResponse("52"),
		DisplayMore("53"),
		ScanBackward("54"),
		ScanForward("55"),
		SpecificFlightAvailabilityRequest("186"),
		SpecificFlightAvailabilityResponse("187"),
		InventoryAdjustment("60"),
		Create("56"),
		Modify("57"),
		Display("131"),
		ChangeStatus("142"),

		PossibleDuplicateMessage("64"),

		/** Undefined */
		UNDEFINED("");

		private final String value;

		private CS1225(String value) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}

		public static CS1225 getEnum(String value) {
			if (value != null) {
				for (CS1225 enumSingle : CS1225.values()) {
					if (enumSingle.value.compareTo(value) == 0)
						return enumSingle;
				}
			}

			return UNDEFINED;
		}
	}

	/** Status, additional codes as per AIRIMP 7.1.2, 7.1.4 */
	public enum CS4405 {

		Add("A"),
		Accrual("AC"),
		AirportControl("AL"),
		Allocated("ALL"),
		Available("AVA"),
		Flown("B"),
		Boarded("BD"),
		Change("C"),
		CheckedIn("CK"),
		Closed("CLO"),
		Reprint("D"),
		Deboarded("DB"),
		DeniedBoarding("DN"),
		ExchangedOrReissued("E"),
		CriticalFreeText("F"),
		NonAirSegment("G"),
		OriginalIssue("I"),
		InformationOnly("IF"),
		InUse("INU"),
		IrregularOperations("IO"),

		/** Confirmed, effective, working, firm, etc... */
		Confirmed1("K"),

		LimitationsOnUse("LIM"),
		NotAvailable("NAV"),
		NotCheckedIn("NC"),
		Offloaded("OF"),
		Confirmed2("OK"),
		ReplacedItem("OLD"),
		Open("OPE"),

		/** Provisional, draft proposed subject to change, etc... */
		Provisional("P"),

		/** Partial Availability - Specified sub-elements only */
		PartialaAvailability("PAV"),

		PrintExchange("PE"),
		Printed("PR"),
		Preferred("PRF"),
		ProposedAllocation("PRP"),
		Request("R"),
		Redemption("RD"),
		Replacement("REP"),
		Revised("REV"),
		Refunded("RF"),
		Requested("RQ"),
		Suspended("S"),
		SpaceAvailable("SA"),
		Standby("SB"),
		Serviceable("SRV"),
		Ticketed("T"),
		Unserviceable("UNS"),
		Void("V"),
		Waitlisted("WL"),
		Cancel("X"),

		/* AIRIMP 7.1.2 - Action codes */
		/** Cancel confirmed/requested */
		XX("XX"),
		/** Cancel listing (waiting list) */
		XL("XL"),
		/** Cancel only if requested segment/auxiliary service is available */
		OX("OX"),
		/** Cancellation recommended (indicate reason as an OSI Supplementary Element) */
		XR("XR"),
		/**
		 * Computer System generated Action/Advice Code to report action taken as a result of previous access between
		 * computer systems (depending on bilateral agreements)
		 */
		LK("LK"),
		/** If holding, cancel */
		IK("IK"),
		/**
		 * If not Holding, Need. Same as Need; however, passenger may already be holding space. Check for duplicate
		 * reservation
		 */
		IN("IN"),
		/**
		 * If not Holding, Sold. Same as Sold; however, passenger may already be holding. Check for duplicate
		 * reservation
		 */
		IS("IS"),
		/** List (add to waiting list) */
		LL("LL"),
		/**
		 * List space available ID passenger (bilateral use - must not be used in conjunction with any other confirmed
		 * or requested or waitlisted positive space)
		 */
		SA("SA"),

		/* AIRIMP 7.1.3 Advice Code */
		/** Cancelled */
		HX("HX"),
		/** Confirming */
		KK("KK"),
		/** Confirming from waiting list */
		KL("KL"),
		/** No action taken */
		NO("NO"),
		/** Unable have waitlisted */
		UU("UU"),
		/** Unable flight does not operate or special service not provided */
		UN("UN"),
		/** Unable to accept sale */
		US("US"),
		/** Waitlist closed */
		UC("UC"),

		/** AIRIMP 7.1.4 Status Code */
		/** Have Listed (on waiting list) */
		HL("HL"),
		/** Have requested */
		HN("HN"),
		/** Have requested (bilateral use only) */
		HA("HA"),
		/** Have waitlisted (bilateral use only) */
		HW("HW"),
		/** Holds confirmed */
		HK("HK"),
		/** Reconfirmed */
		RR("RR"),
		/** Sold (between ATA members or bilateral use only) */
		HS("HS"),
		/** Space already requested as SQ. Twelve hours not yet elapsed. (between ATA members or bilateral use only) */
		HQ("HQ"),
		/** The new carrier may use the electronic coupon, exchange recommended */
		EX("EX"),
		/**
		 * The new carrier may use the electronic coupon. The coupon has not been updated to reflect the new or modified
		 * itinerary
		 */
		FC("FC"),
		/** The new carrier may not use the electronic coupon for the new/modified itinerary */
		NU("NU"),
		/** The new carrier may use the electronic coupon for the new/modified itinerary, further action may be required */
		NV("NV"),
		/** The new carrier may use the electronic coupon which has been updated with the new or modified itinerary */
		RV("RV"),
		/** Need. Reply required. To be used for involuntary rerouting */
		NI("NI"),
		/** Need. Reply required indicating action taken using appropriate code */
		NN("NN"),
		/**
		 * Need the segment specified or alternative segment immediately following. Confirm specified segment if
		 * available and take no action on alternative. If not available, reply with appropriate code and act upon
		 * alternatives in the same manner
		 */
		NA("NA"),
		/** Sold */
		SS("SS"),
		/** Sold (on free sale basis) */
		SF("SF"),
		/** Space requested. Reply only if not available (between ATA Members and/or bilateral use only) */
		SQ("SQ"),

		/** Undefined */
		UNDEFINED("");

		private final String value;

		private CS4405(String value) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}

		public static CS4405 getEnum(String value) {
			if (value != null) {
				for (CS4405 enumSingle : CS4405.values()) {
					if (enumSingle.value.compareTo(value) == 0)
						return enumSingle;
				}
			}

			return UNDEFINED;
		}
	}

	/** Number of units qualifier */
	public enum CS6353 {

		Adult("A"),
		BlockSpace("BS"),
		Child("C"),
		Corporate("CP"),
		Female("F"),
		Group("G"),
		Individual("I"),
		InfantFemale("IF"),
		InfantMale("IM"),
		Infant("IN"),
		IndividualWithinAGroup("IZ"),
		AirportLoungeMember("L"),
		Male("M"),
		NumberOfMealsServed("ML"),
		MaximumNumberOfFlightsDesired("MX"),
		Military("N"),
		NumberOfColumns("NC"),
		NumberOfLines("NL"),
		NumberOfSeatsOccupiedByPaxOnBoard("PX"),
		SameSurname("S"),
		StandbyPositive("SP"),
		StandbySpaceAvailable("SS"),
		FrequentTraveler("T"),
		TotalSeatsAvailableToAssign("TA"),
		TotalCabinClassCapacity("TC"),
		NumberOfTicket("TD"),

		/** Total cabin/compartment seats with acknowledgment pending for seats */
		TotalCabinSeatsWithAcknowledgmentPending("TCA"),

		TotalSeatsSold("TS"),
		TotalSeatsUnassigned("TU"),

		/** Total unassigned seats with acknowledgment pending for seats */
		TotalUnassignedSeatsWithAcknowledgmentPending("TUA"),

		/** Undefined */
		UNDEFINED("");

		private final String value;

		private CS6353(String value) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}

		public static CS6353 getEnum(String value) {
			if (value != null) {
				for (CS6353 enumSingle : CS6353.values()) {
					if (enumSingle.value.compareTo(value) == 0)
						return enumSingle;
				}
			}

			return UNDEFINED;
		}
	}

	/** Processing indicator */
	public enum CS7365 {

		Acknowledgment("ACK"),
		AllFlightsToBeProcessed("AF"),
		AdditionalInformation("AI"),

		/** Boarding pass may not be issued until the mutually agreed time period */
		BoardingNotToBeIssuedUntil("B"),

		BlindSell("BS"),
		ContactInformation("C"),
		ChangeOfDate("CD"),

		/** Change date minus 1 day */
		Minus1Day("CM1"),

		CascadingNotAllowed("CN"),

		/** Change date plus 2 days */
		Plus2Days("CP2"),

		CascadingAllowed("CY"),
		Diplomatic("DP"),
		ExcessBagsCharged("EC"),
		ExcessBagsIdentified("EI"),
		ExcessBagsWaived("EW"),
		FormOfPaymentDetails("F"),
		FirstAvailable("FA"),

		/** Bagtag issuance required by querying system */
		BagtagIssuanceRequired1("FE"),

		SeatRequestNotFulfilled("FN"),
		FareOrTaxOrTotalDetails("FT"),
		SeatRequestFulfilled("FY"),
		GreenCard("GC"),
		HeadOfBaggagePool("HP"),
		ActionBasedOnJourney("J"),

		/** Bagtag issuance required by responding system */
		BagtagIssuanceRequired2("MH"),

		MilitaryID("MI"),
		MemberOfBaggagePool("MP"),
		NoActionRequired("N"),
		BoardingPassMayNotBeIssued("NB"),
		NotPooled("NP"),

		/** Requested city pair, no seat data applies */
		NoSeatDataApplies("NS"),

		ActionRequired("P"),

		/** Purchaser ticketing restriction/conditions */
		PurchaserTicketingConditions("PC"),

		PartialPassengerIndicator("PI"),
		PartialPassengerOrPartialSegmentIndicator("PP"),
		PartialSegmentIndicator("PS"),
		Passport("PT"),
		RoutingInformation("R"),
		ReservationsDetails("RD"),

		/** Seat assignment association - desires seating together */
		SeatAssignmentAssociation("SA"),

		SeamanOrSailor("SS"),
		TotalAmountCollected("T"),

		/** This flight only to be processed */
		FlightToBeProcessed("TF"),

		Visa("VI"),
		BoardingCanBeIssued("Y"),
		Pooled("YP"),

		/** Undefined */
		UNDEFINED("");

		private final String value;

		private CS7365(String value) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}

		public static CS7365 getEnum(String value) {
			if (value != null) {
				for (CS7365 enumSingle : CS7365.values()) {
					if (enumSingle.value.compareTo(value) == 0)
						return enumSingle;
				}
			}

			return UNDEFINED;
		}
	}

	/** Application error code */
	public enum CS9321 {

		AppError_1("1", "Invalid date"),
		AppError_10("10", "Invalid service type code"),
		AppError_100("100", "Invalid Place of Departure Code"),
		AppError_101("101", "Invalid Place of Destination Code"),
		AppError_102("102", "Invalid Departure Date"),
		AppError_103("103", "Invalid Departure Time"),
		AppError_104("104", "Invalid Reservation Booking Designator"),
		AppError_105("105", "Invalid Reservation Booking Modifier"),
		AppError_106("106", "Invalid Number of Inventory Adjustments"),
		AppError_107("107", "Invalid Airline Designator Vendor Supplier"),
		AppError_108("108", "Invalid Place of Connection Code"),
		AppError_109("109", "Invalid Country Code"),
		AppError_11("11", "Invalid aircraft type coded"),
		AppError_110("110", "Invalid Source of Business"),
		AppError_111("111", "Invalid Agent's Code"),
		AppError_112("112", "Requestor Identification Required"),
		AppError_113("113", "Invalid Period of Operation"),
		AppError_114("114", "Invalid Flight Number"),
		AppError_115("115", "Invalid Arrival Date"),
		AppError_116("116", "Invalid Arrival Time"),
		AppError_117("117", "Schedule Change in Progress"),
		AppError_118("118", "System Unable to Process"),
		AppError_119("119", "Shuttle Flight Does Not Allow Reservation"),
		AppError_12("12", "Invalid product details qualifier coded"),
		AppError_120("120", "Invalid Action Code"),
		AppError_121("121", "Invalid Number in Party"),
		AppError_122("122", "Unable Flight Beyond System Capacity"),
		AppError_123("123", "Unable Flight Postponed"),
		AppError_124("124", "Unable Flight Purged"),
		AppError_125("125", "Unable Charter Flight"),
		AppError_126("126", "Unable Codeshare Flight"),
		AppError_127("127", "Unable Group Action Required for Number in Party Requested"),
		AppError_128("128", "Unable  Overbook Not Allowed for Group Code"),
		AppError_129("129", "No PNR Match Found"),
		AppError_13("13", "Invalid aircraft configuration version coded"),
		AppError_130("130", "Invalid Origin and Destination Pair"),
		AppError_131("131", "Invalid Flight Cancel Pair"),
		AppError_132("132", "Exceeds Maximum Number of Segments"),
		AppError_133("133", "FLIFO Exists for this Flight"),
		AppError_134("134", "Advise Times Different from Booked"),
		AppError_135("135", "Advise Dates Different from Booked"),
		AppError_136("136", "Advise Class Different from Booked"),
		AppError_137("137", "Name Change Not Allowed"),
		AppError_138("138", "Name Change Not Allowed, PTA PNR"),
		AppError_139("139", "Name Change Not Allowed, Baggage Info Exists"),
		AppError_14("14", "Invalid meal service note coded"),
		AppError_140("140", "Name Change Not Allowed this Passenger Type Code"),
		AppError_141("141", "Name Change Not Allowed on group name"),
		AppError_142("142", "Name Change Not Allowed Passport Info Exists"),
		AppError_143("143", "Invalid or Ineligible Passenger Type Code"),
		AppError_144("144", "Invalid Requestor Identification"),
		AppError_145("145", "Number in Party Exceeds Maximum"),
		AppError_146("146", "Unequal Number of Names Number in Party"),
		AppError_147("147", "Given Names Title Unequal to Number in Party"),
		AppError_148("148", "Invalid Action on Name Change"),
		AppError_149("149", "Surname too Long"),
		AppError_15("15", "Invalid UTC local time variation"),
		AppError_150("150", "Given Name Title too Long"),
		AppError_151("151", "Surname Mandatory"),
		AppError_152("152", "Given Name Title Mandatory"),
		AppError_153("153", "Name Mismatch"),
		AppError_154("154", "Message Function Invalid"),
		AppError_155("155", "Message Function Not Supported"),
		AppError_156("156", "Business Function Invalid"),
		AppError_157("157", "Business Function Not Supported"),
		AppError_158("158", "No Action Taken  Simultaneous Changes to the Same Passenger Record"),
		AppError_159("159", "Phone Field Required"),
		AppError_16("16", "Invalid date variation"),
		AppError_160("160", "Passenger Name in Message Does Not Match Name in Booking File PNR"),
		AppError_161("161", "Invalid OSI Type"),
		AppError_162("162", "OSI Type Not Supported"),
		AppError_163("163", "The SSR Service Code is not Valid"),
		AppError_164("164", "The SSR Action Code is not Valid for the SSR Service Code"),
		AppError_165("165", "The SSR Action Code is not Valid"),
		AppError_166("166", "The SSR Action Code is not Permitted for this SSR Service Code"),
		AppError_167("167", "Invalid Number of Services specified in SSR"),
		AppError_168("168", "The SSR Flight Number is not Valid"),
		AppError_169("169", "The SSR Flight Number is not Contained in the Itinerary"),
		AppError_17("17", "Invalid terminal coded"),
		AppError_170("170", "The SSR Class or RBD is not Valid"),
		AppError_171("171", "The SSR Class or RBD Does Not Match the Segment in the Itinerary"),
		AppError_172("172", "The SSR Date is not Valid"),
		AppError_173("173", "The SSR Date does not match the Segment in the Itinerary"),
		AppError_174("174", "The SSR Flight Description is Invalid"),
		AppError_175("175", "The SSR Segment Boardpoint and or Offpoint is not Valid"),
		AppError_176("176", "The SSR Segment Boardpoint and or Offpoint Does Not Match the Segment in the Itinerary"),
		AppError_177("177",
				"The SSR Name(s) do not Match the Name(s) in the Name field of the PNR or Contains Invalid Characters"),
		AppError_178("178", "The SSR Number of Names is not Equal to the Number of Special Services Requested"),
		AppError_179("179", "A Free Text Description is Mandatory for this SSR Service Code"),
		AppError_18("18", "Invalid ontime performance indicator"),
		AppError_180("180", "The SSR Free Text Description Length is in Error"),
		AppError_181("181", "A Free Text Description is not Permitted for this SSR Service Code"),
		AppError_182("182", "The SSR is not available and the Service has been waitlisted"),
		AppError_183("183", "The SSR is not available and the Waitlist is not Allowed"),
		AppError_184("184", "The SSR is not available and the Waitlist is Closed"),
		AppError_185("185", "SSR Service Code is not Offered"),
		AppError_186("186", "SSR Service Code is not Offered on this Flight Description"),
		AppError_187("187", "Format of Content of Required Information with this SSR Service Code is Invalid"),
		AppError_188("188", "Invalid SSR Measure Unit Qualifier, Age or Weight"),
		AppError_189("189", "Invalid SSR Measure Unit Value"),
		AppError_19("19", "Invalid type of call at port coded"),
		AppError_190("190", "Invalid Processing Indicator"),
		AppError_191("191", "Name Reference Required"),
		AppError_192("192", "Name Reference Inhibited"),
		AppError_193("193", "Segment Reference Required"),
		AppError_194("194", "Segment Reference Inhibited"),
		AppError_195("195", "Need Action Code"),
		AppError_196("196", "Invalid Flight Status for this SSR"),
		AppError_197("197", "Advance Seat Selection Not Available"),
		AppError_198("198", "Advance Seat Selection Not Available, Airport CheckIn is Active"),
		AppError_199("199", "Advance Seat Selection Temporarily Not Available, Maintenance In Progress"),
		AppError_2("2", "Invalid time"),
		AppError_20("20", "Invalid government action coded"),
		AppError_200("200", "Advance Seat Selection Not Available for this Flight"),
		AppError_201("201", "Advance Seat Selection Not Available for this Class Compartment Zone"),
		AppError_202("202", "Advance Seat Selection Not Available this Carrier"),
		AppError_203("203", "Advance Seat Selection Authorized for Full Fare Passengers Only"),
		AppError_204("204", "Advance Seat Selection Temporarily Not Available, Schedule Change active"),
		AppError_205("205", "Advance Seat Selection Temporarily Not Available, Operational Decision"),
		AppError_206("206", "Advance Seat Selection Temporarily Not Available, Application Suspended"),
		AppError_207("207", "Advance Seat Selection Temporarily Not Authorized this Passenger Type"),
		AppError_208("208", "Advance Seat Selection Not Available, Flight Already Departed"),
		AppError_209("209", "Advance Seat Selection Not Available, Flight Does Not Operate"),
		AppError_21("21", "Invalid facility type coded"),
		AppError_210("210", "Advance Seat Selection Not Available, Flight Does Not Operate this Date"),
		AppError_211("211", "Advance Seat Selection Not Available, Flight Does Not Operate this Date this Board Point"),
		AppError_212("212", "Advance Seat Selection Not Available, Flight Does Not Operate this Date this Off Point"),
		AppError_213("213", "Advance Seat Selection Not Available, Flight Does Not Operate this Date this Board and or Off Point"),
		AppError_214("214", "Advance Seat Assignment Cancelled, Rerequest Seats"),
		AppError_215("215", "Advance Seat Request Code Share Carrier Flight  Seat Number or Zone will Be Advised"),
		AppError_216("216", "Advance Seat Request is not Within Minimum Maximum Flight Date Time Range"),
		AppError_217("217", "Advance Seat Request is not Within Minimum Maximum Flight Date Range"),
		AppError_218("218", "Advance Seat Request is not Within Minimum Maximum Flight Time Range"),
		AppError_219("219", "Advance Seat Request on Code Share Carrier Flight Restricted"),
		AppError_22("22", "Invalid traffic restriction coded"),
		AppError_220("220", "Bassinet Seat has been Reserved"),
		AppError_221("221", "Bassinet NonFilm Viewing Seat has been Reserved"),
		AppError_222("222", "Boarding Pass Request on Code Share Carrier Flight Restricted"),
		AppError_223("223", "Boarding Pass Request is not Within Minimum Maximum Flight Date Time Range"),
		AppError_224("224", "Boarding Pass Request is not Within Minimum Maximum Flight Date Range"),
		AppError_225("225", "Boarding Pass Request is not Within Minimum Maximum Flight Time Range"),
		AppError_226("226", "Boarding Pass Request Denied, Airport CheckIn Active"),
		AppError_227("227", "Boarding Pass Request Denied, Maintenance In Progress"),
		AppError_228("228", "Boarding Pass Request Denied, For this Flight"),
		AppError_229("229", "Boarding Pass Request Denied, This Class Compartment Zone"),
		AppError_23("23", "Invalid traffic restriction type coded"),
		AppError_230("230", "Boarding Pass Request Denied, This Carrier"),
		AppError_231("231", "Boarding Pass Request Denied, Authorized for Full Fare Passengers Only"),
		AppError_232("232", "Boarding Pass Request Temporarily Not Available, Schedule Change Active"),
		AppError_233("233", "Boarding Pass Request Temporarily Not Available, Operational Decision"),
		AppError_234("234", "Boarding Pass Request Temporarily Not Available, Application Suspended"),
		AppError_235("235", "Boarding Pass Request Temporarily Not Authorized this Passenger Type"),
		AppError_236("236", "Boarding Pass Request Not Available, Flight Already Departed"),
		AppError_237("237", "Boarding Pass Request Not Available, Flight Does Not Operate"),
		AppError_238("238", "Boarding Pass Request Not Available, Flight Does Not Operate this Date"),
		AppError_239("239", "Boarding Pass Request Not Available, Flight Does Not Operate this Date this Board Point"),
		AppError_24("24", "Invalid traffic restriction qualifier coded"),
		AppError_240("240", "Boarding Pass Request Not Available, Flight Does Not Operate this Date this Off Point"),
		AppError_241("241", "Boarding Pass Request Not Available, Flight Does Not Operate this Date this Board and or Off Point"),
		AppError_242("242", "Handicap Seat has been Reserved"),
		AppError_243("243", "Invalid Boardpoint and or Off Point in Boarding Pass Request"),
		AppError_244("244", "Not allowed on marketing flights"),
		AppError_245("245", "Please request on operating carrier"),
		AppError_247("247", "Invalid Flight Number in Boarding Pass Request"),
		AppError_248("248", "Invalid Boardpoint in Boarding Pass Request"),
		AppError_249("249", "Invalid Offpoint in Boarding Pass Request"),
		AppError_25("25", "Invalid transport stage qualifier coded"),
		AppError_250("250", "Invalid Flight Date for Advance Seat Request"),
		AppError_251("251", "Invalid Boardpoint and or Offpoint in Advance Seat Request"),
		AppError_252("252", "Invalid Flight Number in Advance Seat Request"),
		AppError_253("253", "Invalid Boardpoint in Advance Seat Request"),
		AppError_254("254", "Invalid Offpoint in Advance Seat Request"),
		AppError_255("255", "Invalid Seat Number, for this Class Compartment"),
		AppError_256("256", "Invalid Seat Number for Aircraft Type"),
		AppError_257("257", "Invalid Seat Number Format"),
		AppError_258("258", "Invalid Generic Seat Characteristic Code"),
		AppError_259("259", "Invalid Combination of Generic Codes"),
		AppError_260("260", "Invalid Frequent Traveler Number"),
		AppError_261("261", "Invalid Row Number, For this Class Compartment"),
		AppError_262("262", "Invalid Row Number, For this Aircraft Configuration"),
		AppError_263("263", "Invalid Row Number Format"),
		AppError_264("264", "Invalid Row Request, Number of Seats Required Exceeds Row Size"),
		AppError_265("265", "Invalid Seat Facility Characteristic for the Seat Requested"),
		AppError_266("266", "Limited Recline Seat has been Reserved"),
		AppError_267("267", "Name in the Request Does Not Match Name in Reservation"),
		AppError_268("268", "Need Names, Passenger Name Information Required to Reserve Seats"),
		AppError_269("269", "Indicates that Additional Data for this Message Reference Number follows"),
		AppError_270("270", "NonFilm Viewing Seat has been Reserved"),
		AppError_271("271", "Number of Seats Requested Exceeds Maximum Allowed"),
		AppError_272("272", "Number of Seats Requested Does Not Equal the Number in Party"),
		AppError_273("273", "Request Specific Seat Number, Generic Request Inhibited this Flight"),
		AppError_274("274", "Request Specific Seat Number for Bassinet Seat"),
		AppError_275("275", "Request Specific Seat Number for NonFilm Viewing Seat"),
		AppError_276("276", "Requested Seat Type Not Available"),
		AppError_277("277", "Seat Request Includes Duplicate Seat Numbers Codes"),
		AppError_278("278", "Seat Request Restricted During Divided PNR"),
		AppError_279("279", "Seat Request Restricted During Name Change"),
		AppError_280("280", "Seat Request Restricted During Reduce Number in Party"),
		AppError_281("281", "Specific Seat Requested Not Available"),
		AppError_282("282",
				"Specific Seat Requested Not Available, Only One Infant may be Accommodated in each Row Section, and One is Already Taken"),
		AppError_283("283", "Specific Seat Requested Not Available  Restricted"),
		AppError_284("284", "Specific Seat Requested Not Available for Unaccompanied Minor"),
		AppError_285("285", "Simultaneous Changes to PNR"),
		AppError_286("286", "Through Seat Not Available"),
		AppError_287("287", "Unable to Satisfy, Need Smoking NoSmoking Code"),
		AppError_288("288", "Unable to Satisfy, Need Confirmed Flight Status"),
		AppError_289("289", "Unable to Retrieve PNR, Database Error"),
		AppError_290("290", "Unable to Process Advance Seat Request"),
		AppError_291("291", "Unable to Process Boarding Pass Request"),
		AppError_292("292", "Segment Sells Restricted"),
		AppError_293("293", "Unable to Sell Due to Sales Limit being Reached"),
		AppError_294("294", "Invalid Format"),
		AppError_295("295", "No Flights Requested"),
		AppError_3("3", "Invalid transfer sequence"),
		AppError_301("301", "Application Suspended"),
		AppError_302("302", "Invalid Length"),
		AppError_303("303", "Flight Cancelled"),
		AppError_304("304", "System Temporarily Unavailable"),
		AppError_305("305", "Security Audit Failure"),
		AppError_306("306", "Invalid Language Code"),
		AppError_307("307", "Received from data missing"),
		AppError_308("308", "Received from data invalid"),
		AppError_309("309", "Received from data missing or invalid"),
		AppError_310("310", "Ticket arrangement data missing"),
		AppError_311("311", "Ticket arrangement data invalid"),
		AppError_312("312", "Ticket arrangement data missing or invalid"),
		AppError_313("313", "Name element data missing"),
		AppError_314("314", "Name element data invalid"),
		AppError_315("315", "Name element data missing or invalid"),
		AppError_316("316", "Contact element (phone and or address) missing"),
		AppError_317("317", "Contact element (phone and or address) invalid"),
		AppError_318("318", "Contact element (phone and or address) missing or invalid"),
		AppError_319("319", "Name element order error"),
		AppError_320("320", "Invalid segment status"),
		AppError_321("321", "Duplicate flight segment"),
		AppError_322("322", "Segment continuity error"),
		AppError_323("323", "Invalid itinerary order"),
		AppError_324("324", "Number of infants exceeds maximum allowed per adult passenger"),
		AppError_325("325", "Related system reference error"),
		AppError_326("326", "Sending system reference error"),
		AppError_327("327", "Invalid segment data in itinerary"),
		AppError_328("328", "Invalid aircraft registration"),
		AppError_329("329", "Invalid hold version"),
		AppError_330("330", "invalid loading type"),
		AppError_331("331", "Invalid ULD type"),
		AppError_332("332", "Invalid serial number"),
		AppError_333("333", "Invalid contour height code"),
		AppError_334("334", "Invalid tare weight"),
		AppError_335("335", "Unit code unknown"),
		AppError_336("336", "Invalid category code"),
		AppError_337("337", "Invalid special load reference"),
		AppError_338("338", "Invalid weight status"),
		AppError_339("339", "Weight volume exceeded"),
		AppError_340("340", "Invalid PCS WGT QTY"),
		AppError_341("341", "UN vs PSN error"),
		AppError_342("342", "Not suitable for passenger aircraft"),
		AppError_343("343", "Not suitable for cargo aircraft"),
		AppError_344("344", "Not suitable for hold"),
		AppError_345("345", "Not suitable for compartment"),
		AppError_346("346", "Not suitable for position"),
		AppError_347("347", "Incompatible loads"),
		AppError_348("348", "Invalid flight status"),
		AppError_349("349", "Invalid application product identification"),
		AppError_350("350", "Flight under airport control"),
		AppError_351("351", "Invalid cancel  married connection single leg(s) not available"),
		AppError_352("352", "Link to inventory system is unavailable"),
		AppError_353("353", "Exceeds number of flights for inventory system book individually"),
		AppError_354("354", "Invalid product status"),
		AppError_356("356", "Query control tables full"),
		AppError_357("357", "Declined to process interactivelydefault to backup"),
		AppError_360("360", "Invalid PNR file address"),
		AppError_361("361", "PNR is secured"),
		AppError_362("362", "Unable to display PNR and or name list"),
		AppError_363("363", "PNR too long"),
		AppError_364("364", "Invalid ticket number"),
		AppError_365("365", "Invalid responding system inhouse code"),
		AppError_366("366", "Name list too long"),
		AppError_367("367", "No active itinerary"),
		AppError_368("368", "Not authorized"),
		AppError_369("369", "No PNR is active, redisplay PNR required"),
		AppError_370("370", "Simultaneous changes to PNR"),
		AppError_371("371", "Prior PNR display required"),
		AppError_372("372", "PNR has already been claim"),
		AppError_373("373", "Ineligible for claim  all open auxiliary itinerary"),
		AppError_374("374", "Ineligible for claim  ticket by mail PTA present PNR already ticketed"),
		AppError_375("375", "Requestor not authorized for this function for this PNR"),
		AppError_376("376", "Pricing ticketing error, text information specified"),
		AppError_377("377", "Unable to price, multiple discounts apply"),
		AppError_378("378", "No rule pricing error, text information specified"),
		AppError_379("379", "Joint fare pricing error, text information specified"),
		AppError_380("380", "Ineligible for claim in current forman update must be made before claiming"),
		AppError_381("381", "Record locator required"),
		AppError_382("382", "PNR status value not equal"),
		AppError_383("383", "Invalid change to status code"),
		AppError_384("384", "Multiple Name Matches"),
		AppError_385("385", "Mutually exclusive optional parameters"),
		AppError_386("386", "Invalid minimum maximum connect time specified"),
		AppError_387("387", "Blackout for this date"),
		AppError_388("388", "Invalid cancel due married connection; remaining segment(s) not available in connection market"),
		AppError_389("389", "Invalid cancel due married connection; remaining segment(s) not available in local market"),
		AppError_390("390", "Unable to reformat"),
		AppError_391("391", "PNR contains non air segments"),
		AppError_392("392", "Invalid block type name"),
		AppError_393("393", "Block sell restrictedrequest block name change"),
		AppError_394("394", "Segment not valid for electronic ticketing"),
		AppError_395("395", "Already ticketed"),
		AppError_396("396", "Invalid ticket coupon status"),
		AppError_397("397", "Maximum ticket limit reached"),
		AppError_398("398", "Advance Boarding Pass already exists"),
		AppError_399("399", "Duplicate Name"),
		AppError_4("4", "Invalid city airport code"),
		AppError_400("400", "Duplicate ticket number"),
		AppError_401("401", "Ticket number not found"),
		AppError_403("403", "Requested Data Not Sorted"),
		AppError_404("404", "No Service Between Requested Cities Airports"),
		AppError_405("405", "No Service Between Requested Cities Airports for Parameters"),
		AppError_406("406", "No Service Between Requested Cities Airports for Parameters for Connection Specified"),
		AppError_407("407", "No Service Between Requested Cities Airports for Parameters for Carrier Specified"),
		AppError_408("408", "No More Data Available"),
		AppError_409("409", "Request is Outside System Date Range"),
		AppError_410("410", "Flight does not operate due to weather, mechanical or other operational conditions"),
		AppError_411("411", "Flight Does Not Operate on Date Requested"),
		AppError_412("412", "Flight Does Not Operate Between Requested Cities"),
		AppError_413("413", "Requested Flight Not Active"),
		AppError_414("414", "Need Prior Availability Request"),
		AppError_415("415", "Need Prior Schedule Request"),
		AppError_416("416", "Need Prior Timetable Request"),
		AppError_417("417", "Repeat Request Updating in Progress"),
		AppError_418("418", "Flight has Departed"),
		AppError_419("419", "Flight is Boarding"),
		AppError_420("420", "Not Available but Waitlist is Open"),
		AppError_421("421", "Not Available and Waitlist is Closed"),
		AppError_422("422", "Not Available Due to Traffic Restrictions"),
		AppError_423("423", "Free ticket request rejected; not enough points for FQTV"),
		AppError_424("424", "Card number does not match with name for FQTV"),
		AppError_425("425", "Discount not applicable on the company for FQTV"),
		AppError_426("426", "Card expiry date is returned for FQTV"),
		AppError_427("427", "Ticket issuance not authorized; number not verified for FQTV"),
		AppError_428("428", "Verification restricted for this requestor for FQTV"),
		AppError_429("429", "Product account number verification denied for FQTV"),
		AppError_430("430", "Invalid product account number for FQTV"),
		AppError_431("431", "Invalid discount code for FQTV"),
		AppError_432("432", "Returned discount code is different for FQTV"),
		AppError_433("433", "Returned company code is different for FQTV"),
		AppError_434("434", "Additional product is returned for FQTV"),
		AppError_435("435", "Unable  flight operated by another carrier code share sales limit reached"),
		AppError_436("436", "Not available due to mixed fare class"),
		AppError_437("437", "Carrier must be in separate PNR"),
		AppError_438("438", "Request is outside system date range for this carrier within this system"),
		AppError_439("439", "Waitlisting not allowed for this carrier within this system"),
		AppError_440("440", "Void request on already voided or printed coupons"),
		AppError_441("441", "Invalid Priority Number"),
		AppError_442("442", "System in control of device is unknown"),
		AppError_443("443", "Device address is unknown"),
		AppError_444("444", "Part number is out of sequence"),
		AppError_445("445", "Security indicator requires originator ID"),
		AppError_446("446", "Device cannot handle this material"),
		AppError_447("447", "Ineligible for claimPNR available for display"),
		AppError_448("448", "Contact Service Provider directly"),
		AppError_449("449", "Request generic seat, specific seat inhibited this class compartment"),
		AppError_450("450", "All electronic ticket coupons have been printed"),
		AppError_451("451", "Only individual seat request allowed at this time"),
		AppError_452("452", "Ineligible for claim  contains flown segment"),
		AppError_453("453", "Bulk fare(s) not supported"),
		AppError_454("454", "Multiple Seat Request Allowed Only in the Same Row, Airport Checkin is Active"),
		AppError_455("455", "UnablePrevious Segment in this Requested Connection is Waitlisted"),
		AppError_456("456", "Passive Segment already exists"),
		AppError_457("457", "UnableConnection Market Unavailable"),
		AppError_458("458", "No Agreement for Passive Notification"),
		AppError_459("459", "Owner Equal to Requestor"),
		AppError_460("460", "PNR Controlled by Third Party System"),
		AppError_461("461", "Ineligible for Requested Action  Group PNR not allowed"),
		AppError_462("462", "Invalid authorization number"),
		AppError_463("463", "Authorization number already used"),
		AppError_464("464", "Unable to price ticket due to fare calculation too large"),
		AppError_465("465", "Unable to Sell  incompatible with existing itinerary"),
		AppError_466("466", "Form of payment missing or invalid for electronic ticketing"),
		AppError_467("467", "Flight segment not found in carrier's PNR"),
		AppError_468("468", "Invalid routing due to 2 or more open JAWs"),
		AppError_469("469", "Invalid routing due to more than 2 stopovers"),
		AppError_470("470", "Invalid routing due to more than 2 connections on an O&D segment"),
		AppError_471("471", "Invalid routing due to open JAW itinerary has enroute stopover"),
		AppError_472("472", "Account does not allow redemption"),
		AppError_473("473", "No award inventory available for requested market and date"),
		AppError_5("5", "Invalid time mode"),
		AppError_6("6", "Invalid operational suffix"),
		AppError_7("7", "Invalid period of schedule validity"),
		AppError_700("700", "Invalid OSI"),
		AppError_701("701", "Invalid SSR"),
		AppError_702("702", "Error at End of Transaction"),
		AppError_703("703", "No negotiated space is available"),
		AppError_704("704", "Invalid Tour Identification"),
		AppError_705("705", "Invalid or missing Coupon Booklet Number"),
		AppError_706("706", "Invalid Certificate Number"),
		AppError_707("707", "Invalid or missing baggage details"),
		AppError_708("708", "Incorrect credit card information"),
		AppError_709("709", "Invalid and or missing frequent traveler information"),
		AppError_70A("70A", "Airport checkin identification (FOID) type not supported"),
		AppError_70B("70B", "Airport checkin identification (FOID) not supported"),
		AppError_70C("70C", "Request for airport control denied"),
		AppError_70D("70D", "Display criteria not supported"),
		AppError_70E("70E", "Open return permitted for additional charge"),
		AppError_70F("70F", "No passenger name for corporate PNR"),
		AppError_70G("70G", "Booked class not in tariff data base"),
		AppError_70H("70H", "Fare based on open jaw or circle trip which may apply"),
		AppError_70I("70I", "Unable to find fare for mixed passenger type code fare type"),
		AppError_70J("70J", "City pair restricted"),
		AppError_70K("70K", "Ticketing record corrupted needs repricing"),
		AppError_70L("70L", "Unable to price  passenger type code has changed"),
		AppError_70M("70M", "City airport code not in tariff data base"),
		AppError_70N("70N", "Must contact validating carrier for ETicket Access"),
		AppError_70O("70O", "City pair not in tariff data base"),
		AppError_70P("70P", "An unpriceable discount fare may apply"),
		AppError_70Q("70Q", "End on end normal fare  through fare may be lower"),
		AppError_70R("70R", "Unpriceable directional minimum fare may apply"),
		AppError_70S("70S", "Too many routings apply"),
		AppError_70T("70T", "Point to point fare undercuts through fare  check carrier rule"),
		AppError_70U("70U", "Unable to price multiple trips airlines classes"),
		AppError_70V("70V", "Booked class for specified city pair inhibited"),
		AppError_70W("70W", "Sum of locals applied to trip"),
		AppError_70X("70X", "Fare load in progress"),
		AppError_70Y("70Y", "Unable to locate gateway fare for U.S. tax calculation"),
		AppError_70Z("70Z", "Departure arrival times are required"),
		AppError_710("710", "Free text qualifier error"),
		AppError_711("711", "Invalid Fare Calculation Status Code"),
		AppError_712("712", "Missing and or invalid monetary information"),
		AppError_713("713", "Invalid Price Type Qualifier"),
		AppError_714("714", "Missing and or invalid Date of Issue"),
		AppError_715("715", "Invalid fare basis"),
		AppError_716("716", "Missing and or invalid reservation control information"),
		AppError_717("717", "Missing and or invalid travel agent and or system identification"),
		AppError_718("718", "Invalid Document Type"),
		AppError_719("719", "No fares available"),
		AppError_71A("71A", "Unable to price in requested currency"),
		AppError_71B("71B", "Currency ticketing location in conflict"),
		AppError_71C("71C", "Check segments booked comply with ticketing rule"),
		AppError_71D("71D", "Multiple excursion fare may apply"),
		AppError_71E("71E", "Too many eligible fares for joint trip"),
		AppError_71F("71F", "Multiple rules may apply to joint fare trip"),
		AppError_71G("71G", "Cannot apply airline code as requested"),
		AppError_71H("71H", "Unable to process flight times that were given"),
		AppError_71I("71I", "Selected trip fare basis code not priceable"),
		AppError_71J("71J", "Travel to from turnaround or connection point not continuous valid"),
		AppError_71K("71K", "International city not in priceable zone"),
		AppError_71L("71L", "Open jaw  fare type not a normal fare"),
		AppError_71M("71M", "Unpriceable surface segment"),
		AppError_71N("71N", "Multiple international trips  separate tickets required"),
		AppError_71O("71O", "Board point and ticket location are in different countries"),
		AppError_71P("71P", "Taxes for booked itinerary country not in tariff data base"),
		AppError_71Q("71Q", "Currency not in tariff data base"),
		AppError_71R("71R", "Invalid decimal usage for currency specified"),
		AppError_71S("71S", "Unable to establish turnaround point"),
		AppError_71T("71T", "Unpriceable side trip"),
		AppError_71U("71U", "Mileage exceeds maximum 25 percent increase"),
		AppError_71V("71V", "Unable to determine stopover rule application"),
		AppError_71W("71W", "Mileage not stored in tariff data base"),
		AppError_71X("71X", "Selected fare basis code not combinable"),
		AppError_71Y("71Y", "Unable to price local fares"),
		AppError_71Z("71Z", "Routing verification requested"),
		AppError_720("720", "No rules exist for this fare"),
		AppError_721("721", "Too much data"),
		AppError_722("722", "Invalid rule"),
		AppError_723("723", "Invalid category"),
		AppError_724("724", "Invalid routing"),
		AppError_725("725", "Domestic itinerary"),
		AppError_726("726", "Invalid global indicator"),
		AppError_727("727", "Invalid amount"),
		AppError_728("728", "Invalid conversion type"),
		AppError_729("729", "Invalid currency code"),
		AppError_72A("72A", "Unable to establish guaranteed air fare application  conflicting rules between carriers"),
		AppError_72B("72B", "Unable to verify routing"),
		AppError_72C("72C", "Unable to price as booked by low fare finder"),
		AppError_72D("72D", "Invalid mandatory construction point"),
		AppError_72E("72E", "Too many possible break points  provide turnaround and or break points"),
		AppError_72F("72F", "Invalid discount type option"),
		AppError_72G("72G", "Invalid point of sale option"),
		AppError_72H("72H", "Invalid bankers selling rate"),
		AppError_72I("72I", "Nonschedules non publishing airline unpriceable"),
		AppError_72J("72J", "Purchase ticketing requirements not met"),
		AppError_72K("72K", "Eligible fares not valid for travel dates time"),
		AppError_72L("72L", "NUC conversion restricted"),
		AppError_72M("72M", "Special international fare may require separate ticket"),
		AppError_72N("72N", "Higher intermediate point may apply"),
		AppError_72O("72O", "Unable to calculate required minimum check"),
		AppError_72P("72P", "Unable to calculate percent surcharge"),
		AppError_72Q("72Q", "City not in priceable zone"),
		AppError_72R("72R", "Pricing option requires amount or percentage"),
		AppError_72S("72S", "Specific passenger type code required  check fare rule"),
		AppError_72T("72T", "Country of payment minimum may apply"),
		AppError_72U("72U", "Country of origin minimum may apply"),
		AppError_72V("72V", "International surface restricted  separate tickets required"),
		AppError_72W("72W", "Low fare finder not allowed on train segments"),
		AppError_72X("72X", "Promotional certificate number required"),
		AppError_72Y("72Y", "Discount from total fare not valid for nonUS itinerary"),
		AppError_72Z("72Z", "Override carrier not in booked itinerary"),
		AppError_730("730", "No fare on this market and or carrier"),
		AppError_731("731", "No fare for compartment"),
		AppError_732("732", "No fare for rule option used"),
		AppError_733("733", "Request not processed per Resolution 100 rules"),
		AppError_734("734", "Too many fares. Enter specific date and or fare type."),
		AppError_735("735", "Unable  claim in progress"),
		AppError_736("736", "Unable  board point required"),
		AppError_737("737", "Unable  Flight operated by a third party"),
		AppError_738("738", "Overflow"),
		AppError_739("739", "BBR rate not available"),
		AppError_73A("73A", "Fare component exceeds maximum international arrivals departures"),
		AppError_73B("73B", "No fare of equal or higher value found for automated reissue"),
		AppError_73C("73C", "ET not available for offline use"),
		AppError_73D("73D", "Unable to convert to ticketing currency"),
		AppError_73E("73E", "Unable to price  currency restrictions"),
		AppError_73F("73F", "Multiple currencies apply  reprice using passenger type code(s)"),
		AppError_73G("73G", "Unable to price due to mixed classes requiring a differential charge that cannot be calculated"),
		AppError_740("740", "Rate not available"),
		AppError_741("741", "Piece concept applicable"),
		AppError_742("742", "No fare on this market carrier"),
		AppError_743("743", "Electronic ticket record purposely not accessable"),
		AppError_744("744", "Missing or invalid airport checkin identification (FOID)"),
		AppError_745("745", "Refund (full or partial) not allowed"),
		AppError_746("746", "Open segment(s) not permitted for first coupon or entire itinerary"),
		AppError_747("747", "Validity date(s) required for electronic tickets"),
		AppError_748("748", "Status change denied"),
		AppError_749("749", "Coupon status not open"),
		AppError_750("750", "Endorsement restriction"),
		AppError_751("751", "Controlled by a third party"),
		AppError_752("752", "Revalidation request denied"),
		AppError_753("753", "Invalid or missing frequent traveler number"),
		AppError_754("754", "Electronic ticket outside validity date"),
		AppError_755("755", "Invalid exchange coupon media"),
		AppError_756("756", "Tour fares not supported"),
		AppError_757("757", "Exchange paper to electronic ticket not allowed"),
		AppError_758("758", "Net remit fares not supported"),
		AppError_759("759", "Multiple forms of payment not allowed"),
		AppError_760("760", "Conjunction ticket numbers are not sequential"),
		AppError_761("761", "Exchange Reissue must include all unused coupons"),
		AppError_762("762", "Invalid tax amount"),
		AppError_763("763", "Invalid tax code"),
		AppError_764("764", "Unavailable  have requested"),
		AppError_765("765", "Penalties do not allow changes"),
		AppError_766("766", "Ticket has no residual value"),
		AppError_767("767", "Historical data not available  unable to process"),
		AppError_768("768", "Changes not allowed on repriced segments  process manually"),
		AppError_769("769", "Unable to calculate residual value  process manually"),
		AppError_770("770", "Unable to determine penalty or original fare process manually"),
		AppError_771("771", "Carrier reissue rule invalid for origin destination change  process manually"),
		AppError_772("772", "Unable to keep original fare per carrier requirements  process manually"),
		AppError_773("773", "Unable to determine residual value  all flights must be flown"),
		AppError_774("774", "Unable to keep original fare  invalid PTC"),
		AppError_775("775", "Unable to keep original fare  day time requirements not met"),
		AppError_776("776", "Unable to keep original fare  seasonality requirements not met"),
		AppError_777("777", "Unable to keep original fare  applicable flight requirements not met"),
		AppError_778("778", "Unable to keep original fare  advance purchase requirements not met"),
		AppError_779("779", "Unable to keep original fare  minimum stay requirements not met"),
		AppError_780("780", "Unable to keep original fare  maximum stay exceeded"),
		AppError_781("781", "Unable to keep original fare  stopover requirements not met"),
		AppError_782("782", "Unable to keep original fare  transfer requirements not met"),
		AppError_783("783", "Unable to keep original fare  combinability not allowed"),
		AppError_784("784", "Unable to keep original fare  travel not allowed on blackout dates"),
		AppError_785("785", "Unable to keep original fare  accompanied travel requirements not met"),
		AppError_786("786", "Unable to keep original fare  travel restrictions not met"),
		AppError_787("787", "Unable to keep original fare  sales restrictions not met"),
		AppError_788("788", "Unable to keep original fare  new itinerary not eligible for original fare"),
		AppError_789("789", "New fare level in effect for new date of travel"),
		AppError_790("790", "Exchange denied  no further exchanges allowed"),
		AppError_791("791", "Unable to void exchanged reissued ticket"),
		AppError_792("792", "Segment not eligible for interline electronic ticket"),
		AppError_793("793", "Fare tax amount too long"),
		AppError_794("794", "Invalid or missing fare calculation"),
		AppError_795("795", "Invalid, missing or conflicting search criteria"),
		AppError_796("796", "Partial void of ticket coupons not allowed"),
		AppError_797("797", "Invalid stopover indicator"),
		AppError_798("798", "Invalid stopover code usage"),
		AppError_799("799", "Electronic ticket exists, no match on specified criteria"),
		AppError_79A("79A", "Invalid office identification"),
		AppError_79B("79B", "Already working another queue"),
		AppError_79C("79C", "Not allowed to access queues for specified office identification"),
		AppError_79D("79D", "Queue identifier has not been assigned for specified office identification"),
		AppError_79E("79E", "Attempting to perform a queue function when not associated with a queue"),
		AppError_79F("79F", "Queue placement or add new queue item is not allowed for the specified office id and queue id"),
		AppError_79G("79G", "Coupon already checked in"),
		AppError_79H("79H", "Meal order exceeded"),
		AppError_79I("79I", "Weight limitation warning"),
		AppError_79J("79J", "Passenger on notification list"),
		AppError_79K("79K", "Internet checkin not allowed for this carrier"),
		AppError_79L("79L", "Internet checkin not allowed for this flight"),
		AppError_79M("79M", "Internet checkin not allowed for this airport"),
		AppError_79N("79N", "Transfer from flight is closed for finalizing (reaccommodation)"),
		AppError_79O("79O", "Tranfer from flight is finally closed (reaccommodation)"),
		AppError_79P("79P", "Transfer to flight is closed for finalizing (reaccommodation)"),
		AppError_79Q("79Q", "Transfer to flight is finally closed (reaccommodation)"),
		AppError_79R("79R", "Complete baggagepool party must be included in request (reaccommodation)"),
		AppError_79S("79S", "Transfer to flight number does not exist (reaccommodation)"),
		AppError_79T("79T", "Transfer to flight destination does not exist (reaccommodation)"),
		AppError_79U("79U", "Passenger through checked  arrival airport must not be changed (reaccommodation)"),
		AppError_8("8", "Invalid days of operation"),
		AppError_9("9", "Invalid frequency rate"),
		AppError_900("900", "Inactivity Time Out Value Exceeded"),
		AppError_901("901", "Communications Line Unavailable"),
		AppError_902("902", "Prior message being processed or already processed"),
		AppError_911("911", "Unable to process  system error"),
		AppError_912("912", "Incomplete message  data missing in query"),
		AppError_913("913", "Item data not found or data not existing in processing host"),
		AppError_914("914", "Invalid format data  data does not match EDIFACT rules"),
		AppError_915("915", "No action  processing host cannot support function"),
		AppError_916("916", "EDIFACT version not supported"),
		AppError_917("917", "EDIFACT message size exceeded");

		private final String value;

		private final String errorText;

		private CS9321(String value, String errorText) {
			this.value = value;
			this.errorText = errorText;
		}

		public String getValue() {
			return this.value;
		}

		public String getErrorText() {
			return errorText;
		}

		public static CS9321 getEnum(String value) {
			if (value != null) {
				for (CS9321 enumSingle : CS9321.values()) {
					if (enumSingle.value.compareTo(value) == 0)
						return enumSingle;
				}
			}

			return null;
		}
	}

}
