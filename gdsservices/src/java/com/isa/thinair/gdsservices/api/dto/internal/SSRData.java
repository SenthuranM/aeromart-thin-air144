package com.isa.thinair.gdsservices.api.dto.internal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.gdsservices.api.dto.external.SSRChildDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRCreditCardDetailDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDetailDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDocoDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRDocsDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSREmergencyContactDetailDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRInfantDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRMinorDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSROSAGDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSRPassportDTO;
import com.isa.thinair.gdsservices.api.dto.external.SsrTicketingTimeLimitDTO;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;

public class SSRData {
	private Collection<SSRPassportDTO> passportSSRDTOs;

	private Map<String, SSREmergencyContactDetailDTO> ssrEmergencyContactDetailDTOMap;

	private Map<String, SSRChildDTO> childSSRDTOMap;

	private Map<String, SSRMinorDTO> minorSSRDTOMap;

	private Map<String, SSRInfantDTO> infantSSRDTOMap;

	private Collection<SSRDetailDTO> detailSSRDTOs;
	
	private Collection<SSRDocsDTO> docsSSRDTOs;
	
	private Collection<SSRDocoDTO> docoSSRDTOs;

	private Collection<SSRDTO> ssrDTOs;

	private SSRDTO eTicketSSRDTO;

	private SsrTicketingTimeLimitDTO ticketingTimeLimit;

	private SSRCreditCardDetailDTO creditCardDetailSSRDTO;

	private SSROSAGDTO oSAGSSRDTO;

	private SSRData() {
		passportSSRDTOs = new ArrayList<SSRPassportDTO>();
		ssrEmergencyContactDetailDTOMap = new HashMap<String, SSREmergencyContactDetailDTO>();
		childSSRDTOMap = new HashMap<String, SSRChildDTO>();
		minorSSRDTOMap = new HashMap<String, SSRMinorDTO>();
		infantSSRDTOMap = new HashMap<String, SSRInfantDTO>();
		detailSSRDTOs = new ArrayList<SSRDetailDTO>();
		docsSSRDTOs = new ArrayList<SSRDocsDTO>();
		docoSSRDTOs = new ArrayList<SSRDocoDTO>();
		ssrDTOs = new ArrayList<SSRDTO>();
	}

	public SSRData(Collection<SSRDTO> ssrDTOs) {
		this();
		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

		for (Iterator<SSRDTO> iterator = ssrDTOs.iterator(); iterator.hasNext();) {
			SSRDTO ssrDTO = (SSRDTO) iterator.next();

			// if (carrierCode.equals(ssrDTO.getCarrierCode())) { //only relevant SSRs receiving
			if (ssrDTO instanceof SSREmergencyContactDetailDTO) {
				SSREmergencyContactDetailDTO ssrContactDTO = (SSREmergencyContactDetailDTO) ssrDTO;
				String paxNameIATA = GDSApiUtils.getIATAName(ssrContactDTO.getPassengerLastName(),
						ssrContactDTO.getPassengerFirstName(), ssrContactDTO.getPassengerTitle());

				this.getSsrEmergencyContactDetailDTOMap().put(paxNameIATA, ssrContactDTO);
			} else if (ssrDTO instanceof SSRPassportDTO) {
				SSRPassportDTO passportSSRDTO = (SSRPassportDTO) ssrDTO;

				this.getPassportSSRDTOs().add(passportSSRDTO);
			} else if (ssrDTO instanceof SSRInfantDTO) {
				SSRInfantDTO ssrInfantDTO = (SSRInfantDTO) ssrDTO;
				String paxNameIATA = null;

				if (ssrInfantDTO.isSeatRequired()) {
					paxNameIATA = GDSApiUtils.getIATAName(ssrInfantDTO.getInfantLastName(), ssrInfantDTO.getInfantLastName(),
							ssrInfantDTO.getInfantTitle());
				} else {
					paxNameIATA = ssrInfantDTO.getIATAGuardianName();
				}

				this.getInfantSSRDTOMap().put(paxNameIATA, ssrInfantDTO);
			} else if (ssrDTO instanceof SSRChildDTO) {
				SSRChildDTO ssrChildDTO = (SSRChildDTO) ssrDTO;
				String paxNameIATA = GDSApiUtils.getIATAName(ssrChildDTO.getLastName(), ssrChildDTO.getFirstName(),
						ssrChildDTO.getTitle());

				this.getChildSSRDTOMap().put(paxNameIATA, ssrChildDTO);
			} else if (ssrDTO instanceof SSRMinorDTO) {
				SSRMinorDTO ssrMinorDTO = (SSRMinorDTO) ssrDTO;
				String paxNameIATA = GDSApiUtils.getIATAName(ssrMinorDTO.getLastName(), ssrMinorDTO.getFirstName(),
						ssrMinorDTO.getTitle());

				this.getMinorSSRDTOMap().put(paxNameIATA, ssrMinorDTO);
			} else if (ssrDTO instanceof SSRDetailDTO) {
				this.getDetailSSRDTOs().add((SSRDetailDTO) ssrDTO);
			} else if (ssrDTO instanceof SSRDetailDTO) {
				this.getDetailSSRDTOs().add((SSRDetailDTO) ssrDTO);
			} else if (ssrDTO instanceof SSRDocsDTO) {
				this.getDocsSSRDTOs().add((SSRDocsDTO) ssrDTO);
			} else if (ssrDTO instanceof SSRDocoDTO) {
				this.getDocoSSRDTOs().add((SSRDocoDTO) ssrDTO);
			} else if (ssrDTO instanceof SSROSAGDTO) {
				this.setOSAGSSRDTO((SSROSAGDTO) ssrDTO);
			}  else if (ssrDTO instanceof SsrTicketingTimeLimitDTO) {
				this.setTicketingTimeLimit((SsrTicketingTimeLimitDTO) ssrDTO);
			} else {
				if (GDSExternalCodes.SSRCodes.ETICKET_NO.getCode().equals(ssrDTO.getCodeSSR())
						|| GDSExternalCodes.SSRCodes.ET_NOT_CHANGED.getCode().equals(ssrDTO.getCodeSSR())) {
					this.setETicketSSRDTO(ssrDTO);
				} else {
					this.getSsrDTOs().add(ssrDTO);
				}
			}
			// }
		}
	}

	/**
	 * @return the passportSSRDTOs
	 */
	public Collection<SSRPassportDTO> getPassportSSRDTOs() {
		return passportSSRDTOs;
	}

	/**
	 * @param passportSSRDTOs
	 *            the passportSSRDTOs to set
	 */
	public void setPassportSSRDTOs(Collection<SSRPassportDTO> passportSSRDTOs) {
		this.passportSSRDTOs = passportSSRDTOs;
	}

	/**
	 * @return the ssrEmergencyContactDetailDTOMap
	 */
	public Map<String, SSREmergencyContactDetailDTO> getSsrEmergencyContactDetailDTOMap() {
		return ssrEmergencyContactDetailDTOMap;
	}

	/**
	 * @param ssrEmergencyContactDetailDTOMap
	 *            the ssrEmergencyContactDetailDTOMap to set
	 */
	public void setSsrEmergencyContactDetailDTOMap(Map<String, SSREmergencyContactDetailDTO> emergencyContactDetailSSRDTOMap) {
		this.ssrEmergencyContactDetailDTOMap = emergencyContactDetailSSRDTOMap;
	}

	/**
	 * @return the docsSSRDTOs
	 */
	public Collection<SSRDocsDTO> getDocsSSRDTOs() {
		return docsSSRDTOs;
	}

	/**
	 * @param docsSSRDTOs the docsSSRDTOs to set
	 */
	public void setDocsSSRDTOs(Collection<SSRDocsDTO> docsSSRDTOs) {
		this.docsSSRDTOs = docsSSRDTOs;
	}

	/**
	 * @return the childSSRDTOMap
	 */
	public Map<String, SSRChildDTO> getChildSSRDTOMap() {
		return childSSRDTOMap;
	}

	/**
	 * @param childSSRDTOMap
	 *            the childSSRDTOMap to set
	 */
	public void setChildSSRDTOMap(Map<String, SSRChildDTO> childSSRDTOMap) {
		this.childSSRDTOMap = childSSRDTOMap;
	}

	/**
	 * @return the minorSSRDTOMap
	 */
	public Map<String, SSRMinorDTO> getMinorSSRDTOMap() {
		return minorSSRDTOMap;
	}

	/**
	 * @param minorSSRDTOMap
	 *            the minorSSRDTOMap to set
	 */
	public void setMinorSSRDTOMap(Map<String, SSRMinorDTO> minorSSRDTOMap) {
		this.minorSSRDTOMap = minorSSRDTOMap;
	}

	/**
	 * @return the infantSSRDTOMap
	 */
	public Map<String, SSRInfantDTO> getInfantSSRDTOMap() {
		return infantSSRDTOMap;
	}

	/**
	 * @param infantSSRDTOMap
	 *            the infantSSRDTOMap to set
	 */
	public void setInfantSSRDTOMap(Map<String, SSRInfantDTO> infantSSRDTOMap) {
		this.infantSSRDTOMap = infantSSRDTOMap;
	}

	/**
	 * @return the detailSSRDTOs
	 */
	public Collection<SSRDetailDTO> getDetailSSRDTOs() {
		return detailSSRDTOs;
	}

	/**
	 * @param detailSSRDTOs
	 *            the detailSSRDTOs to set
	 */
	public void setDetailSSRDTOs(Collection<SSRDetailDTO> detailSSRDTOs) {
		this.detailSSRDTOs = detailSSRDTOs;
	}

	/**
	 * @return the ssrDTOs
	 */
	public Collection<SSRDTO> getSsrDTOs() {
		return ssrDTOs;
	}

	/**
	 * @param ssrDTOs
	 *            the ssrDTOs to set
	 */
	public void setSsrDTOs(Collection<SSRDTO> ssrDTOs) {
		this.ssrDTOs = ssrDTOs;
	}

	/**
	 * @return the eTicketSSRDTO
	 */
	public SSRDTO getETicketSSRDTO() {
		return eTicketSSRDTO;
	}

	/**
	 * @param ticketSSRDTO
	 *            the eTicketSSRDTO to set
	 */
	public void setETicketSSRDTO(SSRDTO ticketSSRDTO) {
		eTicketSSRDTO = ticketSSRDTO;
	}

	/**
	 * @return the creditCardDetailSSRDTO
	 */
	public SSRCreditCardDetailDTO getCreditCardDetailSSRDTO() {
		return creditCardDetailSSRDTO;
	}

	/**
	 * @param creditCardDetailSSRDTO
	 *            the creditCardDetailSSRDTO to set
	 */
	public void setCreditCardDetailSSRDTO(SSRCreditCardDetailDTO creditCardDetailSSRDTO) {
		this.creditCardDetailSSRDTO = creditCardDetailSSRDTO;
	}

	/**
	 * @return the oSAGSSRDTO
	 */
	public SSROSAGDTO getOSAGSSRDTO() {
		return oSAGSSRDTO;
	}

	/**
	 * @param osagssrdto
	 *            the oSAGSSRDTO to set
	 */
	public void setOSAGSSRDTO(SSROSAGDTO osagssrdto) {
		oSAGSSRDTO = osagssrdto;
	}

	public SsrTicketingTimeLimitDTO getTicketingTimeLimit() {
		return ticketingTimeLimit;
	}

	public void setTicketingTimeLimit(SsrTicketingTimeLimitDTO ticketingTimeLimit) {
		this.ticketingTimeLimit = ticketingTimeLimit;
	}

	public Collection<SSRDocoDTO> getDocoSSRDTOs() {
		return docoSSRDTOs;
	}

	public void setDocoSSRDTOs(Collection<SSRDocoDTO> docoSSRDTOs) {
		this.docoSSRDTOs = docoSSRDTOs;
	}
	
}
