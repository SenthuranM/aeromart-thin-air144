package com.isa.thinair.gdsservices.api.dto.typea;

import java.io.Serializable;

import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;

public class ERCDetail implements Serializable {

	private static final long serialVersionUID = 3805919267394203564L;

	private CodeSetEnum.CS9321 errorCode;

	private int level;

	public ERCDetail(CodeSetEnum.CS9321 errorCode, int level) {
		super();
		this.errorCode = errorCode;
		this.level = level;
	}

	/**
	 * @return the errorCode
	 */
	public CodeSetEnum.CS9321 getErrorCode() {
		return errorCode;
	}

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

}
