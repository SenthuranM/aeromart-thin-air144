package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;

public class OSIDTO implements Serializable {

	private static final long serialVersionUID = -4397251160939667062L;

	private String codeOSI;

	private String carrierCode;

	private String osiValue;

	private String fullElement;

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getCodeOSI() {
		return codeOSI;
	}

	public void setCodeOSI(String codeOSI) {
		this.codeOSI = codeOSI;
	}

	public String getFullElement() {
		return fullElement;
	}

	public void setFullElement(String fullElement) {
		this.fullElement = fullElement;
	}

	public String getOsiValue() {
		return osiValue;
	}

	public void setOsiValue(String osiValue) {
		this.osiValue = osiValue;
	}
}
