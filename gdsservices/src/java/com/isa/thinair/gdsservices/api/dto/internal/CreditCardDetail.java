package com.isa.thinair.gdsservices.api.dto.internal;

import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.PaymentType;

public class CreditCardDetail implements PaymentDetail {
	private static final long serialVersionUID = 3048998403205028127L;

	private String cardType;

	private String cardNo;

	private String holderName;

	// MMYY
	private String expiryDate;

	private String secureCode;

	public PaymentType getPaymentType() {
		return PaymentType.CREDIT_CARD;
	}

	/**
	 * @return Returns the cardNo.
	 */
	public String getCardNo() {
		return cardNo;
	}

	/**
	 * @param cardNo
	 *            The cardNo to set.
	 */
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	/**
	 * @return Returns the cardType.
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * @param cardType
	 *            The cardType to set.
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @return Returns the expiryDate.
	 */
	public String getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @param expiryDate
	 *            The expiryDate to set.
	 */
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * @return Returns the holderName.
	 */
	public String getHolderName() {
		return holderName;
	}

	/**
	 * @param holderName
	 *            The holderName to set.
	 */
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	/**
	 * @return Returns the secureCode.
	 */
	public String getSecureCode() {
		return secureCode;
	}

	/**
	 * @param secureCode
	 *            The secureCode to set.
	 */
	public void setSecureCode(String pinNumber) {
		this.secureCode = pinNumber;
	}

	public boolean isEmpty() {
		if (GDSApiUtils.maskNull(getCardType()).length() == 0 || GDSApiUtils.maskNull(getCardNo()).length() == 0
				|| GDSApiUtils.maskNull(getHolderName()).length() == 0 || GDSApiUtils.maskNull(getExpiryDate()).length() == 0
				|| GDSApiUtils.maskNull(getSecureCode()).length() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
