package com.isa.thinair.gdsservices.api.dto.external;

import java.util.Date;

public class BookingSegmentDTO extends SegmentDTO {
	private static final long serialVersionUID = 4057056918676032057L;

	private Date departureTime;
	private Date arrivalTime;
	private int dayOffSet;
	private String dayOffSetSign;
	private int noofPax = 0;
	private String membersBlockIdentifier;
	private String actionOrStatusCode;
	private String adviceOrStatusCode;
	private String carrierCode;
	private String csCarrierCode;
	private boolean changeInScheduleExist;
	private boolean primeFlight;

	/**
	 * @return the carrierCode
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            the carrierCode to set
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the actionOrStatusCode
	 */
	public String getActionOrStatusCode() {
		return actionOrStatusCode;
	}

	/**
	 * @param actionOrStatusCode
	 *            the actionOrStatusCode to set
	 */
	public void setActionOrStatusCode(String actionOrStatusCode) {
		this.actionOrStatusCode = actionOrStatusCode;
	}

	/**
	 * @return the membersBlockIdentifier
	 */
	public String getMembersBlockIdentifier() {
		return membersBlockIdentifier;
	}

	/**
	 * @param membersBlockIdentifier
	 *            the membersBlockIdentifier to set
	 */
	public void setMembersBlockIdentifier(String membersBlockIdentifier) {
		this.membersBlockIdentifier = membersBlockIdentifier;
	}

	public Date getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public int getDayOffSet() {
		return dayOffSet;
	}

	public void setDayOffSet(int dayOffSet) {
		this.dayOffSet = dayOffSet;
	}

	public String getDayOffSetSign() {
		return dayOffSetSign;
	}

	public void setDayOffSetSign(String dayOffSetSign) {
		this.dayOffSetSign = dayOffSetSign;
	}

	public Date getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	public int getNoofPax() {
		return noofPax;
	}

	public void setNoofPax(int noofPax) {
		this.noofPax = noofPax;
	}

	/**
	 * @return the adviceOrStatusCode
	 */
	public String getAdviceOrStatusCode() {
		return adviceOrStatusCode;
	}

	/**
	 * @param adviceOrStatusCode
	 *            the adviceOrStatusCode to set
	 */
	public void setAdviceOrStatusCode(String adviceOrStatusCode) {
		this.adviceOrStatusCode = adviceOrStatusCode;
	}

	public boolean isChangeInScheduleExist() {
		return changeInScheduleExist;
	}

	public void setChangeInScheduleExist(boolean changeInScheduleExist) {
		this.changeInScheduleExist = changeInScheduleExist;
	}

	public String getCsCarrierCode() {
		return csCarrierCode;
	}

	public void setCsCarrierCode(String csCarrierCode) {
		this.csCarrierCode = csCarrierCode;
	}

	public boolean isPrimeFlight() {
		return primeFlight;
	}

	public void setPrimeFlight(boolean primeFlight) {
		this.primeFlight = primeFlight;
	}
}
