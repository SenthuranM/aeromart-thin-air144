/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.api.dto.internal;

import java.util.Collection;
import java.util.Set;

/**
 * To hold reservation authentication data transfer information
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class GDSCredentialsDTO extends GDSBase {
	private static final long serialVersionUID = 8182820884808253566L;

	/** Holds user id */
	private String userId;

	/** Holds sales channel code */
	private Integer salesChannelCode;

	/** Holds agent code */
	private String agentCode;

	/** Holds the agent station */
	private String agentStation;

	/** Holds the user dst information */
	private Collection colUserDST;

	/** Holds user default carrier code */
	private String defaultCarrierCode;

	/** Holds the privilege set */
	private Set<String> privileges;

	/**
	 * @return Returns the salesChannelCode.
	 */
	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	/**
	 * @param salesChannelCode
	 *            The salesChannelCode to set.
	 */
	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	/**
	 * @return Returns the userId.
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            The userId to set.
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return Returns the agentCode.
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return Returns the colUserDST.
	 */
	public Collection getColUserDST() {
		return colUserDST;
	}

	/**
	 * @param colUserDST
	 *            The colUserDST to set.
	 */
	public void setColUserDST(Collection colUserDST) {
		this.colUserDST = colUserDST;
	}

	/**
	 * @return Returns the agentStation.
	 */
	public String getAgentStation() {
		return agentStation;
	}

	/**
	 * @param agentStation
	 *            The agentStation to set.
	 */
	public void setAgentStation(String agentStation) {
		this.agentStation = agentStation;
	}

	/**
	 * @return Returns the defaultCarrierCode.
	 */
	public String getDefaultCarrierCode() {
		return defaultCarrierCode;
	}

	/**
	 * @param defaultCarrierCode
	 *            The defaultCarrierCode to set.
	 */
	public void setDefaultCarrierCode(String defaultCarrierCode) {
		this.defaultCarrierCode = defaultCarrierCode;
	}

	/**
	 * @return Returns the privileges.
	 */
	public Set<String> getPrivileges() {
		return privileges;
	}

	/**
	 * @param privileges
	 *            The privileges to set.
	 */
	public void setPrivileges(Set<String> privileges) {
		this.privileges = privileges;
	}

	public boolean hasPrivilege(String privilegeToCheck) {
		boolean res = false;
		if (privileges != null && !privileges.isEmpty() && privileges.contains(privilegeToCheck)) {
			res = true;
		}

		return res;
	}

}
