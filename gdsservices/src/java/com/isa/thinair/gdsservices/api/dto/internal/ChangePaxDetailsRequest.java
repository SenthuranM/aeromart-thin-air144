package com.isa.thinair.gdsservices.api.dto.internal;

import java.util.Map;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

public class ChangePaxDetailsRequest extends GDSReservationRequestBase {

	private static final long serialVersionUID = -4542316905993114070L;

	private Map<Passenger, Passenger> oldNewPassengerDetails;
	
	private boolean infantNameChangeExists;
	
	private boolean nameChangeExists;
	
	private String userNote;
	
	/**
	 * @return the oldNewPassengerDetails
	 */
	public Map<Passenger, Passenger> getOldNewPassengerDetails() {
		return oldNewPassengerDetails;
	}

	/**
	 * @param oldNewPassengerDetails
	 *            the oldNewPassengerDetails to set
	 */
	public void setOldNewPassengerDetails(Map<Passenger, Passenger> oldNewPassengerDetails) {
		this.oldNewPassengerDetails = oldNewPassengerDetails;
	}

	@Override
	public ReservationAction getActionType() {
		return GDSInternalCodes.ReservationAction.CHANGE_PAX_DETAILS;
	}

	public boolean isInfantNameChangeExists() {
		return infantNameChangeExists;
	}

	public void setInfantNameChangeExists(boolean infantNameChangeExists) {
		this.infantNameChangeExists = infantNameChangeExists;
	}

	/**
	 * @return the nameChangeExists
	 */
	public boolean isNameChangeExists() {
		return nameChangeExists;
	}

	/**
	 * @param nameChangeExists the nameChangeExists to set
	 */
	public void setNameChangeExists(boolean nameChangeExists) {
		this.nameChangeExists = nameChangeExists;
	}

	/**
	 * @return the userNote
	 */
	public String getUserNote() {
		return userNote;
	}

	/**
	 * @param userNote the userNote to set
	 */
	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}
}
