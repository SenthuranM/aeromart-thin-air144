package com.isa.thinair.gdsservices.api.dto.typea;

import java.io.Serializable;

public class TypeBRS implements Serializable {

	public enum ResponseStatus {
		SUCCESSFUL, FAILED
	}

	private ResponseStatus responseStatus = ResponseStatus.SUCCESSFUL;
	
	private String responseMessage;
	
	private boolean synchronousCommunication;

	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(ResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public boolean isSynchronousCommunication() {
		return synchronousCommunication;
	}

	public void setSynchronousCommunication(boolean synchronousCommunication) {
		this.synchronousCommunication = synchronousCommunication;
	}
}
