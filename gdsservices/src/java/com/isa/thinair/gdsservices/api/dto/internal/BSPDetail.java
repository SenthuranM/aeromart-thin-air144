package com.isa.thinair.gdsservices.api.dto.internal;

import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.PaymentType;

public class BSPDetail implements PaymentDetail {
	private String iATACode = null;

	public PaymentType getPaymentType() {
		return PaymentType.BSP;
	}

	/**
	 * @return the iATACode
	 */
	public String getIATACode() {
		return iATACode;
	}

	/**
	 * @param code
	 *            the iATACode to set
	 */
	public void setIATACode(String code) {
		iATACode = code;
	}

	public boolean isEmpty() {
		if (GDSApiUtils.maskNull(getIATACode()).length() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
