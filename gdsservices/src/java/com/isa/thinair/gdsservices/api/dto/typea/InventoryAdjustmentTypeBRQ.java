package com.isa.thinair.gdsservices.api.dto.typea;

import java.util.Collection;

import com.isa.thinair.gdsservices.api.dto.internal.TempSegBcAllocDTO;

public class InventoryAdjustmentTypeBRQ extends TypeBRQ {

	private Collection<TempSegBcAllocDTO> blockedSeats;
	private int paxCount;
	private boolean sendReservationImage;

	public InventoryAdjustmentTypeBRQ() {
		sendReservationImage = false;
	}

	public Collection<TempSegBcAllocDTO> getBlockedSeats() {
		return blockedSeats;
	}

	public void setBlockedSeats(Collection<TempSegBcAllocDTO> blockedSeats) {
		this.blockedSeats = blockedSeats;
	}

	public int getPaxCount() {
		return paxCount;
	}

	public void setPaxCount(int paxCount) {
		this.paxCount = paxCount;
	}

	public boolean isSendReservationImage() {
		return sendReservationImage;
	}

	public void setSendReservationImage(boolean sendReservationImage) {
		this.sendReservationImage = sendReservationImage;
	}
}
