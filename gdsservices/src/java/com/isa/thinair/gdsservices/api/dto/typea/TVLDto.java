package com.isa.thinair.gdsservices.api.dto.typea;

import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;

public class TVLDto {

	private TVLDetail tvlDetail;

	private int paxCount;

	private AvailableFlightDTO availabilityResult;

	private CodeSetEnum.CS4405 resultCode;

	private Set<ERCDetail> ercDetails;

	public TVLDetail getTvlDetail() {
		return tvlDetail;
	}

	public void setTvlDetail(TVLDetail tvlDetail) {
		this.tvlDetail = tvlDetail;
	}

	public int getPaxCount() {
		return paxCount;
	}

	public void setPaxCount(int paxCount) {
		this.paxCount = paxCount;
	}

	public CodeSetEnum.CS4405 getResultCode() {
		return resultCode;
	}

	public void setResultCode(CodeSetEnum.CS4405 resultCode) {
		this.resultCode = resultCode;
	}

	/**
	 * @return the ercDetails
	 */
	public Set<ERCDetail> getErcDetails() {
		if (ercDetails == null) {
			ercDetails = new HashSet<ERCDetail>();
		}
		return ercDetails;
	}

	/**
	 * @param ercDetails
	 *            the ercDetails to set
	 */
	public void setErcDetails(Set<ERCDetail> ercDetails) {
		if (ercDetails == null) {
			ercDetails = new HashSet<ERCDetail>();
		}
		this.ercDetails = ercDetails;
	}

	public void addErcDetails(ERCDetail ercDetail) {
		getErcDetails().add(ercDetail);
	}

	public boolean isErrorSegment() {
		return !getErcDetails().isEmpty();
	}

	/**
	 * @return the availabilityResult
	 */
	public AvailableFlightDTO getAvailabilityResult() {
		return availabilityResult;
	}

	/**
	 * @param availabilityResult
	 *            the availabilityResult to set
	 */
	public void setAvailabilityResult(AvailableFlightDTO availabilityResult) {
		this.availabilityResult = availabilityResult;
	}

	public AvailableFlightSearchDTO getAvailableFlightSearchDTO() {
		AvailableFlightSearchDTO dto = new AvailableFlightSearchDTO();
		dto.setDepatureDateTimeStart(tvlDetail.getDepartureTimeStart());
		dto.setDepatureDateTimeEnd(tvlDetail.getDepartureTimeEnd());
		dto.setSelectedDepatureDateTimeStart(tvlDetail.getDepartureTimeStart());
		dto.setSelectedDepatureDateTimeEnd(tvlDetail.getDepartureTimeEnd());
		dto.setFromAirport(tvlDetail.getFrom());
		dto.setToAirport(tvlDetail.getTo());

		dto.setAdultCount(tvlDetail.getAdultCount());
		dto.setChildCount(tvlDetail.getChildCount());
		dto.setInfantCount(tvlDetail.getInfantCount());

		dto.setBookingClassCode(tvlDetail.getBookingClass());
		dto.getCabinClassSelection().put(tvlDetail.getCabinClass(), null);
		dto.setPosAirport(tvlDetail.getPosAirport());

		return dto;
	}

}
