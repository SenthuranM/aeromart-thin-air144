package com.isa.thinair.gdsservices.api.dto.internal;

import java.util.Collection;

public class SpecialServiceDetailRequest extends SpecialServiceRequest {

	private static final long serialVersionUID = 7304519956499066306L;

	private Collection<Passenger> passengers;

	private Segment segment;

	/**
	 * @return the passengers
	 */
	public Collection<Passenger> getPassengers() {
		return passengers;
	}

	/**
	 * @param passengers
	 *            the passengers to set
	 */
	public void setPassengers(Collection<Passenger> passengers) {
		this.passengers = passengers;
	}

	/**
	 * @return the segment
	 */
	public Segment getSegment() {
		return segment;
	}

	/**
	 * @param segment
	 *            the segment to set
	 */
	public void setSegment(Segment segment) {
		this.segment = segment;
	}

}
