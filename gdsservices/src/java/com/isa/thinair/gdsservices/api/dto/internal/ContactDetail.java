package com.isa.thinair.gdsservices.api.dto.internal;

import com.isa.thinair.gdsservices.api.util.GDSApiUtils;

public class ContactDetail extends GDSBase {

	private static final long serialVersionUID = 6615388555246704569L;

	private String title;
	private String firstName;
	private String lastName;
	private String streetAddress;
	private String city;
	private String country;
	private String landPhoneNumber;
	private String mobilePhoneNumber;
	private String faxNumber;
	private String emailAddress;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress
	 *            the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the faxNumber
	 */
	public String getFaxNumber() {
		return faxNumber;
	}

	/**
	 * @param faxNumber
	 *            the faxNumber to set
	 */
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the landPhoneNumber
	 */
	public String getLandPhoneNumber() {
		return landPhoneNumber;
	}

	/**
	 * @param landPhoneNumber
	 *            the landPhoneNumber to set
	 */
	public void setLandPhoneNumber(String landPhoneNumber) {
		this.landPhoneNumber = landPhoneNumber;
	}

	/**
	 * @return the mobilePhoneNumber
	 */
	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}

	/**
	 * @param mobilePhoneNumber
	 *            the mobilePhoneNumber to set
	 */
	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}

	/**
	 * @return the streetAddress
	 */
	public String getStreetAddress() {
		return streetAddress;
	}

	/**
	 * @param streetAddress
	 *            the streetAddress to set
	 */
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isEmpty() {
		if (GDSApiUtils.maskNull(getLandPhoneNumber()).length() == 0 && GDSApiUtils.maskNull(getTitle()).length() == 0
				&& GDSApiUtils.maskNull(getFirstName()).length() == 0 && GDSApiUtils.maskNull(getLastName()).length() == 0
				&& GDSApiUtils.maskNull(getStreetAddress()).length() == 0 && GDSApiUtils.maskNull(getCity()).length() == 0
				&& GDSApiUtils.maskNull(getCountry()).length() == 0 && GDSApiUtils.maskNull(getMobilePhoneNumber()).length() == 0
				&& GDSApiUtils.maskNull(getFaxNumber()).length() == 0 && GDSApiUtils.maskNull(getEmailAddress()).length() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
