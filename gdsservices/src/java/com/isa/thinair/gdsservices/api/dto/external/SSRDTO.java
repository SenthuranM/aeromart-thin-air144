package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;

/**
 * @author sanjeewaf
 */
public class SSRDTO implements Serializable {

	private static final long serialVersionUID = 8561486118788210412L;

	private String codeSSR;

	private String carrierCode;

	private String ssrValue;

	private String fullElement;

	private String actionOrStatusCode;

	private String adviceOrStatusCode;

	private String freeText;

	/**
	 * @return the adviceOrStatusCode
	 */
	public String getAdviceOrStatusCode() {
		return adviceOrStatusCode;
	}

	/**
	 * @param adviceOrStatusCode
	 *            the adviceOrStatusCode to set
	 */
	public void setAdviceOrStatusCode(String adviceOrStatusCode) {
		this.adviceOrStatusCode = adviceOrStatusCode;
	}

	/**
	 * @return the actionOrStatusCode
	 */
	public String getActionOrStatusCode() {
		return actionOrStatusCode;
	}

	/**
	 * @param actionOrStatusCode
	 *            the actionOrStatusCode to set
	 */
	public void setActionOrStatusCode(String actionCode) {
		this.actionOrStatusCode = actionCode;
	}

	/**
	 * @return the ssrValue
	 */
	public String getSsrValue() {
		return ssrValue;
	}

	/**
	 * @param ssrValue
	 *            the ssrValue to set
	 */
	public void setSsrValue(String ssrValue) {
		this.ssrValue = ssrValue;
	}

	/**
	 * @return the carrierCode
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            the carrierCode to set
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the codeSSR
	 */
	public String getCodeSSR() {
		return codeSSR;
	}

	/**
	 * @param codeSSR
	 *            the codeSSR to set
	 */
	public void setCodeSSR(String codeSSR) {
		this.codeSSR = codeSSR;
	}

	/**
	 * @return the fullElement
	 */
	public String getFullElement() {
		return fullElement;
	}

	/**
	 * @param fullElement
	 *            the fullElement to set
	 */
	public void setFullElement(String fullElement) {
		this.fullElement = fullElement;
	}

	/**
	 * @return the freeText
	 */
	public String getFreeText() {
		return freeText;
	}

	/**
	 * @param freeText
	 *            the freeText to set
	 */
	public void setFreeText(String freeText) {
		this.freeText = freeText;
	}
}
