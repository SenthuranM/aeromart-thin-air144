package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.MessageType;

public class SSMASMMessegeDTO implements Serializable {

	private static final long serialVersionUID = 1L;


	private boolean withdrawal = false;

	private String flightDesignator;
	private Date startDate;
	private Date endDate;

	private String serviceType;
	private String aircraftType;
	private String passengerResBookingDesignator;

	private String senderAddress = null;
	private String recipientAddress = null;
	private String timestamp = null;

	private List<SSMASMFlightLegDTO> legs;

	private boolean codeShareCarrier;

	private String timeMode;

	private String operatingCarrier;

	private String messageSequenceReference;

	private String creatorReference;

	private MessageType messageType;

	private String externalFlightNo;

	private boolean validRequest = true;

	private String errorMessage;
	
	private Date newStartDate;
	private Date newEndDate;
	
	private boolean ownSchedule;

	private Collection<SSMASMSUBMessegeDTO> ssmASMSUBMessegeList;

	private String supplementaryInfo;

	private boolean updateOutResponseStatus;

	private String processingUserID;

	private Integer inMessegeID;
	
	private String rawMessage;
	
	private boolean enableAutoScheduleSplit;

	public boolean isWithdrawal() {
		return withdrawal;
	}

	public void setWithdrawal(boolean withdrawal) {
		this.withdrawal = withdrawal;
	}

	/** ------------------ Flight Information ------------ */
	public String getFlightDesignator() {
		return flightDesignator;
	}

	public void setFlightDesignator(String flightDesignator) {
		this.flightDesignator = flightDesignator;
	}

	/** ------------------ Period and Frequency -------- */
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/** ------------------ Equipment Information ------- */
	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getAircraftType() {
		return aircraftType;
	}

	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}

	public String getPassengerResBookingDesignator() {
		return passengerResBookingDesignator;
	}

	public void setPassengerResBookingDesignator(String passengerResBookingDesignator) {
		this.passengerResBookingDesignator = passengerResBookingDesignator;
	}

	/** ------------------ Legs Information ------- */
	public Collection<SSMASMFlightLegDTO> getLegs() {
		return legs;
	}

	public void setLegs(List<SSMASMFlightLegDTO> legs) {
		this.legs = legs;
	}

	public void addLeg(SSMASMFlightLegDTO ssiFlightLeg) {
		if (this.legs == null)
			this.legs = new ArrayList<SSMASMFlightLegDTO>();
		this.legs.add(ssiFlightLeg);
	}

	/**
	 * @return the senderAddress
	 */
	public String getSenderAddress() {
		return senderAddress;
	}

	/**
	 * @param senderAddress
	 *            the senderAddress to set
	 */
	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}

	/**
	 * @return the recipientAddress
	 */
	public String getRecipientAddress() {
		return recipientAddress;
	}

	/**
	 * @param recipientAddress
	 *            the recipientAddress to set
	 */
	public void setRecipientAddress(String recipientAddress) {
		this.recipientAddress = recipientAddress;
	}

	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp
	 *            the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public boolean isCodeShareCarrier() {
		return codeShareCarrier;
	}

	public void setCodeShareCarrier(boolean codeShareCarrier) {
		this.codeShareCarrier = codeShareCarrier;
	}

	public String getTimeMode() {
		return timeMode;
	}

	public void setTimeMode(String timeMode) {
		this.timeMode = timeMode;
	}

	public String getOperatingCarrier() {
		return operatingCarrier;
	}

	public void setOperatingCarrier(String operatingCarrier) {
		this.operatingCarrier = operatingCarrier;
	}

	public String getMessageSequenceReference() {
		return messageSequenceReference;
	}

	public void setMessageSequenceReference(String messageSequenceReference) {
		this.messageSequenceReference = messageSequenceReference;
	}

	public String getCreatorReference() {
		return creatorReference;
	}

	public void setCreatorReference(String creatorReference) {
		this.creatorReference = creatorReference;
	}

	public MessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}

	public String getExternalFlightNo() {
		return externalFlightNo;
	}

	public void setExternalFlightNo(String externalFlightNo) {
		this.externalFlightNo = externalFlightNo;
	}

	public boolean isValidRequest() {
		return validRequest;
	}

	public void setValidRequest(boolean validRequest) {
		this.validRequest = validRequest;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Date getNewStartDate() {
		return newStartDate;
	}

	public void setNewStartDate(Date newStartDate) {
		this.newStartDate = newStartDate;
	}

	public Date getNewEndDate() {
		return newEndDate;
	}

	public void setNewEndDate(Date newEndDate) {
		this.newEndDate = newEndDate;
	}

	public boolean isOwnSchedule() {
		return ownSchedule;
	}

	public void setOwnSchedule(boolean ownSchedule) {
		this.ownSchedule = ownSchedule;
	}

	public Collection<SSMASMSUBMessegeDTO> getSsmASMSUBMessegeList() {
		return ssmASMSUBMessegeList;
	}

	public void setSsmASMSUBMessegeList(Collection<SSMASMSUBMessegeDTO> ssmASMSUBMessegeList) {
		this.ssmASMSUBMessegeList = ssmASMSUBMessegeList;
	}

	public void addSsmASMSUBMessegeList(SSMASMSUBMessegeDTO ssmASMSUBMessegeDTO) {
		if (this.ssmASMSUBMessegeList == null)
			this.ssmASMSUBMessegeList = new ArrayList<SSMASMSUBMessegeDTO>();
		this.ssmASMSUBMessegeList.add(ssmASMSUBMessegeDTO);
	}

	public String getSupplementaryInfo() {
		return supplementaryInfo;
	}

	public void setSupplementaryInfo(String supplementaryInfo) {
		this.supplementaryInfo = supplementaryInfo;
	}

	public boolean isUpdateOutResponseStatus() {
		return updateOutResponseStatus;
	}

	public void setUpdateOutResponseStatus(boolean updateOutResponseStatus) {
		this.updateOutResponseStatus = updateOutResponseStatus;
	}

	public String getProcessingUserID() {
		return processingUserID;
	}

	public void setProcessingUserID(String processingUserID) {
		this.processingUserID = processingUserID;
	}

	public Integer getInMessegeID() {
		return inMessegeID;
	}

	public void setInMessegeID(Integer inMessegeID) {
		this.inMessegeID = inMessegeID;
	}

	public String getRawMessage() {
		return rawMessage;
	}

	public void setRawMessage(String rawMessage) {
		this.rawMessage = rawMessage;
	}

	public boolean isEnableAutoScheduleSplit() {
		return enableAutoScheduleSplit;
	}

	public void setEnableAutoScheduleSplit(boolean enableAutoScheduleSplit) {
		this.enableAutoScheduleSplit = enableAutoScheduleSplit;
	}
	
}
