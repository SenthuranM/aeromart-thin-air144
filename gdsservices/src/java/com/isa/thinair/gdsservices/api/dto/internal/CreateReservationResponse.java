package com.isa.thinair.gdsservices.api.dto.internal;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

/**
 * @author Manoj Dhanushka
 */
public class CreateReservationResponse extends GDSReservationRequestBase{

	private static final long serialVersionUID = 1L;

	@Override
	public ReservationAction getActionType() {
		return GDSInternalCodes.ReservationAction.CREATE_BOOKING_RESP;
	}

}
