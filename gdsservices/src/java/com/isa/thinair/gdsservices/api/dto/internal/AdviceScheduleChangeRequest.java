package com.isa.thinair.gdsservices.api.dto.internal;

import java.util.List;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

public class AdviceScheduleChangeRequest extends GDSReservationRequestBase {
	
	private static final long serialVersionUID = 1L;
	
	private List<Segment> sourceSegments;
	
	private List<Segment> targetSegments;	

	@Override
	public ReservationAction getActionType() {
		return GDSInternalCodes.ReservationAction.TRANSFER_SEGMENT;
	}

	public List<Segment> getSourceSegments() {
		return sourceSegments;
	}

	public void setSourceSegments(List<Segment> sourceSegments) {
		this.sourceSegments = sourceSegments;
	}

	public List<Segment> getTargetSegments() {
		return targetSegments;
	}

	public void setTargetSegments(List<Segment> targetSegments) {
		this.targetSegments = targetSegments;
	}	
}
