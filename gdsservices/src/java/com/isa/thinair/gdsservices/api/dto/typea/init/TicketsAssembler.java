package com.isa.thinair.gdsservices.api.dto.typea.init;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.airproxy.api.dto.GDSChargeAdjustmentRQ;

public class TicketsAssembler {
	public enum Mode {
		RECONCILE, PAYMENT, REFUND
	}

	public static class PassengerAssembler {

		private int pnrPaxId;
		private String currencyCode;
		private BigDecimal totalAmount;
		private BigDecimal balance;
		private String note;

		public int getPnrPaxId() {
			return pnrPaxId;
		}

		public void setPnrPaxId(int pnrPaxId) {
			this.pnrPaxId = pnrPaxId;
		}

		public String getCurrencyCode() {
			return currencyCode;
		}

		public void setCurrencyCode(String currencyCode) {
			this.currencyCode = currencyCode;
		}

		public BigDecimal getTotalAmount() {
			return totalAmount;
		}

		public void setTotalAmount(BigDecimal totalAmount) {
			this.totalAmount = totalAmount;
		}

		public BigDecimal getBalance() {
			return balance;
		}

		public void setBalance(BigDecimal balance) {
			this.balance = balance;
		}

		public String getNote() {
			return note;
		}

		public void setNote(String note) {
			this.note = note;
		}
	}



	private String pnr;
	private Mode mode;
	private List<PassengerAssembler> passengerAssemblers;
	private GDSChargeAdjustmentRQ gdsChargeAdjustmentRQ;
	private String ipAddress;

	public TicketsAssembler(Mode mode) {
		this.mode = mode;
		this.passengerAssemblers = new ArrayList<PassengerAssembler>();
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public List<PassengerAssembler> getPassengerAssemblers() {
		return passengerAssemblers;
	}

	public void addPassengerAssembler(PassengerAssembler passengerAssembler) {
		this.passengerAssemblers.add(passengerAssembler);
	}

	public Mode getMode() {
		return mode;
	}
	
	public void changeMode(Mode mode) {
		if (mode != null) {
			this.mode = mode;
		}
	}

	public GDSChargeAdjustmentRQ getGdsChargeAdjustmentRQ() {
		return gdsChargeAdjustmentRQ;
	}

	public void setGdsChargeAdjustmentRQ(GDSChargeAdjustmentRQ gdsChargeAdjustmentRQ) {
		this.gdsChargeAdjustmentRQ = gdsChargeAdjustmentRQ;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
}
