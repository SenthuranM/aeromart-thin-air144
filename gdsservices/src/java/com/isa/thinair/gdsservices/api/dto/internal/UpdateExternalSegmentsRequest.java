package com.isa.thinair.gdsservices.api.dto.internal;

import java.util.Collection;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

public class UpdateExternalSegmentsRequest extends GDSReservationRequestBase {
	private static final long serialVersionUID = -7131356122852596295L;

	private Collection<Segment> externalSegments;

	/**
	 * @return the externalSegments
	 */
	public Collection<Segment> getExternalSegments() {
		return externalSegments;
	}

	/**
	 * @param externalSegments
	 *            the externalSegments to set
	 */
	public void setExternalSegments(Collection<Segment> externalSegments) {
		this.externalSegments = externalSegments;
	}

	@Override
	public ReservationAction getActionType() {
		return GDSInternalCodes.ReservationAction.UPDATE_EXTERNAL_SEGMENTS;
	}
}
