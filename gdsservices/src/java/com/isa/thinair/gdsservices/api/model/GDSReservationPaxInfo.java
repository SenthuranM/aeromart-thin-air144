package com.isa.thinair.gdsservices.api.model;

import java.io.Serializable;

/**
 * @hibernate.class table = "T_GDS_PNR_PAX_INFO"
 */
public class GDSReservationPaxInfo implements Serializable {
	private Long gdsReservationPaxInfoId;
	private Integer pnrPaxId;
	private String segmentType;
	private String segmentContent;

	/**
	 * @hibernate.id column = "GDS_PNR_PAX_INFO_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_GDS_PNR_PAX_INFO"
	 */
	public Long getGdsReservationPaxInfoId() {
		return gdsReservationPaxInfoId;
	}

	public void setGdsReservationPaxInfoId(Long gdsReservationPaxInfoId) {
		this.gdsReservationPaxInfoId = gdsReservationPaxInfoId;
	}

	/**
	 * @hibernate.property column = "PNR_PAX_ID"
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}

	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	/**
	 * @hibernate.property column = "SEGMENT_TYPE"
	 */
	public String getSegmentType() {
		return segmentType;
	}

	public void setSegmentType(String segmentType) {
		this.segmentType = segmentType;
	}

	/**
	 * @hibernate.property column = "SEGMENT_CONTENT"
	 */
	public String getSegmentContent() {
		return segmentContent;
	}

	public void setSegmentContent(String segmentContent) {
		this.segmentContent = segmentContent;
	}
}
