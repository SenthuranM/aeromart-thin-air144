package com.isa.thinair.gdsservices.api.util;

public interface TypeAConstants {

    String DELIM_INITIATOR_RESPONDER_KEY = "/";
	String NEW_LINE_DESIGNATOR_TYPE_B = "..";
	int SEQUENCE_NUMBER_FIXED_LENGTH = 4;
	int NULL_INT = -1;
	String DELIM_VERSION_NUMBERS = ".";

	String TYPE_A_CONTROLLING_AGENCY = "1A";

	public enum PaxTitle {
		MR,
		MS,
		DR,
		JR,
		CHD,
		MRS,
		REV,
		MSTR,
		MISS
	}

	enum MessageFlow {
		INITIATE, RESPOND
	}

	interface MessageSessionKey {
		String BLOCKED_SEATS = "BLOCKED_SEATS";
		String MSG_CONTEXT_STATE = "CONTEXT_STATE";
	}

	interface BusinessFunction {
		String AIR_PROVIDER = "1";
	}

	/*
	IATA Message Standard Doc 1225
	*/
	interface MessageFunction {
		String CONFIRMATION = "3";
		String CREATE = "56";
		String INVENTORY_ADJUSTMENT = "60";
		String CANCEL = "79";
		String UNSOLICITED_AIRPORT_CONTROL = "107";
		String ISSUE = "130";
		String DISPLAY = "131";
		String PRINT = "132";
		String VOID = "133";
		String EXCHANGE = "134";
		String REFUND = "135";
		String HISTORY = "137";
		String REVALIDATE = "139";
		String CHANGE_OF_STATUS = "142";
		String ELECTRONIC_IMAGE_PAPER_TICKET = "710";
		String AIRPORT_CONTROL_UNSOLICITED = "733";
		String AIRPORT_CONTROL_GROUND_HANDLER = "734";
		String AIRPORT_CONTROL = "751";
		String REFUND_CANCEL = "775";
		String VOID_EXCHANGE = "776";
	}

	interface SecondaryMsgFunction {
		String PAPER_TO_ELECTRONIC = "134";
		String ELECTRONIC_TO_PAPER = "710";
	}

	interface ActionCode {
		String REPLY_REQUIRED ="NN";
		String CANCEL = "XX";
	}

	interface StatusCode {
		String HOLDS_CONFIRMED = "HK";
		String UNABLE = "UN";
		String CANCELLED = "HX";
	}

	/*
	 IATA Message Standard Doc 7365
	 */
	interface ProcessingIndicator {
		String ACTION_REQUIRED = "P";
	}

	/*
	IATA Message Standard Doc 1050
	*/
	interface SequenceNumber {
		String E_TICKET_CANDIDATE = "ET";
	}

	interface AssociationCode {
		String ESTABLISH = "S";
		String RETRY = "R";
		String CANCEL = "C";
		String ESTABLISHED = "E";
		String INACTIVE = "I";
		String TERMINATE = "T";
		String DUPLICATE = "D";
		String ONE_OFF = "O";
	}

	interface ErrorActionCode {
		String CURRENT_AND_ALL_LOWER_REJ = "4";
		String CURRENT_AND_NEXT_LOWER_ACCEPT = "7";
		String INTERCHANGE_RECEIVED = "8";

	}

	interface ErrorSyntaxCode {
		String SYNTAX_VERSION_NOT_SUPPORT = "2";
		String RECIPIENT_REF_INVALID_MISSING = "11";
		String VALUE_INVALID = "12";
		String MANDATORY_SEG_MISSING = "13";
		String VALUE_POSITION_NOT_SUPPORT = "14";
		String SEGMENT_POSITION_NOT_SUPPORT = "15";
		String UNSPECIFIED_ERROR = "18";
		String SENDER_UNKNOWN = "23";
		String ASSOCIATION_STALE = "24";
		String ASSOCIATION_DUPLICATE = "26";
		String REF_DO_NOT_MATCH = "28";
		String SEG_COUNT_DO_NOT_MATCH = "29";
		String FORMAT_INVALID = "37";
		String DATA_MAX_LENGTH_VIOLATE = "39";
		String DATA_MIN_LENGTH_VIOLATE = "40";
		String COMM_ERROR = "41";
		String TEMP_COMM_ERROR = "42";
		String RECIPIENT_UNKNOWN = "43";
	}

	/*
	IATA Message Standard Doc 4451
	*/
	interface TextSubjectQualifier  {
		String CODED_FREE_TEXT = "1";
		String LITERAL_TEXT = "3";
		String CODED_AND_LITERAL_TEXT = "4";
	}

	/*
	IATA Message Standard Doc 9321
	*/
	interface ApplicationErrorCode {
		String DATE_INVALID = "1";
		String OPERATIONAL_SUFFIX_INVALID = "6";
		String DEPARTURE_TIME_INVALID_MISSING = "103";
		String RES_BOOKING_DESIGNATOR_INVALID_MISSING = "104";
		String AIRLINE_DESIGNATOR_INVALID = "107";
		String FLIGHT_NUMBER_INVALID = "114";
		String NOT_AVAILABLE = "422";
		String UNABLE_TO_PROCESS = "118";
		String ACTION_CODE_INVALID = "120";
		String NUMBER_IN_PARTY_INVALID = "121";
		String NO_PNR_MATCH = "129";
		String UNEQUAL_NUMBER_IN_PARTY = "146";
		String NAME_MISMATCH = "153";
		String MESSAGE_FUNCTION_INVALID = "154";
		String MESSAGE_FUNCTION_NOT_SUPPORTED = "155";
		String PASSENGER_NAME_MISMATCH_WITH_PNR = "160";
		String SSR_NOT_AVAILABLE = "185";
		String SEGMENTS_SELLS_RESTRICTED = "292";
		String UNABLE_TO_SELL_TIME_LIMIT_REACHED = "293";
		String INVALID_FORMAT = "294";
		String INVALID_TICKET_NUMBER = "364";
		String NOT_AUTHORIZED = "368";
		String SIMULTANEOUS_CHANGES_TO_PNR = "370";
		String OPERATION_NOT_AUTHORIZED_FOR_PNR = "375";
		String RECORD_LOCATOR_REQUIRED = "381";
		String ALREADY_TICKETED = "395";
		String INVALID_TICKET_COUPON_STATUS = "396";
		String DUPLICATE_NAME = "399";
		String TICKET_NUMBER_NOT_FOUND = "401";
		String FLIGHT_NOT_ACTIVE = "413";
		String EXISTING_ITINERARY_INCOMPATIBLE ="465";
		String FLIGHT_SEGMENT_NOT_FOUND_IN_PNR = "467";
		String TOO_MUCH_DATA = "721";
		String UNABLE_TO_CONVERT_TO_TICKETING_CURRENCY = "73D";
		String FARE_CALCULATION_MISSING_INVALID = "794";
        String PRIOR_MESSAGE_BEING_PROCESSED = "902";
		String SYSTEM_ERROR = "118";  // TODO -- Change this to 911
	}

	interface ResponseType {
		String PROCESSED_SUCCESSFULLY = "3";
		String RECEIVED_NOT_PROCESSED = "7";
		String RECEIVED_REJECTED = "8";
	}

	/*
	IATA Message Standard Doc 9888
	*/
	interface FormOfPayment {
		// TODO -----------------
	}

	/*
	IATA Message Standard Doc 5025
	*/
	interface MonetaryType {
		String BASE_FARE = "B";
		String TICKET_TOTAL = "T";
		String BULK_TICKET_TOTAL = "I";
		String EQUIVALENT_FARE = "E";
		String NET_FARE = "H";
		String ADDITIONAL_COLLECTION_TOTAL = "C";
	}

	/*
	IATA Message Standard Doc 1001
	*/
	interface DocumentType {
		String TICKET = "T";
	}

	/*
	IATA Message Standard Doc 1159
	*/
	interface CouponMedia {
		String PAPER_TICKET = "P";
		String E_TICKET = "E";
	}

	interface CouponStatus {
		String NULL = null;

		String ORIGINAL_ISSUE = "I";
		String EXCHANGED = "E";
		String VOIDED = "V";
		String REFUNDED = "RF";
		String PRINTED = "PR";
		String FLOWN = "B";
		String AIRPORT_CONTROL = "AL";
		String CHECKED_IN = "CK";
		String BOARDED = "BD";
		String SUSPENDED = "S";
		String TICKETED = "T";
		String REVOKED = "Q";
		String IRREGULAR_OPERATIONS = "IO";
		String PRINT_EXCHANGED = "PE";
		String CLOSED = "Z";
		String NOTIFICATION = "N";

		String NOTIFIED = "701";
//		String OPEN = "OPE";
	}

	interface NumberOfUnitsQualifier {
		String TICKETS_COUNT = "TD";
		String COUPONS_COUNT = "TF";
	}
	
	interface TravellerQualifier {
		String ADULT = "A";
		String CHILD = "C";
		String INFANT = "IN";		
	}
	
    public interface DateTimePeriodQualifier {
        String NOT_VALID_BEFORE = "B";
        String NOT_VALID_AFTER = "A";
        String TRANSACTION = "T";
    }

	interface TicketDataIndicator {
		String OLD = "2";
		String NEW = "3";
	}

	interface ReservationControlType {
		String SYSTEM_REFERENCE = "1";
	}

	/*
	IATA Message Standard Doc 9980
	*/
	interface InformationType {
		String TELEPHONE_NATURE_UNKNOWN = "5";
		String FARE_CALCULATION = "15";
		String ISSUING_AGENCY = "39";
	}

	interface TypeAKeywords {
		String FREE = "FREE";
		String NO_ADDITIONAL_COLLECTION = "NO ADC";
		String ADDITION_COLLECTION_POSTFIX = "A";

		String PAID_DUTY_TAX_FEE = "PD";
	}

	interface ElementMaxLength {
		int INTERACTIVE_FREE_TEXT = 70;
	}

	interface DutyTaxFeeCategory {
		String TO_BE_COLLECTED = "700";
		String PAID = "701";
	}
	
	interface RequestIdentitySystem {
		String ETS = "ETS";
		String TYPEA_MSG_ADAPTOR = "TYPEA_MSG_ADAPTOR";
	}
	
	interface FlightSegmentActionCode {
		String OK = "OK";
	}

	enum MessageElement {
		ROOT,
		ODI,
		TVL
	}
}
