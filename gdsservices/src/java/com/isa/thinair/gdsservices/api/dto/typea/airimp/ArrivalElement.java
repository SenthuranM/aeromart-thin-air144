package com.isa.thinair.gdsservices.api.dto.typea.airimp;

import java.io.Serializable;

public class ArrivalElement implements Serializable {

	private static final long serialVersionUID = 1L;

	private String airline;
	private String flightNum;
	private String rbd;
	private String day;
	private String month;
	private String depAirport;
	private String arrAirport;
	private String action;
	private String numOfSeats;
	private String depTime24;
	private String arrTime24;

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}

	public String getFlightNum() {
		return flightNum;
	}

	public void setFlightNum(String flightNum) {
		this.flightNum = flightNum;
	}

	public String getRbd() {
		return rbd;
	}

	public void setRbd(String rbd) {
		this.rbd = rbd;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getDepAirport() {
		return depAirport;
	}

	public void setDepAirport(String depAirport) {
		this.depAirport = depAirport;
	}

	public String getArrAirport() {
		return arrAirport;
	}

	public void setArrAirport(String arrAirport) {
		this.arrAirport = arrAirport;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getNumOfSeats() {
		return numOfSeats;
	}

	public void setNumOfSeats(String numOfSeats) {
		this.numOfSeats = numOfSeats;
	}

	public String getDepTime24() {
		return depTime24;
	}

	public void setDepTime24(String depTime24) {
		this.depTime24 = depTime24;
	}

	public String getArrTime24() {
		return arrTime24;
	}

	public void setArrTime24(String arrTime24) {
		this.arrTime24 = arrTime24;
	}
}