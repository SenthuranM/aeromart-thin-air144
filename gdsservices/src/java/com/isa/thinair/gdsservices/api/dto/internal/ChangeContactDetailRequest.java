package com.isa.thinair.gdsservices.api.dto.internal;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

public class ChangeContactDetailRequest extends GDSReservationRequestBase {
	private static final long serialVersionUID = 5388127529290243632L;

	private ContactDetail contactDetail;

	/**
	 * @return the contactDetail
	 */
	public ContactDetail getContactDetail() {
		return contactDetail;
	}

	/**
	 * @param contactDetail
	 *            the contactDetail to set
	 */
	public void setContactDetail(ContactDetail contactDetail) {
		this.contactDetail = contactDetail;
	}

	@Override
	public ReservationAction getActionType() {
		return GDSInternalCodes.ReservationAction.CHANGE_CONTACT_DETAILS;
	}
}
