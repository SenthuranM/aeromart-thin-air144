package com.isa.thinair.gdsservices.api.dto.typea.common;

import java.io.Serializable;

public class Coupon implements Serializable {
	private String ticketNumber;
	private String externalTicketNumber;
	private int couponNumber;
	private int externalCouponNumber;

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getExternalTicketNumber() {
		return externalTicketNumber;
	}

	public void setExternalTicketNumber(String externalTicketNumber) {
		this.externalTicketNumber = externalTicketNumber;
	}

	public int getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(int couponNumber) {
		this.couponNumber = couponNumber;
	}

	public int getExternalCouponNumber() {
		return externalCouponNumber;
	}

	public void setExternalCouponNumber(int externalCouponNumber) {
		this.externalCouponNumber = externalCouponNumber;
	}
}
