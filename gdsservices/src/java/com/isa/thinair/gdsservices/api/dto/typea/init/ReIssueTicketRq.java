package com.isa.thinair.gdsservices.api.dto.typea.init;

public class ReIssueTicketRq extends InitBaseRq {
	private String pnr;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
}
