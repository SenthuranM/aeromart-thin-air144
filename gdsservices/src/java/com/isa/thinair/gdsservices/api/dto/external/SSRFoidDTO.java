package com.isa.thinair.gdsservices.api.dto.external;

import java.util.List;

/**
 * DTO class will hold SSR FOID information
 * 
 * @author sanjeewaf
 */
public class SSRFoidDTO extends SSRDTO {

	private static final long serialVersionUID = 7233022615637023378L;

	/**
	 * holds passenger identification type list of possible id types are CC - Credit Card. FF - Frequent Flyer Number.
	 * PP - country code and passport number. DL - Drivers License NI - National Identity TN - Ticket Number ID -
	 * Locally Defined ID Number CN - Confirmation Number or Record Locator
	 */
	private String passengerIdentificationType;

	/** Relevant document number */
	private String passengerIdentificationNo;

	/** holds passenger identifications */
	private List<NameDTO> pnrNameDTOs;

	/**
	 * @return the passengerIdentificationNo
	 */
	public String getPassengerIdentificationNo() {
		return passengerIdentificationNo;
	}

	/**
	 * @param passengerIdentificationNo
	 *            the passengerIdentificationNo to set
	 */
	public void setPassengerIdentificationNo(String passengerIdentificationNo) {
		this.passengerIdentificationNo = passengerIdentificationNo;
	}

	/**
	 * @return the passengerIdentificationType
	 */
	public String getPassengerIdentificationType() {
		return passengerIdentificationType;
	}

	/**
	 * @param passengerIdentificationType
	 *            the passengerIdentificationType to set
	 */
	public void setPassengerIdentificationType(String passengerIdentificationType) {
		this.passengerIdentificationType = passengerIdentificationType;
	}

	/**
	 * @return the pnrNameDTOs
	 */
	public List<NameDTO> getPnrNameDTOs() {
		return pnrNameDTOs;
	}

	/**
	 * @param pnrNameDTOs
	 *            the pnrNameDTOs to set
	 */
	public void setPnrNameDTOs(List<NameDTO> pnrNameDTOs) {
		this.pnrNameDTOs = pnrNameDTOs;
	}
}
