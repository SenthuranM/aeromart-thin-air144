/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of GDS Messages
 * 
 * @author Nilindra Fernando
 * @since 1.0
 * @hibernate.class table = "T_GDS_MESSAGE"
 */
public class GDSMessage extends Persistent implements Serializable {

	private static final long serialVersionUID = 450344520651285733L;

	/** Holds the message id */
	private Long messageId;

	/** Holds the message type */
	private String messageType;

	/** Holds the unique accelaero object instance id */
	private String uniqueAccelAeroRequestId;

	/** Holds the message received date */
	private Date messageReceivedDate;

	/** Holds the communication reference */
	private String communicationReference;

	/** Holds the originator address */
	private String originatorAddress;

	/** Holds the responder address */
	private String responderAddress;

	/** Holds the GDS */
	private String gds;

	/** Holds the request XML */
	private String requestXML;

	/** Holds the response XML */
	private String responseXML;

	/** Holds the process status */
	private String processStatus;

	/** Holds the process status history */
	private String processStatusHistory;

	/** Holds the process description */
	private String processDescription;

	/** Holds the GDS PNR Number */
	private String gdsPnr;

	/** Holds the GDS Agent Code */
	private String gdsAgentCode;

	/** Holds the GDS User Id */
	private String gdsUserId;

	/** Holds the Message Broker Reference Id */
	private String messageBrokerReferenceId;

	/**
	 * @return Returns the communicationReference.
	 * @hibernate.property column = "COMMUNICATION_REFERENCE"
	 */
	public String getCommunicationReference() {
		return communicationReference;
	}

	/**
	 * @param communicationReference
	 *            The communicationReference to set.
	 */
	public void setCommunicationReference(String communicationReference) {
		this.communicationReference = communicationReference;
	}

	/**
	 * @return Returns the messageId.
	 * @hibernate.id column = "MSG_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_GDS_MESSAGE"
	 */
	public Long getMessageId() {
		return messageId;
	}

	/**
	 * @param messageId
	 *            The messageId to set.
	 */
	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	/**
	 * @return Returns the messageReceivedDate.
	 * @hibernate.property column = "MSG_RECEIVED_DATE"
	 */
	public Date getMessageReceivedDate() {
		return messageReceivedDate;
	}

	/**
	 * @param messageReceivedDate
	 *            The messageReceivedDate to set.
	 */
	public void setMessageReceivedDate(Date messageReceivedDate) {
		this.messageReceivedDate = messageReceivedDate;
	}

	/**
	 * @return Returns the messageType.
	 * @hibernate.property column = "MSG_TYPE"
	 */
	public String getMessageType() {
		return messageType;
	}

	/**
	 * @param messageType
	 *            The messageType to set.
	 */
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	/**
	 * @return Returns the originatorAddress.
	 * @hibernate.property column = "ORIGINATOR_ADDRESS"
	 */
	public String getOriginatorAddress() {
		return originatorAddress;
	}

	/**
	 * @param originatorAddress
	 *            The originatorAddress to set.
	 */
	public void setOriginatorAddress(String originatorAddress) {
		this.originatorAddress = originatorAddress;
	}

	/**
	 * @return Returns the processStatus.
	 * @hibernate.property column = "PROCESS_STATUS"
	 */
	public String getProcessStatus() {
		return processStatus;
	}

	/**
	 * @param processStatus
	 *            The processStatus to set.
	 */
	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}

	/**
	 * @return Returns the requestXML.
	 * @hibernate.property column = "REQUEST_XML"
	 */
	public String getRequestXML() {
		return requestXML;
	}

	/**
	 * @param requestXML
	 *            The requestXML to set.
	 */
	public void setRequestXML(String requestXML) {
		this.requestXML = requestXML;
	}

	/**
	 * @return Returns the responderAddress.
	 * @hibernate.property column = "RESPONDER_ADDRESS"
	 */
	public String getResponderAddress() {
		return responderAddress;
	}

	/**
	 * @param responderAddress
	 *            The responderAddress to set.
	 */
	public void setResponderAddress(String responderAddress) {
		this.responderAddress = responderAddress;
	}

	/**
	 * @return Returns the responseXML.
	 * @hibernate.property column = "RESPONSE_XML"
	 */
	public String getResponseXML() {
		return responseXML;
	}

	/**
	 * @param responseXML
	 *            The responseXML to set.
	 */
	public void setResponseXML(String responseXML) {
		this.responseXML = responseXML;
	}

	/**
	 * @return Returns the processStatusHistory.
	 * @hibernate.property column = "STATUS_HISTORY"
	 */
	public String getProcessStatusHistory() {
		return processStatusHistory;
	}

	/**
	 * @param processStatusHistory
	 *            The processStatusHistory to set.
	 */
	public void setProcessStatusHistory(String processStatusHistory) {
		this.processStatusHistory = processStatusHistory;
	}

	/**
	 * @return Returns the uniqueAccelAeroRequestId.
	 * @hibernate.property column = "AA_OBJ_ID"
	 */
	public String getUniqueAccelAeroRequestId() {
		return uniqueAccelAeroRequestId;
	}

	/**
	 * @param uniqueAccelAeroRequestId
	 *            The uniqueAccelAeroRequestId to set.
	 */
	public void setUniqueAccelAeroRequestId(String uniqueAccelAeroRequestId) {
		this.uniqueAccelAeroRequestId = uniqueAccelAeroRequestId;
	}

	/**
	 * @return Returns the processDescription.
	 * @hibernate.property column = "PROCESS_DESCRIPTION"
	 */
	public String getProcessDescription() {
		return processDescription;
	}

	/**
	 * @param processDescription
	 *            The processDescription to set.
	 */
	public void setProcessDescription(String processDescription) {
		this.processDescription = processDescription;
	}

	/**
	 * @return the gds
	 * @hibernate.property column = "GDS_NAME"
	 */
	public String getGds() {
		return gds;
	}

	/**
	 * @param gds
	 *            the gds to set
	 */
	public void setGds(String gds) {
		this.gds = gds;
	}

	/**
	 * @return the gdsPnr
	 * @hibernate.property column = "GDS_PNR"
	 */
	public String getGdsPnr() {
		return gdsPnr;
	}

	/**
	 * @param gdsPnr
	 *            the gdsPnr to set
	 */
	public void setGdsPnr(String gdsPnr) {
		this.gdsPnr = gdsPnr;
	}

	/**
	 * @return the gdsAgentCode
	 * @hibernate.property column = "GDS_AGENT_CODE"
	 */
	public String getGdsAgentCode() {
		return gdsAgentCode;
	}

	/**
	 * @param gdsAgentCode
	 *            the gdsAgentCode to set
	 */
	public void setGdsAgentCode(String gdsAgentCode) {
		this.gdsAgentCode = gdsAgentCode;
	}

	/**
	 * @return the gdsUserId
	 * @hibernate.property column = "GDS_USER_ID"
	 */
	public String getGdsUserId() {
		return gdsUserId;
	}

	/**
	 * @param gdsUserId
	 *            the gdsUserId to set
	 */
	public void setGdsUserId(String gdsUserId) {
		this.gdsUserId = gdsUserId;
	}

	/**
	 * @return the messageBrokerReferenceId
	 * @hibernate.property column = "MSG_BROKER_ID"
	 */
	public String getMessageBrokerReferenceId() {
		return messageBrokerReferenceId;
	}

	/**
	 * @param messageBrokerReferenceId
	 *            the messageBrokerReferenceId to set
	 */
	public void setMessageBrokerReferenceId(String messageBrokerReferenceId) {
		this.messageBrokerReferenceId = messageBrokerReferenceId;
	}
}
