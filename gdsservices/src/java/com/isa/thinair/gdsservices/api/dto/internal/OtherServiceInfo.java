package com.isa.thinair.gdsservices.api.dto.internal;

public class OtherServiceInfo extends GDSBase {
	private static final long serialVersionUID = -9179846662310351489L;

	private String code;

	private String text;

	private String fullElement;

	/**
	 * @return Returns the fullElement.
	 */
	public String getFullElement() {
		return fullElement;
	}

	/**
	 * @param fullElement
	 *            The fullElement to set.
	 */
	public void setFullElement(String fullElement) {
		this.fullElement = fullElement;
	}

	/**
	 * @return Returns the code.
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            The code to set.
	 */
	public void setCode(String osiCode) {
		this.code = osiCode;
	}

	/**
	 * @return Returns the text.
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text
	 *            The text to set.
	 */
	public void setText(String text) {
		this.text = text;
	}

}
