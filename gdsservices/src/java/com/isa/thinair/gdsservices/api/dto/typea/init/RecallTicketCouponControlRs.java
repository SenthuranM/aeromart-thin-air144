package com.isa.thinair.gdsservices.api.dto.typea.init;

import java.util.List;

public class RecallTicketCouponControlRs extends InitBaseRs {

	private List<Integer> grantedPpfsETicketIds;
	private List<Integer> rejectedPpfsEticketIds;

	public List<Integer> getGrantedPpfsETicketIds() {
		return grantedPpfsETicketIds;
	}

	public void setGrantedPpfsETicketIds(List<Integer> grantedPpfsETicketIds) {
		this.grantedPpfsETicketIds = grantedPpfsETicketIds;
	}

	public List<Integer> getRejectedPpfsEticketIds() {
		return rejectedPpfsEticketIds;
	}

	public void setRejectedPpfsEticketIds(List<Integer> rejectedPpfsEticketIds) {
		this.rejectedPpfsEticketIds = rejectedPpfsEticketIds;
	}
}
