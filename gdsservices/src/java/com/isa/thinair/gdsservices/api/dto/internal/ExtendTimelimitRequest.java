package com.isa.thinair.gdsservices.api.dto.internal;

import java.util.Date;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

/**
 * @author Manoj
 */
public class ExtendTimelimitRequest extends GDSReservationRequestBase {
	
	private static final long serialVersionUID = 1L;
	
	private Date timeLimit;
	
	@Override
	public ReservationAction getActionType() {
		return GDSInternalCodes.ReservationAction.EXTEND_TIMELIMIT;
	}

	public Date getTimeLimit() {
		return timeLimit;
	}

	public void setTimeLimit(Date timeLimit) {
		this.timeLimit = timeLimit;
	}

}
