package com.isa.thinair.gdsservices.api.model;

public class GdsEvent {

	public enum Request {
		CHANGE_OF_STATUS_TO_FINAL,
		RECALL_CONTROL,
		AIRPORT_CONTROL,
		TICKET_DISPLAY,
		TICKET_REISSUE
	}

}
