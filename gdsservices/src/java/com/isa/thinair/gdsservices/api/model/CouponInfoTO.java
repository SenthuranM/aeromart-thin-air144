package com.isa.thinair.gdsservices.api.model;

import java.io.Serializable;

/**
 * A holder for ETKT coupon information.
 * 
 * @author sanjaya
 */
public class CouponInfoTO implements Serializable {

	private static final long serialVersionUID = 7837200853915181996L;

	/** The segment sequence of the coupon */
	private int segmentSquence;

	/** The coupon status */
	private String couponStatus;

	/**
	 * @return the segmentSquence
	 */
	public int getSegmentSquence() {
		return segmentSquence;
	}

	/**
	 * @param segmentSquence
	 *            the segmentSquence to set
	 */
	public void setSegmentSquence(int segmentSquence) {
		this.segmentSquence = segmentSquence;
	}

	/**
	 * @return the couponStatus
	 */
	public String getCouponStatus() {
		return couponStatus;
	}

	/**
	 * @param couponStatus
	 *            the couponStatus to set
	 */
	public void setCouponStatus(String couponStatus) {
		this.couponStatus = couponStatus;
	}

}
