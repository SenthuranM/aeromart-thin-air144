package com.isa.thinair.gdsservices.api.dto.internal;

import java.util.Collection;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

public class SplitReservationRequest extends GDSReservationRequestBase {

	private static final long serialVersionUID = 9068613887579151519L;
	private Collection<Passenger> passengers;

	/**
	 * @return the passengers
	 */
	public Collection<Passenger> getPassengers() {
		return passengers;
	}

	/**
	 * @param passengers
	 *            the passengers to set
	 */
	public void setPassengers(Collection<Passenger> passengers) {
		this.passengers = passengers;
	}

	@Override
	public ReservationAction getActionType() {
		return GDSInternalCodes.ReservationAction.SPLIT_RESERVATION;
	}
}
