package com.isa.thinair.gdsservices.api.dto.internal;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

public class AddInfantRequest extends GDSReservationRequestBase {

	private static final long serialVersionUID = 8344987069574047503L;

	private Collection<Infant> infants;

	private PaymentDetail paymentDetail;

	public AddInfantRequest() {
		infants = new ArrayList<Infant>();
	}

	@Override
	public ReservationAction getActionType() {
		return GDSInternalCodes.ReservationAction.ADD_INFANT;
	}

	/**
	 * @return the paymentDetail
	 */
	public PaymentDetail getPaymentDetail() {
		return paymentDetail;
	}

	/**
	 * @param paymentDetail
	 *            the paymentDetail to set
	 */
	public void setPaymentDetail(PaymentDetail paymentDetail) {
		this.paymentDetail = paymentDetail;
	}

	/**
	 * @return the infants
	 */
	public Collection<Infant> getInfants() {
		return infants;
	}

	/**
	 * @param infants
	 *            the infants to set
	 */
	public void setInfants(Collection<Infant> infants) {
		this.infants = infants;
	}

	/**
	 * @return the infant count
	 */
	public int getInfantCount() {
		return infants.size();
	}

}
