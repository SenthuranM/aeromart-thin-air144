package com.isa.thinair.gdsservices.api.dto.typea.init;

import com.isa.thinair.airproxy.api.dto.GDSChargeAdjustmentRQ;

public class TicketDisplayRs extends InitBaseRs {

	private TicketsAssembler ticketsAssembler;
	private GDSChargeAdjustmentRQ gdsChargeAdjustmentRq;

	public TicketsAssembler getTicketsAssembler() {
		return ticketsAssembler;
	}

	public void setTicketsAssembler(TicketsAssembler ticketsAssembler) {
		this.ticketsAssembler = ticketsAssembler;
	}

	public GDSChargeAdjustmentRQ getGdsChargeAdjustmentRq() {
		return gdsChargeAdjustmentRq;
	}

	public void setGdsChargeAdjustmentRq(GDSChargeAdjustmentRQ gdsChargeAdjustmentRq) {
		this.gdsChargeAdjustmentRq = gdsChargeAdjustmentRq;
	}
}
