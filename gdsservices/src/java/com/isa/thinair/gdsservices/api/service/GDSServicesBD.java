/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2008 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.api.service;

import java.util.Collection;
import java.util.Map;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.AvsGenerationFlow;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.ets.AeroMartUpdateCouponStatusRq;
import com.isa.thinair.commons.api.dto.ets.AeroMartUpdateCouponStatusRs;
import com.isa.thinair.commons.api.dto.ets.AeroMartUpdateExternalCouponStatusRq;
import com.isa.thinair.commons.api.dto.ets.AeroMartUpdateExternalCouponStatusRs;
import com.isa.thinair.commons.api.dto.ets.TicketDisplayReq;
import com.isa.thinair.commons.api.dto.ets.TicketDisplayRes;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.external.AVSRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.AVSResponse;
import com.isa.thinair.gdsservices.api.dto.external.BookingRequestDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMMessegeDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMResponse;
import com.isa.thinair.gdsservices.api.dto.internal.criteria.GDSMessageSearchCriteria;
import com.isa.thinair.gdsservices.api.model.GDSEdiMessages;
import com.isa.thinair.gdsservices.api.model.GDSMessage;

/**
 * Interface to define all the methods required to access the gds services
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public interface GDSServicesBD {
	public static final String SERVICE_NAME = "GDSServicesService";

	/**
	 * Processes external requests
	 * 
	 * @param bookingRequestDTOMap
	 * @return
	 * @throws ModuleException
	 */
	public Map<String, BookingRequestDTO> processRequests(Map<String, BookingRequestDTO> bookingRequestDTOMap)
			throws ModuleException;

	/**
	 * Audit the requests
	 * 
	 * @param bookingRequestDTO
	 * @param status
	 * @param processDesc
	 * @throws ModuleException
	 */
	public void auditRequests(BookingRequestDTO bookingRequestDTO, String status, String processDesc) throws ModuleException;

	/**
	 * Search GDS Messages
	 * 
	 * @param GDSMessageSearchCriteria
	 * @param startIndex
	 * @param noRecs
	 * @return
	 * @throws ModuleException
	 */
	public Page searchGDSMessages(GDSMessageSearchCriteria criteria, int startIndex, int noRecs) throws ModuleException;

	/**
	 * Get GDS Message
	 * 
	 * @param GDSMessage
	 *            ID
	 * @return
	 * @throws ModuleException
	 */
	public GDSMessage getGDSMessage(long messageId) throws ModuleException;

	/**
	 * Publish Availability.
	 * 
	 * @throws ModuleException
	 */
	public void publishAvailabilityMessages() throws ModuleException;

	/**
	 * Publish Daily Schedule Messages.
	 * 
	 * @throws ModuleException
	 */
	public void publishDailyScheduleUpdateMessages() throws ModuleException;

	public BookingRequestDTO processRequest(BookingRequestDTO bookingRequestDTO) throws ModuleException;

	/**
	 * Send the flown segment statuses to GDS.
	 * 
	 * @param ppfsIds
	 *            : The collecton of pnr pax fare segment ids for which the flown segment statuses are to be updated.
	 * @throws ModuleException
	 */
	public void sendFlownSegmentStatues(Collection<Integer> ppfsIds) throws ModuleException;

	/**
	 * 
	 * @param edifactMessage
	 * @param isDisplayForETS TODO
	 * @return
	 * @throws ModuleException
	 */
	public String processEdifactMesssage(String edifactMessage, boolean isDisplayForETS) throws ModuleException;

	public void saveGdsEdiMessage(GDSEdiMessages message) throws ModuleException;

	public SSMASMResponse processSSMASMRequests(SSMASMMessegeDTO ssiMessegeDTO) throws ModuleException;

	public AVSResponse processAVSRequests(AVSRequestDTO avsRequestDTO) throws ModuleException;

	public Collection<FlightSchedule> getDSTApplicableSubSchedules(FlightSchedule schedule) throws ModuleException;

	AeroMartUpdateCouponStatusRs updateCouponStatus(AeroMartUpdateCouponStatusRq request) throws ModuleException;

	public AeroMartUpdateExternalCouponStatusRs updateExternalCouponStatus(AeroMartUpdateExternalCouponStatusRq request) throws ModuleException;

}
