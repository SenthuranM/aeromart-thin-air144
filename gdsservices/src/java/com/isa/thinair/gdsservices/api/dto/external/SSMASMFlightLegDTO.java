package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;

public class SSMASMFlightLegDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String depatureAirport;

	private String depatureTime;

	private String arrivalAirport;

	private String arrivalTime;

	private int depatureOffiset;

	private int arrivalOffset;

	public String getDepatureAirport() {
		return depatureAirport;
	}

	public void setDepatureAirport(String depatureAirport) {
		this.depatureAirport = depatureAirport;
	}

	public String getDepatureTime() {
		return depatureTime;
	}

	public void setDepatureTime(String depatureTime) {
		this.depatureTime = depatureTime;
	}

	public String getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public int getDepatureOffiset() {
		return depatureOffiset;
	}

	public void setDepatureOffiset(int depatureOffiset) {
		this.depatureOffiset = depatureOffiset;
	}

	public int getArrivalOffset() {
		return arrivalOffset;
	}

	public void setArrivalOffset(int arrivalOffset) {
		this.arrivalOffset = arrivalOffset;
	}
}
