package com.isa.thinair.gdsservices.api.dto.internal;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.SegmentType;

public class SingleSegment extends SegmentDetail {
	public SegmentType getSegmentType() {
		return GDSInternalCodes.SegmentType.SINGLE_SEGMENT;
	}
}
