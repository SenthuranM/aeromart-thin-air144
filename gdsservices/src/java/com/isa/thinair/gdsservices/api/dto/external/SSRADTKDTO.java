package com.isa.thinair.gdsservices.api.dto.external;

import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;

public class SSRADTKDTO extends SSRDTO {

	private static final long serialVersionUID = -4053434381681713813L;

	
	public SSRADTKDTO() {
		this.setCodeSSR(GDSExternalCodes.SSRCodes.ADTK.getCode());
	}
}
