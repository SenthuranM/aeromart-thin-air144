package com.isa.thinair.gdsservices.api.dto.typea.internal;

import java.io.Serializable;

public class InternalBaseRs implements Serializable {

	private boolean success;

	public InternalBaseRs() {
		success = false;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
}
