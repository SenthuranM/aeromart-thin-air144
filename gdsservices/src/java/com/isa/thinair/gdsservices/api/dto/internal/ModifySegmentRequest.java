package com.isa.thinair.gdsservices.api.dto.internal;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

public class ModifySegmentRequest extends GDSReservationRequestBase {

	private static final long serialVersionUID = -228205673800712179L;

	private Collection<SegmentDetail> newSegmentDetails;
	
	private PaymentDetail paymentDetail;

	private Collection<Segment> cancelingSegments;

	private Collection<TempSegBcAllocDTO> blockedSeats;

	public ModifySegmentRequest() {
		newSegmentDetails = new ArrayList<SegmentDetail>();
		cancelingSegments = new ArrayList<Segment>(); 
	}
	
	/**
	 * @return the cancelingSegments
	 */
	public Collection<Segment> getCancelingSegments() {
		return cancelingSegments;
	}


	/**
	 * @param cancelingSegments the cancelingSegments to set
	 */
	public void setCancelingSegments(Collection<Segment> cancelingSegments) {
		this.cancelingSegments = cancelingSegments;
	}


	@Override
	public ReservationAction getActionType() {
		return GDSInternalCodes.ReservationAction.MODIFY_SEGMENT;
	}


	/**
	 * @return the newSegmentDetails
	 */
	public Collection<SegmentDetail> getNewSegmentDetails() {
		return newSegmentDetails;
	}


	/**
	 * @param newSegmentDetails the newSegmentDetails to set
	 */
	public void setNewSegmentDetails(Collection<SegmentDetail> newSegmentDetails) {
		this.newSegmentDetails = newSegmentDetails;
	}

	/**
	 * @return the paymentDetail
	 */
	public PaymentDetail getPaymentDetail() {
		return paymentDetail;
	}

	/**
	 * @param paymentDetail the paymentDetail to set
	 */
	public void setPaymentDetail(PaymentDetail paymentDetail) {
		this.paymentDetail = paymentDetail;
	}

	public Collection<TempSegBcAllocDTO> getBlockedSeats() {
		return blockedSeats;
	}

	public void setBlockedSeats(Collection<TempSegBcAllocDTO> blockedSeats) {
		this.blockedSeats = blockedSeats;
	}
}
