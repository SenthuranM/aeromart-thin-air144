package com.isa.thinair.gdsservices.api.dto.external;

import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;

/**
 * Use to send GDS the relevant AccelAero information
 * 
 * @author Nilindra Fernando
 */
public class SSROTHERSDTO extends SSRDTO {

	private static final long serialVersionUID = -4053434381681713813L;

	public SSROTHERSDTO() {
		this.setCodeSSR(GDSExternalCodes.SSRCodes.OTHERS.getCode());
	}
}
