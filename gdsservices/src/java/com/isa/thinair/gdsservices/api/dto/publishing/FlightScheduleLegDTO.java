package com.isa.thinair.gdsservices.api.dto.publishing;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import com.isa.thinair.commons.api.dto.Frequency;

/**
 * @author Manoj Dhanushka
 */
public class FlightScheduleLegDTO implements Cloneable, Serializable {

	private static final long serialVersionUID = 1L;

	private String departureStation;

	private String arrivalStation;

	private Date timeOfDeparture;

	private Date timeOfArrival;

	private Date timeOfDepartureZulu;

	private Date timeOfArrivalZulu;

	private Date legStartDate;

	private Date legEndDate;

	private int legSequenceNumber;

	private String localTimeVariationForDepatureSt;

	private String passengerTerminalForDepatureSt;

	private String localTimeVariationForArrivalSt;

	private String passengerTerminalForArrivalSt;

	private String connectionStatus;

	private int recordSerialNumber;

	private Collection<DayOfWeek> daysOfOperation;

	public enum DayOfWeek {
		MONDAY(1), TUESDAY(2), WEDNESDAY(3), THURSDAY(4), FRIDAY(5), SATURDAY(6), SUNDAY(7);

		private final int dayNumber;

		DayOfWeek(int dayNumber) {
			this.dayNumber = dayNumber;
		}

		public int dayNumber() {
			return dayNumber;
		}
	}

	public String getDepartureStation() {
		return departureStation;
	}

	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}

	public String getArrivalStation() {
		return arrivalStation;
	}

	public void setArrivalStation(String arrivalStation) {
		this.arrivalStation = arrivalStation;
	}

	public int getLegSequenceNumber() {
		return legSequenceNumber;
	}

	public String getLegSequenceNumberString() {
		return fillFrontZero(2, String.valueOf(legSequenceNumber));
	}

	public void setLegSequenceNumber(int legSequenceNumber) {
		this.legSequenceNumber = legSequenceNumber;
	}

	public String getLocalTimeVariationForDepatureSt() {
		return localTimeVariationForDepatureSt;
	}

	public void setLocalTimeVariationForDepatureSt(String localTimeVariationForDepatureSt) {
		this.localTimeVariationForDepatureSt = localTimeVariationForDepatureSt;
	}

	public String getPassengerTerminalForDepatureSt() {
		return passengerTerminalForDepatureSt;
	}

	public void setPassengerTerminalForDepatureSt(String passengerTerminalForDepatureSt) {
		this.passengerTerminalForDepatureSt = passengerTerminalForDepatureSt;
	}

	public String getLocalTimeVariationForArrivalSt() {
		return localTimeVariationForArrivalSt;
	}

	public void setLocalTimeVariationForArrivalSt(String localTimeVariationForArrivalSt) {
		this.localTimeVariationForArrivalSt = localTimeVariationForArrivalSt;
	}

	public String getPassengerTerminalForArrivalSt() {
		return passengerTerminalForArrivalSt;
	}

	public void setPassengerTerminalForArrivalSt(String passengerTerminalForArrivalSt) {
		this.passengerTerminalForArrivalSt = passengerTerminalForArrivalSt;
	}

	public Date getTimeOfDeparture() {
		return timeOfDeparture;
	}

	public void setTimeOfDeparture(Date timeOfDeparture) {
		this.timeOfDeparture = timeOfDeparture;
	}

	public Date getTimeOfArrival() {
		return timeOfArrival;
	}

	public void setTimeOfArrival(Date timeOfArrival) {
		this.timeOfArrival = timeOfArrival;
	}

	public String getTimeOfDepartureString() {
		return getTimeString(this.timeOfDeparture);
	}

	public String getTimeOfArrivalString() {
		return getTimeString(this.timeOfArrival);
	}

	public String getConnectionStatus() {
		return connectionStatus;
	}

	public void setConnectionStatus(String connectionStatus) {
		this.connectionStatus = connectionStatus;
	}

	public int getRecordSerialNumber() {
		return recordSerialNumber;
	}

	public void setRecordSerialNumber(int recordSerialNumber) {
		this.recordSerialNumber = recordSerialNumber;
	}

	public String getRecordSerialNumberString() {
		return fillFrontZero(6, String.valueOf(recordSerialNumber));
	}

	public void setDaysOfOperation(Collection<DayOfWeek> daysOfOperation) {
		this.daysOfOperation = daysOfOperation;
	}

	public void setDaysOfOperation(Frequency frequency) {
		if (frequency.getDay0() || frequency.getDay1() || frequency.getDay2() || frequency.getDay3() || frequency.getDay4()
				|| frequency.getDay5() || frequency.getDay6()) {

			this.daysOfOperation = new ArrayList<DayOfWeek>();
			if (frequency.getDay0())
				this.daysOfOperation.add(DayOfWeek.MONDAY);
			if (frequency.getDay1())
				this.daysOfOperation.add(DayOfWeek.TUESDAY);
			if (frequency.getDay2())
				this.daysOfOperation.add(DayOfWeek.WEDNESDAY);
			if (frequency.getDay3())
				this.daysOfOperation.add(DayOfWeek.THURSDAY);
			if (frequency.getDay4())
				this.daysOfOperation.add(DayOfWeek.FRIDAY);
			if (frequency.getDay5())
				this.daysOfOperation.add(DayOfWeek.SATURDAY);
			if (frequency.getDay6())
				this.daysOfOperation.add(DayOfWeek.SUNDAY);
		}
	}

	public void setDaysOfOperation(int dayNumber) {
		this.daysOfOperation = new ArrayList<DayOfWeek>();
		switch (dayNumber) {
		case 0:
			this.daysOfOperation.add(DayOfWeek.MONDAY);
			break;
		case 1:
			this.daysOfOperation.add(DayOfWeek.TUESDAY);
			break;
		case 2:
			this.daysOfOperation.add(DayOfWeek.WEDNESDAY);
			break;
		case 3:
			this.daysOfOperation.add(DayOfWeek.THURSDAY);
			break;
		case 4:
			this.daysOfOperation.add(DayOfWeek.FRIDAY);
			break;
		case 5:
			this.daysOfOperation.add(DayOfWeek.SATURDAY);
			break;
		case 6:
			this.daysOfOperation.add(DayOfWeek.SUNDAY);
			break;
		default:
			this.daysOfOperation.add(DayOfWeek.SUNDAY);
			break;
		}
	}

	public Collection<DayOfWeek> getDaysOfOperation() {
		return daysOfOperation;
	}

	public String getDaysOfOperationString() {
		String daysOfOperationString = "";
		String[] days = new String[7];
		for (int i = 0; i < 7; i++) {
			days[i] = " ";
		}
		for (Iterator iter = this.daysOfOperation.iterator(); iter.hasNext();) {
			DayOfWeek dayOfWeek = (DayOfWeek) iter.next();
			days[dayOfWeek.dayNumber() - 1] = String.valueOf(dayOfWeek.dayNumber());
		}
		for (int j = 0; j < 7; j++) {
			daysOfOperationString += days[j];
		}
		return daysOfOperationString;
	}

	private String fillFrontZero(int targetLength, String source) {
		while (source.length() < targetLength) {
			source = "0" + source;
		}
		return source;
	}

	/**
	 * Method will return String representation of time. time format : HHmm eg: 0954
	 * 
	 * @param date
	 * @return
	 */
	public static String getTimeString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
		return sdf.format(date).toUpperCase().trim();
	}

	public Object clone() {
		try {
			FlightScheduleLegDTO cloned = (FlightScheduleLegDTO) super.clone();
			cloned.departureStation = departureStation;
			cloned.arrivalStation = arrivalStation;
			cloned.timeOfDeparture = (Date) timeOfDeparture.clone();
			cloned.timeOfArrival = (Date) timeOfArrival.clone();
			cloned.legStartDate = (Date) legStartDate.clone();
			cloned.legEndDate = (Date) legEndDate.clone();
			cloned.legSequenceNumber = legSequenceNumber;
			cloned.localTimeVariationForDepatureSt = localTimeVariationForDepatureSt;
			cloned.passengerTerminalForDepatureSt = passengerTerminalForDepatureSt;
			cloned.localTimeVariationForArrivalSt = localTimeVariationForArrivalSt;
			cloned.passengerTerminalForArrivalSt = passengerTerminalForArrivalSt;
			cloned.connectionStatus = connectionStatus;
			cloned.recordSerialNumber = recordSerialNumber;
			cloned.daysOfOperation = daysOfOperation;
			return cloned;
		} catch (CloneNotSupportedException e) {
			System.out.println(e);
			return null;
		}
	}

	public Date getLegStartDate() {
		return legStartDate;
	}

	public Date getLegEndDate() {
		return legEndDate;
	}

	public void setLegStartDate(Date legStartDate) {
		this.legStartDate = legStartDate;
	}

	public void setLegEndDate(Date legEndDate) {
		this.legEndDate = legEndDate;
	}

	public String getFromDateOfPeriodString() {
		Date now = new Date(System.currentTimeMillis());
		if (this.legStartDate.before(now)) {
			this.legStartDate = now;
		}
		return getDateString(this.legStartDate);
	}

	public String getToDateOfPeriodString() {
		return getDateString(this.legEndDate);
	}

	/**
	 * Method will return String representation of date. date format : ddMMMyy eg: 08JUN05
	 * 
	 * @param date
	 * @return
	 */
	private String getDateString(Date date) {

		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyy");
		return sdf.format(date).toUpperCase().trim();
	}

	public Date getTimeOfDepartureZulu() {
		return timeOfDepartureZulu;
	}

	public void setTimeOfDepartureZulu(Date timeOfDepartureZulu) {
		this.timeOfDepartureZulu = timeOfDepartureZulu;
	}

	public Date getTimeOfArrivalZulu() {
		return timeOfArrivalZulu;
	}

	public void setTimeOfArrivalZulu(Date timeOfArrivalZulu) {
		this.timeOfArrivalZulu = timeOfArrivalZulu;
	}

}
