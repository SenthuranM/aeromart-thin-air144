package com.isa.thinair.gdsservices.api.dto.external;

import java.util.Date;

import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;

public class SsrTicketingTimeLimitDTO extends SSRDTO {
	
	private static final long serialVersionUID = 1L;

	public SsrTicketingTimeLimitDTO() {
		this.setCodeSSR(GDSExternalCodes.SSRCodes.EXTEND_TIMELIMIT.getCode());
	}
	
	private Date ticketingTime;

	public Date getTicketingTime() {
		return ticketingTime;
	}

	public void setTicketingTime(Date ticketingTime) {
		this.ticketingTime = ticketingTime;
	}
}