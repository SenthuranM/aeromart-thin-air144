package com.isa.thinair.gdsservices.api.dto.typea;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class InventoryAdjustmentResponseDTO {

	private Set<ERCDetail> ercDetails;

	private List<ODIResponseDTO> odiResponseDTOs;

	/**
	 * @return the ercDetails
	 */
	public Set<ERCDetail> getErcDetails() {
		if (ercDetails == null) {
			ercDetails = new HashSet<ERCDetail>();
		}
		return ercDetails;
	}

	/**
	 * @param ercDetails
	 *            the ercDetails to set
	 */
	public void setErcDetails(Set<ERCDetail> ercDetails) {
		this.ercDetails = ercDetails;
	}

	public void addErcDetails(ERCDetail ercDetail) {
		this.getErcDetails().add(ercDetail);
	}

	public List<ODIResponseDTO> getOdiResponseDTOs() {
		return odiResponseDTOs;
	}

	public void setOdiResponseDTOs(List<ODIResponseDTO> odiResponseDTOs) {
		this.odiResponseDTOs = odiResponseDTOs;
	}

	public void rearangeERC() {

		for (ODIResponseDTO odiResponseDTO : getOdiResponseDTOs()) {
			odiResponseDTO.rearangeERC();
		}

		if (!getOdiResponseDTOs().isEmpty()) {
			Set<ERCDetail> intersection = new HashSet<ERCDetail>(getOdiResponseDTOs().get(0).getErcDetails());
			for (int i = 1; i < getOdiResponseDTOs().size(); i++) {
				intersection.retainAll(getOdiResponseDTOs().get(i).getErcDetails());
			}

			// removing ERC segments common to all TVLs and move up to the ODI level.
			if (!intersection.isEmpty()) {
				for (int i = 0; i < getOdiResponseDTOs().size(); i++) {
					Set<ERCDetail> erciftResponses = getOdiResponseDTOs().get(i).getErcDetails();
					erciftResponses.removeAll(intersection);
				}
				getErcDetails().addAll(intersection);
			}
		}
	}
}
