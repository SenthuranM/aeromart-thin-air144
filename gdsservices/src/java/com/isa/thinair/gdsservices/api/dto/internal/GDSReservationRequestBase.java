package com.isa.thinair.gdsservices.api.dto.internal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

public abstract class GDSReservationRequestBase extends GDSBase {

	private static final long serialVersionUID = 7608887368339682851L;

	private Collection<GDSInternalCodes.ReservationAction> reservationAction;

	private GDSCredentialsDTO gdsCredentialsDTO;

	private String originatorAddress;

	private String responderAddress;

	private GDSExternalCodes.GDS gds;

	private String communicationReference;

	private Date messageReceivedDateStamp;

	private boolean isSuccess;

	private String responseMessage;

	// GDS PNR
	private RecordLocator originatorRecordLocator;

	// PNR
	private RecordLocator responderRecordLocator;
	
	private boolean notSupportedSSRExists;

	private List<String> errorCode;

	private boolean sendResponse;

	public GDSReservationRequestBase() {
		this.errorCode = new ArrayList<>();
		this.sendResponse = true;
	}

	public abstract ReservationAction getActionType();

	/**
	 * @return Returns the gds.
	 */
	public GDSExternalCodes.GDS getGds() {
		return gds;
	}

	/**
	 * @param gds
	 *            The gds to set.
	 */
	public void setGds(GDSExternalCodes.GDS gds) {
		this.gds = gds;
	}

	/**
	 * @return Returns the originatorAddress.
	 */
	public String getOriginatorAddress() {
		return originatorAddress;
	}

	/**
	 * @param originatorAddress
	 *            The originatorAddress to set.
	 */
	public void setOriginatorAddress(String originatorAddress) {
		this.originatorAddress = originatorAddress;
	}

	/**
	 * @return Returns the responderAddress.
	 */
	public String getResponderAddress() {
		return responderAddress;
	}

	/**
	 * @param responderAddress
	 *            The responderAddress to set.
	 */
	public void setResponderAddress(String responderAddress) {
		this.responderAddress = responderAddress;
	}

	/**
	 * @return Returns the communicationReference.
	 */
	public String getCommunicationReference() {
		return communicationReference;
	}

	/**
	 * @param communicationReference
	 *            The communicationReference to set.
	 */
	public void setCommunicationReference(String communicationReference) {
		this.communicationReference = communicationReference;
	}

	/**
	 * @return Returns the messageReceivedDateStamp.
	 */
	public Date getMessageReceivedDateStamp() {
		return messageReceivedDateStamp;
	}

	/**
	 * @param messageReceivedDateStamp
	 *            The messageReceivedDateStamp to set.
	 */
	public void setMessageReceivedDateStamp(Date messageReceivedDateStamp) {
		this.messageReceivedDateStamp = messageReceivedDateStamp;
	}

	/**
	 * @return the reservationAction
	 */
	public Collection<GDSInternalCodes.ReservationAction> getReservationAction() {
		return reservationAction;
	}

	/**
	 * Add Reservation Action
	 * 
	 * @param reservationAction
	 */
	public void addReservationAction(GDSInternalCodes.ReservationAction reservationAction) {
		if (this.getReservationAction() == null) {
			this.setReservationAction(new ArrayList<GDSInternalCodes.ReservationAction>());
		}
		this.getReservationAction().add(reservationAction);
	}

	/**
	 * @param reservationAction
	 *            the reservationAction to set
	 */
	private void setReservationAction(Collection<GDSInternalCodes.ReservationAction> reservationAction) {
		this.reservationAction = reservationAction;
	}

	/**
	 * @return the originatorRecordLocator
	 */
	public RecordLocator getOriginatorRecordLocator() {
		return originatorRecordLocator;
	}

	/**
	 * @param originatorRecordLocator
	 *            the originatorRecordLocator to set
	 */
	public void setOriginatorRecordLocator(RecordLocator originatorRecordLocator) {
		this.originatorRecordLocator = originatorRecordLocator;
	}

	/**
	 * @return the responderRecordLocator
	 */
	public RecordLocator getResponderRecordLocator() {
		return responderRecordLocator;
	}

	/**
	 * @param responderRecordLocator
	 *            the responderRecordLocator to set
	 */
	public void setResponderRecordLocator(RecordLocator responderRecordLocator) {
		this.responderRecordLocator = responderRecordLocator;
	}

	/**
	 * @return the isSuccess
	 */
	public boolean isSuccess() {
		return isSuccess;
	}

	/**
	 * @param isSuccess
	 *            the isSuccess to set
	 */
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	/**
	 * @return the responseMessage
	 */
	public String getResponseMessage() {
		return responseMessage;
	}

	/**
	 * @param responseMessage
	 *            the responseMessage to set
	 */
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	/**
	 * @return the gdsCredentialsDTO
	 */
	public GDSCredentialsDTO getGdsCredentialsDTO() {
		return gdsCredentialsDTO;
	}

	/**
	 * @param gdsCredentialsDTO
	 *            the gdsCredentialsDTO to set
	 */
	public void setGdsCredentialsDTO(GDSCredentialsDTO gdsCredentialsDTO) {
		this.gdsCredentialsDTO = gdsCredentialsDTO;
	}

	public boolean isNotSupportedSSRExists() {
		return notSupportedSSRExists;
	}

	public void setNotSupportedSSRExists(boolean notSupportedSSRExists) {
		this.notSupportedSSRExists = notSupportedSSRExists;
	}

	public List<String> getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(List<String> errorCode) {
		this.errorCode = errorCode;
	}

	public void addErrorCode(String errorCode) {
		this.errorCode.add(errorCode);
	}

	public boolean isSendResponse() {
		return sendResponse;
	}

	public void setSendResponse(boolean sendResponse) {
		this.sendResponse = sendResponse;
	}
}
