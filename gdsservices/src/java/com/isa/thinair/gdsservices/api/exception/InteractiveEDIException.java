package com.isa.thinair.gdsservices.api.exception;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;

public class InteractiveEDIException extends ModuleException {

	private static final long serialVersionUID = 6041339162257890360L;

	private CodeSetEnum.CS0085 errorCode;
	private String description = "";

	public InteractiveEDIException(CodeSetEnum.CS0085 errorCode, String description) {
		super(String.valueOf(errorCode.getCode()));
		this.errorCode = errorCode;
		this.description = description;
	}

	public InteractiveEDIException(String exceptionCode) {
		super(exceptionCode);
	}

	/**
	 * @return the errorCode
	 */
	public CodeSetEnum.CS0085 getErrorCode() {
		return errorCode;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

}
