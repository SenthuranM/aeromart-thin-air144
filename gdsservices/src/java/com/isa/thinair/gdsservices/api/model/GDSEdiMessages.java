package com.isa.thinair.gdsservices.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * Model for saving incoming/outgoing EDIFACT messages
 * 
 * @hibernate.class table = "T_GDS_EDI_MESSAGES"
 */
public class GDSEdiMessages extends Persistent {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String gdsTransactionRef;

	private String reqMessage;
	private Date reqTimestamp;

	private String resMessage;
	private Date resTimestamp;

	private String errors;

	/**
	 * @return id
	 * 
	 * @hibernate.id column = "GDS_EDI_MESSAGES_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_GDS_EDI_MESSAGES"
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return gdsTransactionRef
	 * 
	 * @hibernate.property column = "GDS_TXN_REFERENCE"
	 */
	public String getGdsTransactionRef() {
		return gdsTransactionRef;
	}

	/**
	 * @param gdsTransactionRef
	 */
	public void setGdsTransactionRef(String gdsTransactionRef) {
		this.gdsTransactionRef = gdsTransactionRef;
	}

	/**
	 * @return the reqMessage
	 * 
	 * @hibernate.property column = "REQ_MESSAGE"
	 */
	public String getReqMessage() {
		return reqMessage;
	}

	/**
	 * @param reqMessage
	 *            the reqMessage to set
	 */
	public void setReqMessage(String reqMessage) {
		this.reqMessage = reqMessage;
	}

	/**
	 * @return the reqTimestamp
	 * 
	 * @hibernate.property column = "REQ_TIMESTAMP"
	 */
	public Date getReqTimestamp() {
		return reqTimestamp;
	}

	/**
	 * @param reqTimestamp
	 *            the reqTimestamp to set
	 */
	public void setReqTimestamp(Date reqTimestamp) {
		this.reqTimestamp = reqTimestamp;
	}

	/**
	 * @return the resMessage
	 * 
	 * @hibernate.property column = "RES_MESSAGE"
	 */
	public String getResMessage() {
		return resMessage;
	}

	/**
	 * @param resMessage
	 *            the resMessage to set
	 */
	public void setResMessage(String resMessage) {
		this.resMessage = resMessage;
	}

	/**
	 * @return the resTimestamp
	 * 
	 * @hibernate.property column = "RES_TIMESTAMP"
	 */
	public Date getResTimestamp() {
		return resTimestamp;
	}

	/**
	 * @param resTimestamp
	 *            the resTimestamp to set
	 */
	public void setResTimestamp(Date resTimestamp) {
		this.resTimestamp = resTimestamp;
	}

	/**
	 * @hibernate.property column = "ERROR_DESCRIPTION"
	 */
	public String getErrors() {
		return errors;
	}

	public void setErrors(String errors) {
		this.errors = errors;
	}
}