package com.isa.thinair.gdsservices.api.model;

import java.io.Serializable;

/**
 * @hibernate.class table = "T_GDS_RESERVATION_INFO"
 */
public class GDSReservationInfo implements Serializable {

	private Long gdsReservationInfoId;
	private String pnr;
	private String segmentType;
	private String segmentContent;

	/**
	 * @hibernate.id column = "GDS_RESERVATION_INFO_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_GDS_RESERVATION_INFO"
	 */
	public Long getGdsReservationInfoId() {
		return gdsReservationInfoId;
	}

	public void setGdsReservationInfoId(Long gdsReservationInfoId) {
		this.gdsReservationInfoId = gdsReservationInfoId;
	}

	/**
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @hibernate.property column = "SEGMENT_TYPE"
	 */
	public String getSegmentType() {
		return segmentType;
	}

	public void setSegmentType(String segmentType) {
		this.segmentType = segmentType;
	}

	/**
	 * @hibernate.property column = "SEGMENT_CONTENT"
	 */
	public String getSegmentContent() {
		return segmentContent;
	}

	public void setSegmentContent(String segmentContent) {
		this.segmentContent = segmentContent;
	}
}
