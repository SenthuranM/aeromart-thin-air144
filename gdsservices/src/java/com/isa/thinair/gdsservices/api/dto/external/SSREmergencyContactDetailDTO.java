package com.isa.thinair.gdsservices.api.dto.external;

/**
 * This DTO hold emergency contact Details provided by passenger as a for of a SSR; SSR CODE :PCTC
 * 
 * @author sanjeewaf
 */
public class SSREmergencyContactDetailDTO extends SSRDTO {

	private static final long serialVersionUID = -5737364468765955217L;

	private String emContactName;

	private boolean refused;

	private String emContactNumberwithCountry;

	private String passengerFirstName;

	private String passengerLastName;

	private String passengerTitle;

	private String fullElement;

	/**
	 * @return the passengerTitle
	 */
	public String getPassengerTitle() {
		return passengerTitle;
	}

	/**
	 * @param passengerTitle
	 *            the passengerTitle to set
	 */
	public void setPassengerTitle(String passengerTitle) {
		this.passengerTitle = passengerTitle;
	}

	/**
	 * @return the emContactName
	 */
	public String getEmContactName() {
		return emContactName;
	}

	/**
	 * @param emContactName
	 *            the emContactName to set
	 */
	public void setEmContactName(String emContactName) {
		this.emContactName = emContactName;
	}

	/**
	 * @return the emContactNumberwithCountry
	 */
	public String getEmContactNumberwithCountry() {
		return emContactNumberwithCountry;
	}

	/**
	 * @param emContactNumberwithCountry
	 *            the emContactNumberwithCountry to set
	 */
	public void setEmContactNumberwithCountry(String emContactNumberwithCountry) {
		this.emContactNumberwithCountry = emContactNumberwithCountry;
	}

	/**
	 * @return the fullElement
	 */
	public String getFullElement() {
		return fullElement;
	}

	/**
	 * @param fullElement
	 *            the fullElement to set
	 */
	public void setFullElement(String fullElement) {
		this.fullElement = fullElement;
	}

	/**
	 * @return the passengerFirstName
	 */
	public String getPassengerFirstName() {
		return passengerFirstName;
	}

	/**
	 * @param passengerFirstName
	 *            the passengerFirstName to set
	 */
	public void setPassengerFirstName(String passengerFirstName) {
		this.passengerFirstName = passengerFirstName;
	}

	/**
	 * @return the passengerLastName
	 */
	public String getPassengerLastName() {
		return passengerLastName;
	}

	/**
	 * @param passengerLastName
	 *            the passengerLastName to set
	 */
	public void setPassengerLastName(String passengerLastName) {
		this.passengerLastName = passengerLastName;
	}

	/**
	 * @return the refused
	 */
	public boolean isRefused() {
		return refused;
	}

	/**
	 * @param refused
	 *            the refused to set
	 */
	public void setRefused(boolean refused) {
		this.refused = refused;
	}
}
