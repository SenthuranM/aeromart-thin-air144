package com.isa.thinair.gdsservices.api.dto.typea;

public class InventoryAdjustmentTypeBRS extends TypeBRS {
	private String pnr;
	private MessageError messageError;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public MessageError getMessageError() {
		return messageError;
	}

	public void setMessageError(MessageError messageError) {
		this.messageError = messageError;
	}
}
