package com.isa.thinair.gdsservices.api.dto.typea;

import java.io.Serializable;
import java.util.List;

import com.isa.thinair.msgbroker.api.dto.BookingSegmentDTO;

public class TypeBAnalyseRS implements Serializable {

	private boolean messageError;
	private String gdsCarrierCode;
	private int paxCount;
	private int infantCount;
	private List<BookingSegmentDTO> segments;

	public TypeBAnalyseRS() {
		messageError = true;
	}

	public boolean isMessageError() {
		return messageError;
	}

	public void setMessageError(boolean messageError) {
		this.messageError = messageError;
	}

	public String getGdsCarrierCode() {
		return gdsCarrierCode;
	}

	public void setGdsCarrierCode(String gdsCarrierCode) {
		this.gdsCarrierCode = gdsCarrierCode;
	}

	public int getPaxCount() {
		return paxCount;
	}

	public void setPaxCount(int paxCount) {
		this.paxCount = paxCount;
	}

	public int getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}

	public List<BookingSegmentDTO> getSegments() {
		return segments;
	}

	public void setSegments(List<BookingSegmentDTO> segments) {
		this.segments = segments;
	}
}
