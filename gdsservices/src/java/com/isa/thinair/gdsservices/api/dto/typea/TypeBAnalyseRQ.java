package com.isa.thinair.gdsservices.api.dto.typea;

import java.io.Serializable;

public class TypeBAnalyseRQ implements Serializable {
	private String typeBMessage;
	private boolean countPax;
	private boolean originDestination;

	public String getTypeBMessage() {
		return typeBMessage;
	}

	public void setTypeBMessage(String typeBMessage) {
		this.typeBMessage = typeBMessage;
	}

	public boolean isCountPax() {
		return countPax;
	}

	public void setCountPax(boolean countPax) {
		this.countPax = countPax;
	}

	public boolean isOriginDestination() {
		return originDestination;
	}

	public void setOriginDestination(boolean originDestination) {
		this.originDestination = originDestination;
	}
}
