package com.isa.thinair.gdsservices.api.dto.internal;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

public class RemoveSsrRequest extends AddSsrRequest {

	@Override
	public ReservationAction getActionType() {
		return GDSInternalCodes.ReservationAction.REMOVE_SSR;
	}

}
