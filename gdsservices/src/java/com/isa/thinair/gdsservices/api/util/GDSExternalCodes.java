package com.isa.thinair.gdsservices.api.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Nilindra Fernando
 * @since 7/7/2008
 */
public class GDSExternalCodes {

	public enum NotifyStatus {
		PROCEED, NAC, IGNORE;
	}

	public enum MessageType {
		RESERVATION("RES"), AVS("AVS"), SSM("SSM"), ASM("ASM"), ASC("ASC");

		private MessageType(String value) {
			this.value = value;
		}

		private String value;

		public String getValue() {
			return value;
		}
	}

	public enum ProcessStatus {
		NOT_PROCESSED("NP", NotifyStatus.NAC), ERROR_OCCURED("EO", NotifyStatus.NAC), VALIDATE_SUCCESS("VS", NotifyStatus.PROCEED), VALIDATE_FAILURE(
				"VF", NotifyStatus.PROCEED), GENERAL_FAILURE("GF", NotifyStatus.NAC), EXTRACT_SUCCESS("ES", NotifyStatus.PROCEED), INVOCATION_SUCCESS(
				"IS", NotifyStatus.PROCEED), INVOCATION_FAILURE("IF", NotifyStatus.PROCEED), INVOCATION_IGNORED("II",
				NotifyStatus.IGNORE), INVOCATION_SUCCESS_IGNORE_NOTIFICATION("SI", NotifyStatus.IGNORE);

		private String value;

		// This is the status that message broker should check
		private NotifyStatus notifyStatus;

		private ProcessStatus(String value, NotifyStatus notifyStatus) {
			this.value = value;
			this.notifyStatus = notifyStatus;
		}

		public boolean equals(String element) {
			if (getValue().equals(element)) {
				return true;
			} else {
				return false;
			}
		}

		public NotifyStatus getNotifyStatus() {
			return notifyStatus;
		}

		public String getValue() {
			return value;
		}
	}

	public enum ActionCode {
		CANCEL("XX", "CANCEL"), CANCEL_WAITLISTED("XL", "CANCEL"), CANCEL_IF_AVAILABLE("OX", "CANCEL"), CANCEL_RECOMMENDED("XR",
				"CANCEL"), CANCEL_IF_HOLDING("IX", "CANCEL"), NEED("NN", "ONHOLD_REPLY_REQUIRED"), NEED_ALTERNATE("NA",
				"ONHOLD_REPLY_REQUIRED"), NEED_IFNOT_HOLDING("IN", "ONHOLD_REPLY_REQUIRED"), WAITLIST("LL", "WAITLIST"), SOLD(
				"SS", "ONHOLD"), SOLD_FREE("SF", "ONHOLD"), SOLD_IFNOT_HOLDING("IS", "ONHOLD"), CODESHARE_SELL("CS",
				"CODESHARE_SELL"), CODESHARE_CANCEL("CX", "CODESHARE_CANCEL"), EXCHANGE_RECOMMENDED("EX", "EXCHANGE_RECOMMENDED");

		private String code;
		private String category;
		private static Map<String, ActionCode> valueMap = createValueMap();

		private ActionCode(String code, String category) {
			this.code = code;
			this.category = category;
		}

		public String getCode() {
			return code;
		}

		public static ActionCode getValue(String code) {
			return valueMap.get(code);
		}

		private static Map<String, ActionCode> createValueMap() {
			Map<String, ActionCode> codeMap = new HashMap<String, ActionCode>();
			ActionCode[] values = ActionCode.values();

			for (int i = 0; i < values.length; i++) {
				codeMap.put(values[i].getCode(), values[i]);
			}

			return codeMap;
		}

		public boolean isCancelAction() {
			return "CANCEL".equals(this.category);
		}

		public boolean isConfirmAction() {
			return "CONFIRM".equals(this.category);
		}
	}

	public enum StatusCode {
		HAVE_LISTED("HL"), HAVE_REQUESTED("HN"), HOLDS_CONFIRMED("HK"), HAVE_SOLD("HS"), RECONFIRMED("RR"), CODESHARE_CONFIRMED(
				"CH");

		private String code;
		private static Map<String, StatusCode> valueMap = createValueMap();

		private StatusCode(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		public static StatusCode getValue(String code) {
			return valueMap.get(code);
		}

		private static Map<String, StatusCode> createValueMap() {
			Map<String, StatusCode> codeMap = new HashMap<String, StatusCode>();
			StatusCode[] values = StatusCode.values();

			for (int i = 0; i < values.length; i++) {
				codeMap.put(values[i].getCode(), values[i]);
			}

			return codeMap;
		}
	}

	public static enum AdviceCode {
		NO_ACTION_TAKEN("NO"), CANCELLED("HX"), CONFIRMED("KK"), CONFIRMED_SCHEDULE_CHANGED("TK"), HOLD_NEED_SCHEDULE_CHANGED(
				"TN"), CONFIRMED_FROM_WAIT_LIST("KL"), WAITLISTED("UU"), WAITLIST_CLOSED("UC"), UNABLE_NOT_OPERATED_PROVIDED("UN"), UNABLE_NOT_AVAILABLE(
				"US");

		private String code;
		private static Map<String, AdviceCode> valueMap = createValueMap();

		private AdviceCode(String value) {
			this.code = value;
		}

		public String getCode() {
			return code;
		}

		private static Map<String, AdviceCode> createValueMap() {
			Map<String, AdviceCode> codeMap = new HashMap<String, AdviceCode>();
			AdviceCode[] values = AdviceCode.values();

			for (int i = 0; i < values.length; i++) {
				codeMap.put(values[i].getCode(), values[i]);
			}

			return codeMap;
		}

		public static AdviceCode getValue(String code) {
			return valueMap.get(code);
		}
	}

	public enum MessageIdentifier {
		NEW_BOOKING("BKG"), AMEND_BOOKING("AMD"), CANCEL_BOOKING("CAN"), DEVIDE_BOOKING("DVD"), LOCATE_RESERVATION("RLR"), NEW_CONTINUATION(
				"NCO"), NEW_ARRIVAL("NAR"), ARRIVAL_UNKNOWN("ARNK"), STATUS_REPLY("STR"), TICKETING("ETK"), RES_ACKNOWLEDGEMENT("AKA"), LINK_RECORD_LOCATOR(
				"TRL"), PDM("PDM"), SCHEDULE_CHANGE("ASC"), DIVIDE_RESP("DVR"), RESERVATION_RESP("RRP"), ACKNOWLEDGEMENT("ACK");

		private MessageIdentifier(String code) {
			this.code = code;
		}

		private String code;

		public String getCode() {
			return code;
		}
	}
	
	public enum SyncMessageIdentifier {
		SYNCHRONIZATION("SNC"), DIVIDE("SDV");

		private SyncMessageIdentifier(String code) {
			this.code = code;
		}

		private String code;

		public String getCode() {
			return code;
		}

		public static SyncMessageIdentifier getSyncMessageIdentifier(String msgId) {
			SyncMessageIdentifier id = null;
			for (SyncMessageIdentifier syncMessageIdentifier : SyncMessageIdentifier.values()) {
				if (syncMessageIdentifier.code.equals(msgId)) {
					id = syncMessageIdentifier;
				}
			}

			return id;
		}
	}

	public enum PassengerType {
		ADULT("AD"), CHILD("CH"), INFANT("IN");

		private PassengerType(String code) {
			this.code = code;
		}

		private String code;

		public Collection<String> getTypes() {
			Collection<String> paxTypes = new ArrayList<String>();
			paxTypes.add(PassengerType.ADULT.getCode());
			paxTypes.add(PassengerType.CHILD.getCode());
			paxTypes.add(PassengerType.INFANT.getCode());
			return paxTypes;
		}

		public String getCode() {
			return code;
		}
	}

	public enum PassengerTitle {
		MR("MR"), MS("MS"), MRS("MRS"), MASTER("MSTR"), MISS("MISS"), DOCTOR("DR"), CAPTAIN("CAPT");

		private PassengerTitle(String code) {
			this.code = code;
		}

		private String code;

		public String getCode() {
			return code;
		}
	}

	public enum GDS {
		AMADEUS("AMADEUS"), SABRE("SABRE"), TOPAS("TOPAS"), ABACUS("ABACUS"), UMAIR("UMAIR"), SIRENA("SIRENA"), MAB("MAB"), MAHAN(
				"MAHAN"), GEORGIANAIR("GEORGIANAIR"), CHAMWINGS("CHAMWINGS"), APG("APG");

		private GDS(String code) {
			this.code = code;
		}

		private String code;

		public String getCode() {
			return code;
		}
	}

	public enum SSRCodes {
		CHILD("CHLD"), INFANT("INFT"), PCTC("PCTC"), ETICKET_NO("TKNE"), OTHERS("OTHS"), DOCS("DOCS"), DOCO("DOCO"), ET_NOT_CHANGED("TKNR"), EXTEND_TIMELIMIT(
				"TKTL"), ADTK("ADTK");

		private SSRCodes(String code) {
			this.code = code;
		}

		private String code;

		public String getCode() {
			return code;
		}
	}

	public enum OSICodes {
		CONTACT_PHONE_NATURE_UNKNOWN("CTCP"), OTHERS("OTHS"), CONTACT_PHONE_BUSINESS("CTCB"), CONTACT_PHONE_HOME("CTCH"), CONTACT_PHONE_MOBILE(
				"CTCM"), CONTACT_PHONE_TRAVEL_AGENT("CTCT"), CONTACT_ADDRESS("CTCA"), CONTACT_EMAIL("CTCE"), CONTACT_PHONE_FAX("CTCF"), REMAINING_PASSENGERS("TCP"), RECORD_LOCATOR("RLOC");

		private String code;

		private OSICodes(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
	}

	public enum CardCompanyCode {
		MASTER("XX", "1"), VISA("BA", "2"), AMEX("AX", "3");

		private static Map<String, CardCompanyCode> valueMap = createValueMap();
		// GDS Code
		private String gdsCode;
		// AccelAero Code
		private String code;

		private CardCompanyCode(String gdsCode, String code) {
			this.gdsCode = gdsCode;
			this.code = code;
		}

		public String getGdsCode() {
			return gdsCode;
		}

		public String getCode() {
			return code;
		}

		public static CardCompanyCode getValue(String code) {
			return valueMap.get(code);
		}

		private static Map<String, CardCompanyCode> createValueMap() {
			Map<String, CardCompanyCode> codeMap = new HashMap<String, CardCompanyCode>();
			CardCompanyCode[] values = CardCompanyCode.values();

			for (int i = 0; i < values.length; i++) {
				codeMap.put(values[i].getGdsCode(), values[i]);
			}

			return codeMap;
		}
	}
	
	public enum Gender {
		FEMALE("F"), MALE("M"), FEMALE_INFANT("FI"), MALE_INFANT("MI"), UNDISCLOSED_GENDER("U");

		private String code;

		private Gender(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
	}
}
