package com.isa.thinair.gdsservices.api.dto.typea.airimp;

import java.io.Serializable;

public class CommunicationsReference implements Serializable {

	private static final long serialVersionUID = 1L;

	private String cityAirport;
	private String function;
	private String airline;

	private String day;
	private String month;
	private String year;

	public String getCityAirport() {
		return cityAirport;
	}

	public void setCityAirport(String cityAirport) {
		this.cityAirport = cityAirport;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}
}