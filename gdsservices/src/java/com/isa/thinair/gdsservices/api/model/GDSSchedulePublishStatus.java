package com.isa.thinair.gdsservices.api.model;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * To keep track of GDS Messages
 * 
 * @author Manoj Dhanushka
 * @hibernate.class table = "T_GDS_SCHEDULE_PUBLISH_STATUS"
 */
public class GDSSchedulePublishStatus extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5179112283731055648L;

	/** Holds the message id */
	private Long messageId;

	/** Holds the gds id */
	private int gdsId;

	/** Holds the message content */
	private Blob message;

	/** Holds the status */
	private String status;

	/** Holds the message published Time */
	private Date publishedTime;

	/**
	 * returns the id
	 * 
	 * @hibernate.id column = "GSPS_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_GDS_SCHEDULE_PUBLISH_STATUS"
	 */
	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	/**
	 * return the gds id which message intended to send
	 * 
	 * @hibernate.property column = "GDS_ID"
	 */
	public int getGdsId() {
		return gdsId;
	}

	public void setGdsId(int gdsId) {
		this.gdsId = gdsId;
	}

	/**
	 * returns the message content
	 * 
	 * @hibernate.property column = "MESSAGE"
	 */
	public Blob getMessage() {
		return message;
	}

	public void setMessage(Blob message) {
		this.message = message;
	}

	/**
	 * returns the status of the message
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * returns the message published date
	 * 
	 * @hibernate.property column = "PUBLISHED_TIME"
	 */
	public Date getPublishedTime() {
		return publishedTime;
	}

	public void setPublishedTime(Date publishedTime) {
		this.publishedTime = publishedTime;
	}

}
