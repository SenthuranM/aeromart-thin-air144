package com.isa.thinair.gdsservices.api.dto.publishing;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.thinair.gdsservices.core.bl.dsu.SchedulePublishUtil;

/**
 * @author Manoj Dhanushka
 */
public class ItineraryVariationScheduleMessageDTO implements Cloneable, Serializable {

	private static final long serialVersionUID = 1L;

	int itineraryVariationIdentifier;

	private Date fromDateOfPeriod;

	private Date toDateOfPeriod;

	private List<FlightScheduleLegDTO> legs;

	public String getItineraryVariationIdentifier() {
		return SchedulePublishUtil.fillFrontZero(2, String.valueOf(itineraryVariationIdentifier));
	}

	public void setItineraryVariationIdentifier(int itineraryVariationIdentifier) {
		this.itineraryVariationIdentifier = itineraryVariationIdentifier;
	}

	public Date getFromDateOfPeriod() {
		return fromDateOfPeriod;
	}

	public void setFromDateOfPeriod(Date fromDateOfPeriod) {
		this.fromDateOfPeriod = fromDateOfPeriod;
	}

	public Date getToDateOfPeriod() {
		return toDateOfPeriod;
	}

	public void setToDateOfPeriod(Date toDateOfPeriod) {
		this.toDateOfPeriod = toDateOfPeriod;
	}

	public String getFromDateOfPeriodString() {
		Date now = new Date(System.currentTimeMillis());
		if (this.fromDateOfPeriod.before(now)) {
			this.fromDateOfPeriod = now;
		}
		return getDateString(this.fromDateOfPeriod);
	}

	public String getToDateOfPeriodString() {
		return getDateString(this.toDateOfPeriod);
	}

	/**
	 * Method will return String representation of date. date format : ddMMMyy eg: 08JUN05
	 * 
	 * @param date
	 * @return
	 */
	public static String getDateString(Date date) {

		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyy");
		return sdf.format(date).toUpperCase().trim();
	}

	public List<FlightScheduleLegDTO> getLegs() {
		return legs;
	}

	public void setLegs(List<FlightScheduleLegDTO> legs) {
		this.legs = legs;
	}

	public Object clone() {
		try {
			ItineraryVariationScheduleMessageDTO cloned = (ItineraryVariationScheduleMessageDTO) super.clone();
			cloned.fromDateOfPeriod = (Date) fromDateOfPeriod.clone();
			cloned.toDateOfPeriod = (Date) toDateOfPeriod.clone();
			cloned.itineraryVariationIdentifier = itineraryVariationIdentifier;
			cloned.legs = cloneLegList(legs);
			return cloned;
		} catch (CloneNotSupportedException e) {
			System.out.println(e);
			return null;
		}
	}

	public static List<FlightScheduleLegDTO> cloneLegList(List<FlightScheduleLegDTO> list) {
		List<FlightScheduleLegDTO> clone = new ArrayList<FlightScheduleLegDTO>(list.size());
		for (FlightScheduleLegDTO item : list)
			clone.add((FlightScheduleLegDTO) item.clone());
		return clone;
	}
}
