package com.isa.thinair.gdsservices.api.dto.internal;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

public class AddSegmentRequest extends GDSReservationRequestBase {

	private static final long serialVersionUID = 4239321565105914795L;

	private Collection<SegmentDetail> segmentDetails;

	private Collection<TempSegBcAllocDTO> blockedSeats;

	private PaymentDetail paymentDetail;

	public AddSegmentRequest() {
		segmentDetails = new ArrayList<SegmentDetail>();
	}

	/**
	 * @return the segmentDetails
	 */
	public Collection<SegmentDetail> getSegmentDetails() {
		return segmentDetails;
	}

	/**
	 * @param segmentDetails
	 *            the segmentDetails to set
	 */
	public void setSegmentDetails(Collection<SegmentDetail> segmentDetails) {
		this.segmentDetails = segmentDetails;
	}

	@Override
	public ReservationAction getActionType() {
		return GDSInternalCodes.ReservationAction.ADD_SEGMENT;
	}

	/**
	 * @return the paymentDetail
	 */
	public PaymentDetail getPaymentDetail() {
		return paymentDetail;
	}

	/**
	 * @param paymentDetail
	 *            the paymentDetail to set
	 */
	public void setPaymentDetail(PaymentDetail paymentDetail) {
		this.paymentDetail = paymentDetail;
	}

	public Collection<TempSegBcAllocDTO> getBlockedSeats() {
		return blockedSeats;
	}

	public void setBlockedSeats(Collection<TempSegBcAllocDTO> blockedSeats) {
		this.blockedSeats = blockedSeats;
	}
}
