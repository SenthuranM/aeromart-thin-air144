package com.isa.thinair.gdsservices.api.dto.external;

public class OSIEmailDTO extends OSIDTO {
	
	private static final long serialVersionUID = -5964097038543641348L;

	private String email;

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
}
