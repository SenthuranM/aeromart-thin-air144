package com.isa.thinair.gdsservices.api.dto.internal;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

public class CreateReservationRequest extends GDSReservationRequestBase {

	private static final long serialVersionUID = 1174768241701468205L;

	private Collection<SegmentDetail> newSegmentDetails;

	private Collection<Segment> externalSegments;

	private Collection<Passenger> passengers;

	private ContactDetail contactDetail;

	private Collection<OtherServiceInfo> osis;

	private String userNotes;

	private PaymentDetail paymentDetail;
	
	private Collection<TempSegBcAllocDTO> blockedSeats;

	private Date paymentTimeLimit;

	/**
	 * @return the newSegmentDetails
	 */
	public Collection<SegmentDetail> getNewSegmentDetails() {
		return newSegmentDetails;
	}

	/**
	 * @param newSegmentDetails
	 *            the newSegmentDetails to set
	 */
	public void setNewSegmentDetails(Collection<SegmentDetail> segmentDetails) {
		this.newSegmentDetails = segmentDetails;
	}

	/**
	 * @return the externalSegments
	 */
	public Collection<Segment> getExternalSegments() {
		return externalSegments;
	}

	/**
	 * @param externalSegments
	 *            the externalSegments to set
	 */
	public void setExternalSegments(Collection<Segment> externalSegments) {
		this.externalSegments = externalSegments;
	}

	/**
	 * @return the passengers
	 */
	public Collection<Passenger> getPassengers() {
		return passengers;
	}

	/**
	 * @param passengers
	 *            the passengers to set
	 */
	public void setPassengers(Collection<Passenger> passengers) {
		this.passengers = passengers;
	}

	/**
	 * @return the contactDetail
	 */
	public ContactDetail getContactDetail() {
		return contactDetail;
	}

	/**
	 * @param contactDetail
	 *            the contactDetail to set
	 */
	public void setContactDetail(ContactDetail contactDetail) {
		this.contactDetail = contactDetail;
	}

	@Override
	public ReservationAction getActionType() {
		return GDSInternalCodes.ReservationAction.CREATE_BOOKING;
	}

	/**
	 * @return the osis
	 */
	public Collection<OtherServiceInfo> getOsis() {
		return osis;
	}

	/**
	 * @param osis
	 *            the osis to set
	 */
	public void setOsis(Collection<OtherServiceInfo> osis) {
		this.osis = osis;
	}

	/**
	 * @return the total passenger counts in an Integer Array : (0)-Adults, (1)-Children, (2)-Infants
	 */
	public int[] getTotalPaxCounts() {
		int totAdults = 0;
		int totChildren = 0;
		int totInfants = 0;

		if (passengers == null) {
			return new int[] { 0, 0, 0 };
		}

		for (Iterator iter = passengers.iterator(); iter.hasNext();) {
			Passenger pax = (Passenger) iter.next();
			if (pax.getPaxType().equals(ReservationInternalConstants.PassengerType.ADULT)) {
				totAdults++;
			} else if (pax.getPaxType().equals(ReservationInternalConstants.PassengerType.CHILD)) {
				totChildren++;
			} else if (pax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {
				totInfants++;
			}
		}
		return new int[] { totAdults, totChildren, totInfants };
	}

	/**
	 * @return the userNotes
	 */
	public String getUserNotes() {
		return userNotes;
	}

	/**
	 * @param userNotes
	 *            the userNotes to set
	 */
	public void setUserNotes(String userNotes) {
		this.userNotes = userNotes;
	}

	/**
	 * @return the creditCardDetail
	 */
	public PaymentDetail getPaymentDetail() {
		return paymentDetail;
	}

	/**
	 * @param paymentDetail
	 *            the paymentDetail to set
	 */
	public void setPaymentDetail(PaymentDetail paymentDetail) {
		this.paymentDetail = paymentDetail;
	}
	
	/**
	 * @return the blockedSeats
	 */
	public Collection<TempSegBcAllocDTO> getBlockedSeats() {
		return blockedSeats;
	}
	
	/**
	 * @param blockedSeats the blockedSeats to set
	 */
	public void setBlockedSeats(Collection<TempSegBcAllocDTO> blockedSeats) {
		this.blockedSeats = blockedSeats;
	}

	public Date getPaymentTimeLimit() {
		return paymentTimeLimit;
	}

	public void setPaymentTimeLimit(Date paymentTimeLimit) {
		this.paymentTimeLimit = paymentTimeLimit;
	}
}
