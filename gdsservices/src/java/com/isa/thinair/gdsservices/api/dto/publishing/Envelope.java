package com.isa.thinair.gdsservices.api.dto.publishing;

import java.io.Serializable;
import java.util.HashMap;

public class Envelope implements Serializable {

	private static final long serialVersionUID = 1L;
	private String targetLogic;

	private HashMap<String, Object> messageParams;

	public Envelope() {
	}

	public Envelope(String targetLogic, HashMap<String, Object> messageParams) {
		this.targetLogic = targetLogic;
		this.messageParams = messageParams;
	}

	public Envelope(String targetLogic) {
		this.targetLogic = targetLogic;
	}

	public String getTargetLogic() {
		return targetLogic;
	}

	public void setTargetLogic(String targetLogic) {
		this.targetLogic = targetLogic;
	}

	public HashMap<String, Object> getMessageParams() {
		return messageParams;
	}

	public void setMessageParams(HashMap<String, Object> messageParams) {
		this.messageParams = messageParams;
	}

	public void addParam(String key, Object param) {
		if (this.messageParams == null)
			this.messageParams = new HashMap<String, Object>();
		this.messageParams.put(key, param);
	}
}
