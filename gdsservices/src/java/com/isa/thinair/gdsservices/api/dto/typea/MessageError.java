package com.isa.thinair.gdsservices.api.dto.typea;

import com.isa.thinair.gdsservices.api.util.GdsApplicationError;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MessageError implements Serializable {

	private List<GdsApplicationError> errors;
	private List<String> errorCodes;

	public MessageError() {
		errors = new ArrayList<>();
		errorCodes = new ArrayList<>();
	}

	public List<GdsApplicationError> getErrors() {
		return errors;
	}

	public List<String> getErrorCodes() {
		return errorCodes;
	}

	public void addError(GdsApplicationError error) {
		errors.add(error);
	}
	public void addError(String error) {
		errorCodes.add(error);
	}

	public boolean isErroneous() {
        return ! (errors.isEmpty() && errorCodes.isEmpty());
	}
}
