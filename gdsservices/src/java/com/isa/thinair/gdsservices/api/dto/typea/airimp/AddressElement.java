package com.isa.thinair.gdsservices.api.dto.typea.airimp;

import java.io.Serializable;

public class AddressElement implements Serializable {

	private static final long serialVersionUID = 1L;

	private String cityAirport;
	private FunctionEnum function;
	private String airline;

	public enum FunctionEnum {

		AvailabilityStatus("RI"), PassengerReservations("RM"), PrepaidTicketAdvice("RP"),

		/** Undefined */
		UNDEFINED("");

		private final String value;

		private FunctionEnum(String value) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}

		public static FunctionEnum getEnum(String value) {
			if (value != null) {
				for (FunctionEnum enumSingle : FunctionEnum.values()) {
					if (enumSingle.value.compareTo(value) == 0)
						return enumSingle;
				}
			}

			return UNDEFINED;
		}
	}

	public String getCityAirport() {
		return cityAirport;
	}

	public void setCityAirport(String cityAirport) {
		this.cityAirport = cityAirport;
	}

	public FunctionEnum getFunction() {
		return function;
	}

	public void setFunction(FunctionEnum function) {
		this.function = function;
	}

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}
}