package com.isa.thinair.gdsservices.api.dto.external;

import java.util.Date;

/**
 * DTO class will hold SSR FOID information
 * 
 * @author sanjeewaf
 */
public class SSRChildDTO extends SSRDTO {

	private static final long serialVersionUID = 6203936745144442509L;

	private String title;

	private String firstName;

	private String lastName;

	private Date dateOfBirth;

	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth
	 *            the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the paxTitle
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param paxTitle
	 *            the paxTitle to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
