package com.isa.thinair.gdsservices.api.dto.external;

import java.util.Date;
import java.util.List;

/**
 * Class contains the further breakdown of passenger passport details which comes as a SSR element.
 * 
 * @author sanjeewaf
 */
public class SSRPassportDTO extends SSRDTO {

	private static final long serialVersionUID = -3206921056149140122L;

	/** passengers passport number */
	private String passengerPassportNo;

	/** one to three letter iso code of passport issuing country */
	private String passportCountryCode;

	/** passengers birth date, day month year of birth */
	private Date passengerDateOfBirth;

	/** family name as it appears on the passport */
	private String passengerFamilyName;

	/** given and middle name as it appears on the passport */
	private String passengerGivenName;

	/** gender of passenger, use m-male,f-female,i-infant,fi,mi */
	private String passengerGender;

	/** denote whether this is a multi passenger passport */
	private boolean multiPassengerPassport;

	/** PNR name dto */
	private List<NameDTO> pnrNameDTOs;

	/**
	 * @return the passengerDateOfBirth
	 */
	public Date getPassengerDateOfBirth() {
		return passengerDateOfBirth;
	}

	/**
	 * @param passengerDateOfBirth
	 *            the passengerDateOfBirth to set
	 */
	public void setPassengerDateOfBirth(Date passengerDateOfBirth) {
		this.passengerDateOfBirth = passengerDateOfBirth;
	}

	/**
	 * @return the passengerFamilyName
	 */
	public String getPassengerFamilyName() {
		return passengerFamilyName;
	}

	/**
	 * @param passengerFamilyName
	 *            the passengerFamilyName to set
	 */
	public void setPassengerFamilyName(String passengerFamilyName) {
		this.passengerFamilyName = passengerFamilyName;
	}

	/**
	 * @return the passengerGender
	 */
	public String getPassengerGender() {
		return passengerGender;
	}

	/**
	 * @param passengerGender
	 *            the passengerGender to set
	 */
	public void setPassengerGender(String passengerGender) {
		this.passengerGender = passengerGender;
	}

	/**
	 * @return the passengerGivenName
	 */
	public String getPassengerGivenName() {
		return passengerGivenName;
	}

	/**
	 * @param passengerGivenName
	 *            the passengerGivenName to set
	 */
	public void setPassengerGivenName(String passengerGivenName) {
		this.passengerGivenName = passengerGivenName;
	}

	/**
	 * @return the passengerPassportNo
	 */
	public String getPassengerPassportNo() {
		return passengerPassportNo;
	}

	/**
	 * @param passengerPassportNo
	 *            the passengerPassportNo to set
	 */
	public void setPassengerPassportNo(String passengerPassportNo) {
		this.passengerPassportNo = passengerPassportNo;
	}

	/**
	 * @return the passportCountryCode
	 */
	public String getPassportCountryCode() {
		return passportCountryCode;
	}

	/**
	 * @param passportCountryCode
	 *            the passportCountryCode to set
	 */
	public void setPassportCountryCode(String passportCountryCode) {
		this.passportCountryCode = passportCountryCode;
	}

	/**
	 * @return the multiPassengerPassport
	 */
	public boolean isMultiPassengerPassport() {
		return multiPassengerPassport;
	}

	/**
	 * @param multiPassengerPassport
	 *            the multiPassengerPassport to set
	 */
	public void setMultiPassengerPassport(boolean multiPassengerPassport) {
		this.multiPassengerPassport = multiPassengerPassport;
	}

	/**
	 * @return the pnrNameDTOs
	 */
	public List<NameDTO> getPnrNameDTOs() {
		return pnrNameDTOs;
	}

	/**
	 * @param pnrNameDTOs
	 *            the pnrNameDTOs to set
	 */
	public void setPnrNameDTOs(List<NameDTO> pnrNameDTOs) {
		this.pnrNameDTOs = pnrNameDTOs;
	}
}
