package com.isa.thinair.gdsservices.api.dto.internal;

import java.math.BigDecimal;

public class PriceDetail extends GDSBase {
	private static final long serialVersionUID = 6433536401542757908L;

	private BigDecimal amount;

	/**
	 * @return Returns the amount.
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
