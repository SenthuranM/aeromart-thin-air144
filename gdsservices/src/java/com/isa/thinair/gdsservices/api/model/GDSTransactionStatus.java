package com.isa.thinair.gdsservices.api.model;

import java.sql.Blob;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * Model to represent the current GDS TypeA transaction status+
 * 
 * @author mekanayake
 * @hibernate.class table = "T_GDS_TRANSACTIONAL_RESPRO"
 */
public class GDSTransactionStatus extends Persistent {

	private static final long serialVersionUID = 8960788177519223282L;

	private Long id;

	private Date txnStartTime;

	private String gdsTransactionRef;

	private String tnxStage;

	private Blob txnData;

	public GDSTransactionStatus() {
		super();
	}

	public GDSTransactionStatus(Date txnStartTime, String gdsTransactionRef, Date lastMsgReqTime, String tnxStage,
			Date lastMsgResTime) {
		this();
		this.txnStartTime = txnStartTime;
		this.gdsTransactionRef = gdsTransactionRef;
		this.tnxStage = tnxStage;
	}

	/**
	 * @return id
	 * 
	 * @hibernate.id column = "GDS_TXN_RESPRO_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_GDS_TRANSACTIONAL_RESPRO"
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return the transaction start time.
	 * 
	 * @hibernate.property column = "START_TIMESTAMP"
	 */
	public Date getTxnStartTime() {
		return txnStartTime;
	}

	/**
	 * @param txnStartTime
	 */
	public void setTxnStartTime(Date txnStartTime) {
		this.txnStartTime = txnStartTime;
	}

	/**
	 * @return gdsTransactionRef
	 * 
	 * @hibernate.property column = "GDS_TXN_REFERENCE"
	 */
	public String getGdsTransactionRef() {
		return gdsTransactionRef;
	}

	/**
	 * @param gdsTransactionRef
	 */
	public void setGdsTransactionRef(String gdsTransactionRef) {
		this.gdsTransactionRef = gdsTransactionRef;
	}

	/**
	 * @return tnxStage.
	 * 
	 * @hibernate.property column = "TRANSACTION_STAGE"
	 */
	public String getTnxStage() {
		return tnxStage;
	}

	/**
	 * @param tnxStage
	 */
	public void setTnxStage(String tnxStage) {
		this.tnxStage = tnxStage;
	}

	/**
	 * @hibernate.property column = "TRANSACTION_DATA"
	 */
	public Blob getTxnData() {
		return txnData;
	}

	public void setTxnData(Blob txnData) {
		this.txnData = txnData;
	}
}