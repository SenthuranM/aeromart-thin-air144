package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;
import java.util.List;

public class UnchangedNamesDTO implements Serializable {

	private static final long serialVersionUID = -8647219080340399107L;

	/** Previous values of names* */
	private List<NameDTO> unChangedNames;

	/** no of pax included in the party */
	private int partyTotal;

	/**
	 * @return the oldNameDTOs
	 */
	public List<NameDTO> getUnChangedNames() {
		return unChangedNames;
	}

	/**
	 * @param oldNameDTOs
	 *            the oldNameDTOs to set
	 */
	public void setUnChangedNames(List<NameDTO> oldNameDTOs) {
		this.unChangedNames = oldNameDTOs;
	}

	/**
	 * @return the partyTotal
	 */
	public int getPartyTotal() {
		return partyTotal;
	}

	/**
	 * @param partyTotal
	 *            the partyTotal to set
	 */
	public void setPartyTotal(int partyTotal) {
		this.partyTotal = partyTotal;
	}

}
