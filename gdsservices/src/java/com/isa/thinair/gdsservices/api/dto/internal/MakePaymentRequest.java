package com.isa.thinair.gdsservices.api.dto.internal;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

public class MakePaymentRequest extends GDSReservationRequestBase {

	private static final long serialVersionUID = 5960275985731240471L;

	@Override
	public ReservationAction getActionType() {
		return ReservationAction.MAKE_PAYMENT;
	}

	private PaymentDetail paymentDetail;

	/**
	 * @return the paymentDetail
	 */
	public PaymentDetail getPaymentDetail() {
		return paymentDetail;
	}

	/**
	 * @param paymentDetail
	 *            the paymentDetail to set
	 */
	public void setPaymentDetail(PaymentDetail paymentDetail) {
		this.paymentDetail = paymentDetail;
	}

}
