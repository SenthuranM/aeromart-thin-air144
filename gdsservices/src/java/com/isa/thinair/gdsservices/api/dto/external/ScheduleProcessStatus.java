/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.gdsservices.api.dto.external;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * @author M.Rikaz
 * 
 */
public class ScheduleProcessStatus extends SSMMessageProcessStatus implements Serializable {

	private static final long serialVersionUID = 6213438435959278397L;

	private Date startDate;

	private Date endDate;

	private Frequency frequency;

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Frequency getFrequency() {
		return frequency;
	}

	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append("ScheduleProcessStatus ");
		sb.append(super.toString());

		if (startDate != null && endDate != null) {
			sb.append(" Period From:" + CalendarUtil.formatDate(startDate, CalendarUtil.PATTERN1) + " To:"
					+ CalendarUtil.formatDate(endDate, CalendarUtil.PATTERN1));
		}

		if (frequency != null) {
			sb.append(" " + frequency);
		}

		return sb.toString();
	}

}
