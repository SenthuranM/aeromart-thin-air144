package com.isa.thinair.gdsservices.api.dto.typea;

import java.util.Date;

import com.isa.thinair.gdsservices.api.dto.typea.codeset.CodeSetEnum;

public class TVLDetail {

	private Date departureTimeStart;

	private Date departureTimeEnd;

	private Date arrivalTime;

	private String from;

	private String to;

	private String carrierCode;

	private String flightNumber;

	private String rbd;

	private int adultCount;

	private int childCount;

	private int infantCount;

	private String bookingClass;

	private String cabinClass;

	private String posAirport;

	private CodeSetEnum.CS4405 actionCoded;

	public void setDepartureTimeStart(Date departureTimeStart) {
		this.departureTimeStart = departureTimeStart;
	}

	public Date getDepartureTimeStart() {
		return departureTimeStart;
	}

	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	/**
	 * @return the departureTimeEnd
	 */
	public Date getDepartureTimeEnd() {
		return departureTimeEnd;
	}

	/**
	 * @param departureTimeEnd
	 *            the departureTimeEnd to set
	 */
	public void setDepartureTimeEnd(Date departureTimeEnd) {
		this.departureTimeEnd = departureTimeEnd;
	}

	public Date getArrivalTime() {
		return arrivalTime;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getFrom() {
		return from;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getTo() {
		return to;
	}

	public void setRBD(String rbd) {
		this.rbd = rbd;
	}

	public String getRbd() {
		return rbd;
	}

	/**
	 * @return the adultCount
	 */
	public int getAdultCount() {
		return adultCount;
	}

	/**
	 * @param adultCount
	 *            the adultCount to set
	 */
	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	/**
	 * @return the childCount
	 */
	public int getChildCount() {
		return childCount;
	}

	/**
	 * @param childCount
	 *            the childCount to set
	 */
	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	/**
	 * @return the infantCount
	 */
	public int getInfantCount() {
		return infantCount;
	}

	/**
	 * @param infantCount
	 *            the infantCount to set
	 */
	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}

	public void setActionCoded(CodeSetEnum.CS4405 actionCoded) {
		this.actionCoded = actionCoded;
	}

	public CodeSetEnum.CS4405 getActionCoded() {
		return actionCoded;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @return the carrierCode
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            the carrierCode to set
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the bookingClass
	 */
	public String getBookingClass() {
		return bookingClass;
	}

	/**
	 * @param bookingClass
	 *            the bookingClass to set
	 */
	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	/**
	 * @return the cabinClass
	 */
	public String getCabinClass() {
		return cabinClass;
	}

	/**
	 * @param cabinClass
	 *            the cabinClass to set
	 */
	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	/**
	 * @return the posAirport
	 */
	public String getPosAirport() {
		return posAirport;
	}

	/**
	 * @param posAirport
	 *            the posAirport to set
	 */
	public void setPosAirport(String posAirport) {
		this.posAirport = posAirport;
	}

	public String getId() {
		StringBuilder sb = new StringBuilder();
		sb.append(from).append(to).append(departureTimeStart.getTime());
		return sb.toString();
	}

	public String getSegmentCode() {
		return from.concat("/").concat(to);
	}

}
