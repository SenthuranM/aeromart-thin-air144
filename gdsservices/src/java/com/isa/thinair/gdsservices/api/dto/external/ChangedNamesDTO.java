package com.isa.thinair.gdsservices.api.dto.external;

import java.util.List;

/**
 * 
 * @author Chan
 */
public class ChangedNamesDTO extends NameDTO {

	private static final long serialVersionUID = 6485296473426586167L;

	/** Previous values of names* */
	private List<NameDTO> oldNameDTOs = null;

	/** Hold new changed names * */
	private List<NameDTO> newNameDTOs = null;

	/**
	 * @return the oldNameDTOs
	 */
	public List<NameDTO> getOldNameDTOs() {
		return oldNameDTOs;
	}

	/**
	 * @param oldNameDTOs
	 *            the oldNameDTOs to set
	 */
	public void setOldNameDTOs(List<NameDTO> oldNameDTOs) {
		this.oldNameDTOs = oldNameDTOs;
	}

	/**
	 * @return the newNameDTOs
	 */
	public List<NameDTO> getNewNameDTOs() {
		return newNameDTOs;
	}

	/**
	 * @param newNameDTOs
	 *            the newNameDTOs to set
	 */
	public void setNewNameDTOs(List<NameDTO> newNameDTOs) {
		this.newNameDTOs = newNameDTOs;
	}
}
