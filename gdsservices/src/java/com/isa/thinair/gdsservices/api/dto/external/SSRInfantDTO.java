package com.isa.thinair.gdsservices.api.dto.external;

import java.util.Date;

import com.isa.thinair.gdsservices.api.util.GDSApiUtils;

/**
 * DTO class will hold SSR FOID information
 * 
 * @author sanjeewaf
 */
public class SSRInfantDTO extends SSRDTO {

	private static final long serialVersionUID = 6203936745144442509L;

	private String infantTitle;

	private String infantFirstName;

	private String infantLastName;

	private String guardianFirstName;

	private String guardianLastName;

	private String guardianTitle;

	private SegmentDTO segmentDTO;

	private boolean seatRequired;

	private String infantAge;
	
	private Date infantDateofBirth;
	
	private boolean isNameChangeRelated;
	
	private boolean isRemovePaxRelated;

	/**
	 * @return the parentName
	 */
	public String getIATAGuardianName() {
		return GDSApiUtils.getIATAName(getGuardianLastName(), getGuardianFirstName(), getGuardianTitle());
	}
	
	public String getIATAInfantName () {
		return GDSApiUtils.getIATAName(getInfantLastName(), getInfantFirstName(), getInfantTitle());
	}

	/**
	 * @return the infantTitle
	 */
	public String getInfantTitle() {
		return infantTitle;
	}

	/**
	 * @param infantTitle
	 *            the infantTitle to set
	 */
	public void setInfantTitle(String infantTitle) {
		this.infantTitle = infantTitle;
	}

	/**
	 * @return the infantFirstName
	 */
	public String getInfantFirstName() {
		return infantFirstName;
	}

	/**
	 * @param infantFirstName
	 *            the infantFirstName to set
	 */
	public void setInfantFirstName(String infantFirstName) {
		this.infantFirstName = infantFirstName;
	}

	/**
	 * @return the infantLastName
	 */
	public String getInfantLastName() {
		return infantLastName;
	}

	/**
	 * @param infantLastName
	 *            the infantLastName to set
	 */
	public void setInfantLastName(String infantLastName) {
		this.infantLastName = infantLastName;
	}

	/**
	 * @return the guardianFirstName
	 */
	public String getGuardianFirstName() {
		return guardianFirstName;
	}

	/**
	 * @param guardianFirstName
	 *            the guardianFirstName to set
	 */
	public void setGuardianFirstName(String guardianFirstName) {
		this.guardianFirstName = guardianFirstName;
	}

	/**
	 * @return the guardianLastName
	 */
	public String getGuardianLastName() {
		return guardianLastName;
	}

	/**
	 * @param guardianLastName
	 *            the guardianLastName to set
	 */
	public void setGuardianLastName(String guardianLastName) {
		this.guardianLastName = guardianLastName;
	}

	/**
	 * @return the guardianTitle
	 */
	public String getGuardianTitle() {
		return guardianTitle;
	}

	/**
	 * @param guardianTitle
	 *            the guardianTitle to set
	 */
	public void setGuardianTitle(String guardianTitle) {
		this.guardianTitle = guardianTitle;
	}

	public SegmentDTO getSegmentDTO() {
		return segmentDTO;
	}

	public void setSegmentDTO(SegmentDTO segmentDTO) {
		this.segmentDTO = segmentDTO;
	}

	/**
	 * @return the seatRequired
	 */
	public boolean isSeatRequired() {
		return seatRequired;
	}

	/**
	 * @param seatRequired
	 *            the seatRequired to set
	 */
	public void setSeatRequired(boolean seatRequired) {
		this.seatRequired = seatRequired;
	}

	/**
	 * @return the infantAge
	 */
	public String getInfantAge() {
		return infantAge;
	}

	/**
	 * @param infantAge
	 *            the infantAge to set
	 */
	public void setInfantAge(String infantAge) {
		this.infantAge = infantAge;
	}

	public Date getInfantDateofBirth() {
		return infantDateofBirth;
	}

	public void setInfantDateofBirth(Date infantDateofBirth) {
		this.infantDateofBirth = infantDateofBirth;
	}

	public boolean isNameChangeRelated() {
		return isNameChangeRelated;
	}

	public void setNameChangeRelated(boolean isNameChangeRelated) {
		this.isNameChangeRelated = isNameChangeRelated;
	}

	public boolean isRemovePaxRelated() {
		return isRemovePaxRelated;
	}

	public void setRemovePaxRelated(boolean isRemovePaxRelated) {
		this.isRemovePaxRelated = isRemovePaxRelated;
	}
}
