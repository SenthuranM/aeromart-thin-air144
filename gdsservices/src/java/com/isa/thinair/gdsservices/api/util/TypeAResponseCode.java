package com.isa.thinair.gdsservices.api.util;

/**
 * Type A response code container.
 */
public abstract class TypeAResponseCode {

	public static final String AVAILABILITY_SEARCH_RESULT = "gds.typea.availability.search.success";

	public static final String SPECIFICFLIGHT_SCHEDULE_SUCCSESSFULL = "gds.typea.specificFlightSchedule.success";

	public static final String INVENTORY_ADJUSTMENT_RESPONSE = "gds.typea.inventory.adjustment.response";

	/**
	 * The reservation list related key.
	 */
	public static final String TICKET_DISPLAY_RESULTS = "gds.typea.ticket.display.list.response";

}
