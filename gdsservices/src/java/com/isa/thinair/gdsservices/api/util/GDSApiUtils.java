package com.isa.thinair.gdsservices.api.util;

import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.dto.FlightAllowActionDTO;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlight;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.DataConverterUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.gdsservices.api.dto.external.BookingSegmentDTO;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMResponse;
import com.isa.thinair.gdsservices.api.dto.external.SSMASMSUBMessegeDTO;
import com.isa.thinair.gdsservices.api.dto.external.SegmentDTO;
import com.isa.thinair.gdsservices.api.dto.internal.Segment;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ProcessStatus;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ResponseMessageCode;
import com.isa.thinair.gdsservices.core.util.MessageUtil;

import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GDSApiUtils {

	private static final String PATTERN_TOKEN_1 = "[A-Z]{1}+";
	private static final String PATTERN_TOKEN_2 = "[A-Z]{1}[0-9]*";
	private static final String PATTERN_TOKEN_3 = "[0-9]{1,3}+";
	private static final String AIRCRAFT_TYPE = "AIRCRAFT";
	private static final String PARAM_YES = "Y";
	private static final String LOCAL_TIME = "LT";
	private static final String TIME_MSG = "TIM";

	public static String maskNull(Object object) {
		if (object == null) {
			return "";
		} else {
			return object.toString().trim();
		}
	}

	public static String getIATAName(String lasName, String firstName, String title) {
		return GDSApiUtils.maskNull(lasName) + GDSApiUtils.maskNull(firstName) + GDSApiUtils.maskNull(title);
	}

	public static String getSegmentCode(String departureStation, String arrivalStation) {
		return GDSApiUtils.maskNull(departureStation) + "/" + GDSApiUtils.maskNull(arrivalStation);
	}

	public static String composeProcessHistory(Collection<ProcessStatus> processStatusHistory) {
		String history = "";

		if (processStatusHistory != null && processStatusHistory.size() > 0) {

			for (ProcessStatus processStatus : processStatusHistory) {
				if (history.length() == 0) {
					history = processStatus.getValue();
				} else {
					history = history + "->" + processStatus.getValue();
				}
			}
		}

		return history;
	}

	public static boolean isEqual(String element1, String element2) {
		element1 = GDSApiUtils.maskNull(element1);
		element2 = GDSApiUtils.maskNull(element2);

		if (element1.equals(element2)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isEqual(Date element1, Date element2) {
		if (element1 != null && element2 != null) {
			return element1.compareTo(element2) == 0;
		} else {
			return false;
		}
	}

	public static boolean isEqual(BookingSegmentDTO bookingSegmentDTO, Segment segment) {
		List<String> flightNumberAlternatives = SegmentUtil.getPrioratizedFlightNumbers(bookingSegmentDTO.getCarrierCode() + bookingSegmentDTO.getFlightNumber());
		if (bookingSegmentDTO.getDepartureStation().equals(segment.getDepartureStation())
				&& bookingSegmentDTO.getDestinationStation().equals(segment.getArrivalStation())
				&& flightNumberAlternatives.contains(segment.getFlightNumber())
				// assume same flight number does not repeat in same day
				&& getStartTimeOfDate(bookingSegmentDTO.getDepartureDate()).compareTo(
						getStartTimeOfDate(segment.getDepartureDate())) == 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isEqual(SegmentDTO segmentDTO1, SegmentDTO segmentDTO2) {
		if (segmentDTO1.getDepartureDate() != null && segmentDTO2 != null && segmentDTO2.getDepartureDate() != null
				&& segmentDTO1.getDepartureDate().compareTo(segmentDTO2.getDepartureDate()) == 0) {
			String thisFlightNumber = GDSApiUtils.maskNull(segmentDTO1.getFlightNumber());
			String flightNumber = GDSApiUtils.maskNull(segmentDTO2.getFlightNumber());
			int actualLenght = thisFlightNumber.length();
			int ssrFlightNumberLength = flightNumber.length();
			int difference = ssrFlightNumberLength - actualLenght;
			String actualFlightNumber = "";
			
			if (thisFlightNumber.length() != flightNumber.length() &&  difference > 0) {
				actualFlightNumber = flightNumber.substring(difference);
			} else {
				actualFlightNumber = flightNumber;
			}

			if (isFlightNoEqaul(thisFlightNumber, actualFlightNumber)) {
				String thisDepStation = GDSApiUtils.maskNull(segmentDTO1.getDepartureStation());
				String depStation = GDSApiUtils.maskNull(segmentDTO2.getDepartureStation());

				if (thisDepStation.equals(depStation)) {
					String thisArrivalStation = GDSApiUtils.maskNull(segmentDTO1.getDestinationStation());
					String arrivalStation = GDSApiUtils.maskNull(segmentDTO2.getDestinationStation());

					if (thisArrivalStation.equals(arrivalStation)) {
						if (segmentDTO1.getBookingCode() != null && segmentDTO2.getBookingCode() != null) {
							if (segmentDTO1.getBookingCode().equals(segmentDTO2.getBookingCode())) {
								return true;
							}
						} else {
							return true;
						}
					}
				}
			}
		}

		return false;
	}

	/**
	 * checks whether given flight#s are equal
	 * 
	 * @param flightNumber1
	 * @param flightNumber2
	 * @return
	 */
	private static boolean isFlightNoEqaul(String flightNumber1, String flightNumber2) {
		flightNumber1 = formatFlightNo(flightNumber1);
		flightNumber2 = formatFlightNo(flightNumber2);

		return flightNumber1.equals(flightNumber2);
	}

	/**
	 * formats the flight no
	 * 
	 * @param flightNumber
	 * @return
	 */
	private static String formatFlightNo(String flightNumber) {
		flightNumber = maskNull(flightNumber);
		flightNumber = (flightNumber.substring(0, 1).equals("0")) ? flightNumber.substring(1) : flightNumber;
		return flightNumber;
	}

	public static Date getStartTimeOfDate(Date date) {
		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(date);

		calander.set(GregorianCalendar.HOUR_OF_DAY, 0);
		calander.set(GregorianCalendar.MINUTE, 0);
		calander.set(GregorianCalendar.SECOND, 0);
		calander.set(GregorianCalendar.MILLISECOND, 0);

		return calander.getTime();
	}

	public static Blob toBlob(Object object) {
		Blob blob = null;
		ByteArrayOutputStream baos = null;
		ObjectOutputStream oos = null;
		try {
			baos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(baos);
			oos.writeObject(object);
			blob = DataConverterUtil.createBlob((baos.toByteArray()));

		} catch (Exception e) {
		} finally {
			try {
				oos.close();
			} catch (Exception e) {
			}

			try {
				baos.close();
			} catch (Exception e) {
			}
		}

		return blob;
	}

	public static Object toObject(Blob blob) {
		Object object = null;
		ObjectInputStream ois = null;

		try {
			ois = new ObjectInputStream(blob.getBinaryStream());
			object = ois.readObject();
		} catch (Exception e) {
		} finally {
			try {
				ois.close();
			} catch (Exception e) {
			}
		}

		return object;
	}

	public static GDSExternalCodes.GDS getGDSCode(Integer gdsId) {
		Map<String, GDSStatusTO> gdsStatusMap = ReservationModuleUtils.getGlobalConfig().getActiveGdsMap();
		String gdsCode = gdsStatusMap.get(gdsId.toString()).getGdsCode();
		for (GDSExternalCodes.GDS gdsName : GDSExternalCodes.GDS.values()) {
			if (gdsName.getCode().equals(gdsCode)) {
				return gdsName;
			}
		}
		return null;
	}
	
	public static String getGDSCarrierCode(Integer gdsId) {
		Map<String, GDSStatusTO> gdsStatusMap = ReservationModuleUtils.getGlobalConfig().getActiveGdsMap();
		return gdsStatusMap.get(gdsId.toString()).getCarrierCode();
	}
	
	public static GDSExternalCodes.GDS getGDSCode(String carrierCode) {
		Map<String, GDSStatusTO> gdsStatusMap = ReservationModuleUtils.getGlobalConfig().getActiveGdsMap();
		for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
			if (gdsStatusTO.getCarrierCode().equals(carrierCode)) {
				return GDSExternalCodes.GDS.valueOf(gdsStatusTO.getGdsCode());
			}
		}
		return null;
	}
	
	public static void validateSsmAsmRequest(SSMASMSUBMessegeDTO ssmASMSubMsgsDTO) throws ModuleException {
		if (ssmASMSubMsgsDTO.getLegs() == null || ssmASMSubMsgsDTO.getLegs().isEmpty()) {
			throw new ModuleException("gdsservices.airschedules.logic.bl.leg.data.invalid");
		}
	}
	
	public static String getAAToGDSErrorMessages(String responseCode) {
		String errorMessage = null;
		if (!StringUtil.isNullOrEmpty(responseCode)) {
			String gdsErrorCode = null;

			if (ResponceCodes.INVALID_SCHEDULE_FREQUENCE_FOR_DATE_RANGE.equalsIgnoreCase(responseCode)) {
				gdsErrorCode = ResponseMessageCode.INVALID_DATE_RANGE.getCode();
			} else if (ResponceCodes.ALL_SEGMENTS_INVALID.equalsIgnoreCase(responseCode)) {
				gdsErrorCode = ResponseMessageCode.ALL_SEGMENTS_INVALID.getCode();
			} else if (ResponceCodes.INVALID_LEG_DURATION.equalsIgnoreCase(responseCode)) {
				gdsErrorCode = ResponseMessageCode.INVALID_LEG_DURATION.getCode();
			} else if (ResponceCodes.SCHEDULE_SCHEDULE_CONFLICT.equalsIgnoreCase(responseCode)) {
				gdsErrorCode = ResponseMessageCode.SCHEDULE_SCHEDULE_CONFLICT.getCode();
			} else if (ResponceCodes.SCHEDULE_FLIGHT_CONFLICT.equalsIgnoreCase(responseCode)) {
				gdsErrorCode = ResponseMessageCode.SCHEDULE_FLIGHT_CONFLICT.getCode();
			} else if (ResponceCodes.INVALID_OVERLAPPING_SEGMENT_LEG.equalsIgnoreCase(responseCode)) {
				gdsErrorCode = ResponseMessageCode.INVALID_OVERLAPPING_SEGMENT_LEG.getCode();
			} else if (ResponceCodes.WARNING_FOR_FLIGHT_UPDATE.equalsIgnoreCase(responseCode)) {
				gdsErrorCode = ResponseMessageCode.FLIGHT_CREATION_UPDATION_FAILED.getCode();
			} else if (ResponceCodes.RESERVATIONS_FOUND_FOR_LEGCHANGED_FLIGHT.equalsIgnoreCase(responseCode)) {
				gdsErrorCode = ResponseMessageCode.RESERVATIONS_FOUND_FOR_LEGCHANGED_FLIGHT.getCode();
			} else if ("airmaster.route.routenotfound".equalsIgnoreCase(responseCode)) {
				gdsErrorCode = ResponseMessageCode.SCHEDULE_FLIGHT_ROUTE_NOT_DEFINED.getCode();
			}

			if (gdsErrorCode != null)
				errorMessage = MessageUtil.getMessage(gdsErrorCode);
		}

		if ("Message code set is null or does not exists".equalsIgnoreCase(errorMessage)
				|| StringUtil.isNullOrEmpty(errorMessage)) {
			errorMessage = responseCode;
		}

		return errorMessage;
	}
	
	public static FlightAlertDTO generateFlightAlertDTO(boolean isCancelAction) {

		GlobalConfig globalConfig = GDSServicesModuleUtil.getGlobalConfig();

		boolean blnEmailAlert = globalConfig.getBizParam(SystemParamKeys.SEND_EMAILS_FOR_FLIGHT_ALTERATIONS).equalsIgnoreCase("Y")
				? true
				: false;
		boolean blnSmsAlert = globalConfig.getBizParam(SystemParamKeys.SEND_SMS_FOR_FLIGHT_ALTERATIONS).equalsIgnoreCase("Y")
				? true
				: false;

		boolean blnEmailAlertForScheduleMsg = AppSysParamsUtil.sendEmailForFlightAlterationByScheduleMessages();

		boolean blnSmsAlertForScheduleMsg = AppSysParamsUtil.sendSMSForFlightAlterationByScheduleMessages();

		FlightAlertDTO fltAltDTO = new FlightAlertDTO();

		if (blnSmsAlert && blnSmsAlertForScheduleMsg) {

			boolean blnSmsCnf = globalConfig.getBizParam(SystemParamKeys.SMS_FOR_CONFIRMED_BOOKINGS).equalsIgnoreCase("Y") ? true
					: false;
			boolean blnSmsOnH = globalConfig.getBizParam(SystemParamKeys.SMS_FOR_ON_HOLD_BOOKINGS).equalsIgnoreCase("Y") ? true
					: false;
			if (blnSmsCnf || blnSmsOnH) {
				fltAltDTO.setSendSMSForConfirmedBookings(blnSmsCnf);
				fltAltDTO.setSendSMSForOnHoldBookings(blnSmsOnH);
				if (isCancelAction) {
					fltAltDTO.setSendSMSForRescheduledFlight(true);
				} else {
					fltAltDTO.setSendSMSForFlightAlterations(true);
				}
			}
		}

		fltAltDTO.setAlertForRescheduledFlight(true);
		fltAltDTO.setAlertForCancelledFlight(true);

		if (blnEmailAlert && blnEmailAlertForScheduleMsg) {
			fltAltDTO.setSendEmailsForFlightAlterations(true);
			if (isCancelAction) {
				fltAltDTO.setEmailForCancelledFlight(true);
			} else {
				fltAltDTO.setEmailForRescheduledFlight(true);
			}
		}

		return fltAltDTO;
	}

	public static String getActiveAircraftModelNumberByIataType(String iataType, String aircraftVersion) throws ModuleException {

		List<AircraftModel> aircraftModels = GDSServicesModuleUtil.getAircraftBD().getAllActiveAircraftsByIataType(iataType);

		return getMostAccurateAircraftVersion(aircraftModels, aircraftVersion);
	}
	
	public static void addErrorResponse(SSMASMResponse ssimResponse, Exception e) {
		ssimResponse.setSuccess(false);
		if (e instanceof ModuleException) {
			ModuleException me = (ModuleException) e;
			if (StringUtil.isNullOrEmpty(ssimResponse.getResponseMessage())) {
				if (me.getModuleCode() != null) {
					String msg = MessageUtil.getMessage(me.getModuleCode());
					if (!StringUtil.isNullOrEmpty(me.getExceptionCode())) {
						ssimResponse.setResponseMessage(getAAToGDSErrorMessages(me.getExceptionCode()));
					} else if (!StringUtil.isNullOrEmpty(msg)) {
						ssimResponse.setResponseMessage(msg);
					} else {
						ssimResponse.setResponseMessage(me.getMessageString() + me.getModuleCode());
					}
				} else {
					ssimResponse.setResponseMessage(me.getMessageString());
				}
			}

			if ("adhoc.flight.update".equals(me.getExceptionCode())) {
				ssimResponse.setAdHocFlightUpdate(true);
			}

			if ("Message code set is null or does not exists".equalsIgnoreCase(ssimResponse.getResponseMessage())
					&& (!StringUtil.isNullOrEmpty(me.getExceptionCode()) || !StringUtil.isNullOrEmpty(me.getMessageString()))) {
				String msg = me.getExceptionCode();
				if (StringUtil.isNullOrEmpty(msg) && !StringUtil.isNullOrEmpty(me.getMessageString())) {
					msg = me.getMessageString();
				}
				ssimResponse.setResponseMessage(msg);
			}

		} else {
			ssimResponse.setResponseMessage(MessageUtil.getDefaultErrorMessage() + " - " + e.getMessage());
		}
	}
	
	public static FlightAllowActionDTO getFlightAllowActionDTO() {
		FlightAllowActionDTO fltAllowDTO = new FlightAllowActionDTO();
		fltAllowDTO.setAllowConflicts(true);
		fltAllowDTO.setAllowAnyDuration(true);
		fltAllowDTO.setAllowOverlapForAnyCC(true);
		if (AppSysParamsUtil.enableDowngradeAircraftModel()) {
			fltAllowDTO.setAllowModelForAnyCC(true);
			fltAllowDTO.setAllowSegmentV2I(true);
		}

		return fltAllowDTO;
	}
	
	public static String composeMarketingFlightDesignator(String flightDesignator, String marketingFlightPrefix,
			String csCarrierCode) {
		String marketingFlightDesignator = null;
		if (!StringUtil.isNullOrEmpty(flightDesignator) && !StringUtil.isNullOrEmpty(marketingFlightPrefix)) {
			String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();
			flightDesignator = flightDesignator.replaceFirst(csCarrierCode, "");
			if (flightDesignator.length() >= 4) {
				flightDesignator = flightDesignator.substring(flightDesignator.length() - 3, flightDesignator.length());
			}

			if (!StringUtil.isNullOrEmpty(flightDesignator)) {
				marketingFlightDesignator = flightDesignator.trim();
				marketingFlightDesignator = String.format("%04d", Integer.parseInt(marketingFlightDesignator));
				marketingFlightDesignator = marketingFlightDesignator.replaceFirst("\\d", marketingFlightPrefix);
				marketingFlightDesignator = carrierCode + "" + marketingFlightDesignator;
			}

		}

		return marketingFlightDesignator;
	}
	
	public static String getIATAServiceTypeForAAOperationId(int operationId, boolean isAdHocFlight) {
		String serviceType = null;
		if (AirScheduleCustomConstants.OperationTypes.STANDARD == operationId) {
			if (isAdHocFlight) {
				serviceType = "G";
			} else {
				serviceType = "J";
			}
		} else {
			Map<Integer, List<String>> serviceTypesByOperationType = GDSServicesModuleUtil.getGlobalConfig()
					.getServiceTypesByOperationType();

			if (serviceTypesByOperationType != null && !serviceTypesByOperationType.isEmpty()) {
				List<String> serviceTypes = serviceTypesByOperationType.get(operationId);
				if (serviceTypes != null && !serviceTypes.isEmpty()) {
					serviceType = serviceTypes.get(0);
				}

			}
		}

		return serviceType;
	}
	
	public static int getAAOperationIdForIATAServiceType(String serviceType) throws ModuleException {

		Map<Integer, List<String>> serviceTypesByOperationType = GDSServicesModuleUtil.getGlobalConfig()
				.getServiceTypesByOperationType();

		if (serviceTypesByOperationType != null && !serviceTypesByOperationType.isEmpty()) {
			for (Integer operationId : serviceTypesByOperationType.keySet()) {
				List<String> serviceTypes = serviceTypesByOperationType.get(operationId);
				if (serviceTypes != null && serviceTypes.contains(serviceType)) {
					return operationId;
				}
			}

		}

		throw new ModuleException("gdsservices.response.message.service.type.code.invalid");
	}

	public static HashSet<CodeShareMCFlightSchedule> getCodeShareMCFlightSchedules(List<String> marketingFlightNos) {

		HashSet<CodeShareMCFlightSchedule> codeShareMCFlightSchedules = new HashSet<CodeShareMCFlightSchedule>();
		if (marketingFlightNos != null && !marketingFlightNos.isEmpty()) {
			for (String csMCFlightNumber : marketingFlightNos) {
				CodeShareMCFlightSchedule csMCFlightSchedule = new CodeShareMCFlightSchedule();
				csMCFlightSchedule.setCsMCFlightNumber(csMCFlightNumber);
				csMCFlightSchedule.setCsMCCarrierCode(csMCFlightNumber.substring(0, 2));
				codeShareMCFlightSchedules.add(csMCFlightSchedule);
			}
		}
		return codeShareMCFlightSchedules;
	}

	public static HashSet<CodeShareMCFlight> getCodeShareMCFlights(List<String> marketingFlightNos) {

		HashSet<CodeShareMCFlight> codeShareMCFlights = new HashSet<CodeShareMCFlight>();
		if (marketingFlightNos != null && !marketingFlightNos.isEmpty()) {
			for (String csMCFlightNumber : marketingFlightNos) {
				CodeShareMCFlight csMCFlightSchedule = new CodeShareMCFlight();
				csMCFlightSchedule.setCsMCFlightNumber(csMCFlightNumber);
				csMCFlightSchedule.setCsMCCarrierCode(csMCFlightNumber.substring(0, 2));
				codeShareMCFlights.add(csMCFlightSchedule);
			}
		}
		return codeShareMCFlights;
	}
	
	private static List<String> getMatchedItems(String PATTERN_TOKEN, String version) {

		List<String> chunks = new LinkedList<String>();
		if (!StringUtil.isNullOrEmpty(PATTERN_TOKEN) && !StringUtil.isNullOrEmpty(version)) {
			Pattern VALID_PATTERN = Pattern.compile(PATTERN_TOKEN);
			Matcher matcher = VALID_PATTERN.matcher(version);
			while (matcher.find()) {
				String ss = matcher.group();
				chunks.add(ss);
			}
		}

		return chunks;
	}
	
	private static String getMostAccurateAircraftVersion(List<AircraftModel> aircraftModels, String aircraftVersion)
			throws ModuleException {

		String defaultModel = null;
		String exactModel = null;
		String similarModel = null;
		String capacityOnly = null;
		String currentModel = null;

		boolean isAircraft = true;

		if (aircraftModels != null && !aircraftModels.isEmpty()) {

			List<String> capacityList = getMatchedItems(PATTERN_TOKEN_2, aircraftVersion);
			List<String> cabinList = getMatchedItems(PATTERN_TOKEN_1, aircraftVersion);
			List<String> capacityOnlyList = null;

			AC: for (AircraftModel aircraftModel : aircraftModels) {
				currentModel = aircraftModel.getModelNumber();
				if (PARAM_YES.equalsIgnoreCase(aircraftModel.getDefaultIataAircraft())) {
					defaultModel = currentModel;
				}

				Set<AircraftCabinCapacity> aircraftcapacitys = aircraftModel.getAircraftCabinCapacitys();
				if (!StringUtil.isNullOrEmpty(aircraftVersion) && aircraftcapacitys != null && !aircraftcapacitys.isEmpty()) {

					if (!AIRCRAFT_TYPE.equalsIgnoreCase(aircraftModel.getAircraftTypeCode()) && capacityOnlyList == null) {
						capacityOnlyList = getMatchedItems(PATTERN_TOKEN_3, aircraftVersion);
						isAircraft = false;
					}

					String versionCode = null;
					String cabinCode = null;
					String capacity = null;
					List<String> aaModelCapacity = new ArrayList<String>();
					List<String> aaModelCabins = new ArrayList<String>();
					List<String> aaCapacityOnly = new ArrayList<String>();
					for (AircraftCabinCapacity cabinCapacity : aircraftcapacitys) {
						cabinCode = cabinCapacity.getCabinClassCode();
						capacity = cabinCapacity.getSeatCapacity() + "";
						versionCode = cabinCode + "" + capacity;
						aaModelCapacity.add(versionCode);
						aaModelCabins.add(cabinCode);
						aaCapacityOnly.add(capacity);
					}

					if (aaModelCapacity.containsAll(capacityList)) {
						exactModel = currentModel;
						// exact match
						break AC;
					}

					if (aaModelCabins.containsAll(cabinList)) {

						// same cabins
						similarModel = currentModel;
					}

					if (!isAircraft && capacityOnlyList != null && !capacityOnlyList.isEmpty()
							&& aaCapacityOnly.containsAll(capacityOnlyList)) {
						capacityOnly = currentModel;
					}

				}

			}
		}

		String modelNumber = null;
		if (!StringUtil.isNullOrEmpty(exactModel)) {
			// cabin & capacity matches
			modelNumber = exactModel;
		} else if (!isAircraft && !StringUtil.isNullOrEmpty(capacityOnly)) {
			// capacity matches
			modelNumber = capacityOnly;
		} else if (!StringUtil.isNullOrEmpty(similarModel)) {
			// cabin matches
			modelNumber = similarModel;
		} else if (!StringUtil.isNullOrEmpty(defaultModel)) {
			// no matches so preference given for DEFAULT
			modelNumber = defaultModel;
		} else if (!StringUtil.isNullOrEmpty(currentModel)) {
			// no matches & no DEFAULT defined so last model//random
			modelNumber = currentModel;
		}

		return modelNumber;
	}

	/**
	 * This is allowed only for CNL & TIM msgs
	 * when you are changing time for SSM it should be one flight in the schedule, this is not
	 	allowed to change the time range
	 	if you give part of the past schedule the time change will not change
	 *
	 * @param ssmASMSUBMessegeDTO
	 * @param action
	 * @throws ModuleException
	 */
	public static void validateScheduleDate(SSMASMSUBMessegeDTO ssmASMSUBMessegeDTO, String action)
			throws ModuleException {

		Date scheduleDate = null;
		boolean isOperationDatesEqual = false;
		if (GDSExternalCodes.MessageType.SSM.equals(ssmASMSUBMessegeDTO.getMessageType())) {
			scheduleDate = ssmASMSUBMessegeDTO.getEndDate();
			if (TIME_MSG.equals(action)) {
				isOperationDatesEqual = CalendarUtil
						.isSameDay(ssmASMSUBMessegeDTO.getEndDate(), ssmASMSUBMessegeDTO.getStartDate());
			}

		} else if (GDSExternalCodes.MessageType.ASM.equals(ssmASMSUBMessegeDTO.getMessageType())) {
			scheduleDate = ssmASMSUBMessegeDTO.getStartDate();
		}
		if (scheduleDate != null) {
			Date currentDate = CalendarUtil.getCurrentZuluDateTime();
			if (LOCAL_TIME.equals(ssmASMSUBMessegeDTO.getTimeMode())) {
				currentDate = new Date();
			}

			if (!CalendarUtil.isSameDay(scheduleDate, currentDate) && CalendarUtil.isLessThan(scheduleDate, currentDate)
					&& !isNeedModifyPastFlightDetails(ssmASMSUBMessegeDTO.getMessageType(), scheduleDate, currentDate,
					action, isOperationDatesEqual)) {

				if (GDSExternalCodes.MessageType.SSM.equals(ssmASMSUBMessegeDTO.getMessageType())) {
					throw new ModuleException("gdsservices.response.message.period.of.operation.invalid");
				}
				throw new ModuleException("gdsservices.response.message.departure.date.invalid");
			}
		}

	}

	/**
	 * Check whether past flight is need to be modified or not
	 * Where SSM messages(TIM) are processed only if operation from and to dates are same for past flight
	 *
	 * @param scheduleDate
	 * @param currentDate
	 * @return
	 */
	private static boolean isNeedModifyPastFlightDetails(GDSExternalCodes.MessageType msgIdentifier, Date scheduleDate,
			Date currentDate, String action, boolean isSameDay) {
		boolean isWithinActiveTimeDuration = FlightUtil.isWithinActiveTimeDuration(scheduleDate, currentDate);
		boolean isAllowModificationForPastFlight = AppSysParamsUtil.isAllowedModificationForPastFlight();
		boolean isContainAllowableMsgTypes = false;
		boolean isAllowProcessTIMMsgs = true;
		List<String> msgTypes = AppSysParamsUtil.getAllowableMsgTypes();
		if (msgTypes.contains(SSMASMSUBMessegeDTO.Action.CNL.toString()) || msgTypes
				.contains(SSMASMSUBMessegeDTO.Action.TIM.toString())) {
			isContainAllowableMsgTypes = true;
		}

		if (GDSExternalCodes.MessageType.SSM.equals(msgIdentifier) && (TIME_MSG.equals(action)) && !isSameDay) {
			isAllowProcessTIMMsgs = false;
		}

		return isWithinActiveTimeDuration && isAllowModificationForPastFlight && isContainAllowableMsgTypes
				&& isAllowProcessTIMMsgs;
	}

}
