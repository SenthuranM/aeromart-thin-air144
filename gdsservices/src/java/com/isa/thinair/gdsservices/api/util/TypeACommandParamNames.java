package com.isa.thinair.gdsservices.api.util;

/**
 * class to hold all the command parameters relating to GDS TypeA integration
 * 
 * @author mekanayake
 */
public interface TypeACommandParamNames {

	// Commands param common to all the Messages
	public static final String IC_HEADER = "interchangeControlHeader";
	public static final String MESSAGE_HEADER = "messageHeader";
	public static final String MESSAGE_FUNCTION = "messageFunction";
	public static final String RESERVATION = "reservation";
	public static final String EXTERNAL_COMPANY_ID = "externalCompanyId";
	public static final String E_TICKET_MAP = "eTicketMap";
	public static final String OND_FARE_DTOS = "ondFareDto";
	public static final String USER_PRINCIPLE = "userPrinciple";
	public static final String EXCEPTION_CODE = "errorOrException";
	public static final String PAX_TYPE = "paxType";
	public static final String PAX_PAYMENT = "paxPayment";
	public static final String TICKET_DTO = "ticketDto";
	public static final String TICKET_CONTROL_DTO = "ticketControlDto";

	@Deprecated
	public static final String COMMON_ACCESS_REFERENCE = "commonAccessRef";

	public static final String TVL_DTOS = "TVLDtos";
	public static final String APPLICATION_ERROR = "applicationError";
	public static final String SPECIFIC_FLIGHT_DETAIL_LIST = "SpecificFlightDetailRequestDTOs";
	public static final String CABIN_CLASS_AVAILABILITY_DTO_LIST = "CabinClassAvailabilityDTOs";
	public static final String INVENTORYTORY_ADJUSTMENT_DTO_LIST = "InventorytoryAdjustmentDTOs";

	/** Ticket Display Command Parameters - Start */
	public static final String ETICKET_NUMBER_LIST = "eTicketNumberList";
	public static final String TICKET_SEARCH = "ticketSearch";
	public static final String AIRLINE_CONFIRMATION_NUMBER = "airlineConfirmationNumber";
	public static final String TICKET_DISPLAY_SEARCH_CRITERIA = "ticketDisplaySearchCriteria";
	/** Ticket Display Command Parameters - End */
	
	public static final String EDI_REQUEST_DTO = "EDIRequest";
	public static final String EDI_RESPONSE_DTO = "EDIResponse";
	public static final String ROLLBACK_TRANSACTION = "RollbackTransaction";
	public static final String EDI_RESP_ASSOCIATION_CODE = "EDIRespAssociationCode";
	public static final String EDI_PROCESS_STATUS = "ProcessStatus";
	public static final String USER_PRINCIPAL = "UserPrincipal";
	public static final String EDI_NOTIFICATION = "EDINotification";
	public static final String EDI_NOTIFICATION_REPLY = "EDINotificationReply";
	public static final String PPFS_ID = "PPFS_ID";
	public static final String IATA_OPERATION = "IataOperation";
	public static final String IATA_EVENT = "IataEvent";

}