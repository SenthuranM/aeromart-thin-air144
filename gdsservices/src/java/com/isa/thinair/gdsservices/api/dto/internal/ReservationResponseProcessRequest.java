package com.isa.thinair.gdsservices.api.dto.internal;

import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes.ReservationAction;

public class ReservationResponseProcessRequest extends CreateReservationRequest {
	
	private static final long serialVersionUID = -893740256287561460L;

	@Override
	public ReservationAction getActionType() {
		return GDSInternalCodes.ReservationAction.RESERVATION_RESP;
	}

}
