package com.isa.thinair.gdsservices.api.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.isa.thinair.commons.api.exception.ModuleException;

public class DateUtil {

	/**
	 * Parse java.util.Date to javax.xml.datatype.XMLGregorianCalendar
	 * 
	 * @param date
	 * @return
	 * @throws DatatypeConfigurationException
	 * @throws ModuleException
	 */
	public static XMLGregorianCalendar parse(Date date) throws DatatypeConfigurationException {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return getXMLGregorianCalendar(cal);
	}

	/**
	 * Parse java.util.Date to javax.xml.datatype.XMLGregorianCalendar with only Time included
	 * 
	 * @param date
	 * @return
	 * @throws DatatypeConfigurationException
	 * @throws ModuleException
	 */
	public static XMLGregorianCalendar parseTime(Date date) throws DatatypeConfigurationException {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return getXMLGregorianCalendarTime(cal);
	}

	/**
	 * Prepare javax.xml.datatype.XMLGregorianCalendar conforming to %Y-%M-%DT%h:%m%s format from java.util.Calendar
	 * 
	 * @param cal
	 *            Calendar
	 * @return XMLGregorianCalendar conforms to %Y-%M-%DT%h:%m%s format [example=2006-12-20T10:30:00]
	 * @throws DatatypeConfigurationException
	 * @throws ModuleException
	 */
	public static XMLGregorianCalendar getXMLGregorianCalendar(Calendar cal) throws DatatypeConfigurationException {
		DatatypeFactory factory = DatatypeFactory.newInstance();
		return factory.newXMLGregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE),
				cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND),
				DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED);

	}

	/**
	 * Prepare javax.xml.datatype.XMLGregorianCalendar conforming to h:%m%s format from java.util.Calendar
	 * 
	 * @param cal
	 *            Calendar
	 * @return XMLGregorianCalendar conforms to %Y-%M-%DT%h:%m%s format [example=2006-12-20T10:30:00]
	 * @throws DatatypeConfigurationException
	 * @throws ModuleException
	 */
	public static XMLGregorianCalendar getXMLGregorianCalendarTime(Calendar cal) throws DatatypeConfigurationException {
		DatatypeFactory factory = DatatypeFactory.newInstance();
		return factory.newXMLGregorianCalendarTime(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),
				cal.get(Calendar.SECOND), DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED);

	}

	public static long getTime(XMLGregorianCalendar xmlCalendar) {
		return xmlCalendar.toGregorianCalendar().getTimeInMillis();
	}

	public static String[] constructDatesForSql(Date fromDate, Date toDate) throws ModuleException {
		SimpleDateFormat newDateFormat = new SimpleDateFormat("dd-MMM-yyyy");

		String dtFrom = newDateFormat.format(fromDate);
		String dtEnd = newDateFormat.format(toDate);

		dtFrom = "'" + dtFrom + " 00:00:00'" + ", " + "'dd-mon-yyyy HH24:mi:ss'";
		dtEnd = "'" + dtEnd + " 23:59:59'" + ", " + "'dd-mon-yyyy HH24:mi:ss'";
		return new String[] { dtFrom, dtEnd };
	}

	/**
	 * Format a given date to string.
	 * 
	 * @param utilDate
	 * @param fmt
	 * @return
	 */
	public static String formatDate(Date utilDate, String fmt) {
		String formatedDate = null;

		if (utilDate != null) {
			SimpleDateFormat sdFmt = new SimpleDateFormat(fmt);
			;
			formatedDate = sdFmt.format(utilDate);
		}

		return formatedDate;
	}
}
