package com.isa.thinair.crypto.core.service.bd;

import com.isa.thinair.crypto.core.bl.PBEWithMD5AndDESImpl;
import com.isa.thinair.platform.core.controller.ModuleFramework;

public class EncryptDecrypt {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ModuleFramework.startup();
		
		System.out.println(new PBEWithMD5AndDESImpl().encrypt("q!u@a#r$t%z"));
		
		System.out.println(new PBEWithMD5AndDESImpl().decrypt("mTWt/Meku02VUcHrRtOYug=="));

	}

}
