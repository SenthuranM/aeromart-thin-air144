/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.crypto.core.service.bd;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.crypto.api.service.CryptoServiceBD;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

import junit.framework.TestCase;

/**
 * @author Chamindap
 *
 */
public class TestCryptoServiceBDImpl extends TestCase {

	/**
	 *  Sets up the environment.
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/**
	 * Tear down method of TestCustomerDAOImpl class.
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for 'com.isa.thinair.crypto.core.service.bd
	 * .CryptoServiceBDImpl.encrypt(String)'
	 * @throws ModuleException 
	 */
	public void testEncrypt() throws ModuleException {	
		String encryptText = null;
		String ciphetText = null;		
		String decryptText = null;		
		CryptoServiceBD cryptoServiceBD = null;
		
		encryptText = "CryptoGraphy";	
		
		cryptoServiceBD = getCryptoServiceBD();		
		assertNotNull(cryptoServiceBD);		
		ciphetText = cryptoServiceBD.encrypt(encryptText);
		decryptText = cryptoServiceBD.decrypt(ciphetText);		
		assertEquals("Incorrect Plan text", encryptText, decryptText);
		
	}

	/**
	 * Test method for 'com.isa.thinair.crypto.core.service.bd
	 * .CryptoServiceBDImpl.decrypt(String)'
	 * @throws ModuleException 
	 */
	public void testDecrypt() throws ModuleException {	
		String encryptText = null;
		String ciphetText = null;		
		String decryptText = null;		
		CryptoServiceBD cryptoServiceBD = null;
		
		encryptText = "CryptoGraphy";
		cryptoServiceBD = getCryptoServiceBD();		
		assertNotNull(cryptoServiceBD);		
		ciphetText = cryptoServiceBD.encrypt(encryptText);		
		assertEquals("Incorrect Cipher text", "Y7xEJ6f1N9LqpRSC/KeHyg==", ciphetText);
		
		decryptText = cryptoServiceBD.decrypt("V44hsmFe28GHHW1qhwioaA==");
		System.out.println(decryptText);
		assertEquals("Incorrect Plan text", encryptText, decryptText);
	}
	
	/**
	 * Private method to get CryptoServiceBD reference.
	 * @return CryptoServiceBD
	 */
	private CryptoServiceBD getCryptoServiceBD() { 
		LookupService lookup = LookupServiceFactory.getInstance();
		IModule cryptoModule = lookup.getModule("crypto");
	System.out.println("Crypto module looked up successful ............");
		CryptoServiceBD cryptoService = null;
		try {
			cryptoService = (CryptoServiceBD) cryptoModule
				.getServiceBD("crypto.service.local");				
	System.out.println("Crypto module bd creation successful...........");
			return cryptoService;
			
		} catch (Exception e) {
			System.out.println("Exception in creating delegate");
			e.printStackTrace();
		}
		return cryptoService;
	}
}
