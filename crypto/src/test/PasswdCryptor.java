

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

public class PasswdCryptor {

	private static final String DEF_CHARSET = "UTF8";

	private static final String ALGORITHM = "PBEWithMD5AndDES";

	int _iterationCount = 20;

	private Cipher _oECipher = null;

	private Cipher _oDCipher = null;

	private byte[] _salt = { (byte) 0xc7, (byte) 0x9B, (byte) 0xee,
			(byte) 0x32, (byte) 0x7e, (byte) 0x34, (byte) 0x21, (byte) 0x03 };

	private static final String DEF_RESOURCE_PATH_WIN32 = "c:/crypt.properties";

	private static final String DEF_RESOURCE_PATH_UNIX = "/usr/crypt.properties";
	
	private static final String DEF_CYPHER_KEY = "security";

	private String resourcePath = null;

	private String cypherKey = "security";
	

	/**
	 * @param args
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public static void main(String[] args) throws FileNotFoundException,
			IOException, ClassNotFoundException, SQLException {
		System.out.println(" - Launching ..........");
		PasswdCryptor pc = new PasswdCryptor(System.getProperty("crypt.file"));
		if (args.length > 0) {
				if(args[0].equalsIgnoreCase("-e")){
					String userPrinciple = args[1];
					if(args.length>2 && args[2].equalsIgnoreCase("-k")){
						pc._init(args[3]);	
					}
					else {
						pc._init(DEF_CYPHER_KEY);
					}
					String cyper = pc.encrypt(userPrinciple);
					System.out.println(cyper);
				
				}
				else if(args[0].equalsIgnoreCase("-d")){
					String userCredential = args[1];
					if(args.length>2 && args[2].equalsIgnoreCase("-k")){
						pc._init(args[3]);	
					}
					else {
						pc._init(DEF_CYPHER_KEY);
					}
					String cyper = pc.decrypt(userCredential);
					System.out.println(cyper);
				}
				if(args[0].equalsIgnoreCase("deencrypt")){
					doDbDeEncrypt();					
				}
		}
		else {
			pc.doDbCrypt();
		}
		

	}

	/**
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * 
	 * 
	 */
	private void doDbCrypt() throws FileNotFoundException, IOException,
			ClassNotFoundException, SQLException {
		Properties prop = new Properties();
		System.out.println(" - Initiating DBCrypt ..........");
		prop.load(new FileInputStream(resourcePath));
		String cryptTable = prop.getProperty("crypt.table");
		String cryptTableKey = prop.getProperty("crypt.table.key");
		String cryptPlainColumn = prop.getProperty("crypt.plain.column");
		String cryptCypherColumn = prop.getProperty("crypt.cypher.column");
		//this.setCypherKey(prop.getProperty("crypt.key"));
		this._init(prop.getProperty("crypt.key"));
		
		Connection con = getConnection(prop.getProperty("jdbc.dirver"), prop
				.getProperty("jdbc.url"), prop.getProperty("jdbc.user"), prop
				.getProperty("jdbc.password"));
		String selSql = "SELECT * FROM " + cryptTable;
		PreparedStatement pstmt = con.prepareStatement(selSql);
		ResultSet rs = pstmt.executeQuery();
		String updateSql = "UPDATE " + cryptTable + " SET " + cryptCypherColumn
				+ "=? WHERE " + cryptTableKey + "=?";
		while (rs.next()) {
			String cryptPlain = rs.getString(cryptPlainColumn);
			System.out.println(" - Crypting credential of user , "
					+ rs.getString(cryptTableKey));
			if (cryptPlain != null) {
				PreparedStatement pstmt2 = con.prepareStatement(updateSql);
				//pstmt2.setString(1, cryptTable);
				//pstmt2.setString(2, cryptCypherColumn);
				pstmt2.setString(1, encrypt(cryptPlain));
				//pstmt2.setString(4, cryptTableKey);
				pstmt2.setString(2, rs.getString(cryptTableKey));
				pstmt2.executeUpdate();
				pstmt2.close();
			}
		}
		rs.close();
		pstmt.close();
		con.close();

	}

	/**
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * 
	 * 
	 */
	private static void doDbDeEncrypt() throws FileNotFoundException, IOException,
			ClassNotFoundException, SQLException {
		Properties prop = new Properties();
		System.out.println(" - Initiating DBDeEncrypt ..........");
		prop.load(new FileInputStream(getResourcePath()));
		String cryptTable = prop.getProperty("crypt.table");
		String cryptTableKey = prop.getProperty("crypt.table.key");		
		String cryptCypherColumn = prop.getProperty("crypt.cypher.column");
		String cryptCypherKey = prop.getProperty("crypt.key");
		String cryptCypherNewKey = prop.getProperty("crypt.key.new");
		
		PasswdCryptor pcDe = new PasswdCryptor(null);
		pcDe._init(cryptCypherKey);
		PasswdCryptor pcEn = new PasswdCryptor(null);
		pcEn._init(cryptCypherNewKey);
		
		
		//this.setCypherKey(prop.getProperty("crypt.key"));
		//this._init(prop.getProperty(cryptCypherKey));
		
		Connection con = getConnection(prop.getProperty("jdbc.dirver"), prop
				.getProperty("jdbc.url"), prop.getProperty("jdbc.user"), prop
				.getProperty("jdbc.password"));
		String selSql = "SELECT * FROM " + cryptTable;
		PreparedStatement pstmt = con.prepareStatement(selSql);
		ResultSet rs = pstmt.executeQuery();
		String updateSql = "UPDATE " + cryptTable + " SET " + cryptCypherColumn
				+ "=? WHERE " + cryptTableKey + "=?";
		while (rs.next()) {
			String dbCurrCypher = rs.getString(cryptCypherColumn);
			System.out.println(" - Crypting credential of user , "
					+ rs.getString(cryptTableKey));
			if (dbCurrCypher != null) {
				PreparedStatement pstmt2 = con.prepareStatement(updateSql);
				String dbCurrPlain = pcDe.decrypt(dbCurrCypher);
				//pstmt2.setString(1, cryptTable);
				//pstmt2.setString(2, cryptCypherColumn);
				pstmt2.setString(1, pcEn.encrypt(dbCurrPlain));
				//pstmt2.setString(4, cryptTableKey);
				pstmt2.setString(2, rs.getString(cryptTableKey));
				pstmt2.executeUpdate();
				pstmt2.close();
			}
		}
		rs.close();
		pstmt.close();
		con.close();

	}
	
	private static Connection getConnection(String driver, String url,
			String user, String passwd) throws ClassNotFoundException,
			SQLException {
		Class.forName(driver);
		return DriverManager.getConnection(url, user, passwd);
	}

	public PasswdCryptor(String resPath) {
		if (resPath != null) {
			resourcePath = resPath;
		} else {
			String os = System.getProperty("os.name");
			if (os.equalsIgnoreCase("Windows XP")) {
				resourcePath = DEF_RESOURCE_PATH_WIN32;
			} else {
				resourcePath = DEF_RESOURCE_PATH_UNIX;
			}
		}		
	}
	
	public static String getResourcePath() {
		String resPath = System.getProperty("crypt.file");
		if (resPath != null) {
			return  resPath;
		} else {
			String os = System.getProperty("os.name");
			if (os.equalsIgnoreCase("Windows XP")) {
				return DEF_RESOURCE_PATH_WIN32;
			} else {
				return DEF_RESOURCE_PATH_UNIX;
			}
		}		
	}

	private void _init(String key) {
		try {
			/*
			 * Provider sunJce = new com.sun.crypto.provider.SunJCE();
			 * Security.addProvider(sunJce);
			 */

			// CryptoConfig cryptoConfig = (CryptoConfig) CryptoModuleUtils
			// .getInstance().getModuleConfig();
			// String _sSecKey = cryptoConfig.getCrptoProperties().getProperty(
			// "security");
			String _sSecKey = key;

			if (_sSecKey == null) {
				_sSecKey = "";
			}

			String _sSecKeyAlgo = ALGORITHM;

			KeySpec _oKeySpec = new PBEKeySpec(_sSecKey.toCharArray());

			SecretKey _oKey = SecretKeyFactory.getInstance(_sSecKeyAlgo)
					.generateSecret(_oKeySpec);

			_oECipher = Cipher.getInstance(_oKey.getAlgorithm());
			_oDCipher = Cipher.getInstance(_oKey.getAlgorithm());

			AlgorithmParameterSpec _paramSpec = new PBEParameterSpec(_salt,
					_iterationCount);

			_oECipher.init(Cipher.ENCRYPT_MODE, _oKey, _paramSpec);
			_oDCipher.init(Cipher.DECRYPT_MODE, _oKey, _paramSpec);

		} catch (InvalidAlgorithmParameterException e) {
			// log.error(e.getMessage(), e);
			System.out.println(e.getMessage());
		} catch (InvalidKeySpecException e) {
			// log.error(e.getMessage(), e);
			System.out.println(e.getMessage());
		} catch (NoSuchPaddingException e) {
			// log.error(e.getMessage(), e);
			System.out.println(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			// log.error(e.getMessage(), e);
			System.out.println(e.getMessage());
		} catch (InvalidKeyException e) {
			// log.error(e.getMessage(), e);
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Method to encrypt a plainText.
	 * 
	 * @param plainText
	 *            the plainText.
	 * @return the ciperText.
	 * @see com.isa.thinair.crypto.core.bl.Crypto#encrypt(java.lang.String)
	 */
	public String encrypt(String plainText) {
		try {
			byte[] _encBytes = _oECipher.doFinal(plainText
					.getBytes(DEF_CHARSET));
			return new sun.misc.BASE64Encoder().encode(_encBytes);
		} catch (IllegalStateException e) {
			// /log.error(e);
			System.out.println(e.getMessage());
		} catch (IllegalBlockSizeException e) {
			// log.error(e);
			System.out.println(e.getMessage());
		} catch (BadPaddingException e) {
			// log.error(e);
			System.out.println(e.getMessage());
		} catch (UnsupportedEncodingException e) {
			// log.error(e);
			System.out.println(e.getMessage());
		}
		return plainText;
	}

	/**
	 * Method to decrypt a ciperText.
	 * 
	 * @param ciperText
	 *            the ciperText.
	 * @return the plainText.
	 * @see com.isa.thinair.crypto.core.bl.Crypto#decrypt(java.lang.String)
	 */
	public String decrypt(String cipherText) {
		try {
			byte[] _decBytes = new sun.misc.BASE64Decoder()
					.decodeBuffer(cipherText);
			return new String(_oDCipher.doFinal(_decBytes), DEF_CHARSET);
		} catch (IllegalStateException e) {
			// log.error(e);
			System.out.println(e.getMessage());
		} catch (IllegalBlockSizeException e) {
			// log.error(e);
			System.out.println(e.getMessage());
		} catch (BadPaddingException e) {
			// log.error(e);
			System.out.println(e.getMessage());
		} catch (IOException e) {
			// log.error(e);
			System.out.println(e.getMessage());
		}
		return cipherText;
	}

	public String getCypherKey() {
		return cypherKey;
	}

	public void setCypherKey(String cypherKey) {
		this.cypherKey = cypherKey;
	}
}
