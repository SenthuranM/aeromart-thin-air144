/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.crypto.core.bl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.Security;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.crypto.api.service.CryptoModuleUtils;
import com.isa.thinair.crypto.core.config.CryptoConfig;

/**
 * @author Chamindap
 * 
 */
public class PBEWithMD5AndDESImpl implements Crypto {

	private Log log = LogFactory.getLog(getClass());

	private static final String DEF_CHARSET = "UTF8";

	private static final String ALGORITHM = "PBEWithMD5AndDES";

	int _iterationCount = 20;

	private Cipher _oECipher = null;

	private Cipher _oDCipher = null;

	private byte[] _salt = { (byte) 0xc7, (byte) 0x9B, (byte) 0xee, (byte) 0x32, (byte) 0x7e, (byte) 0x34, (byte) 0x21,
			(byte) 0x03 };

	/**
	 * Assign the Default JCE Provider
	 */
	private void assignDefaultJCEProvider() {
		Provider[] providers = Security.getProviders();
		int intSunJCEIndex = 1;
		Provider jceProvider = null;
		int i = 0;

		for (Provider provider : providers) {
			i++;
			if (provider.getName().equals("SunJCE")) {
				jceProvider = provider;
				break;
			}
		}

		if (jceProvider != null && i != intSunJCEIndex) {
			Security.removeProvider(jceProvider.getName());
			Security.insertProviderAt(jceProvider, intSunJCEIndex);
		}
	}

	/**
	 * The constructor.
	 */
	public PBEWithMD5AndDESImpl() {
		super();
		assignDefaultJCEProvider();
		_init();
	}

	/**
	 * The init method.
	 */
	private void _init() {
		try {
			/*
			 * Provider sunJce = new com.sun.crypto.provider.SunJCE(); Security.addProvider(sunJce);
			 */

			CryptoConfig cryptoConfig = (CryptoConfig) CryptoModuleUtils.getInstance().getModuleConfig();
			String _sSecKey = cryptoConfig.getCrptoProperties().getProperty("security");

			if (_sSecKey == null) {
				_sSecKey = "";
			}

			String _sSecKeyAlgo = ALGORITHM;

			KeySpec _oKeySpec = new PBEKeySpec(_sSecKey.toCharArray());

			SecretKey _oKey = SecretKeyFactory.getInstance(_sSecKeyAlgo).generateSecret(_oKeySpec);

			_oECipher = Cipher.getInstance(_oKey.getAlgorithm());
			_oDCipher = Cipher.getInstance(_oKey.getAlgorithm());

			AlgorithmParameterSpec _paramSpec = new PBEParameterSpec(_salt, _iterationCount);

			_oECipher.init(Cipher.ENCRYPT_MODE, _oKey, _paramSpec);
			_oDCipher.init(Cipher.DECRYPT_MODE, _oKey, _paramSpec);

		} catch (InvalidAlgorithmParameterException e) {
			log.error(e.getMessage(), e);
		} catch (InvalidKeySpecException e) {
			log.error(e.getMessage(), e);
		} catch (NoSuchPaddingException e) {
			log.error(e.getMessage(), e);
		} catch (NoSuchAlgorithmException e) {
			log.error(e.getMessage(), e);
		} catch (InvalidKeyException e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * Method to encrypt a plainText.
	 * 
	 * @param plainText
	 *            the plainText.
	 * @return the ciperText.
	 * @see com.isa.thinair.crypto.core.bl.Crypto#encrypt(java.lang.String)
	 */
	public String encrypt(String plainText) {
		try {
			byte[] _encBytes = _oECipher.doFinal(plainText.getBytes(DEF_CHARSET));
			return new sun.misc.BASE64Encoder().encode(_encBytes);
		} catch (IllegalStateException e) {
			log.error(e);
		} catch (IllegalBlockSizeException e) {
			log.error(e);
		} catch (BadPaddingException e) {
			log.error(e);
		} catch (UnsupportedEncodingException e) {
			log.error(e);
		}
		return plainText;
	}

	/**
	 * Method to decrypt a ciperText.
	 * 
	 * @param ciperText
	 *            the ciperText.
	 * @return the plainText.
	 * @see com.isa.thinair.crypto.core.bl.Crypto#decrypt(java.lang.String)
	 */
	public String decrypt(String cipherText) {
		try {
			byte[] _decBytes = new sun.misc.BASE64Decoder().decodeBuffer(cipherText);
			return new String(_oDCipher.doFinal(_decBytes), DEF_CHARSET);
		} catch (IllegalStateException e) {
			log.error(e);
		} catch (IllegalBlockSizeException e) {
			log.error(e);
		} catch (BadPaddingException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		}
		return cipherText;
	}
}