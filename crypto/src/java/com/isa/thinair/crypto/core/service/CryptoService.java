/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.crypto.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * @author Nasly
 * @author Chamindap Crypto module's service interface
 * @isa.module.service-interface module-name="crypto" description="cryptography module"
 */
public class CryptoService extends DefaultModule {
}
