/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.crypto.core.config;

import java.util.Properties;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @author Nasly
 * @author Chamindap
 * @isa.module.config-bean
 */
public class CryptoConfig extends DefaultModuleConfig {
	private Properties crptoProperties;

	/**
	 * @return Returns the crptoProperties.
	 */
	public Properties getCrptoProperties() {
		return crptoProperties;
	}

	/**
	 * @param crptoProperties
	 *            The crptoProperties to set.
	 */
	public void setCrptoProperties(Properties crptoProperties) {
		this.crptoProperties = crptoProperties;
	}
}
