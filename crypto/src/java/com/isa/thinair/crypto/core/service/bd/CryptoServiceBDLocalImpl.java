/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.crypto.core.service.bd;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseServiceDelegate;
import com.isa.thinair.crypto.api.service.CryptoServiceBD;
import com.isa.thinair.crypto.core.bl.PBEWithMD5AndDESImpl;

/**
 * @author Chamindap
 * @isa.module.bd-impl id="crypto.service.local" description="Crypto Service Local Interface"
 */
public class CryptoServiceBDLocalImpl extends PlatformBaseServiceDelegate implements CryptoServiceBD {

	PBEWithMD5AndDESImpl cryptoService = null;

	/**
	 * The constructor.
	 */
	public CryptoServiceBDLocalImpl() {
		super();
	}

	/**
	 * Method to encrypt a plainText.
	 * 
	 * @param plainText
	 *            the plainText.
	 * @return the ciperText.
	 * @throws ModuleException .
	 * @see com.isa.thinair.crypto.api.service.CryptoServiceBD#encrypt(String)
	 */
	public String encrypt(String plainText) throws ModuleException {
		String cipherText = null;

		cryptoService = new PBEWithMD5AndDESImpl();
		cipherText = cryptoService.encrypt(plainText);

		return cipherText;
	}

	/**
	 * Method to decrypt a ciperText.
	 * 
	 * @param ciperText
	 *            the ciperText.
	 * @return the plainText.
	 * @throws ModuleException .
	 * @see com.isa.thinair.crypto.api.service.CryptoServiceBD#decrypt(String)
	 */
	public String decrypt(String cipherText) throws ModuleException {
		cryptoService = new PBEWithMD5AndDESImpl();
		return cryptoService.decrypt(cipherText);
	}
}