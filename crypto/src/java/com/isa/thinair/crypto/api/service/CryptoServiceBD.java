/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.crypto.api.service;

import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Nasly
 * @author Chamindap
 * @isa.module.bd-intf id="crypto.service"
 */
public interface CryptoServiceBD {

	/**
	 * Method to encrypt a plainText.
	 * 
	 * @param plainText
	 *            the plainText.
	 * @return the ciperText.
	 */
	public String encrypt(String plainText) throws ModuleException;

	/**
	 * Method to decrypt a ciperText.
	 * 
	 * @param ciperText
	 *            the ciperText.
	 * @return the plainText.
	 */
	public String decrypt(String cipherText) throws ModuleException;
}
