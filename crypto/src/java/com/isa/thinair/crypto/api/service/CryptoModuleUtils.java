/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.crypto.api.service;

import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * @author Chamindap
 */
public class CryptoModuleUtils {

	/**
	 * Returns the IModule reference.
	 * 
	 * @return the IModule reference.
	 */
	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule("crypto");
	}
}