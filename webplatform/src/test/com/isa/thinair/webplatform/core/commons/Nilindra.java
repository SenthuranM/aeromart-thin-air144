package com.isa.thinair.webplatform.core.commons;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.platform.api.LookupServiceFactory;

import javax.sql.DataSource;


public class Nilindra {

    public static void main(String[] args) {
        System.out.println("Start ");
        DataSource dataSource = (DataSource) LookupServiceFactory.getInstance().getBean("isa:base://modules?id=myDataSource");
        JdbcTemplate jt = new JdbcTemplate(dataSource);

        String sql = "SELECT user_id FROM t_user WHERE user_id=?";
        String[] params = new String[] {"Janaki"};
        
        /*
        PreparedStatementCreator psCreator = new BasicPreparedStatementCreator(sql, params);
        ResultSetExtractor rsExtracter = new BasicResultSetExtractor();
        
        
        List list = (List) jt.query(psCreator, rsExtracter);
        return list;
        */
        Collection collection = (Collection) 
        jt.query(sql, params, new ResultSetExtractor() {
            public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                if (rs != null) {
                    System.out.println("%%%%%%%%%%%%%%%%");
                    System.out.println(rs.getMetaData().getColumnCount());
                }
                
                return null;
            }
        });
        System.out.println("Exit");
    }
}
