package com.isa.thinair.webplatform.taglib;

import java.util.Map;

import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.webplatform.api.util.Constants;

public class HasPrivilegeTag extends TagSupport {

	private static final long serialVersionUID = -7060847411068288279L;

	private String privilegeId;

	public void setPrivilegeId(String privilegeID) {
		this.privilegeId = privilegeID;
	}

	public String getPrivilegeId() {
		return (privilegeId);
	}

	public int doStartTag() {
		int iProcessBody = SKIP_BODY;
		Map mapPrivileges = (Map) pageContext.getSession().getAttribute(Constants.SES_PRIVILEGE_IDS);
		String[] arrPrivilegeIds = null;
		int len = 0;
		if (privilegeId == null || "".equals(privilegeId)) {
			throw new IllegalArgumentException("HasPrivilegeTag: privilegeId attribute not set");
		}

		arrPrivilegeIds = StringUtils.split(privilegeId, ",");
		len = arrPrivilegeIds.length;

		for (int i = 0; i < len; i++) {
			boolean blnNotOperation = arrPrivilegeIds[i].indexOf('!') == 0;
			String priviId = (blnNotOperation) ? arrPrivilegeIds[i].substring(1) : arrPrivilegeIds[i];
			boolean blnHasPrivi = mapPrivileges.get(priviId) != null;

			if (blnNotOperation) {
				if (!blnHasPrivi) {
					iProcessBody = EVAL_BODY_INCLUDE;
					break;
				}
			} else if (blnHasPrivi) {
				iProcessBody = EVAL_BODY_INCLUDE;
				break;
			}
		}

		return (iProcessBody);
	}

}