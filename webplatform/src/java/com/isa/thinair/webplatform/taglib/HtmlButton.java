package com.isa.thinair.webplatform.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class HtmlButton extends TagSupport {

	private static Log log = LogFactory.getLog(HtmlButton.class);

	/**
	 * Make a Button with Rounded corners, Icon in any direction, and can set to an arabic lang also
	 */
	private static final long serialVersionUID = 10001L;
	private String name;
	private String cssClass = "htmlButton";
	private String value = "HTML Button";
	private String inLineStyles = "";
	private String id = "";
	private String tabIndex = "";
	private String title = "";

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCssClass() {
		return cssClass;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setInLineStyles(String inLineStyles) {
		this.inLineStyles = inLineStyles;
	}

	public String getInLineStyles() {
		return inLineStyles;
	}

	public int doStartTag() {
		String htmlAdd = "";
		if (name == null || "".equals(name)) {
			throw new IllegalArgumentException("name: name attribute not set");
		} else {
			id = (id == null || "".equals(id)) ? name : id;
			if (inLineStyles == null || "".equals(inLineStyles)) {
				htmlAdd += "<button type=\"button\" class=\"isabutton\" id=\"" + id + "\" " + "name=\"" + name + "\" tabindex=\""
						+ tabIndex + "\" title=\"" + title + "\">";
			} else {
				String tempArr[] = inLineStyles.split(":");
				htmlAdd += "<button type=\"button\" class=\"isabutton\" id=\"" + id + "\" " + "name=\"" + name + "\" tabindex=\""
						+ tabIndex + "\" style=\"" + tempArr[0] + ":" + tempArr[1] + "\" title=\"" + title + "\">";

			}
			// htmlAdd += "<span class=\"buttonspan "+cssClass+"\" > ";
		}
		htmlAdd += "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"auto\" class=\"" + cssClass
				+ "\"><tr><td class=\"buttonspan\">&nbsp;</td>";
		htmlAdd += "<td class=\"buttonMiddle\">" + value + "</td></tr></table></button>";
		// htmlAdd += "<span class=\"buttonMiddle\">"+value+"</span></span></button>";
		try {
			// Get the writer object for output.
			JspWriter out = pageContext.getOut();
			out.println(htmlAdd);
		} catch (IOException e) {
			log.error(e);
		}

		return SKIP_BODY;
	}

	public void setTabIndex(String tabIndex) {
		this.tabIndex = tabIndex;
	}

	public String getTabIndex() {
		return tabIndex;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

}
