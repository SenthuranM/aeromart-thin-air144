package com.isa.thinair.webplatform.taglib;

import java.io.IOException;

import javax.servlet.jsp.tagext.TagSupport;

import com.isa.thinair.webplatform.api.util.Constants;

public class ScrollTag extends TagSupport {

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	public int doStartTag() {
		String scroll = null;
		int iProcessBody = SKIP_BODY;
		String screenWidth = (String) pageContext.getSession().getAttribute(Constants.SES_SCREEN_WIDTH);

		if (screenWidth == null || "".equals(screenWidth)) {
			screenWidth = "1024";
		}

		scroll = ("800".equals(screenWidth)) ? "auto" : "no";

		try {
			pageContext.getOut().write(scroll);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		return (iProcessBody);
	}

}