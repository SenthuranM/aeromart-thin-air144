package com.isa.thinair.webplatform.core.schedrept;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;
import com.isa.thinair.webplatform.core.schedrept.ScheduleReportUtil.ScheduleWebConstants;

public class PointRedemptionReportSRPCImpl extends ScheduleReportCommon{

	@Override
	public ReportInputs composeReportInputs(HttpServletRequest request) {
		
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String pnr = request.getParameter("txtPnr");
		String paxName = request.getParameter("txtPaxName");
		String ffid = request.getParameter("txtFFID");
		String mobileNo = request.getParameter("txtMobileNo");
		String preferedCurrency = request.getParameter("radRptCurrency"); 
		String loyaltyProductCode = request.getParameter("selProducts");
		String reportFormat = request.getParameter("radReportOption");
		
		String reportTemplate = "PointRedemptionDetailedReport.jasper";
		String strlive = request.getParameter(ScheduleWebConstants.REP_HDN_LIVE);
		
		ReportsSearchCriteria search = new ReportsSearchCriteria();
		Map<String, String> parameters = new HashMap<String, String>();
		String reportTempleteRelativePath = getReportTemplateRelativeLocation(reportTemplate);
		
		try {

			search.setReportType(ReportsSearchCriteria.POINT_REDEMPTION_DETAILS);
			WPModuleUtils.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1", getReportTemplate(reportTemplate));
			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(convertDate(fromDate) + " 00:00:00");
			}

			search.setDateRangeFrom(convertDate(fromDate) + " 00:00:00");
			search.setDateRangeTo(convertDate(toDate) + " 23:59:59");
			search.setFlightNumber(request.getParameter("txtFlightNo"));
			String source = request.getParameter("selFromStn");
			String destination = request.getParameter("selToStn");
			String via1 = request.getParameter("selVia1");
			String via2 = request.getParameter("selVia2");
			
			search.setFrom(source);
			search.setTo(destination);
			
			if((via1 != null && via1 != "") || (via2 != null  && via2 != "")){
				List<String> viaPoints = new ArrayList<String>();
			
				if(!"".equals(source) && source != null){
					viaPoints.add(source);
				}
				if(!"".equals(via1) && via1 != null){
					viaPoints.add(via1);
				}
				if(!"".equals(via2) && via2 != null){
					viaPoints.add(via2);
				}
				if(!"".equals(destination) && destination != null){
					viaPoints.add(destination);
				}
				search.setViaPoints(viaPoints);
			}
			
			search.setPnr(pnr);
			search.setName(paxName);
			
			if(ffid != null){
				search.setFfid(ffid.toUpperCase());
			}
			
			search.setMobileNumber(mobileNo);
			search.setProductCode(loyaltyProductCode);
			
			if (!"USD".equals(preferedCurrency)) {
				search.setBaseCurrencySelected(true);
			}
			
			
			if (strlive != null) {
				if (ScheduleWebConstants.REP_LIVE.equals(strlive)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (ScheduleWebConstants.REP_OFFLINE.equals(strlive)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}
		
		return new ReportInputs(parameters, search, reportFormat, reportTempleteRelativePath,
				ScheduleReportUtil.ScheduledReportMetaDataIDs.POINT_REDEMPTION_REPORT);
	}

	public String convertDate(String dateString) {
		String date = null;
		int day = Integer.parseInt(dateString.substring(0, 2));
		int month = Integer.parseInt(dateString.substring(3, 5));
		int year = Integer.parseInt(dateString.substring(6));

		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = new GregorianCalendar(year, (month - 1), day);
		date = dateFormat.format(cal.getTime());

		return date;
	}

}
