package com.isa.thinair.webplatform.core.schedrept;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.ForceConfirmedPayStatus;

public class ForceConfirmedReservationsReportSRPCImpl extends ScheduleReportCommon {

	@Override
	public ReportInputs composeReportInputs(HttpServletRequest request) {

		ReportsSearchCriteria reportsSearchCriteria = new ReportsSearchCriteria();

		String bookingFromDate = request.getParameter("txtBkngFromDate");
		String bookingToDate = request.getParameter("txtBkngToDate");
		String reportFormat = "CSV";
		String link = "ForceConfirmedPNRReport.jasper";
		String reportTempleteRelativePath = "";
		Map<String, Object> parameters = new HashMap<String, Object>();

		try {

			reportsSearchCriteria.setDateRangeFrom(convertDate(bookingFromDate));
			reportsSearchCriteria.setDateRangeTo(convertDate(bookingToDate));

			if (!StringUtil.isNullOrEmpty(request.getParameter("txtDeptFromDate"))
					&& !StringUtil.isNullOrEmpty(request.getParameter("txtDeptToDate"))) {

				String departureFromDate = request.getParameter("txtDeptFromDate");
				String departureToDate = request.getParameter("txtDeptToDate");
				reportsSearchCriteria.setDateRange2From(convertDate(departureFromDate));
				reportsSearchCriteria.setDateRange2To(convertDate(departureToDate));
			}

			if (!StringUtil.isNullOrEmpty(request.getParameter("selDeparture"))
					&& !StringUtil.isNullOrEmpty(request.getParameter("selArrival"))) {

				String from = request.getParameter("selDeparture");
				String to = request.getParameter("selArrival");
				reportsSearchCriteria.setFrom(from);
				reportsSearchCriteria.setTo(to);
			}

			if (!StringUtil.isNullOrEmpty(request.getParameter("payStatus"))) {
				String payStatus = request.getParameter("payStatus");
				reportsSearchCriteria.setOnlyTotalAmountToPay(payStatus.equals(ForceConfirmedPayStatus.NOT_PAID.getStatus()));
				reportsSearchCriteria.setIncludePaidStatusFilter(true);
			}

			parameters.put("FROM_DATE", bookingFromDate);
			parameters.put("TO_DATE", bookingToDate);

			String locale = "en";
			parameters.put("language", locale);
			String reportsRootDir = "../images/" + AppSysParamsUtil.getReportLogo(true); // AA145.jpg
			parameters.put("IMG", reportsRootDir);
			
			reportTempleteRelativePath = getReportTemplateRelativeLocation(link);

		} catch (Exception ex) {
			throw new ModuleRuntimeException(ex,"schedrept.parameter.generation.error");
		}

		return new ReportInputs(parameters, reportsSearchCriteria, reportFormat, reportTempleteRelativePath,
				ScheduleReportUtil.ScheduledReportMetaDataIDs.FORCE_CNF_PNR_REPORT);
	}

}
