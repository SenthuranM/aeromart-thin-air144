package com.isa.thinair.webplatform.core.schedrept;

import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class GSTR3ReportSRPCImpl extends ScheduleReportCommon {

	@Override public ReportInputs composeReportInputs(HttpServletRequest request) {

		String stateCode = request.getParameter("selState");
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String reportMode = request.getParameter("hdnLive");

		String reportName;
		String GSTR3ReportTemplate = "GSTR3_Main.jasper";
		String reportTemplateRelativePath;

		Map<String, String> parameters = new HashMap<>();
		String reportFormat = setReportExportType(value);
		this.setReportLogoLocation(reportFormat, parameters);

		ReportsSearchCriteria search = new ReportsSearchCriteria();

		try {

			reportName = ScheduleReportUtil.ScheduledReportMetaDataIDs.GSTR3_REPORT;
			reportTemplateRelativePath = getReportTemplateRelativeLocation(GSTR3ReportTemplate);

			if (!StringUtil.isNullOrEmpty(fromDate)) {
				search.setDateRangeFrom(convertDate(fromDate) + " 00:00:00");
			}

			if (!StringUtil.isNullOrEmpty(toDate)) {
				search.setDateRangeTo(convertDate(toDate) + "  23:59:59");
			}
			if (!StringUtil.isNullOrEmpty(stateCode)) {
				search.setStateCode(stateCode);
			}

			if (reportMode != null) {
				if (ScheduleReportUtil.ScheduleWebConstants.REP_LIVE.equals(reportMode)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (ScheduleReportUtil.ScheduleWebConstants.REP_OFFLINE.equals(reportMode)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("OPTION", value);
			parameters.put("STATE_CODE", stateCode);
			parameters.put("REPORT_MEDIUM", reportMode);

		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}

		return new ReportInputs(parameters, search, reportFormat, reportTemplateRelativePath, reportName);

	}

}
