package com.isa.thinair.webplatform.core.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * Class to access lcc database for the usage of combobox data.
 * 
 * @author mekanayake
 * 
 */
public class LCCWebplatformDAO {

	private static Log log = LogFactory.getLog(LCCWebplatformDAO.class);

	/** data souce to get system params */
	private DataSource dataSource;

	/** properties to store queries for combos * */
	private Properties queryStringForCombo;

	private Properties selectSql;

	private Map<String, List<String[]>> mapParentStationByGroundStation;

	private Map<String, String> mapTopLevelStationByConnectedStation;

	private Map<String, String> connectingAirportDetail;

	/**
	 * 
	 * @param moduleProperty
	 * @return
	 */
	public List<String[]> getTwoColumnMap(String moduleProperty) {

		final List<String[]> list = new ArrayList<String[]>();

		if (AppSysParamsUtil.isLCCConnectivityEnabled() && dataSource != null && queryStringForCombo != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			String queryString = queryStringForCombo.getProperty(moduleProperty);
			templete.query(queryString, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					// if result set is not null
					if (rs != null) {

						// populate recordMap
						while (rs.next()) {
							String s[] = { rs.getObject(1) != null ? rs.getObject(1).toString() : "",
									rs.getObject(2) != null ? rs.getObject(2).toString() : "" };
							list.add(s);
						}
					}
					return null;
				}
			});
		}
		return list;
	}

	/**
	 * 
	 * @param moduleProperty
	 * @return
	 */
	public Collection<String[]> getList(String moduleProperty, Object[] params, int colCount) {
		if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
			JdbcTemplate jt = new JdbcTemplate(dataSource);

			String sql = selectSql.getProperty(moduleProperty);

			if (log.isDebugEnabled()) {
				log.debug("params=" + params.toString() + ";colCount=" + colCount);
				log.debug(sql);
			}

			PreparedStatementCreator psCreator = new BasicPreparedStatementCreator(sql, params);
			ResultSetExtractor rsExtracter = new BasicResultSetExtractor(colCount);

			return (Collection<String[]>) jt.query(psCreator, rsExtracter);
		} else {
			return null;
		}
	}

	public Map[] getLCCParentStationDetailsMap() {

		if (mapParentStationByGroundStation == null || mapTopLevelStationByConnectedStation == null
				|| connectingAirportDetail == null) {
			mapParentStationByGroundStation = new HashMap<String, List<String[]>>();
			mapTopLevelStationByConnectedStation = new HashMap<String, String>();
			connectingAirportDetail = new HashMap<String, String>();

			String queryStringGroundStationParentDetails = "select a.airport_code, a.connecting_airport, a.airport_name from lcc_t_airport a where a.status='ACT' and a.connecting_airport is not null";
			if (AppSysParamsUtil.isLCCConnectivityEnabled() && dataSource != null
					&& queryStringGroundStationParentDetails != null) {

				JdbcTemplate templete = new JdbcTemplate(dataSource);
				templete.query(queryStringGroundStationParentDetails, new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						if (rs != null) {
							while (rs.next()) {
								String airportCode = rs.getString("airport_code");
								String airportDesc = rs.getString("airport_name");
								String groundStationCode = rs.getString("connecting_airport");
								List<String[]> existing = mapParentStationByGroundStation.get(groundStationCode);
								if (existing == null) {
									existing = new ArrayList<String[]>();
									mapParentStationByGroundStation.put(groundStationCode, existing);
								}
								existing.add(new String[] { airportCode, airportDesc });
								mapTopLevelStationByConnectedStation.put(airportCode, groundStationCode);
								connectingAirportDetail.put(airportCode, airportDesc);
							}
						}
						return new Map[] { mapParentStationByGroundStation, mapTopLevelStationByConnectedStation,
								connectingAirportDetail };
					}
				});
			}
		}
		return new Map[] { mapParentStationByGroundStation, mapTopLevelStationByConnectedStation, connectingAirportDetail };
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Properties getQueryStringForCombo() {
		return queryStringForCombo;
	}

	public void setQueryStringForCombo(Properties queryStringForCombo) {
		this.queryStringForCombo = queryStringForCombo;
	}

	public Properties getSelectSql() {
		return selectSql;
	}

	public void setSelectSql(Properties selectSql) {
		this.selectSql = selectSql;
	}
}
