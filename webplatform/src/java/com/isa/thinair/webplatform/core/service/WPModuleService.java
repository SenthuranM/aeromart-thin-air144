/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.webplatform.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * 
 * Webplatform's Module's service interface
 * 
 * @isa.module.service-interface module-name="webplatform" description="webplatform module"
 */
public class WPModuleService extends DefaultModule {
}
