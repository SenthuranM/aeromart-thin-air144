package com.isa.thinair.webplatform.core.schedrept;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.schedrept.ScheduleReportUtil.ScheduleWebConstants;

/**
 * @author Navod Ediriweera
 */
public class InvoiceSummarySRPCImpl extends ScheduleReportCommon {

	@Override
	public ReportInputs composeReportInputs(HttpServletRequest request) {

		String year = request.getParameter("selYear");
		String month = request.getParameter("selMonth");
		String bilingPeriod = request.getParameter("selBP");
		String value = request.getParameter("radReportOption");
		String agent = request.getParameter("hdnAgents");
		String agentName = request.getParameter("hdnAgentName");
		String invoiceNo = request.getParameter("hdnInvoice");
		String rptView = request.getParameter("hdnRpt");
		// String strFrom = request.getParameter("hdnFrompage");
		String strlive = request.getParameter(ScheduleWebConstants.REP_HDN_LIVE);
		String strSource = request.getParameter("selPaySource");
		String strBase = request.getParameter("chkBase");
		String strReportMode = request.getParameter("hdnMode");
		String fromDate = request.getParameter("txtInvoiceFrom");
		String toDate = request.getParameter("txtInvoiceTo");
		boolean blnBase = (strBase != null && strBase.equalsIgnoreCase("on")) ? true : false;
		
		String entity = request.getParameter("selEntity");
		String entityText = request.getParameter("hdnEntityText");


		ReportsSearchCriteria search = new ReportsSearchCriteria();
		String id = "UC_REPM_008";

		// String reportTemplateSummary = "InvoiceSummaryReport.jasper";
		String reportTemplateSummary = "InvoiceSummary.jasper";
		String reportTemplateDetails = "InvoiceDetailReportTemplat.jasper";

		String showPaymentBreakDown = "N";
		String showPaymentCurrency = "N";
		String showPaymentCurrWithBreakDown = "N";
		String showPetrofacReference = "N";
		String showExternalAmount = "N";

		Date firstday = null;
		Date lastday = null;
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

		if (AppSysParamsUtil.isTransactInvoiceEnable()) {
			if (fromDate != null && toDate != null) {
				search.setDateRangeFrom(convertDate(fromDate) + " 00:00:00");
				search.setDateRangeTo(convertDate(toDate) + " 23:59:59");
				int[] intFromDate = getYearMonthDay(fromDate);
				int[] intToDate = getYearMonthDay(toDate);
				year = String.valueOf(intFromDate[0]);
				month = String.valueOf(intFromDate[1]);
				GregorianCalendar frmDate = new GregorianCalendar();
				GregorianCalendar tDate = new GregorianCalendar();
				frmDate.set(Calendar.YEAR, intFromDate[0]);
				frmDate.set(Calendar.MONTH, (intFromDate[1] - 1));
				frmDate.set(Calendar.DATE, intFromDate[2]);

				tDate.set(Calendar.YEAR, intToDate[0]);
				tDate.set(Calendar.MONTH, (intToDate[1] - 1));
				tDate.set(Calendar.DATE, intToDate[2]);
				SimpleDateFormat queryDateFormat = new SimpleDateFormat("dd-MM-yyyy");
				search.setFromDate(queryDateFormat.format(frmDate.getTime()) + " 00:00:00");
				search.setToDate(queryDateFormat.format(tDate.getTime()) + " 23:59:59");
			}
		} else {
			GregorianCalendar gc = new GregorianCalendar();
			gc.set(Calendar.YEAR, Integer.parseInt(year));
			gc.set(Calendar.MONTH, (Integer.parseInt(month) - 1));
			gc.set(Calendar.DATE, 1);
			
			if (AppSysParamsUtil.isWeeklyInvoiceEnabled()) {
				firstday = BLUtil.getInvoceDate(Integer.parseInt(year), (Integer.parseInt(month)), Integer.parseInt(bilingPeriod));
				Calendar last = Calendar.getInstance();
				last.setTime(firstday);
				last.add(Calendar.DATE, 6);
				lastday = last.getTime();
			} else {
				if (bilingPeriod.equals("1")) {
					firstday = gc.getTime();
					gc.set(Calendar.DATE, 15);
					lastday = gc.getTime();
	
				} else {
					gc.set(Calendar.DATE, 16);
					firstday = gc.getTime();
					gc.set(Calendar.DATE, gc.getActualMaximum(Calendar.DAY_OF_MONTH));
					lastday = gc.getTime();
				}
			}

			search.setDateRangeFrom(formatter.format(firstday) + " 00:00:00");
			search.setDateRangeTo(formatter.format(lastday) + "  23:59:59");
		}

		if (agent != "") {
			String agentArr[] = agent.split(",");
			ArrayList<String> agentCol = new ArrayList<String>();
			for (int r = 0; r < agentArr.length; r++) {
				agentCol.add(agentArr[r]);
			}
			search.setAgents(agentCol);
		} else {
			search.setAgents(null);
		}

		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();
		// AIRLINE_37
		String paymentBreakDown = globalConfig.getBizParam(SystemParamKeys.SHOW_BREAKDOWN);
		// AIRLINE_33
		String paymentInAgCur = globalConfig.getBizParam(SystemParamKeys.STORE_PAYMENTS_IN_AGENTS_CURRENCY);
		// AIRLINE_34
		String invoiceInAgCur = globalConfig.getBizParam(SystemParamKeys.INVOICE_IN_AGENTS_CURRENCY);
		boolean isShowPetrofac = globalConfig.getPetroFacAgentsMap().containsKey(agent);

		if (paymentBreakDown != null && paymentBreakDown.equalsIgnoreCase("Y")) {
			showPaymentBreakDown = "Y";
		}
		if (paymentInAgCur.equalsIgnoreCase("Y") && invoiceInAgCur.equalsIgnoreCase("Y") && !blnBase) {
			showPaymentCurrency = "Y";
		}
		if (showPaymentCurrency.equalsIgnoreCase("Y") && showPaymentBreakDown.equalsIgnoreCase("Y")) {
			showPaymentCurrWithBreakDown = "Y";
			showPaymentBreakDown = "N";
		}
		if (isShowPetrofac) {
			showPetrofacReference = "Y";
		}
		if (strSource != null) {
			if (strSource.equalsIgnoreCase("EXTERNAL")) {
				showExternalAmount = "Y";
				search.setPaymentSource(strSource);
			} else if (strSource.equalsIgnoreCase("BOTH")) {
				showExternalAmount = "Y";
				search.setShowExternal("Y");
			}
		}

		String period = "";
		// String reportName = "InvoiceDetailReport";
		String reportTempleteRelativePath = "";
		Map<String, String> parameters = new HashMap<String, String>();
		String reportFormat = setReportExportType(value);
		this.setReportLogoLocation(reportFormat, parameters);
		try {
			parameters.put(ScheduleWebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("REPORT_MEDIUM", strlive);

			if (strlive != null) {
				if (strlive.equals(ScheduleWebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(ScheduleWebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			if (rptView.equals("Summary")) {
				reportTempleteRelativePath = getReportTemplateRelativeLocation(reportTemplateSummary);

				search.setYear(year);
				search.setMonth(month);
				search.setReportType(ReportsSearchCriteria.AGENT_INVOICE_SUMMARY);
				parameters.put("INVOICE_YEAR", year);
				parameters.put("INVOICE_MONTH", month);	

			} else {
				reportTempleteRelativePath = getReportTemplateRelativeLocation(reportTemplateDetails);
				search.setAgentCode(agent);
				search.setYear(year);
				search.setMonth(month);
				search.setReportType(ReportsSearchCriteria.AGENT_INVOICE_DETAILS);
				parameters.put("AGENT_CODE", agent);
				parameters.put("AGENT_NAME", agentName);
				parameters.put("INVOICE_NO", invoiceNo);
				parameters.put("ID", id);
			}
			parameters.put("BASE_CURRENCY", globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY));
			parameters.put("ID", id);
			if (strSource == null) {
				strSource = "INTERNAL";
			}

			parameters.put("SOURCE", strSource);

			parameters.put("SHOW_BREAKDOWN", showPaymentBreakDown);
			parameters.put("SHOW_PAY_CUR", showPaymentCurrency);
			parameters.put("SHOW_PAYCUR_BREKDWN", showPaymentCurrWithBreakDown);
			parameters.put("SHOW_PAYREF", showPetrofacReference);
			parameters.put("SHOW_EXTERNAL", showExternalAmount);

			if (blnBase) {
				parameters.put("CHK_BASE", "on");
			}
			if (AppSysParamsUtil.isTransactInvoiceEnable()) {
				parameters.put("SHOW_INVOICE_ACTUAL_AMOUNT", "Y");
			} else {
				parameters.put("SHOW_INVOICE_ACTUAL_AMOUNT", "N");
			}

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			setPreferedReportFormat(reportNumFormat, parameters);

			search.setPaymentSource(strSource);
			if (bilingPeriod != null && !bilingPeriod.equals("")) {
				search.setBillingPeriod(Integer.parseInt(bilingPeriod));
				period += bilingPeriod;
			}

			search.setEntity(entity);
			
			parameters.put("INVOICE_PERIOD", period);
			parameters.put("OPTION", value);
			
			parameters.put("ENTITY", entityText);
			parameters.put("ENTITY_CODE", entity);

			if (strReportMode.equals(ScheduleWebConstants.ACTION_EXPORT)) {
				parameters.put("SHOW_HEADING", "Y");
				// ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1",
				// parameters, resultSet, response);
			} else {
				if (AppSysParamsUtil.isTransactInvoiceEnable()) {
					parameters.put("DISPLAY_LINK", "N");
				} else {
					parameters.put("DISPLAY_LINK", "Y");
				}
				parameters.put("SHOW_HEADING", "Y");
			}
		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}
		return new ReportInputs(parameters, search, reportFormat, reportTempleteRelativePath,
				ScheduleReportUtil.ScheduledReportMetaDataIDs.INVOICE_SUMMERY);
	}

}
