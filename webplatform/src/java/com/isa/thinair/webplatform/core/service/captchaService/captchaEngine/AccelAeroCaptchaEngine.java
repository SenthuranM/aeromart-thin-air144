package com.isa.thinair.webplatform.core.service.captchaService.captchaEngine;

import java.awt.Font;

import com.octo.captcha.component.image.backgroundgenerator.BackgroundGenerator;
import com.octo.captcha.component.image.backgroundgenerator.FunkyBackgroundGenerator;
import com.octo.captcha.component.image.color.RandomRangeColorGenerator;
import com.octo.captcha.component.image.fontgenerator.FontGenerator;
import com.octo.captcha.component.image.fontgenerator.RandomFontGenerator;
import com.octo.captcha.component.image.textpaster.RandomTextPaster;
import com.octo.captcha.component.image.textpaster.TextPaster;
import com.octo.captcha.component.image.wordtoimage.ComposedWordToImage;
import com.octo.captcha.component.image.wordtoimage.WordToImage;
import com.octo.captcha.component.word.wordgenerator.RandomWordGenerator;
import com.octo.captcha.component.word.wordgenerator.WordGenerator;
import com.octo.captcha.engine.image.ListImageCaptchaEngine;
import com.octo.captcha.image.gimpy.GimpyFactory;

public class AccelAeroCaptchaEngine extends ListImageCaptchaEngine {
	
	private static final String CHARACTER_SET_1 = "abcdefghijklmnopqrstuvwxyz123456789";
	private static final String FONT_1 = "Arial";
	private static final String FONT_2 = "Tahoma";
	private static final String FONT_3 = "Verdana";

	protected void buildInitialFactories() {

		WordGenerator wgen = new RandomWordGenerator(CHARACTER_SET_1);

		RandomRangeColorGenerator cgen = new RandomRangeColorGenerator(new int[] { 0, 100 }, new int[] { 0, 100 }, new int[] { 0,
				100 });
		TextPaster textPaster = new RandomTextPaster(new Integer(5), new Integer(5), cgen, true);

		BackgroundGenerator backgroundGenerator = new FunkyBackgroundGenerator(new Integer(200), new Integer(100));

		Font[] fontsList = new Font[] { new Font(FONT_1, 0, 10), new Font(FONT_2, 0, 10), new Font(FONT_3, 0, 10), };

		FontGenerator fontGenerator = new RandomFontGenerator(new Integer(40), new Integer(55), fontsList);

		WordToImage wordToImage = new ComposedWordToImage(fontGenerator, backgroundGenerator, textPaster);

		this.addFactory(new GimpyFactory(wgen, wordToImage));
	}

}
