package com.isa.thinair.webplatform.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.webplatform.api.v2.util.MultiSelectDataParser;

/**
 * Data parse for MultiSelect
 * 
 * @author Navod Ediriweera
 * 
 */
public class MultiSelectConverterUtil {

	public static List<MultiSelectDataParser> convertMapedStringsFromColumnMap(List<Map<String, String>> mapedCodes,
			String idCode, String categoryCode, String descriptionCode) {
		List<MultiSelectDataParser> multiSelectDataList = new ArrayList<MultiSelectDataParser>();
		for (Map<String, String> map : mapedCodes) {
			MultiSelectDataParser parser = new MultiSelectDataParser();
			parser.setCategory(categoryCode);
			parser.setId(map.get(idCode) == null ? "" : String.valueOf(map.get(idCode)));
			parser.setText(map.get(descriptionCode) == null ? "" : String.valueOf(map.get(descriptionCode)));
			multiSelectDataList.add(parser);
		}
		return multiSelectDataList;
	}

	public static List<MultiSelectDataParser> convertMappedStringFromList(Collection<String[]> items, int idVal, int textVal,
			int categoryVal) {
		List<MultiSelectDataParser> multiSelectDataList = new ArrayList<MultiSelectDataParser>();

		if (!items.isEmpty()) {
			Iterator<String[]> itemIter = items.iterator();
			while (itemIter.hasNext()) {
				String str[] = (String[]) itemIter.next();
				MultiSelectDataParser parser = new MultiSelectDataParser();
				parser.setCategory(str[categoryVal]);
				parser.setId(str[idVal]);
				parser.setText(str[textVal]);
				multiSelectDataList.add(parser);
			}
		}
		return multiSelectDataList;
	}
}