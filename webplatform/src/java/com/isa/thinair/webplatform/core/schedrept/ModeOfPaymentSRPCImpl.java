package com.isa.thinair.webplatform.core.schedrept;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;
import com.isa.thinair.webplatform.core.schedrept.ScheduleReportUtil.ScheduleWebConstants;

public class ModeOfPaymentSRPCImpl extends ScheduleReportCommon {

	private static Log log = LogFactory.getLog(ModeOfPaymentSRPCImpl.class);

	@Override
	public ReportInputs composeReportInputs(HttpServletRequest request) {
		String reportTempleteRelativePath = null;

		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String agents = request.getParameter("hdnAgents");
		String payments = request.getParameter("hdnPayments");
		String reportBy = request.getParameter("chkOption");
		String reoprtType = request.getParameter("hdnRptType");
		String paymentGw = request.getParameter("hdnPaymentGw");

		String strTime = request.getParameter("hdnTime");

		String agentName = request.getParameter("hdnAgentName");
		String paymentDesc = request.getParameter("hdnPaymentDesc");
		String id = "UC_REPM_002";
		String modeOFPaymentSummaryRpt = "ModeOfPaymentSummary.jasper";
		String modeOFPaymentDetailRpt = "ModeOfPaymentDetails.jasper";
		String modeOFPaymentDetailCashRpt = "ModeOfPaymentCashDetails.jasper";
		String modeOFPaymentSummaryAgentRpt = "ModeOfPaymentAgentSummary.jasper";
		String modeOFPaymentDetailAgentRpt = "ModeOfPaymentAgentDetail.jasper";
		String modeOFPaymentSummaryAgentNameRpt = "ModeOfPaymentAgentNameSummary.jasper";
		String modeOFPaymentExternalSummaryRpt = "ModeOfPaymentExternalSummary.jasper";
		String strlive = request.getParameter(ScheduleWebConstants.REP_HDN_LIVE);
		String strEntity = request.getParameter("selEntity");
		String hdnEntity = request.getParameter("hdnEntity");
		
		ReportsSearchCriteria search = new ReportsSearchCriteria();

		Date currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();
		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

		if (fromDate != null && !fromDate.equals("")) {
			if (strTime.equals(ScheduleWebConstants.LOCALTIME)) {
				search.setDateRangeFrom(getLocalDateForMOP(fromDate + " 00:00:00"));
			} else {
				search.setDateRangeFrom(convertDate(fromDate) + " 00:00:00");
			}
		}

		if (toDate != null && !toDate.equals("")) {
			if (strTime.equals(ScheduleWebConstants.LOCALTIME)) {
				search.setDateRangeTo(getLocalDateForMOP(toDate + " 23:59:59"));
			} else {
				search.setDateRangeTo(convertDate(toDate) + " 23:59:59");
			}
		}
		
		search.setEntity(strEntity);

		String zuluTimePart = "";
		String agentStation = "";
		String localTimePart = "";
		Date localtime;
		ZuluLocalTimeConversionHelper timeConverter;

		UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
		agentStation = user.getAgentStation();
		try {
			timeConverter = new ZuluLocalTimeConversionHelper(WPModuleUtils.getAirportBD());
			localtime = timeConverter.getLocalDateTime(agentStation, currentTimestamp);
		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}
		if (!strTime.equals(ScheduleWebConstants.LOCALTIME)) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(currentTimestamp);
			zuluTimePart = calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + ":"
					+ calendar.get(Calendar.SECOND);
		} else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(localtime);
			localTimePart = calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + ":"
					+ calendar.get(Calendar.SECOND);
		}
		Map<String, String> parameters = new HashMap<String, String>();
		String reportFormat = setReportExportType(value);
		this.setReportLogoLocation(reportFormat, parameters);
		try {
			if (strlive != null) {
				if (strlive.equals(ScheduleWebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(ScheduleWebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			if (reportBy.equals("PM")) {
				if (reoprtType.equals("SUMMARY_PM")) {
					parameters.put("BASE_CURRENCY", globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY));
					reportTempleteRelativePath = getReportTemplateRelativeLocation(modeOFPaymentSummaryRpt);
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_SUMMARY);

				} else if (paymentDesc.equals("Cash") || paymentDesc.equals("On Account")) {
					reportTempleteRelativePath = getReportTemplateRelativeLocation(modeOFPaymentDetailCashRpt);
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_DETAIL);
				} else {
					reportTempleteRelativePath = getReportTemplateRelativeLocation(modeOFPaymentDetailRpt);
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_CREDIT_DETAIL);
					search.setPaymentGW(paymentGw);
				}
			} else if (reportBy.equals("AC")) {
				if (reoprtType.equals("SUMMARY_AC")) {
					parameters.put("BASE_CURRENCY", globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY));
					reportTempleteRelativePath = getReportTemplateRelativeLocation(modeOFPaymentSummaryAgentRpt);
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_AGENT_SUMMARY);

				} else {
					reportTempleteRelativePath = getReportTemplateRelativeLocation(modeOFPaymentDetailAgentRpt);
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_AGENT_DETAILS);
					if (agents.equalsIgnoreCase("WEB")) {
						search.setUserId(agents);
					} else {
						search.setUserId(agentName);
					}
					search.setPaymentGW(paymentGw);
				}
			} else if (reportBy.equals("AN")) { // report type
				if (reoprtType.equals("SUMMARY_AN")) {
					parameters.put("BASE_CURRENCY", globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY));
					reportTempleteRelativePath = getReportTemplateRelativeLocation(modeOFPaymentSummaryAgentNameRpt);
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_AGENT_NAME_SUMMARY);
				} else {
					// future requirements if a separate type of detailed report
					// is needed
					reportTempleteRelativePath = getReportTemplateRelativeLocation(modeOFPaymentDetailAgentRpt);
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_AGENT_DETAILS);
				}

			} else if (reportBy.equals("EP")) { // report type
				if (reoprtType.equals("SUMMARY_EXTERNAL")) {
					parameters.put("BASE_CURRENCY", globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY));
					reportTempleteRelativePath = getReportTemplateRelativeLocation(modeOFPaymentExternalSummaryRpt);
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_EXTERNAL_SUMMARY);
				}
			} else if (reportBy.equals("BC")) { // report type
				if (reoprtType.equals("CURRENCY_SUMMARY")) {
					parameters.put("BASE_CURRENCY", globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY));
					reportTempleteRelativePath = getReportTemplateRelativeLocation("ModeOfPaymentSummaryByCurrency.jasper");
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_CURRENCY_SUMMARY);
				} else {
					search.setPaymentCode(paymentDesc);
					reportTempleteRelativePath = getReportTemplateRelativeLocation(modeOFPaymentDetailAgentRpt);
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_CURRENCY_DETAIL);
				}
			}

			ArrayList<String> paymentCol = new ArrayList<String>();
			String paymentArr[] = null;

			if (payments.indexOf(",") != -1) {
				paymentArr = payments.split(",");
			} else {
				paymentArr = new String[1];
				paymentArr[0] = payments;
			}

			String paymentStr = "";
			if (paymentArr != null) {
				for (int r = 0; r < paymentArr.length; r++) {

					if (paymentArr[r].indexOf(".") != -1) {
						paymentStr = paymentArr[r].substring(0, paymentArr[r].indexOf("."));
					} else {
						paymentStr = paymentArr[r];
					}
				}
				String refundCodes[] = { "29", "24", "23", "22", "26", "25", "31", "33" , "37" , "47"};

				for (int r = 0; r < paymentArr.length; r++) {
					if (paymentArr[r].indexOf(".") != -1) {
						paymentStr = paymentArr[r].substring(0, paymentArr[r].indexOf("."));
					} else {
						paymentStr = paymentArr[r];
					}
					if (paymentStr.equals("28")) {
						paymentCol.add("28");
						paymentCol.add(refundCodes[0]);
					}
					if (paymentStr.equals("17")) {
						paymentCol.add("17");
						paymentCol.add(refundCodes[1]);
					}
					if (paymentStr.equals("16")) {
						paymentCol.add("16");
						paymentCol.add(refundCodes[2]);
					}
					if (paymentStr.equals("15")) {
						paymentCol.add("15");
						paymentCol.add(refundCodes[3]);
					}
					if (paymentStr.equals("18")) {
						paymentCol.add("18");
						paymentCol.add(refundCodes[4]);
					}
					if (paymentStr.equals("19")) {
						paymentCol.add("19");
						paymentCol.add(refundCodes[5]);
					}
					if (paymentStr.equals("30")) {
						paymentCol.add("30");
						paymentCol.add(refundCodes[6]);
					}
					if (paymentStr.equals("32")) {
						paymentCol.add("32");
						paymentCol.add(refundCodes[7]);
					}
					if (paymentStr.equals("36")) {
						paymentCol.add("36");
						paymentCol.add(refundCodes[8]);
					}
					if (paymentStr.equals("41")) {
						// Generic debit payments
						paymentCol.add("41");
					}
					if (paymentStr.equals("46")) {
						paymentCol.add("46");
						paymentCol.add(refundCodes[9]);
					}
				}
			}
			search.setPaymentTypes(paymentCol);
			parameters.put("TIME_ZONE", strTime);
			parameters.put("LOCAL_TIME", localTimePart);
			parameters.put("ZULU_TIME", zuluTimePart);
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			parameters.put("OPTION", value);
			parameters.put("USER_ID", agents);
			parameters.put("PAYMENT_MODE", paymentDesc);
			parameters.put("TIME_MODE", strTime);
			parameters.put("REPORT_MEDIUM", strlive);
			parameters.put("REPORT_NAME", search.getReportType());
			parameters.put("DISPLAY_ADDITIONAL_PAYMENT_MODE", new Boolean(AppSysParamsUtil.isAllowCapturePayRef()).toString());
			parameters.put("ENTITY_NAME", hdnEntity);

			parameters.put(ScheduleWebConstants.REP_CARRIER_NAME, strCarrier);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			setPreferedReportFormat(reportNumFormat, parameters);
		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}
		return new ReportInputs(parameters, search, reportFormat, reportTempleteRelativePath,
				ScheduleReportUtil.ScheduledReportMetaDataIDs.MODE_OF_PAYMENT);

	}

	private static String getLocalDateForMOP(String str) {
		String strNew = "";
		SimpleDateFormat sdt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		SimpleDateFormat convsdt = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		long localt = 4 * 3600 * 1000;
		Date serchDate = null;
		try {
			serchDate = sdt.parse(str);
			long searct = serchDate.getTime();
			searct = searct - localt;
			serchDate = new Date(searct);
			strNew = convsdt.format(serchDate);
		} catch (Exception e) {
			log.error("Error in prsingdate in MOde of payment" + e);
		}
		return strNew;
	}

}
