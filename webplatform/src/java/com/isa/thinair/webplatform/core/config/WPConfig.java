package com.isa.thinair.webplatform.core.config;

import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class WPConfig {

	/**
	 * @author Sudheera Keeps configurations used in webplatform module
	 */
	private static Log log = LogFactory.getLog(WPConfig.class);

	private static final String SERVER_MESSEAGES_RESOURCE_BUNDLE_NAME = "resources/messages/WPServerMessages";

	private static final String DEFULT_SERVER_MESSAGE_CODE = "default.server.messsage";

	private static final ResourceBundle serverMessageCodes = loadResourceBundle(SERVER_MESSEAGES_RESOURCE_BUNDLE_NAME);

	private static String defaultLanguage = "en";

	public static String getDefaultLanguage() {
		return defaultLanguage;
	}

	public void setDefaultLanguage(String localeLanguage) {
		WPConfig.defaultLanguage = localeLanguage;
	}

	public static final String getServerMessage(String messageCode) {
		if (messageCode == null || messageCode.equals("")) {
			return serverMessageCodes.getString(DEFULT_SERVER_MESSAGE_CODE);
		} else {
			return serverMessageCodes.getString(messageCode);
		}
	}

	/**
	 * loads a resouce bundle
	 * 
	 * @param bundleName
	 * @return
	 */
	private static final ResourceBundle loadResourceBundle(String bundleName) {
		ResourceBundle messageCodes = null;
		Locale locale = new Locale((getDefaultLanguage() != null) ? getDefaultLanguage() : "en");

		try {
			messageCodes = ResourceBundle.getBundle(bundleName, locale);

		} catch (RuntimeException re) {
			log.fatal("Cannot load the resouce bundle:" + bundleName + ":" + re.getMessage());
			;
			throw re;
		}

		return messageCodes;
	}

}
