package com.isa.thinair.webplatform.core.util;

import java.util.HashMap;

public interface RowDecorator<T> {

	public void decorateRow(HashMap<String, Object> row, T record);
}
