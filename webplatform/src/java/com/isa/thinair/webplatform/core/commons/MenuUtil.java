/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.webplatform.core.commons;

import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * Utility class for access the menus
 * 
 * @author Lasantha Pambagoda
 */
public class MenuUtil {

	public static final String URL_PRIFIX = "isa:base://modules/webplatform?id=";

	public static MenuRoot getRootMenu(String menuTreeName) {

		LookupService lookup = LookupServiceFactory.getInstance();
		MenuRoot menuRoot = (MenuRoot) lookup.getBean(URL_PRIFIX + menuTreeName);

		return menuRoot;
	}

}
