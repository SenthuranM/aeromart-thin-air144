package com.isa.thinair.webplatform.core.util;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.jdbc.core.PreparedStatementCreator;

public class BasicPreparedStatementCreator implements PreparedStatementCreator {

	private String sql;

	private Object[] params;

	public BasicPreparedStatementCreator(String sql, Object[] params) {
		this.sql = sql;
		this.params = params;
	}

	public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(sql);

		if (params != null) {
			for (int i = 0; i < params.length; i++) {
				if (params[i] instanceof Integer) {
					ps.setInt(i + 1, (Integer) params[i]);
				} else if (params[i] instanceof Date) {
					ps.setDate(i + 1, (Date) params[i]);
				} else {
					ps.setString(i + 1, (String) params[i]);
				}
			}
		}
		return ps;
	}

}
