package com.isa.thinair.webplatform.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DataUtil {

	public static <T> ArrayList<Map<String, Object>> createGridData(int startPosition, String objectName, Collection<T> dataList,
			RowDecorator<T> decorator) {

		ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

		if (dataList != null) {
			int index = startPosition;
			Iterator<T> iter = dataList.iterator();
			while (iter.hasNext()) {
				HashMap<String, Object> row = new HashMap<String, Object>();
				T record = iter.next();
				row.put("id", ++index);
				row.put(objectName, record);

				if (decorator != null)
					decorator.decorateRow(row, record);

				list.add(row);
			}
		}
		return list;
	}

	public static <T> ArrayList<Map<String, Object>> createGridData(int startPosition, String objectName, Collection<T> dataList) {
		return createGridData(startPosition, objectName, dataList, null);
	}

	/**
	 * Creates Grid data without setting up the attributes in the record. and only with RowDecorator attributes
	 * 
	 * @param <T>
	 * @param startPosition
	 * @param dataList
	 * @param decorator
	 * @return
	 */
	public static <T> ArrayList<Map<String, Object>> createGridDataDecorateOnly(int startPosition, Collection<T> dataList,
			RowDecorator<T> decorator) {
		ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

		if (dataList != null) {
			int index = startPosition;
			Iterator<T> iter = dataList.iterator();
			while (iter.hasNext()) {
				HashMap<String, Object> row = new HashMap<String, Object>();
				T record = iter.next();
				row.put("id", ++index);
				if (decorator != null)
					decorator.decorateRow(row, record);

				list.add(row);
			}
		}
		return list;
	}

	public static <T> ArrayList<Map<String, Object>> createDynamicGridDataDecorateOnly(int startPosition, String objectName,
			Collection<T> dataList, DynamicRowDecorator<T> decorator) {
		ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

		if (dataList != null) {
			int index = startPosition;
			Iterator<T> iter = dataList.iterator();
			int count = 0;
			while (iter.hasNext()) {
				HashMap<String, Object> row = new HashMap<String, Object>();
				T record = iter.next();
				row.put("id", ++index);
				row.put(objectName, record);
				if (decorator != null)
					decorator.decorateRow(row, record, count);

				list.add(row);
				count++;
			}
		}
		return list;
	}

	public static boolean isNullOrEmpty(String value) {
		return (value == null || "".equals(value.trim()));
	}

	public static boolean isNotNullOrEmpty(String value) {
		return !isNullOrEmpty(value);
	}

	/**
	 * Converts Null objects to a empty string
	 * 
	 * @param value
	 * @return
	 */
	public static String maskNull(Object value) {
		if (value == null) {
			value = "";
		}
		return value.toString();
	}
}
