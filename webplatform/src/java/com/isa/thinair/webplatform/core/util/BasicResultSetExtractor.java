package com.isa.thinair.webplatform.core.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

public class BasicResultSetExtractor implements ResultSetExtractor {

	private int colCount;

	public BasicResultSetExtractor(int colCount) {
		this.colCount = colCount;
	}

	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

		Collection<String[]> col = new ArrayList<String[]>();
		while (rs.next()) {

			String[] rec = new String[colCount];

			for (int i = 0; i < colCount; i++) {
				rec[i] = rs.getString(i + 1);
			}

			col.add(rec);

		}
		return col;
	}

}
