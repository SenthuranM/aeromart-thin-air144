/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.webplatform.core.commons;

import java.util.Map;

/**
 * class to reprent menu root
 * 
 * @author Lasantha Pambaogoda
 */
public class MenuRoot {

	private Map menus;

	/**
	 * @return Returns the menus.
	 */
	public Map getMenus() {
		return menus;
	}

	/**
	 * @param menus
	 *            The menus to set.
	 */
	public void setMenus(Map menus) {
		this.menus = menus;
	}
}
