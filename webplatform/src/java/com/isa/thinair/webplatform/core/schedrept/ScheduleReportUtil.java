package com.isa.thinair.webplatform.core.schedrept;

public class ScheduleReportUtil {

	public interface ScheduledReportMetaDataIDs {
		public static final String AGENT_SALES_REFUND_REPORT = "agentSalesModifyRefundReport";
		public static final String COMPANY_PAYMENT = "companyPaymentSummary";
		public static final String COMPANY_PAYMENT_DETAIL = "companyPaymentDetail";		
		public static final String MODE_OF_PAYMENT = "modeofPaymentRpt";
		public static final String INVOICE_SUMMERY = "invoiceSummary";
		public static final String TRANSACTION_REPORT = "transactionReport";
		public static final String SEAT_INVENTORY_COLLECTIONS = "seatInventoryCollections";
		public static final String REVENUE_AND_TAX = "revenueAndTaxReport";
		public static final String OND_REPORT = "ondReport";
		public static final String STAFF_PERFORMANCE_REPORT = "staffPerformanceReport";
		public static final String POINT_REDEMPTION_REPORT = "pointRedemptionReport";
		public static final String NIL_TRANSACTION_AGENTS  = "NILTransactionsAgentsSummary";
		public static final String IBE_ONHOLD_REPORT = "IBEOnHoldReport";
		public static final String PROMOTION_INFO_REPORT = "promotionInfoReport";
		public static final String FORCE_CNF_PNR_REPORT= "forcedConfirmedReservationsReport";
		public static final String FLOWN_PAX_REPORT = "flownPassengerReport";
		public static final String TOP_AGENTS_REPORT = "topAgentsReport";
		public static final String AGENT_CHARGE_ADJUSTMENT_REPORT = "agentChargeAdjustmentReport";
		public static final String GSTR1_REPORT = "gstr1Report";
		public static final String GSTR3_REPORT = "gstr3Report";
		public static final String GST_ADDITIONAL_REPORT = "gstAdditionalReport";
		public static final String CC_LMS_TRANSACTIONS_REPORT = "ccLmsTransactionsReport";
		public static final String ORIGIN_COUNTRY_OF_CALL_REPORT = "originCountryOfCallReport";
	}

	public interface ScheduledReportReportDisplayNames {
		public static final String AGENT_SALES_REFUND_REPORT = "Agent Sale/Modify/Refund Report";
		public static final String COMPANY_PAYMENT = "Company Payment Report";
		public static final String MODE_OF_PAYMENT = "Mode of Payment Report";
		public static final String INVOICE_SUMMERY = "Invoice Summary Report";
		public static final String TRANSACTION_REPORT = "Credit Card Transaction Report";
		public static final String SEAT_INVENTORY_COLLECTIONS = "Seat Inventory Collection";
		public static final String REVENUE_AND_TAX = "Revenue and Tax Report";
		public static final String OND_REPORT = "OnD Report";
		public static final String STAFF_PERFORMANCE_REPORT = "Staff Performance Report";
		public static final String POINT_REDEMPTION_REPORT = "Point Redemption Report";
		public static final String PROMOTION_INFO_REPORT = "Promotion Information Report";
		public static final String GSTR1_REPORT = "GSTR1 Report";
		public static final String GSTR3_REPORT = "GSTR3 Report";
		public static final String GST_ADDITIONAL_REPORT = "GST Additional Report";
	}
	
	
	public interface ScheduleWebConstants {
		public static final String REP_HDN_LIVE = "hdnLive";
		public static final String REP_CARRIER_NAME = "CARRIER";
		public static final String REP_LIVE = "LIVE";
		public static final String REP_OFFLINE = "OFFLINE";
		public static final String ACTION_EXPORT = "EXPORT";
		public static final String LOCALTIME = "LOCAL";
		public static final String SES_CURR_USER = "sesCurrUser";
		public static final String PRIVI_VIEW_FULL_SEAT_INVENT_RPT = "rpt.pln.icf.restrict";
	}

}
