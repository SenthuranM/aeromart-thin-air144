package com.isa.thinair.webplatform.core.schedrept;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.ReportFormatTypes;
import com.isa.thinair.platform.api.constants.PlatformConstants;

/**
 * Common Reporting Functionalities for V2.
 * 
 * @author M.Rikaz
 */
public abstract class ScheduleReportCommon implements SRPComposer, ReportFormatTypes.ReportTypes {

	protected String strLogo = AppSysParamsUtil.getReportLogo(false);
	protected String reportsRootDir = "../../images/" + AppSysParamsUtil.getReportLogo(true);
	protected String strCarrier = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.CARRIER_NAME)
			+ " Reservation System";

	/**
	 * Retrive the actual report Template path
	 * 
	 * @param fileName
	 * @return
	 */
	protected static String getReportTemplate(String fileName) {
		String reportsRootDir = null;
		reportsRootDir = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/" + fileName;
		return reportsRootDir;
	}

	protected static String getReportTemplateRelativeLocation(String fileName) {
		String reportsRootDir = null;
		reportsRootDir = "/templates/reports/" + fileName;
		return reportsRootDir;
	}

	/**
	 * Set the export type for the report based on the parameter value.
	 * 
	 * @param paramValue
	 * @return
	 */
	protected String setReportExportType(String paramValue) {
		String reportFormat = "";
		if (paramValue != null) {
			if (paramValue.trim().equals("HTML")) {
				reportFormat = HTML;
			} else if (paramValue.trim().equals("PDF")) {
				reportFormat = PDF;
			} else if (paramValue.trim().equals("EXCEL")) {
				reportFormat = EXCEL;
			} else if (paramValue.trim().equals("CSV")) {
				reportFormat = CSV;
			} else if (paramValue.trim().equals("CSV_AGENT")) {
				reportFormat = CSV_AGENT;
			}
		}
		return reportFormat;
	}

	/**
	 * Retrive different locations for the report logo for HTML and PDF reports
	 * 
	 * @param reportFormat
	 * @param parameters
	 */
	protected void setReportLogoLocation(String reportFormat, Map<String, String> parameters) {

		if (HTML.equals(reportFormat)) {
			reportsRootDir = "../../images/" + strLogo;
			parameters.put("IMG", reportsRootDir);

		} else if (PDF.equals(reportFormat)) {
			String strLogo = AppSysParamsUtil.getReportLogo(false);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
			String reportsIMGDir = getReportTemplate(strLogo);
			parameters.put("IMG", reportsIMGDir);

		}
	}

	/**
	 * Sets the prefered report format for US/Europe formats
	 * 
	 * @param prefRptFormat
	 * @param parameters
	 */
	protected void setPreferedReportFormat(String prefRptFormat, Map<String, String> parameters) {
		parameters.put("LOCALE", "US");
		parameters.put("DELIMITER", ",");
		if (prefRptFormat != null && !prefRptFormat.equals("")) {
			if (prefRptFormat.equalsIgnoreCase("FR") || prefRptFormat.equalsIgnoreCase("IT")) {
				parameters.put("LOCALE", "IT");
				parameters.put("DELIMITER", ";");
			}
		}
	}

	@SuppressWarnings("rawtypes")
	protected boolean hasPrivilege(HttpServletRequest request, String privilegeId) {

		Map mapPrivileges = (Map) request.getSession().getAttribute("sesPrivilegeIds");
		boolean has = (mapPrivileges.get(privilegeId) != null) ? true : false;

		return has;
	}

	protected String convertDate(String dateString) {
		String date = null;
		int day = Integer.parseInt(dateString.substring(0, 2));
		int month = Integer.parseInt(dateString.substring(3, 5));
		int year = Integer.parseInt(dateString.substring(6));

		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = new GregorianCalendar(year, (month - 1), day);
		date = dateFormat.format(cal.getTime());

		return date;
	}
	
	protected String convertDateTime(String dateString) {
		String date = null;
		int day = Integer.parseInt(dateString.substring(0, 2));
		int month = Integer.parseInt(dateString.substring(3, 5));
		int year = Integer.parseInt(dateString.substring(6));

		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		Calendar cal = new GregorianCalendar(year, (month - 1), day);
		date = dateFormat.format(cal.getTime());

		return date;
	}

	protected String convertDateFromDetail(String dateString) {
		String date = null;
		int year = Integer.parseInt(dateString.substring(0, 4));
		int month = Integer.parseInt(dateString.substring(5, 7));
		int day = Integer.parseInt(dateString.substring(8));

		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = new GregorianCalendar(year, (month - 1), day);
		date = dateFormat.format(cal.getTime());

		return date;
	}

	protected int[] getYearMonthDay(String dateString) {
		int day = Integer.parseInt(dateString.substring(0, 2));
		int month = Integer.parseInt(dateString.substring(3, 5));
		int year = Integer.parseInt(dateString.substring(6));
		return new int[] { year, month, day };
	}
	/**
	 * Retrive different locations for the report logo for HTML and PDF reports
	 * 
	 * @param reportFormat
	 * @param parameters
	 */
	protected void setReportLogoLocationObject(String reportFormat, Map<String, Object> parameters) {

		if (HTML.equals(reportFormat)) {
			reportsRootDir = "../../images/" + strLogo;
			parameters.put("IMG", reportsRootDir);

		} else if (PDF.equals(reportFormat)) {
			String strLogo = AppSysParamsUtil.getReportLogo(false);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
			String reportsIMGDir = getReportTemplate(strLogo);
			parameters.put("IMG", reportsIMGDir);

		}
	}

	/**
	 * Sets the prefered report format for US/Europe formats
	 * 
	 * @param prefRptFormat
	 * @param parameters
	 */
	protected void setPreferedReportFormatObject(String prefRptFormat, Map<String, Object> parameters) {
		parameters.put("LOCALE", "US");
		parameters.put("DELIMITER", ",");
		if (prefRptFormat != null && !prefRptFormat.equals("")) {
			if (prefRptFormat.equalsIgnoreCase("FR") || prefRptFormat.equalsIgnoreCase("IT")) {
				parameters.put("LOCALE", "IT");
				parameters.put("DELIMITER", ";");
			}
		}
	}
}
