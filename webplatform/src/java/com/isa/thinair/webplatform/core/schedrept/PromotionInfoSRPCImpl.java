package com.isa.thinair.webplatform.core.schedrept;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.schedrept.ScheduleReportUtil.ScheduleWebConstants;

public class PromotionInfoSRPCImpl extends ScheduleReportCommon {

	private static Log log = LogFactory.getLog(PromotionInfoSRPCImpl.class);

	@Override
	public ReportInputs composeReportInputs(HttpServletRequest request) {

		ReportsSearchCriteria search = new ReportsSearchCriteria();
		Map<String, String> parameters = new HashMap<String, String>();

		String fromDate = request.getParameter("departureDateFrm");
		String toDate = request.getParameter("departureDateTo");

		String id = "UC_REPM_084";
		User curUser = (User) request.getSession().getAttribute("sesCurrUser");
		String strUsrerId = curUser.getUserId();
		
		String strAgents = request.getParameter("hdnAgents");
		String strChannels = request.getParameter("hdnChannels");
		String strPromoTypes = request.getParameter("hdnPromoTypes");
		String strOnds = request.getParameter("hdnOnds");
		String strFlightNos = request.getParameter("hdnFlightNos");
		String strlive = request.getParameter(ScheduleWebConstants.REP_HDN_LIVE);

		String reportFormat = "CSV";
		String reportTempleteRelativePath = "linkReport";
		String reportTemplate = "PromoCodeDetailsReport.jasper";
		String selReportFormat = request.getParameter("radReportOption");
		String reportName = ScheduleReportUtil.ScheduledReportMetaDataIDs.PROMOTION_INFO_REPORT;

		reportTempleteRelativePath = getReportTemplateRelativeLocation(reportTemplate);

		try {
			search.setDepartureDateRangeFrom(fromDate);
			search.setDepartureDateRangeTo(toDate);
			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(convertDate(fromDate) + " 00:00:00");
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(convertDate(toDate) + " 23:59:59");
			}

			// if ((request.getParameter("bookedDateFrm") != null && !request.getParameter("bookedDateFrm").isEmpty())
			// && (request.getParameter("bookedDateTo") != null && !request.getParameter("bookedDateTo").isEmpty())) {
			// search.setDateRange2From(convertDate(request.getParameter("bookedDateFrm")) + " 00:00:00");
			// search.setDateRange2To(convertDate(request.getParameter("bookedDateTo")) + " 23:59:59");
			// }

			if (strUsrerId != null) {
				search.setUserId(strUsrerId);
			}

			if (!StringUtil.isNullOrEmpty(strPromoTypes) && !"[]".equals(strPromoTypes)) {
				String[] arrPromotions = strPromoTypes.split(",");
				Set<String> lstPromo = new HashSet<String>();
				for (String promoType : arrPromotions) {
					lstPromo.add(promoType);
				}
				search.setPromotionTypes(lstPromo);
			}

			if (!StringUtil.isNullOrEmpty(strOnds) && !"[]".equals(strOnds)) {
				String[] arrOnds = strOnds.split(",");
				Set<String> lstOnd = new HashSet<String>();
				for (String ond : arrOnds) {
					lstOnd.add(ond);
				}
				search.setSegmentCodes(lstOnd);
			}

			if (!StringUtil.isNullOrEmpty(strFlightNos) && !"[]".equals(strFlightNos)) {
				String[] arrFlightNo = strFlightNos.split(",");
				Set<String> lstFlightNo = new HashSet<String>();
				for (String flight : arrFlightNo) {
					lstFlightNo.add(flight);
				}
				search.setFlightNoCollection(lstFlightNo);
			}

			if (!StringUtil.isNullOrEmpty(strAgents) && !"[]".equals(strAgents)) {
				String[] arrAgents = strAgents.split(",");
				Collection<String> lstAgent = new HashSet<String>();
				for (String agentCode : arrAgents) {
					lstAgent.add(agentCode);
				}
				search.setAgents(lstAgent);
			}

			if (!StringUtil.isNullOrEmpty(strChannels) && !"[]".equals(strChannels)) {
				String[] arrChannels = strChannels.split(",");
				Collection<String> lstChannel = new HashSet<String>();
				for (String salesChannenl : arrChannels) {
					lstChannel.add(salesChannenl);
				}
				search.setSalesChannels(lstChannel);
			}

			if (strlive != null) {
				if (strlive.equals(ScheduleWebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(ScheduleWebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			if (selReportFormat != null && !selReportFormat.isEmpty()) {

				if (selReportFormat.trim().equals("HTML")) {
					reportFormat = "HTML";
				} else if (selReportFormat.trim().equals("PDF")) {
					reportFormat = "PDF";
				} else if (selReportFormat.trim().equals("EXCEL")) {
					reportFormat = "EXCEL";
				} else if (selReportFormat.trim().equals("CSV")) {
					reportFormat = "CSV";
				}
			}

		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}

		parameters.put("CURRENCY", AppSysParamsUtil.getBaseCurrency());
		parameters.put("ID", id);
		parameters.put(ScheduleWebConstants.REP_CARRIER_NAME, strCarrier);
		parameters.put("FROM_DATE", fromDate);
		parameters.put("TO_DATE", toDate);
		// parameters.put("IMG", getReportTemplateRelativeLocation(AppSysParamsUtil.getReportLogo(false)));

		return new ReportInputs(parameters, search, reportFormat, reportTempleteRelativePath, reportName);
	}

}
