package com.isa.thinair.webplatform.core.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ONDSearchDTO;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;

public class ReservationCommonUtil {

	private static final String CARRIER_SEPERATOR = "|";
	private static final String VERSION_SEPERATOR = "-";
	private static Log log = LogFactory.getLog(ReservationCommonUtil.class);

	private static boolean isCorrectCarrierVersion(String version) {
		String[] versions = StringUtils.split(version, CARRIER_SEPERATOR);
		for (String versionNumber : versions) {
			int indexOfAirlineCodeSeparator = versionNumber.lastIndexOf(VERSION_SEPERATOR);
			if (indexOfAirlineCodeSeparator == -1) {
				return false;
			}
		}
		return true;
	}

	public static String getCorrectInterlineVersion(String inVersion) {
		if (!isCorrectCarrierVersion(inVersion)) {
			return (AppSysParamsUtil.getDefaultCarrierCode() + VERSION_SEPERATOR + inVersion);
		}
		return inVersion;
	}

	public static Map<String, List<String>> getCarrierWiseRPHs(List<ONDSearchDTO> ondSearchList) {

		Map<String, List<String>> carrierWiseFlightRPHMap = new HashMap<String, List<String>>();
		try {
			Map<String, String> busAirCarrierCodes = WPModuleUtils.getFlightServiceBD().getBusAirCarrierCodes();
			if (ondSearchList != null && ondSearchList.size() > 0) {
				for (ONDSearchDTO ondSearchDTO : ondSearchList) {
					List<String> flightRPHList = ondSearchDTO.getFlightRPHList();
					if (flightRPHList != null && flightRPHList.size() > 0) {
						for (String flightRPH : flightRPHList) {
							String carrierCode;
							if (flightRPH.indexOf("#") != -1) {
								String arr[] = flightRPH.split("#");
								carrierCode = FlightRefNumberUtil.getOperatingAirline(arr[0]);
								// flightRPH = arr[0];
							} else {
								carrierCode = FlightRefNumberUtil.getOperatingAirline(flightRPH);
							}

							// get the respective airline carrier for bus segment
							if (busAirCarrierCodes.containsKey(carrierCode)) {
								carrierCode = busAirCarrierCodes.get(carrierCode);
							}

							if (carrierWiseFlightRPHMap.get(carrierCode) != null) {
								carrierWiseFlightRPHMap.get(carrierCode).add(flightRPH);
							} else {
								List<String> tmpFlightRPH = new ArrayList<String>();
								tmpFlightRPH.add(flightRPH);
								carrierWiseFlightRPHMap.put(carrierCode, tmpFlightRPH);
							}
						}
					}
				}

			}
		} catch (Exception ex) {
			log.error("ReservationCommonUtil getCarrierWiseRPHs caused Error", ex);
		}
		return carrierWiseFlightRPHMap;
	}

}
