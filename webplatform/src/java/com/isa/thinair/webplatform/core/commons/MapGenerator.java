package com.isa.thinair.webplatform.core.commons;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.core.util.LookupUtils;

/**
 * @author byorn
 * @author mekanayake
 * 
 */
public final class MapGenerator {

	public static final String COUNTRIES = "country";
	public static final String NATIONALITY = "nationality";
	public static final String AIRPORTS = "airport";
	public static final String LCCAIRPORTS = "lccAirportForDisplay";

	// For caching the airport list
	private static final long CASH_TIMEOUT = AppSysParamsUtil.getMasterDataCacheTimeout(); // can
																							// use
																							// for
																							// all
																							// the
																							// list

	private static Map<String, String> nationalityMap = new HashMap<String, String>();
	private static Map<String, String> countryMap = new HashMap<String, String>();
	private static Map<String, String> airportMap = new HashMap<String, String>();

	private static boolean refreshingNationalityMap = false;
	private static boolean refreshingCountryMap = false;
	private static boolean refreshingAirportMap = false;

	private static Date nationalityRecordAge = new Date(System.currentTimeMillis() - CASH_TIMEOUT);
	private static Date countryRecordAge = new Date(System.currentTimeMillis() - CASH_TIMEOUT);
	private static Date airportRecordAge = new Date(System.currentTimeMillis() - CASH_TIMEOUT);

	public final static Map<String, String> getNationalityMap() {
		if ((System.currentTimeMillis() - nationalityRecordAge.getTime()) >= CASH_TIMEOUT) {
			refreshNationalityMap();
		}

		if (!refreshingNationalityMap) {
			return nationalityMap;
		} else {
			synchronized (nationalityMap) {
				return nationalityMap;
			}
		}

	}

	public final static Map<String, String> getCountryMap() {
		if ((System.currentTimeMillis() - countryRecordAge.getTime()) >= CASH_TIMEOUT) {
			refreshCountryMap();
		}

		if (!refreshingCountryMap) {
			return countryMap;
		} else {
			synchronized (countryMap) {
				return countryMap;
			}
		}

	}

	public final static Map<String, String> getAirportMap() {
		if ((System.currentTimeMillis() - airportRecordAge.getTime()) >= CASH_TIMEOUT) {
			refreshAirportMap();
		}

		if (!refreshingAirportMap) {
			return airportMap;
		} else {
			synchronized (airportMap) {
				return airportMap;
			}
		}

	}

	private static void refreshNationalityMap() {
		synchronized (nationalityMap) {
			if ((System.currentTimeMillis() - nationalityRecordAge.getTime()) >= CASH_TIMEOUT) {
				refreshingNationalityMap = true;
				try {
					List<String[]> list = LookupUtils.LookupDAO().getTwoColumnMap(NATIONALITY);
					nationalityMap = getMap(list);
					nationalityRecordAge.setTime(System.currentTimeMillis());
				} finally {
					refreshingNationalityMap = false;
				}
			}
		}
	}

	private static void refreshCountryMap() {
		synchronized (countryMap) {
			if ((System.currentTimeMillis() - countryRecordAge.getTime()) >= CASH_TIMEOUT) {
				refreshingCountryMap = true;
				try {
					List<String[]> list = LookupUtils.LookupDAO().getTwoColumnMap(COUNTRIES);
					countryMap = getMap(list);
					countryRecordAge.setTime(System.currentTimeMillis());
				} finally {
					refreshingCountryMap = false;
				}
			}
		}
	}

	private static void refreshAirportMap() {
		synchronized (airportMap) {
			if ((System.currentTimeMillis() - airportRecordAge.getTime()) >= CASH_TIMEOUT) {
				refreshingAirportMap = true;
				try {
					List<String[]> list = LookupUtils.LookupDAO().getTwoColumnMap(AIRPORTS);
					if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
						List<String[]> lccList = LookupUtils.lccLookupDAO().getTwoColumnMap(LCCAIRPORTS);
						for (String[] lccRec : lccList) {
							boolean found = false;
							for (String[] rec : list) {
								if (lccRec[0].equals(rec[0])) {
									found = true;
									break;
								}
							}
							if (!found) {
								list.add(lccRec);
							}
						}
					}
					airportMap = getMap(list);
					airportRecordAge.setTime(System.currentTimeMillis());
				} finally {
					refreshingAirportMap = false;
				}
			}
		}
	}

	private static Map<String, String> getMap(List<String[]> list) {
		Map<String, String> map = new HashMap<String, String>();
		for (Iterator<String[]> iter = list.iterator(); iter.hasNext();) {
			String[] s = (String[]) iter.next();
			map.put(s[0], s[1]);
		}
		return map;
	}
}
