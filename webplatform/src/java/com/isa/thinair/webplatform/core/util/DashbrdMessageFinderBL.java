package com.isa.thinair.webplatform.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.webplatform.api.dto.DashboardMessageTO;
import com.isa.thinair.webplatform.api.dto.DashbrdMsgRetrivedDataTO;
import com.isa.thinair.webplatform.api.dto.DashbrdUserDataRetrivalTO;

/**
 * Dashboard Message Finder
 * 
 * @author Navod Ediriweera
 * @since 19 Aug 2010
 */
public class DashbrdMessageFinderBL {

	private DashbrdMsgVirtualDAO dashbrdMsgVirtualDAO;
	private static Log log = LogFactory.getLog(DashbrdMessageFinderBL.class);

	private DashbrdMsgRetrivedDataTO userVisibility;
	private DashbrdMsgRetrivedDataTO agentVisibility;
	private DashbrdMsgRetrivedDataTO agentTypeVisibility;
	private DashbrdMsgRetrivedDataTO posVisibility;

	private void setVisiblityData(DashbrdUserDataRetrivalTO dataRetrivalTO) {

		userVisibility = dashbrdMsgVirtualDAO.getMsgIDsForUser(dataRetrivalTO.getUserID());
		agentVisibility = dashbrdMsgVirtualDAO.getMsgIDsForAgent(dataRetrivalTO.getAgentCode());

		agentTypeVisibility = dashbrdMsgVirtualDAO.getMsgIDsForAgentType(dataRetrivalTO.getAgentType());

		posVisibility = dashbrdMsgVirtualDAO.getMsgIDsForPOS(dataRetrivalTO.getPosCode());
	}

	public Map<Integer, Collection<DashboardMessageTO>> getDisplayMessages(Date date, DashbrdUserDataRetrivalTO dataRetrivalTO) {
		try {
			// First retrieve All Message IDS
			Set<Integer> allDashbrdData = dashbrdMsgVirtualDAO.getAllDashbrdMsgIDs(date);
			if (allDashbrdData == null || allDashbrdData.size() == 0)
				return null;

			// Setting Visiblity TO
			this.setVisiblityData(dataRetrivalTO);

			Collection<Integer> mainExcludeSet = new HashSet<Integer>();
			Collection<Integer> mainIncludeSet = new HashSet<Integer>();

			mainExcludeSet.addAll(userVisibility.getExcludedIDSet());
			mainExcludeSet.addAll(agentVisibility.getExcludedIDSet());
			mainExcludeSet.addAll(agentTypeVisibility.getExcludedIDSet());
			mainExcludeSet.addAll(posVisibility.getExcludedIDSet());
			
			mainIncludeSet.addAll(userVisibility.getIncludedIDSet());
			mainIncludeSet.addAll(agentVisibility.getIncludedIDSet());
			mainIncludeSet.addAll(agentTypeVisibility.getIncludedIDSet());
			mainIncludeSet.addAll(posVisibility.getIncludedIDSet());

			mainIncludeSet.removeAll(mainExcludeSet);
			
			Set<Integer> finalMsgData = new HashSet<Integer>();
			for (Integer dashKey : allDashbrdData) {
				for (Integer incKey : mainIncludeSet) {
					if (dashKey.equals(incKey)) {
						finalMsgData.add(dashKey);
					}
				}
			}

			return dashbrdMsgVirtualDAO.getAllDashbrdMessages(new ArrayList<Integer>(finalMsgData));
		} catch (Exception e) {
			log.error("Error Loading Dashboard Messages " + e);
		}
		return null;

	}

	public DashbrdMsgVirtualDAO getDashbrdMsgVirtualDAO() {
		return dashbrdMsgVirtualDAO;
	}

	public void setDashbrdMsgVirtualDAO(DashbrdMsgVirtualDAO dashbrdMsgVirtualDAO) {
		this.dashbrdMsgVirtualDAO = dashbrdMsgVirtualDAO;
	}

}
