package com.isa.thinair.webplatform.core.schedrept;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.schedrept.ScheduleReportUtil.ScheduleWebConstants;

public class CCLMSTransactionReportSRPCImpl extends ScheduleReportCommon {

	@Override
	public ReportInputs composeReportInputs(HttpServletRequest request) {

		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String reportOption = request.getParameter("ccReportName");
		boolean isDetailReport = new Boolean(request.getParameter("hdnDetail"));

		String id = "UC_REPM_097";
		String reportTemplate = "";
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../../images/" + AppSysParamsUtil.getReportLogo(true);
		String strlive = request.getParameter(ScheduleWebConstants.REP_HDN_LIVE);

		Map<String, Object> parameters = new HashMap<String, Object>();
		String reportFormat = setReportExportType(value);
		ReportsSearchCriteria search = new ReportsSearchCriteria();
		String reportTempleteRelativePath = null;

		try {
			search.setReportOption(reportOption);
			if (fromDate != null && !fromDate.equals("")) {
				if (reportOption.equals("CC_LMS_TRAN_DETAIL")) {
					search.setDateRangeFrom(convertDateFromDetail(fromDate));
				} else {
					search.setDateRangeFrom(convertDate(fromDate));
				}
			}
			if (toDate != null && !toDate.equals("")) {
				if (reportOption.equals("CC_LMS_TRAN_DETAIL")) {
					search.setDateRangeTo(convertDateFromDetail(toDate));
				} else {
					search.setDateRangeTo(convertDate(toDate));
				}
			}

			if (strlive != null) {
				if (strlive.equals(ScheduleWebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(ScheduleWebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			boolean isIncludeAdvanceCCDetails = true;
			search.setIncludeAdvanceCCDetails(isIncludeAdvanceCCDetails);

			if (reportOption.equals("CC_LMS_TRAN") && isDetailReport) {
				reportFormat = setReportExportType("CSV");
				reportTemplate = "CCLMSTransactionsDetailTR.jasper";
				search.setDetailReport(isDetailReport);
				reportTempleteRelativePath = getReportTemplateRelativeLocation(reportTemplate);
			} else if (reportOption.equals("CC_LMS_TRAN")) {
				reportTemplate = "CCLMSTransactions.jasper";
				reportTempleteRelativePath = getReportTemplateRelativeLocation(reportTemplate);
			} else if (reportOption.equals("CC_LMS_TRAN_DETAIL")) {
				reportTemplate = "CCLMSTransactionsDetailTR.jasper";
				reportTempleteRelativePath = getReportTemplateRelativeLocation(reportTemplate);
			}

			this.setReportLogoLocationObject(reportFormat, parameters);

			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			parameters.put(ScheduleWebConstants.REP_CARRIER_NAME, strCarrier);

			String reportNumFormat = request.getParameter("radRptNumFormat");
			setPreferedReportFormatObject(reportNumFormat, parameters);

		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}
		return new ReportInputs(parameters, search, reportFormat, reportTempleteRelativePath,
				ScheduleReportUtil.ScheduledReportMetaDataIDs.CC_LMS_TRANSACTIONS_REPORT);

	}

}
