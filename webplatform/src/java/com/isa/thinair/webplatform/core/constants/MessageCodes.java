package com.isa.thinair.webplatform.core.constants;

public interface MessageCodes {
	public static final String COMMON_CREDITCARD_CHARGE_NULL = "common.creditcard.charge.null";
	public static final String INVALID_PAYMENT_CURRENCY = "module.invalid.payment.currency";
	public static final String CURRENCY_EXCHANGE_RATE_UNDEFINED = "airmaster.exchangerate.undefined";
	public static final String UNAUTHORIZED_ACCESS_TO_SCREEN = "module.unauthorized.access";
	public static final String CURRENCY_DEFAULT_PAYMENT_GATEWAY_NULL = "currency.default.paymentgatey.null";
	public static final String COMMON_FLEXI_CHARGE_NULL = "common.flexi.charge.null";
	public static final String PAX_DOB_RANGE_CONFLICT = "airreservations.dob.range.conflict";
	public static final String PAX_INFANT_DOB_MIN_RANGE = "airreservations.infant.dob.min.range";
	public static final String PAX_PASSPORT_EXPIRED = "airreservations.pax.passport.expired";
}
