/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.webplatform.core.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.isa.thinair.airreservation.api.utils.BeanUtils;

/**
 * Class to access database for the usage of combobox data
 * 
 * @author Byorn
 */
public class WebplatformDAO {

	private static Log log = LogFactory.getLog(WebplatformDAO.class);

	/** data souce to get system params */
	private DataSource dataSource;

	/** properties to store queries for combos * */
	private Properties queryStringForCombo;

	/** properties to store queries retrive column values * */
	private Properties selectSql;

	/** method that executes a sql query and returns a collections * */
	@SuppressWarnings("unchecked")
	public List<Map<String, String>> getList(String moduleProperty) {

		List<Map<String, String>> list = null;
		if (dataSource != null && queryStringForCombo != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			String queryString = queryStringForCombo.getProperty(moduleProperty);
			list = transformCastDifferences(templete.queryForList(queryString));
			return list;
		}
		return list;

	}

	/** method that executes a sql query and returns a collections * */
	@SuppressWarnings("unchecked")
	public List<Map<String, String>> getList(String moduleProperty, Object[] params) {

		List<Map<String, String>> list = null;
		if (dataSource != null && queryStringForCombo != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			String queryString = queryStringForCombo.getProperty(moduleProperty);
			list = transformCastDifferences(templete.queryForList(queryString, params));
			return list;
		}
		return list;

	}

	/**
	 * method that executes a sql query and returns a Map, query returns Three String input *
	 */
	@SuppressWarnings("unchecked") public Map<String, Map<String, String>> getMap(String moduleProperty) {
		return getMap(moduleProperty, false);
	}

	/**
	 * method that executes a sql query and returns a Map preserving the order , query returns Three String input *
	 */
	@SuppressWarnings("unchecked") public Map<String, Map<String, String>> getMap(String moduleProperty, boolean preserveOrder) {

		Map<String, Map<String, String>> map = null;
		List<String[]> list = getThreeColumnMap(moduleProperty);
		if (list != null && !list.isEmpty()) {
			map = transformToMap(list, preserveOrder);
		}
		return map;

	}

	private List<Map<String, String>> transformCastDifferences(List<Map<String, Object>> lstMap) {
		List<Map<String, String>> transformList = new ArrayList<Map<String, String>>();

		if (lstMap != null && lstMap.size() > 0) {
			for (Map<String, Object> element : lstMap) {
				Map<String, String> tmpMap = new LinkedHashMap<String, String>();

				for (Entry<String, Object> entry : element.entrySet()) {
					String key = BeanUtils.nullHandler(entry.getKey());
					String value = BeanUtils.nullHandler(entry.getValue());

					tmpMap.put(key, value);
				}

				transformList.add(tmpMap);
			}
		}

		return transformList;
	}

	private Map<String, Map<String, String>> transformToMap(List<String[]> list, boolean preserveOrder) {
		Map<String, Map<String, String>> transformMap = preserveOrder ? new LinkedHashMap<>() : new HashMap<>();
		if (list != null && list.size() > 0) {
			for (String[] strArr : list) {
				if (!transformMap.containsKey(strArr[0])) {
					transformMap.put(strArr[0], preserveOrder ? new LinkedHashMap<>() : new HashMap<>());
				}
				transformMap.get(strArr[0]).put(strArr[1], strArr[2]);
			}
		}
		return transformMap;
	}

	/**
	 * 
	 * @param moduleProperty
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<String[]> getList(String moduleProperty, Object[] params, int colCount) {
		JdbcTemplate jt = new JdbcTemplate(dataSource);

		String sql = selectSql.getProperty(moduleProperty);

		if (log.isDebugEnabled() && params != null) {
			log.debug("params=" + params.toString() + ";colCount=" + colCount);
			log.debug(sql);
		}

		PreparedStatementCreator psCreator = new BasicPreparedStatementCreator(sql, params);
		ResultSetExtractor rsExtracter = new BasicResultSetExtractor(colCount);


		return (Collection<String[]>) jt.query(psCreator, rsExtracter);

	}

	public Collection<String[]> getListForParameterList(String moduleProperty, Map params, int colCount) {
		NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(dataSource);
		String sql = selectSql.getProperty(moduleProperty);
		ResultSetExtractor rsExtracter = new BasicResultSetExtractor(colCount);
		return (Collection<String[]>) template.query(sql,params,rsExtracter);
	}
	/**
	 * Method will return the coulmns which select by the Sql String
	 * 
	 * @param moduleProperty
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<String[]> getList1(String sql, int colCount) {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		ResultSetExtractor rsExtracter = new BasicResultSetExtractor(colCount);
		return (Collection<String[]>) jt.query(sql, rsExtracter);

	}

	/**
	 * @param moduleProperty
	 * @param colCount
	 * @return Collection
	 */
	@SuppressWarnings("unchecked")
	public Collection<String[]> getList2(String moduleProperty, int colCount) {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		String sql = selectSql.getProperty(moduleProperty);
		ResultSetExtractor rsExtracter = new BasicResultSetExtractor(colCount);
		return (Collection<String[]>) jt.query(sql, rsExtracter);
	}

	/**
	 * 
	 * @param moduleProperty
	 * @return
	 */
	public String getStrObject(String moduleProperty, String[] params) {
		JdbcTemplate jt = new JdbcTemplate(dataSource);

		String sql = selectSql.getProperty(moduleProperty);

		if (log.isDebugEnabled() && params != null) {
			log.debug("params=" + params.toString());
			log.debug(sql);
		}
		return (String) jt.queryForObject(sql, params, String.class);

	}

	public List<String> getSingleColumnMap(String moduleProperty, Object[] params) {

		final List<String> list = new ArrayList<String>();

		if (dataSource != null && queryStringForCombo != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			String queryString = queryStringForCombo.getProperty(moduleProperty);
			templete.query(queryString, params, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					// if result set is not null
					if (rs != null) {
						// populate record
						while (rs.next()) {
							String s = rs.getObject(1) != null ? rs.getObject(1).toString() : "";
							list.add(s);
						}
					}
					return null;
				}
			});
		}
		return list;
	}
	
	public List<String> getSingleColumnMap(String moduleProperty) {

		final List<String> list = new ArrayList<String>();

		if (dataSource != null && queryStringForCombo != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			String queryString = queryStringForCombo.getProperty(moduleProperty);
			templete.query(queryString, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					// if result set is not null
					if (rs != null) {
						// populate record
						while (rs.next()) {
							String s = rs.getObject(1) != null ? rs.getObject(1).toString() : "";
							list.add(s);
						}
					}
					return null;
				}
			});
		}
		return list;
	}

	/**
	 * 
	 * @param moduleProperty
	 * @return
	 */
	public List<String[]> getFourColumnMap(String moduleProperty) {

		final List<String[]> list = new ArrayList<String[]>();

		if (dataSource != null && queryStringForCombo != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			String queryString = queryStringForCombo.getProperty(moduleProperty);
			templete.query(queryString, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					// if result set is not null
					if (rs != null) {

						// populate recordMap
						while (rs.next()) {
							String s[] = { rs.getObject(1) != null ? rs.getObject(1).toString() : "",
									rs.getObject(2) != null ? rs.getObject(2).toString() : "",
									rs.getObject(3) != null ? rs.getObject(3).toString() : "",
									rs.getObject(4) != null ? rs.getObject(4).toString() : "" };
							list.add(s);
						}
					}
					return null;
				}
			});
		}
		return list;
	}

	public List<String[]> getFiveColumnData(String sql) {

		final List<String[]> list = new ArrayList<String[]>();

		if (dataSource != null && queryStringForCombo != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			String queryString = sql;
			templete.query(queryString, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					// if result set is not null
					if (rs != null) {

						// populate recordMap
						while (rs.next()) {
							String s[] = { rs.getObject(1) != null ? rs.getObject(1).toString() : "",
									rs.getObject(2) != null ? rs.getObject(2).toString() : "",
									rs.getObject(3) != null ? rs.getObject(3).toString() : "",
									rs.getObject(4) != null ? rs.getObject(4).toString() : "",
									rs.getObject(5) != null ? rs.getObject(5).toString() : "" };
							list.add(s);
						}
					}
					return null;
				}
			});
		}
		return list;
	}

	public List<String[]> getFiveColumnDataProp(String moduleProperty) {

		final List<String[]> list = new ArrayList<String[]>();

		if (dataSource != null && queryStringForCombo != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			// String queryString = sql;
			String queryString = queryStringForCombo.getProperty(moduleProperty);
			templete.query(queryString, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					// if result set is not null
					if (rs != null) {

						// populate recordMap
						while (rs.next()) {
							String s[] = { rs.getObject(1) != null ? rs.getObject(1).toString() : "",
									rs.getObject(2) != null ? rs.getObject(2).toString() : "",
									rs.getObject(3) != null ? rs.getObject(3).toString() : "",
									rs.getObject(4) != null ? rs.getObject(4).toString() : "",
									rs.getObject(5) != null ? rs.getObject(5).toString() : "" };
							list.add(s);
						}
					}
					return null;
				}
			});
		}
		return list;
	}

	public List<String[]> getThreeColumnMap(String moduleProperty) {

		final List<String[]> list = new ArrayList<String[]>();

		if (dataSource != null && queryStringForCombo != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			String queryString = queryStringForCombo.getProperty(moduleProperty);
			templete.query(queryString, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					// if result set is not null
					if (rs != null) {

						// populate recordMap
						while (rs.next()) {
							String s[] = { rs.getObject(1) != null ? rs.getObject(1).toString() : "",
									rs.getObject(2) != null ? rs.getObject(2).toString() : "",
									rs.getObject(3) != null ? rs.getObject(3).toString() : "" };
							list.add(s);
						}
					}
					return null;
				}
			});
		}
		return list;
	}

	/**
	 * 
	 * @param moduleProperty
	 * @return
	 */
	public List<String[]> getTwoColumnMap(String moduleProperty) {

		final List<String[]> list = new ArrayList<String[]>();

		if (dataSource != null && queryStringForCombo != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			String queryString = queryStringForCombo.getProperty(moduleProperty);
			templete.query(queryString, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					// if result set is not null
					if (rs != null) {

						// populate recordMap
						while (rs.next()) {
							String s[] = { rs.getObject(1) != null ? rs.getObject(1).toString() : "",
									rs.getObject(2) != null ? rs.getObject(2).toString() : "" };
							list.add(s);
						}
					}
					return null;
				}
			});
		}
		return list;
	}

	public List<String[]> getTwoColumnMap(String moduleProperty, Object[] obj, int[] types) {

		final List<String[]> list = new ArrayList<String[]>();

		if ((dataSource != null) && (queryStringForCombo != null)) {
			JdbcTemplate template = new JdbcTemplate(dataSource);
			String queryString = queryStringForCombo.getProperty(moduleProperty);
			template.query(queryString, obj, types, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					if (rs != null) {
						while (rs.next()) {
							String s[] = { rs.getObject(1) != null ? rs.getObject(1).toString() : "",
									rs.getObject(2) != null ? rs.getObject(2).toString() : "" };
							list.add(s);
						}
					}
					return null;
				}
			});
		}
		return list;
	}

	public List<String[]> getTwoColumnMap(String moduleProperty, String obj) {

		final List<String[]> list = new ArrayList<String[]>();

		if ((dataSource != null) && (queryStringForCombo != null)) {
			JdbcTemplate template = new JdbcTemplate(dataSource);
			String queryString = queryStringForCombo.getProperty(moduleProperty);
			queryString = queryString + " (" + obj + ")";// todo - Gayan Make reusable function to support this type
															// list
			template.query(queryString, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					if (rs != null) {
						while (rs.next()) {
							String s[] = { rs.getObject(1) != null ? rs.getObject(1).toString() : "",
									rs.getObject(2) != null ? rs.getObject(2).toString() : "" };
							list.add(s);
						}
					}
					return null;
				}
			});
		}
		return list;
	}

	/**
	 * 
	 * @param moduleProperty
	 * @return
	 */
	public List<String[]> getTwoColumnsData(String sql) {

		final List<String[]> list = new ArrayList<String[]>();

		if (dataSource != null && queryStringForCombo != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			String queryString = sql;
			templete.query(queryString, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					// if result set is not null
					if (rs != null) {

						// populate recordMap
						while (rs.next()) {
							String s[] = { rs.getObject(1) != null ? rs.getObject(1).toString() : "",
									rs.getObject(2) != null ? rs.getObject(2).toString() : "" };
							list.add(s);
						}
					}
					return null;
				}
			});
		}
		return list;
	}

	public List<String[]> getThreeColumnData(String sql) {
		final List<String[]> list = new ArrayList<String[]>();

		if (dataSource != null && queryStringForCombo != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			String queryString = queryStringForCombo.getProperty(sql);
			// String queryString = sql;
			templete.query(queryString, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					// if result set is not null
					if (rs != null) {

						// populate recordMap
						while (rs.next()) {
							String s[] = { rs.getObject(1) != null ? rs.getObject(1).toString() : "",
									rs.getObject(2) != null ? rs.getObject(2).toString() : "",
									rs.getObject(3) != null ? rs.getObject(3).toString() : "" };
							list.add(s);
						}
					}
					return null;
				}
			});
		}
		return list;
	}

	public List<String[]> getFourColumnData(String sql) {
		final List<String[]> list = new ArrayList<String[]>();

		if (dataSource != null && queryStringForCombo != null) {
			JdbcTemplate templete = new JdbcTemplate(dataSource);
			String queryString = sql;
			templete.query(queryString, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					// if result set is not null
					if (rs != null) {

						// populate recordMap
						while (rs.next()) {
							String s[] = { rs.getObject(1) != null ? rs.getObject(1).toString() : "",
									rs.getObject(2) != null ? rs.getObject(2).toString() : "",
									rs.getObject(3) != null ? rs.getObject(3).toString() : "",
									rs.getObject(4) != null ? rs.getObject(4).toString() : "" };
							list.add(s);
						}
					}
					return null;
				}
			});
		}
		return list;
	}

	/**
	 * 
	 * @param moduleProperty
	 * @return
	 */
	public String[] getString(String moduleProperty, String[] params, String[] col) {
		JdbcTemplate jt = new JdbcTemplate(dataSource);

		String sql = selectSql.getProperty(moduleProperty);

		if (log.isDebugEnabled() && params != null) {
			log.debug("params=" + params.toString() + ";colCount=");
			log.debug(sql);
		}

		PreparedStatementCreator psCreator = new BasicPreparedStatementCreator(sql, params);
		ResultSetExtractor rsExtracter = new BasicSingleResultExtractor(col);
		return (String[]) jt.query(psCreator, rsExtracter);

	}
	
	/**
	 * @return Returns the dataSource.
	 */
	public DataSource getDataSource() {
		return dataSource;
	}

	/**
	 * @param dataSource
	 *            The dataSource to set.
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * @return Returns the properties of sql.
	 */
	public Properties getQueryStringForCombo() {
		return queryStringForCombo;
	}

	public void setQueryStringForCombo(Properties queryStringForCombo) {
		this.queryStringForCombo = queryStringForCombo;
	}

	public Properties getSelectSql() {
		return selectSql;
	}

	public void setSelectSql(Properties selectSql) {
		this.selectSql = selectSql;
	}
}
