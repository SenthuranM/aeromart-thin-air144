package com.isa.thinair.webplatform.core.commons;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;

import com.isa.thinair.airmaster.api.dto.CachedAirlineDTO;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.PaxFareSegmentTypes;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentStatus;
import com.isa.thinair.airsecurity.api.model.Role;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentType;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.BatchStatus;
import com.isa.thinair.commons.api.constants.CommonsConstants.CarrierServiceType;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateJourneyType;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateReservationFlows;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.constants.CommonsConstants.ModifyOperation;
import com.isa.thinair.commons.api.constants.CommonsConstants.SearchType;
import com.isa.thinair.commons.api.dto.BasePaxConfigDTO;
import com.isa.thinair.commons.api.dto.ChargeApplicabilityTO;
import com.isa.thinair.commons.api.dto.ChargeRateBasis.ChargeBasisEnum;
import com.isa.thinair.commons.api.dto.FareCategoryTO;
import com.isa.thinair.commons.api.dto.FlightTypeDTO;
import com.isa.thinair.commons.api.dto.IBEPaxConfigDTO;
import com.isa.thinair.commons.api.dto.PaxCategoryTO;
import com.isa.thinair.commons.api.dto.ReturnValidityPeriodTO;
import com.isa.thinair.commons.api.dto.SSRInfoDTO;
import com.isa.thinair.commons.api.dto.XBEPaxConfigDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.SubStationUtil;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.GDS;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.MessageType;
import com.isa.thinair.gdsservices.api.util.GDSExternalCodes.ProcessStatus;
import com.isa.thinair.invoicing.api.model.bsp.BSPTransactionDataDTO;
import com.isa.thinair.messaging.core.config.MailServerConfig;
import com.isa.thinair.promotion.api.model.AnciOfferType;
import com.isa.thinair.webplatform.api.util.Constants;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;
import com.isa.thinair.webplatform.core.service.ModuleServiceLocator;
import com.isa.thinair.webplatform.core.util.LookupUtils;

/**
 * @author byorn
 * 
 */
public final class SelectListGenerator {

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	private static final String Countrys = "country";

	private static final String Language = "language";

	private static final String AgentsOfAllAirports = "agentsOfAllAirports";

	private static final String AGENT_TYPE_ROLES = "AgentTypeRole";

	private static final String ROLE_AGENT_TYPE = "RoleAgentType";

	private static final String CARRIER_LIST = "carrierList";

	private static final String ACTIVE_CARRIER_LIST = "actCarrierList";

	private static final String Nationality = "nationality";

	private static final String NationalityCodes = "nationalityList";

	private static final String Station = "stations";
	
	private static final String Cities = "cities";

	private static final String StationForGSA = "stationsForGsa";

	private static final String OWNER_AGENT = "ownstationsForTA";

	private static final String filghtNumbers = "filghtNumbers";

	private static final String filghtNumbersWithoutBus = "filghtNumbersWithoutBus";

	private static final String chargeTemplate = "chargeTemplate";

	private static final String chargeMealTemplateIDs = "chargeMealTemplateIDs";

	private static final String chargeBaggageTemplateIDs = "chargeBaggageTemplateIDs";

	private static final String chargeTemplateIDs = "chargeTemplateIDs";

	private static final String stationList = "stationList";

	private static final String BookingOriginatedSalesChannels = "bookingOriginatedSalesChannels";

	private static final String ownStationList = "ownStationList";

	private static final String stationListAct = "stationListAct";

	private static final String agentsByType = "agentsByType";
	
	private static final String agentsByTypeAndStatus = "agentsByTypeAndStatus";

	private static final String allRoles = "allRoles";

	private static final String allPrivileges = "allPrivileges";
	
	private static final String actPrivileges = "actPrivileges";

	private static final String ownAgentsByType = "ownAgentsByType";

	private static final String agentsByStation = "agentsByStation";
	
	private static final String agentsByStationAndStatus = "agentsByStationAndStatus";

	private static final String ownAgentsByStation = "ownAgentsByStation";

	private static final String agentsByStationActive = "agentsByStationActive";

	private static final String agentsByStationAll = "agentsByStationAll";

	private static final String taByStation = "taByStation";
	
	private static final String taByStationAndStatus = "taByStationAndStatus";

	private static final String ownTaByStation = "ownTaByStation";

	private static final String agentsGSATA = "agentsGSATA";

	private static final String agentsGSATAByStatus = "agentsGSATAByStatus";
	
	private static final String agentsGSATAV2 = "agentsGSATAV2";

	private static final String ownAgentsGSATA = "ownAgentsGSATA";

	private static final String agentTypes = "agentTypes";

	private static final String agentSubTypes = "agentSubTypes";

	private static final String tasfromGSACode = "tasfromGSACode";

	private static final String tasfromStationGSACode = "tasfromStationGSACode";

	private static final String agentDescription = "tafromTaCode";

	private static final String tafromStationTaCode = "tafromStationTaCode";

	private static final String agentTypesWithCode = "agentTypesWithCode";

	private static final String Airports = "airport";

	private static final String AirportsWithStatus = "airportsWithStatus";

	private static final String StationsWithStatus = "stationsWithStatus";

	private static final String StationsWithActiveStatus = "stationsWithActiveStatus";

	private static final String activeAirports = "activeAirports";
	
	private static final String adminFeeAgents = "adminFeeAgents";

	private static final String activeNonSurfaceAirports = "activeNonSurfaceAirports";

	private static final String airportDescODDesc = "airportDescODDesc";

	private static final String airportDescIBE = "airportDescIBE";

	private static final String airportForDisplay = "airportForDisplay"; // Haider

	private static final String airportNameForDisplay = "airportNameForDisplay";

	private static final String currencyForDisplay = "currencyForDisplay"; // Haider

	private static final String airportDescXBE = "airportDescXBE";
	
	private static final String CITY_DESCRIPTION_XBE = "cityDescXBE";
	
	private static final String LCC_CITY_DESCRIPTION_XBE = "lccCityForDisplay";

	private static final String agentWiseOnds = "agentWiseOnds";

	private static final String lccAirportDesc = "lccAirportForDisplay";

	private static final String lccAirportDescXBE = "lccAirportForDisplay";

	private static final String lccAirportsForManifest = "lccAirports";

	private static final String Territorys = "territory";

	private static final String BookingCodes = "bookingcode";
	
	private static final String EventCodeList = "eventcodeList";

	private static final String BookingClass = "bookingClass";

	private static final String AgentNamesForGsa = "agentNamesForGsa";

	private static final String BookingCodeList = "bookingcodes";
	
	// Get ACTIVE booking classes
	private static final String ActiveBookingCodeList = "activebookingcode";

	private static final String COSWiseBookingCodeList = "cosWiseBookingcodes";

	private static final String BasisCodesList = "basisCodes";

	private static final String FareVisibilitysRestricted = "farevisibilityRestricted";

	private static final String Agent = "agents";

	private static final String AgentAgainstId = "agentsAgainstId";

	private static final String ChargeCodes = "charge";

	private static final String bundledFareChargeCodes = "bundledFareCharge";

	private static final String ChargeGroups = "chargegroup";

	private static final String chargeGroupsDisplayList = "chargeGroupsDisplayList";

	private static final String ChargeCategorys = "chargecategory";

	private static final String PaymentPurpose = "paymentpurpose";

	private static final String CollectionTypes = "collectiontypes";

	private static final String CountryList = "countrylist";

	private static final String CountryNameList = "countryNameList";

	private static final String AgentTypeList = "agenttypelist";
	
	private static final String SubAgentTypeList = "subAgentTypelist";
	
	private static final String ReportingAgentTypelist = "reportingAgentTypelist";

	private static final String GivenReportingAgentList = "givenRepotingAgents";

	private static final String ServiceChannels = "servicechannels";

	private static final String DryUserLevels = "dryUserLevels";

	private static final String TerritoryListDesc = "territorylistdesc";

	private static final String AgentList = "agentlist";

	private static final String CabinClasses = "cabinClasses";

	private static final String LogicalCabinClasses = "logicalCabinClasses";

	private static final String cabinClassesList = "cabinClassesList";
	
	private static final String bundleDescTemplateList = "bundleDescTemplateList";

	private static final String allCOSList = "allCOSList";

	private static final String logicalCabinClassesList = "logicalCabinClassesList";

	private static final String ccWiselogicalCCList = "ccWiselogicalCCList";

	private static final String iataCodeList = "iataCodeList";

	private static final String iataCodeListForBaggage = "iataCodeListForBaggage";

	private static final String mealTemplateList = "mealTemplateList";

	private static final String activeMealTemplateList = "activeMealTemplateList";

	private static final String mealList = "mealList";

	private static final String mealCodeList = "mealCodeList";

	private static final String mealCategoryList = "mealCategoryList";

	private static final String fareRule = "fareRule";

	private static final String fareRuleCodes = "fareRuleCodes";

	private static final String fareRuleNewCodes = "fareRuleNewCodes";

	private static final String fareRuleNewIDs = "fareRuleNewIDs";

	private static final String fareRuleIDType = "fareRuleIDType";

	private static final String fareRuleCodesList = "fareRuleCodesList";

	private static final String freeFlexiAttachedFareCount = "freeFlexiAttachedFareCount";

	private static final String AllAgents = "allAgents";

	private static final String AllAgentsWOINT = "allAgentsWOINT";

	private static final String AllOwnAirlineAgentsWOINT = "allOwnAirlineAgentsWOINT";

	private static final String AllGSAList = "AllGSAList";

	private static final String getGSA = "getGSA";
	
	private static final String getAllReportableAgents = "getAllReportableAgents";
	
	private static final String getReportableForGSA = "getReportableForGSA";
	
	private static final String getReportableForSGSA = "getReportableForSGSA";
	
	private static final String getReportableForTA = "getReportableForTA";
	
	private static final String ALL_AGENTS_TYPES = "allAgentswithType";

	private static final String AGENTS_TYPES_BY_AIRLINE = "agentswithTypeFilteredByAirline";

	private static final String AGENTS_TYPES_BY_AIRLINE_STATION = "stationAgentsWithTypeFilteredByAirline";

	private static final String AgentsCashSales = "agentsCashSales";

	// private static final String AgentsOnAccount = "agentsOnAccount";

	private static final String OperationType = "operationType";

	private static final String operationTypeSchedule = "operationTypeSchedule";

	private static final String ScheduleStatus = "scheduleStatus";

	private static final String gdsList = "gdsList";

	private static final String BuildStatus = "buildStatus";

	private static final String MinStopoverTime = "minStopoverTime";

	private static final String CurrencyCode = "currencycode";

	private static final String RegionCode = "regioncode";

	private static final String xActiveCurrencyCodes = "xActiveCurrencyCodes";

	private static final String CURRENCYACTIVEIBE = "currencyActiveIbe";

	private static final String CURRENCYACTIVEXBE = "currencyActiveXbe";

	private static final String FAREDISCOUNTCODES = "fareDiscountCodes";

	private static final String FlightStatus = "flightStatus";

	private static final String CheckinStatus = "checkinStatus";

	private static final String SINGLE_QUOTE = "%27";

	private static final String AgentCombo = "agentCombo";

	private static final String GSACombo = "gsaCombo";

	private static final String AirportCombo = "airportCombo";

	private static final String TerritoryCombo = "territoryCombo";

	private static final String OwnAirlineTerritoryCombo = "ownAirlineTerritoryCombo";

	private static final String TerritoryForGSA = "territoryforGSA";

	private static final String OwnTerritoryForGSA = "ownTerritoryforGSA";

	private static final String AirportTerritory = "airportTerritory";

	private static final String AirportTerritoryGSA = "airportTerritoryGSA";
	
	private static final String GSATerritoryStations = "gsaTerritoryStations";

	private static final String SalesChannels = "salesChannels";

	private static final String StationCountry = "stationCountry";

	private static final String StationCurrency = "stationCurrency";

	private static final String StationCodeCountryCode = "stationCodeCountryCode";

	private static final String StationCountryGSA = "stationCountryGSA";

	private static final String REPORTING_AGENTS_WITH_CREDIT = "reportingTAsWithCredit";

	private static final String StationCodes = "stationCodes";

	private static final String PGW_DATA = "pgwData";

	private static final String StationCountryCode = "stationCountryCode";

	private static final String PARAM_CARRIER_CODES = "paramCarrierCodes";

	private static final String PARAM_DESCRIPTIONS = "paramDescriptions";

	private static final String PARAM_KEYS = "paramKeys";

	private static final String PARAM_TYPES = "paramTypes";

	private static final String CABIN_CLASS_CODES = "cabinClassCodes";

	private static final String LOGICAL_CABIN_CLASS_CODES = "logicalCabinClassCodes";

	private static final String CABIN_CLASS_RANKING_MAP = "cabinClassRanking";

	private static final String CREDIT_CARD_TYPES = "creditCardTypes";

	private static final String CREDIT_CARD_TYPES_ALPHA = "creditCardTypesAlpha";

	private static final String CREDIT_CARD_TYPES_FOR_PG = "creditCardTypesForPg";

	private static final String CREDIT_CARD_TYPES_FOR_ALL_PGW = "creditCardTypesForAllPgw";

	private static final String PAX_TITLES = "paxTitles";

	private static final String PAX_TITLES_ADULT = "paxTitlesAdult";

	private static final String PAX_TITLES_CHILD = "paxTitlesChild";

	private static final String PAX_TITLES_INFANT = "paxTitlesInfant";

	private static final String PAX_TYPES = "paxTypes";

	private static final String DIRECT_ROUT_USAGE_CHECK = "directRouteUsageCheck";

	private static final String FIXED_BC_FARERULE_CHECK = "fixedBCFareRuleCheck";

	private static final String ROUTS_IN_USE = "routesInUse";

	private static final String PAX_TITLES_VISIBLE = "paxTitlesVisible";

	private static final String PAX_CHILD_TITLES_VISIBLE = "paxTitlesForChildVisible";

	private static final String PAX_ADULT_TITLES_VISIBLE = "paxTitlesForAdultVisible";

	private static final String chargeGroupsDisplay = "chargeGroupsDisplay";

	// private static final String SSR_CODES = "ssrCodes";

	private static final String SSR_CODES_IN_USE = "ssrCodesInuse";

	private static final String AIRPORT_TRANSFER_CODES = "airportTransferCodes";

	private static final String ONLINE_AIRPORTS = "onlineAirports";

	private static final String SECREAT_QUESTION = "SecreatQuestion";

	private static final String AIRCRAFT_MODELS = "aircraftModels";

	private static final String ACTIVE_AIRCRAFT_MODELS = "activeAircraftModels";

	private static final String AIRCRAFT_MODELS_WITH_SEAT = "aircraftModelsWithSeats";

	private static final String ALERT_ACTIONS = "alertActions";

	private static final String ALL_AGENTS = "allAgents";

	private static final String ALL_ACTIVE_AGENTS = "allActiveAgents";

	private static final String ALL_ACTIVE_OWN_AGENTS = "allActiveOwnAgents";

	private static final String ALL_IBEIPGS = "ibeipgs";

	private static final String ALL_XBEIPGS = "xbeipgs";
	
	private static final String ALL_OFFLINE_XBEIPGS = "xbeofflineipgs";

	private static final String MODIFICATION_ENABLED_OFFLINE_XBEIPGS = "xbeOfflineModificationsEnabledIpgs";

	private static final String ALL_AGENTS_WITH_CREDITS = "allAgentsWithCredit";

	private static final String AGENT_USERS = "agentUsers";

	private static final String STATION_COUNTRY = "countryStation";

	private static final String COUNTRY_ALL = "countryAll";

	private static final String reporing_Charge_Groups = "reportingChargeGroups";

	private static final String chargegroupsForBC = "chargegroupsForBC";

	private static final String USER_ID_LIST = "userIdList";

	private static final String TASK_GROUP_LIST = "taskGroupList";

	private static final String TASK_LIST = "taskList";

	private static final String ALL_TASKS_LIST = "allTasksList";

	private static final String COUNTRY_TELE_LIST = "countrTeleList";

	private static final String AREA_CODE_LIST = "areaCodeList";

	private static final String TELEPHONE_LIST = "teleCodeList";

	private static final String COUNTRY_PHONE_LIST = "countryPhoneList";

	private static final String SYS_CARRIER_CODES = "systemCarrierCodes";

	private static final String SYS_AIRLINE_CODES = "systemAirlineCodes";

	private static final String SCHED_PUBLISH_MECANISM = "schedPublishMechanism"; // Haider
	// 14Oct08

	private static final String AGENT_COMMITION_TYPE_LIST = "agentCommiTypeList";

	private static final String GSA_CURRENCY_CODE = "selagentsCurrency";

	private static final String FARE_TYPE = "fareTypeFromPnrSegIdAndFareId";

	// private static final String GET_PNRS_WITH_GIVEN_FOID = "uniqueFoidForSegment";

	private static final String GSA_TRTRY_CODE = "selagentsTrtry";

	private static final String OCTUPUS_CITY = "octupusCity";

	private static final String UFI_CITY = "ufiCity";

	private static final String VISIBLE_ROLES = "visibleRoles";

	private static final String DEFAULT_ROLES = "defaultRoles";

	private static final String ADMIN_USER_COUNT = "adminUserCount";

	private static final String SSR_Category_List = "ssrCategory";

	private static final String FareVisibilitys = "farevisibility";

	private static final String FareVisibilityDescription = "farevisibilityDescription";

	private static final String AUDIT_TEMPLATE_LIST = "auditTemplateList";

	private static final String AIRPORT_CURR_CODE = "airportCurrCode";

	private static final String CURRENCY_CODE = "currencyCodeList";

	private static final String USER_THEME_LIST = "userThemeList";

	private static final String ALL_REGIONS_LIST = "allRegionsList";

	private static final String ALL_SALES_CHANNELS_LIST = "allSalesChannelsList";

	private static final String ALL_CHARGES_LIST = "allChargesList";

	private static final String ALL_CHARGES_LIST_GROUP = "allChargesListWithGroup";

	private static final String BOOKING_CLASS_TYPE_LIST = "bookingClassType";

	private static final String ACTUAL_PAYMENT_METHOD_LIST = "actualPaymentMethod";

	private static final String ALL_SALES_CHANNELS_COLORS = "allSalesChannelsColors";

	private static final String ALL_STATIONS_FOR_REGION = "allStationsForRegion";

	private static final String AIRPORT_CODE_NAME_COUNTRY_LIST = "airportCodeNameCountryCode";
	
	private static final String CITY_CODE_NAME_COUNTRY_LIST = "cityCodeNameCountryCode";

	private static final String SCHEDULED_REPORT_NAMES = "allScheduledReportTypes";

	private static final String FLEXI_CODE_LIST = "flexiCodeList";

	private static final String FLEXI_JOURNEY_LIST = "flexiJourneyList";

	private static final String DASHBOARD_MSG_TYPES = "dashBoardMsgTypes";

	private static final String ALL_SALES_CHANNELS_BOOKING_ORIGINATED = "allBookingOriginatedSalesChannels";

	private static final String ALL_SUB_STATION_LIST_XBE = "subStationXBE";

	private static final String ALL_SUB_STATION_LIST_IBE = "subStationIBE";

	// private static final String BUS_CARRIER_NUMBER = "querySampleBusNumber";

	private static final String baggageTemplateList = "baggageTemplateList";

	private static final String OND_BAGGAGE_TEMPLATE_LIST = "ONDBaggageTemplateList";

	private static final String OND_CODE_LIST_FOR_OND_BAGGAGE_TEMPLATES = "ONDCodeListForONDBaggageTemplates";

	private static final String BCCODE_TYPE_STND_FIX_FLAG_ALL_TYPE_LIST = "bCcTypStdFixFlgAllocList";

	private static final String SSR_BY_ADMIN_VISIBLE = "ssrInfoByAdminVisible";

	private static final String SSR_CODES_ALL = "ssrCodesAll";

	private static final String SSR_TEMPLATE_ALL = "ssrTemplateAll";

	private static final String SSR_TEMPLATE_ACT = "ssrTemplateAct";

	private static final String SSR_SUB_CATEGORY_LIST = "ssrSubCategory";

	private static final String SSR_AND_CATEGORY_LIST = "ssrCodesAndCategory";

	private static final String BOOKED_HALA_AIRPORT_BY_ID = "bookedHalaAirportsBySsrId";

	private static final String BOOKING_CLASS_CODES = "bookingClassCodes";

	private static final String BOOKING_CLASS_CODES_FOR_CHARGE_GROUPS = "bookingClassCodesForChargeGroups";

	private static final String ALL_BOOKING_CATEGORIES = "allActiveBookingCategories";

	private static final String BOOKED_SSR_SSR_ID = "bookedSSRBySSRId";

	private static final String BSP_COUNTRY_LIST = "bspCountryList";

	private static final String BSP_AGENT_CODE_COUNTRY_CODE_LIST = "bspAgentCodeCountryCodeList";

	private static final String AGENT_CURRENCY_CODE_LIST = "agentCurrencyList";

	private static final String AGENT_ADDRESS_CITY_LIST = "agentCityList";

	private static final String AIRCRAFT_TYPE_CODE_LIST = "aircraftTypeCodeList";

	private static final String SEAT_CHARGE_TEMPLATE_LIST = "seatChargeTemplate";

	private static final String SEAT_CHARGE_TEMPLATE_TEXT = "seatChargeTemplateWithModel";
	
	private static final String ACTIVE_SEAT_CHARGE_TEMPLATE_TEXT = "actSeatChargeTemplateWithModel";

	private static final String ACTIVE_SEAT_CHARGE_TEMPLATE_LIST = "activeSeatChargeTemplate";

	private static final String PROMOTIONS_TYPES_LIST = "promotionTypes";
	
	private static final String VOUCHER_TEMPLATE_LIST = "voucherNames";

	private static final String ALERT_TYPE_LIST = "alertTypes";

	private static final String AGENT_FARE_MASK_STATUS = "fareMaskStatus";

	private static final String SELECTED_FLIGHT_NOS = "selectedFlightNos";

	private static final String PNRGOV_COUNTRY_LIST = "pnrGovCountryList";

	private static final String ANCI_OFFER_MEAL = "anciOfferMealTemplate";

	private static final String ANCI_OFFER_BAGGAGE = "anciOfferBaggageTemplate";

	private static final String ANCI_OFFER_SEAT = "anciOfferSeatTemplate";

	private static final String SALES_CHANNEL = "saleschannel";

	private static final String TERMS_TEMPLATE_NAMES = "termsTemplateNames";

	private static final String BUNDLED_FARE_NAME_LIST = "bundledFareNames";

	private static final String BUNDLED_CATEGORY_LIST = "bundledCategoryList";

	private static final String BUNDLED_OND_LIST = "bundledOndList";

	private static final String BUNDLED_BOOKING_CLASSES_LIST = "bundledBookingClassesList";

	private static final String BUNDLED_STATUS_LIST = "bundledStatusList";

	private static final String ALL_BUNDLED_FARES_LIST = "allBundledFaresList";

	private static final String TEMPLATE_ID_INAPPLICABLE = "Inapplicable";

	private static final String LOYALTY_PRODUCT_CODES_LIST = "allLoyaltyProductCodes";
	
	private static final String REPORTABLE_ACTIVE_AGENTS = "getReportableAgents";	
		
	private static final String ALL_AGENTS_FOR_GSA_V2 = "allAgentsForGSAV2";
	
	private static final String ALL_REPORTING_AGENTS_FOR_GSA_V2 = "allReportingAgentsForGSAV2";
	
	private static final String ALL_SUB_AGENTS_WITH_CREDITS = "allSubAgentsWithCredit";
	
	private static final String VOUCHER_TERMS_TEMPLATE_NAMES = "voucherTermsTemplateNames";
	
	private static final String AGENT_TOPUP_PGW = "topupPGWData";

	private static final String COUNTRY_STATE = "stateCountryMapping";
	
	private static final String CHARGERS_WITHOUT_SERVICE_TAX = "chargeWithOutServiceTaxes";
	
	private static final String GST_MESSAGE_ENABLED_AIRPORTS = "gstMessageEnabledAirportCodes";	

	private static final String SEARCH_TAGS = "searchTag";

	private static final String ROUTE_OND_CODE_LIST = "routeOndCodeList";
	
	private static final String GSA_COUNTRY_WISE_STATIONS = "countryWiseVisibleStationsForGSA";
	
	private static final String filghtNumbersWithOrigin = "filghtNumbersWithOrigin";
	
	private static final String Entity = "entities";

	private static final String QUEUE_PAGES = "queuePage";
	
	private static final String QUEUES = "queue";
	
	private static final String REQUESTSTATUS = "requestStatus";
	
	private static final String FAMILY_RELATIONSHIP = "relatoinship";
	
	private static final String FAMILY_RELATIONSHIP_TITLE = "relatoinshipTitle";

	private static final String COUNTRY_ACTIVE_AIRPORTS = "countryWiseActiveAirports";

	private static final String COUNTRY_AIRPORTS_WITHOUT_BUS_SEG = "countryWiseAirportsWithoutBusSeg";

	public final static Set<String> getBusCarrierCodes() throws ModuleException {
		return createBusCarrierCodes(SYS_CARRIER_CODES);
	}

	public final static String getSystemCarrierCodes() throws ModuleException {

		return create_Code_Code_List(SYS_CARRIER_CODES);
	}

	public final static Map<String, String> getAgentsCodeNameMap() throws ModuleException {
		return createSecondResultKeyMap(Agent);
	}

	public final static List<String> getFlightNoList() throws ModuleException {
		return createFirstResultList(filghtNumbers);
	}

	public final static List<String> getFlightNoWithoutBusList() throws ModuleException {
		return createFirstResultList(filghtNumbersWithoutBus);
	}

	public final static Map<String, String> getSalesChannelList() throws ModuleException {
		return createFirstResultKeyMap(FareVisibilitys);
	}

	public final static List<String> getCabinClassList() throws ModuleException {
		return createFirstResultList(cabinClassesList);
	}

	public final static String getAirlineCodeList() throws ModuleException {
		return create_Code_Code_List(SYS_AIRLINE_CODES);
	}

	public final static String createFlightNoList() throws ModuleException {
		return create_Code_Code_List(filghtNumbers);
	}
	
	public final static String createOriginFlightNoList() throws ModuleException {
		return create_Code_Name_List(filghtNumbersWithOrigin);
	}
	
	public static final String createRouteOndCodeList() throws ModuleException {
		return create_Code_Code_List(ROUTE_OND_CODE_LIST);
	}
	
	public final static String createFlightNoListWithoutBus() throws ModuleException {
		return create_Code_Code_List(filghtNumbersWithoutBus);
	}

	public final static List<Map<String, String>> createFlightNumbers() throws ModuleException {
		return create_List(filghtNumbers);
	}

	public final static String createChargesTemplate() throws ModuleException {
		return create_Code_Code_List(chargeTemplate);
	}

	public final static Collection<String[]> createMealChargesTemplateIDs(String model) throws ModuleException {

		int colCount = 3;
		String selectSql = chargeMealTemplateIDs;
		String[] params = new String[0];
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		return colData;

	}

	public final static Collection<String[]> createBaggageChargesTemplateIDs(String model) throws ModuleException {

		int colCount = 3;
		String selectSql = chargeBaggageTemplateIDs;
		String[] params = new String[0];
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		return colData;

	}

	public final static Collection<String[]> createChargesTemplateIDs(String model) throws ModuleException {

		int colCount = 3;
		String selectSql = chargeTemplateIDs;
		String[] params = new String[1];
		if (model != null) {
			params[0] = model;
		}
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		return colData;

	}

	public final static List<Map<String, String>> createAgentTypesList() throws ModuleException {
		return create_List(agentTypes);
	}

	public final static List<Map<String, String>> createAgentSubTypesList(String agentTypeCode) throws ModuleException {
		String[] params = new String[2];
		if (agentTypeCode != null) {
			params[0] = agentTypeCode;
			params[1] = agentTypeCode;
		}
		return create_List(agentSubTypes, params);
	}

	public final static String createAgentTypeCodesList() throws ModuleException {
		return create_Code_Description_List(agentTypesWithCode);
	}

	public final static Collection<String[]> createAgentsByType(String params[]) throws ModuleException {
		String selectSql = agentsByType;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 2);
		return col;
	}

	public final static Collection<String[]> createAgentsByTypeAndStatus(String params[]) throws ModuleException {
		String selectSql = agentsByTypeAndStatus;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 2);
		return col;
	}
	
	public final static Collection<String[]> getAllRoles() throws ModuleException {
		String selectSql = allRoles;
		Collection<String[]> col = LookupUtils.LookupDAO().getList2(selectSql, 2);
		return col;
	}

	public final static Collection<String[]> getAllPrivileges() throws ModuleException {
		String selectSql = allPrivileges;
		Collection<String[]> col = LookupUtils.LookupDAO().getList2(selectSql, 2);
		return col;
	}

	public final static Collection<String[]> createOwnAgentsByType(String params[]) throws ModuleException {
		String selectSql = ownAgentsByType;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 2);
		return col;
	}

	public final static Collection<String[]> createVisibleRoles(String params[]) throws ModuleException {
		String selectSql = VISIBLE_ROLES;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 1);
		return col;
	}

	public final static int getAdminUseCount(String params[]) throws ModuleException {
		String selectSql = ADMIN_USER_COUNT;
		int adCount = 0;
		String count = LookupUtils.LookupDAO().getStrObject(selectSql, params);
		if (count != null && !count.equals("")) {
			adCount = new Integer(count).intValue();
		}
		return adCount;
	}

	public final static int getFlexiEnabledFareCount(String params[]) throws ModuleException {
		String sql = freeFlexiAttachedFareCount;
		int attchedFareCount = 0;
		String count = LookupUtils.LookupDAO().getStrObject(sql, params);
		if (count != null && !count.equals("")) {
			attchedFareCount = new Integer(count).intValue();
		}
		return attchedFareCount;
	}

	public final static Collection<String[]> createDefaultRoles(String params[]) throws ModuleException {
		String selectSql = DEFAULT_ROLES;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 1);
		return col;
	}

	public final static Collection<String[]> getAgentCodesForCashSales() throws ModuleException {
		String selectSql = AgentsCashSales;
		Collection<String[]> col = LookupUtils.LookupDAO().getTwoColumnMap(selectSql);
		return col;
	}

	/**
	 * 
	 * @return a collection of Role desription according to visibility
	 * @throws ModuleException
	 */
	public final static Collection<String[]> createRolesByAgentType(String params[]) throws ModuleException {
		String selectSql = AGENT_TYPE_ROLES;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 5);
		return col;
	}

	/**
	 * @param params
	 * @return a collection of Agent type according to role
	 * @throws ModuleException
	 */
	public final static Collection<String[]> createAgentTypeByRole(String params[]) throws ModuleException {
		String selectSql = ROLE_AGENT_TYPE;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 2);
		return col;
	}

	public final static Collection<String[]> createCarrierListWithCode() throws ModuleException {
		String selectSql = CARRIER_LIST;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, null, 2);
		return col;
	}

	public final static Collection<String[]> createActiveCarrierListWithCode() throws ModuleException {
		String selectSql = ACTIVE_CARRIER_LIST;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, null, 2);
		return col;
	}

	public final static Collection<String[]> createAgentsByStation(String params[]) throws ModuleException {
		String selectSql = agentsByStation;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 2);
		return col;
	}
	
	public final static Collection<String[]> createAgentsByStationAndStatus(String params[]) throws ModuleException {
		String selectSql = agentsByStationAndStatus;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 2);
		return col;
	}

	public final static Collection<String[]> createOwnAgentsByStation(String params[]) throws ModuleException {
		String selectSql = ownAgentsByStation;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 2);
		return col;
	}

	public final static Collection<String[]> createAgentsByStationActive(String params[]) throws ModuleException {
		String selectSql = agentsByStationActive;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 2);
		return col;
	}

	public final static Collection<String[]> createActiveAgentsByStation(String params[]) throws ModuleException {
		String selectSql = agentsByStation;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 2);
		return col;
	}

	public final static Collection<String[]> createAllAgentsByStation(String params[]) throws ModuleException {
		String selectSql = agentsByStationAll;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 3);
		return col;
	}

	/**
	 * Method to get TAs by station
	 * 
	 * @param params
	 *            array of agent type
	 * @return Collection of TAs
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static Collection<String[]> createTAsByStation(String params[]) throws ModuleException {
		String selectSql = taByStation;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 2);
		return col;
	}
	
	public final static Collection<String[]> createTAsByStationAndStatus(String params[]) throws ModuleException {
		String selectSql = taByStationAndStatus;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 2);
		return col;
	}

	public final static Collection<String[]> createOwnTAsByStation(String params[]) throws ModuleException {
		String selectSql = ownTaByStation;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 2);
		return col;
	}

	public final static Collection<String[]> createAgentUsers(String params[]) throws ModuleException {
		String selectSql = AGENT_USERS;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 2);
		return col;
	}

	public final static Collection<Map<String, String>> createAllCountry(String params[]) throws ModuleException {
		String selectSql = COUNTRY_ALL;
		Collection<Map<String, String>> col = LookupUtils.LookupDAO().getList(selectSql);
		return col;
	}

	public final static Collection<String[]> createStationsForRegion(String params[]) throws ModuleException {
		String selectSql = ALL_STATIONS_FOR_REGION;
		int colCount = 2;
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}

	/**
	 * Creates a collection of sales channel code & description
	 * 
	 * @param agentCode
	 * @return
	 * @throws ModuleException
	 */
	public static final Collection<String[]> createSalesChannelList() throws ModuleException {
		int colCount = 2;
		String selectSql = ALL_SALES_CHANNELS_LIST;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}

	public static final Collection<String[]> createSalesChannelColoursList() throws ModuleException {
		int colCount = 2;
		String selectSql = ALL_SALES_CHANNELS_COLORS;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}

	/**
	 * create a list of charges with the description(overridden from the T_RPT_CHARGE_GROUP's description)
	 * 
	 * @return
	 */
	public static final Collection<String[]> createChargesList() {
		int colCount = 2;
		String selectSql = ALL_CHARGES_LIST;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}

	public static final Collection<String[]> createChargesListWithGroup() {
		int colCount = 4;
		String selectSql = ALL_CHARGES_LIST_GROUP;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}

	public static final Collection<String[]> createSalesChannelBookingOriginaterList() throws ModuleException {
		int colCount = 2;
		String selectSql = ALL_SALES_CHANNELS_BOOKING_ORIGINATED;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}

	public final static Collection<String[]> createStationCountry(String params[]) throws ModuleException {
		String selectSql = STATION_COUNTRY;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 2);
		return col;
	}

	public final static Collection<String[]> createStationCountryName() throws ModuleException {
		Collection<String[]> col = LookupUtils.LookupDAO().getTwoColumnMap(StationCountry);
		return col;
	}

	public final static Collection<String[]> createTasFromGSA(String params[]) throws ModuleException {
		String selectSql = tasfromGSACode;
		if(params.length != 2 && params[0] != null && !params[0].isEmpty()){
			String agentCode = params[0];
			params = new String[3];
			params[0] = params[1] = params[2] = agentCode;
		}
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 5);
		return col;
	}

	public final static Collection<String[]> createStationTasFromGSA(String params[], String station) throws ModuleException {
		String selectSql = tasfromStationGSACode;
		if (params.length != 2 && params[0] != null && !params[0].isEmpty()) {
			String agentCode = params[0];
			params = new String[5];
			params[0] = params[3] = station;
			params[1] = params[2] = params[4] = agentCode;
		}
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 5);
		return col;
	}

	public final static String createGSAListWOTag() throws ModuleException {
		return create_Code_Description_List_WithoutTag(GSACombo, "arrGSAs");
	}

	public final static Collection<String[]> createGSATAList(String params[]) throws ModuleException {
		String selectSql = agentsGSATA;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 8);
		return col;
	}
	
	public final static Collection<String[]> createGSATAV2List(Map<String, Collection> params) throws ModuleException {
		String selectSql = agentsGSATAV2;
		Collection<String[]> col = LookupUtils.LookupDAO().getListForParameterList(selectSql, params, 8);
		return col;
	}

	public final static Collection<String[]> createGSATAListByStatus(String params[]) throws ModuleException {
		String selectSql = agentsGSATAByStatus;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 8);
		return col;
	}

	public final static Collection<String[]> createOwnGSATAList(String params[]) throws ModuleException {
		String selectSql = ownAgentsGSATA;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 8);
		return col;
	}

	public final static List<Map<String, String>> createSalesChannels() throws ModuleException {
		return create_List(SalesChannels);
	}

	public final static List<Map<String, String>> createBookingOriginatedSalesChannelList() throws ModuleException {
		return create_List(BookingOriginatedSalesChannels);
	}

	public final static String createSalesChannelVisibilityList() throws ModuleException {
		return create_Code_Description_List(FareVisibilitys);
	}

	public final static List<Map<String, String>> createSalesChannelVisibilityMultiList() throws ModuleException {
		return create_List(FareVisibilityDescription);
	}

	public final static String createOnlineActiveAirportListWOTag() throws ModuleException {
		return create_Code_Code_List_WithoutTag(activeAirports);
	}

	public final static String createOnlineAirportListWOTag() throws ModuleException {
		return create_Code_Code_List_WithoutTag(ONLINE_AIRPORTS, "arrOnlineAirports");
	}

	public final static String createActiveAirportList() throws ModuleException {
		return create_Code_Code_List(activeAirports);
	}

	// method to get flight build status
	public final static String createFlightStatus() throws ModuleException {
		return create_Code_Description_List(FlightStatus);
	}

	public final static List<String[]> createFlightStatusList() throws ModuleException {
		return createCodeDescriptionList(FlightStatus);
	}

	// method to get checkin build status
	public final static String createCheckinStatus() throws ModuleException {
		return create_Code_Description_List(CheckinStatus);
	}

	public final static Collection<String[]> createTADescription(String params[]) throws ModuleException {
		String selectSql = agentDescription;
		if(params.length != 2 && params[0] != null && !params[0].isEmpty()){
			String agentCode = params[0];
			params = new String[3];
			params[0] = params[1] = params[2] = agentCode;
		}
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 5);
		return col;
	}

	public final static Collection<String[]> createStationTADescription(String params[], String station) throws ModuleException {
		String selectSql = tafromStationTaCode;
		if (params.length != 2 && params[0] != null && !params[0].isEmpty()) {
			String agentCode = params[0];
			params = new String[5];
			params[0] = params[3] = station;
			params[1] = params[2] = params[4] = agentCode;
		}
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 5);
		return col;
	}

	// method to get the minimum stop over time
	public final static String createMinStopoverTime() throws ModuleException {
		return create_Code_Description_List_WithoutTag(MinStopoverTime, "arrMinStopOverTimeData");
	}

	public final static String createAirportTerritoryList() throws ModuleException {
		return create_Code_Description_List_WithoutTag(AirportTerritory, "arrStatTer");
	}

	public final static String createAirportTerritoryList(String agentcode) throws ModuleException {
		int colCount = 2;
		String selectSql = AirportTerritoryGSA;
		String[] params = new String[] { agentcode };
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		String strArray = getJSArray("arrStatTer", colData);

		return strArray;
	}

	public final static String gsaTerritoryStationList(String agentcode) throws ModuleException {
		int colCount = 2;
		String selectSql = GSATerritoryStations;
		String[] params = new String[] { agentcode };
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		String strArray = getJSArray("arrStatTer", colData);

		return strArray;
	}

	public final static String createAirportComboListWOTag() throws ModuleException {
		return create_Code_Code_List_WithoutTag(AirportCombo, "arrAirports");
	}

	public final static List<String[]> createAirportComboList() throws ModuleException {
		return createCodeDescriptionList(AirportCombo);
	}

	public final static List<Map<String, String>> createCabinClassAndLogicalCabinClasses() throws ModuleException {
		List<Map<String, String>> transformList = new ArrayList<Map<String, String>>();
		String cabinClass = "";
		if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
			List<String[]> logicalCabinClasses = LookupUtils.LookupDAO().getFourColumnMap(allCOSList);
			for (String s[] : logicalCabinClasses) {
				if (!cabinClass.equals(s[0])) {
					Map<String, String> tmpMap = new LinkedHashMap<String, String>();
					String key = s[0] + Constants.UNDERSCORE_SEPERATOR + s[1];
					String value = s[2];
					tmpMap.put(key, value);
					transformList.add(tmpMap);
				}
				cabinClass = s[0];
			}
			return transformList;
		} else {
			StringBuffer sb = new StringBuffer();
			List<String[]> cabinClasses = LookupUtils.LookupDAO().getFourColumnMap(allCOSList);
			for (String s[] : cabinClasses) {
				if (!cabinClass.equals(s[0])) {
					Map<String, String> tmpMap = new LinkedHashMap<String, String>();
					String key = s[0] + Constants.UNDERSCORE_SEPERATOR + s[0];
					String value = s[3];
					tmpMap.put(key, value);
					transformList.add(tmpMap);
				}
				cabinClass = s[0];
			}
			return transformList;
		}
	}

	public final static String createAirportCodeNameListWOTag() throws ModuleException {
		return create_Code_Description_List_WithoutTag(Airports, "airportArr");
	}

	public final static String createTerritoryListWOTag() throws ModuleException {
		return create_Code_Description_List_WithoutTag(TerritoryListDesc, "arrTerriID");
	}

	public final static String createTerritoryComboListWOTag() throws ModuleException {
		return create_Code_Code_List_WithoutTag(TerritoryCombo, "arrTerritories");
	}

	public final static String createOwnAirlineTerritoryComboListWOTag() throws ModuleException {
		int colCount = 2;
		String selectSql = OwnAirlineTerritoryCombo;
		String[] params = new String[] { AppSysParamsUtil.getDefaultAirlineIdentifierCode() };
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		String strArray = getJSArray("arrOwnTerritories", colData);

		return strArray;
	}

	public final static String createTerritoryComboListWOTag(String agentcode) throws ModuleException {
		int colCount = 2;
		String selectSql = TerritoryForGSA;
		String[] params = new String[] { agentcode, agentcode };
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		String strArray = getJSArray("arrTerritories", colData);

		return strArray;
	}

	public final static String createOwnTerritoryComboListWOTag(String agentcode) throws ModuleException {
		int colCount = 2;
		String selectSql = OwnTerritoryForGSA;
		String[] params = new String[] { agentcode, agentcode, AppSysParamsUtil.getDefaultAirlineIdentifierCode() };
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		String strArray = getJSArray("arrOwnTerritories", colData);

		return strArray;
	}

	public final static String createStationComboListWOTag() throws ModuleException {
		return create_Code_Code_List_WithoutTag(Station, "arrAirports");
	}

	public final static String createStationComboListWOTag(String agentcode, String gsaCode) throws ModuleException {
		int colCount = 2;
		String selectSql = StationForGSA;
		String[] params = new String[] { agentcode, gsaCode, agentcode};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		String strArray = getJSArray("arrAirports", colData);

		return strArray;
	}

	public final static String createAllIPGsForXBE(String arrayName) throws ModuleException {
		int colCount = 2;
		String selectSql = ALL_XBEIPGS;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		String strArray = getJSArray(arrayName, colData);
		return strArray;
	}

	public final static String createGSAStations(String agentcode) throws ModuleException {
		StringBuilder ssb = new StringBuilder();
		int colCount = 2;
		String selectSql = StationForGSA;
		String[] params = new String[] {  agentcode, agentcode, agentcode};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		Iterator<String[]> stIt = colData.iterator();
		String[] strSts = null;
		while (stIt.hasNext()) {
			strSts = stIt.next();
			ssb.append("<option value='" + strSts[0] + "'>" + strSts[1] + "</option>");
		}
		return ssb.toString();
	}

	
	public final static String createAllOfflineIPGsForXBE(String arrayName) throws ModuleException {
		return createTupleForOptions(arrayName, ALL_OFFLINE_XBEIPGS);
	}

	public final static String createModificationEnabledIPGsForXBE(String arrayName) throws ModuleException {
		return createTupleForOptions(arrayName, MODIFICATION_ENABLED_OFFLINE_XBEIPGS);
	}

	public final static String createTupleForOptions(String arrayName, String selectSql) throws ModuleException {
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, new String[] {}, 2);
		return getJSArray(arrayName, colData);
	}

	public final static String createGSAStations(String agentcode, String gsacode) throws ModuleException {
		StringBuilder ssb = new StringBuilder();
		int colCount = 2;
		String selectSql = StationForGSA;
		String[] params = new String[] {  agentcode, gsacode, agentcode};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		Iterator<String[]> stIt = colData.iterator();
		String[] strSts = null;
		while (stIt.hasNext()) {
			strSts = stIt.next();
			ssb.append("<option value='" + strSts[0] + "'>" + strSts[1] + "</option>");
		}
		return ssb.toString();
	}

	public final static String createOwnerStations(String agentcode) throws ModuleException {
		StringBuilder ssb = new StringBuilder();
		int colCount = 2;
		String selectSql = OWNER_AGENT;
		String[] params = new String[] { agentcode };
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		Iterator<String[]> stIt = colData.iterator();
		String[] strSts = null;
		while (stIt.hasNext()) {
			strSts = stIt.next();
			ssb.append("<option value='" + strSts[0] + "'>" + strSts[1] + "</option>");
		}
		return ssb.toString();
	}

	public final static String createAgentListWOTag() throws ModuleException {
		return create_Code_Description_List_WithoutTag(AgentCombo, "arrTAs");
	}

	public final static String createAgentListWOTag(String agentcode, String airlineCode) throws ModuleException {
		int colCount = 3;
		String selectSql = REPORTING_AGENTS_WITH_CREDIT;
		String[] params = new String[] { airlineCode, agentcode };
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		String strArray = getJSArray("arrTAs", colData);

		return strArray;
	}

	public final static String createCurrencyCodeListWOTag() throws ModuleException {
		return create_Code_Description_List_WithoutTag(xActiveCurrencyCodes, "arrCurrency");
	}

	public final static String createCOSActiveCodeDescListWOTag() throws ModuleException {
		return create_Code_Description_List_WithoutTag(cabinClassesList, "arrClass");
	}

	/**
	 * Get Class codes list
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public final static List<String[]> getCOSActiveCodeDescList() throws ModuleException {
		return createCodeDescriptionList(cabinClassesList);
	}

	public final static String createIBECurrencyActiveDescListWOTag() throws ModuleException {
		return create_Code_Description_List_WithoutTag(CURRENCYACTIVEIBE, "arrCurrency");
	}

	/**
	 * Get Currency List
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public final static List<String[]> createIBECurrencyActiveDescList() throws ModuleException {
		return createCodeDescriptionList(CURRENCYACTIVEIBE);
	}

	public final static String createXBECurrencyActiveDescListWOTag() throws ModuleException {
		return create_Code_Description_List_WithoutTag(CURRENCYACTIVEXBE, "arrCurrency");
	}

	public final static String createCountryListWOTag() throws ModuleException {
		return create_Code_Description_List_WithoutTag(CountryList, "arrCountry");
	}

	public final static String createCountryDescListWOTag() throws ModuleException {
		return create_Code_Description_List_WithoutTag(CountryList, "arrCountry");
	}

	public final static String createNationalityListWOTag() throws ModuleException {
		return create_Code_Description_List_WithoutTag(Nationality, "arrNationality");
	}

	public final static List<String[]> getNationalityList() throws ModuleException {
		return createCodeDescriptionList(Nationality);
	}

	public final static List<String[]> createFareDiscountCodeList() throws ModuleException {
		return createCodeDescriptionList(FAREDISCOUNTCODES);
	}
	
	public final static List<String[]> getRelationshipList() throws ModuleException {
		return createCodeDescriptionList(FAMILY_RELATIONSHIP);
	}
	
	public final static List<String[]> getRelationshipTitleList() throws ModuleException {
		return createCodeDescriptionList(FAMILY_RELATIONSHIP_TITLE);
	}

	public static final String createFareDiscountCodes() throws ModuleException {
		StringBuilder sb = new StringBuilder();
		List<String[]> discoutTypes = createFareDiscountCodeList();

		for (String[] codeDes : discoutTypes) {
			sb.append("<option value='" + codeDes[0] + "'>" + codeDes[1] + "</option>");
		}
		return sb.toString();
	}

	public final static String createIBEAirportsListWOTag() throws ModuleException {
		return create_Code_Description_Combine_List_WithoutTag(airportDescIBE, "arrAirports");
	}

	/**
	 * Get IBE Airport List
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public final static List<String[]> getIBEAirportsListWSub(boolean includeInterline, boolean isSubStationsAllowed,
			List<String[]> filterList) throws ModuleException {
		List<String[]> airports = new ArrayList<String[]>();

		List<String[]> lcc = null;
		List<String[]> sub = null;
		if (filterList == null) {
			List<String[]> own = createCodeDescriptionList(airportDescIBE);
			airports.addAll(own);
		} else {
			airports.addAll(filterList);
		}

		if (includeInterline) {
			lcc = createLccCodeDescriptionList(lccAirportDesc);
		}
		if (lcc != null) {
			// Add Lcc Airport if not exist in Own list
			for (String[] lccAirport : lcc) {
				boolean isfound = false;
				for (String[] ownAirport : airports) {
					if (lccAirport[0].equals(ownAirport[0])) {
						isfound = true;
						break;
					}
				}

				if (isfound == false) {
					airports.add(lccAirport);
				}
			}

		}
		if (isSubStationsAllowed) {
			sub = createSubAirportCodeDescriptionList(ALL_SUB_STATION_LIST_IBE);
			for (String[] strings : sub) {
				String[] tempMasterDummy = new String[] { strings[0], strings[1] };
				String[] dataObj = new String[] { strings[0] + ":" + strings[2], " --:" + strings[3] };
				List<String[]> copiedList = new ArrayList<String[]>(airports);
				Collections.copy(copiedList, airports);
				int index = 0;
				for (String[] copiedRec : copiedList) {
					if (Arrays.equals(copiedRec, tempMasterDummy)) {
						airports.add(index + 1, dataObj);
					}
					index++;
				}
				int curIndex = airports.indexOf(tempMasterDummy);
				if (curIndex != -1) {
					airports.add(curIndex + 1, dataObj);
				}
			}
		}
		return airports;
	}

	public final static Map<String, String> getAirportsList(boolean includeInterline) throws ModuleException {
		Map<String, String> airports = new HashMap<String, String>();
		List<String[]> own = createCodeDescriptionList(airportDescODDesc);
		List<String[]> lcc = null;
		if (includeInterline) {
			lcc = createLccCodeDescriptionList(lccAirportDesc);
		}
		for (String[] item : own) {
			airports.put(item[0].toUpperCase(), item[1]);
		}
		if (lcc != null) {
			for (String[] item : lcc) {
				airports.put(item[0].toUpperCase(), item[1]);
			}

		}
		return airports;
	}

	public final static String careateAirportOwnerList() {
		return create_Airport_Owner_List_WithoutTag(airportDescXBE, "airportOwnerList");
	}

	public final static String createXBEAirportsListWOTag(boolean includeInterline) throws ModuleException {
		return create_Code_Description_Combine_Lcc_Aggregate_List_WithoutTag(airportDescXBE, lccAirportDescXBE, includeInterline,
				"arrAirports");
	}

	public final static String createXBEV2AirportsListWOTagWSub(boolean includeInterline, List<String[]> filterList,
			String arrName, boolean isSubAllowed) throws ModuleException {
		return create_Code_Description_Combine_Lcc_Aggregate_WSubStations_List_WithoutTag(airportDescXBE, lccAirportDescXBE,
				includeInterline, arrName, isSubAllowed, ALL_SUB_STATION_LIST_XBE, filterList);
	}

	public final static String createOndMapAsPerAgent(boolean includeInterline, List<String[]> filterList, String arrName,
			boolean isSubAllowed, String agentCode) throws ModuleException {
		int colCount = 2;
		String[] params = new String[] { agentCode };
		String selectSql = agentWiseOnds;
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		String strArray = getJSMap(arrName, colData);
		return strArray;
	}

	public final static Map<String, String> createAirportsWithoutSubstations(boolean includeInterline, List<String[]> filterList,
			Collection<String> carrierCodes) throws ModuleException {
		return createAirportsWithoutSubstaions(airportDescXBE, lccAirportsForManifest, includeInterline, filterList, carrierCodes);
	}

	public final static String createIBEV2AirportsListWOTag(boolean includeInterline) throws ModuleException {
		return create_Code_Description_Combine_Lcc_Aggregate_List_WithoutTag(airportDescIBE, lccAirportDesc, includeInterline,
				"arrAirportsV2");
	}

	public final static String createStationCountryListWOTag() throws ModuleException {
		return create_Code_Description_List_WithoutTag(StationCountry, "arrStationCountry");
	}

	public final static String createStationCountryListWOTag(String agentcode) throws ModuleException {
		int colCount = 2;
		String selectSql = StationCountryGSA;
		String[] params = new String[] { agentcode };
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		String strArray = getJSArray("arrStationCountry", colData);

		return strArray;
	}

	public final static String createStationCurrencyListWOTag() throws ModuleException {
		return create_Code_Description_List_WithoutTag(StationCurrency, "arrStationCurrency");
	}

	public final static String createAgentGSAList() throws ModuleException {
		return create_Code_Description_List(AllAgents);
	}

	public final static String createAgentGSAListWOIntegrationAgency() throws ModuleException {
		return create_Code_Description_List(AllAgentsWOINT);
	}

	public final static String createOwnAirlineAgentGSAListWOIntegrationAgency() throws ModuleException {
		StringBuffer sb = new StringBuffer();
		int colCount = 2;
		String selectSql = AllOwnAirlineAgentsWOINT;
		String[] params = new String[] { AppSysParamsUtil.getDefaultAirlineIdentifierCode() };
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		Iterator<String[]> iter = colData.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			sb.append("<option value='" + s[0] + "'>" + s[1] + "</option>");
		}
		return sb.toString();
	}

	public final static String createGSAList() throws ModuleException {
		StringBuffer sb = new StringBuffer();
		int colCount = 2;
		String selectSql = AllGSAList;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		Iterator<String[]> iter = colData.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			sb.append("<option value='" + s[0] + "'>" + s[1] + "</option>");
		}
		return sb.toString();
	}

	public final static String createGSAList(String agentCode) throws ModuleException {
		StringBuffer sb = new StringBuffer();
		int colCount = 2;

		String selectSql;
		String[] params;
		if (agentCode != null) {
			selectSql = getGSA;
			params = new String[] { agentCode };
		} else {
			selectSql = AllGSAList;
			params = new String[] {};
		}

		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		Iterator<String[]> iter = colData.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			sb.append("<option value='" + s[0] + "'>" + s[1] + "</option>");
		}

		return sb.toString();
	}
	
	public final static String createReportingAgentList(String agentCode, String agentTypeCode) throws ModuleException {

		int colCount = 3;
		String selectSql = "";
		String[] params = new String[] {};
		Collection<String[]> colData = new ArrayList<String[]>();
		
		if (AppSysParamsUtil.getCarrierAgent().equals(agentTypeCode)) {
			selectSql = getAllReportableAgents;

			// All GSA
			// All SGSA
			// All TA

		} else if (AppSysParamsUtil.isGSAStructureVersion2Enabled() && !StringUtil.isNullOrEmpty(agentCode)) {
			selectSql = REPORTABLE_ACTIVE_AGENTS;
			params = new String[] { agentCode };
		} else {
			//FIXME : SGSA, TA loading is not correct 
			if (AgentType.GSA.equals(agentTypeCode)) {
				selectSql = getReportableForGSA;
				params = new String[] { agentCode, agentCode, agentCode };
				// SGSA - Reporting to GSA self
				// TA - Reporting to GSA self
				// TA - Reporting to SGSA - SGSAs of all visibile terrirories
				// STA - Reporting to TA - TAs of all visible stations

			} else if (AgentType.SGSA.equals(agentTypeCode)) {
				selectSql = getReportableForSGSA;
				params = new String[] { agentCode, agentCode };
				// TA - Reporting to SGSA - self
				// STA - Reporting to TA - TAs of all visible stations of the
				// territory
			} else if (AgentType.TA.equals(agentTypeCode)) {
				selectSql = getReportableForTA;
				params = new String[] { agentCode };
				// STA - Reporting to TA - self
			}
		}
		

		StringBuffer sbGSA = new StringBuffer();
		StringBuffer sbSGSA = new StringBuffer();
		StringBuffer sbTA = new StringBuffer();

		if(!"".equals(selectSql)){
			colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		}

		for (String[] record : colData) {
			if (AgentType.GSA.equals(record[2])) {
				if (sbGSA.length() == 0) {
					sbGSA.append("<option value=''>GSA</option>");
					sbGSA.append("<option value=''>============================</option>");
				}
				sbGSA.append("<option value='" + record[0] + "'>  -" + record[1].toUpperCase() + "</option>");
			} else if (AgentType.SGSA.equals(record[2])) {
				if (sbSGSA.length() == 0) {
					sbSGSA.append("<option value=''>SGSA</option>");
					sbSGSA.append("<option value=''>============================</option>");
				}
				sbSGSA.append("<option value='" + record[0] + "'>  -" + record[1].toUpperCase() + "</option>");
			} else if (AgentType.TA.equals(record[2])) {
				if (sbTA.length() == 0) {
					sbTA.append("<option value=''>TA</option>");
					sbTA.append("<option value=''>============================</option>");
				}
				sbTA.append("<option value='" + record[0] + "'>  -" + record[1].toUpperCase() + "</option>");
			}

		}

		return sbGSA.append(sbSGSA).append(sbTA).toString();
	}

	public final static Collection<String[]> createAgentsWithType() throws ModuleException {
		int colCount = 5;
		String selectSql = ALL_AGENTS_TYPES;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}

	public final static Collection<String[]> createAgentsWithTypeFilteredByAirline() throws ModuleException {
		int colCount = 5;
		String selectSql = AGENTS_TYPES_BY_AIRLINE;
		String[] params = new String[] { AppSysParamsUtil.getDefaultAirlineIdentifierCode() };
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}

	public final static Collection<String[]> createAgentsWithTypeFilteredByAirlineAndStation(String station)
			throws ModuleException {
		int colCount = 5;
		String selectSql = AGENTS_TYPES_BY_AIRLINE_STATION;
		String[] params = new String[] { AppSysParamsUtil.getDefaultAirlineIdentifierCode(), station };
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}

	public final static String createCollectionTypeList() throws ModuleException {
		return create_Code_Description_List(CollectionTypes);
	}
	
	public final static String createTopupPGWList() throws ModuleException {
		return create_Code_Description_List(AGENT_TOPUP_PGW);
	}

	// get the operation type
	public final static String createOperationType() throws ModuleException {
		return create_Code_Description_List(OperationType);
	}

	// get the operation type array
	public final static String createOperationTypeArray() throws ModuleException {
		return create_Operation_Type_Array(OperationType);
	}

	// get the operation type
	public final static String createOperationTypeForSchedule() throws ModuleException {
		return create_Code_Description_List(operationTypeSchedule);
	}

	// get the operation type
	public final static Collection<String[]> getOperationType() throws ModuleException {
		return get_Code_Description_List(OperationType);
	}

	// get the schedule status
	public final static String createScheduleStatus() throws ModuleException {
		return create_Code_Description_List(ScheduleStatus);
	}

	// get the GDS list status
	public final static String createGDSList() throws ModuleException {
		return create_Code_Description_List(gdsList);
	}

	// Haider 14Oct08
	// get the GDS Publish Mechanism
	public final static String createSchedPublishMechanismList() throws ModuleException {
		return create_Code_Description_List(SCHED_PUBLISH_MECANISM);
	}

	// get the build status
	public final static String createBuildStatus() throws ModuleException {
		return create_Code_Description_List(BuildStatus);
	}

	public final static String createPaymentPurposeList() throws ModuleException {
		return create_Code_Description_List(PaymentPurpose);
	}

	public final static String createCountryList() throws ModuleException {
		return create_Code_Description_List(Countrys);
	}

	public final static List<String[]> getCountryList() throws ModuleException {
		return createCodeDescriptionList(Countrys);
	}

	public final static List<String[]> getDocTypeList() throws ModuleException {
		final List<String[]> list = new ArrayList<String[]>();
		String s[] = { "V", "Visa" };
		list.add(s);
		return list;
	}

	public final static String createLanguageList() throws ModuleException {
		return create_Code_Description_List(Language);
	}

	public final static Collection<String[]> getLanguageList() throws ModuleException {
		return createCodeDescriptionList(Language);
	}

	/*
	 * public final static String createAgentListForAirport(String airportCode) throws ModuleException { String [] obj =
	 * {airportCode}; int [] types = {Types.VARCHAR};
	 * 
	 * return create_Code_Description_List(AgentsOfAirport,obj,types); }
	 */

	public final static String createAgentListForAirport() throws ModuleException {
		// return create_Code_Description_List(AgentsOfAirport);
		return create_Code_Description_Array(AgentsOfAllAirports, AgentsOfAllAirports);
	}

	public final static String createCountryNameList() throws ModuleException {
		return create_Code_Description_List(CountryNameList);
	}

	public final static List<String[]> getCountryNameList() throws ModuleException {
		return createCodeDescriptionList(CountryNameList);
	}

	public final static String createCurrencyList() throws ModuleException {
		return create_Code_Code_List(CurrencyCode);
	}

	public final static String createRegionList() throws ModuleException {
		return create_Code_Description_List(RegionCode);
	}

	public final static String createAirportODDesc() throws ModuleException {
		return create_Code_Description_List(airportDescODDesc);
	}

	public final static String createAirportsList() throws ModuleException {
		return create_Code_Code_List(Airports);
	}

	public final static String createSsrCategoryList() throws ModuleException {
		String SsrCategoryList = create_Code_Description_List(SSR_Category_List);
		if (!AppSysParamsUtil.isAirportTransferEnabled()) { // Chk RES_174 app_parameter
			SsrCategoryList = SsrCategoryList.replace("<option value='" + SSRCategory.ID_AIRPORT_TRANSFER
					+ "'>Airport Transfer</option>", "");
		}
		if (!AppSysParamsUtil.isAirportServicesEnabled()) { // Chk RES_78 app_parameter
			SsrCategoryList = SsrCategoryList.replace("<option value='" + SSRCategory.ID_AIRPORT_SERVICE
					+ "'>Airport Services</option>", "");
		}

		return SsrCategoryList;
	}

	public final static String createMailServerList(Collection<MailServerConfig> mailServerConfigs) throws ModuleException {

		List<String> lstHostAddresses = new ArrayList<String>();
		Iterator<MailServerConfig> iterator = mailServerConfigs.iterator();
		while (iterator.hasNext()) {
			MailServerConfig mailServerConf = iterator.next();
			if (mailServerConf != null && mailServerConf.getManualPnlSend() != null
					&& mailServerConf.getManualPnlSend().equals("true")) {
				lstHostAddresses.add(mailServerConf.getHostAddress());

			}
		}
		// Sort all the host addresses alphabatically
		Collections.sort(lstHostAddresses);

		StringBuffer sb = new StringBuffer();
		Iterator<String> itLstHostAddresses = lstHostAddresses.iterator();
		while (itLstHostAddresses.hasNext()) {
			String strHostAddress = itLstHostAddresses.next();
			sb.append("<option value='" + strHostAddress + "'>" + strHostAddress + "</option>");
		}

		return sb.toString();
	}

	public final static String createFareRuleList() throws ModuleException {
		return create_Code_Description_List(fareRule);
	}

	public final static String createSecreatQuestion() throws ModuleException {
		return create_Code_Description_List(SECREAT_QUESTION);
	}

	/**
	 * Get Secreat Question List
	 * 
	 */
	public final static List<String[]> getSecreatQuestionList() throws ModuleException {
		return createCodeDescriptionList(SECREAT_QUESTION);
	}

	public final static String createFareCodeRuleList() throws ModuleException {
		return create_Code_Code_List(fareRuleNewCodes);
	}

	public final static List<String[]> createFareRuleCodeArrayList() throws ModuleException {
		return createCodeDescriptionList(fareRuleNewCodes);
	}

	public final static List<String[]> createActiveCurrencyCodeList() throws ModuleException {
		return createCodeDescriptionList(xActiveCurrencyCodes);
	}

	public final static List<String[]> createFareRuleIDsList() throws ModuleException {
		return createCodeDescriptionList(fareRuleNewIDs);
	}

	public final static List<String[]> createFareRuleIDTypeList() throws ModuleException {
		return createCodeDescriptionList(fareRuleIDType);
	}

	public final static Map<String, String> createFareRuleIDCodeList() throws ModuleException {
		return createFirstResultKeyMap(fareRuleNewIDs);
	}

	public final static List<Map<String, String>> createFareRuleCodeList() throws ModuleException {
		return create_List(fareRuleNewCodes);
	}

	public final static String createAirportCodeList() throws ModuleException {
		return create_Code_Code_List(Airports);
	}

	public final static List<Map<String, String>> createSSRCodeList() throws ModuleException {
		return create_List(SSR_CODES_IN_USE);
	}

	public final static List<Map<String, String>> createAirportTransferCodeList() throws ModuleException {
		return create_List(AIRPORT_TRANSFER_CODES);
	}

	public final static List<Map<String, String>> createSSRCodeListWithCategory() throws ModuleException {
		return create_List(SSR_AND_CATEGORY_LIST);
	}

	public final static String createAirportsWithStatusList() throws ModuleException {
		return create_Code_Description_List(AirportsWithStatus);
	}

	public final static String createStationWithStatusList() throws ModuleException {
		return create_Code_Description_List(StationsWithStatus);
	}

	public final static List<Map<String, String>> createStationsListWithStatus() throws ModuleException {
		return create_List(StationsWithActiveStatus);
	}

	public final static List<Map<String, String>> createAgentListWithStatus() throws ModuleException {
		return create_List(Agent);
	}

	public final static List<Map<String, String>> createChargesWithOutServiceTax() throws ModuleException {
		return create_List(CHARGERS_WITHOUT_SERVICE_TAX);
	}
	
	public final static String createActiveAirportCodeList() throws ModuleException {
		return create_Code_Code_List(activeAirports);
	}
	
	public final static String createAdminFeeAgentList() throws ModuleException {
		return create_Code_Code_List(adminFeeAgents);
	}

	public final static List<String[]> createActiveAirportCodes() throws ModuleException {
		return createCodeDescriptionList(activeAirports);
	}	

	public final static Map<String, String> createActiveAirportCodeMap() throws ModuleException {
		return createFirstResultKeyMap(activeAirports);
	}

	public final static Map<String, String> createStationCodeMap() throws ModuleException {
		return createFirstResultKeyMap(StationCodes);
	}

	public final static String createActiveNonSurfaceAirportsCodeList() throws ModuleException {
		return create_Code_Code_List(activeNonSurfaceAirports);
	}

	// Conversions from airadmin/commons/SelectListGenerator
	public final static List<Map<String, String>> createAirportsListWithStatus() throws ModuleException {
		return create_List(AirportsWithStatus);
	}

	public final static String createServiceChannels() throws ModuleException {
		return create_Code_Description_List(ServiceChannels);
	}

	public final static String createDryUserLevels() throws ModuleException {
		return create_Code_Description_List(DryUserLevels);
	}

	public final static String createAgentTypeList_SG() throws ModuleException {
		return create_Code_Code_List(AgentTypeList);
	}
	
	public final static String createSubAgentTypes(String agentType) throws ModuleException {
		StringBuilder ssb = new StringBuilder();
		int colCount = 2;
		String selectSql = SubAgentTypeList;
		String[] params = new String[] {  agentType};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		Iterator<String[]> stIt = colData.iterator();
		String[] strSts = null;
		while (stIt.hasNext()) {
			strSts = stIt.next();
			ssb.append("<option value='" + strSts[0] + "'>" + strSts[1] + "</option>");
		}
		return ssb.toString();
	}
	

	public final static List<Map<String, String>> createCountryListWithCode() throws ModuleException {
		return create_List(COUNTRY_ALL);
	}

	public final static String createAllSubAgentTypes() throws ModuleException {
		int colCount = 1;
		String[] params = new String[] {};
		String selectSql = ReportingAgentTypelist;
		StringBuilder slsb = new StringBuilder("var agentSubtypes = new Array();");
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		for (String[] data : colData) {
			slsb.append("agentSubtypes[\"");
			slsb.append(data[0]);
			slsb.append("\"] = \"");
			slsb.append(createSubAgentTypes(data[0]));
			slsb.append("\";");
		}
		return slsb.toString();
	}
	
	public final static Collection<String[]> createGivenSubAgentList(Map<String, Collection> params) throws ModuleException {
		String selectSql = GivenReportingAgentList;
		Collection<String[]> col = LookupUtils.LookupDAO().getListForParameterList(selectSql, params, 8);
		return col;
	}

	public final static String createAgentStatusList_SG() throws ModuleException {
		StringBuffer sb = new StringBuffer();
		// TODO : got to replace the heard coded code
		sb.append("<option value='ACT'>Active</option>");
		sb.append("<option value='INA'  selected='selected'>In-Active</option>");
		sb.append("<option value='NEW'>New</option>");
		sb.append("<option value='BLK'>Black Listed</option>");
		return sb.toString();
	}

	public final static String createTerritoryList_SG_Desc() throws ModuleException {
		return create_Code_Description_List(TerritoryListDesc);
	}

	public final static String createAgentList_SG() throws ModuleException {
		return create_Code_Description_List(AgentList);
	}

	// END Conversions from airadmin/commons/SelectListGenerator
	public final static String createNationalityList() throws ModuleException {
		return create_Code_Description_List(NationalityCodes);
	}

	public final static String createTerritoryList() throws ModuleException {
		return create_Code_Description_List(Territorys);
	}

	public final static String createCabinClassList() throws ModuleException {
		return create_Code_Description_List(cabinClassesList);
	}
	
	public final static String createBundleDescTemplateList() throws ModuleException {
		return create_Code_Description_List(bundleDescTemplateList);
	}

	public final static String createCCWiseLogicalCCList() throws ModuleException {
		return create_CC_Wise_LogicalCC_List(ccWiselogicalCCList);
	}

	private final static String createAllCOS() throws ModuleException {
		return create_Logical_Cabin_Class_Sub_List(allCOSList);
	}

	public final static String createCabinClassAndLogicalCabinClassesList() throws ModuleException {
		if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
			return createAllCOS();
		} else {
			return createCabinClassLists();
		}
	}

	private final static String createCabinClassLists() throws ModuleException {
		return create_Cabin_Class_list(cabinClassesList);
	}

	public final static String createLogicalCabinClassList() throws ModuleException {
		return create_Code_Description_List(logicalCabinClassesList);
	}

	public final static String createIATACodeList() throws ModuleException {
		return create_Code_Code_List(iataCodeList);
	}

	public final static String createIATACodeListForBaggage() throws ModuleException {
		return create_Code_Code_List(iataCodeListForBaggage);
	}

	public final static String createMealTemplateList() throws ModuleException {
		return create_Code_Description_List(mealTemplateList);
	}

	public final static String createActiveMealTemplateList() throws ModuleException {
		return create_Code_Description_List(activeMealTemplateList);
	}

	public final static String createMeal() throws ModuleException {
		return create_Code_Description_List(mealList);
	}

	public final static String createMealCategory() throws ModuleException {
		return create_Code_Description_List(mealCategoryList);
	}

	public final static List<String[]> createMealCategoryList() throws ModuleException {
		return createCodeDescriptionList(mealCategoryList);
	}

	public final static String createMealCode() throws ModuleException {
		return create_Code_Description_List(mealCodeList);
	}

	public final static List<Map<String, String>> createCabinClass() throws ModuleException {
		return create_List(CabinClasses);
	}

	public final static String createAllCabinClass() throws ModuleException {
		return create_Code_Description_List(CabinClasses);
	}

	public final static List<String[]> createAllCabinClassList() throws ModuleException {
		return createCodeDescriptionList(CabinClasses);
	}

	public final static String createAllLogicalCabinClass() throws ModuleException {
		return create_Code_Description_List(LogicalCabinClasses);
	}

	public final static String createBookingCodeList() throws ModuleException {
		return create_Code_Code_List(BookingCodes);
	}

	public final static String createEventCodeList() throws ModuleException {
		return create_Code_Description_List(EventCodeList);
	}

	// create_Code_code_List
	public final static List<Map<String, String>> createBookingClasses() throws ModuleException {
		return create_List(BookingClass);
	}

	public final static List<Map<String, String>> createBasisCodeListMap() throws ModuleException {
		return create_List(BasisCodesList);
	}

	public final static String createBasisCodeList() throws ModuleException {
		return create_Code_Code_List(BasisCodesList);
	}

	public final static List<Map<String, String>> createBookingClassList() throws ModuleException {
		return create_List(BookingCodeList);
	}
	
	public final static List<Map<String, String>> createActiveBookingClassList() throws ModuleException {
		return create_List(ActiveBookingCodeList);
	}

	public final static List<Map<String, String>> createCOSWiseBookingClassList(String cabinClass, String logicalCabinClass)
			throws ModuleException {
		return create_List(COSWiseBookingCodeList,
				new Object[] { BeanUtils.nullHandler(logicalCabinClass), BeanUtils.nullHandler(cabinClass) });
	}

	public final static String createFareVisibilityList() throws ModuleException {
		return create_Code_Description_List(FareVisibilitysRestricted);
	}

	public final static List<String[]> createFareVisibilityArrayList() throws ModuleException {
		return createCodeDescriptionList(FareVisibilitysRestricted);
	}

	public final static Map<String, String> getVisibleSalesChannel() throws ModuleException {
		return createFirstResultKeyMap(FareVisibilitysRestricted);
	}

	public final static List<Map<String, String>> createListcChargeDisplay() throws ModuleException {
		return create_List(chargeGroupsDisplayList);
	}

	public final static String createAgents() throws ModuleException {
		return create_Code_Description_List(Agent);
	}

	public final static String createAgentsNameList() throws ModuleException {
		return create_Code_Description_List(AgentAgainstId);
	}

	public final static String createAgentCodeAgainstIdList() throws ModuleException {
		return create_Code_Description_List(AgentAgainstId);
	}

	public final static String createAgentCodes() throws ModuleException {
		return create_Code_Description_List(AllAgents);
	}

	public final static List<String[]> createAgentCodesList() throws ModuleException {
		return createCodeDescriptionList(AllAgents);
	}

	public final static String createChargeCodeList() throws ModuleException {
		return create_Code_Code_List(ChargeCodes);

	}	

	public final static String createAgentsAndStationsJson(boolean withAllOption) throws ModuleException {
		Collection<String[]> tempCollection;
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		List<String> stations = new ArrayList<String>();

		List<String> stationsTemp = LookupUtils.LookupDAO().getSingleColumnMap(stationListAct);

		List<Collection<String[]>> agentsVsStation = new ArrayList<Collection<String[]>>();

		if (withAllOption) {
			stations.add(CommonsConstants.ALL_DESIGNATOR_STRING);

			tempCollection = new ArrayList<String[]>();
			tempCollection.add(new String[] { "**ALL**", CommonsConstants.ALL_DESIGNATOR_STRING });
			agentsVsStation.add(tempCollection);
		}

		stations.addAll(stationsTemp);
		for (String station : stationsTemp) {
			agentsVsStation.add(LookupUtils.LookupDAO().getList(agentsByStationActive, new String[] { station }, 2));
		}

		jsonMap.put("stations", stations);
		jsonMap.put("agents", agentsVsStation);

		try {
			return JSONUtil.serialize(jsonMap);
		} catch (JSONException e) {
			throw new ModuleException("", e);
		}
	}

	public final static String createBundledFareChargeCodeList() throws ModuleException {
		return create_Code_Code_List(bundledFareChargeCodes);
	}

	public final static String createGDSMessageTypeList() throws ModuleException {
		StringBuilder sb = new StringBuilder();
		GDSExternalCodes.MessageType[] mtArr = GDSExternalCodes.MessageType.values();

		for (MessageType element : mtArr) {
			sb.append("<option value='" + element.getValue() + "'>" + element.getValue() + "</option>");
		}
		return sb.toString();
	}

	public final static String createGDSNamesList() throws ModuleException {
		StringBuilder sb = new StringBuilder();
		GDSExternalCodes.GDS[] mtArr = GDSExternalCodes.GDS.values();

		for (GDS element : mtArr) {
			sb.append("<option value='" + element.getCode() + "'>" + element.getCode() + "</option>");
		}
		return sb.toString();
	}

	public final static String createGDSProcessStatusList() throws ModuleException {
		StringBuilder sb = new StringBuilder();
		GDSExternalCodes.ProcessStatus[] mtArr = GDSExternalCodes.ProcessStatus.values();

		for (ProcessStatus element : mtArr) {
			sb.append("<option value='" + element.getValue() + "'>" + element.getValue() + "</option>");
		}
		return sb.toString();
	}

	public final static String createJourneyTypeList() throws ModuleException {
		StringBuilder sb = new StringBuilder();
		sb.append("<option value='" + ChargeRateJourneyType.ANY + "'>Any</option>");
		sb.append("<option value='" + ChargeRateJourneyType.ONEWAY + "'>Oneway</option>");
		sb.append("<option value='" + ChargeRateJourneyType.RETURN + "'>Return</option>");
		return sb.toString();
	}

	public final static String createChargeGroupList() throws ModuleException {
		return create_Code_Code_List(ChargeGroups);
	}

	public final static String createChargeGroupWithDisplayList() throws ModuleException {
		return create_Code_Description_List(chargeGroupsDisplay);
	}

	public final static String createChargeCategoryList() throws ModuleException {
		return create_Code_Code_List(ChargeCategorys);
	}

	public final static String createStationList() throws ModuleException {
		return create_Code_Description_List(Station);
	}
	
	public final static String createCitiesList() throws ModuleException {
		return create_Code_Description_List(Cities);
	}

	public final static String createParamCarrierCodeList() throws ModuleException {
		return create_Code_Code_List(PARAM_CARRIER_CODES);
	}

	public final static String createParamDescriptionList() throws ModuleException {
		return create_Code_Code_List_WithoutTag(PARAM_DESCRIPTIONS, "paramNameArr");
	}

	public final static String createParamKeyList() throws ModuleException {
		return create_Code_Code_List_WithoutTag(PARAM_KEYS, "paramKeyArr");
	}

	public final static String createParamTypeList() throws ModuleException {
		return create_Code_Code_List(PARAM_TYPES);
	}

	public final static String createStationCodeList() throws ModuleException {
		return create_Code_Code_List(StationCodes);
	}

	public final static String createPGWList() throws ModuleException {
		return create_Code_And_Description_List(PGW_DATA);
	}

	public final static String createStationHtml() throws ModuleException {
		return create_Code_Name_List(StationCountryCode);
	}

	public final static List<Map<String, String>> createStationCodes() throws ModuleException {
		return create_List(stationList);
	}

	public final static List<Map<String, String>> createOwnStationCodes() throws ModuleException {
		String[] params = new String[] { AppSysParamsUtil.getDefaultAirlineIdentifierCode() };
		return create_List(ownStationList, params);
	}

	public final static List<Map<String, String>> createActiveStationCodes() throws ModuleException {
		return create_List(stationListAct);
	}

	public final static String createAircraftModelList_SG() throws ModuleException {
		return create_Code_Description_List(AIRCRAFT_MODELS);
	}

	public final static String createActiveAircraftModelList_SG() throws ModuleException {
		return create_Code_Description_Active(ACTIVE_AIRCRAFT_MODELS);
	}

	public final static String createAircraftModelListForSeatMap_SG() throws ModuleException {
		return create_Code_Description_List(AIRCRAFT_MODELS_WITH_SEAT);
	}

	public final static String createClearAlertActionList() throws ModuleException {
		return create_Code_Description_List(ALERT_ACTIONS);
	}

	public final static String createReporingChargeGroups() throws ModuleException {
		return create_Code_And_Description_List(reporing_Charge_Groups);
	}

	public final static String createUsersList() throws ModuleException {
		return create_Code_Code_List(USER_ID_LIST);
	}

	public final static String createTaskGroupList() throws ModuleException {
		return create_Code_Description_List(TASK_GROUP_LIST);
	}

	public final static String createAuditTemplateList() throws ModuleException {
		return create_Code_Description_List(AUDIT_TEMPLATE_LIST);
	}

	public final static Collection<String[]> createTasks(String params[]) throws ModuleException {
		String selectSql = TASK_LIST;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 2);
		return col;
	}

	public final static String createTaskList(String params[]) throws ModuleException {
		Collection<String[]> col = createTasks(params);
		return create_Code_Description_List(col);

	}

	public final static String createAllTaskList() throws ModuleException {
		return create_Code_Description_List(ALL_TASKS_LIST);
	}

	public final static String createAgentCommiListHtml() throws ModuleException {
		return create_Code_Description_List(AGENT_COMMITION_TYPE_LIST);
	}

	public final static String createBSPCountryList() throws ModuleException {
		return create_Code_Name_List(BSP_COUNTRY_LIST);
	}

	public final static String getBSPAgentCodeCountryCodeList() throws ModuleException {
		return create_Code_Description_List_WithoutTag(BSP_AGENT_CODE_COUNTRY_CODE_LIST, "arrAgents");
	}

	public final static String[] getOctupusCity(String[] params) throws ModuleException {
		String selectSql = OCTUPUS_CITY;
		String[] columns = { "OCT_CITY_CODE", "COUNTRY_CODE" };
		return LookupUtils.LookupDAO().getString(selectSql, params, columns);
	}

	public final static String[] getUfiCity(String[] params) throws ModuleException {
		String selectSql = UFI_CITY;
		String[] columns = { "UFI_CODE", "COUNTRY_CODE", "HOTEL_LINK" };
		return LookupUtils.LookupDAO().getString(selectSql, params, columns);
	}

	public final static String getAgentCurrency(String gsaCode) throws ModuleException {
		String[] arrStr = { gsaCode };
		String strVal = LookupUtils.LookupDAO().getStrObject(GSA_CURRENCY_CODE, arrStr);
		return strVal;
	}

	public final static String getFareType(String pnrSegId, String fareId) throws ModuleException {
		String[] arrStr = { pnrSegId, fareId };
		String strVal = LookupUtils.LookupDAO().getStrObject(FARE_TYPE, arrStr);
		return strVal;
	}

	public final static String getAgentTerrotiry(String gsaCode) throws ModuleException {
		String[] arrStr = { gsaCode };
		String strVal = LookupUtils.LookupDAO().getStrObject(GSA_TRTRY_CODE, arrStr);
		return strVal;
	}

	public final static Collection<String[]> getAirportCurrency(String[] airportCode) throws ModuleException {
		String selectSql = AIRPORT_CURR_CODE;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, airportCode, 1);
		return col;

	}

	public static String createJSArray(String arrayName, Collection<String[]> data) throws ModuleException {
		return getJSArray(arrayName, data);
	}

	public final static List<String[]> getMealNotifyTiming(String airportCode) throws ModuleException {
		String sql = "select mt.meal_notify_timing_id, mt.notify_start, mt.last_notify, mt.notify_gap, mt.status from ml_t_meal_notify_timings mt where mt.airport_code = '"
				+ airportCode.trim() + "'";
		return LookupUtils.LookupDAO().getFiveColumnData(sql);
	}

	public final static List<String[]> getMealAttachedToMT(int mealId) throws ModuleException {
		String sql = "select mc.meal_charge_id, mc.meal_template_id, mc.logical_cabin_class_code, mc.cabin_class_code from ml_t_meal_charge mc where mc.meal_id = "
				+ mealId + " ";
		return LookupUtils.LookupDAO().getFourColumnData(sql);
	}

	/**
	 * generic Array list generator
	 * 
	 * @param paramString
	 * @return String
	 */
	private final static String create_Code_Description_Array(String paramString, String arrayName) throws ModuleException {

		StringBuilder sb = new StringBuilder();
		// StringBuffer sb = new StringBuffer();
		List<String[]> l = LookupUtils.LookupDAO().getFourColumnMap(paramString);
		Iterator<String[]> iter = l.iterator();
		int intCount = 0;
		sb.append(arrayName + " = new Array();");
		while (iter.hasNext()) {
			String s[] = iter.next();
			if ((s != null) && (s.length > 0)) {
				sb.append(arrayName + "[" + intCount + "] = new Array(");
				for (int i = 0; i < s.length; i++) {
					sb.append("'" + s[i] + "'");
					if ((i + 1) < s.length) {
						sb.append(",");
					}
				}
				sb.append(");\n");
				intCount++;
			}
		}
		return sb.toString();
	}

	/**
	 * generic select list generator
	 * 
	 * @param paramString
	 * @return String
	 */
	private final static String create_Code_Description_List(String paramString) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap(paramString);
		Iterator<String[]> iter = l.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			sb.append("<option value='" + s[0] + "'>" + s[1] + "</option>");
		}
		return sb.toString();
	} 
	
	/**
	 * generic select list generator
	 * 
	 * @param paramString
	 * @return String
	 */
	private final static String create_Code_Description_List_Sql(String sql) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		List<String[]> l = LookupUtils.LookupDAO().getTwoColumnsData(sql);
		Iterator<String[]> iter = l.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			sb.append("<option value='" + s[0] + "'>" + s[1] + "</option>");
		}
		return sb.toString();
	}

	private final static String create_Code_Description_Text(String paramString) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		List<String[]> l = LookupUtils.LookupDAO().getThreeColumnData(paramString);
		Iterator<String[]> iter = l.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			sb.append(s[0] + "&" + s[1] + "&" + s[2] + "|");
		}
		return sb.toString();
	}

	private final static String create_Code_Description_Active(String paramString) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		List<String[]> l = LookupUtils.LookupDAO().getThreeColumnData(paramString);
		Iterator<String[]> iter = l.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			if (s[2].equals("ACT")) {
				sb.append("<option value='" + s[0] + "'>" + s[1] + "(" + s[0] + ")" + "</option>");
			}
		}
		return sb.toString();
	}

	private final static List<String> createFirstResultList(String paramString) throws ModuleException {

		ArrayList<String> resultList = new ArrayList<String>();
		List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap(paramString);
		for (String[] results : l) {
			resultList.add(results[0]);
		}
		return resultList;
	}

	private final static List<String> createSingleColumnResultList(String paramString) throws ModuleException {
		return LookupUtils.LookupDAO().getSingleColumnMap(paramString);
	}

	private final static List<String> createSingleColumnResultList(String paramString, Object[] params) throws ModuleException {
		return LookupUtils.LookupDAO().getSingleColumnMap(paramString, params);
	}

	private final static List<String> createFirstResultListWithParam(String paramString, String param) throws ModuleException {

		ArrayList<String> resultList = new ArrayList<String>();
		List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap(paramString, param);
		for (String[] results : l) {
			resultList.add(results[0]);
		}
		return resultList;
	}

	private final static String create_OND_Baggage_Template_Code_List(String paramString) throws ModuleException {
		StringBuffer sb = new StringBuffer();
		List<String[]> l = LookupUtils.LookupDAO().getFourColumnMap(paramString);
		Iterator<String[]> iter = l.iterator();

		while (iter.hasNext()) {
			String s[] = iter.next();
			if (s[2].length() != 0 && s[3].length() != 0) {
				try {
					s[2] = CalendarUtil.convertDate(s[2], "yyyy-MM-dd", CalendarUtil.PATTERN1);
					s[3] = CalendarUtil.convertDate(s[3], "yyyy-MM-dd", CalendarUtil.PATTERN1);
				} catch (ParseException e) {
				}

				s[2] = "(" + s[2] + " - ";
				s[3] = s[3] + ")";
			}
			sb.append("<option value='" + s[0] + "'>" + s[1] + " " + s[2] + s[3] + "</option>");
		}
		return sb.toString();
	}

	private final static String create_Operation_Type_Array(String paramString) throws ModuleException {
		StringBuilder sb = new StringBuilder("var arrOperationTypeData = new Array();");
		int i = 0;
		List<String[]> l = LookupUtils.LookupDAO().getThreeColumnMap(paramString);

		Iterator<String[]> iter = l.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			sb.append("arrOperationTypeData[" + i + "] = new Array('" + s[0] + "','" + s[1] + "','" + s[2] + "'); ");
			i++;
		}
		return sb.toString();

	}

	public final static String create_Logical_Cabin_Class_Sub_List(String paramString) throws ModuleException {
		String cabinClass = "";
		StringBuffer sb = new StringBuffer();
		List<String[]> logicalCabinClasses = LookupUtils.LookupDAO().getFourColumnMap(paramString);
		for (String s[] : logicalCabinClasses) {
			if (!cabinClass.equals(s[1])) {
				sb.append("<option value='" + s[1] + "_" + s[1] + "'>" + s[3] + "</option>");
			}
			sb.append("<option value='" + s[0] + Constants.CARET_SEPERATOR
					+ Constants.UNDERSCORE_SEPERATOR + s[1] + "'>" + "--" + s[2] + "</option>");
			cabinClass = s[1];
		}
		return sb.toString();
	}

	public final static String create_Cabin_Class_list(String paramString) throws ModuleException {
		String cabinClass = "";
		StringBuffer sb = new StringBuffer();
		List<String[]> cabinClasses = LookupUtils.LookupDAO().getTwoColumnMap(paramString);
		for (String s[] : cabinClasses) {
			if (!cabinClass.equals(s[0])) {
				sb.append("<option value='" + s[0] + Constants.UNDERSCORE_SEPERATOR + s[0] + "'>" + s[1]
						+ "</option>");
			}
			cabinClass = s[0];
		}
		return sb.toString();
	}

	/**
	 * generic select list generator
	 * 
	 * @param paramString
	 * @return String
	 */
	private final static String create_CC_Wise_LogicalCC_List(String paramString) throws ModuleException {

		List<String[]> l = LookupUtils.LookupDAO().getThreeColumnMap(paramString);
		Iterator<String[]> iter = l.iterator();
		StringBuilder jsonLogicalCCes = new StringBuilder();
		String cabinClass = null;
		StringBuffer optionList = null;
		int i = 0;

		while (iter.hasNext()) {
			String s[] = iter.next();
			if (cabinClass == null || !cabinClass.equalsIgnoreCase(s[0])) {
				cabinClass = s[0];
				if (i == 0) {
					optionList = new StringBuffer();
					jsonLogicalCCes.append(s[0] + ":");
					optionList.append(s[1] + "-" + s[2]);
				} else {
					jsonLogicalCCes.append(optionList);
					optionList = new StringBuffer();
					jsonLogicalCCes.append("~" + s[0] + ":");
					optionList.append(s[1] + "-" + s[2]);
				}
			} else {
				optionList.append(s[1] + "-" + s[2]);
			}

			if (!iter.hasNext() && optionList.length() > 0) {
				jsonLogicalCCes.append(optionList);
			} else {
				optionList.append(",");
			}
			i++;
		}
		return jsonLogicalCCes.toString();
	}

	/**
	 * generic select list generator
	 * 
	 * @param paramString
	 * @return String
	 */
	public final static String create_Code_Description_List(Collection<String[]> l) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		Iterator<String[]> iter = l.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			sb.append("<option value='" + s[0] + "'>" + s[1] + "</option>");
		}
		return sb.toString();
	}

	private final static String create_Code_And_Description_List(String paramString) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		List<String[]> l = LookupUtils.LookupDAO().getThreeColumnMap(paramString);
		Iterator<String[]> iter = l.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			sb.append("<option value='" + s[0] + "'> " + s[1] + " - " + s[2] + "</option>");
		}
		return sb.toString();
	}

	/**
	 * generic select list generator
	 * 
	 * @param paramString
	 * @return String
	 */
	private final static Collection<String[]> get_Code_Description_List(String paramString) throws ModuleException {

		List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap(paramString);
		return l;
	}

	/**
	 * Generic select list generator
	 * 
	 * @param list
	 * @return
	 * @throws ModuleException
	 */
	private final static String create_Code_Code_List(List<String[]> list) throws ModuleException {
		StringBuffer sb = new StringBuffer();
		Iterator<String[]> iter = list.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			sb.append("<option value='" + s[0] + "'>" + s[0] + "</option>");
		}
		return sb.toString();
	}

	/**
	 * generic select list generator
	 * 
	 * @param paramString
	 * @return String
	 */
	private final static String create_Code_Code_List(String paramString) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap(paramString);

		Iterator<String[]> iter = l.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			sb.append("<option value='" + s[0] + "'>" + s[0] + "</option>");
		}
		return sb.toString();
	}

	private final static String createSingleColumnOptionList(String paramString) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		List<String> l = LookupUtils.LookupDAO().getSingleColumnMap(paramString);

		for (String code : l) {
			sb.append("<option value='" + code + "'>" + code + "</option>");
		}
		return sb.toString();
	}



	private final static String create_Single_Result_Code_Code_List(String paramString) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		Collection<String[]> l = LookupUtils.LookupDAO().getList2(paramString, 1);

		Iterator<String[]> iter = l.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			sb.append("<option value='" + s[0] + "'>" + s[0] + "</option>");
		}
		return sb.toString();
	}

	/**
	 * generic select list generator
	 * 
	 * @param paramString
	 * @return String
	 */
	private final static String create_Code_Name_List(String paramString) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap(paramString);

		Iterator<String[]> iter = l.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			sb.append("<option value='" + s[0] + "'>" + s[1] + "</option>");
		}
		return sb.toString();
	}

	/**
	 * generic select list generator
	 * 
	 * @param paramString
	 * @return String
	 */

	private final static String create_Code_Code_List_WithoutTag(String paramString) throws ModuleException {

		StringBuilder sb = new StringBuilder("var arrAptData = new Array();");
		int i = 0;

		List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap(paramString);
		Iterator<String[]> iter = l.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			sb.append("arrAptData[" + i + "] = new Array('" + s[0] + "','" + s[0] + "'); ");
			i++;
		}
		return sb.toString();
	}

	/**
	 * generic select list generator
	 * 
	 * @param paramString
	 * @return String
	 */
	private final static String create_Code_Code_List_WithoutTag(String paramString, String arrName) throws ModuleException {

		StringBuilder sb = new StringBuilder();
		int i = 0;
		List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap(paramString);
		Iterator<String[]> iter = l.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			String code = s[0];
			code = code.replace("\\", "\\\\");
			sb.append(arrName + "[" + i + "] = new Array('" + code + "','" + code + "'); ");
			i++;
		}
		return sb.toString();
	}

	/**
	 * generic select list generator
	 * 
	 * @param paramString
	 * @return String
	 */
	private final static String create_Code_Description_List_WithoutTag(String paramString, String arrName)
			throws ModuleException {

		StringBuilder sb = new StringBuilder();
		int i = 0;
		List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap(paramString);
		Iterator<String[]> iter = l.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			String code = s[0];
			String desc = StringUtils.replace(s[1], "'", SINGLE_QUOTE);

			sb.append(arrName + "[" + i + "] = new Array('" + code + "','" + desc + "'); ");
			i++;
		}
		return sb.toString();
	}

	/**
	 * generic select list generator
	 * 
	 * @param paramString
	 * @return String
	 */
	private final static List<String[]> createCodeDescriptionList(String paramString) throws ModuleException {
		return LookupUtils.LookupDAO().getTwoColumnMap(paramString);

	}

	/**
	 * Generic result generator. Will return the data in a Map. Because the results will be in a Map, result rows with
	 * the same first result will be replaced regardless of the second result value.
	 * 
	 * @return A Map of results KEY -> First result, VALUE -> Second result
	 */
	private final static Map<String, String> createFirstResultKeyMap(String paramString) throws ModuleException {
		List<String[]> twoColumnResults = LookupUtils.LookupDAO().getTwoColumnMap(paramString);
		Map<String, String> resultMap = new HashMap<String, String>(twoColumnResults.size());

		for (String[] results : twoColumnResults) {
			resultMap.put(results[0], results[1]);
		}

		return resultMap;
	}

	/**
	 * Generic result generator. Will return the data in a Map. Because the results will be in a Map, result rows with
	 * the same second result will be replaced regardless of the first result value.
	 * 
	 * @return A Map of results KEY -> Second result, VALUE -> First result
	 */
	private final static Map<String, String> createSecondResultKeyMap(String paramString) throws ModuleException {
		List<String[]> twoColumnResults = LookupUtils.LookupDAO().getTwoColumnMap(paramString);
		Map<String, String> resultMap = new HashMap<String, String>(twoColumnResults.size());

		for (String[] results : twoColumnResults) {
			resultMap.put(results[1], results[0]);
		}

		return resultMap;
	}

	/**
	 * 
	 * @param paramString
	 * @return
	 * @throws ModuleException
	 */
	private final static List<String[]> createLccCodeDescriptionList(String paramString) throws ModuleException {
		return LookupUtils.lccLookupDAO().getTwoColumnMap(paramString);

	}

	/**
	 * 
	 * @param paramString
	 * @return
	 * @throws ModuleException
	 */
	private final static List<String[]> createSubAirportCodeDescriptionList(String paramString) throws ModuleException {
		return LookupUtils.LookupDAO().getFourColumnMap(paramString);

	}

	/**
	 * generic select list generator
	 * 
	 * @param paramString
	 * @return String
	 */
	private final static String create_Code_Description_Combine_List_WithoutTag(String paramString, String arrName)
			throws ModuleException {

		StringBuilder sb = new StringBuilder();
		int i = 0;
		List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap(paramString);

		Iterator<String[]> iter = l.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			String code = s[0];
			String desc = StringUtils.replace(s[1], "'", SINGLE_QUOTE);

			sb.append(arrName + "[" + i + "] = new Array('" + code + "','" + desc + "','" + code + " - " + desc + "'); ");
			i++;
		}
		return sb.toString();
	}

	/**
	 * This will create a array of airports operated by own carrier.
	 * 
	 * @param paramString1
	 * @param arrName
	 * @return
	 */
	private final static String create_Airport_Owner_List_WithoutTag(String paramString1, String arrName) {
		List<String[]> listOwn = LookupUtils.LookupDAO().getTwoColumnMap(paramString1);
		StringBuilder sb = new StringBuilder();
		for (String[] rec : listOwn) {
			sb.append(arrName + "['" + rec[0] + "'] = 'OWN'; ");
		}

		if (AppSysParamsUtil.enableCityBasesFunctionality()) {
			Map<String, String> cityCodeMap = new HashMap<String, String>();
			List<String[]> listLcc = LookupUtils.LookupDAO().getTwoColumnMap(CITY_DESCRIPTION_XBE);

			if (listLcc != null && !listLcc.isEmpty()) {
				// filter out the duplicate records

				for (String[] rec : listLcc) {
					if (!cityCodeMap.containsKey(rec[0])) {
						cityCodeMap.put(rec[0], rec[1]);
						sb.append(arrName + "['" + rec[0] + "'] = 'OWN'; ");

					}
				}

			}

		}

		return sb.toString();
	}

	/**
	 * 
	 * 
	 * @param paramString
	 * @return String
	 */
	private final static String create_Code_Description_Combine_Lcc_Aggregate_List_WithoutTag(String paramString1,
			String paramString2, boolean includeInterline, String arrName) throws ModuleException {
		StringBuilder sb = new StringBuilder();

		Map<String, String> aggregatedMap = new HashMap<String, String>();
		// get the own records.
		List<String[]> listOwn = LookupUtils.LookupDAO().getTwoColumnMap(paramString1);
		for (String[] rec : listOwn) {
			aggregatedMap.put(rec[0], rec[1]);
		}

		String defaultCarrier = AppSysParamsUtil.getDefaultCarrierCode();
		String[] arg = new String[1];
		int[] argType = new int[1];
		arg[0] = defaultCarrier;
		argType[0] = java.sql.Types.VARCHAR;

		// get the lcc records if it allow by privilege.
		if (includeInterline) {
			List<String[]> listLcc = LookupUtils.lccLookupDAO().getTwoColumnMap(paramString2);
			// filter out the duplicate records
			for (String[] rec : listLcc) {
				if (!aggregatedMap.containsKey(rec[0])) {
					aggregatedMap.put(rec[0], rec[1]);
				}
			}
		}

		int i = 0;
		TreeSet<String> keys = new TreeSet<String>(aggregatedMap.keySet());
		for (String key : keys) {
			String code = key;
			String desc = StringUtils.replace(aggregatedMap.get(key), "'", SINGLE_QUOTE);

			sb.append(arrName + "[" + i + "] = new Array('" + code + "','" + desc + "','" + code + " - " + desc + "'); ");
			i++;
		}

		return sb.toString();
	}

	/**
	 * 
	 * 
	 * @param paramString
	 * @return String
	 */
	private final static String create_Code_Description_Combine_Lcc_Aggregate_WSubStations_List_WithoutTag(String paramString1,
			String paramString2, boolean includeInterline, String arrName, boolean isSubStationAllowed, String subStationParam,
			List<String[]> allowedAirportList) throws ModuleException {
		StringBuilder sb = new StringBuilder();

		Map<String, String> aggregatedMap = new HashMap<String, String>();
		List<String> surfaceStations = new ArrayList<>();
		List<String> lccStations = new ArrayList<>();
		// get the own records.
		if (allowedAirportList != null) {
			for (String[] ap : allowedAirportList) {
				aggregatedMap.put(ap[0], ap[1]);
			}
		} else {
			List<String[]> listOwn = LookupUtils.LookupDAO().getThreeColumnMap(paramString1);
			for (String[] rec : listOwn) {
				aggregatedMap.put(rec[0], rec[1]);
				if("Y".equals(rec[2])) {
					surfaceStations.add(rec[0]);
				}
			}

			String defaultCarrier = AppSysParamsUtil.getDefaultCarrierCode();
			String[] arg = new String[1];
			int[] argType = new int[1];
			arg[0] = defaultCarrier;
			argType[0] = java.sql.Types.VARCHAR;

			// get the lcc records if it allow by privilege.
			if (includeInterline) {
				List<String[]> listLcc = LookupUtils.lccLookupDAO().getTwoColumnMap(paramString2);
				// filter out the duplicate records
				for (String[] rec : listLcc) {
					if (!aggregatedMap.containsKey(rec[0])) {
						aggregatedMap.put(rec[0], rec[1]);
						lccStations.add(rec[0]);
					}
				}
			}
		}
		Map<String, List<Map<String, String>>> subStationMap = new HashMap<String, List<Map<String, String>>>();
		if (isSubStationAllowed) {
			Collection<String[]> subStationList = LookupUtils.LookupDAO().getThreeColumnMap(subStationParam);
			for (String[] rec : subStationList) {
				// 0 - Airport Code | 1 - Sub Code | 2 - Sub Airport Desc
				Map<String, String> recSubAriportDataList = new HashMap<String, String>();
				recSubAriportDataList.put(rec[1], rec[2]);
				List<Map<String, String>> subMapList = subStationMap.get(rec[0]);
				if (subMapList == null) {
					subMapList = new ArrayList<Map<String, String>>();
					subStationMap.put(rec[0], subMapList);
				}
				subMapList.add(recSubAriportDataList);
			}
		}

		int i = 0;
		TreeSet<String> keys = new TreeSet<String>(aggregatedMap.keySet());
		for (String key : keys) {
			String code = key;
			List<Map<String, String>> mappedSubStationList = null;
			String desc = StringUtils.replace(aggregatedMap.get(key), "'", SINGLE_QUOTE);
			if (isSubStationAllowed) {
				mappedSubStationList = subStationMap.get(code);
			}
			if (mappedSubStationList == null) {
				sb.append(arrName + "[" + i + "] = new Array('" + code + "','" + desc + "','" + code + " - " + desc + "','N','" + code + "_N',"
						+ surfaceStations.contains(code) + "," +  lccStations.contains(code) + "); ");
				i++;
			} else {
				String codeVal = SubStationUtil.GROUND_STATION_PARENT_CODE_IDENTIFIER + code;
				sb.append(arrName + "[" + i + "] = new Array('" + codeVal + "','" + desc + "','" + code + " - " + desc + "','N','" + code + "_N',"
						+ surfaceStations.contains(code) + "," +  lccStations.contains(code) + "); ");
				i++;
				for (Map<String, String> mapedData : mappedSubStationList) {
					for (Entry<String, String> mapeEntry : mapedData.entrySet()) {
						String sub_code = code + SubStationUtil.SUB_STATION_CODE_SEPERATOR + mapeEntry.getKey();
						// adding &nbsb s may or may not work correctly. should
						// refer documentation
						String sub_desc = SubStationUtil.SUB_STATION_DISPLAY_SEPERATOR + mapeEntry.getKey() + " - "
								+ mapeEntry.getValue();
						sb.append(arrName + "[" + i + "] = new Array('" + sub_code + "','" + sub_code + "','" + sub_desc + "','N','" + sub_code + "_N',"
								+ surfaceStations.contains(code) + "," +  lccStations.contains(code) + "); ");
						i++;
					}
				}

			}
		}
		
		if (AppSysParamsUtil.enableCityBasesFunctionality()) {
			Map<String, String> cityCodeMap = new HashMap<String, String>();
			List<String[]> listLcc = LookupUtils.LookupDAO().getTwoColumnMap(CITY_DESCRIPTION_XBE);
			String appendDisplayText = " "+AppSysParamsUtil.getCitySearchOnDDescriptionText();			
			
			// get the lcc records if it allow by privilege.
			if (includeInterline) {
				List<String[]> listLcc2 = LookupUtils.lccLookupDAO().getTwoColumnMap(LCC_CITY_DESCRIPTION_XBE);

				if (listLcc == null) {
					listLcc = new ArrayList<>();
				}

				if (listLcc2 != null && !listLcc2.isEmpty()) {
					listLcc.addAll(listLcc2);
				}

			}
			
			
			if (listLcc != null && !listLcc.isEmpty()) {
				// filter out the duplicate records
				String code = null;
				String desc = null;
				for (String[] rec : listLcc) {
					if (!cityCodeMap.containsKey(rec[0])) {
						cityCodeMap.put(rec[0], rec[1]);
						code = rec[0];
						desc = StringUtils.replace(rec[1], "'", SINGLE_QUOTE);
						sb.append(arrName + "[" + i + "] = new Array('" + code + "','" + desc + "" + appendDisplayText + "','" + code + " - " + desc + ""
								+ appendDisplayText + "','Y','" + code + "_Y'," + surfaceStations.contains(code) + "," +  lccStations.contains(code)  + "); ");
						i++;
					}
				}

			}

		}
		

		return sb.toString();
	}

	private final static Map<String, String> createAirportsWithoutSubstaions(String paramString1, String paramString2,
			boolean includeInterline, List<String[]> allowedAirportList, Collection<String> allowedCarrierCodes)
			throws ModuleException {

		Map<String, String> aggregatedMap = new HashMap<String, String>();
		// get the own records.
		if (allowedAirportList != null) {
			for (String[] ap : allowedAirportList) {
				aggregatedMap.put(ap[0], ap[1]);
			}
		} else {
			List<String[]> listOwn = LookupUtils.LookupDAO().getTwoColumnMap(paramString1);
			for (String[] rec : listOwn) {
				aggregatedMap.put(rec[0], rec[1]);
			}

			// get the lcc records if it allow by privilege.
			if (includeInterline) {
				// Getting the airport list from new cached airport implementation
				Map<String, CachedAirportDTO> allAirports = WPModuleUtils.getAirportBD().getAllAirportOperatorMap();
				// Collection<String[]> listLcc = LookupUtils.lccLookupDAO().getList(paramString2, params, 3);
				// filter out the duplicate records
				for (String airport : allAirports.keySet()) {
					if (!aggregatedMap.containsKey(airport)) {
						CachedAirportDTO cadto = allAirports.get(airport);
						// No need to check null value as cadto.getOperatedBy() cant be null
						String[] carrierCodes = cadto.getOperatedBy().trim().split(",");

						for (String carrierCode : carrierCodes) {
							if (allowedCarrierCodes.contains(carrierCode)) {
								aggregatedMap.put(airport, cadto.getAirportName());
								break;
							}
						}
					}
				}
			}
		}

		return aggregatedMap;
	}

	/**
	 * generic select list generator
	 * 
	 * @param paramString
	 * @return String
	 */
	private final static String create_List_For_Display(String paramString, String arrName, Object[] obj, int[] types)
			throws ModuleException {

		StringBuilder sb = new StringBuilder();
		int i = 0;
		List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap(paramString, obj, types);

		Iterator<String[]> iter = l.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			String code = s[0];
			String desc = StringUtils.replace(StringUtil.getUnicode(s[1]), "'", SINGLE_QUOTE);

			sb.append(arrName + "[" + i + "] = new Array('" + code + "','" + desc + "',''); ");
			i++;
		}
		return sb.toString();
	}

	private final static Map<String, String> create_AirportCode_Map(String paramString, Object[] obj, int[] types) {
		Map<String, String> airportCodeMap = new HashMap<String, String>();
		int i = 0;
		List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap(paramString, obj, types);
		while (i < l.size()) {
			String[] airportCodes = l.get(i);
			airportCodeMap.put(airportCodes[0], airportCodes[1]);
			i++;
		}
		return airportCodeMap;

	}

	/**
	 * will return a list, where each list iterator will be a map containing columns as the keyset and its values *
	 */
	/** * first list item will contain *** */
	/** * KEYSET: [AIRPORT_CODE, AIRPORT_NAME, CONTACT, TELEPHONE]****** */
	/**
	 * * ENTRY SET: [AIRPORT_CODE=SSH, AIRPORT_NAME=Sham (SSH), CONTACT=2423, TELEPHONE=23423]******
	 */
	private final static List<Map<String, String>> create_List(String paramString) throws ModuleException {
		List<Map<String, String>> list = LookupUtils.LookupDAO().getList(paramString);
		return list;

	}

	private final static List<Map<String, String>> create_List(String paramString, Object[] params) throws ModuleException {
		List<Map<String, String>> list = LookupUtils.LookupDAO().getList(paramString, params);
		return list;
	}

	public final static String getJSMap(String arrName, Collection<String[]> colData) throws ModuleException {
		StringBuffer sb = new StringBuffer();
		Iterator<String[]> iter = colData.iterator();
		sb.append(arrName + " = {");
		while (iter.hasNext()) {
			String[] s = iter.next();
			sb.append("'" + s[0] + "' : '" + s[1] + "'" + ",");
		}
		sb.append("};");

		return sb.toString();
	}

	public final static String getJSArray(String arrName, Collection<String[]> colData) throws ModuleException {
		StringBuffer sb = new StringBuffer();
		Iterator<String[]> iter = colData.iterator();

		int i = 0;

		while (iter.hasNext()) {
			String[] s = iter.next();

			sb.append(arrName);
			sb.append("[");
			sb.append(i);
			sb.append("]=new Array('");

			for (int j = 0; j < s.length; j++) {
				if (j != 0) {
					sb.append("','");
				}

				sb.append(s[j]);
			}

			sb.append("');");
			i++;
		}

		return sb.toString();
	}

	/**
	 * creates associative array eg: arr['0011'] = new Array('0011','Mihin Lanka','true','23');
	 * 
	 * @param 1 - Array Name, 2 - Data Set, 3 - the element ID of the data set that contains the primary key / key
	 * @return String
	 * @throws ModuleException
	 */
	private final static String getJSAssociativeArray(String arrName, Collection<String[]> colData, int keyElement)
			throws ModuleException {
		StringBuilder sb = new StringBuilder();
		Iterator<String[]> iter = colData.iterator();

		// int i = 0;

		while (iter.hasNext()) {
			String[] s = iter.next();
			if (keyElement < 0 || keyElement > s.length) {
				keyElement = 0;
			}

			sb.append(arrName);
			sb.append("['");
			sb.append(s[keyElement]);
			sb.append("']=new Array('");

			for (int j = 0; j < s.length; j++) {
				if (j != 0) {
					sb.append("','");
				}

				sb.append(s[j]);
			}

			sb.append("');");
			// i++;
		}

		return sb.toString();
	}

	/**
	 * creates reporting travel agents array
	 * 
	 * @param agentCode
	 * @return
	 * @throws ModuleException
	 */
	public static final String createReportingTAsWithCreditArray(String agentCode) throws ModuleException {
		int colCount = 3;
		String selectSql = REPORTING_AGENTS_WITH_CREDIT;
		String[] params = new String[] { agentCode };
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		String strArray = getJSArray("arrTravelAgents", colData);

		return strArray;
	}

	public static final Collection<String[]> createReportingTAsWithCredit(String airlineCode, String agentCode, int colCount)
			throws ModuleException {
		String selectSql = REPORTING_AGENTS_WITH_CREDIT;
		String[] params = new String[] { airlineCode, agentCode };
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		return colData;
	}

	/**
	 * creates reporting travel agents array
	 * 
	 * @param agentCode
	 * @return
	 * @throws ModuleException
	 */
	public static final String createRegionsArray(String arrayName) throws ModuleException {
		int colCount = 2;
		String selectSql = ALL_REGIONS_LIST;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		String strArray = getJSArray(arrayName, colData);

		return strArray;
	}

	/**
	 * creates reporting travel agents array
	 * 
	 * @param agentCode
	 * @return
	 * @throws ModuleException
	 */
	public static final String createSalesChannelsArray(String arrayName) throws ModuleException {
		int colCount = 2;
		String selectSql = ALL_SALES_CHANNELS_LIST;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		String strArray = getJSArray(arrayName, colData);

		return strArray;
	}

	public static final Collection<String[]> getAllTAsCollection() throws ModuleException {
		int colCount = 2;
		String selectSql = ALL_AGENTS;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}

	public static final Collection<String[]> getAllActiveTAsCollection() throws ModuleException {
		int colCount = 5;
		String selectSql = ALL_ACTIVE_AGENTS;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}

	public static final Collection<String[]> getAllBookingCategories() throws ModuleException {
		int colCount = 4;
		String selectSql = ALL_BOOKING_CATEGORIES;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}

	public static final Collection<String[]> getAllActiveOwnTAsCollection(String defaultAirlineCode) throws ModuleException {
		int colCount = 4;
		String selectSql = ALL_ACTIVE_OWN_AGENTS;
		String[] params = new String[] { defaultAirlineCode };
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}

	public static final Collection<String[]> getAllIPGsCollectionForIBE() throws ModuleException {
		int colCount = 2;
		String selectSql = ALL_IBEIPGS;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}

	public static final Collection<String[]> createAllTAsWithCredit(String airlineCode, int colCount) throws ModuleException {
		String selectSql = ALL_AGENTS_WITH_CREDITS;
		String[] params = new String[] { airlineCode };
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}

	public static final Map<String, Integer> createCabinClassRankingMap() throws ModuleException {
		int colCount = 2;
		String selectSql = CABIN_CLASS_RANKING_MAP;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		Map<String, Integer> map = new HashMap<String, Integer>();
		for (String[] arr : colData) {
			if (arr != null) {
				map.put(arr[0], new Integer(arr[1]));
			}
		}
		return map;
	}

	public static final String createCabinClassCodeArray() throws ModuleException {
		int colCount = 2;
		String selectSql = CABIN_CLASS_CODES;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		String strArray = getJSArray("arrCos", colData);
		return strArray;
	}

	public static final String createCosWithLogicalCabinClassList() throws ModuleException {
		int colCount = 5;
		String selectSql = LOGICAL_CABIN_CLASS_CODES;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		List<String[]> colSelData = new ArrayList<String[]>();
		if (colData != null) {
			LinkedHashMap<String, List<String[]>> map = new LinkedHashMap<String, List<String[]>>();
			for (String[] strArr : colData) {
				String ccCode = strArr[0];
				String status = strArr[2];
				if ("ACT".equals(status)) {
					if (!map.containsKey(ccCode)) {
						map.put(ccCode, new ArrayList<String[]>());
					}

					map.get(ccCode).add(strArr);
				}
			}

			for (Entry<String, List<String[]>> entry : map.entrySet()) {
				String cabinClassCode = entry.getKey();

				String[] ccStrArr = new String[2];
				ccStrArr[0] = cabinClassCode;

				List<String[]> logicalCCList = entry.getValue();
				if (logicalCCList != null && logicalCCList.size() > 0) {
					for (String[] qryArr : logicalCCList) {

						if (ccStrArr[1] == null) {
							ccStrArr[1] = qryArr[3];
							colSelData.add(ccStrArr);
						}

						String[] logicalCCArr = new String[2];
						logicalCCArr[0] = qryArr[1] + "-" + cabinClassCode;
						logicalCCArr[1] = "-- " + ((qryArr[4] == null || "".equals(qryArr[4])) ? qryArr[1] : qryArr[4]);
						colSelData.add(logicalCCArr);
					}
				}
			}
		}
		String str = getJSArray("arrLogicalCC", colSelData);
		return str;
	}

	public static final String createCreditCardTypesArray() throws ModuleException {
		int colCount = 2;
		String selectSql = CREDIT_CARD_TYPES;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		String strArray = getJSArray("arrCType", colData);
		return strArray;
	}

	public static Map<String, Integer> creditCardAlphaCodeMap() throws ModuleException {
		int colCount = 2;
		String selectSql = CREDIT_CARD_TYPES_ALPHA;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		Map<String, Integer> map = new HashMap<String, Integer>();
		for (String[] arr : colData) {
			if (arr != null && arr[0] != null) {
				map.put(arr[0], new Integer(arr[1]));
			}
		}
		return map;
	}

	public static final String createCreditCardTypesForPgArray(String arrayName, Integer pgId) throws ModuleException {
		int colCount = 2;
		String selectSql = CREDIT_CARD_TYPES_FOR_PG;
		String[] params = new String[] { pgId.toString() };
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		String strArray = getJSArray(arrayName, colData);
		return strArray;
	}

	public static final Collection<String[]> createCreditCardTypesForPg(Integer pgId) throws ModuleException {
		int colCount = 2;
		String selectSql = CREDIT_CARD_TYPES_FOR_PG;
		String[] params = new String[] { pgId.toString() };
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		return colData;
	}

	public static final Map<String, Collection<String[]>> createCreditCardTypesForAllPGW() throws ModuleException {
		int colCount = 3;
		String selectSql = CREDIT_CARD_TYPES_FOR_ALL_PGW;
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, new String[] {}, colCount);
		Map<String, Collection<String[]>> map = new HashMap<String, Collection<String[]>>();
		for (String[] arr : colData) {
			String key = arr[2];
			if (!map.containsKey(key)) {
				map.put(key, new ArrayList<String[]>());
			}
			map.get(key).add(new String[] { arr[0], arr[1], "" });

		}
		return map;
	}

	public static final String createFixedBCFareRuleArray() throws ModuleException {
		int colCount = 1;
		String selectSql = FIXED_BC_FARERULE_CHECK;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		String strArray = getJSAssociativeArray("fixedBCFareRuleArr", colData, 0);
		return strArray;
	}

	public static final List<String[]> createFixedBCFareRuleArrayList() throws ModuleException {
		int colCount = 1;
		String selectSql = FIXED_BC_FARERULE_CHECK;
		String[] params = new String[] {};
		List<String[]> colData = (List<String[]>) LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}

	public static final String createDirectRouteUsageArray() throws ModuleException {
		int colCount = 1;
		String selectSql = DIRECT_ROUT_USAGE_CHECK;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		String strArray = getJSAssociativeArray("directRoutUsageArr", colData, 0);

		return strArray;
	}

	public static final String createRoutesInUseArray() throws ModuleException {
		int colCount = 1;
		String selectSql = ROUTS_IN_USE;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		String strArray = getJSAssociativeArray("routsInUse", colData, 0);
		return strArray;
	}

	public static final String createPaxTypesAssociativeArray() throws ModuleException {
		int colCount = 2;
		String selectSql = PAX_TYPES;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		String strArray = getJSAssociativeArray("defaultPaxTypes", colData, 0);
		return strArray;
	}

	public static final String createPaxTypesArray() throws ModuleException {
		int colCount = 2;
		String selectSql = PAX_TYPES;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		String strArray = getJSArray("paxTypes", colData);
		return strArray;
	}

	public static final String createPaxTitleArray() throws ModuleException {
		int colCount = 2;
		String selectSql = PAX_TITLES;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		String strArray = getJSArray("arrTitle", colData);
		return strArray;
	}

	public static final String createPaxTitleArrayWithChild() throws ModuleException {
		int colCount = 2;
		String selectSql = PAX_TITLES_CHILD;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		String strArray = getJSArray("arrTitleChild", colData);
		return strArray;
	}

	public static final String createPaxTitleArrayForAdult() throws ModuleException {
		int colCount = 2;
		String selectSql = PAX_TITLES_ADULT;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		String strArray = getJSArray("arrTitle", colData);
		return strArray;
	}

	/**
	 * Creates an Array with infant Titles
	 * 
	 */
	public static final String createInfantTitlesArray() throws ModuleException {

		int colCount = 2;
		String selectSql = PAX_TITLES_INFANT;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		String strArray = getJSArray("arrInfantTitles", colData);
		return strArray;
	}

	public static final String createPaxTitleVisibleArray() throws ModuleException {
		int colCount = 2;
		String selectSql = PAX_TITLES_VISIBLE;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		String strArray = getJSArray("arrPaxTitle", colData);
		return strArray;
	}

	public static final String createPaxTitleForChildVisibleArray() throws ModuleException {
		int colCount = 2;
		String selectSql = PAX_CHILD_TITLES_VISIBLE;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		String strArray = getJSArray("arrChdTitle", colData);
		return strArray;
	}

	public static final String createPaxTitleForAdultVisibleArray() throws ModuleException {
		int colCount = 2;
		String selectSql = PAX_ADULT_TITLES_VISIBLE;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		String strArray = getJSArray("arrAdtTitle", colData);
		return strArray;
	}

	public static final String createSSRCodeArray(AppIndicatorEnum appIndicatorEnum, String array) throws ModuleException {
		Map<Integer, SSRInfoDTO> ssrInfoMap = globalConfig.getSSRInfoByIdMap();
		Collection<SSRInfoDTO> ssrInfos = ssrInfoMap.values();
		StringBuilder sb = new StringBuilder(ssrInfos.size() * 20);

		sb.append(array + "=[");

		boolean firstTime = true;
		for (SSRInfoDTO infoDTO : ssrInfos) {
			if (infoDTO.getSSRCategoryId() == 1) {
				if ((appIndicatorEnum == null)
						|| (SalesChannelsUtil.isAppIndicatorWebOrMobile(appIndicatorEnum) && infoDTO.isShownInIBE())
						|| (AppIndicatorEnum.APP_XBE.equals(appIndicatorEnum) && infoDTO.isShownInXBE())
						|| (AppIndicatorEnum.APP_GDS.equals(appIndicatorEnum) && infoDTO.isShownInGDS())
						|| (AppIndicatorEnum.APP_WS.equals(appIndicatorEnum) && infoDTO.isShownInWs())) {

					if (!firstTime) {
						sb.append(",");
					}

					sb.append("['");
					sb.append(infoDTO.getSSRCode());
					sb.append("','");
					sb.append(infoDTO.getSSRDescription());
					sb.append("']");
					firstTime = false;
				}
			}

		}

		sb.append("];");

		return sb.toString();
	}

	public final static List<Map<String, String>> createChargegroupsForBCList() throws ModuleException {
		return create_List(chargegroupsForBC);
	}

	public final static String createPaxCatagoryList() throws ModuleException {
		StringBuilder sb = new StringBuilder();
		Collection<PaxCategoryTO> colPaxCategoryTO = globalConfig.getPaxCategory();
		PaxCategoryTO paxCategoryTO;

		for (Iterator<PaxCategoryTO> itPaxCategoryCode = colPaxCategoryTO.iterator(); itPaxCategoryCode.hasNext();) {
			paxCategoryTO = itPaxCategoryCode.next();

			if (paxCategoryTO.getStatus() == PaxCategoryTO.PaxCategoryStatusCodes.ACT) {
				sb.append("<option value='" + paxCategoryTO.getPaxCategoryCode() + "'>" + paxCategoryTO.getPaxCategoryDesc()
						+ "</option>");
			}
		}

		return sb.toString();
	}

	public final static List<String[]> createPaxCatagoryArrayList() throws ModuleException {

		// StringBuilder sb = new StringBuilder();
		Collection<PaxCategoryTO> colPaxCategoryTO = globalConfig.getPaxCategory();
		PaxCategoryTO paxCategoryTO;

		List<String[]> paxCatList = new ArrayList<String[]>();
		String[] paxArray = null;

		for (Iterator<PaxCategoryTO> itPaxCategoryCode = colPaxCategoryTO.iterator(); itPaxCategoryCode.hasNext();) {
			paxCategoryTO = itPaxCategoryCode.next();

			if (paxCategoryTO.getStatus() == PaxCategoryTO.PaxCategoryStatusCodes.ACT) {
				paxArray = new String[2];
				paxArray[0] = paxCategoryTO.getPaxCategoryCode();
				paxArray[1] = paxCategoryTO.getPaxCategoryDesc();
				paxCatList.add(paxArray);
			}
		}

		return paxCatList;
	}

	/**
	 * Get List of paxCategories
	 * 
	 * @param status
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<PaxCategoryTO> getPaxCategories(String status) throws ModuleException {

		Collection<PaxCategoryTO> colPaxCategories = new ArrayList<PaxCategoryTO>();
		Collection<PaxCategoryTO> colPaxCategoryTO = globalConfig.getPaxCategory();
		Iterator<PaxCategoryTO> it = colPaxCategoryTO.iterator();
		PaxCategoryTO paxCategoryTO;

		while (it.hasNext()) {
			paxCategoryTO = it.next();

			if (paxCategoryTO != null) {
				if ((paxCategoryTO.getStatus() != null)
						&& ((status == null) || (status.trim().equals(paxCategoryTO.getStatus().toString())))) {
					colPaxCategories.add(paxCategoryTO);
				}
			}
		}
		return colPaxCategories;
	}

	public static final String createPaxCategory(String status) throws ModuleException {
		StringBuilder sb = new StringBuilder();
		int i = 0;

		Collection<PaxCategoryTO> colPaxCategoryTO = globalConfig.getPaxCategory();
		Iterator<PaxCategoryTO> it = colPaxCategoryTO.iterator();
		PaxCategoryTO paxCategoryTO;

		while (it.hasNext()) {
			paxCategoryTO = it.next();

			if (paxCategoryTO != null) {
				if ((paxCategoryTO.getStatus() != null)
						&& ((status == null) || (status.trim().equals(paxCategoryTO.getStatus().toString())))) {
					sb.append("arrCusType[");
					sb.append(i);
					sb.append("] = new Array();");
					sb.append("arrCusType[");
					sb.append(i);
					sb.append("][0] = '");
					sb.append(paxCategoryTO.getPaxCategoryCode());
					sb.append("';");
					sb.append("arrCusType[");
					sb.append(i);
					sb.append("][1] = '");
					sb.append(paxCategoryTO.getPaxCategoryDesc() == null ? "" : paxCategoryTO.getPaxCategoryDesc());
					sb.append("';");
					sb.append("arrCusType[");
					sb.append(i);
					sb.append("][2] = '");
					sb.append(paxCategoryTO.getDefaultCurrency() == null ? "" : paxCategoryTO.getDefaultCurrency());
					sb.append("';");
					sb.append("arrCusType[");
					sb.append(i);
					sb.append("][3] = '");
					sb.append(paxCategoryTO.getDefaultSortOrder());
					sb.append("';");
					sb.append("arrCusType[");
					sb.append(i);
					sb.append("][4] = '");
					sb.append(paxCategoryTO.getStatus() == null ? "" : paxCategoryTO.getStatus().toString());
					sb.append("';");
					i++;
				}
			}
		}
		return sb.toString();
	}

	public final static String createPaxCategoryFoid(String status, String appName) throws ModuleException {
		StringBuilder sb = new StringBuilder();
		int i = 0;

		@SuppressWarnings("rawtypes")
		List allPaxConfig = globalConfig.getpaxConfig(appName);

		String paxCatCode = "";
		String paxCatDesc = "";
		String defaultCurr = "";
		String defaultSortOrd = "";
		String foidNo = "";
		String foidCode = "";
		String foidDesc = "";
		String paxTypeCode = "";
		String foidrequired = "";
		String paxStatus = "";
		String foidUnique = "";

		for (Object obj : allPaxConfig) {
			if (appName.equals(AppIndicatorEnum.APP_XBE.toString())) {
				XBEPaxConfigDTO xbeConfig = (XBEPaxConfigDTO) obj;

				if (xbeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PASSPORT)) {
					paxCatCode = xbeConfig.getPaxCatCode();
					foidCode = xbeConfig.getSsrCode();
					paxTypeCode = xbeConfig.getPaxTypeCode();
					foidrequired = xbeConfig.isXbeMandatory() ? "Y" : "N";
					foidUnique = xbeConfig.getUniqueness();
				} else {
					continue;
				}
			} else if (SalesChannelsUtil.isAppIndicatorWebOrMobile(appName)) {
				IBEPaxConfigDTO ibeConfig = (IBEPaxConfigDTO) obj;
				if (ibeConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PASSPORT)) {
					paxCatCode = ibeConfig.getPaxCatCode();
					foidCode = ibeConfig.getSsrCode();
					paxTypeCode = ibeConfig.getPaxTypeCode();
					foidrequired = ibeConfig.isIbeMandatory() ? "Y" : "N";
					foidUnique = ibeConfig.getUniqueness();
				} else {
					continue;
				}
			}

			sb.append("arrPsntTypeInfo[");
			sb.append(i);
			sb.append("] = new Array();");
			sb.append("arrPsntTypeInfo[");
			sb.append(i);
			sb.append("][0] = '");
			sb.append(paxCatCode);
			sb.append("';");
			sb.append("arrPsntTypeInfo[");
			sb.append(i);
			sb.append("][1] = '");
			sb.append(paxCatDesc);
			sb.append("';");
			sb.append("arrPsntTypeInfo[");
			sb.append(i);
			sb.append("][2] = '");
			sb.append(defaultCurr);
			sb.append("';");
			sb.append("arrPsntTypeInfo[");
			sb.append(i);
			sb.append("][3] = '");
			sb.append(defaultSortOrd);
			sb.append("';");
			sb.append("arrPsntTypeInfo[");
			sb.append(i);
			sb.append("][4] = '");
			sb.append(foidNo);
			sb.append("';");
			sb.append("arrPsntTypeInfo[");
			sb.append(i);
			sb.append("][5] = '");
			sb.append(foidCode);
			sb.append("';");
			sb.append("arrPsntTypeInfo[");
			sb.append(i);
			sb.append("][6] = '");
			sb.append(foidDesc);
			sb.append("';");
			sb.append("arrPsntTypeInfo[");
			sb.append(i);
			sb.append("][7] = '");
			sb.append(foidCode);
			sb.append("';");
			sb.append("arrPsntTypeInfo[");
			sb.append(i);
			sb.append("][8] = '");
			sb.append(paxTypeCode);
			sb.append("';");
			sb.append("arrPsntTypeInfo[");
			sb.append(i);
			sb.append("][9] = '");
			sb.append(foidrequired);
			sb.append("';");
			sb.append("arrPsntTypeInfo[");
			sb.append(i);
			sb.append("][10] = '");
			sb.append(paxStatus);
			sb.append("';");
			sb.append("arrPsntTypeInfo[");
			sb.append(i);
			sb.append("][11] = '");
			sb.append(foidUnique);
			sb.append("';");
			i++;
		}

		return sb.toString();
	}

	public final static String createFareCategoryList() throws ModuleException {
		String html = createFareCategoryList(null);
		return html;
	}

	public final static String createActiveFareCategoryList() throws ModuleException {
		String html = createFareCategoryList("ACT");
		return html;
	}

	private final static String createFareCategoryList(String status) throws ModuleException {

		StringBuilder sb = new StringBuilder();
		Collection<FareCategoryTO> colFareCategoryTO = globalConfig.getFareCategories().values();
		Iterator<FareCategoryTO> it = colFareCategoryTO.iterator();
		FareCategoryTO fareCategoryTO;

		while (it.hasNext()) {
			fareCategoryTO = it.next();

			if (fareCategoryTO != null && (status == null || status.equals(fareCategoryTO.getStatus().name()))) {
				sb.append("<option value='");
				sb.append(fareCategoryTO.getFareCategoryCode());
				sb.append("'>");
				sb.append(fareCategoryTO.getFareCategoryDesc());
				sb.append("</option>");
			}
		}

		return sb.toString();
	}

	public final static List<String[]> createFareCategoryArrayList() throws ModuleException {

		String status = "ACT";
		List<String[]> catList = new ArrayList<String[]>();
		String[] catArray = null;

		Collection<FareCategoryTO> colFareCategoryTO = globalConfig.getFareCategories().values();
		Iterator<FareCategoryTO> it = colFareCategoryTO.iterator();
		FareCategoryTO fareCategoryTO;

		while (it.hasNext()) {
			fareCategoryTO = it.next();

			if (fareCategoryTO != null && (status == null || status.equals(fareCategoryTO.getStatus().name()))) {
				catArray = new String[2];
				catArray[0] = fareCategoryTO.getFareCategoryCode();
				catArray[1] = fareCategoryTO.getFareCategoryDesc();
				catList.add(catArray);
			}
		}

		return catList;
	}

	public final static String createFlexiCodeHTMLList() throws ModuleException {
		StringBuilder sb = new StringBuilder();

		List<String[]> flexiCodes = createFlexiCodeArrayList();

		for (String[] flexiCodeElement : flexiCodes) {
			sb.append("<option value='");
			sb.append(flexiCodeElement[0]);
			sb.append("'>");
			sb.append(flexiCodeElement[1]);
			sb.append("</option>");
		}

		return sb.toString();
	}

	public final static String createFlexiCodeList() throws ModuleException {
		StringBuffer sb = new StringBuffer();
		List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap(FLEXI_CODE_LIST);
		Iterator<String[]> iter = l.iterator();

		sb.append("var arrFlexicodes = new Array();");
		int i = 1;
		sb.append("arrFlexicodes[" + 0 + "] = new Array();");
		sb.append("arrFlexicodes[" + 0 + "][0] = '-1'; arrFlexicodes[" + 0 + "][1] = '';");
		while (iter.hasNext()) {
			String s[] = iter.next();
			sb.append("arrFlexicodes[" + i + "] = new Array();");
			sb.append("arrFlexicodes[" + i + "][0] = '" + s[0] + "'; arrFlexicodes[" + i + "][1] = '" + s[1] + "';");
			i++;
		}
		return sb.toString();
	}

	public final static List<String[]> createFlexiCodeArrayList() throws ModuleException {
		// StringBuffer sb = new StringBuffer();
		List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap(FLEXI_CODE_LIST);
		return l;
	}

	public final static String createFlexiJourneyList() throws ModuleException {
		StringBuffer sb = new StringBuffer();
		List<String[]> l = LookupUtils.LookupDAO().getFourColumnMap(FLEXI_JOURNEY_LIST);
		Iterator<String[]> iter = l.iterator();

		sb.append("var arrFlexiJourney = new Array();");
		sb.append("arrFlexiJourney[" + 0 + "] = new Array();");
		sb.append("arrFlexiJourney[" + 0 + "][0] = '-1'; arrFlexiJourney[" + 0 + "][1] = '';");
		sb.append("arrFlexiJourney[" + 0 + "][2] = ''; arrFlexiJourney[" + 0 + "][3] = '';");
		int i = 1;
		while (iter.hasNext()) {
			String s[] = iter.next();
			sb.append("arrFlexiJourney[" + i + "] = new Array();");
			sb.append("arrFlexiJourney[" + i + "][0] = '" + s[0] + "'; arrFlexiJourney[" + i + "][1] = '" + s[1] + "';");
			sb.append("arrFlexiJourney[" + i + "][2] = '" + s[2] + "'; arrFlexiJourney[" + i + "][3] = '" + s[3] + "';");
			i++;
		}
		return sb.toString();

	}

	public final static List<String[]> createFlexiJourneyArrayList() throws ModuleException {
		// StringBuffer sb = new StringBuffer();
		List<String[]> l = LookupUtils.LookupDAO().getFourColumnMap(FLEXI_JOURNEY_LIST);
		return l;
	}

	public static final String createFareCategories(String arrName) throws ModuleException {
		String html = createFareCategories(arrName, null);
		return html;
	}

	public static final String createActiveFareCategories(String arrName) throws ModuleException {
		String html = createFareCategories(arrName, "ACT");
		return html;
	}

	/**
	 * Get FareCategoryList
	 * 
	 * @param status
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<FareCategoryTO> getFareCategories(String status) throws ModuleException {

		Collection<FareCategoryTO> fareCategoryTOs = new ArrayList<FareCategoryTO>();
		Collection<FareCategoryTO> colFareCategoryTO = globalConfig.getFareCategories().values();
		Iterator<FareCategoryTO> it = colFareCategoryTO.iterator();
		FareCategoryTO fareCategoryTO;

		while (it.hasNext()) {
			fareCategoryTO = it.next();

			if (fareCategoryTO != null && (status == null || status.equals(fareCategoryTO.getStatus().name()))) {
				fareCategoryTOs.add(fareCategoryTO);
			}
		}

		return fareCategoryTOs;
	}

	private static final String createFareCategories(String arrName, String status) throws ModuleException {
		StringBuilder sb = new StringBuilder();
		int i = 0;

		Collection<FareCategoryTO> colFareCategoryTO = globalConfig.getFareCategories().values();
		Iterator<FareCategoryTO> it = colFareCategoryTO.iterator();
		FareCategoryTO fareCategoryTO;

		while (it.hasNext()) {
			fareCategoryTO = it.next();

			if (fareCategoryTO != null && (status == null || status.equals(fareCategoryTO.getStatus().name()))) {
				sb.append(arrName + "[");
				sb.append(i);
				sb.append("]=new Array('");

				sb.append(fareCategoryTO.getFareCategoryCode());
				sb.append("','");

				sb.append(fareCategoryTO.getFareCategoryDesc());
				sb.append("','");

				sb.append(fareCategoryTO.getStatus().toString());
				sb.append("','");

				sb.append(fareCategoryTO.isShowComments() ? "Y" : "N");
				sb.append("','");

				sb.append(fareCategoryTO.isDefault() ? "Y" : "N");
				sb.append("');");

				i++;
			}
		}
		return sb.toString();
	}

	public final static List<String> createInterlinedCarrierCodes() throws ModuleException {

		List<String> carrierCodeList = new ArrayList<String>();
		Collection<String> carrierCodeCol = CommonsServices.getGlobalConfig().getActiveInterlinedCarriersMap().keySet();

		for (String carrierCode : carrierCodeCol) {
			carrierCodeList.add(carrierCode);
		}
		Collections.sort(carrierCodeList);

		return carrierCodeList;
	}

	/**
	 * Gets all the Carrier Codes
	 * 
	 * @return String
	 * @throws ModuleException
	 * @author Dhanusha
	 * @since 25 Feb, 2008
	 */
	public final static String getCarrierCodes() throws ModuleException {
		List<String> lstCarrierCode = new ArrayList<String>();
		List<String[]> lstCarrierCodeDesc = new ArrayList<String[]>();
		String defaultCarrierCode = AppSysParamsUtil.getDefaultCarrierCode();
		Collection<String> colCarrierCode = CommonsServices.getGlobalConfig().getActiveInterlinedCarriersMap().keySet();

		lstCarrierCode.add(defaultCarrierCode);

		for (String carrierCode : colCarrierCode) {
			lstCarrierCode.add(carrierCode);
		}

		Collections.sort(lstCarrierCode);

		for (String carrierCode : lstCarrierCode) {
			lstCarrierCodeDesc.add(new String[] { carrierCode, carrierCode });
		}

		return create_Code_Code_List(lstCarrierCodeDesc);
	}

	/**
	 * Create charge applies to display list without option tags
	 * 
	 * @return
	 */
	public static String createChargeAppliesToListNoTags() {
		Map<Integer, ChargeApplicabilityTO> chargeApplicabilitiesMap = globalConfig.getChargeApplicabilities();
		StringBuilder msg = new StringBuilder();
		ChargeApplicabilityTO chargeApplicabilityTO;
		//
		// if (!AppSysParamsUtil.isChildOnlyBookingsAllowed()) {
		// // Applicability Child incase that Airline doesnt have child fares.
		// chargeApplicabilitiesMap.remove(ChargeApplicabilityTO.APPLICABLE_TO_CHILD_ONLY);
		// }

		int i = 0;
		for (Integer key : chargeApplicabilitiesMap.keySet()) {
			chargeApplicabilityTO = chargeApplicabilitiesMap.get(key);

			if (i > 0) {
				msg.append("^");
			}
			msg.append(key);
			msg.append("|");
			msg.append(chargeApplicabilityTO.getAppliesToDesc());
			i++;
		}

		return msg.toString();
	}

	/**
	 * Create charge applies to display list
	 * 
	 * @return
	 */
	public static String createChargeAppliesToDisplayList() {
		Map<Integer, ChargeApplicabilityTO> chargeApplicabilitiesMap = globalConfig.getChargeApplicabilities();
		StringBuilder msg = new StringBuilder();
		ChargeApplicabilityTO chargeApplicabilityTO;

		// if (!AppSysParamsUtil.isChildOnlyBookingsAllowed()) {
		// // Applicability Child incase that Airline doesnt have child fares.
		// chargeApplicabilitiesMap.remove(ChargeApplicabilityTO.APPLICABLE_TO_CHILD_ONLY);
		// }

		for (Integer key : chargeApplicabilitiesMap.keySet()) {
			chargeApplicabilityTO = chargeApplicabilitiesMap.get(key);

			msg.append("<option value='");
			msg.append(key);
			msg.append("'>");
			msg.append(chargeApplicabilityTO.getAppliesToDesc());
			msg.append("</option>");
		}

		return msg.toString();
	}

	/**
	 * Create charge applies to display list
	 * 
	 * @return
	 */
	public static String createHandlingFeeAppliesToDisplayList() {
		Map<Integer, ChargeApplicabilityTO> chargeApplicabilitiesMap = globalConfig.getChargeApplicabilities();
		StringBuilder msg = new StringBuilder();
		ChargeApplicabilityTO chargeApplicabilityTO;

		for (Integer key : chargeApplicabilitiesMap.keySet()) {
			chargeApplicabilityTO = chargeApplicabilitiesMap.get(key);

			if (ChargeApplicabilityTO.APPLICABLE_TO_RESERVATION == key.intValue()
					|| ChargeApplicabilityTO.APPLICABLE_TO_ADULT_CHILD == key.intValue()) {
				msg.append("<option value='");
				msg.append(key);
				msg.append("'>");
				msg.append(chargeApplicabilityTO.getAppliesToDesc());
				msg.append("</option>");
			}
		}

		return msg.toString();
	}

	/**
	 * Create charge FareBasis to display list
	 * 
	 * @return
	 */
	public static String createFareBasisToDisplayList() {

		StringBuilder msg = new StringBuilder();

		for (ChargeBasisEnum chargeBasis : ChargeBasisEnum.values()) {
			msg.append("<option value='");
			msg.append(chargeBasis.getCode());
			msg.append("' title='");
			msg.append(chargeBasis.getName());
			msg.append("'>");
			msg.append(chargeBasis.getCode());
			msg.append("</option>");
		}

		return msg.toString();
	}

	/**
	 * Create coupon status list
	 * 
	 * @return
	 */
	public static Set<String[]> getAllCouponStatusList() {

		Set<String[]> couponStatusList = new HashSet<String[]>();

		for (EticketStatus status : EticketStatus.values()) {
			String[] couponStatus = new String[2];
			couponStatus[0] = status.code();
			couponStatus[1] = status.code();
			couponStatusList.add(couponStatus);
		}

		return couponStatusList;
	}

	/**
	 * Create pax status list
	 * 
	 * @return
	 */
	public static Set<String[]> getAllPaxStatusList() {

		Set<String[]> paxStatusList = new HashSet<String[]>();

		paxStatusList.add(new String[] { PaxFareSegmentTypes.CONFIRMED, PaxFareSegmentTypes.CONFIRMED });
		paxStatusList.add(new String[] { PaxFareSegmentTypes.FLOWN, PaxFareSegmentTypes.FLOWN });
		paxStatusList.add(new String[] { PaxFareSegmentTypes.NO_REC, PaxFareSegmentTypes.NO_REC });
		paxStatusList.add(new String[] { PaxFareSegmentTypes.NO_SHORE, PaxFareSegmentTypes.NO_SHORE });
		paxStatusList.add(new String[] { PaxFareSegmentTypes.GO_SHORE, PaxFareSegmentTypes.GO_SHORE });
		paxStatusList.add(new String[] { PaxFareSegmentTypes.OFF_LOADED, PaxFareSegmentTypes.OFF_LOADED });

		return paxStatusList;
	}

	public static List<String[]> getPaxStatusList() {

		List<String[]> paxStatusList = new ArrayList<String[]>();

		paxStatusList.add(new String[] { PaxFareSegmentTypes.CONFIRMED, "Confirmed" });
		paxStatusList.add(new String[] { PaxFareSegmentTypes.FLOWN, "Flown" });
		paxStatusList.add(new String[] { PaxFareSegmentTypes.NO_REC, "No Rec" });
		paxStatusList.add(new String[] { PaxFareSegmentTypes.NO_SHORE, "No Show" });
		paxStatusList.add(new String[] { PaxFareSegmentTypes.GO_SHORE, "Go Show" });
		paxStatusList.add(new String[] { PaxFareSegmentTypes.OFF_LOADED, "Offloaded" });

		return paxStatusList;
	}

	/**
	 * Grts a telephone nos according to one param
	 * 
	 * @param countryCode
	 *            country code
	 * @param teleCode
	 *            telecode
	 * @return
	 */
	public static Collection<String[]> getTelephoneCodes(String countryCode, String teleCode) {
		int colCount = 5;
		String selectSql = "";

		String[] params = new String[1];
		if (countryCode != null) {
			params[0] = countryCode;
			selectSql = COUNTRY_TELE_LIST;
		}

		if (teleCode != null) {
			params[0] = teleCode;
			selectSql = TELEPHONE_LIST;
		}
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		return colData;
	}

	/**
	 * Gets the area codes according to country tele code
	 * 
	 * @param countryCode
	 *            country phone code
	 * @return
	 */
	public static Collection<String[]> getAreaCodes(String countryCode) {
		int colCount = 5;
		String selectSql = AREA_CODE_LIST;
		String[] params = new String[1];
		if (countryCode != null) {
			params[0] = countryCode;
		}
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		return colData;
	}

	/**
	 * Gets the area codes according to country tele code
	 * 
	 * @param countryPhone
	 *            country phone code
	 * @return
	 */
	public static Collection<String[]> getCountryTeles() {
		int colCount = 8;
		String selectSql = COUNTRY_PHONE_LIST;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}

	public static String createTelephoneArray() throws ModuleException {
		int colCount = 8;
		String selectSql = COUNTRY_PHONE_LIST;
		String[] params = new String[] {};
		StringBuffer sb = new StringBuffer();
		String[] strData = null;
		String strCountryCode = "";
		String strMinlen = "";
		String strMaxlen = "";
		String strPrevCode = "";
		String strCountry = "";
		String strPrevCountry = "";
		int i = 0;
		int j = 0;
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		if (colData != null) {
			Iterator<String[]> iter = colData.iterator();
			while (iter.hasNext()) {
				strData = iter.next();
				strCountryCode = strData[0];
				strCountry = strData[7];
				if (strData[2] != null) {
					strMinlen = strData[2];
				} else {
					strMinlen = "";
				}
				if (strData[3] != null) {
					strMaxlen = strData[3];
				} else {
					strMaxlen = "";
				}

				if (!strCountryCode.equals(strPrevCode)) {
					j = 0;
					sb.append("arrCountryPhone[" + i + "] = new Array('" + strCountryCode + "','" + strData[1] + "','"
							+ strMinlen + "','" + strMaxlen + "','" + strCountry + "');");
					if (strData[1].equals("Y")) {
						sb.append("arrAreaPhone[" + strCountryCode + "] = new Array();");
						sb.append("arrAreaPhone[" + strCountryCode + "][" + j + "] = new Array('" + strData[4] + "','"
								+ strData[5] + "','" + strData[6] + "');");
					}
					i++;
					strPrevCode = strCountryCode;
					strPrevCountry = strCountry;
				} else {

					if (!strPrevCountry.equals(strCountry)) {
						sb.append("arrCountryPhone[" + i + "] = new Array('" + strCountryCode + "','" + strData[1] + "','"
								+ strMinlen + "','" + strMaxlen + "','" + strCountry + "');");
						i++;
						strPrevCountry = strCountry;
					} else {
						j++;
						sb.append("arrAreaPhone[" + strCountryCode + "][" + j + "] = new Array('" + strData[4] + "','"
								+ strData[5] + "','" + strData[6] + "');");
					}
				}

			}
		}

		return sb.toString();
	}

	public static String[] createTelephoneString() throws ModuleException {
		String[] phoneData = new String[2];
		int colCount = 8;
		String selectSql = COUNTRY_PHONE_LIST;
		String[] params = new String[] {};
		StringBuffer sbCountry = new StringBuffer();
		StringBuffer sbArea = new StringBuffer();
		String[] strData = null;
		String strCountryCode = "";
		String strMinlen = "";
		String strMaxlen = "";
		String strPrevCode = "";
		String strCountry = "";
		String strPrevCountry = "";
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		if (colData != null) {
			Iterator<String[]> iter = colData.iterator();
			while (iter.hasNext()) {
				strData = iter.next();
				strCountryCode = strData[0];
				strCountry = strData[7];
				if (strData[2] != null) {
					strMinlen = strData[2];
				} else {
					strMinlen = "";
				}
				if (strData[3] != null) {
					strMaxlen = strData[3];
				} else {
					strMaxlen = "";
				}

				if (!strCountryCode.equals(strPrevCode)) {
					sbCountry.append(strCountryCode);
					sbCountry.append(Constants.COMMA_SEPARATOR);
					sbCountry.append(strData[1]);
					sbCountry.append(Constants.COMMA_SEPARATOR);
					sbCountry.append(strMinlen);
					sbCountry.append(Constants.COMMA_SEPARATOR);
					sbCountry.append(strMaxlen);
					sbCountry.append(Constants.COMMA_SEPARATOR);
					sbCountry.append(strCountry);
					sbCountry.append(Constants.CARET_SEPERATOR);

					if (strData[1].equals("Y")) {
						sbArea.append(strCountryCode);
						sbArea.append(Constants.COMMA_SEPARATOR);
						sbArea.append(strData[4]);
						sbArea.append(Constants.COMMA_SEPARATOR);
						sbArea.append(strData[5]);
						sbArea.append(Constants.COMMA_SEPARATOR);
						sbArea.append(strData[6]);
						sbArea.append(Constants.CARET_SEPERATOR);
					}
					strPrevCode = strCountryCode;
					strPrevCountry = strCountry;
				} else {

					if (!strPrevCountry.equals(strCountry)) {
						sbCountry.append(strCountryCode);
						sbCountry.append(Constants.COMMA_SEPARATOR);
						sbCountry.append(strData[1]);
						sbCountry.append(Constants.COMMA_SEPARATOR);
						sbCountry.append(strMinlen);
						sbCountry.append(Constants.COMMA_SEPARATOR);
						sbCountry.append(strMaxlen);
						sbCountry.append(Constants.COMMA_SEPARATOR);
						sbCountry.append(strCountry);
						sbCountry.append(Constants.CARET_SEPERATOR);
						strPrevCountry = strCountry;
					} else {
						sbArea.append(strCountryCode);
						sbArea.append(Constants.COMMA_SEPARATOR);
						sbArea.append(strData[4]);
						sbArea.append(Constants.COMMA_SEPARATOR);
						sbArea.append(strData[5]);
						sbArea.append(Constants.COMMA_SEPARATOR);
						sbArea.append(strData[6]);
						sbArea.append(Constants.CARET_SEPERATOR);
					}
				}

			}
		}

		phoneData[0] = sbCountry.toString();
		phoneData[1] = sbArea.toString();

		return phoneData;
	}

	public static String createStationCountryArray() throws ModuleException {

		StringBuffer sb = new StringBuffer();
		String paramString = StationCodeCountryCode;
		List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap(paramString);

		Iterator<String[]> iter = l.iterator();
		int i = 0;
		while (iter.hasNext()) {
			String s[] = iter.next();
			sb.append("arrStationCountry[" + i + "] = new Array('" + s[0] + "','" + s[1] + "');");
			i++;
		}
		return sb.toString();
	}

	/**
	 * Method retrive the foid id's of given foidCode for the givent segments . one of the foidId's in foidIdList
	 * 
	 * @param foidCode
	 *            eg : PSPT,NIC
	 * @param dept
	 *            depature segment id.
	 * @param arr
	 *            arrival segment id.
	 * @param foidIdList
	 *            list of foidId's for duplicate check.
	 * @param actio
	 *            TODO
	 * @return foidIds already exist in the givent segment/segments which match to foidIdList.
	 * @throws ModuleException
	 */
	public final static Collection<Integer[]> getDuplicateFoidIdList(String foidCode, Integer dept, Integer arr,
			Integer foidIdList[], String pnr) throws ModuleException {

		StringBuffer sb;
		int count = 0;
		// String duplicateFoidId = "";
		List<Integer[]> duplicateList = new ArrayList<Integer[]>();
		for (Integer element : foidIdList) {
			sb = new StringBuffer();
			sb.append("SELECT pa.PASSPORT_NUMBER FROM t_pnr_passenger a, T_PNR_PAX_ADDITIONAL_INFO pa, t_reservation r WHERE a.pnr = r.pnr AND a.PNR_PAX_ID=pa.PNR_PAX_ID AND r.status <> 'CNX' AND a.pnr in");
			sb.append(" (SELECT ps.pnr FROM t_pnr_segment ps WHERE ps.status <> 'CNX' AND ps.flt_seg_id in");
			sb.append(" (SELECT fs.flt_seg_id FROM t_flight_segment fs WHERE fs.FLT_SEG_ID in ( ");
			sb.append(dept);
			if (arr != null && !arr.equals("")) {
				sb.append("," + arr);
			}
			sb.append("))) ");
			if (pnr != null) {
				sb.append("  and r.pnr <> '" + pnr + "'");
			}

			Collection<String[]> foidList = LookupUtils.LookupDAO().getList1(sb.toString(), 1);
			for (String[] foid : foidList) {
				String[] passport = (foid[0] != null) ? foid[0].split(",") : null;
				if (passport != null && passport.length > 0) {
					if (passport[0].equalsIgnoreCase(element + "")) {
						count++;
					}
				}
			}

			if (count > 0) {
				duplicateList.add(foidIdList);
				break;
			}
		}
		return duplicateList;
	}

	public final static Map<String, String> create_AirportCode_Map(String langCode) throws ModuleException {
		String[] arg = new String[1];
		int[] argType = new int[1];
		arg[0] = langCode;
		argType[0] = java.sql.Types.VARCHAR;
		return create_AirportCode_Map(airportForDisplay, arg, argType);
	}

	public final static Map<String, String> create_AirportName_Map(String langCode) throws ModuleException {
		String[] arg = new String[1];
		int[] argType = new int[1];
		arg[0] = langCode;
		argType[0] = java.sql.Types.VARCHAR;
		return create_AirportCode_Map(airportNameForDisplay, arg, argType);
	}

	public final static String createAirportsListForDisplay(String langCode, String arrName) throws ModuleException {
		String[] arg = new String[1];
		int[] argType = new int[1];
		arg[0] = langCode;
		argType[0] = java.sql.Types.VARCHAR;
		return create_List_For_Display(airportForDisplay, arrName, arg, argType);
	}

	public final static String createCurrencyListForDisplay(String langCode, String arrName) throws ModuleException {
		String[] arg = new String[1];
		int[] argType = new int[1];
		arg[0] = langCode;
		argType[0] = java.sql.Types.VARCHAR;
		return create_List_For_Display(currencyForDisplay, arrName, arg, argType);
	}

	public static String createReturnValidityPeriods(String arrName, int channelCode) throws ModuleException {
		Collection<ReturnValidityPeriodTO> colData = globalConfig.getReturnValidityPeriods().values();
		StringBuilder sb = new StringBuilder();

		if (colData != null) {
			int i = 0;

			for (ReturnValidityPeriodTO retVal : colData) {
				if (CommonsConstants.STATUS_ACTIVE.equals(retVal.getStatus()) && retVal.getSalesChannelCode() == channelCode) {
					sb.append(arrName);
					sb.append("[");
					sb.append(i);
					sb.append("]=['");
					sb.append(retVal.getId());
					sb.append("','");
					sb.append(retVal.getDescription());
					sb.append("'];");

					i++;
				}
			}
		}

		return sb.toString();
	}

	public final static Collection<String[]> getCurrencyCodeList() throws ModuleException {
		String selectSql = CURRENCY_CODE;
		Collection<String[]> col = LookupUtils.LookupDAO().getTwoColumnMap(selectSql);
		return col;
	}

	public final static String createUserThemeList(String selectedCode) throws ModuleException {
		return create_Code_Description_List(USER_THEME_LIST, selectedCode);
	}

	public static final Map<String, Collection<String[]>> createActualPaymentMethodList() throws ModuleException {
		int colCount = 3;
		String selectSql = ACTUAL_PAYMENT_METHOD_LIST;
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, new String[] {}, colCount);
		Map<String, Collection<String[]>> map = new HashMap<String, Collection<String[]>>();
		for (String[] arr : colData) {
			String key = arr[0];
			if (!map.containsKey(key)) {
				map.put(key, new ArrayList<String[]>());
			}
			map.get(key).add(new String[] { arr[1], arr[2] });

		}
		return map;
	}

	public static final String createBusCarrierCode() throws ModuleException {
		return WPModuleUtils.getFlightServiceBD().getDefaultBusCarrierCode();
	}

	public static final String getDefaultBusCarrierCode(String carrierCode) throws ModuleException {
		return WPModuleUtils.getFlightServiceBD().getDefaultBusCarrierCode(carrierCode);
	}

	/**
	 * generic select list generator with select
	 * 
	 * @param paramString
	 * @return String
	 */
	private final static String create_Code_Description_List(String paramString, String selectedCode) throws ModuleException {
		StringBuilder sb = new StringBuilder();
		List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap(paramString);
		Iterator<String[]> iter = l.iterator();

		while (iter.hasNext()) {
			String s[] = iter.next();
			if (selectedCode.equalsIgnoreCase(s[0])) {
				sb.append("<option value='" + s[0] + "' selected='selected'>" + s[1] + "</option>");
			} else {
				sb.append("<option value='" + s[0] + "'>" + s[1] + "</option>");
			}
		}

		return sb.toString();
	}

	/**
	 * Generate Booking Class Types list
	 * **/
	public final static String createBookingClassTypeList() throws ModuleException {
		return create_Code_Description_List(BOOKING_CLASS_TYPE_LIST);
	}

	/**
	 * Generate Booking Class Types list
	 * **/
	public static String getAirportCodeNameCountryCodeList() throws ModuleException {
		StringBuilder sb = new StringBuilder();
		Collection<String[]> colData = getCollection(AIRPORT_CODE_NAME_COUNTRY_LIST, new String[] {}, 3);
		Iterator<String[]> iter = colData.iterator();

		int i = 0;
		while (iter.hasNext()) {
			String s[] = iter.next();
			String code = s[0];
			String desc1 = StringUtils.replace(s[1], "'", SINGLE_QUOTE);
			String desc2 = StringUtils.replace(s[2], "'", SINGLE_QUOTE);

			sb.append("airportArr" + "[" + i + "] = new Array('" + code + "','" + desc1 + "','" + desc2 + "'); ");
			i++;
		}

		return sb.toString();
	}
	
	public static String getCityCodeNameCountryCodeList() throws ModuleException {
		StringBuilder sb = new StringBuilder();
		Collection<String[]> colData = getCollection(CITY_CODE_NAME_COUNTRY_LIST, new String[] {}, 3);
		Iterator<String[]> iter = colData.iterator();

		int i = 0;
		while (iter.hasNext()) {
			String s[] = iter.next();
			String code = s[0];
			String desc1 = StringUtils.replace(s[1], "'", SINGLE_QUOTE);
			String desc2 = StringUtils.replace(s[2], "'", SINGLE_QUOTE);

			sb.append("cityArr" + "[" + i + "] = new Array('" + code + "','" + desc1 + "','" + desc2 + "'); ");
			i++;
		}

		return sb.toString();
	}
	
	public static String getCityCodeNameCountryCodeListJson() throws ModuleException {
		Collection<String[]> colData = getCollection(CITY_CODE_NAME_COUNTRY_LIST, new String[] {}, 3);
		Iterator<String[]> iter = colData.iterator();

		Map<String, Object> jsonMap = new HashMap<String, Object>();
		ArrayList<String[]> cityCountryArrayList = new ArrayList<String[]>(); 
		
		while (iter.hasNext()) {
			String s[] = iter.next();
			String code = s[0];
			String desc2 = StringUtils.replace(s[2], "'", SINGLE_QUOTE);
			String[] cityCountryArray = {code, desc2};
			cityCountryArrayList.add(cityCountryArray);
		}
		
		jsonMap.put("cityCountryCodes", cityCountryArrayList);

		try {
			return JSONUtil.serialize(jsonMap);
		} catch (JSONException e) {
			throw new ModuleException("", e);
		}
	}

	private final static Collection<String[]> getCollection(String paramString, String[] params, int colCount)
			throws ModuleException {
		Collection<String[]> col = LookupUtils.LookupDAO().getList(paramString, params, colCount);
		return col;
	}

	public static String getScheduledReportTypes() throws ModuleException {
		String selectSql = SCHEDULED_REPORT_NAMES;
		Collection<String[]> col = LookupUtils.LookupDAO().getTwoColumnMap(selectSql);
		return create_Code_Description_List(col);
	}

	public static String SelectAdjustmentCarrierList(String defaultCarrier) {
		StringBuffer sb = new StringBuffer();
		if (defaultCarrier == null) {
			defaultCarrier = AppSysParamsUtil.getDefaultCarrierCode();
		}
		if (AppSysParamsUtil.getAdjustmentCarrierList() != null) {
			for (String carrier : AppSysParamsUtil.getAdjustmentCarrierList()) {
				if (carrier.equals(defaultCarrier)) {
					sb.append(String.format("<option value='%s' SELECTED >%s</option>", carrier, carrier));
				} else {
					sb.append(String.format("<option value='%s'>%s</option>", carrier, carrier));
				}

			}
		}
		return sb.toString();
	}

	public final static String createDashboardMsgTypes() throws ModuleException {
		return createDashboardMsgTypeList(DASHBOARD_MSG_TYPES);
	}

	private final static String createDashboardMsgTypeList(String paramString) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap(paramString);

		Iterator<String[]> iter = l.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			sb.append("<option value='" + s[0] + "'>" + s[1] + "</option>");
		}
		return sb.toString();
	}

	public static String createFareModCancTypes() {
		StringBuilder sb = new StringBuilder();
		String fareValue = "V";
		String farePrecentage = "PF";
		String fareAndSurchangePrecentage = "PFS";
		// String percentageOfTotalFare = "PTF";

		sb.append("<option value='" + fareValue + "'>" + fareValue + "</option>");

		sb.append("<option value='" + farePrecentage + "'>" + farePrecentage + "</option>");
		sb.append("<option value='" + fareAndSurchangePrecentage + "'>" + fareAndSurchangePrecentage + "</option>");
		// sb.append("<option value='" + percentageOfTotalFare + "'>" + percentageOfTotalFare + "</option>");

		return sb.toString();
	}

	// TODO - How to get the list of interlined carrier codes? It seems there are so many ways in the
	// code to do this. Please consider unifying this. Using AIRLINE_61 App param for the time being
	public static String createInterlineCarrierSelectListOptions(Collection<String> accessibleCarrierCodes) {
		Set<String> interlinedCarriers = new HashSet<String>();

		// Multiple carriers will be sent only if lcc is enabled, otherwise we can skip this step
		if (accessibleCarrierCodes != null) {
			String[] carrierList = AppSysParamsUtil.getAdjustmentCarrierList();
			if (carrierList != null && carrierList.length > 0) {
				for (String carrierCode : carrierList) {
					if (!AppSysParamsUtil.getDefaultCarrierCode().equals(carrierCode)
							&& accessibleCarrierCodes.contains(carrierCode)) {
						interlinedCarriers.add(carrierCode);
					}
				}
			}
		}
		StringBuilder sb = new StringBuilder();
		sb.append("<option value='" + AppSysParamsUtil.getDefaultCarrierCode() + "' selected>"
				+ AppSysParamsUtil.getDefaultCarrierCode() + "</option>");
		for (String carrierCode : interlinedCarriers) {
			sb.append("<option value='" + carrierCode + "'>" + carrierCode + "</option>");
		}
		return sb.toString();
	}

	public static List<String[]> createFareModCancTypesList() {

		// StringBuilder sb = new StringBuilder();
		String fareValue = "V";
		String farePrecentage = "PF";
		String fareAndSurchangePrecentage = "PFS";

		List<String[]> modCanTypesList = new ArrayList<String[]>();
		String[] modCanTypeArray = null;

		modCanTypeArray = new String[2];
		modCanTypeArray[0] = fareValue;
		modCanTypeArray[1] = fareValue;
		modCanTypesList.add(modCanTypeArray);

		modCanTypeArray = new String[2];
		modCanTypeArray[0] = farePrecentage;
		modCanTypeArray[1] = farePrecentage;
		modCanTypesList.add(modCanTypeArray);

		modCanTypeArray = new String[2];
		modCanTypeArray[0] = fareAndSurchangePrecentage;
		modCanTypeArray[1] = fareAndSurchangePrecentage;
		modCanTypesList.add(modCanTypeArray);

		return modCanTypesList;
	}

	public static List<String[]> createPaxChargeTypesList() {

		String fareValue = "V";
		String farePrecentage = "PF";
		String fareAndSurchangePrecentage = "PFS";
		String totalFarePrecentage = "PTF";

		List<String[]> paxChargeTypesList = new ArrayList<String[]>();
		String[] paxChargeTypeArray = null;

		paxChargeTypeArray = new String[2];
		paxChargeTypeArray[0] = fareValue;
		paxChargeTypeArray[1] = fareValue;
		paxChargeTypesList.add(paxChargeTypeArray);

		paxChargeTypeArray = new String[2];
		paxChargeTypeArray[0] = farePrecentage;
		paxChargeTypeArray[1] = farePrecentage;
		paxChargeTypesList.add(paxChargeTypeArray);

		paxChargeTypeArray = new String[2];
		paxChargeTypeArray[0] = fareAndSurchangePrecentage;
		paxChargeTypeArray[1] = fareAndSurchangePrecentage;
		paxChargeTypesList.add(paxChargeTypeArray);

		paxChargeTypeArray = new String[2];
		paxChargeTypeArray[0] = totalFarePrecentage;
		paxChargeTypeArray[1] = totalFarePrecentage;
		paxChargeTypesList.add(paxChargeTypeArray);

		return paxChargeTypesList;
	}

	public final static List<Map<String, List<String[]>>> createAgentStationMDList() throws ModuleException {

		List<Map<String, List<String[]>>> stationAgentDetailList = new ArrayList<Map<String, List<String[]>>>();

		String[] agentArray = null;

		List<Map<String, String>> lstAgents = SelectListGenerator.createStationCodes();
		Iterator<Map<String, String>> ite = lstAgents.iterator();

		while (ite.hasNext()) {
			Map<String, List<String[]>> stationAgentmap = new HashMap<String, List<String[]>>();

			Map<String, String> keyValues = ite.next();
			Set<String> keys = keyValues.keySet();
			Iterator<String> keyIterator = keys.iterator();
			while (keyIterator.hasNext()) {
				String agentTpes[] = new String[1];
				String agentType = keyValues.get(keyIterator.next());
				agentTpes[0] = agentType;
				Collection<String[]> col = SelectListGenerator.createAllAgentsByStation(agentTpes);
				if (col != null && col.size() > 0) {
					Iterator<String[]> iteAgents = col.iterator();
					List<String[]> tmpAgentsList = new ArrayList<String[]>();
					while (iteAgents.hasNext()) {
						String str[] = iteAgents.next();
						agentArray = new String[2];
						agentArray[0] = str[1];
						agentArray[1] = "- " + str[0];
						tmpAgentsList.add(agentArray);
					}

					if (stationAgentmap.containsKey(agentType)) {
						stationAgentmap.get(agentType).addAll(tmpAgentsList);
					} else {
						stationAgentmap.put(agentType, tmpAgentsList);
					}
				}
				stationAgentDetailList.add(stationAgentmap);
			}
		}

		return stationAgentDetailList;
	}

	public static String createInclusionExclusionList() {
		StringBuilder sb = new StringBuilder();
		sb.append("<option value='' selected='selected'>" + Role.OWN_ROLE + "</option>");
		sb.append("<option value='" + Role.INCLUDE_RLOE + "'>" + Role.INCLUDE_RLOE + "</option>");
		sb.append("<option value='" + Role.EXCLUDE_RLOE + "'>" + Role.EXCLUDE_RLOE + "</option>");
		return sb.toString();
	}

	public static String createInterlinedAirlineCodesList() throws ModuleException {
		List<String> interlinedAirlineCodes = CommonsServices.getGlobalConfig().getInterlinedAirlineCodesList();
		StringBuilder sb = new StringBuilder();

		String defaultAirlineCode = AppSysParamsUtil.getDefaultAirlineIdentifierCode();
		if (interlinedAirlineCodes != null) {
			for (String airline : interlinedAirlineCodes) {
				if (defaultAirlineCode.equals(airline)) {
					sb.append("<option value='" + airline + "' selected='selected'>" + airline + "</option>");
				} else {
					sb.append("<option value='" + airline + "'>" + airline + "</option>");
				}
			}
		}

		return sb.toString();
	}

	public final static String createBaggageTemplateList() throws ModuleException {
		return create_Code_Description_List(baggageTemplateList);
	}

	public final static String createONDBaggageTemplateList() throws ModuleException {
		return create_OND_Baggage_Template_Code_List(OND_BAGGAGE_TEMPLATE_LIST);
	}

	public final static String createONDCodeList() throws ModuleException {
		return create_Code_Description_List(OND_CODE_LIST_FOR_OND_BAGGAGE_TEMPLATES);
	}

	public final static String createBCCategoryList() throws ModuleException {
		StringBuilder sb = new StringBuilder();
		sb.append("<option value='Standard'>Standard</option>");
		sb.append("<option value='Non-Standard'>Non-Standard</option>");
		sb.append("<option value='Fixed'>Fixed</option>");
		return sb.toString();
	}

	public final static String createBaggageSeatFactorConditionApplyFor() {
		StringBuilder sb = new StringBuilder();

		sb.append("<option value='").append(CommonsConstants.SeatFactorConditionApplyFor.APPLY_FOR_FIRST_SEGMENT.getCondition())
				.append("'>").append(CommonsConstants.SeatFactorConditionApplyFor.APPLY_FOR_FIRST_SEGMENT.getDescription())
				.append("</option>");

		sb.append("<option value='").append(CommonsConstants.SeatFactorConditionApplyFor.APPLY_FOR_LAST_SEGMENT.getCondition())
				.append("'>").append(CommonsConstants.SeatFactorConditionApplyFor.APPLY_FOR_LAST_SEGMENT.getDescription())
				.append("</option>");

		sb.append("<option value='")
				.append(CommonsConstants.SeatFactorConditionApplyFor.APPLY_FOR_MOTST_RESTRICTIVE.getCondition()).append("'>")
				.append(CommonsConstants.SeatFactorConditionApplyFor.APPLY_FOR_MOTST_RESTRICTIVE.getDescription())
				.append("</option>");

		return sb.toString();
	}

	/**
	 * 
	 * @param paramString
	 * @return
	 * @throws ModuleException
	 * 
	 */
	public final static String getBookingClassWithCatTypeAlloType() throws ModuleException {

		List<String[]> bCsList = LookupUtils.LookupDAO().getFiveColumnDataProp(BCCODE_TYPE_STND_FIX_FLAG_ALL_TYPE_LIST);

		StringBuilder sb = new StringBuilder();
		if (bCsList != null && bCsList.size() > 0) {

			for (int i = 0; i < bCsList.size(); i++) {
				String[] row = bCsList.get(i);

				sb.append("<option value='" + row[1] + "|" + row[2] + "|" + row[3] + "|" + row[4] + ";" + row[0] + "'>" + row[0]
						+ "</option>");

			}
		}

		return sb.toString();
	}

	public final static String getBookingClassWithCatTypeAlloTypeNoTags() throws ModuleException {

		List<String[]> bCsList = LookupUtils.LookupDAO().getFiveColumnDataProp(BCCODE_TYPE_STND_FIX_FLAG_ALL_TYPE_LIST);
		StringBuilder sb = new StringBuilder();
		if (bCsList != null && bCsList.size() > 0) {

			for (int i = 0; i < bCsList.size(); i++) {
				String[] row = bCsList.get(i);
				if (i > 0) {
					sb.append("^");
				}

				sb.append(row[1] + "|" + row[2] + "|" + row[3] + "|" + row[4] + ";" + row[0]);

			}
		}

		return sb.toString();
	}

	public final static String createSSRCodeListAll() throws ModuleException {
		return create_Code_Description_List(SSR_CODES_ALL);
	}

	public final static String createSSRTemplateListAll() throws ModuleException {
		return create_Code_Description_List(SSR_TEMPLATE_ALL);
	}

	public final static String createSSRTemplateListACT() throws ModuleException {
		return create_Code_Description_List(SSR_TEMPLATE_ACT);
	}

	public final static String createAircraftTypeCodeList() throws ModuleException {
		return create_Code_Description_List(AIRCRAFT_TYPE_CODE_LIST);
	}

	public final static String createSsrSubCategoryList() throws ModuleException {
		return create_Code_Description_List(SSR_SUB_CATEGORY_LIST);
	}

	public final static Collection<String[]> getAllAirports() throws ModuleException {
		String selectSql = activeNonSurfaceAirports;
		Collection<String[]> col = LookupUtils.LookupDAO().getList2(selectSql, 2);
		return col;
	}

	public final static String createAirportMultiSelect() throws ModuleException {
		StringBuilder sb = new StringBuilder();
		Collection<String[]> roles = null;
		String strRoles = "";

		roles = SelectListGenerator.getAllAirports();
		strRoles = "var ls3 = new Listbox('lstAllAirports', 'lstSelectedAirports', 'spn3', 'ls3');";

		if (!roles.isEmpty()) {
			sb.append("var roles = new Array(); ");
			int tempInt = 0;
			Iterator<String[]> rolesIter = roles.iterator();

			while (rolesIter.hasNext()) {
				String str[] = rolesIter.next();
				sb.append("roles[" + tempInt + "] = new Array('" + str[0] + "','" + str[0] + "'); ");
				tempInt++;
			}
			strRoles = strRoles + " ls3.group1 = roles;";
		}

		sb.append(strRoles);
		sb.append("ls3.height = '100px'; ls3.width  = '90px';");
		sb.append("ls3.headingLeft = '&nbsp;&nbsp;All Airports';");
		sb.append("ls3.headingRight = '&nbsp;&nbsp;Selected Airports';");
		sb.append("ls3.drawListBox();");
		return sb.toString();
	}

	public static final Collection<String> getAdminVisibleSSRId() {
		int colCount = 1;
		String selectSql = SSR_BY_ADMIN_VISIBLE;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		Collection<String> resultCollData = new ArrayList<String>();

		Iterator<String[]> stIt = colData.iterator();
		String[] strSts = null;

		while (stIt.hasNext()) {
			strSts = stIt.next();
			if (strSts[0] != null) {
				resultCollData.add(strSts[0]);
			}

		}

		return resultCollData;
	}

	public final static Map<Integer, List<String>> getBookedHalaAirportBySsrId() throws ModuleException {
		String selectSql = BOOKED_HALA_AIRPORT_BY_ID;
		String[] params = new String[0];

		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, 2);

		Iterator<String[]> stIt = colData.iterator();
		String[] strSts = null;

		Map<Integer, List<String>> bookedHalaBySsrIdMap = new HashMap<Integer, List<String>>();

		while (stIt.hasNext()) {
			strSts = stIt.next();

			if (bookedHalaBySsrIdMap != null && bookedHalaBySsrIdMap.size() > 0) {
				List<String> airportList = bookedHalaBySsrIdMap.get(Integer.parseInt(strSts[0]));

				if (airportList != null && !airportList.contains(strSts[1])) {
					bookedHalaBySsrIdMap.get(Integer.parseInt(strSts[0])).add(strSts[1]);
				} else if (airportList == null) {
					bookedHalaBySsrIdMap.put(Integer.parseInt(strSts[0]), new ArrayList<String>());
					bookedHalaBySsrIdMap.get(Integer.parseInt(strSts[0])).add(strSts[1]);
				}
			} else {
				bookedHalaBySsrIdMap.put(Integer.parseInt(strSts[0]), new ArrayList<String>());
				bookedHalaBySsrIdMap.get(Integer.parseInt(strSts[0])).add(strSts[1]);
			}
		}

		return bookedHalaBySsrIdMap;
	}

	public static final String createBookingClassCodesArray() throws ModuleException {
		Collection<String[]> colData = getBookingClassCodeList();
		String strArray = getJSArray("arrBookingClassCode", colData);
		return strArray;
	}

	public static final Collection<String[]> getBookingClassCodeList() throws ModuleException {
		int colCount = 4;
		String selectSql = BOOKING_CLASS_CODES;
		String[] params = new String[] {};
		return LookupUtils.LookupDAO().getList(selectSql, params, colCount);
	}

	public static final Collection<String> getOnlyBookingClassCodeList() throws ModuleException {
		int colCount = 1;
		String selectSql = BOOKING_CLASS_CODES;
		String[] params = new String[] {};

		Collection<String> resultList = new ArrayList<String>();

		Collection<String[]> res = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		for (String[] resRow : res) {
			resultList.add(resRow[0]);
		}

		return resultList;
	}

	public static final Collection<String> getBookingClassCodeList(String applicableChargeGroup) throws ModuleException {
		int colCount = 1;
		String selectSql = BOOKING_CLASS_CODES_FOR_CHARGE_GROUPS;
		String[] params = new String[] { applicableChargeGroup };

		Collection<String> resultList = new ArrayList<String>();

		Collection<String[]> res = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		for (String[] resRow : res) {
			resultList.add(resRow[0]);
		}

		return resultList;
	}

	public static final String createFlightTypeList(boolean internationalSelected) throws ModuleException {
		StringBuilder sb = new StringBuilder();
		List<FlightTypeDTO> lstFltTypes = AppSysParamsUtil.availableFlightTypes();
		int enabledFlightTypes = 0;

		for (FlightTypeDTO fltType : lstFltTypes) {
			if (("Y".equals(fltType.getFlightTypeStatus()))) {
				enabledFlightTypes++;
			}
		}
		for (FlightTypeDTO fltType : lstFltTypes) {
			if ("Y".equals(fltType.getFlightTypeStatus())) {
				sb.append("<option value='" + fltType.getFlightType()
						+ (enabledFlightTypes == 1 ? "' selected='selected'>" : "'>") + fltType.getFlightTypeDesction()
						+ "</option>");
			}
		}
		return sb.toString();
	}

	public final static List<String> getBookedSSRSegment(String ssrId) throws ModuleException {
		List<String> bookedSegList = null;
		String selectSql = BOOKED_SSR_SSR_ID;
		String[] params = new String[] { ssrId };

		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, 1);

		if (colData != null && colData.size() > 0) {
			bookedSegList = new ArrayList<String>();

			for (String[] segArr : colData) {
				bookedSegList.add(segArr[0]);
			}

		}

		return bookedSegList;
	}

	/**
	 * @return
	 */
	public final static String createEticketStatusList() {
		StringBuilder sb = new StringBuilder();
		boolean isVoidEnabled = AppSysParamsUtil.isVoidReservationsEnabled();

		for (EticketStatus eticketStatus : EticketStatus.values()) {
			String strStatus = eticketStatus.code();
			if (eticketStatus == EticketStatus.VOID) {
				if (isVoidEnabled) {
					sb.append("<option value='" + strStatus + "'>" + strStatus + "</option>");
				}
			} else {
				sb.append("<option value='" + strStatus + "'>" + strStatus + "</option>");
			}
		}
		return sb.toString();
	}

	/**
	 * @return
	 */
	public final static String createSegmentStatusList() {
		StringBuffer sb = new StringBuffer();
		sb.append("<option value='" + ReservationSegmentStatus.CONFIRMED + "'>" + ReservationSegmentStatus.CONFIRMED
				+ "</option>");
		sb.append("<option value='" + ReservationSegmentStatus.CANCEL + "'>" + ReservationSegmentStatus.CANCEL + "</option>");
		return sb.toString();
	}

	public final static String createAgentsListByName() throws ModuleException {
		return create_Description_Code_List(Agent);
	}

	public final static String createAgentsNameList(String[] params) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		Collection<String[]> l = LookupUtils.LookupDAO().getList(AgentNamesForGsa, params, 2); // RoleAgentType
		Iterator<String[]> iter = l.iterator();
		while (iter.hasNext()) {
			String[] s = iter.next();
			sb.append("<option value='" + s[0] + "'>" + s[1] + "</option>");
		}
		return sb.toString();
	}

	public final static Collection<String[]> createAgentsList() throws ModuleException {
		return LookupUtils.LookupDAO().getTwoColumnMap(Agent);

	}

	private final static String create_Description_Code_List(String paramString) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap(paramString);
		Iterator<String[]> iter = l.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			sb.append("<option value='" + s[1] + "'>" + s[0] + "</option>");
		}
		return sb.toString();
	}

	public final static String createAgentCurrencyCodeList() throws ModuleException {
		return create_Code_Code_List(AGENT_CURRENCY_CODE_LIST);
	}

	public final static String createAgentAddressCityList() throws ModuleException {
		return create_Code_Code_List(AGENT_ADDRESS_CITY_LIST);
	}

	public final static String createPaxTypeList() throws ModuleException {
		return create_Code_Name_List(PAX_TYPES);
	}

	public final static String createSeatMapChargeTemplateList() throws ModuleException {
		return create_Code_Description_List(SEAT_CHARGE_TEMPLATE_LIST);
	}

	public final static String createSeatMapChargeTemplateListStr() throws ModuleException {
		return create_Code_Description_Text(SEAT_CHARGE_TEMPLATE_TEXT);
	}
	
	public final static String createActSeatMapChargeTemplateListStr() throws ModuleException {
		return create_Code_Description_Text(ACTIVE_SEAT_CHARGE_TEMPLATE_TEXT);
	}

	public final static String createActiveSeatMapChargeTemplateList() throws ModuleException {
		return create_Code_Description_List(ACTIVE_SEAT_CHARGE_TEMPLATE_LIST);
	}

	public final static String createAlertTypeList() throws ModuleException {
		return create_Code_Description_List(ALERT_TYPE_LIST);
	}

	public final static String createSalesChannelSelect() throws ModuleException {
		return create_Code_Description_List(SALES_CHANNEL);
	}

	public final static List<String> getActiveLogicalCabinClassList() throws ModuleException {
		List<String> logicalCabinClassList = null;
		String selectSql = LOGICAL_CABIN_CLASS_CODES;

		String[] params = new String[] {};

		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, 2);

		if (colData != null && colData.size() > 0) {
			logicalCabinClassList = new ArrayList<String>();

			for (String[] segArr : colData) {
				logicalCabinClassList.add(segArr[1]);
			}

		}

		return logicalCabinClassList;
	}

	public final static String getAgentFareMaskStatus(String agentCode) {
		String[] params = { agentCode };
		return LookupUtils.LookupDAO().getStrObject(AGENT_FARE_MASK_STATUS, params);
	}

	public static String createBookingClassCodesList(HttpServletRequest request) throws ModuleException {
		StringBuffer strbData = new StringBuffer();
		strbData.append(SelectListGenerator.createBookingClassCodesArray());
		return strbData.toString();
	}

	public final static String createPromotionsTypesTemplateList() throws ModuleException {
		return create_Code_Description_List(PROMOTIONS_TYPES_LIST);
	}
	
	public final static String createVoucherTemplateList() throws ModuleException {
		return create_Code_Description_List(VOUCHER_TEMPLATE_LIST);
	}
	
	public final static List<String[]> createVoucherCurrencyList() throws ModuleException {
		return createCodeDescriptionList(CurrencyCode);
	}

	public final static List<String> getSelectedFlightNoList(String routes) throws ModuleException {

		return createFirstResultListWithParam(SELECTED_FLIGHT_NOS, routes);

	}

	public final static String createBundledFareNamesList() throws ModuleException {
		return create_Code_Description_List(BUNDLED_FARE_NAME_LIST);
	}

	public final static String createBundledCategoryList() throws ModuleException {
		return create_Code_Description_List(BUNDLED_CATEGORY_LIST);
	}

	private static String getLCCDescription(String defaultDescriptionContent, String translatedContent) {
		String seatDescription = null;
		// if there's translated content we must use it for seat description.
		if (!StringUtils.isBlank(translatedContent)) {
			seatDescription = StringUtil.getUnicode(translatedContent.trim());
		} else if (!StringUtils.isBlank(defaultDescriptionContent)) {
			seatDescription = defaultDescriptionContent;
		}
		return seatDescription;
	}

	public final static Map<String, Map<String, String>> getAnciOfferTypeNTemplates() {
		Map<String, Map<String, String>> results = new HashMap<String, Map<String, String>>();
		for (AnciOfferType anciType : AnciOfferType.values()) {

			Collection<String[]> resArr;

			switch (anciType) {
			case SEAT:
				resArr = LookupUtils.LookupDAO().getList2(ANCI_OFFER_SEAT, 2);
				break;
			case MEAL:
				resArr = LookupUtils.LookupDAO().getList2(ANCI_OFFER_MEAL, 2);
				break;
			case BAGGAGE:
				resArr = LookupUtils.LookupDAO().getList2(ANCI_OFFER_BAGGAGE, 2);
				break;
			case INSURANCE:
				List<String[]> tmpList = new ArrayList<String[]>();
				tmpList.add(new String[] { "0", TEMPLATE_ID_INAPPLICABLE });
				resArr = tmpList;
				break;
			default:
				throw new RuntimeException("Unhandled AnciOffer type in list generation.");
			}
			results.put(anciType.toString(), toMap(resArr));
		}
		return results;
	}

	private static Map<String, String> toMap(Collection<String[]> mapDataArray) {
		Map<String, String> resultMap = new HashMap<String, String>();
		for (String str[] : mapDataArray) {
			resultMap.put(str[0], str[1]);
		}
		return resultMap;
	}

	public static String getTermsTemplateNameList() throws ModuleException {
		return create_Single_Result_Code_Code_List(TERMS_TEMPLATE_NAMES);
	}
	
	public static String getVoucherTermsTemplateNameList() throws ModuleException {
		return create_Single_Result_Code_Code_List(VOUCHER_TERMS_TEMPLATE_NAMES);
	}

	public static String getBundledOndList() throws ModuleException {
		return createSingleColumnOptionList(BUNDLED_OND_LIST);
	}

	public static String getBundledBookingClassesList() throws ModuleException {
		return createSingleColumnOptionList(BUNDLED_BOOKING_CLASSES_LIST);
	}

	public static String getBundledStatusList() throws ModuleException {
		return createSingleColumnOptionList(BUNDLED_STATUS_LIST);
	}

	public final static String createDropDown(Map<String, String> items) throws ModuleException {

		StringBuffer sb = new StringBuffer();

		for (Entry<String, String> entry : items.entrySet()) {
			sb.append("<option value='" + entry.getKey() + "'>" + entry.getValue() + "</option>");
		}
		return sb.toString();
	}

	public final static String createPNRGOVCountryList() throws ModuleException {
		return create_Code_Name_List(PNRGOV_COUNTRY_LIST);
	}

	public static String getBspTransactionTypes() {

		StringBuffer sb = new StringBuffer();
		sb.append("<option value='" + BSPTransactionDataDTO.TransactionType.TKTT.toString() + "'>"
				+ BSPTransactionDataDTO.TransactionType.TKTT.getDescription() + "</option>");
		sb.append("<option value='" + BSPTransactionDataDTO.TransactionType.RFND.toString() + "'>"
				+ BSPTransactionDataDTO.TransactionType.RFND.getDescription() + "</option>");
		return sb.toString();
	}

	public static final Collection<String[]> createBundledFaresList() throws ModuleException {
		int colCount = 2;
		String selectSql = ALL_BUNDLED_FARES_LIST;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}

	public static final Collection<String[]> createLoyaltyProductCodesList() throws ModuleException {
		int colCount = 2;
		String selectSql = LOYALTY_PRODUCT_CODES_LIST;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}

	public static final String createInfantTitles() throws ModuleException {
		String[] infantTitles = new String[] { String.valueOf(ReservationInternalConstants.INFANT_TITLE_VALUES.MISS),
				String.valueOf(ReservationInternalConstants.INFANT_TITLE_VALUES.MISS.values()) };

		String[] infantValues = new String[] { String.valueOf(ReservationInternalConstants.INFANT_TITLE_VALUES.MSTR),
				String.valueOf(ReservationInternalConstants.INFANT_TITLE_VALUES.MSTR.values()) };

		Collection<String[]> colData = new ArrayList<String[]>();
		colData.add(infantTitles);
		colData.add(infantValues);

		String strArray = getJSArray("arrInfantTitles", colData);
		return strArray;
	}

	private static Set<String> createBusCarrierCodes(String sysCarrierCodes) {
		List<String[]> l = LookupUtils.LookupDAO().getFourColumnMap(sysCarrierCodes);
		Set<String> busCarriers = new HashSet<String>();
		Iterator<String[]> iter = l.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			if (CarrierServiceType.BUS_SERVICE.equals(s[3])) {
				busCarriers.add(s[0]);
			}
		}
		return busCarriers;
	}

	public static final Collection<String[]> getAdultTitles() throws ModuleException {
		int colCount = 2;
		String selectSql = PAX_TITLES_ADULT;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}
	
	public final static String getActPrivilegesList() throws ModuleException {
		String selectSql = actPrivileges;
				
		StringBuffer sb = new StringBuffer();
		Collection<String[]> col = LookupUtils.LookupDAO().getList2(selectSql, 2);
		Iterator<String[]> iter = col.iterator();
		while (iter.hasNext()) {
			String s[] = iter.next();
			sb.append("<option value='" + s[0] + "'>" + s[1] + "</option>");
		}
		return sb.toString();
	
	}

	public static final Collection<String[]> getChildTitle() throws ModuleException {
		int colCount = 2;
		String selectSql = PAX_TITLES_CHILD;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}

	public static final Collection<String[]> getInfantTitles() throws ModuleException {

		int colCount = 2;
		String selectSql = PAX_TITLES_INFANT;
		String[] params = new String[] {};
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);
		return colData;
	}
	
	public final static String createForceConfirmedPayStatuTypes() throws ModuleException {
		
		StringBuffer sb = new StringBuffer();
		List<String> statusList = ForceConfirmedPayStatus.getForceConfirmedPayStatus();
			
		Iterator<String> iter = statusList.iterator();
		while (iter.hasNext()) {
			String state = iter.next();
			if (state.equals(ForceConfirmedPayStatus.NOT_PAID.getStatus())) {
				sb.append("<option value='" + ForceConfirmedPayStatus.NOT_PAID.getStatus() + "'>" + "Not Paid" + "</option>");
			} else {

				sb.append("<option value='" + ForceConfirmedPayStatus.PARTIALLY_PAID.getStatus() + "'>" + "Partially Paid" + "</option>");
			}

		}
		return sb.toString();
	}
		
	public final static Collection<String[]> createAgentForLoggedUserLevelOfRights(String agentCode, int level)
			throws ModuleException {

		String whereClause = "";

		if (level == 2) {
			whereClause = " WHERE L>=1 ";
		} else if (level == 3) {
			whereClause = " WHERE L>=2 ";
		} else if (level == 4) {
			whereClause = " WHERE L=1 ";
		}

		StringBuilder sb = new StringBuilder();
		sb.append(
				"SELECT TA.AGENT_CODE, TA.AGENT_NAME || ' - ' || TA.AIRLINE_CODE, TA.AGENT_CODE,TA.AGENT_NAME,TA.AGENT_TYPE_CODE");
		sb.append(" FROM (SELECT LEVEL L, A.* FROM T_AGENT A CONNECT BY PRIOR AGENT_CODE = GSA_CODE ");
		sb.append(" START WITH AGENT_CODE = '" + agentCode + "' )TA " + whereClause);
		if (level <= 4) {

			Collection<String[]> col = LookupUtils.LookupDAO().getList1(sb.toString(), 5);

			return col;
		}

		return null;
	}
	
	public final static Collection<String[]> createAgentForLoggedUserLevelOfRightsByStation(String agentCode, int level,
			String station) throws ModuleException {

		StringBuilder whereClause = new StringBuilder();
		whereClause.append(" WHERE TA.station_code = '");
		whereClause.append(station);
		whereClause.append("'");
		if (level == 2) {
			whereClause.append(" AND L>=1 ");
		} else if (level == 3) {
			whereClause.append(" AND L>=2 ");
		} else if (level == 4) {
			whereClause.append(" AND L=1 ");
		}

		StringBuilder sb = new StringBuilder();
		sb.append(
				"SELECT TA.AGENT_CODE, TA.AGENT_NAME || ' - ' || TA.AIRLINE_CODE, TA.AGENT_CODE,TA.AGENT_NAME,TA.AGENT_TYPE_CODE");
		sb.append(" FROM (SELECT LEVEL L, A.* FROM T_AGENT A CONNECT BY PRIOR AGENT_CODE = GSA_CODE ");
		sb.append(" START WITH AGENT_CODE = '" + agentCode + "' )TA " + whereClause.toString());
		if (level <= 4) {

			Collection<String[]> col = LookupUtils.LookupDAO().getList1(sb.toString(), 5);

			return col;
		}

		return null;
	}

	public final static Collection<String[]> createAgentsOfReporting(String agentCode, boolean displayAllAgents,
			String strAgentType, boolean blnWithTAs, boolean blnWithCOs, String agentType)
			throws ModuleException {
		String selectSql = ALL_AGENTS_FOR_GSA_V2;

		Map<String, Object> params = new HashMap<>();

		List<String> agentTypes = new ArrayList<>();

		if (strAgentType != null) {
			agentTypes.add(strAgentType);
		}

		if (blnWithCOs) {
			agentTypes.add("CO");
		}

		if (blnWithTAs) {
			agentTypes.add("SGSA");
			agentTypes.add("TA");
			agentTypes.add("STA");
		}

		params.put("agentTypes", agentTypes);

		if (agentType.equals(AppSysParamsUtil.getCarrierAgent()) && !displayAllAgents) {
			selectSql = "allAgentsForType";
		} else if (!agentType.equals(AppSysParamsUtil.getCarrierAgent())) {
			selectSql = "reportingAgentsForGSAV2";

			if (!StringUtil.isNullOrEmpty(agentCode)) {
				params.put("agentCode", agentCode);
			}

			if (displayAllAgents) {
				selectSql = ALL_REPORTING_AGENTS_FOR_GSA_V2;
			}
		}

		Collection<String[]> col = LookupUtils.LookupDAO().getListForParameterList(selectSql, params, 5);
		return col;
	}

	public final static String createAgentStatusTypes() throws ModuleException {
		
		StringBuffer sb = new StringBuffer();
		List<String> statusList = AgentStatus.getAgentStatusToFilter();

		Iterator<String> iter = statusList.iterator();
		while (iter.hasNext()) {
			String state = iter.next();
			if (state.equals(AgentStatus.ACTIVE.getStatus())) {
				sb.append("<option value='" + AgentStatus.ACTIVE.getStatus() + "'>" + "Active" + "</option>");
			} else {

				sb.append("<option value='" + AgentStatus.INACTIVE.getStatus() + "'>" + "Inactive" + "</option>");
			}

		}
		return sb.toString();
	}	

	public final static String createCountryState() throws ModuleException {
		String selectSql = COUNTRY_STATE;

		StringBuilder sb = new StringBuilder();
		StringBuilder sb2 = new StringBuilder();
		Map<String, Map<String, String>> map = LookupUtils.LookupDAO().getMap((selectSql), true);
		
		if (map != null) {
			for (Entry<String, Map<String, String>> element : map.entrySet()) {
				sb2 = new StringBuilder();
				sb2.append('"');
				for(Entry<String, String> element2 : element.getValue().entrySet()){
					sb2.append("<option value='" + element2.getKey().trim() + "'>" + element2.getValue() + "</option>");
				}
				sb2.append('"');
				sb.append("countryStateArry" + "['" + element.getKey() + "'] = " +sb2.toString() + "; ");
			}
		}
		return sb.toString();
	}

	public final static String createCountryStateArrayForMultiSelect() throws ModuleException {
		String selectSql = COUNTRY_STATE;
		StringBuilder sb = new StringBuilder();
		sb.append("countryStateArray = new Array();");
		Map<String, Map<String, String>> map = LookupUtils.LookupDAO().getMap((selectSql), true);

		if (map != null) {
			StringBuilder sb2 = new StringBuilder();
			int countryIndex = 0;
			for (Entry<String, Map<String, String>> element : map.entrySet()) {
				sb2 = new StringBuilder();
				sb2.append("var country" + countryIndex + " = new Array(); ");
				int stateIndex = 0;
				for (Entry<String, String> element2 : element.getValue().entrySet()) {
					sb2.append("country" + countryIndex + "['" + stateIndex + "'] = new Array('" + element2.getValue() + "','"
									+ element2.getKey().trim() + "'); ");
					stateIndex++;
				}
				sb.append(sb2.toString()).
						append("countryStateArray['" + element.getKey() + "'] = country" + countryIndex + ";");
				countryIndex++;
			}
		}
		return sb.toString();
	}

	public static final Collection<String[]> createAllSubAgentsWithCredit(String airlineCode, String agentCode, int colCount)
			throws ModuleException {
		String selectSql = ALL_SUB_AGENTS_WITH_CREDITS;
		String[] params = new String[] { airlineCode, agentCode };
		Collection<String[]> colData = LookupUtils.LookupDAO().getList(selectSql, params, colCount);

		return colData;
	}

	public final static Collection<String[]> createAgentList(Collection<Agent> agents) throws ModuleException {
		Collection<String[]> col = new ArrayList<>();
		agents.forEach(agent -> col.add(new String[] { agent.getAgentName(), agent.getAgentCode() }));
		return col;
	}
	
	public static final String getGSTMessageEnabledAirports(){ 
		
		List<String> airportCodes = LookupUtils.LookupDAO().getSingleColumnMap(GST_MESSAGE_ENABLED_AIRPORTS);
		StringBuffer sb = new StringBuffer();
		int i = 0;
		
		for (String airportCode : airportCodes) {
			sb.append("gstMessageEnabledAirports["+i+"] = '"+airportCode+"';");
			i++;
		}
		
		return sb.toString();
	}	

	public static final Collection<String> getRouteOndCodeList() throws ModuleException {
		return createSingleColumnResultList(ROUTE_OND_CODE_LIST);
	}

	public static final Collection<String> getSearchTags(SearchType searchType) throws ModuleException {
		return createSingleColumnResultList(SEARCH_TAGS, new Object[] { searchType.name() });
	}

	public final static Collection<String[]> createStationCountryForGSA(String params[]) throws ModuleException {
		String selectSql = GSA_COUNTRY_WISE_STATIONS;
		Collection<String[]> col = LookupUtils.LookupDAO().getList(selectSql, params, 2);
		return col;
	}
		
	public static final String createQueuePagesList() throws ModuleException{
		
		return create_Code_Description_List(QUEUE_PAGES);
		
	}
	
	public static final String createQueuesList() throws ModuleException{
		
		return create_Code_Description_List(QUEUES);
		
	}
	
	public static final String createRequestStatusList() throws ModuleException{
		
		return create_Code_Description_List(REQUESTSTATUS);
		
	}
	
	public static final String createQueueListForPage(String sql) throws ModuleException{
		
		return create_Code_Description_List_Sql(sql);
		
	}

	public final static String createApplicableHandlingFeeModificationOps() throws ModuleException {

		StringBuffer sb = new StringBuffer();
		List<Integer> opearionList = ModifyOperation.getReservationOperations();

		Iterator<Integer> iter = opearionList.iterator();

		while (iter.hasNext()) {
			Integer operation = iter.next();

			switch (operation.intValue()) {
			case 2:
				sb.append("<option value='" + operation.intValue() + "'>" + "Modify Seg" + "</option>");
				break;
			case 3:
				sb.append("<option value='" + operation.intValue() + "'>" + "Cancel Seg" + "</option>");
				break;
			case 4:
				sb.append("<option value='" + operation.intValue() + "'>" + "Add Seg" + "</option>");
				break;
			case 5:
				sb.append("<option value='" + operation.intValue() + "'>" + "Name Change" + "</option>");
				break;
			case 6:
				sb.append("<option value='" + operation.intValue() + "'>" + "Anci Modify" + "</option>");
				break;
			case 7:
				sb.append("<option value='" + operation.intValue() + "'>" + "Remove Pax" + "</option>");
				break;
			case 8:
				sb.append("<option value='" + operation.intValue() + "'>" + "Add Infant" + "</option>");
				break;
			case 9:
				sb.append("<option value='" + operation.intValue() + "'>" + "Cancel PNR" + "</option>");
				break;
			case 10:
				sb.append("<option value='" + operation.intValue() + "'>" + "Charge Adjust" + "</option>");
				break;
			default:
				sb.append("<option value='" + 0 + "'>" + " " + "</option>");
			}
		}
		return sb.toString();
	}
	
	public final static String createHandlingFeeModificationStatus() throws ModuleException {
		StringBuffer sb = new StringBuffer();
		
		sb.append("<option value='ACT'>ACT</option>");
		sb.append("<option value='INA'  selected='selected'>INA</option>");
		
		return sb.toString();
	}
	
	
	public final static String createReservationFlows() throws ModuleException {
		StringBuilder sb = new StringBuilder();
		sb.append("<option value='" + ChargeRateReservationFlows.ANY+"'>Any</option>");
		sb.append("<option value='" + ChargeRateReservationFlows.CREATE+"'>Create</option>");
		sb.append("<option value='" + ChargeRateReservationFlows.MODIFY+"'>Modify</option>");
		return sb.toString();
	}

	public static String createStatusList() {
		StringBuilder sb = new StringBuilder();
		sb.append("<option value='" + BatchStatus.DONE+"'>Done</option>");
		sb.append("<option value='" + BatchStatus.WAITING+"'>Waiting</option>");
		sb.append("<option value='" + BatchStatus.IN_PROGRESS+"'>In Progress</option>");
		sb.append("<option value='" + BatchStatus.ERROR+"'>Error</option>");
		sb.append("<option value='" + BatchStatus.TERMINATED+"'>Terminated</option>");
		return sb.toString();
	}

	public final static Collection<String[]> createHubCountry() throws ModuleException {
		Collection<String[]> hubCountries = new ArrayList<String[]>();

		Map<String, CachedAirlineDTO> cachedAirlineDTOMap = AirproxyModuleUtils.getAirportBD().getLccAirlineList();
		if (cachedAirlineDTOMap != null) {
			for (Entry<String, CachedAirlineDTO> element : cachedAirlineDTOMap.entrySet()) {
				String[] hubCountry = new String[] { element.getKey(), element.getValue().getCountryCode() };
				hubCountries.add(hubCountry);
			}
		}
		return hubCountries;
	}
	
	public final static String getCountryWiseActiveAirports() throws ModuleException {
		StringBuilder sb = new StringBuilder();
		Map<String, Map<String, String>> activeAirportMap = LookupUtils.LookupDAO().getMap((COUNTRY_AIRPORTS_WITHOUT_BUS_SEG));
		sb.append(" var countries = new Array();");
		sb.append(" var cntryData = new Array();");
		int tempint = 0;
		int i = 0;
		if (activeAirportMap != null) {

			for (Entry<String, Map<String, String>> airportCountrySet : activeAirportMap.entrySet()) {
				sb.append("countries[" + i + "]");
				sb.append(" = '" + airportCountrySet.getKey() + "';");
				sb.append("var cities_" + i + " = new Array();");
				tempint = 0;
				for (Entry<String, String> element2 : airportCountrySet.getValue().entrySet()) {
					sb.append("cities_" + i + "[" + tempint + "] = new Array('" + element2.getValue().trim() + "("
							+ element2.getKey() + ")','" + element2.getKey() + "');");
					tempint++;
				}
				sb.append("cntryData[" + i + "] = cities_" + i + ";");
				i++;
			}
		}
		sb.append("var cntryGroup1 = countries; var cntryGroup2 = new Array();");
		sb.append("var cntryData2 = new Array();");
		sb.append("cntryData2[0] = new Array();");
		sb.append("var stls = new Listbox('lstSTC', 'lstAssignedSTC', 'airportList','stls');");
		sb.append("stls.group1 = cntryGroup1; stls.group2 = cntryGroup2;");
		sb.append("stls.list1 = cntryData; stls.list2 = cntryData2;");
		sb.append("stls.height = '125px'; stls.width = '160px';");
		sb.append("stls.headingLeft = \"<input type='text' placeholder='Search Country' id='searchItem' disabled='disabled' style='width: 160px;'>\";");
		sb.append("stls.drawListBox();");
		return sb.toString();
	}
	
	public static final String createMealCategoryArray() throws ModuleException {
		String selectSql = mealCategoryList;
		Collection<String[]> colData = LookupUtils.LookupDAO().getTwoColumnMap(selectSql);
		String strArray = getJSArray("arrMealCategory", colData);
		return strArray;
	}

        public final static String createEntityListByName() throws ModuleException {
		return create_Code_Name_List(Entity);
	}

}

