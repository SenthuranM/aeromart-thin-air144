package com.isa.thinair.webplatform.core.util;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletOutputStream;

public class GenericServletOutputStream extends ServletOutputStream {

	private OutputStream stream;

	public GenericServletOutputStream(OutputStream output) {
		stream = output;
	}

	public void write(int b) throws IOException {
		stream.write(b);
	}

	public void write(byte[] b) throws IOException {
		stream.write(b);
	}

	public void write(byte[] b, int off, int len) throws IOException {
		stream.write(b, off, len);
	}
}
