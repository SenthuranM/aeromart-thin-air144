package com.isa.thinair.webplatform.core.commons;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author rajitha
 *
 */
public enum AgentStatus {

	ACTIVE("ACT"), INACTIVE("INA");

	private final String status;

	AgentStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public static List<String> getAgentStatusToFilter() {

		List<String> statusTypes = new ArrayList<String>();

		for (AgentStatus status : AgentStatus.values()) {
			statusTypes.add(status.getStatus());
		}

		return statusTypes;
	}

}
