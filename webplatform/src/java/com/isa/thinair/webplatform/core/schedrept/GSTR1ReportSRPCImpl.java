package com.isa.thinair.webplatform.core.schedrept;

import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("ALL") public class GSTR1ReportSRPCImpl extends ScheduleReportCommon {
	private static Log log = LogFactory.getLog(GSTR1ReportSRPCImpl.class);

	@Override public ReportInputs composeReportInputs(HttpServletRequest request) {

		String stateCode = request.getParameter("selState");
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String reportMode = request.getParameter("hdnLive");

		String reportName;
		String GSTR1ReportTemplate = "GSTR1_Main.jasper";
		String reportTemplateRelativePath;

		Map parameters = new HashMap();
		String reportFormat = setReportExportType(value);
		this.setReportLogoLocation(reportFormat, parameters);

		ReportsSearchCriteria search = new ReportsSearchCriteria();

		try {

			reportName = ScheduleReportUtil.ScheduledReportMetaDataIDs.GSTR1_REPORT;
			reportTemplateRelativePath = getReportTemplateRelativeLocation(GSTR1ReportTemplate);

			if (!StringUtil.isNullOrEmpty(fromDate)) {
				search.setDateRangeFrom(convertDate(fromDate) + " 00:00:00");
			}

			if (!StringUtil.isNullOrEmpty(toDate)) {
				search.setDateRangeTo(convertDate(toDate) + "  23:59:59");
			}

			if (!StringUtil.isNullOrEmpty(stateCode)) {
				search.setStateCode(stateCode);
			}

			if (reportMode != null) {
				if (ScheduleReportUtil.ScheduleWebConstants.REP_LIVE.equals(reportMode)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (ScheduleReportUtil.ScheduleWebConstants.REP_OFFLINE.equals(reportMode)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("OPTION", value);
			parameters.put("STATE_CODE", stateCode);
			parameters.put("REPORT_MEDIUM", reportMode);

		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}

		return new ReportInputs(parameters, search, reportFormat, reportTemplateRelativePath, reportName);

	}

}
