/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Virsion $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.webplatform.core.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.dto.DashboardMessageTO;
import com.isa.thinair.webplatform.api.dto.DashbrdUserDataRetrivalTO;

/**
 * Class to access database for the usage of combobox data
 * 
 * @author Byorn
 */
public class WebServicesDAO extends PlatformBaseJdbcDAOSupport {

	private DataSource dataSource;

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * 
	 * @param ssrCode
	 * @return
	 */
	public boolean isValidSSR(String ssrCode) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		return ((Boolean) template.query("select 1 from T_SSR_INFO where ssr_code = ?", new Object[] { ssrCode },
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						return new Boolean(rs.next());
					}
				})).booleanValue();
	}

	public Map<String, Object[]> getAirportShortNames(String airportCode) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		final Map<String, Object[]> aptShortName = new HashMap<String, Object[]>();

		Object[] params = null;
		if (airportCode == null) {
			params = new Object[] { "ACT" };
		} else {
			params = new Object[] { "ACT", airportCode };
		}

		template.query("SELECT AIRPORT_CODE, AIRPORT_NAME, AIRPORT_SHORT_NAME, TERMINAL " + "FROM T_AIRPORT "
				+ "WHERE STATUS = ? " + (airportCode != null ? " AND AIRPORT_CODE = ? " : ""), params, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					aptShortName.put(
							rs.getString("AIRPORT_CODE"),
							new Object[] { rs.getString("AIRPORT_NAME"), rs.getString("AIRPORT_SHORT_NAME"),
									rs.getString("TERMINAL") });
				}
				return aptShortName;
			}
		});
		return aptShortName;
	}

	public Map<Integer, String> getGraphContent(String agentCode, String fromDate, String toDate) {

		JdbcTemplate template = new JdbcTemplate(getDataSource());
		final Map<Integer, String> graphDataMap = new HashMap<Integer, String>();
		Object[] params = new Object[] { agentCode, fromDate, toDate };

		template.query("SELECT count(a.pnr) as pnr_count, u.display_name as user_name" + " FROM t_reservation a, t_user u "
				+ " where a.origin_agent_code = ? " + " and a.origin_user_id = u.user_id "
				+ " and trunc(a.booking_timestamp) between ? and ? "
				+ " group by a.origin_user_id, u.display_name order by pnr_count desc",

		params, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				int i = 0;
				int totalAmount = 0;

				while (rs.next()) {
					if (i < 20) {
						graphDataMap.put(rs.getInt("pnr_count"), rs.getString("user_name"));
					} else {
						totalAmount += rs.getInt("pnr_count");
					}
				}
				if (totalAmount != 0) {
					graphDataMap.put(totalAmount, "Others");
				}
				return graphDataMap;
			}
		});
		return graphDataMap;
	}

	public Map<Integer, Collection<DashboardMessageTO>> getDashboardContent(Date date, DashbrdUserDataRetrivalTO userData) {
		DashbrdMessageFinderBL dashbrdMessageFinder = new DashbrdMessageFinderBL();
		dashbrdMessageFinder.setDashbrdMsgVirtualDAO(new DashbrdMsgVirtualDAO(getDataSource()));
		return dashbrdMessageFinder.getDisplayMessages(date, userData);
	}

	/**
	 * Returns true if both from and to airports are valid and active.
	 * 
	 * @param fromAirportCode
	 * @param toAirportCode
	 */
	public Boolean isFromAndToAirportsValid(String fromAirportCode, String toAirportCode) {
		Boolean isValid = new Boolean(false);
		Object[] params = new Object[] { "ACT", fromAirportCode, toAirportCode };

		JdbcTemplate template = new JdbcTemplate(getDataSource());
		isValid = (Boolean) template.query("SELECT COUNT(DISTINCT AIRPORT_CODE) AS CNT " + "FROM T_AIRPORT "
				+ "WHERE STATUS = ? " + " AND (AIRPORT_CODE = ? OR AIRPORT_CODE = ?)", params, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				int count = 0;
				if (rs.next()) {
					count = rs.getInt("CNT");
				}
				return new Boolean(count == 2);
			}
		});

		return isValid;
	}

	/**
	 * Returns true if paymentAgentCode is a reporting agency of requestingAgentCode
	 * 
	 * @param requestingAgentCode
	 * @param paymentAgentCode
	 * @return
	 */
	public Boolean isReportingAgency(String requestingAgentCode, String paymentAgentCode) {
		Boolean isReportingAgentPayment = new Boolean(false);
		Object[] params = new Object[] { paymentAgentCode, requestingAgentCode };

		JdbcTemplate template = new JdbcTemplate(getDataSource());
		isReportingAgentPayment = (Boolean) template.query(" SELECT COUNT(*) AS CNT FROM T_AGENT AG "
				+ " WHERE AG.agent_code = ? " + " AND AG.report_to_gsa = 'Y' " + " AND AG.status = 'ACT' "
				+ " AND AG.gsa_code = ? ", params, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				int count = 0;
				if (rs.next()) {
					count = rs.getInt("CNT");
				}
				return new Boolean(count == 1);
			}
		});

		return isReportingAgentPayment;
	}	
	
	public Boolean isOriginDestinaitionValidCitySearch(String origin, String destination) {
		return isLocationCodeExistInAiportOrCity(origin) && isLocationCodeExistInAiportOrCity(destination);
	}

	@SuppressWarnings({ "unchecked" })
	private Boolean isLocationCodeExistInAiportOrCity(String locationCode) {
		Boolean isValid = new Boolean(false);
		if (!StringUtil.isNullOrEmpty(locationCode)) {
			Object[] params = new Object[] {};

			JdbcTemplate template = new JdbcTemplate(getDataSource());

			StringBuilder sb = new StringBuilder();

			sb.append("SELECT (SELECT COUNT(CITY_CODE)  FROM T_CITY WHERE STATUS   = 'ACT' ");
			sb.append("AND CITY_CODE IN ('" + locationCode + "')) as CCNT,");
			sb.append("(SELECT COUNT(DISTINCT AIRPORT_CODE) 	FROM T_AIRPORT ");
			sb.append("WHERE STATUS      = 'ACT' ");
			sb.append("AND AIRPORT_CODE IN ('" + locationCode + "')) AS ACNT FROM DUAL");

			isValid = (Boolean) template.query(sb.toString(), params, new ResultSetExtractor() {
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					int count = 0;
					if (rs.next()) {
						count = rs.getInt("CCNT") + rs.getInt("ACNT");
					}
					return new Boolean(count >= 1);
				}
			});
		}

		return isValid;
	}
}
