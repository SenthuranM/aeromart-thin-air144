package com.isa.thinair.webplatform.core.util;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

public class ByteResponseWrapper extends HttpServletResponseWrapper {

	private ByteArrayOutputStream output;

	public String toString() {
		return output.toString();
	}

	public ByteResponseWrapper(HttpServletResponse response) {
		super(response);
		output = new ByteArrayOutputStream();
	}

	public PrintWriter getWriter() {
		return new PrintWriter(output);
	}
}
