package com.isa.thinair.webplatform.core.schedrept;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.schedrept.ScheduleReportUtil.ScheduleWebConstants;

public class TopAgentsReportSenderImpl extends ScheduleReportCommon {

	private static Log log = LogFactory.getLog(TopAgentsReportSenderImpl.class);

	@Override
	public ReportInputs composeReportInputs(HttpServletRequest request) {
		String reportTempleteRelativePath = null;

		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String segFrom = request.getParameter("selFrom");
		String segTo = request.getParameter("selTo");
		String noOfAgents = request.getParameter("radNoTopAgents");
		String otherNo = request.getParameter("txtTopAgentsOther");
		String flightType = request.getParameter("selFlightType");

		String id = "UC_REPM_012";
		String topAgentsRpt = "TopAgentsReport.jasper";
		String strCarrier = AppSysParamsUtil.getDefaultCarrierCode() + " Reservation System";
		String strBase = AppSysParamsUtil.getBaseCurrency();
		String strlive = request.getParameter(ScheduleWebConstants.REP_HDN_LIVE);
		
		ReportsSearchCriteria search = new ReportsSearchCriteria();

		if (toDate != null && !toDate.equals("")) {
			search.setDateRangeTo(convertDate(toDate) + " 23:59:59");
		}
		if (fromDate != null && !fromDate.equals("")) {
			search.setDateRangeFrom(convertDate(fromDate) + " 00:00:00");
		}

		if (segFrom != null && !segFrom.equals("")) {
			search.setSectorFrom(segFrom);
		} else {
			search.setSectorFrom("");
		}

		if (segTo != null && !segTo.equals("")) {
			search.setSectorTo(segTo);
		} else {
			search.setSectorTo("");
		}

		if (flightType != null && !flightType.equals("")) {
			search.setFlightType(flightType);
		}

		if (noOfAgents != null && !noOfAgents.equals("")) {
			if (!noOfAgents.equals("other")) {
				search.setNoOfTops(Integer.parseInt(noOfAgents));
			} else {
				search.setNoOfTops(Integer.parseInt(otherNo));
			}
		}

		if (strlive != null) {
			if (strlive.equals(ScheduleWebConstants.REP_LIVE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
			} else if (strlive.equals(ScheduleWebConstants.REP_OFFLINE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
			}
		}

		Map<String, String> parameters = new HashMap<String, String>();
		String reportFormat = setReportExportType(value);
		this.setReportLogoLocation(reportFormat, parameters);
		try {
			search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);

			reportTempleteRelativePath = getReportTemplateRelativeLocation(topAgentsRpt);

			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("SEGMENT", segFrom + "/" + segTo);
			parameters.put("ID", id);
			parameters.put(ScheduleWebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("AMT_1", "(" + strBase + ")");
			parameters.put("FLIGHT_TYPE", flightType);

			if (AppSysParamsUtil.isPromoCodeEnabled()) {
				parameters.put("SHW_DISCOUNT", "Y");
			} else {
				parameters.put("SHW_DISCOUNT", "N");
			}
			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			setPreferedReportFormat(reportNumFormat, parameters);
		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}
		return new ReportInputs(parameters, search, reportFormat, reportTempleteRelativePath,
				ScheduleReportUtil.ScheduledReportMetaDataIDs.TOP_AGENTS_REPORT);

	}

	protected String convertDate(String dateString) {
		String date = null;
		int day = Integer.parseInt(dateString.substring(0, 2));
		int month = Integer.parseInt(dateString.substring(3, 5));
		int year = Integer.parseInt(dateString.substring(6));

		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = new GregorianCalendar(year, (month - 1), day);
		date = dateFormat.format(cal.getTime());

		return date;
	}
}
