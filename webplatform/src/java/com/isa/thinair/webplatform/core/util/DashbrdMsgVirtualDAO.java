package com.isa.thinair.webplatform.core.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.api.dto.DashboardMessageTO;
import com.isa.thinair.webplatform.api.dto.DashbrdMsgRetrivedDataTO;

/**
 * Database Data for Dashbrd Messages
 * 
 * @author Navod Ediriweera
 * 
 */
public class DashbrdMsgVirtualDAO {

	private JdbcTemplate template;

	public DashbrdMsgVirtualDAO(DataSource dataSource) {
		if (template == null)
			template = new JdbcTemplate(dataSource);
	}

	public static final String APPLY_STATUS_Y = "Y";
	public static final String APPLY_STATUS_N = "N";
	private static final String MSG_ACTIVE = "ACT";

	public DashbrdMsgRetrivedDataTO getMsgIDsForAgent(Object... params) {
		return this.getDashbrdMsgIDs(this.buildAgentSQL(), params);
	}

	public DashbrdMsgRetrivedDataTO getMsgIDsForAgentType(Object... params) {
		return this.getDashbrdMsgIDs(this.buildAgentTypeSQL(), params);
	}

	public DashbrdMsgRetrivedDataTO getMsgIDsForUser(Object... params) {
		return this.getDashbrdMsgIDs(this.buildUserSQL(), params);
	}

	public DashbrdMsgRetrivedDataTO getMsgIDsForPOS(Object... params) {
		return this.getDashbrdMsgIDs(this.buildPosSQL(), params);
	}

	private DashbrdMsgRetrivedDataTO getDashbrdMsgIDs(String sql, Object... params) {

		DashbrdMsgRetrivedDataTO msgIDs = (DashbrdMsgRetrivedDataTO) template.query(sql, params, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				DashbrdMsgRetrivedDataTO visibilityTO = new DashbrdMsgRetrivedDataTO();
				while (rs.next()) {
					Integer id = rs.getInt("dashboard_message_id");
					String applyStatus = rs.getString("applyStatus");
					if (APPLY_STATUS_Y.equals(applyStatus)) {
						visibilityTO.getIncludedIDSet().add(id);
					} else if (APPLY_STATUS_N.equals(applyStatus)) {
						visibilityTO.getExcludedIDSet().add(id);
					}
				}
				return visibilityTO;
			}
		});
		return msgIDs;
	}

	@SuppressWarnings("unchecked")
	public Set<Integer> getAllDashbrdMsgIDs(Date date) {

		Object[] params = new Object[] { new Timestamp(date.getTime()) };

		Set<Integer> setData = (Set) template.query("SELECT a.dashboard_message_id " + " FROM t_dashboard_message a"
				+ " WHERE (? BETWEEN a.from_date AND a.end_date) AND a.STATUS = '" + MSG_ACTIVE + "'", params,
				new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Set<Integer> setData = new HashSet<Integer>();
						while (rs.next()) {
							setData.add(rs.getInt("dashboard_message_id"));
						}
						return setData;
					}
				});

		return setData;
	}

	@SuppressWarnings("unchecked")
	public Map<Integer, Collection<DashboardMessageTO>> getAllDashbrdMessages(List<Integer> msgIDs) {

		Map<Integer, Collection<DashboardMessageTO>> mapDashboardMessages = (Map) template.query(
				"SELECT a.dashboard_message_id, a.message_type, a.MESSAGE_CONTENT, "
						+ " a.from_date, a.end_date, a.user_list, b.priority_id, b.message_description"
						+ " FROM t_dashboard_message a, t_dashboard_message_type b " + " WHERE a.dashboard_message_id IN ("
						+ Util.buildIntegerInClauseContent(msgIDs) + ")" + " AND a.message_type = b.message_type "
						+ " AND a.STATUS = '" + MSG_ACTIVE + "' " + " ORDER BY b.priority_id, a.priority_id ASC ",
				new Object[] {}, new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<Integer, Collection<DashboardMessageTO>> mapDashboardMessages = new TreeMap<Integer, Collection<DashboardMessageTO>>();
						Collection<DashboardMessageTO> colDashboardMessageTO;
						DashboardMessageTO dashboardMessageTO;
						Integer priorityId;

						while (rs.next()) {
							dashboardMessageTO = new DashboardMessageTO();
							String actualMsg = rs.getString("MESSAGE_CONTENT");
							if (PlatformUtiltiies.isNotEmptyString(actualMsg) && actualMsg.trim().length() != 0) {
								priorityId = rs.getInt("priority_id");

								dashboardMessageTO.setMessageType(PlatformUtiltiies.nullHandler(rs.getString("message_type")));
								dashboardMessageTO.setMessageId(rs.getInt("dashboard_message_id"));
								dashboardMessageTO.setMessageTypeDesc(PlatformUtiltiies.nullHandler(rs
										.getString("message_description")));
								dashboardMessageTO.setSubject(actualMsg);
								dashboardMessageTO.setUserList(PlatformUtiltiies.nullHandler(rs.getString("user_list")));
								dashboardMessageTO.setFromDate(rs.getTimestamp("from_date"));
								dashboardMessageTO.setEndDate(rs.getTimestamp("end_date"));

								colDashboardMessageTO = mapDashboardMessages.get(priorityId);
								if (colDashboardMessageTO == null) {
									colDashboardMessageTO = new ArrayList<DashboardMessageTO>();
									mapDashboardMessages.put(priorityId, colDashboardMessageTO);
								}
								colDashboardMessageTO.add(dashboardMessageTO);
							}

						}
						return mapDashboardMessages;
					}
				});
		return mapDashboardMessages;
	}

	private String buildAgentSQL() {
		StringBuilder sb = new StringBuilder(
				"SELECT a.dashboard_message_id , APPLY_STATUS as applyStatus FROM t_dashboard_agent a");
		sb.append(" WHERE a.AGENT_CODE = ? ");
		return sb.toString();
	}

	private String buildAgentTypeSQL() {
		StringBuilder sb = new StringBuilder(
				"SELECT a.dashboard_message_id , APPLY_STATUS as applyStatus  FROM T_DASHBOARD_AGENT_TYPE a");
		sb.append(" WHERE a.AGENT_TYPE_CODE = ? ");

		return sb.toString();
	}

	private String buildUserSQL() {
		StringBuilder sb = new StringBuilder(
				"SELECT a.dashboard_message_id , APPLY_STATUS as applyStatus  FROM T_DASHBOARD_USER a");
		sb.append(" WHERE a.USER_ID = ? ");
		return sb.toString();
	}

	private String buildPosSQL() {
		StringBuilder sb = new StringBuilder("SELECT a.dashboard_message_id , APPLY_STATUS as applyStatus FROM T_DASHBOARD_POS a");
		sb.append(" WHERE a.POS_CODE = ? ");
		return sb.toString();
	}

}
