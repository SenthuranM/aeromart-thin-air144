package com.isa.thinair.webplatform.core.schedrept;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.schedrept.ScheduleReportUtil.ScheduleWebConstants;

public class SeatInventoryCollectionsSRPCImpl extends ScheduleReportCommon {

	@Override
	public ReportInputs composeReportInputs(HttpServletRequest request) {

		String strReportType = request.getParameter("selReportType");
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String bookedFromDate = request.getParameter("txtBookedFromDate");
		String bookedToDate = request.getParameter("txtBookedToDate");
		String value = request.getParameter("radReportOption");
		String from = request.getParameter("selDept");
		String to = request.getParameter("selArival");
		String flightNo = request.getParameter("txtFlightNo");
		String selVia1 = request.getParameter("selVia1");
		String selVia2 = request.getParameter("selVia2");
		String selVia3 = request.getParameter("selVia3");
		String selVia4 = request.getParameter("selVia4");
		String strOperationType = request.getParameter("selOperationType");
		String strFlightStatus = request.getParameter("selFlightStatus");
		String carrierCode = request.getParameter("hdnCarrierCode");
		
		boolean logicalCabinClassEnable = AppSysParamsUtil.isLogicalCabinClassEnabled();
		String logicalCCEnable = "" + logicalCabinClassEnable + "";
		boolean showFullColumns = hasPrivilege(request, PriviledgeConstants.PRIVI_VIEW_FULL_SEAT_INVENT_RPT);
		boolean showRevenueColumn = hasPrivilege(request, PriviledgeConstants.PRIVI_SHOW_REVENUE_SEAT_INVENT_RPT);
		boolean showAditionalColumns = AppSysParamsUtil.isShowFwdBookingReportAdditionalInfo();
		
		Collection<String> carrierCodesCol = new ArrayList<String>();

		List<String> viaList = new ArrayList<String>();
		if (selVia1 != null && !selVia1.equals("")) {
			viaList.add(selVia1);
		}
		if (selVia2 != null && !selVia2.equals("")) {
			viaList.add(selVia2);
		}
		if (selVia3 != null && !selVia3.equals("")) {
			viaList.add(selVia3);
		}
		if (selVia4 != null && !selVia4.equals("")) {
			viaList.add(selVia4);
		}
		String id = "UC_REPM_015";
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../../images/" +  AppSysParamsUtil.getReportLogo(true);

		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strlive = request.getParameter(ScheduleWebConstants.REP_HDN_LIVE);

		Map<String, String> parameters = new HashMap<String, String>();
		String reportFormat = setReportExportType(value);
		this.setReportLogoLocation(reportFormat, parameters);
		ReportsSearchCriteria search = new ReportsSearchCriteria();
		String reportTempleteRelativePath = null;
		try {
			search.setReportOption(ReportsSearchCriteria.FORWARD_BOOKING);
			search.setOperationType(strOperationType);

			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(convertDate(fromDate) + " 00:00:00");
			}
			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(convertDate(toDate) + "  23:59:59");
			}			
			
			if (bookedFromDate != null && !bookedFromDate.equals("")) {				
				search.setDateRange2From(convertDate(bookedFromDate) + " 00:00:00");
				parameters.put("BOOKED_FROM_DATE", bookedFromDate);
			} else {
				parameters.put("BOOKED_FROM_DATE", "");
			}
			if (bookedToDate != null && !bookedToDate.equals("")) {				
				search.setDateRange2To(convertDate(bookedToDate) + "  23:59:59");
				parameters.put("BOOKED_TO_DATE", bookedToDate);
			} else {
				parameters.put("BOOKED_TO_DATE", "");
			}

			search.setMatchCombination(false);

			if (flightNo != null && !flightNo.equals("")) {
				search.setFlightNumber(flightNo.trim());
			}
			if (to != null && !to.equals("")) {
				search.setSectorTo(to);
			}
			if (from != null && !from.equals("")) {
				search.setSectorFrom(from);
			}
			search.setViaPoints(viaList);

			if (strFlightStatus != null && !strFlightStatus.equals("")) {
				search.setFlightStatus(strFlightStatus);
			}

			String carrierCodeArr[] = null;
			if (!carrierCode.trim().equals("")) {
				if (carrierCode != null && carrierCode.indexOf(",") != -1) {
					carrierCodeArr = carrierCode.split(",");
				} else {
					carrierCodeArr = new String[1];
					carrierCodeArr[0] = carrierCode;
				}
			}
			if (carrierCodeArr != null && carrierCodeArr.length > 0) {
				for (int i = 0; i < carrierCodeArr.length; i++) {
					carrierCodesCol.add(carrierCodeArr[i]);
				}
			}
			search.setCarrierCodes(carrierCodesCol);

			if (strlive != null) {
				if (strlive.equals(ScheduleWebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(ScheduleWebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			String reptemplateTotal = "";

			if (strReportType.equals(ReportsSearchCriteria.FORWARD_BOOKING_OPTION_BY_FLIGHT)) {
				search.setByFlight(true);
				parameters.put("SHOW_FLIGHT", "Y");
				parameters.put("SHOW_STATION", "N");
				reptemplateTotal = "ViewSeatInvFareMvmntTot.jasper";
			} else if (strReportType.equals(ReportsSearchCriteria.FORWARD_BOOKING_OPTION_BY_STATION)) {
				search.setByStation(true);
				parameters.put("SHOW_STATION", "Y");
				reptemplateTotal = "ViewSeatInvFareMvmntTot.jasper";
			} else if (strReportType.equals(ReportsSearchCriteria.FORWARD_BOOKING_OPTION_BY_AGENT)) {
				search.setByAgent(true);
				parameters.put("SHOW_AGENT", "Y");
				reptemplateTotal = "ViewSeatInvFareMvmntAgt.jasper";
			}

			parameters.put("SHOW_FULL_COLUMNS", String.valueOf(showFullColumns));
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			parameters.put(ScheduleWebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("AMT_1", "(" + strBase + ")");
			parameters.put("LOGICAL_CABIN_CLASS_ENABLE", logicalCCEnable);
			parameters.put("SHOW_TOTAL_COLUMNS", "TRUE");
			parameters.put("SHOW_REVENUE_DATA", String.valueOf(showRevenueColumn));
			parameters.put("SHOW_ADDITIONAL_COLUMNS", String.valueOf(showAditionalColumns));
			
			if(AppSysParamsUtil.isLogicalCabinClassEnabled()){
				parameters.put("CABINCLASS", "Y");
			}
			
			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", image);
				reportTempleteRelativePath = getReportTemplateRelativeLocation(reptemplateTotal);
			} else if (value.trim().equals("PDF")) {
				image = getReportTemplate(strLogo);
				parameters.put("IMG", image);
				reportTempleteRelativePath = getReportTemplateRelativeLocation(reptemplateTotal);
			} else if (value.trim().equals("EXCEL")) {
				parameters.put("SHOW_TOTAL_COLUMNS", "FALSE");
				reportTempleteRelativePath = getReportTemplateRelativeLocation(reptemplateTotal);
			} else if (value.trim().equals("CSV")) {
				parameters.put("SHOW_TOTAL_COLUMNS", "FALSE");
				reportTempleteRelativePath = getReportTemplateRelativeLocation(reptemplateTotal);
			}
		} catch (Exception e) {
			throw new ModuleRuntimeException(e, "schedrept.parameter.generation.error");
		}
		return new ReportInputs(parameters, search, reportFormat, reportTempleteRelativePath,
				ScheduleReportUtil.ScheduledReportMetaDataIDs.SEAT_INVENTORY_COLLECTIONS);

	}

}
