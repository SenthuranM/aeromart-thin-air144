package com.isa.thinair.webplatform.core.util;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.opentravel.ota._2003._05.CodeContentType;
import org.opentravel.ota._2003._05.OTACodeTables;

/**
 * Utility for generating java interface loaded with OTA Code tables
 * 
 * @author Mohamed Nasly
 * 
 */
public class OTACodeTablesGenerator {

	/**
	 * @param args
	 *            - args[0] = Abs. directory path for gen-src directory args[1] = Interface Package args[2] = Interface
	 *            Name args[3] = Codes XML filename
	 * @throws JAXBException
	 * @throws IOException
	 */
	public static void main(String[] args) throws JAXBException, IOException {

		JAXBContext jc = JAXBContext.newInstance("org.opentravel.ota._2003._05");
		Unmarshaller u = jc.createUnmarshaller();

		ClassLoader loader = ClassLoader.getSystemClassLoader();
		OTACodeTables otaCodeTables = (OTACodeTables) u.unmarshal(new BufferedInputStream(loader.getResourceAsStream(args[3])));

		String nl = "\n\r";
		StringBuffer otaCodeTablesBuf = new StringBuffer();
		otaCodeTablesBuf.append("package " + args[1] + ";").append(nl);

		otaCodeTablesBuf.append("/**").append(nl).append(" * Generated from ").append(args[3]).append(". DO NOT EDIT MANUALLY.")
				.append(nl).append(" */").append(nl);
		otaCodeTablesBuf.append("public interface " + args[2] + " {").append(nl);
		ArrayList<String> leafCodeNamesCol = new ArrayList<String>();
		String leafCode = "";
		String codeTableName = "";
		if (otaCodeTables != null) {
			for (OTACodeTables.OTACodeTable otaCodeTable : otaCodeTables.getOTACodeTable()) {
				codeTableName = constructName(otaCodeTable.getName()) + "_" + otaCodeTable.getNameCode();
				leafCodeNamesCol.clear();
				for (OTACodeTables.OTACodeTable.Codes.Code code : otaCodeTable.getCodes().getCode()) {
					for (CodeContentType codeContent : code.getCodeContents().getCodeContent()) {

						leafCode = getUniqueName(getUpperCase(codeContent.getName()), leafCodeNamesCol);
						otaCodeTablesBuf.append("     public static final String " + codeTableName + "_" + leafCode)
								.append(" = \"").append(otaCodeTable.getNameCode() + "." + code.getValue()).append("\";")
								.append(nl);
						leafCodeNamesCol.add(leafCode);
					}
				}
			}
		}
		otaCodeTablesBuf.append("}").append(nl);

		String buildTargetDirPath = args[0];

		BufferedWriter out = new BufferedWriter(new FileWriter(buildTargetDirPath + "/" + (args[1]).replace('.', '/') + "/"
				+ args[2] + ".java"));
		out.write(otaCodeTablesBuf.toString());
		out.close();

		System.out.println("Generated class " + args[1] + "." + args[2] + ".java");
	}

	private static String constructName(String var) {
		String updated = var.toLowerCase();
		char[] charArr = updated.toCharArray();
		boolean capitalizeNextCharacter = false;
		StringBuffer name = new StringBuffer();
		for (int i = 0; i < charArr.length; i++) {
			if (capitalizeNextCharacter || i == 0) {
				name.append(Character.toString(charArr[i]).toUpperCase());
			} else {
				name.append(charArr[i]);
			}
			if (charArr[i] == ' ') {
				capitalizeNextCharacter = true;
			} else {
				capitalizeNextCharacter = false;
			}
		}

		return name.toString().replace(" ", "");
	}

	private static String getUpperCase(String var) {
		String updated = var.toUpperCase().replace("/", "__").replace('-', '_').replace("(", "").replace(")", "")
				.replace(',', '_').replace(':', '_').replace('.', '_').replace('?', '_').replace("'", "").replace("%", "PERCENT")
				.replace(' ', '_');
		if (updated.startsWith("0") || updated.startsWith("1") || updated.startsWith("2") || updated.startsWith("3")
				|| updated.startsWith("4") || updated.startsWith("5") || updated.startsWith("6") || updated.startsWith("7")
				|| updated.startsWith("8") || updated.startsWith("9")) {
			updated = "_" + updated;
		}
		return updated;

	}

	private static String getUniqueName(String leafCode, ArrayList<String> leafCodesCol) {
		if (leafCodesCol.contains(leafCode)) {
			leafCode = leafCode + "_";
			return getUniqueName(leafCode, leafCodesCol);
		} else {
			return leafCode;
		}
	}
}
