package com.isa.thinair.webplatform.core.commons;

import java.util.ArrayList;
import java.util.List;

public enum ForceConfirmedPayStatus {
	
	NOT_PAID("N"), PARTIALLY_PAID("P");

	private final String status;

	ForceConfirmedPayStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public static List<String> getForceConfirmedPayStatus() {

		List<String> statusTypes = new ArrayList<String>();

		for (ForceConfirmedPayStatus status : ForceConfirmedPayStatus.values()) {
			statusTypes.add(status.getStatus());
		}

		return statusTypes;
	}

}
