package com.isa.thinair.webplatform.core.service;

import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.platform.api.DefaultModuleConfig;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

public class ModuleServiceLocator {

	private static final Log log = LogFactory.getLog(ModuleServiceLocator.class);

	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}
	

}
