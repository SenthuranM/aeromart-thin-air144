package com.isa.thinair.webplatform.core.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.AirschedulesConstants;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.airsecurity.api.utils.AirsecurityConstants;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.crypto.api.service.CryptoServiceBD;
import com.isa.thinair.crypto.api.utils.CryptoConstants;
import com.isa.thinair.lccclient.api.service.LCCReservationBD;
import com.isa.thinair.lccclient.api.utils.LccclientConstants;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.utils.PaymentbrokerConstants;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.reporting.api.service.DataExtractionBD;
import com.isa.thinair.reporting.api.utils.ReportingConstants;
import com.isa.thinair.reportingframework.api.service.ReportingFrameworkBD;
import com.isa.thinair.reportingframework.api.utils.ReportingframeworkConstants;
import com.isa.thinair.webplatform.api.utils.WebplatformConstants;
import com.isa.thinair.webplatform.core.schedrept.SRPComposer;
import com.isa.thinair.wsclient.api.service.WSClientBD;
import com.isa.thinair.wsclient.api.utils.WsclientConstants;



public class WPModuleUtils {

	private static Log log = LogFactory.getLog(WPModuleUtils.class);

	/**
	 * Returns IModule instance for web platform module
	 * 
	 * @return
	 */
	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule(WebplatformConstants.MODULE_NAME);
	}

	private static IServiceDelegate lookupServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getWPModuleConfig(), "webplatform",
				"webplatform.config.dependencymap.invalid");
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getWPModuleConfig(), "webplatform",
				"webplatform.config.dependencymap.invalid");
	}

	/**
	 * Returns SegmentBD.
	 * 
	 * @return
	 */
	public static SegmentBD getResSegmentBD() {
		return (SegmentBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, SegmentBD.SERVICE_NAME);
	}

	/**
	 * Returns PassengerBD
	 * 
	 * @return
	 */
	public static PassengerBD getResPassengerBD() {
		return (PassengerBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, PassengerBD.SERVICE_NAME);
	}

	/**
	 * Returns WSClientBD.
	 * 
	 * @return
	 */
	public static WSClientBD getWSClientBD() {
		return (WSClientBD) lookupEJB3Service(WsclientConstants.MODULE_NAME, WSClientBD.SERVICE_NAME);
	}

	/**
	 * Returns ReservationBD.
	 * 
	 * @return
	 */
	public static ReservationBD getReservationBD() {
		return (ReservationBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationBD.SERVICE_NAME);
	}

	/**
	 * Returns ReservationQueryBD.
	 * 
	 * @return
	 */
	public static ReservationQueryBD getResQueryBD() {
		return (ReservationQueryBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationQueryBD.SERVICE_NAME);
	}

	/**
	 * Returns SecurityBD.
	 * 
	 * @return
	 */
	public static SecurityBD getSecurityBD() {
		return (SecurityBD) lookupEJB3Service(AirsecurityConstants.MODULE_NAME, SecurityBD.SERVICE_NAME);
	}

	/**
	 * Returns SecurityBD.
	 * 
	 * @return
	 */
	public static PaymentBrokerBD getPaymentBrokerBD() {
		return (PaymentBrokerBD) lookupEJB3Service(PaymentbrokerConstants.MODULE_NAME, PaymentBrokerBD.SERVICE_NAME);
	}

	public static LCCReservationBD getLCCReservationBD() {
		return (LCCReservationBD) lookupEJB3Service(LccclientConstants.MODULE_NAME, LCCReservationBD.SERVICE_NAME);
	}

	/**
	 * Returns WPModuleConfig
	 * 
	 * @return
	 */
	public static WPModuleConfig getWPModuleConfig() {
		return (WPModuleConfig) getInstance().getModuleConfig();
	}

	public static CryptoServiceBD getCryptoServiceBD() {
		return (CryptoServiceBD) lookupServiceBD(CryptoConstants.MODULE_NAME, CryptoConstants.BDKeys.CRYPTO_SERVICE);
	}

	public static CommonMasterBD getCommonMasterBD() {
		return (CommonMasterBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, CommonMasterBD.SERVICE_NAME);
	}

	public static AirportBD getAirportBD() {
		return (AirportBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, AirportBD.SERVICE_NAME);

	}

	public final static FlightBD getFlightServiceBD() {
		return (FlightBD) lookupEJB3Service(AirschedulesConstants.MODULE_NAME, FlightBD.SERVICE_NAME);
	}

	public final static ReportingFrameworkBD getReportingFrameworkBD() {
		return (ReportingFrameworkBD) lookupServiceBD(ReportingframeworkConstants.MODULE_NAME,
				ReportingframeworkConstants.BDKeys.REPORTINGFRAMEWORK_SERVICE);
	}

	
	public static SRPComposer getSRPComposer(String reportName) {
		SRPComposer composer = (SRPComposer) LookupServiceFactory.getInstance()
				.getBean("isa:base://modules/webplatform?id=" + reportName);
		return composer;
	}

	public final static DataExtractionBD getDataExtractionBD() {

		DataExtractionBD reprotingDelegate = null;
		try {
			reprotingDelegate = (DataExtractionBD) lookupEJB3Service(ReportingConstants.MODULE_NAME,
					DataExtractionBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating DataExtractionBD", e);
			throw new ModuleRuntimeException(e, "module.lookup.failed", ReportingConstants.MODULE_NAME);
		}
		return reprotingDelegate;
	}
}
