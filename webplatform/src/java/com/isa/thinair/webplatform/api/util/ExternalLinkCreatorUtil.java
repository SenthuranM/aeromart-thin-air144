package com.isa.thinair.webplatform.api.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.dto.CarLinkParamsTO;
import com.isa.thinair.webplatform.api.dto.HolidaysLinkParamsTO;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;

/**
 * External Link Generator for Octupus holidays
 * 
 * @author Duminda G
 * @since Nov 07, 2008
 * @ver 0.1
 */
public class ExternalLinkCreatorUtil {

	private static Log log = LogFactory.getLog(ExternalLinkCreatorUtil.class);

	public static String createLink(HolidaysLinkParamsTO holidaysLinkParamsTO) {
		try {
			// Set Checkin date, Checkout date and City
			setHotelDestination(holidaysLinkParamsTO);
			return link(fillHolidaysMap(holidaysLinkParamsTO), holidaysLinkParamsTO.getStaticURL(), ";");
		} catch (ModuleException e) {
			log.error(e);
			return holidaysLinkParamsTO.getStaticURL();
		}
	}

	public static String createCarLink(CarLinkParamsTO carLinkParamsTO) {
		return link(fillCarMap(carLinkParamsTO), carLinkParamsTO.getStaticURL(), "&");
	}
	
	public static String createCarLinkForManageBooking(Collection<FlightInfoTO> flights, CarLinkParamsTO carLinkParamsTO) {
		try {
			return createCarLink(setCarDestination(flights, carLinkParamsTO));
		} catch (ModuleException e) {
			log.error(e);
			return carLinkParamsTO.getStaticURL();
		}
	}

	private static void setHotelDestination(HolidaysLinkParamsTO holidaysLinkParamsTO) throws ModuleException {

		// Get Segment Collection & Filter out CNF Future Segments
		Map<Integer, Object> mapReservationSegments = new HashMap<Integer, Object>();
		// Collections.sor
		ArrayList reservationSegmentDTOs = new ArrayList(holidaysLinkParamsTO.getSegmentView());

		// Sort based on seg_sequence and date
		Collections.sort(reservationSegmentDTOs);

		log.debug("Size of Segments: " + reservationSegmentDTOs.size());
		int segmentSequence = 0; // 0,1,2,3...........

		for (Object object : reservationSegmentDTOs) {

			if (object instanceof ReservationSegmentDTO) {
				ReservationSegmentDTO reservationSegmentDTO = (ReservationSegmentDTO) object;

				// Considering confirmed unflown segments
				if (reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)
						&& reservationSegmentDTO.getDepartureDate().after(DateUtil.getCurrentZuluDateTime())) {
					mapReservationSegments.put(segmentSequence++, reservationSegmentDTO);
				}
			} else if (object instanceof LCCClientReservationSegment) {
				LCCClientReservationSegment reservationSegmentDTO = (LCCClientReservationSegment) object;
				// Considering confirmed unflown segments
				if (reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)
						&& reservationSegmentDTO.getDepartureDate().after(DateUtil.getCurrentZuluDateTime())) {
					mapReservationSegments.put(segmentSequence++, reservationSegmentDTO);
				}
			}

		}

		// Handle Single Segment ====> One Way booking. Destination, Check-in on Arrival Date, No of Nights 1

		if (mapReservationSegments.size() == 1) { // Definitely one sector booking.
			Object obj = mapReservationSegments.get(0);
			holidaysLinkParamsTO.setNights("1"); // Single oneway booking will be only for one night

			if (obj instanceof ReservationSegmentDTO) {
				ReservationSegmentDTO reservationSegmentDTO = (ReservationSegmentDTO) obj;
				holidaysLinkParamsTO.setCheckin_monthday(getDate(reservationSegmentDTO.getArrivalDate()));
				holidaysLinkParamsTO.setCheckin_year_month(getYearMonth(reservationSegmentDTO.getArrivalDate()));
				Date checkOutDate = addThreeDays(reservationSegmentDTO.getArrivalDate());
				holidaysLinkParamsTO.setCheckout_monthday(getDate(checkOutDate));
				holidaysLinkParamsTO.setCheckout_year_month(getYearMonth(checkOutDate));
				setCityAndCountry(holidaysLinkParamsTO, reservationSegmentDTO.getSegmentCode().substring(
						reservationSegmentDTO.getSegmentCode().length() - 3));
				// Passing last 3 chars of segment code as destination ex: SHJ/CMB 'CMB' , CMB/SHJ/BAH 'BAH'
			} else if (obj instanceof LCCClientReservationSegment) {
				LCCClientReservationSegment reservationSegmentDTO = (LCCClientReservationSegment) obj;
				holidaysLinkParamsTO.setCheckin_monthday(getDate(reservationSegmentDTO.getArrivalDate()));
				holidaysLinkParamsTO.setCheckin_year_month(getYearMonth(reservationSegmentDTO.getArrivalDate()));
				Date checkOutDate = addThreeDays(reservationSegmentDTO.getArrivalDate());
				holidaysLinkParamsTO.setCheckout_monthday(getDate(checkOutDate));
				holidaysLinkParamsTO.setCheckout_year_month(getYearMonth(checkOutDate));
				setCityAndCountry(holidaysLinkParamsTO, reservationSegmentDTO.getSegmentCode().substring(
						reservationSegmentDTO.getSegmentCode().length() - 3));
				// Passing last 3 chars of segment code as destination ex: SHJ/CMB 'CMB' , CMB/SHJ/BAH 'BAH'
			}

		}

		// Handle Two Segments ===>
		// 1. Continuing Segments. --> Connection Booking. Destination will be final Destination. Check-in On final on
		// Arrival, no of nights 1
		// 2. Origin and last Destination Equal and return flag available
		else if (mapReservationSegments.size() == 2) {
			Object firstObj = reservationSegmentDTOs.get(0);
			Object secondObj = reservationSegmentDTOs.get(1);

			if (firstObj instanceof ReservationSegmentDTO && secondObj instanceof ReservationSegmentDTO) {

				ReservationSegmentDTO firstReservationSegmentDTO = (ReservationSegmentDTO) firstObj;
				ReservationSegmentDTO secondReservationSegmentDTO = (ReservationSegmentDTO) secondObj;
				// Identifying return booking
				if (firstReservationSegmentDTO.getDestination().equalsIgnoreCase(secondReservationSegmentDTO.getOrigin())
						&& firstReservationSegmentDTO.getOrigin().equalsIgnoreCase(secondReservationSegmentDTO.getDestination())
						&& secondReservationSegmentDTO.getReturnFlag().equalsIgnoreCase(
								ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE)) {

					// RETURN Booking: First segment destination will be used for hotel
					holidaysLinkParamsTO.setCheckin_monthday(getDate(firstReservationSegmentDTO.getArrivalDate()));
					holidaysLinkParamsTO.setCheckin_year_month(getYearMonth(firstReservationSegmentDTO.getArrivalDate()));
					holidaysLinkParamsTO.setCheckout_monthday(getDate(secondReservationSegmentDTO.getDepartureDate()));
					holidaysLinkParamsTO.setCheckout_year_month(getYearMonth(secondReservationSegmentDTO.getDepartureDate()));
					// Return booking up to return date
					setCityAndCountry(holidaysLinkParamsTO, firstReservationSegmentDTO.getSegmentCode().substring(
							firstReservationSegmentDTO.getSegmentCode().length() - 3));
					// Passing last 3 chars of segment code as destination ex: SHJ/CMB 'CMB' , CMB/SHJ/BAH 'BAH'
				}

				// Identifying connection bookings
				else if (firstReservationSegmentDTO.getDestination().equalsIgnoreCase(secondReservationSegmentDTO.getOrigin())
						&& !firstReservationSegmentDTO.getOrigin().equalsIgnoreCase(secondReservationSegmentDTO.getDestination())
						&& secondReservationSegmentDTO.getReturnFlag().equalsIgnoreCase(
								ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE)) {
					// CONNECTION Booking: Second segment destination will be used for hotel
					holidaysLinkParamsTO.setCheckin_monthday(getDate(secondReservationSegmentDTO.getArrivalDate()));
					holidaysLinkParamsTO.setCheckin_year_month(getYearMonth(secondReservationSegmentDTO.getArrivalDate()));
					Date checkOutDate = addThreeDays(secondReservationSegmentDTO.getArrivalDate());
					holidaysLinkParamsTO.setCheckout_monthday(getDate(checkOutDate));
					holidaysLinkParamsTO.setCheckout_year_month(getYearMonth(checkOutDate));
					setCityAndCountry(holidaysLinkParamsTO, secondReservationSegmentDTO.getSegmentCode().substring(
							secondReservationSegmentDTO.getSegmentCode().length() - 3));
					// Passing last 3 chars of segment code as destination ex: SHJ/CMB 'CMB' , CMB/SHJ/BAH 'BAH'
				}
			}

			if (firstObj instanceof LCCClientReservationSegment && secondObj instanceof LCCClientReservationSegment) {

				LCCClientReservationSegment firstReservationSegmentDTO = (LCCClientReservationSegment) firstObj;
				LCCClientReservationSegment secondReservationSegmentDTO = (LCCClientReservationSegment) secondObj;

				// Identifying return booking
				if (firstReservationSegmentDTO.getArrivalAirportName().equalsIgnoreCase(
						secondReservationSegmentDTO.getDepartureAirportName())
						&& firstReservationSegmentDTO.getDepartureAirportName().equalsIgnoreCase(
								secondReservationSegmentDTO.getArrivalAirportName())
						&& secondReservationSegmentDTO.getReturnFlag().equalsIgnoreCase(
								ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE)) {

					// RETURN Booking: First segment destination will be used for hotel
					holidaysLinkParamsTO.setCheckin_monthday(getDate(firstReservationSegmentDTO.getArrivalDate()));
					holidaysLinkParamsTO.setCheckin_year_month(getYearMonth(firstReservationSegmentDTO.getArrivalDate()));
					holidaysLinkParamsTO.setCheckout_monthday(getDate(secondReservationSegmentDTO.getDepartureDate()));
					holidaysLinkParamsTO.setCheckout_year_month(getYearMonth(secondReservationSegmentDTO.getDepartureDate()));
					setCityAndCountry(holidaysLinkParamsTO, firstReservationSegmentDTO.getSegmentCode().substring(
							firstReservationSegmentDTO.getSegmentCode().length() - 3));
					// Passing last 3 chars of segment code as destination ex: SHJ/CMB 'CMB' , CMB/SHJ/BAH 'BAH'
				}

				// Identifying connection bookings
				else if (firstReservationSegmentDTO.getArrivalAirportName().equalsIgnoreCase(
						secondReservationSegmentDTO.getDepartureAirportName())
						&& !firstReservationSegmentDTO.getDepartureAirportName().equalsIgnoreCase(
								secondReservationSegmentDTO.getArrivalAirportName())
						&& secondReservationSegmentDTO.getReturnFlag().equalsIgnoreCase(
								ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE)) {
					// CONNECTION Booking: Second segment destination will be used for hotel
					holidaysLinkParamsTO.setCheckin_monthday(getDate(secondReservationSegmentDTO.getArrivalDate()));
					holidaysLinkParamsTO.setCheckin_year_month(getYearMonth(secondReservationSegmentDTO.getArrivalDate()));
					Date checkOutDate = addThreeDays(secondReservationSegmentDTO.getArrivalDate());
					holidaysLinkParamsTO.setCheckout_monthday(getDate(checkOutDate));
					holidaysLinkParamsTO.setCheckout_year_month(getYearMonth(checkOutDate));
					setCityAndCountry(holidaysLinkParamsTO, secondReservationSegmentDTO.getSegmentCode().substring(
							secondReservationSegmentDTO.getSegmentCode().length() - 3));
					// Passing last 3 chars of segment code as destination ex: SHJ/CMB 'CMB' , CMB/SHJ/BAH 'BAH'
				}

			}

		}

		else if (mapReservationSegments.size() == 4) {
			// Identifying connection return booking with segment fares
			Object firstObj = reservationSegmentDTOs.get(0);
			Object secondObj = reservationSegmentDTOs.get(1);
			Object thirdObj = reservationSegmentDTOs.get(2);
			Object fourthObj = reservationSegmentDTOs.get(3);

			if (firstObj instanceof ReservationSegmentDTO && secondObj instanceof ReservationSegmentDTO
					&& thirdObj instanceof ReservationSegmentDTO && fourthObj instanceof ReservationSegmentDTO) {

				ReservationSegmentDTO firstReservationSegmentDTO = (ReservationSegmentDTO) firstObj;
				ReservationSegmentDTO secondReservationSegmentDTO = (ReservationSegmentDTO) secondObj;
				ReservationSegmentDTO thirdReservationSegmentDTO = (ReservationSegmentDTO) thirdObj;
				ReservationSegmentDTO fourReservationSegmentDTO = (ReservationSegmentDTO) fourthObj;

				// If first two segments are connection.
				if (firstReservationSegmentDTO.getDestination().equalsIgnoreCase(secondReservationSegmentDTO.getOrigin())
						&& !firstReservationSegmentDTO.getOrigin().equalsIgnoreCase(secondReservationSegmentDTO.getDestination())
						&& secondReservationSegmentDTO.getReturnFlag().equalsIgnoreCase(
								ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE)) {

					// Second and 3rd become return, assume as connection return
					if (secondReservationSegmentDTO.getDestination().equalsIgnoreCase(thirdReservationSegmentDTO.getOrigin())
							&& secondReservationSegmentDTO.getOrigin().equalsIgnoreCase(
									thirdReservationSegmentDTO.getDestination())
							&& thirdReservationSegmentDTO.getReturnFlag().equalsIgnoreCase(
									ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE)) {

						// CONNECTION RETURN Booking: Second segment destination will be used for hotel
						holidaysLinkParamsTO.setCheckin_monthday(getDate(secondReservationSegmentDTO.getArrivalDate()));
						holidaysLinkParamsTO.setCheckin_year_month(getYearMonth(secondReservationSegmentDTO.getArrivalDate()));
						holidaysLinkParamsTO.setCheckout_monthday(getDate(thirdReservationSegmentDTO.getDepartureDate()));
						holidaysLinkParamsTO.setCheckout_year_month(getYearMonth(thirdReservationSegmentDTO.getDepartureDate()));
						// Single oneway booking will be only for one night
						setCityAndCountry(holidaysLinkParamsTO, secondReservationSegmentDTO.getSegmentCode().substring(
								secondReservationSegmentDTO.getSegmentCode().length() - 3));
						// Passing last 3 chars of segment code as destination ex: SHJ/CMB 'CMB' , CMB/SHJ/BAH 'BAH'
					}

				}

			}

			if (firstObj instanceof LCCClientReservationSegment && secondObj instanceof LCCClientReservationSegment
					&& thirdObj instanceof LCCClientReservationSegment && fourthObj instanceof LCCClientReservationSegment) {

				LCCClientReservationSegment firstReservationSegmentDTO = (LCCClientReservationSegment) firstObj;
				LCCClientReservationSegment secondReservationSegmentDTO = (LCCClientReservationSegment) secondObj;
				LCCClientReservationSegment thirdReservationSegmentDTO = (LCCClientReservationSegment) thirdObj;
				LCCClientReservationSegment fourReservationSegmentDTO = (LCCClientReservationSegment) fourthObj;

				// If first two segments are connection.
				if (firstReservationSegmentDTO.getArrivalAirportName().equalsIgnoreCase(
						secondReservationSegmentDTO.getDepartureAirportName())
						&& !firstReservationSegmentDTO.getDepartureAirportName().equalsIgnoreCase(
								secondReservationSegmentDTO.getArrivalAirportName())
						&& secondReservationSegmentDTO.getReturnFlag().equalsIgnoreCase(
								ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE)) {

					// Second and 3rd become return, assume as connection return
					if (secondReservationSegmentDTO.getArrivalAirportName().equalsIgnoreCase(
							thirdReservationSegmentDTO.getDepartureAirportName())
							&& secondReservationSegmentDTO.getDepartureAirportName().equalsIgnoreCase(
									thirdReservationSegmentDTO.getArrivalAirportName())
							&& thirdReservationSegmentDTO.getReturnFlag().equalsIgnoreCase(
									ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE)) {

						// CONNECTION RETURN Booking: Second segment destination will be used for hotel
						holidaysLinkParamsTO.setCheckin_monthday(getDate(secondReservationSegmentDTO.getArrivalDate()));
						holidaysLinkParamsTO.setCheckin_year_month(getYearMonth(secondReservationSegmentDTO.getArrivalDate()));
						holidaysLinkParamsTO.setCheckout_monthday(getDate(thirdReservationSegmentDTO.getDepartureDate()));
						holidaysLinkParamsTO.setCheckout_year_month(getYearMonth(thirdReservationSegmentDTO.getDepartureDate()));
						// Single oneway booking will be only for one night
						setCityAndCountry(holidaysLinkParamsTO, secondReservationSegmentDTO.getSegmentCode().substring(
								secondReservationSegmentDTO.getSegmentCode().length() - 3));
						// Passing last 3 chars of segment code as destination ex: SHJ/CMB 'CMB' , CMB/SHJ/BAH 'BAH'
					}

				}
			}

		}
	}
	
	private static CarLinkParamsTO setCarDestination(Collection<FlightInfoTO> flights, CarLinkParamsTO carLinkParamsTO)
			throws ModuleException {
		// Get Segment Collection & Filter out CNF Future Segments
		Map<Integer, Object> mapReservationSegments = new HashMap<Integer, Object>();
		// Collections.sor
		ArrayList reservationSegmentDTOs = new ArrayList(flights);

		// Sort based on seg_sequence and date
		Collections.sort(reservationSegmentDTOs);

		log.debug("Size of Segments: " + reservationSegmentDTOs.size());
		int segmentSequence = 0; // 0,1,2,3...........

		for (Object object : reservationSegmentDTOs) {
			if (object instanceof FlightInfoTO) {
				FlightInfoTO reservationSegmentDTO = (FlightInfoTO) object;
				// Considering confirmed unflown segments
				if (reservationSegmentDTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)
						&& new Date(reservationSegmentDTO.getDepartureDateLong()).after(DateUtil.getCurrentZuluDateTime())) {
					mapReservationSegments.put(segmentSequence++, reservationSegmentDTO);
				}
			}
		}

		// Handle Single Segment ====> One Way booking. Destination, Check-in on Arrival Date, No of Nights 1
		if (mapReservationSegments.size() == 1) { // Definitely one sector booking.
			Object obj = mapReservationSegments.get(0);
			if (obj instanceof FlightInfoTO) {
				FlightInfoTO reservationSegmentDTO = (FlightInfoTO) obj;
				setCarData(reservationSegmentDTO, carLinkParamsTO);
				carLinkParamsTO.setRtDateTime(carLinkParamsTO.getPkDateTime());
			}
		}

		// Handle Two Segments ===>
		// 1. Continuing Segments. --> Connection Booking. Destination will be final Destination. Check-in On final on
		// Arrival, no of nights 1
		// 2. Origin and last Destination Equal and return flag available
		else if (mapReservationSegments.size() == 2) {
			Object firstObj = reservationSegmentDTOs.get(0);
			Object secondObj = reservationSegmentDTOs.get(1);

			if (firstObj instanceof FlightInfoTO && secondObj instanceof FlightInfoTO) {

				FlightInfoTO firstReservationSegmentDTO = (FlightInfoTO) firstObj;
				FlightInfoTO secondReservationSegmentDTO = (FlightInfoTO) secondObj;
				// Identifying return booking
				if (firstReservationSegmentDTO.getArrival().equalsIgnoreCase(secondReservationSegmentDTO.getDeparture())
						&& firstReservationSegmentDTO.getDeparture().equalsIgnoreCase(secondReservationSegmentDTO.getArrival())
						&& secondReservationSegmentDTO.isReturnFlag()) {
					setCarData(firstReservationSegmentDTO, carLinkParamsTO);
					carLinkParamsTO.setRtDateTime(formatDateTimeYYYYMMDDHHMM(secondReservationSegmentDTO.getDepartureDateLong()));
				}

				// Identifying connection bookings
				else if (firstReservationSegmentDTO.getArrival().equalsIgnoreCase(secondReservationSegmentDTO.getDeparture())
						&& !firstReservationSegmentDTO.getDeparture().equalsIgnoreCase(secondReservationSegmentDTO.getArrival())
						&& !secondReservationSegmentDTO.isReturnFlag()) {
					// CONNECTION Booking: Second segment destination will be used for hotel
					setCarData(secondReservationSegmentDTO, carLinkParamsTO);
					carLinkParamsTO.setRtDateTime(carLinkParamsTO.getPkDateTime());
				}
			}

		}

		else if (mapReservationSegments.size() == 4) {
			// Identifying connection return booking with segment fares
			Object firstObj = reservationSegmentDTOs.get(0);
			Object secondObj = reservationSegmentDTOs.get(1);
			Object thirdObj = reservationSegmentDTOs.get(2);
			Object fourthObj = reservationSegmentDTOs.get(3);

			if (firstObj instanceof FlightInfoTO && secondObj instanceof FlightInfoTO && thirdObj instanceof FlightInfoTO
					&& fourthObj instanceof FlightInfoTO) {

				FlightInfoTO firstReservationSegmentDTO = (FlightInfoTO) firstObj;
				FlightInfoTO secondReservationSegmentDTO = (FlightInfoTO) secondObj;
				FlightInfoTO thirdReservationSegmentDTO = (FlightInfoTO) thirdObj;
				FlightInfoTO fourReservationSegmentDTO = (FlightInfoTO) fourthObj;

				// If first two segments are connection.
				if (firstReservationSegmentDTO.getArrival().equalsIgnoreCase(secondReservationSegmentDTO.getDeparture())
						&& !firstReservationSegmentDTO.getDeparture().equalsIgnoreCase(secondReservationSegmentDTO.getArrival())
						&& !secondReservationSegmentDTO.isReturnFlag()) {

					// Second and 3rd become return, assume as connection return
					if (secondReservationSegmentDTO.getArrival().equalsIgnoreCase(thirdReservationSegmentDTO.getDeparture())
							&& secondReservationSegmentDTO.getDeparture().equalsIgnoreCase(
									thirdReservationSegmentDTO.getArrival()) && thirdReservationSegmentDTO.isReturnFlag()) {
						// CONNECTION RETURN Booking: Second segment destination will be used for hotel
						setCarData(secondReservationSegmentDTO, carLinkParamsTO);
						carLinkParamsTO.setRtDateTime(formatDateTimeYYYYMMDDHHMM(thirdReservationSegmentDTO
								.getDepartureDateLong()));
					}

				}

			}
		}
		return carLinkParamsTO;
	}

	private static void setCarData(FlightInfoTO reservationSegmentDTO, CarLinkParamsTO carLinkParamsTO) throws ModuleException {
		String locCode = "";
		String pkDateTime = "";
		long pkDateTimeLong = reservationSegmentDTO.getArrivalDateLong() + 7200000; // 2 hours after arrival time
		pkDateTime = formatDateTimeYYYYMMDDHHMM(pkDateTimeLong);
		carLinkParamsTO.setPkDateTime(pkDateTime);
		String[] locCodes = reservationSegmentDTO.getSegmentShortCode().split("/");
		locCode = locCodes[locCodes.length - 1];
		carLinkParamsTO.setLocCode(locCode);
		Collection<String> colAirPortCodes = new ArrayList<String>();
		colAirPortCodes.add(locCode);
		Map<String, CachedAirportDTO> mapAirports = new HashMap<String, CachedAirportDTO>();
		if (colAirPortCodes.size() != mapAirports.size()) {
			mapAirports = WPModuleUtils.getAirportBD().getCachedAllAirportMap(colAirPortCodes);
			carLinkParamsTO.setResidencyId(mapAirports.get(locCode) != null ? mapAirports.get(locCode).getCountryCode() : "");
		}
	}

	public static String formatDateTimeYYYYMMDDHHMM(long timeLong) {
		Date date = new Date(timeLong);
		if (date != null) {
			SimpleDateFormat formetter = new SimpleDateFormat("yyyyMMddHHmm");
			return formetter.format(date);
		} else {
			return null;
		}
	}

	private static String getDate(Date date) {
		GregorianCalendar day = new GregorianCalendar();
		day.setTime(date);
		return Integer.toString(day.get(GregorianCalendar.DATE));
	}

	private static String getYearMonth(Date date) {
		GregorianCalendar day = new GregorianCalendar();
		day.setTime(date);
		int year = day.get(GregorianCalendar.YEAR);
		int month = day.get(GregorianCalendar.MONTH) + 1;
		String monthStr = Integer.toString(month);
		if (month < 10) {
			monthStr = "0" + Integer.toString(month);
		}
		return year + "-" + monthStr;
	}

	private static Date addThreeDays(Date date) {
		GregorianCalendar day = new GregorianCalendar();
		day.setTime(date);
		day.add(GregorianCalendar.DATE, 3);
		return day.getTime();
	}

	private static Map fillHolidaysMap(HolidaysLinkParamsTO holidaysLinkParamsTO) {
		Map paramSet = new LinkedHashMap();
		paramSet.put("checkin_monthday", holidaysLinkParamsTO.getCheckin_monthday());
		paramSet.put("checkin_year_month", holidaysLinkParamsTO.getCheckin_year_month());
		paramSet.put("checkout_monthday", holidaysLinkParamsTO.getCheckout_monthday());
		paramSet.put("checkout_year_month", holidaysLinkParamsTO.getCheckout_year_month());
		paramSet.put("city", holidaysLinkParamsTO.getCity());
		return paramSet;
	}

	private static Map fillCarMap(CarLinkParamsTO carLinkParamsTO) {
		Map paramSet = new LinkedHashMap();
		paramSet.put("clientId", carLinkParamsTO.getClientId());
		paramSet.put("pkDateTime", carLinkParamsTO.getPkDateTime());
		paramSet.put("rtDateTime", carLinkParamsTO.getRtDateTime());
		paramSet.put("locCode", carLinkParamsTO.getLocCode());
		paramSet.put("lang", carLinkParamsTO.getLang());
		paramSet.put("residencyId", carLinkParamsTO.getResidencyId());
		paramSet.put("currency", carLinkParamsTO.getCurrency());
		paramSet.put("orderId", carLinkParamsTO.getOrderId());		
		return paramSet;
	}

	/**
	 * @deprecated refactored for efficient identification.
	 * @param segments
	 * @return
	 * @throws ModuleException
	 */
	private static String[] setCheckinDetails(Collection segments) throws ModuleException {
		Map<String, Date> segMap = new LinkedHashMap();
		String seg = null;
		String startSeg = null;
		boolean isGetfirstSeg = false;
		for (Iterator iter = segments.iterator(); iter.hasNext();) {
			// Handle ReservationSegmentDTO
			Object object = (Object) iter.next();
			if (object instanceof ReservationSegmentDTO) {
				ReservationSegmentDTO reservationSegmentDTO = (ReservationSegmentDTO) object;
				Date arrivalDate = reservationSegmentDTO.getZuluArrivalDate();

				String[] segment = reservationSegmentDTO.getSegmentCode().split("\\/");
				if (segments.size() > 1 && !isGetfirstSeg) { // two segments
					startSeg = segment[0];
					isGetfirstSeg = true;
				}
				// TODO: have to look at this again.
				if (reservationSegmentDTO.getReturnFlag().equals(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE)
						&& segments.size() == 1) { // return flight
					segMap.put(segment[1], arrivalDate);
					break;
				} else if (startSeg != null) {
					if (startSeg.equals(segment[1])) { // two segments looks like a return flight
						segMap.put(segment[0], arrivalDate);
						break;
					} else {
						segMap.put(segment[1], arrivalDate);
					}
				} else {
					segMap.put(segment[1], arrivalDate);
				}
			}
			// LCCClientReservationSegment
			if (object instanceof LCCClientReservationSegment) {
				LCCClientReservationSegment reservationSegmentDTO = (LCCClientReservationSegment) object;
				Date arrivalDate = reservationSegmentDTO.getArrivalDate();

				String[] segment = reservationSegmentDTO.getSegmentCode().split("\\/");
				if (segments.size() > 1 && !isGetfirstSeg) { // two segments
					startSeg = segment[0];
					isGetfirstSeg = true;
				}
				// TODO: have to look at this again.
				if (reservationSegmentDTO.getReturnFlag() != null
						&& reservationSegmentDTO.getReturnFlag()
								.equals(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE) && segments.size() == 1) { // return
					// flight
					segMap.put(segment[1], arrivalDate);
					break;
				} else if (startSeg != null) {
					if (startSeg.equals(segment[1])) { // two segments looks like a return flight
						segMap.put(segment[0], arrivalDate);
						break;
					} else {
						segMap.put(segment[1], arrivalDate);
					}
				} else {
					segMap.put(segment[1], arrivalDate);
				}
			}

		}

		// get last segment
		Date arrivalDate = new Date();
		for (Iterator iter = segMap.keySet().iterator(); iter.hasNext();) {
			seg = (String) iter.next();
			arrivalDate = (Date) segMap.get(seg);
		}

		SimpleDateFormat sdFmt = new SimpleDateFormat("yyyy-MM-dd");
		return setCity(seg, sdFmt.format(arrivalDate));
	}

	private static String[] getCheckinDetails(Collection segments) throws ModuleException {
		Map<String, Date> segMap = new LinkedHashMap();
		String seg = null;
		String startSeg = null;
		boolean isGetfirstSeg = false;
		for (Iterator iter = segments.iterator(); iter.hasNext();) {
			Object object = (Object) iter.next();
			if (object instanceof ReservationSegmentDTO) {
				ReservationSegmentDTO reservationSegmentDTO = (ReservationSegmentDTO) object;
				Date arrivalDate = reservationSegmentDTO.getZuluArrivalDate();

				String[] segment = reservationSegmentDTO.getSegmentCode().split("\\/");
				if (segments.size() > 1 && !isGetfirstSeg) { // two segments
					startSeg = segment[0];
					isGetfirstSeg = true;
				}
				// TODO: have to look at this again.
				if (reservationSegmentDTO.getReturnFlag().equals(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE)
						&& segments.size() == 1) { // return flight
					segMap.put(segment[1], arrivalDate);
					break;
				} else if (startSeg != null) {
					if (startSeg.equals(segment[1])) { // two segments looks like a return flight
						segMap.put(segment[0], arrivalDate);
						break;
					} else {
						segMap.put(segment[1], arrivalDate);
					}
				} else {
					segMap.put(segment[1], arrivalDate);
				}
			}
			// LCCClientReservationSegment
			if (object instanceof LCCClientReservationSegment) {
				LCCClientReservationSegment reservationSegmentDTO = (LCCClientReservationSegment) object;
				Date arrivalDate = reservationSegmentDTO.getArrivalDate();

				String[] segment = reservationSegmentDTO.getSegmentCode().split("\\/");
				if (segments.size() > 1 && !isGetfirstSeg) { // two segments
					startSeg = segment[0];
					isGetfirstSeg = true;
				}
				// TODO: have to look at this again.
				if (reservationSegmentDTO.getReturnFlag() != null
						&& reservationSegmentDTO.getReturnFlag()
								.equals(ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE) && segments.size() == 1) { // return
					// flight
					segMap.put(segment[1], arrivalDate);
					break;
				} else if (startSeg != null) {
					if (startSeg.equals(segment[1])) { // two segments looks like a return flight
						segMap.put(segment[0], arrivalDate);
						break;
					} else {
						segMap.put(segment[1], arrivalDate);
					}
				} else {
					segMap.put(segment[1], arrivalDate);
				}
			}

		}

		// get last segment
		Date arrivalDate = new Date();
		for (Iterator iter = segMap.keySet().iterator(); iter.hasNext();) {
			seg = (String) iter.next();
			arrivalDate = (Date) segMap.get(seg);
		}

		SimpleDateFormat sdFmt = new SimpleDateFormat("yyyy-MM-dd");
		return setCity(seg, sdFmt.format(arrivalDate));
	}

	/**
	 * @deprecated
	 * @param segment
	 * @param checkin
	 * @return
	 */
	private static String[] setCity(String segment, String checkin) {
		try {
			String[] seg = new String[1];
			seg[0] = segment;
			String desination[] = SelectListGenerator.getOctupusCity(seg);
			return new String[] { desination[0], checkin, desination[1] };
		} catch (ModuleException e) {
			log.error(e);
			return new String[] { "", "", "" };
		}
	}

	/**
	 * 
	 * @param holidaysLinkParamsTO
	 * @param segment
	 * @throws ModuleException
	 */
	private static void setCityAndCountry(HolidaysLinkParamsTO holidaysLinkParamsTO, String segment) throws ModuleException {
		 String[] seg = new String[1];
		 seg[0] = segment;
		 String destination[] = SelectListGenerator.getUfiCity(seg);
		 if (destination != null && destination.length > 0) {
			 String city = null;
			 if (destination[0] != null && destination[0].length() > 0) {
				 city = "-" + destination[0];				 
			 } else {
				 String[] hub = new String[1];
				 hub[0] = AppSysParamsUtil.getHubAirport();
				 String hubCity[] = SelectListGenerator.getUfiCity(hub);
				 if (hubCity[0] != null && hubCity[0].length() > 0) {
					 city = "-" + hubCity[0];	
				 }
			 }
			 holidaysLinkParamsTO.setCity(city);
			 if (destination.length > 2 && destination[2] != null && destination[2].length() > 0) {
				 holidaysLinkParamsTO.setAncillaryReminderURL(destination[2]);
			 }
		 }
	}

	private static String link(Map params, String staticUrl, String symbol) {
		StringBuilder url = new StringBuilder();
		// assume "?" part is in the configured url
		// url.append(staticUrl).append("?");
		url.append(staticUrl);

		boolean and = false;
		for (Iterator iter = params.keySet().iterator(); iter.hasNext();) {
			String key = (String) iter.next();
			String value = (String) params.get(key);
			if (value != null) {
				if (and) {
					url.append(symbol);
				}
				url.append(key).append("=").append(value);
				and = true;
			}
		}
		return url.toString();
	}

	private static int calcNumberOfRooms(Collection passengers) {
		int paxCount = 0;
		int numberOfRooms = 0;
		ReservationPax reservationPax;
		for (Iterator iter = passengers.iterator(); iter.hasNext();) {
			Object object = iter.next();
			// Handle ReservationPax
			if (object instanceof ReservationPax) {
				reservationPax = (ReservationPax) object;
				if (!ReservationApiUtils.isInfantType(reservationPax)) {
					paxCount++;
				}
			}
			// For Interline
			// Handle LCCClientReservationPax
			if (object instanceof LCCClientReservationPax) {
				LCCClientReservationPax clientReservationPax = (LCCClientReservationPax) object;
				if (!ReservationInternalConstants.PassengerType.INFANT.equals(clientReservationPax.getPaxType())) {
					paxCount++;
				}
			}
		}
		numberOfRooms = (paxCount / 2) + (paxCount % 2); // just to see whether this works
		return numberOfRooms;
	}
}
