package com.isa.thinair.webplatform.api.v2.util;

import java.io.Serializable;

/**
 * Datapaerser for Jquery multi Select
 * 
 * @author Navod Ediriweera
 * 
 */
public class MultiSelectDataParser implements Serializable {

	private static final long serialVersionUID = 1L;

	private String category;
	private String id;
	private String text;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
