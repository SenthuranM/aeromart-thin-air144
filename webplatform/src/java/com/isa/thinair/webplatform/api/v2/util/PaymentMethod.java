package com.isa.thinair.webplatform.api.v2.util;

public enum PaymentMethod {

	ACCO("ACCO", "On Account Payment"), CASH("CASH", "Cash Payment"), CRED("CRED", "Credit Card Payment"), BSP("BSP",
			"BSP Account payment"), NOPAY("NOPAY", "No payment"), MASHREQ_LOYALTY("MASHREQ_LOYALTY", "MASHREQ LOYALTY"), LMS(
			"LMS", "LMS"), VOUCHER("VOUCHER", "Voucher/Gift Voucher"), OFFLINE("OFFLINE", "Offline Payment");
	private PaymentMethod(String code, String name) {
		this.code = code;
		this.name = name;
	}

	private final String code;
	private final String name;

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static PaymentMethod getPaymentType(String code) {
		for (PaymentMethod paymentType : PaymentMethod.values()) {
			if (paymentType.getCode().equals(code)) {
				return paymentType;
			}
		}
		return null;
	}
}