package com.isa.thinair.webplatform.api.v2.util;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.dto.PayByVoucherInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCCPaymentMetaInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;

public class PaymentAssemblerComposer {
	private LCCClientPaymentAssembler paymentAssembler;

	public PaymentAssemblerComposer(List<LCCClientExternalChgDTO> perPaxExtChgs) {
		this.paymentAssembler = new LCCClientPaymentAssembler();
		paymentAssembler.getPerPaxExternalCharges().addAll(perPaxExtChgs);
	}

	public void addCreditCardPayment(BigDecimal totalPaymentAmount, IPGResponseDTO creditInfo, IPGIdentificationParamsDTO ipgDTO,
			PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp, String lccUniquekey) {

		LCCClientCCPaymentMetaInfo lccClientCCPaymentMetaInfo = new LCCClientCCPaymentMetaInfo();
		lccClientCCPaymentMetaInfo.setPaymentSuccessful(creditInfo.isSuccess());
		lccClientCCPaymentMetaInfo.setPaymentBrokerId(creditInfo.getPaymentBrokerRefNo() == 0 ? null : creditInfo
				.getPaymentBrokerRefNo());
		lccClientCCPaymentMetaInfo.setTemporyPaymentId(creditInfo.getTemporyPaymentId());

		paymentAssembler.addExternalCardPayment(new Integer(creditInfo.getCardType()), creditInfo.getCcLast4Digits(),
				totalPaymentAmount, AppIndicatorEnum.APP_IBE, TnxModeEnum.MAIL_TP_ORDER, lccClientCCPaymentMetaInfo, ipgDTO,
				payCurrencyDTO, paymentTimestamp, lccUniquekey, creditInfo.getAuthorizationCode(), false, null);
	}

	public void addCreditCardPaymentInternal(BigDecimal totalPaymentAmount, IPGResponseDTO creditInfo,
			IPGIdentificationParamsDTO ipgDTO, PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp, String cardNumber,
			String ExpDate, String securityCode, String cardHolderName, String lccUniqueKey) {

		LCCClientCCPaymentMetaInfo lccClientCCPaymentMetaInfo = new LCCClientCCPaymentMetaInfo();
		lccClientCCPaymentMetaInfo.setPaymentSuccessful(creditInfo.isSuccess());
		lccClientCCPaymentMetaInfo.setPaymentBrokerId(creditInfo.getPaymentBrokerRefNo() == 0 ? null : creditInfo
				.getPaymentBrokerRefNo());
		lccClientCCPaymentMetaInfo.setTemporyPaymentId(creditInfo.getTemporyPaymentId());

		paymentAssembler.addInternalCardPayment(new Integer(creditInfo.getCardType()), ExpDate, cardNumber, cardHolderName, "",
				securityCode, totalPaymentAmount, AppIndicatorEnum.APP_IBE, TnxModeEnum.SECURE_3D, ipgDTO, payCurrencyDTO,
				paymentTimestamp, cardHolderName, "", null, lccClientCCPaymentMetaInfo, lccUniqueKey,
				creditInfo.getAuthorizationCode());
	}

	public void addCreditCardPaymentInternal(BigDecimal totalPaymentAmount, IPGResponseDTO creditInfo,
			IPGIdentificationParamsDTO ipgDTO, PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp, String cardNumber,
			String ExpDate, String securityCode, String cardHolderName, String paymentReference, Integer actualPaymentMethod,
			String lccUniqueKey) {

		LCCClientCCPaymentMetaInfo lccClientCCPaymentMetaInfo = new LCCClientCCPaymentMetaInfo();
		lccClientCCPaymentMetaInfo.setPaymentSuccessful(creditInfo.isSuccess());
		lccClientCCPaymentMetaInfo.setPaymentBrokerId(creditInfo.getPaymentBrokerRefNo() == 0 ? null : creditInfo
				.getPaymentBrokerRefNo());
		lccClientCCPaymentMetaInfo.setTemporyPaymentId(creditInfo.getTemporyPaymentId());

		paymentAssembler.addInternalCardPayment(new Integer(creditInfo.getCardType()), ExpDate, cardNumber, cardHolderName, "",
				securityCode, totalPaymentAmount, AppIndicatorEnum.APP_IBE, TnxModeEnum.MAIL_TP_ORDER, ipgDTO, payCurrencyDTO,
				paymentTimestamp, cardHolderName, paymentReference, actualPaymentMethod, lccClientCCPaymentMetaInfo,
				lccUniqueKey, creditInfo.getAuthorizationCode());
	}

	public void addLoyaltyCreditPayment(String loyaltyAgentCode, BigDecimal totalPaymentAmount, String loyaltyAccount,
			PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp) {
		paymentAssembler.addLoyaltyCreditPayment(loyaltyAgentCode, totalPaymentAmount, loyaltyAccount, payCurrencyDTO,
				paymentTimestamp);
	}

	public void addLMSMemberPayment(String loyaltyMemberId, String[] rewardIDs,
			Map<Integer, Map<String, BigDecimal>> paxProductRedeemed, BigDecimal totalPaymentAmount,
			PayCurrencyDTO payCurrencyDTO, Date paymentTimestamp, String carrierCode) {
		paymentAssembler.addLMSMemberPayment(loyaltyMemberId, rewardIDs, paxProductRedeemed, totalPaymentAmount, payCurrencyDTO,
				paymentTimestamp, carrierCode);
	}

	public LCCClientPaymentAssembler getPaymentAssembler() {
		return paymentAssembler;
	}

	public void addVoucherPayment(PayByVoucherInfo payByVoucherInfo, BigDecimal paxPayAmt, PayCurrencyDTO cardPayCurrencyDTO,
			Date currentDatetime) {

		BigDecimal totalConsumablePaxPayAmount;

		if (!paymentAssembler.isExternalChargesConsumed()) {
			totalConsumablePaxPayAmount = AccelAeroCalculator.add(paxPayAmt,
					paymentAssembler.calculateTotalExternalChargesAmount());
		} else {
			totalConsumablePaxPayAmount = paxPayAmt;
		}

		for (VoucherDTO voucherDTO : payByVoucherInfo.getVoucherDTOList()) {
			BigDecimal voucherAmount = new BigDecimal(voucherDTO.getAmountInBase());
			BigDecimal voucherRedeemAmount = new BigDecimal(voucherDTO.getRedeemdAmount());
			BigDecimal remainingVoucherAmount = AccelAeroCalculator.subtract(voucherAmount, voucherRedeemAmount);
			if (voucherRedeemAmount.compareTo(voucherAmount) < 0) {
				VoucherDTO voucher = new VoucherDTO();
				voucher.setAmountInBase(voucherDTO.getAmountInBase());
				voucher.setVoucherId(voucherDTO.getVoucherId());
				if (remainingVoucherAmount.compareTo(totalConsumablePaxPayAmount) >= 0) {
					if (!paymentAssembler.isExternalChargesConsumed()) {
						voucher.setRedeemdAmount(AccelAeroCalculator
								.subtract(totalConsumablePaxPayAmount, paymentAssembler.calculateTotalExternalChargesAmount())
								.toString());
					} else {
						voucher.setRedeemdAmount(totalConsumablePaxPayAmount.toString());
					}
					paymentAssembler.addVoucherPayment(voucher, cardPayCurrencyDTO, currentDatetime);
					voucherDTO.setRedeemdAmount(
							AccelAeroCalculator.add(new BigDecimal(voucher.getRedeemdAmount()), voucherRedeemAmount).toString());
					break;
				} else {
					if (!paymentAssembler.isExternalChargesConsumed()) {
						if (remainingVoucherAmount.compareTo(totalConsumablePaxPayAmount) >= 0) {
							voucher.setRedeemdAmount(AccelAeroCalculator
									.subtract(totalConsumablePaxPayAmount, paymentAssembler.calculateTotalExternalChargesAmount()).toString());
						} else {
							voucher.setRedeemdAmount(AccelAeroCalculator
									.subtract(remainingVoucherAmount, paymentAssembler.calculateTotalExternalChargesAmount())
									.toString());
						}
					} else if (remainingVoucherAmount.compareTo(totalConsumablePaxPayAmount) > -1) {
						voucher.setRedeemdAmount(totalConsumablePaxPayAmount.toString());
					} else {
						voucher.setRedeemdAmount(remainingVoucherAmount.toString());
					}
					paymentAssembler.addVoucherPayment(voucher, cardPayCurrencyDTO, currentDatetime);
					voucherDTO.setRedeemdAmount(
							AccelAeroCalculator.add(new BigDecimal(voucher.getRedeemdAmount()), voucherRedeemAmount).toString());
				}
				totalConsumablePaxPayAmount = AccelAeroCalculator.subtract(totalConsumablePaxPayAmount,
						new BigDecimal(voucher.getRedeemdAmount()));
			}
		}
	}
}
