package com.isa.thinair.webplatform.api.util;

import java.math.BigDecimal;
import java.util.Collection;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.webplatform.api.dto.PaxSet;

/**
 * @author eric
 * 
 */
public class PriceUtil {

	/**
	 * @param quotedCharges
	 * @param paxSet
	 * @return
	 * @throws ModuleException
	 */
	public static BigDecimal getTotalQuotePrice(Collection<OndFareDTO> OndFareDTOs, PaxSet paxSet) throws ModuleException {
		BigDecimal totalTicketPrice = AccelAeroCalculator.getDefaultBigDecimalZero();

		BigDecimal perAdultPaxTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal perInfantPaxTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal perChildPaxTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();

		double[] perOndPrice;

		for (OndFareDTO quotedOndFareDTO : OndFareDTOs) {
			perOndPrice = quotedOndFareDTO.getTotalCharges();

			if (ReservationApiUtils.isFareExist(quotedOndFareDTO.getAdultFare())) {
				perAdultPaxTotalPrice = AccelAeroCalculator.add(perAdultPaxTotalPrice,
						AccelAeroCalculator.parseBigDecimal(perOndPrice[0]),
						AccelAeroCalculator.parseBigDecimal(quotedOndFareDTO.getAdultFare()));
			}

			if (ReservationApiUtils.isFareExist(quotedOndFareDTO.getChildFare())) {
				perChildPaxTotalPrice = AccelAeroCalculator.add(perChildPaxTotalPrice,
						AccelAeroCalculator.parseBigDecimal(perOndPrice[2]),
						AccelAeroCalculator.parseBigDecimal(quotedOndFareDTO.getChildFare()));
			}
			if (ReservationApiUtils.isFareExist(quotedOndFareDTO.getInfantFare())) {
				perInfantPaxTotalPrice = AccelAeroCalculator.add(perInfantPaxTotalPrice,
						AccelAeroCalculator.parseBigDecimal(perOndPrice[1]),
						AccelAeroCalculator.parseBigDecimal(quotedOndFareDTO.getInfantFare()));
			}

		}
		totalTicketPrice = AccelAeroCalculator.add((AccelAeroCalculator.multiply(perAdultPaxTotalPrice, paxSet.getNoOfAdluts())),
				(AccelAeroCalculator.multiply(perChildPaxTotalPrice, paxSet.getNoOfChildren())),
				(AccelAeroCalculator.multiply(perInfantPaxTotalPrice, paxSet.getNoOfInfants())));

		return totalTicketPrice;
	}

	/**
	 * @param quotedCharges
	 * @param paxSet
	 * @return
	 * @throws ModuleException
	 */
	public static BigDecimal getTotalBaseFare(Collection<OndFareDTO> OndFareDTOs, PaxSet paxSet) throws ModuleException {
		BigDecimal totalBaseFare = AccelAeroCalculator.getDefaultBigDecimalZero();

		BigDecimal perAdultPaxTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal perInfantPaxTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal perChildPaxTotalPrice = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (OndFareDTO quotedOndFareDTO : OndFareDTOs) {

			if (ReservationApiUtils.isFareExist(quotedOndFareDTO.getAdultFare())) {
				perAdultPaxTotalPrice = AccelAeroCalculator.add(perAdultPaxTotalPrice,
						AccelAeroCalculator.parseBigDecimal(quotedOndFareDTO.getAdultFare()));
			}
			if (ReservationApiUtils.isFareExist(quotedOndFareDTO.getChildFare())) {
				perChildPaxTotalPrice = AccelAeroCalculator.add(perChildPaxTotalPrice,
						AccelAeroCalculator.parseBigDecimal(quotedOndFareDTO.getChildFare()));
			}

			if (ReservationApiUtils.isFareExist(quotedOndFareDTO.getInfantFare())) {
				perInfantPaxTotalPrice = AccelAeroCalculator.add(perInfantPaxTotalPrice,
						AccelAeroCalculator.parseBigDecimal(quotedOndFareDTO.getInfantFare()));
			}

		}
		totalBaseFare = AccelAeroCalculator.add((AccelAeroCalculator.multiply(perAdultPaxTotalPrice, paxSet.getNoOfAdluts())),
				(AccelAeroCalculator.multiply(perChildPaxTotalPrice, paxSet.getNoOfChildren())),
				(AccelAeroCalculator.multiply(perInfantPaxTotalPrice, paxSet.getNoOfInfants())));

		return totalBaseFare;
	}

	/**
	 * @param OndFareDTOs
	 * @param paxType
	 * @return
	 * @throws ModuleException
	 */
	public static BigDecimal getPaxBaseFare(Collection<OndFareDTO> OndFareDTOs, String paxType) throws ModuleException {
		BigDecimal paxBasefare = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (OndFareDTO quotedOndFareDTO : OndFareDTOs) {

			if (PaxTypeTO.ADULT.equals(paxType)) {

				if (ReservationApiUtils.isFareExist(quotedOndFareDTO.getAdultFare())) {
					paxBasefare = AccelAeroCalculator.add(paxBasefare,
							AccelAeroCalculator.parseBigDecimal(quotedOndFareDTO.getAdultFare()));
				}

			} else if (PaxTypeTO.CHILD.equals(paxType)) {
				if (ReservationApiUtils.isFareExist(quotedOndFareDTO.getChildFare())) {
					paxBasefare = AccelAeroCalculator.add(paxBasefare,
							AccelAeroCalculator.parseBigDecimal(quotedOndFareDTO.getChildFare()));
				}
			}

			else if (PaxTypeTO.INFANT.equals(paxType)) {

				if (ReservationApiUtils.isFareExist(quotedOndFareDTO.getInfantFare())) {
					paxBasefare = AccelAeroCalculator.add(paxBasefare,
							AccelAeroCalculator.parseBigDecimal(quotedOndFareDTO.getInfantFare()));
				}

			}
		}
		return paxBasefare;
	}

	/**
	 * @param OndFareDTOs
	 * @param paxType
	 * @return
	 * @throws ModuleException
	 */
	public static BigDecimal getPaxTotalFare(Collection<OndFareDTO> OndFareDTOs, String paxType) throws ModuleException {

		BigDecimal paxTotalFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		double[] perOndCharges;
		for (OndFareDTO quotedOndFareDTO : OndFareDTOs) {
			perOndCharges = quotedOndFareDTO.getTotalCharges();
			if (PaxTypeTO.ADULT.equals(paxType)) {

				if (ReservationApiUtils.isFareExist(quotedOndFareDTO.getAdultFare())) {
					paxTotalFare = AccelAeroCalculator.add(paxTotalFare, AccelAeroCalculator.parseBigDecimal(perOndCharges[0]),
							AccelAeroCalculator.parseBigDecimal(quotedOndFareDTO.getAdultFare()));
				}

			} else if (PaxTypeTO.CHILD.equals(paxType)) {

				if (ReservationApiUtils.isFareExist(quotedOndFareDTO.getChildFare())) {
					paxTotalFare = AccelAeroCalculator.add(paxTotalFare, AccelAeroCalculator.parseBigDecimal(perOndCharges[2]),
							AccelAeroCalculator.parseBigDecimal(quotedOndFareDTO.getChildFare()));
				}
			}

			else if (PaxTypeTO.INFANT.equals(paxType)) {

				if (ReservationApiUtils.isFareExist(quotedOndFareDTO.getInfantFare())) {
					paxTotalFare = AccelAeroCalculator.add(paxTotalFare, AccelAeroCalculator.parseBigDecimal(perOndCharges[1]),
							AccelAeroCalculator.parseBigDecimal(quotedOndFareDTO.getInfantFare()));
				}

			}
		}
		return paxTotalFare;
	}
}
