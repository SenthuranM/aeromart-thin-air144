package com.isa.thinair.webplatform.api.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;

/**
 * 
 * 
 */
public class HandlingFeeCalculationUtil {

	private static Log log = LogFactory.getLog(HandlingFeeCalculationUtil.class);

	private static String getFirstFlightReference(Collection<LCCClientReservationSegment> lccSegments) throws Exception {
		if (lccSegments != null && !lccSegments.isEmpty()) {
			for (LCCClientReservationSegment segment : lccSegments) {
				if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(segment.getStatus())
						&& !segment.isFlownSegment()) {
					if (!StringUtil.isNullOrEmpty(segment.getFlightSegmentRefNumber())
							&& !StringUtil.isNullOrEmpty(segment.getBookingFlightSegmentRefNumber()))
						return FlightRefNumberUtil.composePnrSegRPHFromFlightRPH(segment.getFlightSegmentRefNumber(),
								segment.getBookingFlightSegmentRefNumber());
				}
			}
		}

		return null;
	}

	public static Map<Integer, List<LCCClientExternalChgDTO>> composeExternalChargesByPaxSequence(String pnr,
			Collection<LCCClientReservationSegment> lccSegments, List<Integer> exclusionPaxSequence, int operationMode)
			throws ModuleException, Exception {

		Map<Integer, List<LCCClientExternalChgDTO>> paxExternalCharges = new HashMap<>();
		if (AppSysParamsUtil.applyHandlingFeeOnModification() && operationMode > 0 && !StringUtil.isNullOrEmpty(pnr)) {
			Collection<ReservationPax> resPaxList = WPModuleUtils.getResPassengerBD().getPassengers(pnr, false);

			ExternalChgDTO handlingChgDTO = WPModuleUtils.getReservationBD().getHandlingFeeForModification(operationMode);
			if (handlingChgDTO != null && handlingChgDTO.getAmount().doubleValue() > 0) {
				int divPaxCount = resPaxList.size();

				if (exclusionPaxSequence != null && !exclusionPaxSequence.isEmpty()) {
					divPaxCount = divPaxCount - exclusionPaxSequence.size();
				}

				BigDecimal paxWiseHandlingFee[] = AccelAeroCalculator.roundAndSplit(handlingChgDTO.getAmount(), divPaxCount);
				String flightRef = getFirstFlightReference(lccSegments);

				int paxCount = 0;

				for (ReservationPax resPax : resPaxList) {

					if (exclusionPaxSequence != null && !exclusionPaxSequence.isEmpty()
							&& exclusionPaxSequence.contains(resPax.getPaxSequence())) {
						continue;
					}

					List<LCCClientExternalChgDTO> listExteCHs = new ArrayList<>();

					LCCClientExternalChgDTO chg = new LCCClientExternalChgDTO();
					chg.setAmount(paxWiseHandlingFee[paxCount]);
					chg.setExternalCharges(EXTERNAL_CHARGES.HANDLING_CHARGE);
					chg.setCode(handlingChgDTO.getChargeCode());
					chg.setFlightRefNumber(flightRef);
					chg.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(flightRef));
					chg.setOperationMode(operationMode);

					listExteCHs.add(chg);

					paxExternalCharges.put(resPax.getPaxSequence(), listExteCHs);

					paxCount++;

				}
			}
		}

		return paxExternalCharges;
	}

	public static BigDecimal applyPaxWiseHandlingFeeForModification(Collection<ReservationPaxTO> paxList, String flightRef,
			int operationMode) throws ModuleException {
		BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (AppSysParamsUtil.applyHandlingFeeOnModification() && paxList != null && !paxList.isEmpty()
				&& !StringUtil.isNullOrEmpty(flightRef) && operationMode > 0) {
			ExternalChgDTO handlingChgDTO = WPModuleUtils.getReservationBD().getHandlingFeeForModification(operationMode);
			totalAmount = applyHandlingFeeToPassengers(paxList, handlingChgDTO, flightRef, operationMode);
		}

		return totalAmount;
	}

	private static BigDecimal applyHandlingFeeToPassengers(Collection<ReservationPaxTO> paxList, ExternalChgDTO handlingChgDTO,
			String flightRef, int operationMode) {
		BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (handlingChgDTO != null && handlingChgDTO.getAmount().doubleValue() > 0) {
			totalAmount = handlingChgDTO.getAmount();

			int paxCountToApplyHandlingCharge =  paxList.size();
			
			if(CommonsConstants.ModifyOperation.MODIFY_ANCI.getOperation().intValue() == operationMode){
				paxCountToApplyHandlingCharge = getAnciModifiedPaxCount(paxList);
			}
			
			if(paxCountToApplyHandlingCharge > 0){
				BigDecimal paxWiseHandlingFee[] = AccelAeroCalculator.roundAndSplit(handlingChgDTO.getAmount(),paxCountToApplyHandlingCharge);
				int i = 0;
				for (ReservationPaxTO pax : paxList) {
					LCCClientExternalChgDTO chg = new LCCClientExternalChgDTO();
					if(CommonsConstants.ModifyOperation.MODIFY_ANCI.getOperation().intValue() == operationMode){
						if(!isAnciModifiedPax(pax)){
							continue;
						}
					} 
					chg.setAmount(paxWiseHandlingFee[i]);
					chg.setExternalCharges(EXTERNAL_CHARGES.HANDLING_CHARGE);
					chg.setCode(handlingChgDTO.getChargeCode());
					chg.setFlightRefNumber(flightRef);
					chg.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(flightRef));
					chg.setOperationMode(operationMode);
					pax.addExternalCharges(chg);
					i++;
				}
			}
		}
		return totalAmount;
	}

	private static int getAnciModifiedPaxCount(Collection<ReservationPaxTO> paxList) {

		int anciModifiedPax = 0;
		for (ReservationPaxTO pax : paxList) {
			if (isAnciModifiedPax(pax)) {
				++anciModifiedPax;
			}
		}

		return anciModifiedPax;
	}
	
	private static boolean isAnciModifiedPax(ReservationPaxTO pax) {
		boolean isAnciModified = false;
		if (pax.getToRemoveAncillaryTotal().compareTo(BigDecimal.ZERO) > 0
				|| pax.getToUpdateAncillaryTotal().compareTo(BigDecimal.ZERO) > 0
				|| (pax.getExternalCharges() != null && !pax.getExternalCharges().isEmpty())) {
			isAnciModified = true;
		}
		return isAnciModified;
	}
}
