package com.isa.thinair.webplatform.api.dto;

public interface ReservationConstants {

	public static int MAX_NO_OF_SEGMENTS_FOR_BSP_PAYMENT = 8;

	/** Reservation Segments Alteration Operations */
	public static interface AlterationType {
		public static String ALT_CANCEL_RES = "CNXRES";
		public static String ALT_CANCEL_OND = "CNXOND";
		public static String ALT_CANCEL_OND_WITH_MODIFY_CHARGE = "CNXONDWITHMODCHG";
		public static String ALT_MODIFY_OND = "MODOND";
		public static String ALT_ADD_OND = "ADDOND";
		public static String ALT_ADD_INF = "ADDINF";
	}

	public static interface AlterationTypeV2 {
		public static String ALT_CANCEL_RES = "CNXRES";
		public static String ALT_CANCEL_OND = "CNXOND";
		public static String ALT_CANCEL_OND_WITH_MODIFY_CHARGE = "CNXONDWITHMODCHG";
		public static String ALT_ADD_OND = "ADDOND";
	}

	public static interface SegmentAncillaryStatus {
		public static final String ALL = "ALL";
		public static final String PARTIAL = "PARTIAL";
		public static final String NONE = "NONE";
	}

	public static interface FlightSegNotifyEvent {
		public static String NOTSENT = "NOTSENT";
		public static String INPROGRESS = "INPROGRESS";
		public static String SENT = "SENT";
		public static String FAILED = "FAILED";
	}

	public static interface PnrSegNotifyEvent {
		public static String NOTSENT = "NOTSENT";
		public static String SENT = "SENT";
		public static String FAILED = "FAILED";
	}

	public static interface NotificationType {
		public static String ANCI_REMINDER = "ANCI_REMINDER";
		public static String CHECKIN_REMINDER = "CHECKIN_REMINDER";
	}

}
