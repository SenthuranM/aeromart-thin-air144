package com.isa.thinair.webplatform.api.v2.modify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;

public class ModifyReservationJSONUtil {
	/**
	 * Get Collection<LCCClientReservationSegment> from JSON type Stream
	 * 
	 * @param resSegments
	 * @return
	 * @throws Exception
	 */
	public static Collection<LCCClientReservationSegment> transformJsonSegments(String resSegments) throws Exception {
		Collection<LCCClientReservationSegment> colSegs = new ArrayList<LCCClientReservationSegment>();
		JSONArray jsonSegmentArray = (JSONArray) new JSONParser().parse(resSegments);
		String strDateformat = "yyyy-MM-dd'T'HH:mm:ss";
		Iterator<?> itIter = jsonSegmentArray.iterator();
		while (itIter.hasNext()) {
			LCCClientReservationSegment segment = new LCCClientReservationSegment();
			JSONObject jPPObj = (JSONObject) itIter.next();
			segment.setArrivalAirportName(BeanUtils.nullHandler(jPPObj.get("arrivalAirportName")));
			segment.setArrivalDate(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("arrivalDate")), strDateformat));
			segment.setArrivalDateZulu(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("arrivalDateZulu")), strDateformat));
			segment.setBookingFlightSegmentRefNumber(BeanUtils.nullHandler(jPPObj.get("bookingFlightSegmentRefNumber")));
			segment.setBookingType(BeanUtils.nullHandler(jPPObj.get("bookingType")));
			segment.setCabinClassCode(BeanUtils.nullHandler(jPPObj.get("cabinClassCode")));
			segment.setLogicalCabinClass(BeanUtils.nullHandler(jPPObj.get("logicalCabinClass")));
			segment.setCarrierCode(BeanUtils.nullHandler(jPPObj.get("carrierCode")));
			segment.setDepartureAirportName(BeanUtils.nullHandler(jPPObj.get("departureAirportName")));
			segment.setDepartureDate(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("departureDate")), strDateformat));
			segment.setDepartureDateZulu(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("departureDateZulu")), strDateformat));
			segment.setFlightNo(BeanUtils.nullHandler(jPPObj.get("flightNo")));
			segment.setFlightSegmentRefNumber(BeanUtils.nullHandler(jPPObj.get("flightSegmentRefNumber")));
			segment.setRouteRefNumber(BeanUtils.nullHandler(jPPObj.get("routeRefNumber")));
			segment.setInterlineGroupKey(BeanUtils.nullHandler(jPPObj.get("interlineGroupKey")));
			segment.setModifyTillBufferDateTime(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("modifyTillBufferDateTime")),
					strDateformat));
			segment.setCancelTillBufferDateTime(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("cancelTillBufferDateTime")),
					strDateformat));
			segment.setModifyTillFlightClosureDateTime(DateUtil.parseDate(
					BeanUtils.nullHandler(jPPObj.get("modifyTillFlightClosureDateTime")), strDateformat));
			segment.setReturnFlag(BeanUtils.nullHandler(jPPObj.get("returnFlag")));
			segment.setSegmentCode(BeanUtils.nullHandler(jPPObj.get("segmentCode")));
			segment.setSegmentSeq(new Integer(BeanUtils.nullHandler(jPPObj.get("segmentSeq"))));
			segment.setStatus(BeanUtils.nullHandler(jPPObj.get("status")));
			segment.setModifible(new Boolean(BeanUtils.nullHandler(jPPObj.get("modifible"))));
			segment.setSubStationShortName(jPPObj.get("subStationShortName") == null ? null : (String) jPPObj
					.get("subStationShortName"));
			segment.setAlertAutoCancellation(new Boolean(BeanUtils.nullHandler(jPPObj.get("alertAutoCancellation"))));
			if (jPPObj.get("departureDateZulu") != null) {
				segment.setDepartureDateZulu(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("departureDateZulu")),
						strDateformat));
			}
			if (jPPObj.get("arrivalDateZulu") != null) {
				segment.setArrivalDateZulu(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("arrivalDateZulu")), strDateformat));
			}
			segment.setInterlineReturnGroupKey(BeanUtils.nullHandler(jPPObj.get("interlineReturnGroupKey")));
			// Set open return info
			if (((Boolean) jPPObj.get("openReturnSegment")).booleanValue()) {
				segment.setOpenReturnSegment(true);
				segment.setOpenRetConfirmBefore(BeanUtils.nullHandler(jPPObj.get("openRetConfirmBefore")));
				segment.setOpenReturnTravelExpiry(BeanUtils.nullHandler(jPPObj.get("openReturnTravelExpiry")));
			}

			if (jPPObj.get("fareTO") != null) {
				JSONObject jsonFareRuleObj = (JSONObject) new JSONParser().parse(BeanUtils.nullHandler(jPPObj.get("fareTO")));
				FareTO fareTO = new FareTO();
				fareTO.setFareId(new Integer(BeanUtils.nullHandler(jsonFareRuleObj.get("fareId"))).intValue());
				fareTO.setFareRuleID(new Integer(BeanUtils.nullHandler(jsonFareRuleObj.get("fareRuleID"))).intValue());
				fareTO.setBulkTicketFareRule(new Boolean(BeanUtils.nullHandler(jsonFareRuleObj.get("bulkFareRule"))));
				fareTO.setAmount(new BigDecimal(BeanUtils.nullHandler(jsonFareRuleObj.get("amount"))));
				fareTO.setSegmentCode(BeanUtils.nullHandler(jsonFareRuleObj.get("segmentCode")));
				fareTO.setCarrierCode(BeanUtils.nullHandler(jsonFareRuleObj.get("carrierCode")));
				fareTO.setBookingClassCode(BeanUtils.nullHandler(jsonFareRuleObj.get("bookingClassCode")));
				segment.setFareTO(fareTO);
			}

			if (jPPObj.get("depAirportMinConTime") != null) {
				segment.setDepAirportMinConTime(BeanUtils.nullHandler(jPPObj.get("depAirportMinConTime")));
			}

			if (jPPObj.get("depAirportMaxConTime") != null) {
				segment.setDepAirportMaxConTime(BeanUtils.nullHandler(jPPObj.get("depAirportMaxConTime")));
			}

			if (jPPObj.get("arriAirportMinConTime") != null) {
				segment.setArriAirportMinConTime(BeanUtils.nullHandler(jPPObj.get("arriAirportMinConTime")));
			}

			if (jPPObj.get("arriAirportMaxConTime") != null) {
				segment.setArriAirportMaxConTime(BeanUtils.nullHandler(jPPObj.get("arriAirportMaxConTime")));
			}

			if (jPPObj.get("lastFareQuoteDate") != null) {
				segment.setLastFareQuoteDate(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("lastFareQuoteDate")),
						strDateformat));
			}

			String baggageONDGroupId = BeanUtils.nullHandler(jPPObj.get("baggageONDGroupId"));
			if (baggageONDGroupId.length() > 0) {
				segment.setBaggageONDGroupId(baggageONDGroupId);
			}

			if (jPPObj.get("ticketValidTill") != null) {
				segment.setTicketValidTill(DateUtil.parseDate(BeanUtils.nullHandler(jPPObj.get("ticketValidTill")), strDateformat));
			}

			if (jPPObj.get("journeySequence") != null) {
				segment.setJourneySequence(new Integer(BeanUtils.nullHandler(jPPObj.get("journeySequence"))).intValue());
			}
			if (jPPObj.get("allPaxNoShow") != null) {
				segment.setAllPaxNoShow(new Boolean(BeanUtils.nullHandler(jPPObj.get("allPaxNoShow"))));
			}
			if (jPPObj.get("unSegment") != null) {
				segment.setUnSegment(new Boolean(BeanUtils.nullHandler(jPPObj.get("unSegment"))));
			}
			
			if(jPPObj.get("flownSegment") != null) {
				segment.setFlownSegment(new Boolean(BeanUtils.nullHandler(jPPObj.get("flownSegment"))));
			}
			
			colSegs.add(segment);
		}

		List<LCCClientReservationSegment> lstSegments = new ArrayList<LCCClientReservationSegment>(colSegs);
		Collections.sort(lstSegments);
		colSegs = lstSegments;
		return colSegs;
	}

	public static Map<String, RollForwardFlightRQ> populateRollForwardRequest(String jsonRollForwardFlights)
			throws ParseException, java.text.ParseException {
		Map<String, RollForwardFlightRQ> rollForwardRequestMap = new HashMap<String, RollForwardFlightRQ>();
		Map<String, List<RollForwardFlightDTO>> rollForwardingFlights = transformJsonRollForwardFlights(jsonRollForwardFlights);

		for (Entry<String, List<RollForwardFlightDTO>> entry : rollForwardingFlights.entrySet()) {
			RollForwardFlightRQ rollForwardFlightRQ = new RollForwardFlightRQ();
			rollForwardFlightRQ.setRollForwardFlights(entry.getValue());

			rollForwardRequestMap.put(entry.getKey(), rollForwardFlightRQ);
		}

		return rollForwardRequestMap;
	}

	private static Map<String, List<RollForwardFlightDTO>> transformJsonRollForwardFlights(String jsonRollForwardFlights)
			throws ParseException, java.text.ParseException {
		Map<String, List<RollForwardFlightDTO>> rollForwardingFlights = new HashMap<String, List<RollForwardFlightDTO>>();
		JSONArray jsonSegmentArray = (JSONArray) new JSONParser().parse(jsonRollForwardFlights);
		Iterator<?> itIter = jsonSegmentArray.iterator();
		while (itIter.hasNext()) {
			JSONObject jPPObj = (JSONObject) itIter.next();
			String deptDate = BeanUtils.nullHandler(jPPObj.get("departureDate"));

			rollForwardingFlights.put(deptDate, new ArrayList<RollForwardFlightDTO>());

			JSONArray jsonFltArr = (JSONArray) jPPObj.get("rollForwardFlights");
			if (jsonFltArr != null) {
				Iterator<?> fltItr = jsonFltArr.iterator();
				String strDateformat = "yyyy-MM-dd'T'HH:mm:ss";
				while (fltItr.hasNext()) {
					JSONObject fltObj = (JSONObject) fltItr.next();
					RollForwardFlightDTO rollForwardFlightDTO = new RollForwardFlightDTO();
					rollForwardFlightDTO.setBookingClass(BeanUtils.nullHandler(fltObj.get("bookingClass")));
					rollForwardFlightDTO.setCabinClass(BeanUtils.nullHandler(fltObj.get("cabinClass")));
					rollForwardFlightDTO.setDepartureDateTime(DateUtil.parseDate(
							BeanUtils.nullHandler(fltObj.get("departureDateTime")), strDateformat));
					rollForwardFlightDTO.setDepartureDateTimeZulu(DateUtil.parseDate(
							BeanUtils.nullHandler(fltObj.get("departureDateTimeZulu")), strDateformat));
					rollForwardFlightDTO.setFareId(new Integer(BeanUtils.nullHandler(fltObj.get("fareId"))));
					rollForwardFlightDTO.setFareRuleId(new Integer(BeanUtils.nullHandler(fltObj.get("fareRuleId"))));
					rollForwardFlightDTO.setFlightRefNo(BeanUtils.nullHandler(fltObj.get("flightRefNo")));
					rollForwardFlightDTO.setFlightSegId(new Integer(BeanUtils.nullHandler(fltObj.get("flightSegId"))));
					rollForwardFlightDTO.setReturnFlag(BeanUtils.nullHandler(fltObj.get("returnFlag")));
					rollForwardFlightDTO.setSegmentCode(BeanUtils.nullHandler(fltObj.get("segmentCode")));
					rollForwardFlightDTO.setSegmentSeq(new Integer(BeanUtils.nullHandler(fltObj.get("segmentSeq"))));
					rollForwardFlightDTO.setDomesticFlight(new Boolean(BeanUtils.nullHandler(fltObj.get("domesticFlight"))));
					rollForwardFlightDTO.setOpenReturn(new Boolean(BeanUtils.nullHandler(fltObj.get("openReturn"))));

					rollForwardingFlights.get(deptDate).add(rollForwardFlightDTO);
				}
			}
		}

		return rollForwardingFlights;
	}
}
