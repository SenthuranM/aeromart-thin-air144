package com.isa.thinair.webplatform.api.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOption;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SubStationUtil;
import com.isa.thinair.webplatform.api.dto.SurfaceSegmentValidationTO;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ONDSearchDTO;
import com.isa.thinair.webplatform.core.commons.DatabaseUtil;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;

/**
 * Add / Modify Surface Segment validation util
 * 
 * # One ground station can have one to many surface station # One surface station must be linked only to one ground
 * station # Ground station must be linked to a connecting airport # Connecting airport can have one to many ground
 * stations
 * 
 * @author Duminda G
 * @since October 22, 2010
 */
public final class AddModifySurfaceSegmentValidations extends SurfaceSegmentValidations {

	/**
	 * [Origin / Destination is SubStation]
	 * 
	 * @param searchDTO
	 * @return
	 * @throws ModuleException
	 */
	public synchronized static boolean isValidSearchForFreshBooking(FlightSearchDTO searchDTO) throws ModuleException {
		SurfaceSegmentValidationTO surfaceValTO = getSubStations(searchDTO, null);
		surfaceValTO = getConnectingStations(searchDTO, surfaceValTO);

		if (surfaceValTO.isOriginConnectingStation() && surfaceValTO.isDestinationSubStation()) {
			return false;
		}
		if (surfaceValTO.isDestinationConnectingStation() && surfaceValTO.isOriginSubStation()) {
			return false;
		}
		if (surfaceValTO.isOriginSubStation() && surfaceValTO.isDestinationSubStation()) {
			return false;
		}

		debugMe(surfaceValTO);
		return true;
	}

	public synchronized static boolean isSubStationMissing(FlightSearchDTO searchDTO) throws ModuleException {
		if (hasEmptyOriginDestination(searchDTO)) {
			return true;
		}

		return false;
	}

	/**
	 * @author Navod
	 * @param segmentCode
	 * @param fromSubAirport
	 * @return
	 */
	public static boolean isMatchingSegmentCodeForFromSubStation(String segmentCode, String fromSubAirport, boolean isReturn) {
		segmentCode = BeanUtils.nullHandler(segmentCode);
		fromSubAirport = BeanUtils.nullHandler(fromSubAirport);

		if (segmentCode.length() == 0 || fromSubAirport.length() == 0) {
			return false;
		}

		String mainAirport = CommonsServices.getGlobalConfig().getMapPrimaryStationBySubStation().get(fromSubAirport);
		if (mainAirport == null) {
			mainAirport = fromSubAirport;
		}
		if (segmentCode.length() >= 3) {
			String startingCode = null;
			if (isReturn) {
				startingCode = getMainGroundStationBySegmentCode(segmentCode, false, true);
			} else {
				startingCode = getMainGroundStationBySegmentCode(segmentCode, true, false);
			}

			if (startingCode.equals(mainAirport)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @author Navod
	 * @param segmentCode
	 * @param toSubAirport
	 * @return
	 */
	public static boolean isMatchingSegmentCodeForToSubStation(String segmentCode, String toSubAirport, boolean isReturn) {
		segmentCode = BeanUtils.nullHandler(segmentCode);
		toSubAirport = BeanUtils.nullHandler(toSubAirport);

		if (segmentCode.length() == 0 || toSubAirport.length() == 0) {
			return false;
		}

		String mainAirport = CommonsServices.getGlobalConfig().getMapPrimaryStationBySubStation().get(toSubAirport);

		String endCode = null;
		if (isReturn) {
			endCode = getMainGroundStationBySegmentCode(segmentCode, true, false);
		} else {
			endCode = getMainGroundStationBySegmentCode(segmentCode, false, true);
		}
		if (endCode.equals(mainAirport)) {
			return true;
		}

		return false;
	}

	public static boolean isMatchingSegCodeForToAirportCode(String segCode, String toAirport, boolean isReturn)
			throws ModuleException {
		List<String> list = new ArrayList<String>();
		list.add(toAirport);
		String toSubAirport = returnGroundSegment(list);
		return isMatchingSegmentCodeForFromSubStation(segCode, toSubAirport, isReturn);
	}

	public static boolean isModifyingSurfaceSegment(String origin, String destination) {
		if (isConnectingStation(origin) || isConnectingStation(destination)) {
			return (isPrimarySurfaceStation(origin) || isPrimarySurfaceStation(destination));
		}
		return false;
	}

	public static boolean isValidAddGroundSegment(FlightSearchDTO searchDTO) throws ModuleException {
		Map<String, CachedAirportDTO> mapAirports = getAirportMapFromSearchDTO(searchDTO);
		if (!hasValue(searchDTO.getFromAirportSubStation()) && !hasValue(searchDTO.getToAirportSubStation())
				&& !WPModuleUtils.getAirportBD().isContainSurfaceAirport(mapAirports)) {
			return false;
		}
		return true;
	}

	public static String returnGroundSegment(Collection<String> colAirPortCodes) throws ModuleException {
		return returnGroundSegment(WPModuleUtils.getAirportBD().getCachedAllAirportMap(colAirPortCodes));
	}

	private static Map<String, CachedAirportDTO> getAirportMapFromSearchDTO(FlightSearchDTO searchDTO) throws ModuleException {
		List<String> colAirPortCodes = new ArrayList<String>();
		if (searchDTO.getFromAirport() != null && !searchDTO.getFromAirport().equalsIgnoreCase("")) {
			colAirPortCodes.add(searchDTO.getFromAirport());
		}
		if (searchDTO.getToAirport() != null && !searchDTO.getToAirport().equalsIgnoreCase("")) {
			colAirPortCodes.add(searchDTO.getToAirport());
		}

		return WPModuleUtils.getAirportBD().getCachedAllAirportMap(colAirPortCodes);
	}

	public static void updateGroundSegments(FlightSearchDTO searchDTO, SurfaceSegmentValidationTO surfaceSegmentValidationTO)
			throws ModuleException {
		Map<String, CachedAirportDTO> mapAirports = getAirportMapFromSearchDTO(searchDTO);

		// check this due to availability search may includes other carriers airports
		if (mapAirports.get(searchDTO.getFromAirport()) != null) {
			if (mapAirports.get(searchDTO.getFromAirport()).isSurfaceSegment()) {
				surfaceSegmentValidationTO.setOriginSubStation(true);
			}
		}

		// check this due to availability search may includes other carriers airports
		if (mapAirports.get(searchDTO.getToAirport()) != null) {
			if (mapAirports.get(searchDTO.getToAirport()).isSurfaceSegment()) {
				surfaceSegmentValidationTO.setDestinationSubStation(true);
			}
		}
	}

	/**
	 * Assumption only one ground station will return.
	 * 
	 * @param airportCodes
	 * @return
	 */
	public static String returnGroundSegment(Map<String, CachedAirportDTO> airportCodes) {
		Collection<CachedAirportDTO> airports = airportCodes.values();
		for (Iterator<CachedAirportDTO> iterator = airports.iterator(); iterator.hasNext();) {
			CachedAirportDTO airport = iterator.next();
			if (airport.isSurfaceSegment()) {
				return airport.getAirportCode();
			}
		}
		return null;
	}

	/**
	 * Validate whether the connecting surface segment is valid
	 * 
	 * @param existingFltSegments
	 * @param colSelectedFltSegment
	 * @param linkedSegmentPnrSegId
	 * @return
	 */
	public static boolean isValidSegment(Collection<LCCClientReservationSegment> existingFltSegments,
			Collection<FlightSegmentTO> colSelectedFltSegment, String linkedSegmentPnrSegId) {
		try {
			if (!AppSysParamsUtil.isGroundServiceEnabled()) {
				return true;
			}
			LCCClientReservationSegment parentResSeg = null;
			IFlightSegment flightSegmentTo = null;
			Long parentSegmentDepArrTime;
			Long childSegmentDepArrTime;
			boolean isIncomming = false;

			if (linkedSegmentPnrSegId == null || colSelectedFltSegment == null) {
				return true;
			}
			int flightSegId = Integer.parseInt(DatabaseUtil.getFlightSegmentId(linkedSegmentPnrSegId));
			for (LCCClientReservationSegment lCCClientReservationSegment : existingFltSegments) {
				int bookedFlightSegId = Integer.parseInt(lCCClientReservationSegment.getBookingFlightSegmentRefNumber());
				if (flightSegId == bookedFlightSegId) {
					parentResSeg = lCCClientReservationSegment;
					break;
				}
			}
			if (parentResSeg == null) {
				return true;
			}

			long flightclsgapMin = Long.parseLong(CommonsServices.getGlobalConfig().getBizParam(
					SystemParamKeys.FLIGHT_CLOSURE_DEPARTURE_GAP));
			long flightClsgapMs = flightclsgapMin * 60 * 1000;
			long connectionTime = 43200000L;

			Iterator iterFltSegments = colSelectedFltSegment.iterator();
			while (iterFltSegments.hasNext()) {
				flightSegmentTo = (IFlightSegment) iterFltSegments.next();
				break;
			}
			if (isGetDepartureTime(parentResSeg.getSegmentCode())) {
				parentSegmentDepArrTime = parentResSeg.getDepartureDateZulu().getTime();
			} else {
				parentSegmentDepArrTime = parentResSeg.getArrivalDateZulu().getTime();
			}
			if (isGetDepartureTime(flightSegmentTo.getSegmentCode())) {
				childSegmentDepArrTime = flightSegmentTo.getDepartureDateTimeZulu().getTime();
			} else {
				childSegmentDepArrTime = flightSegmentTo.getArrivalDateTimeZulu().getTime();
				isIncomming = true;
			}
			if (isIncomming) {
				long parentflightClosureTime = parentSegmentDepArrTime - flightClsgapMs;
				if ((childSegmentDepArrTime > parentflightClosureTime)
						|| (childSegmentDepArrTime < parentflightClosureTime - connectionTime)) {
					return false;
				}
			} else {
				long childflightClosureTime = childSegmentDepArrTime - flightClsgapMs;
				if ((childflightClosureTime < parentSegmentDepArrTime)
						|| childflightClosureTime > parentSegmentDepArrTime + connectionTime) {
					return false;
				}
			}

		} catch (Exception e) {
			// TODO
		}
		return true;
	}

	private static boolean isFirstSegmentConnectingStation(String segmentCode) {

		String[] selFltSegments = segmentCode.split("/");

		if (isConnectingStation(selFltSegments[0])) {
			return true;
		}
		return false;
	}

	private static boolean isGetDepartureTime(String segmentCode) {
		return isFirstSegmentConnectingStation(segmentCode);
	}

	public static Collection getAirportsFromSegmentCode(String segmentCode) {
		Collection colAirports = new ArrayList();
		String airports[] = segmentCode.split("/");
		for (int i = 0; i < airports.length; i++) {
			colAirports.add(airports[i]);
		}
		return colAirports;
	}

	public static void mapStationsBySegments(Collection<OriginDestinationOption> segments, Map<Integer, String> segmentMap,
			Map<Integer, Integer> groundSegmentByAirFlightSegmentID) throws NumberFormatException, ModuleException {
		if (!AppSysParamsUtil.isGroundServiceEnabled()) {
			return;
		}

		if (segmentMap == null) {
			segmentMap = new LinkedHashMap<Integer, String>();
		}
		if (groundSegmentByAirFlightSegmentID == null) {
			groundSegmentByAirFlightSegmentID = new LinkedHashMap<Integer, Integer>();
		}
		// FlightReF No : SegmentCode
		Map<Integer, String> airSegments = new LinkedHashMap<Integer, String>();
		// Airport Code of the Parent Ground Segment Connection : Flight Ref.
		// No.
		Map<String, List<Integer>> groundSegments = new LinkedHashMap<String, List<Integer>>();
		boolean isGroundSegment = false;
		// Ordering Segment Sequance. For Local use
		Set<FlightSegment> outBoundSegs = new LinkedHashSet<FlightSegment>();
		Set<FlightSegment> orderedSegSetReturn = new LinkedHashSet<FlightSegment>();
		Set<FlightSegment> orderedSegments = new LinkedHashSet<FlightSegment>();

		for (OriginDestinationOption clientSeg : segments) {
			for (FlightSegment flightSegment : clientSeg.getFlightSegment()) {
				if ("Y".equals(flightSegment.getReturnFlag())) {
					orderedSegSetReturn.add(flightSegment);
				} else {
					outBoundSegs.add(flightSegment);
				}
			}
		}
		orderedSegments.addAll(outBoundSegs);
		orderedSegments.addAll(orderedSegSetReturn);
		
		FlightSegment[]  sortedSegmentArray = sortFlightSegmentByDepDate(orderedSegments);
		
		for (FlightSegment clientReservationSegment : sortedSegmentArray) {
			isGroundSegment = false;

			if (clientReservationSegment.getSubStationShortName() != null
					|| WPModuleUtils.getAirportBD().isContainGroundSegment(getAirportCollection(clientReservationSegment))) {
				String groundStation = null;
				if (clientReservationSegment.getSubStationShortName() != null) {
					groundStation = clientReservationSegment.getSubStationShortName();
				} else {
					groundStation = returnGroundSegment(getAirportCollection(clientReservationSegment));
				}
				segmentMap.put(Integer.parseInt(clientReservationSegment.getFlightRefNumber()), groundStation);
				isGroundSegment = true;
				List<Integer> existing = groundSegments.get(groundStation);
				// assuming segments are sent in the Dep/ return order: TODO:
				// CLEAN THIS UP
				if (existing == null) {
					existing = new ArrayList<Integer>();
					if (groundStation != null) {
						groundSegments.put(groundStation, existing);
					}
				}
				existing.add(Integer.parseInt(clientReservationSegment.getFlightRefNumber()));
			}
			if (!isGroundSegment) {
				airSegments.put(Integer.parseInt(clientReservationSegment.getFlightRefNumber()),
						clientReservationSegment.getSegmentCode());
			}
		}

		for (Entry<Integer, String> airSegmentMapEntry : airSegments.entrySet()) {
			for (Entry<String, List<Integer>> geoundSegMapEntry : groundSegments.entrySet()) {
				if (SubStationUtil.isPrimaryStationBelongingToGroundStationPresent(airSegmentMapEntry.getValue(),
						geoundSegMapEntry.getKey())) {
					List<Integer> existing = geoundSegMapEntry.getValue();
					if (existing != null && existing.size() > 0)
						groundSegmentByAirFlightSegmentID.put(existing.remove(0), airSegmentMapEntry.getKey());
					break;
				}
			}
		}
	}

	private static Collection getAirportCollection(FlightSegment clientReservationSegment) {
		Collection airports = Arrays.asList(clientReservationSegment.getSegmentCode().split("/"));
		return airports;
	}
	
	private static FlightSegment[] sortFlightSegmentByDepDate(Set<FlightSegment> setReservationSegments) {
		int size = setReservationSegments.size();
		FlightSegment[] arrayToSort = setReservationSegments.toArray(new FlightSegment[size]);
		Arrays.sort(arrayToSort, new Comparator<FlightSegment>() {
			@Override
			public int compare(FlightSegment rs1, FlightSegment rs2) {
				return rs1.getDepatureDateTimeZulu().compareTo(rs2.getDepatureDateTimeZulu());
			}
		});

		return arrayToSort;
	}
	
	public static boolean isGroundSegmentOnlyExist(FlightSearchDTO searchParams) throws ModuleException {

		if (searchParams != null && searchParams.getOndList() != null && searchParams.getOndList().size() > 0) {
			List<ONDSearchDTO> ondList = searchParams.getOndList();
			for (ONDSearchDTO ondSearchDTO : ondList) {
				if (ondList != null) {
					FlightSearchDTO flightSearchDTO = searchParams;
					flightSearchDTO.setFromAirport(ondSearchDTO.getFromAirport());
					flightSearchDTO.setToAirport(ondSearchDTO.getToAirport());

					if (!isValidSearchForFreshBooking(flightSearchDTO)) {
						return true;
					}
				}

			}
		}
		return false;

	}

	public static boolean isOnlyGroundSegments(List<FlightSearchDTO> searchDTOs) throws ModuleException {

		boolean onlyGroundSegments = false;
		int busCount = 0;
		for (FlightSearchDTO searchDTO : searchDTOs) {

			if (!isValidSearchForFreshBooking(searchDTO)) {
				busCount++;
			}
		}

		if (busCount > 0 && busCount == searchDTOs.size()) {
			onlyGroundSegments = true;

		}

		return onlyGroundSegments;

	}
}
