package com.isa.thinair.webplatform.api.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndLocationDTO;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.dto.ServiceTaxDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRS;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.ChangeFareRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BaseFareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.ExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FeeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.LogicalCabinClassInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ONDExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OpenReturnOptionsTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ.DISCOUNT_METHOD;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.PaxChargesTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airproxy.api.utils.assembler.OndConvertAssembler;
import com.isa.thinair.airreservation.api.dto.NameDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.HumanVerificationConstants;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.v2.modify.ModifyReservationJSONUtil;
import com.isa.thinair.webplatform.api.v2.reservation.AvailableFlightInfo;
import com.isa.thinair.webplatform.api.v2.reservation.CalendarFlightAvailInfo;
import com.isa.thinair.webplatform.api.v2.reservation.FareQuoteSummaryTO;
import com.isa.thinair.webplatform.api.v2.reservation.FareQuoteTO;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ONDSearchDTO;
import com.isa.thinair.webplatform.api.v2.reservation.OndFlightDTO;
import com.isa.thinair.webplatform.api.v2.reservation.PaxFareTO;
import com.isa.thinair.webplatform.api.v2.reservation.PaxPriceTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.reservation.SegmentFareTO;
import com.isa.thinair.webplatform.api.v2.reservation.SurchargeFETO;
import com.isa.thinair.webplatform.api.v2.reservation.TaxFETO;
import com.isa.thinair.webplatform.api.v2.util.BigDecimalWrapper;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.webplatform.core.commons.DatabaseUtil;
import com.isa.thinair.webplatform.core.commons.MapGenerator;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;
import com.isa.thinair.webplatform.core.constants.FareTypeCodes;

import org.springframework.util.CollectionUtils;

/**
 * Utility class for reservation related DTO transformations and DTO based utility checks.
 * 
 */
public class ReservationBeanUtil {

	private static String DATE_FORMAT_PATTERN = CalendarUtil.PATTERN7;
	private static Log log = LogFactory.getLog(ReservationBeanUtil.class);

	/**
	 * method to create AvailableFlightSearchDTO from FlightSearchDTO
	 * 
	 * @throws ParseException
	 */
	public static FlightAvailRQ createFlightAvailRQ(FlightSearchDTO flightSearch) throws ParseException {

		FlightAvailRQ flightAvailRQ = new FlightAvailRQ();

		// TODO remove this
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

		// setting origin destination information
		OriginDestinationInformationTO depatureOnD = flightAvailRQ.addNewOriginDestinationInformation();
		boolean isEnableCityBasesFunctionality = AppSysParamsUtil.enableCityBasesFunctionality();

		if (!flightSearch.isOpenReturnConfirm()) { // For open return confirmations there won't be a departure date
			depatureOnD.setOrigin(flightSearch.getFromAirport());
			depatureOnD.setDestination(flightSearch.getToAirport());
			format.setLenient(false);
			Date depatureDate = format.parse(flightSearch.getDepartureDate());

			Date depatureDateTimeStart = CalendarUtil.getStartTimeOfDate(depatureDate);
			depatureDateTimeStart = CalendarUtil.getOfssetAddedTime(depatureDateTimeStart,
					flightSearch.getDepartureVariance() * -1);

			// set within Ticket validity period
			// depatureDateTimeStart = getTicketValidtyVariance(flightSearch.isTicketValidityExist(), true,
			// flightSearch.getTicketValidityMax(), flightSearch.getTicketValidityMin(), depatureDateTimeStart);

			Date depatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(depatureDate);
			depatureDateTimeEnd = CalendarUtil.getOfssetAddedTime(depatureDateTimeEnd, flightSearch.getDepartureVariance());

			// set within Ticket validity period
			// depatureDateTimeEnd = getTicketValidtyVariance(flightSearch.isTicketValidityExist(), false,
			// flightSearch.getTicketValidityMax(), flightSearch.getTicketValidityMin(), depatureDateTimeEnd);

			depatureOnD.setPreferredDate(depatureDate);
			depatureOnD.setDepartureDateTimeStart(depatureDateTimeStart);
			depatureOnD.setDepartureDateTimeEnd(depatureDateTimeEnd);
			depatureOnD.setPreferredLogicalCabin(flightSearch.getPreferredLogicalCC(OndSequence.OUT_BOUND));
			depatureOnD.setPreferredClassOfService(flightSearch.getPreferredClassOfService(OndSequence.OUT_BOUND));
			depatureOnD.setPreferredBookingClass(flightSearch.getPreferredBookingClass(OndSequence.OUT_BOUND));
			depatureOnD.setPreferredBookingType(flightSearch.getPreferredBookingType(OndSequence.OUT_BOUND));
			depatureOnD.setPreferredBundleFarePeriodId(flightSearch.getPreferredBundledFarePeriodId(OndSequence.OUT_BOUND));

			updateOriginDestinationCitySearchEnabled(isEnableCityBasesFunctionality, flightSearch.getOndWiseCitySearch(),
					depatureOnD, OndSequence.OUT_BOUND);

			depatureOnD.setReturnFlag(false);
			if (flightSearch.getHubTimeDetailJSON() != null && !flightSearch.getHubTimeDetailJSON().equals("")) {
				setHubTimeDetails(depatureOnD, flightSearch.getHubTimeDetailJSON(), true);
			}
		} else { // For open return confirmation only the return date is available in the UI
			if (!StringUtil.isNullOrEmpty(flightSearch.getReturnDate())) {
				depatureOnD.setOrigin(flightSearch.getFromAirport());
				depatureOnD.setDestination(flightSearch.getToAirport());
				Date depatureDate = format.parse(flightSearch.getReturnDate());
				Date depatureDateTimeStart = CalendarUtil.getStartTimeOfDate(depatureDate);
				depatureDateTimeStart = CalendarUtil.getOfssetAddedTime(depatureDateTimeStart,
						flightSearch.getReturnVariance() * -1);

				Date depatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(depatureDate);
				depatureDateTimeEnd = CalendarUtil.getOfssetAddedTime(depatureDateTimeEnd, flightSearch.getReturnVariance());

				depatureOnD.setPreferredDate(depatureDate);
				depatureOnD.setDepartureDateTimeStart(depatureDateTimeStart);
				depatureOnD.setDepartureDateTimeEnd(depatureDateTimeEnd);
				depatureOnD.setReturnFlag(false);
				depatureOnD.setPreferredLogicalCabin(flightSearch.getPreferredLogicalCC(OndSequence.OUT_BOUND));
				depatureOnD.setPreferredClassOfService(flightSearch.getPreferredClassOfService(OndSequence.OUT_BOUND));
				depatureOnD.setPreferredBookingClass(flightSearch.getPreferredBookingClass(OndSequence.OUT_BOUND));
				depatureOnD.setPreferredBookingType(flightSearch.getPreferredBookingType(OndSequence.OUT_BOUND));
				depatureOnD.setPreferredBundleFarePeriodId(flightSearch.getPreferredBundledFarePeriodId(OndSequence.OUT_BOUND));

				flightSearch.setDepartureDate(flightSearch.getReturnDate());
				// Next/Prev navigation logic change
			} else if (!StringUtil.isNullOrEmpty(flightSearch.getDepartureDate())) {
				depatureOnD.setOrigin(flightSearch.getFromAirport());
				depatureOnD.setDestination(flightSearch.getToAirport());
				Date depatureDate = format.parse(flightSearch.getDepartureDate());
				Date depatureDateTimeStart = CalendarUtil.getStartTimeOfDate(depatureDate);
				depatureDateTimeStart = CalendarUtil.getOfssetAddedTime(depatureDateTimeStart,
						flightSearch.getDepartureVariance() * -1);

				Date depatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(depatureDate);
				depatureDateTimeEnd = CalendarUtil.getOfssetAddedTime(depatureDateTimeEnd, flightSearch.getDepartureVariance());

				depatureOnD.setPreferredDate(depatureDate);
				depatureOnD.setDepartureDateTimeStart(depatureDateTimeStart);
				depatureOnD.setDepartureDateTimeEnd(depatureDateTimeEnd);
				depatureOnD.setReturnFlag(false);
				depatureOnD.setPreferredLogicalCabin(flightSearch.getPreferredLogicalCC(OndSequence.OUT_BOUND));
				depatureOnD.setPreferredClassOfService(flightSearch.getPreferredClassOfService(OndSequence.OUT_BOUND));
				depatureOnD.setPreferredBookingClass(flightSearch.getPreferredBookingClass(OndSequence.OUT_BOUND));
				depatureOnD.setPreferredBookingType(flightSearch.getPreferredBookingType(OndSequence.OUT_BOUND));
				depatureOnD.setPreferredBundleFarePeriodId(flightSearch.getPreferredBundledFarePeriodId(OndSequence.OUT_BOUND));
			}
		}
		if (!StringUtil.isNullOrEmpty(flightSearch.getReturnDate()) && !flightSearch.isOpenReturnConfirm()) {

			OriginDestinationInformationTO returnOnD = flightAvailRQ.addNewOriginDestinationInformation();
			returnOnD.setOrigin(flightSearch.getToAirport());
			returnOnD.setDestination(flightSearch.getFromAirport());

			Date returnDate = format.parse(flightSearch.getReturnDate());

			Date returnDateTimeStart = CalendarUtil.getStartTimeOfDate(returnDate);
			returnDateTimeStart = CalendarUtil.getOfssetAddedTime(returnDateTimeStart, flightSearch.getReturnVariance() * -1);

			Date returnDateTimeEnd = CalendarUtil.getEndTimeOfDate(returnDate);
			returnDateTimeEnd = CalendarUtil.getOfssetAddedTime(returnDateTimeEnd, flightSearch.getReturnVariance());

			returnOnD.setPreferredDate(returnDate);
			returnOnD.setDepartureDateTimeStart(returnDateTimeStart);
			returnOnD.setDepartureDateTimeEnd(returnDateTimeEnd);
			returnOnD.setReturnFlag(true);
			returnOnD.setPreferredLogicalCabin(flightSearch.getPreferredLogicalCC(OndSequence.IN_BOUND));
			returnOnD.setPreferredClassOfService(flightSearch.getPreferredClassOfService(OndSequence.IN_BOUND));
			returnOnD.setPreferredBookingClass(flightSearch.getPreferredBookingClass(OndSequence.IN_BOUND));
			returnOnD.setPreferredBookingType(flightSearch.getPreferredBookingType(OndSequence.IN_BOUND));
			returnOnD.setPreferredBundleFarePeriodId(flightSearch.getPreferredBundledFarePeriodId(OndSequence.IN_BOUND));

			if (flightSearch.getHubTimeDetailJSON() != null && !flightSearch.getHubTimeDetailJSON().equals("")) {
				setHubTimeDetails(returnOnD, flightSearch.getHubTimeDetailJSON(), false);
			}

			updateOriginDestinationCitySearchEnabled(isEnableCityBasesFunctionality, flightSearch.getOndWiseCitySearch(),
					returnOnD, OndSequence.IN_BOUND);

			flightSearch.setHalfReturnFareQuote(AppSysParamsUtil.isAllowHalfReturnFaresForNewBookings());

		}

		// setting traveler information
		TravelerInfoSummaryTO traverlerInfo = flightAvailRQ.getTravelerInfoSummary();

		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);

		if (flightSearch.getAdultCount() == null) {
			adultsQuantity.setQuantity(0);
		} else {
			adultsQuantity.setQuantity(flightSearch.getAdultCount());
		}

		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);

		if (flightSearch.getChildCount() == null) {
			childQuantity.setQuantity(0);
		} else {
			childQuantity.setQuantity(flightSearch.getChildCount());
		}

		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);

		if (flightSearch.getInfantCount() == null) {
			infantQuantity.setQuantity(0);
		} else {
			infantQuantity.setQuantity(flightSearch.getInfantCount());
		}

		// setting traveler preferences
		TravelPreferencesTO travelerPref = flightAvailRQ.getTravelPreferences();
		if (!StringUtil.isNullOrEmpty(flightSearch.getBookingType())) {
			travelerPref.setBookingType(flightSearch.getBookingType());
		}
		travelerPref.setOpenReturn(flightSearch.getOpenReturn());
		travelerPref.setValidityId(flightSearch.getValidity());
		travelerPref.setOpenReturnConfirm(flightSearch.isOpenReturnConfirm());
		if (AppSysParamsUtil.isBCSelectionAtAvailabilitySearchEnabled()) {
			if (StringUtils.isNotEmpty(flightSearch.getBookingClassCode())) {
				travelerPref.setBookingClassCode(flightSearch.getBookingClassCode());
			}
		}

		AvailPreferencesTO availPref = flightAvailRQ.getAvailPreferences();
		if (!StringUtil.isNullOrEmpty(flightSearch.getPaxType())) {
			availPref.setBookingPaxType(flightSearch.getPaxType());
		}
		if (!StringUtil.isNullOrEmpty(flightSearch.getFareType())) {
			availPref.setFareCategoryType(flightSearch.getFareType());
		}
		if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
			availPref.setSearchSystem(SYSTEM.getEnum(flightSearch.getSearchSystem(), SYSTEM.INT));
		} else {
			availPref.setSearchSystem(SYSTEM.getEnum(flightSearch.getSearchSystem(), SYSTEM.AA));
		}
		availPref.setAppIndicator(ApplicationEngine.XBE);

		if (!StringUtil.isNullOrEmpty(flightSearch.getFirstDeparture())) {
			availPref.setFirstDepatureDate(CalendarUtil.getParsedTime(flightSearch.getFirstDeparture(), DATE_FORMAT_PATTERN));

		}

		if (!StringUtil.isNullOrEmpty(flightSearch.getLastArrival())) {
			availPref.setLastArrivalDate(CalendarUtil.getParsedTime(flightSearch.getLastArrival(), DATE_FORMAT_PATTERN));
		}

		availPref.setStayOverTimeInMillis(flightSearch.getStayOverTimeInMillis());
		availPref.setQuoteOndFlexi(flightSearch.getOndQuoteFlexi());
		availPref.setOndFlexiSelected(flightSearch.getOndSelectedFlexi());
		availPref.setTravelAgentCode(flightSearch.getTravelAgentCode());
		availPref.setHalfReturnFareQuote(flightSearch.isHalfReturnFareQuote());
		availPref.setExcludedCharges(flightSearch.getExcludeCharge());
		availPref.setPromoCode(flightSearch.getPromoCode());

		if (flightSearch.getBankIdentificationNo() != null && !"".equals(flightSearch.getBankIdentificationNo().trim())) {
			availPref.setBankIdentificationNumber(Integer.parseInt(flightSearch.getBankIdentificationNo().trim()));
		}

		return flightAvailRQ;
	}

	public static FareAvailRQ createFareAvailRQ(FlightSearchDTO flightSearch, boolean isRequoteFlow, boolean multicitySearch)
			throws ParseException, ModuleException {
		BaseAvailRQ baseAvail = null;
		if (isRequoteFlow || multicitySearch) {
			baseAvail = createFareQuoteRQ(flightSearch, AppIndicatorEnum.APP_XBE);
		} else {
			baseAvail = createFlightAvailRQ(flightSearch);
		}
		FareAvailRQ fareAvailRQ = new FareAvailRQ(baseAvail);
		fareAvailRQ.setTravelAgentCode(flightSearch.getTravelAgentCode());
		return fareAvailRQ;
	}

	public static ChangeFareRQ createChangeFareRQ(FlightSearchDTO flightSearch, boolean isModifyFlow, boolean multicitySearch)
			throws ParseException, ModuleException {
		BaseAvailRQ baseAvail = null;
		if (isModifyFlow || multicitySearch) {
			baseAvail = createFareQuoteRQ(flightSearch, AppIndicatorEnum.APP_XBE);
		} else {
			baseAvail = createFlightAvailRQ(flightSearch);
		}
		ChangeFareRQ changeFareRQ = new ChangeFareRQ(baseAvail);
		return changeFareRQ;
	}

	public static ChangeFareRQ createChangeFareRQ(FlightSearchDTO flightSearch, ArrayList<String> outFlightRPHList,
			ArrayList<String> retFlightRPHList) throws ParseException {
		BaseAvailRQ baseAvail = createFareQuoteRQ(flightSearch, outFlightRPHList, retFlightRPHList);
		ChangeFareRQ changeFareRQ = new ChangeFareRQ(baseAvail);
		return changeFareRQ;
	}

	public static List<OndFlightDTO> getOndFlightAvailDTOs(BaseAvailRS baseAvailRS, FlightSearchDTO searchDTO,
			boolean viewAvalableSeats, boolean viewFlightsWithLesserSeats, String radioPrifix, boolean isNextPrevious,
			boolean setSeatAvailFrmCount, Set<String> busCarrierCodes) throws ModuleException {

		List<OndFlightDTO> ondFltList = new ArrayList<OndFlightDTO>();
		int adultCount = searchDTO.getAdultCount() + searchDTO.getChildCount();
		int infantCount = searchDTO.getInfantCount();

		for (OriginDestinationInformationTO ondInfoTO : baseAvailRS.getOriginDestinationInformationList()) {
			OndFlightDTO ondFlightDTO = new OndFlightDTO(ondInfoTO.getOrigin(), ondInfoTO.getDestination());
			ondOptFor: for (OriginDestinationOptionTO ondOpt : ondInfoTO.getOrignDestinationOptions()) {
				boolean seatAvailFromCount = true;
				AvailableFlightInfo availableFlightInfo = new AvailableFlightInfo();
				int i = 0;
				for (FlightSegmentTO flightSegmentTO : ondOpt.getFlightSegmentList()) {
					// AARESAA-12804 - Requesting seat count is already validated by availability restriction
					// level,hence
					// there is no need for this
					// if (!viewFlightsWithLesserSeats && !flightSegmentTO.hasRequestingNumOfSeats(adultCount,
					// infantCount)) {
					// break ondOptFor;
					// }
					availableFlightInfo.addFlightSegment(flightSegmentTO);
					if (busCarrierCodes != null && WPModuleUtils.getAirportBD().isBusSegment(flightSegmentTO.getSegmentCode())) {
						busCarrierCodes.add(flightSegmentTO.getOperatingAirline());
					}
					if (!flightSegmentTO.hasRequestingNumOfSeats(adultCount, infantCount)) {
						seatAvailFromCount = false;
					}
					availableFlightInfo.addFlightStopOverDurationList(getStopOverDuration(ondOpt.getFlightSegmentList(), i));
				}
				availableFlightInfo.setSystem(ondOpt.getSystem().toString());
				availableFlightInfo.setSeatAvailable(seatAvailFromCount);

				// availableFlightInfo.setReturnFlag(returnFlag);
				ondFlightDTO.addAvailableFlightInfo(availableFlightInfo);
			}
			ondFltList.add(ondFlightDTO);
		}
		return ondFltList;
	}

	/**
	 * 
	 * @param baseAvailRS
	 * @param searchDTO
	 * @param viewAvalableSeats
	 * @param viewFlightsWithLesserSeats
	 * @param radioPrifix
	 * @param isNextPrevious
	 * @param setSeatAvailFrmCount
	 *            - Decide to show the seat count based on the actual seat availability rather than the value returned
	 *            from OndOptionTO This is used in next previous to show available flights that might have return fare
	 * @param busCarrierCodes
	 * @return
	 * @throws ModuleException
	 */
	public static Collection<AvailableFlightInfo> createAvailFlightInfoList(BaseAvailRS baseAvailRS, FlightSearchDTO searchDTO,
			boolean viewAvalableSeats, boolean viewFlightsWithLesserSeats, String radioPrifix, boolean isNextPrevious,
			boolean setSeatAvailFrmCount, Set<String> busCarrierCodes) throws ModuleException {
		String origin = null;
		String destination = null;
		boolean returnFlag = false;
		if (!"depature".equals(radioPrifix)) {
			returnFlag = true;
		}
		if ("depature".equals(radioPrifix) || isNextPrevious) {
			origin = searchDTO.getFromAirport();
			destination = searchDTO.getToAirport();
		} else {
			if (searchDTO.isOpenReturnConfirm()) {
				origin = searchDTO.getFromAirport();
				destination = searchDTO.getToAirport();
			} else {
				origin = searchDTO.getToAirport();
				destination = searchDTO.getFromAirport();
			}
		}

		int intCount = 1;

		int adultCount = searchDTO.getAdultCount() + searchDTO.getChildCount();
		int infantCount = searchDTO.getInfantCount();
		List<AvailableFlightInfo> flightInfoList = new ArrayList<AvailableFlightInfo>();
		Date lastDepartureDate = null;
		boolean needSorting = false;
		for (OriginDestinationInformationTO ondInfoTO : baseAvailRS.getOriginDestinationInformationList()) {

			if (isInValidOriginDestinationInformation(isNextPrevious, returnFlag, ondInfoTO.isReturnFlag())) {
				continue;
			}
			ondFor: for (OriginDestinationOptionTO ondOpTO : ondInfoTO.getOrignDestinationOptions()) {
				boolean seatAvailFromCount = true;
				AvailableFlightInfo availableFlightInfo = new AvailableFlightInfo(viewAvalableSeats);
				int i = 0;
				Collections.sort(ondOpTO.getFlightSegmentList());
				for (FlightSegmentTO flightSegmentTO : ondOpTO.getFlightSegmentList()) {
					if (!viewFlightsWithLesserSeats && flightSegmentTO.isOverbookEnabled()) {
						viewFlightsWithLesserSeats = true;
					}
					if (!viewFlightsWithLesserSeats && !flightSegmentTO.hasRequestingNumOfSeats(adultCount, infantCount)) {
						break ondFor;
					}
					availableFlightInfo.addFlightSegment(flightSegmentTO);
					if (busCarrierCodes != null && WPModuleUtils.getAirportBD().isBusSegment(flightSegmentTO.getSegmentCode())) {
						busCarrierCodes.add(flightSegmentTO.getOperatingAirline());
					}
					if (!flightSegmentTO.hasRequestingNumOfSeats(adultCount, infantCount)) {
						seatAvailFromCount = false;
					}
					availableFlightInfo.addFlightStopOverDurationList(getStopOverDuration(ondOpTO.getFlightSegmentList(), i));
					i++;
				}
				availableFlightInfo.setSystem(ondOpTO.getSystem().toString());
				if (AppSysParamsUtil.isAllCoSChangeFareEnabled()) {
					availableFlightInfo.setSeatAvailable(true);
				} else {
					if (setSeatAvailFrmCount) {
						availableFlightInfo.setSeatAvailable(seatAvailFromCount);
					} else {
						availableFlightInfo.setSeatAvailable(ondOpTO.isSeatAvailable());
					}
				}

				// set whether flight is selected flight or not
				if (!isNextPrevious && ondOpTO.isSelected()) {
					availableFlightInfo.setSelected(true);
				} else if (intCount == 1 && (searchDTO.isOpenReturnConfirm())) {
					availableFlightInfo.setSelected(true);
				}

				if (intCount != 1) {
					if (!needSorting && lastDepartureDate.after(availableFlightInfo.getFirstDepartureDate())) {
						needSorting = true;
					}
				}
				lastDepartureDate = availableFlightInfo.getFirstDepartureDate();

				availableFlightInfo.setReturnFlag(returnFlag);
				availableFlightInfo.setIndex(intCount);
				flightInfoList.add(availableFlightInfo);
				intCount++;
			}
		}
		if (needSorting) {
			Collections.sort(flightInfoList);
			intCount = 1;
			for (AvailableFlightInfo avi : flightInfoList) {
				avi.setIndex(intCount++);
			}
		}
		return flightInfoList;
	}

	public static List<List<AvailableFlightInfo>> createAvailFlightInfoList(BaseAvailRS baseAvailRS, FlightSearchDTO searchDTO,
			boolean viewAvalableSeats, boolean viewFlightsWithLesserSeats, boolean setSeatAvailFrmCount,
			Set<String> busCarrierCodes) throws ModuleException {

		int intCount = 1;

		int adultCount = searchDTO.getAdultCount() + searchDTO.getChildCount();
		int infantCount = searchDTO.getInfantCount();
		List<List<AvailableFlightInfo>> ondAvlFltList = new ArrayList<List<AvailableFlightInfo>>();
		for (OriginDestinationInformationTO ondInfoTO : baseAvailRS.getOriginDestinationInformationList()) {
			List<AvailableFlightInfo> flightInfoList = new ArrayList<AvailableFlightInfo>();
			ondAvlFltList.add(flightInfoList);
			Date lastDepartureDate = null;
			boolean needSorting = false;
			ondFor: for (OriginDestinationOptionTO ondOpTO : ondInfoTO.getOrignDestinationOptions()) {
				boolean seatAvailFromCount = true;
				AvailableFlightInfo availableFlightInfo = new AvailableFlightInfo(viewAvalableSeats);
				int i = 0;
				Collections.sort(ondOpTO.getFlightSegmentList());
				for (FlightSegmentTO flightSegmentTO : ondOpTO.getFlightSegmentList()) {
					if (!viewFlightsWithLesserSeats && !flightSegmentTO.hasRequestingNumOfSeats(adultCount, infantCount)) {
						break ondFor;
					}
					availableFlightInfo.addFlightSegment(flightSegmentTO);
					if (busCarrierCodes != null && WPModuleUtils.getAirportBD().isBusSegment(flightSegmentTO.getSegmentCode())) {
						busCarrierCodes.add(flightSegmentTO.getOperatingAirline());
					}
					if (!flightSegmentTO.hasRequestingNumOfSeats(adultCount, infantCount)) {
						seatAvailFromCount = false;
					}
					availableFlightInfo.addFlightStopOverDurationList(getStopOverDuration(ondOpTO.getFlightSegmentList(), i));
					i++;

				}
				availableFlightInfo.setSystem(ondOpTO.getSystem().toString());
				if (AppSysParamsUtil.isAllCoSChangeFareEnabled()) {
					availableFlightInfo.setSeatAvailable(true);
				} else {
					if (setSeatAvailFrmCount) {
						availableFlightInfo.setSeatAvailable(seatAvailFromCount);
					} else {
						availableFlightInfo.setSeatAvailable(ondOpTO.isSeatAvailable());
					}
				}

				// set whether flight is selected flight or not
				if (ondOpTO.isSelected()) {
					availableFlightInfo.setSelected(true);
				} else if (intCount == 1 && (searchDTO.isOpenReturnConfirm())) {
					availableFlightInfo.setSelected(true);
				}

				if (intCount == 1) {
					lastDepartureDate = availableFlightInfo.getFirstDepartureDate();
				} else {
					if (!needSorting && lastDepartureDate.after(availableFlightInfo.getFirstDepartureDate())) {
						needSorting = true;
					}
				}

				// availableFlightInfo.setReturnFlag(returnFlag);
				availableFlightInfo.setIndex(intCount);
				flightInfoList.add(availableFlightInfo);
				intCount++;
				// }
			}
			if (needSorting) {
				Collections.sort(flightInfoList);
				intCount = 1;
				for (AvailableFlightInfo avi : flightInfoList) {
					avi.setIndex(intCount++);
				}
			}
		}
		return ondAvlFltList;
	}

	/**
	 * method to create AvailableFlightSearchDTO from FlightSearchDTO
	 * 
	 * @param appIndicator
	 *            TODO
	 * 
	 * @throws ParseException
	 * @throws ModuleException
	 */

	public static FlightPriceRQ createFareQuoteRQ(FlightSearchDTO flightSearch, AppIndicatorEnum appIndicator)
			throws ParseException, ModuleException {
		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();
		// setting origin destination information

		// Map to flight seg code & flight seg Id key value for populating cabin class & logical cabin class selection
		// Map<String, Integer> selectedFlightSegMap = new HashMap<String, Integer>();
		int ondSequence = 0;

		// TODO: AIRLINE_163 app param and the changes to be removed after completing
		// "Re-quote only for Required ONDs [AIRLINE_177]" in XBE & IBE.
		// With the AIRLINE_163 changes only segment fares can be skipped but in many of the airlines uses
		// mix of fares SEG/CON/RET and there are many combinations.
		if (AppSysParamsUtil.isSkipNonModifiedSegFareONDs()) {
			List<String> excludedSegFarePnrSegIds = new ArrayList<String>();
			List<ONDSearchDTO> removeOndList = new ArrayList<ONDSearchDTO>();
			for (ONDSearchDTO ond : flightSearch.getOndList()) {
				try {
					if (ond.getModifiedResSegList() == null && (ond.getExistingResSegRPHList() != null
							&& !ond.getExistingResSegRPHList().isEmpty() && getOwnOldPerPaxFareTO(ond) != null
							&& FareTypeCodes.SEGMENT
									.equalsIgnoreCase(SelectListGenerator.getFareType(ond.getExistingResSegRPHList().get(0),
											Integer.toString(getOwnOldPerPaxFareTO(ond).getFareId()))))) {
						excludedSegFarePnrSegIds.addAll(ond.getExistingResSegRPHList());
						removeOndList.add(ond);
					}
				} catch (ModuleException e) {
					// Continue if the fare id is empty.
					e.printStackTrace();
				}
			}
			if (!removeOndList.isEmpty()) {
				flightSearch.getOndList().removeAll(removeOndList);
			}
			flightSearch.setExcludedSegFarePnrSegIds(excludedSegFarePnrSegIds);
		}

		List<Integer> unTouchedOndList = new ArrayList<Integer>();
		List<String> unTouchedFltRPHList = new ArrayList<String>();
		List<String> sameFareRequired = new ArrayList<String>();
		Map<Integer, List<Integer>> existRetGropMap = new HashMap<Integer, List<Integer>>();

		if (AppSysParamsUtil.isReQuoteOnlyRequiredONDs()) {
			int seq = 0;
			List<Integer> skipOndList = getSkipFareQuoteOnD(flightSearch.getOndList(), unTouchedOndList, unTouchedFltRPHList);
			for (ONDSearchDTO ond : flightSearch.getOndList()) {
				if (skipOndList.contains(seq)) {
					sameFareRequired.addAll(ond.getFlightRPHList());
				}
				seq++;
			}
		}

		for (ONDSearchDTO ond : flightSearch.getOndList()) {
			OriginDestinationInformationTO ondInfoTO = flightPriceRQ.addNewOriginDestinationInformation();
			ondInfoTO.setOrigin(ond.getFromAirport());
			ondInfoTO.setDestination(ond.getToAirport());

			Date depatureDate = ond.getDepartureDate();

			validateDateVarience(depatureDate, ond.getDepartureVariance(), appIndicator);

			Date depatureDateTimeStart = CalendarUtil.getStartTimeOfDate(depatureDate);
			depatureDateTimeStart = CalendarUtil.getOfssetAddedTime(depatureDateTimeStart, ond.getDepartureVariance() * -1);

			Date depatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(depatureDate);
			depatureDateTimeEnd = CalendarUtil.getOfssetAddedTime(depatureDateTimeEnd, ond.getDepartureVariance());

			ondInfoTO.setPreferredDate(depatureDate);
			ondInfoTO.setDepartureDateTimeStart(depatureDateTimeStart);
			ondInfoTO.setDepartureDateTimeEnd(depatureDateTimeEnd);
			ondInfoTO.setPreferredLogicalCabin(flightSearch.getPreferredLogicalCC(ondSequence));
			ondInfoTO.setPreferredClassOfService(flightSearch.getPreferredClassOfService(ondSequence));
			ondInfoTO.setPreferredBookingClass(flightSearch.getPreferredBookingClass(ondSequence));
			ondInfoTO.setPreferredBookingType(flightSearch.getPreferredBookingType(ondSequence));
			if (ond.getBundledServicePeriodId() != null) {
				ondInfoTO.setPreferredBundleFarePeriodId(ond.getBundledServicePeriodId());
			} else {
				ondInfoTO.setPreferredBundleFarePeriodId(flightSearch.getPreferredBundledFarePeriodId(ondSequence));
			}
			ondInfoTO.setSegmentBookingClassSelection(flightSearch.getPreferredOndSegmentBookingClasses(ondSequence));
			ondInfoTO.setStatus(ond.getStatus());
			ondInfoTO.setSameFlightModification(ond.isSameFlightModification());

			if (AppSysParamsUtil.isEnforceSameHigher() && ond.getOldPerPaxFare() != null) {
				ondInfoTO.setOldPerPaxFare(ond.getOldPerPaxFare());
				ondInfoTO.setOldPerPaxFareTOList(ond.getOldPerPaxFareTOList());
			}

			if (AppSysParamsUtil.isReQuoteOnlyRequiredONDs() && (!flightSearch.isFromNameChange() || ond.isFlownOnd())) {

				if (ond.getOldPerPaxFareTOList() != null) {
					ondInfoTO.setOldPerPaxFareTOList(ond.getOldPerPaxFareTOList());
				}

				if (unTouchedFltRPHList.containsAll(ond.getFlightRPHList())
						&& sameFareRequired.containsAll(ond.getFlightRPHList())) {

					if (ond.getExistingResSegRPHList() != null && ond.getExistingResSegRPHList().size() > 0) {
						List<Integer> flightSegmentIds = new ArrayList<Integer>();
						ondInfoTO.setUnTouchedResSegList(ond.getExistingResSegRPHList());

						ondInfoTO.setUnTouchedOnd(true);

						FareTO oldPerPaxFareTO = getOwnOldPerPaxFareTO(ond);

						if (oldPerPaxFareTO != null && oldPerPaxFareTO.getReturnGroupId() > 0) {
							int returnGrpId = oldPerPaxFareTO.getReturnGroupId();
							List<Integer> pnrSegIdList = new ArrayList<Integer>();

							for (String flightRefNumber : ond.getExistingFlightRPHList()) {
								if (flightRefNumber.indexOf("#") != -1) {
									String arr[] = flightRefNumber.split("#");
									flightSegmentIds.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(arr[0]));
								} else {
									flightSegmentIds.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightRefNumber));
								}
							}

							if (existRetGropMap.get(returnGrpId) != null) {
								pnrSegIdList = existRetGropMap.get(returnGrpId);
							}
							pnrSegIdList.addAll(flightSegmentIds);

							existRetGropMap.put(returnGrpId, pnrSegIdList);
						}

					}

				}

			}

			if (AppSysParamsUtil.isSameFareModificationEnabled() || AppSysParamsUtil.isEnforceSameHigher()) {
				if (ond.getOldPerPaxFareTOList() != null) {
					ondInfoTO.setOldPerPaxFareTOList(ond.getOldPerPaxFareTOList());
				}
				ondInfoTO.setEligibleToSameBCMod(ond.isEligibleToSameBCMod());

				if (ond.getDateChangedResSegList() != null) {
					List<Integer> dateChangedPnrSegIds = new ArrayList<Integer>();
					for (String dateChangedPnrSegIdStr : ond.getDateChangedResSegList()) {
						if (dateChangedPnrSegIdStr != null) {
							Integer dateChangedPnrSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(dateChangedPnrSegIdStr);
							if (dateChangedPnrSegId != null) {
								dateChangedPnrSegIds.add(dateChangedPnrSegId);
							}
						}
					}
					ondInfoTO.setDateChangedResSegList(dateChangedPnrSegIds);
				}
			}

			OriginDestinationOptionTO depOndOptionTO = new OriginDestinationOptionTO();
			if (ond.getFlightRPHList() != null) {
				for (String flightRefNumber : ond.getFlightRPHList()) {
					FlightSegmentTO flightSegementTO = new FlightSegmentTO();
					if (flightRefNumber.indexOf("#") != -1) {
						String arr[] = flightRefNumber.split("#");
						flightSegementTO.setFlightRefNumber(arr[0]);
						flightSegementTO.setRouteRefNumber(arr[1]);
						flightSegementTO.setSegmentCode(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(arr[0]));
						flightSegementTO.setFlightNumber(FlightRefNumberUtil.getOperatingAirline(arr[0]));
						flightSegementTO.setOperatingAirline(FlightRefNumberUtil.getOperatingAirline(arr[0]));
					} else {
						flightSegementTO.setFlightRefNumber(flightRefNumber);
						flightSegementTO.setSegmentCode(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(flightRefNumber));
						flightSegementTO.setFlightNumber(FlightRefNumberUtil.getOperatingAirline(flightRefNumber));
						flightSegementTO.setOperatingAirline(FlightRefNumberUtil.getOperatingAirline(flightRefNumber));
					}
					depOndOptionTO.getFlightSegmentList().add(flightSegementTO);
					// selectedFlightSegMap.put(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(fltRefNo),
					// FlightRefNumberUtil.getSegmentIdFromFlightRPH(fltRefNo));
				}
			}

			if (ond.getExistingFlightRPHList() != null) {
				List<Integer> flightSegmentIds = new ArrayList<Integer>();
				for (String flightRefNumber : ond.getExistingFlightRPHList()) {
					if (flightRefNumber.indexOf("#") != -1) {
						String arr[] = flightRefNumber.split("#");
						flightSegmentIds.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(arr[0]));
					} else {
						flightSegmentIds.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightRefNumber));
					}
				}
				ondInfoTO.getExistingFlightSegIds().addAll(flightSegmentIds);
			}

			if (ond.getExistingResSegRPHList() != null) {
				ondInfoTO.getExistingPnrSegRPHs().addAll(ond.getExistingResSegRPHList());
			}

			ondInfoTO.setFlownOnd(ond.isFlownOnd());
			ondInfoTO.setDepartureCitySearch(ond.isFromCitySearch());
			ondInfoTO.setArrivalCitySearch(ond.isToCitySearch());

			ondInfoTO.getOrignDestinationOptions().add(depOndOptionTO);
			ondSequence++;

		}

		// setClassOfServiceSelection(flightPriceRQ.getClassOfServicePrefTO(), flightSearch.getClassOfService(),
		// flightSearch.getLogicalCabinClass(), flightSearch.getFareQuoteLogicalCCSelection(), selectedFlightSegMap);

		// setting traveler information
		TravelerInfoSummaryTO traverlerInfo = flightPriceRQ.getTravelerInfoSummary();

		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
		adultsQuantity.setQuantity(flightSearch.getAdultCount());

		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);
		childQuantity.setQuantity(flightSearch.getChildCount());

		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);
		infantQuantity.setQuantity(flightSearch.getInfantCount());

		// setting traveler preferences
		TravelPreferencesTO travelerPref = flightPriceRQ.getTravelPreferences();
		travelerPref.setBookingType(flightSearch.getBookingType());
		travelerPref.setOpenReturn(flightSearch.getOpenReturn());
		travelerPref.setValidityId(flightSearch.getValidity());
		if (AppSysParamsUtil.isBCSelectionAtAvailabilitySearchEnabled()
				&& StringUtils.isNotEmpty(flightSearch.getBookingClassCode())) {
			travelerPref.setBookingClassCode(flightSearch.getBookingClassCode());
		}
		travelerPref.setOpenReturnConfirm(flightSearch.isOpenReturnConfirm());

		AvailPreferencesTO availPref = flightPriceRQ.getAvailPreferences();
		availPref.setBookingPaxType(flightSearch.getPaxType());
		availPref.setFareCategoryType(flightSearch.getFareType());
		availPref.setLastFareQuotedDate(flightSearch.getLastFareQuoteDate());
		availPref.setTicketValidTill(flightSearch.getTicketValidTill());

		availPref.setHalfReturnFareQuote(flightSearch.isHalfReturnFareQuote());
		availPref.setStayOverTimeInMillis(setDefaultStayOverDaysForReturnFareSearch(flightPriceRQ));

		if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
			availPref.setSearchSystem(SYSTEM.getEnum(flightSearch.getSearchSystem(), SYSTEM.INT));
		} else {
			availPref.setSearchSystem(SYSTEM.getEnum(flightSearch.getSearchSystem(), SYSTEM.AA));
		}

		availPref.setAppIndicator(appIndicator.equals(AppIndicatorEnum.APP_XBE) ? ApplicationEngine.XBE : ApplicationEngine.IBE);
		availPref.setQuoteOndFlexi(flightSearch.getOndQuoteFlexi());
		availPref.setOndFlexiSelected(flightSearch.getOndSelectedFlexi());
		availPref.setTravelAgentCode(flightSearch.getTravelAgentCode());
		availPref.setExcludedCharges(flightSearch.getExcludeCharge());
		availPref.setExistRetGropMap(existRetGropMap);
		availPref.setPromoCode(flightSearch.getPromoCode());
		availPref.setFromNameChange(flightSearch.isFromNameChange());

		return flightPriceRQ;
	}

	public static FlightPriceRQ createFareQuoteRQ(FlightSearchDTO flightSearch, ArrayList<String> outFlightRPHList,
			ArrayList<String> retFlightRPHList) throws ParseException {

		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();
		boolean isEnableCityBasesFunctionality = AppSysParamsUtil.enableCityBasesFunctionality();
		// setting origin destination information
		OriginDestinationInformationTO depatureOnD = flightPriceRQ.addNewOriginDestinationInformation();
		depatureOnD.setOrigin(flightSearch.getFromAirport());
		depatureOnD.setDestination(flightSearch.getToAirport());

		Date depatureDate = DateUtil.parseDate(flightSearch.getDepartureDate(), "dd/MM/yyyy");

		Date depatureDateTimeStart = CalendarUtil.getStartTimeOfDate(depatureDate);
		depatureDateTimeStart = CalendarUtil.getOfssetAddedTime(depatureDateTimeStart, flightSearch.getDepartureVariance() * -1);

		Date depatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(depatureDate);
		depatureDateTimeEnd = CalendarUtil.getOfssetAddedTime(depatureDateTimeEnd, flightSearch.getDepartureVariance());

		depatureOnD.setPreferredDate(depatureDate);
		depatureOnD.setDepartureDateTimeStart(depatureDateTimeStart);
		depatureOnD.setDepartureDateTimeEnd(depatureDateTimeEnd);
		depatureOnD.setPreferredLogicalCabin(flightSearch.getPreferredLogicalCC(OndSequence.OUT_BOUND));
		depatureOnD.setPreferredClassOfService(flightSearch.getPreferredClassOfService(OndSequence.OUT_BOUND));
		depatureOnD.setPreferredBookingClass(flightSearch.getPreferredBookingClass(OndSequence.OUT_BOUND));
		depatureOnD.setPreferredBookingType(flightSearch.getPreferredBookingType(OndSequence.OUT_BOUND));
		depatureOnD.setPreferredBundleFarePeriodId(flightSearch.getPreferredBundledFarePeriodId(OndSequence.OUT_BOUND));
		depatureOnD.setSegmentBookingClassSelection(flightSearch.getPreferredOndSegmentBookingClasses(OndSequence.OUT_BOUND));

		// Map to flight seg code & flight seg Id key value for populating cabin class & logical cabin class selection
		Map<String, Integer> selectedFlightSegMap = new HashMap<String, Integer>();

		OriginDestinationOptionTO depOndOptionTO = new OriginDestinationOptionTO();
		if (outFlightRPHList != null) {
			for (String flightRefNumber : outFlightRPHList) {
				FlightSegmentTO flightSegementTO = new FlightSegmentTO();
				String fltRefNo = "";
				if (flightRefNumber.indexOf("#") != -1) {
					String arr[] = flightRefNumber.split("#");
					flightSegementTO.setFlightRefNumber(arr[0]);
					flightSegementTO.setRouteRefNumber(arr[1]);
					fltRefNo = arr[0];
				} else {
					flightSegementTO.setFlightRefNumber(flightRefNumber);
					fltRefNo = flightRefNumber;
				}
				depOndOptionTO.getFlightSegmentList().add(flightSegementTO);
				selectedFlightSegMap.put(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(fltRefNo),
						FlightRefNumberUtil.getSegmentIdFromFlightRPH(fltRefNo));
			}
		}
		depatureOnD.getOrignDestinationOptions().add(depOndOptionTO);
		updateOriginDestinationCitySearchEnabled(isEnableCityBasesFunctionality, flightSearch.getOndWiseCitySearch(), depatureOnD,
				OndSequence.OUT_BOUND);

		if (!StringUtil.isNullOrEmpty(flightSearch.getReturnDate()) && !flightSearch.getReturnDate().equals("null")
				&& !flightSearch.isOpenReturnConfirm()) {

			OriginDestinationInformationTO returnOnD = flightPriceRQ.addNewOriginDestinationInformation();
			returnOnD.setOrigin(flightSearch.getToAirport());
			returnOnD.setDestination(flightSearch.getFromAirport());

			Date returnDate = DateUtil.parseDate(flightSearch.getReturnDate(), "dd/MM/yyyy");

			Date returnDateTimeStart = CalendarUtil.getStartTimeOfDate(returnDate);
			returnDateTimeStart = CalendarUtil.getOfssetAddedTime(returnDateTimeStart, flightSearch.getReturnVariance() * -1);

			Date returnDateTimeEnd = CalendarUtil.getEndTimeOfDate(returnDate);
			returnDateTimeEnd = CalendarUtil.getOfssetAddedTime(returnDateTimeEnd, flightSearch.getReturnVariance());

			returnOnD.setPreferredDate(returnDate);
			returnOnD.setDepartureDateTimeStart(returnDateTimeStart);
			returnOnD.setDepartureDateTimeEnd(returnDateTimeEnd);
			returnOnD.setPreferredLogicalCabin(flightSearch.getPreferredLogicalCC(OndSequence.IN_BOUND));
			returnOnD.setPreferredClassOfService(flightSearch.getPreferredClassOfService(OndSequence.IN_BOUND));
			returnOnD.setPreferredBookingClass(flightSearch.getPreferredBookingClass(OndSequence.IN_BOUND));
			returnOnD.setPreferredBookingType(flightSearch.getPreferredBookingType(OndSequence.IN_BOUND));
			returnOnD.setPreferredBundleFarePeriodId(flightSearch.getPreferredBundledFarePeriodId(OndSequence.IN_BOUND));
			returnOnD.setSegmentBookingClassSelection(flightSearch.getPreferredOndSegmentBookingClasses(OndSequence.IN_BOUND));

			OriginDestinationOptionTO retOndOptionTO = new OriginDestinationOptionTO();
			if (retFlightRPHList != null) {
				for (String flightRefNumber : retFlightRPHList) {
					FlightSegmentTO flightSegementTO = new FlightSegmentTO();
					String fltRefNo = "";
					if (flightRefNumber.indexOf("#") != -1) {
						String arr[] = flightRefNumber.split("#");
						flightSegementTO.setFlightRefNumber(arr[0]);
						flightSegementTO.setRouteRefNumber(arr[1]);
						fltRefNo = arr[0];
					} else {
						flightSegementTO.setFlightRefNumber(flightRefNumber);
						fltRefNo = flightRefNumber;
					}
					retOndOptionTO.getFlightSegmentList().add(flightSegementTO);
					selectedFlightSegMap.put(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(fltRefNo),
							FlightRefNumberUtil.getSegmentIdFromFlightRPH(fltRefNo));
				}
			}
			updateOriginDestinationCitySearchEnabled(isEnableCityBasesFunctionality, flightSearch.getOndWiseCitySearch(),
					returnOnD, OndSequence.IN_BOUND);
			returnOnD.getOrignDestinationOptions().add(retOndOptionTO);
			returnOnD.setReturnFlag(true);
		}

		// setting traveler information
		TravelerInfoSummaryTO traverlerInfo = flightPriceRQ.getTravelerInfoSummary();

		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
		adultsQuantity.setQuantity(flightSearch.getAdultCount());

		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);
		childQuantity.setQuantity(flightSearch.getChildCount());

		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);
		infantQuantity.setQuantity(flightSearch.getInfantCount());

		// setting traveler preferences
		TravelPreferencesTO travelerPref = flightPriceRQ.getTravelPreferences();
		travelerPref.setBookingType(flightSearch.getBookingType());
		travelerPref.setOpenReturn(flightSearch.getOpenReturn());
		travelerPref.setValidityId(flightSearch.getValidity());
		if (AppSysParamsUtil.isBCSelectionAtAvailabilitySearchEnabled()
				&& StringUtils.isNotEmpty(flightSearch.getBookingClassCode())) {
			travelerPref.setBookingClassCode(flightSearch.getBookingClassCode());
		}
		travelerPref.setOpenReturnConfirm(flightSearch.isOpenReturnConfirm());

		AvailPreferencesTO availPref = flightPriceRQ.getAvailPreferences();
		availPref.setBookingPaxType(flightSearch.getPaxType());
		availPref.setFareCategoryType(flightSearch.getFareType());

		availPref.setHalfReturnFareQuote(flightSearch.isHalfReturnFareQuote());
		availPref.setStayOverTimeInMillis(setDefaultStayOverDaysForReturnFareSearch(flightPriceRQ));

		if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
			availPref.setSearchSystem(SYSTEM.getEnum(flightSearch.getSearchSystem(), SYSTEM.INT));
		} else {
			availPref.setSearchSystem(SYSTEM.getEnum(flightSearch.getSearchSystem(), SYSTEM.AA));
		}
		availPref.setAppIndicator(ApplicationEngine.XBE);
		availPref.setQuoteOndFlexi(flightSearch.getOndQuoteFlexi());
		availPref.setOndFlexiSelected(flightSearch.getOndSelectedFlexi());
		availPref.setExcludedCharges(flightSearch.getExcludeCharge());
		availPref.setPromoCode(flightSearch.getPromoCode());
		if (flightSearch.getBankIdentificationNo() != null && !"".equals(flightSearch.getBankIdentificationNo().trim())) {
			availPref.setBankIdentificationNumber(Integer.parseInt(flightSearch.getBankIdentificationNo().trim()));
		}
		return flightPriceRQ;
	}

	private static long setDefaultStayOverDaysForReturnFareSearch(BaseAvailRQ baseAvailRQ) {
		long stayOverTimeBetweenRequestedDates = 0;
		OndConvertAssembler ondAssm = new OndConvertAssembler(baseAvailRQ.getOriginDestinationInformationList());
		if (ondAssm.isReturnFlight() && !baseAvailRQ.getTravelPreferences().isOpenReturn()) {
			stayOverTimeBetweenRequestedDates = ondAssm.getSelectedInboundDateTimeEnd().getTime()
					- ondAssm.getSelectedOutboundDateTimeStart().getTime();
		}
		return stayOverTimeBetweenRequestedDates;
	}

	/**
	 * 
	 * @param domesticSegmentCodeList
	 * @param internationalSegmentCodeList
	 * @param busCarrierCodes
	 * @param overideSubJourneyId
	 *            TODO
	 * @param returnFlights
	 * @param outboundFlights
	 * @return
	 */
	public static Collection<FlightInfoTO> createSelecteFlightInfo(BaseAvailRS baseAvailRS,
			ArrayList<String> domesticSegmentCodeList, ArrayList<String> internationalSegmentCodeList,
			Set<String> busCarrierCodes, boolean overideSubJourneyId) {

		List<FlightInfoTO> collection = new ArrayList<FlightInfoTO>();

		// Outbound Flights
		// for (OriginDestinationInformationTO originDestinationInformationTO :
		// baseAvailRS.getOriginDestinationInformationList()) {
		// if (origin.compareTo(originDestinationInformationTO.getOrigin()) == 0
		// && destination.compareTo(originDestinationInformationTO.getDestination()) == 0) {
		// for (OriginDestinationOptionTO originDestinationOptionTO : originDestinationInformationTO
		// .getOrignDestinationOptions()) {
		// if (originDestinationOptionTO.isSelected()) {
		// int i = 0;
		// for (FlightSegmentTO flightSegmentTO : originDestinationOptionTO.getFlightSegmentList()) {
		//
		// FlightInfoTO flightInfoTO = new FlightInfoTO(flightSegmentTO);
		//
		// flightInfoTO.setFlightStopOverDuration(getStopOverDuration(
		// originDestinationOptionTO.getFlightSegmentList(), i));
		//
		// collection.add(flightInfoTO);
		// i++;
		// }
		// }
		// }
		// }
		// }

		// Return Flights
		OpenReturnOptionsTO openReturnOptionsTO = baseAvailRS.getOpenReturnOptionsTO();
		int sequence = 0;
		int segmentSequence = 0;
		for (OriginDestinationInformationTO originDestinationInformationTO : baseAvailRS
				.getOrderedOriginDestinationInformationList()) {
			// if (origin.compareTo(originDestinationInformationTO.getDestination()) == 0
			// && destination.compareTo(originDestinationInformationTO.getOrigin()) == 0) {
			for (OriginDestinationOptionTO originDestinationOptionTO : originDestinationInformationTO
					.getOrignDestinationOptions()) {
				if (originDestinationOptionTO.isSelected()) {
					int i = 0;
					for (FlightSegmentTO flightSegmentTO : originDestinationOptionTO.getFlightSegmentList()) {

						// Fill FlightInfoTO object
						FlightInfoTO flightInfoTO = new FlightInfoTO(flightSegmentTO);
						if (openReturnOptionsTO != null && openReturnOptionsTO.getAirportCode() != null) {
							if (openReturnOptionsTO.getAirportCode().compareTo(originDestinationInformationTO.getOrigin()) == 0) {
								flightInfoTO.setOpenReturnSegment(true);
							}
						}

						if (busCarrierCodes != null
								&& AirScheduleCustomConstants.OperationTypes.BUS_SERVICE == flightSegmentTO.getOperationType()) {
							busCarrierCodes.add(flightSegmentTO.getOperatingAirline());
						}

						if (domesticSegmentCodeList != null && flightSegmentTO.isDomesticFlight()) {
							domesticSegmentCodeList.add(flightSegmentTO.getSegmentCode());
						}

						if (internationalSegmentCodeList != null && !flightSegmentTO.isDomesticFlight()) {
							internationalSegmentCodeList.add(flightSegmentTO.getSegmentCode());
						}
						if (AppSysParamsUtil.isEnableOndSplitForBusFareQuote()) {
							if (flightSegmentTO.getOperationType() == 0) {
								try {
									if (WPModuleUtils.getAirportBD().isBusSegment(flightSegmentTO.getSegmentCode())) {
										flightInfoTO.setSegmentTypeId(AirScheduleCustomConstants.SegmentTypes.BUS_SEGMENT);
										if (overideSubJourneyId) {
											flightInfoTO.setSubJourneyGroup(AirScheduleCustomConstants.SegmentTypes.BUS_SEGMENT);
										}
									} else {
										flightInfoTO.setSegmentTypeId(AirScheduleCustomConstants.SegmentTypes.AIR_SEGMENT);
										if (overideSubJourneyId) {
											flightInfoTO.setSubJourneyGroup(AirScheduleCustomConstants.SegmentTypes.AIR_SEGMENT);
										}
									}
								} catch (ModuleException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							} else {
								if (AirScheduleCustomConstants.OperationTypes.BUS_SERVICE == flightSegmentTO.getOperationType()) {
									flightInfoTO.setSegmentTypeId(AirScheduleCustomConstants.SegmentTypes.BUS_SEGMENT);
									if (overideSubJourneyId) {
										flightInfoTO.setSubJourneyGroup(AirScheduleCustomConstants.SegmentTypes.BUS_SEGMENT);
									}
								} else {
									flightInfoTO.setSegmentTypeId(AirScheduleCustomConstants.SegmentTypes.AIR_SEGMENT);
									if (overideSubJourneyId) {
										flightInfoTO.setSubJourneyGroup(AirScheduleCustomConstants.SegmentTypes.AIR_SEGMENT);
									}

								}
							}
							flightInfoTO.setSegmentSequence(segmentSequence);
							segmentSequence++;
						}

						flightInfoTO.setFlightStopOverDuration(
								getStopOverDuration(originDestinationOptionTO.getFlightSegmentList(), i));
						flightInfoTO.setOndSequence(sequence);
						flightInfoTO.setCsOcCarrierCode(flightSegmentTO.getCsOcCarrierCode());
						flightInfoTO.setRequestedOndSequence(flightSegmentTO.getRequestedOndSequence());

						collection.add(flightInfoTO);
						i++;
					}
				}
				// }
			}
			sequence++;
		}
		Collections.sort(collection);
		return collection;
	}

	public static HashMap<String, List<String>> getSegmentsMap(Collection<FlightInfoTO> flightInfo) {
		HashMap<String, List<String>> segmentsMap = new HashMap<String, List<String>>();

		for (FlightInfoTO fltSegmentDTO : flightInfo) {
			List<String> fltSegIdList = new ArrayList<String>();
			String segTypeId = fltSegmentDTO.getSegmentTypeId() + "";
			if (segmentsMap.get(segTypeId) != null && segmentsMap.get(segTypeId).size() > 0) {
				fltSegIdList = segmentsMap.get(segTypeId);
			}
			fltSegIdList.add(fltSegmentDTO.getFlightRefNumber());
			segmentsMap.put(segTypeId, fltSegIdList);
		}
		return segmentsMap;
	}

	// private static BigDecimal getTotalExternalCharges(boolean addOutboundFlexiCharge, boolean addInboundFlexiCharge,
	// BigDecimal flexiOutbound, BigDecimal flexiInbound, BigDecimal flexiOutboundInbound) {
	// BigDecimal totalFlexi = AccelAeroCalculator.getDefaultBigDecimalZero();
	//
	// if (addOutboundFlexiCharge && addInboundFlexiCharge) {
	// totalFlexi = flexiOutboundInbound;
	// } else {
	// if (addOutboundFlexiCharge) {
	// totalFlexi = flexiOutbound;
	// }
	//
	// if (addInboundFlexiCharge) {
	// totalFlexi = flexiInbound;
	// // }
	// }
	// }
	//
	// return totalFlexi;
	// }

	/**
	 * 
	 * @param fareTypeTO
	 * @param isOnHoldRestricted
	 * @param fareDiscountAmount
	 * @return
	 */
	public static FareQuoteSummaryTO createFareQuoteSummary(FareTypeTO fareTypeTO, boolean isOnHoldRestricted, String curencyCode,
			String selectedCurrency, Collection<FareQuoteTO> colFareQuoteTO, BigDecimal handlingCharge,
			BigDecimal fareDiscountAmount) throws ModuleException {
		FareQuoteSummaryTO fareQuoteSummaryTO = new FareQuoteSummaryTO();
		Map<Integer, Boolean> ondFlexiSelection = fareTypeTO.getOndFlexiSelection();

		// Calculate applicable external charge based on selected flexi
		BigDecimal applicableExternalCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

		int ondSequence = 0;
		for (ONDExternalChargeTO ondExtChg : fareTypeTO.getOndExternalCharges()) {
			if (!ondFlexiSelection.isEmpty() && ondFlexiSelection.get(ondSequence) != null
					&& ondFlexiSelection.get(ondSequence)) {
				applicableExternalCharges = AccelAeroCalculator.add(applicableExternalCharges,
						ondExtChg.getTotalOndExternalCharges());
			}
			ondSequence++;
		}

		BigDecimal unRoundedTotalAmount = getUnRoundedTotalAmount(colFareQuoteTO);
		BigDecimal totalPrice = fareTypeTO.getTotalPrice();
		if (fareDiscountAmount != null) {
			totalPrice = AccelAeroCalculator.add(totalPrice, fareDiscountAmount.negate());
			fareQuoteSummaryTO.setDiscountedFareAmount(AccelAeroCalculator.formatAsDecimal(fareDiscountAmount));
		}
		BigDecimal totalBookingPrice = AccelAeroCalculator.add(totalPrice, handlingCharge);

		fareQuoteSummaryTO.setBaseFare(AccelAeroCalculator.formatAsDecimal(fareTypeTO.getTotalBaseFare()));
		fareQuoteSummaryTO.setCurrency(curencyCode);
		fareQuoteSummaryTO.setOnHoldRestricted(isOnHoldRestricted);

		BigDecimal totSurcharge = AccelAeroCalculator.add(fareTypeTO.getTotalSurcharges(), applicableExternalCharges);
		fareQuoteSummaryTO.setTotalSurcharges(AccelAeroCalculator.formatAsDecimal(totSurcharge));

		fareQuoteSummaryTO.setTotalTaxes(AccelAeroCalculator.formatAsDecimal(fareTypeTO.getTotalTaxes()));
		fareQuoteSummaryTO.setTotalCurrencyRoundUpDiff(
				AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.subtract(totalPrice, unRoundedTotalAmount)));
		fareQuoteSummaryTO.setTotalHandlingCharge(AccelAeroCalculator.formatAsDecimal(handlingCharge));

		BigDecimal totHandlingCharge = AccelAeroCalculator.add(totalPrice, applicableExternalCharges);
		fareQuoteSummaryTO.setTotalWithoutHandlingCharge(AccelAeroCalculator.formatAsDecimal(totHandlingCharge));

		fareQuoteSummaryTO.setFareType(String.valueOf(fareTypeTO.getFareType()));

		BigDecimal totPrice = AccelAeroCalculator.add(totalBookingPrice, applicableExternalCharges);
		fareQuoteSummaryTO.setTotalPrice(AccelAeroCalculator.formatAsDecimal(totPrice));

		fareQuoteSummaryTO.setSelectedCurrency(selectedCurrency);

		// Geting the exchangeRate
		CurrencyExchangeRate currencyExchangeRate = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu())
				.getCurrencyExchangeRate(selectedCurrency);
		Currency currency = currencyExchangeRate.getCurrency();

		fareQuoteSummaryTO.setSelectedEXRate(currencyExchangeRate.getExrateBaseToCurNumber().toPlainString());

		BigDecimal selectedTotPrice = AccelAeroCalculator.add(totalBookingPrice, applicableExternalCharges);
		fareQuoteSummaryTO.setSelectedtotalPrice(AccelAeroCalculator
				.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(),
						selectedTotPrice, currency.getBoundryValue(), currency.getBreakPoint())));

		return fareQuoteSummaryTO;
	}

	private static BigDecimal getUnRoundedTotalAmount(Collection<FareQuoteTO> colFareQuoteTO) {
		BigDecimal unRoundedTotalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (colFareQuoteTO != null && colFareQuoteTO.size() > 0) {
			for (FareQuoteTO fareQuoteTO : colFareQuoteTO) {
				unRoundedTotalAmount = AccelAeroCalculator.add(unRoundedTotalAmount, new BigDecimal(fareQuoteTO.getTotalPrice()));
			}
		}

		return unRoundedTotalAmount;
	}

	/**
	 * This method is written assuming same type of passengers are having same taxes, surcharges and fares.
	 * 
	 * @param fareTypeTO
	 * @param selectedIBOndCode
	 * @param selectedOBOndCode
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static void fillFareQuote(Collection<FareQuoteTO> fareCollection, Collection<PaxFareTO> paxFareTOs,
			Collection<PaxPriceTO> paxPriceTOs, Collection<SurchargeTO> surchargeTOs, Collection<TaxTO> taxTOs,
			List<Collection<ExternalChargeTO>> ondExternalChargeTOs, FareTypeTO fareTypeTO, List<String> selectedOndCodeList,
			List<Set<String>> selectedOndSegCodes, String selectedCurrency, ExchangeRateProxy exchangeRateProxy)
			throws ModuleException {

		Collection<PerPaxPriceInfoTO> perPriceInfoCollection = fareTypeTO.getPerPaxPriceInfo();
		Map<Integer, Boolean> ondFlexiSelection = fareTypeTO.getOndFlexiSelection();

		// if no selected flight found
		if (selectedOndCodeList == null || selectedOndCodeList.isEmpty()) {
			return;
		}

		int totalAdultCount = 0;
		int totalChildCount = 0;
		int totalInfantCount = 0;

		boolean perAdultPriceCalculated = false;
		boolean perChildPriceCalculated = false;
		boolean perInfantPriceCalculated = false;

		List<BigDecimalWrapper> ppAdultONDFares = new ArrayList<BigDecimalWrapper>();
		List<BigDecimalWrapper> ppChildONDFares = new ArrayList<BigDecimalWrapper>();
		List<BigDecimalWrapper> ppInfantONDFares = new ArrayList<BigDecimalWrapper>();

		List<BigDecimalWrapper> ppAdultONDTaxes = new ArrayList<BigDecimalWrapper>();
		List<BigDecimalWrapper> ppChildONDTaxes = new ArrayList<BigDecimalWrapper>();
		List<BigDecimalWrapper> ppInfantONDTaxes = new ArrayList<BigDecimalWrapper>();

		List<BigDecimalWrapper> ppAdultONDSurcharges = new ArrayList<BigDecimalWrapper>();
		List<BigDecimalWrapper> ppChildONDSurcharges = new ArrayList<BigDecimalWrapper>();
		List<BigDecimalWrapper> ppInfantONDSurcharges = new ArrayList<BigDecimalWrapper>();

		List<BigDecimalWrapper> ppAdultONDFees = new ArrayList<BigDecimalWrapper>();
		List<BigDecimalWrapper> ppChildONDFees = new ArrayList<BigDecimalWrapper>();
		List<BigDecimalWrapper> ppInfantONDFees = new ArrayList<BigDecimalWrapper>();

		List<BigDecimalWrapper> ppAdultONDExternalCharges = new ArrayList<BigDecimalWrapper>();
		List<BigDecimalWrapper> ppChildONDExternalCharges = new ArrayList<BigDecimalWrapper>();
		List<BigDecimalWrapper> ppInfantONDExternalCharges = new ArrayList<BigDecimalWrapper>();

		Collection<SurchargeTO> adultSurchargeTOs = new ArrayList<SurchargeTO>();
		Collection<SurchargeTO> childSurchargeTOs = new ArrayList<SurchargeTO>();
		Collection<SurchargeTO> infantSurchargeTOs = new ArrayList<SurchargeTO>();

		Collection<TaxTO> adultTaxTOs = new ArrayList<TaxTO>();
		Collection<TaxTO> childTaxTOs = new ArrayList<TaxTO>();
		Collection<TaxTO> infantTaxTOs = new ArrayList<TaxTO>();

		List<Collection<ExternalChargeTO>> adultONDExternalChargeTOs = new ArrayList<Collection<ExternalChargeTO>>();
		List<Collection<ExternalChargeTO>> childONDExternalChargeTOs = new ArrayList<Collection<ExternalChargeTO>>();
		List<Collection<ExternalChargeTO>> infantONDExternalChargeTOs = new ArrayList<Collection<ExternalChargeTO>>();

		for (int ondSequence = 0; ondSequence < selectedOndCodeList.size(); ondSequence++) {
			ppAdultONDFares.add(ondSequence, new BigDecimalWrapper());
			ppChildONDFares.add(ondSequence, new BigDecimalWrapper());
			ppInfantONDFares.add(ondSequence, new BigDecimalWrapper());

			ppAdultONDTaxes.add(ondSequence, new BigDecimalWrapper());
			ppChildONDTaxes.add(ondSequence, new BigDecimalWrapper());
			ppInfantONDTaxes.add(ondSequence, new BigDecimalWrapper());

			ppAdultONDSurcharges.add(ondSequence, new BigDecimalWrapper());
			ppChildONDSurcharges.add(ondSequence, new BigDecimalWrapper());
			ppInfantONDSurcharges.add(ondSequence, new BigDecimalWrapper());

			ppAdultONDFees.add(ondSequence, new BigDecimalWrapper());
			ppChildONDFees.add(ondSequence, new BigDecimalWrapper());
			ppInfantONDFees.add(ondSequence, new BigDecimalWrapper());

			ppAdultONDExternalCharges.add(ondSequence, new BigDecimalWrapper());
			ppChildONDExternalCharges.add(ondSequence, new BigDecimalWrapper());
			ppInfantONDExternalCharges.add(ondSequence, new BigDecimalWrapper());

			adultONDExternalChargeTOs.add(ondSequence, new ArrayList<ExternalChargeTO>());
			childONDExternalChargeTOs.add(ondSequence, new ArrayList<ExternalChargeTO>());
			infantONDExternalChargeTOs.add(ondSequence, new ArrayList<ExternalChargeTO>());
		}

		for (PerPaxPriceInfoTO perPaxPriceInfoTO : perPriceInfoCollection) {
			if (perPaxPriceInfoTO.getPassengerType().compareTo(PaxTypeTO.ADULT) == 0) {

				totalAdultCount++;
				// assume all adults have same fares, taxes, surcharges and fees
				if (!perAdultPriceCalculated) {
					findPerPaxPrices(perPaxPriceInfoTO, ppAdultONDFares, ppAdultONDTaxes, ppAdultONDSurcharges, ppAdultONDFees,
							ppAdultONDExternalCharges);
					adultSurchargeTOs.addAll(perPaxPriceInfoTO.getPassengerPrice().getSurcharges());
					adultTaxTOs.addAll(perPaxPriceInfoTO.getPassengerPrice().getTaxes());

					int ondSequence = 0;
					for (ONDExternalChargeTO ondExtChg : perPaxPriceInfoTO.getPassengerPrice().getOndExternalCharges()) {
						if (!ondFlexiSelection.isEmpty() && ondFlexiSelection.get(ondSequence) != null
								&& ondFlexiSelection.get(ondSequence)) {
							adultONDExternalChargeTOs.get(ondSequence).addAll(ondExtChg.getExternalCharges());
						}
						ondSequence++;
					}
					perAdultPriceCalculated = true;
				}

			} else if (perPaxPriceInfoTO.getPassengerType().compareTo(PaxTypeTO.CHILD) == 0) {

				totalChildCount++;
				// assume all children have same fares, taxes, surcharges and
				// fees
				if (!perChildPriceCalculated) {
					findPerPaxPrices(perPaxPriceInfoTO, ppChildONDFares, ppChildONDTaxes, ppChildONDSurcharges, ppChildONDFees,
							ppChildONDExternalCharges);
					childSurchargeTOs.addAll(perPaxPriceInfoTO.getPassengerPrice().getSurcharges());
					childTaxTOs.addAll(perPaxPriceInfoTO.getPassengerPrice().getTaxes());
					int ondSequence = 0;
					for (ONDExternalChargeTO ondExtChg : perPaxPriceInfoTO.getPassengerPrice().getOndExternalCharges()) {
						if (!ondFlexiSelection.isEmpty() && ondFlexiSelection.get(ondSequence) != null
								&& ondFlexiSelection.get(ondSequence)) {
							adultONDExternalChargeTOs.get(ondSequence).addAll(ondExtChg.getExternalCharges());
						}
						ondSequence++;
					}
					perChildPriceCalculated = true;
				}

			} else if (perPaxPriceInfoTO.getPassengerType().compareTo(PaxTypeTO.INFANT) == 0) {

				totalInfantCount++;
				// assume all infants have same fares, taxes, surcharges and
				// fees
				if (!perInfantPriceCalculated) {
					findPerPaxPrices(perPaxPriceInfoTO, ppInfantONDFares, ppInfantONDTaxes, ppInfantONDSurcharges,
							ppInfantONDFees, ppInfantONDExternalCharges);
					infantSurchargeTOs.addAll(perPaxPriceInfoTO.getPassengerPrice().getSurcharges());
					infantTaxTOs.addAll(perPaxPriceInfoTO.getPassengerPrice().getTaxes());
					int ondSequence = 0;
					for (ONDExternalChargeTO ondExtChg : perPaxPriceInfoTO.getPassengerPrice().getOndExternalCharges()) {
						if (!ondFlexiSelection.isEmpty() && ondFlexiSelection.get(ondSequence) != null
								&& ondFlexiSelection.get(ondSequence)) {
							adultONDExternalChargeTOs.get(ondSequence).addAll(ondExtChg.getExternalCharges());
						}
						ondSequence++;
					}
					perInfantPriceCalculated = true;
				}

			} else {
				// TODO : Fix error message
				throw new ModuleException(
						"Wrong passenger type detected! Passenger type passed = [" + perPaxPriceInfoTO.getPassengerType() + "]");
			}
		}

		meargeBasedOnCodeAndAddAllSurcharges(surchargeTOs, adultSurchargeTOs);
		meargeBasedOnCodeAndAddAllSurcharges(surchargeTOs, childSurchargeTOs);
		meargeBasedOnCodeAndAddAllSurcharges(surchargeTOs, infantSurchargeTOs);

		meargeBasedOnCodeAndAddAllTaxes(taxTOs, adultTaxTOs);
		meargeBasedOnCodeAndAddAllTaxes(taxTOs, childTaxTOs);
		meargeBasedOnCodeAndAddAllTaxes(taxTOs, infantTaxTOs);

		// Setting this to Zero when creating no other charges
		BigDecimalWrapper ppOther = new BigDecimalWrapper();

		BigDecimalWrapper ppAdultFare = new BigDecimalWrapper();
		BigDecimalWrapper ppAdultSurcharge = new BigDecimalWrapper();
		BigDecimalWrapper ppAdultTax = new BigDecimalWrapper();
		BigDecimalWrapper ppAdultflexiChargeWrapper = new BigDecimalWrapper();

		BigDecimalWrapper ppChildFare = new BigDecimalWrapper();
		BigDecimalWrapper ppChildSurcharge = new BigDecimalWrapper();
		BigDecimalWrapper ppChildTax = new BigDecimalWrapper();
		BigDecimalWrapper ppChildflexiChargeWrapper = new BigDecimalWrapper();

		BigDecimalWrapper ppInfantFare = new BigDecimalWrapper();
		BigDecimalWrapper ppInfantSurcharge = new BigDecimalWrapper();
		BigDecimalWrapper ppInfantTax = new BigDecimalWrapper();
		BigDecimalWrapper ppInfantflexiChargeWrapper = new BigDecimalWrapper();

		for (int ondSequence = 0; ondSequence < selectedOndCodeList.size(); ondSequence++) {
			// Setting the departure fare information
			Collection<PaxFareTO> collectionONDPaxWise = new ArrayList<PaxFareTO>();
			BigDecimalWrapper totalONDPrice = new BigDecimalWrapper();

			if (totalAdultCount > 0) {

				if (!ondFlexiSelection.isEmpty() && ondFlexiSelection.get(ondSequence) != null
						&& ondFlexiSelection.get(ondSequence)) {
					ppAdultONDSurcharges.get(ondSequence).add(ppAdultONDExternalCharges.get(ondSequence).getValue());
				}

				PaxFareTO adultFareTO = createPaxFareTOWhileAddingTotalPrice(PaxTypeTO.PAX_TYPE_ADULT_DISPLAY, totalAdultCount,
						totalONDPrice, ppAdultONDFares.get(ondSequence), ppAdultONDSurcharges.get(ondSequence),
						ppAdultONDTaxes.get(ondSequence), ppOther, selectedCurrency, true, exchangeRateProxy);
				collectionONDPaxWise.add(adultFareTO);
			}
			if (totalChildCount > 0) {

				if (!ondFlexiSelection.isEmpty() && ondFlexiSelection.get(ondSequence) != null
						&& ondFlexiSelection.get(ondSequence) != null && ondFlexiSelection.get(ondSequence)) {
					ppChildONDSurcharges.get(ondSequence).add(ppChildONDExternalCharges.get(ondSequence).getValue());
				}

				PaxFareTO childFareTO = createPaxFareTOWhileAddingTotalPrice(PaxTypeTO.PAX_TYPE_CHILD_DISPLAY, totalChildCount,
						totalONDPrice, ppChildONDFares.get(ondSequence), ppChildONDSurcharges.get(ondSequence),
						ppChildONDTaxes.get(ondSequence), ppOther, selectedCurrency, true, exchangeRateProxy);
				collectionONDPaxWise.add(childFareTO);
			}
			if (totalInfantCount > 0) {

				if (!ondFlexiSelection.isEmpty() && ondFlexiSelection.get(ondSequence) != null
						&& ondFlexiSelection.get(ondSequence) != null && ondFlexiSelection.get(ondSequence)) {
					ppInfantONDSurcharges.get(ondSequence).add(ppInfantONDExternalCharges.get(ondSequence).getValue());
				}

				PaxFareTO intantFareTO = createPaxFareTOWhileAddingTotalPrice(PaxTypeTO.PAX_TYPE_INFANT_DISPLAY, totalInfantCount,
						totalONDPrice, ppInfantONDFares.get(ondSequence), ppInfantONDSurcharges.get(ondSequence),
						ppInfantONDTaxes.get(ondSequence), ppOther, selectedCurrency, true, exchangeRateProxy);
				collectionONDPaxWise.add(intantFareTO);
			}

			// Getting segment break up of base fares
			Collection<SegmentFareTO> collectionDepSegs = new ArrayList<SegmentFareTO>();
			String paxType = "";

			for (String segCode : selectedOndSegCodes.get(ondSequence)) {
				BigDecimalWrapper depTotalPrice = new BigDecimalWrapper();
				Collection<PaxFareTO> collectionONDSegWisePaxWise = new ArrayList<PaxFareTO>();
				for (PerPaxPriceInfoTO perPaxPriceInfoTO : perPriceInfoCollection) {
					BigDecimalWrapper depBaseFare = new BigDecimalWrapper();
					PaxFareTO paxFareTO = null;
					boolean fareExists = false;
					if (perPaxPriceInfoTO.getPassengerType().compareTo(PaxTypeTO.ADULT) == 0) {
						fareExists = findPerSegmentFares(perPaxPriceInfoTO.getPassengerPrice().getBaseFares(), depBaseFare,
								segCode, ondSequence, PaxTypeTO.ADULT);
						findPerSegmentTotal(perPaxPriceInfoTO.getPassengerPrice(), depTotalPrice, segCode, ondSequence,
								PaxTypeTO.ADULT);
						paxType = PaxTypeTO.ADULT;
					} else if (perPaxPriceInfoTO.getPassengerType().compareTo(PaxTypeTO.CHILD) == 0) {
						fareExists = findPerSegmentFares(perPaxPriceInfoTO.getPassengerPrice().getBaseFares(), depBaseFare,
								segCode, ondSequence, PaxTypeTO.CHILD);
						findPerSegmentTotal(perPaxPriceInfoTO.getPassengerPrice(), depTotalPrice, segCode, ondSequence,
								PaxTypeTO.CHILD);
						paxType = PaxTypeTO.CHILD;
					} else if (perPaxPriceInfoTO.getPassengerType().compareTo(PaxTypeTO.INFANT) == 0) {
						fareExists = findPerSegmentFares(perPaxPriceInfoTO.getPassengerPrice().getBaseFares(), depBaseFare,
								segCode, ondSequence, PaxTypeTO.INFANT);
						findPerSegmentTotal(perPaxPriceInfoTO.getPassengerPrice(), depTotalPrice, segCode, ondSequence,
								PaxTypeTO.INFANT);
						paxType = PaxTypeTO.INFANT;
					}

					if (fareExists) {
						paxFareTO = new PaxFareTO();
						paxFareTO.setPaxType(paxType);
						paxFareTO.setFare(depBaseFare.getDisplayValue());
						collectionONDSegWisePaxWise.add(paxFareTO);
					}
				}
				if (collectionONDSegWisePaxWise.size() > 0) {
					SegmentFareTO segmentFareTO = new SegmentFareTO();
					segmentFareTO.setSegmentCode(segCode);
					segmentFareTO.setTotalPrice(depTotalPrice.getDisplayValue());
					segmentFareTO.setPaxWise(collectionONDSegWisePaxWise);
					collectionDepSegs.add(segmentFareTO);
				}

			}

			ppAdultFare.add(ppAdultONDFares.get(ondSequence).getValue());
			ppAdultSurcharge.add(ppAdultONDSurcharges.get(ondSequence).getValue());
			ppAdultTax.add(ppAdultONDTaxes.get(ondSequence).getValue());
			if (!ondFlexiSelection.isEmpty() && ondFlexiSelection.get(ondSequence) != null
					&& ondFlexiSelection.get(ondSequence)) {
				ppAdultflexiChargeWrapper.add(ppAdultONDExternalCharges.get(ondSequence).getValue());
			}

			ppChildFare.add(ppChildONDFares.get(ondSequence).getValue());
			ppChildSurcharge.add(ppChildONDSurcharges.get(ondSequence).getValue());
			ppChildTax.add(ppChildONDTaxes.get(ondSequence).getValue());
			if (!ondFlexiSelection.isEmpty() && ondFlexiSelection.get(ondSequence) != null
					&& ondFlexiSelection.get(ondSequence)) {
				ppChildflexiChargeWrapper.add(ppChildONDExternalCharges.get(ondSequence).getValue());
			}

			ppInfantFare.add(ppInfantONDFares.get(ondSequence).getValue());
			ppInfantSurcharge.add(ppInfantONDSurcharges.get(ondSequence).getValue());
			ppInfantTax.add(ppInfantONDTaxes.get(ondSequence).getValue());
			if (!ondFlexiSelection.isEmpty() && ondFlexiSelection.get(ondSequence) != null
					&& ondFlexiSelection.get(ondSequence)) {
				ppInfantflexiChargeWrapper.add(ppInfantONDExternalCharges.get(ondSequence).getValue());
			}

			Collection<ExternalChargeTO> colExternalCharges = new ArrayList<ExternalChargeTO>();
			if (adultONDExternalChargeTOs.get(ondSequence) != null) {
				colExternalCharges.addAll(adultONDExternalChargeTOs.get(ondSequence));
			}
			if (childONDExternalChargeTOs.get(ondSequence) != null) {
				colExternalCharges.addAll(childONDExternalChargeTOs.get(ondSequence));
			}
			if (infantONDExternalChargeTOs.get(ondSequence) != null) {
				colExternalCharges.addAll(infantONDExternalChargeTOs.get(ondSequence));
			}
			ondExternalChargeTOs.add(ondSequence, colExternalCharges);

			// findPerSegmentFares

			FareQuoteTO depFareQuoteTO = new FareQuoteTO();
			depFareQuoteTO.setOndCode(selectedOndCodeList.get(ondSequence));
			depFareQuoteTO.setTotalPrice(totalONDPrice.getDisplayValue());

			depFareQuoteTO.setPaxWise(collectionONDPaxWise);
			depFareQuoteTO.setSegmentWise(collectionDepSegs);
			if (fareTypeTO.getOndWiseFareType() != null && !fareTypeTO.getOndWiseFareType().isEmpty()
					&& fareTypeTO.getOndWiseFareType().get(ondSequence) != null) {
				depFareQuoteTO.setFareType(fareTypeTO.getOndWiseFareType().get(ondSequence));
			}
			if (fareTypeTO.getOndWiseFareId() != null && !fareTypeTO.getOndWiseFareId().isEmpty()
					&& fareTypeTO.getOndWiseFareId().get(ondSequence) != null) {
				depFareQuoteTO.setFareId(fareTypeTO.getOndWiseFareId().get(ondSequence));
			}
			depFareQuoteTO.setOndSequence(ondSequence);
			fareCollection.add(depFareQuoteTO);

		}

		BigDecimalWrapper totalPrice = new BigDecimalWrapper();

		// create the total pax fare tos
		if (totalAdultCount > 0) {

			PaxFareTO adultFareTO = createPaxFareTOWhileAddingTotalPrice(PaxTypeTO.PAX_TYPE_ADULT_DISPLAY, totalAdultCount,
					totalPrice, ppAdultFare, ppAdultSurcharge, ppAdultTax, ppOther, selectedCurrency, true, exchangeRateProxy);
			paxFareTOs.add(adultFareTO);

			PaxPriceTO paxPriceTO = getPerPaxPriceInfo(perPriceInfoCollection, PaxTypeTO.ADULT, selectedCurrency,
					ppAdultflexiChargeWrapper.getValue(), exchangeRateProxy);
			paxPriceTOs.add(paxPriceTO);
		}

		if (totalChildCount > 0) {

			PaxFareTO childFareTO = createPaxFareTOWhileAddingTotalPrice(PaxTypeTO.PAX_TYPE_CHILD_DISPLAY, totalChildCount,
					totalPrice, ppChildFare, ppChildSurcharge, ppChildTax, ppOther, selectedCurrency, true, exchangeRateProxy);
			paxFareTOs.add(childFareTO);

			PaxPriceTO paxPriceTO = getPerPaxPriceInfo(perPriceInfoCollection, PaxTypeTO.CHILD, selectedCurrency,
					ppChildflexiChargeWrapper.getValue(), exchangeRateProxy);
			paxPriceTOs.add(paxPriceTO);
		}

		if (totalInfantCount > 0) {
			// add departure flexi charge to departure surcharge if flexi is chargeable
			PaxFareTO intantFareTO = createPaxFareTOWhileAddingTotalPrice(PaxTypeTO.PAX_TYPE_INFANT_DISPLAY, totalInfantCount,
					totalPrice, ppInfantFare, ppInfantSurcharge, ppInfantTax, ppOther, selectedCurrency, true, exchangeRateProxy);
			paxFareTOs.add(intantFareTO);

			PaxPriceTO paxPriceTO = getPerPaxPriceInfo(perPriceInfoCollection, PaxTypeTO.INFANT, selectedCurrency,
					ppInfantflexiChargeWrapper.getValue(), exchangeRateProxy);
			paxPriceTOs.add(paxPriceTO);
		}

	}

	private static PaxPriceTO getPerPaxPriceInfo(Collection<PerPaxPriceInfoTO> perPriceInfoCollection, String paxType,
			String selectedCurrency, BigDecimal flexiCharge, ExchangeRateProxy exchangeRateProxy) throws ModuleException {

		CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selectedCurrency);
		Currency currency = currencyExchangeRate.getCurrency();

		for (PerPaxPriceInfoTO perPaxPriceInfoTO : perPriceInfoCollection) {
			if (perPaxPriceInfoTO.getPassengerType().equals(paxType)) {

				PaxPriceTO paxPriceTO = new PaxPriceTO();
				paxPriceTO.setPaxType(perPaxPriceInfoTO.getPassengerType());

				BigDecimal totalPrice = AccelAeroCalculator.add(perPaxPriceInfoTO.getPassengerPrice().getTotalPrice(),
						flexiCharge);
				BigDecimal totalPriceWithInfant = AccelAeroCalculator
						.add(perPaxPriceInfoTO.getPassengerPrice().getTotalPriceWithInfant(), flexiCharge);

				paxPriceTO.setPaxTotalPrice(AccelAeroCalculator.formatAsDecimal(totalPrice));

				paxPriceTO.setPaxTotalPriceWithInfant(AccelAeroCalculator.formatAsDecimal(totalPriceWithInfant));

				paxPriceTO.setPaxTotalPriceInSelectedCurr(AccelAeroCalculator
						.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(),
								totalPrice, currency.getBoundryValue(), currency.getBreakPoint())));

				return paxPriceTO;
			}
		}

		// This can not happen
		throw new IllegalArgumentException();
	}

	public static void meargeBasedOnCodeAndAddAllSurcharges(Collection<SurchargeTO> surchargeTOs,
			Collection<SurchargeTO> paxSurchargeTOs) {

		HashMap<String, SurchargeTO> meargedMap = new HashMap<String, SurchargeTO>();
		for (SurchargeTO surchargeTO : paxSurchargeTOs) {
			String mergeKey = surchargeTO.getCarrierCode() + surchargeTO.getSurchargeCode();
			SurchargeTO meargedSurchargeTO = meargedMap.get(mergeKey);
			if (meargedSurchargeTO == null) {
				meargedSurchargeTO = surchargeTO.clone();
				meargedMap.put(mergeKey, meargedSurchargeTO);
			} else {
				meargedSurchargeTO.setAmount(AccelAeroCalculator.add(meargedSurchargeTO.getAmount(), surchargeTO.getAmount()));
				if ((meargedSurchargeTO.getLocalCurrencyAmount() != null) && (surchargeTO.getLocalCurrencyAmount() != null)) {
					meargedSurchargeTO.setLocalCurrencyAmount(AccelAeroCalculator.add(meargedSurchargeTO.getLocalCurrencyAmount(),
							surchargeTO.getLocalCurrencyAmount()));
				}
			}
		}
		for (SurchargeTO meargedSurchargeTO : meargedMap.values()) {
			if (meargedSurchargeTO.getAmount() != null && meargedSurchargeTO.getAmount().doubleValue() != 0) {
				surchargeTOs.add(meargedSurchargeTO);
			}
		}
	}

	public static void meargeBasedOnCodeAndAddAllTaxes(Collection<TaxTO> taxTOs, Collection<TaxTO> paxTaxTOs) {

		HashMap<String, TaxTO> meargedMap = new HashMap<String, TaxTO>();
		for (TaxTO taxTO : paxTaxTOs) {
			String mergeKey = taxTO.getCarrierCode() + taxTO.getTaxCode();
			TaxTO meargedTaxTO = meargedMap.get(mergeKey);
			if (meargedTaxTO == null) {
				meargedTaxTO = taxTO.clone();
				meargedMap.put(mergeKey, meargedTaxTO);
			} else {
				meargedTaxTO.setAmount(AccelAeroCalculator.add(meargedTaxTO.getAmount(), taxTO.getAmount()));
				if ((meargedTaxTO.getLocalCurrencyAmount() != null) && (taxTO.getLocalCurrencyAmount() != null)) {
					meargedTaxTO.setLocalCurrencyAmount(
							AccelAeroCalculator.add(meargedTaxTO.getLocalCurrencyAmount(), taxTO.getLocalCurrencyAmount()));
				}
			}
		}
		for (TaxTO meargedTaxTO : meargedMap.values()) {
			if (meargedTaxTO.getAmount() != null && meargedTaxTO.getAmount().doubleValue() != 0) {
				taxTOs.add(meargedTaxTO);
			}
		}
	}

	public static PaxFareTO createPaxFareTOWhileAddingTotalPrice(String paxTypeDisplay, int paxCount,
			BigDecimalWrapper totalPrice, BigDecimalWrapper ppFare, BigDecimalWrapper ppSurcharge, BigDecimalWrapper ppTax,
			BigDecimalWrapper ppOther, String selCurrCode, boolean isGroupPnr, ExchangeRateProxy exchangeRateProxy)
			throws ModuleException {

		BigDecimalWrapper totalPerPaxPrice = new BigDecimalWrapper();

		// Here we can't take the total price since it's the departure total
		// price we are calculating
		// The API(s) are not sending total price per departure / per return
		// There fore we need to sum these and show the price
		totalPerPaxPrice
				.add(AccelAeroCalculator.add(ppFare.getValue(), ppSurcharge.getValue(), ppTax.getValue(), ppOther.getValue()));

		BigDecimalWrapper totalPriceForAllPax = new BigDecimalWrapper();
		if (isGroupPnr) {
			totalPriceForAllPax.add(AccelAeroCalculator.multiply(totalPerPaxPrice.getValue(), new BigDecimal(paxCount)));
		} else {
			totalPriceForAllPax.add(totalPerPaxPrice.getValue());
		}

		PaxFareTO ppFareTO = createPaxFareTO(paxTypeDisplay, paxCount, totalPriceForAllPax, totalPerPaxPrice, ppFare, ppSurcharge,
				ppTax, ppOther, selCurrCode, exchangeRateProxy);
		totalPrice.add(totalPriceForAllPax.getValue());

		return ppFareTO;
	}

	public static boolean calculateBufferTime(FareTypeTO fareTypeTO) {
		long bufferStartTime = fareTypeTO.getOnHoldStartTime() != null ? fareTypeTO.getOnHoldStartTime().getTime() : 0;
		long bufferEndTime = fareTypeTO.getOnHoldStopTime() != null ? fareTypeTO.getOnHoldStopTime().getTime() : 0;
		long currentTime = new Date().getTime();

		if (currentTime > bufferStartTime && currentTime < bufferEndTime) {
			return true;
		}
		return false;

	}

	private static void findPerPaxPrices(PerPaxPriceInfoTO perPaxPriceInfoTO, List<BigDecimalWrapper> ppDepartureFare,
			List<BigDecimalWrapper> ppDepatureTax, List<BigDecimalWrapper> ppDepatureSurcharge,
			List<BigDecimalWrapper> ppDepatureFees, List<BigDecimalWrapper> ppAdultDepatureExternalCharge)
			throws ModuleException {

		findPerPaxFares(perPaxPriceInfoTO.getPassengerPrice().getBaseFares(), ppDepartureFare);
		findPerPaxTaxes(perPaxPriceInfoTO.getPassengerPrice().getTaxes(), ppDepatureTax);
		findPerPaxSurcharges(perPaxPriceInfoTO.getPassengerPrice().getSurcharges(), ppDepatureSurcharge);
		findPerPaxFees(perPaxPriceInfoTO.getPassengerPrice().getFees(), ppDepatureFees);
		findPerPaxExternalCharges(perPaxPriceInfoTO.getPassengerPrice().getOndExternalCharges(), ppAdultDepatureExternalCharge);
		// findPerPaxExternalCharges(perPaxPriceInfoTO.getPassengerPrice().getInBoundExternalCharges(),
		// ppAdultDepatureExternalCharge, selectedONDCodeList);
	}

	private static void findPerPaxFares(Collection<BaseFareTO> baseFareTOList, List<BigDecimalWrapper> depFareRef)
			throws ModuleException {

		for (BaseFareTO baseFareTO : baseFareTOList) {
			depFareRef.get(baseFareTO.getOndSequence()).add(baseFareTO.getAmount());
		}

	}

	private static void findPerPaxTaxes(Collection<TaxTO> taxList, List<BigDecimalWrapper> depTaxRef) throws ModuleException {

		for (TaxTO taxTO : taxList) {
			depTaxRef.get(taxTO.getOndSequence()).add(taxTO.getAmount());
		}

	}

	private static void findPerPaxSurcharges(Collection<SurchargeTO> surList, List<BigDecimalWrapper> depSurRef)
			throws ModuleException {

		for (SurchargeTO surchargeTO : surList) {
			depSurRef.get(surchargeTO.getOndSequence()).add(surchargeTO.getAmount());
		}

	}

	private static void findPerPaxExternalCharges(List<ONDExternalChargeTO> ondExtChgs, List<BigDecimalWrapper> depSurRef)
			throws ModuleException {

		if (ondExtChgs != null) {
			for (ONDExternalChargeTO ondExtChg : ondExtChgs) {
				for (SurchargeTO surchargeTO : ondExtChg.getExternalCharges()) {
					depSurRef.get(surchargeTO.getOndSequence()).add(surchargeTO.getAmount());
				}
			}
		}

	}

	private static void findPerPaxFees(Collection<FeeTO> feeList, List<BigDecimalWrapper> depFeeRef) throws ModuleException {

		for (FeeTO feeTO : feeList) {
			depFeeRef.get(feeTO.getOndSequence()).add(feeTO.getAmount());
		}

	}

	private static boolean findPerSegmentFares(Collection<BaseFareTO> baseFareTOList, BigDecimalWrapper fareRef,
			String segmentCode, int ondSequence, String paxType) throws ModuleException {

		boolean fareExists = false;
		BigDecimal fare = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (BaseFareTO baseFareTO : baseFareTOList) {
			if ((baseFareTO.getSegmentCode().equals(segmentCode) || baseFareTO.getSegmentCode().contains(segmentCode))
					&& baseFareTO.getOndSequence() == ondSequence) {
				fareExists = true;
				if (baseFareTO.getApplicablePassengerTypeCode().contains(paxType)) {
					fare = baseFareTO.getAmount();
					break;
				}
			}
		}

		fareRef.add(fare);

		return fareExists;
	}

	private static void findPerSegmentTotal(FareTypeTO fareTypeTO, BigDecimalWrapper totalRef, String segmentCode,
			int ondSequence, String paxType) throws ModuleException {

		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();

		findPerSegmentFares(fareTypeTO.getBaseFares(), totalRef, segmentCode, ondSequence, paxType);

		for (SurchargeTO surchargeTO : fareTypeTO.getSurcharges()) {
			if (surchargeTO.getSegmentCode().equals(segmentCode)) {
				if (surchargeTO.getApplicablePassengerTypeCode().contains(paxType)) {
					total = AccelAeroCalculator.add(total, surchargeTO.getAmount());
				}
			}
		}
		for (TaxTO taxTO : fareTypeTO.getTaxes()) {
			if (taxTO.getSegmentCode().equals(segmentCode)) {
				if (taxTO.getApplicablePassengerTypeCode().contains(paxType)) {
					total = AccelAeroCalculator.add(total, taxTO.getAmount());
				}
			}
		}
		for (FeeTO feeTO : fareTypeTO.getFees()) {
			if (feeTO.getSegmentCode().equals(segmentCode)) {
				if (feeTO.getApplicablePassengerTypeCode().contains(paxType)) {
					total = AccelAeroCalculator.add(total, feeTO.getAmount());
				}
			}
		}
		totalRef.add(total);
	}

	private static PaxFareTO createPaxFareTO(String paxTypeDescription, int totalPaxCount,
			BigDecimalWrapper totalPriceForPaxCount, BigDecimalWrapper ppTotalPrice, BigDecimalWrapper ppFare,
			BigDecimalWrapper ppSurcharge, BigDecimalWrapper ppTax, BigDecimalWrapper ppOther, String selCurrency,
			ExchangeRateProxy exchangeRateProxy) throws ModuleException {

		// Set PaxFareTO for Child & Children charges
		PaxFareTO adultPaxFareTO = new PaxFareTO();
		adultPaxFareTO.setPaxType(paxTypeDescription);
		adultPaxFareTO.setFare(ppFare.getDisplayValue());
		adultPaxFareTO.setNoPax(totalPaxCount + "");
		adultPaxFareTO.setPerPax(ppTotalPrice.getDisplayValue());
		adultPaxFareTO.setSur(ppSurcharge.getDisplayValue());
		adultPaxFareTO.setTax(ppTax.getDisplayValue());
		adultPaxFareTO.setTotal(totalPriceForPaxCount.getDisplayValue());
		adultPaxFareTO.setOther(ppOther.getDisplayValue());

		CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(selCurrency);
		Currency currency = currencyExchangeRate.getCurrency();

		adultPaxFareTO.setSelCurtotal(AccelAeroCalculator
				.formatAsDecimal(AccelAeroRounderPolicy.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(),
						ppTotalPrice.getValue(), currency.getBoundryValue(), currency.getBreakPoint())));

		return adultPaxFareTO;
	}

	public static Collection<SurchargeFETO> convertToSurchargeFETO(Collection<SurchargeTO> surcharges,
			String selectedCurrencyCode, ExchangeRateProxy exchangeRateProxy) throws ModuleException {
		Collection<SurchargeFETO> returnCollec = new ArrayList<SurchargeFETO>();
		for (SurchargeTO surchargeTO : surcharges) {

			SurchargeFETO surchageFeto = new SurchargeFETO();
			returnCollec.add(surchageFeto);
			surchageFeto.setAmount(AccelAeroCalculator.formatAsDecimal(surchargeTO.getAmount()));
			surchageFeto.setApplicablePassengerTypeCode(surchargeTO.getApplicablePassengerTypeCode());
			surchageFeto.setApplicableToDisplay(surchargeTO.getApplicableToDisplay());
			surchageFeto.setCarrierCode(surchargeTO.getCarrierCode());
			surchageFeto.setSegmentCode(surchargeTO.getSegmentCode());
			surchageFeto.setSurchargeCode(surchargeTO.getSurchargeCode());
			surchageFeto.setSurchargeName(surchargeTO.getSurchargeName());
			surchageFeto.setReportingChargeGroupCode(surchargeTO.getReportingChargeGroupCode());
			if (!StringUtil.isNullOrEmpty(selectedCurrencyCode)) {
				BigDecimal amountInSelectedCurrency = exchangeRateProxy.convert(selectedCurrencyCode, surchargeTO.getAmount());
				surchageFeto.setAmountInSelectedCurrency(AccelAeroCalculator.formatAsDecimal(amountInSelectedCurrency));
			}
			if (surchargeTO.isDefinedInLocal()) {
				surchageFeto.setAmountInLocalCurrency(AccelAeroCalculator.formatAsDecimal(surchargeTO.getLocalCurrencyAmount()));
				surchageFeto.setLocalCurrencyCode(surchargeTO.getLocalCurrencyCode());
			} else {
				surchageFeto.setAmountInLocalCurrency(surchageFeto.getAmount());
				surchageFeto.setLocalCurrencyCode(AppSysParamsUtil.getBaseCurrency());
			}
		}
		return returnCollec;
	}

	public static Collection<SurchargeFETO> convertExternalChargeTOToSurchargeFETO(Collection<ExternalChargeTO> surcharges,
			String selectedCurrencyCode, ExchangeRateProxy exchangeRateProxy) throws ModuleException {
		Collection<SurchargeFETO> returnCollec = new ArrayList<SurchargeFETO>();
		for (SurchargeTO surchargeTO : surcharges) {

			SurchargeFETO surchageFeto = new SurchargeFETO();
			returnCollec.add(surchageFeto);
			surchageFeto.setAmount(AccelAeroCalculator.formatAsDecimal(surchargeTO.getAmount()));
			surchageFeto.setApplicablePassengerTypeCode(surchargeTO.getApplicablePassengerTypeCode());
			surchageFeto.setApplicableToDisplay(surchargeTO.getApplicableToDisplay());
			surchageFeto.setCarrierCode(surchargeTO.getCarrierCode());
			surchageFeto.setSegmentCode(surchargeTO.getSegmentCode());
			surchageFeto.setSurchargeCode(surchargeTO.getSurchargeCode());
			surchageFeto.setSurchargeName(surchargeTO.getSurchargeName());
			if (!StringUtil.isNullOrEmpty(selectedCurrencyCode)) {
				BigDecimal amountInSelectedCurrency = exchangeRateProxy.convert(selectedCurrencyCode, surchargeTO.getAmount());
				surchageFeto.setAmountInSelectedCurrency(AccelAeroCalculator.formatAsDecimal(amountInSelectedCurrency));
			}
		}
		return returnCollec;
	}

	public static Collection<TaxFETO> convertToTaxFETO(Collection<TaxTO> taxes, String selectedCurrencyCode,
			ExchangeRateProxy exchangeRateProxy) throws ModuleException {
		Collection<TaxFETO> returnCollec = new ArrayList<TaxFETO>();
		for (TaxTO taxTO : taxes) {

			TaxFETO taxFeto = new TaxFETO();
			returnCollec.add(taxFeto);
			taxFeto.setAmount(AccelAeroCalculator.formatAsDecimal(taxTO.getAmount()));
			taxFeto.setApplicablePassengerTypeCode(taxTO.getApplicablePassengerTypeCode());
			taxFeto.setApplicableToDisplay(taxTO.getApplicableToDisplay());
			taxFeto.setCarrierCode(taxTO.getCarrierCode());
			taxFeto.setSegmentCode(taxTO.getSegmentCode());
			taxFeto.setTaxCode(taxTO.getTaxCode());
			taxFeto.setTaxName(taxTO.getTaxName());
			taxFeto.setReportingChargeGroupCode(taxTO.getReportingChargeGroupCode());
			if (!StringUtil.isNullOrEmpty(selectedCurrencyCode)) {
				BigDecimal amountInSelectedCurrency = exchangeRateProxy.convert(selectedCurrencyCode, taxTO.getAmount());
				taxFeto.setAmountInSelectedCurrency(AccelAeroCalculator.formatAsDecimal(amountInSelectedCurrency));
			}

			if (taxTO.isDefinedInLocal()) {
				taxFeto.setAmountInLocalCurrency(AccelAeroCalculator.formatAsDecimal(taxTO.getLocalCurrencyAmount()));
				taxFeto.setLocalCurrencyCode(taxTO.getLocalCurrencyCode());
			} else {
				taxFeto.setAmountInLocalCurrency(taxFeto.getAmount());
				taxFeto.setLocalCurrencyCode(AppSysParamsUtil.getBaseCurrency());
			}
		}
		return returnCollec;
	}

	public static Collection<FlightSegmentDTO>
			convertReservationSegmentsToFlightSegmentDTOs(Collection<LCCClientReservationSegment> colSegments) {
		List<FlightSegmentDTO> colFlightSegments = new ArrayList<FlightSegmentDTO>();
		if (colSegments != null) {
			for (LCCClientReservationSegment resSeg : colSegments) {
				FlightSegmentDTO fltSegDTO = new FlightSegmentDTO();
				fltSegDTO.setSegmentCode(resSeg.getSegmentCode());
				// TODO: This is wrong, we are setting pnr segment id in FlightSegmentDTO's segmentId and using it in
				// many places.
				// It is misleading and this should be refactored.
				fltSegDTO.setSegmentId(Integer.valueOf(resSeg.getBookingFlightSegmentRefNumber()));
				fltSegDTO.setFlightNumber(resSeg.getFlightNo());
				fltSegDTO.setDepartureDateTime(resSeg.getDepartureDate());
				fltSegDTO.setArrivalDateTime(resSeg.getArrivalDate());
				fltSegDTO.setDepartureDateTimeZulu(resSeg.getDepartureDateZulu());
				fltSegDTO.setArrivalDateTimeZulu(resSeg.getArrivalDateZulu());
				fltSegDTO.setFromAirport(resSeg.getSegmentCode().substring(0, fltSegDTO.getSegmentCode().indexOf("/")));
				fltSegDTO.setToAirport(getDestinationFromSegmentCode(resSeg.getSegmentCode()));
				fltSegDTO.setOperationTypeID(AirScheduleCustomConstants.OperationTypes.STANDARD);
				fltSegDTO.setPnrSegmentStatus(resSeg.getStatus());
				fltSegDTO.setLastFareQuotedDate(resSeg.getLastFareQuoteDate());
				fltSegDTO.setTicketValidTill(resSeg.getTicketValidTill());
				fltSegDTO.setInterlineReturnGroupKey(resSeg.getInterlineReturnGroupKey());
				fltSegDTO.setOpenReturnSegment(resSeg.isOpenReturnSegment());
				fltSegDTO.setOpenRetConfirmBefore(resSeg.getOpenRetConfirmBefore());
				fltSegDTO.setOperatingCarrier(resSeg.getCarrierCode());
				colFlightSegments.add(fltSegDTO);

			}
		}
		return colFlightSegments;
	}

	public static List<FlightSegmentTO>
			convertReservationSegmentsToFlightSegmentTOs(Collection<LCCClientReservationSegment> colSegments) {
		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();

		if (colSegments != null) {
			for (LCCClientReservationSegment resSeg : colSegments) {
				FlightSegmentTO fltSegTO = new FlightSegmentTO();
				fltSegTO.setSegmentCode(resSeg.getSegmentCode());
				fltSegTO.setFlightNumber(resSeg.getFlightNo());
				fltSegTO.setFlightRefNumber(resSeg.getFlightSegmentRefNumber());
				fltSegTO.setDepartureDateTime(resSeg.getDepartureDate());
				fltSegTO.setArrivalDateTime(resSeg.getArrivalDate());
				fltSegTO.setDepartureDateTimeZulu(resSeg.getDepartureDateZulu());
				fltSegTO.setArrivalDateTimeZulu(resSeg.getArrivalDateZulu());
				fltSegTO.setOperatingAirline(resSeg.getCarrierCode());
				fltSegTO.setReturnFlag((resSeg.getReturnFlag() != null && "Y".equals(resSeg.getReturnFlag())) ? true : false);
				fltSegTO.setSegmentSequence(resSeg.getSegmentSeq());
				fltSegTO.setCsOcCarrierCode(resSeg.getCsOcCarrierCode());
				/*
				 * Integer fltSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(resSeg.getFlightSegmentRefNumber());
				 * fltSegTO.setFlightSegId(fltSegId);
				 */
				flightSegmentTOs.add(fltSegTO);
			}
		}

		return flightSegmentTOs;
	}

	private static String getDestinationFromSegmentCode(String segmentCode) {
		// Capture last 3 Characters ex: JIB/HGA/MGQ
		return segmentCode.substring(segmentCode.length() - 3, segmentCode.length());
	}

	public static List<FlightSegmentTO> getAllCNFSegsForModify(Collection<LCCClientReservationSegment> oldSegs,
			Collection<FlightSegmentTO> newSegs) {
		List<FlightSegmentTO> allCNFSegs = new ArrayList<FlightSegmentTO>();
		allCNFSegs.addAll(newSegs);
		FlightSegmentTO oldFltTo = null;
		for (LCCClientReservationSegment oldSeg : oldSegs) {
			if (oldSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
				oldFltTo = new FlightSegmentTO();
				oldFltTo.setFlightNumber(oldSeg.getFlightNo());
				oldFltTo.setDepartureDateTimeZulu(oldSeg.getDepartureDateZulu());
				oldFltTo.setOperatingAirline(oldSeg.getCarrierCode());
				oldFltTo.setSegmentCode(oldSeg.getSegmentCode());
				allCNFSegs.add(oldFltTo);
			}
		}
		return allCNFSegs;

	}

	public static void setExistingSegInfo(BaseAvailRQ flightAvailRQ, String oldAllSegments, String modifyingSegmentRefNos)
			throws ModuleException, Exception {

		Collection<LCCClientReservationSegment> colsegs = ModifyReservationJSONUtil.transformJsonSegments(oldAllSegments);
		Collections.sort((List) colsegs);
		flightAvailRQ.getAvailPreferences()
				.setExistingSegmentsList(ReservationBeanUtil.convertReservationSegmentsToFlightSegmentDTOs(colsegs));

		Collection<LCCClientReservationSegment> colModifiedSegs = new ArrayList<LCCClientReservationSegment>();
		if (modifyingSegmentRefNos != null) {
			String[] arrPnrSegIdsWithOndGroup = modifyingSegmentRefNos.split(",");

			for (String element : arrPnrSegIdsWithOndGroup) {
				String pnrSegId = element.substring(0, element.indexOf('$'));
				for (LCCClientReservationSegment resSeg : colsegs) {
					if (resSeg.getBookingFlightSegmentRefNumber().equals(pnrSegId)) {
						colModifiedSegs.add(resSeg);
						break;
					}
				}
			}
		}

		flightAvailRQ.getAvailPreferences()
				.setModifiedSegmentsList(ReservationBeanUtil.convertReservationSegmentsToFlightSegmentDTOs(colModifiedSegs));

		if (modifyingSegmentRefNos != null) {
			String modifiedInterlineReturnGroupKey = null;
			String modifiedInterlineGroupKey = null;
			boolean isInboundFareModified = false;
			BigDecimal modifiedOndFareAmount = null;
			Integer modifiedOndFareId = null;
			String oldFareCarrierCode = null;

			for (LCCClientReservationSegment resSeg : colModifiedSegs) {
				modifiedInterlineGroupKey = resSeg.getInterlineGroupKey();
				modifiedInterlineReturnGroupKey = resSeg.getInterlineReturnGroupKey();
				isInboundFareModified = ("Y".equals(resSeg.getReturnFlag())
						|| (flightAvailRQ.getTravelPreferences().isOpenReturnConfirm() && resSeg.isOpenReturnSegment()));

				if (resSeg.getFareTO() != null) {
					modifiedOndFareAmount = resSeg.getFareTO().getAmount();
					modifiedOndFareId = resSeg.getFareTO().getFareId();
					oldFareCarrierCode = resSeg.getFareTO().getCarrierCode();
				}

				break;
			}

			flightAvailRQ.getAvailPreferences().setOldFareAmount(modifiedOndFareAmount);
			flightAvailRQ.getAvailPreferences().setOldFareID(modifiedOndFareId);
			flightAvailRQ.getAvailPreferences().setOldFareCarrierCode(oldFareCarrierCode);
			flightAvailRQ.getAvailPreferences().setInboundFareModified(isInboundFareModified);

			OndConvertAssembler ondAssm = new OndConvertAssembler(flightAvailRQ.getOriginDestinationInformationList());
			if (!ondAssm.isReturnFlight()) {
				// boolean blnAllowHalfReturnFaresForModification =
				// AppSysParamsUtil.isAllowHalfReturnFaresForModification();
				// if (blnAllowHalfReturnFaresForModification) {
				Collection<LCCClientReservationSegment> colInverseSegs = new ArrayList<LCCClientReservationSegment>();
				Integer inverseOndFareId = null;

				if (colModifiedSegs != null) {
					// Get inverse of modified segments
					for (LCCClientReservationSegment resSeg : colsegs) {
						if (!resSeg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
							if (modifiedInterlineReturnGroupKey != null
									&& modifiedInterlineReturnGroupKey.equals(resSeg.getInterlineReturnGroupKey())) {// same
																														// return
																														// grouping
								if (modifiedInterlineGroupKey != null
										&& !modifiedInterlineGroupKey.equals(resSeg.getInterlineGroupKey())) { // different
																												// ond
																												// grouping
									colInverseSegs.add(resSeg);

									if (resSeg.getFareTO() != null) {
										inverseOndFareId = resSeg.getFareTO().getFareId();
									}

								}
							}
						}
					}
				}

				flightAvailRQ.getAvailPreferences().setInverseSegmentsList(
						ReservationBeanUtil.convertReservationSegmentsToFlightSegmentDTOs(colInverseSegs));
				flightAvailRQ.getAvailPreferences().setInverseFareID(inverseOndFareId);
				// }
			}
		}
	}

	public static boolean isRecieptGenerationEnable(String payMethod) throws ModuleException {
		boolean enableResiept = false;
		if (payMethod != null && !payMethod.equals("")) {
			enableResiept = DatabaseUtil.isRecieptGenerationEnable(payMethod);
		}
		return enableResiept;
	}

	public static OnHoldReleaseTimeDTO getOnHoldReleaseTimeDTO(List<FlightSegmentTO> flightSegmentTOs, FareTypeTO fare)
			throws ModuleException {
		OnHoldReleaseTimeDTO onHoldReleaseTimeDTO = new OnHoldReleaseTimeDTO();
		onHoldReleaseTimeDTO.setBookingDate(new Date());
		if (flightSegmentTOs != null && flightSegmentTOs.size() > 0) {
			Collections.sort(flightSegmentTOs);
			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				if (flightSegmentTO != null) {
					String segmentCode = flightSegmentTO.getSegmentCode();
					// Nili You don't need to eliminate the bus segment because it's needs for onhold release time.
					// otherwise there will be inconsistent with the release time calculations
					if (segmentCode != null /* && !WPModuleUtils.getAirportBD().isBusSegment(segmentCode) */) {
						onHoldReleaseTimeDTO.setOndCode(segmentCode);
						onHoldReleaseTimeDTO.setFlightDepartureDate(flightSegmentTO.getDepartureDateTimeZulu());
						onHoldReleaseTimeDTO.setDomestic(flightSegmentTO.isDomesticFlight());
						if (flightSegmentTO.getCabinClassCode() != null) {
							onHoldReleaseTimeDTO.setCabinClass(flightSegmentTO.getCabinClassCode());
						}
						break;
					}
				}
			}

			String bookingClass = OnHoldReleaseTimeDTO.ONHOLD_ANY;
			int fareRuleId = -1;
			if (fare != null) {
				List<FareRuleDTO> fareRuleDTOs = fare.getApplicableFareRules();
				if (fareRuleDTOs != null) {
					for (FareRuleDTO fareRuleDTO : fareRuleDTOs) {
						if (fareRuleDTO != null && fareRuleDTO.getOrignNDest() != null) {
							if (onHoldReleaseTimeDTO.getOndCode().split("/")[0]
									.equalsIgnoreCase(fareRuleDTO.getOrignNDest().split("/")[0])) {
								bookingClass = fareRuleDTO.getBookingClassCode();
								fareRuleId = fareRuleDTO.getFareRuleId();
								break;
							}
						}
					}
				}
			}
			onHoldReleaseTimeDTO.setBookingClass(bookingClass);
			onHoldReleaseTimeDTO.setFareRuleId(fareRuleId);
		}

		return onHoldReleaseTimeDTO;
	}

	public static String getClassOfServiceDescription(Collection<FlightInfoTO> flightInfo) throws ModuleException {
		String cabinDescription = null;
		List<String> ccList = new ArrayList<String>();
		for (FlightInfoTO flightInfoTO : flightInfo) {
			if (flightInfoTO.getBkgClass() != null) {
				if (!ccList.contains(flightInfoTO.getBkgClass())) {
					ccList.add(flightInfoTO.getBkgClass());
				}
			}
		}
		for (String cabinClassCode : ccList) {
			if (cabinDescription != null) {
				cabinDescription = CommonsServices.getGlobalConfig().getActiveCabinClassesMap().get(cabinClassCode);
			}
		}
		return cabinDescription;
	}

	private static String getDuration(Date departureDate, Date arrivalDate) {

		NumberFormat nfr = new DecimalFormat("##00");

		Calendar depCal = Calendar.getInstance();
		depCal.setTime(departureDate);

		Calendar arrCal = Calendar.getInstance();
		arrCal.setTime(arrivalDate);

		long diff = arrCal.getTimeInMillis() - depCal.getTimeInMillis();

		long diffMinutes = diff / (60 * 1000);

		String durationHoursStr = nfr.format((diffMinutes / 60)) + ":" + nfr.format((diffMinutes % 60));

		return durationHoursStr;
	}

	private static String getStopOverDuration(List<FlightSegmentTO> fltSegmentList, int currentSegIndex) {
		int nextSegmentIndex = currentSegIndex + 1;
		if (nextSegmentIndex < fltSegmentList.size()) {
			FlightSegmentTO currentSeg = fltSegmentList.get(currentSegIndex);
			FlightSegmentTO nextSeg = fltSegmentList.get(nextSegmentIndex);

			if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(currentSeg.getFlightSegmentStatus())
					&& !ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(nextSeg.getFlightSegmentStatus())) {
				return getDuration(currentSeg.getArrivalDateTime(), nextSeg.getDepartureDateTime());
			}

		}
		return "-";
	}

	private static Date getTicketValidtyVariance(boolean ticketValidityExist, boolean isStartDateTime, String ticketValidityMax,
			String ticketValidityMin, Date depatureDateTime) throws ParseException {

		Date returnDate = CalendarUtil.getCopyofDate(depatureDateTime);
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm");

		if (ticketValidityExist) {
			if (!isStartDateTime) {
				Date ticketValidMaxDate = null;

				if (!StringUtil.isNullOrEmpty(ticketValidityMax)) {
					ticketValidMaxDate = format.parse(ticketValidityMax);

					if ((!CalendarUtil.isSameDay(depatureDateTime, ticketValidMaxDate))
							&& (depatureDateTime.getTime() > ticketValidMaxDate.getTime())) {
						returnDate = CalendarUtil.getEndTimeOfDate(ticketValidMaxDate);
					}
				}

			} else {
				Date ticketValidMinDate = null;
				if (!StringUtil.isNullOrEmpty(ticketValidityMin)) {
					ticketValidMinDate = format.parse(ticketValidityMin);

					if ((!CalendarUtil.isSameDay(depatureDateTime, ticketValidMinDate))
							&& (depatureDateTime.getTime() < ticketValidMinDate.getTime())) {
						returnDate = CalendarUtil.getStartTimeOfDate(ticketValidMinDate);
					}
				}

			}

		}

		return returnDate;
	}

	public static void setClassOfServiceInfo(FlightSegmentTO flightSegTO, FlightSearchDTO searchParams)
			throws org.json.simple.parser.ParseException {

		String logicalCabinClass = searchParams.getLogicalCabinClass();
		String fareQuoteOndSegLogicalCCSelection = searchParams.getFareQuoteOndSegLogicalCCSelection();

		if (fareQuoteOndSegLogicalCCSelection != null && !fareQuoteOndSegLogicalCCSelection.isEmpty()) {
			String segmentCode = flightSegTO.getSegmentCode();
			String ondSequnce = flightSegTO.getOndSequence() + "";
			String extractedLogicalCC = getLogicalCabinClass(fareQuoteOndSegLogicalCCSelection, ondSequnce, segmentCode);
			if (extractedLogicalCC != null && !extractedLogicalCC.isEmpty()) {
				logicalCabinClass = extractedLogicalCC;
			}

		} else {
			String logicalCCSelection = searchParams.getFareQuoteLogicalCCSelection();
			logicalCabinClass = searchParams.getLogicalCabinClass();
			Map<Integer, String> ondWiseLogicalCC = getOndWiseLogicalCC(logicalCCSelection);

			if (ondWiseLogicalCC.containsKey(flightSegTO.getOndSequence())) {
				logicalCabinClass = ondWiseLogicalCC.get(flightSegTO.getOndSequence());
			}
		}
		logicalCabinClass = logicalCabinClass == null ? searchParams.getClassOfService() : logicalCabinClass;

		if (logicalCabinClass != null) {
			LogicalCabinClassDTO logicalCC = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap().get(logicalCabinClass);
			flightSegTO.setLogicalCabinClassCode(logicalCabinClass);
			flightSegTO.setCabinClassCode(logicalCC.getCabinClassCode());
		}
	}

	public static void setBookingClassInfo(FlightSegmentTO flightSegTO, FlightSearchDTO searchParams)
			throws org.json.simple.parser.ParseException {

		String fareQuoteOndSegBookingClassSelection = searchParams.getFareQuoteOndSegBookingClassSelection();

		if (fareQuoteOndSegBookingClassSelection != null && !fareQuoteOndSegBookingClassSelection.isEmpty()) {
			String segmentCode = flightSegTO.getSegmentCode();
			String ondSequnce = flightSegTO.getOndSequence() + "";
			String extractedBookingClass = getBookingClass(fareQuoteOndSegBookingClassSelection, ondSequnce, segmentCode);
			if (extractedBookingClass != null) {
				flightSegTO.setBookingClass(extractedBookingClass);
			}
		}

	}

	private static String getLogicalCabinClass(String jsonString, String ondSeq, String segCode)
			throws org.json.simple.parser.ParseException {
		String logicalCC = null;

		JSONParser parser = new JSONParser();
		JSONObject jsonObject = (JSONObject) parser.parse(jsonString);
		Set<String> ondSequences = jsonObject.keySet();
		for (String ondSequence : ondSequences) {
			if (ondSeq.equals(ondSequence)) {
				JSONObject segLcc = (JSONObject) jsonObject.get(ondSequence);
				Set<String> segCodes = segLcc.keySet();
				boolean hasMultipleSegments = segCodes.size() > 1;
				for (String seg : segCodes) {
					if (hasMultipleSegments) {
						if (seg.equals(segCode)) {
							logicalCC = (String) segLcc.get(segCode);
							break;
						}
					} else {
						logicalCC = (String) segLcc.get(seg);
						// connection with same cabin class
					}
				}
				break;
			}
		}
		return logicalCC;
	}

	private static String getBookingClass(String jsonString, String ondSeq, String segCode)
			throws org.json.simple.parser.ParseException {
		String bookingClassCode = null;

		JSONParser parser = new JSONParser();
		JSONObject jsonObject = (JSONObject) parser.parse(jsonString);
		Set<String> ondSequences = jsonObject.keySet();
		for (String ondSequence : ondSequences) {
			if (ondSeq.equals(ondSequence)) {
				JSONObject segBcc = (JSONObject) jsonObject.get(ondSequence);
				if (segBcc != null) {
					Set<String> segCodes = segBcc.keySet();
					boolean hasMultipleSegments = segCodes.size() > 1;
					for (String seg : segCodes) {
						if (hasMultipleSegments) {
							if (seg.equals(segCode)) {
								bookingClassCode = (String) segBcc.get(segCode);
								break;
							}
						} else {
							bookingClassCode = (String) segBcc.get(seg);
							// connection with same booking class
						}
					}
				}
				break;
			}
		}
		return bookingClassCode;
	}

	/**
	 * This is erreoneous [with change fare it may have different cabin classes for single ond]
	 */
	@Deprecated
	@SuppressWarnings("unchecked")
	public static Map<Integer, String> getOndWiseLogicalCC(String logicalCCSelection) {
		Map<Integer, String> ondWiseMap = new HashMap<Integer, String>();

		if (logicalCCSelection != null && !"".equals(logicalCCSelection) && !"null".equals(logicalCCSelection)) {
			Map<String, String> feSelectionMap = JSONFETOParser.parseMap(HashMap.class, String.class, logicalCCSelection);

			if (feSelectionMap != null) {
				for (Entry<String, String> feEntry : feSelectionMap.entrySet()) {
					if (feEntry.getKey() != null && !"".equals(feEntry.getKey())) {
						ondWiseMap.put(Integer.parseInt(feEntry.getKey()), feEntry.getValue());
					}
				}
			}
		}

		return ondWiseMap;
	}

	@SuppressWarnings("unchecked")
	public static Map<String, String> getSegmentWiseLogicalCC(String logicalCCSelection) {
		Map<String, String> segWiseLogicalCCMap = new HashMap<String, String>();
		if (logicalCCSelection != null && !"".equals(logicalCCSelection) && !"null".equals(logicalCCSelection)) {
			segWiseLogicalCCMap = JSONFETOParser.parseMap(HashMap.class, String.class, logicalCCSelection);
		}
		return segWiseLogicalCCMap;
	}

	public static void populateOndLogicalCCAvailability(List<OndClassOfServiceSummeryTO> allLogicalCCList,
			List<List<OndClassOfServiceSummeryTO>> ondLogicalCCList) {
		if (allLogicalCCList != null) {
			Collections.sort(allLogicalCCList);
			for (OndClassOfServiceSummeryTO ondLogicalCCDto : allLogicalCCList) {
				if (ondLogicalCCList.size() == ondLogicalCCDto.getSequence()) {
					ondLogicalCCList.add(ondLogicalCCDto.getSequence(), new ArrayList<OndClassOfServiceSummeryTO>());
				} else if (ondLogicalCCList.size() < ondLogicalCCDto.getSequence()) {
					int ondSeq = ondLogicalCCList.size();
					while (ondLogicalCCList.size() <= ondLogicalCCDto.getSequence()) {
						if (!ondLogicalCCList.contains(ondSeq)) {
							ondLogicalCCList.add(ondSeq++, new ArrayList<OndClassOfServiceSummeryTO>());
						}
					}
				}
				ondLogicalCCList.get(ondLogicalCCDto.getSequence()).add(ondLogicalCCDto);
			}
		}
	}

	public static void populateOBIBLogicalCCAvailability(List<OndClassOfServiceSummeryTO> allLogicalCCList,
			List<OndClassOfServiceSummeryTO> outboundLogicalCCList, List<OndClassOfServiceSummeryTO> inboundLogicalCCList) {
		if (allLogicalCCList != null) {
			for (OndClassOfServiceSummeryTO ondLogicalCCDto : allLogicalCCList) {
				if (!ondLogicalCCDto.isInbound()) {
					if (outboundLogicalCCList == null) {
						outboundLogicalCCList = new ArrayList<OndClassOfServiceSummeryTO>();
					}
					outboundLogicalCCList.add(ondLogicalCCDto);
				} else {
					if (inboundLogicalCCList == null) {
						inboundLogicalCCList = new ArrayList<OndClassOfServiceSummeryTO>();
					}
					inboundLogicalCCList.add(ondLogicalCCDto);
				}
			}
			if (outboundLogicalCCList != null) {
				Collections.sort(outboundLogicalCCList);
			}
			if (inboundLogicalCCList != null) {
				Collections.sort(inboundLogicalCCList);
			}
		}
	}

	public static FlightAvailRQ createMultiCityAvailSearchRQ(FlightSearchDTO flightSearch, AppIndicatorEnum appIndicator)
			throws ParseException, ModuleException {

		FlightAvailRQ flightAvailRQ = new FlightAvailRQ();

		int ondSequence = 0;
		for (ONDSearchDTO ond : flightSearch.getOndList()) {
			OriginDestinationInformationTO ondInfoTO = flightAvailRQ.addNewOriginDestinationInformation();
			ondInfoTO.setOrigin(ond.getFromAirport());
			ondInfoTO.setDestination(ond.getToAirport());

			Date depatureDate = ond.getDepartureDate();

			validateDateVarience(depatureDate, ond.getDepartureVariance(), appIndicator);

			// Set flight availability for multicity calendar
			if (SalesChannelsUtil.isAppIndicatorWebOrMobile(appIndicator)) {
				ond.setDepartureVariance(AppSysParamsUtil.getMaxDepartureReturnVarience(appIndicator));
			}

			Date depatureDateTimeStart = CalendarUtil.getStartTimeOfDate(depatureDate);
			depatureDateTimeStart = CalendarUtil.getOfssetAddedTime(depatureDateTimeStart, ond.getDepartureVariance() * -1);

			Date depatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(depatureDate);
			depatureDateTimeEnd = CalendarUtil.getOfssetAddedTime(depatureDateTimeEnd, ond.getDepartureVariance());

			ondInfoTO.setPreferredDate(depatureDate);
			ondInfoTO.setDepartureDateTimeStart(depatureDateTimeStart);
			ondInfoTO.setDepartureDateTimeEnd(depatureDateTimeEnd);
			ondInfoTO.setPreferredLogicalCabin(flightSearch.getPreferredLogicalCC(ondSequence));
			ondInfoTO.setPreferredClassOfService(flightSearch.getPreferredClassOfService(ondSequence));
			ondInfoTO.setPreferredBookingClass(flightSearch.getPreferredBookingClass(ondSequence));
			ondInfoTO.setPreferredBookingType(flightSearch.getPreferredBookingType(ondSequence));
			ondInfoTO.setPreferredBundleFarePeriodId(flightSearch.getPreferredBundledFarePeriodId(ondSequence));
			ondInfoTO.setDepartureCitySearch(ond.isFromCitySearch());
			ondInfoTO.setArrivalCitySearch(ond.isToCitySearch());

			OriginDestinationOptionTO depOndOptionTO = new OriginDestinationOptionTO();
			if (ond.getFlightRPHList() != null) {
				for (String flightRefNumber : ond.getFlightRPHList()) {
					FlightSegmentTO flightSegementTO = new FlightSegmentTO();
					if (flightRefNumber.indexOf("#") != -1) {
						String arr[] = flightRefNumber.split("#");
						flightSegementTO.setFlightRefNumber(arr[0]);
						flightSegementTO.setRouteRefNumber(arr[1]);
						flightSegementTO.setSegmentCode(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(arr[0]));
						flightSegementTO.setFlightNumber(FlightRefNumberUtil.getOperatingAirline(arr[0]));
					} else {
						flightSegementTO.setFlightRefNumber(flightRefNumber);
						flightSegementTO.setSegmentCode(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(flightRefNumber));
						flightSegementTO.setFlightNumber(FlightRefNumberUtil.getOperatingAirline(flightRefNumber));
					}
					depOndOptionTO.getFlightSegmentList().add(flightSegementTO);
					// selectedFlightSegMap.put(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(fltRefNo),
					// FlightRefNumberUtil.getSegmentIdFromFlightRPH(fltRefNo));
				}
			}

			if (ond.getExistingFlightRPHList() != null) {
				List<Integer> flightSegmentIds = new ArrayList<Integer>();
				for (String flightRefNumber : ond.getExistingFlightRPHList()) {
					if (flightRefNumber.indexOf("#") != -1) {
						String arr[] = flightRefNumber.split("#");
						flightSegmentIds.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(arr[0]));
					} else {
						flightSegmentIds.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightRefNumber));
					}
				}
				ondInfoTO.getExistingFlightSegIds().addAll(flightSegmentIds);
			}

			if (ond.getExistingResSegRPHList() != null) {
				ondInfoTO.getExistingPnrSegRPHs().addAll(ond.getExistingResSegRPHList());
			}

			ondInfoTO.setFlownOnd(ond.isFlownOnd());

			ondInfoTO.getOrignDestinationOptions().add(depOndOptionTO);
			ondSequence++;

		}

		Collections.sort(flightAvailRQ.getOriginDestinationInformationList());

		// setting traveler information
		TravelerInfoSummaryTO traverlerInfo = flightAvailRQ.getTravelerInfoSummary();

		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);

		if (flightSearch.getAdultCount() == null) {
			adultsQuantity.setQuantity(0);
		} else {
			adultsQuantity.setQuantity(flightSearch.getAdultCount());
		}

		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);

		if (flightSearch.getChildCount() == null) {
			childQuantity.setQuantity(0);
		} else {
			childQuantity.setQuantity(flightSearch.getChildCount());
		}

		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);

		if (flightSearch.getInfantCount() == null) {
			infantQuantity.setQuantity(0);
		} else {
			infantQuantity.setQuantity(flightSearch.getInfantCount());
		}

		// setting traveler preferences
		TravelPreferencesTO travelerPref = flightAvailRQ.getTravelPreferences();
		if (!StringUtil.isNullOrEmpty(flightSearch.getBookingType())) {
			travelerPref.setBookingType(flightSearch.getBookingType());
		}
		travelerPref.setOpenReturn(flightSearch.getOpenReturn());
		travelerPref.setValidityId(flightSearch.getValidity());
		travelerPref.setOpenReturnConfirm(flightSearch.isOpenReturnConfirm());
		if (AppSysParamsUtil.isBCSelectionAtAvailabilitySearchEnabled()) {
			if (StringUtils.isNotEmpty(flightSearch.getBookingClassCode())) {
				travelerPref.setBookingClassCode(flightSearch.getBookingClassCode());
			}
		}

		AvailPreferencesTO availPref = flightAvailRQ.getAvailPreferences();
		if (!StringUtil.isNullOrEmpty(flightSearch.getPaxType())) {
			availPref.setBookingPaxType(flightSearch.getPaxType());
		}
		if (!StringUtil.isNullOrEmpty(flightSearch.getFareType())) {
			availPref.setFareCategoryType(flightSearch.getFareType());
		}
		if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
			availPref.setSearchSystem(SYSTEM.getEnum(flightSearch.getSearchSystem(), SYSTEM.INT));
		} else {
			availPref.setSearchSystem(SYSTEM.getEnum(flightSearch.getSearchSystem(), SYSTEM.AA));
		}
		availPref.setAppIndicator(appIndicator.equals(AppIndicatorEnum.APP_XBE) ? ApplicationEngine.XBE : ApplicationEngine.IBE);

		if (!StringUtil.isNullOrEmpty(flightSearch.getFirstDeparture())) {
			availPref.setFirstDepatureDate(CalendarUtil.getParsedTime(flightSearch.getFirstDeparture(), DATE_FORMAT_PATTERN));

		}

		if (!StringUtil.isNullOrEmpty(flightSearch.getLastArrival())) {
			availPref.setLastArrivalDate(CalendarUtil.getParsedTime(flightSearch.getLastArrival(), DATE_FORMAT_PATTERN));
		}

		availPref.setStayOverTimeInMillis(flightSearch.getStayOverTimeInMillis());
		// availPref.setQuoteOutboundFlexi(flightSearch.isQuoteOutboundFlexi());
		// availPref.setQuoteInboundFlexi(flightSearch.isQuoteInboundFlexi());
		// FIXME ond sequence hard coded
		availPref.setQuoteOndFlexi(OndSequence.OUT_BOUND, flightSearch.isQuoteOutboundFlexi());
		if (flightAvailRQ.getOriginDestinationInformationList().size() > 1) {
			availPref.setQuoteOndFlexi(OndSequence.IN_BOUND, flightSearch.isQuoteInboundFlexi());
		}
		availPref.setTravelAgentCode(flightSearch.getTravelAgentCode());
		availPref.setHalfReturnFareQuote(flightSearch.isHalfReturnFareQuote());

		return flightAvailRQ;
	}

	public static List<Collection<AvailableFlightInfo>> createAvailFlightInfoListMultiCity(BaseAvailRS baseAvailRS,
			FlightSearchDTO searchDTO, boolean viewAvalableSeats, boolean viewFlightsWithLesserSeats,
			boolean setSeatAvailFrmCount, Set<String> busCarrierCodes, List<String> availableLogicalCabClassList,
			List<List<String>> ondWiseAvailableCOSList) throws ModuleException {

		List<Collection<AvailableFlightInfo>> ondSeqFlightsList = new ArrayList<Collection<AvailableFlightInfo>>();

		int adultCount = searchDTO.getAdultCount() + searchDTO.getChildCount();
		int infantCount = searchDTO.getInfantCount();

		int ondSequence = 0;
		String bookingType = null;
		for (OriginDestinationInformationTO ondInfoTO : baseAvailRS.getOrderedOriginDestinationInformationList()) {
			int intCount = 1;
			if (searchDTO.getOndList() != null && !searchDTO.getOndList().isEmpty()) {
				bookingType = searchDTO.getOndList().get(ondSequence).getBookingType();
			} else {
				bookingType = searchDTO.getBookingType();
			}

			List<String> availableCOSList = new ArrayList<String>();
			List<AvailableFlightInfo> flightInfoList = new ArrayList<AvailableFlightInfo>();
			for (OriginDestinationOptionTO ondOpTO : ondInfoTO.getOrignDestinationOptions()) {
				boolean seatAvailFromCount = true;
				boolean isReturnSegment = false;
				AvailableFlightInfo availableFlightInfo = new AvailableFlightInfo(viewAvalableSeats);
				int i = 0;
				Collections.sort(ondOpTO.getFlightSegmentList());
				for (FlightSegmentTO flightSegmentTO : ondOpTO.getFlightSegmentList()) {
					if (!viewFlightsWithLesserSeats && !flightSegmentTO.hasRequestingNumOfSeats(adultCount, infantCount)
							&& !ondOpTO.isSeatAvailable()) {
						break;
					}
					availableFlightInfo.addFlightSegment(flightSegmentTO);

					if (busCarrierCodes != null
							&& AirScheduleCustomConstants.OperationTypes.BUS_SERVICE == flightSegmentTO.getOperationType()) {
						busCarrierCodes.add(flightSegmentTO.getOperatingAirline());
					}

					availableFlightInfo.setLogicalCabinClassAvailabilityInfo(flightSegmentTO, availableLogicalCabClassList,
							adultCount, infantCount, bookingType);

					if (!flightSegmentTO.hasRequestingNumOfSeats(adultCount, infantCount)) {
						seatAvailFromCount = false;
					}

					// Set via points
					if (ondOpTO.getFlightSegmentList().size() > 1 && i > 0) {
						availableFlightInfo.setViaPointList((flightSegmentTO.getSegmentCode().split("/")[0]));
					}

					isReturnSegment = flightSegmentTO.isReturnFlag();

					availableFlightInfo.addFlightStopOverDurationList(getStopOverDuration(ondOpTO.getFlightSegmentList(), i));
					i++;
				}
				if (availableFlightInfo.getFlightSegIdList() != null && availableFlightInfo.getFlightSegIdList().size() > 0) {
					availableFlightInfo.setSystem(ondOpTO.getSystem().toString());

					if (setSeatAvailFrmCount) {
						availableFlightInfo.setSeatAvailable(seatAvailFromCount);
					} else {
						availableFlightInfo.setSeatAvailable(ondOpTO.isSeatAvailable());
					}

					// sets seat availability with available flight segments by logical cabin class wise, useful when
					// there
					// is connection flights
					availableFlightInfo.setSeatAvaiStatusByCOSList(availableCOSList);

					availableFlightInfo.setReturnFlag(isReturnSegment);
					availableFlightInfo.setIndex(intCount);
					flightInfoList.add(availableFlightInfo);
					intCount++;
				}
			}

			ondWiseAvailableCOSList.add(availableCOSList);
			ondSeqFlightsList.add(flightInfoList);
			ondSequence++;
		}
		return ondSeqFlightsList;
	}

	public static String getONDDetails(String strOND) {
		Map<String, String> mapAirports = MapGenerator.getAirportMap();
		String strSegements = "";
		String[] strArrSegment = strOND.split("/");
		strSegements = "";
		for (int i = 0; i < strArrSegment.length; i++) {
			if (AppSysParamsUtil.isHideStopOverEnabled() && strArrSegment.length > 2 && i > 0 && i < (strArrSegment.length - 1))
				continue;
			if (strSegements != "") {
				strSegements += " / ";
			}
			if (mapAirports.get(strArrSegment[i]) != null) {
				strSegements += mapAirports.get(strArrSegment[i]);
			} else {
				strSegements += strArrSegment[i];
			}
		}
		return strSegements;
	}

	public static boolean hasIntAndDomFlights(ArrayList<String> domesticSegmentCodeList,
			ArrayList<String> internationalSegmentCodeList) {

		if (domesticSegmentCodeList != null && domesticSegmentCodeList.size() > 0 && internationalSegmentCodeList != null
				&& internationalSegmentCodeList.size() > 0) {
			return true;
		}

		return false;
	}

	public static Map<String, String> getReQuoteResSegmentMap(List<ONDSearchDTO> ondSearchList) {

		Map<String, String> requoteSegmentMap = new HashMap<String, String>();

		if (ondSearchList != null && ondSearchList.size() > 0) {
			Iterator<ONDSearchDTO> ondSearchItr = ondSearchList.iterator();
			while (ondSearchItr.hasNext()) {
				ONDSearchDTO ondSearchDTO = ondSearchItr.next();

				List<String> flightRPHList = ondSearchDTO.getFlightRPHList();
				List<String> existResSegIdList = ondSearchDTO.getExistingResSegRPHList();
				List<String> modResSegIdList = ondSearchDTO.getModifiedResSegList();

				if (flightRPHList != null && flightRPHList.size() > 0) {
					int index = 0;
					for (String flightRPH : flightRPHList) {
						String[] flightInfoArr = flightRPH.split("\\$");
						String existSegId = null;
						if (existResSegIdList != null && existResSegIdList.size() > 0) {
							existSegId = existResSegIdList.get(index);
						}

						if (existSegId == null && modResSegIdList != null && modResSegIdList.size() > 0) {
							// To handle, modifying one sector booking to a connection booking.
							if (modResSegIdList.size() >= index + 1) {
								existSegId = modResSegIdList.get(index);
							} else {
								existSegId = modResSegIdList.get(modResSegIdList.size() - 1);
							}
						}
						requoteSegmentMap.put(flightInfoArr[2], existSegId);

						index++;
					}
				}

			}

		}

		return requoteSegmentMap;
	}

	public static Map<Integer, String> getONDFareTypeByFareIdMap(List<ONDSearchDTO> ondSearchList,
			Map<Integer, Integer> oldFareIdByFltSegIdMap) {

		Map<Integer, String> fareTypeByFareIdMap = new HashMap<Integer, String>();

		if (ondSearchList != null && ondSearchList.size() > 0) {
			Iterator<ONDSearchDTO> ondSearchItr = ondSearchList.iterator();
			while (ondSearchItr.hasNext()) {
				ONDSearchDTO ondSearchDTO = ondSearchItr.next();
				Integer oldFareId = null;
				if (getOwnOldPerPaxFareTO(ondSearchDTO) != null && getOwnOldPerPaxFareTO(ondSearchDTO).getFareType() != null) {
					fareTypeByFareIdMap.put(getOwnOldPerPaxFareTO(ondSearchDTO).getFareId(),
							getOwnOldPerPaxFareTO(ondSearchDTO).getFareType());

					oldFareId = getOwnOldPerPaxFareTO(ondSearchDTO).getFareId();
				}

				List<String> flightRPHList = ondSearchDTO.getFlightRPHList();

				if (flightRPHList != null && flightRPHList.size() > 0) {

					for (String flightRPH : flightRPHList) {
						String[] flightInfoArr = flightRPH.split("\\$");

						if (!StringUtil.isNullOrEmpty(flightInfoArr[2])) {
							oldFareIdByFltSegIdMap.put(new Integer(flightInfoArr[2]), oldFareId);
						}
					}
				}

			}

		}

		return fareTypeByFareIdMap;
	}

	/**
	 * List of untouched ONDs with Fare Type
	 * 
	 */
	public static List<Integer> getSkipFareQuoteOnD(List<ONDSearchDTO> ondList, List<Integer> unTouchedOndList,
			List<String> unTouchedFltRPHList) {

		List<Integer> skipOndList = new ArrayList<Integer>();

		if (ondList != null && ondList.size() > 0) {
			int ondSequence = 0;

			for (ONDSearchDTO ondInfoTO : ondList) {
				boolean isUnTouched = false;

				// To identify the untouched ONDs
				if (ondInfoTO.getOldPerPaxFareTOList() != null
						&& ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(ondInfoTO.getStatus())) {

					if (ondInfoTO.isFlownOnd() || (!ondInfoTO.isFlownOnd()
							&& (ondInfoTO.getModifiedResSegList() == null || ondInfoTO.getModifiedResSegList().isEmpty()))) {

						if (ondInfoTO.getExistingResSegRPHList() != null && ondInfoTO.getExistingResSegRPHList().size() > 0) {
							isUnTouched = true;
							unTouchedOndList.add(ondSequence);
							unTouchedFltRPHList.addAll(ondInfoTO.getFlightRPHList());
						}
					}
				}

				if (isUnTouched) {
					skipOndList.add(ondSequence);
				}

				ondSequence++;
			}

		}
		return skipOndList;
	}

	public static void setHubTimeDetails(OriginDestinationInformationTO ondInfo, String hubTimeDetailsJSON, boolean isOutbound) {
		try {
			JSONParser parser = new JSONParser();
			JSONArray jsonHubDetailArray = (JSONArray) parser.parse(hubTimeDetailsJSON);

			if (jsonHubDetailArray.size() > 0) {
				Map<String, Integer> hubDetailMap = new HashMap<String, Integer>();
				for (Object hubDetailObj : jsonHubDetailArray) {
					JSONObject hubDetail = (JSONObject) hubDetailObj;
					if (hubDetail.get("hubCode") != null && !hubDetail.get("hubCode").equals("")) {
						if (isOutbound && hubDetail.get("outboundStopOverTime") != null
								&& !hubDetail.get("outboundStopOverTime").equals("")) {
							hubDetailMap.put(hubDetail.get("hubCode").toString(),
									Integer.valueOf(hubDetail.get("outboundStopOverTime").toString()));
						} else if (hubDetail.get("inboundStopOverTime") != null
								&& !hubDetail.get("inboundStopOverTime").equals("")) {
							hubDetailMap.put(hubDetail.get("hubCode").toString(),
									Integer.valueOf(hubDetail.get("inboundStopOverTime").toString()));
						}

					}

				}

				ondInfo.setHubTimeDetailMap(hubDetailMap);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static List<String> getONDWiseFlexiCharges(FareTypeTO fareTypeTo) {
		Map<Integer, String> ondWiseTotalFlexiMap = new TreeMap<Integer, String>();

		for (ONDExternalChargeTO ondExternalChargeTO : fareTypeTo.getOndExternalCharges()) {
			BigDecimal totalFlexiAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			for (ExternalChargeTO externalChargeTO : ondExternalChargeTO.getExternalCharges()) {
				if (externalChargeTO.getType() == EXTERNAL_CHARGES.FLEXI_CHARGES) {
					totalFlexiAmount = AccelAeroCalculator.add(totalFlexiAmount, externalChargeTO.getAmount());
				}
			}

			ondWiseTotalFlexiMap.put(ondExternalChargeTO.getOndSequence(), totalFlexiAmount.toString());
		}

		List<String> ondwiseTotalFlexiCharge = new ArrayList<String>(ondWiseTotalFlexiMap.values());

		return ondwiseTotalFlexiCharge;
	}

	public static List<Boolean> getONDWiseFlexiAvailable(List<OndClassOfServiceSummeryTO> availableLogicalCCList) {
		Map<Integer, Boolean> ondWiseFlexiAvailabilityMap = new TreeMap<Integer, Boolean>();

		for (OndClassOfServiceSummeryTO ondClassOfServiceSummeryTO : availableLogicalCCList) {
			boolean isFlexiAvailable = false;
			for (LogicalCabinClassInfoTO logicalCabinClassInfoTO : ondClassOfServiceSummeryTO.getAvailableLogicalCCList()) {
				if (isFlexiBundleAvailable(logicalCabinClassInfoTO) || isFreeFlexiWithBundleAvailable(logicalCabinClassInfoTO)) {
					isFlexiAvailable = true;
					break;
				}
			}

			ondWiseFlexiAvailabilityMap.put(ondClassOfServiceSummeryTO.getSequence(), isFlexiAvailable);
		}

		List<Boolean> ondwiseFlexiAvailable = new ArrayList<Boolean>(ondWiseFlexiAvailabilityMap.values());

		return ondwiseFlexiAvailable;
	}

	private static boolean isFreeFlexiWithBundleAvailable(LogicalCabinClassInfoTO logicalCabinClassInfoTO) {
		return logicalCabinClassInfoTO.isSelected() && logicalCabinClassInfoTO.isWithFlexi()
				&& logicalCabinClassInfoTO.getBundledFarePeriodId() != null;
	}

	private static boolean isFlexiBundleAvailable(LogicalCabinClassInfoTO logicalCabinClassInfoTO) {
		return logicalCabinClassInfoTO.isFlexiAvailable() && logicalCabinClassInfoTO.getBundledFarePeriodId() == null;
	}

	public static FareTO getOwnOldPerPaxFareTO(ONDSearchDTO ondInfoTO) {
		if (ondInfoTO.getOldPerPaxFareTOList() != null) {
			String defaultCarrier = AppSysParamsUtil.getDefaultCarrierCode();
			String busCarrier = null;

			try {
				busCarrier = SelectListGenerator.getDefaultBusCarrierCode(defaultCarrier);
			} catch (Exception e) {
				// log error
			}

			for (FareTO fareTO : ondInfoTO.getOldPerPaxFareTOList()) {
				if (fareTO != null && (defaultCarrier.equals(fareTO.getCarrierCode())
						|| (busCarrier != null && busCarrier.equals(fareTO.getCarrierCode())))) {
					return fareTO;
				}
			}
		}
		return null;
	}

	/**
	 * when list of available logical-CC provided following function will generate a list of string with relevant
	 * information of each OND wise.
	 * 
	 * Ex: 2#C$Business Class^Y$Economy Class
	 */
	public static List<String> generateCOSAvailabilityStringList(List<List<String>> ondWiseAvailableCOSList)
			throws ModuleException {
		List<String> ondWiseAvailableCOSInfoList = new ArrayList<String>();
		if (ondWiseAvailableCOSList != null && ondWiseAvailableCOSList.size() > 0) {
			List<String[]> activeCos = SelectListGenerator.getCOSActiveCodeDescList();
			Map<String, String> cosDescriptionMap = new HashMap<String, String>();
			for (String[] cosArray : activeCos) {
				if (!StringUtil.isNullOrEmpty(cosArray[0]) && !StringUtil.isNullOrEmpty(cosArray[1])) {
					cosDescriptionMap.put(cosArray[0], cosArray[1]);
				}
			}

			for (List<String> cosList : ondWiseAvailableCOSList) {
				String buildStr = cosList.size() + "#";
				boolean isFirst = true;
				for (String cos : cosList) {
					if (!isFirst) {
						buildStr += "^";
					}

					buildStr += cos + "$" + cosDescriptionMap.get(cos);
					isFirst = false;
				}
				ondWiseAvailableCOSInfoList.add(buildStr);

			}

		}
		return ondWiseAvailableCOSInfoList;
	}

	@Deprecated
	public static List<List<String>> buildIBECalendarDaysOld(List<Collection<AvailableFlightInfo>> ondSeqFlightsList) {
		List<List<String>> calendarDayList = new ArrayList<List<String>>();
		if (ondSeqFlightsList != null && ondSeqFlightsList.size() > 0) {
			for (Collection<AvailableFlightInfo> flightInfoList : ondSeqFlightsList) {
				if (flightInfoList != null && flightInfoList.size() > 0) {
					ONDFLIT: for (AvailableFlightInfo flightInfo : flightInfoList) {
						if (flightInfo != null && flightInfo.getFirstDepartureDate() != null) {
							List<String> dayList = new ArrayList<String>();
							int dateVariance = 3;
							Date depatureDate = CalendarUtil.getStartTimeOfDate(flightInfo.getFirstDepartureDate());
							Date dateStartCal = CalendarUtil.getOfssetAddedTime(depatureDate, dateVariance * -1);
							Date currentDate = CalendarUtil.getStartTimeOfDate(CalendarUtil.getCurrentSystemTimeInZulu());

							if (dateStartCal.compareTo(currentDate) < 0) {
								dateStartCal.setTime(currentDate.getTime());
							}

							for (int i = 0; i < 7; i++) {
								dayList.add(CalendarUtil.formatDate(dateStartCal, "dd/MM/yyyy"));
								dateStartCal = CalendarUtil.addDateVarience(dateStartCal, 1);
							}
							calendarDayList.add(dayList);
							break ONDFLIT;

						}
					}
				}
			}
		}

		return calendarDayList;
	}

	public static List<List<String>> buildIBECalendarDays(List<OriginDestinationInformationTO> originDestinationInformationList) {
		List<List<String>> calendarDayList = new ArrayList<List<String>>();
		if (originDestinationInformationList != null && originDestinationInformationList.size() > 0) {

			for (OriginDestinationInformationTO ondInfoTO : originDestinationInformationList) {

				List<String> dayList = new ArrayList<String>();
				int dateVariance = 3;
				Date depatureDate = CalendarUtil.getStartTimeOfDate(ondInfoTO.getPreferredDate());
				Date dateStartCal = CalendarUtil.getOfssetAddedTime(depatureDate, dateVariance * -1);
				Date currentDate = CalendarUtil.getStartTimeOfDate(CalendarUtil.getCurrentSystemTimeInZulu());

				if (dateStartCal.compareTo(currentDate) < 0) {
					dateStartCal.setTime(currentDate.getTime());
				}

				for (int i = 0; i < 7; i++) {
					dayList.add(CalendarUtil.formatDate(dateStartCal, "dd/MM/yyyy"));
					dateStartCal = CalendarUtil.addDateVarience(dateStartCal, 1);
				}
				calendarDayList.add(dayList);

			}

		}

		return calendarDayList;
	}

	public static List<List<CalendarFlightAvailInfo>> updateCalendarFlightAvailability(
			List<OriginDestinationInformationTO> originDestinationInformationList, List<List<String>> calendarDayList) {
		List<List<CalendarFlightAvailInfo>> ondCalFareAvailList = new ArrayList<List<CalendarFlightAvailInfo>>();
		for (int ond = 0; ond < calendarDayList.size(); ond++) {
			List<CalendarFlightAvailInfo> calFareAvailList = new ArrayList<CalendarFlightAvailInfo>();
			List<String> dateList = calendarDayList.get(ond);
			OriginDestinationInformationTO ondInfo = originDestinationInformationList.get(ond);
			for (int dt = 0; dt < dateList.size(); dt++) {
				CalendarFlightAvailInfo calFareAvail = new CalendarFlightAvailInfo();
				calFareAvail.setDate(dateList.get(dt));
				for (int opt = 0; opt < ondInfo.getOrignDestinationOptions().size(); opt++) {
					OriginDestinationOptionTO ondOpt = ondInfo.getOrignDestinationOptions().get(opt);
					if (ondOpt.getFirstDepartureDate() != null) {
						String depDateStr = CalendarUtil.formatDate(ondOpt.getFirstDepartureDateLocal(), "dd/MM/yyyy");
						if (depDateStr.equals(calFareAvail.getDate())) {
							calFareAvail.setIsFareAvailable(true);
							break;
						}
					}
				}
				calFareAvailList.add(calFareAvail);
			}
			ondCalFareAvailList.add(calFareAvailList);
		}
		return ondCalFareAvailList;
	}

	public static void filterSelDateFlights(FlightAvailRS flightAvailRS) {
		List<OriginDestinationInformationTO> ondList = flightAvailRS.getOrderedOriginDestinationInformationList();
		for (int ond = 0; ond < ondList.size(); ond++) {
			List<OriginDestinationOptionTO> filteredOpts = new ArrayList<OriginDestinationOptionTO>();
			OriginDestinationInformationTO ondInfo = ondList.get(ond);
			for (int opt = 0; opt < ondInfo.getOrignDestinationOptions().size(); opt++) {
				OriginDestinationOptionTO ondOpt = ondInfo.getOrignDestinationOptions().get(opt);
				if (CalendarUtil.formatDate(ondOpt.getFirstDepartureDateLocal(), "dd/MM/yyyy")
						.equals(CalendarUtil.formatDate(ondInfo.getPreferredDate(), "dd/MM/yyyy"))) {
					filteredOpts.add(ondOpt);
				}
			}
			flightAvailRS.getOrderedOriginDestinationInformationList().get(ond).getOrignDestinationOptions().clear();
			flightAvailRS.getOrderedOriginDestinationInformationList().get(ond).getOrignDestinationOptions().addAll(filteredOpts);
		}
	}

	public static void validateDateVarience(Date preferredDate, Integer varience, AppIndicatorEnum appIndicator)
			throws ModuleException {

		int maxVarience = AppSysParamsUtil.getMaxDepartureReturnVarience(appIndicator);

		if (varience > maxVarience) {
			if ((maxVarience % 2 != 0 && (varience > 2 * maxVarience || ((2 * maxVarience) + 1 < (varience
					+ (CalendarUtil.getTimeDifferenceInDays(CalendarUtil.truncateTime(CalendarUtil.getCurrentSystemTimeInZulu()),
							CalendarUtil.truncateTime(preferredDate)))))))
					|| (maxVarience % 2 == 0 && (varience > (2 * maxVarience - 1)
							|| ((2 * maxVarience - 1) < (varience + (CalendarUtil.getTimeDifferenceInDays(
									CalendarUtil.truncateTime(CalendarUtil.getCurrentSystemTimeInZulu()),
									CalendarUtil.truncateTime(preferredDate)))))))) {
				throw new ModuleException("module.invalid.departurereturn.variance");

			}

		}

	}

	public static void setFlightStatus(FlightSegmentTO flightSegmentTO, List<ONDSearchDTO> ondList) {
		for (ONDSearchDTO ondSearchDTO : ondList) {
			for (String flightRPH : ondSearchDTO.getFlightRPHList()) {
				IFlightSegment tmpFlightSegmentTO = FlightRefNumberUtil.getFlightSegmentFromRPH(flightRPH);
				if (flightSegmentTO.getSegmentCode().equals(tmpFlightSegmentTO.getSegmentCode())
						&& flightSegmentTO.getDepartureDateTime().equals(tmpFlightSegmentTO.getDepartureDateTime())) {
					flightSegmentTO.setFlightSegmentStatus(ondSearchDTO.getStatus());
				}
			}
		}
	}

	public static boolean isCaptchaValidationRequired(HttpServletRequest request) {

		if (!AppSysParamsUtil.isIBEHumanVerficationEnabled()) {
			return false;
		}

		boolean captchaValidationRequired = false;
		if (request.getAttribute(HumanVerificationConstants.VALIDATE_CAPTCHA) != null
				&& (Boolean) request.getAttribute(HumanVerificationConstants.VALIDATE_CAPTCHA)) {

			if (request.getSession().getAttribute(HumanVerificationConstants.CAPTCHA_VALIDATED) != null
					&& (Boolean) request.getSession().getAttribute(HumanVerificationConstants.CAPTCHA_VALIDATED)) {

				int sessionHitCount = (Integer) request.getSession().getAttribute(HumanVerificationConstants.MAX_SESSION_HITS);
				sessionHitCount++;
				int maxPerSessionHitCount = AppSysParamsUtil.maxPerSessionHitCount();

				if (sessionHitCount > maxPerSessionHitCount) {
					captchaValidationRequired = true;
					request.getSession().setAttribute(HumanVerificationConstants.CAPTCHA_VALIDATED, false);
					request.getSession().setAttribute(HumanVerificationConstants.MAX_SESSION_HITS, 0);
				} else {
					request.getSession().setAttribute(HumanVerificationConstants.MAX_SESSION_HITS, sessionHitCount);
				}
			} else {
				captchaValidationRequired = true;
			}
		}

		return captchaValidationRequired;
	}

	public static Map<Integer, NameDTO> getNameChangedPaxMap(String paxNameDetails) throws Exception {
		Map<Integer, NameDTO> colpaxs = new HashMap<Integer, NameDTO>();
		JSONObject jsonPaxObj = (JSONObject) new JSONParser().parse(paxNameDetails);
		if (jsonPaxObj.get("adultNames") != null) {
			JSONArray paxAdults = (JSONArray) jsonPaxObj.get("adultNames");
			Iterator<?> itIter = paxAdults.iterator();
			while (itIter.hasNext()) {
				JSONObject jPPObj = (JSONObject) itIter.next();
				if (jPPObj != null) {
					NameDTO nameDTO = transformAdultName(jPPObj);
					colpaxs.put(nameDTO.getPnrPaxId(), nameDTO);
				}
			}
		}

		if (jsonPaxObj.get("infantNames") != null) {
			JSONArray paxAdults = (JSONArray) jsonPaxObj.get("infantNames");
			Iterator<?> itIter = paxAdults.iterator();
			while (itIter.hasNext()) {
				JSONObject jPPObj = (JSONObject) itIter.next();
				if (jPPObj != null) {
					NameDTO nameDTO = transformInfantName(jPPObj);
					colpaxs.put(nameDTO.getPnrPaxId(), nameDTO);
				}
			}
		}

		return colpaxs;
	}

	private static NameDTO transformAdultName(JSONObject jPPObj) {
		NameDTO nameDTO = new NameDTO();
		nameDTO.setTitle(BeanUtils.nullHandler(jPPObj.get("displayAdultTitle")));
		nameDTO.setFirstname(BeanUtils.nullHandler(jPPObj.get("displayAdultFirstName")));
		nameDTO.setLastName(BeanUtils.nullHandler(jPPObj.get("displayAdultLastName")));
		nameDTO.setPnrPaxId(PaxTypeUtils.getPnrPaxId(BeanUtils.nullHandler(jPPObj.get("displayPaxTravelReference"))));
		nameDTO.setPaxType(BeanUtils.nullHandler(jPPObj.get("displayAdultType")));
		nameDTO.setPaxReference(BeanUtils.nullHandler(jPPObj.get("displayPaxTravelReference")));
		nameDTO.setFFID(BeanUtils.nullHandler(jPPObj.get("displayFFID")));

		return nameDTO;
	}

	private static NameDTO transformInfantName(JSONObject jPPObj) {
		NameDTO nameDTO = new NameDTO();
		nameDTO.setFirstname(BeanUtils.nullHandler(jPPObj.get("displayInfantFirstName")));
		nameDTO.setLastName(BeanUtils.nullHandler(jPPObj.get("displayInfantLastName")));
		nameDTO.setPnrPaxId(PaxTypeUtils.getPnrPaxId(BeanUtils.nullHandler(jPPObj.get("displayPaxTravelReference"))));
		nameDTO.setPaxType(BeanUtils.nullHandler(jPPObj.get("displayInfantType")));
		nameDTO.setPaxReference(BeanUtils.nullHandler(jPPObj.get("displayPaxTravelReference")));
		return nameDTO;
	}

	public static List<FlightSegmentTO> createFlightSegmentTos(BaseAvailRS baseAvailRS) {

		List<FlightSegmentTO> collection = new ArrayList<FlightSegmentTO>();
		int sequence = 0;
		for (OriginDestinationInformationTO originDestinationInformationTO : baseAvailRS
				.getOrderedOriginDestinationInformationList()) {
			for (OriginDestinationOptionTO originDestinationOptionTO : originDestinationInformationTO
					.getOrignDestinationOptions()) {
				for (FlightSegmentTO flightSegmentTO : originDestinationOptionTO.getFlightSegmentList()) {
					flightSegmentTO.setOndSequence(sequence);
					collection.add(flightSegmentTO);
				}
			}
			sequence++;
		}
		Collections.sort(collection);
		return collection;
	}

	/**
	 * Check if the flight has available seats for the given flightRPH
	 * 
	 * @return true if seats available in the flight otherwise false will returned
	 * @param adultCount
	 *            addition of adult and child count for the reservation
	 * @param infantCount
	 *            infant count of the reservation
	 * 
	 */
	public static boolean hasInventoryForFlight(BaseAvailRS baseAvailRS, String flightRPH, int adultCount, int infantCount)
			throws ModuleException {

		if (baseAvailRS.getOriginDestinationInformationList() != null) {
			for (OriginDestinationInformationTO ondInfoTO : baseAvailRS.getOriginDestinationInformationList()) {

				for (OriginDestinationOptionTO ondOpTO : ondInfoTO.getOrignDestinationOptions()) {

					for (FlightSegmentTO flightSegmentTO : ondOpTO.getFlightSegmentList()) {

						if (flightSegmentTO.getFlightRefNumber().equals(flightRPH)) {
							if (!flightSegmentTO.hasRequestingNumOfSeats(adultCount, infantCount)) {
								return false;
							} else {
								return true;
							}
						}

					}

				}
			}
		}

		return false;

	}

	public static Collection<PaxChargesTO> transformPaxList(Collection<ReservationPaxTO> paxList) {
		Collection<PaxChargesTO> paxExtChargesList = new ArrayList<PaxChargesTO>();
		if (paxList != null && !paxList.isEmpty()) {
			for (ReservationPaxTO resPaxTO : paxList) {
				PaxChargesTO paxExternalChargesTO = new PaxChargesTO();
				String paxType = resPaxTO.getPaxType();
				if (PaxTypeTO.INFANT.equals(paxType)) {
					continue;
				}

				if (PaxTypeTO.PARENT.equals(paxType)) {
					paxType = PaxTypeTO.ADULT;
				}

				paxExternalChargesTO.setPaxTypeCode(paxType);
				paxExternalChargesTO.setPaxSequence(resPaxTO.getSeqNumber());
				paxExternalChargesTO.setExtChgList(resPaxTO.getExternalCharges());
				paxExternalChargesTO.setParent(resPaxTO.getIsParent());

				paxExtChargesList.add(paxExternalChargesTO);
			}
		}
		return paxExtChargesList;
	}

	public static DiscountRQ getPromotionCalculatorRQ(DiscountedFareDetails discountInfoDTO,
			Collection<PaxChargesTO> paxExtChargesList, Collection<PassengerTypeQuantityTO> paxQtyList) {
		DiscountRQ promotionRQ = null;
		if (discountInfoDTO != null && ((paxExtChargesList != null && !paxExtChargesList.isEmpty())
				|| (paxQtyList != null && !paxQtyList.isEmpty()))) {
			DISCOUNT_METHOD discountMethod = null;
			if (discountInfoDTO.getPromotionId() == null) {
				if (discountInfoDTO.getDomesticSegmentCodeList().size() > 0 && AppSysParamsUtil.isDomesticFareDiscountEnabled()) {
					discountMethod = DISCOUNT_METHOD.DOM_FARE_DISCOUNT;

				} else if (discountInfoDTO.getFarePercentage() > 0) {
					discountMethod = DISCOUNT_METHOD.FARE_DISCOUNT;

				}
			} else {
				discountMethod = DISCOUNT_METHOD.PROMOTION;
			}

			promotionRQ = new DiscountRQ(discountMethod);
			promotionRQ.setDiscountInfoDTO(discountInfoDTO);
			promotionRQ.setPaxChargesList(paxExtChargesList);
			promotionRQ.setPaxQtyList(paxQtyList);
		}

		return promotionRQ;

	}

	public static BigDecimal addPaxWiseApplicableServiceTax(Collection<ReservationPaxTO> paxList,
			ServiceTaxQuoteForTicketingRevenueRS serviceTaxQuoteRS) {
		BigDecimal totalServiceTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (serviceTaxQuoteRS != null && paxList != null && !paxList.isEmpty()) {

			String taxDepositCountryCode = serviceTaxQuoteRS.getServiceTaxDepositeCountryCode();
			String taxDepositStateCode = serviceTaxQuoteRS.getServiceTaxDepositeStateCode();

			for (ReservationPaxTO pax : paxList) {

				List<LCCClientExternalChgDTO> chgDTOs = new ArrayList<>();
				List<LCCClientExternalChgDTO> infantChgDTOs = new ArrayList<>();
				String paxType = pax.getPaxType();
				Integer paxSequence = pax.getSeqNumber();
				boolean isParent = pax.getIsParent();

				if (PaxTypeTO.INFANT.equals(paxType)) {
					continue;
				}

				if (serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes() != null
						&& !serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes().isEmpty()) {
					List<ServiceTaxDTO> serviceTaxs = serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes().get(paxType);
					addPaxServiceTaxesAsExtCharges(serviceTaxs, chgDTOs, taxDepositCountryCode, taxDepositStateCode);
					printServiceTaxChargesWithTotal(serviceTaxs, " PAX-Type " + paxType.toUpperCase());
					if (isParent) {
						List<ServiceTaxDTO> infantServiceTaxs = serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes()
								.get(PaxTypeTO.INFANT);
						addPaxServiceTaxesAsExtCharges(infantServiceTaxs, infantChgDTOs, taxDepositCountryCode,
								taxDepositStateCode);
						printServiceTaxChargesWithTotal(infantServiceTaxs, " PAX-Type " + PaxTypeTO.INFANT);
					}

				}

				if (serviceTaxQuoteRS.getPaxWiseServiceTaxes() != null && !serviceTaxQuoteRS.getPaxWiseServiceTaxes().isEmpty()) {
					List<ServiceTaxDTO> serviceTaxs = serviceTaxQuoteRS.getPaxWiseServiceTaxes().get(paxSequence);
					addPaxServiceTaxesAsExtCharges(serviceTaxs, chgDTOs, taxDepositCountryCode, taxDepositStateCode);
					printServiceTaxChargesWithTotal(serviceTaxs, " PAX-Sequence " + paxSequence);
				}

				if (chgDTOs != null && !chgDTOs.isEmpty()) {
					List<LCCClientExternalChgDTO> groupedChargeDTOs = groupServiceTaxByChargeCode(chgDTOs);
					if (groupedChargeDTOs != null && !groupedChargeDTOs.isEmpty()) {
						totalServiceTaxAmount = AccelAeroCalculator.add(totalServiceTaxAmount,
								getTotalServiceTaxAmount(groupedChargeDTOs));
						pax.addExternalCharges(groupedChargeDTOs);
					}

				}

				if (isParent) {
					if (infantChgDTOs != null && !infantChgDTOs.isEmpty()) {
						List<LCCClientExternalChgDTO> groupedChargeDTOs = groupServiceTaxByChargeCode(infantChgDTOs);
						if (groupedChargeDTOs != null && !groupedChargeDTOs.isEmpty()) {
							totalServiceTaxAmount = AccelAeroCalculator.add(totalServiceTaxAmount,
									getTotalServiceTaxAmount(groupedChargeDTOs));
							pax.addInfantExternalCharges(groupedChargeDTOs);
						}
					}
				}

			}
		}
		return totalServiceTaxAmount;
	}

	private static LCCClientExternalChgDTO externalChargeAdaptor(ServiceTaxDTO serviceTax, boolean isTicketingRevenue,
			String taxDepositCountryCode, String taxDepositStateCode) {

		LCCClientExternalChgDTO chg = new LCCClientExternalChgDTO();
		chg.setAmount(serviceTax.getAmount());
		chg.setExternalCharges(EXTERNAL_CHARGES.SERVICE_TAX);
		chg.setCode(serviceTax.getChargeCode());
		chg.setFlightRefNumber(serviceTax.getFlightRefNumber());
		chg.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(serviceTax.getFlightRefNumber()));
		chg.setTaxableAmount(serviceTax.getTaxableAmount());
		chg.setNonTaxableAmount(serviceTax.getNonTaxableAmount());
		chg.setTicketingRevenue(isTicketingRevenue);
		chg.setTaxDepositCountryCode(taxDepositCountryCode);
		chg.setTaxDepositStateCode(taxDepositStateCode);

		return chg;

	}

	public static BigDecimal getAmountDueWhenOnlyInfantHasAmountDue(LCCClientPassengerSummaryTO adultPaxSummaryTO,
			Collection<LCCClientPassengerSummaryTO> colPassengerSummary, boolean isInfantPaymentSeparated) {

		BigDecimal amountDue = adultPaxSummaryTO.getTotalAmountDue();

		if (isInfantPaymentSeparated && amountDue.compareTo(BigDecimal.ZERO) <= 0) {
			for (LCCClientPassengerSummaryTO infantSummaryTO : colPassengerSummary) {
				if (PaxTypeTO.INFANT.equals(infantSummaryTO.getPaxType())) {

					String adultPaxSequence = getPaxSequence(adultPaxSummaryTO.getTravelerRefNumber());
					String infantParentSequence = getAdultPaxSequence(infantSummaryTO.getTravelerRefNumber());
					if (!StringUtil.isNullOrEmpty(adultPaxSequence) && adultPaxSequence.equalsIgnoreCase(infantParentSequence)
							&& infantSummaryTO.getTotalAmountDue()
									.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
						amountDue = infantSummaryTO.getTotalAmountDue();
						break;
					}

				}
			}
		}

		return amountDue;
	}

	public static String getPaxSequence(String travelRef) {
		if (!StringUtil.isNullOrEmpty(travelRef)) {
			String travelerRefNoArray[] = travelRef.split(",");
			if (travelerRefNoArray != null && travelerRefNoArray.length > 0) {
				for (int i = 0; i < travelerRefNoArray.length; i++) {
					if (!StringUtil.isNullOrEmpty(travelerRefNoArray[i])) {
						String[] adultArray = travelerRefNoArray[i].split("\\$");
						if (adultArray[0] != null) {
							String[] paxSequence = adultArray[0].split("\\|");

							if (paxSequence != null && paxSequence.length > 0)
								return paxSequence[1];
						}
					}

				}
			}

		}

		return null;

	}

	public static String getAdultPaxSequence(String infantTravelRef) {
		if (!StringUtil.isNullOrEmpty(infantTravelRef)) {
			String travelerRefNoArray[] = infantTravelRef.split(",");
			if (travelerRefNoArray != null && travelerRefNoArray.length > 0) {
				for (int i = 0; i < travelerRefNoArray.length; i++) {
					if (!StringUtil.isNullOrEmpty(travelerRefNoArray[i])) {
						String[] infantArray = travelerRefNoArray[i].split("\\$");
						if (infantArray[0] != null) {
							String[] adultSequence = infantArray[0].split("\\/");

							if (adultSequence != null && adultSequence.length > 0)
								return adultSequence[1];
						}
					}

				}
			}

		}
		return null;
	}

	private static List<LCCClientExternalChgDTO> groupServiceTaxByChargeCode(List<LCCClientExternalChgDTO> chgDTOs) {
		List<LCCClientExternalChgDTO> groupChgDTOs = new ArrayList<>();
		if (chgDTOs != null && !chgDTOs.isEmpty()) {
			Map<String, Map<String, List<LCCClientExternalChgDTO>>> chargesFltWiseByChargeCodeMap = new HashMap<>();
			for (LCCClientExternalChgDTO chgDTO : chgDTOs) {
				String flightRef = chgDTO.getFlightRefNumber();
				String chargeCode = chgDTO.getCode();

				if (chargesFltWiseByChargeCodeMap.get(flightRef) == null) {
					chargesFltWiseByChargeCodeMap.put(flightRef, new HashMap<String, List<LCCClientExternalChgDTO>>());
				}

				if (chargesFltWiseByChargeCodeMap.get(flightRef).get(chargeCode) == null) {
					List<LCCClientExternalChgDTO> chargeGrpList = new ArrayList<>();
					chargeGrpList.add(chgDTO);
					chargesFltWiseByChargeCodeMap.get(flightRef).put(chargeCode, chargeGrpList);
				} else {
					List<LCCClientExternalChgDTO> chargeGrpList = chargesFltWiseByChargeCodeMap.get(flightRef).get(chargeCode);

					if (chargeGrpList == null || chargeGrpList.isEmpty()) {
						chargeGrpList = new ArrayList<>();
						chargeGrpList.add(chgDTO);
						chargesFltWiseByChargeCodeMap.get(flightRef).put(chargeCode, chargeGrpList);
					} else {
						for (LCCClientExternalChgDTO existChargeDTO : chargeGrpList) {
							if ((existChargeDTO.isTicketingRevenue() && chgDTO.isTicketingRevenue())
									|| (!existChargeDTO.isTicketingRevenue() && !chgDTO.isTicketingRevenue())) {
								existChargeDTO.setAmount(AccelAeroCalculator.add(existChargeDTO.getAmount(), chgDTO.getAmount()));
								existChargeDTO.setTaxableAmount(
										AccelAeroCalculator.add(existChargeDTO.getTaxableAmount(), chgDTO.getAmount()));
								existChargeDTO.setNonTaxableAmount(
										AccelAeroCalculator.add(existChargeDTO.getNonTaxableAmount(), chgDTO.getAmount()));
							} else {
								chargesFltWiseByChargeCodeMap.get(flightRef).get(chargeCode).add(chgDTO);
							}

						}
					}

				}

			}

			if (chargesFltWiseByChargeCodeMap != null && !chargesFltWiseByChargeCodeMap.isEmpty()) {
				for (String key : chargesFltWiseByChargeCodeMap.keySet()) {
					Map<String, List<LCCClientExternalChgDTO>> chargesByChargeCodeMap = chargesFltWiseByChargeCodeMap.get(key);
					if (chargesByChargeCodeMap != null && !chargesByChargeCodeMap.isEmpty()) {
						for (String chargeCode : chargesByChargeCodeMap.keySet()) {
							List<LCCClientExternalChgDTO> chargeGrpList = chargesByChargeCodeMap.get(chargeCode);
							if (chargeGrpList != null && !chargeGrpList.isEmpty()) {
								groupChgDTOs.addAll(chargeGrpList);
							}
						}
					}

				}
			}

		}

		return groupChgDTOs;

	}

	public static void addPaxServiceTaxesAsExtCharges(List<ServiceTaxDTO> serviceTaxs, List<LCCClientExternalChgDTO> chgDTOs,
			String taxDepositCountryCode, String taxDepositStateCode) {
		if (chgDTOs != null && serviceTaxs != null && !serviceTaxs.isEmpty()) {
			for (ServiceTaxDTO serviceTax : serviceTaxs) {
				chgDTOs.add(externalChargeAdaptor(serviceTax, true, taxDepositCountryCode, taxDepositStateCode));

			}
		}
	}

	// TODO: temporary audit for verification purpose only
	private static void printServiceTaxChargesWithTotal(List<ServiceTaxDTO> serviceTaxs, String description) {
		BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		int i = 1;
		if (serviceTaxs != null && !serviceTaxs.isEmpty()) {

			for (ServiceTaxDTO serviceTax : serviceTaxs) {
				log.info(i + " , " + serviceTax.getChargeCode() + " , " + serviceTax.getFlightRefNumber() + " , "
						+ serviceTax.getAmount());
				totalAmount = AccelAeroCalculator.add(totalAmount, serviceTax.getAmount());
				i++;
			}

		}

		log.info("Sevice Tax Calculation summary [ " + description + " ] Entries: " + i + " Total Amount :" + totalAmount);
	}

	private static BigDecimal getTotalServiceTaxAmount(List<LCCClientExternalChgDTO> groupedChargeDTOs) {
		BigDecimal totalTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (groupedChargeDTOs != null && !groupedChargeDTOs.isEmpty()) {
			for (LCCClientExternalChgDTO groupedChargeDTO : groupedChargeDTOs) {
				totalTaxAmount = AccelAeroCalculator.add(totalTaxAmount, groupedChargeDTO.getAmount());
			}
		}
		return totalTaxAmount;
	}

	public static boolean isRequoteValidWithServiceTax(String oldFromAirportCode, String newFromAirportCode, boolean isGroupPNR,
			boolean isTaxInvoicesExist) {
		String[] taxRegNoEnabledCountries = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries().split(",");
		if (taxRegNoEnabledCountries != null && taxRegNoEnabledCountries.length > 0) {
			if (oldFromAirportCode != null && newFromAirportCode != null) {
				if (!newFromAirportCode.equals(oldFromAirportCode)) {
					CachedAirportDTO newOriginAirport = null;
					CachedAirportDTO oldOriginAirport = null;

					try {
						Map<String, CachedAirportDTO> airportsInfo = WPModuleUtils.getAirportBD()
								.getCachedAllAirportMap(new ArrayList<>(Arrays.asList(newFromAirportCode, oldFromAirportCode)));
						oldOriginAirport = airportsInfo.get(oldFromAirportCode);
						newOriginAirport = airportsInfo.get(newFromAirportCode);
					} catch (ModuleException e) {
						log.error("ERROR in loading old and new origin codes", e);
						return false;
					}
					// isTaxInvoiceDoesNotExists()
					// isTaxInvoiceExistsAndNewRouteOriginIsNonTaxApplicable()
					// isTaxInvoiceNewRouteIsTaxApplicableAndStatesMatch()
					
					return !isTaxInvoicesExist ||!ArrayUtils.contains(taxRegNoEnabledCountries, newOriginAirport.getCountryCode())
							||  (
							StringUtils.equals(oldOriginAirport.getCountryCode(), newOriginAirport.getCountryCode())
									&& StringUtils.equals(oldOriginAirport.getStateCode(), newOriginAirport.getStateCode()));
				}
				return true;
			}
			return false;
		} else {
			return true;
		}
	}

	public static boolean isContactInfoValidForRouteModWithServiceTax(String newFromAirportCode, String contactCountry,
			String contactState, String contactAddress, boolean isGroupPnr) {
		String[] taxRegNoEnabledCountries = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries().split(",");
		if (!ArrayUtils.contains(taxRegNoEnabledCountries, contactCountry)) {
			return true;
		}
		if (ArrayUtils.contains(taxRegNoEnabledCountries, contactCountry)) {
			CachedAirportDTO airportDTO = null;
			try {
				Map<String, CachedAirportDTO> airportsInfo = WPModuleUtils.getAirportBD()
						.getCachedAllAirportMap(new ArrayList<>(Arrays.asList(newFromAirportCode)));
				airportDTO = airportsInfo.get(newFromAirportCode);
			} catch (ModuleException e) {
				log.error("ERROR in loading country code for the origin airport", e);
				return false;
			}
			return !ArrayUtils.contains(taxRegNoEnabledCountries, airportDTO.getCountryCode())
					|| (StringUtils.isNotBlank(contactState) && StringUtils.isNotBlank(contactAddress));
		}
		return false;
	}

	public static boolean isRequoteValidWithServiceTax(String oldFromAirportCode, OndLocationDTO newOriginLocationDto,
			boolean isGroupPNR, boolean isTaxInvoicesExist) {
		if (!newOriginLocationDto.getCity()) {
			return isRequoteValidWithServiceTax(oldFromAirportCode, newOriginLocationDto.getOndLocationCode(), isGroupPNR,
					isTaxInvoicesExist);
		} else {
			String[] taxRegNoEnabledCountries = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries().split(",");
			if (taxRegNoEnabledCountries != null && taxRegNoEnabledCountries.length > 0) {
				CachedAirportDTO oldOriginAirport = null;
				Set<String> countryAndStateCodesForNewOrigin = null;
				String newOriginCountryCode = null;
				try {
					Map<String, CachedAirportDTO> airportsInfo = WPModuleUtils.getAirportBD()
							.getCachedAllAirportMap(new ArrayList<>(Arrays.asList(oldFromAirportCode)));
					oldOriginAirport = airportsInfo.get(oldFromAirportCode);
					countryAndStateCodesForNewOrigin = WPModuleUtils.getAirportBD().getCityVsCountryAndStateCodesMap()
							.get(newOriginLocationDto.getOndLocationCode());

					if (CollectionUtils.isEmpty(countryAndStateCodesForNewOrigin)) {
						log.error("City id was not in cached city map");
						return false;
					}
					newOriginCountryCode = countryAndStateCodesForNewOrigin.iterator().next().split("_")[0];
				} catch (ModuleException e) {
					log.error("ERROR in loading old and new origin codes", e);
					return false;
				}
				return !ArrayUtils.contains(taxRegNoEnabledCountries, newOriginCountryCode) || !isTaxInvoicesExist
						|| (StringUtils.equals(oldOriginAirport.getCountryCode(), newOriginCountryCode)
								&& countryAndStateCodesForNewOrigin.size() == 1
								&& StringUtils.equals(oldOriginAirport.getCountryCode() + "_" + oldOriginAirport.getStateCode(),
										countryAndStateCodesForNewOrigin.iterator().next()));

			}
			return true;
		}
	}

	public static boolean isContactInfoValidForRouteModWithServiceTax(OndLocationDTO ondLocationDTO, String contactCountry,
			String contactState, String contactAddress, boolean isGroupPnr) {
		if (!ondLocationDTO.getCity()) {
			return isContactInfoValidForRouteModWithServiceTax(ondLocationDTO.getOndLocationCode(), contactCountry, contactState,
					contactAddress, isGroupPnr);
		} else {
			String[] taxRegNoEnabledCountries = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries().split(",");
			String newOriginCountryCode = null;
			Set<String> countryAndStateCodesForNewOrigin = null;
			if (ArrayUtils.contains(taxRegNoEnabledCountries, contactCountry)) {
				try {
					countryAndStateCodesForNewOrigin = WPModuleUtils.getAirportBD().getCityVsCountryAndStateCodesMap()
							.get(ondLocationDTO.getOndLocationCode());

					if (CollectionUtils.isEmpty(countryAndStateCodesForNewOrigin)) {
						log.error("City id was not in cached city map");
						return false;
					}
					newOriginCountryCode = countryAndStateCodesForNewOrigin.iterator().next().split("_")[0];
				} catch (ModuleException e) {
					log.error("ERROR in loading old and new origin codes", e);
				}
				return !ArrayUtils.contains(taxRegNoEnabledCountries, newOriginCountryCode)
						|| (StringUtils.isNotBlank(contactState) && StringUtils.isNotBlank(contactAddress));
			}
			return false;
		}
	}

	private static CachedAirportDTO getAirportInfo(String fromAirport) {
		Object[] airportInfo = CommonsServices.getGlobalConfig().retrieveAirportInfo(fromAirport);
		CachedAirportDTO cachedAirportDTO = new CachedAirportDTO();
		if (airportInfo != null && airportInfo.length > 5) {
			cachedAirportDTO.setCountryCode((String) airportInfo[3]);
			cachedAirportDTO.setStateCode((String) airportInfo[5]);
		}
		return cachedAirportDTO;
	}

	public static TaxTO convertToTaxTO(ServiceTaxTO serviceTaxTO, String paxType) {

		TaxTO taxTO = new TaxTO();

		taxTO.setAmount(serviceTaxTO.getAmount());
		taxTO.setCarrierCode(serviceTaxTO.getCarrierCode());
		taxTO.getApplicablePassengerTypeCode().add(paxType);
		taxTO.setChargeCode(serviceTaxTO.getChargeCode());
		taxTO.setChargeRateId(String.valueOf(serviceTaxTO.getChargeRateId()));
		taxTO.setChagrgeGroupCode(serviceTaxTO.getChargeGroupCode());
		taxTO.setTaxCode(serviceTaxTO.getChargeCode());
		taxTO.setSegmentCode(serviceTaxTO.getFlightRefNumber().split("\\$")[1]);

		return taxTO;
	}

	private static void updateOriginDestinationCitySearchEnabled(boolean isEnableCityBasesFunctionality,
			Map<Integer, Map<String, Boolean>> ondWiseCitySearch, OriginDestinationInformationTO ondInfo, int ondSequence) {
		if (isEnableCityBasesFunctionality && ondWiseCitySearch != null && !ondWiseCitySearch.isEmpty()) {
			Map<String, Boolean> citySearchMap = ondWiseCitySearch.get(ondSequence);
			if (citySearchMap != null && !citySearchMap.isEmpty()) {
				if (citySearchMap.get("DEP") != null) {
					ondInfo.setDepartureCitySearch(citySearchMap.get("DEP"));
				}
				if (citySearchMap.get("ARR") != null) {
					ondInfo.setArrivalCitySearch(citySearchMap.get("ARR"));
				}

			}

		}
	}

	private static boolean isInValidOriginDestinationInformation(boolean isNextPrevious, boolean fetchReturnFlightsOnly,
			boolean isReturnOnDInfo) {
		if (!isNextPrevious && ((fetchReturnFlightsOnly && !isReturnOnDInfo) || (!fetchReturnFlightsOnly && isReturnOnDInfo))) {
			return true;
		}
		return false;
	}

	public static void updateOnDCitySearchInfoForNextPrev(FlightAvailRQ flightAvailRQ,
			Map<Integer, Map<String, Boolean>> ondWiseCitySearch) {
		boolean isEnableCityBasesFunctionality = AppSysParamsUtil.enableCityBasesFunctionality();
		if (isEnableCityBasesFunctionality && flightAvailRQ != null && flightAvailRQ.getOriginDestinationInformationList() != null
				&& !flightAvailRQ.getOriginDestinationInformationList().isEmpty()) {
			for (OriginDestinationInformationTO ondInfo : flightAvailRQ.getOriginDestinationInformationList()) {
				updateOriginDestinationCitySearchEnabled(isEnableCityBasesFunctionality, ondWiseCitySearch, ondInfo,
						OndSequence.IN_BOUND);
			}
		}
	}

}