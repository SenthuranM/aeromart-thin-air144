/*
 * ==============================================================================
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webplatform.api.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.FreeTextType;
import org.opentravel.ota._2003._05.IOTACodeTables;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

/**
 * @author Mohamed Nasly
 * @author Nilindra
 * @author Mehdi
 * @author Byorn
 */
public class CommonUtil {
	/** Decimals in currency values */
	public static final BigInteger DECIMALS_AED = new BigInteger("2");

	/** Default decimals truncation for BigDecimal input */
	public static DecimalFormat DEFAULT_DECIMAL_FORMAT = new DecimalFormat(".####");

	private static final Log log = LogFactory.getLog(CommonUtil.class);

	/**
	 * Check if given departure/arrival belongs to inbound segment
	 * 
	 * @param obAirports
	 * @param departureAirport
	 * @param arrivalAirport
	 * @return boolean
	 */
	public static boolean isReturnSegment(Collection<String> obAirports, String departureAirport, String arrivalAirport) {
		return obAirports.contains(departureAirport) && obAirports.contains(arrivalAirport);
	}

	/**
	 * Return false if any of airport in the ibound journey is not present in outbound journey
	 * 
	 * @param obAirports
	 * @param inAirports
	 * @return boolean
	 */
	public static boolean validateReturnJouney(Collection<String> obAirports, Collection<String> inAirports) {
		for (String obAirport : obAirports) {
			if (!inAirports.contains(obAirport)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Converts a collection of string to a string literal having comma sperated values
	 * 
	 * @param col
	 * @return String
	 */
	public static String getCollectionElementsStr(Collection<String> col) {
		String colStr = "";
		for (String item : col) {
			if (colStr.equals("")) {
				colStr = item;
			} else {
				colStr += "," + item;
			}
		}
		return colStr;
	}

	/**
	 * Parse java.util.Date to javax.xml.datatype.XMLGregorianCalendar
	 * 
	 * @param date
	 * @return
	 * @throws ModuleException
	 */
	public static XMLGregorianCalendar parse(Date date) throws ModuleException {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return getXMLGregorianCalendar(cal);
	}

	/**
	 * Parse java.util.Date to javax.xml.datatype.XMLGregorianCalendar with only Time included
	 * 
	 * @param date
	 * @return
	 * @throws ModuleException
	 */
	public static XMLGregorianCalendar parseTime(Date date) throws ModuleException {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return getXMLGregorianCalendarTime(cal);
	}

	/**
	 * Parse java.util.Date to javax.xml.datatype.XMLGregorianCalendar
	 * 
	 * @param date
	 * @return
	 * @throws ModuleException
	 */
	public static XMLGregorianCalendar parseZuluDateTime(Date date) throws ModuleException {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return getXMLZuluGregorianCalendar(cal);
	}

	/**
	 * Prepare javax.xml.datatype.XMLGregorianCalendar conforming to yyyy-MM-dd'T':%m%s format from java.util.Calendar
	 * 
	 * @param cal
	 *            Calendar
	 * @return XMLGregorianCalendar =2006-12-20T10:30:00.0Z]
	 * @throws ModuleException
	 */
	public static XMLGregorianCalendar getXMLZuluGregorianCalendar(Calendar cal) throws ModuleException {
		try {
			DatatypeFactory factory = DatatypeFactory.newInstance();
			return factory.newXMLGregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE),
					cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND), 0, 0);
		} catch (DatatypeConfigurationException e) {
			log.error(e);
			// TODO defined exception code
			throw new ModuleException(e, "");
			// throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_DATE, e.getMessage());
		}
	}

	/**
	 * Only set the date part. 2010-11-23Z
	 * 
	 * @param date
	 * @return
	 * @throws ModuleException
	 */
	public static XMLGregorianCalendar parseZuluDate(Date date) throws ModuleException {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return getXMLZuluGregorianDate(cal);
	}

	public static XMLGregorianCalendar getXMLZuluGregorianDate(Calendar cal) throws ModuleException {
		try {
			DatatypeFactory factory = DatatypeFactory.newInstance();
			return factory.newXMLGregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE),
					DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED,
					DatatypeConstants.FIELD_UNDEFINED, 0 // results in Z at the end
					);
		} catch (DatatypeConfigurationException e) {
			log.error(e);
			// TODO defined exception code
			throw new ModuleException(e, "");
			// throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_DATE, e.getMessage());
		}
	}

	/**
	 * Prepare javax.xml.datatype.XMLGregorianCalendar conforming to %Y-%M-%DT%h:%m%s format from java.util.Calendar
	 * 
	 * @param cal
	 *            Calendar
	 * @return XMLGregorianCalendar conforms to %Y-%M-%DT%h:%m%s format [example=2006-12-20T10:30:00]
	 * @throws ModuleException
	 */
	public static XMLGregorianCalendar getXMLGregorianCalendar(Calendar cal) throws ModuleException {
		try {
			DatatypeFactory factory = DatatypeFactory.newInstance();
			return factory.newXMLGregorianCalendar(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE),
					cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND),
					DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException e) {
			log.error(e);
			// TODO defined exception code
			throw new ModuleException(e, "");
			// throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_DATE, e.getMessage());
		}
	}

	/**
	 * Prepare javax.xml.datatype.XMLGregorianCalendar conforming to h:%m%s format from java.util.Calendar
	 * 
	 * @param cal
	 *            Calendar
	 * @return XMLGregorianCalendar conforms to %Y-%M-%DT%h:%m%s format [example=2006-12-20T10:30:00]
	 * @throws ModuleException
	 */
	public static XMLGregorianCalendar getXMLGregorianCalendarTime(Calendar cal) throws ModuleException {
		try {
			DatatypeFactory factory = DatatypeFactory.newInstance();
			return factory.newXMLGregorianCalendarTime(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),
					cal.get(Calendar.SECOND), DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException e) {
			log.error(e);
			// TODO exception code
			throw new ModuleException(e, "");
			// throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_DATE, e.getMessage());
		}
	}

	public static long getTime(XMLGregorianCalendar xmlCalendar) {
		return xmlCalendar.toGregorianCalendar().getTimeInMillis();
	}

	/**
	 * Prepare javax.xml.datatype.XMLGregorianCalendar conforming to %Y-%M-%DT%h:%m%s format
	 * 
	 * @param date
	 * @return XMLGregorianCalendar
	 * @throws ModuleException
	 */
	public static String getFormattedDate(Date date) throws ModuleException {
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		// cal.set(Calendar.SECOND, 0);//FIXME

		return getXMLGregorianCalendar(cal).toXMLFormat();
	}

	/**
	 * Prepare java.util.Calendar parsing date string conforming to yyyy-MM-dd'T'HH:mm:ss format
	 * 
	 * @param dateString
	 * @return Calendar
	 * @throws ModuleException
	 */
	public static Calendar parseDate(String dateString) throws ModuleException {
		try {
			if (log.isDebugEnabled())
				log.debug("parsing date [dateString=" + dateString + "]");
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			// return XMLGregorianCalendarImpl.parse(dateString).toGregorianCalendar();
			Calendar cal = Calendar.getInstance();
			cal.setTime(dateFormat.parse(dateString));
			return cal;
		} catch (ParseException e) {
			log.error(e);
			// TODO define exception code
			throw new ModuleException(e, "");
			// throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_DATE, e.getMessage());
		}

	}

	public static Calendar getZuluDate(XMLGregorianCalendar xmlCal) {
		Calendar cal = xmlCal.toGregorianCalendar();
		// remove offset injected due to time zone difference of the system to the calendar if any
		// make sure the functionality is uniform irrespective of the system time zone
		Calendar cal2 = Calendar.getInstance();
		cal.add(Calendar.MILLISECOND, -(cal2.getTimeZone().getRawOffset() + cal2.getTimeZone().getDSTSavings()));
		return cal;
	}

	public static Calendar parseZuluDateTime(String dateString) throws ModuleException {
		try {
			if (log.isDebugEnabled())
				log.debug("parsing date [dateString=" + dateString + "]");
			XMLGregorianCalendar xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(dateString);
			return getZuluDate(xmlCal);
		} catch (DatatypeConfigurationException e) {
			log.error(e);
			throw new ModuleException(e, "");
		}

	}

	/**
	 * Parses date time string sent xs:dateTime to java date
	 * 
	 * @param dateString
	 * @return
	 * @throws ModuleException
	 */
	public static Date parseDateTime(String dateString) throws ModuleException {
		try {
			if (log.isDebugEnabled())
				log.debug("parsing date [dateString=" + dateString + "]");

			if (dateString.endsWith("z") || dateString.endsWith("Z")) {
				dateString = dateString.substring(0, dateString.length() - 1);
			}
			XMLGregorianCalendar xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(dateString);

			return xmlCal.toGregorianCalendar().getTime();

		} catch (DatatypeConfigurationException e) {
			log.error(e);
			throw new ModuleException(e, "");
		}

	}

	/**
	 * TODO - control the format of the output
	 * 
	 * @param durationInMillis
	 * @return
	 * @throws ModuleException
	 */
	public static Duration getDuration(long durationInMillis) throws ModuleException {
		try {
			DatatypeFactory factory = DatatypeFactory.newInstance();
			return factory.newDuration(durationInMillis);
		} catch (DatatypeConfigurationException e) {
			log.error(e);
			throw new ModuleException(e, "");
		}
	}

	/**
	 * Prepare java.math.BigDecimal with default decimals inplace
	 * 
	 * @param doubleValue
	 * @param precision
	 * @return
	 */
	public static BigDecimal getBigDecimalWithDefaultPrecision(BigDecimal doubleValue) {
		return new BigDecimal(DEFAULT_DECIMAL_FORMAT.format(doubleValue)).setScale(DECIMALS_AED.intValue(), RoundingMode.UP);
	}

	public static List asList(List... multipleLists) {
		List list = new ArrayList();
		for (int x = 0; x < multipleLists.length; x++) {
			list.addAll(multipleLists[x]);
		}
		return list;
	}

	public static Collection asCollection(Collection... multipleCollections) {
		Collection collection = new ArrayList();
		for (int x = 0; x < multipleCollections.length; x++) {
			collection.addAll(multipleCollections[x]);
		}
		return collection;
	}

	public static FreeTextType getFreeTextType(String text) throws ModuleException {
		FreeTextType ftt = new FreeTextType();
		ftt.setValue(text);
		return ftt;
	}

	/**
	 * Handles null value and replace with a "" value. This also trims if a String value exist
	 * 
	 * @author byorn.
	 * @param string
	 * @return
	 */
	public static String nullHandler(Object object) {
		if (object == null) {
			return "";
		} else {
			return object.toString().trim();
		}
	}

	/**
	 * Retreives the element at given index from the list
	 * 
	 * @param <T>
	 * @param list
	 * @param index
	 * @return
	 */
	public static <T extends Object> T getValueFromNullSafeList(List<T> list, int index) {
		if (list != null && index < list.size())
			return list.get(index);
		else
			return null;
	}

	/**
	 * Retreives the element at given index from the list
	 * 
	 * @param list
	 * @param index
	 * @return
	 */
	public static String getStringFromNullSafeList(List<String> list, int index) {
		if (list != null && index < list.size())
			return list.get(index);
		else
			return null;
	}

	/**
	 * Get the code value from OTA combined code (codeTableNameCode.codeValue)
	 * 
	 * @param combinedCode
	 * @return
	 */
	public static String getOTACodeValue(String combinedCode) {
		return combinedCode.substring(combinedCode.indexOf('.') + 1);
	}

	public static boolean isEmpty(String str) {
		return BeanUtils.nullHandler(str).length() == 0;
	}

	/**
	 * Returns values rounded up to nearest 10.
	 * 
	 * @param totalAvailableBalance
	 * @param toNearest
	 * @return
	 */
	public static BigDecimal getRoundedUpValue(BigDecimal totalAvailableBalance, int toNearest) {
		int roundedUpAmount = totalAvailableBalance.setScale(0, RoundingMode.UP).intValue();

		while ((roundedUpAmount / toNearest) * toNearest != roundedUpAmount) {
			roundedUpAmount += 1;
		}
		return new BigDecimal(roundedUpAmount);
	}

	/**
	 * Returns corresponding WS paxTitle for AA paxTitle.
	 * 
	 * TODO - define AA - OTA pax title mappings in a configuration file
	 * 
	 * @param aaPaxTitle
	 * @return
	 */
	public static String getWSPaxTitle(String aaPaxTitle) {
		String wsPaxTitle = null;
		if (ReservationInternalConstants.PassengerTitle.MR.equals(aaPaxTitle)) {
			wsPaxTitle = getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_MR);
		} else if (ReservationInternalConstants.PassengerTitle.MS.equals(aaPaxTitle)) {
			wsPaxTitle = getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_MS);
		} else if (ReservationInternalConstants.PassengerTitle.CHILD.equals(aaPaxTitle)) {
			wsPaxTitle = getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_CHILD);
		} else if (ReservationInternalConstants.PassengerTitle.MRS.equals(aaPaxTitle)) {
			wsPaxTitle = getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_MRS);
		} else if (ReservationInternalConstants.PassengerTitle.MISS.equals(aaPaxTitle)) {
			wsPaxTitle = getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_MISS);
		} else if (ReservationInternalConstants.PassengerTitle.MASTER.equals(aaPaxTitle)) {
			wsPaxTitle = getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_MASTER);
		} else if (ReservationInternalConstants.PassengerTitle.DOCTOR.equals(aaPaxTitle)) {
			wsPaxTitle = getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_DOCTOR);
		} else if (ReservationInternalConstants.PassengerTitle.CAPTAIN.equals(aaPaxTitle)) {
			wsPaxTitle = getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_CAPTAIN);
		}
		return wsPaxTitle;
	}

	/**
	 * Returns corresponding AA paxTitle for WS paxTitle.
	 * 
	 * @param wsPaxTitle
	 * @return
	 */
	public static String getAAPaxTitle(String wsPaxTitle) {
		String aaPaxTitle = null;
		if (getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_MR).equals(wsPaxTitle)) {
			aaPaxTitle = ReservationInternalConstants.PassengerTitle.MR;
		} else if (getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_MS).equals(wsPaxTitle)) {
			aaPaxTitle = ReservationInternalConstants.PassengerTitle.MS;
		} else if (getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_CHILD).equals(wsPaxTitle)) {
			aaPaxTitle = ReservationInternalConstants.PassengerTitle.CHILD;
		} else if (getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_MRS).equals(wsPaxTitle)) {
			aaPaxTitle = ReservationInternalConstants.PassengerTitle.MRS;
		} else if (getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_MISS).equals(wsPaxTitle)) {
			aaPaxTitle = ReservationInternalConstants.PassengerTitle.MISS;
		} else if (getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_MASTER).equals(wsPaxTitle)) {
			aaPaxTitle = ReservationInternalConstants.PassengerTitle.MASTER;
		} else if (getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_DOCTOR).equals(wsPaxTitle)) {
			aaPaxTitle = ReservationInternalConstants.PassengerTitle.DOCTOR;
		} else if (getOTACodeValue(IOTACodeTables.AccelaeroPaxTitles_APT_CAPTAIN).equals(wsPaxTitle)) {
			aaPaxTitle = ReservationInternalConstants.PassengerTitle.CAPTAIN;
		}
		return aaPaxTitle;
	}

	public static String getCarrierCode(String flightNumber) {
		return flightNumber.substring(0, 2);
	}

	public static void checkCarrierCodeAccessibility(Collection usersCarrierCodes, String targetCarrierCode)
			throws ModuleException {
		if (usersCarrierCodes == null || !usersCarrierCodes.contains(targetCarrierCode)) {
			throw new ModuleException("module.invalid.carrier");
		}
	}

	public static String getActualPaxType(ReservationPaxTO pax) {
		String paxType = pax.getPaxType();

		if (PaxTypeTO.ADULT.equals(pax.getPaxType()) && pax.getIsParent()) {
			paxType = PaxTypeTO.PARENT;
		}

		return paxType;
	}

	public static Date parseXMLGregorianCalendar(XMLGregorianCalendar xmlGregorianCalendar) {
		if (xmlGregorianCalendar != null) {
			return new Date(getTime(xmlGregorianCalendar));
		} else {
			return null;
		}
	}
	
	public static String removeNonNumericCharactersFromNumericString (String numericString) {
		String nonNumericRegex = "[^\\d]";
		if (isEmpty(numericString)) {
			return "";
		} else {
			numericString = numericString.replaceAll(nonNumericRegex, "");
			return numericString;
		}
	}
	
	public static String removeNonLetterCharactersFromString (String inputString) {
		String nonLetterRegex = "[^a-zA-Z ]";
		if (isEmpty(inputString)) {
			return "";
		} else {
			String outputString = inputString.replaceAll(nonLetterRegex, "");
			return outputString;
		}
	}
	
	public static String removeInvalidCharactersFromEmailAddress (String emailAddress) {
		String emailRegex = "[^a-zA-Z0-9-_.@$]";
		if (isEmpty(emailAddress)) {
			return "";
		} else {
			String outputString = emailAddress.replaceAll(emailRegex, "");
			return outputString;
		}
	}
	
	public static String removeNonAlphaNumericCharactersFromString (String inputString) {
		String nonLetterRegex = "[^a-zA-Z0-9 ]";
		if (isEmpty(inputString)) {
			return "";
		} else {
			String outputString = inputString.replaceAll(nonLetterRegex, "");
			return outputString.replace(" ","").trim();
		}
	}

}