package com.isa.thinair.webplatform.api.util;

public class Constants {

	public static final String MODULE_CODE = "webplatform.desc";

	public static final String SES_PRIVILEGE_IDS = "sesPrivilegeIds";

	public static final String SES_SCREEN_WIDTH = "sesScreenWidth";

	public static final String APP_MAP_SECURE_URLS = "appMapSecureURLs";

	public static final String STRING_SEPARATOR = ",";
	
	public static final String COMMA_SEPARATOR = ",";

	public static final String LINE_SEPERATOR = "^";
	
	public static final String CARET_SEPERATOR = "^";
	
	public static final String UNDERSCORE_SEPERATOR = "_";
	
	public static final String LOGICAL_CABIN_CLASS_APPENDER = "^";

	public static final String CABIN_CLASS_LOGICAL_CABIN_CLASS_SEPERATOR = "_";
	
	public enum ReservationFlow {CREATE, MODIFY};

}
