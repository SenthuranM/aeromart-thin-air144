package com.isa.thinair.webplatform.api.dto;

import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class PAXSummeryDTO {

	private Integer pnrPaxId;

	private String paxName;

	private BigDecimal currentCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentPayments = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentCredit = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newFare = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newCarryForwards = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newBalance = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal totalCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String paxType;

	private Integer infantId;

	public Integer getPNRPaxId() {
		return pnrPaxId;
	}

	public void setPNRPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	public String getPaxName() {
		return paxName;
	}

	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	public BigDecimal getCurrentCharges() {
		return currentCharges;
	}

	public void setCurrentCharges(BigDecimal currentCharges) {
		this.currentCharges = currentCharges;
	}

	public BigDecimal getCurrentPayments() {
		return currentPayments;
	}

	public void setCurrentPayments(BigDecimal currentPayments) {
		this.currentPayments = currentPayments;
	}

	public BigDecimal getNewCarryForwards() {
		return newCarryForwards;
	}

	public void setNewCarryForwards(BigDecimal newCarryForwards) {
		this.newCarryForwards = newCarryForwards;
	}

	public BigDecimal getNewCharge() {
		return newCharge;
	}

	public void setNewCharge(BigDecimal newCharges) {
		this.newCharge = newCharges;
	}

	public BigDecimal getNewBalance() {
		return newBalance;
	}

	public void setNewBalance(BigDecimal newBalance) {
		this.newBalance = newBalance;
	}

	public BigDecimal getNewFare() {
		return newFare;
	}

	public void setNewFare(BigDecimal newFare) {
		this.newFare = newFare;
	}

	public BigDecimal getNewTotalCharges() {
		return totalCharges;

	}

	public void setNewTotalCharges(BigDecimal newTCharge) {
		this.totalCharges = newTCharge;

	}

	public BigDecimal getCurrentCredit() {
		return currentCredit;
	}

	public void setCurrentCredit(BigDecimal currentCredit) {
		this.currentCredit = currentCredit;
	}

	/**
	 * @return Returns the paxType.
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            The paxType to set.
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return Returns the infantId.
	 */
	public Integer getInfantId() {
		return infantId;
	}

	/**
	 * @param infantId
	 *            The infantId to set.
	 */
	public void setInfantId(Integer infantId) {
		this.infantId = infantId;
	}

}
