package com.isa.thinair.webplatform.api.v2.reservation;

import java.util.List;

public class SurchargeFETO {

	private String surchargeCode;
	private String amount;
	private String carrierCode;
	private String segmentCode;
	private List<String> applicablePassengerTypeCode;
	private String surchargeName;
	private String applicableToDisplay;
	private String amountInSelectedCurrency;
	private String amountInLocalCurrency;
	private String localCurrencyCode;
	private String reportingChargeGroupCode;

	/**
	 * @return the surchargeCode
	 */
	public String getSurchargeCode() {
		return surchargeCode;
	}

	/**
	 * @param surchargeCode
	 *            the surchargeCode to set
	 */
	public void setSurchargeCode(String surchargeCode) {
		this.surchargeCode = surchargeCode;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the carrierCode
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            the carrierCode to set
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the segmentCode
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            the segmentCode to set
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return the applicablePassengerTypeCode
	 */
	public List<String> getApplicablePassengerTypeCode() {
		return applicablePassengerTypeCode;
	}

	/**
	 * @param applicablePassengerTypeCode
	 *            the applicablePassengerTypeCode to set
	 */
	public void setApplicablePassengerTypeCode(List<String> applicablePassengerTypeCode) {
		this.applicablePassengerTypeCode = applicablePassengerTypeCode;
	}

	/**
	 * @return the surchargeName
	 */
	public String getSurchargeName() {
		return surchargeName;
	}

	/**
	 * @param surchargeName
	 *            the surchargeName to set
	 */
	public void setSurchargeName(String surchargeName) {
		this.surchargeName = surchargeName;
	}

	/**
	 * @return the applicableToDisplay
	 */
	public String getApplicableToDisplay() {
		return applicableToDisplay;
	}

	/**
	 * @param applicableToDisplay
	 *            the applicableToDisplay to set
	 */
	public void setApplicableToDisplay(String applicableToDisplay) {
		this.applicableToDisplay = applicableToDisplay;
	}

	/**
	 * @return the surcharge amount in the selected user currency.
	 */
	public String getAmountInSelectedCurrency() {
		return amountInSelectedCurrency;
	}

	/**
	 * @param amountInSelectedCurrency
	 *            : The surcharge amount in the selected user currency.
	 */
	public void setAmountInSelectedCurrency(String amountInSelectedCurrency) {
		this.amountInSelectedCurrency = amountInSelectedCurrency;
	}

	public String getAmountInLocalCurrency() {
		return amountInLocalCurrency;
	}

	public void setAmountInLocalCurrency(String amountInLocalCurrency) {
		this.amountInLocalCurrency = amountInLocalCurrency;
	}

	public String getLocalCurrencyCode() {
		return localCurrencyCode;
	}

	public void setLocalCurrencyCode(String localCurrencyCode) {
		this.localCurrencyCode = localCurrencyCode;
	}

	/**
	 * @return the reportingChargeGroupCode
	 */
	public String getReportingChargeGroupCode() {
		return reportingChargeGroupCode;
	}

	/**
	 * @param reportingChargeGroupCode the reportingChargeGroupCode to set
	 */
	public void setReportingChargeGroupCode(String reportingChargeGroupCode) {
		this.reportingChargeGroupCode = reportingChargeGroupCode;
	}

}