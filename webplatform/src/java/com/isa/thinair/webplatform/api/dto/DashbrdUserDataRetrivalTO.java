package com.isa.thinair.webplatform.api.dto;

import java.io.Serializable;

/**
 * User
 * 
 * @author Navod Ediriweera
 * 
 */
public class DashbrdUserDataRetrivalTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1780257077878665355L;
	private String userID;
	private String agentCode;
	private String agentType;
	private String posCode;

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentType() {
		return agentType;
	}

	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}

	public String getPosCode() {
		return posCode;
	}

	public void setPosCode(String posCode) {
		this.posCode = posCode;
	}

}
