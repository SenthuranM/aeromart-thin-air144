package com.isa.thinair.webplatform.api.v2.modify;

public class ModifcationParamTypes {

	public static final String ADD_SEAT = "addSeat";
	
	public static final String EDIT_SEAT = "editSeat";

	public static final String ADD_MEAL = "addMeal";
	
	public static final String EDIT_MEAL = "editMeal";

	public static final String ADD_BAGGAGE = "addBaggage";
	
	public static final String EDIT_BAGGAGE = "editBaggage";

	public static final String ADD_AIRPORT_SERVICE = "addAirportService";
	
	public static final String EDIT_AIRPORT_SERVICE = "editAirPortSearvice";
	
	public static final String ADD_AIRPORT_TRANSFER = "addAirportTransfer";

	public static final String ADD_INSURANCE = "addInsurance";
	
	public static final String EDIT_INSURANCE = "editInsurance";

	public static final String CANCEL_ALLOW = "cancelAllow";

	public static final String ALLOW_MODIFY_DATE = "allowModifyByDate";

	public static final String ALLOW_MODIFY_ROUTE = "allowModifyByRoute";

	public static final String ALLOW_MODIFY_PAX_DETAIL = "allowModifyPaxDetail";

	public static final String BUFFER_TIME_MODIFICATION = "buffertimeModification";

	public static final String ALLOW_MODIFY_CNX_RESERVATION = "allowModifyCNXRes";

	public static final String ALLOW_MODIFY_FLOWN = "allowModifyFlown";

	public static final String ADD_SEGMENT = "addSegment";

	public static final String ADD_BUS_SEGMENT = "addBusSegment";

	public static final String CANCEL_SEGMENT = "cancelSegment";

	public static final String TRANSFER_SEGMENT = "transferSegment";

	public static final String SPLIT_RESERVATION = "splitReservation";
	
	public static final String SPLIT_BULK_RESERVATION = "splitBulkReservation";

	public static final String REMOVE_PAX = "removePax";
	
	public static final String BULK_TKT_REMOVE_PAX = "removeBulkTicketPax";

	public static final String ADD_INFANT = "addInfant";

	public static final String TRANSFER_OWNERSHIP = "transferOwnership";

	public static final String TRANSFER_OWNERSHIP_TO_ANY_CARRIER = "transferOwnershipToAnyCarrier";

	public static final String VIEW_CNX_ITEN = "viewCnxIten";

	public static final String PRIVI_ONHOLD = "onhold";

	public static final String EXTEND_ONHOLD = "extendOnhold";

	public static final String ALLOW_MODFIFY_BUFFERTIME_SEG_OR_PAX = "modifyBuffertimeSegPax";

	public static final String ADD_USER_NOTES = "addUserNotes";

	public static final String VIEW_ITN_WITH_PAYMENT_INFO = "viewItineraryWithPaymentInfo";

	public static final String VIEW_ITN_WITHOUT_PAX_CHARGES = "viewItineraryWithoutPaxCharges";

	public static final String VIEW_ITN_WITH_CHARGES = "viewItineraryWithCharges";

	public static final String ALLOW_NAME_CHANGE = "allowNameChange";
	
	public static final String ONHOLD_NAME_CHANGE = "onholdNameChange";

	public static final String ALLOW_TBA_NAME_CHANGE = "allowTBANameChange";

	public static final String ALLOW_BUFFERTIME_NAME_CHANGE = "allowBufferTimeNameChange";

	public static final String ALLOW_MODIFY_CONTACT_DETAILS = "allowModifyContactDetails";

	public static final String VIEW_RESERVATION_HISTORY = "viewReservationHistory";

	public static final String VIEW_RESERVATION_HISTORY_OTHER_CARRIERS = "viewReservationHistoryOtherCarriers";
	
	public static final String ADD_USR_NOTE = "allowAddUNote";
	
	public static final String CLASSIFY_USR_NOTE = "allowClassifyUNote";

	public static final String ALLOW_OVERRIDE_REQUOTE_NAME_CHANGE = "allowOverrideNameChange";

	/* Payment Related Info */
	public static final String PARTIAL_PAYMENT_MODIFY = "partialPaymentModify";
	
	public static final String MODIFY_ONHOLD_CONFIM = "modifyOnholdConfirm";

	public static final String CASH_PAYMENTS = "cashPayments";

	public static final String CREDIT_CARD_PAYMENTS = "creditCardPayments";

	public static final String VOUCHER_PAYMENTS = "voucherPayment";

	public static final String ON_ACCOUNT_PAYMENTS = "onAccountPayments";
	
	public static final String OFFLINE_PAYMENTS = "offlinePayments";

	public static final String MULTIPLE_AGENT_ON_ACCOUNT_PAYMENTS = "multipleAgentOnAccountPayments";

	public static final String BSP_PAYMENTS = "bspPayments";

	public static final String ON_ACCOUNT_PAYMENTS_ANY = "onAccountPaymentsAny";

	public static final String ON_ACCOUNT_PAYMENTS_REPORTING = "onAccountPaymentsReporting";

	public static final String FORCE_CONFIRM = "forceConfirm";

	public static final String LOAD_PROFILE = "loadProfile";

	public static final String ALLOW_CAPTURE_PAY_REFERENCE = "allowCapturePayReference";

	public static final String ALLOW_NAMED_CREDIT_TRANSFER = "allowNamedCreditTransfer";

	public static final String ALLOW_ANY_CREDIT_TRANSFER = "allowAnyCreditTransfer";
	/* Payment Related Info */

	/* charges related info */
	public static final String ADJUST_CHARGES = "adjustCharges";

	public static final String REFUNDABLE_CHARGE_ADJUST = "refundableChargeAdjust";

	public static final String MULTIPLE_CHARGE_ADJUSTMENT = "groupChargeAdjustment";

	public static final String ALLOW_PAX_CREDIT_EXP_DATE_CHANGE = "paxCreditExpDateChange";

	public static final String ALLOW_REINSTATE_PAX_CREDITS = "allowReinstatePaxCrdits";

	public static final String ADJUST_OTHER_CARRIER_CHARGES = "adjustOtherCarrierCharges";

	public static final String NON_REFFUNDABLE_CHARGE_ADJUST = "nonRefundableChargeAdjust";

	public static final String ALLOW_REFUND_NO_CREDIT = "allowRefundNoCredit";

	public static final String ALLOW_CASH_REFUND = "allowCashRefund";

	public static final String ALLOW_ON_ACCOUNT_REFUND = "allowOnAccountRefund";

	public static final String ALLOW_BSP_REFUND = "allowBSPRefund";

	public static final String ALLOW_ON_ACCOUNT_RPT_REFUND = "allowOnAccountRptRefund";

	public static final String ALLOW_ANY_CARRIER_REFUND = "allowAnyCarrierRefund";

	public static final String ALLOW_ANY_OPCARRIER_ON_ACC_REFUND = "allowAnyOpCarrieOnAccRefund";

	public static final String ALLOW_CREDIT_CARD_REFUND = "allowCreditCardRefund";

	public static final String ALLOW_ANY_CREDIT_CARD_REFUND = "allowAnyCreditCardRefund";

	public static final String VIEW_EXTERNAL_PAYMENTS = "viewExternalPayments";

	public static final String VIEW_CREDIT = "viewCredit";

	public static final String ALLOW_REFUND = "allowRefund";

	public static final String HAS_OVERRIDE_CNX_MOD_CHARGE = "hasOverrideCnxModChargePrivilege";

	public static final String SHOW_CHARGE_TYPE_OVERRIDE = "blnShowChargeTypeOveride";
	/* charges related info */

	public static final String BUFFERTIME_MOD_ACCOUNT_ALLOWED = "bufferTimeModAccountAllowed";

	public static final String ALLOW_LCC_ANY_OND_MODIFY = "allowLCCAnyONDModify";

	public static final String PRINT_ITN_FOR_PARTIAL_PAYMENTS = "printItineraryForPartialPayments";

	public static final String CLEAR_ALERT = "clearAlert";

	public static final String TRANSFER_ALERT = "transferAlert";

	public static final String GROUND_SERVICE_ENABLED = "groundServiceEnabled";

	/* create reservation params */

	public static final String CHANGE_FARE_ANY = "changeFareAny";

	public static final String OVERRIDE_AGENT_FARE_ONLY = "overrideAgentFare";
	
	public static final String VIEW_PUBLIC_ONEWAY_FOR_RETURNS = "viewPublicOnewayForReturns";

	public static final String OVERRIDE_VISIBLE_FARE = "overrideVisibleFare";

	public static final String ALLOW_ON_HOLD = "allowOnHold";

	public static final String ALLOW_ON_HOLD_BUFFER_TIME = "allowOnHoldBufferTime";

	public static final String ALLOW_BLOCK_SEATS = "allowBlockSeats";

	public static final String MULTI_MEAL_ENABLED = "multiMealEnabled";

	public static final String ENABLE_TRANSACTION_GRANULARITY_TRACKING = "enableTransactionGranularityTracking";

	public static final String BAGGAGE_MANDATORY_ENABLED = "baggageMandatory";

	public static final String CHANGE_PASSENGER_COUPON_STATUS = "allowChangePassengerCouponStatus";
	
	public static final String ALLOW_EXCEPTIONAL_ETICKET_COUPON_MODIFICATIONS = "allowExceptionalEticketCouponModifications";

	public static final String OVERRIDE_SAME_FARE_MODIFICATION = "overrideSameFareModification";

	public static final String ALLOW_OVERRIDE_SAME_OR_HIGHER_FARE = "allowOverrideSameOrHigherFare";

	public static final String ALLOW_CSV_DETAIL_UPLOAD = "allowCSVDetailUpload";

	public static final String MODIFY_EXTENDED_NAME_WITHIN_BUFFER = "modifyExtendedNameWithinBuffer";
	
	public static final String MODIFY_PAX_NAME_WITHIN_THRESHOLD_TIME = "modifyPaxNameWithinThresholdTime";

	public static final String MODIFY_CREDIT_UTILIZED_PAX_NAME = "modifyCreditUtilizedPaxName";

	public static final String MODIFY_PAX_NAME_HAVING_DEPARTED_SEGMENTS = "modifyPaxNameHavingDepatedSegments";

	public static final String MODIFY_USER_NOTE_HAVING_DEPARTED_SEGMENTS = "modifyUserNoteHavingDepatedSegments";

	public static final String MODIFY_EXTENDED_NAME = "modifyExtendedName";
	
	public static final String MODIFY_PAX_NAME_WITHIN_NAME_CHANGE_CUTOFF = "modifyPaxNameWithinCutoff";

	public static final String ALLOW_CREATE_USER_ALERT = "allowCreateUserAlert";
	
	public static final String ALLOW_MULTICITY_SEARCH = "allowMultiCitySearch";	
	
	public static final String RESTRICT_MODIFICATION_CNX_FLOWN_BOOKINGS = "restrictModificationCnxFlown";

	public static final String ADD_SSR = "addSSR";

	public static final String EDIT_SSR = "editSSR";

	public static final String FARE_RULE_NCC_ENABLED = "enableFareRuleNCC";
	
	public static final String FARE_RULE_NCC_FOR_FLOWN_ENABLED = "enableFareRuleNCCForFlown";
	
	public static final String DUPLICATE_NAME_CHECK_ENABLED = "duplicateNameCheckEnabled";
	
	public static final String DUPLICATE_NAME_SKIP = "duplicateNameSkipPrivilege";
	
	public static final String DUPLICATE_NAME_SKIP_MODIFY = "duplicateNameSkipModifyPrivilege";

	public static final String ENABLE_BLACKLIST_PAX ="enableBlacklistPax";

}
