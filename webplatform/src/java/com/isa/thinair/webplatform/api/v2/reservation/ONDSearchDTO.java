package com.isa.thinair.webplatform.api.v2.reservation;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;

public class ONDSearchDTO {
	private String fromAirport = null;

	private String toAirport = null;

	private String classOfService = null;

	private String logicalCabinClass = null;

	private String bookingClass = null;

	private String bookingType = null;

	private Date departureDate = null;

	private Integer departureVariance = 0;

	private List<String> flightRPHList;

	private List<String> existingFlightRPHList;

	private List<String> existingResSegRPHList;

	private boolean flownOnd = false;

	private BigDecimal oldPerPaxFare = BigDecimal.ZERO;

	private List<String> modifiedResSegList;

	private List<String> dateChangedResSegList;

	private boolean eligibleToSameBCMod = false;

	private List<FareTO> oldPerPaxFareTOList = null;

	private String status = null;

	private boolean sameFlightModification;

	private String lastFareQuoteDateZulu;

	private Integer bundledServicePeriodId;
	
	private boolean fromCitySearch = false;

	private boolean toCitySearch = false;

	public String getFromAirport() {
		return fromAirport;
	}

	public void setFromAirport(String fromAirport) {
		this.fromAirport = fromAirport;
	}

	public String getToAirport() {
		return toAirport;
	}

	public void setToAirport(String toAirport) {
		this.toAirport = toAirport;
	}

	public String getClassOfService() {
		return classOfService;
	}

	public void setClassOfService(String classOfService) {
		this.classOfService = classOfService;
	}

	public String getLogicalCabinClass() {
		return logicalCabinClass;
	}

	public void setLogicalCabinClass(String logicalCabinClass) {
		this.logicalCabinClass = logicalCabinClass;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public String getBookingType() {
		return bookingType;
	}

	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public Integer getDepartureVariance() {
		return departureVariance;
	}

	public List<String> getFlightRPHList() {
		return flightRPHList;
	}

	public void setFlightRPHList(List<String> flightRPHList) {
		this.flightRPHList = flightRPHList;
	}

	public void setDepartureVariance(Integer departureVariance) {
		this.departureVariance = departureVariance;
	}

	public List<String> getExistingFlightRPHList() {
		return existingFlightRPHList;
	}

	public void setExistingFlightRPHList(List<String> existingFlightRPHList) {
		this.existingFlightRPHList = existingFlightRPHList;
	}

	public boolean isFlownOnd() {
		return flownOnd;
	}

	public void setFlownOnd(boolean flownOnd) {
		this.flownOnd = flownOnd;
	}

	public List<String> getExistingResSegRPHList() {
		return existingResSegRPHList;
	}

	public void setExistingResSegRPHList(List<String> existingPnrSegRPHList) {
		this.existingResSegRPHList = existingPnrSegRPHList;
	}

	public BigDecimal getOldPerPaxFare() {
		return oldPerPaxFare;
	}

	public void setOldPerPaxFare(BigDecimal oldPerPaxFare) {
		this.oldPerPaxFare = oldPerPaxFare;
	}

	public List<String> getModifiedResSegList() {
		return modifiedResSegList;
	}

	public void setModifiedResSegList(List<String> modifiedResSegList) {
		this.modifiedResSegList = modifiedResSegList;
	}

	public List<String> getDateChangedResSegList() {
		return dateChangedResSegList;
	}

	public void setDateChangedResSegList(List<String> dateChangedResSegList) {
		this.dateChangedResSegList = dateChangedResSegList;
	}

	public boolean isEligibleToSameBCMod() {
		return eligibleToSameBCMod;
	}

	public void setEligibleToSameBCMod(boolean eligibleToSameBCMod) {
		this.eligibleToSameBCMod = eligibleToSameBCMod;
	}

	public List<FareTO> getOldPerPaxFareTOList() {
		return oldPerPaxFareTOList;
	}

	public void setOldPerPaxFareTOList(List<FareTO> oldPerPaxFareTOList) {
		this.oldPerPaxFareTOList = oldPerPaxFareTOList;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isSameFlightModification() {
		return sameFlightModification;
	}

	public void setSameFlightModification(boolean sameFlightModification) {
		this.sameFlightModification = sameFlightModification;
	}

	public String getLastFareQuoteDateZulu() {
		return lastFareQuoteDateZulu;
	}

	public void setLastFareQuoteDateZulu(String lastFareQuoteDateZulu) {
		this.lastFareQuoteDateZulu = lastFareQuoteDateZulu;
	}

	public Integer getBundledServicePeriodId() {
		return bundledServicePeriodId;
	}

	public void setBundledServicePeriodId(Integer bundledServicePeriodId) {
		this.bundledServicePeriodId = bundledServicePeriodId;
	}

	public boolean isFromCitySearch() {
		return fromCitySearch;
	}

	public void setFromCitySearch(boolean fromCitySearch) {
		this.fromCitySearch = fromCitySearch;
	}

	public boolean isToCitySearch() {
		return toCitySearch;
	}

	public void setToCitySearch(boolean toCitySearch) {
		this.toCitySearch = toCitySearch;
	}

}
