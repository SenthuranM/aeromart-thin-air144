package com.isa.thinair.webplatform.api.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteCriteriaForTktDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.webplatform.api.util.Constants.ReservationFlow;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

/**
 * Utility class for Service Tax Calculation Response Adaptation
 * 
 */
public class ServiceTaxCalculatorUtil {

	private static Log log = LogFactory.getLog(ServiceTaxCalculatorUtil.class);

	public static BigDecimal getTotalServiceTaxAmount(Collection<ReservationPaxTO> paxList,
			List<PassengerTypeQuantityTO> paxQtyList,
			Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteForTktRevResTOMap,
			ApplicationEngine appIndicator) {

		BigDecimal totalServiceTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (serviceTaxQuoteForTktRevResTOMap != null && !serviceTaxQuoteForTktRevResTOMap.isEmpty()) {

			for (String carrierCode : serviceTaxQuoteForTktRevResTOMap.keySet()) {
				ServiceTaxQuoteForTicketingRevenueResTO serviceTaxQuoteRS = serviceTaxQuoteForTktRevResTOMap.get(carrierCode);

				if (paxList != null && !paxList.isEmpty()) {
					for (ReservationPaxTO pax : paxList) {

						String paxType = pax.getPaxType();
						Integer paxSequence = pax.getSeqNumber();

						if (serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes() != null
								&& !serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes().isEmpty()) {
							List<ServiceTaxTO> serviceTaxs = serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes().get(paxType);

							totalServiceTaxAmount = AccelAeroCalculator.add(totalServiceTaxAmount, getTotal(serviceTaxs));

						}

						if (serviceTaxQuoteRS.getPaxWiseServiceTaxes() != null
								&& !serviceTaxQuoteRS.getPaxWiseServiceTaxes().isEmpty()) {
							List<ServiceTaxTO> serviceTaxs = serviceTaxQuoteRS.getPaxWiseServiceTaxes().get(paxSequence);
							totalServiceTaxAmount = AccelAeroCalculator.add(totalServiceTaxAmount, getTotal(serviceTaxs));

						}

					}
				} else if (paxQtyList != null && !paxQtyList.isEmpty()) {
					for (PassengerTypeQuantityTO paxQtyTO : paxQtyList) {
						String paxType = paxQtyTO.getPassengerType();
						if (serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes() != null
								&& !serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes().isEmpty()) {
							List<ServiceTaxTO> serviceTaxs = serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes().get(paxType);
							BigDecimal totalForPaxType = AccelAeroCalculator.multiply(getTotal(serviceTaxs),
									paxQtyTO.getQuantity());
							totalServiceTaxAmount = AccelAeroCalculator.add(totalServiceTaxAmount, totalForPaxType);

						}
					}
				}
			}

		}
		return totalServiceTaxAmount;
	}

	private static BigDecimal getTotal(List<ServiceTaxTO> serviceTaxs) {
		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (serviceTaxs != null) {
			for (ServiceTaxTO taxTO : serviceTaxs) {
				total = AccelAeroCalculator.add(total, taxTO.getAmount());
			}
		}

		return total;
	}

	public static BigDecimal addPaxWiseApplicableServiceTax(Collection<ReservationPaxTO> paxList,
			Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteForTktRevResTOMap, boolean isServiceTaxForCCFee,
			ApplicationEngine appIndicator, boolean skipPaxServiceTaxUpdate) {

		BigDecimal totalServiceTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (serviceTaxQuoteForTktRevResTOMap != null && !serviceTaxQuoteForTktRevResTOMap.isEmpty() && paxList != null
				&& !paxList.isEmpty()) {
			boolean considerInfantChargesSeparately = hasInfant(paxList);

			for (String carrierCode : serviceTaxQuoteForTktRevResTOMap.keySet()) {
				ServiceTaxQuoteForTicketingRevenueResTO serviceTaxQuoteRS = serviceTaxQuoteForTktRevResTOMap.get(carrierCode);
				int[] payablePaxCount = getPayablePaxCount(paxList, null);
				String taxDepositCountryCode = serviceTaxQuoteRS.getServiceTaxDepositeCountryCode();
				String taxDepositStateCode = serviceTaxQuoteRS.getServiceTaxDepositeStateCode();
				log.info("Sevice Tax Calculation for Carrier - " + carrierCode);
				for (ReservationPaxTO pax : paxList) {

					List<LCCClientExternalChgDTO> chgDTOs = new ArrayList<>();
					List<LCCClientExternalChgDTO> infantChgDTOs = new ArrayList<>();
					String paxType = pax.getPaxType();
					Integer paxSequence = pax.getSeqNumber();
					boolean isParent = pax.getIsParent() != null ? pax.getIsParent() : false;

					if (PaxTypeTO.INFANT.equals(paxType) && !considerInfantChargesSeparately) {
						continue;
					}

					if (serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes() != null
							&& !serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes().isEmpty()) {
						List<ServiceTaxTO> serviceTaxs = serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes().get(paxType);
						addPaxServiceTaxesAsExtCharges(serviceTaxs, chgDTOs, taxDepositCountryCode, taxDepositStateCode,
								isServiceTaxForCCFee);
						printServiceTaxChargesWithTotal(serviceTaxs, " PAX-Type " + paxType.toUpperCase());
						if (isParent && !considerInfantChargesSeparately) {
							List<ServiceTaxTO> infantServiceTaxs = serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes()
									.get(PaxTypeTO.INFANT);
							addPaxServiceTaxesAsExtCharges(infantServiceTaxs, infantChgDTOs, taxDepositCountryCode,
									taxDepositStateCode, isServiceTaxForCCFee);
							printServiceTaxChargesWithTotal(infantServiceTaxs, " PAX-Type " + PaxTypeTO.INFANT);
						}

					}

					if (serviceTaxQuoteRS.getPaxWiseServiceTaxes() != null
							&& !serviceTaxQuoteRS.getPaxWiseServiceTaxes().isEmpty()) {
						List<ServiceTaxTO> serviceTaxs = serviceTaxQuoteRS.getPaxWiseServiceTaxes().get(paxSequence);
						addPaxServiceTaxesAsExtCharges(serviceTaxs, chgDTOs, taxDepositCountryCode, taxDepositStateCode,
								isServiceTaxForCCFee);
						printServiceTaxChargesWithTotal(serviceTaxs, " PAX-Sequence " + paxSequence);

						if (isParent && !considerInfantChargesSeparately) {
							int infantSequence;
							/*
							 * FIXME infant sequence is varying in different flows. Add common method to get correct
							 * infant sequence.
							 */
							if (pax.getInfantWith() != null) {
								infantSequence = payablePaxCount[0] + pax.getInfantWith();
							} else {
								infantSequence = getInfantSequence(pax, paxList);
							}
							List<ServiceTaxTO> infantServiceTaxs = serviceTaxQuoteRS.getPaxWiseServiceTaxes().get(infantSequence);
							addPaxServiceTaxesAsExtCharges(infantServiceTaxs, infantChgDTOs, taxDepositCountryCode,
									taxDepositStateCode, isServiceTaxForCCFee);
							printServiceTaxChargesWithTotal(infantServiceTaxs, " PAX-Sequence " + infantSequence);
						}

					}

					if (isServiceTaxForCCFee && !skipPaxServiceTaxUpdate) {
						pax.removeServiceTaxAppliedToCCFee();
					}

					if (chgDTOs != null && !chgDTOs.isEmpty()) {
						List<LCCClientExternalChgDTO> groupedChargeDTOs = groupServiceTaxByChargeCode(chgDTOs);
						if (groupedChargeDTOs != null && !groupedChargeDTOs.isEmpty()) {
							totalServiceTaxAmount = AccelAeroCalculator.add(totalServiceTaxAmount,
									getTotalServiceTaxAmount(groupedChargeDTOs));
							if (!skipPaxServiceTaxUpdate) {
								pax.addExternalCharges(groupedChargeDTOs);
							}
						}

					}

					if (isParent) {
						if (infantChgDTOs != null && !infantChgDTOs.isEmpty()) {
							List<LCCClientExternalChgDTO> groupedChargeDTOs = groupServiceTaxByChargeCode(infantChgDTOs);
							if (groupedChargeDTOs != null && !groupedChargeDTOs.isEmpty()) {
								totalServiceTaxAmount = AccelAeroCalculator.add(totalServiceTaxAmount,
										getTotalServiceTaxAmount(groupedChargeDTOs));
								if (!skipPaxServiceTaxUpdate) {
									pax.addInfantExternalCharges(groupedChargeDTOs);
								}
							}
						}
					}

				}

			}

		}
		return totalServiceTaxAmount;
	}

	private static boolean hasInfant(Collection<ReservationPaxTO> paxList) {
		for (ReservationPaxTO x : paxList) {
			if (PaxTypeTO.INFANT.equals(x.getPaxType())) {
				return true;
			}
		}
		return false;
	}

	public static List<LCCClientExternalChgDTO> groupServiceTax(
			Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteForTktRevResTOMap, String paxType,
			Integer paxSequence) {
		List<LCCClientExternalChgDTO> chgDTOs = new ArrayList<>();
		if (serviceTaxQuoteForTktRevResTOMap != null && !serviceTaxQuoteForTktRevResTOMap.isEmpty()) {
			for (String carrierCode : serviceTaxQuoteForTktRevResTOMap.keySet()) {
				ServiceTaxQuoteForTicketingRevenueResTO serviceTaxQuoteRS = serviceTaxQuoteForTktRevResTOMap.get(carrierCode);
				String taxDepositCountryCode = serviceTaxQuoteRS.getServiceTaxDepositeCountryCode();
				String taxDepositStateCode = serviceTaxQuoteRS.getServiceTaxDepositeStateCode();
				if (serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes() != null
						&& !serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes().isEmpty()) {
					List<ServiceTaxTO> serviceTaxs = serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes().get(paxType);
					addPaxServiceTaxesAsExtCharges(serviceTaxs, chgDTOs, taxDepositCountryCode, taxDepositStateCode, false);
				}
				if (serviceTaxQuoteRS.getPaxWiseServiceTaxes() != null && !serviceTaxQuoteRS.getPaxWiseServiceTaxes().isEmpty()) {
					List<ServiceTaxTO> serviceTaxs = serviceTaxQuoteRS.getPaxWiseServiceTaxes().get(paxSequence);
					addPaxServiceTaxesAsExtCharges(serviceTaxs, chgDTOs, taxDepositCountryCode, taxDepositStateCode, false);
				}
				if (chgDTOs != null && !chgDTOs.isEmpty()) {
					chgDTOs = groupServiceTaxByChargeCode(chgDTOs);
				}
			}
		}
		return chgDTOs;
	}

	private static LCCClientExternalChgDTO externalChargeAdaptor(ServiceTaxTO serviceTax, boolean isTicketingRevenue,
			String taxDepositCountryCode, String taxDepositStateCode, boolean isServiceTaxAppliedToCCFee) {

		LCCClientExternalChgDTO chg = new LCCClientExternalChgDTO();
		chg.setAmount(serviceTax.getAmount());
		chg.setExternalCharges(EXTERNAL_CHARGES.SERVICE_TAX);
		chg.setCode(serviceTax.getChargeCode());
		chg.setFlightRefNumber(serviceTax.getFlightRefNumber());
		chg.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(serviceTax.getFlightRefNumber()));
		chg.setTaxableAmount(serviceTax.getTaxableAmount());
		chg.setNonTaxableAmount(serviceTax.getNonTaxableAmount());
		chg.setTicketingRevenue(isTicketingRevenue);
		chg.setTaxDepositCountryCode(taxDepositCountryCode);
		chg.setTaxDepositStateCode(taxDepositStateCode);
		chg.setServiceTaxAppliedToCCFee(isServiceTaxAppliedToCCFee);

		return chg;

	}

	public static List<LCCClientExternalChgDTO> groupServiceTaxByChargeCode(List<LCCClientExternalChgDTO> chgDTOs) {
		List<LCCClientExternalChgDTO> groupChgDTOs = new ArrayList<>();
		if (chgDTOs != null && !chgDTOs.isEmpty()) {
			Map<String, Map<String, List<LCCClientExternalChgDTO>>> chargesFltWiseByChargeCodeMap = new HashMap<>();
			for (LCCClientExternalChgDTO chgDTO : chgDTOs) {
				String flightRef = chgDTO.getFlightRefNumber();
				String chargeCode = chgDTO.getCode();

				if (chargesFltWiseByChargeCodeMap.get(flightRef) == null) {
					chargesFltWiseByChargeCodeMap.put(flightRef, new HashMap<String, List<LCCClientExternalChgDTO>>());
				}

				if (chargesFltWiseByChargeCodeMap.get(flightRef).get(chargeCode) == null) {
					List<LCCClientExternalChgDTO> chargeGrpList = new ArrayList<>();
					chargeGrpList.add(chgDTO);
					chargesFltWiseByChargeCodeMap.get(flightRef).put(chargeCode, chargeGrpList);
				} else {
					List<LCCClientExternalChgDTO> chargeGrpList = chargesFltWiseByChargeCodeMap.get(flightRef).get(chargeCode);

					if (chargeGrpList == null || chargeGrpList.isEmpty()) {
						chargeGrpList = new ArrayList<>();
						chargeGrpList.add(chgDTO);
						chargesFltWiseByChargeCodeMap.get(flightRef).put(chargeCode, chargeGrpList);
					} else {
						for (LCCClientExternalChgDTO existChargeDTO : chargeGrpList) {
							if ((existChargeDTO.isTicketingRevenue() && chgDTO.isTicketingRevenue())
									|| (!existChargeDTO.isTicketingRevenue() && !chgDTO.isTicketingRevenue())) {
								existChargeDTO.setAmount(AccelAeroCalculator.add(existChargeDTO.getAmount(), chgDTO.getAmount()));
								existChargeDTO.setTaxableAmount(
										AccelAeroCalculator.add(existChargeDTO.getTaxableAmount(), chgDTO.getTaxableAmount()));
								existChargeDTO.setNonTaxableAmount(AccelAeroCalculator.add(existChargeDTO.getNonTaxableAmount(),
										chgDTO.getNonTaxableAmount()));
							} else {
								chargesFltWiseByChargeCodeMap.get(flightRef).get(chargeCode).add(chgDTO);
							}

						}
					}

				}

			}

			if (chargesFltWiseByChargeCodeMap != null && !chargesFltWiseByChargeCodeMap.isEmpty()) {
				for (String key : chargesFltWiseByChargeCodeMap.keySet()) {
					Map<String, List<LCCClientExternalChgDTO>> chargesByChargeCodeMap = chargesFltWiseByChargeCodeMap.get(key);
					if (chargesByChargeCodeMap != null && !chargesByChargeCodeMap.isEmpty()) {
						for (String chargeCode : chargesByChargeCodeMap.keySet()) {
							List<LCCClientExternalChgDTO> chargeGrpList = chargesByChargeCodeMap.get(chargeCode);
							if (chargeGrpList != null && !chargeGrpList.isEmpty()) {
								groupChgDTOs.addAll(chargeGrpList);
							}
						}
					}

				}
			}

		}

		return groupChgDTOs;

	}

	public static void addPaxServiceTaxesAsExtCharges(List<ServiceTaxTO> serviceTaxs, List<LCCClientExternalChgDTO> chgDTOs,
			String taxDepositCountryCode, String taxDepositStateCode, boolean isServiceTaxAppliedToCCFee) {
		if (chgDTOs != null && serviceTaxs != null && !serviceTaxs.isEmpty()) {
			for (ServiceTaxTO serviceTax : serviceTaxs) {
				chgDTOs.add(externalChargeAdaptor(serviceTax, true, taxDepositCountryCode, taxDepositStateCode,
						isServiceTaxAppliedToCCFee));

			}
		}
	}

	// TODO: temporary audit for verification purpose only
	public static void printServiceTaxChargesWithTotal(List<ServiceTaxTO> serviceTaxs, String description) {
		BigDecimal totalAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		int i = 1;
		if (serviceTaxs != null && !serviceTaxs.isEmpty()) {

			for (ServiceTaxTO serviceTax : serviceTaxs) {
				log.info(i + " , " + serviceTax.getChargeCode() + " , " + serviceTax.getFlightRefNumber() + " , "
						+ serviceTax.getAmount());
				totalAmount = AccelAeroCalculator.add(totalAmount, serviceTax.getAmount());
				i++;
			}

		}

		log.info("Sevice Tax Calculation summary [ " + description + " ] Entries: " + i + " Total Amount :" + totalAmount);
	}

	public static BigDecimal getTotalServiceTaxAmount(List<LCCClientExternalChgDTO> groupedChargeDTOs) {
		BigDecimal totalTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (groupedChargeDTOs != null && !groupedChargeDTOs.isEmpty()) {
			for (LCCClientExternalChgDTO groupedChargeDTO : groupedChargeDTOs) {
				totalTaxAmount = AccelAeroCalculator.add(totalTaxAmount, groupedChargeDTO.getAmount());
			}
		}
		return totalTaxAmount;
	}

	public static List<LCCClientExternalChgDTO> segmentWiseInsuranceChargeDistribution(List<FlightSegmentTO> flightSegmentTOs,
			List<LCCClientExternalChgDTO> externalCharges) {
		List<LCCClientExternalChgDTO> chgDTOs = new ArrayList<>();

		if (externalCharges != null && !externalCharges.isEmpty()) {

			Iterator<LCCClientExternalChgDTO> itr = externalCharges.iterator();
			while (itr.hasNext()) {
				LCCClientExternalChgDTO externalChgDTO = (LCCClientExternalChgDTO) itr.next();

				if (externalChgDTO != null && externalChgDTO.getAmount().doubleValue() > 0) {
					if (EXTERNAL_CHARGES.INSURANCE == externalChgDTO.getExternalCharges()) {

						List<LCCClientExternalChgDTO> charges = segmentWiseDistribution(flightSegmentTOs,
								externalChgDTO.getAmount(), externalChgDTO.getExternalCharges(), externalChgDTO.getCode(), false);

						if (charges == null || charges.isEmpty()) {
							chgDTOs.add(externalChgDTO);
						} else {
							chgDTOs.addAll(charges);
						}

					} else {
						chgDTOs.add(externalChgDTO);
					}
				}

			}
		}

		return chgDTOs;
	}

	public static List<LCCClientExternalChgDTO> segmentWiseHandlingChargeDistribution(List<FlightSegmentTO> flightSegmentTOs,
			BigDecimal paxTotalHandlingFee, ExternalChgDTO handlingChargeDTO) {
		List<LCCClientExternalChgDTO> chgDTOs = new ArrayList<>();
		if (handlingChargeDTO != null) {
			chgDTOs = segmentWiseDistribution(flightSegmentTOs, paxTotalHandlingFee, handlingChargeDTO.getExternalChargesEnum(),
					handlingChargeDTO.getChargeCode(), false);
		}

		return chgDTOs;
	}

	public static List<LCCClientExternalChgDTO> segmentWiseCreditCardChargeDistribution(List<FlightSegmentTO> flightSegmentTOs,
			BigDecimal paxTotalCreditCardFee, ExternalChgDTO creditChargeDTO, boolean isDisplayOnly) {
		List<LCCClientExternalChgDTO> chgDTOs = new ArrayList<>();
		if (creditChargeDTO != null) {
			chgDTOs = segmentWiseDistribution(flightSegmentTOs, paxTotalCreditCardFee, creditChargeDTO.getExternalChargesEnum(),
					creditChargeDTO.getChargeCode(), isDisplayOnly);
		}

		return chgDTOs;
	}

	private static List<LCCClientExternalChgDTO> segmentWiseDistribution(List<FlightSegmentTO> flightSegmentTOs,
			BigDecimal amount, EXTERNAL_CHARGES externalChargesEnum, String chargeCode, boolean isCCFeeCalculationOnly) {
		List<LCCClientExternalChgDTO> chgDTOs = new ArrayList<>();
		if (flightSegmentTOs != null && !flightSegmentTOs.isEmpty()) {
			BigDecimal[] paxSegmentWiseCardFee = AccelAeroCalculator.roundAndSplit(amount, flightSegmentTOs.size());
			int p = 0;
			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				if (paxSegmentWiseCardFee[p].doubleValue() > 0) {
					LCCClientExternalChgDTO chg = new LCCClientExternalChgDTO();
					chg.setAmount(paxSegmentWiseCardFee[p]);
					chg.setExternalCharges(externalChargesEnum);
					chg.setCode(chargeCode);
					chg.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
					chg.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(flightSegmentTO.getFlightRefNumber()));
					chgDTOs.add(chg);
				}

				p++;
			}
		} else if (isCCFeeCalculationOnly) {
			// valid only for cc fee calculation only## to display total amount only
			LCCClientExternalChgDTO chg = new LCCClientExternalChgDTO();
			chg.setAmount(amount);
			chg.setExternalCharges(externalChargesEnum);
			chg.setCode(chargeCode);
			chg.setFlightRefNumber(null);
			chg.setCarrierCode(null);
			chgDTOs.add(chg);
		}
		return chgDTOs;
	}

	// TODO: temporary audit for verification purpose only
	public static void printQuotedExternalChargesPaxWise(Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges) {

		if (paxWiseExternalCharges != null && !paxWiseExternalCharges.isEmpty()) {
			for (Integer paxSeq : paxWiseExternalCharges.keySet()) {
				List<LCCClientExternalChgDTO> externalChgDTOs = paxWiseExternalCharges.get(paxSeq);
				if (externalChgDTOs != null && !externalChgDTOs.isEmpty()) {
					log.info("Service Tax Quoted External Charges for PAX :" + paxSeq);
					for (LCCClientExternalChgDTO externalChgDTO : externalChgDTOs) {
						log.info(externalChgDTO.getExternalCharges() + " - " + externalChgDTO.getFlightRefNumber() + " - "
								+ externalChgDTO.getCode() + " : " + externalChgDTO.getAmount());
					}
				}

			}
		}

	}

	public static void addPaxExternalChargesToServiceTaxQuoteRQ(ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO,
			Collection<ReservationPaxTO> paxList, List<FlightSegmentTO> flightSegmentTOs, ExternalChgDTO ccFeeChgDTO,
			BigDecimal totalTransactionFee, ExternalChgDTO handlingChargeDTO, BigDecimal[] paxWiseHandlingFee,
			boolean isReCalculateServiceTax) throws ModuleException {

		ExternalChgDTO bspFeeChgDTO = null;
		BigDecimal bspTotalTransactionFee = AccelAeroCalculator.getDefaultBigDecimalZero();

		addPaxExternalChargesToServiceTaxQuoteRQ(serviceTaxQuoteCriteriaDTO, paxList, flightSegmentTOs, ccFeeChgDTO,
				totalTransactionFee, handlingChargeDTO, paxWiseHandlingFee, isReCalculateServiceTax, bspFeeChgDTO,
				bspTotalTransactionFee);
	}

	public static void addPaxExternalChargesToServiceTaxQuoteRQ(ServiceTaxQuoteCriteriaForTktDTO serviceTaxQuoteCriteriaDTO,
			Collection<ReservationPaxTO> paxList, List<FlightSegmentTO> flightSegmentTOs, ExternalChgDTO ccFeeChgDTO,
			BigDecimal totalTransactionFee, ExternalChgDTO handlingChargeDTO, BigDecimal[] paxWiseHandlingFee,
			boolean isReCalculateServiceTax, ExternalChgDTO bspFeeChgDTO, BigDecimal bspTotalTransactionFee)
			throws ModuleException {

		Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges = new HashMap<>();
		Map<Integer, String> paxWisePaxTypes = new HashMap<>();

		Map<Integer, List<LCCClientExternalChgDTO>> paxWiseCCExternalCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		ServiceTaxCalculatorUtil.populatePaxWiseCreditCardCharge(paxList, ccFeeChgDTO, totalTransactionFee,
				paxWiseCCExternalCharges, paxWisePaxTypes, flightSegmentTOs, null, null);

		// BSP Fee
		Map<Integer, List<LCCClientExternalChgDTO>> paxWiseBSPExternalCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		ServiceTaxCalculatorUtil.populatePaxWiseCreditCardCharge(paxList, bspFeeChgDTO, bspTotalTransactionFee,
				paxWiseBSPExternalCharges, paxWisePaxTypes, flightSegmentTOs, null, null);

		if (serviceTaxQuoteCriteriaDTO != null) {
			if (paxList != null && !paxList.isEmpty() && flightSegmentTOs != null && !flightSegmentTOs.isEmpty()) {
				int paxCount = 0;
				for (ReservationPaxTO pax : paxList) {
					if (PaxTypeTO.INFANT.equals(pax.getPaxType())) {
						continue;
					}

					if (isReCalculateServiceTax) {
						pax.removeSelectedExternalCharge(EXTERNAL_CHARGES.SERVICE_TAX);
					}

					List<LCCClientExternalChgDTO> paxExternalCharges = ServiceTaxCalculatorUtil
							.segmentWiseInsuranceChargeDistribution(flightSegmentTOs, pax.getExternalCharges());

					paxExternalCharges.addAll(ServiceTaxCalculatorUtil.segmentWiseHandlingChargeDistribution(flightSegmentTOs,
							paxWiseHandlingFee[paxCount], handlingChargeDTO));

					if (paxExternalCharges != null && paxExternalCharges.size() > 0) {
						paxWiseExternalCharges.put(pax.getSeqNumber(), paxExternalCharges);
						paxWisePaxTypes.put(pax.getSeqNumber(), pax.getPaxType());
					}

					paxCount++;
				}

				for (Integer paxSeq : paxWiseCCExternalCharges.keySet()) {
					if (paxWiseExternalCharges.get(paxSeq) != null) {
						paxWiseExternalCharges.get(paxSeq).addAll(paxWiseCCExternalCharges.get(paxSeq));
					} else {
						paxWiseExternalCharges.put(paxSeq, paxWiseCCExternalCharges.get(paxSeq));
					}
				}

				for (Integer paxSeq : paxWiseBSPExternalCharges.keySet()) {
					if (paxWiseExternalCharges.get(paxSeq) != null) {
						paxWiseExternalCharges.get(paxSeq).addAll(paxWiseBSPExternalCharges.get(paxSeq));
					} else {
						paxWiseExternalCharges.put(paxSeq, paxWiseBSPExternalCharges.get(paxSeq));
					}
				}

				// TODO: remove once the verification is done or only debug mode
				ServiceTaxCalculatorUtil.printQuotedExternalChargesPaxWise(paxWiseExternalCharges);
			}

			serviceTaxQuoteCriteriaDTO.setPaxWiseExternalCharges(paxWiseExternalCharges);
			serviceTaxQuoteCriteriaDTO.setPaxWisePaxTypes(paxWisePaxTypes);
		}

	}

	public static int[] getPayablePaxCount(Collection<ReservationPaxTO> paxList,
			Collection<LCCClientPassengerSummaryTO> passengerSummaryList) {

		int adultChildCount = 0;
		int infantCount = 0;

		if (passengerSummaryList == null || passengerSummaryList.isEmpty()) {
			for (ReservationPaxTO pax : paxList) {
				if (PaxTypeTO.INFANT.equals(pax.getPaxType())) {
					continue;
				}
				adultChildCount++;
				if (pax.getIsParent()) {
					infantCount++;
				}
			}
		} else {
			for (ReservationPaxTO pax : paxList) {

				if (PaxTypeTO.INFANT.equals(pax.getPaxType())) {
					continue;
				}

				if (pax.getIsParent()
						&& ReservationUtil.isAssociateInfantHasAmountDue(pax.getTravelerRefNumber(), passengerSummaryList)) {
					infantCount++;
				}

				if (!ReservationUtil.hasPaxAmountDue(pax.getTravelerRefNumber(), passengerSummaryList)) {
					continue;
				}

				adultChildCount++;
			}
		}

		int[] payablePaxCount = { adultChildCount, infantCount };
		return payablePaxCount;
	}

	public static void populatePaxWiseCreditCardCharge(Collection<ReservationPaxTO> paxList, ExternalChgDTO creditCardChgDTO,
			BigDecimal ccChargeAmount, Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges,
			Map<Integer, String> paxWisePaxTypes, List<FlightSegmentTO> flightSegmentTOs,
			Collection<LCCClientPassengerSummaryTO> passengerSummaryList, Set<Integer> payablePaxSequence)
			throws ModuleException {

		if (paxList != null && !paxList.isEmpty() && ccChargeAmount != null && ccChargeAmount.doubleValue() > 0
				&& paxWiseExternalCharges != null && paxWisePaxTypes != null) {
			int[] payablePaxCount = ServiceTaxCalculatorUtil.getPayablePaxCount(paxList, passengerSummaryList);
			int adultChildCount = payablePaxCount[0];
			int infantCount = payablePaxCount[1];
			int totalPayablePaxCount = 0;

			if (payablePaxSequence != null && !payablePaxSequence.isEmpty()) {
				totalPayablePaxCount = payablePaxSequence.size();
			} else {
				totalPayablePaxCount = adultChildCount + infantCount;
			}
			BigDecimal[] paxWiseCardFee = AccelAeroCalculator.roundAndSplit(ccChargeAmount, totalPayablePaxCount);
			int adultIndex = 0;
			int infantIndex = 0;
			for (ReservationPaxTO pax : paxList) {

				if (payablePaxSequence != null && !payablePaxSequence.isEmpty()
						&& !payablePaxSequence.contains(pax.getSeqNumber())) {
					continue;
				}

				if (!ReservationUtil.hasPaxAmountDue(pax.getTravelerRefNumber(), passengerSummaryList)) {
					continue;
				}

				List<LCCClientExternalChgDTO> chgDTOs = new ArrayList<>();
				chgDTOs.addAll(ServiceTaxCalculatorUtil.segmentWiseCreditCardChargeDistribution(flightSegmentTOs,
						paxWiseCardFee[adultIndex], creditCardChgDTO, false));

				appendPaxCharges(pax.getSeqNumber(), paxWiseExternalCharges, chgDTOs);
				paxWisePaxTypes.put(pax.getSeqNumber(), pax.getPaxType());
				
				if (passengerSummaryList == null && !PaxTypeTO.INFANT.equals(pax.getPaxType())
						&& (pax.getIsParent())
						&& (ReservationUtil.isAssociateInfantHasAmountDue(pax.getTravelerRefNumber(), passengerSummaryList))
						&& !(ReservationUtil.isAssociateInfantHasAmountDue(pax.getTravelerRefNumber(), passengerSummaryList) && !ReservationUtil
								.hasPaxAmountDue(pax.getTravelerRefNumber(), passengerSummaryList))) {
					int infantSequence = adultChildCount + infantIndex;
					int realInfantSequence = getAssociateInfantSeqNum(pax.getTravelerRefNumber(), paxList);
					// FIXME sometimes infant sequence can not get from traveler ref num
					if (realInfantSequence == -1) {
						realInfantSequence = infantSequence;
					}
					List<LCCClientExternalChgDTO> infantChgDTOs = new ArrayList<>();
					infantChgDTOs.addAll(ServiceTaxCalculatorUtil.segmentWiseCreditCardChargeDistribution(flightSegmentTOs,
							paxWiseCardFee[infantSequence], creditCardChgDTO, false));
					appendPaxCharges((infantSequence + 1), paxWiseExternalCharges, infantChgDTOs);
					paxWisePaxTypes.put(realInfantSequence, PaxTypeTO.INFANT);

					infantIndex++;

				}
				adultIndex++;
			}

			if (paxWiseExternalCharges.keySet().size() < totalPayablePaxCount) {
				if (passengerSummaryList != null && passengerSummaryList.size() > paxList.size()) {
					for (LCCClientPassengerSummaryTO summaryPax : passengerSummaryList) {
						Integer paxSeq = PaxTypeUtils.getPaxSeq(summaryPax.getTravelerRefNumber());
						if (!paxWiseExternalCharges.keySet().contains(paxSeq)) {
							List<LCCClientExternalChgDTO> chgDTOs = new ArrayList<>();
							chgDTOs.addAll(ServiceTaxCalculatorUtil.segmentWiseCreditCardChargeDistribution(flightSegmentTOs,
									paxWiseCardFee[adultIndex], creditCardChgDTO, false));

							appendPaxCharges(paxSeq, paxWiseExternalCharges, chgDTOs);
							paxWisePaxTypes.put(paxSeq, summaryPax.getPaxType());
							adultIndex++;
						}
					}

				}
			}
			// if (paxWiseExternalCharges.keySet().size() != totalPayablePaxCount) {
			// throw new ModuleException("Something is wrong! Let's fix");
			// }

		}

	}

	private static int getAssociateInfantSeqNum(String parentTravelerRefNum, Collection<ReservationPaxTO> paxList) {
		int infantSeq = -1;
		for (ReservationPaxTO resPax : paxList) {
			if (PaxTypeTO.INFANT.equals(resPax.getPaxType()) && parentTravelerRefNum != null
					&& PaxTypeUtils.getParentSeq(resPax.getTravelerRefNumber()) == PaxTypeUtils.getPaxSeq(parentTravelerRefNum)) {
				infantSeq = resPax.getSeqNumber();
				break;
			}
		}
		return infantSeq;
	}

	protected static void appendPaxCharges(int paxSequence, Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges,
			List<LCCClientExternalChgDTO> newCharges) {
		if (paxWiseExternalCharges != null) {
			paxWiseExternalCharges.get(paxSequence);

			if (paxWiseExternalCharges.get(paxSequence) == null) {
				paxWiseExternalCharges.put(paxSequence, new ArrayList<>());
			}

			paxWiseExternalCharges.get(paxSequence).addAll(newCharges);

		}
	}

	protected static int getInfantSequence(ReservationPaxTO parent, Collection<ReservationPaxTO> paxList) {
		int infantSequence = -1;
		for (ReservationPaxTO pax : paxList) {
			if (PaxTypeTO.INFANT.equals(pax.getPaxType()) && pax.getInfantWith().equals(parent.getSeqNumber())) {
				infantSequence = pax.getSeqNumber();
				break;
			}
		}

		return infantSequence;
	}

	public static Map<String, BigDecimal> addPaxWiseApplicableServiceTaxForDisplay(Collection<ReservationPaxTO> paxList,
			Map<String, ServiceTaxQuoteForTicketingRevenueResTO> serviceTaxQuoteForTktRevResTOMap,
			ReservationFlow reservationFlow) {

		BigDecimal totalServiceTaxAmountForAncillary = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalServiceTaxAmountForFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		Map<String, BigDecimal> serviceTaxAmountMap = new HashMap<>();

		if (serviceTaxQuoteForTktRevResTOMap != null && !serviceTaxQuoteForTktRevResTOMap.isEmpty() && paxList != null
				&& !paxList.isEmpty()) {

			for (String carrierCode : serviceTaxQuoteForTktRevResTOMap.keySet()) {
				ServiceTaxQuoteForTicketingRevenueResTO serviceTaxQuoteRS = serviceTaxQuoteForTktRevResTOMap.get(carrierCode);
				int[] payablePaxCount = getPayablePaxCount(paxList, null);
				String taxDepositCountryCode = serviceTaxQuoteRS.getServiceTaxDepositeCountryCode();
				String taxDepositStateCode = serviceTaxQuoteRS.getServiceTaxDepositeStateCode();
				log.info("Sevice Tax Calculation for Carrier - " + carrierCode);
				for (ReservationPaxTO pax : paxList) {

					String paxType = pax.getPaxType();
					Integer paxSequence = pax.getSeqNumber();
					boolean isParent = pax.getIsParent() != null ? pax.getIsParent() : false;

					if (PaxTypeTO.INFANT.equals(paxType)) {
						continue;
					}

					if (serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes() != null
							&& !serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes().isEmpty()) {
						List<LCCClientExternalChgDTO> chgDTOs = new ArrayList<>();
						List<LCCClientExternalChgDTO> infantChgDTOs = new ArrayList<>();
						List<ServiceTaxTO> serviceTaxs = serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes().get(paxType);
						addPaxServiceTaxesAsExtCharges(serviceTaxs, chgDTOs, taxDepositCountryCode, taxDepositStateCode, false);
						printServiceTaxChargesWithTotal(serviceTaxs, " PAX-Type " + paxType.toUpperCase());
						if (isParent) {
							List<ServiceTaxTO> infantServiceTaxs = serviceTaxQuoteRS.getPaxTypeWiseServiceTaxes()
									.get(PaxTypeTO.INFANT);
							addPaxServiceTaxesAsExtCharges(infantServiceTaxs, infantChgDTOs, taxDepositCountryCode,
									taxDepositStateCode, false);
							printServiceTaxChargesWithTotal(infantServiceTaxs, " PAX-Type " + PaxTypeTO.INFANT);
						}

						if (chgDTOs != null && !chgDTOs.isEmpty()) {
							List<LCCClientExternalChgDTO> groupedChargeDTOs = groupServiceTaxByChargeCode(chgDTOs);
							if (groupedChargeDTOs != null && !groupedChargeDTOs.isEmpty()) {
								totalServiceTaxAmountForFare = AccelAeroCalculator.add(totalServiceTaxAmountForFare,
										getTotalServiceTaxAmount(groupedChargeDTOs));
							}
						}

						if (isParent) {
							if (infantChgDTOs != null && !infantChgDTOs.isEmpty()) {
								List<LCCClientExternalChgDTO> groupedChargeDTOs = groupServiceTaxByChargeCode(infantChgDTOs);
								if (groupedChargeDTOs != null && !groupedChargeDTOs.isEmpty()) {
									totalServiceTaxAmountForFare = AccelAeroCalculator.add(totalServiceTaxAmountForFare,
											getTotalServiceTaxAmount(groupedChargeDTOs));

								}
							}
						}

					}

					if (serviceTaxQuoteRS.getPaxWiseServiceTaxes() != null
							&& !serviceTaxQuoteRS.getPaxWiseServiceTaxes().isEmpty()) {
						List<LCCClientExternalChgDTO> chgDTOs = new ArrayList<>();
						List<LCCClientExternalChgDTO> infantChgDTOs = new ArrayList<>();
						List<ServiceTaxTO> serviceTaxs = serviceTaxQuoteRS.getPaxWiseServiceTaxes().get(paxSequence);
						addPaxServiceTaxesAsExtCharges(serviceTaxs, chgDTOs, taxDepositCountryCode, taxDepositStateCode, false);
						printServiceTaxChargesWithTotal(serviceTaxs, " PAX-Sequence " + paxSequence);

						if (isParent) {
							int infantSequence;
							if (pax.getInfantWith() != null) {
								if (ReservationFlow.MODIFY == reservationFlow) {
									infantSequence = pax.getInfantWith() + 1;
								} else {
									infantSequence = payablePaxCount[0] + pax.getInfantWith();
								}
							} else {
								infantSequence = getInfantSequence(pax, paxList);
							}
							List<ServiceTaxTO> infantServiceTaxs = serviceTaxQuoteRS.getPaxWiseServiceTaxes().get(infantSequence);
							addPaxServiceTaxesAsExtCharges(infantServiceTaxs, infantChgDTOs, taxDepositCountryCode,
									taxDepositStateCode, false);
							printServiceTaxChargesWithTotal(infantServiceTaxs, " PAX-Sequence " + infantSequence);
						}

						if (chgDTOs != null && !chgDTOs.isEmpty()) {
							List<LCCClientExternalChgDTO> groupedChargeDTOs = groupServiceTaxByChargeCode(chgDTOs);
							if (groupedChargeDTOs != null && !groupedChargeDTOs.isEmpty()) {
								totalServiceTaxAmountForAncillary = AccelAeroCalculator.add(totalServiceTaxAmountForAncillary,
										getTotalServiceTaxAmount(groupedChargeDTOs));
							}
						}

						if (isParent) {
							if (infantChgDTOs != null && !infantChgDTOs.isEmpty()) {
								List<LCCClientExternalChgDTO> groupedChargeDTOs = groupServiceTaxByChargeCode(infantChgDTOs);
								if (groupedChargeDTOs != null && !groupedChargeDTOs.isEmpty()) {
									totalServiceTaxAmountForAncillary = AccelAeroCalculator.add(totalServiceTaxAmountForAncillary,
											getTotalServiceTaxAmount(groupedChargeDTOs));

								}
							}
						}

					}

				}

			}

		}

		totalServiceTaxAmountForFare = totalServiceTaxAmountForFare.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		totalServiceTaxAmountForAncillary = totalServiceTaxAmountForAncillary.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		serviceTaxAmountMap.put(ReservationInternalConstants.ServiceTaxIdentificationConstant.FARE, totalServiceTaxAmountForFare);
		serviceTaxAmountMap.put(ReservationInternalConstants.ServiceTaxIdentificationConstant.ANCI,
				totalServiceTaxAmountForAncillary);

		return serviceTaxAmountMap;
	}
}
