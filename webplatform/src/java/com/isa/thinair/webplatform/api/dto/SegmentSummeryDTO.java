package com.isa.thinair.webplatform.api.dto;

import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class SegmentSummeryDTO {

	public static final int IDX_FARE = 0;

	public static final int IDX_CHARGE = 1;

	public static final int IDX_CNX_CHARGE = 2;

	public static final int IDX_MOD_CHARGE = 3;

	public static final int IDX_ADJUSTEMTS = 4;

	public static final int IDX_TOTAL_CHARGES = 5;

	public static final int IDX_REFUNDS = 6;

	public static final int IDX_NON_REFUNDS = 7;

	// current
	private BigDecimal currentFare = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentCnxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentModCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentAdjustments = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentTotalCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentRefunds = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal currentNonRefunds = AccelAeroCalculator.getDefaultBigDecimalZero();

	// new
	private BigDecimal newFare = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newCnxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newModCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newAdjustments = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newTotalCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newRefunds = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal newNonRefunds = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal identifiedTotalCnxCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal identifiedTotalModCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	
	private BigDecimal identifiedExtraFeeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	public BigDecimal getCurrentAdjustments() {
		return currentAdjustments;
	}

	public void setCurrentAdjustments(BigDecimal currentAdjustments) {
		this.currentAdjustments = currentAdjustments;
	}

	public BigDecimal getCurrentCharge() {
		return currentCharge;
	}

	public void setCurrentCharge(BigDecimal currentCharge) {
		this.currentCharge = currentCharge;
	}

	public BigDecimal getCurrentCnxCharge() {
		return currentCnxCharge;
	}

	public void setCurrentCnxCharge(BigDecimal currentCnxCharge) {
		this.currentCnxCharge = currentCnxCharge;
	}

	public BigDecimal getCurrentFare() {
		return currentFare;
	}

	public void setCurrentFare(BigDecimal currentFare) {
		this.currentFare = currentFare;
	}

	public BigDecimal getCurrentModCharge() {
		return currentModCharge;
	}

	public void setCurrentModCharge(BigDecimal currentModCharge) {
		this.currentModCharge = currentModCharge;
	}

	public BigDecimal getCurrentRefunds() {
		return currentRefunds;
	}

	public void setCurrentRefunds(BigDecimal currentRefunds) {
		this.currentRefunds = currentRefunds;
	}

	public BigDecimal getCurrentNonRefunds() {
		return currentNonRefunds;
	}

	public void setCurrentNonRefunds(BigDecimal currentNonRefunds) {
		this.currentNonRefunds = currentNonRefunds;
	}

	public BigDecimal getCurrentTotalCharges() {
		return currentTotalCharges;
	}

	public void setCurrentTotalCharges(BigDecimal currentTotalCharges) {
		this.currentTotalCharges = currentTotalCharges;
	}

	public BigDecimal getNewAdjustments() {
		return newAdjustments;
	}

	public void setNewAdjustments(BigDecimal newAdjustments) {
		this.newAdjustments = newAdjustments;
	}

	public BigDecimal getNewCharge() {
		return newCharge;
	}

	public void setNewCharge(BigDecimal newCharge) {
		this.newCharge = newCharge;
	}

	public BigDecimal getNewCnxCharge() {
		return newCnxCharge;
	}

	public void setNewCnxCharge(BigDecimal newCnxCharge) {
		this.newCnxCharge = newCnxCharge;
	}

	public BigDecimal getNewFare() {
		return newFare;
	}

	public void setNewFare(BigDecimal newFare) {
		this.newFare = newFare;
	}

	public BigDecimal getNewModCharge() {
		return newModCharge;
	}

	public void setNewModCharge(BigDecimal newModCharge) {
		this.newModCharge = newModCharge;
	}

	public BigDecimal getNewRefunds() {
		return newRefunds;
	}

	public void setNewRefunds(BigDecimal newRefunds) {
		this.newRefunds = newRefunds;
	}

	public BigDecimal getNewNonRefunds() {
		return newNonRefunds;
	}

	public void setNewNonRefunds(BigDecimal newNonRefunds) {
		this.newNonRefunds = newNonRefunds;
	}

	public BigDecimal getNewTotalCharges() {
		return newTotalCharges;
	}

	public void setNewTotalCharges(BigDecimal newTotalCharges) {
		this.newTotalCharges = newTotalCharges;
	}

	public BigDecimal getIdentifiedTotalCnxCharge() {
		return identifiedTotalCnxCharge;
	}

	public void setIdentifiedTotalCnxCharge(BigDecimal identifiedTotalCnxCharge) {
		this.identifiedTotalCnxCharge = identifiedTotalCnxCharge;
	}

	public BigDecimal getIdentifiedTotalModCharge() {
		return identifiedTotalModCharge;
	}

	public void setIdentifiedTotalModCharge(BigDecimal identifiedTotalModCharge) {
		this.identifiedTotalModCharge = identifiedTotalModCharge;
	}

	public BigDecimal getIdentifiedExtraFeeAmount() {
		return identifiedExtraFeeAmount;
	}

	public void setIdentifiedExtraFeeAmount(BigDecimal identifiedExtraFeeAmount) {
		this.identifiedExtraFeeAmount = identifiedExtraFeeAmount;
	}
	
}
