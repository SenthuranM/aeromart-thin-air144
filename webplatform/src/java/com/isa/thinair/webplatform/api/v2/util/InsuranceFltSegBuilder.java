package com.isa.thinair.webplatform.api.v2.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class InsuranceFltSegBuilder {
	private boolean isReturn = false;
	private List<FlightSegmentTO> selectedFlightSegments = null;
	private List<FlightSegmentTO> insurableFltSegs = null;
	private String originAirport = null;
	private String destinationAirport = null;
	private FlightSegmentTO firstFltSeg = null;
	private IFlightSegment lastFltSeg = null;

	public InsuranceFltSegBuilder(List<FlightSegmentTO> fltSegments) throws ModuleException {
		this.selectedFlightSegments = fltSegments;
		Collections.sort(selectedFlightSegments);
		this.insurableFltSegs = new ArrayList<FlightSegmentTO>();
		calculateOndInfo();
	}

	public FlightSegmentTO getFlightSegment() {
		return firstFltSeg;
	}

	public Date getJourneyStartDate() {
		return firstFltSeg.getDepartureDateTime();
	}

	public Date getJourneyStartDateZulu() {
		return firstFltSeg.getDepartureDateTimeZulu();
	}

	public String getOriginFltRefNo() {
		return firstFltSeg.getFlightRefNumber();
	}

	public Date getJourneyEndDate() {
		return lastFltSeg.getArrivalDateTime();
	}

	public Date getJourneyEndDateZulu() {
		return lastFltSeg.getArrivalDateTimeZulu();
	}

	public String getOriginAirport() {
		return originAirport;
	}

	public String getDestinationAirport() {
		return destinationAirport;
	}

	public boolean isReturnJourney() {
		return isReturn;
	}

	public List<FlightSegmentTO> getInsurableFlightSegments() {
		return insurableFltSegs;
	}

	public boolean hasFlightSegments() {
		return (insurableFltSegs.size() > 0);
	}

	public List<String> getInsurableFlightRefNumbers() {
		List<String> arrList = new ArrayList<String>();
		for (FlightSegmentTO fltSeg : insurableFltSegs) {
			arrList.add(fltSeg.getFlightRefNumber());
		}
		return arrList;
	}

	private void calculateOndInfo() throws ModuleException {
		if (this.selectedFlightSegments != null) {
			Date currentDate = CalendarUtil.getCurrentSystemTimeInZulu();
			IFlightSegment lastFltSegOneway = null;
			
			Map<String, String> busAirCarrierCodes = WPModuleUtils.getFlightServiceBD().getBusAirCarrierCodes();
			
			for (FlightSegmentTO fltSeg : selectedFlightSegments) {
				//Assumption: We don't quote insurance for bus segments
				if (busAirCarrierCodes
						.containsKey(FlightRefNumberUtil.getOperatingAirline(fltSeg.getFlightRefNumber()))
						|| fltSeg.getDepartureDateTimeZulu().compareTo(currentDate) <= 0)
					continue;
				insurableFltSegs.add(fltSeg);

				if (!fltSeg.isReturnFlag()
						&& (lastFltSegOneway == null || lastFltSegOneway.getArrivalDateTimeZulu().compareTo(
								fltSeg.getArrivalDateTimeZulu()) < 0)) {
					lastFltSegOneway = fltSeg;
				}
				if (fltSeg.isReturnFlag()) {
					isReturn = true;
				}
			}

			Collections.sort(insurableFltSegs);
			if (insurableFltSegs != null && insurableFltSegs.size() > 0) {
				firstFltSeg = insurableFltSegs.get(0);
				lastFltSeg = insurableFltSegs.get(insurableFltSegs.size() - 1);
				originAirport = insurableFltSegs.get(0).getSegmentCode().split("/")[0];
			}

			if (lastFltSegOneway != null) {
				destinationAirport = lastFltSegOneway.getSegmentCode().split("/")[lastFltSegOneway.getSegmentCode().split("/").length - 1];
			}
		}
	}
}
