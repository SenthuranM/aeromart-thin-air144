package com.isa.thinair.webplatform.api.util;

import java.util.List;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.webplatform.core.service.ModuleServiceLocator;

/**
 * Manoj
 */
public class CodeshareUtil {	
	
	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();
	
	public static boolean isCodeshareMCPnr(List<FlightSegmentTO> flightSegmentTOs) {
		Map<String, GDSStatusTO> gdsStatusMap = globalConfig.getActiveGdsMap();
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (gdsStatusMap != null && !gdsStatusMap.isEmpty() && flightSegmentTO.getOperatingAirline() != null
					&& !AppSysParamsUtil.getDefaultCarrierCode().equals(flightSegmentTO.getOperatingAirline())) {
				for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
					if (gdsStatusTO != null && gdsStatusTO.isCodeShareCarrier()
							&& flightSegmentTO.getOperatingAirline().equals(gdsStatusTO.getCarrierCode())) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public static boolean isCodeshareMCPnr(FlightPriceRQ flightPriceRQ) {
		if (flightPriceRQ != null && flightPriceRQ.getOrderedOriginDestinationInformationList() != null
				&& flightPriceRQ.getOrderedOriginDestinationInformationList().size() > 0) {
			for (OriginDestinationInformationTO originDestinationInformationTO : flightPriceRQ
					.getOrderedOriginDestinationInformationList()) {
				if (originDestinationInformationTO.getOrignDestinationOptions() != null
						&& !originDestinationInformationTO.getOrignDestinationOptions().isEmpty()) {
					for (OriginDestinationOptionTO originDestinationOptionTO : originDestinationInformationTO
							.getOrignDestinationOptions()) {
						if (originDestinationOptionTO.getFlightSegmentList() != null
								&& !originDestinationOptionTO.getFlightSegmentList().isEmpty()) {
							isCodeshareMCPnr(originDestinationOptionTO.getFlightSegmentList());
						}
					}
				}
			}
		}
		return false;
	}

}
