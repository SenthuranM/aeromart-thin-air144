package com.isa.thinair.webplatform.api.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * TO To hold user visiblity data by message id.
 * 
 * @author Navod Ediriweera
 * 
 */
public class DashbrdMsgRetrivedDataTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3571373876765278939L;
	private Set<Integer> includedIDSet;
	private Set<Integer> excludedIDSet;

	public Set<Integer> getIncludedIDSet() {
		if (this.includedIDSet == null)
			includedIDSet = new HashSet<Integer>();
		return includedIDSet;
	}

	public void setIncludedIDSet(Set<Integer> includedIDSet) {
		this.includedIDSet = includedIDSet;
	}

	public Set<Integer> getExcludedIDSet() {
		if (this.excludedIDSet == null)
			excludedIDSet = new HashSet<Integer>();
		return excludedIDSet;
	}

	public void setExcludedIDSet(Set<Integer> excludedIDSet) {
		this.excludedIDSet = excludedIDSet;
	}

}
