package com.isa.thinair.webplatform.api.dto;

public class AiportServiceSearchDTO {

	int noOfAdult = 0;
	int noOfChd = 0;
	int noOfInf = 0;

	String fromAirport;

	String toAirport;

	boolean isReturn;

	/**
	 * @return Returns the noOfAdult.
	 */
	public int getNoOfAdult() {
		return noOfAdult;
	}

	/**
	 * @param noOfAdult
	 *            The noOfAdult to set.
	 */
	public void setNoOfAdult(int noOfAdult) {
		this.noOfAdult = noOfAdult;
	}

	/**
	 * @return Returns the noOfChd.
	 */
	public int getNoOfChd() {
		return noOfChd;
	}

	/**
	 * @param noOfChd
	 *            The noOfChd to set.
	 */
	public void setNoOfChd(int noOfChd) {
		this.noOfChd = noOfChd;
	}

	/**
	 * @return Returns the noOfInf.
	 */
	public int getNoOfInf() {
		return noOfInf;
	}

	/**
	 * @param noOfInf
	 *            The noOfInf to set.
	 */
	public void setNoOfInf(int noOfInf) {
		this.noOfInf = noOfInf;
	}

	/**
	 * @return Returns the fromAirport.
	 */
	public String getFromAirport() {
		return fromAirport;
	}

	/**
	 * @param fromAirport
	 *            The fromAirport to set.
	 */
	public void setFromAirport(String fromAirport) {
		this.fromAirport = fromAirport;
	}

	/**
	 * @return Returns the isReturn.
	 */
	public boolean isReturn() {
		return isReturn;
	}

	/**
	 * @param isReturn
	 *            The isReturn to set.
	 */
	public void setReturn(boolean isReturn) {
		this.isReturn = isReturn;
	}

	/**
	 * @return Returns the toAirport.
	 */
	public String getToAirport() {
		return toAirport;
	}

	/**
	 * @param toAirport
	 *            The toAirport to set.
	 */
	public void setToAirport(String toAirport) {
		this.toAirport = toAirport;
	}

}
