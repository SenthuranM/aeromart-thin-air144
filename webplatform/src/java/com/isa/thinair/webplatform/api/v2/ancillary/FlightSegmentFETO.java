package com.isa.thinair.webplatform.api.v2.ancillary;

import java.io.Serializable;

/**
 * 
 * @author rumesh
 * 
 */

public class FlightSegmentFETO implements Serializable {
	private static final long serialVersionUID = -6332231481523326525L;

	private String segmentRPH;
	private String segmentCode;
	private String flightNo;
	private boolean domesticFlight;
	private String carrierCode;
	private String departureDate;
	private String departureDateTimeZulu;
	private String arrivalDate;
	private String arrivalDateTimeZulu;
	private int ondSequence;
	private boolean flownSegment;

	public String getSegmentRPH() {
		return segmentRPH;
	}

	public void setSegmentRPH(String segmentRPH) {
		this.segmentRPH = segmentRPH;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public boolean isDomesticFlight() {
		return domesticFlight;
	}

	public void setDomesticFlight(boolean domesticFlight) {
		this.domesticFlight = domesticFlight;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getDepartureDateTimeZulu() {
		return departureDateTimeZulu;
	}

	public void setDepartureDateTimeZulu(String departureDateTimeZulu) {
		this.departureDateTimeZulu = departureDateTimeZulu;
	}

	public String getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getArrivalDateTimeZulu() {
		return arrivalDateTimeZulu;
	}

	public void setArrivalDateTimeZulu(String arrivalDateTimeZulu) {
		this.arrivalDateTimeZulu = arrivalDateTimeZulu;
	}

	public int getOndSequence() {
		return ondSequence;
	}

	public void setOndSequence(int ondSequence) {
		this.ondSequence = ondSequence;
	}

	public boolean isFlownSegment() {
		return flownSegment;
	}

	public void setFlownSegment(boolean flownSegment) {
		this.flownSegment = flownSegment;
	}
	
}
