package com.isa.thinair.webplatform.api.util;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

public class HttpUtil {
	public static String getHTTPHeaderValue(HttpServletRequest request, String headerName) {
		String headerParamValue = "";
		try {
			headerParamValue = StringUtils.trimToEmpty(request.getHeader(headerName));
		} catch (Exception e) {
			headerParamValue = "-";
		}
		return headerParamValue;
	}
}
