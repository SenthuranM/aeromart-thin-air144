package com.isa.thinair.webplatform.api.v2.reservation;

import java.math.BigDecimal;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class ConfirmUpdateChargesListTO {

	private String displayDesc;

	private String displayOldCharges;

	private String displayNewCharges;

	private CurrencyExchangeRate selCurrExgRate;

	public ConfirmUpdateChargesListTO(CurrencyExchangeRate selCurrExgRate) {
		this.selCurrExgRate = selCurrExgRate;
	}

	public ConfirmUpdateChargesListTO() {
	}

	// Controls whether charge is visible to out side or not. Added for flexi
	private boolean enableDisplay = true;

	/**
	 * @return the displayDesc
	 */
	public String getDisplayDesc() {
		return displayDesc;
	}

	/**
	 * @param displayDesc
	 *            the displayDesc to set
	 */
	public void setDisplayDesc(String displayDesc) {
		this.displayDesc = displayDesc;
	}

	/**
	 * @return the displayOldCharges
	 */
	public String getDisplayOldCharges() {
		return displayOldCharges;
	}

	/**
	 * @return the displayOldCharges
	 */
	public String getSelCurDisplayOldCharges() {
		if (selCurrExgRate != null) {
			return getValueConvertedAndFormatted(getDisplayOldCharges());
		}
		return null;
	}

	/**
	 * @param displayOldCharges
	 *            the displayOldCharges to set
	 */
	public void setDisplayOldCharges(String displayOldCharges) {
		this.displayOldCharges = displayOldCharges;
	}

	/**
	 * @return the displayNewCharges
	 */
	public String getDisplayNewCharges() {
		return displayNewCharges;
	}

	public String getSelCurDisplayNewCharges() {
		if (selCurrExgRate != null) {
			return getValueConvertedAndFormatted(getDisplayNewCharges());
		}
		return null;
	}

	private String getValueConvertedAndFormatted(String str) {
		str = str.replace(" ", "");
		if ("".equals(str))
			return str;
		BigDecimal value = AccelAeroCalculator.multiply(selCurrExgRate.getMultiplyingExchangeRate(), new BigDecimal(str));
		if (value.compareTo(BigDecimal.ZERO) < 0) {
			return "- " + AccelAeroCalculator.formatAsDecimal(value.negate());
		} else {
			return AccelAeroCalculator.formatAsDecimal(value);
		}
	}

	/**
	 * @param displayNewCharges
	 *            the displayNewCharges to set
	 */
	public void setDisplayNewCharges(String displayNewCharges) {
		this.displayNewCharges = displayNewCharges;
	}

	/**
	 * Checks if is enable display.
	 * 
	 * @return true, if is enable display
	 */
	public boolean isEnableDisplay() {
		return enableDisplay;
	}

	/**
	 * Sets the enable display.
	 * 
	 * @param enableDisplay
	 *            the new enable display
	 */
	public void setEnableDisplay(boolean enableDisplay) {
		this.enableDisplay = enableDisplay;
	}

}
