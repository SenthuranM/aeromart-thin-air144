package com.isa.thinair.webplatform.api.util.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD })
public @interface IGNORE {
	enum TYPE {
		JS_UNDEFINED, JS_STR_NULL
	}

	TYPE[] value();
}