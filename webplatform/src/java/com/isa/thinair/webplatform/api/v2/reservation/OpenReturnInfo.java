package com.isa.thinair.webplatform.api.v2.reservation;

import java.text.SimpleDateFormat;

import com.isa.thinair.airproxy.api.model.reservation.commons.OpenReturnOptionsTO;

public class OpenReturnInfo {
	private String confirmExpiryDate;
	private String travelExpiryDate;
	private String segmentCode;

	public OpenReturnInfo(OpenReturnOptionsTO oroTO, String fromAirport, String toAirport) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		this.confirmExpiryDate = sdf.format(oroTO.getConfirmExpiryDate()) + " " + oroTO.getAirportCode();
		this.travelExpiryDate = sdf.format(oroTO.getTravelExpiryDate()) + " " + oroTO.getAirportCode();
		this.segmentCode = toAirport + "/" + fromAirport;
	}

	/**
	 * @return the confirmExpiryDate
	 */
	public String getConfirmExpiryDate() {
		return confirmExpiryDate;
	}

	/**
	 * @return the travelExpiryDate
	 */
	public String getTravelExpiryDate() {
		return travelExpiryDate;
	}

	/**
	 * @return the segmentCode
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

}
