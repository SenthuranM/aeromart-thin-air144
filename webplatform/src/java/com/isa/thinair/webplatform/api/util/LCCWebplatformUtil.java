package com.isa.thinair.webplatform.api.util;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.webplatform.core.util.LookupUtils;

public class LCCWebplatformUtil {
	private static Log log = LogFactory.getLog(LCCWebplatformUtil.class);

	public static Map[] getLCCParentStationDetailsMap() {
		return LookupUtils.lccLookupDAO().getLCCParentStationDetailsMap();
	}
}
