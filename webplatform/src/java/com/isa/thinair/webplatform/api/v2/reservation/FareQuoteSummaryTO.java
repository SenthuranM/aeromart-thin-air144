package com.isa.thinair.webplatform.api.v2.reservation;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class FareQuoteSummaryTO {
	private String currency;

	private String totalSurcharges;

	private String baseFare;

	private boolean onHoldRestricted = false;

	private String totalTaxes;

	private String totalPrice;

	private String totalWithoutHandlingCharge;

	private String selectedCurrency;

	private String selectedEXRate;

	private String selectedtotalPrice;

	private String totalCurrencyRoundUpDiff;

	private String totalHandlingCharge;

	private String fareType;

	private String totalFareDiscount;

	private String totalApplicableAgentCommission;
	
	private String totalServiceTax = "";
	
	private String adminFee;
	
	private String selectCurrAdminFee;
	

	public FareQuoteSummaryTO() {
		setCurrency(AppSysParamsUtil.getBaseCurrency());
	}

	public FareQuoteSummaryTO(String selectedCurrency) {
		setCurrency(AppSysParamsUtil.getBaseCurrency());
		setSelectedCurrency(selectedCurrency);
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the totalSurcharges
	 */
	public String getTotalSurcharges() {
		return totalSurcharges;
	}

	/**
	 * @param totalSurcharges
	 *            the totalSurcharges to set
	 */
	public void setTotalSurcharges(String totalSurcharges) {
		this.totalSurcharges = totalSurcharges;
	}

	/**
	 * @return the baseFare
	 */
	public String getBaseFare() {
		return baseFare;
	}

	/**
	 * @param baseFare
	 *            the baseFare to set
	 */
	public void setBaseFare(String baseFare) {
		this.baseFare = baseFare;
	}

	/**
	 * @return the onHoldRestricted
	 */
	public boolean isOnHoldRestricted() {
		return onHoldRestricted;
	}

	/**
	 * @param onHoldRestricted
	 *            the onHoldRestricted to set
	 */
	public void setOnHoldRestricted(boolean onHoldRestricted) {
		this.onHoldRestricted = onHoldRestricted;
	}

	/**
	 * @return the totalTaxes
	 */
	public String getTotalTaxes() {
		return totalTaxes;
	}

	/**
	 * @param totalTaxes
	 *            the totalTaxes to set
	 */
	public void setTotalTaxes(String totalTaxes) {
		this.totalTaxes = totalTaxes;
	}

	/**
	 * @return the totalPrice
	 */
	public String getTotalPrice() {
		return totalPrice;
	}

	/**
	 * @param totalPrice
	 *            the totalPrice to set
	 */
	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getSelectedCurrency() {
		return selectedCurrency;
	}

	public void setSelectedCurrency(String selectedCurrency) {
		this.selectedCurrency = selectedCurrency;
	}

	public String getSelectedEXRate() {
		return selectedEXRate;
	}

	public void setSelectedEXRate(String selectedEXRate) {
		this.selectedEXRate = selectedEXRate;
	}

	public String getSelectedtotalPrice() {
		return selectedtotalPrice;
	}

	public void setSelectedtotalPrice(String selectedtotalPrice) {
		this.selectedtotalPrice = selectedtotalPrice;
	}

	/**
	 * @return the totalWithoutHandlingCharge
	 */
	public String getTotalWithoutHandlingCharge() {
		return totalWithoutHandlingCharge;
	}

	/**
	 * @param totalWithoutHandlingCharge
	 *            the totalWithoutHandlingCharge to set
	 */
	public void setTotalWithoutHandlingCharge(String totalWithoutHandlingCharge) {
		this.totalWithoutHandlingCharge = totalWithoutHandlingCharge;
	}

	/**
	 * @return the totalCurrencyRoundUpDiff
	 */
	public String getTotalCurrencyRoundUpDiff() {
		return totalCurrencyRoundUpDiff;
	}

	/**
	 * @param totalCurrencyRoundUpDiff
	 *            the totalCurrencyRoundUpDiff to set
	 */
	public void setTotalCurrencyRoundUpDiff(String totalCurrencyRoundUpDiff) {
		this.totalCurrencyRoundUpDiff = totalCurrencyRoundUpDiff;
	}

	/**
	 * @return the totalHandlingCharge
	 */
	public String getTotalHandlingCharge() {
		return totalHandlingCharge;
	}

	/**
	 * @param totalHandlingCharge
	 *            the totalHandlingCharge to set
	 */
	public void setTotalHandlingCharge(String totalHandlingCharge) {
		this.totalHandlingCharge = totalHandlingCharge;
	}

	public String getFareType() {
		return fareType;
	}

	public void setFareType(String fareType) {
		this.fareType = fareType;
	}

	public void setDiscountedFareAmount(String totalFareDiscount) {
		this.totalFareDiscount = totalFareDiscount;
	}

	public String getTotalFareDiscount() {
		return totalFareDiscount;
	}

	public String getDomFareDiscountPerc() throws ModuleException {
		return AppSysParamsUtil.getDomesticFareDiscountPercentage() + "";
	}

	public String getTotalApplicableAgentCommission() {
		return totalApplicableAgentCommission;
	}

	public void setTotalApplicableAgentCommission(String totalApplicableAgentCommission) {
		this.totalApplicableAgentCommission = totalApplicableAgentCommission;
	}
	
	public String getTotalServiceTax() {
		return totalServiceTax;
	}

	public void setTotalServiceTax(String totalServiceTax) {
		this.totalServiceTax = totalServiceTax;
	}
	/**
	 * 
	 * @return
	 */
	public String getAdminFee() {
		return adminFee;
	}

	/**
	 * 
	 * @param adminFee
	 */
	public void setAdminFee(String adminFee) {
		this.adminFee = adminFee;
	}

	public String getSelectCurrAdminFee() {
		return selectCurrAdminFee;
	}

	public void setSelectCurrAdminFee(String selectCurrAdminFee) {
		this.selectCurrAdminFee = selectCurrAdminFee;
	}
	

}
