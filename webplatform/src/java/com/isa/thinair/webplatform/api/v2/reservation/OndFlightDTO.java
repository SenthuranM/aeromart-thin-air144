package com.isa.thinair.webplatform.api.v2.reservation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.struts2.json.annotations.JSON;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class OndFlightDTO implements Serializable, Comparable<OndFlightDTO> {

	private final String DATE_DISPLAY_FMT = "ddMMMyy";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<AvailableFlightInfo> flightInfo = new ArrayList<AvailableFlightInfo>();
	private Date firstDepartureDate = null;
	private boolean avlFltSortingReq = false;
	private int avlFltIndex = 1;
	private String ondCode = null;

	public OndFlightDTO(String origin, String destination) {
		this.ondCode = origin + "/" + destination;
	}

	public void addAvailableFlightInfo(AvailableFlightInfo availableFlightInfo) {
		if (firstDepartureDate == null || firstDepartureDate.after(availableFlightInfo.getFirstDepartureDate())) {
			if (firstDepartureDate != null) {
				avlFltSortingReq = true;
			}
			firstDepartureDate = availableFlightInfo.getFirstDepartureDate();
		}
		availableFlightInfo.setIndex(avlFltIndex++);
		flightInfo.add(availableFlightInfo);
	}

	@JSON(serialize = false)
	public Date getFirstDepartureDate() {
		return firstDepartureDate;
	}

	public String getOndCode() {
		return ondCode;
	}

	public String getDate() {
		return DateUtil.formatDate(firstDepartureDate, DATE_DISPLAY_FMT);
	}

	public List<AvailableFlightInfo> getFlightInfo() {
		if (avlFltSortingReq) {
			Collections.sort(flightInfo);
			avlFltSortingReq = false;
		}
		return flightInfo;
	}

	@Override
	public int compareTo(OndFlightDTO ondFlt) {
		return this.firstDepartureDate.compareTo(ondFlt.getFirstDepartureDate());
	}
}
