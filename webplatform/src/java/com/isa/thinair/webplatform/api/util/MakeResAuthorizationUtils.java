package com.isa.thinair.webplatform.api.util;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * Make Reservation validation Utils
 * 
 * @author Duminda G
 * @since Oct 07, 2008
 */
public class MakeResAuthorizationUtils {

	private static Log log = LogFactory.getLog(MakeResAuthorizationUtils.class);

	/**
	 * Returns the Segment is in cutover time
	 * 
	 * @param segmentDeparture
	 * @return
	 * @throws ModuleException
	 */
	public static boolean isInCutoverTime(Collection<String> privilegeKeys, Date departureDate, Date currentDate,
			String cutoverTime) {
		try {
			if (AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_ADD_SEATS_IN_CUTOVER)) {
				if (log.isDebugEnabled()) {
					log.debug("Has xbe.res.add.seatcutover privilege");
				}
				return false;
			} else if ((departureDate.getTime() - currentDate.getTime()) < getTotalMiliseconds(cutoverTime)) {
				if (log.isDebugEnabled()) {
					log.debug("In Seat cutover time");
				}
				return true;
			} else {
				if (log.isDebugEnabled()) {
					log.debug("Can add seats");
				}
				return false;
			}
		} catch (ModuleException e) {
			log.error("Error seat map cutover time ", e);
			return true;
		}
	}

	/**
	 * Convert cutover time to miliseconds
	 * 
	 * @param duration
	 * @return
	 * @throws ModuleException
	 */
	private static long getTotalMiliseconds(String duration) throws ModuleException {
		if (duration == null || duration.equals("") || duration.length() < 5) {
			throw new ModuleException("MakeResAuthorizationUtils");
		}
		String[] arrDuration = duration.split(":");

		return ((new Long(arrDuration[0]).intValue() * 60) + new Long(arrDuration[1]).intValue()) * 60 * 1000;
	}

}
