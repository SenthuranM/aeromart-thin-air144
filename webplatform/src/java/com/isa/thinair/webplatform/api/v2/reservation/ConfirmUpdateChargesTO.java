package com.isa.thinair.webplatform.api.v2.reservation;

import java.util.Collection;

public class ConfirmUpdateChargesTO {
	private String displayCredit;

	private String displayDue;

	private Collection<ConfirmUpdateChargesListTO> chargesList;

	/**
	 * @return the displayCredit
	 */
	public String getDisplayCredit() {
		return displayCredit;
	}

	/**
	 * @param displayCredit
	 *            the displayCredit to set
	 */
	public void setDisplayCredit(String displayCredit) {
		this.displayCredit = displayCredit;
	}

	/**
	 * @return the displayDue
	 */
	public String getDisplayDue() {
		return displayDue;
	}

	/**
	 * @param displayDue
	 *            the displayDue to set
	 */
	public void setDisplayDue(String displayDue) {
		this.displayDue = displayDue;
	}

	/**
	 * @return the chargesList
	 */
	public Collection<ConfirmUpdateChargesListTO> getChargesList() {
		return chargesList;
	}

	/**
	 * @param chargesList
	 *            the chargesList to set
	 */
	public void setChargesList(Collection<ConfirmUpdateChargesListTO> chargesList) {
		this.chargesList = chargesList;
	}
}
