package com.isa.thinair.webplatform.api.util;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airsecurity.api.model.Privilege;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;

public class PrivilegeUtil {

	private static Map<String, Privilege> activePrivilegeMap = new HashMap<String, Privilege>();
	private static final long CASH_TIMEOUT = AppSysParamsUtil.getMasterDataCacheTimeout();
	private static volatile Date recordAge = new Date(System.currentTimeMillis() - CASH_TIMEOUT);
	private static volatile boolean refreshing = false;

	private static Log log = LogFactory.getLog(PrivilegeUtil.class);

	public static Map<String, Privilege> getActivePrivilegeMap() throws ModuleException {

		if ((System.currentTimeMillis() - recordAge.getTime()) >= CASH_TIMEOUT) {
			loadActivePrivilegeMap();
		}

		if (!refreshing) {
			return activePrivilegeMap;
		} else {
			synchronized (activePrivilegeMap) {
				return activePrivilegeMap;
			}
		}
	}

	private static void loadActivePrivilegeMap() throws ModuleException {
		synchronized (activePrivilegeMap) {
			if ((System.currentTimeMillis() - recordAge.getTime()) >= CASH_TIMEOUT) {
				refreshing = true;
				try {
					SecurityBD securityBD = WPModuleUtils.getSecurityBD();
					Collection privilegeCol = securityBD.getActivePrivileges("AA");
					activePrivilegeMap = new HashMap<String, Privilege>();
					for (Iterator iter = privilegeCol.iterator(); iter.hasNext();) {
						Privilege privilege = (Privilege) iter.next();
						activePrivilegeMap.put(privilege.getPrivilegeId(), privilege);
					}
					log.info("Privilage list referched at" + recordAge);
					recordAge.setTime(System.currentTimeMillis());
				} finally {
					refreshing = false;
				}
			}

		}
	}

	/**
	 * populate the privileges for given user in XBE/Admin/AAServices Normal User get the privileges assign to the user
	 * filter out the inactive privileges Dry User get the privileges assign to the dry user form the MC side filter out
	 * the inactive privileges from the OC remove the excluded privileges defined in the MC side add the included
	 * privileges defined in the MC side
	 * 
	 * @param user
	 * @param userPrincipal
	 * @return
	 * @throws ModuleException
	 */
	public static Map<String, String> getXBEAndAdminUserPrivileges(User user, UserPrincipal userPrincipal) throws ModuleException {

		String userDefaultcarrierCode = user.getDefaultCarrierCode();
		String defaultCarrierCode = AppSysParamsUtil.getDefaultCarrierCode();
		String defaultCarrierIdentifier = AppSysParamsUtil.getDefaultAirlineIdentifierCode();

		// Filter out the inactive privileges
		Map<String, String> activePrivileges = new HashMap<String, String>();
		Map<String, Privilege> allPrivilegeMap = PrivilegeUtil.getActivePrivilegeMap();
		for (String privID : (Set<String>) user.getPrivitedgeIDs()) {
			Privilege privilege = allPrivilegeMap.get(privID);
			if (privilege != null && privilege.getStatus().equals(Privilege.STATUS_ACTIVE)) {
				activePrivileges.put(privID, "1");
			}
		}
		activePrivileges.get("xbe.res.make.pay.card");
		// apply exclude/include filters for dry users
		if (!userDefaultcarrierCode.equals(defaultCarrierCode)) {
			// Excluding
			if (!(isSystemUser(user.getUserId()) || isSchedulerUser(user.getUserId()))) {
				Set<String> excludePrivileges = WPModuleUtils.getSecurityBD().getExcludePrivilegesForDryUser(user.getUserId(),
						defaultCarrierIdentifier);
				for (String excludePrivile : excludePrivileges) {
					activePrivileges.remove(excludePrivile);
				}
			}

			// Including
			Set<String> includePrivileges = WPModuleUtils.getSecurityBD().getIncludePrivilegesForDryUser(user.getUserId(),
					defaultCarrierIdentifier);
			for (String includePrivile : includePrivileges) {
				activePrivileges.put(includePrivile, "1");
			}
		}

		return activePrivileges;
	}

	public static boolean hasPrivilege(Set<String> privileges, String priviKey) {
		if (privileges.contains(priviKey)) {
			return true;
		}
		return false;
	}

	public static boolean isSystemUser(String userID) {
		boolean isSystemUser = false;

		if (userID != null && userID.length() == 9 && userID.endsWith("SYSTEM")) {
			isSystemUser = true;
		}

		return isSystemUser;
	}
	
	public static boolean isSchedulerUser(String userID) {
		boolean isSchedulerUser = false;

		if (userID != null && userID.length() == 12 && userID.endsWith("SCHEDULER")) {
			isSchedulerUser = true;
		}

		return isSchedulerUser;
	}

}
