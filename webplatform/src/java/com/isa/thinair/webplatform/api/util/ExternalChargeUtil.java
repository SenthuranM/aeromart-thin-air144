package com.isa.thinair.webplatform.api.util;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.opentravel.ota._2003._05.IOTACodeTables;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxChargesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateJourneyType;
import com.isa.thinair.commons.api.dto.ChargeApplicabilityTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.webplatform.api.dto.PaxSet;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

/**
 * @author eric
 */
public class ExternalChargeUtil {
	/**
	 * @param paxSet
	 * @param priceInfoTO
	 * @param externalCharge
	 * @throws ModuleException
	 */
	public static void calculateExternalChargeAmount(PaxSet paxSet, PriceInfoTO priceInfoTO, ExternalChgDTO externalCharge,
			boolean returnJourney) throws ModuleException {

		if (externalCharge != null) {
			// set applicable charge rate
			externalCharge.setApplicableChargeRate(returnJourney ? ChargeRateJourneyType.RETURN : ChargeRateJourneyType.ONEWAY);

			if (externalCharge.isValid() && externalCharge.getRatioValue() != null
					&& externalCharge.getRatioValue().compareTo(BigDecimal.ZERO) > 0) {
				if (externalCharge.isRatioValueInPercentage()) {
					calcualteExtrenalChargeByPercentage(paxSet, priceInfoTO, externalCharge);
				} else {
					calcualteExtrenalChargeByValue(paxSet, priceInfoTO, externalCharge);
				}
			}
		}
	}

	/**
	 * @param paxSet
	 * @param priceInfoTO
	 * @param externalCharge
	 */
	private static void
			calcualteExtrenalChargeByPercentage(PaxSet paxSet, PriceInfoTO priceInfoTO, ExternalChgDTO externalCharge)
					throws ModuleException {

		BigDecimal totalAmount = BigDecimal.ZERO;

		switch (externalCharge.getAppliesTo()) {

		case ChargeApplicabilityTO.APPLICABLE_TO_RESERVATION:
			totalAmount = getHandlingFeeAmountToReservation(priceInfoTO.getFareTypeTO(), externalCharge);

			break;
		case ChargeApplicabilityTO.APPLICABLE_TO_ADULT_CHILD:
			totalAmount = getHandlingFeeAmountToAdultChild(priceInfoTO.getFareTypeTO(), externalCharge);
			break;
		}

		if (externalCharge.isChargeRateRoundingApplicable()) {
			BigDecimal roundedTotal = AccelAeroRounderPolicy.getRoundedValue(totalAmount, externalCharge.getBoundryValue(),
					externalCharge.getBreakPoint());
			totalAmount = roundedTotal;
		}

		totalAmount = getApplicableAmount(externalCharge.getChargeValueMin(), externalCharge.getChargeValueMax(), totalAmount);
		externalCharge.setTotalAmount(AccelAeroCalculator.parseBigDecimal(totalAmount.doubleValue()));

	}

	/**
	 * @param fareTypeTO
	 * @param chargeBasis
	 * @return
	 */
	private static BigDecimal getHandlingFeeAmountToReservation(FareTypeTO fareTypeTO, ExternalChgDTO externalCharge) {
		BigDecimal totalAmount = BigDecimal.ZERO;
		BigDecimal fareAmount = BigDecimal.ZERO;

		if (fareTypeTO != null && externalCharge != null) {

			totalAmount = BigDecimal.ZERO;
			fareAmount = BigDecimal.ZERO;

			switch (externalCharge.getChargeBasis()) {

			case PERCENTAGE_OF_TOTAL_FARE:
				fareAmount = fareTypeTO.getTotalBaseFare();
				break;
			case PERCENTAGE_OF_TOTAL_FARE_TAX_SURCHARGES:
				fareAmount = fareTypeTO.getTotalPrice();
				break;
			}
			totalAmount = AccelAeroCalculator.divide(AccelAeroCalculator.multiply(fareAmount, externalCharge.getRatioValue()),
					100);
		}
		return totalAmount;

	}

	private static BigDecimal getHandlingFeeAmountToAdultChild(FareTypeTO fareTypeTO, ExternalChgDTO externalCharge) {
		BigDecimal totalAmount = BigDecimal.ZERO;
		if (fareTypeTO != null && externalCharge != null) {

			for (PerPaxPriceInfoTO perPax : fareTypeTO.getPerPaxPriceInfo()) {

				if (perPax.getPassengerType().equals(PaxTypeTO.INFANT)) {
					continue;
				}
				BigDecimal perPaxFareAmount = BigDecimal.ZERO;

				switch (externalCharge.getChargeBasis()) {

				case PERCENTAGE_OF_TOTAL_FARE:
					perPaxFareAmount = perPax.getPassengerPrice().getTotalBaseFare();
					break;
				case PERCENTAGE_OF_TOTAL_FARE_TAX_SURCHARGES:
					perPaxFareAmount = perPax.getPassengerPrice().getTotalPrice();
					break;
				}
				totalAmount = AccelAeroCalculator.add(
						totalAmount,
						AccelAeroCalculator.multiply(perPaxFareAmount,
								AccelAeroCalculator.divide(externalCharge.getRatioValue(), 100)));
			}

		}
		return totalAmount;

	}

	/**
	 * @param paxSet
	 * @param priceInfoTO
	 * @param externalCharge
	 */
	private static void calcualteExtrenalChargeByValue(PaxSet paxSet, PriceInfoTO priceInfoTO, ExternalChgDTO externalCharge)
			throws ModuleException {

		BigDecimal totalAmount = BigDecimal.ZERO;

		switch (externalCharge.getAppliesTo()) {
		case ChargeApplicabilityTO.APPLICABLE_TO_RESERVATION:
			totalAmount = externalCharge.getRatioValue();
			break;
		case ChargeApplicabilityTO.APPLICABLE_TO_ADULT_CHILD:
			totalAmount = AccelAeroCalculator.multiply(externalCharge.getRatioValue(),
					(paxSet.getNoOfAdluts() + paxSet.getNoOfChildren()));
			break;
		}
		if (externalCharge.getAppliesTo() != ChargeApplicabilityTO.APPLICABLE_TO_RESERVATION) {
			totalAmount = getApplicableAmount(externalCharge.getChargeValueMin(), externalCharge.getChargeValueMax(), totalAmount);
		}

		externalCharge.setTotalAmount(AccelAeroCalculator.parseBigDecimal(totalAmount.doubleValue()));
	}

	// ///////////// webservice related utils //////////////////////////

	/**
	 * @param ondFareDTOs
	 * @param paxSet
	 * @param externalCharge
	 * @param returnJourney
	 * @throws ModuleException
	 */
	public static void calculateExternalChargeAmount(Collection<OndFareDTO> ondFareDTOs, PaxSet paxSet,
			ExternalChgDTO externalCharge, boolean returnJourney) throws ModuleException {

		if (externalCharge != null) {

			externalCharge.setApplicableChargeRate(returnJourney ? ChargeRateJourneyType.RETURN : ChargeRateJourneyType.ONEWAY);

			if (externalCharge.isValid() && externalCharge.getRatioValue() != null
					&& externalCharge.getRatioValue().compareTo(BigDecimal.ZERO) > 0) {
				if (externalCharge.isRatioValueInPercentage()) {
					ExternalChargeUtil.calcualteExtrenalChargeByPercentage(ondFareDTOs, externalCharge, paxSet);
				} else {
					ExternalChargeUtil.calcualteExtrenalChargeByValue(paxSet, externalCharge);
				}
			}
		}
	}

	/**
	 * 
	 * @param quotedPriceInfoTO
	 * @param paxSet
	 * @param externalCharge
	 * @throws ModuleException
	 */
	public static void calculateExternalChargeAmount(QuotedPriceInfoTO quotedPriceInfoTO, PaxSet paxSet,
			ExternalChgDTO externalCharge) throws ModuleException {

		if (externalCharge != null) {

			externalCharge.setApplicableChargeRate(quotedPriceInfoTO.isReturnQuote()
					? ChargeRateJourneyType.RETURN
					: ChargeRateJourneyType.ONEWAY);

			if (externalCharge.isValid() && externalCharge.getRatioValue() != null
					&& externalCharge.getRatioValue().compareTo(BigDecimal.ZERO) > 0) {
				if (externalCharge.isRatioValueInPercentage()) {
					ExternalChargeUtil.calcualteExtrenalChargeByPercentage(quotedPriceInfoTO, externalCharge, paxSet);
				} else {
					ExternalChargeUtil.calcualteExtrenalChargeByValue(paxSet, externalCharge);
				}
			}
		}
	}

	/**
	 * @param ondFareDTOs
	 * @param externalCharge
	 * @param paxSet
	 * @throws ModuleException
	 */
	private static void calcualteExtrenalChargeByPercentage(Collection<OndFareDTO> ondFareDTOs, ExternalChgDTO externalCharge,
			PaxSet paxSet) throws ModuleException {

		BigDecimal totalAmount = BigDecimal.ZERO;

		switch (externalCharge.getAppliesTo()) {

		case ChargeApplicabilityTO.APPLICABLE_TO_RESERVATION:
			totalAmount = getHandlingFeeAmountToReservation(ondFareDTOs, externalCharge, paxSet);
			break;
		case ChargeApplicabilityTO.APPLICABLE_TO_ADULT_CHILD:
			totalAmount = getHandlingFeeAmountToAdultChild(ondFareDTOs, externalCharge, paxSet);
			break;
		}

		if (externalCharge.isChargeRateRoundingApplicable()) {
			BigDecimal roundedValue = AccelAeroRounderPolicy.getRoundedValue(totalAmount, externalCharge.getBoundryValue(),
					externalCharge.getBreakPoint());
			totalAmount = roundedValue;
		}

		totalAmount = getApplicableAmount(externalCharge.getChargeValueMin(), externalCharge.getChargeValueMax(), totalAmount);
		externalCharge.setAmount(AccelAeroCalculator.parseBigDecimal(totalAmount.doubleValue()));

	}

	/**
	 * 
	 * @param quotedPriceInfoTO
	 * @param externalCharge
	 * @param paxSet
	 * @throws ModuleException
	 */
	private static void calcualteExtrenalChargeByPercentage(QuotedPriceInfoTO quotedPriceInfoTO, ExternalChgDTO externalCharge,
			PaxSet paxSet) throws ModuleException {

		BigDecimal totalAmount = BigDecimal.ZERO;

		switch (externalCharge.getAppliesTo()) {

		case ChargeApplicabilityTO.APPLICABLE_TO_RESERVATION:
			totalAmount = getHandlingFeeAmountToReservation(quotedPriceInfoTO, externalCharge);
			break;
		case ChargeApplicabilityTO.APPLICABLE_TO_ADULT_CHILD:
			totalAmount = getHandlingFeeAmountToAdultChild(quotedPriceInfoTO, externalCharge, paxSet);
			break;
		}

		if (externalCharge.isChargeRateRoundingApplicable()) {
			BigDecimal roundedValue = AccelAeroRounderPolicy.getRoundedValue(totalAmount, externalCharge.getBoundryValue(),
					externalCharge.getBreakPoint());
			totalAmount = roundedValue;
		}

		totalAmount = getApplicableAmount(externalCharge.getChargeValueMin(), externalCharge.getChargeValueMax(), totalAmount);
		externalCharge.setAmount(AccelAeroCalculator.parseBigDecimal(totalAmount.doubleValue()));

	}

	/**
	 * @param ondFareDTOs
	 * @param externalCharge
	 * @param paxSet
	 * @return
	 * @throws ModuleException
	 */
	private static BigDecimal getHandlingFeeAmountToAdultChild(Collection<OndFareDTO> ondFareDTOs, ExternalChgDTO externalCharge,
			PaxSet paxSet) throws ModuleException {
		BigDecimal totalAmount = BigDecimal.ZERO;
		BigDecimal ratiovalue = AccelAeroCalculator.divide(externalCharge.getRatioValue(), 100);

		switch (externalCharge.getChargeBasis()) {

		case PERCENTAGE_OF_TOTAL_FARE:
			if (paxSet.getNoOfAdluts() > 0) {
				totalAmount = AccelAeroCalculator.add(totalAmount, AccelAeroCalculator.multiply(
						AccelAeroCalculator.multiply(PriceUtil.getPaxBaseFare(ondFareDTOs, PaxTypeTO.ADULT), ratiovalue),
						paxSet.getNoOfAdluts()));
			}
			if (paxSet.getNoOfChildren() > 0) {
				totalAmount = AccelAeroCalculator.add(totalAmount, AccelAeroCalculator.multiply(
						AccelAeroCalculator.multiply(PriceUtil.getPaxBaseFare(ondFareDTOs, PaxTypeTO.CHILD), ratiovalue),
						paxSet.getNoOfChildren()));

			}
			break;

		case PERCENTAGE_OF_TOTAL_FARE_TAX_SURCHARGES:

			if (paxSet.getNoOfAdluts() > 0) {
				totalAmount = AccelAeroCalculator.add(totalAmount, AccelAeroCalculator.multiply(
						AccelAeroCalculator.multiply(PriceUtil.getPaxTotalFare(ondFareDTOs, PaxTypeTO.ADULT), ratiovalue),
						paxSet.getNoOfAdluts()));
			}
			if (paxSet.getNoOfChildren() > 0) {
				totalAmount = AccelAeroCalculator.add(totalAmount, AccelAeroCalculator.multiply(
						AccelAeroCalculator.multiply(PriceUtil.getPaxTotalFare(ondFareDTOs, PaxTypeTO.CHILD), ratiovalue),
						paxSet.getNoOfChildren()));

			}

			break;
		}
		return totalAmount;
	}

	/**
	 * 
	 * @param quotedPriceInfoTO
	 * @param externalCharge
	 * @param paxSet
	 * @return
	 * @throws ModuleException
	 */
	private static BigDecimal getHandlingFeeAmountToAdultChild(QuotedPriceInfoTO quotedPriceInfoTO,
			ExternalChgDTO externalCharge, PaxSet paxSet) throws ModuleException {
		BigDecimal totalAmount = BigDecimal.ZERO;
		BigDecimal ratiovalue = AccelAeroCalculator.divide(externalCharge.getRatioValue(), 100);

		PerPaxChargesTO adultPerPaxChargesTO = quotedPriceInfoTO.getPerPaxChargeByPaxType(CommonUtil
				.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
		PerPaxChargesTO childPerPaxChargesTO = quotedPriceInfoTO.getPerPaxChargeByPaxType(CommonUtil
				.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));

		switch (externalCharge.getChargeBasis()) {

		case PERCENTAGE_OF_TOTAL_FARE:

			if (paxSet.getNoOfAdluts() > 0 && adultPerPaxChargesTO != null) {
				totalAmount = AccelAeroCalculator.add(
						totalAmount,
						AccelAeroCalculator.multiply(
								AccelAeroCalculator.multiply(adultPerPaxChargesTO.getTotalBaseFare(), ratiovalue),
								paxSet.getNoOfAdluts()));
			}
			if (paxSet.getNoOfChildren() > 0 && childPerPaxChargesTO != null) {
				totalAmount = AccelAeroCalculator.add(
						totalAmount,
						AccelAeroCalculator.multiply(
								AccelAeroCalculator.multiply(childPerPaxChargesTO.getTotalBaseFare(), ratiovalue),
								paxSet.getNoOfChildren()));

			}
			break;

		case PERCENTAGE_OF_TOTAL_FARE_TAX_SURCHARGES:

			if (paxSet.getNoOfAdluts() > 0 && adultPerPaxChargesTO != null) {
				totalAmount = AccelAeroCalculator.add(
						totalAmount,
						AccelAeroCalculator.multiply(
								AccelAeroCalculator.multiply(adultPerPaxChargesTO.getTotalFare(), ratiovalue),
								paxSet.getNoOfAdluts()));
			}
			if (paxSet.getNoOfChildren() > 0 && childPerPaxChargesTO != null) {
				totalAmount = AccelAeroCalculator.add(
						totalAmount,
						AccelAeroCalculator.multiply(
								AccelAeroCalculator.multiply(childPerPaxChargesTO.getTotalFare(), ratiovalue),
								paxSet.getNoOfChildren()));

			}

			break;
		}
		return totalAmount;
	}

	/**
	 * @param ondFareDTOs
	 * @param externalCharge
	 * @param paxSet
	 * @return
	 * @throws ModuleException
	 */
	private static BigDecimal getHandlingFeeAmountToReservation(Collection<OndFareDTO> ondFareDTOs,
			ExternalChgDTO externalCharge, PaxSet paxSet) throws ModuleException {

		BigDecimal totalAmount = BigDecimal.ZERO;
		BigDecimal ratiovalue = AccelAeroCalculator.divide(externalCharge.getRatioValue(), 100);

		switch (externalCharge.getChargeBasis()) {

		case PERCENTAGE_OF_TOTAL_FARE_TAX_SURCHARGES:
			totalAmount = AccelAeroCalculator.multiply(PriceUtil.getTotalQuotePrice(ondFareDTOs, paxSet), ratiovalue);
			break;

		case PERCENTAGE_OF_TOTAL_FARE:
			totalAmount = AccelAeroCalculator.multiply(PriceUtil.getTotalBaseFare(ondFareDTOs, paxSet), ratiovalue);
			break;
		}

		return totalAmount;
	}

	/**
	 * 
	 * @param quotedPriceInfoTO
	 * @param externalCharge
	 * @param paxSet
	 * @return
	 * @throws ModuleException
	 */
	private static BigDecimal
			getHandlingFeeAmountToReservation(QuotedPriceInfoTO quotedPriceInfoTO, ExternalChgDTO externalCharge)
					throws ModuleException {

		BigDecimal totalAmount = BigDecimal.ZERO;
		BigDecimal ratiovalue = AccelAeroCalculator.divide(externalCharge.getRatioValue(), 100);

		switch (externalCharge.getChargeBasis()) {

		case PERCENTAGE_OF_TOTAL_FARE_TAX_SURCHARGES:
			totalAmount = AccelAeroCalculator.multiply(quotedPriceInfoTO.getTotalTicketPrice(), ratiovalue);
			break;

		case PERCENTAGE_OF_TOTAL_FARE:
			totalAmount = AccelAeroCalculator.multiply(quotedPriceInfoTO.getTotalBaseFare(), ratiovalue);
			break;
		}

		return totalAmount;
	}

	/**
	 * @param paxSet
	 * @param externalCharge
	 * @throws ModuleException
	 */
	private static void calcualteExtrenalChargeByValue(PaxSet paxSet, ExternalChgDTO externalCharge) throws ModuleException {

		BigDecimal totalAmount = BigDecimal.ZERO;

		switch (externalCharge.getAppliesTo()) {
		case ChargeApplicabilityTO.APPLICABLE_TO_RESERVATION:
			totalAmount = externalCharge.getRatioValue();
			break;
		case ChargeApplicabilityTO.APPLICABLE_TO_ADULT_CHILD:
			totalAmount = AccelAeroCalculator.multiply(externalCharge.getRatioValue(),
					(paxSet.getNoOfAdluts() + paxSet.getNoOfChildren()));
			break;
		}

		if (externalCharge.getAppliesTo() != ChargeApplicabilityTO.APPLICABLE_TO_RESERVATION) {
			totalAmount = getApplicableAmount(externalCharge.getChargeValueMin(), externalCharge.getChargeValueMax(), totalAmount);
		}

		externalCharge.setAmount(AccelAeroCalculator.parseBigDecimal(totalAmount.doubleValue()));

	}

	/**
	 * @param minMargin
	 * @param maxMargin
	 * @param chargeAmount
	 * @return
	 */
	private static BigDecimal getApplicableAmount(BigDecimal minMargin, BigDecimal maxMargin, BigDecimal chargeAmount) {

		if (minMargin != null && chargeAmount.compareTo(minMargin) < 0) {
			chargeAmount = minMargin;
		} else if (maxMargin != null && chargeAmount.compareTo(maxMargin) > 0) {
			chargeAmount = maxMargin;
		}
		return chargeAmount;
	}

	public static void injectOndBaggageGroupId(Collection<ReservationPaxTO> colReservationPaxTO,
			List<FlightSegmentTO> flightSegmentTOs) {

		if (colReservationPaxTO != null && colReservationPaxTO != null) {
			Map<String, String> mapFlightRefAndOndBaggageId = new HashMap<String, String>();

			for (ReservationPaxTO reservationPaxTO : colReservationPaxTO) {
				if (reservationPaxTO != null && reservationPaxTO.getMapFlightRefAndOndBaggageId() != null) {
					mapFlightRefAndOndBaggageId.putAll(reservationPaxTO.getMapFlightRefAndOndBaggageId());
				}
			}

			if (mapFlightRefAndOndBaggageId.size() > 0) {
				for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
					String baggageOndGroupId = mapFlightRefAndOndBaggageId.get(flightSegmentTO.getFlightRefNumber());
					flightSegmentTO.setBaggageONDGroupId(baggageOndGroupId);
				}
			}
		}
	}

	public static void setPaxEffectiveExternalChargeTaxAmount(ReservationPaxTO resPaxTO, BigDecimal taxRatio,
			String flightRefNumber, Map<Integer, Map<EXTERNAL_CHARGES, BigDecimal>> paxWiseModifyingSegAnciTotal) {
		BigDecimal anciPaxTotal = resPaxTO.getAncillaryTotal(true);

		if (anciPaxTotal != null
				&& AccelAeroCalculator.isGreaterThan(anciPaxTotal, AccelAeroCalculator.getDefaultBigDecimalZero())) {
			String taxApplicableCarrier = FlightRefNumberUtil.getOperatingAirline(flightRefNumber);
			BigDecimal effectiveAnciChargeTotal = AccelAeroCalculator.getDefaultBigDecimalZero();

			Map<EXTERNAL_CHARGES, BigDecimal> anciWiseTotal = null;
			if (paxWiseModifyingSegAnciTotal != null && paxWiseModifyingSegAnciTotal.containsKey(resPaxTO.getSeqNumber())) {
				anciWiseTotal = paxWiseModifyingSegAnciTotal.get(resPaxTO.getSeqNumber());
			}

			BigDecimal addedAnciTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal removedAnciTotal = AccelAeroCalculator.getDefaultBigDecimalZero();

			BigDecimal anciSeatCharge = resPaxTO.getAncillaryCharge(EXTERNAL_CHARGES.SEAT_MAP, taxApplicableCarrier);
			BigDecimal remAnciSeatCharge = resPaxTO.getRemovedAncillaryCharge(EXTERNAL_CHARGES.SEAT_MAP, taxApplicableCarrier);
			remAnciSeatCharge = addModifyingSegAnciTotal(anciWiseTotal, EXTERNAL_CHARGES.SEAT_MAP, remAnciSeatCharge);

			addedAnciTotal = AccelAeroCalculator.add(addedAnciTotal, anciSeatCharge);
			removedAnciTotal = AccelAeroCalculator.add(removedAnciTotal, remAnciSeatCharge);

			BigDecimal anciMealCharge = resPaxTO.getAncillaryCharge(EXTERNAL_CHARGES.MEAL, taxApplicableCarrier);
			BigDecimal remAnciMealCharge = resPaxTO.getRemovedAncillaryCharge(EXTERNAL_CHARGES.MEAL, taxApplicableCarrier);
			remAnciMealCharge = addModifyingSegAnciTotal(anciWiseTotal, EXTERNAL_CHARGES.MEAL, remAnciMealCharge);

			addedAnciTotal = AccelAeroCalculator.add(addedAnciTotal, anciMealCharge);
			removedAnciTotal = AccelAeroCalculator.add(removedAnciTotal, remAnciMealCharge);

			BigDecimal anciBaggageCharge = resPaxTO.getAncillaryCharge(EXTERNAL_CHARGES.BAGGAGE, taxApplicableCarrier);
			BigDecimal remAnciBaggageCharge = resPaxTO.getRemovedAncillaryCharge(EXTERNAL_CHARGES.BAGGAGE, taxApplicableCarrier);
			remAnciBaggageCharge = addModifyingSegAnciTotal(anciWiseTotal, EXTERNAL_CHARGES.BAGGAGE, remAnciBaggageCharge);

			addedAnciTotal = AccelAeroCalculator.add(addedAnciTotal, anciBaggageCharge);
			removedAnciTotal = AccelAeroCalculator.add(removedAnciTotal, remAnciBaggageCharge);

			BigDecimal anciInflightCharge = resPaxTO.getAncillaryCharge(EXTERNAL_CHARGES.INFLIGHT_SERVICES, taxApplicableCarrier);
			BigDecimal remAnciInflightCharge = resPaxTO.getRemovedAncillaryCharge(EXTERNAL_CHARGES.INFLIGHT_SERVICES,
					taxApplicableCarrier);
			remAnciInflightCharge = addModifyingSegAnciTotal(anciWiseTotal, EXTERNAL_CHARGES.INFLIGHT_SERVICES,
					remAnciInflightCharge);

			addedAnciTotal = AccelAeroCalculator.add(addedAnciTotal, anciInflightCharge);
			removedAnciTotal = AccelAeroCalculator.add(removedAnciTotal, remAnciInflightCharge);

			BigDecimal anciAirportServiceCharge = resPaxTO.getAncillaryCharge(EXTERNAL_CHARGES.AIRPORT_SERVICE,
					taxApplicableCarrier);
			BigDecimal remAnciAirportServiceCharge = resPaxTO.getRemovedAncillaryCharge(EXTERNAL_CHARGES.AIRPORT_SERVICE,
					taxApplicableCarrier);
			remAnciAirportServiceCharge = addModifyingSegAnciTotal(anciWiseTotal, EXTERNAL_CHARGES.AIRPORT_SERVICE,
					remAnciAirportServiceCharge);

			addedAnciTotal = AccelAeroCalculator.add(addedAnciTotal, anciAirportServiceCharge);
			removedAnciTotal = AccelAeroCalculator.add(removedAnciTotal, remAnciAirportServiceCharge);

			BigDecimal anciAirportTransferCharge = resPaxTO.getAncillaryCharge(EXTERNAL_CHARGES.AIRPORT_TRANSFER,
					taxApplicableCarrier);
			BigDecimal remAnciAirportTransferCharge = resPaxTO.getRemovedAncillaryCharge(EXTERNAL_CHARGES.AIRPORT_TRANSFER,
					taxApplicableCarrier);
			remAnciAirportTransferCharge = addModifyingSegAnciTotal(anciWiseTotal, EXTERNAL_CHARGES.AIRPORT_TRANSFER,
					remAnciAirportTransferCharge);

			addedAnciTotal = AccelAeroCalculator.add(addedAnciTotal, anciAirportTransferCharge);
			removedAnciTotal = AccelAeroCalculator.add(removedAnciTotal, remAnciAirportTransferCharge);

			BigDecimal anciAutoCheckinCharge = resPaxTO.getAncillaryCharge(EXTERNAL_CHARGES.AUTOMATIC_CHECKIN,
					taxApplicableCarrier);
			BigDecimal remAnciAutoCheckinCharge = resPaxTO.getRemovedAncillaryCharge(EXTERNAL_CHARGES.AUTOMATIC_CHECKIN,
					taxApplicableCarrier);
			remAnciMealCharge = addModifyingSegAnciTotal(anciWiseTotal, EXTERNAL_CHARGES.AUTOMATIC_CHECKIN,
					remAnciAutoCheckinCharge);

			addedAnciTotal = AccelAeroCalculator.add(addedAnciTotal, anciAutoCheckinCharge);
			removedAnciTotal = AccelAeroCalculator.add(removedAnciTotal, remAnciAutoCheckinCharge);

			BigDecimal anciInsuranceCharge = resPaxTO.getAncillaryCharge(EXTERNAL_CHARGES.INSURANCE, taxApplicableCarrier);
			BigDecimal remAnciInsuranceCharge = resPaxTO.getRemovedAncillaryCharge(EXTERNAL_CHARGES.INSURANCE,
					taxApplicableCarrier);
			remAnciInsuranceCharge = addModifyingSegAnciTotal(anciWiseTotal, EXTERNAL_CHARGES.INSURANCE, remAnciInsuranceCharge);

			addedAnciTotal = AccelAeroCalculator.add(addedAnciTotal, anciInsuranceCharge);
			removedAnciTotal = AccelAeroCalculator.add(removedAnciTotal, remAnciInsuranceCharge);

			BigDecimal flexiCharge = resPaxTO.getAncillaryCharge(EXTERNAL_CHARGES.FLEXI_CHARGES, taxApplicableCarrier);

			addedAnciTotal = AccelAeroCalculator.add(addedAnciTotal, flexiCharge);

			if (AccelAeroCalculator.isGreaterThan(addedAnciTotal, removedAnciTotal)) {
				effectiveAnciChargeTotal = AccelAeroCalculator.subtract(addedAnciTotal, removedAnciTotal);
			}

			if (AccelAeroCalculator.isGreaterThan(effectiveAnciChargeTotal, AccelAeroCalculator.getDefaultBigDecimalZero())) {
				BigDecimal anciTax = AccelAeroCalculator.multiplyDefaultScale(effectiveAnciChargeTotal, taxRatio);
				LCCClientExternalChgDTO chgDTO = new LCCClientExternalChgDTO();
				chgDTO.setExternalCharges(EXTERNAL_CHARGES.JN_ANCI);
				chgDTO.setAmount(anciTax);
				chgDTO.setFlightRefNumber(flightRefNumber);
				chgDTO.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(flightRefNumber));
				resPaxTO.addExternalCharges(chgDTO);
			}
		}

	}

	public static Map<Integer, Map<EXTERNAL_CHARGES, BigDecimal>> getPaxWiseModifyingSegmentAnciTotal(
			List<FlightSegmentTO> flightSegmentTOs, List<String> modifyingFlightList,
			Collection<LCCClientReservationPax> colPassengers) {
		Map<Integer, Map<EXTERNAL_CHARGES, BigDecimal>> paxWiseModifyingAnciTotal = null;
		String taxApplicablCarrier = FlightRefNumberUtil.getOperatingAirline(flightSegmentTOs.get(0).getFlightRefNumber());
		if (ReservationUtil.isDateModification(flightSegmentTOs, modifyingFlightList)) {
			paxWiseModifyingAnciTotal = new HashMap<Integer, Map<EXTERNAL_CHARGES, BigDecimal>>();
			for (LCCClientReservationPax lccPax : colPassengers) {
				List<LCCSelectedSegmentAncillaryDTO> selAnciList = lccPax.getSelectedAncillaries();
				if (selAnciList != null && !selAnciList.isEmpty()) {
					for (LCCSelectedSegmentAncillaryDTO selAnciDto : selAnciList) {
						FlightSegmentTO flightSegmentTO = selAnciDto.getFlightSegmentTO();
						if (modifyingFlightList.contains(flightSegmentTO.getFlightRefNumber())
								&& taxApplicablCarrier.equals(FlightRefNumberUtil.getOperatingAirline(flightSegmentTO
										.getFlightRefNumber()))) {
							if (!paxWiseModifyingAnciTotal.containsKey(lccPax.getPaxSequence())) {
								paxWiseModifyingAnciTotal.put(lccPax.getPaxSequence(),
										new HashMap<ReservationInternalConstants.EXTERNAL_CHARGES, BigDecimal>());
							}

							Map<EXTERNAL_CHARGES, BigDecimal> anciWiseTotal = paxWiseModifyingAnciTotal.get(lccPax
									.getPaxSequence());

							if (selAnciDto.getAirSeatDTO() != null && selAnciDto.getAirSeatDTO().getSeatCharge() != null) {
								BigDecimal currSeatTotal = getCurrentPaxAnciCharge(anciWiseTotal, EXTERNAL_CHARGES.SEAT_MAP);
								currSeatTotal = AccelAeroCalculator
										.add(currSeatTotal, selAnciDto.getAirSeatDTO().getSeatCharge());
								anciWiseTotal.put(EXTERNAL_CHARGES.SEAT_MAP, currSeatTotal);
							}

							if (selAnciDto.getMealDTOs() != null && !selAnciDto.getMealDTOs().isEmpty()) {
								BigDecimal mealCharge = getCurrentPaxAnciCharge(anciWiseTotal, EXTERNAL_CHARGES.MEAL);
								for (LCCMealDTO mealDto : selAnciDto.getMealDTOs()) {
									mealCharge = AccelAeroCalculator.add(mealCharge,
											AccelAeroCalculator.multiply(mealDto.getMealCharge(), mealDto.getSoldMeals()));
								}

								anciWiseTotal.put(EXTERNAL_CHARGES.MEAL, mealCharge);
							}

							if (selAnciDto.getBaggageDTOs() != null && !selAnciDto.getBaggageDTOs().isEmpty()) {
								BigDecimal bgChg = getCurrentPaxAnciCharge(anciWiseTotal, EXTERNAL_CHARGES.BAGGAGE);
								for (LCCBaggageDTO bgDto : selAnciDto.getBaggageDTOs()) {
									bgChg = AccelAeroCalculator.add(bgChg, bgDto.getBaggageCharge());
								}

								anciWiseTotal.put(EXTERNAL_CHARGES.BAGGAGE, bgChg);
							}

							if (selAnciDto.getInsuranceQuotations() != null && !selAnciDto.getInsuranceQuotations().isEmpty()) {
								BigDecimal currInsChg = getCurrentPaxAnciCharge(anciWiseTotal, EXTERNAL_CHARGES.INSURANCE);
								for (LCCInsuranceQuotationDTO quotation : selAnciDto.getInsuranceQuotations()) {
									currInsChg = AccelAeroCalculator.add(currInsChg, quotation.getTotalPerPaxPremiumAmount());
								}
								anciWiseTotal.put(EXTERNAL_CHARGES.INSURANCE, currInsChg);
							}

							if (selAnciDto.getAirportServiceDTOs() != null && !selAnciDto.getAirportServiceDTOs().isEmpty()) {
								BigDecimal airportServiceChg = getCurrentPaxAnciCharge(anciWiseTotal,
										EXTERNAL_CHARGES.AIRPORT_SERVICE);
								for (LCCAirportServiceDTO apsDto : selAnciDto.getAirportServiceDTOs()) {
									airportServiceChg = AccelAeroCalculator.add(airportServiceChg, apsDto.getServiceCharge());
								}

								anciWiseTotal.put(EXTERNAL_CHARGES.AIRPORT_SERVICE, airportServiceChg);
							}

							if (selAnciDto.getAirportTransferDTOs() != null && !selAnciDto.getAirportTransferDTOs().isEmpty()) {
								BigDecimal airportTransferChg = getCurrentPaxAnciCharge(anciWiseTotal,
										EXTERNAL_CHARGES.AIRPORT_TRANSFER);
								for (LCCAirportServiceDTO transferDto : selAnciDto.getAirportTransferDTOs()) {
									airportTransferChg = AccelAeroCalculator.add(airportTransferChg,
											transferDto.getServiceCharge());
								}

								anciWiseTotal.put(EXTERNAL_CHARGES.AIRPORT_TRANSFER, airportTransferChg);
							}

							if (selAnciDto.getAutomaticCheckinDTOs() != null && !selAnciDto.getAutomaticCheckinDTOs().isEmpty()) {
								BigDecimal currAutoCheckinCharge = getCurrentPaxAnciCharge(anciWiseTotal, EXTERNAL_CHARGES.AUTOMATIC_CHECKIN);
								for (LCCAutomaticCheckinDTO autoCheckinDTO : selAnciDto.getAutomaticCheckinDTOs()) {
									currAutoCheckinCharge = AccelAeroCalculator.add(currAutoCheckinCharge,
											autoCheckinDTO.getAutomaticCheckinCharge());
								}

								anciWiseTotal.put(EXTERNAL_CHARGES.AUTOMATIC_CHECKIN, currAutoCheckinCharge);
							}

							if (selAnciDto.getSpecialServiceRequestDTOs() != null
									&& !selAnciDto.getSpecialServiceRequestDTOs().isEmpty()) {
								BigDecimal ssrChg = getCurrentPaxAnciCharge(anciWiseTotal, EXTERNAL_CHARGES.INFLIGHT_SERVICES);
								for (LCCSpecialServiceRequestDTO ssrDto : selAnciDto.getSpecialServiceRequestDTOs()) {
									ssrChg = AccelAeroCalculator.add(ssrChg, ssrDto.getCharge());
								}

								anciWiseTotal.put(EXTERNAL_CHARGES.INFLIGHT_SERVICES, ssrChg);
							}
						}
					}
				}
			}
		}
		return paxWiseModifyingAnciTotal;
	}

	private static BigDecimal getCurrentPaxAnciCharge(Map<EXTERNAL_CHARGES, BigDecimal> anciWiseTotal, EXTERNAL_CHARGES extChg) {
		if (!anciWiseTotal.containsKey(extChg)) {
			anciWiseTotal.put(extChg, AccelAeroCalculator.getDefaultBigDecimalZero());
		}
		return anciWiseTotal.get(extChg);
	}

	private static BigDecimal addModifyingSegAnciTotal(Map<EXTERNAL_CHARGES, BigDecimal> anciWiseTotal, EXTERNAL_CHARGES extChg,
			BigDecimal removeAnciChg) {
		if (anciWiseTotal != null && anciWiseTotal.containsKey(extChg)) {
			removeAnciChg = AccelAeroCalculator.add(removeAnciChg, anciWiseTotal.get(extChg));
		}

		return removeAnciChg;
	}
}
