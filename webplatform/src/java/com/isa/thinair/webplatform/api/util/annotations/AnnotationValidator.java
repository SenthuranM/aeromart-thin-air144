package com.isa.thinair.webplatform.api.util.annotations;

import java.lang.reflect.Method;

import com.isa.thinair.webplatform.api.util.annotations.IGNORE.TYPE;

public class AnnotationValidator {

	public String getValidatedValue(String obj, Method invM) {
		if (obj != null && invM != null) {
			if (invM.isAnnotationPresent(TRIM.class)) {
				obj = obj.toString().trim();
			}

			if (invM.isAnnotationPresent(IGNORE.class)) {
				TYPE[] values = invM.getAnnotation(IGNORE.class).value();
				if (values != null) {
					for (TYPE value : values) {
						if (TYPE.JS_STR_NULL == value && "NULL".compareToIgnoreCase(obj.toString()) == 0) {
							obj = null;
						}
						if (TYPE.JS_UNDEFINED == value && "UNDEFINED".compareToIgnoreCase(obj.toString()) == 0) {
							obj = null;
						}
					}
				}
			}
		}
		return obj;
	}
}
