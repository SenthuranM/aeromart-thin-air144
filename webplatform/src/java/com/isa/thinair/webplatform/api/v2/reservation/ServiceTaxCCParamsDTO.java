package com.isa.thinair.webplatform.api.v2.reservation;

import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;

public class ServiceTaxCCParamsDTO {

	private FlightSearchDTO searchParams;

	private FlightSearchDTO fareQuoteParams;

	private String paxWiseAnci;

	private String resContactInfo;

	private String paxState;

	private String paxCountryCode;

	private boolean paxTaxRegistered;

	private String flightRPHList;

	private String selectedFlightList;

	private String flightSegmentToList;
	
	private String nameChangeFlightSegmentList;
	
	private String resPaxs;
	
	private boolean addSegment;

	private String groupPNR;

	public ServiceTaxCCParamsDTO(FlightSearchDTO searchParams, FlightSearchDTO fareQuoteParams, String paxWiseAnci, String resPaxs,
			String resContactInfo, String paxState, String paxCountryCode, boolean paxTaxRegistered, String flightRPHList,
			String selectedFlightList, String flightSegmentToList, String nameChangeFlightSegmentList, boolean addSegment, String groupPNR) {

		this.setSearchParams(searchParams);
		this.setFareQuoteParams(fareQuoteParams);
		this.setPaxWiseAnci(paxWiseAnci);
		this.setResContactInfo(resContactInfo);
		this.setPaxState(paxState);
		this.setPaxCountryCode(paxCountryCode);
		this.setPaxTaxRegistered(paxTaxRegistered);
		this.setFlightRPHList(flightRPHList);
		this.setSelectedFlightList(selectedFlightList);
		this.setFlightSegmentToList(flightSegmentToList);
		this.setNameChangeFlightSegmentList(nameChangeFlightSegmentList);
		this.setResPaxs(resPaxs);
		this.setAddSegment(addSegment);
		this.setGroupPNR(groupPNR);

	}

	public ServiceTaxCCParamsDTO(FlightSearchDTO fareQuoteParams, String paxState, String paxCountryCode,
			boolean paxTaxRegistered, String flightRPHList, String groupPNR) {

		this.setFareQuoteParams(fareQuoteParams);
		this.setPaxState(paxState);
		this.setPaxCountryCode(paxCountryCode);
		this.setPaxTaxRegistered(paxTaxRegistered);
		this.setFlightRPHList(flightRPHList);
		this.setGroupPNR(groupPNR);

	}

	public ServiceTaxCCParamsDTO(FlightSearchDTO fareQuoteParams, String paxWiseAnci, String resContactInfo,
			String flightRPHList, String groupPNR) {

		this.setFareQuoteParams(fareQuoteParams);
		this.setPaxWiseAnci(paxWiseAnci);
		this.setResContactInfo(resContactInfo);
		this.setFlightRPHList(flightRPHList);
		this.setGroupPNR(groupPNR);

	}

	public FlightSearchDTO getSearchParams() {
		return searchParams;
	}

	public void setSearchParams(FlightSearchDTO searchParams) {
		this.searchParams = searchParams;
	}

	public FlightSearchDTO getFareQuoteParams() {
		return fareQuoteParams;
	}

	public void setFareQuoteParams(FlightSearchDTO fareQuoteParams) {
		this.fareQuoteParams = fareQuoteParams;
	}

	public String getPaxWiseAnci() {
		return paxWiseAnci;
	}

	public void setPaxWiseAnci(String paxWiseAnci) {
		this.paxWiseAnci = paxWiseAnci;
	}

	public String getResContactInfo() {
		return resContactInfo;
	}

	public void setResContactInfo(String resContactInfo) {
		this.resContactInfo = resContactInfo;
	}

	public String getPaxState() {
		return paxState;
	}

	public void setPaxState(String paxState) {
		this.paxState = paxState;
	}

	public String getPaxCountryCode() {
		return paxCountryCode;
	}

	public void setPaxCountryCode(String paxCountryCode) {
		this.paxCountryCode = paxCountryCode;
	}

	public boolean isPaxTaxRegistered() {
		return paxTaxRegistered;
	}

	public void setPaxTaxRegistered(boolean paxTaxRegistered) {
		this.paxTaxRegistered = paxTaxRegistered;
	}

	public String getFlightRPHList() {
		return flightRPHList;
	}

	public void setFlightRPHList(String flightRPHList) {
		this.flightRPHList = flightRPHList;
	}

	public String getSelectedFlightList() {
		return selectedFlightList;
	}

	public void setSelectedFlightList(String selectedFlightList) {
		this.selectedFlightList = selectedFlightList;
	}

	public String getFlightSegmentToList() {
		return flightSegmentToList;
	}

	public void setFlightSegmentToList(String flightSegmentToList) {
		this.flightSegmentToList = flightSegmentToList;
	}

	public String getNameChangeFlightSegmentList() {
		return nameChangeFlightSegmentList;
	}

	public void setNameChangeFlightSegmentList(String nameChangeFlightSegmentList) {
		this.nameChangeFlightSegmentList = nameChangeFlightSegmentList;
	}

	public String getResPaxs() {
		return resPaxs;
	}

	public void setResPaxs(String resPaxs) {
		this.resPaxs = resPaxs;
	}

	public String getGroupPNR() {
		return groupPNR;
	}

	public void setGroupPNR(String groupPNR) {
		this.groupPNR = groupPNR;
	}

	public boolean isAddSegment() {
		return addSegment;
	}

	public void setAddSegment(boolean addSegment) {
		this.addSegment = addSegment;
	}

}
