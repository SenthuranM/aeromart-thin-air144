package com.isa.thinair.webplatform.api.v2.ancillary;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.isa.thinair.airinventory.api.util.ONDBaggageUtil;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.PassengerType;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.api.v2.util.InsuranceFltSegBuilder;

public class AncillaryJSONUtil {

	public static List<LCCInsuranceQuotationDTO> getSelectedInsuranceQuotations(String insurances,
			List<FlightSegmentTO> flightSegmentTOs, String insurableFltRefNumbers) throws ModuleException, ParseException {

		List<LCCInsuranceQuotationDTO> insuranceQuotations = new ArrayList<LCCInsuranceQuotationDTO>();

		List<LCCClientReservationInsurance> selectedInsurances = getLCCClientReservationInsurances(insurances, flightSegmentTOs,
				insurableFltRefNumbers);

		if (selectedInsurances != null && !selectedInsurances.isEmpty()) {
			LCCInsuranceQuotationDTO insuranceQuotation = null;

			for (LCCClientReservationInsurance selectedInsurance : selectedInsurances) {
				insuranceQuotation = new LCCInsuranceQuotationDTO();
				insuranceQuotation.setInsuranceRefNumber(selectedInsurance.getInsuranceQuoteRefNumber());
				insuranceQuotation.setInsuredJourney(selectedInsurance.getInsuredJourneyDTO());
				insuranceQuotation.setQuotedTotalPremiumAmount(selectedInsurance.getQuotedTotalPremium());
				insuranceQuotation.setOperatingAirline(selectedInsurance.getOperatingAirline());

				boolean isAllDomastic = true;
				for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
					if (!flightSegmentTO.isDomesticFlight()) {
						isAllDomastic = false;
						break;
					}
				}
				insuranceQuotation.setAllDomastic(isAllDomastic);
				insuranceQuotations.add(insuranceQuotation);
			}

		}
		return insuranceQuotations;
	}

	public static List<LCCClientReservationInsurance> getLCCClientReservationInsurances(String insurances,
			List<FlightSegmentTO> insFltSegments, String insurableFltRefNumbers) throws ParseException, ModuleException {

		List<LCCClientReservationInsurance> selectedInsurances = new ArrayList<LCCClientReservationInsurance>();
		LCCClientReservationInsurance reservationInsurance = null;

		if (isNullSafe(insurances)) {
			JSONParser parser = new JSONParser();
			JSONArray jsonArray = null;
			if (parser.parse(insurances) instanceof JSONArray) {
				jsonArray = (JSONArray) parser.parse(insurances);
			} else {
				jsonArray = new JSONArray();
				jsonArray.add((JSONObject) parser.parse(insurances));
			}

			Iterator<?> insuranceIterator = jsonArray.iterator();

			while (insuranceIterator.hasNext()) {

				JSONObject jSonInsurance = (JSONObject) insuranceIterator.next();

				Object selected = jSonInsurance.get("selected");
				Object quotedTotalPremium = jSonInsurance.get("quotedTotalPremiumAmount");
				Object insuranceRefNumber = jSonInsurance.get("insuranceRefNumber");
				Object operatingAirline = jSonInsurance.get("operatingAirline");

				if (selected.toString().equals("true")) {
					reservationInsurance = new LCCClientReservationInsurance();
					if (isNullSafe(quotedTotalPremium) && isNullSafe(insuranceRefNumber) && isNullSafe(operatingAirline)) {

						reservationInsurance.setQuotedTotalPremium(new BigDecimal(quotedTotalPremium.toString()));
						reservationInsurance.setInsuranceQuoteRefNumber(insuranceRefNumber.toString());
						reservationInsurance.setOperatingAirline(operatingAirline.toString());

						if (insFltSegments != null) {
							Map<String, FlightSegmentTO> fltSegMap = new HashMap<String, FlightSegmentTO>();
							for (FlightSegmentTO fltSeg : insFltSegments) {
								fltSegMap.put(fltSeg.getFlightRefNumber(), fltSeg);
							}
							List<FlightSegmentTO> seletedFltSegList = new ArrayList<FlightSegmentTO>();

							JSONArray fltRefsArray = (JSONArray) parser.parse(insurableFltRefNumbers);
							Iterator<?> iterator = fltRefsArray.iterator();
							while (iterator.hasNext()) {
								String fltRefNo = (String) iterator.next();
								seletedFltSegList.add(fltSegMap.get(fltRefNo));
							}
							InsuranceFltSegBuilder insFltSegBldr = new InsuranceFltSegBuilder(seletedFltSegList);

							String sOffset = AncillaryDTOUtil.getOffset(insFltSegBldr.getJourneyStartDate().getTime()
									- insFltSegBldr.getJourneyStartDateZulu().getTime());
							String eOffset = AncillaryDTOUtil.getOffset(insFltSegBldr.getJourneyEndDate().getTime()
									- insFltSegBldr.getJourneyEndDateZulu().getTime());

							LCCInsuredJourneyDTO jrny = new LCCInsuredJourneyDTO();
							jrny.setJourneyStartAirportCode(insFltSegBldr.getOriginAirport());
							jrny.setJourneyEndAirportCode(insFltSegBldr.getDestinationAirport());
							jrny.setJourneyStartDate(insFltSegBldr.getJourneyStartDate());
							jrny.setJourneyStartDateOffset(sOffset);
							jrny.setJourneyEndDate(insFltSegBldr.getJourneyEndDate());
							jrny.setJourneyEndDateOffset(eOffset);
							jrny.setRoundTrip(insFltSegBldr.isReturnJourney());

							reservationInsurance.setInsuredJourneyDTO(jrny);
						}

						selectedInsurances.add(reservationInsurance);
					}

				}

			}

		}

		return selectedInsurances;
	}

	public static List<LCCInsuranceQuotationDTO> getInsuranceQuotation(String insurance, LCCInsuredJourneyDTO insJrnyDto)
			throws ParseException, ModuleException {

		List<LCCInsuranceQuotationDTO> insuranceQuotations = new ArrayList<LCCInsuranceQuotationDTO>();
		List<LCCClientReservationInsurance> reservationInsurances = new ArrayList<LCCClientReservationInsurance>();
		reservationInsurances = AncillaryJSONUtil.getLCCClientReservationInsurances(insurance, null, null);

		Iterator<LCCClientReservationInsurance> it = reservationInsurances.iterator();
		while (it.hasNext()) {
			LCCInsuranceQuotationDTO insuranceQuotation = new LCCInsuranceQuotationDTO();
			LCCClientReservationInsurance reservationInsurance = it.next();
			reservationInsurance.setInsuredJourneyDTO(insJrnyDto);

			insuranceQuotation = new LCCInsuranceQuotationDTO();
			insuranceQuotation.setInsuranceRefNumber(reservationInsurance.getInsuranceQuoteRefNumber());
			insuranceQuotation.setInsuredJourney(reservationInsurance.getInsuredJourneyDTO());
			insuranceQuotation.setQuotedTotalPremiumAmount(reservationInsurance.getQuotedTotalPremium());
			insuranceQuotation.setOperatingAirline(reservationInsurance.getOperatingAirline());
			insuranceQuotations.add(insuranceQuotation);
		}
		return insuranceQuotations;
	}

	public static List<LCCSegmentSeatDTO> getBlockSeatDTOs(String jsonString) throws ParseException {
		List<LCCSegmentSeatDTO> segSeatList = new ArrayList<LCCSegmentSeatDTO>();

		if (jsonString != null && !jsonString.equals("")) {
			JSONParser parser = new JSONParser();

			JSONArray jsonArray = (JSONArray) parser.parse(jsonString);
			Iterator<?> iterator = jsonArray.iterator();
			while (iterator.hasNext()) {
				JSONObject jObject = (JSONObject) iterator.next();

				String fltRefNo = (String) jObject.get("fltRefNo");

				FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
				flightSegmentTO.setFlightRefNumber(fltRefNo);

				LCCSegmentSeatDTO blockSeatDTO = new LCCSegmentSeatDTO();
				blockSeatDTO.setFlightSegmentTO(flightSegmentTO);

				List<LCCAirSeatDTO> airSeatDTOs = new ArrayList<LCCAirSeatDTO>();

				JSONArray seatsArr = (JSONArray) jObject.get("seats");

				Iterator<?> seatArrItr = seatsArr.iterator();
				while (seatArrItr.hasNext()) {
					JSONObject seatObj = (JSONObject) seatArrItr.next();
					String paxType = (String) seatObj.get("paxType");
					String seatNo = (String) seatObj.get("seatNo");

					LCCAirSeatDTO airSeatDTO = new LCCAirSeatDTO();
					airSeatDTO.setBookedPassengerType(paxType);
					airSeatDTO.setSeatNumber(seatNo);

					airSeatDTOs.add(airSeatDTO);

				}

				blockSeatDTO.setAirSeatDTOs(airSeatDTOs);

				segSeatList.add(blockSeatDTO);
			}
		}
		return segSeatList;
	}

	/**
	 * Parses a json string of Long types i.e.: [123123123,123123123213,123123123123] to a List<Long>
	 * 
	 * @param jsonString
	 *            JSON String to be parsed
	 * @return List<Long> containing the parsed data
	 * @throws ParseException
	 *             if the JSON string is malformed
	 */
	@SuppressWarnings("unchecked")
	public static List<Long> parseLongList(String jsonString) throws ParseException {
		List<Long> resultList = new ArrayList<Long>();
		if (jsonString != null && !jsonString.equals("")) {
			JSONParser parser = new JSONParser();

			JSONArray jsonArray = (JSONArray) parser.parse(jsonString);
			Iterator<Long> iterator = jsonArray.iterator();
			while (iterator.hasNext()) {
				resultList.add(iterator.next());
			}
		}
		return resultList;
	}

	private static Date getDateObj(String strDate) {
		try {
			Date dtReturn = null;
			if (strDate != null && !"".equals(strDate)) {
				int date = Integer.parseInt(strDate.substring(0, 2));
				int month = Integer.parseInt(strDate.substring(3, 5));
				int year = Integer.parseInt(strDate.substring(6, 10));

				Calendar validDate = new GregorianCalendar(year, month - 1, date);
				dtReturn = new Date(validDate.getTime().getTime());
			}
			return dtReturn;
		} catch (Exception e) {
			return null;
		}
	}
	
	private static char getCharValue(String str) {
		char strToChar = '\u0000';
		if (str != null && str.length() > 0) {
			strToChar = str.charAt(0);
		}
		return strToChar;
	}

	private static Date getTimeStampObj(String strDate, String strTime) {
		try {
			Calendar validDate =null;
			Date dtReturn = null;
			if (!strDate.equals("")) {
				int date = Integer.parseInt(strDate.substring(0, 2));
				int month = Integer.parseInt(strDate.substring(3, 5));
				int year = Integer.parseInt(strDate.substring(6, 10));
				
				validDate = new GregorianCalendar(year, month - 1, date);
				
			}
			if (!strTime.equals("") && validDate!= null) {
				Date tmpDate = validDate.getTime();
				Calendar tmpCalendar = Calendar.getInstance();
				tmpCalendar.setTime(tmpDate);
				if (strTime.length() == 4) {
					int hours = Integer.parseInt(strTime.substring(0, 1));
					int minutes = Integer.parseInt(strTime.substring(2, 4));
					tmpCalendar.add(Calendar.HOUR, hours);
					tmpCalendar.add(Calendar.MINUTE, minutes);
					validDate = tmpCalendar;
				} else {
					int hours = Integer.parseInt(strTime.substring(0, 2));
					int minutes = Integer.parseInt(strTime.substring(3, 5));
					tmpCalendar.add(Calendar.HOUR, hours);
					tmpCalendar.add(Calendar.MINUTE, minutes);
					validDate = tmpCalendar;
				}
				
			}
			if (validDate != null) {
				dtReturn = validDate.getTime();
			}
			return dtReturn;
			
		} catch (Exception e) {
			return null;
		}
	}

	public static List<LCCClientReservationPax> extractPaxOnly(String jsonString, ApplicationEngine appEngine)
			throws ParseException {
		List<LCCClientReservationPax> paxList = new ArrayList<LCCClientReservationPax>();
		if (jsonString != null && !"".equals(jsonString)) {
			JSONParser parser = new JSONParser();
			JSONArray jsonArray = (JSONArray) parser.parse(jsonString);
			Iterator<?> iterator = jsonArray.iterator();
			while (iterator.hasNext()) {
				LCCClientReservationPax pax = new LCCClientReservationPax();

				JSONObject jObject = (JSONObject) iterator.next();
				PaxDetailsExtractor paxDetailsEx = new PaxDetailsExtractor(jObject, appEngine);
				String paxType = paxDetailsEx.getPaxType();
				String title = paxDetailsEx.getTitle();
				String firstName = paxDetailsEx.getFirstName();
				String lastName = paxDetailsEx.getLastName();
				String traverRefNo = paxDetailsEx.getTravelerRefNo();
				// Integer nationality = paxDetailsEx.getNationalityIndex();
				// Integer infantWith = paxDetailsEx.getInfantId();
				// Integer seqNumber = paxDetailsEx.getSequenceNumber();
				// String traverRefNo = paxDetailsEx.getTravelerRefNo();
				// Date dob = getBirthDate(paxDetailsEx.getBirthDate());
				// String eTicket = paxDetailsEx.getETicket();
				// String pnrPaxCatFOIDNumber = paxDetailsEx.getPnrPaxCatFOIDNumber();

				pax.setPaxType(paxType);
				pax.setTitle(title);
				pax.setFirstName(firstName);
				pax.setLastName(lastName);
				if (traverRefNo != null && !traverRefNo.isEmpty() && traverRefNo.split("\\$").length > 1) {
					pax.setTravelerRefNumber(traverRefNo.split("\\$")[1]);
				}
				// pax.setDateOfBirth(dob);
				// pax.setNationality(nationality);
				// pax.setInfantWith(infantWith);
				// pax.setSeqNumber(seqNumber);
				// pax.setTravelerRefNumber(traverRefNo);
				// pax.seteTicket(eTicket);
				// pax.setPnrPaxCatFOIDNumber(pnrPaxCatFOIDNumber);

				paxList.add(pax);
			}
		}
		return paxList;
	}
	
	public static List<LCCClientReservationPax> extractPaxDetails(String jsonString, ApplicationEngine appEngine)
            throws ParseException {
        List<LCCClientReservationPax> paxList = new ArrayList<LCCClientReservationPax>();
        if (jsonString != null && !"".equals(jsonString)) {
            JSONParser parser = new JSONParser();
            JSONArray jsonArray = (JSONArray) parser.parse(jsonString);
            Iterator<?> iterator = jsonArray.iterator();
            while (iterator.hasNext()) {
                LCCClientReservationPax pax = new LCCClientReservationPax();

                JSONObject jObject = (JSONObject) iterator.next();
                PaxDetailsExtractor paxDetailsEx = new PaxDetailsExtractor(jObject, appEngine);
                String title = paxDetailsEx.getTitle(); 
                String firstName = paxDetailsEx.getFirstName();
                String lastName = paxDetailsEx.getLastName();
                Integer nationality = paxDetailsEx.getNationalityIndex();
                Date dob = getDateObj(paxDetailsEx.getBirthDate());
                String pnrPaxCatFOIDNumber = paxDetailsEx.getPnrPaxCatFOIDNumber();
                pax.setTitle(title);
                pax.setNationalityCode(nationality);
                pax.setFirstName(firstName);
                pax.setLastName(lastName);
                pax.setDateOfBirth(dob);
                if(pax.getLccClientAdditionPax()!= null){
                	pax.getLccClientAdditionPax().setPassportNo(pnrPaxCatFOIDNumber);   
                }

                paxList.add(pax);
            }
        }
        return paxList;
    }

	public static List<ReservationPaxTO> extractReservationPax(String jsonString,
			List<LCCInsuranceQuotationDTO> insuranceQuotations, ApplicationEngine appEngine,
			List<FlightSegmentTO> insuredFltSegs, String ondBaggage,
			Map<String, Map<EXTERNAL_CHARGES, Integer>> anciOfferTemplates) throws ParseException, ModuleException,
			java.text.ParseException {
		List<ReservationPaxTO> paxList = new ArrayList<ReservationPaxTO>();
		int nonInfants = 0;
		int paxCount = 0;
		if (jsonString != null && !"".equals(jsonString)) {
			JSONParser parser = new JSONParser();
			JSONArray jsonArray = (JSONArray) parser.parse(jsonString);
			Iterator<?> iterator = jsonArray.iterator();

			while (iterator.hasNext()) {
				ReservationPaxTO pax = new ReservationPaxTO();

				JSONObject jObject = (JSONObject) iterator.next();
				PaxDetailsExtractor paxDetailsEx = new PaxDetailsExtractor(jObject, appEngine);
				boolean blnParent = paxDetailsEx.isParent();
				String paxType = paxDetailsEx.getPaxType();
				String title = paxDetailsEx.getTitle();
				String firstName = paxDetailsEx.getFirstName();
				String lastName = paxDetailsEx.getLastName();
				Integer nationality = paxDetailsEx.getNationalityIndex();
				Integer infantWith = paxDetailsEx.getInfantId();
				Integer seqNumber = paxDetailsEx.getSequenceNumber();
				String traverRefNo = paxDetailsEx.getTravelerRefNo();
				Date dob = getDateObj(paxDetailsEx.getBirthDate());
				String eTicket = paxDetailsEx.getETicket();
				String pnrPaxCatFOIDNumber = paxDetailsEx.getPnrPaxCatFOIDNumber();
				Date pnrPaxCatFOIDExpiry = getDateObj(paxDetailsEx.getPnrPaxCatFOIDExpiry());
				String pnrPaxCatFOIDPlace = paxDetailsEx.getPnrPaxCatFOIDPlace();
				String travelerReference = getTravellerReference(paxType, seqNumber + "");
				String paxCategory = paxDetailsEx.getPaxCategory();
				String ffid = paxDetailsEx.getPnrPaxFfid();
				String firstNameInOtherLanguage = "";
				String lastNameInOtherLanguage = "";
				String titleInOtherLanguage = "";
				String nationalIDNo = paxDetailsEx.getNationalIDNo();
				String pnrPaxArrivalIntlFlightNumber = paxDetailsEx.getPnrPaxArrivalInternationalFlightNumber();
				Date pnrPaxIntlFltArrivalTimeStamp = getTimeStampObj(paxDetailsEx.getPnrPaxInternationalFltArrivalDate(),
						paxDetailsEx.getPnrPaxInternationalFlightArrivalTime());
				String pnrPaxDepartureIntlFlightNumber = paxDetailsEx.getPnrPaxDepartureInternationalFlightNumber();
				Date pnrPaxDepartureIntlFltTimeStamp = getTimeStampObj(paxDetailsEx.getPnrPaxInternationalFltDepartureDate(),
						paxDetailsEx.getPnrPaxInternationalFlightDepartureTime());
				String pnrPaxGroupId = paxDetailsEx.getPnrPaxGroupId();

				if ((!("").equals(BeanUtils.nullHandler(paxDetailsEx.getFirstNameInOtherLanguages())))
						&& (!("").equals(BeanUtils.nullHandler(paxDetailsEx.getLastNameInOtherLanguages())))) {
					firstNameInOtherLanguage = StringUtil.convertToHex(paxDetailsEx.getFirstNameInOtherLanguages());
					lastNameInOtherLanguage = StringUtil.convertToHex(paxDetailsEx.getLastNameInOtherLanguages());
					titleInOtherLanguage = StringUtil.convertToHex(paxDetailsEx.getTitleInOtherLanguage());
					pax.setNameTranslationLanguage(BeanUtils.nullHandler(paxDetailsEx.getNameTranslationLanguage()));
					pax.setFirstNameInOtherLanguage(firstNameInOtherLanguage);
					pax.setLastNameInOtherLanguage(lastNameInOtherLanguage);
					pax.setTitleInOtherLanguage(titleInOtherLanguage);
				}
				pax.setIsParent(new Boolean(blnParent));
				pax.setPaxType(paxType);
				pax.setTitle(title);
				pax.setDateOfBirth(dob);
				pax.setFirstName(firstName);
				pax.setLastName(lastName);
				pax.setNationality(nationality);
				pax.setInfantWith(infantWith);
				pax.setSeqNumber(seqNumber);
				pax.setTravelerRefNumber(traverRefNo);
				pax.seteTicket(eTicket);

				PaxAdditionalInfoDTO paxAdditionalInfoDTO = new PaxAdditionalInfoDTO();
				paxAdditionalInfoDTO.setPassportNo(pnrPaxCatFOIDNumber);
				paxAdditionalInfoDTO.setPassportExpiry(pnrPaxCatFOIDExpiry);
				paxAdditionalInfoDTO.setPassportIssuedCntry(pnrPaxCatFOIDPlace);
				paxAdditionalInfoDTO.setPlaceOfBirth(paxDetailsEx.getPnrPaxPlaceOfBirth());
				paxAdditionalInfoDTO.setTravelDocumentType(paxDetailsEx.getTravelDocType());
				paxAdditionalInfoDTO.setVisaDocNumber(paxDetailsEx.getVisaDocNumber());
				paxAdditionalInfoDTO.setVisaDocIssueDate(getDateObj(paxDetailsEx.getVisaDocIssueDate()));
				paxAdditionalInfoDTO.setVisaApplicableCountry(paxDetailsEx.getVisaApplicableCountry());
				paxAdditionalInfoDTO.setVisaDocPlaceOfIssue(paxDetailsEx.getVisaDocPlaceOfIssue());
				paxAdditionalInfoDTO.setNationalIDNo(nationalIDNo);
				paxAdditionalInfoDTO.setVisaDocPlaceOfIssue(paxDetailsEx.getVisaDocPlaceOfIssue());	
				paxAdditionalInfoDTO.setFfid(ffid.toUpperCase());
				pax.setPaxAdditionalInfo(paxAdditionalInfoDTO);
				
				pax.setPaxCategory(paxCategory);
				pax.setPnrPaxArrivalIntlFltNumber(pnrPaxArrivalIntlFlightNumber);
				pax.setPnrPaxIntlFltArrivalDate(pnrPaxIntlFltArrivalTimeStamp);
				pax.setPnrPaxDepartureIntlFltNumber(pnrPaxDepartureIntlFlightNumber);
				pax.setPnrPaxIntlFltDepartureDate(pnrPaxDepartureIntlFltTimeStamp);
				pax.setPnrPaxGroupId(pnrPaxGroupId);


				if (!PaxTypeTO.INFANT.equals(pax.getPaxType())) {
					nonInfants++;
					paxCount++;
					if (pax.getIsParent()) {
						paxCount++;
					}
				}

				JSONArray jSegCount = (JSONArray) (jObject.get("anci"));
				Iterator<?> segCountIterator = jSegCount.iterator();

				Map<String, Integer> baggageONDGrpIdWiseCount = new HashMap<String, Integer>();
				Map<String, List<String>> orderedCarriers = new LinkedHashMap<String, List<String>>();
				Map<String, String> mapFlightRefAndOndBaggageId = new HashMap<String, String>();

				while (segCountIterator.hasNext()) {
					JSONObject jAnci = (JSONObject) segCountIterator.next();
					FlightSegmentTO flightSegmentTO = getFlightSegmentTO((JSONObject) jAnci.get("segInfo"));

					if (flightSegmentTO != null && BeanUtils.nullHandler(flightSegmentTO.getBaggageONDGroupId()).length() > 0) {
						mapFlightRefAndOndBaggageId.put(flightSegmentTO.getFlightRefNumber(),
								BeanUtils.nullHandler(flightSegmentTO.getBaggageONDGroupId()));

						Integer count = baggageONDGrpIdWiseCount.get(flightSegmentTO.getBaggageONDGroupId());
						if (count == null) {
							baggageONDGrpIdWiseCount.put(flightSegmentTO.getBaggageONDGroupId(), new Integer(1));
						} else {
							baggageONDGrpIdWiseCount.put(flightSegmentTO.getBaggageONDGroupId(), ++count);
						}

						List<String> carriers = orderedCarriers.get(flightSegmentTO.getBaggageONDGroupId());
						if (carriers == null) {
							carriers = new ArrayList<String>();
							carriers.add(FlightRefNumberUtil.getOperatingAirline(flightSegmentTO.getFlightRefNumber()));
							orderedCarriers.put(flightSegmentTO.getBaggageONDGroupId(), carriers);
						} else {
							carriers.add(FlightRefNumberUtil.getOperatingAirline(flightSegmentTO.getFlightRefNumber()));
						}
					}
				}

				JSONArray jSeg = (JSONArray) (jObject.get("anci"));
				JSONArray jRSeg = null;
				Iterator<?> segRIterator = null;
				boolean modifyAvailable = false;
				if (jObject.get("removeAnci") != null) {
					jRSeg = (JSONArray) (jObject.get("removeAnci"));
					segRIterator = (jRSeg).iterator();
					modifyAvailable = true;
				}

				Iterator<?> segIterator = jSeg.iterator();
				while (segIterator.hasNext()) {

					JSONObject jAnci = (JSONObject) segIterator.next();
					FlightSegmentTO flightSegmentTO = getFlightSegmentTO((JSONObject) jAnci.get("segInfo"));

					Map<EXTERNAL_CHARGES, Integer> segOffers = anciOfferTemplates == null ? null : anciOfferTemplates
							.get(flightSegmentTO.getFlightRefNumber());

					Integer mealAnciOfferedTemplateID = segOffers == null ? null : segOffers.get(EXTERNAL_CHARGES.MEAL);

					if (flightSegmentTO != null) {
						// populate operating airline
						flightSegmentTO.setOperatingAirline(FlightRefNumberUtil.getOperatingAirline(flightSegmentTO
								.getFlightRefNumber()));
						flightSegmentTO.setFlownSegment(isFlightSegmentFlown(insuredFltSegs, flightSegmentTO.getFlightRefNumber()));
						LCCAirSeatDTO seatDTO = getSeatDTO((JSONObject) jAnci.get("seat"), paxType);
						List<LCCAirSeatDTO> extraSeats = getExtraSeatsDTO((JSONObject) jAnci.get("extraSeats"), paxType);
						List<LCCMealDTO> mealDTOs = getMealDTOs((JSONObject) jAnci.get("meal"), mealAnciOfferedTemplateID);

						List<LCCBaggageDTO> baggageDTOs = getRespectiveBaggages(jAnci, flightSegmentTO, orderedCarriers,
								baggageONDGrpIdWiseCount);
						List<LCCAutomaticCheckinDTO> autoCheckinDtos = getAutoCheckinDTOs((JSONObject) jAnci
								.get("automaticCheckin"));
						List<LCCSpecialServiceRequestDTO> ssrDTOs = getSSRDTOs((JSONObject) jAnci.get("ssr"));
						List<LCCAirportServiceDTO> airportServiceDTOs = getAirportServiceDTOs((JSONObject) jAnci.get("aps"));
						List<LCCAirportServiceDTO> airportTransferDTOs = getAirportTransferDTOs((JSONObject) jAnci.get("apt"));

						if (modifyAvailable) {
							LCCSelectedSegmentAncillaryDTO removedAnciSeg = new LCCSelectedSegmentAncillaryDTO();
							LCCSelectedSegmentAncillaryDTO updatedAnciSeg = new LCCSelectedSegmentAncillaryDTO();
							JSONObject jRemoveAnci = (JSONObject) segRIterator.next();
							LCCAirSeatDTO removeSeatDTO = getSeatDTO((JSONObject) jRemoveAnci.get("seat"), paxType);
							List<LCCAirSeatDTO> removeExtraSeats = getExtraSeatsDTO((JSONObject) jRemoveAnci.get("extraSeats"),
									paxType);
							List<LCCMealDTO> removeMealDTOs = getMealDTOs((JSONObject) jRemoveAnci.get("meal"),
									mealAnciOfferedTemplateID);
							List<LCCBaggageDTO> removeBaggageDTOs = getBaggageDTOs((JSONObject) jRemoveAnci.get("baggage"), null);
							List<LCCSpecialServiceRequestDTO> removeSsrDTOs = getSSRDTOs((JSONObject) jRemoveAnci.get("ssr"));
							List<LCCAirportServiceDTO> removeApsDTOs = getAirportServiceDTOs((JSONObject) jRemoveAnci.get("aps"));
							List<LCCAirportServiceDTO> removeAPTransfers = getAirportTransferDTOs((JSONObject) jRemoveAnci
									.get("apt"));
							List<LCCAutomaticCheckinDTO> removeAutoCheckinDtos = getAutoCheckinDTOs((JSONObject) jRemoveAnci
									.get("automaticCheckin"));

							removeSeatDuplicates(seatDTO, removeSeatDTO);
							removeMealDuplicates(mealDTOs, removeMealDTOs);
							removeBaggageDuplicates(baggageDTOs, removeBaggageDTOs);
							List<LCCSpecialServiceRequestDTO> updateSsrs = removeSSRDuplicatesNAddUpdates(ssrDTOs, removeSsrDTOs);
							removeAirportServiceDuplicates(airportServiceDTOs, removeApsDTOs);
							removeAirportTransferDuplicates(airportTransferDTOs, removeAPTransfers);
							removeAutoCheckinDuplicates(autoCheckinDtos, removeAutoCheckinDtos);

							removedAnciSeg.setAirSeatDTO(removeSeatDTO);
							removedAnciSeg.setMealDTOs(removeMealDTOs);
							removedAnciSeg.setBaggageDTOs(removeBaggageDTOs);
							removedAnciSeg.setSpecialServiceRequestDTOs(removeSsrDTOs);
							removedAnciSeg.setAirportServiceDTOs(removeApsDTOs);
							removedAnciSeg.setAirportTransferDTOs(removeAPTransfers);
							removedAnciSeg.setExtraSeatDTOs(removeExtraSeats);
							removedAnciSeg.setFlightSegmentTO(flightSegmentTO);
							removedAnciSeg.setTravelerRefNumber(travelerReference);
							removedAnciSeg.setAutomaticCheckinDTOs(removeAutoCheckinDtos);

							if (blnParent) {
								removedAnciSeg.setPaxType(PaxTypeTO.PARENT);
							} else {
								removedAnciSeg.setPaxType(paxType);
							}

							removedAnciSeg.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(flightSegmentTO
									.getFlightRefNumber()));

							updatedAnciSeg.setSpecialServiceRequestDTOs(updateSsrs);
							updatedAnciSeg.setFlightSegmentTO(flightSegmentTO);
							updatedAnciSeg.setTravelerRefNumber(travelerReference);

							updatedAnciSeg.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(flightSegmentTO
									.getFlightRefNumber()));

							pax.addAncillariesToRemove(removedAnciSeg);
							pax.addAncillariesToUpdate(updatedAnciSeg);
						}

						pax.addExternalCharges(getSeatExternalCharges(seatDTO, flightSegmentTO));
						pax.addExternalCharges(getMealExternalCharges(mealDTOs, flightSegmentTO));
						pax.addExternalCharges(getBaggageExternalCharges(baggageDTOs, flightSegmentTO));
						pax.addExternalCharges(getSSRExternalCharges(ssrDTOs, flightSegmentTO));
						pax.addExternalCharges(getAirportServiceExternalCharges(airportServiceDTOs, flightSegmentTO));
						pax.addExternalCharges(getAirportTransferExternalCharges(airportTransferDTOs, flightSegmentTO));
						pax.setMapFlightRefAndOndBaggageId(mapFlightRefAndOndBaggageId);
						pax.addExternalCharges(getAutoCheckinExternalCharges(autoCheckinDtos, flightSegmentTO));
						LCCSelectedSegmentAncillaryDTO anciSeg = new LCCSelectedSegmentAncillaryDTO();
						anciSeg.setAirSeatDTO(seatDTO);
						anciSeg.setMealDTOs(mealDTOs);
						anciSeg.setBaggageDTOs(baggageDTOs);
						anciSeg.setSpecialServiceRequestDTOs(ssrDTOs);
						anciSeg.setAirportServiceDTOs(airportServiceDTOs);
						anciSeg.setAirportTransferDTOs(airportTransferDTOs);
						anciSeg.setFlightSegmentTO(flightSegmentTO);
						// anciSeg.setInsuranceQuotation(insuranceQuotation); /* CHECK */
						anciSeg.setInsuranceQuotations(insuranceQuotations);
						anciSeg.setTravelerRefNumber(travelerReference);
						anciSeg.setAutomaticCheckinDTOs(autoCheckinDtos);

						anciSeg.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(flightSegmentTO.getFlightRefNumber()));

						pax.addSelectedAncillaries(anciSeg);
					} else {
						segRIterator.next();
					}

				}
				paxList.add(pax);
			}
			/* Pax wise breakdown of the insurance - Kept for pax wise data saving purpose */
			if (insuranceQuotations != null && !insuranceQuotations.isEmpty()) {
				// BigDecimal[] insuranceQuota =
				// AccelAeroCalculator.roundAndSplit(insuranceQuotation.getTotalPremiumAmount(),
				// nonInfants*operatingFlightSegs.values().size());
				// Segmentwise splitting is handled at the create reservation level

				BigDecimal totalInsuracePremium = AccelAeroCalculator.getDefaultBigDecimalZero();
				String operatingCarrier = null;

				for (LCCInsuranceQuotationDTO insuranceQuotation : insuranceQuotations) {
					operatingCarrier = insuranceQuotation.getOperatingAirline(); // all quotes will be having same
					totalInsuracePremium = AccelAeroCalculator.add(totalInsuracePremium,
							insuranceQuotation.getQuotedTotalPremiumAmount());
				}

				BigDecimal[] insuranceQuota = null;
				if (AppSysParamsUtil.allowAddInsurnaceForInfants()) {
					insuranceQuota = AccelAeroCalculator.roundAndSplit(totalInsuracePremium, paxCount);
				} else {
					insuranceQuota = AccelAeroCalculator.roundAndSplit(totalInsuracePremium, nonInfants);
				}
				LCCClientExternalChgDTO chgDTO = new LCCClientExternalChgDTO();
				chgDTO.setExternalCharges(EXTERNAL_CHARGES.INSURANCE);
				chgDTO.setCarrierCode(operatingCarrier);

				FlightSegmentTO operatingSegment = null;
				for (FlightSegmentTO fltSegment : insuredFltSegs) {
					if (FlightRefNumberUtil.getOperatingAirline(fltSegment.getFlightRefNumber()).equals(operatingCarrier)) {
						operatingSegment = fltSegment;
						break;
					}
				}
				int insIndex = 0;

				/* Only add insurance to the Originating airline segments */
				// for(FlightSegmentTO fSeg: operatingFlightSegs.values()){
				for (ReservationPaxTO pax : paxList) {
					if (!PaxTypeTO.INFANT.equals(pax.getPaxType())) {
						LCCClientExternalChgDTO tmpChgDTO = chgDTO.clone();
						tmpChgDTO.setFlightRefNumber(operatingSegment.getFlightRefNumber());
						if (pax.getIsParent() && AppSysParamsUtil.allowAddInsurnaceForInfants()) {
							tmpChgDTO.setAmount(AccelAeroCalculator.add(insuranceQuota[insIndex++], insuranceQuota[insIndex++]));
						} else {
							tmpChgDTO.setAmount(insuranceQuota[insIndex++]);
						}
						pax.addExternalCharges(tmpChgDTO);
					}
				}
				// }

			}
		}
		return paxList;
	}
	
	private static List<LCCClientExternalChgDTO> getAutoCheckinExternalCharges(List<LCCAutomaticCheckinDTO> autoCheckinDtos,
			FlightSegmentTO flightSegmentTO) {
		List<LCCClientExternalChgDTO> chgDTOs = null;
		if (autoCheckinDtos != null && autoCheckinDtos.size() > 0) {
			chgDTOs = new ArrayList<LCCClientExternalChgDTO>();
			for (LCCAutomaticCheckinDTO autoCheckin : autoCheckinDtos) {
				LCCClientExternalChgDTO chg = new LCCClientExternalChgDTO();
				chg.setAmount(autoCheckin.getAutomaticCheckinCharge());
				chg.setExternalCharges(EXTERNAL_CHARGES.AUTOMATIC_CHECKIN);
				chg.setCode(String.valueOf(autoCheckin.getAutoCheckinId()));
				chg.setSeatTypePreference(autoCheckin.getSeatPref());
				chg.setEmail(autoCheckin.getEmail());
				chg.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
				chg.setSeatCode(autoCheckin.getSeatCode());
				chg.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(flightSegmentTO.getFlightRefNumber()));

				chgDTOs.add(chg.clone());
			}
		}
		return chgDTOs;
	}

	private static void removeAutoCheckinDuplicates(List<LCCAutomaticCheckinDTO> autoCheckinDtos,
			List<LCCAutomaticCheckinDTO> removeAutoCheckinDtos) {
		Set<LCCAutomaticCheckinDTO> remFromAdd = new HashSet<LCCAutomaticCheckinDTO>();
		Set<LCCAutomaticCheckinDTO> remFromRem = new HashSet<LCCAutomaticCheckinDTO>();
		for (LCCAutomaticCheckinDTO addAutoCheckin : autoCheckinDtos) {
			top: for (LCCAutomaticCheckinDTO removeAutoCheckin : removeAutoCheckinDtos) {
				if (addAutoCheckin.getAutoCheckinId().equals(removeAutoCheckin.getAutoCheckinId())) {
					remFromAdd.add(addAutoCheckin);
					remFromRem.add(removeAutoCheckin);
					break top;
				}
			}
		}
		autoCheckinDtos.removeAll(remFromAdd);
		removeAutoCheckinDtos.removeAll(remFromRem);

	}

	private static List<LCCAutomaticCheckinDTO> getAutoCheckinDTOs(JSONObject jsonAutoCheckin) {
		List<LCCAutomaticCheckinDTO> autoCheckinList = new ArrayList<LCCAutomaticCheckinDTO>();
		if (jsonAutoCheckin != null
				&& ((jsonAutoCheckin.get("seatPreference") != null && !StringUtil.isNullOrEmpty(jsonAutoCheckin.get(
						"seatPreference").toString())) || (jsonAutoCheckin.get("seatCode") != null && !StringUtil
						.isNullOrEmpty(jsonAutoCheckin.get("seatCode").toString())))) {
			if (jsonAutoCheckin != null) {
				LCCAutomaticCheckinDTO autoCheckin = new LCCAutomaticCheckinDTO();
				BigDecimal charge = new BigDecimal(jsonAutoCheckin.get("autoCheckinCharge").toString());
				autoCheckin.setAutomaticCheckinCharge(charge);
				if (!StringUtil.isNullOrEmpty(jsonAutoCheckin.get("seatPreference").toString())) {
					autoCheckin.setSeatPref(jsonAutoCheckin.get("seatPreference").toString());
				} else if (!StringUtil.isNullOrEmpty(jsonAutoCheckin.get("seatCode").toString())) {
					autoCheckin.setSeatCode(jsonAutoCheckin.get("seatCode").toString());
				}
				autoCheckin.setEmail(jsonAutoCheckin.get("email").toString());
				autoCheckin.setAutoCheckinId(Integer.valueOf(jsonAutoCheckin.get("autoCheckinTemplateId").toString()));
				autoCheckinList.add(autoCheckin);
			}
		}
		return autoCheckinList;
	}

	public static List<ReservationPaxTO> extractReservationPaxBasicInfo(String jsonString, ApplicationEngine appEngine) throws ParseException, ModuleException,
			java.text.ParseException {
		List<ReservationPaxTO> paxList = new ArrayList<ReservationPaxTO>();
		if (jsonString != null && !"".equals(jsonString)) {
			JSONParser parser = new JSONParser();
			JSONArray jsonArray = (JSONArray) parser.parse(jsonString);
			Iterator<?> iterator = jsonArray.iterator();

			while (iterator.hasNext()) {
				ReservationPaxTO pax = new ReservationPaxTO();
				JSONObject jObject = (JSONObject) iterator.next();
				
				String paxType = (String) BeanUtils.nullHandler(jObject.get("paxType"));
				Integer seqNumber = Integer.parseInt((String) BeanUtils.nullHandler(jObject.get("paxSequence")));
				boolean isParent = Boolean.parseBoolean(BeanUtils.nullHandler(jObject.get("parent")));
			
				pax.setPaxType(paxType);
				pax.setSeqNumber(seqNumber);
				pax.setIsParent(isParent);
				if (isParent){
					Integer infantWith = Integer.parseInt((String) BeanUtils.nullHandler(jObject.get("infantWith")));
					pax.setInfantWith(infantWith);
				}
				
				paxList.add(pax);
			}
			
			// remove duplicates
			Map<Integer, ReservationPaxTO> resPaxMap = new HashMap<Integer, ReservationPaxTO>();
			for (ReservationPaxTO pax: paxList){
				resPaxMap.put(pax.getSeqNumber(), pax);
			}
			paxList.clear();
			paxList = new ArrayList<ReservationPaxTO>(resPaxMap.values());
		}
		return paxList;
	}
	
	private static boolean isFlightSegmentFlown(List<FlightSegmentTO> fltSegs, String flightRefNumber){
		boolean flownSegment = false;
		
		for (Iterator<FlightSegmentTO> fltSegsIterator = fltSegs.iterator();fltSegsIterator.hasNext();){
			FlightSegmentTO fltSeg = fltSegsIterator.next();
			if (fltSeg.getFlightRefNumber().equals(flightRefNumber)){
				flownSegment = fltSeg.isFlownSegment();
			}
		}
		
		return flownSegment;
	}

	private static List<LCCBaggageDTO> getRespectiveBaggages(JSONObject jAnci, FlightSegmentTO flightSegmentTO,
			Map<String, List<String>> orderedCarriers, Map<String, Integer> baggageONDGrpIdWiseCount) throws ModuleException {

		String baggageONDGrpId = BeanUtils.nullHandler(flightSegmentTO.getBaggageONDGroupId());
		List<LCCBaggageDTO> baggageDTOs = getBaggageDTOs((JSONObject) jAnci.get("baggage"), baggageONDGrpId);

		if (baggageONDGrpId != null && !baggageONDGrpId.isEmpty()) {
			List<String> lstCarriers = orderedCarriers.get(baggageONDGrpId);
			String firstCarrier = BeanUtils.getFirstElement(lstCarriers);
			String flightSegmentOperatingAirline = FlightRefNumberUtil.getOperatingAirline(flightSegmentTO.getFlightRefNumber());

			if (flightSegmentOperatingAirline.equals(firstCarrier)
					|| isCarriersInSameAirline(flightSegmentOperatingAirline, firstCarrier)) {
				int countForSameCarriers = countForSameCarriers(firstCarrier, lstCarriers);
				ONDBaggageUtil.spitAmountSegWise(baggageDTOs, countForSameCarriers);
			} else {
				baggageDTOs = new ArrayList<LCCBaggageDTO>();
			}
		}

		return baggageDTOs;
	}

	private static int countForSameCarriers(String firstCarrier, List<String> lstCarriers) throws ModuleException {
		int i = 0;
		for (String element : lstCarriers) {
			if (firstCarrier.equals(element) || isCarriersInSameAirline(firstCarrier, element)) {
				i++;
			}
		}

		return i;
	}

	private static boolean isCarriersInSameAirline(String carrierOne, String carrierTwo) throws ModuleException {
		boolean ownCarrier = false;
		Set<String> ownAirlineCarriers  = CommonsServices.getGlobalConfig().getOwnAirlineCarrierCodesExcludingBus();
		if (ownAirlineCarriers.contains(carrierOne) && ownAirlineCarriers.contains(carrierTwo)) {
			ownCarrier = true;
		}
		return ownCarrier;
	}

	private static List<LCCSpecialServiceRequestDTO> removeSSRDuplicatesNAddUpdates(List<LCCSpecialServiceRequestDTO> ssrDTOs,
			List<LCCSpecialServiceRequestDTO> removeSsrDTOs) {
		Set<LCCSpecialServiceRequestDTO> rremSet = new HashSet<LCCSpecialServiceRequestDTO>();
		Set<LCCSpecialServiceRequestDTO> aremSet = new HashSet<LCCSpecialServiceRequestDTO>();
		List<LCCSpecialServiceRequestDTO> updateList = new ArrayList<LCCSpecialServiceRequestDTO>();
		for (LCCSpecialServiceRequestDTO addSsr : ssrDTOs) {
			top: for (LCCSpecialServiceRequestDTO removeSsr : removeSsrDTOs) {
				if (addSsr.getSsrCode().equals(removeSsr.getSsrCode())) {
					if ((addSsr.getText() != null && removeSsr.getText() != null && !(addSsr.getText()
							.equals(removeSsr.getText())))) {
						addSsr.setCharge(AccelAeroCalculator.subtract(addSsr.getCharge(), removeSsr.getCharge()));
						updateList.add(addSsr);
						rremSet.add(removeSsr);
						aremSet.add(addSsr);
						break top;
					}
					if ((addSsr.getText() == null && removeSsr.getText() == null)
							|| (addSsr.getText() != null && removeSsr.getText() != null && (addSsr.getText().equals(removeSsr
									.getText())))) {
						rremSet.add(removeSsr);
						aremSet.add(addSsr);
						break top;
					}
				}
			}
		}
		ssrDTOs.removeAll(aremSet);
		removeSsrDTOs.removeAll(rremSet);
		return updateList;
	}

	private static void removeMealDuplicates(List<LCCMealDTO> mealDTOs, List<LCCMealDTO> removeMealDTOs) {
		Set<LCCMealDTO> remFromAdd = new HashSet<LCCMealDTO>();
		Set<LCCMealDTO> remFromRem = new HashSet<LCCMealDTO>();
		for (LCCMealDTO addMeal : mealDTOs) {
			top: for (LCCMealDTO removeMeal : removeMealDTOs) {
				if (addMeal.getMealCode().equals(removeMeal.getMealCode())) {
					if (addMeal.getAllocatedMeals() == removeMeal.getAllocatedMeals()) {
						remFromAdd.add(addMeal);
						remFromRem.add(removeMeal);
					} else if (addMeal.getAllocatedMeals() > removeMeal.getAllocatedMeals()) {
						addMeal.setAllocatedMeals(addMeal.getAllocatedMeals() - removeMeal.getAllocatedMeals());
						remFromRem.add(removeMeal);
					} else if (removeMeal.getAllocatedMeals() > addMeal.getAllocatedMeals()) {
						removeMeal.setAllocatedMeals(removeMeal.getAllocatedMeals() - addMeal.getAllocatedMeals());
						remFromAdd.add(addMeal);
					}
					break top;
				}
			}
		}
		mealDTOs.removeAll(remFromAdd);
		removeMealDTOs.removeAll(remFromRem);
	}

	private static void removeBaggageDuplicates(List<LCCBaggageDTO> baggageDTOs, List<LCCBaggageDTO> removeBaggageDTOs) {
		Set<LCCBaggageDTO> commonSet = new HashSet<LCCBaggageDTO>();
		for (LCCBaggageDTO addBaggage : baggageDTOs) {
			top: for (LCCBaggageDTO removeBaggage : removeBaggageDTOs) {
				if (addBaggage.getBaggageName().equals(removeBaggage.getBaggageName())) {
					commonSet.add(removeBaggage);
					break top;
				}
			}
		}
		baggageDTOs.removeAll(commonSet);
		removeBaggageDTOs.removeAll(commonSet);
	}

	private static void removeSeatDuplicates(LCCAirSeatDTO addSeat, LCCAirSeatDTO removeSeat) {
		if (addSeat != null && removeSeat != null) {
			if (addSeat.getSeatNumber().equals(removeSeat.getSeatNumber())) {
				addSeat = null;
				removeSeat = null;
			}
		}
	}

	private static void removeAirportServiceDuplicates(List<LCCAirportServiceDTO> addAirportServiceDTOs,
			List<LCCAirportServiceDTO> removeAirportServiceDTOs) {
		Set<LCCAirportServiceDTO> remFromAdd = new HashSet<LCCAirportServiceDTO>();
		Set<LCCAirportServiceDTO> remFromRemove = new HashSet<LCCAirportServiceDTO>();
		for (LCCAirportServiceDTO addService : addAirportServiceDTOs) {
			top: for (LCCAirportServiceDTO removeService : removeAirportServiceDTOs) {
				if (addService.getSsrCode().equals(removeService.getSsrCode())
						&& addService.getAirportCode().equals(removeService.getAirportCode())) {
					remFromRemove.add(removeService);
					remFromAdd.add(addService);
					break top;
				}
			}
		}

		addAirportServiceDTOs.removeAll(remFromAdd);
		removeAirportServiceDTOs.removeAll(remFromRemove);
	}

	private static void removeAirportTransferDuplicates(List<LCCAirportServiceDTO> addAirportTransferDTOs,
			List<LCCAirportServiceDTO> removeAPTransfers) {
		/*
		 * Set<LCCAirportServiceDTO> remFromAdd = new HashSet<LCCAirportServiceDTO>(); Set<LCCAirportServiceDTO>
		 * remFromRemove = new HashSet<LCCAirportServiceDTO>(); for (LCCAirportServiceDTO addTransfer :
		 * addAirportTransferDTOs) { top: for (LCCAirportServiceDTO removeTransfer : removeAPTransfers) { if
		 * (addTransfer.getAirportCode().equals(removeTransfer.getAirportCode()) &&
		 * addTransfer.getApplyOn().equals(removeTransfer.getApplyOn())) { remFromRemove.add(removeTransfer);
		 * remFromAdd.add(addTransfer); break top; } } }
		 * 
		 * addAirportTransferDTOs.removeAll(remFromAdd); removeAPTransfers.removeAll(remFromRemove);
		 */
		Set<LCCAirportServiceDTO> remFromAdd = new HashSet<LCCAirportServiceDTO>();
		for (LCCAirportServiceDTO addTransfer : addAirportTransferDTOs) {
			if (addTransfer.isExisting()) {
				remFromAdd.add(addTransfer);
			}
		}
		addAirportTransferDTOs.removeAll(remFromAdd);
	}

	private static LCCClientExternalChgDTO getSeatExternalCharges(LCCAirSeatDTO seatDTO, FlightSegmentTO flightSegmentTO) {
		LCCClientExternalChgDTO chgDTO = null;
		if (seatDTO != null && seatDTO.getSeatNumber() != null && !seatDTO.getSeatNumber().equals("")) {
			chgDTO = new LCCClientExternalChgDTO();
			chgDTO.setExternalCharges(EXTERNAL_CHARGES.SEAT_MAP);
			chgDTO.setAmount(seatDTO.getSeatCharge());
			chgDTO.setCode(seatDTO.getSeatNumber());
			chgDTO.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
			chgDTO.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(flightSegmentTO.getFlightRefNumber()));
		}
		return chgDTO;
	}

	private static List<LCCClientExternalChgDTO>
			getMealExternalCharges(List<LCCMealDTO> mealDTOs, FlightSegmentTO flightSegmentTO) {
		List<LCCClientExternalChgDTO> chgDTOs = null;
		if (mealDTOs != null && mealDTOs.size() > 0) {
			chgDTOs = new ArrayList<LCCClientExternalChgDTO>();
			for (LCCMealDTO meal : mealDTOs) {
				LCCClientExternalChgDTO chg = new LCCClientExternalChgDTO();
				chg.setAmount(meal.getMealCharge());
				chg.setExternalCharges(EXTERNAL_CHARGES.MEAL);
				chg.setCode(meal.getMealCode());
				chg.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
				chg.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(flightSegmentTO.getFlightRefNumber()));
				chg.setAnciOffer(meal.isAnciOffer());
				chg.setOfferedAnciTemplateID(meal.getOfferedTemplateID());
				for (int i = 0; i < meal.getAllocatedMeals(); i++) {
					chgDTOs.add(chg.clone());
				}
			}
		}
		return chgDTOs;
	}

	private static List<LCCClientExternalChgDTO> getBaggageExternalCharges(List<LCCBaggageDTO> baggageDTOs,
			FlightSegmentTO flightSegmentTO) {
		List<LCCClientExternalChgDTO> chgDTOs = null;
		if (baggageDTOs != null && baggageDTOs.size() > 0) {
			chgDTOs = new ArrayList<LCCClientExternalChgDTO>();
			for (LCCBaggageDTO baggage : baggageDTOs) {
				LCCClientExternalChgDTO chg = new LCCClientExternalChgDTO();
				chg.setAmount(baggage.getBaggageCharge());
				chg.setExternalCharges(EXTERNAL_CHARGES.BAGGAGE);
				chg.setCode(baggage.getBaggageName());
				chg.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
				chg.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(flightSegmentTO.getFlightRefNumber()));
				chg.setOndBaggageChargeGroupId(baggage.getOndBaggageChargeId());
				chg.setOndBaggageGroupId(baggage.getOndBaggageGroupId());

				chgDTOs.add(chg.clone());

			}
		}
		return chgDTOs;
	}

	private static List<LCCClientExternalChgDTO> getSSRExternalCharges(List<LCCSpecialServiceRequestDTO> ssrDTOs,
			FlightSegmentTO flightSegmentTO) {
		List<LCCClientExternalChgDTO> chgDTOs = null;
		if (ssrDTOs != null && ssrDTOs.size() > 0) {
			chgDTOs = new ArrayList<LCCClientExternalChgDTO>();
			for (LCCSpecialServiceRequestDTO ssr : ssrDTOs) {
				LCCClientExternalChgDTO chg = new LCCClientExternalChgDTO();
				chg.setAmount(ssr.getCharge());
				chg.setExternalCharges(EXTERNAL_CHARGES.INFLIGHT_SERVICES);
				chg.setCode(ssr.getSsrCode());
				chg.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
				chg.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(flightSegmentTO.getFlightRefNumber()));
				chg.setUserNote(ssr.getText());
				chgDTOs.add(chg);
			}
		}
		return chgDTOs;
	}

	private static List<LCCClientExternalChgDTO> getAirportServiceExternalCharges(List<LCCAirportServiceDTO> airportServiceDTOs,
			FlightSegmentTO flightSegmentTO) {
		List<LCCClientExternalChgDTO> chgDTOs = null;

		if (airportServiceDTOs != null && airportServiceDTOs.size() > 0) {
			chgDTOs = new ArrayList<LCCClientExternalChgDTO>();
			for (LCCAirportServiceDTO lccAirportServiceDTO : airportServiceDTOs) {
				LCCClientExternalChgDTO chg = new LCCClientExternalChgDTO();
				chg.setAmount(lccAirportServiceDTO.getServiceCharge());
				chg.setExternalCharges(EXTERNAL_CHARGES.AIRPORT_SERVICE);
				chg.setCode(lccAirportServiceDTO.getSsrCode());
				chg.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
				chg.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(flightSegmentTO.getFlightRefNumber()));
				chg.setAirportCode(lccAirportServiceDTO.getAirportCode());
				chg.setApplyOn(lccAirportServiceDTO.getApplyOn());
				chg.setSegmentSequence(flightSegmentTO.getSegmentSequence());
				chgDTOs.add(chg);
			}
		}

		return chgDTOs;
	}

	private static List<LCCClientExternalChgDTO> getAirportTransferExternalCharges(
			List<LCCAirportServiceDTO> airportTransferDTOs, FlightSegmentTO flightSegmentTO) {
		List<LCCClientExternalChgDTO> chgDTOs = null;

		if (airportTransferDTOs != null && airportTransferDTOs.size() > 0) {
			chgDTOs = new ArrayList<LCCClientExternalChgDTO>();
			for (LCCAirportServiceDTO lccAirportServiceDTO : airportTransferDTOs) {
				LCCClientExternalChgDTO chg = new LCCClientExternalChgDTO();
				chg.setAmount(lccAirportServiceDTO.getServiceCharge());
				chg.setExternalCharges(EXTERNAL_CHARGES.AIRPORT_TRANSFER);
				chg.setCode(lccAirportServiceDTO.getSsrCode());
				chg.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
				chg.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(flightSegmentTO.getFlightRefNumber()));
				chg.setAirportCode(lccAirportServiceDTO.getAirportCode());
				chg.setOfferedAnciTemplateID(lccAirportServiceDTO.getAirportTransferId());
				chg.setApplyOn(lccAirportServiceDTO.getApplyOn());
				chg.setSegmentSequence(flightSegmentTO.getSegmentSequence());
				chg.setTransferDate(lccAirportServiceDTO.getTranferDate());
				chg.setTransferType(lccAirportServiceDTO.getTransferType());
				chg.setTransferAddress(lccAirportServiceDTO.getTransferAddress());
				chg.setTransferContact(lccAirportServiceDTO.getTransferContact());
				chgDTOs.add(chg);
			}
		}

		return chgDTOs;
	}

	private static FlightSegmentTO getFlightSegmentTO(JSONObject jsonFS) {
		FlightSegmentTO flightSegmentTO = null;
		if (jsonFS != null && !jsonFS.equals("")) {
			flightSegmentTO = new FlightSegmentTO();
			flightSegmentTO.setFlightRefNumber(jsonFS.get("flightRefNumber").toString());
			if (jsonFS.get("flightNumber") != null) {
				flightSegmentTO.setFlightNumber(jsonFS.get("flightNumber").toString());
			}
			if (jsonFS.get("cabinClassCode") != null) {
				flightSegmentTO.setCabinClassCode(jsonFS.get("cabinClassCode").toString());
			}
			if (jsonFS.get("logicalCabinClass") != null) {
				flightSegmentTO.setLogicalCabinClassCode(jsonFS.get("logicalCabinClass").toString());
			}
			if (jsonFS.get("returnFlag") != null) {
				// flightSegmentTO.setReturnFlag(Boolean.parseBoolean(jsonFS.get("returnFlag").toString()));
				flightSegmentTO.setReturnFlag(new Boolean(jsonFS.get("returnFlag").toString()).booleanValue());
			}
			if (jsonFS.get("isConnection") != null) {
				flightSegmentTO.setConnection(new Boolean(jsonFS.get("isConnection").toString()).booleanValue());
			}
			if (jsonFS.get("baggageONDGroupId") != null) {
				flightSegmentTO.setBaggageONDGroupId(jsonFS.get("baggageONDGroupId").toString());
			}
			if (jsonFS.get("pnrSegId") != null) {
				flightSegmentTO.setPnrSegId(jsonFS.get("pnrSegId").toString());
			}
		}
		return flightSegmentTO;
	}

	private static LCCAirSeatDTO getSeatDTO(JSONObject jsonSeat, String paxType) {
		LCCAirSeatDTO seatDTO = null;
		if (jsonSeat != null && !BeanUtils.nullHandler(jsonSeat.get("seatNumber")).equals("")) {
			seatDTO = new LCCAirSeatDTO();
			BigDecimal seatCharge = new BigDecimal(jsonSeat.get("seatCharge").toString());
			seatDTO.setSeatCharge(seatCharge);
			seatDTO.setSeatNumber(BeanUtils.nullHandler(jsonSeat.get("seatNumber")));
			seatDTO.setBookedPassengerType(paxType);

		}
		return seatDTO;
	}

	private static List<LCCAirSeatDTO> getExtraSeatsDTO(JSONObject jsonExtraSeat, String paxType) {

		List<LCCAirSeatDTO> extraSeats = new ArrayList<LCCAirSeatDTO>();
		if (jsonExtraSeat != null && !BeanUtils.nullHandler(jsonExtraSeat).equals("")) {
			JSONArray jsonExtraSeats = (JSONArray) jsonExtraSeat.get("extraSeats");
			if (jsonExtraSeats != null) {
				Iterator<?> iterator = jsonExtraSeats.iterator();
				while (iterator.hasNext()) {
					LCCAirSeatDTO extraSeatDTO = new LCCAirSeatDTO();
					JSONObject jExtraSeat = (JSONObject) iterator.next();
					BigDecimal seatCharge = new BigDecimal(jExtraSeat.get("seatCharge").toString());
					extraSeatDTO.setSeatCharge(seatCharge);
					extraSeatDTO.setSeatNumber(BeanUtils.nullHandler(jExtraSeat.get("seatNumber")));
					extraSeatDTO.setBookedPassengerType(paxType);
					extraSeats.add(extraSeatDTO);
				}
			}
		}
		return extraSeats;
	}

	private static List<LCCMealDTO> getMealDTOs(JSONObject jsonMeal, Integer offeredTemplateID) {
		List<LCCMealDTO> mealList = new ArrayList<LCCMealDTO>();
		if (jsonMeal != null && !BeanUtils.nullHandler(jsonMeal).equals("")) {
			JSONArray jsonMeals = (JSONArray) jsonMeal.get("meals");
			if (jsonMeals != null) {
				Iterator<?> iterator = jsonMeals.iterator();
				while (iterator.hasNext()) {
					LCCMealDTO meal = new LCCMealDTO();
					JSONObject jMeal = (JSONObject) iterator.next();
					Integer qty = new Integer(jMeal.get("mealQty").toString());
					BigDecimal charge = new BigDecimal(jMeal.get("mealCharge").toString());
					meal.setMealCharge(charge);
					meal.setAllocatedMeals(qty);
					meal.setMealCode(jMeal.get("mealCode").toString());
					meal.setMealName(jMeal.get("mealName").toString());
					meal.setAnciOffer(offeredTemplateID != null);
					meal.setOfferedTemplateID(offeredTemplateID);
					mealList.add(meal);
				}
			}
		}
		return mealList;
	}

	private static List<LCCBaggageDTO> getBaggageDTOs(JSONObject jsonBaggage, String baggageONDGrpId) {
		List<LCCBaggageDTO> baggageList = new ArrayList<LCCBaggageDTO>();
		if (jsonBaggage != null && !BeanUtils.nullHandler(jsonBaggage).equals("")) {
			JSONArray jsonBaggages = (JSONArray) jsonBaggage.get("baggages");
			if (jsonBaggages != null) {
				Iterator<?> iterator = jsonBaggages.iterator();
				while (iterator.hasNext()) {
					LCCBaggageDTO baggage = new LCCBaggageDTO();
					JSONObject jBaggage = (JSONObject) iterator.next();
					Integer qty = new Integer(jBaggage.get("baggageQty").toString());
					BigDecimal charge = new BigDecimal(jBaggage.get("baggageCharge").toString());
					baggage.setBaggageCharge(charge);
					// baggage.setAllocatedBaggages(qty);
					// baggage.setBaggageName(jBaggage.get("baggageName").toString());
					baggage.setBaggageName(jBaggage.get("baggageName").toString());
					baggage.setOndBaggageChargeId(BeanUtils.nullHandler(jBaggage.get("ondBaggageChargeId")));
					baggage.setOndBaggageGroupId(baggageONDGrpId);

					baggageList.add(baggage);
				}
			}
		}
		return baggageList;
	}

	private static List<LCCSpecialServiceRequestDTO> getSSRDTOs(JSONObject jsonSSR) {
		List<LCCSpecialServiceRequestDTO> ssrList = new ArrayList<LCCSpecialServiceRequestDTO>();
		if (jsonSSR != null && !BeanUtils.nullHandler(jsonSSR).equals("")) {
			JSONArray jsonSSRs = (JSONArray) jsonSSR.get("ssrs");
			Iterator<?> iterator = jsonSSRs.iterator();
			while (iterator.hasNext()) {
				LCCSpecialServiceRequestDTO ssr = new LCCSpecialServiceRequestDTO();
				JSONObject jSSR = (JSONObject) iterator.next();
				BigDecimal charge = new BigDecimal(BeanUtils.nullHandler(jSSR.get("charge")));
				ssr.setCharge(charge);
				ssr.setSsrCode(BeanUtils.nullHandler(jSSR.get("ssrCode")));
				ssr.setSsrName(BeanUtils.nullHandler(jsonSSR.get("ssrName")));
				ssr.setDescription(BeanUtils.nullHandler(jSSR.get("description")));
				ssr.setText(BeanUtils.nullHandler(jSSR.get("text")));
				ssrList.add(ssr);
			}
		}
		return ssrList;
	}

	private static List<LCCAirportServiceDTO> getAirportServiceDTOs(JSONObject jsonAPS) {
		List<LCCAirportServiceDTO> apsList = new ArrayList<LCCAirportServiceDTO>();
		if (jsonAPS != null && !BeanUtils.nullHandler(jsonAPS).equals("")) {
			if (jsonAPS.get("apss") != null) {
				JSONArray jsonAPSs = (JSONArray) jsonAPS.get("apss");
				Iterator<?> iterator = jsonAPSs.iterator();
				while (iterator.hasNext()) {
					LCCAirportServiceDTO aps = new LCCAirportServiceDTO();
					JSONObject jAPS = (JSONObject) iterator.next();
					BigDecimal charge = new BigDecimal(BeanUtils.nullHandler(jAPS.get("charge")));
					aps.setServiceCharge(charge);
					aps.setSsrCode(BeanUtils.nullHandler(jAPS.get("ssrCode")));
					aps.setSsrName(BeanUtils.nullHandler(jAPS.get("text")));
					aps.setSsrDescription(BeanUtils.nullHandler(jAPS.get("description")));
					aps.setAirportCode(BeanUtils.nullHandler(jAPS.get("airport")));
					aps.setApplyOn(BeanUtils.nullHandler(jAPS.get("airportType")));
					aps.setServiceNumber(BeanUtils.nullHandler(jAPS.get("serviceNumber")));
					apsList.add(aps);
				}
			}
		}

		return apsList;
	}

	private static List<LCCAirportServiceDTO> getAirportTransferDTOs(JSONObject jsonAPTransfers) throws java.text.ParseException {
		List<LCCAirportServiceDTO> transferList = new ArrayList<LCCAirportServiceDTO>();
		if (jsonAPTransfers != null && !BeanUtils.nullHandler(jsonAPTransfers).isEmpty()) {
			JSONArray jsonAPTs = (JSONArray) jsonAPTransfers.get("apts");
			Iterator<?> iterator = jsonAPTs.iterator();
			while (iterator.hasNext()) {
				LCCAirportServiceDTO airportTransfer = new LCCAirportServiceDTO();
				JSONObject jAPS = (JSONObject) iterator.next();
				BigDecimal charge = BeanUtils.nullToZero(BeanUtils.nullHandler(jAPS.get("charge")));
				airportTransfer.setServiceCharge(charge);
				airportTransfer.setSsrCode(BeanUtils.nullHandler(jAPS.get("ssrCode")));
				airportTransfer.setSsrName(BeanUtils.nullHandler(jAPS.get("text")));
				airportTransfer.setSsrDescription(BeanUtils.nullHandler(jAPS.get("description")));
				airportTransfer.setAirportCode(BeanUtils.nullHandler(jAPS.get("airport")));
				airportTransfer.setApplyOn(BeanUtils.nullHandler(jAPS.get("airportType")));

				JSONObject paxInfo = (JSONObject) jAPS.get("info");
				String date = BeanUtils.nullHandler(paxInfo.get("pdDate"));
				String hour = BeanUtils.nullHandler(paxInfo.get("pdHour"));
				String min = BeanUtils.nullHandler(paxInfo.get("pdMin"));
				String transferDate = date + " " + hour + ":" + min; // dd-MM-yyyy HH:mm
				airportTransfer.setTranferDate(transferDate);
				airportTransfer.setTransferType(BeanUtils.nullHandler(paxInfo.get("pdType")));
				airportTransfer.setTransferAddress(BeanUtils.nullHandler(paxInfo.get("pdAddress")));
				airportTransfer.setTransferContact(BeanUtils.nullHandler(paxInfo.get("pdContactNo")));
				airportTransfer.setAirportTransferId(new Integer(BeanUtils.nullHandler(jAPS.get("providerId"))));
				String existing = BeanUtils.nullHandler(jAPS.get("old"));
				if (existing != null && existing.equals("true")) {
					airportTransfer.setExisting(true);
				}
				transferList.add(airportTransfer);
			}

		}

		return transferList;
	}

	private static boolean isNullSafe(Object param) {
		if (param == null) {
			return false;
		} else {
			return !param.toString().isEmpty() && !param.equals("null") && !param.equals("undefined");
		}
	}

	private static String getTravellerReference(String paxType, String paxSequence) {
		String travelerReference = null;

		if (PassengerType.ADULT.equals(paxType)) {
			travelerReference = "A" + paxSequence + "$";
		} else if (PassengerType.CHILD.equals(paxType)) {
			travelerReference = "C" + paxSequence + "$";
		} else if (PassengerType.INFANT.equals(paxType)) {
			travelerReference = "I" + paxSequence + "$";
		} else if (PassengerType.PARENT.equals(paxType)) {
			travelerReference = "P" + paxSequence + "$";
		}

		return travelerReference;
	}

	private static LCCAirSeatDTO getSelectedSeat(JSONObject selectedSeat) throws NumberFormatException {
		String seatNumber = (String) selectedSeat.get("seatNumber");
		Double seatCharge = new Double(selectedSeat.get("seatCharge").toString());

		LCCAirSeatDTO airSeatDTO = null;
		if (seatNumber != null && !seatNumber.equals("")) {
			airSeatDTO = new LCCAirSeatDTO();
			airSeatDTO.setSeatNumber(seatNumber);
			airSeatDTO.setSeatCharge(new BigDecimal(seatCharge));
		}

		return airSeatDTO;
	}

	private static List<LCCSpecialServiceRequestDTO> getSelectedSSRs(JSONParser parser, JSONObject selectedSSR)
			throws ParseException {
		List<LCCSpecialServiceRequestDTO> serviceRequestDTOs = new ArrayList<LCCSpecialServiceRequestDTO>();

		JSONArray selectedSSRs = (JSONArray) selectedSSR.get("ssrs");
		JSONArray selectedSSRsArray = (JSONArray) parser.parse(selectedSSRs.toJSONString());
		Iterator<?> selectedMealsIterator = selectedSSRsArray.iterator();
		while (selectedMealsIterator.hasNext()) {
			JSONObject selectedSSRJObject = (JSONObject) selectedMealsIterator.next();

			String ssrCode = (String) selectedSSRJObject.get("ssrCode");
			String text = (String) selectedSSRJObject.get("text");
			String charge = selectedSSRJObject.get("charge").toString();

			LCCSpecialServiceRequestDTO serviceRequestDTO = new LCCSpecialServiceRequestDTO();
			serviceRequestDTO.setSsrCode(ssrCode);
			serviceRequestDTO.setText(text);
			serviceRequestDTO.setCharge(new BigDecimal(charge));

			serviceRequestDTOs.add(serviceRequestDTO);
		}
		return serviceRequestDTOs;
	}

	private static List<LCCMealDTO> getSelectedMeals(JSONParser parser, JSONObject selectedMeal) throws ParseException,
			NumberFormatException {

		List<LCCMealDTO> mealDTOs = new ArrayList<LCCMealDTO>();

		JSONArray selectedMeals = (JSONArray) selectedMeal.get("meals");
		JSONArray selectedMealsArray = (JSONArray) parser.parse(selectedMeals.toJSONString());
		Iterator<?> selectedMealsIterator = selectedMealsArray.iterator();
		while (selectedMealsIterator.hasNext()) {
			JSONObject selectedMealJObject = (JSONObject) selectedMealsIterator.next();

			String mealCode = (String) selectedMealJObject.get("mealCode");
			String mealName = (String) selectedMealJObject.get("mealName");
			Long mealQty = (Long) selectedMealJObject.get("mealQty");
			String subTotal = selectedMealJObject.get("subTotal").toString();

			LCCMealDTO mealDTO = new LCCMealDTO();
			mealDTO.setMealCode(mealCode);
			mealDTO.setMealName(mealName);
			mealDTO.setAllocatedMeals(mealQty.intValue());
			mealDTO.setMealCharge(new BigDecimal(subTotal));

			mealDTOs.add(mealDTO);
		}
		return mealDTOs;
	}

	private static List<LCCBaggageDTO> getSelectedBaggages(JSONParser parser, JSONObject selectedBaggage) throws ParseException,
			NumberFormatException {

		List<LCCBaggageDTO> baggageDTOs = new ArrayList<LCCBaggageDTO>();

		JSONArray selectedBaggages = (JSONArray) selectedBaggage.get("baggages");
		JSONArray selectedBaggagesArray = (JSONArray) parser.parse(selectedBaggages.toJSONString());
		Iterator<?> selectedBaggagesIterator = selectedBaggagesArray.iterator();
		while (selectedBaggagesIterator.hasNext()) {
			JSONObject selectedBaggageJObject = (JSONObject) selectedBaggagesIterator.next();

			String baggageName = (String) selectedBaggageJObject.get("baggageName");
			String subTotal = selectedBaggageJObject.get("baggageCharge").toString();

			LCCBaggageDTO baggageDTO = new LCCBaggageDTO();

			baggageDTO.setBaggageName(baggageName);
			baggageDTO.setBaggageCharge(new BigDecimal(subTotal));

			baggageDTOs.add(baggageDTO);
		}
		return baggageDTOs;
	}
}

class PaxDetailsExtractor {
	private JSONObject paxObj;
	private ApplicationEngine appEngine;

	PaxDetailsExtractor(JSONObject paxObj, ApplicationEngine oe) {
		this.paxObj = paxObj;
		this.appEngine = oe;
	}

	private String getValue(String key) {
		Object obj = paxObj.get(key);
		if (obj != null) {
			return obj.toString();
		}
		return null;
	}

	boolean isParent() {
		if (appEngine == ApplicationEngine.IBE) {
			return PaxTypeTO.PARENT.equals(getPaxType());
		} else {
			return getValue("displayInfantWith") != null && !getValue("displayInfantWith").equals("")
					&& ("Y".equals(getValue("displayInfantWith")) || (Integer.parseInt(getValue("displayInfantWith")) >= 0));
		}
	}

	String getPaxType() {
		if (appEngine == ApplicationEngine.IBE) {
			return getValue("paxType");
		} else {
			return getValue("displayAdultType");
		}
	}

	String getETicket() {
		if (appEngine == ApplicationEngine.IBE) {
			return null;
		} else {
			return getValue("displayETicket");
		}
	}

	String getPnrPaxCatFOIDNumber() {
		if (appEngine == ApplicationEngine.IBE) {
			return BeanUtils.nullHandler(getValue("foidNumber"));
		} else {
			return BeanUtils.nullHandler(getValue("displayPnrPaxCatFOIDNumber"));
		}
	}

	String getPnrPaxCatFOIDExpiry() {
		if (appEngine == ApplicationEngine.IBE) {
			return BeanUtils.nullHandler(getValue("foidExpiry"));
		} else {
			return BeanUtils.nullHandler(getValue("displayPnrPaxCatFOIDExpiry"));
		}
	}
	
	String getPnrPaxCatFOIDPlace() {
		if (appEngine == ApplicationEngine.IBE) {
			return BeanUtils.nullHandler(getValue("foidPlace"));
		} else {
			return BeanUtils.nullHandler(getValue("displayPnrPaxCatFOIDPlace"));
		}
	}

	String getPnrPaxPlaceOfBirth() {
		if (appEngine == ApplicationEngine.IBE) {
			return BeanUtils.nullHandler(getValue("placeOfBirth"));
		} else {
			return BeanUtils.nullHandler(getValue("displayPnrPaxPlaceOfBirth"));
		}
	}
	
	String getTravelDocType() {
		if (appEngine == ApplicationEngine.IBE) {
			return BeanUtils.nullHandler(getValue("travelDocumentType"));
		} else {
			return BeanUtils.nullHandler(getValue("displayTravelDocType"));
		}
	}
	
	String getVisaDocNumber() {
		if (appEngine == ApplicationEngine.IBE) {
			return BeanUtils.nullHandler(getValue("visaDocNumber"));
		} else {
			return BeanUtils.nullHandler(getValue("displayVisaDocNumber"));
		}
	}
	
	String getVisaDocPlaceOfIssue() {
		if (appEngine == ApplicationEngine.IBE) {
			return BeanUtils.nullHandler(getValue("visaDocPlaceOfIssue"));
		} else {
			return BeanUtils.nullHandler(getValue("displayVisaDocPlaceOfIssue"));
		}
	}
	
	String getVisaDocIssueDate() {
		if (appEngine == ApplicationEngine.IBE) {
			return BeanUtils.nullHandler(getValue("visaDocIssueDate"));
		} else {
			return BeanUtils.nullHandler(getValue("displayVisaDocIssueDate"));
		}
	}
	
	String getVisaApplicableCountry() {
		if (appEngine == ApplicationEngine.IBE) {
			return BeanUtils.nullHandler(getValue("visaApplicableCountry"));
		} else {
			return BeanUtils.nullHandler(getValue("displayVisaApplicableCountry"));
		}
	}
	
	String getTitle() {
		if (appEngine == ApplicationEngine.IBE) {
			return getValue("title");
		} else {
			return getValue("displayAdultTitle");
		}
	}

	String getFirstName() {
		if (appEngine == ApplicationEngine.IBE) {
			return getValue("firstName");
		} else {
			return getValue("displayAdultFirstName");
		}
	}

	String getLastName() {
		if (appEngine == ApplicationEngine.IBE) {
			return getValue("lastName");
		} else {
			return getValue("displayAdultLastName");
		}
	}

	Integer getNationalityIndex() {
		if (appEngine == ApplicationEngine.IBE) {
			return parseInt(getValue("nationality"));
		} else {
			return parseInt(getValue("displayAdultNationality"));
		}
	}

	private Integer parseInt(String strNo) {
		if (strNo != null && !"".equals(strNo)) {
			return new Integer(strNo);
		} else {
			return null;
		}
	}

	Integer getInfantId() {
		String id = null;
		if (appEngine == ApplicationEngine.IBE) {
			id = getValue("travelWith");
		} else {
			id = getValue("displayInfantWith");
		}
		if (id != null) {
			return parseInt(id);
		} else {
			return null;
		}
	}

	Integer getSequenceNumber() {
		if (getValue("seqNumber") != null && !"".equals(getValue("seqNumber"))) {
			return parseInt(getValue("seqNumber"));
		} else {
			return parseInt(getValue("paxId")) + 1;
		}
	}
	
	Integer getPaxSequenceNumber() {
		if (getValue("paxSequence") != null && !"".equals(getValue("paxSequence"))) {
			return parseInt(getValue("paxSequence"));
		} else {
			return null;
		}
	}

	String getBirthDate() {
		if (appEngine == ApplicationEngine.IBE) {
			return getValue("dateOfBirth");
		} else {
			return getValue("displayAdultDOB");
		}
	}

	String getTravelerRefNo() {
		if (appEngine == ApplicationEngine.IBE && getValue("travelerRefNumber") != null) {
			return getValue("travelerRefNumber");
		} else if (appEngine == ApplicationEngine.XBE && getValue("paxRefNum") != null) {
			return getValue("paxRefNum");
		} else {
			return null;
		}
	}

	String getPaxCategory() {
		if (appEngine == ApplicationEngine.IBE) {
			return getValue("paxCategory");
		} else {
			return getValue("displayPaxCategory");
		}
	}

	String getFirstNameInOtherLanguages() {
		return getValue("displayAdultFirstNameOl");
	}

	String getLastNameInOtherLanguages() {
		return getValue("displayAdultLastNameOl");
	}

	String getNameTranslationLanguage() {
		return getValue("displayNameTranslationLanguage");
	}

	String getTitleInOtherLanguage() {
		return getValue("displayAdultTitleOl");
	}
	
	String getPnrPaxArrivalInternationalFlightNumber() {
		return BeanUtils.nullHandler(getValue("displaypnrPaxArrivalFlightNumber"));
	}
	
	String getPnrPaxInternationalFltArrivalDate() {
		return BeanUtils.nullHandler(getValue("displaypnrPaxFltArrivalDate"));
	}
	
	String getPnrPaxInternationalFlightArrivalTime() {
		return BeanUtils.nullHandler(getValue("displaypnrPaxArrivalTime"));
	}
	
	String getPnrPaxDepartureInternationalFlightNumber() {
		return BeanUtils.nullHandler(getValue("displaypnrPaxDepartureFlightNumber"));
	}
	
	String getPnrPaxInternationalFltDepartureDate() {
		return BeanUtils.nullHandler(getValue("displaypnrPaxFltDepartureDate"));
	}
	
	String getPnrPaxInternationalFlightDepartureTime() {
		return BeanUtils.nullHandler(getValue("displaypnrPaxDepartureTime"));
	}
	
	String getPnrPaxGroupId(){
		return BeanUtils.nullHandler(getValue("displayPnrPaxGroupId"));
	}

	String getPnrPaxFfid() {
		String ffid = BeanUtils.nullHandler(getValue("ffid"));
		if (ffid.equals("")) {
			ffid = BeanUtils.nullHandler(getValue("displayFFID"));
		}
		return ffid;
	}

	String getNationalIDNo() {
		return getValue("displayNationalIDNo");
	}
}