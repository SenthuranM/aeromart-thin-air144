package com.isa.thinair.webplatform.api.v2.reservation;

import java.util.Collection;

public class SegmentFareTO {

	private Collection<PaxFareTO> paxWise;

	private String segmentCode;

	private String segmentName;

	private String totalPrice;

	public Collection<PaxFareTO> getPaxWise() {
		return paxWise;
	}

	public void setPaxWise(Collection<PaxFareTO> paxWise) {
		this.paxWise = paxWise;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getSegmentName() {
		return segmentName;
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

}
