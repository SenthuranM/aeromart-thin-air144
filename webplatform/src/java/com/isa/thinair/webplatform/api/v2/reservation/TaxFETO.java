package com.isa.thinair.webplatform.api.v2.reservation;

import java.util.List;

public class TaxFETO {

	private String taxCode;
	private String taxName;
	private String amount;
	private String carrierCode;
	private String segmentCode;
	private List<String> applicablePassengerTypeCode;
	private String applicableToDisplay;
	private String amountInSelectedCurrency;
	private String amountInLocalCurrency;
	private String localCurrencyCode;
	private String reportingChargeGroupCode;

	/**
	 * @return the taxCode
	 */
	public String getTaxCode() {
		return taxCode;
	}

	/**
	 * @param taxCode
	 *            the taxCode to set
	 */
	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	/**
	 * @return the taxName
	 */
	public String getTaxName() {
		return taxName;
	}

	/**
	 * @param taxName
	 *            the taxName to set
	 */
	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the carrierCode
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            the carrierCode to set
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the segmentCode
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            the segmentCode to set
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return the applicablePassengerTypeCode
	 */
	public List<String> getApplicablePassengerTypeCode() {
		return applicablePassengerTypeCode;
	}

	/**
	 * @param applicablePassengerTypeCode
	 *            the applicablePassengerTypeCode to set
	 */
	public void setApplicablePassengerTypeCode(List<String> applicablePassengerTypeCode) {
		this.applicablePassengerTypeCode = applicablePassengerTypeCode;
	}

	/**
	 * @return the applicableToDisplay
	 */
	public String getApplicableToDisplay() {
		return applicableToDisplay;
	}

	/**
	 * @param applicableToDisplay
	 *            the applicableToDisplay to set
	 */
	public void setApplicableToDisplay(String applicableToDisplay) {
		this.applicableToDisplay = applicableToDisplay;
	}

	/**
	 * 
	 * @return The tax amount in the selected currency.
	 */
	public String getAmountInSelectedCurrency() {
		return amountInSelectedCurrency;
	}

	/**
	 * 
	 * @param amountInSelectedCurrency
	 *            The tax amount in the selected currency.
	 */
	public void setAmountInSelectedCurrency(String amountInSelectedCurrency) {
		this.amountInSelectedCurrency = amountInSelectedCurrency;
	}

	public String getAmountInLocalCurrency() {
		return amountInLocalCurrency;
	}

	public void setAmountInLocalCurrency(String amountInLocalCurrency) {
		this.amountInLocalCurrency = amountInLocalCurrency;
	}

	public String getLocalCurrencyCode() {
		return localCurrencyCode;
	}

	public void setLocalCurrencyCode(String localCurrencyCode) {
		this.localCurrencyCode = localCurrencyCode;
	}

	/**
	 * @return the reportingChargeGroupCode
	 */
	public String getReportingChargeGroupCode() {
		return reportingChargeGroupCode;
	}

	/**
	 * @param reportingChargeGroupCode the reportingChargeGroupCode to set
	 */
	public void setReportingChargeGroupCode(String reportingChargeGroupCode) {
		this.reportingChargeGroupCode = reportingChargeGroupCode;
	}
}