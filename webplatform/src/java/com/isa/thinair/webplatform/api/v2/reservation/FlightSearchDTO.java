package com.isa.thinair.webplatform.api.v2.reservation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import org.apache.struts2.json.annotations.JSON;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants.BookingPaxType;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants.FareCategoryType;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.SubStationUtil;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;

public class FlightSearchDTO {

	private String fromAirport = null;

	private String toAirport = null;

	private String fromAirportSubStation = null;

	private String toAirportSubStation = null;

	private String selectedCurrency = null;

	private String classOfService = null;

	private String logicalCabinClass = null;

	// change fare will leads to have diffent logical cc selection in an ond
	@Deprecated
	private String fareQuoteLogicalCCSelection = null;

	private String fareQuoteOndSegLogicalCCSelection = null;

	private String departureDate = null;

	private String returnDate = null;

	private String bookingType = null;

	private Integer validity = null;

	private List<ONDSearchDTO> ondList = new ArrayList<ONDSearchDTO>();

	private Integer departureVariance = 0;

	private Integer returnVariance = 0;

	private Integer adultCount = 0;

	private Integer childCount = 0;

	private Integer infantCount = 0;

	private Boolean openReturn = false;

	private boolean returnFlag = false;

	private String searchSystem;

	private String paxType = BookingPaxType.ANY;

	private String fareType = FareCategoryType.ANY;

	private String fromAirportName = null;

	private String toAirportName = null;

	private String firstDepature = null;

	private String lastArrival = null;

	private boolean quoteOutboundFlexi = true;

	private boolean quoteInboundFlexi = true;

	private String ondQuoteFlexiStr = null;

	private String ondAvailbleFlexiStr = null;

	private String ondSelectedFlexiStr = null;

	private String flightSearchOndFlexiSelectionStr = null;

	private Map<Integer, Boolean> ondQuoteFlexi = new HashMap<Integer, Boolean>();

	private Map<Integer, Boolean> ondAvailableFlexi = new HashMap<Integer, Boolean>();

	private Map<Integer, Boolean> ondSelectedFlexi = new HashMap<Integer, Boolean>();

	private Map<Integer, Boolean> flightSearchOndFlexiSelection = new HashMap<Integer, Boolean>();

	private String resOndwiseFlexiAvilableForAnci = null;

	private String resOndwiseFlexiChargesForAnci = null;

	private boolean flexiSelected = false;

	// Half return params
	private boolean halfReturnFareQuote = false;

	private long stayOverTimeInMillis = 0;

	private boolean openReturnConfirm = false;

	private String travelAgentCode = null;

	private boolean busForDeparture = false;

	private String busConValiedTimeFrom = null;

	private String busConValiedTimeTo = null;

	private String bookingClassCode = null;

	private HashMap<String, String> prefLogicCCMap;

	private Date ticketValidTill;

	private Date lastFareQuoteDate;

	private String ondListString;

	private String ticketValidTillStr;

	private String lastFareQuoteDateStr;

	private List<String> excludeCharge;

	private String promoCode;

	private String bankIdentificationNo;

	private List<String> excludedSegFarePnrSegIds;

	private String isPreviousSearch;

	private String flightSearchDates;

	private List<Date> flightSearchDateList = new ArrayList<Date>();

	private Map<String, List<Integer>> dateFareMap = new HashMap<String, List<Integer>>();

	private String gmtOffset;

	private String sysDate;

	private String preferredBundledFares;

	private Map<String, String> prefBundledFares;

	private String preferredBookingCodes;

	private Map<String, String> prefBookingCodes;

	private String hubTimeDetails;

	private String hubTimeDetailJSON;

	private String availableBundleFareLCClass;

	private Map<String, String> mapAvailableBundleFareLCClass;

	private boolean modifyFlexiSelected;

	private String ondSegBookingClassStr;

	private Map<Integer, Map<String, String>> ondSegBookingClass;

	private boolean fromNameChange = false;

	private Boolean allowOverrideSameOrHigherFare;

	private String pointOfSale;
	
	private String fareQuoteOndSegBookingClassSelection = null;
	
	private String ondWiseCitySearchStr = null;
	
	private Map<Integer, Map<String,Boolean>> ondWiseCitySearch = new HashMap<>();

	public List<Date> getFlightSearchDateList() {
		return flightSearchDateList;
	}

	public Map<String, List<Integer>> getFlightDateFareMap() {
		return dateFareMap;
	}

	public void setFlightDateFareMap() {

		int offset = TimeZone.getDefault().getRawOffset();
		long comparisonOffset = 0;
		int userTimeZoneOffset = new Integer(this.gmtOffset) * 1000 * 60 * -1;

		if (offset > userTimeZoneOffset) {
			comparisonOffset = offset;
		} else {
			comparisonOffset = userTimeZoneOffset;
		}
		String ddMMyyy = "dd/MM/yyyy";
		SimpleDateFormat formatter = new SimpleDateFormat(ddMMyyy);
		Date today = CalendarUtil.getStartTimeOfDate(new Date());
		this.sysDate = formatter.format(today);
		if (!this.flightSearchDates.equals("")) {
			String[] searchDates = this.flightSearchDates.split(",");
			for (int i = 0; i < searchDates.length; i++) {
				Date searchDate = null;
				try {
					searchDate = formatter.parse(searchDates[i].split("#")[0]);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String offeredFare = searchDates[i].split("#")[1];

				Date searchDateWithOffset = CalendarUtil.getStartTimeOfDate(new Date((comparisonOffset + searchDate.getTime())));
				int dataDiff = (int) ((today.getTime() - searchDateWithOffset.getTime()) / (1000 * 60 * 60 * 24));
				if (dataDiff > 0) {
					if (dateFareMap.get(offeredFare) == null) {
						dateFareMap.put(offeredFare, new ArrayList<Integer>());
					}
					dateFareMap.get(offeredFare).add(dataDiff);
				}
			}
		}
	}

	public String getIsPreviousSearch() {
		return isPreviousSearch;
	}

	public void setIsPreviousSearch(String isPreviousSearch) {
		this.isPreviousSearch = isPreviousSearch;
	}

	public String getFlightSearchDates() {
		return flightSearchDates;
	}

	public void setFlightSearchDates(String flightSearchDates) {
		this.flightSearchDates = flightSearchDates;
	}

	public String getFromAirport() {
		if (fromAirport != null && fromAirport.indexOf(SubStationUtil.SUB_STATION_CODE_SEPERATOR) != -1) {
			// XBE Code can be splited. IBE Code cannot be splited - NE 29Sep2010.
			String[] splited = fromAirport.split(SubStationUtil.SUB_STATION_CODE_SEPERATOR);
			this.setFromAirport(splited[0]);
			this.setFromAirportSubStation(splited[1]);
		}
		return fromAirport;
	}

	public void setFromAirport(String fromAirport) {
		this.fromAirport = fromAirport;
	}

	public String getToAirport() {
		if (toAirport != null && toAirport.indexOf(SubStationUtil.SUB_STATION_CODE_SEPERATOR) != -1) {
			String[] splited = toAirport.split(SubStationUtil.SUB_STATION_CODE_SEPERATOR);
			this.setToAirport(splited[0]);
			this.setToAirportSubStation(splited[1]);
		}
		return toAirport;
	}

	public void setToAirport(String toAirport) {
		this.toAirport = toAirport;
	}

	public String getSelectedCurrency() {
		return selectedCurrency;
	}

	public void setSelectedCurrency(String selectedCurrency) {
		this.selectedCurrency = selectedCurrency;
	}

	public String getClassOfService() {
		if (classOfService == null) {
			return "Y";
		}
		return classOfService;
	}

	public void setClassOfService(String classOfService) {
		this.classOfService = classOfService;
	}

	public String getLogicalCabinClass() {
		return logicalCabinClass;
	}

	public void setLogicalCabinClass(String logicalCabinClass) {
		if (logicalCabinClass != null && !logicalCabinClass.trim().equals("") && !logicalCabinClass.trim().equals("null")
				&& !logicalCabinClass.trim().equals("undefined")) {
			this.logicalCabinClass = logicalCabinClass;
		}
	}

	public String getFareQuoteLogicalCCSelection() {
		return fareQuoteLogicalCCSelection;
	}

	public String getFareQuoteOndSegLogicalCCSelection() {
		return fareQuoteOndSegLogicalCCSelection;
	}

	public void setFareQuoteOndSegLogicalCCSelection(String fareQuoteOndSegLogicalCCSelection) {
		if (fareQuoteOndSegLogicalCCSelection != null && !fareQuoteOndSegLogicalCCSelection.trim().equals("")
				&& !fareQuoteOndSegLogicalCCSelection.trim().equals("null")
				&& !fareQuoteOndSegLogicalCCSelection.trim().equals("undefined")) {
			this.fareQuoteOndSegLogicalCCSelection = fareQuoteOndSegLogicalCCSelection;
		}
	}

	public ONDSearchDTO getSearchOND(int ondSequence) {
		if (ondList != null && ondList.size() > ondSequence) {
			return ondList.get(ondSequence);
		}
		return null;
	}

	public String getPreferredClassOfService(int ondSequence) {
		// if there are existing logical class of service, cabin class is unset, so that
		// from the backend, cc will be derived from logical cabin
		// if (getPreferredLogicalCC(ondSequence) != null) {
		// return null;
		// }
		if (getSearchOND(ondSequence) != null && getSearchOND(ondSequence).getClassOfService() != null) {
			String classOfService = getSearchOND(ondSequence).getClassOfService();
			if (classOfService != null) {
				return classOfService;
			}
		}
		return getClassOfService();
	}

	public String getPreferredBookingClass(int ondSequence) {

		Integer bundledFarePeriodId = getPreferredBundledFarePeriodId(ondSequence);

		if (bundledFarePeriodId == null) {
			if (getSearchOND(ondSequence) != null && getSearchOND(ondSequence).getBookingClass() != null) {
				String bookingClass = getSearchOND(ondSequence).getBookingClass();
				if (bookingClass == null) {
					return getBookingClassCode();
				} else {
					return bookingClass;
				}
			}
			return getBookingClassCode();
		} else {
			return getPreferredBookingClassCode(ondSequence);
		}

	}

	public String getPreferredBookingType(int ondSequence) {
		if (getSearchOND(ondSequence) != null && getSearchOND(ondSequence).getBookingType() != null) {
			String bookingType = getSearchOND(ondSequence).getBookingType();
			if (bookingType == null) {
				return getBookingType();
			} else {
				return bookingType;
			}
		}
		return getBookingType();
	}

	public String getPreferredLogicalCC(int ondSequence) {
		// No need to check getSearchOND(ondSequence).getLogicalCabinClass() is null because,
		// if OND list has ONDSearchDTO obj with the given ond sequence logical cabin class should correctly set
		if (getSearchOND(ondSequence) != null) {
			return getSearchOND(ondSequence).getLogicalCabinClass();
		}
		if (this.prefLogicCCMap != null && this.prefLogicCCMap.containsKey(ondSequence + "")) {
			return this.prefLogicCCMap.get(ondSequence + "");
		}
		return logicalCabinClass;
	}

	public void setFareQuoteLogicalCCSelection(String fareQuoteLogicalCCSelection) {
		this.prefLogicCCMap = JSONFETOParser.parseMap(HashMap.class, String.class, fareQuoteLogicalCCSelection);
		if (prefLogicCCMap == null) {
			prefLogicCCMap = new HashMap<String, String>();
		}
		this.fareQuoteLogicalCCSelection = fareQuoteLogicalCCSelection;
	}

	public Integer getPreferredBundledFarePeriodId(int ondSequence) {
		String strOndSeq = Integer.toString(ondSequence);
		if (this.prefBundledFares != null && this.prefBundledFares.containsKey(strOndSeq)) {
			String strBundledFarePeriodId = this.prefBundledFares.get(strOndSeq);
			if (strBundledFarePeriodId != null && !"".equals(strBundledFarePeriodId) && !"null".equalsIgnoreCase(strBundledFarePeriodId)) {
				return Integer.parseInt(strBundledFarePeriodId);
			}
		}
		return null;
	}

	public String getPreferredBundledFares() {
		return preferredBundledFares;
	}

	public void setPreferredBundledFares(String preferredBundledFares) {
		this.preferredBundledFares = preferredBundledFares;

		this.prefBundledFares = JSONFETOParser.parseMap(HashMap.class, String.class, preferredBundledFares);
		if (prefBundledFares == null) {
			prefBundledFares = new HashMap<String, String>();
		}
	}

	private String getPreferredBookingClassCode(int ondSequence) {
		String strOndSeq = Integer.toString(ondSequence);
		if (this.prefBookingCodes != null && this.prefBookingCodes.containsKey(strOndSeq)) {
			return this.prefBookingCodes.get(strOndSeq);
		}
		return null;
	}

	public String getPreferredBookingCodes() {
		return preferredBookingCodes;
	}

	public void setPreferredBookingCodes(String preferredBookingCodes) {
		this.preferredBookingCodes = preferredBookingCodes;

		this.prefBookingCodes = JSONFETOParser.parseMap(HashMap.class, String.class, preferredBookingCodes);
		if (prefBookingCodes == null) {
			prefBookingCodes = new HashMap<String, String>();
		}
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public String getBookingType() {
		return bookingType;
	}

	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	public Integer getValidity() {
		return validity;
	}

	public void setValidity(Integer validity) {
		this.validity = validity;
	}

	public Integer getDepartureVariance() {
		return departureVariance;
	}

	public void setDepartureVariance(Integer departureVariance) {
		if (departureVariance == null) {
			departureVariance = new Integer(0);
		}
		this.departureVariance = departureVariance;
	}

	public Integer getReturnVariance() {
		return returnVariance;
	}

	public void setReturnVariance(Integer returnVariance) {
		if (returnVariance == null) {
			returnVariance = new Integer(0);
		}
		this.returnVariance = returnVariance;
	}

	public Integer getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(Integer adultCount) {
		this.adultCount = adultCount;
	}

	public Integer getChildCount() {
		return childCount;
	}

	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}

	public Integer getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(Integer infantCount) {
		this.infantCount = infantCount;
	}

	public Boolean getOpenReturn() {
		return openReturn;
	}

	public void setOpenReturn(Boolean openReturn) {
		this.openReturn = openReturn;
	}

	public String getSearchSystem() {
		return searchSystem;
	}

	public void setSearchSystem(String searchSystem) {
		this.searchSystem = searchSystem;
	}

	/**
	 * @return the paxType
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return the fareType
	 */
	public String getFareType() {
		return fareType;
	}

	/**
	 * @param fareType
	 *            the fareType to set
	 */
	public void setFareType(String fareType) {
		this.fareType = fareType;
	}

	public boolean isReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(boolean returnFlag) {
		this.returnFlag = returnFlag;
	}

	public String getFromAirportName() {
		return fromAirportName;
	}

	public void setFromAirportName(String fromAirportName) {
		this.fromAirportName = fromAirportName;
	}

	public String getToAirportName() {
		return toAirportName;
	}

	public void setToAirportName(String toAirportName) {
		this.toAirportName = toAirportName;
	}

	public String getFirstDeparture() {
		return firstDepature;
	}

	public void setFirstDeparture(String firstDeparture) {
		this.firstDepature = firstDeparture;
	}

	public String getLastArrival() {
		return lastArrival;
	}

	public void setLastArrival(String lastArrival) {
		this.lastArrival = lastArrival;
	}

	// We have change backend to pass immediate previous segment arrival segment. This can be removed with open return
	// changes
	public long getStayOverTimeInMillis() {
		return stayOverTimeInMillis;
	}

	// We have change backend to pass immediate previous segment arrival segment. This can be removed with open return
	// changes
	public void setStayOverTimeInMillis(long stayOverTimeInMillis) {
		this.stayOverTimeInMillis = stayOverTimeInMillis;
	}

	public boolean isHalfReturnFareQuote() {
		return halfReturnFareQuote;
	}

	public void setHalfReturnFareQuote(boolean halfReturnFareQuote) {
		this.halfReturnFareQuote = halfReturnFareQuote;
	}

	public boolean isQuoteOutboundFlexi() {
		return quoteOutboundFlexi;
	}

	public void setQuoteOutboundFlexi(boolean quoteOutboundFlexi) {
		this.quoteOutboundFlexi = quoteOutboundFlexi;
	}

	public boolean isQuoteInboundFlexi() {
		return quoteInboundFlexi;
	}

	public void setQuoteInboundFlexi(boolean quoteInboundFlexi) {
		this.quoteInboundFlexi = quoteInboundFlexi;
	}

	public boolean isFlexiSelected() {
		return flexiSelected;
	}

	public void setFlexiSelected(boolean flexiSelected) {
		this.flexiSelected = flexiSelected;
	}

	public String getOndQuoteFlexiStr() {
		return ondQuoteFlexiStr;
	}

	public void setOndQuoteFlexiStr(String ondQuoteFlexiStr) {
		this.ondQuoteFlexiStr = ondQuoteFlexiStr;

		if (this.ondQuoteFlexiStr != null && !"".equals(this.ondQuoteFlexiStr)) {
			Map<Integer, Boolean> intBlnMap = WebplatformUtil.convertMap(JSONFETOParser.parseMap(HashMap.class, String.class,
					this.ondQuoteFlexiStr));
			if (AppSysParamsUtil.isFlexiFareEnabled()) {
				setOndQuoteFlexi(intBlnMap);
			} else {
				Map<Integer, Boolean> updatedIntBlnMap = new HashMap<Integer, Boolean>();
				if (intBlnMap != null && intBlnMap.size() > 0) {
					for (Entry<Integer, Boolean> entry : intBlnMap.entrySet()) {
						updatedIntBlnMap.put(entry.getKey(), false);
					}
				}
				setOndQuoteFlexi(updatedIntBlnMap);
			}
		}
	}

	public String getOndAvailbleFlexiStr() {
		return ondAvailbleFlexiStr;
	}

	public void setOndAvailbleFlexiStr(String ondAvailbleFlexiStr) {
		this.ondAvailbleFlexiStr = ondAvailbleFlexiStr;

		if (this.ondAvailbleFlexiStr != null && !"".equals(this.ondAvailbleFlexiStr)) {
			setOndAvailableFlexi(WebplatformUtil.convertMap(JSONFETOParser.parseMap(HashMap.class, String.class,
					this.ondAvailbleFlexiStr)));
		}
	}

	public String getOndSelectedFlexiStr() {
		return ondSelectedFlexiStr;
	}

	public void setOndSelectedFlexiStr(String ondSelectedFlexiStr) {
		this.ondSelectedFlexiStr = ondSelectedFlexiStr;

		if (this.ondSelectedFlexiStr != null && !"".equals(this.ondSelectedFlexiStr)) {
			setOndSelectedFlexi(WebplatformUtil.convertMap(JSONFETOParser.parseMap(HashMap.class, String.class,
					this.ondSelectedFlexiStr)));
			for (Entry<Integer, Boolean> flexiEntry : this.ondSelectedFlexi.entrySet()) {
				if (flexiEntry.getValue()) {
					this.flexiSelected = true;
					break;
				}
			}
		}
	}

	@JSON(serialize = false)
	public Map<Integer, Boolean> getOndQuoteFlexi() {
		return ondQuoteFlexi;
	}

	public void setOndQuoteFlexi(Map<Integer, Boolean> ondQuoteFlexi) {
		this.ondQuoteFlexi = ondQuoteFlexi;
	}

	@JSON(serialize = false)
	public Map<Integer, Boolean> getOndAvailableFlexi() {
		return ondAvailableFlexi;
	}

	public void setOndSelectedFlexi(Map<Integer, Boolean> ondSelectedFlexi) {
		this.ondSelectedFlexi = ondSelectedFlexi;
	}

	@JSON(serialize = false)
	public Map<Integer, Boolean> getOndSelectedFlexi() {
		return ondSelectedFlexi;
	}

	public String getFlightSearchOndFlexiSelectionStr() {
		return flightSearchOndFlexiSelectionStr;
	}

	public void setFlightSearchOndFlexiSelectionStr(String flightSearchOndFlexiSelectionStr) {
		this.flightSearchOndFlexiSelectionStr = flightSearchOndFlexiSelectionStr;

		if (this.flightSearchOndFlexiSelectionStr != null && !"".equals(this.flightSearchOndFlexiSelectionStr)) {
			setFlightSearchOndFlexiSelection(WebplatformUtil.convertMap(JSONFETOParser.parseMap(HashMap.class, String.class,
					this.flightSearchOndFlexiSelectionStr)));
		}
	}

	@JSON(serialize = false)
	public Map<Integer, Boolean> getFlightSearchOndFlexiSelection() {
		return flightSearchOndFlexiSelection;
	}

	public void setFlightSearchOndFlexiSelection(Map<Integer, Boolean> flightSearchOndFlexiSelection) {
		this.flightSearchOndFlexiSelection = flightSearchOndFlexiSelection;
	}

	public void setOndAvailableFlexi(Map<Integer, Boolean> ondAvailableFlexi) {
		this.ondAvailableFlexi = ondAvailableFlexi;
	}

	public String getResOndwiseFlexiAvilableForAnci() {
		return resOndwiseFlexiAvilableForAnci;
	}

	public void setResOndwiseFlexiAvilableForAnci(String resOndwiseFlexiAvilableForAnci) {
		this.resOndwiseFlexiAvilableForAnci = resOndwiseFlexiAvilableForAnci;
	}

	public String getResOndwiseFlexiChargesForAnci() {
		return resOndwiseFlexiChargesForAnci;
	}

	public void setResOndwiseFlexiChargesForAnci(String resOndwiseFlexiChargesForAnci) {
		this.resOndwiseFlexiChargesForAnci = resOndwiseFlexiChargesForAnci;
	}

	public boolean isOpenReturnConfirm() {
		return openReturnConfirm;
	}

	public void setOpenReturnConfirm(boolean openReturnConfirm) {
		this.openReturnConfirm = openReturnConfirm;
	}

	public String getFromAirportSubStation() {
		return fromAirportSubStation;
	}

	public void setFromAirportSubStation(String fromAirportSubStation) {
		this.fromAirportSubStation = fromAirportSubStation;
	}

	public String getToAirportSubStation() {
		return toAirportSubStation;
	}

	public void setToAirportSubStation(String toAirportSubStation) {
		this.toAirportSubStation = toAirportSubStation;
	}

	public String getTravelAgentCode() {
		return travelAgentCode;
	}

	public void setTravelAgentCode(String travelAgentCode) {
		this.travelAgentCode = travelAgentCode;
	}

	public boolean isBusForDeparture() {
		return busForDeparture;
	}

	public void setBusForDeparture(boolean busForDeparture) {
		this.busForDeparture = busForDeparture;
	}

	public String getBusConValiedTimeFrom() {
		return busConValiedTimeFrom;
	}

	public void setBusConValiedTimeFrom(String busConValiedTimeFrom) {
		this.busConValiedTimeFrom = busConValiedTimeFrom;
	}

	public String getBusConValiedTimeTo() {
		return busConValiedTimeTo;
	}

	public void setBusConValiedTimeTo(String busConValiedTimeTo) {
		this.busConValiedTimeTo = busConValiedTimeTo;
	}

	public String getBookingClassCode() {
		return bookingClassCode;
	}

	public void setBookingClassCode(String bookingClassCode) {
		if (bookingClassCode != null && !bookingClassCode.trim().equals("")) {
			this.bookingClassCode = bookingClassCode;
		}
	}

	public void setOndListString(String ondListString) {
		this.ondListString = ondListString;

		if (ondListString != null && !ondListString.equals(""))
			setOndList(JSONFETOParser.parseCollection(List.class, ONDSearchDTO.class, ondListString));
	}

	public String getOndListString() {
		return this.ondListString;
	}

	public List<ONDSearchDTO> getOndList() {
		return ondList;
	}

	public void setOndList(List<ONDSearchDTO> ondList) {
		this.ondList = ondList;
	}

	public Date getLastFareQuoteDate() {
		return lastFareQuoteDate;
	}

	public void setTicketValidTill(Date ticketValidTill) {
		this.ticketValidTill = ticketValidTill;
	}

	public void setLastFareQuoteDate(Date lastFareQuoteDate) {
		this.lastFareQuoteDate = lastFareQuoteDate;
	}

	public void setLastFareQuoteDateStr(String lastFareQuoteDate) {
		this.lastFareQuoteDateStr = lastFareQuoteDate;
		this.lastFareQuoteDate = JSONFETOParser.parseDate(lastFareQuoteDate);
	}

	public Date getTicketValidTill() {
		return ticketValidTill;
	}

	public void setTicketValidTillStr(String ticketValidTill) {
		this.ticketValidTillStr = ticketValidTill;
		this.ticketValidTill = JSONFETOParser.parseDate(ticketValidTill);
	}

	public String getTicketValidTillStr() {
		return this.ticketValidTillStr;
	}

	public String getLastFareQuoteDateStr() {
		return this.lastFareQuoteDateStr;
	}

	public List<String> getExcludeCharge() {
		return excludeCharge;
	}

	public void setExcludeCharge(List<String> excludeCharge) {
		this.excludeCharge = excludeCharge;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public String getBankIdentificationNo() {
		return bankIdentificationNo;
	}

	public void setBankIdentificationNo(String bankIdentificationNo) {
		this.bankIdentificationNo = bankIdentificationNo;
	}

	public List<String> getExcludedSegFarePnrSegIds() {
		return excludedSegFarePnrSegIds;
	}

	public void setExcludedSegFarePnrSegIds(List<String> excludedSegFarePnrSegIds) {
		this.excludedSegFarePnrSegIds = excludedSegFarePnrSegIds;
	}

	/**
	 * @return the gmtOffset
	 */
	public String getGmtOffset() {
		return gmtOffset;
	}

	/**
	 * @param gmtOffset
	 *            the gmtOffset to set
	 */
	public void setGmtOffset(String gmtOffset) {
		this.gmtOffset = gmtOffset;
		if (!"".equals(gmtOffset) && gmtOffset != null) {
			setFlightDateFareMap();
		}
	}

	/**
	 * @return the sysDate
	 */
	public String getSysDate() {
		return sysDate;
	}

	/**
	 * @param sysDate
	 *            the sysDate to set from IBE
	 */
	public void setSysDate(String sysDate) {
		this.sysDate = sysDate;
	}

	public String getHubTimeDetails() {
		return hubTimeDetails;
	}

	public void setHubTimeDetails(String hubTimeDetails) {
		this.hubTimeDetails = hubTimeDetails;
	}

	public String getHubTimeDetailJSON() {
		return hubTimeDetailJSON;
	}

	public void setHubTimeDetailJSON(String hubTimeDetailJSON) {
		this.hubTimeDetailJSON = hubTimeDetailJSON;
	}

	public String getAvailableBundleFareLCClass() {
		return availableBundleFareLCClass;
	}

	public void setAvailableBundleFareLCClass(String availableBundleFareLCClass) {
		this.availableBundleFareLCClass = availableBundleFareLCClass;
	}

	public Map<String, String> getMapAvailableBundleFareLCClass() {
		return mapAvailableBundleFareLCClass;
	}

	public void setMapAvailableBundleFareLCClass(Map<String, String> mapAvailableBundleFareLCClass) {
		this.mapAvailableBundleFareLCClass = mapAvailableBundleFareLCClass;
	}

	public boolean isModifyFlexiSelected() {
		return modifyFlexiSelected;
	}

	public void setModifyFlexiSelected(boolean modifyFlexiSelected) {
		this.modifyFlexiSelected = modifyFlexiSelected;
	}

	@JSON(serialize = false)
	public Map<Integer, Map<String, String>> getOndSegBookingClass() {
		return ondSegBookingClass;
	}

	@SuppressWarnings("unchecked")
	public void setOndSegBookingClassStr(String ondSegBookingClassStr) throws org.json.simple.parser.ParseException {
		this.ondSegBookingClassStr = ondSegBookingClassStr;
		if (this.ondSegBookingClassStr != null && !"".equals(ondSegBookingClassStr)) {
			this.ondSegBookingClass = new HashMap<Integer, Map<String, String>>();
			JSONParser parser = new JSONParser();
			JSONObject jObject = (JSONObject) parser.parse(this.ondSegBookingClassStr);

			if (jObject != null) {
				Set<String> ondSeqs = (Set<String>) jObject.keySet();
				for (String ondSeq : ondSeqs) {
					Integer ondSequence = Integer.parseInt(ondSeq);
					if (!this.ondSegBookingClass.containsKey(ondSequence)) {
						this.ondSegBookingClass.put(ondSequence, new HashMap<String, String>());
					}

					Map<String, String> segBookingClass = this.ondSegBookingClass.get(ondSequence);

					JSONObject segBkObj = (JSONObject) jObject.get(ondSeq);
					if (segBkObj != null) {
						Set<String> segs = (Set<String>) segBkObj.keySet();
						for (String segCode : segs) {
							segBookingClass.put(segCode, (String) segBkObj.get(segCode));
						}
					}
				}
			}
		}
	}

	public String getOndSegBookingClassStr() {
		return ondSegBookingClassStr;
	}

	public Map<String, String> getPreferredOndSegmentBookingClasses(int ondSequence) {
		if (this.ondSegBookingClass != null && this.ondSegBookingClass.containsKey(ondSequence)) {
			return this.ondSegBookingClass.get(ondSequence);
		}

		return null;
	}

	public boolean isFromNameChange() {
		return fromNameChange;
	}

	public void setFromNameChange(boolean fromNameChange) {
		this.fromNameChange = fromNameChange;
	}

	public String getPointOfSale() {
		return pointOfSale;
	}

	public void setPointOfSale(String pointOfSale) {
		this.pointOfSale = pointOfSale;
	}

	public Boolean getAllowOverrideSameOrHigherFare() {
		return allowOverrideSameOrHigherFare;
	}

	public void setAllowOverrideSameOrHigherFare(Boolean allowOverrideSameOrHigherFare) {
		this.allowOverrideSameOrHigherFare = allowOverrideSameOrHigherFare;
	}

	public String getFareQuoteOndSegBookingClassSelection() {
		return fareQuoteOndSegBookingClassSelection;
	}

	public void setFareQuoteOndSegBookingClassSelection(
			String fareQuoteOndSegBookingClassSelection) {
		
		if (fareQuoteOndSegBookingClassSelection != null && !fareQuoteOndSegBookingClassSelection.trim().equals("")
				&& !fareQuoteOndSegBookingClassSelection.trim().equals("null")
				&& !fareQuoteOndSegBookingClassSelection.trim().equals("undefined")) {
			this.fareQuoteOndSegBookingClassSelection = fareQuoteOndSegBookingClassSelection;
		}

	}

	public String getOndWiseCitySearchStr() {
		return ondWiseCitySearchStr;
	}

	@SuppressWarnings("unchecked")
	public void setOndWiseCitySearchStr(String ondWiseCitySearchStr) throws org.json.simple.parser.ParseException {
		this.ondWiseCitySearchStr = ondWiseCitySearchStr;

		if (!StringUtil.isNullOrEmpty(this.ondWiseCitySearchStr)) {
			Map<Integer, Map<String, Boolean>> ondWiseCitySearchMap = new HashMap<>();
			JSONParser parser = new JSONParser();
			JSONObject jObject = (JSONObject) parser.parse(this.ondWiseCitySearchStr);

			if (jObject != null) {
				Set<String> ondSeqs = (Set<String>) jObject.keySet();
				for (String ondSeq : ondSeqs) {
					Integer ondSequence = Integer.parseInt(ondSeq);
					if (!ondWiseCitySearchMap.containsKey(ondSequence)) {
						ondWiseCitySearchMap.put(ondSequence, new HashMap<String, Boolean>());
					}

					Map<String, Boolean> originDestinationCitySearch = ondWiseCitySearchMap.get(ondSequence);

					JSONObject statusCodesObj = (JSONObject) jObject.get(ondSeq);
					if (statusCodesObj != null) {
						Set<String> statusCodes = (Set<String>) statusCodesObj.keySet();
						for (String statusCode : statusCodes) {
							originDestinationCitySearch.put(statusCode, (Boolean) statusCodesObj.get(statusCode));
						}
					}
				}

				setOndWiseCitySearch(ondWiseCitySearchMap);
			}
		}

	}

	public Map<Integer, Map<String, Boolean>> getOndWiseCitySearch() {
		return ondWiseCitySearch;
	}

	public void setOndWiseCitySearch(Map<Integer, Map<String, Boolean>> ondWiseCitySearch) {
		this.ondWiseCitySearch = ondWiseCitySearch;
	}
		
}
