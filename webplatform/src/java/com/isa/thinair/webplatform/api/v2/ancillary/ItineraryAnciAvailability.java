package com.isa.thinair.webplatform.api.v2.ancillary;

public class ItineraryAnciAvailability {
	private boolean seatAvailable;
	private boolean mealAvailable;
	private boolean ssrAvailable;
	private boolean baggageAvailable;
	private boolean airportServiceAvailable;
	private boolean airportTransferAvailable;
	private boolean autoCheckinAvailable;

	public boolean isSeatAvailable() {
		return seatAvailable;
	}

	public void setSeatAvailable(boolean seatAvailable) {
		this.seatAvailable = seatAvailable;
	}

	public boolean isMealAvailable() {
		return mealAvailable;
	}

	public void setMealAvailable(boolean mealAvailable) {
		this.mealAvailable = mealAvailable;
	}

	public boolean isSsrAvailable() {
		return ssrAvailable;
	}

	public void setSsrAvailable(boolean ssrAvailable) {
		this.ssrAvailable = ssrAvailable;
	}

	public boolean isAllAvailable() {
		return (this.seatAvailable && this.mealAvailable && this.ssrAvailable);
	}

	/**
	 * @return the baggageAvailable
	 */
	public boolean isBaggageAvailable() {
		return baggageAvailable;
	}

	/**
	 * @param baggageAvailable
	 *            the baggageAvailable to set
	 */
	public void setBaggageAvailable(boolean baggageAvailable) {
		this.baggageAvailable = baggageAvailable;
	}

	/**
	 * @return the airportServiceAvailable
	 */
	public boolean isAirportServiceAvailable() {
		return airportServiceAvailable;
	}

	/**
	 * @param airportServiceAvailable
	 *            the airportServiceAvailable to set
	 */
	public void setAirportServiceAvailable(boolean airportServiceAvailable) {
		this.airportServiceAvailable = airportServiceAvailable;
	}

	public boolean isAirportTransferAvailable() {
		return airportTransferAvailable;
	}

	public void setAirportTransferAvailable(boolean airportTransferAvailable) {
		this.airportTransferAvailable = airportTransferAvailable;
	}

	/**
	 * @return the autoCheckinAvailable
	 */
	public boolean isAutoCheckinAvailable() {
		return autoCheckinAvailable;
	}

	/**
	 * @param autoCheckinAvailable the autoCheckinAvailable to set
	 */
	public void setAutoCheckinAvailable(boolean autoCheckinAvailable) {
		this.autoCheckinAvailable = autoCheckinAvailable;
	}

}
