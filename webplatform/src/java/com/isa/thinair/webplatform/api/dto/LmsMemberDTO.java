package com.isa.thinair.webplatform.api.dto;


public class LmsMemberDTO {
	
	private String emailId; 
    
	private String firstName;
     
	private String middleName;
	 
	private String lastName;
	
	private String dateOfBirth;
	
	private String mobileNumber;
	
	private String phoneNumber; 
	
	private String passportNum;
	
	private String language;
	
	private String nationalityCode;
	
	private String residencyCode;
	
	private String headOFEmailId;
	
	private String genderCode;
	
	private String emailStatus;
	
	private String availablePoints;
	
	private String appCode;
	
	private String refferedEmail;

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the dateOfBirth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the passportNum
	 */
	public String getPassportNum() {
		return passportNum;
	}

	/**
	 * @param passportNum the passportNum to set
	 */
	public void setPassportNum(String passportNum) {
		this.passportNum = passportNum;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the nationalityCode
	 */
	public String getNationalityCode() {
		return nationalityCode;
	}

	/**
	 * @param nationalityCode the nationalityCode to set
	 */
	public void setNationalityCode(String nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	/**
	 * @return the residencyCode
	 */
	public String getResidencyCode() {
		return residencyCode;
	}

	/**
	 * @param residencyCode the residencyCode to set
	 */
	public void setResidencyCode(String residencyCode) {
		this.residencyCode = residencyCode;
	}

	/**
	 * @return the headOFEmailId
	 */
	public String getHeadOFEmailId() {
		return headOFEmailId;
	}

	/**
	 * @param headOFEmailId the headOFEmailId to set
	 */
	public void setHeadOFEmailId(String headOFEmailId) {
		this.headOFEmailId = headOFEmailId;
	}

	/**
	 * @return the genderCode
	 */
	public String getGenderCode() {
		return genderCode;
	}

	/**
	 * @param genderCode the genderCode to set
	 */
	public void setGenderCode(String genderCode) {
		this.genderCode = genderCode;
	}

	/**
	 * @return the appCode
	 */
	public String getAppCode() {
		return appCode;
	}

	/**
	 * @param appCode the appCode to set
	 */
	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}


	/**
	 * @return
	 */
	public String getEmailStatus() {
		return emailStatus;
	}

	/**
	 * @param emailStatus
	 */
	public void setEmailStatus(String emailStatus) {
		this.emailStatus = emailStatus;
	}

	/**
	 * @return
	 */
	public String getAvailablePoints() {
		return availablePoints;
	}

	/**
	 * @param availablePoints
	 */
	public void setAvailablePoints(String availablePoints) {
		this.availablePoints = availablePoints;
	}

	/**
	 * @return
	 */
	public String getRefferedEmail() {
		return refferedEmail;
	}

	/**
	 * @param refferedEmail
	 */
	public void setRefferedEmail(String refferedEmail) {
		this.refferedEmail = refferedEmail;
	}


}
