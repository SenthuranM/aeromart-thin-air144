package com.isa.thinair.webplatform.api.dto;

import java.util.Map;

/**
 * Transfer object for airport services
 * 
 * @author Dumindag
 */
public class AirportServiceTO {

	private boolean elegMAAS = false;

	private boolean elegLounge = false;

	private Map mapSSRchargeData;

	private String langCode = "EN";

	/**
	 * @return Returns the elegLounge.
	 */
	public boolean isElegLounge() {
		return elegLounge;
	}

	/**
	 * @param elegLounge
	 *            The elegLounge to set.
	 */
	public void setElegLounge(boolean elegLounge) {
		this.elegLounge = elegLounge;
	}

	/**
	 * @return Returns the elegMAAS.
	 */
	public boolean isElegMAAS() {
		return elegMAAS;
	}

	/**
	 * @param elegMAAS
	 *            The elegMAAS to set.
	 */
	public void setElegMAAS(boolean elegMAAS) {
		this.elegMAAS = elegMAAS;
	}

	/**
	 * @return Returns the mapSSRchargeData.
	 */
	public Map getMapSSRchargeData() {
		return mapSSRchargeData;
	}

	/**
	 * @param mapSSRchargeData
	 *            The mapSSRchargeData to set.
	 */
	public void setMapSSRchargeData(Map mapSSRchargeData) {
		this.mapSSRchargeData = mapSSRchargeData;
	}

	/**
	 * @return Returns the langCode
	 */
	public String getLangCode() {
		return langCode;
	}

	/**
	 * @param langCode
	 *            the langCode to set
	 */
	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}

}
