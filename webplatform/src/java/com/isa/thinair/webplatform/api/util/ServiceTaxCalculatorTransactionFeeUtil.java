package com.isa.thinair.webplatform.api.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

public class ServiceTaxCalculatorTransactionFeeUtil extends ServiceTaxCalculatorUtil {
	
	public static void populatePaxWiseCreditCardCharge(Collection<ReservationPaxTO> paxList, ExternalChgDTO creditCardChgDTO,
			BigDecimal ccChargeAmount, Map<Integer, List<LCCClientExternalChgDTO>> paxWiseExternalCharges,
			Map<Integer, String> paxWisePaxTypes, List<FlightSegmentTO> flightSegmentTOs, int payableAdultChildCount,
			int payableInfantCount, Set<Integer> payablePaxSequnce) {

		if (paxList != null && !paxList.isEmpty() && ccChargeAmount != null && ccChargeAmount.doubleValue() > 0
				&& paxWiseExternalCharges != null && paxWisePaxTypes != null) {

			int[] payablePaxCount = ServiceTaxCalculatorUtil.getPayablePaxCount(paxList, null);
			int adultChildCount = payablePaxCount[0];
			int infantCount = payablePaxCount[1];
			int totalPaxCount = adultChildCount + infantCount; 				
			
			int totalPayablePaxCount = payableAdultChildCount + payableInfantCount;
			
			BigDecimal[] paxWiseCardFee = AccelAeroCalculator.roundAndSplit(ccChargeAmount, (totalPayablePaxCount));
			int adultIndex = 0;
			int infantIndex = 0;			
			boolean isPaymentforAllPasengers = totalPaxCount == totalPayablePaxCount;
			
			for (ReservationPaxTO pax : paxList) {
				  // Reimplement method to cover all scnarios 
				   if (!payablePaxSequnce.isEmpty() && !isPaymentforAllPasengers) {
					   
					  if (payablePaxSequnce.contains(pax.getSeqNumber())) {
						   
						    List<LCCClientExternalChgDTO> chgDTOs = new ArrayList<>();
							chgDTOs.addAll(ServiceTaxCalculatorUtil.segmentWiseCreditCardChargeDistribution(flightSegmentTOs,
									paxWiseCardFee[0], creditCardChgDTO, false));
	
							appendPaxCharges(pax.getSeqNumber(), paxWiseExternalCharges, chgDTOs);
							paxWisePaxTypes.put(pax.getSeqNumber(), pax.getPaxType());
							   
					   } 	   
					   
					   
				   } else {
					   
					   if (PaxTypeTO.INFANT.equals(pax.getPaxType())) {
							continue;
						}

						List<LCCClientExternalChgDTO> chgDTOs = new ArrayList<>();
						chgDTOs.addAll(ServiceTaxCalculatorUtil.segmentWiseCreditCardChargeDistribution(flightSegmentTOs,
								paxWiseCardFee[adultIndex], creditCardChgDTO, false));

						appendPaxCharges(pax.getSeqNumber(), paxWiseExternalCharges, chgDTOs);
						paxWisePaxTypes.put(pax.getSeqNumber(), pax.getPaxType());

						if (pax.getIsParent()) {
							int infantSequence = adultChildCount + infantIndex;
							List<LCCClientExternalChgDTO> infantChgDTOs = new ArrayList<>();
							infantChgDTOs.addAll(ServiceTaxCalculatorUtil.segmentWiseCreditCardChargeDistribution(flightSegmentTOs,
									paxWiseCardFee[infantSequence], creditCardChgDTO, false));
							appendPaxCharges((infantSequence + 1), paxWiseExternalCharges, infantChgDTOs);
							paxWisePaxTypes.put((infantSequence + 1), PaxTypeTO.INFANT);

							infantIndex++;

						}
						adultIndex++;		
					   
				   }			  
				
			}
		}

	}

}
