package com.isa.thinair.webplatform.api.v2.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class PaxPaymentUtil {

	private BigDecimal totalWithAncillary;
	private BigDecimal totalExtChgs;
	private BigDecimal infantPaymentAmount;	
	private Map<PaymentMethod, BigDecimal> payments = new HashMap<PaymentMethod, BigDecimal>();
	private Map<PaymentMethod, BigDecimal> infantPayments = new HashMap<PaymentMethod, BigDecimal>();
	private List<PaymentMethod> consumingOrder = new ArrayList<PaymentMethod>();
	Iterator<PaymentMethod> payItr = null;
	private BigDecimal remainingLoyalty = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal remainingVoucherAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	public PaxPaymentUtil(BigDecimal totalWithAncillary, BigDecimal totalExtChgs, BigDecimal loyaltyPayAmount,
			BigDecimal lmsMemberPayment, BigDecimal voucherPaymentAmount, BigDecimal infantPaymentAmount) {
		this.totalWithAncillary = totalWithAncillary;
		this.totalExtChgs = totalExtChgs;
		this.infantPaymentAmount = infantPaymentAmount;

		if (hasPayment(lmsMemberPayment)) {
			consumePayment(PaymentMethod.LMS, lmsMemberPayment);
		}

		if (hasPayment(voucherPaymentAmount)) {
			BigDecimal remainingTotalwithAncillary = this.totalWithAncillary;
			if (voucherPaymentAmount.compareTo(remainingTotalwithAncillary) > 0) {
				consumePayment(PaymentMethod.VOUCHER, remainingTotalwithAncillary);
				remainingVoucherAmount = AccelAeroCalculator.subtract(voucherPaymentAmount, remainingTotalwithAncillary);
			} else {
				consumePayment(PaymentMethod.VOUCHER, voucherPaymentAmount);
			}
		}
		
		
		if (hasPayment(loyaltyPayAmount)) {
			BigDecimal remainingTotalwithAncillary = this.totalWithAncillary;
			if (loyaltyPayAmount.compareTo(remainingTotalwithAncillary) > 0) {
				consumePayment(PaymentMethod.MASHREQ_LOYALTY, remainingTotalwithAncillary);
				remainingLoyalty = AccelAeroCalculator.subtract(loyaltyPayAmount, remainingTotalwithAncillary);
			} else {
				consumePayment(PaymentMethod.MASHREQ_LOYALTY, loyaltyPayAmount);
			}
		}

		if (hasPayment(this.totalWithAncillary)) {
			consumePayment(PaymentMethod.CRED, this.totalWithAncillary);
		}

	}

	public BigDecimal getRemainingLoyalty() {
		return remainingLoyalty;
	}
	
	public BigDecimal getRemainingVoucherAmount() {
		return remainingVoucherAmount;
	}

	public boolean hasConsumablePayments() {
		if (totalWithAncillary.equals(AccelAeroCalculator.getDefaultBigDecimalZero()) && consumingOrder.size() > 0) {
			if (payItr == null) {
				payItr = consumingOrder.iterator();
			}
			return payItr.hasNext();
		}
		return false;
	}

	public PaymentMethod getNextPaymentMethod() {
		return payItr.next();
	}

	public BigDecimal getPaymentAmount(PaymentMethod payMethod) {
		return payments.get(payMethod);
	}
	
	public BigDecimal getInfantPaymentAmount(PaymentMethod payMethod) {
		return infantPayments.get(payMethod);
	}

	private void consumePayment(PaymentMethod method, BigDecimal payAmount) {
		if (!payments.containsKey(method) && totalWithAncillary.compareTo(payAmount) >= 0) {
			if (totalExtChgs.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0) {
				totalWithAncillary = AccelAeroCalculator.subtract(totalWithAncillary, totalExtChgs);
				payAmount = AccelAeroCalculator.subtract(payAmount, totalExtChgs);
				totalExtChgs = AccelAeroCalculator.getDefaultBigDecimalZero();
			}
			consumingOrder.add(method);
			if (infantPaymentAmount.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) == 0
					|| AccelAeroCalculator.subtract(totalWithAncillary, infantPaymentAmount).compareTo(payAmount) > 0) {
				payments.put(method, payAmount);
				totalWithAncillary = AccelAeroCalculator.subtract(totalWithAncillary, payAmount);
			} else {
				BigDecimal adultTotal = AccelAeroCalculator.subtract(totalWithAncillary, infantPaymentAmount);
				payments.put(method, adultTotal);
				infantPayments.put(method, AccelAeroCalculator.subtract(payAmount, adultTotal));
				totalWithAncillary = AccelAeroCalculator.subtract(totalWithAncillary, payAmount);
			}
			
		}
	}

	private boolean hasPayment(BigDecimal payAmt) {
		return (payAmt != null && payAmt.compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) > 0);
	}
}
