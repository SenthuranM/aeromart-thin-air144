package com.isa.thinair.webplatform.api.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCarrierOndGroup;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientFlexiExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.utils.ReleaseTimeUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.ClientCommonInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;

public class WebplatformUtil {

	private static Log log = LogFactory.getLog(WebplatformUtil.class);

	public static boolean isOnholdRestricted(Collection<OndFareDTO> ondFareDTOs) {
		Collection<SegmentSeatDistsDTO> segCollection = new ArrayList<SegmentSeatDistsDTO>();
		for (OndFareDTO ondFareDTO : ondFareDTOs) {
			segCollection.addAll(ondFareDTO.getSegmentSeatDistsDTO());
		}

		if (segCollection.size() > 0) {
			return checkOnHoldRestrictions(segCollection);
		} else {
			return false;
		}

	}

	private static boolean checkOnHoldRestrictions(Collection<SegmentSeatDistsDTO> colsegSeatDto) {
		Collection<SeatDistribution> colSeatDist;
		Iterator<SegmentSeatDistsDTO> itersegSeats;
		Iterator<SeatDistribution> iterSeatDist;
		SegmentSeatDistsDTO segSeatDto;
		SeatDistribution seatDist;

		itersegSeats = colsegSeatDto.iterator();
		while (itersegSeats.hasNext()) {

			segSeatDto = (SegmentSeatDistsDTO) itersegSeats.next();
			colSeatDist = segSeatDto.getEffectiveSeatDistribution();
			if (colSeatDist != null) {
				iterSeatDist = colSeatDist.iterator();
				while (iterSeatDist.hasNext()) {
					seatDist = (SeatDistribution) iterSeatDist.next();
					if (seatDist.isOnholdRestricted()) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Returns the confirmed departure segments
	 * 
	 * @param colReservationSegmentDTO
	 */
	public static Collection getConfirmedDepartureSegments(Collection colReservationSegmentDTO) {
		return ReleaseTimeUtil.getConfirmedDepartureSegments(colReservationSegmentDTO);
	}

	/**
	 * Returns the first flight segment's departure datetime in Zulu
	 */
	public static Date getFirstFlightDepartureTimeZulu(Collection<OndFareDTO> ondFareDTOs) {
		return ReleaseTimeUtil.getFirstFlightDepartureTimeZulu(ondFareDTOs);
	}

	/**
	 * Returns the release time stamp
	 * 
	 * @param colFlightSegmentDTOs
	 * @param privilegesKeys
	 * @param enforcePrivilegesCheck
	 * @param agentCode
	 *            agent code
	 * @param appIndicator
	 * @return
	 * @throws ModuleException
	 */
	public static Date getReleaseTimeStamp(Collection colFlightSegmentDTOs, Collection privilegesKeys,
			boolean enforcePrivilegesCheck, String agentCode, String appIndicator, OnHoldReleaseTimeDTO onHoldReleaseTimeDTO)
			throws ModuleException {
		return ReleaseTimeUtil.getReleaseTimeStamp(colFlightSegmentDTOs, privilegesKeys, enforcePrivilegesCheck, agentCode,
				appIndicator, onHoldReleaseTimeDTO);
	}

	/**
	 * Utility method to get onhold restricted for this reservatio or not
	 * 
	 * @param ondFareDTOList
	 * @return
	 */
	public static boolean isOnHoldRestricted(Collection<OndFareDTO> ondFareDTOList) {

		for (OndFareDTO ondFareDTO : ondFareDTOList) {
			Collection<SegmentSeatDistsDTO> colsegSeatDto = ondFareDTO.getSegmentSeatDistsDTO();
			if (colsegSeatDto != null) {
				for (SegmentSeatDistsDTO segSeatDistDTO : colsegSeatDto) {
					Collection<SeatDistribution> effectiveSeatDistList = segSeatDistDTO.getEffectiveSeatDistribution();
					if (effectiveSeatDistList != null) {
						for (SeatDistribution effectiveDist : effectiveSeatDistList) {
							if (effectiveDist.isOnholdRestricted()) {
								return true;
							}
						}
					}
				}
			}
		}

		return false;
	}

	/**
	 * Calculates maximum allowed release time when extending hold (implicitly assumes user has buffer time onhold
	 * privilege)
	 * 
	 * @param colFlightSegmentDTOs
	 * @return
	 */
	public static Date getMaxAllowedReleaseTimeZulu(Collection colFlightSegmentDTOs) {
		return ReleaseTimeUtil.getMaxAllowedReleaseTimeZulu(colFlightSegmentDTOs);
	}

	/**
	 * Calculates Hold Releasetime considering the flight closure gap
	 */
	public static Date calculateClosureReleaseTime(Date departureDateTimeZulu) {
		return ReleaseTimeUtil.calculateClosureReleaseTime(departureDateTimeZulu);
	}

	public static Date getCurrentZuluDateTime() {
		return ReleaseTimeUtil.getCurrentZuluDateTime();
	}

	/**
	 * Validates requested payment broker configuration available. If corresponding pay gateway not available,
	 * {@link ModuleException} is thrown.
	 * 
	 * @param module
	 * @param carrierCode
	 * @param paymentCurrencyCode
	 * @return
	 * @throws ModuleException
	 */
	public static IPGIdentificationParamsDTO
			validateAndPrepareIPGConfigurationParamsDTO(Integer ipgId, String paymentCurrencyCode) throws ModuleException {

		if (ipgId == null && paymentCurrencyCode == null) {
			log.warn("IPG identification params with carrier or pay currency unspecified found " + "[ipgId=" + ipgId
					+ ",payCurCode=" + paymentCurrencyCode + "]");
		}

		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId, paymentCurrencyCode);

		if (!WPModuleUtils.getPaymentBrokerBD().checkForIPG(ipgIdentificationParamsDTO)) {
			// paymentCurrencyCode = AppSysParamsUtil.getDefaultPGCurrency();
			// ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId, paymentCurrencyCode);
			throw new ModuleException("module.invalid.payment.currency");
		}
		ipgIdentificationParamsDTO.setFQIPGConfigurationName(ipgId + "_" + paymentCurrencyCode);
		ipgIdentificationParamsDTO.set_isIPGConfigurationExists(true);

		return ipgIdentificationParamsDTO;
	}

	public static IPGIdentificationParamsDTO prepareIPGConfigurationParamsDTOPerPGWId(Integer ipgId) throws ModuleException {

		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId, null);
		String pgwName = WPModuleUtils.getPaymentBrokerBD().checkForIPGPerPGWId(ipgIdentificationParamsDTO);
		if (pgwName == null) {
			throw new ModuleException("module.invalid.payment.currency");
		}
		ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId, pgwName.split("_")[1]);
		ipgIdentificationParamsDTO.setFQIPGConfigurationName(pgwName);
		ipgIdentificationParamsDTO.set_isIPGConfigurationExists(true);

		return ipgIdentificationParamsDTO;
	}

	/**
	 * Returns the release time stamp
	 * 
	 * @param colFlightSegmentDTOs
	 * @param privilegesKeys
	 * @param enforcePrivilegesCheck
	 * @return
	 * @throws ModuleException
	 */
	public static Object[] getRestrictedBufferTime(Collection colFlightSegmentDTOs) throws ModuleException {
		return ReleaseTimeUtil.getRestrictedBufferTime(colFlightSegmentDTOs);
	}

	/**
	 * 
	 * Get client's IP from HttpServletRequest object
	 * 
	 * @param request
	 * @return
	 */
	public static String getClientIpAddress(HttpServletRequest request) {

		// clientIps = clientIP, proxyIP1, proxyIP2
		String clientIps = request.getHeader("X-Forwarded-For");
		String clientIp = null;

		if (clientIps != null && !clientIps.trim().isEmpty()) {

			String[] clientIpArray = clientIps.split(Constants.COMMA_SEPARATOR);
			if (clientIpArray.length > 0) {
				for (String ipAddress : clientIpArray) {
					if (ipAddress != null && !ipAddress.trim().isEmpty()) {
						clientIp = ipAddress.trim();
						break;
					}
				}
			}

		}

		if (clientIp == null || clientIp.equals("")) {
			clientIp = request.getRemoteAddr();
		}

		return clientIp;
	}

	public static ClientCommonInfoDTO getClientInfoDTO(HttpServletRequest request) {
		ClientCommonInfoDTO clientInfoDTO = new ClientCommonInfoDTO();
		String clientIp = WebplatformUtil.getClientIpAddress(request);
		clientInfoDTO.setIpAddress(clientIp);
		clientInfoDTO.setFwdIpAddress(request.getHeader(""));
		clientInfoDTO.setUrl(request.getRequestURL().toString());
		clientInfoDTO.setBrowser(request.getHeader("User-Agent"));
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		// DO not pass the defaultCarrier directly here. It will affect the dry user functionalities.Insted use the
		// userPrincipal.getDefaultCarrierCode
		// if it's not null
		String defaultCArrier = (userPrincipal == null) ? AppSysParamsUtil.getDefaultCarrierCode() : userPrincipal
				.getDefaultCarrierCode();
		clientInfoDTO.setCarrierCode(defaultCArrier);
		return clientInfoDTO;
	}

	/**
	 * Handles null value and replace with a "" value. This also trims if a String value exist
	 * 
	 * @param string
	 * @return
	 */
	public static String nullHandler(Object object) {
		if (object == null) {
			return "";
		} else {
			return object.toString().trim();
		}
	}

	public static Map<Integer, Boolean> convertMap(Map<String, String> stringMap) {
		Map<Integer, Boolean> intBlnMap = new HashMap<Integer, Boolean>();
		if (stringMap != null && stringMap.size() > 0) {
			for (Entry<String, String> entry : stringMap.entrySet()) {
				intBlnMap.put(new Integer(entry.getKey()), new Boolean(entry.getValue()));
			}
		}
		return intBlnMap;
	}

	public static void updateOndSequence(String jsonOnds, List<FlightSegmentTO> flightSegmentTOs) {
		if (jsonOnds != null && !"".equals(jsonOnds)) {
			@SuppressWarnings("unchecked")
			List<LCCClientCarrierOndGroup> ondGroups = JSONFETOParser.parseCollection(List.class, LCCClientCarrierOndGroup.class,
					jsonOnds);
			ondGroups = filterConfirmedOndGroups(ondGroups);
			ondGroups = SortUtil.sortByDepartureDateTime(ondGroups);

			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				if (flightSegmentTO.getPnrSegId() != null && !"".equals(flightSegmentTO.getPnrSegId())) {
					int pnrSegId = Integer.parseInt(flightSegmentTO.getPnrSegId());
					int ondSequence = 0;
					for (LCCClientCarrierOndGroup ondGroup : ondGroups) {
						Collection<Integer> ondPnrSegIds = ondGroup.getCarrierPnrSegIds();
						if (ondPnrSegIds != null
								&& ondPnrSegIds.contains(pnrSegId)
								&& WebplatformUtil.nullHandler(ondGroup.getCarrierCode()).equals(
										WebplatformUtil.nullHandler(flightSegmentTO.getOperatingAirline()))) {
							flightSegmentTO.setOndSequence(ondSequence);
							break;
						}
						ondSequence++;
					}
				}
			}
		}
	}

	public static Map<String, TreeMap<String, List<LCCClientExternalChgDTO>>> populatePaxExternalCharges(
			Collection<ReservationPaxTO> paxList) {
		Map<String, List<LCCClientExternalChgDTO>> paxExternalCharges = new TreeMap<String, List<LCCClientExternalChgDTO>>();

		Map<String, TreeMap<String, List<LCCClientExternalChgDTO>>> paxCarrierExternalCharges = new HashMap<String, TreeMap<String, List<LCCClientExternalChgDTO>>>();

		for (ReservationPaxTO reservationPaxTO : paxList) {
			String paxKey = reservationPaxTO.getSeqNumber() + "_" + reservationPaxTO.getPaxType() + "_"
					+ ((reservationPaxTO.getIsParent()) ? "Y" : "N");

			List<LCCClientExternalChgDTO> externalChgDTOs = reservationPaxTO.getExternalCharges();
			Map<EXTERNAL_CHARGES, LCCClientExternalChgDTO> extChgMap = new HashMap<EXTERNAL_CHARGES, LCCClientExternalChgDTO>();
			Map<String, Map<EXTERNAL_CHARGES, LCCClientExternalChgDTO>> carrierExtChgMap = new HashMap<String, Map<EXTERNAL_CHARGES, LCCClientExternalChgDTO>>();
			TreeMap<String, List<LCCClientExternalChgDTO>> carrierExtCharges = new TreeMap<String, List<LCCClientExternalChgDTO>>();

			if (externalChgDTOs != null) {

				BigDecimal removedCharges = reservationPaxTO.getToRemoveAncillaryTotal();

				Comparator<LCCClientExternalChgDTO> exChargeComparator = new Comparator<LCCClientExternalChgDTO>() {
					public int compare(LCCClientExternalChgDTO c1, LCCClientExternalChgDTO c2) {
						return c1.getExternalCharges().toString().compareTo(c2.getExternalCharges().toString());
					}
				};

				Collections.sort(externalChgDTOs, exChargeComparator);

				for (LCCClientExternalChgDTO lccClientExternalChgDTO : externalChgDTOs) {
					String chargeCarrierCode = lccClientExternalChgDTO.getCarrierCode();
					if (!carrierExtChgMap.containsKey(chargeCarrierCode)) {
						extChgMap = new HashMap<EXTERNAL_CHARGES, LCCClientExternalChgDTO>();
					} else {
						extChgMap = carrierExtChgMap.get(chargeCarrierCode);
					}

					if (!extChgMap.containsKey(lccClientExternalChgDTO.getExternalCharges())) {
						LCCClientExternalChgDTO extChg = null;
						if (lccClientExternalChgDTO.getExternalCharges() == EXTERNAL_CHARGES.FLEXI_CHARGES) {
							extChg = getLCCClientFlexiExternalChgDTO(lccClientExternalChgDTO);
						} else {
							extChg = lccClientExternalChgDTO.clone();
						}

						extChg.setAmount(reservationPaxTO.getCarrierAncillaryCharge(lccClientExternalChgDTO.getExternalCharges(),
								chargeCarrierCode));
						if (AccelAeroCalculator.isGreaterThan(removedCharges, AccelAeroCalculator.getDefaultBigDecimalZero())) {
							BigDecimal addedCharges = extChg.getAmount();
							if (removedCharges.compareTo(addedCharges) == 0) {
								removedCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
								extChg.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
							} else if (removedCharges.compareTo(addedCharges) > 0) {
								removedCharges = AccelAeroCalculator.subtract(removedCharges, addedCharges);
								extChg.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
							} else if (removedCharges.compareTo(addedCharges) < 0) {
								BigDecimal remainingExgCharge = AccelAeroCalculator.subtract(addedCharges, removedCharges);
								extChg.setAmount(remainingExgCharge);
								removedCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
							}
						}

						extChgMap.put(extChg.getExternalCharges(), extChg);
					}
					carrierExtChgMap.put(chargeCarrierCode, extChgMap);

					/*
					 * if (!extChgMap.containsKey(lccClientExternalChgDTO.getExternalCharges())) {
					 * LCCClientExternalChgDTO extChg = null; if (lccClientExternalChgDTO.getExternalCharges() ==
					 * EXTERNAL_CHARGES.FLEXI_CHARGES) { extChg = (LCCClientFlexiExternalChgDTO)
					 * lccClientExternalChgDTO; } else { extChg = lccClientExternalChgDTO.clone(); }
					 * 
					 * extChg.setAmount(reservationPaxTO.getAncillaryCharge(lccClientExternalChgDTO.getExternalCharges())
					 * ); if (AccelAeroCalculator.isGreaterThan(removedCharges,
					 * AccelAeroCalculator.getDefaultBigDecimalZero())) { BigDecimal addedCharges = extChg.getAmount();
					 * if (removedCharges.compareTo(addedCharges) == 0) { removedCharges =
					 * AccelAeroCalculator.getDefaultBigDecimalZero();
					 * extChg.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero()); } else if
					 * (removedCharges.compareTo(addedCharges) > 0) { removedCharges =
					 * AccelAeroCalculator.subtract(removedCharges, addedCharges);
					 * extChg.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero()); } else if
					 * (removedCharges.compareTo(addedCharges) < 0) { BigDecimal remainingExgCharge =
					 * AccelAeroCalculator.subtract(addedCharges, removedCharges); extChg.setAmount(remainingExgCharge);
					 * removedCharges = AccelAeroCalculator.getDefaultBigDecimalZero(); } }
					 * 
					 * extChgMap.put(extChg.getExternalCharges(), extChg); }
					 */
				}
			}

			for (String carrierCode : carrierExtChgMap.keySet()) {
				carrierExtCharges.put(carrierCode, new ArrayList<LCCClientExternalChgDTO>(carrierExtChgMap.get(carrierCode)
						.values()));
			}

			paxCarrierExternalCharges.put(paxKey, carrierExtCharges);
		}

		return paxCarrierExternalCharges;
	}

	public static Map<Integer, Map<String, Map<String, BigDecimal>>> populatePaxProductDueAmounts(
			ReservationBalanceTO reservationBalanceTO) {
		Map<Integer, Map<String, Map<String, BigDecimal>>> paxCarrierProductDueAmount = new HashMap<Integer, Map<String, Map<String, BigDecimal>>>();

		for (LCCClientPassengerSummaryTO paxSummary : reservationBalanceTO.getPassengerSummaryList()) {
			Integer paxSeq = PaxTypeUtils.getPaxSeq(paxSummary.getTravelerRefNumber());
			paxCarrierProductDueAmount.put(paxSeq, paxSummary.getCarrierProductCodeWiseAmountDue());
		}

		return paxCarrierProductDueAmount;
	}

	public static Map<String, Double> getProductTotalRedeemPoints(Map<Integer, Map<String, Double>> paxProductPoints) {
		Map<String, Double> productTotalPoints = new LinkedHashMap<String, Double>();

		for (Entry<Integer, Map<String, Double>> paxEntry : paxProductPoints.entrySet()) {
			Map<String, Double> productPoints = paxEntry.getValue();
			for (Entry<String, Double> productEntry : productPoints.entrySet()) {
				double pointSum = 0;
				if (!productTotalPoints.containsKey(productEntry.getKey())) {
					pointSum = productEntry.getValue();
				} else {
					pointSum = productEntry.getValue() + productTotalPoints.get(productEntry.getKey());
				}

				if (pointSum > 0) {
					productTotalPoints.put(productEntry.getKey(), pointSum);
				}
			}
		}

		return productTotalPoints;
	}

	private static List<LCCClientCarrierOndGroup> filterConfirmedOndGroups(List<LCCClientCarrierOndGroup> allOndGroups) {
		List<LCCClientCarrierOndGroup> ondGroups = new ArrayList<LCCClientCarrierOndGroup>();

		for (LCCClientCarrierOndGroup lccClientCarrierOndGroup : allOndGroups) {
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(lccClientCarrierOndGroup.getStatus())) {
				ondGroups.add(lccClientCarrierOndGroup);
			}
		}

		return ondGroups;
	}

	private static LCCClientFlexiExternalChgDTO getLCCClientFlexiExternalChgDTO(LCCClientExternalChgDTO lccClientExternalChgDTO) {
		LCCClientFlexiExternalChgDTO lccClientFlexiExternalChgDTO = new LCCClientFlexiExternalChgDTO();
		lccClientFlexiExternalChgDTO.setAirportCode(lccClientExternalChgDTO.getAirportCode());
		lccClientFlexiExternalChgDTO.setAmount(lccClientExternalChgDTO.getAmount());
		lccClientFlexiExternalChgDTO.setAmountConsumedForPayment(lccClientExternalChgDTO.isAmountConsumedForPayment());
		lccClientFlexiExternalChgDTO.setAnciOffer(lccClientExternalChgDTO.isAnciOffer());
		lccClientFlexiExternalChgDTO.setApplyOn(lccClientExternalChgDTO.getApplyOn());
		lccClientFlexiExternalChgDTO.setCarrierCode(lccClientExternalChgDTO.getCarrierCode());
		lccClientFlexiExternalChgDTO.setCode(lccClientExternalChgDTO.getCode());
		lccClientFlexiExternalChgDTO.setExternalCharges(lccClientExternalChgDTO.getExternalCharges());
		lccClientFlexiExternalChgDTO.setFlightRefNumber(lccClientExternalChgDTO.getFlightRefNumber());
		lccClientFlexiExternalChgDTO.setJourneyType(lccClientExternalChgDTO.getJourneyType());
		lccClientFlexiExternalChgDTO.setOfferedAnciTemplateID(lccClientExternalChgDTO.getOfferedAnciTemplateID());
		lccClientFlexiExternalChgDTO.setOndBaggageChargeGroupId(lccClientExternalChgDTO.getOndBaggageChargeGroupId());
		lccClientFlexiExternalChgDTO.setOndBaggageGroupId(lccClientExternalChgDTO.getOndBaggageGroupId());
		lccClientFlexiExternalChgDTO.setSegmentCode(lccClientExternalChgDTO.getSegmentCode());
		lccClientFlexiExternalChgDTO.setSegmentSequence(lccClientExternalChgDTO.getSegmentSequence());
		lccClientFlexiExternalChgDTO.setTransferAddress(lccClientExternalChgDTO.getTransferAddress());
		lccClientFlexiExternalChgDTO.setTransferContact(lccClientExternalChgDTO.getTransferContact());
		lccClientFlexiExternalChgDTO.setTransferDate(lccClientExternalChgDTO.getTransferDate());
		lccClientFlexiExternalChgDTO.setTransferType(lccClientExternalChgDTO.getTransferType());

		return lccClientFlexiExternalChgDTO;
	}

	@SuppressWarnings("unchecked")
	public static String getRequestParameterNameValue(HttpServletRequest request) {
		StringBuffer nameValueString = new StringBuffer();
		Enumeration<String> enumeration = request.getParameterNames();
		while (enumeration.hasMoreElements()) {
			String fieldName = (String) enumeration.nextElement();
			nameValueString.append(fieldName).append("=");
			nameValueString.append(request.getParameter(fieldName)).append("|");
		}

		return nameValueString.toString();
	}

	/**
	 * This method can be used to get the maximum adults allowed in a reservation for the logged in user for relevant channel
	 * 
	 * @param userID user id of the logged user
	 * @param channel reservation channel
	 * @return maximum adults allowed in a reservation for the logged in user for relevant channel
	 */
	public static int getMaxAdultCount(String userID , String channel) {
		
		int maxAdultCountAppParam; // this will hold the max adult count defined in the app parameter for corresponding channel
		
		Integer maxAdultCount = WPModuleUtils.getSecurityBD().getMaxAdultCount(userID); // this will give the 
        																				// minimum of the max adult count of the agent and the user 			
		if(channel.equals("XBE")){		
			maxAdultCountAppParam = AppSysParamsUtil.getMaxAdultCountXBE();		
		}else if(channel.equals("WS")){			
			maxAdultCountAppParam = AppSysParamsUtil.getMaxAdultCountWS();		
		}else if(channel.equals("GDS")){			
			maxAdultCountAppParam = AppSysParamsUtil.getMaxAdultCountGDS();
		}else{
			maxAdultCountAppParam = 0;
		}		 
		
		if(maxAdultCount == null){
			return maxAdultCountAppParam;
		}
		
		if(maxAdultCountAppParam < maxAdultCount){		
			return maxAdultCountAppParam;		
		}else{		
			return maxAdultCount;		
		}
				
	}

}
