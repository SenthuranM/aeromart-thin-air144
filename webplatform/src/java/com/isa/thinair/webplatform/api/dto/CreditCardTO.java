package com.isa.thinair.webplatform.api.dto;

import com.isa.thinair.airreservation.api.utils.BeanUtils;

/**
 * @author Thushara
 * 
 */
public class CreditCardTO {

	private String cardType;

	private String expiryDate;

	private String cardNo;

	private String cardHoldersName;

	private String address;

	private String cardCVV;

	private String billingAddress1;

	private String billingAddress2;

	private String city;

	private String postalCode;

	private String state;
	
	private String paymentMethod;

	/**
	 * @return cardType the cardType
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * Sets the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @return expiryDate the expiryDate
	 */
	public String getExpiryDate() {
		return expiryDate;
	}

	/**
	 * Sets the expiryDate to set
	 */
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getExpiryDateYYMMFormat() {
		String expiryDate = BeanUtils.nullHandler(this.getExpiryDate());
		if (expiryDate.length() == 4) {
			return expiryDate.substring(2, 4) + expiryDate.substring(0, 2);
		} else {
			return null;
		}
	}

	/**
	 * @return cardNo the cardNo
	 */
	public String getCardNo() {
		return cardNo;
	}

	/**
	 * Sets the cardNo to set
	 */
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	/**
	 * @return cardHoldersName the cardHoldersName
	 */
	public String getCardHoldersName() {
		return cardHoldersName;
	}

	/**
	 * Sets the cardHoldersName to set
	 */
	public void setCardHoldersName(String cardHoldersName) {
		this.cardHoldersName = cardHoldersName;
	}

	/**
	 * @return address the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return cardCVV the cardCVV
	 */
	public String getCardCVV() {
		return cardCVV;
	}

	/**
	 * Sets the cardCVV to set
	 */
	public void setCardCVV(String cardCVV) {
		this.cardCVV = cardCVV;
	}

	public void setBillingAddress1(String billingAddress1) {
		this.billingAddress1 = billingAddress1;
	}

	public String getBillingAddress1() {
		return billingAddress1;
	}

	public void setBillingAddress2(String billingAddress2) {
		this.billingAddress2 = billingAddress2;
	}

	public String getBillingAddress2() {
		return billingAddress2;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCity() {
		return city;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getState() {
		return state;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}	

}