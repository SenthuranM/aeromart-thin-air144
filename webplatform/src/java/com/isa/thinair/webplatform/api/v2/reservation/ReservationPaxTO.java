package com.isa.thinair.webplatform.api.v2.reservation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAutomaticCheckinDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.webplatform.api.util.ReservationUtil;

public class ReservationPaxTO implements Serializable, IChargeablePax, Comparable<ReservationPaxTO> {

	private static final long serialVersionUID = 1L;

	private String paxType;

	private String title;

	private String firstName;

	private String lastName;

	private String firstNameInOtherLanguage;

	private String lastNameInOtherLanguage;

	private String nameTranslationLanguage;

	private String titleInOtherLanguage;

	private Boolean isParent;

	private Integer seqNumber;

	private Integer nationality;

	private Date dateOfBirth;

	private Integer infantWith;
	/* Identify Pax */
	private String travelerRefNumber;

	private String eTicket;

	private String paxCategory;

	private PaxAdditionalInfoDTO paxAdditionalInfo;

	private List<LCCClientExternalChgDTO> externalCharges;

	private List<LCCSelectedSegmentAncillaryDTO> selectedAncillaries;

	private List<LCCSelectedSegmentAncillaryDTO> ancillariesToRemove;

	private List<LCCSelectedSegmentAncillaryDTO> ancillariesToUpdate;

	private Map<String, String> mapFlightRefAndOndBaggageId;

	private Date pnrPaxIntlFltArrivalDate;

	private String pnrPaxArrivalIntlFltNumber;

	private Date pnrPaxIntlFltDepartureDate;

	private String pnrPaxDepartureIntlFltNumber;

	private String pnrPaxGroupId;

	private List<LCCClientExternalChgDTO> infantExternalCharges;

	public ExternalChgDTO getAncillaryChargeDTO(EXTERNAL_CHARGES externalCharge) {
		BigDecimal total = getAncillaryCharge(externalCharge);
		ExternalChgDTO chgDTO = new ExternalChgDTO();
		chgDTO.setExternalChargesEnum(externalCharge);
		chgDTO.setAmount(total);
		return chgDTO;
	}

	public BigDecimal getAncillaryCharge(EXTERNAL_CHARGES externalCharge) {
		BigDecimal total = BigDecimal.ZERO;
		if (getExternalCharges() != null) {
			for (LCCClientExternalChgDTO charge : getExternalCharges()) {
				if (charge.getExternalCharges() == externalCharge) {
					total = AccelAeroCalculator.add(total, charge.getAmount());
				}
			}
		}
		return total;
	}
	
	public BigDecimal getServiceTaxExcludingServiceTaxForCC() {
		BigDecimal total = BigDecimal.ZERO;
		if (getExternalCharges() != null) {
			for (LCCClientExternalChgDTO charge : getExternalCharges()) {
				if (charge.getExternalCharges() == EXTERNAL_CHARGES.SERVICE_TAX && !charge.isServiceTaxAppliedToCCFee()) {
					total = AccelAeroCalculator.add(total, charge.getAmount());
				}
			}
		}
		return total;
	}

	public BigDecimal getCarrierAncillaryCharge(EXTERNAL_CHARGES externalCharge, String carrierCode) {
		BigDecimal total = BigDecimal.ZERO;
		if (getExternalCharges() != null) {
			for (LCCClientExternalChgDTO charge : getExternalCharges()) {
				if (charge.getCarrierCode().equals(carrierCode) && charge.getExternalCharges() == externalCharge) {
					total = AccelAeroCalculator.add(total, charge.getAmount());
				}
			}
		}
		return total;
	}

	public BigDecimal getAncillaryCharge(EXTERNAL_CHARGES externalCharge, String carrierCode) {
		BigDecimal total = BigDecimal.ZERO;
		if (getExternalCharges() != null) {
			for (LCCClientExternalChgDTO charge : getExternalCharges()) {
				if (charge.getExternalCharges() == externalCharge && carrierCode.equals(charge.getCarrierCode())) {
					total = AccelAeroCalculator.add(total, charge.getAmount());
				}
			}
		}
		return total;
	}

	public BigDecimal getRemovedAncillaryCharge(EXTERNAL_CHARGES externalCharge, String carrierCode) {
		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();

		List<LCCSelectedSegmentAncillaryDTO> removeAnciList = getAncillariesToRemove();
		if (removeAnciList != null) {
			for (LCCSelectedSegmentAncillaryDTO segAnci : removeAnciList) {

				String fltCarrierCode = FlightRefNumberUtil
						.getOperatingAirline(segAnci.getFlightSegmentTO().getFlightRefNumber());

				if (carrierCode != null && !carrierCode.equals(fltCarrierCode)) {
					continue;
				}

				switch (externalCharge) {
				case SEAT_MAP:
					if (segAnci.getAirSeatDTO() != null && segAnci.getAirSeatDTO().getSeatNumber() != null
							&& !segAnci.getAirSeatDTO().getSeatNumber().trim().equals("")) {
						total = AccelAeroCalculator.add(total, segAnci.getAirSeatDTO().getSeatCharge());
					}
					break;
				case MEAL:
					if (segAnci.getMealDTOs() != null) {
						for (LCCMealDTO meal : segAnci.getMealDTOs()) {
							total = AccelAeroCalculator.add(total,
									AccelAeroCalculator.multiply(meal.getMealCharge(), new BigDecimal(meal.getAllocatedMeals())));
						}
					}
					break;
				case BAGGAGE:
					if (segAnci.getBaggageDTOs() != null) {
						for (LCCBaggageDTO baggage : segAnci.getBaggageDTOs()) {
							total = AccelAeroCalculator.add(total, baggage.getBaggageCharge());
						}
					}
					break;
				case INFLIGHT_SERVICES:
					if (segAnci.getSpecialServiceRequestDTOs() != null) {
						for (LCCSpecialServiceRequestDTO ssr : segAnci.getSpecialServiceRequestDTOs()) {
							total = AccelAeroCalculator.add(total, ssr.getCharge());
						}
					}
					break;
				case AIRPORT_SERVICE:
					if (segAnci.getAirportServiceDTOs() != null) {
						for (LCCAirportServiceDTO airportService : segAnci.getAirportServiceDTOs()) {
							total = AccelAeroCalculator.add(total, airportService.getServiceCharge());
						}
					}
					break;
				case AIRPORT_TRANSFER:
					if (segAnci.getAirportTransferDTOs() != null) {
						for (LCCAirportServiceDTO airportTrasnsfer : segAnci.getAirportTransferDTOs()) {
							total = AccelAeroCalculator.add(total, airportTrasnsfer.getServiceCharge());
						}
					}
					break;
				case AUTOMATIC_CHECKIN:
					if (segAnci.getAutomaticCheckinDTOs() != null && segAnci.getAutomaticCheckinDTOs().size() > 0) {
						for (LCCAutomaticCheckinDTO autoCheckinDTO : segAnci.getAutomaticCheckinDTOs()) {
							total = AccelAeroCalculator.add(total, autoCheckinDTO.getAutomaticCheckinCharge());
						}
					}
					break;
				case INSURANCE:
					break;
				default:
					break;
				}
			}
		}

		return total;
	}

	public BigDecimal getAncillaryTotal(boolean withInsurance) {
		BigDecimal total = BigDecimal.ZERO;
		if (getExternalCharges() != null) {
			for (LCCClientExternalChgDTO charge : getExternalCharges()) {
				if (charge.getExternalCharges() == EXTERNAL_CHARGES.SERVICE_TAX) {
					continue;
				}
				if (withInsurance) {
					total = AccelAeroCalculator.add(total, charge.getAmount());
				} else if (charge.getExternalCharges() != EXTERNAL_CHARGES.INSURANCE) {
					total = AccelAeroCalculator.add(total, charge.getAmount());
				}
			}
		}
		return total;
	}

	public BigDecimal getToRemoveAncillaryTotal() {
		BigDecimal total = BigDecimal.ZERO;
		List<LCCSelectedSegmentAncillaryDTO> toRList = getAncillariesToRemove();
		if (toRList != null) {
			for (LCCSelectedSegmentAncillaryDTO segAnci : toRList) {
				if (segAnci.getAirSeatDTO() != null && segAnci.getAirSeatDTO().getSeatNumber() != null
						&& !segAnci.getAirSeatDTO().getSeatNumber().trim().equals("")) {
					total = AccelAeroCalculator.add(total, segAnci.getAirSeatDTO().getSeatCharge());
				}

				if (segAnci.getMealDTOs() != null) {
					for (LCCMealDTO meal : segAnci.getMealDTOs()) {
						total = AccelAeroCalculator.add(total,
								AccelAeroCalculator.multiply(meal.getMealCharge(), new BigDecimal(meal.getAllocatedMeals())));
					}
				}

				if (segAnci.getBaggageDTOs() != null) {
					for (LCCBaggageDTO baggage : segAnci.getBaggageDTOs()) {
						total = AccelAeroCalculator.add(total, baggage.getBaggageCharge());
					}
				}

				if (segAnci.getAutomaticCheckinDTOs() != null && segAnci.getAutomaticCheckinDTOs().size() > 0) {
					for (LCCAutomaticCheckinDTO autoCheckinDTO : segAnci.getAutomaticCheckinDTOs()) {
						total = AccelAeroCalculator.add(total, autoCheckinDTO.getAutomaticCheckinCharge());
					}
				}

				if (segAnci.getSpecialServiceRequestDTOs() != null) {
					for (LCCSpecialServiceRequestDTO ssr : segAnci.getSpecialServiceRequestDTOs()) {
						total = AccelAeroCalculator.add(total, ssr.getCharge());
					}
				}

				if (segAnci.getAirportServiceDTOs() != null) {
					for (LCCAirportServiceDTO airportService : segAnci.getAirportServiceDTOs()) {
						total = AccelAeroCalculator.add(total, airportService.getServiceCharge());
					}
				}

				if (segAnci.getAirportTransferDTOs() != null) {
					for (LCCAirportServiceDTO airportTransfer : segAnci.getAirportTransferDTOs()) {
						total = AccelAeroCalculator.add(total, airportTransfer.getServiceCharge());
					}
				}
			}

		}
		return total;
	}

	public BigDecimal getToUpdateAncillaryTotal() {
		BigDecimal total = BigDecimal.ZERO;
		List<LCCSelectedSegmentAncillaryDTO> toUList = getAncillariesToUpdate();
		if (toUList != null) {
			for (LCCSelectedSegmentAncillaryDTO segAnci : toUList) {
				if (segAnci.getAirSeatDTO() != null && segAnci.getAirSeatDTO().getSeatNumber() != null
						&& !segAnci.getAirSeatDTO().getSeatNumber().trim().equals("")) {
					total = AccelAeroCalculator.add(total, segAnci.getAirSeatDTO().getSeatCharge());
				}

				if (segAnci.getMealDTOs() != null) {
					for (LCCMealDTO meal : segAnci.getMealDTOs()) {
						total = AccelAeroCalculator.add(total, meal.getMealCharge());
					}
				}

				if (segAnci.getBaggageDTOs() != null) {
					for (LCCBaggageDTO baggage : segAnci.getBaggageDTOs()) {
						total = AccelAeroCalculator.add(total, baggage.getBaggageCharge());
					}
				}

				if (segAnci.getAutomaticCheckinDTOs() != null && segAnci.getAutomaticCheckinDTOs().size() > 0) {
					for (LCCAutomaticCheckinDTO autoCheckinDTO : segAnci.getAutomaticCheckinDTOs()) {
						total = AccelAeroCalculator.add(total, autoCheckinDTO.getAutomaticCheckinCharge());
					}
				}

				if (segAnci.getSpecialServiceRequestDTOs() != null) {
					for (LCCSpecialServiceRequestDTO ssr : segAnci.getSpecialServiceRequestDTOs()) {
						total = AccelAeroCalculator.add(total, ssr.getCharge());
					}
				}

				if (segAnci.getAirportServiceDTOs() != null) {
					for (LCCAirportServiceDTO airportService : segAnci.getAirportServiceDTOs()) {
						total = AccelAeroCalculator.add(total, airportService.getServiceCharge());
					}
				}

				if (segAnci.getAirportTransferDTOs() != null) {
					for (LCCAirportServiceDTO airportTransfer : segAnci.getAirportTransferDTOs()) {
						total = AccelAeroCalculator.add(total, airportTransfer.getServiceCharge());
					}
				}
			}

		}
		return total;
	}

	public void updateAnciModifiedSegments(List<String> modifiedSegment) {
		if (this.externalCharges != null) {
			for (LCCClientExternalChgDTO charge : this.externalCharges) {
				String filteredFlightRPH = FlightRefNumberUtil.filterFlightRPH(charge.getFlightRefNumber(), "-");
				if (!modifiedSegment.contains(filteredFlightRPH)) {
					modifiedSegment.add(filteredFlightRPH);
				}
			}
		}
	}

	/**
	 * @return the paxType
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		if (firstName != null) {
			this.firstName = firstName.trim();
		}
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		if (lastName != null) {
			this.lastName = lastName.trim();
		}
	}

	/**
	 * @return the isParent
	 */
	public Boolean getIsParent() {
		return isParent;
	}

	/**
	 * @param isParent
	 *            the isParent to set
	 */
	public void setIsParent(Boolean isParent) {
		this.isParent = isParent;
	}

	/**
	 * @return the seqNumber
	 */
	public Integer getSeqNumber() {
		return seqNumber;
	}

	/**
	 * @param seqNumber
	 *            the seqNumber to set
	 */
	public void setSeqNumber(Integer seqNumber) {
		this.seqNumber = seqNumber;
	}

	/**
	 * @return the nationality
	 */
	public Integer getNationality() {
		return nationality;
	}

	/**
	 * @param nationality
	 *            the nationality to set
	 */
	public void setNationality(Integer nationality) {
		this.nationality = nationality;
	}

	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth
	 *            the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the infantWith
	 */
	public Integer getInfantWith() {
		return infantWith;
	}

	/**
	 * @param infantWith
	 *            the infantWith to set
	 */
	public void setInfantWith(Integer infantWith) {
		this.infantWith = infantWith;
	}

	/**
	 * @return the externalCharges
	 */
	public List<LCCClientExternalChgDTO> getExternalCharges() {
		if (externalCharges != null) {
			return externalCharges;
		} else {
			return new ArrayList<LCCClientExternalChgDTO>();
		}
	}

	public PaxAdditionalInfoDTO getPaxAdditionalInfo() {
		return paxAdditionalInfo;
	}

	public void setPaxAdditionalInfo(PaxAdditionalInfoDTO paxAdditionalInfo) {
		this.paxAdditionalInfo = paxAdditionalInfo;
	}

	public ExternalChgDTO getExternalCharge(EXTERNAL_CHARGES externalCharge) {
		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (getExternalCharges() != null) {
			for (LCCClientExternalChgDTO charge : getExternalCharges()) {
				if (charge.getExternalCharges() == externalCharge) {
					total = AccelAeroCalculator.add(total, charge.getAmount());
				}
			}
		}
		ExternalChgDTO chgDTO = new ExternalChgDTO();
		chgDTO.setExternalChargesEnum(externalCharge);
		chgDTO.setAmount(total);
		return chgDTO;
	}

	public ExternalChgDTO getInfantExternalCharge(EXTERNAL_CHARGES externalCharge) {
		BigDecimal total = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (getInfantExternalCharges() != null && getIsParent()) {
			for (LCCClientExternalChgDTO charge : getInfantExternalCharges()) {
				if (charge.getExternalCharges() == externalCharge) {
					total = AccelAeroCalculator.add(total, charge.getAmount());
				}
			}
		}
		ExternalChgDTO chgDTO = new ExternalChgDTO();
		chgDTO.setExternalChargesEnum(externalCharge);
		chgDTO.setAmount(total);
		return chgDTO;
	}

	public Collection<ExternalChgDTO> getExternalCharges(EXTERNAL_CHARGES externalCharge) {
		Collection<ExternalChgDTO> externalCharges = new ArrayList<ExternalChgDTO>();
		if (getExternalCharges() != null) {
			for (LCCClientExternalChgDTO charge : getExternalCharges()) {
				if (charge.getExternalCharges() == externalCharge) {
					ExternalChgDTO externalChgDTO = new ExternalChgDTO();
					externalChgDTO.setAmount(charge.getAmount());
					externalChgDTO.setAmountConsumedForPayment(true);
					externalChgDTO.setChargeCode(charge.getCode());
					externalChgDTO.setExternalChargesEnum(charge.getExternalCharges());
					externalCharges.add(externalChgDTO);
				}
			}
		}
		return externalCharges;
	}

	public List<LCCClientExternalChgDTO> getExternalChargesAnci(EXTERNAL_CHARGES externalCharge) {
		List<LCCClientExternalChgDTO> lccClientExternalChgDTOs = new ArrayList<LCCClientExternalChgDTO>();

		if (getExternalCharges() != null) {
			for (LCCClientExternalChgDTO charge : getExternalCharges()) {
				if (charge.getExternalCharges() == externalCharge) {
					lccClientExternalChgDTOs.add(charge);
				}
			}
		}

		return lccClientExternalChgDTOs;
	}

	/**
	 * @return the selectedAncillaries
	 */
	public List<LCCSelectedSegmentAncillaryDTO> getSelectedAncillaries() {
		if (selectedAncillaries == null) {
			return new ArrayList<LCCSelectedSegmentAncillaryDTO>();
		} else {
			return selectedAncillaries;
		}
	}

	/**
	 * 
	 * @param selectedAncillaries
	 */
	public void addSelectedAncillaries(LCCSelectedSegmentAncillaryDTO selectedAncillaries) {
		if (selectedAncillaries == null)
			return;
		if (this.selectedAncillaries == null) {
			this.selectedAncillaries = new ArrayList<LCCSelectedSegmentAncillaryDTO>();
		}

		this.selectedAncillaries.add(selectedAncillaries);
	}

	/**
	 * @return the selectedAncillaries
	 */
	public List<LCCSelectedSegmentAncillaryDTO> getAncillariesToRemove() {
		if (ancillariesToRemove == null) {
			return new ArrayList<LCCSelectedSegmentAncillaryDTO>();
		} else {
			return ancillariesToRemove;
		}
	}

	/**
	 * 
	 * @param ancillariesToRemove
	 */
	public void addAncillariesToRemove(LCCSelectedSegmentAncillaryDTO ancillariesToRemove) {
		if (ancillariesToRemove == null)
			return;
		if (this.ancillariesToRemove == null) {
			this.ancillariesToRemove = new ArrayList<LCCSelectedSegmentAncillaryDTO>();
		}

		this.ancillariesToRemove.add(ancillariesToRemove);
	}

	/**
	 * @return the selectedAncillaries
	 */
	public List<LCCSelectedSegmentAncillaryDTO> getAncillariesToUpdate() {
		if (ancillariesToUpdate == null) {
			return new ArrayList<LCCSelectedSegmentAncillaryDTO>();
		} else {
			return ancillariesToUpdate;
		}
	}

	/**
	 * 
	 * @param ancillariesToRemove
	 */
	public void addAncillariesToUpdate(LCCSelectedSegmentAncillaryDTO ancillariesToUpdate) {
		if (ancillariesToUpdate == null)
			return;
		if (this.ancillariesToUpdate == null) {
			this.ancillariesToUpdate = new ArrayList<LCCSelectedSegmentAncillaryDTO>();
		}

		this.ancillariesToUpdate.add(ancillariesToUpdate);
	}

	/**
	 * 
	 * @param externalCharges
	 */
	public void addExternalCharges(List<LCCClientExternalChgDTO> externalCharges) {
		if (externalCharges == null)
			return;

		if (this.externalCharges == null) {
			this.externalCharges = new ArrayList<LCCClientExternalChgDTO>();
		}
		this.externalCharges.addAll(externalCharges);
	}

	/**
	 * 
	 * @param externalCharge
	 */
	public void addExternalCharges(LCCClientExternalChgDTO externalCharge) {
		if (externalCharge == null)
			return;

		if (this.externalCharges == null) {
			this.externalCharges = new ArrayList<LCCClientExternalChgDTO>();
		}
		this.externalCharges.add(externalCharge);
	}

	public String getTravelerRefNumber() {
		return travelerRefNumber;
	}

	public void setTravelerRefNumber(String travelerRefNumber) {
		this.travelerRefNumber = travelerRefNumber;
	}

	public String geteTicket() {
		return eTicket;
	}

	public void seteTicket(String eTicket) {
		this.eTicket = eTicket;
	}

	public String getPaxCategory() {
		return paxCategory;
	}

	public void setPaxCategory(String paxCategory) {
		this.paxCategory = paxCategory;
	}

	/**
	 * @return the mapFlightRefAndOndBaggageId
	 */
	public Map<String, String> getMapFlightRefAndOndBaggageId() {
		return mapFlightRefAndOndBaggageId;
	}

	/**
	 * @param mapFlightRefAndOndBaggageId
	 *            the mapFlightRefAndOndBaggageId to set
	 */
	public void setMapFlightRefAndOndBaggageId(Map<String, String> mapFlightRefAndOndBaggageId) {
		this.mapFlightRefAndOndBaggageId = mapFlightRefAndOndBaggageId;
	}

	public String getFirstNameInOtherLanguage() {
		return firstNameInOtherLanguage;
	}

	public void setFirstNameInOtherLanguage(String firstNameInOtherLanguage) {
		this.firstNameInOtherLanguage = firstNameInOtherLanguage;
	}

	public String getLastNameInOtherLanguage() {
		return lastNameInOtherLanguage;
	}

	public void setLastNameInOtherLanguage(String lastNameInOtherLanguage) {
		this.lastNameInOtherLanguage = lastNameInOtherLanguage;
	}

	public String getNameTranslationLanguage() {
		return nameTranslationLanguage;
	}

	public void setNameTranslationLanguage(String nameTranslationLanguage) {
		this.nameTranslationLanguage = nameTranslationLanguage;
	}

	public String getTitleInOtherLanguage() {
		return titleInOtherLanguage;
	}

	public void setTitleInOtherLanguage(String titleInOtherLanguage) {
		this.titleInOtherLanguage = titleInOtherLanguage;
	}

	@Override
	public int compareTo(ReservationPaxTO obj) {
		if (this.getSeqNumber() != null && obj.getSeqNumber() != null) {
			return this.getSeqNumber().compareTo(obj.getSeqNumber());
		}

		return 0;
	}

	public Date getPnrPaxIntlFltArrivalDate() {
		return pnrPaxIntlFltArrivalDate;
	}

	public void setPnrPaxIntlFltArrivalDate(Date pnrPaxIntlFltArrivalDate) {
		this.pnrPaxIntlFltArrivalDate = pnrPaxIntlFltArrivalDate;
	}

	public String getPnrPaxArrivalIntlFltNumber() {
		return pnrPaxArrivalIntlFltNumber;
	}

	public void setPnrPaxArrivalIntlFltNumber(String pnrPaxArrivalIntlFltNumber) {
		this.pnrPaxArrivalIntlFltNumber = pnrPaxArrivalIntlFltNumber;
	}

	public Date getPnrPaxIntlFltDepartureDate() {
		return pnrPaxIntlFltDepartureDate;
	}

	public void setPnrPaxIntlFltDepartureDate(Date pnrPaxIntlFltDepartureDate) {
		this.pnrPaxIntlFltDepartureDate = pnrPaxIntlFltDepartureDate;
	}

	public String getPnrPaxDepartureIntlFltNumber() {
		return pnrPaxDepartureIntlFltNumber;
	}

	public void setPnrPaxDepartureIntlFltNumber(String pnrPaxDepartureIntlFltNumber) {
		this.pnrPaxDepartureIntlFltNumber = pnrPaxDepartureIntlFltNumber;
	}

	public String getPnrPaxGroupId() {
		return pnrPaxGroupId;
	}

	public void setPnrPaxGroupId(String pnrPaxGroupId) {
		this.pnrPaxGroupId = pnrPaxGroupId;
	}

	public void addInfantExternalCharges(List<LCCClientExternalChgDTO> externalCharges) {
		if (externalCharges == null)
			return;

		if (this.infantExternalCharges == null) {
			this.infantExternalCharges = new ArrayList<LCCClientExternalChgDTO>();
		}
		this.infantExternalCharges.addAll(externalCharges);
	}

	public void removeInfantExternalCharges() {
		if (getInfantExternalCharges() != null && !getInfantExternalCharges().isEmpty()) {
			getInfantExternalCharges().clear();
		}
	}

	public List<LCCClientExternalChgDTO> getInfantExternalCharges() {
		return infantExternalCharges;
	}

	public BigDecimal getInfantExternalChargeTotal() {
		BigDecimal total = BigDecimal.ZERO;
		if (getInfantExternalCharges() != null) {
			for (LCCClientExternalChgDTO charge : getInfantExternalCharges()) {
				total = AccelAeroCalculator.add(total, charge.getAmount());
			}
		}
		return total;
	}
	
	public BigDecimal getInfantExternalChargeTotalExcludingServiceTaxForCC() {
		BigDecimal total = BigDecimal.ZERO;
		if (getInfantExternalCharges() != null) {
			for (LCCClientExternalChgDTO charge : getInfantExternalCharges()) {
				if (!charge.isServiceTaxAppliedToCCFee()) {
					total = AccelAeroCalculator.add(total, charge.getAmount());
				}
			}
		}
		return total;
	}

	public void removeSelectedExternalCharge(EXTERNAL_CHARGES externalCharge) {

		if (this.getExternalChargesAnci(externalCharge) != null && !this.getExternalChargesAnci(externalCharge).isEmpty()) {

			this.getExternalCharges().removeAll(this.getExternalChargesAnci(externalCharge));
		}

		if (EXTERNAL_CHARGES.SERVICE_TAX == externalCharge) {
			removeInfantExternalCharges();
		}
	}

	public void removeServiceTaxAppliedToCCFee() {
		if (this.getExternalCharges() != null && !this.getExternalCharges().isEmpty()) {
			ListIterator<LCCClientExternalChgDTO> iter = this.getExternalCharges().listIterator();
			while (iter.hasNext()) {
				LCCClientExternalChgDTO extChgDto = iter.next();
				if (EXTERNAL_CHARGES.SERVICE_TAX.equals(extChgDto.getExternalCharges())
						&& extChgDto.isServiceTaxAppliedToCCFee()) {
					iter.remove();
				}
			}
		}

		if (getInfantExternalCharges() != null && !getInfantExternalCharges().isEmpty()) {
			ListIterator<LCCClientExternalChgDTO> infantIter = getInfantExternalCharges().listIterator();
			while (infantIter.hasNext()) {
				LCCClientExternalChgDTO extChgDto = infantIter.next();
				if (EXTERNAL_CHARGES.SERVICE_TAX.equals(extChgDto.getExternalCharges())
						&& extChgDto.isServiceTaxAppliedToCCFee()) {
					infantIter.remove();
				}
			}
		}
	}

	public BigDecimal getTotalServiceTaxCharge(boolean isWithInfant, boolean excludeServiceTaxForCC) {
		BigDecimal total = BigDecimal.ZERO;
		if (excludeServiceTaxForCC) {
			total = getServiceTaxExcludingServiceTaxForCC();
		} else {
			total = getAncillaryCharge(EXTERNAL_CHARGES.SERVICE_TAX);
		}
		if (isWithInfant) {
			if (excludeServiceTaxForCC) {
				total = AccelAeroCalculator.add(total, getInfantExternalChargeTotalExcludingServiceTaxForCC());
			} else {
				total = AccelAeroCalculator.add(total, getInfantExternalChargeTotal());
			}
		}
		return total;
	}

	public boolean isTBA() {
		return ReservationUtil.TBA_USER.equals(getFirstName()) && ReservationUtil.TBA_USER.equals(getLastName());
	}
}
