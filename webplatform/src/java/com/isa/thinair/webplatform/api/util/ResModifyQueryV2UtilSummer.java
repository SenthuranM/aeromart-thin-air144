package com.isa.thinair.webplatform.api.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airproxy.api.dto.FareInfoTOV2;
import com.isa.thinair.airproxy.api.dto.PAXSummaryTOV2;
import com.isa.thinair.airproxy.api.dto.ResBalancesSummaryTOV2;
import com.isa.thinair.airproxy.api.dto.SegmentSummaryTOV2;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * @author Nilindra Fernando
 */
public class ResModifyQueryV2UtilSummer {

	public static ResBalancesSummaryTOV2 sum(ResBalancesSummaryTOV2 targetResBalancesSummaryTOV2,
			ResBalancesSummaryTOV2 newResBalancesSummaryTOV2) {

		targetResBalancesSummaryTOV2.getColCredits().addAll(newResBalancesSummaryTOV2.getColCredits());
		targetResBalancesSummaryTOV2.getColPayments().addAll(newResBalancesSummaryTOV2.getColPayments());
		targetResBalancesSummaryTOV2.getColLccPaxPayments().addAll(newResBalancesSummaryTOV2.getColLccPaxPayments());

		targetResBalancesSummaryTOV2.setTotalCnxChargeForCurrentAlt(AccelAeroCalculator.add(
				targetResBalancesSummaryTOV2.getTotalCnxChargeForCurrentAlt(),
				newResBalancesSummaryTOV2.getTotalCnxChargeForCurrentAlt()));
		targetResBalancesSummaryTOV2.setTotalModChargeForCurrentAlt(AccelAeroCalculator.add(
				targetResBalancesSummaryTOV2.getTotalModChargeForCurrentAlt(),
				newResBalancesSummaryTOV2.getTotalModChargeForCurrentAlt()));
		targetResBalancesSummaryTOV2.setTotalPrice(AccelAeroCalculator.add(targetResBalancesSummaryTOV2.getTotalPrice(),
				newResBalancesSummaryTOV2.getTotalPrice()));

		targetResBalancesSummaryTOV2.setTotalAmountDue(AccelAeroCalculator.add(targetResBalancesSummaryTOV2.getTotalAmountDue(),
				newResBalancesSummaryTOV2.getTotalAmountDue()));
		targetResBalancesSummaryTOV2.setTotalCreditAmount(AccelAeroCalculator.add(
				targetResBalancesSummaryTOV2.getTotalCreditAmount(), newResBalancesSummaryTOV2.getTotalCreditAmount()));

		targetResBalancesSummaryTOV2.setSegmentSummaryTOV2(sum(targetResBalancesSummaryTOV2.getSegmentSummaryTOV2(),
				newResBalancesSummaryTOV2.getSegmentSummaryTOV2()));

		targetResBalancesSummaryTOV2.setPaxSummaryTOV2(sum(targetResBalancesSummaryTOV2.getPaxSummaryTOV2(),
				newResBalancesSummaryTOV2.getPaxSummaryTOV2()));

		return targetResBalancesSummaryTOV2;
	}

	private static SegmentSummaryTOV2 sum(SegmentSummaryTOV2 targetSegmentSummaryTOV2, SegmentSummaryTOV2 newSegmentSummaryTOV2) {

		targetSegmentSummaryTOV2.setIdentifiedTotalCnxCharge(AccelAeroCalculator.add(
				targetSegmentSummaryTOV2.getIdentifiedTotalCnxCharge(), newSegmentSummaryTOV2.getIdentifiedTotalCnxCharge()));
		targetSegmentSummaryTOV2.setIdentifiedTotalModCharge(AccelAeroCalculator.add(
				targetSegmentSummaryTOV2.getIdentifiedTotalModCharge(), newSegmentSummaryTOV2.getIdentifiedTotalModCharge()));

		targetSegmentSummaryTOV2.setCurrentAmounts(sum(targetSegmentSummaryTOV2.getCurrentAmounts(),
				newSegmentSummaryTOV2.getCurrentAmounts()));
		targetSegmentSummaryTOV2.setCurrentNonRefundableAmounts(sum(targetSegmentSummaryTOV2.getCurrentNonRefundableAmounts(),
				newSegmentSummaryTOV2.getCurrentNonRefundableAmounts()));
		targetSegmentSummaryTOV2.setCurrentRefundableAmounts(sum(targetSegmentSummaryTOV2.getCurrentRefundableAmounts(),
				newSegmentSummaryTOV2.getCurrentRefundableAmounts()));

		targetSegmentSummaryTOV2.setNewAmounts(sum(targetSegmentSummaryTOV2.getNewAmounts(),
				newSegmentSummaryTOV2.getNewAmounts()));
		targetSegmentSummaryTOV2.setNewNonRefundableAmounts(sum(targetSegmentSummaryTOV2.getNewNonRefundableAmounts(),
				newSegmentSummaryTOV2.getNewNonRefundableAmounts()));
		targetSegmentSummaryTOV2.setNewRefundableAmounts(sum(targetSegmentSummaryTOV2.getNewRefundableAmounts(),
				newSegmentSummaryTOV2.getNewRefundableAmounts()));

		return targetSegmentSummaryTOV2;
	}

	private static FareInfoTOV2 sum(FareInfoTOV2 targetFareInfoTOV2, FareInfoTOV2 newFareInfoTOV2) {

		if (targetFareInfoTOV2 != null && newFareInfoTOV2 != null) {
			targetFareInfoTOV2.setTotalPrice(AccelAeroCalculator.add(targetFareInfoTOV2.getTotalPrice(),
					newFareInfoTOV2.getTotalPrice()));
			targetFareInfoTOV2.setTotalFare(AccelAeroCalculator.add(targetFareInfoTOV2.getTotalFare(),
					newFareInfoTOV2.getTotalFare()));
			targetFareInfoTOV2.setTotalTax(AccelAeroCalculator.add(targetFareInfoTOV2.getTotalTax(),
					newFareInfoTOV2.getTotalTax()));
			targetFareInfoTOV2.setTotalSurcharge(AccelAeroCalculator.add(targetFareInfoTOV2.getTotalSurcharge(),
					newFareInfoTOV2.getTotalSurcharge()));
			targetFareInfoTOV2.setTotalModCharge(AccelAeroCalculator.add(targetFareInfoTOV2.getTotalModCharge(),
					newFareInfoTOV2.getTotalModCharge()));
			targetFareInfoTOV2.setTotalCanCharge(AccelAeroCalculator.add(targetFareInfoTOV2.getTotalCanCharge(),
					newFareInfoTOV2.getTotalCanCharge()));
			targetFareInfoTOV2.setTotalAdjCharge(AccelAeroCalculator.add(targetFareInfoTOV2.getTotalAdjCharge(),
					newFareInfoTOV2.getTotalAdjCharge()));

			targetFareInfoTOV2.getFare().addAll(newFareInfoTOV2.getFare());
			targetFareInfoTOV2.getTaxes().addAll(newFareInfoTOV2.getTaxes());
			targetFareInfoTOV2.getSurcharges().addAll(newFareInfoTOV2.getSurcharges());
			targetFareInfoTOV2.getModifications().addAll(newFareInfoTOV2.getModifications());
			targetFareInfoTOV2.getCancellations().addAll(newFareInfoTOV2.getCancellations());
			targetFareInfoTOV2.getAdjustments().addAll(newFareInfoTOV2.getAdjustments());

			return targetFareInfoTOV2;
		} else {
			return null;
		}
	}

	private static Collection<PAXSummaryTOV2> sum(Collection<PAXSummaryTOV2> targetColPaxSummaryTOV2,
			Collection<PAXSummaryTOV2> newColPaxSummaryTOV2) {
		Map<Integer, PAXSummaryTOV2> paxWisePaxSummaryDTOMap = getPaxWisePAXSummaryTOV2(newColPaxSummaryTOV2);
		Collection<PAXSummaryTOV2> summedPaxSummaryTOV2 = new ArrayList<PAXSummaryTOV2>();

		for (PAXSummaryTOV2 targetPaxSummaryTOV2 : targetColPaxSummaryTOV2) {
			PAXSummaryTOV2 newPaxSummaryTOV2 = paxWisePaxSummaryDTOMap.get(targetPaxSummaryTOV2.getPnrPaxId());
			summedPaxSummaryTOV2.add(sum(targetPaxSummaryTOV2, newPaxSummaryTOV2));
		}

		return summedPaxSummaryTOV2;
	}

	private static PAXSummaryTOV2 sum(PAXSummaryTOV2 targetPaxSummaryTOV2, PAXSummaryTOV2 newPaxSummaryTOV2) {

		targetPaxSummaryTOV2.setTotalPrice(AccelAeroCalculator.add(targetPaxSummaryTOV2.getTotalPrice(),
				newPaxSummaryTOV2.getTotalPrice()));
		targetPaxSummaryTOV2.setTotalAmountDue(AccelAeroCalculator.add(targetPaxSummaryTOV2.getTotalAmountDue(),
				newPaxSummaryTOV2.getTotalAmountDue()));
		targetPaxSummaryTOV2.setTotalCreditAmount(AccelAeroCalculator.add(targetPaxSummaryTOV2.getTotalCreditAmount(),
				newPaxSummaryTOV2.getTotalCreditAmount()));

		targetPaxSummaryTOV2.setTotalCnxChargeForCurrentAlt(AccelAeroCalculator.add(
				targetPaxSummaryTOV2.getTotalCnxChargeForCurrentAlt(), newPaxSummaryTOV2.getTotalCnxChargeForCurrentAlt()));
		targetPaxSummaryTOV2.setTotalModChargeForCurrentAlt(AccelAeroCalculator.add(
				targetPaxSummaryTOV2.getTotalModChargeForCurrentAlt(), newPaxSummaryTOV2.getTotalModChargeForCurrentAlt()));

		targetPaxSummaryTOV2.getColCredits().addAll(targetPaxSummaryTOV2.getColCredits());
		targetPaxSummaryTOV2.getColCredits().addAll(newPaxSummaryTOV2.getColCredits());

		targetPaxSummaryTOV2.getColPayments().addAll(targetPaxSummaryTOV2.getColPayments());
		targetPaxSummaryTOV2.getColPayments().addAll(newPaxSummaryTOV2.getColPayments());

		targetPaxSummaryTOV2.setSegmentSummaryTOV2(sum(targetPaxSummaryTOV2.getSegmentSummaryTOV2(),
				newPaxSummaryTOV2.getSegmentSummaryTOV2()));

		return targetPaxSummaryTOV2;
	}

	private static Map<Integer, PAXSummaryTOV2> getPaxWisePAXSummaryTOV2(Collection<PAXSummaryTOV2> newPaxSummaryTOV2) {
		Map<Integer, PAXSummaryTOV2> paxWisePaxSummaryDTO = new HashMap<Integer, PAXSummaryTOV2>();

		for (PAXSummaryTOV2 paxSummaryTOV2 : newPaxSummaryTOV2) {
			paxWisePaxSummaryDTO.put(paxSummaryTOV2.getPnrPaxId(), paxSummaryTOV2);
		}

		return paxWisePaxSummaryDTO;
	}
}
