package com.isa.thinair.webplatform.api.v2.reservation;

public class ModifySegmentTO {
	private String PNR;

	private String mode;

	private String modifyingSegments;

	private String from;

	private String to;

	private String departureDate;

	private String version;

	private String bookingType;

	private String bookingclass;

	private int noOfAdults;

	private int noOfChildren;

	private int noOfInfants;

	private String selectedCurrency;

	/**
	 * @return the pNR
	 */
	public String getPNR() {
		return PNR;
	}

	/**
	 * @param pNR
	 *            the pNR to set
	 */
	public void setPNR(String pNR) {
		PNR = pNR;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getModifyingSegments() {
		return modifyingSegments;
	}

	public void setModifyingSegments(String modifyingSegments) {
		this.modifyingSegments = modifyingSegments;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getBookingType() {
		return bookingType;
	}

	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	public String getBookingclass() {
		return bookingclass;
	}

	public void setBookingclass(String bookingclass) {
		this.bookingclass = bookingclass;
	}

	public int getNoOfAdults() {
		return noOfAdults;
	}

	public void setNoOfAdults(int noOfAdults) {
		this.noOfAdults = noOfAdults;
	}

	public int getNoOfChildren() {
		return noOfChildren;
	}

	public void setNoOfChildren(int noOfChildren) {
		this.noOfChildren = noOfChildren;
	}

	public int getNoOfInfants() {
		return noOfInfants;
	}

	public void setNoOfInfants(int noOfInfants) {
		this.noOfInfants = noOfInfants;
	}

	public String getSelectedCurrency() {
		return selectedCurrency;
	}

	public void setSelectedCurrency(String selectedCurrency) {
		this.selectedCurrency = selectedCurrency;
	}

}