package com.isa.thinair.webplatform.api.util;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class ReservationPaxUtil {

	public static Collection<LCCClientReservationPax>
			toLccClientReservationPax(Collection<ReservationPaxTO> reservationPaxTOsList) {
		Collection<LCCClientReservationPax> lccClientReservationPaxList = new ArrayList<>();

		for (Iterator<ReservationPaxTO> reservationPaxTOsListIterator = reservationPaxTOsList.iterator(); reservationPaxTOsListIterator
				.hasNext();) {
			lccClientReservationPaxList.add(convertResPaxToLccResPax(reservationPaxTOsListIterator.next()));
		}

		return lccClientReservationPaxList;
	}

	private static LCCClientReservationPax convertResPaxToLccResPax(ReservationPaxTO resPaxTo) {
		LCCClientReservationPax lccClientReservationPax = new LCCClientReservationPax();
		lccClientReservationPax.setTitle(resPaxTo.getTitle());
		lccClientReservationPax.setFirstName(resPaxTo.getFirstName());
		lccClientReservationPax.setLastName(resPaxTo.getLastName());
		lccClientReservationPax.setPaxSequence(resPaxTo.getSeqNumber());

		return lccClientReservationPax;
	}

	public static int getPayablePaxCount(Collection<LCCClientPassengerSummaryTO> passengers) {
		int payableAdultChildCount = 0;
		for (LCCClientPassengerSummaryTO pax : passengers) {
			if (!pax.getPaxType().equals("IN") && pax.getTotalAmountDue().compareTo(BigDecimal.ZERO) > 0) {
				payableAdultChildCount++;
			}
		}
		return payableAdultChildCount;
	}	
	
	public static int getPayableInfantCount(Collection<LCCClientPassengerSummaryTO> passengers, boolean infantPaymentSeparated) {
		
		int payableInfantCount = 0;
		
		if (infantPaymentSeparated) {			
			for (LCCClientPassengerSummaryTO pax : passengers) {				
				if (pax.getPaxType().equals("IN") && pax.getTotalAmountDue().compareTo(BigDecimal.ZERO) > 0) {
					payableInfantCount++;
				}
			}
		}
			
		return payableInfantCount;
	}
	
	
}
