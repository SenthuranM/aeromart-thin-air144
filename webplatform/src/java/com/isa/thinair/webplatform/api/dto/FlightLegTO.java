package com.isa.thinair.webplatform.api.dto;

public class FlightLegTO {

	private int legNumber;

	private String legOrigin;

	private String legDestination;

	private String legDepTimeLocal;

	private String legDepTimeZulu;

	private String legArrivalTimeLocal;

	private String legArrivalTimeZulu;

	private int legDepDayOffsetLocal;

	private int legDepDayOffsetZulu;

	private int legArrivalOffsetLocal;

	private int legArrivalOffsetZulu;

	private int legDuration;

	private int legId;

	private boolean isOverlappingLeg;

	/**
	 * @return the legNumber
	 */
	public int getLegNumber() {
		return legNumber;
	}

	/**
	 * @param legNumber
	 *            the legNumber to set
	 */
	public void setLegNumber(int legNumber) {
		this.legNumber = legNumber;
	}

	/**
	 * @return the legOrigin
	 */
	public String getLegOrigin() {
		return legOrigin;
	}

	/**
	 * @param legOrigin
	 *            the legOrigin to set
	 */
	public void setLegOrigin(String legOrigin) {
		this.legOrigin = legOrigin;
	}

	/**
	 * @return the legDestination
	 */
	public String getLegDestination() {
		return legDestination;
	}

	/**
	 * @param legDestination
	 *            the legDestination to set
	 */
	public void setLegDestination(String legDestination) {
		this.legDestination = legDestination;
	}

	/**
	 * @return the legDepTimeLocal
	 */
	public String getLegDepTimeLocal() {
		return legDepTimeLocal;
	}

	/**
	 * @param legDepTimeLocal
	 *            the legDepTimeLocal to set
	 */
	public void setLegDepTimeLocal(String legDepTimeLocal) {
		this.legDepTimeLocal = legDepTimeLocal;
	}

	/**
	 * @return the legDepTimeZulu
	 */
	public String getLegDepTimeZulu() {
		return legDepTimeZulu;
	}

	/**
	 * @param legDepTimeZulu
	 *            the legDepTimeZulu to set
	 */
	public void setLegDepTimeZulu(String legDepTimeZulu) {
		this.legDepTimeZulu = legDepTimeZulu;
	}

	/**
	 * @return the legArrivalTimeLocal
	 */
	public String getLegArrivalTimeLocal() {
		return legArrivalTimeLocal;
	}

	/**
	 * @param legArrivalTimeLocal
	 *            the legArrivalTimeLocal to set
	 */
	public void setLegArrivalTimeLocal(String legArrivalTimeLocal) {
		this.legArrivalTimeLocal = legArrivalTimeLocal;
	}

	/**
	 * @return the legArrivalTimeZulu
	 */
	public String getLegArrivalTimeZulu() {
		return legArrivalTimeZulu;
	}

	/**
	 * @param legArrivalTimeZulu
	 *            the legArrivalTimeZulu to set
	 */
	public void setLegArrivalTimeZulu(String legArrivalTimeZulu) {
		this.legArrivalTimeZulu = legArrivalTimeZulu;
	}

	/**
	 * @return the legDepDayOffsetLocal
	 */
	public int getLegDepDayOffsetLocal() {
		return legDepDayOffsetLocal;
	}

	/**
	 * @param legDepDayOffsetLocal
	 *            the legDepDayOffsetLocal to set
	 */
	public void setLegDepDayOffsetLocal(int legDepDayOffsetLocal) {
		this.legDepDayOffsetLocal = legDepDayOffsetLocal;
	}

	/**
	 * @return the legDepDayOffsetZulu
	 */
	public int getLegDepDayOffsetZulu() {
		return legDepDayOffsetZulu;
	}

	/**
	 * @param legDepDayOffsetZulu
	 *            the legDepDayOffsetZulu to set
	 */
	public void setLegDepDayOffsetZulu(int legDepDayOffsetZulu) {
		this.legDepDayOffsetZulu = legDepDayOffsetZulu;
	}

	/**
	 * @return the legArrivalOffsetLocal
	 */
	public int getLegArrivalOffsetLocal() {
		return legArrivalOffsetLocal;
	}

	/**
	 * @param legArrivalOffsetLocal
	 *            the legArrivalOffsetLocal to set
	 */
	public void setLegArrivalOffsetLocal(int legArrivalOffsetLocal) {
		this.legArrivalOffsetLocal = legArrivalOffsetLocal;
	}

	/**
	 * @return the legArrivalOffsetZulu
	 */
	public int getLegArrivalOffsetZulu() {
		return legArrivalOffsetZulu;
	}

	/**
	 * @param legArrivalOffsetZulu
	 *            the legArrivalOffsetZulu to set
	 */
	public void setLegArrivalOffsetZulu(int legArrivalOffsetZulu) {
		this.legArrivalOffsetZulu = legArrivalOffsetZulu;
	}

	/**
	 * @return the legDuration
	 */
	public int getLegDuration() {
		return legDuration;
	}

	/**
	 * @param legDuration
	 *            the legDuration to set
	 */
	public void setLegDuration(int legDuration) {
		this.legDuration = legDuration;
	}

	/**
	 * @return the legId
	 */
	public int getLegId() {
		return legId;
	}

	/**
	 * @param legId
	 *            the legId to set
	 */
	public void setLegId(int legId) {
		this.legId = legId;
	}

	/**
	 * @return the isOverlappingLeg
	 */
	public boolean isOverlappingLeg() {
		return isOverlappingLeg;
	}

	/**
	 * @param isOverlappingLeg
	 *            the isOverlappingLeg to set
	 */
	public void setOverlappingLeg(boolean isOverlappingLeg) {
		this.isOverlappingLeg = isOverlappingLeg;
	}

}