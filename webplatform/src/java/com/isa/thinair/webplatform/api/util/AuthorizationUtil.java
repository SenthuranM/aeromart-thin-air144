package com.isa.thinair.webplatform.api.util;

import java.util.Collection;

import com.isa.thinair.airreservation.api.utils.AuthorizationsUtil;

public class AuthorizationUtil {

	/**
	 * Returns true if the privilegeToCheck to check is included in the privilegesKeys collection.
	 */
	public static boolean hasPrivilege(Collection<String> privilegesKeys, String privilegeToCheck) {
		return AuthorizationsUtil.hasPrivilege(privilegesKeys, privilegeToCheck);
	}

	public static boolean hasPrivileges(Collection<String> privilegesKeys, String... privilegesToCheck) {
		return AuthorizationsUtil.hasPrivileges(privilegesKeys, privilegesToCheck);
	}
}
