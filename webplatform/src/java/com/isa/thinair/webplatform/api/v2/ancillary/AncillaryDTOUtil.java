package com.isa.thinair.webplatform.api.v2.ancillary;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;

public class AncillaryDTOUtil {

	public static void populateResPaxWithSelAnciDtls(Collection<ReservationPaxTO> paxList,
			CommonReservationAssembler lcclientReservationAssembler) {

		Set<LCCClientReservationPax> reservationPaxs = lcclientReservationAssembler.getLccreservation().getPassengers();
		for (LCCClientReservationPax lccClientReservationPax : reservationPaxs) {
			if (paxList != null) {
				for (ReservationPaxTO pax : paxList) {
					if (pax.getSeqNumber().equals(lccClientReservationPax.getPaxSequence())) {
						lccClientReservationPax.addSelectedAncillaries(pax.getSelectedAncillaries());
						break;
					}
				}
			}
		}
	}

	public static LCCInsuredJourneyDTO getInsuranceJourneyDetails(List<FlightSegmentTO> flightSegmentTOs, boolean roundTrip,
			int salesChannel) {

		IFlightSegment startSeg = null;
		IFlightSegment endSeg = null;

		String endAirportCode = null;
		Object[] outboundInboundFlifghts = getOutboundInboundList(flightSegmentTOs);
		List<FlightSegmentTO> outboundFlightSegmentTOs = (List<FlightSegmentTO>) outboundInboundFlifghts[0];
		List<FlightSegmentTO> inboundFlightSegmentTOs = (List<FlightSegmentTO>) outboundInboundFlifghts[1];
		if (outboundFlightSegmentTOs.size() > 0) {
			startSeg = outboundFlightSegmentTOs.get(0);
			endSeg = outboundFlightSegmentTOs.get(0);

			IFlightSegment inboundFlightSegmentTO = inboundFlightSegmentTOs.size() == 0 ? null : inboundFlightSegmentTOs
					.get(inboundFlightSegmentTOs.size() - 1);
			if (inboundFlightSegmentTO == null) {
				if (outboundFlightSegmentTOs.size() > 1) {
					endSeg = outboundFlightSegmentTOs.get(outboundFlightSegmentTOs.size() - 1);
				}
				endAirportCode = endSeg.getSegmentCode().split("/")[endSeg.getSegmentCode().split("/").length - 1];
			} else {
				endAirportCode = inboundFlightSegmentTOs.get(0).getSegmentCode().split("/")[0];
				endSeg = inboundFlightSegmentTOs.get(inboundFlightSegmentTOs.size() - 1);
				roundTrip = true;
			}

			String sOffset = getOffset(startSeg.getDepartureDateTime().getTime() - startSeg.getDepartureDateTimeZulu().getTime());
			String eOffset = getOffset(endSeg.getArrivalDateTime().getTime() - endSeg.getArrivalDateTimeZulu().getTime());

			LCCInsuredJourneyDTO jrny1 = new LCCInsuredJourneyDTO();
			jrny1.setJourneyStartAirportCode(startSeg.getSegmentCode().split("/")[0]);
			jrny1.setJourneyEndAirportCode(endAirportCode);
			jrny1.setJourneyStartDate(startSeg.getDepartureDateTime());
			jrny1.setJourneyStartDateOffset(sOffset);
			jrny1.setJourneyEndDate(endSeg.getArrivalDateTime());
			jrny1.setJourneyEndDateOffset(eOffset);
			jrny1.setRoundTrip(roundTrip);
			jrny1.setSalesChannel(salesChannel);
			return jrny1;
		}
		return null;
	}

	public static Object[] getOutboundInboundList(List<FlightSegmentTO> flightSegmentTOs) {
		Collections.sort(flightSegmentTOs);

		List<FlightSegmentTO> cloneFlightSegmentTOs = new ArrayList<FlightSegmentTO>();
		List<FlightSegmentTO> outboundFlightSegmentTOs = new ArrayList<FlightSegmentTO>();
		List<FlightSegmentTO> inboundFlightSegmentTOs = new ArrayList<FlightSegmentTO>();

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			FlightSegmentTO cloneFlightSegmentTO = flightSegmentTO.clone();
			cloneFlightSegmentTOs.add(cloneFlightSegmentTO);
		}

		Date currenctDate = CalendarUtil.getCurrentSystemTimeInZulu();

		Collections.sort(cloneFlightSegmentTOs);
		ArrayList<String> orderedSegmentList = new ArrayList<String>();

		for (IFlightSegment flightSegmentTO : cloneFlightSegmentTOs) {
			if (flightSegmentTO.getDepartureDateTimeZulu().after(currenctDate)) {
				orderedSegmentList.add(flightSegmentTO.getSegmentCode());
			}
		}

		boolean isFirst = true;
		List<String> airportCodes = new ArrayList<String>();
		List<String> outBoundSegmentCodes = new ArrayList<String>();

		OUTER: for (String segmentCode : orderedSegmentList) {
			String[] segmentSplit = segmentCode.split("/");

			if (isFirst) {
				for (String code : segmentSplit) {
					airportCodes.add(code);
				}
				outBoundSegmentCodes.add(segmentCode);
				isFirst = false;
			} else {
				INNER2: for (int i = 0; i < segmentSplit.length; ++i) {
					if (i == 0)
						continue INNER2;
					else {
						if (airportCodes.contains(segmentSplit[i])) {
							break OUTER;
						} else {
							airportCodes.add(segmentSplit[i]);
						}
					}
					outBoundSegmentCodes.add(segmentCode);
				}
			}
		}

		for (FlightSegmentTO flightSegmentTO : cloneFlightSegmentTOs) {
			if (flightSegmentTO.getDepartureDateTimeZulu().after(currenctDate)) {
				if (outBoundSegmentCodes.contains(flightSegmentTO.getSegmentCode())) {
					outboundFlightSegmentTOs.add(flightSegmentTO);
				} else {
					inboundFlightSegmentTOs.add(flightSegmentTO);
				}
			}
		}

		Collections.sort(outboundFlightSegmentTOs);
		Collections.sort(inboundFlightSegmentTOs);

		return new Object[] { outboundFlightSegmentTOs, inboundFlightSegmentTOs };
	}

	public static Object[] getOutboundInboundList(Collection<FlightInfoTO> flightDetails) {

		List<FlightInfoTO> cloneFlightSegmentTOs = new ArrayList<FlightInfoTO>();
		List<FlightInfoTO> outboundFlightSegmentTOs = new ArrayList<FlightInfoTO>();
		List<FlightInfoTO> inboundFlightSegmentTOs = new ArrayList<FlightInfoTO>();

		for (FlightInfoTO flightSegmentTO : flightDetails) {
			FlightInfoTO cloneFlightSegmentTO = (FlightInfoTO) flightSegmentTO.clone();
			cloneFlightSegmentTOs.add(cloneFlightSegmentTO);
		}

		Collections.sort(cloneFlightSegmentTOs);
		ArrayList<String> orderedSegmentList = new ArrayList<String>();

		for (FlightInfoTO flightSegmentTO : cloneFlightSegmentTOs) {
			if (flightSegmentTO.getDepartureTimeZuluLong() > Calendar.getInstance().getTimeInMillis()
					&& !flightSegmentTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
				orderedSegmentList.add(flightSegmentTO.getOrignNDest());
			}
		}

		boolean isFirst = true;
		List<String> airportCodes = new ArrayList<String>();
		List<String> outBoundSegmentCodes = new ArrayList<String>();

		OUTER: for (String segmentCode : orderedSegmentList) {
			String[] segmentSplit = segmentCode.split("/");

			if (isFirst) {
				for (String code : segmentSplit) {
					airportCodes.add(code);
				}
				outBoundSegmentCodes.add(segmentCode);
				isFirst = false;
			} else {
				INNER2: for (int i = 0; i < segmentSplit.length; ++i) {
					if (i == 0)
						continue INNER2;
					else {
						if (airportCodes.contains(segmentSplit[i])) {
							break OUTER;
						} else {
							airportCodes.add(segmentSplit[i]);
						}
					}
					outBoundSegmentCodes.add(segmentCode);
				}
			}
		}

		for (FlightInfoTO flightSegmentTO : cloneFlightSegmentTOs) {
			if (flightSegmentTO.getDepartureTimeZuluLong() > Calendar.getInstance().getTimeInMillis()
					&& !flightSegmentTO.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
				if (outBoundSegmentCodes.contains(flightSegmentTO.getOrignNDest())) {
					outboundFlightSegmentTOs.add(flightSegmentTO);
				} else {
					inboundFlightSegmentTOs.add(flightSegmentTO);
				}
			}
		}

		Collections.sort(outboundFlightSegmentTOs);
		Collections.sort(inboundFlightSegmentTOs);

		return new Object[] { outboundFlightSegmentTOs, inboundFlightSegmentTOs };
	}

	public static String getOffset(long time) {
		StringBuilder sb = new StringBuilder();
		if (time >= 0) {
			sb.append("+");
		} else {
			sb.append("-");
			time *= -1;
		}

		sb.append(BeanUtils.parseDurationFormat(time, "HH:mm"));
		return sb.toString();
	}

	public static void populateInsurance(List<LCCInsuranceQuotationDTO> insuranceQuotes,
			CommonReservationAssembler commonReservationAssembler) {
		if (insuranceQuotes == null || insuranceQuotes.isEmpty()) {
			commonReservationAssembler.getLccreservation().setReservationInsurances(null);
		} else {

			List<LCCClientReservationInsurance> reservationInsurances = new ArrayList<LCCClientReservationInsurance>();
			LCCClientReservationInsurance reservationInsurance = null;

			Iterator<LCCInsuranceQuotationDTO> quoteIterator = insuranceQuotes.iterator();

			while (quoteIterator.hasNext()) {
				LCCInsuranceQuotationDTO insuranceQuote = quoteIterator.next();

				reservationInsurance = new LCCClientReservationInsurance();
				reservationInsurance.setInsuranceQuoteRefNumber(insuranceQuote.getInsuranceRefNumber());
				reservationInsurance.setQuotedTotalPremium(insuranceQuote.getQuotedTotalPremiumAmount());
				reservationInsurance.setOrigin(insuranceQuote.getInsuredJourney().getJourneyStartAirportCode());
				reservationInsurance.setDestination(insuranceQuote.getInsuredJourney().getJourneyEndAirportCode());
				reservationInsurance.setDateOfTravel(insuranceQuote.getInsuredJourney().getJourneyStartDate());
				reservationInsurance.setDateOfReturn(insuranceQuote.getInsuredJourney().getJourneyEndDate());
				reservationInsurance.setOperatingAirline(insuranceQuote.getOperatingAirline());
				reservationInsurance.setInsuredJourneyDTO(insuranceQuote.getInsuredJourney());
				reservationInsurance.setInsuranceType(insuranceQuote.getInsuranceType());
				reservationInsurance.setAllDomastic(insuranceQuote.isAllDomastic());
				reservationInsurance.setPolicyCode(insuranceQuote.getPolicyCode());
				reservationInsurance.setSsrFeeCode(insuranceQuote.getSsrFeeCode());
				reservationInsurance.setPlanCode(insuranceQuote.getPlanCode());

				reservationInsurances.add(reservationInsurance);
			}

			commonReservationAssembler.getLccreservation().setReservationInsurances(reservationInsurances);
		}
	}

	public static Map<String, Set<String>> getFlightReferenceWiseSelectedMeals(List<ReservationPaxTO> colpaxs) {
		Map<String, Set<String>> fltRefWiseSelectedMeal = new HashMap<String, Set<String>>();

		if (colpaxs != null) {
			for (ReservationPaxTO pax : colpaxs) {
				if (pax.getSelectedAncillaries() != null) {
					for (LCCSelectedSegmentAncillaryDTO selAnci : pax.getSelectedAncillaries()) {
						if (selAnci.getMealDTOs() != null) {
							for (LCCMealDTO meal : selAnci.getMealDTOs()) {
								if (fltRefWiseSelectedMeal.get(selAnci.getFlightSegmentTO().getFlightRefNumber()) == null) {
									fltRefWiseSelectedMeal.put(selAnci.getFlightSegmentTO().getFlightRefNumber(),
											new HashSet<String>());
								}

								fltRefWiseSelectedMeal.get(selAnci.getFlightSegmentTO().getFlightRefNumber()).add(
										meal.getMealCode());
							}
						}
					}
				}

				if (pax.getAncillariesToRemove() != null) {
					for (LCCSelectedSegmentAncillaryDTO remAnci : pax.getAncillariesToRemove()) {
						if (remAnci.getMealDTOs() != null) {
							for (LCCMealDTO meal : remAnci.getMealDTOs()) {
								if (fltRefWiseSelectedMeal.get(remAnci.getFlightSegmentTO().getFlightRefNumber()) == null) {
									fltRefWiseSelectedMeal.put(remAnci.getFlightSegmentTO().getFlightRefNumber(),
											new HashSet<String>());
								}

								fltRefWiseSelectedMeal.get(remAnci.getFlightSegmentTO().getFlightRefNumber()).add(
										meal.getMealCode());
							}
						}
					}
				}

				if (pax.getAncillariesToUpdate() != null) {
					for (LCCSelectedSegmentAncillaryDTO modAnci : pax.getAncillariesToUpdate()) {
						if (modAnci.getMealDTOs() != null) {
							for (LCCMealDTO meal : modAnci.getMealDTOs()) {
								if (fltRefWiseSelectedMeal.get(modAnci.getFlightSegmentTO().getFlightRefNumber()) == null) {
									fltRefWiseSelectedMeal.put(modAnci.getFlightSegmentTO().getFlightRefNumber(),
											new HashSet<String>());
								}

								fltRefWiseSelectedMeal.get(modAnci.getFlightSegmentTO().getFlightRefNumber()).add(
										meal.getMealCode());
							}
						}
					}
				}
			}
		}

		return fltRefWiseSelectedMeal;
	}

	public static boolean isFlexiQuote(List<ReservationPaxTO> reservationPaxTOs) {
		if (reservationPaxTOs != null) {
			for (ReservationPaxTO reservationPaxTO : reservationPaxTOs) {
				if (reservationPaxTO.getExternalCharges() != null) {
					for (LCCClientExternalChgDTO extCharges : reservationPaxTO.getExternalCharges()) {
						if (extCharges.getExternalCharges() == EXTERNAL_CHARGES.FLEXI_CHARGES) {
							return true;
						}
					}
				}
			}
		}

		return false;
	}

}