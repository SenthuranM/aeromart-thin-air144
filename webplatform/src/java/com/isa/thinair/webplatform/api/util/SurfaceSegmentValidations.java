package com.isa.thinair.webplatform.api.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.webplatform.api.dto.SurfaceSegmentValidationTO;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;

/**
 * Surface Segment validation util
 * 
 * @author Duminda G
 * @since October 22, 2010
 */

public class SurfaceSegmentValidations {

	private static Log log = LogFactory.getLog(SurfaceSegmentValidations.class);

	protected static boolean hasEmptyOriginDestination(FlightSearchDTO searchDTO) {
		return !hasValue(searchDTO.getFromAirport()) || !hasValue(searchDTO.getToAirport());
	}

	protected static SurfaceSegmentValidationTO getSubStations(FlightSearchDTO searchDTO,
			SurfaceSegmentValidationTO surfaceSegmentValidationTO) throws ModuleException {
		if (surfaceSegmentValidationTO == null) {
			surfaceSegmentValidationTO = new SurfaceSegmentValidationTO();
		}
		surfaceSegmentValidationTO.setOriginSubStation(hasValue(searchDTO.getFromAirportSubStation()));
		surfaceSegmentValidationTO.setDestinationSubStation(hasValue(searchDTO.getToAirportSubStation()));

		AddModifySurfaceSegmentValidations.updateGroundSegments(searchDTO, surfaceSegmentValidationTO);
		return surfaceSegmentValidationTO;
	}

	protected static SurfaceSegmentValidationTO getConnectingStations(FlightSearchDTO searchDTO,
			SurfaceSegmentValidationTO surfaceSegmentValidationTO) {
		if (surfaceSegmentValidationTO == null) {
			surfaceSegmentValidationTO = new SurfaceSegmentValidationTO();
		}
		if (SYSTEM.getEnum(searchDTO.getSearchSystem()) == SYSTEM.INT) {
			surfaceSegmentValidationTO.setOriginConnectingStation(isConnectingStationFromLCC(searchDTO.getFromAirport(),
					searchDTO.getToAirport()));
			surfaceSegmentValidationTO.setDestinationConnectingStation(isConnectingStationFromLCC(searchDTO.getToAirport(),
					searchDTO.getFromAirport()));
		} else {
			surfaceSegmentValidationTO.setOriginConnectingStation(isConnectingStation(searchDTO.getFromAirport(),
					searchDTO.getToAirport()));
			surfaceSegmentValidationTO.setDestinationConnectingStation(isConnectingStation(searchDTO.getToAirport(),
					searchDTO.getFromAirport()));
		}
		return surfaceSegmentValidationTO;
	}

	protected static boolean hasValue(String param) {
		return param != null && !param.trim().isEmpty() ? true : false;
	}

	protected static boolean isConnectingStation(String station) {
		return station != null && !station.isEmpty()
				&& CommonsServices.getGlobalConfig().getMapParentStationByGroundStation().containsKey(station.trim())
				? true
				: false;
	}

	/**
	 * @param station
	 * @param busStation
	 * @return
	 */
	protected static boolean isConnectingStation(String station, String busStation) {

		Map<String, List<String[]>> mapParentStationByGroundStation = CommonsServices.getGlobalConfig()
				.getMapParentStationByGroundStation();

		if (station != null && !station.isEmpty()) {
			if (mapParentStationByGroundStation.containsKey(station)) {
				List<String[]> busStationList = mapParentStationByGroundStation.get(station);

				if (busStationList != null && busStationList.size() > 0) {
					Iterator<String[]> busStationItr = busStationList.iterator();
					while (busStationItr.hasNext()) {
						String[] busStationArray = busStationItr.next();

						if (busStationArray[0] != null && busStationArray[0].equalsIgnoreCase(busStation)) {
							return true;
						}
					}
				}
			}
		}

		return false;

	}

	/**
	 * @param station
	 * @param busStation
	 * @return
	 */
	protected static boolean isConnectingStationFromLCC(String station, String busStation) {
		Map[] groundSegmentConnectivityArr = LCCWebplatformUtil.getLCCParentStationDetailsMap();
		Map<String, List<String[]>> mapParentStationByGroundStation = new HashMap<String, List<String[]>>();
		mapParentStationByGroundStation.putAll(groundSegmentConnectivityArr[0]);

		if (station != null && !station.isEmpty()) {
			if (mapParentStationByGroundStation.containsKey(station)) {
				List<String[]> busStationList = mapParentStationByGroundStation.get(station);

				if (busStationList != null && busStationList.size() > 0) {
					Iterator<String[]> busStationItr = busStationList.iterator();
					while (busStationItr.hasNext()) {
						String[] busStationArray = busStationItr.next();
						if (busStationArray[0] != null && busStationArray[0].equalsIgnoreCase(busStation)) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	protected static boolean isPrimarySurfaceStation(String station) {
		return station != null && !station.isEmpty()
				&& CommonsServices.getGlobalConfig().getMapTopLevelStationByConnectedStation().containsKey(station.trim());
	}

	protected static String getMainGroundStationBySegmentCode(String segmentCode, boolean isFromAirport, boolean isToAirport) {
		String stationCode = null;
		if (isFromAirport) {
			stationCode = segmentCode.substring(0, 3);
		} else if (isToAirport) {
			stationCode = segmentCode.substring(segmentCode.length() - 3, segmentCode.length());
		}
		return stationCode;
	}

	protected static void debugMe(Object obj) {
		if (!log.isDebugEnabled()) {
			return;
		}
		if (obj instanceof SurfaceSegmentValidationTO) {
			SurfaceSegmentValidationTO validationTo = (SurfaceSegmentValidationTO) obj;
			log.debug("----- Surface Segment Validations -----");
			log.debug("isOriginConnectingStation      " + validationTo.isOriginConnectingStation());
			log.debug("isDestinationConnectingStation " + validationTo.isDestinationConnectingStation());
			log.debug("isOriginSubStation		      " + validationTo.isOriginSubStation());
			log.debug("isDestinationSubStation	      " + validationTo.isDestinationSubStation());
		}

		if (obj instanceof HashMap) {

		}
	}
}
