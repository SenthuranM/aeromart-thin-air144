package com.isa.thinair.webplatform.api.dto;

import java.util.Collection;
import java.util.Set;

public class HolidaysLinkParamsTO {

	private String country;

	private String language;

	private String currency;

	private String destination;

	private String checkIn;

	private String nights;

	private String roomType;

	private String rooms;

	private String starrating;

	private String staticURL;

	private Set passengers;

	private Collection segmentView;
	
	private String checkin_monthday;
	
	private String checkin_year_month;
	
	private String checkout_monthday;
	
	private String checkout_year_month;
	
	private String city;
	
	private String cityName;
	
	private String ancillaryReminderURL;
	

	/**
	 * @return Returns the checkIn.
	 */
	public String getCheckIn() {
		return checkIn;
	}

	/**
	 * @param checkIn
	 *            The checkIn to set.
	 */
	public void setCheckIn(String checkIn) {
		this.checkIn = checkIn;
	}

	/**
	 * @return Returns the country.
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            The country to set.
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return Returns the currency.
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            The currency to set.
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return Returns the destination.
	 */
	public String getDestination() {
		return destination;
	}

	/**
	 * @param destination
	 *            The destination to set.
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * @return Returns the language.
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            The language to set.
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return Returns the nights.
	 */
	public String getNights() {
		return nights;
	}

	/**
	 * @param nights
	 *            The nights to set.
	 */
	public void setNights(String nights) {
		this.nights = nights;
	}

	/**
	 * @return Returns the rooms.
	 */
	public String getRooms() {
		return rooms;
	}

	/**
	 * @param rooms
	 *            The rooms to set.
	 */
	public void setRooms(String rooms) {
		this.rooms = rooms;
	}

	/**
	 * @return Returns the roomType.
	 */
	public String getRoomType() {
		return roomType;
	}

	/**
	 * @param roomType
	 *            The roomType to set.
	 */
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	/**
	 * @return Returns the starrating.
	 */
	public String getStarrating() {
		return starrating;
	}

	/**
	 * @param starrating
	 *            The starrating to set.
	 */
	public void setStarrating(String starrating) {
		this.starrating = starrating;
	}

	/**
	 * @return Returns the staticURL.
	 */
	public String getStaticURL() {
		return staticURL;
	}

	/**
	 * @param staticURL
	 *            The staticURL to set.
	 */
	public void setStaticURL(String staticURL) {
		this.staticURL = staticURL;
	}

	/**
	 * @return Returns the passengers.
	 */
	public Set getPassengers() {
		return passengers;
	}

	/**
	 * @param passengers
	 *            The passengers to set.
	 */
	public void setPassengers(Set passengers) {
		this.passengers = passengers;
	}

	/**
	 * @return Returns the segmentView.
	 */
	public Collection getSegmentView() {
		return segmentView;
	}

	/**
	 * @param segmentView
	 *            The segmentView to set.
	 */
	public void setSegmentView(Collection segmentView) {
		this.segmentView = segmentView;
	}

	public String getCheckin_monthday() {
		return checkin_monthday;
	}

	public void setCheckin_monthday(String checkinMonthday) {
		checkin_monthday = checkinMonthday;
	}

	public String getCheckin_year_month() {
		return checkin_year_month;
	}

	public void setCheckin_year_month(String checkinYearMonth) {
		checkin_year_month = checkinYearMonth;
	}

	public String getCheckout_monthday() {
		return checkout_monthday;
	}

	public void setCheckout_monthday(String checkoutMonthday) {
		checkout_monthday = checkoutMonthday;
	}

	public String getCheckout_year_month() {
		return checkout_year_month;
	}

	public void setCheckout_year_month(String checkoutYearMonth) {
		checkout_year_month = checkoutYearMonth;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getAncillaryReminderURL() {
		return ancillaryReminderURL;
	}

	public void setAncillaryReminderURL(String ancillaryReminderURL) {
		this.ancillaryReminderURL = ancillaryReminderURL;
	}
}
