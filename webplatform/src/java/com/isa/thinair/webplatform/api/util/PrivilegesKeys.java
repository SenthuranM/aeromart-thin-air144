package com.isa.thinair.webplatform.api.util;

/**
 * This should be in Airproxy. These could be used in both modules and web modules. Therefore moving to 
 * the webplatform is out of the question. Having this in airproxy makes sense than keeping in commons.
 */
public interface PrivilegesKeys extends com.isa.thinair.airproxy.api.utils.PrivilegesKeys {

}
