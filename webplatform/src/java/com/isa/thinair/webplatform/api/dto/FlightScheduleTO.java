package com.isa.thinair.webplatform.api.dto;

import java.util.Collection;

public class FlightScheduleTO {
	private String flightStartTime;

	private String flightStopTime;

	private String flightFrequency;

	private String flightStartZulu;

	private String flightStopZulu;

	private String flightNumber;

	private String destinationFrom;

	private String destinationTo;

	private String flightModelNumber;

	private Collection<FlightSegmentTO> flilghtSegmentTO;

	private Collection<FlightLegTO> flightLegTO;

	private String mainScheduleFrom;

	private String mainScheduleTo;

	/**
	 * @return the flightStartTime
	 */
	public String getFlightStartTime() {
		return flightStartTime;
	}

	/**
	 * @param flightStartTime
	 *            the flightStartTime to set
	 */
	public void setFlightStartTime(String flightStartTime) {
		this.flightStartTime = flightStartTime;
	}

	/**
	 * @return the flightStopTime
	 */
	public String getFlightStopTime() {
		return flightStopTime;
	}

	/**
	 * @param flightStopTime
	 *            the flightStopTime to set
	 */
	public void setFlightStopTime(String flightStopTime) {
		this.flightStopTime = flightStopTime;
	}

	/**
	 * @return the flightFrequency
	 */
	public String getFlightFrequency() {
		return flightFrequency;
	}

	/**
	 * @param flightFrequency
	 *            the flightFrequency to set
	 */
	public void setFlightFrequency(String flightFrequency) {
		this.flightFrequency = flightFrequency;
	}

	/**
	 * @return the flightStartZulu
	 */
	public String getFlightStartZulu() {
		return flightStartZulu;
	}

	/**
	 * @param flightStartZulu
	 *            the flightStartZulu to set
	 */
	public void setFlightStartZulu(String flightStartZulu) {
		this.flightStartZulu = flightStartZulu;
	}

	/**
	 * @return the flightStopZulu
	 */
	public String getFlightStopZulu() {
		return flightStopZulu;
	}

	/**
	 * @param flightStopZulu
	 *            the flightStopZulu to set
	 */
	public void setFlightStopZulu(String flightStopZulu) {
		this.flightStopZulu = flightStopZulu;
	}

	/**
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the destinationFrom
	 */
	public String getDestinationFrom() {
		return destinationFrom;
	}

	/**
	 * @param destinationFrom
	 *            the destinationFrom to set
	 */
	public void setDestinationFrom(String destinationFrom) {
		this.destinationFrom = destinationFrom;
	}

	/**
	 * @return the destinationTo
	 */
	public String getDestinationTo() {
		return destinationTo;
	}

	/**
	 * @param destinationTo
	 *            the destinationTo to set
	 */
	public void setDestinationTo(String destinationTo) {
		this.destinationTo = destinationTo;
	}

	/**
	 * @return the flightModelNumber
	 */
	public String getFlightModelNumber() {
		return flightModelNumber;
	}

	/**
	 * @param flightModelNumber
	 *            the flightModelNumber to set
	 */
	public void setFlightModelNumber(String flightModelNumber) {
		this.flightModelNumber = flightModelNumber;
	}

	/**
	 * @return the mainScheduleFrom
	 */
	public String getMainScheduleFrom() {
		return mainScheduleFrom;
	}

	/**
	 * @param mainScheduleFrom
	 *            the mainScheduleFrom to set
	 */
	public void setMainScheduleFrom(String mainScheduleFrom) {
		this.mainScheduleFrom = mainScheduleFrom;
	}

	/**
	 * @return the mainScheduleTo
	 */
	public String getMainScheduleTo() {
		return mainScheduleTo;
	}

	/**
	 * @param mainScheduleTo
	 *            the mainScheduleTo to set
	 */
	public void setMainScheduleTo(String mainScheduleTo) {
		this.mainScheduleTo = mainScheduleTo;
	}

	/**
	 * @return the flilghtSegmentTO
	 */
	public Collection<FlightSegmentTO> getFlilghtSegmentTO() {
		return flilghtSegmentTO;
	}

	/**
	 * @param flilghtSegmentTO
	 *            the flilghtSegmentTO to set
	 */
	public void setFlilghtSegmentTO(Collection<FlightSegmentTO> flilghtSegmentTO) {
		this.flilghtSegmentTO = flilghtSegmentTO;
	}

	/**
	 * @return the flightLegTO
	 */
	public Collection<FlightLegTO> getFlightLegTO() {
		return flightLegTO;
	}

	/**
	 * @param flightLegTO
	 *            the flightLegTO to set
	 */
	public void setFlightLegTO(Collection<FlightLegTO> flightLegTO) {
		this.flightLegTO = flightLegTO;
	}

}