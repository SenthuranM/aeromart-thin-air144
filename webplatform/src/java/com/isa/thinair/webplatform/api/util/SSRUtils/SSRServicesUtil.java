package com.isa.thinair.webplatform.api.util.SSRUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.SpecialServiceRequestDTO;
import com.isa.thinair.airpricing.api.dto.SSRChargeDTO;
import com.isa.thinair.airpricing.api.dto.SSRChargeSearchDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airreservation.api.dto.ssr.MedicalSsrDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.webplatform.api.v2.reservation.ReservationPaxTO;
import com.isa.thinair.webplatform.core.commons.DatabaseUtil;

/**
 * Utlility to extract valid airport services (SSR details)
 * 
 * @author dumindag
 * @since 1.0
 */
public class SSRServicesUtil {

	private static Log log = LogFactory.getLog(SSRServicesUtil.class);

	/**
	 * Get Segment Code
	 * 
	 * @param fltSegID
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static LinkedHashMap getSegmentCode(Integer fltSegID) {
		try {
			String segmentCode = null;
			LinkedList stack = new LinkedList();
			LinkedHashMap flightSegDetails = new LinkedHashMap();

			segmentCode = DatabaseUtil.getFlightSegment(String.valueOf(fltSegID));
			if (segmentCode != null && !segmentCode.equals("")) {
				StringTokenizer st = new StringTokenizer(segmentCode, "/", false);
				while (st.hasMoreElements()) {
					stack.addFirst(st.nextElement());
					flightSegDetails.put(fltSegID, stack);
				}
				return flightSegDetails;
			} else {
				return null;
			}
		} catch (ModuleException e) {
			log.error("Error in getSegmentCode", e);
			return null;
		}
	}

	/**
	 * Attach Segment IDs
	 * <p>
	 * Segment IDs are needed to be sent through the front end to be used when saving external charges
	 * </p>
	 * <p>
	 * not very good.its a headache to attach flightSegmentID, but no other option
	 * </p>
	 * 
	 * @param ssrChargeDTOs
	 * @param colSSRIds
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Map attachSegmetIDs(Map ssrChargeDTOs, List<Integer> colSegIds, Reservation reservation) {

		if (!ssrChargeDTOs.isEmpty()) {
			Map<String, Collection> ssrChargesWSegIDs = new HashMap<String, Collection>();
			Collection<Integer> colFilterdSegIDs = setValidSegments(colSegIds);
			Map SegSeq = null;
			if (reservation != null) {
				try {
					SegSeq = ReservationApiUtils.loadSegmentSeq(reservation);

					SegSeq = updateMap(SegSeq, colSegIds);

				} catch (ModuleException e) {
					log.error(e);
				}
			}

			for (Integer segId : colFilterdSegIDs) {
				String segment = null;
				LinkedHashMap seg = new LinkedHashMap();
				LinkedList segmentCodes = new LinkedList();
				boolean breakFromDisplTran = false;
				seg = getSegmentCode(segId); // getSegment Codes
				segmentCodes = (LinkedList) seg.get(segId);

				ListIterator itrSegmentCodes = segmentCodes.listIterator();
				int outGoingSegment = 0;
				outGoingSegment = getApplicabilityCode(segmentCodes);
				while (itrSegmentCodes.hasNext()) {
					segment = (String) itrSegmentCodes.next();
					if (segment != null) {
						HashSet<SSRChargeDTO> colSsrCharges = (HashSet) ssrChargeDTOs.get(segment);
						if (colSsrCharges != null) {
							for (SSRChargeDTO ssrChargeDTO : colSsrCharges) {
								Collection<SSRChargeDTO> colSSRSegDTOs = ssrChargesWSegIDs.get(segment);
								SSRChargeDTO newSSRChargeDTO = null;

								if (colSSRSegDTOs == null) {
									colSSRSegDTOs = new ArrayList<SSRChargeDTO>();
									ssrChargesWSegIDs.put(segment, colSSRSegDTOs);
								}

								try {
									newSSRChargeDTO = ssrChargeDTO.clone();
									Integer intSegSeq = 0;
									if (reservation != null && SegSeq != null) {
										intSegSeq = (Integer) SegSeq.get(segId);
									}
									newSSRChargeDTO.setSegmentSeq(intSegSeq);

									newSSRChargeDTO.setAttachSegmentCode(segId);
									if (newSSRChargeDTO.isApplyForTransit()) {
										newSSRChargeDTO.setApplyForArrival(false);
										newSSRChargeDTO.setApplyForDeparture(false);
										if (colSegIds.size() < 3) {
											breakFromDisplTran = true;
											newSSRChargeDTO.setOneway(true);
										}
									} else if (!newSSRChargeDTO.isApplyForTransit() && !newSSRChargeDTO.isApplyForDeparture()
											&& !newSSRChargeDTO.isApplyForArrival()) {

									} else if (outGoingSegment == 1) {
										newSSRChargeDTO.setApplyForArrival(false);
										newSSRChargeDTO.setApplyForDeparture(true);

									} else if (outGoingSegment == 2) {
										newSSRChargeDTO.setApplyForArrival(true);
										newSSRChargeDTO.setApplyForDeparture(false);
									} else {
										newSSRChargeDTO.setApplyForArrival(false);
										newSSRChargeDTO.setApplyForDeparture(false);
									}

									colSSRSegDTOs.add(newSSRChargeDTO);
								} catch (CloneNotSupportedException e) {
									throw new RuntimeException(e);
								}

							}
						}
					}
				}
				if (breakFromDisplTran) {
					break;
				}
			} // end for
			if (log.isDebugEnabled()) {
				debugListSSRChargeInfo(ssrChargesWSegIDs);
			}
			return ssrChargesWSegIDs;// ssrChargesWSegIDs;
		} else {
			return null;
		}

	}

	/**
	 * <b>set Applicability value</b>
	 * <p>
	 * Instead of taking three variables to the frontend and putting through loops in JS here we identify the
	 * applicability of a SSR and returns a single digit value according to the applicability
	 * </p>
	 * <br>
	 * <p>
	 * Frontend UI is generated using this value. Less data and processing in JS level making the JS more manageable
	 * </p>
	 * 
	 * @param isAppArival
	 * @param isAppDep
	 * @param isAppTransit
	 * @return
	 */
	public static int setApplicability(boolean isAppArival, boolean isAppDep, boolean isAppTransit) {
		int ADT = 0;

		if (isAppArival) {
			ADT += 1;
		}
		if (isAppDep) {
			ADT += 3;
		}
		if (isAppTransit) {
			ADT += 5;
		}

		return ADT;
	}

	@SuppressWarnings("all")
	private static List setValidSegments(List colSegIDs) {
		List filterdSeg = new ArrayList();

		if (colSegIDs.size() > 3) {
			filterdSeg.add(colSegIDs.get(0));
			filterdSeg.add(colSegIDs.get(2));
		} else {
			return colSegIDs;
		}

		return filterdSeg;
	}

	/**
	 * Get Applicability of SSRChargeDTO Arrival \ Dep
	 * 
	 * @param segmentCodes
	 * @return
	 */
	protected static int getApplicabilityCode(LinkedList segmentCodes) {
		String Base = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM);
		if (segmentCodes.getFirst().toString().equalsIgnoreCase(Base)) {
			return 2;
		}
		if (segmentCodes.getLast().toString().equalsIgnoreCase(Base)) {
			return 1;
		}
		return 0;
	}

	@SuppressWarnings("unchecked")
	private static void debugListSSRChargeInfo(Map ssrChargeInfo) {
		log.debug("Start debug SSR CHARGE DETAILS");
		log.debug("------------------------------");
		try {
			Set<String> keySet = ssrChargeInfo.keySet();
			if (keySet != null) {
				for (String key : keySet) {
					Collection<SSRChargeDTO> colSsrChargeDetails = (Collection) ssrChargeInfo.get(key);
					log.debug("#--- AIRPORT " + key);
					for (SSRChargeDTO ssrCharge : colSsrChargeDetails) {
						log.debug("         SSR ID           :" + ssrCharge.getSSRId());
						log.debug("         SSR CODE         :" + ssrCharge.getSSRCode());
						log.debug("         SSR DESCRIPTION  :" + ssrCharge.getDescription());
						log.debug("         SSR CHARGE ID    :" + ssrCharge.getSSRChargeId());
						log.debug("         SSR CHARGE CODE  :" + ssrCharge.getSSRChargeCode());
						log.debug("         OND              :" + ssrCharge.getONDCode());
						log.debug("         ARRIVAL          :" + String.valueOf(ssrCharge.isApplyForArrival()));
						log.debug("         DEPARTURE        :" + String.valueOf(ssrCharge.isApplyForDeparture()));
						log.debug("         TRANSIT          :" + String.valueOf(ssrCharge.isApplyForTransit()));
						log.debug("         CHARGES          :" + ssrCharge.getChargeAmount());
						log.debug("         CHARGES AD       :" + setPaxSSRCharges(ssrCharge.getAdultCharges()));
						log.debug("         CHARGES CH       :" + setPaxSSRCharges(ssrCharge.getChildCharges()));
						log.debug("         CHARGES INF      :" + setPaxSSRCharges(ssrCharge.getInfantCharges()));
						log.debug("         SEGMENT CODE     :" + ssrCharge.getAttachSegmentCode());
						log.debug("         MAX MIN AD PAS   :" + ssrCharge.getMaximumAdultPax() + " < AD < "
								+ ssrCharge.getMinimumAdultPax());
						log.debug("         MAX MIN CH PAS   :" + ssrCharge.getMaximumChildPax() + " < CH < "
								+ ssrCharge.getMinimumChildPax());
						log.debug("");
					}
				}
			}
			log.debug("End debug SSR CHARGE DETAILS");
			log.debug("----------------------------");

		} catch (Exception e) {
			log.error(e); // Do not throw
		}
	}

	private static String setPaxSSRCharges(BigDecimal[] charge) {
		StringBuilder strCharges = new StringBuilder();
		for (int a = 0; a < charge.length; a++) {
			strCharges.append(charge[a]).append(", ");
		}

		return strCharges.toString();
	}

	@SuppressWarnings("unchecked")
	public static Map getReservationSSR(Reservation reservation) {

		Map mapPaxSSR = new HashMap();
		Collection<ReservationPax> paxList = reservation.getPassengers();
		for (ReservationPax currPax : paxList) {
			mapPaxSSR.put(currPax.getPnrPaxId(), currPax.getPaxSSR());
		}
		return mapPaxSSR;
	}

	@SuppressWarnings("unchecked")
	public static SSRChargeSearchDTO setSSRSearchDTO(Reservation reservation) throws ModuleException {

		SSRChargeSearchDTO ssrChargeSearchDTO = new SSRChargeSearchDTO();
		ssrChargeSearchDTO.setAdultCount(reservation.getTotalPaxAdultCount());
		ssrChargeSearchDTO.setChildCount(reservation.getTotalPaxChildCount());
		ssrChargeSearchDTO.setInfantCount(reservation.getTotalPaxInfantCount());
		String[] arrOnd = getOnd(reservation);
		ssrChargeSearchDTO.setFromAirpot(arrOnd[0]);
		ssrChargeSearchDTO.setToAirpot(arrOnd[1]);
		ssrChargeSearchDTO.setAppIndicator(AppIndicatorEnum.APP_IBE);
		ssrChargeSearchDTO.setFlightSegments(ReservationApiUtils.getFlightSegments(reservation));
		return ssrChargeSearchDTO;
	}

	public static boolean isDepartureFromBase(String[] OnD) {
		if (OnD[0].trim().equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM))) {
			return true;
		} else {
			return false;
		}
	}

	public static String[] getOnd(Reservation reservation) throws ModuleException {
		String strOnd = ReservationApiUtils.getOndCode(reservation);
		if (strOnd != null) {
			return strOnd.split("/");
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	private static Map updateMap(Map mapSegSeq, List colSegIds) {

		for (int key = 0; key < colSegIds.size(); key++) {
			if (mapSegSeq.keySet().contains(colSegIds.get(key))) {
				return mapSegSeq;
			}
		}

		mapSegSeq.put(colSegIds.get(0), mapSegSeq.size() + 1);
		return mapSegSeq;

	}

	@SuppressWarnings("unchecked")
	public static Map getSsrDescMap(String langCode) throws ModuleException {
		Map mapSsrDesc = DatabaseUtil.getSsrDesc(langCode);

		return mapSsrDesc;

	}

	public static List<SpecialServiceRequestDTO> getUniqueSSRList(List<SpecialServiceRequestDTO> inSSRDTos) {
		List<SpecialServiceRequestDTO> outSSRDTOs = new ArrayList<SpecialServiceRequestDTO>();
		Map<String, SpecialServiceRequestDTO> ssrMap = new HashMap<String, SpecialServiceRequestDTO>();
		for (SpecialServiceRequestDTO ssr : inSSRDTos) {
			ssrMap.put(ssr.getSsrCode(), ssr);
		}
		for (SpecialServiceRequestDTO ssr : ssrMap.values()) {
			outSSRDTOs.add(ssr);
		}

		return outSSRDTOs;
	}

	/*
	 * Method use to set segment sequence for ssr external charge DTO objects
	 */
	public static void setSegmentSeqForSSR(List<ReservationPaxTO> paxList, List<FlightSegmentTO> flightSegmentTOs,
			Integer startFltSegSeq) {

		SegmentUtil.setFltSegSequence(flightSegmentTOs, startFltSegSeq);

		for (ReservationPaxTO reservationPaxTO : paxList) {
			if (!PaxTypeTO.INFANT.equals(reservationPaxTO.getPaxType())) {
				setextChgSequence(reservationPaxTO.getExternalCharges(), flightSegmentTOs);
				setSelectedAnciFltSegSeq(reservationPaxTO.getSelectedAncillaries(), flightSegmentTOs);
			}
		}
	}

	private static void setSelectedAnciFltSegSeq(List<LCCSelectedSegmentAncillaryDTO> selAnciList,
			List<FlightSegmentTO> flightSegmentTOs) {
		if (selAnciList != null && selAnciList.size() > 0) {
			for (LCCSelectedSegmentAncillaryDTO selAnciDTO : selAnciList) {
				if (selAnciDTO.getFlightSegmentTO() != null && flightSegmentTOs != null) {
					selAnciDTO.getFlightSegmentTO().setSegmentSequence(
							getSegmentSeqFromSegments(flightSegmentTOs, selAnciDTO.getFlightSegmentTO().getFlightRefNumber()));
				}
			}
		}
	}

	private static void setextChgSequence(Collection<LCCClientExternalChgDTO> externalChgDTOs,
			List<FlightSegmentTO> flightSegmentTOs) {
		if (externalChgDTOs != null) {
			for (LCCClientExternalChgDTO extChgDTO : externalChgDTOs) {
				if (extChgDTO.getExternalCharges() == EXTERNAL_CHARGES.AIRPORT_SERVICE) {
					extChgDTO.setSegmentSequence(getSegmentSeqFromSegments(flightSegmentTOs, extChgDTO.getFlightRefNumber()));
				} else if (extChgDTO.getExternalCharges() == EXTERNAL_CHARGES.AIRPORT_TRANSFER) {
					extChgDTO.setSegmentSequence(getSegmentSeqFromSegments(flightSegmentTOs, extChgDTO.getFlightRefNumber()));
				} else if (extChgDTO.getExternalCharges() == EXTERNAL_CHARGES.INFLIGHT_SERVICES) {
					extChgDTO.setSegmentSequence(getSegmentSeqFromSegments(flightSegmentTOs, extChgDTO.getFlightRefNumber()));
				}
			}
		}
	}

	private static int getSegmentSeqFromSegments(List<FlightSegmentTO> flightSegmentTOs, String fltRefNo) {
		int seq = 0;
		for (FlightSegmentTO fltSegment : flightSegmentTOs) {
			if (fltSegment.getFlightRefNumber().equals(fltRefNo)) {
				seq = fltSegment.getSegmentSequence();
				break;
			}
		}

		return seq;
	}

	public static Map<String, Integer> extractAiportServiceCountFromJsonObj(Collection<LCCClientReservationPax> colpaxs) {
		Map<String, Integer> apServiceCount = new HashMap<String, Integer>();

		for (LCCClientReservationPax pax : colpaxs) {
			List<LCCSelectedSegmentAncillaryDTO> anciList = pax.getSelectedAncillaries();
			for (LCCSelectedSegmentAncillaryDTO anciDTO : anciList) {
				if (anciDTO != null && anciDTO.getAirportServiceDTOs() != null && anciDTO.getAirportServiceDTOs().size() > 0) {
					String fltRef = anciDTO.getFlightSegmentTO().getFlightRefNumber();
					List<LCCAirportServiceDTO> apServiceList = anciDTO.getAirportServiceDTOs();

					for (LCCAirportServiceDTO apService : apServiceList) {
						if (apServiceCount.get(fltRef + AirportServiceDTO.SEPERATOR + apService.getAirportCode()) == null) {
							apServiceCount.put(fltRef + AirportServiceDTO.SEPERATOR + apService.getAirportCode(), 1);
						} else {
							int c = apServiceCount.get(fltRef + AirportServiceDTO.SEPERATOR + apService.getAirportCode());
							apServiceCount.put(fltRef + AirportServiceDTO.SEPERATOR + apService.getAirportCode(), ++c);
						}
					}

				}
			}
		}

		return apServiceCount;
	}

	public static void sendMedicalSsrEmail(LCCClientReservation reservation) {
		Map<Integer, List<LCCClientExternalChgDTO>> paxSeqWiseExternalChgMap = getPaxSeqWiseExtChgDTO(reservation.getPassengers());

		boolean hasBalanceDue = (reservation.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) > 0);

		MedicalSsrDTO medicalSsrDTO = new MedicalSsrDTO(reservation.getPNR(), reservation.getPassengers(),
				paxSeqWiseExternalChgMap, reservation.getContactInfo(), hasBalanceDue);
		medicalSsrDTO.sendEmail();
	}

	public static void sendMedicalSsrEmail(String pnr, Collection<LCCClientReservationPax> paxSet,
			Map<Integer, List<LCCClientExternalChgDTO>> paxSeqWiseExternalChgMap, CommonReservationContactInfo contactInfo,
			boolean hasBalanceDue) {
		MedicalSsrDTO medicalSsrDTO = new MedicalSsrDTO(pnr, paxSet, paxSeqWiseExternalChgMap, contactInfo, hasBalanceDue);
		medicalSsrDTO.sendEmail();
	}

	public static void sendMedicalSsrEmail(String pnr, Collection<LCCClientReservationPax> paxSet,
			List<ReservationPaxTO> anciIntegratedPaxList, CommonReservationContactInfo contactInfo) {
		Map<Integer, List<LCCClientExternalChgDTO>> paxSeqWiseExternalChgMap = getPaxSeqWiseExtChgDTO(anciIntegratedPaxList);
		MedicalSsrDTO medicalSsrDTO = new MedicalSsrDTO(pnr, paxSet, paxSeqWiseExternalChgMap, contactInfo, false);
		medicalSsrDTO.sendEmail();
	}

	private static Map<Integer, List<LCCClientExternalChgDTO>> getPaxSeqWiseExtChgDTO(
			Set<LCCClientReservationPax> lccClientReservationPaxSet) {

		Map<Integer, List<LCCClientExternalChgDTO>> paxSeqWiseExternalChgMap = new HashMap<Integer, List<LCCClientExternalChgDTO>>();

		for (Iterator<LCCClientReservationPax> lccClientReservationPaxSetIterator = lccClientReservationPaxSet.iterator(); lccClientReservationPaxSetIterator
				.hasNext();) {
			LCCClientReservationPax pax = lccClientReservationPaxSetIterator.next();
			List<LCCClientExternalChgDTO> externalChgDtoList = new ArrayList<LCCClientExternalChgDTO>();

			List<LCCSelectedSegmentAncillaryDTO> lccSelectedSegmentAncillaryDTO = pax.getSelectedAncillaries();

			for (Iterator<LCCSelectedSegmentAncillaryDTO> anciListIterator = lccSelectedSegmentAncillaryDTO.iterator(); anciListIterator
					.hasNext();) {
				LCCSelectedSegmentAncillaryDTO selectedSsrAnciDTO = anciListIterator.next();

				List<LCCSpecialServiceRequestDTO> ssrDto = selectedSsrAnciDTO.getSpecialServiceRequestDTOs();

				for (Iterator<LCCSpecialServiceRequestDTO> ssrListIterator = ssrDto.iterator(); ssrListIterator.hasNext();) {
					LCCSpecialServiceRequestDTO ssr = ssrListIterator.next();
					LCCClientExternalChgDTO externalChg = new LCCClientExternalChgDTO();
					externalChg.setCode(ssr.getSsrCode());
					externalChgDtoList.add(externalChg);
				}
			}

			paxSeqWiseExternalChgMap.put(pax.getPaxSequence(), externalChgDtoList);
		}

		return paxSeqWiseExternalChgMap;
	}

	private static Map<Integer, List<LCCClientExternalChgDTO>>
			getPaxSeqWiseExtChgDTO(List<ReservationPaxTO> anciIntegratedPaxList) {

		Map<Integer, List<LCCClientExternalChgDTO>> paxSeqWiseExternalChgMap = new HashMap<Integer, List<LCCClientExternalChgDTO>>();

		if (anciIntegratedPaxList != null) {
			for (Iterator<ReservationPaxTO> reservationPaxIterator = anciIntegratedPaxList.iterator(); reservationPaxIterator
					.hasNext();) {
				ReservationPaxTO pax = reservationPaxIterator.next();

				List<LCCClientExternalChgDTO> externalChgDtoList = new ArrayList<LCCClientExternalChgDTO>();

				List<LCCSelectedSegmentAncillaryDTO> lccSelectedSegmentAncillaryDTO = pax.getSelectedAncillaries();

				for (Iterator<LCCSelectedSegmentAncillaryDTO> anciListIterator = lccSelectedSegmentAncillaryDTO.iterator(); anciListIterator
						.hasNext();) {
					LCCSelectedSegmentAncillaryDTO selectedSsrAnciDTO = anciListIterator.next();

					List<LCCSpecialServiceRequestDTO> ssrDto = selectedSsrAnciDTO.getSpecialServiceRequestDTOs();

					for (Iterator<LCCSpecialServiceRequestDTO> ssrListIterator = ssrDto.iterator(); ssrListIterator.hasNext();) {
						LCCSpecialServiceRequestDTO ssr = ssrListIterator.next();
						LCCClientExternalChgDTO externalChg = new LCCClientExternalChgDTO();
						externalChg.setCode(ssr.getSsrCode());
						externalChgDtoList.add(externalChg);
					}
				}

				paxSeqWiseExternalChgMap.put(pax.getSeqNumber(), externalChgDtoList);
			}
		}

		return paxSeqWiseExternalChgMap;
	}

	/*
	 * This method is used to validate whether the medical ssr in all segments for a given passenger. If passenger
	 * select medical ssr for a given segment he/she should select medical ssr for other segments. MEDA: medical ssr
	 * code
	 */
	public static void checkMedicalSsrInAllSegments(Collection<ReservationPaxTO> paxList, List<FlightSegmentTO> flightSegmentTOs,
			String ssrCode) throws ModuleException {
		int segmentSize = getNumberOfUnFlownSegmentSize(flightSegmentTOs);

		for (Iterator<ReservationPaxTO> paxListIterator = paxList.iterator(); paxListIterator.hasNext();) {
			boolean medicalSsrInAllSegmentsIfThere = false;
			ReservationPaxTO pax = paxListIterator.next();

			int numberOfSegmentsContainsMedicalSsr = getNumberOfSegmentsContainsSsr(pax, ssrCode);
			int numberOfSegmentsDoNotContainMedicalSsr = getNumberOfSegmentsDoNotContainSsr(pax, ssrCode);

			if ((numberOfSegmentsContainsMedicalSsr == segmentSize) || (numberOfSegmentsDoNotContainMedicalSsr == segmentSize)) {
				medicalSsrInAllSegmentsIfThere = true;
			} else if ((numberOfSegmentsContainsMedicalSsr == 0) && (numberOfSegmentsDoNotContainMedicalSsr == 0)) {
				medicalSsrInAllSegmentsIfThere = true;
			} else {
				medicalSsrInAllSegmentsIfThere = false;
			}

			if (!medicalSsrInAllSegmentsIfThere) {
				throw new ModuleException("airreservations.ssr.medical.all.segments.should.have");
			}
		}
	}

	public static void alertMedicalSsrForAllSegments(LCCClientReservation reservation,
			Set<LCCClientReservationSegment> lccSegments, Collection<ReservationPaxTO> paxList, String ssrCode,
			List<FlightSegmentTO> newAllFltSegmentTOs) throws ModuleException {

		for (Iterator<ReservationPaxTO> paxListIterator = paxList.iterator(); paxListIterator.hasNext();) {
			ReservationPaxTO pax = paxListIterator.next();
			boolean validateSsrSegmentsInModify = validateSsrSegmentsInModify(pax, ssrCode);
			if (!validateSsrSegmentsInModify) {
				throw new ModuleException("airreservations.ssr.medical.all.requote.segments.validation");
			}
		}

		int allUnFlownSegmentSize = getNumberOfUnFlownLccSegmentSize(newAllFltSegmentTOs);

		if (allUnFlownSegmentSize > 1) {
			for (Iterator<ReservationPaxTO> paxListIterator = paxList.iterator(); paxListIterator.hasNext();) {
				ReservationPaxTO pax = paxListIterator.next();
				boolean isModifySegmentPaxContainsSSR = isPaxContainsSsr(pax, ssrCode);
				LCCClientReservationPax lccClientReservationPax = getLCCClientReservationPax(reservation.getPassengers(), pax);

				if (lccClientReservationPax != null) {
					boolean isPaxPreviouslyContainsSSR = isPaxContainsSsr(lccClientReservationPax, ssrCode);
					boolean isEqual = (isModifySegmentPaxContainsSSR == isPaxPreviouslyContainsSSR);

					if (!isEqual) {
						throw new ModuleException("airreservations.ssr.medical.all.requote.segments.validation");
					}
				}
			}
		}
	}

	private static int getNumberOfUnFlownLccSegmentSize(List<FlightSegmentTO> segmentSet) {
		int allUnFlownSegmentSize = 0;

		for (Iterator<FlightSegmentTO> segmentSetIterator = segmentSet.iterator(); segmentSetIterator.hasNext();) {
			FlightSegmentTO segment = segmentSetIterator.next();
			if (!segment.isFlownSegment()) {
				allUnFlownSegmentSize++;
			}
		}

		return allUnFlownSegmentSize;
	}

	private static int getNumberOfUnFlownSegmentSize(List<FlightSegmentTO> segmentSet) {
		int allUnFlownSegmentSize = 0;

		for (Iterator<FlightSegmentTO> segmentSetIterator = segmentSet.iterator(); segmentSetIterator.hasNext();) {
			FlightSegmentTO segment = segmentSetIterator.next();
			if (!segment.isFlownSegment()) {
				allUnFlownSegmentSize++;
			}
		}

		return allUnFlownSegmentSize;
	}

	private static LCCClientReservationPax getLCCClientReservationPax(Set<LCCClientReservationPax> reservationPaxList,
			ReservationPaxTO pax) {
		LCCClientReservationPax lccClientReservationPax = null;

		for (Iterator<LCCClientReservationPax> reservationPaxListIterator = reservationPaxList.iterator(); reservationPaxListIterator
				.hasNext();) {
			LCCClientReservationPax tempPax = reservationPaxListIterator.next();
			if (pax.getTravelerRefNumber().equals(tempPax.getTravelerRefNumber())) {
				lccClientReservationPax = tempPax;
				break;
			}
		}

		return lccClientReservationPax;
	}

	private static boolean isPaxContainsSsr(ReservationPaxTO pax, String ssrCode) {
		boolean isPaxContainsSsrInAllNewSegments = false;
		int numberOfsegmentsHasSSR = 0;

		List<LCCSelectedSegmentAncillaryDTO> anciToUpdateList = pax.getSelectedAncillaries();
		int newSegmentSize = anciToUpdateList.size();
		if (anciToUpdateList != null) {
			for (Iterator<LCCSelectedSegmentAncillaryDTO> anciToUpdateListIterator = anciToUpdateList.iterator(); anciToUpdateListIterator
					.hasNext();) {
				LCCSelectedSegmentAncillaryDTO selectedSegmentAnci = anciToUpdateListIterator.next();

				if (selectedSegmentAnci != null && !selectedSegmentAnci.getFlightSegmentTO().isFlownSegment()) {
					List<LCCSpecialServiceRequestDTO> ssrList = selectedSegmentAnci.getSpecialServiceRequestDTOs();
					for (Iterator<LCCSpecialServiceRequestDTO> ssrListIterator = ssrList.iterator(); ssrListIterator.hasNext();) {
						LCCSpecialServiceRequestDTO ssr = ssrListIterator.next();
						if (ssr.getSsrCode().equals(ssrCode)) {
							numberOfsegmentsHasSSR++;
						}
					}
				}

			}
		}

		if ((newSegmentSize == numberOfsegmentsHasSSR) && numberOfsegmentsHasSSR == 0) {
			isPaxContainsSsrInAllNewSegments = false;
		} else if (newSegmentSize == numberOfsegmentsHasSSR) {
			isPaxContainsSsrInAllNewSegments = true;
		}

		return isPaxContainsSsrInAllNewSegments;
	}

	private static boolean validateSsrSegmentsInModify(ReservationPaxTO pax, String ssrCode) {
		boolean isPaxContainsOrDoNotContainSsrInAllNewSegments = false;
		int numberOfsegmentsHasSSR = 0;

		List<LCCSelectedSegmentAncillaryDTO> anciToUpdateList = pax.getSelectedAncillaries();
		int newSegmentSize = anciToUpdateList.size();
		if (anciToUpdateList != null) {
			for (Iterator<LCCSelectedSegmentAncillaryDTO> anciToUpdateListIterator = anciToUpdateList.iterator(); anciToUpdateListIterator
					.hasNext();) {
				LCCSelectedSegmentAncillaryDTO selectedSegmentAnci = anciToUpdateListIterator.next();

				if (selectedSegmentAnci != null && !selectedSegmentAnci.getFlightSegmentTO().isFlownSegment()) {
					List<LCCSpecialServiceRequestDTO> ssrList = selectedSegmentAnci.getSpecialServiceRequestDTOs();
					for (Iterator<LCCSpecialServiceRequestDTO> ssrListIterator = ssrList.iterator(); ssrListIterator.hasNext();) {
						LCCSpecialServiceRequestDTO ssr = ssrListIterator.next();
						if (ssr.getSsrCode().equals(ssrCode)) {
							numberOfsegmentsHasSSR++;
						}
					}
				}

			}
		}

		if (newSegmentSize == numberOfsegmentsHasSSR || numberOfsegmentsHasSSR == 0) {
			isPaxContainsOrDoNotContainSsrInAllNewSegments = true;
		}

		return isPaxContainsOrDoNotContainSsrInAllNewSegments;
	}

	private static boolean isPaxContainsSsr(LCCClientReservationPax pax, String ssrCode) {
		boolean isPaxContainsSsr = false;

		List<LCCSelectedSegmentAncillaryDTO> anciToUpdateList = pax.getSelectedAncillaries();
		if (anciToUpdateList != null) {
			for (Iterator<LCCSelectedSegmentAncillaryDTO> anciToUpdateListIterator = anciToUpdateList.iterator(); anciToUpdateListIterator
					.hasNext();) {
				LCCSelectedSegmentAncillaryDTO selectedSegmentAnci = anciToUpdateListIterator.next();

				if (selectedSegmentAnci != null && !selectedSegmentAnci.getFlightSegmentTO().isFlownSegment()) {
					List<LCCSpecialServiceRequestDTO> ssrList = selectedSegmentAnci.getSpecialServiceRequestDTOs();
					for (Iterator<LCCSpecialServiceRequestDTO> ssrListIterator = ssrList.iterator(); ssrListIterator.hasNext();) {
						LCCSpecialServiceRequestDTO ssr = ssrListIterator.next();
						if (ssr.getSsrCode().equals(ssrCode)) {
							isPaxContainsSsr = true;
						}
					}
				}

			}
		}
		return isPaxContainsSsr;
	}

	private static int getNumberOfSegmentsContainsSsr(ReservationPaxTO pax, String ssrCode) {
		int numberOfSegmentsContainsMedicalSsr = 0;

		List<LCCSelectedSegmentAncillaryDTO> anciToUpdateList = pax.getSelectedAncillaries();
		if (anciToUpdateList != null) {
			for (Iterator<LCCSelectedSegmentAncillaryDTO> anciToUpdateListIterator = anciToUpdateList.iterator(); anciToUpdateListIterator
					.hasNext();) {
				LCCSelectedSegmentAncillaryDTO selectedSegmentAnci = anciToUpdateListIterator.next();

				if (selectedSegmentAnci != null && !selectedSegmentAnci.getFlightSegmentTO().isFlownSegment()) {
					List<LCCSpecialServiceRequestDTO> ssrList = selectedSegmentAnci.getSpecialServiceRequestDTOs();
					for (Iterator<LCCSpecialServiceRequestDTO> ssrListIterator = ssrList.iterator(); ssrListIterator.hasNext();) {
						LCCSpecialServiceRequestDTO ssr = ssrListIterator.next();
						if (ssr.getSsrCode().equals(ssrCode)) {
							numberOfSegmentsContainsMedicalSsr++;
						}
					}
				}

			}
		}
		return numberOfSegmentsContainsMedicalSsr;
	}

	private static int getNumberOfSegmentsDoNotContainSsr(ReservationPaxTO pax, String ssrCode) {
		int numberOfSegmentsDoNotContainMedicalSsr = 0;

		List<LCCSelectedSegmentAncillaryDTO> anciToRemoveList = pax.getAncillariesToRemove();

		if (anciToRemoveList != null) {
			for (Iterator<LCCSelectedSegmentAncillaryDTO> anciToRemoveListIterator = anciToRemoveList.iterator(); anciToRemoveListIterator
					.hasNext();) {
				LCCSelectedSegmentAncillaryDTO selectedSegmentAnci = anciToRemoveListIterator.next();

				if (selectedSegmentAnci != null && !selectedSegmentAnci.getFlightSegmentTO().isFlownSegment()) {
					List<LCCSpecialServiceRequestDTO> ssrList = selectedSegmentAnci.getSpecialServiceRequestDTOs();
					for (Iterator<LCCSpecialServiceRequestDTO> ssrListIterator = ssrList.iterator(); ssrListIterator.hasNext();) {
						LCCSpecialServiceRequestDTO ssr = ssrListIterator.next();
						if (ssr.getSsrCode().equals(ssrCode)) {
							numberOfSegmentsDoNotContainMedicalSsr++;
						}
					}
				}

			}
		}

		return numberOfSegmentsDoNotContainMedicalSsr;
	}
}
