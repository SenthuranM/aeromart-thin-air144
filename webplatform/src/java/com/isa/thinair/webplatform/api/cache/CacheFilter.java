package com.isa.thinair.webplatform.api.cache;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.webplatform.core.util.GenericHttpServletResponseWrapper;
import com.isa.thinair.webplatform.core.util.JSMin;

public class CacheFilter implements Filter {

	private static Log log = LogFactory.getLog(CacheFilter.class);
	
	ServletContext servletContext;

	FilterConfig filterConfig;

	int debug;

	long cacheTimeout = Long.MAX_VALUE;

	public void init(FilterConfig filterConfig) {

		this.filterConfig = filterConfig;
		this.servletContext = filterConfig.getServletContext();

		String strCacheTimeout = filterConfig.getInitParameter("cacheTimeout");
		String strDebug = filterConfig.getInitParameter("debug");

		if (strCacheTimeout != null) {
			cacheTimeout = 60 * 1000 * Long.parseLong(strCacheTimeout);
		}

		if (strDebug != null) {
			debug = Integer.parseInt(strDebug);
		} else {
			debug = 0;
		}
	}

	public void destroy() {
		this.servletContext = null;
		this.filterConfig = null;
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;

		// check if was a resource that shouldn't be cached.

		String noCache = filterConfig.getInitParameter(request.getRequestURI());
		String servletPath = request.getServletPath();

		if (noCache != null && noCache.equals("false")) {
			chain.doFilter(request, response);
			return;
		}

		String path = servletContext.getRealPath(servletPath);
		String id = servletPath;

		id = "/" + id.replace('/', '_');

		File tempDir = (File) servletContext.getAttribute("javax.servlet.context.tempdir");

		// get possible cache
		String temp = tempDir.getAbsolutePath();
		File file = new File(temp + id);

		File current = new File(path);

		try {
			long now = Calendar.getInstance().getTimeInMillis();
			// set timestamp check
			if (!file.exists() || (file.exists() && current.lastModified() > file.lastModified())
					|| cacheTimeout < now - file.lastModified()) {
				GenericHttpServletResponseWrapper wrappedResponse = null;
				String name = file.getAbsolutePath();
				name = name.substring(0, name.lastIndexOf("/") == -1 ? 0 : name.lastIndexOf("/"));
				new File(name).mkdirs();

				wrappedResponse = new GenericHttpServletResponseWrapper(response);
				wrappedResponse.setDebug(debug);

				chain.doFilter(req, wrappedResponse);
				wrappedResponse.finishResponse();

				if (wrappedResponse.getByteArray().length == 0) {
					chain.doFilter(request, response);
					return;
				}

				FileOutputStream fos = new FileOutputStream(file);
				int jsIdx = id.indexOf(".js");

				if (jsIdx != -1 && jsIdx == id.length() - 3) {
					// String js = new String();
					ByteArrayOutputStream output = new ByteArrayOutputStream();
					JSMin jsMin = new JSMin(new ByteArrayInputStream(wrappedResponse.getByteArray()), output);

					try {
						jsMin.jsmin();
						fos.write(output.toByteArray());
					} catch (Exception e) {
						log.error(e);
						fos.write(wrappedResponse.getByteArray());
					}
				} else {
					fos.write(wrappedResponse.getByteArray());
				}

				fos.flush();
				fos.close();
			}
		} catch (ServletException e) {
			log.error(e);
			if (!file.exists()) {
				chain.doFilter(request, response);
				return;
			}
		} catch (IOException e) {
			log.error(e);
			if (!file.exists()) {
				chain.doFilter(request, response);
				return;
			}
		}

		FileInputStream fis = new FileInputStream(file);
		// String mt = sc.getMimeType(request.getRequestURI());
		// response.setContentType(mt);
		response.setContentType(request.getContentType());
		ServletOutputStream sos = res.getOutputStream();

		for (int i = fis.read(); i != -1; i = fis.read()) {
			sos.write((byte) i);
		}

	}

}
