package com.isa.thinair.webplatform.api.dto;

import java.io.Serializable;

/**
 * Holds Validation details of flight search parameters
 * 
 * @author duminda G
 * 
 */
public class SurfaceSegmentValidationTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean isOriginSubStation = false;

	private boolean isDestinationSubStation = false;

	private boolean isOriginConnectingStation;

	private boolean isDestinationConnectingStation;

	public boolean isOriginSubStation() {
		return isOriginSubStation;
	}

	public boolean isDestinationSubStation() {
		return isDestinationSubStation;
	}

	public boolean isOriginConnectingStation() {
		return isOriginConnectingStation;
	}

	public boolean isDestinationConnectingStation() {
		return isDestinationConnectingStation;
	}

	public void setOriginSubStation(boolean isOriginSubStation) {
		this.isOriginSubStation = isOriginSubStation;
	}

	public void setDestinationSubStation(boolean isDestinationSubStation) {
		this.isDestinationSubStation = isDestinationSubStation;
	}

	public void setOriginConnectingStation(boolean isOriginConnectingStation) {
		this.isOriginConnectingStation = isOriginConnectingStation;
	}

	public void setDestinationConnectingStation(boolean isDestinationConnectingStation) {
		this.isDestinationConnectingStation = isDestinationConnectingStation;
	}
}
