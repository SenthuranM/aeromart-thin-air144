package com.isa.thinair.webplatform.api.v2.util;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;

/**
 * Contain utilities related to Tracking information related manipulations
 * 
 * @author Dilan Anuruddha
 * 
 */
public class TrackInfoUtil {

	public static final String APPLICATION_IDENTIFIER = "application.identifier";

	/**
	 * Compose BasicTrackInfo object of UserPrinciple available in the HttpServletRequest If no UserPrinciple is
	 * available on request, method will return null
	 * 
	 * @param request
	 * @return
	 * 
	 * @see BasicTrackInfo
	 * @see HttpServletRequest
	 */
	public static BasicTrackInfo getBasicTrackInfo(HttpServletRequest request) {
		BasicTrackInfo bt = null;
		if (request != null) {
			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
			if (userPrincipal != null) {
				bt = getBasicTrackInfo(userPrincipal);
			} else {
				String instanceId = SystemPropertyUtil.getInstanceId();
				String sessionId = request.getSession().getId();
				bt = getBasicTrackInfo(instanceId, sessionId);
			}
		}
		return bt;

	}

	/**
	 * Compose BasicTrackInfo object of UserPrinciple If no UserPrinciple is available, method will return null
	 * 
	 * @param request
	 * @return
	 * 
	 * @see UserPrincipal
	 */
	public static BasicTrackInfo getBasicTrackInfo(UserPrincipal userPrincipal) {
		BasicTrackInfo bt = null;
		if (userPrincipal != null) {
			bt = new BasicTrackInfo();
			bt.setSessionId(userPrincipal.getSessionId());
			bt.setCallingInstanceId(userPrincipal.getCallingInstanceId());
		}
		return bt;
	}

	private static BasicTrackInfo getBasicTrackInfo(String instanceId, String sessionId) {
		BasicTrackInfo bt = new BasicTrackInfo();
		bt.setCallingInstanceId(instanceId);
		bt.setSessionId(sessionId);
		return bt;
	}

	public static TrackInfoDTO updateTrackInfo(TrackInfoDTO trackInfo, HttpServletRequest request) {
		if (trackInfo != null) {
			BasicTrackInfo bt = getBasicTrackInfo(request);
			if (bt != null) {
				trackInfo.setCallingInstanceId(bt.getCallingInstanceId());
				trackInfo.setSessionId(bt.getSessionId());
			}
		}
		return trackInfo;
	}
	
	public static int getWebOriginSalesChannel(HttpServletRequest request) {
		int salesChannelCode = SalesChannelsUtil.SALES_CHANNEL_WEB;

		String appId = System.getProperty(APPLICATION_IDENTIFIER);

		if (appId != null) {
			switch (appId) {
			case SalesChannelsUtil.SALES_CHANNEL_IOS_KEY:
				salesChannelCode = SalesChannelsUtil.SALES_CHANNEL_IOS;
				break;
			case SalesChannelsUtil.SALES_CHANNEL_ANDROID_KEY:
				salesChannelCode = SalesChannelsUtil.SALES_CHANNEL_ANDROID;
				break;
			default:
				break;
			}
		}
		return salesChannelCode;
	}
	
	public static AppIndicatorEnum getAppIndicatorEnum(int salesChannelCode) {

		AppIndicatorEnum appIndicatorEnum = null;
		switch (salesChannelCode) {
		case SalesChannelsUtil.SALES_CHANNEL_AGENT:
			appIndicatorEnum = AppIndicatorEnum.APP_XBE;
			break;
		case SalesChannelsUtil.SALES_CHANNEL_PUBLIC:
			appIndicatorEnum = AppIndicatorEnum.APP_IBE;
			break;
		case SalesChannelsUtil.SALES_CHANNEL_WEB:
			appIndicatorEnum = AppIndicatorEnum.APP_IBE;
			break;
		case SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES:
			appIndicatorEnum = AppIndicatorEnum.APP_WS;
			break;
		case SalesChannelsUtil.SALES_CHANNEL_LCC:
			appIndicatorEnum = AppIndicatorEnum.APP_LCC;
			break;
		case SalesChannelsUtil.SALES_CHANNEL_GDS:
			appIndicatorEnum = AppIndicatorEnum.APP_GDS;
			break;
		case SalesChannelsUtil.SALES_CHANNEL_GOQUO:
			appIndicatorEnum = AppIndicatorEnum.APP_GOQUO;
			break;
		case SalesChannelsUtil.SALES_CHANNEL_IOS:
			appIndicatorEnum = AppIndicatorEnum.APP_IOS;
			break;
		case SalesChannelsUtil.SALES_CHANNEL_ANDROID:
			appIndicatorEnum = AppIndicatorEnum.APP_ANDROID;
			break;
		default:
			break;
		}

		return appIndicatorEnum;
	}

}
