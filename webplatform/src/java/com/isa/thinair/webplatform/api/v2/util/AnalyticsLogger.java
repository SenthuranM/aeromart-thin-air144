package com.isa.thinair.webplatform.api.v2.util;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class AnalyticsLogger {
	final private static String ANALYTICS_LOG = "analytics-log";
	private static Log log = LogFactory.getLog(ANALYTICS_LOG);

	public enum AnalyticSource {
		IBE_CRE_AVL, IBE_MCS_AVL, IBE_CRE_NXTPREV, IBE_MOD_AVL, IBE_MOD_NXTPREV, IBE_REQUOTE, XBE_CRE_AVL, XBE_REQUOTE, XBE_CRE_NXTPREV, XBE_MOD_AVL, XBE_MOD_NXTPREV, WS_CRE_AVL, XBE_MCS_AVL, XBE_MSC_FQ, XBE_CRE_FQ, XBE_MOD_FQ, IBE_MCS_FQ, WS_CRE_AVL_WFQ, WS_AVL_FQ, WS_MOD_REQUOTE, WS_CNX_REQUOTE, IBE_CRE_FQ, IBE_MOD_FQ;
	}

	private static void logData(AnalyticSource source, BaseAvailRQ baseRQ, HttpServletRequest request, String pnr,
			UserPrincipal currentUserPrincipal, TrackInfoDTO trackInfo) {
		try {
			String agentCode = null;
			String userId = null;
			String channel = null;
			String ip = null;
			if (trackInfo != null) {
				ip = trackInfo.getIpAddress();
			}
			if (request != null) {
				UserPrincipal up = (UserPrincipal) request.getUserPrincipal();
				if (up != null) {
					agentCode = up.getAgentCode();
					userId = up.getUserId();
					channel = up.getSalesChannel() + "";
					if (ip == null) {
						ip = up.getIpAddress();
					}
				}
			} else if (currentUserPrincipal != null) {

				agentCode = currentUserPrincipal.getAgentCode();
				userId = currentUserPrincipal.getUserId();
				channel = currentUserPrincipal.getSalesChannel() + "";
				ip = currentUserPrincipal.getIpAddress();
			}

			int ondCount = 0;
			StringBuffer ondStr = new StringBuffer();
			if (baseRQ.getOriginDestinationInformationList().size() > 0) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				ondCount = baseRQ.getOriginDestinationInformationList().size();
				for (OriginDestinationInformationTO ond : baseRQ.getOriginDestinationInformationList()) {
					if (ondStr.length() > 0) {
						ondStr.append("-");
					}
					ondStr.append(ond.getOrigin() + "/" + ond.getDestination() + "#" + sdf.format(ond.getPreferredDate()) + "#"
							+ CalendarUtil.daysUntil(ond.getDepartureDateTimeStart(), ond.getPreferredDate()) + "#"
							+ CalendarUtil.daysUntil(ond.getPreferredDate(), ond.getDepartureDateTimeEnd()));
				}
			}
			PaxCountAssembler pca = new PaxCountAssembler(baseRQ.getTravelerInfoSummary().getPassengerTypeQuantityList());

			logSearch(source.toString(), AppSysParamsUtil.getCarrierCode(), agentCode, userId, channel, ip, ondCount,
					ondStr.toString(), pca.getAdultCount(), pca.getChildCount(), pca.getInfantCount(), baseRQ
							.getAvailPreferences().getSearchSystem().toString(), pnr);
		} catch (Exception e) {
			// DO NOTHING
		}
	}

	private static void logSearch(String source, String carrier, String agentCode, String userId, String channel, String ip,
			int ondCount, String ondStr, Integer adults, Integer children, Integer infants, String searchSystem, String pnr) {
		log.debug(createLogString(source, carrier, agentCode, userId, ondCount + "", ondStr, adults + "", children + "", infants
				+ "", searchSystem, channel, ip, pnr));
	}

	private static String createLogString(String... arr) {
		StringBuffer sb = new StringBuffer();
		for (String x : arr) {
			if (sb.length() > 0) {
				sb.append(",");
			}
			sb.append(x);
		}
		return sb.toString();
	}

	public static void logAvlSearch(AnalyticSource source, BaseAvailRQ searchParams, HttpServletRequest request,
			TrackInfoDTO trackInfo) {
		logData(source, searchParams, request, null, null, trackInfo);
	}

	public static void logAvlSearch(AnalyticSource source, BaseAvailRQ baseRQ, UserPrincipal principal, TrackInfoDTO trackInfo) {
		logData(source, baseRQ, null, null, principal, trackInfo);
	}

	public static void logRequoteSearch(AnalyticSource source, BaseAvailRQ baseRQ, HttpServletRequest request, String pnr,
			TrackInfoDTO trackInfo) {
		logData(source, baseRQ, request, pnr, null, trackInfo);
	}

	public static void logRequoteSearch(AnalyticSource source, BaseAvailRQ baseRQ, UserPrincipal principal, String pnr,
			TrackInfoDTO trackInfo) {
		logData(source, baseRQ, null, pnr, principal, trackInfo);
	}

	public static void logModSearch(AnalyticSource source, BaseAvailRQ baseRQ, HttpServletRequest request, String pnr,
			TrackInfoDTO trackInfo) {
		logData(source, baseRQ, request, pnr, null, trackInfo);
	}
}
