package com.isa.thinair.webplatform.api.dto;

public class FlexiDTO {

	private String originDestination;

	private String flexiDetails;

	public String getOriginDestination() {
		return originDestination;
	}

	public void setOriginDestination(String originDestination) {
		this.originDestination = originDestination;
	}

	public String getFlexiDetails() {
		return flexiDetails;
	}

	public void setFlexiDetails(String flexiDetails) {
		this.flexiDetails = flexiDetails;
	}

}
