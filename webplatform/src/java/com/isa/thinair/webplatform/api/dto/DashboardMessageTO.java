/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:17:16
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.webplatform.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * To hold Dashboard Message
 * 
 * @author Nilindra Fernando
 */
public class DashboardMessageTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2898630208806164614L;

	private int messageId;

	private String messageType;

	private String messageTypeDesc;

	private String subject;

	private Date fromDate;

	private Date endDate;

	private String userList;

	/**
	 * @return Returns the endDate.
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            The endDate to set.
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return Returns the fromDate.
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate
	 *            The fromDate to set.
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return Returns the messageId.
	 */
	public int getMessageId() {
		return messageId;
	}

	/**
	 * @param messageId
	 *            The messageId to set.
	 */
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	/**
	 * @return Returns the messageType.
	 */
	public String getMessageType() {
		return messageType;
	}

	/**
	 * @param messageType
	 *            The messageType to set.
	 */
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	/**
	 * @return Returns the messageTypeDesc.
	 */
	public String getMessageTypeDesc() {
		return messageTypeDesc;
	}

	/**
	 * @param messageTypeDesc
	 *            The messageTypeDesc to set.
	 */
	public void setMessageTypeDesc(String messageTypeDesc) {
		this.messageTypeDesc = messageTypeDesc;
	}

	/**
	 * @return Returns the subject.
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject
	 *            The subject to set.
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return Returns the userList.
	 */
	public String getUserList() {
		return userList;
	}

	/**
	 * @param userList
	 *            The userList to set.
	 */
	public void setUserList(String userList) {
		this.userList = userList;
	}
}
