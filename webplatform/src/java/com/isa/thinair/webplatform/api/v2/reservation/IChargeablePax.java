package com.isa.thinair.webplatform.api.v2.reservation;

import java.math.BigDecimal;

import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;

public interface IChargeablePax {

	public abstract ExternalChgDTO getAncillaryChargeDTO(EXTERNAL_CHARGES externalCharge);

	public abstract BigDecimal getAncillaryCharge(EXTERNAL_CHARGES externalCharge);

	/**
	 * @return the externalCharge
	 */
	public abstract ExternalChgDTO getExternalCharge(EXTERNAL_CHARGES externalCharge);
	
	public abstract ExternalChgDTO getInfantExternalCharge(EXTERNAL_CHARGES externalCharge);	

}