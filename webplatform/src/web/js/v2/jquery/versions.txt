LCC Renamed			Actual Name						Release		URL/Comments

jquery.js			jquery-1.9.1.min.js				1.9.1		Last working "jquery-1.4.2.min.js" this file is in the CVS renammed as "jquery_last_worked_ver.js"
jquery.ui.js		jquery-ui-1.10.0.custom.min		1.10.0		Last working "jQuery UI 1.7.2.min.js" this file is in the CVS renammed as "jquery.ui_last_worked_ver.js"
jquery.jqGrid.js	jquery.jqGrid.js 1.5			1.5
jquery.pstrength.js	jquery.pstrength-min.1.2.js		1.2
jquery.combo.js		jquery.sexy-combo-2.1.0.min.js	2.1.0		http://plugins.jquery.com/project/SexyCombo
combo.css			sexy-combo.css + skins/sexy.css	2.1.0		http://plugins.jquery.com/project/SexyCombo
jquery.json.js      jquery.json.js                  2.1 