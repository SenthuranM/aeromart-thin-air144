(function($) {
    var tMin ="";
    var tMax = "";

   $.fn.fareslider = function(settings) {
     var config = {

     };
     if (settings) $.extend(config, settings);
      tMin = config.stDate;
      tMax = config.edDate;
     return this.each(function() {
     	 var el = $(this);
    	 var pID = el[0].id;
    	 var baseClass = el[0].title;
    	 var fName = el[0].title;
     	 var txt = $("#"+pID+" "+config.breaker).text();
    	 var newtxt = new Array();
    	 newtxt = txt.split(",");
     	 var htmlToAdd = '<input type="hidden" id="'+pID+$.trim(newtxt[0])+'" name="'+pID+$.trim(newtxt[0])+'" value="'+$.trim(newtxt[1])+'">'+
    	 '<input type="hidden" id="'+pID+$.trim(newtxt[2])+'" name="'+pID+$.trim(newtxt[2])+'" value="'+$.trim(newtxt[3])+'">'+
    	 '<input type="hidden" id="'+pID+$.trim(newtxt[4])+'" name="'+pID+$.trim(newtxt[4])+'" value="'+$.trim(newtxt[5])+'">'+
    	 '<input type="hidden" id="'+pID+$.trim(newtxt[6])+'" name="'+pID+$.trim(newtxt[6])+'" value="'+$.trim(newtxt[7])+'">'+
    	 '<input type="hidden" id="'+pID+$.trim(newtxt[8])+'" name="'+pID+$.trim(newtxt[8])+'" value="'+$.trim(newtxt[9])+'">'+
    	 '<input type="hidden" id="'+pID+$.trim(newtxt[10])+'" name="'+pID+$.trim(newtxt[10])+'" value="'+$.trim(newtxt[11])+'">'+
    	 '<input type="hidden" id="'+pID+$.trim(newtxt[12])+'" name="'+pID+$.trim(newtxt[12])+'" value="'+$.trim(newtxt[13])+'">'+
    	 '<input type="hidden" id="'+pID+'bClass" name="'+pID+'bClass" value="'+baseClass+'">'+
    	 '<div class="_farelabel"><table width="100%" cellspacing="0" cellpadding="0" border="0" align="center"><tr>'+
    	 '<td width="8" class="leftFare-Coner">&nbsp;</td><td class="f_midclass">'
    	 +fName+'</td>'+
    	 '<td width="8" align="right" class="rightFare-Coner">&nbsp;</td></tr></table></div>'+
    	 '<table width="100%"><tr><td style="height:20px">'+
 		'<div id="salesSlader'+pID+'" class="cl"></div><div id="departureSlider'+pID+'"class="cl"></div><div id="returnSlider'+pID+'" class="cl"></div>'+
 		'</td></tr></table>';
    	 el.html(htmlToAdd);
    	 $("#load-"+config.parent+" #fareVal"+getFareRange(newtxt[13])).append(el);
    	 buildsilders ("salesSlader"+pID,"msg"+pID,newtxt[1],newtxt[3],"Sales");
    	 buildsilders ("departureSlider"+pID,"msg"+pID,newtxt[5],newtxt[7],"Departure");
    	 buildsilders ("returnSlider"+pID,"msg"+pID,newtxt[9],newtxt[11],"Return");
  
     });
 
   };
   
   function getFareRange(_fare){
	   if (minFareVal%2 == 0){
		   var actRange = _fare%range;
	   }else{
		   var actRange = (_fare%range); 
	   }
	   var nactRange = (actRange == 0)? 0:actRange;
	   rr = parseFloat(_fare)+nactRange
	   return rr;
   };
   
   function dateVal( str1 ) {
	    var diff = Date.parse( str1 );
	    return isNaN( diff ) ? NaN : {
	    	diff : diff,
	    	ms : Math.floor( diff            % 1000 ),
	    	s  : Math.floor( diff /     1000 %   60 ),
	    	m  : Math.floor( diff /    60000 %   60 ),
	    	h  : Math.floor( diff /  3600000 %   24 ),
	    	d  : Math.floor( diff / 86400000        )
	    };
	};
   
	function valDate(str1) {
	    var d = new Date();
	    d.setTime(str1 * 86400000);
	    var str = Number(d.getMonth()+1) + "/" + Number(d.getDate()) + "/" + d.getFullYear();
	    return str;
   };
   
   
   
   	buildsilders = function (slID,lalID,st,ed,nclass){
   		$("#"+slID).slider({
			range: "min",
			//disabled:true,
			value:dateVal(ed).d,
			min: dateVal(tMin).d,
			max: dateVal(tMax).d,
			//[dateVal(st).d, dateVal(ed).d],
			slide: function(event, ui) {
   				$("#"+lalID).fadeIn("slow");
				//$("#"+lalID).text( valDate(ui.values[0]) + ' - ' + valDate(ui.values[1]));
   				$("#"+lalID).text( valDate(ui.value));
			},
			stop: function(event, ui) { 
				$("#"+lalID).fadeOut("slow");
			}
		});
		$("#"+slID).addClass(nclass);
		if (nclass != "Sales")
			$("#"+slID).css("display","none")
   };
 
 })(jQuery);
