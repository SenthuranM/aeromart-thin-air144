﻿﻿// Author: suneth abayarathna added to have the gregorian calendar with the translations for Persian 
// requested by AirArabia 
jQuery(function($){
	$.datepicker.regional['fa'] = {
		closeText: 'بستن',
		prevText: 'قبل',
		nextText: 'بعد',
		currentText: 'امروز',
		monthNames: ['ژانویه','فوریه','مارس','آوریل','می','ژوئن','ژوئیه','اوت','سپتامبر','اکتبر','نوامبر','دسامبر'],
		monthNamesShort: ['ژانویه','فوریه','مارس','آوریل','می','ژوئن','ژوئیه','اوت','سپتامبر','اکتبر','نوامبر','دسامبر'],
		dayNames: ['یکشنبه', 'دوشنبه', 'سه شنبه', 'چهارشنبه', 'پنجشنبه', 'جمعه', 'شنبه'],
		dayNamesShort: ['یک', 'دو', 'سه', 'چهار', 'پنج', 'جمعه', 'شنبه'],
		dayNamesMin: ['ی','د','س','چ','پ','ج','ش'],
		weekHeader: 'ه',
		dateFormat: 'dd/mm/yy',
		firstDay: 6,
		isRTL: true,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['fa']);
});
