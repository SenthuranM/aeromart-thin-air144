$(function () {

	$.fn.pageInitialize();
	
});

$.fn.addDatatoCombo = function (id){
	
	$.each(jsCntryCity, function(key, value) { 
		$(id).append('<option value="'+ value.stationCode +'">' + value.stationCode +' - '+ value.stationDesc +'</option>').val( value.stationCode);
	});
	
	//$(id).append('<option value="All">All</option>').val('All');
	$(id).append('<option value="" selected="selected"></option>').val('');
	$(id).sexyCombo({
		triggerSelected: true,
		hideListCallback: function(){
			if (id == "#origin"){
				origin = $(id).val();
				$("#destination").next().focus();
			}else if (id == "#destination"){
				desitination = $(id).val();
				$("#via1").next().focus();
			}else if (id == "#via1"){
				via1 = $(id).val();
				$("#via2").next().focus();
			}else if (id == "#via2"){
				via2 = $(id).val();
				$("#via3").next().focus();
			}else if (id == "#via3"){
				via3 = $(id).val();
				$("#via4").next().focus();
			}else{
				via4 = $(id).val();
			}
			fillRoots();
		}
	});
	$("#origin").next().focus();
};


$.fn.fillCombo = function(){
	if (origin == "" || desitination == "")
		alert("Please Select the Origin & Destination");
	
	return false;
};

function checkBlank(txt){
	if (txt == "")
		return "";
	else
		return "-";
}

function fillRoots(){
	$("#ondlistms2side__sx").empty();
	$("#ondlistms2side__dx").empty();
	if (origin != ""){
		//alert(origin +"--"+ desitination+"--"+ via1+"--"+ via2+"--"+ via3+"--"+ via4)
		vias =  checkBlank(via1) + via1 + checkBlank(via2) + via2+ checkBlank(via3) + via3 + checkBlank(via4) + via4;
		
		$("#ondlistms2side__dx").append('<option value="'+ origin + vias +'-' + desitination + '">'
				+ origin + vias +'-' + desitination + '</option>').val(''+ origin +  vias +'-' + desitination + '');
		/*
		if (desitination != "All"){
			$("#ondlistms2side__sx").attr("disabled","disabled");
			$("#ondlistms2side__dx").attr("disabled","disabled");
		}else if (OND[1] == "All" && OND[2] == undefined ){
			$("#ondlistms2side__sx").attr("disabled","");
			$("#ondlistms2side__dx").attr("disabled","");
			dataFiling("dRoot");
		}else if (OND[1] != "All" && OND[2] != undefined ){
			$("#ondlistms2side__sx").attr("disabled","");
			$("#ondlistms2side__dx").attr("disabled","");
			//alert("show hab roots");
		}else if (OND[1] == "All" && OND[2] != undefined ){
			$("#ondlistms2side__sx").attr("disabled","");
			$("#ondlistms2side__dx").attr("disabled","");
			dataFiling("hRoot");
		}*/
	}
}

function dataFiling(wat){
	$.each(jsRoots, function(key, value){
		if(ond[0] == value.oriStationCode){
			if(wat == "dRoot")
				root = value.dRoots.split(",");
			else
				root = value.hRoots.split(",");
		}
	});
	splitArray(root);
}

function splitArray(arr){
	for (var i=0; i < arr.length; i++){
		$("#ondlistms2side__sx").append('<option value="'+ arr[i] + '">'
			+ arr[i] + '</option>').val(''+ arr[i] + '');
	}
}

function allONDS(){
	$("#ondlistms2side__sx").empty();
	$("#ondlistms2side__dx").empty();
	document.getElementsByName("__sexyCombo")[0].disabled = false;
	document.getElementsByName("__sexyCombo")[1].disabled = false;
	$("#ondlistms2side__sx").attr("disabled","disabled");
	$("#ondlistms2side__dx").attr("disabled","disabled");
}

function enabledAll(){
	
	document.getElementsByName("__sexyCombo")[0].disabled = false;
	document.getElementsByName("__sexyCombo")[1].disabled = false;

}

$.fn.search = function (){
	jsTree[0].roots = "";
	jsTree[0].frs = "";
	jsTree[0].bcs= "";
	jsTree[0].roots = $("#ondlistms2side__dx").children();
	jsTree[0].frs = $("#fareRulems2side__dx").children();
	jsTree[0].bcs = $("#bookingClassms2side__dx").children();
	$.fn.buildTree();
};

$.fn.buildTree = function () {
	$("#tree").empty();
	$("#tree").append(buildtreeFirstNodes(jsTree[0].roots));
	$("#tree").treeview({
		 animated: "fast",
		 collapsed: true,
		 unique: true
		 
	 });
	//$("#tree li:first-child").removeClass("expandable").addClass("collapsable");

	 
};

function buildtreeFirstNodes(data){
	var str="";
	if (data != null || data != undefined){
		$.each(data, function (key, value){
			if (jsTree[0].bcs.length < 1)
			str += "<li><a href='#' id='"+value.text+"' class='showFares'>"+value.text+"</a></li>";
			else
			str += "<li><a href='#' id='"+value.text+"' class='showFares'>"+value.text+"</a><ul>" +
				buildtreeTriNodes(jsTree[0].bcs) + "</ul></li>";
		});
	}
	return str;
};

function buildtreeSecNodes(data){
	var str = "";
		if (data != null || data != undefined){
			$.each(data, function (key, value){
				if (jsTree[0].bcs.length < 1)
					str += "<li><a href='#' id='"+value.text+"' class=''>"+value.text+"</a></li>";
					else
					str += "<li><a href='#' id='"+value.text+"' class=''>"+value.text+"</a><ul>" +
						buildtreeTriNodes(jsTree[0].bcs) + "</ul></li>";
			});
		}
	return str;
};

function buildtreeTriNodes(data){
	var str = "";
	if (data != null || data != undefined){
		$.each(data, function (key, value){
			str += "<li><a href='#' id='"+value.text+"' class='showFares'>"+value.text+"</a></li>";
		});
	}
	return str;
};

$.fn.pageInitialize = function (){
	
	$.fn.addDatatoCombo("#origin");
	$.fn.addDatatoCombo("#destination");
	$.fn.addDatatoCombo("#via1");
	$.fn.addDatatoCombo("#via2");
	$.fn.addDatatoCombo("#via3");
	$.fn.addDatatoCombo("#via4");
	
	
	$('#ondlist').multiselect2side({selectedPosition: 'right', moveOptions: false});
	$('#fareRule').multiselect2side({selectedPosition: 'right', moveOptions: false});
	$('#bookingClass').multiselect2side({selectedPosition: 'right', moveOptions: false});
	
	//Sales
	$('#fromSalesDate').datepicker({
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
	$('#toSalesDate').datepicker({
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
	
	//Departure
	$('#fromDepartureDate').datepicker({
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
	$('#toDepartureDate').datepicker({
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
	
	//Arrival
	$('#fromReturnDate').datepicker({
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
	$('#toReturnDate').datepicker({
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
//Radio button Functions
	$("'input:radio[name=ondType]'").click(function() {
	    if (true == document.getElementById("ondDirect").checked){
	    	enabledAll();
	    	fillRoots(ond, "Direct");
	    	ondTypes = "Direct";
	    }else if (true == document.getElementById("ondAll").checked){
	    	allONDS();
	    	ondTypes = "All";
	    }else{
	    	enabledAll();
	    	fillRoots(ond, "Hub");
	    	ondTypes = "Hub";
	    }
	});
	
	$("#tabs").tabs();
	$(".tabs-bottom .ui-tabs-nav, .tabs-bottom .ui-tabs-nav > *") 
		.removeClass("ui-corner-all ui-corner-top") 
		.addClass("ui-corner-bottom");
	//properties

	
	//banchmark
	$(".banchmarkTriger").click(function (){
		var display = $("#tableBanchmark").css("display");
		if (display == "none"){
			$("#tableBanchmark").slideDown();
			$(this).attr("src","../../images/upArrow_no_cache.png");
		}else{
			$("#tableBanchmark").slideUp();
			$(this).attr("src","../../images/downArrow_no_cache.png");
		}

	});
	
	function hideLoading(){
		$("#mask").hide();
	}

	function showLoading(){
		//alert("11")
		$("#mask").show();
		setTimeout("$('#mask').hide();",2000);
	}
	
	$("#bmSearch").click(function (){
		showLoading();
		$(".bDetails").css("display","block");
		var disp = $("#tabs").css("display");
		if (disp == "none"){
			$(".bDetails").css("display","block");
			$(".bDetails").animate(
			{width: 528},
			{duration:500,
				complete: function() {
					$("#tabs").css("display","block");
					$("._save").css("display","block");
				}
			});
		}
	});
	$("#record").click(function(){
		$(".quickTableClass").slideUp();
		$(".addProperty").slideUp();
		$("#proAutoDetails").slideDown();
		if ($(this).hasClass("recordbtn-on")){
			$(this).removeClass("recordbtn-on");
			alert("Recording Stoped");
		}else{
			$(this).addClass("recordbtn-on");
			alert("Please set the Auto Action options to start Recording");
		}
	})
	
	$('#fromRecodrdDate').datepicker({
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
	
	$('#toRecodrdDate').datepicker({
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
	
	$('#fromDepDate').datepicker({
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
	
	$('#toDepDate').datepicker({
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true
	});
	
	
	
	$("#qAction").click(function(){
		var t = $(".quickTableClass").css("display")
		if (t == "block"){
			$(".quickTableClass").slideUp();
		}else{
			$(".quickTableClass").slideDown();
		}
	});
	
	$("#pro").click(function(){
		var t = $(".addProperty").css("display")
		if (t == "block"){
			$(".addProperty").slideUp();
		}else{
			$(".addProperty").slideDown();
		}
	});
	
	$("#proAuto").click(function(){
		var t = $(".addAuto").css("display")
		if (t == "block"){
			$(".addAuto").slideUp();
		}else{
			$(".addAuto").slideDown();
		}
	});
	
	 
	var boxH = $(".ms2side__div select").css("height");
	
	$("#btnSearch").click( function(){
		var txt = $(this).text();
			if (origin == "" || desitination == "")
				alert("Please Select the Origin & Destination");
			else{
				/*$(".ms2side__options").animate({paddingTop: 0},500);
				$(".ms2side__div select").animate({height: 70},500, function(){
					$(".mask").css("height",$(".searchCri").css("height"));
					$(".mask").css("width",$(".searchCri").css("width"));
					$(".mask").css("display","block");
				});*/
				$(".searchCri").slideUp();
				$(".search-res").slideDown();
				$(".searchTriger").attr("src","../../images/upArrow_no_cache.png");
				$.fn.search();
			}
	});
	
	$(".searchTriger").click( function(){
		var dip = $(".searchCri").css("display");
		if (dip == "block"){
			$(".searchCri").slideUp();
			$(".search-res").slideDown();
			$(".searchTriger").attr("src","../../images/downArrow_no_cache.png");
		}else{
			$(".searchCri").slideDown();
			$(".search-res").slideUp();
			$(".searchTriger").attr("src","../../images/upArrow_no_cache.png");
		}
	});
	
	/*
	var $tabs = $('#tabs1').tabs({
		tabTemplate: '<li><a href="#{href}">#{label}</a> <span class="ui-icon ui-icon-close">Remove Tab</span></li>',
		add: function(event, ui) {
			//var tab_content = $tab_content_input.val() || 'Tab '+tab_counter+' content.';
			//$(ui.panel).append('<p>'+tab_content+'</p>');
			$.ajax({
				url: uri,
				dataType:"html",
				context: document.body,
				success: function(result){
					$(ui.panel).html(result);
		      	},
		      	error: function (error){
		      		alert(error);
		      	},
		      	complete:function (){
		      		$( "#tabs1" ).tabs( "option", "active", tab_counter-1 );
		      	}
			});
		}
	});
	
	
	$('#tabs1 span.ui-icon-close').live('click', function() {
		var takeing = $(this).prev("a").attr("href");
		if ($(takeing+" .fares").hasClass("selected"))
			alert("Please un-select the fare(s) before close the tab");
		else{
		var index = $('li',$tabs).index($(this).parent());
		$tabs.tabs('remove', index);
		tab_counter--;
		var tk = takeing.split("#load-")
		$("#"+tk[1]).attr("disabled","");
		}
		if ($("#tabs1").tabs("length") == 0)
			$("#tabs1 ul").css("border-bottom","0px");
		
	});*/
	
		
	$('.button').live('click', function(){
		addTab(tab_counter);
	});
	

	$('#svcheck').click(function (){
		if ($(this).attr("checked")){
			$('#fromSalesDate').attr("disabled","");
			$('#toSalesDate').attr("disabled","");
		}else{
			$('#fromSalesDate').attr("disabled","disabled");
			$('#toSalesDate').attr("disabled","disabled");
		}
	});
	$('#dvcheck').click(function (){
		if ($(this).attr("checked")){
			$('#fromDepartureDate').attr("disabled","");
			$('#toDepartureDate').attr("disabled","");
		}else{
			$('#fromDepartureDate').attr("disabled","disabled");
			$('#toDepartureDate').attr("disabled","disabled");
		}
	});
	$('#rvcheck').click(function (){
		if ($(this).attr("checked")){
			$('#fromReturnDate').attr("disabled","");
			$('#toReturnDate').attr("disabled","");
		}else{
			$('#fromReturnDate').attr("disabled","disabled");
			$('#toReturnDate').attr("disabled","disabled");
		}
	});
	
	
	/*$('.showFares').live('click', function(){
		if ($(this).attr("disabled"))
			return false;
		else
			addTab($(this).text());
		
		$(this).attr("disabled","disabled");
	});*/

	
};

