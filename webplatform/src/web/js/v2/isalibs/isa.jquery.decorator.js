$.fn.decoratePanel = function(title) {
	$(this).wrap('<div class="ui-widget ui-widget-content ui-corner-all" style="margin-top:5px;"></div>');
	$(this).parent().prepend($('<div class="ui-dialog-titlebar ui-widget-header ui-corner-all panelHeader ui-helper-clearfix" style="height:15px;padding-top:2px;padding-left:5px;"></div>').append(title));
}

$.fn.decoratePanelWithAlign = function(title) {
	$(this).wrap('<div class="ui-widget ui-widget-content ui-corner-all " style="margin-top:5px;"></div>');
	$(this).parent().prepend('<div class="ui-dialog-titlebar ui-widget-header ui-corner-all panelHeader ui-helper-clearfix" style="height:15px;padding-top:2px;padding-left:5px;text-align:left;">' + title + '</div>');
}

$.fn.decoratePanelWithoutTitle = function() {
	$(this).wrap('<div class="ui-widget ui-widget-content ui-corner-all "></div>');
}

$.fn.decorateButton = function() {
	$(this).addClass('ui-state-default ui-corner-all');
	$(this).hover(
				function() { $(this).addClass('ui-state-hover'); }, 
				function() { $(this).removeClass('ui-state-hover'); }
			);
}

$.fn.decoButton = function(className){
	return this.each(function() {
		var text = $(this).text();
		var tHtml = '<table cellpadding="0" cellspacing="0" border="0" width="auto" class="'+className+'"><tbody><tr><td class="buttonspan">&nbsp;</td><td class="buttonMiddle" align="right">'+text+'</td></tr></tbody></table>'
		return $(this).attr("title", text).html(tHtml);
	});
}

$.fn.fillDropDown = function(options) {
	try {
		var optsTxt = [];
		var c = 0;
		if(options.firstEmpty) {
			//$(this).append('<option value="">&nbsp;</option');
			optsTxt[c++] = '<option value="">&nbsp;</option>';
		}
		
		function loopArray(key,arr){
			var k = (key==null)?"":key+":";
			for(var i=0; i< arr.length; i++) {
				//$(this).append('<option value="' + options.dataArray[i][options.keyIndex] + '">' + options.dataArray[i][options.valueIndex] + '</option>');
				optsTxt[c++] = '<option value="';
				optsTxt[c++] = k+arr[i][options.keyIndex];
				optsTxt[c++] = '">';
				optsTxt[c++] = arr[i][options.valueIndex];
				optsTxt[c++] = '</option>';
					
			}
		}

		if(options.dataArray != null) {
			if (options.grouped){
				for(var x=0; x< options.dataArray.length; x++) {
					for (key in options.dataArray[x]) {
						optsTxt[c++] = '<option value="';
						optsTxt[c++] = key+":"+key;
						optsTxt[c++] = '">';
						optsTxt[c++] = key;
						optsTxt[c++] = '</option>';
						if (options.dataArray[x].hasOwnProperty(key)){
							var arra = options.dataArray[x][key];
							loopArray(key, arra);
						}
					} 
				}
			}else{
				loopArray(null, options.dataArray);
			}
		}
		if(optsTxt.length >0){
			$(this).append(optsTxt.join(''));
		}
	} catch (ex) {
		alert("Error filling dropdown box");
	}
}

$.fn.isFieldEmpty = function(){
    return (this.val() == null || $.trim(this.val()) == "");
}

$.fn.disableButton = function() {
	$(this).removeClass('ui-state-hover');
	$(this).addClass('ui-state-disabled');
	$(this).attr('disabled', true);
}

$.fn.enableButton = function() {
	$(this).removeClass('ui-state-disabled');
	$(this).attr('disabled', false);
}

$.fn.clearForm = function() {
  // iterate each matching form
  return this.each(function() {
	 // iterate the elements within the form
	 $(':input', this).each(function() {
	   var type = this.type, tag = this.tagName.toLowerCase();
	   if (type == 'text' || type == 'password' || tag == 'textarea' || type == 'hidden')
	  this.value = '';
	   else if (type == 'checkbox' || type == 'radio')
	  this.checked = false;
	   else if (tag == 'select')
	  this.selectedIndex = -1;
	 });
  });
};

$.fn.disableForm = function() {
  // iterate each matching form
  return this.each(function() {
	 // iterate the elements within the form
	 $(':input', this).each(function() {
	   var type = this.type, tag = this.tagName.toLowerCase();
	   if (type == 'text' || type == 'password' || tag == 'textarea' || type == 'checkbox' || type == 'radio' || tag == 'select')
	  		$(this).attr('disabled', true);
	 });
  });
};

$.fn.enableForm = function() {
  // iterate each matching form
  return this.each(function() {
	 // iterate the elements within the form
	 $(':input', this).each(function() {
	   var type = this.type, tag = this.tagName.toLowerCase();
	   if (type == 'text' || type == 'password' || tag == 'textarea' || type == 'checkbox' || type == 'radio' || tag == 'select') 	
			$(this).removeAttr('disabled');
	 });
  });
};
/**
 * Enables the items sent by the selector. 
 */
$.fn.enableFormSelective = function() {
    return this.each(function() {
 	   var type = this.type, tag = this.tagName.toLowerCase();
	   if (type == 'text' || type == 'password' || tag == 'textarea' || type == 'checkbox' || type == 'radio' || tag == 'select') 	
			$(this).removeAttr('disabled');          
    });
};

$.fn.enableForm = function() {
	  // iterate each matching form
	  return this.each(function() {
		 // iterate the elements within the form
		 $(':input', this).each(function() {
		   var type = this.type, tag = this.tagName.toLowerCase();
		   if (type == 'text' || type == 'password' || tag == 'textarea' || type == 'checkbox' || type == 'radio' || tag == 'select') 	
				$(this).removeAttr('disabled');
		 });
	  });
};

