$.fn.iterateTemplete = function(params) {

	var templeteName = params.templeteName;
	var clonedTemplateId;
	var elementIdAppend;


	var dtoRecord ;
	//iterate the dto list
	for(var i=0; i < params.data.length ; i++) {

		clonedTemplateId = templeteName + "_" + i;
		//clone the template row
		var clone = $( "#" + templeteName + "").clone();
		//set the id for the clone (to avoid to have the same id)
		clone.attr("id", clonedTemplateId );
		//append clone to the parent of the template
		clone.appendTo($("#" + templeteName).parent());  

		//get i th dto of the dto list
		dtoRecord = params.data[i];
		//appender for the element names (eg student[0].name)
		elementIdAppend = params.dtoName + "[" + i + "].";

		//setting the value and id of labels
		$("#"+clonedTemplateId).find("label").each(function( intIndex ){
			$(this).text(dtoRecord[this.id]);
			$(this).attr("id",  elementIdAppend +  this.id);
		});
		
		//setting the value and id of spans
		$("#"+clonedTemplateId).find("span").each(function( intIndex ){
			if(dtoRecord[this.id]!=null){
				$(this).html(dtoRecord[this.id]);
				$(this).attr("id",  elementIdAppend +  this.id);
			}
		});
		
		//setting the value, id and name of selects
		$("#"+clonedTemplateId).find("select").each(function( intIndex ){
			$(this).val(dtoRecord[this.id]);
			$(this).attr("id",  elementIdAppend +  this.id);
			$(this).attr("name",  this.id);
		});
		//setting the value, id and name of inputs
		$("#"+clonedTemplateId).find("input").each(function( intIndex ){
			$(this).val(dtoRecord[this.id]);
			$(this).attr("id",  elementIdAppend +  this.id);
			$(this).attr("name",  this.id);
		});
		
		//setting the value and id of labels
		$("#"+clonedTemplateId).find("img").each(function( intIndex ){			
			$(this).attr("src", dtoRecord[this.id]);
			$(this).attr("id",  elementIdAppend +  this.id);
		});
		
		//setting the value, id and name of textarea
		$("#"+clonedTemplateId).find("textarea").each(function( intIndex ){
			$(this).val(dtoRecord[this.id]);
			$(this).attr("id",  elementIdAppend +  this.id);
			//$(this).attr("name",  this.id);
		});
		
		$("#" + clonedTemplateId).show();		
	}

	$( "#" + templeteName).hide();
}