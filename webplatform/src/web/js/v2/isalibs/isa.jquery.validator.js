/**
 * This validator validates froms. several conventions used in the validation 
 * process.
 * 
 * Default behavior - Uses Predefined Errors. Uses Element Id as a display name 
 * 		eg - <input type = 'text' id = 'inputid1' class = 'v_mandatory'/>
 * 		Error is - "inputid1 cannot be blank."
 * 
 * Form Elements Must have an ID
 * Available valication classs are 
 * 
 * v_mandatory
 * v_allowSpecials
 * v_numericOnly
 * v_stringOnly
 * v_noDecimals
 * v_URL
 * v_URI
 * v_Email
 * v_ddMMYYYY
 * v_HHmm
 * v_actualLength
 * v_range
 * v_maxVal
 * v_maxDecimals
 * v_NumberAndDecimalRange_decimals
 * v_NumberAndDecimalRange_num
 * v_ddMMYYYYPreventPast
 * 
 * Here are the conventions to follow and how to override default behavior
 * 
 * 1) special class names used for the validation identification.
 * 
 * 		eg v_mandatory - to specify a form field mandatory.
 * 
 * 2) Input field Names And Error Displays
 * 
 * 		{0} th parameter is used for field name in the message
 * 		
 * 
 * @author Navod Ediriweera, Malaka Ekanayake, Baladewa Welathanthri
 * @since September 03, 2009
 * @lastModified October 14 2009
 * @version 1.0 - Tested With jQuery Form Plugin version: 2.28 (http://malsup.com/jquery/form/) and Jquery v1.3.2
 */

(function($){
	
	/**
	 * parameters will have these attributes
	 * parameters.screenId
	 * parameters.proceedAtError
	 */
			
			
			
	
	$.fn.isaValidateDiv = function (parameters) {
			
		var commonErrors = parameters.commonErrors;
		var proceed =  parameters.proceedAtError;		
		var errorMessages = new Array();
		var errorCount = 0;
		// Tested With jQuery Form Plugin version: 2.28
		var elements = this.find("input");
							
		for(var i=0;i<elements.length;i++){
			
			if(!proceed && errorMessages.length > 0) break;//exiting loop if errors found
			
			if(elements[i].id==""){
				continue;
			}
			
			var elementID = elements[i].id;
			inputJQ = $("#"+elementID);
			var classNames = inputJQ.attr('class');
			var screenActual = parseDisplayFieldName( {elementID:elementID , classNames:classNames });
					
			if(classNames==null || classNames =="" || classNames.length==0){
				//skiping if no classes are attached
				continue;
			}
			//mandatory validation
			if(inputJQ.hasClass("v_mandatory")){
				if(!inputJQ.val()){
					errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_mandatory'],screenActual);	
					errorCount++;
					if(!proceed)break;
				}
			}
			//skipping optional value checking if the value is empty
			if( (inputJQ.val()== null || inputJQ.val()=="") && !inputJQ.hasClass("v_mandatory")) {
				continue;//goes to next in the for loop
			}
			//validating invalid characters if not spcificall stated not to
			if(!inputJQ.hasClass("v_allowSpecials")){
				var strChkEmpty = $.fn.isaValidator.findChar(inputJQ.val());
				if(strChkEmpty != "0"){
					errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_allowSpecials'],screenActual);				
					errorCount++;
					if(!proceed)break;
				}		
			}
			var classArr = classNames.split(" ");	
			$.each(classArr,function(){
				var splited = this.split(":");
				//Common Validations
				if(splited.length==1){				
					if(splited[0]=="v_numericOnly"){
						if( isNaN(inputJQ.val()) ){
							errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_numericOnly'],screenActual);	
							errorCount++;
							if(!proceed) return false;	
						}
					}
					if(splited[0]=="v_stringOnly"){
						var inputValue = inputJQ.val();
						if( !$.fn.isaValidator.checkStringOnly(inputValue) ){
							errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_stringOnly'],screenActual);	
							errorCount++;
							if(!proceed) return false;	
						}
					}
					if(splited[0]=="v_noDecimals"){
						var inputValue = inputJQ.val().split(".");
						if( inputValue.length>1 ){
							errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_noDecimals'],screenActual);	
							errorCount++;
							if(!proceed) return false;	
						}
					}
					if(splited[0]=="v_URL"){
						var inputValue = inputJQ.val();
						if( !$.fn.isaValidator.validateURL(inputValue) ){
							errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_URL'],screenActual);	
							errorCount++;
							if(!proceed) return false;	
						}
					}
					if(splited[0]=="v_URI"){		
						var inputValue = inputJQ.val();
						if( !$.fn.isaValidator.validateURI(inputValue) ){
							errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_URI'],screenActual);	
							errorCount++;
							if(!proceed) return false;	
						}
					}
					if(splited[0]=="v_Email"){
						var inputValue = inputJQ.val();
						if( !$.fn.isaValidator.checkEmail(inputValue) ){
							errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_Email'],screenActual);	
							errorCount++;
							if(!proceed) return false;		
						}
					}
					//date format validation for dd/mm/yyyy
					if(splited[0]=="v_ddMMYYYY"){						
						var parts = inputJQ.val().split("/");
						if(parts.length != 3){
							errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_ddMMYYYY'],screenActual);	
							errorCount++;
							if(!proceed) return false;	
						}
						//fix later for different months //TODO: write reg exp for validating dates in month
						if(parts[0].length != 2 || Number(parts[0]) > 31 
								|| parts[1].length != 2 || Number(parts[1]) >12
								|| parts[2].length != 4) {
							errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_ddMMYYYY'],screenActual);	
							errorCount++;
							if(!proceed) return false;	
						}				
					}
					
					//Validations for the date format for dd/mm/yyyy AND prevent past date less than current date.
					if(splited[0]=="v_ddMMYYYYPreventPast"){						
						var parts = inputJQ.val().split("/");
						if(parts.length != 3){
							errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_ddMMYYYY'],screenActual);	
							errorCount++;
							if(!proceed) return false;	
						}
						//fix later for different months //TODO: write reg exp for validating dates in month
						if(parts[0].length != 2 || Number(parts[0]) > 31 
								|| parts[1].length != 2 || Number(parts[1]) >12
								|| parts[2].length != 4) {
							errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_ddMMYYYY'],screenActual);	
							errorCount++;
							if(!proceed) return false;	
						}
						var dd = new Date();
						var today=dd.getFullYear() + "/" + (dd.getMonth() + 1)+ "/" + dd.getDate();
						var enterdDate= Number(parts[2]) +"/"+Number(parts[1]) + "/" +Number(parts[0]);
						
						if( (new Date(enterdDate)) < (new Date(today))) {
							errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_ddMMYYYYPreventPast'],screenActual);	
							errorCount++;
							if(!proceed) return false;	
						}
						
					}
					
						
					
					//date format validation for dd/MM/yyyy HH:mm
					if(splited[0]=="v_ddMMYYYYHHmm"){						
						var dtArr = inputJQ.val().split(" ");
						if(dtArr.length != 2){
							errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_ddMMYYYYHHmm'],screenActual);	
							errorCount++;
							if(!proceed) return false;	
						}						
						
						if (!$.fn.isaValidator.validateDDMMYYYY(dtArr[0])) {							
							errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_ddMMYYYYHHmm'],screenActual);	
							errorCount++;
							if(!proceed) return false;	
						}
						
						if (!$.fn.isaValidator.validateHHmm(dtArr[1])) {							
							errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_ddMMYYYYHHmm'],screenActual);	
							errorCount++;
							if(!proceed) return false;	
						}			
								
					}
					
							
					//date format validation for dd/mm/yyyy
					if(splited[0]=="v_HHmm"){						
						var parts = inputJQ.val().split(":");
						if(parts.length != 2){
							errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_HHmm'],screenActual);	
							errorCount++;
							if(!proceed) return false;	
						}	
						if(parts[0] == null || parts[0].length ==0 ||parts[1] ==null || parts[1].length ==0 ){
							errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_HHmm'],screenActual);
							errorCount++;
							if(!proceed) return false;	
						}val
						if( parts[0]>23){
							errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_maxVal'],screenActual,parts[0]);
							errorCount++;
							if(!proceed) return false;	
						}	
						if(parts[1]>59){
							errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_maxVal'],screenActual,parts[1]);
							errorCount++;
							if(!proceed) return false;	
						}
					}
				}
				//Number Related Validations
				if(splited.length > 1){
					var inputValue = inputJQ.val();
					try {
						var actualNumb_1 = Number(splited[1]);
						var actualNumb_2 = Number(splited[2]);
					}catch(e){
						//ignoring string --> number type casting errors
					}				
					if(splited[0]=="v_actualLength"){
						if( inputValue.length != actualNumb_1 ){
							errorMessages[errorCount] = 
								isaParseValidationMessage(commonErrors['v_actualLength'],screenActual,actualNumb_1);	
							errorCount++;
							if(!proceed) return false;	
						}
					}
					if(splited[0]=="v_NumberAndDecimalRange_num"){
						if( inputValue.length > actualNumb_1 ){
							errorMessages[errorCount] = 
								isaParseValidationMessage(commonErrors['v_NumberAndDecimalRange_num'],screenActual,actualNumb_1);	
							errorCount++;
							if(!proceed) return false;	
						}
					}
					if(splited[0]=="v_range"){//range validation					
						if( inputValue < actualNumb_1 || inputValue > actualNumb_2 ){
							errorMessages[errorCount] = 
								isaParseValidationMessage(commonErrors['v_range'],screenActual,actualNumb_1,actualNumb_2);	
							errorCount++;
							if(!proceed) return false;
						}					
					}
					if(splited[0]=="v_maxVal"){//Maximum range validation					
						if( inputValue > actualNumb_1){
							errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_maxVal'],screenActual,actualNumb_1);
							errorCount++;
							if(!proceed) return false;
						}
					}	
					if(splited[0]=="v_maxDecimals"){//Max Number of Decimal Points (0---n)
						var decimalsArr = inputValue.split(".");
						if(decimalsArr != null && decimalsArr.length == 2 && decimalsArr[1].length > actualNumb_1) {					
							errorMessages[errorCount] = 
								isaParseValidationMessage(commonErrors['v_maxDecimals'],screenActual,actualNumb_1);
							errorCount++;
							if(!proceed) return false;
						}
					}
					if(splited[0]=="v_NumberAndDecimalRange"){
						//Number of whole numbers and number of decimal points
						var decimalsArr = inputValue.split(".");
						if(decimalsArr != null) {	
							if(decimalsArr.length > 0 && decimalsArr[0].length > actualNumb_1){
								errorMessages[errorCount] = 
									isaParseValidationMessage(commonErrors['v_NumberAndDecimalRange'],screenActual,actualNumb_1);	
								errorCount++;
								if(!proceed) return false;
							}
							if(decimalsArr.length > 1 && decimalsArr[1].length > actualNumb_2){							
								errorMessages[errorCount] = 
									isaParseValidationMessage(commonErrors['v_NumberAndDecimalRange_decimals'],screenActual,actualNumb_1);	
								errorCount++;
								if(!proceed) return false;
							}						
						}
					}
				}			
			})		
		}
		
		var elements = this.find("select");
		
		for(var i=0;i<elements.length;i++){
			
			if(!proceed && errorMessages.length > 0) break;//exiting loop if errors found
			
			if(elements[i].id==""){
				continue;
			}
			
			var elementID = elements[i].id;
			inputJQ = $("#"+elementID);
			var classNames = inputJQ.attr('class');
			var screenActual = parseDisplayFieldName( {elementID:elementID , classNames:classNames });
					
			if(classNames==null || classNames =="" || classNames.length==0){
				//skiping if no classes are attached
				continue;
			}
			//mandatory validation
			if(inputJQ.hasClass("v_mandatory")){
				
				var classArr = classNames.split(" ");	
				$.each(classArr,function(){
					var splited = this.split(":");
					//Common Validations
					var defaultValues = {
						EMPTY : "",
						MINUS : "-1",
						SPACE : " "
					};
					
					if(splited.length==2){	
						var inputValue = inputJQ.val();
						var label = splited[0];
						var defualtValue = splited[1];
						
						if(label=="v_defualt"){
							if( inputJQ.val() == defaultValues[defualtValue] ){
								errorMessages[errorCount] = isaParseValidationMessage(commonErrors['v_mandatory'],screenActual);	
								errorCount++;
								if(!proceed) {
									return false;	
								}
							}
						}
					}
				});
			}
		}
		
		if(!proceed){//focusing.		
			doFocusOnErrorField(inputJQ);
			if(errorCount > 0) {
				DCS_Message.display("e", errorMessages[errorCount-1]);	
			}
		}
		
		return errorMessages;
	};
	
	function validateTime (){
		
	}
		
	function doFocusOnErrorField(inputJQ){
		if(!inputJQ.attr('disabled') && inputJQ.attr('type') != 'hidden'){	
			try {
				inputJQ.focus();
			}catch(e){}//do nothing if error is found
		}	
	}
	
	function isaParseValidationMessage (strMessage,screenActual) {		
		strMessage = strMessage.replace("{" + 0 + "}", screenActual);	
		
		if (arguments.length > 2) {
			for ( var i = 0; i < arguments.length-1; i++) {
				strMessage = strMessage.replace("{" + Number(i+1) + "}", arguments[i + 2]);
			}
		}
		return strMessage;
	};
	
	function parseDisplayFieldName (params) {
		
		var elementID = params.elementID;
		var classNames = params.classNames;
		var elementName=null;
		
		var classArr = classNames.split(" ");
		$.each(classArr,function() {
			var splited = this.split(":");
				if(splited[0]=="v_label") {
					elementName = splited[1];
					return false;
				}
		});
		
		if(elementName) {
			return $("#"+elementName).text();
		}
		
		return elementID;
	};
	
	$.fn.isaValidator = {
			
		validateIsPositiveInt: function(n){
				return RegExp("^[+]?[0-9]+$").test( n );
			},
		validateDDMMYYYY : function (strDate){
			var splitedDate = strDate.split("/");
			if(splitedDate.length!=3){
				return false;
			}
			if(splitedDate[0].length>2) {
				return false;
			}
			if(splitedDate[1].length>2) {
				return false;
			}
			if(splitedDate[2].length>4) {
				return false;
			}
			return true;
		}
		,validateHHmm : function (strTime){
			var splitedTime = strTime.split(":");
			if(splitedTime.length!=2){
				return false;
			}
			if(splitedTime[0].length>2) {
				return false;
			}
			if(splitedTime[1].length>2) {
				return false;
			}			
			return true;
		}
		,validateURL : function (strURL) {
			return strURL.match(/^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/);
		}
		,validateURI : function (strURI){
			//var regexUri = /[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?$/;
			//return regexUri.test(strURI);
			return true;
		}		
		,checkEmail : function checkEmail(s){return RegExp( "^[a-zA-Z0-9-_.]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,7}$" ).test(s);}

		,checkStringOnly : function checkStringOnly(s){
			if(s.match("[^A-Za-z\\s]+") != null){
				return s.match("[^A-Za-z\\s]+").length == 0;
			} else {
				return true;
			}
		}

		,checkInvalidChar : function (strValue){
			var strChkEmpty = this.findChar(strValue)
			if (strChkEmpty != "0"){
				return false;
			}else{
				return true;
			}
		}
		
		,findChar : function (StringIn){
			if(StringIn == null || StringIn.length ==0){
				return "0";
			}
			/**
			// ------------- Check the standards characters			  
			* @Author			: Rilwan A. Latiff
			* @Version		: 1.0
			* @Last Modified	: 1st June 2005				
				\b 	Backspace						\f 	Form feed
				\n 	New line						\r 	Carriage return
				\t 	tab								\" 	quotation mark
				\'  MS Office single quote 6		\'  Ms Office singel Quote 9
				\\  Back Slash
			****************************/
		//	var CharInArray = new Array("'","<",">","^",'"',"~","-");
			var CharInArray = new Array("<", ">", "^", "~","-",'+','=','#','%','&','$','*','!','{','}','[',']',';','?','|');
			var CharOutArray=new Array();
			for (var i=0;i<StringIn.length;i++){
				switch (StringIn.charCodeAt(i)){
					case 92 :
						CharOutArray[0]="\\ "
						CharOutArray[1]=eval(i+1);
						return (CharOutArray)
						break;
					case 8216 :
					case 8217 :
						CharOutArray[0]="' "
						CharOutArray[1]=eval(i+1);
						return (CharOutArray)
						break;
					default :
						for (var j=0;j<CharInArray.length;j++){
							if (StringIn.charAt(i)==CharInArray[j]){
								CharOutArray[0]=CharInArray[j]
							CharOutArray[1]=eval(i+1);
								return (CharOutArray);
							}
						}
						break;
				}
			}
			return "0";
		}
	};
	
})(jQuery);