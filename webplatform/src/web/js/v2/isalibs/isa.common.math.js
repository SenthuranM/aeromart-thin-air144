function ISAMath() {}

ISAMath.DEFAULT_DECIMAL_COUNT = 2; 

/**
 * for new currency round up rules
 * @author Lasantha Pambagoda
 */
ISAMath.getFormattRoundedValue = function getFormattRoundedValue(isRoundVisible, input, boundry, breakPoint){

	var roundedValue = null;
	var formattedRoundedValue = null;
	
	if(isRoundVisible == true) {
		
		if(boundry != null && boundry != "" && breakPoint != null && breakPoint != ""){
			roundedValue = ISAMath.getRoundedValue(input, boundry, breakPoint);
			if(boundry < 1) {
				// get the decimal points to format
				var fomatPlaces = ISAMath.getNoOfDecimalPlaces(boundry);
				formattedRoundedValue = CurrencyFormat(roundedValue, fomatPlaces);
			} else {
				formattedRoundedValue = CurrencyFormat(roundedValue, ISAMath.DEFAULT_DECIMAL_COUNT);
			}
		// parameters are missing, apply the two decimal round up 
		} else {
			roundedValue = ISAMath.defaultRounding(input);
			formattedRoundedValue = CurrencyFormat(roundedValue, ISAMath.DEFAULT_DECIMAL_COUNT);
		}
	// round visibility is false, apply the two decimal round up
	} else {
		roundedValue = ISAMath.defaultRounding(input);
		formattedRoundedValue = CurrencyFormat(roundedValue, ISAMath.DEFAULT_DECIMAL_COUNT);
	}
	return formattedRoundedValue;
}

/**
 * get the no of decimal places for a given number
 * @author Lasantha Pambagoda
 * @deprecated
 */
ISAMath.getNoOfDecimalPlaces =  function getNoOfDecimalPlaces(value){
	var noOfPlaces = ISAMath.DEFAULT_DECIMAL_COUNT;
	if(value != null && !isNaN(value)) {
		var strValue = value + "";
		var indexDecimal = strValue.indexOf(".");
		if(indexDecimal != -1) {
			try {
				noOfPlaces = (strValue.substring(indexDecimal + 1)).length;
			}catch (e) {
				// do nothing noOfPlaces remains 2
			}
		}
	}
	return noOfPlaces;
}

/**
 * Default rounding rule, two decimal round up
 * @author Lasantha Pambagoda
 */	
ISAMath.defaultRounding = function defaultRounding(input) {
	
	var multiplyBy = ISAMath.toThePower(10, ISAMath.DEFAULT_DECIMAL_COUNT);
	if(input != null && !isNaN(input)) {
		var roundUpValue = Math.ceil(input * multiplyBy)/multiplyBy;
	}
	return roundUpValue;
}

/**
 * Calculate and return the to the power. power should be plus integer. 
 * otherwise return number itself
 */	
ISAMath.toThePower = function toThePower(number, power) {
	var result = number;
	if(power > 0) {
		for(var i = 0; i < power; i++) {
			result = result * power;
		}
	}
	return result;
}

/**
 * Calculate and return the rounded value depending on the boundary and breakpoint
 */	
ISAMath.getRoundedValue = function getRoundedValue(input, boundry, breakPoint) {
	
	var output  = 0;
	if(boundry == 0) return output;
	var divValue = Math.floor(input / boundry);			
	var modValue = input % boundry;			
	var difference = 0;
	if(modValue != 0) {
		if(breakPoint == 0) {
			divValue = divValue +1;
		}else if(breakPoint != boundry) {
			difference = input - (divValue * boundry);					
			if(difference == breakPoint || difference > breakPoint) {
				divValue = divValue +1;
			}
		}		
	}
	output = divValue * boundry;		
	return output;
}