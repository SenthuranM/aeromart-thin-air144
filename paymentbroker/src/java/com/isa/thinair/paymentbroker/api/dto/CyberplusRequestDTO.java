package com.isa.thinair.paymentbroker.api.dto;

import java.io.Serializable;

import javax.xml.datatype.XMLGregorianCalendar;

public class CyberplusRequestDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5785885773722689628L;

	private String shopId;

	private XMLGregorianCalendar transmissionDate;

	private String transactionId;

	private int sequenceNb;

	private String ctxMode;

	private String newTransactionId;

	private long amount;

	private int currency;

	private XMLGregorianCalendar presentationDate;

	private int validationMode;

	private String comment;

	private String paymentMethod;

	private String orderId;

	private String cardNumber;

	private String cardNetwork;

	private XMLGregorianCalendar cardExpirationDate;

	private String cvv;

	private String wsSignature;

	private String orderInfo;

	private String orderInfo2;

	private String orderInfo3;

	private String contractNumber;

	private String customerId;

	private String customerTitle;

	private String customerName;

	private String customerPhone;

	private String customerMail;

	private String customerAddress;

	private String customerZipCode;

	private String customerCity;

	private String customerCountry;

	private String customerLanguage;

	private String customerIP;

	private boolean customerSendEmail;

	private String proxyHost;

	private String proxyPort;

	public String getShopId() {
		return shopId;
	}

	public void setShopId(String shopId) {
		this.shopId = shopId;
	}

	public XMLGregorianCalendar getTransmissionDate() {
		return transmissionDate;
	}

	public void setTransmissionDate(XMLGregorianCalendar transmissionDate) {
		this.transmissionDate = transmissionDate;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public int getSequenceNb() {
		return sequenceNb;
	}

	public void setSequenceNb(int sequenceNb) {
		this.sequenceNb = sequenceNb;
	}

	public String getCtxMode() {
		return ctxMode;
	}

	public void setCtxMode(String ctxMode) {
		this.ctxMode = ctxMode;
	}

	public String getNewTransactionId() {
		return newTransactionId;
	}

	public void setNewTransactionId(String newTransactionId) {
		this.newTransactionId = newTransactionId;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public int getCurrency() {
		return currency;
	}

	public void setCurrency(int currency) {
		this.currency = currency;
	}

	public XMLGregorianCalendar getPresentationDate() {
		return presentationDate;
	}

	public void setPresentationDate(XMLGregorianCalendar presentationDate) {
		this.presentationDate = presentationDate;
	}

	public int getValidationMode() {
		return validationMode;
	}

	public void setValidationMode(int validationMode) {
		this.validationMode = validationMode;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardNetwork() {
		return cardNetwork;
	}

	public void setCardNetwork(String cardNetwork) {
		this.cardNetwork = cardNetwork;
	}

	public XMLGregorianCalendar getCardExpirationDate() {
		return cardExpirationDate;
	}

	public void setCardExpirationDate(XMLGregorianCalendar cardExpirationDate) {
		this.cardExpirationDate = cardExpirationDate;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getWsSignature() {
		return wsSignature;
	}

	public void setWsSignature(String wsSignature) {
		this.wsSignature = wsSignature;
	}

	public String getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(String orderInfo) {
		this.orderInfo = orderInfo;
	}

	public String getOrderInfo2() {
		return orderInfo2;
	}

	public void setOrderInfo2(String orderInfo2) {
		this.orderInfo2 = orderInfo2;
	}

	public String getOrderInfo3() {
		return orderInfo3;
	}

	public void setOrderInfo3(String orderInfo3) {
		this.orderInfo3 = orderInfo3;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerTitle() {
		return customerTitle;
	}

	public void setCustomerTitle(String customerTitle) {
		this.customerTitle = customerTitle;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public String getCustomerMail() {
		return customerMail;
	}

	public void setCustomerMail(String customerMail) {
		this.customerMail = customerMail;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getCustomerZipCode() {
		return customerZipCode;
	}

	public void setCustomerZipCode(String customerZipCode) {
		this.customerZipCode = customerZipCode;
	}

	public String getCustomerCity() {
		return customerCity;
	}

	public void setCustomerCity(String customerCity) {
		this.customerCity = customerCity;
	}

	public String getCustomerCountry() {
		return customerCountry;
	}

	public void setCustomerCountry(String customerCountry) {
		this.customerCountry = customerCountry;
	}

	public String getCustomerLanguage() {
		return customerLanguage;
	}

	public void setCustomerLanguage(String customerLanguage) {
		this.customerLanguage = customerLanguage;
	}

	public String getCustomerIP() {
		return customerIP;
	}

	public void setCustomerIP(String customerIP) {
		this.customerIP = customerIP;
	}

	public boolean isCustomerSendEmail() {
		return customerSendEmail;
	}

	public void setCustomerSendEmail(boolean customerSendEmail) {
		this.customerSendEmail = customerSendEmail;
	}

	public String getProxyHost() {
		return proxyHost;
	}

	public void setProxyHost(String proxyHost) {
		this.proxyHost = proxyHost;
	}

	public String getProxyPort() {
		return proxyPort;
	}

	public void setProxyPort(String proxyPort) {
		this.proxyPort = proxyPort;
	}

	public String toString() {
		StringBuilder cyberplusRequest = new StringBuilder();
		cyberplusRequest.append(shopId).append(":").append(nullConvertToString(getShopId())).append(", ").append(shopId)
				.append(":").append(nullConvertToString(getShopId())).append(", ").append(transmissionDate).append(":")
				.append(nullConvertToString(getTransmissionDate())).append(", ").append(transactionId).append(":")
				.append(nullConvertToString(getTransactionId())).append(", ").append(sequenceNb).append(":")
				.append(nullConvertToString(getSequenceNb())).append(", ").append(ctxMode).append(":")
				.append(nullConvertToString(getCtxMode())).append(", ").append(newTransactionId).append(":")
				.append(nullConvertToString(getNewTransactionId())).append(", ").append(amount).append(":")
				.append(nullConvertToString(getAmount())).append(", ").append(currency).append(":")
				.append(nullConvertToString(getCurrency())).append(", ").append(presentationDate).append(":")
				.append(nullConvertToString(getPresentationDate())).append(", ").append(validationMode).append(":")
				.append(nullConvertToString(getValidationMode())).append(", ").append(comment).append(":")
				.append(nullConvertToString(getComment())).append(", ").append(paymentMethod).append(":")
				.append(nullConvertToString(getPaymentMethod())).append(", ").append(orderId).append(":")
				.append(nullConvertToString(getOrderId())).append(", ").append(cardNumber).append(":")
				.append(nullConvertToString(getCardNumber())).append(", ").append(cardNetwork).append(":")
				.append(nullConvertToString(getCardNetwork())).append(", ").append(cardExpirationDate).append(":")
				.append(nullConvertToString(getCardExpirationDate())).append(", ").append(cvv).append(":")
				.append(nullConvertToString(getCvv()));
		return cyberplusRequest.toString();
	}

	private String nullConvertToString(Object obj) {
		String strReturn = "";

		if (obj == null) {
			strReturn = "";
		} else {
			strReturn = obj.toString().trim();
		}
		return strReturn;
	}
}
