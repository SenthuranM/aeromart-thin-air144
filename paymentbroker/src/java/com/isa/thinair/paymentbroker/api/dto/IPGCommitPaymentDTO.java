package com.isa.thinair.paymentbroker.api.dto;

/**
 * DTO to transfer parameters required for payment collection
 */
public class IPGCommitPaymentDTO extends IPGBaseParams {

	public IPGCommitPaymentDTO(IPGIdentificationParamsDTO identificationParams) {
		super(identificationParams);
	}
}
