package com.isa.thinair.paymentbroker.api.constants;

public class PaymentAPIConsts {
	
	public static final String REDIRECT_CONTROLLER_URL_SUFFIX = "/controller/postPayment/redirect";
	public static final String REDIRECT_CONTROLLER_MOBILE_URL_SUFFIX = "/controller/postPayment/mibe/redirect";
	public static final String REDIRECT_VOUCHER_PURCHASE_CONTROLLER_URL_SUFFIX = "/controller/voucher/payment/redirect";
	public static final String REDIRECT_VOUCHER_PURCHASE_CONTROLLER_MOBILE_URL_SUFFIX = "/controller/voucher/payment/mibe/redirect";
	public static final String REDIRECT_CONTROLLER_URL_OLD_SUFFIX = "handleInterlineIPGResponse.action";	
	public static final String REDIRECT_CONTROLLER_MTC_OFFLINE_URL_SUFFIX = "mTCOfflinePaymentResponseHandler.action";
	public static final String REDIRECT_CONTROLLER_MTC_LOAD_URL_SUFFIX = "mTCLoadReservation.action";
	public static final String REDIRECT_CONTROLLER_MTC_EW_URL_SUFFIX = "mTCEWPaymentResponseHandler.action";	
	
}
