package com.isa.thinair.paymentbroker.api.dto;

import java.io.Serializable;

/**
 * DTO to provide payment advise from the payment collector. Depending on this Advise caller will decide how to collect
 * the payment.
 */
public class PaymentCollectionAdvise implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7791122111914932178L;

	private String ipgType;

	private String preValidationRequired;

	private String redirectMechanism;
}
