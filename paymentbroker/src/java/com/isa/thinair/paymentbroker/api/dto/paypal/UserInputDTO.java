package com.isa.thinair.paymentbroker.api.dto.paypal;

import java.io.Serializable;

import com.paypal.soap.api.CountryCodeType;
import com.paypal.soap.api.CurrencyCodeType;
import com.paypal.soap.api.PaymentActionCodeType;

public class UserInputDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String token;

	private String amount;

	private String returnUrl;

	private String cancelUrl;

	private String refundType;

	private String transactionId;

	private CurrencyCodeType currencyCodeType;

	private String invoiceId;

	private String userId;

	private String version;

	private String creditCardType;

	private String creditCardNumber;

	private int expMonth;

	private int expYear;

	private String CVV2;

	private String IPAddress;

	private String typeOfGoods;// For digital goods this will be 0. And for others this will be 1.

	private String noShipping;// For digital goods this will be 1. And for others this will be 0.

	private PaymentActionCodeType paymentAction;

	private String buyerFirstName;

	private String buyerLastName;

	private String payerCountry;

	private CountryCodeType countryCodeType;

	private String buyerAddress1;

	private String buyerAddress2;

	private String buyerCity;

	private String buyerState;

	private String postalCode;

	private int paymentBrokerRefNo;

	public void setToken(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setCancelUrl(String cancelUrl) {
		this.cancelUrl = cancelUrl;
	}

	public String getCancelUrl() {
		return cancelUrl;
	}

	public void setRefundType(String refundType) {
		this.refundType = refundType;
	}

	public String getRefundType() {
		return refundType;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setCurrencyCodeType(CurrencyCodeType currencyCodeType) {
		this.currencyCodeType = currencyCodeType;
	}

	public CurrencyCodeType getCurrencyCodeType() {
		return currencyCodeType;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserId() {
		return userId;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAmount() {
		return amount;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getVersion() {
		return version;
	}

	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}

	public String getCreditCardType() {
		return creditCardType;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setExpMonth(int expMonth) {
		this.expMonth = expMonth;
	}

	public int getExpMonth() {
		return expMonth;
	}

	public void setExpYear(int expYear) {
		this.expYear = expYear;
	}

	public int getExpYear() {
		return expYear;
	}

	public void setCVV2(String cVV2) {
		CVV2 = cVV2;
	}

	public String getCVV2() {
		return CVV2;
	}

	public void setIPAddress(String iPAddress) {
		IPAddress = iPAddress;
	}

	public String getIPAddress() {
		return IPAddress;
	}

	public void setTypeOfGoods(String typeOfGoods) {
		this.typeOfGoods = typeOfGoods;
	}

	public String getTypeOfGoods() {
		return typeOfGoods;
	}

	public void setNoShipping(String noShipping) {
		this.noShipping = noShipping;
	}

	public String getNoShipping() {
		return noShipping;
	}

	public void setPaymentAction(PaymentActionCodeType paymentAction) {
		this.paymentAction = paymentAction;
	}

	public PaymentActionCodeType getPaymentAction() {
		return paymentAction;
	}

	public void setBuyerFirstName(String buyerFirstName) {
		this.buyerFirstName = buyerFirstName;
	}

	public String getBuyerFirstName() {
		return buyerFirstName;
	}

	public void setBuyerLastName(String buyerLastName) {
		this.buyerLastName = buyerLastName;
	}

	public String getBuyerLastName() {
		return buyerLastName;
	}

	public void setPayerCountry(String payerCountry) {
		this.payerCountry = payerCountry;
	}

	public String getPayerCountry() {
		return payerCountry;
	}

	public void setCountryCodeType(CountryCodeType countryCodeType) {
		this.countryCodeType = countryCodeType;
	}

	public CountryCodeType getCountryCodeType() {
		return countryCodeType;
	}

	public void setBuyerAddress1(String buyerAddress1) {
		this.buyerAddress1 = buyerAddress1;
	}

	public String getBuyerAddress1() {
		return buyerAddress1;
	}

	public void setBuyerAddress2(String buyerAddress2) {
		this.buyerAddress2 = buyerAddress2;
	}

	public String getBuyerAddress2() {
		return buyerAddress2;
	}

	public void setBuyerCity(String buyerCity) {
		this.buyerCity = buyerCity;
	}

	public String getBuyerCity() {
		return buyerCity;
	}

	public void setBuyerState(String buyerState) {
		this.buyerState = buyerState;
	}

	public String getBuyerState() {
		return buyerState;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPaymentBrokerRefNo(int paymentBrokerRefNo) {
		this.paymentBrokerRefNo = paymentBrokerRefNo;
	}

	public int getPaymentBrokerRefNo() {
		return paymentBrokerRefNo;
	}

}
