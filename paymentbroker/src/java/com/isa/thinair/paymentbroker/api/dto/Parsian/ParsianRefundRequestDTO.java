
package com.isa.thinair.paymentbroker.api.dto.Parsian;

import java.io.Serializable;

public class ParsianRefundRequestDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private long receiptNo;
	
	private long refundId;
	
	private long amount;
	
	private String refundUserName;
	
	private String password; 

	public long getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(long receiptNo) {
		this.receiptNo = receiptNo;
	}

	public long getRefundId() {
		return refundId;
	}

	public void setRefundId(long refundId) {
		this.refundId = refundId;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("receipt_no:" + receiptNo).append(",refund_amount:" + amount).append(",refund_Id:" + refundId);
		return builder.toString();
	}

	public String getRefundUserName() {
		return refundUserName;
	}

	public void setRefundUserName(String refundUserName) {
		this.refundUserName = refundUserName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
