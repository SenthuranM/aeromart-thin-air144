package com.isa.thinair.paymentbroker.api.dto;

import java.io.Serializable;

public class CyberplusResponseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6949346856503606407L;

	private int errorCode;

	private String extendedErrorCode;

	private int transactionStatus;

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getExtendedErrorCode() {
		return extendedErrorCode;
	}

	public void setExtendedErrorCode(String extendedErrorCode) {
		this.extendedErrorCode = extendedErrorCode;
	}

	public int getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(int transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public String toString() {
		StringBuilder cyberplusResponse = new StringBuilder();
		cyberplusResponse.append("errorCode").append(":").append(errorCode).append(", ").append("extendedErrorCode").append(":")
				.append(extendedErrorCode).append(", ").append("transactionStatus").append(":").append(transactionStatus)
				.append(", ");
		return cyberplusResponse.toString();
	}
}
