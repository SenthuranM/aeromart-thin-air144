package com.isa.thinair.paymentbroker.api.dto.cmi;

import java.io.Serializable;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class CMIRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	public static String BROKER_TYPE = "brokerType";
	public static String REQ_METHOD = "requestMethod";
	public static String CLIENT_ID = "clientID";
	public static String IPG_URL = "ipgURL";
	public static String XML_RES = "xmlResponse";
	public static String STORETYPE = "storetype";
	public static String HASH = "hash";
	public static String TRANTYPE = "trantype";
	public static String AMOUNT = "amount";
	public static String CURRENCY = "currency";
	public static String OID = "oid";
	public static String OKURL = "okurl";
	public static String FAILURL = "failurl";
	public static String LANG = "lang";
	public static String RND = "rnd";
	public static String HASHALGORITHM = "hashalgorithm";
	public static String USERNAME="userName";
	public static String PASSWORD="password";
	public static String CALLBACKRESPONSE="CallbackResponse";
	public static String STOREKEY="storeKey";
	public static String CALLBACK_URL="CallbackURL";
	public static String PHONE="tel";
	public static String EMAIL="email";
	public static String BILLTONAME="BillToName";
	public static String BILLTOCITY="BillToCity";
	public static String BILLTOSTATEPROV="BillToStateProv";
	public static String BILLTOCOUNTRY="BillToCountry";
	public static String ENCODING="encoding";
	
	private String billToCountry;
	private String billToStateProv;
	private String billToName;
	private String billToCity;
	private String clientID;
	private String email;
	private String tel;
	private String storetype;
	private String hash;
	private String tranType;
	private String currency;
	private String oid;
	private String okUrl;
	private String failUrl;
	private String lang;
	private String rnd;
	private String hashAlgorithm;
	private String storeKey;
	private String encoding;
	//public static final String RESPONSE_URL ="http://a374e9ca.ngrok.io/service-app/controller/cmi/callback" ;//AppSysParamsUtil.getSecureIBEUrl() + "cmiNotifyPaymentResponseHandler.action";
	/**
	 * params for xbe
	 */
	private String userName;
	private String password;
	private String type;
	private String curCode;
	private String cardNumber;
	private String expiry;
	private String cvv;
	private double amount;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCurCode() {
		return curCode;
	}

	public void setCurCode(String curCode) {
		this.curCode = curCode;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getExpiry() {
		return expiry;
	}

	public String getStoreKey() {
		return storeKey;
	}

	public void setStoreKey(String storeKey) {
		this.storeKey = storeKey;
	}

	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
	

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	@Override
	public String toString() {
		return "CMIRequest [clientID=" + clientID + ", storetype=" + storetype
				+ ", hash=" + hash + ", tranType=" + tranType + ", currency="
				+ currency + ", oid=" + oid + ", okUrl=" + okUrl + ", failUrl="
				+ failUrl + ", lang=" + lang + ", rnd=" + rnd
				+ ", hashAlgorithm=" + hashAlgorithm + ", storeKey=" + storeKey
				+ ", userName=" + userName + ", password=" + password
				+ ", type=" + type + ", curCode=" + curCode + ", cardNumber="
				+ cardNumber + ", expiry=" + expiry + ", cvv=" + cvv
				+ ", amount=" + amount + "]";
	}

}
