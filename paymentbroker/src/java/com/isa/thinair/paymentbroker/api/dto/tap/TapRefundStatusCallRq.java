package com.isa.thinair.paymentbroker.api.dto.tap;

import java.io.Serializable;

public class TapRefundStatusCallRq implements Serializable {

	private static final long serialVersionUID = -522528578944591L;
	
	private String referenceID;
	private String userName;
	private Long merchantID;
	public String getReferenceID() {
		return referenceID;
	}
	public void setReferenceID(String referenceID) {
		this.referenceID = referenceID;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Long getMerchantID() {
		return merchantID;
	}
	public void setMerchantID(Long merchantID) {
		this.merchantID = merchantID;
	}

}
