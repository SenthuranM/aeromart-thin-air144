package com.isa.thinair.paymentbroker.api.dto.qiwi;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class QiwiPRequest implements Serializable, Cloneable {
	
	public static final String QIWI_PROVIDER_CODE = "QIWI";

	public static final String MERCHANT_ID = "prv_id";
	public static final String TNX_ID = "bill_id";
	public static final String USER = "user";
	public static final String AMOUNT = "amount";
	public static final String CURRENCY = "ccy";
	public static final String COMMENT = "comment";
	public static final String LIFETIME = "lifetime";
	public static final String PAY_SOURCE = "pay_source";	//optional
	public static final String MERCHANT_NAME = "prv_name";
	public static final String AUTHORIZATION = "authorization";
	
	public static final String SHOP = "shop";
	public static final String TRANSACTION = "transaction";
	public static final String SUCCESS_URL = "successUrl";
	public static final String FAIL_URL = "failUrl";
	
	public static final String STATUS = "status";
	public static final String RESULT_CODE = "result_code";
	
	public static final String ERROR = "error";
	public static final String DESCRIPTION = "description";
	
	// invoice status codes
	public static final String WAITING = "waiting";
	public static final String PAID = "paid";
	public static final String REJECTED = "rejected";
	public static final String UNPAID = "unpaid";
	public static final String EXPIRED = "expired";
	
	// payment status codes
	public static final String PROCESSING = "processing";
	public static final String SUCCESS = "success";
	public static final String FAIL = "fail";
	public static final String RESPONSE_ORDER = "order";
	
	public static final String INVOICE_TIMEOUT_PERIOD = "invoiceTimeout";
	public static final String INVOICE_STATUS_REQ_URL = "invoiceStatusReqURL";
	public static final String CREATE_INVOICE_URL = "createInvoiceURL";
	public static final String REDIRECT_URL = "redirectURL";
	public static final String INVOICE_CREATION_METHOD = "invoiceCreationMethod";
	public static final String HDN_QIWI_MOBILE = "hdnQiwiMobileNumber";
	public static final String IS_ONLINE_PAYMENT_MODE = "isQiwiOnlineMode";
	public static final String REFUND_URL = "refundURL";
	public static final String REFUND_STATUS_REQ_URL = "refundStatusReqURL";
	public static final String QIWI_RESPONSE_WAITING_TIME_IN_SEC = "qiwiResponseWaitingTimeInSec";
	
	public static final String LOGIN = "login";
	public static final String PASSWORD = "password";
	public static final String SERVER_TO_SERVER_RES_PASSWORD = "invoiceNotifPassword";
	
	public static final String FORM_SUMBIT_METHOD = "formSubmitMethod";
	public static final String FORM_ACTION = "formAction";
	
	public static final String QIWI_ONHOLD_AVAILABLE = "onholdPnr";
	public static final String QIWI_INVOICE_STATUS = "invoiceStatus";
	public static final String QIWI_ERROR_CODE = "qiwiErrorCode";
	public static final String QIWI_WAITING_FOR_PAYMENT_CONF = "qiwi.waiting.Payment.Confirmation";
	public static final String QIWI_PAYMENT_CONF_FAILED = "qiwi.failed.Payment.Confirmation";
	public static final String QIWI_TIME_TO_SPARE = "qiwiTimeToSpare";
	public static final String QIWI_PNR = "qiwiPnr";
	
	private String qiwiMerchantID;
	private String qiwiCreateInvoiceURL;
	private String qiwiInvoiceStatusReqURL;
	private String qiwiRedirectURL;
	private String qiwiInvoiceTimeout;
	private String createInvoiceRequestMethod;
	private String checkInvoiceRequestMethod;
	private String qiwiMobileNumber;
	private String transactionID;
	private String merchantID;
	private String login;
	private String password;
	private String pnr;
	private Date onholdReleaseTime;
	private String refundURL;
	private String refundStatusReqURL;
	private String amount;
	private String refundID;
	private BigDecimal totalPaymentAmount;
	private String merchantName;
	private boolean isOfflineMode;
	private int qiwiResponseWaitingTimeInSec;
	
	public String getQiwiMerchantID() {
		return qiwiMerchantID;
	}
	public void setQiwiMerchantID(String qiwiMerchantID) {
		this.qiwiMerchantID = qiwiMerchantID;
	}
	public String getQiwiInvoiceCreationURL() {
		return qiwiCreateInvoiceURL;
	}
	public void setQiwiCreationInvoiceURL(String qiwiCreateInvoiceURL) {
		this.qiwiCreateInvoiceURL = qiwiCreateInvoiceURL;
	}
	public String getQiwiInvoiceStatusReqURL() {
		return qiwiInvoiceStatusReqURL;
	}
	public void setQiwiInvoiceStatusReqURL(String qiwiInvoiceStatusReqURL) {
		this.qiwiInvoiceStatusReqURL = qiwiInvoiceStatusReqURL;
	}
	public String getQiwiRedirectURL() {
		return qiwiRedirectURL;
	}
	public void setQiwiRedirectURL(String qiwiRedirectURL) {
		this.qiwiRedirectURL = qiwiRedirectURL;
	}
	public String getQiwiInvoiceTimeout() {
		return qiwiInvoiceTimeout;
	}
	public void setQiwiInvoiceTimeout(String qiwiInvoiceTimeout) {
		this.qiwiInvoiceTimeout = qiwiInvoiceTimeout;
	}
	public void setQiwiMobileNumber(String qiwiMobileNumber) {
		this.qiwiMobileNumber = qiwiMobileNumber;		
	}
	public String getQiwiMobileNumber(){
		return qiwiMobileNumber;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	
	/**
	 * @return transaction id(bill id)
	 * 
	 * */
	public String getTransactionID(){
		return transactionID;
	}
	public String getMerchantID() {
		return merchantID;
	}
	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}
	public String getCreateInvoiceRequestMethod() {
		return createInvoiceRequestMethod;
	}
	public void setCreateInvoiceRequestMethod(String createInvoiceRequestMethod) {
		this.createInvoiceRequestMethod = createInvoiceRequestMethod;
	}
	public String getCheckInvoiceRequestMethod() {
		return checkInvoiceRequestMethod;
	}
	public void setCheckInvoiceRequestMethod(String checkInvoiceRequestMethod) {
		this.checkInvoiceRequestMethod = checkInvoiceRequestMethod;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	public Date getOnholdReleaseTime() {
		return onholdReleaseTime;
	}
	public void setOnholdReleaseTime(Date onholdReleaseTime) {
		this.onholdReleaseTime = onholdReleaseTime;
	}
	public String getRefundStatusReqURL() {
		return refundStatusReqURL;
	}
	public void setRefundStatusReqURL(String refundStatusReqURL) {
		this.refundStatusReqURL = refundStatusReqURL;
	}
	public String getRefundURL() {
		return refundURL;
	}
	public void setRefundURL(String refundURL) {
		this.refundURL = refundURL;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getRefundID() {
		return refundID;
	}
	public void setRefundID(String refundID) {
		this.refundID = refundID;
	}
	
	@Override
	public String toString(){
			
		String s = "Amount :" + amount;
		
		if (refundID != null) {
			s += "\n refundId :" + refundID;
		}
		if (transactionID != null) {
			s += "\n Bill Id :" + transactionID;
		}
		if(pnr != null){
			s += "\n pnr :" + pnr;
		}
		if (login != null) {
			s += "\n login :" + login;
		}
		if (password != null) {
			s += "\n password :" + password;
		}
		
		return s;
	}
	public BigDecimal getTotalPaymentAmount() {
		return totalPaymentAmount;
	}
	public void setTotalPaymentAmount(BigDecimal totalPaymentAmount) {
		this.totalPaymentAmount = totalPaymentAmount;
	}
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	
	public Object clone(){  
	    try{  
	        return super.clone();  
	    }catch(Exception e){ 
	        return null; 
	    }
	}
	/**
	 * @return the isOfflineMode
	 */
	public boolean isOfflineMode() {
		return isOfflineMode;
	}
	/**
	 * @param isOfflineMode the isOfflineMode to set
	 */
	public void setOfflineMode(boolean isOfflineMode) {
		this.isOfflineMode = isOfflineMode;
	}
	/**
	 * @return the qiwiResponseWaitingTimeInSec
	 */
	public int getQiwiResponseWaitingTimeInSec() {
		return qiwiResponseWaitingTimeInSec;
	}
	/**
	 * @param qiwiResponseWaitingTimeInSec the qiwiResponseWaitingTimeInSec to set
	 */
	public void setQiwiResponseWaitingTimeInSec(int qiwiResponseWaitingTimeInSec) {
		this.qiwiResponseWaitingTimeInSec = qiwiResponseWaitingTimeInSec;
	}
}
