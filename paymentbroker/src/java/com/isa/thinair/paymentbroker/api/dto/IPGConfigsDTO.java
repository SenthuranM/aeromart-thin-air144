package com.isa.thinair.paymentbroker.api.dto;

import java.io.Serializable;

public class IPGConfigsDTO implements Serializable {

	private static final long serialVersionUID = -5849423522428580311L;

	private String merchantID;

	private String accessCode;

	private String ipgURL;

	private String username;

	private String password;

	private String version;

	private String statusUrl;

	private String ctxMode;

	private String secureSecret;

	private int responseWaitingTimeInSec;

	private String paymentAmount;

	private boolean refunding;

	private String paymentCurrency;

	private String opaque;

	/**
	 * @return Returns the accessCode.
	 */
	public String getAccessCode() {
		return accessCode;
	}

	/**
	 * @param accessCode
	 *            The accessCode to set.
	 */
	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	/**
	 * @return Returns the ipgURL.
	 */
	public String getIpgURL() {
		return ipgURL;
	}

	/**
	 * @param ipgURL
	 *            The ipgURL to set.
	 */
	public void setIpgURL(String ipgURL) {
		this.ipgURL = ipgURL;
	}

	/**
	 * @return Returns the merchantID.
	 */
	public String getMerchantID() {
		return merchantID;
	}

	/**
	 * @param merchantID
	 *            The merchantID to set.
	 */
	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	/**
	 * @return Returns the password.
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            The password to set.
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return Returns the username.
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            The username to set.
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return Returns the version.
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            The version to set.
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the statusUrl
	 */
	public String getStatusUrl() {
		return statusUrl;
	}

	/**
	 * @param statusUrl
	 *            the statusUrl to set
	 */
	public void setStatusUrl(String statusUrl) {
		this.statusUrl = statusUrl;
	}

	/**
	 * @return the ctxMode
	 */
	public String getCtxMode() {
		return ctxMode;
	}

	/**
	 * @param ctxMode
	 *            the ctxMode to set
	 */
	public void setCtxMode(String ctxMode) {
		this.ctxMode = ctxMode;
	}

	/**
	 * 
	 * @return the secureSecret
	 */

	public String getSecureSecret() {
		return secureSecret;
	}

	/**
	 * 
	 * @param secureSecret
	 */
	public void setSecureSecret(String secureSecret) {
		this.secureSecret = secureSecret;
	}

	/**
	 * @return the responseWaitingTimeInSec
	 */
	public int getResponseWaitingTimeInSec() {
		return responseWaitingTimeInSec;
	}

	/**
	 * @param responseWaitingTimeInSec
	 *            the responseWaitingTimeInSec to set
	 */
	public void setResponseWaitingTimeInSec(int responseWaitingTimeInSec) {
		this.responseWaitingTimeInSec = responseWaitingTimeInSec;
	}

	/**
	 * @return the paymentAmount
	 */
	public String getPaymentAmount() {
		return paymentAmount;
	}

	/**
	 * @param paymentAmount
	 *            the paymentAmount to set
	 */
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public boolean isRefunding() {
		return refunding;
	}

	public void setRefunding(boolean refunding) {
		this.refunding = refunding;
	}

	public String getPaymentCurrency() {
		return paymentCurrency;
	}

	public void setPaymentCurrency(String paymentCurrency) {
		this.paymentCurrency = paymentCurrency;
	}

	public String getOpaque() {
		return opaque;
	}

	public void setOpaque(String opaque) {
		this.opaque = opaque;
	}
}
