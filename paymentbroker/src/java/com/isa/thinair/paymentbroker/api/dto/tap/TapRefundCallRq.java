package com.isa.thinair.paymentbroker.api.dto.tap;

import java.io.Serializable;
import java.math.BigDecimal;

public class TapRefundCallRq implements Serializable {

	private static final long serialVersionUID = -564848578944591L;
	
	private String referenceID;
	private String orderID;
	private BigDecimal amount;
	private String currencyCode;
	private String userName;
	private Long merchantID;
	
	public String getReferenceID() {
		return referenceID;
	}
	public void setReferenceID(String referenceID) {
		this.referenceID = referenceID;
	}
	public String getOrderID() {
		return orderID;
	}
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Long getMerchantID() {
		return merchantID;
	}
	public void setMerchantID(Long merchantID) {
		this.merchantID = merchantID;
	}
	
	public String toString(){
		StringBuffer resBuff = new StringBuffer();
		resBuff.append("Tap GoSell TapRefundCallRq Details [");
		resBuff.append(super.toString());
		resBuff.append(",referenceID: " + this.referenceID);
		resBuff.append(", orderID: " + this.orderID);
		resBuff.append(",amount: " + this.amount);
		resBuff.append(",currencyCode: " + this.currencyCode);
		resBuff.append(",userName: " + this.userName);
		resBuff.append(",merchantID: " + this.merchantID);
		resBuff.append(" ] ");
		return resBuff.toString();
	}
}
