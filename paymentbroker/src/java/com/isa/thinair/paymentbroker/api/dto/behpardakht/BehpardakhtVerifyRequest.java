package com.isa.thinair.paymentbroker.api.dto.behpardakht;

import java.io.Serializable;

public class BehpardakhtVerifyRequest extends BehpardakhtCommonRequest implements Serializable {

	private static final long serialVersionUID = -190909098464591L;

	public static final String SALEORDER_ID = "saleOrder_Id";
	public static final String SALEREFERENCE_ID = "saleReference_Id";

	private Long saleOrderId;
	private Long saleReferenceId;

	// TO-DO
	// need to override the toString method and write the javadocs

	public Long getSaleOrderId() {
		return saleOrderId;
	}

	public void setSaleOrderId(Long saleOrderId) {
		this.saleOrderId = saleOrderId;
	}

	public Long getSaleReferenceId() {
		return saleReferenceId;
	}

	public void setSaleReferenceId(Long saleReferenceId) {
		this.saleReferenceId = saleReferenceId;
	}

	public String toString() {
		StringBuffer resBuff = new StringBuffer();
		resBuff.append("Behpardakht BehpardakhtInquiryRequest Details [");
		resBuff.append(super.toString());
		resBuff.append("saleOrderId: " + this.saleOrderId);
		resBuff.append(", saleReferenceId: " + this.saleReferenceId);
		resBuff.append(" ] ");
		return resBuff.toString();
	}

}
