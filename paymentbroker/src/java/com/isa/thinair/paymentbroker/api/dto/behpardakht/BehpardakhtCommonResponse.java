package com.isa.thinair.paymentbroker.api.dto.behpardakht;

import java.io.Serializable;

public class BehpardakhtCommonResponse implements Serializable {

	private static final long serialVersionUID = -190999585994591L;

	public static final String REF_ID = "RefId";

	public static final String RES_CODE = "ResCode";

	public static final String SALES_ORDER_ID = "SaleOrderId";

	public static final String SALES_REF_ID = "SaleReferenceId";

	public static final String CARD_HOLDER_INFO = "CardHolderInfo";

	public String RefId;
	public String ResCode;
	public String SaleOrderId;
	public Long SaleReferenceId;
	public String CardHolderInfo;

	public String getRefId() {
		return RefId;
	}

	public void setRefId(String refId) {
		RefId = refId;
	}

	public String getResCode() {
		return ResCode;
	}

	public void setResCode(String resCode) {
		ResCode = resCode;
	}

	public String getSaleOrderId() {
		return SaleOrderId;
	}

	public void setSaleOrderId(String saleOrderId) {
		SaleOrderId = saleOrderId;
	}

	public Long getSaleReferenceId() {
		return SaleReferenceId;
	}

	public void setSaleReferenceId(Long saleReferenceId) {
		SaleReferenceId = saleReferenceId;
	}

	public String getCardHolderInfo() {
		return CardHolderInfo;
	}

	public void setCardHolderInfo(String cardHolderInfo) {
		CardHolderInfo = cardHolderInfo;
	}

}
