package com.isa.thinair.paymentbroker.api.dto.tap;

import java.io.Serializable;

public class TapGetOrderStatusRequestCallRs extends TapCommonResponse implements Serializable {

	private static final long serialVersionUID = -122339478944591L;
	
	private String payTxnID;
	private String paymode;
	private String trackID;
	
	public String getPayTxnID() {
		return payTxnID;
	}
	public void setPayTxnID(String payTxnID) {
		this.payTxnID = payTxnID;
	}
	public String getPaymode() {
		return paymode;
	}
	public void setPaymode(String paymode) {
		this.paymode = paymode;
	}
	public String getTrackID() {
		return trackID;
	}
	public void setTrackID(String trackID) {
		this.trackID = trackID;
	}
	public String toString(){
		StringBuffer resBuff = new StringBuffer();

		resBuff.append("PAYTXNID : ").append(payTxnID).append("PAYMODE : ").append(paymode).append("TRACKID : ")
				.append(trackID).append("RESPONSECODE : ").append(getResponseCode()).append("RESPONSEMESSAGE : ").append(getResponseMessage()); 
		return resBuff.toString();
	}
}
