package com.isa.thinair.paymentbroker.api.constants;

public class EFawateerConstants {

	public static final String BILLPULL_RESPONSE = "BILPULRS";
	public static final String PAYMENT_NOTIFICATION_RESPONSE = "BLRPMTNTFRS";
	public static final String SUCCESS_ERROR_CODE = "0";
	public static final String SUCCESS_ERROR_DESC = "Success";
	public static final String SUCCESS_SEVERITY = "Info";
	public static final String SIGNATURE_ERROR_CODE = "2";
	public static final String SIGNATURE_ERROR_DESC = "InvalidSignature";
	public static final String ERROR_SEVERITY = "Error";
	public static final String INTERNAL_ERROR_CODE = "303";
	public static final String INTERNAL_ERROR_DESC = "InternalError";
	public static final String BILLPULL_REQUEST = "BILPULRQ";
	public static final String PAYMENT_NOTIFICATION_REQUEST = "BLRPMTNTFRQ";
	public static final String JOEBPPSTRX = "JOEBPPSTrx";
	public static final String DUE_AMOUNT = "DueAmt";
	public static final String SHA256_WITH_RSA = "SHA256withRSA";
	public static final String X_509 = "X.509";
	public static final String PMT_NEW = "PmtNew";
	public static final String BILL_NEW = "BillNew";
	public static final String RSA = "RSA";
	public static final String MESSAGE_BODY = "MsgBody";
	public static final String REQUEST = "Request";
	public static final String REC_COUNT = "1";
	public static final String REC_COUNT_ERROR = "0";
	public static final String TIME_ZONE = "GMT+03:00";
	public static final String STATUS_OHD = "OHD";
	public static final String SDR_CODE = "1";
	public static final String TRANSPORTATION = "Transportation";
	public static final String BANKTRXID = "BankTrxID";

	private EFawateerConstants() {

	}
}
