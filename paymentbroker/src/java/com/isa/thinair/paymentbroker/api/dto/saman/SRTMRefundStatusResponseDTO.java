package com.isa.thinair.paymentbroker.api.dto.saman;

import java.io.Serializable;

public class SRTMRefundStatusResponseDTO implements Serializable {

	private static final long serialVersionUID = -8303009098978344L;

	private String referenceNo;

	private String refundReg;

	private String refundRegDesc;

	private String refundTrace;

	private String refundTraceDesc;

	private String fullRespStr;

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getRefundReg() {
		return refundReg;
	}

	public void setRefundReg(String refundReg) {
		this.refundReg = refundReg;
	}

	public String getRefundRegDesc() {
		return refundRegDesc;
	}

	public void setRefundRegDesc(String refundRegDesc) {
		this.refundRegDesc = refundRegDesc;
	}

	public String getRefundTrace() {
		return refundTrace;
	}

	public void setRefundTrace(String refundTrace) {
		this.refundTrace = refundTrace;
	}

	public String getRefundTraceDesc() {
		return refundTraceDesc;
	}

	public void setRefundTraceDesc(String refundTraceDesc) {
		this.refundTraceDesc = refundTraceDesc;
	}

	public String getFullRespStr() {
		return fullRespStr;
	}

	public void setFullRespStr(String fullRespStr) {
		this.fullRespStr = fullRespStr;
	}

}
