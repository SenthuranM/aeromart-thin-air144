package com.isa.thinair.paymentbroker.api.service;

import java.math.BigDecimal;

import java.math.BigDecimal;
import com.isa.thinair.airreservation.api.dto.PaymentMethodDetailsDTO;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.isa.thinair.airreservation.api.dto.PaymentMethodDetailsDTO;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.CountryPaymentCardBehaviourDTO;
import com.isa.thinair.paymentbroker.api.dto.CreditCardPaymentStatusDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Srikantha
 */
public interface PaymentBrokerBD {

	public static final String SERVICE_NAME = "PaymentBrokerService";

	public static interface PaymentBrokerProperties {

		public String KEY_BROKER_TYPE = "brokerType";

		public String KEY_SUPPORT_OFFLINE_PAYMENT = "supportOffLinePayment";

		public String KEY_IPG_URL = "ipgURL";

		public String KEY_REQUEST_METHOD = "requestMethod";

		public String KEY_RESPONSE_TYPE_XML = "xmlResponse";

		public String KEY_SWITCH_TO_EXTERNAL_URL = "switchToExternalURL";

		public String KEY_VIEW_PAYMENT_IFRAME = "viewPaymentInIframe";

		public static interface BrokerTypes {
			/**
			 * CC Capture happens in AccelAero and payment gateway handshaking happens without transparent to the user
			 */
			public String BROKER_TYPE_INTERNAL = "internal";

			/**
			 * CC Capture happens in AccelAero and payment gateway handshaking happens with transparent to the user
			 */
			public String BROKER_TYPE_INTERNAL_EXTERNAL = "internal-external";

			/**
			 * CC Capture and handshaking happens in payment gateway with transparent to the user
			 */
			public String BROKER_TYPE_EXTERNAL = "external";
			/**
			 * CC Capture happens in AccelAero with 3D Secure payment gateway handshaking
			 * 
			 */
			public String BROKER_TYPE_INTERNAL_3DEXTERNAL = "internal-3dexternal";
		}

		public enum REQUEST_METHODS {
			GET, POST, PUT
		};
	}

	/**
	 * Refund the customer
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException;

	/**
	 * Charge the customer
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException;

	/**
	 * Reverse a payment
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException;

	/**
	 * Capture a payment
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce capture(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException;

	/**
	 * Returns properties of Payment broker
	 * 
	 * 
	 */
	public Properties getProperties(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException;

	/**
	 * Returns the request data with auditing the request
	 * 
	 * @param ipgRequestDTO
	 * @return
	 */
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException;

	/**
	 * 
	 * @param ipgRequestDTO
	 * @param configDataList
	 * @return
	 * @throws ModuleException
	 */
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException;

	/**
	 * Returns the RESPONSE with auditing the response
	 * 
	 * @param receiptyMap
	 * @param ipgResponseDTO
	 * @param paymentGateway
	 * @return
	 * @throws ModuleException
	 */
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException;

	/**
	 * Save the REQUEST
	 * 
	 * @param currentState
	 * @param paymentBrokerRefNo
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	public void saveRequest(String currentState, int paymentBrokerRefNo, String request) throws ModuleException;

	/**
	 * Save the RESPONSE
	 * 
	 * @param currentState
	 * @param paymentBrokerRefNo
	 * @param response
	 * @param tnxResultCode
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public void
			saveResponse(String currentState, int paymentBrokerRefNo, String response, String transactionId, int tnxResultCode)
					throws ModuleException;

	/**
	 * Save the RESPONSE
	 * 
	 * @param currentState
	 * @param paymentBrokerRefNo
	 * @param response
	 * @param tnxResultCode
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public CreditCardTransaction auditTransaction(String pnr, String merchantId, String merchantTnxId, Integer temporyPaymentId,
			String serviceType, String strRequest, String strResponse, String paymentGatwayName, String transactionId,
			boolean isSuccess) throws ModuleException;

	/**
	 *
	 * @param creditCardPaymentStatusDTO
	 * @param ipgIdentificationParamsDTO
	 * @return
	 * @throws ModuleException
	 */
	public CreditCardTransaction auditCCTransaction(CreditCardPaymentStatusDTO creditCardPaymentStatusDTO,IPGIdentificationParamsDTO ipgIdentificationParamsDTO)
			throws ModuleException;

	/**
	 * Save the RESPONSE
	 * 
	 * @param transactionRefNo
	 * @param plainTextReceipt
	 * @param errorSpecification
	 * @return
	 * @throws ModuleException
	 */
	public void updateAuditTransactionByTnxRefNo(int transactionRefNo, String plainTextReceipt, String errorSpecification,
			String authorizationCode, String transactionId, int tnxResultCode) throws ModuleException;

	/**
	 * Save the RESPONSE
	 * 
	 * @param oCreditCardTransaction
	 * @param
	 * @param
	 * @return
	 * @throws ModuleException
	 */
	public void saveTransaction(CreditCardTransaction oCreditCardTransaction) throws ModuleException;

	/**
	 * Resolves partial payments [Invokes via a scheduler operation]
	 * 
	 * @param colIPGQueryDTO
	 *            collection of IPGQueryDTO
	 * @param refundFlag
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException;

	/**
	 * THIS METHOD IS USED TO REVERSE VOUCHER PAYMENTS IF VOUCHERS ARE NOT CREATED
	 *
	 * @param colIPGQueryDTO
	 * @param refundFlag
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce resolvePartialPaymentsForVouchers(Collection colIPGQueryDTO, boolean refundFlag)
			throws ModuleException;


	/**
	 * Captures the status response
	 * 
	 * @param receiptyMap
	 * @param paymentGateway
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce captureStatusResponse(Map receiptyMap, IPGIdentificationParamsDTO ipgIdentificationParamsDTO)
			throws ModuleException;

	/**
	 * Validates IPGIdentificationParamsDTO
	 * 
	 * @param ipgIdentifierRequestDTO
	 * @return
	 * @throws ModuleException
	 */
	public void validateIPGIdentificationParams(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException;

	/**
	 * Returns all the payment gateways having provision for refunding payments (of unsuccessful booking) by scheduler
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Collection getSchedulerRefundEnabledIPGs() throws ModuleException;

	/**
	 * checks whether any IPG available with given parameters.
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public boolean checkForIPG(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException;
	
	public String checkForIPGPerPGWId(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException;

	/**
	 * Returns the payment gateway available for the currency chosen.
	 * 
	 * @return Map
	 * @throws ModuleException
	 */
	public Map getPaymentOptions(IPGPaymentOptionDTO ipgPaymentOptionDTO) throws ModuleException;

	/**
	 * Returns the payment gateway available .
	 * 
	 * @param ipgPaymentOptionDTO
	 * @return List
	 * @throws ModuleException
	 */
	public List<IPGPaymentOptionDTO> getPaymentGateways(IPGPaymentOptionDTO ipgPaymentOptionDTO) throws ModuleException;

	/**
	 * Returns the payment gateway available .
	 * 
	 * @return List
	 * @throws ModuleException
	 */
	public List getPaymentGateways(String modulecode) throws ModuleException;

	/**
	 * Returns the payment gateway available .
	 * 
	 * @return List
	 * @throws ModuleException
	 */
	public List getPaymentGateways(Integer ipgId) throws ModuleException;

	/**
	 * 
	 * @param transactionRefNo
	 * @return
	 */
	public Map<Integer, CreditCardTransaction> loadCreditCardTransactionMap(String transactionRefNo);

	/**
	 * Returns the payment having support for given currency or not .
	 * 
	 * @return List
	 * @throws ModuleException
	 */
	public boolean isPaymentGatewaySupportsCurrency(Integer ipgId, String currencyCode) throws ModuleException;

	/**
	 * Returns the payment having support for given currency or not .
	 * 
	 * @param module
	 *            TODO
	 * 
	 * @return List
	 * @throws ModuleException
	 */
	public boolean isPaymentGatewaySupportedCurrency(String currencyCode, ApplicationEngine module) throws ModuleException;

	/**
	 * Return payment gateway name for the cc transaction.
	 * 
	 * @param isOwnTnx
	 *            TODO
	 * @return List
	 * @throws ModuleException
	 */
	public String getPaymentGatewayNameForCCTransaction(Integer tnxId, boolean isOwnTnx) throws ModuleException;

	public String getPaymentGatewayNameForLCCCCTnx(Integer tptId) throws ModuleException;

	public boolean isRefundWithoutCardDetailsEnabled(String ipgName) throws ModuleException;

	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException;

	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException;

	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException;

	public CreditCardTransaction loadTransactionByTempPayId(int tempPayId) throws ModuleException;
	
	public CreditCardTransaction loadTransactionByReference(String transactionReference) throws ModuleException;

	/**
	 * 
	 * @param paymentGatewayID
	 * @return
	 */
	public List<CardDetailConfigDTO> getPaymentGatewayCardConfigData(String paymentGatewayID) throws ModuleException;

	public Collection getContryPaymentCardBehavior(String countryCode, Collection paymentGatewayIDs) throws ModuleException;

	public Collection<CountryPaymentCardBehaviourDTO> getONDWiseCountryCardBehavior(String ondCode, Collection paymentGatewayIDs) throws ModuleException;

	public Collection getCreditCardsListForAgent(Collection paymentGatewayIDs) throws ModuleException;

	/**
	 * Returns the XMLResponse after parsing the XML
	 * 
	 * @param ipgRequestDTO
	 * @return
	 */
	public XMLResponseDTO getXMLResponse(IPGIdentificationParamsDTO ipgIdentificationParamsDTO, Map postDataMap)
			throws ModuleException;

	/**
	 * Get Payments available payments methods
	 * 
	 * @param countryCode
	 * @return
	 * @throws ModuleException 
	 */
	public Set<String> getPaymentMethodNames(PaymentMethodDetailsDTO paymentMethodDetailsDTO) throws ModuleException;


	/**
	 * Handle Deferred payment response
	 * 
	 * @param receiptyMap
	 * @param ipgResponseDTO
	 * @param ipgIdentificationParamsDTO
	 * @throws ModuleException
	 */
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException;
	
	/**
	 * Return payment gateway configs to a given provider name (i.e Ogone)
	 * 
	 * @param providerName
	 * @return List<IPGPaymentOptionDTO>
	 */
	public List<IPGPaymentOptionDTO> getActivePaymentGatewayByProviderName(String providerName);

	public boolean isCreditCardFeeFromPGW(Integer ipgId) throws ModuleException;
	
	public String getPaymentGatewayNameFromCCTransaction(int temporyPayId);
	
	public List getPaymentGatewaysForAccountTab(Integer ipgId) throws ModuleException;
	
	public CreditCardTransaction loadOfflineTransactionByReference(String transactionReference) throws ModuleException;
	
	public CreditCardTransaction loadOfflineConfirmedTransactionByReference(String transactionReference) throws ModuleException;
	
	public BigDecimal getPaymentAmountFromTPTId(Integer tempTnxId) throws ModuleException;

	public boolean isOfflinePaymentsCancellationAllowed(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException;

	public IPGRequestResultsDTO  getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException;
	
}
