package com.isa.thinair.paymentbroker.api.dto;

import java.io.Serializable;

/**
 * 
 * @author pkarunanayake
 * 
 */

public class CardDetailConfigDTO implements Serializable {

	private static final long serialVersionUID = -7268787840296726136L;

	private String fieldName;

	private String whatToStore;

	private String whatToDisplay;

	public String getFieldName() {
		return fieldName;
	}

	public String getWhatToStore() {
		return whatToStore;
	}

	public String getWhatToDisplay() {
		return whatToDisplay;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setWhatToStore(String whatToStore) {
		this.whatToStore = whatToStore;
	}

	public void setWhatToDisplay(String whatToDisplay) {
		this.whatToDisplay = whatToDisplay;
	}

}
