package com.isa.thinair.paymentbroker.api.dto;

import java.io.Serializable;

import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Captures parameters for Payment Gateway identification.
 */
public class IPGIdentificationParamsDTO implements Serializable {

	private static final long serialVersionUID = -1754215725993914727L;

	private Integer ipgId = null;

	private String paymentCurrencyCode = null;

	private String fullyQualifiedIPGConfigurationName = null;

	private boolean _isIPGConfigurationExists = false;

	public IPGIdentificationParamsDTO(Integer ipgId, String paymentCurrencyCode) {
		this.ipgId = ipgId;
		setPaymentCurrencyCode(paymentCurrencyCode);
		set_isIPGConfigurationExists(false);
	}

	public IPGIdentificationParamsDTO(String fullyQualifiedIPGConfigurationName) {
		setFQIPGConfigurationName(fullyQualifiedIPGConfigurationName);
		set_isIPGConfigurationExists(false);
	}

	/**
	 * @return the paymentCurrencyCode
	 */
	public String getPaymentCurrencyCode() {
		return paymentCurrencyCode;
	}

	/**
	 * @param paymentCurrencyCode
	 *            the paymentCurrencyCode to set
	 */
	private void setPaymentCurrencyCode(String paymentCurrencyCode) {
		this.paymentCurrencyCode = paymentCurrencyCode;
	}

	/**
	 * @return the validatedIPGConfigurationName
	 */
	public String getFQIPGConfigurationName() {
		return fullyQualifiedIPGConfigurationName;
	}

	/**
	 * @param validatedIPGConfigurationName
	 *            the validatedIPGConfigurationName to set
	 */
	public void setFQIPGConfigurationName(String fullyQualifiedIPGConfigurationName) {
		this.fullyQualifiedIPGConfigurationName = fullyQualifiedIPGConfigurationName;
	}

	/**
	 * @return the _isIPGConfigurationExists
	 */
	public boolean is_isIPGConfigurationExists() {
		return _isIPGConfigurationExists;
	}

	/**
	 * @param configurationExists
	 *            the _isIPGConfigurationExists to set
	 */
	public void set_isIPGConfigurationExists(boolean configurationExists) {
		_isIPGConfigurationExists = configurationExists;
	}

	public Integer getIpgId() {
		return ipgId;
	}

	public void setIpgId(Integer ipgId) {
		this.ipgId = ipgId;
	}

	public int hashCode() {
		String hashStr = "";
		if (getFQIPGConfigurationName() != null) {
			hashStr = getFQIPGConfigurationName();
		} else {
			hashStr += getIpgId() != null ? getIpgId().toString() : "";
			hashStr += getPaymentCurrencyCode() != null ? getPaymentCurrencyCode() : "";
		}
		return new HashCodeBuilder().append(hashStr).toHashCode();
	}

	public boolean equals(Object o) {
		boolean isEquals = false;
		if (o != null) {
			if (o instanceof IPGIdentificationParamsDTO) {
				IPGIdentificationParamsDTO oIPG = (IPGIdentificationParamsDTO) o;
				if (oIPG.getFQIPGConfigurationName() != null && this.getFQIPGConfigurationName() != null) {
					if (oIPG.getFQIPGConfigurationName().equals(this.getFQIPGConfigurationName())) {
						isEquals = true;
					}
				} else {
					if ((this.getIpgId() != null && this.getIpgId().toString().equals(oIPG.getIpgId().toString()))
							&& (this.getPaymentCurrencyCode() != null && this.getPaymentCurrencyCode().equals(
									oIPG.getPaymentCurrencyCode()))) {
						isEquals = true;
					}
				}
			} else {
				isEquals = false;
			}
		}
		return isEquals;
	}

	public IPGIdentificationParamsDTO clone() {
		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(this.getFQIPGConfigurationName());
		ipgIdentificationParamsDTO.setIpgId(this.getIpgId());
		ipgIdentificationParamsDTO.setPaymentCurrencyCode(this.getPaymentCurrencyCode());
		return ipgIdentificationParamsDTO;
	}
}
