package com.isa.thinair.paymentbroker.api.model;

import java.io.Serializable;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : Srikantha
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_CCARD_PAYMENT_STATUS"
 * 
 *                  CreditCardTransaction is the entity class to repesent T_CCARD_PAYMENT_STATUS table
 * 
 */
public class CreditCardTransaction extends Persistent implements Serializable {

	private static final long serialVersionUID = 409095528300045803L;

	private int transactionRefNo;

	private String transactionTimestamp;

	private String transactionRefCccompany;

	private int transactionResultCode;

	private String aidCccompnay;

	private String avsResult;

	private int resultCode;

	private String transactionId;

	private String resultCodeString;

	private int request;

	private String errorSpecification;

	private String transactionReference;

	private String cvvResult;

	private String tempReferenceNum;

	private Integer temporyPaymentId;

	private String serviceType;

	private String requestText;

	private String responseText;

	private String paymentGatewayConfigurationName;

	private String comments;

	private Long responseTime;

	private String intermediateRequest1;

	private String intermediateRequest2;

	private String intermediateResponse1;

	private String intermediateResponse2;
	
	private String gatewayPaymentID;
	
	private String deferredResponseText;

	/**
	 * returns the transactionRefNo (PK_CCARD_PAYMENT_STATUS)
	 * 
	 * @return Returns the transactionRefNo.
	 * @hibernate.id column = "TRANSACTION_REF_NO" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_CCARD_PAYMENT_STATUS"
	 */
	public int getTransactionRefNo() {
		return transactionRefNo;
	}

	/**
	 * sets the transactionRefNo
	 * 
	 * @param transactionRefNo
	 *            The transactionRefNo to set.
	 */
	public void setTransactionRefNo(int transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	/**
	 * returns the aidCccompnay
	 * 
	 * @return Returns the aidCccompnay.
	 * @hibernate.property column = "AID_CCCOMPNAY"
	 */
	public String getAidCccompnay() {
		return aidCccompnay;
	}

	/**
	 * sets the aidCccompnay
	 * 
	 * @param aidCccompnay
	 *            The aidCccompnay to set.
	 */
	public void setAidCccompnay(String aidCccompnay) {
		this.aidCccompnay = aidCccompnay;
	}

	/**
	 * returns the avsResult
	 * 
	 * @return Returns the avsResult.
	 * @hibernate.property column = "AVS_RESULT"
	 */
	public String getAvsResult() {
		return avsResult;
	}

	/**
	 * sets the avsResult
	 * 
	 * @param avsResult
	 *            The avsResult to set.
	 */
	public void setAvsResult(String avsResult) {
		this.avsResult = avsResult;
	}

	/**
	 * returns the errorSpecification
	 * 
	 * @return Returns the errorSpecification.
	 * @hibernate.property column = "ERROR_SPECIFICATION"
	 */
	public String getErrorSpecification() {
		return errorSpecification;
	}

	/**
	 * sets the errorSpecification
	 * 
	 * @param errorSpecification
	 *            The errorSpecification to set.
	 */
	public void setErrorSpecification(String errorSpecification) {
		this.errorSpecification = errorSpecification;
	}

	/**
	 * returns the request
	 * 
	 * @return Returns the request.
	 * @hibernate.property column = "REQUEST"
	 */
	public int getRequest() {
		return request;
	}

	/**
	 * sets the request
	 * 
	 * @param request
	 *            The request to set.
	 */
	public void setRequest(int request) {
		this.request = request;
	}

	/**
	 * returns the resultCode
	 * 
	 * @return Returns the resultCode.
	 * @hibernate.property column = "RESULT_CODE"
	 */
	public int getResultCode() {
		return resultCode;
	}

	/**
	 * sets the resultCode
	 * 
	 * @param resultCode
	 *            The resultCode to set.
	 */
	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}

	/**
	 * returns the resultCodeString
	 * 
	 * @return Returns the resultCodeString.
	 * @hibernate.property column = "RESULT_CODE_STRING"
	 */
	public String getResultCodeString() {
		return resultCodeString;
	}

	/**
	 * sets the resultCodeString
	 * 
	 * @param resultCodeString
	 *            The resultCodeString to set.
	 */
	public void setResultCodeString(String resultCodeString) {
		this.resultCodeString = resultCodeString;
	}

	/**
	 * returns the transactionResultCode
	 * 
	 * @return Returns the transactionResultCode.
	 * @hibernate.property column = "TRANSACTION_RESULT_CODE"
	 */
	public int getTransactionResultCode() {
		return transactionResultCode;
	}

	/**
	 * sets the transactionResultCode
	 * 
	 * @param transactionResultCode
	 *            The transactionResultCode to set.
	 */
	public void setTransactionResultCode(int transactionResultCode) {
		this.transactionResultCode = transactionResultCode;
	}

	/**
	 * returns the transactionId
	 * 
	 * @return Returns the transactionId.
	 * @hibernate.property column = "TRANSACTION_ID"
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * sets the transactionId
	 * 
	 * @param transactionId
	 *            The transactionId to set.
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * returns the transactionRefCccompany
	 * 
	 * @return Returns the transactionRefCccompany.
	 * @hibernate.property column = "TRANSACTION_REF_CCCOMPANY"
	 */
	public String getTransactionRefCccompany() {
		return transactionRefCccompany;
	}

	/**
	 * sets the transactionRefCccompany
	 * 
	 * @param transactionRefCccompany
	 *            The transactionRefCccompany to set.
	 */
	public void setTransactionRefCccompany(String transactionRefCccompany) {
		this.transactionRefCccompany = transactionRefCccompany;
	}

	/**
	 * returns the transactionTimestamp
	 * 
	 * @return Returns the transactionTimestamp.
	 * @hibernate.property column = "TRANSACTION_TIMESTAMP"
	 */
	public String getTransactionTimestamp() {
		return transactionTimestamp;
	}

	/**
	 * sets the status
	 * 
	 * @param transactionTimestamp
	 *            The transactionTimestamp to set.
	 */
	public void setTransactionTimestamp(String transactionTimestamp) {
		this.transactionTimestamp = transactionTimestamp;
	}

	/**
	 * returns the transactionReference <br>
	 * <b>********** PNR Number ********</b>
	 * 
	 * @return Returns the transactionReference.
	 * @hibernate.property column = "TRANSACTION_REFERENCE"
	 */
	public String getTransactionReference() {
		return transactionReference;
	}

	/**
	 * sets the status
	 * 
	 * @param transactionReference
	 *            The transactionReference to set.
	 */
	public void setTransactionReference(String transactionReference) {
		this.transactionReference = transactionReference;
	}

	/**
	 * returns the cvvResult
	 * 
	 * @return Returns the cvvResult.
	 * @hibernate.property column = "CVV_RESULT"
	 */
	public String getCvvResult() {
		return cvvResult;
	}

	/**
	 * sets the status
	 * 
	 * @param cvvResult
	 *            The cvvResult to set.
	 */
	public void setCvvResult(String cvvResult) {
		this.cvvResult = cvvResult;
	}

	/**
	 * returns the tempReferenceNum <br>
	 * <b>**** Merchant ID *******</b>
	 * 
	 * @return Returns the tempReferenceNum.
	 * @hibernate.property column = "NM_TRANSACTION_REFERENCE"
	 */
	public String getTempReferenceNum() {
		return tempReferenceNum;
	}

	/**
	 * sets the status
	 * 
	 * @param tempReferenceNum
	 *            The tempReferenceNum to set.
	 */
	public void setTempReferenceNum(String tempReferenceNum) {
		this.tempReferenceNum = tempReferenceNum;
	}

	/**
	 * <br>
	 * <b>******** TPT_ID ***********</b> <br>
	 * 
	 * @return Returns the temporyPaymentId.
	 * @hibernate.property column = "TPT_ID"
	 */
	public Integer getTemporyPaymentId() {
		return temporyPaymentId;
	}

	/**
	 * @param temporyPaymentId
	 *            The temporyPaymentId to set.
	 */
	public void setTemporyPaymentId(Integer temporyPaymentId) {
		this.temporyPaymentId = temporyPaymentId;
	}

	/**
	 * @return Returns the serviceType.
	 * @hibernate.property column = "SERVICE_TYPE"
	 */
	public String getServiceType() {
		return serviceType;
	}

	/**
	 * @param serviceType
	 *            The serviceType to set.
	 */
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	/**
	 * @return Returns the responseText.
	 * @hibernate.property column = "RESPONSE_TEXT"
	 */
	public String getResponseText() {
		return responseText;
	}

	/**
	 * @param responseText
	 *            The responseText to set.
	 */
	public void setResponseText(String responseText) {
		this.responseText = responseText;
	}

	/**
	 * @return Returns the requestText.
	 * @hibernate.property column = "REQUEST_TEXT"
	 */
	public String getRequestText() {
		return requestText;
	}

	/**
	 * @param requestText
	 *            The requestText to set.
	 */
	public void setRequestText(String requestText) {
		this.requestText = requestText;
	}

	/**
	 * @return Returns the paymentGatewayName.
	 * @hibernate.property column = "GATEWAY_NAME"
	 */
	public String getPaymentGatewayConfigurationName() {
		return paymentGatewayConfigurationName;
	}

	/**
	 * @param paymentGatewayConfigurationName
	 *            The paymentGatewayName to set.
	 */
	public void setPaymentGatewayConfigurationName(String paymentGatewayConfigurationName) {
		this.paymentGatewayConfigurationName = paymentGatewayConfigurationName;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "COMMENTS"
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * 
	 * @param comments
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "RESPONSETIME"
	 */
	public Long getResponseTime() {
		return responseTime;
	}

	/**
	 * 
	 * @param responseTime
	 */
	public void setResponseTime(Long responseTime) {
		this.responseTime = responseTime;
	}

	/**
	 * 
	 * @param intermediateRequest1
	 */
	public void setIntermediateRequest1(String intermediateRequest1) {
		this.intermediateRequest1 = intermediateRequest1;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "INTERMEDIATE_REQUEST_1"
	 */
	public String getIntermediateRequest1() {
		return intermediateRequest1;
	}

	/**
	 * 
	 * @param intermediateRequest2
	 */
	public void setIntermediateRequest2(String intermediateRequest2) {
		this.intermediateRequest2 = intermediateRequest2;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "INTERMEDIATE_REQUEST_2"
	 */
	public String getIntermediateRequest2() {
		return intermediateRequest2;
	}

	/**
	 * 
	 * @param intermediateResponse1
	 */
	public void setIntermediateResponse1(String intermediateResponse1) {
		this.intermediateResponse1 = intermediateResponse1;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "INTERMEDIATE_RESPONSE_1"
	 */
	public String getIntermediateResponse1() {
		return intermediateResponse1;
	}

	/**
	 * 
	 * @param intermediateResponse2
	 */
	public void setIntermediateResponse2(String intermediateResponse2) {
		this.intermediateResponse2 = intermediateResponse2;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "INTERMEDIATE_RESPONSE_2"
	 */
	public String getIntermediateResponse2() {
		return intermediateResponse2;
	}
	
	/**
	 * 
	 * @return
	 * @hibernate.property column = "GATEWAY_PAYMENT_ID"
	 */
	public String getGatewayPaymentID() {
		return gatewayPaymentID;
	}

	public void setGatewayPaymentID(String gatewayPaymentID) {
		this.gatewayPaymentID = gatewayPaymentID;
	}
	/**
	 * @return Returns the deferredResponseText.
	 * @hibernate.property column = "DEFERRED_RESPONSE_TEXT"
	 */
	public String getDeferredResponseText() {
		return deferredResponseText;
	}
	/**
	 * @param deferredResponseText
	 *            The deferredResponseText to set.
	 */
	public void setDeferredResponseText(String deferredResponseText) {
		this.deferredResponseText = deferredResponseText;
	}
	
}
