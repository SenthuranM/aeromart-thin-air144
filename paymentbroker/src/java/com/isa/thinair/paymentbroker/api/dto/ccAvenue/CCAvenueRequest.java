package com.isa.thinair.paymentbroker.api.dto.ccAvenue;

public class CCAvenueRequest{
	
	public static final String AA_CCAVENUE_PROVIDER_CODE = "CCAvenue";
	
	public static final String MERCHANT_ID = "merchant_id"; 
	public static final String ORDER_ID = "order_id"; 
	public static final String CURRENCY = "currency"; 
	public static final String AMOUNT = "amount"; 
	public static final String REDIRECT_URL = "redirect_url"; 
	public static final String CANCEL_URL = "cancel_url"; 
	public static final String LANGUAGE = "language"; 
	public static final String ENC_REQUEST = "encRequest";
	public static final String ACCESS_CODE = "access_code";
	public static final String CCAVENUE_CARD_TYPE = "card_type";
	public static final String CCAVENUE_PAYMENT_OPTION = "payment_option";
	
	// optional
	public static final String BILLING_NAME = "billing_name"; // customer name
	public static final String BILLING_EMAIL = "billing_email";
	public static final String BILLING_COUNTRY = "billing_country";
	public static final String BILLING_ADDRESS = "billing_address"; // customer name
	public static final String BILLING_TEL = "billing_tel"; // customer phone
	public static final String MERCHANT_PARAM1 = "merchant_param1"; // pnr
	public static final String MERCHANT_PARAM2 = "merchant_param2"; // ipg id & pay currency
	
	public static final String MERCHANT_PARAM_DELIMETER = "MPD";

	
	public static final String ENCRYPTION_KEY = "encryptionKey";
	public static final String COMMAND_INITIATE_TRANS = "initiateTransaction";
	public static final String COMMAND_CONFIRM_ORDER = "orderStatusTracker";
	public static final String COMMAND_ORDER_LOOKUP = "orderLookup";
	public static final String COMMAND_REFUND = "refundOrder";
	public static final String COMMAND = "command";
	public static final String OK = "0";
	
	//actions
	public static final String CONFIRM_ACTION = "confirm";
	
	// server to server xml resquest
	public static final String S2S_REQUEST_URL = "serverToServerRequestURL";
	public static final String S2S_REQUEST_ENC_XML = "enc_request";
	public static final String S2S_REQUEST_ACCESS_CODE = "access_code";
	
	//response params
	public static final String TRACKING_ID = "tracking_id";
	public static final String ORDER_STATUS = "order_status";
	public static final String FAILURE_MESSAGE = "failure_message";
	public static final String PAYMENT_MODE = "payment_mode";
	public static final String CARD_NAME = "card_name";
	public static final String STATUS_CODE = "status_code";
	public static final String STATUS_MESSAGE = "status_message";
	public static final String BANK_REF_NO = "bank_ref_no";
	
	private String encryptionKey;
	private String accessCode;
	private String merchantId;
	private String orderId;
	private String amount;
	private String serverToServerURL;
	private String command;
	private String trackingId;
	private String refundId;
	private String transactionTimestamp;
	
	@Override
	public String toString(){
		String toStr = "";
		if(amount != null){
			toStr += "Amount : " + amount;
		}
		if(command != null){
			toStr += "\n Command : " + command;
		}
		if(trackingId != null){
			toStr += "\n Tracking ID : " + trackingId;
		}
		if(orderId != null){
			toStr += "\n Order ID : " + orderId;
		}		
		if(refundId != null){
			toStr += "\n Refund ID : " + refundId;
		}	
		if(serverToServerURL != null){
			toStr += "\n server To Server URL : " + serverToServerURL;
		}
		
		return toStr;
	}
	
	/**
	 * @return the encryptionKey
	 */
	public String getEncryptionKey() {
		return encryptionKey;
	}
	/**
	 * @param encryptionKey the encryptionKey to set
	 */
	public void setEncryptionKey(String encryptionKey) {
		this.encryptionKey = encryptionKey;
	}
	/**
	 * @return the accessCode
	 */
	public String getAccessCode() {
		return accessCode;
	}
	/**
	 * @param accessCode the accessCode to set
	 */
	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}
	/**
	 * @return the merchantId
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * @param merchantId the merchantId to set
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
	/**
	 * @return the serverToServerURL
	 */
	public String getServerToServerURL() {
		return serverToServerURL;
	}
	/**
	 * @param serverToServerURL the serverToServerURL to set
	 */
	public void setServerToServerURL(String serverToServerURL) {
		this.serverToServerURL = serverToServerURL;
	}
	/**
	 * @return the command
	 */
	public String getCommand() {
		return command;
	}
	/**
	 * @param command the command to set
	 */
	public void setCommand(String command) {
		this.command = command;
	}
	/**
	 * @return the trackingId
	 */
	public String getTrackingId() {
		return trackingId;
	}
	/**
	 * @param trackingId the trackingId to set
	 */
	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}

	/**
	 * @return the refundId
	 */
	public String getRefundId() {
		return refundId;
	}

	/**
	 * @param refundId the refundId to set
	 */
	public void setRefundId(String refundId) {
		this.refundId = refundId;
	}

	public String getTransactionTimestamp() {
		return transactionTimestamp;
	}

	public void setTransactionTimestamp(String transactionTimestamp) {
		this.transactionTimestamp = transactionTimestamp;
	}
	
}