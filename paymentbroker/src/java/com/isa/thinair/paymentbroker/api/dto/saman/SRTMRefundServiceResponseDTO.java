package com.isa.thinair.paymentbroker.api.dto.saman;

import java.io.Serializable;

public class SRTMRefundServiceResponseDTO implements Serializable {

	private static final long serialVersionUID = -8303009098978356L;

	private String actionName;

    private String description;

    private String errorCode;

    private String errorMessage;

    private Long referenceId;

    private Integer requestStatus;
    
    private int transactionRefNo;

	public String getActionName() {
		return actionName;
	}


	public void setActionName(String actionName) {
		this.actionName = actionName;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getErrorCode() {
		return errorCode;
	}


	public void setErrorCode(
			String errorCode) {
		this.errorCode = errorCode;
	}


	public String getErrorMessage() {
		return errorMessage;
	}


	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}


	public Long getReferenceId() {
		return referenceId;
	}


	public void setReferenceId(Long referenceId) {
		this.referenceId = referenceId;
	}


	public Integer getRequestStatus() {
		return requestStatus;
	}


	public void setRequestStatus(Integer requestStatus) {
		this.requestStatus = requestStatus;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getTransactionRefNo() {
		return transactionRefNo;
	}

	
	public void setTransactionRefNo(int transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("actionName:" + actionName).append(",description:" + description)
				.append(",errorCode:" + errorCode).append(",errorMessage:" + errorMessage)
				.append(",referenceId:" + referenceId).append(",requestStatus:" + requestStatus).append(",transactionRefNo:" + transactionRefNo);

		return builder.toString();
	}

}
