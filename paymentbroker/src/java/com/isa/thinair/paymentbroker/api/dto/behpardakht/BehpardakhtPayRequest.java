package com.isa.thinair.paymentbroker.api.dto.behpardakht;

import java.io.Serializable;

public class BehpardakhtPayRequest extends BehpardakhtCommonRequest implements Serializable {

	private static final long serialVersionUID = -190909098464586L;

	public static final String AMOUNT = "amount";
	public static final String LOCAL_DATE = "localDate";
	public static final String LOCAL_TIME = "localTime";
	public static final String ADDITIONAL_DATA = "additionalData";
	public static final String CALLBACK_URL = "callBack_Url";
	public static final String PAYER_ID = "payer_Id";

	private Long amount;
	private String localDate;
	private String localTime;
	private String additionalData;
	private String callBackUrl;
	private Long payerId;

	// TO-DO
	// need to override the toString method and write the javadocs

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getLocalDate() {
		return localDate;
	}

	public void setLocalDate(String localDate) {
		this.localDate = localDate;
	}

	public String getLocalTime() {
		return localTime;
	}

	public void setLocalTime(String localTime) {
		this.localTime = localTime;
	}

	public String getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalData(String additionalData) {
		this.additionalData = additionalData;
	}

	public String getCallBackUrl() {
		return callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}

	public Long getPayerId() {
		return payerId;
	}

	public void setPayerId(Long payerId) {
		this.payerId = payerId;
	}

	public String toString() {
		StringBuffer resBuff = new StringBuffer();
		resBuff.append("Behpardakht BehpardakhtInquiryRequest Details [");
		resBuff.append(super.toString());
		resBuff.append("amount: " + this.amount);
		resBuff.append(", localDate: " + this.localDate);
		resBuff.append("localTime: " + this.localTime);
		resBuff.append(", additionalData: " + this.additionalData);
		resBuff.append("callBackUrl: " + this.callBackUrl);
		resBuff.append(", payerId: " + this.payerId);
		resBuff.append(" ] ");
		return resBuff.toString();
	}

}
