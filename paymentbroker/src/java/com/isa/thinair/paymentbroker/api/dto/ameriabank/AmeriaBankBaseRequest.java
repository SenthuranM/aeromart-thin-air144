package com.isa.thinair.paymentbroker.api.dto.ameriabank;

import java.io.Serializable;

public class AmeriaBankBaseRequest implements Serializable {

	private static final long serialVersionUID = -190909085994591L;

	private String clientID;
	private String userName;
	private String userPassword;

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder resBuff = new StringBuilder();
		resBuff.append("clientID: " + this.clientID);
		resBuff.append(", userName: " + this.userName);
		// TODO check whether to store password
		// resBuff.append(", userPassword: " + this.userPassword);
		return resBuff.toString();
	}
}
