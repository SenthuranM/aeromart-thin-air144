package com.isa.thinair.paymentbroker.api.dto.ameriabank;

import java.io.Serializable;
import java.math.BigDecimal;

public class AmeriaBankCommonRequest extends AmeriaBankBaseRequest implements Serializable {

	private static final long serialVersionUID = -190909098464586L;

	private Integer orderId;
	private BigDecimal paymentAmount;

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	@Override
	public String toString() {
		StringBuilder resBuff = new StringBuilder();
		resBuff.append(super.toString());
		resBuff.append("orderId: " + this.orderId);
		resBuff.append("paymentAmount: " + this.paymentAmount);
		return resBuff.toString();
	}

}
