package com.isa.thinair.paymentbroker.api.dto.payFort;

import java.io.Serializable;

import javax.xml.datatype.XMLGregorianCalendar;

public class CheckOrderDTO implements Serializable {
	
	// later implments with a common class for Requests
	private static final long serialVersionUID = -8393939998531544L;
	
	private String merchantID;
	
	private String orderID;
	
	private String currency;
	
	private String payment;
	
	private String serviceName;
	
	private String signature;
	
	private XMLGregorianCalendar msgDate;

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public XMLGregorianCalendar getMsgDate() {
		return msgDate;
	}

	public void setMsgDate(XMLGregorianCalendar msgDate) {
		this.msgDate = msgDate;
	}

}
