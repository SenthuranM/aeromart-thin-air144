package com.isa.thinair.paymentbroker.api.dto.tap;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class TapPaymentRequestCallRq implements Serializable {

	private static final long serialVersionUID = -190789478944591L;

	// Customer Details
	private String mobileNo;
	private String name;
	private String email;

	// Product Details
	private String unitName;
	private Integer quantity;
	private BigDecimal unitPrice;
	private BigDecimal totalPrice;
	private String CurrencyCode;

	// Merchant Details
	private Long merchantID;
	private String userName;
	private String returnURL;
	private String referenceID;
	private String hashString;

	// Gateway Details
	private List<String> gatewayList;

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getCurrencyCode() {
		return CurrencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		CurrencyCode = currencyCode;
	}

	public Long getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(Long merchantID) {
		this.merchantID = merchantID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getReturnURL() {
		return returnURL;
	}

	public void setReturnURL(String returnURL) {
		this.returnURL = returnURL;
	}

	public String getReferenceID() {
		return referenceID;
	}

	public void setReferenceID(String referenceID) {
		this.referenceID = referenceID;
	}

	public String getHashString() {
		return hashString;
	}

	public void setHashString(String hashString) {
		this.hashString = hashString;
	}

	public List<String> getGatewayList() {
		return gatewayList;
	}

	public void setGatewayList(List<String> gatewayList) {
		this.gatewayList = gatewayList;
	}

}
