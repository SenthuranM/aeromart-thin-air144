/*
 * ==============================================================================
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.api.util;

import java.util.Set;

import com.google.common.collect.Sets;

/**
 * @author Srikantha
 *
 */
public class PaymentConstants {

	public static final String PAYMENTBROKER_TRANSACTION_MESSAGE = "paymentbroker.success.message";

	public static final String PAYMENTBROKER_TRANSACTION_CODE = "paymentbroker.tran.code";

	public static final String PAYMENTBROKER_RESULTS = "paymentbroker.authentication.resultsets";

	public static final String PAYMENTBROKER_AUTHORIZATION_ID = "paymentbroker.authenticationId";

	public static final String PARAM_CC_LAST_DIGITS = "paymentbroker.lastdigits";

	public static final String PARAM_COMPANY = "company";

	public static final String PARAM_COMPANY_SUB_ID = "company_sub_id";

	public static final String PARAM_COMPANY_SERVICETYPE = "servicetype";

	public static final String PARAM_COMPANY_CARDHOLDER = "cardholder";

	public static final String PARAM_COMPANY_CARDNUMBER = "cardnumber";

	public static final String PARAM_COMPANY_EXPIRY = "expiry";

	public static final String PARAM_COMPANY_AMOUNT = "amount";

	public static final String PARAM_COMPANY_CURRENCY = "currency";

	public static final String PARAM_COMPANY_REFERENCENUM = "referencenum";

	public static final String PARAM_COMPANY_CVV = "cvv";

	public static final String PARAM_COMPANY_TRANSACTIONNR = "transactionnr";

	public static final String PARAM_COMPANY_AID = "aid";

	public static final String PARAM_COMPANY_TRANSREF = "transref";

	public static final String PARAM_COMPANY_CARDHOLDERREF = "cardholderref";

	public static final String PARAM_QUERY_DR_TXN_CODE = "txncode";

	public static final String PARAM_QUERY_DR_STATUS_IP = "IP";

	public static final String PARAM_QUERY_DR_STATUS_II = "II";

	public static final String PARAM_QUERY_DR_STATUS_IS = "IS";

	public static final String PARAM_QUERY_DR_STATUS_IF = "IF";

	public static final String PARAM_QUERY_DR_RESPONSE_OBJECT = "responseObject";
	
	public static final String PARAM_PAYMENT_RESPONSE = "response";

	/**
	 * Constants to hold the parameter key value for indicating a successful refund.
	 */
	public static final String PARAM_SUCCESSFULL_REFUND = "refund.successful";

	public static final int PNR_LENGTH_CARDHOLDERREF = 9;

	public static final int PNR_LENGTH_REFERENCENUM = 9;

	public static final char PNR_LEFT_PAD_CHAR = '0';

	public static final String DUMMY_PAYMENT_AMOUNT = "1";

	public static final String PG_DATA_DELIMITER = "_";

	// NOT the best way, but as a quick solution, error are defined as constants
	public static final String SERVICE_RESPONSE_ERROR_SERVER = "Unable to process payment request, due to service unavailability";

	public static final String SERVICE_RESPONSE_REVERSE_TIME = "Not within the same date time, for reversal";

	public static final String SERVICE_RESPONSE_REVERSE_REFUND = "Transaction reveresed successfully, [for refund]";

	public static final String SERVICE_RESPONSE_REVERSE_VOID = "Transaction reveresed successfully, [for void]";

	public static final String SERVICE_RESPONSE_REVERSE_CANCEL = "Transaction canceled successfully.";

	public static final String SERVICE_RESPONSE_CAPTURE = "Transaction captured successfully ";

	public static final String SERVICE_RESPONSE_QUERYDR = "Transaction queried successfully";

	public static final String SERVICE_RESPONSE_REVERSE_TNX = "Previous transaction details, does not exist for reversal";

	public static final String PREFIX_CARDHOLDERREF = "RES";

	public static final int ASCII_A = 65;

	public static final int ASCII_B = 66;

	public static final char ASCII_C = 67;

	public static final int ASCII_AT = 64;

	public static final int ASCII_Z = 90;

	public static enum MODE_OF_SERVICE {
		PAYMENT, REFUND, VOID, CAPTURE, QUERYDR, STATUS, CANCEL, REFUND_REG, REFUND_EXC, INITIALIZE
	}

	public static final String MSG_CODE_PAYMENTBROKER_NOT_FOUND = "paymentbroker.cannot.found.paymentbroker";

	public static final String IPG_SESSION_ID = "IPG_SESSION_ID";

	public static final String STATUS = "Status";

	public static final String AMOUNT = "Amount";

	public static final String EDIRHAM_AMOUNT = "EDirhamAmount";

	public static final String REQUEST_ID = "requestId";

	public static final String ORDER_ID = "orderId";

	public static final String CURRENCY = "currency";

	public static final String PAYMENT_METHOD = "paymethod";

	public static final int TRANSACTION_RESULT_CODE = 1;

	public static enum MODE_OF_REDIRECTION {
		FORM("FORM"), URL("URL"), FORM_SUBMIT("FORMSUBMIT");
		String value;

		MODE_OF_REDIRECTION(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public enum ServiceType {
		AUTHORIZE("SERVICETYPE_AUTHORIZE"), REEFUND("SERVICETYPE_REFUND"), REVERSAL("SERVICETYPE_REVERSAL"), QUERY(
				"SERVICETYPE_QUERY"), CAPTURE("SERVICETYPE_CAPTURE"), MANUAL_REFUND(
				"SERVICETYPE_MANUAL_REFUND"), CAPTURE_STATUS("SERVICETYPE_CAPTURE_STATUS");
		String code;

		ServiceType(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
	}

	public static enum XML_RESPONSE {

		ORDERID("orderID"), PAYID("PAYID"), NCSTATUS("NCSTATUS"), NCERROR("NCERROR"), NCERRORPLUS(
				"NCERRORPLUS"), ACCEPTANCE("ACCEPTANCE"), STATUS("STATUS"), ECI("ECI"), AMOUNT("amount"), CURRENCY(
				"currency"), PAYMENT_METHOD("PM"), BRAND("BRAND"), ALIAS("ALIAS"), SHAREQUIRED("SHAREQUIRED");

		String value;

		XML_RESPONSE(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	};

	//Status codes for Parsian PGW

	public static final short SUCCESSFUL = 0;

	public static final short PIN_INCORRECT = 22;

	public static final short INVALID_IP = 20;

	public static final short OPERATION_ALREADY_COMPLETED = 30;

	public static final short INVALID_MERCHANT_TXN_ID = 34;

	// constants added for CMI payment gateway

	public static final String CMI_PG_TRNS_TYPE_PRE_AUTH = "preAuth";

	public static final String CMI_PG_RETURN_URL_PATH = "/cmi";

	public static final String CMI_PG_REDIRECT_URL_PATH = "/cmi/ok";

	public static final String CMI_PG_HOST_HOST = "true";

	public static final String REFUND_SUCCESS = "Accepted";

	public static final String PIPE = "|";

	public static final Set<String> EXCEPTONALPARAMETERSET = Sets.newHashSet(
			"encoding", "hash", "storeKey");

	public static final String ENCODING_ALG = "SHA-512";

	public static final String RETURN_CODE = "ProcReturnCode";

	public static final String RETURN_CODE_SUCCESS = "00";
	
	public static final String APPROVED_FOR_VOID="APPROVED";
	
	public static final String APPROVED_POST="ACTION=POSTAUTH";
	
	public static final String VOID_POST="ACTION=VOID";

	public static final String RETURN_CODE_GATEWAY_ERROR = "99";

	public static final String RESPONSE_PARAM = "Response";

	public static final String CMI_TXN = "TransId";

	public static final String PRE_AUTH = "PreAuth";

	public static final String POST_AUTH = "PostAuth";

	public static final String REFUND = "Credit";

	public static final String VOID = "Void";

	public static final String APPROVED = "Approved";

	public static final String DECLINED = "Declined";

	public static final String ERROR = "Error";

	public static final String SERVICETYPE_AUTHORIZE = "SERVICETYPE_AUTHORIZE";

	public static final String SERVICETYPE_CAPTURE = "SERVICETYPE_CAPTURE";

	public static final String SERVICETYPE_REVERSAL = "SERVICETYPE_REVERSAL";

	public static final String SERVICETYPE_REFUND = "SERVICETYPE_REFUND";

	public static final String XML_PARSING_ERROR = "XML parsing error";

	public static final String YEAR_PREFIX = "20";

	public static final String SLASH = "/";

	public static final String CONTENT_TYPE = "Content-Type";

	public static final String CONTENT_LENGTH = "Content-Length";

	public static final String AUTH_CODE = "AuthCode";

	public static final String TEXT_XML = "text/xml";

	public static final String UTF_8 = "UTF-8";
	
	public static final String ORDER_QUERY="QUERY";
	
	public static final Set<String> TRANS_STAT_SUCCESS = Sets.newHashSet(
			"C", "S");
		
	public static final String  CHARGE_TYPE_CD_SUCCESS="S";
	
	public static final String HASH = "#";
	
	public static final String HASH_TEXT = "[HASH]";

        public static final String OTHER_CONTRIES = "OT";
	
	//CMI fatourati
	public static final String DATE_FORMAT = "YYYYMMDDhhmmSS";
	public static final String  REFFATOURATI ="RefFatourati";
	public static final String  CODERETOUR ="CodeRetour";
	public static final String  MSGRETOUR ="MsgRetour";
	public static final String  SUCCESS ="success";
	public static final String  SUCCESS_VALUE="000";
	
	

}
