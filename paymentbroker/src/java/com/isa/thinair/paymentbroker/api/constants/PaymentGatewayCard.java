package com.isa.thinair.paymentbroker.api.constants;

public class PaymentGatewayCard {

	public static final String REPLACE_PATTERN_DIGIT = "\\d";

	public static final String REPLACE_PATTERN_CHAR = "\\S";

	public static final String REPLACE_STRING = "X";

	public enum FieldName {
		CARDNUMBER("CARD_NUMBER"), CARDHOLDERNAME("CARDHOLDER_NAME"), CVV("CVV"), EXPIRYDATE("EXPIRY_DATE");

		private String code;

		private FieldName(String code) {
			this.code = code;
		}

		public String code() {
			return this.code;
		}
	}

	public enum CardNumber {
		FIRSTSIXDIGIT("F6"), LASTFOURDIGIT("L4"), FIRSTSIXDIGITANDLASTFOURDIGIT("F6L4"), ALL("ALL"), NONE("NONE");

		private String code;

		private CardNumber(String code) {
			this.code = code;
		}

		public String code() {
			return this.code;
		}
	}

	public enum CardHolderName {
		ALL("ALL"), NONE("NONE");

		private String code;

		private CardHolderName(String code) {
			this.code = code;
		}

		public String code() {
			return this.code;
		}
	}

	public enum CardCVV {
		ALL("ALL"), NONE("NONE");

		private String code;

		private CardCVV(String code) {
			this.code = code;
		}

		public String code() {
			return this.code;
		}
	}

	public enum CardExpiryDate {
		ALL("ALL"), NONE("NONE");

		private String code;

		private CardExpiryDate(String code) {
			this.code = code;
		}

		public String code() {
			return this.code;
		}
	}

	public enum CardDispalyForm {
		PLAINTEXT("PLAINTEXT"), PROTECTED("CODED");

		private String code;

		private CardDispalyForm(String code) {
			this.code = code;
		}

		public String code() {
			return this.code;
		}
	}

}
