package com.isa.thinair.paymentbroker.api.dto.payFort;

import java.io.Serializable;

import javax.xml.datatype.XMLGregorianCalendar;

public class GetCitiesResponseDTO implements Serializable {
	
	// later implments with a common class for Responses
	private static final long serialVersionUID = -8393939923121544L;
	
	private String merchantID;
	
	private String countryCode;
	
	private String currency;
	
	private Object cities;
	
	private String cityID;
	
	private String 	name;
	
	private String signature;
	
	private XMLGregorianCalendar msgDate;
	
	private Integer statusCode;
	
	private String 	Status;
	
	private String additionalNote;
	
	private String errorCode;
	
	private String errorDesc;

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Object getCities() {
		return cities;
	}

	public void setCities(Object cities) {
		this.cities = cities;
	}

	public String getCityID() {
		return cityID;
	}

	public void setCityID(String cityID) {
		this.cityID = cityID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public XMLGregorianCalendar getMsgDate() {
		return msgDate;
	}

	public void setMsgDate(XMLGregorianCalendar msgDate) {
		this.msgDate = msgDate;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getAdditionalNote() {
		return additionalNote;
	}

	public void setAdditionalNote(String additionalNote) {
		this.additionalNote = additionalNote;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDesc() {
		return errorDesc;
	}

	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

}
