package com.isa.thinair.paymentbroker.api.util;

public class CVVOptions {

	public static final int CARD_CVV_OPTION_MANDATORY = 1;
	public static final int CARD_CVV_OPTION_OPTIONAL = 2;

}
