/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.api.model;

import java.io.Serializable;

import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.TravelDTO;
import com.isa.thinair.paymentbroker.api.dto.paypal.UserInputDTO;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;

/**
 * @author Srikantha
 * 
 */
public class CreditCardPayment implements Serializable {

	private static final long serialVersionUID = 56101275740748351L;

	/** Represents name of the credit card holder */
	private String cardholderName = null;

	/** Represents credit card number */
	private String cardNumber = null;

	/** Represents expiry date of credit card [should be in YYMM format] */
	private String expiryDate = null;

	/** Represents amount to be credited from the credit card */
	private String amount = null;

	/** Represents the the currency format [should be in 3 capital letter character format] */
	private String currency = null;

	/** Represents ccv/cvc field of the credit card backside */
	private String cvvField = null;

	/** Represents card type field of the credit card backside */
	private int cardType;

	/** Payment broker identification parameters */
	IPGIdentificationParamsDTO ipgIdentificationParamsDTO;

	/** Holds the tempory payment id */
	private int temporyPaymentId;

	/** Holds the payment broker reference number */
	private int paymentBrokerRefNo;

	private String pnr = null;

	private AppIndicatorEnum appIndicator;

	private TnxModeEnum tnxMode;

	/** Holds the previous credit card payment information */
	private PreviousCreditCardPayment previousCCPayment;

	private int noOfDecimalPoints = 2;
	/** User IP Address **/
	private String userIP;

	/** Hold fraud checking data **/
	private TravelDTO travelDTO;

	/** Holds the lcc userInputDTO */
	private UserInputDTO userInputDTO;
	
	private boolean offlinePaymentCancel;

	/**
	 * @return String
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return String
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * @param cardNumber
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return String
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return String
	 */
	public String getCvvField() {
		return cvvField;
	}

	/**
	 * @param cvvField
	 */
	public void setCvvField(String cvvField) {
		this.cvvField = cvvField;
	}

	/**
	 * @return String
	 */
	public String getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @param expiryDate
	 */
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * @return String
	 */
	public String getCardholderName() {
		return cardholderName;
	}

	/**
	 * @param cardholderName
	 */
	public void setCardholderName(String cardholderName) {
		this.cardholderName = cardholderName;
	}

	/**
	 * @return Returns the temporyPaymentId.
	 */
	public int getTemporyPaymentId() {
		return temporyPaymentId;
	}

	/**
	 * @param temporyPaymentId
	 *            The temporyPaymentId to set.
	 */
	public void setTemporyPaymentId(int temporyPaymentId) {
		this.temporyPaymentId = temporyPaymentId;
	}

	/**
	 * @return Returns the previousCCPayment.
	 */
	public PreviousCreditCardPayment getPreviousCCPayment() {
		return previousCCPayment;
	}

	/**
	 * @param previousCCPayment
	 *            The previousCCPayment to set.
	 */
	public void setPreviousCCPayment(PreviousCreditCardPayment previousCCPayment) {
		this.previousCCPayment = previousCCPayment;
	}

	/**
	 * @return Returns the appIndicator.
	 */
	public AppIndicatorEnum getAppIndicator() {
		return appIndicator;
	}

	/**
	 * @param appIndicator
	 *            The appIndicator to set.
	 */
	public void setAppIndicator(AppIndicatorEnum appIndicator) {
		this.appIndicator = appIndicator;
	}

	/**
	 * @return Returns the pnr.
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the tnxMode.
	 */
	public TnxModeEnum getTnxMode() {
		return tnxMode;
	}

	/**
	 * @param tnxMode
	 *            The tnxMode to set.
	 */
	public void setTnxMode(TnxModeEnum tnxMode) {
		this.tnxMode = tnxMode;
	}

	/**
	 * @return Returns the paymentBrokerRefNo.
	 */
	public int getPaymentBrokerRefNo() {
		return paymentBrokerRefNo;
	}

	/**
	 * @param paymentBrokerRefNo
	 *            The paymentBrokerRefNo to set.
	 */
	public void setPaymentBrokerRefNo(int paymentBrokerRefNo) {
		this.paymentBrokerRefNo = paymentBrokerRefNo;
	}

	/**
	 * @return the ipgIdentificationParamsDTO
	 */
	public IPGIdentificationParamsDTO getIpgIdentificationParamsDTO() {
		return ipgIdentificationParamsDTO;
	}

	/**
	 * @param ipgIdentificationParamsDTO
	 *            the ipgIdentificationParamsDTO to set
	 */
	public void setIpgIdentificationParamsDTO(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) {
		this.ipgIdentificationParamsDTO = ipgIdentificationParamsDTO;
	}

	/**
	 * @return the noOfDecimalPoints
	 */
	public int getNoOfDecimalPoints() {
		return noOfDecimalPoints;
	}

	/**
	 * @param noOfDecimalPoints
	 *            the noOfDecimalPoints to set
	 */
	public void setNoOfDecimalPoints(int noOfDecimalPoints) {
		this.noOfDecimalPoints = noOfDecimalPoints;
	}

	public Integer getCardType() {
		return cardType;
	}

	public void setCardType(Integer cardType) {
		this.cardType = cardType;
	}

	public String getUserIP() {
		return userIP;
	}

	public void setUserIP(String userIP) {
		this.userIP = userIP;
	}

	public TravelDTO getTravelDTO() {
		return travelDTO;
	}

	public void setTravelDTO(TravelDTO travelDTO) {
		this.travelDTO = travelDTO;
	}

	public void setUserInputDTO(UserInputDTO userInputDTO) {
		this.userInputDTO = userInputDTO;
	}

	public UserInputDTO getUserInputDTO() {
		return userInputDTO;
	}

	public boolean isOfflinePaymentCancel() {
		return offlinePaymentCancel;
	}

	public void setOfflinePaymentCancel(boolean offlinePaymentCancel) {
		this.offlinePaymentCancel = offlinePaymentCancel;
	}
	
	
}
