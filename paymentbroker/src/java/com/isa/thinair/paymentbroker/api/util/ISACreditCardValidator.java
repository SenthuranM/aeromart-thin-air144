/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.api.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;

/**
 * @author Srikantha
 * 
 */
public class ISACreditCardValidator {

	// use this method to validate the credit card numbers. This function
	// returns a boolean value
	// depending on the validation. supports Amex, Dinners, Master and Visa.
	public boolean isValidCreditCardNumber(CreditCardPayment ccPayment) {
		String strCardNo = ccPayment.getCardNumber();
		boolean isValid = false;
		int intStart = 0;

		// AMEX
		if (strCardNo.length() == 15) {
			intStart = strCardNo.charAt(0) + strCardNo.charAt(1);
			if ((intStart == 37) || (intStart == 34)) {
				isValid = CreditCardUtil.luhnFormula(strCardNo);
			}
		}

		// DINNERS
		if (strCardNo.length() == 14) {
			intStart = strCardNo.charAt(0) + strCardNo.charAt(1);

			if ((intStart == 36) || (intStart == 38)) {
				isValid = CreditCardUtil.luhnFormula(strCardNo);
			} else {
				intStart = intStart + strCardNo.charAt(2);
				if ((intStart == 300) || (intStart == 301) || (intStart == 302) || (intStart == 303) || (intStart == 304)
						|| (intStart == 305)) {
					isValid = CreditCardUtil.luhnFormula(strCardNo);
				}
			}
		}

		// MASTER
		if (strCardNo.length() == 16) {
			intStart = strCardNo.charAt(0) + strCardNo.charAt(1);
			if ((intStart == 51) || (intStart == 52) || (intStart == 53) || (intStart == 54) || (intStart == 55)) {
				isValid = CreditCardUtil.luhnFormula(strCardNo);
			}
		}

		// VISA
		if ((strCardNo.length() == 16) || (strCardNo.toString().length() == 13)) {
			intStart = strCardNo.charAt(0);
			if (intStart == 4) {
				isValid = CreditCardUtil.luhnFormula(strCardNo);
			}
		}

		return isValid;
	}

	/**
	 * excepts the cvv value in credit card payment
	 */
	public boolean isValidCVV(String cvv) {

		if (cvv.length() < 3 || cvv.length() > 4) {
			return false;
		}

		try {
			Integer.parseInt(cvv);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}

	}

	/**
	 * We expect the expeiry date to be in YYMM
	 */
	public boolean isValidExpiryDate(String expDate) {
		if (expDate.length() != 4) {
			return false;
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyMM");
		Date d = new Date();
		String currentYYMM = dateFormat.format(d);

		int expYY = Integer.parseInt(expDate.substring(0, 2));
		int expMM = Integer.parseInt(expDate.substring(2, 4));

		int curYY = Integer.parseInt(currentYYMM.substring(0, 2));
		int curMM = Integer.parseInt(currentYYMM.substring(2, 4));

		/**
		 * Note: 10 is hardcoded. Cos a card will have maximum of 10 years as an expiry date
		 */
		if (expYY < curYY || (expYY == curYY && expMM < curMM) || (expYY > (curYY + 10))) {
			return false;
		} else {
			return true;
		}
	}

}