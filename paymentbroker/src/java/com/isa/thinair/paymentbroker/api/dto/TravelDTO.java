package com.isa.thinair.paymentbroker.api.dto;

import java.io.Serializable;
import java.util.Collection;

public class TravelDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Collection<TravelSegmentDTO> travelSegments;
	
	private String emailAddress;
	
	private String contactNumber;
	
	private String residenceCountryCode;
	
	private String contactName;

	public Collection<TravelSegmentDTO> getTravelSegments() {
		return travelSegments;
	}

	public void setTravelSegments(Collection<TravelSegmentDTO> travelSegments) {
		this.travelSegments = travelSegments;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	

	public String getResidenceCountryCode() {
		return residenceCountryCode;
	}

	public void setResidenceCountryCode(String residenceCountryCode) {
		this.residenceCountryCode = residenceCountryCode;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}	
}
