package com.isa.thinair.paymentbroker.api.dto.ameriabank;

import java.io.Serializable;
import java.math.BigDecimal;

public class AmeriaBankPaymentFieldsResponse extends AmeriaBankCommonResponse implements Serializable {

	private static final long serialVersionUID = -190909098464589L;

	private String paymentAmount;
	private BigDecimal approvedAmount;
	private String authCode;
	private String cardNumber;
	private String clientName;
	private String dateTime;
	private BigDecimal depositAmount;
	private String currency;
	private String merchantId;
	private String orderId;
	private String paymentState;
	private int paymentType;
	private String rrn;
	private String stan;
	private String terminalId;
	private String trxnDetails;
	private BigDecimal rewardPoint;
	private short orderStatusCode;
	private BigDecimal refundAmount;
	private String userId;
	private String error;

	public BigDecimal getApprovedAmount() {
		return approvedAmount;
	}

	public void setApprovedAmount(BigDecimal approvedAmount) {
		this.approvedAmount = approvedAmount;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public BigDecimal getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getPaymentState() {
		return paymentState;
	}

	public void setPaymentState(String paymentState) {
		this.paymentState = paymentState;
	}

	public int getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(int paymentType) {
		this.paymentType = paymentType;
	}

	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	public String getStan() {
		return stan;
	}

	public void setStan(String stan) {
		this.stan = stan;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public BigDecimal getRewardPoint() {
		return rewardPoint;
	}

	public void setRewardPoint(BigDecimal rewardPoint) {
		this.rewardPoint = rewardPoint;
	}

	public BigDecimal getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(BigDecimal refundAmount) {
		this.refundAmount = refundAmount;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getTrxnDetails() {
		return trxnDetails;
	}

	public void setTrxnDetails(String trxnDetails) {
		this.trxnDetails = trxnDetails;
	}

	public short getOrderStatusCode() {
		return orderStatusCode;
	}

	public void setOrderStatusCode(short orderStatusCode) {
		this.orderStatusCode = orderStatusCode;
	}

	@Override
	public String toString() {
		StringBuilder resBuff = new StringBuilder();
		resBuff.append(super.toString());
		resBuff.append(", approvedAmount: " + this.approvedAmount);
		resBuff.append(", authCode: " + this.authCode);
		// verify credit card number storage (first 6+ last 4)
		// resBuff.append(", cardNumber: " + this.cardNumber);
		resBuff.append(", clientName: " + this.clientName);
		resBuff.append(", dateTime: " + this.dateTime);
		resBuff.append(", depositAmount: " + this.depositAmount);
		resBuff.append(", merchantId: " + this.merchantId);
		resBuff.append(", orderId: " + this.orderId);
		resBuff.append(", currency: " + this.currency);
		resBuff.append(", paymentState: " + this.paymentState);
		resBuff.append(", paymentType: " + this.paymentType);
		resBuff.append(", rrn: " + this.rrn);
		resBuff.append(", stan: " + this.stan);
		resBuff.append(", terminalId: " + this.terminalId);
		resBuff.append(", trxnDetails: " + this.trxnDetails);
		resBuff.append(", rewardPoint: " + this.rewardPoint);
		resBuff.append(", orderStatusCode: " + this.orderStatusCode);
		resBuff.append(", refundAmount: " + this.refundAmount);
		resBuff.append(", userId: " + this.userId);

		return resBuff.toString();
	}

	public String getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
