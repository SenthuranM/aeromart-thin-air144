package com.isa.thinair.paymentbroker.api.dto.paypal;

import java.io.Serializable;

public class ProfileDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String mode;

	private String apiUsername;

	private String apiPassword;

	private String signature;

	private String environment;

	private String certificateFilePath;

	private String privateKeyPassword;

	private String useProxy;

	private String proxyHost;

	private String proxyPort;

	private String redirectUrl;

	private String cancelUrl;

	private String note;

	private String orderDescription;

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getMode() {
		return mode;
	}

	public void setApiUsername(String apiUsername) {
		this.apiUsername = apiUsername;
	}

	public String getApiUsername() {
		return apiUsername;
	}

	public void setApiPassword(String apiPassword) {
		this.apiPassword = apiPassword;
	}

	public String getApiPassword() {
		return apiPassword;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getSignature() {
		return signature;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setCertificateFilePath(String certificateFilePath) {
		this.certificateFilePath = certificateFilePath;
	}

	public String getCertificateFilePath() {
		return certificateFilePath;
	}

	public void setPrivateKeyPassword(String privateKeyPassword) {
		this.privateKeyPassword = privateKeyPassword;
	}

	public String getPrivateKeyPassword() {
		return privateKeyPassword;
	}

	public void setUseProxy(String useProxy) {
		this.useProxy = useProxy;
	}

	public String getUseProxy() {
		return useProxy;
	}

	public void setProxyHost(String proxyHost) {
		this.proxyHost = proxyHost;
	}

	public String getProxyHost() {
		return proxyHost;
	}

	public void setProxyPort(String proxyPort) {
		this.proxyPort = proxyPort;
	}

	public String getProxyPort() {
		return proxyPort;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setCancelUrl(String cancelUrl) {
		this.cancelUrl = cancelUrl;
	}

	public String getCancelUrl() {
		return cancelUrl;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getNote() {
		return note;
	}

	public void setOrderDescription(String orderDescription) {
		this.orderDescription = orderDescription;
	}

	public String getOrderDescription() {
		return orderDescription;
	}

}
