package com.isa.thinair.paymentbroker.api.dto.ameriabank;

import java.io.Serializable;

public class AmeriaBankBaseResponse implements Serializable {

	private static final long serialVersionUID = -190909098464589L;

	private String respCode;
	private String respMessage;

	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public String getRespMessage() {
		return respMessage;
	}

	public void setRespMessage(String respMessage) {
		this.respMessage = respMessage;
	}

	@Override
	public String toString() {
		StringBuilder resBuff = new StringBuilder();
		resBuff.append("respCode: " + this.respCode);
		resBuff.append(", respMessage: " + this.respMessage);

		return resBuff.toString();
	}

}
