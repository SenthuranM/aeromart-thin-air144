package com.isa.thinair.paymentbroker.api.dto;

import com.isa.thinair.paymentbroker.api.util.PaymentConstants;

import java.io.Serializable;

public class CreditCardPaymentStatusDTO implements Serializable {

	private static final long serialVersionUID = 5861037940984079571L;

	private String pnr;
	private String merchantTnxId;
	private Integer temporaryPaymentId;
	private PaymentConstants.ServiceType serviceType;
	private String strRequest;
	private String strResponse;
	private String transactionId;
	private boolean isSuccess;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getMerchantTnxId() {
		return merchantTnxId;
	}

	public void setMerchantTnxId(String merchantTnxId) {
		this.merchantTnxId = merchantTnxId;
	}

	public Integer getTemporaryPaymentId() {
		return temporaryPaymentId;
	}

	public void setTemporaryPaymentId(Integer temporaryPaymentId) {
		this.temporaryPaymentId = temporaryPaymentId;
	}

	public String getStrRequest() {
		return strRequest;
	}

	public void setStrRequest(String strRequest) {
		this.strRequest = strRequest;
	}

	public String getStrResponse() {
		return strResponse;
	}

	public void setStrResponse(String strResponse) {
		this.strResponse = strResponse;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public PaymentConstants.ServiceType getServiceType() {
		return serviceType;
	}

	public void setServiceType(PaymentConstants.ServiceType serviceType) {
		this.serviceType = serviceType;
	}
}
