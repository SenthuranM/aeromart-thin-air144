package com.isa.thinair.paymentbroker.api.dto.payFort;

public enum PayFortOnlineParam {

    COMMAND("command"),
    ACCESS_CODE("access_code"),
    MERCHANT_IDENTIFIER("merchant_identifier"),
    MERCHANT_REFERENCE("merchant_reference"),
    AMOUNT("amount"),
    CURRENCY("currency"),
    LANGUAGE("language"),
    CUSTOMER_EMAIL("customer_email"),
    SIGNATURE("signature"),
    PAYMENT_OPTION("payment_option"),
    RETURN_URL("return_url"),
    CUSTOMER_NAME("customer_name"),
    PHONE_NUMBER("phone_number"),
    TOKEN_NAME("token_name"),
    SADAD_OLP("sadad_olp"),
    E_COMMERCE_INDICATOR("eci"),
    ORDER_DESCRIPTION("order_description"),
    CUSTOMER_IP("customer_ip"),
    PNR("merchant_extra"),
    MERCHANT_EXTRA_1("merchant_extra1"),
    MERCHANT_EXTRA_2("merchant_extra2"),
    MERCHANT_EXTRA_3("merchant_extra3"),
    MERCHANT_EXTRA_4("merchant_extra4"),
    REMEMBER_ME("remember_me"),
    SETTLEMENT_REFERENCE("settlement_reference"),
    FORT_ID("fort_id"),
    AUTHORIZATION_CODE("authorization_code"),
    RESPONSE_CODE("response_code"),
    RESPONSE_MESSAGE("response_message"),
    STATUS("status"),
    CARD_HOLDER_NAME("card_holder_name"),
    EXPIRY_DATE("expiry_date"),
    CARD_NUMBER("card_number"),
    TRANSACTION_STATUS("transaction_status"),
    TRANSACTION_CODE("transaction_code"),
    TRANSACTION_MESSAGE("transaction_message"),
    REFUNDED_AMOUNT("refunded_amount"),
    CAPTURED_AMOUNT("captured_amount"),
    AUTHORIZED_AMOUNT("authorized_amount"),
    INSTALLMENTS("installments"),
    NUMBER_OF_INSTALLMENTS("number_of_installments"),
    QUERY_COMMAND("query_command"),
    REQUEST_EXPIRY_DATE("request_expiry_date"),
    PAYMENT_PARTNER("payment_partner"),
    SERVICE_COMMAND("service_command"),
    NOTIFICATION_TYPE("notification_type"),
    PAYMENT_LINK_ID("payment_link_id"),
    PAYMENT_LINK("payment_link"),
    BILL_NUMBER("bill_number");

    private final String name;

    PayFortOnlineParam(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
