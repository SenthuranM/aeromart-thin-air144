package com.isa.thinair.paymentbroker.api.dto.behpardakht;

import java.io.Serializable;
import java.util.Map;

public class BehpardakhtPayResponse extends BehpardakhtCommonResponse implements Serializable {

	private static final long serialVersionUID = -190909098464587L;

	public static final String NO_VALUE = "";

	public static final String OK = "OK";

	public void setResponse(Map response) {

		this.RefId = (String) response.get(REF_ID);
		this.ResCode = null2unknown((String) response.get(RES_CODE));
		this.SaleOrderId = null2unknown((String) response.get(SALES_ORDER_ID));
		String salesRefTxt = null2unknown((String) response.get(SALES_REF_ID));
		if (salesRefTxt.equals("")) {
			this.SaleReferenceId = null;
		} else {
			this.SaleReferenceId = Long.parseLong(salesRefTxt);
		}
		this.CardHolderInfo = null2unknown((String) response.get(CARD_HOLDER_INFO));

	}

	/*
	 * This method takes a data String and returns a predefined value if empty If data Sting is null, returns string
	 * "No Value Returned", else returns input
	 * 
	 * @param in String containing the data String
	 * 
	 * @return String containing the output String
	 */
	private static String null2unknown(String in) {
		if (in == null || in.length() == 0) {
			return NO_VALUE;
		} else {
			return in;
		}
	}

	public String toString() {
		StringBuffer resBuff = new StringBuffer();
		resBuff.append("Behpardakht Payment Details [");
		resBuff.append("RefId: " + this.RefId);
		resBuff.append(", ResCode: " + this.ResCode);
		resBuff.append(", SaleOrderId: " + this.SaleOrderId);
		resBuff.append(", SaleReferenceId: " + this.SaleReferenceId);
		resBuff.append(", CardHolderInfo: " + this.CardHolderInfo);
		resBuff.append(" ] ");
		return resBuff.toString();
	}

}
