package com.isa.thinair.paymentbroker.api.dto.payFort;

import java.io.Serializable;

import javax.xml.datatype.XMLGregorianCalendar;

public class CheckOrderResponseDTO implements Serializable {

	// later implments with a common class for Responses
	private static final long serialVersionUID = -8393454998531544L;
	
	private XMLGregorianCalendar msgDate;
	
	private String orderID;
	
	private String currency;
	
	private String payment;
	
	private String orderStatus;
	
	private String signature;
	
	private String errorCode;
	
	private String errorDesc;
	
	private String status;
	
	private String additionalNote;

	public XMLGregorianCalendar getMsgDate() {
		return msgDate;
	}

	public void setMsgDate(XMLGregorianCalendar msgDate) {
		this.msgDate = msgDate;
	}

	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDesc() {
		return errorDesc;
	}

	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

	public String getAdditionalNote() {
		return additionalNote;
	}

	public void setAdditionalNote(String additionalNote) {
		this.additionalNote = additionalNote;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
