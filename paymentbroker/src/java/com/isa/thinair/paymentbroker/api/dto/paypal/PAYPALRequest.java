package com.isa.thinair.paymentbroker.api.dto.paypal;

import java.io.Serializable;

import com.paypal.soap.api.DoDirectPaymentRequestType;
import com.paypal.soap.api.DoExpressCheckoutPaymentRequestType;
import com.paypal.soap.api.GetExpressCheckoutDetailsRequestType;
import com.paypal.soap.api.SetExpressCheckoutRequestType;

public class PAYPALRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private int paymentBrokerRefNo;

	private String currentState;

	private DoDirectPaymentRequestType doDirectPaymentRequestType;

	private SetExpressCheckoutRequestType setExpressCheckoutRequestType;

	private GetExpressCheckoutDetailsRequestType getExpressCheckoutDetailsRequestType;

	private DoExpressCheckoutPaymentRequestType doExpressCheckoutPaymentRequestType;

	public void setDoDirectPaymentRequestType(DoDirectPaymentRequestType doDirectPaymentRequestType) {
		this.doDirectPaymentRequestType = doDirectPaymentRequestType;
	}

	public DoDirectPaymentRequestType getDoDirectPaymentRequestType() {
		return doDirectPaymentRequestType;
	}

	public void setSetExpressCheckoutRequestType(SetExpressCheckoutRequestType setExpressCheckoutRequestType) {
		this.setExpressCheckoutRequestType = setExpressCheckoutRequestType;
	}

	public SetExpressCheckoutRequestType getSetExpressCheckoutRequestType() {
		return setExpressCheckoutRequestType;
	}

	public void setDoExpressCheckoutPaymentRequestType(DoExpressCheckoutPaymentRequestType doExpressCheckoutPaymentRequestType) {
		this.doExpressCheckoutPaymentRequestType = doExpressCheckoutPaymentRequestType;
	}

	public DoExpressCheckoutPaymentRequestType getDoExpressCheckoutPaymentRequestType() {
		return doExpressCheckoutPaymentRequestType;
	}

	public void
			setGetExpressCheckoutDetailsRequestType(GetExpressCheckoutDetailsRequestType getExpressCheckoutDetailsRequestType) {
		this.getExpressCheckoutDetailsRequestType = getExpressCheckoutDetailsRequestType;
	}

	public GetExpressCheckoutDetailsRequestType getGetExpressCheckoutDetailsRequestType() {
		return getExpressCheckoutDetailsRequestType;
	}

	public void setPaymentBrokerRefNo(int paymentBrokerRefNo) {
		this.paymentBrokerRefNo = paymentBrokerRefNo;
	}

	public int getPaymentBrokerRefNo() {
		return paymentBrokerRefNo;
	}

	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}

	public String getCurrentState() {
		return currentState;
	}

}
