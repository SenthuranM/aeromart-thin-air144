package com.isa.thinair.paymentbroker.api.dto.Parsian;

import java.io.Serializable;

public class ParsianPaymentResponseDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private long authority;
	
	private short status;
	
	private int paymentBrokerRefNo;
	
	private long invoiceNumber;
	
	public long getAuthority() {
		return authority;
	}

	public void setAuthority(long authority) {
		this.authority = authority;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("authority:" + authority).append(",status:" + status);
		return builder.toString();
	}

	public int getPaymentBrokerRefNo() {
		return paymentBrokerRefNo;
	}

	public void setPaymentBrokerRefNo(int paymentBrokerRefNo) {
		this.paymentBrokerRefNo = paymentBrokerRefNo;
	}

	public long getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(long invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}	
	
}
