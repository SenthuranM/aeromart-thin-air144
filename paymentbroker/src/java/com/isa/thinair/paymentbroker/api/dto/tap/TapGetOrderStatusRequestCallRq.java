package com.isa.thinair.paymentbroker.api.dto.tap;

import java.io.Serializable;

public class TapGetOrderStatusRequestCallRq implements Serializable {

	private static final long serialVersionUID = -564839478944591L;

	private Long merchantID;
	private String password;
	private String referenceID;
	private String userName;

	public Long getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(Long merchantID) {
		this.merchantID = merchantID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getReferenceID() {
		return referenceID;
	}

	public void setReferenceID(String referenceID) {
		this.referenceID = referenceID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String toString() {

		StringBuffer resBuff = new StringBuffer();

		resBuff.append("MERCHANTID : ").append(merchantID).append("PASSWORD : ").append(password).append("REFERENCEID : ")
				.append(referenceID).append("USERNAME : ").append(userName);
		return resBuff.toString();
	}
}
