package com.isa.thinair.paymentbroker.api.dto.ameriabank;

import java.io.Serializable;
import java.math.BigDecimal;

public class AmeriaBankSessionInfo implements Serializable {

	private static final long serialVersionUID = -190909098464589L;

	private BigDecimal paymentAmount;
	private String paymentId;
	private String opaque;
	private String currency;
	private String error;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getOpaque() {
		return opaque;
	}

	public void setOpaque(String opaque) {
		this.opaque = opaque;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Override
	public String toString() {
		StringBuffer resBuff = new StringBuffer();
		resBuff.append(", paymentAmount: " + this.paymentAmount);
		resBuff.append(", opaque: " + this.opaque);
		resBuff.append(", currency: " + this.currency);
		return resBuff.toString();
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

}
