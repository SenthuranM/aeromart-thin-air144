package com.isa.thinair.paymentbroker.api.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.isa.thinair.airproxy.api.model.reservation.core.CommonItineraryParamDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StaticFileNameUtil;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;

public class PaymentEmailUtil {

	public static void sendFatouratiEmail(LCCClientReservation lccInfo, CommonItineraryParamDTO itDto, TrackInfoDTO trackInfoDTO)
			throws ModuleException {
		MessagingServiceBD messagingServiceBD = ReservationModuleUtils.getMessagingServiceBD();

		// User Message
		UserMessaging userMessaging = new UserMessaging();
		userMessaging.setFirstName(lccInfo.getContactInfo().getFirstName());
		userMessaging.setLastName(lccInfo.getContactInfo().getLastName());
		userMessaging.setToAddres(lccInfo.getContactInfo().getEmail());

		List<UserMessaging> messages = new ArrayList<UserMessaging>();
		messages.add(userMessaging);
		HashMap<String, Object> itineraryDataMap = new HashMap<String, Object>();
		itineraryDataMap.put("fatourati", lccInfo);
		itineraryDataMap.put("duration", AppSysParamsUtil.getOnholdOfflinePaymentTimeInMillis() / (1000 * 60));
		// -- Logo
		itineraryDataMap.put("ibeURL", CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL));
		itineraryDataMap.put("logoImageName",
				StaticFileNameUtil.getCorrected("LogoAni" + AppSysParamsUtil.getDefaultCarrierCode() + ".gif"));

		// Topic
		Topic topic = new Topic();
		topic.setTopicParams(itineraryDataMap);
		topic.setTopicName(ReservationInternalConstants.PnrTemplateNames.FATOURATI_EMAIL);
		// topic.setLocale(itineraryLayoutModesDTO.getLocale());
		topic.setAttachMessageBody(true);
		// topic.setAuditInfo(composeAudit(itineraryLayoutModesDTO.getPnr(),
		// credentialsDTO));

		// User Profile
		MessageProfile messageProfile = new MessageProfile();
		messageProfile.setUserMessagings(messages);
		messageProfile.setTopic(topic);

		// Send the message
		messagingServiceBD.sendMessage(messageProfile);
	}

}
