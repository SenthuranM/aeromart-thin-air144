package com.isa.thinair.paymentbroker.api.dto;

import java.io.Serializable;

import com.isa.thinair.paymentbroker.api.dto.paypal.ProfileDTO;
import com.isa.thinair.paymentbroker.api.dto.paypal.UserInputDTO;

public class PAYPALRequestDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ProfileDTO profileDTO;

	private UserInputDTO userInputDTO;

	public void setProfileDTO(ProfileDTO profileDTO) {
		this.profileDTO = profileDTO;
	}

	public ProfileDTO getProfileDTO() {
		return profileDTO;
	}

	public void setUserInputDTO(UserInputDTO userInputDTO) {
		this.userInputDTO = userInputDTO;
	}

	public UserInputDTO getUserInputDTO() {
		return userInputDTO;
	}

}
