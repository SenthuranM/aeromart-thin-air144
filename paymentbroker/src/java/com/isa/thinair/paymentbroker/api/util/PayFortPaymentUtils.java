package com.isa.thinair.paymentbroker.api.util;

import java.math.BigDecimal;

public class PayFortPaymentUtils {

	public static final String PAYFORT_PAYSTORE_PAYMENT_CONF = "payfort.voucher.Display";
	public static final String PAYFORT_PAYSTORE_PAYMENT_FAIL = "payfort.voucher.Display.fail";
	public static final String PAYFORT_PAYSTORE_PAYMENT_DISPLAY_AGAIN = "payfort.voucher.Display.again";
	public static final String PAYFORT_PAYSTORE_VOUCHER_POPUP_MESSAGE = "payfort.voucher.popup.message";
	public static final String PAYFORT_PAYHOME_TRACK_NO_POPUP_MESSAGE = "payfort.payAtHome.TrackNum.popup.message";
	public static final String PAYFORT_PAYHOME_PAYMENT_CANCEL_MESSAGE = "payfort.payAtHome.cancel.message";

	public static final String TICKET_NUMBER = "ticketNumber";
	public static final String PROCESSED = "Processed";
	public static final String CURRENCY = "currency";
	public static final String MERCHANT_ID = "merchantID";
	public static final String ORDER_ID = "orderID";
	public static final String PAYMENT = "payment";
	public static final String SERVICE_NAME = "serviceName";
	public static final String SIGNATURE = "signature";
	public static final String PAYFORT = "PAYFORT";
	public static final String SUCCESS = "Success";
	public static final String ERROR = "Error";
	public static final String NOTIFICATION = "notification";
	public static final String PAY_AT_STORE = "PAYFORT_PAY_AT_STORE";
	public static final String PAY_AT_HOME = "PAYFORT_PAY_AT_HOME";

	public static final String PAY_FORT_STORE = "PAYFORT_PAY_AT_STORE";

	/**
	 * PayFort PG doesn't support decimal values, PayFort PG supports only Integer values
	 * 
	 * @param amount
	 * @return
	 */
	public static BigDecimal getFormattedAmount(String amount, boolean roundUp) {

		BigDecimal amt = new BigDecimal(amount);

		if (roundUp) {
			amt = amt.setScale(0, BigDecimal.ROUND_UP);
		} else {
			amt = amt.setScale(0, BigDecimal.ROUND_DOWN);
		}

		amount = amt.toString();
		BigDecimal finalamount = new BigDecimal(amount);
		return finalamount;
	}

}
