package com.isa.thinair.paymentbroker.api.dto;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.paypal.PAYPALRequest;
import com.isa.thinair.paymentbroker.api.dto.paypal.PAYPALResponse;
import com.isa.thinair.paymentbroker.api.dto.paypal.UserInputDTO;

public class IPGRequestDTO implements Serializable {

	private static final long serialVersionUID = -7268787840296726136L;

	private String returnUrl;

	private String statusUrl;

	private String offerUrl;

	private String receiptUrl;

	private AppIndicatorEnum applicationIndicator;

	private String pnr;

	private String cardType;

	private String cardNo;

	private String cardName;

	private String holderName;

	private String secureCode;

	private String expiryDate; // yymm

	private String amount;

	private int applicationTransactionId;

	private String sessionID;

	private String requestedCarrierCode;

	IPGIdentificationParamsDTO ipgIdentificationParamsDTO;

	private int noOfDecimals = 2;

	// Contact Details

	private String contactEmail;

	private String contactFirstName;

	private String contactLastName;

	private String contactAddressLine1;

	private String contactAddressLine2;

	private String contactCity;

	private String contactState;

	private String contactCountryCode;
	
	private String contactCountryName;

	private String contactPostalCode;

	private String contactMobileNumber;

	private String contactPhoneNumber;

	private String sessionLanguageCode;

	private String userIPAddress;

	private String userAgent;

	private String ipCountryCode;
	/** Hold reservation data for fraud Checking **/
	private TravelDTO travelDTO;

	private PAYPALRequest paypalRequest;

	private PAYPALResponse paypalResponse;

	private String paymentGateWayName;

	private UserInputDTO userInputDTO;

	private boolean isIntExtPaymentGateway;

	private String paymentMethod;

	private String defaultCarrierName;

	private long expireMTCOffLinePeriod;

	private String selectedSystem;
	
	private Date invoiceExpirationTime ;
	
	private boolean isOfflineMode;
	
	private boolean serviceAppFlow;

	private Integer payCurrencyISODecimalPlaces;

	private String preferredLanguage;

	private String signature;

	private String requestData;

	private boolean saveCreditCard;
	
	private String customerId;
	
	private String alias;
	
	private Date expDate;
	
	private String tnxType;

	/**
	 * @return the timeToSpare
	 */
	public Long getTimeToSpare() {
		return timeToSpare;
	}

	/**
	 * @param timeToSpare the timeToSpare to set
	 */
	public void setTimeToSpare(Long timeToSpare) {
		this.timeToSpare = timeToSpare;
	}

	private Long timeToSpare;
	
	/**
	 * @return Returns the requestedCarrierCode.
	 */
	public String getRequestedCarrierCode() {
		return requestedCarrierCode;
	}

	/**
	 * @param requestedCarrierCode
	 *            The requestedCarrierCode to set.
	 */
	public void setRequestedCarrierCode(String requestedCarrierCode) {
		this.requestedCarrierCode = requestedCarrierCode;
	}

	/**
	 * @return Returns the amount.
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return Returns the applicationIndicator.
	 */
	public AppIndicatorEnum getApplicationIndicator() {
		return applicationIndicator;
	}

	/**
	 * @param applicationIndicator
	 *            The applicationIndicator to set.
	 */
	public void setApplicationIndicator(AppIndicatorEnum applicationIndicator) {
		this.applicationIndicator = applicationIndicator;
	}

	/**
	 * @return Returns the cardNo.
	 */
	public String getCardNo() {
		return cardNo;
	}

	/**
	 * @param cardNo
	 *            The cardNo to set.
	 */
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	/**
	 * @return Returns the cardType.
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * @param cardType
	 *            The cardType to set.
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @return Returns the expiryDate.
	 */
	public String getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @param expiryDate
	 *            The expiryDate to set.
	 */
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * @return Returns the holderName.
	 */
	public String getHolderName() {
		return holderName;
	}

	/**
	 * @param holderName
	 *            The holderName to set.
	 */
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	/**
	 * @return Returns the pnr.
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the returnUrl.
	 */
	public String getReturnUrl() {
		return returnUrl;
	}

	/**
	 * @param returnUrl
	 *            The returnUrl to set.
	 */
	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	/**
	 * @return Returns the secureCode.
	 */
	public String getSecureCode() {
		return secureCode;
	}

	/**
	 * @param secureCode
	 *            The secureCode to set.
	 */
	public void setSecureCode(String secureCode) {
		this.secureCode = secureCode;
	}

	/**
	 * @param applicationTransactionId
	 *            The applicationTransactionId to set.
	 */
	public void setApplicationTransactionId(int applicationTransactionId) {
		this.applicationTransactionId = applicationTransactionId;
	}

	/**
	 * @return Returns the applicationTransactionId.
	 */
	public int getApplicationTransactionId() {
		return applicationTransactionId;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	/**
	 * @return Returns the statusUrl.
	 */
	public String getStatusUrl() {
		return statusUrl;
	}

	/**
	 * @param statusUrl
	 *            The statusUrl to set.
	 */
	public void setStatusUrl(String statusUrl) {
		this.statusUrl = statusUrl;
	}

	/**
	 * @return the ipgIdentificationParamsDTO
	 */
	public IPGIdentificationParamsDTO getIpgIdentificationParamsDTO() {
		return ipgIdentificationParamsDTO;
	}

	/**
	 * @param ipgIdentificationParamsDTO
	 *            the ipgIdentificationParamsDTO to set
	 */
	public void setIpgIdentificationParamsDTO(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) {
		this.ipgIdentificationParamsDTO = ipgIdentificationParamsDTO;
	}

	/**
	 * @return the noOfDecimals
	 */
	public int getNoOfDecimals() {
		return noOfDecimals;
	}

	/**
	 * @param noOfDecimals
	 *            the noOfDecimals to set
	 */
	public void setNoOfDecimals(int noOfDecimals) {
		this.noOfDecimals = noOfDecimals;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return contactEmail;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.contactEmail = email;
	}

	/**
	 * @return the contactEmail
	 */
	public String getContactEmail() {
		return contactEmail;
	}

	/**
	 * @param contactEmail
	 *            the contactEmail to set
	 */
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	/**
	 * @return the contactFirstName
	 */
	public String getContactFirstName() {
		return contactFirstName;
	}

	/**
	 * @param contactFirstName
	 *            the contactFirstName to set
	 */
	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	/**
	 * @return the contactLastName
	 */
	public String getContactLastName() {
		return contactLastName;
	}

	/**
	 * @param contactLastName
	 *            the contactLastName to set
	 */
	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	/**
	 * @return the contactCity
	 */
	public String getContactCity() {
		return contactCity;
	}

	/**
	 * @param contactCity
	 *            the contactCity to set
	 */
	public void setContactCity(String contactCity) {
		this.contactCity = contactCity;
	}

	/**
	 * @return the contactState
	 */
	public String getContactState() {
		return contactState;
	}

	/**
	 * @param contactState
	 *            the contactState to set
	 */
	public void setContactState(String contactState) {
		this.contactState = contactState;
	}

	/**
	 * @return the contactCountryCode
	 */
	public String getContactCountryCode() {
		return contactCountryCode;
	}

	/**
	 * @param contactCountryCode
	 *            the contactCountryCode to set
	 */
	public void setContactCountryCode(String contactCountryCode) {
		this.contactCountryCode = contactCountryCode;
	}

	/**
	 * @return the contactMobileNumber
	 */
	public String getContactMobileNumber() {
		return contactMobileNumber;
	}

	/**
	 * @param contactMobileNumber
	 *            the contactMobileNumber to set
	 */
	public void setContactMobileNumber(String contactMobileNumber) {
		this.contactMobileNumber = contactMobileNumber;
	}

	/**
	 * @return the contactPhoneNumber
	 */
	public String getContactPhoneNumber() {
		return contactPhoneNumber;
	}

	/**
	 * @param contactPhoneNumber
	 *            the contactPhoneNumber to set
	 */
	public void setContactPhoneNumber(String contactPhoneNumber) {
		this.contactPhoneNumber = contactPhoneNumber;
	}

	/**
	 * @return the sessionLanguageCode
	 */
	public String getSessionLanguageCode() {
		return sessionLanguageCode;
	}

	/**
	 * @param sessionLanguageCode
	 *            the sessionLanguageCode to set
	 */
	public void setSessionLanguageCode(String sessionLanguageCode) {
		this.sessionLanguageCode = sessionLanguageCode;
	}

	/**
	 * @return the contactAddressLine1
	 */
	public String getContactAddressLine1() {
		return contactAddressLine1;
	}

	/**
	 * @param contactAddressLine1
	 *            the contactAddressLine1 to set
	 */
	public void setContactAddressLine1(String contactAddressLine1) {
		this.contactAddressLine1 = contactAddressLine1;
	}

	/**
	 * @return the contactAddressLine2
	 */
	public String getContactAddressLine2() {
		return contactAddressLine2;
	}

	/**
	 * @param contactAddressLine2
	 *            the contactAddressLine2 to set
	 */
	public void setContactAddressLine2(String contactAddressLine2) {
		this.contactAddressLine2 = contactAddressLine2;
	}

	/**
	 * @return the contactPostalCode
	 */
	public String getContactPostalCode() {
		return contactPostalCode;
	}

	/**
	 * @param contactPostalCode
	 *            the contactPostalCode to set
	 */
	public void setContactPostalCode(String contactPostalCode) {
		this.contactPostalCode = contactPostalCode;
	}

	/**
	 * @return the offerUrl
	 */
	public String getOfferUrl() {
		return offerUrl;
	}

	/**
	 * @param offerUrl
	 *            the offerUrl to set
	 */
	public void setOfferUrl(String offerUrl) {
		this.offerUrl = offerUrl;
	}

	public String getUserIPAddress() {
		return userIPAddress;
	}

	public void setUserIPAddress(String userIPAddress) {
		this.userIPAddress = userIPAddress;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getIpCountryCode() {
		return ipCountryCode;
	}

	public void setIpCountryCode(String ipCountryCode) {
		this.ipCountryCode = ipCountryCode;
	}

	public TravelDTO getTravelDTO() {
		return travelDTO;
	}

	public void setTravelDTO(TravelDTO travelDTO) {
		this.travelDTO = travelDTO;
	}

	public void setPaypalRequest(PAYPALRequest paypalRequest) {
		this.paypalRequest = paypalRequest;
	}

	public PAYPALRequest getPaypalRequest() {
		return paypalRequest;
	}

	public void setPaypalResponse(PAYPALResponse paypalResponse) {
		this.paypalResponse = paypalResponse;
	}

	public PAYPALResponse getPaypalResponse() {
		return paypalResponse;
	}

	public void setPaymentGateWayName(String paymentGateWayName) {
		this.paymentGateWayName = paymentGateWayName;
	}

	public String getPaymentGateWayName() {
		return paymentGateWayName;
	}

	public void setIntExtPaymentGateway(boolean isIntExtPaymentGateway) {
		this.isIntExtPaymentGateway = isIntExtPaymentGateway;
	}

	public boolean isIntExtPaymentGateway() {
		return isIntExtPaymentGateway;
	}

	public void setUserInputDTO(UserInputDTO userInputDTO) {
		this.userInputDTO = userInputDTO;
	}

	public UserInputDTO getUserInputDTO() {
		return userInputDTO;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	/**
	 * @return the receiptUrl
	 */
	public String getReceiptUrl() {
		return receiptUrl;
	}

	/**
	 * @param receiptUrl
	 *            the receiptUrl to set
	 */
	public void setReceiptUrl(String receiptUrl) {
		this.receiptUrl = receiptUrl;
	}

	/**
	 * @return the defaultCarrierName
	 */
	public String getDefaultCarrierName() {
		return defaultCarrierName;
	}

	/**
	 * @param defaultCarrierName
	 *            the defaultCarrierName to set
	 */
	public void setDefaultCarrierName(String defaultCarrierName) {
		this.defaultCarrierName = defaultCarrierName;
	}

	public long getExpireMTCOffLinePeriod() {
		return expireMTCOffLinePeriod;
	}

	public void setExpireMTCOffLinePeriod(long expireMTCOffLinePeriod) {
		this.expireMTCOffLinePeriod = expireMTCOffLinePeriod;
	}

	public String getSelectedSystem() {
		return selectedSystem;
	}

	public void setSelectedSystem(String selectedSystem) {
		this.selectedSystem = selectedSystem;
	}

	public Date getInvoiceExpirationTime() {
		return invoiceExpirationTime;
	}

	public void setInvoiceExpirationTime(Date invoiceExpirationTime) {
		this.invoiceExpirationTime = invoiceExpirationTime;
	}

	/**
	 * @return the isOfflineMode
	 */
	public boolean isOfflineMode() {
		return isOfflineMode;
	}

	/**
	 * @param isOfflineMode the isOfflineMode to set
	 */
	public void setOfflineMode(boolean isOfflineMode) {
		this.isOfflineMode = isOfflineMode;
	}

	public String getContactCountryName() {
		return contactCountryName;
	}

	public void setContactCountryName(String contactCountryName) {
		this.contactCountryName = contactCountryName;
	}

	public boolean isServiceAppFlow() {
		return serviceAppFlow;
	}

	public void setServiceAppFlow(boolean serviceAppFlow) {
		this.serviceAppFlow = serviceAppFlow;
	}

	public String getCardName() {
		return cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public Integer getPayCurrencyISODecimalPlaces() {
		return payCurrencyISODecimalPlaces;
	}

	public void setPayCurrencyISODecimalPlaces(Integer payCurrencyISODecimalPlaces) {
		this.payCurrencyISODecimalPlaces = payCurrencyISODecimalPlaces;
	}
	
	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getRequestData() {
		return requestData;
	}

	public void setRequestData(String requestData) {
		this.requestData = requestData;
	}

	public String getPreferredLanguage() {
		return preferredLanguage;
	}

	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}

	public boolean isSaveCreditCard() {
		return saveCreditCard;
	}

	public void setSaveCreditCard(boolean saveCreditCard) {
		this.saveCreditCard = saveCreditCard;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	//This should be alias expiry date, not credit cards expiry date
	@Deprecated
	public Date getExpDate() {
		return expDate;
	}

	@Deprecated
	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}

	public String getTnxType() {
		return tnxType;
	}

	public void setTnxType(String tnxType) {
		this.tnxType = tnxType;
	}
}
