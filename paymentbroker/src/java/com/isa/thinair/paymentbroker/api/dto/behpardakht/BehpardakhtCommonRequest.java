package com.isa.thinair.paymentbroker.api.dto.behpardakht;

import java.io.Serializable;

public class BehpardakhtCommonRequest implements Serializable {

	private static final long serialVersionUID = -190909085994591L;

	public static final String TERMINAL_ID = "terminal_Id";
	public static final String USERNAME = "userName";
	public static final String USERPASSWORD = "userPassword";
	public static final String ORDER_ID = "order_Id";

	public Long terminalId;
	public String userName;
	public String userPassword;
	public Long orderId;

	public Long getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(Long terminalId) {
		this.terminalId = terminalId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String toString() {
		StringBuffer resBuff = new StringBuffer();
		resBuff.append("terminalId: " + this.terminalId);
		resBuff.append(", userName: " + this.userName);
		resBuff.append(", userPassword: " + this.userPassword);
		resBuff.append(", orderId: " + this.orderId);
		return resBuff.toString();
	}

}
