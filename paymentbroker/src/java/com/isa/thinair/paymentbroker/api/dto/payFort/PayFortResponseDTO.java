package com.isa.thinair.paymentbroker.api.dto.payFort;

import java.io.Serializable;
import java.util.Map;

import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPRequest;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPResponse;

public class PayFortResponseDTO implements Serializable {

	// later implments with a common class for Responses
	private static final long serialVersionUID = -8308508955531544L;
	
	public static final String INVOICE_WAITING = "waiting";
	public static final String INVOICE_PAID = "paid";
	public static final String INVOICE_REJECTED = "rejected";
	public static final String INVOICE_UNPAID = "unpaid";
	public static final String INVOICE_EXPIRED = "expired";
	public static final String QIWI_RESPONSE_PARAMS = "qiwiResponseParams";
	
	public static final String COMMAND = "command";
	public static final String TXN_ID = "txn_id";
	public static final String ACCOUNT = "account";
	public static final String SUM = "sum";
	public static final String CCY = "ccy";
	public static final String CHECK = "check";
	public static final String BILL = "bill";
	public static final String PAY = "pay";
	public static final String TXN_DATE = "txn_date";
	public static final String STATUS = "status";
	
	private String resultCode;
	private String billId;
	private String amount;
	private String currency;
	private String status;
	private String user;
	private String comment;
	private String description;
	private String error;
	private String date;
	private String command;
	private boolean isOnOnhold;
	private String refundId;
	private long timeToSpare;
	private String pnr;
	private String qiwiTxnId;
	private String paymentCompletionTime;
	private boolean offlineMode;
	
	public void setResponse(Map response) {
		this.billId = (String) response.get(QiwiPRequest.RESPONSE_ORDER);
		this.error = (String) response.get(QiwiPRequest.ERROR);
		this.description = (String) response.get(QiwiPRequest.DESCRIPTION);
		
		if(billId == "" || billId == null){
			this.billId = (String) response.get(QiwiPRequest.TNX_ID);
		}
		
		this.status = (String) response.get(QiwiPResponse.STATUS);
		this.error = (String) response.get(QiwiPRequest.ERROR);
		
		if(error == "" || error == null){
			this.error = (String) response.get(QiwiPRequest.QIWI_ERROR_CODE);
		}
		this.amount = (String) response.get(QiwiPRequest.AMOUNT);
		this.user = (String) response.get(QiwiPRequest.USER);
		this.currency = (String) response.get(QiwiPResponse.CCY);
		this.comment = (String) response.get(QiwiPRequest.COMMENT);
		this.command = (String) response.get(QiwiPResponse.COMMAND);
	}
	
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getBillId() {
		return billId;
	}
	public void setBillId(String billId) {
		this.billId = billId;
	}
	public String getAmount() {
		return amount;
	}
	/**
	 * @return the offlineMode
	 */
	public boolean isOfflineMode() {
		return offlineMode;
	}

	/**
	 * @param offlineMode the offlineMode to set
	 */
	public void setOfflineMode(boolean offlineMode) {
		this.offlineMode = offlineMode;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public String toString(){		
		
		String s = "Qiwi IPG Response : ";
		
		if(isOnOnhold != false){
			s += "\n Qiwi Offline Mode : " + isOnOnhold;
		}
		if(qiwiTxnId != null){
			s += "\n Qiwi's Payment Reference : " + qiwiTxnId;
		}
		if(command != null){
			s += "\n Offline Payment Response Command : " + command;
		}
		if(pnr != null){
			s += "\n PNR : " + pnr;
		}
		if (resultCode != null) {
			s += "\n ResultCode : " + resultCode;
		}
		if (status != null) {
			s += "\n status : " + status;
		}
		if (billId != null) {
			s += "\n Bill Id :" + billId;
		}
		if(error != null){
			s += "\n Error : " + error;
		}
		if (description != null) {
			s += "\n Description : " + description;
		}
		if (amount != null) {
			s += "\n Amount : " + amount;
		}
		if(currency != null){
			s += "\n Currency : " + currency;
		}
		if(user != null){
			s += "\n user :" + user;
		}
		if(comment != null){
			s += "\n comment : " + comment;
		}
		if(date != null){
			s += "\n date : " + date;
		}
		return s;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	public boolean isPaidInvoice(){
		
		if(status.equalsIgnoreCase(QiwiPResponse.INVOICE_PAID)){
			return true;
		}else{
			return false;
		}
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	/**
	 * @return the isOnOnhold
	 */
	public boolean isOnOnhold() {
		return isOnOnhold;
	}

	/**
	 * @param isOnOnhold the isOnOnhold to set
	 */
	public void setOnOnhold(boolean isOnOnhold) {
		this.isOnOnhold = isOnOnhold;
	}

	/**
	 * @return the refundId
	 */
	public String getRefundId() {
		return refundId;
	}

	/**
	 * @param refundId the refundId to set
	 */
	public void setRefundId(String refundId) {
		this.refundId = refundId;
	}

	/**
	 * @return the timeToSpare
	 */
	public long getTimeToSpare() {
		return timeToSpare;
	}

	/**
	 * @param timeToSpare the timeToSpare to set
	 */
	public void setTimeToSpare(long timeToSpare) {
		this.timeToSpare = timeToSpare;
	}

	/**
	 * @return the qiwiTxnId
	 */
	public String getQiwiTxnId() {
		return qiwiTxnId;
	}

	/**
	 * @param qiwiTxnId the qiwiTxnId to set
	 */
	public void setQiwiTxnId(String qiwiTxnId) {
		this.qiwiTxnId = qiwiTxnId;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the paymentCompletionTime
	 */
	public String getPaymentCompletionTime() {
		return paymentCompletionTime;
	}

	/**
	 * @param paymentCompletionTime the paymentCompletionTime to set
	 */
	public void setPaymentCompletionTime(String paymentCompletionTime) {
		this.paymentCompletionTime = paymentCompletionTime;
	}
		
		
}
