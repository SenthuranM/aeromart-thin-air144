package com.isa.thinair.paymentbroker.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankSessionInfo;
import com.isa.thinair.paymentbroker.api.dto.paypal.PAYPALResponse;
import com.isa.thinair.paymentbroker.api.dto.paypal.UserInputDTO;

public class IPGResponseDTO implements Serializable {

	private static final long serialVersionUID = -4485861284499816606L;

	public static final String STATUS_ACCEPTED = "ACCEPTED";

	public static final String STATUS_REJECTED = "REJECTED";

	private String status;

	private String errorCode;

	private String message;

	private String applicationTransactionId;

	private String authorizationCode;

	private int paymentBrokerRefNo;

	private int cardType;

	private Date requestTimsStamp;

	private Date responseTimeStamp;

	private String ccLast4Digits;

	private Integer temporyPaymentId;

	private PAYPALResponse paypalResponse;

	private AmeriaBankSessionInfo ameriaBankSessionInfo;

	private UserInputDTO userInputDTO;

	private String brokerType;

	private IPGTransactionResultDTO ipgTransactionResultDTO;

	private boolean onholdCreated;

	private BigDecimal eDirhamFee;

	private String responseData;

	private String signature;

	private String alias;

	private boolean saveCreditCard;

	/**
	 * @return Returns the authorizationCode.
	 */
	public String getAuthorizationCode() {
		return authorizationCode;
	}

	/**
	 * @param authorizationCode
	 *            The authorizationCode to set.
	 */
	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	/**
	 * @return Returns the applicationTransactionId.
	 */
	public String getApplicationTransactionId() {
		return applicationTransactionId;
	}

	/**
	 * @param applicationTransactionId
	 *            The applicationTransactionId to set.
	 */
	public void setApplicationTransactionId(String applicationTransactionId) {
		this.applicationTransactionId = applicationTransactionId;
	}

	/**
	 * @return Returns the message.
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            The message to set.
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the errorCode.
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode
	 *            The errorCode to set.
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return Returns the cardType.
	 */
	public int getCardType() {
		return cardType;
	}

	/**
	 * @param cardType
	 *            The cardType to set.
	 */
	public void setCardType(int cardType) {
		this.cardType = cardType;
	}

	/**
	 * Returns if the Credit Card Payment was successful or not
	 * 
	 * @return
	 */
	public boolean isSuccess() {
		if (IPGResponseDTO.STATUS_ACCEPTED.equals(this.getStatus())) {
			return true;
		}

		return false;
	}

	/**
	 * @return Returns the paymentBrokerRefNo.
	 */
	public int getPaymentBrokerRefNo() {
		return paymentBrokerRefNo;
	}

	/**
	 * @param paymentBrokerRefNo
	 *            The paymentBrokerRefNo to set.
	 */
	public void setPaymentBrokerRefNo(int paymentBrokerRefNo) {
		this.paymentBrokerRefNo = paymentBrokerRefNo;
	}

	public Date getRequestTimsStamp() {
		return requestTimsStamp;
	}

	public void setRequestTimsStamp(Date requestTimsStamp) {
		this.requestTimsStamp = requestTimsStamp;
	}

	public Date getResponseTimeStamp() {
		return responseTimeStamp;
	}

	public void setResponseTimeStamp(Date responseTimeStamp) {
		this.responseTimeStamp = responseTimeStamp;
	}

	/**
	 * @return the ccLast4Digits
	 */
	public String getCcLast4Digits() {
		return ccLast4Digits;
	}

	/**
	 * @param ccLast4Digits
	 *            the ccLast4Digits to set
	 */
	public void setCcLast4Digits(String ccLast4Digits) {
		this.ccLast4Digits = ccLast4Digits;
	}

	/**
	 * @return the temporyPaymentId
	 */
	public Integer getTemporyPaymentId() {
		return temporyPaymentId;
	}

	/**
	 * @param temporyPaymentId
	 *            the temporyPaymentId to set
	 */
	public void setTemporyPaymentId(Integer temporyPaymentId) {
		this.temporyPaymentId = temporyPaymentId;
	}

	public void setPaypalResponse(PAYPALResponse paypalResponse) {
		this.paypalResponse = paypalResponse;
	}

	public PAYPALResponse getPaypalResponse() {
		return paypalResponse;
	}

	public void setBrokerType(String brokerType) {
		this.brokerType = brokerType;
	}

	public String getBrokerType() {
		return brokerType;
	}

	public void setUserInputDTO(UserInputDTO userInputDTO) {
		this.userInputDTO = userInputDTO;
	}

	public UserInputDTO getUserInputDTO() {
		return userInputDTO;
	}

	/**
	 * @return the ipgTransactionResultDTO
	 */
	public IPGTransactionResultDTO getIpgTransactionResultDTO() {
		return ipgTransactionResultDTO;
	}

	/**
	 * @param ipgTransactionResultDTO
	 *            the ipgTransactionResultDTO to set
	 */
	public void setIpgTransactionResultDTO(IPGTransactionResultDTO ipgTransactionResultDTO) {
		this.ipgTransactionResultDTO = ipgTransactionResultDTO;
	}

	/**
	 * @return the onholdCreated
	 */
	public boolean isOnholdCreated() {
		return onholdCreated;
	}

	/**
	 * @param onholdCreated
	 *            the onholdCreated to set
	 */
	public void setOnholdCreated(boolean onholdCreated) {
		this.onholdCreated = onholdCreated;
	}

	public BigDecimal geteDirhamFee() {
		return eDirhamFee;
	}

	public void seteDirhamFee(BigDecimal eDirhamFee) {
		this.eDirhamFee = eDirhamFee;
	}

	public AmeriaBankSessionInfo getAmeriaBankSessionInfo() {
		return ameriaBankSessionInfo;
	}

	public void setAmeriaBankSessionInfo(AmeriaBankSessionInfo ameriaBankSessionInfo) {
		this.ameriaBankSessionInfo = ameriaBankSessionInfo;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getResponseData() {
		return responseData;
	}

	public void setResponseData(String responseData) {
		this.responseData = responseData;
	}
	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public boolean isSaveCreditCard() {
		return saveCreditCard;
	}

	public void setSaveCreditCard(boolean saveCreditCard) {
		this.saveCreditCard = saveCreditCard;
	}
}
