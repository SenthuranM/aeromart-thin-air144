/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.paymentbroker.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;

/**
 * Holds IPG Query related data transfer information
 * 
 * @author Nilindra Fernando
 * @since 2.0
 */
public class IPGQueryDTO implements Serializable {

	private static final long serialVersionUID = -7268787840296726136L;

	/** Holds the tempory payment id */
	private int temporyPaymentId;

	/** Holds the payment broker reference number */
	private int paymentBrokerRefNo;

	/** Holds payment gateway identification data */
	IPGIdentificationParamsDTO ipgIdentificationParamsDTO;

	/** Holds the app indicator enum */
	private AppIndicatorEnum appIndicatorEnum;

	/** Holds the payment amount */
	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();
	
	/** Holds the base Amount */
	private BigDecimal baseAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	
	/** Holds the pnr */
	private String pnr;
	
	/** Holds the pnr status */
	private String pnrStatus;
	
	/** Holds the group PNR */
	private String groupPnr;
	
	/** Holds the reservation total charge */
	private BigDecimal pnrTotalCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	
	/** Holds the gateway name */
	private String gatewayName;
	
	private Date timestamp;
	
	private PaymentType paymentType;
	
	private String creditCardNo;
	
	private String authorizationNo;
	
	private String paymentCurrencyCode;

	private BigDecimal eDirhamFee = AccelAeroCalculator.getDefaultBigDecimalZero();
	
	private String userId;

	private String aidCompany;

	private String transacationId;

	private String productType;

	/**
	 * @return Returns the amount.
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return Returns the paymentBrokerRefNo.
	 */
	public int getPaymentBrokerRefNo() {
		return paymentBrokerRefNo;
	}

	/**
	 * @param paymentBrokerRefNo
	 *            The paymentBrokerRefNo to set.
	 */
	public void setPaymentBrokerRefNo(int paymentBrokerRefNo) {
		this.paymentBrokerRefNo = paymentBrokerRefNo;
	}

	/**
	 * @return Returns the temporyPaymentId.
	 */
	public int getTemporyPaymentId() {
		return temporyPaymentId;
	}

	/**
	 * @param temporyPaymentId
	 *            The temporyPaymentId to set.
	 */
	public void setTemporyPaymentId(int temporyPaymentId) {
		this.temporyPaymentId = temporyPaymentId;
	}

	/**
	 * @return Returns the appIndicatorEnum.
	 */
	public AppIndicatorEnum getAppIndicatorEnum() {
		return appIndicatorEnum;
	}

	/**
	 * @return the ipgIdentificationParamsDTO
	 */
	public IPGIdentificationParamsDTO getIpgIdentificationParamsDTO() {
		return ipgIdentificationParamsDTO;
	}

	/**
	 * @param ipgIdentificationParamsDTO
	 *            the ipgIdentificationParamsDTO to set
	 */
	public void setIpgIdentificationParamsDTO(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) {
		this.ipgIdentificationParamsDTO = ipgIdentificationParamsDTO;
	}

	/**
	 * @param appIndicatorEnum
	 *            The appIndicatorEnum to set.
	 */
	public void setAppIndicatorEnum(AppIndicatorEnum appIndicatorEnum) {
		this.appIndicatorEnum = appIndicatorEnum;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the pnrTotalCharge
	 */
	public BigDecimal getPnrTotalCharge() {
		return pnrTotalCharge;
	}

	/**
	 * @param pnrTotalCharge the pnrTotalCharge to set
	 */
	public void setPnrTotalCharge(BigDecimal pnrTotalCharge) {
		this.pnrTotalCharge = pnrTotalCharge;
	}

	/**
	 * @return the pnrStatus
	 */
	public String getPnrStatus() {
		return pnrStatus;
	}

	/**
	 * @param pnrStatus the pnrStatus to set
	 */
	public void setPnrStatus(String pnrStatus) {
		this.pnrStatus = pnrStatus;
	}

	/**
	 * @return the groupPnr
	 */
	public String getGroupPnr() {
		return groupPnr;
	}

	/**
	 * @param groupPnr the groupPnr to set
	 */
	public void setGroupPnr(String groupPnr) {
		this.groupPnr = groupPnr;
	}

	/**
	 * @return the gatewayName
	 */
	public String getGatewayName() {
		return gatewayName;
	}

	/**
	 * @param gatewayName the gatewayName to set
	 */
	public void setGatewayName(String gatewayName) {
		this.gatewayName = gatewayName;
	}

	/**
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the paymentType
	 */
	public PaymentType getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * @return the creditCardNo
	 */
	public String getCreditCardNo() {
		return creditCardNo;
	}

	/**
	 * @param creditCardNo the creditCardNo to set
	 */
	public void setCreditCardNo(String creditCardNo) {
		this.creditCardNo = creditCardNo;
	}

	/**
	 * @return the authorizationNo
	 */
	public String getAuthorizationNo() {
		return authorizationNo;
	}

	/**
	 * @param authorizationNo the authorizationNo to set
	 */
	public void setAuthorizationNo(String authorizationNo) {
		this.authorizationNo = authorizationNo;
	}

	/**
	 * @return the baseAmount
	 */
	public BigDecimal getBaseAmount() {
		return baseAmount;
	}

	/**
	 * @param baseAmount the baseAmount to set
	 */
	public void setBaseAmount(BigDecimal baseAmount) {
		this.baseAmount = baseAmount;
	}

	/**
	 * @return the paymentCurrencyCode
	 */
	public String getPaymentCurrencyCode() {
		return paymentCurrencyCode;
	}

	/**
	 * @param paymentCurrencyCode the paymentCurrencyCode to set
	 */
	public void setPaymentCurrencyCode(String paymentCurrencyCode) {
		this.paymentCurrencyCode = paymentCurrencyCode;
	}

	public BigDecimal geteDirhamFee() {
		return eDirhamFee;
	}

	public void seteDirhamFee(BigDecimal eDirhamFee) {
		this.eDirhamFee = eDirhamFee;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAidCompany() {
		return aidCompany;
	}

	public void setAidCompany(String aidCompany) {
		this.aidCompany = aidCompany;
	}

	public String getTransacationId() {
		return transacationId;
	}

	public void setTransacationId(String transacationId) {
		this.transacationId = transacationId;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getProductType() {
		return productType;
	}

}
