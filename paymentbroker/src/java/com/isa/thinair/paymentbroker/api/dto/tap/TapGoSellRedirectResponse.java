package com.isa.thinair.paymentbroker.api.dto.tap;

import java.io.Serializable;
import java.util.Map;

public class TapGoSellRedirectResponse implements Serializable {

	private static final long serialVersionUID = -190999104594591L;

	public static final String NO_VALUE = "";

	private String Ref;
	private String Result;
	private String Payid;
	private String Crdtype;
	private String Trackid;
	private String Crd;
	private String Hash;

	public String getRef() {
		return Ref;
	}

	public void setRef(String ref) {
		Ref = ref;
	}

	public String getResult() {
		return Result;
	}

	public void setResult(String result) {
		Result = result;
	}

	public String getPayid() {
		return Payid;
	}

	public void setPayid(String payid) {
		Payid = payid;
	}

	public String getCrdtype() {
		return Crdtype;
	}

	public void setCrdtype(String crdtype) {
		Crdtype = crdtype;
	}

	public String getTrackid() {
		return Trackid;
	}

	public void setTrackid(String trackid) {
		Trackid = trackid;
	}

	public String getCrd() {
		return Crd;
	}

	public void setCrd(String crd) {
		Crd = crd;
	}

	public String getHash() {
		return Hash;
	}

	public void setHash(String hash) {
		Hash = hash;
	}

	public TapGoSellRedirectResponse(Map response) {

		this.Ref = (String) response.get("ref");
		this.Result = null2unknown((String) response.get("result"));
		this.Payid = null2unknown((String) response.get("payid"));
		this.Crdtype = null2unknown((String) response.get("crdtype"));
		this.Trackid = null2unknown((String) response.get("trackid"));
		this.Crd = null2unknown((String) response.get("crd"));
		this.Hash = null2unknown((String) response.get("hash"));

	}

	public String toString() {
		StringBuffer str = new StringBuffer();
		str.append("Tap GoSell Tap payment Response Details [");
		str.append("Tap Ref :").append(getRef()).append(",");
		str.append("Tap Result :").append(getResult()).append(",");
		str.append("Tap payid :").append(getPayid()).append(",");
		str.append("Pay Card Type :").append(getCrdtype()).append(",");
		str.append("Merchant Ref :").append(getTrackid()).append(",");
		str.append("Tap Payment Card last four Digits :").append(getCrd()).append(",");
		str.append("Tap Hash :").append(getHash());
		str.append("]");

		return str.toString();
	}

	private static String null2unknown(String in) {
		if (in == null || in.length() == 0) {
			return NO_VALUE;
		} else {
			return in;
		}
	}
}
