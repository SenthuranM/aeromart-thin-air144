package com.isa.thinair.paymentbroker.api.dto.payFort;

import java.io.Serializable;

import javax.xml.datatype.XMLGregorianCalendar;

public class Pay_AT_StoreResponseDTO implements Serializable{
	
	// later implments with a common class for Responses
	private static final long serialVersionUID = -8308509598531544L;
	
	
	public static final String VOUCHER_ID = "voucherID";
	public static final String REQUEST_ID = "requestID";
	public static final String PAYFORT = "payFortStatus";
	public static final String PAY_AT_STORE_SCRIPT_URL = "payAtstoreScriptUrl";
	public static final String PAY_AT_STORE_VOUCHER = "payFortVoucher";
	
	private Integer statusCode;
	
	private String orderID;
	
	private String voucherNumber;
	
	private String requestID;
	
	private String signature;
	
	private String 	Status;
	
	private String additionalNote;
	
	private String errorCode;
	
	private String errorDesc;
	
	private XMLGregorianCalendar msgDate;

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getAdditionalNote() {
		return additionalNote;
	}

	public void setAdditionalNote(String additionalNote) {
		this.additionalNote = additionalNote;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDesc() {
		return errorDesc;
	}

	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

	public XMLGregorianCalendar getMsgDate() {
		return msgDate;
	}

	public void setMsgDate(XMLGregorianCalendar msgDate) {
		this.msgDate = msgDate;
	}

}
