package com.isa.thinair.paymentbroker.api.dto;

import java.io.Serializable;
import java.util.Date;

public class TravelSegmentDTO implements Serializable, Comparable<TravelSegmentDTO> {

	private static final long serialVersionUID = 1L;

	private String flightNumber;

	private Date flightDate;

	private String carrierCode;

	private String departureAirportCode;

	private String departureAirportName;

	private String arrivalAirportCode;

	private String arrivalAirportName;
	
	private Integer noOfStopvers;
	
	private String classOfService;
	
	private Date departureDateZulu;

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	public String getDepartureAirportName() {
		return departureAirportName;
	}

	public void setDepartureAirportName(String departureAirportName) {
		this.departureAirportName = departureAirportName;
	}

	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	public String getArrivalAirportName() {
		return arrivalAirportName;
	}

	public void setArrivalAirportName(String arrivalAirportName) {
		this.arrivalAirportName = arrivalAirportName;
	}

	public Integer getNoOfStopvers() {
		return noOfStopvers;
	}

	public void setNoOfStopvers(Integer noOfStopvers) {
		this.noOfStopvers = noOfStopvers;
	}

	public String getClassOfService() {
		return classOfService;
	}

	public void setClassOfService(String classOfService) {
		this.classOfService = classOfService;
	}

	public Date getDepartureDateZulu() {
		return departureDateZulu;
	}

	public void setDepartureDateZulu(Date departureDateZulu) {
		this.departureDateZulu = departureDateZulu;
	}


	public int compareTo(TravelSegmentDTO travelSegment) {	
		int result = 0;
		if (departureDateZulu != null && travelSegment.getDepartureDateZulu() != null) {
			result = departureDateZulu.compareTo(travelSegment.getDepartureDateZulu());
		} 
		return result;
	}
	
	
}
