package com.isa.thinair.paymentbroker.api.dto;

public class IPGPrepPaymentDTO extends IPGBaseParams {

	public IPGPrepPaymentDTO(IPGIdentificationParamsDTO identificationParams) {
		super(identificationParams);
	}

}
