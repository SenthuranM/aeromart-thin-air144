package com.isa.thinair.paymentbroker.api.dto.cmi;

import java.io.Serializable;

public class CMIResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String orderId;
	private String groupId;
	private String response;
	private String authCode;
	private String hostrefNumber;
	private String procRetrncode;
	private String tranId;
	private String errorMsg;
	private ExtraInformation extra;
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getAuthCode() {
		return authCode;
	}
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}
	public String getHostrefNumber() {
		return hostrefNumber;
	}
	public void setHostrefNumber(String hostrefNumber) {
		this.hostrefNumber = hostrefNumber;
	}
	public String getProcRetrncode() {
		return procRetrncode;
	}
	public void setProcRetrncode(String procRetrncode) {
		this.procRetrncode = procRetrncode;
	}
	public String getTranId() {
		return tranId;
	}
	public void setTranId(String tranId) {
		this.tranId = tranId;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public ExtraInformation getExtra() {
		return extra;
	}
	public void setExtra(ExtraInformation extra) {
		this.extra = extra;
	}
	

}
