package com.isa.thinair.paymentbroker.api.dto;

import java.io.Serializable;
import java.util.Map;

import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankSessionInfo;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtResponse;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPResponse;
import com.paypal.soap.api.DoDirectPaymentResponseType;
import com.paypal.soap.api.SetExpressCheckoutResponseType;

public class IPGRequestResultsDTO implements Serializable {

	private static final long serialVersionUID = -7268787840296726136L;

	public static final String SUBMIT_METHOD_POST = "POST";

	public static final String SUBMIT_METHOD_GET = "GET";

	private String requestData;

	private int paymentBrokerRefNo;

	private String accelAeroTransactionRef; // Merchant Transaction Reference

	private Map postDataMap;

	private String submitMethod;

	private DoDirectPaymentResponseType directPaymentResponse;

	private SetExpressCheckoutResponseType setExpressCheckoutResponse;

	private QiwiPResponse qiwiResponse;

	private String errorCode;

	// For offline payments, sometimes it is needed to show the payment gateway reference number (Bill Number)
	// To display the bill number in XBE reservation summary page it is possible to populate the bill number
	// Similarly label for the bill number can vary for different payment gateways
	// Hence extra field is used for pgwbillnumber label key (i18n key)
	// For example for fawry offline pgw label should be "Fawry Reference Number"
	// If label key is not not provided then label defaults to "Bill Number :"
	private String billNumber;

	private String billNumberLabelKey;

	private boolean signatureVerified;

	private BehpardakhtResponse behpardakhtResponse;

	// TODO define a new DTO
	private AmeriaBankSessionInfo ameriaBankSessionInfo;

	private boolean payFort = false; // to identify payfort response in payment flows


	/**
	 * @return Returns the paymentBrokerRefNo.
	 */
	public int getPaymentBrokerRefNo() {
		return paymentBrokerRefNo;
	}

	/**
	 * @param paymentBrokerRefNo
	 *            The paymentBrokerRefNo to set.
	 */
	public void setPaymentBrokerRefNo(int paymentBrokerRefNo) {
		this.paymentBrokerRefNo = paymentBrokerRefNo;
	}

	/**
	 * @return Returns the requestData.
	 */
	public String getRequestData() {
		return requestData;
	}

	/**
	 * @param requestData
	 *            The requestData to set.
	 */
	public void setRequestData(String requestData) {
		this.requestData = requestData;
	}

	/**
	 * @return the postMap
	 */
	public Map getPostDataMap() {
		return postDataMap;
	}

	/**
	 * @param postMap
	 *            the postMap to set
	 */
	public void setPostDataMap(Map postMap) {
		this.postDataMap = postMap;
	}

	/**
	 * @return the method
	 */
	public String getSubmitMethod() {
		return submitMethod;
	}

	/**
	 * @param method
	 *            the method to set
	 */
	public void setSubmitMethod(String method) {
		this.submitMethod = method;
	}

	/**
	 * @return the accelAeroTransactionRef
	 */
	public String getAccelAeroTransactionRef() {
		return accelAeroTransactionRef;
	}

	/**
	 * @param accelAeroTransactionRef
	 *            the accelAeroTransactionRef to set
	 */
	public void setAccelAeroTransactionRef(String accelAeroTransactionRef) {
		this.accelAeroTransactionRef = accelAeroTransactionRef;
	}

	public void setDirectPaymentResponse(DoDirectPaymentResponseType directPaymentResponse) {
		this.directPaymentResponse = directPaymentResponse;
	}

	public DoDirectPaymentResponseType getDirectPaymentResponse() {
		return directPaymentResponse;
	}

	public void setSetExpressCheckoutResponse(SetExpressCheckoutResponseType setExpressCheckoutResponse) {
		this.setExpressCheckoutResponse = setExpressCheckoutResponse;
	}

	public SetExpressCheckoutResponseType getSetExpressCheckoutResponse() {
		return setExpressCheckoutResponse;
	}

	public QiwiPResponse getQiwiResponse() {
		return qiwiResponse;
	}

	public void setQiwiResponse(QiwiPResponse qiwiResponse) {
		this.qiwiResponse = qiwiResponse;
	}

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode
	 *            the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public BehpardakhtResponse getBehpardakhtResponse() {
		return behpardakhtResponse;
	}

	public void setBehpardakhtResponse(BehpardakhtResponse behpardakhtResponse) {
		this.behpardakhtResponse = behpardakhtResponse;
	}

	/**
	 * @return payFort payment
	 */
	public boolean isPayFort() {
		return payFort;
	}

	/**
	 * @param set
	 *            payFort payment
	 */
	public void setPayFort(boolean payFort) {
		this.payFort = payFort;
	}

	public AmeriaBankSessionInfo getAmeriaBankSessionInfo() {
		return ameriaBankSessionInfo;
	}

	public void setAmeriaBankSessionInfo(AmeriaBankSessionInfo ameriaBankSessionInfo) {
		this.ameriaBankSessionInfo = ameriaBankSessionInfo;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public String getBillNumberLabelKey() {
		return billNumberLabelKey;
	}

	public void setBillNumberLabelKey(String billNumberLabelKey) {
		this.billNumberLabelKey = billNumberLabelKey;
	}

	public boolean isSignatureVerified() {
		return signatureVerified;
	}

	public void setSignatureVerified(boolean signatureVerified) {
		this.signatureVerified = signatureVerified;
	}
}
