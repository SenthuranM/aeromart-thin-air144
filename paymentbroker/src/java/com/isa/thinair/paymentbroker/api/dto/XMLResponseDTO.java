package com.isa.thinair.paymentbroker.api.dto;

import java.io.Serializable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class XMLResponseDTO implements Serializable {

	private static Log log = LogFactory.getLog(XMLResponseDTO.class);

	private String orderId;

	private String payId;

	private String ncStatus;

	private String ncError;

	private String ncErrorPlus;

	private String acceptance;

	private String status;

	private String eci;

	private String amount;

	private String currency;

	private String paymentMethod;

	private String brand;

	private String payIdSub;

	private String html;
	
	private String alias;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getNcStatus() {
		return ncStatus;
	}

	public void setNcStatus(String ncStatus) {
		this.ncStatus = ncStatus;
	}

	public String getNcError() {
		return ncError;
	}

	public void setNcError(String ncError) {
		this.ncError = ncError;
	}

	public String getNcErrorPlus() {
		return ncErrorPlus;
	}

	public void setNcErrorPlus(String ncErrorPlus) {
		this.ncErrorPlus = ncErrorPlus;
	}

	public String getAcceptance() {
		return acceptance;
	}

	public void setAcceptance(String acceptance) {
		this.acceptance = acceptance;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEci() {
		return eci;
	}

	public void setEci(String eci) {
		this.eci = eci;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getPayIdSub() {
		return payIdSub;
	}

	public void setPayIdSub(String payIdSub) {
		this.payIdSub = payIdSub;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	
	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String toString() {

		StringBuffer resBuff = new StringBuffer();

		resBuff.append("ORDERID : ").append(orderId).append("PAYID : ").append(payId).append("NCSTATUS : ").append(ncStatus)
				.append("NCERROR : ").append(ncError).append("NCERRORPLUS : ").append(ncErrorPlus).append("ACCEPTANCE : ")
				.append(acceptance).append("STATUS : ").append(status).append("ECI : ").append(eci).append("AMOUNT : ")
				.append(amount).append("CURRENCY : ").append(currency).append("PAYMENTMETHOD : ").append(paymentMethod)
				.append("BRAND : ").append(brand).append("PAYIDSUB : ").append(payIdSub).append("ALIAS : ").append(alias)
				.append("HTML : ").append(html);

		if (resBuff.toString().length() < 2500) {
			return resBuff.toString();
		} else {
			// Response is too large to save in db field.
			log.debug("[XMLResponse::toString()] Response before trimming -" + resBuff.toString());
			return resBuff.toString().substring(0, 2500);
		}
	}
}
