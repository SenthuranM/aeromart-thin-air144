package com.isa.thinair.paymentbroker.api.dto.ccAvenue;

import java.util.Hashtable;
import java.util.Map;
import java.util.StringTokenizer;

import com.ccavenue.security.AesCryptUtil;

public class CCAvenueResponse{
	
	public static final String RESP_STATUS = "status";
	public static final String RESP_ENC_RES_XML = "enc_response";
	
	public static final String SUCCESS_STATUS = "0";
	public static final String FINAL_STATUS = "1";
	
	public static final String ENCRESP = "encResp";

	public static final String RESPONSE_SUCCESS = "Success";
	
	public static final String ORDER_STATUS_ABORTED = "Aborted";
	public static final String ORDER_STATUS_AUTO_CANCELLED = "Auto-Cancelled";
	public static final String ORDER_STATUS_AUTO_REVERSED = "Auto-Reversed";
	public static final String ORDER_STATUS_AWAITED = "Awaited";
	public static final String ORDER_STATUS_CANCELLED = "Cancelled";
	public static final String ORDER_STATUS_INVALID = "Invalid";
	public static final String ORDER_STATUS_FRAUD = "Fraud";
	public static final String ORDER_STATUS_INITIATED = "Initiated";
	public static final String ORDER_STATUS_REFUNDED = "Refunded";
	public static final String ORDER_STATUS_SHIPPED = "Shipped";
	public static final String ORDER_STATUS_SYSTEM_REFUND = "System refund";
	public static final String ORDER_STATUS_UNSUCCESSFUL = "Unsuccessful" ;
	public static final String ORDER_STATUS_SUCCESSFUL = "Successful";
	
	private String encryptionKey;
	
	private String orderId;
	private String tracking_id;
	private String bankRefNo;
	private String orderStatus;
	private String failureMessage;
	private String paymentMode;
	private String cardName;
	private String statusCode;
	private String statusMessage;
	private String currency;
	private String amount;
	private String billingName;
	private String billingTel;
	private String merchantParam1;
	private String merchantParam2;
	private boolean error;
	private String reason;
	private boolean isRefund;
	private String billingAddress;
	
	public CCAvenueResponse(){
		this.statusCode = "1";
	}
	
	public void setResponse(Map<String,String> map){
		
		String encResp = map.get("encResp");
		AesCryptUtil aesUtil = new AesCryptUtil(getEncryptionKey());
		String decResp = aesUtil.decrypt(encResp);
		StringTokenizer tokenizer = new StringTokenizer(decResp, "&");
		Hashtable<String, String> hs = new Hashtable<String, String>();
		String pair = null, pname = null, pvalue = null;
	
		while (tokenizer.hasMoreTokens()) {
			pair = (String) tokenizer.nextToken();
			if (pair != null) {
				StringTokenizer strTok = new StringTokenizer(pair, "=");
				pname = "";
				pvalue = "";
				if (strTok.hasMoreTokens()) {
					pname = (String) strTok.nextToken();
					if (strTok.hasMoreTokens())
						pvalue = (String) strTok.nextToken();
					hs.put(pname, pvalue);
				}
			}
		}
		
		setOrderId(hs.get(CCAvenueRequest.ORDER_ID));
		setTracking_id(hs.get(CCAvenueRequest.TRACKING_ID));
		setBankRefNo(hs.get(CCAvenueRequest.BANK_REF_NO));
		setOrderStatus(hs.get(CCAvenueRequest.ORDER_STATUS));
		setFailureMessage(hs.get(CCAvenueRequest.FAILURE_MESSAGE));
		setPaymentMode(hs.get(CCAvenueRequest.PAYMENT_MODE));
		setCardName(hs.get(CCAvenueRequest.CARD_NAME));
		setStatusCode(hs.get(CCAvenueRequest.STATUS_CODE));
		setStatusMessage(hs.get(CCAvenueRequest.STATUS_MESSAGE));
		setCurrency(hs.get(CCAvenueRequest.CURRENCY));
		setAmount(hs.get(CCAvenueRequest.AMOUNT));
		setBillingName(hs.get(CCAvenueRequest.BILLING_NAME));
		setBillingTel(hs.get(CCAvenueRequest.BILLING_TEL));
		setMerchantParam1(hs.get(CCAvenueRequest.MERCHANT_PARAM1));
		setMerchantParam2(hs.get(CCAvenueRequest.MERCHANT_PARAM2));
	}
	
	@Override
	public String toString(){
		String toStr = "";
		
		if(error){
			toStr += "Error!!! ";
		}
		if(merchantParam1 != null){
			toStr += "\n merchant param1(pnr) : " + merchantParam1;
		}
		if(orderId != null){
			toStr += "\n Order ID : " + orderId;
		}
		if(tracking_id != null){
			toStr += "\n Tracking ID : " + tracking_id;
		}
		if(bankRefNo != null){
			toStr += "\n Bank Ref No : " + bankRefNo;
		}
		if(orderStatus != "null"){
			toStr += "\n Order Status : " + orderStatus;
		}
		if(failureMessage != "null"){
			toStr += "\n failure Message : " + failureMessage;
		}
		if(paymentMode != "null"){
			toStr += "\n Payment Mode : " + paymentMode;
		}
		if(cardName != null){
			toStr += "\n Card Name : " + cardName;
		}
		if(statusCode != null){
			toStr += "\n Status Code : " + statusCode;
		}
		if(statusMessage != null){
			toStr += "\n status Message : " + statusMessage;
		}
		if(currency != null){
			toStr += "\n Currency : " + currency;
		}
		if(amount != null){
			toStr += "\n amount : " + amount;
		}
		if(billingName != null){
			toStr += "\n billing Name : " + billingName;
		}
		if(billingTel != null){
			toStr += "\n billing Tel : " + billingTel;
		}
		if(merchantParam2 != null){
			toStr += "\n merchat param2 : " + merchantParam2;
		}
		if(reason != null){
			toStr += "\n Error Reason : " + reason;
		}
		return toStr;
	}

	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	/**
	 * @return the encryptionKey
	 */
	public String getEncryptionKey() {
		return encryptionKey;
	}

	/**
	 * @param encryptionKey the encryptionKey to set
	 */
	public void setEncryptionKey(String encryptionKey) {
		this.encryptionKey = encryptionKey;
	}

	/**
	 * @return the tracking_id
	 */
	public String getTracking_id() {
		return tracking_id;
	}

	/**
	 * @param tracking_id the tracking_id to set
	 */
	public void setTracking_id(String tracking_id) {
		this.tracking_id = tracking_id;
	}

	/**
	 * @return the bankRefNo
	 */
	public String getBankRefNo() {
		return bankRefNo;
	}

	/**
	 * @param bankRefNo the bankRefNo to set
	 */
	public void setBankRefNo(String bankRefNo) {
		this.bankRefNo = bankRefNo;
	}

	/**
	 * @return the orderStatus
	 */
	public String getOrderStatus() {
		return orderStatus;
	}

	/**
	 * @param orderStatus the orderStatus to set
	 */
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	/**
	 * @return the failureMessage
	 */
	public String getFailureMessage() {
		return failureMessage;
	}

	/**
	 * @param failureMessage the failureMessage to set
	 */
	public void setFailureMessage(String failureMessage) {
		this.failureMessage = failureMessage;
	}

	/**
	 * @return the paymentMode
	 */
	public String getPaymentMode() {
		return paymentMode;
	}

	/**
	 * @param paymentMode the paymentMode to set
	 */
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	/**
	 * @return the cardName
	 */
	public String getCardName() {
		return cardName;
	}

	/**
	 * @param cardName the cardName to set
	 */
	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	/**
	 * @return the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the statusMessage
	 */
	public String getStatusMessage() {
		return statusMessage;
	}

	/**
	 * @param statusMessage the statusMessage to set
	 */
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the billingName
	 */
	public String getBillingName() {
		return billingName;
	}

	/**
	 * @param billingName the billingName to set
	 */
	public void setBillingName(String billingName) {
		this.billingName = billingName;
	}

	/**
	 * @return the billingTel
	 */
	public String getBillingTel() {
		return billingTel;
	}

	/**
	 * @param billingTel the billingTel to set
	 */
	public void setBillingTel(String billingTel) {
		this.billingTel = billingTel;
	}

	/**
	 * @return the merchantParam1
	 */
	public String getMerchantParam1() {
		return merchantParam1;
	}

	/**
	 * @param merchantParam1 the merchantParam1 to set
	 */
	public void setMerchantParam1(String merchantParam1) {
		this.merchantParam1 = merchantParam1;
	}

	/**
	 * @return the error
	 */
	public boolean isError() {
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(boolean error) {
		this.error = error;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReason(){
		return this.reason;
	}

	public boolean isRefund() {
		return isRefund;
	}

	public void setRefund(boolean isRefund) {
		this.isRefund = isRefund;
	}

	public String getMerchantParam2() {
		return merchantParam2;
	}

	public void setMerchantParam2(String merchantParam2) {
		this.merchantParam2 = merchantParam2;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}
}