package com.isa.thinair.paymentbroker.api.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.paymentbroker.api.constants.PaymentGatewayCard;
import com.isa.thinair.paymentbroker.api.constants.PaymentGatewayCard.FieldName;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;

public class CreditCardUtil {

	private static final String CARD_TYPE_AMEX = "AMEX";

	private static final String CARD_TYPE_DINNERS = "DINNERS";

	private static final String CARD_TYPE_MASTER = "MASTER";

	private static final String CARD_TYPE_VISA = "VISA";

	private static final String CARD_TYPE_DISCOVER = "DISCOVER";

	// validation using Luhn's Formula
	// FIXME: is this focmula correct - sudheera
	static boolean luhnFormula(String strCardNo) {
		Integer ar[] = new Integer[strCardNo.length()];
		int i = 0, sum = 0;

		for (i = 0; i < strCardNo.length(); ++i) {
			Character c = new Character(strCardNo.charAt(i));
			ar[i] = new Integer(c.toString());
		}

		for (i = ar.length - 2; i >= 0; i -= 2) { // you have to start from
			// the right, and work back.
			int x = ar[i].intValue();
			x *= 2; // every second digit starting with the right most (check
			// digit)
			if (x > 9)
				x -= 9; // will be doubled, and summed with the skipped digits.
		} // if the double digit is > 9, ADD those individual digits together

		for (i = 0; i < ar.length; ++i) {
			int xx = ar[i].intValue();
			sum += xx; // if the sum is divisible by 10 mod10 succeeds
		}
		return (((sum % 10) == 0) ? true : false);
	}

	public static String getCreditCardType(String number) {
		String cardType = null;

		if (number.length() == 16 && Integer.parseInt(number.substring(0, 2)) >= 51
				&& Integer.parseInt(number.substring(0, 2)) <= 55) {
			cardType = CARD_TYPE_MASTER;
		}

		if ((number.length() == 13 || number.length() == 16) && Integer.parseInt(number.substring(0, 1)) == 4) {
			cardType = CARD_TYPE_VISA;
		}

		if (number.length() == 15
				&& (Integer.parseInt(number.substring(0, 2)) == 34 || Integer.parseInt(number.substring(0, 2)) == 37)) {
			cardType = CARD_TYPE_AMEX;
		}

		if (number.length() == 16 && Integer.parseInt(number.substring(0, 5)) == 6011) {
			cardType = CARD_TYPE_DISCOVER;
		}

		if (number.length() == 14
				&& ((Integer.parseInt(number.substring(0, 2)) == 36 || Integer.parseInt(number.substring(0, 2)) == 38) || Integer
						.parseInt(number.substring(0, 3)) >= 300 && Integer.parseInt(number.substring(0, 3)) <= 305)) {
			cardType = CARD_TYPE_DINNERS;
		}

		if (luhnValidate(number)) {
			return cardType;
		} else {
			return null;
		}
	}

	// sudheera
	// The Luhn algorithm is basically a CRC type
	// system for checking the validity of an entry.
	// All major credit cards use numbers that will
	// pass the Luhn check. Also, all of them are based
	// on MOD 10.

	private static boolean luhnValidate(String numberString) {
		char[] charArray = numberString.toCharArray();
		int[] number = new int[charArray.length];
		int total = 0;

		for (int i = 0; i < charArray.length; i++) {
			number[i] = Character.getNumericValue(charArray[i]);
		}

		for (int i = number.length - 2; i > -1; i -= 2) {
			number[i] *= 2;

			if (number[i] > 9)
				number[i] -= 9;
		}

		for (int i = 0; i < number.length; i++)
			total += number[i];

		if (total % 10 != 0)
			return false;

		return true;
	}

	private static String getDBStroreData(List<CardDetailConfigDTO> configDataList, String fieldName, String value) {
		StringBuffer cardStoreSb = new StringBuffer("");

		// if (configDataList == null)
		// throw new ModuleException("paymentbroker.cardconfigaration.not.found");

		String repDigitPattern = PaymentGatewayCard.REPLACE_PATTERN_DIGIT;
		String repStringPattern = PaymentGatewayCard.REPLACE_PATTERN_CHAR;
		String repChar = PaymentGatewayCard.REPLACE_STRING;
		boolean isFiledNameValid = false;
		String whatToStore = null;

		if (value != null && !value.trim().isEmpty() && fieldName != null && !fieldName.trim().isEmpty()) {
			for (CardDetailConfigDTO configDTO : configDataList) {
				whatToStore = configDTO.getWhatToStore();
				isFiledNameValid = fieldName.equalsIgnoreCase(configDTO.getFieldName());
				// Card Number
				if (FieldName.CARDNUMBER.code().equalsIgnoreCase(fieldName) && isFiledNameValid) {
					if (PaymentGatewayCard.CardNumber.ALL.code().equalsIgnoreCase(whatToStore) || value.trim().length() < 5) {
						cardStoreSb.append(value);
					} else if (PaymentGatewayCard.CardNumber.FIRSTSIXDIGITANDLASTFOURDIGIT.code().equalsIgnoreCase(whatToStore)) {
						cardStoreSb.append(value.substring(0, 6));
						cardStoreSb.append(value.substring(6, value.length() - 4).replaceAll(repDigitPattern, repChar));
						cardStoreSb.append(value.substring(value.length() - 4));
					} else if (PaymentGatewayCard.CardNumber.LASTFOURDIGIT.code().equalsIgnoreCase(whatToStore)) {
						cardStoreSb.append(value.substring(0, value.length() - 4).replaceAll(repDigitPattern, repChar));
						cardStoreSb.append(value.substring(value.length() - 4));
					} else if (PaymentGatewayCard.CardNumber.NONE.code().equalsIgnoreCase(whatToStore)) {
						cardStoreSb.append(value.replaceAll(repDigitPattern, repChar));
					}
					break;

				} else if (FieldName.EXPIRYDATE.code().equalsIgnoreCase(fieldName) && isFiledNameValid) {
					// Card expiry date
					if (PaymentGatewayCard.CardExpiryDate.ALL.code().equalsIgnoreCase(whatToStore)) {
						cardStoreSb.append(value);
					} else if (PaymentGatewayCard.CardExpiryDate.NONE.code().equalsIgnoreCase(whatToStore)) {
						cardStoreSb.append(value.replaceAll(repDigitPattern, repChar));
					}
					break;
				} else if (FieldName.CVV.code().equals(fieldName) && isFiledNameValid) {
					// Card CVV
					if (PaymentGatewayCard.CardCVV.ALL.code().equalsIgnoreCase(whatToStore)) {
						cardStoreSb.append(value);
					} else if (PaymentGatewayCard.CardCVV.NONE.code().equalsIgnoreCase(whatToStore)) {
						cardStoreSb.append(value.replaceAll(repDigitPattern, repChar));
					}
					break;
				} else if (FieldName.CARDHOLDERNAME.code().equals(fieldName) && isFiledNameValid) {
					// Card Holder name
					if (PaymentGatewayCard.CardHolderName.ALL.code().equalsIgnoreCase(whatToStore)) {
						cardStoreSb.append(value);
					} else if (PaymentGatewayCard.CardCVV.NONE.code().equalsIgnoreCase(whatToStore)) {
						cardStoreSb.append(value.replaceAll(repStringPattern, repChar));
					}
					break;
				}
			}
		}

		return cardStoreSb.toString();
	}

	public static Map<String, String> getDBStroreData(List<CardDetailConfigDTO> configDataList, String cardNumber,
			String cardHolderName, String expiryDate, String cvv) {

		Map<String, String> dbStoreDataMap = new HashMap<String, String>();

		dbStoreDataMap.put(FieldName.CARDNUMBER.code(), getDBStroreData(configDataList, FieldName.CARDNUMBER.code(), cardNumber));
		dbStoreDataMap.put(FieldName.CARDHOLDERNAME.code(),
				getDBStroreData(configDataList, FieldName.CARDHOLDERNAME.code(), cardHolderName));
		dbStoreDataMap.put(FieldName.EXPIRYDATE.code(), getDBStroreData(configDataList, FieldName.EXPIRYDATE.code(), expiryDate));
		dbStoreDataMap.put(FieldName.CVV.code(), getDBStroreData(configDataList, FieldName.CVV.code(), cvv));

		return dbStoreDataMap;

	}

}
