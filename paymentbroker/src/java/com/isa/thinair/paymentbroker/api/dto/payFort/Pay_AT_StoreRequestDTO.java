package com.isa.thinair.paymentbroker.api.dto.payFort;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.datatype.XMLGregorianCalendar;

public class Pay_AT_StoreRequestDTO implements Serializable{
	
	// later implments with a common class for Requests
	private static final long serialVersionUID = -8303009098531544L;
	
	private String merchantID;
	
	private String orderID;
	
	private BigDecimal amount;
	
	private String currency;
	
	private String itemName;
	
	private XMLGregorianCalendar expiryDate;
	
	private String 	clientCountry;
	
	private String 	clientName;
	
	private String clientEmail;
	
	private String clientMobile;
	
	private String ticketNumber;
	
	private String serviceName;
	
	private String signature;
	
	private XMLGregorianCalendar msgDate;

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public XMLGregorianCalendar getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(XMLGregorianCalendar expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientEmail() {
		return clientEmail;
	}

	public void setClientEmail(String clientEmail) {
		this.clientEmail = clientEmail;
	}

	public String getClientMobile() {
		return clientMobile;
	}

	public void setClientMobile(String clientMobile) {
		this.clientMobile = clientMobile;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public XMLGregorianCalendar getMsgDate() {
		return msgDate;
	}

	public void setMsgDate(XMLGregorianCalendar msgDate) {
		this.msgDate = msgDate;
	}

	public String getClientCountry() {
		return clientCountry;
	}

	public void setClientCountry(String clientCountry) {
		this.clientCountry = clientCountry;
	}

}
