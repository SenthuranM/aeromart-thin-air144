package com.isa.thinair.paymentbroker.api.dto.behpardakht;

import java.io.Serializable;

public class BehpardakhtResponse extends BehpardakhtCommonResponse implements Serializable {

	private static final long serialVersionUID = -190909098464589L;

	public static final String RESPONSE_CODE_0 = "Ref_Id";
	public static final String RESPONSE_CODE_11 = "ResCode";
	public static final String RESPONSE_CODE_12 = "SaleOrder_Id";
	public static final String RESPONSE_CODE_13 = "SaleReference_Id";
	public static final String RESPONSE_CODE_14 = "CardHolderInfo";

	public static final String REF_NO = "RefId";

	private String Response;
	private String refNumber;
	private boolean success = false;

	private String error;

	// TO-DO
	// need to set the constructor
	// implement the setResponce method
	// need to override the toString method and write the javadocs

	public String getResponse() {
		return Response;
	}

	public void setResponse(String response) {
		Response = response;
	}

	public String getRefNumber() {
		return refNumber;
	}

	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String toString() {
		StringBuffer resBuff = new StringBuffer();
		resBuff.append("Behpardakht Response Details [");
		resBuff.append("RefId: " + this.RefId);
		resBuff.append(", ResCode: " + this.ResCode);
		resBuff.append(", SaleOrderId: " + this.SaleOrderId);
		resBuff.append(", SaleReferenceId: " + this.SaleReferenceId);
		resBuff.append(", CardHolderInfo: " + this.CardHolderInfo);
		resBuff.append(", CardHolderInfo: " + this.CardHolderInfo);
		resBuff.append(", Response: " + this.Response);
		resBuff.append(", success: " + this.success);
		resBuff.append(" ] ");
		return resBuff.toString();
	}

}
