/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.bl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.paymentbroker.api.dto.CreditCardPaymentStatusDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.cyberplus.CyberPlusPaymentUtils;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.CollectionUtils;

/**
 * Main Template Pattern for all Payment Gateway implementations
 * 
 * @author Nilindra Fernando
 * @since 2.0
 */
public abstract class PaymentBrokerTemplate {

	public static String MODE_OF_SERVICE_REFUND_PREFIX = "-R";

	public static String MODE_OF_SERVICE_VOID_PREFIX = "-V";

	public static String MODE_OF_SERVICE_QUERY_PREFIX = "-Q";

	public static String MODE_OF_SERVICE_CAPTURE_PREFIX = "-C";

	public static String MODE_OF_SERVICE_STATUS_PREFIX = "-S";

	public static String MODE_OF_SERVICE_PAYMENT_PREFIX = "-P";

	public static String MODE_OF_SERVICE_CANCEL_PREFIX = "-X";

	protected static final ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	private static final Log log = LogFactory.getLog(PaymentBrokerTemplate.class);

	protected String brokerType;

	protected String merchantId;

	protected String secureKeyLocation;

	protected String keystoreName;

	protected String logPath;

	protected String proxyHost;

	protected String proxyPort;

	protected String ipgURL;

	protected String requestMethod;

	protected String merchantAccessCode;

	protected String locale;

	protected String orderInfo;

	protected String version;

	protected String paymentClientPath;

	protected Map<String, String> mapCardType;

	protected boolean checksumRequired;

	protected String secureSecret;

	protected String user;

	protected String password;

	protected String voidUser;

	protected String voidPassword;

	protected String emailDefault;

	protected boolean useProxy;

	protected String refundWithoutCardDetails;

	protected PaymentQueryDR paymentQueryDR = null;

	protected IPGIdentificationParamsDTO ipgIdentificationParamsDTO;

	protected boolean enableRefundByScheduler;

	protected String reversalRequestMethod;

	protected String reversalURL;

	protected String reversalUser;

	protected String reversalPassword;

	protected boolean reversalChecksumRequired;

	protected String reversalSecureSecret;

	protected boolean reversalUseProxy;

	protected String refundEnabled;

	protected boolean cancelBeforeRefund;

	protected boolean voidBeforeRefund;

	protected String xmlResponse;

	protected String switchToExternalURL;

	protected String supportOffLinePayment;

	protected String merchantPostURL;

	protected String serialNumber;

	protected String receiptLinkText;

	protected Map<String, String> paymentMethods;

	protected String keyStoreKeyPassword;

	protected String keyStorePassword;

	protected String keyStoreAlias;

	protected boolean viewPaymentInIframe;

	protected String queryURL;

	protected boolean restrictAttempt3D;

	protected String hostIPAddress;

	protected boolean supportOnlyFullRefund;

	protected String clientID;
	
	protected String refundUser;
	
	protected String refundPass;
	
	protected String exeTime;

	protected String dynamicTemplateUrl;

	protected boolean immediateRefund;
	
	protected String merchantAccount;
	
	protected String terminalCode;
	
	protected String actionId;

	protected String verifyURL;
	
	protected boolean allowSendCancelRequest;
	
	protected Map<String, String> mapPGWOptions;

	public abstract String getPaymentGatewayName() throws ModuleException;

	/**
	 * Loads Configuration
	 */
	public Properties getProperties() {
		Properties props = new Properties();

		props.put("brokerType", PlatformUtiltiies.nullHandler(brokerType));
		props.put("requestMethod", PlatformUtiltiies.nullHandler(requestMethod));
		props.put("merchantId", PlatformUtiltiies.nullHandler(merchantId));
		props.put("secureKeyLocation", PlatformUtiltiies.nullHandler(secureKeyLocation));
		props.put("logPath", PlatformUtiltiies.nullHandler(logPath));
		props.put("ipgURL", PlatformUtiltiies.nullHandler(ipgURL));
		props.put("merchantAccessCode", PlatformUtiltiies.nullHandler(merchantAccessCode));
		props.put("locale", PlatformUtiltiies.nullHandler(locale));
		props.put("refundWithoutCardDetails", PlatformUtiltiies.nullHandler(refundWithoutCardDetails));
		props.put("secureSecret", PlatformUtiltiies.nullHandler(secureSecret));
		props.put("emailDefault", PlatformUtiltiies.nullHandler(emailDefault));
		props.put("refundEnabled", PlatformUtiltiies.nullHandler(refundEnabled));
		props.put("xmlResponse", PlatformUtiltiies.nullHandler(xmlResponse));
		props.put("switchToExternalURL", PlatformUtiltiies.nullHandler(switchToExternalURL));
		props.put("supportOffLinePayment", PlatformUtiltiies.nullHandler(supportOffLinePayment));
		props.put("paymentMethods", PlatformUtiltiies.nullHandler(paymentMethods));
		props.put("viewPaymentInIframe", PlatformUtiltiies.nullHandler(viewPaymentInIframe));
		props.put("supportOnlyFullRefund", PlatformUtiltiies.nullHandler(supportOnlyFullRefund));
		props.put("clientID", PlatformUtiltiies.nullHandler(clientID));
		props.put("dynamicTemplateUrl", PlatformUtiltiies.nullHandler(dynamicTemplateUrl));
		props.put("verifyURL", PlatformUtiltiies.nullHandler(verifyURL));
		props.put("mapPGWOptions", PlatformUtiltiies.nullHandler(mapPGWOptions));
		return props;
	}

	public ServiceResponce resolvePartialPaymentsForVouchers(Collection<IPGQueryDTO> ipgQueryDTOs, boolean refundFlag)
			throws ModuleException {
		Collection<Integer> oResultIF = new ArrayList<>();
		Collection<Integer> oResultIS = new ArrayList<>();
		DefaultServiceResponse response = new DefaultServiceResponse(false);

		for (IPGQueryDTO ipgQueryDTO : ipgQueryDTOs) {
			try {
				List<VoucherDTO> vouchers = AirproxyModuleUtils.getVoucherBD().getVouchersFromGroupId(ipgQueryDTO.getPnr());
				if (CollectionUtils.isEmpty(vouchers)) {

					CreditCardTransaction creditCardTransaction = loadAuditTransactionByTnxRefNo(
							ipgQueryDTO.getPaymentBrokerRefNo());
					CreditCardPayment creditCardPayment = createCreditCardPayment(ipgQueryDTO, creditCardTransaction);

					ServiceResponce serviceResponce = reverse(creditCardPayment, creditCardPayment.getPnr(),
							creditCardPayment.getAppIndicator(), creditCardPayment.getTnxMode());

					if (serviceResponce.isSuccess()) {
						oResultIS.add(creditCardTransaction.getTemporyPaymentId());
					} else {
						oResultIF.add(creditCardTransaction.getTemporyPaymentId());
					}
				} else {
					log.error("Vouchers exist but paymnet was not successful");
				}
			} catch (ModuleException exception) {
				log.error("Exception occured proceesing voucher purchase details", exception);
			}
		}
		response.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF, oResultIF);
		response.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS, oResultIS);
		response.setSuccess(true);
		return response;
	}

	private CreditCardPayment createCreditCardPayment(IPGQueryDTO ipgQueryDTO, CreditCardTransaction creditCardTransaction) {
		PreviousCreditCardPayment prevCCPayment = new PreviousCreditCardPayment();
		prevCCPayment.setPaymentBrokerRefNo(creditCardTransaction.getTransactionRefNo());

		CreditCardPayment creditCardPayment = new CreditCardPayment();
		creditCardPayment.setPreviousCCPayment(prevCCPayment);
		creditCardPayment.setAmount(ipgQueryDTO.getAmount().toString());
		creditCardPayment.setCurrency(ipgQueryDTO.getPaymentCurrencyCode());
		creditCardPayment.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
		creditCardPayment.setPaymentBrokerRefNo(creditCardTransaction.getTransactionRefNo());
		creditCardPayment.setIpgIdentificationParamsDTO(ipgQueryDTO.getIpgIdentificationParamsDTO());
		creditCardPayment.setPnr(creditCardTransaction.getTransactionReference());
		creditCardPayment.setTemporyPaymentId(creditCardTransaction.getTemporyPaymentId());
		return creditCardPayment;
	}

	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		return new DefaultServiceResponse(false);
	}

	/**
	 * Audit the Transaction
	 * 
	 * @param pnr
	 * @param merchantId
	 * @param merchantTnxId
	 * @param temporyPaymentId
	 * @param serviceType
	 * @param strRequest
	 * @param strResponse
	 * @param paymentGatwayName
	 * @param isSuccess
	 * @return
	 * @throws ModuleException
	 */
	public CreditCardTransaction auditTransaction(String pnr, String merchantId, String merchantTnxId, Integer temporyPaymentId,
			String serviceType, String strRequest, String strResponse, String paymentGatwayName, String transactionId,
			boolean isSuccess) throws ModuleException {
		CreditCardTransaction transaction = new CreditCardTransaction();

		SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddHHmmss");
		transaction.setTransactionTimestamp(fmt.format(new Date()));
		transaction.setTransactionReference(pnr);
		transaction.setTransactionId(transactionId);
		transaction.setTransactionRefCccompany(merchantId);
		transaction.setTempReferenceNum(merchantTnxId);
		transaction.setTemporyPaymentId(temporyPaymentId);
		transaction.setServiceType(serviceType);
		transaction.setRequestText(strRequest);
		transaction.setResponseText(strResponse);
		transaction.setPaymentGatewayConfigurationName(paymentGatwayName);

		if (isSuccess) {
			transaction.setResultCode(0);
			transaction.setTransactionResultCode(1);
		}

		PaymentBrokerUtils.getPaymentBrokerBD().saveTransaction(transaction);

		return transaction;
	}

	/**
	 * Audit the transaction at once
	 * 
	 * @param merchantId
	 * @param merchantTnxId
	 * @param serviceType
	 * @param strRequest
	 * @param strResponse
	 * @param paymentGatwayName
	 * @param transactionId
	 * @param errorSpecification
	 * @param authorizationCode
	 * @param isSuccess
	 * @return
	 */
	protected CreditCardTransaction auditTransactionAtOnce(String merchantId, String merchantTnxId, String serviceType,
			String strRequest, String strResponse, String paymentGatwayName, String transactionId, String errorSpecification,
			String authorizationCode, boolean isSuccess) {
		CreditCardTransaction transaction = new CreditCardTransaction();

		SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddHHmmss");
		transaction.setTransactionTimestamp(fmt.format(new Date()));
		transaction.setTransactionId(transactionId);
		transaction.setTransactionRefCccompany(merchantId);
		transaction.setTempReferenceNum(merchantTnxId);
		transaction.setServiceType(serviceType);
		transaction.setRequestText(strRequest);
		transaction.setResponseText(strResponse);
		transaction.setPaymentGatewayConfigurationName(paymentGatwayName);
		transaction.setErrorSpecification(errorSpecification);
		transaction.setAidCccompnay(authorizationCode);

		if (isSuccess) {
			transaction.setResultCode(0);
			transaction.setTransactionResultCode(1);
		}

		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(transaction);

		return transaction;
	}

	/**
	 * Saves data in the T_CCARD_PAYMENT_STATUS table using a DTO
	 * @param creditCardPaymentStatusDTO
	 * @param ipgIdentificationParamsDTO
	 * @return
	 * @throws ModuleException
	 */

	public CreditCardTransaction auditCCTransaction(CreditCardPaymentStatusDTO creditCardPaymentStatusDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {

		CreditCardTransaction creditCardTransaction = auditTransaction(creditCardPaymentStatusDTO.getPnr(),
				getMerchantId(), creditCardPaymentStatusDTO.getMerchantTnxId(),
				creditCardPaymentStatusDTO.getTemporaryPaymentId(),
				creditCardPaymentStatusDTO.getServiceType() != null ?
						bundle.getString(creditCardPaymentStatusDTO.getServiceType().getCode()) :
						"", creditCardPaymentStatusDTO.getStrRequest(), creditCardPaymentStatusDTO.getStrResponse(),
				ipgIdentificationParamsDTO.getFQIPGConfigurationName(), creditCardPaymentStatusDTO.getTransactionId(),
				creditCardPaymentStatusDTO.isSuccess());
		return creditCardTransaction;
	}

	/**
	 * Updates the Audit Transaction
	 * 
	 * @param tempPayId
	 * @param plainTextReceipt
	 * @param errorSpecification
	 * @param authorizationCode
	 * @param tnxResultCode
	 */
	protected void updateAuditTransactionByTmpPayId(int tempPayId, String plainTextReceipt, String errorSpecification,
			String authorizationCode, String transactionId, int tnxResultCode) {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransactionByTempPayId(
				tempPayId);

		creditCardTransaction.setResponseText(plainTextReceipt);
		creditCardTransaction.setErrorSpecification(errorSpecification);
		creditCardTransaction.setAidCccompnay(authorizationCode);

		creditCardTransaction.setResultCode(0);
		creditCardTransaction.setTransactionResultCode(tnxResultCode);
		creditCardTransaction.setTransactionId(transactionId);

		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(creditCardTransaction);
	}

	/**
	 * Updates the Audit Transaction
	 * 
	 * @param transactionRefNo
	 * @param plainTextReceipt
	 * @param errorSpecification
	 * @param authorizationCode
	 * @param transactionId
	 * @param tnxResultCode
	 */
	protected void updateAuditTransactionByTnxRefNo(int transactionRefNo, String plainTextReceipt, String errorSpecification,
			String authorizationCode, String transactionId, int tnxResultCode) {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				transactionRefNo);

		creditCardTransaction.setResponseText(plainTextReceipt);
		creditCardTransaction.setErrorSpecification(errorSpecification);
		creditCardTransaction.setAidCccompnay(authorizationCode);

		creditCardTransaction.setResultCode(0);
		creditCardTransaction.setTransactionResultCode(tnxResultCode);
		creditCardTransaction.setTransactionId(transactionId);

		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(creditCardTransaction);
	}

	protected void updateAuditTransactionByTnxRefNo(int transactionRefNo, String plainTextRequest, String paymentID) {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				transactionRefNo);
		creditCardTransaction.setRequestText(plainTextRequest);
		creditCardTransaction.setGatewayPaymentID(paymentID);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(creditCardTransaction);
	}

	protected void updateAuditTransactionByTnxRefNo(int transactionRefNo, String plainTextReceipt, String errorSpecification,
			String authorizationCode, String transactionId, int tnxResultCode, String resultCodeString, String payId) {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				transactionRefNo);

		creditCardTransaction.setResponseText(plainTextReceipt);
		creditCardTransaction.setErrorSpecification(errorSpecification);
		creditCardTransaction.setAidCccompnay(authorizationCode);

		creditCardTransaction.setResultCode(0);
		creditCardTransaction.setTransactionResultCode(tnxResultCode);
		creditCardTransaction.setResultCodeString(resultCodeString);
		creditCardTransaction.setTransactionId(transactionId);
		creditCardTransaction.setTransactionRefCccompany(payId);

		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(creditCardTransaction);
	}

	/**
	 * Updates the Audit Transaction
	 * 
	 * @param transactionRefNo
	 * @return
	 */
	protected CreditCardTransaction loadAuditTransactionByTnxRefNo(int transactionRefNo) {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				transactionRefNo);
		return creditCardTransaction;
	}

	/**
	 * load the Audit Transaction
	 * 
	 * @param temporyPayId
	 * @return
	 */
	protected CreditCardTransaction loadAuditTransactionByTmpPayID(int temporyPayId) {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransactionByTempPayId(
				temporyPayId);
		return creditCardTransaction;
	}

	/**
	 * load the Audit Transaction
	 * 
	 * @param transactionRefference
	 * @return
	 */
	protected CreditCardTransaction loadTransactionByReference(String transactionRefference) {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransactionByReference(
				transactionRefference);
		return creditCardTransaction;
	}

	/**
	 * @return Returns the brokerType.
	 */
	public String getBrokerType() {
		return brokerType;
	}

	/**
	 * @param brokerType
	 *            The brokerType to set.
	 */
	public void setBrokerType(String brokerType) {
		this.brokerType = brokerType;
	}

	/**
	 * @return Returns the ipgURL.
	 */
	public String getIpgURL() {
		return ipgURL;
	}

	/**
	 * @param ipgURL
	 *            The ipgURL to set.
	 */
	public void setIpgURL(String ipgURL) {
		this.ipgURL = ipgURL;
	}

	/**
	 * @return Returns the locale.
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * @param locale
	 *            The locale to set.
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	/**
	 * @return Returns the logPath.
	 */
	public String getLogPath() {
		return logPath;
	}

	/**
	 * @param logPath
	 *            The logPath to set.
	 */
	public void setLogPath(String logPath) {
		this.logPath = logPath;
	}

	/**
	 * @return Returns the merchantAccessCode.
	 */
	public String getMerchantAccessCode() {
		return merchantAccessCode;
	}

	/**
	 * @param merchantAccessCode
	 *            The merchantAccessCode to set.
	 */
	public void setMerchantAccessCode(String merchantAccessCode) {
		this.merchantAccessCode = merchantAccessCode;
	}

	/**
	 * @return Returns the merchantId.
	 */
	public String getMerchantId() {
		return merchantId;
	}

	/**
	 * @param merchantId
	 *            The merchantId to set.
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	/**
	 * @return Returns the orderInfo.
	 */
	public String getOrderInfo() {
		return orderInfo;
	}

	/**
	 * @param orderInfo
	 *            The orderInfo to set.
	 */
	public void setOrderInfo(String orderInfo) {
		this.orderInfo = orderInfo;
	}

	/**
	 * @return Returns the proxyHost.
	 */
	public String getProxyHost() {
		return proxyHost;
	}

	/**
	 * @param proxyHost
	 *            The proxyHost to set.
	 */
	public void setProxyHost(String proxyHost) {
		this.proxyHost = proxyHost;
	}

	/**
	 * @return Returns the proxyPort.
	 */
	public String getProxyPort() {
		return proxyPort;
	}

	/**
	 * @param proxyPort
	 *            The proxyPort to set.
	 */
	public void setProxyPort(String proxyPort) {
		this.proxyPort = proxyPort;
	}

	/**
	 * @return Returns the requestMethod.
	 */
	public String getRequestMethod() {
		return requestMethod;
	}

	/**
	 * @param requestMethod
	 *            The requestMethod to set.
	 */
	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}

	/**
	 * @return Returns the secureKeyLocation.
	 */
	public String getSecureKeyLocation() {
		return secureKeyLocation;
	}

	/**
	 * @param secureKeyLocation
	 *            The secureKeyLocation to set.
	 */
	public void setSecureKeyLocation(String secureKeyLocation) {
		this.secureKeyLocation = secureKeyLocation;
	}

	/**
	 * @return Returns the version.
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            The version to set.
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the paymentClientPath
	 */
	public String getPaymentClientPath() {
		return paymentClientPath;
	}

	/**
	 * @param paymentClientPath
	 *            the paymentClientPath to set
	 */
	public void setPaymentClientPath(String paymentClientPath) {
		this.paymentClientPath = paymentClientPath;
	}

	/**
	 * @return the mapCardType
	 */
	public Map<String, String> getMapCardType() {
		return mapCardType;
	}

	/**
	 * @param mapCardType
	 *            the mapCardType to set
	 */
	public void setMapCardType(Map<String, String> mapCardType) {
		this.mapCardType = mapCardType;
	}

	/**
	 * @return Returns the password.
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            The password to set.
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return Returns the secureSecret.
	 */
	public String getSecureSecret() {
		return secureSecret;
	}

	/**
	 * @param secureSecret
	 *            The secureSecret to set.
	 */
	public void setSecureSecret(String secureSecret) {
		this.secureSecret = secureSecret;
	}

	/**
	 * @return Returns the user.
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param user
	 *            The user to set.
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @return Returns the useProxy.
	 */
	public boolean isUseProxy() {
		if (SystemPropertyUtil.isDisableProxyGlobally()) {
			return false;
		} else {
			return useProxy;
		}
	}

	/**
	 * @param useProxy
	 *            The useProxy to set.
	 */
	public void setUseProxy(boolean useProxy) {
		this.useProxy = useProxy;
	}

	/**
	 * @return Returns the refundWithoutCardDetails.
	 */
	public String getRefundWithoutCardDetails() {
		return refundWithoutCardDetails;
	}

	/**
	 * @param refundWithoutCardDetails
	 *            The refundWithoutCardDetails to set.
	 */
	public void setRefundWithoutCardDetails(String refundWithoutCardDetails) {
		this.refundWithoutCardDetails = refundWithoutCardDetails;
	}

	/**
	 * Makes the Merchant Transaction Id
	 * 
	 * @param appIndicatorEnum
	 * @param auditId
	 * @param mode
	 *            PaymentConstants.MODE_OF_SERVICE
	 * @return
	 */
	protected String composeMerchantTransactionId(AppIndicatorEnum appIndicatorEnum, long auditId,
			PaymentConstants.MODE_OF_SERVICE mode) {
		if (mode.equals(PaymentConstants.MODE_OF_SERVICE.REFUND)) {
			return appIndicatorEnum.getCode() + (new Long(auditId)).toString() + MODE_OF_SERVICE_REFUND_PREFIX;
		} else if (mode.equals(PaymentConstants.MODE_OF_SERVICE.VOID)) {
			return appIndicatorEnum.getCode() + (new Long(auditId)).toString() + MODE_OF_SERVICE_VOID_PREFIX;
		} else if (mode.equals(PaymentConstants.MODE_OF_SERVICE.QUERYDR)) {
			return appIndicatorEnum.getCode() + (new Long(auditId)).toString() + MODE_OF_SERVICE_QUERY_PREFIX;
		} else if (mode.equals(PaymentConstants.MODE_OF_SERVICE.CAPTURE)) {
			return appIndicatorEnum.getCode() + (new Long(auditId)).toString() + MODE_OF_SERVICE_CAPTURE_PREFIX;
		} else if (mode.equals(PaymentConstants.MODE_OF_SERVICE.STATUS)) {
			return appIndicatorEnum.getCode() + (new Long(auditId)).toString() + MODE_OF_SERVICE_STATUS_PREFIX;
		} else {
			return appIndicatorEnum.getCode() + (new Long(auditId)).toString();
		}
	}

	/**
	 * Makes transaction reference.
	 */
	protected String composeAccelAeroTransactionRef(AppIndicatorEnum appIndicatorEnum, String siteId, String date,
			String transId, PaymentConstants.MODE_OF_SERVICE mode) {
		StringBuffer transactionRef = new StringBuffer();
		transactionRef.append(CyberPlusPaymentUtils.COLON).append(siteId).append(CyberPlusPaymentUtils.COLON).append(
				date)
				.append(CyberPlusPaymentUtils.COLON).append(transId).append(CyberPlusPaymentUtils.COLON);

		if (mode.equals(PaymentConstants.MODE_OF_SERVICE.PAYMENT)) {
			return appIndicatorEnum.getCode() + transactionRef.toString() + MODE_OF_SERVICE_PAYMENT_PREFIX;
		} else if (mode.equals(PaymentConstants.MODE_OF_SERVICE.REFUND)) {
			return appIndicatorEnum.getCode() + transactionRef.toString() + MODE_OF_SERVICE_REFUND_PREFIX;
		} else if (mode.equals(PaymentConstants.MODE_OF_SERVICE.CANCEL)) {
			return appIndicatorEnum.getCode() + transactionRef.toString() + MODE_OF_SERVICE_CANCEL_PREFIX;
		} else {
			return appIndicatorEnum.getCode() + transactionRef.toString();
		}
	}

	/**
	 * Makes transaction reference including new transaction id.
	 */
	protected String composeAccelAeroTransactionRef(AppIndicatorEnum appIndicatorEnum, String siteId, String date,
			String transId, int newTransId, PaymentConstants.MODE_OF_SERVICE mode) {
		StringBuffer transactionRef = new StringBuffer();
		transactionRef.append(CyberPlusPaymentUtils.COLON).append(siteId).append(CyberPlusPaymentUtils.COLON).append(
				date)
				.append(CyberPlusPaymentUtils.COLON).append(transId).append(CyberPlusPaymentUtils.COLON).append(
				newTransId)
				.append(CyberPlusPaymentUtils.COLON);

		if (mode.equals(PaymentConstants.MODE_OF_SERVICE.REFUND)) {
			return appIndicatorEnum.getCode() + transactionRef.toString() + MODE_OF_SERVICE_REFUND_PREFIX;
		} else {
			return appIndicatorEnum.getCode() + transactionRef.toString();
		}
	}

	/**
	 * Check Payment Gateway compatibility
	 * 
	 * @param ccTransaction
	 * @return
	 * @throws ModuleException
	 */
	protected ServiceResponce checkPaymentGatewayCompatibility(CreditCardTransaction ccTransaction) throws ModuleException {
		if (ccTransaction == null) {
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);
			return sr;
		} else if (!(ccTransaction.getPaymentGatewayConfigurationName().equals(this.getPaymentGatewayName()))) {
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.DIFFERENT_PAYMENT_GATEWAY_TRANSACTION);
			return sr;
		}
		return new DefaultServiceResponse(true);
	}

	/**
	 * Lookup the corresponding other IPG
	 * 
	 * @param serviceResponse
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @param paymentGatewayName
	 * @return
	 * @throws ModuleException
	 */
	protected ServiceResponce lookupOtherIPG(ServiceResponce serviceResponse, CreditCardPayment creditCardPayment, String pnr,
			AppIndicatorEnum appIndicator, TnxModeEnum tnxMode, String paymentGatewayName) throws ModuleException {

		String errorMsg = PlatformUtiltiies.nullHandler(
				serviceResponse.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE));

		if (PaymentBrokerInternalConstants.MessageCodes.DIFFERENT_PAYMENT_GATEWAY_TRANSACTION.equals(errorMsg)) {
			creditCardPayment.setIpgIdentificationParamsDTO(new IPGIdentificationParamsDTO(paymentGatewayName));
			return PaymentBrokerManager.refund(creditCardPayment, pnr, appIndicator, tnxMode);
		}

		return serviceResponse;
	}

	/**
	 * 
	 * @return QueryDR api
	 * @throws ModuleException
	 */
	public PaymentQueryDR getPaymentQueryDR() throws ModuleException {
		if (paymentQueryDR != null) {
			((PaymentBrokerTemplate) paymentQueryDR).setIPGIdentificationParams(this.ipgIdentificationParamsDTO);
		}
		return paymentQueryDR;
	}

	/**
	 * 
	 * @param paymentQueryDR
	 */
	public void setPaymentQueryDR(PaymentQueryDR paymentQueryDR) {
		this.paymentQueryDR = paymentQueryDR;
	}

	/**
	 * Validates and set IPGIdentificationParamsDTO
	 * 
	 * @param ipgIdentificationParamsDTO
	 * @throws ModuleException
	 */
	public void setIPGIdentificationParams(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		PaymentBrokerManager.validateIPGIdentificationParams(ipgIdentificationParamsDTO);
		this.ipgIdentificationParamsDTO = ipgIdentificationParamsDTO;
	}

	/**
	 * @return the enableRefundByScheduler
	 */
	public boolean isEnableRefundByScheduler() {
		return enableRefundByScheduler;
	}

	/**
	 * @param enableRefundByScheduler
	 *            the enableRefundByScheduler to set
	 */
	public void setEnableRefundByScheduler(boolean enableRefundByScheduler) {
		this.enableRefundByScheduler = enableRefundByScheduler;

	}

	/**
	 * @return the emailDefault
	 */
	public String getEmailDefault() {
		return emailDefault;
	}

	/**
	 * @param emailDefault
	 *            the emailDefault to set
	 */
	public void setEmailDefault(String emailDefault) {
		this.emailDefault = emailDefault;
	}

	/**
	 * @return the refundEnabled
	 */
	public String getRefundEnabled() {
		return refundEnabled;
	}

	/**
	 * @param refundEnabled
	 *            the emailDefault to set
	 */
	public void setRefundEnabled(String refundEnabled) {
		this.refundEnabled = refundEnabled;
	}

	/**
	 * @return the checksumRequired
	 */
	public boolean isChecksumRequired() {
		return checksumRequired;
	}

	/**
	 * @param checksumRequired
	 *            the checksumRequired to set
	 */
	public void setChecksumRequired(boolean checksumRequired) {
		this.checksumRequired = checksumRequired;
	}

	/**
	 * @return the reversalRequestMethod
	 */
	public String getReversalRequestMethod() {
		return reversalRequestMethod;
	}

	/**
	 * @param reversalRequestMethod
	 *            the reversalRequestMethod to set
	 */
	public void setReversalRequestMethod(String reversalRequestMethod) {
		this.reversalRequestMethod = reversalRequestMethod;
	}

	/**
	 * @return the reversalURL
	 */
	public String getReversalURL() {
		return reversalURL;
	}

	/**
	 * @param reversalURL
	 *            the reversalURL to set
	 */
	public void setReversalURL(String reversalURL) {
		this.reversalURL = reversalURL;
	}

	/**
	 * @return the reversalUser
	 */
	public String getReversalUser() {
		return reversalUser;
	}

	/**
	 * @param reversalUser
	 *            the reversalUser to set
	 */
	public void setReversalUser(String reversalUser) {
		this.reversalUser = reversalUser;
	}

	/**
	 * @return the reversalPassword
	 */
	public String getReversalPassword() {
		return reversalPassword;
	}

	/**
	 * @param reversalPassword
	 *            the reversalPassword to set
	 */
	public void setReversalPassword(String reversalPassword) {
		this.reversalPassword = reversalPassword;
	}

	/**
	 * @return the reversalChecksumRequired
	 */
	public boolean isReversalChecksumRequired() {
		return reversalChecksumRequired;
	}

	/**
	 * @param reversalChecksumRequired
	 *            the reversalChecksumRequired to set
	 */
	public void setReversalChecksumRequired(boolean reversalChecksumRequired) {
		this.reversalChecksumRequired = reversalChecksumRequired;
	}

	/**
	 * @return the reversalSecureSecret
	 */
	public String getReversalSecureSecret() {
		return reversalSecureSecret;
	}

	/**
	 * @param reversalSecureSecret
	 *            the reversalSecureSecret to set
	 */
	public void setReversalSecureSecret(String reversalSecureSecret) {
		this.reversalSecureSecret = reversalSecureSecret;
	}

	/**
	 * @return the cancelBeforeRefund
	 */
	public boolean isCancelBeforeRefund() {
		return cancelBeforeRefund;
	}

	/**
	 * @param cancelBeforeRefund
	 *            the cancelBeforeRefund to set
	 */
	public void setCancelBeforeRefund(boolean cancelBeforeRefund) {
		this.cancelBeforeRefund = cancelBeforeRefund;
	}

	public boolean isReversalUseProxy() {
		return reversalUseProxy;
	}

	public void setReversalUseProxy(boolean reversalUseProxy) {
		this.reversalUseProxy = reversalUseProxy;
	}

	public String getVoidUser() {
		return voidUser;
	}

	public void setVoidUser(String voidUser) {
		this.voidUser = voidUser;
	}

	public String getVoidPassword() {
		return voidPassword;
	}

	public void setVoidPassword(String voidPassword) {
		this.voidPassword = voidPassword;
	}

	public boolean isVoidBeforeRefund() {
		return voidBeforeRefund;
	}

	public void setVoidBeforeRefund(boolean voidBeforeRefund) {
		this.voidBeforeRefund = voidBeforeRefund;
	}

	public String getXmlResponse() {
		return xmlResponse;
	}

	public void setXmlResponse(String xmlResponse) {
		this.xmlResponse = xmlResponse;
	}

	public String isSwitchToExternalURL() {
		return switchToExternalURL;
	}

	public void setSwitchToExternalURL(String switchToExternalURL) {
		this.switchToExternalURL = switchToExternalURL;
	}

	public String getSupportOffLinePayment() {
		return supportOffLinePayment;
	}

	public void setSupportOffLinePayment(String supportOffLinePayment) {
		this.supportOffLinePayment = supportOffLinePayment;
	}

	public Map<String, String> getPaymentMethods() {
		return paymentMethods;
	}

	public void setPaymentMethods(Map<String, String> paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	public String getReceiptLinkText() {
		return receiptLinkText;
	}

	public void setReceiptLinkText(String receiptLinkText) {
		this.receiptLinkText = receiptLinkText;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getMerchantPostURL() {
		return merchantPostURL;
	}

	public void setMerchantPostURL(String merchantPostURL) {
		this.merchantPostURL = merchantPostURL;
	}

	/**
	 * @return the keyStoreKeyPassword
	 */
	public String getKeyStoreKeyPassword() {
		return keyStoreKeyPassword;
	}

	/**
	 * @param keyStoreKeyPassword
	 *            the keyStoreKeyPassword to set
	 */
	public void setKeyStoreKeyPassword(String keyStoreKeyPassword) {
		this.keyStoreKeyPassword = keyStoreKeyPassword;
	}

	/**
	 * @return the keyStorePassword
	 */
	public String getKeyStorePassword() {
		return keyStorePassword;
	}

	/**
	 * @param keyStorePassword
	 *            the keyStorePassword to set
	 */
	public void setKeyStorePassword(String keyStorePassword) {
		this.keyStorePassword = keyStorePassword;
	}

	/**
	 * @return the keyStoreAlias
	 */
	public String getKeyStoreAlias() {
		return keyStoreAlias;
	}

	/**
	 * @param keyStoreAlias
	 *            the keyStoreAlias to set
	 */
	public void setKeyStoreAlias(String keyStoreAlias) {
		this.keyStoreAlias = keyStoreAlias;
	}

	/**
	 * @return the viewPaymentInIframe
	 */
	public boolean isViewPaymentInIframe() {
		return viewPaymentInIframe;
	}

	/**
	 * @param viewPaymentInIframe
	 *            the viewPaymentInIframe to set
	 */
	public void setViewPaymentInIframe(boolean viewPaymentInIframe) {
		this.viewPaymentInIframe = viewPaymentInIframe;
	}

	/**
	 * @return the switchToExternalURL
	 */
	public String getSwitchToExternalURL() {
		return switchToExternalURL;
	}

	/**
	 * @return the keystoreName
	 */
	public String getKeystoreName() {
		return keystoreName;
	}

	/**
	 * @param keystoreName
	 *            the keystoreName to set
	 */
	public void setKeystoreName(String keystoreName) {
		this.keystoreName = keystoreName;
	}

	/**
	 * @return the queryURL
	 */
	public String getQueryURL() {
		return queryURL;
	}

	/**
	 * @param queryURL
	 *            the queryURL to set
	 */
	public void setQueryURL(String queryURL) {
		this.queryURL = queryURL;
	}

	/**
	 * @return the restrictAttempt3D
	 */
	public boolean isRestrictAttempt3D() {
		return restrictAttempt3D;
	}

	/**
	 * @param restrictAttempt3D
	 *            the restrictAttempt3D to set
	 */
	public void setRestrictAttempt3D(boolean restrictAttempt3D) {
		this.restrictAttempt3D = restrictAttempt3D;
	}

	/**
	 * @return the hostIPAddress
	 */
	public String getHostIPAddress() {
		return hostIPAddress;
	}

	/**
	 * @param hostIPAddress
	 *            the hostIPAddress to set
	 */
	public void setHostIPAddress(String hostIPAddress) {
		this.hostIPAddress = hostIPAddress;
	}

	/**
	 * @return the supportOnlyFullRefund
	 */
	public boolean isSupportOnlyFullRefund() {
		return supportOnlyFullRefund;
	}

	/**
	 * @param supportOnlyFullRefund
	 *            the supportOnlyFullRefund to set
	 */
	public void setSupportOnlyFullRefund(boolean supportOnlyFullRefund) {
		this.supportOnlyFullRefund = supportOnlyFullRefund;
	}

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public String getDynamicTemplateUrl() {
		return dynamicTemplateUrl;
	}

	public void setDynamicTemplateUrl(String dynamicTemplateUrl) {
		this.dynamicTemplateUrl = dynamicTemplateUrl;
	}

	public String getRefundUser() {
		return refundUser;
	}

	public void setRefundUser(String refundUser) {
		this.refundUser = refundUser;
	}

	public String getRefundPass() {
		return refundPass;
	}

	public void setRefundPass(String refundPass) {
		this.refundPass = refundPass;
	}

	public String getExeTime() {
		return exeTime;
	}

	public void setExeTime(String exeTime) {
		this.exeTime = exeTime;
	}

	public boolean isImmediateRefund() {
		return immediateRefund;
	}

	public void setImmediateRefund(boolean immediateRefund) {
		this.immediateRefund = immediateRefund;
	}

	public String getMerchantAccount() {
		return merchantAccount;
	}

	public void setMerchantAccount(String merchantAccount) {
		this.merchantAccount = merchantAccount;
	}

	public String getTerminalCode() {
		return terminalCode;
	}

	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}

	public String getActionId() {
		return actionId;
	}

	public void setActionId(String actionId) {
		this.actionId = actionId;
	}

	public String getVerifyURL() {
		return verifyURL;
	}

	public void setVerifyURL(String verifyURL) {
		this.verifyURL = verifyURL;
	}

	public boolean isAllowSendCancelRequest() {
		return allowSendCancelRequest;
	}

	public void setAllowSendCancelRequest(boolean allowSendCancelRequest) {
		this.allowSendCancelRequest = allowSendCancelRequest;
	}

	public Map<String, String> getMapPGWOptions() {
		return mapPGWOptions;
	}

	public void setMapPGWOptions(Map<String, String> mapPGWOptions) {
		this.mapPGWOptions = mapPGWOptions;
	}

	public boolean isOfflinePaymentsCancellationAllowed() {
		return false;
	}
}