package com.isa.thinair.paymentbroker.core.bl.passargad;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "result", "action", "transactionReferenceID", "invoiceNumber", "invoiceDate", "merchantCode",
		"terminalCode", "amount", "traceNumber", "referenceNumber", "transactionDate" })
@XmlRootElement(name = "resultObj")
public class CheckTransactionResponse {

	public static String TRUE = "True";

	public static String FALSE = "False";

	private String result;

	private String action;

	private String transactionReferenceID;

	private String invoiceNumber;

	private String invoiceDate;

	private String merchantCode;

	private String terminalCode;

	private String amount;

	private String traceNumber;

	private String referenceNumber;

	private String transactionDate;

	public String getTraceNumber() {
		return traceNumber;
	}

	@XmlElement(name = "traceNumber")
	public void setTraceNumber(String traceNumber) {
		this.traceNumber = traceNumber;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	@XmlElement(name = "referenceNumber")
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	@XmlElement(name = "transactionDate")
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	@XmlElement(name = "result")
	public void setResult(String result) {
		this.result = result;
	}

	public String getResult() {
		return result;
	}

	public String getAction() {
		return action;
	}

	@XmlElement(name = "action")
	public void setAction(String action) {
		this.action = action;
	}

	public String getTransactionReferenceID() {
		return transactionReferenceID;
	}

	@XmlElement(name = "transactionReferenceID")
	public void setTransactionReferenceID(String transactionReferenceID) {
		this.transactionReferenceID = transactionReferenceID;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	@XmlElement(name = "invoiceNumber")
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	@XmlElement(name = "invoiceDate")
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getMerchantCode() {
		return merchantCode;
	}

	@XmlElement(name = "merchantCode")
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public String getTerminalCode() {
		return terminalCode;
	}

	@XmlElement(name = "terminalCode")
	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}

	public String getAmount() {
		return amount;
	}

	@XmlElement(name = "amount")
	public void setAmount(String amount) {
		this.amount = amount;
	}
}
