package com.isa.thinair.paymentbroker.core.bl.knet;



import com.aciworldwide.commerce.gateway.plugins.e24PaymentPipe;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.core.bl.knet.KnetResponse.RESULTSTATUS;

/**
 * 
 * @author Pradeep Karunanayake
 *
 */
public class KnetUtil {
	
	private static final String ERROR_CODE_PREFIX = "knet.";
	
	public static String getLanguageCode(String custLanguage) {
		String language = KnetRequest.LANGUAGE.ENGLISH.getLanguage();		
		if (KnetRequest.ARABIC.equalsIgnoreCase(custLanguage)) {
			language = KnetRequest.LANGUAGE.ARABIC.getLanguage();
		}		
		return language;		
	}
	
	public static void addAdditionalData(IPGRequestDTO ipgRequestDTO, e24PaymentPipe pipe) {
		String phoneNo = ipgRequestDTO.getContactMobileNumber();
		if (phoneNo ==  null || phoneNo.trim().isEmpty()) {
			phoneNo = ipgRequestDTO.getContactPhoneNumber();
		}
		if (phoneNo != null) 
			phoneNo = StringUtil.extractNumberFromString(phoneNo);
		String  email = ipgRequestDTO.getEmail();		
				
		if (email != null && !email.trim().isEmpty())
			pipe.setUdf1(email);		
		if (phoneNo != null && !phoneNo.trim().isEmpty())
			pipe.setUdf2(phoneNo);			
	}
	
	public static String concatPaymentRequestString(IPGRequestDTO ipgRequestDTO, e24PaymentPipe pipe) {
		StringBuilder request = new StringBuilder();
		if (ipgRequestDTO != null) {
			request.append("PaymentID:").append(pipe.getPaymentId()).append(",");
			request.append("Action:").append(pipe.getAction()).append(",");
			request.append("Currency:").append(ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode()).append(",");
			request.append("Amount:").append(pipe.getAmt()).append(",");
			request.append("Language:").append(pipe.getLanguage()).append(",");
			request.append("TrackId:").append(pipe.getTrackId()).append(",");
			request.append("Udf5:").append(pipe.getUdf5()).append(",");
			request.append("ResponseURL:").append(pipe.getResponseURL()).append(",");
			request.append("Udf1:").append(pipe.getUdf1()).append(",");
			request.append("Udf2:").append(pipe.getUdf2()).append(",");
			request.append("Payment URL:").append(pipe.getPaymentPage()).append(",");
			request.append("Udf3:").append(pipe.getUdf3()).append(",");
			request.append("Udf4:").append(pipe.getUdf4()).append(",");
			request.append("Session ID:").append(ipgRequestDTO.getSessionID());
		}		
		return request.toString();
	}
	
	public static String getErrorCodeWithPrefix(RESULTSTATUS result) {
		return 	ERROR_CODE_PREFIX + result.toString();	
	}
	
	public static String getErrorCodeDescription(RESULTSTATUS result) {
		String despn = "";
		switch(result) {
			case CAPTURED : despn = "Transaction was approved."; break;
			case NOT_CAPTURED : despn = "Transaction was not approved."; break;
			case CANCELED : despn = "Canceled Transaction."; break;
			case DENIDED_BY_RISK : despn = "Risk denied the transaction."; break;
			case HOST_TIMEOUT : despn = "The authorization system did not respond within the timeout limit."; break;
			default : despn = "Not identified"; break;
		}		
		return despn;
	}

}
