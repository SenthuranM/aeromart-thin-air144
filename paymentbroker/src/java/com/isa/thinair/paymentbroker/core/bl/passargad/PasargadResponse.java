package com.isa.thinair.paymentbroker.core.bl.passargad;

import java.util.Map;

public class PasargadResponse {

	public static final String NO_VALUE = "";

	public static String RESULT = "result";

	public static String INVOICE_DATE = "iD";

	public static String INVOICE_NUMBER = "iN";

	public static String TRANSACTION_REF_NUMBER = "tref";

	public static String RESULT_MESSAGE = "resultMessage";

	private String status;

	private String result;

	private Map response = null;

	private String resultMessage;

	private String invoiceNumber;

	private String invoiceDate;

	private String transactionReferenceID;

	public void setReponse(Map reponse) {
		this.response = reponse;
		invoiceNumber = null2unknown((String) reponse.get(INVOICE_NUMBER));
		invoiceDate = null2unknown((String) reponse.get(INVOICE_DATE));
		transactionReferenceID = null2unknown((String) reponse.get(TRANSACTION_REF_NUMBER));
		result = null2unknown((String) reponse.get(RESULT));
		resultMessage = null2unknown((String) reponse.get(RESULT_MESSAGE));
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getTransactionReferenceID() {
		return transactionReferenceID;
	}

	public void setTransactionReferenceID(String transactionReferenceID) {
		this.transactionReferenceID = transactionReferenceID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Map getResponse() {
		return response;
	}

	private static String null2unknown(String in) {
		if (in == null || in.length() == 0) {
			return NO_VALUE;
		} else {
			return in;
		}
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

}
