package com.isa.thinair.paymentbroker.core.bl.mcpg;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MCPGResponse {

	private static Log log = LogFactory.getLog(MCPGResponse.class);

	public static final String NO_VALUE = "";
	public static final String VALID_HASH = "VALID HASH.";
	public static final String INVALID_HASH = "INVALID HASH.";
	public static final String INVALID_RESPONSE = "INVALID RESPONSE";

	private String txnResponseCode;

	private String merchTxnRef;

	private String authorizeID;

	private String cardType;

	// Response as a Map
	Map response = null;

	public void setReponse(Map reponse) {
		this.response = reponse;
		txnResponseCode = null2unknown((String) reponse.get("respcod"));
		merchTxnRef = null2unknown((String) reponse.get("invnum"));
		authorizeID = null2unknown((String) reponse.get("gtnum"));
		cardType = null2unknown((String) reponse.get("crdtype"));
	}

	private static String null2unknown(String in) {
		if (in == null || in.length() == 0) {
			return NO_VALUE;
		} else {
			return in;
		}
	}

	public String toString() {
		StringBuffer resBuff = new StringBuffer();

		// create a list
		List<String> fieldNames = new ArrayList<String>(response.keySet());

		// move through the list and create a series of URL key/value pairs
		int count = 0;
		for (String fieldName : fieldNames) {
			// add a '&' to the end if we have more fields coming.
			if (count != 0) {
				resBuff.append('&');
			}
			String fieldValue = (String) response.get(fieldName);
			resBuff.append(fieldName + " : " + fieldValue);
			count++;
		}

		if (resBuff.toString().length() < 2500) {
			return resBuff.toString();
		} else {
			// Response is too large to save in db field.
			log.debug("[MCPGResponse::toString()] Response before trimming -" + resBuff.toString());
			return resBuff.toString().substring(0, 2500);
		}
	}

	public String getTxnResponseCode() {
		return txnResponseCode;
	}

	public void setTxnResponseCode(String txnResponseCode) {
		this.txnResponseCode = txnResponseCode;
	}

	public String getMerchTxnRef() {
		return merchTxnRef;
	}

	public void setMerchTxnRef(String merchTxnRef) {
		this.merchTxnRef = merchTxnRef;
	}

	public String getAuthorizeID() {
		return authorizeID;
	}

	public void setAuthorizeID(String authorizeID) {
		this.authorizeID = authorizeID;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

}
