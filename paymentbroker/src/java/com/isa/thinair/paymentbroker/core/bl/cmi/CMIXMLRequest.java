package com.isa.thinair.paymentbroker.core.bl.cmi;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@XmlRootElement(name = "CC5Request")
public class CMIXMLRequest {
	@XmlElement
	private String Name;
	@XmlElement
	private String Password;
	@XmlElement
	private String ClientId;
	@XmlElement
	private String Type;
	@XmlElement
	private Double Total;
	@XmlElement
	private Integer Currency;
	@XmlElement
	private String Number;
	@XmlElement
	private String Expires;
	@XmlElement
	private String Cvv2Val;
	@XmlElement
	private String OrderId;
	@XmlElement
	private String TransId;
	@XmlElement
	private String Email;
	@XmlElement
	private Extra Extra;
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public BillTo getBillTo() {
		return BillTo;
	}
	public void setBillTo(BillTo billTo) {
		BillTo = billTo;
	}
	@XmlElement
	private BillTo BillTo;
	
	public String getName() {
		return Name;
	}
	public String getPassword() {
		return Password;
	}
	public String getClientId() {
		return ClientId;
	}
	public String getType() {
		return Type;
	}
	
	
	@Override
	public String toString() {
		return "CMIXMLRequest [Name=" + Name + ", Password=" + Password
				+ ", ClientId=" + ClientId + ", Type=" + Type + ", Total="
				+ Total + ", Currency=" + Currency + ", Number=" + Number
				+ ", Expires=" + Expires + ", Cvv2Val=" + Cvv2Val
				+ ", OrderId=" + OrderId + ", TransId=" + TransId + ", Extra="
				+ Extra + "]";
	}
	public String getTransId() {
		return TransId;
	}
	public void setTransId(String transId) {
		TransId = transId;
	}
	public String getNumber() {
		return Number;
	}
	public String getExpires() {
		return Expires;
	}
	
	public void setName(String name) {
		Name = name;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public void setClientId(String clientId) {
		ClientId = clientId;
	}
	public void setType(String type) {
		Type = type;
	}
	
	public void setNumber(String number) {
		Number = number;
	}
	public void setExpires(String expires) {
		Expires = expires;
	}
	
	
	public String getOrderId() {
		return OrderId;
	}
	public void setOrderId(String orderId) {
		OrderId = orderId;
	}
	public String getCvv2Val() {
		return Cvv2Val;
	}
	public void setCvv2Val(String cvv2Val) {
		Cvv2Val = cvv2Val;
	}
	public Double getTotal() {
		return Total;
	}
	public void setTotal(Double total) {
		Total = total;
	}
	public Integer getCurrency() {
		return Currency;
	}
	public void setCurrency(Integer currency) {
		Currency = currency;
	}
	public Extra getExtra() {
		return Extra;
	}
	public void setExtra(Extra extra) {
		Extra = extra;
	}
	
	
}
