package com.isa.thinair.paymentbroker.core.bl.unionpay;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.unionpay.acp.sdk.SDKConfig;
import com.unionpay.acp.sdk.SDKUtil;

public class PaymentBrokerUnionPayQueryDR extends PaymentBrokerUnionPayTemplate implements PaymentQueryDR {

	private static Log log = LogFactory.getLog(PaymentBrokerUnionPayQueryDR.class);

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	protected static final String AUTHORIZATION_SUCCESS = "5";

	protected static final String PAYMENT_SUCCESS = "9";

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		PaymentBrokerManager.validateIPGIdentificationParams(this.ipgIdentificationParamsDTO);
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	@Override
	public ServiceResponce query(CreditCardTransaction oCreditCardTransaction, IPGConfigsDTO ipgConfigsDTO,
			QUERY_CALLER operCaller) throws ModuleException {
		log.debug("[PaymentBrokerEDirhamQueryDR::query()] Begin");

		String errorSpecification = null;
		String merchantTxnReference = oCreditCardTransaction.getTempReferenceNum();
		String queryParamString = "RefNo:" + oCreditCardTransaction.getTransactionId() + ",MerchantId:" + getMerchantId();

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		log.debug("Query Request Params : " + queryParamString);

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		postDataMap.put(UnionPayConstants.VERSION, getVersion());
		postDataMap.put(UnionPayConstants.ENCODING, "UTF-8");
		postDataMap.put(UnionPayConstants.SIGN_METHOD, UnionPayConstants.SignatureMethod.RSA);
		postDataMap.put(UnionPayConstants.TXN_TYPE, UnionPayConstants.TxnType.STATE_INQUIRY);
		postDataMap.put(UnionPayConstants.TXN_SUB_TYPE, UnionPayConstants.TxnSubType.DEFAULT);
		postDataMap.put(UnionPayConstants.PRODUCT_TYPE, UnionPayConstants.ProductType.DEFAULT);
		postDataMap.put(UnionPayConstants.CHANNEL_TYPE, UnionPayConstants.ChannelType.INTERNET);
		postDataMap.put(UnionPayConstants.ACCESS_TYPE, UnionPayConstants.AccessType.MERCAHNT_DIRECT);
		postDataMap.put(UnionPayConstants.TXN_TIME, oCreditCardTransaction.getTransactionTimestamp());
		postDataMap.put(UnionPayConstants.MERCHANT_ID, getMerchantId());
		postDataMap.put(UnionPayConstants.ORDER_ID, oCreditCardTransaction.getTempReferenceNum());

		SDKUtil.signByCertInfo(postDataMap, "UTF-8", getSecureKeyLocation(), getKeyStorePassword());

		String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

		CreditCardTransaction ccTransaction = auditTransaction(oCreditCardTransaction.getTransactionReference(), getMerchantId(),
				merchantTxnReference, new Integer(oCreditCardTransaction.getTemporyPaymentId()),
				bundle.getString("SERVICETYPE_QUERY"), PaymentBrokerUtils.getRequestDataAsString(postDataMap), "",
				getPaymentGatewayName(), null, false);

		String ipgURL = getIpgURL() + getStatusTNXURLSuffix();

		postDataMap.put("USE_PROXY", isUseProxy() ? "Y" : "N");
		postDataMap.put("PROXY_HOST", getProxyHost());
		postDataMap.put("PROXY_PORT", getProxyPort());
		postDataMap.put("URL", ipgURL);

		if (log.isDebugEnabled()) {
			log.debug("Union pay Request Data : " + strRequestParams);
		}

		UnionPayResponse response = UnionPayPaymentUtils.getResponse(postDataMap, getConfigPropertiesLocation());
		String status;

		if (response != null && response.getResponseCode().equals(UnionPayConstants.ResponseCode.SUCCESS)) {
			log.debug("[PaymentBrokerUnionPayQueryDR::query() : Extract Status confirmed] ConfirmationID :"
					+ response.getTraceNo());

			status = IPGResponseDTO.STATUS_ACCEPTED;
			sr.setSuccess(true);
			sr.addResponceParam(PaymentConstants.AMOUNT, response.getTxnAmount());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, PaymentConstants.SERVICE_RESPONSE_QUERYDR);
			sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE, oCreditCardTransaction.getTransactionId());
			sr.addResponceParam("queryId", response.getOrigQueryId());
		 } else {
			status = IPGResponseDTO.STATUS_REJECTED;
			String error = null;
			// error = UnionPayPaymentUtils.getErrorInfo(response);

			errorSpecification = "Status:" + response.getResponseCode() + " Error code:" + response.getResponseCode()
					+ " Error Desc:" + response.getResponseMsg();
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, error);
		 }
		
		updateAuditTransactionByTnxRefNo(ccTransaction.getTransactionRefNo(),
				PaymentBrokerUtils.getRequestDataAsString(response.getResponse()), errorSpecification,
				oCreditCardTransaction.getTransactionId(), oCreditCardTransaction.getTransactionId(),
				Integer.parseInt(response.getResponseCode()));
		
		 sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID,
		 oCreditCardTransaction.getTransactionId());
		 sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE,
				response.getResponseCode());
		
		 log.debug("[PaymentBrokerUnionPayQueryDR::query() : Query Status : " + status + "] ");
		 log.debug("[PaymentBrokerUnionPayQueryDR::query()] End");

		return sr;
	}
}
