/**
 * 
 */
package com.isa.thinair.paymentbroker.core.bl.hsbc;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Asiri Liyanage
 */
public class PaymentBrokerHSBCMOTOImpl extends PaymentBrokerTemplate implements PaymentBroker {

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	private static Log log = LogFactory.getLog(PaymentBrokerHSBCMOTOImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.isa.thinair.paymentbroker.core.bl.PaymentBroker#charge(com.isa.thinair.paymentbroker.api.model.CreditCardPayment
	 * , java.lang.String, com.isa.thinair.paymentbroker.api.util.AppIndicatorEnum,
	 * com.isa.thinair.paymentbroker.api.util.TnxModeEnum)
	 */
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {

		// Get a merchant transaction id to identify txn in the payment server, in case of failure.
		String merchantTxnId = composeMerchantTransactionId(appIndicator, creditCardPayment.getTemporyPaymentId(),
				PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		HsbcIPGRequest hsbcIPGRequest = new HsbcIPGRequest();
		hsbcIPGRequest.setMerchantTxnRef(merchantTxnId);
		hsbcIPGRequest.setPaymentClientPath(paymentClientPath);
		hsbcIPGRequest.setMerchantId(merchantId);
		hsbcIPGRequest.setLocale(locale);
		hsbcIPGRequest.setCreditCardPayment(creditCardPayment);

		CreditCardTransaction creditCardTransaction = auditTransaction(creditCardPayment.getPnr(), merchantId, merchantTxnId,
				creditCardPayment.getTemporyPaymentId(), bundle.getString("SERVICETYPE_AUTHORIZE"),
				"[REQUEST AUDIT]::[PNR]:" + creditCardPayment.getPnr() + "::[MERCHANTID]::" + merchantId + "::[AMOUNT]::"
						+ creditCardPayment.getAmount() + "::[LOCALE]::" + locale, null, getPaymentGatewayName(), null, false);

		// Get the response from the payment server.
		HsbcIPGUtil hsbcIPGUtil = new HsbcIPGUtil();
		String responseString = "";
		String errorSpecification = "";
		String authorizationId = "";
		String transactionId = "";
		String errorCode = "";
		String message;
		int tnxResultCode = 0;
		boolean success = false;

		try {
			HsbcIPGResponse hsbcIPGResponse = hsbcIPGUtil.charge(hsbcIPGRequest);
			responseString = hsbcIPGResponse.toString();
			transactionId = hsbcIPGResponse.getIpgTxnId();
			authorizationId = hsbcIPGResponse.getAuthorizeID();
			errorCode = hsbcIPGResponse.getErrorCode();
			message = hsbcIPGResponse.getMessage();

			if (IPGResponseDTO.STATUS_ACCEPTED.equals(hsbcIPGResponse.getStatus())) {
				success = true;
				tnxResultCode = 1;
			} else {
				errorSpecification = errorCode + "::" + hsbcIPGResponse.getErrorSpecification() + "::" + message;
			}
		} catch (ModuleException e) {
			message = e.getExceptionCode();
			errorSpecification = e.getExceptionCode();
		}

		updateAuditTransactionByTnxRefNo(creditCardTransaction.getTransactionRefNo(), responseString, errorSpecification,
				authorizationId, transactionId, tnxResultCode);

		// Populate service response object
		DefaultServiceResponse serviceResponse = new DefaultServiceResponse(success);
		serviceResponse.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, message);
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, authorizationId);

		return serviceResponse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isa.thinair.paymentbroker.core.bl.PaymentBroker#getReponseData(java.util.Map, int)
	 */
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isa.thinair.paymentbroker.core.bl.PaymentBroker#getRequestData(com.isa.thinair.paymentbroker.api.dto.
	 * IPGRequestDTO)
	 */
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.isa.thinair.paymentbroker.core.bl.PaymentBroker#refund(com.isa.thinair.paymentbroker.api.model.CreditCardPayment
	 * , java.lang.String, com.isa.thinair.paymentbroker.api.util.AppIndicatorEnum,
	 * com.isa.thinair.paymentbroker.api.util.TnxModeEnum)
	 */
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		if (creditCardPayment.getPreviousCCPayment() != null) {
			CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
					creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return serviceResponse;
			}
		}

		String amount = PlatformUtiltiies.nullHandler(creditCardPayment.getAmount());
		String merchantTnxId = composeMerchantTransactionId(creditCardPayment.getAppIndicator(),
				creditCardPayment.getTemporyPaymentId(), PaymentConstants.MODE_OF_SERVICE.REFUND);

		CreditCardTransaction creditCardTransaction = auditTransaction(creditCardPayment.getPnr(), merchantId, merchantTnxId,
				creditCardPayment.getTemporyPaymentId(), bundle.getString("SERVICETYPE_MANUAL_REFUND"),
				"Manual Refund requested for " + amount, "Manual Refund responded for " + amount, getPaymentGatewayName(), null,
				true);

		DefaultServiceResponse sr = new DefaultServiceResponse(true);
		sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());

		return sr;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.isa.thinair.paymentbroker.core.bl.PaymentBroker#reverse(com.isa.thinair.paymentbroker.api.model.CreditCardPayment
	 * , java.lang.String, com.isa.thinair.paymentbroker.api.util.AppIndicatorEnum,
	 * com.isa.thinair.paymentbroker.api.util.TnxModeEnum)
	 */
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
				PaymentBrokerInternalConstants.MessageCodes.REVERSE_OPERATION_NOT_SUPPORTED);

		return sr;
	}

	/**
	 * Returns Payment Gateway Name
	 * 
	 * @throws ModuleException
	 */
	@Override
	public String getPaymentGatewayName() throws ModuleException {
		PaymentBrokerManager.validateIPGIdentificationParams(ipgIdentificationParamsDTO);
		return ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	/**
	 * Resolves partial payments [ Invokes via a scheduler operation ]
	 */
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, "paymentbroker.generic.operation.notsupported");
		return sr;
	}

	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
				PaymentBrokerInternalConstants.MessageCodes.CAPTURE_OPERATION_NOT_SUPPORTED);

		return sr;
	}

	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
				PaymentBrokerInternalConstants.MessageCodes.CAPTURE_STATUS_OPERATION_NOT_SUPPORTED);

		return sr;
	}

	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");		
	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
}
