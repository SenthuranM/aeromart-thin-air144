package com.isa.thinair.paymentbroker.core.bl.migs;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.migs.MIGSRequest;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.wsclient.api.service.WSClientBD;

public class PaymentBrokerMigsImpl extends PaymentBrokerTemplate implements PaymentBroker {

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	private static final String TRANSACTION_STATUS_ACCEPTED = "success";

	private static final String REFUND_SUCCESS = "Accepted";

	private static final String DEFAULT_TRANSACTION_CODE = "5";

	private static Log log = LogFactory.getLog(PaymentBrokerMigsImpl.class);

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		log.debug("[PaymentBrokerMigsImpl::refund()] Begin");
		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status;
		String errorSpecification;
		String merchTnxRef;

		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

		String refundAmountStr = creditCardPayment.getAmount();

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		if (creditCardPayment.getPreviousCCPayment() != null && getRefundEnabled() != null && getRefundEnabled().equals("true")) {
			if (log.isDebugEnabled()) {
				log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
				log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
				log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
				log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
				log.debug("CC Old pay Formatted sAmount : " + refundAmountStr);
				log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());
			}

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}

			merchTnxRef = composeMerchantTransactionId(appIndicator, creditCardPayment.getTemporyPaymentId(),
					PaymentConstants.MODE_OF_SERVICE.REFUND);

			Integer paymentBrokerRefNo;
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
			Date paymentDate = null;
			try {
				paymentDate = dateFormat.parse(ccTransaction.getTransactionTimestamp());
			} catch (ParseException e1) {
				log.error(e1);
			}
			dateFormat.applyPattern("dd/MM/yyyy");

			MIGSRequest refundRequest = new MIGSRequest();
			refundRequest.setClientCode(getClientID());
			refundRequest.setRefNo(merchTnxRef);
			refundRequest.setApplicationId(ccTransaction.getTempReferenceNum().toString());
			refundRequest.setAmount(refundAmountStr);
			refundRequest.setUser(getUser());
			refundRequest.setPassword(getPassword());
			refundRequest.setPaymentDate(dateFormat.format(paymentDate).toString());
			refundRequest.setRefundRequest();

			PaymentQueryDR queryDR = getPaymentQueryDR();
			ServiceResponce response = queryDR.query(ccTransaction,
					MigsPaymentUtils.getIPGConfigs(getMerchantId(), getPassword()),
					PaymentBrokerInternalConstants.QUERY_CALLER.VERIFY);
			CreditCardTransaction ccNewTransaction = auditTransaction(pnr, getMerchantId(), merchTnxRef,
					creditCardPayment.getTemporyPaymentId(), bundle.getString("SERVICETYPE_REFUND"), refundRequest.toString(),
					"", getPaymentGatewayName(), null, false);

			if (IPGResponseDTO.STATUS_ACCEPTED.equals(response.getResponseParam(PaymentConstants.STATUS))) {
				String refundResponse = null;
				MIGSResponse refundRes = new MIGSResponse();
				try {
					WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
					refundResponse = ebiWebervices.postRefundRequest(refundRequest);

					paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());
					refundRes.setRefundResponse(refundResponse);
				} catch (Exception e) {
					log.error(e);
					throw new ModuleException("paymentbroker.error.unknown");
				}
				if (log.isDebugEnabled()) {
					log.debug("Refund Response : " + refundResponse.toString());
				}

				if (REFUND_SUCCESS.equals(refundRes.getResult())) {
					transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
					status = IPGResponseDTO.STATUS_ACCEPTED;
					tnxResultCode = 1;
					errorSpecification = "";
					sr.setSuccess(true);
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
							PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
				} else {
					status = IPGResponseDTO.STATUS_REJECTED;
					String error = refundRes.getError();
					transactionMsg = error;
					tnxResultCode = 0;
					errorSpecification = "Status:" + status + " Error Desc:" + transactionMsg;
				}

				updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), refundResponse.toString(), errorSpecification,
						refundRes.getResultref(), refundRes.getResultref(), tnxResultCode);

				sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, refundRes.getResultref());
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, refundRes.getError());
			} else {
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);
			}
		} else {
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);

		}
		return sr;
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		String merchantTxnId = "" + UUID.randomUUID().hashCode();
		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();

		Map<String, String> postDataMap = new HashMap<String, String>();
		postDataMap.put(MIGSRequest.VPC_CLIENT, getClientID());
		postDataMap.put(MIGSRequest.AMOUNT, ipgRequestDTO.getAmount());
		postDataMap.put(MIGSRequest.VPC_IPG_URL, getIpgURL());
		postDataMap.put(MIGSRequest.VPC_RET_URL, ipgRequestDTO.getReturnUrl());
		postDataMap.put(MIGSRequest.ORDER_INFO, getOrderInfo());

		postDataMap.put(MIGSRequest.ORDER_ID, "" + merchantTxnId);

		String requestData = MigsPaymentUtils.getPostInputDataFormHTML(postDataMap);
		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"), requestData
						+ "," + sessionID, "", getPaymentGatewayName(), null, false);
		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(requestData);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);
		return ipgRequestResultsDTO;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO oipgResponseDTO) throws ModuleException {
		IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();

		MIGSResponse response = new MIGSResponse();
		String responseText = receiptyMap.values().iterator().next().toString();

		response.setPaymentXml(responseText);

		String status = response.getResultdesc();
		String appTxnId = response.getOrderid();
		String authorizationCode = response.getResult();
		String errorCode = null;
		String message = "";
		String errorSpecification;
		int tnxResultCode;

		if (TRANSACTION_STATUS_ACCEPTED.equals(status)) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			tnxResultCode = 1;
			errorSpecification = "";
		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
			errorCode = DEFAULT_TRANSACTION_CODE;
			errorSpecification = status + " " + errorCode + " " + message;
		}

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setMessage(message);
		ipgResponseDTO.setApplicationTransactionId(appTxnId);
		ipgResponseDTO.setAuthorizationCode(authorizationCode);
		ipgResponseDTO.setCardType(5);

		updateAuditTransactionByTnxRefNo(oipgResponseDTO.getPaymentBrokerRefNo(), responseText, errorSpecification,
				authorizationCode, null, tnxResultCode);

		return ipgResponseDTO;
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection creditCardPayment, boolean refundFlag) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		throw new ModuleException("Deferred response is not supported");
	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
}
