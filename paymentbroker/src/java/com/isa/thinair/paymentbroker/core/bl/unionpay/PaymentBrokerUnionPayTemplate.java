package com.isa.thinair.paymentbroker.core.bl.unionpay;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.TimeZone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class PaymentBrokerUnionPayTemplate extends PaymentBrokerTemplate implements PaymentBroker {

	private static Log log = LogFactory.getLog(PaymentBrokerUnionPayTemplate.class);

	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	protected String sha1In;

	protected String sha1Out;

	protected String maintenanceURL;

	protected String operation;
	
	protected String dynamicTemplateUrl;
	
	protected boolean enableDynamicTemplate;
	
	protected String aquirerId;

	protected String merchantCategory;

	protected String merchantName;

	protected String merchantAbbriviation;

	protected String frontEndTNXURLSuffix;

	protected String backEndTNXURLSuffix;

	protected String statusTNXURLSuffix;

	protected String tnxTimeout;

	protected String configPropertiesLocation;

	public String getConfigPropertiesLocation() {
		return configPropertiesLocation;
	}

	public void setConfigPropertiesLocation(String configPropertiesLocation) {
		this.configPropertiesLocation = configPropertiesLocation;
	}

	public String getMerchantAbbriviation() {
		return merchantAbbriviation;
	}

	public void setMerchantAbbriviation(String merchantAbbriviation) {
		this.merchantAbbriviation = merchantAbbriviation;
	}

	public Properties getProperties() {
		Properties props = super.getProperties();
		props.put("dynamicTemplateUrl", PlatformUtiltiies.nullHandler(dynamicTemplateUrl));	
		props.put("enableDynamicTemplate", PlatformUtiltiies.nullHandler(enableDynamicTemplate));	
		return props;
	}

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO()
				.loadTransaction(creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection creditCardPayment, boolean refundFlag) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	public String getAquirerId() {
		return aquirerId;
	}

	public void setAquirerId(String aquirerId) {
		this.aquirerId = aquirerId;
	}

	public String getMerchantCategory() {
		return merchantCategory;
	}

	public void setMerchantCategory(String merchantCategory) {
		this.merchantCategory = merchantCategory;
	}

	public String getTnxTimeout() {
		return tnxTimeout;
	}

	public void setTnxTimeout(String tnxTimeout) {
		this.tnxTimeout = tnxTimeout;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getDynamicTemplateUrl() {
		return dynamicTemplateUrl;
	}

	public void setDynamicTemplateUrl(String dynamicTemplateUrl) {
		this.dynamicTemplateUrl = dynamicTemplateUrl;
	}

	public boolean isEnableDynamicTemplate() {
		return enableDynamicTemplate;
	}

	public void setEnableDynamicTemplate(boolean enableDynamicTemplate) {
		this.enableDynamicTemplate = enableDynamicTemplate;
	}

	public String getFrontEndTNXURLSuffix() {
		return frontEndTNXURLSuffix;
	}

	public void setFrontEndTNXURLSuffix(String frontEndTNXURLSuffix) {
		this.frontEndTNXURLSuffix = frontEndTNXURLSuffix;
	}

	public String getBackEndTNXURLSuffix() {
		return backEndTNXURLSuffix;
	}

	public void setBackEndTNXURLSuffix(String backEndTNXURLSuffix) {
		this.backEndTNXURLSuffix = backEndTNXURLSuffix;
	}

	public String getStatusTNXURLSuffix() {
		return statusTNXURLSuffix;
	}

	public void setStatusTNXURLSuffix(String statusTNXURLSuffix) {
		this.statusTNXURLSuffix = statusTNXURLSuffix;
	}

	protected DateFormat getUnionPayDefaultDateFormat() {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+8"));
		return dateFormat;
	}

	protected String getTimeout() {
		Calendar date = Calendar.getInstance();
		long t = date.getTimeInMillis();
		Date tnxExpireTime = new Date(t + Long.valueOf(getTnxTimeout()));
		return getUnionPayDefaultDateFormat().format(tnxExpireTime);
	}

	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

}
