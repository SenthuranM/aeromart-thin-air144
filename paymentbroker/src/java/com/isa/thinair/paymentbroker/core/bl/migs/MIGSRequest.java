package com.isa.thinair.paymentbroker.core.bl.migs;

import java.io.Serializable;

public class MIGSRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String VPC_CLIENT = "vpc_Client";

	public static final String VPC_IPG_URL = "vpc_ipg_url";

	public static final String VPC_RET_URL = "vpc_ret_url";

	public static final String ORDER_ID = "order_id";

	public static final String AMOUNT = "amount";

	public static final String ORDER_INFO = "orderInfo";

	public static final String ACTION_CHARGE = "launchpay";

	public static final String ACTION_REFUND = "refund";

	public static final String ACTION_TRANSFER = "transfer";

	public static final String PAY_TYPE = "CBD.CC";

	public static final String TYPE = "batch";

	public static final String TRANSACTION_COUNT = "1";

	private String clientCode;
	private String transactionId;
	private String type;
	private String payType;
	private String batchId;
	private String transCount;
	private String refNo;
	private String amount;
	private String paymentDate;
	private String status;
	private String refundRequest;
	private String user;
	private String password;
	private String queryRequest;
	private String applicationId;

	public String getClientCode() {
		return clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getTransCount() {
		return transCount;
	}

	public void setTransCount(String transCount) {
		this.transCount = transCount;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRefundRequest() {
		return refundRequest;
	}

	public void setRefundRequest() {
		StringBuilder sb = new StringBuilder();
		sb.append("<Request>");
		sb.append("<Header>");
		sb.append("<ClientCode>" + getClientCode() + "</ClientCode> ");
		sb.append("<RefNo>" + getRefNo() + "</RefNo>");
		sb.append("<FromDate>" + getPaymentDate() + "</FromDate>");
		sb.append("<EndDate>" + getPaymentDate() + "</EndDate>");
		sb.append("<TransCount>" + TRANSACTION_COUNT + "</TransCount>");
		sb.append("<Amount>" + getAmount() + "</Amount>");
		sb.append("<PayType>" + PAY_TYPE + "</PayType>");
		sb.append("<ReqType>" + ACTION_REFUND + "</ReqType>");
		sb.append("</Header>");
		sb.append("<Body>");
		sb.append("<Transaction>");
		sb.append("<TransactionId>" + getApplicationId() + "</TransactionId>");
		sb.append("</Transaction>");
		sb.append("</Body>");
		sb.append("</Request>");
		this.refundRequest = sb.toString();
	}

	public String getQueryRequest() {
		return queryRequest;
	}

	public void setQueryRequest() {
		StringBuilder sb = new StringBuilder();
		sb.append("<Request>");
		sb.append("<Header>");
		sb.append("<ClientCode>" + getClientCode() + "</ClientCode>");
		sb.append("<Type>batch</Type>");
		sb.append("<PayType>" + PAY_TYPE + "</PayType>");
		sb.append("</Header>");
		sb.append("<Body>");
		sb.append("<Batch>");
		sb.append("<BatchId>" + getApplicationId() + "</BatchId>");
		sb.append("<TransCount>0</TransCount>");
		sb.append("<RefNo>" + getRefNo() + "</RefNo>");
		sb.append("<Amount>" + getAmount() + "</Amount>");
		sb.append("<Status></Status>");
		sb.append("<PaymentDate></PaymentDate>");
		sb.append("</Batch>");
		sb.append("</Body>");
		sb.append("</Request>");

		this.queryRequest = sb.toString();
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId
	 *            the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the applicationId
	 */
	public String getApplicationId() {
		return applicationId;
	}

	/**
	 * @param applicationId
	 *            the applicationId to set
	 */
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
}
