package com.isa.thinair.paymentbroker.core.bl.ebs;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.bl.ogone.OgonePaymentUtils;
import com.isa.thinair.paymentbroker.core.bl.ogone.OgoneResponse;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;

public class PaymentBrokerEbsECommerceImpl extends PaymentBrokerEbsTemplate {

	private static Log log = LogFactory.getLog(PaymentBrokerEbsECommerceImpl.class);
	
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		String finalAmount = PaymentBrokerUtils.getFormattedAmount(ipgRequestDTO.getAmount(), ipgRequestDTO.getNoOfDecimals());
		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		// Default language is en_US, it is fixed
		String language = EBSRequest.LANGUAGE_US;

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		postDataMap.put(EBSRequest.PSPID, getMerchantId());
		postDataMap.put(EBSRequest.ORDERID, merchantTxnId);
		postDataMap.put(EBSRequest.LANGUAGE, language);
		postDataMap.put(EBSRequest.AMOUNT, finalAmount);
		postDataMap.put(EBSRequest.CURRENCY, ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode());				
		postDataMap.put(EBSRequest.ACCEPTURL, ipgRequestDTO.getReturnUrl());
		postDataMap.put(EBSRequest.DECLINEURL, ipgRequestDTO.getReturnUrl());
		postDataMap.put(EBSRequest.EXCEPTIONURL, ipgRequestDTO.getReturnUrl());		
		// Fraud Checking data
		OgonePaymentUtils.addFraudCheckingData(postDataMap, ipgRequestDTO);
		// As these parameters should be captured from payment gateway. Unfortunately  they are not doing
		if (postDataMap.containsKey(EBSRequest.CUSTOMER_EMAIL)) {
			postDataMap.put(EBSRequest.CUSTOMER_NAME, postDataMap.get(EBSRequest.CUSTOMER_EMAIL));
		} else {
			postDataMap.put(EBSRequest.CUSTOMER_NAME, getEmailDefault());
		}
		
		if (!postDataMap.containsKey(EBSRequest.CUSTOMER_ADDRESS)) {
			postDataMap.put(EBSRequest.CUSTOMER_ADDRESS, EBSRequest.DEFAULT_VALUE);
		}
		
		if (!postDataMap.containsKey(EBSRequest.CUSTOMER_TOWN)) {
			postDataMap.put(EBSRequest.CUSTOMER_TOWN, EBSRequest.DEFAULT_VALUE);
		} 
		
		if (!postDataMap.containsKey(EBSRequest.CUSTOMER_ZIP)) {
			postDataMap.put(EBSRequest.CUSTOMER_ZIP, EBSRequest.DEFAULT_VALUE);
		} 
		
		if (!postDataMap.containsKey(EBSRequest.CUSTOMER_COUNTRY)) {
			postDataMap.put(EBSRequest.CUSTOMER_COUNTRY, EBSRequest.DEFAULT_VALUE);
		} 
		
		if (!postDataMap.containsKey(EBSRequest.CUSTOMER_EMAIL)) {
			postDataMap.put(EBSRequest.CUSTOMER_EMAIL, getEmailDefault());
		}		
		
		String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

		try {
			String shaSign = OgonePaymentUtils.computeSHA1(postDataMap, getSha1In());
			postDataMap.put(EBSRequest.SHASIGN, shaSign);
			strRequestParams = strRequestParams + "," + EBSRequest.SHASIGN + "=" + shaSign;
		} catch (Exception e) {
			if (log.isDebugEnabled())
				log.debug("Error computing SHA-1 : " + e.toString());
			throw new ModuleException(e.getMessage());
		}

		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();

		String requestDataForm = PaymentBrokerUtils.getPostInputDataFormHTML(postDataMap, getIpgURL());

		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				strRequestParams + "," + sessionID, "", getPaymentGatewayName(), null, false);

		if (log.isDebugEnabled()) {
			log.debug("EBS Request Data : " + strRequestParams + ", sessionID : " + sessionID);
		}

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(requestDataForm);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);
		ipgRequestResultsDTO.setPostDataMap(postDataMap);

		return ipgRequestResultsDTO;
	}
	
	public IPGResponseDTO getReponseData(Map fields, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		if (log.isDebugEnabled())
			log.debug("[PaymentBrokerEbsECommerceImpl::getReponseData()] Begin :  TempID:" +ipgResponseDTO.getTemporyPaymentId());

		boolean errorExists = false;
		int tnxResultCode;
		String errorCode = "";
		String status;
		String errorSpecification;
		String merchantTxnReference;
		String authorizeID;
		int cardType = 5; // default card type
		String hashValidated = null;
		String responseMismatch = "";
		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());

		// Calculate response time
		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time
		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

		EBSResponse ebsResp = new EBSResponse();
		ebsResp.setReponse(fields);
		if (log.isDebugEnabled())
			log.debug("[PaymentBrokerEbsECommerceImpl::getReponseData()] Mid Response -" + ebsResp);
		
		if (ebsResp != null && !(ebsResp.getStatus().trim().equals(""))) {
			// Validate the Hash if required
			if (ebsResp.getShaRequired().equals("N") || ebsResp.validateSHAHash(getSha1Out())) {
				// Secure Hash validation succeeded, add a data field to be
				// displayed later.
				hashValidated = OgoneResponse.VALID_HASH;

				// Check for correct response
				errorExists = true;

				// CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(paymentBrokerRefNo);
				// Check if response is not equal
				if (oCreditCardTransaction.getTempReferenceNum().equalsIgnoreCase(ebsResp.getOrderId())) {
					errorExists = false;
				} else {
					responseMismatch = OgoneResponse.INVALID_RESPONSE;

					// Use QueryDR to verify the transaction
					PaymentQueryDR QueryDR = getPaymentQueryDR();
					ServiceResponce serviceResponce = QueryDR.query(oCreditCardTransaction,
							OgonePaymentUtils.getIPGConfigs(getMerchantId(), getUser(), getPassword()),
							PaymentBrokerInternalConstants.QUERY_CALLER.INVALIDRESPONSE);
					if (serviceResponce.isSuccess()) {
						errorExists = false;
					}
				}
			} else {
				// Secure Hash validation failed, add a data field to be
				// displayed later.
				errorExists = true;
				hashValidated = OgoneResponse.INVALID_HASH;

				PaymentQueryDR QueryDR = getPaymentQueryDR();
				ServiceResponce serviceResponce = QueryDR.query(oCreditCardTransaction,
						OgonePaymentUtils.getIPGConfigs(getMerchantId(), getUser(), getPassword()),
						PaymentBrokerInternalConstants.QUERY_CALLER.HASHMISMATCH);
				if (serviceResponce.isSuccess()) {
					errorExists = false;
				}
			}
		} else {
			// Secure Hash was not validated,
			hashValidated = OgoneResponse.NO_VALUE;
			errorCode = ERROR_CODE_PREFIX;
		}

		merchantTxnReference = ebsResp.getOrderId();
		authorizeID = ebsResp.getAcceptance();
		cardType = getStandardCardType(ebsResp.getBrand());

		if ((AUTHORIZATION_SUCCESS.equals(ebsResp.getStatus()) || PAYMENT_SUCCESS.equals(ebsResp.getStatus()))
				&& !errorExists) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			tnxResultCode = 1;
			errorSpecification = "" + responseMismatch;
		} else {
			String responseStatus = ebsResp.getStatus();
			// Log unknown status transactions
			if (responseStatus.equals("50") || responseStatus.equals("51") || responseStatus.equals("52")
					|| responseStatus.equals("55") || responseStatus.equals("59")) {
				if (log.isDebugEnabled())
					log.debug("We received an unknown status for the transaction. Order id : " + ebsResp.getOrderId());
			}

			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
			errorCode = OgonePaymentUtils.getFilteredErrorCode(ebsResp.getStatus(), ebsResp.getNcError());
			errorSpecification = status + " " + hashValidated + " "
					+ OgonePaymentUtils.getResponseStatusDescription(ebsResp.getStatus(), ebsResp.getNcError()) + " "
					+ ebsResp.getNcError() + " " + ebsResp.getNcErrorPlus() + " " + responseMismatch;
		}

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setMessage(ebsResp.getNcErrorPlus());
		ipgResponseDTO.setApplicationTransactionId(merchantTxnReference);
		ipgResponseDTO.setAuthorizationCode(authorizeID);
		ipgResponseDTO.setCardType(cardType);
		String trimedDigits = "";
		if (ebsResp.getCardNo() != null && ebsResp.getCardNo().length() > 4) {
			trimedDigits = ebsResp.getCardNo().substring(ebsResp.getCardNo().length() - 4);
		}

		ipgResponseDTO.setCcLast4Digits(trimedDigits);
		String ogoneResponse = ebsResp.toString();
		updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), ogoneResponse, errorSpecification,
				ebsResp.getAcceptance(), ebsResp.getPayId(), tnxResultCode);
		if (log.isDebugEnabled())
			log.debug("[PaymentBrokerEbsECommerceImpl::getReponseData()] End -" + ebsResp.getOrderId() + "");

		return ipgResponseDTO;
	}
	
	
	protected int getStandardCardType(String card) {
		// [1-MASTER, 2-VISA, 5-GENERIC]
		int mapType = 5; // default card type
		if (card != null && !card.equals("") && mapCardType.get(card) != null) {
			String mapTypeStr = (String) mapCardType.get(card);
			mapType = Integer.parseInt(mapTypeStr);
		}
		return mapType;
	}


}
