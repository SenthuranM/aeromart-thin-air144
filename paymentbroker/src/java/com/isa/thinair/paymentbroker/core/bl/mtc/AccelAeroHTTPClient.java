package com.isa.thinair.paymentbroker.core.bl.mtc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;

import javax.xml.bind.JAXBException;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;

/**
 * 
 * @author Nasly
 * 
 */
public class AccelAeroHTTPClient {

	static Log log = LogFactory.getLog(AccelAeroHTTPClient.class);

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	public Object[] doSubmitCardTransaction(String pnr, int temporyPaymentId, String targetURL, Map<String, String> dataFields,
			boolean isCharge, String merchantTxnId, PaymentBrokerTemplate paymentBrokerTemplate) throws ModuleException {

		HTTPRequestParams httpRequestParams = new HTTPRequestParams();

		String requestMethod = isCharge ? paymentBrokerTemplate.getRequestMethod() : paymentBrokerTemplate
				.getReversalRequestMethod();

		if (PaymentBrokerBD.PaymentBrokerProperties.REQUEST_METHODS.POST.toString().equals(requestMethod)) {
			httpRequestParams.setHttpMethod(HTTPRequestParams.HTTP_METHOD_POST);
		} else {
			httpRequestParams.setHttpMethod(HTTPRequestParams.HTTP_METHOD_GET);
		}
		if (isCharge) {
			httpRequestParams.setUseProxy(paymentBrokerTemplate.isUseProxy());
		} else {
			httpRequestParams.setUseProxy(paymentBrokerTemplate.isReversalUseProxy());
		}
		httpRequestParams.setProxyHost(paymentBrokerTemplate.getProxyHost());
		httpRequestParams.setProxyPort(Integer.parseInt(paymentBrokerTemplate.getProxyPort()));
		httpRequestParams.setTargetUrl(targetURL);
		httpRequestParams.setRequestParamsMap(dataFields);

		if (log.isInfoEnabled()) {
			log.info("Payment Broker Request Parameters ==> \n\r" + getRequestParamsAsString(targetURL, dataFields));
		}

		AccelAeroHTTPClient accelAeroHTTPClient = new AccelAeroHTTPClient();
		String res = null;

		res = accelAeroHTTPClient.doSubmit(httpRequestParams);

		if (log.isInfoEnabled()) {
			log.info("Payment Broker Response Parameters ==> \n\r" + res);
		}

		String serviceType;

		if (isCharge) {
			serviceType = bundle.getString("SERVICETYPE_AUTHORIZE");
		} else {
			serviceType = bundle.getString("SERVICETYPE_REFUND");
		}

		CreditCardTransaction ccTransaction = paymentBrokerTemplate.auditTransaction(pnr, paymentBrokerTemplate.getMerchantId(),
				merchantTxnId, new Integer(temporyPaymentId), serviceType, getRequestParamsAsString(targetURL, dataFields), "",
				paymentBrokerTemplate.getPaymentGatewayName(), null, false);

		if (res == null || "".equals(res)) {
			log.error("Empty or null null response received from Payment Broker [brokerName="
					+ paymentBrokerTemplate.getPaymentGatewayName() + "]");
			throw new ModuleException("paymentbroker.parsingresponse.failed");
		}

		MTCXMLResponse response = null;

		try {
			response = XMLUtil.getResponse(res);
		} catch (JAXBException e) {
			log.error("Parsing Payment Broker resposne XML failed [res=" + res + "]", e);
			throw new ModuleException(e, "paymentbroker.parsingresponse.failed");
		}

		return new Object[] { response, new Integer(ccTransaction.getTransactionRefNo()) };
	}

	private String getRequestParamsAsString(String targetUrl, Map<String, String> fields) {
		StringBuffer sb = new StringBuffer();
		sb.append("url=" + targetUrl);
		String fieldName = null;
		for (Iterator<String> itr = fields.keySet().iterator(); itr.hasNext();) {
			fieldName = itr.next();
			if (!MTCMotoRequest.CARDNUMBER.equalsIgnoreCase(fieldName) && !MTCMotoRequest.CARDEXPIRY.equalsIgnoreCase(fieldName)
					&& !MTCMotoRequest.CVSCODE.equalsIgnoreCase(fieldName)
					&& !MTCMotoRequest.CARDOWNER.equalsIgnoreCase(fieldName)) {
				sb.append(", " + fieldName + "=" + fields.get(fieldName));
			}
		}
		return sb.toString();
	}

	public String doSubmit(HTTPRequestParams httpRequestParams) throws ModuleException {
		HttpClient httpClient = new HttpClient();

		if (httpRequestParams.isUseProxy()) {
			httpClient.getHostConfiguration().setProxy(httpRequestParams.getProxyHost(), httpRequestParams.getProxyPort());
		}

		HttpMethod httpMethod = null;

		if (HTTPRequestParams.HTTP_METHOD_GET.equals(httpRequestParams.getHttpMethod())) {

			String url = httpRequestParams.getTargetUrl();
			if (httpRequestParams.getRequestParamsMap() != null && httpRequestParams.getRequestParamsMap().size() > 0) {
				StringBuffer httpGETData = new StringBuffer();
				try {
					appendQueryFields(httpGETData, httpRequestParams.getRequestParamsMap());
				} catch (UnsupportedEncodingException e) {
					log.error(e);
					throw new ModuleException(e, "paymentbroker.error.unsupportedencoding");
				}
				url = url + "?" + httpGETData.toString();
			}

			httpMethod = new GetMethod(url);
		} else if (HTTPRequestParams.HTTP_METHOD_POST.equals(httpRequestParams.getHttpMethod())) {

			httpMethod = new PostMethod(httpRequestParams.getTargetUrl());
			if (httpRequestParams.getRequestParamsMap() != null && httpRequestParams.getRequestParamsMap().size() > 0) {
				for (String fieldName : httpRequestParams.getRequestParamsMap().keySet()) {
					((PostMethod) httpMethod).setParameter(fieldName, httpRequestParams.getRequestParamsMap().get(fieldName));
				}
			}

		} else {
			// throw not supported
			throw new ModuleException("paymentbroker.generic.operation.notsupported");
		}

		try {
			httpClient.executeMethod(httpMethod);
		} catch (HttpException he) {
			log.error(he);
			throw new ModuleException(he, "paymentbroker.error.ioException");
		} catch (IOException ioe) {
			log.error(ioe);
			throw new ModuleException(ioe, "paymentbroker.error.ioException");
		} catch (Exception e) {
			log.error(e);
			throw new ModuleException(e, "paymentbroker.communication.failure");
		}

		String redirectUrl = null;

		for (Header header : httpMethod.getResponseHeaders()) {
			if ("location".equalsIgnoreCase(header.getName())) {
				redirectUrl = header.getValue();
				break;
			}
		}
		if (redirectUrl != null) {
			if (redirectUrl.startsWith("./")) {
				redirectUrl = StringUtils.substring(httpRequestParams.getTargetUrl(), 0, httpRequestParams.getTargetUrl()
						.lastIndexOf("/"))
						+ redirectUrl.substring(1);
			}
			httpMethod = new GetMethod(redirectUrl);
			try {
				httpClient.executeMethod(httpMethod);
			} catch (HttpException he) {
				log.error(he);
				throw new ModuleException(he, "paymentbroker.error.ioException");
			} catch (IOException ioe) {
				log.error(ioe);
				throw new ModuleException(ioe, "paymentbroker.error.ioException");
			} catch (Exception e) {
				log.error(e);
				throw new ModuleException(e, "paymentbroker.communication.failure");
			}
		}

		String httpResponse;
		try {
			httpResponse = new String(readAll(httpMethod.getResponseBodyAsStream()));
		} catch (IOException e) {
			log.error(e);
			throw new ModuleException("paymentbroker.readingresponse.failed");
		}

		return httpResponse;
	}

	private static void appendQueryFields(StringBuffer buf, Map<String, String> fields) throws UnsupportedEncodingException {
		String fieldName = null;
		String fieldValue = null;
		for (Iterator<String> itr = fields.keySet().iterator(); itr.hasNext();) {
			fieldName = itr.next();
			fieldValue = (String) fields.get(fieldName);

			if ((fieldValue != null) && (fieldValue.length() > 0)) {
				buf.append(URLEncoder.encode(fieldName, "UTF-8"));
				buf.append('=');
				buf.append(URLEncoder.encode(fieldValue, "UTF-8"));
			}

			if (itr.hasNext()) {
				buf.append('&');
			}
		}
	}

	private static byte[] readAll(InputStream is) throws IOException {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		int len;

		while (true) {
			len = is.read(buf);
			if (len < 0) {
				break;
			}
			baos.write(buf, 0, len);
		}

		return baos.toByteArray();
	}

}
