package com.isa.thinair.paymentbroker.core.bl.saman;

public class SamanPRequest {

	public static final String MERCHANT_ID = "MID";

	public static final String TNX_ID = "ResNum";
	
	public static final String PNR = "ResNum1";

	public static final String AMOUNT = "Amount";

	public static final String REDIRECT_URL = "RedirectURL";

	public static final String TABLE_BORDER_COLOR = "TableBorderColor";

	public static final String TABLE_BG_COLOR = "TableBGColor";

	public static final String PAGE_BORDER_COLOR = "PageBorderColor";

	public static final String PAGE_BG_COLOR = "PageBGColor";

	public static final String TITLE_FONT = "TitleFont";

	public static final String TITLE_COLOR = "TitleColor";

	public static final String TITLE_SIZE = "TitleSize";

	public static final String TEXT_FONT = "TextFont";

	public static final String TEXT_COLOR = "TextColor";

	public static final String TEXT_SIZE = "TextSize";

	public static final String TYPE_TEXT_FONT = "TypeTextFont";

	public static final String TYPE_TEXT_COLOR = "TypeTextColor";

	public static final String TYPE_TEXT_SIZE = "TypeTextSize";

	public static final String LOGO_URL = "LogoURL";
	
	public static final String LANGUAGE = "language";
	

}
