package com.isa.thinair.paymentbroker.core.bl.migs;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import org.apache.commons.digester.Digester;
import org.apache.commons.lang.StringUtils;
import org.xml.sax.SAXException;

public class MIGSResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private String ClientID;

	private String action;

	private String orderid;

	private String result;

	private String resultdesc;

	private String resultref;

	private String paymentdate;

	private String paymenttime;

	private String resDate;

	private String sessionID;

	private String paymentXml;

	private String refundResponse;

	private String payType;

	private String batchId;

	private String refNo;

	private String amount;

	private String status;

	private String queryResponse;

	private String type;

	private String reqType;

	private String error;

	public void addResponseHeader(String clientCode, String payType) {
		this.ClientID = clientCode;
		this.payType = payType;
	}

	public void addResponseBody(String batchId, String refNo, String amount, String status, String paymentDate) {
		this.batchId = batchId;
		this.refNo = refNo;
		this.amount = amount;
		this.status = status;
		this.paymentdate = paymentDate;
	}

	public String getQueryResponse() {
		return queryResponse;
	}

	public void setQueryResponse(String queryResponse) {
		Digester digester = new Digester();
		InputStream is = new ByteArrayInputStream(queryResponse.toString().getBytes());
		this.queryResponse = queryResponse;

		digester.addObjectCreate("Response", "com.isa.thinair.paymentbroker.core.bl.migs.MIGSResponse");
		digester.addCallMethod("Response/Header/Clientcode", "setClientID", 0);
		digester.addCallMethod("Response/Header/PayType", "setPayType", 0);

		digester.addCallMethod("Response/Body/Batch/BatchId", "setBatchId", 0);
		digester.addCallMethod("Response/Body/Batch/Refno", "setRefNo", 0);
		digester.addCallMethod("Response/Body/Batch/Amount", "setAmount", 0);
		digester.addCallMethod("Response/Body/Batch/Status", "setStatus", 0);
		digester.addCallMethod("Response/Body/Batch/PaymentDate", "setPaymentdate", 0);

		try {
			MIGSResponse response = (MIGSResponse) digester.parse(is);
			ClientID = StringUtils.trim(response.getClientID());
			payType = StringUtils.trim(response.getPayType());
			batchId = StringUtils.trim(response.getBatchId());
			refNo = StringUtils.trim(response.getRefNo());
			amount = StringUtils.trim(response.getAmount());
			status = StringUtils.trim(response.getStatus());
			paymentdate = StringUtils.trim(response.getPaymentdate());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}

	public String getRefundResponse() {
		return refundResponse;
	}

	public void setRefundResponse(String refundResponse) {
		Digester digester = new Digester();
		InputStream is = new ByteArrayInputStream(refundResponse.toString().getBytes());
		this.refundResponse = refundResponse;

		digester.addObjectCreate("Response", "com.isa.thinair.paymentbroker.core.bl.migs.MIGSResponse");
		digester.addCallMethod("Response/Header/ClientCode", "setClientID", 0);
		digester.addCallMethod("Response/Header/RefNo", "setRefNo", 0);
		digester.addCallMethod("Response/Header/ResDate", "setResDate", 0);
		digester.addCallMethod("Response/Header/Status", "setResult", 0);
		digester.addCallMethod("Response/Header/Type", "setType", 0);
		digester.addCallMethod("Response/Header/PayType", "setPayType", 0);
		digester.addCallMethod("Response/Header/ReqType", "setReqType", 0);
		digester.addCallMethod("Response/Header/Error", "setError", 0);
		digester.addCallMethod("Response/Body/Batch/BatchId", "setBatchId", 0);
		digester.addCallMethod("Response/Body/Batch/Status", "setStatus", 0);

		try {
			MIGSResponse response = (MIGSResponse) digester.parse(is);
			ClientID = StringUtils.trim(response.getClientID());
			refNo = StringUtils.trim(response.getRefNo());
			resDate = StringUtils.trim(response.getResDate());
			type = StringUtils.trim(response.getType());
			payType = StringUtils.trim(response.getPayType());
			reqType = StringUtils.trim(response.getReqType());
			error = StringUtils.trim(response.getError());
			batchId = StringUtils.trim(response.getBatchId());
			status = StringUtils.trim(response.getStatus());
			result = StringUtils.trim(response.getResult());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}

	public void setPaymentXml(String paymentXml) {
		Digester digester = new Digester();
		InputStream is = new ByteArrayInputStream(paymentXml.toString().getBytes());
		this.paymentXml = paymentXml;

		digester.addObjectCreate("bankddreturn", "com.isa.thinair.paymentbroker.core.bl.migs.MIGSResponse");
		digester.addCallMethod("bankddreturn/ClientID", "setClientID", 0);
		digester.addCallMethod("bankddreturn/action", "setAction", 0);
		digester.addCallMethod("bankddreturn/orderid", "setOrderid", 0);
		digester.addCallMethod("bankddreturn/result", "setResult", 0);
		digester.addCallMethod("bankddreturn/resultdesc", "setResultdesc", 0);
		digester.addCallMethod("bankddreturn/resultref", "setResultref", 0);
		digester.addCallMethod("bankddreturn/paymentdate", "setPaymentdate", 0);
		digester.addCallMethod("bankddreturn/paymenttime", "setPaymenttime", 0);

		try {
			MIGSResponse response = (MIGSResponse) digester.parse(is);
			ClientID = StringUtils.trim(response.getClientID());
			action = StringUtils.trim(response.getAction());
			orderid = StringUtils.trim(response.getOrderid());
			result = StringUtils.trim(response.getResult());
			resultdesc = StringUtils.trim(response.getResultdesc());
			resultref = StringUtils.trim(response.getResultref());
			paymentdate = StringUtils.trim(response.getPaymentdate());
			paymenttime = StringUtils.trim(response.getPaymenttime());

		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}

	public String getPaymentXml() {
		return paymentXml;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getClientID() {
		return ClientID;
	}

	public void setClientID(String clientID) {
		ClientID = clientID;
	}

	public String getOrderid() {
		return orderid;
	}

	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getResultdesc() {
		return resultdesc;
	}

	public void setResultdesc(String resultdesc) {
		this.resultdesc = resultdesc;
	}

	public String getResultref() {
		return resultref;
	}

	public void setResultref(String resultref) {
		this.resultref = resultref;
	}

	public String getPaymentdate() {
		return paymentdate;
	}

	public void setPaymentdate(String paymentdate) {
		this.paymentdate = paymentdate;
	}

	public String getPaymenttime() {
		return paymenttime;
	}

	public void setPaymenttime(String paymenttime) {
		this.paymenttime = paymenttime;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResDate() {
		return resDate;
	}

	public void setResDate(String resDate) {
		this.resDate = resDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getReqType() {
		return reqType;
	}

	public void setReqType(String reqType) {
		this.reqType = reqType;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
