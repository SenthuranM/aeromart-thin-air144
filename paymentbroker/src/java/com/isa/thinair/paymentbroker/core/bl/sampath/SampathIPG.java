package com.isa.thinair.paymentbroker.core.bl.sampath;

import ipgclient2.CShroff2;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.CommonsException;
import com.isa.thinair.commons.api.exception.CommonsRuntimeException;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.util.WebClient;

public class SampathIPG {

	private static final String PARAM_ENCRYPTED_INVOICE_PAY = "encryptedInvoicePay";

	private static Log log = LogFactory.getLog(SampathIPG.class);

	private String proxyHost;

	private String proxyPort;

	private String secureKeyLocation;

	private String logPath;

	private String ipgURL;

	public SampathIPG(String proxyHost, String proxyPort, String securekeyLocation, String logPath, String ipgURL) {
		this.proxyHost = proxyHost;
		this.proxyPort = proxyPort;
		this.secureKeyLocation = securekeyLocation;
		this.logPath = logPath;
		this.ipgURL = ipgURL;
	}

	/**
	 * gets the encrypted invoice
	 * 
	 * @param plainTextRequest
	 * @param secureKeyLocation
	 * @param logPath
	 * @return
	 * @throws CommonsException
	 */
	public String getEncryptedRequest(String plainTextRequest) {
		CShroff2 cShroff2 = new CShroff2(secureKeyLocation, logPath);

		String encryptedInvoice = null;
		int iResponse = 0;

		if (cShroff2.getErrorCode() != 0) {
			log.error(cShroff2.getErrorCode() + ":" + cShroff2.getErrorMsg());
			throw new CommonsRuntimeException(PaymentBrokerInternalConstants.MessageCodes.ERROR_CREATING_CSHROF2,
					PaymentBrokerInternalConstants.MODULE_DESC);
		}

		iResponse = cShroff2.setPlainTextInvoice(plainTextRequest);

		if (iResponse == 0) {
			encryptedInvoice = cShroff2.getEncryptedInvoice();
		} else {
			log.error(cShroff2.getErrorCode() + ":" + cShroff2.getErrorMsg());
			throw new CommonsRuntimeException(PaymentBrokerInternalConstants.MessageCodes.ERROR_ENCRYPTING_REQUEST,
					PaymentBrokerInternalConstants.MODULE_DESC);
		}

		return encryptedInvoice;
	}

	/**
	 * gets the decrypted receipt
	 * 
	 * @param encryptedResponse
	 * @param secureKeyLocation
	 * @param logPath
	 * @return
	 * @throws CommonsException
	 */
	public String getDecryptedResponse(String encryptedResponse) {
		CShroff2 cShroff2 = new CShroff2(secureKeyLocation, logPath);

		String plainTextReceipt = null;
		int iResponse = 0;

		if (cShroff2.getErrorCode() != 0) {
			log.error(cShroff2.getErrorCode() + ":" + cShroff2.getErrorMsg());
			throw new CommonsRuntimeException(PaymentBrokerInternalConstants.MessageCodes.ERROR_CREATING_CSHROF2,
					PaymentBrokerInternalConstants.MODULE_DESC);
		}

		iResponse = cShroff2.setEncryptedReceipt(encryptedResponse);

		if (iResponse == 0) {
			plainTextReceipt = cShroff2.getPlainTextReceipt();
		} else {
			log.error(cShroff2.getErrorCode() + ":" + cShroff2.getErrorMsg());
			throw new CommonsRuntimeException(PaymentBrokerInternalConstants.MessageCodes.ERROR_DECRYPTING_RESPONSE,
					PaymentBrokerInternalConstants.MODULE_DESC);
		}

		return plainTextReceipt;
	}

	/**
	 * sends the request
	 * 
	 * @param plainTextInvoice
	 * @return
	 * @throws CommonsException
	 */
	public String sendRequest(String plainTextInvoice) throws CommonsException {
		WebClient wc = new WebClient(proxyHost, proxyPort);

		String encryptedInvoice = getEncryptedRequest(plainTextInvoice);
		String encryptedReceipt = null;
		String plainTextReceipt = null;

		Map mapParam = new HashMap();

		log.info("encryptedInvoice:" + encryptedInvoice);
		mapParam.put(PARAM_ENCRYPTED_INVOICE_PAY, encryptedInvoice);

		String responseStr = wc.post(ipgURL, mapParam);
		encryptedReceipt = extractEncryptedReceiptPay(responseStr);
		log.info("encryptedReceipt:" + encryptedReceipt);

		plainTextReceipt = getDecryptedResponse(encryptedReceipt);
		log.info("plainTextReceipt:" + plainTextReceipt);

		return plainTextReceipt;
	}

	private String extractEncryptedReceiptPay(String responseStr) {
		return StringUtils.substringBetween(responseStr, "value=\"", "\" />");
	}
}
