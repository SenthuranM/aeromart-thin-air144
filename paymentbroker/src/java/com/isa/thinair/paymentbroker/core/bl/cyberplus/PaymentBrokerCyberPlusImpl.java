package com.isa.thinair.paymentbroker.core.bl.cyberplus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.CyberplusRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.CyberplusTransactionInfoDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.wsclient.api.service.WSClientBD;

public class PaymentBrokerCyberPlusImpl extends PaymentBrokerCyberPlusTemplate implements PaymentBroker {

	private static Log log = LogFactory.getLog(PaymentBrokerCyberPlusImpl.class);

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	private static final String THREEDS_MPI = "0";

	private static final String THREEDS_STATUS = "N";

	private static final String ERROR_CODE_PREFIX = "cyberPlus.12";

	private static final String TRANSACTION_RESULT_SUCCESS = "00";

	private static final int REFUND_RESULT_SUCCESS = 0;

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Performs card payment operation
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	@Override
	public IPGResponseDTO getReponseData(Map fields, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		log.debug("[PaymentBrokerCyberPlusImpl::getReponseData()] Begin ");

		boolean errorExists = false;
		int tnxResultCode;
		String errorCode = "";
		String status;
		String errorSpecification;
		String merchantTxnReference;
		String authResult;
		String authNumber;
		int cardType = 2; // default card type

		String SignatureValidated = null;
		String responseMismatch = "";
		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());

		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time
		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

		CyberPlusResponse cyberPlusResp = new CyberPlusResponse();
		cyberPlusResp.setReponse(fields);

		String[] userInfo = cyberPlusResp.getUserInfo().split(CyberPlusPaymentUtils.COLON);
		AppIndicatorEnum appIndicatorEnum = SalesChannelsUtil.isAppIndicatorWebOrMobile(userInfo[0])
				? AppIndicatorEnum.APP_IBE
				: AppIndicatorEnum.APP_XBE;
		merchantTxnReference = composeAccelAeroTransactionRef(appIndicatorEnum, cyberPlusResp.getSiteId(),
				cyberPlusResp.getTransDate(), cyberPlusResp.getTransId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		authResult = cyberPlusResp.getAuthResult();
		authNumber = cyberPlusResp.getAuthNumber();
		cardType = CyberPlusPaymentUtils.getCardCode(cyberPlusResp.getCardBrand());

		log.debug("[PaymentBrokerCyberPlusPImpl::getReponseData()] Mid Response -" + merchantTxnReference);
		if (cyberPlusResp != null && authResult != null && !(authResult.trim().equals(""))) {

			// Validate the Secure Hash
			if (cyberPlusResp.validateSignature(getSecureSecret())) {
				// Signature validation succeeded, add a data field to be
				// displayed later.
				SignatureValidated = CyberPlusResponse.VALID_SIGNATURE;

				// Check for correct response
				errorExists = true;

				// Check if response is not equal
				if (oCreditCardTransaction.getTempReferenceNum().equalsIgnoreCase(merchantTxnReference)) {
					errorExists = false;
				} else {
					responseMismatch = CyberPlusResponse.INVALID_RESPONSE;

					PaymentQueryDR oNewQuery = getPaymentQueryDR();
					ServiceResponce serviceResponce = oNewQuery.query(oCreditCardTransaction,
							CyberPlusPaymentUtils.getIPGConfigs(getCtxMode()),
							PaymentBrokerInternalConstants.QUERY_CALLER.INVALIDRESPONSE);
					if (serviceResponce.isSuccess()) {
						errorExists = false;
					}
				}
			} else {
				// Signature validation failed, add a data field to be
				// displayed later.
				errorExists = true;
				SignatureValidated = CyberPlusResponse.INVALID_SIGNATURE;

				PaymentQueryDR oNewQuery = getPaymentQueryDR();
				ServiceResponce serviceResponce = oNewQuery.query(oCreditCardTransaction,
						CyberPlusPaymentUtils.getIPGConfigs(getCtxMode()),
						PaymentBrokerInternalConstants.QUERY_CALLER.HASHMISMATCH);
				if (serviceResponce.isSuccess()) {
					errorExists = false;
				}
			}
		} else {
			// Signature was not validated,
			SignatureValidated = CyberPlusResponse.NO_VALUE;
			errorCode = ERROR_CODE_PREFIX;
		}

		if (TRANSACTION_RESULT_SUCCESS.equals(cyberPlusResp.getResult())
				&& TRANSACTION_RESULT_SUCCESS.equals(cyberPlusResp.getAuthResult()) && !errorExists) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			tnxResultCode = 1;
			errorSpecification = "" + responseMismatch;
		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;

			errorCode = CyberPlusPaymentUtils.getErrorCode(authResult, cyberPlusResp.getResult());

			errorSpecification = "Status:" + status + " Signature Status:" + SignatureValidated + " Response:"
					+ CyberPlusPaymentUtils.getResponseDescription(cyberPlusResp.getAuthResult()) + " 3DSStatus:"
					+ CyberPlusPaymentUtils.getStatusDescription(cyberPlusResp.getThreedsStatus()) + " ResponseMismatch:"
					+ responseMismatch;
		}

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setMessage(status);
		ipgResponseDTO.setApplicationTransactionId(merchantTxnReference);
		ipgResponseDTO.setAuthorizationCode(authNumber);
		ipgResponseDTO.setCardType(cardType);
		String trimedDigits = "";
		if (cyberPlusResp.getCardNumber() != null && cyberPlusResp.getCardNumber().length() > 4) {
			// Get the last four digits of the card number.
			trimedDigits = cyberPlusResp.getCardNumber().substring(cyberPlusResp.getCardNumber().length() - 4);
		}

		ipgResponseDTO.setCcLast4Digits(trimedDigits);
		String cyberPlusResponse = cyberPlusResp.toString();

		updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), cyberPlusResponse, errorSpecification,
				authNumber, cyberPlusResp.getPaymentCertificate(), tnxResultCode);

		log.debug("[PaymentBrokerCyberPlusImpl::getReponseData()] End -" + merchantTxnReference + "");

		return ipgResponseDTO;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		String finalAmount = CyberPlusPaymentUtils.getFormattedAmount(ipgRequestDTO.getAmount(), ipgRequestDTO.getNoOfDecimals());

		int transId = CyberPlusPaymentUtils.getTransId(ipgRequestDTO.getApplicationTransactionId());
		String date = CyberPlusPaymentUtils.getFormatedDate();
		String merchantTxnId = composeAccelAeroTransactionRef(ipgRequestDTO.getApplicationIndicator(), getMerchantId(), date,
				Integer.toString(transId), PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		String payCurrencyNumericCode = getStandardCurrencyCode(ipgRequestDTO.getIpgIdentificationParamsDTO()
				.getPaymentCurrencyCode());

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		postDataMap.put(CyberPlusRequest.VADS_ACTION_MODE, getActionMode());
		postDataMap.put(CyberPlusRequest.VADS_AMOUNT, finalAmount);
		postDataMap.put(CyberPlusRequest.VADS_CTX_MODE, getCtxMode());
		postDataMap.put(CyberPlusRequest.VADS_CURRENCY, payCurrencyNumericCode);
		postDataMap.put(CyberPlusRequest.VADS_PAYMENT_CONFIG, getPaymentConfig());
		postDataMap.put(CyberPlusRequest.VADS_SITE_ID, getMerchantId());
		postDataMap.put(CyberPlusRequest.VADS_THREEDS_STATUS, THREEDS_STATUS);
		postDataMap.put(CyberPlusRequest.VADS_TRANS_DATE, date);
		postDataMap.put(CyberPlusRequest.VADS_TRANS_ID, Integer.toString(transId));
		postDataMap.put(CyberPlusRequest.VADS_VERSION, getVersion());
		postDataMap.put(CyberPlusRequest.VADS_PAGE_ACTION, getPageAction());
		postDataMap.put(CyberPlusRequest.VADS_PAYMENT_CARDS, getStandardCardType(ipgRequestDTO.getCardType()));
		postDataMap.put(CyberPlusRequest.VADS_RETURN_MODE, getReturnMode());
		postDataMap.put(CyberPlusRequest.VADS_THREEDS_MPI, THREEDS_MPI);
		postDataMap.put(CyberPlusRequest.VADS_URL_SUCCESS, ipgRequestDTO.getReturnUrl());
		postDataMap.put(CyberPlusRequest.VADS_URL_RETURN, ipgRequestDTO.getReturnUrl());
		postDataMap.put(CyberPlusRequest.VADS_LANGUAGE, DEFAULT_LANGUAGE);
		postDataMap.put(CyberPlusRequest.VADS_CUST_EMAIL, ipgRequestDTO.getContactEmail());
		postDataMap.put(CyberPlusRequest.VADS_ORDER_ID, ipgRequestDTO.getPnr());

		String userInfo = CyberPlusPaymentUtils.getUserInfo(ipgRequestDTO.getApplicationIndicator(),
				PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		postDataMap.put(CyberPlusRequest.VADS_USER_INFO, userInfo);

		// get request parameters before adding card details to log.
		String strRequestParams = CyberPlusPaymentUtils.getRequestDataAsString(postDataMap);

		postDataMap.put(CyberPlusRequest.VADS_CARD_NUMBER, ipgRequestDTO.getCardNo());
		postDataMap.put(CyberPlusRequest.VADS_CVV, ipgRequestDTO.getSecureCode());
		postDataMap.put(CyberPlusRequest.VADS_EXPIRY_MONTH, ipgRequestDTO.getExpiryDate().substring(2));
		postDataMap.put(CyberPlusRequest.VADS_EXPIRY_YEAR, "20" + ipgRequestDTO.getExpiryDate().substring(0, 2));

		String signature = CyberPlusPaymentUtils.getSignature(postDataMap, getSecureSecret());
		postDataMap.put(CyberPlusRequest.SIGNATURE, signature);

		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();

		String requestDataForm = CyberPlusPaymentUtils.getPostInputDataFormHTML(postDataMap, getIpgURL());

		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				strRequestParams + "," + sessionID, "", getPaymentGatewayName(), null, false);

		if (log.isDebugEnabled()) {
			log.debug("Cyberplus Request Data : " + strRequestParams + ", sessionID : " + sessionID);
		}

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(requestDataForm);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);

		return ipgRequestResultsDTO;
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status;
		String errorSpecification;
		String merchTnxRef;

		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
		String amount = CyberPlusPaymentUtils.getFormattedAmount(creditCardPayment.getAmount(),
				creditCardPayment.getNoOfDecimalPoints());

		// Cancel payments for refunds if cancelBeforeFefund flag is enabled.
		// Should cancel the full payment amount. Cannot cancel a part of a payment.
		// Should modify the transaction amount to refund a part of a transaction.
		if (isCancelBeforeRefund()) {
			PaymentQueryDR queryDR = getPaymentQueryDR();
			if (queryDR instanceof CyberPlusPaymentQueryDR) {
				CyberplusTransactionInfoDTO queryResponce = ((CyberPlusPaymentQueryDR) queryDR).queryDetails(ccTransaction,
						CyberPlusPaymentUtils.getIPGConfigs(getCtxMode()), PaymentBrokerInternalConstants.QUERY_CALLER.CANCEL);
				if (WAITING_FOR_SUBMISSION == queryResponce.getTransactionStatus()) {
					if (amount.equals(String.valueOf(queryResponce.getAmount()))) {
						ServiceResponce cancelResponse = cancel(creditCardPayment, pnr, appIndicator, tnxMode);
						if (cancelResponse.isSuccess()) {
							return cancelResponse;
						}
					} else {
						// Set previousCCPayment amount for the current total amount for already modified transactions.
						// This can happen for transactions with partial refunds.
						String initialPaymentAmount = CyberPlusPaymentUtils.getFormattedAmount(
								String.valueOf(creditCardPayment.getPreviousCCPayment().getTotalAmount().doubleValue()),
								creditCardPayment.getNoOfDecimalPoints());
						if (!initialPaymentAmount.equals(String.valueOf(queryResponce.getAmount()))) {
							creditCardPayment.getPreviousCCPayment().setTotalAmount(
									new BigDecimal(queryResponce.getAmount() / 100));
						}
						ServiceResponce modifyResponse = modify(creditCardPayment, pnr, appIndicator, tnxMode);
						if (modifyResponse.isSuccess()) {
							return modifyResponse;
						}
					}
				}
			}
		}

		if (creditCardPayment.getPreviousCCPayment() != null && getRefundEnabled() != null && getRefundEnabled().equals("true")) {
			log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
			log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
			log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
			log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
			log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}

			merchTnxRef = ccTransaction.getTempReferenceNum();
			String[] merchTnxRefBreakDown = CyberPlusPaymentUtils.getBreakDown(merchTnxRef);
			AppIndicatorEnum appIndicatorEnum = SalesChannelsUtil.isAppIndicatorWebOrMobile(merchTnxRefBreakDown[0])
					? AppIndicatorEnum.APP_IBE
					: AppIndicatorEnum.APP_XBE;

			int newTransId = CyberPlusPaymentUtils.getTransId(creditCardPayment.getTemporyPaymentId());
			String updatedMerchTnxRef = composeAccelAeroTransactionRef(appIndicatorEnum, merchTnxRefBreakDown[1],
					merchTnxRefBreakDown[2], merchTnxRefBreakDown[3], newTransId, PaymentConstants.MODE_OF_SERVICE.REFUND);

			XMLGregorianCalendar transDate = CyberPlusPaymentUtils.getXMLGregorianCalFromDate(merchTnxRefBreakDown[2]);
			String payCurrencyNumericCode = getStandardCurrencyCode(creditCardPayment.getCurrency());

			CyberplusRequestDTO requestDTO = new CyberplusRequestDTO();
			requestDTO.setShopId(getMerchantId());
			requestDTO.setTransmissionDate(transDate);
			requestDTO.setTransactionId(merchTnxRefBreakDown[3]);
			requestDTO.setSequenceNb(STARTIGN_SEQ_NO);
			requestDTO.setCtxMode(getCtxMode());
			requestDTO.setNewTransactionId(Integer.toString(newTransId));
			requestDTO.setAmount(Long.valueOf(amount));
			requestDTO.setPresentationDate(transDate);
			requestDTO.setValidationMode(0);
			requestDTO.setComment("");
			requestDTO.setCurrency(Integer.valueOf(payCurrencyNumericCode));
			requestDTO.setProxyHost(getProxyHost());
			requestDTO.setProxyPort(getProxyPort());

			String signature = CyberPlusPaymentUtils.createSignature(getSecureSecret(), requestDTO.getShopId(),
					CyberPlusPaymentUtils.formatDate(merchTnxRefBreakDown[2], CyberPlusPaymentUtils.DATE_FORMAT),
					requestDTO.getTransactionId(), requestDTO.getSequenceNb(), requestDTO.getCtxMode(),
					requestDTO.getNewTransactionId(), requestDTO.getAmount(), requestDTO.getCurrency(), requestDTO
							.getPresentationDate().toGregorianCalendar().getTime(), requestDTO.getValidationMode(),
					requestDTO.getComment());

			requestDTO.setWsSignature(signature);

			CyberplusTransactionInfoDTO response = null;
			Integer paymentBrokerRefNo;

			CreditCardTransaction ccNewTransaction = auditTransaction(pnr, getMerchantId(), updatedMerchTnxRef, new Integer(
					ccTransaction.getTemporyPaymentId()), bundle.getString("SERVICETYPE_REFUND"), requestDTO.toString(), "",
					getPaymentGatewayName(), null, false);

			// Sends the refund request and gets the response

			try {
				// Refund request.
				WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
				response = ebiWebervices.refund(requestDTO);

				paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());

			} catch (Exception e) {
				log.error(e);
				throw new ModuleException("paymentbroker.error.unknown");
			}

			log.debug("Refund Response : " + response.toString());

			DefaultServiceResponse sr = new DefaultServiceResponse(false);

			if (ERROR_CODE_RESULT_SUCCESS == response.getErrorCode() && WAITING_FOR_SUBMISSION == response.getTransactionStatus()) {
				errorCode = "";
				transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
				status = IPGResponseDTO.STATUS_ACCEPTED;
				tnxResultCode = 1;
				errorSpecification = "";
				sr.setSuccess(true);
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
			} else {
				// TODO
				errorCode = CyberPlusPaymentUtils.API_ERROR_CODE_PREFIX + response.getErrorCode();
				transactionMsg = CyberPlusPaymentUtils.getWSAPIErrorDescription(response.getErrorCode(),
						response.getTransactionStatus());
				status = IPGResponseDTO.STATUS_REJECTED;
				tnxResultCode = 0;
				errorSpecification = "Status:" + status + " Error code:" + errorCode + " Description:" + transactionMsg;
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
			}

			updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), response.toString(), errorSpecification,
					response.getAuthNb(), "", tnxResultCode);

			sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, response.getAuthNb());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
			return sr;
		} else {
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);

			return sr;
		}
	}

	/**
	 * Performs reverse card payment
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		String refundEnabled = getRefundEnabled();
		ServiceResponce reverseResult = null;

		if (refundEnabled != null && refundEnabled.equals("false") && !isCancelBeforeRefund()) {

			DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);
			return defaultServiceResponse;
			// throw new ModuleException("paymentbroker.reverse.operation.not.supported");
		}
		reverseResult = refund(creditCardPayment, pnr, appIndicator, tnxMode);
		return reverseResult;
	}

	/**
	 * Resolves partial payments [Invokes via a scheduler operation]
	 */
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {
		Iterator itColIPGQueryDTO = colIPGQueryDTO.iterator();

		Collection<Integer> oResultIF = new ArrayList<Integer>();
		Collection<Integer> oResultIP = new ArrayList<Integer>();
		Collection<Integer> oResultII = new ArrayList<Integer>();
		Collection<Integer> oResultIS = new ArrayList<Integer>();

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		IPGQueryDTO ipgQueryDTO;
		CreditCardTransaction oCreditCardTransaction;
		PreviousCreditCardPayment oPrevCCPayment;
		CreditCardPayment oCCPayment;

		while (itColIPGQueryDTO.hasNext()) {
			ipgQueryDTO = (IPGQueryDTO) itColIPGQueryDTO.next();

			oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgQueryDTO.getPaymentBrokerRefNo());

			try {
				oPrevCCPayment = new PreviousCreditCardPayment();
				oPrevCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());

				oCCPayment = new CreditCardPayment();
				oCCPayment.setPreviousCCPayment(oPrevCCPayment);
				oCCPayment.setAmount(ipgQueryDTO.getAmount().toString());
				oCCPayment.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
				oCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
				oCCPayment.setIpgIdentificationParamsDTO(ipgQueryDTO.getIpgIdentificationParamsDTO());
				oCCPayment.setTnxMode(TnxModeEnum.SECURE_3D);
				oCCPayment.setPnr(oCreditCardTransaction.getTransactionReference());
				oCCPayment.setTemporyPaymentId(oCreditCardTransaction.getTemporyPaymentId());

				String currencyCode = PaymentBrokerUtils.getPaymentBrokerDAO()
						.getTempPaymentInfo(oCreditCardTransaction.getTemporyPaymentId()).getPaymentCurrencyCode();
				oCCPayment.setCurrency(currencyCode);

				PaymentQueryDR oNewQuery = getPaymentQueryDR();
				ServiceResponce serviceResponce = oNewQuery.query(oCreditCardTransaction,
						CyberPlusPaymentUtils.getIPGConfigs(getCtxMode()), PaymentBrokerInternalConstants.QUERY_CALLER.SCHEDULER);

				// If successful payment remains at Bank
				if (serviceResponce.isSuccess()) {
					if (isCancelBeforeRefund() && WAITING_FOR_SUBMISSION == Integer.valueOf(serviceResponce.getResponseCode())) {
						ServiceResponce cancelResponse = cancel(oCCPayment, oCCPayment.getPnr(), oCCPayment.getAppIndicator(),
								oCCPayment.getTnxMode());
						if (cancelResponse.isSuccess()) {
							// Change State to 'IS'
							oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
							if (log.isDebugEnabled()) {
								log.debug("Status moing to IS:" + oCreditCardTransaction.getTemporyPaymentId());
							}
						} else {
							// Change State to 'IF'
							oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
							if (log.isDebugEnabled()) {
								log.debug("Status moing to IF:" + oCreditCardTransaction.getTemporyPaymentId());
							}
						}
					} else if (SUBMITTED == Integer.valueOf(serviceResponce.getResponseCode())) {
						if (refundFlag) {
							ServiceResponce srRev = refund(oCCPayment, oCCPayment.getPnr(), oCCPayment.getAppIndicator(),
									oCCPayment.getTnxMode());
							if (srRev.isSuccess()) {
								// Change State to 'IS'
								oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
								if (log.isDebugEnabled()) {
									log.debug("Status moing to IS:" + oCreditCardTransaction.getTemporyPaymentId());
								}
							} else {
								// Change State to 'IF'
								oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
								if (log.isDebugEnabled()) {
									log.debug("Status moing to IF:" + oCreditCardTransaction.getTemporyPaymentId());
								}
							}
						} else {
							// Change State to 'IP'
							oResultIP.add(oCreditCardTransaction.getTemporyPaymentId());
							if (log.isDebugEnabled()) {
								log.debug("Status moing to IP:" + oCreditCardTransaction.getTemporyPaymentId());
							}
						}
					} else {
						// Change State to 'IP'
						oResultIP.add(oCreditCardTransaction.getTemporyPaymentId());
						if (log.isDebugEnabled()) {
							log.debug("Status moing to IP:" + oCreditCardTransaction.getTemporyPaymentId());
						}
					}
				}
				// No payment detail at bank
				else {
					// No Payments exist
					// Update status to 'II'
					oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
					if (log.isDebugEnabled()) {
						log.debug("Status moing to II:" + oCreditCardTransaction.getTemporyPaymentId());
					}
				}

			} catch (ModuleException e) {
				log.error("Scheduler::ResolvePayment FAILED!!! " + e.getMessage(), e);
			}
		}

		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF, oResultIF);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_II, oResultII);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IP, oResultIP);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS, oResultIS);

		sr.setSuccess(true);

		return sr;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		// TODO - Implement card configuration data
		return getRequestData(ipgRequestDTO);
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
}
