/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.bl.mtc;

import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.persistence.hibernate.PaymentBrokerDAOImpl;
import com.isa.thinair.paymentbroker.core.util.PaymentURLUtil;

/**
 * MIGS payment response
 * 
 * @author Manjula
 */
public class MTCResponse {
	
	private Log log = LogFactory.getLog(MTCResponse.class);

	public static final String NO_VALUE = "";
	public static final String VALID_HASH = "VALID HASH.";
	public static final String INVALID_HASH = "INVALID HASH.";
	public static final String INVALID_RESPONSE = "INVALID RESPONSE";

	public String actionSLK;
	public String sLKSecretkey;
	public String storeId;
	public String langue;
	public String offerURL;
	public String updateURL;
	public String buyerName;
	public String address;
	public String city;
	public String state;
	public String country;
	public String postcode;
	public String tel;
	public String email;
	public String checksum;
	public String cartId;
	public String mode;
	public String count;
	public String desc;
	public String qty;
	public String itemsPrices;
	public String amounTX;
	public String shippingCharge;
	public String totalamountTx;
	public String totalamountCur;
	public String symbolCur;
	public String shippingWeight;

	private String mtcOrderNum;
	private String paymentResult;

	private String sessionID;
	// Response as a Map
	Map response = null;

	public void setResponse(Map response) {
		this.response = response;
		actionSLK = null2unknown((String) response.get("actionSLK"));
		sLKSecretkey = null2unknown((String) response.get("sLKSecretkey"));
		storeId = null2unknown((String) response.get("StoreId"));
		langue = null2unknown((String) response.get("langue"));
		offerURL = null2unknown((String) response.get("offerURL"));
		updateURL = null2unknown((String) response.get("updateURL"));
		buyerName = null2unknown((String) response.get("buyerName"));
		address = null2unknown((String) response.get("address"));
		city = null2unknown((String) response.get("city"));
		state = null2unknown((String) response.get("state"));
		country = null2unknown((String) response.get("country"));
		postcode = null2unknown((String) response.get("postcode"));
		tel = null2unknown((String) response.get("tel"));
		email = null2unknown((String) response.get("email"));
		checksum = null2unknown((String) response.get("checksum"));
		cartId = null2unknown((String) response.get("cartId"));
		mode = null2unknown((String) response.get("mode"));

		// 3-D Secure Data
		count = null2unknown((String) response.get("count"));
		desc = null2unknown((String) response.get("desc"));
		qty = null2unknown((String) response.get("qty"));
		itemsPrices = null2unknown((String) response.get("itemsPrices"));
		amounTX = null2unknown((String) response.get("amounTX"));
		shippingCharge = null2unknown((String) response.get("shippingCharge"));
		totalamountTx = null2unknown((String) response.get("TotalmountTx"));
		totalamountCur = null2unknown((String) response.get("totalamountCur"));

		// Capture Data
		symbolCur = null2unknown((String) response.get("symbolCur"));
		shippingWeight = null2unknown((String) response.get("shippingWeight"));
		mtcOrderNum = null2unknown((String) response.get("mtc_order_num"));
		paymentResult = null2unknown((String) response.get("paymentResult"));

		sessionID = null2unknown((String) response.get(PaymentConstants.IPG_SESSION_ID));
	}

	public String toString() {
		StringBuffer resBuff = new StringBuffer();

		// create a list
		List fieldNames = new ArrayList(response.keySet());
		Iterator itr = fieldNames.iterator();

		// move through the list and create a series of URL key/value pairs
		while (itr.hasNext()) {
			String fieldName = (String) itr.next();
			String fieldValue = (String) response.get(fieldName);
			resBuff.append(fieldName + " : " + fieldValue);
			// add a '&' to the end if we have more fields coming.
			if (itr.hasNext()) {
				resBuff.append(", ");
			}
		}
		return resBuff.toString();
	}
	
	public boolean validateChecksum(String SecureSecret) {
		
		String dataMD5WithoutReturnURL = storeId + cartId + count + totalamountTx + email + SecureSecret;
		List<String> retunURLList = PaymentURLUtil.getReturnURLList();
		List<String>  dataMD5List = new ArrayList<>();
		
		for (String returnURL : retunURLList) {
			dataMD5List.add(returnURL + dataMD5WithoutReturnURL);
		}		
		
		for (String dataMD5 : dataMD5List) {
			
			String generatedChecksum = getGeneratedChecksum(dataMD5);
			
			if (generatedChecksum.equals(checksum)) {
				return true;
			} else {
				if (log.isDebugEnabled()) {
					log.debug("Generated checksum : " + generatedChecksum +"# PG checksum:" + checksum + "#DataMD5 :" + dataMD5);
				}
			}
		}
		
		log.info("### MTC E-commerce payment gateway - Users are tring to make fraud booking. Please attend..");
		
		return false;
	}
	
	private String getGeneratedChecksum(String dataMD5) {
		
		String generatedChecksum = "";
		
		try {			
			String uRLEncodeData = URLEncoder.encode(dataMD5, "UTF-8");
			MessageDigest md = MessageDigest.getInstance("MD5");			
			md.reset();
			md.update((uRLEncodeData).getBytes());
			generatedChecksum = StringUtil.byteArrayToHexString(md.digest()).toLowerCase();				
		} catch (Exception e) {
			log.error(e + dataMD5);				
		}
		
		return generatedChecksum;
	}

	/*
	 * This method takes a data String and returns a predefined value if empty If data Sting is null, returns string
	 * "No Value Returned", else returns input
	 * 
	 * @param in String containing the data String
	 * 
	 * @return String containing the output String
	 */
	private static String null2unknown(String in) {
		if (in == null || in.length() == 0) {
			return NO_VALUE;
		} else {
			return in;
		}
	}

	public String getActionSLK() {
		return actionSLK;
	}

	public String getSLKSecretkey() {
		return sLKSecretkey;
	}

	public String getStoreId() {
		return storeId;
	}

	public String getLangue() {
		return langue;
	}

	public String getOfferURL() {
		return offerURL;
	}

	public String getUpdateURL() {
		return updateURL;
	}

	public String getBuyerName() {
		return buyerName;
	}

	public String getAddress() {
		return address;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getCountry() {
		return country;
	}

	public String getPostcode() {
		return postcode;
	}

	public String getTel() {
		return tel;
	}

	public String getEmail() {
		return email;
	}

	public String getChecksum() {
		return checksum;
	}

	public String getCartId() {
		return cartId;
	}

	public String getMode() {
		return mode;
	}

	public String getCount() {
		return count;
	}

	public String getDesc() {
		return desc;
	}

	public String getQty() {
		return qty;
	}

	public String getItemsPrices() {
		return itemsPrices;
	}

	public String getAmounTX() {
		return amounTX;
	}

	public String getShippingCharge() {
		return shippingCharge;
	}

	public String getTotalamountTx() {
		return totalamountTx;
	}

	public String getTotalamountCur() {
		return totalamountCur;
	}

	public String getSymbolCur() {
		return symbolCur;
	}

	public String getShippingWeight() {
		return shippingWeight;
	}

	public String getMtcOrderNum() {
		return mtcOrderNum;
	}

	public String getPaymentResult() {
		return paymentResult;
	}
}
