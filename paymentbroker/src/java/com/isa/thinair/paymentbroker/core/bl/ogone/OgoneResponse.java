package com.isa.thinair.paymentbroker.core.bl.ogone;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;

public class OgoneResponse {

	private static Log log = LogFactory.getLog(OgoneResponse.class);

	public static final String NO_VALUE = "";

	public static final String VALID_HASH = "VALID HASH.";

	public static final String INVALID_HASH = "INVALID HASH.";

	public static final String INVALID_RESPONSE = "INVALID RESPONSE";

	public static final String ALIAS = "ALIAS";

	public static String ORDERID = "orderID";

	public static String PAYID = "PAYID";

	public static String NCSTATUS = "NCSTATUS";

	public static String NCERROR = "NCERROR";

	public static String NCERRORPLUS = "NCERRORPLUS";

	public static String ACCEPTANCE = "ACCEPTANCE";

	public static String STATUS = "STATUS";

	public static String ECI = "ECI";

	public static String AMOUNT = "amount";

	public static String CURRENCY = "currency";

	public static String PAYMENT_METHOED = "PM";

	public static String BRAND = "BRAND";

	public static String SHASIGN = "SHASIGN";

	public static String CARDNO = "CARDNO";

	public static String SHAREQUIRED = "SHAREQUIRED";

	private String orderId;

	private String payId;

	private String ncStatus;

	private String ncError;

	private String ncErrorPlus;

	private String acceptance;

	private String status;

	private String eci;

	private String amount;

	private String currency;

	private String paymentMethod;

	private String brand;

	private String cardNo;

	private String shaRequired;

	private String sessionID;

	private String alias;

	// Response as a Map
	Map response = null;

	public void setReponse(Map reponse) {
		this.response = reponse;
		orderId = null2unknown((String) reponse.get(ORDERID));
		payId = null2unknown((String) reponse.get(PAYID));
		ncStatus = null2unknown((String) reponse.get(NCSTATUS));
		ncError = null2unknown((String) reponse.get(NCERROR));
		ncErrorPlus = null2unknown((String) reponse.get(NCERRORPLUS));
		acceptance = null2unknown((String) reponse.get(ACCEPTANCE));
		status = null2unknown((String) reponse.get(STATUS));
		eci = null2unknown((String) reponse.get(ECI));
		amount = null2unknown((String) reponse.get(AMOUNT));
		currency = null2unknown((String) reponse.get(CURRENCY));
		paymentMethod = null2unknown((String) reponse.get(PAYMENT_METHOED));
		brand = null2unknown((String) reponse.get(BRAND));
		cardNo = null2unknown((String) reponse.get(CARDNO));
		shaRequired = null2unknown((String) reponse.get(SHAREQUIRED));
		alias = null2unknown((String) reponse.get(ALIAS));
		sessionID = null2unknown((String) reponse.get(PaymentConstants.IPG_SESSION_ID));
	}

	/*
	 * This method takes a data String and returns a predefined value if empty If data Sting is null, returns string
	 * "No Value Returned", else returns input
	 * 
	 * @param in String containing the data String
	 * 
	 * @return String containing the output String
	 */
	private static String null2unknown(String in) {
		if (in == null || in.length() == 0) {
			return NO_VALUE;
		} else {
			return in;
		}
	}

	/*
	 * Validate Response signature
	 * 
	 * @param certificate
	 * 
	 * @return
	 */
	public boolean validateSHAHash(String passphrase) throws ModuleException {
		String sha1 = "";
		try {
			sha1 = OgonePaymentUtils.computeSHA1(response, passphrase);
		} catch (Exception e) {
			throw new ModuleException(e.getMessage());
		}
		if (sha1.equalsIgnoreCase((String) response.get(SHASIGN)))
			return true;
		else
			return false;
	}

	public String toString() {
		StringBuffer responseBuff = new StringBuffer();

		// create a list
		List fieldNames = new ArrayList(response.keySet());
		Iterator itr = fieldNames.iterator();

		// move through the list and create a series of URL key/value pairs
		while (itr.hasNext()) {
			String fieldName = (String) itr.next();
			String fieldValue = (String) response.get(fieldName);
			responseBuff.append(fieldName + " : " + fieldValue);
			// add a '&' to the end if we have more fields coming.
			if (itr.hasNext()) {
				responseBuff.append('&');
			}
		}
		if (responseBuff.toString().length() < 2500) {
			return responseBuff.toString();
		} else {
			// Response is too large to save in db field.
			log.debug("[OgoneResponse::toString()] Response before trimming -" + responseBuff.toString());
			return responseBuff.toString().substring(0, 2500);
		}
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getNcStatus() {
		return ncStatus;
	}

	public void setNcStatus(String ncStatus) {
		this.ncStatus = ncStatus;
	}

	public String getNcError() {
		return ncError;
	}

	public void setNcError(String ncError) {
		this.ncError = ncError;
	}

	public String getNcErrorPlus() {
		return ncErrorPlus;
	}

	public void setNcErrorPlus(String ncErrorPlus) {
		this.ncErrorPlus = ncErrorPlus;
	}

	public String getAcceptance() {
		return acceptance;
	}

	public void setAcceptance(String acceptance) {
		this.acceptance = acceptance;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEci() {
		return eci;
	}

	public void setEci(String eci) {
		this.eci = eci;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getShaRequired() {
		return shaRequired;
	}

	public void setShaRequired(String shaRequired) {
		this.shaRequired = shaRequired;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public Map getResponse() {
		return response;
	}

	public void setResponse(Map response) {
		this.response = response;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

}
