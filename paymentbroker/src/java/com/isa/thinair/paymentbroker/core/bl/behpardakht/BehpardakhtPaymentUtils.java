package com.isa.thinair.paymentbroker.core.bl.behpardakht;

import java.math.BigDecimal;

import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;

public class BehpardakhtPaymentUtils {
	
	public static final String RESPONSE_CODE_0 = "Transaction Approved"; 
	public static final String RESPONSE_CODE_11 = "Invalid Card Number"; 
	public static final String RESPONSE_CODE_12 = "No Sufficient Funds"; 
	public static final String RESPONSE_CODE_13 = "Incorrect Pin"; 
	public static final String RESPONSE_CODE_14 = "Allowable Number Of Pin Tries Exceeded"; 
	public static final String RESPONSE_CODE_15 = "Card Not Effective"; 
	public static final String RESPONSE_CODE_16 = "Exceeds Withdrawal Frequency Limit"; 
	public static final String RESPONSE_CODE_17 = "Customer Cancellation"; 
	public static final String RESPONSE_CODE_18 = "Expired Card"; 
	public static final String RESPONSE_CODE_19 = "Exceeds Withdrawal Amount Limit"; 
	
	public static final String RESPONSE_CODE_111 = "No Such Issuer"; 
	public static final String RESPONSE_CODE_112 = "Card Switch Internal Error"; 
	public static final String RESPONSE_CODE_113 = "Issuer Or Switch Is Inoperative"; 
	public static final String RESPONSE_CODE_114 = "Transaction Not Permitted To Card Holder"; 
	
	public static final String RESPONSE_CODE_21 = "Invalid Merchant"; 
	public static final String RESPONSE_CODE_23 = "Security Violation"; 
	public static final String RESPONSE_CODE_24 = "Invalid User Or Password"; 
	public static final String RESPONSE_CODE_25 = "Invalid Amount"; 
	
	public static final String RESPONSE_CODE_31 = "Invalid Response"; 
	public static final String RESPONSE_CODE_32 = "Format Error"; 
	public static final String RESPONSE_CODE_33 = "No Investment Account"; 
	public static final String RESPONSE_CODE_34 = "System Internal Error"; 
	public static final String RESPONSE_CODE_35 = "Invalid Business Date"; 
	
	public static final String RESPONSE_CODE_41 = "Duplicate Order Id"; 
	public static final String RESPONSE_CODE_42 = "Sale Transaction Not Found"; 
	public static final String RESPONSE_CODE_43 = "Duplicate Verify"; 
	public static final String RESPONSE_CODE_44 = "Verify Transaction Not Found"; 
	public static final String RESPONSE_CODE_45 = "Transaction Has Been Settled"; 
	public static final String RESPONSE_CODE_46 = "Transaction Has Not Been Settled";
	public static final String RESPONSE_CODE_47 = "Settle Transaction Not Found"; 
	public static final String RESPONSE_CODE_48 = "Transaction Has Been Reversed";
	public static final String RESPONSE_CODE_49 = "Refund Transaction Not Found"; 
	
	public static final String RESPONSE_CODE_412 = "Bill Digit Incorrect"; 
	public static final String RESPONSE_CODE_413 = "Payment Digit Incorrect"; 
	public static final String RESPONSE_CODE_414 = "Bill Organization Not Valid"; 
	public static final String RESPONSE_CODE_415 = "Session Timeout"; 
	public static final String RESPONSE_CODE_416 = "Data Access Exception";
	public static final String RESPONSE_CODE_417 = "Payer Id Is Invalid"; 
	public static final String RESPONSE_CODE_418= "Customer Not Found"; 
	public static final String RESPONSE_CODE_419 = "Try Count Exceeded"; 
	public static final String RESPONSE_CODE_421 = "Invalid IP"; 

	public static final String RESPONSE_CODE_51 = "Duplicate Transmission"; 
	public static final String RESPONSE_CODE_54 = "Original Transaction Not Found"; 
	public static final String RESPONSE_CODE_55 = "Invalid Transaction"; 
	public static final String RESPONSE_CODE_61 = "Error In Settle"; 
	
	public static final String TRANSACTION_APPROVED = "0"; 
	public static final String DUPLICATION_VERIFY = "43";
	public static final String TRANSACTION_NOT_FOUND = "44";
	public static final String TRANSACTION_SETTLED = "45"; 
	public static final String TRANSACTION_NOT_SETTLED = "46";
	public static final String TRANSACTION_REVERSED = "48";
	
	
	
	
	
	public static String getErrorDescription(String code) {

		int Code = Integer.parseInt(code);
        String errorDescription;
        switch (Code) {
            case 0:  errorDescription = RESPONSE_CODE_0;
                     break;
            case 11:  errorDescription = RESPONSE_CODE_11;
                     break;
            case 12:  errorDescription = RESPONSE_CODE_12;
                     break;
            case 13:  errorDescription = RESPONSE_CODE_13;
                     break;
            case 14:  errorDescription = RESPONSE_CODE_14;
                     break;
            case 15:  errorDescription = RESPONSE_CODE_15;
                     break;
            case 16:  errorDescription = RESPONSE_CODE_16;
                     break;
            case 17:  errorDescription = RESPONSE_CODE_17;
                     break;
            case 18:  errorDescription = RESPONSE_CODE_18;
                     break;
            case 19: errorDescription = RESPONSE_CODE_19;
                     break;
            case 111: errorDescription = RESPONSE_CODE_111;
                     break;
            case 112: errorDescription = RESPONSE_CODE_112;
                     break;
            case 113:  errorDescription = RESPONSE_CODE_113;
            		 break;
		    case 114:  errorDescription = RESPONSE_CODE_114;
		            break;
		    case 21:  errorDescription = RESPONSE_CODE_21;
		            break;
		    case 23:  errorDescription = RESPONSE_CODE_23;
		            break;
		    case 24:  errorDescription = RESPONSE_CODE_24;
		            break;
		    case 25:  errorDescription = RESPONSE_CODE_25;
		            break;
		    case 31:  errorDescription = RESPONSE_CODE_31;
		             break;
		    case 32:  errorDescription = RESPONSE_CODE_32;
		            break;
		    case 33: errorDescription = RESPONSE_CODE_33;
		            break;
		    case 34: errorDescription = RESPONSE_CODE_34;
		            break;
		    case 35: errorDescription = RESPONSE_CODE_35;
		            break;
		    case 41:  errorDescription = RESPONSE_CODE_41;
		    		break;
		    case 42:  errorDescription = RESPONSE_CODE_42;
		            break;
		    case 43:  errorDescription = RESPONSE_CODE_43;
		            break;
		    case 44:  errorDescription = RESPONSE_CODE_44;
		            break;
		    case 45:  errorDescription = RESPONSE_CODE_45;
		             break;
		    case 46:  errorDescription = RESPONSE_CODE_46;
		            break;
		    case 47: errorDescription = RESPONSE_CODE_47;
		            break;
		    case 48: errorDescription = RESPONSE_CODE_48;
		            break;
		    case 49: errorDescription = RESPONSE_CODE_49;
		            break;
		    case 412:  errorDescription = RESPONSE_CODE_412;
		    		break;
		    case 413:  errorDescription = RESPONSE_CODE_413;
		            break;
		    case 414:  errorDescription = RESPONSE_CODE_414;
		            break;
		    case 415:  errorDescription = RESPONSE_CODE_415;
		            break;
		    case 416:  errorDescription = RESPONSE_CODE_416;
		             break;
		    case 417:  errorDescription = RESPONSE_CODE_417;
		            break;
		    case 418: errorDescription = RESPONSE_CODE_418;
		            break;
		    case 419: errorDescription = RESPONSE_CODE_419;
		            break;
		    case 421: errorDescription = RESPONSE_CODE_421;
		            break;
		    case 51:  errorDescription = RESPONSE_CODE_51;
		    		break;
		    case 54:  errorDescription = RESPONSE_CODE_54;
		    		break;
		    case 55:  errorDescription = RESPONSE_CODE_55;
		    		break;
		    case 61: errorDescription = RESPONSE_CODE_61;
		    		break;
            default: errorDescription = "";
                     break;
        }
        
        return errorDescription;
	}
	
	/**
	 * Behpardakht PG doesn't support decimal values, Behpardakht PG supports only for IRR and PG accepts only Long values
	 * 
	 * @param amount
	 * @return
	 */
	public static String getFormattedAmount(String amount, boolean roundUp) {

		BigDecimal amt = new BigDecimal(amount);

		if (roundUp) {
			amt = amt.setScale(0, BigDecimal.ROUND_UP);
		} else {
			amt = amt.setScale(0, BigDecimal.ROUND_DOWN);
		}

		amount = amt.toString();

		// BigDecimal value = AccelAeroCalculator.multiply(new BigDecimal(amount),
		// AccelAeroCalculator.parseBigDecimal(Math.pow(10, noOfDecimalPoints)));

		return amount;
	}
	
	
	public static IPGConfigsDTO getIPGConfigs(String merchantID, String userName, String password) {
		IPGConfigsDTO oIPGConfigsDTO = new IPGConfigsDTO();
		oIPGConfigsDTO.setMerchantID(merchantID);
		oIPGConfigsDTO.setUsername(userName);
		oIPGConfigsDTO.setPassword(password);

		return oIPGConfigsDTO;
	}

}
