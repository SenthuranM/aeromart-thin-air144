package com.isa.thinair.paymentbroker.core.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;

public class IOUtil {

	public static String getString(InputStream is) {
		BufferedReader in = new BufferedReader(new InputStreamReader(is));
		StringBuffer sb = new StringBuffer();
		String line = null;
		try {
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			throw new ModuleRuntimeException(e, PaymentBrokerInternalConstants.MODULE_DESC);
		}

		return sb.toString();
	}

}
