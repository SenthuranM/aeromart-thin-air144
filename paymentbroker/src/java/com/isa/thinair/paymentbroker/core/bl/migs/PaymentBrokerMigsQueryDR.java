package com.isa.thinair.paymentbroker.core.bl.migs;

import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.migs.MIGSRequest;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.wsclient.api.service.WSClientBD;

public class PaymentBrokerMigsQueryDR extends PaymentBrokerTemplate implements PaymentQueryDR {
	private static Log log = LogFactory.getLog(PaymentBrokerMigsQueryDR.class);

	private static final String PAID = "PAID";
	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	@Override
	public ServiceResponce query(CreditCardTransaction oCreditCardTransaction, IPGConfigsDTO ipgConfigsDTO,
			PaymentBrokerInternalConstants.QUERY_CALLER caller) throws ModuleException {

		log.debug("[PaymentBrokerMigsQueryDR::query()] Begin");

		int tnxResultCode;
		String transactionMsg;
		String status = null;
		String errorSpecification = null;

		String merchantTxnReference = oCreditCardTransaction.getTempReferenceNum();

		String queryParamString = "RefNo:" + oCreditCardTransaction.getTransactionId() + ",MerchantId:" + getMerchantId();

		CreditCardTransaction ccTransaction = auditTransaction(oCreditCardTransaction.getTransactionReference(), getMerchantId(),
				merchantTxnReference, new Integer(oCreditCardTransaction.getTemporyPaymentId()),
				bundle.getString("SERVICETYPE_QUERY"), queryParamString, "", getPaymentGatewayName(), null, false);

		String resultRef = MigsPaymentUtils.getPaymentResponseParams(oCreditCardTransaction.getResponseText(),
				"<resultref>([0-9])*");
		String amount = MigsPaymentUtils.getPaymentResponseParams(oCreditCardTransaction.getRequestText(), "<s_amt>([0-9])*");

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		log.debug("Query Request Params : " + queryParamString);

		String verificationResponse = null;

		MIGSRequest verifyRequest = new MIGSRequest();
		verifyRequest.setClientCode(getClientID());
		verifyRequest.setRefNo(resultRef);// Add transaction amount
		verifyRequest.setApplicationId(oCreditCardTransaction.getTempReferenceNum());
		verifyRequest.setUser(getUser());
		verifyRequest.setPassword(getPassword());
		verifyRequest.setAmount(amount);
		verifyRequest.setQueryRequest();

		WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
		verificationResponse = ebiWebervices.verifyTransaction(verifyRequest);

		if (verificationResponse.startsWith("<")) {
			MIGSResponse verifyResponse = new MIGSResponse();
			verifyResponse.setQueryResponse(verificationResponse);

			log.debug("Query Response : " + verifyResponse.getStatus());
			if (PAID.equals(verifyResponse.getStatus())) {
				status = IPGResponseDTO.STATUS_ACCEPTED;
				transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
				tnxResultCode = 1;
				errorSpecification = " " + caller.toString();
				sr.setSuccess(true);
				sr.addResponceParam(PaymentConstants.AMOUNT, verifyResponse.getAmount());
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, PaymentConstants.SERVICE_RESPONSE_QUERYDR);
				sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE, oCreditCardTransaction.getTransactionId());
				sr.addResponceParam(PaymentConstants.STATUS, status);
			} else {
				status = IPGResponseDTO.STATUS_REJECTED;
				transactionMsg = verifyResponse.getStatus();
				tnxResultCode = 0;
				errorSpecification = "Status:" + status + " Error Desc:" + transactionMsg + " Caller:" + caller.toString();
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
				sr.addResponceParam(PaymentConstants.STATUS, status);
			}
		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			transactionMsg = verificationResponse;
			tnxResultCode = 0;
			errorSpecification = "Status:" + status + " Error Desc:" + transactionMsg + " Caller:" + caller.toString();
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
			sr.addResponceParam(PaymentConstants.STATUS, status);
		}

		updateAuditTransactionByTnxRefNo(ccTransaction.getTransactionRefNo(), verificationResponse, errorSpecification,
				oCreditCardTransaction.getTransactionId(), oCreditCardTransaction.getTransactionId(), tnxResultCode);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, oCreditCardTransaction.getTransactionId());
		log.debug("[PaymentBrokerMigsQueryDR::query()] End");
		return sr;
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		PaymentBrokerManager.validateIPGIdentificationParams(this.ipgIdentificationParamsDTO);
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}
}
