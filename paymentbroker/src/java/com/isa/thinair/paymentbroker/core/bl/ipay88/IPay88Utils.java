package com.isa.thinair.paymentbroker.core.bl.ipay88;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Base64;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * The IPay88Utils class for keep IPay88 utilities
 * 
 */
public class IPay88Utils {

	private static Log log = LogFactory.getLog(IPay88Utils.class);

	public static final String MERCHANTKEY = "MerchantKey";

	/**
	 * This method is to compute sha1 (base 64) when String Map is given as input parameter
	 * 
	 * @param postDataMap
	 *            the postDataMap to set
	 * @return base64 SHA1 String
	 */
	public static String computeSHA1(Map<String, String> postDataMap) throws Exception {
		StringBuilder stringToHash = new StringBuilder();
		for (String key : postDataMap.keySet()) {
			stringToHash.append(postDataMap.get(key));
		}
		return computeSHA1(stringToHash.toString());
	}

	/**
	 * This method is to compute sha1 (base 64) when String is given as input parameter
	 * 
	 * @param string
	 *            value
	 * 
	 * @return base64 SHA1 String
	 */
	public static String computeSHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md;
		md = MessageDigest.getInstance("SHA1");
		byte[] sha1hash = new byte[40];
		md.update(text.getBytes("UTF-8"), 0, text.length());
		sha1hash = md.digest();
		return convertToBase64(sha1hash);
	}

	/**
	 * This method is to convert byte array to base 64
	 * 
	 * @param byte array
	 * 
	 * @return base64 SHA1 String
	 */
	private static String convertToBase64(byte[] data) {
		return Base64.getEncoder().encodeToString(data);
	}

}
