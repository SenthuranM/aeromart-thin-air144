package com.isa.thinair.paymentbroker.core.bl.mtcew;

import java.util.Map;

public class HTTPRequestParams {

	public static final String HTTP_METHOD_GET = "GET";

	public static final String HTTP_METHOD_POST = "POST";

	private String httpMethod;

	private String targetUrl;

	private boolean useProxy;

	private String proxyHost;

	private int proxyPort;

	private Map<String, String> requestParamsMap;

	/**
	 * @return the httpMethod
	 */
	public String getHttpMethod() {
		return httpMethod;
	}

	/**
	 * @param httpMethod
	 *            the httpMethod to set
	 */
	public void setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
	}

	/**
	 * @return the targetUrl
	 */
	public String getTargetUrl() {
		return targetUrl;
	}

	/**
	 * @param targetUrl
	 *            the targetUrl to set
	 */
	public void setTargetUrl(String targetUrl) {
		this.targetUrl = targetUrl;
	}

	/**
	 * @return the useProxy
	 */
	public boolean isUseProxy() {
		return useProxy;
	}

	/**
	 * @param useProxy
	 *            the useProxy to set
	 */
	public void setUseProxy(boolean useProxy) {
		this.useProxy = useProxy;
	}

	/**
	 * @return the proxyHost
	 */
	public String getProxyHost() {
		return proxyHost;
	}

	/**
	 * @param proxyHost
	 *            the proxyHost to set
	 */
	public void setProxyHost(String proxyHost) {
		this.proxyHost = proxyHost;
	}

	/**
	 * @return the proxyPort
	 */
	public int getProxyPort() {
		return proxyPort;
	}

	/**
	 * @param proxyPort
	 *            the proxyPort to set
	 */
	public void setProxyPort(int proxyPort) {
		this.proxyPort = proxyPort;
	}

	/**
	 * @return the requestParamsMap
	 */
	public Map<String, String> getRequestParamsMap() {
		return requestParamsMap;
	}

	/**
	 * @param requestParamsMap
	 *            the requestParamsMap to set
	 */
	public void setRequestParamsMap(Map<String, String> requestParamsMap) {
		this.requestParamsMap = requestParamsMap;
	}
}
