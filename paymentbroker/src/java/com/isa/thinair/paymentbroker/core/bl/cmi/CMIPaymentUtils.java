package com.isa.thinair.paymentbroker.core.bl.cmi;

import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.CONTENT_LENGTH;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.CONTENT_TYPE;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.ENCODING_ALG;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.PIPE;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.SLASH;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.TEXT_XML;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.UTF_8;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.YEAR_PREFIX;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.cmi.CMIRequest;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;

public class CMIPaymentUtils {

	private CMIPaymentUtils() {

	}

	public static final String APP_NAME = "IBE";
	private static Log log = LogFactory.getLog(CMIPaymentUtils.class);

	/**
	 * Generating XML string from the request object
	 * 
	 * @param request
	 * @return
	 * @throws JAXBException
	 */
	public static String generateXMLString(CMIXMLRequest request)
			throws JAXBException {
		JAXBContext contextObj = JAXBContext.newInstance(CMIXMLRequest.class);
		Marshaller marshallerObj = contextObj.createMarshaller();
		marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		StringWriter sw = new StringWriter();
		marshallerObj.marshal(request, sw);
		return sw.toString();
	}

	/**
	 * A method to retrieve response object from CMI_PG API response
	 * 
	 * @param input
	 * @return
	 * @throws JAXBException
	 * @throws IOException
	 * @throws XMLStreamException
	 * @throws ModuleException
	 */
	public static CMIXMLResponse generateResponseObject(String input)
			throws JAXBException, XMLStreamException, IOException,
			ModuleException {
		if (input == null) {
			throw new ModuleException(
					"paymentbroker.cmi.online.request.API.error");
		}

		JAXBContext jaxbContext = JAXBContext.newInstance(CMIXMLResponse.class);
		XMLInputFactory xif = XMLInputFactory.newFactory();
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		XMLStreamReader xsr = xif.createXMLStreamReader(IOUtils.toInputStream(
				input, UTF_8));
		return (CMIXMLResponse) jaxbUnmarshaller.unmarshal(xsr);

	}

	/**
	 * HTTP client for firing the REST request.
	 * 
	 * @param request
	 * @param connectionUrl
	 * @return
	 * @throws XMLStreamException
	 * @throws JAXBException
	 * @throws ModuleException
	 */
	public static CMIXMLResponse fireRequest(String request,
			String connectionUrl) throws XMLStreamException, JAXBException,
			ModuleException {
		try {
			java.net.URL url = new java.net.URL(connectionUrl);
			java.net.HttpURLConnection con = (java.net.HttpURLConnection) url
					.openConnection();
			con.setUseCaches(false);
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setRequestProperty(CONTENT_TYPE, TEXT_XML);
			con.setConnectTimeout(60000);
			// xmlDocAsStr is the submit XML document as a String
			con.setRequestProperty(CONTENT_LENGTH, Integer
					.toString(request == null ? 0 : request.getBytes().length));
			PrintStream out = null;
			if (request != null) {
				out = new PrintStream(con.getOutputStream());
				out.print(request); // send the submit xml document
				out.close();
			}
			BufferedReader buffReader = new BufferedReader(
					new InputStreamReader(con.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String inputLine;
			while ((inputLine = buffReader.readLine()) != null) {
				sb.append(inputLine); // receive the response xml document
			}
			// the response XML document is now in sb as a String
			if (out != null) {
				out.close(); // ALWAYS close your connection !
			}
			return generateResponseObject(sb.toString());
		} catch (Exception e) {
			throw new ModuleException(
					"paymentbroker.cmi.online.request.api.xml.error");
		}
	}

	public static String getExpiryDate(String expiryDate) {
		if (expiryDate != null) {
			String year = YEAR_PREFIX + expiryDate.substring(0, 2);
			String month = expiryDate.substring(expiryDate.length() - 2,
					expiryDate.length());
			return month + SLASH + year;
		}
		return expiryDate;

	}

	public static StringBuilder prepareCustomID(IPGRequestDTO ipgRequestDTO) {

		StringBuilder merchantTxnId = new StringBuilder();
		merchantTxnId.append(AppIndicatorEnum.APP_IBE);
		merchantTxnId.append(PaymentConstants.PIPE);
		merchantTxnId.append(ipgRequestDTO.getIpgIdentificationParamsDTO()
				.getIpgId());
		merchantTxnId.append(PaymentConstants.PIPE);
		merchantTxnId.append(ipgRequestDTO.getIpgIdentificationParamsDTO()
				.getPaymentCurrencyCode());
		merchantTxnId.append(PaymentConstants.PIPE);
		merchantTxnId.append(ipgRequestDTO.getApplicationTransactionId());
		return merchantTxnId;
	}

	/**
	 * This method is used for generating plain text from the parameter map.the
	 * order of the parameter is given below
	 * //amount|BillToCompany|BillToName|callbackUrl
	 * |clientid|currency|email|failUrl|hashAlgorithm|la
	 * ng|okurl|rnd|storetype|TranType|storeKey
	 * 
	 * @param postDataMap
	 * @return
	 * @throws ModuleException
	 */
	/**
	 * public static String generatePlainText(Map<String, String> postDataMap)
	 * throws ModuleException { // init hash value StringBuilder hashVal = new
	 * StringBuilder(); try { // create sorted map SortedMap<String, String>
	 * allRequestParams = new TreeMap<>();
	 * 
	 * postDataMap.forEach((k, v) -> { allRequestParams.put(k, v);
	 * 
	 * });
	 * 
	 * allRequestParams .forEach((k, v) -> { log.info(k + "==" + v); if
	 * (!EXCEPTONALPARAMETERSET.contains(k .toLowerCase(Locale.US))) {
	 * hashVal.append(formatValue(v)).append(PIPE);
	 * 
	 * }
	 * 
	 * }); String storeKey = postDataMap.get(CMIRequest.STOREKEY);
	 * hashVal.append(formatValue(storeKey));
	 * 
	 * return hashVal.toString(); } catch (Exception e) { log.error(e); throw
	 * new ModuleException(
	 * "paymentbroker.cmi.online.request.rejected.invalid_hashKey"); } }
	 **/

	public static String getRandomString() {
		return RandomStringUtils.random(8, true, true);
	}

	private static String formatValue(String value) {
		return value.replace("\\", "\\\\").replace("|", "\\|");
	}

	public static String generateHash(String plainText) throws ModuleException {
		try {
			return new String(Base64.getEncoder().encode(
					MessageDigest.getInstance(ENCODING_ALG).digest(
							plainText.getBytes(StandardCharsets.UTF_8))));
		} catch (NoSuchAlgorithmException e) {
			log.error(e);// exception handling
			throw new ModuleException(
					"paymentbroker.cmi.online.request.rejected.invalid_hashKey");
		}
	}

	@SuppressWarnings("unchecked")
	/**public static String prepareResponseString(Map receiptyMap) {

		StringBuilder recieptString = new StringBuilder();

		if (receiptyMap != null) {
			receiptyMap.forEach((k, v) -> {
				recieptString.append(k + ":" + v);
			});

		}
		return recieptString.toString();

	}**/
	public static String prepareResponseString(Map receiptMap) {
		StringBuilder recieptString = new StringBuilder();

		Iterator it = receiptMap.entrySet().iterator();
		while (it.hasNext()) {
			if (recieptString.length() > 0) {
				recieptString.append(",");
			}
		
			Map.Entry pair = (Map.Entry) it.next();
			recieptString.append(pair.getKey() + ":" + pair.getValue());
		}
		return recieptString.toString();
	}

	/**
	 * 
	 * @param postRequestString
	 * @param pGUrl
	 * @param serviceResponse
	 * @throws ModuleException
	 */
	public static CMIXMLResponse executePostRequest(String postRequestString,
			String pGUrl, DefaultServiceResponse serviceResponse)
			throws ModuleException {

		return executeCMIApiRequest(postRequestString, pGUrl);

	}

	/**
	 * 
	 * @param requestString
	 * @param ipgURL
	 * @return
	 * @throws ModuleException
	 */
	public static CMIXMLResponse executeCMIApiRequest(String requestString,
			String ipgURL) throws ModuleException {
		try {
			return CMIPaymentUtils.fireRequest(requestString, ipgURL);
		} catch (XMLStreamException | JAXBException e) {
			log.error("Error while calling CMI webservice" + e);
			throw new ModuleException(
					"paymentbroker.cmi.online.request.api.xml.error");
		} catch (Exception e) {
			throw new ModuleException(
					"paymentbroker.cmi.online.request.api.xml.unexpected.error");
		}
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	public static String generateXMLRequest(CMIXMLRequest request)
			throws ModuleException {
		String requestString = null;
		try {
			requestString = CMIPaymentUtils.generateXMLString(request);
		} catch (JAXBException e) {
			log.error(CMIConstants.ERROR_MSG_WS + e);
			throw new ModuleException(e,
					"paymentbroker.cmi.online.request.API.error");
		}
		if (requestString == null) {
			throw new ModuleException(
					"paymentbroker.cmi.online.request.API.error");
		}
		return requestString;

	}

	public static String buildErrorCode(String errorCode, Object object,
			String responseFromCMI) {
		StringBuilder errorCodes = new StringBuilder();
		errorCodes.append(errorCode);
		errorCodes.append(PIPE);
		errorCodes.append(object);
		errorCodes.append(PIPE);
		errorCodes.append(responseFromCMI);
		return errorCodes.toString();
	}

	public static void prepareErrorStatus(
			DefaultServiceResponse serviceResponse, CMIXMLResponse postResponse) {
		String status = IPGResponseDTO.STATUS_REJECTED;
		status = status + "" + postResponse.getErrMsg();
		serviceResponse.addResponceParam(
				PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, status);

	}

	// for test only

	public  static String generatePlainText(Map<String, String> postDataMap) {
		try {
			// create sorted map
						SortedMap<String, String> allRequestParams = new TreeMap<String, String>(
								new Comparator<String>() {
									public int compare(String str1, String str2) {
										str1 = str1.toUpperCase(Locale.US);
										str2 = str2.toUpperCase(Locale.US);
										return str1.compareTo(str2);
									}
								});
			Set<String> requestParams = postDataMap.keySet();
			for (String params : requestParams) {
				String value = postDataMap.get(params);
				allRequestParams.put(params, value);
			}

			// init hash value
			String hashVal = "";
			for (String requestParam : allRequestParams.keySet()) {
				
				String lowerParam = requestParam.toLowerCase(Locale.US);
				if (!lowerParam.equals("encoding")
						&& !lowerParam.equals("hash")
						&& !lowerParam.equals("storeKey")) {
					hashVal += postDataMap.get(requestParam)
							.replace("\\", "\\\\").replace("|", "\\|").trim()
							+ "|";
				}
			}
			String storeKey = postDataMap.get(CMIRequest.STOREKEY);

			storeKey = storeKey.replace("\\", "\\\\").replace("|", "\\|");
			hashVal += storeKey;
			log.info("*********************Plain text for hashing:-- "
					+ hashVal);
			return hashVal;
		} catch (Exception e) {
			log.error(e);
		}
		return null;
	}
	
	public static  String fetchAppTxnId(Map param)
	{
		String customInformation = (String) param.get("oid");
		if (customInformation != null) {
			String resultArray[] = customInformation.split("\\|");
			if (resultArray != null && resultArray.length > 0) {
				return  resultArray[3];

			}
		}
		return null;
	}

}
