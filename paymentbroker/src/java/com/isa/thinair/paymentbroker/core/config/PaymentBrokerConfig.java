package com.isa.thinair.paymentbroker.core.config;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @author Srikantha isa.module.config-bean
 */
public class PaymentBrokerConfig extends DefaultModuleConfig {

	public PaymentBrokerConfig() {
		super();
	}
}
