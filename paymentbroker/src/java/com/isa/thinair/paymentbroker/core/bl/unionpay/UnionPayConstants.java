package com.isa.thinair.paymentbroker.core.bl.unionpay;

public final class UnionPayConstants {

	public static String VERSION = "version";

	public static String ENCODING = "encoding";

	public static String CERT_ID = "certId";

	public static String SIGNATURE = "signature";

	public static String SIGN_METHOD = "signMethod";

	public static String TXN_TYPE = "txnType";

	public static String TXN_SUB_TYPE = "txnSubType";

	public static String PRODUCT_TYPE = "bizType";

	public static String ACCESS_TYPE = "accessType";

	public static String CHANNEL_TYPE = "channelType";

	public static String FRONT_END_URL = "frontUrl";

	public static String BACK_END_URL = "backUrl";

	public static String ACQ_INST_CODE = "acqInsCode";

	public static String MERCHANT_ID = "merId";

	public static String MERCHANT_TYPE = "merCatCode";

	public static String MERCHANT_NAME = "merName";

	public static String MERCHANT_ABBR = "merAbbr";

	public static String SUB_MERCHANT_ID = "subMerId";

	public static String SUB_MERCHANT_NAME = "subMerName";

	public static String SUB_MERCHANT_ABN = "subMerAbbr";

	public static String ORDER_ID = "orderId";

	public static String TXN_TIME = "txnTime";

	public static String PRIMARY_ACC_NO = "accNo";

	public static String TXN_AMOUNT = "txnAmt";

	public static String TXN_CURRENCY = "currencyCode";

	public static String CUSTOMER_INFO = "customerInfo";

	public static String ORDER_TIMEOUT = "payTimeout";

	public static String REQUESTOR_RESERVED_FIELD = "reqReserved";

	public static String RESERVED_FIELD = "reserved";

	public static String RISK_RATE_INFO = "riskRateInfo";

	public static String ENCRYPTED_CERT_ID = "encryptCertId";

	public static String ERROR_REDIRECT_URL = "frontFailUrl";

	public static String CUSTOMER_IP = "customerIp";

	public static String TERMINAL_ID = "termId";

	public static String CARD_TNX_DATA = "cardTransData";

	public static String ORDER_DESCRIPTION = "orderDesc";

	public static String TOKEN_PAY_INFO = "tokenPayData";

	public static String TXN_QUERY_NUMBER = "queryId";

	public static String ORIGIN_QUERY_NUMBER = "origQryId";

	public static String RS_CODE = "respCode";

	public static String RS_MSG = "respMsg";

	public static String PAY_CARD_TYPE = "payCardType";

	public static String TXN_NUMBER = "tn";

	public static String SETTLE_DATE = "settleDate";

	public static String SETTLE_CURR_CODE = "settleCurrencyCode";

	public static String SETTLE_AMNT = "settleAmt";

	public static String EXC_RATE = "exchangeRate";

	public static String EXC_DATE = "exchangeDate";

	public static String TRACE_NO = "traceNo";

	public static String TRACE_TIME = "traceTime";

	public static String PAYMENT_CARD_NAME = "payCardIssueNa";

	public static String CARD_TXN_DATA = "cardTransData";

	public static String ORIG_TNX_QUERY_ID = "origQryId";

	public static String ISSUER_ID_MODE = "IsserIdentifyMode";

	public static String ORIGIN_RS_MSG = "origRespMsg";

	public static String ORIGIN_RS_CODE = "origRespCode";

	public static String CARD_NO = "payCardNo";

	public static String CARD_ISSUE_NAME = "payCardIssueNa";

	public static String CARD_DATA = "cardTransData";

	public static interface TxnType {
		public static final String STATE_INQUIRY = "00";
		public static final String PURCHASE = "01";
		public static final String PURCHASE_CANCEL = "31";
		public static final String PURCHASE_REFUND = "04";

	}

	public static interface TxnSubType {
		public static final String DEFAULT = "00";
		public static final String PURCHASE = "01";
		public static final String MOTO = "02";
	}

	public static interface ProductType {
		public static final String DEFAULT = "000000";
		public static final String SECURE_PAY = "000201";
	}

	public static interface AccessType {
		public static final String MERCAHNT_DIRECT = "0";
		public static final String AQUIRER = "1";
		public static final String PLATFORM_MERCHANT = "2";
	}

	public static interface SignatureMethod {
		public static final String RSA = "01";
	}

	public static interface ChannelType {
		public static final String INTERNET = "07";
		public static final String MOBILE = "08";
	}

	public static interface ResponseCode {
		public static final String SUCCESS = "00";
	}
}
