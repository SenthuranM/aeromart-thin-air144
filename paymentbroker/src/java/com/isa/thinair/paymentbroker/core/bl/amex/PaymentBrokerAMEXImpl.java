package com.isa.thinair.paymentbroker.core.bl.amex;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class PaymentBrokerAMEXImpl extends PaymentBrokerTemplate implements PaymentBroker {

	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	public static final String PARAM_ENCRYPTED_RECEIPT_PAY = "encryptedReceiptPay";

	private static final String TRANSACTION_STATUS_ACCEPTED = "ACCEPTED";

	private static final String DEFAULT_TRANSACTION_CODE = "5"; // Auth
																// declined
	private static final int CARD_TYPE_AMEX = 3;

	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		String finalAmount = ipgRequestDTO.getAmount();
		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		postDataMap.put(AMEXRequest.PSPID, getMerchantId());
		postDataMap.put(AMEXRequest.IPGURL, getIpgURL());
		postDataMap.put(AMEXRequest.REQUESTID, merchantTxnId);
		postDataMap.put(AMEXRequest.PASSWORD, getPassword());

		postDataMap.put(AMEXRequest.KEYSTORE_LOCATION, PlatformConstants.getConfigRootAbsPath()
				+ "/repository/modules/paymentbroker/" + getKeystoreName());
		postDataMap.put(AMEXRequest.AMOUNT, finalAmount);
		postDataMap.put(AMEXRequest.CURRENCY, ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode());
		postDataMap.put(AMEXRequest.LANGUAGE, ipgRequestDTO.getSessionLanguageCode());
		postDataMap.put(AMEXRequest.RETURNURL, ipgRequestDTO.getReturnUrl());
		postDataMap.put(AMEXRequest.ACTION, "SaleTxn");
		postDataMap.put(AMEXRequest.LOG_PATH, getLogPath());

		String requestDataForm = AMEXPaymentUtils.getPostInputDataFormHTML(postDataMap);

		String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();

		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				strRequestParams + "," + sessionID, "", getPaymentGatewayName(), null, false);

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(requestDataForm);
		ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setPostDataMap(postDataMap);

		return ipgRequestResultsDTO;
	}

	protected String composeMerchantTransactionId(AppIndicatorEnum appIndicatorEnum, long auditId,
			PaymentConstants.MODE_OF_SERVICE mode) {
		if (mode.equals(PaymentConstants.MODE_OF_SERVICE.REFUND)) {
			return appIndicatorEnum.getCode() + (new Long(auditId)).toString() + MODE_OF_SERVICE_REFUND_PREFIX;
		} else if (mode.equals(PaymentConstants.MODE_OF_SERVICE.VOID)) {
			return appIndicatorEnum.getCode() + (new Long(auditId)).toString() + MODE_OF_SERVICE_VOID_PREFIX;
		} else if (mode.equals(PaymentConstants.MODE_OF_SERVICE.QUERYDR)) {
			return appIndicatorEnum.getCode() + (new Long(auditId)).toString() + MODE_OF_SERVICE_QUERY_PREFIX;
		} else if (mode.equals(PaymentConstants.MODE_OF_SERVICE.CAPTURE)) {
			return appIndicatorEnum.getCode() + (new Long(auditId)).toString() + MODE_OF_SERVICE_CAPTURE_PREFIX;
		} else if (mode.equals(PaymentConstants.MODE_OF_SERVICE.STATUS)) {
			return appIndicatorEnum.getCode() + (new Long(auditId)).toString() + MODE_OF_SERVICE_STATUS_PREFIX;
		} else {
			return appIndicatorEnum.getCode() + (new Long(auditId)).toString();
		}
	}

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO migs2IPGResponseDTO) throws ModuleException {
		IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();

		CreditCardTransaction migs2CreditCardTransaction = loadAuditTransactionByTnxRefNo(migs2IPGResponseDTO
				.getPaymentBrokerRefNo());
		String strResponseTime = CalendarUtil.getTimeDifference(migs2IPGResponseDTO.getRequestTimsStamp(),
				migs2IPGResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(migs2IPGResponseDTO.getRequestTimsStamp(),
				migs2IPGResponseDTO.getResponseTimeStamp());

		// Update response time
		migs2CreditCardTransaction.setComments(strResponseTime);
		migs2CreditCardTransaction.setResponseTime(timeDiffInMillis);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(migs2CreditCardTransaction);
		String plainTextResponse = AMEXPaymentUtils.getDecryptedResponse(
				BeanUtils.nullHandler(receiptyMap.get(AMEXPaymentUtils.PAYMENT_RESPONSE_HASH)), getLogPath());

		AMEXResponse migs2Response = new AMEXResponse();
		migs2Response.setXml(plainTextResponse);

		String status = migs2Response.getStatus();
		String message = (migs2Response.getReason() != null ? migs2Response.getReason() : "");
		String errorCode = "";
		String appTxnId = migs2Response.getMerchantRefId();
		String authorizationCode = "";
		String errorSpecification = status + " " + errorCode + " " + message;
		String ipgTxnId = migs2Response.getIpgTransactionId();
		int tnxResultCode = 0;

		if (TRANSACTION_STATUS_ACCEPTED.equals(status)) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			tnxResultCode = 1;
			errorSpecification = "";
			authorizationCode = migs2Response.getAuthorizationCode();
		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			errorCode = DEFAULT_TRANSACTION_CODE;
			errorSpecification = status + " " + errorCode;
		}

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setMessage(message);
		ipgResponseDTO.setApplicationTransactionId(appTxnId);
		ipgResponseDTO.setAuthorizationCode(authorizationCode);

		// TODO verify and improve AMEX card number storage, now setting "" as AMEX is external
		// ipgResponseDTO.setCcLast4Digits(migs2Response.getMaskedAccNo().substring(migs2Response.getMaskedAccNo().length()
		// - 4));
		ipgResponseDTO.setCcLast4Digits("");

		// FIXME: As this PG only supports AMEX cards, as per client's configuration (CinnamonAir) and we don't get a
		// card type
		// from response, card type is hard coded as AMEX. But in a scenario where PG supports multiple card types this
		// will not
		// work. Hence should get a clear clarification from PG and set the card type here as per their feedback
		ipgResponseDTO.setCardType(CARD_TYPE_AMEX);

		updateAuditTransactionByTnxRefNo(migs2IPGResponseDTO.getPaymentBrokerRefNo(), plainTextResponse, errorSpecification,
				authorizationCode, ipgTxnId, tnxResultCode);

		return ipgResponseDTO;

	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection creditCardPayment, boolean refundFlag) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");

	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

}
