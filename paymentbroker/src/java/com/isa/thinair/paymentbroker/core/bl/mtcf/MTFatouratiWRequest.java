/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.bl.mtcf;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * MTC Fatourati payment request
 * 
 * @author Primal
 */
public class MTFatouratiWRequest {

	private static Log log = LogFactory.getLog(MTFatouratiWRequest.class);

	public static final String MTC_ACTION_SLK = "paymentForm";

	public static final String MTC_STORE_ID = "storeId";

	public static final String MTC_LANGUE = "langue";

	public static final String MTC_OFFER_URL = "offerURL";

	public static final String MTC_UPDATE_URL = "updateURL";

	public static final String MTC_BOOKING_URL = "bookURL";

	public static final String MTC_CART_ID = "cartId";

	public static final String MTC_AMOUNT_TX = "totalAmountTx";

	public static final String MTC_TOTAL_AMOUNT_CURRENCY = "totalAmountCur";

	public static final String MTC_SYMBOL_CURRENCY = "symbolCur";

	public static final String MTC_BUYER_NAME = "name";

	public static final String MTC_ADDRESS = "address";

	public static final String MTC_CITY = "city";

	public static final String MTC_STATE = "state";

	public static final String MTC_COUNTRY = "country";

	public static final String MTC_POST_CODE = "postCode";

	public static final String MTC_TELEPHONE_NO = "tel";

	public static final String MTC_EMAIL_ID = "email";

	public static final String MTC_MERCHANT_TYPE = "merchantType";

	public static final String MTC_PAYMENT_TYPE = "paymentType";

	public static final String MTC_MERCHANT_FATOURATATI_CODE = "merchantFatouratiCode";

	public static final String MTC_CREANCE_FATOURATATI_CODE = "creanceFatouratiCode";

	public static final String MTC_EXPIRATION_DATE = "expirationDate";

	public static final String MTC_CHECH_SUM = "checksum";

	public static final String CHAR_ENC = "UTF-8";

	/*
	 * public static final String MTC_MODE = "Mode";
	 * 
	 * public static final String MTC_COUNT = "Count";
	 * 
	 * public static final String MTC_DESC = "Desc";
	 * 
	 * public static final String MTC_QTY = "Qty";
	 * 
	 * public static final String MTC_ITEM_PRICES = "ItemsPrices";
	 * 
	 * public static final String MTC_SHIPPING_CHARGES = "ShippingCharge";
	 * 
	 * public static final String MTC_SHIPPING_WEIGHT = "ShippingWeight";
	 * 
	 * public static final String MTC_TOTAL_AMOUNT_TX = "TotalmountTx";
	 */

	Map<String, String> fields = new LinkedHashMap<String, String>();

	public MTFatouratiWRequest() {
	}

	/**
	 * This method is for creating a URL query string.
	 * 
	 * @param buf
	 *            is the inital URL for appending the encoded fields to
	 * @param fields
	 *            is the input parameters from the order page
	 */
	public void appendQueryFields(StringBuffer buf, Map fields) throws Exception {

		// create a list
		List fieldNames = new ArrayList(fields.keySet());
		Iterator itr = fieldNames.iterator();

		// move through the list and create a series of URL key/value pairs
		while (itr.hasNext()) {
			String fieldName = (String) itr.next();
			String fieldValue = (String) fields.get(fieldName);

			if ((fieldValue != null) && (fieldValue.length() > 0)) {
				// append the URL parameters

				buf.append(URLEncoder.encode(fieldName, CHAR_ENC));

				buf.append('=');
				buf.append(URLEncoder.encode(fieldValue, CHAR_ENC));
			}

			// add a '&' to the end if we have more fields coming.
			if (itr.hasNext()) {
				buf.append('&');
			}
		}
	}

}
