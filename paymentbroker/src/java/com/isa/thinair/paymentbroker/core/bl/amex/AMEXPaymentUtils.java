package com.isa.thinair.paymentbroker.core.bl.amex;

import ipgclient2.CShroff2;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.webplatform.api.util.CommonUtil;

public class AMEXPaymentUtils {

	public static final String PAYMENT_RESPONSE_HASH = "encryptedReceiptPay";
	private static Log log = LogFactory.getLog(AMEXPaymentUtils.class);

	private static String getPaymentRequest(Map<String, String> postDataMap) {
		StringBuilder sb = new StringBuilder();
		sb.append("<req>");
		sb.append("<mer_id>" + postDataMap.get(AMEXRequest.PSPID) + "</mer_id>");
		sb.append("<mer_txn_id>" + postDataMap.get(AMEXRequest.REQUESTID) + "</mer_txn_id>");
		sb.append("<action>" + postDataMap.get(AMEXRequest.ACTION) + "</action>");
		sb.append("<txn_amt>"
				+ CommonUtil.getBigDecimalWithDefaultPrecision(new BigDecimal(postDataMap.get(AMEXRequest.AMOUNT))).toString()
				+ "</txn_amt>");
		sb.append("<cur>" + postDataMap.get(AMEXRequest.CURRENCY) + "</cur>");
		sb.append("<lang>" + postDataMap.get(AMEXRequest.LANGUAGE) + "</lang>");
		sb.append("<ret_url>" + postDataMap.get(AMEXRequest.RETURNURL) + "</ret_url>");
		sb.append("</req>");
		return sb.toString();
	}

	public static String getEncryptedPaymentRequest(String originalRequest, String logpath) {
		CShroff2 cShroff2 = new CShroff2(PlatformConstants.getConfigRootAbsPath() + "/repository/modules/paymentbroker/", logpath);
		int val = cShroff2.getErrorCode();
		if (val == 0) {
			val = cShroff2.setPlainTextInvoice(originalRequest);
			if (val == 0) {
				return cShroff2.getEncryptedInvoice();
			} else {
				log.info("paymentRequestEncryptionFailed : " + cShroff2.getErrorMsg());
				return null;
			}
		} else {
			log.info("paymentRequestEncryptionFailed : " + cShroff2.getErrorMsg());
			return null;
		}
	}

	public static String getPostInputDataFormHTML(Map<String, String> postDataMap) {
		String paymentRequest = getPaymentRequest(postDataMap);
		log.debug("paymentRequest : " + paymentRequest);
		String encryptedRequest = getEncryptedPaymentRequest(paymentRequest, postDataMap.get(AMEXRequest.LOG_PATH));
		log.debug("encryptedPaymentRequest : " + encryptedRequest);

		return "<form method='POST' action='" + postDataMap.get(AMEXRequest.IPGURL) + "' name = form1 >"
				+ "<input type='hidden' value='" + encryptedRequest + "' name='encryptedInvoicePay'>"
				+ "<script type='text/javascript'>" + "function AutoSubmitForm() {document.form1.submit();}</script></form>";
	}

	public static String getDecryptedResponse(String encryptedResponse, String logpath) {
		int result = 0;
		if (encryptedResponse != null) {
			if (encryptedResponse.length() > 0) {
				CShroff2 cShroff2 = new CShroff2(PlatformConstants.getConfigRootAbsPath() + "/repository/modules/paymentbroker/",
						logpath);

				result = cShroff2.getErrorCode();
				if (result == 0) {
					result = cShroff2.setEncryptedReceipt(encryptedResponse);

					if (result == 0) {
						return cShroff2.getPlainTextReceipt();
					} else {
						log.error(cShroff2.getErrorCode() + " : " + cShroff2.getErrorMsg());
						return cShroff2.getErrorMsg();
					}

				} else {
					log.error(cShroff2.getErrorCode() + " : " + cShroff2.getErrorMsg());
					return cShroff2.getErrorMsg();
				}
			}
		}
		return null;
	}

	public static IPGConfigsDTO getIPGConfigs(String merchantID, String userName, String password) {
		IPGConfigsDTO oIPGConfigsDTO = new IPGConfigsDTO();
		oIPGConfigsDTO.setMerchantID(merchantID);
		oIPGConfigsDTO.setUsername(userName);
		oIPGConfigsDTO.setPassword(password);

		return oIPGConfigsDTO;
	}
}
