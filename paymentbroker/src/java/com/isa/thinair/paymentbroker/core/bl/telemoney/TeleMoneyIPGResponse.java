/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.bl.telemoney;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Dhanushka Ranatunga
 */
public class TeleMoneyIPGResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6802733539979870948L;

	/** Indicates that a bank had authorized the payment transaction */
	public static final String STATUS_YES = "YES";

	/** Indicates that bank rejected the transaction */
	public static final String STATUS_NO = "NO";

	/** Indicates that shopper cancels the payment */
	public static final String STATUS_ABORT = "ABT";

	private String merchantId;

	private String merchantTxnId;

	private String currency;

	private String amount;

	private String status;

	private String errorMsg;

	private String paymentType;

	private String approvalCode;

	private String reverseId;

	public TeleMoneyIPGResponse(Map receiptyMap) {
		if (receiptyMap != null) {
			this.setMerchantId(PlatformUtiltiies.nullHandler(receiptyMap.get("TM_MCode")));
			this.setMerchantTxnId(PlatformUtiltiies.nullHandler(receiptyMap.get("TM_RefNo")));
			this.setCurrency(PlatformUtiltiies.nullHandler(receiptyMap.get("TM_Currency")));
			this.setAmount(PlatformUtiltiies.nullHandler(receiptyMap.get("TM_DebitAmt")));
			this.setStatus(PlatformUtiltiies.nullHandler(receiptyMap.get("TM_Status")));
			this.setErrorMsg(PlatformUtiltiies.nullHandler(receiptyMap.get("TM_ErrorMsg")));
			this.setPaymentType(PlatformUtiltiies.nullHandler(receiptyMap.get("TM_PaymentType")));
			this.setApprovalCode(PlatformUtiltiies.nullHandler(receiptyMap.get("TM_ApprovalCode")));
			this.setReverseId(PlatformUtiltiies.nullHandler(receiptyMap.get("TM_UxID")));
		}
	}

	/**
	 * @return Returns the amount.
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            The amount to set.
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return Returns the approvalCode.
	 */
	public String getApprovalCode() {
		return approvalCode;
	}

	/**
	 * @param approvalCode
	 *            The approvalCode to set.
	 */
	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

	/**
	 * @return Returns the currency.
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            The currency to set.
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return Returns the errorMsg.
	 */
	public String getErrorMsg() {
		return errorMsg;
	}

	/**
	 * @param errorMsg
	 *            The errorMsg to set.
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	/**
	 * @return Returns the merchantId.
	 */
	public String getMerchantId() {
		return merchantId;
	}

	/**
	 * @param merchantId
	 *            The merchantId to set.
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	/**
	 * @return Returns the merchantTxnId.
	 */
	public String getMerchantTxnId() {
		return merchantTxnId;
	}

	/**
	 * @param merchantTxnId
	 *            The merchantTxnId to set.
	 */
	public void setMerchantTxnId(String merchantTxnId) {
		this.merchantTxnId = merchantTxnId;
	}

	/**
	 * @return Returns the paymentType.
	 */
	public String getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType
	 *            The paymentType to set.
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * @return Returns the reverseId.
	 */
	public String getReverseId() {
		return reverseId;
	}

	/**
	 * @param reverseId
	 *            The reverseId to set.
	 */
	public void setReverseId(String reverseId) {
		this.reverseId = reverseId;
	}

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	public static Map getResponseParamMap(String responseText) {
		Map responseParamMap = new HashMap();
		String[] params = new String(responseText).trim().split("&");
		for (int i = 0; i < params.length; i++) {
			String[] element = params[i].split(":");
			responseParamMap.put(element[0].trim(), element[1].trim());
		}
		return responseParamMap;
	}

	public static String toString(Map receiptyMap) {
		StringBuffer resBuff = new StringBuffer();

		// create a list
		List fieldNames = new ArrayList(receiptyMap.keySet());
		Iterator itr = fieldNames.iterator();

		// move through the list and create a series of URL key/value pairs
		while (itr.hasNext()) {
			String fieldName = (String) itr.next();
			String fieldValue = (String) receiptyMap.get(fieldName);
			resBuff.append(fieldName + " : " + fieldValue);
			// add a '&' to the end if we have more fields coming.
			if (itr.hasNext()) {
				resBuff.append('&');
			}
		}
		return resBuff.toString();
	}
}