package com.isa.thinair.paymentbroker.core.bl.passargad;

import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;

public class PaymentBrokerPasargadQueryDR extends PaymentBrokerTemplate implements PaymentQueryDR {
	private static Log log = LogFactory.getLog(PaymentBrokerPasargadQueryDR.class);

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	@Override
	public ServiceResponce query(CreditCardTransaction passargadCreditCardTransaction, IPGConfigsDTO ipgConfigsDTO,
			QUERY_CALLER caller) throws ModuleException {

		String status;
		String errorCode;
		String transactionMsg;
		
		String merchantTxnReference = passargadCreditCardTransaction.getTempReferenceNum();
		String dateFormat1 = "yyyy/MM/dd HH:mm:ss";
		SimpleDateFormat format = new SimpleDateFormat(dateFormat1);
		String gregorianDate = format.format(CalendarUtil.getCurrentZuluDateTime());

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		postDataMap.put(PasargadRequest.MERCHANT_CODE, getMerchantId());
		postDataMap.put(PasargadRequest.TERMINAL_CODE, getTerminalCode());
		postDataMap.put(PasargadRequest.TIMESTAMP, gregorianDate);
		postDataMap.put(PasargadRequest.KEYSTORE_LOCATION, getKeystoreName());

		String[] tokens = passargadCreditCardTransaction.getRequestText().split(",");
		for (int i = 0; i < tokens.length - 1; i++) {
			String[] keyValuPair = tokens[i].split(":",2);
			if (keyValuPair[0].trim().equals(PasargadRequest.INVOICE_NUMBER)) {
				postDataMap.put(PasargadRequest.INVOICE_NUMBER, keyValuPair[1].trim());
			}
			if (keyValuPair[0].trim().equals(PasargadRequest.INVOICE_DATE)) {
				postDataMap.put(PasargadRequest.INVOICE_DATE, keyValuPair[1].trim());
			}
			if (keyValuPair[0].trim().equals(PasargadRequest.AMOUNT)) {
				postDataMap.put(PasargadRequest.AMOUNT, keyValuPair[1].trim());
			}
		}

		String encodedSign = PasargadPaymentUtil.getEncodedSign(postDataMap);
		String verifyRequest = "InvoiceNumber=" + postDataMap.get(PasargadRequest.INVOICE_NUMBER) + "&InvoiceDate="
				+ postDataMap.get(PasargadRequest.INVOICE_DATE) + "&MerchantCode="
				+ postDataMap.get(PasargadRequest.MERCHANT_CODE) + "&TerminalCode="
				+ postDataMap.get(PasargadRequest.TERMINAL_CODE) + "&Amount=" + postDataMap.get(PasargadRequest.AMOUNT)
				+ "&TimeStamp=" + postDataMap.get(PasargadRequest.TIMESTAMP) + "&Sign=" + encodedSign;

		String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

		// Add other required parameters to create the post request.
		postDataMap.put("PROXY_HOST", getProxyHost());
		postDataMap.put("PROXY_PORT", getProxyPort());
		postDataMap.put("USE_PROXY", isUseProxy() ? "Y" : "N");
		postDataMap.put("URL", getQueryURL());
		postDataMap.put("REQUEST_DATA", verifyRequest);
		postDataMap.put(PasargadRequest.KEYSTORE_ALIAS, getKeyStoreAlias());

		CreditCardTransaction ccTransaction = auditTransaction(passargadCreditCardTransaction.getTransactionReference(),
				getMerchantId(), merchantTxnReference, new Integer(passargadCreditCardTransaction.getTemporyPaymentId()),
				bundle.getString("SERVICETYPE_QUERY"), strRequestParams, "", getPaymentGatewayName(), null, false);

		VerifyTransactionResponse response;
		try {
			response = (VerifyTransactionResponse) PasargadPaymentUtil.getPasargadXMLResponse(postDataMap,
					VerifyTransactionResponse.class);
		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		log.debug("Query Response : " + response.toString());

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		if (VerifyTransactionResponse.TRUE.equals(response.getResult())) {
			errorCode = "";
			sr.setSuccess(true);
			status = IPGResponseDTO.STATUS_ACCEPTED;
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, PaymentConstants.SERVICE_RESPONSE_QUERYDR);
		} else {
			errorCode = response.getResultMessage();
			transactionMsg = IPGResponseDTO.STATUS_REJECTED;
			status = IPGResponseDTO.STATUS_REJECTED;
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
		}

		sr.setResponseCode(String.valueOf(ccTransaction.getTransactionRefNo()));
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
		sr.addResponceParam(PaymentConstants.STATUS, status);
		log.debug("FINISH QUERY TRANSACTION ");

		return sr;
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		PaymentBrokerManager.validateIPGIdentificationParams(this.ipgIdentificationParamsDTO);
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}
}
