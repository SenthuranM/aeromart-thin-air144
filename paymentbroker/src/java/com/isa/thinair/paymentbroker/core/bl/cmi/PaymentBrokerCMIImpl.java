package com.isa.thinair.paymentbroker.core.bl.cmi;

import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.AUTH_CODE;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.CMI_PG_HOST_HOST;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.CMI_PG_RETURN_URL_PATH;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.CMI_TXN;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.PIPE;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.RESPONSE_PARAM;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.RETURN_CODE;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.RETURN_CODE_SUCCESS;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGTransactionResultDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.cmi.CMIRequest;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class PaymentBrokerCMIImpl extends PaymentBrokerCMITemplate {
	private static ResourceBundle bundle = PaymentBrokerModuleUtil
			.getResourceBundle();

	private static Log log = LogFactory.getLog(PaymentBrokerCMIImpl.class);

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment,
			String pnr, AppIndicatorEnum appIndicator, TnxModeEnum tnxMode,
			List<CardDetailConfigDTO> cardDetailConfigData)
			throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils
				.getPaymentBrokerDAO().loadTransaction(
						creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null
				&& !PlatformUtiltiies.nullHandler(
						creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction
					.getTransactionRefNo()));
			sr.addResponceParam(
					PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID,
					creditCardTransaction.getAidCccompnay());
			return sr;
		}
		throw new ModuleException(
				"airreservations.temporyPayment.corruptedParameters");
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment,
			String pnr, AppIndicatorEnum appIndicator, TnxModeEnum tnxMode)
			throws ModuleException {
		throw new ModuleException(
				"paymentbroker.generic.operation.notsupported");
	}

	/**
	 * For IBE. The method will generate form based request for CMI payment
	 * gateway.
	 */
	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO)
			throws ModuleException {
		StringBuilder customOrderId = CMIPaymentUtils
				.prepareCustomID(ipgRequestDTO);
		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		CMIRequest cmiRequest = new CMIRequest();
		String sessionID = ipgRequestDTO.getSessionID() == null ? ""
				: ipgRequestDTO.getSessionID();
		Map<String, String> postDataMap = new HashMap();
		postDataMap.put(CMIRequest.CLIENT_ID, getMerchantId());
		postDataMap.put(CMIRequest.AMOUNT, ipgRequestDTO.getAmount());
		postDataMap.put(CMIRequest.IPG_URL, getIpgURL());
		postDataMap.put(CMIRequest.STORETYPE, getStoretype());
		postDataMap.put(CMIRequest.TRANTYPE,
				PaymentConstants.CMI_PG_TRNS_TYPE_PRE_AUTH);
		postDataMap.put(CMIRequest.HASHALGORITHM, getHashAlgorithm());
		postDataMap.put(CMIRequest.CURRENCY, getCurrency());
		postDataMap.put(CMIRequest.PHONE, ipgRequestDTO.getContactMobileNumber());
		postDataMap.put(CMIRequest.BILLTONAME, ipgRequestDTO.getContactFirstName());
		postDataMap.put(CMIRequest.EMAIL, ipgRequestDTO.getContactEmail());
		postDataMap.put(CMIRequest.BILLTOCITY, ipgRequestDTO.getContactCity());
		postDataMap.put(CMIRequest.BILLTOSTATEPROV, ipgRequestDTO.getContactState());
		postDataMap.put(CMIRequest.BILLTOCOUNTRY, ipgRequestDTO.getContactCountryName());
			
		postDataMap.put(CMIRequest.OKURL, ipgRequestDTO.getReturnUrl());
		postDataMap.put(CMIRequest.FAILURL, ipgRequestDTO.getReturnUrl());
		postDataMap.put(CMIRequest.LANG, ipgRequestDTO.getSessionLanguageCode());
		postDataMap.put(CMIRequest.RND, CMIPaymentUtils.getRandomString());
		postDataMap.put(CMIRequest.STOREKEY, getStoreKey());
		postDataMap.put(CMIRequest.CALLBACKRESPONSE, CMI_PG_HOST_HOST);
		postDataMap.put(CMIRequest.CALLBACK_URL, ipgRequestDTO.getReturnUrl()
				+ CMI_PG_RETURN_URL_PATH);
		String requestString=CMIPaymentUtils.prepareResponseString(postDataMap);
		CreditCardTransaction ccTransaction = auditTransaction(
				ipgRequestDTO.getPnr(), getMerchantId(),
				merchantTxnId,
				ipgRequestDTO.getApplicationTransactionId(),
				bundle.getString("SERVICETYPE_AUTHORIZE"),
				requestString, sessionID, getPaymentGatewayName(),
				null, false);
		customOrderId = customOrderId.append(PIPE).append(
				ccTransaction.getTransactionRefNo());
		postDataMap.put(CMIRequest.OID, customOrderId.toString());
		String plainText = CMIPaymentUtils.generatePlainText(postDataMap);
		postDataMap.put(CMIRequest.HASH,
				CMIPaymentUtils.generateHash(plainText));
		String requestData = PaymentBrokerUtils.getPostInputDataFormHTML(
				postDataMap, getIpgURL());

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(requestData);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction
				.getTransactionRefNo());
		log.info("Transaction ref number="
				+ ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setAccelAeroTransactionRef(customOrderId
				.toString());
		return ipgRequestResultsDTO;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO,
			List<CardDetailConfigDTO> configDataList) throws ModuleException {
		throw new ModuleException(
				"paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap,
			IPGResponseDTO ipgResponseDTO) throws ModuleException {
		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO
				.getPaymentBrokerRefNo());
		/*
		 * CreditCardTransaction oCreditCardTransaction =
		 * loadAuditTransactionByTnxRefNo(Integer
		 * .parseInt(ipgResponseDTO.getApplicationTransactionId()));
		 */
		String strResponseTime = CalendarUtil.getTimeDifference(
				ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(
				ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(
				oCreditCardTransaction);
		String plainText = CMIPaymentUtils.generatePlainText(receiptyMap);
		
		String responseFromCMI = (String) receiptyMap.get(RETURN_CODE);
		String authCode = (String) receiptyMap.get(AUTH_CODE);
		String recieptString = CMIPaymentUtils
				.prepareResponseString(receiptyMap);
		String actualHash=CMIPaymentUtils.generateHash(plainText);
		
		String errorCode = "";
		String cmiTransactionId = (String) receiptyMap.get(CMI_TXN);

		IPGTransactionResultDTO ipgTransactionResultDTO = new IPGTransactionResultDTO();
		
		if (RETURN_CODE_SUCCESS.equals(responseFromCMI)) {
			ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_ACCEPTED);
			ipgTransactionResultDTO.setTransactionStatus(IPGResponseDTO.STATUS_ACCEPTED);
		} else {
			ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_REJECTED);
			ipgTransactionResultDTO.setTransactionStatus(IPGResponseDTO.STATUS_REJECTED);
			errorCode = CMIPaymentUtils.buildErrorCode(errorCode,
					receiptyMap.get(RESPONSE_PARAM), responseFromCMI);
			ipgResponseDTO.setErrorCode(errorCode);
		}
		ipgResponseDTO.setApplicationTransactionId(String.valueOf(ipgResponseDTO.getTemporyPaymentId()));
		
		boolean showTransactionDetail =  true;
		ipgTransactionResultDTO.setAmount(receiptyMap.get("amount").toString());
		ipgTransactionResultDTO.setTransactionTime(CalendarUtil.formatDate(ipgResponseDTO.getRequestTimsStamp(), CalendarUtil.PATTERN5));
		ipgTransactionResultDTO.setShowTransactionDetail(showTransactionDetail);		
		ipgTransactionResultDTO.setMerchantTrackID(cmiTransactionId + "/" + oCreditCardTransaction.getTransactionReference());
		ipgResponseDTO.setIpgTransactionResultDTO(ipgTransactionResultDTO);
		
		updateAuditTransactionByTnxRefNo(
				ipgResponseDTO.getPaymentBrokerRefNo(), recieptString,
				errorCode, authCode, cmiTransactionId,
				Integer.parseInt(responseFromCMI));

		return ipgResponseDTO;
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap)
			throws ModuleException {
		throw new ModuleException(
				"paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params)
			throws ModuleException {
		throw new ModuleException(
				"paymentbroker.generic.operation.notsupported");
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(
			IPGPrepPaymentDTO params) throws ModuleException {
		throw new ModuleException(
				"paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params)
			throws ModuleException {
		throw new ModuleException(
				"paymentbroker.generic.operation.notsupported");
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap)
			throws ModuleException {
		throw new ModuleException(
				"paymentbroker.generic.operation.notsupported");
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap,
			IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO)
			throws ModuleException {
		throw new ModuleException(
				"paymentbroker.generic.operation.notsupported");

	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

}
