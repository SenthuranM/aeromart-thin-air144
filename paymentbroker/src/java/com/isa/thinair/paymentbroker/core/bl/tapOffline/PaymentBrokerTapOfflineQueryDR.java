package com.isa.thinair.paymentbroker.core.bl.tapOffline;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;

public class PaymentBrokerTapOfflineQueryDR extends PaymentBrokerTemplate implements PaymentQueryDR {

	private static Log log = LogFactory.getLog(PaymentBrokerTapOfflineQueryDR.class);

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	private String deviceLicenseID;

	private String hash;

	@Override
	public ServiceResponce query(CreditCardTransaction oCreditCardTransaction, IPGConfigsDTO ipgConfigsDTO, QUERY_CALLER caller)
			throws ModuleException {

		log.debug("[PaymentBrokerTapOfflineQueryDR::query()] Begin");
		int tnxResultCode;
		String status;
		boolean verified;
		String errorSpecification = null;
		String merchantTxnReference = oCreditCardTransaction.getTempReferenceNum();
		String queryParamString = "RefNo:" + oCreditCardTransaction.getTransactionId() + ",MerchantId:" + getMerchantId();

		CreditCardTransaction ccTransaction = auditTransaction(oCreditCardTransaction.getTransactionReference(), getMerchantId(),
				merchantTxnReference, new Integer(oCreditCardTransaction.getTemporyPaymentId()),
				bundle.getString("SERVICETYPE_QUERY"), queryParamString, "", getPaymentGatewayName(),
				oCreditCardTransaction.getTransactionId(), false);

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		log.debug("Query Request Params : " + queryParamString);

		Properties props = PaymentBrokerManager.getProperties(this.ipgIdentificationParamsDTO);

		TapOfflineResponse response = null;

		Map<String, String> statusRequestMap = new HashMap<String, String>();
		statusRequestMap.put(TapOfflineResponse.TAP_REF_ID, oCreditCardTransaction.getTransactionId());
		statusRequestMap.put(TapOfflineRequest.ORDER_ID_FOR_STATUS, oCreditCardTransaction.getTempReferenceNum());
		statusRequestMap.put(TapOfflineRequest.MERCHANT_ID, getMerchantId());
		statusRequestMap.put(TapOfflineRequest.DEVICE_LICENSE_ID, getDeviceLicenseID());
		statusRequestMap.put(TapOfflineRequest.HASH, getHash());

		try {
			response = TapOfflinePaymentUtils.getCheckStatusResponse(statusRequestMap, getIpgURL(), isUseProxy(), getProxyHost(),
					getProxyPort());

		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		String statusMessage = response.getResMsg();
		// TODO remove pending option after completing the test
		if (statusMessage.equals(TapOfflinePaymentUtils.SUCCESS)) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			log.debug("[PaymentBrokerTapOfflineQueryDR::query() : invoice is paid]  invoice ID :"
					+ response.getPayRefID());
			tnxResultCode = 1;
			errorSpecification = " " + caller.toString();
			verified = true;
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, PaymentConstants.SERVICE_RESPONSE_QUERYDR);
			sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE, oCreditCardTransaction.getTransactionId());
		}

		else {
			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
			errorSpecification = "Status:" + status + " Error code:" + response.getResCode() + " Description:"
					+ response.getResMsg() + " Caller:" + caller.toString();
			verified = false;
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, errorSpecification);
		}
		Map<String, String> postResponseDataMap = new LinkedHashMap<String, String>();

		postResponseDataMap.put(response.RES_CODE, response.getResCode());
		postResponseDataMap.put(response.RES_MESSAGE, response.getResMsg());
		postResponseDataMap.put(response.PAY_MODE, response.getPaymode());
		postResponseDataMap.put(response.PAY_REF_ID, response.getPayRefID());

		String responseMessage = PaymentBrokerUtils.getRequestDataAsString(postResponseDataMap);

		updateAuditTransactionByTnxRefNo(ccTransaction.getTransactionRefNo(), responseMessage, errorSpecification,
				oCreditCardTransaction.getAidCccompnay(), oCreditCardTransaction.getTransactionId(), tnxResultCode);

		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, oCreditCardTransaction.getTransactionId());
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, response.getResMsg());
		sr.setSuccess(verified);
		log.debug("[PaymentBrokerTapOfflineQueryDR::query() : Query Status :  " + status + "] ");
		log.debug("[PaymentBrokerTapOfflineQueryDR::query()] End");

		return sr;

	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		PaymentBrokerManager.validateIPGIdentificationParams(this.ipgIdentificationParamsDTO);
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	public String getDeviceLicenseID() {
		return deviceLicenseID;
	}

	public void setDeviceLicenseID(String deviceLicenseID) {
		this.deviceLicenseID = deviceLicenseID;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

}
