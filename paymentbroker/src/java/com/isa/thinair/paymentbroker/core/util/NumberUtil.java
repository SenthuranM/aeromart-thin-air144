package com.isa.thinair.paymentbroker.core.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class NumberUtil {

	private static final String TWO_DECIMAL_FROMAT = "0.00";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BigDecimal payment = new BigDecimal("1115");
		double doublePayment = payment.doubleValue();
		String s = formatAsDecimal(doublePayment);
		System.out.println(s);

	}

	public static String formatAsDecimal(double amount) {
		DecimalFormat fmt = new DecimalFormat(TWO_DECIMAL_FROMAT);
		return fmt.format(amount);
	}

	public static String formatAsDecimal(String amount) {
		return formatAsDecimal((new BigDecimal(amount)).doubleValue());
	}

	public static String formatAsDecimal(double amount, int noDecimalPlaces) {
		String strFmt = getFormat(noDecimalPlaces);
		DecimalFormat fmt = new DecimalFormat(strFmt);
		return fmt.format(amount);
	}

	public static String formatAsDecimal(String amount, int noDecimalPlaces) {
		return formatAsDecimal((new BigDecimal(amount)).doubleValue(), noDecimalPlaces);
	}

	private static String getFormat(int noDecimalPlaces) {
		String strFmt = "0.";

		for (int i = 0; i < noDecimalPlaces; i++) {
			strFmt += "0";
		}

		return strFmt;
	}

	public static double multiply(double d, long l) {
		BigDecimal bd1 = BigDecimal.valueOf(d);
		BigDecimal bd2 = BigDecimal.valueOf(l);
		return bd1.multiply(bd2).doubleValue();
	}

	public static double multiply(double d1, double d2) {
		BigDecimal bd1 = BigDecimal.valueOf(d1);
		BigDecimal bd2 = BigDecimal.valueOf(d2);
		return bd1.multiply(bd2).doubleValue();
	}

	public static double divide(double d, long l) {
		BigDecimal bd1 = BigDecimal.valueOf(d);
		BigDecimal bd2 = BigDecimal.valueOf(l);
		return bd1.divide(bd2, 3, RoundingMode.UP).doubleValue();
	}

	public static double divide(double d1, double d2) {
		BigDecimal bd1 = BigDecimal.valueOf(d1);
		BigDecimal bd2 = BigDecimal.valueOf(d2);
		return bd1.divide(bd2, 3, RoundingMode.UP).doubleValue();
	}
}
