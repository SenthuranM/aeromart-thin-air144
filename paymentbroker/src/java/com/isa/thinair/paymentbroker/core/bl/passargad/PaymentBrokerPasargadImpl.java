package com.isa.thinair.paymentbroker.core.bl.passargad;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CalendarTool;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class PaymentBrokerPasargadImpl extends PaymentBrokerTemplate implements PaymentBroker {

	private static Log log = LogFactory.getLog(PaymentBrokerPasargadImpl.class);

	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.paymentgateway.notsupported");
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("") &&
				!PlatformUtiltiies.nullHandler(creditCardTransaction.getErrorSpecification()).equals(IPGResponseDTO.STATUS_REJECTED)) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("paymentbroker.generic.operation.paymentgateway.notsupported");
	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.paymentgateway.notsupported");
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		String finalAmount = ipgRequestDTO.getAmount().split("\\.")[0];
		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		CalendarTool cal = new CalendarTool();
		String iranianDate = cal.getIranianDate();

		String dateFormat1 = "yyyy/MM/dd HH:mm:ss";
		SimpleDateFormat format = new SimpleDateFormat(dateFormat1);
		String gregorianDate = format.format(CalendarUtil.getCurrentZuluDateTime());
		String currentTime = gregorianDate.split(" ")[1];
		String dateNtime = iranianDate + " " + currentTime;

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		postDataMap.put(PasargadRequest.MERCHANT_CODE, getMerchantId());
		postDataMap.put(PasargadRequest.TERMINAL_CODE, getTerminalCode());
		// postDataMap.put(PasargadRequest.INVOICE_NUMBER, ipgRequestDTO.getPnr() + merchantTxnId);
		postDataMap.put(PasargadRequest.INVOICE_NUMBER, Integer.toString(ipgRequestDTO.getApplicationTransactionId()));
		postDataMap.put(PasargadRequest.INVOICE_DATE, dateNtime);
		postDataMap.put(PasargadRequest.AMOUNT, finalAmount);
		postDataMap.put(PasargadRequest.REDIRECT_URL, ipgRequestDTO.getReturnUrl());
		postDataMap.put(PasargadRequest.IPG_URL, getIpgURL());
		postDataMap.put(PasargadRequest.ACTION, getActionId());
		postDataMap.put(PasargadRequest.TIMESTAMP, gregorianDate);
		postDataMap.put(PasargadRequest.KEYSTORE_LOCATION, getKeystoreName());
		postDataMap.put(PasargadRequest.REDIRECTION_MECHANISM, PaymentConstants.MODE_OF_REDIRECTION.FORM.getValue());
		postDataMap.put(PasargadRequest.BROKER_TYPE, getBrokerType());

		String requestDataForm = PasargadPaymentUtil.generateFormRequest(postDataMap);
		String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);
		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();

		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				strRequestParams + "," + sessionID, "", getPaymentGatewayName(), null, false);

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(requestDataForm);
		ipgRequestResultsDTO.setAccelAeroTransactionRef(Integer.toString(ipgRequestDTO.getApplicationTransactionId()));
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setPostDataMap(postDataMap);

		return ipgRequestResultsDTO;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGResponseDTO getReponseData(Map fields, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		log.debug("[PaymentBrokerPasargadImpl::getReponseData()] Begin ");

		boolean errorExists = false;
		String errorCode = "";
		int tnxResultCode = 0;
		String errorSpecification = "";
		String transactionId = null;
		String status;
		int cardType = 5; // default card type
		String responseString = createReceiptMapString(fields);

		CreditCardTransaction pasargadCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO
				.getPaymentBrokerRefNo());

		// Calculate response time
		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time
		pasargadCreditCardTransaction.setComments(strResponseTime);
		pasargadCreditCardTransaction.setResponseTime(timeDiffInMillis);
		pasargadCreditCardTransaction.setTransactionId(fields.get("tref").toString());
		pasargadCreditCardTransaction.setAidCccompnay(fields.get("iN").toString());
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(pasargadCreditCardTransaction);

		PasargadResponse pasargadResp = new PasargadResponse();
		pasargadResp.setReponse(fields);
		log.debug("[PaymentBrokerPasargadImpl::getReponseData()] Mid Response -" + pasargadResp);

		ServiceResponce response;
		if (pasargadCreditCardTransaction.getTransactionId().equalsIgnoreCase(pasargadResp.getTransactionReferenceID())) {

			errorExists = false;
			PaymentQueryDR paymentQueryDR = getPaymentQueryDR();
			response = paymentQueryDR.query(pasargadCreditCardTransaction, PasargadPaymentUtil.getIPGConfigs(getMerchantId()),
					PaymentBrokerInternalConstants.QUERY_CALLER.VERIFY);
			if (response.isSuccess()) {
				errorExists = false;
			} else {
				errorSpecification = "" + response.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE);
				errorExists = true;
			}

		} else {
			errorExists = true;
		}
		if (!errorExists) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			tnxResultCode = 1;
			transactionId = fields.get("tref").toString();
		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
		}
		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setApplicationTransactionId(fields.get("iN").toString());
		ipgResponseDTO.setCardType(cardType);
		updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), responseString, errorSpecification,
				transactionId, transactionId, tnxResultCode);
		log.debug("[PaymentBrokerPasargadImpl::getReponseData()] End -");

		return ipgResponseDTO;
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection creditCardPayment, boolean refundFlag) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		// TODO Auto-generated method stub

	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}
	
	private String createReceiptMapString(Map receiptMap) {
		StringBuffer resBuff = new StringBuffer();

		List fieldNames = new ArrayList(receiptMap.keySet());
		Iterator itr = fieldNames.iterator();

		while (itr.hasNext()) {
			String fieldName = (String) itr.next();
			String fieldValue = (String) receiptMap.get(fieldName);
			resBuff.append(fieldName + " : " + fieldValue);
			if (itr.hasNext()) {
				resBuff.append(", ");
			}
		}
		return resBuff.toString();
	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

}
