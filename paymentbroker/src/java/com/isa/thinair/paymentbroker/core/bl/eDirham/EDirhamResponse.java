package com.isa.thinair.paymentbroker.core.bl.eDirham;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.paymentbroker.core.bl.eDirham.EDirhamPaymentUtils.EDIRHAM_OPERATION;

public class EDirhamResponse {

	public static final String STATUS = "Response.Status";

	public static final String CONFIRMATIONID = "Response.ConfirmationID";

	public static final String STATUSMESSAGE = "Response.StatusMessage";

	public static final String MERCHANTID = "Response.MerchantID";

	public static final String BANKID = "Response.BankID";

	public static final String RETRIEVALREFNUMBER = "Response.RetrievalRefNumber";

	public static final String UNIQUETRANSACTIONID = "Response.UniqueTransactionID";

	public static final String AMOUNT = "Response.Amount";

	public static final String CURRENCY = "Response.Currency";

	public static final String PAYMENTMETHODTYPE = "Response.PaymentMethodType";

	public static final String TERMINALID = "Response.TerminalID";

	public static final String EDIRHAMFEES = "Response.EDirhamFees";

	public static final String COLLECTIONCENTERFEES = "Response.CollectionCenterFees";

	public static final String TRANSACTIONAMOUNT = "Response.TransactionAmount";

	public static final String SECUREHASH = "Response.SecureHash";

	public static final String TRANSACTIONRESPONSEDATE = "Response.TransactionResponseDate";

	public static final String ESERVICEAMOUNT_1 = "Response.EServiceAmount_1";

	public static final String ESERVICETOTALAMOUNT_1 = "Response.EServiceTotalAmount_1";

	public static final String ESERVICEFEE_1 = "Response.EServiceFee_1";

	public static final String ESERVICEMAINCODESUBCODE_1 = "Response.EServiceMainCodeSubCode_1";

	public static final String ESERVICEENGLISHDESCRIPTION_1 = "Response.EServiceEnglishDescription_1";

	public static final String ESERVICEARABICDESCRIPTION_1 = "Response.EServiceArabicDescription_1";

	public static final String ESERVICEMINISTRYENGLISHNAME_1 = "Response.EServiceMinistryEnglishName_1";

	public static final String ESERVICEMINISTRYARABICNAME_1 = "Response.EServiceMinistryArabicName_1";

	public static final String MERCHANTMODULESESSIONID = "Response.MerchantModuleSessionID";

	public static final String PUN = "Response.PUN";

	public static final String RECEIPTID = "Response.ReceiptID";

	public static final String ESERVICEDATA = "Response.EServiceData";

	public static final String ACTION = "Response.Action";

	public static final String ORIGINALTRANSACTIONSTATUS = "Response.OriginalTransactionStatus";

	public static final String ORIGINALTRANSACTIONSTATUSMESSAGE = "Response.OriginalTransactionStatusMessage";

	public static final String EXTRACTSTATUS = "Response.ExtractStatus";

	public static final String NO_VALUE = "";

	private String status;
	private String confirmationID;
	private String statusMessage;
	private String transactionResponseDate;
	private String secureHash;
	private String paymentMethodType;
	private String amount;
	private String totalAmount;
	private String fees;
	private String currency;
	private String merchantModuleSessionId;
	private String pun;
	private String merchantId;
	private String bankId;
	private String terminalId;
	private String receiptId;
	private String collectionCenterFees;
	private String eDirhamFees;
	private String transactionAmount;
	private String eServiceData;
	private String mainCodeSubCode;
	private String englishDescription;
	private String ministryEnglishName;
	private String retrievalRefNumber;
	private String action;
	private String originalTransactionStatus;
	private String originalTransactionStatusMessage;
	private String extractStatus;

	Map response = null;

	private boolean offlineMode;

	public EDirhamResponse() {
	}

	public void setResponse(Map response, String operation) {
		this.response = response;
		this.setStatus(null2unknown((String) response.get(EDirhamResponse.STATUS)));
		this.setStatusMessage(null2unknown((String) response.get(EDirhamResponse.STATUSMESSAGE)));
		this.setTransactionResponseDate(null2unknown((String) response.get(EDirhamResponse.TRANSACTIONRESPONSEDATE)));
		this.setCurrency(null2unknown((String) response.get(EDirhamResponse.CURRENCY)));
		this.setBankId(null2unknown((String) response.get(EDirhamResponse.BANKID)));
		this.setTerminalId(null2unknown((String) response.get(EDirhamResponse.TERMINALID)));
		this.seteDirhamFees(null2unknown((String) response.get(EDirhamResponse.EDIRHAMFEES)));
		this.setCollectionCenterFees(null2unknown((String) response.get(EDirhamResponse.COLLECTIONCENTERFEES)));
		this.setTransactionAmount(null2unknown((String) response.get(EDirhamResponse.TRANSACTIONAMOUNT)));
		this.setPaymentMethodType(null2unknown((String) response.get(EDirhamResponse.PAYMENTMETHODTYPE)));
		this.setSecureHash(null2unknown((String) response.get(EDirhamResponse.SECUREHASH)));
		this.setMerchantId(null2unknown((String) response.get(EDirhamResponse.MERCHANTID)));

		if (operation.equals(EDIRHAM_OPERATION.PAY_WEB.getOperation())) {
			this.setConfirmationID(null2unknown((String) response.get(EDirhamResponse.CONFIRMATIONID)));
			this.setAmount(null2unknown((String) response.get(EDirhamResponse.AMOUNT)));
			this.setMerchantModuleSessionId(null2unknown((String) response.get(EDirhamResponse.MERCHANTMODULESESSIONID)));
			this.setPun(null2unknown((String) response.get(EDirhamResponse.PUN)));
			this.setReceiptId(null2unknown((String) response.get(EDirhamResponse.RECEIPTID)));
			this.seteServiceData(null2unknown((String) response.get(EDirhamResponse.ESERVICEDATA)));
		} else if (operation.equals(EDIRHAM_OPERATION.AUTO_UPDATE.getOperation())) {
			this.setAction(null2unknown((String) response.get(EDirhamResponse.ACTION)));
			this.setOriginalTransactionStatus(null2unknown((String) response.get(EDirhamResponse.ORIGINALTRANSACTIONSTATUS)));
			String originalTxnStatusMsg = null2unknown((String) response.get(EDirhamResponse.ORIGINALTRANSACTIONSTATUSMESSAGE));
			originalTxnStatusMsg = originalTxnStatusMsg.replaceAll(" ", "+");
			this.setOriginalTransactionStatusMessage(originalTxnStatusMsg);
			this.setConfirmationID(null2unknown((String) response.get(EDirhamResponse.CONFIRMATIONID)));
			this.setMerchantModuleSessionId(null2unknown((String) response.get(EDirhamResponse.MERCHANTMODULESESSIONID)));
			this.setExtractStatus(null2unknown((String) response.get(EDirhamResponse.EXTRACTSTATUS)));
			this.setPun(null2unknown((String) response.get(EDirhamResponse.PUN)));
			this.seteServiceData(null2unknown((String) response.get(EDirhamResponse.ESERVICEDATA)));
		} else if (operation.equals(EDIRHAM_OPERATION.REFUND)) {
			this.setRetrievalRefNumber(null2unknown((String) response.get(EDirhamResponse.RETRIEVALREFNUMBER)));
			this.setPun(null2unknown((String) response.get(EDirhamResponse.UNIQUETRANSACTIONID)));
			this.setMerchantId(null2unknown((String) response.get(EDirhamResponse.MERCHANTID)));
		}

	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getConfirmationID() {
		return confirmationID;
	}

	public void setConfirmationID(String confirmationID) {
		this.confirmationID = confirmationID;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getTransactionResponseDate() {
		return transactionResponseDate;
	}

	public void setTransactionResponseDate(String transactionResponseDate) {
		this.transactionResponseDate = transactionResponseDate;
	}

	public String getSecureHash() {
		return secureHash;
	}

	public void setSecureHash(String secureHash) {
		this.secureHash = secureHash;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}


	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getMerchantModuleSessionId() {
		return merchantModuleSessionId;
	}

	public void setMerchantModuleSessionId(String merchantModuleSessionId) {
		this.merchantModuleSessionId = merchantModuleSessionId;
	}

	public String getPun() {
		return pun;
	}

	public void setPun(String pun) {
		this.pun = pun;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getReceiptId() {
		return receiptId;
	}

	public void setReceiptId(String receiptId) {
		this.receiptId = receiptId;
	}

	public String getCollectionCenterFees() {
		return collectionCenterFees;
	}

	public void setCollectionCenterFees(String collectionCenterFees) {
		this.collectionCenterFees = collectionCenterFees;
	}

	public String geteDirhamFees() {
		return eDirhamFees;
	}

	public void seteDirhamFees(String eDirhamFees) {
		this.eDirhamFees = eDirhamFees;
	}

	public String getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public String geteServiceData() {
		return eServiceData;
	}

	public void seteServiceData(String eServiceData) {
		this.eServiceData = eServiceData;
	}

	public String getPaymentMethodType() {
		return paymentMethodType;
	}

	public void setPaymentMethodType(String paymentMethodType) {
		this.paymentMethodType = paymentMethodType;
	}

	public boolean isOfflineMode() {
		return offlineMode;
	}

	public void setOfflineMode(boolean offlineMode) {
		this.offlineMode = offlineMode;
	}

	public String getFees() {
		return fees;
	}

	public void setFees(String fees) {
		this.fees = fees;
	}

	public String getMainCodeSubCode() {
		return mainCodeSubCode;
	}

	public void setMainCodeSubCode(String mainCodeSubCode) {
		this.mainCodeSubCode = mainCodeSubCode;
	}

	public String getEnglishDescription() {
		return englishDescription;
	}

	public void setEnglishDescription(String englishDescription) {
		this.englishDescription = englishDescription;
	}

	public String getMinistryEnglishName() {
		return ministryEnglishName;
	}

	public void setMinistryEnglishName(String ministryEnglishName) {
		this.ministryEnglishName = ministryEnglishName;
	}

	public String getRetrievalRefNumber() {
		return retrievalRefNumber;
	}

	public void setRetrievalRefNumber(String retrievalRefNumber) {
		this.retrievalRefNumber = retrievalRefNumber;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getOriginalTransactionStatus() {
		return originalTransactionStatus;
	}

	public void setOriginalTransactionStatus(String originalTransactionStatus) {
		this.originalTransactionStatus = originalTransactionStatus;
	}

	public String getOriginalTransactionStatusMessage() {
		return originalTransactionStatusMessage;
	}

	public void setOriginalTransactionStatusMessage(String originalTransactionStatusMessage) {
		this.originalTransactionStatusMessage = originalTransactionStatusMessage;
	}

	public String getExtractStatus() {
		return extractStatus;
	}

	public void setExtractStatus(String extractStatus) {
		this.extractStatus = extractStatus;
	}

	private static String null2unknown(String in) {
		if (in == null || in.length() == 0) {
			return NO_VALUE;
		} else {
			return in;
		}
	}

	public String toString() {
		StringBuffer resBuff = new StringBuffer();

		// create a list
		List fieldNames = new ArrayList(response.keySet());
		Iterator itr = fieldNames.iterator();

		// move through the list and create a series of URL key/value pairs
		while (itr.hasNext()) {
			String fieldName = (String) itr.next();
			String fieldValue = (String) response.get(fieldName);
			resBuff.append(fieldName + " : " + fieldValue);
			// add a '&' to the end if we have more fields coming.
			if (itr.hasNext()) {
				resBuff.append(", ");
			}
		}
		return resBuff.toString();
	}

}
