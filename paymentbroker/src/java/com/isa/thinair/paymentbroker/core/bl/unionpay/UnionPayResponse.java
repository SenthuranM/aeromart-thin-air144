package com.isa.thinair.paymentbroker.core.bl.unionpay;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class UnionPayResponse {

	private static Log log = LogFactory.getLog(UnionPayResponse.class);

	public static final String NO_VALUE = "";

	// Response as a Map
	Map response = null;

	private String orderId;

	private String encoding;

	private String cetificateID;

	private String signature;

	private String signatureMethod;

	private String txnType;

	private String txnSubType;

	private String productType;

	private String accessType;

	private String acqInsCode;

	private String txnTime;

	private String txnAmount;

	private String txnCurrency;

	private String queryId;

	private String responseCode;

	private String responseMsg;

	private String settleDate;

	private String settleCode;

	private String settleAmnt;

	private String traceNo;

	private String origQueryId;

	public void setReponse(Map reponse) {
		this.response = reponse;
		encoding = null2unknown((String) reponse.get(UnionPayConstants.ENCODING));
		cetificateID = null2unknown((String) reponse.get(UnionPayConstants.CERT_ID));
		signature = null2unknown((String) reponse.get(UnionPayConstants.SIGNATURE));
		signatureMethod = null2unknown((String) reponse.get(UnionPayConstants.SIGN_METHOD));
		txnType = null2unknown((String) reponse.get(UnionPayConstants.TXN_TYPE));
		txnSubType = null2unknown((String) reponse.get(UnionPayConstants.TXN_SUB_TYPE));
		productType = null2unknown((String) reponse.get(UnionPayConstants.PRODUCT_TYPE));
		accessType = null2unknown((String) reponse.get(UnionPayConstants.ACCESS_TYPE));
		acqInsCode = null2unknown((String) reponse.get(UnionPayConstants.ACQ_INST_CODE));
		orderId = null2unknown((String) reponse.get(UnionPayConstants.ORDER_ID));
		txnTime = null2unknown((String) reponse.get(UnionPayConstants.TXN_TIME));
		txnAmount = null2unknown((String) reponse.get(UnionPayConstants.TXN_AMOUNT));
		txnCurrency = null2unknown((String) reponse.get(UnionPayConstants.TXN_CURRENCY));
		queryId = null2unknown((String) reponse.get(UnionPayConstants.TXN_QUERY_NUMBER));
		origQueryId = null2unknown((String) reponse.get(UnionPayConstants.TXN_QUERY_NUMBER));
		responseCode = null2unknown((String) reponse.get(UnionPayConstants.RS_CODE));
		responseMsg = null2unknown((String) reponse.get(UnionPayConstants.RS_MSG));
		settleDate = null2unknown((String) reponse.get(UnionPayConstants.SETTLE_DATE));
		settleCode = null2unknown((String) reponse.get(UnionPayConstants.SETTLE_CURR_CODE));
		settleAmnt = null2unknown((String) reponse.get(UnionPayConstants.SETTLE_AMNT));
		traceNo = null2unknown((String) reponse.get(UnionPayConstants.TRACE_NO));
	}

	/*
	 * This method takes a data String and returns a predefined value if empty If data Sting is null, returns string
	 * "No Value Returned", else returns input
	 * 
	 * @param in String containing the data String
	 * 
	 * @return String containing the output String
	 */
	private static String null2unknown(String in) {
		if (in == null || in.length() == 0) {
			return NO_VALUE;
		} else {
			return in;
		}
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Map getResponse() {
		return response;
	}

	public void setResponse(Map response) {
		this.response = response;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public String getCetificateID() {
		return cetificateID;
	}

	public void setCetificateID(String cetificateID) {
		this.cetificateID = cetificateID;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getSignatureMethod() {
		return signatureMethod;
	}

	public void setSignatureMethod(String signatureMethod) {
		this.signatureMethod = signatureMethod;
	}

	public String getTxnType() {
		return txnType;
	}

	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

	public String getTxnSubType() {
		return txnSubType;
	}

	public void setTxnSubType(String txnSubType) {
		this.txnSubType = txnSubType;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getAccessType() {
		return accessType;
	}

	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}

	public String getAcqInsCode() {
		return acqInsCode;
	}

	public void setAcqInsCode(String acqInsCode) {
		this.acqInsCode = acqInsCode;
	}

	public String getTxnTime() {
		return txnTime;
	}

	public void setTxnTime(String txnTime) {
		this.txnTime = txnTime;
	}

	public String getTxnAmount() {
		return txnAmount;
	}

	public void setTxnAmount(String txnAmount) {
		this.txnAmount = txnAmount;
	}

	public String getTxnCurrency() {
		return txnCurrency;
	}

	public void setTxnCurrency(String txnCurrency) {
		this.txnCurrency = txnCurrency;
	}

	public String getQueryId() {
		return queryId;
	}

	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMsg() {
		return responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}

	public String getSettleDate() {
		return settleDate;
	}

	public void setSettleDate(String settleDate) {
		this.settleDate = settleDate;
	}

	public String getSettleCode() {
		return settleCode;
	}

	public void setSettleCode(String settleCode) {
		this.settleCode = settleCode;
	}

	public String getSettleAmnt() {
		return settleAmnt;
	}

	public void setSettleAmnt(String settleAmnt) {
		this.settleAmnt = settleAmnt;
	}

	public String getTraceNo() {
		return traceNo;
	}

	public void setTraceNo(String traceNo) {
		this.traceNo = traceNo;
	}

	public String getOrigQueryId() {
		return origQueryId;
	}

	public void setOrigQueryId(String origQueryId) {
		this.origQueryId = origQueryId;
	}

}
