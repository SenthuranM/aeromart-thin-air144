package com.isa.thinair.paymentbroker.core.bl.cmi.fatourati;

import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.CODERETOUR;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.DATE_FORMAT;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.MSGRETOUR;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.REFFATOURATI;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.SUCCESS;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.SUCCESS_VALUE;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.utils.ConfirmReservationUtil;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PanierClientType;
import com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PanierClientTypeEtat;
import com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PayByRefFatGenReq;
import com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PayByRefFatGenReqAction;
import com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PayByRefFatGenRes;
import com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.RefFatouratiWSProxy;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.cmi.fatourati.util.FatouratiUtil;
import com.isa.thinair.paymentbroker.core.bl.payFort.PayFortPaymentUtils;
import com.isa.thinair.paymentbroker.core.bl.tapOffline.TapOfflinePaymentUtils;
import com.isa.thinair.paymentbroker.core.bl.tapOffline.TapOfflineResponse;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class CmiFatouratiImpl extends PaymentBrokerTemplate implements PaymentBroker {

	private static Log log = LogFactory.getLog(CmiFatouratiImpl.class);
	public static final String CARD_TYPE_GENERIC = "GC";
	private String creancierId;
	private String creanceId;
	private String refClient;
	private String nonProxyHosts;
	private String fatouratiUrl;
	private String macControl;
	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();
	private String defaultDescription;

	public String getDefaultDescription() {
		return defaultDescription;
	}

	public void setDefaultDescription(String defaultDescription) {
		this.defaultDescription = defaultDescription;
	}

	public String getMacControl() {
		return macControl;
	}

	public void setMacControl(String macControl) {
		this.macControl = macControl;
	}

	public String getCreanceId() {
		return creanceId;
	}

	public void setCreanceId(String creanceId) {
		this.creanceId = creanceId;
	}

	public String getCreancierId() {
		return creancierId;
	}

	public void setCreancierId(String creancierId) {
		this.creancierId = creancierId;
	}

	public String getRefClient() {
		return refClient;
	}

	public void setRefClient(String refClient) {
		this.refClient = refClient;
	}

	public String getNonProxyHosts() {
		return nonProxyHosts;
	}

	public void setNonProxyHosts(String nonProxyHosts) {
		this.nonProxyHosts = nonProxyHosts;
	}

	public String getFatouratiUrl() {
		return fatouratiUrl;
	}

	public void setFatouratiUrl(String fatouratiUrl) {
		this.fatouratiUrl = fatouratiUrl;
	}

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {

		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO()
				.loadTransaction(creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");

	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		CreditCardTransaction ccTransaction = checkModify(ipgRequestDTO);
		String merchantTxnId = null;
		boolean modifyflow = false;
		boolean readyToaddNew = true;
		if (ccTransaction != null && ccTransaction.getTransactionId() != null) {
			log.info("Modification flow,need to remove the fatourati reference and a new one with updated amount");
			modifyflow = true;
			merchantTxnId = ccTransaction.getTempReferenceNum();
		}

		PayByRefFatGenReq request = prepareRequest(ipgRequestDTO);

		if (modifyflow) {

			readyToaddNew = removeFatouratiReference(request, ccTransaction);

		} else {
			request.setAction(PayByRefFatGenReqAction.value1);
			merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
					ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);
			String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();
			ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
					ipgRequestDTO.getApplicationTransactionId(), bundle.getString("SERVICETYPE_AUTHORIZE"), "" + "," + sessionID,
					"", getPaymentGatewayName(), null, false);

		}

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();

		if (readyToaddNew) {
			RefFatouratiWSProxy fatourityService = new RefFatouratiWSProxy(getIpgURL());

			PayByRefFatGenRes response = new PayByRefFatGenRes();

			try {

				request.setMac(FatouratiUtil.getHash(request, getMacControl()));
				if (getNonProxyHosts() != null) {
					System.setProperty("http.nonProxyHosts", getNonProxyHosts());
				}
				if (isUseProxy()) {
					System.getProperties().put("socksProxyHost", getProxyHost());
					System.getProperties().put("socksProxyPort", getProxyPort());
				}
				response = fatourityService.genRefFatourati(request);
			} catch (Exception e) {
				log.info("Getting sample value");
				throw new ModuleException("Error while API invocation" + e);
			}
			if (SUCCESS_VALUE.equals(response.getCodeRetour()) && SUCCESS.equalsIgnoreCase(response.getMsgRetour())) {
				String fatouratiRef = response.getPanierClientRes(0).getRefFatourati();
				Map<String, String> postResponseDataMap = new LinkedHashMap<>();
				postResponseDataMap.put(REFFATOURATI, fatouratiRef);
				postResponseDataMap.put(CODERETOUR, response.getCodeRetour());
				postResponseDataMap.put(MSGRETOUR, response.getMsgRetour());
				ccTransaction.setResponseText(PaymentBrokerUtils.getRequestDataAsString(postResponseDataMap));
				ccTransaction.setAidCccompnay(fatouratiRef);
				ccTransaction.setTransactionId(fatouratiRef);

				// For display purpose
				Map<String, String> postDataMap = new HashMap<>();
				postDataMap.put(REFFATOURATI, fatouratiRef);
				String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);
				ipgRequestResultsDTO.setPostDataMap(postDataMap);
				if (log.isDebugEnabled()) {
					log.debug("PaymentBrokerTapOfflineImpl PG Request Data : " + strRequestParams);
				}
				ipgRequestResultsDTO.setRequestData(strRequestParams);
				ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());

			} else {
				ccTransaction.setErrorSpecification(response.getCodeRetour());
				ipgRequestResultsDTO.setRequestData("");
				ipgRequestResultsDTO.setPaymentBrokerRefNo(0);
				ipgRequestResultsDTO.setErrorCode(response.getMsgRetour());
			}
			PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(ccTransaction);

			ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);
		}
		return ipgRequestResultsDTO;
	}

	private boolean removeFatouratiReference(PayByRefFatGenReq request, CreditCardTransaction ccTransaction)
			throws ModuleException {
		boolean readyToaddNew = false;
		request.setAction(PayByRefFatGenReqAction.value5);
		// generating MAC value
		RefFatouratiWSProxy fatourityService = new RefFatouratiWSProxy(getIpgURL());
		try {
			request.setMac(FatouratiUtil.getHash(request, getMacControl()));
		} catch (ModuleException e1) {

			throw new ModuleException("Error in secret key ");
		}
		if (getNonProxyHosts() != null) {
			System.setProperty("http.nonProxyHosts", getNonProxyHosts());
		}
		if (isUseProxy()) {
			System.getProperties().put("socksProxyHost", getProxyHost());
			System.getProperties().put("socksProxyPort", getProxyPort());
		}
		request.setRefFatourati(ccTransaction.getTransactionId());
		PayByRefFatGenRes response = new PayByRefFatGenRes();
		try {
			response = fatourityService.genRefFatourati(request);
		} catch (RemoteException e) {
			throw new ModuleException("Error while API invocation" + e);
		}
		if (SUCCESS_VALUE.equals(response.getCodeRetour()) && SUCCESS.equalsIgnoreCase(response.getMsgRetour())) {
			readyToaddNew = true;
		}
		request.setRefFatourati(null);
		return readyToaddNew;
	}

	private PayByRefFatGenReq prepareRequest(IPGRequestDTO ipgRequestDTO) {
		String refClientId = Integer.toString(ipgRequestDTO.getApplicationTransactionId());
		SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
		PayByRefFatGenReq request = new PayByRefFatGenReq();
		request.setCreancierId(getCreancierId());
		request.setDateServeurCreancier(format.format(new Date()));
		request.setMontantTotalArticles(new BigDecimal(ipgRequestDTO.getAmount()));
		request.setNbrTotalArticles(1);

		PanierClientType panierClient = new PanierClientType();
		panierClient.setCreanceId(getCreanceId());
		panierClient.setDescription(getDefaultDescription());
		panierClient.setIdArticle(Integer.toString(ipgRequestDTO.getApplicationTransactionId()));
		panierClient.setMailClient(ipgRequestDTO.getContactEmail());
		panierClient.setMontant(new BigDecimal(ipgRequestDTO.getAmount()));
		panierClient.setRefClient(refClientId);

		request.setPanierClient(new PanierClientType[] { panierClient });
		return request;
	}

	private CreditCardTransaction checkModify(IPGRequestDTO ipgRequestDTO) {

		return PaymentBrokerUtils.getPaymentBrokerDAO().loadOfflineLatestTransactionByReference(ipgRequestDTO.getPnr());

	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {

		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {

		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection creditCardPayment, boolean refundFlag) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {

		log.info("[PaymentBroker-cmi OfflineImpl::handleDeferredResponse()] Begin");

		if (AppSysParamsUtil.enableServerToServerMessages()
				&& AppSysParamsUtil.isConfirmOnholdReservationByServerToServerMessages()) {

			int temporyPaymentId = ipgResponseDTO.getTemporyPaymentId();
			CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTmpPayID(temporyPaymentId);
			ReservationBD reservationBD = ReservationModuleUtils.getReservationBD();
			boolean isReservationConfirmationSuccess = false;
			String pnr = oCreditCardTransaction.getTransactionReference();
			LCCClientReservation commonReservation = null;
			Reservation reservation = null;
			String status = "";

			/* Save Card Transaction response Details */
			Date requestTime = reservationBD.getPaymentRequestTime(oCreditCardTransaction.getTemporyPaymentId());
			Date now = Calendar.getInstance().getTime();
			String strResponseTime = CalendarUtil.getTimeDifference(requestTime, now);
			long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(requestTime, now);

			oCreditCardTransaction.setComments(strResponseTime);
			oCreditCardTransaction.setResponseTime(timeDiffInMillis);
			oCreditCardTransaction.setTransactionResultCode(1);
			oCreditCardTransaction.setDeferredResponseText(PaymentBrokerUtils.getRequestDataAsString(receiptyMap));

			PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

			IPGQueryDTO ipgQueryDTO = reservationBD.getTemporyPaymentInfo(oCreditCardTransaction.getTransactionRefNo());

			if (ipgQueryDTO != null) {
				/* Update with payment response details */

				LCCClientPnrModesDTO modes = new LCCClientPnrModesDTO();
				modes.setPnr(ipgQueryDTO.getPnr());
				modes.setRecordAudit(false);

				ipgQueryDTO.setPaymentType(getPaymentType(getStandardCardType(CARD_TYPE_GENERIC)));

				// for dry interline validation fix
				IPGIdentificationParamsDTO defIPGIdentificationParamsDTO = ipgQueryDTO.getIpgIdentificationParamsDTO();
				if (defIPGIdentificationParamsDTO.getIpgId() == null) {
					defIPGIdentificationParamsDTO.setIpgId(ipgResponseDTO.getPaymentBrokerRefNo());
					ipgQueryDTO.setIpgIdentificationParamsDTO(defIPGIdentificationParamsDTO);
				}

				/** NO RESERVATION OR CANCELED RESERVATION */
				if (!isReservationExist(ipgQueryDTO)
						|| ReservationInternalConstants.ReservationStatus.CANCEL.equals(ipgQueryDTO.getPnrStatus())) {

					/**
					 * When confirming a reservation made from another carrier reservation does not exists in own
					 * reservation table , therefore we need to load the reservation through LCC and clarify reservation
					 * exists or not
					 **/
					String moduleCode = receiptyMap.get(TapOfflinePaymentUtils.ORDER_DONE_BY);
					LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
					status = commonReservation.getStatus();

					if (status.equals("") || ReservationInternalConstants.ReservationStatus.CANCEL.equals(status)) {

						log.info("[handleDeferredResponse()] Refunding Payment -> No Reservation has been created :"
								+ isReservationConfirmationSuccess);
						ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_REJECTED);
						receiptyMap.put("status", TapOfflinePaymentUtils.ERROR);

					} else {
						if (commonReservation != null) {
							ipgQueryDTO.setPnr(commonReservation.getPNR());
							ipgQueryDTO.setGroupPnr(commonReservation.isGroupPNR() ? commonReservation.getPNR() : null);
							ipgQueryDTO.setPnrStatus(commonReservation.getStatus());
						}
					}
				}

				/** ONHOLD RESERVATION */
				if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(ipgQueryDTO.getPnrStatus())) {

					if (reservation == null && commonReservation == null) {
						try {
							reservation = PaymentBrokerModuleUtil.getReservationBD().getReservation(modes, null);
							status = reservation.getStatus();
						} catch (ModuleException e) {
							log.error(" ((o)) CommonsDataAccessException::getReservation " + e.getCause());
						}
					}

					if (!status.equals("") && ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(status)) {

						if (isSuccessFromIPG(oCreditCardTransaction, receiptyMap)) {
							isReservationConfirmationSuccess = ConfirmReservationUtil
									.isReservationConfirmationSuccessForOfflinePayments(ipgQueryDTO);
						}

						if (!isReservationConfirmationSuccess) {
							log.debug("[handleDeferredResponse()] Refunding Payment -> Onhold confirmation failed :"
									+ isReservationConfirmationSuccess);
							ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_REJECTED);
							receiptyMap.put("status", PayFortPaymentUtils.ERROR);
							throw new ModuleException("Onhold confirmation failed:" + pnr);
						} else {
							receiptyMap.put("status", PayFortPaymentUtils.SUCCESS);
							log.info("Reservation confirmation successful:" + ipgResponseDTO.getPaymentBrokerRefNo());
							ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_ACCEPTED);
						}

					}
				}

				/** ALREADY CONFIRMED RESERVATION */
				if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(ipgQueryDTO.getPnrStatus())) {

					if (reservation == null && commonReservation == null) {
						try {
							reservation = PaymentBrokerModuleUtil.getReservationBD().getReservation(modes, null);
							status = reservation.getStatus();
						} catch (ModuleException e) {
							log.error(" ((o)) CommonsDataAccessException::getReservation " + e.getCause());
						}
					}

					TempPaymentTnx tempPaymentTnx = null;
					tempPaymentTnx = ReservationModuleUtils.getReservationBD().loadTempPayment(temporyPaymentId);

					if (!status.equals("") && ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(status)
							&& "RS".equals(tempPaymentTnx.getStatus())) {
						receiptyMap.put("status", PaymentConstants.SUCCESS);
						ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_ACCEPTED);
						log.debug("[handleDeferredResponse()] Confirm Payment -> Reservation already confirmed :" + pnr);

					} else {
						receiptyMap.put(PaymentConstants.MSGRETOUR, TapOfflinePaymentUtils.ERROR);
						ipgResponseDTO.setStatus(TapOfflinePaymentUtils.STATUS_REJECTED);
						log.debug("[handleDeferredResponse()] Confirm Payment -> confirm by another payment gateway :" + pnr);
						throw new ModuleException("confirm by another payment gateway :" + pnr);
					}
				}

			} else {
				receiptyMap.put(TapOfflineResponse.RES_MESSAGE, TapOfflinePaymentUtils.ERROR);
				ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_REJECTED);
				log.error("PaymentBrokerTapOfflineImpl temp transaction not found:" + ipgResponseDTO.getPaymentBrokerRefNo());
				throw new ModuleException("Duplicate Receipt....");
			}

		} else {
			log.error("error check Response for transation:" + ipgResponseDTO.getPaymentBrokerRefNo());
			ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_REJECTED);
			receiptyMap.put(TapOfflineResponse.RES_MESSAGE, TapOfflinePaymentUtils.ERROR);
			throw new ModuleException("Server to Server calls are disabled..");
		}

		log.info("[PaymentBroker ::handleDeferredResponse()] End");

	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	protected boolean isReservationExist(IPGQueryDTO ipgQueryDTO) {
		boolean isExist = false;
		if (ipgQueryDTO != null) {
			if (ipgQueryDTO.getPnr() != null && ipgQueryDTO.getPnr().trim().length() != 0) {
				isExist = true;
			}
		}
		return isExist;
	}

	private boolean isSuccessFromIPG(CreditCardTransaction oCreditCardTransaction, Map<String, String> postDataMap)
			throws ModuleException {

		if (!oCreditCardTransaction.getTransactionReference().equals(postDataMap.get("pnr"))) {
			return false;
		} else if (!oCreditCardTransaction.getTransactionId().equals(postDataMap.get("orderNumber"))) {
			return false;
		} else {

			return true;
		}

	}

	protected int getStandardCardType(String card) {
		// [1-MASTER, 2-VISA, 3-AMEX, 4-DINNERS, 5-GENERIC, 6-OFFLINE]
		int mapType = 5; // default card type
		if (card != null && !card.equals("") && mapCardType.get(card) != null) {
			String mapTypeStr = (String) mapCardType.get(card);
			mapType = Integer.parseInt(mapTypeStr);
		}
		return mapType;
	}

	protected PaymentType getPaymentType(int cardID) {

		PaymentType paymentType = PaymentType.CARD_GENERIC;
		switch (cardID) {
		case 1:
			paymentType = PaymentType.CARD_MASTER;
			break;
		case 2:
			paymentType = PaymentType.CARD_VISA;
			break;
		case 3:
			paymentType = PaymentType.CARD_AMEX;
			break;
		case 4:
			paymentType = PaymentType.CARD_DINERS;
			break;
		case 5:
			paymentType = PaymentType.CARD_GENERIC;
			break;
		case 6:
			paymentType = PaymentType.CARD_CMI;
			break;
		}
		return paymentType;
	}

	/**
	 * 
	 * @param fatId
	 * @return
	 * @throws ModuleException
	 */

	private boolean isPaid(String fatId) throws ModuleException {
		RefFatouratiWSProxy proxy = new RefFatouratiWSProxy();
		SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
		PayByRefFatGenReq request = new PayByRefFatGenReq();
		request.setAction(PayByRefFatGenReqAction.value7);
		request.setCreancierId(getCreancierId());
		request.setDateServeurCreancier(format.format(new Date()));
		request.setMac(FatouratiUtil.getHash(request, getMacControl()));
		request.setMontantTotalArticles(new BigDecimal("10.0"));
		request.setRefFatourati(fatId);
		PayByRefFatGenRes res;
		try {
			res = proxy.genRefFatourati(request);
			if (res.getPanierClientRes() != null && res.getPanierClientRes().length > 0
					&& res.getPanierClientRes()[0].getEtat() == PanierClientTypeEtat.value2) {
				return true;
			}
		} catch (RemoteException e) {
		}

		return false;
	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
}
