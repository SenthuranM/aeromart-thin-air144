package com.isa.thinair.paymentbroker.core.bl.saman;

import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.saman.SamanRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.saman.SamanResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.wsclient.api.service.WSClientBD;

/**
 * 
 * @author Janaka Padukka
 * 
 */
public class PaymentBrokerSamanQueryDR extends PaymentBrokerTemplate implements PaymentQueryDR {

	private static Log log = LogFactory.getLog(PaymentBrokerSamanQueryDR.class);

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	@Override
	public ServiceResponce query(CreditCardTransaction oCreditCardTransaction, IPGConfigsDTO ipgConfigsDTO,
			QUERY_CALLER caller) throws ModuleException {

		log.debug("[PaymentBrokerSamanQueryDR::query()] Begin");

		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status = null;
		String errorSpecification = null;

		String merchantTxnReference = oCreditCardTransaction.getTempReferenceNum();

		String queryParamString = "RefNo:" + oCreditCardTransaction.getTransactionId() + ",MerchantId:" + getMerchantId();

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		CreditCardTransaction ccTransaction = auditTransaction(oCreditCardTransaction.getTransactionReference(), getMerchantId(),
				merchantTxnReference, new Integer(oCreditCardTransaction.getTemporyPaymentId()),
				bundle.getString("SERVICETYPE_QUERY"), queryParamString, "", getPaymentGatewayName(), null, false);
		log.debug("Query Request Params : " + queryParamString);

		SamanRequestDTO verifyRequest = new SamanRequestDTO();
		verifyRequest.setMerchantId(ipgConfigsDTO.getMerchantID());
		verifyRequest.setReferenceNo(oCreditCardTransaction.getTransactionId());

		WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
		if (verifyRequest.getReferenceNo() != null) {
			SamanResponseDTO verifyResponse = ebiWebervices.verifyTransaction(verifyRequest);

			log.debug("Query Response : " + verifyResponse.getVerifiactionResult());

			// positive pay amount means verification is correct, negative implies
			// its an error code
			if (verifyResponse.getVerifiactionResult() > 0) {
				status = IPGResponseDTO.STATUS_ACCEPTED;
				errorCode = "";
				transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
				tnxResultCode = 1;
				errorSpecification = " " + caller.toString();
				sr.setSuccess(true);
				sr.addResponceParam(PaymentConstants.AMOUNT, verifyResponse.getVerifiactionResult());
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, PaymentConstants.SERVICE_RESPONSE_QUERYDR);
				sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE, oCreditCardTransaction.getTransactionId());
			} else {
				status = IPGResponseDTO.STATUS_REJECTED;
				String[] error = SamanPaymentUtils.getMappedUnifiedError(verifyResponse.getVerifiactionResult());
				errorCode = error[0];
				transactionMsg = error[1];
				tnxResultCode = 0;
				errorSpecification = "Status:" + status + " Error code:" + errorCode + " Error Desc:" + transactionMsg
						+ " Caller:" + caller.toString();
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
			}

			updateAuditTransactionByTnxRefNo(ccTransaction.getTransactionRefNo(), verifyResponse.getVerifiactionResult()
					.toString(), errorSpecification, oCreditCardTransaction.getTransactionId(),
					oCreditCardTransaction.getTransactionId(), tnxResultCode);

			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, oCreditCardTransaction.getTransactionId());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			log.error("[PaymentBrokerSamanQueryDR::query()] Failed. Reference No is null");
		}
		log.debug("[PaymentBrokerSamanQueryDR::query()] End");

		return sr;
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		PaymentBrokerManager.validateIPGIdentificationParams(this.ipgIdentificationParamsDTO);
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

}
