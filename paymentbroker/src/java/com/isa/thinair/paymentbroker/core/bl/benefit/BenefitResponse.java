package com.isa.thinair.paymentbroker.core.bl.benefit;


import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;

import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.bl.knet.KnetResponse.RESPONSETYPE;

public class BenefitResponse {
	
	private static final Log  log =  LogFactory.getLog(BenefitResponse.class);
	
	public static final String PAYMENT_ID = "paymentid";
	
	public static final String  ERROR = "Error";
	
	public static final String  UDF1 = "udf1";
	
	public static final String  UDF2 = "udf2";
	
	public static final String  UDF3 = "udf3";
	
	public static final String  UDF4 = "udf4";
	
	public static final String  UDF5 = "udf5";
	
	public static final String  ERROR_TEXT = "ErrorText";
	
	public static final String  POST_DATE = "postdate";
	
	public static final String  TRANSACTION_ID = "tranid";
	
	public static final String  AUTHORIZATION_CODE = "auth";
	
	public static final String  TRACK_ID = "trackid";
	
	public static final String  REFERENCE_NO = "ref";
	
	public static final String  RESULT = "result";
	
	public static final String RESPONSE_CODE = "responsecode";
	
	public static final String RESPONSE_TYPE = "responseType";
	
	public enum RESULTSTATUS {
		CAPTURED("CAPTURED"), NOT_CAPTURED("NOT CAPTURED"), DEFAULT("DEFAULT");
		
		private String resultCode;
		
		private RESULTSTATUS(String result) {
			resultCode = result;
		}		
		public String getCode() {
			return resultCode;
		}
	}
	
	public enum RESPONCECODE{
		RESCODE00("00"), RESCODE05("05"), RESCODE33("33"), RESCODE51("51"), RESCODE54("54"),
	    RESCODE55("55"), RESCODE61("61"), RESCODE91("91");
	    
	    private String responceCode;
		
		private RESPONCECODE(String result) {
			responceCode = result;
		}		
		public String getCode() {
			return responceCode;
		}
	}
	
		
	private String paymentId;
	
	private String error;
	
	private String udf1;
	
	private String udf2;
	
	private String udf3;
	
	private String udf4;
	
	private String udf5;
	
	private String errorText;
	
	private String postDate;
	
	private String tranid;
	
	private String auth;
	
	private String trackid;
	
	private String ref;	
	
	private RESULTSTATUS result;
	
	private String sessionID;	
	
	private RESPONCECODE responsecode;
	
	private RESPONSETYPE responseType;
	
	Map<String, String> response = null;

	public Map<String, String> getResponse() {
		if (response == null)
			this.response = new HashMap<String, String>();			
		return response;
	}

	public void setResponse(Map<String, String> response) {		
		this.response = response;
		paymentId = null2Empty(response.get(PAYMENT_ID));
		error = null2Empty(response.get(ERROR));
		udf1 = null2Empty(response.get(UDF1));
		udf2 = null2Empty(response.get(UDF2));
		udf3 = null2Empty(response.get(UDF3));
		udf4 = null2Empty(response.get(UDF4));
		udf5 = null2Empty(response.get(UDF5));
		errorText = null2Empty(response.get(ERROR_TEXT));
		postDate = null2Empty(response.get(POST_DATE));
		tranid = null2Empty(response.get(TRANSACTION_ID));
		auth = null2Empty( response.get(AUTHORIZATION_CODE));
		trackid = null2Empty(response.get(TRACK_ID));
		ref = null2Empty( response.get(REFERENCE_NO));	
		result = getResultCode(null2Empty(response.get(RESULT)));		
		responsecode = getResponsecodeToVerify(null2Empty(response.get(RESPONSE_CODE)));
		setResponseType(getResponseTypeToVerify(null2Empty(response.get(RESPONSE_TYPE))));
	
		sessionID = null2Empty((String) response.get(PaymentConstants.IPG_SESSION_ID));
	}	
	
	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getUdf1() {
		return udf1;
	}

	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}

	public String getUdf2() {
		return udf2;
	}

	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}

	public String getUdf3() {
		return udf3;
	}

	public void setUdf3(String udf3) {
		this.udf3 = udf3;
	}

	public String getUdf4() {
		return udf4;
	}

	public void setUdf4(String udf4) {
		this.udf4 = udf4;
	}

	public String getUdf5() {
		return udf5;
	}

	public void setUdf5(String udf5) {
		this.udf5 = udf5;
	}

	public String getErrorText() {
		return errorText;
	}

	public void setErrorText(String errorText) {
		this.errorText = errorText;
	}

	public String getPostDate() {
		return postDate;
	}

	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}

	public String getTranid() {
		return tranid;
	}

	public void setTranid(String tranid) {
		this.tranid = tranid;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public String getTrackid() {
		return trackid;
	}

	public void setTrackid(String trackid) {
		this.trackid = trackid;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}	
	
	
	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}	

	public RESULTSTATUS getResult() {
		return result;
	}

	public void setResult(RESULTSTATUS result) {
		this.result = result;
	}	
	

	public RESPONCECODE getResponsecode() {
		return responsecode;
	}

	public void setResponseCode(RESPONCECODE responsecode) {
		this.responsecode = responsecode;
	}
	
	public RESPONSETYPE getResponseType() {
		return responseType;
	}

	public void setResponseType(RESPONSETYPE responseType) {
		this.responseType = responseType;
	}

	private String null2Empty(String str) {
		if (str == null || str.trim().isEmpty()) 
			return "";
		else
			return str;
	}	
	
	
	public String toString() {
		StringBuffer responseBuff = new StringBuffer();
		
		for (Map.Entry<String, String> entry : response.entrySet()) {
			responseBuff.append(entry.getKey()).append(":").append(entry.getValue()).append("&");		    
		}

		// Response is too large to save in db field.
		if (responseBuff.toString().length() < 2500) {
			return responseBuff.toString();
		} else {
			if (log.isDebugEnabled())
				log.debug("[Benefit response] Response before trimming -" + responseBuff.toString());
			return responseBuff.toString().substring(0, 2500);
		}
	}
	
	private RESULTSTATUS getResultCode(String result) {
		RESULTSTATUS resultCode = RESULTSTATUS.DEFAULT;
		if (RESULTSTATUS.CAPTURED.getCode().equals(result)) {
			resultCode = RESULTSTATUS.CAPTURED;
		} else if (RESULTSTATUS.NOT_CAPTURED.getCode().equals(result)) {
			resultCode = RESULTSTATUS.NOT_CAPTURED;
		} 	
		return resultCode;
	}
	
	private RESPONCECODE getResponsecodeToVerify(String responsecode) {
		RESPONCECODE resCode = RESPONCECODE.RESCODE00 ;

		if (RESPONCECODE.RESCODE05.getCode().equals(responsecode)) {
			resCode = RESPONCECODE.RESCODE05;
		} else if (RESPONCECODE.RESCODE33.getCode().equals(responsecode)) {
			resCode = RESPONCECODE.RESCODE33;
		} else if (RESPONCECODE.RESCODE51.getCode().equals(responsecode)) {
			resCode = RESPONCECODE.RESCODE51;
		} else if (RESPONCECODE.RESCODE54.getCode().equals(responsecode)) {
			resCode = RESPONCECODE.RESCODE54;
		} else if (RESPONCECODE.RESCODE54.getCode().equals(responsecode)) {
			resCode = RESPONCECODE.RESCODE54;
		} else if (RESPONCECODE.RESCODE55.getCode().equals(responsecode)) {
			resCode = RESPONCECODE.RESCODE55;
		} else if (RESPONCECODE.RESCODE61.getCode().equals(responsecode)) {
			resCode = RESPONCECODE.RESCODE55;
		} else if (RESPONCECODE.RESCODE91.getCode().equals(responsecode)) {
			resCode = RESPONCECODE.RESCODE91;
		}
		return resCode;
	}
	private RESPONSETYPE getResponseTypeToVerify(String responseType) {
		RESPONSETYPE resType = RESPONSETYPE.DIRECT;
		if (RESPONSETYPE.INTERMEDIATE.toString().equalsIgnoreCase(responseType)) {
			resType = RESPONSETYPE.INTERMEDIATE;
		}
		return resType;
	}
}
