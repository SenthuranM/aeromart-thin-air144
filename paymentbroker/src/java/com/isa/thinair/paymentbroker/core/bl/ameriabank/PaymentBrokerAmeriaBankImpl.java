package com.isa.thinair.paymentbroker.core.bl.ameriabank;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.utils.ConfirmReservationUtil;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankCommonRequest;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankCommonResponse;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankPaymentIdRequest;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankSessionInfo;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.wsclient.api.service.WSClientBD;

public class PaymentBrokerAmeriaBankImpl extends PaymentBrokerAmeriaBankTemplate {

	private static Log log = LogFactory.getLog(PaymentBrokerAmeriaBankImpl.class);
	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();
	protected static final int CARD_TYPE_GENERIC = 5;
	protected static final String ERROR = "unknown error (likely at AccelAero end)";

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		log.debug("[PaymentBrokerAmeriaBankECommerceImpl::getRequestData()] Begin ");

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		AmeriaBankSessionInfo ameriaBankSessionInfo = new AmeriaBankSessionInfo();

		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();

		AmeriaBankPaymentIdRequest ameriaBankPayRequest = new AmeriaBankPaymentIdRequest();

		ameriaBankPayRequest.setClientID(getMerchantId());
		ameriaBankPayRequest.setUserName(getUser());
		ameriaBankPayRequest.setUserPassword(getPassword());
		ameriaBankPayRequest.setDescription(getDescription());

		String backURL = ipgRequestDTO.getReturnUrl();
		// if (!ipgRequestDTO.isServiceAppFlow()) {
		// backURL = AppParamUtil.getSecureIBEUrl() + "ameriaBankPaymentResponseHandler.action";
		// }
		ameriaBankPayRequest.setBackURL(backURL);
		ameriaBankPayRequest.setPaymentAmount(new BigDecimal(ipgRequestDTO.getAmount()));
		ameriaBankPayRequest.setCurrency(AmeriaBankPaymentUtil.getCurrencyCode(ipgRequestDTO.getIpgIdentificationParamsDTO()
				.getPaymentCurrencyCode()));
		ameriaBankPayRequest.setOrderId(ipgRequestDTO.getApplicationTransactionId());

		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				ameriaBankPayRequest.toString(), "", getPaymentGatewayName(), null, false);

		int paymentBrokerRefNo = ccTransaction.getTransactionRefNo();
		String opaque = String.valueOf(paymentBrokerRefNo);
		ameriaBankPayRequest.setOpaque(opaque);

		WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
		AmeriaBankCommonResponse ameriaBankPaymentIdResponse = new AmeriaBankCommonResponse();
		ameriaBankPaymentIdResponse = ebiWebervices.getAmeriaBankPaymentId(ameriaBankPayRequest);

		if (!StringUtil.isNullOrEmpty(ameriaBankPaymentIdResponse.getPaymentId())
				&& AmeriaBankPaymentUtil.PAYMENT_ID_RESP_SUCCESS_CODE.contentEquals(ameriaBankPaymentIdResponse.getRespCode())
				&& (AmeriaBankPaymentUtil.PAYMENT_ID_RESP_SUCCESS_MESSAGE).contentEquals(ameriaBankPaymentIdResponse
						.getRespMessage())) {

			postDataMap.put("clientid", getMerchantId());
			// could set dummy value (only required for iframe approach)
			postDataMap.put("clienturl", ipgRequestDTO.getReturnUrl());
			postDataMap.put("lang", getLocale());
			postDataMap.put("paymentid", ameriaBankPaymentIdResponse.getPaymentId());

			String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

			String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();

			String requestDataForm = PaymentBrokerUtils.getInputDataGETFormHTML(postDataMap, ipgURL);

			if (log.isDebugEnabled()) {
				log.debug("AmeriaBank PG Request Data : " + strRequestParams + ", sessionID : " + sessionID);
			}

			ipgRequestResultsDTO.setRequestData(requestDataForm);
			ipgRequestResultsDTO.setPaymentBrokerRefNo(paymentBrokerRefNo);
			ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);
			ipgRequestResultsDTO.setPostDataMap(postDataMap);

			// for Payment fields response validation
			ameriaBankSessionInfo.setPaymentAmount(new BigDecimal(ipgRequestDTO.getAmount()));
			ameriaBankSessionInfo.setOpaque(opaque);
			ameriaBankSessionInfo.setPaymentId(ameriaBankPaymentIdResponse.getPaymentId());
			ameriaBankSessionInfo.setCurrency(AmeriaBankPaymentUtil.getCurrencyCode(ipgRequestDTO.getIpgIdentificationParamsDTO()
					.getPaymentCurrencyCode()));

			log.debug("[PaymentBrokerAmeriaBankECommerceImpl::getRequestData()] End ");
			// gateway payment ID will be updated if the initial response is successful
			updateAuditTransactionByTnxRefNo(paymentBrokerRefNo, ameriaBankPayRequest.toString(),
					ameriaBankPaymentIdResponse.getPaymentId());

		}

		else {
			String respCode = ameriaBankPaymentIdResponse.getRespCode();
			String respMessage = ameriaBankPaymentIdResponse.getRespMessage();
			AmeriaBankPaymentUtil.getErrorDescription(respCode, respMessage);
			ameriaBankSessionInfo.setError(respMessage);
			log.error("Ameria Bank initial request failed: " + respMessage);
		}

		ipgRequestResultsDTO.setAmeriaBankSessionInfo(ameriaBankSessionInfo);

		return ipgRequestResultsDTO;
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {

		log.debug("[PaymentBrokerAmeriaBankImpl::getReponseData()] Begin ");

		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());

		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time and comments (these fields be there if response has received)
		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

		boolean isValidReceipt = AmeriaBankPaymentUtil.validateReceiptMap(receiptMap, ipgResponseDTO.getAmeriaBankSessionInfo()
				.getPaymentId(), oCreditCardTransaction.getTemporyPaymentId());

		if (!isValidReceipt) {
			log.error("Ameria Bank Payment receipt failed." + (String) receiptMap.get("respcode") + " "
					+ (String) receiptMap.get("description"));
			ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_REJECTED);
			ipgResponseDTO.setErrorCode((String) receiptMap.get("respcode"));
			return ipgResponseDTO;
		}

		PaymentQueryDR queryDR = getPaymentQueryDR();

		IPGConfigsDTO ameriaBankIPGDto = AmeriaBankPaymentUtil.getIPGConfigs(getMerchantId(), getUser(), getPassword());
		ameriaBankIPGDto.setOpaque(ipgResponseDTO.getAmeriaBankSessionInfo().getOpaque());
		ameriaBankIPGDto.setPaymentCurrency(ipgResponseDTO.getAmeriaBankSessionInfo().getCurrency());

		ServiceResponce serviceResponce = queryDR.query(oCreditCardTransaction, ameriaBankIPGDto,
				PaymentBrokerInternalConstants.QUERY_CALLER.VERIFY);

		String responseAuthCode = (String) serviceResponce
				.getResponseParam(AmeriaBankPaymentUtil.AMERIA_PAYMENT_FIELDS_AUTH_CODE);

		String responseOrderId = (String) serviceResponce.getResponseParam(AmeriaBankPaymentUtil.AMERIA_PAYMENT_FIELDS_ORDER_ID);

		String status = (String) serviceResponce.getResponseParam(AmeriaBankPaymentUtil.AMERIA_PAYMENT_FIELDS_STATUS);

		String errorCode = (String) serviceResponce.getResponseParam(AmeriaBankPaymentUtil.AMERIA_PAYMENT_FIELDS_ERROR_CODE);

		String errorDescription = (String) serviceResponce
				.getResponseParam(AmeriaBankPaymentUtil.AMERIA_PAYMENT_FIELDS_ERROR_DESC);

		int tnxResultCode = (Integer) serviceResponce
				.getResponseParam(AmeriaBankPaymentUtil.AMERIA_PAYMENT_FIELDS_TNX_RESULTS_CODE);

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setApplicationTransactionId(oCreditCardTransaction.getTempReferenceNum());
		ipgResponseDTO.setAuthorizationCode(responseAuthCode);
		ipgResponseDTO.setCardType(CARD_TYPE_GENERIC);

		if (log.isDebugEnabled()) {
			log.debug("[PaymentBrokerAmeriaBankECommerceImpl::getReponseData()] - Staus-" + status + " Error Code-" + errorCode
					+ " Error Spec-" + errorDescription);
		}

		updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), status, errorDescription, responseAuthCode,
				responseOrderId, tnxResultCode);

		log.debug("[PaymentBrokerAmeriaBankImpl::getReponseData()] End");

		return ipgResponseDTO;

	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		log.debug("[PaymentBrokerCCAvenueTemplate::refund()] Begin");
		int tnxResultCode;
		String errorCode;
		String errorSpecification;
		Integer newpaymentBrokerRefNo;

		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
		String merchTnxRef = ccTransaction.getTempReferenceNum();

		String refundAmountStr = creditCardPayment.getAmount();
		DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);

		if (creditCardPayment.getPreviousCCPayment() != null && getRefundEnabled() != null && getRefundEnabled().equals("true")) {

			if (log.isDebugEnabled()) {
				log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
				log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
				log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
				log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
				log.debug("CC Old pay Formatted sAmount : " + refundAmountStr);
				log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());
			}

			WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
			AmeriaBankCommonResponse ameriaBankRefundResponse;

			PaymentQueryDR queryDR = getPaymentQueryDR();

			IPGConfigsDTO ameriaBankIPGDto = AmeriaBankPaymentUtil.getIPGConfigs(getMerchantId(), getUser(), getPassword());

			ServiceResponce serviceResponce = queryDR.query(ccTransaction, ameriaBankIPGDto,
					PaymentBrokerInternalConstants.QUERY_CALLER.VERIFY);

			if (serviceResponce.isSuccess()) {
				// AmeriaBank refund request
				AmeriaBankCommonRequest ameriaRefundRequest = new AmeriaBankCommonRequest();
				ameriaRefundRequest.setClientID(getMerchantId());
				ameriaRefundRequest.setOrderId(ccTransaction.getTemporyPaymentId());
				ameriaRefundRequest.setUserName(getUser());
				ameriaRefundRequest.setPaymentAmount(new BigDecimal(refundAmountStr));
				ameriaRefundRequest.setUserPassword(getPassword());

				String updatedMerchTnxRef = merchTnxRef + PaymentConstants.MODE_OF_SERVICE.REFUND;

				CreditCardTransaction ccNewTransaction = auditTransaction(pnr, getMerchantId(), updatedMerchTnxRef,
						creditCardPayment.getTemporyPaymentId(), bundle.getString("SERVICETYPE_REFUND"),
						ameriaRefundRequest.toString(), "", getPaymentGatewayName(), null, false);

				newpaymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());

				ameriaBankRefundResponse = ebiWebervices.refundAmeriaBankPayment(ameriaRefundRequest);

			} else {
				defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						"paymentbroker.ameria.payment.fields.response.invalid");
				return defaultServiceResponse;
			}

			if (ameriaBankRefundResponse == null) {
				defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						"paymentbroker.ameria.payment.refund.response.invalid");
				return defaultServiceResponse;
			}

			// got refund response
			Integer paymentBrokerRefNo = new Integer(ccTransaction.getTransactionRefNo());
			log.info("[PaymentBrokerAmeriaBankImpl::refund()] Refund Status : " + ameriaBankRefundResponse.toString());

			if (ameriaBankRefundResponse.getRespCode().contentEquals(AmeriaBankPaymentUtil.COMMON_RESP_SUCCESS_CODE)) {

				errorCode = "";

				tnxResultCode = 1;
				errorSpecification = "";
				defaultServiceResponse.setSuccess(true);
				defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);

			} else {
				String status = IPGResponseDTO.STATUS_REJECTED;
				errorCode = ameriaBankRefundResponse.getRespCode();
				String transactionMsg;
				if (!StringUtil.isNullOrEmpty(ameriaBankRefundResponse.getRespMessage())) {
					transactionMsg = ameriaBankRefundResponse.getRespMessage();
				} else {

					errorCode = ameriaBankRefundResponse.getRespCode();
					transactionMsg = ameriaBankRefundResponse.getRespMessage();
					AmeriaBankPaymentUtil.getErrorDescription(errorCode, transactionMsg);
				}

				tnxResultCode = 0;
				errorSpecification = "Status:" + status + " Error code:" + errorCode + " Error Desc:" + transactionMsg;

				defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);

			}

			updateAuditTransactionByTnxRefNo(newpaymentBrokerRefNo.intValue(), ameriaBankRefundResponse.toString(),
					errorSpecification, ameriaBankRefundResponse.getRespCode(), ameriaBankRefundResponse.getOpaque(),
					tnxResultCode);

			defaultServiceResponse.setResponseCode(String.valueOf(paymentBrokerRefNo));
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, null);
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);

		} else {
			log.info("[PaymentBrokerAmeriaBank::refund()] Cannot Refund");
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);
		}

		log.debug("[PaymentBrokerAmeriaBank::refund()] End");

		return defaultServiceResponse;
	}

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		return this.reverse(creditCardPayment, pnr, appIndicator, tnxMode);
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {
		log.info("[PaymentBrokerAmeriaBank::schedular-refund()] Begin");

		Iterator itColIPGQueryDTO = colIPGQueryDTO.iterator();

		Collection<Integer> oResultIF = new ArrayList<Integer>();
		Collection<Integer> oResultIP = new ArrayList<Integer>();
		Collection<Integer> oResultII = new ArrayList<Integer>();
		Collection<Integer> oResultIS = new ArrayList<Integer>();

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		IPGQueryDTO ipgQueryDTO;
		CreditCardTransaction oCreditCardTransaction;
		PaymentQueryDR query;
		PreviousCreditCardPayment oPrevCCPayment;
		CreditCardPayment oCCPayment;
		Date currentTime = Calendar.getInstance().getTime();
		Calendar queryValidTime = Calendar.getInstance();

		while (itColIPGQueryDTO.hasNext()) {
			ipgQueryDTO = (IPGQueryDTO) itColIPGQueryDTO.next();

			oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgQueryDTO.getPaymentBrokerRefNo());
			query = getPaymentQueryDR();

			try {
				ServiceResponce serviceResponce = query.query(oCreditCardTransaction,
						AmeriaBankPaymentUtil.getIPGConfigs(getMerchantId(), getUser(), getPassword()),
						PaymentBrokerInternalConstants.QUERY_CALLER.SCHEDULER);

				queryValidTime.setTime(ipgQueryDTO.getTimestamp());
				queryValidTime.add(Calendar.MINUTE, AppSysParamsUtil.getTimeForPaymentStatusUpdate());

				// If successful payment remains at Bank
				if (serviceResponce.isSuccess()) {
					boolean isResConfirmationSuccess = ConfirmReservationUtil.isReservationConfirmationSuccess(ipgQueryDTO);

					if (isResConfirmationSuccess) {
						if (log.isDebugEnabled()) {
							log.debug("Status moving to RS:" + oCreditCardTransaction.getTemporyPaymentId());
						}

					} else {
						if (refundFlag) {
							// Do refund
							String txnNo = PlatformUtiltiies.nullHandler(serviceResponce
									.getResponseParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE));

							oPrevCCPayment = new PreviousCreditCardPayment();
							oPrevCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
							oPrevCCPayment.setPgSpecificTxnNumber(txnNo);

							oCCPayment = new CreditCardPayment();
							oCCPayment.setPreviousCCPayment(oPrevCCPayment);
							oCCPayment.setAmount(ipgQueryDTO.getAmount().toString());
							oCCPayment.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
							oCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
							oCCPayment.setIpgIdentificationParamsDTO(ipgQueryDTO.getIpgIdentificationParamsDTO());
							oCCPayment.setPnr(oCreditCardTransaction.getTransactionReference());
							oCCPayment.setTemporyPaymentId(oCreditCardTransaction.getTemporyPaymentId());

							ServiceResponce refundServiceResponse = refundWithoutQuery(oCCPayment, oCCPayment.getPnr(),
									oCCPayment.getAppIndicator(), oCCPayment.getTnxMode());

							if (refundServiceResponse.isSuccess()) {
								// Change State to 'IS'
								oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
								if (log.isDebugEnabled()) {
									log.debug("Status moving to IS:" + oCreditCardTransaction.getTemporyPaymentId());
								}
							} else {
								// Change State to 'IF'
								oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
								if (log.isDebugEnabled()) {
									log.debug("Status moving to IF:" + oCreditCardTransaction.getTemporyPaymentId());
								}
							}
						} else {
							// Change State to 'IP'
							oResultIP.add(oCreditCardTransaction.getTemporyPaymentId());
							if (log.isDebugEnabled()) {
								log.debug("Status moving to IP:" + oCreditCardTransaction.getTemporyPaymentId());
							}
						}
					}
				}
				// No payment detail at bank
				else {
					// No Payments exist
					// Update status to 'II'
					if (currentTime.getTime() > queryValidTime.getTimeInMillis()) {
						oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
						log.info("Status moing to II:" + oCreditCardTransaction.getTemporyPaymentId());
					} else {
						// Can not move payment status. with in status change for 3D secure payments
						log.info("Status not moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
					}
				}
			} catch (ModuleException e) {
				log.info("Scheduler::ResolvePayment FAILED!!! " + e.getMessage(), e);
			}
		}

		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF, oResultIF);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_II, oResultII);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IP, oResultIP);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS, oResultIS);

		sr.setSuccess(true);

		log.info("[PaymentBrokerAmeriaBank::schedular-refund()] End");

		return sr;
	}

	private ServiceResponce refundWithoutQuery(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		log.debug("[PaymentBrokerCCAvenueTemplate::refund()] Begin");
		int tnxResultCode;
		String errorCode;
		String errorSpecification;
		Integer newpaymentBrokerRefNo;

		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
		String merchTnxRef = ccTransaction.getTempReferenceNum();

		String refundAmountStr = creditCardPayment.getAmount();
		DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);

		if (creditCardPayment.getPreviousCCPayment() != null && getRefundEnabled() != null && getRefundEnabled().equals("true")) {

			if (log.isDebugEnabled()) {
				log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
				log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
				log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
				log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
				log.debug("CC Old pay Formatted sAmount : " + refundAmountStr);
				log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());
			}

			WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
			AmeriaBankCommonResponse ameriaBankRefundResponse;

			// AmeriaBank refund request
			AmeriaBankCommonRequest ameriaRefundRequest = new AmeriaBankCommonRequest();
			ameriaRefundRequest.setClientID(getMerchantId());
			ameriaRefundRequest.setOrderId(ccTransaction.getTemporyPaymentId());
			ameriaRefundRequest.setUserName(getUser());
			ameriaRefundRequest.setPaymentAmount(new BigDecimal(refundAmountStr));
			ameriaRefundRequest.setUserPassword(getPassword());

			String updatedMerchTnxRef = merchTnxRef + PaymentConstants.MODE_OF_SERVICE.REFUND;

			CreditCardTransaction ccNewTransaction = auditTransaction(pnr, getMerchantId(), updatedMerchTnxRef,
					creditCardPayment.getTemporyPaymentId(), bundle.getString("SERVICETYPE_REFUND"),
					ameriaRefundRequest.toString(), "", getPaymentGatewayName(), null, false);

			newpaymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());

			ameriaBankRefundResponse = ebiWebervices.refundAmeriaBankPayment(ameriaRefundRequest);

			if (ameriaBankRefundResponse == null) {
				defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						"paymentbroker.ameria.payment.refund.response.invalid");
				return defaultServiceResponse;
			}

			// got refund response
			Integer paymentBrokerRefNo = new Integer(ccTransaction.getTransactionRefNo());
			log.info("[PaymentBrokerAmeriaBankImpl::refund()] Refund Status : " + ameriaBankRefundResponse.toString());

			if (ameriaBankRefundResponse.getRespCode().contentEquals(AmeriaBankPaymentUtil.COMMON_RESP_SUCCESS_CODE)) {

				errorCode = "";

				tnxResultCode = 1;
				errorSpecification = "";
				defaultServiceResponse.setSuccess(true);
				defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);

			} else {
				String status = IPGResponseDTO.STATUS_REJECTED;
				errorCode = ameriaBankRefundResponse.getRespCode();
				String transactionMsg;
				if (!StringUtil.isNullOrEmpty(ameriaBankRefundResponse.getRespMessage())) {
					transactionMsg = ameriaBankRefundResponse.getRespMessage();
				} else {

					errorCode = ameriaBankRefundResponse.getRespCode();
					transactionMsg = ameriaBankRefundResponse.getRespMessage();
					AmeriaBankPaymentUtil.getErrorDescription(errorCode, transactionMsg);
				}

				tnxResultCode = 0;
				errorSpecification = "Status:" + status + " Error code:" + errorCode + " Error Desc:" + transactionMsg;

				defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);

			}

			updateAuditTransactionByTnxRefNo(newpaymentBrokerRefNo.intValue(), ameriaBankRefundResponse.toString(),
					errorSpecification, ameriaBankRefundResponse.getRespCode(), ameriaBankRefundResponse.getOpaque(),
					tnxResultCode);

			defaultServiceResponse.setResponseCode(String.valueOf(paymentBrokerRefNo));
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, null);
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);

		} else {
			log.info("[PaymentBrokerAmeriaBank::refund()] Cannot Refund");
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);
		}

		log.debug("[PaymentBrokerAmeriaBank::refund()] End");

		return defaultServiceResponse;
	}
}
