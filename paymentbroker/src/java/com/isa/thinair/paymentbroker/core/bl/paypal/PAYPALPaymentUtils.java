package com.isa.thinair.paymentbroker.core.bl.paypal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PAYPALPaymentUtils {
	private static Log log = LogFactory.getLog(PAYPALPaymentUtils.class);

	private static final String ERROR_CODE_PREFIX = "paypal.";

	public static String paypal = "PAYPAL";

	public static String NoShipping = "1";
	public static String TypeOfGoods = "0";
	public static String brokerType = "brokerType";
	public static String requestMethod = "requestMethod";
	public static String mode = "mode";
	public static String apiUsername = "apiUsername";
	public static String apiPassword = "apiPassword";
	public static String signature = "signature";
	public static String environment = "environment";
	public static String certificateFilePath = "certificateFilePath";
	public static String privateKeyPassword = "privateKeyPassword";
	public static String useProxy = "useProxy";
	public static String proxyHost = "proxyHost";
	public static String proxyPort = "proxyPort";
	public static String redirectUrl = "redirectUrl";
	public static String cancelUrl = "cancelUrl";
	public static String note = "note";
	public static String orderDescription = "orderDescription";

	public static String PARTIAL_REFUND = "Partial";
	public static String ACCEPTED = "ACCEPTED";
	public static String REJECTED = "REJECTED";
	public static String SUCCESS = "Success";
	public static String EXTERNAL = "external";
	public static String INTERNAL_EXTERNAL = "internal-external";
	public static String COMPLETED = "Completed";

	/**
	 * This method filter the application specific error code on specified priority
	 * 
	 * @param
	 * @return
	 */
	public static String getFilteredErrorCode(String status) {

		String errorCode = "";

		// #1. Check for response status
		errorCode = getMatchingResponseCodeFromStatus(status);

		log.info("[PayPalPaymentUtils::getFilteredErrorCode]Return Code :" + errorCode);
		return errorCode;
	}

	/**
	 * Returns the application specific error code for the Issuer response
	 * 
	 * @param responseStatus
	 *            the Issuer response code
	 * @return the matching error code
	 */
	private static String getMatchingResponseCodeFromStatus(String responseStatus) {
		String result = "";

		// check if a response code code is null or empty
		if (responseStatus != null && !responseStatus.equals("") && !responseStatus.isEmpty()) {
			log.info("[PayPalPaymentUtils::getMatchingResponseCodeFromStatus]Issuer Error Code found :" + responseStatus);

			int input = 0;
			try {
				input = Integer.parseInt(responseStatus);
				switch (input) {
				case 10561:
					result = ERROR_CODE_PREFIX + "10561";
					break;
				case 10562:
					result = ERROR_CODE_PREFIX + "10562";
					break;
				case 10563:
					result = ERROR_CODE_PREFIX + "10563";
					break;
				case 10565:
					result = ERROR_CODE_PREFIX + "10565";
					break;
				case 10701:
					result = ERROR_CODE_PREFIX + "10701";
					break;
				case 10702:
					result = ERROR_CODE_PREFIX + "10702";
					break;
				case 10703:
					result = ERROR_CODE_PREFIX + "10703";
					break;
				case 10704:
					result = ERROR_CODE_PREFIX + "10704";
					break;
				case 10705:
					result = ERROR_CODE_PREFIX + "10705";
					break;
				case 10706:
					result = ERROR_CODE_PREFIX + "10706";
					break;
				case 10707:
					result = ERROR_CODE_PREFIX + "10707";
					break;
				case 10708:
					result = ERROR_CODE_PREFIX + "10708";
					break;
				case 10760:
					result = ERROR_CODE_PREFIX + "10760";
					break;
				case 10762:
					result = ERROR_CODE_PREFIX + "10762";
					break;
				case 10763:
					result = ERROR_CODE_PREFIX + "10763";
					break;
				case 10764:
					result = ERROR_CODE_PREFIX + "10764";
					break;
				default:
					result = ERROR_CODE_PREFIX + "00000";
				}
			} catch (NumberFormatException e) {
				// If return code is not an Integer return ""
				log.error(e);
				result = ERROR_CODE_PREFIX + "00000";
			}

			return result;

		} else {
			return ERROR_CODE_PREFIX + "00000";
		}
	}
}
