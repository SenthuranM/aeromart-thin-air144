package com.isa.thinair.paymentbroker.core.bl.parsian;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;

public class ParsianPaymentUtils {
	
	private static Log log = LogFactory.getLog(ParsianPaymentUtils.class);
	
	private static final String ERROR_CODE_PREFIX = "parsian.";
	
	private static final String ERROR_CODE_PREFIX_REFUND = "parsian.refund.";
	
	public static String getFormattedAmount(String amount, boolean roundUp) {

		BigDecimal amt = new BigDecimal(amount);
		if (roundUp) {
			amt = amt.setScale(0, BigDecimal.ROUND_UP);
		} else {
			amt = amt.setScale(0, BigDecimal.ROUND_DOWN);
		}
		amount = amt.toString();
		return amount;
	}
	
	public static String[] getMappedUnifiedError(short status) {

		String[] errorDesc = new String[2];

		int errorCode = status;

		switch (errorCode) {
		case 0:
			errorDesc[0] = ERROR_CODE_PREFIX + "0001";
			errorDesc[1] = "Successful.";
			break;
		case 22:
			errorDesc[0] = ERROR_CODE_PREFIX + "0002";
			errorDesc[1] = "Merchant's pin or IP server is not correct.";
			break;
		case 24:
			errorDesc[0] = ERROR_CODE_PREFIX + "0003";
			errorDesc[1] = "Merchant's pin or IP server is not correct.";
			break;
		case 30:
			errorDesc[0] = ERROR_CODE_PREFIX + "0004";
			errorDesc[1] = "operation has already been performed successfully.";
			break;
		case 34:
			errorDesc[0] = ERROR_CODE_PREFIX + "0005";
			errorDesc[1] = "Merchant transaction number is not correct.";
			break;
		case 21:
			errorDesc[0] = ERROR_CODE_PREFIX + "0006";
			errorDesc[1] = "Transaction is cancelled by user.";
			break;
		default:
			errorDesc[0] = ERROR_CODE_PREFIX + "1999";
			errorDesc[1] = "Unidentified WS Error -" + status;
			break;

		}

		return errorDesc;

	}
	
	public static String[] getMappedUnifiedErrorForRefund(short status) {

		String[] errorDesc = new String[2];

		int errorCode = status;

		switch (errorCode) {
		case 0:
			errorDesc[0] = ERROR_CODE_PREFIX_REFUND + "0001";
			errorDesc[1] = "Successful Refund.";
			break;
		case 1:
			errorDesc[0] = ERROR_CODE_PREFIX_REFUND + "0002";
			errorDesc[1] = "Refund failed.";
			break;
		case 2:
			errorDesc[0] = ERROR_CODE_PREFIX_REFUND + "0003";
			errorDesc[1] = "Refund Suspected.";
			break;
		case 3:
			errorDesc[0] = ERROR_CODE_PREFIX_REFUND + "0004";
			errorDesc[1] = "operation has already been performed successfully.";
			break;
		default:
			errorDesc[0] = ERROR_CODE_PREFIX_REFUND + "1999";
			errorDesc[1] = "Unidentified WS Error -" + status;
			break;
		}

		return errorDesc;

	}
	
	public static IPGConfigsDTO getIPGConfigs(String pin) {
		IPGConfigsDTO iPGConfigsDTO = new IPGConfigsDTO();
		iPGConfigsDTO.setMerchantID(pin);
		return iPGConfigsDTO;
	}
	
	public static void logRefundStatus(short statusId) {
		switch (statusId) {
		case 1:
			log.error("Parsian Refund status : The initial registration of the refund is performed and is waiting for the confirmation or rejection of the user.");
			break;
		case 2:
			log.error("Parsian Refund status : The requested refund is confirmed by the user..");
			break;
		case 3:
			log.error("Parsian Refund status : The requested refund is canceled by the user..");
			break;				
		case 4:
			log.error("Parsian Refund status : The requesting refund information is sent to the bank service..");
			break;
		default:
			break;
		}
	}
	
}
