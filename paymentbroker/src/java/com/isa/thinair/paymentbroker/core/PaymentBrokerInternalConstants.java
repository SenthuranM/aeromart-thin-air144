package com.isa.thinair.paymentbroker.core;

public abstract class PaymentBrokerInternalConstants {

	public static String MODULE_DESC = "paymentbroker.desc";

	public static enum QUERY_CALLER {
		HASHMISMATCH, INVALIDRESPONSE, SCHEDULER, CANCEL, REFUND, VERIFY, VOID
	}

	public interface MessageCodes {

		String ERROR_DEFAULT_SERVER = "paymentbroker.error.default";

		String ERROR_PARSING_RESPONSE = "paymentbroker.error.parsing.response";

		String ERROR_CREATING_CSHROF2 = "paymentbroker.error.creating.cshroff2";

		String ERROR_ENCRYPTING_REQUEST = "paymentbroker.error.encrypting.request";

		String ERROR_DECRYPTING_RESPONSE = "paymentbroker.error.decrypting.response";

		String REDIRECT_NOT_ALLOWED = "paymentbroker.redirect.not.allowed";

		String REDIRECT_URL_NULL = "paymentbroker.redirect.url.null";

		String REFUND_OPERATION_NOT_SUPPORTED = "paymentbroker.refund.operation.not.supported";

		String REVERSE_OPERATION_NOT_SUPPORTED = "paymentbroker.reverse.operation.not.supported";

		String CAPTURE_OPERATION_NOT_SUPPORTED = "paymentbroker.capture.operation.not.supported";

		String CAPTURE_STATUS_OPERATION_NOT_SUPPORTED = "paymentbroker.capturestatus.operation.not.supported";

		String DIFFERENT_PAYMENT_GATEWAY_TRANSACTION = "paymentbroker.refund.cannotrefundforothertransaction";

		String PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND = "paymentbroker.refund.previouspaymentsrequired";

		String PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REVERSAL = "paymentbroker.reversal.previouspaymentsrequired";

		String PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_CAPTURE = "paymentbroker.capture.previouspaymentsrequired";

		String REVERSE_PAYMENT_ERROR = "paymentbroker.reverse.genericReverseError";

		String CAPTURE_PAYMENT_ERROR = "paymentbroker.capture.genericReverseError";
	}
}
