package com.isa.thinair.paymentbroker.core.bl.tapOffline;

public class TapOfflineRequest {

	public static final String MERCHANT_ID = "Merchant.MerchantID";

	public static final String DEVICE_LICENSE_ID = "Merchant.DeviceLicenseID";

	public static final String HASH = "Merchant.Hash";

	public static final String CUSTOMER_ID = "Customer.ID";

	public static final String CUSTOMER_NAME = "Customer.Name";

	public static final String CUSTOMER_EMAIL = "Customer.Email";

	public static final String CUSTOMER_MOBILE = "Customer.Mobile";

	public static final String ORDER_ID = "MerOrder.OrdID";

	public static final String ORDER_DATE = "MerOrder.OrdDt";

	public static final String ORDER_DUE_DATE = "MerOrder.DueDt";

	public static final String ORDER_CURRENCY_CODE = "MerOrder.CurrencyCode";

	public static final String ORDER_SALES_PERSON = "MerOrder.SalesPerson";

	public static final String PRODUCT_ID = "lstProduct[0].ProdID";

	public static final String PRODUCT_NAME = "lstProduct[0].ProdName";

	public static final String PRODUCT_DESCRIPTION = "lstProduct[0].ProdDesc";

	public static final String PRODUCT_PRICE = "lstProduct[0].UnitPrice";

	public static final String PRODUCT_QUANTITY = "lstProduct[0].Quantity";

	public static final String CONTACT_NAME = "Contact.Name";

	public static final String CONTACT_PHONE = "Contact.Phone";

	public static final String ORDER_NUMBER = "OrdNo";

	public static final String ORDER_ID_FOR_STATUS = "OrdID";

	public static final String TAP_REFERENCE_ID = "TapRefID";
	
	public static final String IBAN = "IBAN";
	
	public static final String ACCOUNT_NAME = "AccountName";
	
	public static final String AMOUNT = "Amount";
}
