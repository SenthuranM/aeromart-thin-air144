package com.isa.thinair.paymentbroker.core.bl.ccAvenue;

import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.ccAvenue.CCAvenueRequest;
import com.isa.thinair.paymentbroker.api.dto.ccAvenue.CCAvenueResponse;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.platform.api.ServiceResponce;

public class PaymentBrokerCCAvenueQueryDR extends PaymentBrokerTemplate
		implements PaymentQueryDR {

	private static Log log = LogFactory.getLog(PaymentBrokerCCAvenueQueryDR.class);

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();
	private String serverToServerReqURL;
	
	@Override
	public ServiceResponce query(CreditCardTransaction oCreditCardTransaction,
			IPGConfigsDTO ipgConfigsDTO, QUERY_CALLER caller)
			throws ModuleException {

		log.debug("[PaymentBrokerCCAvenueQueryDR::query()] Begin");

		String errorSpecification = null;
		String merchantTxnReference = oCreditCardTransaction.getTempReferenceNum();
		String queryParamString = "RefNo: " + oCreditCardTransaction.getTransactionId() + ", MerchantId:"
				+ ipgConfigsDTO.getMerchantID();

		CreditCardTransaction ccTransaction = auditTransaction(oCreditCardTransaction.getTransactionReference(),
				getMerchantId(), merchantTxnReference, new Integer(oCreditCardTransaction.getTemporyPaymentId()),
				bundle.getString("SERVICETYPE_QUERY"), queryParamString, "", getPaymentGatewayName(), null, false);
		
		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		log.debug("Query Request Params : " + queryParamString);
		
		Properties props = PaymentBrokerManager.getProperties(this.ipgIdentificationParamsDTO);
		
		CCAvenueRequest verifyRequest = new CCAvenueRequest();
		verifyRequest.setMerchantId(String.valueOf(ipgConfigsDTO.getMerchantID()));
		verifyRequest.setOrderId(String.valueOf(merchantTxnReference));
		verifyRequest.setAmount(ipgConfigsDTO.getPaymentAmount());
		verifyRequest.setAccessCode(ipgConfigsDTO.getAccessCode());
		verifyRequest.setServerToServerURL(getServerToServerReqURL());
		verifyRequest.setTrackingId(oCreditCardTransaction.getTransactionId());
		verifyRequest.setTransactionTimestamp(oCreditCardTransaction.getTransactionTimestamp());
		
		String xmlRequest = null;
		String encryptedXmlReq = null;
		String response = null;
		CCAvenueResponse ccAveResp = new CCAvenueResponse();
		
		try {
			
			if(verifyRequest.getTrackingId() == null || "".equals(verifyRequest.getTrackingId())){
				// if the tracking id(ccavenue payment reference) is missing, performing a lookupQuery.
				// With out tracking id, cannot refund
				
				log.debug("PaymentBrokerCCAvenueQueryDR : Tracking ID is missing. Performing LookUpQuery for " + verifyRequest.getOrderId());
				verifyRequest.setCommand(CCAvenueRequest.COMMAND_ORDER_LOOKUP);
				xmlRequest = CCAvenuePaymentUtils.composeOrderLookupQuery(verifyRequest);
				encryptedXmlReq = CCAvenuePaymentUtils.encrypt(xmlRequest, props.getProperty(CCAvenueRequest.ENCRYPTION_KEY));
				response = CCAvenuePaymentUtils.sendServerToServerRequest(encryptedXmlReq, verifyRequest);
				ccAveResp = CCAvenuePaymentUtils.processServerToServerResponse(response, props.getProperty(CCAvenueRequest.ENCRYPTION_KEY));
				
				if(ccAveResp ==  null){
					log.warn("PaymentBrokerCCAvenueQueryDR : No tracking_id(ccavenue side tnx id) found for order id : " + verifyRequest.getOrderId() );
					return sr;
				}else{
					oCreditCardTransaction.setTransactionId(ccAveResp.getTracking_id());
					verifyRequest.setTrackingId(ccAveResp.getTracking_id());
				}				
			}
				
			verifyRequest.setCommand(CCAvenueRequest.COMMAND_CONFIRM_ORDER);
			xmlRequest = CCAvenuePaymentUtils.composePaymentStatusCheckQuery(verifyRequest);
			log.debug("PaymentBrokerCCAvenueQueryDR : xmlRequest " + xmlRequest);
			encryptedXmlReq = CCAvenuePaymentUtils.encrypt(xmlRequest, props.getProperty(CCAvenueRequest.ENCRYPTION_KEY));
			response = CCAvenuePaymentUtils.sendServerToServerRequest(encryptedXmlReq, verifyRequest);
			ccAveResp = CCAvenuePaymentUtils.processServerToServerResponse(response, props.getProperty(CCAvenueRequest.ENCRYPTION_KEY));
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String status;
		
		/*
		 * CCAvenue Recommends to consider both "Success" and "Shipped" values as
		 * order_status values for a successful payment.
		 */
		if(!ipgConfigsDTO.isRefunding() && !ccAveResp.isError() && (CCAvenueResponse.ORDER_STATUS_SUCCESSFUL.equals(ccAveResp.getOrderStatus())
				|| CCAvenueResponse.ORDER_STATUS_SHIPPED.equals(ccAveResp.getOrderStatus()))){
			log.debug("[PaymentBrokerCCAvenueQueryDR::query() : Successful Order ] order ID :" + merchantTxnReference);
			
			status = IPGResponseDTO.STATUS_ACCEPTED;
			sr.setSuccess(true);
			sr.addResponceParam(PaymentConstants.AMOUNT, verifyRequest.getAmount());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,	PaymentConstants.SERVICE_RESPONSE_QUERYDR);
			sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE, oCreditCardTransaction.getTransactionId());	
			sr.addResponceParam(CCAvenueRequest.ORDER_STATUS, ccAveResp.getOrderStatus());
			sr.addResponceParam(CCAvenueRequest.TRACKING_ID, verifyRequest.getTrackingId());
			
		/*
		 * Only "Shipped" status transactions are eligible for refunding.
		 * It takes about 5,6 minutes for a transaction to become "Shipped" from "Success"
		 * Since "Auto confirm mode" is enabled for the AirArabia ccAvenue account,  
		 * "Success" to "Shipped" transition happens automatically
		 */
		} else if(ipgConfigsDTO.isRefunding() && !ccAveResp.isError() 
				&& (CCAvenueResponse.ORDER_STATUS_SHIPPED.equals(ccAveResp.getOrderStatus()) || CCAvenueResponse.ORDER_STATUS_REFUNDED.equals(ccAveResp.getOrderStatus()))){ 
			log.debug("[PaymentBrokerCCAvenueQueryDR::query() : Shipped Order ] order ID :" + merchantTxnReference);

			status = IPGResponseDTO.STATUS_ACCEPTED;
			sr.setSuccess(true);
			sr.addResponceParam(PaymentConstants.AMOUNT, verifyRequest.getAmount());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,	PaymentConstants.SERVICE_RESPONSE_QUERYDR);
			sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE, oCreditCardTransaction.getTransactionId());	
			sr.addResponceParam(CCAvenueRequest.ORDER_STATUS, ccAveResp.getOrderStatus());
			sr.addResponceParam(CCAvenueRequest.TRACKING_ID, verifyRequest.getTrackingId());
			
		} else {
			log.debug("[PaymentBrokerCCAvenueQueryDR::query() : Payment not yet confirmed ] order ID :" + merchantTxnReference);
			
			status = IPGResponseDTO.STATUS_REJECTED;
			String error = ccAveResp.getStatusMessage();
			errorSpecification = "Status:" + ccAveResp.getStatusCode() + " Error code:" + ccAveResp.getStatusMessage() + " Error Desc:" + error;
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, error);
			sr.addResponceParam(CCAvenueRequest.ORDER_STATUS, ccAveResp.getOrderStatus());
			sr.addResponceParam(CCAvenueRequest.TRACKING_ID, verifyRequest.getTrackingId());
		}	
		
		updateAuditTransactionByTnxRefNo(ccTransaction.getTransactionRefNo(), status, errorSpecification, oCreditCardTransaction.getTransactionId(),
				oCreditCardTransaction.getTransactionId(), Integer.parseInt(ccAveResp.getStatusCode()));

		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, oCreditCardTransaction.getTransactionId());
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, ccAveResp.getStatusCode());

		log.debug("[PaymentBrokerCCAvenueQueryDR::query() : Query Status :  " + status + "] ");
		log.debug("[PaymentBrokerCCAvenueQueryDR::query()] End");

		return sr;
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		PaymentBrokerManager.validateIPGIdentificationParams(this.ipgIdentificationParamsDTO);
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	/**
	 * @return the serverToServerReqURL
	 */
	public String getServerToServerReqURL() {
		return serverToServerReqURL;
	}

	/**
	 * @param serverToServerReqURL the serverToServerReqURL to set
	 */
	public void setServerToServerReqURL(String serverToServerReqURL) {
		this.serverToServerReqURL = serverToServerReqURL;
	}

}
