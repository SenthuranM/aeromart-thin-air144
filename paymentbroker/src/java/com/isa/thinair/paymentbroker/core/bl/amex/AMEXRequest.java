package com.isa.thinair.paymentbroker.core.bl.amex;

public class AMEXRequest {

	public static final String PSPID = "PSPID";

	public static final String REQUESTID = "REQUESTID";

	public static final String ACTION = "ACTION";

	public static final String AMOUNT = "AMOUNT";

	public static final String CURRENCY = "CURRENCY";

	public static final String LANGUAGE = "LANGUAGE";

	public static final String RETURNURL = "RETURNURL";

	public static final String IPGURL = "IPGURL";

	public static final String USERID = "USERID";

	public static final String PASSWORD = "PASSWORD";

	public static final String KEYSTORE_LOCATION = "KEYSTORE_LOCATION";
	
	public static final String LOG_PATH = "LOG_PATH";

}
