package com.isa.thinair.paymentbroker.core.bl.saman;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SamanPResponse {

	public static final String TNX_ID = "ResNum";

	public static final String MERCHANT_TNX_REF_NO = "RefNum";

	public static final String STATE = "State";
	
	public static final String NO_VALUE = "";
	
	public static final String OK = "OK";
	
	private String merchantTnxId;
	
	private String merchantTnxRefNo;
	
	private String tnxStatus;
	

	Map response = null;
	
	
	public void setResponse(Map response) {
		
		this.response = response;
		this.merchantTnxId = null2unknown((String) response.get(TNX_ID));
		this.merchantTnxRefNo = null2unknown((String) response.get(MERCHANT_TNX_REF_NO));
		this.tnxStatus = null2unknown((String) response.get(STATE));
		
	}
	
	public String getMerchantTnxId() {
		return merchantTnxId;
	}

	public void setMerchantTnxId(String merchantTnxId) {
		this.merchantTnxId = merchantTnxId;
	}

	public String getMerchantTnxRefNo() {
		return merchantTnxRefNo;
	}

	public void setMerchantTnxRefNo(String merchantTnxRefNo) {
		this.merchantTnxRefNo = merchantTnxRefNo;
	}

	public String getTnxStatus() {
		return tnxStatus;
	}

	public void setTnxStatus(String tnxStatus) {
		this.tnxStatus = tnxStatus;
	}

	public Map getResponse() {
		return response;
	}
	
	/*
	 * This method takes a data String and returns a predefined value if empty If data Sting is null, returns string
	 * "No Value Returned", else returns input
	 * 
	 * @param in String containing the data String
	 * 
	 * @return String containing the output String
	 */
	private static String null2unknown(String in) {
		if (in == null || in.length() == 0) {
			return NO_VALUE;
		} else {
			return in;
		}
	}
	
	public String toString() {
		StringBuffer resBuff = new StringBuffer();

		// create a list
		List fieldNames = new ArrayList(response.keySet());
		Iterator itr = fieldNames.iterator();

		// move through the list and create a series of URL key/value pairs
		while (itr.hasNext()) {
			String fieldName = (String) itr.next();
			String fieldValue = (String) response.get(fieldName);
			resBuff.append(fieldName + " : " + fieldValue);
			// add a '&' to the end if we have more fields coming.
			if (itr.hasNext()) {
				resBuff.append(", ");
			}
		}
		return resBuff.toString();
	}

	
	

}
