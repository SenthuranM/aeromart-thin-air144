package com.isa.thinair.paymentbroker.core.bl.saman;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StaticFileNameUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * 
 * @author Janaka Padukka
 * 
 */
public class PaymentBrokerSamanECommerceImpl extends PaymentBrokerSamanTemplate {

	private static Log log = LogFactory.getLog(PaymentBrokerSamanECommerceImpl.class);

	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		log.debug("[PaymentBrokerSamanECommerceImpl::getRequestData()] Begin ");

		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();

		String finalAmount = SamanPaymentUtils.getFormattedAmount(ipgRequestDTO.getAmount(), true);

		// Look and feel parameters are not working, setting only for completion
		postDataMap.put(SamanPRequest.MERCHANT_ID, getMerchantId());
		postDataMap.put(SamanPRequest.TNX_ID, merchantTxnId);
		postDataMap.put(SamanPRequest.PNR, ipgRequestDTO.getPnr());
		postDataMap.put(SamanPRequest.AMOUNT, finalAmount);
		postDataMap.put(SamanPRequest.REDIRECT_URL, ipgRequestDTO.getReturnUrl());
		postDataMap.put(SamanPRequest.PAGE_BG_COLOR, "#ED1E24");
		postDataMap.put(SamanPRequest.PAGE_BORDER_COLOR, "#ED1E24");
		postDataMap.put(SamanPRequest.TABLE_BG_COLOR, "#ED1E24");
		postDataMap.put(SamanPRequest.TABLE_BORDER_COLOR, "#ED1E24");
		postDataMap.put(SamanPRequest.TEXT_COLOR, "#ED1E24");
		postDataMap.put(SamanPRequest.TEXT_FONT, "Arial");
		postDataMap.put(SamanPRequest.TEXT_SIZE, "12");
		postDataMap.put(SamanPRequest.TITLE_COLOR, "#ED1E24");
		postDataMap.put(SamanPRequest.TITLE_FONT, "Arial");
		postDataMap.put(SamanPRequest.TITLE_SIZE, "16");
		postDataMap.put(SamanPRequest.TYPE_TEXT_COLOR, "#ED1E24");
		postDataMap.put(SamanPRequest.TYPE_TEXT_FONT, "Arial");
		postDataMap.put(SamanPRequest.TYPE_TEXT_SIZE, "12");
		postDataMap.put(SamanPRequest.LOGO_URL, CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL)
				+ "/images/" + StaticFileNameUtil.getCorrected("LogoAni" + AppSysParamsUtil.getDefaultCarrierCode() + ".gif"));

		// Payment gateway requests language code's first letter to be
		// capitalized
		String languageCode = DEFAULT_LANGUAGE;
		if (ipgRequestDTO.getSessionLanguageCode() != null) {
			char[] langCodeArray = ipgRequestDTO.getSessionLanguageCode().toCharArray();
			langCodeArray[0] = Character.toUpperCase(langCodeArray[0]);
			languageCode = new String(langCodeArray);
		}
		postDataMap.put(SamanPRequest.LANGUAGE, languageCode);

		String strRequestParams = PaymentBrokerUtils.getPostInputDataFormHTML(postDataMap, getIpgURL());

		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();

		String requestDataForm = PaymentBrokerUtils.getPostInputDataFormHTML(postDataMap, getIpgURL());

		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				strRequestParams + "," + sessionID, "", getPaymentGatewayName(), null, false);

		if (log.isDebugEnabled()) {
			log.debug("Saman PG Request Data : " + strRequestParams + ", sessionID : " + sessionID);
		}

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(requestDataForm);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);
		ipgRequestResultsDTO.setPostDataMap(postDataMap);

		log.debug("[PaymentBrokerSamanECommerceImpl::getRequestData()] End ");
		return ipgRequestResultsDTO;
	}

	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {

		log.debug("[PaymentBrokerSamanECommerceImpl::getReponseData()] Begin ");

		int tnxResultCode;
		String errorCode = "";
		String status;
		String errorSpecification = "";
		String merchantTxnReference;
		String accelAeroTxnReference;


		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response
		SamanPResponse samanResp = new SamanPResponse();
		samanResp.setResponse(receiptyMap);
		merchantTxnReference = samanResp.getMerchantTnxRefNo();
		
		boolean isDuplicateExists = PaymentBrokerUtils.getPaymentBrokerDAO().isDuplicateTransactionExists(merchantTxnReference);
		
		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());
		
		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		oCreditCardTransaction.setResponseText(samanResp.toString());
		oCreditCardTransaction.setTransactionId(merchantTxnReference);
		oCreditCardTransaction.setAidCccompnay(merchantTxnReference);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

		

		if (log.isDebugEnabled()) {
			log.debug("[PaymentBrokerSamanECommerceImpl::getReponseData()] Response -" + samanResp.toString());
		}

		if (samanResp.getTnxStatus().equalsIgnoreCase(SamanPResponse.OK)) {

			// If verification ok, then status should be set as accepted
			// Verifying transaction

			// Refund request.

			BigDecimal paymentAmount = ReservationModuleUtils.getReservationBD().getTempPaymentAmount(
					oCreditCardTransaction.getTemporyPaymentId());

			String paymentAmountStr = SamanPaymentUtils.getFormattedAmount(paymentAmount.toString(), true);

			PaymentQueryDR oNewQuery = getPaymentQueryDR();
			oCreditCardTransaction.setTransactionId(samanResp.getMerchantTnxRefNo());
	
			
			if (!isDuplicateExists) {
				ServiceResponce serviceResponce = oNewQuery.query(oCreditCardTransaction,
						SamanPaymentUtils.getIPGConfigs(getMerchantId(), getPassword()), QUERY_CALLER.VERIFY);
				if (serviceResponce.isSuccess()) {
					Double responseAmount = (Double) serviceResponce.getResponseParam(PaymentConstants.AMOUNT);
					String responseAmountStr = SamanPaymentUtils.getFormattedAmount(responseAmount.toString(), true);

					if (log.isDebugEnabled()) {
						log.debug("[PaymentBrokerSamanECommerceImpl::getReponseData()] - Payment Amount-" + paymentAmount
								+ " Verified Amount-" + responseAmount);
						log.debug("[PaymentBrokerSamanECommerceImpl::getReponseData()] - Formatted Payment Amount-"
								+ paymentAmountStr + " Formatted Verified Amount-" + responseAmountStr);
					}

					if (paymentAmountStr.equals(responseAmountStr)) {
						status = IPGResponseDTO.STATUS_ACCEPTED;
						tnxResultCode = 1;
					} else {
						// Theoritically this scenario cannot happen
						status = IPGResponseDTO.STATUS_REJECTED;
						tnxResultCode = 0;
						errorCode = VERIFICATION_AMOUNTS_ERROR;
						errorSpecification = "Payment and verifcation amounts different!!!";
						// Different amount in response
						log.error("[PaymentBrokerSamanECommerceImpl::getReponseData()] - Payment and verification amounts are different");
					}

				} else {
					status = IPGResponseDTO.STATUS_REJECTED;
					tnxResultCode = 0;
					errorCode = serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE).toString();
					errorSpecification = ""
							+ serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE);
				}
			} else {
				status = IPGResponseDTO.STATUS_REJECTED;
				tnxResultCode = 0;
			}

		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
			String[] error = SamanPaymentUtils.getMappedUnifiedError(samanResp.getTnxStatus());
			errorCode = error[0];
			errorSpecification = "" + error[1];
		}

		
		accelAeroTxnReference = samanResp.getMerchantTnxId();

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setApplicationTransactionId(accelAeroTxnReference);
		ipgResponseDTO.setAuthorizationCode(merchantTxnReference);
		// We don't receive card type information, hence settting it as generic
		ipgResponseDTO.setCardType(getStandardCardType(CARD_TYPE_GENERIC));
		String samanResponse = samanResp.toString();
		updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), samanResponse, errorSpecification,
				merchantTxnReference, merchantTxnReference, tnxResultCode);

		if (log.isDebugEnabled()) {
			log.debug("[PaymentBrokerSamanECommerceImpl::getReponseData()] - Staus-" + status + " Error Code-" + errorCode
					+ " Error Spec-" + errorSpecification);
		}

		log.debug("[PaymentBrokerSamanECommerceImpl::getReponseData()] End");

		return ipgResponseDTO;
	}

	private int getStandardCardType(String card) {
		// [1-MASTER, 2-VISA, 3-AMEX, 4-DINNERS, 5-GENERIC]
		int mapType = 5; // default card type
		if (card != null && !card.equals("") && mapCardType.get(card) != null) {
			String mapTypeStr = (String) mapCardType.get(card);
			mapType = Integer.parseInt(mapTypeStr);
		}
		return mapType;
	}

}
