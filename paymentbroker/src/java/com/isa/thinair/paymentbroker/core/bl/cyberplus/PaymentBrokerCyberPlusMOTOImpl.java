package com.isa.thinair.paymentbroker.core.bl.cyberplus;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.CyberplusRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.CyberplusTransactionInfoDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.wsclient.api.service.WSClientBD;

public class PaymentBrokerCyberPlusMOTOImpl extends PaymentBrokerCyberPlusTemplate implements PaymentBroker {

	static Log log = LogFactory.getLog(PaymentBrokerCyberPlusMOTOImpl.class);

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	private static final int TRANSACTION_RESULT_SUCCESS = 0;

	private static final String CARD_EXPIRY_PREFIX = "20";

	private static final String CARD_EXPIRY_SUFIX = "01120000";

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		String merchantTxnId;
		String errorCode;
		String transactionMsg;
		int txnResultCode;
		String status;
		String errorSpecification;
		String cardNumber;
		String cardExpiry;
		String cardExpiryDate;
		String cvv;
		int cardType;

		int transId = CyberPlusPaymentUtils.getTransId(creditCardPayment.getTemporyPaymentId());
		String date = CyberPlusPaymentUtils.getFormatedDate();
		XMLGregorianCalendar xmlGregorianCal = CyberPlusPaymentUtils.getXMLGregorianCalFromDate(date);
		// Get a merchant transaction id to identify txn in the payment server, in case of failure.
		merchantTxnId = composeAccelAeroTransactionRef(appIndicator, getMerchantId(), date, Integer.toString(transId),
				PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		cardNumber = creditCardPayment.getCardNumber();
		cardExpiry = creditCardPayment.getExpiryDate();
		cardExpiryDate = CARD_EXPIRY_PREFIX + cardExpiry + CARD_EXPIRY_SUFIX;
		XMLGregorianCalendar expiryDate = CyberPlusPaymentUtils.getXMLGregorianCalFromDate(cardExpiryDate);

		cvv = creditCardPayment.getCvvField();
		cardType = creditCardPayment.getCardType();

		String formatedAmount = CyberPlusPaymentUtils.getFormattedAmount(creditCardPayment.getAmount(),
				creditCardPayment.getNoOfDecimalPoints());

		// Creating the order information by buddling the PNR and the Merchant Txn Id together
		String strOrderInfo = CyberPlusPaymentUtils.getOrderInfo(creditCardPayment.getPnr(), "");
		String payCurrencyNumericCode = getStandardCurrencyCode(creditCardPayment.getIpgIdentificationParamsDTO()
				.getPaymentCurrencyCode());

		CyberplusRequestDTO paymentInfo = new CyberplusRequestDTO();

		paymentInfo.setShopId(getMerchantId());
		paymentInfo.setTransmissionDate(xmlGregorianCal);
		paymentInfo.setTransactionId(String.valueOf(transId));
		paymentInfo.setPaymentMethod("CALLCENTER");
		paymentInfo.setOrderId(strOrderInfo);
		paymentInfo.setOrderInfo(null);
		paymentInfo.setOrderInfo2(null);
		paymentInfo.setOrderInfo3(null);
		paymentInfo.setAmount(Long.valueOf(formatedAmount));
		paymentInfo.setCurrency(Integer.valueOf(payCurrencyNumericCode));
		paymentInfo.setPresentationDate(xmlGregorianCal);
		paymentInfo.setValidationMode(0);
		paymentInfo.setCardNumber(cardNumber);
		paymentInfo.setCardNetwork(getStandardCardType(String.valueOf(cardType)));
		paymentInfo.setCardExpirationDate(expiryDate);
		paymentInfo.setCvv(cvv);
		paymentInfo.setContractNumber(null);
		paymentInfo.setCustomerId(null);
		paymentInfo.setCustomerTitle(null);
		paymentInfo.setCustomerName(null);
		paymentInfo.setCustomerPhone(null);
		paymentInfo.setCustomerMail(null);
		paymentInfo.setCustomerAddress(null);
		paymentInfo.setCustomerZipCode(null);
		paymentInfo.setCustomerCity(null);
		paymentInfo.setCustomerCountry(null);
		paymentInfo.setCustomerLanguage(null);
		paymentInfo.setCustomerIP(null);
		paymentInfo.setCustomerSendEmail(false);
		paymentInfo.setCtxMode(getCtxMode());
		paymentInfo.setComment(null);
		paymentInfo.setProxyHost(getProxyHost());
		paymentInfo.setProxyPort(getProxyPort());

		String signature = CyberPlusPaymentUtils.createSignature(getSecureSecret(), paymentInfo.getShopId(),
				CyberPlusPaymentUtils.formatDate(date, CyberPlusPaymentUtils.DATE_FORMAT), paymentInfo.getTransactionId(),
				paymentInfo.getPaymentMethod(), paymentInfo.getOrderId(), paymentInfo.getOrderInfo(),
				paymentInfo.getOrderInfo2(), paymentInfo.getOrderInfo3(), paymentInfo.getAmount(), paymentInfo.getCurrency(),
				CyberPlusPaymentUtils.formatDate(date, CyberPlusPaymentUtils.DATE_FORMAT), paymentInfo.getValidationMode(),
				paymentInfo.getCardNumber(), paymentInfo.getCardNetwork(),
				CyberPlusPaymentUtils.formatDate(cardExpiryDate, CyberPlusPaymentUtils.DATE_FORMAT), paymentInfo.getCvv(),
				paymentInfo.getContractNumber(), paymentInfo.getCustomerId(), paymentInfo.getCustomerTitle(),
				paymentInfo.getCustomerName(), paymentInfo.getCustomerPhone(), paymentInfo.getCustomerMail(),
				paymentInfo.getCustomerAddress(), paymentInfo.getCustomerZipCode(), paymentInfo.getCustomerCity(),
				paymentInfo.getCustomerCountry(), paymentInfo.getCustomerLanguage(), paymentInfo.getCustomerIP(),
				paymentInfo.isCustomerSendEmail(), paymentInfo.getCtxMode(), paymentInfo.getComment());

		paymentInfo.setWsSignature(signature);

		CreditCardTransaction ccTransaction = auditTransaction(pnr, getMerchantId(), merchantTxnId,
				new Integer(creditCardPayment.getTemporyPaymentId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				paymentInfo.toString(), "", getPaymentGatewayName(), null, false);

		Integer paymentBrokerRefNo;
		CyberplusTransactionInfoDTO response;

		// Sends the payment request and gets the response
		try {
			WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
			response = ebiWebervices.create(paymentInfo);

			paymentBrokerRefNo = new Integer(ccTransaction.getTransactionRefNo());
		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		// create a hash map for the response data
		DefaultServiceResponse serviceResponse = new DefaultServiceResponse(false);

		if (ERROR_CODE_RESULT_SUCCESS == response.getErrorCode() && TRANSACTION_RESULT_SUCCESS == response.getAuthResult()) {
			errorCode = "";
			transactionMsg = "";
			status = IPGResponseDTO.STATUS_ACCEPTED;
			txnResultCode = 1;
			errorSpecification = "";
			serviceResponse.setSuccess(true);
		} else {
			errorCode = CyberPlusPaymentUtils.API_ERROR_CODE_PREFIX + response.getErrorCode();
			transactionMsg = CyberPlusPaymentUtils.getWSAPIErrorDescription(response.getErrorCode(),
					response.getTransactionStatus());
			status = IPGResponseDTO.STATUS_REJECTED;
			txnResultCode = 0;
			errorSpecification = "Status:" + status + " Error code:" + errorCode + " Description:" + transactionMsg;
		}

		updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), response.toString(), errorSpecification,
				response.getAuthNb(), "", txnResultCode);

		// Populate service response object
		serviceResponse.setResponseCode(String.valueOf(paymentBrokerRefNo));
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, response.getAuthNb());
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);

		return serviceResponse;
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status = null;
		String errorSpecification = null;
		String merchTnxRef = "";
		DefaultServiceResponse sr = null;

		if (creditCardPayment.getPreviousCCPayment() != null) {
			log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
			log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
			log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());

			CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
					creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}

			merchTnxRef = ccTransaction.getTempReferenceNum();
			String amount = CyberPlusPaymentUtils.getFormattedAmount(creditCardPayment.getAmount(),
					creditCardPayment.getNoOfDecimalPoints());
			String[] merchTnxRefBreakDown = CyberPlusPaymentUtils.getBreakDown(merchTnxRef);
			AppIndicatorEnum appIndicatorEnum = SalesChannelsUtil.isAppIndicatorWebOrMobile(merchTnxRefBreakDown[0])
					? AppIndicatorEnum.APP_IBE
					: AppIndicatorEnum.APP_XBE;

			int newTransId = CyberPlusPaymentUtils.getTransId(creditCardPayment.getTemporyPaymentId());
			String updatedMerchTnxRef = composeAccelAeroTransactionRef(appIndicatorEnum, merchTnxRefBreakDown[1],
					merchTnxRefBreakDown[2], merchTnxRefBreakDown[3], newTransId, PaymentConstants.MODE_OF_SERVICE.REFUND);

			XMLGregorianCalendar transDate = CyberPlusPaymentUtils.getXMLGregorianCalFromDate(merchTnxRefBreakDown[2]);
			String payCurrencyNumericCode = getStandardCurrencyCode(creditCardPayment.getCurrency());

			// Cancel payments for refunds done through XBE if cancelBeforeFefund flag is enabled.
			// Should cancel the full payment amount. Cannot cancel a part of a payment.
			// Should modify the transaction amount to refund a part of a transaction.
			if (isCancelBeforeRefund()) {
				PaymentQueryDR queryDR = getPaymentQueryDR();
				if (queryDR instanceof CyberPlusPaymentQueryDR) {
					CyberplusTransactionInfoDTO queryResponce = ((CyberPlusPaymentQueryDR) queryDR)
							.queryDetails(ccTransaction, CyberPlusPaymentUtils.getIPGConfigs(getCtxMode()),
									PaymentBrokerInternalConstants.QUERY_CALLER.CANCEL);
					if (WAITING_FOR_SUBMISSION == queryResponce.getTransactionStatus()) {
						if (amount.equals(String.valueOf(queryResponce.getAmount()))) {
							ServiceResponce cancelResponse = cancel(creditCardPayment, pnr, appIndicator, tnxMode);
							if (cancelResponse.isSuccess()) {
								return cancelResponse;
							}
						} else {
							// Set previousCCPayment amount for the current total amount for already modified
							// transactions.
							// This can happen for transactions with partial refunds.
							String initialPaymentAmount = CyberPlusPaymentUtils.getFormattedAmount(
									String.valueOf(creditCardPayment.getPreviousCCPayment().getTotalAmount().doubleValue()),
									creditCardPayment.getNoOfDecimalPoints());
							if (!initialPaymentAmount.equals(String.valueOf(queryResponce.getAmount()))) {
								creditCardPayment.getPreviousCCPayment().setTotalAmount(
										new BigDecimal(queryResponce.getAmount() / 100));
							}
							ServiceResponce modifyResponse = modify(creditCardPayment, pnr, appIndicator, tnxMode);
							if (modifyResponse.isSuccess()) {
								return modifyResponse;
							}
						}
					}
				}
			}

			if (getRefundEnabled() != null && getRefundEnabled().equals("true")) {

				CyberplusRequestDTO requestDTO = new CyberplusRequestDTO();
				requestDTO.setShopId(getMerchantId());
				requestDTO.setTransmissionDate(transDate);
				requestDTO.setTransactionId(merchTnxRefBreakDown[3]);
				requestDTO.setSequenceNb(STARTIGN_SEQ_NO);
				requestDTO.setCtxMode(getCtxMode());
				requestDTO.setNewTransactionId(Integer.toString(newTransId));
				requestDTO.setAmount(Long.valueOf(amount));
				requestDTO.setPresentationDate(transDate);
				requestDTO.setValidationMode(0);
				requestDTO.setComment("");
				requestDTO.setCurrency(Integer.valueOf(payCurrencyNumericCode));
				requestDTO.setProxyHost(getProxyHost());
				requestDTO.setProxyPort(getProxyPort());

				String signature = CyberPlusPaymentUtils.createSignature(getSecureSecret(), requestDTO.getShopId(),
						CyberPlusPaymentUtils.formatDate(merchTnxRefBreakDown[2], CyberPlusPaymentUtils.DATE_FORMAT),
						requestDTO.getTransactionId(), requestDTO.getSequenceNb(), requestDTO.getCtxMode(),
						requestDTO.getNewTransactionId(), requestDTO.getAmount(), requestDTO.getCurrency(), requestDTO
								.getPresentationDate().toGregorianCalendar().getTime(), requestDTO.getValidationMode(),
						requestDTO.getComment());

				requestDTO.setWsSignature(signature);

				CyberplusTransactionInfoDTO response = null;
				Integer paymentBrokerRefNo;

				CreditCardTransaction ccNewTransaction = auditTransaction(pnr, getMerchantId(), updatedMerchTnxRef, new Integer(
						ccTransaction.getTemporyPaymentId()), bundle.getString("SERVICETYPE_REFUND"), requestDTO.toString(), "",
						getPaymentGatewayName(), null, false);

				// Sends the refund request and gets the response
				try {
					// Refund request.
					WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
					response = ebiWebervices.refund(requestDTO);

					paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());
				} catch (Exception e) {
					log.error(e);
					throw new ModuleException("paymentbroker.error.unknown");
				}

				log.debug("Refund Response : " + response.toString());

				sr = new DefaultServiceResponse(false);

				if (ERROR_CODE_RESULT_SUCCESS == response.getErrorCode()
						&& WAITING_FOR_SUBMISSION == response.getTransactionStatus()) {
					errorCode = "";
					transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
					status = IPGResponseDTO.STATUS_ACCEPTED;
					tnxResultCode = 1;
					errorSpecification = "";
					sr.setSuccess(true);
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
							PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
				} else {
					// TODO
					errorCode = CyberPlusPaymentUtils.API_ERROR_CODE_PREFIX + response.getErrorCode();
					transactionMsg = CyberPlusPaymentUtils.getWSAPIErrorDescription(response.getErrorCode(),
							response.getTransactionStatus());
					status = IPGResponseDTO.STATUS_REJECTED;
					tnxResultCode = 0;
					errorSpecification = "Status:" + status + " Error code:" + errorCode + " Description:" + transactionMsg;
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
				}

				updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), response.toString(), errorSpecification,
						response.getAuthNb(), "", tnxResultCode);

				sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, response.getAuthNb());
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
			}
			if (sr != null) {
				return sr;
			} else {
				sr = new DefaultServiceResponse(false);
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);

				return sr;
			}
		} else {
			sr = new DefaultServiceResponse(false);
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);

			return sr;
		}
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection creditCardPayment, boolean refundFlag) throws ModuleException {
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, "paymentbroker.generic.operation.notsupported");
		return sr;
	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		String refundEnabled = getRefundEnabled();
		ServiceResponce reverseResult = null;

		if (refundEnabled != null && refundEnabled.equals("false") && !isCancelBeforeRefund()) {

			DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);
			return defaultServiceResponse;
			// throw new ModuleException("paymentbroker.reverse.operation.not.supported");
		}
		reverseResult = refund(creditCardPayment, pnr, appIndicator, tnxMode);
		return reverseResult;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");		
	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
	

}