package com.isa.thinair.paymentbroker.core.bl.wirecard;

public class WireCardRequest {

	public static final String AMOUNT = "AMOUNT";

	public static final String HTTP_USER_AGENT = "HTTP_USER_AGENT";

	public static final String PSPID = "PSPID";

	public static final String REQUESTID = "REQUESTID";

	public static final String ORDERNUMBER = "ORDERNUMBER";

	public static final String ORDERDETAIL = "ORDERDETAIL";

	public static final String USERID = "USERID";

	public static final String PASSWORD = "PASSWORD";

	public static final String LANGUAGE = "LANGUAGE";

	public static final String CURRENCY = "CURRENCY";

	public static final String USE_PROXY = "USE_PROXY";

	public static final String PROXY_HOST = "PROXY_HOST";

	public static final String PROXY_PORT = "PROXY_PORT";

	public static final String IPG_URL = "IPG_URL";

	public static final String URL = "URL";

	public static final String FLAG3D = "FLAG3D";

	public static final String PAYMENT_METHOD = "PAYMENT_METHOD";

	public static final String KEYSTORE_LOCATION = "CERTIFICATE_PATH";

	public static final String IP = "IP";

	public static final String TRANSACTION_ID = "TRANSACTION_ID";

	public static final String OPERATION = "OPERATION";

	public static final String RESTRICT_ATTEMPT_3D = "RESTRICT_ATTEMPT_3D";

	public static final String HOST_IP_ADDRESS = "HOST_IP_ADDRESS";
}