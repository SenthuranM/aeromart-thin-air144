package com.isa.thinair.paymentbroker.core.bl.mpg;

public class MpgRequest {

	public static final String VERSION = "Version";
	public static final String MERCHANT_ID = "MerID";
	public static final String ACQUIRER_ID = "AcqID";
	public static final String MERCHANT_RESP_URL = "MerRespURL";
	public static final String PURCHASE_CURRENCY = "PurchaseCurrency";
	public static final String PURCHASE_CURRENCY_EXPONENT = "PurchaseCurrencyExponent";
	public static final String ORDER_ID = "OrderID";
	public static final String SIGNATURE_METHOD = "SignatureMethod";
	public static final String PURCHASE_AMOUNT = "PurchaseAmt";
	public static final String SIGNATURE = "Signature";
	
	public static final String PASSWORD = "password";
	public static final String MERCHANT_NAME = "merchantName";
	public static final String REASON_CODE = "ReasonCode";
	public static final String REASON_CODE_DESC = "ReasonCodeDesc";
	public static final String RESPONSE_CODE = "ResponseCode";
	public static final String REFERENCE_NUMBER = "ReferenceNo";
	public static final String CARD_NUMBER = "PaddedCardNo";
	public static final String AUTHORIAZTION_CODE = "AuthCode";
	public static final String BILLING_ADDRESS = "BillingAddress";
	public static final String SHIPPING_ADDRESS = "ShippingAddress";
}
