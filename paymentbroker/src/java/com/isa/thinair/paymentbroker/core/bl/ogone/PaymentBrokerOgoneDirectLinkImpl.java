package com.isa.thinair.paymentbroker.core.bl.ogone;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;

public class PaymentBrokerOgoneDirectLinkImpl extends PaymentBrokerOgoneTemplate {

	private static Log log = LogFactory.getLog(PaymentBrokerOgoneDirectLinkImpl.class);

	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		String finalAmount = PaymentBrokerUtils.getFormattedAmount(ipgRequestDTO.getAmount(), ipgRequestDTO.getNoOfDecimals());

		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		// Language = Language_CountryCode, en_EN will be used if this is incorrect.
		String userLanguage = ipgRequestDTO.getSessionLanguageCode();
		String language = userLanguage + "_" + ipgRequestDTO.getIpCountryCode();

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		postDataMap.put(OgoneRequest.ACCEPTURL, ipgRequestDTO.getReturnUrl());
		postDataMap.put(OgoneRequest.AMOUNT, finalAmount);
		postDataMap.put(OgoneRequest.CURRENCY, ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode());
		postDataMap.put(OgoneRequest.DECLINEURL, ipgRequestDTO.getReturnUrl());
		postDataMap.put(OgoneRequest.ECI, String.valueOf(OgonePaymentUtils.ECI.ECOMMERCE.getECIValue()));
		postDataMap.put(OgoneRequest.EXCEPTIONURL, ipgRequestDTO.getReturnUrl());
		postDataMap.put(OgoneRequest.FLAG3D, "Y");
		postDataMap.put(OgoneRequest.LANGUAGE, language);
		postDataMap.put(OgoneRequest.OPERATION, getOperation());
		postDataMap.put(OgoneRequest.ORDERID, merchantTxnId);
		postDataMap.put(OgoneRequest.PSPID, getMerchantId());
		postDataMap.put(OgoneRequest.USERID, getUser());
		postDataMap.put(OgoneRequest.WIN3DS, WIN3DS_MAINW);
		postDataMap.put(OgoneRequest.REMOTE_ADDR, ipgRequestDTO.getUserIPAddress());
		postDataMap.put(OgoneRequest.HTTP_USER_AGENT, ipgRequestDTO.getUserAgent());
		OgonePaymentUtils.addDynamicTemplateUrl(isEnableDynamicTemplate(), postDataMap, getDynamicTemplateUrls(), userLanguage);
		// Fraud Checking data		
		OgonePaymentUtils.addFraudCheckingData(postDataMap, ipgRequestDTO);

		// get request parameters before adding card details to log.
		String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

		postDataMap.put(OgoneRequest.PSWD, getPassword());
		
		if (ipgRequestDTO.getCustomerId() != null) {
			if (ipgRequestDTO.isSaveCreditCard()) {
				postDataMap.put(OgoneRequest.ALIAS, ipgRequestDTO.getAlias());
				postDataMap.put(OgoneRequest.CARDNO, ipgRequestDTO.getCardNo());
				postDataMap.put(OgoneRequest.ED, ipgRequestDTO.getExpiryDate().substring(2)
						+ ipgRequestDTO.getExpiryDate().substring(0, 2));
				postDataMap.put(OgoneRequest.CUSTOMER_NAME, ipgRequestDTO.getHolderName());
			} else if (!StringUtil.isNullOrEmpty(ipgRequestDTO.getAlias())) {
				postDataMap.put(OgoneRequest.ALIAS, ipgRequestDTO.getAlias());
			} else {
				postDataMap.put(OgoneRequest.CARDNO, ipgRequestDTO.getCardNo());
				postDataMap.put(OgoneRequest.ED, ipgRequestDTO.getExpiryDate().substring(2)
						+ ipgRequestDTO.getExpiryDate().substring(0, 2));
				postDataMap.put(OgoneRequest.CUSTOMER_NAME, ipgRequestDTO.getHolderName());
			}
		} else {
			postDataMap.put(OgoneRequest.CARDNO, ipgRequestDTO.getCardNo());
			postDataMap.put(OgoneRequest.ED, ipgRequestDTO.getExpiryDate().substring(2)
					+ ipgRequestDTO.getExpiryDate().substring(0, 2));
			postDataMap.put(OgoneRequest.CUSTOMER_NAME, ipgRequestDTO.getHolderName());
		}
		
		postDataMap.put(OgoneRequest.CVC, ipgRequestDTO.getSecureCode());

		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();

		String requestDataForm = "";

		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				strRequestParams + "," + sessionID, "", getPaymentGatewayName(), null, false);
			
		/* Add tracking ID */
		postDataMap.put(OgoneRequest.COMPLUS, OgonePaymentUtils.getTrackingParameter(ccTransaction.getTransactionRefNo(), ipgRequestDTO));
		
		try {
			String shaSign = OgonePaymentUtils.computeSHA1(postDataMap, getSha1In());
			postDataMap.put(OgoneRequest.SHASIGN, shaSign);
			strRequestParams = strRequestParams + OgoneRequest.SHASIGN + "=" + shaSign;
		} catch (Exception e) {
			log.debug("Error computing SHA-1 : " + e.toString());
			throw new ModuleException(e.getMessage());
		}	
		
		if (log.isDebugEnabled()) {
			log.debug("Ogone Request Data : " + strRequestParams + ", sessionID : " + sessionID);
		}	
		
		// Add other required parameters to create the post request.
		postDataMap.put("PROXY_HOST", getProxyHost());
		postDataMap.put("PROXY_PORT", getProxyPort());
		postDataMap.put("USE_PROXY", isUseProxy() ? "Y" : "N");
		postDataMap.put("URL", getIpgURL());
		
		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(requestDataForm);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);
		ipgRequestResultsDTO.setPostDataMap(postDataMap);

		return ipgRequestResultsDTO;
	}

	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		String finalAmount = PaymentBrokerUtils.getFormattedAmount(ipgRequestDTO.getAmount(), ipgRequestDTO.getNoOfDecimals());		
		String orderId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		
		postDataMap.put(OgoneRequest.PSPID, getMerchantId());
		postDataMap.put(OgoneRequest.ORDERID, orderId);
		postDataMap.put(OgoneRequest.USERID, getUser());
		postDataMap.put(OgoneRequest.PSWD, getPassword());
		postDataMap.put(OgoneRequest.AMOUNT, finalAmount);
		postDataMap.put(OgoneRequest.CURRENCY, ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode());
		postDataMap.put(OgoneRequest.CARDNO, ipgRequestDTO.getCardNo());
		postDataMap.put(OgoneRequest.ED,
				ipgRequestDTO.getExpiryDate().substring(2) + ipgRequestDTO.getExpiryDate().substring(0, 2));
		postDataMap.put(OgoneRequest.CVC, ipgRequestDTO.getSecureCode());
		postDataMap.put(OgoneRequest.OPERATION, getOperation());
		postDataMap.put(OgoneRequest.ALIAS, ipgRequestDTO.getAlias());
		postDataMap.put(OgoneRequest.CUSTOMER_NAME, ipgRequestDTO.getHolderName());
			
		try {
			String shaSign = OgonePaymentUtils.computeSHA1(postDataMap, getSha1In());
			postDataMap.put(OgoneRequest.SHASIGN, shaSign);
		} catch (Exception e) {
			log.error("Error computing SHA-1 : " + e.toString());
			throw new ModuleException(e.getMessage());
		}
		
		postDataMap.put("PROXY_HOST", getProxyHost());
		postDataMap.put("PROXY_PORT", getProxyPort());
		postDataMap.put("USE_PROXY", isUseProxy() ? "Y" : "N");
		postDataMap.put("URL", getIpgURL());
		
		
		String auditedRecord = PaymentBrokerUtils.getRequestDataAsString(postDataMap);
		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();
		
		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), orderId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				auditedRecord + "," + sessionID, "", getPaymentGatewayName(), null, false);

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setAccelAeroTransactionRef(orderId);
		ipgRequestResultsDTO.setPostDataMap(postDataMap);
		return ipgRequestResultsDTO;
	}

}
