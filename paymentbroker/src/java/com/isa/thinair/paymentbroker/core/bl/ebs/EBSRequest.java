package com.isa.thinair.paymentbroker.core.bl.ebs;

import com.isa.thinair.paymentbroker.core.bl.ogone.OgoneRequest;

public class EBSRequest extends OgoneRequest {
	
	public static final String LANGUAGE_US = "en_US";	
	
	public static final String ACCOUNT_ID = "AccountID";
	
	public static final String ACTION = "Action";
	
	public static final String QUERY_ACTION = "statusByRef";
	
	public static final String REFUND_ACTION = "refund";
	
	public static final String SECRET_KEY = "SecretKey";
	
	public static final String REF_NO = "RefNo";
	
	public static final String SUBMITTED = "submitted";
	
	public static final String SUBMITTED_VALUE = "Submit";
	
	public static final String PAYMENT_ID = "PaymentID";
	
	public static final String DEFAULT_VALUE = "AA";
	
}
