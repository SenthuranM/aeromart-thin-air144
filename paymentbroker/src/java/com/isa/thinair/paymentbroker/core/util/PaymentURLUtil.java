package com.isa.thinair.paymentbroker.core.util;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.constants.PaymentAPIConsts;

public class PaymentURLUtil {
	
	private static List<String> paymentReturnURLList = new ArrayList<>();
	
	static {
		paymentReturnURLList.add(AppSysParamsUtil.getSecureServiceAppIBEUrl() + PaymentAPIConsts.REDIRECT_CONTROLLER_URL_SUFFIX);
		paymentReturnURLList.add(AppSysParamsUtil.getSecureServiceAppIBEUrl() + PaymentAPIConsts.REDIRECT_CONTROLLER_MOBILE_URL_SUFFIX);
		paymentReturnURLList.add(AppSysParamsUtil.getSecureIBEUrl() + PaymentAPIConsts.REDIRECT_CONTROLLER_URL_OLD_SUFFIX);
	}
	
	public static List getReturnURLList() {
		return paymentReturnURLList;
	}
	
}
