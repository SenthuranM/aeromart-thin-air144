package com.isa.thinair.paymentbroker.core.bl.cmi;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class Extra {

	@XmlElement
	private String ERRORCODE;
	@XmlElement
	private String SETTLEID;
	@XmlElement
	private String HOSTMSG;
	@XmlElement
	private String TRXDATE;
	@XmlElement
	private String TERMINALID;
	@XmlElement
	private String NUMCODE;
	@XmlElement
	private String MERCHANTID;
	@XmlElement
	private String CARDBRAND;
	@XmlElement
	private String CARDISSUER;
	@XmlElement
	private String TRANS_STAT;
	@XmlElement
	private String ORDERSTATUS;
	@XmlElement
	private String CHARGE_TYPE_CD;
	
	public String getERRORCODE() {
		return ERRORCODE;
	}
	public String getSETTLEID() {
		return SETTLEID;
	}
	public String getHOSTMSG() {
		return HOSTMSG;
	}
	public String getTRXDATE() {
		return TRXDATE;
	}
	public String getTERMINALID() {
		return TERMINALID;
	}
	public String getNUMCODE() {
		return NUMCODE;
	}
	public String getMERCHANTID() {
		return MERCHANTID;
	}
	public String getCARDBRAND() {
		return CARDBRAND;
	}
	public String getCARDISSUER() {
		return CARDISSUER;
	}
	public void setERRORCODE(String eRRORCODE) {
		ERRORCODE = eRRORCODE;
	}
	public void setSETTLEID(String sETTLEID) {
		SETTLEID = sETTLEID;
	}
	public void setHOSTMSG(String hOSTMSG) {
		HOSTMSG = hOSTMSG;
	}
	public void setTRXDATE(String tRXDATE) {
		TRXDATE = tRXDATE;
	}
	public void setTERMINALID(String tERMINALID) {
		TERMINALID = tERMINALID;
	}
	public void setNUMCODE(String nUMCODE) {
		NUMCODE = nUMCODE;
	}
	public void setMERCHANTID(String mERCHANTID) {
		MERCHANTID = mERCHANTID;
	}
	public void setCARDBRAND(String cARDBRAND) {
		CARDBRAND = cARDBRAND;
	}
	public void setCARDISSUER(String cARDISSUER) {
		CARDISSUER = cARDISSUER;
	}
	
	public String getTRANS_STAT() {
		return TRANS_STAT;
	}
	public void setTRANS_STAT(String tRANS_STAT) {
		TRANS_STAT = tRANS_STAT;
	}
	public String getORDERSTATUS() {
		return ORDERSTATUS;
	}
	public void setORDERSTATUS(String oRDERSTATUS) {
		ORDERSTATUS = oRDERSTATUS;
	}
	
	public String getCHARGE_TYPE_CD() {
		return CHARGE_TYPE_CD;
	}
	public void setCHARGE_TYPE_CD(String cHARGE_TYPE_CD) {
		CHARGE_TYPE_CD = cHARGE_TYPE_CD;
	}
	@Override
	public String toString() {
		return "Extra [ERRORCODE=" + ERRORCODE + ", SETTLEID=" + SETTLEID
				+ ", HOSTMSG=" + HOSTMSG + ", TRXDATE=" + TRXDATE
				+ ", TERMINALID=" + TERMINALID + ", NUMCODE=" + NUMCODE
				+ ", MERCHANTID=" + MERCHANTID + ", CARDBRAND=" + CARDBRAND
				+ ", CARDISSUER=" + CARDISSUER + ", TRANS_STAT=" + TRANS_STAT
				+ ", ORDERSTATUS=" + ORDERSTATUS + "]";
	}
	
	

}
