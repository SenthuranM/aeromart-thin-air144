/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.bl.migs3party;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.constants.PaymentGatewayCard;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;

/**
 * MIGS payment request
 * 
 * @author Kanchana
 */
public class MIGS3PRequest {

	private static Log log = LogFactory.getLog(MIGS3PRequest.class);

	public static final String ACTION_CHARGE = "SaleTxnNoPage";

	public static final String CHAR_ENC = "UTF-8";

	public static final String VPC_COMMAND = "vpc_Command";

	public static final String VPC_VERSION = "vpc_Version";

	public static final String VPC_ORDER_INFO = "vpc_OrderInfo";

	public static final String VPC_ACC_CODE = "vpc_AccessCode";

	public static final String VPC_MERCH_TXN_REF = "vpc_MerchTxnRef";

	public static final String VPC_MERCHANT = "vpc_Merchant";

	public static final String VPC_AMOUNT = "vpc_Amount";

	public static final String VPC_LOCALE = "vpc_Locale";

	public static final String VPC_RETURN_URL = "vpc_ReturnURL";

	public static final String VPC_SECURE_HASH = "vpc_SecureHash";

	public static final String MESSAGE_DIGEST_ALGO = "MD5";

	public static final String VPC_TXN_NO = "vpc_TransNo";

	public static final String VPC_USER = "vpc_User";

	public static final String VPC_PASSWORD = "vpc_Password";

	public static final String VPC_CARD_NUM = "vpc_CardNum";

	public static final String VPC_CARD_EXP = "vpc_CardExp"; // In
																// YYMM
																// format

	public static final String VPC_CARD_CVV = "vpc_CardSecurityCode";

	public static final String VPC_TXN_SOURCE = "vpc_TxSource"; // Should
																// be
																// chosen
																// from
																// INTERNET,
																// MAILORDER
																// or
																// TELORDER

	public static final String VPC_TXN_SOURCE_INTERNET = "INTERNET";

	public static final String VPC_TXN_SOURCE_MAILORDER = "MAILORDER";

	public static final String VPC_TXN_SOURCE_TELORDER = "TELORDER";

	public static final String VPC_TXN_SOURCE_SUB_TYPE = "vpc_TxSourceSubType"; // Should
																				// be
																				// chosen
																				// from
																				// SINGLE,

	// INSTALLMENT or RECURRING

	public static final String VPC_TXN_SOURCE_SUB_TYPE_SINGLE = "SINGLE";

	public static final String VPC_TXN_SOURCE_SUB_TYPE_INSTALLMENT = "INSTALLMENT";

	public static final String VPC_TXN_SOURCE_SUB_TYPE_RECURRING = "RECURRING";

	private static final char[] HEX_TABLE = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D',
			'E', 'F' };

	// MCSC Implementation
	public static final String VPC_GATEWAY = "vpc_gateway";

	public enum VpcGatewaySecurity {
		SSL("ssl"), THEREEDSECURE("threeDSecure");

		private String code;

		private VpcGatewaySecurity(String code) {
			this.code = code;
		}

		public String code() {
			return this.code;
		}
	}

	public enum VpcCard {
		// [1-MASTER, 2-VISA, 3-AMEX, 4-DINNERS, 5-GENERIC]
		MASTER("Mastercard"), VISA("Visa"), AMEX("Amex"), DINNERS("Dinners");

		private String code;

		private VpcCard(String code) {
			this.code = code;
		}

		public String code() {
			return this.code;
		}
	}

	public static final String VPC_CARD = "vpc_card";

	public static final String VPC_CONTINUE = "Continue";

	public static final String VPC_SECURE_HASH_TYPE = "vpc_SecureHashType";

	public static final String SECURE_HASH_TYPE_VALUE = "SHA256";

	// ################################

	private String version;

	private String merchantId;

	private String locale;

	private String payCommand;

	private String accessCode;

	private String orderInfo;

	private String amount;

	private String returnURL;

	private String merchantTxnId;

	private String ipgURL;

	private String cardNumber;

	private String cardExpiry;

	private String cardSecurityCode;

	private Map fields = new HashMap();

	// MCSC Implementation
	private String cardType;

	public MIGS3PRequest() {
	}

	@SuppressWarnings("unchecked")
	public MIGS3PRequest(String merchantId, String returnURL, String locale, String command, String merchantAccessCode,
			String orderInfo, String version, String amount, String merchantTxnId, String ipgURL) {
		this.merchantId = merchantId;
		this.returnURL = returnURL;
		this.locale = locale;
		this.payCommand = command;
		this.accessCode = merchantAccessCode;
		this.orderInfo = orderInfo;
		this.version = version;
		this.amount = amount;
		this.merchantTxnId = merchantTxnId;
		this.ipgURL = ipgURL;

		// Fills the hash map
		fields.put(VPC_VERSION, this.version);
		fields.put(VPC_COMMAND, this.payCommand);
		fields.put(VPC_ACC_CODE, this.accessCode);
		fields.put(VPC_MERCH_TXN_REF, this.merchantTxnId);
		fields.put(VPC_MERCHANT, this.merchantId);

		// for the ease of reporting to have the tnx id instead of order info- requested by NI
		// Sending PNR + Temp Payment Id for ease of reporting. Requested by Nasly - Nili 1:39 PM 11/1/2007
		fields.put(VPC_ORDER_INFO, this.orderInfo);
		fields.put(VPC_AMOUNT, this.amount);
		fields.put(VPC_LOCALE, this.locale);
		fields.put(VPC_RETURN_URL, this.returnURL);
	}

	@SuppressWarnings("unchecked")
	public MIGS3PRequest setPostRequestData(String cardType, String cardNo, String expiryDate, String securityCode, boolean isSSL) {

		if (isSSL) {
			fields.put(MIGS3PRequest.VPC_GATEWAY, MIGS3PRequest.VpcGatewaySecurity.SSL.code());
		} else {
			fields.put(MIGS3PRequest.VPC_GATEWAY, MIGS3PRequest.VpcGatewaySecurity.THEREEDSECURE.code());
			// get Card using util
			fields.put(MIGS3PRequest.VPC_CARD, cardType);
			fields.put(MIGS3PRequest.VPC_CARD_NUM, cardNo);
			fields.put(MIGS3PRequest.VPC_CARD_EXP, expiryDate);
			fields.put(MIGS3PRequest.VPC_CARD_CVV, securityCode);
		}
		return this;
	}

	public String getReturnURL() {
		return returnURL;
	}

	public void setReturnURL(String returnURL) {
		this.returnURL = returnURL;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	public String getPayCommand() {
		return payCommand;
	}

	public void setPayCommand(String payCommand) {
		this.payCommand = payCommand;
	}

	/**
	 * This method is for sorting the fields and creating an MD5 secure hash.
	 * 
	 * @param fields
	 *            is a map of all the incoming hey-value pairs from the VPC
	 * @param buf
	 *            is the hash being returned for comparison to the incoming hash
	 */
	/*
	 * public String hashAllFields(String secureSecret) {
	 * 
	 * // create a list and sort it List fieldNames = new ArrayList(fields.keySet()); Collections.sort(fieldNames);
	 * 
	 * // create a buffer for the md5 input and add the secure secret first StringBuffer buf = new StringBuffer();
	 * buf.append(secureSecret);
	 * 
	 * // iterate through the list and add the remaining field values Iterator itr = fieldNames.iterator();
	 * 
	 * while (itr.hasNext()) { String fieldName = (String) itr.next(); String fieldValue = (String)
	 * fields.get(fieldName); if ((fieldValue != null) && (fieldValue.length() > 0)) { buf.append(fieldValue); } }
	 * 
	 * MessageDigest md5 = null; byte[] ba = null;
	 * 
	 * // create the md5 hash and UTF-8 encode it try { md5 = MessageDigest.getInstance(MESSAGE_DIGEST_ALGO); ba =
	 * md5.digest(buf.toString().getBytes(CHAR_ENC)); } catch (Exception e) { log.error(" FATAL ERROR  ", e); } // wont
	 * happen
	 * 
	 * return hex(ba);
	 * 
	 * }
	 */

	/**
	 * Returns Hex output of byte array
	 */
	/*
	 * private String hex(byte[] input) { // create a StringBuffer 2x the size of the hash array StringBuffer sb = new
	 * StringBuffer(input.length * 2);
	 * 
	 * // retrieve the byte array data, convert it to hex // and add it to the StringBuffer for (int i = 0; i <
	 * input.length; i++) { sb.append(HEX_TABLE[(input[i] >> 4) & 0xf]); sb.append(HEX_TABLE[input[i] & 0xf]); } return
	 * sb.toString(); }
	 */

	public String getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(String orderInfo) {
		this.orderInfo = orderInfo;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getMerchantTxnId() {
		return merchantTxnId;
	}

	public void setMerchantTxnId(String merchantTxnId) {
		this.merchantTxnId = merchantTxnId;
	}

	public Map getFields() {
		return fields;
	}

	public void setFields(Map fields) {
		this.fields = fields;
	}

	/**
	 * @return the cardExpiry
	 */
	public String getCardExpiry() {
		return cardExpiry;
	}

	/**
	 * @param cardExpiry
	 *            the cardExpiry to set
	 */
	public void setCardExpiry(String cardExpiry) {
		this.cardExpiry = cardExpiry;
	}

	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * @param cardNumber
	 *            the cardNumber to set
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return the cardSecurityCode
	 */
	public String getCardSecurityCode() {
		return cardSecurityCode;
	}

	/**
	 * @param cardSecurityCode
	 *            the cardSecurityCode to set
	 */
	public void setCardSecurityCode(String cardSecurityCode) {
		this.cardSecurityCode = cardSecurityCode;
	}

	public String getRequestRedirectionURL(String secureSecret) throws ModuleException {

		// Create SHA256 secure hash and insert it into the hash map if it was created
		// created. Remember if SECURE_SECRET = "" it will not be created
		if (secureSecret != null && secureSecret.length() > 0) {
			String secureHash = MIGSPaymentUtils.hashAllFieldsMigsSHA256(fields, secureSecret);
			fields.put(VPC_SECURE_HASH, secureHash.toUpperCase());
			fields.put(VPC_SECURE_HASH_TYPE, SECURE_HASH_TYPE_VALUE);
		}

		// Creates the redirection URL
		StringBuffer buf = new StringBuffer();
		buf.append(this.ipgURL).append('?');
		try {
			appendQueryFields(buf, null, fields);
		} catch (UnsupportedEncodingException uee) {
			throw new ModuleException("paymentbroker.error.unsupportedencoding");
		}
		return buf.toString();
	}

	/**
	 * This method is for creating a URL query string.
	 * 
	 * @param buf
	 *            is the inital URL for appending the encoded fields to
	 * @param fields
	 *            is the input parameters from the order page
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public void
			appendQueryFields(StringBuffer reqBuf, StringBuffer dummyBuf, Map fields, List<CardDetailConfigDTO> configDataList)
					throws UnsupportedEncodingException {

		if (dummyBuf == null) {
			dummyBuf = new StringBuffer();
		}
		// create a list
		List fieldNames = new ArrayList(fields.keySet());
		Iterator itr = fieldNames.iterator();
		String dummyValue = null;
		// move through the list and create a series of URL key/value pairs
		while (itr.hasNext()) {
			String fieldName = (String) itr.next();
			String fieldValue = (String) fields.get(fieldName);

			if ((fieldValue != null) && (fieldValue.length() > 0)) {
				// append the URL parameters
				reqBuf.append(URLEncoder.encode(fieldName, CHAR_ENC));
				reqBuf.append('=');
				reqBuf.append(URLEncoder.encode(fieldValue, CHAR_ENC));

				dummyBuf.append(URLEncoder.encode(fieldName, CHAR_ENC));
				dummyBuf.append('=');
				dummyValue = fieldValue;
				if (configDataList != null) {
					if (MIGS3PRequest.VPC_CARD_NUM.equals(fieldName)) {
						dummyValue = PaymentBrokerUtils.getDBStroreData(configDataList,
								PaymentGatewayCard.FieldName.CARDNUMBER.code(), fieldValue);
					} else if (MIGS3PRequest.VPC_CARD_EXP.equals(fieldName)) {
						dummyValue = PaymentBrokerUtils.getDBStroreData(configDataList,
								PaymentGatewayCard.FieldName.EXPIRYDATE.code(), fieldValue);
					} else if (MIGS3PRequest.VPC_CARD_CVV.equals(fieldName)) {
						dummyValue = PaymentBrokerUtils.getDBStroreData(configDataList, PaymentGatewayCard.FieldName.CVV.code(),
								fieldValue);
					}
				}
				dummyBuf.append(URLEncoder.encode(dummyValue, CHAR_ENC));
			}

			// add a '&' to the end if we have more fields coming.
			if (itr.hasNext()) {
				reqBuf.append('&');
				dummyBuf.append('&');
			}
		}
	}

	/**
	 * 
	 * @param reqBuf
	 * @param dummyBuf
	 * @param fields
	 * @throws UnsupportedEncodingException
	 * @throws ModuleException
	 */
	public void appendQueryFields(StringBuffer reqBuf, StringBuffer dummyBuf, Map fields) throws UnsupportedEncodingException {
		appendQueryFields(reqBuf, dummyBuf, fields, null);
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
}
