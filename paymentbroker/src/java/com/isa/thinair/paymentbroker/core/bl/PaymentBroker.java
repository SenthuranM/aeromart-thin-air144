/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.bl;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.*;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Sudheera
 * 
 *         PaymentBroker
 */
public interface PaymentBroker {

	/**
	 * Performs card refund operation
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException;

	/**
	 * Performs card payment operation
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException;

	/**
	 * Performs reverse card payment
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException;

	/**
	 * Performs reverse card payment
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException;

	/**
	 * Returns Payment Broker Properties
	 * 
	 * @return
	 */
	public Properties getProperties();

	/**
	 * Returns the request data with auditing the request
	 * 
	 * @param ipgRequestDTO
	 * @return
	 * @deprecated
	 */
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException;

	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException;

	/**
	 * Saves data in the T_CCARD_PAYMENT_STATUS table
	 * @param pnr
	 * @param merchantId
	 * @param merchantTnxId
	 * @param temporyPaymentId
	 * @param serviceType
	 * @param strRequest
	 * @param strResponse
	 * @param paymentGatwayName
	 * @param transactionId
	 * @param isSuccess
	 * @return
	 * @throws ModuleException
	 */
	public CreditCardTransaction auditTransaction(String pnr, String merchantId, String merchantTnxId,
			Integer temporyPaymentId, String serviceType, String strRequest, String strResponse,
			String paymentGatwayName, String transactionId, boolean isSuccess) throws ModuleException;

	/**
	 * Saves data in the T_CCARD_PAYMENT_STATUS table using a DTO
	 * @param creditCardPaymentStatusDTO
	 * @param ipgIdentificationParamsDTO
	 * @return
	 * @throws ModuleException
	 */
	public CreditCardTransaction auditCCTransaction(CreditCardPaymentStatusDTO creditCardPaymentStatusDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException;

	/**
	 * Returns the RESPONSE with auditing the response
	 * 
	 * @param receiptyMap
	 * @param ipgResponseDTO
	 * @return
	 */
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO) throws ModuleException;

	/**
	 * Capture the status response
	 * 
	 * @param receiptyMap
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException;

	/**
	 * Resolves partial payments [ Invokes via a scheduler operation ]
	 * 
	 * @param creditCardPayment
	 *            Collection of CreditCardPayment to be treated
	 * @param refundFlag
	 *            Flag to speficy to do a refund or not
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce resolvePartialPayments(Collection creditCardPayment, boolean refundFlag) throws ModuleException;

	/**
	 * THIS METHOD IS IMPLEMENTED TO REVERSE CREDIT CARD PAYMENTS IF VOUCHERS ARE FOUND WITH THE GIVEN VOUCHER_GROUP_ID
	 *
	 * @param creditCardPayment
	 * @param refundFlag
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce resolvePartialPaymentsForVouchers(Collection<IPGQueryDTO> creditCardPayment, boolean refundFlag)
			throws ModuleException;


	/**
	 * Sets Payment broker identification parameters.
	 */
	public void setIPGIdentificationParams(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException;

	public boolean isEnableRefundByScheduler();

	public String getRefundWithoutCardDetails();

	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException;

	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException;

	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException;

	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException;
	
	/**
	 * Handle post payment response
	 * @throws ModuleException
	 */
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException;

	/**
	 * Returns true if offline payments cancellation is allowed from PGW
	 * @return
	 */
	public boolean isOfflinePaymentsCancellationAllowed();

	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException;
	
}
