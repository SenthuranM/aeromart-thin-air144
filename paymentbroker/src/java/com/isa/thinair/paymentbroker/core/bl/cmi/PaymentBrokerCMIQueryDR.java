package com.isa.thinair.paymentbroker.core.bl.cmi;

import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.CHARGE_TYPE_CD_SUCCESS;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.ORDER_QUERY;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.TRANS_STAT_SUCCESS;

import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.platform.api.ServiceResponce;

public class PaymentBrokerCMIQueryDR extends PaymentBrokerCMITemplate implements
		PaymentQueryDR {

	@Override
	public ServiceResponce query(CreditCardTransaction oCreditCardTransaction,
			IPGConfigsDTO ipgConfigsDTO, QUERY_CALLER caller)
			throws ModuleException {

		String requestString = null;
		CMIXMLResponse response = null;
		StringBuilder errorCode = new StringBuilder();
		errorCode.append(caller.toString());
		DefaultServiceResponse serviceResponse = new DefaultServiceResponse(
				false);

		CMIXMLRequest request = new CMIXMLRequest();
		request.setClientId(getMerchantId());
		request.setName(getUserName());
		request.setPassword(getPassword());
		Extra extraInfo = new Extra();
		extraInfo.setORDERSTATUS(ORDER_QUERY);
		request.setExtra(extraInfo);
		String referenceIdForIPG = oCreditCardTransaction.getTempReferenceNum();
		if (referenceIdForIPG.length() > 1) {
			String key = referenceIdForIPG.substring(0, 1);

			// If it's a IBE
			if (SalesChannelsUtil.isAppIndicatorWebOrMobile(key)) {
				request.setTransId(oCreditCardTransaction.getTransactionId());
				// If it's a XBE
			} else if (AppIndicatorEnum.APP_XBE.toString().equals(key)) {
				request.setOrderId(oCreditCardTransaction.getTransactionId());
			} else {
				throw new ModuleException(
						"paymentbroker.cmi.online.request.build.error");
			}
		}

		//request.setOrderId(oCreditCardTransaction.getTransactionId());
		try {
			requestString = CMIPaymentUtils.generateXMLString(request);
		} catch (JAXBException e) {
			log.error(CMIConstants.ERROR_MSG_WS + e);
			throw new ModuleException(
					"paymentbroker.cmi.online.request.build.error");
		}

		try {
			response = CMIPaymentUtils.fireRequest(requestString, ipgURL);
			if (response == null) {
				throw new ModuleException(
						"paymentbroker.cmi.online.request.rejected.error");
			}

			if (PaymentConstants.RETURN_CODE_SUCCESS.equals(response
					.getProcReturnCode())) {

				serviceResponse.setResponseCode(IPGResponseDTO.STATUS_ACCEPTED);
				if ((TRANS_STAT_SUCCESS.contains(response.getExtra()
						.getTRANS_STAT()) && (CHARGE_TYPE_CD_SUCCESS
						.equals(response.getExtra().getCHARGE_TYPE_CD())))) {
					serviceResponse.setSuccess(true);
					serviceResponse.addResponceParam(
							PaymentConstants.PARAM_QUERY_DR_TXN_CODE,
							oCreditCardTransaction.getTransactionId());
				}

			} else {
				serviceResponse.setResponseCode(IPGResponseDTO.STATUS_REJECTED);
			}
			errorCode.append(response.getErrMsg());
		} catch (XMLStreamException | JAXBException e) {
			log.error(CMIConstants.ERROR_MSG_WS + e);
			throw new ModuleException(
					"paymentbroker.cmi.online.request.rejected.error");
		} catch (ModuleException e) {
			throw new ModuleException(
					"paymentbroker.cmi.online.request.build.error");
		}

		return serviceResponse;

	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment,
			String pnr, AppIndicatorEnum appIndicator, TnxModeEnum tnxMode,
			List<CardDetailConfigDTO> cardDetailConfigData)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment,
			String pnr, AppIndicatorEnum appIndicator, TnxModeEnum tnxMode)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO,
			List<CardDetailConfigDTO> configDataList) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap,
			IPGResponseDTO ipgResponseDTO) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(
			IPGPrepPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap,
			IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO)
			throws ModuleException {
		// TODO Auto-generated method stub

	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

}
