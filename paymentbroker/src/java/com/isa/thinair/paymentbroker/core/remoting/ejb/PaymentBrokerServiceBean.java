package com.isa.thinair.paymentbroker.core.remoting.ejb;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import com.isa.thinair.paymentbroker.api.dto.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.TransactionTimeout;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airreservation.api.dto.PaymentMethodDetailsDTO;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.service.bd.PaymentBrokerServiceDelegateImpl;
import com.isa.thinair.paymentbroker.core.service.bd.PaymentBrokerServiceLocalDelegateImpl;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Srikantha
 */
@Stateless
@RemoteBinding(jndiBinding = "PaymentBrokerService.remote")
@LocalBinding(jndiBinding = "PaymentBrokerService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class PaymentBrokerServiceBean extends PlatformBaseSessionBean implements PaymentBrokerServiceDelegateImpl,
		PaymentBrokerServiceLocalDelegateImpl {

	private static final long serialVersionUID = 8244781239882651936L;

	private Log log = LogFactory.getLog(PaymentBrokerServiceBean.class);

	private static Map<String, List<CardDetailConfigDTO>> paymentGatewayCardHolderConfigMap = new HashMap<String, List<CardDetailConfigDTO>>();

	/**
	 * pre validate the payment before payment collection.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		return null;
	}

	/**
	 * advise how to collect the payment to the caller.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		return null;
	}

	/**
	 * post commit the payment once payment is collected.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		return null;
	}

	/**
	 * refund a given payment.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@TransactionTimeout(600)
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		PaymentBrokerUtils.debug(log, "PaymentBrokerServiceBean:refund()");
		try {
			return PaymentBrokerManager.refund(creditCardPayment, pnr, appIndicator, tnxMode);
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * charge the payment.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)	
	@TransactionTimeout(600)
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		PaymentBrokerUtils.debug(log, "PaymentBrokerServiceBean:charge()");
		try {
			return PaymentBrokerManager.charge(creditCardPayment, pnr, appIndicator, tnxMode, cardDetailConfigData);
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * reverse the payment.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		PaymentBrokerUtils.debug(log, "PaymentBrokerServiceBean:reverse()");
		try {
			return PaymentBrokerManager.reverse(creditCardPayment, pnr, appIndicator, tnxMode);
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * capture the payment.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce capture(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		PaymentBrokerUtils.debug(log, "PaymentBrokerServiceBean:capture()");
		try {
			return PaymentBrokerManager.capture(creditCardPayment, pnr, appIndicator, tnxMode);
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * get the request data.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		PaymentBrokerUtils.debug(log, "PaymentBrokerServiceBean:getRequestData()");
		try {
			return PaymentBrokerManager.getRequestData(ipgRequestDTO);
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * 
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		PaymentBrokerUtils.debug(log, "PaymentBrokerServiceBean:getRequestData()");
		try {
			return PaymentBrokerManager.getRequestData(ipgRequestDTO, configDataList);
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Capture the status response.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce captureStatusResponse(Map receiptyMap, IPGIdentificationParamsDTO ipgIdentificationParamsDTO)
			throws ModuleException {
		PaymentBrokerUtils.debug(log, "PaymentBrokerServiceBean:captureStatusResponse()");
		try {
			return PaymentBrokerManager.captureStatusResponse(receiptyMap, ipgIdentificationParamsDTO);
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Returns Response Data.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		PaymentBrokerUtils.debug(log, "PaymentBrokerServiceBean:getReponseData()");
		try {
			return PaymentBrokerManager.getReponseData(receiptyMap, ipgResponseDTO, ipgIdentificationParamsDTO);
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Save Request
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveRequest(String currentState, int paymentBrokerRefNo, String request) throws ModuleException {
		PaymentBrokerUtils.debug(log, "SaveRequestServiceBean:saveRequest()");
		try {
			PaymentBrokerUtils.saveRequest(currentState, paymentBrokerRefNo, request);
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Save Response
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void
			saveResponse(String currentState, int paymentBrokerRefNo, String response, String transactionId, int tnxResultCode)
					throws ModuleException {
		PaymentBrokerUtils.debug(log, "SaveResponseServiceBean:saveResponse()");
		try {
			PaymentBrokerUtils.saveResponse(currentState, paymentBrokerRefNo, response, transactionId, 0);
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Save Response
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public CreditCardTransaction auditTransaction(String pnr, String merchantId, String merchantTnxId, Integer temporyPaymentId,
			String serviceType, String strRequest, String strResponse, String paymentGatwayName, String transactionId,
			boolean isSuccess) throws ModuleException {
		PaymentBrokerUtils.debug(log, "SaveResponseServiceBean:saveResponse()");
		try {
			return PaymentBrokerUtils.auditTransaction(pnr, merchantId, merchantTnxId, temporyPaymentId, serviceType, strRequest,
					strResponse, paymentGatwayName, transactionId, isSuccess);
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * 
	 * @param creditCardPaymentStatusDTO
	 * @param ipgIdentificationParamsDTO
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override public CreditCardTransaction auditCCTransaction(CreditCardPaymentStatusDTO creditCardPaymentStatusDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		PaymentBrokerUtils.debug(log, "PaymentBrokerServiceBean:auditCCTransaction()");
		try {
			return PaymentBrokerManager.auditCCTransaction(creditCardPaymentStatusDTO, ipgIdentificationParamsDTO);
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Save Response
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateAuditTransactionByTnxRefNo(int transactionRefNo, String plainTextReceipt, String errorSpecification,
			String authorizationCode, String transactionId, int tnxResultCode) throws ModuleException {
		PaymentBrokerUtils.debug(log, "UpdateAuditTransactionByTnxRefNoServiceBean:updateAuditTransactionByTnxRefNo()");
		try {
			PaymentBrokerUtils.updateAuditTransactionByTnxRefNo(transactionRefNo, plainTextReceipt, errorSpecification,
					authorizationCode, transactionId, tnxResultCode);
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Save Response
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveTransaction(CreditCardTransaction oCreditCardTransaction) throws ModuleException {
		PaymentBrokerUtils.debug(log, "UpdateAuditTransactionByTnxRefNoServiceBean:updateAuditTransactionByTnxRefNo()");
		try {
			PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Returns the payment gateway properties.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Properties getProperties(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		return PaymentBrokerManager.getProperties(ipgIdentificationParamsDTO);
	}

	/**
	 * Resolve Partial Payments Operation
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {
		PaymentBrokerUtils.debug(log, "PaymentBrokerServiceBean:resolvePartialPayments()");
		try {
			return PaymentBrokerManager.resolvePartialPayments(colIPGQueryDTO, refundFlag);
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ServiceResponce resolvePartialPaymentsForVouchers(
			Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {
		PaymentBrokerUtils.debug(log, "PaymentBrokerServiceBean:resolvePartialPaymentsForVouchers()");
		try {
			return PaymentBrokerManager.resolvePartialPaymentsForVouchers(colIPGQueryDTO, refundFlag);
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Validates IPG identification parameters.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void validateIPGIdentificationParams(IPGIdentificationParamsDTO ipgIdentifierRequestDTO) throws ModuleException {
		PaymentBrokerManager.validateIPGIdentificationParams(ipgIdentifierRequestDTO);
	}

	/**
	 * checks whether any IPG available with given parameters.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean checkForIPG(IPGIdentificationParamsDTO ipgIdentifierRequestDTO) throws ModuleException {
		return PaymentBrokerManager.checkForIPG(ipgIdentifierRequestDTO);
	}

	public String checkForIPGPerPGWId(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		return PaymentBrokerManager.checkForIPGPerPGWId(ipgIdentificationParamsDTO);
	}

	/**
	 * Returns the scheduler refund enabled ipg names
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection getSchedulerRefundEnabledIPGs() throws ModuleException {
		return PaymentBrokerManager.getSchedulerRefundEnabledIPGs();
	}

	/**
	 * Returns the scheduler refund enabled ipg names
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isRefundWithoutCardDetailsEnabled(String ipgName) throws ModuleException {
		return PaymentBrokerManager.isRefundWithoutCardDetailsEnabled(ipgName);
	}

	/**
	 * Returns the payment gateway available for the currency chosen.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map getPaymentOptions(IPGPaymentOptionDTO ipgPaymentOptionDTO) throws ModuleException {
		return PaymentBrokerManager.getPaymentOptions(ipgPaymentOptionDTO);
	}

	/**
	 * Returns the payment gateway available
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<IPGPaymentOptionDTO> getPaymentGateways(IPGPaymentOptionDTO ipgPaymentOptionDTO) throws ModuleException {
		return PaymentBrokerManager.getPaymentGateways(ipgPaymentOptionDTO);
	}

	/**
	 * Returns the payment gateway available .
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List getPaymentGateways(String modulecode) throws ModuleException {
		return PaymentBrokerUtils.getPaymentBrokerDAO().getDefaultCurrencyPaymentGateway(modulecode, null, false);
	}

	/**
	 * Returns the payment gateway available .
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List getPaymentGateways(Integer ipgId) throws ModuleException {
		return PaymentBrokerUtils.getPaymentBrokerDAO().getDefaultCurrencyPaymentGateway(ipgId);
	}

	/**
	 * 
	 * @param transactionRefNo
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, CreditCardTransaction> loadCreditCardTransactionMap(String transactionRefNo) {
		return PaymentBrokerUtils.getPaymentBrokerDAO().loadTransactionMap(transactionRefNo);
	}

	/**
	 * Returns the payment having support for given currency or not.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isPaymentGatewaySupportsCurrency(Integer ipgId, String currencyCode) throws ModuleException {
		return PaymentBrokerUtils.getPaymentBrokerDAO().isPaymentGatewaySupportsCurrency(ipgId, currencyCode);
	}

	/**
	 * Returns the payment having support for given currency or not.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isPaymentGatewaySupportedCurrency(String currencyCode, ApplicationEngine module) throws ModuleException {
		return PaymentBrokerUtils.getPaymentBrokerDAO().isPaymentGatewaySupportedCurrency(currencyCode, module);
	}

	/**
	 * Returns payment broker name for the transaction.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getPaymentGatewayNameForCCTransaction(Integer tnxId, boolean isOwnTnx) throws ModuleException {
		return PaymentBrokerUtils.getPaymentBrokerDAO().getPaymentGatewayNameForCCTransaction(tnxId, isOwnTnx);
	}

	/**
	 * Returns payment broker name for the transaction.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getPaymentGatewayNameForLCCCCTnx(Integer tptId) throws ModuleException {
		return PaymentBrokerUtils.getPaymentBrokerDAO().getPaymentGatewayNameForLCCCCTnx(tptId);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CreditCardTransaction loadTransactionByTempPayId(int tempPayId) throws ModuleException {
		return PaymentBrokerUtils.getPaymentBrokerDAO().loadTransactionByTempPayId(tempPayId);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CreditCardTransaction loadTransactionByReference(String transactionReference) throws ModuleException {
		return PaymentBrokerUtils.getPaymentBrokerDAO().loadTransactionByReference(transactionReference);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<CardDetailConfigDTO> getPaymentGatewayCardConfigData(String paymentGatewayID) throws ModuleException {
		List<CardDetailConfigDTO> configData = null;
		if (paymentGatewayCardHolderConfigMap.containsKey(paymentGatewayID)) {
			configData = paymentGatewayCardHolderConfigMap.get(paymentGatewayID);
		} else {
			configData = PaymentBrokerUtils.getPaymentBrokerDAO().getPaymentGatewayCardConfigData(paymentGatewayID);
			paymentGatewayCardHolderConfigMap.put(paymentGatewayID, configData);
		}

		if (configData == null || configData.isEmpty())
			throw new ModuleException("paymentbroker.cardconfigaration.not.found");
		return configData;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection getContryPaymentCardBehavior(String countryCode, Collection paymentGatewayIDs) throws ModuleException {
		try {
			Collection<CountryPaymentCardBehaviourDTO> cardInfoList = PaymentBrokerUtils.getPaymentBrokerDAO()
					.getContryPaymentCardBehavior(countryCode, paymentGatewayIDs);
			Collection<CountryPaymentCardBehaviourDTO> activeCardInfoList = new ArrayList<CountryPaymentCardBehaviourDTO>();
			if (cardInfoList != null) {
				for (CountryPaymentCardBehaviourDTO card : cardInfoList) {
					if ("ACT".equalsIgnoreCase(card.getStatus())) {
						activeCardInfoList.add(card);
					}
				}
			}
			cardInfoList = null;
			return activeCardInfoList;
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException("paymentbroker.cardbeavour.not.found");
		}
	}


	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<CountryPaymentCardBehaviourDTO> getONDWiseCountryCardBehavior(String ondCode, Collection paymentGatewayIDs) throws ModuleException {
		try {
			return PaymentBrokerUtils.getPaymentBrokerDAO().getONDWiseCountryCardBehavior(ondCode, paymentGatewayIDs);
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException("paymentbroker.cardbeavour.not.found");
		}
	}


	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection getCreditCardsListForAgent(Collection paymentGatewayIDs) throws ModuleException {
		try {
			Collection<CountryPaymentCardBehaviourDTO> cardInfoList = PaymentBrokerUtils.getPaymentBrokerDAO()
					.getCreditCardsListForAgent(paymentGatewayIDs);
			Collection<CountryPaymentCardBehaviourDTO> activeCardInfoList = new ArrayList<CountryPaymentCardBehaviourDTO>();
			if (cardInfoList != null) {
				for (CountryPaymentCardBehaviourDTO card : cardInfoList) {
					//if ("ACT".equalsIgnoreCase(card.getStatus())) {
						activeCardInfoList.add(card);
					//}
				}
			}
			cardInfoList = null;
			return activeCardInfoList;
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException("paymentbroker.cardbeavour.not.found");
		}
	}

	/*
	 * get the XMLResponse.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@TransactionTimeout(600)
	public XMLResponseDTO getXMLResponse(IPGIdentificationParamsDTO ipgIdentificationParamsDTO, Map postDataMap)
			throws ModuleException {
		PaymentBrokerUtils.debug(log, "PaymentBrokerServiceBean:getXMLResponse()");
		try {
			return PaymentBrokerManager.getXMLResponse(ipgIdentificationParamsDTO, postDataMap);
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Set<String> getPaymentMethodNames(PaymentMethodDetailsDTO paymentMethodDetailsDTO) throws ModuleException {
		return PaymentBrokerUtils.getPaymentBrokerDAO().getPaymentMethodNames(paymentMethodDetailsDTO);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void handleDeferredResponse(Map<String, String> receiptyMap,	IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO)
			throws ModuleException {
		if (log.isDebugEnabled())
			PaymentBrokerUtils.debug(log, "PaymentBrokerServiceBean:handleDeferredResponse()");
		try {
			PaymentBrokerManager.handleDeferredResponse(receiptyMap, ipgResponseDTO, ipgIdentificationParamsDTO);
		} catch (CommonsDataAccessException cdaex) {
			PaymentBrokerUtils.error(log, cdaex.getMessageString(), cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
		
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<IPGPaymentOptionDTO> getActivePaymentGatewayByProviderName(String providerName) {
		return PaymentBrokerUtils.getPaymentBrokerDAO().getActivePaymentGatewayByProviderName(providerName);
	}

	public boolean isDuplicateTransactionExists(String paramString) throws ModuleException {
		return PaymentBrokerUtils.getPaymentBrokerDAO().isDuplicateTransactionExists(paramString);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isCreditCardFeeFromPGW(Integer ipgId) throws ModuleException {
		return PaymentBrokerUtils.getPaymentBrokerDAO().isCreditCardFeeFromPGW(ipgId);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getPaymentGatewayNameFromCCTransaction(int temporyPayId) {
		return PaymentBrokerUtils.getPaymentBrokerDAO().getPaymentGatewayNameFromCCTransaction(temporyPayId);
	}
	
	/**
	 * Returns the payment gateway available for XBE account tab.
	 * 
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List getPaymentGatewaysForAccountTab(Integer ipgId) throws ModuleException {
		return PaymentBrokerUtils.getPaymentBrokerDAO().getDefaultCurrencyPaymentGatewayForAccountTab(ipgId);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CreditCardTransaction loadOfflineTransactionByReference(String transactionReference) throws ModuleException {
		return PaymentBrokerUtils.getPaymentBrokerDAO().loadOfflineLatestTransactionByReference(transactionReference);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CreditCardTransaction loadOfflineConfirmedTransactionByReference(String transactionReference) throws ModuleException {
		return PaymentBrokerUtils.getPaymentBrokerDAO().loadOfflineConfirmedTransactionByReference(transactionReference);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public BigDecimal getPaymentAmountFromTPTId(Integer tempTnxId) throws ModuleException {
		return PaymentBrokerUtils.getPaymentBrokerDAO().getPaymentAmountFromTPTId(tempTnxId);
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isOfflinePaymentsCancellationAllowed(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		return PaymentBrokerManager.isOfflinePaymentsCancellationAllowed(ipgIdentificationParamsDTO);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		return PaymentBrokerManager.getRequestDataForAliasPayment(ipgRequestDTO);
	}

}
