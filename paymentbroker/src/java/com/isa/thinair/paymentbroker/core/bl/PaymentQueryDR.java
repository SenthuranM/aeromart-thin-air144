package com.isa.thinair.paymentbroker.core.bl;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.platform.api.ServiceResponce;

public interface PaymentQueryDR {

	/**
	 * Performs query to find out whether this transaction was really successful at the IPG.
	 * 
	 * @param oCreditCardTransaction
	 * @param ipgConfigsDTO
	 * @param caller
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce query(CreditCardTransaction oCreditCardTransaction, IPGConfigsDTO ipgConfigsDTO,
			PaymentBrokerInternalConstants.QUERY_CALLER caller) throws ModuleException;

}
