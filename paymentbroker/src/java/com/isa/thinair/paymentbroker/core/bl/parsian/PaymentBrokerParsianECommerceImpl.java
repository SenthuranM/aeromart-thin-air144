package com.isa.thinair.paymentbroker.core.bl.parsian;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.Parsian.ParsianPaymentRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.Parsian.ParsianPaymentResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.Parsian.ParsianRefundRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.Parsian.ParsianRefundResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.wsclient.api.service.WSClientBD;

public class PaymentBrokerParsianECommerceImpl extends PaymentBrokerTemplate implements PaymentBroker {

	private static Log log = LogFactory.getLog(PaymentBrokerParsianECommerceImpl.class);

	protected static final String DEFAULT_LANGUAGE = "En";
	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();
	protected static final String CARD_TYPE_GENERIC = "GC";
	protected static final short OPERATION_SUCCESS = 0;

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		log.debug("[PaymentBrokerParsianECommerceImpl::refund()] Begin ");

		ServiceResponce sr = new DefaultServiceResponse(false);
		sr = doRefund(creditCardPayment, pnr, appIndicator, tnxMode);

		log.debug("[PaymentBrokerParsianECommerceImpl::refund()] End");
		return sr;
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		log.debug("[PaymentBrokerParsianECommerceImpl::reverse()] Begin ");
		String refundEnabled = getRefundEnabled();
		ServiceResponce reverseResult = null;
		if (refundEnabled != null && refundEnabled.equals("false")) {

			DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);
			return defaultServiceResponse;
		}

		reverseResult = getCommonRefundReverseResponse(creditCardPayment, pnr, appIndicator, tnxMode, true);
		log.debug("[PaymentBrokerParsianECommerceImpl::reverse()] End ");
		return reverseResult;
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		log.debug("[PaymentBrokerParsianECommerceImpl::getRequestData()] Begin ");

		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();

		String finalAmount = ParsianPaymentUtils.getFormattedAmount(ipgRequestDTO.getAmount(), true);

		ParsianPaymentRequestDTO pinPaymentRequest = new ParsianPaymentRequestDTO();
		pinPaymentRequest.setPin(getMerchantId());
		pinPaymentRequest.setAmount(Integer.parseInt(finalAmount));
		pinPaymentRequest.setCallbackUrl(ipgRequestDTO.getReturnUrl());
		pinPaymentRequest.setOrderId(ipgRequestDTO.getApplicationTransactionId());
		pinPaymentRequest.setAdditionalData(ipgRequestDTO.getPnr());

		WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();

		ParsianPaymentResponseDTO response = ebiWebervices.pinPaymentRequest(pinPaymentRequest);

		if (response.getStatus() == PaymentConstants.SUCCESSFUL) {
			log.debug("[PaymentBrokerParsianECommerceImpl::pinPaymentRequest success] End ");
		} else if (response.getStatus() == PaymentConstants.INVALID_IP || response.getStatus() == PaymentConstants.PIN_INCORRECT) {
			log.debug("[PaymentBrokerParsianECommerceImpl::pinPaymentRequest failed because of invalid IP or invalid Merchant PIN] End ");
		} else if (response.getStatus() == PaymentConstants.OPERATION_ALREADY_COMPLETED) {
			log.debug("[PaymentBrokerParsianECommerceImpl::pinPaymentRequest failed because Operation has been already completed] End ");
		} else if (response.getStatus() == PaymentConstants.INVALID_MERCHANT_TXN_ID) {
			log.debug("[PaymentBrokerParsianECommerceImpl::pinPaymentRequest failed because of invalid Merchant TXN ID] End ");
		}

		String ipgURL = getIpgURL() + "?au=" + response.getAuthority();

		String strRequestParams = PaymentBrokerUtils.getPostInputDataFormHTML(postDataMap, ipgURL);

		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();

		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				strRequestParams + "," + sessionID, "", getPaymentGatewayName(), null, false);

		if (log.isDebugEnabled()) {
			log.debug("Parsian PG Request Data : " + strRequestParams + ", sessionID : " + sessionID);
		}

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(strRequestParams);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);
		ipgRequestResultsDTO.setPostDataMap(postDataMap);

		log.debug("[PaymentBrokerParsianECommerceImpl::getRequestData()] End ");
		return ipgRequestResultsDTO;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		log.debug("[PaymentBrokerParsianECommerceImpl::getReponseData()] Begin ");

		int tnxResultCode;
		String errorCode = "";
		String status;
		String errorSpecification = "";
		String merchantTxnReference;
		String accelAeroTxnReference;
		short parsianStatus = 0;
		String transactionId = null;

		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		merchantTxnReference = receiptMap.get("au").toString();
		String responseString = createReceiptMapString(receiptMap);
		boolean isDuplicateExists = PaymentBrokerUtils.getPaymentBrokerDAO().isDuplicateTransactionExists(merchantTxnReference);

		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());

		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		oCreditCardTransaction.setResponseText(responseString);
		oCreditCardTransaction.setAidCccompnay(merchantTxnReference);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

		if (log.isDebugEnabled()) {
			log.debug("[PaymentBrokerParsianECommerceImpl::getReponseData()] Response -" + responseString);
		}

		oCreditCardTransaction.setTransactionId(merchantTxnReference);
		PaymentQueryDR oNewQuery = getPaymentQueryDR();
		if (!isDuplicateExists) {
			ServiceResponce serviceResponce = oNewQuery.query(oCreditCardTransaction,
					ParsianPaymentUtils.getIPGConfigs(getMerchantId()), QUERY_CALLER.VERIFY);
			if (serviceResponce.isSuccess()) {
				transactionId = (String) serviceResponce.getResponseParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE);				
				status = IPGResponseDTO.STATUS_ACCEPTED;
				tnxResultCode = 1;
			} else {
				status = IPGResponseDTO.STATUS_REJECTED;
				tnxResultCode = 0;
				transactionId = merchantTxnReference;
				errorCode = serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE).toString();
				errorSpecification = "" + serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE);
			}
		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
		}

		accelAeroTxnReference = oCreditCardTransaction.getTempReferenceNum();

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setApplicationTransactionId(accelAeroTxnReference);
		ipgResponseDTO.setAuthorizationCode(merchantTxnReference);
		// We don't receive card type information, hence setting it as generic
		ipgResponseDTO.setCardType(getStandardCardType(CARD_TYPE_GENERIC));
		updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), responseString, errorSpecification,
				transactionId, transactionId, tnxResultCode);

		if (log.isDebugEnabled()) {
			log.debug("[PaymentBrokerParsianECommerceImpl::getReponseData()] - Staus-" + status + " Error Code-" + errorCode
					+ " Error Spec-" + errorSpecification);
		}

		log.debug("[PaymentBrokerParsianECommerceImpl::getReponseData()] End");

		return ipgResponseDTO;
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection creditCardPayment, boolean refundFlag) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		// TODO Auto-generated method stub

	}

	private String createReceiptMapString(Map receiptMap) {
		StringBuffer resBuff = new StringBuffer();

		List fieldNames = new ArrayList(receiptMap.keySet());
		Iterator itr = fieldNames.iterator();

		while (itr.hasNext()) {
			String fieldName = (String) itr.next();
			String fieldValue = (String) receiptMap.get(fieldName);
			resBuff.append(fieldName + " : " + fieldValue);
			if (itr.hasNext()) {
				resBuff.append(", ");
			}
		}
		return resBuff.toString();
	}

	private int getStandardCardType(String card) {
		// [1-MASTER, 2-VISA, 3-AMEX, 4-DINNERS, 5-GENERIC]
		int mapType = 5; // default card type
		if (card != null && !card.equals("") && mapCardType.get(card) != null) {
			String mapTypeStr = (String) mapCardType.get(card);
			mapType = Integer.parseInt(mapTypeStr);
		}
		return mapType;
	}

	private ServiceResponce getCommonRefundReverseResponse(CreditCardPayment creditCardPayment, String pnr,
			AppIndicatorEnum appIndicator, TnxModeEnum tnxMode, boolean isReversePayment) throws ModuleException {

		String errorCode = "";
		String transactionMsg;
		String merchTnxRef;

		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

		String refundAmountStr = ParsianPaymentUtils.getFormattedAmount(creditCardPayment.getAmount(), false);

		Integer refundAmount = new Integer(refundAmountStr);

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		if (creditCardPayment.getPreviousCCPayment() != null && getRefundEnabled() != null && getRefundEnabled().equals("true")) {
			if (log.isDebugEnabled()) {
				log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
				log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
				log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
				log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
				log.debug("CC Old pay Formatted sAmount : " + refundAmountStr);
				log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());
			}

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}

			merchTnxRef = ccTransaction.getTempReferenceNum();
			Integer paymentBrokerRefNo;
			ParsianPaymentRequestDTO refundRequest = new ParsianPaymentRequestDTO();
			refundRequest.setPin(getMerchantId());
			refundRequest.setOrderId(ccTransaction.getTransactionRefNo());
			refundRequest.setOrderToRefund(ccTransaction.getTemporyPaymentId());
			refundRequest.setAmount(refundAmount);

			if (log.isDebugEnabled()) {
				log.debug("Calling Parsian Payment Refund ");
			}
			ParsianPaymentResponseDTO parsianRefundResponse = refundParsian(refundRequest, merchTnxRef,
					creditCardPayment.getTemporyPaymentId(), isReversePayment);
			paymentBrokerRefNo = parsianRefundResponse.getPaymentBrokerRefNo();

			if (OPERATION_SUCCESS == parsianRefundResponse.getStatus()) {

				log.info(" Parsian Payment Refund Success. ");
				errorCode = "";
				sr.setSuccess(true);
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
			} else {
				log.info(" Parsian Payment Refund failed, Not call REFUND_EXC  ");
				String[] error = ParsianPaymentUtils.getMappedUnifiedError(parsianRefundResponse.getStatus());
				errorCode = error[0];
				transactionMsg = error[1];
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
			}

			sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, "");
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);

		} else {
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);

		}
		return sr;

	}

	private ParsianPaymentResponseDTO refundParsian(ParsianPaymentRequestDTO refundRequest, String merchTnxRef,
			int temporyPaymentId, boolean isReversePayment) throws ModuleException {

		log.debug("[PaymentBrokerParsianECommerceImpl::refundParsian()] Begin");
		short tnxResultCode = -1;
		String errorCode;
		String transactionMsg;
		String status;
		String errorSpecification;
		ParsianPaymentResponseDTO refundResponse = null;

		if (isValidParsianRefundExcRequest(refundRequest, merchTnxRef)) {

			String updatedMerchTnxRef = merchTnxRef + PaymentConstants.MODE_OF_SERVICE.REFUND;
			CreditCardTransaction ccNewTransaction;

			ccNewTransaction = auditTransaction("", getMerchantId(), updatedMerchTnxRef, temporyPaymentId,
					bundle.getString("SERVICETYPE_REVERSAL"), refundRequest.getRefundString(), "", getPaymentGatewayName(), null,
					false);

			Integer paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());
			try {
				WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
				if (isReversePayment) {
					refundResponse = ebiWebervices.pinReversal(refundRequest);
				}
				refundResponse.setPaymentBrokerRefNo(paymentBrokerRefNo);

			} catch (Exception e) {
				log.error(e);
				throw new ModuleException("paymentbroker.error.unknown");
			}

			if (refundResponse != null) {
				tnxResultCode = refundResponse.getStatus();
			}

			if (tnxResultCode != 0) {
				// when Unsuccessful operation a negative integer will be returned
				status = IPGResponseDTO.STATUS_REJECTED;
				String[] error = ParsianPaymentUtils.getMappedUnifiedError(tnxResultCode);
				errorCode = error[0];
				transactionMsg = error[1];
				tnxResultCode = 1;
				errorSpecification = "Status:" + status + " Error code:" + errorCode + " Error Desc:" + transactionMsg;

			} else {
				errorCode = "";
				transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
				status = IPGResponseDTO.STATUS_ACCEPTED;
				tnxResultCode = 1;
				errorSpecification = "";
			}

			String response = refundResponse != null ? refundResponse.toString() : "";
			updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), response, errorSpecification,
					Integer.toString(refundRequest.getOrderId()), Integer.toString(refundRequest.getOrderId()), tnxResultCode);

			log.debug("[PaymentBrokerParsianECommerceImpl::refundParsian()] End");

		}
		return refundResponse;

	}

	private boolean isValidParsianRefundExcRequest(ParsianPaymentRequestDTO refundRequest, String merchTnxRef) {

		if (refundRequest == null || merchTnxRef == null) {
			log.debug("Missing param in Parsian Refund request");
			return false;
		}

		if (refundRequest.getPin() == null || refundRequest.getAmount() == 0 || refundRequest.getOrderId() == 0
				|| refundRequest.getOrderToRefund() == 0) {
			log.debug("Invalid param found in ParsianPaymentRequestDTO");
			return false;
		}

		return true;
	}

	private ServiceResponce doRefund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		String errorCode = "";
		String transactionMsg;
		String merchTnxRef;

		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

		String refundAmountStr = ParsianPaymentUtils.getFormattedAmount(creditCardPayment.getAmount(), false);

		Integer refundAmount = new Integer(refundAmountStr);

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		if (creditCardPayment.getPreviousCCPayment() != null && getRefundEnabled() != null && getRefundEnabled().equals("true")) {
			if (log.isDebugEnabled()) {
				log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
				log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
				log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
				log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
				log.debug("CC Old pay Formatted sAmount : " + refundAmountStr);
				log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());
			}

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}

			merchTnxRef = ccTransaction.getTempReferenceNum();
			Integer paymentBrokerRefNo;
			ParsianRefundRequestDTO requestDTO = new ParsianRefundRequestDTO();
			requestDTO.setReceiptNo(Long.parseLong(ccTransaction.getTransactionId()));
			requestDTO.setRefundId(ccTransaction.getTransactionRefNo());
			requestDTO.setAmount(refundAmount);
			requestDTO.setRefundUserName(getRefundUser());
			requestDTO.setPassword(getRefundPass());

			if (log.isDebugEnabled()) {
				log.debug("Calling Parsian Payment Refund ");
			}

			ParsianRefundResponseDTO parsianRefundResponse = refundParsianNew(requestDTO, merchTnxRef,
					creditCardPayment.getTemporyPaymentId());
			paymentBrokerRefNo = parsianRefundResponse.getPaymentBrokerRefNo();

			if (OPERATION_SUCCESS == parsianRefundResponse.getTransactionStatus()) {

				log.info(" Parsian Payment Refund Success. ");
				errorCode = "";
				sr.setSuccess(true);
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
			} else {
				log.info(" Parsian Payment Refund failed, Not call REFUND_EXC  ");
				String[] error = ParsianPaymentUtils.getMappedUnifiedErrorForRefund(parsianRefundResponse.getRefundStatusId());
				errorCode = error[0];
				transactionMsg = error[1];
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
			}

			sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, "");
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);

		} else {
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);

		}
		return sr;

	}

	private ParsianRefundResponseDTO
			refundParsianNew(ParsianRefundRequestDTO requestDTO, String merchTnxRef, int temporyPaymentId) throws ModuleException {

		log.debug("[PaymentBrokerParsianECommerceImpl::refundParsian()] Begin");
		short tnxResultCode = -1;
		String errorCode;
		String transactionMsg;
		String status;
		String errorSpecification;
		ParsianRefundResponseDTO refundResponse = null;

		String updatedMerchTnxRef = merchTnxRef + PaymentConstants.MODE_OF_SERVICE.REFUND;
		CreditCardTransaction ccNewTransaction;

		ccNewTransaction = auditTransaction("", getMerchantId(), updatedMerchTnxRef, temporyPaymentId,
				bundle.getString("SERVICETYPE_REFUND"), requestDTO.toString(), "", getPaymentGatewayName(), null, false);
		// In multiple refunds refundID should be unique
		requestDTO.setRefundId(ccNewTransaction.getTransactionRefNo());
		Integer paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());
		try {
			refundResponse = doRefund(requestDTO);
			refundResponse.setPaymentBrokerRefNo(paymentBrokerRefNo);
		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		if (refundResponse != null) {
			tnxResultCode = refundResponse.getTransactionStatus();
			ParsianPaymentUtils.logRefundStatus(refundResponse.getRefundStatusId());			
		}
		if (tnxResultCode != 0) {
			// when Unsuccessful operation a negative integer will be returned
			status = IPGResponseDTO.STATUS_REJECTED;
			String[] error = ParsianPaymentUtils.getMappedUnifiedErrorForRefund(tnxResultCode);
			errorCode = error[0];
			transactionMsg = error[1];
			tnxResultCode = 1;
			errorSpecification = "Status:" + status + " Error code:" + errorCode + " Error Desc:" + transactionMsg;

		} else {
			errorCode = "";
			transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
			status = IPGResponseDTO.STATUS_ACCEPTED;
			tnxResultCode = 1;
			errorSpecification = "";
		}

		String response = refundResponse != null ? refundResponse.toString() : "";
		updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), response, errorSpecification,
				Long.toString(requestDTO.getRefundId()), Long.toString(requestDTO.getRefundId()), tnxResultCode);

		log.debug("[PaymentBrokerParsianECommerceImpl::refundParsian()] End");

		return refundResponse;
	}

	private ParsianRefundResponseDTO doRefund(ParsianRefundRequestDTO requestDTO) throws ModuleException {

		WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
		ParsianRefundResponseDTO responseDTO;
		try {
			responseDTO = ebiWebervices.doRefund(requestDTO);
			log.debug("[PaymentBrokerParsianECommerceImpl::doRefund()] completed");
			if (responseDTO.isHasPermission()) {
				responseDTO = ebiWebervices.approveRefund(requestDTO);
				log.debug("[PaymentBrokerParsianECommerceImpl::approveRefund()] completed");
			} else {
				log.debug("[PaymentBrokerParsianECommerceImpl::approveRefund()] did not complete");
				responseDTO = ebiWebervices.cancleSuspendedRefund(requestDTO);
				log.debug("[PaymentBrokerParsianECommerceImpl::cancelRefund()] completed");				
				return responseDTO;
			}

		} catch (ModuleException e) {
			log.error(e);
			throw new ModuleException("paymentbroker.error.unknown");
		}
		return responseDTO;

	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

}
