package com.isa.thinair.paymentbroker.core.bl.unionpay;

public enum UnionPayResponseMsg {
	
	SUCCESS("00","Success"),
	PARTIAL_SUCCESS("A6","Partial success"),
	TNX_FAILED_CONTACT_SERVICE("01", "Transaction failed. For details please inquire overseas service hotline 1 ."),
	SYSTEM_DOWN("02","System is not started or temporarily down, please try again later"),
	TNX_TIMEOUT("03","Transaction communication time out, please initiate inquiry transaction"),
	ACCEPTED("05","Transaction has been accepted, please inquire about transaction result shortly"),
	SYSTEM_BUSY("06","System is busy, please retry it later."),
	FORMAT_ERROR("10","Message format error"),
	SIGN_VERIFY_ERROR("11","Verify signature error"),
	REPEAT_TNX("12","Repeat transaction"),
	TNX_KEY_MISSING("13","Message transaction key element missing"),
	TNX_FAILED("30","Transaction failed, please try using other UnionPay card for payment or contact overseas service hotline"),
	MERCHANT_STATE_INCORRECT("31","Merchant state incorrect"),
	NO_SUCH_TNX("32","No such transaction right"),
	EXCEEDING_LIMIT("33","Transaction amount exceeds limit"),
	CANNOT_FIND_TNX("34","Could not find this transaction"),
	INCORRECT_ORIGIN_TNX_STATE("35","Original transaction does not exist or state is incorrect"),
	INVALID_ORIGIN_TNX("36","Does not match original transaction information"),
	FREQUENT_INQUIRES("37","Max number of inquiries exceeded or too frequent operations"),
	RISK_CONSTRAINT("38","UnionPay risk constraint"),
	TIMEOUT("39","Transaction is not within the acceptance time range"),
	SUCCESS_DEDUCTION_EXCEEDING_LIMIT("42","Balance deduction successful but transaction exceeded payment time limit"),
	NOT_ALLOWED("43","Business not allowed, please contact overseas service hotline for help."),
	INCORRECT_NUMBER("44","Wrong number entered or business not opened, please contact overseas service hotline for help."),
	TNX_FAILED_CONTACT_ISSUER("60","Transaction failure, for details, please inquire with your issuer"),
	INVALID_CARD("61","Card number entered is invalid, please double check and enter"),
	INVALID_ISSUER("62","Transaction failed, issuer does not support this merchant, please change to another bank card"),
	INCORRECT_CARD_STATE("63","Card state is incorrect"),
	INSUFFICIENT_BALANCE("64","Card balance is insufficient"),
	CARD_DETAIL_ERROR("65","Error with PIN, expiration date, or CVN2 entered, transaction failure"),
	INCORRECT_MOBILE("66","Cardholder identity information or mobile number entered are incorrect, verification failure"),
	EXCEEDING_PIN_ATTEMPTS("67","Limit on number of PIN entry attempts exceeded"),
	INVALID_BANK("68","Your bank card currently does not support this business, please inquire with your bank or overseas service hotline for help"),
	EXCEED_ENTRY_TIME("69","Time limit on entry exceeded, transaction failure"),
	REDIRECTION_ERROR("70","Transaction has been redirected, waiting for cardholder input"),
	VERIFICATION_FAILD("71","Dynamic password or SMS verification code validation failure"),
	HAVENT_SIGNED_UP("72","You have not signed up for UnionPay card-not-present payment service at the bank counter or on your personal online bank, please go to a bank counter or access your online banking to activate it or contact overseas service hotline for help."),
	EXCEEDED_EXPIRATION("73","Payment card has exceeded expiration date"),
	ENCRY_REQUIRED("76","Requires encryption verification for activation"),
	AUTH_FAILD("77","Bank card has not been activated for authenticated payment"),
	ISSUER_INVALID("78","Issuer transaction rights limited, for details please contact your issuer"),
	SMS_VERIFICATION_FAILED("79","The bank card is valid, but issuer does not support SMS verification"),
	EXPIRED_TOKEN("80","Transaction failed and the token has expired"),
	MONTHLY_AMOUNT_EXCEED("81","Monthly accumulated transaction counter (amount) exceeded"),
	NEEED_PIN_VERIFICATION("82","PIN needs to be verified"),
	ISUUER_PROCESSING("83","Under issuer processing"),
	EMPTY_PIN("84","PIN is required but not submitted"),
	INVALID_FILE("98","File does not exist"),
	GENERAL_ERROR("99","General error");
	
	private final String value;
	private final String key;

	UnionPayResponseMsg(String key, String value) {
		this.value = value;
		this.key = key;
	
	}

	public String getValue() {
		return this.value;
	}

	public String getKey() {
		return this.key;
	}

	public static String getMsgByCode(String key) {
		for (UnionPayResponseMsg e : values()) {
			if (e.key.equals(key))
				return e.value;
		}
		return null;
	}

}
