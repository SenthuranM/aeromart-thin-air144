package com.isa.thinair.paymentbroker.core.bl.amex;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.apache.commons.digester.Digester;
import org.apache.commons.lang.StringUtils;

import com.isa.thinair.commons.api.exception.CommonsRuntimeException;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;

public class AMEXResponse {

	private String maskedAccNo;
	private String action;
	private String bankRefId;
	private String currency;
	private String ipgTransactionId;
	private String language;
	private String merchantRefId;
	private String merVar1;
	private String merVar2;
	private String merVar3;
	private String merVar4;
	private String customerName;
	private String status;
	private String txnAmount;
	private String reason;
	private String authorizationCode;
	private String xml;

	/**
	 * @return the maskedAccNo
	 */
	public String getMaskedAccNo() {
		return maskedAccNo;
	}

	/**
	 * @param maskedAccNo
	 *            the maskedAccNo to set
	 */
	public void setMaskedAccNo(String maskedAccNo) {
		this.maskedAccNo = maskedAccNo;
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action
	 *            the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return the bankRefId
	 */
	public String getBankRefId() {
		return bankRefId;
	}

	/**
	 * @param bankRefId
	 *            the bankRefId to set
	 */
	public void setBankRefId(String bankRefId) {
		this.bankRefId = bankRefId;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the ipgTransactionId
	 */
	public String getIpgTransactionId() {
		return ipgTransactionId;
	}

	/**
	 * @param ipgTransactionId
	 *            the ipgTransactionId to set
	 */
	public void setIpgTransactionId(String ipgTransactionId) {
		this.ipgTransactionId = ipgTransactionId;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the merchantRefId
	 */
	public String getMerchantRefId() {
		return merchantRefId;
	}

	/**
	 * @param merchantRefId
	 *            the merchantRefId to set
	 */
	public void setMerchantRefId(String merchantRefId) {
		this.merchantRefId = merchantRefId;
	}

	/**
	 * @return the merVar1
	 */
	public String getMerVar1() {
		return merVar1;
	}

	/**
	 * @param merVar1
	 *            the merVar1 to set
	 */
	public void setMerVar1(String merVar1) {
		this.merVar1 = merVar1;
	}

	/**
	 * @return the merVar2
	 */
	public String getMerVar2() {
		return merVar2;
	}

	/**
	 * @param merVar2
	 *            the merVar2 to set
	 */
	public void setMerVar2(String merVar2) {
		this.merVar2 = merVar2;
	}

	/**
	 * @return the merVar3
	 */
	public String getMerVar3() {
		return merVar3;
	}

	/**
	 * @param merVar3
	 *            the merVar3 to set
	 */
	public void setMerVar3(String merVar3) {
		this.merVar3 = merVar3;
	}

	/**
	 * @return the merVar4
	 */
	public String getMerVar4() {
		return merVar4;
	}

	/**
	 * @param merVar4
	 *            the merVar4 to set
	 */
	public void setMerVar4(String merVar4) {
		this.merVar4 = merVar4;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName
	 *            the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the txnAmount
	 */
	public String getTxnAmount() {
		return txnAmount;
	}

	/**
	 * @param txnAmount
	 *            the txnAmount to set
	 */
	public void setTxnAmount(String txnAmount) {
		this.txnAmount = txnAmount;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason
	 *            the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the authorizationCode
	 */
	public String getAuthorizationCode() {
		return authorizationCode;
	}

	/**
	 * @param authorizationCode
	 *            the authorizationCode to set
	 */
	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	/**
	 * @return the xml
	 */
	public String getXml() {
		return xml;
	}

	/**
	 * @param xml
	 *            the xml to set
	 */
	public void setXml(String xml) {

		Digester digester = new Digester();
		InputStream is = new ByteArrayInputStream(xml.toString().getBytes());

		this.xml = xml;
		digester.addObjectCreate("res", "com.isa.thinair.paymentbroker.core.bl.amex.AMEXResponse");
		digester.addCallMethod("res/acc_no", "setMaskedAccNo", 0);
		digester.addCallMethod("res/action", "setAction", 0);
		digester.addCallMethod("res/bank_ref_id", "setBankRefId", 0);
		digester.addCallMethod("res/cur", "setCurrency", 0);
		digester.addCallMethod("res/ipg_txn_id", "setIpgTransactionId", 0);
		digester.addCallMethod("res/lang", "setLanguage", 0);
		digester.addCallMethod("res/mer_txn_id", "setMerchantRefId", 0);
		digester.addCallMethod("res/mer_var1", "setMerVar1", 0);
		digester.addCallMethod("res/mer_var2", "setMerVar2", 0);
		digester.addCallMethod("res/mer_var3", "setMerVar3", 0);
		digester.addCallMethod("res/mer_var4", "setMerVar4", 0);
		digester.addCallMethod("res/name", "setCustomerName", 0);
		digester.addCallMethod("res/reason", "setReason", 0);
		digester.addCallMethod("res/txn_amt", "setTxnAmount", 0);
		digester.addCallMethod("res/txn_status", "setStatus", 0);
		digester.addCallMethod("res/auth_code", "setAuthorizationCode", 0);

		try {
			AMEXResponse response = (AMEXResponse) digester.parse(is);

			maskedAccNo = StringUtils.trim(response.getMaskedAccNo());
			action = StringUtils.trim(response.getAction());
			bankRefId = response.getBankRefId();
			currency = StringUtils.trim(response.getCurrency());
			ipgTransactionId = StringUtils.trim(response.getIpgTransactionId());
			language = StringUtils.trim(response.getLanguage());
			merchantRefId = StringUtils.trim(response.getMerchantRefId());
			merVar1 = StringUtils.trim(response.getMerVar1());
			merVar2 = StringUtils.trim(response.getMerVar2());
			merVar3 = StringUtils.trim(response.getMerVar3());
			merVar4 = StringUtils.trim(response.getMerVar4());
			customerName = StringUtils.trim(response.getCustomerName());
			reason = StringUtils.trim(response.getReason());
			txnAmount = StringUtils.trim(response.getTxnAmount());
			status = StringUtils.trim(response.getStatus());
			authorizationCode = StringUtils.trim(response.getAuthorizationCode());
		} catch (Exception e) {
			throw new CommonsRuntimeException(PaymentBrokerInternalConstants.MessageCodes.ERROR_PARSING_RESPONSE,
					PaymentBrokerInternalConstants.MODULE_DESC);
		}

	}
}