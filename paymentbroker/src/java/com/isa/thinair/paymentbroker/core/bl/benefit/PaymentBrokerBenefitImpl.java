package com.isa.thinair.paymentbroker.core.bl.benefit;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;




//import com.aciworldwide.commerce.gateway.plugins.e24PaymentPipe;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGTransactionResultDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.bl.benefit.BenefitResponse.RESPONCECODE;
import com.isa.thinair.paymentbroker.core.bl.benefit.BenefitResponse.RESULTSTATUS;
import com.isa.thinair.paymentbroker.core.bl.knet.KnetResponse.RESPONSETYPE;
import com.isa.thinair.paymentbroker.core.bl.passargad.PasargadRequest;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;


public class PaymentBrokerBenefitImpl extends PaymentBrokerBenefitTemplate {

	private static Log log = LogFactory.getLog(PaymentBrokerBenefitTemplate.class);

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {

		if (log.isInfoEnabled())
			log.info("###Start to process the request..: ");		
		
        IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
        
        Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		postDataMap.put(PasargadRequest.REDIRECTION_MECHANISM, PaymentConstants.MODE_OF_REDIRECTION.URL.getValue());
		postDataMap.put(PasargadRequest.BROKER_TYPE, getBrokerType());
		ipgRequestResultsDTO.setPostDataMap(postDataMap);
		
		String finalAmount = ipgRequestDTO.getAmount();
		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		e24PaymentPipe pipe = new e24PaymentPipe();

		// the following properties (setAction, setCurrency, setLanguage) must not be modified at all.
		pipe.setAction(String.valueOf(BenefitRequest.ACTION.PURCHASE.getAction()));
		pipe.setCurrency(BenefitRequest.CURRENCY_CODE_BPG);
		pipe.setLanguage(BenefitRequest.LANGUAGE_CODE);

		// Set the path to the FOLDER (WITHOUT writing the file name) that contains the resource File.
		// Use "\\" instead of "\"
		pipe.setResourcePath(getSecureKeyLocation());

		// Set the path to response page.
		if (ipgRequestDTO.getReturnUrl() != null && !ipgRequestDTO.getReturnUrl().equals("")) {
			pipe.setResponseURL(BenefitRequest.RESPONSE_URL_XBE);
			pipe.setErrorURL(BenefitRequest.RESPONSE_URL_XBE);
		} else {
			pipe.setResponseURL(BenefitRequest.RESPONSE_URL);

			// Set the path to error page.
			pipe.setErrorURL(BenefitRequest.RESPONSE_URL);
		}

		// Set the alias name.
		pipe.setAlias(getMerchantId());

		// TrackId and UDF ("user defined field" will travel with payment request)
		// can be used as a refrence (to identify the request that has been sent)
		// after getting the response back from "BENEFIT Payment Gateway".
		// Example of UDF: user_ID, Account_No, transaction_ID, customer_ID
		pipe.setTrackId(merchantTxnId);
		BenefitUtil.addAdditionalData(ipgRequestDTO, pipe);

		String paymentRequestString = BenefitUtil.concatPaymentRequestString(ipgRequestDTO, pipe);
		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				paymentRequestString, "", getPaymentGatewayName(), null, false);
		
		// We set this parameters to payment response validate
		int paymentBrokerRefNo = ccTransaction.getTransactionRefNo();
		pipe.setUdf3(ipgRequestDTO.getIpgIdentificationParamsDTO().getIpgId() + "_" + ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode());
		pipe.setUdf4(String.valueOf(paymentBrokerRefNo));
		pipe.setUdf5(ipgRequestDTO.getPnr() != null ? ipgRequestDTO.getPnr() : "");

		// Set the desired amount to be paid.
		pipe.setAmt(finalAmount);
		
		
		 if (log.isInfoEnabled())
	        	log.info("try to generate paymentID -PNR is " + ipgRequestDTO.getPnr());

		try {
			if (pipe.performPaymentInitialization() == e24PaymentPipe.SUCCESS) {
				// The Payment Initialization Was Sent Successfully.
				// Use the following format of URL to redirect users: pipe.getPaymentPage() + "?PaymentID=" +
				// pipe.getPaymentId()
				// Example: https://www.YourWebsiteName.com/payment/payment.jsp?PaymentID=3797500191042430
	            log.info("Successfully Sent the Payment Initialization Request: PaymentID = " + pipe.getPaymentId());                  

			} else {
				// There Was An Error During Sending The Payment Initilization.
				// Write your own logic.
				log.error("Error sending Payment Initialization Request: " + pipe.getErrorMsg());                  
	            throw new ModuleException("paymentbroker.gateway.not.respond");
			}
		} catch (Exception e) {
			// Write your own logic.
			log.error("Error sending Payment Initialization Request: ", e);   
        	throw new ModuleException("paymentbroker.gateway.not.respond");
        	
		} finally {
			// Write your own logic.
			 if (log.isInfoEnabled())
		        	log.info("After Gererated payment ID - PNR is " + ipgRequestDTO.getPnr());
		        // Get results        
		        String payID = pipe.getPaymentId();
		        String payURL = pipe.getPaymentPage();        
		        paymentRequestString = BenefitUtil.concatPaymentRequestString(ipgRequestDTO, pipe);
		        if (log.isInfoEnabled())
		        	log.info(paymentRequestString);  
		        
		        updateAuditTransactionByTnxRefNo(paymentBrokerRefNo, paymentRequestString , payID);    
		        
				ipgRequestResultsDTO.setRequestData(payURL + "?PaymentID=" + payID );
				ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
				ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);	
				
				if (log.isInfoEnabled())
					log.info("End of request data process..");
				
		}
		return ipgRequestResultsDTO;

	}
	
	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		if (log.isInfoEnabled()) 
			log.info("Benefit response start:" + ipgResponseDTO.getTemporyPaymentId());		

		boolean errorExists = false;
		int tnxResultCode;
		String errorCode = "";
		String status;
		String errorSpecification;
		String merchantTxnReference;
		String authorizeID;
		int cardType = 5; // Generic Card type		
		
		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());

		// Calculate response time
		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time
		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

		BenefitResponse benefitResp = new BenefitResponse();
		benefitResp.setResponse(receiptyMap);
		
		if (log.isInfoEnabled())
			log.info("Benefit Response Mid Response :" + benefitResp);
		
		if (oCreditCardTransaction == null || !oCreditCardTransaction.getTempReferenceNum().equalsIgnoreCase(benefitResp.getTrackid()) || 
				(benefitResp.getPaymentId() == null || benefitResp.getPaymentId().isEmpty()) || !oCreditCardTransaction.getGatewayPaymentID().equals(benefitResp.getPaymentId())
				||(!benefitResp.getError().isEmpty()||!benefitResp.getErrorText().isEmpty())) {
			errorExists = true;
		}	

		merchantTxnReference = benefitResp.getTrackid();
		authorizeID = "Benefit Ref:" + benefitResp.getRef();	
		if (authorizeID.length() >= 30) {
			authorizeID = authorizeID.substring(0, 30);
		}		
		// Validate response
		// We are receiving final response via (HandleInterlineIPGResponseAction)
		boolean isValidResponse = true;
		if (!errorExists && benefitResp.getResponseType() == RESPONSETYPE.DIRECT && 
				RESULTSTATUS.CAPTURED == benefitResp.getResult() &&
				oCreditCardTransaction.getTransactionResultCode() != 1) {
			isValidResponse = false;
			if (log.isInfoEnabled())
				log.info("Fatal error.User try to changed the payment response data or invalid response from gateway.."+ benefitResp.getUdf5());
			throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
		}
		
		if (RESULTSTATUS.CAPTURED == benefitResp.getResult() && !errorExists && isValidResponse ) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			tnxResultCode = 1;
			errorSpecification = "";
		} else if(RESULTSTATUS.NOT_CAPTURED == benefitResp.getResult() && !errorExists) {
			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
			errorCode = BenefitUtil.getErrorCodeWithPrefix(benefitResp.getResult(),benefitResp.getResponsecode());
			errorSpecification = BenefitUtil.getErrorCodeDescription(benefitResp.getResponsecode());
		}else {
			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
			errorCode = "ERROR: "+benefitResp.getError();
			errorSpecification = benefitResp.getErrorText();
		}

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setMessage(errorSpecification);
		ipgResponseDTO.setApplicationTransactionId(merchantTxnReference);
		ipgResponseDTO.setAuthorizationCode(authorizeID);
		ipgResponseDTO.setCardType(cardType);		
		ipgResponseDTO.setCcLast4Digits("");
		// Add Transaction Details
		IPGTransactionResultDTO ipgTransactionResultDTO = new IPGTransactionResultDTO();
		// TODO move to configuration or db field
		boolean showTransactionDetail =  true;
		if (RESULTSTATUS.CAPTURED == benefitResp.getResult() && benefitResp.getResponsecode()==RESPONCECODE.RESCODE00)  {
			ipgTransactionResultDTO.setTransactionStatus(IPGResponseDTO.STATUS_ACCEPTED);
		} else {
			ipgTransactionResultDTO.setTransactionStatus(IPGResponseDTO.STATUS_REJECTED);
		}
		if (benefitResp.getUdf3() != null) {
			String[] udf3Arr = benefitResp.getUdf3().split("_");
			String strPayCurCode = udf3Arr.length > 0 ? udf3Arr[1] : "";
			ipgTransactionResultDTO.setCurrency(strPayCurCode);
		}
		ipgTransactionResultDTO.setTransactionTime(CalendarUtil.formatDate(ipgResponseDTO.getRequestTimsStamp(), CalendarUtil.PATTERN5));
		ipgTransactionResultDTO.setShowTransactionDetail(showTransactionDetail);		
		ipgTransactionResultDTO.setMerchantTrackID(merchantTxnReference + "/" + oCreditCardTransaction.getTransactionReference());
		ipgTransactionResultDTO.setReferenceID(benefitResp.getRef());
		ipgResponseDTO.setIpgTransactionResultDTO(ipgTransactionResultDTO);

		updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), benefitResp.toString(), errorSpecification,
				authorizeID, benefitResp.getTranid(), tnxResultCode);
		if (log.isInfoEnabled())
			log.info("[BEBEFIT Response::getReponseData()] End -" + benefitResp.getTrackid() + ":"+benefitResp.getUdf5());

		return ipgResponseDTO;
	}
}
