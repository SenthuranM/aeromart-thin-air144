package com.isa.thinair.paymentbroker.core.bl.cybersource;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import sun.misc.BASE64Encoder;

public class CyberSourceUtils {

	private static final Log log = LogFactory.getLog(CyberSourceUtils.class);

	private String merchantId;

	private String sharedSecret;

	private String serialNumber;

	public CyberSourceUtils(String merchantId, String sharedSecret, String serialNumber) {
		this.setMerchantId(merchantId);
		this.setSharedSecret(sharedSecret);
		this.setSerialNumber(serialNumber);
	}

	public String getPublicDigest(String customValues) throws Exception {
		String pub = getSharedSecret();
		BASE64Encoder encoder = new BASE64Encoder();
		Mac sha1Mac = Mac.getInstance("HmacSHA1");
		SecretKeySpec publicKeySpec = new SecretKeySpec(pub.getBytes(), "HmacSHA1");
		sha1Mac.init(publicKeySpec);
		byte[] publicBytes = sha1Mac.doFinal(customValues.getBytes());
		String publicDigest = encoder.encodeBuffer(publicBytes);
		return publicDigest.replaceAll("\n", "");
	}

	/**
	 * @param map
	 *            - Map containing fields that are to be signed. Can only contain fields and values that should not be
	 *            changed. At the very minimum, map should contain 'amount', 'currency', and 'orderPage_transactionType'
	 *            if 'orderPage_transactionType' is 'subscription' or 'subscription_modify', the following are also
	 *            required: 'recurringSubscriptionInfo_amount', 'recurringSubscriptionInfo_numberOfPayments',
	 *            'recurringSubscriptionInfo_frequency', 'recurringSubscriptionInfo_startDate',
	 *            'recurringSubscriptionInfo_automaticRenew' if 'orderPage_transactionType' is 'subscription_modify'
	 *            then 'paySubscriptionCreateReply_subscriptionID' is also required
	 * @return html of hidden fields
	 */
	public String insertSignature(Map map, String ipgURL) {
		if (map == null) {
			return "";
		}
		try {
			Set keys = map.keySet();
			StringBuilder customFields = new StringBuilder();
			StringBuilder dataToSign = new StringBuilder();
			StringBuilder output = new StringBuilder();
			for (Iterator i = keys.iterator(); i.hasNext();) {
				String key = (String) i.next();
				customFields.append(key);
				dataToSign.append(key + "=" + String.valueOf(map.get(key)));
				if (i.hasNext()) {
					customFields.append(',');
					dataToSign.append(',');
				}

				output.append("<input type=\"hidden\" name=\"");
				output.append(key);
				output.append("\" value=\"");
				output.append(String.valueOf(map.get(key)));
				output.append("\">\n");
			}
			if (customFields.length() > 0) {
				dataToSign.append(',');
			}
			dataToSign.append("signedFieldsPublicSignature=");
			dataToSign.append(getPublicDigest(customFields.toString()).trim());
			output.append("<input type=\"hidden\" name=\"orderPage_signaturePublic\" value=\""
					+ getPublicDigest(dataToSign.toString()) + "\">\n");
			output.append("<input type=\"hidden\" name=\"orderPage_signedFields\" value=\"" + customFields.toString() + "\">\n");

			return "<form action='" + ipgURL + "' method='POST'>" + output.toString() + "</form>";
		} catch (Exception e) {
			log.error("ERROR ", e);
			return "";
		}
	}

	public boolean verifySignature(String data, String signature) {
		if (data == null || signature == null) {
			return false;
		}
		try {
			String pub = getSharedSecret();
			BASE64Encoder encoder = new BASE64Encoder();
			Mac sha1Mac = Mac.getInstance("HmacSHA1");
			SecretKeySpec publicKeySpec = new SecretKeySpec(pub.getBytes(), "HmacSHA1");
			sha1Mac.init(publicKeySpec);
			byte[] publicBytes = sha1Mac.doFinal(data.getBytes());
			String publicDigest = encoder.encodeBuffer(publicBytes);
			publicDigest = publicDigest.replaceAll("[\r\n\t]", "");
			return signature.equals(publicDigest);
		} catch (Exception e) {
			log.error("ERROR ", e);
			return false;
		}
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getSharedSecret() {
		return sharedSecret;
	}

	public void setSharedSecret(String sharedSecret) {
		this.sharedSecret = sharedSecret;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

}
