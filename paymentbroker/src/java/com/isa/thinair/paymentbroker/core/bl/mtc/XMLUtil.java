package com.isa.thinair.paymentbroker.core.bl.mtc;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.IOUtils;

/**
 * @todo to be moved to a common place,
 * @author byorn
 * 
 */
public class XMLUtil {

	public static MTCXMLResponse getResponse(String responseString) throws JAXBException {
		MTCXMLResponse r = new MTCXMLResponse();
		/*
		 * StringBuffer s = new StringBuffer("<?xml version=\"1.0\" ?>"+
		 * "<response xmlns=\"http://www.isaaviation.com/thinair/webservices/api/paymentgateway\"" +
		 * " orderID=\"test\" />");
		 */

		String s = responseString;

		JAXBContext jc = JAXBContext.newInstance(r.getClass());
		Unmarshaller unm = jc.createUnmarshaller();
		r = (MTCXMLResponse) unm.unmarshal(IOUtils.toInputStream(s.toString()));

		return r;

	}

}
