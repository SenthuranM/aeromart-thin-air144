package com.isa.thinair.paymentbroker.core.persistence.hibernate;

import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.OTHER_CONTRIES;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.sql.DataSource;

import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.isa.thinair.airreservation.api.dto.PaymentMethodDetailsDTO;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.TempPaymentInconsistentStatusHistoryTypes;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.TempPaymentTnxTypes;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.CountryPaymentCardBehaviourDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.core.persistence.dao.PaymentBrokerDAO;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

/**
 * BHive: PaymentBroker module
 * 
 * @author Srikantha
 * 
 *         PaymentBrokerDAOImpl is the PaymentBrokerDAO implementatiion for hibernate
 * @isa.module.dao-impl dao-name="PaymentBrokerDAO"
 */
public class PaymentBrokerDAOImpl extends PlatformHibernateDaoSupport implements PaymentBrokerDAO {

	private Log log = LogFactory.getLog(PaymentBrokerDAOImpl.class);

	/*
	 * by kasun.
	 */
	public int composeTransaction(Map mapResult, int temporyPaymentId, String serviceType, String paymentGatewayName) {

		CreditCardTransaction transaction = new CreditCardTransaction();

		String transactionTimestamp = "";
		String transactionRefCccompany = "";
		int transactionResultCode = 0;
		String aidCccompnay = "";
		String avsResult = "";
		int resultCode = 0;
		String transactionId = "";
		String resultCodeString = "";
		int request = 0;
		String errorSpecification = "";
		String transactionReference = "";
		String tempReferenceNum = "";
		String cvvResult = "";

		if (mapResult.containsKey("_timestamp"))
			transactionTimestamp = castToString(mapResult.get("_timestamp"));
		if (mapResult.containsKey("transactionnr"))
			transactionRefCccompany = castToString(mapResult.get("transactionnr"));

		if (mapResult.containsKey("_rc"))
			transactionResultCode = Integer.parseInt(castToString(mapResult.get("_rc")));

		if (mapResult.containsKey("aid"))
			aidCccompnay = castToString(mapResult.get("aid"));

		if (mapResult.containsKey("avs_result"))
			avsResult = castToString(mapResult.get("avs_result"));

		if (mapResult.containsKey("rc"))
			resultCode = Integer.parseInt(castToString(mapResult.get("rc")));

		if (mapResult.containsKey("_transaction"))
			transactionId = castToString(mapResult.get("_transaction"));

		if (mapResult.containsKey("rc_str"))
			resultCodeString = castToString(mapResult.get("rc_str"));

		if (mapResult.containsKey("_req_ms"))
			request = Integer.parseInt(castToString(mapResult.get("_req_ms")));

		if (mapResult.containsKey("err_spec"))
			errorSpecification = errorSpecification + "|err_spec=" + castToString(mapResult.get("err_spec"));

		if (mapResult.containsKey("_err_spec"))
			errorSpecification = errorSpecification + "|_err_spec=" + castToString(mapResult.get("_err_spec"));

		if (mapResult.containsKey("transref"))
			transactionReference = castToString(mapResult.get("transref"));

		if (mapResult.containsKey("referencenum"))
			tempReferenceNum = castToString(mapResult.get("referencenum"));

		if (mapResult.containsKey("cvv_result"))
			cvvResult = castToString(mapResult.get("cvv_result"));

		PaymentBrokerUtils.debug(log, "=======================================");
		PaymentBrokerUtils.debug(log, "T_CCARD_PAYMENT_STATUS Insertion Values");
		PaymentBrokerUtils.debug(log, "transactionTimestamp:       " + transactionTimestamp);
		PaymentBrokerUtils.debug(log, "transactionRefCccompany:    " + transactionRefCccompany);
		PaymentBrokerUtils.debug(log, "transactionResultCode:      " + transactionResultCode);
		PaymentBrokerUtils.debug(log, "aidCccompnay:               " + aidCccompnay);
		PaymentBrokerUtils.debug(log, "avsResult:                  " + avsResult);
		PaymentBrokerUtils.debug(log, "resultCode:                 " + resultCode);
		PaymentBrokerUtils.debug(log, "transactionId:              " + transactionId);
		PaymentBrokerUtils.debug(log, "resultCodeString:           " + resultCodeString);
		PaymentBrokerUtils.debug(log, "request:                    " + request);
		PaymentBrokerUtils.debug(log, "errorSpecification:         " + errorSpecification);
		PaymentBrokerUtils.debug(log, "transactionReference:       " + transactionReference);
		PaymentBrokerUtils.debug(log, "tempReferenceNum:           " + tempReferenceNum);
		PaymentBrokerUtils.debug(log, "cvvResult:                  " + cvvResult);
		PaymentBrokerUtils.debug(log, "=======================================");

		transaction.setTransactionTimestamp(transactionTimestamp);
		transaction.setTransactionRefCccompany(transactionRefCccompany);
		transaction.setTransactionResultCode(transactionResultCode);
		transaction.setAidCccompnay(aidCccompnay);
		transaction.setAvsResult(avsResult);
		transaction.setResultCode(resultCode);
		transaction.setTransactionId(transactionId);
		transaction.setResultCodeString(resultCodeString);
		transaction.setRequest(request);
		transaction.setErrorSpecification(errorSpecification);
		transaction.setTransactionReference(transactionReference);
		transaction.setTempReferenceNum(tempReferenceNum);
		transaction.setCvvResult(cvvResult);
		transaction.setTemporyPaymentId(new Integer(temporyPaymentId));
		transaction.setServiceType(serviceType);
		transaction.setPaymentGatewayConfigurationName(paymentGatewayName);

		hibernateSaveOrUpdate(transaction);
		return transaction.getTransactionRefNo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isa.thinair.paymentbroker.core.persistence.dao.PaymentBrokerDAO# saveTransaction
	 * (com.isa.thinair.paymentbroker.api.model.CreditCardTransaction)
	 */
	public void saveTransaction(CreditCardTransaction transactionNM) {
		hibernateSaveOrUpdate(transactionNM);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isa.thinair.paymentbroker.core.persistence.dao.PaymentBrokerDAO# loadTransaction(int)
	 */
	public CreditCardTransaction loadTransaction(int transactionReferenceNumber) {
		return (CreditCardTransaction) get(CreditCardTransaction.class,
				new Integer(transactionReferenceNumber));
	}

	/**
	 * Get DataSource
	 * 
	 * @return
	 */
	private DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	@SuppressWarnings("unchecked")
	public Map<Integer, CreditCardTransaction> loadTransactionMap(String pnr) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		String query = " SELECT ccs.tpt_id, ccs.aid_cccompnay  FROM t_ccard_payment_status ccs,  t_temp_payment_tnx tpt  WHERE ccs.tpt_id = tpt.tpt_id AND tpt.status = 'RS' AND tpt.pnr = ?";
		Map<Integer, CreditCardTransaction> tptCardMap = (Map<Integer, CreditCardTransaction>) jdbcTemplate.query(query,
				new String[] { pnr }, new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						CreditCardTransaction ctnx;
						Map<Integer, CreditCardTransaction> tptCardMap = new HashMap<Integer, CreditCardTransaction>();
						if (rs != null) {
							while (rs.next()) {
								ctnx = new CreditCardTransaction();
								ctnx.setAidCccompnay(rs.getString("aid_cccompnay"));
								ctnx.setTemporyPaymentId(new Integer(rs.getString("tpt_id")));
								tptCardMap.put(ctnx.getTemporyPaymentId(), ctnx);
							}

						}

						return tptCardMap;
					}
				});

		return tptCardMap;
	}
	
	public boolean isDuplicateTransactionExists(String tnxId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		String query = " SELECT ccs.*  FROM t_ccard_payment_status ccs ";
		if (tnxId != null) {
			query += " WHERE ccs.transaction_id =  '" + tnxId + "'";
		}
		return (Boolean) jdbcTemplate.query(query, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null) {
					while (rs.next()) {
						return true;
					}
				}

				return false;
			}
		});
	}

	public boolean isDuplicateBankTransactionExists(String bankTnxId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		String query = " SELECT ccs.*  FROM t_ccard_payment_status ccs ";
		if (bankTnxId != null) {
			query += " WHERE ccs.aid_cccompnay =  '" + bankTnxId + "'";
		}
		return (Boolean) jdbcTemplate.query(query, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null) {
					while (rs.next()) {
						return true;
					}
				}

				return false;
			}
		});
	}

	private String castToString(Object obj) {
		String str = (obj == null) ? "" : obj.toString();
		return str;

	}

	public CreditCardTransaction loadTransactionByReference(String transactionReference) {

		CreditCardTransaction creditCardTransaction = null;
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		StringBuffer query = new StringBuffer();
		query.append("SELECT * ");
		query.append("FROM t_ccard_payment_status CC, t_temp_payment_tnx TTPT ");
		query.append("WHERE TTPT.pnr = '").append(transactionReference).append("' ");
		query.append("AND TTPT.tpt_id = CC.tpt_id ");
		query.append("ORDER BY CC.TRANSACTION_TIMESTAMP desc ");

		if (log.isDebugEnabled())
			log.debug("Query to retrive credit card payment details by pnr(transaction refference): " + query.toString());

		creditCardTransaction = (CreditCardTransaction) jdbcTemplate.query(query.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				CreditCardTransaction creditCardTransaction = null;
				if (rs != null && rs.next()) {

					creditCardTransaction = new CreditCardTransaction();
					creditCardTransaction.setAidCccompnay(rs.getString("AID_CCCOMPNAY"));
					creditCardTransaction.setAvsResult(rs.getString("AVS_RESULT"));
					creditCardTransaction.setComments(rs.getString("COMMENTS"));
					creditCardTransaction.setCvvResult(rs.getString("CVV_RESULT"));
					creditCardTransaction.setDeferredResponseText(rs.getString("DEFERRED_RESPONSE_TEXT"));
					creditCardTransaction.setErrorSpecification(rs.getString("ERROR_SPECIFICATION"));
					creditCardTransaction.setGatewayPaymentID(rs.getString("GATEWAY_PAYMENT_ID"));
					creditCardTransaction.setIntermediateRequest1(rs.getString("INTERMEDIATE_REQUEST_1"));
					creditCardTransaction.setIntermediateRequest2(rs.getString("INTERMEDIATE_REQUEST_2"));
					creditCardTransaction.setIntermediateResponse1(rs.getString("INTERMEDIATE_RESPONSE_1"));
					creditCardTransaction.setIntermediateResponse2(rs.getString("INTERMEDIATE_RESPONSE_2"));
					creditCardTransaction.setPaymentGatewayConfigurationName(rs.getString("GATEWAY_NAME"));
					creditCardTransaction.setRequest(rs.getInt("REQUEST"));
					creditCardTransaction.setRequestText(rs.getString("REQUEST_TEXT"));
					creditCardTransaction.setResponseText(rs.getString("RESPONSE_TEXT"));
					creditCardTransaction.setResponseTime(rs.getLong("RESPONSETIME"));
					creditCardTransaction.setResultCode(rs.getInt("RESULT_CODE"));
					creditCardTransaction.setResultCodeString(rs.getString("RESULT_CODE_STRING"));
					creditCardTransaction.setServiceType(rs.getString("SERVICE_TYPE"));
					creditCardTransaction.setTemporyPaymentId(rs.getInt("TPT_ID"));
					creditCardTransaction.setTempReferenceNum(rs.getString("NM_TRANSACTION_REFERENCE"));
					creditCardTransaction.setTransactionId(rs.getString("TRANSACTION_ID"));
					creditCardTransaction.setTransactionRefCccompany(rs.getString("TRANSACTION_REF_CCCOMPANY"));
					creditCardTransaction.setTransactionReference(rs.getString("TRANSACTION_REFERENCE"));
					creditCardTransaction.setTransactionRefNo(rs.getInt("TRANSACTION_REF_NO"));
					creditCardTransaction.setTransactionResultCode(rs.getInt("TRANSACTION_RESULT_CODE"));
					creditCardTransaction.setTransactionTimestamp(rs.getString("TRANSACTION_TIMESTAMP"));
					creditCardTransaction.setVersion(rs.getLong("VERSION"));

				}
				return creditCardTransaction;
			}
		});
		return creditCardTransaction;
	}

	public CreditCardTransaction loadTransactionByTempPayId(int temporyPayId) {

		ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();
		Object[] params = { new Integer(temporyPayId), bundle.getString("SERVICETYPE_QUERY") };
		Collection colCreditCardTransaction = find(
				"from CreditCardTransaction where temporyPaymentId = ? and serviceType != ? order by transactionTimestamp desc ", params, CreditCardTransaction.class);

		Iterator itColCreditCardTransaction = colCreditCardTransaction.iterator();
		CreditCardTransaction creditCardTransaction = null;
		if (itColCreditCardTransaction.hasNext()) {
			creditCardTransaction = (CreditCardTransaction) itColCreditCardTransaction.next();
		}

		return creditCardTransaction;
	}

	public CreditCardTransaction loadTransactionByMerchantTransId(String merchantTnxId) {
		Object[] params = { merchantTnxId };
		Collection colCreditCardTransaction = find(
				"from CreditCardTransaction where tempReferenceNum = ? order by transactionTimestamp desc ", params, CreditCardTransaction.class);

		Iterator itColCreditCardTransaction = colCreditCardTransaction.iterator();
		CreditCardTransaction creditCardTransaction = null;
		if (itColCreditCardTransaction.hasNext()) {
			creditCardTransaction = (CreditCardTransaction) itColCreditCardTransaction.next();
		}

		return creditCardTransaction;
	}

	/**
	 * Added by manjula The payment gateway available for a currency is retrieved
	 * 
	 * @param ipgPaymentOptionDTO
	 * @return List
	 */
	public List getPaymentGateway(IPGPaymentOptionDTO ipgPaymentOptionDTO) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		String query = "SELECT PG.PAYMENT_GATEWAY_ID, PG.PROVIDER_CODE, PG.PROVIER_NAME, PG.DEFAULT_CURRENCY_CODE, PG.OHD_RELEASE_TIME, PGC.CURRENCY_CODE, PG.ALIAS_PAYMENT_AMOUNT_IN_BASE "
				+ "FROM T_PAYMENT_GATEWAY PG "
				+ "LEFT JOIN T_PAYMENT_GATEWAY_CURRENCY PGC ON PGC.PAYMENT_GATEWAY_ID = PG.PAYMENT_GATEWAY_ID "
				+ "WHERE PGC.CURRENCY_CODE = ? AND PG.STATUS= 'ACT' ";
		List list = (List) jdbcTemplate.query(query, new String[] { ipgPaymentOptionDTO.getSelCurrency() },
				new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						List<IPGPaymentOptionDTO> paymentList = new ArrayList<IPGPaymentOptionDTO>();

						if (rs != null) {
							while (rs.next()) {

								IPGPaymentOptionDTO ipgPayDet = new IPGPaymentOptionDTO();
								ipgPayDet.setPaymentGateway(rs.getInt("PAYMENT_GATEWAY_ID"));
								ipgPayDet.setProviderCode(rs.getString("PROVIDER_CODE"));
								ipgPayDet.setProviderName(rs.getString("PROVIER_NAME"));
								ipgPayDet.setBaseCurrency(rs.getString("DEFAULT_CURRENCY_CODE"));
								ipgPayDet.setSelCurrency(rs.getString("CURRENCY_CODE"));
								ipgPayDet.setOnholdReleaseTime(rs.getInt("OHD_RELEASE_TIME"));
								ipgPayDet.setAliasBasePaymentAmount(rs.getBigDecimal("ALIAS_PAYMENT_AMOUNT_IN_BASE"));
								paymentList.add(ipgPayDet);
							}

						}

						return paymentList;
					}
				});
		return list;

	}

	public Set<String> getPaymentMethodNames(PaymentMethodDetailsDTO paymentDetails) throws ModuleException{
		boolean isOndWisePayment = AppSysParamsUtil.getIsONDWisePayment();
		if(isOndWisePayment){
			return getPaymentsOptionsByFirstOndSegment(paymentDetails.getFirstSegmentONDCode(),paymentDetails.isEnableOffline());
		}else{
			return getPaymentsMethodsIp(paymentDetails);
		}
	}

	private Set<String> getPaymentsOptionsByFirstOndSegment(String ondCode,boolean isEnableOffline) throws ModuleException{

		String originCountryCode = ondCode == null ? OTHER_CONTRIES : PaymentBrokerUtils.getCommonServiceBD()
				.getCountryCodeByAirportCode(ondCode.split("/")[0]);

		String destCountryCode = ondCode == null ? OTHER_CONTRIES : PaymentBrokerUtils.getCommonServiceBD()
				.getCountryCodeByAirportCode(ondCode.split("/")[1]);

		Set<String> cardTypes = new HashSet<String>();
		NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(new JdbcTemplate(
				ReservationModuleUtils.getDatasource()).getDataSource());

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT cardType.css_class_name AS CARD_NAME ");
		sql.append("FROM t_payment_gateway gateway ");
		sql.append("INNER JOIN t_payment_gateway_card_type cardType ");
		sql.append("ON gateway.payment_gateway_id = cardType.payment_gateway_id ");
		sql.append("INNER JOIN t_payment_country_country_card countryCard ");
		sql.append("ON countrycard.payment_gateway_card_type_id = cardtype.payment_gateway_card_type_id ");
		sql.append("WHERE gateway.status                        = 'ACT' ");
		sql.append("AND cardType.css_class_name                IS NOT NULL ");
		sql.append("AND (countryCard.origin_country_code        = :originCountryCode ");
		sql.append("OR countryCard.origin_country_code          = 'OT') ");
		sql.append("AND (countryCard.destination_country_code   = :destCountryCode ");
		sql.append("OR countryCard.destination_country_code     = 'OT') ");
		sql.append("AND countryCard.status = 'ACT' ");

		if (!isEnableOffline) {
			sql.append("AND gateway.Is_Offline            = 'N' ");
		}

		sql.append("ORDER BY cardType.group_key, cardType.group_order ");

		Map<String, String> parameters = new HashMap();
		parameters.put("originCountryCode", originCountryCode);
		parameters.put("destCountryCode", destCountryCode);

		jdbcTemplate.query(sql.toString(), parameters, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null) {
					while (rs.next()) {
						cardTypes.add(rs.getString("CARD_NAME"));
					}
				}
				return cardTypes;
			}
		});

		return cardTypes;
	}

	public Integer getLatestTempTransactionID(String PNR, String fqIPGConfigurationName, String transactionStatus) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		String query = "SELECT TT.TPT_ID FROM T_TEMP_PAYMENT_TNX TT INNER JOIN T_CCARD_PAYMENT_STATUS CC "
				+ "ON TT.TPT_ID=CC.TPT_ID WHERE TT.PNR=? and CC.GATEWAY_NAME=? and TT.STATUS=? ORDER BY TT.TPT_ID desc";

		return (Integer) jdbcTemplate.query(query, new String[] { PNR, fqIPGConfigurationName ,transactionStatus}, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer tmpId = null;

				if (rs != null) {
					while (rs.next()) {
						tmpId = rs.getInt("TPT_ID");
						break;
					}
				}
				return tmpId;
			}
		});
	}

	@SuppressWarnings("unchecked")
	public List<IPGPaymentOptionDTO> getPaymentGateways(IPGPaymentOptionDTO ipgPaymentOptionDTO) throws ModuleException {
		return getPaymentGatewaysOptions(ipgPaymentOptionDTO);
	}
	
	public List<IPGPaymentOptionDTO> getPaymentGatewaysOptions(IPGPaymentOptionDTO ipgPaymentOptionDTO) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String moduleCode = ipgPaymentOptionDTO.getModuleCode();
		String query = "SELECT PG.PAYMENT_GATEWAY_ID, PG.IS_SAVE_CARD, PG.PROVIDER_CODE, PG.PROVIER_NAME, PG.DEFAULT_CURRENCY_CODE, PG.DESCRIPTION, PG.CVV_OPTION, PG.IS_DEFAULT_PG, PG.IS_MODIFICATION_ALLOWED , PGC.CURRENCY_CODE, PG.OHD_RELEASE_TIME ,PG.ENTITY_CODE, PG.ALIAS_PAYMENT_AMOUNT_IN_BASE "
				+ "FROM T_PAYMENT_GATEWAY PG, T_PAYMENT_GATEWAY_CURRENCY PGC where PGC.PAYMENT_GATEWAY_ID = PG.PAYMENT_GATEWAY_ID and "
				+ "PGC.CURRENCY_CODE = ? AND PG.STATUS= 'ACT' ";
		if (!ipgPaymentOptionDTO.isOfflinePayment()) {
			query += " AND PG.IS_OFFLINE = 'N' ";
		}
		
		if(ipgPaymentOptionDTO.getEntityCode() != null) {
			query += " AND PG.ENTITY_CODE = '"+ipgPaymentOptionDTO.getEntityCode()+"' ";
		}else {
			query += " AND PG.ENTITY_CODE = 'E_RES' "; //default entity
		}
		if (ipgPaymentOptionDTO.getPaymentGateway() > 0) {
			query += " AND PG.PAYMENT_GATEWAY_ID = " + ipgPaymentOptionDTO.getPaymentGateway();
		}else if (moduleCode != null && !moduleCode.trim().isEmpty()) {
			query += " AND PG.MODULE_CODE = '" + moduleCode +"' ";
		}
		query += " UNION "
				+ "SELECT PG.PAYMENT_GATEWAY_ID,PG.IS_SAVE_CARD, PG.PROVIDER_CODE, PG.PROVIER_NAME, PG.DEFAULT_CURRENCY_CODE, PG.DESCRIPTION, PG.CVV_OPTION, PG.IS_DEFAULT_PG, PG.IS_MODIFICATION_ALLOWED , PGC.CURRENCY_CODE, PG.OHD_RELEASE_TIME,PG.ENTITY_CODE, PG.ALIAS_PAYMENT_AMOUNT_IN_BASE  "
				+ "FROM T_PAYMENT_GATEWAY PG, T_PAYMENT_GATEWAY_CURRENCY PGC where PGC.PAYMENT_GATEWAY_ID = PG.PAYMENT_GATEWAY_ID "
				+ "AND PG.STATUS= 'ACT' AND PGC.CURRENCY_CODE=PG.DEFAULT_CURRENCY_CODE ";
		if (!ipgPaymentOptionDTO.isOfflinePayment()) {
			query += " AND PG.IS_OFFLINE = 'N' ";
		}
		if (ipgPaymentOptionDTO.getPaymentGateway() > 0) {
			query += " AND PG.PAYMENT_GATEWAY_ID = " + ipgPaymentOptionDTO.getPaymentGateway();
		}else if(moduleCode != null && !moduleCode.trim().isEmpty()) {
			query += " AND PG.MODULE_CODE = '" + moduleCode +"' ";
		}
		query += " AND PG.PAYMENT_GATEWAY_ID NOT IN "
				+ "( "
				+ "SELECT PG.PAYMENT_GATEWAY_ID "
				+ "FROM T_PAYMENT_GATEWAY PG, T_PAYMENT_GATEWAY_CURRENCY PGC where PGC.PAYMENT_GATEWAY_ID = PG.PAYMENT_GATEWAY_ID and "
				+ "PGC.CURRENCY_CODE = ? AND PG.STATUS= 'ACT' AND PG.MODULE_CODE = ?) ";
		List<IPGPaymentOptionDTO> list = (List<IPGPaymentOptionDTO>) jdbcTemplate.query(
				query,
				new String[] { ipgPaymentOptionDTO.getSelCurrency(), ipgPaymentOptionDTO.getSelCurrency(),
						ipgPaymentOptionDTO.getModuleCode(), }, new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						List<IPGPaymentOptionDTO> paymentList = new ArrayList<IPGPaymentOptionDTO>();

						if (rs != null) {
							while (rs.next()) {
								IPGPaymentOptionDTO ipgPayDet = new IPGPaymentOptionDTO();
								ipgPayDet.setPaymentGateway(rs.getInt("PAYMENT_GATEWAY_ID"));
								ipgPayDet.setProviderCode(rs.getString("PROVIDER_CODE"));
								ipgPayDet.setProviderName(rs.getString("PROVIER_NAME"));
								ipgPayDet.setBaseCurrency(rs.getString("DEFAULT_CURRENCY_CODE"));
								ipgPayDet.setDescription(rs.getString("DESCRIPTION"));
								ipgPayDet.setSelCurrency(rs.getString("CURRENCY_CODE"));
								ipgPayDet.setCvvOption(rs.getInt("CVV_OPTION"));
								ipgPayDet.setIsDefault(rs.getString("IS_DEFAULT_PG").trim());
								ipgPayDet.setIsModificationAllow(rs.getString("IS_MODIFICATION_ALLOWED").trim());
								ipgPayDet.setOnholdReleaseTime(rs.getInt("OHD_RELEASE_TIME"));
								ipgPayDet.setSaveCard((rs.getString("IS_SAVE_CARD").equalsIgnoreCase("Y") ? true : false));
								ipgPayDet.setEntityCode(rs.getString("ENTITY_CODE"));
								ipgPayDet.setAliasBasePaymentAmount(rs.getBigDecimal("ALIAS_PAYMENT_AMOUNT_IN_BASE"));
								paymentList.add(ipgPayDet);
							}

						}

						return paymentList;
					}
				});
		return list;
	}

	private Set<String> getPaymentsMethodsIp(PaymentMethodDetailsDTO paymentDetails){
		boolean enableOffline = paymentDetails.isEnableOffline();
		String countryCode = paymentDetails.getCountryCode();

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		StringBuffer query = new StringBuffer();
		query.append("SELECT UNIQUE pgct.css_class_name ");
		query.append("FROM t_payment_gateway pg, ");
		query.append("t_payment_gateway_card_type pgct, ");
		query.append("t_payment_country_card_type pcct ");
		query.append("WHERE pg.payment_gateway_id = pgct.payment_gateway_id ");
		query.append("AND pg.status = 'ACT' ");
		query.append("AND module_code = 'IBE' ");
		query.append("AND pgct.css_class_name IS NOT NULL ");
		query.append("AND pcct.payment_gateway_card_type_id = pgct.payment_gateway_card_type_id ");
		query.append("AND pcct.ibe_card_status = 'ACT' ");
		query.append("AND (pcct.country_code = '").append(countryCode).append("' ");
		query.append("OR pcct.country_code = 'OT') ");
		if (!enableOffline) {
			query.append("AND pg.IS_OFFLINE = 'N' ");
		}

		if (log.isDebugEnabled())
			log.debug("Query to retrive payment methods: " + query.toString());

		Object paymentMethods = jdbcTemplate.query(query.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Set<String> paymentMethods = new HashSet<String>();
				if (rs != null) {
					while (rs.next()) {
						paymentMethods.add(rs.getString("css_class_name"));
					}
				}
				return paymentMethods;
			}
		});

		if (log.isDebugEnabled())
			log.debug("Applicable payment methods:" + paymentMethods.toString());

		return (Set<String>) paymentMethods;
	}

	/**
	 * Added by manjula The payment gateway available for base currency is retrieved
	 * 
	 * @param
	 * @return ipgPaymentOptionDTO
	 */
	public List getDefaultCurrencyPaymentGateway(Integer ipgId) {
		return getDefaultCurrencyPaymentGateway(null, ipgId, false);
	}

	/**
	 * Method change functionality of inactive payment gateway refund TODO :Add Payment gateway active status parameter
	 * 
	 * Added by manjula The payment gateway available for base currency is retrieved
	 * 
	 * @param
	 * @return ipgPaymentOptionDTO
	 */
	public List<IPGPaymentOptionDTO> getDefaultCurrencyPaymentGateway(String modulecode, Integer ipgId, boolean isOfflinePayment) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		String paymentGatewayStatus = "";
		if (modulecode != null) {
			paymentGatewayStatus = " PG.STATUS= 'ACT' ";
		}

		if (ipgId != null) {
			paymentGatewayStatus = "";
		}
		
		String query = "SELECT PG.PAYMENT_GATEWAY_ID, PG.IS_SAVE_CARD, PG.DEFAULT_CURRENCY_CODE, PG.PROVIDER_CODE, PG.DESCRIPTION, PG.PROVIER_NAME, PG.CVV_OPTION, PG.IS_MODIFICATION_ALLOWED, PG.IS_DEFAULT_PG, PG.OHD_RELEASE_TIME, PG.ALIAS_PAYMENT_AMOUNT_IN_BASE FROM T_PAYMENT_GATEWAY PG WHERE "
				+ paymentGatewayStatus;
		
		if (ipgId != null && paymentGatewayStatus.isEmpty()) {
			query += " PG.PAYMENT_GATEWAY_ID=" + ipgId;
		} else if (ipgId != null && !paymentGatewayStatus.isEmpty()) {
			query += " AND PG.PAYMENT_GATEWAY_ID=" + ipgId;
		}	
		
		if ((modulecode != null && !paymentGatewayStatus.isEmpty() && ipgId != null)
				|| (modulecode != null && ipgId == null && !paymentGatewayStatus.isEmpty())) {
			query += " AND  PG.module_code= '" + modulecode + "'";
		} else if (modulecode != null && ipgId == null && paymentGatewayStatus.isEmpty()) {
			query += " PG.module_code= '" + modulecode + "'";
		}
		
		if (!isOfflinePayment) {
			query += "AND PG.IS_OFFLINE= 'N' ";
		}

		return  (List<IPGPaymentOptionDTO>) jdbcTemplate.query(query, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				IPGPaymentOptionDTO ipgPayDet;
				List<IPGPaymentOptionDTO> paymentList = new ArrayList<IPGPaymentOptionDTO>();
				if (rs != null) {
					while (rs.next()) {
						ipgPayDet = new IPGPaymentOptionDTO();
						ipgPayDet.setPaymentGateway(rs.getInt("PAYMENT_GATEWAY_ID"));
						ipgPayDet.setBaseCurrency(rs.getString("DEFAULT_CURRENCY_CODE"));
						ipgPayDet.setProviderCode(rs.getString("PROVIDER_CODE"));
						ipgPayDet.setDescription(rs.getString("DESCRIPTION"));
						ipgPayDet.setProviderName(rs.getString("PROVIER_NAME"));
						ipgPayDet.setCvvOption(rs.getInt("CVV_OPTION"));
						ipgPayDet.setIsDefault(rs.getString("IS_DEFAULT_PG").trim());
						ipgPayDet.setIsModificationAllow(rs.getString("IS_MODIFICATION_ALLOWED").trim());
						ipgPayDet.setOnholdReleaseTime(rs.getInt("OHD_RELEASE_TIME"));
						ipgPayDet.setSaveCard((rs.getString("IS_SAVE_CARD").equalsIgnoreCase("Y") ? true : false));
						ipgPayDet.setAliasBasePaymentAmount(rs.getBigDecimal("ALIAS_PAYMENT_AMOUNT_IN_BASE"));
						paymentList.add(ipgPayDet);
					}
				}

				return paymentList;
			}
		});
		
	}

	/**
	 * Method change functionality of inactive payment gateway refund
	 * 
	 * TODO :Add Payment gateway active status parameter
	 */
	public boolean isPaymentGatewaySupportsCurrency(Integer ipgId, String currencyCode) {

		String paymentGatewayStatus = "";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		if (ipgId == null || ipgId <= 0) {
			paymentGatewayStatus = " and pg.status = 'ACT' ";
		}
		/*
		 * String query = "SELECT pg.payment_gateway_id FROM t_payment_gateway pg, t_payment_gateway_currency pgc" +
		 * " WHERE pg.payment_gateway_id = pgc.payment_gateway_id and pgc.currency_code = '" + currencyCode +
		 * "' and pg.status = 'ACT' and pg.payment_gateway_id=" + ipgId.toString();
		 */
		String query = "SELECT pg.payment_gateway_id FROM t_payment_gateway pg, t_payment_gateway_currency pgc"
				+ " WHERE pg.payment_gateway_id = pgc.payment_gateway_id and pgc.currency_code = '" + currencyCode
				+ "' and pg.payment_gateway_id=" + ipgId.toString() + paymentGatewayStatus;

		Boolean found = (Boolean) jdbcTemplate.query(query, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				boolean found = false;
				if (rs != null) {
					while (rs.next()) {
						found = true;
						break;
					}

				}

				return new Boolean(found);
			}
		});

		return found.booleanValue();
	}

	public boolean isPaymentGatewaySupportedCurrency(String currencyCode, ApplicationEngine module) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		StringBuilder query = new StringBuilder();
		query.append("SELECT PGC.CURRENCY_CODE FROM T_PAYMENT_GATEWAY_CURRENCY PGC,T_PAYMENT_GATEWAY PG ");
		query.append("WHERE PGC.PAYMENT_GATEWAY_ID = PG.PAYMENT_GATEWAY_ID AND PG.STATUS = 'ACT' ");
		query.append("AND PG.MODULE_CODE = '" + module + "' ");
		query.append("AND PGC.CURRENCY_CODE = '" + currencyCode + "'");

		Boolean found = (Boolean) jdbcTemplate.query(query.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				boolean found = false;
				if (rs != null) {
					while (rs.next()) {
						found = true;
						break;
					}
				}
				return new Boolean(found);
			}
		});
		return found.booleanValue();
	}

	public String getPaymentGatewayNameForCCTransaction(Integer tnxId, boolean isOwnTnx) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String query = null;

		if (isOwnTnx) {
			query = "select cs.gateway_name from t_ccard_payment_status cs, t_res_pcd pcd, t_pax_transaction tnx "
					+ "where tnx.txn_id = pcd.txn_id and pcd.transaction_ref_no = cs.transaction_ref_no and tnx.txn_id="
					+ tnxId.toString();
		} else {
			query = "SELECT cs.gateway_name FROM t_ccard_payment_status cs, t_res_pcd pcd,  t_pax_ext_carrier_transactions tnx "
					+ "WHERE tnx.pax_ext_carrier_txn_id = pcd.ext_txn_id AND pcd.transaction_ref_no = cs.transaction_ref_no "
					+ "AND tnx.pax_ext_carrier_txn_id=" + tnxId.toString();
		}
		String ipgName = (String) jdbcTemplate.query(query, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				String ipgName = null;
				if (rs != null) {
					while (rs.next()) {
						ipgName = rs.getString("gateway_name");
						break;
					}
				}
				return ipgName;
			}
		});

		return ipgName;
	}

	public String getPaymentGatewayNameForLCCCCTnx(Integer tptId) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String query = "SELECT GATEWAY_NAME FROM T_CCARD_PAYMENT_STATUS T1 "
				+ "JOIN T_TEMP_PAYMENT_TNX T2 ON T1.TPT_ID = T2.TPT_ID " + "AND T2.TPT_ID = " + tptId.toString();

		String ipgName = (String) jdbcTemplate.query(query, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				String ipgName = null;
				if (rs != null) {
					while (rs.next()) {
						ipgName = rs.getString("GATEWAY_NAME");
						break;
					}
				}
				return ipgName;
			}
		});

		return ipgName;
	}

	/**
	 * Method Check weather Card Bin no belongs to 30 accepted range
	 */
	public boolean isBinPrefixExists(String ccNo) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String query = "select cc_validation_id, payment_gateway_id from t_cc_validation_no where bin_number like '"
				+ ccNo.substring(0, 6) + "%" + "'" + "and status ='ACT'";

		Boolean found = (Boolean) jdbcTemplate.query(query, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				boolean found = false;
				if (rs != null) {
					while (rs.next()) {
						found = true;
						break;
					}

				}

				return new Boolean(found);
			}
		});

		return found.booleanValue();
	}

	/**
	 * Currency Numeric code(4217 ISO standard) from currency code.
	 * 
	 * @param
	 * @return ipgPaymentOptionDTO
	 */
	public String getCurrencyNumericCode(String code, Integer ipgId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String query = "SELECT PGC.CURRENCY_NUMERIC_CODE FROM T_PAYMENT_GATEWAY_CURRENCY PGC WHERE";

		if (code != null)
			query += " PGC.CURRENCY_CODE= '" + code + "'";
		if (ipgId != null)
			query += " AND  PGC.PAYMENT_GATEWAY_ID= '" + ipgId + "'";

		String numericCode = (String) jdbcTemplate.query(query, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				String numericCode = null;
				if (rs != null) {
					while (rs.next()) {
						numericCode = rs.getString("CURRENCY_NUMERIC_CODE");
						break;
					}
				}
				return numericCode;
			}
		});

		return numericCode;

	}

	/**
	 * Get Payment Gateway
	 * 
	 * @param paymentGatewayID
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<CardDetailConfigDTO> getPaymentGatewayCardConfigData(String paymentGatewayID) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String query = "SELECT FIELD_NAME, WHAT_TO_STORE,WHAT_TO_DISPLAY FROM T_PAYMENT_GATEWAY_CONFIG WHERE PAYMENT_GATEWAY_ID = '"
				+ paymentGatewayID + "' ";

		List<CardDetailConfigDTO> list = (List<CardDetailConfigDTO>) jdbcTemplate.query(query, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				CardDetailConfigDTO cardDetailConfigDTO;
				List<CardDetailConfigDTO> configList = new ArrayList<CardDetailConfigDTO>();
				if (rs != null) {
					while (rs.next()) {
						cardDetailConfigDTO = new CardDetailConfigDTO();
						cardDetailConfigDTO.setFieldName(rs.getString("FIELD_NAME"));
						cardDetailConfigDTO.setWhatToStore(rs.getString("WHAT_TO_STORE"));
						cardDetailConfigDTO.setWhatToDisplay(rs.getString("WHAT_TO_DISPLAY"));
						configList.add(cardDetailConfigDTO);
					}
				}
				return configList;
			}
		});

		return list;
	}

	/**
	 * 
	 * @param countryCode
	 * @param paymentGatewayIDs
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Collection getContryPaymentCardBehavior(String countryCode, Collection paymentGatewayIDs) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		if (countryCode == null || countryCode.trim().equals(""))
			countryCode = "OT";
		StringBuffer query = new StringBuffer();
		query.append("SELECT pgct.payment_gateway_id, \n");
		query.append("  pgct.card_type_id, \n");
		query.append("  pcct.ibe_gateway_behavior, \n");
		query.append("  pgct.card_display_name, \n");
		query.append("  ct.card_type, \n");
		query.append("  pgct.css_class_name, \n");
		query.append("  pcct.country_code, \n");
		query.append("  pgct.payment_gateway_card_type_id, \n");
		query.append("  pcct.ibe_card_status, \n");
		query.append("  pgct.i18n_message_key \n");
		query.append("FROM t_payment_country_card_type pcct, \n");
		query.append("  t_payment_gateway_card_type pgct, \n");
		query.append("  t_creditcard_type ct \n");
		query.append("WHERE pcct.country_code in('OT','");
		query.append(countryCode);
		query.append("') AND pgct.payment_gateway_card_type_id = pcct.payment_gateway_card_type_id \n");
		query.append("  AND ct.card_type_id = pgct.card_type_id \n");
		query.append("AND pcct.IBE_CARD_STATUS = 'ACT'");
		query.append("AND pgct.payment_gateway_id          IN (");
		query.append(Util.buildIntegerInClauseContent(paymentGatewayIDs));
		query.append(") ");
		query.append(" ORDER BY pgct.group_key, \n");
		query.append("  pgct.group_order");

		Collection<CountryPaymentCardBehaviourDTO> list = (Collection<CountryPaymentCardBehaviourDTO>) jdbcTemplate.query(
				query.toString(), new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						CountryPaymentCardBehaviourDTO cardBahaviuor;
						Map<Integer, CountryPaymentCardBehaviourDTO> allCardsMap = new LinkedHashMap<Integer, CountryPaymentCardBehaviourDTO>();
						String cssClassName = null;
						String cardName = null;
						String cardDisplayName = null;
						String dbCountryName = null;
						Integer paymentCardTypeID = null;
						if (rs != null) {
							while (rs.next()) {
								cardBahaviuor = new CountryPaymentCardBehaviourDTO();
								cardBahaviuor.setPaymentGateWayID(rs.getInt("payment_gateway_id"));
								cardBahaviuor.setCardType(rs.getInt("card_type_id"));
								cardBahaviuor.setBehaviour(rs.getString("ibe_gateway_behavior"));
								cardBahaviuor.setStatus(rs.getString("ibe_card_status"));
								cardName = rs.getString("card_type");
								cardDisplayName = rs.getString("card_display_name");
								cardBahaviuor.setCardDisplayName(cardDisplayName != null ? cardDisplayName : cardName);
								cssClassName = rs.getString("css_class_name");
								cardBahaviuor.setCssClassName(cssClassName != null ? cssClassName : cardName);
								cardBahaviuor.setI18nMsgKey(rs.getString("i18n_message_key"));
								dbCountryName = rs.getString("country_code");
								paymentCardTypeID = rs.getInt("payment_gateway_card_type_id");
								if (!allCardsMap.containsKey(paymentCardTypeID)) {
									allCardsMap.put(paymentCardTypeID, cardBahaviuor);
								} else {
									if (!"OT".equals(dbCountryName)) {
										allCardsMap.put(paymentCardTypeID, cardBahaviuor);
									}
								}
							}
						}

						return new ArrayList(allCardsMap.values());
					}
				});

		return list;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Collection<CountryPaymentCardBehaviourDTO> getONDWiseCountryCardBehavior(String ondCode, Collection paymentGatewayIDs)
			throws ModuleException {
		NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(new JdbcTemplate(
				ReservationModuleUtils.getDatasource()).getDataSource());

		String originCountryCode = ondCode == null ? OTHER_CONTRIES : PaymentBrokerUtils.getCommonServiceBD()
				.getCountryCodeByAirportCode(ondCode.split("/")[0]);

		String destCountryCode = ondCode == null ? OTHER_CONTRIES : PaymentBrokerUtils.getCommonServiceBD()
				.getCountryCodeByAirportCode(ondCode.split("/")[1]);

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT cardType.payment_gateway_id      AS payment_gateway_id, ");
		sql.append("  cardType.card_type_id                 AS card_type_id, ");
		sql.append("  cardType.css_class_name               AS css_class_name, ");
		sql.append("  cardType.payment_gateway_card_type_id AS payment_gateway_card_type_id, ");
		sql.append("  cardType.i18n_message_key             AS i18n_message_key, ");
		sql.append("  cardType.card_display_name            AS card_display_name, ");
		sql.append("  NULL                                  AS behavior, ");
		sql.append("  countryCard.origin_country_code       AS origin_country_code, ");
		sql.append("  countryCard.status                    AS status, ");
		sql.append("  creditType.card_type                  AS card_type ");
		sql.append("FROM t_payment_gateway_card_type cardType ");
		sql.append("INNER JOIN T_PAYMENT_COUNTRY_COUNTRY_CARD countryCard ");
		sql.append("ON countryCard.Payment_Gateway_Card_Type_Id = Cardtype.Payment_Gateway_Card_Type_Id ");
		sql.append("INNER JOIN T_CREDITCARD_TYPE creditType ");
		sql.append("ON creditType.card_type_id                = cardType.card_type_id ");
		sql.append("WHERE cardType.PAYMENT_GATEWAY_ID        IN (:paymentGateways) ");
		sql.append("AND countryCard.status                    = 'ACT' ");
		sql.append("AND (countryCard.origin_country_code      = :originCountryCode ");
		sql.append("OR countryCard.origin_country_code        = 'OT' ) ");
		sql.append("AND (countryCard.destination_country_code = :destCountryCode ");
		sql.append("OR countryCard.destination_country_code   = 'OT' ) ");
		sql.append("ORDER BY cardType.group_key, ");
		sql.append("  cardType.group_order ");

		Map parameters = new HashMap();
		parameters.put("paymentGateways", paymentGatewayIDs);
		parameters.put("originCountryCode", originCountryCode);
		parameters.put("destCountryCode", destCountryCode);

		return (Collection<CountryPaymentCardBehaviourDTO>) jdbcTemplate.query(sql.toString(), parameters,
				new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				CountryPaymentCardBehaviourDTO cardBahaviuor;
				Map<Integer, CountryPaymentCardBehaviourDTO> allCardsMap = new LinkedHashMap<Integer, CountryPaymentCardBehaviourDTO>();
				String cssClassName = null;
				String cardName = null;
				String cardDisplayName = null;
				String dbCountryName = null;
				Integer paymentCardTypeID = null;
				if (rs != null) {
					while (rs.next()) {
						cardBahaviuor = new CountryPaymentCardBehaviourDTO();
						cardBahaviuor.setPaymentGateWayID(rs.getInt("payment_gateway_id"));
						cardBahaviuor.setCardType(rs.getInt("card_type_id"));
						cardBahaviuor.setStatus(rs.getString("status"));
						cardName = rs.getString("card_type");
						cardDisplayName = rs.getString("card_display_name");
						cardBahaviuor.setCardDisplayName(cardDisplayName != null ? cardDisplayName : cardName);
						cssClassName = rs.getString("css_class_name");
						cardBahaviuor.setCssClassName(cssClassName != null ? cssClassName : cardName);
						cardBahaviuor.setBehaviour(rs.getString("behavior"));
						cardBahaviuor.setI18nMsgKey(rs.getString("i18n_message_key"));
						dbCountryName = rs.getString("origin_country_code");
						paymentCardTypeID = rs.getInt("payment_gateway_card_type_id");
						allCardsMap.put(paymentCardTypeID, cardBahaviuor);
					}
				}

				return new ArrayList(allCardsMap.values());
			}
		});
	}

	/**
	 * 
	 * @param countryCode
	 * @param paymentGatewayIDs
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Collection getCreditCardsListForAgent(Collection paymentGatewayIDs) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		StringBuffer query = new StringBuffer();
		
		query.append("SELECT cct.card_type_id, \n");
		query.append("  cct.card_type, \n");
		query.append("  pgct.payment_gateway_id \n");
		
		query.append("FROM t_creditcard_type cct, \n");
		query.append("  t_payment_gateway_card_type pgct \n");
		
		query.append("WHERE cct.VISIBILITY = 1 \n");
		query.append("  AND cct.card_type_id= pgct.card_type_id \n");
		
		query.append("  AND pgct.payment_gateway_id          IN (");
		query.append(Util.buildIntegerInClauseContent(paymentGatewayIDs));
		query.append(") ");

		query.append("  ORDER by card_type desc");
		
		Collection<CountryPaymentCardBehaviourDTO> list = (Collection<CountryPaymentCardBehaviourDTO>) jdbcTemplate.query(
				query.toString(), new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						CountryPaymentCardBehaviourDTO cardBahaviuor;
						Map<Integer, CountryPaymentCardBehaviourDTO> allCardsMap = new LinkedHashMap<Integer, CountryPaymentCardBehaviourDTO>();
						String cssClassName = null;
						String cardName = null;
						String cardDisplayName = null;
						String dbCountryName = null;
						Integer paymentCardTypeID = null;
						if (rs != null) {
							while (rs.next()) {
								cardBahaviuor = new CountryPaymentCardBehaviourDTO();
								cardBahaviuor.setPaymentGateWayID(rs.getInt("payment_gateway_id"));
								cardBahaviuor.setCardType(rs.getInt("card_type_id"));
								cardName = rs.getString("card_type");
								cardBahaviuor.setCardDisplayName(cardDisplayName != null ? cardDisplayName : cardName);
								cssClassName = null;
								cardBahaviuor.setCssClassName(cssClassName != null ? cssClassName : cardName);
								paymentCardTypeID = rs.getInt("card_type_id");
								cardBahaviuor.setBehaviour("internal-external");
								allCardsMap.put(paymentCardTypeID, cardBahaviuor);
							}
						}

						return new ArrayList(allCardsMap.values());
					}
				});

		return list;
	}

	public TempPaymentTnx getTempPaymentInfo(int tnxId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		// TempPaymentTnx
		String query = "SELECT * FROM T_TEMP_PAYMENT_TNX T WHERE T.TPT_ID = ? AND T.STATUS = '"
				+ ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS + "'";

		TempPaymentTnx paymentInfo = (TempPaymentTnx) jdbcTemplate.query(query, new String[] { String.valueOf(tnxId) },
				new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						TempPaymentTnx tempPayInfo = new TempPaymentTnx();
						if (rs != null) {
							if (rs.next()) {
								tempPayInfo.setAmount(rs.getBigDecimal("AMOUNT"));
								tempPayInfo.setContactPerson(rs.getString("CONTACT_PERSON"));
								tempPayInfo.setPaymentTimeStamp(rs.getDate("TIMESTAMP"));
								tempPayInfo.setLast4DigitsCC(rs.getString("CC_LAST_4_DIGITS"));
								tempPayInfo.setPnr(rs.getString("PNR"));
								tempPayInfo.setTnxType(rs.getString("DR_CR"));
								tempPayInfo.setStatus(rs.getString("STATUS"));
								tempPayInfo.setIpAddress(rs.getString("IP_ADDRESS"));
								tempPayInfo.setMobileNo(rs.getString("MOBILE_NO"));
								tempPayInfo.setStatus(rs.getString("STATUS"));
								tempPayInfo.setPaymentCurrencyAmount(rs.getBigDecimal("PAYMENT_CURRENCY_AMOUNT"));
								tempPayInfo.setPaymentCurrencyCode(rs.getString("PAYMENT_CURRENCY_CODE"));
							}
						}
						return tempPayInfo;
					}
				});

		return paymentInfo;
	}

	/**
	 * Get Payment methods Names
	 * 
	 * @param countryCode
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Set<String> getPaymentMethodNames(String countryCode, boolean enableOffline) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		StringBuffer query = new StringBuffer();
		query.append("SELECT UNIQUE pgct.css_class_name ");
		query.append("FROM t_payment_gateway pg, ");
		query.append("t_payment_gateway_card_type pgct, ");
		query.append("t_payment_country_card_type pcct ");
		query.append("WHERE pg.payment_gateway_id = pgct.payment_gateway_id ");
		query.append("AND pg.status = 'ACT' ");
		query.append("AND module_code = 'IBE' ");
		query.append("AND pgct.css_class_name IS NOT NULL ");
		query.append("AND pcct.payment_gateway_card_type_id = pgct.payment_gateway_card_type_id ");
		query.append("AND pcct.ibe_card_status = 'ACT' ");
		query.append("AND (pcct.country_code = '").append(countryCode).append("' ");
		query.append("OR pcct.country_code = 'OT') ");
		if (!enableOffline) {
			query.append("AND pg.IS_OFFLINE = 'N' ");
		}

		if (log.isDebugEnabled())
			log.debug("Query to retrive payment methods: " + query.toString());

		Object paymentMethods = jdbcTemplate.query(query.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Set<String> paymentMethods = new HashSet<String>();
				if (rs != null) {
					while (rs.next()) {
						paymentMethods.add(rs.getString("css_class_name"));
					}
				}
				return paymentMethods;
			}
		});

		if (log.isDebugEnabled())
			log.debug("Applicable payment methods:" + paymentMethods.toString());

		return (Set<String>) paymentMethods;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<IPGPaymentOptionDTO> getActivePaymentGatewayByProviderName(String providerName) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		StringBuffer query = new StringBuffer();
		query.append("SELECT * ");
		query.append("FROM t_payment_gateway pg ");
		query.append("WHERE pg.status = 'ACT' ");
		query.append("AND provier_name = '").append(providerName).append("' ");
		
		if (log.isDebugEnabled())
			log.debug("Query to retrive payment gateway by provider name: " + query.toString());
		
		Object pgws = jdbcTemplate.query(query.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<IPGPaymentOptionDTO> pgws = new ArrayList<IPGPaymentOptionDTO>();
				if (rs != null) {
					while (rs.next()) {
						IPGPaymentOptionDTO pgw = new IPGPaymentOptionDTO();
						
						pgw.setBaseCurrency(rs.getString("DEFAULT_CURRENCY_CODE"));
						pgw.setPaymentGateway(rs.getInt("PAYMENT_GATEWAY_ID"));
						pgw.setProviderCode(rs.getString("PROVIDER_CODE"));
						pgw.setProviderName(rs.getString("PROVIER_NAME"));
						pgw.setModuleCode(rs.getString("MODULE_CODE"));
						pgw.setDescription(rs.getString("DESCRIPTION"));
						pgw.setOnholdReleaseTime(rs.getInt("OHD_RELEASE_TIME"));
						pgw.setAliasBasePaymentAmount(rs.getBigDecimal("ALIAS_PAYMENT_AMOUNT_IN_BASE"));

						pgws.add(pgw);
					}
					
				}
				return pgws;
			}
		});
		
		return (List<IPGPaymentOptionDTO>)pgws;
	}

	public boolean isCreditCardFeeFromPGW(Integer ipgId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		String query = "SELECT PG.IS_CC_FEE_FROM_PGW FROM t_payment_gateway pg " + " WHERE pg.payment_gateway_id = ? ";

		Boolean found = (Boolean) jdbcTemplate.query(query, new Integer[] { ipgId }, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				boolean isCCFeeFromPGW = false;
				if (rs != null) {
					while (rs.next()) {
						String ccFeeFromPGW = rs.getString("IS_CC_FEE_FROM_PGW");
						if (ccFeeFromPGW != null && ccFeeFromPGW.equals("Y")) {
							isCCFeeFromPGW = true;
						}
						break;
					}

				}

				return new Boolean(isCCFeeFromPGW);
			}
		});

		return found.booleanValue();
	}
	
	@Override
	public String getPaymentGatewayNameFromCCTransaction(int temporyPayId) {

		Criteria cr = getSession()
				.createCriteria(CreditCardTransaction.class)
				.add(Restrictions.eq("temporyPaymentId", temporyPayId))
				.setProjection(
						Projections.projectionList().add(Projections.property("paymentGatewayConfigurationName"),
								"paymentGatewayConfigurationName"))
				.setResultTransformer(Transformers.aliasToBean(CreditCardTransaction.class));

		List list = cr.list();

		if (list != null && !list.isEmpty()) {
			return ((CreditCardTransaction) list.get(0)).getPaymentGatewayConfigurationName();
		}
		return null;
	}

	@Override
	public List getDefaultCurrencyPaymentGatewayForAccountTab(Integer ipgId) {
		return getDefaultCurrencyPaymentGateway(null, ipgId, true);
	}
	
	@Override
	public CreditCardTransaction loadOfflineLatestTransactionByReference(String transactionReference) {

		CreditCardTransaction creditCardTransaction = null;
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		StringBuffer query = new StringBuffer();
		query.append("SELECT * ");
		query.append("FROM t_ccard_payment_status CC, t_temp_payment_tnx TTPT ");
		query.append("WHERE TTPT.pnr = '").append(transactionReference).append("' ");
		query.append("AND TTPT.IS_OFFLINE_PAYMENT = 'Y' ");
		query.append("AND (TTPT.STATUS = 'I' OR TTPT.STATUS = 'IP') ");
		query.append("AND TTPT.tpt_id = CC.tpt_id ");
		query.append("AND CC.service_type = 'ccauthorize' ");
		query.append("ORDER BY CC.TRANSACTION_TIMESTAMP desc ");

		if (log.isDebugEnabled())
			log.debug("Query to retrive offline credit card payment details by pnr(transaction refference): " + query.toString());

		creditCardTransaction = (CreditCardTransaction) jdbcTemplate.query(query.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				CreditCardTransaction creditCardTransaction = null;
				if (rs != null && rs.next()) {

					creditCardTransaction = new CreditCardTransaction();
					creditCardTransaction.setAidCccompnay(rs.getString("AID_CCCOMPNAY"));
					creditCardTransaction.setAvsResult(rs.getString("AVS_RESULT"));
					creditCardTransaction.setComments(rs.getString("COMMENTS"));
					creditCardTransaction.setCvvResult(rs.getString("CVV_RESULT"));
					creditCardTransaction.setDeferredResponseText(rs.getString("DEFERRED_RESPONSE_TEXT"));
					creditCardTransaction.setErrorSpecification(rs.getString("ERROR_SPECIFICATION"));
					creditCardTransaction.setGatewayPaymentID(rs.getString("GATEWAY_PAYMENT_ID"));
					creditCardTransaction.setIntermediateRequest1(rs.getString("INTERMEDIATE_REQUEST_1"));
					creditCardTransaction.setIntermediateRequest2(rs.getString("INTERMEDIATE_REQUEST_2"));
					creditCardTransaction.setIntermediateResponse1(rs.getString("INTERMEDIATE_RESPONSE_1"));
					creditCardTransaction.setIntermediateResponse2(rs.getString("INTERMEDIATE_RESPONSE_2"));
					creditCardTransaction.setPaymentGatewayConfigurationName(rs.getString("GATEWAY_NAME"));
					creditCardTransaction.setRequest(rs.getInt("REQUEST"));
					creditCardTransaction.setRequestText(rs.getString("REQUEST_TEXT"));
					creditCardTransaction.setResponseText(rs.getString("RESPONSE_TEXT"));
					creditCardTransaction.setResponseTime(rs.getLong("RESPONSETIME"));
					creditCardTransaction.setResultCode(rs.getInt("RESULT_CODE"));
					creditCardTransaction.setResultCodeString(rs.getString("RESULT_CODE_STRING"));
					creditCardTransaction.setServiceType(rs.getString("SERVICE_TYPE"));
					creditCardTransaction.setTemporyPaymentId(rs.getInt("TPT_ID"));
					creditCardTransaction.setTempReferenceNum(rs.getString("NM_TRANSACTION_REFERENCE"));
					creditCardTransaction.setTransactionId(rs.getString("TRANSACTION_ID"));
					creditCardTransaction.setTransactionRefCccompany(rs.getString("TRANSACTION_REF_CCCOMPANY"));
					creditCardTransaction.setTransactionReference(rs.getString("TRANSACTION_REFERENCE"));
					creditCardTransaction.setTransactionResultCode(rs.getInt("TRANSACTION_RESULT_CODE"));
					creditCardTransaction.setTransactionRefNo(rs.getInt("TRANSACTION_REF_NO"));
					creditCardTransaction.setTransactionTimestamp(rs.getString("TRANSACTION_TIMESTAMP"));
					creditCardTransaction.setVersion(rs.getLong("VERSION"));

				}
				return creditCardTransaction;
			}
		});
		return creditCardTransaction;
	}
	
	@Override
	public CreditCardTransaction loadOfflineConfirmedTransactionByReference(String transactionReference) {
		CreditCardTransaction creditCardTransaction = null;
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		StringBuffer query = new StringBuffer();
		query.append("SELECT CC.TRANSACTION_REF_NO ");
		query.append("FROM t_ccard_payment_status CC, t_temp_payment_tnx TTPT ");
		query.append("WHERE TTPT.pnr = '").append(transactionReference).append("' ");
		query.append("AND TTPT.IS_OFFLINE_PAYMENT = 'Y' ");
		query.append("AND (TTPT.STATUS = 'RS') ");
		query.append("AND (TTPT.STATUS_HISTORY = 'I->PS->RS') ");
		query.append("AND TTPT.tpt_id = CC.tpt_id ");
		query.append("AND CC.service_type = 'ccauthorize' ");
		query.append("ORDER BY CC.TRANSACTION_TIMESTAMP desc ");

		if (log.isDebugEnabled())
			log.debug("Query to retrive offline credit card payment details by pnr(transaction refference): " + query.toString());

		creditCardTransaction = (CreditCardTransaction) jdbcTemplate.query(query.toString(), new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				CreditCardTransaction creditCardTransaction = null;
				if (rs != null && rs.next()) {

					creditCardTransaction = new CreditCardTransaction();
					creditCardTransaction.setTransactionRefNo(rs.getInt("TRANSACTION_REF_NO"));

				}
				return creditCardTransaction;
			}
		});
		return creditCardTransaction;
	}
	
	@Override
	public BigDecimal getPaymentAmountFromTPTId(Integer tempTnxId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String query = "SELECT PAYMENT_CURRENCY_AMOUNT FROM T_TEMP_PAYMENT_TNX T WHERE T.TPT_ID = ? ";
		BigDecimal amount = (BigDecimal) jdbcTemplate.query(query, new Integer[] { tempTnxId }, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				BigDecimal payAmount = null;
				if (rs != null) {
					while (rs.next()) {
						payAmount = rs.getBigDecimal("PAYMENT_CURRENCY_AMOUNT");
						break;
					}
				}
				return payAmount;
			}
		});

		return amount;
	}
	
	public boolean updateTempPaymentEntryForCancellingInvoice(Integer tempTnxId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String updateSQL = " UPDATE T_TEMP_PAYMENT_TNX SET STATUS = ?, STATUS_HISTORY = ? WHERE TPT_ID = ? ";
		Object[] params = { TempPaymentTnxTypes.PAYMENT_FAILURE, TempPaymentInconsistentStatusHistoryTypes.I_PF, tempTnxId};
		int[] types = { Types.VARCHAR, Types.VARCHAR, Types.NUMERIC };
		int rows = jdbcTemplate.update(updateSQL, params, types);
		if (rows == 0) {
			return false;
		}
		return true;
	}
	
	
}
