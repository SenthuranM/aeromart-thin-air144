package com.isa.thinair.paymentbroker.core.bl.migs;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.migs.MIGSRequest;

public class MigsPaymentUtils {

	public static final String ERROR_CODE_PREFIX = "migs.";

	public static String getPostInputDataFormHTML(Map<String, String> postDataMap) {
		StringBuilder sb = new StringBuilder();
		String inputParamValue = "<bankinfo><clientID>" + postDataMap.get(MIGSRequest.VPC_CLIENT)
				+ "</clientID><clientKey>cbdcckey</clientKey><action>launchpay</action><retPage>"
				+ postDataMap.get(MIGSRequest.VPC_RET_URL) + "</retPage>" + "<orderid>" + postDataMap.get(MIGSRequest.ORDER_ID)
				+ "</orderid><s_amt>" + postDataMap.get(MIGSRequest.AMOUNT) + "</s_amt><s_desc>"
				+ postDataMap.get(MIGSRequest.ORDER_INFO) + "</s_desc></bankinfo>";

		sb.append("<form name='frmreturn' method='post' action='" + postDataMap.get(MIGSRequest.VPC_IPG_URL)
				+ "' id='frmreturn' enctype='application/x-www-form-urlencoded'>");
		sb.append("<input type='hidden' name='d' value='" + inputParamValue + "'> ");
		sb.append("<script type='text/javascript'> document.getElementById('frmreturn').submit();" + "</script>");
		sb.append("</form>");

		return sb.toString();
	}

	public static String getMappedUnifiedError(String error) {

		if (error.equals("00")) {
			return "success";
		} else if (error.equals("01")) {
			return "Unknown Error";
		} else if (error.equals("02")) {
			return "Failed";
		} else if (error.equals("04")) {
			return "Expired Card";
		} else if (error.equals("05")) {
			return "Insufficient Funds";
		} else if (error.equals("07")) {
			return "Payment Server System Error";
		} else if (error.equals("90")) {
			return "Order already PAID";
		} else if (error.equals("94")) {
			return "Payment Failed";
		}
		return null;
	}

	public static IPGConfigsDTO getIPGConfigs(String merchantID, String password) {
		IPGConfigsDTO iPGConfigsDTO = new IPGConfigsDTO();
		iPGConfigsDTO.setMerchantID(merchantID);
		iPGConfigsDTO.setPassword(password);
		return iPGConfigsDTO;
	}

	public static String getPaymentResponseParams(String string, String patternString) {
		Pattern pattern = null;
		pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(string);

		if (matcher.find()) {
			for (int i = 1; i <= matcher.groupCount(); i++) {
				int startIndex = matcher.toMatchResult().group().indexOf('>');
				return matcher.toMatchResult().group().substring(startIndex + 1);
			}
		}
		return null;
	}
}
