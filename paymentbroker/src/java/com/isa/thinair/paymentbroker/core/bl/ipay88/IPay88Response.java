package com.isa.thinair.paymentbroker.core.bl.ipay88;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * The IPay88Response class to contain response variables
 * 
 */
public class IPay88Response {

	private static Log log = LogFactory.getLog(IPay88Response.class);

	public static final String NO_VALUE = "";

	public static final String VALID_HASH = "VALID HASH.";

	public static final String INVALID_HASH = "INVALID HASH.";

	public static final String INVALID_RESPONSE = "INVALID RESPONSE";

	public static String MERCHANTCODE = "MerchantCode";

	// optional field
	public static String PAYMENTID = "PaymentId";

	public static String REFNO = "RefNo";

	public static String AMOUNT = "Amount";

	public static String CURRENCY = "Currency";

	// optional field
	public static String REMARK = "Remark";

	public static String TRANSID = "TransId";

	public static String AUTHCODE = "AuthCode";

	public static String STATUS = "Status";

	public static String ErrDesc = "ErrDesc";

	public static String SIGNATURE = "Signature";

	private String merchantCode;

	private String paymentId;

	private String refNo;

	private String amount;

	private String currency;

	private String remark;

	private String transId;

	private String authCode;

	private String status;

	private String errDesc;

	private String signature;

	// Response as a Map
	Map response = null;

	public void setReponse(Map reponse) {
		this.response = reponse;
		merchantCode = null2unknown((String) reponse.get(MERCHANTCODE));
		paymentId = null2unknown((String) reponse.get(PAYMENTID));
		refNo = null2unknown((String) reponse.get(REFNO));
		amount = null2unknown((String) reponse.get(AMOUNT));
		currency = null2unknown((String) reponse.get(CURRENCY));
		remark = null2unknown((String) reponse.get(REMARK));
		transId = null2unknown((String) reponse.get(TRANSID));
		authCode = null2unknown((String) reponse.get(AUTHCODE));
		status = null2unknown((String) reponse.get(STATUS));
		errDesc = null2unknown((String) reponse.get(ErrDesc));
		signature = null2unknown((String) reponse.get(SIGNATURE));
	}

	/**
	 * This method takes a data String and returns a predefined value if empty If data Sting is null, returns string
	 * "No Value Returned", else returns input
	 * 
	 * @param in
	 *            String containing the data String
	 * 
	 * @return String containing the output String
	 */
	private static String null2unknown(String in) {
		if (in == null || in.length() == 0) {
			return NO_VALUE;
		} else {
			return in;
		}
	}

	/**
	 * Validate Response signature
	 * 
	 * @param certificate
	 * 
	 * @return
	 */
	public boolean validateSHA1Hash(Map<String, String> postDataMap) throws ModuleException {
		String sha1 = "";
		try {
			sha1 = IPay88Utils.computeSHA1(postDataMap);
		} catch (Exception e) {
			throw new ModuleException(e.getMessage());
		}
		
		String signature = (String) response.get(SIGNATURE);
		
		// FIXME temporary fix to replace " " by +
		String replacedSignature = signature.replace(' ','+');
		
		if (replacedSignature.equals(sha1))
			return true;
		else
			return false;
	}

	/**
	 * @return the merchantCode
	 */
	public String getMerchantCode() {
		return merchantCode;
	}

	/**
	 * @param merchantCode
	 *            the merchantCode to set
	 */
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	/**
	 * @return the paymentId
	 */
	public String getPaymentId() {
		return paymentId;
	}

	/**
	 * @param paymentId
	 *            the paymentId to set
	 */
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	/**
	 * @return the refNo
	 */
	public String getRefNo() {
		return refNo;
	}

	/**
	 * @param refNo
	 *            the refNo to set
	 */
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency
	 *            the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark
	 *            the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @return the transId
	 */
	public String getTransId() {
		return transId;
	}

	/**
	 * @param transId
	 *            the transId to set
	 */
	public void setTransId(String transId) {
		this.transId = transId;
	}

	/**
	 * @return the authCode
	 */
	public String getAuthCode() {
		return authCode;
	}

	/**
	 * @param authCode
	 *            the authCode to set
	 */
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the errDesc
	 */
	public String getErrDesc() {
		return errDesc;
	}

	/**
	 * @param errDesc
	 *            the errDesc to set
	 */
	public void setErrDesc(String errDesc) {
		this.errDesc = errDesc;
	}

	/**
	 * @return the signature
	 */
	public String getSignature() {
		return signature;
	}

	/**
	 * @param signature
	 *            the signature to set
	 */
	public void setSignature(String signature) {
		this.signature = signature;
	}

	/**
	 * @return the response
	 */
	public Map getResponse() {
		return response;
	}

	/**
	 * @param response
	 *            the response to set
	 */
	public void setResponse(Map response) {
		this.response = response;
	}

}
