package com.isa.thinair.paymentbroker.core.bl.cybersource;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CyberSourceRequest {

	private static final Log log = LogFactory.getLog(CyberSourceRequest.class);

	public static final String AMOUNT = "amount";
	public static final String ORDER_NUMBER = "orderNumber";
	public static final String BILL_FIRST_NAME = "billTo_firstName";
	public static final String BILL_LAST_NAME = "billTo_lastName";
	public static final String BILL_STREET_1 = "billTo_street1";
	public static final String BILL_CITY = "billTo_city";
	public static final String BILL_COUNTRY = "billTo_country";
	public static final String BILL_EMAIL ="billTo_email";
	public static final String BILL_POSTAL_CODE = "billTo_postalCode";
	
	public static final String HTTP_BROWSER_COOKIES_ACCEPTED="billTo_httpBrowserCookiesAccepted";

	public static final String MERCHANT_ID = "merchantID";
	public static final String CURRENCY = "currency";
	public static final String ORDER_PAGE_TIMESTAMP = "orderPage_timestamp";
	public static final String ORDER_PAGE_VERSION = "orderPage_version";
	public static final String ORDER_PAGE_SERIAL_NUMBER = "orderPage_serialNumber";
	public static final String ORDER_PAGE_TRANSACTION_TYPE = "orderPage_transactionType";

	public static final String ORDER_PAGE_MERCHANT_URL_POST = "orderPage_sendMerchantURLPost";
	public static final String ORDER_PAGE_MERCHANT_URL_POST_ADDRESS = "orderPage_merchantURLPostAddress";

	public static final String ORDER_PAGE_RECEIPT_LINK_TEXT = "orderPage_receiptLinkText";
	public static final String ORDER_PAGE_RECEIPT_RESPONSE_URL = "orderPage_receiptResponseURL";

	public static final String ORDER_PAGE_DECLINE_LINK_TEXT = "orderPage_declineLinkText";
	public static final String ORDER_PAGE_DECLINE_RESPONSE_URL = "orderPage_declineResponseURL";
	
	public static final String DEFAULT_CURRENCY = "usd";
	public static final String ORDER_PAGE_TRANSACTION_TYPE_SALE = "sale";

	private Map<String, String> requestFields = new HashMap<String, String>();

	public CyberSourceRequest(String amount, String orderNumber, String billFirstName, String billLastName, String billStreet1,
			String billCity, String billCountry, String billEmail, String billPostalCode) {
		requestFields.put(AMOUNT, amount);
		requestFields.put(ORDER_NUMBER, orderNumber);
		requestFields.put(BILL_FIRST_NAME, billFirstName);
		requestFields.put(BILL_LAST_NAME, billLastName);
		requestFields.put(BILL_STREET_1, billStreet1);
		requestFields.put(BILL_CITY, billCity);
		requestFields.put(BILL_COUNTRY, billCountry);
		requestFields.put(BILL_EMAIL, billEmail);
		requestFields.put(BILL_POSTAL_CODE, billPostalCode);
	}

	public void composeCyberSourceConfigData(String merchantId, String sharedSecret, String serialNumber, String version,
			String merchantPostURL, String receiptURL, String receiptLinkText, String sessionId) {

		if (log.isDebugEnabled()) {
			log.debug("################### Payment Gateway Request Session Id: " + sessionId + "############################");
		}

		requestFields.put(MERCHANT_ID, merchantId);
		requestFields.put(ORDER_PAGE_TIMESTAMP, String.valueOf(System.currentTimeMillis()));
		requestFields.put(ORDER_PAGE_VERSION, version);
		requestFields.put(ORDER_PAGE_SERIAL_NUMBER, serialNumber);

		requestFields.put(ORDER_PAGE_TRANSACTION_TYPE, ORDER_PAGE_TRANSACTION_TYPE_SALE);
		
		requestFields.put(CURRENCY, DEFAULT_CURRENCY);

		requestFields.put(ORDER_PAGE_MERCHANT_URL_POST, "true");
		requestFields.put(ORDER_PAGE_MERCHANT_URL_POST_ADDRESS, merchantPostURL + "?sessionId=" + sessionId);

		requestFields.put(ORDER_PAGE_RECEIPT_LINK_TEXT, receiptLinkText);
		requestFields.put(ORDER_PAGE_RECEIPT_RESPONSE_URL, receiptURL + "?sessionId=" + sessionId);

		requestFields.put(ORDER_PAGE_DECLINE_LINK_TEXT, receiptLinkText);
		requestFields.put(ORDER_PAGE_DECLINE_RESPONSE_URL, receiptURL + "?sessionId=" + sessionId);
		requestFields.put(HTTP_BROWSER_COOKIES_ACCEPTED, "false");
	}

	public Map<String, String> getRequestFields() {
		return requestFields;
	}

}
