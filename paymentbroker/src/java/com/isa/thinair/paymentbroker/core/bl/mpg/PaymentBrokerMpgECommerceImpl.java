package com.isa.thinair.paymentbroker.core.bl.mpg;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;

public class PaymentBrokerMpgECommerceImpl extends PaymentBrokerMpgTemplate {

	private static Log log = LogFactory.getLog(PaymentBrokerMpgECommerceImpl.class);

	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		String finalAmount = MpgPaymentUtils.formatAmount(ipgRequestDTO.getAmount());

		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		
		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		
		postDataMap.put(MpgRequest.VERSION, getVersion());
		postDataMap.put(MpgRequest.MERCHANT_ID, getMerchantId());
		postDataMap.put(MpgRequest.ACQUIRER_ID, getAcquirerId());
		postDataMap.put(MpgRequest.MERCHANT_RESP_URL, ipgRequestDTO.getReturnUrl());
		postDataMap.put(MpgRequest.PURCHASE_CURRENCY, getPurchaseCurrency());
		postDataMap.put(MpgRequest.PURCHASE_CURRENCY_EXPONENT, getPurchaseCurrencyExponent());
		postDataMap.put(MpgRequest.ORDER_ID, merchantTxnId);
		postDataMap.put(MpgRequest.SIGNATURE_METHOD, getSignatureMethod());
		postDataMap.put(MpgRequest.PURCHASE_AMOUNT, finalAmount);
		postDataMap.put(MpgRequest.PASSWORD, getPassword());
		postDataMap.put(MpgRequest.SIGNATURE, MpgPaymentUtils.computeSHA1(postDataMap));
		
		
		String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);
		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();	
		
		postDataMap.remove(MpgRequest.PASSWORD);
		
		String requestDataForm = PaymentBrokerUtils.getPostInputDataFormHTML(postDataMap, getIpgURL());
		
		if (log.isDebugEnabled()) {
			log.debug("MPG Request Data : " + strRequestParams + ", sessionID : " + sessionID);
		}
		
		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				strRequestParams + "," + sessionID, "", getPaymentGatewayName(), null, false);	
	
		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(requestDataForm);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);
		ipgRequestResultsDTO.setPostDataMap(postDataMap);

		return ipgRequestResultsDTO;
	}
}
