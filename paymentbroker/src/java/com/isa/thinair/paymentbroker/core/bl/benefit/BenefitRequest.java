package com.isa.thinair.paymentbroker.core.bl.benefit;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class BenefitRequest {

	public static final String PAYMENTID = "PaymentID";

	public static final String CURRENCY_CODE_BPG = "048";

	public static final String CURRENCY = "048";

	public static final int NUMBER_OF_DECIMAL_BPG = 3;

	public static final String ARABIC = "ar";
	
	public static final String LANGUAGE_CODE = "USA" ;

	public static final String RESPONSE_URL = AppSysParamsUtil.getSecureIBEUrl() + "benefitNotifyPaymentResponseHandler.action";
	
	public static final String RESPONSE_URL_XBE = AppSysParamsUtil.getAirlineReservationURL()
			+ "/xbe/public/benefitNotifyPaymentResponseHandler.action";
	
	public enum ACTION {
		PURCHASE(1);
		private int action;

		private ACTION(int payAction) {
			action = payAction;
		}

		public int getAction() {
			return action;
		}
	};

	public enum LANGUAGE {
		ENGLISH("USA");
		private String language;

		private LANGUAGE(String lan) {
			language = lan;
		}

		public String getLanguage() {
			return language;
		}
	};

}
