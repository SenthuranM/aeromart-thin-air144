package com.isa.thinair.paymentbroker.core.bl.cmi;

public interface CMIConstants {

	public static  final String ACK="Acknowledgement";
	public static final String APPROVED="APPROVED";
	public static final String ACK_SETTLE="Acknowledgement&Settle";
	public static final String ACK_VOID="Acknowlegement&Void";
	public static final String  TIMEOUT="Timeout";
	public static final String SYNTAX_ERROR="Merchant’s response syntax error";
	public static final String ERROR_MSG_WS="Error while creating a request string";
	public static final String APPROVED_POST="ACTION=POSTAUTH";
}
