package com.isa.thinair.paymentbroker.core.bl.payFort;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.ConfirmReservationUtil;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckNotificationDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckNotificationResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckOrderDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.CheckOrderResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.wsclient.api.service.WSClientBD;

public class PaymentBrokerPayFortTemplate extends PaymentBrokerTemplate implements PaymentBroker {

	private static Log log = LogFactory.getLog(PaymentBrokerPayFortTemplate.class);

	protected static final String DEFAULT_LANGUAGE = "En";
	public static final String CARD_TYPE_OFFLINE = "OF";
	public static final String CARD_TYPE_GENERIC = "GC";
	protected static final String DATE_FORMAT = "dd-MM-yyyy HH:mm";
	protected static final String VERIFICATION_HASH_ERROR = "payfort.payment.mismatch";
	protected static final String PAYFORT_RESPONSE_ERROR = "payfort.pay_at_store.payment.error";

	private String encryptionKey;
	private String serviceName;
	private String itemPrefix;
	private String expireTimeInMinutes;

	public String getExpireTimeInMinutes() {
		return expireTimeInMinutes;
	}

	public void setExpireTimeInMinutes(String expireTimeInMinutes) {
		this.expireTimeInMinutes = expireTimeInMinutes;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getEncryptionKey() {
		return encryptionKey;
	}

	public void setEncryptionKey(String encryptionKey) {
		this.encryptionKey = encryptionKey;
	}

	public String getItemPrefix() {
		return itemPrefix;
	}

	public void setItemPrefix(String itemPrefix) {
		this.itemPrefix = itemPrefix;
	}

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		String refundEnabled = getRefundEnabled();
		ServiceResponce reverseResult = null;

		if (refundEnabled != null && refundEnabled.equals("false")) {

			DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);
			return defaultServiceResponse;
		}
		reverseResult = refund(creditCardPayment, pnr, appIndicator, tnxMode);
		return reverseResult;
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGResponseDTO getReponseData(Map fields, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection creditCardPayment, boolean refundFlag) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {

		log.info("[PaymentBrokerPayFortTemplate::handleDeferredResponse()] Begin");

		if (AppSysParamsUtil.enableServerToServerMessages()
				&& AppSysParamsUtil.isConfirmOnholdReservationByServerToServerMessages()) {

			int temporyPaymentId = ipgResponseDTO.getTemporyPaymentId();
			CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTmpPayID(temporyPaymentId);
			ReservationBD reservationBD = ReservationModuleUtils.getReservationBD();
			boolean isReservationConfirmationSuccess = false;
			String pnr = oCreditCardTransaction.getTransactionReference();
			LCCClientReservation commonReservation = null;
			Reservation reservation = null;
			String status = "";

			if (oCreditCardTransaction != null) {

				/* Save Card Transaction response Details */
				Date requestTime = reservationBD.getPaymentRequestTime(oCreditCardTransaction.getTemporyPaymentId());
				Date now = Calendar.getInstance().getTime();
				String strResponseTime = CalendarUtil.getTimeDifference(requestTime, now);
				long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(requestTime, now);

				oCreditCardTransaction.setComments(strResponseTime);
				oCreditCardTransaction.setResponseTime(timeDiffInMillis);
				oCreditCardTransaction.setTransactionResultCode(1);
				oCreditCardTransaction.setDeferredResponseText(PaymentBrokerUtils.getRequestDataAsString(receiptyMap));
				oCreditCardTransaction.setTransactionId(String.valueOf(temporyPaymentId));

				PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

				IPGQueryDTO ipgQueryDTO = reservationBD.getTemporyPaymentInfo(oCreditCardTransaction.getTransactionRefNo());

				if (ipgQueryDTO != null) {
					/* Update with payment response details */

					LCCClientPnrModesDTO modes = new LCCClientPnrModesDTO();
					modes.setPnr(ipgQueryDTO.getPnr());
					modes.setRecordAudit(false);

					ipgQueryDTO.setPaymentType(getPaymentType(getStandardCardType(CARD_TYPE_GENERIC)));

					// for dry interline validation fix
					IPGIdentificationParamsDTO defIPGIdentificationParamsDTO = ipgQueryDTO.getIpgIdentificationParamsDTO();
					if (defIPGIdentificationParamsDTO.getIpgId() == null) {
						defIPGIdentificationParamsDTO.setIpgId(ipgResponseDTO.getPaymentBrokerRefNo());
						ipgQueryDTO.setIpgIdentificationParamsDTO(defIPGIdentificationParamsDTO);
					}

					/** NO RESERVATION OR CANCELED RESERVATION */
					if (!isReservationExist(ipgQueryDTO)
							|| ReservationInternalConstants.ReservationStatus.CANCEL.equals(ipgQueryDTO.getPnrStatus())) {

						Collection<IPGQueryDTO> col = new ArrayList<IPGQueryDTO>();
						col.add(ipgQueryDTO);

						/**
						 * When confirming a reservation made from another carrier reservation does not exists in own
						 * reservation table , therefore we need to load the reservation through LCC and clarify
						 * reservation exists or not
						 **/

						LCCClientPnrModesDTO pnrModesDTO = getPnrModesDTO(pnr, true, true, ApplicationEngine.IBE);
						commonReservation = AirproxyModuleUtils.getAirproxyReservationBD().searchReservationByPNR(pnrModesDTO,
								null, getTrackingInfo(ipgQueryDTO));
						status = commonReservation.getStatus();

						if (status.equals("") || ReservationInternalConstants.ReservationStatus.CANCEL.equals(status)) {

							log.info("[handleDeferredResponse()] Refunding Payment -> No Reservation has been created :"
									+ isReservationConfirmationSuccess);
							ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_REJECTED);
							receiptyMap.put("status", PayFortPaymentUtils.ERROR);

						} else {
							if (commonReservation != null) {
								ipgQueryDTO.setPnr(commonReservation.getPNR());
								ipgQueryDTO.setGroupPnr(commonReservation.getPNR());
								ipgQueryDTO.setPnrStatus(commonReservation.getStatus());
							}
						}
					}

					/** ONHOLD RESERVATION */
					if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(ipgQueryDTO.getPnrStatus())) {

						if (reservation == null && commonReservation == null) {
							try {
								reservation = PaymentBrokerModuleUtil.getReservationBD().getReservation(modes, null);
								status = reservation.getStatus();
							} catch (ModuleException e) {
								log.error(" ((o)) CommonsDataAccessException::getReservation " + e.getCause());
							}
						}

						if (!status.equals("") && ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(status)) {

							isReservationConfirmationSuccess = ConfirmReservationUtil
									.isReservationConfirmationSuccess(ipgQueryDTO);

							if (!isReservationConfirmationSuccess) {

								log.debug("[handleDeferredResponse()] Refunding Payment -> Onhold confirmation failed :"
										+ isReservationConfirmationSuccess);
								ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_REJECTED);
								receiptyMap.put("status", PayFortPaymentUtils.ERROR);
							} else {
								receiptyMap.put("status", PayFortPaymentUtils.SUCCESS);
								log.info("Reservation confirmation successful:" + ipgResponseDTO.getPaymentBrokerRefNo());
								ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_ACCEPTED);
							}

						}
					}

					/** ALREADY CONFIRMED RESERVATION */
					if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(ipgQueryDTO.getPnrStatus())) {

						if (reservation == null && commonReservation == null) {
							try {
								reservation = PaymentBrokerModuleUtil.getReservationBD().getReservation(modes, null);
								status = reservation.getStatus();
							} catch (ModuleException e) {
								log.error(" ((o)) CommonsDataAccessException::getReservation " + e.getCause());
							}
						}

						TempPaymentTnx tempPaymentTnx = null;
						tempPaymentTnx = ReservationModuleUtils.getReservationBD().loadTempPayment(temporyPaymentId);

						if (!status.equals("") && ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(status)
								&& "RS".equals(tempPaymentTnx.getStatus())) {
							receiptyMap.put("status", PayFortPaymentUtils.SUCCESS);
							ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_ACCEPTED);
							log.debug("[handleDeferredResponse()] Confirm Payment -> Reservation already confirmed :" + pnr);

						} else {
							receiptyMap.put("status", PayFortPaymentUtils.ERROR);
							ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_REJECTED);
							log.debug("[handleDeferredResponse()] Confirm Payment -> confirm by another payment gateway :" + pnr);
						}
					}

				} else {
					receiptyMap.put("status", PayFortPaymentUtils.ERROR);
					log.error("payfort temp transaction not found:" + ipgResponseDTO.getPaymentBrokerRefNo());
				}

			} else {
				receiptyMap.put("status", PayFortPaymentUtils.ERROR);
				log.error("creadit card transaction not found:" + ipgResponseDTO.getPaymentBrokerRefNo());
			}
			// notify the payfort to confirm the request
			getNotificationResponse(receiptyMap, ipgResponseDTO);
		} else {
			log.error("error check Response for transation:" + ipgResponseDTO.getPaymentBrokerRefNo());
		}

		log.info("[PaymentBrokerPayFortTemplate::handleDeferredResponse()] End");
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	@Override
	public Properties getProperties() {
		Properties props = super.getProperties();
		props.put("encryptionKey", PlatformUtiltiies.nullHandler(encryptionKey));
		props.put("serviceName", PlatformUtiltiies.nullHandler(serviceName));
		props.put("itemPrefix", PlatformUtiltiies.nullHandler(itemPrefix));
		props.put("expireTimeInMinutes", PlatformUtiltiies.nullHandler(expireTimeInMinutes));
		return props;
	}

	public void getNotificationResponse(Map fields, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		log.debug("[PaymentBrokerPayFortTemplate::getReponseData()] Begin ");

		int tnxResultCode = -1;
		String errorSpecification = "";
		String responseMismatch = "";
		String strRequestParams = "";
		XMLGregorianCalendar currentCalendar = null;
		CheckNotificationResponseDTO payFortNotifyingResponse = new CheckNotificationResponseDTO();
		Map<String, String> postDataMap = new LinkedHashMap<String, String>();

		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTmpPayID(ipgResponseDTO.getTemporyPaymentId());

		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time
		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

		CheckNotificationDTO payFortNotifyingRequest = new CheckNotificationDTO();
		// set map values to the DTO
		payFortNotifyingRequest.setRequest(fields);

		String currentDate = CalendarUtil.getDateInFormattedString(DATE_FORMAT, CalendarUtil.getCurrentZuluDateTime());
		currentCalendar = convertStringDateToXmlGregorianCalendar(currentDate, DATE_FORMAT, true);
		payFortNotifyingRequest.setMsgDate(currentCalendar);

		String signature = payFortNotifyingRequest.getCurrency() + "" + payFortNotifyingRequest.getMerchantID() + ""
				+ payFortNotifyingRequest.getOrderID() + "" + payFortNotifyingRequest.getPayment() + ""
				+ payFortNotifyingRequest.getServiceName() + "" + payFortNotifyingRequest.getStatus() + ""
				+ payFortNotifyingRequest.getTicketNumber() + getEncryptionKey();

		String signatureSHA1 = "";
		try {
			signatureSHA1 = SHA1(signature);
		} catch (NoSuchAlgorithmException e1) {
			log.error(e1);
			throw new ModuleException("paymentbroker.error.unknown");
		} catch (UnsupportedEncodingException e1) {
			log.error(e1);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		payFortNotifyingRequest.setSignature(signatureSHA1);
		log.debug("[PaymentBrokerPayFortTemplate::getNotificationResponse()] ");

		// calling remote web service call
		try {
			WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
			payFortNotifyingResponse = ebiWebervices.checkNotification(payFortNotifyingRequest);

		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		if (payFortNotifyingResponse != null) {
			if (payFortNotifyingResponse.getStatus().equals(PayFortPaymentUtils.SUCCESS)) {

				postDataMap.put(payFortNotifyingResponse.CURRENCY, payFortNotifyingResponse.getCurrency());
				postDataMap.put(payFortNotifyingResponse.MERCHANT_ID, payFortNotifyingResponse.getMerchantID());
				postDataMap.put(payFortNotifyingResponse.ORDER_ID, payFortNotifyingResponse.getOrderID());
				postDataMap.put(payFortNotifyingResponse.PAYMENT, payFortNotifyingResponse.getPayment());
				postDataMap.put(payFortNotifyingResponse.SIGNATURE, payFortNotifyingResponse.getSignature());
				postDataMap.put(payFortNotifyingResponse.TICKET_NUMBER, (String) fields.get(PayFortPaymentUtils.TICKET_NUMBER));

				strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

				if (checkHashForNotifiction(postDataMap)) {
					tnxResultCode = 1;
					errorSpecification = "" + responseMismatch;
				} else {
					tnxResultCode = 0;
					errorSpecification = "Security Validation failed!!!";
					// Different amount in response
					log.error("[PaymentBrokerPayFortTemplate::getNotificationResponse()] - SHA1 signation not satisfy from PayFort PGW");
				}

			} else {
				tnxResultCode = 0;
				errorSpecification = "" + payFortNotifyingResponse.getErrorDesc();

			}
		}

		updateAuditTransactionByTnxRefNo(oCreditCardTransaction.getTransactionRefNo(), strRequestParams, errorSpecification, "",
				oCreditCardTransaction.getTransactionReference(), tnxResultCode);

	}

	protected boolean isReservationExist(IPGQueryDTO ipgQueryDTO) {
		boolean isExist = false;
		if (ipgQueryDTO != null) {
			if (ipgQueryDTO.getPnr() != null && ipgQueryDTO.getPnr().trim().length() != 0) {
				isExist = true;
			}
		}
		return isExist;
	}

	protected int getStandardCardType(String card) {
		// [1-MASTER, 2-VISA, 3-AMEX, 4-DINNERS, 5-GENERIC, 6-OFFLINE]
		int mapType = 5; // default card type
		if (card != null && !card.equals("") && mapCardType.get(card) != null) {
			String mapTypeStr = (String) mapCardType.get(card);
			mapType = Integer.parseInt(mapTypeStr);
		}
		return mapType;
	}

	protected PaymentType getPaymentType(int cardID) {

		PaymentType paymentType = PaymentType.CARD_GENERIC;
		switch (cardID) {
		case 1:
			paymentType = PaymentType.CARD_MASTER;
			break;
		case 2:
			paymentType = PaymentType.CARD_VISA;
			break;
		case 3:
			paymentType = PaymentType.CARD_AMEX;
			break;
		case 4:
			paymentType = PaymentType.CARD_DINERS;
			break;
		case 5:
			paymentType = PaymentType.CARD_GENERIC;
			break;
		case 6:
			paymentType = PaymentType.CARD_CMI;
			break;
		}
		return paymentType;
	}

	/**
	 * Convert lexical representation of date to java.util.Date.
	 * 
	 * @param source
	 *            the source
	 * @return the dateThis method used to parse the lexical string representation defined in XML Schema 1.0 Part 2,
	 *         Section 3.2.[7-14].1 to a java.util.Date based on XMLGregorianCalendar. This replaced the behavior of
	 *         convertToDate() method that copied from Axis1 .
	 */
	protected static XMLGregorianCalendar convertXmlToDate(String source) {
		try {
			XMLGregorianCalendar cal = DatatypeFactory.newInstance().newXMLGregorianCalendar(source);
			return cal;
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		return null;
	}

	protected static String SHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md;
		md = MessageDigest.getInstance("SHA-1");
		byte[] sha1hash = new byte[40];
		md.update(text.getBytes("iso-8859-1"), 0, text.length());
		sha1hash = md.digest();
		return convertToHex(sha1hash);
	}

	protected static String convertToHex(byte[] data) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			int halfbyte = (data[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				if ((0 <= halfbyte) && (halfbyte <= 9))
					buf.append((char) ('0' + halfbyte));
				else
					buf.append((char) ('a' + (halfbyte - 10)));
				halfbyte = data[i] & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

	protected static XMLGregorianCalendar convertStringDateToXmlGregorianCalendar(String dateStr, String dateFormat,
			boolean noTimezone) {
		try {
			// this may throw DatatypeConfigurationException
			DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
			GregorianCalendar calendar = new GregorianCalendar();
			// reset all fields
			calendar.clear();

			Calendar parsedCalendar = Calendar.getInstance();
			// eg "yyyy-MM-dd"
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			// this may throw ParseException
			Date rawDate = sdf.parse(dateStr);
			parsedCalendar.setTime(rawDate);

			// set date from parameter and leave time as default calendar values
			calendar.set(parsedCalendar.get(Calendar.YEAR), parsedCalendar.get(Calendar.MONTH),
					parsedCalendar.get(Calendar.DATE), parsedCalendar.get(Calendar.HOUR_OF_DAY),
					parsedCalendar.get(Calendar.MINUTE));

			XMLGregorianCalendar xmlCalendar = datatypeFactory.newXMLGregorianCalendar(calendar);
			// clears default timezone
			if (noTimezone) {
				xmlCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
			}
			xmlCalendar.setFractionalSecond(null);

			// filter for GMT+00:00 calendar date
			String text = xmlCalendar.toXMLFormat();
			int pos = text.indexOf('Z');
			if (pos > 0) {

				String strxmlCalendar = text.substring(0, pos) + "+00:01";
				xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(strxmlCalendar);

				if (noTimezone) {
					xmlCalendar.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
				}
				xmlCalendar.setFractionalSecond(null);
				return xmlCalendar;
			}

			return xmlCalendar;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected static int roundUpExpireDate(long num, long divisor) {
		return (int) ((num + divisor - 1) / divisor);
	}

	protected CreditCardTransaction isTransactionExists(String pnr) {
		CreditCardTransaction oCreditCardTransaction = loadTransactionByReference(pnr);
		if (oCreditCardTransaction != null) {
			return oCreditCardTransaction;
		}
		return null;
	}

	protected CheckOrderResponseDTO checkTransactionStatus(Map<String, String> receiptyMap,
			CreditCardTransaction oCreditCardTransaction) throws ModuleException {

		CheckOrderDTO checkOrder = new CheckOrderDTO();

		XMLGregorianCalendar currentCalendar = null;
		TempPaymentTnx tmpPayment = null;
		CheckOrderResponseDTO orderResponse = null;

		try {
			tmpPayment = ReservationModuleUtils.getReservationBD().loadTempPayment(oCreditCardTransaction.getTemporyPaymentId());
			log.info("[handleDeferredResponse()] TempPaymentTnx " + tmpPayment.getTnxId() + " is loaded");
		} catch (ModuleException e) {
			log.error(e);
		}
		String currentDate = CalendarUtil.getDateInFormattedString(DATE_FORMAT, CalendarUtil.getCurrentZuluDateTime());
		currentCalendar = convertStringDateToXmlGregorianCalendar(currentDate, DATE_FORMAT, true);

		checkOrder.setCurrency(receiptyMap.get(PayFortPaymentUtils.CURRENCY));
		checkOrder.setMerchantID(receiptyMap.get(PayFortPaymentUtils.MERCHANT_ID));
		checkOrder.setOrderID(receiptyMap.get(PayFortPaymentUtils.ORDER_ID));
		checkOrder.setPayment(receiptyMap.get(PayFortPaymentUtils.PAYMENT));
		checkOrder.setServiceName(receiptyMap.get(PayFortPaymentUtils.SERVICE_NAME));
		checkOrder.setMsgDate(currentCalendar);

		String signature = checkOrder.getCurrency() + "" + checkOrder.getMerchantID() + "" + checkOrder.getOrderID() + ""
				+ checkOrder.getPayment() + "" + checkOrder.getServiceName() + getEncryptionKey();

		String signatureSHA1 = "";
		try {
			signatureSHA1 = SHA1(signature);
		} catch (NoSuchAlgorithmException e1) {
			log.error(e1);
			throw new ModuleException("paymentbroker.error.unknown");
		} catch (UnsupportedEncodingException e1) {
			log.error(e1);
			throw new ModuleException("paymentbroker.error.unknown");
		}
		checkOrder.setSignature(signatureSHA1);

		if (tmpPayment != null && checkOrder.getCurrency().equals(tmpPayment.getPaymentCurrencyCode())) {

			// calling web service call
			try {
				WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
				orderResponse = ebiWebervices.checkTransaction(checkOrder);

			} catch (Exception e) {
				log.error(e);
				throw new ModuleException("paymentbroker.error.unknown");
			}

			log.info("[handleDeferredResponse()] serviceResponce " + orderResponse.getStatus());

		}

		return orderResponse;

	}

	protected boolean checkHashForNotifiction(Map<String, String> notificationResponseMap) {
		String signature = notificationResponseMap.get(PayFortPaymentUtils.CURRENCY)
				+ notificationResponseMap.get(PayFortPaymentUtils.MERCHANT_ID)
				+ notificationResponseMap.get(PayFortPaymentUtils.ORDER_ID)
				+ notificationResponseMap.get(PayFortPaymentUtils.PAYMENT) + getEncryptionKey();
		String signatureSHA1 = "";
		try {
			signatureSHA1 = SHA1(signature);
		} catch (NoSuchAlgorithmException e) {
			log.error(e);
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			log.error(e);
			e.printStackTrace();
		}
		if (signatureSHA1.equals(notificationResponseMap.get(PayFortPaymentUtils.SIGNATURE)))
			return true;

		return false;
	}

	@Override
	public boolean isEnableRefundByScheduler() {
		return true;
	}

	protected static String toXMLFormat(XMLGregorianCalendar calendar) {
		String text = calendar.toXMLFormat();
		int pos = text.indexOf('Z');

		return pos < 0 ? text : text.substring(0, pos) + "+00:00";
	}

	private static LCCClientPnrModesDTO getPnrModesDTO(String pnr, boolean isGroupPNR, boolean recordAudit,
			ApplicationEngine appIndicator) {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();

		boolean loadLocalTimes = false;
		if (isGroupPNR) {
			pnrModesDTO.setGroupPNR(pnr);
			loadLocalTimes = true;
		} else {
			pnrModesDTO.setPnr(pnr);
			loadLocalTimes = false;
		}

		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadLastUserNote(false);
		pnrModesDTO.setLoadLocalTimes(loadLocalTimes);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setRecordAudit(recordAudit);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true);
		pnrModesDTO.setAppIndicator(appIndicator);
		pnrModesDTO.setLoadExternalPaxPayments(true);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
		pnrModesDTO.setLoadEtickets(true);

		return pnrModesDTO;
	}

	private static TrackInfoDTO getTrackingInfo(IPGQueryDTO ipgQueryDTO) {
		TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
		trackInfoDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		trackInfoDTO.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
		trackInfoDTO.setCallingInstanceId(SystemPropertyUtil.getInstanceId());

		return trackInfoDTO;
	}

	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

}
