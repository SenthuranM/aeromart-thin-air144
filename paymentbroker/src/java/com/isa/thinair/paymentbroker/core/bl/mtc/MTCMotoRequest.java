/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.bl.mtc;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.core.bl.migs3party.MIGSPaymentUtils;

/**
 * MIGS payment request
 * 
 * @author byorn
 */
public class MTCMotoRequest {

	private static Log log = LogFactory.getLog(MTCMotoRequest.class);

	public static final String CHAR_ENC = "UTF-8";
	public static final String MERCHANTID = "merchantId";
	public static final String ORDERID = "orderId";
	public static final String USER = "user";
	public static final String PASSWORD = "password";
	public static final String AMOUNT = "amount";
	public static final String CURRENCY = "currency";
	public static final String CARDTYPE = "cardType";
	public static final String CARDNUMBER = "cardNB";
	public static final String CARDEXPIRY = "cardExpiracy";
	public static final String CARDOWNER = "cardOwner";
	public static final String CVSCODE = "cvsCode";
	public static final String CVSSTATE = "cvsState";
	public static final String EMAILADD = "emailAddress";

	// cancellation
	public static final String USERPASSWORD = "userPassword";
	public static final String STORE = "store";
	public static final String ORDER_NUMBER = "orderNumber";
	public static final String CHECKSUM = "checksum";

	private static final char[] HEX_TABLE = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D',
			'E', 'F' };

	private String version;

	private String merchantId;

	private String locale;

	private String payCommand;

	private String accessCode;

	private String orderInfo;

	private String amount;

	private String returnURL;

	private String merchantTxnId;

	private String ipgURL;

	private String cardNumber;

	private String cardExpiry;

	private String cardSecurityCode;

	private Map fields = new HashMap();

	public MTCMotoRequest() {
	}

	public MTCMotoRequest(String merchantId, String returnURL, String locale, String command, String merchantAccessCode,
			String orderInfo, String version, String amount, String merchantTxnId, String ipgURL) {
		this.merchantId = merchantId;
		this.returnURL = returnURL;
		this.locale = locale;
		this.payCommand = command;
		this.accessCode = merchantAccessCode;
		this.orderInfo = orderInfo;
		this.version = version;
		this.amount = amount;
		this.merchantTxnId = merchantTxnId;
		this.ipgURL = ipgURL;

		// Fills the hash map
		// fields.put(VPC_VERSION, this.version);
		/*
		 * fields.put(VPC_COMMAND, this.payCommand); fields.put(VPC_ACC_CODE, this.accessCode);
		 * fields.put(VPC_MERCH_TXN_REF, this.merchantTxnId); fields.put(VPC_MERCHANT, this.merchantId);
		 */

		// for the ease of reporting to have the tnx id instead of order info- requested by NI
		// Sending PNR + Temp Payment Id for ease of reporting. Requested by Nasly - Nili 1:39 PM 11/1/2007
		/* fields.put(VPC_ORDER_INFO, this.orderInfo); */

		/*
		 * fields.put(VPC_AMOUNT, this.amount); fields.put(VPC_LOCALE, this.locale); fields.put(VPC_RETURN_URL,
		 * this.returnURL);
		 */
	}

	public String getReturnURL() {
		return returnURL;
	}

	public void setReturnURL(String returnURL) {
		this.returnURL = returnURL;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	public String getPayCommand() {
		return payCommand;
	}

	public void setPayCommand(String payCommand) {
		this.payCommand = payCommand;
	}

	/**
	 * This method is for sorting the fields and creating an MD5 secure hash.
	 * 
	 * @param fields
	 *            is a map of all the incoming hey-value pairs from the VPC
	 * @param buf
	 *            is the hash being returned for comparison to the incoming hash
	 */
	/*
	 * public String hashAllFields(String secureSecret) {
	 * 
	 * // create a list and sort it List fieldNames = new ArrayList(fields.keySet()); Collections.sort(fieldNames);
	 * 
	 * // create a buffer for the md5 input and add the secure secret first StringBuffer buf = new StringBuffer();
	 * buf.append(secureSecret);
	 * 
	 * // iterate through the list and add the remaining field values Iterator itr = fieldNames.iterator();
	 * 
	 * while (itr.hasNext()) { String fieldName = (String) itr.next(); String fieldValue = (String)
	 * fields.get(fieldName); if ((fieldValue != null) && (fieldValue.length() > 0)) { buf.append(fieldValue); } }
	 * 
	 * MessageDigest md5 = null; byte[] ba = null;
	 * 
	 * // create the md5 hash and UTF-8 encode it try { md5 = MessageDigest.getInstance(MESSAGE_DIGEST_ALGO); ba =
	 * md5.digest(buf.toString().getBytes(CHAR_ENC)); } catch (Exception e) { log.error(" FATAL ERROR  ", e); } // wont
	 * happen
	 * 
	 * return hex(ba);
	 * 
	 * }
	 */

	/**
	 * Returns Hex output of byte array
	 */
	/*
	 * private String hex(byte[] input) { // create a StringBuffer 2x the size of the hash array StringBuffer sb = new
	 * StringBuffer(input.length * 2);
	 * 
	 * // retrieve the byte array data, convert it to hex // and add it to the StringBuffer for (int i = 0; i <
	 * input.length; i++) { sb.append(HEX_TABLE[(input[i] >> 4) & 0xf]); sb.append(HEX_TABLE[input[i] & 0xf]); } return
	 * sb.toString(); }
	 */

	public String getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(String orderInfo) {
		this.orderInfo = orderInfo;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getMerchantTxnId() {
		return merchantTxnId;
	}

	public void setMerchantTxnId(String merchantTxnId) {
		this.merchantTxnId = merchantTxnId;
	}

	public Map getFields() {
		return fields;
	}

	public void setFields(Map fields) {
		this.fields = fields;
	}

	/**
	 * @return the cardExpiry
	 */
	public String getCardExpiry() {
		return cardExpiry;
	}

	/**
	 * @param cardExpiry
	 *            the cardExpiry to set
	 */
	public void setCardExpiry(String cardExpiry) {
		this.cardExpiry = cardExpiry;
	}

	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * @param cardNumber
	 *            the cardNumber to set
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return the cardSecurityCode
	 */
	public String getCardSecurityCode() {
		return cardSecurityCode;
	}

	/**
	 * @param cardSecurityCode
	 *            the cardSecurityCode to set
	 */
	public void setCardSecurityCode(String cardSecurityCode) {
		this.cardSecurityCode = cardSecurityCode;
	}

	public String getRequestRedirectionURL(String secureSecret) throws ModuleException {
		// Create MD5 secure hash and insert it into the hash map if it was created
		// //String secureHash = hashAllFields(secureSecret);
		String secureHash = MIGSPaymentUtils.hashAllFields(fields, secureSecret);
		// fields.put(VPC_SECURE_HASH, secureHash);

		// Creates the redirection URL
		StringBuffer buf = new StringBuffer();
		buf.append(this.ipgURL).append('?');
		try {
			appendQueryFields(buf, fields);
		} catch (UnsupportedEncodingException uee) {
			throw new ModuleException("paymentbroker.error.unsupportedencoding");
		}
		return buf.toString();
	}

	/**
	 * This method is for creating a URL query string.
	 * 
	 * @param buf
	 *            is the inital URL for appending the encoded fields to
	 * @param fields
	 *            is the input parameters from the order page
	 */
	public void appendQueryFields(StringBuffer buf, Map fields) throws UnsupportedEncodingException {

		// create a list
		List fieldNames = new ArrayList(fields.keySet());
		Iterator itr = fieldNames.iterator();

		// move through the list and create a series of URL key/value pairs
		while (itr.hasNext()) {
			String fieldName = (String) itr.next();
			String fieldValue = (String) fields.get(fieldName);

			if ((fieldValue != null) && (fieldValue.length() > 0)) {
				// append the URL parameters
				buf.append(URLEncoder.encode(fieldName, CHAR_ENC));
				buf.append('=');
				buf.append(URLEncoder.encode(fieldValue, CHAR_ENC));
			}

			// add a '&' to the end if we have more fields coming.
			if (itr.hasNext()) {
				buf.append('&');
			}
		}
	}

}
