package com.isa.thinair.paymentbroker.core.bl.ccAvenue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.ConfirmReservationUtil;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.ccAvenue.CCAvenueRequest;
import com.isa.thinair.paymentbroker.api.dto.ccAvenue.CCAvenueResponse;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.bl.ogone.OgonePaymentUtils;
import com.isa.thinair.paymentbroker.core.bl.qiwi.QiwiPaymentUtils;
import com.isa.thinair.paymentbroker.core.bl.saman.SamanPaymentUtils;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;


public class PaymentBrokerCCAvenueTemplate extends PaymentBrokerTemplate implements PaymentBroker {

	
	private static Log log = LogFactory.getLog(PaymentBrokerCCAvenueTemplate.class);
	protected static final String CARD_TYPE_GENERIC = "GC";
	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	private String encryptionKey;
	private String accessCode;
	private String serverToServerReqURL;
	private String ccAvenueCardType;
	private String ccAvenuePaymentOption;
	
	
	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		log.debug("[PaymentBrokerCCAvenueTemplate::refund()] Begin");
		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status;
		String errorSpecification;
		String merchTnxRef;

		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

		String refundAmountStr = creditCardPayment.getAmount();
		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		if (creditCardPayment.getPreviousCCPayment() != null && getRefundEnabled() != null && getRefundEnabled().equals("true")) {
			
			if (log.isDebugEnabled()) {
				log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
				log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
				log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
				log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
				log.debug("CC Old pay Formatted sAmount : " + refundAmountStr);
				log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());
			}

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				log.info("[PaymentBrokerCCAvenueTemplate::refund()] Looking for OtherIPGs ....");
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}
			
			Properties ipgProps = PaymentBrokerUtils.getPaymentBrokerBD().getProperties(creditCardPayment.getIpgIdentificationParamsDTO());
			
			// ccAvenue refund request
			CCAvenueRequest refReq = new CCAvenueRequest();
			refReq.setMerchantId(ipgProps.getProperty("merchantId"));
			refReq.setServerToServerURL(ipgProps.getProperty(CCAvenueRequest.S2S_REQUEST_URL));
			refReq.setOrderId(String.valueOf(ccTransaction.getTemporyPaymentId()));
			refReq.setTrackingId(ccTransaction.getTransactionId());
			refReq.setAccessCode(ipgProps.getProperty(CCAvenueRequest.S2S_REQUEST_ACCESS_CODE));
			refReq.setAmount(QiwiPaymentUtils.getFormattedAmount(refundAmountStr));
			refReq.setCommand(CCAvenueRequest.COMMAND_CONFIRM_ORDER);
			refReq.setTransactionTimestamp(ccTransaction.getTransactionTimestamp());
			
			CCAvenueResponse ccAveResp = null;
			
			PaymentQueryDR oNewQuery = getPaymentQueryDR();
			// set merchant transaction reference
			IPGConfigsDTO ipgConfigs = SamanPaymentUtils.getIPGConfigs(getMerchantId(), getPassword());
			ipgConfigs.setIpgURL(getIpgURL());
			ipgConfigs.setPaymentAmount(refReq.getAmount());
			ipgConfigs.setAccessCode(getAccessCode());
			ipgConfigs.setRefunding(true);
			
			serviceResponse = oNewQuery.query(ccTransaction,ipgConfigs, QUERY_CALLER.REFUND);			
			log.info("[PaymentBrokerCCAvenueTemplate::refund()] Order Status Code : " + serviceResponse.getResponseParam(CCAvenueRequest.ORDER_STATUS));
			
			if(serviceResponse.isSuccess() 
					&& (serviceResponse.getResponseParam(CCAvenueRequest.ORDER_STATUS).equals(CCAvenueResponse.ORDER_STATUS_SHIPPED)
							|| serviceResponse.getResponseParam(CCAvenueRequest.ORDER_STATUS).equals(CCAvenueResponse.ORDER_STATUS_REFUNDED))){
				
				merchTnxRef = ccTransaction.getTempReferenceNum();
				CreditCardTransaction ccNewTransaction = auditTransaction(pnr, getMerchantId(), merchTnxRef,
						creditCardPayment.getTemporyPaymentId(), bundle.getString("SERVICETYPE_REFUND"), refReq.toString(),
						"", getPaymentGatewayName(), null, false);

				refReq.setCommand(CCAvenueRequest.COMMAND_REFUND);
				
				if(refReq.getTrackingId() == null || "".equals(refReq.getTrackingId())){
					refReq.setTrackingId(serviceResponse.getResponseParam(CCAvenueRequest.TRACKING_ID).toString());
				}
				
				String refundXml;
				
				try {					
					refundXml = CCAvenuePaymentUtils.composeRefundQuery(refReq);
					log.debug("[PaymentBrokerCCAvenueTemplate::refund()]: xmlRequest " + refundXml);
					String encryptedXmlReq = CCAvenuePaymentUtils.encrypt(refundXml, ipgProps.getProperty(CCAvenueRequest.ENCRYPTION_KEY));
					String response = CCAvenuePaymentUtils.sendServerToServerRequest(encryptedXmlReq, refReq);
					ccAveResp = CCAvenuePaymentUtils.processServerToServerResponse(response, ipgProps.getProperty(CCAvenueRequest.ENCRYPTION_KEY));
					
				} catch (Exception e) {
					e.printStackTrace();
				} 
				
				Integer paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());
				log.info("[PaymentBrokerCCAvenueTemplate::refund()] Refund Status : " + ccAveResp.toString());
				
				if(ccAveResp.getStatusCode().equals(CCAvenueResponse.SUCCESS_STATUS)){
					
					errorCode = "";
					transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
					status = IPGResponseDTO.STATUS_ACCEPTED;
					tnxResultCode = 1;
					errorSpecification = "";
					sr.setSuccess(true);
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,	PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
					
				} else {
					
					status = IPGResponseDTO.STATUS_REJECTED;
					errorCode = ccAveResp.getStatusCode();
					transactionMsg = ccAveResp.getReason();
					tnxResultCode = 0;
					errorSpecification = "Status:" + status + " Error code:" + errorCode + " Error Desc:" + transactionMsg;
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
			
				}				
				
				updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), ccAveResp.toString(), errorSpecification,
						ccAveResp.getStatusCode(), refReq.getRefundId() , tnxResultCode);
				
				sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, merchTnxRef);
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
				
			} else {
				log.info("[PaymentBrokerCCAvenueTemplate::refund()] Invoice has not been paid. Cannot Refund. Order status : " + serviceResponse.getResponseParam(CCAvenueRequest.ORDER_STATUS));
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);
			}			

		} else {
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);
		}
		
		log.debug("[PaymentBrokerCCAvenueTemplate::refund()] End");
		return sr;
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");

	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		String refundEnabled = getRefundEnabled();
		ServiceResponce reverseResult = null;

		if (refundEnabled != null && refundEnabled.equals("false")) {

			DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);
			return defaultServiceResponse;
		}
		reverseResult = refund(creditCardPayment, pnr, appIndicator, tnxMode);
		return reverseResult;
	}

	@Override
	public Properties getProperties(){
		Properties props = super.getProperties();
		props.put(CCAvenueRequest.ENCRYPTION_KEY, PlatformUtiltiies.nullHandler(encryptionKey));	
		props.put(CCAvenueRequest.ACCESS_CODE, PlatformUtiltiies.nullHandler(accessCode));
		props.put(CCAvenueRequest.S2S_REQUEST_URL, PlatformUtiltiies.nullHandler(serverToServerReqURL));
		return props;
	}
	
	
	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {

		log.info("[PaymentBrokerCCAvenueTemplate::resolvePartialPayments()] Begin");

		Iterator itColIPGQueryDTO = colIPGQueryDTO.iterator();

		Collection<Integer> oResultIF = new ArrayList<Integer>();
		Collection<Integer> oResultIP = new ArrayList<Integer>();
		Collection<Integer> oResultII = new ArrayList<Integer>();
		Collection<Integer> oResultIS = new ArrayList<Integer>();

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		IPGQueryDTO ipgQueryDTO;
		CreditCardTransaction oCreditCardTransaction;
		PaymentQueryDR query;
		PreviousCreditCardPayment oPrevCCPayment;
		CreditCardPayment oCCPayment;

		while (itColIPGQueryDTO.hasNext()) {
			ipgQueryDTO = (IPGQueryDTO) itColIPGQueryDTO.next();

			oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgQueryDTO.getPaymentBrokerRefNo());
			query = getPaymentQueryDR();

			IPGConfigsDTO ccAveConfigs = OgonePaymentUtils.getIPGConfigs(getMerchantId(), getUser(), getPassword());
			ccAveConfigs.setAccessCode(getAccessCode());
			
			try {
				ServiceResponce serviceResponce = query.query(oCreditCardTransaction, ccAveConfigs,
						PaymentBrokerInternalConstants.QUERY_CALLER.SCHEDULER);

				// If successful payment remains at Bank
				if (serviceResponce.isSuccess()) {
					boolean isResConfirmationSuccess = ConfirmReservationUtil.isReservationConfirmationSuccess(ipgQueryDTO);
					log.debug("Confirming Reservation : " + isResConfirmationSuccess + "pnr : " + ipgQueryDTO.getPnr());
					
					if (isResConfirmationSuccess) {
						if (log.isDebugEnabled()) {
							log.debug("Status moving to RS:" + oCreditCardTransaction.getTemporyPaymentId());
						}

					} else if (refundFlag) {
						// Do refund
						String txnNo = PlatformUtiltiies.nullHandler(serviceResponce
								.getResponseParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE));

						oPrevCCPayment = new PreviousCreditCardPayment();
						oPrevCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
						oPrevCCPayment.setPgSpecificTxnNumber(txnNo);

						oCCPayment = new CreditCardPayment();
						oCCPayment.setPreviousCCPayment(oPrevCCPayment);
						oCCPayment.setAmount(ipgQueryDTO.getAmount().toString());
						oCCPayment.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
						oCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
						oCCPayment.setIpgIdentificationParamsDTO(ipgQueryDTO.getIpgIdentificationParamsDTO());
						oCCPayment.setPnr(oCreditCardTransaction.getTransactionReference());
						oCCPayment.setTemporyPaymentId(oCreditCardTransaction.getTemporyPaymentId());

						ServiceResponce srRev = refund(oCCPayment, oCCPayment.getPnr(), oCCPayment.getAppIndicator(),
								oCCPayment.getTnxMode());

						if (srRev.isSuccess()) {
							// Change State to 'IS'
							oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
							log.info("Status moving to IS:" + oCreditCardTransaction.getTemporyPaymentId() + " pnr : " + ipgQueryDTO.getPnr());
						} else {
							// Change State to 'IF'
							oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
							log.info("Status moving to IF:" + oCreditCardTransaction.getTemporyPaymentId() + " pnr : " + ipgQueryDTO.getPnr());
						}
					} else {
						// Change State to 'IP'
						oResultIP.add(oCreditCardTransaction.getTemporyPaymentId());
						log.info("Status moving to IP:" + oCreditCardTransaction.getTemporyPaymentId() + " pnr : " + ipgQueryDTO.getPnr());
					}
				}
				// No payment detail at bank
				else {
					// No Payments exist
					// Update status to 'II'
					log.info("No Payment Exist at Bank " + "pnr : " + ipgQueryDTO.getPnr());
					oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
					log.info("Status moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
				}
			} catch (ModuleException e) {
				log.error("Scheduler::ResolvePayment FAILED!!! " + e.getMessage(), e);
			}
		}

		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF, oResultIF);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_II, oResultII);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IP, oResultIP);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS, oResultIS);

		sr.setSuccess(true);

		log.info("[PaymentBrokerCCAvenueTemplate::resolvePartialPayments()] End");
		return sr;

	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}
	
	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		
		log.info("[PaymentBrokerCCAvenueTemplate::handleDeferredResponse()] Begin");
		
		if (AppSysParamsUtil.enableServerToServerMessages() && AppSysParamsUtil.isConfirmOnholdReservationByServerToServerMessages()) {			

			int transactionRefNo = ipgResponseDTO.getPaymentBrokerRefNo();
			CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(transactionRefNo);
			ReservationBD reservationBD = ReservationModuleUtils.getReservationBD();
			boolean isReservationConfirmationSuccess = false;
			
			if (oCreditCardTransaction != null) {
				
				String responseData = receiptyMap.get(CCAvenueResponse.ENCRESP);
				TempPaymentTnx tmpPayment = ReservationModuleUtils.getReservationBD().loadTempPayment(oCreditCardTransaction.getTemporyPaymentId());			
				
				log.info("[handleDeferredResponse()] PNR : " + tmpPayment.getPnr() + " is loaded");
				CCAvenueResponse res = CCAvenuePaymentUtils.processDynamicEventNotification(responseData);
				
				BigDecimal igpCurrencyAmount = tmpPayment.getPaymentCurrencyAmount();
				BigDecimal responseAmount = new BigDecimal(res.getAmount());
				
				if (tmpPayment != null && (igpCurrencyAmount.compareTo(responseAmount) == 0) 
						&& res.getCurrency().equals(tmpPayment.getPaymentCurrencyCode())) {
					
					if (oCreditCardTransaction.getTransactionResultCode() != 1) {
						
						ServiceResponce serviceResponce = null;						
						IPGConfigsDTO ipgConfigsDTO = OgonePaymentUtils.getIPGConfigs(getMerchantId(), getUser(), getPassword());
						ipgConfigsDTO.setAccessCode(getAccessCode());
						ipgConfigsDTO.setRefunding(false); 
						
						PaymentQueryDR query = getPaymentQueryDR();
						serviceResponce = query.query(oCreditCardTransaction,ipgConfigsDTO,PaymentBrokerInternalConstants.QUERY_CALLER.SCHEDULER);
						
						log.info("[handleDeferredResponse()] serviceResponce " + serviceResponce.getResponseCode());
						
						if(serviceResponce.isSuccess()){
							/* Save Card Transaction response Details */
							Date requestTime = reservationBD.getPaymentRequestTime(oCreditCardTransaction.getTemporyPaymentId());
							Date now = Calendar.getInstance().getTime();
							String strResponseTime = CalendarUtil.getTimeDifference(requestTime, now);
							long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(requestTime, now);
		
							oCreditCardTransaction.setComments(strResponseTime);
							oCreditCardTransaction.setResponseTime(timeDiffInMillis);
							oCreditCardTransaction.setTransactionResultCode(1);
							oCreditCardTransaction.setDeferredResponseText(PaymentBrokerUtils.getRequestDataAsString(receiptyMap));
							oCreditCardTransaction.setTransactionId(res.getTracking_id());
							PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);
		
							IPGQueryDTO ipgQueryDTO = reservationBD.getTemporyPaymentInfo(oCreditCardTransaction.getTransactionRefNo());							
								
							if (ipgQueryDTO != null) {
								
								/* Update with payment response details */
								LCCClientPnrModesDTO modes = new LCCClientPnrModesDTO();
								modes.setPnr(ipgQueryDTO.getPnr());
								modes.setRecordAudit(false);
								
								ipgQueryDTO.setPaymentType(getPaymentType(getStandardCardType(CARD_TYPE_GENERIC)));
								
								/** NO RESERVATION OR CANCELED RESERVATION */
								if((!isReservationExist(ipgQueryDTO) || ReservationInternalConstants.ReservationStatus.CANCEL.equals(ipgQueryDTO.getPnrStatus()))){
									
									Collection<IPGQueryDTO> col = new ArrayList<IPGQueryDTO>();
									col.add(ipgQueryDTO);
										
									log.info("[handleDeferredResponse()] Refunding Payment -> No Reservation has been created :" + ipgQueryDTO.getPnr());
									this.resolvePartialPayments(col, AppSysParamsUtil.isQueryDRRefund());
									ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_REJECTED);
									return;
								}
								
								/** ONHOLD RESERVATION */
								if(ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(ipgQueryDTO.getPnrStatus())){
									
									isReservationConfirmationSuccess = ConfirmReservationUtil.isReservationConfirmationSuccess(ipgQueryDTO);
									
									if(!isReservationConfirmationSuccess){
										Collection<IPGQueryDTO> col = new ArrayList<IPGQueryDTO>();
										col.add(ipgQueryDTO);
										
										log.info("[handleDeferredResponse()] Refunding Payment -> Onhold confirmation failed :" + isReservationConfirmationSuccess);
										this.resolvePartialPayments(col, AppSysParamsUtil.isQueryDRRefund());
										ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_REJECTED);											
										return;
									}else{
										log.info("[handleDeferredResponse()] Onhold Reservation Confirmation Success : " + ipgQueryDTO.getPnr());
									}
									
									ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_ACCEPTED);
								}
							}
							
						}else{
							log.info("[handleDeferredResponse()]  Order status is not \"Shipped\" nor \"Successful\" : CCAvenue Ref No " + res.getTracking_id());
						} 
					}											
				}
	
			} else {
				log.error("Invalid credit card transation:" + ipgResponseDTO.getPaymentBrokerRefNo());
			}
			
		} else {
			log.error("Invalid credit card transation:" + ipgResponseDTO.getPaymentBrokerRefNo());
		}
	
		log.info("[PaymentBrokerCCAvenueTemplate::handleDeferredResponse()] End");
	}

	/**
	 * @return the encryptionKey
	 */
	public String getEncryptionKey() {
		return encryptionKey;
	}

	/**
	 * @param encryptionKey the encryptionKey to set
	 */
	public void setEncryptionKey(String encryptionKey) {
		this.encryptionKey = encryptionKey;
	}

	/**
	 * @return the accessCode
	 */
	public String getAccessCode() {
		return accessCode;
	}

	/**
	 * @param accessCode the accessCode to set
	 */
	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	/**
	 * @return the serverToServerReqURL
	 */
	public String getServerToServerReqURL() {
		return serverToServerReqURL;
	}

	/**
	 * @param serverToServerReqURL the serverToServerReqURL to set
	 */
	public void setServerToServerReqURL(String serverToServerReqURL) {
		this.serverToServerReqURL = serverToServerReqURL;
	}

	public String getCcAvenueCardType() {
		return ccAvenueCardType;
	}

	public void setCcAvenueCardType(String ccAvenueCardType) {
		this.ccAvenueCardType = ccAvenueCardType;
	}

	public String getCcAvenuePaymentOption() {
		return ccAvenuePaymentOption;
	}

	public void setCcAvenuePaymentOption(String ccAvenuePaymentOption) {
		this.ccAvenuePaymentOption = ccAvenuePaymentOption;
	}

	protected int getStandardCardType(String card) {
		// [1-MASTER, 2-VISA, 3-AMEX, 4-DINNERS, 5-GENERIC, 6-OFFLINE]
		int mapType = 5; // default card type
		if (card != null && !card.equals("") && mapCardType.get(card) != null) {
			String mapTypeStr = (String) mapCardType.get(card);
			mapType = Integer.parseInt(mapTypeStr);
		}
		return mapType;
	}
	
	protected PaymentType getPaymentType(int cardID) {

		PaymentType paymentType = PaymentType.CARD_GENERIC;
		switch (cardID) {
		case 1:
			paymentType = PaymentType.CARD_MASTER;
			break;
		case 2:
			paymentType = PaymentType.CARD_VISA;
			break;
		case 3:
			paymentType = PaymentType.CARD_AMEX;
			break;
		case 4:
			paymentType = PaymentType.CARD_DINERS;
			break;
		case 5:
			paymentType = PaymentType.CARD_GENERIC;
			break;
		case 6:
			paymentType = PaymentType.CARD_CMI;
			break;
		}
		return paymentType;
	}
	
	private static boolean isReservationExist(IPGQueryDTO ipgQueryDTO) {
		boolean isExist = false;
		if (ipgQueryDTO != null) {
			if (ipgQueryDTO.getPnr() != null && ipgQueryDTO.getPnr().trim().length() != 0) {
				isExist = true;
			}
		}
		return isExist;
	}

	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}


}