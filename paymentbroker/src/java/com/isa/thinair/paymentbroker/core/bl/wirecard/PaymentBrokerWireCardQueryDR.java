package com.isa.thinair.paymentbroker.core.bl.wirecard;

import java.util.List;
import java.util.ResourceBundle;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ep.engine.model.Payment;
import com.ep.engine.model.Status;
import com.ep.engine.model.TransactionState;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.platform.api.ServiceResponce;

public class PaymentBrokerWireCardQueryDR extends PaymentBrokerTemplate implements PaymentQueryDR {

	private static final String ERROR_CODE_PREFIX = "wirecard.";

	private static Log log = LogFactory.getLog(PaymentBrokerWireCardQueryDR.class);

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		PaymentBrokerManager.validateIPGIdentificationParams(this.ipgIdentificationParamsDTO);
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	@Override
	public ServiceResponce query(CreditCardTransaction wcCreditCardTransaction, IPGConfigsDTO ipgConfigsDTO, QUERY_CALLER caller)
			throws ModuleException {

		log.debug("[PaymentBrokerWIRECARDQueryDR::query()] Begin");
		Payment payment = null;
		int tnxResultCode;
		String errorCode = "";
		String transactionMsg;
		String status = null;
		String errorSpecification = null;
		String paymentResponseXMLString = "";

		String merchantTxnReference = wcCreditCardTransaction.getTempReferenceNum();

		String queryParamString = "RefNo:" + wcCreditCardTransaction.getTransactionId() + ",MerchantId:" + getMerchantId();

		CreditCardTransaction ccTransaction = auditTransaction(wcCreditCardTransaction.getTransactionReference(),
				getMerchantId(), merchantTxnReference, new Integer(wcCreditCardTransaction.getTemporyPaymentId()),
				bundle.getString("SERVICETYPE_QUERY"), queryParamString, "", getPaymentGatewayName(), null, false);

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		if (wcCreditCardTransaction.getTransactionId() != null) {
			log.debug("Query Request Params : " + queryParamString);

			// Build your payment payload
			String URL = getQueryURL() + getMerchantId() + "/payments/" + wcCreditCardTransaction.getTransactionId();

			GetMethod get = new GetMethod(URL);
			get.addRequestHeader("Content-Type", "application/xml");
			get.addRequestHeader("Accept", "application/xml");

			try {
				HttpClient client = WireCardPaymentUtils.composeHttpClient(getKeystoreName(), getKeyStorePassword(), getUser(),
						getPassword());
				int result = client.executeMethod(get);
				if (log.isDebugEnabled()) {
					log.debug("Response HTTP status code: " + result);
					log.debug("--Payment Response--");
				}
				paymentResponseXMLString = BeanUtils.nullHandler(get.getResponseBodyAsString());
				payment = WireCardPaymentUtils.getPaymentObject(paymentResponseXMLString);

				if (log.isDebugEnabled()) {
					log.debug("Query Response : " + paymentResponseXMLString);
				}
			} catch (Exception e) {
				errorSpecification = BeanUtils.nullHandler(e.getMessage());
				log.error("Exception occurred: ", e);
			} finally {
				get.releaseConnection();
			}

			if (payment != null && payment.getTransactionState() == TransactionState.SUCCESS) {
				status = IPGResponseDTO.STATUS_ACCEPTED;
				errorCode = "";
				transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
				tnxResultCode = 1;
				errorSpecification = "";
				sr.setSuccess(true);
				sr.addResponceParam(PaymentConstants.AMOUNT, payment.getRequestedAmount().getValue());
				sr.addResponceParam(PaymentConstants.REQUEST_ID, payment.getRequestId());
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, PaymentConstants.SERVICE_RESPONSE_QUERYDR);
				sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE, payment.getTransactionId());

			} else {
				status = IPGResponseDTO.STATUS_REJECTED;
				tnxResultCode = 0;

				if (payment != null) {
					Status ipgStatus = BeanUtils.getFirstElement(payment.getStatuses().getStatuses());

					errorCode = ERROR_CODE_PREFIX + ipgStatus.getCode();
					transactionMsg = ipgStatus.getDescription();
					errorSpecification = "ERROR_CODE[" + errorCode + "], ERROR_DESC[" + ipgStatus.getDescription() + "] ";
					errorSpecification = "Status:" + status + " Error code:" + errorCode + " Error Desc:" + "" + " Caller:"
							+ caller.toString();
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);

					if (log.isDebugEnabled()) {
						List<Status> statusList = payment.getStatuses() != null ? payment.getStatuses().getStatuses() : null;
						if (statusList != null && statusList.size() > 0) {
							for (Status errorStatus : statusList) {
								log.debug("Transaction Status Code: " + errorStatus.getCode());
								log.debug("Transaction Status Description: " + errorStatus.getDescription());
							}
						}
					}
				} else {
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, errorSpecification);
				}
			}

			updateAuditTransactionByTnxRefNo(ccTransaction.getTransactionRefNo(), paymentResponseXMLString, errorSpecification,
					payment.getAuthorizationCode(), payment.getTransactionId(), tnxResultCode);// HERE ==>
																								// payment.getAuthorizationCode()
																								// = null

			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, wcCreditCardTransaction.getTransactionId());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
			sr.addResponceParam(PaymentConstants.STATUS, payment.getTransactionState());

			log.debug("[PaymentBrokerSamanQueryDR::query()] End");
		}
		return sr;
	}
}
