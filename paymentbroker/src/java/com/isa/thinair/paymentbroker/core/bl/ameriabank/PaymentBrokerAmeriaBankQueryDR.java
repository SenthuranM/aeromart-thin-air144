package com.isa.thinair.paymentbroker.core.bl.ameriabank;

import java.math.BigDecimal;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.constants.CommonsConstants.ErrorType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankCommonRequest;
import com.isa.thinair.paymentbroker.api.dto.ameriabank.AmeriaBankPaymentFieldsResponse;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.wsclient.api.service.WSClientBD;

public class PaymentBrokerAmeriaBankQueryDR extends PaymentBrokerAmeriaBankTemplate implements PaymentQueryDR {

	private static Log log = LogFactory.getLog(PaymentBrokerAmeriaBankQueryDR.class);

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	@Override
	public ServiceResponce query(CreditCardTransaction oCreditCardTransaction, IPGConfigsDTO ipgConfigsDTO,
			QUERY_CALLER operCaller) throws ModuleException {

		String errorCode = "";
		String errorDescription = "";
		String status;
		int tnxResultCode = 0;

		log.debug("[PaymentBrokerAmeriaBankDR::query()] Begin");

		BigDecimal paymentCurrencyAmount = ReservationModuleUtils.getReservationBD().getTempPaymentCurrencyAmount(
				oCreditCardTransaction.getTemporyPaymentId());

		AmeriaBankCommonRequest ameriaBankPaymentFieldsRequest = AmeriaBankPaymentUtil.buildPaymentFieldsRequest(getMerchantId(),
				getUser(), getPassword(), paymentCurrencyAmount, oCreditCardTransaction.getTemporyPaymentId());

		CreditCardTransaction ccTransaction = auditTransaction(oCreditCardTransaction.getTransactionReference(), getMerchantId(),
				oCreditCardTransaction.getTempReferenceNum(), new Integer(oCreditCardTransaction.getTemporyPaymentId()),
				bundle.getString("SERVICETYPE_QUERY"), ameriaBankPaymentFieldsRequest.toString(), "", getPaymentGatewayName(),
				null, false);

		DefaultServiceResponse serviceResponse = new DefaultServiceResponse(false);

		WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();

		AmeriaBankPaymentFieldsResponse ameriaBankPaymentFieldsResponse = ebiWebervices
				.getAmeriaBankPaymentFields(ameriaBankPaymentFieldsRequest);

		if (ameriaBankPaymentFieldsResponse == null) {
			ModuleException me = new ModuleException("paymentbroker.ameria.payment.fields.response.invalid");
			me.setErrorType(ErrorType.PAYMENT_ERROR);
			throw me;
		}

		if (!AmeriaBankPaymentUtil.COMMON_RESP_SUCCESS_CODE.contentEquals(ameriaBankPaymentFieldsResponse.getRespCode())) {

			errorCode = ameriaBankPaymentFieldsResponse.getRespCode();
			errorDescription = ameriaBankPaymentFieldsResponse.getRespMessage();

			AmeriaBankPaymentUtil.getErrorDescription(errorCode, errorDescription);
			status = IPGResponseDTO.STATUS_REJECTED;

		} else {

			try {

				// opaque set in paymentFlow only
				boolean isPaymentFlow = ipgConfigsDTO.getOpaque() != null;

				AmeriaBankPaymentUtil.validatePaymentFieldsResponse(ameriaBankPaymentFieldsResponse,
						paymentCurrencyAmount, ipgConfigsDTO.getOpaque(),
						ipgConfigsDTO.getPaymentCurrency(), isPaymentFlow);

				tnxResultCode = 1;
				status = IPGResponseDTO.STATUS_ACCEPTED;
				serviceResponse.setSuccess(true);

			} catch (ModuleException e) {
				status = IPGResponseDTO.STATUS_REJECTED;
				errorDescription = e.getExceptionCode();
				errorCode = ameriaBankPaymentFieldsResponse.getPaymentState();
				log.error("Error while Ameria query processing", e);
			}

		}

		updateAuditTransactionByTnxRefNo(ccTransaction.getTransactionRefNo(), ameriaBankPaymentFieldsResponse.toString(),
				errorDescription, oCreditCardTransaction.getTransactionId(), oCreditCardTransaction.getTransactionId(),
				tnxResultCode);

		serviceResponse.addResponceParam(AmeriaBankPaymentUtil.AMERIA_PAYMENT_FIELDS_AUTH_CODE,
				ameriaBankPaymentFieldsResponse.getAuthCode());
		serviceResponse.addResponceParam(AmeriaBankPaymentUtil.AMERIA_PAYMENT_FIELDS_OPAQUE,
				ameriaBankPaymentFieldsResponse.getOpaque());
		serviceResponse.addResponceParam(AmeriaBankPaymentUtil.AMERIA_PAYMENT_FIELDS_ORDER_ID,
				ameriaBankPaymentFieldsResponse.getOrderId());
		serviceResponse.addResponceParam(AmeriaBankPaymentUtil.AMERIA_PAYMENT_FIELDS_STATUS, status);
		serviceResponse.addResponceParam(AmeriaBankPaymentUtil.AMERIA_PAYMENT_FIELDS_TNX_RESULTS_CODE, tnxResultCode);
		serviceResponse.addResponceParam(AmeriaBankPaymentUtil.AMERIA_PAYMENT_FIELDS_ERROR_CODE, errorCode);
		serviceResponse.addResponceParam(AmeriaBankPaymentUtil.AMERIA_PAYMENT_FIELDS_ERROR_DESC, errorDescription);

		log.debug("[PaymentBrokerAmeriaBankQueryDR::query() : Query Status :  " + status + "] ");
		log.debug("[PaymentBrokerAmeriaBankQueryDR::query()] End");

		return serviceResponse;
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		PaymentBrokerManager.validateIPGIdentificationParams(this.ipgIdentificationParamsDTO);
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}
}
