package com.isa.thinair.paymentbroker.core.bl.payFort.v2;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.payFort.PayFortOnlineParam;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public class PaymentBrokerPayFortInvoicingService extends PaymentBrokerPayFortOfflineTemplate {

    private static final ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();
    private static final Log log = LogFactory.getLog(PaymentBrokerPayFortPayOffline.class);

    private static final String INVOICE_GENERATION_NOTIFICATION_VM = "payfort_invoice_generation_notification";
    private static final String PAY_FORT_PAYMENT_LINK = "payFortPaymentLink";

    private String notificationType = "NONE";

    private List<String> supportedLanguagesForNotificationMail;

    @Override
    public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
        log.debug("[PaymentBrokerPayFortInvoicingService::getRequestData()] Begin ");

        String merchantRefID = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
                ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);
        boolean isPaymentProcessedSuccessfully = false;

        Map<String, String> postDataMap = getPostDataMap(ipgRequestDTO, merchantRefID);
        String sessionID = StringUtils.defaultString(ipgRequestDTO.getSessionID());
        String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);
        IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();

        CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantRefID,
                ipgRequestDTO.getApplicationTransactionId(), bundle.getString("SERVICETYPE_AUTHORIZE"),
                strRequestParams + "," + sessionID, "", ipgRequestDTO.getIpgIdentificationParamsDTO().getFQIPGConfigurationName(),
                null, false);

        Map<String, String> response = getResponseFromPayFort(postDataMap, getIpgURL());

        if (!CollectionUtils.isEmpty(response)) {
            ccTransaction.setResponseText(PaymentBrokerUtils.getRequestDataAsString(response));
            ccTransaction.setTransactionId(response.get(PayFortOnlineParam.PAYMENT_LINK_ID.getName()));
            // Please check here status is success
            if (isSignatureValid(response, getShaResponsePhrase()) && INVOICE_GENERATION_SUCCESS
                    .equals(response.get(PayFortOnlineParam.STATUS.getName()))) {
                // For display purpose
                logIfDebugEnabled("PaymentBrokerPayFortInvoicingService PG Request Data : " + strRequestParams + ", sessionID : "
                        + sessionID);
                ipgRequestResultsDTO.setRequestData(strRequestParams);
                ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
                ipgRequestResultsDTO.setPostDataMap(response);
                sendPaymentLinkToContact(response, ipgRequestDTO);
                isPaymentProcessedSuccessfully = true;

            } else {
                ccTransaction.setErrorSpecification(StringUtils
                        .defaultString(response.get(PayFortOnlineParam.RESPONSE_CODE.getName()), "Signature Mismatch"));
                ipgRequestResultsDTO.setPaymentBrokerRefNo(0);
                ipgRequestResultsDTO.setErrorCode(StringUtils
                        .defaultString(response.get(PayFortOnlineParam.RESPONSE_MESSAGE.getName()), "Signature Mismatch"));
                log.error(
                        "PaymentBrokerPayFortInvoicingService PG Request Data fails with error message : " + ipgRequestResultsDTO
                                .getErrorCode());
            }
            PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(ccTransaction);
        }
        ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantRefID);
        log.debug("PayFort Invoice Generation Request Data : " + strRequestParams + ", sessionID : " + sessionID);

        if (!isPaymentProcessedSuccessfully) {
            throw new ModuleException("paymentbroker.payfort.invoice.request.failed");
        }

        return ipgRequestResultsDTO;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    private void sendPaymentLinkToContact(Map<String, String> response, IPGRequestDTO ipgRequestDTO) {
        HashMap map = new HashMap();
        map.put(FIRST_NAME, StringUtils.capitalize(ipgRequestDTO.getContactFirstName()));
        map.put(PAY_FORT_PAYMENT_LINK, response.get(PayFortOnlineParam.PAYMENT_LINK.getName()));
        map.put(PNR, ipgRequestDTO.getPnr());
        map.put(CARRIER_NAME, AppSysParamsUtil.getDefaultCarrierName());
        map.put(VALID_PERIOD, PaymentBrokerPayFortUtils.getRemainingTimeFromNow(ipgRequestDTO.getInvoiceExpirationTime()));
        PaymentBrokerPayFortUtils.populateRemainingDaysHoursAndMin(ipgRequestDTO.getInvoiceExpirationTime(), map);
        PaymentBrokerPayFortUtils
                .sendEmail(INVOICE_GENERATION_NOTIFICATION_VM, map, ipgRequestDTO, getLocaleForEmail(ipgRequestDTO));
    }

    private Locale getLocaleForEmail(IPGRequestDTO ipgRequestDTO) {
        if (!CollectionUtils.isEmpty(supportedLanguagesForNotificationMail) && StringUtils
                .isNoneBlank(ipgRequestDTO.getPreferredLanguage())) {
            for (String locale : supportedLanguagesForNotificationMail) {
                if (StringUtils.equalsIgnoreCase(locale, ipgRequestDTO.getPreferredLanguage())) {
                    return LocaleUtils.toLocale(locale);
                }
            }

        }
        return null;
    }

    private Map<String, String> getPostDataMap(IPGRequestDTO ipgRequestDTO, String refNo) throws ModuleException {
        Map<String, String> postDataMap = new LinkedHashMap<>();
        postDataMap.put(PayFortOnlineParam.SERVICE_COMMAND.getName(), "PAYMENT_LINK");
        postDataMap.put(PayFortOnlineParam.ACCESS_CODE.getName(), getMerchantAccessCode());
        postDataMap.put(PayFortOnlineParam.AMOUNT.getName(), getISOFormattedAmount(ipgRequestDTO.getAmount()));
        postDataMap.put(PayFortOnlineParam.MERCHANT_IDENTIFIER.getName(), getMerchantId());
        postDataMap.put(PayFortOnlineParam.MERCHANT_REFERENCE.getName(), refNo);
        postDataMap.put(PayFortOnlineParam.REQUEST_EXPIRY_DATE.getName(),
                PaymentBrokerPayFortUtils.getFormattedDate(ipgRequestDTO.getInvoiceExpirationTime()));
        postDataMap.put(PayFortOnlineParam.CURRENCY.getName(),
                ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode());
        postDataMap.put(PayFortOnlineParam.LANGUAGE.getName(), getLocale());
        postDataMap.put(PayFortOnlineParam.CUSTOMER_EMAIL.getName(), ipgRequestDTO.getContactEmail());
        postDataMap.put(PayFortOnlineParam.NOTIFICATION_TYPE.getName(), notificationType);
        postDataMap.put(PayFortOnlineParam.SIGNATURE.getName(), getSignature(postDataMap, getShaRequestPhrase()));
        return postDataMap;
    }

    @Override
    public boolean isEnableRefundByScheduler() {
        return true;
    }

    public void setSupportedLanguagesForNotificationMail(List<String> supportedLanguagesForNotificationMail) {
        this.supportedLanguagesForNotificationMail = supportedLanguagesForNotificationMail;
    }
}
