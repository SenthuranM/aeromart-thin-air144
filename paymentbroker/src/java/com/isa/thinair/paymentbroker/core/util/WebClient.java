package com.isa.thinair.paymentbroker.core.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.CommonsException;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;

public class WebClient {

	private String proxyHost;

	private String proxyPort;

	private static final Log logger = LogFactory.getLog(WebClient.class);

	private static final int RETRY_COUNT = 0;

	public WebClient(String proxyHost, String proxyPort) {
		this.proxyHost = proxyHost;
		this.proxyPort = proxyPort;
	}

	public String get(String url) throws CommonsException {
		HttpClient client = new HttpClient();
		return get(client, url);
	}

	public String get(HttpClient client, String url) throws CommonsException {
		debug();
		String responseBody = null;
		GetMethod method = new GetMethod(url);
		method.setFollowRedirects(true);
		responseBody = sendRequest(client, method);

		return responseBody;

	}

	public String post(String url, Map mapParam) throws CommonsException {
		HttpClient client = new HttpClient();
		return post(client, mapParam, url);
	}

	public String post(HttpClient client, Map mapParam, String url) throws CommonsException {
		debug();
		PostMethod postMethod = new PostMethod(url);

		Set colKeys = mapParam.keySet();
		String responseBody = null;

		for (Iterator iter = colKeys.iterator(); iter.hasNext();) {
			String key = (String) iter.next();
			postMethod.addParameter(key, (String) mapParam.get(key));
		}

		responseBody = sendRequest(client, postMethod);

		return responseBody;
	}

	private String sendRequest(HttpClient client, HttpMethodBase method) throws CommonsException {
		HttpMethodParams httpMethodParams = null;
		int statusCode = 0;

		String responseBody = null;

		if (proxyHost != null && !"".equals(proxyHost) && proxyPort != null && !"".equals(proxyPort)) {
			client.getHostConfiguration().setProxy(proxyHost, Integer.parseInt(proxyPort));
		}

		method.setRequestHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0b; Windows 98)");

		// Provide custom retry handler is necessary
		// httpMethodParams = method.getParams();
		// httpMethodParams.setParameter(HttpMethodParams.RETRY_HANDLER, new
		// DefaultHttpMethodRetryHandler(RETRY_COUNT, false));

		try {
			statusCode = client.executeMethod(method);
			responseBody = extractResponse(method, statusCode);

			if (responseBody == null && statusCode == 302) {
				Header locationHeader = method.getResponseHeader("location");

				if (locationHeader != null) {
					String redirectLocation = locationHeader.getValue();

					method.releaseConnection();

					// URI uri = new URI(redirectLocation, true);
					HttpMethodBase redirectMethod = new PostMethod(redirectLocation);

					responseBody = sendRequest(client, redirectMethod);
					redirectMethod.addRequestHeader("", method.getResponseHeader("").getValue());

				} else {
					throw new CommonsException(PaymentBrokerInternalConstants.MessageCodes.REDIRECT_URL_NULL,
							PaymentBrokerInternalConstants.MODULE_DESC);
				}
			}
		} catch (Exception e) {

		}

		// responseBody = extractResponse(method, statusCode);

		// try {
		// statusCode = client.executeMethod(method);
		// responseBody = extractResponse(method, statusCode);
		//
		// if (responseBody == null && statusCode == 302) {
		// Header locationHeader = method.getResponseHeader("location");
		// String redirectLocation = null;
		//
		// if (locationHeader != null) {
		// redirectLocation = locationHeader.getValue();
		// method.setURI(new URI(redirectLocation, true));
		//
		// statusCode = client.executeMethod(method);
		// responseBody = extractResponse(method, statusCode);
		// } else {
		// throw new
		// CommonsException(PaymentBrokerInternalConstants.MessageCodes.REDIRECT_URL_NULL,
		// PaymentBrokerInternalConstants.MODULE_DESC);
		// }
		// }
		// } catch (Exception e) {
		// throw new CommonsException(e,
		// PaymentBrokerInternalConstants.MODULE_DESC);
		// } finally {
		// method.releaseConnection();
		// }

		return responseBody;

	}

	private String extractResponse(HttpMethodBase method, int statusCode) throws IOException {
		String responseBody = null;
		InputStream is = null;

		is = method.getResponseBodyAsStream();

		if (statusCode == HttpStatus.SC_OK) {
			responseBody = IOUtil.getString(is);
			return responseBody;
		}

		return responseBody;
	}

	private static void debug() {
		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
		System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
		System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire.header", "debug");
		// System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire",
		// "debug");
		System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "debug");

	}
}
