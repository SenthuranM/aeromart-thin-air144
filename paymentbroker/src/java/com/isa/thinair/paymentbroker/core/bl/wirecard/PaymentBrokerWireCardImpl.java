package com.isa.thinair.paymentbroker.core.bl.wirecard;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ep.engine.model.Payment;
import com.ep.engine.model.Status;
import com.ep.engine.model.TransactionState;
import com.ep.engine.model.TransactionType;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.XMLStreamer;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.api.dto.XMLResponse;

public class PaymentBrokerWireCardImpl extends PaymentBrokerTemplate implements PaymentBroker {

	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();
	private static Log log = LogFactory.getLog(PaymentBrokerWireCardImpl.class);

	private static final String ERROR_CODE_PREFIX = "wirecard.";

	protected static final String PAYMENT_SUCCESS = "9";

	protected static final String SUCCESS = "SUCCESS";

	protected static final String REFUND_PENDING = "81";

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		return getReverseRefundResponse(creditCardPayment, pnr, appIndicator, tnxMode, false);
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		return getReverseRefundResponse(creditCardPayment, pnr, appIndicator, tnxMode, true);
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	private String getRequestID(String pnr, String merchantTxnId) {
		SimpleDateFormat format = new SimpleDateFormat("ddMMyyyyHHmmss");
		String currentTime = format.format(CalendarUtil.getCurrentZuluDateTime());
		return pnr + currentTime + merchantTxnId;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		String finalAmount = ipgRequestDTO.getAmount();
		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		postDataMap.put(WireCardRequest.PSPID, getMerchantId());
		postDataMap.put(WireCardRequest.REQUESTID, getRequestID(ipgRequestDTO.getPnr(), merchantTxnId));
		postDataMap.put(WireCardRequest.USERID, getUser());
		postDataMap.put(WireCardRequest.PASSWORD, getPassword());
		postDataMap.put(WireCardRequest.ORDERNUMBER, ipgRequestDTO.getPnr() + merchantTxnId);
		postDataMap.put(WireCardRequest.ORDERDETAIL, ipgRequestDTO.getDefaultCarrierName() + " ticket " + ipgRequestDTO.getPnr());

		postDataMap.put(WireCardRequest.KEYSTORE_LOCATION, PlatformConstants.getConfigRootAbsPath()
				+ "/repository/modules/paymentbroker/" + getKeystoreName());
		postDataMap.put(WireCardRequest.PROXY_HOST, getProxyHost());
		postDataMap.put(WireCardRequest.PROXY_PORT, getProxyPort());
		postDataMap.put(WireCardRequest.IPG_URL, getIpgURL());
		postDataMap.put(WireCardRequest.USE_PROXY, isUseProxy() ? "Y" : "N");
		postDataMap.put(WireCardRequest.FLAG3D, "N");
		postDataMap.put(WireCardRequest.RESTRICT_ATTEMPT_3D, isRestrictAttempt3D() ? "Y" : "N");
		postDataMap.put(WireCardRequest.AMOUNT, finalAmount);
		postDataMap.put(WireCardRequest.CURRENCY, ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode());
		postDataMap.put(WireCardRequest.LANGUAGE, ipgRequestDTO.getSessionLanguageCode());

		String requestDataForm = WireCardPaymentUtils.getPostInputDataFormHTML(postDataMap, getKeyStoreAlias(),
				getKeyStorePassword(), getKeyStoreKeyPassword(), ipgRequestDTO);

		postDataMap.put(WireCardRequest.HTTP_USER_AGENT, ipgRequestDTO.getUserAgent());

		String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();

		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				strRequestParams + "," + sessionID, "", getPaymentGatewayName(), null, false);

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(requestDataForm);
		ipgRequestResultsDTO.setAccelAeroTransactionRef(ipgRequestDTO.getPnr() + merchantTxnId);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setPostDataMap(postDataMap);

		return ipgRequestResultsDTO;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());
		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time
		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

		Payment payment = WireCardPaymentUtils.getPaymentResponse(BeanUtils.nullHandler(receiptyMap
				.get(WireCardPaymentUtils.PAYMENT_RESPONSE_HASH)));

		int tnxResultCode;
		String status;
		String errorCode = "";
		String errorSpecification = "";
		String orderNumber = "";
		String authorizationCode = "";
		String ipgTransactionId = "";
		String maskedCCNumber = "";

		if (payment != null && payment.getTransactionState() == TransactionState.SUCCESS) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			tnxResultCode = 1;
			orderNumber = payment.getOrderNumber();
			authorizationCode = payment.getAuthorizationCode();
			ipgTransactionId = payment.getTransactionId();
			if (payment.getCardToken() != null) {
				maskedCCNumber = payment.getCardToken().getMaskedAccountNumber();
			}
		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;

			if (payment != null) {
				Status ipgStatus = BeanUtils.getFirstElement(payment.getStatuses().getStatuses());
				errorCode = ERROR_CODE_PREFIX + ipgStatus.getCode();
				errorSpecification = "ERROR_CODE[" + errorCode + "], ERROR_DESC[" + ipgStatus.getDescription() + "] ";
				orderNumber = payment.getOrderNumber();
				ipgTransactionId = payment.getTransactionId();
			}
		}

		int cardType = WireCardPaymentUtils.getCardType(maskedCCNumber);

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setApplicationTransactionId(orderNumber);
		ipgResponseDTO.setAuthorizationCode(authorizationCode);
		ipgResponseDTO.setCardType(cardType);

		if (maskedCCNumber.length() > 4) {
			ipgResponseDTO.setCcLast4Digits(maskedCCNumber.substring(maskedCCNumber.length() - 4));
		}
		String response = payment != null ? XMLStreamer.compose(payment) : "";

		// TODO Check these params are correct. I am drunk now.
		updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), response, errorSpecification, authorizationCode,
				ipgTransactionId, tnxResultCode);

		return ipgResponseDTO;
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection creditCardPayment, boolean refundFlag) throws ModuleException {
		Iterator itColIPGQueryDTO = creditCardPayment.iterator();

		Collection wcResultIF = new ArrayList();
		Collection wcResultIP = new ArrayList();
		Collection wcResultII = new ArrayList();
		Collection wcResultIS = new ArrayList();

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		IPGQueryDTO ipgQueryDTO;
		CreditCardTransaction wcCreditCardTransaction;
		PaymentQueryDR wcNewQuery;
		PreviousCreditCardPayment wcPrevCCPayment;
		CreditCardPayment wcCCPayment;

		while (itColIPGQueryDTO.hasNext()) {
			ipgQueryDTO = (IPGQueryDTO) itColIPGQueryDTO.next();

			wcCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgQueryDTO.getPaymentBrokerRefNo());
			wcNewQuery = getPaymentQueryDR();

			try {
				// query(TPT_ID, PNR, MERCHANT_TRANSACTION_REF, MERCHANT_ID, MERCHANT_ACCESS_CODE )
				ServiceResponce serviceResponce = wcNewQuery.query(wcCreditCardTransaction,
						WireCardPaymentUtils.getIPGConfigs(getMerchantId(), getUser(), getPassword()),
						PaymentBrokerInternalConstants.QUERY_CALLER.SCHEDULER);

				// If successful payment remains at Bank
				if (serviceResponce.isSuccess()) {
					if (refundFlag) {
						// Do refund
						String txnNo = PlatformUtiltiies.nullHandler(serviceResponce
								.getResponseParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE));

						wcPrevCCPayment = new PreviousCreditCardPayment();
						wcPrevCCPayment.setPaymentBrokerRefNo(wcCreditCardTransaction.getTransactionRefNo());
						wcPrevCCPayment.setPgSpecificTxnNumber(txnNo);

						wcCCPayment = new CreditCardPayment();
						wcCCPayment.setPreviousCCPayment(wcPrevCCPayment);
						wcCCPayment.setAmount(ipgQueryDTO.getAmount().toString());
						wcCCPayment.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
						wcCCPayment.setPaymentBrokerRefNo(wcCreditCardTransaction.getTransactionRefNo());
						wcCCPayment.setIpgIdentificationParamsDTO(ipgQueryDTO.getIpgIdentificationParamsDTO());
						wcCCPayment.setPnr(wcCreditCardTransaction.getTransactionReference());
						wcCCPayment.setTemporyPaymentId(wcCreditCardTransaction.getTemporyPaymentId());

						ServiceResponce srRev = refund(wcCCPayment, wcCCPayment.getPnr(), wcCCPayment.getAppIndicator(),
								wcCCPayment.getTnxMode());

						if (srRev.isSuccess()) {
							// Change State to 'IS'
							wcResultIS.add(wcCreditCardTransaction.getTemporyPaymentId());
							if (log.isDebugEnabled()) {
								log.debug("Status moing to IS:" + wcCreditCardTransaction.getTemporyPaymentId());
							}
						} else {
							// Change State to 'IF'
							wcResultIF.add(wcCreditCardTransaction.getTemporyPaymentId());
							if (log.isDebugEnabled()) {
								log.debug("Status moing to IF:" + wcCreditCardTransaction.getTemporyPaymentId());
							}
						}
					} else {
						// Change State to 'IP'
						wcResultIP.add(wcCreditCardTransaction.getTemporyPaymentId());
						if (log.isDebugEnabled()) {
							log.debug("Status moing to IP:" + wcCreditCardTransaction.getTemporyPaymentId());
						}
					}
				}
				// No payment detail at bank
				else {
					// No Payments exist
					// Update status to 'II'
					wcResultII.add(wcCreditCardTransaction.getTemporyPaymentId());
					if (log.isDebugEnabled()) {
						log.debug("Status moing to II:" + wcCreditCardTransaction.getTemporyPaymentId());
					}
				}
			} catch (ModuleException e) {
				log.error("Scheduler::ResolvePayment FAILED!!! " + e.getMessage(), e);
			}
		}

		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF, wcResultIF);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_II, wcResultII);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IP, wcResultIP);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS, wcResultIS);

		sr.setSuccess(true);

		return sr;
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	public ServiceResponce cancel(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	private ServiceResponce getReverseRefundResponse(CreditCardPayment creditCardPayment, String pnr,
			AppIndicatorEnum appIndicator, TnxModeEnum tnxMode, boolean isReversePayment) throws ModuleException {
		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status;
		String errorSpecification;

		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

		if (creditCardPayment.getPreviousCCPayment() != null && getRefundEnabled() != null && getRefundEnabled().equals("true")) {
			if (log.isDebugEnabled()) {
				log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
				log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
				log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
				log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
				log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());
			}

			PaymentQueryDR queryDR = getPaymentQueryDR();
			ServiceResponce queryServiceResponce = queryDR.query(ccTransaction,
					WireCardPaymentUtils.getIPGConfigs(getMerchantId(), getUser(), getPassword()),
					PaymentBrokerInternalConstants.QUERY_CALLER.REFUND);
			DefaultServiceResponse sr = new DefaultServiceResponse(false);

			if (SUCCESS.equalsIgnoreCase(queryServiceResponce.getResponseParam(PaymentConstants.STATUS).toString())) {
				BigDecimal queryAmount = new BigDecimal(queryServiceResponce.getResponseParam(PaymentConstants.AMOUNT).toString());
				BigDecimal paidAmount = PaymentBrokerUtils.getPaymentBrokerDAO()
						.getTempPaymentInfo(ccTransaction.getTemporyPaymentId()).getPaymentCurrencyAmount();

				if ((isReversePayment) || (!isReversePayment && (paidAmount.doubleValue() == queryAmount.doubleValue()))) {
					ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
					if (!serviceResponse.isSuccess()) {
						return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
								ccTransaction.getPaymentGatewayConfigurationName());
					}

					Map<String, String> postDataMap = new LinkedHashMap<String, String>();
					postDataMap.put(WireCardRequest.PSPID, getMerchantId());
					if (getUser() != null && !getUser().equals("")) {
						postDataMap.put(WireCardRequest.USERID, getUser());
						postDataMap.put(WireCardRequest.PASSWORD, getPassword());
					}
					// TODO : At the moment partial refund is not implement
					if (isReversePayment) {
						postDataMap.put(WireCardRequest.OPERATION, TransactionType.VOID_PURCHASE.toString());
						postDataMap.put(WireCardRequest.REQUESTID,
								queryServiceResponce.getResponseParam(PaymentConstants.REQUEST_ID).toString() + "-"
										+ PaymentBrokerInternalConstants.QUERY_CALLER.VOID);
					} else {
						postDataMap.put(WireCardRequest.OPERATION, TransactionType.REFUND_PURCHASE.toString());
						postDataMap.put(WireCardRequest.REQUESTID,
								queryServiceResponce.getResponseParam(PaymentConstants.REQUEST_ID).toString() + "-"
										+ PaymentBrokerInternalConstants.QUERY_CALLER.REFUND);
					}

					postDataMap.put(WireCardRequest.TRANSACTION_ID, ccTransaction.getTransactionId());
					postDataMap.put(WireCardRequest.PSPID, getMerchantId());
					postDataMap.put(WireCardRequest.URL, getReversalURL());
					postDataMap.put(WireCardRequest.HOST_IP_ADDRESS, getHostIPAddress());

					String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

					// Add other required parameters to create the post request.
					postDataMap.put("PROXY_HOST", getProxyHost());
					postDataMap.put("PROXY_PORT", getProxyPort());
					postDataMap.put("USE_PROXY", isUseProxy() ? "Y" : "N");

					CreditCardTransaction ccNewTransaction = null;
					if (isReversePayment) {
						postDataMap.put(WireCardRequest.OPERATION, TransactionType.VOID_PURCHASE.toString());
						ccNewTransaction = auditTransaction(pnr, getMerchantId(), ccTransaction.getTempReferenceNum(),
								creditCardPayment.getTemporyPaymentId(), bundle.getString("SERVICETYPE_REVERSAL"),
								strRequestParams, "", getPaymentGatewayName(), null, false);

					} else {
						postDataMap.put(WireCardRequest.AMOUNT, paidAmount.toString());
						postDataMap.put(WireCardRequest.CURRENCY, creditCardPayment.getCurrency());
						postDataMap.put(WireCardRequest.OPERATION, TransactionType.REFUND_PURCHASE.toString());
						ccNewTransaction = auditTransaction(pnr, getMerchantId(), ccTransaction.getTempReferenceNum(),
								creditCardPayment.getTemporyPaymentId(), bundle.getString("SERVICETYPE_REFUND"),
								strRequestParams, "", getPaymentGatewayName(), null, false);
					}

					Integer paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());
					XMLResponse response = WireCardPaymentUtils.getVoidRefundXMLResponse(postDataMap, getKeystoreName(),
							getKeyStorePassword());

					if (SUCCESS.equals(response.getStatus())) {
						status = IPGResponseDTO.STATUS_ACCEPTED;
						tnxResultCode = 1;
						transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
						errorCode = "";
						errorSpecification = "";
						sr.setSuccess(true);
						sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
								PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
					} else {
						status = IPGResponseDTO.STATUS_REJECTED;
						transactionMsg = response.getNcError();
						tnxResultCode = 0;
						errorCode = ERROR_CODE_PREFIX + response.getNcStatus();
						errorSpecification = "Status:" + status + " Error code:" + errorCode + " Description:" + transactionMsg;
						sr.setSuccess(false);
						sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
					}

					updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), response.getRawXMLResponse(),
							errorSpecification, response.getAcceptance(), response.getPayId(), tnxResultCode);

					sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, response.getAcceptance());
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
				} else {
					throw new ModuleException("paymentbroker.generic.operation.notsupported");
				}
			}
			return sr;
		} else {
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);

			return sr;
		}
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		// TODO Auto-generated method stub

	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
}