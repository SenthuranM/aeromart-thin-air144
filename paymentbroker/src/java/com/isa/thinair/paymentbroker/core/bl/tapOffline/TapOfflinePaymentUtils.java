package com.isa.thinair.paymentbroker.core.bl.tapOffline;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD.PaymentBrokerProperties.REQUEST_METHODS;

public class TapOfflinePaymentUtils {

	private static Log log = LogFactory.getLog(TapOfflinePaymentUtils.class);

	private final static String UTF_ENCODING = "UTF-8";

	private final static String CHAR_SET_UTF = "utf-8";

	private final static String CONTENT_TYPE = "application/x-www-form-urlencoded";
	
	public final static String SUCCESS = "SUCCESS";
	
	public final static String NOT_AVAILABLE = "NOT AVAILABLE";
	
	public final static String ERROR = "ERROR";
	
	public final static String STATUS_REJECTED = "STATUS_REJECTED";
	
	public static final String ORDER_DONE_BY = "OrderPlacedBy";
	
	public final static String PENDING = "PENDING";
	
	public final static String PAID = "PAID";		

	public static TapOfflineResponse getCreateInvoiceResponse(Map<String, String> postDataMap, String ipgUrl,
			boolean useProxy, String proxyAddress, String proxyPort) {

		String responseStr = getPOSTMethodResponse(postDataMap, ipgUrl, useProxy, proxyAddress, proxyPort);
		return getResponse(responseStr);
	}
	
	public static TapOfflineResponse getCheckStatusResponse(Map<String, String> postDataMap, String ipgUrl,
			boolean useProxy, String proxyAddress, String proxyPort) {

		String responseStr = getPOSTMethodResponse(postDataMap, ipgUrl, useProxy, proxyAddress, proxyPort);
		return getCheckStatusResponse(responseStr);
	}
	

	private static String getPOSTMethodResponse(Map<String, String> postDataMap, String ipgUrl, boolean useProxy,
			String proxyAddress, String proxyPort) {

		String urlParameters = getRequestParameters(postDataMap);
		byte[] postData = null;
		try {
			postData = urlParameters.getBytes(UTF_ENCODING);
		} catch (UnsupportedEncodingException e1) {
			log.debug("UTF Encodeing Not Supported: " + e1);
		}
		int postDataLength = postData.length;
		String response = "";
		URL url;
		try {
			url = new URL(ipgUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			if (useProxy) {
				System.setProperty("http.proxyHost", proxyAddress);
				System.setProperty("http.proxyPort", proxyPort);
			}
			conn.setDoOutput(true);
			conn.setInstanceFollowRedirects(false);
			conn.setRequestMethod(REQUEST_METHODS.POST.toString());
			conn.setRequestProperty("Content-Type", CONTENT_TYPE);
			conn.setRequestProperty("charset", CHAR_SET_UTF);
			conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
			conn.setUseCaches(false);
			try {
				DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
				wr.write(postData);
				Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), UTF_ENCODING));
				StringBuilder sb = new StringBuilder();
				for (int c; (c = in.read()) >= 0;)
					sb.append((char) c);
				response = sb.toString();
			} catch (Exception e) {
				log.debug("HTTP POST Method error for TapOffline PGW: " + e);
			}
		} catch (IOException e) {
			log.debug("HTTP POST Method error for TapOffline PGW: " + e);
		}
		return response;

	}

	private static String getRequestParameters(Map<String, String> postDataMap) {
		StringBuilder params = new StringBuilder("");
		for (Map.Entry<String, String> entry : postDataMap.entrySet()) {
			params.append(entry.getKey() + "=" + entry.getValue() + "&");
		}
		return params.substring(0, params.length() - 1).toString();
	}

	private static TapOfflineResponse getResponse(String responseString) {
		if (!responseString.equals("")) {
			TapOfflineResponse response = new TapOfflineResponse();
			responseString = responseString.replace("{", "").replace("}", "").replace("\"", "");
			String[] data = responseString.split(",");
			response.setTapRefID(data[0].split(":")[1]);
			response.setResCode(data[1].split(":")[1]);
			response.setResMsg(data[2].split(":")[1]);
			return response;
		}
		return null;
	}
	
	private static TapOfflineResponse getCheckStatusResponse(String responseString) {
		if (!responseString.equals("")) {			
			TapOfflineResponse response = new TapOfflineResponse();
			responseString = responseString.replace("{", "").replace("}", "").replace("\"", "");
			String[] data = responseString.split(",");
			response.setResCode(data[0].split(":").length== 2 ? data[0].split(":")[1] : null);
			response.setResMsg(data[1].split(":").length ==2 ? data[1].split(":")[1] : null);
			response.setPaymode(data[2].split(":").length ==2 ? data[2].split(":")[1]: null);
			response.setPayRefID(data[3].split(":").length ==2 ? data[3].split(":")[1]: null);
			return response;
		}
		return null;
	}
	
	public static TapOfflineResponse getCancelInvoiceResponse(Map<String, String> postDataMap, String ipgUrl,
			boolean useProxy, String proxyAddress, String proxyPort) {

		String responseStr = getPOSTMethodResponse(postDataMap, ipgUrl, useProxy, proxyAddress, proxyPort);
		return getCancelInvoiceResponse(responseStr);
	}
	
	private static TapOfflineResponse getCancelInvoiceResponse(String responseString) {
		if (!responseString.equals("")) {			
			TapOfflineResponse response = new TapOfflineResponse();
			responseString = responseString.replace("{", "").replace("}", "").replace("\"", "");
			String[] data = responseString.split(",");
			response.setTapRefID(data[0].split(":").length== 2 ? data[0].split(":")[1] : null);
			response.setResCode(data[1].split(":").length== 2 ? data[1].split(":")[1] : null);
			response.setResMsg(data[2].split(":").length ==2 ? data[2].split(":")[1] : null);
			return response;
		}
		return null;
	}
	
	public static IPGConfigsDTO getIPGConfigs(String merchantID) {
		IPGConfigsDTO oIPGConfigsDTO = new IPGConfigsDTO();
		oIPGConfigsDTO.setMerchantID(merchantID);

		return oIPGConfigsDTO;
	}

}
