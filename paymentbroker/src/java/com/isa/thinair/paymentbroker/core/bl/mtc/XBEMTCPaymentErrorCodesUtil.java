/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.bl.mtc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * The payment broker response code mapping for MIGS
 * 
 * @author Byorn De Silva
 */
public class XBEMTCPaymentErrorCodesUtil {

	private static Log log = LogFactory.getLog(XBEMTCPaymentErrorCodesUtil.class);

	public static String getResponseDescription(String vResponseCode) {

		int intSwitch = new Integer(vResponseCode).intValue();

		String result = "";
		switch (intSwitch) {

		case 50:
			result = "DECLINE - DECLINED";
		case 51:
			result = "DECLINE - EXPIRED CARD";
		case 52:
			result = "DECLINE - EXCEEDED PIN TRIES";
		case 53:
			result = "DECLINE - NO SHARING";
		case 54:
			result = "DECLINE - NO ATALLA BOX";
		case 55:
			result = "DECLINE - INVALID TRAN";
		case 56:
			result = "DECLINE - NO SUPPORT";
		case 57:
			result = "DECLINE - LOST OR STOLEN CARD";
		case 58:
			result = "DECLINE - INVALID STATUS";
		case 59:
			result = "DECLINE - DECLINED";
		case 60:
			result = "DECLINE - DECLINED";
		case 61:
			result = "DECLINE - DECLINED";
		case 62:
			result = "DECLINE - DECLINED";
		case 63:
			result = "DECLINE - DECLINED";
		case 64:
			result = "DECLINE - DECLINED";
		case 65:
			result = "DECLINE - DECLINED";
		case 66:
			result = "DECLINE - DECLINED";
		case 67:
			result = "DECLINE - DECLINED";
		case 68:
			result = "DECLINE - DECLINED";
		case 69:
			result = "DECLINE - DECLINED";
		case 70:
			result = "DECLINE - DECLINED";
		case 71:
			result = "DECLINE - DECLINED";
		case 72:
			result = "DECLINE - DECLINED";
		case 73:
			result = "DECLINE - DECLINED";
		case 74:
			result = "DECLINE - DECLINED";
		case 75:
			result = "DECLINE - DECLINED";
		case 76:
			result = "DECLINE - DECLINED";
		case 77:
			result = "DECLINE - DECLINED";
		case 78:
			result = "DECLINE - DECLINED";
		case 79:
			result = "DECLINE - DECLINED";
		case 80:
			result = "DECLINE - DECLINED";
		case 81:
			result = "DECLINE - DECLINED";
		case 82:
			result = "DECLINE - DECLINED";
		default:
			result = "DECLINE - DECLINED";

			/*
			 * case Description Base 24 Code ISO Code DECLINE - DECLINED 050 05 DECLINE - EXPIRED CARD 051 54 DECLINE -
			 * EXCEEDED PIN TRIES 052 75 DECLINE - NO SHARING 053 58 DECLINE - NO ATALLA BOX 054 96 DECLINE - INVALID
			 * TRAN 055 40 DECLINE - NO SUPPORT 056 40 DECLINE - LOST OR STOLEN CARD 057 62 DECLINE - INVALID STATUS 058
			 * 05 DECLINE - RESTRICTED 059 62 52 or 53 based on DECLINE - NO ACCOUNTS 060 processing code DECLINE - NO
			 * PBF 061 56 DECLINE - PBF UPDATE ERROR 062 05 DECLINE - INVALID AUTH CODE 063 05 DECLINE - BAD TRACK2 064
			 * 14 DECLINE - ADJ NOT ALLOWED 065 40 DECLINE - INVALID INCREMENT 066 05 DECLINE - INVALID TRAN DATE 067 77
			 * DECLINE - TLF ERROR 068 96 DECLINE - MSG EDIT ERROR 069 06 DECLINE - NO IDF RECORD 070 92 DECLINE -
			 * INVALID ROUTE AUTH 071 92 DECLINE - ON NAT'L NEG 072 05 DECLINE - INV ROUTE SERVICE 073 92 DECLINE -
			 * UNABLE TO AUTHORIZE 074 91 DECLINE - INVALID PAN LENGHT 075 14 DECLINE - LOW FUNDS 076 51 Confidential,
			 * Maroc telecommerce Copyright Page 9 DECLINE - PRE-AUTH FULL 077 DECLINE - DUPLICATE TRAN 078 DECLINE -
			 * MAX REFUND ONLINE 079 DECLINE - MAX REFUND OFFLINE 080 DECLINE - MAX PER REFUND 081 DECLINE - NUM TIMES
			 * USED 082 DECLINE - MAX REFUND TOTAL 083 DECLINE - CUST NEG REASON 084 DECLINE - INQ NOT ALLOWED 085
			 * DECLINE - OVER FLOOR LIMIT 086 DECLINE - MAX REFUND NUMBER 087 DECLINE - PLACE CALL 088 DECLINE - CAF
			 * STATUS 0 OR 9 089 DECLINE - REFERRAL FILE FULL 090 DECLINE - NEG FILE PROBLEM 091 DECLINE - ADVANCE <
			 * MINIMUM 092 DECLINE - DELINQUENT 093 DECLINE - OVER LIMIT TABLE 094 DECLINE - AMOUNT OVER MAX 095 DECLINE
			 * - PIN REQUIRED 096 DECLINE - MOD TEN CHECK 097 DECLINE - FORCED POST 098 DECLINE - BAD PBF 099 REFER -
			 * UNABLE TO PROCESS 100 REFER - ISSUE CALL 101 REFER - CALL 102 REFER - NEG FILE PROBLEM 103 REFER - CAF
			 * FILE PROBLEM 104 REFER - CARD NOT SUPPORTED 105 REFER - AMOUNT OVER MAX 106 REFER - OVER DAILY LIMIT 107
			 * REFER - CAF NOT FOUND 108 REFER - ADVANCE < MINIMUM 109 REFER - NUM TIMES USED 110 REFER - DELINQUENT 111
			 * REFER - OVER LIMIT TABLE 112 REFER - TIMEOUT 113 REFER - PTLF FULL 115 REFER - ADMIN FILE PROBLEM 121
			 * REFER - UNABLE TO VALIDATE PIN 122 SERVICE - MERCHANT NOT ON FILE 150 ERROR - ACCOUNT 200
			 * 
			 * 
			 * case 0 : result = "Transaction Successful."; break;
			 * 
			 * default : result = "Unable to be determined.";
			 */
		}

		return result;

	}

}
