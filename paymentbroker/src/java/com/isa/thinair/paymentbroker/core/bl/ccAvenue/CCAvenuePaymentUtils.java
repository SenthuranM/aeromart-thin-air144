package com.isa.thinair.paymentbroker.core.bl.ccAvenue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ccavenue.security.AesCryptUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.dto.ccAvenue.CCAvenueRequest;
import com.isa.thinair.paymentbroker.api.dto.ccAvenue.CCAvenueResponse;

public class CCAvenuePaymentUtils {
	private static final String ERROR_CODE_PREFIX = "ccavenue.";
	private static Log log = LogFactory.getLog(CCAvenuePaymentUtils.class);
	
	public static String encrypt(Map<String, String> map , CCAvenueRequest req){
		AesCryptUtil aesUtil = new AesCryptUtil(req.getEncryptionKey());
		String encRequest = aesUtil.encrypt(ComposeDataString(map));
		return encRequest;
	}
	
	public static String encrypt(String request, String key){
		AesCryptUtil aesUtil = new AesCryptUtil(key);
		String encStr = aesUtil.encrypt(request);
		return encStr;
	}
	
	public static String decrypt(String encStr, String key){
		AesCryptUtil aesUtil = new AesCryptUtil(key);
		String decResp = aesUtil.decrypt(encStr.replace("\n", "").replace("\r", "").trim());
		if(decResp == null){
			log.info("[CCAvenuePaymentUtils:: Decryption Failed.");
			return null;
		}
		log.info("[CCAvenuePaymentUtils:: decrypt : " + decResp);
		return decResp;
	}
	
	private static String ComposeDataString(Map<String, String> map){
		Set<String> keys = map.keySet();
		String dataStr = "";
		for (String key : keys) {
			dataStr = dataStr + key + "=" + map.get(key) + "&";
		}
		return dataStr;
	}
	
	public static String formatAmount(String amount){
		Double d = Double.parseDouble(amount);
		DecimalFormat df = new DecimalFormat("#.00");
		df.setMaximumFractionDigits(2);
		return df.format(d);	
	}
	
	public static String[] getMappedUnifiedError(String errorCode){
		String[] error = new String[2];
		
		if(errorCode.equals("null") || "".equals(errorCode)){
			error[0] = ERROR_CODE_PREFIX +  "0";
			error[1] = "Intended or unintended Payment Failure";
		}else if(errorCode.equals("2")){
			error[0] = ERROR_CODE_PREFIX +  errorCode;
			error[1] = "Rejected By Acquirer";
		}
		
		return error;
	}

	public static String composePaymentStatusCheckQuery(CCAvenueRequest verifyRequest) throws ParserConfigurationException, TransformerException{
	
		String responseStr =  "";
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.newDocument();
		doc.setXmlStandalone(true);		
		
		Element rootElement = doc.createElement("Order_Status_Query");
		
		doc.appendChild(rootElement);
		
		Attr orderNoAttribute = doc.createAttribute("order_no");
		orderNoAttribute.setValue(verifyRequest.getOrderId());
		
		Attr ccAveRefNoAttribute = doc.createAttribute("reference_no");
		ccAveRefNoAttribute.setValue(verifyRequest.getTrackingId());
		
		rootElement.setAttributeNode(orderNoAttribute);
		rootElement.setAttributeNode(ccAveRefNoAttribute);
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new StringWriter());
		
		transformer.transform(source, result);
		responseStr = result.getWriter().toString();
		
		log.debug("[CCAvenuePaymentUtils::composePaymentStatusCheckQuery] " + responseStr);		
		return responseStr;
	}

	public static String sendServerToServerRequest(String encXmlRequest, CCAvenueRequest verifyRequest) {
		
		log.info("[CCAvenuePaymentUtils::sendServerToServerRequest] :: Start");
				
		HttpClient client = new HttpClient();
		HttpMethod method = new PostMethod(verifyRequest.getServerToServerURL());

		method.setRequestHeader("Content-type",	"application/x-www-form-urlencoded; charset=utf-8");
		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,	new DefaultHttpMethodRetryHandler(3, false));
		
		method.setQueryString(CCAvenueRequest.S2S_REQUEST_ENC_XML + "=" + encXmlRequest 
				+ "&" + CCAvenueRequest.S2S_REQUEST_ACCESS_CODE + "=" + verifyRequest.getAccessCode()
				+ "&" + CCAvenueRequest.COMMAND + "=" + verifyRequest.getCommand() + "&request_type=XML&response_type=XML");
		
		try {
			client.executeMethod(method);
			String response = null;
			response = new String(readAll(method.getResponseBodyAsStream()));
			
			log.info("[CCAvenuePaymentUtils::sendServerToServerRequest : Response ] " + response + " Command : " + verifyRequest.getCommand());
			return response;
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			method.releaseConnection();
		}
		return null;
	}

	public static CCAvenueResponse processServerToServerResponse(String response, String key) throws SAXException, IOException, ParserConfigurationException {
		/**
		 * <Order_Status_Result>
		    
		    <order_card_name>VISA</order_card_name>
		    <order_currncy>INR</order_currncy>
		    <order_date_time>2013-09-07 13:27:47.9</order_date_time>
		    <order_device_type>PC</order_device_type>
		    <order_discount>0.0</order_discount>
		    <order_fee_flat>0.0</order_fee_flat>
		    <order_fee_perc_value>0.02</order_fee_perc_value>
		    <order_fraud_status>NR</order_fraud_status>
		    <order_gross_amt>1.0</order_gross_amt>
		    <order_gtw_id>AXIS</order_gtw_id>
		    <order_ip>192.168.2.175</order_ip>
		    <order_no>33231644</order_no>
		    <order_notes>order will be shipped</order_notes>
		    <order_option_type>OPTCRDC   </order_option_type>
		    <order_ship_name></order_ship_name>
		   
		    <order_status_date_time>2013-09-19 19:32:55.3</order_status_date_time>
		    <order_tax>0.0</order_tax>
		    
		    <order_status>Refunded</order_status>
		    <reference_no>225013271813</reference_no>
		    <status>0</status>
		  </Order_Status_Result>
		 * */
		
		CCAvenueResponse res = new CCAvenueResponse();
		String[] tokens = response.split("&");
		String[] status = null;
		
		for (String token : tokens) {
			status = token.split("=");
			if(status[0].compareToIgnoreCase(CCAvenueResponse.RESP_STATUS) == 0){
				break;
			}
		}
		
		if(status[0].compareToIgnoreCase(CCAvenueResponse.RESP_STATUS) == 0 
				&& status[1].equals(CCAvenueResponse.SUCCESS_STATUS)){
			
			String[] encTokens = null;
			
			for (String token : tokens) {
				encTokens = token.split("=");
				if(encTokens[0].compareToIgnoreCase(CCAvenueResponse.RESP_ENC_RES_XML) == 0){
					break;
				}
			}			
			
			if(encTokens[0].compareToIgnoreCase(CCAvenueResponse.RESP_ENC_RES_XML) == 0){
				
				String responseXml = decrypt(encTokens[1], key);				
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(responseXml));
				Document doc = dBuilder.parse(is);
				
				if(responseXml.indexOf("refund_status") > 0){
					
					res.setRefund(true);
					NodeList nodes = doc.getElementsByTagName("Refund_Order_Result");
					Element element = (Element) nodes.item(0);
					
					String refundStatus = element.getAttributeNode("refund_status").getTextContent();
					res.setStatusCode(refundStatus);
				    
				    if("1".equals(refundStatus) || refundStatus == null){
				    	//refund failure
				    	Attr reasonAttr = element.getAttributeNode("reason");
				    	if(reasonAttr != null){
					    	res.setReason(element.getAttributeNode("reason").getTextContent());
				    	}
				    	res.setError(true);
				    	
				    }else if("0".equals(refundStatus)){
				    	res.setError(false);
				    }
				    return res;
				}else if(responseXml.indexOf("Order_Lookup_Result") > 0){
					
					NodeList nodes = doc.getElementsByTagName("order");
					log.info("[CCAvenuePaymentUtils::processServerToServerResponse] Order_Lookup_Result size : " + nodes.getLength());

					if(nodes.getLength() == 0){
						log.warn("No orders found for the given order id");
						return null;
					}
					
					Element element = (Element) nodes.item(0);
					
					String trackingId = element.getAttributeNode("reference_no").getTextContent();
					res.setTracking_id(trackingId);
					
					String orderId = element.getAttributeNode("order_no").getTextContent();
					res.setOrderId(orderId);					
					return res;
				}
				
				NodeList nodes = doc.getElementsByTagName("status");
				Element element = (Element) nodes.item(0);
				String statusCodeString = element.getTextContent();

			    res.setStatusCode(statusCodeString);
			    
			    nodes = doc.getElementsByTagName("error_desc");
			    element = (Element) nodes.item(0);
			    if(element != null){
			    	res.setStatusMessage(element.getTextContent());
			    	res.setReason(element.getTextContent());
			    }
			    
			    nodes = doc.getElementsByTagName("order_status");
			    element = (Element) nodes.item(0);
			    if(element != null){
			    	res.setOrderStatus(element.getTextContent());
			    }
			    
			    nodes = doc.getElementsByTagName("order_status");
			    element = (Element) nodes.item(0);
			    if(element != null){
			    	res.setOrderStatus(element.getTextContent());
			    }
			    
			    nodes = doc.getElementsByTagName("order_no");
			    element = (Element) nodes.item(0);
			    if(element != null){
			    	res.setOrderId(element.getTextContent());
			    }
			}
			
			return res;
		}
		
		res.setStatusCode("1");
		res.setError(true);
		return res;
	}

	public static String composeRefundQuery(CCAvenueRequest refReq) throws TransformerException, ParserConfigurationException {
		String responseStr =  "";
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.newDocument();
		doc.setXmlStandalone(true);		
			
		Element rootElement = doc.createElement("Refund_Order_Query");	
		
		Attr trackingIdAttribute = doc.createAttribute("reference_no");
		trackingIdAttribute.setValue(refReq.getTrackingId());
		
		Attr amountAttribute = doc.createAttribute("refund_amount");
		amountAttribute.setValue(refReq.getAmount());
		
		refReq.setRefundId(getUniqueRefundID());
		Attr refundIDAttribute = doc.createAttribute("refund_ref_no");
		refundIDAttribute.setValue(refReq.getRefundId());
		
		rootElement.setAttributeNode(trackingIdAttribute);
		rootElement.setAttributeNode(amountAttribute);
		rootElement.setAttributeNode(refundIDAttribute);
		
		doc.appendChild(rootElement);
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new StringWriter());
		
		transformer.transform(source, result);
		responseStr = result.getWriter().toString();
		
		log.debug("[CCAvenuePaymentUtils::composeRefundQuery] " + responseStr);		
		return responseStr;
	}
	
	
	public static String getUniqueRefundID(){
		
		UUID myuuid = UUID.randomUUID();
		long highbits = myuuid.getMostSignificantBits();
		long lowbits = myuuid.getLeastSignificantBits();
		
		String id = String.valueOf(highbits) + String.valueOf(lowbits);
		id = id.replaceAll("-", "");
	    SecureRandom random = new SecureRandom();
	    int beginIndex = random.nextInt(15);       //Begin index + length of your string < data length
	    int endIndex = beginIndex + 6;            //Length of string which you want

	    String uniqueID = id.substring(beginIndex, endIndex);
		  
		return uniqueID;
	}
	
	
	private static byte[] readAll(InputStream is) throws IOException {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		int len;

		while (true) {
			len = is.read(buf);
			if (len < 0) {
				break;
			}
			baos.write(buf, 0, len);
		}

		return baos.toByteArray();
	}

	public static CCAvenueResponse processDynamicEventNotification(String responseData) throws ModuleException {
		
		CCAvenueResponse res = new CCAvenueResponse();
		
		StringTokenizer tokenizer = new StringTokenizer(responseData, "&");
		Hashtable<String, String> hs = new Hashtable<String, String>();
		String pair = null, pname = null, pvalue = null;
	
		while (tokenizer.hasMoreTokens()) {
			pair = (String) tokenizer.nextToken();
			if (pair != null) {
				StringTokenizer strTok = new StringTokenizer(pair, "=");
				pname = "";
				pvalue = "";
				if (strTok.hasMoreTokens()) {
					pname = (String) strTok.nextToken();
					if (strTok.hasMoreTokens())
						pvalue = (String) strTok.nextToken();
					hs.put(pname, pvalue);
				}
			}
		}
		
		res.setOrderId(hs.get(CCAvenueRequest.ORDER_ID));
		res.setTracking_id(hs.get(CCAvenueRequest.TRACKING_ID));
		res.setBankRefNo(hs.get(CCAvenueRequest.BANK_REF_NO));
		res.setOrderStatus(hs.get(CCAvenueRequest.ORDER_STATUS));
		res.setFailureMessage(hs.get(CCAvenueRequest.FAILURE_MESSAGE));
		res.setPaymentMode(hs.get(CCAvenueRequest.PAYMENT_MODE));
		res.setCardName(hs.get(CCAvenueRequest.CARD_NAME));
		res.setStatusCode(hs.get(CCAvenueRequest.STATUS_CODE));
		res.setStatusMessage(hs.get(CCAvenueRequest.STATUS_MESSAGE));
		res.setCurrency(hs.get(CCAvenueRequest.CURRENCY));
		res.setAmount(hs.get(CCAvenueRequest.AMOUNT));
		res.setBillingName(hs.get(CCAvenueRequest.BILLING_NAME));
		res.setBillingAddress(hs.get(CCAvenueRequest.BILLING_ADDRESS));
		res.setBillingTel(hs.get(CCAvenueRequest.BILLING_TEL));
		res.setMerchantParam1(hs.get(CCAvenueRequest.MERCHANT_PARAM1));
		res.setMerchantParam2(hs.get(CCAvenueRequest.MERCHANT_PARAM2));
				
		return res;
	}

	public static String composeOrderLookupQuery(CCAvenueRequest verifyRequest) throws ParserConfigurationException, TransformerException, ParseException {
	/*	
		<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
		<Order_Lookup_Query>
			<order_no>xxxxxxx</order_no>
			
			<from_date>xx-xx-xxxx</from_date>
			
		</Order_Lookup_Query> */
				
		String responseStr = "";
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.newDocument();
		doc.setXmlStandalone(true);		
		
		Element rootElement = doc.createElement("Order_Lookup_Query");
		
		doc.appendChild(rootElement);
		
		Element orderNoAttribute = doc.createElement("order_no");
		orderNoAttribute.setTextContent(verifyRequest.getOrderId());
		
		String transactionDate = verifyRequest.getTransactionTimestamp();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		Date date = format.parse(transactionDate);
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(date.getTime());
		format = new SimpleDateFormat("dd-MM-yyyy");
		
		Element fromDateAttribute = doc.createElement("from_date");
		fromDateAttribute.setTextContent(format.format(cal.getTime()));
		
		rootElement.appendChild(orderNoAttribute);
		rootElement.appendChild(fromDateAttribute);
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new StringWriter());
		
		transformer.transform(source, result);
		responseStr = result.getWriter().toString();
		
		log.debug("[CCAvenuePaymentUtils::composeOrderLookupQuery] " + responseStr);		
		
		return responseStr;
	}
}