/**
 * 
 */
package com.isa.thinair.paymentbroker.core.bl.mtcf;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.ResourceBundle;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.sun.net.ssl.SSLContext;
import com.sun.net.ssl.X509TrustManager;

/**
 * @author Manjula
 * 
 */
public class PaymentBrokerMTCCapture extends PaymentBrokerTemplate {

	private static Log log = LogFactory.getLog(PaymentBrokerMTCCapture.class);

	private static final String IPG_URL_PART_CAPTURE = "vpcdps";
	private static final String TRANSACTION_RESULT_SUCCESS = "0";
	private static final String PAY_APPROVED_MESSAGE = "Approved";

	public static SSLSocketFactory s_sslSocketFactory = null;

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	static {
		X509TrustManager s_x509TrustManager = new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[] {};
			}

			public boolean isClientTrusted(X509Certificate[] chain) {
				return true;
			}

			public boolean isServerTrusted(X509Certificate[] chain) {
				return true;
			}
		};

		java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

		try {
			SSLContext context = SSLContext.getInstance("TLS");
			context.init(null, new X509TrustManager[] { s_x509TrustManager }, null);
			s_sslSocketFactory = context.getSocketFactory();
		} catch (Exception e) {
			log.error("FATAL ERROR ", e);
			throw new RuntimeException(e.getMessage());
		}
	}

	/**
	 * Performs card refund operation
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @param ipgURL
	 * @param merchantID
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce capture(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, Map fields, String ipgURL, String merchantID) throws ModuleException {
		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status;
		String errorSpecification;
		String ipgURLCapture = ipgURL + IPG_URL_PART_CAPTURE;

		log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
		log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
		log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());

		// ----------------------------------------
		String refundResponse = null;
		Integer paymentBrokerRefNo;

		// Sends the refund request and gets the response
		try {
			Object[] responses = doPost(pnr, creditCardPayment, ipgURLCapture, fields, merchantID);

			refundResponse = (String) responses[0];
			paymentBrokerRefNo = (Integer) responses[1];

		} catch (UnsupportedEncodingException uee) {
			log.error(uee);
			throw new ModuleException("paymentbroker.error.unsupportedencoding");
		} catch (IOException ioe) {
			log.error(ioe);
			throw new ModuleException("paymentbroker.error.ioException");
		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		log.debug("Capture Response : " + refundResponse);

		// create a hash map for the response data
		Map responseFields = MTCPaymentUtils.createMapFromResponse(refundResponse);
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		MTCFatouratiResponse migsResp = new MTCFatouratiResponse();
		migsResp.setResponse(responseFields);

		// if (TRANSACTION_RESULT_SUCCESS.equals(migsResp.getTxnResponseCode())
		// && PAY_APPROVED_MESSAGE.equals(migsResp.getMessage())) {
		errorCode = "";
		// transactionMsg = migsResp.getMessage();
		status = IPGResponseDTO.STATUS_ACCEPTED;
		tnxResultCode = 1;
		errorSpecification = "";
		sr.setSuccess(true);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, PaymentConstants.SERVICE_RESPONSE_CAPTURE);
		// } else {
		// /*errorCode = getMatchingTxnErrorCode(migsResp.getTxnResponseCode());*/
		// errorCode = PaymentResponseCodesUtil.getFilteredErrorCode(migsResp);
		// transactionMsg = PaymentResponseCodesUtil.getResponseDescription(migsResp.getTxnResponseCode());
		// status = IPGResponseDTO.STATUS_REJECTED;
		// tnxResultCode = 0;
		// errorSpecification = status + " " + transactionMsg
		// + " " + migsResp.getMessage();
		// sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
		// }

		// updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), migsResp.toString(),
		// errorSpecification, migsResp.getAuthorizeID(), migsResp.getTransactionNo(), tnxResultCode);

		sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
		// sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, migsResp.getAuthorizeID());
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
		return sr;

	}

	/**
	 * This method is for performing a Form POST operation from input data parameters.
	 * 
	 * @param vpc_Host
	 *            - is a String containing the vpc URL
	 * @param merchantID
	 * @param data
	 *            - is a String containing the post data key value pairs
	 * @param useProxy
	 *            - is a boolean indicating if a Proxy Server is involved in the transfer
	 * @param proxyHost
	 *            - is a String containing the IP address of the Proxy to send the data to
	 * @param proxyPort
	 *            - is an integer containing the port number of the Proxy socket listener
	 * 
	 * @return - is body data of the POST data response
	 * @throws ModuleException
	 */
	public Object[] doPost(String pnr, CreditCardPayment creditCardPayment, String vpc_Host, Map dataFields, String merchantID)
			throws IOException, ModuleException {

		InputStream is;
		OutputStream os;
		int vpc_Port = 443;
		String fileName = "";
		boolean useSSL = false;
		StringBuffer postData = new StringBuffer();
		MTFatouratiWRequest migsReq = new MTFatouratiWRequest();
		// migsReq.appendQueryFields(postData, dataFields);

		// determine if SSL encryption is being used
		if (vpc_Host.substring(0, 8).equalsIgnoreCase("HTTPS://")) {
			useSSL = true;
			// remove 'HTTPS://' from host URL
			vpc_Host = vpc_Host.substring(8);
			// get the filename from the last section of vpc_URL
			fileName = vpc_Host.substring(vpc_Host.lastIndexOf("/"));
			// get the IP address of the VPC machine
			vpc_Host = vpc_Host.substring(0, vpc_Host.lastIndexOf("/"));
		}

		// use the next block of code if using a proxy server
		if (useProxy) {
			Socket s = new Socket(proxyHost, Integer.parseInt(proxyPort));
			os = s.getOutputStream();
			is = s.getInputStream();
			// use next block of code if using SSL encryption
			if (useSSL) {
				String msg = "CONNECT " + vpc_Host + ":" + vpc_Port + " HTTP/1.0\r\n" + "User-Agent: HTTP Client\r\n\r\n";
				os.write(msg.getBytes());
				byte[] buf = new byte[4096];
				int len = is.read(buf);
				String res = new String(buf, 0, len);

				// check if a successful HTTP connection
				if (res.indexOf("200") < 0) {
					throw new IOException("Proxy would now allow connection - " + res);
				}

				// write output to VPC
				SSLSocket ssl = (SSLSocket) s_sslSocketFactory.createSocket(s, vpc_Host, vpc_Port, true);
				ssl.startHandshake();
				os = ssl.getOutputStream();
				// get response data from VPC
				is = ssl.getInputStream();
				// use the next block of code if NOT using SSL encryption
			} else {
				fileName = vpc_Host;
			}
			// use the next block of code if NOT using a proxy server
		} else {
			// use next block of code if using SSL encryption
			if (useSSL) {
				Socket s = s_sslSocketFactory.createSocket(vpc_Host, vpc_Port);
				os = s.getOutputStream();
				is = s.getInputStream();
				// use next block of code if NOT using SSL encryption
			} else {
				Socket s = new Socket(vpc_Host, vpc_Port);
				os = s.getOutputStream();
				is = s.getInputStream();
			}
		}

		String req = "POST " + fileName + " HTTP/1.0\r\n" + "User-Agent: HTTP Client\r\n"
				+ "Content-Type: application/x-www-form-urlencoded\r\n" + "Content-Length: " + postData.toString().length()
				+ "\r\n\r\n" + postData.toString();

		CreditCardTransaction ccTransaction = auditTransaction(pnr, merchantID, (String) dataFields.get(""), new Integer(
				creditCardPayment.getTemporyPaymentId()), bundle.getString("SERVICETYPE_CAPTURE"), req, "", creditCardPayment
				.getIpgIdentificationParamsDTO().getFQIPGConfigurationName(), null, false);

		log.debug("******** IPG CAPTURE REQUEST URL : " + req);

		os.write(req.getBytes());
		String res = new String(MTCPaymentUtils.readAll(is));

		log.debug("Immediate refund response file : " + res);

		// check if a successful connection
		if (res.indexOf("200") < 0) {
			throw new IOException("Connection Refused - " + res);
		}

		if (res.indexOf("404 Not Found") > 0) {
			throw new IOException("File Not Found Error - " + res);
		}

		int resIndex = res.indexOf("\r\n\r\n");
		String body = res.substring(resIndex + 4, res.length());

		log.debug("Response file body : " + body);
		return new Object[] { body, new Integer(ccTransaction.getTransactionRefNo()) };
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		// FIXME - Should ensure IPGConfigurationParamDTO is set at instantiation time of PaymentBrokerMIGS3Capture
		// class
		PaymentBrokerManager.validateIPGIdentificationParams(this.ipgIdentificationParamsDTO);
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}
}
