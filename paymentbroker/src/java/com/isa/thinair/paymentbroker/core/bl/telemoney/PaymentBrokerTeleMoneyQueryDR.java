/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.paymentbroker.core.bl.telemoney;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ResourceBundle;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Dhanushka Ranatunga
 */
public class PaymentBrokerTeleMoneyQueryDR extends PaymentBrokerTemplate implements PaymentQueryDR {

	private static Log log = LogFactory.getLog(PaymentBrokerTeleMoneyQueryDR.class);

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	/**
	 * This method is for performing a Form POST operation from input data parameters.
	 * 
	 * @param pnr
	 * @param temporyPaymentId
	 * @param vpc_Host
	 * @param dataFields
	 * @param isCharge
	 * @return
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 */
	private Object[] doPost(String pnr, int temporyPaymentId, TeleMoneyIPGRequest dataFields, boolean isCharge)
			throws ModuleException, IOException, UnsupportedEncodingException {
		String response = null;

		HttpClient client = new HttpClient();
		HttpMethod method = new PostMethod(dataFields.getIpgUrl());
		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(3, false));
		method.getParams().setParameter(TeleMoneyIPGRequest.MID, dataFields.getMerchantId());
		method.getParams().setParameter(TeleMoneyIPGRequest.REF, dataFields.getMerchantTxnId());
		method.getParams().setParameter(TeleMoneyIPGRequest.STATUSURL, dataFields.getStatusUrl());

		String serviceType = bundle.getString("SERVICETYPE_QUERY");

		CreditCardTransaction ccTransaction = auditTransaction(pnr, getMerchantId(), dataFields.getMerchantTxnId(), new Integer(
				temporyPaymentId), serviceType, method.getQueryString(), "", getPaymentGatewayName(), null, false);

		try {
			int statusCode = client.executeMethod(method);

			if (statusCode != HttpStatus.SC_OK) {
				log.error("PaymentBrokerTeleMoneyQueryDR.doPost()- Method failed: " + method.getStatusLine());
			}

			// Read the response body.
			// FIXME - need to check if the relevent data fields are sent
			response = method.getResponseBodyAsString();

		} catch (HttpException e) {
			log.error(e);
			throw new ModuleException("paymentbroker.error.httpconnectionfailed");
		} catch (IOException ioe) {
			log.error(ioe);
			throw new ModuleException("paymentbroker.error.ioException");
		} finally {
			method.releaseConnection();
		}

		return new Object[] { response, new Integer(ccTransaction.getTransactionRefNo()) };

	}

	/**
	 * Performs the query operation for a executed tpt_id, pnr
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce query(CreditCardTransaction oCreditCardTransaction, IPGConfigsDTO ipgConfigsDTO,
			PaymentBrokerInternalConstants.QUERY_CALLER caller) throws ModuleException {

		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status = null;
		String errorSpecification = null;

		TeleMoneyIPGRequest requestData = new TeleMoneyIPGRequest();
		requestData.setMerchantId(ipgConfigsDTO.getMerchantID());
		requestData.setMerchantTxnId(oCreditCardTransaction.getTempReferenceNum());
		requestData.setStatusUrl(ipgConfigsDTO.getStatusUrl());
		requestData.setIpgUrl(ipgURL);

		Object[] resQS = null;
		String queryResponse = null;
		Integer paymentBrokerRefNo;

		try {
			// create a URL connection to the Virtual Payment Client
			resQS = doPost(oCreditCardTransaction.getTransactionReference(), oCreditCardTransaction.getTemporyPaymentId(),
					requestData, false);
			queryResponse = (String) resQS[0];
			paymentBrokerRefNo = (Integer) resQS[1];

		} catch (UnsupportedEncodingException uee) {
			log.error(uee);
			throw new ModuleException("paymentbroker.error.unsupportedencoding");
		} catch (IOException ioe) {
			log.error(ioe);
			throw new ModuleException("paymentbroker.error.ioException");
		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		log.debug("Response : " + queryResponse);

		// create a hash map for the response data
		TeleMoneyIPGResponse tmRes = new TeleMoneyIPGResponse(TeleMoneyIPGResponse.getResponseParamMap(queryResponse));
		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		if (tmRes.getStatus().equals(TeleMoneyIPGResponse.STATUS_YES)) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			errorCode = "";
			transactionMsg = "";
			tnxResultCode = 1;
			errorSpecification = "";
			sr.setSuccess(true);
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, PaymentConstants.SERVICE_RESPONSE_QUERYDR);
			sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE, tmRes.getMerchantTxnId());
		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			// FIXME - need to check if the relevent data fields are sent
			errorCode = tmRes.getErrorMsg();
			// transactionMsg = PaymentResponseCodesUtil.getResponseDescription(migsResp.getTxnResponseCode());
			transactionMsg = tmRes.getErrorMsg();
			tnxResultCode = 0;
			errorSpecification = status + " " + transactionMsg;
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
		}

		// FIXME - need to check if the relevent data fields are sent
		updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), tmRes.toString(), errorSpecification,
				tmRes.getApprovalCode(), tmRes.getMerchantTxnId(), tnxResultCode);

		sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, tmRes.getApprovalCode());
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);

		log.debug("FINISH TRANSACTION - Process the VPC Response Data ");

		return sr;
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		// FIXME - Should ensure IPGConfigurationParamDTO is set at instantiation time of PaymentBrokerTeleMoneyQueryDR
		// class
		PaymentBrokerManager.validateIPGIdentificationParams(this.ipgIdentificationParamsDTO);
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

}
