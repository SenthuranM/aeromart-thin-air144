package com.isa.thinair.paymentbroker.core.bl.knet;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * 
 * @author Pradeep Karunanayake
 *
 */
public class KnetRequest {
	
	public static final String PAYMENTID =  "PaymentID";
	
	public static final String CURRENCY_CODE_KWD = "414";
	
	public static final String CURRENCY = "KWD";
	
	public static final int NUMBER_OF_DECIMAL_KWD = 3;
	
	public static final String ARABIC = "ar";
	
	public static final String RESPONSE_URL = AppSysParamsUtil.getSecureIBEUrl() + "knetNotifyPaymentResponseHandler.action";
	
	public static final String SERVICE_APP_RESPONSE_URL = AppSysParamsUtil.getSecureServiceAppIBEUrl() + "/controller/postPayment/knet/redirect";
		
	public enum ACTION{
		PURCHASE(1);
		private int action;
		private ACTION(int payAction) {
			action = payAction;
		}		
		public int getAction() {
			return action;
		}		
	};
	
	public enum LANGUAGE{
		ENGLISH("ENG"), ARABIC("ARA");
		private String language;
		private LANGUAGE(String lan) {
			language = lan;
		}
		public String getLanguage() {
			return language;
		}
	};

}
