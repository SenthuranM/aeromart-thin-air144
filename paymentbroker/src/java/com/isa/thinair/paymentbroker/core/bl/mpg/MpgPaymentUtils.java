package com.isa.thinair.paymentbroker.core.bl.mpg;

import java.security.MessageDigest;
import java.util.Map;
import java.util.Properties;

import org.apache.axis.encoding.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MpgPaymentUtils {
	private static Log log = LogFactory.getLog(MpgPaymentUtils.class);
	private static final String ERROR_CODE_PREFIX = "mpg.";
	
	public static String computeSHA1(Map<String, String> postDataMap){		
//		(Password + MerchantID + AcquirerID + OrderID + Purchase Amount + Purchase Currency)
		
		String candidate = postDataMap.get(MpgRequest.PASSWORD) 
						 + postDataMap.get(MpgRequest.MERCHANT_ID)
						 + postDataMap.get(MpgRequest.ACQUIRER_ID)
						 + postDataMap.get(MpgRequest.ORDER_ID)
						 + postDataMap.get(MpgRequest.PURCHASE_AMOUNT)
						 + postDataMap.get(MpgRequest.PURCHASE_CURRENCY);
		
		try {
			
			MessageDigest md;
			md = MessageDigest.getInstance("SHA-1");
			byte[] sha1hash = new byte[40];
			md.update(candidate.getBytes("UTF-8"), 0, candidate.length());
			sha1hash = md.digest();
			return Base64.encode(sha1hash);
			
		} catch (Exception e) {
			log.debug("[computeSHA1] Error computing SHA-1 : " + e.toString());
		} 
		
		return null;
	}


	public static String formatAmount(String finalAmount) {
		StringBuilder sb = new StringBuilder(finalAmount);
		int decimalPointindex = 0;
		if(sb.indexOf(".") > 0){
			decimalPointindex = finalAmount.indexOf(".");
			sb = sb.replace(decimalPointindex, decimalPointindex + 1, "");
		}
		
		//12 is the specified length by MPG
		while(sb.length() < 12){
			sb = sb.insert(0, "0");
		}
		
		return sb.toString();
	}
	
	public static boolean validateHASH(MpgResponse resp, Properties props){
		//(Password + MerchantID + AcquirerId + OrderID + Response code + Reason code)
		
		String candidate = props.getProperty(MpgRequest.PASSWORD)
						 + props.getProperty(MpgRequest.MERCHANT_ID)
						 + props.getProperty(MpgRequest.ACQUIRER_ID)
						 + resp.getMerchantOrderId()						 
						 + resp.getResponseCode()
						 + resp.getReasonCode();
		
		String hash = "";
		try {
			
			MessageDigest md;
			md = MessageDigest.getInstance("SHA-1");
			md.update(candidate.getBytes("UTF-8"));
			hash = Base64.encode(md.digest());
			
		} catch (Exception e) {
			log.debug("[validateHASH] Error computing SHA-1 : " + e.toString());
		}
		
		return hash.equals(resp.getSignature()); 
	}
	
	public static String getFilteredErrorCode(String reasonCode) {

		String errorCode = "";	
		errorCode = ERROR_CODE_PREFIX + "100";
			
		if (log.isDebugEnabled()) {
			log.debug("[MpgPaymentUtils::getFilteredErrorCode] Return Code :" + errorCode);
		}
		
		return errorCode;
	}
	
}
