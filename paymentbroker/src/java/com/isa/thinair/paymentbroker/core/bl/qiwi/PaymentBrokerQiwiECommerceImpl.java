package com.isa.thinair.paymentbroker.core.bl.qiwi;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.TimeZone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPRequest;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPResponse;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class PaymentBrokerQiwiECommerceImpl extends PaymentBrokerQiwiTemplate {

	// mapCardType defined in configs xml
	protected static final String CARD_TYPE_GENERIC = "GC";
	protected static final String CARD_TYPE_OFFLINE = "OF";
	private static Log log = LogFactory.getLog(PaymentBrokerQiwiECommerceImpl.class);
	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();
	

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO)
			throws ModuleException {
		log.debug("[PaymentBrokerQiwiECommerceImpl::getRequestData()] Begin ");
		
		// change to get a numeric tnxId
		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();
		Long timeToSpare = null;
		QiwiPRequest qiwiRequest = new QiwiPRequest();
		qiwiRequest.setQiwiInvoiceTimeout(getInvoiceTimeout());
		qiwiRequest.setQiwiCreationInvoiceURL(getCreateInvoiceURL());
		qiwiRequest.setQiwiMerchantID(getMerchantId());
		qiwiRequest.setQiwiRedirectURL(getRedirectURL());
		qiwiRequest.setMerchantID(getMerchantId());
		qiwiRequest.setCreateInvoiceRequestMethod(getInvoiceCreationMethod());
		qiwiRequest.setCheckInvoiceRequestMethod(getInvoiceStatusCheckMethod());
		qiwiRequest.setTransactionID(String.valueOf(ipgRequestDTO.getApplicationTransactionId()));
		qiwiRequest.setQiwiMobileNumber(ipgRequestDTO.getContactMobileNumber());
		qiwiRequest.setAmount(ipgRequestDTO.getAmount());
		qiwiRequest.setPnr(ipgRequestDTO.getPnr());
		qiwiRequest.setMerchantName(ipgRequestDTO.getDefaultCarrierName());
		qiwiRequest.setLogin(getLogin());
		qiwiRequest.setPassword(getPassword());
				
		if(this.getMapCardType().get(CARD_TYPE_GENERIC).equals(ipgRequestDTO.getCardType())){
			qiwiRequest.setOfflineMode(false);
		}else if(this.getMapCardType().get(CARD_TYPE_OFFLINE).equals(ipgRequestDTO.getCardType())){
			qiwiRequest.setOfflineMode(true);
		}
		
		Properties props = PaymentBrokerManager.getProperties(this.ipgIdentificationParamsDTO);
		qiwiRequest.setQiwiInvoiceStatusReqURL(props.getProperty(QiwiPRequest.INVOICE_STATUS_REQ_URL));
		
		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		
		postDataMap.put(QiwiPRequest.FORM_ACTION, QiwiPaymentUtils.composeQiwiInvoiceCreationURL(qiwiRequest));
		postDataMap.put(QiwiPRequest.USER, QiwiPaymentUtils.getValidatedQiwiMobileNo(qiwiRequest.getQiwiMobileNumber()) );  // mobile number
		postDataMap.put(QiwiPRequest.AMOUNT, QiwiPaymentUtils.getFormattedAmount(ipgRequestDTO.getAmount()));
		postDataMap.put(QiwiPRequest.CURRENCY, ipgIdentificationParamsDTO.getPaymentCurrencyCode());
		postDataMap.put(QiwiPRequest.COMMENT, QiwiPaymentUtils.composeComment(qiwiRequest));
	
		// payment retry
		if(ipgRequestDTO.getInvoiceExpirationTime() == null){
			if(ipgRequestDTO.getTimeToSpare() > 0){
				Date dt = new Date(new Date().getTime() + ipgRequestDTO.getTimeToSpare());
				ipgRequestDTO.setInvoiceExpirationTime(dt);
			}else{
				Date dt = new Date(new Date().getTime() + AppSysParamsUtil.getOnholdOfflinePaymentTimeInMillis());
				ipgRequestDTO.setInvoiceExpirationTime(dt);
			}	
		}
		
		timeToSpare = ipgRequestDTO.getInvoiceExpirationTime().getTime() - new Date().getTime();
		
		// Date and time in ISO 8601 format 
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("Europe/Moscow"));
		Calendar cal = Calendar.getInstance(); 
	    cal.setTime(ipgRequestDTO.getInvoiceExpirationTime()); 
	    
		postDataMap.put(QiwiPRequest.LIFETIME, df.format(cal.getTime()));
		postDataMap.put(QiwiPRequest.MERCHANT_NAME, ipgRequestDTO.getDefaultCarrierName()); 		
		postDataMap.put(QiwiPRequest.MERCHANT_ID, getMerchantId());
		postDataMap.put(QiwiPRequest.TNX_ID, qiwiRequest.getTransactionID());
		postDataMap.put(QiwiPRequest.REDIRECT_URL, QiwiPaymentUtils.composeQiwiRedirectURL(ipgRequestDTO, qiwiRequest));
		postDataMap.put("SESSION ID", sessionID);       
		
		// creating html form to redirect the user to qiwi, inorder to make the payment
		String requestDataForm = QiwiPaymentUtils.getQiwiRedirectHTML(postDataMap);
		
		postDataMap.put(QiwiPRequest.AUTHORIZATION, QiwiPaymentUtils.getAuthorizationToken(getLogin(), getPassword()));

		// create invoice in QIWI system
		QiwiPResponse response = QiwiPaymentUtils.createQiwiInvoice(postDataMap);
		response.setOfflineMode(qiwiRequest.isOfflineMode());
		CreditCardTransaction ccTransaction = null;
		response.setTimeToSpare(timeToSpare);
		
		if(response.getStatus().equals(QiwiPRequest.WAITING)){
			
			ccTransaction = auditTransaction(PlatformUtiltiies.nullHandler(qiwiRequest.getPnr()), getMerchantId(), ipgRequestDTO.getApplicationIndicator() + response.getBillId(),
					ipgRequestDTO.getApplicationTransactionId(), bundle.getString("SERVICETYPE_AUTHORIZE"),
					PaymentBrokerUtils.getRequestDataAsString(postDataMap), response.toString(), getPaymentGatewayName(), null, false);
			
		}else{
			
			IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
			ipgRequestResultsDTO.setQiwiResponse(response);
			ipgRequestResultsDTO.setErrorCode(QiwiPaymentUtils.getErrorCode(response));
			return ipgRequestResultsDTO;
		}

		log.error("[PaymentBrokerQiwiECommerceImpl::getRequestData()]" + QiwiPaymentUtils.getErrorInfo(response));
		
		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setPaymentBrokerRefNo((ccTransaction == null) ? null : ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setAccelAeroTransactionRef(response.getBillId());
		ipgRequestResultsDTO.setPostDataMap(postDataMap);
		ipgRequestResultsDTO.setRequestData(requestDataForm);
		ipgRequestResultsDTO.setQiwiResponse(response);
		
		log.debug("[PaymentBrokerQiwiECommerceImpl::getRequestData()] End ");
		return ipgRequestResultsDTO;
	}
	

	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap,
			IPGResponseDTO ipgResponseDTO) throws ModuleException {
		log.debug("[PaymentBrokerQiwiECommerceImpl::getReponseData()] Begin ");

		int tnxResultCode;
		String errorCode = "";
		String status;
		String errorSpecification = "";
		String merchantTxnReference;

		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());
		
		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		if(oCreditCardTransaction != null){
			// Update response time
			oCreditCardTransaction.setComments(strResponseTime);
			oCreditCardTransaction.setResponseTime(timeDiffInMillis);
			PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);
		}
		
		QiwiPResponse qiwiResp = new QiwiPResponse();
		qiwiResp.setResponse(receiptyMap);
		
		if (log.isDebugEnabled()) {
			log.debug("[PaymentBrokerQiwiECommerceImpl::getReponseData()] Response -" + qiwiResp.toString());
		}

		if (qiwiResp.getBillId() != "" && qiwiResp.getBillId() != null) {

			TempPaymentTnx tmpPayment = ReservationModuleUtils.getReservationBD().loadTempPayment(oCreditCardTransaction.getTemporyPaymentId());
			BigDecimal paymentAmount = tmpPayment.getPaymentCurrencyAmount();

			String paymentAmountStr = paymentAmount.toString();

			PaymentQueryDR oNewQuery = getPaymentQueryDR();
			oCreditCardTransaction.setTransactionId(qiwiResp.getBillId());
			
			IPGConfigsDTO configs = QiwiPaymentUtils.getIPGConfigs(getMerchantId(), getPassword());
			configs.setResponseWaitingTimeInSec(getQiwiResponseWaitingTimeInSec());
			
			// after the notification has arrived from qiwi, check invoice status 
			ServiceResponce serviceResponce = oNewQuery.query(oCreditCardTransaction,configs, QUERY_CALLER.VERIFY);
			if (serviceResponce.isSuccess()) {
				Double responseAmount = Double.parseDouble(((String) serviceResponce.getResponseParam(PaymentConstants.AMOUNT)).toString());
				String responseAmountStr = responseAmount.toString();

				if (log.isDebugEnabled()) {
					log.debug("[PaymentBrokerQiwiECommerceImpl::getReponseData()] - Payment Amount-" + paymentAmount
							+ " Verified Amount-" + responseAmount);
					log.debug("[PaymentBrokerQiwiECommerceImpl::getReponseData()] - Formatted Payment Amount-"
							+ paymentAmountStr + " Formatted Verified Amount-" + responseAmountStr);
				}

				if (paymentAmountStr.equals(responseAmountStr)) {
					status = IPGResponseDTO.STATUS_ACCEPTED;
					tnxResultCode = 1;
				} else {
					status = IPGResponseDTO.STATUS_REJECTED;
					tnxResultCode = 0;
					errorCode = VERIFICATION_AMOUNTS_ERROR;
					errorSpecification = "Payment and verifcation amounts different!!!";
					// Different amount in response
					log.error("[PaymentBrokerQiwiECommerceImpl::getReponseData()] - Payment and verification amounts are different");
				}
				
			} else {
				status = IPGResponseDTO.STATUS_REJECTED;
				tnxResultCode = 0;
				errorCode = QiwiPRequest.QIWI_WAITING_FOR_PAYMENT_CONF;
				if(!ipgResponseDTO.isOnholdCreated()){
					errorCode = QiwiPRequest.QIWI_PAYMENT_CONF_FAILED;
				}
				errorSpecification = "" + serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE);
			}
			
			log.debug("[PaymentBrokerQiwiECommerceImpl::getReponseData()] - Status : " + status + "service response code : " + serviceResponce.getResponseCode());

		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
			String error = QiwiPaymentUtils.getErrorInfo(qiwiResp);
			errorCode = ((qiwiResp.getError() == null) || (qiwiResp.getError() == "")) ? QiwiPaymentUtils.getErrorCode(qiwiResp) : qiwiResp.getError();
			log.error("[PaymentBrokerQiwiECommerceImpl::getReponseData()] - Invoice Not Paid " + error);
		}

		merchantTxnReference = qiwiResp.getBillId();

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setApplicationTransactionId(merchantTxnReference);
		ipgResponseDTO.setAuthorizationCode(merchantTxnReference);
		// Qiwi Payment can be a mobile payment or a card transaction
		ipgResponseDTO.setCardType(getStandardCardType(CARD_TYPE_GENERIC));
		String qiwiResponse = qiwiResp.toString();
		if(ipgResponseDTO.getPaymentBrokerRefNo() > 0){
			updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), qiwiResponse, errorSpecification,
					merchantTxnReference, merchantTxnReference, tnxResultCode);
		}
		if (log.isDebugEnabled()) {
			log.debug("[PaymentBrokerQiwiECommerceImpl::getReponseData()] - Staus-" + status + " Error Code-" + errorCode
					+ " Error Spec-" + errorSpecification);
		}

		log.debug("[PaymentBrokerQiwiECommerceImpl::getReponseData()] End");

		return ipgResponseDTO;
	}

}
