package com.isa.thinair.paymentbroker.core.bl.ebs;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.platform.api.ServiceResponce;

public class PaymentBrokerEbsTemplate extends PaymentBrokerTemplate implements PaymentBroker {
	
	protected static final String ERROR_CODE_PREFIX = "ogone.0";
	
	protected String sha1In;

	protected String sha1Out;
	
	protected static final String AUTHORIZATION_SUCCESS = "5";

	protected static final String PAYMENT_SUCCESS = "9";

	protected static final String REFUND_SUCCESS = "8";

	protected static final String REFUND_PENDING = "81";

	protected static final String CANCELLATION_SUCCESS = "61";

	protected static final String AUTHORIZED_AND_CANCELLED = "6";
	
	protected String maintenanceURL;
	
	protected String operation;
	
	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();	

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment,
			String pnr, AppIndicatorEnum appIndicator, TnxModeEnum tnxMode)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment,
			String pnr, AppIndicatorEnum appIndicator, TnxModeEnum tnxMode,
			List<CardDetailConfigDTO> cardDetailConfigData)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment,
			String pnr, AppIndicatorEnum appIndicator, TnxModeEnum tnxMode)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment,
			String pnr, AppIndicatorEnum appIndicator, TnxModeEnum tnxMode)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}
	

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO,
			List<CardDetailConfigDTO> configDataList) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap,
			IPGResponseDTO ipgResponseDTO) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection creditCardPayment,
			boolean refundFlag) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setIPGIdentificationParams(
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO)
			throws ModuleException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isEnableRefundByScheduler() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getRefundWithoutCardDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(
			IPGPrepPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	
	public String getSha1In() {
		return sha1In;
	}

	public void setSha1In(String sha1In) {
		this.sha1In = sha1In;
	}

	public String getSha1Out() {
		return sha1Out;
	}

	public void setSha1Out(String sha1Out) {
		this.sha1Out = sha1Out;
	}

	public static String getAuthorizationSuccess() {
		return AUTHORIZATION_SUCCESS;
	}

	public static String getPaymentSuccess() {
		return PAYMENT_SUCCESS;
	}

	public static String getRefundSuccess() {
		return REFUND_SUCCESS;
	}

	public static String getRefundPending() {
		return REFUND_PENDING;
	}

	public static String getCancellationSuccess() {
		return CANCELLATION_SUCCESS;
	}

	public static String getAuthorizedAndCancelled() {
		return AUTHORIZED_AND_CANCELLED;
	}
	

	public String getMaintenanceURL() {
		return maintenanceURL;
	}

	public void setMaintenanceURL(String maintenanceURL) {
		this.maintenanceURL = maintenanceURL;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	protected String composeMerchantTransactionId(AppIndicatorEnum appIndicatorEnum, long auditId,
			PaymentConstants.MODE_OF_SERVICE mode) {
		if (mode.equals(PaymentConstants.MODE_OF_SERVICE.REFUND)) {
			return appIndicatorEnum.getCode() + (new Long(auditId)).toString() + MODE_OF_SERVICE_REFUND_PREFIX;
		} else if (mode.equals(PaymentConstants.MODE_OF_SERVICE.VOID)) {
			return appIndicatorEnum.getCode() + (new Long(auditId)).toString() + MODE_OF_SERVICE_VOID_PREFIX;
		} else if (mode.equals(PaymentConstants.MODE_OF_SERVICE.QUERYDR)) {
			return appIndicatorEnum.getCode() + (new Long(auditId)).toString() + MODE_OF_SERVICE_QUERY_PREFIX;
		} else if (mode.equals(PaymentConstants.MODE_OF_SERVICE.CAPTURE)) {
			return appIndicatorEnum.getCode() + (new Long(auditId)).toString() + MODE_OF_SERVICE_CAPTURE_PREFIX;
		} else if (mode.equals(PaymentConstants.MODE_OF_SERVICE.STATUS)) {
			return appIndicatorEnum.getCode() + (new Long(auditId)).toString() + MODE_OF_SERVICE_STATUS_PREFIX;
		} else {
			return appIndicatorEnum.getCode() + (new Long(auditId)).toString();
		}
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");		
	}

	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
}
