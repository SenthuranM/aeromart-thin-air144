package com.isa.thinair.paymentbroker.core.bl.mpg;

import java.util.Map;

public class MpgResponse {

	public static String TRANSACTION_APPROVED = "1";
	
	public static final String VALID_HASH = "VALID_HASH";
	public static final String INVALID_HASH = "INVALID_HASH";
	public static final String INVALID_RESPONSE = "INVALID_RESPONSE";
	public static final String NO_VALUE = "";
	
	private String responseCode;
	private String reasonCode;
	private String reasonDescription;
	private String merchantId;
	private String acquirerId;
	private String merchantOrderId;
	private String signature;
	private String signatureMethod;
	private String referenceNumber;
	private String cardNumber;
	private String authorizationCode;
	private String billingAddress;
	private String shippingAddress;
	
	public void setResponse(Map receipMap) {
	
		// common for all 3 response types
		setReasonCode(receipMap.get(MpgRequest.REASON_CODE) != null ? receipMap.get(MpgRequest.REASON_CODE).toString() : null);
		setReasonDescription(receipMap.get(MpgRequest.REASON_CODE_DESC) != null ? receipMap.get(MpgRequest.REASON_CODE_DESC).toString() : null);
		setResponseCode(receipMap.get(MpgRequest.RESPONSE_CODE) != null ? receipMap.get(MpgRequest.RESPONSE_CODE).toString() : null);
		
		// for response type 2
		setMerchantOrderId(receipMap.get(MpgRequest.ORDER_ID) != null ? receipMap.get(MpgRequest.ORDER_ID).toString() : null);
		setSignatureMethod(receipMap.get(MpgRequest.SIGNATURE_METHOD) != null ? receipMap.get(MpgRequest.SIGNATURE_METHOD).toString() : null);

		// for response type 1
		setMerchantId(receipMap.get(MpgRequest.MERCHANT_ID) != null ? receipMap.get(MpgRequest.MERCHANT_ID).toString() : null);
		setSignature(receipMap.get(MpgRequest.SIGNATURE) != null ? receipMap.get(MpgRequest.SIGNATURE).toString() : null);
		setAcquirerId(receipMap.get(MpgRequest.ACQUIRER_ID) != null ? receipMap.get(MpgRequest.ACQUIRER_ID).toString() : null);
		setReferenceNumber(receipMap.get(MpgRequest.REFERENCE_NUMBER) != null ? receipMap.get(MpgRequest.REFERENCE_NUMBER).toString() : null);
		setCardNumber(receipMap.get(MpgRequest.CARD_NUMBER) != null ? receipMap.get(MpgRequest.CARD_NUMBER).toString() : null);
		setAuthorizationCode(receipMap.get(MpgRequest.AUTHORIAZTION_CODE) != null ? receipMap.get(MpgRequest.AUTHORIAZTION_CODE).toString() : null);
		setBillingAddress(receipMap.get(MpgRequest.BILLING_ADDRESS) != null ? receipMap.get(MpgRequest.BILLING_ADDRESS).toString() : null);
		setShippingAddress(receipMap.get(MpgRequest.SHIPPING_ADDRESS) != null ? receipMap.get(MpgRequest.SHIPPING_ADDRESS).toString() : null);
	}
	
	@Override
	public String toString(){
		String str = "";
		
		if(!"".equals(responseCode)){
			str += "\n Response Code " + responseCode;
		}if(!"".equals(reasonCode)){
			str += "\n Reason Code " + reasonCode;
		}if(!"".equals(reasonDescription)){
			str += "\n Reason Description " + reasonDescription;
		}if(!"".equals(merchantId)){
			str += "\n Merchant Id " + merchantId;
		}if(!"".equals(acquirerId)){
			str += "\n Acquirer Id " + acquirerId;
		}if(!"".equals(merchantOrderId)){
			str += "\n Merchant Order Id " + merchantOrderId;
		}if(!"".equals(signature)){
			str += "\n Signature " + signature;
		}if(!"".equals(referenceNumber)){
			str += "\n Reference Number " + referenceNumber;
		}if(!"".equals(cardNumber)){
			str += "\n Card Number " + cardNumber;
		}if(!"".equals(authorizationCode)){
			str += "\n Authorization Code " + authorizationCode;
		}if(!"".equals(billingAddress)){
			str += "\n Billing Address " + billingAddress;
		}if(!"".equals(shippingAddress)){
			str += "\n Shipping Address " + shippingAddress;
		}
		
		return str;
	}
	
	/**
	 * @return the responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}

	/**
	 * @param responseCode the responseCode to set
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * @return the reasonCose
	 */
	public String getReasonCode() {
		return reasonCode;
	}

	/**
	 * @param reasonCose the reasonCose to set
	 */
	public void setReasonCode(String reasonCose) {
		this.reasonCode = reasonCose;
	}

	/**
	 * @return the reasonDescription
	 */
	public String getReasonDescription() {
		return reasonDescription;
	}

	/**
	 * @param reasonDescription the reasonDescription to set
	 */
	public void setReasonDescription(String reasonDescription) {
		this.reasonDescription = reasonDescription;
	}

	/**
	 * @return the merchantId
	 */
	public String getMerchantId() {
		return merchantId;
	}

	/**
	 * @param merchantId the merchantId to set
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	/**
	 * @return the acquirerId
	 */
	public String getAcquirerId() {
		return acquirerId;
	}

	/**
	 * @param acquirerId the acquirerId to set
	 */
	public void setAcquirerId(String acquirerId) {
		this.acquirerId = acquirerId;
	}

	/**
	 * @return the merchantOrderId
	 */
	public String getMerchantOrderId() {
		return merchantOrderId;
	}

	/**
	 * @param merchantOrderId the merchantOrderId to set
	 */
	public void setMerchantOrderId(String merchantOrderId) {
		this.merchantOrderId = merchantOrderId;
	}

	/**
	 * @return the signature
	 */
	public String getSignature() {
		return signature;
	}

	/**
	 * @param signature the signature to set
	 */
	public void setSignature(String signature) {
		this.signature = signature;
	}

	/**
	 * @return the referenceNumber
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * @param referenceNumber the referenceNumber to set
	 */
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * @param cardNumber the cardNumber to set
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return the authorizationCode
	 */
	public String getAuthorizationCode() {
		return authorizationCode;
	}

	/**
	 * @param authorizationCode the authorizationCode to set
	 */
	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	/**
	 * @return the billingAddress
	 */
	public String getBillingAddress() {
		return billingAddress;
	}

	/**
	 * @param billingAddress the billingAddress to set
	 */
	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	/**
	 * @return the shippingAddress
	 */
	public String getShippingAddress() {
		return shippingAddress;
	}

	/**
	 * @param shippingAddress the shippingAddress to set
	 */
	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public String getSignatureMethod() {
		return signatureMethod;
	}

	public void setSignatureMethod(String signatureMethod) {
		this.signatureMethod = signatureMethod;
	}
}
