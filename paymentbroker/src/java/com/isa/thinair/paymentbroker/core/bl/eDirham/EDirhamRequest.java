package com.isa.thinair.paymentbroker.core.bl.eDirham;

public class EDirhamRequest {

	public static final String ACTION = "Action";

	public static final String BANKID = "BankID";

	public static final String MERCHANTID = "MerchantID";

	public static final String TERMINALID = "TerminalID";

	public static final String APPLICATIONNUMBER = "ApplicationNumber";

	public static final String TRANSACTIONREQUESTDATE = "TransactionRequestDate";

	public static final String SECUREHASH = "SecureHash";

	public static final String UNIQUETRANSACTIONID = "UniqueTransactionID";

	public static final String VERSION = "Version";

	public static final String CURRENCY = "Currency";

	public static final String PAYMENTMETHODTYPE = "PaymentMethodType";

	public static final String EXTRAFIELDS_INTENDEDEDIRHAMSERVICE = "ExtraFields_intendedEDirhamService";

	public static final String ESERVICEMAINCODESUBCODE_1 = "EServiceMainCodeSubCode_1";

	public static final String ESERVICEPRICE_1 = "EServicePrice_1";

	public static final String ESERVICEQUANTITY_1 = "EServiceQuantity_1";

	public static final String EXTRAFIELDS_F14 = "ExtraFields_f14";

	public static final String EXTRAFIELDS_F16 = "ExtraFields_f16";

	public static final String EXTRAFIELDS_F18 = "ExtraFields_f18";

	public static final String LANGUAGE = "Lang";

	public static final String MERCHANTMODULESESSIONID = "MerchantModuleSessionID";

	public static final String PUN = "PUN";

	public static final String PAYMENTDESCRIPTION = "PaymentDescription";

	public static final String FIELD61_SERVICECODEQUANTITY = "Field61_ServiceCodeQuantity";

	public static final String FIELD61_SERVICEMAINCODESUBCODE = "Field61_ServiceMainCodeSubCode";

	public static final String FIELD63_ORIGINALTRANSACTIONUNIQUEID = "Field63_OriginalTransactionUniqueID";

	public static final String TRANSACTIONAMOUNT = "TransactionAmount";

}
