package com.isa.thinair.paymentbroker.core.bl.paypal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.XMLStreamer;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PAYPALRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.paypal.ProfileDTO;
import com.isa.thinair.paymentbroker.api.dto.paypal.UserInputDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerPAYPALTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.wsclient.api.service.WSClientBD;
import com.paypal.soap.api.CurrencyCodeType;
import com.paypal.soap.api.DoDirectPaymentRequestType;
import com.paypal.soap.api.DoDirectPaymentResponseType;
import com.paypal.soap.api.ErrorType;
import com.paypal.soap.api.GetTransactionDetailsRequestType;
import com.paypal.soap.api.GetTransactionDetailsResponseType;
import com.paypal.soap.api.PaymentActionCodeType;
import com.paypal.soap.api.RefundTransactionRequestType;
import com.paypal.soap.api.RefundTransactionResponseType;

public class PaymentBrokerPAYPALMOTOImpl extends PaymentBrokerPAYPALTemplate implements PaymentBroker {

	private static Log log = LogFactory.getLog(PaymentBrokerPAYPALMOTOImpl.class);

	private PAYPALRequestDTO paypalRequestDTO = new PAYPALRequestDTO();

	private ProfileDTO profileDTO = new ProfileDTO();

	private UserInputDTO userInputDTO = new UserInputDTO();

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		String status;
		String errorCode;
		Integer paymentBrokerRefNo;
		int txnResultCode;
		String errorSpecification;

		CreditCardTransaction cccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
		setProfileData();
		userInputDTO.setTransactionId(cccTransaction.getTransactionId());

		userInputDTO.setRefundType(PAYPALPaymentUtils.PARTIAL_REFUND);

		String amount = creditCardPayment.getAmount();

		userInputDTO.setAmount(amount);

		// create a hash map for the response data
		DefaultServiceResponse serviceResponse = new DefaultServiceResponse(false);
		// Get a merchant transaction id to identify txn in the payment server, in case of failure.
		String merchantTxnId = composeMerchantTransactionId(appIndicator, creditCardPayment.getTemporyPaymentId(),
				PaymentConstants.MODE_OF_SERVICE.REFUND);

		WSClientBD paypalWebervices = ReservationModuleUtils.getWSClientBD();
		RefundTransactionRequestType refundRequest = paypalWebervices.prepareRefund(paypalRequestDTO);
		CreditCardTransaction ccTransaction = auditTransaction(pnr, "", merchantTxnId,
				new Integer(creditCardPayment.getTemporyPaymentId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				XMLStreamer.compose(refundRequest), "", creditCardPayment.getIpgIdentificationParamsDTO()
				.getFQIPGConfigurationName(), null, false);

		paymentBrokerRefNo = new Integer(ccTransaction.getTransactionRefNo());

		RefundTransactionResponseType refundResponse = paypalWebervices.doRefundWebServiceCall(paypalRequestDTO, refundRequest);

		if (refundResponse != null && refundResponse.getAck().getValue().toString().equalsIgnoreCase(PAYPALPaymentUtils.SUCCESS)) {
			status = PAYPALPaymentUtils.ACCEPTED;
			txnResultCode = 1;
			errorSpecification = "";
			errorCode = "";
			serviceResponse.setSuccess(true);
		} else {
			status = PAYPALPaymentUtils.REJECTED;
			txnResultCode = 0;
			ErrorType[] errorArry = refundResponse.getErrors();
			errorCode = errorArry[0].getErrorCode().toString();
			errorSpecification = status + " " + PAYPALPaymentUtils.getFilteredErrorCode(errorCode) + " " + "";
		}

		updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), XMLStreamer.compose(refundResponse), errorSpecification,
				"", refundResponse.getRefundTransactionID(), txnResultCode);

		// Populate service response object
		serviceResponse.setResponseCode(String.valueOf(paymentBrokerRefNo));
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, "");
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE,
				PAYPALPaymentUtils.getFilteredErrorCode(errorCode));
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, "");

		return serviceResponse;
	}

	/**
	 * Returns Payment Gateway Name
	 */
	@Override
	public String getPaymentGatewayName() {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		String status;
		String errorCode;
		Integer paymentBrokerRefNo;
		int txnResultCode;
		String errorSpecification;

		setPPRequestDTO(creditCardPayment, pnr, appIndicator, tnxMode, cardDetailConfigData);

		// create a hash map for the response data
		DefaultServiceResponse serviceResponse = new DefaultServiceResponse(false);
		// Get a merchant transaction id to identify txn in the payment server, in case of failure.
		String merchantTxnId = composeMerchantTransactionId(appIndicator, creditCardPayment.getTemporyPaymentId(),
				PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		WSClientBD paypalWebervices = ReservationModuleUtils.getWSClientBD();
		DoDirectPaymentRequestType directPaymentRequest = paypalWebervices.prepareDoDirectPaymentRequestType(paypalRequestDTO);
		CreditCardTransaction ccTransaction = auditTransaction(pnr, "", merchantTxnId,
				new Integer(creditCardPayment.getTemporyPaymentId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				XMLStreamer.compose(directPaymentRequest), "", creditCardPayment.getIpgIdentificationParamsDTO()
						.getFQIPGConfigurationName(), null, false);

		paymentBrokerRefNo = new Integer(ccTransaction.getTransactionRefNo());

		DoDirectPaymentResponseType directPaymentResponse = paypalWebervices.doDirectPaymentWebServiceCall(paypalRequestDTO,
				directPaymentRequest);

		if (directPaymentResponse.getAck().getValue().toString().equalsIgnoreCase(PAYPALPaymentUtils.SUCCESS)) {
			status = PAYPALPaymentUtils.ACCEPTED;
			txnResultCode = 1;
			errorSpecification = "";
			errorCode = "";
			serviceResponse.setSuccess(true);
		} else {
			status = PAYPALPaymentUtils.REJECTED;
			txnResultCode = 0;
			ErrorType[] errorArry = directPaymentResponse.getErrors();
			errorCode = errorArry[0].getErrorCode().toString();
			errorSpecification = status + " " + PAYPALPaymentUtils.getFilteredErrorCode(errorCode) + " " + "";
		}

		updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), XMLStreamer.compose(directPaymentResponse),
				errorSpecification, "", directPaymentResponse.getTransactionID(), txnResultCode);

		// Populate service response object
		serviceResponse.setResponseCode(String.valueOf(paymentBrokerRefNo));
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, "");
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE,
				PAYPALPaymentUtils.getFilteredErrorCode(errorCode));
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, "");

		return serviceResponse;
	}

	public void setPPRequestDTO(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {

		setProfileData();

		userInputDTO = creditCardPayment.getUserInputDTO();

		userInputDTO.setAmount(creditCardPayment.getAmount());
		userInputDTO.setCurrencyCodeType(CurrencyCodeType.fromString(creditCardPayment.getCurrency()));
		userInputDTO.setInvoiceId(creditCardPayment.getPnr());
		userInputDTO.setIPAddress(creditCardPayment.getUserIP());
		userInputDTO.setNoShipping(PAYPALPaymentUtils.NoShipping);
		userInputDTO.setPaymentAction(PaymentActionCodeType.Sale);
		userInputDTO.setTypeOfGoods(PAYPALPaymentUtils.TypeOfGoods);
		userInputDTO.setCreditCardNumber(creditCardPayment.getCardNumber());
		userInputDTO.setCreditCardType(getStandardCardType(creditCardPayment.getCardType()));
		userInputDTO.setCVV2(creditCardPayment.getCvvField());
		userInputDTO.setExpMonth(Integer.parseInt(creditCardPayment.getExpiryDate().substring(2, 4)));
		userInputDTO.setExpYear(2000 + Integer.parseInt(creditCardPayment.getExpiryDate().substring(0, 2)));

		paypalRequestDTO.setUserInputDTO(userInputDTO);
	}

	public void setProfileData() {
		Properties ipgProps = getProperties();

		profileDTO.setMode(ipgProps.getProperty(PAYPALPaymentUtils.mode));
		profileDTO.setApiUsername(ipgProps.getProperty(PAYPALPaymentUtils.apiUsername));
		profileDTO.setApiPassword(ipgProps.getProperty(PAYPALPaymentUtils.apiPassword));
		profileDTO.setSignature(ipgProps.getProperty(PAYPALPaymentUtils.signature));
		profileDTO.setEnvironment(ipgProps.getProperty(PAYPALPaymentUtils.environment));
		profileDTO.setCertificateFilePath(ipgProps.getProperty(PAYPALPaymentUtils.certificateFilePath));
		profileDTO.setPrivateKeyPassword(ipgProps.getProperty(PAYPALPaymentUtils.privateKeyPassword));
		profileDTO.setUseProxy(ipgProps.getProperty(PAYPALPaymentUtils.useProxy));
		profileDTO.setProxyHost(ipgProps.getProperty(PAYPALPaymentUtils.proxyHost));
		profileDTO.setProxyPort(ipgProps.getProperty(PAYPALPaymentUtils.proxyPort));
		profileDTO.setRedirectUrl(ipgProps.getProperty(PAYPALPaymentUtils.redirectUrl));
		profileDTO.setCancelUrl(ipgProps.getProperty(PAYPALPaymentUtils.cancelUrl));
		profileDTO.setNote(ipgProps.getProperty(PAYPALPaymentUtils.note));
		profileDTO.setOrderDescription(ipgProps.getProperty(PAYPALPaymentUtils.orderDescription));

		paypalRequestDTO.setProfileDTO(profileDTO);
		log.info("Get configuration info");
	}

	private String getStandardCardType(int card) {
		// [1-MASTER, 2-VISA, 3-AMEX, 4-DINNERS, 5-GENERIC, 11-PAYPAL]
		String myString = null;
		char[] stringArray = PaymentType.getPaymentTypeDesc(card).toLowerCase().toCharArray();
		stringArray[0] = Character.toUpperCase(stringArray[0]);
		myString = new String(stringArray);
		
		//PayPal accepts 'MasterCard', not 'Master' in its fromString method in type CreditCardTypeType
		if("Master".equals(myString)){
			myString = "MasterCard";
		}
		
		return myString;
	}

	/**
	 * Updates the Audit Transaction
	 * 
	 * @param transactionRefNo
	 * @param plainTextReceipt
	 * @param errorSpecification
	 * @param authorizationCode
	 * @param transactionId
	 * @param tnxResultCode
	 */
	protected void updateAuditTransactionByTnxRefNo(int transactionRefNo, String plainTextReceipt, String errorSpecification,
			String authorizationCode, String transactionId, int tnxResultCode) {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(transactionRefNo);
		creditCardTransaction.setResponseText(plainTextReceipt);
		creditCardTransaction.setErrorSpecification(errorSpecification);
		creditCardTransaction.setAidCccompnay(authorizationCode);
		creditCardTransaction.setResultCode(0);
		creditCardTransaction.setTransactionResultCode(tnxResultCode);
		creditCardTransaction.setTransactionId(transactionId);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(creditCardTransaction);
	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {
		String status;
		String errorCode;
		Integer paymentBrokerRefNo;
		int txnResultCode;
		String errorSpecification;
		Iterator itColIPGQueryDTO = colIPGQueryDTO.iterator();
		Collection oResultIF = new ArrayList();
		Collection oResultIP = new ArrayList();
		Collection oResultII = new ArrayList();
		Collection oResultIS = new ArrayList();
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		IPGQueryDTO ipgQueryDTO;
		CreditCardTransaction oCreditCardTransaction;
		PreviousCreditCardPayment oPrevCCPayment;
		CreditCardPayment oCCPayment;

		setProfileData();

		while (itColIPGQueryDTO.hasNext()) {
			ipgQueryDTO = (IPGQueryDTO) itColIPGQueryDTO.next();
			oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgQueryDTO.getPaymentBrokerRefNo());
			try {
				userInputDTO.setTransactionId(oCreditCardTransaction.getTransactionId());
				paypalRequestDTO.setUserInputDTO(userInputDTO);
				WSClientBD paypalWebervices = ReservationModuleUtils.getWSClientBD();
				GetTransactionDetailsRequestType pprequest = paypalWebervices
						.prepareDoGetTransactionDetailsCode(paypalRequestDTO);
				String merchantTxnId = composeMerchantTransactionId(ipgQueryDTO.getAppIndicatorEnum(),
						oCreditCardTransaction.getTemporyPaymentId(), PaymentConstants.MODE_OF_SERVICE.QUERYDR);
//				CreditCardTransaction ccTransaction = auditTransaction(oCreditCardTransaction.getTransactionReference(), "",
//						merchantTxnId, new Integer(oCreditCardTransaction.getTemporyPaymentId()),
//						bundle.getString("SERVICETYPE_AUTHORIZE"), XMLStreamer.compose(pprequest), "", PAYPALPaymentUtils.paypal,
//						null, false);
				GetTransactionDetailsResponseType serviceResponce = paypalWebervices.doGetTransactionDetailsCode(pprequest,
						paypalRequestDTO);

				if (serviceResponce.getAck().getValue().toString().equalsIgnoreCase(PAYPALPaymentUtils.SUCCESS)) {
					status = PAYPALPaymentUtils.ACCEPTED;
					txnResultCode = 1;
					errorSpecification = "";
					errorCode = "";
				} else {
					status = PAYPALPaymentUtils.REJECTED;
					txnResultCode = 0;
					ErrorType[] errorArry = serviceResponce.getErrors();
					errorCode = errorArry[0].getErrorCode().toString();
					errorSpecification = status + " " + PAYPALPaymentUtils.getFilteredErrorCode(errorCode) + " " + "";
				}
//				paymentBrokerRefNo = new Integer(ccTransaction.getTransactionRefNo());
//				updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), XMLStreamer.compose(serviceResponce),
//						errorSpecification, "", serviceResponce.getCorrelationID(), txnResultCode);

				// If successful payment remains at Bank
				if (serviceResponce.getPaymentTransactionDetails().getPaymentInfo().getPaymentStatus().getValue().toString()
						.equalsIgnoreCase(PAYPALPaymentUtils.COMPLETED)) {
					if (refundFlag) {
						// Do refund

						oPrevCCPayment = new PreviousCreditCardPayment();
						oPrevCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());

						oCCPayment = new CreditCardPayment();
						oCCPayment.setPreviousCCPayment(oPrevCCPayment);
						oCCPayment.setAmount(ipgQueryDTO.getAmount().toString());
						oCCPayment.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
						oCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
						oCCPayment.setIpgIdentificationParamsDTO(ipgQueryDTO.getIpgIdentificationParamsDTO());
						oCCPayment.setTnxMode(TnxModeEnum.SECURE_3D);
						oCCPayment.setPnr(oCreditCardTransaction.getTransactionReference());
						oCCPayment.setTemporyPaymentId(oCreditCardTransaction.getTemporyPaymentId());

						ServiceResponce srRev = refund(oCCPayment, oCCPayment.getPnr(), oCCPayment.getAppIndicator(),
								oCCPayment.getTnxMode());

						if (srRev.isSuccess()) {
							// Change State to 'IS'
							oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
							log.info("Status moing to IS:" + oCreditCardTransaction.getTemporyPaymentId());
						} else {
							// Change State to 'IF'
							oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
							log.info("Status moing to IF:" + oCreditCardTransaction.getTemporyPaymentId());
						}
					} else {
						// Change State to 'IP'
						oResultIP.add(oCreditCardTransaction.getTemporyPaymentId());
						log.info("Status moing to IP:" + oCreditCardTransaction.getTemporyPaymentId());
					}
				}
				// No payment detail at bank
				else {
					// No Payments exist
					// Update status to 'II'
					oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
					log.info("Status moing to II:" + oCreditCardTransaction.getTemporyPaymentId());
				}
			} catch (ModuleException e) {
				log.info("Scheduler::ResolvePayment FAILED!!! " + e.getMessage(), e);
			}
		}
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF, oResultIF);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_II, oResultII);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IP, oResultIP);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS, oResultIS);
		sr.setSuccess(true);

		return sr;
	}

	@Override
	public void setIPGIdentificationParams(IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		// TODO Auto-generated method stub

	}

	@Override
	public String getRefundWithoutCardDetails() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");		
	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

}
