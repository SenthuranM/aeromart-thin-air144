package com.isa.thinair.paymentbroker.core.bl.payFort.v2;

import com.isa.thinair.aircustomer.api.service.AirCustomerModuleUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;

import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class PaymentBrokerPayFortUtils {

	public static final String DAYS = "days";
	public static final String HOURS = "hours";
	public static final String MINUTES = "minutes";

	private PaymentBrokerPayFortUtils() {
	}

	public static String getFormattedDate(Date date) {
		return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").format(date);
	}

	public static ApplicationEngine getApplicationEngine(CreditCardTransaction creditCardTransaction) {
		ApplicationEngine applicationEngine = null;
		String appIndicator = StringUtils.left(creditCardTransaction.getTempReferenceNum(), 1);
		if (StringUtils.isNotEmpty(appIndicator)) {
			char appCode = CharUtils.toChar(appIndicator);
			if (appCode == AppIndicatorEnum.APP_IBE.getCode()) {
				applicationEngine = ApplicationEngine.IBE;
			} else if (appCode == AppIndicatorEnum.APP_XBE.getCode()) {
				applicationEngine = ApplicationEngine.XBE;
			} else if (appCode == AppIndicatorEnum.APP_ANDROID.getCode()) {
				applicationEngine = ApplicationEngine.ANDROID;
			} else if (appCode == AppIndicatorEnum.APP_IOS.getCode()) {
				applicationEngine = ApplicationEngine.IOS;
			} 
		}
		return applicationEngine;
	}

	public static void sendEmail(String topicName, HashMap parameters, IPGRequestDTO ipgRequestDTO, Locale locale) {
		if (StringUtils.isNoneBlank(ipgRequestDTO.getContactEmail())) {
			UserMessaging userMessaging = new UserMessaging();
			MessageProfile msgProfile = new MessageProfile();
			Topic topic = new Topic();

			userMessaging.setToAddres(ipgRequestDTO.getContactEmail());
			userMessaging.setFirstName(ipgRequestDTO.getContactFirstName());

			msgProfile.setUserMessagings(Collections.singletonList(userMessaging));

			topic.setTopicName(topicName);
			topic.setTopicParams(parameters);

			if (locale != null) {
				topic.setLocale(locale);
			}
			msgProfile.setTopic(topic);
			AirCustomerModuleUtils.getMessagingServiceBD().sendMessage(msgProfile);
		}
	}

	public static String getRemainingTimeFromNow(Date date) {
		long timeDiffInMillis = date.getTime() - new Date().getTime();
		long days = timeDiffInMillis / DateUtils.MILLIS_PER_DAY;
		long hours = (timeDiffInMillis % DateUtils.MILLIS_PER_DAY) / DateUtils.MILLIS_PER_HOUR;
		long minutes = (timeDiffInMillis % DateUtils.MILLIS_PER_HOUR) / DateUtils.MILLIS_PER_MINUTE;

		String remainingTime = new StringBuilder().append(days > 0 ? days + " days " : "")
				.append(hours > 0 ? hours + " hours " : "").toString();
		return StringUtils.isEmpty(remainingTime) ? minutes + " minutes" : remainingTime + " and " + minutes + " minutes";
	}

	public static void populateRemainingDaysHoursAndMin(Date date, Map map) {
		long timeDiffInMillis = date.getTime() - new Date().getTime();
		long days = timeDiffInMillis / DateUtils.MILLIS_PER_DAY;
		long hours = (timeDiffInMillis % DateUtils.MILLIS_PER_DAY) / DateUtils.MILLIS_PER_HOUR;
		long minutes = (timeDiffInMillis % DateUtils.MILLIS_PER_HOUR) / DateUtils.MILLIS_PER_MINUTE;

		if (hours > 0) {
			map.put(HOURS, hours);
		}
		if (minutes > 0) {
			map.put(MINUTES, minutes);
		}

		if (days > 0) {
			map.put(DAYS, days);
		}
	}

}
