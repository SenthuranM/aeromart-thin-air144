package com.isa.thinair.paymentbroker.core.bl.tapOffline;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.ConfirmReservationUtil;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys.OnHold;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.bl.payFort.PayFortPaymentUtils;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class PaymentBrokerTapOfflineImpl extends PaymentBrokerTemplate implements PaymentBroker {

	private static Log log = LogFactory.getLog(PaymentBrokerTapOfflineImpl.class);

	private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

	public static final String CARD_TYPE_GENERIC = "GC";

	private String deviceLicenseID;

	private String hash;

	private String merchantName;

	private String createInvoiceUrl;

	private String cancelInvoiceUrl;

	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		
		
		log.debug("[PaymentBrokerTapOfflineImpl::refund()] Begin");
		int tnxResultCode;
		String errorCode;
		String errorSpecification;
		Integer newpaymentBrokerRefNo;
		TapOfflineResponse response;

		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
		String merchTnxRef = ccTransaction.getTempReferenceNum();

		String refundAmountStr = creditCardPayment.getAmount();
		DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);

		if (creditCardPayment.getPreviousCCPayment() != null && getRefundEnabled() != null && getRefundEnabled().equals("true")) {

			if (log.isDebugEnabled()) {
				log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
				log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
				log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
				log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
				log.debug("CC Old pay Formatted sAmount : " + refundAmountStr);
				log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());
			}

			PaymentQueryDR queryDR = getPaymentQueryDR();
			IPGConfigsDTO tapIPGDTO = TapOfflinePaymentUtils.getIPGConfigs(getMerchantId());

			IPGRequestDTO ipgRequestDTO = new IPGRequestDTO();
			ipgRequestDTO.setAmount(refundAmountStr);
			
			Map<String, String> postDataMap = createPostDataMap(ipgRequestDTO, ccTransaction.getTransactionId(),
					PaymentConstants.MODE_OF_SERVICE.REFUND);
			String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

			ServiceResponce serviceResponce = queryDR.query(ccTransaction, tapIPGDTO,
					PaymentBrokerInternalConstants.QUERY_CALLER.VERIFY);

			if (serviceResponce.isSuccess()) {
				String updatedMerchTnxRef = merchTnxRef + PaymentConstants.MODE_OF_SERVICE.REFUND;

				try {
					response = TapOfflinePaymentUtils.getCreateInvoiceResponse(postDataMap, getReversalURL(), isUseProxy(),
							getProxyHost(), getProxyPort());

				} catch (Exception e) {
					log.error(e);
					throw new ModuleException("paymentbroker.error.unknown");
				}

				CreditCardTransaction ccNewTransaction = auditTransaction(pnr, getMerchantId(), updatedMerchTnxRef,
						creditCardPayment.getTemporyPaymentId(), bundle.getString("SERVICETYPE_REFUND"), strRequestParams, "",
						getPaymentGatewayName(), null, false);

				newpaymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());
			} else {
				defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						"paymentbroker.ameria.payment.fields.response.invalid");
				return defaultServiceResponse;
			}

			if (response == null) {
				defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						"paymentbroker.ameria.payment.refund.response.invalid");
				return defaultServiceResponse;
			}

			// got refund response
			Integer paymentBrokerRefNo = new Integer(ccTransaction.getTransactionRefNo());
			log.info("[PaymentBrokerTapOfflineImpl::refund()] Refund Status : " + response.toString());

			if (response.getResMsg().equals(TapOfflinePaymentUtils.SUCCESS)
					|| response.getResMsg().equals(TapOfflinePaymentUtils.PENDING)) {

				errorCode = "";

				tnxResultCode = 1;
				errorSpecification = "";
				defaultServiceResponse.setSuccess(true);
				defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);

			} else {
				String status = IPGResponseDTO.STATUS_REJECTED;
				errorCode = response.getResCode();
				String transactionMsg;
				if (!StringUtil.isNullOrEmpty(response.getResMsg())) {
					transactionMsg = response.getResMsg();
				} else {

					errorCode = response.getResCode();
					transactionMsg = response.getResMsg();
				}

				tnxResultCode = 0;
				errorSpecification = "Status:" + status + " Error code:" + errorCode + " Error Desc:" + transactionMsg;

				defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
			}
			
			Map<String, String> refundResponseMap = new HashMap<String, String>();
			refundResponseMap.put(response.TAP_REF_ID, response.getTapRefID());
			refundResponseMap.put(response.RES_CODE, response.getResCode());
			refundResponseMap.put(response.RES_MESSAGE, response.getResMsg());
			
			String strResponseParams = PaymentBrokerUtils.getRequestDataAsString(refundResponseMap);

			updateAuditTransactionByTnxRefNo(newpaymentBrokerRefNo.intValue(), strResponseParams, errorSpecification,
					response.getResCode(), response.getTapRefID(), tnxResultCode);
			defaultServiceResponse.setResponseCode(String.valueOf(paymentBrokerRefNo));
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, null);
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
		
		} else if (getRefundEnabled() != null && getRefundEnabled().equals("false")) {
			log.info("[PaymentBrokerTapOfflineImpl::refund()] Refund not Enabled");
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.REFUND_OPERATION_NOT_SUPPORTED);

		} else {
			log.info("[PaymentBrokerTapOfflineImpl::refund()] Cannot Refund");
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);
		}

		log.debug("[PaymentBrokerTapOfflineImpl::refund()] End");

		return defaultServiceResponse;
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		if (creditCardPayment.isOfflinePaymentCancel()) {
			if (isAllowSendCancelRequest()) {
				return cancelInvoice(creditCardPayment);
			}
		} else {
			return refund(creditCardPayment, pnr, appIndicator, tnxMode);
		}
		return null;
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		log.debug("[PaymentBrokerTapOfflineImpl::getRequestData()] Begin ");

		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		TapOfflineResponse response = null;
		Map<String, String> postDataMap = createPostDataMap(ipgRequestDTO, merchantTxnId,
				PaymentConstants.MODE_OF_SERVICE.PAYMENT);
		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();
		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();

		String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);
		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				strRequestParams + "," + sessionID, "", getPaymentGatewayName(), null, false);

		// calling remote
		try {
			response = TapOfflinePaymentUtils.getCreateInvoiceResponse(postDataMap, getCreateInvoiceUrl(), isUseProxy(),
					getProxyHost(), getProxyPort());

		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		if (response != null) {

			Map<String, String> postResponseDataMap = new LinkedHashMap<String, String>();
			postResponseDataMap.put(response.TAP_REF_ID, response.getTapRefID());
			postResponseDataMap.put(response.RES_CODE, response.getResCode());
			postResponseDataMap.put(response.RES_MESSAGE, response.getResMsg());
			ccTransaction.setResponseText(PaymentBrokerUtils.getRequestDataAsString(postResponseDataMap));
			ccTransaction.setAidCccompnay(response.getTapRefID());
			ccTransaction.setTransactionId(response.getTapRefID());

			if (response.getResMsg().equals(TapOfflinePaymentUtils.SUCCESS)) {
				// For display purpose
				postDataMap.put(response.TAP_REF_ID, response.getTapRefID());
				if (log.isDebugEnabled()) {
					log.debug("PaymentBrokerTapOfflineImpl PG Request Data : " + strRequestParams + ", sessionID : " + sessionID);
				}
				ipgRequestResultsDTO.setRequestData(strRequestParams);
				ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());

			} else {
				ccTransaction.setErrorSpecification(response.getResCode());
				ipgRequestResultsDTO.setRequestData("");
				ipgRequestResultsDTO.setPaymentBrokerRefNo(new Integer(0));
				ipgRequestResultsDTO.setErrorCode(response.getResMsg());
				log.error("PaymentBrokerTapOfflineImpl PG Request Data fails with error message : " + response.getResMsg());
			}
			PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(ccTransaction);
		}

		ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);
		ipgRequestResultsDTO.setPostDataMap(postDataMap);

		log.debug("[PaymentBrokerTapOfflineImpl::getRequestData()] End ");
		return ipgRequestResultsDTO;

	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection creditCardPayment, boolean refundFlag) throws ModuleException {
		log.info("[PaymentBrokerTapOfflineImpl::resolvePartialPayments()] Begin");

		Iterator itColIPGQueryDTO = creditCardPayment.iterator();

		Collection<Integer> oResultIF = new ArrayList<Integer>();
		Collection<Integer> oResultIP = new ArrayList<Integer>();
		Collection<Integer> oResultII = new ArrayList<Integer>();
		Collection<Integer> oResultIS = new ArrayList<Integer>();

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		IPGQueryDTO ipgQueryDTO;
		CreditCardTransaction oCreditCardTransaction;
		PaymentQueryDR query;
		PreviousCreditCardPayment oPrevCCPayment;
		CreditCardPayment oCCPayment;

		while (itColIPGQueryDTO.hasNext()) {
			ipgQueryDTO = (IPGQueryDTO) itColIPGQueryDTO.next();

			oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgQueryDTO.getPaymentBrokerRefNo());
			query = getPaymentQueryDR();

			try {
				ServiceResponce serviceResponce = query.query(oCreditCardTransaction,
						TapOfflinePaymentUtils.getIPGConfigs(getMerchantId()),
						PaymentBrokerInternalConstants.QUERY_CALLER.SCHEDULER);

				String message = (String) serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE);

				// If successful payment remains at Bank
				if (message.equals(TapOfflinePaymentUtils.SUCCESS)) {

					boolean isResConfirmationSuccess = ConfirmReservationUtil.isReservationConfirmationSuccess(ipgQueryDTO);

					if (!isResConfirmationSuccess) {
						if (refundFlag) {
							// Do refund
							String txnNo = PlatformUtiltiies.nullHandler(serviceResponce
									.getResponseParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE));

							oPrevCCPayment = new PreviousCreditCardPayment();
							oPrevCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
							oPrevCCPayment.setPgSpecificTxnNumber(txnNo);

							oCCPayment = new CreditCardPayment();
							oCCPayment.setPreviousCCPayment(oPrevCCPayment);
							oCCPayment.setAmount(ipgQueryDTO.getAmount().toString());
							oCCPayment.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
							oCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
							oCCPayment.setIpgIdentificationParamsDTO(ipgQueryDTO.getIpgIdentificationParamsDTO());
							oCCPayment.setPnr(oCreditCardTransaction.getTransactionReference());
							oCCPayment.setTemporyPaymentId(oCreditCardTransaction.getTemporyPaymentId());

							ServiceResponce srRev = refund(oCCPayment, oCCPayment.getPnr(), oCCPayment.getAppIndicator(),
									oCCPayment.getTnxMode());

							if (srRev.isSuccess()) {
								// Change State to 'IS'
								oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
								log.info("Status moving to IS:" + oCreditCardTransaction.getTemporyPaymentId());
							} else {
								// Change State to 'IF'
								oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
								log.info("Status moving to IF:" + oCreditCardTransaction.getTemporyPaymentId());
							}
						} else {
							// Change State to 'IP'
							oResultIP.add(oCreditCardTransaction.getTemporyPaymentId());
							log.info("Status moving to IP:" + oCreditCardTransaction.getTemporyPaymentId());
						}
					} else {
						if (log.isDebugEnabled()) {
							log.debug("Status moving to RS:" + oCreditCardTransaction.getTemporyPaymentId());
						}
					}

				} else if (message.equals(TapOfflinePaymentUtils.PENDING)) {
					// Still in pending mode
					log.info("Do nothing as payment can be done from offline");
				}
				// No payment detail at TAP or failed
				else {
					// No Payments exist
					// Update status to 'II'
					oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
					log.info("Status moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
				}
			} catch (ModuleException e) {
				log.error("Scheduler::ResolvePayment FAILED!!! " + e.getMessage(), e);
			}
		}

		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF, oResultIF);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_II, oResultII);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IP, oResultIP);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS, oResultIS);

		sr.setSuccess(true);

		log.info("[PaymentBrokerTapOfflineImpl::resolvePartialPayments()] End");
		return sr;

	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		log.info("[PaymentBrokerTapOfflineImpl::handleDeferredResponse()] Begin");

		if (AppSysParamsUtil.enableServerToServerMessages()
				&& AppSysParamsUtil.isConfirmOnholdReservationByServerToServerMessages()) {

			int temporyPaymentId = ipgResponseDTO.getTemporyPaymentId();
			CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTmpPayID(temporyPaymentId);
			ReservationBD reservationBD = ReservationModuleUtils.getReservationBD();
			boolean isReservationConfirmationSuccess = false;
			String pnr = oCreditCardTransaction.getTransactionReference();
			LCCClientReservation commonReservation = null;
			Reservation reservation = null;
			String status = "";

			/* Save Card Transaction response Details */
			Date requestTime = reservationBD.getPaymentRequestTime(oCreditCardTransaction.getTemporyPaymentId());
			Date now = Calendar.getInstance().getTime();
			String strResponseTime = CalendarUtil.getTimeDifference(requestTime, now);
			long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(requestTime, now);

			oCreditCardTransaction.setComments(strResponseTime);
			oCreditCardTransaction.setResponseTime(timeDiffInMillis);
			oCreditCardTransaction.setTransactionResultCode(1);
			oCreditCardTransaction.setDeferredResponseText(PaymentBrokerUtils.getRequestDataAsString(receiptMap));

			PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

			IPGQueryDTO ipgQueryDTO = reservationBD.getTemporyPaymentInfo(oCreditCardTransaction.getTransactionRefNo());

			if (ipgQueryDTO != null) {
				/* Update with payment response details */

				LCCClientPnrModesDTO modes = new LCCClientPnrModesDTO();
				modes.setPnr(ipgQueryDTO.getPnr());
				modes.setRecordAudit(false);

				ipgQueryDTO.setPaymentType(getPaymentType(getStandardCardType(CARD_TYPE_GENERIC)));

				// for dry interline validation fix
				IPGIdentificationParamsDTO defIPGIdentificationParamsDTO = ipgQueryDTO.getIpgIdentificationParamsDTO();
				if (defIPGIdentificationParamsDTO.getIpgId() == null) {
					defIPGIdentificationParamsDTO.setIpgId(ipgResponseDTO.getPaymentBrokerRefNo());
					ipgQueryDTO.setIpgIdentificationParamsDTO(defIPGIdentificationParamsDTO);
				}

				/** NO RESERVATION OR CANCELED RESERVATION */
				if (!isReservationExist(ipgQueryDTO)
						|| ReservationInternalConstants.ReservationStatus.CANCEL.equals(ipgQueryDTO.getPnrStatus())) {

					/**
					 * When confirming a reservation made from another carrier reservation does not exists in own
					 * reservation table , therefore we need to load the reservation through LCC and clarify reservation
					 * exists or not
					 **/
					String moduleCode = receiptMap.get(TapOfflinePaymentUtils.ORDER_DONE_BY);
					LCCClientPnrModesDTO pnrModesDTO = getPnrModesDTO(pnr, false, true,
							SalesChannelsUtil.isAppEngineWebOrMobile(moduleCode) ? ApplicationEngine.IBE : ApplicationEngine.XBE);
					commonReservation = AirproxyModuleUtils.getAirproxyReservationBD().searchReservationByPNR(pnrModesDTO, null,
							getTrackingInfo(ipgQueryDTO));
					status = commonReservation.getStatus();

					if (status.equals("") || ReservationInternalConstants.ReservationStatus.CANCEL.equals(status)) {

						log.info("[handleDeferredResponse()] Refunding Payment -> No Reservation has been created :"
								+ isReservationConfirmationSuccess);
						ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_REJECTED);
						receiptMap.put("status", TapOfflinePaymentUtils.ERROR);

					} else {
						if (commonReservation != null) {
							ipgQueryDTO.setPnr(commonReservation.getPNR());
							ipgQueryDTO.setGroupPnr(commonReservation.isGroupPNR() ? commonReservation.getPNR() : null);
							ipgQueryDTO.setPnrStatus(commonReservation.getStatus());
						}
					}
				}

				/** ONHOLD RESERVATION */
				if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(ipgQueryDTO.getPnrStatus())) {

					if (reservation == null && commonReservation == null) {
						try {
							reservation = PaymentBrokerModuleUtil.getReservationBD().getReservation(modes, null);
							status = reservation.getStatus();
						} catch (ModuleException e) {
							log.error(" ((o)) CommonsDataAccessException::getReservation " + e.getCause());
						}
					}

					if (!status.equals("") && ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(status)) {

						if (isSuccessFromIPG(oCreditCardTransaction, receiptMap)) {
							isReservationConfirmationSuccess = ConfirmReservationUtil
									.isReservationConfirmationSuccess(ipgQueryDTO);
						}

						if (!isReservationConfirmationSuccess) {
							log.debug("[handleDeferredResponse()] Refunding Payment -> Onhold confirmation failed :"
									+ isReservationConfirmationSuccess);
							ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_REJECTED);
							receiptMap.put("status", PayFortPaymentUtils.ERROR);
							throw new ModuleException("Onhold confirmation failed:" + pnr);
						} else {
							receiptMap.put("status", PayFortPaymentUtils.SUCCESS);
							log.info("Reservation confirmation successful:" + ipgResponseDTO.getPaymentBrokerRefNo());
							ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_ACCEPTED);
						}

					}
				}

				/** ALREADY CONFIRMED RESERVATION */
				if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(ipgQueryDTO.getPnrStatus())) {

					if (reservation == null && commonReservation == null) {
						try {
							reservation = PaymentBrokerModuleUtil.getReservationBD().getReservation(modes, null);
							status = reservation.getStatus();
						} catch (ModuleException e) {
							log.error(" ((o)) CommonsDataAccessException::getReservation " + e.getCause());
						}
					}

					TempPaymentTnx tempPaymentTnx = null;
					tempPaymentTnx = ReservationModuleUtils.getReservationBD().loadTempPayment(temporyPaymentId);

					if (!status.equals("") && ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(status)
							&& "RS".equals(tempPaymentTnx.getStatus())) {
						receiptMap.put("status", TapOfflinePaymentUtils.SUCCESS);
						ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_ACCEPTED);
						log.debug("[handleDeferredResponse()] Confirm Payment -> Reservation already confirmed :" + pnr);

					} else {
						receiptMap.put(TapOfflineResponse.RES_MESSAGE, TapOfflinePaymentUtils.ERROR);
						ipgResponseDTO.setStatus(TapOfflinePaymentUtils.STATUS_REJECTED);
						log.debug("[handleDeferredResponse()] Confirm Payment -> confirm by another payment gateway :" + pnr);
						throw new ModuleException("confirm by another payment gateway :" + pnr);
					}
				}

			} else {
				receiptMap.put(TapOfflineResponse.RES_MESSAGE, TapOfflinePaymentUtils.ERROR);
				ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_REJECTED);
				log.error("PaymentBrokerTapOfflineImpl temp transaction not found:" + ipgResponseDTO.getPaymentBrokerRefNo());
				throw new ModuleException("Duplicate Receipt....");
			}

		} else {
			log.error("error check Response for transation:" + ipgResponseDTO.getPaymentBrokerRefNo());
			ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_REJECTED);
			receiptMap.put(TapOfflineResponse.RES_MESSAGE, TapOfflinePaymentUtils.ERROR);
			throw new ModuleException("Server to Server calls are disabled..");
		}

		log.info("[PaymentBrokerPayFortTemplate::handleDeferredResponse()] End");

	}

	private boolean isSuccessFromIPG(CreditCardTransaction oCreditCardTransaction, Map<String, String> postDataMap)
			throws ModuleException {

		if (!oCreditCardTransaction.getTempReferenceNum().equals(postDataMap.get(TapOfflineRequest.ORDER_NUMBER))) {
			return false;
		} else if (!oCreditCardTransaction.getTransactionId().equals(postDataMap.get(TapOfflineResponse.TAP_REF_ID))) {
			return false;
		} else {
			try {
				PaymentQueryDR oNewQuery = getPaymentQueryDR();
				ServiceResponce serviceResponce = oNewQuery.query(oCreditCardTransaction,
						TapOfflinePaymentUtils.getIPGConfigs(getMerchantId()), QUERY_CALLER.VERIFY);
				if (serviceResponce.isSuccess()) {
					return true;
				}
			} catch (ModuleException e) {
				log.error("Calling query error...");
				return false;
			}

			return false;
		}

	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	public String getDeviceLicenseID() {
		return deviceLicenseID;
	}

	public void setDeviceLicenseID(String deviceLicenseID) {
		this.deviceLicenseID = deviceLicenseID;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getCreateInvoiceUrl() {
		return createInvoiceUrl;
	}

	public void setCreateInvoiceUrl(String createInvoiceUrl) {
		this.createInvoiceUrl = createInvoiceUrl;
	}

	private Map<String, String> createPostDataMap(IPGRequestDTO ipgRequestDTO, String merchantTxnId,
			PaymentConstants.MODE_OF_SERVICE mode) throws ModuleException {

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();

		postDataMap.put(TapOfflineRequest.MERCHANT_ID, getMerchantId());
		postDataMap.put(TapOfflineRequest.DEVICE_LICENSE_ID, getDeviceLicenseID());
		postDataMap.put(TapOfflineRequest.HASH, getHash());
		String mobileNumber = null;

		// remove hiphen from submitting mobile number
		if (ipgRequestDTO.getContactMobileNumber() != null) {
			mobileNumber = ipgRequestDTO.getContactMobileNumber().replace("-", "");
		}

		if (mode.equals(PaymentConstants.MODE_OF_SERVICE.PAYMENT)) {
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			String orderDate = sdf.format(new Date());
			String dueDate = sdf.format(getDueDate(ipgRequestDTO.getInvoiceExpirationTime()));

			if (dueDate == null) {
				throw new ModuleException("Cannot parse date from IBEOFFLINEPAYMENT param..");
			}

			postDataMap.put(TapOfflineRequest.CUSTOMER_ID, ipgRequestDTO.getPnr());
			postDataMap.put(TapOfflineRequest.CUSTOMER_MOBILE, mobileNumber);
			postDataMap.put(TapOfflineRequest.CUSTOMER_EMAIL, ipgRequestDTO.getContactEmail());
			postDataMap.put(TapOfflineRequest.CUSTOMER_NAME, ipgRequestDTO.getContactFirstName());

			postDataMap.put(TapOfflineRequest.CONTACT_NAME, ipgRequestDTO.getContactFirstName());
			postDataMap.put(TapOfflineRequest.CONTACT_PHONE, mobileNumber);

			postDataMap.put(TapOfflineRequest.ORDER_ID, merchantTxnId);
			postDataMap.put(TapOfflineRequest.ORDER_DATE, orderDate);
			postDataMap.put(TapOfflineRequest.ORDER_DUE_DATE, dueDate);
			postDataMap.put(TapOfflineRequest.ORDER_CURRENCY_CODE, ipgRequestDTO.getIpgIdentificationParamsDTO()
					.getPaymentCurrencyCode());
			postDataMap.put(TapOfflineRequest.ORDER_SALES_PERSON, getMerchantName());

			postDataMap.put(TapOfflineRequest.PRODUCT_ID, ipgRequestDTO.getPnr());
			postDataMap.put(TapOfflineRequest.PRODUCT_NAME, getOrderInfo() + " " + ipgRequestDTO.getPnr());
			postDataMap.put(TapOfflineRequest.PRODUCT_DESCRIPTION, getOrderInfo() + " : " + ipgRequestDTO.getPnr());
			postDataMap.put(TapOfflineRequest.PRODUCT_PRICE, ipgRequestDTO.getAmount());
			postDataMap.put(TapOfflineRequest.PRODUCT_QUANTITY, "1");
		} else if (mode.equals(PaymentConstants.MODE_OF_SERVICE.REFUND)) {
			postDataMap.put(TapOfflineRequest.TAP_REFERENCE_ID, merchantTxnId + "");
			postDataMap.put(TapOfflineRequest.ORDER_ID_FOR_STATUS, "1");
			postDataMap.put(TapOfflineRequest.IBAN, null);
			postDataMap.put(TapOfflineRequest.ACCOUNT_NAME, null);
			postDataMap.put(TapOfflineRequest.AMOUNT, ipgRequestDTO.getAmount());
		}

		return postDataMap;
	}

	private Date getDueDate(Date expireTime) {

		if (expireTime != null) {
			return expireTime;
		} else {
			String timeToPay = AppSysParamsUtil.getOnHoldDuration(OnHold.IBEOFFLINEPAYMENT);

			if (timeToPay != null && !timeToPay.equals("")) {
				String time[] = timeToPay.split(":");
				if (time.length == 2) {
					Calendar cal = Calendar.getInstance();
					cal.setTime(new Date());
					cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
					cal.add(Calendar.MINUTE, Integer.parseInt(time[1]));
					return cal.getTime();
				}
			}
		}

		return null;
	}

	protected boolean isReservationExist(IPGQueryDTO ipgQueryDTO) {
		boolean isExist = false;
		if (ipgQueryDTO != null) {
			if (ipgQueryDTO.getPnr() != null && ipgQueryDTO.getPnr().trim().length() != 0) {
				isExist = true;
			}
		}
		return isExist;
	}

	protected int getStandardCardType(String card) {
		// [1-MASTER, 2-VISA, 3-AMEX, 4-DINNERS, 5-GENERIC, 6-OFFLINE]
		int mapType = 5; // default card type
		if (card != null && !card.equals("") && mapCardType.get(card) != null) {
			String mapTypeStr = (String) mapCardType.get(card);
			mapType = Integer.parseInt(mapTypeStr);
		}
		return mapType;
	}

	protected PaymentType getPaymentType(int cardID) {

		PaymentType paymentType = PaymentType.CARD_GENERIC;
		switch (cardID) {
		case 1:
			paymentType = PaymentType.CARD_MASTER;
			break;
		case 2:
			paymentType = PaymentType.CARD_VISA;
			break;
		case 3:
			paymentType = PaymentType.CARD_AMEX;
			break;
		case 4:
			paymentType = PaymentType.CARD_DINERS;
			break;
		case 5:
			paymentType = PaymentType.CARD_GENERIC;
			break;
		case 6:
			paymentType = PaymentType.CARD_CMI;
			break;
		}
		return paymentType;
	}

	private static LCCClientPnrModesDTO getPnrModesDTO(String pnr, boolean isGroupPNR, boolean recordAudit,
			ApplicationEngine appIndicator) {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();

		boolean loadLocalTimes = false;
		if (isGroupPNR) {
			pnrModesDTO.setGroupPNR(pnr);
			loadLocalTimes = true;
		} else {
			pnrModesDTO.setPnr(pnr);
			loadLocalTimes = false;
		}

		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadLastUserNote(false);
		pnrModesDTO.setLoadLocalTimes(loadLocalTimes);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setRecordAudit(recordAudit);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true);
		pnrModesDTO.setAppIndicator(appIndicator);
		pnrModesDTO.setLoadExternalPaxPayments(true);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
		pnrModesDTO.setLoadEtickets(true);

		return pnrModesDTO;
	}

	private static TrackInfoDTO getTrackingInfo(IPGQueryDTO ipgQueryDTO) {
		TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
		trackInfoDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		trackInfoDTO.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
		trackInfoDTO.setCallingInstanceId(SystemPropertyUtil.getInstanceId());

		return trackInfoDTO;
	}

	private ServiceResponce cancelInvoice(CreditCardPayment creditCardPayment) throws ModuleException {
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		TapOfflineResponse response = null;
		Map<String, String> cancelRequestMap = new HashMap<String, String>();
		cancelRequestMap.put(TapOfflineResponse.TAP_REF_ID, ccTransaction.getAidCccompnay());
		cancelRequestMap.put(TapOfflineRequest.ORDER_ID_FOR_STATUS, ccTransaction.getTempReferenceNum());
		cancelRequestMap.put(TapOfflineRequest.MERCHANT_ID, getMerchantId());
		cancelRequestMap.put(TapOfflineRequest.DEVICE_LICENSE_ID, getDeviceLicenseID());
		cancelRequestMap.put(TapOfflineRequest.HASH, getHash());

		response = TapOfflinePaymentUtils.getCancelInvoiceResponse(cancelRequestMap, getCancelInvoiceUrl(), isUseProxy(),
				getProxyHost(), getProxyPort());
		if (response != null
				&& response.getResMsg() != null
				&& (response.getResMsg().equals(TapOfflinePaymentUtils.SUCCESS) || response.getResMsg().equals(
						TapOfflinePaymentUtils.NOT_AVAILABLE)) || response.getResMsg().equals(TapOfflinePaymentUtils.PAID)) {
			sr.setSuccess(true);
			boolean tempPaymentCancelled = PaymentBrokerUtils.getPaymentBrokerDAO().updateTempPaymentEntryForCancellingInvoice(
					creditCardPayment.getTemporyPaymentId());
			if (tempPaymentCancelled) {

				Map<String, String> cancelResponseMap = new HashMap<String, String>();
				cancelResponseMap.put(response.TAP_REF_ID, response.getTapRefID());
				cancelResponseMap.put(response.RES_CODE, response.getResCode());
				cancelResponseMap.put(response.RES_MESSAGE, response.getResMsg());

				String updatedMerchTnxRef = ccTransaction.getTransactionRefNo() + "" + PaymentConstants.MODE_OF_SERVICE.CANCEL;
				String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(cancelRequestMap);
				String strResponseParams = PaymentBrokerUtils.getRequestDataAsString(cancelResponseMap);
				CreditCardTransaction ccNewTransaction = auditTransaction(creditCardPayment.getPnr(), getMerchantId(),
						updatedMerchTnxRef, creditCardPayment.getTemporyPaymentId(), bundle.getString("SERVICETYPE_REVERSAL"),
						strRequestParams, strResponseParams, getPaymentGatewayName(), ccTransaction.getTransactionId(), true);
				log.error("Successfully Cancelled........New Transaction Ref No : " + ccNewTransaction.getTransactionRefNo());
			} else {
				log.error("Recording payment failure error for TPT_ID : " + creditCardPayment.getTemporyPaymentId());
			}

		} else {
			log.error("Cancelling Invoice error..." + response.getResMsg());
		}
		return sr;
	}

	public String getCancelInvoiceUrl() {
		return cancelInvoiceUrl;
	}

	public void setCancelInvoiceUrl(String cancelInvoiceUrl) {
		this.cancelInvoiceUrl = cancelInvoiceUrl;
	}

	@Override
	public boolean isOfflinePaymentsCancellationAllowed() {
		return true;
	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
}
