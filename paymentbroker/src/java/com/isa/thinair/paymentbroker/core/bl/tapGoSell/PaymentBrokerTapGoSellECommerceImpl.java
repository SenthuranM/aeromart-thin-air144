package com.isa.thinair.paymentbroker.core.bl.tapGoSell;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.utils.ConfirmReservationUtil;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.tap.TapCommonResponse;
import com.isa.thinair.paymentbroker.api.dto.tap.TapGetOrderStatusRequestCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapGetOrderStatusRequestCallRs;
import com.isa.thinair.paymentbroker.api.dto.tap.TapGoSellRedirectResponse;
import com.isa.thinair.paymentbroker.api.dto.tap.TapPaymentRequestCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapPaymentRequestCallRs;
import com.isa.thinair.paymentbroker.api.dto.tap.TapRefundCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapRefundCallRs;
import com.isa.thinair.paymentbroker.api.dto.tap.TapRefundStatusCallRq;
import com.isa.thinair.paymentbroker.api.dto.tap.TapRefundStatusCallRs;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.wsclient.api.service.WSClientBD;

public class PaymentBrokerTapGoSellECommerceImpl extends PaymentBrokerTapTemplate {

	private static Log log = LogFactory.getLog(PaymentBrokerTapGoSellECommerceImpl.class);

	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	private static WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();

	protected final static String SUCCESS = "SUCCESS";
	protected final static String PENDING = "PENDING";
	protected final static String REFUNDED = "REFUNDED";
	protected final static String INQURE_SUCCESS = "0";
	protected final static String TAP_RESPONSE_SUCCESS = "0";
	protected final static String TAP_REFUND_SUCCESS = "1";
	protected final static String CAPTURED = "CAPTURED";
	protected static final String CARD_TYPE_GENERIC = "GC";

	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		log.debug("[PaymentBrokerTapGoSellECommerceImpl::getRequestData()] Begin ");

		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		List<String> pgwList = new ArrayList<String>();
		for (String pgwOption : getMapPGWOptions().values()) {
			pgwList.add(pgwOption);
		}

		String finalAmount = ipgRequestDTO.getAmount();

		TapPaymentRequestCallRq pgwRequest = new TapPaymentRequestCallRq();
		pgwRequest.setCurrencyCode(ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode());
		pgwRequest.setEmail(ipgRequestDTO.getContactEmail());
		pgwRequest.setGatewayList(pgwList);
		pgwRequest.setMerchantID(Long.parseLong(getMerchantId()));
		pgwRequest.setUserName(getUser());
		if(ipgRequestDTO.getContactMobileNumber() != null){
			pgwRequest.setMobileNo(ipgRequestDTO.getContactMobileNumber().replace("-", "").replace("+", ""));
		}else {
			pgwRequest.setMobileNo(ipgRequestDTO.getContactPhoneNumber().replaceAll("-", "").replace("+", ""));
		}
		pgwRequest.setName(ipgRequestDTO.getHolderName() != null ? ipgRequestDTO.getHolderName() : ipgRequestDTO
				.getContactFirstName());
		pgwRequest.setQuantity(1);
		pgwRequest.setReferenceID(merchantTxnId);
		pgwRequest.setReturnURL(ipgRequestDTO.getReturnUrl());
		pgwRequest.setTotalPrice(new BigDecimal(finalAmount));
		pgwRequest.setUnitName(ipgRequestDTO.getPnr() == null ? getProductDetails() : getProductDetails() + ":"
				+ ipgRequestDTO.getPnr());
		pgwRequest.setUnitPrice(new BigDecimal(finalAmount));
		pgwRequest.setHashString(TapPaymentUtils.getHashStringForPayRq(getMerchantId(), getUser(), merchantTxnId,
				pgwRequest.getMobileNo(), pgwRequest.getCurrencyCode(), finalAmount, getApiKey()));

		TapPaymentRequestCallRs pgwResponse = ebiWebervices.paymentRequest(pgwRequest);

		// handle response from PGW
		if (TAP_RESPONSE_SUCCESS.equals(pgwResponse.getResponseCode()) && pgwResponse.getReferenceID() != null) {
			postDataMap.put(TapPaymentUtils.PAYMENT_REFERENCE, pgwResponse.getReferenceID());
			postDataMap.put(TapPaymentUtils.PAYMENT_URL, pgwResponse.getPaymentURL());
			postDataMap.put(TapPaymentUtils.TAP_PAYMENT_URL, pgwResponse.getTapPaymentURL());
			postDataMap.put(TapPaymentUtils.REDIRECTION_MECHANISM, PaymentConstants.MODE_OF_REDIRECTION.FORM_SUBMIT.getValue());			
			String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();
			String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);
			String requestDataForm = PaymentBrokerUtils.getPostInputDataFormHTML(new HashMap<String, String>(),
					pgwResponse.getPaymentURL());

			CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
					new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
					strRequestParams + "," + sessionID, "", getPaymentGatewayName(), pgwResponse.getReferenceID(), false);

			if (log.isDebugEnabled()) {
				log.debug("TapGoSell PG Request Data : " + strRequestParams + ", sessionID : " + sessionID);
			}

			ipgRequestResultsDTO.setRequestData(requestDataForm);
			ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
			ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);
			ipgRequestResultsDTO.setPostDataMap(postDataMap);

			log.debug("[PaymentBrokerTapGoSellECommerceImpl::getRequestData()] End ");
			return ipgRequestResultsDTO;

		} else {
			throw new ModuleException("paymentbroker.card.invalid"); // TODO handle properly
		}
	}

	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {

		log.debug("[PaymentBrokerTapGoSellECommerceImpl::getReponseData()] Begin ");

		int tnxResultCode;
		String errorCode = "";
		String status = "";
		String errorSpecification = "";
		String merchantTxnReference = "";
		String accelAeroTxnReference = "";
		String applicationTransactionId = "";

		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());

		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time
		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);

		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);
		applicationTransactionId = oCreditCardTransaction.getTempReferenceNum();
		TapGoSellRedirectResponse tapGoSellResp = new TapGoSellRedirectResponse(receiptyMap);

		if (log.isDebugEnabled()) {
			log.debug("[PaymentBrokerTapGoSellECommerceImpl::getReponseData()] Response ");
		}
		merchantTxnReference = tapGoSellResp.getRef();
		accelAeroTxnReference = tapGoSellResp.getPayid();

		if (SUCCESS.equals(tapGoSellResp.getResult())
				&& tapGoSellResp.getHash() != null
				&& tapGoSellResp.getHash().equals(
						TapPaymentUtils.getHashStringForPayRs(getMerchantId(), merchantTxnReference, tapGoSellResp.getResult(),
								oCreditCardTransaction.getTempReferenceNum(), getApiKey()))) {
			DefaultServiceResponse inquireRq = new DefaultServiceResponse();
			inquireRq = (DefaultServiceResponse) inqurePaymentStatus(tapGoSellResp.getRef(), oCreditCardTransaction);

			if (inquireRq.isSuccess()) {
				status = IPGResponseDTO.STATUS_ACCEPTED;
				tnxResultCode = 1;
				errorSpecification = "";
			} else {
				status = IPGResponseDTO.STATUS_REJECTED;
				tnxResultCode = 0;
				errorCode = inquireRq.getResponseCode();
				errorSpecification = TapPaymentUtils.getErrorDescription(inquireRq.getResponseCode());
				log.error("[PaymentBrokerTapGoSellECommerceImpl::getReponseData()] - Payment Inqury shows pament Falied");
			}
		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
			errorCode = tapGoSellResp.getResult();
			errorSpecification = TapPaymentUtils.getErrorDescription(tapGoSellResp.getResult());
			log.error("[PaymentBrokerTapGoSellECommerceImpl::getReponseData()] - payment response Failed");
		}

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setApplicationTransactionId(applicationTransactionId);
		ipgResponseDTO.setAuthorizationCode(merchantTxnReference);
		ipgResponseDTO.setCardType(getStandardCardType(CARD_TYPE_GENERIC));
		updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), tapGoSellResp.toString(), errorSpecification,
				accelAeroTxnReference, merchantTxnReference, tnxResultCode);

		if (log.isDebugEnabled()) {
			log.debug("[PaymentBrokerTapGoSellECommerceImpl::getReponseData()] - Staus-" + status + " Error Code-" + errorCode
					+ " Error Spec-" + errorSpecification);
		}

		log.debug("[PaymentBrokerTapGoSellECommerceImpl::getReponseData()] End");

		return ipgResponseDTO;
	}

	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		log.debug("[PaymentBrokerTapGoSellECommerceImpl::refund()] Begin ");
		String transactionMsg;
		String merchTnxRef;
		String tapTnxRef;
		String errorCode = "";
		int tnxResultCode = 0;
		String errorSpecification;

		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		if (creditCardPayment.getPreviousCCPayment() != null && getRefundEnabled() != null && getRefundEnabled().equals("true")) {
			if (log.isDebugEnabled()) {
				log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
				log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
				log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
				log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
				log.debug("CC Old pay Formatted sAmount : " + creditCardPayment.getAmount());
				log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());
			}

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}

			merchTnxRef = ccTransaction.getTempReferenceNum();
			tapTnxRef = ccTransaction.getAidCccompnay();

			DefaultServiceResponse inquireRq = (DefaultServiceResponse) inqurePaymentStatus(ccTransaction.getTransactionId(), ccTransaction);

			if (inquireRq.isSuccess()) {

				String updatedMerchTnxRef = merchTnxRef + PaymentConstants.MODE_OF_SERVICE.REFUND;

				CreditCardTransaction ccNewTransaction = auditTransaction(ccTransaction.getTransactionReference(),
						getMerchantId(), updatedMerchTnxRef, creditCardPayment.getTemporyPaymentId(),
						bundle.getString("SERVICETYPE_REFUND"), TapPaymentUtils.generateRefundRequestTxt(tapTnxRef, merchTnxRef,
								creditCardPayment.getAmount(), TapPaymentUtils.DEFAULT_CURRENCY, getUser(), getMerchantId()), "",
						getPaymentGatewayName(), null, false);

				DefaultServiceResponse refundRs = (DefaultServiceResponse) refundPayment(new BigDecimal(creditCardPayment.getAmount()), merchTnxRef,
						ccTransaction.getTransactionId());

				if (refundRs.isSuccess()) {
					tnxResultCode = 1;
					errorSpecification = "Successfully refund to user";
					sr.setSuccess(true);
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
							PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
					log.info(" REFUND Succes.");
				} else {
					errorCode = refundRs.getResponseCode();
					tnxResultCode = 0;
					errorSpecification = TapPaymentUtils.getErrorDescription(refundRs.getResponseCode());
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, errorSpecification);
					log.info(" REFUND Status Check Failed ");
				}
				
				String responseText = getResponseText(refundRs);
				
				updateAuditTransactionByTnxRefNo(ccNewTransaction.getTransactionRefNo(), responseText, errorSpecification, ccTransaction.getAidCccompnay(),
						ccTransaction.getTransactionId(), tnxResultCode);

			} else {
				errorCode = inquireRq.getResponseCode();
				transactionMsg = "No Payment Exists To Refund";
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
				log.info(" REFUND Fail, Not call REFUND_EXC  ");
			}
			sr.setResponseCode(String.valueOf(ccTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, ccTransaction.getAidCccompnay());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);

		} else {
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);

		}

		log.debug("[PaymentBrokerTapGoSellECommerceImpl::refund()] End");
		return sr;
	}

	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		log.debug("[PaymentBrokerTapGoSellECommerceImpl::reverse()] Begin ");
		String transactionMsg;
		String merchTnxRef;
		String tapTnxRef;
		int tnxResultCode = 0;
		String errorCode = "";
		String errorSpecification;

		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		if (getRefundEnabled() != null && getRefundEnabled().equals("true")
				&& checkPaymentGatewayCompatibility(ccTransaction).isSuccess()) {
			if (log.isDebugEnabled()) {
				log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
				log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
				log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
				log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
				log.debug("CC Old pay Formatted sAmount : " + creditCardPayment.getAmount());
				log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());
			}

			merchTnxRef = ccTransaction.getTempReferenceNum();
			tapTnxRef = ccTransaction.getAidCccompnay();

			DefaultServiceResponse inquireRq = (DefaultServiceResponse) inqurePaymentStatus(ccTransaction.getTransactionId(), ccTransaction);

			if (inquireRq.isSuccess()) {

				String updatedMerchTnxRef = merchTnxRef + PaymentConstants.MODE_OF_SERVICE.VOID;

				CreditCardTransaction ccNewTransaction = auditTransaction(ccTransaction.getTransactionReference(),
						getMerchantId(), updatedMerchTnxRef, ccTransaction.getTemporyPaymentId(),
						bundle.getString("SERVICETYPE_REVERSAL"), TapPaymentUtils.generateRefundRequestTxt(tapTnxRef,
								merchTnxRef, creditCardPayment.getAmount(), TapPaymentUtils.DEFAULT_CURRENCY, getUser(),
								getMerchantId()), "", getPaymentGatewayName(), null, false);

				DefaultServiceResponse refundRs = (DefaultServiceResponse) refundPayment(new BigDecimal(creditCardPayment.getAmount()), merchTnxRef,
						ccTransaction.getTransactionId());

				if (refundRs.isSuccess()) {
					tnxResultCode = 1;
					errorSpecification = "Successfully reverse to user";
					log.info(" REVERSAL Succes.");
					sr.setSuccess(true);
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
							PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
				} else {
					errorCode = refundRs.getResponseCode();
					tnxResultCode = 0;
					errorSpecification = TapPaymentUtils.getErrorDescription(refundRs.getResponseCode());
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, errorSpecification);
					log.info(" REVERSAL Status Check Failed ");
				}
				updateAuditTransactionByTnxRefNo(ccNewTransaction.getTransactionRefNo(), getResponseText(refundRs), errorSpecification, ccTransaction.getAidCccompnay(),
						ccTransaction.getTransactionId(), tnxResultCode);

			} else {
				errorCode = inquireRq.getResponseCode();
				transactionMsg = "No Payment Exists To Refund";
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
				log.info(" REVERSAL Fail, Not call REFUND_EXC  ");
			}
			sr.setResponseCode(String.valueOf(ccTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, ccTransaction.getAidCccompnay());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
		} else {
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);

		}

		log.debug("[PaymentBrokerTapGoSellECommerceImpl::reverse()] End");
		return sr;

	}

	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {

		log.info("[PaymentBrokerTapGoSellECommerceImpl::resolvePartialPayments()] Begin");

		Iterator itColIPGQueryDTO = colIPGQueryDTO.iterator();

		Collection<Integer> oResultIF = new ArrayList<Integer>();
		Collection<Integer> oResultIP = new ArrayList<Integer>();
		Collection<Integer> oResultII = new ArrayList<Integer>();
		Collection<Integer> oResultIS = new ArrayList<Integer>();

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		IPGQueryDTO ipgQueryDTO;
		CreditCardTransaction oCreditCardTransaction;
		Date currentTime = Calendar.getInstance().getTime();
		Calendar queryValidTime = Calendar.getInstance();
		int srtmRefRegResponse = 0;

		while (itColIPGQueryDTO.hasNext()) {
			ipgQueryDTO = (IPGQueryDTO) itColIPGQueryDTO.next();

			oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgQueryDTO.getPaymentBrokerRefNo());

			try {

				queryValidTime.setTime(ipgQueryDTO.getTimestamp());
				queryValidTime.add(Calendar.MINUTE, AppSysParamsUtil.getTimeForPaymentStatusUpdate());

				if (oCreditCardTransaction.getTransactionId() != null) {

					DefaultServiceResponse inquireRq = (DefaultServiceResponse) inqurePaymentStatus(oCreditCardTransaction.getTransactionId(),
							oCreditCardTransaction);

					if (inquireRq.isSuccess()) {
						boolean isResConfirmationSuccess = ConfirmReservationUtil.isReservationConfirmationSuccess(ipgQueryDTO);

						if (isResConfirmationSuccess) {
							if (log.isDebugEnabled()) {
								log.debug("Status moving to RS:" + oCreditCardTransaction.getTemporyPaymentId());
							}

						} else {
							if (refundFlag) {
								inquireRq = (DefaultServiceResponse) inqurePaymentStatus(
										oCreditCardTransaction.getTransactionId(), oCreditCardTransaction);
								// if reservation confirmation doesn't refund
								if (inquireRq.isSuccess()) {
									srtmRefRegResponse = partialPaymentRefund(oCreditCardTransaction, ipgQueryDTO);
								}

								if (srtmRefRegResponse == 1) {
									// Change State to 'IS'
									oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
									if (log.isDebugEnabled()) {
										log.debug("Status moving to IS:" + oCreditCardTransaction.getTemporyPaymentId());
									}
								} else {
									// Change State to 'IF'
									oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
									if (log.isDebugEnabled()) {
										log.debug("Status moving to IF:" + oCreditCardTransaction.getTemporyPaymentId());
									}
								}
							} else {
								// Change State to 'IP'
								oResultIP.add(oCreditCardTransaction.getTemporyPaymentId());
								if (log.isDebugEnabled()) {
									log.debug("Status moving to IP:" + oCreditCardTransaction.getTemporyPaymentId());
								}
							}
						}
					} else {
						// No Payments exist
						// Update status to 'II'
						if (currentTime.getTime() > queryValidTime.getTimeInMillis()) {
							oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
							log.info("Status moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
						} else {
							// Can not move payment status. with in status change for 3D secure payments
							log.info("Status not moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
						}
					}
				}
				// No payment detail at bank
				else {
					// No Payments exist
					// Update status to 'II'
					if (currentTime.getTime() > queryValidTime.getTimeInMillis()) {
						oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
						log.info("Status moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
					} else {
						// Can not move payment status. with in status change for 3D secure payments
						log.info("Status not moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
					}
				}
			} catch (ModuleException e) {
				log.error("Scheduler::ResolvePayment FAILED!!! " + e.getMessage(), e);
			}
		}

		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF, oResultIF);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_II, oResultII);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IP, oResultIP);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS, oResultIS);

		sr.setSuccess(true);

		log.info("[PaymentBrokerTapGoSellECommerceImpl::resolvePartialPayments()] End");
		return sr;

	}
	
	private String getResponseText(ServiceResponce serviceResponse) {
	   return 	(String) serviceResponse.getResponseParam(PaymentConstants.PARAM_PAYMENT_RESPONSE);
	}

	private int partialPaymentRefund(CreditCardTransaction oCreditCardTransaction, IPGQueryDTO ipgQueryDTO)
			throws ModuleException {

		int tnxResultCode = 0;
		String errorSpecification;
		String updatedResponseStr;

		String updatedMerchTnxRef = oCreditCardTransaction.getTempReferenceNum() + PaymentConstants.MODE_OF_SERVICE.REFUND;

		CreditCardTransaction ccNewTransaction = auditTransaction(oCreditCardTransaction.getTransactionReference(),
				getMerchantId(), updatedMerchTnxRef, oCreditCardTransaction.getTemporyPaymentId(),
				bundle.getString("SERVICETYPE_REFUND"), TapPaymentUtils.generateRefundRequestTxt(oCreditCardTransaction
						.getAidCccompnay(), oCreditCardTransaction.getTempReferenceNum(), ipgQueryDTO.getAmount().toString(),
						TapPaymentUtils.DEFAULT_CURRENCY, getUser(), getMerchantId()), "", getPaymentGatewayName(), null, false);

		ServiceResponce refundRs = 	refundPayment(ipgQueryDTO.getAmount(), oCreditCardTransaction.getTempReferenceNum(),
				oCreditCardTransaction.getTransactionId());	

		if (refundRs.isSuccess()) {
			tnxResultCode = 1;
			errorSpecification = "Successfully refund to user";
			log.info(" REFUND Succes.");
		} else {
			tnxResultCode = 0;
			errorSpecification = TapPaymentUtils.getErrorDescription(refundRs.getResponseCode());
		}

		updatedResponseStr = getResponseText(refundRs);
		
		updateAuditTransactionByTnxRefNo(ccNewTransaction.getTransactionRefNo(), updatedResponseStr, errorSpecification,
				oCreditCardTransaction.getAidCccompnay(), oCreditCardTransaction.getTransactionId(), tnxResultCode);

		return tnxResultCode;
	}
	
	private StringBuilder addResponseData(StringBuilder reponseTextBuilder, TapCommonResponse tapCommonResponse) {
		
		reponseTextBuilder.append(tapCommonResponse.getClass().getSimpleName()).append(",");
		reponseTextBuilder.append(tapCommonResponse.getResponseCode()).append(",");
		reponseTextBuilder.append(tapCommonResponse.getResponseMessage()).append(",  ");		
		
		return reponseTextBuilder;
	}

	private ServiceResponce refundPayment(BigDecimal amount, String merRefNo, String tapRefId) {

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		TapRefundCallRq refundRq = new TapRefundCallRq();
		refundRq.setAmount(amount);
		refundRq.setCurrencyCode(TapPaymentUtils.DEFAULT_CURRENCY);// TODO change accordingly
		refundRq.setMerchantID(new Long(getMerchantId()));
		refundRq.setOrderID(merRefNo);
		refundRq.setReferenceID(tapRefId);
		refundRq.setUserName(getUser());
		StringBuilder reponseTextBuilder = new StringBuilder();

		try {
			TapRefundCallRs refundRs = ebiWebervices.refund(refundRq);
			addResponseData(reponseTextBuilder,refundRs);
			
			if (SUCCESS.equals(refundRs.getResponseMessage())) {

				TapRefundStatusCallRq refundStatusRq = new TapRefundStatusCallRq();
				refundStatusRq.setMerchantID(new Long(getMerchantId()));
				refundStatusRq.setReferenceID(tapRefId);
				refundStatusRq.setUserName(getUser());

				TapRefundStatusCallRs refundStatusRs = ebiWebervices.getRefundStatus(refundStatusRq);
				addResponseData(reponseTextBuilder,refundStatusRs);
				
				if (SUCCESS.equals(refundStatusRs.getResponseMessage()) || PENDING.equals(refundStatusRs.getResponseMessage())) {
					sr.setSuccess(true);
					sr.setResponseCode(refundStatusRs.getResponseMessage());
					log.info(" REFUND Succes." );

				} else {
					sr.setResponseCode(refundStatusRs.getResponseMessage());
					sr.setSuccess(false);
					log.info(" Tap Refund Status Check Not Success");
				}
			} else {
				sr.setResponseCode(refundRs.getResponseMessage());
				sr.setSuccess(false);
				log.info(" Tap Refund Not Success ");
			}

		} catch (ModuleException e) {
			sr.setResponseCode("WSFAILED");
			sr.setSuccess(false);
			log.info(" Tap Refund Status Check Not Success" , e);
		}
		
		sr.addResponceParam(PaymentConstants.PARAM_PAYMENT_RESPONSE, reponseTextBuilder.toString());
		
		return sr;
	}

	private ServiceResponce inqurePaymentStatus(String tapRefId, CreditCardTransaction oCreditCardTransaction) {

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		TapGetOrderStatusRequestCallRq inquireRq = new TapGetOrderStatusRequestCallRq();
		inquireRq.setMerchantID(new Long(getMerchantId()));
		inquireRq.setReferenceID(tapRefId);
		inquireRq.setUserName(getUser());
		String strRequestParams = inquireRq.toString();
		
		try {
			CreditCardTransaction ccTransaction = auditTransaction(oCreditCardTransaction.getTransactionReference(),
					getMerchantId(), oCreditCardTransaction.getTempReferenceNum(),
					new Integer(oCreditCardTransaction.getTemporyPaymentId()), bundle.getString("SERVICETYPE_QUERY"),
					strRequestParams, "", getPaymentGatewayName(), oCreditCardTransaction.getTransactionId(), false);

			TapGetOrderStatusRequestCallRs inquireRs = ebiWebervices.getOrderStatus(inquireRq);
			String errorSpecification = "";
			int tnxResultCode = 0;
			if (INQURE_SUCCESS.equals(inquireRs.getResponseCode()) && CAPTURED.equals(inquireRs.getResponseMessage())) {
				sr.setSuccess(true);
				sr.setResponseCode(CAPTURED);
				errorSpecification = "Status : " + TapPaymentUtils.getErrorDescription(inquireRs.getResponseCode());
				tnxResultCode = 1;

			} else {
				sr.setResponseCode(inquireRs.getResponseMessage());
				errorSpecification = "Status : " + TapPaymentUtils.getErrorDescription(inquireRs.getResponseCode())
						+ "Error Message : " + inquireRs.getResponseMessage() + "Error Code : " + inquireRs.getResponseCode();
				tnxResultCode = 0;
			}

			updateAuditTransactionByTnxRefNo(ccTransaction.getTransactionRefNo(), inquireRs.toString(), errorSpecification,
					oCreditCardTransaction.getAidCccompnay(), ccTransaction.getTransactionId(), tnxResultCode);
		} catch (ModuleException e) {
			log.error("Tap Payment Web Service Call Failed ");
			sr.setResponseCode("WSFAILED");
		}

		return sr;
	}

	private int getStandardCardType(String card) {
		// [1-MASTER, 2-VISA, 3-AMEX, 4-DINNERS, 5-GENERIC]
		int mapType = 5; // default card type
		if (card != null && !card.equals("") && mapCardType.get(card) != null) {
			String mapTypeStr = (String) mapCardType.get(card);
			mapType = Integer.parseInt(mapTypeStr);
		}
		return mapType;
	}

}
