/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.paymentbroker.core.bl.hsbc;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Asiri Liyanage
 */
public class PaymentBrokerHSBCImpl extends PaymentBrokerTemplate implements PaymentBroker {

	private static Log log = LogFactory.getLog(PaymentBrokerHSBCImpl.class);

	private static final String DEFAULT_TRANSACTION_CODE = "-1";

	private static final String PARAM_ENCRYPTED_DIGITAL_RECIEPT = "DR";

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	// TODO TEST
	// private IPGIdentificationParamsDTO ipgIdentificationParamsDTO = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.isa.thinair.paymentbroker.core.bl.PaymentBroker#charge(com.isa.thinair.paymentbroker.api.model.CreditCardPayment
	 * , java.lang.String, com.isa.thinair.paymentbroker.api.util.AppIndicatorEnum,
	 * com.isa.thinair.paymentbroker.api.util.TnxModeEnum)
	 */
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.isa.thinair.paymentbroker.core.bl.PaymentBroker#getPostHtml(com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO
	 * )
	 */
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		log.debug("getRequestData...............");

		String digitalOrder = null;
		String merchantTxnId;
		HsbcIPGRequest hsbcIPGRequest;
		HsbcIPGUtil hsbcIPGUtil;

		merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		hsbcIPGRequest = new HsbcIPGRequest();
		hsbcIPGRequest.setIpgRequestDTO(ipgRequestDTO);
		hsbcIPGRequest.setMerchantTxnRef(merchantTxnId);
		hsbcIPGRequest.setPaymentClientPath(paymentClientPath);
		hsbcIPGRequest.setMerchantId(merchantId);
		hsbcIPGRequest.setLocale(locale);

		hsbcIPGUtil = new HsbcIPGUtil();
		digitalOrder = hsbcIPGUtil.getEncryptedRequest(hsbcIPGRequest);

		String serviceType = bundle.getString("SERVICETYPE_AUTHORIZE");
		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), merchantId, merchantTxnId,
				ipgRequestDTO.getApplicationTransactionId(), serviceType, digitalOrder, null, getPaymentGatewayName(), null,
				false);

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(digitalOrder);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());

		return ipgRequestResultsDTO;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isa.thinair.paymentbroker.core.bl.PaymentBroker#getReponseData(java.util.Map, int)
	 */
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO2) throws ModuleException {
		log.debug("getReponseData...............");

		IPGResponseDTO ipgResponseDTO = null;
		String encryptedDR = null;

		String status = IPGResponseDTO.STATUS_REJECTED;
		String errorCode = DEFAULT_TRANSACTION_CODE;
		String message = "";
		String authorizationCode = "";
		String errorSpecification = "";
		int txnResultCode = 0;

		String merchTxnRef = null;
		String ipgTxnId = null;
		int intCardType = 5;
		String paidAmount = null;
		HsbcIPGUtil hsbcIPGUtil;
		HsbcIPGResponse hsbcIPGResponse = null;

		encryptedDR = (String) receiptyMap.get(PARAM_ENCRYPTED_DIGITAL_RECIEPT);

		hsbcIPGUtil = new HsbcIPGUtil();
		hsbcIPGResponse = hsbcIPGUtil.getDecryptedResponse(encryptedDR, paymentClientPath);

		intCardType = getCardType(hsbcIPGResponse.getCardType());
		status = hsbcIPGResponse.getStatus();
		errorCode = hsbcIPGResponse.getErrorCode();
		message = hsbcIPGResponse.getMessage();
		ipgTxnId = hsbcIPGResponse.getIpgTxnId();
		merchTxnRef = hsbcIPGResponse.getMerchTxnRef();
		authorizationCode = hsbcIPGResponse.getAuthorizeID();
		errorSpecification = hsbcIPGResponse.getErrorSpecification();
		txnResultCode = hsbcIPGResponse.getTxnResultCode();

		ipgResponseDTO = new IPGResponseDTO();
		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setMessage(message);
		ipgResponseDTO.setApplicationTransactionId(merchTxnRef);
		ipgResponseDTO.setAuthorizationCode(authorizationCode);
		ipgResponseDTO.setCardType(intCardType);
		updateAuditTransactionByTnxRefNo(ipgResponseDTO2.getPaymentBrokerRefNo(), encryptedDR, errorSpecification,
				authorizationCode, ipgTxnId, txnResultCode);

		return ipgResponseDTO;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.isa.thinair.paymentbroker.core.bl.PaymentBroker#refund(com.isa.thinair.paymentbroker.api.model.CreditCardPayment
	 * , java.lang.String, com.isa.thinair.paymentbroker.api.util.AppIndicatorEnum,
	 * com.isa.thinair.paymentbroker.api.util.TnxModeEnum)
	 */
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		if (creditCardPayment.getPreviousCCPayment() != null) {
			CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
					creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return serviceResponse;
			}
		}

		String amount = PlatformUtiltiies.nullHandler(creditCardPayment.getAmount());
		String merchantTnxId = composeMerchantTransactionId(creditCardPayment.getAppIndicator(),
				creditCardPayment.getTemporyPaymentId(), PaymentConstants.MODE_OF_SERVICE.REFUND);

		CreditCardTransaction creditCardTransaction = auditTransaction(creditCardPayment.getPnr(), merchantId, merchantTnxId,
				creditCardPayment.getTemporyPaymentId(), bundle.getString("SERVICETYPE_MANUAL_REFUND"),
				"Manual Refund requested for " + amount, "Manual Refund responded for " + amount, getPaymentGatewayName(), null,
				true);

		DefaultServiceResponse sr = new DefaultServiceResponse(true);
		sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());

		return sr;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.isa.thinair.paymentbroker.core.bl.PaymentBroker#reverse(com.isa.thinair.paymentbroker.api.model.CreditCardPayment
	 * , java.lang.String, com.isa.thinair.paymentbroker.api.util.AppIndicatorEnum,
	 * com.isa.thinair.paymentbroker.api.util.TnxModeEnum)
	 */
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
				PaymentBrokerInternalConstants.MessageCodes.REVERSE_OPERATION_NOT_SUPPORTED);

		return sr;
	}

	/**
	 * Returns Payment Gateway Name
	 * 
	 * @throws ModuleException
	 */
	@Override
	public String getPaymentGatewayName() {
		return ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	/**
	 * Returns the card type
	 * 
	 * @param cardType
	 * @return
	 */
	private int getCardType(String cardType) {
		int intCardType = 5;
		Integer iCardType;
		String strCardType = mapCardType.get(cardType);

		if (strCardType != null || !strCardType.equals("")) {
			iCardType = new Integer(strCardType);
			intCardType = iCardType.intValue();
		}

		return intCardType;
	}

	/**
	 * Resolves partial payments [ Invokes via a scheduler operation ]
	 */
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, "paymentbroker.generic.operation.notsupported");
		return sr;
	}

	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
				PaymentBrokerInternalConstants.MessageCodes.CAPTURE_OPERATION_NOT_SUPPORTED);

		return sr;
	}

	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
				PaymentBrokerInternalConstants.MessageCodes.CAPTURE_STATUS_OPERATION_NOT_SUPPORTED);

		return sr;
	}

	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		// TODO - Implement card configuration data
		return getRequestData(ipgRequestDTO);
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");		
	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
}
