package com.isa.thinair.paymentbroker.core.bl.eDirham;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.TempPaymentTnxTypes;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.LanguageCodesEnum;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;


public class PaymentBrokerEDirhamECommerceImpl extends PaymentBrokerEDirhamTemplate {

	private static final String E_DIRHAM_ERROR_PREFIX = "edirham.";
	private static Log log = LogFactory.getLog(PaymentBrokerEDirhamECommerceImpl.class);
	private static final String RETURN_URL = AppSysParamsUtil.getSecureIBEUrl() + "handleInterlineIPGResponse.action";

	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		Map<String, String> payWebPostData = preparePayWebRequest(merchantTxnId, ipgRequestDTO);

		String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(payWebPostData);
		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();
		String requestDataForm = PaymentBrokerUtils.getPostInputDataFormHTML(payWebPostData, getIpgURL());

		log.debug("payWebPostData: " + strRequestParams + " SessionID: " + sessionID);

		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
				strRequestParams + "," + sessionID, "", getPaymentGatewayName(), null, false);

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(requestDataForm);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);
		ipgRequestResultsDTO.setPostDataMap(payWebPostData);


		return ipgRequestResultsDTO;

	}

	private Map<String, String> preparePayWebRequest(String merchantTxnId, IPGRequestDTO ipgRequestDTO)
			throws ModuleException {

		String language = getSupportedLanguages().contains(ipgRequestDTO.getSessionLanguageCode()) ? ipgRequestDTO
				.getSessionLanguageCode() : LanguageCodesEnum.LANGUAGE.ENGLISH.getLanguage();
		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		postDataMap.put(EDirhamRequest.ACTION, EDirhamPaymentUtils.EDIRHAM_OPERATION.PAY_WEB.getOperation());
		postDataMap.put(EDirhamRequest.APPLICATIONNUMBER, getApplicationNumber());
		postDataMap.put(EDirhamRequest.BANKID, getBankId());
		postDataMap.put(EDirhamRequest.CURRENCY, getNumericCurrencyCode(ipgRequestDTO.getIpgIdentificationParamsDTO()
				.getPaymentCurrencyCode()));
		postDataMap.put(EDirhamRequest.ESERVICEMAINCODESUBCODE_1, getServiceMainCodeSubCode());
		postDataMap.put(EDirhamRequest.ESERVICEPRICE_1, ipgRequestDTO.getAmount());
		postDataMap.put(EDirhamRequest.ESERVICEQUANTITY_1, getServiceQuantity());
		postDataMap.put(EDirhamRequest.EXTRAFIELDS_F14, ipgRequestDTO.getReturnUrl());//RETURN_URL
		postDataMap.put(EDirhamRequest.EXTRAFIELDS_F16, getStandardCardType(ipgRequestDTO.getPaymentMethod()));
		postDataMap.put(EDirhamRequest.EXTRAFIELDS_F18, getTerminalId());
		postDataMap.put(EDirhamRequest.EXTRAFIELDS_INTENDEDEDIRHAMSERVICE,
				EDirhamPaymentUtils.EDIRHAM_OPERATION.PAY_WEB.getOperation());
		postDataMap.put(EDirhamRequest.LANGUAGE, language);
		postDataMap.put(EDirhamRequest.MERCHANTID, getMerchantId());
		postDataMap.put(EDirhamRequest.MERCHANTMODULESESSIONID, sessionID);
		// postDataMap.put(EDirhamRequest.NATIONALID, "");
		postDataMap.put(EDirhamRequest.PUN, merchantTxnId);
		postDataMap.put(EDirhamRequest.PAYMENTDESCRIPTION, EDirhamPaymentUtils.PAY_DESCRIPTION);
		// + EDirhamPaymentUtils.EDIRHAM_OPERATION.PAY_WEB.toString());
		postDataMap.put(EDirhamRequest.TRANSACTIONREQUESTDATE, EDirhamPaymentUtils.getTransactionReqTime());

		String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

		try {
			String secureHash = EDirhamPaymentUtils.computeSecureHash(postDataMap, getSha1In(),
					EDirhamPaymentUtils.EDIRHAM_OPERATION.PAY_WEB);
			postDataMap.put(EDirhamRequest.SECUREHASH, secureHash);
			strRequestParams = strRequestParams + "," + EDirhamRequest.SECUREHASH + "=" + secureHash;
		} catch (Exception e) {
			log.debug("Error computing SECUREHASH : " + e.toString());
			throw new ModuleException(e.getMessage());
		}

		return postDataMap;

	}

	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {

		log.debug("[PaymentBrokerEDirhamECommerceImpl::getReponseData()] Begin ");

		int tnxResultCode;
		String errorCode = "";
		String status;
		String errorSpecification = "";
		String merchantTxnReference;
		String accelAeroTxnReference;
		BigDecimal receivedEDirhamFee = AccelAeroCalculator.getDefaultBigDecimalZero();

		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());

		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

		EDirhamResponse eDirhamResp = new EDirhamResponse();
		eDirhamResp.setResponse(receiptyMap, EDirhamPaymentUtils.EDIRHAM_OPERATION.PAY_WEB.getOperation());

		if (log.isDebugEnabled()) {
			log.debug("[PaymentBrokerEDirhamECommerceImpl::getReponseData()] Response -" + eDirhamResp.toString());
		}

		if (eDirhamResp.getStatus().equalsIgnoreCase(EDirhamPaymentUtils.TRANSACTION_SUCCESSFULL)) {

			String genSecureHash = "";

			try {
				genSecureHash = EDirhamPaymentUtils.generateSecureHashFromResponse(eDirhamResp, getSha1In(),
						EDirhamPaymentUtils.EDIRHAM_OPERATION.PAY_WEB);
			} catch (Exception e) {
				log.debug("Error computing SECUREHASH : " + e.toString());
				throw new ModuleException(e.getMessage());
			}

			if (genSecureHash.equals(eDirhamResp.getSecureHash())) {
				TempPaymentTnx tempPaymentTnx = ReservationModuleUtils.getReservationBD().loadTempPayment(oCreditCardTransaction.getTemporyPaymentId());
				//skip multiple HandleInterlineIPGResponseAction executes @ RS status
				if(TempPaymentTnxTypes.RESERVATION_SUCCESS.equals(tempPaymentTnx.getStatus())||TempPaymentTnxTypes.PAYMENT_SUCCESS.equals(tempPaymentTnx.getStatus())){
					log.debug("HandleInterlineIPGResponseAction multiply executed ");					
					ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_ACCEPTED);					
					return ipgResponseDTO;										
				}				
				BigDecimal paymentAmount = tempPaymentTnx.getAmount();
				
				BigDecimal receivedPayAmount = new BigDecimal(eDirhamResp.getTransactionAmount());
				receivedEDirhamFee = new BigDecimal(eDirhamResp.geteDirhamFees());
				// TODO handle other fees if they are in the response

				receivedPayAmount = AccelAeroCalculator.divide(receivedPayAmount, 100);
				receivedEDirhamFee = AccelAeroCalculator.divide(receivedEDirhamFee, 100);

				log.debug("[PaymentBrokerEDirhamECommerceImpl::getReponseData()] - Payment Amount-" + paymentAmount
						+ " receivedPayAmount-" + receivedPayAmount + " receivedEDirhamFee-" + receivedEDirhamFee);

				if (paymentAmount.compareTo(receivedPayAmount.subtract(receivedEDirhamFee)) == 0) {
					status = IPGResponseDTO.STATUS_ACCEPTED;
					tnxResultCode = 1;
				} else {
					// Theoritically this scenario cannot happen
					status = IPGResponseDTO.STATUS_REJECTED;
					tnxResultCode = 0;
					errorCode = "VERIFICATION_AMOUNTS_ERROR";
					errorSpecification = "Payment and verifcation amounts different!!!";
					// Different amount in response
					log.error("[PaymentBrokerEDirhamECommerceImpl::getReponseData()] - Payment and response amounts are different");
				}
			} else {
				log.warn("Wrong secure hash in response genSecureHash:" + genSecureHash + ", received SecureHash: "
						+ eDirhamResp.getSecureHash());
				status = IPGResponseDTO.STATUS_REJECTED;
				tnxResultCode = 0;
				String[] error = EDirhamPaymentUtils.getMappedUnifiedError(eDirhamResp.getStatus());
				errorCode = E_DIRHAM_ERROR_PREFIX + error[0];
				errorSpecification = "" + error[1];
			}


		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
			String[] error = EDirhamPaymentUtils.getMappedUnifiedError(eDirhamResp.getStatus());
			errorCode = E_DIRHAM_ERROR_PREFIX + error[0];
			errorSpecification = "" + error[1];
		}

		merchantTxnReference = eDirhamResp.getConfirmationID();
		accelAeroTxnReference = eDirhamResp.getPun();

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setApplicationTransactionId(accelAeroTxnReference);
		ipgResponseDTO.setAuthorizationCode(merchantTxnReference);
		ipgResponseDTO.setCardType(5);
		ipgResponseDTO.seteDirhamFee(receivedEDirhamFee);
		String eDirhamResponseStr = eDirhamResp.toString();
		updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), eDirhamResponseStr, errorSpecification,
				merchantTxnReference, merchantTxnReference, tnxResultCode);

		if (log.isDebugEnabled()) {
			log.debug("[PaymentBrokerEDirhamECommerceImpl::getReponseData()] - Staus-" + status + " Error Code-" + errorCode
					+ " Error Spec-" + errorSpecification);
		}

		log.debug("[PaymentBrokerEDirhamECommerceImpl::getReponseData()] End");

		return ipgResponseDTO;
	}
}
