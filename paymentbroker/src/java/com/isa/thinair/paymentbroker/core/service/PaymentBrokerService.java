package com.isa.thinair.paymentbroker.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * @author Srikantha
 * 
 *         PaymentBrokerModule's service interface
 * @isa.module.service-interface module-name="paymentbroker" description="The Payment Broker module"
 */
public class PaymentBrokerService extends DefaultModule {

}
