package com.isa.thinair.paymentbroker.core.bl.cmi;

import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.APPROVED;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.CHARGE_TYPE_CD_SUCCESS;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.DECLINED;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.ERROR;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.REFUND;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.SERVICETYPE_REFUND;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.SERVICETYPE_REVERSAL;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.TRANS_STAT_SUCCESS;
import static com.isa.thinair.paymentbroker.api.util.PaymentConstants.VOID;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.utils.ConfirmReservationUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.bl.cyberplus.PaymentBrokerCyberPlusMOTOImpl;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class PaymentBrokerCMITemplate extends PaymentBrokerTemplate implements PaymentBroker {

	static Log log = LogFactory.getLog(PaymentBrokerCyberPlusMOTOImpl.class);

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	private String storetype;
	private String currency;
	private String oid;
	private String okUrl;
	private String failUrl;
	private String lang;
	private String hashAlgorithm;
	private String userName;
	private String storeKey;

	@Override
	public Properties getProperties() {
		Properties props = super.getProperties();
		props.put("storetype", PlatformUtiltiies.nullHandler(getStoretype()));
		props.put("lang", PlatformUtiltiies.nullHandler(getLang()));
		props.put("hashAlgorithm", PlatformUtiltiies.nullHandler(getHashAlgorithm()));
		props.put("userName", PlatformUtiltiies.nullHandler(getUserName()));
		props.put("password", PlatformUtiltiies.nullHandler(getPassword()));
		return props;
	}

	public String getStoretype() {
		return storetype;
	}

	public void setStoretype(String storetype) {
		this.storetype = storetype;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public String getOkUrl() {
		return okUrl;
	}

	public String getStoreKey() {
		return storeKey;
	}

	public void setStoreKey(String storeKey) {
		this.storeKey = storeKey;
	}

	public void setOkUrl(String okUrl) {
		this.okUrl = okUrl;
	}

	public String getFailUrl() {
		return failUrl;
	}

	public void setFailUrl(String failUrl) {
		this.failUrl = failUrl;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getHashAlgorithm() {
		return hashAlgorithm;
	}

	public void setHashAlgorithm(String hashAlgorithm) {
		this.hashAlgorithm = hashAlgorithm;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		log.debug("Inside the refund() of CMI pg implementation");
		// to load previous transaction with id
		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO()
				.loadTransaction(creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

		CMIXMLRequest request = new CMIXMLRequest();
		request.setClientId(getMerchantId());
		request.setName(getUserName());
		request.setPassword(getPassword());
		request.setType(VOID);
		if (creditCardPayment.getTravelDTO() != null) {
			BillTo buyerDet = new BillTo();
			request.setEmail(creditCardPayment.getTravelDTO().getEmailAddress());
			buyerDet.setName(creditCardPayment.getTravelDTO().getContactName());
			buyerDet.setTelVoice(creditCardPayment.getTravelDTO().getContactNumber());
			buyerDet.setCountry(creditCardPayment.getTravelDTO().getResidenceCountryCode());
			request.setBillTo(buyerDet);
		}
		// If it's a IBE
		if (SalesChannelsUtil.isAppIndicatorWebOrMobile(appIndicator.toString())) {
			request.setTransId(ccTransaction.getTransactionId());
			// If it's a XBE
		} else if (AppIndicatorEnum.APP_XBE.toString().equals(appIndicator.toString())) {
			request.setOrderId(ccTransaction.getTransactionId());
		} else {
			throw new ModuleException("paymentbroker.cmi.online.request.build.error");
		}
		String requestString = null;
		try {
			requestString = CMIPaymentUtils.generateXMLString(request);
		} catch (JAXBException e) {
			throw new ModuleException("paymentbroker.cmi.online.request.build.error");
		}

		// getting merchant transaction id

		String merchantTxnId = composeAccelAeroTransactionRef(appIndicator, getMerchantId(), null, null,
				PaymentConstants.MODE_OF_SERVICE.VOID);

		ccTransaction = auditTransaction(pnr, getMerchantId(), merchantTxnId, creditCardPayment.getTemporyPaymentId(),
				bundle.getString(SERVICETYPE_REVERSAL), request.toString(), "", getPaymentGatewayName(), null, false);
		DefaultServiceResponse serviceResponse = new DefaultServiceResponse(false);
		Integer ProcReturnCode = 0;
		// calling the payment gateway API
		try {
			CMIXMLResponse response = CMIPaymentUtils.fireRequest(requestString, ipgURL);
			Integer paymentBrokerRefNo = ccTransaction.getTransactionRefNo();
			if (response.getResponse().equals(APPROVED)) {
				serviceResponse.setResponseCode(IPGResponseDTO.STATUS_ACCEPTED);
				serviceResponse.setSuccess(true);
				ProcReturnCode = 00;
			} else if (response.getResponse().equals(DECLINED)) {
				serviceResponse.setResponseCode(IPGResponseDTO.STATUS_REJECTED);
				ProcReturnCode = 99;
			} else if (response.getResponse().equals(ERROR)) {
				serviceResponse.setResponseCode(IPGResponseDTO.STATUS_REJECTED);
			}
			updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), response.toString(), response.getErrMsg(),
					response.getAuthCode(), response.getOrderId(), ProcReturnCode);
			serviceResponse.setResponseCode(String.valueOf(paymentBrokerRefNo));
		} catch (XMLStreamException | JAXBException e) {
			log.error(CMIConstants.ERROR_MSG_WS + e);
			throw new ModuleException("paymentbroker.cmi.online.request.rejected.error");
		} catch (ModuleException e) {
			throw new ModuleException("paymentbroker.cmi.online.request.build.error");
		}

		return serviceResponse;
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {
		Iterator itColIPGQueryDTO = colIPGQueryDTO.iterator();

		Collection<Integer> oResultIF = new ArrayList<Integer>();
		Collection<Integer> oResultIP = new ArrayList<Integer>();
		Collection<Integer> oResultII = new ArrayList<Integer>();
		Collection<Integer> oResultIS = new ArrayList<Integer>();

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		IPGQueryDTO ipgQueryDTO;
		CreditCardTransaction oCreditCardTransaction;
		PaymentQueryDR query;
		PreviousCreditCardPayment oPrevCCPayment;
		CreditCardPayment oCCPayment;
		Date currentTime = Calendar.getInstance().getTime();
		Calendar queryValidTime = Calendar.getInstance();

		while (itColIPGQueryDTO.hasNext()) {
			ipgQueryDTO = (IPGQueryDTO) itColIPGQueryDTO.next();

			oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgQueryDTO.getPaymentBrokerRefNo());

			query = getPaymentQueryDR();
			IPGConfigsDTO ipgConfigsDTO = new IPGConfigsDTO();
			try {
				ServiceResponce serviceResponce = query.query(oCreditCardTransaction, null,
						PaymentBrokerInternalConstants.QUERY_CALLER.SCHEDULER);

				queryValidTime.setTime(ipgQueryDTO.getTimestamp());
				queryValidTime.add(Calendar.MINUTE, AppSysParamsUtil.getTimeForPaymentStatusUpdate());

				// If successful payment remains at Bank
				if (serviceResponce.getResponseCode().equals(IPGResponseDTO.STATUS_ACCEPTED)) {

					if (serviceResponce.isSuccess()) {
						boolean isResConfirmationSuccess = ConfirmReservationUtil.isReservationConfirmationSuccess(ipgQueryDTO);

						if (isResConfirmationSuccess) {
							if (log.isDebugEnabled()) {
								log.debug("Status moving to RS:" + oCreditCardTransaction.getTemporyPaymentId());
								continue;

							}

						}

					} else {
						if (refundFlag) {
							// Do refund
							String txnNo = PlatformUtiltiies
									.nullHandler(serviceResponce.getResponseParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE));

							oPrevCCPayment = new PreviousCreditCardPayment();
							oPrevCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
							oPrevCCPayment.setPgSpecificTxnNumber(txnNo);

							oCCPayment = new CreditCardPayment();
							oCCPayment.setPreviousCCPayment(oPrevCCPayment);
							oCCPayment.setAmount(ipgQueryDTO.getAmount().toString());
							oCCPayment.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
							oCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
							oCCPayment.setIpgIdentificationParamsDTO(ipgQueryDTO.getIpgIdentificationParamsDTO());
							oCCPayment.setPnr(oCreditCardTransaction.getTransactionReference());
							oCCPayment.setTemporyPaymentId(oCreditCardTransaction.getTemporyPaymentId());

							ServiceResponce srRev = reverse(oCCPayment, oCCPayment.getPnr(), oCCPayment.getAppIndicator(),
									oCCPayment.getTnxMode());

							if (srRev.isSuccess()) {
								// Change State to 'IS'
								oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
								if (log.isDebugEnabled()) {
									log.debug("Status moving to IS:" + oCreditCardTransaction.getTemporyPaymentId());
								}
							} else {
								// Change State to 'IF'
								oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
								if (log.isDebugEnabled()) {
									log.debug("Status moving to IF:" + oCreditCardTransaction.getTemporyPaymentId());
								}
							}
						} else {
							// Change State to 'IP'
							oResultIP.add(oCreditCardTransaction.getTemporyPaymentId());
							if (log.isDebugEnabled()) {
								log.debug("Status moving to IP:" + oCreditCardTransaction.getTemporyPaymentId());
							}
						}
					}
				}
				// No payment detail at bank
				else {
					// No Payments exist
					// Update status to 'II'
					if (currentTime.getTime() > queryValidTime.getTimeInMillis()) {
						oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
						log.info("Status moing to II:" + oCreditCardTransaction.getTemporyPaymentId());
					} else {
						// Can not move payment status. with in status change
						// for 3D secure payments
						log.info("Status not moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
					}
				}
			} catch (ModuleException e) {
				log.info("Scheduler::ResolvePayment FAILED!!! " + e.getMessage(), e);
			}
		}

		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF, oResultIF);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_II, oResultII);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IP, oResultIP);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS, oResultIS);

		sr.setSuccess(true);

		return sr;
	}

	public ServiceResponce queryOrder(CMIXMLRequest request, CreditCardTransaction creditCardTransaction, QUERY_CALLER scheduler)
			throws ModuleException {

		String requestString = null;
		CMIXMLResponse response = null;
		StringBuilder errorCode = new StringBuilder();
		errorCode.append(scheduler.toString());
		DefaultServiceResponse serviceResponse = new DefaultServiceResponse(false);
		try {
			requestString = CMIPaymentUtils.generateXMLString(request);
		} catch (JAXBException e) {
			log.error(CMIConstants.ERROR_MSG_WS + e);
			throw new ModuleException("paymentbroker.cmi.online.request.build.error");
		}

		try {
			response = CMIPaymentUtils.fireRequest(requestString, ipgURL);
			if (response == null) {
				throw new ModuleException("paymentbroker.cmi.online.request.rejected.error");
			}

			if (PaymentConstants.RETURN_CODE_SUCCESS.equals(response.getProcReturnCode())) {

				serviceResponse.setResponseCode(IPGResponseDTO.STATUS_ACCEPTED);
				if ((TRANS_STAT_SUCCESS.contains(response.getExtra().getTRANS_STAT())
						&& (CHARGE_TYPE_CD_SUCCESS.equals(response.getExtra().getCHARGE_TYPE_CD())))) {
					serviceResponse.setSuccess(true);
					serviceResponse.addResponceParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE,
							creditCardTransaction.getTransactionId());
				}

			} else {
				serviceResponse.setResponseCode(IPGResponseDTO.STATUS_REJECTED);
			}
			errorCode.append(response.getErrMsg());
		} catch (XMLStreamException | JAXBException e) {
			log.error(CMIConstants.ERROR_MSG_WS + e);
			throw new ModuleException("paymentbroker.cmi.online.request.rejected.error");
		} catch (ModuleException e) {
			throw new ModuleException("paymentbroker.cmi.online.request.build.error");
		}

		return serviceResponse;
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		// TODO Auto-generated method stub

	}

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// to load previous transaction with id
		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO()
				.loadTransaction(creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

		CMIXMLRequest request = new CMIXMLRequest();
		request.setClientId(getMerchantId());
		request.setName(getUserName());
		request.setPassword(getPassword());
		request.setTotal(Double.parseDouble(creditCardPayment.getAmount()));
		request.setType(REFUND);
		if (creditCardPayment.getTravelDTO() != null) {
			BillTo buyerDet = new BillTo();
			request.setEmail(creditCardPayment.getTravelDTO().getEmailAddress());
			buyerDet.setName(creditCardPayment.getTravelDTO().getContactName());
			buyerDet.setTelVoice(creditCardPayment.getTravelDTO().getContactNumber());
			buyerDet.setCountry(creditCardPayment.getTravelDTO().getResidenceCountryCode());
			request.setBillTo(buyerDet);
		}
		// If it's a IBE
		if (SalesChannelsUtil.isAppIndicatorWebOrMobile(appIndicator.toString())) {
			request.setTransId(ccTransaction.getTransactionId());
			// If it's a XBE
		} else if (AppIndicatorEnum.APP_XBE.toString().equals(appIndicator.toString())) {
			request.setOrderId(ccTransaction.getTransactionId());
		} else {
			throw new ModuleException("paymentbroker.cmi.online.request.build.error");
		}
		String requestString = null;
		try {
			requestString = CMIPaymentUtils.generateXMLString(request);
		} catch (JAXBException e) {
			log.error(CMIConstants.ERROR_MSG_WS + e);
			throw new ModuleException("paymentbroker.cmi.online.request.build.error");
		}

		// getting merchant transaction id

		String merchantTxnId = composeAccelAeroTransactionRef(appIndicator, getMerchantId(), null, null,
				PaymentConstants.MODE_OF_SERVICE.REFUND);

		ccTransaction = auditTransaction(pnr, getMerchantId(), merchantTxnId, creditCardPayment.getTemporyPaymentId(),
				bundle.getString(SERVICETYPE_REFUND), request.toString(), "", getPaymentGatewayName(),
				ccTransaction.getTransactionId(), false);
		DefaultServiceResponse serviceResponse = new DefaultServiceResponse(false);
		Integer procReturnCode = 0;
		// calling the payment gateway API
		Integer paymentBrokerRefNo = ccTransaction.getTransactionRefNo();
		CMIXMLResponse response = null;
		String errorCode = null;
		try {
			response = CMIPaymentUtils.fireRequest(requestString, ipgURL);
			if (response == null) {
				throw new ModuleException("paymentbroker.cmi.online.request.rejected.error");
			}
			procReturnCode = Integer.parseInt(response.getProcReturnCode());

			if (APPROVED.equals(response.getResponse())) {
				serviceResponse.setResponseCode(IPGResponseDTO.STATUS_ACCEPTED);
				serviceResponse.setSuccess(true);
			} else {
				errorCode = response.getErrMsg();
				serviceResponse.setResponseCode(IPGResponseDTO.STATUS_REJECTED);
			}
		} catch (XMLStreamException | JAXBException e) {
			log.error(CMIConstants.ERROR_MSG_WS + e);
			throw new ModuleException("paymentbroker.cmi.online.request.rejected.error");
		} catch (ModuleException e) {
			throw new ModuleException("paymentbroker.cmi.online.request.build.error");
		}
		updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), response.toString(), errorCode, response.getAuthCode(),
				response.getOrderId(), procReturnCode);
		serviceResponse.setResponseCode(String.valueOf(paymentBrokerRefNo));
		return serviceResponse;
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

}
