package com.isa.thinair.paymentbroker.core.bl.telemoney;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;

/**
 * @author Dhanushka Ranatunga
 */
public class TeleMoneyIPGUtil {

	private static Log log = LogFactory.getLog(TeleMoneyIPGUtil.class);

	/**
	 * Build IPGConfigsDTO on privided details
	 * 
	 * @param merchantID
	 * @param accessCode
	 * @param ipgURL
	 * @param userName
	 * @param password
	 * @param statusURL
	 * @return
	 */
	public static IPGConfigsDTO getIPGConfigs(String merchantID, String statusUrl) {
		IPGConfigsDTO oIPGConfigsDTO = new IPGConfigsDTO();
		oIPGConfigsDTO.setMerchantID(merchantID);
		oIPGConfigsDTO.setStatusUrl(statusUrl);

		return oIPGConfigsDTO;
	}

}