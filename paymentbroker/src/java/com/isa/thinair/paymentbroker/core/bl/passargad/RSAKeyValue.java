package com.isa.thinair.paymentbroker.core.bl.passargad;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "modulus", "exponent", "p", "q", "dp", "dq", "d", "inverseQ" })
@XmlRootElement(name = "RSAKeyValue")
public class RSAKeyValue {

	private String modulus;

	private String exponent;

	private String inverseQ;

	private String p;

	private String q;

	private String dp;

	private String dq;

	private String d;

	@XmlElement(name = "P")
	public void setP(String p) {
		this.p = p;
	}

	@XmlElement(name = "D")
	public void setD(String d) {
		this.d = d;
	}

	@XmlElement(name = "Modulus")
	public void setModulus(String modulus) {
		this.modulus = modulus;
	}

	@XmlElement(name = "Exponent")
	public void setExponent(String exponent) {
		this.exponent = exponent;
	}

	@XmlElement(name = "Q")
	public void setQ(String q) {
		this.q = q;
	}

	@XmlElement(name = "DP")
	public void setDp(String dp) {
		this.dp = dp;
	}

	@XmlElement(name = "DQ")
	public void setDq(String dq) {
		this.dq = dq;
	}

	@XmlElement(name = "InverseQ")
	public void setInverseQ(String inverseQ) {
		this.inverseQ = inverseQ;
	}

	public String getModulus() {
		return modulus;
	}

	public String getExponent() {
		return exponent;
	}

	public String getInverseQ() {
		return inverseQ;
	}

	public String getP() {
		return p;
	}

	public String getQ() {
		return q;
	}

	public String getDp() {
		return dp;
	}

	public String getDq() {
		return dq;
	}

	public String getD() {
		return d;
	}
}
