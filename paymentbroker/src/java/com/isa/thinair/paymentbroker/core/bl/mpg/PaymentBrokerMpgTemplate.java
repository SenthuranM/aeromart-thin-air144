package com.isa.thinair.paymentbroker.core.bl.mpg;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class PaymentBrokerMpgTemplate extends PaymentBrokerTemplate implements
		PaymentBroker {
	
	private static Log log = LogFactory.getLog(PaymentBrokerMpgTemplate.class);

	protected static final String ERROR_CODE_PREFIX = "mpg.";
	protected static final String CARD_TYPE_GENERIC = "GC";
	protected static final String DEFAULT_ERROR_CODE = "payment.failed";
	
	private String merchantName;
	private String purchaseCurrency;
	private String purchaseCurrencyExponent;
	private String acquirerId;
	private String signatureMethod;
	private String validReasonsCodes;
	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();
	
	@Override
	public Properties getProperties() {
		Properties props = super.getProperties();

		props.put(MpgRequest.ACQUIRER_ID, PlatformUtiltiies.nullHandler(acquirerId));
		props.put(MpgRequest.PURCHASE_CURRENCY, PlatformUtiltiies.nullHandler(purchaseCurrency));
		props.put(MpgRequest.PURCHASE_CURRENCY_EXPONENT, PlatformUtiltiies.nullHandler(purchaseCurrencyExponent));
		props.put(MpgRequest.SIGNATURE_METHOD, PlatformUtiltiies.nullHandler(signatureMethod));
		props.put(MpgRequest.MERCHANT_ID, PlatformUtiltiies.nullHandler(merchantId));
		props.put(MpgRequest.MERCHANT_NAME, PlatformUtiltiies.nullHandler(merchantName));
		props.put(MpgRequest.PASSWORD, PlatformUtiltiies.nullHandler(password));
		
		return props;
	}
	

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment,
			String pnr, AppIndicatorEnum appIndicator, TnxModeEnum tnxMode)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment,
			String pnr, AppIndicatorEnum appIndicator, TnxModeEnum tnxMode,
			List<CardDetailConfigDTO> cardDetailConfigData)
			throws ModuleException {
		
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment,
			String pnr, AppIndicatorEnum appIndicator, TnxModeEnum tnxMode)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment,
			String pnr, AppIndicatorEnum appIndicator, TnxModeEnum tnxMode)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO,
			List<CardDetailConfigDTO> configDataList) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap,
			IPGResponseDTO ipgResponseDTO) throws ModuleException {
		log.debug("[PaymentBrokerMpgTemplate::getReponseData()] Begin ");

		boolean errorExists = false;
		int tnxResultCode;
		String errorCode = "";
		String status;
		String errorSpecification;
		String merchantTxnReference;
		String authorizeID;
		int cardType = 5; // default card type

		String hashValidated = null;
		String responseMismatch = "";
		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());

		// Calculate response time
		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time
		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

		MpgResponse mpgResp = new MpgResponse();
		mpgResp.setResponse(receiptyMap);
		log.debug("[PaymentBrokerMpgTemplate::getReponseData()] Mid Response -" + mpgResp.toString());
		
		if (MpgResponse.TRANSACTION_APPROVED.equals(mpgResp.getReasonCode())) {
			// Validate the Hash if required
			if (MpgPaymentUtils.validateHASH(mpgResp, getProperties())) {

				hashValidated = MpgResponse.VALID_HASH;
				// Check for correct response
				errorExists = true;

				if (oCreditCardTransaction.getTempReferenceNum().equalsIgnoreCase(mpgResp.getMerchantOrderId())) {
					errorExists = false;
				} else {
					responseMismatch = MpgResponse.INVALID_RESPONSE;
					// Use QueryDR to verify the transaction
					// Currently there is no method for Query
				}
			} else {
				errorExists = true;
				hashValidated = MpgResponse.INVALID_HASH;
			}
		} else {
			// Secure Hash was not validated,
			hashValidated = MpgResponse.NO_VALUE;
			errorCode = ERROR_CODE_PREFIX;
		}

		merchantTxnReference = mpgResp.getMerchantOrderId();
		authorizeID = mpgResp.getReasonCode();
		cardType = getStandardCardType(CARD_TYPE_GENERIC);

		if ((MpgResponse.TRANSACTION_APPROVED.equals(mpgResp.getReasonCode()))	&& !errorExists) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			tnxResultCode = 1;
			errorSpecification = "" + responseMismatch;
		} else {
			
			String responseStatus = mpgResp.getReasonCode();
			log.debug( "[PaymentBrokerMpgTemplate::getReponseData()] " + responseStatus + " : " + mpgResp.getMerchantOrderId());
			
			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
			
			if(validReasonsCodes !=null && validReasonsCodes.contains(mpgResp.getReasonCode())){
				errorCode = ERROR_CODE_PREFIX + mpgResp.getReasonCode();
			}else{
				errorCode = ERROR_CODE_PREFIX + DEFAULT_ERROR_CODE;
			}
			
			errorSpecification = status + " " + hashValidated + " "
					+ mpgResp.getReasonDescription() + " "
					+ mpgResp.getReasonCode() + " " + responseMismatch;
		}

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setMessage(mpgResp.getReasonDescription());
		ipgResponseDTO.setApplicationTransactionId(merchantTxnReference);
		ipgResponseDTO.setAuthorizationCode(authorizeID);
		ipgResponseDTO.setCardType(cardType);
		String trimedDigits = "";
		if (mpgResp.getCardNumber() != null && mpgResp.getCardNumber().length() > 4) {
			trimedDigits = mpgResp.getCardNumber().substring(mpgResp.getCardNumber().length() - 4);
		}

		ipgResponseDTO.setCcLast4Digits(trimedDigits);
		String mpgResponse = mpgResp.toString();

		updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), mpgResponse, errorSpecification,
				mpgResp.getReferenceNumber(), mpgResp.getReferenceNumber(), tnxResultCode);

		log.debug("[PaymentBrokerMpgTemplate::getReponseData()] End -" + mpgResp.getMerchantOrderId() + "");

		return ipgResponseDTO;
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection creditCardPayment,
			boolean refundFlag) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(
			IPGPrepPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap,
			IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO)
			throws ModuleException {
		// TODO Auto-generated method stub

	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	/**
	 * @return the purchaseCurrency
	 */
	public String getPurchaseCurrency() {
		return purchaseCurrency;
	}

	/**
	 * @param purchaseCurrency the purchaseCurrency to set
	 */
	public void setPurchaseCurrency(String purchaseCurrency) {
		this.purchaseCurrency = purchaseCurrency;
	}

	/**
	 * @return the purchaseCurrencyExponent
	 */
	public String getPurchaseCurrencyExponent() {
		return purchaseCurrencyExponent;
	}

	/**
	 * @param purchaseCurrencyExponent the purchaseCurrencyExponent to set
	 */
	public void setPurchaseCurrencyExponent(String purchaseCurrencyExponent) {
		this.purchaseCurrencyExponent = purchaseCurrencyExponent;
	}

	/**
	 * @return the acquirerId
	 */
	public String getAcquirerId() {
		return acquirerId;
	}

	/**
	 * @param acquirerId the acquirerId to set
	 */
	public void setAcquirerId(String acquirerId) {
		this.acquirerId = acquirerId;
	}

	/**
	 * @return the signatureMethod
	 */
	public String getSignatureMethod() {
		return signatureMethod;
	}

	/**
	 * @param signatureMethod the signatureMethod to set
	 */
	public void setSignatureMethod(String signatureMethod) {
		this.signatureMethod = signatureMethod;
	}


	/**
	 * @return the merchantName
	 */
	public String getMerchantName() {
		return merchantName;
	}


	/**
	 * @param merchantName the merchantName to set
	 */
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	
	private int getStandardCardType(String card) {
		// [1-MASTER, 2-VISA, 3-AMEX, 4-DINNERS, 5-GENERIC]
		int mapType = 5; // default card type
		if (card != null && !card.equals("") && mapCardType.get(card) != null) {
			String mapTypeStr = (String) mapCardType.get(card);
			mapType = Integer.parseInt(mapTypeStr);
		}
		return mapType;
	}


	public String getValidReasonsCodes() {
		return validReasonsCodes;
	}


	public void setValidReasonsCodes(String validResponseCodes) {
		this.validReasonsCodes = validResponseCodes;
	}

	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
	
}
