package com.isa.thinair.paymentbroker.core.bl.cybersource;

import java.util.Map;
import java.util.StringTokenizer;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import sun.misc.BASE64Encoder;

import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class CyberSourceResponse {

	private static final Log log = LogFactory.getLog(CyberSourceResponse.class);

	public static final String NO_VALUE = "";
	public static final String VALID_HASH = "VALID HASH.";
	public static final String INVALID_HASH = "INVALID HASH.";
	public static final String INVALID_RESPONSE = "INVALID RESPONSE";

	private static final String DECISION = "decision";
	private static final String ORDER_NUMBER = "orderNumber";
	private static final String REASON_CODE = "reasonCode";
	private static final String CARD_TYPE = "card_cardType";
	private static final String AUTHORIZATION_CODE = "ccAuthReply_ authorizationCode";
	private static final String REQUEST_ID = "requestID";

	public static final String DECISION_ACCEPT = "ACCEPT";

	private String sharedSecret;
	private String decision = "";
	private String orderNumber = "";
	private String reasonCode = "";
	private String cardType = "";
	private String authorizationCode = "";
	private String requestID = "";

	private Map<String, String> response = null;

	public CyberSourceResponse(Map<String, String> fields) {
		this.response = fields;
		this.decision = null2Empty(fields.get(DECISION));
		this.orderNumber = null2Empty(fields.get(ORDER_NUMBER));
		this.reasonCode = null2Empty(fields.get(REASON_CODE));
		this.cardType = null2Empty(fields.get(CARD_TYPE));
		this.authorizationCode = null2Empty(fields.get(AUTHORIZATION_CODE));
		this.requestID = null2Empty(fields.get(REQUEST_ID));
	}

	public boolean verifyTransactionSignature(String sharedSecret) {
		Map<String, String> map = getResponse();
		this.sharedSecret = sharedSecret;

		if (map == null) {
			return false;
		}
		String transactionSignature = (String) map.get("signedDataPublicSignature");
		if (transactionSignature == null) {
			return false;
		}
		String transactionSignatureFields = (String) map.get("signedFields");
		if (transactionSignatureFields == null) {
			return false;
		}
		StringTokenizer tokenizer = new StringTokenizer(transactionSignatureFields, ",", false);
		StringBuilder data = new StringBuilder();
		while (tokenizer.hasMoreTokens()) {
			String key = tokenizer.nextToken();
			data.append(key + "=" + map.get(key));
			data.append(',');
		}
		data.append("signedFieldsPublicSignature=");
		try {
			data.append(getPublicDigest(transactionSignatureFields).trim());
		} catch (Exception e) {
			log.error("verifyTransactionSignature", e);
			return false;
		}
		return verifySignature(data.toString(), transactionSignature);
	}

	private boolean verifySignature(String data, String signature) {
		if (data == null || signature == null) {
			return false;
		}
		try {
			String pub = getSharedSecret();
			BASE64Encoder encoder = new BASE64Encoder();
			Mac sha1Mac = Mac.getInstance("HmacSHA1");
			SecretKeySpec publicKeySpec = new SecretKeySpec(pub.getBytes(), "HmacSHA1");
			sha1Mac.init(publicKeySpec);
			byte[] publicBytes = sha1Mac.doFinal(data.getBytes());
			String publicDigest = encoder.encodeBuffer(publicBytes);
			publicDigest = publicDigest.replaceAll("[\r\n\t]", "");
			return signature.equals(publicDigest);
		} catch (Exception e) {
			log.error("verifySignature", e);
			return false;
		}
	}

	private String getPublicDigest(String customValues) throws Exception {
		String pub = getSharedSecret();
		BASE64Encoder encoder = new BASE64Encoder();
		Mac sha1Mac = Mac.getInstance("HmacSHA1");
		SecretKeySpec publicKeySpec = new SecretKeySpec(pub.getBytes(), "HmacSHA1");
		sha1Mac.init(publicKeySpec);
		byte[] publicBytes = sha1Mac.doFinal(customValues.getBytes());
		String publicDigest = encoder.encodeBuffer(publicBytes);
		return publicDigest.replaceAll("\n", "");
	}

	public int getCardType() {
		int cardNo = Integer.parseInt(this.cardType);
		// [1-MASTER, 2-VISA, 3-AMEX, 4-DINNERS, 5-GENERIC]
		int type = 5;

		switch (cardNo) {
		case 1:
			type = 2;
			break;

		case 2:
			type = 1;
			break;

		case 7:
			type = 5;
			break;

		default:
			type = 5;
			break;
		}

		return type;
	}

	private static String null2Empty(String in) {
		return PlatformUtiltiies.nullHandler(in);
	}

	public String getSharedSecret() {
		return sharedSecret;
	}

	public String getDecision() {
		return decision;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public String getAuthorizationCode() {
		return authorizationCode;
	}

	public Map<String, String> getResponse() {
		return response;
	}

	public String getRequestID() {
		return requestID;
	}

}
