package com.isa.thinair.paymentbroker.core.bl.nmcryptgate;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import net.netmanagement.NMCG;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.ISACreditCardValidator;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Srikantha
 * 
 *         PaymentBrokerNMCryptgateImpl
 */
public class PaymentBrokerNMCryptgateImpl extends PaymentBrokerTemplate implements PaymentBroker {

	private static Log log = LogFactory.getLog(PaymentBrokerNMCryptgateImpl.class);

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	private NMCG nmcg = null;

	/**
	 * Call NMCryptgate
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @param arrServiceType
	 * @return
	 */
	private ServiceResponce callNMCryptgate(CreditCardPayment creditCardPayment, String pnr, char appIndicator, int tnxMode,
			Map mapInput) throws ModuleException {

		if (log.isDebugEnabled())
			log.debug("Begin PaymentBrokerNMCryptgateImpl:callNMCryptgate()");

		nmcg = new NMCG();

		// Creating new VLIST object.
		// Return value is a handle of that object in all operations. Object is
		// accessed via handle
		int VLIST = nmcg.nmlistalloc();
		if (log.isDebugEnabled())
			log.debug("## Completed NMCryptgate VLIST memory allocation ...");

		// 0 and positive values are correct, negative value means error
		if (VLIST != -1) {

			// Process the valid vlist entries
			ServiceResponce serviceResponce = this.processValidVLIST(VLIST, nmcg, appIndicator, creditCardPayment, pnr, mapInput);

			// De-allocate memory to avoid memory leaks
			nmcg.nmlistfree(VLIST);
			if (log.isDebugEnabled())
				log.debug("## Completed NMCryptgate VLIST memory de-allocation ...");

			if (log.isDebugEnabled())
				log.debug("End PaymentBrokerNMCryptgateImpl:callNMCryptgate()");
			return serviceResponce;
		} else {
			log.error("## Error while allocating memory for NMCryptgate VLIST [nmcryptgate error message="
					+ nmcg.nmcryptgateStrerror() + "]");

			// Free memory before throwing the exception
			nmcg.nmlistfree(VLIST);
			if (log.isDebugEnabled())
				log.debug("## Completed NMCryptgate VLIST memory de-allocation ...");

			throw new ModuleException("paymentbroker.error.memory");
		}
	}

	/**
	 * Process valid VLIST objects
	 * 
	 * @param VLIST
	 * @param nmcg
	 * @param appIndicator
	 * @param creditCardPayment
	 * @param pnr
	 * @param mapInput
	 * @return
	 * @throws ModuleException
	 */
	private ServiceResponce processValidVLIST(int VLIST, NMCG nmcg, char appIndicator, CreditCardPayment creditCardPayment,
			String pnr, Map mapInput) throws ModuleException {
		DefaultServiceResponse serviceResponse = new DefaultServiceResponse(false);

		HashMap mapOutput = new HashMap();
		boolean isSuccess = false;
		if (log.isDebugEnabled()) {
			log.debug("## Begin composing NMCryptegate request params");
			log.debug("## " + PaymentConstants.PARAM_COMPANY + " -- " + !isNull(bundle.getString("COMPANY")));
		}
		nmcg.nmtagadd(VLIST, PaymentConstants.PARAM_COMPANY, bundle.getString("COMPANY"));

		String strUserName = "";
		String strPassword = "";
		String referenceNum = "";
		String cardHolderRef = "";

		if (AppIndicatorEnum.APP_XBE.getCode() == appIndicator) {
			nmcg.nmtagadd(VLIST, PaymentConstants.PARAM_COMPANY_SUB_ID, bundle.getString("COMPANY_SUB_ID_C"));
			if (log.isDebugEnabled())
				log.debug("## " + PaymentConstants.PARAM_COMPANY_SUB_ID + " -- " + !isNull(bundle.getString("COMPANY_SUB_ID_C")));

			strUserName = bundle.getString("USER_NAME_C");
			strPassword = bundle.getString("PASSWORD_C");
		} else {
			nmcg.nmtagadd(VLIST, PaymentConstants.PARAM_COMPANY_SUB_ID, bundle.getString("COMPANY_SUB_ID_W"));
			if (log.isDebugEnabled())
				log.debug("## " + PaymentConstants.PARAM_COMPANY_SUB_ID + " -- " + !isNull(bundle.getString("COMPANY_SUB_ID_W")));

			strUserName = bundle.getString("USER_NAME_W");
			strPassword = bundle.getString("PASSWORD_W");
		}

		referenceNum = composeReferenceNum(pnr, appIndicator);
		cardHolderRef = composeCardHolderRef(pnr, appIndicator);

		if (log.isDebugEnabled()) {
			log.debug("## " + PaymentConstants.PARAM_COMPANY_SERVICETYPE + " -- " + mapInput.get("SERVICETYPE"));
			log.debug("## " + PaymentConstants.PARAM_COMPANY_CARDHOLDER + " -- " + !isNull(creditCardPayment.getCardholderName()));
			log.debug("## " + PaymentConstants.PARAM_COMPANY_CARDNUMBER + " -- " + !isNull(creditCardPayment.getCardNumber()));
			log.debug("## " + PaymentConstants.PARAM_COMPANY_EXPIRY + " -- " + !isNull(creditCardPayment.getExpiryDate()));
			log.debug("## " + PaymentConstants.PARAM_COMPANY_AMOUNT + " -- " + !isNull(creditCardPayment.getAmount()));
			log.debug("## " + PaymentConstants.PARAM_COMPANY_CURRENCY + " -- " + creditCardPayment.getCurrency());
			log.debug("## " + PaymentConstants.PARAM_COMPANY_CVV + " -- " + !isNull(creditCardPayment.getCvvField()));
			log.debug("## " + PaymentConstants.PARAM_COMPANY_CARDHOLDERREF + " -- " + cardHolderRef);
			log.debug("## " + PaymentConstants.PARAM_COMPANY_REFERENCENUM + " -- " + referenceNum);
		}

		nmcg.nmtagadd(VLIST, PaymentConstants.PARAM_COMPANY_SERVICETYPE, (String) mapInput.get("SERVICETYPE"));
		nmcg.nmtagadd(VLIST, PaymentConstants.PARAM_COMPANY_CARDHOLDER, creditCardPayment.getCardholderName());
		nmcg.nmtagadd(VLIST, PaymentConstants.PARAM_COMPANY_CARDNUMBER, creditCardPayment.getCardNumber());
		nmcg.nmtagadd(VLIST, PaymentConstants.PARAM_COMPANY_EXPIRY, creditCardPayment.getExpiryDate());
		nmcg.nmtagadd(VLIST, PaymentConstants.PARAM_COMPANY_AMOUNT, creditCardPayment.getAmount());
		nmcg.nmtagadd(VLIST, PaymentConstants.PARAM_COMPANY_CURRENCY, creditCardPayment.getCurrency());

		nmcg.nmtagadd(VLIST, PaymentConstants.PARAM_COMPANY_REFERENCENUM, referenceNum);
		nmcg.nmtagadd(VLIST, PaymentConstants.PARAM_COMPANY_CARDHOLDERREF, cardHolderRef);
		nmcg.nmtagadd(VLIST, PaymentConstants.PARAM_COMPANY_CVV, creditCardPayment.getCvvField());

		// If service type is reversal, additional properties need to be
		// processed
		if (mapInput != null && ((String) mapInput.get("SERVICETYPE")).equals(bundle.getString("SERVICETYPE_REVERSAL"))) {
			if (log.isDebugEnabled()) {
				log.debug("## " + PaymentConstants.PARAM_COMPANY_TRANSACTIONNR + " -- " + (String) mapInput.get("TRANSACTIONNR"));
				log.debug("## " + PaymentConstants.PARAM_COMPANY_AID + " -- " + (String) mapInput.get("AID"));
				log.debug("## " + PaymentConstants.PARAM_COMPANY_TRANSREF + " -- " + (String) mapInput.get("TRANSACTIONREF"));
			}

			nmcg.nmtagadd(VLIST, PaymentConstants.PARAM_COMPANY_TRANSACTIONNR, (String) mapInput.get("TRANSACTIONNR"));
			nmcg.nmtagadd(VLIST, PaymentConstants.PARAM_COMPANY_AID, (String) mapInput.get("AID"));
			nmcg.nmtagadd(VLIST, PaymentConstants.PARAM_COMPANY_TRANSREF, (String) mapInput.get("TRANSACTIONREF"));
		}

		if (log.isDebugEnabled())
			log.debug("## End composing NMCryptegate request params");

		String strServiceName = bundle.getString("SERVICE_NAME");

		// Trying to output/print list of all tags. Rewind list pointer to
		// the begin of the list

		/**
		 * ENABLE THE FOLLOWING BIT **** IF YOU WANT TO REVIEW WHATS GOING IN THE NM TRANSACTION *
		 */
		/***********************************************************************
		 * nmcryptgate.nmlistrewind(VLIST);
		 * 
		 * while (true) { String strResult = nmcryptgate.nmtagnext(VLIST); if (strResult == null) { break; }
		 * PaymentBrokerUtils.debug(log, "\t\t" + strResult); // --remove }
		 **********************************************************************/

		if (log.isDebugEnabled())
			log.debug("## Start NMCryptgate call [COMPANYreferencenum=" + referenceNum + "]");

		// Calling cryptgate with our input VLIST. crypgate will return new
		// VLIST with result
		int RESULT_VLIST = nmcg.nmcryptgate(strUserName, strPassword, strServiceName, VLIST);

		if (log.isDebugEnabled())
			log.debug("## Completed NMCryptgate call [COMPANYreferencenum=" + referenceNum + "]");

		nmcg.nmlistrewind(RESULT_VLIST);

		boolean rcSuccess = false;
		boolean _rcSuccess = false;
		String strResultFinal = "";

		while (true) {
			strResultFinal = nmcg.nmtagnext(RESULT_VLIST);

			if (strResultFinal == null) {
				break;
			}

			StringTokenizer st = new StringTokenizer(strResultFinal, "\n\r");
			while (st.hasMoreTokens()) {

				StringTokenizer stl = new StringTokenizer(st.nextToken(), "=");
				if (stl.hasMoreTokens()) {
					String strTag = stl.nextToken();

					if (stl.hasMoreTokens()) {
						String strValue = stl.nextToken();

						/*
						 * The Linux system, which installed nmcryptgate.so file should opened the port number [1610]
						 * 
						 * In most cases, nmcryptgate() will return allocated VLIST anyway -- even in case of
						 * communication failure. Programmer should examine value of special return code tag ("_rc", and
						 * "_rc_str") to find exact status of performed operation. In case of possible success
						 * (sucessful server connection, sucessful authentication, and login), value of "_rc" tag will
						 * be "1". Otherwise, it will indicate error on lower level. The values and their description
						 * are:
						 * 
						 * 1 -- success -1 -- undefined error (no memory, etc) -2 -- client configuration is bad, can't
						 * find server for requested service. -3 -- connect() failed -4 -- stage 1 communication failure
						 * -5 -- stage 1 protocol failure (authorization) -6 -- request encode error. -7 -- no server
						 * found in config file.
						 * 
						 * [rc] -- result code [rc_str] -- result code string [_rc] -- transaction result code
						 */

						// Put the Key-Value pairs into the HashMap object
						mapOutput.put(strTag, strValue);

						if (strTag.equals("rc")) {
							if (strValue.equals("0")) {
								rcSuccess = true;
							} else {
								rcSuccess = false;
							}
						}

						if (strTag.equals("_rc")) {
							if (strValue.equals("1")) {
								_rcSuccess = true;
							} else {
								_rcSuccess = false;
							}
						}
					}
				}
			}
		}

		isSuccess = (rcSuccess && _rcSuccess);

		int transref = -1;

		try {
			// Save the transaction
			transref = PaymentBrokerUtils.getPaymentBrokerDAO().composeTransaction(mapOutput,
					creditCardPayment.getTemporyPaymentId(), (String) mapInput.get("SERVICETYPE"), getPaymentGatewayName());
		} catch (Exception e) {
			log.error("Error in saving card payment transaction response", e);
		}

		if (log.isInfoEnabled()) {
			String NMTransactionNo = mapOutput.containsKey("_transaction") ? (String) mapOutput.get("_transaction") : "";
			log.info("Card payment completed [status = " + (isSuccess ? "SUCCESS" : "FAILED") + ",serviceType= "
					+ mapInput.get("SERVICETYPE") + ",dbRefNo=" + transref + ",COMPANYreferencenum=" + referenceNum
					+ ",NM_transaction=" + NMTransactionNo + ",amount=" + creditCardPayment.getAmount() + "]");
		}

		serviceResponse.setResponseCode(String.valueOf(transref));

		Object rcValue = mapOutput.get("rc");
		Object _rcValue = mapOutput.get("_rc");

		if (rcValue == null) {
			if (_rcValue == null) {
				log.error("Invalid NMCryptgate reposnse found [rc and _rc are null]");
				throw new ModuleException("paymentbroker.result.null");
			} else {
				String _rcStrValue = _rcValue.toString();
				serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, _rcStrValue);
				serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentConstants.SERVICE_RESPONSE_ERROR_SERVER);
			}
		} else {
			String rcStrValue = rcValue.toString();
			serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, rcStrValue);
			if (mapOutput.get("rc_str") != null) {
				serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, mapOutput.get("rc_str")
						.toString());
			}
		}

		if (log.isDebugEnabled())
			log.debug("Card payment completed [AA Service Reponse="
					+ serviceResponse.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE) + ",dbRefNo="
					+ transref + "]");

		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_RESULTS, mapOutput);
		serviceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, mapOutput.get("aid"));

		// To avoid memory leaks
		nmcg.nmlistfree(RESULT_VLIST);

		serviceResponse.setSuccess(isSuccess);

		return serviceResponse;
	}

	/**
	 * stick to 14 characters (was 13 characters) Added appIndicator to make the make the card holder reference number
	 * among all the channels unique - 11 Dec 2006
	 * 
	 * Made it back to 13 characters by removing one left padded character in reseration number - 17 Dec 2006
	 * 
	 * @param pnr
	 * @param appIndicator
	 *            Indicates the channel
	 * @return
	 */
	private String composeCardHolderRef(String pnr, char appIndicator) {
		return PaymentConstants.PREFIX_CARDHOLDERREF + appIndicator
				+ StringUtils.leftPad(pnr, PaymentConstants.PNR_LENGTH_CARDHOLDERREF, PaymentConstants.PNR_LEFT_PAD_CHAR);
	}

	/**
	 * stick to 15 chars max length is changed to 14 chars by by removing one left padded character in reseration number
	 * - 17 Dec 2006
	 * 
	 * @param pnr
	 * @param appIndicator
	 * @return
	 */
	private String composeReferenceNum(String pnr, char appIndicator) {
		return appIndicator + composeReferenceDate()
				+ StringUtils.leftPad(pnr, PaymentConstants.PNR_LENGTH_REFERENCENUM, PaymentConstants.PNR_LEFT_PAD_CHAR);
	}

	/**
	 * formats the now date in the following fashion 060521 ==> GF21 010910 ==> BI10
	 * 
	 * @return
	 */
	private String composeReferenceDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yy/MM/dd");
		String date[] = StringUtils.split(sdf.format(new Date()), '/');
		int y = Integer.parseInt(date[0]) + PaymentConstants.ASCII_A;
		int m = Integer.parseInt(date[1]) + PaymentConstants.ASCII_A;
		int d = Integer.parseInt(date[2]);
		if (d > 9) {
			d = (d - 9) + PaymentConstants.ASCII_A;
			return (String.valueOf((char) y) + String.valueOf((char) m) + String.valueOf((char) d));
		}
		return (String.valueOf((char) y) + String.valueOf((char) m) + d);

	}

	/**
	 * Atomic reverse
	 * 
	 * @param newCardPayment
	 * @param previousCreditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @param additionalInfo
	 * @return
	 * @throws ModuleException
	 */

	private ServiceResponce reverse(CreditCardPayment newCardPayment, PreviousCreditCardPayment previousCreditCardPayment,
			String pnr, AppIndicatorEnum appIndicator, TnxModeEnum tnxMode) throws ModuleException {

		if (log.isDebugEnabled())
			log.debug("## reverse: Initiating validation ...");

		boolean proceed = false;

		// Check the main credit card information
		checkCreditCardParameters(newCardPayment);

		DefaultServiceResponse response = new DefaultServiceResponse();

		// Added Ceil to make the decimal point correct if any decimal point
		// conversion error exist
		double newPayment = Math.ceil(Double.parseDouble(newCardPayment.getAmount()));
		double oldPayment = Math.ceil(previousCreditCardPayment.getTotalAmount().doubleValue());

		DecimalFormat df = new DecimalFormat("#.00");

		if (!df.format(oldPayment).equalsIgnoreCase(df.format(newPayment))) {
			proceed = false;
			if (log.isDebugEnabled())
				log.debug("## reverse: Amount Mismatch ");
		} else if (!newCardPayment.getCardholderName().equalsIgnoreCase(previousCreditCardPayment.getName())) {
			proceed = false;
			if (log.isDebugEnabled())
				log.debug("## reverse: Card Holder Name Mismatch");
		} else if (!newCardPayment.getCvvField().equalsIgnoreCase(previousCreditCardPayment.getSecurityCode())) {
			proceed = false;
			if (log.isDebugEnabled())
				log.debug("## reverse: Card CVV Mismatch");
		} else if (!newCardPayment.getCardNumber()
				.substring(newCardPayment.getCardNumber().length() - 4, newCardPayment.getCardNumber().length())
				.equalsIgnoreCase(previousCreditCardPayment.getNoLastDigits())) {
			proceed = false;
			if (log.isDebugEnabled())
				log.debug("## reverse: Card Number Mismatch");
		} else if (!newCardPayment.getExpiryDate().equalsIgnoreCase(previousCreditCardPayment.getEDate())) {
			proceed = false;
			if (log.isDebugEnabled())
				log.debug("## reverse: Card Expiry Mismatch");
		} else {
			proceed = true;
		}

		if (log.isDebugEnabled())
			log.debug("## reverse: parameters validation [status = " + proceed + "]");

		if (!proceed) {
			throw new ModuleException("paymentbroker.reverse.invalid");
		}

		if (log.isDebugEnabled())
			log.debug("##  reverse: prilimanary parameters are ready for reverse...");

		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				previousCreditCardPayment.getPaymentBrokerRefNo());

		// Adding payment gateway check to avoid reverse for other payment gateway transactions
		// Nili 5:58 PM 9/6/2007
		ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
		if (!serviceResponse.isSuccess()) {
			return serviceResponse;
		}

		if (ccTransaction != null) {
			try {
				// Check the valid cut over time or not
				if (this.checkValidCutOverTime(ccTransaction)) {

					if (log.isDebugEnabled())
						log.debug("## reverse: paramters are ready for reverse in local ...");

					Map mapInput = new HashMap();
					mapInput.put("SERVICETYPE", bundle.getString("SERVICETYPE_REVERSAL"));
					mapInput.put("TRANSACTIONNR", ccTransaction.getTransactionRefCccompany());
					mapInput.put("AID", ccTransaction.getAidCccompnay());
					mapInput.put("TRANSACTIONREF", ccTransaction.getTransactionReference());

					return callNMCryptgate(newCardPayment, pnr, appIndicator.getCode(), tnxMode.getCode(), mapInput);
				} else {
					if (log.isDebugEnabled())
						log.debug("## reverse: paramters are NOT ready for reverse in local ...");
					response.setSuccess(false);
					response.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
							PaymentConstants.SERVICE_RESPONSE_REVERSE_TIME);
					return response;
				}
			} catch (NumberFormatException e) {
				throw new ModuleException("paymentbroker.error.int");

			} catch (ParseException e) {
				throw new ModuleException("paymentbroker.error.int");
			}
		} else {
			if (log.isDebugEnabled())
				log.debug("## reverse: Unable to locate the original transaction ...");
			response.setSuccess(false);
			response.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentConstants.SERVICE_RESPONSE_REVERSE_TNX);
			return response;
		}

	}

	/**
	 * Check the valid cut over time
	 * 
	 * Chk within the same date and before CARD daily process, which is @ 19:30 GMT
	 * 
	 * @param transactionNM
	 * @return
	 * @throws ParseException
	 */
	private boolean checkValidCutOverTime(CreditCardTransaction transactionNM) throws ParseException {
		SimpleDateFormat yyyyMMddHHmmss = new SimpleDateFormat("yyyyMMddHHmmss");
		Date tranDate = yyyyMMddHHmmss.parse(transactionNM.getTransactionTimestamp());
		Date currentDate = new Date();

		// Get the days difference
		long diffDays = (currentDate.getTime() - tranDate.getTime()) / (24 * 60 * 60 * 1000);

		/**
		 * Ok folks here goes the situvations.. Say cut over time is 19:30
		 * 
		 * Trans Date | Current Date | Status 10:00 10:01 Y 20:00 20:01 Y 19:29 19:31 N 19:30 19:30 Y 19:30 19:31 N
		 * 19:29 19:30 Y
		 */

		// It's with in the day
		if (diffDays == 0) {
			Date pgGMTcutoverDT = this.getPGGMTCutOverDT();
			SimpleDateFormat hhmm = new SimpleDateFormat("HHmm");

			int currentDateHHMM = Integer.parseInt(hhmm.format(currentDate));
			int pgGMTCutOverDateHHMM = Integer.parseInt(hhmm.format(pgGMTcutoverDT));
			int transDateHHMM = Integer.parseInt(hhmm.format(tranDate));

			if (currentDateHHMM < pgGMTCutOverDateHHMM) {
				return true;
			} else if (currentDateHHMM > pgGMTCutOverDateHHMM) {
				if (transDateHHMM <= pgGMTCutOverDateHHMM) {
					return false;
				} else {
					return true;
				}
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	/**
	 * Returns the payment gateway gmt cut over date time
	 * 
	 * @return
	 * @throws ParseException
	 */
	private Date getPGGMTCutOverDT() throws ParseException {
		SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");

		String pgGMTcutover = PaymentBrokerUtils.getGlobalConfig().getBizParam(SystemParamKeys.PG_GMT_CUTOVER_TIME);
		return sdfTime.parse(pgGMTcutover);
	}

	/**
	 * Performing refund operation
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @param additionalInfo
	 * @param reverseOnly
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		PreviousCreditCardPayment previousCreditCardPayment = creditCardPayment.getPreviousCCPayment();

		if (log.isDebugEnabled())
			log.debug("## refund: Initiating ...");

		if (previousCreditCardPayment != null) {
			if (log.isDebugEnabled())
				log.debug("## refund: attempting reverse instead refund ...");

			try {
				DefaultServiceResponse response = (DefaultServiceResponse) reverse(creditCardPayment, pnr, appIndicator, tnxMode);

				if (response.isSuccess()) {
					response.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
							PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
					if (log.isInfoEnabled())
						log.info("## refund: Reversed instead of refund [pnr=" + pnr + "]");
					return response;
				}
			} catch (ModuleException me) {
				log.error("Error in reversing payment", me);
			}
		}

		// Now trying the refund
		checkCreditCardParameters(creditCardPayment);

		if (log.isDebugEnabled())
			log.debug("## refund: attempting refund ...");

		Hashtable ht = new Hashtable();
		ht.put("SERVICETYPE", bundle.getString("SERVICETYPE_REFUND"));

		return callNMCryptgate(creditCardPayment, pnr, appIndicator.getCode(), tnxMode.getCode(), ht);
	}

	/**
	 * Authorize
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {

		if (log.isDebugEnabled())
			log.debug("## authorize: initiating ...");

		// Checking the original card payment
		checkCreditCardParameters(creditCardPayment);

		Map mapInput = new HashMap();
		mapInput.put("SERVICETYPE", bundle.getString("SERVICETYPE_AUTHORIZE"));

		if (log.isDebugEnabled())
			log.debug("## authorize: attempting authorize ...");
		return callNMCryptgate(creditCardPayment, pnr, appIndicator.getCode(), tnxMode.getCode(), mapInput);
	}

	/**
	 * Reverse the card payment
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		PreviousCreditCardPayment previousCreditCardPayment = creditCardPayment.getPreviousCCPayment();

		if (log.isDebugEnabled())
			log.debug("## reverse: initiating pure reverse ");

		if (previousCreditCardPayment == null) {
			if (log.isDebugEnabled())
				log.debug("## reverse: missing original transaction [pnr=" + pnr + "]");
			throw new ModuleException("paymentbroker.reverse.invalid");
		}

		if (log.isDebugEnabled())
			log.debug("## reverse: attempting pure reverse ");
		// now try performing a reverse
		return reverse(creditCardPayment, previousCreditCardPayment, pnr, appIndicator, tnxMode);
	}

	/**
	 * Check Credit card parameters
	 * 
	 * @param ccPayment
	 * @throws CommonsDataAccessException
	 */
	private void checkCreditCardParameters(CreditCardPayment ccPayment) throws ModuleException {

		// if ccPayment is null or the necessary inputs are null throw the
		// exception
		if (ccPayment == null || ccPayment.getCardholderName() == null || ccPayment.getCardNumber() == null
				|| ccPayment.getExpiryDate() == null || ccPayment.getCurrency() == null || ccPayment.getAmount() == null) {
			throw new ModuleException("paymentbroker.invalid.parameters");
		}

		ISACreditCardValidator cardValidator = new ISACreditCardValidator();

		if (!cardValidator.isValidExpiryDate(ccPayment.getExpiryDate())) {
			throw new ModuleException("paymentbroker.invalid.expdate");
		}

		if (!cardValidator.isValidCVV(ccPayment.getCvvField())) {
			throw new ModuleException("paymentbroker.invalid.cvv");
		}
	}

	/**
	 * Check to see the param is null or not
	 * 
	 * @param param
	 * @return
	 */
	private boolean isNull(String param) {
		return (param == null);
	}

	/**
	 * Returns Request Data
	 * 
	 * @param ipgRequestDTO
	 * @return
	 */
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	/**
	 * Returns Response Data
	 * 
	 * @param receiptyMap
	 * @param ipgResponseDTO
	 * @return
	 */
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	/**
	 * Returns Payment Gateway Name
	 * 
	 * @throws ModuleException
	 */
	@Override
	public String getPaymentGatewayName() throws ModuleException {
		PaymentBrokerManager.validateIPGIdentificationParams(ipgIdentificationParamsDTO);
		return ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	/**
	 * Resolves partial payments [ Invokes via a scheduler operation ]
	 */
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, "paymentbroker.generic.operation.notsupported");
		return sr;
	}

	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
				PaymentBrokerInternalConstants.MessageCodes.CAPTURE_OPERATION_NOT_SUPPORTED);

		return sr;
	}

	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
				PaymentBrokerInternalConstants.MessageCodes.CAPTURE_STATUS_OPERATION_NOT_SUPPORTED);

		return sr;
	}

	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		// TODO - Implement card configuration data
		return getRequestData(ipgRequestDTO);
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");		
	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
}
