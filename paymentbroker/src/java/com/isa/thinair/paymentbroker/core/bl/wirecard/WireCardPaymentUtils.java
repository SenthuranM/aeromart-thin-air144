package com.isa.thinair.paymentbroker.core.bl.wirecard;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.contrib.ssl.AuthSSLProtocolSocketFactory;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;

import com.ep.engine.model.AccountHolder;
import com.ep.engine.model.Address;
import com.ep.engine.model.CardType;
import com.ep.engine.model.MerchantAccountId;
import com.ep.engine.model.Money;
import com.ep.engine.model.Payment;
import com.ep.engine.model.PaymentMethod;
import com.ep.engine.model.PaymentMethodName;
import com.ep.engine.model.PaymentMethods;
import com.ep.engine.model.Status;
import com.ep.engine.model.ThreeD;
import com.ep.engine.model.TransactionType;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.webplatform.api.dto.XMLResponse;

public class WireCardPaymentUtils {

	private static Log log = LogFactory.getLog(WireCardPaymentUtils.class);

	private static final String YES = "Y";

	public static final String PAYMENT_RESPONSE_HASH = "eppresponse";

	public static enum WIRECARD_OPERATION {
		RES, DES, RFD
	};

	public static String getPostInputDataFormHTML(Map<String, String> postDataMap, String keyStoreAlias, String keyStorePassword,
			String keyStoreKeyPassword, IPGRequestDTO ipgRequestDTO) {
		try {
			Payment paymentRequest = WireCardPaymentUtils.getPayment(postDataMap, ipgRequestDTO);

			String KEYSTORE_LOCATION = postDataMap.get(WireCardRequest.KEYSTORE_LOCATION);
			String KEYSTORE_ALIAS = keyStoreAlias;
			String KEYSTORE_STORE_PASSWORD = keyStorePassword;
			String KEYSTORE_KEY_PASSWORD = keyStoreKeyPassword;

			String xmlDocEncoded = null;
			JAXBContext jaxbContext = JAXBContext.newInstance(Payment.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			PrintWriter printwriter = new PrintWriter(System.out);
			jaxbMarshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(paymentRequest, new StreamResult(printwriter));

			// We need to convert it to XML and add Signature
			// Create a DOM XMLSignatureFactory that will be used to
			// generate the enveloped signature.
			XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");
			// Create a Reference to the enveloped document (in this case,
			// you are signing the whole document, so a URI of "" signifies
			// that, and also specify the SHA1 digest algorithm and
			// the ENVELOPED Transform.
			Reference ref = fac.newReference("", fac.newDigestMethod(DigestMethod.SHA1, null),
					Collections.singletonList(fac.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null)), null, null);
			// Create the SignedInfo.
			SignedInfo si = fac.newSignedInfo(
					fac.newCanonicalizationMethod(CanonicalizationMethod.INCLUSIVE, (C14NMethodParameterSpec) null),
					fac.newSignatureMethod(SignatureMethod.RSA_SHA1, null), Collections.singletonList(ref));
			// Load the KeyStore and get the signing key and certificate.
			KeyStore ks = KeyStore.getInstance("JKS");
			ks.load(new FileInputStream(KEYSTORE_LOCATION), KEYSTORE_STORE_PASSWORD.toCharArray());
			KeyStore.PrivateKeyEntry keyEntry = (KeyStore.PrivateKeyEntry) ks.getEntry(KEYSTORE_ALIAS,
					new KeyStore.PasswordProtection(KEYSTORE_KEY_PASSWORD.toCharArray()));
			X509Certificate cert = (X509Certificate) keyEntry.getCertificate();

			// Create the KeyInfo containing the X509Data.
			KeyInfoFactory kif = fac.getKeyInfoFactory();
			List<Object> x509Content = new ArrayList<Object>();
			x509Content.add(cert.getSubjectX500Principal().getName());
			x509Content.add(cert);
			X509Data xd = kif.newX509Data(x509Content);
			KeyInfo ki = kif.newKeyInfo(Collections.singletonList(xd));

			// Instantiate the document to be signed.
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			StreamResult streamResult = new StreamResult(byteArrayOutputStream);
			jaxbMarshaller.marshal(paymentRequest, streamResult);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			Document doc = dbf.newDocumentBuilder().parse(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));

			// Create a DOMSignContext and specify the RSA PrivateKey and
			// location of the resulting XMLSignature's parent element.
			DOMSignContext dsc = new DOMSignContext(keyEntry.getPrivateKey(), doc.getDocumentElement());

			// Create the XMLSignature, but don't sign it yet.
			XMLSignature signature = fac.newXMLSignature(si, ki);

			// Marshal, generate, and sign the enveloped signature.
			signature.sign(dsc);

			// Output the resulting document. return payment;

			Source source = new DOMSource(doc);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer trans = tf.newTransformer();
			StringWriter sw = new StringWriter();
			trans.transform(source, new StreamResult(sw));

			if (log.isDebugEnabled()) {
				log.debug(sw.toString());
			}

			byteArrayOutputStream.reset();
			trans.transform(source, streamResult);

			// Create base 64 of the document
			xmlDocEncoded = Base64.encodeBase64String(byteArrayOutputStream.toByteArray());
			StringBuilder sb = new StringBuilder();

			sb.append("<form action='" + postDataMap.get(WireCardRequest.IPG_URL)
					+ "' method='post' id='epform' enctype='application/x-www-form-urlencoded'> ");
			sb.append("<input type='hidden' name='epprequest' value='" + xmlDocEncoded + "'> ");
			sb.append("<script type='text/javascript'> document.getElementById('epform').submit();" + "<\\/script>");
			sb.append("</form>");
			return sb.toString();
		} catch (Exception e) {
			log.error("getPostInputDataFormHTML", e);
		}
		return null;
	}

	public static Payment getPayment(Map<String, String> postDataMap, IPGRequestDTO ipgRequestDTO) {
		Payment payment = new Payment();

		MerchantAccountId mid = new MerchantAccountId();
		mid.setValue(postDataMap.get(WireCardRequest.PSPID));
		payment.setMerchantAccountId(mid);

		AccountHolder accountHolder = new AccountHolder();
		accountHolder.setFirstName(BeanUtils.nullHandler(ipgRequestDTO.getContactFirstName()));
		accountHolder.setLastName(BeanUtils.nullHandler(ipgRequestDTO.getContactLastName()));
		accountHolder.setEmail(BeanUtils.maskSpace(ipgRequestDTO.getEmail()));
		accountHolder.setPhone(BeanUtils.nullHandler(ipgRequestDTO.getContactMobileNumber()));

		Address address = new Address();
		address.setCity(BeanUtils.maskSpace(ipgRequestDTO.getContactCity()));
		address.setCountry(BeanUtils.maskSpace(ipgRequestDTO.getContactCountryCode()));
		address.setState(BeanUtils.maskSpace(ipgRequestDTO.getContactState()));
		address.setStreet1(BeanUtils.maskSpace(ipgRequestDTO.getContactAddressLine2()));
		address.setStreet2(BeanUtils.maskSpace(ipgRequestDTO.getContactAddressLine1()));
		address.setPostalCode(BeanUtils.maskSpace(ipgRequestDTO.getContactMobileNumber().split("-")[1]));
		accountHolder.setAddress(address);

		payment.setAccountHolder(accountHolder);
		payment.setRequestId(postDataMap.get(WireCardRequest.REQUESTID));
		payment.setTransactionType(TransactionType.PURCHASE);

		Money amt = new Money();
		amt.setCurrency(BeanUtils.nullHandler(postDataMap.get(WireCardRequest.CURRENCY)));
		amt.setValue(new BigDecimal(postDataMap.get(WireCardRequest.AMOUNT)));
		payment.setRequestedAmount(amt);

		PaymentMethods paymentMethods = new PaymentMethods();
		PaymentMethod method = new PaymentMethod();
		method.setName(PaymentMethodName.CREDITCARD);
		paymentMethods.getPaymentMethods().add(method);
		payment.setPaymentMethods(paymentMethods);

		payment.setOrderNumber(postDataMap.get(WireCardRequest.ORDERNUMBER));
		payment.setOrderDetail(postDataMap.get(WireCardRequest.ORDERDETAIL));
		payment.setIpAddress(ipgRequestDTO.getUserIPAddress());
		payment.setRedirectUrl(ipgRequestDTO.getReturnUrl());
		payment.setLocale(postDataMap.get(WireCardRequest.LANGUAGE));

		ThreeD threeD = new ThreeD();
		threeD.setAttemptThreeD((YES.equals(postDataMap.get(WireCardRequest.RESTRICT_ATTEMPT_3D))) ? false : true);
		payment.setThreeD(threeD);

		return payment;
	}

	private static Payment getVoidPayment(Map<String, String> postDataMap) {
		Payment payment = new Payment();
		MerchantAccountId mid = new MerchantAccountId();
		mid.setValue(postDataMap.get(WireCardRequest.PSPID));
		payment.setMerchantAccountId(mid);
		payment.setRequestId(postDataMap.get(WireCardRequest.REQUESTID));
		payment.setTransactionType(TransactionType.VOID_PURCHASE);
		payment.setParentTransactionId(postDataMap.get(WireCardRequest.TRANSACTION_ID));
		payment.setIpAddress(postDataMap.get(WireCardRequest.HOST_IP_ADDRESS));

		PaymentMethods paymentMethods = new PaymentMethods();
		PaymentMethod method = new PaymentMethod();
		method.setName(PaymentMethodName.CREDITCARD);
		paymentMethods.getPaymentMethods().add(method);
		payment.setPaymentMethods(paymentMethods);
		return payment;
	}

	private static Payment getRefundPayment(Map<String, String> postDataMap) {
		Payment payment = new Payment();
		MerchantAccountId mid = new MerchantAccountId();
		mid.setValue(postDataMap.get(WireCardRequest.PSPID));
		payment.setMerchantAccountId(mid);
		payment.setRequestId(postDataMap.get(WireCardRequest.REQUESTID));
		payment.setTransactionType(TransactionType.REFUND_PURCHASE);
		payment.setParentTransactionId(postDataMap.get(WireCardRequest.TRANSACTION_ID));
		payment.setIpAddress(postDataMap.get(WireCardRequest.HOST_IP_ADDRESS));

		Money amt = new Money();
		amt.setCurrency(postDataMap.get(WireCardRequest.CURRENCY));
		amt.setValue(new BigDecimal(postDataMap.get(WireCardRequest.AMOUNT)));
		payment.setRequestedAmount(amt);

		PaymentMethods paymentMethods = new PaymentMethods();
		PaymentMethod method = new PaymentMethod();
		method.setName(PaymentMethodName.CREDITCARD);
		paymentMethods.getPaymentMethods().add(method);
		payment.setPaymentMethods(paymentMethods);
		return payment;
	}

	public static XMLResponse getVoidRefundXMLResponse(Map<String, String> postDataMap, String keystoreName,
			String keyStorePassword) {
		Payment paymentRequest = null;
		if (postDataMap.get(WireCardRequest.OPERATION).equals(TransactionType.REFUND_PURCHASE.toString())) {
			paymentRequest = getRefundPayment(postDataMap);
		} else if (postDataMap.get(WireCardRequest.OPERATION).equals(TransactionType.VOID_PURCHASE.toString())) {
			paymentRequest = getVoidPayment(postDataMap);
		}
		String paymentRequestXMLString = getPaymentXMLString(paymentRequest);

		PostMethod post = new PostMethod(postDataMap.get(WireCardRequest.URL));
		// Make sure that these headers are always set to get the appropriate response as per REST
		post.addRequestHeader("Content-Type", "application/xml");
		post.addRequestHeader("Accept", "application/xml");

		XMLResponse xmlResponse = new XMLResponse();

		try {
			// Makes sure the encoding is set UTF-8
			StringRequestEntity requestEntity = new StringRequestEntity(paymentRequestXMLString, post.getRequestHeader(
					"Content-Type").getValue(), "UTF-8");
			post.setRequestEntity(requestEntity);
			HttpClient client = composeHttpClient(keystoreName, keyStorePassword, postDataMap.get(WireCardRequest.USERID),
					postDataMap.get(WireCardRequest.PASSWORD));
			int result = client.executeMethod(post);
			String paymentResponseXMLString = BeanUtils.nullHandler(post.getResponseBodyAsString());

			if (log.isDebugEnabled()) {
				log.debug("Response HTTP status code: " + result);
				log.debug("--Payment Response--");
				log.debug(paymentResponseXMLString);
			}

			// Convert the response back to Java Objects
			if (!paymentResponseXMLString.isEmpty()) {
				return parseXML(paymentResponseXMLString);
			}
		} catch (Exception e) {
			xmlResponse.setNcError(BeanUtils.nullHandler(e.getMessage()));
			log.error("Exception occurred: ", e);
		} finally {
			post.releaseConnection();
		}

		return xmlResponse;
	}

	private static String getPaymentXMLString(Payment payment) {
		StringWriter stringWriter = new StringWriter();
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Payment.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(payment, stringWriter);
		} catch (Exception e) {
			log.error("getPaymentXMLString", e);
		}
		return stringWriter.toString();
	}

	public static Payment getPaymentResponse(String responseHash) {
		Payment payment = null;
		try {
			if (responseHash.length() > 0) {
				JAXBContext jaxbContext = JAXBContext.newInstance(Payment.class);
				Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

				ByteArrayInputStream bis = new ByteArrayInputStream(Base64.decodeBase64(responseHash));
				payment = (Payment) unmarshaller.unmarshal(bis);
			}
		} catch (Exception e) {
			log.error("getPaymentResponse", e);
		}

		return payment;
	}

	private static int getCardCode(CardType cardType) {
		if (CardType.MASTERCARD.equals(cardType)) {
			return 1;
		} else if (CardType.VISA.equals(cardType)) {
			return 2;
		} else if (CardType.AMEX.equals(cardType)) {
			return 3;
		} else if (CardType.DINERS.equals(cardType)) {
			return 4;
		} else {
			// return GENERIC as default
			return 5;
		}
	}

	public static int getCardType(String maskedCCNumber) {
		/**
		 * '*CARD TYPES *PREFIX *WIDTH 'American Express 34, 37 15 'Diners Club 300 to 305, 36 14 'Carte Blanche 38 14
		 * 'Discover 6011 16 'EnRoute 2014, 2149 15 'JCB 3 16 'JCB 2131, 1800 15 'Master Card 51 to 55 16 'Visa 4 13, 16
		 */
		int startingNumber = getStartingValue(maskedCCNumber, 2);
		if (startingNumber == 34 || startingNumber == 37) {
			return getCardCode(CardType.AMEX);
		} else if (startingNumber == 36) {
			return getCardCode(CardType.DINERS);
		} else if (startingNumber >= 51 && startingNumber <= 55) {
			return getCardCode(CardType.MASTERCARD);
		} else {
			startingNumber = getStartingValue(maskedCCNumber, 1);
			if (startingNumber == 4) {
				return getCardCode(CardType.VISA);
			} else {
				return getCardCode(null);
			}
		}
	}

	private static int getStartingValue(String maskedCCNumber, int startCount) {
		try {
			return new Integer(maskedCCNumber.substring(0, startCount));
		} catch (Exception e) {
			log.error("Invalid CreditCard Number", e);
			return 0;
		}
	}

	public static Payment getPaymentObject(String paymentXML) {
		Payment payment = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Payment.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(paymentXML);
			payment = (Payment) jaxbUnmarshaller.unmarshal(reader);
		} catch (Exception e) {
			log.error("getPaymentObject", e);
		}
		return payment;
	}

	public static XMLResponse parseXML(String refundPaymentResponse) {

		Payment paymentResponse = getPaymentObject(refundPaymentResponse);
		if (paymentResponse != null) {
			if (log.isDebugEnabled()) {
				// This ID should stored for further reference as well troubleshooting purpose
				log.debug("Engine Unique Transaction ID :" + paymentResponse.getTransactionId());

				// This is the property which signifies if a transaction is success or failed
				log.debug("Transaction State: "
						+ (paymentResponse.getTransactionState() != null ? paymentResponse.getTransactionState().value() : null));
			}
		}

		XMLResponse xmlResponse = new XMLResponse();
		xmlResponse.setStatus(paymentResponse.getTransactionState().toString());
		xmlResponse.setAcceptance(paymentResponse.getAuthorizationCode());
		List<Status> statusList = paymentResponse.getStatuses() != null ? paymentResponse.getStatuses().getStatuses() : null;
		if (statusList != null && statusList.size() > 0) {
			for (Status status : statusList) {
				xmlResponse.setNcError(status.getDescription());
				xmlResponse.setNcStatus(status.getCode());
				if (log.isDebugEnabled()) {
					log.debug("Transaction Status Code: " + status.getCode());
					log.debug("Transaction Status Description: " + status.getDescription());
				}
			}
		}
		xmlResponse.setPayId(paymentResponse.getTransactionId());
		xmlResponse.setRawXMLResponse(refundPaymentResponse);
		return xmlResponse;
	}

	public static IPGConfigsDTO getIPGConfigs(String merchantID, String userName, String password) {
		IPGConfigsDTO oIPGConfigsDTO = new IPGConfigsDTO();
		oIPGConfigsDTO.setMerchantID(merchantID);
		oIPGConfigsDTO.setUsername(userName);
		oIPGConfigsDTO.setPassword(password);

		return oIPGConfigsDTO;
	}

	public static HttpClient composeHttpClient(String keystoreName, String keystorePassword, String userName, String password)
			throws GeneralSecurityException, IOException {
		String keystoreLocation = PlatformConstants.getConfigRootAbsPath() + "/repository/modules/paymentbroker/" + keystoreName;
		URL storeUrl = new File(keystoreLocation).toURI().toURL();
		ProtocolSocketFactory socketFactory = new AuthSSLProtocolSocketFactory(storeUrl, keystorePassword, null, null);
		Protocol.registerProtocol("https", new Protocol("https", socketFactory, 443));

		// Prepare the communication to engine endpoint
		HttpClient client = new HttpClient();
		client.getParams().setParameter("http.auth.credential-charset", "UTF-8");
		Credentials defaultcreds = new UsernamePasswordCredentials(userName, password);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);

		return client;
	}
}