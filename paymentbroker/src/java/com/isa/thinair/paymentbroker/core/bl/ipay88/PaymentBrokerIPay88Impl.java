package com.isa.thinair.paymentbroker.core.bl.ipay88;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;

/**
 * The PaymentBrokerIPay88Impl class to get Request Data
 * 
 * extends PaymentBrokerIPay88Template
 * 
 */
public class PaymentBrokerIPay88Impl extends PaymentBrokerIPay88Template {

	private static Log log = LogFactory.getLog(PaymentBrokerIPay88Impl.class);

	private static GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

	/**
	 * This method is to create IPGRequestResultsDTO
	 * 
	 * @param ipgRequestDTO
	 *            IPGRequestDTO containing the payment gateway request data
	 * 
	 * @return IPGRequestResultsDTO containing the payment gateway result data
	 */
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		String finalAmount = PaymentBrokerUtils.getFormattedThousandAmount(ipgRequestDTO.getAmount(), 2);

		String refNo = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		postDataMap.put(IPay88Request.MERCHANTCODE, getMerchantCode());
		postDataMap.put(IPay88Request.PAYMENTID, "");
		postDataMap.put(IPay88Request.REFNO, refNo);
		postDataMap.put(IPay88Request.AMOUNT, finalAmount);
		postDataMap.put(IPay88Request.CURRENCY, ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode());
		postDataMap.put(IPay88Request.PRODDESC, globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME));
		postDataMap.put(IPay88Request.USERNAME, ipgRequestDTO.getContactFirstName() + " " + ipgRequestDTO.getContactLastName());
		postDataMap.put(IPay88Request.USEREMAIL, ipgRequestDTO.getContactEmail());
		postDataMap.put(IPay88Request.USERCONTACT, ipgRequestDTO.getContactMobileNumber());
		postDataMap.put(IPay88Request.REMARK, "");

		Map<String, String> dataMapForHash = new LinkedHashMap<String, String>();
		dataMapForHash.put(IPay88Utils.MERCHANTKEY, getMerchantKey());
		dataMapForHash.put(IPay88Request.MERCHANTCODE, getMerchantCode());
		dataMapForHash.put(IPay88Request.REFNO, refNo);
		// . and , should be removed from the amount to calculate hash
		dataMapForHash.put(IPay88Request.AMOUNT, finalAmount.replaceAll("[.,]", ""));
		dataMapForHash.put(IPay88Request.CURRENCY, ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode());

		try {
			String signature = IPay88Utils.computeSHA1(dataMapForHash);
			postDataMap.put(IPay88Request.SIGNATURE, signature);
		} catch (Exception e) {
			log.debug("Error computing SHA-1 : " + e.toString());
			throw new ModuleException(e.getMessage());
		}

		postDataMap.put(IPay88Request.RESPONSEURL, ipgRequestDTO.getReturnUrl());
		postDataMap.put(IPay88Request.BACKENDURL, getBackendURL());

		String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();

		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), refNo, new Integer(
				ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"), strRequestParams + ","
				+ sessionID, "", ipgRequestDTO.getIpgIdentificationParamsDTO().getFQIPGConfigurationName(), null, false);

		String requestDataForm = PaymentBrokerUtils.getPostInputDataFormHTML(postDataMap, getIpgURL());

		if (log.isDebugEnabled()) {
			log.debug("IPAY88 Request Data : " + strRequestParams + ", sessionID : " + sessionID);
		}

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(requestDataForm);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setAccelAeroTransactionRef(refNo);
		ipgRequestResultsDTO.setPostDataMap(postDataMap);

		return ipgRequestResultsDTO;
	}

}
