/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.bl.migs3party;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.sun.net.ssl.SSLContext;
import com.sun.net.ssl.X509TrustManager;

/**
 * Migs3 query related reference implementation
 * 
 * @author Indika
 * 
 */
public class PaymentBrokerMIGS3QueryDR extends PaymentBrokerTemplate implements PaymentQueryDR {

	private static Log log = LogFactory.getLog(PaymentBrokerMIGS3QueryDR.class);
	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	private static final String IPG_URL_PART_QUERYDR = "vpcdps";

	private static final Object QUERY_COMMAND = "queryDR";
	private static final String TRANSACTION_RESULT_SUCCESS = "0";
	private static final String PAY_APPROVED_MESSAGE = "Approved";

	public static SSLSocketFactory s_sslSocketFactory = null;

	static {
		X509TrustManager s_x509TrustManager = new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[] {};
			}

			public boolean isClientTrusted(X509Certificate[] chain) {
				return true;
			}

			public boolean isServerTrusted(X509Certificate[] chain) {
				return true;
			}
		};

		java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		try {
			SSLContext context = SSLContext.getInstance("TLS");
			context.init(null, new X509TrustManager[] { s_x509TrustManager }, null);
			s_sslSocketFactory = context.getSocketFactory();
		} catch (Exception e) {
			log.error(e);
			throw new RuntimeException(e.getMessage());
		}
	}

	/**
	 * This method is for performing a Form POST operation from input data parameters.
	 * 
	 * @param pnr
	 * @param temporyPaymentId
	 * @param vpc_Host
	 * @param dataFields
	 * @param isCharge
	 * @return
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 * @throws ModuleException
	 */
	private Object[] doPost(String pnr, int temporyPaymentId, String vpc_Host, Map dataFields, boolean isCharge)
			throws IOException, UnsupportedEncodingException, ModuleException {

		InputStream is;
		OutputStream os;
		int vpc_Port = 443;
		String fileName = "";
		boolean useSSL = false;
		StringBuffer postData = new StringBuffer();
		MIGS3PRequest migsReq = new MIGS3PRequest();
		migsReq.appendQueryFields(postData, null, dataFields);

		// determine if SSL encryption is being used
		if (vpc_Host.substring(0, 8).equalsIgnoreCase("HTTPS://")) {
			useSSL = true;
			// remove 'HTTPS://' from host URL
			vpc_Host = vpc_Host.substring(8);
			// get the filename from the last section of vpc_URL
			fileName = vpc_Host.substring(vpc_Host.lastIndexOf("/"));
			// get the IP address of the VPC machine
			vpc_Host = vpc_Host.substring(0, vpc_Host.lastIndexOf("/"));
		}

		// use the next block of code if using a proxy server
		if (useProxy) {
			Socket s = new Socket(proxyHost, Integer.parseInt(proxyPort));
			os = s.getOutputStream();
			is = s.getInputStream();
			// use next block of code if using SSL encryption
			if (useSSL) {
				String msg = "CONNECT " + vpc_Host + ":" + vpc_Port + " HTTP/1.0\r\n" + "User-Agent: HTTP Client\r\n\r\n";
				os.write(msg.getBytes());
				byte[] buf = new byte[4096];
				int len = is.read(buf);
				String res = new String(buf, 0, len);

				// check if a successful HTTP connection
				if (res.indexOf("200") < 0) {
					throw new IOException("Proxy would now allow connection - " + res);
				}

				// write output to VPC
				SSLSocket ssl = (SSLSocket) s_sslSocketFactory.createSocket(s, vpc_Host, vpc_Port, true);
				ssl.startHandshake();
				os = ssl.getOutputStream();
				// get response data from VPC
				is = ssl.getInputStream();
				// use the next block of code if NOT using SSL encryption
			} else {
				fileName = vpc_Host;
			}
			// use the next block of code if NOT using a proxy server
		} else {
			// use next block of code if using SSL encryption
			if (useSSL) {
				Socket s = s_sslSocketFactory.createSocket(vpc_Host, vpc_Port);
				os = s.getOutputStream();
				is = s.getInputStream();
				// use next block of code if NOT using SSL encryption
			} else {
				Socket s = new Socket(vpc_Host, vpc_Port);
				os = s.getOutputStream();
				is = s.getInputStream();
			}
		}

		String req = "POST " + fileName + " HTTP/1.0\r\n" + "User-Agent: HTTP Client\r\n"
				+ "Content-Type: application/x-www-form-urlencoded\r\n" + "Content-Length: " + postData.toString().length()
				+ "\r\n\r\n" + postData.toString();

		log.debug("Query DR Request : " + req);

		String serviceType = bundle.getString("SERVICETYPE_QUERY");

		CreditCardTransaction ccTransaction = auditTransaction(pnr, getMerchantId(),
				(String) dataFields.get(MIGS3PRequest.VPC_MERCH_TXN_REF), new Integer(temporyPaymentId), serviceType, req, "",
				getPaymentGatewayName(), null, false);

		os.write(req.getBytes());
		String res = new String(MIGSPaymentUtils.readAll(is));

		// check if a successful connection
		if (res.indexOf("200") < 0) {
			throw new IOException("Connection Refused - " + res);
		}

		if (res.indexOf("404 Not Found") > 0) {
			throw new IOException("File Not Found Error - " + res);
		}

		int resIndex = res.indexOf("\r\n\r\n");
		String body = res.substring(resIndex + 4, res.length());
		log.debug("Query DR Response file body : " + body);
		return new Object[] { body, new Integer(ccTransaction.getTransactionRefNo()) };
	}

	/**
	 * Performs the query operation for a executed tpt_id, pnr
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce query(CreditCardTransaction oCreditCardTransaction, IPGConfigsDTO ipgConfigsDTO,
			PaymentBrokerInternalConstants.QUERY_CALLER caller) throws ModuleException {

		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status = null;
		String errorSpecification = null;

		Map fields = new HashMap();
		fields.put(MIGS3PRequest.VPC_VERSION, ipgConfigsDTO.getVersion());
		fields.put(MIGS3PRequest.VPC_COMMAND, QUERY_COMMAND);
		fields.put(MIGS3PRequest.VPC_ACC_CODE, ipgConfigsDTO.getAccessCode()); // check the access code
		fields.put(MIGS3PRequest.VPC_MERCH_TXN_REF, oCreditCardTransaction.getTempReferenceNum()); // Search Criteria
		fields.put(MIGS3PRequest.VPC_MERCHANT, ipgConfigsDTO.getMerchantID());
		fields.put(MIGS3PRequest.VPC_USER, ipgConfigsDTO.getUsername());
		fields.put(MIGS3PRequest.VPC_PASSWORD, ipgConfigsDTO.getPassword());

		Object[] resQS = null;
		String queryResponse = null;
		Integer paymentBrokerRefNo;

		String ipgURLPay = getIpgURL() + IPG_URL_PART_QUERYDR;

		try {
			// create a URL connection to the Virtual Payment Client
			resQS = doPost(oCreditCardTransaction.getTransactionReference(), oCreditCardTransaction.getTemporyPaymentId(),
					ipgURLPay, fields, false);
			queryResponse = (String) resQS[0];
			paymentBrokerRefNo = (Integer) resQS[1];

		} catch (UnsupportedEncodingException uee) {
			log.error(uee);
			throw new ModuleException("paymentbroker.error.unsupportedencoding");
		} catch (IOException ioe) {
			log.error(ioe);
			throw new ModuleException("paymentbroker.error.ioException");
		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		log.debug("Refund Response : " + queryResponse);

		// create a hash map for the response data
		Map responseFields = MIGSPaymentUtils.createMapFromResponse(queryResponse);
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		MIGS3PResponse migsResp = new MIGS3PResponse();
		migsResp.setReponse(responseFields);

		if (TRANSACTION_RESULT_SUCCESS.equals(migsResp.getTxnResponseCode())
				&& PAY_APPROVED_MESSAGE.equals(migsResp.getMessage())) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			errorCode = "";
			transactionMsg = migsResp.getMessage();
			tnxResultCode = 1;
			errorSpecification = " " + caller.toString();
			sr.setSuccess(true);
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, PaymentConstants.SERVICE_RESPONSE_QUERYDR);
			sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE, migsResp.getTransactionNo());
		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			errorCode = PaymentResponseCodesUtil.getFilteredErrorCodeMOTO(migsResp);
			transactionMsg = PaymentResponseCodesUtil.getResponseDescription(migsResp.getTxnResponseCode());
			tnxResultCode = 0;
			errorSpecification = status + " " + transactionMsg + " " + migsResp.getMessage() + "-" + caller.toString();
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
		}

		updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), migsResp.toString(), errorSpecification,
				migsResp.getAuthorizeID(), migsResp.getTransactionNo(), tnxResultCode);

		sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, migsResp.getAuthorizeID());
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);

		log.debug("FINISH TRANSACTION - Process the VPC Response Data ");

		return sr;
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		PaymentBrokerManager.validateIPGIdentificationParams(this.ipgIdentificationParamsDTO);
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

}
