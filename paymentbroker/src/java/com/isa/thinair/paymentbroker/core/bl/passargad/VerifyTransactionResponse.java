package com.isa.thinair.paymentbroker.core.bl.passargad;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "result", "resultMessage"})
@XmlRootElement(name = "actionResult")
public class VerifyTransactionResponse {
public static String TRUE = "True";
	
	public static String FALSE = "False";
	
	private String result;
	
	private String resultMessage;

	public String getResult() {
		return result;
	}

	@XmlElement(name = "result")
	public void setResult(String result) {
		this.result = result;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	@XmlElement(name = "resultMessage")
	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
	
	
}
