package com.isa.thinair.paymentbroker.core.bl.mcpg;

public class MCPGRequest {

	public static final String INVOICE_NUMBER = "invnum";

	public static final String AMOUNT = "trnamt";

	public static final String ORDER_INFO = "param_1";

	public MCPGRequest() {

	}

}
