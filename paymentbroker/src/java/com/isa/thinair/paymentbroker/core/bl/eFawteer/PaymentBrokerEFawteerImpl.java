package com.isa.thinair.paymentbroker.core.bl.eFawteer;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.ConfirmReservationUtil;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.constants.EFawateerConstants;
import com.isa.thinair.paymentbroker.api.dto.*;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigDecimal;
import java.util.*;

public class PaymentBrokerEFawteerImpl extends PaymentBrokerTemplate implements PaymentBroker {
	private static final Log log = LogFactory.getLog(PaymentBrokerEFawteerImpl.class);

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr,
			AppIndicatorEnum appIndicator, TnxModeEnum tnxMode) throws ModuleException {
		DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);
		defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
				PaymentBrokerInternalConstants.MessageCodes.REFUND_OPERATION_NOT_SUPPORTED);
		return defaultServiceResponse;
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr,
			AppIndicatorEnum appIndicator, TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData)
			throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO()
				.loadTransaction(creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText())
				.equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID,
					creditCardTransaction.getAidCccompnay());
			return sr;
		}
		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr,
			AppIndicatorEnum appIndicator, TnxModeEnum tnxMode) throws ModuleException {
		return null;
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr,
			AppIndicatorEnum appIndicator, TnxModeEnum tnxMode) throws ModuleException {
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		IPGRequestResultsDTO resultsDTO = new IPGRequestResultsDTO();
		Boolean isVerified = EFawateerPaymentUtils
				.isSignatureValid(ipgRequestDTO.getRequestData(), ipgRequestDTO.getSignature(), getKeystoreName());
		resultsDTO.setRequestData(getMerchantId());
		resultsDTO.setSignatureVerified(isVerified);
		return resultsDTO;

	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO,
			List<CardDetailConfigDTO> configDataList) throws ModuleException {
		return null;

	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO)
			throws ModuleException {

		String signature = EFawateerPaymentUtils.getSignature(ipgResponseDTO.getResponseData(), getSecureKeyLocation());
		ipgResponseDTO.setSignature(signature);
		return ipgResponseDTO;
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		return null;
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection creditCardPayment, boolean refundFlag)
			throws ModuleException {
		return null;
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		return null;
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		return null;
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		return null;
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		return null;
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		String pmtStatus = receiptyMap.get(PaymentConstants.STATUS);
		boolean isDuplicateExists = PaymentBrokerUtils.getPaymentBrokerDAO()
				.isDuplicateTransactionExists(receiptyMap.get(EFawateerConstants.JOEBPPSTRX));
		boolean isDuplicateTrxExists = PaymentBrokerUtils.getPaymentBrokerDAO()
				.isDuplicateBankTransactionExists(receiptyMap.get(EFawateerConstants.BANKTRXID));
		if (pmtStatus.equalsIgnoreCase(EFawateerConstants.PMT_NEW) && !isDuplicateExists && !isDuplicateTrxExists) {
			String pnr = receiptyMap.get(CommandParamNames.PNR);
			String fqIPGConfigurationName = ipgIdentificationParamsDTO.getFQIPGConfigurationName();
			if (!BeanUtils.isNull(PaymentBrokerUtils.getPaymentBrokerDAO()
					.getLatestTempTransactionID(pnr, fqIPGConfigurationName,
							ReservationInternalConstants.TempPaymentTnxTypes.INITIATED))) {
				int tempPayID = PaymentBrokerUtils.getPaymentBrokerDAO()
						.getLatestTempTransactionID(pnr, fqIPGConfigurationName,
								ReservationInternalConstants.TempPaymentTnxTypes.INITIATED);
				CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTmpPayID(tempPayID);
				ReservationBD reservationBD = ReservationModuleUtils.getReservationBD();
				String status = "";

				Date requestTime = reservationBD.getPaymentRequestTime(oCreditCardTransaction.getTemporyPaymentId());
				Date now = Calendar.getInstance().getTime();
				String strResponseTime = CalendarUtil.getTimeDifference(requestTime, now);
				long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(requestTime, now);

				oCreditCardTransaction.setComments(strResponseTime);
				oCreditCardTransaction.setResponseTime(timeDiffInMillis);
				oCreditCardTransaction.setTransactionResultCode(PaymentConstants.TRANSACTION_RESULT_CODE);
				oCreditCardTransaction.setAidCccompnay(receiptyMap.get(EFawateerConstants.BANKTRXID));
				oCreditCardTransaction.setDeferredResponseText(receiptyMap.get(EFawateerConstants.REQUEST));
				oCreditCardTransaction.setTransactionId(receiptyMap.get(EFawateerConstants.JOEBPPSTRX));

				PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);


				IPGQueryDTO ipgQueryDTO = reservationBD.getTemporyPaymentInfo(oCreditCardTransaction.getTransactionRefNo());

				if (ipgQueryDTO != null) {

					BigDecimal reqDueAmount = AccelAeroCalculator
							.parseBigDecimal(Double.parseDouble(receiptyMap.get(EFawateerConstants.DUE_AMOUNT)));
					BigDecimal dueAmount = ipgQueryDTO.getAmount();
					if (ConfirmReservationUtil.checkAmountsWithInRange(reqDueAmount, dueAmount)) {

					  /* Update with payment response details */
						LCCClientPnrModesDTO modes = new LCCClientPnrModesDTO();
						modes.setPnr(ipgQueryDTO.getPnr());
						ipgQueryDTO.setPaymentType(PaymentType.CARD_GENERIC);
						ipgQueryDTO.setAidCompany(receiptyMap.get(EFawateerConstants.BANKTRXID));
						ipgQueryDTO.setTransacationId(receiptyMap.get(EFawateerConstants.JOEBPPSTRX));
						ipgQueryDTO.setAuthorizationNo(receiptyMap.get(EFawateerConstants.JOEBPPSTRX));

						// for dry interline validation fix
						ipgQueryDTO.setIpgIdentificationParamsDTO(ipgIdentificationParamsDTO);

						if (ipgIdentificationParamsDTO.getIpgId() == null) {
							ipgIdentificationParamsDTO.setIpgId(ipgResponseDTO.getPaymentBrokerRefNo());
						}

						if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(ipgQueryDTO.getPnrStatus())
								|| ReservationInternalConstants.ReservationStatus.CONFIRMED
								.equals(ipgQueryDTO.getPnrStatus())) {
							try {
								Reservation reservation = PaymentBrokerModuleUtil.getReservationBD()
										.getReservation(modes, null);
								status = reservation.getStatus();
							} catch (ModuleException e) {
								log.error(" ((o)) CommonsDataAccessException::getReservation " + e.getCause());
							}

							if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(status)
									|| ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(status)) {
								if (ConfirmReservationUtil.isConfirmationSuccess(ipgQueryDTO,
										ReservationInternalConstants.ReservationStatus.CONFIRMED
												.equals(ipgQueryDTO.getPnrStatus()))) {
									log.info("Reservation confirmation successful:" + ipgResponseDTO
											.getPaymentBrokerRefNo());
								} else {
									log.debug("[handleDeferredResponse()] Refunding Payment -> Confirmation failed.");
									throw new ModuleException("Confirmation failed for: " + pnr);
								}
							}
						} else {
							log.error("The reservation has been canceled.");
							throw new ModuleException("Reservation has been canceled");
						}
					} else {
						log.error("The due amounts do not match");
						throw new ModuleException("The due amount in the request is wrong");
					}
				} else {
					log.error(
							"PaymentBrokerEFawateer temp transaction not found:" + ipgResponseDTO.getPaymentBrokerRefNo());
					throw new ModuleException("Temporary transaction details could not be found");
				}
			}
			else{
				log.error("The reservation has already been confirmed");
				throw new ModuleException("Could not make any more payments for this reservation");
			}
		} else {
			log.error("PaymentBrokerEFawateer status change is not a new one");
			throw new ModuleException("Payment notification is not new or unique");
		}
		log.info("[PaymentBrokerEFawateerImpl::handleDeferredResponse()] End");
	}

	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
}
