package com.isa.thinair.paymentbroker.core.bl.qiwi;

import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPRequest;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPResponse;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.bl.saman.PaymentBrokerSamanQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.platform.api.ServiceResponce;

public class PaymentBrokerQiwiQueryDR extends PaymentBrokerTemplate implements PaymentQueryDR {

	private static Log log = LogFactory.getLog(PaymentBrokerSamanQueryDR.class);

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();
	
	@Override
	public ServiceResponce query(CreditCardTransaction oCreditCardTransaction,
			IPGConfigsDTO ipgConfigsDTO, QUERY_CALLER caller)
			throws ModuleException {
		
		log.debug("[PaymentBrokerQiwiQueryDR::query()] Begin");

		String errorSpecification = null;
		String merchantTxnReference = oCreditCardTransaction.getTempReferenceNum();
		String queryParamString = "RefNo:" + oCreditCardTransaction.getTransactionId() + ",MerchantId:"
				+ getMerchantId();

		CreditCardTransaction ccTransaction = auditTransaction(oCreditCardTransaction.getTransactionReference(),
				getMerchantId(), merchantTxnReference, new Integer(oCreditCardTransaction.getTemporyPaymentId()),
				bundle.getString("SERVICETYPE_QUERY"), queryParamString, "", getPaymentGatewayName(), null, false);

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		log.debug("Query Request Params : " + queryParamString);
		
		Properties props = PaymentBrokerManager.getProperties(this.ipgIdentificationParamsDTO);
		
		QiwiPRequest verifyRequest = new QiwiPRequest();
		verifyRequest.setMerchantID(ipgConfigsDTO.getMerchantID());
		verifyRequest.setTransactionID(String.valueOf(oCreditCardTransaction.getTemporyPaymentId()));
		verifyRequest.setQiwiInvoiceStatusReqURL(props.getProperty(QiwiPRequest.INVOICE_STATUS_REQ_URL));
		verifyRequest.setLogin(props.getProperty(QiwiPRequest.LOGIN));
		verifyRequest.setPassword(props.getProperty(QiwiPRequest.PASSWORD));
		
		QiwiPResponse res = QiwiPaymentUtils.checkInvoiceStatus(verifyRequest);
		String status;
		
		int waitingTime = ipgConfigsDTO.getResponseWaitingTimeInSec();
		// keep waiting in intervals of 4 seconds
		waitingTime = (int) Math.ceil(waitingTime/4.0);
		
		for (int i = 0; i < waitingTime; i++) {
			
			if(res.getStatus().equals(QiwiPRequest.WAITING)){
				//*********************** FIX ME *************************
				log.debug("[PaymentBrokerQiwiQueryDR::query()] Qiwi temporary Thread Sleep : Start " + i);
				// Qiwi is having a 2,3 second delay in changing the invoice status. 
				// This delay is buying qiwi few seconds to change the invoice status from "waiting" to "paid" in successful ONLINE payments.
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					log.debug("[PaymentBrokerQiwiQueryDR::query()] Qiwi temporary Thread Sleep : End " + i);
				//*********************** FIX ME *************************
					
				res = QiwiPaymentUtils.checkInvoiceStatus(verifyRequest);	
			}else{
				break;
			}			
		}
		
		
		if(res.isPaidInvoice()){
			log.debug("[PaymentBrokerQiwiQueryDR::query() : invoice is not paid yet]  invoice ID :" + res.getBillId() );
			
			status = IPGResponseDTO.STATUS_ACCEPTED;
			sr.setSuccess(true);
			sr.addResponceParam(PaymentConstants.AMOUNT, res.getAmount());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentConstants.SERVICE_RESPONSE_QUERYDR);
			sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE, oCreditCardTransaction.getTransactionId());			
			
		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			String error = QiwiPaymentUtils.getErrorInfo(res);

			errorSpecification = "Status:" + res.getStatus() + " Error code:" + res.getError() + " Error Desc:" + error;
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, error);
		}
		
		
		updateAuditTransactionByTnxRefNo(ccTransaction.getTransactionRefNo(), status, errorSpecification, oCreditCardTransaction.getTransactionId(),
				oCreditCardTransaction.getTransactionId(), Integer.parseInt(res.getError()));

		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, oCreditCardTransaction.getTransactionId());
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, res.getError());

		log.debug("[PaymentBrokerQiwiQueryDR::query() : Query Status :  " + status + "] ");
		log.debug("[PaymentBrokerQiwiQueryDR::query()] End");

		return sr;
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		PaymentBrokerManager.validateIPGIdentificationParams(this.ipgIdentificationParamsDTO);
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

}
