package com.isa.thinair.paymentbroker.core.bl.ogone;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.dto.XMLResponse;

public class PaymentBrokerOgoneQueryDR extends PaymentBrokerTemplate implements PaymentQueryDR {

	private static Log log = LogFactory.getLog(PaymentBrokerOgoneQueryDR.class);

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	protected static final String AUTHORIZATION_SUCCESS = "5";

	protected static final String PAYMENT_SUCCESS = "9";

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		PaymentBrokerManager.validateIPGIdentificationParams(this.ipgIdentificationParamsDTO);
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	@Override
	public ServiceResponce query(CreditCardTransaction oCreditCardTransaction, IPGConfigsDTO ipgConfigsDTO, QUERY_CALLER caller)
			throws ModuleException {

		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status = null;
		String errorSpecification = null;

		String merchantTxnReference = oCreditCardTransaction.getTempReferenceNum();

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		postDataMap.put(OgoneRequest.PSPID, ipgConfigsDTO.getMerchantID());
		if (ipgConfigsDTO.getUsername() != null && !ipgConfigsDTO.getUsername().equals("")) {
			postDataMap.put(OgoneRequest.USERID, getUser());
			postDataMap.put(OgoneRequest.PSWD, getPassword());
		}
		postDataMap.put(OgoneRequest.ORDERID, merchantTxnReference);
		String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

		// Add other required parameters to create the post request.
		postDataMap.put("PROXY_HOST", getProxyHost());
		postDataMap.put("PROXY_PORT", getProxyPort());
		postDataMap.put("USE_PROXY", isUseProxy() ? "Y" : "N");
		postDataMap.put("URL", getIpgURL());

		CreditCardTransaction ccTransaction = auditTransaction(oCreditCardTransaction.getTransactionReference(), getMerchantId(),
				merchantTxnReference, new Integer(oCreditCardTransaction.getTemporyPaymentId()),
				bundle.getString("SERVICETYPE_QUERY"), strRequestParams, "", getPaymentGatewayName(), null, false);

		XMLResponse response;
		try {
			response = OgonePaymentUtils.getOgoneXMLResponse(postDataMap);
		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		log.debug("Query Response : " + response.toString());

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		if (response.getPayId() != null && !response.getPayId().equals("") && !response.getStatus().equals("0")
				&& (AUTHORIZATION_SUCCESS.equals(response.getStatus()) || PAYMENT_SUCCESS.equals(response.getStatus()))) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			errorCode = "";
			transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
			tnxResultCode = 1;
			errorSpecification = " " + caller.toString();
			sr.setSuccess(true);
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, PaymentConstants.SERVICE_RESPONSE_QUERYDR);
			sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE, oCreditCardTransaction.getTransactionId());
			
			//FIXME: This is a temporary hack for IBE payment failure reported by AARESAA-12031
			sr.addResponceParam(PaymentConstants.ORDER_ID, response.getOrderId());
			sr.addResponceParam(PaymentConstants.CURRENCY, response.getCurrency());
			sr.addResponceParam(PaymentConstants.PAYMENT_METHOD, response.getPaymentMethod());
		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			errorCode = OgonePaymentUtils.getFilteredErrorCode(response.getStatus(), response.getNcError());
			transactionMsg = OgonePaymentUtils.getResponseStatusDescription(response.getStatus(), response.getNcError());
			tnxResultCode = 0;
			errorSpecification = "Status:" + status + " Error code:" + errorCode + " Transaction status:" + response.getStatus()
					+ " Transaction error details:" + response.getNcErrorPlus() + " Caller:" + caller.toString();
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
		}

		updateAuditTransactionByTnxRefNo(ccTransaction.getTransactionRefNo(), response.toString(), errorSpecification,
				response.getAcceptance(), response.getPayId() + ":" + response.getPayIdSub(), tnxResultCode);

		sr.setResponseCode(String.valueOf(ccTransaction.getTransactionRefNo()));
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, response.getAcceptance());
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
		sr.addResponceParam(PaymentConstants.STATUS, response.getStatus());
		sr.addResponceParam(PaymentConstants.AMOUNT, response.getAmount());

		log.debug("FINISH QUERY TRANSACTION ");

		return sr;
	}

}
