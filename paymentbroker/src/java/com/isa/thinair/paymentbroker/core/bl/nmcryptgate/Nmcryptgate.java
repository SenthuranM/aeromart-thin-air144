package com.isa.thinair.paymentbroker.core.bl.nmcryptgate;

/**
 * @author Srikantha
 * 
 */
public class Nmcryptgate {

	public native String displayHelloWorld();

	public native int nmcryptgateVersion();

	public native int nmlistalloc();

	public native int nmlistfree(int VLIST);

	public native void nmurldecode(int VLIST, String parameters);

	public native String nmurlencode(int VLIST);

	public native void nmixdecode(int VLIST, String parameters);

	public native String nmixencode(int VLIST);

	public native String nmtagget(int VLIST, String tagName);

	public native String nmtaggetlim(int VLIST, String tagName, int searchFirstNmembers);

	public native int nmtagrem(int VLIST, String tagName);

	public native int nmtagadd(int VLIST, String tagName, String tagValueName);

	public native int nmtagset(int VLIST, String tagName, String newTagValueName);

	public native void nmlistrewind(int VLIST);

	public native String nmtagnext(int VLIST);

	public native int nmcryptgate(String username, String password, String service, int VLIST);

	public native String nmcryptgateStrerror();
}
