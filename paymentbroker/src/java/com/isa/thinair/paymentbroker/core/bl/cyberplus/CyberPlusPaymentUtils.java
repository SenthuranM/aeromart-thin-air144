package com.isa.thinair.paymentbroker.core.bl.cyberplus;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;

public class CyberPlusPaymentUtils {

	private static Log log = LogFactory.getLog(CyberPlusPaymentUtils.class);

	public static final String SEPARATOR = "+";

	public static final String COLON = ":";

	private static final String TRANSACTION_RESULT_SUCCESS = "00";

	private static final String ERROR_CODE_PREFIX = "cyberPlus.";

	public static final String API_ERROR_CODE_PREFIX = "cyberPlus.api.";

	private static final int TRANSID_MAX = 800000;

	public static final String DATE_FORMAT = "yyyyMMddHHmmss";

	public static String MODE_OF_SERVICE_REFUND_PREFIX = "-R";

	public static String MODE_OF_SERVICE_PAYMENT_PREFIX = "-P";

	/**
	 * This method is used to generate the HTTP POST request.
	 * 
	 * @param postDataMap
	 *            map containing request parameters
	 * @param ipgURL
	 *            URL to submit the form data
	 * @return String to post from html page.
	 */
	public static String getPostInputDataFormHTML(Map<String, String> postDataMap, String ipgURL) {
		StringBuffer sb = new StringBuffer();

		sb.append("<form name=\"paymentForm\" action=\"" + ipgURL + "\" method=\"post\">");
		for (String fieldName : postDataMap.keySet()) {
			sb.append("<input type=\"hidden\" name=\"" + fieldName + "\" value=\"" + postDataMap.get(fieldName) + "\"/>");
		}
		sb.append("</form>");

		return sb.toString();
	}

	/**
	 * User info will include app module and payment type. Use : as separator.
	 * 
	 * @param appIndicatorEnum
	 *            AppIndicatorEnum containing the module identifier
	 * @param mode
	 *            type of payment
	 * @return userInfo String containing the appropriate user info
	 */
	public static String getUserInfo(AppIndicatorEnum appIndicatorEnum, PaymentConstants.MODE_OF_SERVICE mode) {
		StringBuffer userInfo = new StringBuffer();
		userInfo.append(appIndicatorEnum.toString());

		if (mode.equals(PaymentConstants.MODE_OF_SERVICE.PAYMENT)) {
			userInfo.append(COLON).append(MODE_OF_SERVICE_PAYMENT_PREFIX);
		} else if (mode.equals(PaymentConstants.MODE_OF_SERVICE.REFUND)) {
			userInfo.append(COLON).append(MODE_OF_SERVICE_REFUND_PREFIX);
		}
		return userInfo.toString();
	}

	/**
	 * This function uses the returned status code retrieved from the Digital Response and returns an appropriate
	 * description for the code
	 * 
	 * @param vResponseCode
	 *            String containing the vads_auth_result
	 * @return description String containing the appropriate description
	 */
	public static String getResponseDescription(String vResponseCode) {

		String result = "";

		// check if a two digit response code
		if (vResponseCode.length() == 2) {
			int input = Integer.parseInt(vResponseCode);

			switch (input) {
			case 0:
				result = "transaction approved or successfully treated";
				break;
			case 2:
				result = "Contact the card issuer";
				break;
			case 3:
				result = "Invalid acceptor";
				break;
			case 4:
				result = "Keep the card";
				break;
			case 5:
				result = "Do not honor";
				break;
			case 7:
				result = "Keep the card, special conditions";
				break;
			case 8:
				result = "Approved after identification";
				break;
			case 12:
				result = "Invalid transaction";
				break;
			case 13:
				result = "Invalid amount";
				break;
			case 14:
				result = "Invalid holder number";
				break;
			case 30:
				result = "Format error";
				break;
			case 31:
				result = "Unknown buying organization identifier";
				break;
			case 33:
				result = "Expired card validity date";
				break;
			case 34:
				result = "Fraud suspected";
				break;
			case 41:
				result = "Lost card";
				break;
			case 43:
				result = "Stolen card";
				break;
			case 51:
				result = "Insufficient provision or exceeds credit";
				break;
			case 54:
				result = "Expired card validity date";
				break;
			case 56:
				result = "Card not in database";
				break;
			case 57:
				result = "Transaction not allowed for this holder";
				break;
			case 58:
				result = "Transaction not allowed from this terminal";
				break;
			case 59:
				result = "Fraud suspected";
				break;
			case 60:
				result = "The card acceptor must contact buyer";
				break;
			case 61:
				result = "Amount over withdrawal limits";
				break;
			case 63:
				result = "Does not abide to security rules";
				break;
			case 68:
				result = "Response not received or received too late";
				break;
			case 90:
				result = "System temporarily stopped";
				break;
			case 91:
				result = "Inaccessible card issuer";
				break;
			case 94:
				result = "Duplicated transaction";
				break;
			case 96:
				result = "Faulty system";
				break;
			case 97:
				result = "Global surveillance time out expired";
				break;
			case 98:
				result = "Unavailable server ; repeat network routing requested";
				break;
			case 99:
				result = "Instigator domain  incident";
				break;
			default:
				result = "Unable to be determined.";
			}

			return result;
		} else {
			return "No Value Returned.";
		}
	}

	/**
	 * This function uses the returned error code to get the error code description
	 * 
	 * @param errorCode
	 *            integer containing the WS API error code
	 * @param errorCode
	 *            integer containing the WS API error code
	 * @return description String containing the appropriate description
	 */
	public static String getWSAPIErrorDescription(int errorCode, int status) {

		String result = "";

		if (errorCode != 0) {
			switch (errorCode) {
			case 1:
				result = "Unauthorized action ";
				break;
			case 2:
				result = "Transaction not found";
				break;
			case 3:
				result = "Transaction not with the right status";
				break;
			case 4:
				result = "Concurrent update of the reservation detected. Please try again";
				break;
			case 5:
				result = "Internal Error. Wrong signature";
				break;
			case 10:
				result = "Internal Error. Wrong amount";
				break;
			case 11:
				result = "Internal Error. Wrong currency";
				break;
			case 12:
				result = "Internal Error. Unknown card type";
				break;
			case 13:
				result = "Card payment failed. Check card details.";
				break;
			case 14:
				result = "Card payment failed. Check card details(Required CVV)";
				break;
			case 15:
				result = "Internal Error. Unknown contract";
				break;
			case 16:
				result = "Card payment failed. Check card details.";
				break;
			case 50:
				result = "Card payment failed. Check card details(Invalid shopId parameter)";
				break;
			case 51:
				result = "Internal Error. Invalid transmissionDate parameter";
				break;
			case 52:
				result = "Internal Error. Invalid transactionId parameter";
				break;
			case 53:
				result = "Internal Error. Invalid ctxMode parameter";
				break;
			case 54:
				result = "Internal Error. Invalid comment parameter";
				break;
			case 57:
				result = "Internal Error. Invalid presentationDate parameter";
				break;
			case 58:
				result = "Internal Error. Invalid newTransactionId parameter";
				break;
			case 59:
				result = "Internal Error. Invalid validationMode parameter";
				break;
			case 60:
				result = "Internal Error. Invalid orderId parameter";
				break;
			case 61:
				result = "Internal Error. Invalid orderInfo parameter";
				break;
			case 62:
				result = "Internal Error. Invalid orderInfo2 parameter";
				break;
			case 63:
				result = "Internal Error. Invalid orderInfo3 parameter";
				break;
			case 64:
				result = "Internal Error. Invalid PaymentMethod parameter";
				break;
			case 65:
				result = "Internal Error. Invalid cardNetwork parameter";
				break;
			case 66:
				result = "Internal Error. Invalid contractNumber Parameter";
				break;
			case 67:
				result = "Internal Error. Invalid customerId parameter";
				break;
			case 68:
				result = "Internal Error. Invalid customerTitle parameter";
				break;
			case 69:
				result = "Internal Error. Invalid customerName parameter";
				break;
			case 70:
				result = "Internal Error. Invalid customerPhone parameter";
				break;
			case 71:
				result = "Internal Error. Invalid customerMail parameter";
				break;
			case 72:
				result = "Internal Error. Invalid customerAddress parameter";
				break;
			case 73:
				result = "Internal Error. Invalid customerZipCode parameter";
				break;
			case 74:
				result = "Internal Error. Invalid customerCity parameter";
				break;
			case 75:
				result = "Internal Error. Invalid customerCountry parameter";
				break;
			case 76:
				result = "Internal Error. Invalid customerLanguage parameter";
				break;
			case 77:
				result = "Internal Error. Invalid customerIP parameter";
				break;
			case 78:
				result = "Internal Error. Invalid customerSendMail parameter";
				break;
			default:
				result = "Unknown error";
				break;
			}
		} else if (result.equals("") && status != 0) {
			switch (status) {
			case 1:
				result = "To be validated";
				break;
			case 2:
				result = "To be forced, Contact issuer";
				break;
			case 3:
				result = "To be validated and authorized ";
				break;
			case 4:
				result = "Waiting for submission ";
				break;
			case 5:
				result = "Waiting for authorization";
				break;
			case 6:
				result = "Transaction details are already submitted";
				break;
			case 7:
				result = "Transaction details are expired";
				break;
			case 8:
				result = "Credit card details refused by Payment Gateway";
				break;
			case 9:
				result = "Cancelled transaction";
				break;
			case 10:
				result = "Waiting";
				break;
			case 11:
				result = "Being submitted";
				break;
			case 12:
				result = "Being authorized";
				break;
			case 13:
				result = "Failed";
				break;
			}
		}

		return result;
	}

	/**
	 * This method uses the 3DS vads_threeds_status retrieved from the Response and returns an appropriate description
	 * for this code.
	 * 
	 * @param vads_threeds_status
	 *            String containing the status code
	 * @return description String containing the appropriate description
	 */
	public static String getStatusDescription(String vStatus) {
		String result = "";
		if (vStatus != null && !vStatus.equals("")) {
			// Java cannot switch on a string so turn everything to a character
			char input = vStatus.charAt(0);
			switch (input) {
			case 'Y':
				result = "Successful.";
				break;
			case 'N':
				result = "Failed.";
				break;
			case 'U':
				result = "Authentication could not be performed. ";
				break;
			case 'A':
				result = "Proof of attempted authentication.";
				break;
			default:
				result = "Unable to be determined.";
				break;
			}
		} else {
			result = "null response";
		}
		return result;
	}

	public static String getSignature(Map fields, String certificate) {
		// create a list and sort it
		List fieldNames = new ArrayList(fields.keySet());
		Collections.sort(fieldNames);

		StringBuffer signature = new StringBuffer();
		Iterator itr = fieldNames.iterator();

		// move through the list and create signature
		while (itr.hasNext()) {
			String fieldName = (String) itr.next();
			String fieldValue = (String) fields.get(fieldName);
			if (fieldName != null
					&& fieldName.startsWith("vads_")
					&& !((fieldName.trim().equalsIgnoreCase("signature")) || (fieldName.trim()
							.equalsIgnoreCase(PaymentConstants.IPG_SESSION_ID)))) {
				// append the field
				signature.append(fieldValue);
				// add a '+' to the end.
				signature.append(SEPARATOR);
			}
		}
		// append the certificate at the end
		signature.append(certificate);

		return encode(signature.toString());
	}

	public static String createSignature(String certificate, Object... params) {

		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < params.length; i++) {
			if (i != 0) {
				builder.append(SEPARATOR);
			}
			if (params[i] != null) {
				if (params[i] instanceof Date) {
					Date date = (Date) params[i];
					SimpleDateFormat myDateFormat = new SimpleDateFormat("yyyyMMdd");
					builder.append(myDateFormat.format(date));
				} else if (params[i] instanceof Boolean) {
					if ((Boolean) params[i])
						builder.append(1);
					else
						builder.append(0);
				} else {
					builder.append(params[i]);
				}
			}
		}
		String signature = builder.toString() + SEPARATOR + certificate;
		return encode(signature);
	}

	public static String encode(String src) {
		try {
			MessageDigest md;
			md = MessageDigest.getInstance("SHA-1");

			byte bytes[] = src.getBytes("iso-8859-1");

			md.update(bytes, 0, bytes.length);
			byte[] sha1hash = md.digest();

			return convertToHex(sha1hash);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static String convertToHex(byte[] sha1hash) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < sha1hash.length; i++) {
			byte c = sha1hash[i];

			addHex(builder, (c >> 4) & 0xf);
			addHex(builder, c & 0xf);

		}
		return builder.toString();
	}

	private static void addHex(StringBuilder builder, int c) {
		if (c < 10)
			builder.append((char) (c + '0'));
		else
			builder.append((char) (c + 'a' - 10));
	}

	public static String getRequestDataAsString(Map<String, String> postDataMap) {
		StringBuffer sb = new StringBuffer();

		if (postDataMap != null) {
			for (String fieldName : postDataMap.keySet()) {
				if (sb.length() > 0) {
					sb.append(",");
				}
				sb.append(fieldName + " : " + postDataMap.get(fieldName));
			}
		}

		return sb.toString();
	}

	/**
	 * Returns the formatted amount
	 * 
	 * @param amount
	 * @return
	 */
	public static String getFormattedAmount(String amount, int noOfDecimalPoints) {
		if (SystemPropertyUtil.checkRemoveCardPaymentDecimals()) {
			amount = Long.toString(Math.round(Double.parseDouble(amount)));
		}

		BigDecimal value = AccelAeroCalculator.multiply(new BigDecimal(amount),
				AccelAeroCalculator.parseBigDecimal(Math.pow(10, noOfDecimalPoints)));
		return String.valueOf(value.intValue());
	}

	public static String getFormatedDate() {
		SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT);
		return formater.format(getZuluTime());
	}

	public static Date getZuluTime() {
		Calendar c = Calendar.getInstance();
		TimeZone z = c.getTimeZone();
		c.add(Calendar.MILLISECOND, -(z.getRawOffset() + z.getDSTSavings()));
		return c.getTime();
	}

	public static Date formatDate(String date, String format) {
		DateFormat formater = new SimpleDateFormat(format);
		Date formatedDate = new Date();
		try {
			formatedDate = formater.parse(date);
		} catch (ParseException e) {
			log.error("FATAL ERROR", e);
		}
		return formatedDate;
	}

	public static int getTransId(int identifier) {
		// vads_trans_id must necessarily range from 100000 to 899999.

		return 100000 + (identifier % TRANSID_MAX);
	}

	public static String getErrorCode(String authResult, String result) {
		String errorCode = "";
		if (authResult != null && !authResult.isEmpty() && !TRANSACTION_RESULT_SUCCESS.equals(authResult)) {
			errorCode = ERROR_CODE_PREFIX + authResult;
		} else if (result != null && !result.isEmpty() && !TRANSACTION_RESULT_SUCCESS.equals(result)) {
			errorCode = ERROR_CODE_PREFIX + result;
		}
		return errorCode;
	}

	/**
	 * This function uses the returned merchTnxRef to recreate module, siteId, date and transId It should be in format
	 * (W | C) + (x digit siteId) + (14 digit date) + (6 digit transId) + (2 letter payment type eg: -P, -R) separated
	 * by colons.
	 * 
	 * @param merchTnxRef
	 *            String containing the merchant transaction reference no
	 * @return breakDown String[] containing the breakdown
	 */
	public static String[] getBreakDown(String merchTnxRef) {
		String[] breakDown = new String[5];
		breakDown = merchTnxRef.split(COLON);

		return breakDown;
	}

	/**
	 * This method use to get the card cord MASTERCARD = 1; VISA = 2; CB = 5;
	 * 
	 * @param cardType
	 *            String containing the card type
	 * @return relevant card code
	 */
	public static int getCardCode(String cardType) {
		if (cardType.equals("MASTERCARD")) {
			return 1;
		} else if (cardType.equals("VISA")) {
			return 2;
		} else if (cardType.equals("CB")) {
			// using GENERIC card type for CB
			return 5;
		} else {
			// return GENERIC as default
			return 5;
		}
	}

	public static XMLGregorianCalendar getXMLGregorianCalFromDate(String date) {

		int year = Integer.valueOf(date.substring(0, 4));
		int month = Integer.valueOf(date.substring(4, 6));
		int day = Integer.valueOf(date.substring(6, 8));
		int hr = Integer.valueOf(date.substring(8, 10));
		int min = Integer.valueOf(date.substring(10, 12));
		int sec = Integer.valueOf(date.substring(12, 14));
		try {
			XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(year, month, day, hr, min, sec,
					0, 0);
			return xmlDate;
		} catch (DatatypeConfigurationException e) {
			throw new java.lang.Error(e);
		}
	}

	public static IPGConfigsDTO getIPGConfigs(String ctxMode) {
		IPGConfigsDTO oIPGConfigsDTO = new IPGConfigsDTO();
		oIPGConfigsDTO.setCtxMode(ctxMode);
		return oIPGConfigsDTO;
	}

	/**
	 * Returns the order information
	 * 
	 * @param pnr
	 * @param carrierCode
	 *            TODO
	 * @return
	 */
	public static String getOrderInfo(String pnr, String carrierCode) {
		return carrierCode + pnr;
	}
}
