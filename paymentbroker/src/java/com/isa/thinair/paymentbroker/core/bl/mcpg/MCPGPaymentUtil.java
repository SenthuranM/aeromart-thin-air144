package com.isa.thinair.paymentbroker.core.bl.mcpg;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MCPGPaymentUtil {

	private static Log log = LogFactory.getLog(MCPGPaymentUtil.class);

	private static final String ERROR_CODE_PREFIX = "mcpg.";

	public static String getMatchingTxnErrorCode(int responseCode) {

		String result = "";
		if (log.isDebugEnabled()) {
			log.debug("[MCPGPaymentUtil::getMatchingTxnErrorCode]Txn Error Code found :" + responseCode);
		}

		if (responseCode == 0) {
			result = ERROR_CODE_PREFIX + "00";
		} else if (responseCode == 5) {
			result = ERROR_CODE_PREFIX + "05";
		} else if (responseCode == 13) {
			result = ERROR_CODE_PREFIX + "13";
		} else if (responseCode == 19) {
			result = ERROR_CODE_PREFIX + "19";
		} else if (responseCode == 33) {
			result = ERROR_CODE_PREFIX + "33";
		} else if (responseCode == 43) {
			result = ERROR_CODE_PREFIX + "43";
		} else if (responseCode == 51) {
			result = ERROR_CODE_PREFIX + "51";
		} else if (responseCode == 55) {
			result = ERROR_CODE_PREFIX + "55";
		} else if (responseCode == 58) {
			result = ERROR_CODE_PREFIX + "58";
		} else if (responseCode == 61) {
			result = ERROR_CODE_PREFIX + "61";
		} else if (responseCode == 75) {
			result = ERROR_CODE_PREFIX + "75";
		} else if (responseCode == 78) {
			result = ERROR_CODE_PREFIX + "78";
		} else if (responseCode == 79) {
			result = ERROR_CODE_PREFIX + "79";
		} else if (responseCode == 80) {
			result = ERROR_CODE_PREFIX + "80";
		} else if (responseCode == 91) {
			result = ERROR_CODE_PREFIX + "91";
		} else {
			result = ERROR_CODE_PREFIX + "100";
		}
		return result;
	}

	public static String getResponseDescription(int responseCode) {

		String result = "";
		if (log.isDebugEnabled()) {
			log.debug("[MCPGPaymentUtil::getMatchingTxnErrorCode]Txn Error Code found :" + responseCode);
		}

		if (responseCode == 0) {
			result = "Approved";
		} else if (responseCode == 5) {
			result = "Card Declined";
		} else if (responseCode == 13) {
			result = "Invalid Amount";
		} else if (responseCode == 19) {
			result = "No response from bank";
		} else if (responseCode == 33) {
			result = "Expired Card";
		} else if (responseCode == 43) {
			result = "Pick up Card";
		} else if (responseCode == 51) {
			result = "Insufficient Balance";
		} else if (responseCode == 55) {
			result = "Incorrect PIN";
		} else if (responseCode == 58) {
			result = "Invalid Terminal ID";
		} else if (responseCode == 61) {
			result = "Exceeds amount limit";
		} else if (responseCode == 75) {
			result = "Excessive PIN Tries";
		} else if (responseCode == 78) {
			result = "Account not Found";
		} else if (responseCode == 79) {
			result = "Unable to Process, No Activity Allowed, Invalid Transaction";
		} else if (responseCode == 80) {
			result = "Synchronization Error";
		} else if (responseCode == 91) {
			result = "Bank is Down";
		} else {
			result = "No Value Returned";
		}
		return result;
	}

	/**
	 * Returns the order information
	 * 
	 * @param pnr
	 * @param merchantTxnId
	 * @param carrierCode
	 *            TODO
	 * @return
	 */
	public static String getOrderInfo(String pnr, String merchantTxnId, String carrierCode, String userIP) {
		// JIRA ID THINAIR-866 : requested by Byorn
		StringBuilder sb = new StringBuilder();
		userIP = (userIP != null ? "-" + userIP : "");
		return sb.append(carrierCode).append("-").append(pnr).append("-").append(merchantTxnId).append(userIP).toString();
	}
}
