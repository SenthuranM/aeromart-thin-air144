package com.isa.thinair.paymentbroker.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;

/**
 * @author Srikantha
 */
@Local
public interface PaymentBrokerServiceLocalDelegateImpl extends PaymentBrokerBD {

}
