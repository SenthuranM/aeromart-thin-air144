/**
 * 
 */
package com.isa.thinair.paymentbroker.core.bl.mtcew;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;

/**
 * @author IndikaA
 * 
 */
public class MTCPaymentUtils {

	private static Log log = LogFactory.getLog(MTCPaymentUtils.class);

	public static final String MESSAGE_DIGEST_ALGO = "MD5";

	public static final String CHAR_ENC = "UTF-8";

	private static final char[] HEX_TABLE = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D',
			'E', 'F' };

	/**
	 * This method is for sorting the fields and creating an MD5 secure hash.
	 * 
	 * @param fields
	 *            is a map of all the incoming hey-value pairs from the VPC
	 * @param buf
	 *            is the hash being returned for comparison to the incoming hash
	 */
	public static String hashAllFields(Map fields, String secureSecret) {

		// create a list and sort it
		List fieldNames = new ArrayList(fields.keySet());
		Collections.sort(fieldNames);

		// create a buffer for the md5 input and add the secure secret first
		StringBuffer buf = new StringBuffer();
		buf.append(secureSecret);

		// iterate through the list and add the remaining field values
		Iterator itr = fieldNames.iterator();

		while (itr.hasNext()) {
			String fieldName = (String) itr.next();
			if (fieldName != null
					&& !((fieldName.trim().equalsIgnoreCase("checksum")) || (fieldName.trim()
							.equalsIgnoreCase(PaymentConstants.IPG_SESSION_ID)))) {
				String fieldValue = (String) fields.get(fieldName);
				if ((fieldValue != null) && (fieldValue.length() > 0)) {
					buf.append(fieldValue);
				}
			}
		}

		MessageDigest md5 = null;
		byte[] ba = null;

		// create the md5 hash and UTF-8 encode it
		try {
			md5 = MessageDigest.getInstance(MESSAGE_DIGEST_ALGO);
			ba = md5.digest(buf.toString().getBytes(CHAR_ENC));
		} catch (Exception e) {
			log.error(" FATAL ERROR  ", e);
		} // wont happen

		return hex(ba);

	}

	/**
	 * Returns Hex output of byte array
	 */
	private static String hex(byte[] input) {
		// create a StringBuffer 2x the size of the hash array
		StringBuffer sb = new StringBuffer(input.length * 2);

		// retrieve the byte array data, convert it to hex
		// and add it to the StringBuffer
		for (int i = 0; i < input.length; i++) {
			sb.append(HEX_TABLE[(input[i] >> 4) & 0xf]);
			sb.append(HEX_TABLE[input[i] & 0xf]);
		}
		return sb.toString();
	}

	/**
	 * This method is for creating a byte array from input stream data.
	 * 
	 * @param is
	 *            - the input stream containing the data
	 * @return is the byte array of the input stream data
	 */
	public static byte[] readAll(InputStream is) throws IOException {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		int len;

		while (true) {
			len = is.read(buf);
			if (len < 0) {
				break;
			}
			baos.write(buf, 0, len);
		}

		return baos.toByteArray();
	}

	/**
	 * This method is for creating a Map from the response data string.
	 * 
	 * @param queryString
	 *            is the input String from POST data response
	 * @return is a Hashmap of Post data response inputs
	 */
	public static Map createMapFromResponse(String queryString) {
		Map map = new HashMap();
		StringTokenizer st = new StringTokenizer(queryString, "&");
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			int i = token.indexOf('=');
			if (i > 0) {
				try {
					String key = token.substring(0, i);
					String value = URLDecoder.decode(token.substring(i + 1, token.length()), MTCEWRequest.CHAR_ENC);
					map.put(key, value);
				} catch (Exception ex) {
					log.error("FATAL ERROR", ex);
					// Do Nothing and keep looping through data
				}
			}
		}
		return map;
	}

	/**
	 * Returns the formatted amount
	 * 
	 * @param amount
	 * @return
	 */
	public static String getFormattedAmount(String amount, int noOfDecimalPoints) {
		if (SystemPropertyUtil.checkRemoveCardPaymentDecimals()) {
			amount = Long.toString(Math.round(Double.parseDouble(amount)));
		}

		BigDecimal value = AccelAeroCalculator.multiply(new BigDecimal(amount),
				AccelAeroCalculator.parseBigDecimal(Math.pow(10, noOfDecimalPoints)));
		return String.valueOf(value.intValue());
	}

	/**
	 * Returns the VPC Tnx Number
	 * 
	 * @param ccTransaction
	 * @param previousCreditCardPayment
	 * @return
	 */
	public static String getVPCTnxNo(CreditCardTransaction ccTransaction, PreviousCreditCardPayment previousCreditCardPayment) {
		if (previousCreditCardPayment.getPgSpecificTxnNumber() != null) {
			return previousCreditCardPayment.getPgSpecificTxnNumber();
		} else {
			return ccTransaction.getTransactionId();
		}
	}

	/**
	 * Returns the order information
	 * 
	 * @param pnr
	 * @param merchantTxnId
	 * @param carrierCode
	 *            TODO
	 * @return
	 */
	public static String getOrderInfo(String pnr, String merchantTxnId, String carrierCode) {
		// JIRA ID THINAIR-866 : requested by Byorn
		return carrierCode + pnr;
	}

	/**
	 * Build IPGConfigsDTO on privided details
	 * 
	 * @param merchantID
	 * @param accessCode
	 * @param ipgURL
	 * @param userName
	 * @param password
	 * @return
	 */
	public static IPGConfigsDTO getIPGConfigs(String merchantID, String accessCode, String ipgURL, String userName,
			String password, String version) {
		IPGConfigsDTO oIPGConfigsDTO = new IPGConfigsDTO();
		oIPGConfigsDTO.setMerchantID(merchantID);
		oIPGConfigsDTO.setAccessCode(accessCode);
		oIPGConfigsDTO.setIpgURL(ipgURL);
		oIPGConfigsDTO.setUsername(userName);
		oIPGConfigsDTO.setPassword(password);
		oIPGConfigsDTO.setVersion(version);

		return oIPGConfigsDTO;
	}

	// /**
	// * @author indika
	// *
	// * @param timestamp
	// * @return
	// */
	// public static Date getDateFromCCTimeStamp(String timestamp){
	//
	// int year = Integer.parseInt(timestamp.substring(0,4));
	// int month = Integer.parseInt(timestamp.substring(4,6));
	// int date1 = Integer.parseInt(timestamp.substring(6,8));
	// int hour = Integer.parseInt(timestamp.substring(8,10));
	// int min = Integer.parseInt(timestamp.substring(10,12));
	// int sec = Integer.parseInt(timestamp.substring(12,14));
	//
	// Calendar cal = Calendar.getInstance();
	// cal.set(year,month,date1,hour,min,sec);
	// return cal.getTime();
	// }

}
