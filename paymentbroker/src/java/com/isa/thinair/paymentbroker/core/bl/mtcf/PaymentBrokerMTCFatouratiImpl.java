/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.bl.mtcf;

import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.utils.ConfirmReservationUtil;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.paymentbroker.api.constants.PaymentAPIConsts;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * The payment broker client implementation of MTC
 * 
 * @author Manjula
 * 
 *         TODO - Remove unused methods and Clean - 2013/May
 */
@SuppressWarnings("deprecation")
public class PaymentBrokerMTCFatouratiImpl extends PaymentBrokerTemplate implements PaymentBroker {

	private static Log log = LogFactory.getLog(PaymentBrokerMTCFatouratiImpl.class);

	private static final String IPG_URL_PART_PAY = "vpcpay";
	private static final String IPG_URL_PART_REFUND = "vpcdps";
	private static final String DEFAULT_TRANSACTION_CODE = "5";
	private static final String PAY_APPROVED_MESSAGE = "Approved";
	private static final String TRANSACTION_RESULT_SUCCESS = "0";
	private static final String PAY_COMMAND = "pay";
	private static final String REFUND_COMMAND = "refund";
	private static final String CAPTURE_COMMAND = "capture";

	protected static final String AUTHORIZATION_SUCCESS = "5";

	protected static final String PAYMENT_SUCCESS = "9";

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	public ServiceResponce capture(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");

	}

	/**
	 * Performs card refund operation
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		// FIXME - Refund API to be integrated
		return reverse(creditCardPayment, pnr, appIndicator, tnxMode);
	}

	/**
	 * Performs card payment operation
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	/**
	 * Performs reverse card payment
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	/**
	 * Performs Capture card payment
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	/**
	 * MTC Payment request parameters
	 * 
	 * @param ipgRequestDTO
	 *            IPG payment request
	 */
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		String pnrType = ipgRequestDTO.getSelectedSystem();
		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		String strStoreId = getMerchantId();
		String strLanguage = "en".toUpperCase();
		if ("fr".equals(ipgRequestDTO.getSessionLanguageCode())) {
			strLanguage = "fr".toUpperCase();
		}

		String strOfferURL = ipgRequestDTO.getOfferUrl();

		String strUpdateURL;
		String strBookURL;

		if (ipgRequestDTO.isServiceAppFlow()) {
			strBookURL = AppSysParamsUtil.getSecureIBEUrl() + PaymentAPIConsts.REDIRECT_CONTROLLER_MTC_OFFLINE_URL_SUFFIX;
			strUpdateURL = AppSysParamsUtil.getSecureIBEUrl() + PaymentAPIConsts.REDIRECT_CONTROLLER_MTC_LOAD_URL_SUFFIX;
		} else {
			strBookURL = ipgRequestDTO.getReturnUrl();
			strBookURL = strBookURL.replace("handleInterlineIPGResponse.action", PaymentAPIConsts.REDIRECT_CONTROLLER_MTC_OFFLINE_URL_SUFFIX);

			strUpdateURL = ipgRequestDTO.getReceiptUrl();
			strUpdateURL = strUpdateURL.replace("receiptIPGResponse.action", PaymentAPIConsts.REDIRECT_CONTROLLER_MTC_LOAD_URL_SUFFIX);
		}
		
		String strCartId = merchantTxnId + "-" + ipgRequestDTO.getPnr() + "-" + pnrType;

		String strPaymentType = "3";
		String strMerchantType = "3";
		String strAmountTX = ipgRequestDTO.getAmount();
		String strTotalamountCur = "";
		String strSymbolCur = "MAD";
		String strmerchantFatouratiCode = "1023";
		String strcreanceFatouratiCode = "01";

		String strBuyerName = ipgRequestDTO.getContactFirstName() + " " + ipgRequestDTO.getContactLastName();
		String strAddress = ipgRequestDTO.getContactAddressLine1() + " " + ipgRequestDTO.getContactAddressLine2();
		int expireInterval = (int) ipgRequestDTO.getExpireMTCOffLinePeriod();
		strAddress = strAddress.trim().isEmpty() ? "Not Available" : strAddress;
		String strCity = ipgRequestDTO.getContactCity().trim().isEmpty() ? "Not Available" : ipgRequestDTO.getContactCity();
		String strState = ipgRequestDTO.getContactState().trim().isEmpty() ? "Not Available" : ipgRequestDTO.getContactState();
		String strCountry = StringUtils.trimToEmpty(ipgRequestDTO.getContactCountryCode());
		String strPostcode = ipgRequestDTO.getContactPostalCode() == null
				|| ipgRequestDTO.getContactPostalCode().trim().isEmpty() ? "Not Available" : ipgRequestDTO.getContactPostalCode();

		String strTel = "";

		String strExpirationDate = Integer.toString(expireInterval);

		if (!"".equals(ipgRequestDTO.getContactMobileNumber()) && !"--".equals(ipgRequestDTO.getContactMobileNumber())) {
			strTel = ipgRequestDTO.getContactMobileNumber();
		} else if (!"".equals(ipgRequestDTO.getContactPhoneNumber()) && !"--".equals(ipgRequestDTO.getContactPhoneNumber())) {
			strTel = ipgRequestDTO.getContactPhoneNumber();
		}
		String strEmail = StringUtils.trimToEmpty(ipgRequestDTO.getEmail());

		if (strEmail == null || "".equals(strEmail.trim())) {
			strEmail = getEmailDefault();
		}

		String ipgURLPay = getIpgURL();
		String strSLKSecretKey = getSecureSecret();
		String checksum = "";
		// String dataMD5 = ipgURLPay + strStoreId + strCartId + strCount + strAmountTX + strEmail + strSLKSecretKey;
		String dataMD5 = ipgURLPay + strStoreId + strmerchantFatouratiCode + strcreanceFatouratiCode + strCartId + strPaymentType
				+ strAmountTX + strExpirationDate + strEmail + strSLKSecretKey;

		try {
			String uRLEncodeData = URLEncoder.encode(dataMD5, "UTF-8");
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.reset();
			md.update((uRLEncodeData).getBytes());
			checksum = StringUtil.byteArrayToHexString(md.digest());
			checksum = checksum.toLowerCase();
		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("");
		}

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();
		postDataMap.put(MTFatouratiWRequest.MTC_ACTION_SLK, ipgURLPay);
		postDataMap.put(MTFatouratiWRequest.MTC_STORE_ID, strStoreId);
		postDataMap.put(MTFatouratiWRequest.MTC_LANGUE, strLanguage);
		postDataMap.put(MTFatouratiWRequest.MTC_OFFER_URL, strOfferURL);
		postDataMap.put(MTFatouratiWRequest.MTC_UPDATE_URL, strUpdateURL);
		postDataMap.put(MTFatouratiWRequest.MTC_BOOKING_URL, strBookURL);
		postDataMap.put(MTFatouratiWRequest.MTC_CART_ID, strCartId);
		postDataMap.put(MTFatouratiWRequest.MTC_AMOUNT_TX, strAmountTX);
		postDataMap.put(MTFatouratiWRequest.MTC_TOTAL_AMOUNT_CURRENCY, strTotalamountCur);
		postDataMap.put(MTFatouratiWRequest.MTC_SYMBOL_CURRENCY, strSymbolCur);
		postDataMap.put(MTFatouratiWRequest.MTC_BUYER_NAME, strBuyerName);
		postDataMap.put(MTFatouratiWRequest.MTC_ADDRESS, strAddress);
		postDataMap.put(MTFatouratiWRequest.MTC_CITY, strCity);
		postDataMap.put(MTFatouratiWRequest.MTC_STATE, strState);
		postDataMap.put(MTFatouratiWRequest.MTC_COUNTRY, strCountry);
		postDataMap.put(MTFatouratiWRequest.MTC_POST_CODE, strPostcode);
		postDataMap.put(MTFatouratiWRequest.MTC_TELEPHONE_NO, strTel);
		postDataMap.put(MTFatouratiWRequest.MTC_EMAIL_ID, strEmail);
		postDataMap.put(MTFatouratiWRequest.MTC_PAYMENT_TYPE, strPaymentType);
		postDataMap.put(MTFatouratiWRequest.MTC_MERCHANT_TYPE, strMerchantType);
		postDataMap.put(MTFatouratiWRequest.MTC_MERCHANT_FATOURATATI_CODE, strmerchantFatouratiCode);
		postDataMap.put(MTFatouratiWRequest.MTC_CREANCE_FATOURATATI_CODE, strcreanceFatouratiCode);
		postDataMap.put(MTFatouratiWRequest.MTC_EXPIRATION_DATE, strExpirationDate);
		postDataMap.put(MTFatouratiWRequest.MTC_CHECH_SUM, checksum);

		String strRequestParams = getRequestDataAsString(postDataMap);

		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();
		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), merchantId, merchantTxnId, new Integer(
				ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"), strRequestParams
				+ ",sessionID : " + sessionID, "", getPaymentGatewayName(), null, false);

		if (log.isDebugEnabled()) {
			log.debug("MTC Request Data : " + strRequestParams + ",sessionID : " + sessionID);
		}

		String requestDataForm = getPostInputDataFormHTML(postDataMap);

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(requestDataForm);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setSubmitMethod(IPGRequestResultsDTO.SUBMIT_METHOD_POST);
		ipgRequestResultsDTO.setPostDataMap(postDataMap);
		ipgRequestResultsDTO.setAccelAeroTransactionRef(strCartId);

		return ipgRequestResultsDTO;
	}

	private String getPostInputDataFormHTML(Map<String, String> postDataMap) {
		StringBuffer sb = new StringBuffer();

		sb.append("<form name=\"paymentForm\" action=\"" + postDataMap.get(MTFatouratiWRequest.MTC_ACTION_SLK)
				+ "\" method=\"post\">");
		for (String fieldName : postDataMap.keySet()) {
			if (!MTFatouratiWRequest.MTC_ACTION_SLK.equals(fieldName)) {
				sb.append("<input type=\"hidden\" name=\"" + fieldName + "\" value=\"" + postDataMap.get(fieldName) + "\"/>");
			}
		}
		sb.append("</form>");

		return sb.toString();
	}

	private String getRequestDataAsString(Map<String, String> postDataMap) {
		StringBuffer sb = new StringBuffer();

		if (postDataMap != null) {
			for (String fieldName : postDataMap.keySet()) {
				if (sb.length() > 0)
					sb.append(",");
				sb.append(fieldName + " : " + postDataMap.get(fieldName));
			}
		}

		return sb.toString();
	}

	/**
	 * Reads the response returns from the MIGS server
	 * 
	 * @param encryptedReceiptPay
	 *            the secured response returns from the IPG
	 */
	public IPGResponseDTO getReponseData(Map fields, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		log.debug("[PaymentBrokerMTCImpl::getReponseData()] Begin ");

		boolean errorExists = false;
		int tnxResultCode;
		String errorCode = "";
		String status;
		String errorSpecification = "";
		String merchantTxnReference;
		int cardType = 5; // default card type
		String hashValidated = null;
		String responseMismatch = "";

		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTmpPayID(ipgResponseDTO.getTemporyPaymentId());

		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time
		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

		MTCFatouratiResponse mtcResp = new MTCFatouratiResponse();
		mtcResp.setResponse(fields);

		log.debug("[PaymentBrokerMTCImpl::getReponseData()] Mid Response -" + mtcResp.getMtcOrderNum());
		hashValidated = MTCFatouratiResponse.VALID_HASH;

		if (mtcResp != null && !(mtcResp.getCartId().trim().equals(""))) {
			if (mtcResp.validateChecksum(getSecureSecret())) {
				hashValidated = MTCFatouratiResponse.VALID_HASH;
				errorExists = false;

				// TODO - Check request cart Id and response Id matches
			} else {
				// Secure Hash validation failed, add a data field to be
				// displayed later.
				errorExists = true;
				hashValidated = MTCFatouratiResponse.INVALID_HASH;

				// TODO - Integrate Query API
			}
		} else {
			// Transaction response is empty
			hashValidated = MTCFatouratiResponse.NO_VALUE;
			errorCode = DEFAULT_TRANSACTION_CODE;
		}

		merchantTxnReference = mtcResp.getCartId();
		cardType = getStandardCardType("Generic");

		if (mtcResp.getResponseType().equalsIgnoreCase("MTCGENARATEPAYMENTREFARANCE") && !errorExists) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			tnxResultCode = 0;
			errorSpecification = "" + responseMismatch;
		} else {
			// Note - No failure scenarios are sent by MTC
			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
		}

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setApplicationTransactionId(merchantTxnReference);
		ipgResponseDTO.setCardType(cardType);
		String mtcResponse = mtcResp.toString();

		updateAuditTransactionByTnxRefNo(oCreditCardTransaction.getTransactionRefNo(), mtcResponse, errorSpecification, "",
				mtcResp.getPaymentSeqID(), tnxResultCode);

		return ipgResponseDTO;
	}

	/**
	 * This method uses the 3DS verStatus retrieved from the Response and returns an appropriate description for this
	 * code.
	 * 
	 * @param vpc_VerStatus
	 *            String containing the status code
	 * @return description String containing the appropriate description
	 */
	private String getStatusDescription(String vStatus) {
		String result = "";
		if (vStatus != null && !vStatus.equals("")) {

			if (vStatus.equalsIgnoreCase("Unsupported") || vStatus.equals("No Value Returned")) {
				result = "3DS not supported or there was no 3DS data provided.";
			} else {
				// Java cannot switch on a string so turn everything to a character
				char input = vStatus.charAt(0);
				switch (input) {
				case 'Y':
					result = "The cardholder was successfully authenticated.";
					break;
				case 'E':
					result = "The cardholder is not enrolled.";
					break;
				case 'N':
					result = "The cardholder was not verified.";
					break;
				case 'U':
					result = "The cardholder's Issuer was unable to authenticate due to some system error at the Issuer.";
					break;
				case 'F':
					result = "There was an error in the format of the request from the merchant.";
					break;
				case 'A':
					result = "Authentication of your Merchant ID and Password to the ACS Directory Failed.";
					break;
				case 'D':
					result = "Error communicating with the Directory Server.";
					break;
				case 'C':
					result = "The card type is not supported for authentication.";
					break;
				case 'S':
					result = "The signature on the response received from the Issuer could not be validated.";
					break;
				case 'P':
					result = "Error parsing input from Issuer.";
					break;
				case 'I':
					result = "Internal Payment Server system error.";
					break;
				default:
					result = "Unable to be determined.";
					break;
				}
			}
		} else {
			result = "null response";
		}
		return result;
	}

	/**
	 * Returns Payment Gateway Name
	 */
	@Override
	public String getPaymentGatewayName() {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	/**
	 * Returns the card type as integer
	 * 
	 * @param card
	 *            the card
	 * @return card type
	 */
	private int getStandardCardType(String card) {
		// [1-MASTER, 2-VISA, 3-AMEX, 4-DINNERS, 5-GENERIC]
		int mapType = 5; // default card type
		// if (card != null && !card.equals("") && mapCardType.get(card) != null) {
		// String mapTypeStr = (String) mapCardType.get(card);
		// mapType = Integer.parseInt(mapTypeStr);
		// }
		return mapType;
	}

	/**
	 * Resolves partial payments [Invokes via a scheduler operation]
	 */
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		// TODO - Implement card configuration data
		return getRequestData(ipgRequestDTO);
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		if (AppSysParamsUtil.enableServerToServerMessages()) {
			int paymentBrokerRefNo = ipgResponseDTO.getPaymentBrokerRefNo();
			CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTmpPayID(ipgResponseDTO.getTemporyPaymentId());
			ipgResponseDTO.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
			// CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(paymentBrokerRefNo);
			if (oCreditCardTransaction != null) {
				MTCFatouratiResponse mtcResp = new MTCFatouratiResponse();

				mtcResp.setResponse(receiptyMap);
				if (mtcResp != null && !(mtcResp.getCartId().trim().equals(""))) {

					if ((mtcResp.validateChecksum(getSecureSecret()))) {
						/* if payment response does not process TransactionResultCode will be 0 */
						if (oCreditCardTransaction.getTransactionResultCode() != 1) {
							if (log.isDebugEnabled())
								log.debug(mtcResp.toString());
							/* Save Card Transaction response Details */
							ReservationBD reservationBD = ReservationModuleUtils.getReservationBD();
							Date requestTime = reservationBD.getPaymentRequestTime(oCreditCardTransaction.getTemporyPaymentId());
							Date now = Calendar.getInstance().getTime();
							String strResponseTime = CalendarUtil.getTimeDifference(requestTime, now);
							long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(requestTime, now);

							oCreditCardTransaction.setComments(strResponseTime);
							oCreditCardTransaction.setResponseTime(timeDiffInMillis);
							oCreditCardTransaction.setTransactionResultCode(1);
							oCreditCardTransaction.setDeferredResponseText(mtcResp.toString());
							// oCreditCardTransaction.setAidCccompnay(mtcResp.getAcceptance());

							PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

							IPGQueryDTO ipgQueryDTO = reservationBD.getTemporyPaymentInfo(oCreditCardTransaction
									.getTransactionRefNo());
							ipgQueryDTO.setPaymentType(getPaymentType(getStandardCardType("Genaric")));

							if (ipgQueryDTO != null) {
								boolean isResConfirmationSuccess = false;
								if (AppSysParamsUtil.isConfirmOnholdReservationByServerToServerMessages()) {
									isResConfirmationSuccess = ConfirmReservationUtil
											.isReservationConfirmationSuccess(ipgQueryDTO);

									if (!isResConfirmationSuccess) {
										throw new ModuleException("Reservation.Confirmation.Faild");
									}
								}

							}
						} else {
							throw new ModuleException("Transaction.Already.Updated");
						}

					} else {
						log.error("Invalid credit card transation:" + ipgResponseDTO.getPaymentBrokerRefNo());
					}
				} else {
					log.error("Invalid credit card transation:" + ipgResponseDTO.getPaymentBrokerRefNo());
				}
			}
		}

	}

	private PaymentType getPaymentType(int cardID) {

		PaymentType paymentType = PaymentType.CARD_GENERIC;
		switch (cardID) {
		case 1:
			paymentType = PaymentType.CARD_MASTER;
			break;
		case 2:
			paymentType = PaymentType.CARD_VISA;
			break;
		case 3:
			paymentType = PaymentType.CARD_AMEX;
			break;
		case 4:
			paymentType = PaymentType.CARD_DINERS;
			break;
		case 6:
			paymentType = PaymentType.CARD_CMI;
			break;
		}
		return paymentType;
	}

	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

}
