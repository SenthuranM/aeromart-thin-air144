package com.isa.thinair.paymentbroker.core.bl.qiwi;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.ConfirmReservationUtil;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPRequest;
import com.isa.thinair.paymentbroker.api.dto.qiwi.QiwiPResponse;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.bl.ogone.OgonePaymentUtils;
import com.isa.thinair.paymentbroker.core.bl.telemoney.PaymentBrokerTeleMoneyImpl;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class PaymentBrokerQiwiTemplate extends PaymentBrokerTemplate implements
		PaymentBroker {

	protected static final String VERIFICATION_AMOUNTS_ERROR = "qiwi.payment.mismatch";
	public static final String CARD_TYPE_GENERIC = "Genaric";
	private static Log log = LogFactory.getLog(PaymentBrokerTeleMoneyImpl.class);

	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();
	
	private String invoiceStatusReqURL;
	private String createInvoiceURL;
	private String invoiceTimeout;
	private String redirectURL;
	private String invoiceCreationMethod;
	private String invoiceStatusCheckMethod;
	private String login;
	private String password;
	private String refundURL;
	private String refundStatusReqURL;
	private int qiwiResponseWaitingTimeInSec;
	private String invoiceNotifPassword;

	public String getRefundURL() {
		return refundURL;
	}

	public void setRefundURL(String refundURL) {
		this.refundURL = refundURL;
	}

	public String getRefundStatusReqURL() {
		return refundStatusReqURL;
	}

	public void setRefundStatusReqURL(String refundStatusReqURL) {
		this.refundStatusReqURL = refundStatusReqURL;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the invoiceNotifPassword
	 */
	public String getInvoiceNotifPassword() {
		return invoiceNotifPassword;
	}

	/**
	 * @param invoiceNotifPassword the invoiceNotifPassword to set
	 */
	public void setInvoiceNotifPassword(String invoiceNotifPassword) {
		this.invoiceNotifPassword = invoiceNotifPassword;
	}

	@Override
	public ServiceResponce refund(CreditCardPayment creditCardPayment,
			String pnr, AppIndicatorEnum appIndicator, TnxModeEnum tnxMode)
			throws ModuleException {
	
		log.debug("[PaymentBrokerQiwiECommerceImpl::refund()] Begin");
		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status;
		String errorSpecification;
		String merchTnxRef;

		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

		String refundAmountStr = creditCardPayment.getAmount();
		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		if (creditCardPayment.getPreviousCCPayment() != null && getRefundEnabled() != null && getRefundEnabled().equals("true")) {
			
			if (log.isDebugEnabled()) {
				log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
				log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
				log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
				log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
				log.debug("CC Old pay Formatted sAmount : " + refundAmountStr);
				log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());
			}

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				log.info("[PaymentBrokerQiwiECommerceImpl::refund()] Looking for OtherIPGs ....");
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}
			
			
			Properties ipgProps =  	PaymentBrokerUtils.getPaymentBrokerBD().getProperties(creditCardPayment.getIpgIdentificationParamsDTO());
			
			// qiwi refund request
			QiwiPRequest qiwiReq = new QiwiPRequest();
			qiwiReq.setMerchantID(ipgProps.getProperty("merchantId"));
			qiwiReq.setQiwiInvoiceStatusReqURL(ipgProps.getProperty(QiwiPRequest.INVOICE_STATUS_REQ_URL));
			qiwiReq.setLogin(ipgProps.getProperty(QiwiPRequest.LOGIN));
			qiwiReq.setPassword(ipgProps.getProperty(QiwiPRequest.PASSWORD));
			qiwiReq.setRefundURL(ipgProps.getProperty(QiwiPRequest.REFUND_URL));
			qiwiReq.setAmount(QiwiPaymentUtils.getFormattedAmount(refundAmountStr));
			qiwiReq.setTransactionID(String.valueOf(ccTransaction.getTemporyPaymentId()));
			
			QiwiPResponse res = QiwiPaymentUtils.checkInvoiceStatus(qiwiReq);
			log.info("[PaymentBrokerQiwiECommerceImpl::refund()] Invoice Status : " + res.toString());
			
			if(res.getStatus().equals(QiwiPRequest.PAID)){
				
				merchTnxRef = ccTransaction.getTempReferenceNum();
				String refundID = QiwiPaymentUtils.getUniqueRefundID();
				
				qiwiReq.setRefundID(refundID);
				qiwiReq.setTransactionID(String.valueOf(ccTransaction.getTemporyPaymentId())); // if tempTnx is null, then it is not put on onhold

				CreditCardTransaction ccNewTransaction = auditTransaction(pnr, getMerchantId(), refundID,
						creditCardPayment.getTemporyPaymentId(), bundle.getString("SERVICETYPE_REFUND"), qiwiReq.toString(),
						"", getPaymentGatewayName(), null, false);

				QiwiPResponse response = QiwiPaymentUtils.initiateRefund(qiwiReq);
				QiwiPResponse refundConf = QiwiPaymentUtils.checkRefundStatus(qiwiReq);
				Integer paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());
				
				log.info("[PaymentBrokerQiwiECommerceImpl::refund()] Refund Status : " + refundConf.toString());
				
				if(response.getStatus().equals(QiwiPRequest.SUCCESS) && refundConf.getStatus().equals(QiwiPRequest.SUCCESS)){
					
					errorCode = "";
					transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
					status = IPGResponseDTO.STATUS_ACCEPTED;
					tnxResultCode = 1;
					errorSpecification = "";
					sr.setSuccess(true);
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,	PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
					
				} else {
					
					System.err.println("[PaymentBrokerQiwiECommerceImpl::refund()] " + QiwiPaymentUtils.getErrorInfo(response));						
					status = IPGResponseDTO.STATUS_REJECTED;
					errorCode = response.getError();
					transactionMsg = QiwiPaymentUtils.getErrorInfo(response);
					tnxResultCode = 0;
					errorSpecification = "Status:" + status + " Error code:" + errorCode + " Error Desc:" + transactionMsg;
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
			
				}				
				
				updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), response.toString(), errorSpecification,
						response.getResultCode(), response.getBillId(), tnxResultCode);
				
				sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, response.getBillId());
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
				
			} else {
				System.err.println("Invoice has not been paid! \n Cannot Refund.");
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);
			}			

		} else {
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);
		}
		
		log.debug("[PaymentBrokerQiwiECommerceImpl::refund()] End");
		return sr;
	}

	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment,
			String pnr, AppIndicatorEnum appIndicator, TnxModeEnum tnxMode,
			List<CardDetailConfigDTO> cardDetailConfigData)
			throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	@Override
	public Properties getProperties(){
		Properties props = super.getProperties();
		props.put(QiwiPRequest.INVOICE_TIMEOUT_PERIOD, PlatformUtiltiies.nullHandler(invoiceTimeout));	
		props.put(QiwiPRequest.INVOICE_STATUS_REQ_URL, PlatformUtiltiies.nullHandler(invoiceStatusReqURL));	
		props.put(QiwiPRequest.CREATE_INVOICE_URL, PlatformUtiltiies.nullHandler(createInvoiceURL));	
		props.put(QiwiPRequest.REDIRECT_URL, PlatformUtiltiies.nullHandler(redirectURL));	
		props.put(QiwiPRequest.INVOICE_CREATION_METHOD, PlatformUtiltiies.nullHandler(invoiceCreationMethod));
		props.put(QiwiPRequest.LOGIN,PlatformUtiltiies.nullHandler(login));
		props.put(QiwiPRequest.PASSWORD, PlatformUtiltiies.nullHandler(password));
		props.put(QiwiPRequest.SERVER_TO_SERVER_RES_PASSWORD, PlatformUtiltiies.nullHandler(invoiceNotifPassword));
		props.put(QiwiPRequest.REFUND_URL, PlatformUtiltiies.nullHandler(refundURL));
		props.put(QiwiPRequest.REFUND_STATUS_REQ_URL, PlatformUtiltiies.nullHandler(refundStatusReqURL));
		props.put(QiwiPRequest.QIWI_RESPONSE_WAITING_TIME_IN_SEC, PlatformUtiltiies.nullHandler(qiwiResponseWaitingTimeInSec));
		
		return props;
	}
	
	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment,
			String pnr, AppIndicatorEnum appIndicator, TnxModeEnum tnxMode)
			throws ModuleException {
		
		String refundEnabled = getRefundEnabled();
		ServiceResponce reverseResult = null;

		if (refundEnabled != null && refundEnabled.equals("false")) {

			DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);
			return defaultServiceResponse;
		}
		reverseResult = refund(creditCardPayment, pnr, appIndicator, tnxMode);
		return reverseResult;
	}

	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment,
			String pnr, AppIndicatorEnum appIndicator, TnxModeEnum tnxMode)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO)
			throws ModuleException {
		return null;
	}
	
		
	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO,
			List<CardDetailConfigDTO> configDataList) throws ModuleException {
		return getRequestData(ipgRequestDTO);
	}

	@Override
	public IPGResponseDTO getReponseData(Map receiptyMap,
			IPGResponseDTO ipgResponseDTO) throws ModuleException {
		return null;
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO,
			boolean refundFlag) throws ModuleException {
		log.info("[PaymentBrokerQiwiECommerceImpl::resolvePartialPayments()] Begin");

		Iterator itColIPGQueryDTO = colIPGQueryDTO.iterator();

		Collection<Integer> oResultIF = new ArrayList<Integer>();
		Collection<Integer> oResultIP = new ArrayList<Integer>();
		Collection<Integer> oResultII = new ArrayList<Integer>();
		Collection<Integer> oResultIS = new ArrayList<Integer>();

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		IPGQueryDTO ipgQueryDTO;
		CreditCardTransaction oCreditCardTransaction;
		PaymentQueryDR query;
		PreviousCreditCardPayment oPrevCCPayment;
		CreditCardPayment oCCPayment;

		while (itColIPGQueryDTO.hasNext()) {
			ipgQueryDTO = (IPGQueryDTO) itColIPGQueryDTO.next();

			oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgQueryDTO.getPaymentBrokerRefNo());
			query = getPaymentQueryDR();

			try {
				ServiceResponce serviceResponce = query.query(oCreditCardTransaction,
						OgonePaymentUtils.getIPGConfigs(getMerchantId(), getUser(), getPassword()),
						PaymentBrokerInternalConstants.QUERY_CALLER.SCHEDULER);

				log.info("[PaymentBrokerQiwiECommerceImpl::resolvePartialPayments()] serviceResponce :" + serviceResponce.getResponseCode());
				
				// If successful payment remains at Bank
				if (serviceResponce.isSuccess()) {
					if (refundFlag) {
						// Do refund
						String txnNo = PlatformUtiltiies.nullHandler(serviceResponce
								.getResponseParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE));

						oPrevCCPayment = new PreviousCreditCardPayment();
						oPrevCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
						oPrevCCPayment.setPgSpecificTxnNumber(txnNo);

						oCCPayment = new CreditCardPayment();
						oCCPayment.setPreviousCCPayment(oPrevCCPayment);
						oCCPayment.setAmount(ipgQueryDTO.getAmount().toString());
						oCCPayment.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
						oCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
						oCCPayment.setIpgIdentificationParamsDTO(ipgQueryDTO.getIpgIdentificationParamsDTO());
						oCCPayment.setPnr(oCreditCardTransaction.getTransactionReference());
						oCCPayment.setTemporyPaymentId(oCreditCardTransaction.getTemporyPaymentId());

						log.info("[PaymentBrokerQiwiECommerceImpl::resolvePartialPayments()] Refunding ....");
						ServiceResponce srRev = refund(oCCPayment, oCCPayment.getPnr(), oCCPayment.getAppIndicator(),
								oCCPayment.getTnxMode());

						if (srRev.isSuccess()) {
							// Change State to 'IS'
							oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
							log.info("Status moving to IS:" + oCreditCardTransaction.getTemporyPaymentId());
						} else {
							// Change State to 'IF'
							oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
							log.info("Status moving to IF:" + oCreditCardTransaction.getTemporyPaymentId());
						}
					} else {
						// Change State to 'IP'
						oResultIP.add(oCreditCardTransaction.getTemporyPaymentId());
						log.info("Status moving to IP:" + oCreditCardTransaction.getTemporyPaymentId());
					}
				}
				// No payment detail at bank
				else {
					// No Payments exist
					// Update status to 'II'
					oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
					log.info("Status moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
				}
			} catch (ModuleException e) {
				log.error("Scheduler::ResolvePayment FAILED!!! " + e.getMessage(), e);
			}
		}

		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF, oResultIF);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_II, oResultII);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IP, oResultIP);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS, oResultIS);

		sr.setSuccess(true);

		log.info("[PaymentBrokerSamanTemplate::resolvePartialPayments()] End");
		return sr;

	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(
			IPGPrepPaymentDTO params) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params)
			throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap)
			throws ModuleException {
		// TODO Auto-generated method stubdsff
		return null;
	}

	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap,
			IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO)
			throws ModuleException {
		
		log.info("[PaymentBrokerQiwiECommerceImpl::handleDeferredResponse()] Begin");
		
		if (AppSysParamsUtil.enableServerToServerMessages() && AppSysParamsUtil.isConfirmOnholdReservationByServerToServerMessages()) {			

			int temporyPaymentId = ipgResponseDTO.getTemporyPaymentId();
			CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTmpPayID(temporyPaymentId);
			ReservationBD reservationBD = ReservationModuleUtils.getReservationBD();
			boolean isReservationConfirmationSuccess = false;
			
			if (oCreditCardTransaction != null) {

				String billId = receiptyMap.get(QiwiPRequest.TNX_ID);
				String status = receiptyMap.get(QiwiPResponse.STATUS);
				String error = receiptyMap.get(QiwiPRequest.ERROR);
				String amount = receiptyMap.get(QiwiPRequest.AMOUNT);
				String user = receiptyMap.get(QiwiPRequest.USER);
				String ccy = receiptyMap.get(QiwiPResponse.CCY);
				String comment = receiptyMap.get(QiwiPRequest.COMMENT);
				String command = receiptyMap.get(QiwiPResponse.COMMAND);
				
				TempPaymentTnx tmpPayment = ReservationModuleUtils.getReservationBD().loadTempPayment(oCreditCardTransaction.getTemporyPaymentId());			
				
				log.info("[handleDeferredResponse()] TempPaymentTnx " + tmpPayment.getTnxId() + " is loaded");
				
				QiwiPResponse res = new QiwiPResponse();
				res.setBillId(billId);
				res.setStatus(status);
				res.setError(error);
				res.setAmount(amount);
				res.setUser(user);
				res.setCurrency(ccy);
				res.setComment(comment);
				res.setCommand(command);
				
				BigDecimal igpCurrencyAmount = tmpPayment.getPaymentCurrencyAmount();
				BigDecimal responseAmount = new BigDecimal(res.getAmount());
				
				if (tmpPayment != null && (igpCurrencyAmount.compareTo(responseAmount) == 0) && res.getCurrency().equals(tmpPayment.getPaymentCurrencyCode())) {
					
					if (oCreditCardTransaction.getTransactionResultCode() != 1) {
						
						ServiceResponce serviceResponce = null;
						if("true".equals(receiptyMap.get("qiwiOfflineMode"))){							
							serviceResponce = new DefaultServiceResponse(true);
						}else{							
							PaymentQueryDR query = getPaymentQueryDR();
							serviceResponce = query.query(oCreditCardTransaction,
									OgonePaymentUtils.getIPGConfigs(getMerchantId(), getUser(), getPassword()),
									PaymentBrokerInternalConstants.QUERY_CALLER.SCHEDULER);
						}
						log.info("[handleDeferredResponse()] serviceResponce " + serviceResponce.getResponseCode());
						
						if (serviceResponce.isSuccess()) {
							/* Save Card Transaction response Details */
							Date requestTime = reservationBD.getPaymentRequestTime(oCreditCardTransaction.getTemporyPaymentId());
							Date now = Calendar.getInstance().getTime();
							String strResponseTime = CalendarUtil.getTimeDifference(requestTime, now);
							long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(requestTime, now);
		
							oCreditCardTransaction.setComments(strResponseTime);
							oCreditCardTransaction.setResponseTime(timeDiffInMillis);
							oCreditCardTransaction.setTransactionResultCode(1);
							oCreditCardTransaction.setDeferredResponseText(PaymentBrokerUtils.getRequestDataAsString(receiptyMap));
							oCreditCardTransaction.setTransactionId(String.valueOf(temporyPaymentId));
							
							PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);
		
							IPGQueryDTO ipgQueryDTO = reservationBD.getTemporyPaymentInfo(oCreditCardTransaction.getTransactionRefNo());							
							
							if(ipgQueryDTO != null){																									
								/* Update with payment response details */
								
								LCCClientPnrModesDTO modes = new LCCClientPnrModesDTO();
								modes.setPnr(ipgQueryDTO.getPnr());
								modes.setRecordAudit(false);
								
								ipgQueryDTO.setPaymentType(getPaymentType(getStandardCardType(CARD_TYPE_GENERIC)));
								
								/** NO RESERVATION OR CANCELED RESERVATION */
								if(!isReservationExist(ipgQueryDTO) 
										|| ReservationInternalConstants.ReservationStatus.CANCEL.equals(ipgQueryDTO.getPnrStatus()) ){
									
									Collection<IPGQueryDTO> col = new ArrayList<IPGQueryDTO>();
									col.add(ipgQueryDTO);
									
									/** Server to server response and realtime redirection from Qiwi might arrive almost the same time. 
									 * If so, Session data might be obsolete. Therefore loading the reservation again and checking
									 **/
									Reservation reservation = PaymentBrokerModuleUtil.getReservationBD().getReservation(modes, null);
									
									if(reservation == null || reservation.getStatus() == null || ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())){
										
										log.info("[handleDeferredResponse()] Refunding Payment -> No Reservation has been created :" + isReservationConfirmationSuccess);
										this.resolvePartialPayments(col, AppSysParamsUtil.isQueryDRRefund());
										ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_REJECTED);
										return;
									}									
								}
								
								/** ONHOLD RESERVATION */
								if(ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(ipgQueryDTO.getPnrStatus())){
									
									Reservation reservation = PaymentBrokerModuleUtil.getReservationBD().getReservation(modes, null);
									
									if(reservation != null && ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())){
										
										isReservationConfirmationSuccess = ConfirmReservationUtil.isReservationConfirmationSuccess(ipgQueryDTO);
										
										if(!isReservationConfirmationSuccess){
											Collection<IPGQueryDTO> col = new ArrayList<IPGQueryDTO>();
											col.add(ipgQueryDTO);
											
											log.debug("[handleDeferredResponse()] Refunding Payment -> Onhold confirmation failed :" + isReservationConfirmationSuccess);
											this.resolvePartialPayments(col, AppSysParamsUtil.isQueryDRRefund());
											ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_REJECTED);											
											return;
										}
										
										ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_ACCEPTED);
									}
								}
								
								/** ALREADY CONFIRMED RESERVATION */
								if(ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(ipgQueryDTO.getPnrStatus())){
									
									Reservation reservation = PaymentBrokerModuleUtil.getReservationBD().getReservation(modes, null);
									
									if(reservation != null && ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())){
										Collection<IPGQueryDTO> col = new ArrayList<IPGQueryDTO>();
										col.add(ipgQueryDTO);
										
										log.debug("[handleDeferredResponse()] Refunding Payment -> Reservation already confirmed :" + reservation.getPnr());
										this.resolvePartialPayments(col, AppSysParamsUtil.isQueryDRRefund());
										ipgResponseDTO.setStatus(IPGResponseDTO.STATUS_ACCEPTED);
										return;
									}
								}
								
							}
						} 
					}											
				}
	
			} else {
				log.error("Invalid credit card transation:" + ipgResponseDTO.getPaymentBrokerRefNo());
			}
			
		} else {
			log.error("Invalid credit card transation:" + ipgResponseDTO.getPaymentBrokerRefNo());
		}
	
		log.info("[PaymentBrokerQiwiECommerceImpl::handleDeferredResponse()] End");
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}
	
	@Override
	public boolean isEnableRefundByScheduler() {
		return true;
	}

	public String getInvoiceTimeout() {
		return invoiceTimeout;
	}

	public void setInvoiceTimeout(String invoiceTimeout) {
		this.invoiceTimeout = invoiceTimeout;
	}

	public String getCreateInvoiceURL() {
		return createInvoiceURL;
	}

	public void setCreateInvoiceURL(String createInvoiceURL) {
		this.createInvoiceURL = createInvoiceURL;
	}

	public String getInvoiceStatusReqURL() {
		return invoiceStatusReqURL;
	}

	public void setInvoiceStatusReqURL(String invoiceStatusReqURL) {
		this.invoiceStatusReqURL = invoiceStatusReqURL;
	}

	public String getRedirectURL() {
		return redirectURL;
	}

	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}

	public String getInvoiceCreationMethod() {
		return invoiceCreationMethod;
	}

	public void setInvoiceCreationMethod(String invoiceCreationMethod) {
		this.invoiceCreationMethod = invoiceCreationMethod;
	}

	public String getInvoiceStatusCheckMethod() {
		return invoiceStatusCheckMethod;
	}

	public void setInvoiceStatusCheckMethod(String invoiceStatusCheckMethod) {
		this.invoiceStatusCheckMethod = invoiceStatusCheckMethod;
	}

	protected int getStandardCardType(String card) {
		// [1-MASTER, 2-VISA, 3-AMEX, 4-DINNERS, 5-GENERIC, 6-OFFLINE]
		int mapType = 5; // default card type
		if (card != null && !card.equals("") && mapCardType.get(card) != null) {
			String mapTypeStr = (String) mapCardType.get(card);
			mapType = Integer.parseInt(mapTypeStr);
		}
		return mapType;
	}
	
	protected PaymentType getPaymentType(int cardID) {

		PaymentType paymentType = PaymentType.CARD_GENERIC;
		switch (cardID) {
		case 1:
			paymentType = PaymentType.CARD_MASTER;
			break;
		case 2:
			paymentType = PaymentType.CARD_VISA;
			break;
		case 3:
			paymentType = PaymentType.CARD_AMEX;
			break;
		case 4:
			paymentType = PaymentType.CARD_DINERS;
			break;
		case 5:
			paymentType = PaymentType.CARD_GENERIC;
			break;
		case 6:
			paymentType = PaymentType.CARD_CMI;
			break;
		}
		return paymentType;
	}

	/**
	 * @return the qiwiResponseWaitingTimeInSec
	 */
	public int getQiwiResponseWaitingTimeInSec() {
		return qiwiResponseWaitingTimeInSec;
	}

	/**
	 * @param qiwiResponseWaitingTimeInSec the qiwiResponseWaitingTimeInSec to set
	 */
	public void setQiwiResponseWaitingTimeInSec(int qiwiResponseWaitingTimeInSec) {
		this.qiwiResponseWaitingTimeInSec = qiwiResponseWaitingTimeInSec;
	}
	
	private static boolean isReservationExist(IPGQueryDTO ipgQueryDTO) {
		boolean isExist = false;
		if (ipgQueryDTO != null) {
			if (ipgQueryDTO.getPnr() != null && ipgQueryDTO.getPnr().trim().length() != 0) {
				isExist = true;
			}
		}
		return isExist;
	}
	
	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
}
