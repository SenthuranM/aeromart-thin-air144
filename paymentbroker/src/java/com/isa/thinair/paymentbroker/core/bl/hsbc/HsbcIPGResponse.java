/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.bl.hsbc;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * @author Asiri
 */
public class HsbcIPGResponse {

	private String merchTxnRef;

	private String orderinfo;

	private String merchantId;

	private String purchaseAmount;

	private String receiptNo;

	private String qsiResponseCode;

	private String authorizeID;

	private String batchNo;

	private String ipgTxnId;

	private String cardType;

	private String errorCode;

	private int txnResultCode;

	private String errorSpecification;

	private String status;

	private String message;

	/**
	 * @return the qsiResponseCode
	 */
	public String getQsiResponseCode() {
		return qsiResponseCode;
	}

	/**
	 * @param qsiResponseCode
	 *            the qsiResponseCode to set
	 */
	public void setQsiResponseCode(String qsiResponseCode) {
		this.qsiResponseCode = qsiResponseCode;
	}

	/**
	 * @return the authorizeID
	 */
	public String getAuthorizeID() {
		return authorizeID;
	}

	/**
	 * @param authorizeID
	 *            the authorizeID to set
	 */
	public void setAuthorizeID(String authorizeID) {
		this.authorizeID = authorizeID;
	}

	/**
	 * @return the batchNo
	 */
	public String getBatchNo() {
		return batchNo;
	}

	/**
	 * @param batchNo
	 *            the batchNo to set
	 */
	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * @param cardType
	 *            the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @return the ipgTxnId
	 */
	public String getIpgTxnId() {
		return ipgTxnId;
	}

	/**
	 * @param ipgTxnId
	 *            the ipgTxnId to set
	 */
	public void setIpgTxnId(String ipgTxnId) {
		this.ipgTxnId = ipgTxnId;
	}

	/**
	 * @return the merchantId
	 */
	public String getMerchantId() {
		return merchantId;
	}

	/**
	 * @param merchantId
	 *            the merchantId to set
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	/**
	 * @return the merchTxnRef
	 */
	public String getMerchTxnRef() {
		return merchTxnRef;
	}

	/**
	 * @param merchTxnRef
	 *            the merchTxnRef to set
	 */
	public void setMerchTxnRef(String merchTxnRef) {
		this.merchTxnRef = merchTxnRef;
	}

	/**
	 * @return the orderinfo
	 */
	public String getOrderinfo() {
		return orderinfo;
	}

	/**
	 * @param orderinfo
	 *            the orderinfo to set
	 */
	public void setOrderinfo(String orderinfo) {
		this.orderinfo = orderinfo;
	}

	/**
	 * @return the purchaseAmount
	 */
	public String getPurchaseAmount() {
		return purchaseAmount;
	}

	/**
	 * @param purchaseAmount
	 *            the purchaseAmount to set
	 */
	public void setPurchaseAmount(String purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}

	/**
	 * @return the receiptNo
	 */
	public String getReceiptNo() {
		return receiptNo;
	}

	/**
	 * @param receiptNo
	 *            the receiptNo to set
	 */
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode
	 *            the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the errorSpecification
	 */
	public String getErrorSpecification() {
		return errorSpecification;
	}

	/**
	 * @param errorSpecification
	 *            the errorSpecification to set
	 */
	public void setErrorSpecification(String errorSpecification) {
		this.errorSpecification = errorSpecification;
	}

	/**
	 * @return the txnResultCode
	 */
	public int getTxnResultCode() {
		return txnResultCode;
	}

	/**
	 * @param tnxResultCode
	 *            the tnxResultCode to set
	 */
	public void setTxnResultCode(int txnResultCode) {
		this.txnResultCode = txnResultCode;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}