package com.isa.thinair.paymentbroker.core.bl.cyberplus;

import java.util.ResourceBundle;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.paymentbroker.api.dto.CyberplusRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.CyberplusTransactionInfoDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerManager;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.wsclient.api.service.WSClientBD;

public class CyberPlusPaymentQueryDR extends PaymentBrokerTemplate implements PaymentQueryDR {

	private static Log log = LogFactory.getLog(CyberPlusPaymentQueryDR.class);
	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	private static final int TRANSACTION_RESULT_SUCCESS = 0;

	private static final int WAITING_FOR_SUBMISSION = 4;

	private static final int SUBMITTED = 6;

	@Override
	public ServiceResponce query(CreditCardTransaction oCreditCardTransaction, IPGConfigsDTO ipgConfigsDTO, QUERY_CALLER caller)
			throws ModuleException {
		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status = null;
		String errorSpecification = null;

		String merchantTxnReference = oCreditCardTransaction.getTempReferenceNum();
		String[] breakDown = CyberPlusPaymentUtils.getBreakDown(merchantTxnReference);
		int transactionId = Integer.valueOf(breakDown[3]);

		XMLGregorianCalendar xmlGregorianCal = CyberPlusPaymentUtils.getXMLGregorianCalFromDate(breakDown[2]);

		CyberplusRequestDTO cyberplusRequestDTO = new CyberplusRequestDTO();
		cyberplusRequestDTO.setShopId(getMerchantId());
		cyberplusRequestDTO.setTransmissionDate((xmlGregorianCal));
		cyberplusRequestDTO.setTransactionId(Integer.toString(transactionId));
		cyberplusRequestDTO.setSequenceNb(1);
		cyberplusRequestDTO.setCtxMode(ipgConfigsDTO.getCtxMode());
		cyberplusRequestDTO.setProxyHost(getProxyHost());
		cyberplusRequestDTO.setProxyPort(getProxyPort());

		String signature = CyberPlusPaymentUtils.createSignature(getSecureSecret(), cyberplusRequestDTO.getShopId(),
				CyberPlusPaymentUtils.formatDate(breakDown[2], CyberPlusPaymentUtils.DATE_FORMAT),
				cyberplusRequestDTO.getTransactionId(), cyberplusRequestDTO.getSequenceNb(), cyberplusRequestDTO.getCtxMode());

		cyberplusRequestDTO.setWsSignature(signature);

		CreditCardTransaction ccTransaction = auditTransaction(oCreditCardTransaction.getTransactionReference(), getMerchantId(),
				merchantTxnReference, new Integer(oCreditCardTransaction.getTemporyPaymentId()),
				bundle.getString("SERVICETYPE_QUERY"), cyberplusRequestDTO.toString(), "", getPaymentGatewayName(), null, false);

		CyberplusTransactionInfoDTO response;
		try {
			WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
			response = ebiWebervices.getInfo(cyberplusRequestDTO);
		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("paymentbroker.error.unknown");
		}

		log.debug("Query Response : " + response.toString());

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		if (TRANSACTION_RESULT_SUCCESS == response.getErrorCode()
				&& (WAITING_FOR_SUBMISSION == response.getTransactionStatus() || SUBMITTED == response.getTransactionStatus())) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			errorCode = "";
			transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
			tnxResultCode = 1;
			errorSpecification = " " + caller.toString();
			sr.setSuccess(true);
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, PaymentConstants.SERVICE_RESPONSE_QUERYDR);
			sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE, oCreditCardTransaction.getTransactionId());
		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			errorCode = CyberPlusPaymentUtils.API_ERROR_CODE_PREFIX + response.getErrorCode();
			transactionMsg = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
			errorSpecification = "Status:" + status + " Error code:" + errorCode + " Transaction status:"
					+ response.getTransactionStatus() + " Caller:" + caller.toString();
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
		}

		updateAuditTransactionByTnxRefNo(ccTransaction.getTransactionRefNo(), response.toString(), errorSpecification, "", "",
				tnxResultCode);

		sr.setResponseCode(String.valueOf(response.getTransactionStatus()));
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, oCreditCardTransaction.getAidCccompnay());
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);

		log.debug("FINISH QUERY TRANSACTION ");

		return sr;
	}

	public CyberplusTransactionInfoDTO queryDetails(CreditCardTransaction oCreditCardTransaction, IPGConfigsDTO ipgConfigsDTO,
			QUERY_CALLER caller) throws ModuleException {

		String merchantTxnReference = oCreditCardTransaction.getTempReferenceNum();
		String[] breakDown = CyberPlusPaymentUtils.getBreakDown(merchantTxnReference);

		XMLGregorianCalendar xmlGregorianCal = CyberPlusPaymentUtils.getXMLGregorianCalFromDate(breakDown[2]);

		CyberplusRequestDTO cyberplusRequestDTO = new CyberplusRequestDTO();
		cyberplusRequestDTO.setShopId(getMerchantId());
		cyberplusRequestDTO.setTransmissionDate((xmlGregorianCal));
		cyberplusRequestDTO.setTransactionId(breakDown[3]);
		cyberplusRequestDTO.setSequenceNb(1);
		cyberplusRequestDTO.setCtxMode(ipgConfigsDTO.getCtxMode());
		cyberplusRequestDTO.setProxyHost(getProxyHost());
		cyberplusRequestDTO.setProxyPort(getProxyPort());

		String signature = CyberPlusPaymentUtils.createSignature(getSecureSecret(), cyberplusRequestDTO.getShopId(),
				CyberPlusPaymentUtils.formatDate(breakDown[2], CyberPlusPaymentUtils.DATE_FORMAT),
				cyberplusRequestDTO.getTransactionId(), cyberplusRequestDTO.getSequenceNb(), cyberplusRequestDTO.getCtxMode());

		cyberplusRequestDTO.setWsSignature(signature);

		CyberplusTransactionInfoDTO response;
		try {
			WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
			response = ebiWebervices.getInfo(cyberplusRequestDTO);
		} catch (Exception e) {
			log.error(e);
			throw new ModuleException("paymentbroker.error.unknown");
		}
		return response;
	}

	@Override
	public String getPaymentGatewayName() throws ModuleException {
		PaymentBrokerManager.validateIPGIdentificationParams(this.ipgIdentificationParamsDTO);
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}
}
