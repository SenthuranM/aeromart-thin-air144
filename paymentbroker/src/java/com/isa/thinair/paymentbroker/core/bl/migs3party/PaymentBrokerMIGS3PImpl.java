/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.bl.migs3party;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.constants.PaymentGatewayCard;
import com.isa.thinair.paymentbroker.api.dto.CardDetailConfigDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGCommitPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPrepPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.PCValiPaymentDTO;
import com.isa.thinair.paymentbroker.api.dto.PaymentCollectionAdvise;
import com.isa.thinair.paymentbroker.api.dto.XMLResponseDTO;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.model.PreviousCreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.bl.PaymentBroker;
import com.isa.thinair.paymentbroker.core.bl.PaymentBrokerTemplate;
import com.isa.thinair.paymentbroker.core.bl.PaymentQueryDR;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.sun.net.ssl.SSLContext;
import com.sun.net.ssl.X509TrustManager;

/**
 * The payment broker client implementation of MIGS
 * 
 * @author Kanchana
 */
@SuppressWarnings("deprecation")
public class PaymentBrokerMIGS3PImpl extends PaymentBrokerTemplate implements PaymentBroker {

	private static Log log = LogFactory.getLog(PaymentBrokerMIGS3PImpl.class);

	public static SSLSocketFactory s_sslSocketFactory = null;

	private static final String IPG_URL_PART_PAY = "vpcpay";
	private static final String IPG_URL_PART_REFUND = "vpcdps";
	private static final String DEFAULT_TRANSACTION_CODE = "5";
	private static final String PAY_APPROVED_MESSAGE = "Approved";
	private static final String TRANSACTION_RESULT_SUCCESS = "0";
	private static final String PAY_COMMAND = "pay";
	private static final String REFUND_COMMAND = "refund";
	private static final String VOID_COMMAND = "voidPurchase";
	private static final String CAPTURE_COMMAND = "capture";
	private static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();

	static {
		X509TrustManager s_x509TrustManager = new X509TrustManager() {
			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[] {};
			}

			@Override
			public boolean isClientTrusted(X509Certificate[] chain) {
				return true;
			}

			@Override
			public boolean isServerTrusted(X509Certificate[] chain) {
				return true;
			}
		};

		java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

		try {
			SSLContext context = SSLContext.getInstance("TLS");
			context.init(null, new X509TrustManager[] { s_x509TrustManager }, null);
			s_sslSocketFactory = context.getSocketFactory();
		} catch (Exception e) {
			log.error("FATAL ERROR ", e);
			throw new RuntimeException(e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	public ServiceResponce capture(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		String merchTnxRef;

		if (creditCardPayment.getPreviousCCPayment() != null) {

			CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
					creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
			if (ccTransaction != null) {
				if (log.isDebugEnabled()) {
					log.debug("*********** Retrieving Previous Payment Data ********************** ");
					log.debug("*********** GatewayName : " + ccTransaction.getPaymentGatewayConfigurationName() + "******");
					log.debug("*********** Txn Ref No  : " + ccTransaction.getTransactionRefNo() + "  *******");
				}
			}
			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}

			merchTnxRef = composeMerchantTransactionId(appIndicator, creditCardPayment.getTemporyPaymentId(),
					PaymentConstants.MODE_OF_SERVICE.CAPTURE);
			String amount = MIGSPaymentUtils.getFormattedAmount(creditCardPayment.getAmount(),
					creditCardPayment.getNoOfDecimalPoints());

			String vpc_Txn_No = MIGSPaymentUtils.getVPCTnxNo(ccTransaction, creditCardPayment.getPreviousCCPayment());

			Map fields = new HashMap();
			fields.put(MIGS3PRequest.VPC_VERSION, getVersion());
			fields.put(MIGS3PRequest.VPC_COMMAND, CAPTURE_COMMAND);
			fields.put(MIGS3PRequest.VPC_ACC_CODE, getMerchantAccessCode());
			fields.put(MIGS3PRequest.VPC_MERCH_TXN_REF, merchTnxRef);
			fields.put(MIGS3PRequest.VPC_MERCHANT, getMerchantId());
			fields.put(MIGS3PRequest.VPC_TXN_NO, vpc_Txn_No);
			fields.put(MIGS3PRequest.VPC_AMOUNT, amount);
			// fields.put(MIGS3PRequest.VPC_ORDER_INFO, MIGSPaymentUtils.getOrderInfo(creditCardPayment.getPnr(),
			// merchTnxRef));
			fields.put(MIGS3PRequest.VPC_USER, getUser());
			fields.put(MIGS3PRequest.VPC_PASSWORD, getPassword());

			PaymentBrokerMIGS3Capture oPaymentBroakerMIGS3Capture = new PaymentBrokerMIGS3Capture();
			ServiceResponce sr = oPaymentBroakerMIGS3Capture.capture(creditCardPayment, pnr, appIndicator, tnxMode, fields,
					getIpgURL(), getMerchantId());
			return sr;

		} else {
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);
			return sr;
		}

	}

	/**
	 * Performs card refund operation
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings("unchecked")
	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status;
		String errorSpecification;
		String merchTnxRef;
		String ipgURLRefund = getIpgURL() + IPG_URL_PART_REFUND;

		if (creditCardPayment.getPreviousCCPayment() != null) {
			log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
			log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
			log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
			log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
			log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());

			CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
					creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}

			merchTnxRef = composeMerchantTransactionId(appIndicator, creditCardPayment.getTemporyPaymentId(),
					PaymentConstants.MODE_OF_SERVICE.REFUND);
			String amount = MIGSPaymentUtils.getFormattedAmount(creditCardPayment.getAmount(),
					creditCardPayment.getNoOfDecimalPoints());

			String vpc_Txn_No = MIGSPaymentUtils.getVPCTnxNo(ccTransaction, creditCardPayment.getPreviousCCPayment());

			Map fields = new HashMap();
			fields.put(MIGS3PRequest.VPC_VERSION, getVersion());
			fields.put(MIGS3PRequest.VPC_COMMAND, REFUND_COMMAND);
			fields.put(MIGS3PRequest.VPC_ACC_CODE, getMerchantAccessCode());
			fields.put(MIGS3PRequest.VPC_MERCH_TXN_REF, merchTnxRef);
			fields.put(MIGS3PRequest.VPC_MERCHANT, getMerchantId());
			fields.put(MIGS3PRequest.VPC_TXN_NO, vpc_Txn_No);
			fields.put(MIGS3PRequest.VPC_AMOUNT, amount);
			fields.put(MIGS3PRequest.VPC_ORDER_INFO,
					MIGSPaymentUtils.getOrderInfo(creditCardPayment.getPnr(), merchTnxRef, "", null));
			fields.put(MIGS3PRequest.VPC_USER, getUser());
			fields.put(MIGS3PRequest.VPC_PASSWORD, getPassword());

			String refundResponse = null;
			Integer paymentBrokerRefNo;

			// Sends the refund request and gets the response
			try {
				Object[] responses = doPost(pnr, creditCardPayment.getTemporyPaymentId(), ipgURLRefund, fields);

				refundResponse = (String) responses[0];
				paymentBrokerRefNo = (Integer) responses[1];

			} catch (UnsupportedEncodingException uee) {
				log.error(uee);
				throw new ModuleException("paymentbroker.error.unsupportedencoding");
			} catch (IOException ioe) {
				log.error(ioe);
				throw new ModuleException("paymentbroker.error.ioException");
			} catch (Exception e) {
				log.error(e);
				throw new ModuleException("paymentbroker.error.unknown");
			}

			log.debug("Refund Response : " + refundResponse);

			// create a hash map for the response data
			Map responseFields = MIGSPaymentUtils.createMapFromResponse(refundResponse);
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			MIGS3PResponse migsResp = new MIGS3PResponse();
			migsResp.setReponse(responseFields);

			if (TRANSACTION_RESULT_SUCCESS.equals(migsResp.getTxnResponseCode())
					&& PAY_APPROVED_MESSAGE.equals(migsResp.getMessage())) {
				errorCode = "";
				transactionMsg = migsResp.getMessage();
				status = IPGResponseDTO.STATUS_ACCEPTED;
				tnxResultCode = 1;
				errorSpecification = "";
				sr.setSuccess(true);
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
			} else {
				/* errorCode = getMatchingTxnErrorCode(migsResp.getTxnResponseCode()); */
				errorCode = PaymentResponseCodesUtil.getFilteredErrorCode(migsResp);
				transactionMsg = PaymentResponseCodesUtil.getResponseDescription(migsResp.getTxnResponseCode());
				status = IPGResponseDTO.STATUS_REJECTED;
				tnxResultCode = 0;
				errorSpecification = status + " " + transactionMsg + " " + migsResp.getMessage();
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
			}

			updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), migsResp.toString(), errorSpecification,
					migsResp.getAuthorizeID(), migsResp.getTransactionNo(), tnxResultCode);

			sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, migsResp.getAuthorizeID());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
			return sr;
		} else {
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);

			return sr;
		}

	}

	/**
	 * Performs card voidPurchase operation
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	private ServiceResponce voidPurchase(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status;
		String errorSpecification;
		String merchTnxRef;
		String ipgURLRefund = getIpgURL() + IPG_URL_PART_REFUND;

		if (creditCardPayment.getPreviousCCPayment() != null) {
			log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
			log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
			log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
			log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
			log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());

			CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
					creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}

			merchTnxRef = composeMerchantTransactionId(appIndicator, creditCardPayment.getTemporyPaymentId(),
					PaymentConstants.MODE_OF_SERVICE.VOID);
			String amount = MIGSPaymentUtils.getFormattedAmount(creditCardPayment.getAmount(),
					creditCardPayment.getNoOfDecimalPoints());

			String vpc_Txn_No = MIGSPaymentUtils.getVPCTnxNo(ccTransaction, creditCardPayment.getPreviousCCPayment());

			Map fields = new HashMap();
			fields.put(MIGS3PRequest.VPC_VERSION, getVersion());
			fields.put(MIGS3PRequest.VPC_COMMAND, VOID_COMMAND);
			fields.put(MIGS3PRequest.VPC_ACC_CODE, getMerchantAccessCode());
			fields.put(MIGS3PRequest.VPC_MERCH_TXN_REF, merchTnxRef);
			fields.put(MIGS3PRequest.VPC_MERCHANT, getMerchantId());
			fields.put(MIGS3PRequest.VPC_TXN_NO, vpc_Txn_No);
			fields.put(MIGS3PRequest.VPC_AMOUNT, amount);
			fields.put(MIGS3PRequest.VPC_ORDER_INFO,
					MIGSPaymentUtils.getOrderInfo(creditCardPayment.getPnr(), merchTnxRef, "", null));
			fields.put(MIGS3PRequest.VPC_USER, getVoidUser());
			fields.put(MIGS3PRequest.VPC_PASSWORD, getVoidPassword());

			String voidResponse = null;
			Integer paymentBrokerRefNo;

			// Sends the refund request and gets the response
			try {
				Object[] responses = doPost(pnr, creditCardPayment.getTemporyPaymentId(), ipgURLRefund, fields);

				voidResponse = (String) responses[0];
				paymentBrokerRefNo = (Integer) responses[1];

			} catch (UnsupportedEncodingException uee) {
				log.error(uee);
				throw new ModuleException("paymentbroker.error.unsupportedencoding");
			} catch (IOException ioe) {
				log.error(ioe);
				throw new ModuleException("paymentbroker.error.ioException");
			} catch (Exception e) {
				log.error(e);
				throw new ModuleException("paymentbroker.error.unknown");
			}

			log.debug("Void Response : " + voidResponse);

			// create a hash map for the response data
			Map responseFields = MIGSPaymentUtils.createMapFromResponse(voidResponse);
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			MIGS3PResponse migsResp = new MIGS3PResponse();
			migsResp.setReponse(responseFields);

			if (TRANSACTION_RESULT_SUCCESS.equals(migsResp.getTxnResponseCode())
					&& PAY_APPROVED_MESSAGE.equals(migsResp.getMessage())) {
				errorCode = "";
				transactionMsg = migsResp.getMessage();
				status = IPGResponseDTO.STATUS_ACCEPTED;
				tnxResultCode = 1;
				errorSpecification = "";
				sr.setSuccess(true);
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentConstants.SERVICE_RESPONSE_REVERSE_VOID);
			} else {
				/* errorCode = getMatchingTxnErrorCode(migsResp.getTxnResponseCode()); */
				errorCode = PaymentResponseCodesUtil.getFilteredErrorCode(migsResp);
				transactionMsg = PaymentResponseCodesUtil.getResponseDescription(migsResp.getTxnResponseCode());
				status = IPGResponseDTO.STATUS_REJECTED;
				tnxResultCode = 0;
				errorSpecification = status + " " + transactionMsg + " " + migsResp.getMessage();
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
			}

			updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), migsResp.toString(), errorSpecification,
					migsResp.getAuthorizeID(), migsResp.getTransactionNo(), tnxResultCode);

			sr.setResponseCode(String.valueOf(paymentBrokerRefNo));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, migsResp.getAuthorizeID());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
			return sr;
		} else {
			DefaultServiceResponse sr = new DefaultServiceResponse(false);
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REVERSAL);

			return sr;
		}

	}

	/**
	 * Performs card payment operation
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce charge(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode, List<CardDetailConfigDTO> cardDetailConfigData) throws ModuleException {
		CreditCardTransaction creditCardTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPaymentBrokerRefNo());

		if (creditCardTransaction != null && !PlatformUtiltiies.nullHandler(creditCardTransaction.getRequestText()).equals("")) {
			DefaultServiceResponse sr = new DefaultServiceResponse(true);
			sr.setResponseCode(String.valueOf(creditCardTransaction.getTransactionRefNo()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, creditCardTransaction.getAidCccompnay());
			return sr;
		}

		throw new ModuleException("airreservations.temporyPayment.corruptedParameters");
	}

	/**
	 * Performs reverse card payment
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		String refundEnabled = getRefundEnabled();
		boolean refundRequired = false;
		ServiceResponce sr = null;

		if (isVoidBeforeRefund()) {
			sr = voidPurchase(creditCardPayment, pnr, appIndicator, tnxMode);
			if (sr.isSuccess()) {
				return sr;
			} else {
				refundRequired = true;
			}
		} else {
			refundRequired = true;
		}

		if (refundRequired) {
			if (refundEnabled != null && refundEnabled.equals("false")) {

				DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);
				defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);
				return defaultServiceResponse;
				// throw new ModuleException("paymentbroker.reverse.operation.not.supported");
			}

			sr = refund(creditCardPayment, pnr, appIndicator, tnxMode);

			if (!sr.isSuccess()) {
				DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);
				defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);
				return defaultServiceResponse;
			}
		}

		return sr;
	}

	/**
	 * Performs Capture card payment
	 * 
	 * @param creditCardPayment
	 * @param pnr
	 * @param appIndicator
	 * @param tnxMode
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce capturePayment(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {
		ServiceResponce sr = capture(creditCardPayment, pnr, appIndicator, tnxMode);

		if (!sr.isSuccess()) {
			DefaultServiceResponse defaultServiceResponse = new DefaultServiceResponse(false);
			defaultServiceResponse.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.CAPTURE_PAYMENT_ERROR);
			return defaultServiceResponse;
		}

		return sr;
	}

	/**
	 * Creates the message to send to the MIGS server
	 * 
	 * @param ipgRequestDTO
	 *            IPG payment request
	 */
	@Override
	@SuppressWarnings("unchecked")
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO, List<CardDetailConfigDTO> configDataList)
			throws ModuleException {
		// TODO introduce new variable to identify broker type and payment gateway type
		boolean isPostMethod = false;
		String requestData = null;
		String storeData = null;
		Map<String, String> postDataMap = null;

		String cardNo = ipgRequestDTO.getCardNo();
		if (cardNo != null && !cardNo.trim().isEmpty() || getRequestMethod().contentEquals("POST")) {
			isPostMethod = true;
		}

		String ipgURLPay = getIpgURL() + IPG_URL_PART_PAY;

		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		String finalAmount = MIGSPaymentUtils.getFormattedAmount(ipgRequestDTO.getAmount(), ipgRequestDTO.getNoOfDecimals());

		String strOrderInfo = MIGSPaymentUtils.getOrderInfo(ipgRequestDTO.getPnr(), merchantTxnId,
				ipgRequestDTO.getRequestedCarrierCode(), ipgRequestDTO.getUserIPAddress());

		MIGS3PRequest ipgRequest = new MIGS3PRequest(merchantId, ipgRequestDTO.getReturnUrl(), locale, PAY_COMMAND,
				merchantAccessCode, strOrderInfo, version, finalAmount, merchantTxnId, ipgURLPay);

		if (!isPostMethod) {
			requestData = ipgRequest.getRequestRedirectionURL(getSecureSecret());
			storeData = requestData;
		} else {
			// Set PostRequest Data
			String cardType = MIGSPaymentUtils.getCardType(ipgRequestDTO.getCardType());
			if (cardType == null) {
				throw new ModuleException("errors.resv.pay.cardType.notsupport");
			}

			// FIXME make it configurable to support 2.5 and 3
			boolean isSSL = true;
			ipgRequest.setPostRequestData(cardType, ipgRequestDTO.getCardNo(), ipgRequestDTO.getExpiryDate(),
					ipgRequestDTO.getSecureCode(), isSSL);
			
			
			// Create Secure Hash
			if (getSecureSecret() != null && getSecureSecret().length() > 0) {
				String secureHash = MIGSPaymentUtils.hashAllFieldsMigsSHA256(ipgRequest.getFields(), getSecureSecret());
				ipgRequest.getFields().put(MIGS3PRequest.VPC_SECURE_HASH, secureHash.toUpperCase());
				ipgRequest.getFields().put(MIGS3PRequest.VPC_SECURE_HASH_TYPE, MIGS3PRequest.SECURE_HASH_TYPE_VALUE);
			}
			String[] formData = getPostInputDataFormHTML(ipgRequest.getFields(), ipgURLPay, configDataList);
			requestData = formData[0];
			storeData = formData[1];
		}

		String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();
		CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
				new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"), storeData
						+ "," + sessionID, "", getPaymentGatewayName(), null, false);

		if (log.isDebugEnabled()) {
			log.debug("******** IPG CHARGE REQUEST URL : " + storeData);
			log.debug("******** IPG CHARGE REQUEST SESSION ID : " + sessionID);
		}

		IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
		ipgRequestResultsDTO.setRequestData(requestData);
		ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
		ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);

		return ipgRequestResultsDTO;
	}

	private String[] getPostInputDataFormHTML(Map<String, String> postDataMap, String url,
			List<CardDetailConfigDTO> configDataList) {
		String[] forrmDataArray = new String[2];
		StringBuffer inputFormData = new StringBuffer();
		StringBuffer storeFormData = new StringBuffer();
		String data = "<form name=\"RedirectForm\" action=\"" + url + "\" method=\"post\">";
		String storeCardNo = null;
		String value = null;
		StringBuffer cardStoreSb = new StringBuffer();
		inputFormData.append(data);
		storeFormData.append(data);

		for (String fieldName : postDataMap.keySet()) {
			value = postDataMap.get(fieldName);
			data = "<input type=\"hidden\" name=\"" + fieldName + "\" value=\"" + value + "\"/>";
			inputFormData.append(data);

			if (MIGS3PRequest.VPC_CARD_NUM.equals(fieldName)) {
				value = PaymentBrokerUtils.getDBStroreData(configDataList, PaymentGatewayCard.FieldName.CARDNUMBER.code(), value);
			} else if (MIGS3PRequest.VPC_CARD_EXP.equals(fieldName)) {
				value = PaymentBrokerUtils.getDBStroreData(configDataList, PaymentGatewayCard.FieldName.EXPIRYDATE.code(), value);
			} else if (MIGS3PRequest.VPC_CARD_CVV.equals(fieldName)) {
				value = PaymentBrokerUtils.getDBStroreData(configDataList, PaymentGatewayCard.FieldName.CVV.code(), value);
			}

			data = "<input type=\"hidden\" name=\"" + fieldName + "\" value=\"" + value + "\"/>";
			storeFormData.append(data);
		}

		inputFormData.append("</form>");
		storeFormData.append("</form>");
		forrmDataArray[0] = inputFormData.toString();
		forrmDataArray[1] = storeFormData.toString();

		return forrmDataArray;
	}

	/**
	 * Reads the response returns from the MIGS server
	 * 
	 * @param encryptedReceiptPay
	 *            the secured response returns from the IPG
	 */
	@Override
	public IPGResponseDTO getReponseData(Map fields, IPGResponseDTO ipgResponseDTO) throws ModuleException {
		log.debug("[PaymentBrokerMIGS3PImpl::getReponseData()] Begin ");

		// IPGResponseDTO ipgResponseDTO = new IPGResponseDTO();
		boolean errorExists = false;
		int tnxResultCode;
		String errorCode = "";
		String status;
		String errorSpecification;
		String merchantTxnReference;
		String authorizeID;
		int cardType = 5; // default card type

		String hashValidated = null;
		String responseMismatch = "";
		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());

		// Calculate response time
		// Calendar responseTime = Calendar.getInstance();
		// Date requestTime = MIGSPaymentUtils.getDateFromCCTimeStamp(oCreditCardTransaction.getTransactionTimestamp());
		// String strResponseTime = CalanderUtil.getTimeDifference(requestTime, responseTime.getTime());
		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time
		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);
		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);

		MIGS3PResponse migsResp = new MIGS3PResponse();
		migsResp.setReponse(fields);
		log.debug("[PaymentBrokerMIGS3PImpl::getReponseData()] Mid Response -" + migsResp.getMerchTxnRef());
		if (migsResp != null && !(migsResp.getTxnResponseCode().trim().equals(""))) {

			// Validate the Secure Hash (remember MD5 hashes are not case sensitive)
			if (migsResp.validateHash(getSecureSecret())) {
				// Secure Hash validation succeeded, add a data field to be
				// displayed later.
				hashValidated = MIGS3PResponse.VALID_HASH;

				// Check for correct response
				errorExists = true;

				// CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(paymentBrokerRefNo);
				// Check if response is not equal
				if (oCreditCardTransaction.getTempReferenceNum().equalsIgnoreCase(migsResp.getMerchTxnRef())) {
					errorExists = false;
				} else {
					responseMismatch = MIGS3PResponse.INVALID_RESPONSE;

					// Use QueryDR to verify the transaction
					PaymentQueryDR oNewQuery = getPaymentQueryDR();
					// query(TPT_ID, PNR, MERCHANT_TRANSACTION_REF, MERCHANT_ID, MERCHANT_ACCESS_CODE )
					ServiceResponce serviceResponce = oNewQuery.query(oCreditCardTransaction, MIGSPaymentUtils.getIPGConfigs(
							getMerchantId(), getMerchantAccessCode(), getIpgURL(), getUser(), getPassword(), getVersion()),
							PaymentBrokerInternalConstants.QUERY_CALLER.INVALIDRESPONSE);
					if (serviceResponce.isSuccess()) {
						errorExists = false;
					}
				}
			} else {
				// Secure Hash validation failed, add a data field to be
				// displayed later.
				errorExists = true;
				hashValidated = MIGS3PResponse.INVALID_HASH;

				// CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(paymentBrokerRefNo);

				PaymentQueryDR oNewQuery = getPaymentQueryDR();
				// query(TPT_ID, PNR, MERCHANT_TRANSACTION_REF, MERCHANT_ID, MERCHANT_ACCESS_CODE )
				ServiceResponce serviceResponce = oNewQuery.query(oCreditCardTransaction, MIGSPaymentUtils.getIPGConfigs(
						getMerchantId(), getMerchantAccessCode(), getIpgURL(), getUser(), getPassword(), getVersion()),
						PaymentBrokerInternalConstants.QUERY_CALLER.HASHMISMATCH);
				if (serviceResponce.isSuccess()) {
					errorExists = false;
				}
			}
		} else {
			// Secure Hash was not validated,
			hashValidated = MIGS3PResponse.NO_VALUE;
			errorCode = DEFAULT_TRANSACTION_CODE;
		}

		merchantTxnReference = migsResp.getMerchTxnRef();
		authorizeID = migsResp.getAuthorizeID();
		cardType = getStandardCardType(migsResp.getCardType());

		if (TRANSACTION_RESULT_SUCCESS.equals(migsResp.getTxnResponseCode())
				&& PAY_APPROVED_MESSAGE.equals(migsResp.getMessage()) && !errorExists) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			tnxResultCode = 1;
			errorSpecification = "" + responseMismatch;
		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;

			/* errorCode=getFilteredErrorCode(migsResp); */
			errorCode = PaymentResponseCodesUtil.getFilteredErrorCode(migsResp);

			errorSpecification = status + " " + hashValidated + " "
					+ PaymentResponseCodesUtil.getResponseDescription(migsResp.getTxnResponseCode()) + " "
					+ getStatusDescription(migsResp.getVerStatus3DS()) + " " + migsResp.getMessage() + " " + responseMismatch;
		}

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setMessage(migsResp.getMessage());
		ipgResponseDTO.setApplicationTransactionId(merchantTxnReference);
		ipgResponseDTO.setAuthorizationCode(authorizeID);
		ipgResponseDTO.setCardType(cardType);
		String trimedDigits = "";
		if (migsResp.getCcLast4Digits() != null && migsResp.getCcLast4Digits().length() > 4)
			trimedDigits = migsResp.getCcLast4Digits().substring(migsResp.getCcLast4Digits().length() - 4);

		ipgResponseDTO.setCcLast4Digits(trimedDigits);
		String migsResponse = migsResp.toString();

		updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), migsResponse, errorSpecification,
				migsResp.getAuthorizeID(), migsResp.getTransactionNo(), tnxResultCode);

		log.debug("[PaymentBrokerMIGS3PImpl::getReponseData()] End -" + migsResp.getMerchTxnRef() + "");

		return ipgResponseDTO;
	}

	/**
	 * This method uses the 3DS verStatus retrieved from the Response and returns an appropriate description for this
	 * code.
	 * 
	 * @param vpc_VerStatus
	 *            String containing the status code
	 * @return description String containing the appropriate description
	 */
	private String getStatusDescription(String vStatus) {
		String result = "";
		if (vStatus != null && !vStatus.equals("")) {

			if (vStatus.equalsIgnoreCase("Unsupported") || vStatus.equals("No Value Returned")) {
				result = "3DS not supported or there was no 3DS data provided.";
			} else {
				// Java cannot switch on a string so turn everything to a character
				char input = vStatus.charAt(0);
				switch (input) {
				case 'Y':
					result = "The cardholder was successfully authenticated.";
					break;
				case 'E':
					result = "The cardholder is not enrolled.";
					break;
				case 'N':
					result = "The cardholder was not verified.";
					break;
				case 'U':
					result = "The cardholder's Issuer was unable to authenticate due to some system error at the Issuer.";
					break;
				case 'F':
					result = "There was an error in the format of the request from the merchant.";
					break;
				case 'A':
					result = "Authentication of your Merchant ID and Password to the ACS Directory Failed.";
					break;
				case 'D':
					result = "Error communicating with the Directory Server.";
					break;
				case 'C':
					result = "The card type is not supported for authentication.";
					break;
				case 'S':
					result = "The signature on the response received from the Issuer could not be validated.";
					break;
				case 'P':
					result = "Error parsing input from Issuer.";
					break;
				case 'I':
					result = "Internal Payment Server system error.";
					break;
				default:
					result = "Unable to be determined.";
					break;
				}
			}
		} else {
			result = "null response";
		}
		return result;
	}

	/**
	 * Returns Payment Gateway Name
	 */
	@Override
	public String getPaymentGatewayName() {
		return this.ipgIdentificationParamsDTO.getFQIPGConfigurationName();
	}

	/**
	 * Returns the card type as integer
	 * 
	 * @param card
	 *            the card
	 * @return card type
	 */
	private int getStandardCardType(String card) {
		// [1-MASTER, 2-VISA, 3-AMEX, 4-DINNERS, 5-GENERIC]
		int mapType = 5; // default card type
		if (card != null && !card.equals("") && mapCardType.get(card) != null) {
			String mapTypeStr = mapCardType.get(card);
			mapType = Integer.parseInt(mapTypeStr);
		}
		return mapType;
	}

	/**
	 * This method is for performing a Form POST operation from input data parameters.
	 * 
	 * @param vpc_Host
	 *            - is a String containing the vpc URL
	 * @param data
	 *            - is a String containing the post data key value pairs
	 * @param useProxy
	 *            - is a boolean indicating if a Proxy Server is involved in the transfer
	 * @param proxyHost
	 *            - is a String containing the IP address of the Proxy to send the data to
	 * @param proxyPort
	 *            - is an integer containing the port number of the Proxy socket listener
	 * @return - is body data of the POST data response
	 * @throws ModuleException 
	 */
	public Object[] doPost(String pnr, int temporyPaymentId, String vpc_Host, Map dataFields) throws IOException,
			UnsupportedEncodingException, ModuleException {

		InputStream is;
		OutputStream os;
		int vpc_Port = 443;
		String fileName = "";
		boolean useSSL = false;
		StringBuffer postData = new StringBuffer();
		MIGS3PRequest migsReq = new MIGS3PRequest();
		migsReq.appendQueryFields(postData, null, dataFields);

		// determine if SSL encryption is being used
		if (vpc_Host.substring(0, 8).equalsIgnoreCase("HTTPS://")) {
			useSSL = true;
			// remove 'HTTPS://' from host URL
			vpc_Host = vpc_Host.substring(8);
			// get the filename from the last section of vpc_URL
			fileName = vpc_Host.substring(vpc_Host.lastIndexOf("/"));
			// get the IP address of the VPC machine
			vpc_Host = vpc_Host.substring(0, vpc_Host.lastIndexOf("/"));
		}

		// use the next block of code if using a proxy server
		if (useProxy) {
			Socket s = new Socket(proxyHost, Integer.parseInt(proxyPort));
			os = s.getOutputStream();
			is = s.getInputStream();
			// use next block of code if using SSL encryption
			if (useSSL) {
				String msg = "CONNECT " + vpc_Host + ":" + vpc_Port + " HTTP/1.0\r\n" + "User-Agent: HTTP Client\r\n\r\n";
				os.write(msg.getBytes());
				byte[] buf = new byte[4096];
				int len = is.read(buf);
				String res = new String(buf, 0, len);

				// check if a successful HTTP connection
				if (res.indexOf("200") < 0) {
					throw new IOException("Proxy would now allow connection - " + res);
				}

				// write output to VPC
				SSLSocket ssl = (SSLSocket) s_sslSocketFactory.createSocket(s, vpc_Host, vpc_Port, true);
				ssl.startHandshake();
				os = ssl.getOutputStream();
				// get response data from VPC
				is = ssl.getInputStream();
				// use the next block of code if NOT using SSL encryption
			} else {
				fileName = vpc_Host;
			}
			// use the next block of code if NOT using a proxy server
		} else {
			// use next block of code if using SSL encryption
			if (useSSL) {
				Socket s = s_sslSocketFactory.createSocket(vpc_Host, vpc_Port);
				os = s.getOutputStream();
				is = s.getInputStream();
				// use next block of code if NOT using SSL encryption
			} else {
				Socket s = new Socket(vpc_Host, vpc_Port);
				os = s.getOutputStream();
				is = s.getInputStream();
			}
		}

		String req = "POST " + fileName + " HTTP/1.0\r\n" + "User-Agent: HTTP Client\r\n"
				+ "Content-Type: application/x-www-form-urlencoded\r\n" + "Content-Length: " + postData.toString().length()
				+ "\r\n\r\n" + postData.toString();

		CreditCardTransaction ccTransaction = auditTransaction(pnr, getMerchantId(),
				(String) dataFields.get(MIGS3PRequest.VPC_MERCH_TXN_REF), new Integer(temporyPaymentId),
				bundle.getString("SERVICETYPE_REFUND"), req, "", getPaymentGatewayName(), null, false);

		log.debug("******** IPG REFUND REQUEST URL : " + req);

		os.write(req.getBytes());
		String res = new String(MIGSPaymentUtils.readAll(is));

		log.debug("Immediate refund response file : " + res);

		// check if a successful connection
		if (res.indexOf("200") < 0) {
			throw new IOException("Connection Refused - " + res);
		}

		if (res.indexOf("404 Not Found") > 0) {
			throw new IOException("File Not Found Error - " + res);
		}

		int resIndex = res.indexOf("\r\n\r\n");
		String body = res.substring(resIndex + 4, res.length());

		log.debug("Response file body : " + body);
		return new Object[] { body, new Integer(ccTransaction.getTransactionRefNo()) };
	}

	/**
	 * Resolves partial payments [Invokes via a scheduler operation]
	 */
	@Override
	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {
		Iterator itColIPGQueryDTO = colIPGQueryDTO.iterator();

		Collection oResultIF = new ArrayList();
		Collection oResultIP = new ArrayList();
		Collection oResultII = new ArrayList();
		Collection oResultIS = new ArrayList();

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		IPGQueryDTO ipgQueryDTO;
		CreditCardTransaction oCreditCardTransaction;
		PaymentQueryDR oNewQuery;
		PreviousCreditCardPayment oPrevCCPayment;
		CreditCardPayment oCCPayment;

		while (itColIPGQueryDTO.hasNext()) {
			ipgQueryDTO = (IPGQueryDTO) itColIPGQueryDTO.next();

			oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgQueryDTO.getPaymentBrokerRefNo());
			oNewQuery = getPaymentQueryDR();

			try {
				// query(TPT_ID, PNR, MERCHANT_TRANSACTION_REF, MERCHANT_ID, MERCHANT_ACCESS_CODE )
				ServiceResponce serviceResponce = oNewQuery.query(oCreditCardTransaction, MIGSPaymentUtils.getIPGConfigs(
						getMerchantId(), getMerchantAccessCode(), getIpgURL(), getUser(), getPassword(), getVersion()),
						PaymentBrokerInternalConstants.QUERY_CALLER.SCHEDULER);

				// If successful payment remains at Bank
				if (serviceResponce.isSuccess()) {
					if (refundFlag) {
						// Do refund
						String txnNo = PlatformUtiltiies.nullHandler(serviceResponce
								.getResponseParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE));

						oPrevCCPayment = new PreviousCreditCardPayment();
						oPrevCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
						oPrevCCPayment.setPgSpecificTxnNumber(txnNo);

						oCCPayment = new CreditCardPayment();
						oCCPayment.setPreviousCCPayment(oPrevCCPayment);
						oCCPayment.setAmount(ipgQueryDTO.getAmount().toString());
						oCCPayment.setAppIndicator(ipgQueryDTO.getAppIndicatorEnum());
						oCCPayment.setPaymentBrokerRefNo(oCreditCardTransaction.getTransactionRefNo());
						oCCPayment.setIpgIdentificationParamsDTO(ipgQueryDTO.getIpgIdentificationParamsDTO());
						oCCPayment.setTnxMode(TnxModeEnum.SECURE_3D);
						oCCPayment.setPnr(oCreditCardTransaction.getTransactionReference());
						oCCPayment.setTemporyPaymentId(oCreditCardTransaction.getTemporyPaymentId());

						ServiceResponce srRev = refund(oCCPayment, oCCPayment.getPnr(), oCCPayment.getAppIndicator(),
								oCCPayment.getTnxMode());

						if (srRev.isSuccess()) {
							// Change State to 'IS'
							oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
							if (log.isDebugEnabled()) {
								log.debug("Status moing to IS:" + oCreditCardTransaction.getTemporyPaymentId());
							}
						} else {
							// Change State to 'IF'
							oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
							if (log.isDebugEnabled()) {
								log.debug("Status moing to IF:" + oCreditCardTransaction.getTemporyPaymentId());
							}
						}
					} else {
						// Change State to 'IP'
						oResultIP.add(oCreditCardTransaction.getTemporyPaymentId());
						if (log.isDebugEnabled()) {
							log.debug("Status moing to IP:" + oCreditCardTransaction.getTemporyPaymentId());
						}
					}
				}
				// No payment detail at bank
				else {
					// No Payments exist
					// Update status to 'II'
					oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
					if (log.isDebugEnabled()) {
						log.debug("Status moing to II:" + oCreditCardTransaction.getTemporyPaymentId());
					}
				}
			} catch (ModuleException e) {
				log.error("Scheduler::ResolvePayment FAILED!!! " + e.getMessage(), e);
			}
		}

		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF, oResultIF);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_II, oResultII);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IP, oResultIP);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS, oResultIS);

		sr.setSuccess(true);

		return sr;
	}

	@Override
	public ServiceResponce captureStatusResponse(Map receiptyMap) throws ModuleException {
		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
				PaymentBrokerInternalConstants.MessageCodes.CAPTURE_STATUS_OPERATION_NOT_SUPPORTED);

		return sr;
	}

	@Override
	public PaymentCollectionAdvise advisePaymentCollection(IPGPrepPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce postCommitPayment(IPGCommitPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public ServiceResponce preValidatePayment(PCValiPaymentDTO params) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}

	@Override
	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		return getRequestData(ipgRequestDTO, null);
	}

	@Override
	public XMLResponseDTO getXMLResponse(Map postDataMap) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void handleDeferredResponse(Map<String, String> receiptyMap, IPGResponseDTO ipgResponseDTO,
			IPGIdentificationParamsDTO ipgIdentificationParamsDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");		
	}

	@Override
	public IPGRequestResultsDTO getRequestDataForAliasPayment(IPGRequestDTO ipgRequestDTO) throws ModuleException {
		throw new ModuleException("paymentbroker.generic.operation.notsupported");
	}
}
