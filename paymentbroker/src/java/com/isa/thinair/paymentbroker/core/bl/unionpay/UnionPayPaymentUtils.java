package com.isa.thinair.paymentbroker.core.bl.unionpay;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.unionpay.acp.sdk.SDKConfig;
import com.unionpay.acp.sdk.SDKUtil;
public class UnionPayPaymentUtils {

	private static Log log = LogFactory.getLog(UnionPayPaymentUtils.class);

	public static final String DATA_SEPARATOR = "&";

	public static final String VALUE_SEPARATOR = "=";

	public static enum UNIONPAY_OPERATION {
		RES, DES, RFD, INQ
	};

	public static enum ECI {
		MOTO(1), ECOMMERCE(7);

		int eciValue;

		ECI(int eciValue) {
			this.eciValue = eciValue;
		}

		public int getECIValue() {
			return eciValue;
		}
	};

	public static String getTrackingParameter(int transactionRefNo, IPGRequestDTO ipgRequestDTO) {
		return transactionRefNo + "_" + ipgRequestDTO.getIpgIdentificationParamsDTO().getIpgId() + "_"
				+ ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode();
	}



	public static UnionPayResponse getResponse(Map<String, String> postDataMap, String propertiesPath) {
		HttpClient client = new HttpClient();
		UnionPayResponse unionPayResponse = new UnionPayResponse();
		PostMethod method = new PostMethod(postDataMap.get("URL"));
		prepareCommonRequest(client, method, postDataMap);

		String responseString = null;
		try {
			log.debug("Executing the Union Pay Request for URL : " + postDataMap.get("URL"));
			int returnCode = client.executeMethod(method);

			if (returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
				log.debug("The Post method is not implemented by this URI");
				return null;
			} else {
				responseString = method.getResponseBodyAsString();

				log.debug("HTTP Response : " + responseString);

				Map<String, String> responseParamsMap = getParamsMap(responseString);
				unionPayResponse.setReponse(responseParamsMap);

				SDKConfig.getConfig().loadPropertiesFromPath(propertiesPath);
				boolean validSignature = SDKUtil.validate(responseParamsMap, "UTF-8");

				if (validSignature) {
					return unionPayResponse;
				} else {
					log.warn("Wrong secure hash in response genSecureHash:" + responseParamsMap.get(UnionPayConstants.SIGNATURE)
							+ ", received SecureHash: " + unionPayResponse.getSignature());
					return new UnionPayResponse();
				}
			}
		} catch (Exception e) {
			log.debug("Exception while parsing the HTTP response " + e.getMessage(), e);
		} finally {
			method.releaseConnection();
		}
		return null;
	}

	private static Map<String, String> getParamsMap(String responseString) {
		Map<String, String> responseParamMap = new HashMap<String, String>();
		if (responseString != null) {
			String parameters[] = responseString.split(DATA_SEPARATOR);
			for (int i = 0; i < parameters.length; i++) {
				String paramValue[] = parameters[i].split(VALUE_SEPARATOR, 2);
				responseParamMap.put(paramValue[0], paramValue[1]);
			}
		}
		return responseParamMap;
	}

	private static void prepareCommonRequest(HttpClient client, PostMethod method, Map<String, String> postDataMap) {
		HostConfiguration hcon = new HostConfiguration();

		if (postDataMap.get("USE_PROXY").equals("Y")) {
			hcon.setProxy(postDataMap.get("PROXY_HOST"), Integer.valueOf(postDataMap.get("PROXY_PORT")));
		}

		client.setHostConfiguration(hcon);
		method.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");

		for (String key : postDataMap.keySet()) {

			if (log.isDebugEnabled() && key.equalsIgnoreCase("PROXY_HOST") || key.equalsIgnoreCase("PROXY_PORT")
					|| key.equalsIgnoreCase("URL") || key.equalsIgnoreCase("USE_PROXY")) {
				log.debug("Union-pay Proxy Details : " + key + " : " + postDataMap.get(key));
			}

			if (!key.equals("PROXY_HOST") && !key.equals("PROXY_PORT") && !key.equals("URL") && !key.equals("USE_PROXY")) {
				method.addParameter(key, postDataMap.get(key));
			}
		}
	}

	public static String getFormattedAmount(String amount, boolean roundUp) {

		BigDecimal amt = new BigDecimal(amount);

		if (roundUp) {
			amt = amt.setScale(0, BigDecimal.ROUND_UP);
		} else {
			amt = amt.setScale(0, BigDecimal.ROUND_DOWN);
		}

		amount = amt.toString();

		return amount;
	}

	public static IPGConfigsDTO getIPGConfigs(String merchantID, String userName, String password) {
		IPGConfigsDTO oIPGConfigsDTO = new IPGConfigsDTO();
		oIPGConfigsDTO.setMerchantID(merchantID);
		oIPGConfigsDTO.setUsername(userName);
		oIPGConfigsDTO.setPassword(password);

		return oIPGConfigsDTO;
	}

}
