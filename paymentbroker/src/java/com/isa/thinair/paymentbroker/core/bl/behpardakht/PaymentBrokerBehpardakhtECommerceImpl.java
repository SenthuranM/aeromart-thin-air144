package com.isa.thinair.paymentbroker.core.bl.behpardakht;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airproxy.api.utils.ConfirmReservationUtil;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGConfigsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGQueryDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestResultsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGResponseDTO;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtInquiryRequest;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtPayRequest;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtPayResponse;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtRefundRequest;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtResponse;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtReversalRequest;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtSettleRequest;
import com.isa.thinair.paymentbroker.api.dto.behpardakht.BehpardakhtVerifyRequest;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants;
import com.isa.thinair.paymentbroker.core.PaymentBrokerInternalConstants.QUERY_CALLER;
import com.isa.thinair.paymentbroker.core.config.PaymentBrokerModuleUtil;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.wsclient.api.service.WSClientBD;

public class PaymentBrokerBehpardakhtECommerceImpl extends PaymentBrokerBehpardakhtTemplate {

	private static Log log = LogFactory.getLog(PaymentBrokerBehpardakhtECommerceImpl.class);
	protected static ResourceBundle bundle = PaymentBrokerModuleUtil.getResourceBundle();
	protected static final String CARD_TYPE_GENERIC = "GC";
	protected static final String ERROR = "error";
	protected static final String PERSIAN = "fa";

	public IPGRequestResultsDTO getRequestData(IPGRequestDTO ipgRequestDTO) throws ModuleException {

		log.debug("[PaymentBrokerBehpardakhtECommerceImpl::getRequestData()] Begin ");
		
		String ipgURL;

		String merchantTxnId = composeMerchantTransactionId(ipgRequestDTO.getApplicationIndicator(),
				ipgRequestDTO.getApplicationTransactionId(), PaymentConstants.MODE_OF_SERVICE.PAYMENT);

		Integer orderId = Integer.parseInt(merchantTxnId.substring(1));
		IPGRequestResultsDTO IPGRequestResultsDTO = new IPGRequestResultsDTO();

		Map<String, String> postDataMap = new LinkedHashMap<String, String>();

		String finalAmount = BehpardakhtPaymentUtils.getFormattedAmount(ipgRequestDTO.getAmount(), true); // Numeric
																											// (12,2)

		BehpardakhtPayRequest behpardakhtPayRequest = new BehpardakhtPayRequest();

		String merchantId = getMerchantId();
		Long terminalId = Long.parseLong(merchantId);
		
		if(ipgRequestDTO.getSessionLanguageCode() != null && !ipgRequestDTO.getSessionLanguageCode().isEmpty() && PERSIAN.equals(ipgRequestDTO.getSessionLanguageCode())){
			ipgURL = getIpgPersianURL();
		} else {
			ipgURL = getIpgURL();
		}

		Calendar calendar = new GregorianCalendar();
		calendar.setTime(CalendarUtil.getCurrentZuluDateTime());

		String[] strTimeArray = new SimpleDateFormat("HH:mm:ss").format(calendar.getTime()).split(":");
		String hours = strTimeArray[0];
		String minutes = strTimeArray[1];
		String seconds = strTimeArray[2];

		behpardakhtPayRequest.setTerminalId(terminalId);
		behpardakhtPayRequest.setUserName(getUser());
		behpardakhtPayRequest.setLocalDate(CalendarUtil.formatDateYYYYMMDD(CalendarUtil.getCurrentZuluDateTime()));
		behpardakhtPayRequest.setLocalTime(hours + "" + minutes + "" + seconds);
		behpardakhtPayRequest.setAmount(Long.parseLong(finalAmount));
		behpardakhtPayRequest.setOrderId(orderId.longValue());
		Integer temp = 0;
		behpardakhtPayRequest.setPayerId(temp.longValue());
		behpardakhtPayRequest.setUserPassword(getPassword());
		behpardakhtPayRequest.setCallBackUrl(ipgRequestDTO.getReturnUrl());
		behpardakhtPayRequest.setAdditionalData(getAdditionalData() + ipgRequestDTO.getPnr());

		WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
		BehpardakhtResponse payResponse = ebiWebervices.payRequest(behpardakhtPayRequest);

		// handle null and string util null or empty in for payResponse and for response
		String[] response = payResponse.getResponse().split(",");
		if (response.length > 1) {
			if ((BehpardakhtPaymentUtils.TRANSACTION_APPROVED).equals(response[0])) {
				payResponse.setRefNumber(response[1]);
				payResponse.setSuccess(true);
				postDataMap.put(payResponse.REF_NO, payResponse.getRefNumber().trim());

				String strRequestParams = PaymentBrokerUtils.getRequestDataAsString(postDataMap);

				String sessionID = ipgRequestDTO.getSessionID() == null ? "" : ipgRequestDTO.getSessionID();

				String requestDataForm = PaymentBrokerUtils.getPostInputDataFormHTML(postDataMap, ipgURL);

				CreditCardTransaction ccTransaction = auditTransaction(ipgRequestDTO.getPnr(), getMerchantId(), merchantTxnId,
						new Integer(ipgRequestDTO.getApplicationTransactionId()), bundle.getString("SERVICETYPE_AUTHORIZE"),
						strRequestParams + "," + sessionID, "", getPaymentGatewayName(), null, false);

				if (log.isDebugEnabled()) {
					log.debug("Behpardakht PG Request Data : " + strRequestParams + ", sessionID : " + sessionID);
				}

				IPGRequestResultsDTO ipgRequestResultsDTO = new IPGRequestResultsDTO();
				ipgRequestResultsDTO.setRequestData(requestDataForm);
				ipgRequestResultsDTO.setPaymentBrokerRefNo(ccTransaction.getTransactionRefNo());
				ipgRequestResultsDTO.setAccelAeroTransactionRef(merchantTxnId);
				ipgRequestResultsDTO.setPostDataMap(postDataMap);

				log.debug("[PaymentBrokerBehpardakhtECommerceImpl::getRequestData()] End ");
				return ipgRequestResultsDTO;

			}

		} else {
			payResponse.setError(ERROR);
		}
		IPGRequestResultsDTO.setBehpardakhtResponse(payResponse);
		return IPGRequestResultsDTO;
	}

	public IPGResponseDTO getReponseData(Map receiptyMap, IPGResponseDTO ipgResponseDTO) throws ModuleException {

		log.debug("[PaymentBrokerBehpardakhtECommerceImpl::getReponseData()] Begin ");

		int tnxResultCode;
		String errorCode = "";
		String status = "";
		String errorSpecification = "";
		String merchantTxnReference = "";
		String accelAeroTxnReference = "";
		String applicationTransactionId = "";
		String behpardakhtResponse = "";

		CreditCardTransaction oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo());

		String strResponseTime = CalendarUtil.getTimeDifference(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());
		long timeDiffInMillis = CalendarUtil.getTimeDifferenceInMillis(ipgResponseDTO.getRequestTimsStamp(),
				ipgResponseDTO.getResponseTimeStamp());

		// Update response time
		oCreditCardTransaction.setComments(strResponseTime);
		oCreditCardTransaction.setResponseTime(timeDiffInMillis);

		PaymentBrokerUtils.getPaymentBrokerDAO().saveTransaction(oCreditCardTransaction);
		applicationTransactionId = oCreditCardTransaction.getTempReferenceNum();
		BehpardakhtPayResponse BehpardakhtResp = new BehpardakhtPayResponse();
		BehpardakhtResp.setResponse(receiptyMap);

		if (BehpardakhtResp.getSaleReferenceId() == null && BehpardakhtResp.getSaleOrderId() != null) {

			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
			errorCode = BehpardakhtResp.getResCode();
			errorSpecification = BehpardakhtPaymentUtils.getErrorDescription(errorCode);
			// Different amount in response
			log.error("[PaymentBrokerBehpardakhtECommerceImpl::getReponseData()] - cancel by the user");

		} else if (BehpardakhtResp.getSaleReferenceId() != null && BehpardakhtResp.getSaleOrderId() != null
				&& oCreditCardTransaction.getTransactionId() == null && oCreditCardTransaction.getAidCccompnay() == null) {

			merchantTxnReference = BehpardakhtResp.getSaleReferenceId().toString();
			accelAeroTxnReference = BehpardakhtResp.getSaleOrderId();
			behpardakhtResponse = BehpardakhtResp.toString();
			errorCode = BehpardakhtResp.getResCode();
			errorSpecification = BehpardakhtPaymentUtils.getErrorDescription(errorCode);
			tnxResultCode = 0;

			log.info("[PaymentBrokerBehpardakhtECommerceImpl::getReponseData()] - save response to DB first time");

		} else if (BehpardakhtResp.getSaleReferenceId() != null && BehpardakhtResp.getSaleOrderId() != null
				&& oCreditCardTransaction.getTransactionId() != null && oCreditCardTransaction.getAidCccompnay() != null) {

			if (log.isDebugEnabled()) {
				log.debug("[PaymentBrokerBehpardakhtECommerceImpl::getReponseData()] Response ");
			}
			merchantTxnReference = BehpardakhtResp.getSaleReferenceId().toString();
			accelAeroTxnReference = BehpardakhtResp.getSaleOrderId();
			behpardakhtResponse =  oCreditCardTransaction.getResponseText();
			
			if (BehpardakhtResp.getResCode().equalsIgnoreCase("0")) {

				BehpardakhtVerifyRequest behpardakhtVerifyRequest = new BehpardakhtVerifyRequest();
				String merchantId = getMerchantId();
				Long terminalId = Long.parseLong(merchantId);

				behpardakhtVerifyRequest.setTerminalId(terminalId);
				behpardakhtVerifyRequest.setUserName(getUser());
				behpardakhtVerifyRequest.setUserPassword(getPassword());
				behpardakhtVerifyRequest.setOrderId(oCreditCardTransaction.getTemporyPaymentId().longValue());
				behpardakhtVerifyRequest.setSaleOrderId(Long.parseLong(BehpardakhtResp.getSaleOrderId()));
				behpardakhtVerifyRequest.setSaleReferenceId(BehpardakhtResp.getSaleReferenceId());

				WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
				BehpardakhtResponse verifyResponse = ebiWebervices.verifyRequest(behpardakhtVerifyRequest);

				if (BehpardakhtPaymentUtils.TRANSACTION_APPROVED.equals(verifyResponse.getResponse())) {

					BehpardakhtSettleRequest behpardakhtSettleRequest = new BehpardakhtSettleRequest();

					behpardakhtSettleRequest.setOrderId(oCreditCardTransaction.getTemporyPaymentId().longValue());
					behpardakhtSettleRequest.setSaleOrderId(Long.parseLong(BehpardakhtResp.getSaleOrderId()));
					behpardakhtSettleRequest.setSaleReferenceId(BehpardakhtResp.getSaleReferenceId());
					behpardakhtSettleRequest.setTerminalId(terminalId);
					behpardakhtSettleRequest.setUserName(getUser());
					behpardakhtSettleRequest.setUserPassword(getPassword());

					BehpardakhtResponse settleResponse = ebiWebervices.settleRequest(behpardakhtSettleRequest);

					if (BehpardakhtPaymentUtils.TRANSACTION_SETTLED.equals(settleResponse.getResponse())
							|| BehpardakhtPaymentUtils.TRANSACTION_APPROVED.equals(settleResponse.getResponse())) {
						status = IPGResponseDTO.STATUS_ACCEPTED;
						tnxResultCode = 1;
						errorSpecification = "";
					} else {
						status = IPGResponseDTO.STATUS_REJECTED;
						tnxResultCode = 0;
						errorCode = settleResponse.getResponse();
						errorSpecification = BehpardakhtPaymentUtils.getErrorDescription(errorCode);
						// Different amount in response
						log.error("[PaymentBrokerBehpardakhtECommerceImpl::getReponseData()] - Payment and settle Falied");
					}
				} else {
					status = IPGResponseDTO.STATUS_REJECTED;
					tnxResultCode = 0;
					errorCode = verifyResponse.getResponse();
					errorSpecification = BehpardakhtPaymentUtils.getErrorDescription(errorCode);
					// Different amount in response
					log.error("[PaymentBrokerBehpardakhtECommerceImpl::getReponseData()] - Payment and verification Failed");
				}

			} else {
				status = IPGResponseDTO.STATUS_REJECTED;
				tnxResultCode = 0;
				errorCode = BehpardakhtResp.getResCode();
				errorSpecification = BehpardakhtPaymentUtils.getErrorDescription(errorCode);
				// Different amount in response
				log.error("[PaymentBrokerBehpardakhtECommerceImpl::getReponseData()] - payment response Failed");
			}

		} else {

			status = IPGResponseDTO.STATUS_REJECTED;
			tnxResultCode = 0;
			errorCode = BehpardakhtResp.getResCode();
			errorSpecification = BehpardakhtPaymentUtils.getErrorDescription(errorCode);
			// Different amount in response
			log.error("[PaymentBrokerBehpardakhtECommerceImpl::getReponseData()] - payment response Failed");
		}

		ipgResponseDTO.setStatus(status);
		ipgResponseDTO.setErrorCode(errorCode);
		ipgResponseDTO.setApplicationTransactionId(applicationTransactionId);
		ipgResponseDTO.setAuthorizationCode(merchantTxnReference);
		ipgResponseDTO.setCardType(getStandardCardType(CARD_TYPE_GENERIC));
		updateAuditTransactionByTnxRefNo(ipgResponseDTO.getPaymentBrokerRefNo(), behpardakhtResponse, errorSpecification,
				merchantTxnReference, accelAeroTxnReference, tnxResultCode);

		if (log.isDebugEnabled()) {
			log.debug("[PaymentBrokerBehpardakhtECommerceImpl::getReponseData()] - Staus-" + status + " Error Code-" + errorCode
					+ " Error Spec-" + errorSpecification);
		}

		log.debug("[PaymentBrokerBehpardakhtECommerceImpl::getReponseData()] End");

		return ipgResponseDTO;
	}

	private int getStandardCardType(String card) {
		// [1-MASTER, 2-VISA, 3-AMEX, 4-DINNERS, 5-GENERIC]
		int mapType = 5; // default card type
		if (card != null && !card.equals("") && mapCardType.get(card) != null) {
			String mapTypeStr = (String) mapCardType.get(card);
			mapType = Integer.parseInt(mapTypeStr);
		}
		return mapType;
	}

	public ServiceResponce refund(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		log.debug("[PaymentBrokerBehpardakhtECommerceImpl::refund()] Begin ");
		String errorCode = "";
		String transactionMsg;
		String merchTnxRef;

		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

		// TODO: when refund is a partial refund then refund amount should be round-down all the other cases
		// should be round-up
		String refundAmountStr = BehpardakhtPaymentUtils.getFormattedAmount(creditCardPayment.getAmount(), false);

		Long refundAmount = new Long(refundAmountStr);

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		if (creditCardPayment.getPreviousCCPayment() != null && getRefundEnabled() != null && getRefundEnabled().equals("true")) {
			if (log.isDebugEnabled()) {
				log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
				log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
				log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
				log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
				log.debug("CC Old pay Formatted sAmount : " + refundAmountStr);
				log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());
			}

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}

			merchTnxRef = ccTransaction.getTempReferenceNum();
			BehpardakhtRefundRequest refundRequest = new BehpardakhtRefundRequest();
			String merchantId = getMerchantId();
			Long terminalId = Long.parseLong(merchantId);
			refundRequest.setTerminalId(terminalId);
			refundRequest.setUserPassword(getPassword());
			refundRequest.setRefundAmount(refundAmount);
			refundRequest.setOrderId(ccTransaction.getTemporyPaymentId().longValue());
			refundRequest.setSaleOrderId(ccTransaction.getTemporyPaymentId().longValue());
			refundRequest.setUserName(getUser());
			refundRequest.setSaleReferenceId(Long.parseLong(ccTransaction.getAidCccompnay()));
			String srtmRefResNumber = ccTransaction.getAidCccompnay();

			int srtmRefRegResponse = refundSTRM(ccTransaction, refundRequest, merchTnxRef,
					creditCardPayment.getTemporyPaymentId());

			if (srtmRefRegResponse == 1) {

				log.info(" REFUND Succes, Not a Immidiate refund. ");
				errorCode = "";
				sr.setSuccess(true);
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
						PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);

			} else {
				log.info(" REFUND Fail, Not call REFUND_EXC  ");
				transactionMsg = "Error In Refunding";
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
			}

			sr.setResponseCode(String.valueOf(ccTransaction.getTransactionReference()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, srtmRefResNumber);
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);

		} else {
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);

		}

		log.debug("[PaymentBrokerBehpardakhtECommerceImpl::refund()] End");
		return sr;
	}

	// This Function will be used to call refund for BrokerBehpardakht
	private int refundSTRM(CreditCardTransaction ccTransaction, BehpardakhtRefundRequest refundRequest, String merchTnxRef,
			int temporyPaymentId) throws ModuleException {

		log.debug("[PaymentBrokerBehpardakhtECommerceImpl::refundSTRM()] Begin");
		int tnxResultCode = -20;
		String errorSpecification;

		ServiceResponce serviceResponce = inquiryRequest(ccTransaction);
		if (serviceResponce.isSuccess()) {
			String responseStr = (String) serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE);
			if (isValidRefundRequest(refundRequest, merchTnxRef)
					&& (BehpardakhtPaymentUtils.TRANSACTION_SETTLED.equals(responseStr) || BehpardakhtPaymentUtils.TRANSACTION_APPROVED
							.equals(responseStr))) {
				String updatedMerchTnxRef = merchTnxRef + PaymentConstants.MODE_OF_SERVICE.REFUND;

				CreditCardTransaction ccNewTransaction = auditTransaction(ccTransaction.getTransactionReference(),
						getMerchantId(), updatedMerchTnxRef, temporyPaymentId, bundle.getString("SERVICETYPE_REFUND"),
						refundRequest.toString(), "", getPaymentGatewayName(), null, false);

				String srtmRespStr = null;
				String updatedResponseStr = ccNewTransaction.getResponseText();
				Integer paymentBrokerRefNo;
				int resultCode = -20;
				try {
					WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
					BehpardakhtResponse RefundResponse = ebiWebervices.refundRequest(refundRequest);
					if (RefundResponse != null && RefundResponse.getResponse() != null) {
						srtmRespStr = RefundResponse.getResponse();
					} else {
						srtmRespStr = "";
					}
					paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());

				} catch (Exception e) {
					log.error(e);
					throw new ModuleException("paymentbroker.error.unknown");
				}

				if (log.isDebugEnabled()) {
					log.debug("Refund Response : " + srtmRespStr);
				}

				if (srtmRespStr != null && !"".equals(srtmRespStr)) {
					resultCode = Integer.parseInt(srtmRespStr);
				} else {
					resultCode = 200;
				}

				if (resultCode > 0) {
					// when Unsuccessful operation a negative integer will be returned
					tnxResultCode = 0;
					errorSpecification = BehpardakhtPaymentUtils.getErrorDescription(srtmRespStr);

				} else {
					tnxResultCode = 1;
					errorSpecification = "Successfully refund to user";
				}

				updatedResponseStr = srtmRespStr + "," + errorSpecification;
				updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), updatedResponseStr, errorSpecification,
						refundRequest.getSaleReferenceId().toString(), refundRequest.getSaleOrderId().toString(), tnxResultCode);

				log.debug("[PaymentBrokerBehpardakhtECommerceImpl::refundSTRM()] End");

			}
		}
		return tnxResultCode;

	}

	public ServiceResponce reverse(CreditCardPayment creditCardPayment, String pnr, AppIndicatorEnum appIndicator,
			TnxModeEnum tnxMode) throws ModuleException {

		log.debug("[PaymentBrokerBehpardakhtECommerceImpl::reverse()] Begin ");
		int tnxResultCode = -20;
		String errorCode = "";
		String transactionMsg;
		String merchTnxRef;
		String errorSpecification = "";
		Integer paymentBrokerRefNo;

		CreditCardTransaction ccTransaction = PaymentBrokerUtils.getPaymentBrokerDAO().loadTransaction(
				creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());

		// TODO: when refund is a partial refund then refund amount should be round-down all the other cases
		// should be round-up
		String refundAmountStr = BehpardakhtPaymentUtils.getFormattedAmount(creditCardPayment.getAmount(), false);

		Long refundAmount = new Long(refundAmountStr);

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		ServiceResponce serviceResponce = inquiryRequest(ccTransaction);

		if (serviceResponce.isSuccess()) {
			String responseStr = (String) serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE);
			if ("".equals(responseStr)) {
				log.info("Behpardakht Inquery Request Fail For Reverse  ");
				transactionMsg = "Behpardakht Inquery Request No Error code Returned";
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);

			} else if (BehpardakhtPaymentUtils.TRANSACTION_NOT_SETTLED.equals(responseStr)) {
				String updatedMerchTnxRef = ccTransaction.getTempReferenceNum() + PaymentConstants.MODE_OF_SERVICE.REFUND;
				String updatedResponseStr = "";

				BehpardakhtReversalRequest reverseRequest = new BehpardakhtReversalRequest();
				String merchantId = getMerchantId();
				Long terminalId = Long.parseLong(merchantId);
				reverseRequest.setTerminalId(terminalId);
				reverseRequest.setUserPassword(getPassword());
				reverseRequest.setOrderId(ccTransaction.getTemporyPaymentId().longValue());
				reverseRequest.setSaleOrderId(ccTransaction.getTemporyPaymentId().longValue());
				reverseRequest.setUserName(getUser());
				reverseRequest.setSaleReferenceId(Long.parseLong(ccTransaction.getAidCccompnay()));

				CreditCardTransaction ccNewTransaction = auditTransaction(ccTransaction.getTransactionReference(),
						getMerchantId(), updatedMerchTnxRef, creditCardPayment.getTemporyPaymentId(),
						bundle.getString("SERVICETYPE_REFUND"), reverseRequest.toString(), "", getPaymentGatewayName(), null,
						false);

				WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
				BehpardakhtResponse RefundResponse = ebiWebervices.reversalRequest(reverseRequest);

				paymentBrokerRefNo = new Integer(ccNewTransaction.getTransactionRefNo());

				if (RefundResponse != null && RefundResponse.getResponse() != null) {
					if (BehpardakhtPaymentUtils.TRANSACTION_APPROVED.equals(RefundResponse.getResponse())
							&& BehpardakhtPaymentUtils.TRANSACTION_REVERSED.equals(RefundResponse.getResponse())) {
						errorCode = "";
						tnxResultCode = 1;
						errorSpecification = BehpardakhtPaymentUtils.getErrorDescription(RefundResponse.getResponse());
						sr.setSuccess(true);
						sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
								PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
					} else {
						transactionMsg = "error";
						tnxResultCode = 0;
						errorSpecification = BehpardakhtPaymentUtils.getErrorDescription(RefundResponse.getResponse());
						sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
								PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);
					}
				} else {
					transactionMsg = "error";
					tnxResultCode = 0;
					errorSpecification = "Behpardakht Reverse Request Failed";
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
							PaymentBrokerInternalConstants.MessageCodes.REVERSE_PAYMENT_ERROR);
				}

				updatedResponseStr = RefundResponse + "," + errorSpecification;
				updateAuditTransactionByTnxRefNo(paymentBrokerRefNo.intValue(), updatedResponseStr, errorSpecification,
						reverseRequest.getSaleReferenceId().toString(), reverseRequest.getSaleOrderId().toString(), tnxResultCode);

			} else if (BehpardakhtPaymentUtils.TRANSACTION_SETTLED.equals(responseStr)) {

				merchTnxRef = ccTransaction.getTempReferenceNum();
				BehpardakhtRefundRequest refundRequest = new BehpardakhtRefundRequest();
				String merchantId = getMerchantId();
				Long terminalId = Long.parseLong(merchantId);
				refundRequest.setTerminalId(terminalId);
				refundRequest.setUserPassword(getPassword());
				refundRequest.setRefundAmount(refundAmount);
				refundRequest.setOrderId(ccTransaction.getTemporyPaymentId().longValue());
				refundRequest.setSaleOrderId(ccTransaction.getTemporyPaymentId().longValue());
				refundRequest.setUserName(getUser());
				refundRequest.setSaleReferenceId(Long.parseLong(ccTransaction.getAidCccompnay()));

				int srtmRefRegResponse = refundSTRM(ccTransaction, refundRequest, merchTnxRef,
						creditCardPayment.getTemporyPaymentId());

				if (srtmRefRegResponse == 1) {
					if (isImmediateRefund()) {
						sr.setSuccess(true);
						sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
								PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);

					} else {
						log.info(" REFUND Succes, Not a Immidiate refund. ");
						errorCode = "";
						sr.setSuccess(true);
						sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
								PaymentConstants.SERVICE_RESPONSE_REVERSE_REFUND);
					}
				} else {
					log.info(" REFUND Fail, Not call REFUND_EXC  ");
					transactionMsg = "error";
					sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
				}

				sr.setResponseCode(String.valueOf(ccTransaction.getTransactionReference()));
				sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);
			}
		}

		if (creditCardPayment.getPreviousCCPayment() != null && getRefundEnabled() != null && getRefundEnabled().equals("true")) {
			if (log.isDebugEnabled()) {
				log.debug("Old Temp pay ID : " + creditCardPayment.getPreviousCCPayment().getTemporyPaymentId());
				log.debug("Old PaymentBroker Ref ID : " + creditCardPayment.getPreviousCCPayment().getPaymentBrokerRefNo());
				log.debug("CC Temp pay ID : " + creditCardPayment.getTemporyPaymentId());
				log.debug("CC Old pay Amount : " + creditCardPayment.getAmount());
				log.debug("CC Old pay Formatted sAmount : " + refundAmountStr);
				log.debug("Previous CC Amount : " + creditCardPayment.getPreviousCCPayment().getTotalAmount());
			}

			ServiceResponce serviceResponse = checkPaymentGatewayCompatibility(ccTransaction);
			if (!serviceResponse.isSuccess()) {
				return lookupOtherIPG(serviceResponse, creditCardPayment, pnr, appIndicator, tnxMode,
						ccTransaction.getPaymentGatewayConfigurationName());
			}

			sr.setResponseCode(String.valueOf(ccTransaction.getTransactionReference()));
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);

		} else {
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE,
					PaymentBrokerInternalConstants.MessageCodes.PREVIOUS_PAYMENT_DETAILS_REQUIRED_FOR_REFUND);

		}

		log.debug("[PaymentBrokerBehpardakhtECommerceImpl::reverse()] End");
		return sr;

	}

	public ServiceResponce resolvePartialPayments(Collection colIPGQueryDTO, boolean refundFlag) throws ModuleException {

		log.info("[PaymentBrokerBehpardakhtECommerceImpl::resolvePartialPayments()] Begin");

		Iterator itColIPGQueryDTO = colIPGQueryDTO.iterator();

		Collection<Integer> oResultIF = new ArrayList<Integer>();
		Collection<Integer> oResultIP = new ArrayList<Integer>();
		Collection<Integer> oResultII = new ArrayList<Integer>();
		Collection<Integer> oResultIS = new ArrayList<Integer>();

		DefaultServiceResponse sr = new DefaultServiceResponse(false);
		IPGQueryDTO ipgQueryDTO;
		CreditCardTransaction oCreditCardTransaction;
		String merchTnxRef = "";
		Date currentTime = Calendar.getInstance().getTime();
		Calendar queryValidTime = Calendar.getInstance();
		int srtmRefRegResponse = 0;

		while (itColIPGQueryDTO.hasNext()) {
			ipgQueryDTO = (IPGQueryDTO) itColIPGQueryDTO.next();

			oCreditCardTransaction = loadAuditTransactionByTnxRefNo(ipgQueryDTO.getPaymentBrokerRefNo());

			try {

				queryValidTime.setTime(ipgQueryDTO.getTimestamp());
				queryValidTime.add(Calendar.MINUTE, AppSysParamsUtil.getTimeForPaymentStatusUpdate());

				if (oCreditCardTransaction.getTransactionId() != null && oCreditCardTransaction.getAidCccompnay() != null) {

					ServiceResponce serviceResponce = inquiryRequest(oCreditCardTransaction);

					// If successful payment remains at Bank
					if (serviceResponce.isSuccess()) {
						String responseStr = (String) serviceResponce
								.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE);

						String merchantId = getMerchantId();
						Long terminalId = Long.parseLong(merchantId);

						BigDecimal paymentAmount = ReservationModuleUtils.getReservationBD().getTempPaymentAmount(
								oCreditCardTransaction.getTemporyPaymentId());

						WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();

						if (BehpardakhtPaymentUtils.TRANSACTION_NOT_FOUND.equals(responseStr)) {

							BehpardakhtVerifyRequest behpardakhtVerifyRequest = new BehpardakhtVerifyRequest();

							behpardakhtVerifyRequest.setTerminalId(terminalId);
							behpardakhtVerifyRequest.setUserName(getUser());
							behpardakhtVerifyRequest.setUserPassword(getPassword());
							behpardakhtVerifyRequest.setOrderId(oCreditCardTransaction.getTemporyPaymentId().longValue());
							behpardakhtVerifyRequest.setSaleOrderId(oCreditCardTransaction.getTemporyPaymentId().longValue());
							behpardakhtVerifyRequest.setSaleReferenceId(Long.parseLong(oCreditCardTransaction.getAidCccompnay()));

							BehpardakhtResponse verifyResponse = ebiWebervices.verifyRequest(behpardakhtVerifyRequest);

							if (BehpardakhtPaymentUtils.TRANSACTION_APPROVED.equals(verifyResponse.getResponse())
									|| BehpardakhtPaymentUtils.DUPLICATION_VERIFY.equals(verifyResponse.getResponse())) {

								BehpardakhtSettleRequest behpardakhtSettleRequest = new BehpardakhtSettleRequest();

								behpardakhtSettleRequest.setOrderId(oCreditCardTransaction.getTemporyPaymentId().longValue());
								behpardakhtSettleRequest.setSaleOrderId(oCreditCardTransaction.getTemporyPaymentId().longValue());
								behpardakhtSettleRequest.setSaleReferenceId(Long.parseLong(oCreditCardTransaction
										.getAidCccompnay()));
								behpardakhtSettleRequest.setTerminalId(terminalId);
								behpardakhtSettleRequest.setUserName(getUser());
								behpardakhtSettleRequest.setUserPassword(getPassword());

								BehpardakhtResponse settleResponse = ebiWebervices.settleRequest(behpardakhtSettleRequest);

								if (BehpardakhtPaymentUtils.TRANSACTION_SETTLED.equals(settleResponse.getResponse())
										|| BehpardakhtPaymentUtils.TRANSACTION_APPROVED.equals(settleResponse.getResponse())) {

									boolean isResConfirmationSuccess = ConfirmReservationUtil
											.isReservationConfirmationSuccess(ipgQueryDTO);

									if (isResConfirmationSuccess) {
										if (log.isDebugEnabled()) {
											log.debug("Status moving to RS:" + oCreditCardTransaction.getTemporyPaymentId());
										}

									} else {
										if (refundFlag) {
//											TODO: refund was to check with phase 2 remove comment when refund enable
//											srtmRefRegResponse = partialPaymentRefund(terminalId, oCreditCardTransaction, paymentAmount);

											if (srtmRefRegResponse == 1) {
												// Change State to 'IS'
												oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
												if (log.isDebugEnabled()) {
													log.debug("Status moving to IS:"
															+ oCreditCardTransaction.getTemporyPaymentId());
												}
											} else {
												// Change State to 'IF'
												oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
												if (log.isDebugEnabled()) {
													log.debug("Status moving to IF:"
															+ oCreditCardTransaction.getTemporyPaymentId());
												}
											}
										} else {
											// Change State to 'IP'
											oResultIP.add(oCreditCardTransaction.getTemporyPaymentId());
											if (log.isDebugEnabled()) {
												log.debug("Status moving to IP:" + oCreditCardTransaction.getTemporyPaymentId());
											}
										}
									}
								} else {
									// No Payments exist
									// Update status to 'II'
									if (currentTime.getTime() > queryValidTime.getTimeInMillis()) {
										oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
										log.info("Status moing to II:" + oCreditCardTransaction.getTemporyPaymentId());
									} else {
										// Can not move payment status. with in status change for 3D secure payments
										log.info("Status not moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
									}
								}
							} else {
								// No Payments exist
								// Update status to 'II'
								if (currentTime.getTime() > queryValidTime.getTimeInMillis()) {
									oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
									log.info("Status moing to II:" + oCreditCardTransaction.getTemporyPaymentId());
								} else {
									// Can not move payment status. with in status change for 3D secure payments
									log.info("Status not moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
								}
							}
						} else if (BehpardakhtPaymentUtils.TRANSACTION_SETTLED.equals(responseStr)) {

							boolean isResConfirmationSuccess = ConfirmReservationUtil
									.isReservationConfirmationSuccess(ipgQueryDTO);

							if (isResConfirmationSuccess) {
								if (log.isDebugEnabled()) {
									log.debug("Status moving to RS:" + oCreditCardTransaction.getTemporyPaymentId());
								}

							} else {
								if (refundFlag) {
//									TODO: refund was to check with phase 2 remove comment when refund enable
//									srtmRefRegResponse = partialPaymentRefund(terminalId, oCreditCardTransaction, paymentAmount);

									if (srtmRefRegResponse == 1) {
										// Change State to 'IS'
										oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
										if (log.isDebugEnabled()) {
											log.debug("Status moving to IS:" + oCreditCardTransaction.getTemporyPaymentId());
										}
									} else {
										// Change State to 'IF'
										oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
										if (log.isDebugEnabled()) {
											log.debug("Status moving to IF:" + oCreditCardTransaction.getTemporyPaymentId());
										}
									}
								} else {
									// Change State to 'IP'
									oResultIP.add(oCreditCardTransaction.getTemporyPaymentId());
									if (log.isDebugEnabled()) {
										log.debug("Status moving to IP:" + oCreditCardTransaction.getTemporyPaymentId());
									}
								}
							}

						} else if (BehpardakhtPaymentUtils.TRANSACTION_APPROVED.equals(responseStr)
								|| BehpardakhtPaymentUtils.TRANSACTION_NOT_SETTLED.equals(responseStr)) {

							BehpardakhtSettleRequest behpardakhtSettleRequest = new BehpardakhtSettleRequest();

							behpardakhtSettleRequest.setOrderId(oCreditCardTransaction.getTemporyPaymentId().longValue());
							behpardakhtSettleRequest.setSaleOrderId(oCreditCardTransaction.getTemporyPaymentId().longValue());
							behpardakhtSettleRequest.setSaleReferenceId(Long.parseLong(oCreditCardTransaction.getAidCccompnay()));
							behpardakhtSettleRequest.setTerminalId(terminalId);
							behpardakhtSettleRequest.setUserName(getUser());
							behpardakhtSettleRequest.setUserPassword(getPassword());

							BehpardakhtResponse settleResponse = ebiWebervices.settleRequest(behpardakhtSettleRequest);

							if (BehpardakhtPaymentUtils.TRANSACTION_SETTLED.equals(settleResponse.getResponse())
									|| BehpardakhtPaymentUtils.TRANSACTION_APPROVED.equals(settleResponse.getResponse())) {

								boolean isResConfirmationSuccess = ConfirmReservationUtil
										.isReservationConfirmationSuccess(ipgQueryDTO);

								if (isResConfirmationSuccess) {
									if (log.isDebugEnabled()) {
										log.debug("Status moving to RS:" + oCreditCardTransaction.getTemporyPaymentId());
									}

								} else {
									if (refundFlag) {
//									 	TODO: refund was to check with phase 2 remove comment when refund enable
//										srtmRefRegResponse = partialPaymentRefund(terminalId, oCreditCardTransaction, paymentAmount);

										if (srtmRefRegResponse == 1) {
											// Change State to 'IS'
											oResultIS.add(oCreditCardTransaction.getTemporyPaymentId());
											if (log.isDebugEnabled()) {
												log.debug("Status moving to IS:" + oCreditCardTransaction.getTemporyPaymentId());
											}
										} else {
											// Change State to 'IF'
											oResultIF.add(oCreditCardTransaction.getTemporyPaymentId());
											if (log.isDebugEnabled()) {
												log.debug("Status moving to IF:" + oCreditCardTransaction.getTemporyPaymentId());
											}
										}
									} else {
										// Change State to 'IP'
										oResultIP.add(oCreditCardTransaction.getTemporyPaymentId());
										if (log.isDebugEnabled()) {
											log.debug("Status moving to IP:" + oCreditCardTransaction.getTemporyPaymentId());
										}
									}
								}
							} else {
								// No Payments exist
								// Update status to 'II'
								if (currentTime.getTime() > queryValidTime.getTimeInMillis()) {
									oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
									log.info("Status moing to II:" + oCreditCardTransaction.getTemporyPaymentId());
								} else {
									// Can not move payment status. with in status change for 3D secure payments
									log.info("Status not moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
								}
							}

						} else {

							// No Payments exist
							// Update status to 'II'
							if (currentTime.getTime() > queryValidTime.getTimeInMillis()) {
								oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
								log.info("Status moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
							} else {
								// Can not move payment status. with in status change for 3D secure payments
								log.info("Status not moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
							}

						}
					} else {
						// No Payments exist
						// Update status to 'II'
						if (currentTime.getTime() > queryValidTime.getTimeInMillis()) {
							oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
							log.info("Status moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
						} else {
							// Can not move payment status. with in status change for 3D secure payments
							log.info("Status not moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
						}
					}
				}
				// No payment detail at bank
				else {
					// No Payments exist
					// Update status to 'II'
					if (currentTime.getTime() > queryValidTime.getTimeInMillis()) {
						oResultII.add(oCreditCardTransaction.getTemporyPaymentId());
						log.info("Status moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
					} else {
						// Can not move payment status. with in status change for 3D secure payments
						log.info("Status not moving to II:" + oCreditCardTransaction.getTemporyPaymentId());
					}
				}
			} catch (ModuleException e) {
				log.error("Scheduler::ResolvePayment FAILED!!! " + e.getMessage(), e);
			}
		}

		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IF, oResultIF);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_II, oResultII);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IP, oResultIP);
		sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_STATUS_IS, oResultIS);

		sr.setSuccess(true);

		log.info("[PaymentBrokerBehpardakhtECommerceImpl::resolvePartialPayments()] End");
		return sr;

	}

	public ServiceResponce inquiryRequest(CreditCardTransaction oCreditCardTransaction) throws ModuleException {

		log.debug("[PaymentBrokerBehpardakhtECommerceImpl::inquiryRequest()] Begin");

		String errorCode;
		String transactionMsg;

		String queryParamString = "RefNo:" + oCreditCardTransaction.getTransactionId() + ",MerchantId:" + getMerchantId();

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		log.debug("Query Request Params : " + queryParamString);

		BehpardakhtInquiryRequest inquiryRequest = new BehpardakhtInquiryRequest();

		String merchantId = getMerchantId();
		Long terminalId = Long.parseLong(merchantId);

		inquiryRequest.setTerminalId(terminalId);
		inquiryRequest.setUserName(getUser());
		inquiryRequest.setUserPassword(getPassword());
		inquiryRequest.setOrderId(oCreditCardTransaction.getTemporyPaymentId().longValue());
		inquiryRequest.setSaleOrderId(oCreditCardTransaction.getTemporyPaymentId().longValue());
		inquiryRequest.setSaleReferenceId(Long.parseLong(oCreditCardTransaction.getAidCccompnay()));

		WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
		BehpardakhtResponse inquiryResponse = ebiWebervices.inquiryRequest(inquiryRequest);

		log.debug("Query Response : " + inquiryResponse.getResponse());

		// positive pay amount means verification is correct, negative implies
		// its an error code
		if (inquiryResponse.getResponse() != null && inquiryResponse.getResponse().length() > 0) {
			errorCode = inquiryResponse.getResponse();
			transactionMsg = BehpardakhtPaymentUtils.getErrorDescription(inquiryResponse.getResponse());
			sr.setSuccess(true);
			sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE, oCreditCardTransaction.getTransactionId());
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
		} else {
			errorCode = "";
			transactionMsg = "Behpardakht PGW web service call fail";
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
		}

		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, oCreditCardTransaction.getTransactionId());
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);

		log.debug("[PaymentBrokerBehpardakhtECommerceImpl::inquiryRequest()] End");

		return sr;
	}

	public ServiceResponce settle(CreditCardTransaction oCreditCardTransaction, IPGConfigsDTO ipgConfigsDTO, QUERY_CALLER caller)
			throws ModuleException {

		log.debug("[PaymentBrokerBehpardakhtECommerceImpl::settle()] Begin");

		int tnxResultCode;
		String errorCode;
		String transactionMsg;
		String status = null;
		String errorSpecification = null;

		String merchantTxnReference = oCreditCardTransaction.getTempReferenceNum();

		String queryParamString = "RefNo:" + oCreditCardTransaction.getTransactionId() + ",MerchantId:" + getMerchantId();

		CreditCardTransaction ccTransaction = auditTransaction(oCreditCardTransaction.getTransactionReference(), getMerchantId(),
				merchantTxnReference, new Integer(oCreditCardTransaction.getTemporyPaymentId()),
				bundle.getString("SERVICETYPE_QUERY"), queryParamString, "", getPaymentGatewayName(), null, false);

		DefaultServiceResponse sr = new DefaultServiceResponse(false);

		BehpardakhtSettleRequest settleRequest = new BehpardakhtSettleRequest();
		String merchantId = getMerchantId();
		Long terminalId = Long.parseLong(merchantId);

		settleRequest.setTerminalId(terminalId);
		settleRequest.setUserName(getUser());
		settleRequest.setUserPassword(getPassword());
		settleRequest.setOrderId(oCreditCardTransaction.getTemporyPaymentId().longValue());
		settleRequest.setSaleOrderId(oCreditCardTransaction.getTemporyPaymentId().longValue());
		settleRequest.setSaleReferenceId(Long.parseLong(oCreditCardTransaction.getAidCccompnay()));

		WSClientBD ebiWebervices = ReservationModuleUtils.getWSClientBD();
		BehpardakhtResponse settleResponse = ebiWebervices.settleRequest(settleRequest);

		log.debug("Query Response : " + settleResponse.getResponse());

		// positive pay amount means verification is correct, negative implies
		// its an error code
		if ((BehpardakhtPaymentUtils.TRANSACTION_APPROVED).equals(settleResponse.getResponse())) {
			status = IPGResponseDTO.STATUS_ACCEPTED;
			errorCode = "";
			transactionMsg = IPGResponseDTO.STATUS_ACCEPTED;
			tnxResultCode = 1;
			errorSpecification = " " + caller.toString();
			sr.setSuccess(true);
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, PaymentConstants.SERVICE_RESPONSE_QUERYDR);
			sr.addResponceParam(PaymentConstants.PARAM_QUERY_DR_TXN_CODE, oCreditCardTransaction.getTransactionId());
		} else {
			status = IPGResponseDTO.STATUS_REJECTED;
			errorCode = settleResponse.getResponse();
			transactionMsg = BehpardakhtPaymentUtils.getErrorDescription(errorCode);
			;
			tnxResultCode = 0;
			errorSpecification = "Status:" + status + " Error code:" + errorCode + " Error Desc:" + transactionMsg + " Caller:"
					+ caller.toString();
			sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE, transactionMsg);
		}

		updateAuditTransactionByTnxRefNo(ccTransaction.getTransactionRefNo(), settleResponse.getResponse(), errorSpecification,
				oCreditCardTransaction.getTransactionId(), oCreditCardTransaction.getTransactionId(), tnxResultCode);

		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_AUTHORIZATION_ID, oCreditCardTransaction.getTransactionId());
		sr.addResponceParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_CODE, errorCode);

		log.debug("[PaymentBrokerBehpardakhtECommerceImpl::settle()] End");

		return sr;
	}

	private boolean isValidRefundRequest(BehpardakhtRefundRequest refundRequest, String merchTnxRef) {

		if (refundRequest == null || merchTnxRef == null) {
			log.debug("Missing param in Behpardakht SRTM request");
			return false;
		}

		if (refundRequest.getOrderId() == null || refundRequest.getRefundAmount() == null
				|| refundRequest.getSaleOrderId() == null || refundRequest.getSaleReferenceId() == null
				|| refundRequest.getRefundAmount().doubleValue() < 0 || refundRequest.getTerminalId() == null
				|| refundRequest.getUserName() == null || refundRequest.getUserPassword() == null) {
			log.debug("Invalid param found in BehpardakhtRefundRequest");
			return false;
		}

		return true;
	}
	
	// common method called inside the resolve partial payment 
	private int partialPaymentRefund(Long terminalId, CreditCardTransaction oCreditCardTransaction, BigDecimal paymentAmount) {

		BehpardakhtRefundRequest refundRequest = new BehpardakhtRefundRequest();
		String merchTnxRef = "";
		int srtmRefRegResponse = 0;
		refundRequest.setTerminalId(terminalId);
		refundRequest.setUserPassword(getPassword());
		refundRequest.setRefundAmount(paymentAmount.longValue());
		refundRequest.setOrderId(oCreditCardTransaction.getTemporyPaymentId().longValue());
		refundRequest.setSaleOrderId(oCreditCardTransaction.getTemporyPaymentId().longValue());
		refundRequest.setUserName(getUser());
		refundRequest
				.setSaleReferenceId(Long.parseLong(oCreditCardTransaction.getAidCccompnay()));

		merchTnxRef = oCreditCardTransaction.getTempReferenceNum();

		try {
			srtmRefRegResponse =  refundSTRM(oCreditCardTransaction, refundRequest, merchTnxRef,
					oCreditCardTransaction.getTemporyPaymentId());
		} catch (ModuleException e) {
			log.error("BehpardakhtRefundRequest Failed " + e.getCause());
		}
		return srtmRefRegResponse;
	}

}
