/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.bl.migs3party;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * The payment broker response code mapping for MIGS
 * 
 * @author Indika
 */
public class PaymentResponseCodesUtil {

	private static Log log = LogFactory.getLog(PaymentResponseCodesUtil.class);

	private static final String ERROR_CODE_PREFIX = "migs3.";

	/**
	 * This method filter the application sepecific error code on specified priority
	 * 
	 * @param migsResp
	 * @return
	 */
	public static String getFilteredErrorCode(MIGS3PResponse migsResp) {

		String errorCode = "";

		// #1. Check for Issuer Response Code
		errorCode = getMatchingIssuerResponseCode(migsResp.getAcqResponseCode());

		// #2. Check for Txn Error Code
		if (errorCode.equals("")) {
			errorCode = getMatchingTxnErrorCode(migsResp.getTxnResponseCode());
		}

		// #3. Check for 3D Error Code
		if (errorCode.equals("")) {
			errorCode = getMatching3DStatusCode(migsResp.getVerStatus3DS());
		}
		if (log.isDebugEnabled()) {
			log.debug("[PaymentResponseCodesUtil::getFilteredErrorCode]Return Code :" + errorCode);
		}
		return errorCode;
	}

	/**
	 * This method filter the application sepecific error code on specified priority
	 * 
	 * @param migsResp
	 * @return
	 */
	public static String getFilteredErrorCodeMOTO(MIGS3PResponse migsResp) {

		String errorCode = "";

		// #1. Check for Issuer Response Code
		errorCode = getMatchingIssuerResponseCode(migsResp.getAcqResponseCode());

		// #2. Check for Txn Error Code
		if (errorCode.equals("")) {
			errorCode = getMatchingTxnErrorCode(migsResp.getTxnResponseCode());
		}

		/*
		 * // #3. Check for 3D Error Code if (errorCode.equals("")) { errorCode =
		 * getMatching3DStatusCode(migsResp.getVerStatus3DS()); }
		 */
		if (log.isDebugEnabled()) {
			log.debug("[PaymentResponseCodesUtil::getFilteredErrorCodeMOTO]Return Code :" + errorCode);
		}
		return errorCode;
	}

	/**
	 * Returns the application specific error code for the Issuer response
	 * 
	 * @param vResponseCode
	 *            the Issuer reponse code
	 * @return the matching error code
	 */
	private static String getMatchingIssuerResponseCode(String vResponseCode) {
		String result = "";

		// check if a response code code is null or empty
		if (vResponseCode != null && vResponseCode != "" && !vResponseCode.equals("") && !vResponseCode.isEmpty()) {
			if (log.isDebugEnabled()) {
				log.debug("[PaymentResponseCodesUtil::getMatchingIssuerResponseCode]Issuer Error Code found :" + vResponseCode);
			}

			int input = 0;
			try {
				input = Integer.parseInt(vResponseCode);
				switch (input) {
				case 0:
					result = ERROR_CODE_PREFIX + "000";
					break;
				case 1:
					result = ERROR_CODE_PREFIX + "001";
					break;
				case 2:
					result = ERROR_CODE_PREFIX + "002";
					break;
				case 3:
					result = ERROR_CODE_PREFIX + "003";
					break;
				case 4:
					result = ERROR_CODE_PREFIX + "004";
					break;
				case 5:
					result = ERROR_CODE_PREFIX + "005";
					break;
				case 7:
					result = ERROR_CODE_PREFIX + "006";
					break;
				case 12:
					result = ERROR_CODE_PREFIX + "007";
					break;
				case 14:
					result = ERROR_CODE_PREFIX + "008";
					break;
				case 15:
					result = ERROR_CODE_PREFIX + "009";
					break;
				case 33:
					result = ERROR_CODE_PREFIX + "010";
					break;
				case 34:
					result = ERROR_CODE_PREFIX + "011";
					break;
				case 36:
					result = ERROR_CODE_PREFIX + "012";
					break;
				case 39:
					result = ERROR_CODE_PREFIX + "013";
					break;
				case 41:
					result = ERROR_CODE_PREFIX + "014";
					break;
				case 43:
					result = ERROR_CODE_PREFIX + "015";
					break;
				case 51:
					result = ERROR_CODE_PREFIX + "016";
					break;
				case 54:
					result = ERROR_CODE_PREFIX + "017";
					break;
				case 57:
					result = ERROR_CODE_PREFIX + "018";
					break;
				case 59:
					result = ERROR_CODE_PREFIX + "019";
					break;
				case 62:
					result = ERROR_CODE_PREFIX + "020";
					break;
				case 65:
					result = ERROR_CODE_PREFIX + "021";
					break;
				case 91:
					result = ERROR_CODE_PREFIX + "022";
					break;
				default:
					result = "";
				}
			} catch (NumberFormatException e) {
				// If return code is not an Integer return ""
				log.error(e);
				result = "";
			}

			return result;

		} else {
			return "";
		}

	}

	/**
	 * Returns the application specific error code for the transaction response
	 * 
	 * @param vResponseCode
	 *            the transaction reponse code
	 * @return the matching error code
	 */
	private static String getMatchingTxnErrorCode(String vResponseCode) {
		String result = "";
		if (log.isDebugEnabled()) {
			log.debug("[PaymentResponseCodesUtil::getMatchingTxnErrorCode]Txn Error Code found :" + vResponseCode);
		}

		// check if a single digit response code
		if (vResponseCode.length() == 1) {

			// Java cannot switch on a string so turn everything to a char
			char input = vResponseCode.charAt(0);

			switch (input) {
			case '0':
				result = ERROR_CODE_PREFIX + "000";
				break;
			case '1':
				result = ERROR_CODE_PREFIX + "023";
				break;
			case '2':
				result = ERROR_CODE_PREFIX + "024";
				break;
			case '3':
				result = ERROR_CODE_PREFIX + "025";
				break;
			case '4':
				result = ERROR_CODE_PREFIX + "026";
				break;
			case '5':
				result = ERROR_CODE_PREFIX + "027";
				break;
			case '6':
				result = ERROR_CODE_PREFIX + "028";
				break;
			case '7':
				result = ERROR_CODE_PREFIX + "029";
				break;
			case '8':
				result = ERROR_CODE_PREFIX + "030";
				break;
			case '9':
				result = ERROR_CODE_PREFIX + "031";
				break;
			case 'A':
				result = ERROR_CODE_PREFIX + "032";
				break;
			case 'C':
				result = ERROR_CODE_PREFIX + "033";
				break;
			case 'D':
				result = ERROR_CODE_PREFIX + "034";
				break;
			case 'F':
				result = ERROR_CODE_PREFIX + "035";
				break;
			case 'I':
				result = ERROR_CODE_PREFIX + "035";
				break;
			case 'L':
				result = ERROR_CODE_PREFIX + "036";
				break;
			case 'N':
				result = ERROR_CODE_PREFIX + "037";
				break;
			case 'P':
				result = ERROR_CODE_PREFIX + "038";
				break;
			case 'R':
				result = ERROR_CODE_PREFIX + "039";
				break;
			case 'S':
				result = ERROR_CODE_PREFIX + "040";
				break;
			case 'T':
				result = ERROR_CODE_PREFIX + "035";
				break;
			case 'U':
				result = ERROR_CODE_PREFIX + "035";
				break;
			case 'V':
				result = ERROR_CODE_PREFIX + "035";
				break;
			case '?':
				result = ERROR_CODE_PREFIX + "023";
				break;
			default:
				result = ERROR_CODE_PREFIX + "023";
			}
			return result;
		} else {
			return "";
		}
	}

	/**
	 * Returns the application specific error code for the 3DS status
	 * 
	 * @param vStatus
	 *            the 3DS status code
	 * @return the application specific error code for the 3DS status
	 */
	private static String getMatching3DStatusCode(String vStatus) {

		if (log.isDebugEnabled()) {
			log.debug("[PaymentResponseCodesUtil::getMatching3DStatusCode]3D Status Error Code found :" + vStatus);
		}
		String result = "";
		if (vStatus != null && !vStatus.equals("")) {

			if (vStatus.equalsIgnoreCase("Unsupported") || vStatus.equals("No Value Returned")) {
				result = ERROR_CODE_PREFIX + "041";
			} else {
				// Java cannot switch on a string so turn everything to a character
				char input = vStatus.charAt(0);
				switch (input) {
				// case 'Y' : result = "The cardholder was successfully authenticated."; break;
				case 'E':
					result = ERROR_CODE_PREFIX + "035";
					break;
				case 'N':
					result = ERROR_CODE_PREFIX + "035";
					break;
				case 'U':
					result = ERROR_CODE_PREFIX + "042";
					break;
				case 'F':
					result = ERROR_CODE_PREFIX + "043";
					break;
				case 'A':
					result = ERROR_CODE_PREFIX + "044";
					break;
				case 'D':
					result = ERROR_CODE_PREFIX + "046";
					break;
				case 'C':
					result = ERROR_CODE_PREFIX + "047";
					break;
				case 'S':
					result = ERROR_CODE_PREFIX + "045";
					break;
				case 'P':
					result = ERROR_CODE_PREFIX + "043";
					break;
				case 'I':
					result = ERROR_CODE_PREFIX + "023";
					break;
				default:
					result = ERROR_CODE_PREFIX + "023";
					break;
				}
			}
		} else {
			result = "023";
		}
		return result;
	}

	/**
	 * This function uses the returned status code retrieved from the Digital Response and returns an appropriate
	 * description for the code
	 * 
	 * @param vResponseCode
	 *            String containing the vpc_TxnResponseCode
	 * @return description String containing the appropriate description
	 */
	public static String getResponseDescription(String vResponseCode) {

		String result = "";

		// check if a single digit response code
		if (vResponseCode.length() == 1) {

			// Java cannot switch on a string so turn everything to a char
			char input = vResponseCode.charAt(0);

			switch (input) {
			case '0':
				result = "Transaction Successful.";
				break;
			case '1':
				result = "Unknown Error.";
				break;
			case '2':
				result = "Bank Declined Transaction.";
				break;
			case '3':
				result = "No Reply from Bank.";
				break;
			case '4':
				result = "Expired Card.";
				break;
			case '5':
				result = "Insufficient Funds.";
				break;
			case '6':
				result = "Error Communicating with Bank.";
				break;
			case '7':
				result = "Payment Server System Error.";
				break;
			case '8':
				result = "Transaction Type Not Supported.";
				break;
			case '9':
				result = "Bank declined transaction (Do not contact Bank).";
				break;
			case 'A':
				result = "Transaction Aborted.";
				break;
			case 'C':
				result = "Transaction Cancelled.";
				break;
			case 'D':
				result = "Deferred transaction has been received and is awaiting processing.";
				break;
			case 'E':
				result = "Payment Server System Error.";
				break;
			case 'F':
				result = "3D Secure Authentication failed.";
				break;
			case 'I':
				result = "Card Security Code verification failed.";
				break;
			case 'L':
				result = "Shopping Transaction Locked (Please try the transaction again later).";
				break;
			case 'N':
				result = "Cardholder is not enrolled in Authentication Scheme.";
				break;
			case 'P':
				result = "Transaction has been received by the Payment Adaptor and is being processed.";
				break;
			case 'R':
				result = "Transaction was not processed - Reached limit of retry attempts allowed.";
				break;
			case 'S':
				result = "Duplicate SessionID (OrderInfo).";
				break;
			case 'T':
				result = "Address Verification Failed.";
				break;
			case 'U':
				result = "Card Security Code Failed.";
				break;
			case 'V':
				result = "Address Verification and Card Security Code Failed.";
				break;
			case '?':
				result = "Transaction status is unknown.";
				break;
			default:
				result = "Unable to be determined.";
			}

			return result;
		} else {
			return "No Value Returned.";
		}
	}

}
