package com.isa.thinair.paymentbroker.core.bl.benefit;



//import com.aciworldwide.commerce.gateway.plugins.e24PaymentPipe;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGRequestDTO;
import com.isa.thinair.paymentbroker.core.bl.benefit.BenefitResponse.RESPONCECODE;
import com.isa.thinair.paymentbroker.core.bl.benefit.BenefitResponse.RESULTSTATUS;



public class BenefitUtil {
	
	private static final String ERROR_CODE_PREFIX = "benefit.";
	
	public static void addAdditionalData(IPGRequestDTO ipgRequestDTO, e24PaymentPipe pipe) {
		String phoneNo = ipgRequestDTO.getContactMobileNumber();
		if (phoneNo ==  null || phoneNo.trim().isEmpty()) {
			phoneNo = ipgRequestDTO.getContactPhoneNumber();
		}
		if (phoneNo != null) 
			phoneNo = StringUtil.extractNumberFromString(phoneNo);
		String  email = ipgRequestDTO.getEmail();		
				
		if (email != null && !email.trim().isEmpty())
			pipe.setUdf1(email);		
		if (phoneNo != null && !phoneNo.trim().isEmpty())
			pipe.setUdf2(phoneNo);			
	}
	
	public static String concatPaymentRequestString(IPGRequestDTO ipgRequestDTO, e24PaymentPipe pipe) {
		StringBuilder request = new StringBuilder();
		if (ipgRequestDTO != null) {
			request.append("PaymentID:").append(pipe.getPaymentId()).append(",");
			request.append("Action:").append(pipe.getAction()).append(",");
			request.append("Currency:").append(ipgRequestDTO.getIpgIdentificationParamsDTO().getPaymentCurrencyCode()).append(",");
			request.append("Amount:").append(pipe.getAmt()).append(",");
			request.append("Language:").append(pipe.getLanguage()).append(",");
			request.append("TrackId:").append(pipe.getTrackId()).append(",");
			request.append("Udf5:").append(pipe.getUdf5()).append(",");
			request.append("ResponseURL:").append(pipe.getResponseURL()).append(",");
			request.append("Udf1:").append(pipe.getUdf1()).append(",");
			request.append("Udf2:").append(pipe.getUdf2()).append(",");
			request.append("Payment URL:").append(pipe.getPaymentPage()).append(",");
			request.append("Udf3:").append(pipe.getUdf3()).append(",");
			request.append("Udf4:").append(pipe.getUdf4()).append(",");
			request.append("Session ID:").append(ipgRequestDTO.getSessionID());
		}		
		return request.toString();
	}
	
	public static String getErrorCodeWithPrefix(RESULTSTATUS result, RESPONCECODE responcecode) {
		return 	ERROR_CODE_PREFIX + result.toString() + "." + responcecode.toString();	
	}
	
	public static String getErrorCodeDescription(RESPONCECODE resCode) {
		String despn = "";
		switch(resCode) {
			case RESCODE00 : despn = "Transaction was approved."; break;
			case RESCODE05 : despn = "There is an issue with the card's bank."; break;
			case RESCODE33 : despn = "The customer card is expired."; break;
			case RESCODE51 : despn = "Insufficient funds."; break;
			case RESCODE54 : despn = "The customer card is expired."; break;
			case RESCODE55 : despn = "Incorrect pin number."; break;
			case RESCODE61 : despn = "The card exceeds withdrawal amount limit."; break;
			case RESCODE91 : despn = "The bank is disconnected at the moment."; break;
			default : despn = "Not identified"; break;
		}		
		return despn;
	}

}
