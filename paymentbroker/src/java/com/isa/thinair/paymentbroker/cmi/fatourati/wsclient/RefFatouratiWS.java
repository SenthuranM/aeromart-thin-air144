/**
 * RefFatouratiWS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.paymentbroker.cmi.fatourati.wsclient;

public interface RefFatouratiWS extends java.rmi.Remote {
    public com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PayByRefFatGenRes genRefFatourati(com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PayByRefFatGenReq arg0) throws java.rmi.RemoteException;
}
