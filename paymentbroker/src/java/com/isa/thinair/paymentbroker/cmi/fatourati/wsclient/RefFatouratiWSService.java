/**
 * RefFatouratiWSService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.paymentbroker.cmi.fatourati.wsclient;

public interface RefFatouratiWSService extends javax.xml.rpc.Service {
    public java.lang.String getRefFatouratiWSPortAddress();

    public com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.RefFatouratiWS getRefFatouratiWSPort() throws javax.xml.rpc.ServiceException;

    public com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.RefFatouratiWS getRefFatouratiWSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
