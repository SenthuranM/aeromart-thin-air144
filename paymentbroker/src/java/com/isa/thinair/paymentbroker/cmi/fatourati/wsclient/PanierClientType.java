/**
 * PanierClientType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.paymentbroker.cmi.fatourati.wsclient;

public class PanierClientType  implements java.io.Serializable {
    /* Code retour du résultat du traitement de
     * 						l’article.
     * 						Obligatoire 000 si ok. */
    private java.lang.String codeRetour;

    /* Identifiant du service du créancier dans le SI
     * 						Fatourati.
     * 						Communiqué par le CMI au lancement du service.
     * 						Un créancier peut avoir plusieurs services pour lesquels il
     * souhaite
     * 						avoir une référence.
     * 						Pour chaque creanceId un créancier peut générer jusqu’à 1 millions
     * de
     * 						référence Fatourati.
     * 						L’ajout d’un creanceID. */
    private java.lang.String creanceId;

    /* Date d’émission de l’article */
    private java.lang.String dateArticle;

    /* Date d’expiration de la Réf Fatourati. Au-delà
     * 						de cette date, l’article ne sera plus payable
     * 						via réf Fatourati. Si non communiquée le SI
     * 						Fatourati définie une valuer par défaut de … .
     * 						Les Réf non payé pendant une période donnée
     * 						seront supprimées suivant la politique
     * 						d’archivage du CMI. */
    private java.lang.String dateExpiration;

    /* Description de l’article */
    private java.lang.String description;

    /* Etat de paiement de l’article. */
    private com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PanierClientTypeEtat etat;

    /* Liste qui contient des valeurs supplémentaires. */
    private com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.ParamsType[] globalParams;

    /* Identifiant de l’article, unique par “Référence
     * 						Fatourati” */
    private java.lang.String idArticle;

    /* Mail du client */
    private java.lang.String mailClient;

    /* Numéro Mobile du client. Optionnel. Obligatoire
     * 						si le créancier veut envoyer un SMS par mail. */
    private java.lang.String mobileClient;

    /* Montant de l’article en dirhams */
    private java.math.BigDecimal montant;

    /* Code retour du résultat du traitement de
     * 						l’article */
    private java.lang.String msgRetour;

    /* Nom ou désignation du client (ça peut être un
     * 						numéro de contrat ou d’entreprise.
     * 						C’est la désignation qui sera affiché sur le canal de paiement */
    private java.lang.String nomClient;

    /* identifiant du client, utilisé pour regrouper
     * 						plusieurs articles en une seule “Référence
     * 						Fatourati” si action = 1
     * 						(add_group) */
    private java.lang.String refClient;

    /* Référence Fatourati si déjà générée en cas
     * 						d’action != {0, 1} */
    private java.lang.String refFatourati;

    /* Référence de paiement si réglé */
    private java.lang.String refPaiement;

    public PanierClientType() {
    }

    public PanierClientType(
           java.lang.String codeRetour,
           java.lang.String creanceId,
           java.lang.String dateArticle,
           java.lang.String dateExpiration,
           java.lang.String description,
           com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PanierClientTypeEtat etat,
           com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.ParamsType[] globalParams,
           java.lang.String idArticle,
           java.lang.String mailClient,
           java.lang.String mobileClient,
           java.math.BigDecimal montant,
           java.lang.String msgRetour,
           java.lang.String nomClient,
           java.lang.String refClient,
           java.lang.String refFatourati,
           java.lang.String refPaiement) {
           this.codeRetour = codeRetour;
           this.creanceId = creanceId;
           this.dateArticle = dateArticle;
           this.dateExpiration = dateExpiration;
           this.description = description;
           this.etat = etat;
           this.globalParams = globalParams;
           this.idArticle = idArticle;
           this.mailClient = mailClient;
           this.mobileClient = mobileClient;
           this.montant = montant;
           this.msgRetour = msgRetour;
           this.nomClient = nomClient;
           this.refClient = refClient;
           this.refFatourati = refFatourati;
           this.refPaiement = refPaiement;
    }


    /**
     * Gets the codeRetour value for this PanierClientType.
     * 
     * @return codeRetour   * Code retour du résultat du traitement de
     * 						l’article.
     * 						Obligatoire 000 si ok.
     */
    public java.lang.String getCodeRetour() {
        return codeRetour;
    }


    /**
     * Sets the codeRetour value for this PanierClientType.
     * 
     * @param codeRetour   * Code retour du résultat du traitement de
     * 						l’article.
     * 						Obligatoire 000 si ok.
     */
    public void setCodeRetour(java.lang.String codeRetour) {
        this.codeRetour = codeRetour;
    }


    /**
     * Gets the creanceId value for this PanierClientType.
     * 
     * @return creanceId   * Identifiant du service du créancier dans le SI
     * 						Fatourati.
     * 						Communiqué par le CMI au lancement du service.
     * 						Un créancier peut avoir plusieurs services pour lesquels il
     * souhaite
     * 						avoir une référence.
     * 						Pour chaque creanceId un créancier peut générer jusqu’à 1 millions
     * de
     * 						référence Fatourati.
     * 						L’ajout d’un creanceID.
     */
    public java.lang.String getCreanceId() {
        return creanceId;
    }


    /**
     * Sets the creanceId value for this PanierClientType.
     * 
     * @param creanceId   * Identifiant du service du créancier dans le SI
     * 						Fatourati.
     * 						Communiqué par le CMI au lancement du service.
     * 						Un créancier peut avoir plusieurs services pour lesquels il
     * souhaite
     * 						avoir une référence.
     * 						Pour chaque creanceId un créancier peut générer jusqu’à 1 millions
     * de
     * 						référence Fatourati.
     * 						L’ajout d’un creanceID.
     */
    public void setCreanceId(java.lang.String creanceId) {
        this.creanceId = creanceId;
    }


    /**
     * Gets the dateArticle value for this PanierClientType.
     * 
     * @return dateArticle   * Date d’émission de l’article
     */
    public java.lang.String getDateArticle() {
        return dateArticle;
    }


    /**
     * Sets the dateArticle value for this PanierClientType.
     * 
     * @param dateArticle   * Date d’émission de l’article
     */
    public void setDateArticle(java.lang.String dateArticle) {
        this.dateArticle = dateArticle;
    }


    /**
     * Gets the dateExpiration value for this PanierClientType.
     * 
     * @return dateExpiration   * Date d’expiration de la Réf Fatourati. Au-delà
     * 						de cette date, l’article ne sera plus payable
     * 						via réf Fatourati. Si non communiquée le SI
     * 						Fatourati définie une valuer par défaut de … .
     * 						Les Réf non payé pendant une période donnée
     * 						seront supprimées suivant la politique
     * 						d’archivage du CMI.
     */
    public java.lang.String getDateExpiration() {
        return dateExpiration;
    }


    /**
     * Sets the dateExpiration value for this PanierClientType.
     * 
     * @param dateExpiration   * Date d’expiration de la Réf Fatourati. Au-delà
     * 						de cette date, l’article ne sera plus payable
     * 						via réf Fatourati. Si non communiquée le SI
     * 						Fatourati définie une valuer par défaut de … .
     * 						Les Réf non payé pendant une période donnée
     * 						seront supprimées suivant la politique
     * 						d’archivage du CMI.
     */
    public void setDateExpiration(java.lang.String dateExpiration) {
        this.dateExpiration = dateExpiration;
    }


    /**
     * Gets the description value for this PanierClientType.
     * 
     * @return description   * Description de l’article
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this PanierClientType.
     * 
     * @param description   * Description de l’article
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the etat value for this PanierClientType.
     * 
     * @return etat   * Etat de paiement de l’article.
     */
    public com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PanierClientTypeEtat getEtat() {
        return etat;
    }


    /**
     * Sets the etat value for this PanierClientType.
     * 
     * @param etat   * Etat de paiement de l’article.
     */
    public void setEtat(com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PanierClientTypeEtat etat) {
        this.etat = etat;
    }


    /**
     * Gets the globalParams value for this PanierClientType.
     * 
     * @return globalParams   * Liste qui contient des valeurs supplémentaires.
     */
    public com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.ParamsType[] getGlobalParams() {
        return globalParams;
    }


    /**
     * Sets the globalParams value for this PanierClientType.
     * 
     * @param globalParams   * Liste qui contient des valeurs supplémentaires.
     */
    public void setGlobalParams(com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.ParamsType[] globalParams) {
        this.globalParams = globalParams;
    }

    public com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.ParamsType getGlobalParams(int i) {
        return this.globalParams[i];
    }

    public void setGlobalParams(int i, com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.ParamsType _value) {
        this.globalParams[i] = _value;
    }


    /**
     * Gets the idArticle value for this PanierClientType.
     * 
     * @return idArticle   * Identifiant de l’article, unique par “Référence
     * 						Fatourati”
     */
    public java.lang.String getIdArticle() {
        return idArticle;
    }


    /**
     * Sets the idArticle value for this PanierClientType.
     * 
     * @param idArticle   * Identifiant de l’article, unique par “Référence
     * 						Fatourati”
     */
    public void setIdArticle(java.lang.String idArticle) {
        this.idArticle = idArticle;
    }


    /**
     * Gets the mailClient value for this PanierClientType.
     * 
     * @return mailClient   * Mail du client
     */
    public java.lang.String getMailClient() {
        return mailClient;
    }


    /**
     * Sets the mailClient value for this PanierClientType.
     * 
     * @param mailClient   * Mail du client
     */
    public void setMailClient(java.lang.String mailClient) {
        this.mailClient = mailClient;
    }


    /**
     * Gets the mobileClient value for this PanierClientType.
     * 
     * @return mobileClient   * Numéro Mobile du client. Optionnel. Obligatoire
     * 						si le créancier veut envoyer un SMS par mail.
     */
    public java.lang.String getMobileClient() {
        return mobileClient;
    }


    /**
     * Sets the mobileClient value for this PanierClientType.
     * 
     * @param mobileClient   * Numéro Mobile du client. Optionnel. Obligatoire
     * 						si le créancier veut envoyer un SMS par mail.
     */
    public void setMobileClient(java.lang.String mobileClient) {
        this.mobileClient = mobileClient;
    }


    /**
     * Gets the montant value for this PanierClientType.
     * 
     * @return montant   * Montant de l’article en dirhams
     */
    public java.math.BigDecimal getMontant() {
        return montant;
    }


    /**
     * Sets the montant value for this PanierClientType.
     * 
     * @param montant   * Montant de l’article en dirhams
     */
    public void setMontant(java.math.BigDecimal montant) {
        this.montant = montant;
    }


    /**
     * Gets the msgRetour value for this PanierClientType.
     * 
     * @return msgRetour   * Code retour du résultat du traitement de
     * 						l’article
     */
    public java.lang.String getMsgRetour() {
        return msgRetour;
    }


    /**
     * Sets the msgRetour value for this PanierClientType.
     * 
     * @param msgRetour   * Code retour du résultat du traitement de
     * 						l’article
     */
    public void setMsgRetour(java.lang.String msgRetour) {
        this.msgRetour = msgRetour;
    }


    /**
     * Gets the nomClient value for this PanierClientType.
     * 
     * @return nomClient   * Nom ou désignation du client (ça peut être un
     * 						numéro de contrat ou d’entreprise.
     * 						C’est la désignation qui sera affiché sur le canal de paiement
     */
    public java.lang.String getNomClient() {
        return nomClient;
    }


    /**
     * Sets the nomClient value for this PanierClientType.
     * 
     * @param nomClient   * Nom ou désignation du client (ça peut être un
     * 						numéro de contrat ou d’entreprise.
     * 						C’est la désignation qui sera affiché sur le canal de paiement
     */
    public void setNomClient(java.lang.String nomClient) {
        this.nomClient = nomClient;
    }


    /**
     * Gets the refClient value for this PanierClientType.
     * 
     * @return refClient   * identifiant du client, utilisé pour regrouper
     * 						plusieurs articles en une seule “Référence
     * 						Fatourati” si action = 1
     * 						(add_group)
     */
    public java.lang.String getRefClient() {
        return refClient;
    }


    /**
     * Sets the refClient value for this PanierClientType.
     * 
     * @param refClient   * identifiant du client, utilisé pour regrouper
     * 						plusieurs articles en une seule “Référence
     * 						Fatourati” si action = 1
     * 						(add_group)
     */
    public void setRefClient(java.lang.String refClient) {
        this.refClient = refClient;
    }


    /**
     * Gets the refFatourati value for this PanierClientType.
     * 
     * @return refFatourati   * Référence Fatourati si déjà générée en cas
     * 						d’action != {0, 1}
     */
    public java.lang.String getRefFatourati() {
        return refFatourati;
    }


    /**
     * Sets the refFatourati value for this PanierClientType.
     * 
     * @param refFatourati   * Référence Fatourati si déjà générée en cas
     * 						d’action != {0, 1}
     */
    public void setRefFatourati(java.lang.String refFatourati) {
        this.refFatourati = refFatourati;
    }


    /**
     * Gets the refPaiement value for this PanierClientType.
     * 
     * @return refPaiement   * Référence de paiement si réglé
     */
    public java.lang.String getRefPaiement() {
        return refPaiement;
    }


    /**
     * Sets the refPaiement value for this PanierClientType.
     * 
     * @param refPaiement   * Référence de paiement si réglé
     */
    public void setRefPaiement(java.lang.String refPaiement) {
        this.refPaiement = refPaiement;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PanierClientType)) return false;
        PanierClientType other = (PanierClientType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codeRetour==null && other.getCodeRetour()==null) || 
             (this.codeRetour!=null &&
              this.codeRetour.equals(other.getCodeRetour()))) &&
            ((this.creanceId==null && other.getCreanceId()==null) || 
             (this.creanceId!=null &&
              this.creanceId.equals(other.getCreanceId()))) &&
            ((this.dateArticle==null && other.getDateArticle()==null) || 
             (this.dateArticle!=null &&
              this.dateArticle.equals(other.getDateArticle()))) &&
            ((this.dateExpiration==null && other.getDateExpiration()==null) || 
             (this.dateExpiration!=null &&
              this.dateExpiration.equals(other.getDateExpiration()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.etat==null && other.getEtat()==null) || 
             (this.etat!=null &&
              this.etat.equals(other.getEtat()))) &&
            ((this.globalParams==null && other.getGlobalParams()==null) || 
             (this.globalParams!=null &&
              java.util.Arrays.equals(this.globalParams, other.getGlobalParams()))) &&
            ((this.idArticle==null && other.getIdArticle()==null) || 
             (this.idArticle!=null &&
              this.idArticle.equals(other.getIdArticle()))) &&
            ((this.mailClient==null && other.getMailClient()==null) || 
             (this.mailClient!=null &&
              this.mailClient.equals(other.getMailClient()))) &&
            ((this.mobileClient==null && other.getMobileClient()==null) || 
             (this.mobileClient!=null &&
              this.mobileClient.equals(other.getMobileClient()))) &&
            ((this.montant==null && other.getMontant()==null) || 
             (this.montant!=null &&
              this.montant.equals(other.getMontant()))) &&
            ((this.msgRetour==null && other.getMsgRetour()==null) || 
             (this.msgRetour!=null &&
              this.msgRetour.equals(other.getMsgRetour()))) &&
            ((this.nomClient==null && other.getNomClient()==null) || 
             (this.nomClient!=null &&
              this.nomClient.equals(other.getNomClient()))) &&
            ((this.refClient==null && other.getRefClient()==null) || 
             (this.refClient!=null &&
              this.refClient.equals(other.getRefClient()))) &&
            ((this.refFatourati==null && other.getRefFatourati()==null) || 
             (this.refFatourati!=null &&
              this.refFatourati.equals(other.getRefFatourati()))) &&
            ((this.refPaiement==null && other.getRefPaiement()==null) || 
             (this.refPaiement!=null &&
              this.refPaiement.equals(other.getRefPaiement())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodeRetour() != null) {
            _hashCode += getCodeRetour().hashCode();
        }
        if (getCreanceId() != null) {
            _hashCode += getCreanceId().hashCode();
        }
        if (getDateArticle() != null) {
            _hashCode += getDateArticle().hashCode();
        }
        if (getDateExpiration() != null) {
            _hashCode += getDateExpiration().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getEtat() != null) {
            _hashCode += getEtat().hashCode();
        }
        if (getGlobalParams() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGlobalParams());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGlobalParams(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getIdArticle() != null) {
            _hashCode += getIdArticle().hashCode();
        }
        if (getMailClient() != null) {
            _hashCode += getMailClient().hashCode();
        }
        if (getMobileClient() != null) {
            _hashCode += getMobileClient().hashCode();
        }
        if (getMontant() != null) {
            _hashCode += getMontant().hashCode();
        }
        if (getMsgRetour() != null) {
            _hashCode += getMsgRetour().hashCode();
        }
        if (getNomClient() != null) {
            _hashCode += getNomClient().hashCode();
        }
        if (getRefClient() != null) {
            _hashCode += getRefClient().hashCode();
        }
        if (getRefFatourati() != null) {
            _hashCode += getRefFatourati().hashCode();
        }
        if (getRefPaiement() != null) {
            _hashCode += getRefPaiement().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PanierClientType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.fawatir.fatouratibo.mtc.com", "panierClientType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codeRetour");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codeRetour"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creanceId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "creanceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateArticle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dateArticle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateExpiration");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dateExpiration"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("etat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "etat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.fawatir.fatouratibo.mtc.com", ">panierClientType>etat"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("globalParams");
        elemField.setXmlName(new javax.xml.namespace.QName("", "globalParams"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.fawatir.fatouratibo.mtc.com", "paramsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idArticle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "idArticle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mailClient");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mailClient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mobileClient");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mobileClient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("montant");
        elemField.setXmlName(new javax.xml.namespace.QName("", "montant"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgRetour");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msgRetour"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomClient");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomClient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refClient");
        elemField.setXmlName(new javax.xml.namespace.QName("", "refClient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refFatourati");
        elemField.setXmlName(new javax.xml.namespace.QName("", "refFatourati"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refPaiement");
        elemField.setXmlName(new javax.xml.namespace.QName("", "refPaiement"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
