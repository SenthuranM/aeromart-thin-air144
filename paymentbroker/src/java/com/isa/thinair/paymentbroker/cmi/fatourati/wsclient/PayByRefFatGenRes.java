/**
 * PayByRefFatGenRes.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.paymentbroker.cmi.fatourati.wsclient;

public class PayByRefFatGenRes  implements java.io.Serializable {
    private java.lang.String action;

    private java.lang.String codeRetour;

    private java.lang.String creancierId;

    private java.util.Calendar dateServeur;

    private com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.ParamsType[] globalParams;

    private java.math.BigDecimal montantFailedArticles;

    private java.math.BigDecimal montantSuccessArticles;

    private java.lang.String[] messages;

    private java.lang.String msgRetour;

    private java.lang.Integer nbrFailedArticles;

    private java.lang.Integer nbrSuccessArticles;

    private com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PanierClientType[] panierClientRes;

    private java.lang.String refFatourati;

    public PayByRefFatGenRes() {
    }

    public PayByRefFatGenRes(
           java.lang.String action,
           java.lang.String codeRetour,
           java.lang.String creancierId,
           java.util.Calendar dateServeur,
           com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.ParamsType[] globalParams,
           java.math.BigDecimal montantFailedArticles,
           java.math.BigDecimal montantSuccessArticles,
           java.lang.String[] messages,
           java.lang.String msgRetour,
           java.lang.Integer nbrFailedArticles,
           java.lang.Integer nbrSuccessArticles,
           com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PanierClientType[] panierClientRes,
           java.lang.String refFatourati) {
           this.action = action;
           this.codeRetour = codeRetour;
           this.creancierId = creancierId;
           this.dateServeur = dateServeur;
           this.globalParams = globalParams;
           this.montantFailedArticles = montantFailedArticles;
           this.montantSuccessArticles = montantSuccessArticles;
           this.messages = messages;
           this.msgRetour = msgRetour;
           this.nbrFailedArticles = nbrFailedArticles;
           this.nbrSuccessArticles = nbrSuccessArticles;
           this.panierClientRes = panierClientRes;
           this.refFatourati = refFatourati;
    }


    /**
     * Gets the action value for this PayByRefFatGenRes.
     * 
     * @return action
     */
    public java.lang.String getAction() {
        return action;
    }


    /**
     * Sets the action value for this PayByRefFatGenRes.
     * 
     * @param action
     */
    public void setAction(java.lang.String action) {
        this.action = action;
    }


    /**
     * Gets the codeRetour value for this PayByRefFatGenRes.
     * 
     * @return codeRetour
     */
    public java.lang.String getCodeRetour() {
        return codeRetour;
    }


    /**
     * Sets the codeRetour value for this PayByRefFatGenRes.
     * 
     * @param codeRetour
     */
    public void setCodeRetour(java.lang.String codeRetour) {
        this.codeRetour = codeRetour;
    }


    /**
     * Gets the creancierId value for this PayByRefFatGenRes.
     * 
     * @return creancierId
     */
    public java.lang.String getCreancierId() {
        return creancierId;
    }


    /**
     * Sets the creancierId value for this PayByRefFatGenRes.
     * 
     * @param creancierId
     */
    public void setCreancierId(java.lang.String creancierId) {
        this.creancierId = creancierId;
    }


    /**
     * Gets the dateServeur value for this PayByRefFatGenRes.
     * 
     * @return dateServeur
     */
    public java.util.Calendar getDateServeur() {
        return dateServeur;
    }


    /**
     * Sets the dateServeur value for this PayByRefFatGenRes.
     * 
     * @param dateServeur
     */
    public void setDateServeur(java.util.Calendar dateServeur) {
        this.dateServeur = dateServeur;
    }


    /**
     * Gets the globalParams value for this PayByRefFatGenRes.
     * 
     * @return globalParams
     */
    public com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.ParamsType[] getGlobalParams() {
        return globalParams;
    }


    /**
     * Sets the globalParams value for this PayByRefFatGenRes.
     * 
     * @param globalParams
     */
    public void setGlobalParams(com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.ParamsType[] globalParams) {
        this.globalParams = globalParams;
    }

    public com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.ParamsType getGlobalParams(int i) {
        return this.globalParams[i];
    }

    public void setGlobalParams(int i, com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.ParamsType _value) {
        this.globalParams[i] = _value;
    }


    /**
     * Gets the montantFailedArticles value for this PayByRefFatGenRes.
     * 
     * @return montantFailedArticles
     */
    public java.math.BigDecimal getMontantFailedArticles() {
        return montantFailedArticles;
    }


    /**
     * Sets the montantFailedArticles value for this PayByRefFatGenRes.
     * 
     * @param montantFailedArticles
     */
    public void setMontantFailedArticles(java.math.BigDecimal montantFailedArticles) {
        this.montantFailedArticles = montantFailedArticles;
    }


    /**
     * Gets the montantSuccessArticles value for this PayByRefFatGenRes.
     * 
     * @return montantSuccessArticles
     */
    public java.math.BigDecimal getMontantSuccessArticles() {
        return montantSuccessArticles;
    }


    /**
     * Sets the montantSuccessArticles value for this PayByRefFatGenRes.
     * 
     * @param montantSuccessArticles
     */
    public void setMontantSuccessArticles(java.math.BigDecimal montantSuccessArticles) {
        this.montantSuccessArticles = montantSuccessArticles;
    }


    /**
     * Gets the messages value for this PayByRefFatGenRes.
     * 
     * @return messages
     */
    public java.lang.String[] getMessages() {
        return messages;
    }


    /**
     * Sets the messages value for this PayByRefFatGenRes.
     * 
     * @param messages
     */
    public void setMessages(java.lang.String[] messages) {
        this.messages = messages;
    }

    public java.lang.String getMessages(int i) {
        return this.messages[i];
    }

    public void setMessages(int i, java.lang.String _value) {
        this.messages[i] = _value;
    }


    /**
     * Gets the msgRetour value for this PayByRefFatGenRes.
     * 
     * @return msgRetour
     */
    public java.lang.String getMsgRetour() {
        return msgRetour;
    }


    /**
     * Sets the msgRetour value for this PayByRefFatGenRes.
     * 
     * @param msgRetour
     */
    public void setMsgRetour(java.lang.String msgRetour) {
        this.msgRetour = msgRetour;
    }


    /**
     * Gets the nbrFailedArticles value for this PayByRefFatGenRes.
     * 
     * @return nbrFailedArticles
     */
    public java.lang.Integer getNbrFailedArticles() {
        return nbrFailedArticles;
    }


    /**
     * Sets the nbrFailedArticles value for this PayByRefFatGenRes.
     * 
     * @param nbrFailedArticles
     */
    public void setNbrFailedArticles(java.lang.Integer nbrFailedArticles) {
        this.nbrFailedArticles = nbrFailedArticles;
    }


    /**
     * Gets the nbrSuccessArticles value for this PayByRefFatGenRes.
     * 
     * @return nbrSuccessArticles
     */
    public java.lang.Integer getNbrSuccessArticles() {
        return nbrSuccessArticles;
    }


    /**
     * Sets the nbrSuccessArticles value for this PayByRefFatGenRes.
     * 
     * @param nbrSuccessArticles
     */
    public void setNbrSuccessArticles(java.lang.Integer nbrSuccessArticles) {
        this.nbrSuccessArticles = nbrSuccessArticles;
    }


    /**
     * Gets the panierClientRes value for this PayByRefFatGenRes.
     * 
     * @return panierClientRes
     */
    public com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PanierClientType[] getPanierClientRes() {
        return panierClientRes;
    }


    /**
     * Sets the panierClientRes value for this PayByRefFatGenRes.
     * 
     * @param panierClientRes
     */
    public void setPanierClientRes(com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PanierClientType[] panierClientRes) {
        this.panierClientRes = panierClientRes;
    }

    public com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PanierClientType getPanierClientRes(int i) {
        return this.panierClientRes[i];
    }

    public void setPanierClientRes(int i, com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PanierClientType _value) {
        this.panierClientRes[i] = _value;
    }


    /**
     * Gets the refFatourati value for this PayByRefFatGenRes.
     * 
     * @return refFatourati
     */
    public java.lang.String getRefFatourati() {
        return refFatourati;
    }


    /**
     * Sets the refFatourati value for this PayByRefFatGenRes.
     * 
     * @param refFatourati
     */
    public void setRefFatourati(java.lang.String refFatourati) {
        this.refFatourati = refFatourati;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PayByRefFatGenRes)) return false;
        PayByRefFatGenRes other = (PayByRefFatGenRes) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.action==null && other.getAction()==null) || 
             (this.action!=null &&
              this.action.equals(other.getAction()))) &&
            ((this.codeRetour==null && other.getCodeRetour()==null) || 
             (this.codeRetour!=null &&
              this.codeRetour.equals(other.getCodeRetour()))) &&
            ((this.creancierId==null && other.getCreancierId()==null) || 
             (this.creancierId!=null &&
              this.creancierId.equals(other.getCreancierId()))) &&
            ((this.dateServeur==null && other.getDateServeur()==null) || 
             (this.dateServeur!=null &&
              this.dateServeur.equals(other.getDateServeur()))) &&
            ((this.globalParams==null && other.getGlobalParams()==null) || 
             (this.globalParams!=null &&
              java.util.Arrays.equals(this.globalParams, other.getGlobalParams()))) &&
            ((this.montantFailedArticles==null && other.getMontantFailedArticles()==null) || 
             (this.montantFailedArticles!=null &&
              this.montantFailedArticles.equals(other.getMontantFailedArticles()))) &&
            ((this.montantSuccessArticles==null && other.getMontantSuccessArticles()==null) || 
             (this.montantSuccessArticles!=null &&
              this.montantSuccessArticles.equals(other.getMontantSuccessArticles()))) &&
            ((this.messages==null && other.getMessages()==null) || 
             (this.messages!=null &&
              java.util.Arrays.equals(this.messages, other.getMessages()))) &&
            ((this.msgRetour==null && other.getMsgRetour()==null) || 
             (this.msgRetour!=null &&
              this.msgRetour.equals(other.getMsgRetour()))) &&
            ((this.nbrFailedArticles==null && other.getNbrFailedArticles()==null) || 
             (this.nbrFailedArticles!=null &&
              this.nbrFailedArticles.equals(other.getNbrFailedArticles()))) &&
            ((this.nbrSuccessArticles==null && other.getNbrSuccessArticles()==null) || 
             (this.nbrSuccessArticles!=null &&
              this.nbrSuccessArticles.equals(other.getNbrSuccessArticles()))) &&
            ((this.panierClientRes==null && other.getPanierClientRes()==null) || 
             (this.panierClientRes!=null &&
              java.util.Arrays.equals(this.panierClientRes, other.getPanierClientRes()))) &&
            ((this.refFatourati==null && other.getRefFatourati()==null) || 
             (this.refFatourati!=null &&
              this.refFatourati.equals(other.getRefFatourati())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAction() != null) {
            _hashCode += getAction().hashCode();
        }
        if (getCodeRetour() != null) {
            _hashCode += getCodeRetour().hashCode();
        }
        if (getCreancierId() != null) {
            _hashCode += getCreancierId().hashCode();
        }
        if (getDateServeur() != null) {
            _hashCode += getDateServeur().hashCode();
        }
        if (getGlobalParams() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGlobalParams());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGlobalParams(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMontantFailedArticles() != null) {
            _hashCode += getMontantFailedArticles().hashCode();
        }
        if (getMontantSuccessArticles() != null) {
            _hashCode += getMontantSuccessArticles().hashCode();
        }
        if (getMessages() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMessages());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMessages(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMsgRetour() != null) {
            _hashCode += getMsgRetour().hashCode();
        }
        if (getNbrFailedArticles() != null) {
            _hashCode += getNbrFailedArticles().hashCode();
        }
        if (getNbrSuccessArticles() != null) {
            _hashCode += getNbrSuccessArticles().hashCode();
        }
        if (getPanierClientRes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPanierClientRes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPanierClientRes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRefFatourati() != null) {
            _hashCode += getRefFatourati().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PayByRefFatGenRes.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.fawatir.fatouratibo.mtc.com", "payByRefFatGenRes"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("action");
        elemField.setXmlName(new javax.xml.namespace.QName("", "action"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codeRetour");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codeRetour"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creancierId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "creancierId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateServeur");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dateServeur"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("globalParams");
        elemField.setXmlName(new javax.xml.namespace.QName("", "globalParams"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.fawatir.fatouratibo.mtc.com", "paramsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("montantFailedArticles");
        elemField.setXmlName(new javax.xml.namespace.QName("", "montantFailedArticles"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("montantSuccessArticles");
        elemField.setXmlName(new javax.xml.namespace.QName("", "montantSuccessArticles"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("messages");
        elemField.setXmlName(new javax.xml.namespace.QName("", "messages"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgRetour");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msgRetour"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nbrFailedArticles");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nbrFailedArticles"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nbrSuccessArticles");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nbrSuccessArticles"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("panierClientRes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "panierClientRes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.fawatir.fatouratibo.mtc.com", "panierClientType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refFatourati");
        elemField.setXmlName(new javax.xml.namespace.QName("", "refFatourati"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
