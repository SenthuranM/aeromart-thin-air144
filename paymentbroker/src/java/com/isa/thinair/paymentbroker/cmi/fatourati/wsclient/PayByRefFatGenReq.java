/**
 * PayByRefFatGenReq.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.paymentbroker.cmi.fatourati.wsclient;

public class PayByRefFatGenReq  implements java.io.Serializable {
    /* Action demandée */
    private com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PayByRefFatGenReqAction action;

    /* Identifiant du créancier dans le SI Fatourati.
     * 						Communiqué par le CMI au lancement du service. */
    private java.lang.String creancierId;

    /* Date d’initiation de la demande par le créancier */
    private java.lang.String dateServeurCreancier;

    /* Tableau qui contient des valeurs supplémentaires */
    private com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.ParamsType[] globalParams;

    /* 2ème Identifiant du client sur le système de
     * 						paiement choisi */
    private java.lang.String ident2ClientSysPmt;

    /* Identifiant du client sur le système de paiement
     * 						choisi */
    private java.lang.String identClientSysPmt;

    /* Code permettant de vérifier l’intégrité du
     * 						message
     * 						Méthode de calcul :
     * 						creancierId + dateServeurCreancier + action + nbrTotalArticles
     * +
     * 						montantTotalArticles + la concatenation des IdArticle de la
     * liste
     * 						des panierClient + cle Secrete => hash MD5 */
    private byte[] mac;

    /* Montant total des prix des articles du tableau
     * 						panierClient en centimes */
    private java.math.BigDecimal montantTotalArticles;

    /* Nombre des articles (taille de la liste
     * 						panierClient) */
    private java.lang.Integer nbrTotalArticles;

    /* Liste d’Objets contenant les informations
     * 						relatives aux articles pour lesquels le SI Fatourati va générer
     * la
     * 						réf fatourati */
    private com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PanierClientType[] panierClient;

    /* “Référence Fatourati” en cas de modification,
     * 						activation/désactivation ou suppression */
    private java.lang.String refFatourati;

    /* Code système de paiement choisi éventuellement
     * 						par le client */
    private java.lang.String sysPmtCible;

    public PayByRefFatGenReq() {
    }

    public PayByRefFatGenReq(
           com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PayByRefFatGenReqAction action,
           java.lang.String creancierId,
           java.lang.String dateServeurCreancier,
           com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.ParamsType[] globalParams,
           java.lang.String ident2ClientSysPmt,
           java.lang.String identClientSysPmt,
           byte[] mac,
           java.math.BigDecimal montantTotalArticles,
           java.lang.Integer nbrTotalArticles,
           com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PanierClientType[] panierClient,
           java.lang.String refFatourati,
           java.lang.String sysPmtCible) {
           this.action = action;
           this.creancierId = creancierId;
           this.dateServeurCreancier = dateServeurCreancier;
           this.globalParams = globalParams;
           this.ident2ClientSysPmt = ident2ClientSysPmt;
           this.identClientSysPmt = identClientSysPmt;
           this.mac = mac;
           this.montantTotalArticles = montantTotalArticles;
           this.nbrTotalArticles = nbrTotalArticles;
           this.panierClient = panierClient;
           this.refFatourati = refFatourati;
           this.sysPmtCible = sysPmtCible;
    }


    /**
     * Gets the action value for this PayByRefFatGenReq.
     * 
     * @return action   * Action demandée
     */
    public com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PayByRefFatGenReqAction getAction() {
        return action;
    }


    /**
     * Sets the action value for this PayByRefFatGenReq.
     * 
     * @param action   * Action demandée
     */
    public void setAction(com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PayByRefFatGenReqAction action) {
        this.action = action;
    }


    /**
     * Gets the creancierId value for this PayByRefFatGenReq.
     * 
     * @return creancierId   * Identifiant du créancier dans le SI Fatourati.
     * 						Communiqué par le CMI au lancement du service.
     */
    public java.lang.String getCreancierId() {
        return creancierId;
    }


    /**
     * Sets the creancierId value for this PayByRefFatGenReq.
     * 
     * @param creancierId   * Identifiant du créancier dans le SI Fatourati.
     * 						Communiqué par le CMI au lancement du service.
     */
    public void setCreancierId(java.lang.String creancierId) {
        this.creancierId = creancierId;
    }


    /**
     * Gets the dateServeurCreancier value for this PayByRefFatGenReq.
     * 
     * @return dateServeurCreancier   * Date d’initiation de la demande par le créancier
     */
    public java.lang.String getDateServeurCreancier() {
        return dateServeurCreancier;
    }


    /**
     * Sets the dateServeurCreancier value for this PayByRefFatGenReq.
     * 
     * @param dateServeurCreancier   * Date d’initiation de la demande par le créancier
     */
    public void setDateServeurCreancier(java.lang.String dateServeurCreancier) {
        this.dateServeurCreancier = dateServeurCreancier;
    }


    /**
     * Gets the globalParams value for this PayByRefFatGenReq.
     * 
     * @return globalParams   * Tableau qui contient des valeurs supplémentaires
     */
    public com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.ParamsType[] getGlobalParams() {
        return globalParams;
    }


    /**
     * Sets the globalParams value for this PayByRefFatGenReq.
     * 
     * @param globalParams   * Tableau qui contient des valeurs supplémentaires
     */
    public void setGlobalParams(com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.ParamsType[] globalParams) {
        this.globalParams = globalParams;
    }

    public com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.ParamsType getGlobalParams(int i) {
        return this.globalParams[i];
    }

    public void setGlobalParams(int i, com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.ParamsType _value) {
        this.globalParams[i] = _value;
    }


    /**
     * Gets the ident2ClientSysPmt value for this PayByRefFatGenReq.
     * 
     * @return ident2ClientSysPmt   * 2ème Identifiant du client sur le système de
     * 						paiement choisi
     */
    public java.lang.String getIdent2ClientSysPmt() {
        return ident2ClientSysPmt;
    }


    /**
     * Sets the ident2ClientSysPmt value for this PayByRefFatGenReq.
     * 
     * @param ident2ClientSysPmt   * 2ème Identifiant du client sur le système de
     * 						paiement choisi
     */
    public void setIdent2ClientSysPmt(java.lang.String ident2ClientSysPmt) {
        this.ident2ClientSysPmt = ident2ClientSysPmt;
    }


    /**
     * Gets the identClientSysPmt value for this PayByRefFatGenReq.
     * 
     * @return identClientSysPmt   * Identifiant du client sur le système de paiement
     * 						choisi
     */
    public java.lang.String getIdentClientSysPmt() {
        return identClientSysPmt;
    }


    /**
     * Sets the identClientSysPmt value for this PayByRefFatGenReq.
     * 
     * @param identClientSysPmt   * Identifiant du client sur le système de paiement
     * 						choisi
     */
    public void setIdentClientSysPmt(java.lang.String identClientSysPmt) {
        this.identClientSysPmt = identClientSysPmt;
    }


    /**
     * Gets the mac value for this PayByRefFatGenReq.
     * 
     * @return mac   * Code permettant de vérifier l’intégrité du
     * 						message
     * 						Méthode de calcul :
     * 						creancierId + dateServeurCreancier + action + nbrTotalArticles
     * +
     * 						montantTotalArticles + la concatenation des IdArticle de la
     * liste
     * 						des panierClient + cle Secrete => hash MD5
     */
    public byte[] getMac() {
        return mac;
    }


    /**
     * Sets the mac value for this PayByRefFatGenReq.
     * 
     * @param mac   * Code permettant de vérifier l’intégrité du
     * 						message
     * 						Méthode de calcul :
     * 						creancierId + dateServeurCreancier + action + nbrTotalArticles
     * +
     * 						montantTotalArticles + la concatenation des IdArticle de la
     * liste
     * 						des panierClient + cle Secrete => hash MD5
     */
    public void setMac(byte[] mac) {
        this.mac = mac;
    }


    /**
     * Gets the montantTotalArticles value for this PayByRefFatGenReq.
     * 
     * @return montantTotalArticles   * Montant total des prix des articles du tableau
     * 						panierClient en centimes
     */
    public java.math.BigDecimal getMontantTotalArticles() {
        return montantTotalArticles;
    }


    /**
     * Sets the montantTotalArticles value for this PayByRefFatGenReq.
     * 
     * @param montantTotalArticles   * Montant total des prix des articles du tableau
     * 						panierClient en centimes
     */
    public void setMontantTotalArticles(java.math.BigDecimal montantTotalArticles) {
        this.montantTotalArticles = montantTotalArticles;
    }


    /**
     * Gets the nbrTotalArticles value for this PayByRefFatGenReq.
     * 
     * @return nbrTotalArticles   * Nombre des articles (taille de la liste
     * 						panierClient)
     */
    public java.lang.Integer getNbrTotalArticles() {
        return nbrTotalArticles;
    }


    /**
     * Sets the nbrTotalArticles value for this PayByRefFatGenReq.
     * 
     * @param nbrTotalArticles   * Nombre des articles (taille de la liste
     * 						panierClient)
     */
    public void setNbrTotalArticles(java.lang.Integer nbrTotalArticles) {
        this.nbrTotalArticles = nbrTotalArticles;
    }


    /**
     * Gets the panierClient value for this PayByRefFatGenReq.
     * 
     * @return panierClient   * Liste d’Objets contenant les informations
     * 						relatives aux articles pour lesquels le SI Fatourati va générer
     * la
     * 						réf fatourati
     */
    public com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PanierClientType[] getPanierClient() {
        return panierClient;
    }


    /**
     * Sets the panierClient value for this PayByRefFatGenReq.
     * 
     * @param panierClient   * Liste d’Objets contenant les informations
     * 						relatives aux articles pour lesquels le SI Fatourati va générer
     * la
     * 						réf fatourati
     */
    public void setPanierClient(com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PanierClientType[] panierClient) {
        this.panierClient = panierClient;
    }

    public com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PanierClientType getPanierClient(int i) {
        return this.panierClient[i];
    }

    public void setPanierClient(int i, com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.PanierClientType _value) {
        this.panierClient[i] = _value;
    }


    /**
     * Gets the refFatourati value for this PayByRefFatGenReq.
     * 
     * @return refFatourati   * “Référence Fatourati” en cas de modification,
     * 						activation/désactivation ou suppression
     */
    public java.lang.String getRefFatourati() {
        return refFatourati;
    }


    /**
     * Sets the refFatourati value for this PayByRefFatGenReq.
     * 
     * @param refFatourati   * “Référence Fatourati” en cas de modification,
     * 						activation/désactivation ou suppression
     */
    public void setRefFatourati(java.lang.String refFatourati) {
        this.refFatourati = refFatourati;
    }


    /**
     * Gets the sysPmtCible value for this PayByRefFatGenReq.
     * 
     * @return sysPmtCible   * Code système de paiement choisi éventuellement
     * 						par le client
     */
    public java.lang.String getSysPmtCible() {
        return sysPmtCible;
    }


    /**
     * Sets the sysPmtCible value for this PayByRefFatGenReq.
     * 
     * @param sysPmtCible   * Code système de paiement choisi éventuellement
     * 						par le client
     */
    public void setSysPmtCible(java.lang.String sysPmtCible) {
        this.sysPmtCible = sysPmtCible;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PayByRefFatGenReq)) return false;
        PayByRefFatGenReq other = (PayByRefFatGenReq) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.action==null && other.getAction()==null) || 
             (this.action!=null &&
              this.action.equals(other.getAction()))) &&
            ((this.creancierId==null && other.getCreancierId()==null) || 
             (this.creancierId!=null &&
              this.creancierId.equals(other.getCreancierId()))) &&
            ((this.dateServeurCreancier==null && other.getDateServeurCreancier()==null) || 
             (this.dateServeurCreancier!=null &&
              this.dateServeurCreancier.equals(other.getDateServeurCreancier()))) &&
            ((this.globalParams==null && other.getGlobalParams()==null) || 
             (this.globalParams!=null &&
              java.util.Arrays.equals(this.globalParams, other.getGlobalParams()))) &&
            ((this.ident2ClientSysPmt==null && other.getIdent2ClientSysPmt()==null) || 
             (this.ident2ClientSysPmt!=null &&
              this.ident2ClientSysPmt.equals(other.getIdent2ClientSysPmt()))) &&
            ((this.identClientSysPmt==null && other.getIdentClientSysPmt()==null) || 
             (this.identClientSysPmt!=null &&
              this.identClientSysPmt.equals(other.getIdentClientSysPmt()))) &&
            ((this.mac==null && other.getMac()==null) || 
             (this.mac!=null &&
              java.util.Arrays.equals(this.mac, other.getMac()))) &&
            ((this.montantTotalArticles==null && other.getMontantTotalArticles()==null) || 
             (this.montantTotalArticles!=null &&
              this.montantTotalArticles.equals(other.getMontantTotalArticles()))) &&
            ((this.nbrTotalArticles==null && other.getNbrTotalArticles()==null) || 
             (this.nbrTotalArticles!=null &&
              this.nbrTotalArticles.equals(other.getNbrTotalArticles()))) &&
            ((this.panierClient==null && other.getPanierClient()==null) || 
             (this.panierClient!=null &&
              java.util.Arrays.equals(this.panierClient, other.getPanierClient()))) &&
            ((this.refFatourati==null && other.getRefFatourati()==null) || 
             (this.refFatourati!=null &&
              this.refFatourati.equals(other.getRefFatourati()))) &&
            ((this.sysPmtCible==null && other.getSysPmtCible()==null) || 
             (this.sysPmtCible!=null &&
              this.sysPmtCible.equals(other.getSysPmtCible())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAction() != null) {
            _hashCode += getAction().hashCode();
        }
        if (getCreancierId() != null) {
            _hashCode += getCreancierId().hashCode();
        }
        if (getDateServeurCreancier() != null) {
            _hashCode += getDateServeurCreancier().hashCode();
        }
        if (getGlobalParams() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGlobalParams());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGlobalParams(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getIdent2ClientSysPmt() != null) {
            _hashCode += getIdent2ClientSysPmt().hashCode();
        }
        if (getIdentClientSysPmt() != null) {
            _hashCode += getIdentClientSysPmt().hashCode();
        }
        if (getMac() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMac());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMac(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMontantTotalArticles() != null) {
            _hashCode += getMontantTotalArticles().hashCode();
        }
        if (getNbrTotalArticles() != null) {
            _hashCode += getNbrTotalArticles().hashCode();
        }
        if (getPanierClient() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPanierClient());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPanierClient(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRefFatourati() != null) {
            _hashCode += getRefFatourati().hashCode();
        }
        if (getSysPmtCible() != null) {
            _hashCode += getSysPmtCible().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PayByRefFatGenReq.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.fawatir.fatouratibo.mtc.com", "payByRefFatGenReq"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("action");
        elemField.setXmlName(new javax.xml.namespace.QName("", "action"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.fawatir.fatouratibo.mtc.com", ">payByRefFatGenReq>action"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creancierId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "creancierId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateServeurCreancier");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dateServeurCreancier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("globalParams");
        elemField.setXmlName(new javax.xml.namespace.QName("", "globalParams"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.fawatir.fatouratibo.mtc.com", "paramsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ident2ClientSysPmt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ident2ClientSysPmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identClientSysPmt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "identClientSysPmt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mac");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mac"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("montantTotalArticles");
        elemField.setXmlName(new javax.xml.namespace.QName("", "montantTotalArticles"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nbrTotalArticles");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nbrTotalArticles"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("panierClient");
        elemField.setXmlName(new javax.xml.namespace.QName("", "panierClient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://services.fawatir.fatouratibo.mtc.com", "panierClientType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refFatourati");
        elemField.setXmlName(new javax.xml.namespace.QName("", "refFatourati"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sysPmtCible");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sysPmtCible"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
