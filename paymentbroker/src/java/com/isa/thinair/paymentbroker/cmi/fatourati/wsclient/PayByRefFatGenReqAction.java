/**
 * PayByRefFatGenReqAction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.paymentbroker.cmi.fatourati.wsclient;

public class PayByRefFatGenReqAction implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected PayByRefFatGenReqAction(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _value1 = "0";
    public static final java.lang.String _value2 = "1";
    public static final java.lang.String _value3 = "2";
    public static final java.lang.String _value4 = "3";
    public static final java.lang.String _value5 = "4";
    public static final java.lang.String _value6 = "5";
    public static final java.lang.String _value7 = "6";
    public static final java.lang.String _value8 = "7";
    public static final java.lang.String _value9 = "8";
    public static final java.lang.String _value10 = "9";
    public static final PayByRefFatGenReqAction value1 = new PayByRefFatGenReqAction(_value1);
    public static final PayByRefFatGenReqAction value2 = new PayByRefFatGenReqAction(_value2);
    public static final PayByRefFatGenReqAction value3 = new PayByRefFatGenReqAction(_value3);
    public static final PayByRefFatGenReqAction value4 = new PayByRefFatGenReqAction(_value4);
    public static final PayByRefFatGenReqAction value5 = new PayByRefFatGenReqAction(_value5);
    public static final PayByRefFatGenReqAction value6 = new PayByRefFatGenReqAction(_value6);
    public static final PayByRefFatGenReqAction value7 = new PayByRefFatGenReqAction(_value7);
    public static final PayByRefFatGenReqAction value8 = new PayByRefFatGenReqAction(_value8);
    public static final PayByRefFatGenReqAction value9 = new PayByRefFatGenReqAction(_value9);
    public static final PayByRefFatGenReqAction value10 = new PayByRefFatGenReqAction(_value10);
    public java.lang.String getValue() { return _value_;}
    public static PayByRefFatGenReqAction fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        PayByRefFatGenReqAction enumeration = (PayByRefFatGenReqAction)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static PayByRefFatGenReqAction fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PayByRefFatGenReqAction.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://services.fawatir.fatouratibo.mtc.com", ">payByRefFatGenReq>action"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
