/**
 * RefFatouratiWSServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isa.thinair.paymentbroker.cmi.fatourati.wsclient;

public class RefFatouratiWSServiceLocator extends org.apache.axis.client.Service implements com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.RefFatouratiWSService {

    public RefFatouratiWSServiceLocator() {
    }


    public RefFatouratiWSServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public RefFatouratiWSServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for RefFatouratiWSPort
    private java.lang.String RefFatouratiWSPort_address = "http://194.204.248.119/cmifat/services/RefFatouratiWS";

    public java.lang.String getRefFatouratiWSPortAddress() {
        return RefFatouratiWSPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String RefFatouratiWSPortWSDDServiceName = "RefFatouratiWSPort";

    public java.lang.String getRefFatouratiWSPortWSDDServiceName() {
        return RefFatouratiWSPortWSDDServiceName;
    }

    public void setRefFatouratiWSPortWSDDServiceName(java.lang.String name) {
        RefFatouratiWSPortWSDDServiceName = name;
    }

    public com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.RefFatouratiWS getRefFatouratiWSPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(RefFatouratiWSPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getRefFatouratiWSPort(endpoint);
    }

    public com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.RefFatouratiWS getRefFatouratiWSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.RefFatouratiWSSoapBindingStub _stub = new com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.RefFatouratiWSSoapBindingStub(portAddress, this);
            _stub.setPortName(getRefFatouratiWSPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setRefFatouratiWSPortEndpointAddress(java.lang.String address) {
        RefFatouratiWSPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.RefFatouratiWS.class.isAssignableFrom(serviceEndpointInterface)) {
                com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.RefFatouratiWSSoapBindingStub _stub = new com.isa.thinair.paymentbroker.cmi.fatourati.wsclient.RefFatouratiWSSoapBindingStub(new java.net.URL(RefFatouratiWSPort_address), this);
                _stub.setPortName(getRefFatouratiWSPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("RefFatouratiWSPort".equals(inputPortName)) {
            return getRefFatouratiWSPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://services.fawatir.fatouratibo.mtc.com", "RefFatouratiWSService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://services.fawatir.fatouratibo.mtc.com", "RefFatouratiWSPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("RefFatouratiWSPort".equals(portName)) {
            setRefFatouratiWSPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
