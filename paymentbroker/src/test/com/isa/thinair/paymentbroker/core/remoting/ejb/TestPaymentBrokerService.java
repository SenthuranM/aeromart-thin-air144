package com.isa.thinair.paymentbroker.core.remoting.ejb;

import java.util.HashMap;
import java.util.Iterator;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;

import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.utils.PaymentbrokerConstants;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestPaymentBrokerService extends PlatformTestCase {

    private final Log log = LogFactory.getLog(getClass());

    PaymentBrokerBD paymentBrokerDelegate = null;

    // Calling property file for Constants
    // private static final String
    // PAYMENTBROKER_CONSTANTS_RESOURCE_BUNDLE_NAME="resources/resource-bundles/PaymentBrokerPro";
    // ResourceBundle bundle=
    // ResourceBundle.getBundle(PAYMENTBROKER_CONSTANTS_RESOURCE_BUNDLE_NAME);

    protected void setUp() throws Exception {
        super.setUp();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    private PaymentBrokerBD getPaymentBrokerService() {
        System.out.println("Inside the getPaymentBrokerService()");

        LookupService lookup = LookupServiceFactory.getInstance();
        IModule paymentbrokerModule = lookup.getModule(PaymentbrokerConstants.MODULE_NAME);
        System.out.println("Payment broker Lookup successful...");

        try {
            paymentBrokerDelegate = (PaymentBrokerBD) paymentbrokerModule
                    .getServiceBD(PaymentbrokerConstants.FullBDKeys.PAYMENTBROKER_SERVICE_REMOTE);

            System.out.println(paymentBrokerDelegate.getClass().toString());
            System.out.println("payment broker delegate creation successful...");

            return paymentBrokerDelegate;

        } catch (Exception e) {
            System.out.println("Exception in creating paymentBrokerDelegate: " + e.getMessage());
        }

        return paymentBrokerDelegate;
    }

    public void testCreditService() {
        System.out.println("testCreditService");
        try {

            CreditCardPayment creditCardPayment = new CreditCardPayment();
            creditCardPayment.setCardholderName("Hans Mustermann");
            creditCardPayment.setCardNumber("5232050000010003");
            creditCardPayment.setExpiryDate("0312");
            creditCardPayment.setAmount("21.06");
            creditCardPayment.setCurrency("EUR");
            creditCardPayment.setCvvField("003");
            // creditCardPayment.setPnrReference("TestCardholder15");

            System.out.println("creditCardPayment object is created...");
            System.out.println("calling the service method");
            ServiceResponce serviceResponce = null;
            // ServiceResponce serviceResponce =
            // getPaymentBrokerService().credit(creditCardPayment);

            if (serviceResponce == null)
                System.out.println("testCreditService : serviceResponce is null...");

            System.out.println("==================================================");
            System.out.println("Transaction Success Status :" + serviceResponce.isSuccess());
            System.out.println("Transaction Response Code :" + serviceResponce.getResponseCode());
            String transMessage = (String) serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE);
            System.out.println("Transaction Status Result :" + transMessage);
            System.out.println("==================================================");

            HashMap hashMap = (HashMap) serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_RESULTS);

            // Get all keys that are still being referenced
            if (hashMap != null) {
                Iterator iterator = hashMap.keySet().iterator();
                while (iterator.hasNext()) {
                    // Get key
                    Object key = iterator.next();
                    String value = (String) hashMap.get(key);

                    System.out.println(key + " : " + value);
                }
            }

            assertNotNull("testCreditService is null", serviceResponce);

        } catch (Exception exception) {
            System.out.println("inside the Exception...");
            exception.printStackTrace();
            fail("Exception at testCreditService = " + exception.getMessage());
            PaymentBrokerUtils.error(log, "Exception at testCreditService = " + exception.getMessage());
        }
    }

    /*
     * public void testDebitService() { log.info("Inside the
     * testDebitService()"); try {
     * 
     * CreditCardPayment creditCardPayment = new CreditCardPayment();
     * creditCardPayment.setCardholderName("Hans Mustermann");
     * creditCardPayment.setCardNumber("5232050000010003");
     * creditCardPayment.setExpiryDate("0912");
     * creditCardPayment.setAmount("1.06");
     * creditCardPayment.setCurrency("EUR");
     * creditCardPayment.setCvvField("003");
     * creditCardPayment.setPnrReference("TestCardholder15");
     * 
     * ServiceResponce serviceResponce =
     * getPaymentBrokerService().debit(creditCardPayment);
     * 
     * if(serviceResponce == null) System.out.println("serviceResponce-debit is
     * null...");
     * 
     * System.out.println("==================================================");
     * System.out.println("Transaction Success Status :"+
     * serviceResponce.isSuccess()); System.out.println("Transaction Response
     * Code :"+ serviceResponce.getResponseCode()); String transMessage =
     * (String)serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE);
     * System.out.println("Transaction Status Result :"+ transMessage);
     * System.out.println("==================================================");
     * 
     * HashMap hashMap =
     * (HashMap)serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_RESULTS);
     * 
     * if(hashMap != null) { // Get all keys that are still being referenced
     * Iterator iterator = hashMap.keySet().iterator(); while
     * (iterator.hasNext()) { // Get key Object key = iterator.next(); String
     * value= (String)hashMap.get(key);
     * 
     * System.out.println(key+":"+value); } }
     * 
     * assertNotNull("strCredit-debit is null", serviceResponce); } catch
     * (ModuleException e) { System.out.println("ModuleException at
     * testDebitService = " + e.getMessageString()); fail("ModuleException at
     * testDebitService = " + e); } catch (Exception e) { fail("Exception at
     * testDebitService = " + e); } }
     * 
     * 
     * 
     * public void testReverseService() { log.info("Inside the
     * testReverseService()");
     * 
     * try { CreditCardPayment creditCardPayment = new CreditCardPayment();
     * creditCardPayment.setTranRefNumber(57);
     * creditCardPayment.setCardholderName("Hans Mustermann");
     * creditCardPayment.setCardNumber("5232050000010003");
     * creditCardPayment.setExpiryDate("0912");
     * creditCardPayment.setAmount("1.06");
     * creditCardPayment.setCurrency("EUR");
     * creditCardPayment.setCvvField("003");
     * creditCardPayment.setPnrReference("TestCardholder15");
     * 
     * ServiceResponce serviceResponce =
     * getPaymentBrokerService().reverse(creditCardPayment);
     * 
     * if(serviceResponce == null) System.out.println("serviceResponce-debit is
     * null...");
     * 
     * System.out.println("==================================================");
     * System.out.println("Transaction Success Status :"+
     * serviceResponce.isSuccess()); System.out.println("Transaction Response
     * Code :"+ serviceResponce.getResponseCode()); String strTransMessage =
     * (String)serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE);
     * System.out.println("Transaction Status Result :"+ strTransMessage);
     * System.out.println("==================================================");
     * 
     * HashMap hashMap =
     * (HashMap)serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_RESULTS);
     * 
     * if(hashMap != null) { // Get all keys that are still being referenced
     * Iterator iterator = hashMap.keySet().iterator(); while
     * (iterator.hasNext()) { // Get key Object key = iterator.next(); String
     * value= (String)hashMap.get(key);
     * 
     * System.out.println(key+":"+value); } } assertNotNull("strCredit-Reverse
     * is null", serviceResponce); } catch (ModuleException e) {
     * fail("ModuleException at testReverseService = " + e); } catch (Exception
     * e) { fail("Exception at testReverseService = " + e); } }
     */
}