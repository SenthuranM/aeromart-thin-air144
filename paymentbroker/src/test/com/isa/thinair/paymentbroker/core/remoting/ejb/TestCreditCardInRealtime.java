/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.remoting.ejb;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messaging.api.model.EmailMessage;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.core.processor.MessageManager;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;

import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.api.utils.PaymentbrokerConstants;
import com.isa.thinair.paymentbroker.core.util.PaymentBrokerUtils;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestCreditCardInRealtime extends PlatformTestCase {

    private final Log log = LogFactory.getLog(getClass());

    PaymentBrokerBD paymentBrokerDelegate = null;

    private static final String PAYMENTBROKER_TEST_DATA_BUNDLE_NAME = "resources/testdata/credit_card_data";

    private String strServiceType = "";

    private String strCardHolderName = "";

    private String strCardNumber = "";

    private String strExpiryDate = "";

    private String strAmount = "";

    private String strCurrency = "";

    private String strCvvField = "";

    private String strPnrRef = "";

    private String strTransRefNo = "";

    protected void setUp() throws Exception {
        super.setUp();
        initializeTestData();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    private void initializeTestData() {
        ResourceBundle testDatabundle = ResourceBundle.getBundle(PAYMENTBROKER_TEST_DATA_BUNDLE_NAME);
        strServiceType = testDatabundle.getString("SERVICE_TYPE");

        strCardHolderName = testDatabundle.getString("CARDHOLDERNAME");
        strCardNumber = testDatabundle.getString("CARDNUMBER");
        strExpiryDate = testDatabundle.getString("EXPIRYDATE");
        strAmount = testDatabundle.getString("AMOUNT");
        strCurrency = testDatabundle.getString("CURRENCY");
        strCvvField = testDatabundle.getString("CVVFIELD");
        strPnrRef = testDatabundle.getString("PNRREFERENCE");

        // If service type is ccreversal, following key value should be set
        strTransRefNo = testDatabundle.getString("TRANSACTION_REF_NUMBER");

    }

    private PaymentBrokerBD getPaymentBrokerService() {
        System.out.println("Inside the getPaymentBrokerService()");

        LookupService lookup = LookupServiceFactory.getInstance();
        IModule paymentbrokerModule = lookup.getModule(PaymentbrokerConstants.MODULE_NAME);
        System.out.println("Payment broker Lookup successful...");

        try {
            paymentBrokerDelegate = (PaymentBrokerBD) paymentbrokerModule
                    .getServiceBD(PaymentbrokerConstants.FullBDKeys.PAYMENTBROKER_SERVICE_REMOTE);

            System.out.println(paymentBrokerDelegate.getClass().toString());
            System.out.println("payment broker delegate creation successful...");

            return paymentBrokerDelegate;

        } catch (Exception e) {
            System.out.println("Exception in creating paymentBrokerDelegate: " + e.getMessage());
        }

        return paymentBrokerDelegate;
    }

    public void testService() {
        System.out.println("testService");
        ServiceResponce serviceResponce = null;
        try {

            CreditCardPayment creditCardPayment = new CreditCardPayment();
            creditCardPayment.setCardholderName(strCardHolderName);
            creditCardPayment.setCardNumber(strCardNumber);
            creditCardPayment.setExpiryDate(strExpiryDate);
            creditCardPayment.setAmount(strAmount);
            creditCardPayment.setCurrency(strCurrency);
            creditCardPayment.setCvvField(strCvvField);
            // creditCardPayment.setPnrReference(strPnrRef);

            // if (strServiceType.equals("ccrefund"))
            // serviceResponce =
            // getPaymentBrokerService().credit(creditCardPayment);
            //
            // if (strServiceType.equals("ccreserve"))
            // serviceResponce =
            // getPaymentBrokerService().debit(creditCardPayment);
            //
            // if (strServiceType.equals("ccreversal")) {
            // creditCardPayment.setTranRefNumber(Integer.parseInt(strTransRefNo));
            // serviceResponce =
            // getPaymentBrokerService().reverse(creditCardPayment);
            // }

            if (serviceResponce == null)
                System.out.println("testCreditService : serviceResponce is null...");

            try {

                String strFilePath = PlatformConstants.getAbsAttachmentsPath() + "/netmanagement_output.txt";

                BufferedWriter out = new BufferedWriter(new FileWriter(strFilePath, true));

                out.write("\n==================================================");
                out.write("\nRUNNING THE TESTCASE FOR SERVICE TYPE: " + strServiceType);
                out.write("\n");
                out.write("\nCredit Card Input Values");
                out.write("\n---------------------------------------------------");
                out.write("\nCardHolderName: " + strCardHolderName);
                out.write("\nCardNumber: " + strCardNumber);
                out.write("\nExpiryDate: " + strExpiryDate);
                out.write("\nAmount: " + strAmount);
                out.write("\nCurrency: " + strCurrency);
                out.write("\nCvvField: " + strCvvField);
                out.write("\nPnrRef: " + strPnrRef);

                out.write("\n");
                out.write("\nNetManagement Output Values");
                out.write("\n---------------------------------------------------");

                HashMap hashMap = (HashMap) serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_RESULTS);

                // Get all keys that are still being referenced
                if (hashMap != null) {
                    Iterator iterator = hashMap.keySet().iterator();
                    while (iterator.hasNext()) {
                        // Get key
                        Object key = iterator.next();
                        String value = (String) hashMap.get(key);
                        String strResultCombination = key + " : " + value;
                        out.write("\n" + strResultCombination);
                    }
                }
                out.write("\n");
                out.write("\nOverall Transaction Result");
                out.write("\n---------------------------------------------------");

                out.write("\nTransaction Success Status :" + serviceResponce.isSuccess());
                out.write("\nTransaction Response Code :" + serviceResponce.getResponseCode());
                String transMessage = (String) serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE);
                out.write("\nTransaction Status Result :" + transMessage);

                out.write("\n");
                out.write("\nEND OF TESTCASE");
                out.write("\n==================================================");

                out.close();

                // Email The Test Results
                // emailTestResults();
            } catch (IOException e) {
            }

            assertNotNull("testCreditService is null", serviceResponce);

        } catch (Exception exception) {
            System.out.println("inside the Exception...");
            exception.printStackTrace();
            fail("Exception at testCreditService = " + exception.getMessage());
            PaymentBrokerUtils.error(log, "Exception at testCreditService = " + exception.getMessage());
        }
    }

    private void emailTestResults() {
        MessageProfile profile = new MessageProfile();

        UserMessaging user = new UserMessaging();
        user.setFirstName("Srikanth");
        user.setLastName("Kanagaratnam");
        user.setToAddres("kksrikanth@gmail.com");
        List messageList = new ArrayList();
        messageList.add(user);
        profile.setUserMessagings(messageList);

        Topic topic = new Topic();
        HashMap map = new HashMap();
        map.put("user", user);

        topic.setTopicParams(map);
        topic.setTopicName("creditcard_test_results");
        topic.setContentType(EmailMessage.CONTENT_TYPE_HTML);// current impl
        // only support
        // html
        topic.setMailServerConfigurationName("default");
        topic.addAttachmentFileName("netmanagement_output.txt");
        profile.setTopic(topic);

        List list = new ArrayList();
        list.add(profile);
        new MessageManager().processMessages(list);

    }
}