/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.paymentbroker.core.remoting.ejb;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.ResourceBundle;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.paymentbroker.api.model.CreditCardPayment;
import com.isa.thinair.paymentbroker.api.util.PaymentConstants;
import com.isa.thinair.paymentbroker.core.remoting.ejb.PaymentBrokerService;
import com.isa.thinair.paymentbroker.core.remoting.ejb.PaymentBrokerServiceHome;
import com.isa.thinair.platform.api.ServiceResponce;

public class PaymentBrokerClient {

    private static final String PAYMENTBROKER_TEST_DATA_BUNDLE_NAME = "resources/testdata/credit_card_data";

    private String strServiceType = "";

    private String strCardHolderName = "";

    private String strCardNumber = "";

    private String strExpiryDate = "";

    private String strAmount = "";

    private String strCurrency = "";

    private String strCvvField = "";

    private String strPnrRef = "";

    private String strTransRefNo = "";

    private String strServerName = "";

    public PaymentBrokerClient() {
        initializeTestData();
    }

    private void initializeTestData() {
        ResourceBundle testDatabundle = ResourceBundle.getBundle(PAYMENTBROKER_TEST_DATA_BUNDLE_NAME);
        strServiceType = testDatabundle.getString("SERVICE_TYPE");

        strCardHolderName = testDatabundle.getString("CARDHOLDERNAME");
        strCardNumber = testDatabundle.getString("CARDNUMBER");
        strExpiryDate = testDatabundle.getString("EXPIRYDATE");
        strAmount = testDatabundle.getString("AMOUNT");
        strCurrency = testDatabundle.getString("CURRENCY");
        strCvvField = testDatabundle.getString("CVVFIELD");
        strPnrRef = testDatabundle.getString("PNRREFERENCE");

        // If service type is ccreversal, following key value should be set
        strTransRefNo = testDatabundle.getString("TRANSACTION_REF_NUMBER");

        strServerName = testDatabundle.getString("SERVER_NAME");

    }

    public void testCreditCardService() {
        ServiceResponce serviceResponce = null;
        try {

            CreditCardPayment creditCardPayment = new CreditCardPayment();
            creditCardPayment.setCardholderName(strCardHolderName);
            creditCardPayment.setCardNumber(strCardNumber);
            creditCardPayment.setExpiryDate(strExpiryDate);
            creditCardPayment.setAmount(strAmount);
            creditCardPayment.setCurrency(strCurrency);
            creditCardPayment.setCvvField(strCvvField);
            // creditCardPayment.setPnrReference(strPnrRef);

            // if(strServiceType.equals("ccrefund"))
            // serviceResponce =
            // getPaymentBrokerService(strServerName).charge(creditCardPayment);
            //
            // if(strServiceType.equals("ccauthorize"))
            // serviceResponce =
            // getPaymentBrokerService(strServerName).debit(creditCardPayment);
            //
            // if(strServiceType.equals("ccreversal")) {
            // creditCardPayment.setTranRefNumber(Integer.parseInt(strTransRefNo));
            // serviceResponce =
            // getPaymentBrokerService(strServerName).reverse(creditCardPayment);
            // }

            if (serviceResponce == null)
                System.out.println("testCreditService : serviceResponce is null...");

            System.out.println("\n==================================================");
            System.out.println("RUNNING THE TESTCASE FOR SERVICE TYPE: " + strServiceType);
            System.out.println("");
            System.out.println("Credit Card Input Values");
            System.out.println("---------------------------------------------------");
            System.out.println("CardHolderName: " + strCardHolderName);
            System.out.println("CardNumber: " + strCardNumber);
            System.out.println("ExpiryDate: " + strExpiryDate);
            System.out.println("Amount: " + strAmount);
            System.out.println("Currency: " + strCurrency);
            System.out.println("CvvField: " + strCvvField);
            System.out.println("PnrRef: " + strPnrRef);

            System.out.println("");
            System.out.println("NetManagement Output Values");
            System.out.println("---------------------------------------------------");

            HashMap hashMap = (HashMap) serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_RESULTS);

            // Get all keys that are still being referenced
            if (hashMap != null) {
                Iterator iterator = hashMap.keySet().iterator();
                while (iterator.hasNext()) {
                    // Get key
                    Object key = iterator.next();
                    String value = (String) hashMap.get(key);
                    String strResultCombination = key + " : " + value;
                    System.out.println(strResultCombination);
                }
            }

            System.out.println("");
            System.out.println("Overall Transaction Result");
            System.out.println("---------------------------------------------------");

            System.out.println("Transaction Success Status :" + serviceResponce.isSuccess());
            System.out.println("Transaction Response Code :" + serviceResponce.getResponseCode());
            String transMessage = (String) serviceResponce.getResponseParam(PaymentConstants.PAYMENTBROKER_TRANSACTION_MESSAGE);
            System.out.println("\nTransaction Status Result :" + transMessage);

            System.out.println("");
            System.out.println("END OF TESTCASE");
            System.out.println("==================================================");

        } catch (Exception exception) {
            System.out.println("inside the Exception...");
            exception.printStackTrace();
        }
    }

    private PaymentBrokerService getPaymentBrokerService(String ipAddress) throws Exception {

        Hashtable hashtable = new Hashtable();
        hashtable.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
        hashtable.put("java.naming.provider.url", "jnp://" + ipAddress + ":1099");
        try {
            InitialContext initialContext = new InitialContext(hashtable);
            PaymentBrokerServiceHome home = (PaymentBrokerServiceHome) initialContext.lookup("ejb/PaymentBrokerService");
            PaymentBrokerService paymentService = home.create();
            return paymentService;

        } catch (NamingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String args[]) throws Exception {
        new PaymentBrokerClient().testCreditCardService();
    }
}
