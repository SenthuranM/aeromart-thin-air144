package com.isa.thinair.scheduler.test;

import org.quartz.SchedulerException;

import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.core.controller.ModuleFramework;
import com.isa.thinair.scheduler.core.logic.logicinterface.IScheduler;

public class testingscheduler {

	/**
	 * @param args
	 * @throws SchedulerException 
	 */
	public static void main(String[] args) throws SchedulerException {
		// TODO Auto-generated method stub
		ModuleFramework.startup();
		LookupService lookup = LookupServiceFactory.getInstance();
		
		IScheduler scheduler = (IScheduler) lookup.getBean("isa:base://modules/scheduler?id=ISchedulerProxy");
			scheduler.start();
	
	}

}
