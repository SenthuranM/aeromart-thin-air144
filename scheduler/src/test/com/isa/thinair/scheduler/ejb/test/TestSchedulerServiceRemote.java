/*
 * Created on Jul 4, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.isa.thinair.scheduler.ejb.test;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.util.PlatformTestCase;
import com.isa.thinair.scheduler.api.Job;
import com.isa.thinair.scheduler.api.JobDetail;
import com.isa.thinair.scheduler.api.service.SchedulerBD;
import com.isa.thinair.scheduler.core.job.CancelBookingJob;
import com.isa.thinair.scheduler.core.job.CheckBalanceJob;






/**
 * @author byorn
 *
 */
public class TestSchedulerServiceRemote extends PlatformTestCase{
	
	protected void setUp() throws Exception{
		super.setUp();
	}
	
	
	public void testStartScheduler() throws Exception
	{
		getSchedulerService().startScheduler();
		
		
		
//		  JobDetail jobDetail1 = new JobDetail();
//		  
//		  jobDetail1.setVolatility(true);
//
//		   
//		   String jobId1 = "jobCancelBooking";
//        
//		 jobDetail1.getJobDataMap().put(Job.PROP_JOB_ID, jobId1);
//         jobDetail1.getJobDataMap().put(Job.PROP_JOB_GROUP,  new Date().toString());
//         
//         jobDetail1.getJobDataMap().put(CancelBookingJob.PROP_CUST_CODE,
//                 "Ctdest");
//         jobDetail1.getJobDataMap().put(CancelBookingJob.PROP_YEAR, "2004");
//         
//      
//         System.out.print("VOLATILE : " + jobDetail1.isVolatile());
//         //jobDetail1.setDurability(true);
//         
//         
//         //scheduleManager.addJob(new Date(), null, 10, 5, jobDetail1);
//		
//         getSchedulerService().addJob(new Date("2005/9/14 14:58:00") , null, 0, 3, jobDetail1);
//
//         
         
         
         
         //         
         //getSchedulerService().stopScheduler();		
        
//         JobDetail jobDetail2 = new JobDetail();
//         
//		 String jobId2 = "jobCheckBalance";
//         jobDetail2.getJobDataMap().put(Job.PROP_JOB_ID, jobId2);
//         jobDetail2.getJobDataMap().put(Job.PROP_JOB_GROUP,  new Date().toString());
//       
//         jobDetail2.getJobDataMap().put(CheckBalanceJob.PROP_CUST_CODE,
//                 "4");
//         jobDetail2.getJobDataMap().put(CheckBalanceJob.PROP_MONTH, "2004");
//	
//         getSchedulerService().addJob(new Date("2005/9/12 10:13:00"), null, 5, 3, jobDetail2);
	}
						
	private SchedulerBD getSchedulerService()
	{ 
		LookupService lookup = LookupServiceFactory.getInstance();
		IModule airmasterModule = lookup.getModule("scheduler");
		System.out.println("Lookup successful...");
		SchedulerBD delegate = null;
		try {
			delegate = (SchedulerBD)airmasterModule.getServiceBD("scheduler.service.remote");
			System.out.println("Delegate creation successful...");
			return delegate;
			
		} catch (Exception e) {
			System.out.println("Exception in creating delegate");
			e.printStackTrace();
		}
		return delegate;
	}

}
