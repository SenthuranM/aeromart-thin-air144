/*
 * ==============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2003 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Nov 23, 2004
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduler.api;

import java.io.Serializable;

import org.quartz.impl.JobDetailImpl;

/**
 * Commons Framework: Reusable components and templates
 * 
 * @author Isuru
 * 
 *         JobDetail is the super classfor all job detail classes
 * 
 */
public class JobDetail extends JobDetailImpl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2772055548579125246L;

	/**
     * 
     */
	public JobDetail() {
		super();

	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 */
	public JobDetail(String arg0, String arg1, Class arg2) {
		super(arg0, arg1, arg2);
	}
}
