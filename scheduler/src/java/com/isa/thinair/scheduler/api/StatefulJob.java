/*
 * ==============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2003 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Nov 22, 2004
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduler.api;

/**
 * 
 * 
 * @author Isuru
 * 
 *         Job is the interface class for all job implementations
 * 
 */
public interface StatefulJob extends org.quartz.StatefulJob {

	public final String PROP_JOB_ID = "PROP_JOB_ID";

	public final String PROP_JOB_GROUP = "PROP_JOB_GROUP";
	public final String PROP_FlightId = "PROP_FlightId"; // defaultflightId
	// public static final Timestamp PROP_TransmissionTimeStamp =null;
	public final String PROP_DepartureStation = "PROP_DepartureStation";

	public final String PROP_DAY = "PROP_DAY";

	public final String PROP_FLIGHT_NUMBER = "PROP_FLIGHT_NUMBER";

	public final String PROP_MONTH = "PROP_MONTH";

	public final String PROP_JOB_TYPE = "PROP_JOB_TYPE";

	public final String PROP_DEP_DATE = "PROP_DEP_DATE";

}