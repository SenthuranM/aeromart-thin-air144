package com.isa.thinair.scheduler.api.service;

import org.quartz.Scheduler;

import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupServiceFactory;

public class SchedulerUtils {

	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule("scheduler");
	}

	public static Scheduler getScheduler() {
		return (Scheduler) LookupServiceFactory.getInstance().getModule("scheduler").getLocalBean("xscheduler");
	}

}
