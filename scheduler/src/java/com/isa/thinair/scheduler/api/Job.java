/*
 * ==============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2003 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Nov 22, 2004
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduler.api;

/**
 * 
 * 
 * @author Isuru
 * 
 *         Job is the interface class for all job implementations
 * 
 */
public interface Job extends org.quartz.Job {

	public static final String PROP_JOB_ID = "PROP_JOB_ID";

	public static final String PROP_JOB_GROUP = "PROP_JOB_GROUP";

	public static final String PROP_JOB_DESCRIPTION = "PROP_JOB_DESCRIPTION";

	public static final String PROP_FlightId = "PROP_FlightId";

	public static final String PROP_DepartureStation = "PROP_DepartureStation";

	public static final String PROP_DAY = "PROP_DAY";

	public static final String PROP_FLIGHT_NUMBER = "PROP_FLIGHT_NUMBER";

	public static final String PROP_MONTH = "PROP_MONTH";

	public static final String PROP_JOB_TYPE = "PROP_JOB_TYPE";

	public static final String PROP_DEP_DATE = "PROP_DEP_DATE";

	public static final String PROP_FlightSegId = "PROP_FlightSegId";

	public static final String PROP_FLIGHT_SEG_NOTIFICATION_EVENT_ID = "PROP_FLIGHT_SEG_NOTIFICATION_EVENT_ID";

	public static final String PROP_DEPARTURE_DATE_LOCAL = "PROP_DEPARTURE_DATE_LOCAL";

	public static final String PROP_DEPARTURE_DATE_ZULU = "PROP_DEPARTURE_DATE_ZULU";

	public static final String PROP_AIRPORT_ONLINE_CHECKIN = "PROP_AIRPORT_ONLINE_CHECKIN";

	public static final String PROP_ANCILLARY_CUT_OVER_TIME = "PROP_ANCILLARY_CUT_OVER_TIME";

	public static final String PROP_INSURANCE_PUBLISH_STATUS = "PROP_INSURANCE_PUBLISH_STATUS";

	public static final String PROP_NOTIFICATION_TYPE = "PROP_NOTIFICATION_TYPE";

	public static final String PROP_NOTIFY_COUNT = "PROP_NOTIFY_COUNT";

	public static final String PROP_NOTIFY_RESULT = "PROP_NOTIFY_RESULT";

	public static final String PROP_BOOKING_DATE = "PROP_BOOKING_DATE";

	public static final String PROP_IS_RECOVERY = "PROP_IS_RECOVERY";

	public static final String PROP_INSURANCE_PUBLISH_TYPE = "PROP_INSURANCE_PUBLISH_TYPE";

	public static final String PROP_NOTIFY_INTERVAL = "NOTIFICATION_REMINDER_INTERVAL";

	public static final String PROP_FIRST_NAME = "PROP_FIRST_NAME";

	public static final String PROP_LAST_NAME = "PROP_LAST_NAME";

	public static final String PROP_PNR = "PROP_PNR";

	public static final String PROP_EMAIL = "PROP_EMAIL";

	public static final String PROP_LANGUAGE = "PROP_LANGUAGE";
	
	public static final String PROP_COUNTRY_CODE = "PROP_COUNTRY_CODE";
	
	public static final String PROP_AIRPORT_CODE = "PROP_AIRPORT_CODE";
	
	public static final String PROP_INBOUND_OUTBOUND = "PROP_INBOUND_OUTBOUND";
	
	public static final String PROP_TIME_GAP = "PROP_TIME_GAP";
	
	public static final String PROP_GDS_ID = "PROP_GDS_ID";
	
	public static final String PROP_GDS_SSMRECAP_EMAIL = "PROP_GDS_SSMRECAP_EMAIL";
	
	public static final String PROP_GDS_SSMRECAP_SCHED_START_DATE = "PROP_GDS_SSMRECAP_SCHED_START_DATE";
	
	public static final String PROP_GDS_SSMRECAP_SCHED_END_DATE = "PROP_GDS_SSMRECAP_SCHED_END_DATE";
	
	public static final String PROP_GDS_IS_SELECTED_SCHED = "PROP_GDS_IS_SELECTED_SCHED";
	
	public static final String PROP_EXTERNAL_SSMRECAP_EMAIL = "PROP_EXTERNAL_SSMRECAP_EMAIL";
	
	public static final String PROP_EXTERNAL_SSMRECAP_SITA = "PROP_EXTERNAL_SSMRECAP_SITA";
	
	public static final String PROP_EXTERNAL_SSMRECAP_SENDING = "PROP_EXTERNAL_SSMRECAP_SENDING";
	
	public static final String PROP_EXTERNAL_SSMRECAP_SCHED_START_DATE = "PROP_EXTERNAL_SSMRECAP_SCHED_START_DATE";
	
	public static final String PROP_EXTERNAL_SSMRECAP_SCHED_END_DATE = "PROP_EXTERNAL_SSMRECAP_SCHED_END_DATE";
	
	public static final String PROP_EXTERNAL_IS_SELECTED_SCHED = "PROP_EXTERNAL_IS_SELECTED_SCHED";	
	
	public static final String PROP_CAMP_ID = "PROP_CAMP_ID";
}
