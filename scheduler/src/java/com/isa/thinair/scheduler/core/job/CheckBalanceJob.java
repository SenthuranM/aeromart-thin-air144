/*
 * ==============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2003 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Nov 18, 2004
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduler.core.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.isa.thinair.scheduler.api.Job;
import com.isa.thinair.scheduler.api.JobDetail;

/**
 * Commons Framework: Reusable components and templates
 * 
 * @author sudheera
 * 
 *         CheckBalanceJob is job schedule class to check balance
 * 
 */
public class CheckBalanceJob implements Job {

	public static final String PROP_CUST_CODE = "prop.cust.code";
	public static final String PROP_AC_NO = "prop.ac.no";
	public static final String PROP_MONTH = "prop.month";

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	public void execute(JobExecutionContext jobExcContext) throws JobExecutionException {
		JobDetail jobDetail = (JobDetail) jobExcContext.getJobDetail();
		System.out.println("Excuting " + jobDetail.getJobDataMap().getString(Job.PROP_JOB_ID) + " ....................."
				+ this.PROP_CUST_CODE);
	}

}
