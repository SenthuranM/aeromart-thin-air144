package com.isa.thinair.scheduler.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * @author Byorn
 * 
 *         SchedulerModule's service interface
 * @isa.module.service-interface module-name="scheduler" description="The scheduler module"
 */

public class SchedulerService extends DefaultModule {

}
