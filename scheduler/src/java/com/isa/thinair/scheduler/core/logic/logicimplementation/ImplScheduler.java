/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:14:52
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.scheduler.core.logic.logicimplementation;

import com.isa.thinair.scheduler.api.Job;
import com.isa.thinair.scheduler.api.JobDetail;
import com.isa.thinair.scheduler.api.service.SchedulerUtils;
import com.isa.thinair.scheduler.core.listeners.AJobListener;
import com.isa.thinair.scheduler.core.listeners.ASchedulerListener;
import com.isa.thinair.scheduler.core.logic.logicinterface.IScheduler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobKey;
import org.quartz.ObjectAlreadyExistsException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.quartz.impl.triggers.SimpleTriggerImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * SchedulerImplementation is the IScheduler implementation
 */
public class ImplScheduler implements IScheduler {

	private Log log = LogFactory.getLog(ImplScheduler.class);

	private Scheduler getScheduler() throws SchedulerException {
		return SchedulerUtils.getScheduler();
	}

	public void start() throws SchedulerException {

		Scheduler scheduler = getScheduler();

		if (scheduler.isInStandbyMode() || scheduler.isShutdown()) {
			scheduler.start();

			log.info(" :::::::::::::: Scheduler starting ....... ::::::::::::::");

			scheduler.getListenerManager().addJobListener(new AJobListener("Quartz-Job-Listner"));
			scheduler.getListenerManager().addSchedulerListener(new ASchedulerListener("Quartz-Scheduler-Listner"));

			log.info(" :::::::::::::: Scheduler started ::::::::::::::");
		} else {
			log.info(" :::::::::::::: Attempting to start scheduler while it is already started ::::::::::::::");
		}

	}

	public void stop() throws SchedulerException {
		log.info(" :::::::::::::: Scheduler stopping ....... ::::::::::::::");
		getScheduler().shutdown();
		log.info(" :::::::::::::: Scheduler stopped ::::::::::::::");
	}

	public String getStatus() throws SchedulerException {
		log.info("Getting Status of Scheduler");
		return getScheduler().getMetaData().getSummary();

	}
	
	public void scheduleJob(Map<String, String> jobDataMap) throws SchedulerException {
		Class act = null;
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			act = Class.forName(jobDataMap.get("CLASS_NAME"));
		} catch (ClassNotFoundException e) {
			log.info("Getting class from class name is failed.");
		}
		JobDetail jobDetail = new JobDetail(jobDataMap.get("JOB_NAME"), jobDataMap.get("JOB_GROUP_NAME"), act);
		JobDataMap jobData = createJobDataMap(jobDataMap, df);
		jobDetail.setJobDataMap(jobData);
		Date scheduledTime = null;
		try {
			scheduledTime = df.parse(jobDataMap.get("SCHEDULED_TIME"));
		} catch (ParseException e1) {
			log.info("Date parse error.");
			e1.printStackTrace();
		}
		Trigger trigger = new SimpleTriggerImpl(jobDataMap.get("JOB_NAME"), jobDataMap.get("JOB_GROUP_NAME") + "/"
				+ jobDataMap.get("SCHEDULED_TIME"), scheduledTime, null, 0, 0);
		try {
			getScheduler().scheduleJob(jobDetail, trigger);
		} catch (SchedulerException e) {
			if (e instanceof ObjectAlreadyExistsException) {
				log.info("Job already exists");
			} else {
				log.info("Job Execution is failed");
				throw e;
			}
		}
	}

	
	public void scheduleJob(JobDetail jobDetail, Date executionTime, String jobBeanName) throws SchedulerException {
		
		jobDetail.setJobClass(getJobClass(jobBeanName));
		String strJobID = jobDetail.getJobDataMap().getString(Job.PROP_JOB_ID);
		Trigger trigger = new SimpleTriggerImpl(strJobID, jobDetail.getJobDataMap().getString(Job.PROP_JOB_GROUP), executionTime,
				null, 0, 0);
		try {
			log.info("Creating new Schedule");
			getScheduler().scheduleJob(jobDetail, trigger);
			log.info("#########################################################################################");
			log.info("Scheduling new Job for JobID  " + strJobID + " Scheduled For :" + executionTime.toString());
			log.info("#########################################################################################");
		} catch (org.quartz.ObjectAlreadyExistsException e) {
			log.error(" JOB ID " + strJobID + " Already Exists ", e);
			throw e;
		} catch (SchedulerException e2) {
			log.error("JOB: " + strJobID + " DID NOT SCHEDULE ! " + e2);
			throw e2;
		}
	}

	public void addJob(String jobDetailBeanName, String jobName, String jobGroupName, String cronExpression)
			throws SchedulerException {

		try {

			if (jobGroupName == null || jobGroupName.equals("")) {
				jobGroupName = Scheduler.DEFAULT_GROUP;
			}
			JobDetail jobDetail = new JobDetail(jobName, jobGroupName, getJobClass(jobDetailBeanName));
			CronTrigger cronTrigger = new CronTriggerImpl(jobName, jobGroupName,cronExpression);
			getScheduler().scheduleJob(jobDetail, cronTrigger);
			log.info("Scheduling Jobs for jobName  " + jobName);

		} catch (Exception e) {
			log.info("Failed when trying to Schedule Jobs for jobName" + jobName + "and JobGroup" + jobGroupName + "EXCEPTION:"
					+ e.getLocalizedMessage());
			throw new SchedulerException("addJob:scheduleJob", e);
		}
	}

	public void removeJob(String jobName, String jobGroupName) throws SchedulerException {
		try {		
			JobKey key = new JobKey(jobName, jobGroupName);
			getScheduler().deleteJob(key);
		} catch (Exception e) {
			log.info("Failed when trying to delete Jobs for jobName" + jobName + "and JobGroup" + jobGroupName + "EXCEPTION:"
					+ e.getLocalizedMessage());
			throw new SchedulerException("deleteJob:scheduleJob", e);
		}
	}

	private Class getJobClass(String jobId) {
		JobDetailImpl  jobDetail = (JobDetailImpl) SchedulerUtils.getInstance().getLocalBean(jobId);
		return jobDetail.getJobClass();
	}

	public void triggerJob(String jobName, String jobGroupName) throws SchedulerException {

		try {
			JobKey key = new JobKey(jobName, jobGroupName);
			getScheduler().triggerJob(key);
			log.info("Finished triggering job" + jobName + " " + jobGroupName);
		} catch (SchedulerException e) {
			log.error(e);
			throw new SchedulerException("addJob:scheduleJob", e);
		}
	}

	private JobDataMap createJobDataMap(Map<String, String> jobDataMap, SimpleDateFormat df) {
		JobDataMap jobData = new JobDataMap();

		for (String jobDataName : jobDataMap.keySet()) {
			if (jobDataName.equals("FLIGHT_ID")) {
				jobData.put(Job.PROP_FlightId, jobDataMap.get("FLIGHT_ID"));
			}
			if (jobDataName.equals("DEPT_AIRPORT")) {
				jobData.put(Job.PROP_DepartureStation, jobDataMap.get("DEPT_AIRPORT"));
			}
			if (jobDataName.equals("FLIGHT_NUMBER")) {
				jobData.put(Job.PROP_FLIGHT_NUMBER, jobDataMap.get("FLIGHT_NUMBER"));
			}
			if (jobDataName.equals("PROP_GDS_IS_SELECTED_SCHED")) {
				if (jobDataMap.get("PROP_GDS_IS_SELECTED_SCHED").equals("Y")) {
					jobData.put(Job.PROP_GDS_IS_SELECTED_SCHED, true);
				} else {
					jobData.put(Job.PROP_GDS_IS_SELECTED_SCHED, false);
				}
			}
			if (jobDataName.equals("PROP_GDS_SSMRECAP_SCHED_START_DATE")) {
				try {
					jobData.put(Job.PROP_GDS_SSMRECAP_SCHED_START_DATE,
							df.parse(jobDataMap.get("PROP_GDS_SSMRECAP_SCHED_START_DATE")));
				} catch (ParseException e1) {
					log.info("Date parse error.");
					e1.printStackTrace();
				}
			}
			if (jobDataName.equals("PROP_GDS_SSMRECAP_SCHED_END_DATE")) {
				try {
					jobData.put(Job.PROP_GDS_SSMRECAP_SCHED_END_DATE,
							df.parse(jobDataMap.get("PROP_GDS_SSMRECAP_SCHED_END_DATE")));
				} catch (ParseException e1) {
					log.info("Date parse error.");
					e1.printStackTrace();
				}
			}
			if (jobDataName.equals("PROP_EXTERNAL_SSMRECAP_EMAIL")) {
				jobData.put(Job.PROP_EXTERNAL_SSMRECAP_EMAIL, jobDataMap.get("PROP_EXTERNAL_SSMRECAP_EMAIL"));
			}
			if (jobDataName.equals("PROP_EXTERNAL_SSMRECAP_SITA")) {
				jobData.put(Job.PROP_EXTERNAL_SSMRECAP_SITA, jobDataMap.get("PROP_EXTERNAL_SSMRECAP_SITA"));
			}
			if (jobDataName.equals("PROP_EXTERNAL_SSMRECAP_SENDING")) {
				if (jobDataMap.get("PROP_EXTERNAL_SSMRECAP_SENDING").equals("Y")) {
					jobData.put(Job.PROP_EXTERNAL_SSMRECAP_SENDING, true);
				} else {
					jobData.put(Job.PROP_EXTERNAL_SSMRECAP_SENDING, false);
				}
			}
			if (jobDataName.equals("PROP_GDS_ID")) {
				jobData.put(Job.PROP_GDS_ID, Integer.parseInt(jobDataMap.get("PROP_GDS_ID")));
			}
			if (jobDataName.equals("PROP_GDS_SSMRECAP_EMAIL")) {
				jobData.put(Job.PROP_GDS_SSMRECAP_EMAIL, jobDataMap.get("PROP_GDS_SSMRECAP_EMAIL"));
			}
			
		}
		return jobData;

	}

	public boolean wasJobScheduled(String jobName, String jobGroup) throws SchedulerException {
		if (SchedulerUtils.getScheduler().getJobDetail(new JobKey(jobName, jobGroup)) != null) {
			return true;
		}
		return false;
	}
}
