package com.isa.thinair.scheduler.core.logic.logicimplementation;

import org.springframework.scheduling.quartz.SchedulerFactoryBean;

public class AutoStaringSchedulerFactoryBean extends SchedulerFactoryBean {

	@Override
	public void afterPropertiesSet() throws Exception {
		super.afterPropertiesSet();
		if (super.isAutoStartup()) {
			startScheduler(super.getScheduler(), 0);
		}

	}
}
