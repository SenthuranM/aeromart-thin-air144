/*
 * Created on Jun 30, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.isa.thinair.scheduler.core.job;

import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.SchedulerException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.isa.thinair.scheduler.api.Job;
import com.isa.thinair.scheduler.api.JobDetail;
import com.isa.thinair.scheduler.core.logic.logicimplementation.ScheduleManager;

/**
 * @author byorn
 * 
 *         TODO To change the template for this generated type comment go to Window - Preferences - Java - Code Style -
 *         Code Templates
 */
public class Somejob extends QuartzJobBean {

	private int timeout;
	private String jobclass;

	/**
	 * Setter called after the ExampleJob is instantiated with the value from the JobDetailBean (5)
	 */
	public void setTimeout(int timeout) {
		this.timeout = timeout;

	}

	public void setJobclass(String jobclass) {
		this.jobclass = jobclass;
	}

	protected void executeInternal(JobExecutionContext ctx) {

		try {

			// create the details of your Job.

			// the job instance
			// Collection dto;
			// iterate dtos;
			// {
			Job job = new CancelBookingJob();

			JobDetail jobDetail = new JobDetail();

			jobDetail.getJobDataMap().put(job.PROP_JOB_ID, Integer.toString(1));
			jobDetail.getJobDataMap().put(job.PROP_JOB_GROUP, new Date().toString());
			jobDetail.getJobDataMap().put(CancelBookingJob.PROP_CUST_CODE, "BBB");
			jobDetail.getJobDataMap().put(CancelBookingJob.PROP_YEAR, "2005");

			jobDetail.setJobClass(CancelBookingJob.class);
			// jobDetail.setDurability(false);
			// jobDetail.setVolatility(false);

			//java.util.Date pnlstartTime = TriggerUtils.getDateOf(56, 59, 01);// ,pnlscheduledDay,pnlscheduledMonth,pnlscheduledYear);

			java.util.Date pnlstartTime = new Date();
			
			// JobDetailBean jobDetail = new JobDetailBean();
			//
			// jobDetail.getJobDataMap().put(job.PROP_JOB_ID, Integer.toString(1));
			// jobDetail.getJobDataMap().put(job.PROP_JOB_GROUP, new Date().toString());
			// jobDetail.getJobDataMap().put(CancelBookingJob.PROP_CUST_CODE, "BBB");
			// jobDetail.getJobDataMap().put(CancelBookingJob.PROP_YEAR,"2005");
			// jobDetail.setJobClass(CancelBookingJob.class);

			// schedule the job.
			ScheduleManager.scheduleJob(jobDetail, pnlstartTime, null, 0, 0);
			// }

		}

		catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// scheduleManager.setJobDetail(jobDetail)
		// ScheduleManager.scheduleJob(ctx, jobId, starttime, end time, num of times, frequecy)

	}

}
