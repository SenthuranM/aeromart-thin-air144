/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.scheduler.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.scheduler.api.service.SchedulerBD;

/**
 * @author Byorn
 */
@Local
public interface SchedulerServiceLocalDelegateImpl extends SchedulerBD {

}
