package com.isa.thinair.scheduler.core.logic.logicimplementation;

import org.springframework.scheduling.quartz.JobDetailFactoryBean;

public class DurableJobDetailFactoryBean extends JobDetailFactoryBean {

	 public DurableJobDetailFactoryBean() {
	        setDurability(true);
	 }
}
