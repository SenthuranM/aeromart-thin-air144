/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:29:42
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduler.core.remoting.ejb;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.scheduler.core.logic.logicimplementation.ImplScheduler;
import com.isa.thinair.scheduler.core.logic.logicinterface.IScheduler;
import com.isa.thinair.scheduler.core.service.bd.SchedulerServiceDelegateImpl;
import com.isa.thinair.scheduler.core.service.bd.SchedulerServiceLocalDelegateImpl;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;
import org.quartz.SchedulerException;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import java.util.Map;

/**
 * @author Byorn
 */
@Stateless
@RemoteBinding(jndiBinding = "SchedulerService.remote")
@LocalBinding(jndiBinding = "SchedulerService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class SchedulerServiceBean extends PlatformBaseSessionBean implements SchedulerServiceDelegateImpl,
		SchedulerServiceLocalDelegateImpl {

	/**
	 * @throws ModuleException
	 * @throws ModuleException
	 */
	public void startScheduler() throws ModuleException {

		try {
			lookupScheduler().start();
		} catch (SchedulerException e) {
			throw new ModuleException(e, "scheduler.start.error", "scheduler.desc");
		}

	}

	/**
	 * @throws ModuleException
	 * @throws ModuleException
	 */
	public void addJob(String jobDetailBeanName, String jobName, String jobGroupName, String cronExpression)
			throws ModuleException {
		try {
			lookupScheduler().addJob(jobDetailBeanName, jobName, jobGroupName, cronExpression);
		} catch (SchedulerException e) {
			throw new ModuleException(e, "scheduler.addjob.error", "scheduler.desc");
		}

	}

	/**
	 * @throws ModuleException
	 * @throws ModuleException
	 */
	public void removeJob(String jobName, String jobGroupName) throws ModuleException {
		try {
			lookupScheduler().removeJob(jobName, jobGroupName);
		} catch (SchedulerException e) {
			throw new ModuleException(e, "scheduler.removejob.error", "scheduler.desc");
		}

	}

	/**
	 * @throws ModuleException
	 * @throws ModuleException
	 */
	public String getStatusOfScheduler() throws ModuleException {
		try {
			return lookupScheduler().getStatus();
		} catch (SchedulerException e) {
			throw new ModuleException(e, "scheduler.getstatus.error", "scheduler.desc");
		}

	}

	/**
	 * @throws ModuleException
	 * @throws ModuleException
	 */
	public void stopScheduler() throws ModuleException {
		try {
			lookupScheduler().stop();
		} catch (SchedulerException e) {
			throw new ModuleException("scheduler.stop.error", e);
		}
	}

	/**
	 * @throws ModuleException
	 * @throws ModuleException
	 */
	public void triggerJob(String jobName, String jobGroupName) throws ModuleException {
		try {
			lookupScheduler().triggerJob(jobName, jobGroupName);
		} catch (SchedulerException e) {
			throw new ModuleException("triggerJob", e);
		}
	}

	private IScheduler lookupScheduler() {
		IScheduler scheduler = new ImplScheduler();
		return scheduler;
	}

	public void scheduleJob(Map<String,String> jobDataMap) throws ModuleException {
		try {
			lookupScheduler().scheduleJob(jobDataMap);
		} catch (SchedulerException e) {
			throw new ModuleException(e, "scheduler.addjob.error", "scheduler.desc");
		}
		
	}

	public boolean wasJobScheduled(String jobName, String jobGroup) throws ModuleException {
		try {
			return lookupScheduler().wasJobScheduled(jobName, jobGroup);
		} catch (SchedulerException e) {
			throw new ModuleException("JobWasScheduled", e);
		}
	}
}
