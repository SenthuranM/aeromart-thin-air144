/*
 * ==============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2003 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Nov 22, 2004
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.scheduler.core.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.isa.thinair.scheduler.api.Job;

/**
 * Commons Framework: Reusable components and templates
 * 
 * @author sudheera
 * 
 *         CancelBookingJob is job class to cancel flight booking
 * 
 */
public class CancelBookingJob implements Job {
	public static final String PROP_CUST_CODE = "propCustCode";

	public static final String PROP_YEAR = "propYear";

	/**
     *  
     */
	public CancelBookingJob() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	public void execute(JobExecutionContext jobExcContext) throws JobExecutionException {
		// JobDetail jobDetail = (JobDetail) jobExcContext.getJobDetail();

		System.out.println("Excuting ......" + jobExcContext.getJobDetail().getJobDataMap().getString(Job.PROP_JOB_ID)
				+ " ....................." + jobExcContext.getJobDetail().getJobDataMap().getString(this.PROP_CUST_CODE));

		// System.out.println("Excuting ......" + jobDetail.getJobDataMap().getString(Job.PROP_JOB_ID) +
		// " ....................." + this.PROP_CUST_CODE);
	}

}