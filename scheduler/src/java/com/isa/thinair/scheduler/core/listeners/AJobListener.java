/*
 * Created on Jul 8, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.isa.thinair.scheduler.core.listeners;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;

import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author byorn
 * 
 *         TODO To change the template for this generated type comment go to Window - Preferences - Java - Code Style -
 *         Code Templates
 */
public class AJobListener implements JobListener {

	private String name;

	private Log log = LogFactory.getLog(AJobListener.class);

	public AJobListener() {

	}

	public AJobListener(String name) {
		setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @see org.quartz.JobListener#jobToBeExecuted(org.quartz.JobExecutionContext)
	 */
	public void jobToBeExecuted(JobExecutionContext context) {

		log.info("Listener " + name + " says: \"Job '" + context.getJobDetail().getDescription() + "' Will be executed.\"");
	}

	/**
	 * @see org.quartz.JobListener#jobExecutionVetoed(org.quartz.JobExecutionContext)
	 */
	public void jobExecutionVetoed(JobExecutionContext context) {
		// log.info("Listener " + name + " says: \"Job '"+context.getJobDetail().getFullName()+"' was vetoed.\"");
	}

	/**
	 * @see org.quartz.JobListener#jobWasExecuted(org.quartz.JobExecutionContext, org.quartz.JobExecutionException)
	 */
	public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {

		if (jobException != null) {

			Throwable cause = jobException.getCause();
			if (cause != null && cause instanceof ModuleException) {
				ModuleException exception = (ModuleException) cause;
				log.error("Listener " + name + " says: \"Job '" + context.getJobDetail().getKey().getName() + "' executed.\""
						+ "error:" + exception.getMessageString());
				log.error("Error: ", jobException);
				return;
			} else {
				// the code should not come here.
				log.error(jobException);
			}

		}

	}

}
