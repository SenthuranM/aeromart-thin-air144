package com.isa.thinair.scheduler.core.logic.logicimplementation;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.triggers.SimpleTriggerImpl;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

import com.isa.thinair.scheduler.api.Job;
import com.isa.thinair.scheduler.api.JobDetail;
import com.isa.thinair.scheduler.api.service.SchedulerUtils;

/**
 * Schedule Manager is the utility class that accepts a JobDetails & corresponding Trigger for a given job id
 * 
 * @author Isuru
 */
public class ScheduleManager {

	private static Log log = LogFactory.getLog(ScheduleManager.class);

	public static void scheduleJob(JobExecutionContext ctx, JobDetail jobDetail, Date startDate, Date endDate, int repeatCount,
			int repeatInterval) throws SchedulerException {

		String jobId = jobDetail.getJobDataMap().getString(Job.PROP_JOB_ID);
		String jobGroup = jobDetail.getJobDataMap().getString(Job.PROP_JOB_GROUP);

		jobDetail.setName(jobId);
		jobDetail.setGroup(jobGroup);
		jobDetail.setDurability(true);

		Trigger trigger = new SimpleTriggerImpl(jobId, jobGroup + "/" + startDate.toString(), startDate, endDate, repeatCount,
				repeatInterval);

		try {
			ctx.getScheduler().scheduleJob(jobDetail, trigger);
		} catch (org.quartz.ObjectAlreadyExistsException e) {
			log.error(" JOB ID " + jobId + " Already Exists ", e);
			throw e;
		}

	}

	public static void scheduleJob(JobDetail jobDetail, Date startDate, Date endDate, int repeatCount, int repeatInterval)
			throws SchedulerException {

		try {
			String jobId = jobDetail.getJobDataMap().getString(Job.PROP_JOB_ID);
			String jobGroup = jobDetail.getJobDataMap().getString(Job.PROP_JOB_GROUP);
			String jobDescription = jobDetail.getJobDataMap().getString(Job.PROP_JOB_DESCRIPTION);
			jobDetail.setName(jobId);
			jobDetail.setGroup(jobGroup);
			jobDetail.setDurability(false); // The Table space is growing unnecessarily.
			Trigger trigger = new SimpleTriggerImpl(jobId, jobGroup + "/" + startDate.toString(), startDate, endDate, repeatCount,
					repeatInterval);
			try {
				SchedulerUtils.getScheduler().scheduleJob(jobDetail, trigger);
			} catch (org.quartz.ObjectAlreadyExistsException e) {
				log.error(" JOB ID " + jobId + " Already Exists ", e);
				throw e;
			}
			log.debug("Scheduling Jobs for JobID  " + jobId + "and JobGroup  " + jobGroup + " Scheduled For :"
					+ startDate.toString());
		} catch (Exception e) {
			log.error(e);
		}
	}

	public static void scheduleJob(JobDetailFactoryBean jobDetail, Date startDate, Date endDate, int repeatCount, int repeatInterval)
			throws SchedulerException {

		String jobId = jobDetail.getJobDataMap().getString(Job.PROP_JOB_ID);
		String jobGroup = jobDetail.getJobDataMap().getString(Job.PROP_JOB_GROUP);
		String jobDescription = jobDetail.getJobDataMap().getString(Job.PROP_JOB_DESCRIPTION);

		jobDetail.setName(jobId);
		jobDetail.setGroup(jobGroup);
		// jobDetail.setDescription(jobDescription);
		//jobDetail.setVolatility(false);
		jobDetail.setDurability(true);

		
		Trigger trigger = new SimpleTriggerImpl(jobId, jobGroup, startDate, endDate, repeatCount, repeatInterval);

		try {
			SchedulerUtils.getScheduler().scheduleJob(jobDetail.getObject(), trigger);
		} catch (org.quartz.ObjectAlreadyExistsException e) {
			log.error(" JOB ID " + jobId + " Already Exists ", e);
			throw e;
		}
		log.debug("Scheduling Jobs for JobID  " + jobId + "and JobGroup  " + jobGroup + " Scheduled For :" + startDate.toString());
	}

	public static boolean jobWasScheduled(String jobName, String jobGroup) throws SchedulerException {

		if (SchedulerUtils.getScheduler().getJobDetail(new JobKey(jobName, jobGroup)) != null) {
			return true;
		}
		return false;

	}

	/**
	 * Creates a new Sheduler Job
	 * 
	 * @param strJobID
	 * @param jobDetail
	 * @param executionTime
	 * @throws SchedulerException
	 */
	public static void createSchedule(org.quartz.JobDetail jobDetail, Date executionTime) throws SchedulerException {
		String strJobID = jobDetail.getJobDataMap().getString(Job.PROP_JOB_ID);
		Trigger trigger = new SimpleTriggerImpl(strJobID, jobDetail.getJobDataMap().getString(Job.PROP_JOB_GROUP), executionTime,
				null, 0, 0);
		try {
			log.info("Creating new Schedule");
			SchedulerUtils.getScheduler().scheduleJob(jobDetail, trigger);
			log.info("#########################################################################################");
			log.info("Scheduling new Job for JobID  " + strJobID + " Scheduled For :" + executionTime.toString());
			log.info("#########################################################################################");
		} catch (org.quartz.ObjectAlreadyExistsException e) {
			log.error(" JOB ID " + strJobID + " Already Exists ", e);
			throw e;
		} catch (SchedulerException e2) {
			log.error("JOB: " + strJobID + " DID NOT SCHEDULE ! " + e2);
			throw e2;
		}
	}

	public static void triggerJob(String jobName, String groupName) throws SchedulerException {
		try {
			log.info(" Triggering Job" + jobName + " Group Name:" + groupName);
			SchedulerUtils.getScheduler().triggerJob(new JobKey(jobName, groupName));
		} catch (SchedulerException e) {
			log.error("trigger job error", e);
			throw e;
		}
	}

}