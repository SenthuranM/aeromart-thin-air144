package com.isa.thinair.scheduler.client;

import java.util.Properties;

import org.springframework.jndi.JndiObjectFactoryBean;

import com.isa.thinair.login.Constants;
import com.isa.thinair.login.client.ClientLoginModule;
import com.isa.thinair.scheduler.api.service.SchedulerBD;

public class SchedulerClient {

	private static String START = "start";
	private static String STOP = "stop";
	private static String STATUS = "status";

	private static String ipAddress = "";
	private static String operation = "";
	private static String port = "";
	private static String pathToLoginConfig = "";

	public static void main(String args[]) throws Exception {

		ipAddress = args[0];
		port = args[1];
		operation = args[2];
		pathToLoginConfig = args[3];

		if (operation.equals(START)) {
			new SchedulerClient().Start();
		}

		if (operation.equals(STOP)) {
			new SchedulerClient().Stop();
		}

		if (operation.equals(STATUS)) {
			new SchedulerClient().printStatus();
		}
	}

	private void Start() throws Exception {
		getScheduler(ipAddress, port).startScheduler();
		System.out.println("Scheduler is started successfully");
	}

	private void Stop() throws Exception {
		getScheduler(ipAddress, port).stopScheduler();
		System.out.println("Scheduler has shut down successfully");
	}

	private void printStatus() throws Exception {
		String status = getScheduler(ipAddress, port).getStatusOfScheduler();
		System.out.println(status);
	}

	private SchedulerBD getScheduler(String ipAddress, String portNumber) throws Exception {
		SchedulerBD schedulerBD = null;

		Properties properties = new Properties();
		properties.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
		properties.put("java.naming.provider.url", "jnp://" + ipAddress + ":" + portNumber);

		try {
			ClientLoginModule clientLoginModule = new ClientLoginModule();
			clientLoginModule.login(Constants.WEB_USER_ID, Constants.WEB_USER_ID, pathToLoginConfig, null);

			JndiObjectFactoryBean jndiObjectFactoryBean = new JndiObjectFactoryBean();
			jndiObjectFactoryBean.setJndiEnvironment(properties);
			jndiObjectFactoryBean.setJndiName("SchedulerService.remote");
			jndiObjectFactoryBean.afterPropertiesSet();

			schedulerBD = (SchedulerBD) jndiObjectFactoryBean.getObject();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return schedulerBD;
	}

}
