/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 8, 2005
 * 
 * Client.java
 * 
 * ============================================================================
 */
package com.isa.thinair.web.client;

import java.util.Random;

import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.samplemodule.api.model.SampleOjbect;
import com.isa.thinair.samplemodule.api.service.SampleServiceBD;

/**
 * @author Nasly
 * 
 */
public class Client {

	/**
	 * 
	 */
	public Client() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void saveSampleObject() {
		LookupService lookup = LookupServiceFactory.getInstance();
		IModule sampleModule = lookup.getModule("samplemodule");
		System.out.println("Lookup successful...");
		SampleServiceBD delegate = null;
		try {
			delegate = (SampleServiceBD) sampleModule.getServiceBD("sample.service.local");
			System.out.println("Delegate creation successful...");
		} catch (Exception e) {
			System.out.println("Exception in creating delegate");
			e.printStackTrace();
		}
		SampleOjbect obj = new SampleOjbect();
		obj.setId(getRandomId(8));
		obj.setMessage("test");
		if (delegate != null) {
			try {
				delegate.saveOrUpdate(obj);
			} catch (Exception e1) {
				System.out.println("Exception in saving object");
				e1.printStackTrace();
			}
		}

		if (delegate != null) {
			try {
				delegate.delete(obj.getId());
			} catch (Exception e1) {
				System.out.println("Exception in deleting object");
				e1.printStackTrace();
			}
		}
	}

	private String getRandomId(int length) {
		char[] alpahbet = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm' };
		Random rand = new Random();
		int n = 12;
		char[] ranId = new char[length];
		for (int j = 0; j < length; j++) {
			int i = rand.nextInt(n + 1);
			ranId[j] = alpahbet[i];
		}
		return new String(ranId);
	}

}
