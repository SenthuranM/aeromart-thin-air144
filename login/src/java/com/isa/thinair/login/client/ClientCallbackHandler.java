package com.isa.thinair.login.client;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 * @author Mohamed Nasly
 */
public class ClientCallbackHandler implements CallbackHandler {

	/** Username which will be set in the NameCallback, when NameCallback is handled */
	private String username;

	/** Password which will be set in the PasswordCallback, when PasswordCallback is handled */
	private String password;

	public ClientCallbackHandler(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public void handle(Callback callbacks[]) throws IOException, UnsupportedCallbackException {

		for (int i = 0; i < callbacks.length; i++) {
			if (callbacks[i] instanceof NameCallback) {
				NameCallback nc = (NameCallback) callbacks[i];
				nc.setName(username);
			} else if (callbacks[i] instanceof PasswordCallback) {
				PasswordCallback pc = (PasswordCallback) callbacks[i];
				pc.setPassword(password.toCharArray());
			} else {
				throw new UnsupportedCallbackException(callbacks[i], "Unrecognized Callback");
			}
		}
	}
}