/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.login.module;

import java.security.Principal;
import java.security.acl.Group;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;
import javax.security.jacc.PolicyContext;
import javax.security.jacc.PolicyContextException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.security.SimpleGroup;
import org.jboss.security.SimplePrincipal;
import org.jboss.security.auth.spi.DatabaseServerLoginModule;

import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.login.Constants;
import com.isa.thinair.login.util.DatabaseUtil;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.login.util.ForceLoginInvoker.LOGIN_TYPES;
import com.isa.thinair.login.util.StringUtil;

/**
 * Jboss Main login module
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class JBossLoginModule extends DatabaseServerLoginModule {

	/** Holds the custom principal */
	private UserPrincipal caller;

	/** Holds the customer query will be loaded from the login-config */
	private String principalsQueryCustomer = "SELECT a.password " + "FROM t_customer a WHERE a.status ='C' and a.email_id = ?";

	private String principalsQueryForInactiveUser = "SELECT a.password  FROM t_user a where a.user_id=? AND a.status = 'INA'";

	public static final String ERROR_FOR_INACTIVE_USER = "User/Agent ID is INACTIVE";

	public static final String ERROR_FOR_INACTIVE_AGENT = "Staff of a inactive agent";

	/** The JACC PolicyContext key for the current Subject */
	public static final String WEB_REQUEST_KEY = "javax.servlet.http.HttpServletRequest";

	private static final Log logger = LogFactory.getLog(JBossLoginModule.class);

	private static String scrHeight = "";
	
	public static final String APPLICATION_IDENTIFIER = "application.identifier";

	/**
	 * Initialize the login module
	 * 
	 * @param subject
	 * @param callbackHandler
	 * @param sharedState
	 * @param options
	 */
	public void initialize(Subject subject, CallbackHandler callbackHandler, Map sharedState, Map options) {
		super.initialize(subject, callbackHandler, sharedState, options);

		Object tmp = options.get("principalsQueryCustomer");

		if (tmp != null) {
			principalsQueryCustomer = tmp.toString();
		}
	}

	/**
	 * Login method
	 * 
	 * @return
	 * @throws LoginException
	 */
	public boolean login() throws LoginException {
		try {
			HttpServletRequest request = (HttpServletRequest) PolicyContext.getContext(WEB_REQUEST_KEY);

			if (super.getUsernameAndPassword() != null && super.getUsernameAndPassword().length == 2) {
				logger.debug("Username:" + filterUserName(super.getUsernameAndPassword()[0]));
			}
			resetIsInactiveUserLogin(request);
			if (super.login()) {

				String userText = super.getIdentity().getName();

				// Find out the login type
				LOGIN_TYPES loginType = this.getLoginType(userText);
				if (loginType == ForceLoginInvoker.LOGIN_TYPES.XBE_LOGIN_TYPE) {
					userText = getUsernameFromXBELogin(userText);
					loginType = ForceLoginInvoker.LOGIN_TYPES.AGENT_LOGIN_TYPE;
				}

				int salesChannelCode = -1;
				Integer customerId = null;
				String customerEmailId = null;
				String userId = null;
				String agentCode = null;
				Collection colUserDST = null;
				Collection carrierCodes = null;
				String defaultCarrierCode = null;
				String airlineCode = null;
				String ipAddress = null;
				String fwdIpAddress = null;
				String requestedURL = null;
				String browser = null;
				String[] agentInfo = null;
				String screenHeight = null;
				String sessionId = null;

				// Agent Login Type
				if (loginType == ForceLoginInvoker.LOGIN_TYPES.AGENT_LOGIN_TYPE) {
					salesChannelCode = SalesChannelsUtil.SALES_CHANNEL_AGENT;
					userId = userText;
					agentCode = DatabaseUtil.getAgentCode(userId);
					agentInfo = DatabaseUtil.getAgentInfo(agentCode);
					ipAddress = getIPAddress(request);
					fwdIpAddress = getForwardedIP(request);
					requestedURL = getRequestedURL(request);
					browser = getUserBrowser(request);
					sessionId = getSessionId(request);
					screenHeight = scrHeight;

					// Inactive agent
					if (agentInfo[1] == null) {
						throw new LoginException(ERROR_FOR_INACTIVE_AGENT);
					}

					carrierCodes = DatabaseUtil.getCarrierCodes(userId);
					defaultCarrierCode = DatabaseUtil.getDefaultCarrierCode(userId);
					airlineCode = DatabaseUtil.getAirlineIdentifierCode(userId);
					colUserDST = DatabaseUtil.getAgentLocalTime(agentInfo[1]);

				}
				// Kiosk Login Type
				else if (loginType == ForceLoginInvoker.LOGIN_TYPES.KIOSK_LOGIN_TYPE) {
					salesChannelCode = SalesChannelsUtil.SALES_CHANNEL_WEB;
					userId = this.getUserIdFromKioskUserText(userText);
					customerEmailId = this.getCustomerEmailIdFromKioskUserText(userText);
					agentCode = DatabaseUtil.getAgentCode(userId);
					agentInfo = DatabaseUtil.getAgentInfo(agentCode);
					defaultCarrierCode = DatabaseUtil.getDefaultCarrierCode(userId);
					airlineCode = DatabaseUtil.getAirlineIdentifierCode(userId);
					ipAddress = getIPAddress(request);
					fwdIpAddress = getForwardedIP(request);
					requestedURL = getRequestedURL(request);
					browser = getUserBrowser(request);
					// sessionId = getSessionId(request);
					screenHeight = null;

					// Inactive agent
					if (agentInfo[1] == null) {
						throw new LoginException(ERROR_FOR_INACTIVE_AGENT);
					}

					colUserDST = DatabaseUtil.getAgentLocalTime(agentInfo[1]);

					// Loading the customer id since we only have the customer email id
					if (!customerEmailId.equals(Constants.WEB_USER_ID)) {
						customerId = new Integer(DatabaseUtil.getCustomerId(customerEmailId));
					}
				}
				// IBE login type
				else if (loginType == ForceLoginInvoker.LOGIN_TYPES.IBE_LOGIN_TYPE) {
					salesChannelCode = getWebSalesChannel(request);
					customerEmailId = userText;
					ipAddress = getIPAddress(request);
					fwdIpAddress = getForwardedIP(request);
					requestedURL = getRequestedURL(request);
					browser = getUserBrowser(request);
					// sessionId = getSessionId(request);
					screenHeight = null;

					// Loading the customer id since we only have the customer email id
					if (!customerEmailId.equals(Constants.WEB_USER_ID)) {
						customerId = new Integer(DatabaseUtil.getCustomerId(customerEmailId));
					}
				}
				// WS login type
				else if (loginType == ForceLoginInvoker.LOGIN_TYPES.WS_LOGIN_TYPE) {
					salesChannelCode = this.getSalesChannelFromWSUserText(userText);

					userId = this.getUserIdFromWSUserText(userText);
					agentCode = DatabaseUtil.getAgentCode(userId);
					agentInfo = DatabaseUtil.getAgentInfo(agentCode);
					defaultCarrierCode = DatabaseUtil.getDefaultCarrierCode(userId);
					airlineCode = DatabaseUtil.getAirlineIdentifierCode(userId);
					ipAddress = getIPAddress(request);
					fwdIpAddress = getForwardedIP(request);
					requestedURL = getRequestedURL(request);
					browser = getUserBrowser(request);
					sessionId = getSessionId(request);
					screenHeight = null;

					// Inactive agent
					if (agentInfo[1] == null) {
						throw new LoginException(ERROR_FOR_INACTIVE_AGENT);
					}

					colUserDST = DatabaseUtil.getAgentLocalTime(agentInfo[1]);
				}

				// HahnAir login type
				else if (loginType == ForceLoginInvoker.LOGIN_TYPES.HAHNAIR_LOGIN_TYPE) {
					salesChannelCode = SalesChannelsUtil.SALES_CHANNEL_GDS;

					userId = this.getUserIdFromHahnAirUserText(userText);
					agentCode = DatabaseUtil.getAgentCode(userId);
					agentInfo = DatabaseUtil.getAgentInfo(agentCode);
					defaultCarrierCode = DatabaseUtil.getDefaultCarrierCode(userId);
					airlineCode = DatabaseUtil.getAirlineIdentifierCode(userId);
					ipAddress = getIPAddress(request);
					fwdIpAddress = getForwardedIP(request);
					requestedURL = getRequestedURL(request);
					browser = getUserBrowser(request);
					sessionId = getSessionId(request);
					screenHeight = null;

				}

				if (agentInfo != null) {

					caller = (UserPrincipal) UserPrincipal.createIdentity(userText, salesChannelCode, agentCode, agentInfo[1],
							customerId, userId, colUserDST, null, carrierCodes, defaultCarrierCode, airlineCode, agentInfo[0],
							agentInfo[2], ipAddress, browser, screenHeight, sessionId);
				} else {
					caller = (UserPrincipal) UserPrincipal.createIdentity(userText, salesChannelCode, agentCode, null,
							customerId, userId, colUserDST, null, carrierCodes, defaultCarrierCode, airlineCode, null, null,
							ipAddress, browser, screenHeight, sessionId);
				}

				if (logger.isInfoEnabled()) {
					logger.info("Login success for [UserId=" + userId + ",agentCode=" + agentCode + ",Channel="
							+ salesChannelCode
							+ ((customerId != null && !"".equals(customerId)) ? (",customerId=" + customerId) : "")
							+ ",loginTypeDesc=" + loginType.getLoginTypeDescription() + "\n\r" + ",ipAddress=" + ipAddress
							+ ",fwdIpAddress=" + fwdIpAddress + ",browser=" + browser + ",requestedURL=" + requestedURL
							+ ",screenHeight=" + screenHeight + ", callingInstanceId="
							+ (caller != null ? caller.getCallingInstanceId() : "{caller null}") + "]");
				}

				if ((loginType != ForceLoginInvoker.LOGIN_TYPES.IBE_LOGIN_TYPE) && applicableAccountLock(userText)) {
					String message = getUserAccountLockStatus(userText, true);
					if (message != null) {
						request.setAttribute("lockedMessage", message);
						return false;
					}
				}

				return true;
			} else {
				logger.info("Login failed................");
				return false;
			}

		} catch (RuntimeException e) {
			logger.error(e);
			throw new LoginException(e.getMessage());
		} catch (Throwable e) {
			logger.error(e);
			HttpServletRequest request = null;
			String userText = super.getIdentity().getName();
			if (applicableAccountLock(userText)) {
				LOGIN_TYPES loginType = this.getLoginType(userText);
				if (loginType == ForceLoginInvoker.LOGIN_TYPES.XBE_LOGIN_TYPE) {
					userText = getUsernameFromXBELogin(userText);
				}
				try {
					String message = getUserAccountLockStatus(userText, false);
					request = (HttpServletRequest) PolicyContext.getContext(WEB_REQUEST_KEY);
					auditLoginFailure(userText, request);
					if (message != null) {
						request.setAttribute("lockedMessage", message);
					}
				} catch (Exception ex) {
					logger.error(ex);
				}
				HttpSession session = request.getSession();
				session.setAttribute("isInactiveUserLogin", true);
			} else if (e.getLocalizedMessage().equals(ERROR_FOR_INACTIVE_USER)
					|| e.getLocalizedMessage().equals(ERROR_FOR_INACTIVE_AGENT)) {
				try {
					request = (HttpServletRequest) PolicyContext.getContext(WEB_REQUEST_KEY);
				} catch (PolicyContextException e1) {
					logger.error(e1);
				}
				HttpSession session = request.getSession();
				session.setAttribute("isInactiveUserLogin", true);
			}
			logger.error(e);

			try {
				request = (HttpServletRequest) PolicyContext.getContext(WEB_REQUEST_KEY);
				boolean isCaptchaValidated = (boolean) request.getSession().getAttribute(Constants.LOGIN_CAPTCHA);
				if (isCaptchaValidated) {
					request.getSession().setAttribute(Constants.LOGIN_CAPTCHA, false);
				}
			} catch (PolicyContextException e1) {
				logger.error(e1);
			} catch (NullPointerException e1) {
				logger.error(e1);
			}
			
			throw new LoginException(e.getMessage());
		}

	}

	private boolean applicableAccountLock(String userText) {
		return AppSysParamsUtil.isEnableAccountLock() && AppSysParamsUtil.includeAccountLock(userText);
	}

	/**
	 * decrypt the password
	 */
	protected String convertRawPassword(String password) {
		return StringUtil.decrypt(password);
	}

	/**
	 * gets the identity
	 */
	protected Principal getIdentity() {
		Principal identity = caller;

		if (identity == null) {
			identity = super.getIdentity();
		}

		return identity;
	}

	/**
	 * returns the role sets
	 */
	protected Group[] getRoleSets() throws LoginException {
		try {
			// The declarative permissions
			Group roles = new SimpleGroup("Roles");
			// The caller identity
			Group callerPrincipal = new SimpleGroup("CallerPrincipal");

			// Create the groups
			Group[] newGroups = { roles, callerPrincipal };

			// Add the user role
			roles.addMember(new SimplePrincipal("user"));

			// Add the custom principal for the caller
			callerPrincipal.addMember(caller);

			return newGroups;
		} catch (Exception e) {
			logger.error("Failed to obtain groups for user = " + getUsername(), e);
			throw new LoginException(e.toString());
		}
	}

	/**
	 * Find out the login type
	 * 
	 * @param userText
	 * @return
	 */
	private LOGIN_TYPES getLoginType(String userText) {

		int kioskIndex = userText.indexOf(Constants.KIOSK_LOGIN);
		int wsIndex = userText.indexOf(Constants.WS_LOGIN);
		int xbeIndex = userText.indexOf(Constants.XBE_LOGIN_TYPE);
		int hahnAirIndex = userText.indexOf(Constants.HAHNAIR_LOGIN);

		if (userText.equals(Constants.WEB_USER_ID) || userText.indexOf("@") > 0 || kioskIndex > 0 || wsIndex > 0 || xbeIndex > 0
				|| hahnAirIndex > 0) {

			if (kioskIndex > 0 && wsIndex == -1 && xbeIndex == -1 && hahnAirIndex == -1) {
				return ForceLoginInvoker.LOGIN_TYPES.KIOSK_LOGIN_TYPE;
			} else if (wsIndex > 0 && kioskIndex == -1 && xbeIndex == -1 && hahnAirIndex == -1) {
				return ForceLoginInvoker.LOGIN_TYPES.WS_LOGIN_TYPE;
			} else if (xbeIndex > 0 && kioskIndex == -1 && wsIndex == -1 && hahnAirIndex == -1) {
				return ForceLoginInvoker.LOGIN_TYPES.XBE_LOGIN_TYPE;
			} else if (hahnAirIndex > 0 && kioskIndex == -1 && wsIndex == -1 && xbeIndex == -1) {
				return ForceLoginInvoker.LOGIN_TYPES.HAHNAIR_LOGIN_TYPE;
			} else {
				return ForceLoginInvoker.LOGIN_TYPES.IBE_LOGIN_TYPE;
			}

		} else {
			return ForceLoginInvoker.LOGIN_TYPES.AGENT_LOGIN_TYPE;
		}
	}

	/**
	 * Return user id from kiosk user text
	 * 
	 * @param userText
	 * @return
	 */
	private String getUserIdFromKioskUserText(String userText) {
		return userText.substring(0, userText.indexOf(Constants.KIOSK_LOGIN));
	}

	/**
	 * Return customer email id from Kiosk user text
	 * 
	 * @param userText
	 * @return
	 */
	private String getCustomerEmailIdFromKioskUserText(String userText) {
		return userText.substring(userText.indexOf(Constants.KIOSK_LOGIN) + Constants.KIOSK_LOGIN.length(), userText.length());
	}

	/**
	 * Returns the user id from the web service user text
	 * 
	 * @param userText
	 * @return
	 */
	private String getUserIdFromWSUserText(String userText) {
		return userText.substring(0, userText.indexOf(Constants.WS_LOGIN));
	}

	/**
	 * Returns the user id from hahn air user text
	 * 
	 * @param userText
	 * @return
	 */
	private String getUserIdFromHahnAirUserText(String userText) {
		return userText.substring(0, userText.indexOf(Constants.HAHNAIR_LOGIN));
	}

	/**
	 * Returns the sales channel id from the web service user text
	 * 
	 * @param userText
	 * @return
	 * @throws LoginException
	 */
	private int getSalesChannelFromWSUserText(String userText) throws LoginException {
		String salesChannelId = userText.substring(userText.lastIndexOf(Constants.WS_LOGIN) + Constants.WS_LOGIN.length());
		return SalesChannelsUtil.validateAndGetSalesChannelForWS(salesChannelId);
	}

	/**
	 * Returns the users password
	 */
	protected String getUsersPassword() throws LoginException {
		String userText = getUsername();

		LOGIN_TYPES loginType = this.getLoginType(userText);

		// Agent Login Type
		if (loginType == ForceLoginInvoker.LOGIN_TYPES.AGENT_LOGIN_TYPE) {
			return this.authenticateUser(userText, principalsQuery, principalsQueryForInactiveUser);
			// XBE Login Type
		} else if (loginType == ForceLoginInvoker.LOGIN_TYPES.XBE_LOGIN_TYPE) {
			return this.authenticateUser(getUsernameFromXBELogin(userText), principalsQuery, principalsQueryForInactiveUser);
			// Kiosk Login Type
		} else if (loginType == ForceLoginInvoker.LOGIN_TYPES.KIOSK_LOGIN_TYPE) {
			String actualUserId = this.getUserIdFromKioskUserText(userText);
			String actualCustomerEmailId = this.getCustomerEmailIdFromKioskUserText(userText);

			if (actualCustomerEmailId.equals(Constants.WEB_USER_ID)) {
				return this.authenticateUser(actualUserId, principalsQuery, null) + Constants.KIOSK_PASS + Constants.WEB_USER_ID;
			} else {
				return this.authenticateUser(actualUserId, principalsQuery, null) + Constants.KIOSK_PASS
						+ this.authenticateUser(actualCustomerEmailId, principalsQueryCustomer, null);
			}
			// IBE Login Type
		} else if (loginType == ForceLoginInvoker.LOGIN_TYPES.IBE_LOGIN_TYPE) {
			if (userText.equals(Constants.WEB_USER_ID)) {
				return userText;
			} else {
				return this.authenticateUser(userText, principalsQueryCustomer, principalsQueryForInactiveUser);
			}
			// WS Login Type
		} else if (loginType == ForceLoginInvoker.LOGIN_TYPES.WS_LOGIN_TYPE) {
			String actualUserId = this.getUserIdFromWSUserText(userText);
			return this.authenticateUser(actualUserId, principalsQuery, null);
			// Hahn air booking
		} else if (loginType == ForceLoginInvoker.LOGIN_TYPES.HAHNAIR_LOGIN_TYPE) {
			String actualUserId = this.getUserIdFromHahnAirUserText(userText);
			return this.authenticateUser(actualUserId, principalsQuery, null);
		}

		throw new FailedLoginException("No matching username found in Principals");
	}

	protected String getUsernameFromXBELogin(String xbeLogin) {
		return (filterUserName(xbeLogin).substring(0, filterUserName(xbeLogin).indexOf(Constants.XBE_LOGIN_TYPE))).split("@")[0];
	}

	private String filterUserName(String xbeLoginValue) {
		String xbeLogin = "";
		if (xbeLoginValue.indexOf("|") > -1) {
			xbeLogin = xbeLoginValue.split("\\|")[0];
			scrHeight = xbeLoginValue.split("\\|")[1];
		} else {
			xbeLogin = xbeLoginValue;
		}
		return xbeLogin;
	}

	/**
	 * Authenticate the user
	 * 
	 * @param principalsQueryForInactiveUser
	 *            TODO
	 * @param userName
	 * 
	 * @return
	 * @throws LoginException
	 */
	private String authenticateUser(String actualUserName, String principalQuery, String principalsQueryForInactiveUser)
			throws LoginException {
		Connection conn = null;
		PreparedStatement ps = null;
		String password = null;

		try {
			conn = DataSourceUtilities.getDataSource(dsJndiName).getConnection();
			// Get the password
			ps = conn.prepareStatement(principalQuery);
			ps.setString(1, actualUserName);
			ResultSet rs = ps.executeQuery();

			if (rs.next() == false) {
				if (principalsQueryForInactiveUser != null && AppSysParamsUtil.displaySpecificErrorsForIncorrectLogin()) {
					ps = conn.prepareStatement(principalsQueryForInactiveUser);
					ps.setString(1, actualUserName);
					ResultSet result = ps.executeQuery();
					String userPassword = getUsernameAndPassword()[1];
					if (result.next() == true) {
						password = result.getString(1);
						password = convertRawPassword(password);
						result.close();
						if (validatePassword(password, userPassword)) {
							throw new FailedLoginException(ERROR_FOR_INACTIVE_USER);
						} else {
							throw new FailedLoginException("No matching username found in Principals");
						}
					} else {
						throw new FailedLoginException("No matching username found in Principals");
					}
				} else {
					throw new FailedLoginException("No matching username found in Principals");
				}
			}

			password = rs.getString(1);
			password = convertRawPassword(password);
			rs.close();
		} catch (SQLException ex) {
			logger.error("Query failed", ex);
			throw new LoginException(ex.toString());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException ex) {
				}
			}
		}

		return password;
	}

	private String getIPAddress(HttpServletRequest request) {
		String ipAddress = "";
		try {
			ipAddress = request.getRemoteAddr().toString();
		} catch (Exception ex) {
			ipAddress = "-";
		}
		return ipAddress;
	}

	private String getForwardedIP(HttpServletRequest request) {
		String fwdIPAddress = "";
		try {
			fwdIPAddress = request.getHeader("");
		} catch (Exception e) {
			fwdIPAddress = "-";
		}
		return fwdIPAddress;
	}

	private String getRequestedURL(HttpServletRequest request) {
		String requestedURL = "";
		try {
			requestedURL = request.getRequestURL().toString();
		} catch (Exception e) {
			requestedURL = "-";
		}
		return requestedURL;
	}

	private String getUserBrowser(HttpServletRequest request) {
		String browser = "";
		try {
			browser = request.getHeader("User-Agent");
		} catch (Exception ex) {
			browser = "-";
		}
		return browser;
	}

	private String getSessionId(HttpServletRequest request) {
		String sessionId = "";
		try {
			sessionId = request.getSession().getId();
		} catch (Exception ex) {
			sessionId = UUID.randomUUID().toString();
			logger.warn("SESSION ID IS NOT AVAILABLE and generating a random id" + sessionId);
		}
		return sessionId;
	}

	private String getUserAccountLockStatus(String userID, boolean clearLock) throws Exception {
		String message = null;
		User user = getUser(userID);
		Integer loginAttempts = user.getLoginAttempts();
		Date lockReleaseDate = user.getAccountLockReleaseTime();
		int maxLoginAttepmts = AppSysParamsUtil.getUserLoginMaxAttempts();
		int accountLockReleaseMinutes = AppSysParamsUtil.getAccountLockReleaseMinutes();
		Date currentTime = Calendar.getInstance().getTime();

		if (clearLock && lockReleaseDate == null) {
			user.setLoginAttempts(0);
			return message;
		} else if (clearLock && (currentTime.compareTo(lockReleaseDate) >= 0)) {
			user.setAccountLockReleaseTime(null);
			user.setLoginAttempts(0);
		} else {
			if (lockReleaseDate != null) {
				if (currentTime.compareTo(lockReleaseDate) >= 0) {
					user.setAccountLockReleaseTime(null);
					user.setLoginAttempts(1);
				} else {
					message = CalendarUtil.formatDate(lockReleaseDate);
					return message;
				}
			} else if (loginAttempts != null) {
				if ((loginAttempts + 1) > maxLoginAttepmts) {
					Calendar todayCal = Calendar.getInstance();
					todayCal.add(Calendar.MINUTE, accountLockReleaseMinutes);
					user.setAccountLockReleaseTime(todayCal.getTime());
					user.setLoginAttempts(loginAttempts + 1);
					message = CalendarUtil.formatDate(todayCal.getTime());
				} else {
					user.setLoginAttempts(loginAttempts + 1);
				}
			} else {
				user.setLoginAttempts(1);
			}
		}
		updateUser(user);

		return message;
	}

	private User getUser(String userID) throws LoginException {
		Connection conn = null;
		PreparedStatement ps = null;
		User user = new User();

		try {
			conn = DataSourceUtilities.getDataSource(dsJndiName).getConnection();
			ps = conn.prepareStatement("SELECT login_attempts,  account_lock_release_date FROM t_user WHERE user_id = ? ");
			ps.setString(1, userID);
			ResultSet rs = ps.executeQuery();
			if (rs.next() == false) {
				throw new FailedLoginException("No matching username found in Principals");
			}
			user.setUserId(userID);
			user.setLoginAttempts(rs.getInt(1));
			user.setAccountLockReleaseTime(rs.getTimestamp(2));

			rs.close();
		} catch (SQLException ex) {
			logger.error("Query failed", ex);
			throw new LoginException(ex.toString());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException ex) {
				}
			}
		}

		return user;
	}

	private void updateUser(User user) throws LoginException {
		Connection conn = null;
		PreparedStatement ps = null;

		try {
			conn = DataSourceUtilities.getDataSource(dsJndiName).getConnection();
			ps = conn.prepareStatement(" update t_user set login_attempts=?, account_lock_release_date=?  WHERE user_id =? ");
			ps.setInt(1, user.getLoginAttempts());
			if (user.getAccountLockReleaseTime() == null) {
				ps.setNull(2, java.sql.Types.DATE);
			} else {
				ps.setTimestamp(2, new java.sql.Timestamp(user.getAccountLockReleaseTime().getTime()));
			}
			ps.setString(3, user.getUserId());
			int rs = ps.executeUpdate();
			if (rs == 0) {
				log.error("Update fail");
			}
		} catch (SQLException ex) {
			logger.error("Query failed", ex);
			throw new LoginException(ex.toString());
		} catch (Exception ex) {
			logger.error(ex);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException ex) {
				}
			}
		}

	}

	private void auditLoginFailure(String userID, HttpServletRequest request) throws LoginException {
		Connection conn = null;
		PreparedStatement ps = null;
		StringBuilder content = new StringBuilder();
		content.append("USERID:=").append(userID).append("||");
		content.append("Timestamp:=").append(new Date()).append("||");
		content.append("RemoteAddr:=").append(request.getRemoteAddr()).append("||");
		content.append("RemoteHost:=").append(request.getRemoteHost()).append("||");
		content.append("X-Forwarded-For").append(request.getHeader("X-Forwarded-For"));

		try {
			conn = DataSourceUtilities.getDataSource(dsJndiName).getConnection();
			ps = conn
					.prepareStatement(" INSERT INTO t_audit (audit_id, task_code, timestamp, app_code, user_id, content, version, "
							+ "ip_address) VALUES(s_audit.nextval, '2032', ?, 'xbe', ?, ?, 0,null) ");
			ps.setTimestamp(1, new java.sql.Timestamp((new Date()).getTime()));
			ps.setString(2, userID);
			ps.setString(3, content.toString());
			int rs = ps.executeUpdate();
			if (rs == 0) {
				log.error("Insert fail");
			}
		} catch (SQLException ex) {
			logger.error("Query failed", ex);
			throw new LoginException(ex.toString());
		} catch (Exception ex) {
			logger.error(ex);
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException ex) {
				}
			}
		}

	}

	private void resetIsInactiveUserLogin(HttpServletRequest request) {
		try {
			HttpSession session = request.getSession();
			request.setAttribute("isInactiveUserLogin", false);
			session.setAttribute("isInactiveUserLogin", false);
		} catch (Exception ex) {
			logger.warn("SESSION Value for isInactiveUserLogin is Unavailable");
		}
	}
	
	private int getWebSalesChannel(HttpServletRequest request) {
		int salesChannelCode = SalesChannelsUtil.SALES_CHANNEL_WEB;
		try {
			String appId = System.getProperty(APPLICATION_IDENTIFIER);

			if (appId != null) {
				switch (appId) {
				case SalesChannelsUtil.SALES_CHANNEL_IOS_KEY:
					salesChannelCode = SalesChannelsUtil.SALES_CHANNEL_IOS;
					break;
				case SalesChannelsUtil.SALES_CHANNEL_ANDROID_KEY:
					salesChannelCode = SalesChannelsUtil.SALES_CHANNEL_ANDROID;
					break;
				default:
					break;
				}
			}
		} catch (Exception ex) {
			logger.error("Error while determining web sales channel, Default web channel will be returned");
		}
		return salesChannelCode;
	}
}