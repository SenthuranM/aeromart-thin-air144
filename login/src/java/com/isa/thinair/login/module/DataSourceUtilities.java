/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.login.module;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.login.LoginException;
import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * DataSource Utilites
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class DataSourceUtilities {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(DataSourceUtilities.class);

	/** Holds the DataSource */
	private static DataSource ds;

	/** Holds the dsJndiName */
	private static String dsJndiName;

	/** Holds the lock object */
	private static Object lock = new Object();

	/**
	 * Protect it from instantiation
	 */
	private DataSourceUtilities() {

	}

	/**
	 * Acquire the Data Source
	 * 
	 * @return
	 * @throws LoginException
	 */
	private static DataSource acquireDataSource(String dsJndiName) throws LoginException {
		// Looking up the DataSource Only once. This is high trafic method
		// Inorder to increase the performance
		if (ds == null) {
			try {
				ds = (DataSource) new InitialContext().lookup(dsJndiName);
				return ds;
			} catch (NamingException ex) {
				log.error("Query failed", ex);
				throw new LoginException(ex.toString(true));
			}
		} else {
			return ds;
		}
	}

	/**
	 * Returns the data source
	 * 
	 * @param refDSJndiName
	 * @return
	 * @throws LoginException
	 */
	public static DataSource getDataSource(String refDSJndiName) throws LoginException {
		if (refDSJndiName == null) {
			throw new LoginException("Invalid JNDI Name");
		}

		if (dsJndiName == null) {
			synchronized (lock) {
				dsJndiName = refDSJndiName;
				return acquireDataSource(dsJndiName);
			}
		} else {
			return acquireDataSource(dsJndiName);
		}
	}
}
