/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.login.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * Login module's service interface
 * 
 * @isa.module.service-interface module-name="login" description="login module"
 */
public class LoginService extends DefaultModule {
}
