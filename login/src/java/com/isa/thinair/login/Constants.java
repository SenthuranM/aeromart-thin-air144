/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.login;

public interface Constants {

	public static final String MODULE_CODE = "login.desc";

	public static final String WEB_USER_ID = "$IBEWEB$";

	public static final String KIOSK_LOGIN = "$KTYPE$";

	public static final String KIOSK_PASS = "$KPASS$";

	public static final String WS_LOGIN = "$WS$";

	public static final String HAHNAIR_LOGIN = "$HAHNAIR$";

	public static final String XBE_LOGIN_TYPE = "$XBETYPE$";

	public static final String ISA_CLIENT = "ISAClient";

	public static final String GMT_OFFSET_ACTION_MINUS = "-";
	
	public static final String LOGIN_CAPTCHA = "isCaptchaValidated";
}
