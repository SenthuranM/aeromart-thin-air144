/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.login.util;

import java.util.Collection;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;

/**
 * Class to access database for the usage of combobox data
 * 
 * @author Byorn
 */
public class LoginDAO {

	/** data souce to get system params */
	private DataSource dataSource;

	/** properties to store queries retrive column values * */
	private Properties selectSql;

	/**
	 * 
	 * @param moduleProperty
	 * @return
	 */
	public Collection getList(String moduleProperty, String[] params, int colCount) {
		JdbcTemplate jt = new JdbcTemplate(dataSource);

		String sql = selectSql.getProperty(moduleProperty);

		PreparedStatementCreator psCreator = new BasicPreparedStatementCreator(sql, params);
		ResultSetExtractor rsExtracter = new BasicResultSetExtractor(colCount);

		return (Collection) jt.query(psCreator, rsExtracter);

	}

	/**
	 * @return Returns the dataSource.
	 */
	public DataSource getDataSource() {
		return dataSource;
	}

	/**
	 * @param dataSource
	 *            The dataSource to set.
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Properties getSelectSql() {
		return selectSql;
	}

	public void setSelectSql(Properties selectSql) {
		this.selectSql = selectSql;
	}

}
