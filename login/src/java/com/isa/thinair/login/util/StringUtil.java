package com.isa.thinair.login.util;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.crypto.api.service.CryptoServiceBD;
import com.isa.thinair.login.Constants;
import com.isa.thinair.login.api.service.LoginModuleUtils;

public class StringUtil {

	/**
	 * encrypts a given string
	 * 
	 * @param str
	 * @return
	 * @throws ModuleRuntimeException
	 */
	public static String encrypt(String str) throws ModuleRuntimeException {
		CryptoServiceBD cryptoServiceBD = (CryptoServiceBD) LoginModuleUtils.getCryptoServiceBD();

		String encryptedStr = null;
		try {
			encryptedStr = cryptoServiceBD.encrypt(str);
		} catch (ModuleException e) {
			throw new ModuleRuntimeException(e, e.getExceptionCode(), Constants.MODULE_CODE);
		}

		return encryptedStr;
	}

	/**
	 * decrypts a given string
	 * 
	 * @param encryptedStr
	 * @return
	 * @throws ModuleRuntimeException
	 */
	public static String decrypt(String encryptedStr) throws ModuleRuntimeException {
		CryptoServiceBD cryptoServiceBD = (CryptoServiceBD) LoginModuleUtils.getCryptoServiceBD();

		try {
			return cryptoServiceBD.decrypt(encryptedStr);
		} catch (ModuleException e) {
			throw new ModuleRuntimeException(e, e.getExceptionCode(), Constants.MODULE_CODE);
		}
	}

}
