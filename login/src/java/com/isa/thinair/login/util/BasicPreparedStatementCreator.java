package com.isa.thinair.login.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.jdbc.core.PreparedStatementCreator;

public class BasicPreparedStatementCreator implements PreparedStatementCreator {

	private String sql;

	private String[] params;

	public BasicPreparedStatementCreator(String sql, String[] params) {
		this.sql = sql;
		this.params = params;
	}

	public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(sql);

		for (int i = 0; i < params.length; i++) {
			ps.setString(i + 1, params[i]);
		}

		return ps;
	}

}
