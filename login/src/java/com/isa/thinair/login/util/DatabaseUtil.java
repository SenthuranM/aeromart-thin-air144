package com.isa.thinair.login.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserDST;
import com.isa.thinair.login.Constants;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class DatabaseUtil {

	/**
	 * 
	 * @param paramString
	 * @param params
	 * @return
	 * @throws ModuleException
	 */
	private final static String getValue(String paramString, String[] params) throws ModuleException {
		int colCount = 1;
		Collection col = null;
		String value = null;
		col = LookupUtils.LookupDAO().getList(paramString, params, colCount);
		Iterator iter = col.iterator();

		if (iter.hasNext()) {
			Object[] s = (Object[]) iter.next();
			value = s[0].toString();
		}

		return value;
	}

	private final static Collection getValues(String paramString, String[] params) throws ModuleException {
		int colCount = 1;
		Collection col = null;
		String value = null;
		return LookupUtils.LookupDAO().getList(paramString, params, colCount);
	}

	/**
	 * 
	 * @param paramString
	 * @param params
	 * @return
	 * @throws ModuleException
	 */
	private final static Map getMap(String paramString, String[] params) throws ModuleException {
		int colCount = 2;
		Collection col = null;
		Map map = new HashMap();
		col = LookupUtils.LookupDAO().getList(paramString, params, colCount);
		Iterator iter = col.iterator();

		while (iter.hasNext()) {
			Object[] s = (Object[]) iter.next();
			map.put(s[0].toString(), s[1].toString());
		}

		return map;
	}

	/**
	 * Returns the customer id
	 * 
	 * @param customerEmailId
	 * @return
	 * @throws ModuleException
	 */
	public final static int getCustomerId(String customerEmailId) throws ModuleException {
		String customerId = getValue("customerId", new String[] { customerEmailId });
		return Integer.parseInt(customerId);
	}

	/**
	 * Return Agent Local time
	 * 
	 * @param agentStation
	 * @return
	 */
	public final static Collection getAgentLocalTime(String agentStation) {
		Collection colRecords = LookupUtils.LookupDAO().getList("agentLocalTimes", new String[] { agentStation }, 6);
		if (colRecords != null) {
			colRecords = getDstsForAgentStation(colRecords, agentStation);
		}

		Iterator itColRecords = colRecords.iterator();
		Collection colUserDST = new ArrayList();
		UserDST userDST;
		Object[] values;

		while (itColRecords.hasNext()) {
			values = (Object[]) itColRecords.next();

			userDST = new UserDST();

			// Setting the gmt offset with the sign
			if (values[0] != null) {
				if (PlatformUtiltiies.nullHandler(values[0]).equals(Constants.GMT_OFFSET_ACTION_MINUS)) {
					userDST.setGmtOffsetMinutes((new Integer(PlatformUtiltiies.nullHandler(values[1])).intValue() * -1));
				} else {
					userDST.setGmtOffsetMinutes((new Integer(PlatformUtiltiies.nullHandler(values[1])).intValue() * 1));
				}
			}

			// Setting the dst start date time
			if (values[2] != null) {
				userDST.setDstStartDateTime((Date) values[2]);
			}

			// Setting the dst end date time
			if (values[3] != null) {
				userDST.setDstEndDateTime((Date) values[3]);
			}

			// Setting the adjust mintues
			if (values[4] != null) {
				userDST.setAdjustMinutes(new Integer(PlatformUtiltiies.nullHandler(values[4])).intValue());
			}

			colUserDST.add(userDST);
		}

		return colUserDST;
	}

	/**
	 * Return Agent information 0 - Agent Type, 1 - Agent Station Code, 2 - Agent Currency Code
	 * 
	 * @param agentStation
	 * @return
	 */
	public final static String[] getAgentInfo(String agentCode) {
		Collection colRecords = LookupUtils.LookupDAO().getList("agentInfo", new String[] { agentCode }, 3);
		Iterator itColRecords = colRecords.iterator();
		String currencyCode = null;
		String agentType = null;
		String stationCode = null;
		Object[] values;

		if (itColRecords.hasNext()) {
			values = (Object[]) itColRecords.next();

			agentType = PlatformUtiltiies.nullHandler(values[0]);
			stationCode = PlatformUtiltiies.nullHandler(values[1]);
			currencyCode = PlatformUtiltiies.nullHandler(values[2]);
		}

		return new String[] { agentType, stationCode, currencyCode };
	}

	/**
	 * gets the agent code
	 * 
	 * @param userId
	 * @return
	 * @throws ModuleException
	 */
	public final static String getAgentCode(String userId) throws ModuleException {
		return getValue("agentCode", new String[] { userId });
	}

	public static Collection getCarrierCodes(String userId) throws ModuleException {
		Collection carrierCodesToReturn = new ArrayList();
		Collection carrierCodes = getValues("userCarrierCodes", new String[] { userId });
		if (carrierCodes == null || carrierCodes.size() < 1) {
			throw new ModuleException("module.user.carrier");
		} else {
			Iterator iter = carrierCodes.iterator();
			while (iter.hasNext()) {
				Object[] o = (Object[]) iter.next();
				carrierCodesToReturn.add(o[0]);
			}
		}
		return carrierCodesToReturn;
	}

	public static String getDefaultCarrierCode(String userId) throws ModuleException {
		return getValue("userDefaultCarrierCode", new String[] { userId });
	}

	public static String getAirlineIdentifierCode(String userId) throws ModuleException {
		return getValue("airlineIdentifierCode", new String[] { userId });
	}

	public static boolean isCurrencyCodeExists(String MarkettingCurrencyCode) {

		Collection colRecords = LookupUtils.LookupDAO().getList("CurrencyCode", new String[] { MarkettingCurrencyCode }, 1);
		if (colRecords == null || colRecords.size() < 1)
			return false;
		else
			return true;

	}

	/**
	 * This method will return DSTs linked to airport with the same name as agent station. Eg. Station CMB, it will
	 * check return CMB airport DSTs. If not found will return all.
	 */
	private static Collection getDstsForAgentStation(Collection allDsts, String agentStation) {
		Collection agentDstList = new ArrayList();
		for (Iterator iterator = allDsts.iterator(); iterator.hasNext();) {
			Object[] value = (Object[]) iterator.next();
			if (agentStation.equals(PlatformUtiltiies.nullHandler(value[5]))) {
				agentDstList.add(value);
			}
		}
		if (agentDstList.size() != 0)
			return agentDstList;
		else
			return allDsts;
	}
}