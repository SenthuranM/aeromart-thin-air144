/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * ===============================================================================
 */

package com.isa.thinair.login.util;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.security.SecurityAssociation;
import org.jboss.security.auth.callback.SecurityAssociationHandler;

import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.login.Constants;

/**
 * Main Login force utility
 * 
 * @author Nilindra Fernando
 * @since 1.0
 */
public class ForceLoginInvoker {

	private static Log log = LogFactory.getLog(ForceLoginInvoker.class);

	private static Map<String, List<String>> accessControlList;

	private static List<String> aclEnabledChannelsList;

	private static ACLCache aclCache = null;

	/** Holds the login types */
	public static enum LOGIN_TYPES {

		AGENT_LOGIN_TYPE("AGENT LOGIN"), IBE_LOGIN_TYPE("IBE LOGIN"), KIOSK_LOGIN_TYPE("KIOSK LOGIN"), WS_LOGIN_TYPE(
				"WS LOGIN"), XBE_LOGIN_TYPE("XBE LOGIN"), HAHNAIR_LOGIN_TYPE("HahnAir loging");

		private String loginTypeDescription;

		private LOGIN_TYPES(String loginTypeDescription) {
			this.loginTypeDescription = loginTypeDescription;
		}

		/**
		 * @return the loginTypeDescription
		 */
		public String getLoginTypeDescription() {
			return loginTypeDescription;
		}

	};

	/**
	 * Invoke the login process accepting to throw the LoginException
	 * 
	 * @param userName
	 * @param password
	 * @throws LoginException
	 */
	private static void invokeLoginThrowingLoginException(String userName, String password) throws LoginException {
		SecurityAssociationHandler handler = new SecurityAssociationHandler();
		Principal principal = (UserPrincipal) UserPrincipal.createIdentity(userName);
		handler.setSecurityInfo(principal, password.toCharArray());
		LoginContext loginContext = new LoginContext(Constants.ISA_CLIENT, (CallbackHandler) handler);
		loginContext.login();
	}

	/**
	 * Invoke Login Process
	 * 
	 * @param userName
	 * @param password
	 */
	private static void invokeLoginProcess(String userName, String password) {
		try {
			invokeLoginThrowingLoginException(userName, password);
		} catch (LoginException e) {
			log.error(e);
		}
	}

	/**
	 * Standard login process
	 * 
	 * @param userName
	 * @param password
	 */
	public static void login(String userName, String password) {
		invokeLoginProcess(userName, password);
	}

	/**
	 * Default login process
	 */
	public static void defaultLogin() {
		invokeLoginProcess(Constants.WEB_USER_ID, Constants.WEB_USER_ID);
	}

	/**
	 * Invoke Webservice login
	 * 
	 * @param userName
	 * @param password
	 * @param salesChannelId
	 * @throws LoginException
	 */
	public static void webserviceLogin(String userName, String password, int salesChannelId) throws LoginException {

		if (isAccessAllowed(userName, Integer.toString(salesChannelId))) {
			invokeLoginThrowingLoginException(userName + Constants.WS_LOGIN + salesChannelId, password);
		} else {
			log.error("User ID -[" + userName + "] is not in the allowed list of users for accessing channel -" + salesChannelId);
			throw new LoginException("Access Denied!!!");
		}
	}

	/**
	 * Kiosk Login
	 * 
	 * @param userName
	 * @param userPassword
	 * @param customerName
	 * @param customerPassword
	 */
	public static void kioskLogin(String userName, String userPassword, String customerName, String customerPassword) {
		invokeLoginProcess(userName + Constants.KIOSK_LOGIN + customerName,
				userPassword + Constants.KIOSK_PASS + customerPassword);
	}

	public static void hahnAirLogin(String userName, String userPassword) {
		invokeLoginProcess(userName + Constants.HAHNAIR_LOGIN, userPassword);
	}

	/**
	 * Kiosk Default Login
	 * 
	 * @param userName
	 * @param password
	 */
	public static void kioskDefaultLogin(String userName, String password) {
		invokeLoginProcess(userName + Constants.KIOSK_LOGIN + Constants.WEB_USER_ID,
				password + Constants.KIOSK_PASS + Constants.WEB_USER_ID);
	}

	public static void close() {
		SecurityAssociation.clear();
	}

	public static boolean isAccessAllowed(String userName, String salesChannelId) {

		boolean accessAllowed = false;

		if (aclCache == null) {
			aclCache = new ACLCache();
		}

		if (!aclCache.isAccessAllowed(userName, salesChannelId)) {
			ACLCache aclCacheTmp = new ACLCache();
			if (aclCacheTmp.isAccessAllowed(userName, salesChannelId)) {
				accessAllowed = true;
				aclCache = aclCacheTmp;
			}
		} else {
			accessAllowed = true;
		}

		return accessAllowed;

	}

}

class ACLCache {

	private static Map<String, List<String>> accessControlList;

	private static List<String> aclEnabledChannelsList;

	public ACLCache() {
		accessControlList = getChannelWiseUserACL();
		aclEnabledChannelsList = getACLChannelList();
	}

	public boolean isAccessAllowed(String userName, String salesChannelId) {
		boolean accessAllowed = false;

		if (aclEnabledChannelsList != null && aclEnabledChannelsList.contains(salesChannelId)) {
			if (accessControlList != null && accessControlList.get(salesChannelId) != null
					&& accessControlList.get(salesChannelId).contains(userName)) {
				accessAllowed = true;
			}
		} else {
			accessAllowed = true;
		}

		return accessAllowed;
	}

	private List<String> getACLChannelList() {
		List<String> aclEnabledChannelsList = new ArrayList<String>();
		Collection channelRecords = LookupUtils.LookupDAO().getList("aclEnabledChannels", new String[] {}, 1);
		for (Object channelRecord : channelRecords) {
			Object[] record = (Object[]) channelRecord;
			if (record.length >= 1) {
				String value = getString(record[0]);
				if (value != null && !"".equals(value.trim())) {
					aclEnabledChannelsList.add(record[0].toString());
				}
			}
		}

		return aclEnabledChannelsList;
	}

	private String getString(Object obj) {
		if (obj != null) {
			return obj.toString();
		}
		return null;
	}

	private Map<String, List<String>> getChannelWiseUserACL() {
		Map<String, List<String>> accessControlList = new HashMap<String, List<String>>();
		Collection aclRecords = LookupUtils.LookupDAO().getList("userAcl", new String[] {}, 2);
		for (Object acl : aclRecords) {
			Object[] aclRecord = (Object[]) acl;
			if (aclRecord.length >= 2) {
				String key = getString(aclRecord[1]);
				if (key != null && !"".equals(key.trim())) {
					if (accessControlList.get(key) == null) {
						accessControlList.put(key, new ArrayList<String>());
					}
					String value = getString(aclRecord[0]);
					if (value != null && !"".equals(value.trim())) {
						accessControlList.get(key).add(value);
					}
				}
			}
		}

		return accessControlList;
	}
}
