package com.isa.thinair.login.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

public class BasicResultSetExtractor implements ResultSetExtractor {

	private int colCount;

	public BasicResultSetExtractor(int colCount) {
		this.colCount = colCount;
	}

	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

		Collection col = new ArrayList();
		while (rs.next()) {

			Object[] rec = new Object[colCount];

			for (int i = 0; i < colCount; i++) {
				rec[i] = rs.getObject(i + 1);
			}

			col.add(rec);

		}
		return col;
	}

}
