package com.isa.thinair.login.util;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestGeneral extends PlatformTestCase {

    /**
     * 
     * @throws Exception .
     * @see junit.framework.TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();
    }

    /**
     * 
     * @throws Exception .
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }



     public void testGetAgentCode() throws ModuleException {
        String agentCode = "BEY042";

        String agentStation = DatabaseUtil.getAgentStation(agentCode);

        System.out.println("agentStation:" + agentStation);

    }

}

