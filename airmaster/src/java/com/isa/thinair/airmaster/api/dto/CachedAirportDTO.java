package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;

/**
 * Lightweight airport dto which is cached by the system
 * 
 * @author malaka
 */
public class CachedAirportDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String airportName;

	private String airportCode;

	private String isSurfaceStation;

	private String gmtOffsetAction;

	private int gmtOffsetHours;

	private String operatedBy;

	private String countryCode;

	private String cityId;

	private String stateCode;

	private String connectingAirport;
	
	private boolean citySearchCode;

	/**
	 * @return the airportName
	 */
	public String getAirportName() {
		return airportName;
	}

	/**
	 * @param airportName
	 *            the airportName to set
	 */
	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}

	/**
	 * @return the airportCode
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            the airportCode to set
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return the isSurfaceStation
	 */
	public boolean isSurfaceSegment() {
		if (this.isSurfaceStation.equalsIgnoreCase("Y")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param isSurfaceStation
	 *            the isSurfaceStation to set
	 */
	public void setSurfaceStation(String isSurfaceStation) {
		this.isSurfaceStation = isSurfaceStation;
	}

	/**
	 * @return the gmtOffsetAction
	 */
	public String getGmtOffsetAction() {
		return gmtOffsetAction;
	}

	/**
	 * @param gmtOffsetAction
	 *            the gmtOffsetAction to set
	 */
	public void setGmtOffsetAction(String gmtOffsetAction) {
		this.gmtOffsetAction = gmtOffsetAction;
	}

	/**
	 * @return the gmtOffsetHours
	 */
	public int getGmtOffsetHours() {
		return gmtOffsetHours;
	}

	/**
	 * @param gmtOffsetHours
	 *            the gmtOffsetHours to set
	 */
	public void setGmtOffsetHours(int gmtOffsetHours) {
		this.gmtOffsetHours = gmtOffsetHours;
	}

	/**
	 * @return the operatedBy
	 */
	public String getOperatedBy() {
		return operatedBy;
	}

	/**
	 * @param operatedBy
	 *            the operatedBy to set
	 */
	public void setOperatedBy(String operatedBy) {
		this.operatedBy = operatedBy;
	}

	/**
	 * this is only returning countryCode for own airlines, have to set it later once the country wise pax configs
	 * implements for dry/interline flows
	 * 
	 * @return countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param countryCode
	 *            the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + airportCode.hashCode();
		result = 31 * result + gmtOffsetAction.hashCode();
		result = 31 * result + gmtOffsetHours;
		result = 31 * result + stateCode.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof CachedAirportDTO)) {
			return false;
		}

		CachedAirportDTO that = (CachedAirportDTO) obj;
		return (that.airportCode.equals(this.airportCode)
				&& that.gmtOffsetAction.equals(this.gmtOffsetAction)
				&& that.gmtOffsetHours == this.gmtOffsetHours
				&& that.stateCode == this.stateCode);
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getConnectingAirport() {
		return connectingAirport;
	}

	public void setConnectingAirport(String connectingAirport) {
		this.connectingAirport = connectingAirport;
	}

	public boolean isCitySearchCode() {
		return citySearchCode;
	}

	public void setCitySearchCode(boolean citySearchCode) {
		this.citySearchCode = citySearchCode;
	}
	
}
