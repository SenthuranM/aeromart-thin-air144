package com.isa.thinair.airmaster.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : Dhanya
 * 
 *         25Aug09
 * 
 * @hibernate.class table = "ML_T_MEAL_FOR_DISPLAY"
 * 
 *                  MealForDisplay is the entity class to represent the translation of meal in other language Added Meal
 *                  Title for translation
 */

public class MealForDisplay extends Persistent {

	private static final long serialVersionUID = -6635492652984180122L;

	private Integer mealForDisplayId;

	private Integer mealId;

	private String languageCode;

	private String mealDescriptionOl;

	private String mealTitleOl;

	/**
	 * returns the mealForDisplayId
	 * 
	 * @return Returns the mealForDisplayId.
	 * 
	 * @hibernate.id column = "MEAL_FOR_DISPLAY_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "ML_S_MEAL_FOR_DISPLAY"
	 * 
	 * 
	 */
	public Integer getId() {
		return mealForDisplayId;
	}

	public void setId(Integer id) {
		this.mealForDisplayId = id;
	}

	/**
	 * returns the mealId
	 * 
	 * @return Returns the mealId.
	 * 
	 * @hibernate.property column = "MEAL_ID"
	 */
	public Integer getMealId() {
		return mealId;
	}

	/**
	 * sets the mealId
	 * 
	 * @param mealId
	 *            The mealId to set.
	 */
	public void setMealId(Integer mealId) {
		this.mealId = mealId;
	}

	/**
	 * returns the mealDescriptionOl
	 * 
	 * @return Returns the mealDescriptionOl.
	 * 
	 * @hibernate.property column = "MEAL_DESCRIPTION_OL"
	 */
	public String getMealDescriptionOl() {
		return mealDescriptionOl;
	}

	/**
	 * sets the mealDescriptionOl
	 * 
	 * @param mealDescriptionOl
	 *            The mealDescriptionOl to set.
	 */
	public void setMealDescriptionOl(String mealDescriptionOl) {
		this.mealDescriptionOl = mealDescriptionOl;
	}

	/**
	 * returns the languageCode
	 * 
	 * @return Returns the languageCode.
	 * 
	 * @hibernate.property column = "LANGUAGE_CODE"
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	/**
	 * sets the languageCode
	 * 
	 * @param languageCode
	 *            The languageCode to set.
	 */
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	/**
	 * returns the mealTitleOl
	 * 
	 * @return Returns the mealTitleOl.
	 * 
	 * @hibernate.property column = "MEAL_TITLE_OL"
	 */
	public String getMealTitleOl() {
		return mealTitleOl;
	}

	/**
	 * sets the mealDescriptionOl
	 * 
	 * @param mealTitleOl
	 *            The mealTitleOl to set.
	 */
	public void setMealTitleOl(String mealTitleOl) {
		this.mealTitleOl = mealTitleOl;
	}

}
