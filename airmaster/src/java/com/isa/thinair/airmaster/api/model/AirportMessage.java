package com.isa.thinair.airmaster.api.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author eric
 * @version : Ver 1.0.0
 * @hibernate.class table = "T_AIRPORT_MSG"
 */
public class AirportMessage extends Persistent {

	private static final long serialVersionUID = 1L;

	private Long messageId;
	private String airport;
	private String depArrType;
	private Integer salesChannelCode;
	private String messageType;
	private Date displayFrom;
	private Date displayTo;
	private Date validFrom;
	private Date validTo;
	private String message;
	private Date createdDate;
	private String createdBy;
	private Date modifiedDate;
	private String modifiedBy;

	private I18nMessageKey i18nMessageKey;

	private Set<AirportMsgFlight> flights;

	private Set<AirportMsgSalesChannel> salesChannels;

	private Set<AirportMsgOND> onds;

	/**
	 * @return Returns the messageId.
	 * @hibernate.id column = "AIRPORT_MSG_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AIRPORT_MSG"
	 */
	public Long getMessageId() {
		return messageId;
	}

	/**
	 * @param messageId
	 */
	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	/**
	 * @return airport
	 * @hibernate.property column = "AIRPORT_CODE"
	 */
	public String getAirport() {
		return airport;
	}

	/**
	 * @param airport
	 */
	public void setAirport(String airport) {
		this.airport = airport;
	}

	/**
	 * @return
	 * @hibernate.property column = "DEP_ARR"
	 */
	public String getDepArrType() {
		return depArrType;
	}

	/**
	 * @param depArrType
	 */
	public void setDepArrType(String depArrType) {
		this.depArrType = depArrType;
	}

	/**
	 * @return salesChannelCode.
	 * @hibernate.property column = "SALES_CHANNEL_CODE"
	 */
	public Integer getSalesChannelCode() {
		return salesChannelCode;
	}

	/**
	 * @param salesChannelCode
	 */
	public void setSalesChannelCode(Integer salesChannelCode) {
		this.salesChannelCode = salesChannelCode;
	}

	/**
	 * @return
	 * @hibernate.property column = "STAGE"
	 */
	public String getMessageType() {
		return messageType;
	}

	/**
	 * @param messageType
	 */
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	/**
	 * @return
	 * @hibernate.property column = "TIMESTAMP_FROM"
	 */
	public Date getDisplayFrom() {
		return displayFrom;
	}

	/**
	 * @param displayFrom
	 */
	public void setDisplayFrom(Date displayFrom) {
		this.displayFrom = displayFrom;
	}

	/**
	 * @return
	 * @hibernate.property column = "TIMESTAMP_TO"
	 */
	public Date getDisplayTo() {
		return displayTo;
	}

	/**
	 * @param dispalyTo
	 * 
	 */
	public void setDisplayTo(Date displayTo) {
		this.displayTo = displayTo;
	}

	/**
	 * @return
	 * @hibernate.property column = "TRAVEL_VALIDITY_FROM"
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return
	 * @hibernate.property column = "TRAVEL_VALIDITY_TO"
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return
	 * @hibernate.property column = "MSG_TEXT"
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return
	 * @hibernate.property column = "CREATED_DATE"
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return
	 * @hibernate.property column = "CREATED_BY"
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return
	 * @hibernate.property column = "MODIFIED_DATE"
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}

	/**
	 * @param modifiedDate
	 * 
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 * @return
	 * @hibernate.property column = "MODIFIED_BY"
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return Returns the i18nMessageKey.
	 * @hibernate.many-to-one lazy="false" column = "I18N_MESSAGE_KEY"
	 *                        class="com.isa.thinair.airmaster.api.model.I18nMessageKey" not-found="ignore"
	 *                        cascade="save-update"
	 * 
	 */
	public I18nMessageKey getI18nMessageKey() {
		return i18nMessageKey;
	}

	/**
	 * @param i18nMessageKey
	 */
	public void setI18nMessageKey(I18nMessageKey i18nMessageKey) {
		this.i18nMessageKey = i18nMessageKey;
	}

	/**
	 * @return the flight ids related to airport message
	 * @hibernate.set lazy="false" inverse="true" cascade="all-delete-orphan" table="T_AIRPORT_MSG_FLIGHT"
	 * @hibernate.collection-key column="AIRPORT_MSG_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airmaster.api.model.AirportMsgFlight"
	 */

	public Set<AirportMsgFlight> getFlights() {
		if (flights == null) {
			flights = new HashSet<AirportMsgFlight>();
		}
		return flights;
	}

	/**
	 * @param flights
	 */
	public void setFlights(Set<AirportMsgFlight> flights) {
		this.flights = flights;
	}

	/**
	 * @return the sales channels related to airport message
	 * @hibernate.set lazy="false" inverse="true" cascade="all-delete-orphan" table="T_AIRPORT_MSG_SALES_CHANNEL"
	 * @hibernate.collection-key column="AIRPORT_MSG_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airmaster.api.model.AirportMsgSalesChannel"
	 */
	public Set<AirportMsgSalesChannel> getSalesChannels() {
		if (salesChannels == null) {
			salesChannels = new HashSet<AirportMsgSalesChannel>();
		}
		return salesChannels;
	}

	/**
	 * @param salesChannels
	 */
	public void setSalesChannels(Set<AirportMsgSalesChannel> salesChannels) {
		this.salesChannels = salesChannels;
	}

	/**
	 * @return the OND s related to airport message
	 * @hibernate.set lazy="false" inverse="true" cascade="all-delete-orphan" table="T_AIRPORT_MSG_OND"
	 * @hibernate.collection-key column="AIRPORT_MSG_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airmaster.api.model.AirportMsgOND"
	 */
	public Set<AirportMsgOND> getOnds() {
		if (onds == null) {
			onds = new HashSet<AirportMsgOND>();
		}
		return onds;
	}

	/**
	 * @param onds
	 */
	public void setOnds(Set<AirportMsgOND> onds) {
		this.onds = onds;
	}

	/**
	 * @param airportMsgFlight
	 */
	public void addFlightNumber(AirportMsgFlight airportMsgFlight) {
		if (this.getFlights() == null) {
			this.setFlights(new HashSet<AirportMsgFlight>());
		}

		airportMsgFlight.setAirportMessage(this);
		this.getFlights().add(airportMsgFlight);
	}

	/**
	 * @param airportMsgFlight
	 */
	public void removeFlightNumber(AirportMsgFlight airportMsgFlight) {
		if (this.getFlights() != null) {
			airportMsgFlight.setAirportMessage(null);
			this.getFlights().remove(airportMsgFlight);
		}
	}

	/**
	 * @param airportMsgOND
	 */
	public void addOND(AirportMsgOND airportMsgOND) {
		if (this.getOnds() == null) {
			this.setOnds(new HashSet<AirportMsgOND>());
		}

		airportMsgOND.setAirportMessage(this);
		this.getOnds().add(airportMsgOND);
	}

	/**
	 * @param airportMsgOND
	 */
	public void removeOnd(AirportMsgOND airportMsgOND) {
		if (this.getOnds() != null) {
			airportMsgOND.setAirportMessage(null);
			this.getOnds().remove(airportMsgOND);
		}
	}

	/**
	 * @param airportMsgSalesChannel
	 */
	public void addSalesChannel(AirportMsgSalesChannel airportMsgSalesChannel) {
		if (this.getSalesChannels() == null) {
			this.setSalesChannels(new HashSet<AirportMsgSalesChannel>());
		}

		airportMsgSalesChannel.setAirportMessage(this);
		this.getSalesChannels().add(airportMsgSalesChannel);
	}

	/**
	 * @param airportMsgSalesChannel
	 */
	public void removeSalesChannel(AirportMsgSalesChannel airportMsgSalesChannel) {
		if (this.getSalesChannels() != null) {
			airportMsgSalesChannel.setAirportMessage(null);
			this.getSalesChannels().remove(airportMsgSalesChannel);
		}
	}

}
