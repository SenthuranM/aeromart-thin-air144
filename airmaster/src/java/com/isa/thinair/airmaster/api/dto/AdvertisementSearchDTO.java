package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;

public class AdvertisementSearchDTO implements Serializable {

	private static final long serialVersionUID = -8875126064321449326L;

	private String origin;
	private String destination;
	private String title;
	private String language;

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

}
