package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;

public class StationDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String stationCode;
	
	private String stateCode;
	
	private String countryCode;

	public String getStationCode() {
		return stationCode;
	}

	public String getStateCode() {
		return stateCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
}
