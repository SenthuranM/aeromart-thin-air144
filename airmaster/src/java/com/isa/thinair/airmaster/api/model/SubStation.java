package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;

/**
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_SUB_STATION"
 * 
 *                  SubStation is the entity class to represent a sub station model
 * 
 */
public class SubStation implements Serializable {
	private static final long serialVersionUID = 1L;

	private int subStationId;

	private String airportCode;

	private String description;

	private String shortName;

	private int sequence;

	private String status;

	/**
	 * returns the subStationId
	 * 
	 * @return Returns the subStationId.
	 * 
	 * @hibernate.id column = "SUB_STATION_ID" generator-class = "assigned"
	 */
	public int getSubStationId() {
		return subStationId;
	}

	/**
	 * sets the subStationId
	 * 
	 * @param subStationId
	 *            The subStationId to set.
	 */
	public void setSubStationId(int subStationId) {
		this.subStationId = subStationId;
	}

	/**
	 * returns the airportCode
	 * 
	 * @return Returns the airportCode.
	 * 
	 * @hibernate.property column = "AIRPORT_CODE"
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * sets the airportCode
	 * 
	 * @param airportCode
	 *            The airportCode to set.
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * returns the description
	 * 
	 * @return Returns the description.
	 * 
	 * @hibernate.property column = "SUB_STATION_DESC"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * sets the description
	 * 
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * returns the shortName
	 * 
	 * @return Returns the shortName.
	 * 
	 * @hibernate.property column = "SUB_STATION_SHORTNAME"
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * sets the shortName
	 * 
	 * @param shortName
	 *            The shortName to set.
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * returns the sequence
	 * 
	 * @return Returns the sequence.
	 * 
	 * @hibernate.property column = "STATION_SEQUENCE"
	 */
	public int getSequence() {
		return sequence;
	}

	/**
	 * sets the sequence
	 * 
	 * @param sequence
	 *            The sequence to set.
	 */
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}
