package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;
import java.util.Set;

/**
 * 
 * @author : M.Rikaz
 * 
 * @hibernate.class table = "T_SSR_SUB_CATEGORY" lazy="false"
 */

public class SSRSubCategory implements Serializable {

	public static final Integer ID_STANDARD = new Integer("0");
	public static final Integer ID_MASS = new Integer("1");
	public static final Integer ID_LOUNGE = new Integer("2");
	public static final Integer ID_AIRPORT_CABS = new Integer("8");

	private static final long serialVersionUID = 8718956196375999768L;

	private Integer subCatId;

	private String description;

	private SSRCategory category;

	private Set<SSR> ssr;

	private String adminVisibility;

	/**
	 * @return the status
	 * @hibernate.property column = "ADMIN_VISIBILITY"
	 */
	public String getAdminVisibility() {
		return adminVisibility;
	}

	public void setAdminVisibility(String adminVisibility) {
		this.adminVisibility = adminVisibility;
	}

	/**
	 * @return Returns the passengers.
	 * @hibernate.set lazy="false" cascade="all" inverse="true"
	 * @hibernate.collection-key column="SSR_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airmaster.api.model.SSR"
	 */
	public Set<SSR> getSsr() {
		return ssr;
	}

	public void setSsr(Set<SSR> ssr) {
		this.ssr = ssr;
	}

	private String status;

	private Integer modCutoverMins;

	private String sendInPnlAdl;

	private String sendInEmail;

	private Integer emailSendAttempts;

	private Integer emailStartCutoverMins;

	private Integer emailStopCutoverMins;

	private String emailAddress;

	/**
	 * @return Returns the subCatId.
	 * @hibernate.id column = "SSR_SUB_CAT_ID" generator-class = "native"
	 */
	public Integer getSubCatId() {
		return subCatId;
	}

	public void setSubCatId(Integer subCatId) {
		this.subCatId = subCatId;
	}

	/**
	 * @return the description
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "BOOK_MOD_CUTOVER_IN_MINS"
	 */
	public Integer getModCutoverMins() {
		return modCutoverMins;
	}

	public void setModCutoverMins(Integer modCutoverMins) {
		this.modCutoverMins = modCutoverMins;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "SEND_IN_PNL_ADL"
	 */
	public String getSendInPnlAdl() {
		return sendInPnlAdl;
	}

	public void setSendInPnlAdl(String sendInPnlAdl) {
		this.sendInPnlAdl = sendInPnlAdl;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "SEND_IN_EMAIL"
	 */
	public String getSendInEmail() {
		return sendInEmail;
	}

	public void setSendInEmail(String sendInEmail) {
		this.sendInEmail = sendInEmail;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "EMAIL_SEND_ATTEMPTS"
	 */
	public Integer getEmailSendAttempts() {
		return emailSendAttempts;
	}

	public void setEmailSendAttempts(Integer emailSendAttempts) {
		this.emailSendAttempts = emailSendAttempts;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "EMAIL_SEND_START_CUTOVER_MINS"
	 */
	public Integer getEmailStartCutoverMins() {
		return emailStartCutoverMins;
	}

	public void setEmailStartCutoverMins(Integer emailStartCutoverMins) {
		this.emailStartCutoverMins = emailStartCutoverMins;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "EMAIL_SEND_STOP_CUTOVER_MINS"
	 */
	public Integer getEmailStopCutoverMins() {
		return emailStopCutoverMins;
	}

	public void setEmailStopCutoverMins(Integer emailStopCutoverMins) {
		this.emailStopCutoverMins = emailStopCutoverMins;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "EMAIL_ADDRESSES"
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return Returns the category.
	 * @hibernate.many-to-one column = "SSR_CAT_ID" class= "com.isa.thinair.airmaster.api.model.SSRCategory"
	 */
	public SSRCategory getCategory() {
		return category;
	}

	public void setCategory(SSRCategory category) {
		this.category = category;
	}

}
