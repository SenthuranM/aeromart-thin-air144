package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;

/**
 * 
 * @author : M.Rikaz
 * @hibernate.class table = "T_SSR_APPLICABLE_AIRPORT"
 */

public class SSRApplicableAirports implements Serializable {

	private static final long serialVersionUID = 8718956196375999768L;

	private Integer appAirportId;

	private SSRConstraints appConstraints;

	/**
	 * @hibernate.many-to-one column="SSR_APP_CON_ID" class="com.isa.thinair.airmaster.api.model.SSRConstraints"
	 * @return Returns the ssrInfo.
	 */
	public SSRConstraints getAppConstraints() {
		return appConstraints;
	}

	public void setAppConstraints(SSRConstraints appConstraints) {
		this.appConstraints = appConstraints;
	}

	private String airportCode;

	/**
	 * @return Returns the appAirportId.
	 * 
	 * @hibernate.id column="SSR_APPLICABLE_AIRPORT_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_SSR_APPLICABLE_AIRPORT"
	 */
	public Integer getAppAirportId() {
		return appAirportId;
	}

	public void setAppAirportId(Integer appAirportId) {
		this.appAirportId = appAirportId;
	}

	/**
	 * @return the airportCode
	 * @hibernate.property column = "AIRPORT_CODE"
	 */
	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

}
