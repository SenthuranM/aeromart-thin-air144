package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author rumesh
 * 
 */

public class AirportServiceDTO implements Serializable {

	public static final String SEPERATOR = "$";

	public static final String APPLICABILITY_TYPE_PAX = "P";
	public static final String APPLICABILITY_TYPE_RESERVATION = "R";

	private static final String SSR_IMAGE = "ssr_";
	private static final String SSR_IMAGE_THUMB = "ssr_thumbnail_";
	private static final String IMAGE_FORMAT = ".jpg";

	private static final long serialVersionUID = 5576610463367267076L;

	private Long ssrID;
	private Long ssrChargeID;
	private String ssrCode;
	private String ssrName;
	private String description;
	private String airport;
	private String applicabilityType;
	private String adultAmount;
	private String childAmount;
	private String infantAmount;
	private String reservationAmount;
	private Date effectiveFrom;
	private Date effectiveTo;
	private Integer availableQty;
	private String decriptionSelectedLanguage;
	private String status;

	// used for list airport services in WS - [D-Departue,A-Arrival,T-Transit]
	private String applyOn;
	private String ssrImagePath;
	private String ssrThumbnailImagePath;

	private ServiceProviderDTO provider;

	/**
	 * @return the ssrID
	 */
	public Long getSsrID() {
		return ssrID;
	}

	/**
	 * @param ssrID
	 *            the ssrID to set
	 */
	public void setSsrID(Long ssrID) {
		this.ssrID = ssrID;
	}

	/**
	 * @return the ssrChargeID
	 */
	public Long getSsrChargeID() {
		return ssrChargeID;
	}

	/**
	 * @param ssrChargeID
	 *            the ssrChargeID to set
	 */
	public void setSsrChargeID(Long ssrChargeID) {
		this.ssrChargeID = ssrChargeID;
	}

	/**
	 * @return the ssrCode
	 */
	public String getSsrCode() {
		return ssrCode;
	}

	/**
	 * @param ssrCode
	 *            the ssrCode to set
	 */
	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	/**
	 * @return the ssrName
	 */
	public String getSsrName() {
		return ssrName;
	}

	/**
	 * @param ssrName
	 *            the ssrName to set
	 */
	public void setSsrName(String ssrName) {
		this.ssrName = ssrName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the airport
	 */
	public String getAirport() {
		return airport;
	}

	/**
	 * @param airport
	 *            the airport to set
	 */
	public void setAirport(String airport) {
		this.airport = airport;
	}

	/**
	 * @return the applicabilityType
	 */
	public String getApplicabilityType() {
		return applicabilityType;
	}

	/**
	 * @param applicabilityType
	 *            the applicabilityType to set
	 */
	public void setApplicabilityType(String applicabilityType) {
		this.applicabilityType = applicabilityType;
	}

	/**
	 * @return the adultAmount
	 */
	public String getAdultAmount() {
		return adultAmount;
	}

	/**
	 * @param adultAmount
	 *            the adultAmount to set
	 */
	public void setAdultAmount(String adultAmount) {
		this.adultAmount = adultAmount;
	}

	/**
	 * @return the childAmount
	 */
	public String getChildAmount() {
		return childAmount;
	}

	/**
	 * @param childAmount
	 *            the childAmount to set
	 */
	public void setChildAmount(String childAmount) {
		this.childAmount = childAmount;
	}

	/**
	 * @return the infantAmount
	 */
	public String getInfantAmount() {
		return infantAmount;
	}

	/**
	 * @param infantAmount
	 *            the infantAmount to set
	 */
	public void setInfantAmount(String infantAmount) {
		this.infantAmount = infantAmount;
	}

	/**
	 * @return the reservationAmount
	 */
	public String getReservationAmount() {
		return reservationAmount;
	}

	/**
	 * @param reservationAmount
	 *            the reservationAmount to set
	 */
	public void setReservationAmount(String reservationAmount) {
		this.reservationAmount = reservationAmount;
	}

	/**
	 * @return the effectiveFrom
	 */
	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	/**
	 * @param effectiveFrom
	 *            the effectiveFrom to set
	 */
	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	/**
	 * @return the effectiveTo
	 */
	public Date getEffectiveTo() {
		return effectiveTo;
	}

	/**
	 * @param effectiveTo
	 *            the effectiveTo to set
	 */
	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	/**
	 * @return the availableQty
	 */
	public Integer getAvailableQty() {
		return availableQty;
	}

	/**
	 * @param availableQty
	 *            the availableQty to set
	 */
	public void setAvailableQty(Integer availableQty) {
		this.availableQty = availableQty;
	}

	/**
	 * @return the decriptionSelectedLanguage
	 */
	public String getDecriptionSelectedLanguage() {
		return decriptionSelectedLanguage;
	}

	/**
	 * @param decriptionSelectedLanguage
	 *            the decriptionSelectedLanguage to set
	 */
	public void setDecriptionSelectedLanguage(String decriptionSelectedLanguage) {
		this.decriptionSelectedLanguage = decriptionSelectedLanguage;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the applyOn
	 */
	public String getApplyOn() {
		return applyOn;
	}

	/**
	 * @param applyOn
	 *            the applyOn to set
	 */
	public void setApplyOn(String applyOn) {
		this.applyOn = applyOn;
	}

	/**
	 * @return the ssrImagePath
	 */
	public String getSsrImagePath() {
		return ssrImagePath;
	}

	/**
	 * @param ssrImagePath
	 *            the ssrImagePath to set
	 */
	public void setSsrImagePath(String ssrImagePath) {
		this.ssrImagePath = ssrImagePath;
	}

	/**
	 * @return the ssrThumbnailImagePath
	 */
	public String getSsrThumbnailImagePath() {
		return ssrThumbnailImagePath;
	}

	/**
	 * @param ssrThumbnailImagePath
	 *            the ssrThumbnailImagePath to set
	 */
	public void setSsrThumbnailImagePath(String ssrThumbnailImagePath) {
		this.ssrThumbnailImagePath = ssrThumbnailImagePath;
	}

	public void setImagePaths(String url) {
		if (this.ssrCode != null && !"".equals(ssrCode)) {
			// Set thumbnail path
			StringBuilder ssrImgThumbPath = new StringBuilder(url);
			ssrImgThumbPath.append(SSR_IMAGE_THUMB);
			ssrImgThumbPath.append(this.ssrCode);
			ssrImgThumbPath.append(IMAGE_FORMAT);
			this.ssrThumbnailImagePath = ssrImgThumbPath.toString();

			// Set image path
			StringBuilder ssrImgPath = new StringBuilder(url);
			ssrImgPath.append(SSR_IMAGE);
			ssrImgPath.append(this.ssrCode);
			ssrImgPath.append(IMAGE_FORMAT);
			this.ssrImagePath = ssrImgPath.toString();
		}
	}

	public ServiceProviderDTO getProvider() {
		return provider;
	}

	public void setProvider(ServiceProviderDTO provider) {
		this.provider = provider;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ssrChargeID == null) ? 0 : ssrChargeID.hashCode());
		result = prime * result + ((ssrCode == null) ? 0 : ssrCode.hashCode());
		result = prime * result + ((ssrID == null) ? 0 : ssrID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AirportServiceDTO other = (AirportServiceDTO) obj;
		if (ssrChargeID == null) {
			if (other.ssrChargeID != null)
				return false;
		} else if (!ssrChargeID.equals(other.ssrChargeID))
			return false;
		if (ssrCode == null) {
			if (other.ssrCode != null)
				return false;
		} else if (!ssrCode.equals(other.ssrCode))
			return false;
		if (ssrID == null) {
			if (other.ssrID != null)
				return false;
		} else if (!ssrID.equals(other.ssrID))
			return false;
		return true;
	}

}
