package com.isa.thinair.airmaster.api.to;

import java.io.Serializable;

public class OperationStatusTO implements Serializable {
	private static final long serialVersionUID = -8235312141402852262L;
	private boolean emailSent = false;
	private boolean alertSent = false;

	/**
	 * @return Returns the alertSent.
	 */
	public boolean isAlertSent() {
		return alertSent;
	}

	/**
	 * @param alertSent
	 *            The alertSent to set.
	 */
	public void setAlertSent(boolean alertSent) {
		this.alertSent = alertSent;
	}

	/**
	 * @return Returns the emailSent.
	 */
	public boolean isEmailSent() {
		return emailSent;
	}

	/**
	 * @param emailSent
	 *            The emailSent to set.
	 */
	public void setEmailSent(boolean emailSent) {
		this.emailSent = emailSent;
	}

}
