/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:44
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import java.util.Set;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : Haider
 * 
 *         8Oct08
 * 
 * @hibernate.class table = "T_GDS"
 * 
 *                  Gds is the entity class to repesent a GDS information
 * 
 */
public class Gds extends Persistent {

	private static final long serialVersionUID = -656709990343286971L;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	private int gdsId;

	private String gdsCode;

	private String description;

	private Integer avsAvailThreshold;

	private Integer avsClosureThreshold;

	private String status;

	private String schedPublishMechanismCode;

	private String remarks;
	
	private char onlyANumeric;

	private char enableNavs;
	
	public static char ENABLED_Y = 'Y';

	public static char DISABLED_N = 'N';

	private boolean airlineResModifiable;

	private String typeAVersion;

	private String typeASenderId;

	private String typeASyntaxId;

	private String typeASyntaxVersionNumber;

	private String typeAControllingAgency;
	
	private String carrierCode;
	
	private boolean codeShareCarrier;
	
	private String userId;
	
	private boolean aaValidatingCarrier;
	
	private String airimpVersion;

	private int typeBSyncType;

	private String aaInterchangeId;

	private String gdsInterchangeId;
	
	private String marketingFlightPrefix;	
	
	private boolean applyGlobalOnholdTimeLimit;
	
	private boolean sendOALSegments;
	
	private String paymentType;

	//private Set<String> gdsAutoPublishCarriers;
	
	private Set<String> gdsAutoPublishRoutes;
	
	private char enableRouteWiseAutoPublish;
	
	private boolean attachRouteWiseDefaultAnciTemplate;
	
	private boolean schedulingSystem;
	
	private char enableAutoScheduleSplit;
	
	private boolean allowSendSSRADTK;
	
	private char useAeroMartETS;

	private String typeASenderSubId;

	private Boolean primeFlightEnabled;
	

	/**
	 * returns the gdsId
	 * 
	 * @return Returns the gdsId.
	 * 
	 * @hibernate.id column = "GDS_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_GDS"
	 * 
	 * 
	 */
	public int getGdsId() {
		return gdsId;
	}

	public void setGdsId(int gdsId) {
		this.gdsId = gdsId;
	}

	/**
	 * returns the GDS Code
	 * 
	 * @return Returns the GDS code.
	 * 
	 * @hibernate.property column = "GDS_CODE"
	 */
	public String getGdsCode() {
		return gdsCode;
	}

	public void setGdsCode(String gdsCode) {
		this.gdsCode = gdsCode;
	}

	/**
	 * returns the GDS Description
	 * 
	 * @return Returns description.
	 * 
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * returns the AVS available threshold
	 * 
	 * @return Returns avsAvailThreshold.
	 * 
	 * @hibernate.property column = "AVS_AVAIL_THRESHOLD"
	 */
	public Integer getAvsAvailThreshold() {
		return avsAvailThreshold;
	}

	public void setAvsAvailThreshold(Integer avsAvailThreshold) {
		this.avsAvailThreshold = avsAvailThreshold;
	}

	/**
	 * returns AVS closure threshold
	 * 
	 * @return Returns avsClosureThreshold.
	 * 
	 * @hibernate.property column = "AVS_CLOSURE_THRESHOLD"
	 */
	public Integer getAvsClosureThreshold() {
		return avsClosureThreshold;
	}

	public void setAvsClosureThreshold(Integer avsClosureThreshold) {
		this.avsClosureThreshold = avsClosureThreshold;
	}

	/**
	 * returns GDS status
	 * 
	 * @return Returns status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * returns GDS schedule publish mechanim code
	 * 
	 * @return Returns schedPublishMechanismCode.
	 * 
	 * @hibernate.property column = "SCHED_PUBLISH_MECHANISM_CODE"
	 */
	public String getSchedPublishMechanismCode() {
		return schedPublishMechanismCode;
	}

	public void setSchedPublishMechanismCode(String schedPublishMechanismCode) {
		this.schedPublishMechanismCode = schedPublishMechanismCode;
	}

	/**
	 * returns Remarks
	 * 
	 * @return Returns remarks .
	 * 
	 * @hibernate.property column = "REMARKS"
	 */

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	/**
	 * @return the enableNavs
	 * 
	 * @hibernate.property column = "ENABLE_NAVS"
	 */
	public char getEnableNavs() {
		return enableNavs;
	}

	/**
	 * @param enableNavs the enableNavs to set
	 */
	public void setEnableNavs(char enableNavs) {
		this.enableNavs = enableNavs;
	}
	
	public boolean checkNAVSEnabled() {
		if (ENABLED_Y == getEnableNavs())
			return true;
		else
			return false;
	}
	
	/**
	 * @return the onlyNumeric
	 * 
	 * @hibernate.property column = "ONLY_ANUMERIC"
	 */
	public char getOnlyANumeric() {
		return onlyANumeric;
	}

	public void setOnlyANumeric(char onlyANumeric) {
		this.onlyANumeric = onlyANumeric;
	}
	
	public boolean checkOnlyANumeric() {
		if (ENABLED_Y == getOnlyANumeric()) 
			return true;
		else
			return false;
	}
	
	
	
	/**
	 * @return the airlineResModifiable
	 * 
	 * @hibernate.property column = "AIRLINE_RES_MODIFIABLE" type="yes_no"
	 */
	public boolean getAirlineResModifiable() {
		return airlineResModifiable;
	}

	/**
	 * @param addAddressToMessageBody the addAddressToMessageBody to set
	 */
	public void setAirlineResModifiable(boolean airlineResModifiable) {
		this.airlineResModifiable = airlineResModifiable;
	}

	/**
	 * @hibernate.property column = "TYPE_A_VERSION"
	 */
	public String getTypeAVersion() {
		return typeAVersion;
	}

	public void setTypeAVersion(String typeAVersion) {
		this.typeAVersion = typeAVersion;
	}

	/**
	 * @hibernate.property column = "TYPE_A_SENDER_ID"
	 */
	public String getTypeASenderId() {
		return typeASenderId;
	}

	public void setTypeASenderId(String typeASenderId) {
		this.typeASenderId = typeASenderId;
	}

	/**
	 * @hibernate.property column = "TYPE_A_SYNTAX_ID"
	 */
	public String getTypeASyntaxId() {
		return typeASyntaxId;
	}

	public void setTypeASyntaxId(String typeASyntaxId) {
		this.typeASyntaxId = typeASyntaxId;
	}

	/**
	 * @hibernate.property column = "TYPE_A_SYNTAX_VERSION"
	 */
	public String getTypeASyntaxVersionNumber() {
		return typeASyntaxVersionNumber;
	}

	public void setTypeASyntaxVersionNumber(String typeASyntaxVersionNumber) {
		this.typeASyntaxVersionNumber = typeASyntaxVersionNumber;
	}

	public String getTypeAControllingAgency() {
		return typeAControllingAgency;
	}

	public void setTypeAControllingAgency(String typeAControllingAgency) {
		this.typeAControllingAgency = typeAControllingAgency;
	}

	/**
	 * @return the carrierCode
	 * 
	 * @hibernate.property column = "CARRIER_CODE"
	 */
	public String getCarrierCode() {
		return carrierCode;
	}
	
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the codeShareCarrier
	 * 
	 * @hibernate.property column = "CS_CARRIER" type="yes_no"
	 */
	public boolean getCodeShareCarrier() {
		return codeShareCarrier;
	}

	public void setCodeShareCarrier(boolean codeShareCarrier) {
		this.codeShareCarrier = codeShareCarrier;
	}

	/**
	 * @return the userId
	 * 
	 * @hibernate.property column = "USER_ID" 
	 */
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the codeShareCarrier
	 * 
	 * @hibernate.property column = "AA_VALIDATING_CARRIER" type="yes_no"
	 */
	public boolean isAaValidatingCarrier() {
		return aaValidatingCarrier;
	}

	public void setAaValidatingCarrier(boolean aaValidatingCarrier) {
		this.aaValidatingCarrier = aaValidatingCarrier;
	}

	/**
	 * @return the airimpVersion
	 * 
	 * @hibernate.property column = "AIRIMP_VER" 
	 */
	public String getAirimpVersion() {
		return airimpVersion;
	}

	public void setAirimpVersion(String airimpVersion) {
		this.airimpVersion = airimpVersion;
	}

	/**
	 * @hibernate.property column = "TYPE_B_SYNC_TYPE"
	 */
	public int getTypeBSyncType() {
		return typeBSyncType;
	}

	public void setTypeBSyncType(int typeBSyncType) {
		this.typeBSyncType = typeBSyncType;
	}

	/**
	 * @hibernate.property column = "AA_INTERCHANGE_ID"
	 */
	public String getAaInterchangeId() {
		return aaInterchangeId;
	}

	public void setAaInterchangeId(String aaInterchangeId) {
		this.aaInterchangeId = aaInterchangeId;
	}

	/**
	 * @hibernate.property column = "GDS_INTERCHANGE_ID"
	 */
	public String getGdsInterchangeId() {
		return gdsInterchangeId;
	}

	public void setGdsInterchangeId(String gdsInterchangeId) {
		this.gdsInterchangeId = gdsInterchangeId;
	}

	/**
	 * @hibernate.property column = "MARKETING_FLIGHT_PREFIX"
	 */
	public String getMarketingFlightPrefix() {
		return marketingFlightPrefix;
	}

	public void setMarketingFlightPrefix(String marketingFlightPrefix) {
		this.marketingFlightPrefix = marketingFlightPrefix;
	}

	/**
	 * @hibernate.property column = "APPLY_GLOBAL_ONHOLD_TIME_LIMIT" type="yes_no"
	 */
	public boolean isApplyGlobalOnholdTimeLimit() {

		return applyGlobalOnholdTimeLimit;
	}

	public void setApplyGlobalOnholdTimeLimit(boolean applyGlobalOnholdTimeLimit) {
		this.applyGlobalOnholdTimeLimit = applyGlobalOnholdTimeLimit;
	}

	/**
	 * @hibernate.property column = "SEND_OAL_SEGMENTS" type="yes_no"
	 */
	public boolean isSendOALSegments() {
		return sendOALSegments;
	}

	public void setSendOALSegments(boolean sendOALSegments) {
		this.sendOALSegments = sendOALSegments;
	}
	
	 
//	/**
//	 * returns the carrier codes related with GDS
//	 * 
//	 * @hibernate.set lazy="false" cascade="all" table="T_GDS_AUTO_PUBLISH_CARRIERS"
//	 * @hibernate.collection-element column="CARRIER_CODE" type="string"
//	 * @hibernate.collection-key column="GDS_ID"
//	 */
	 
//	public Set<String> getGdsAutoPublishCarriers() {
//		return gdsAutoPublishCarriers;
//	}
//
//	public void setGdsAutoPublishCarriers(Set<String> gdsAutoPublishCarriers) {
//		this.gdsAutoPublishCarriers = gdsAutoPublishCarriers;
//	}

	/**
	 * returns GDS/CS paymentType
	 * 
	 * @return Returns paymentType.
	 * 
	 * @hibernate.property column = "PAYMENT_TYPE"
	 */
	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * returns the ond codes related with GDS
	 * 
	 * @hibernate.set lazy="false" cascade="all" table="T_GDS_AUTO_PUBLISH_ROUTES"
	 * @hibernate.collection-element column="OND_CODE" type="string"
	 * @hibernate.collection-key column="GDS_ID"
	 */
	public Set<String> getGdsAutoPublishRoutes() {
		return gdsAutoPublishRoutes;
	}

	public void setGdsAutoPublishRoutes(Set<String> gdsAutoPublishRoutes) {
		this.gdsAutoPublishRoutes = gdsAutoPublishRoutes;
	}

	/**
	 * @return the enableRouteWiseAutoPublish
	 * 
	 * @hibernate.property column = "ENABLE_ROUTE_WISE_AUTO_PUBLISH"
	 */
	public char getEnableRouteWiseAutoPublish() {
		return enableRouteWiseAutoPublish;
	}

	/**
	 * @param enableRouteWiseAutoPublish
	 *            the enableRouteWiseAutoPublish to set
	 */
	public void setEnableRouteWiseAutoPublish(char enableRouteWiseAutoPublish) {
		this.enableRouteWiseAutoPublish = enableRouteWiseAutoPublish;
	}

	
	public boolean isRouteWiseAutoPublishEnabled() {
		if (ENABLED_Y == getEnableRouteWiseAutoPublish())
			return true;
		else
			return false;
	}

	/**
	 * @return the attachRouteWiseDefaultAnciTemplate
	 * @hibernate.property column = "ATTACH_DEF_ANCI_TEMPLATE" type="yes_no"
	 */
	public boolean isAttachRouteWiseDefaultAnciTemplate() {
		return attachRouteWiseDefaultAnciTemplate;
	}

	/**
	 * @param attachRouteWiseDefaultAnciTemplate the attachRouteWiseDefaultAnciTemplate to set
	 */
	public void setAttachRouteWiseDefaultAnciTemplate(boolean attachRouteWiseDefaultAnciTemplate) {
		this.attachRouteWiseDefaultAnciTemplate = attachRouteWiseDefaultAnciTemplate;
	}
	
	
	/**
	 * @return schedulingSystem
	 * 
	 * @hibernate.property column = "SCHEDULING_SYSTEM" type="yes_no"
	 */
	public boolean getSchedulingSystem() {
		return schedulingSystem;
	}

	public void setSchedulingSystem(boolean schedulingSystem) {
		this.schedulingSystem = schedulingSystem;
	}

	/**
	 * @return the enableAutoScheduleSplit
	 * @hibernate.property column = "ENABLE_SCHEDULE_AUTO_SPLIT" 
	 */
	public char getEnableAutoScheduleSplit() {
		return enableAutoScheduleSplit;
	}

	public void setEnableAutoScheduleSplit(char enableAutoScheduleSplit) {
		this.enableAutoScheduleSplit = enableAutoScheduleSplit;
	}

	public boolean checkEnableAutoScheduleSplit() {
		if (ENABLED_Y == getEnableAutoScheduleSplit())
			return true;
		else
			return false;
	}

	/**
	 * @return the useAeroMartETS
	 * @hibernate.property column = "USE_AA_ETS"
	 */
	public char getUseAeroMartETS() {
		return useAeroMartETS;
	}

	public void setUseAeroMartETS(char useAeroMartETS) {
		this.useAeroMartETS = useAeroMartETS;
	}

	public boolean isAeroMartETSEnabled() {
		if (ENABLED_Y == getUseAeroMartETS())
			return true;
		else
			return false;
	}
	
	
	/**
	 * @hibernate.property column = "TYPE_A_SENDER_SUB_ID"
	 */
	public String getTypeASenderSubId() {
		return typeASenderSubId;
	}
	
	
	public void setTypeASenderSubId(String typeASenderSubId) {
		this.typeASenderSubId = typeASenderSubId;
	}

	/**
	 * @hibernate.property column = "PRIME_FLIGHT_ENABLED" type="yes_no"
	 */
	public Boolean getPrimeFlightEnabled() {
		return primeFlightEnabled;
	}

	public void setPrimeFlightEnabled(Boolean primeFlightEnabled) {
		this.primeFlightEnabled = primeFlightEnabled;
	}
	
	
	/**
	 * @hibernate.property column = "ALLOW_SSRADTK" type="yes_no"
	 */
	public boolean isAllowSendSSRADTK() {

		return allowSendSSRADTK;
	}

	public void setAllowSendSSRADTK(boolean allowSendSSRADTK) {
		this.allowSendSSRADTK = allowSendSSRADTK;
	}
}
