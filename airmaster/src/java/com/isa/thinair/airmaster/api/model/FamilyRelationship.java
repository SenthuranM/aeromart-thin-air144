package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;

/**
 * 
 * @hibernate.class table = "T_FAMILY_RELATIONSHIP"
 * 
 */
public class FamilyRelationship implements Serializable {

	private static final long serialVersionUID = 1101614369691412370L;
	
	/** Holds the Family Relationship ID */
	private String familyRelationshipId;
	
	/** Holds the Family Relationship Description */
	private String description;
	
	/** Holds the Family Relationship Title Code */
	private String titleCode;
	
	private int minimumAgeInDays;
	
	private int maximumAgeInDays;
	
	/**
	 * 
	 * @return Returns the familyRelationshipId
	 * 
	 * @hibernate.id column = "FAMILY_RELATIONSHIP_ID" generator-class = "assigned"
	 */
	public String getFamilyRelationshipId() {
		return familyRelationshipId;
	}

	/**
	 * @param familyRelationshipId
	 *            the familyRelationshipId to set
	 */
	public void setFamilyRelationshipId(String familyRelationshipId) {
		this.familyRelationshipId = familyRelationshipId;
	}

	/**
	 * 
	 * @return the description
	 * 
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return the titleCode
	 * 
	 * @hibernate.property column = "TITLE_CODE"
	 */
	public String getTitleCode() {
		return titleCode;
	}

	/**
	 * @param titleCode
	 *            the titleCode to set
	 */
	public void setTitleCode(String titleCode) {
		this.titleCode = titleCode;
	}
	/**
	 * 
	 * @return the minimumAgeInDays
	 * 
	 * @hibernate.property column = "MINIMUM_AGE_IN_DAYS"
	 */
	public int getMinimumAgeInDays() {
		return minimumAgeInDays;
	}

	public void setMinimumAgeInDays(int minimumAgeInDays) {
		this.minimumAgeInDays = minimumAgeInDays;
	}
	/**
	 * 
	 * @return the maximumAgeInDays
	 * 
	 * @hibernate.property column = "MAXIMUM_AGE_IN_DAYS"
	 */
	public int getMaximumAgeInDays() {
		return maximumAgeInDays;
	}

	public void setMaximumAgeInDays(int maximumAgeInDays) {
		this.maximumAgeInDays = maximumAgeInDays;
	}	

}