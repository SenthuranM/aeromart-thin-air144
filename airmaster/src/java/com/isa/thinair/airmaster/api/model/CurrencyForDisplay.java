/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:44
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : Haider
 * 
 *         05Mar09
 * 
 * @hibernate.class table = "T_CURRENCY_FOR_DISPLAY"
 * 
 *                  CurrencyForDisplay is the entity class to represent the translation of currency in other language
 * 
 */
public class CurrencyForDisplay extends Persistent {

	private static final long serialVersionUID = 5062561029855427574L;

	private Integer currencyForDisplayId;

	private String currencyCode;

	private String languageCode;

	private String currencyNameOl;

	/**
	 * returns the currencyForDisplayId
	 * 
	 * @return Returns the currencyForDisplayId.
	 * 
	 * @hibernate.id column = "CURRENCY_FOR_DISPLAY_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_CURRENCY_FOR_DISPLAY"
	 * 
	 * 
	 */
	public Integer getId() {
		return currencyForDisplayId;
	}

	public void setId(Integer id) {
		this.currencyForDisplayId = id;
	}

	/**
	 * returns the currencyCode
	 * 
	 * @return Returns the currencyCode.
	 * 
	 * @hibernate.property column = "CURRENCY_CODE"
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * sets the currencyCode
	 * 
	 * @param currencyCode
	 *            The currencyCode to set.
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * returns the currencyNameOl
	 * 
	 * @return Returns the currencyNameOl.
	 * 
	 * @hibernate.property column = "CURRENCY_NAME_OL"
	 */
	public String getCurrencyNameOl() {
		return currencyNameOl;
	}

	/**
	 * sets the currencyNameOl
	 * 
	 * @param currencyNameOl
	 *            The currencyNameOl to set.
	 */
	public void setCurrencyNameOl(String currencyNameOl) {
		this.currencyNameOl = currencyNameOl;
	}

	/**
	 * returns the languageCode
	 * 
	 * @return Returns the languageCode.
	 * 
	 * @hibernate.property column = "LANGUAGE_CODE"
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	/**
	 * sets the languageCode
	 * 
	 * @param languageCode
	 *            The languageCode to set.
	 */
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
}
