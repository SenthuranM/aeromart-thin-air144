/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:28:24
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_AIRPORT" Airport is the entity class to repesent a Airport model
 */
public class Airport extends Tracking implements Serializable {

	private static final long serialVersionUID = 8635444917655337202L;

	public static final String LATITUDE_NORTH_SOUTH_N = "N";

	public static final String LATITUDE_NORTH_SOUTH_S = "S";

	public static final String LONGITUDE_EAST_WEST_E = "E";

	public static final String LONGITUDE_EAST_WEST_W = "W";

	public static final String GMT_OFFSET_ACTION_ADD = "+";

	public static final String GMT_OFFSET_ACTION_DEDUCT = "-";

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	public static final int ONLINE_STATUS_ACTIVE = 1;

	public static final int ONLINE_STATUS_INACTIVE = 0;

	public static final String YES = "Y";

	public static final String NO = "N";

	private String airportCode;

	private String airportName;

	private String contact;

	private String telephone;

	private String fax;

	private String gmtOffsetAction;

	private int gmtOffsetHours;

	private int minStopoverTime;

	private int minConnectionTime;

	private String latitudeNorthSouth;

	private String latitude;

	private String longitudeEastWest;

	private String longitude;

	private String status;

	private int onlineStatus;

	private String remarks;

	private String stationCode;

	private String closestAirportCode;

	private int ibeVisibility = 0;

	private int xbeVisibility = 0;

	private int lccVisibility = 0;

	private String goshowAgentCode;

	private String isGcc = "N";

	private String manualCheckin;

	private String carrierCode;

	private String lccPublishStatus;

	private int notificationStartCutovertime;

	private int notificationEndCutovertime;

	private String onlineCheckin;

	private String isSurfaceStation;

	private String connectingAirport;

	private Set<SubStation> subStations;

	private String notificationEmail;
	
	private String cityCode;

	private String cityId;

	private String isAADCSEnabled;

	private String etlProcessEnabled;

	private String autoFlownProcessEnabled;
	
	private String enableCFGElementForPNL;

	private String stateCode;

	/**
	 * returns the airportCode
	 * 
	 * @return Returns the airportCode.
	 * 
	 * @hibernate.id column = "AIRPORT_CODE" generator-class = "assigned"
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * sets the airportCode
	 * 
	 * @param airportCode
	 *            The airportCode to set.
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * returns the airportName
	 * 
	 * @return Returns the airportName.
	 * 
	 * @hibernate.property column = "AIRPORT_NAME"
	 */
	public String getAirportName() {
		return airportName;
	}

	/**
	 * sets the airportName
	 * 
	 * @param airportName
	 *            The airportName to set.
	 */
	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}

	/**
	 * returns the contact
	 * 
	 * @return Returns the contact.
	 * 
	 * @hibernate.property column = "CONTACT"
	 */
	public String getContact() {
		return contact;
	}

	/**
	 * sets the contact
	 * 
	 * @param contact
	 *            The contact to set.
	 */
	public void setContact(String contact) {
		this.contact = contact;
	}

	/**
	 * returns the telephone
	 * 
	 * @return Returns the telephone.
	 * 
	 * @hibernate.property column = "TELEPHONE"
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * sets the telephone
	 * 
	 * @param telephone
	 *            The telephone to set.
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * returns the fax
	 * 
	 * @return Returns the fax.
	 * 
	 * @hibernate.property column = "FAX"
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * sets the fax
	 * 
	 * @param fax
	 *            The fax to set.
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * returns the gmtOffsetAction
	 * 
	 * @return Returns the gmtOffsetAction.
	 * 
	 * @hibernate.property column = "GMT_OFFSET_ACTION"
	 */
	public String getGmtOffsetAction() {
		return gmtOffsetAction;
	}

	/**
	 * sets the gmtOffsetAction
	 * 
	 * @param gmtOffsetAction
	 *            The gmtOffsetAction to set.
	 */
	public void setGmtOffsetAction(String gmtOffsetAction) {
		this.gmtOffsetAction = gmtOffsetAction;
	}

	/**
	 * returns the gmtOffsetHours
	 * 
	 * @return Returns the gmtOffsetHours.
	 * 
	 * @hibernate.property column = "GMT_OFFSET_HOURS"
	 */
	public int getGmtOffsetHours() {
		return gmtOffsetHours;
	}

	/**
	 * sets the gmtOffsetHours
	 * 
	 * @param gmtOffsetHours
	 *            The gmtOffsetHours to set.
	 */
	public void setGmtOffsetHours(int gmtOffsetHours) {
		this.gmtOffsetHours = gmtOffsetHours;
	}

	/**
	 * returns the minStopoverTime
	 * 
	 * @return Returns the minStopoverTime.
	 * 
	 * @hibernate.property column = "MIN_STOPOVER_TIME"
	 */
	public int getMinStopoverTime() {
		return minStopoverTime;
	}

	/**
	 * sets the minStopoverTime
	 * 
	 * @param minStopoverTime
	 *            The minStopoverTime to set.
	 */
	public void setMinStopoverTime(int minStopoverTime) {
		this.minStopoverTime = minStopoverTime;
	}

	/**
	 * returns the minConnectionTime
	 * 
	 * @return Returns the minConnectionTime.
	 * 
	 * @hibernate.property column = "MIN_CONNECTION_TIME"
	 */
	public int getMinConnectionTime() {
		return minConnectionTime;
	}

	/**
	 * sets the minConnectionTime
	 * 
	 * @param minConnectionTime
	 *            The minConnectionTime to set.
	 */
	public void setMinConnectionTime(int minConnectionTime) {
		this.minConnectionTime = minConnectionTime;
	}

	/**
	 * returns the latitudeNorthSouth
	 * 
	 * @return Returns the latitudeNorthSouth.
	 * 
	 * @hibernate.property column = "LATITUDE_NORTH_SOUTH"
	 */
	public String getLatitudeNorthSouth() {
		return latitudeNorthSouth;
	}

	/**
	 * sets the latitudeNorthSouth
	 * 
	 * @param latitudeNorthSouth
	 *            The latitudeNorthSouth to set.
	 */
	public void setLatitudeNorthSouth(String latitudeNorthSouth) {
		this.latitudeNorthSouth = latitudeNorthSouth.intern();
	}

	/**
	 * returns the latitude
	 * 
	 * @return Returns the latitude.
	 * 
	 * @hibernate.property column = "LATITUDE"
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * sets the latitude
	 * 
	 * @param latitude
	 *            The latitude to set.
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * returns the longitudeEastWest
	 * 
	 * @return Returns the longitudeEastWest.
	 * 
	 * @hibernate.property column = "LONGITUDE_EAST_WEST"
	 */
	public String getLongitudeEastWest() {
		return longitudeEastWest;
	}

	/**
	 * sets the longitudeEastWest
	 * 
	 * @param longitudeEastWest
	 *            The longitudeEastWest to set.
	 */
	public void setLongitudeEastWest(String longitudeEastWest) {
		this.longitudeEastWest = longitudeEastWest.intern();
	}

	/**
	 * returns the longitude
	 * 
	 * @return Returns the longitude.
	 * 
	 * @hibernate.property column = "LONGITUDE"
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * sets the longitude
	 * 
	 * @param longitude
	 *            The longitude to set.
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * returns the activeStatus
	 * 
	 * @return Returns the activeStatus.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the Status
	 * 
	 * @param activeStatus
	 *            The activeStatus to set.
	 */
	public void setStatus(String status) {
		this.status = status.intern();
	}

	/**
	 * returns the onlineStatus
	 * 
	 * @return Returns the onlineStatus.
	 * 
	 * @hibernate.property column = "ONLINE_STATUS"
	 */
	public int getOnlineStatus() {
		return onlineStatus;
	}

	/**
	 * sets the onlineStatus
	 * 
	 * @param onlineStatus
	 *            The onlineStatus to set.
	 */
	public void setOnlineStatus(int onlineStatus) {
		this.onlineStatus = onlineStatus;
	}

	/**
	 * returns the remarks
	 * 
	 * @return Returns the remarks.
	 * 
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * sets the remarks
	 * 
	 * @param remarks
	 *            The remarks to set.
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * 
	 * @hibernate.property column = "CLOSET_AIRPORT"
	 * 
	 */
	public String getClosestAirport() {
		return closestAirportCode;
	}

	/**
   * 
   */
	public void setClosestAirport(String airportCode) {
		this.closestAirportCode = airportCode;
	}

	/**
	 * 
	 * @hibernate.property column = "STATION_CODE"
	 * 
	 */
	public String getStationCode() {
		return stationCode;
	}

	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}

	/**
	 * @return Returns the ibeVisibility.
	 * @hibernate.property column = "IBE_VISIBLITY"
	 */

	public int getIBEVisibility() {
		return ibeVisibility;
	}

	/**
	 * @param bookVisibililty
	 *            The bookVisibility to set.
	 */

	public void setIBEVisibility(int ibeVisibility) {
		this.ibeVisibility = ibeVisibility;
	}

	/**
	 * @return Returns the xbeVisibility.
	 * @hibernate.property column = "XBE_VISIBLITY"
	 */

	public int getXBEVisibility() {
		return xbeVisibility;
	}

	/**
	 * @param bookVisibililty
	 *            The bookVisibility to set.
	 */

	public void setXBEVisibility(int xbeVisibility) {
		this.xbeVisibility = xbeVisibility;
	}

	/**
	 * 
	 * @return Return GOSHOW Agent
	 * @hibernate.property column = "GOSHOW_AGENT_CODE"
	 */
	public String getGoshowAgentCode() {
		return goshowAgentCode;
	}

	/**
	 * 
	 * @param goshowAgentCode
	 */
	public void setGoshowAgentCode(String goshowAgentCode) {
		this.goshowAgentCode = goshowAgentCode;
	}

	/**
	 * @return the isGcc
	 * @hibernate.property column = "GCC"
	 */
	public String getIsGcc() {
		return isGcc;
	}

	/**
	 * @param isGcc
	 *            the isGcc to set
	 */
	public void setIsGcc(String isGcc) {
		this.isGcc = isGcc.intern();
	}

	/**
	 * @return lccVisibility
	 * @hibernate.property column = "XBEV2_VISIBILITY"
	 */
	public int getLccVisibility() {
		return lccVisibility;
	}

	/**
	 * @param lccVisibility
	 */
	public void setLccVisibility(int lccVisibility) {
		this.lccVisibility = lccVisibility;
	}

	/**
	 * @return the manualCheckin
	 * @hibernate.property column = "MANUAL_CHECKIN"
	 */
	public String getManualCheckin() {
		return manualCheckin;
	}

	/**
	 * @param manualCheckin
	 *            the manualCheckin to set
	 */

	public void setManualCheckin(String manualCheckin) {
		this.manualCheckin = manualCheckin.intern();
	}

	/**
	 * @return the carrierCode
	 * @hibernate.property column = "OPERATED_CARRIERS"
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrierCode
	 *            the carrierCode to set
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	/**
	 * @return the carrierCode
	 * @hibernate.property column = "LCC_PUBLISH_STATUS"
	 */
	public String getLccPublishStatus() {
		return lccPublishStatus;
	}

	/**
	 * @param lccPublishStatus
	 *            the lccPublishStatus to set
	 */

	public void setLccPublishStatus(String lccPublishStatus) {
		this.lccPublishStatus = lccPublishStatus.intern();
	}

	/**
	 * @return the notificationStartCutovertime
	 * @hibernate.property column = "ANCI_NOTIFY_START_CUTOVER"
	 */
	public int getNotificationStartCutovertime() {
		return notificationStartCutovertime;
	}

	/**
	 * @param notificationStartCutovertime
	 *            the notificationStartCutovertime to set
	 */
	public void setNotificationStartCutovertime(int notificationStartCutovertime) {
		this.notificationStartCutovertime = notificationStartCutovertime;
	}

	/**
	 * @return the notificationEndCutovertime
	 * @hibernate.property column = "ANCI_NOTIFY_END_CUTOVER"
	 */
	public int getNotificationEndCutovertime() {
		return notificationEndCutovertime;
	}

	/**
	 * @param notificationEndCutovertime
	 *            the notificationEndCutovertime to set
	 */
	public void setNotificationEndCutovertime(int notificationEndCutovertime) {
		this.notificationEndCutovertime = notificationEndCutovertime;
	}

	/**
	 * @return the notificationEndCutovertime
	 * @hibernate.property column = "ONLINE_CHECKIN"
	 */
	public String getOnlineCheckin() {
		return onlineCheckin;
	}

	/**
	 * @param onlineCheckin
	 *            the onlineCheckin to set
	 */
	public void setOnlineCheckin(String onlineCheckin) {
		this.onlineCheckin = onlineCheckin;
	}

	/**
	 * 
	 * @return the isSurfaceStation
	 * @hibernate.property column = "IS_SURFACE_STATION"
	 */
	public String getIsSurfaceStation() {
		return isSurfaceStation;
	}

	/**
	 * 
	 * @param isSurfaceStation
	 *            the isSurfaceStation to set
	 */
	public void setIsSurfaceStation(String isSurfaceStation) {
		this.isSurfaceStation = isSurfaceStation.intern();
	}

	/**
	 * 
	 * @return the connecting airport
	 * @hibernate.property column = "CONNECTING_AIRPORT"
	 */
	public String getConnectingAirport() {
		return connectingAirport;
	}

	/**
	 * 
	 * @param connectingAirport
	 *            the connecting airport to set
	 */
	public void setConnectingAirport(String connectingAirport) {
		this.connectingAirport = connectingAirport;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true" order-by="SUB_STATION_ID"
	 * @hibernate.collection-key column="AIRPORT_CODE"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airmaster.api.model.SubStation"
	 */
	public Set<SubStation> getSubStations() {
		return subStations;
	}

	/**
	 * 
	 * @param subStations
	 *            the subStations to set
	 */
	public void setSubStations(Set<SubStation> subStations) {
		this.subStations = subStations;
	}

	/**
	 * @return the isSurfaceSegment
	 */
	public boolean isSurfaceSegment() {
		if (this.isSurfaceStation.equalsIgnoreCase("Y")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return the notificationEmail
	 * @hibernate.property column = "NOTIFICATION_EMAIL"
	 */
	public String getNotificationEmail() {
		return notificationEmail;
	}

	/**
	 * @param notificationEmail
	 *            the notificationEmail to set
	 */
	public void setNotificationEmail(String notificationEmail) {
		this.notificationEmail = notificationEmail;
	}

	/**
	 * @return the cityId
	 * @hibernate.property column = "CITY_ID"
	 */
	public String getCityId() {
		return cityId;
	}

	/**
	 * @param cityId
	 *            the cityId to set
	 */
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	
	/**
	 * 
	 * @return the isAADCSEnabled
	 * @hibernate.property column = "IS_AA_DCS_ENABLED"
	 */
	public String getIsAADCSEnabled() {
		return isAADCSEnabled;
	}

	public void setIsAADCSEnabled(String isAADCSEnabled) {
		this.isAADCSEnabled = isAADCSEnabled;
	}

	public boolean isWSAADCSEnabled() {
		if (this.isAADCSEnabled != null && this.isAADCSEnabled.equalsIgnoreCase("Y")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @return the etlProcessEnabled
	 * @hibernate.property column = "ETL_PROCESS_ENABLED"
	 */
	public String getEtlProcessEnabled() {
		return etlProcessEnabled;
	}

	public void setEtlProcessEnabled(String etlProcessEnabled) {
		this.etlProcessEnabled = etlProcessEnabled;
	}

	public boolean etlProcessEnabled() {
		return YES.equals(this.etlProcessEnabled);
	}

	/**
	 * 
	 * @return the etlProcessEnabled
	 * @hibernate.property column = "AUTO_FLOWN_PROCESS_ENABLED"
	 */
	public String getAutoFlownProcessEnabled() {
		return autoFlownProcessEnabled;
	}

	public void setAutoFlownProcessEnabled(String autoFlownProcessEnabled) {
		this.autoFlownProcessEnabled = autoFlownProcessEnabled;
	}

	public boolean hasAutomaticFlownProcessEnabled() {
		return YES.equals(this.autoFlownProcessEnabled);
	}

	/**
	 * 
	 * @return the etlProcessEnabled
	 * @hibernate.property column = "PNL_CFG_ENABLED"
	 */
	public String getEnableCFGElementForPNL() {
		return enableCFGElementForPNL;
	}

	public void setEnableCFGElementForPNL(String enableCFGElementForPNL) {
		this.enableCFGElementForPNL = enableCFGElementForPNL;
	}
	
	public boolean hasEnableCFGElementForPNL(){
		return YES.equals(this.enableCFGElementForPNL);
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
}
