package com.isa.thinair.airmaster.api.service;

import java.util.List;
import java.util.Map;

import com.isa.thinair.airmaster.api.criteria.AutomaticCheckinTemplatesSearchCriteria;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.dto.AutomaticCheckinDTO;
import com.isa.thinair.airmaster.api.model.AutoCheckinTemplateAirport;
import com.isa.thinair.airmaster.api.model.AutomaticCheckinTemplate;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author aravinth.r
 *
 */
public interface AutomaticCheckinBD {

	public static final String SERVICE_NAME = "AutomaticCheckinService";

	public AutomaticCheckinTemplate getAutomaticCheckinTemplate(int id) throws ModuleException;

	public Page<AutomaticCheckinTemplate> getAutomaticCheckinTemplates(int startRec, int numOfRecs,
			AutomaticCheckinTemplatesSearchCriteria actSearchCriteria) throws ModuleException;

	public AutomaticCheckinTemplate updateAutomaticCheckinTemplate(AutomaticCheckinTemplate automaticCheckinTemplate)
			throws ModuleException;

	public Map<AirportServiceKeyTO, List<AutomaticCheckinDTO>> getAvailableAutomaticCheckins(
			Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseSegMap) throws ModuleException;

	public void saveAutomaticCheckinTemplate(AutomaticCheckinTemplate autoCheckinTemplate) throws ModuleException;

	public List<AutomaticCheckinDTO> getAutomaticCheckinDetails(String airportCode) throws ModuleException;

	public List<AutoCheckinTemplateAirport> getAutomaticCheckinAirports(int autoCheckinTempId) throws ModuleException;

	public List<String> getAutoCheckinAirportList(List<String> airportList) throws ModuleException;
}
