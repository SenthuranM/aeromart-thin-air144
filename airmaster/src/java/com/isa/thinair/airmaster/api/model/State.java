package com.isa.thinair.airmaster.api.model;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_STATE"
 * 
 */
public class State extends Tracking {

	private static final long serialVersionUID = 4054634866764702118L;
	
	private Integer stateId;

	private String stateCode;

	private String stateName;

	private String status;

	private String countryCode;
	
	private String airlineOfficeSTAddress1;
	
	private String airlineOfficeSTAddress2;
	
	private String airlineOfficeCity;
	
	private String airlineOfficeTaxRegNo;
	
	private String digitalSignature;
	
	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	
	/**
	 * @return Returns the stateId.
	 * 
	 * @hibernate.id column="STATE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value ="S_STATE"
	 */
	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}
	
	/**
	 * @return Returns the StateCode.
	 * @hibernate.property  column = "STATE_CODE"
	 */
	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}


	/**
	 * @return Returns the State name.
	 * @hibernate.property column = "STATE_NAME"
	 */
	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the countryCode.
	 * @hibernate.property column = "COUNTRY_CODE"
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param countryCode
	 *            The countryCode to set.
	 * 
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "AIRLINE_OFC_STREET_ADDRESS_1"
	 */
	public String getAirlineOfficeSTAddress1() {
		return airlineOfficeSTAddress1;
	}

	public void setAirlineOfficeSTAddress1(String airlineOfficeSTAddress1) {
		this.airlineOfficeSTAddress1 = airlineOfficeSTAddress1;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "AIRLINE_OFC_STREET_ADDRESS_2"
	 */
	public String getAirlineOfficeSTAddress2() {
		return airlineOfficeSTAddress2;
	}

	public void setAirlineOfficeSTAddress2(String airlineOfficeSTAddress2) {
		this.airlineOfficeSTAddress2 = airlineOfficeSTAddress2;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "AIRLINE_OFC_CITY"
	 */
	public String getAirlineOfficeCity() {
		return airlineOfficeCity;
	}

	public void setAirlineOfficeCity(String airlineOfficeCity) {
		this.airlineOfficeCity = airlineOfficeCity;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "AIRLINE_OFC_TAX_REG_NUMBER"
	 */
	public String getAirlineOfficeTaxRegNo() {
		return airlineOfficeTaxRegNo;
	}

	public void setAirlineOfficeTaxRegNo(String airlineOfficeTaxRegNo) {
		this.airlineOfficeTaxRegNo = airlineOfficeTaxRegNo;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "DIGITAL_SIGNATURE"
	 */
	public String getDigitalSignature() {
		return digitalSignature;
	}

	public void setDigitalSignature(String digitalSignature) {
		this.digitalSignature = digitalSignature;
	}

}
