package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;

public class MealSearchDTO implements Serializable {

	private static final long serialVersionUID = -8875126064321449325L;
	private String mealName;
	private String mealCode;
	private String status;
	private String classOfService;
	private String iataCode;

	public String getMealName() {
		return mealName;
	}

	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	public String getMealCode() {
		return mealCode;
	}

	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the iataCode
	 */
	public String getIataCode() {
		return iataCode;
	}

	/**
	 * @param iataCode
	 *            the iataCode to set
	 */
	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	/**
	 * @return the classOfService
	 */
	public String getClassOfService() {
		return classOfService;
	}

	/**
	 * @param classOfService
	 *            the classOfService to set
	 */
	public void setClassOfService(String classOfService) {
		this.classOfService = classOfService;
	}
}
