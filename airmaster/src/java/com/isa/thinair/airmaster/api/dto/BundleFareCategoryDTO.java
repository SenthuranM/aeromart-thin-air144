package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by degorov on 4/30/17.
 */
public class BundleFareCategoryDTO implements Serializable {

    private Integer bundledCategoryId;
    private String status;
    private String language;
    private String categoryName;
    private String description;
    private Integer priority;
    private Long version;

    private Map<String, BundledFareCategoryTranslation> translations;

    public static class BundledFareCategoryTranslation  implements Serializable {
        private String categoryName;
        private String description;

        public BundledFareCategoryTranslation(String categoryName, String description) {
            this.categoryName = categoryName;
            this.description = description;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getBundledCategoryId() {
        return bundledCategoryId;
    }

    public void setBundledCategoryId(Integer bundledCategoryId) {
        this.bundledCategoryId = bundledCategoryId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Map<String, BundledFareCategoryTranslation> getTranslations() {
        return translations;
    }

    public void setTranslations(Map<String, BundledFareCategoryTranslation> translations) {
        this.translations = translations;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setDefaultTranslations(String language){
        this.language = language;
        this.description = "";
        this.categoryName = "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BundleFareCategoryDTO that = (BundleFareCategoryDTO) o;

        return bundledCategoryId != null ? bundledCategoryId.equals(that.bundledCategoryId) : that.bundledCategoryId == null;
    }

    @Override
    public int hashCode() {
        return bundledCategoryId != null ? bundledCategoryId.hashCode() : 0;
    }
}
