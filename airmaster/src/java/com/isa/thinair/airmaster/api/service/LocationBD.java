/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:10
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airmaster.api.criteria.StationSearchCriteria;
import com.isa.thinair.airmaster.api.dto.CountryDetailsDTO;
import com.isa.thinair.airmaster.api.model.City;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.HubInfo;
import com.isa.thinair.airmaster.api.model.Station;
import com.isa.thinair.airmaster.api.model.Territory;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.ondpublish.OndLocationDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Byorn
 */
public interface LocationBD {
	public static final String SERVICE_NAME = "LocationService";

	public void saveCountry(Country obj) throws ModuleException;

	public void saveTerritory(Territory obj) throws ModuleException;

	public void deleteCountry(String key) throws ModuleException;

	public void deleteTerritory(String key) throws ModuleException;

	public void deleteCountry(Country country) throws ModuleException;

	public void deleteTerritory(Territory territory) throws ModuleException;

	public Collection<Country> getCountries() throws ModuleException;

	public Page getCountries(int startrec, int numofrecs, String countryCode) throws ModuleException;

	/**
	 * This method will return country codes for given region. If no region passed all country codes will be return.
	 */
	public List<Country> getCountryCodesForRegions(List<String> regionCodes) throws ModuleException;

	public Collection<Territory> getTerritories() throws ModuleException;

	public Page getTerritories(int startrec, int numofrecs) throws ModuleException;

	public Page getTerritorys(int StartRec, int NumOfRecs, String territoryCode, String territoryDesc) throws ModuleException;

	public Country getCountry(String countryCode) throws ModuleException;

	public Territory getTerritory(String territoryCode) throws ModuleException;

	public Collection<Territory> getActiveTerritories() throws ModuleException;

	public Collection<Country> getActiveCountries() throws ModuleException;

	public Page getStations(StationSearchCriteria criteria, int startRec, int pageSize) throws ModuleException;

	public void saveStation(Station station) throws ModuleException;

	public void removeStation(String stationCode) throws ModuleException;

	public void removeStation(Station station) throws ModuleException;

	public Station getStation(String stationCode) throws ModuleException;

	public Collection<String> getTerritoriesForStations(Collection<String> stationCodes) throws ModuleException;

	public Map<String, String> getStationContactDetails(String airportCode) throws ModuleException;

	public List<String> getStationDetails(String stCode, boolean isGsa) throws ModuleException;

	/**
	 * Hub Details related
	 */

	/**
	 * This method is used to search Hub Details information from the Database.
	 * 
	 * @param searchCriteria
	 * @param startRec
	 * @param pageSize
	 * @return Page
	 * @throws ModuleException
	 * @author Dhanusha
	 * @since 25 Feb, 2008
	 */
	public Page getHubDetails(List<ModuleCriterion> searchCriteria, int startRec, int pageSize) throws ModuleException;

	/**
	 * This method is used to Save/Update information to the Database.
	 * 
	 * @param hubInfo
	 * @throws ModuleException
	 * @author Dhanusha
	 * @since 25 Feb, 2008
	 */
	public void saveHubInfo(HubInfo hubInfo) throws ModuleException;

	/**
	 * This method is used to Delete information from the Database.
	 * 
	 * @param hubInfo
	 * @throws ModuleException
	 * @author Dhanusha
	 * @since 25 Feb, 2008
	 */
	public void removeHubInfo(HubInfo hubInfo) throws ModuleException;

	/**
	 * 
	 * @param airportCodes
	 * @return
	 */
	public Collection<String> getValidHubCodes(Collection<String> airportCodes) throws ModuleException;

	/**
	 * 
	 * Get Hub Information by airport code, in-bound carrier & out-bound carrier
	 * 
	 * @param airportCode
	 * @param ibCarrier
	 * @param obCarrier
	 * @return
	 * @throws ModuleException
	 */
	public HubInfo getHubInfo(String airportCode, String ibCarrier, String obCarrier) throws ModuleException;

	public String getNameChangeThresholTimePerStation(String station) throws ModuleException;

	/**
	 * This method is used to retrieve Active hub codes
	 * 
	 * @return Collection
	 * @throws ModuleException
	 */
	public Collection<String> getActiveHubCodes() throws ModuleException;
	
	public Collection<OndLocationDTO> getActiveHubDetails() throws ModuleException;

	public List<Station> getLCCUnPublishedAirports() throws ModuleException;

	public List<Territory> getLCCUnPublishedTerritories() throws ModuleException;

	public void updateStationLCCPublishStatus(List<Station> unPublishedStations, List<String> updatedStationCodes)
			throws ModuleException;

	public void updateTerritoryLCCPublishStatus(List<Territory> unPublishedTerritories, List<String> updatedTerritoryCodes)
			throws ModuleException;

	public void updateStationVersion(String stationCode, long version) throws ModuleException;

	public void updateTerritoryVersion(String territoryCode, long version) throws ModuleException;

	public Country getCountryByIsoCode(String isoCode) throws ModuleException;
	
	public List<CountryDetailsDTO> getCountryDetailsList() throws ModuleException;

	public boolean isValidCityStationCombination(String stationCode, String cityId) throws ModuleException;

	public City getCity(String cityCode) throws ModuleException;

	public List<City> getLCCUnPublishedCities() throws ModuleException;

	public void updateCityLCCPublishStatus(List<City> originalCities, List<String> updatedCityCodes) throws ModuleException;

	public void saveCity(City city) throws ModuleException;

	public List<String> getCityCodes() throws ModuleException;

    public CountryDetailsDTO getCountryByPhoneCode(String phoneCode) throws ModuleException;
	
}
