package com.isa.thinair.airmaster.api.dto.external;

import java.io.Serializable;

import com.isa.thinair.airmaster.api.dto.ExchangeRateInfoDTO;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.commons.core.framework.Tracking;

public class ExRateAutoUpdateResultDTO extends Tracking implements Serializable {

	private static final long serialVersionUID = -245352851994106590L;

	public enum RateUpdateStatus {
		SUCCESS, FAILED_TO_EXCEPTION, FAILED_TO_AUTOUPDATE_DISABLED, FAILED_TO_VARIANCE
	};

	private ExchangeRateInfoDTO exchangeRateSourceInfo;

	private CurrencyExchangeRate newRate;

	private CurrencyExchangeRate changedRate;

	private boolean isUpdated;

	private String errorMessage;

	private RateUpdateStatus updateStatus;

	private Currency currency;

	public ExchangeRateInfoDTO getExchangeRateSourceInfo() {
		return exchangeRateSourceInfo;
	}

	public void setExchangeRateSourceInfo(ExchangeRateInfoDTO exchangeRateSourceInfo) {
		this.exchangeRateSourceInfo = exchangeRateSourceInfo;
	}

	public CurrencyExchangeRate getNewRate() {
		return newRate;
	}

	public void setNewRate(CurrencyExchangeRate newRate) {
		this.newRate = newRate;
	}

	public CurrencyExchangeRate getChangedRate() {
		return changedRate;
	}

	public void setChangedRate(CurrencyExchangeRate changedRate) {
		this.changedRate = changedRate;
	}

	public boolean isUpdated() {
		return isUpdated;
	}

	public void setUpdated(boolean isUpdated) {
		this.isUpdated = isUpdated;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public RateUpdateStatus getUpdateStatus() {
		return updateStatus;
	}

	public void setUpdateStatus(RateUpdateStatus updateStatus) {
		this.updateStatus = updateStatus;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

}
