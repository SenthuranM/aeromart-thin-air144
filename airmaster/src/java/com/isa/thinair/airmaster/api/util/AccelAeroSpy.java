package com.isa.thinair.airmaster.api.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.SpyRecord;
import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * AccelAero Spy is a tool which helps to identify mysterious bugs by tracking those in the database with the relevant
 * triggering conditions. This will also helps to track whether particular situations are happening or not.
 * 
 * AccelAero had many mysterious bugs which was not able to reproduce easily. This motivated me to write this tool.
 * Please feel free to improve this.
 * 
 * @author Nilindra Fernando
 * 
 * @since Oct 10, 2010
 */
public class AccelAeroSpy {

	private static Log log = LogFactory.getLog(AccelAeroSpy.class);

	public enum SpyCode {
		SAMPLE_CASE_1("CASE1", "Sample description of the case");

		private SpyCode(String value, String desc) {
			this.value = value;
			this.desc = desc;
		}

		private String value;
		private String desc;

		public String getValue() {
			return value;
		}
	}

	public static void spy(SpyCode spyCode, String contents) {
		spy(spyCode, null, 5, contents);
	}

	public static void spy(SpyCode spyCode, UserPrincipal userPrincipal, String contents) {
		spy(spyCode, userPrincipal, 5, contents);
	}

	public static boolean isSpyEnabled() {
		return AppSysParamsUtil.isAccelAeroSpyEnabled();
	}

	public static void spy(SpyCode spyCode, UserPrincipal userPrincipal, int maximumAttempts, String contents) {
		if (isSpyEnabled()) {
			try {
				SpyRecord spyRecord = new SpyRecord();
				spyRecord.setAuditCode(spyCode.getValue());
				spyRecord.setTimeStamp(new Date());
				spyRecord.setMaximumAttempts(maximumAttempts);
				spyRecord.setContent(getContents(contents, 4000));

				if (userPrincipal != null) {
					spyRecord.setUserId(userPrincipal.getUserId());
					spyRecord.setClientInfo(userPrincipal.getClientInfo());
				}

				Collection<SpyRecord> colSpyRecord = new ArrayList<SpyRecord>();
				colSpyRecord.add(spyRecord);
				AirmasterUtils.getCommonMasterBD().saveSpyMessages(colSpyRecord);
			} catch (Exception e) {
				log.error(e);
			}
		}
	}

	private static String getContents(String content, int size) {
		content = PlatformUtiltiies.nullHandler(content);

		if (content.length() > size) {
			return content.substring(0, size);
		} else {
			return content;
		}
	}

}
