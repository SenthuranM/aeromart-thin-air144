package com.isa.thinair.airmaster.api.model;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author rumesh
 * 
 * @hibernate.class table = "T_SSR_TEMPLATE_CABIN_QTY"
 */
public class SSRTemplateCabinQuantity extends Persistent {

	private static final long serialVersionUID = 1L;

	private Integer templateCabinQtyId;
	private SSRTemplate ssrTemplate;
	private Integer ssrId;
	private String cabinClass;
	private String logicalCCCode;
	private Integer maxQuantity;
	private String status;

	/**
	 * @return the templateCabinQtyId
	 * 
	 * @hibernate.id column="SSR_TEMPLATE_CABIN_QTY_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_SSR_TEMPLATE_CABIN_QTY"
	 */
	public Integer getTemplateCabinQtyId() {
		return templateCabinQtyId;
	}

	/**
	 * @param templateCabinQtyId
	 *            the templateCabinQtyId to set
	 */
	public void setTemplateCabinQtyId(Integer templateCabinQtyId) {
		this.templateCabinQtyId = templateCabinQtyId;
	}

	/**
	 * @return the ssrTemplate
	 * 
	 * @hibernate.many-to-one column="SSR_TEMPLATE_ID" class="com.isa.thinair.airmaster.api.model.SSRTemplate"
	 */
	public SSRTemplate getSsrTemplate() {
		return ssrTemplate;
	}

	/**
	 * @param ssrTemplate
	 *            the ssrTemplate to set
	 */
	public void setSsrTemplate(SSRTemplate ssrTemplate) {
		this.ssrTemplate = ssrTemplate;
	}

	/**
	 * @return the ssrId
	 * 
	 * @hibernate.property column = "SSR_ID"
	 */
	public Integer getSsrId() {
		return ssrId;
	}

	/**
	 * @param ssrId
	 *            the ssrId to set
	 */
	public void setSsrId(Integer ssrId) {
		this.ssrId = ssrId;
	}

	/**
	 * @return the cabinClass
	 * 
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClass() {
		return cabinClass;
	}

	/**
	 * @param cabinClass
	 *            the cabinClass to set
	 */
	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	/**
	 * @return the maxQuantity
	 * 
	 * @hibernate.property column = "MAX_QTY"
	 */
	public Integer getMaxQuantity() {
		return maxQuantity;
	}

	/**
	 * @param maxQuantity
	 *            the maxQuantity to set
	 */
	public void setMaxQuantity(Integer maxQuantity) {
		this.maxQuantity = maxQuantity;
	}

	/**
	 * @return the status
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	public boolean equals(Object o) {
		if (o instanceof SSRTemplateCabinQuantity) {
			SSRTemplateCabinQuantity obj = (SSRTemplateCabinQuantity) o;
			if (obj.getTemplateCabinQtyId() != null && this.getTemplateCabinQtyId() != null) {

				if (obj.getTemplateCabinQtyId().intValue() == this.getTemplateCabinQtyId().intValue()) {
					return true;
				} else {
					return false;
				}

			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public int hashCode() {
		return new HashCodeBuilder().append(this.templateCabinQtyId).toHashCode();
	}
	
	/***
	 * 
	 * @return
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

}
