/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:10
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.service;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Haider
 */
public interface GdsBD {

	public static final String SERVICE_NAME = "GdsService";

	public List<Gds> getGDSs() throws ModuleException;

	public Gds getGds(int id) throws ModuleException;

	public void saveGds(Gds gds) throws ModuleException;

	public void removeGds(int id) throws ModuleException;

	public Page getGDSs(int startRec, int noRecs) throws ModuleException;

	public Collection<Gds> getActiveGDSs() throws ModuleException;

	public Gds getGDSByCode(String gdsCode) throws ModuleException;
}
