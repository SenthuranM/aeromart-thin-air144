package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;

/**
 * @author eric
 * @version : Ver 1.0.0
 * @hibernate.class table = "T_LANGUAGE"
 */
public class Language implements Serializable {

	private static final long serialVersionUID = 1L;
	private String languageCode;
	private String languageName;
	private String ibeContent;
	private String ibeItinerary;
	private String xbeContent;
	private String xbeItinerary;
	private String adminContent;

	/**
	 * @return Returns the languageCode.
	 * @hibernate.id column = "LANGUAGE_CODE" generator-class = "assigned"
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	/**
	 * @param languageCode
	 */
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	/**
	 * @return
	 * @hibernate.property column = "LANGUAGE_NAME"
	 */
	public String getLanguageName() {
		return languageName;
	}

	/**
	 * @param languageName
	 */
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	/**
	 * @return
	 * @hibernate.property column = "IBE_CONTENT "
	 */
	public String getIbeContent() {
		return ibeContent;
	}

	/**
	 * @param ibeContent
	 * 
	 */
	public void setIbeContent(String ibeContent) {
		this.ibeContent = ibeContent;
	}

	/**
	 * @return
	 * @hibernate.property column = "IBE_ITINERARY"
	 */
	public String getIbeItinerary() {
		return ibeItinerary;
	}

	/**
	 * @param ibeItinerary
	 */
	public void setIbeItinerary(String ibeItinerary) {
		this.ibeItinerary = ibeItinerary;
	}

	/**
	 * @return
	 * @hibernate.property column = "XBE_CONTENT"
	 */
	public String getXbeContent() {
		return xbeContent;
	}

	/**
	 * @param xbeContent
	 */
	public void setXbeContent(String xbeContent) {
		this.xbeContent = xbeContent;
	}

	/**
	 * @return
	 * @hibernate.property column = "XBE_ITINERARY"
	 */
	public String getXbeItinerary() {
		return xbeItinerary;
	}

	/**
	 * @param xbeItinerary
	 */
	public void setXbeItinerary(String xbeItinerary) {
		this.xbeItinerary = xbeItinerary;
	}

	/**
	 * @return
	 * @hibernate.property column = "ADMIN_CONTENT"
	 */
	public String getAdminContent() {
		return adminContent;
	}

	/**
	 * @param adminContent
	 */
	public void setAdminContent(String adminContent) {
		this.adminContent = adminContent;
	}

}
