package com.isa.thinair.airmaster.api.service;

import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airmaster.core.bl.exrateautomation.ExRateAutomateConfigImpl;
import com.isa.thinair.airmaster.core.config.AirmasterConfig;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.utils.AirpricingConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.service.ScheduleBD;
import com.isa.thinair.airschedules.api.utils.AirschedulesConstants;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.airsecurity.api.utils.AirsecurityConstants;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.utils.AuditorConstants;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.gdsservices.api.service.GDSPublishingBD;
import com.isa.thinair.gdsservices.api.utils.GdsservicesConstants;
import com.isa.thinair.lccclient.api.service.LCCMasterDataPublisherBD;
import com.isa.thinair.lccclient.api.utils.LccclientConstants;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.core.bean.UnifiedBeanFactory;
import com.isa.thinair.promotion.api.service.BunldedFareBD;
import com.isa.thinair.promotion.api.utils.PromotionConstants;

public class AirmasterUtils {

	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule("airmaster");
	}

	/**
	 * For Looking the BookingClass BD IServiceDelegate
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @return
	 */
	private static IServiceDelegate lookupServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getAirmasterConfig(), "airmaster",
				"airmaster.config.dependencymap.invalid");
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getAirmasterConfig(), "airmaster",
				"airmaster.config.dependencymap.invalid");
	}

	public static AirmasterConfig getAirmasterConfig() {
		return (AirmasterConfig) LookupServiceFactory.getInstance().getModuleConfig("airmaster");
	}

	public static AuditorBD getAuditorBD() {
		return (AuditorBD) lookupServiceBD(AuditorConstants.MODULE_NAME, AuditorConstants.BDKeys.AUDITOR_SERVICE);
	}

	public static MessagingServiceBD getMessagingBD() {
		return (MessagingServiceBD) lookupEJB3Service(MessagingConstants.MODULE_NAME, MessagingServiceBD.SERVICE_NAME);

	}

	/**
	 * Gets the Common Master BD
	 * 
	 * @return CommonMasterBD
	 */
	public static CommonMasterBD getCommonMasterBD() {
		return (CommonMasterBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, CommonMasterBD.SERVICE_NAME);
	}

	public static FlightBD getFlightBD() {
		return (FlightBD) lookupEJB3Service(AirschedulesConstants.MODULE_NAME, FlightBD.SERVICE_NAME);
	}

	public static FlightInventoryBD getFlightInventoryBD() {
		return (FlightInventoryBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, FlightInventoryBD.SERVICE_NAME);
	}

	public static ChargeBD getChargeBD() {
		return (ChargeBD) lookupEJB3Service(AirpricingConstants.MODULE_NAME, ChargeBD.SERVICE_NAME);
	}

	// exchange rate automation source lookup
	public static ExRateAutomateConfigImpl getExchangeRateSource() {
		return (ExRateAutomateConfigImpl) LookupServiceFactory.getInstance().getBean(
				"isa:base://modules/airmaster?id=exRateAutomateConfigImpl");
	}

	public static AirportBD getAirportBD() {
		return (AirportBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, AirportBD.SERVICE_NAME);
	}

	public final static TranslationBD getTranslationBD() {
		return (TranslationBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, TranslationBD.SERVICE_NAME);
	}

	public final static TravelAgentBD getTravelAgentBD() {
		return (TravelAgentBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, TravelAgentBD.SERVICE_NAME);
	}

	public final static ScheduleBD getScheduleBD() {
		return (ScheduleBD) lookupEJB3Service(AirschedulesConstants.MODULE_NAME, ScheduleBD.SERVICE_NAME);
	}

	public final static LocationBD getLocationBD() {
		return (LocationBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, LocationBD.SERVICE_NAME);
	}

	public final static SecurityBD getSecurityBD() {
		return (SecurityBD) lookupEJB3Service(AirsecurityConstants.MODULE_NAME, SecurityBD.SERVICE_NAME);
	}

	public static LCCMasterDataPublisherBD getLCCMasterDataPublisherBD() {
		return (LCCMasterDataPublisherBD) lookupEJB3Service(LccclientConstants.MODULE_NAME, LCCMasterDataPublisherBD.SERVICE_NAME);
	}

	public static final BunldedFareBD getBundledFareBD() {
		return (BunldedFareBD) lookupEJB3Service(PromotionConstants.MODULE_NAME, BunldedFareBD.SERVICE_NAME);
	}
	
	public static GDSPublishingBD getGDSPublishingBD() {
		return (GDSPublishingBD) lookupServiceBD(GdsservicesConstants.MODULE_NAME,
				GdsservicesConstants.BDKeys.GDSSERVICES_GDSPUBLISHING);
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getBeanByName(String beanName) {
		return (T)LookupServiceFactory.getInstance().getBean(
				UnifiedBeanFactory.URI_SCHEME_PLUS_SLASHES + "modules/airmaster?id=" + beanName);
	}
	
	
	
}
