package com.isa.thinair.airmaster.api.util;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

/**
 * ExchangeRateProxy caches the exchange rates info, in order prevent loading multiple times This was mainly introduced
 * to keep the CPU utilization low.
 * 
 * @author Nilindra Fernando
 * @since May 15, 2012
 */
public class ExchangeRateProxy {

	private static Log log = LogFactory.getLog(ExchangeRateProxy.class);

	private Date executionDate;

	private Map<Pair<String, String>, BigDecimal> mapPairAmounts;

	private Map<String, CurrencyExchangeRate> mapCurrencyExchangeRates;

	private Map<String, BigDecimal> mapPurchaseAmounts;

	public ExchangeRateProxy(Date executionDate) {
		setExecutionDate(executionDate);
		setMapPairAmounts(new HashMap<Pair<String, String>, BigDecimal>());
		setMapCurrencyExchangeRates(new HashMap<String, CurrencyExchangeRate>());
		setMapPurchaseAmounts(new HashMap<String, BigDecimal>());
	}

	public BigDecimal getExchangeRate(String fromCurrencyCode, String toCurrencyCode) throws ModuleException {
		Pair<String, String> pair = Pair.of(fromCurrencyCode, toCurrencyCode);
		BigDecimal exchangeRate = getMapPairAmounts().get(pair);

		if (exchangeRate == null) {
			exchangeRate = ReservationModuleUtils.getCommonMasterBD().getExchangeRateValue(fromCurrencyCode, toCurrencyCode,
					getExecutionDate(), false);
			getMapPairAmounts().put(pair, exchangeRate);
		}

		return exchangeRate;
	}

	public CurrencyExchangeRate getCurrencyExchangeRate(String currencyCode) throws ModuleException {
		CurrencyExchangeRate currencyExchangeRate = getMapCurrencyExchangeRates().get(currencyCode);

		if (currencyExchangeRate == null) {
			currencyExchangeRate = ReservationModuleUtils.getCommonMasterBD().getExchangeRate(currencyCode, getExecutionDate());
			getMapCurrencyExchangeRates().put(currencyCode, currencyExchangeRate);
		}

		return currencyExchangeRate;
	}

	public BigDecimal getPurchaseExchangeRateAgainstBase(String purchaseCurrencyCode) throws ModuleException {
		BigDecimal exchangeRate = getMapPurchaseAmounts().get(purchaseCurrencyCode);

		if (exchangeRate == null) {
			exchangeRate = ReservationModuleUtils.getCommonMasterBD().getExchangeRateValue(purchaseCurrencyCode,
					AppSysParamsUtil.getBaseCurrency(), getExecutionDate(), true);
			getMapPurchaseAmounts().put(purchaseCurrencyCode, exchangeRate);
		}

		return exchangeRate;
	}

	public boolean isValidCurrencyExchangeRateExists(String currencyCode, ApplicationEngine channel) throws ModuleException {
		return getCurrencyExchangeRate(currencyCode, channel) != null;
	}

	public CurrencyExchangeRate getCurrencyExchangeRate(String currencyCode, ApplicationEngine channel) throws ModuleException {
		CurrencyExchangeRate currencyExchangeRate = getCurrencyExchangeRate(currencyCode);
		Currency currency = currencyExchangeRate.getCurrency();

		if (channel != null
				&& ((SalesChannelsUtil.isAppEngineWebOrMobile(channel) && currency.getIBEVisibility() == 0) || (channel == ApplicationEngine.XBE && currency
						.getXBEVisibility() == 0))) {
			currencyExchangeRate = null;
		}

		return currencyExchangeRate;
	}

	public BigDecimal convert(String currencyCode, BigDecimal amount, boolean applyRounding) throws ModuleException {
		if (currencyCode == null || amount == null) {
			throw new ModuleException("webplatform.arg.invalidArgs");
		}
		CurrencyExchangeRate currencyExchangeRate = getCurrencyExchangeRate(currencyCode);
		if (applyRounding) {
			Currency currency = currencyExchangeRate.getCurrency();
			return AccelAeroRounderPolicy.convertAndRound(currencyExchangeRate.getMultiplyingExchangeRate(), amount,
					currency.getBoundryValue(), currency.getBreakPoint());
		} else {
			return AccelAeroCalculator.multiply(currencyExchangeRate.getMultiplyingExchangeRate(), amount);
		}
	}

	public BigDecimal convert(String currencyCode, BigDecimal amount) throws ModuleException {
		return convert(currencyCode, amount, AppSysParamsUtil.isCurrencyRoundupEnabled());
	}

	/**
	 * @return the executionDate
	 */
	public Date getExecutionDate() {
		return executionDate;
	}

	/**
	 * @param executionDate
	 *            the executionDate to set
	 */
	private void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}

	/**
	 * @return the mapPairAmounts
	 */
	public Map<Pair<String, String>, BigDecimal> getMapPairAmounts() {
		return mapPairAmounts;
	}

	/**
	 * @param mapPairAmounts
	 *            the mapPairAmounts to set
	 */
	private void setMapPairAmounts(Map<Pair<String, String>, BigDecimal> mapPairAmounts) {
		this.mapPairAmounts = mapPairAmounts;
	}

	/**
	 * @return the mapCurrencyExchangeRates
	 */
	public Map<String, CurrencyExchangeRate> getMapCurrencyExchangeRates() {
		return mapCurrencyExchangeRates;
	}

	/**
	 * @param mapCurrencyExchangeRates
	 *            the mapCurrencyExchangeRates to set
	 */
	private void setMapCurrencyExchangeRates(Map<String, CurrencyExchangeRate> mapCurrencyExchangeRates) {
		this.mapCurrencyExchangeRates = mapCurrencyExchangeRates;
	}

	/**
	 * @return the mapPurchaseAmounts
	 */
	public Map<String, BigDecimal> getMapPurchaseAmounts() {
		return mapPurchaseAmounts;
	}

	/**
	 * @param mapPurchaseAmounts
	 *            the mapPurchaseAmounts to set
	 */
	private void setMapPurchaseAmounts(Map<String, BigDecimal> mapPurchaseAmounts) {
		this.mapPurchaseAmounts = mapPurchaseAmounts;
	}

}
