/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Aug 04, 2010 17:09:25
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Domain for DashBoard messages Agents
 * 
 * @author : Navod Ediriweera
 * @version : Ver 1.0.0
 * @hibernate.class table = "T_DASHBOARD_AGENT_TYPE"
 */
public class DashbrdMsgAgentType implements Serializable {

	private static final long serialVersionUID = 1L;

	private String agentTypeCode;
	private Integer dashbrdAgentTypeID;
	private String applyStatus;
	private Integer dashbrdMsgID;

	/**
	 * @return Returns the acccId.
	 * @hibernate.id column = "DASHBOARD_AGENT_TYPE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_DASHBOARD_AGENT_TYPE"
	 */
	public Integer getDashbrdAgentTypeID() {
		return dashbrdAgentTypeID;
	}

	public void setDashbrdAgentTypeID(Integer dashbrdAgentTypeID) {
		this.dashbrdAgentTypeID = dashbrdAgentTypeID;
	}

	/**
	 * @hibernate.property column = "DASHBOARD_MESSAGE_ID"
	 * @return
	 */
	public Integer getDashbrdMsgID() {
		return dashbrdMsgID;
	}

	public void setDashbrdMsgID(Integer dashbrdMsgID) {
		this.dashbrdMsgID = dashbrdMsgID;
	}

	/**
	 * @hibernate.property column = "AGENT_TYPE_CODE"
	 * @return
	 */
	public String getAgentTypeCode() {
		return agentTypeCode;
	}

	public void setAgentTypeCode(String agentTypeCode) {
		this.agentTypeCode = agentTypeCode;
	}

	/**
	 * @hibernate.property column = "APPLY_STATUS"
	 * @return
	 */
	public String getApplyStatus() {
		return applyStatus;
	}

	public void setApplyStatus(String applyStatus) {
		this.applyStatus = applyStatus;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		DashbrdMsgAgentType dhObj = (DashbrdMsgAgentType) obj;
		if (dhObj.getAgentTypeCode().equals(this.agentTypeCode) && dhObj.getDashbrdMsgID().equals(this.dashbrdMsgID)
				&& dhObj.getApplyStatus().equals(this.applyStatus))
			return true;
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		HashCodeBuilder bu = new HashCodeBuilder();
		return bu.append(this.agentTypeCode).append(this.applyStatus).append(this.dashbrdMsgID).toHashCode();
	}

}
