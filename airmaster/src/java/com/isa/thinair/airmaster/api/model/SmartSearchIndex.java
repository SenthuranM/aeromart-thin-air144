package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;

/**
 * 
 * @author Pradeep Karunanayake
 * 
 * @hibernate.class table = "T_SMART_SEARCH_INDEX"
 *
 */
public class SmartSearchIndex implements Serializable {

	private static final long serialVersionUID = 1092458702447397500L;

	private String searchIndex;

	private String searchIndexText;

	private Integer routeID;
	
	/**
	 * @hibernate.id column = "SEARCH_INDEX" generator-class = "assigned"
	 */
	public String getSearchIndex() {
		return searchIndex;
	}

	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}

	/**
	 * 
	 * @hibernate.property column = "SEARCH_INDEX_PLAIN_TEXT"
	 */
	public String getSearchIndexText() {
		return searchIndexText;
	}

	public void setSearchIndexText(String searchIndexText) {
		this.searchIndexText = searchIndexText;
	}

	/**
	 * 
	 * @hibernate.property column = "ROUTE_ID"
	 */
	public Integer getRouteID() {
		return routeID;
	}

	public void setRouteID(Integer routeID) {
		this.routeID = routeID;
	}

}
