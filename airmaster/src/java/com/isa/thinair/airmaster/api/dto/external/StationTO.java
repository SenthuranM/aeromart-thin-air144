/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:10
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.dto.external;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * @author : Nilindra Fernando
 * @since : 2.0
 */
public class StationTO extends Tracking {

	private static final long serialVersionUID = -6746783956524516630L;

	private String stationCode;

	private String stationName;

	private int onlineStatus;

	private String status;

	private String territoryCode;

	private String countryCode;

	private String remarks;

	public static final int ONLINE_STATUS_ACTIVE = 1;

	public static final int ONLINE_STATUS_INACTIVE = 0;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	/**
	 * @return Returns the remarks.
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 *            The remarks to set.
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * returns the stationCode
	 * 
	 * @return Returns the aircraftId.
	 */
	public String getStationCode() {
		return stationCode;
	}

	/**
	 * @param stationCode
	 *            The stationCode to set.
	 */
	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}

	/**
	 * @return Returns the stationName.
	 */
	public String getStationName() {
		return stationName;
	}

	/**
	 * @param stationName
	 *            The stationName to set.
	 */
	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the territoryCode.
	 */
	public String getTerritoryCode() {
		return territoryCode;
	}

	/**
	 * @param territoryCode
	 *            The territoryCode to set.
	 */
	public void setTerritoryCode(String territoryCode) {
		this.territoryCode = territoryCode;
	}

	/**
	 * @return Returns the onlineStatus.
	 */
	public int getOnlineStatus() {
		return onlineStatus;
	}

	/**
	 * @param onlineStatus
	 *            The onlineStatus to set.
	 */
	public void setOnlineStatus(int onlineStatus) {
		this.onlineStatus = onlineStatus;
	}

	/**
	 * @return Returns the countryCode.
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param countryCode
	 *            The countryCode to set.
	 * 
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
}
