/**
 * 
 */
package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;
import java.util.Collection;

import com.isa.thinair.airmaster.api.model.AirCraftModelSeats;
import com.isa.thinair.airmaster.api.model.AircraftModel;

/**
 * @author indika
 * 
 */
public class AirCraftModelSeatsDTO implements Serializable {

	private static final long serialVersionUID = 1873265933893640081L;

	private int totalseats;

	private int totalRows;

	private int totalColumns;

	private AircraftModel aircraftModel;

	Collection<AirCraftModelSeats> airCraftModelSeats;

	/**
	 * @return Returns the airCraftModelSeats.
	 */
	public Collection<AirCraftModelSeats> getAirCraftModelSeats() {
		return airCraftModelSeats;
	}

	/**
	 * @param airCraftModelSeats
	 *            The airCraftModelSeats to set.
	 */
	public void setAirCraftModelSeats(Collection<AirCraftModelSeats> airCraftModelSeats) {
		this.airCraftModelSeats = airCraftModelSeats;
	}

	/**
	 * @return Returns the totalColumns.
	 */
	public int getTotalColumns() {
		return totalColumns;
	}

	/**
	 * @param totalColumns
	 *            The totalColumns to set.
	 */
	public void setTotalColumns(int totalColumns) {
		this.totalColumns = totalColumns;
	}

	/**
	 * @return Returns the totalRows.
	 */
	public int getTotalRows() {
		return totalRows;
	}

	/**
	 * @param totalRows
	 *            The totalRows to set.
	 */
	public void setTotalRows(int totalRows) {
		this.totalRows = totalRows;
	}

	/**
	 * @return Returns the totalseats.
	 */
	public int getTotalseats() {
		return totalseats;
	}

	/**
	 * @param totalseats
	 *            The totalseats to set.
	 */
	public void setTotalseats(int totalseats) {
		this.totalseats = totalseats;
	}

	public AircraftModel getAircraftModel() {
		return aircraftModel;
	}

	public void setAircraftModel(AircraftModel aircraftModel) {
		this.aircraftModel = aircraftModel;
	}

}
