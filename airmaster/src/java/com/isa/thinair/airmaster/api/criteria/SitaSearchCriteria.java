/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:16:12
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.criteria;

import java.io.Serializable;

public class SitaSearchCriteria implements Serializable {

	private static final long serialVersionUID = 9075591959070772620L;
	private String airportId;
	private String activeStatus;
	private String sitaAddress;
	private String carrierCode;

	/**
	 * @return Returns the activeStatus.
	 */
	public String getActiveStatus() {
		return activeStatus;
	}

	/**
	 * @param activeStatus
	 *            The activeStatus to set.
	 */
	public void setActiveStatus(String activeStatus) {
		this.activeStatus = activeStatus;
	}

	/**
	 * @return Returns the sitaAddress.
	 */
	public String getSitaAddress() {
		return sitaAddress;
	}

	/**
	 * @param sitaAddress
	 *            The sitaAddress to set.
	 */
	public void setSitaAddress(String sitaAddress) {
		this.sitaAddress = sitaAddress;
	}

	/**
	 * @return Returns the airportId.
	 */
	public String getAirportId() {
		return airportId;
	}

	/**
	 * @param airportId
	 *            The airportId to set.
	 */
	public void setAirportId(String airportId) {
		this.airportId = airportId;
	}

	/**
	 * @return Returns the carrier codes
	 */
	public String getCarrierCode() {
		return carrierCode;
	}

	/**
	 * @param carrier
	 *            code The carrier codes to set.
	 */
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

}
