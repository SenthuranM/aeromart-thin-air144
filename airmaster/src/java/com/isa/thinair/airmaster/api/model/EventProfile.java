package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;

public class EventProfile  implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Event event;
	
	private String topicName;

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

}
