/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:28:24
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_SITA_ADDRESS"
 * 
 *                  Airport is the entity class to repesent a Airport model
 * 
 */
public class SITAAddress extends Persistent {

	/*
	 * public static final String STATUS_ACTIVE = "ACT";
	 * 
	 * public static final String STATUS_INACTIVE = "INA";
	 */

	private static final long serialVersionUID = 6372816569966199559L;

	public static final String ACTIVE_STATUS_YES = "ACT";

	public static final String ACTIVE_STATUS_NO = "INA";

	public static final String SITA_MESSAGE_VIA_MCONNECT = "M-CONNECT";

	public static final String SITA_MESSAGE_VIA_SITATEX = "SITATEX";
	
	public static final String SITA_MESSAGE_VIA_ARINC = "ARINC";

	/*
	 * public static final String ADDRESS_TYPE_PRIMARY = "P";
	 * 
	 * public static final String ADDRESS_TYPE_SECONDARY = "S";
	 */

	private int airportSITAId;
	private String airportCode;
	private String sitaAddress;
	private String sitaLocation;
	private String activeStatus;
	private String emailId;
	private Set<String> carriercodes;
	private String sitaDeliveryMethod;
	private boolean allowForPNLADL;
	private boolean allowForPALCAL;
	private Set<String> ondCodes;
	private Set<String> flightNumbers;

	/**
	 * returns the airportName
	 * 
	 * @return Returns the airportName.
	 * 
	 * @hibernate.property column = "SITA_ADDRESS"
	 */
	public String getSitaAddress() {
		return sitaAddress;
	}

	/**
	 * @param sitaAddress
	 *            The sitaAddress to set.
	 */
	public void setSitaAddress(String sitaAddress) {
		this.sitaAddress = sitaAddress;
	}

	/**
	 * returns the airportName
	 * 
	 * @return Returns the airportName.
	 * 
	 * @hibernate.property column = "SITA_LOCATION"
	 */
	public String getSitaLocation() {
		return sitaLocation;
	}

	/**
	 * @param sitaLocation
	 *            The sitaLocation to set.
	 */
	public void setSitaLocation(String sitaLocation) {
		this.sitaLocation = sitaLocation;
	}

	/**
	 * returns the airportName
	 * 
	 * @return Returns the airportName.
	 * 
	 * @hibernate.property column = "AIRPORT_CODE"
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            The airportCode to set.
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * 
	 * @return Returns the airportSITAId.
	 * @hibernate.id column = "AIRPORT_SITA_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_SITA_ADDRESS"
	 */
	public int getAirportSITAId() {
		return airportSITAId;
	}

	/**
	 * @param airportSITAId
	 *            The airportSITAId to set.
	 */
	public void setAirportSITAId(int airportSITAId) {
		this.airportSITAId = airportSITAId;
	}

	/**
	 * @return Returns the activeStatus.
	 * 
	 * @return Returns the airportName.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getActiveStatus() {
		return activeStatus;
	}

	/**
	 * @param activeStatus
	 *            The activeStatus to set.
	 */
	public void setActiveStatus(String activeStatus) {
		this.activeStatus = activeStatus;
	}

	/**
	 * @return Returns the emailId.
	 * @hibernate.property column = "EMAIL_ID"
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId
	 *            The emailId to set.
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @hibernate.set table="T_SITA_ADDRESS_CARRIER" cascade="all"
	 * @hibernate.collection-key column="AIRPORT_SITA_ID"
	 * @hibernate.collection-element type="string" column="carrier_code"
	 */
	public Set<String> getCarriercodes() {
		return this.carriercodes;
	}

	/**
	 * Add a Carrier Code
	 * 
	 * @param carrierCode
	 */
	public void addCarriercode(String carrier) {
		if (this.getCarriercodes() == null) {
			this.setCarriercodes(new HashSet<String>());
		}

		this.getCarriercodes().add(carrier);
	}

	/**
	 * 
	 * @param carrierCode
	 *            The carrier code to set.
	 */
	public void setCarriercodes(Set<String> set) {
		this.carriercodes = set;
	}

	/**
	 * @return
	 * @hibernate.property column = "SITA_DELIVERY_METHOD"
	 */
	public String getSitaDeliveryMethod() {
		return sitaDeliveryMethod;
	}

	public void setSitaDeliveryMethod(String sitaDeliveryMethod) {
		this.sitaDeliveryMethod = sitaDeliveryMethod;
	}

	/**
	 * @return the allowForPNLADL
	 * @hibernate.property column = "SEND_PNL_ADL" type = "yes_no"
	 */
	public boolean isAllowForPNLADL() {
		return allowForPNLADL;
	}

	/**
	 * @param allowForPNLADL the allowForPNLADL to set
	 */
	public void setAllowForPNLADL(boolean allowForPNLADL) {
		this.allowForPNLADL = allowForPNLADL;
	}

	/**
	 * @return the allowForPALCAL
	 * @hibernate.property column = "SEND_PAL_CAL" type = "yes_no"
	 */
	public boolean isAllowForPALCAL() {
		return allowForPALCAL;
	}

	/**
	 * @param allowForPALCAL the allowForPALCAL to set
	 */
	public void setAllowForPALCAL(boolean allowForPALCAL) {
		this.allowForPALCAL = allowForPALCAL;
	}

	/**
	 * @hibernate.set table="T_SITA_ADDRESS_OND" cascade="all"
	 * @hibernate.collection-key column="AIRPORT_SITA_ID"
	 * @hibernate.collection-element type="string" column="ond_code"
	 */
	public Set<String> getOndCodes() {
		return ondCodes;
	}

	/**
	 * 
	 * @param ondCodes
	 *            The ond codes to set.
	 */
	public void setOndCodes(Set<String> ondCodes) {
		this.ondCodes = ondCodes;
	}

	/**
	 * @hibernate.set table="T_SITA_ADDRESS_FLIGHT_NUM" cascade="all"
	 * @hibernate.collection-key column="AIRPORT_SITA_ID"
	 * @hibernate.collection-element type="string" column="flight_number"
	 */
	public Set<String> getFlightNumbers() {
		return flightNumbers;
	}

	/**
	 * 
	 * @param flightNumbers
	 *            The flight numbers to set.
	 */
	public void setFlightNumbers(Set<String> flightNumbers) {
		this.flightNumbers = flightNumbers;
	}
	
}
