package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ExchangeRateInfoDTO implements Serializable {

	private static final long serialVersionUID = -5879185184182696328L;

	private String currencyCode;

	private String currencyName;

	private BigDecimal exchangeRate;

	private Date dateOfQuote;

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public Date getDateOfQuote() {
		return dateOfQuote;
	}

	public void setDateOfQuote(Date dateOfQuote) {
		this.dateOfQuote = dateOfQuote;
	}

}
