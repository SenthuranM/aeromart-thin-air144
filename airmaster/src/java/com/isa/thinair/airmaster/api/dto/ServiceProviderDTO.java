package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;

public class ServiceProviderDTO implements Serializable {

	private static final long serialVersionUID = 5831005290395635258L;

	private Integer airportTransferId;
	private String name;
	private String number;
	private String address;
	private String emailId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Integer getAirportTransferId() {
		return airportTransferId;
	}

	public void setAirportTransferId(Integer airportTransferId) {
		this.airportTransferId = airportTransferId;
	}

}
