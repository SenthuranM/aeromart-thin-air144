package com.isa.thinair.airmaster.api.model;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @author : M.Rikaz
 * @hibernate.class table = "T_SSR_INFO"
 * 
 */

public class SSR extends Tracking {

	private static final long serialVersionUID = 3182130582757508269L;

	public interface Code {
		String HAJ = "HAJ";
		String MAAS = "MAAS";
		String LGFC = "LGFC";
		String LGBC = "LGBC";
	}

	private int ssrId;

	private String ssrCode;

	private String ssrDesc;

	private String ssrName;

	private int ssrCatId;

	private double defaultCharge;

	private String status;

	private Set<Integer> visibleChannelIds;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	private SSRSubCategory ssrSubCategory;

	private String showInIBE;

	private String showInXBE;
	
	private String showInGDS;
	
	private String showInWs;

	private Set<SSRConstraints> ssrConstraintsSet;

	private I18nMessageKey i18nMessageKey;

	private int sortOrder;
	
	private String ssrCutoffTime;

	private boolean validForPALCAL;
	
	private boolean userDefinedSSR;

	/**
	 * @return Returns the i18nMessageKey.
	 * @hibernate.many-to-one lazy="false" column = "I18N_MESSAGE_KEY"
	 *                        class="com.isa.thinair.airmaster.api.model.I18nMessageKey" not-found="ignore"
	 *                        cascade="save-update"
	 * 
	 */
	public I18nMessageKey getI18nMessageKey() {
		return i18nMessageKey;
	}

	/**
	 * @param i18nMessageKey
	 */
	public void setI18nMessageKey(I18nMessageKey i18nMessageKey) {
		this.i18nMessageKey = i18nMessageKey;
	}

	/**
	 * @return Returns the ssrConstraintsSet.
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="SSR_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airmaster.api.model.SSRConstraints"
	 */
	public Set<SSRConstraints> getSsrConstraintsSet() {
		return ssrConstraintsSet;
	}

	public void setSsrConstraintsSet(Set<SSRConstraints> ssrConstraintsSet) {
		this.ssrConstraintsSet = ssrConstraintsSet;
	}

	public SSRConstraints getSSRConstraints() {
		SSRConstraints r = new SSRConstraints();
		if (getSsrConstraintsSet() != null) {
			Iterator<SSRConstraints> i = getSsrConstraintsSet().iterator();
			if (i.hasNext()) {
				r = (SSRConstraints) i.next();
			}
		}
		return r;
	}

	public void setSSRConstraints(SSRConstraints ssrConstraints) {
		setSsrConstraintsSet(new HashSet<SSRConstraints>());
		ssrConstraints.setSsrInfo(this);
		this.ssrConstraintsSet.add(ssrConstraints);
	}

	/**
	 * 
	 * @hibernate.property column = "SHOW_IN_IBE"
	 */
	public String getShowInIBE() {
		return showInIBE;
	}

	public void setShowInIBE(String showInIBE) {
		this.showInIBE = showInIBE;
	}

	/**
	 * 
	 * @hibernate.property column = "SHOW_IN_XBE"
	 */
	public String getShowInXBE() {
		return showInXBE;
	}

	public void setShowInXBE(String showInXBE) {
		this.showInXBE = showInXBE;
	}

	/**
	 * 
	 * @hibernate.property column = "SHOW_IN_GDS"
	 */
	public String getShowInGDS() {
		return showInGDS;
	}

	/**
	 * @param showInGDS the showInGDS to set
	 */
	public void setShowInGDS(String showInGDS) {
		this.showInGDS = showInGDS;
	}

	/**
	 * returns the ssrId
	 * 
	 * @return Returns the ssrId.
	 * 
	 * @hibernate.id column="SSR_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_SSR_INFO"
	 */
	public int getSsrId() {
		return ssrId;
	}

	/**
	 * sets the ssrId
	 * 
	 * @param ssrId
	 *            The ssrId to set.
	 */
	public void setSsrId(int ssrId) {
		this.ssrId = ssrId;
	}

	/**
	 * returns the ssrCode
	 * 
	 * @return Returns the ssrCode.
	 * 
	 * @hibernate.property column = "SSR_CODE"
	 */
	public String getSsrCode() {
		return ssrCode;
	}

	/**
	 * sets the ssrCode
	 * 
	 * @param ssrCode
	 *            The ssrCode to set.
	 */
	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	/**
	 * returns the ssrDesc
	 * 
	 * @return Returns the ssrDesc.
	 * 
	 * @hibernate.property column = "SSR_DESC"
	 */
	public String getSsrDesc() {
		return ssrDesc;
	}

	/**
	 * sets the ssrDesc
	 * 
	 * @param ssrDesc
	 *            The ssrDesc to set.
	 */
	public void setSsrDesc(String ssrDesc) {
		this.ssrDesc = ssrDesc;
	}

	/**
	 * returns the ssrName
	 * 
	 * @return Returns the ssrName.
	 * 
	 * @hibernate.property column = "SSR_NAME"
	 */
	public String getSsrName() {
		return ssrName;
	}

	/**
	 * sets the ssrName
	 * 
	 * @param ssrName
	 *            The ssrName to set.
	 */
	public void setSsrName(String ssrName) {
		this.ssrName = ssrName;
	}

	/**
	 * returns the defaultCharge
	 * 
	 * @return Returns the defaultCharge.
	 * 
	 */
	public double getDefaultCharge() {
		return defaultCharge;
	}

	/**
	 * sets the defaultCharge
	 * 
	 * @param defaultCharge
	 *            The defaultCharge to set.
	 */
	public void setDefaultCharge(double defaultCharge) {
		this.defaultCharge = defaultCharge;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the visibleChannelIds.
	 * 
	 * @hibernate.set table="T_SSR_VISIBLE_CHANNEL" cascade="all"
	 * @hibernate.collection-key column="SSR_ID"
	 * @hibernate.collection-element type="integer" column="SALES_CHANNEL_CODE"
	 * 
	 */
	public Set<Integer> getVisibleChannelIds() {
		return visibleChannelIds;
	}

	/**
	 * @param visibleChannelIds
	 *            The visibleChannelIds to set.
	 */
	public void setVisibleChannelIds(Set<Integer> visibleChannelIds) {
		this.visibleChannelIds = visibleChannelIds;
	}

	/**
	 * @return Returns the ssrSubCategory.
	 * @hibernate.many-to-one column = "SSR_SUB_CAT_ID" class="com.isa.thinair.airmaster.api.model.SSRSubCategory"
	 */
	public SSRSubCategory getSsrSubCategory() {
		return ssrSubCategory;
	}

	public void setSsrSubCategory(SSRSubCategory ssrSubCategory) {
		this.ssrSubCategory = ssrSubCategory;
	}

	public String toString() {
		return "SSR [defaultCharge=" + defaultCharge + ", ssrCatId=" + ssrSubCategory + ", ssrCode=" + ssrCode + ", ssrDesc="
				+ ssrDesc + ", ssrId=" + ssrId + ", status=" + status + ", visibleChannelIds=" + visibleChannelIds + "]";
	}

	public int getSsrCatId() {
		return ssrCatId;
	}

	public void setSsrCatId(int ssrCatId) {
		this.ssrCatId = ssrCatId;
	}

	/**
	 * @return the sortOrder
	 * 
	 * @hibernate.property column = "SORT_ORDER"
	 * 
	 */
	public int getSortOrder() {
		return sortOrder;
	}

	/**
	 * @param sortOrder
	 *            the sortOrder to set
	 */
	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	/**
	 * @return
	 * @hibernate.property column = "SSR_CUTOFF_TIME"
	 */
	public String getSsrCutoffTime() {
		return ssrCutoffTime;
	}

	public void setSsrCutoffTime(String ssrCutoffTime) {
		this.ssrCutoffTime = ssrCutoffTime;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "SHOW_IN_WS"
	 */
	public String getShowInWs() {
		return showInWs;
	}

	public void setShowInWs(String showInWs) {
		this.showInWs = showInWs;
	}

	/**
	 * @return the validForPALCAL
	 * @hibernate.property column = "VALID_FOR_PALCAL" type = "yes_no"
	 */
	public boolean isValidForPALCAL() {
		return validForPALCAL;
	}

	/**
	 * @param validForPALCAL the validForPALCAL to set
	 */
	public void setValidForPALCAL(boolean validForPALCAL) {
		this.validForPALCAL = validForPALCAL;
	}

	/**
	 * @return the userDefinedSSR
	 * @hibernate.property column = "USER_DEFINED_SSR" type = "yes_no"
	 */
	public boolean isUserDefinedSSR() {
		return userDefinedSSR;
	}

	/**
	 * @param userDefinedSSR the userDefinedSSR to set
	 */
	public void setUserDefinedSSR(boolean userDefinedSSR) {
		this.userDefinedSSR = userDefinedSSR;
	}
	
}
