package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author asiri
 * 
 */

public class MealCategoryDTO implements Serializable {

	private static final long serialVersionUID = 4785756984266299854L;
	private Integer mealCategoryId;
	private String mealCategoryName;

	private String createdBy;
	private Date createdDate;
	private String modifiedBy;
	private Date modifiedDate;
	private long version;

	private String languageList;
	private String selectedLanguageTranslations;

	public String getLanguageList() {
		return languageList;
	}

	public void setLanguageList(String languageList) {
		this.languageList = languageList;
	}

	public String getSelectedLanguageTranslations() {
		return selectedLanguageTranslations;
	}

	public void setSelectedLanguageTranslations(String selectedLanguageTranslations) {
		this.selectedLanguageTranslations = selectedLanguageTranslations;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public Integer getMealCategoryId() {
		return mealCategoryId;
	}

	public void setMealCategoryId(Integer mealCategoryId) {
		this.mealCategoryId = mealCategoryId;
	}

	public String getMealCategoryName() {
		return mealCategoryName;
	}

	public void setMealCategoryName(String mealCategoryName) {
		this.mealCategoryName = mealCategoryName;
	}

	/** The i18n message key used for translations. */
	// private String i18nMessageKey;

}
