package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;

/**
 * 
 * @author rumesh
 * 
 */

public class SSRTemplateSearchCriteria implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer templateId;
	private String status;

	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
