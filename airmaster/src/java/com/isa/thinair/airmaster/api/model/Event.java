package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;
import java.util.Map;

public class Event implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String eventName;
	
	private String eventCategory;
	
	private Map<String, String> eventData;

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventCategory() {
		return eventCategory;
	}

	public void setEventCategory(String eventCategory) {
		this.eventCategory = eventCategory;
	}

	public Map<String, String> getEventData() {
		return eventData;
	}

	public void setEventData(Map<String, String> eventData) {
		this.eventData = eventData;
	}	

}
