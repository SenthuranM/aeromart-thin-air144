package com.isa.thinair.airmaster.api.util;

public class EventConstants {

	public static String TOPIC_NAME = "cache_eviction";

	public interface Delimeters {
		String SEPARATOR = "#";
	}

	public interface EventCategory {
		String INVENTORY = "inventory";
	}

	public interface EventName {
		String INVENTORY_MOVEMENT = "inventory_movement";
	}

	public interface EventData {
		String ORIGIN = "origin";
		String DESTINATION = "destination";
		String FLIGHT_NUMBER = "flightNumber";
		String LOCAL_FLIGHT_DATE = "localDepartureDate";
	}

}
