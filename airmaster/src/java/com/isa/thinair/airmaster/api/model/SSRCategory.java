package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;

/**
 * 
 * @author : M.Rikaz
 * @hibernate.class table = "T_SSR_CATEGORY" lazy="false"
 * 
 */

public class SSRCategory implements Serializable {

	public static final Integer ID_INFLIGHT_SERVICE = new Integer("1");
	public static final Integer ID_AIRPORT_SERVICE = new Integer("2");
	public static final Integer ID_AIRPORT_TRANSFER = new Integer("3");

	public static final String NOTIFICATION_EMAIL_SENT = "Y";
	public static final String NOTIFICATION_EMAIL_NOT_SENT = "N";

	private static final long serialVersionUID = 8718956196375999768L;

	private Integer catId;

	private String description;

	private String status;

	private Integer bookCutMins;

	private Integer addNotStartCutMins;

	private Integer addNotEndCutMins;

	private String sendInPnlAdl;

	private String sendInEmail;

	private Integer cancelNotStartCutMins;

	private Integer cancelNotEndCutMins;

	private Integer notifyAttempts;

	private Integer notifyStartCutMins;

	private Integer notifyEndCutMins;

	/**
	 * @return Returns the catId.
	 * @hibernate.id column = "SSR_CAT_ID" generator-class = "native"
	 */
	public Integer getCatId() {
		return catId;
	}

	public void setCatId(Integer catId) {
		this.catId = catId;
	}

	/**
	 * @return Returns the description.
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the bookCutMins.
	 * @hibernate.property column = "BOOKING_CUTOVER_IN_MINS"
	 */
	public Integer getBookCutMins() {
		return bookCutMins;
	}

	public void setBookCutMins(Integer bookCutMins) {
		this.bookCutMins = bookCutMins;
	}

	/**
	 * @return Returns the addNotStartCutMins.
	 * @hibernate.property column = "ADD_NOTIFY_START_CUTOVER_MINS"
	 */
	public Integer getAddNotStartCutMins() {
		return addNotStartCutMins;
	}

	public void setAddNotStartCutMins(Integer addNotStartCutMins) {
		this.addNotStartCutMins = addNotStartCutMins;
	}

	/**
	 * @return Returns the addNotEndCutMins.
	 * @hibernate.property column = "ADD_NOTIFY_END_CUTOVER_MINS"
	 */
	public Integer getAddNotEndCutMins() {
		return addNotEndCutMins;
	}

	public void setAddNotEndCutMins(Integer addNotEndCutMins) {
		this.addNotEndCutMins = addNotEndCutMins;
	}

	/**
	 * @return Returns the sendInPnlAdl.
	 * @hibernate.property column = "SEND_IN_PNL_ADL"
	 */
	public String getSendInPnlAdl() {
		return sendInPnlAdl;
	}

	public void setSendInPnlAdl(String sendInPnlAdl) {
		this.sendInPnlAdl = sendInPnlAdl;
	}

	/**
	 * @return Returns the sendInEmail.
	 * @hibernate.property column = "SEND_IN_EMAIL"
	 */
	public String getSendInEmail() {
		return sendInEmail;
	}

	public void setSendInEmail(String sendInEmail) {
		this.sendInEmail = sendInEmail;
	}

	/**
	 * @return Returns the cancelNotStartCutMins.
	 * @hibernate.property column = "CNX_NOTIFY_START_CUTOVER_MINS"
	 */
	public Integer getCancelNotStartCutMins() {
		return cancelNotStartCutMins;
	}

	public void setCancelNotStartCutMins(Integer cancelNotStartCutMins) {
		this.cancelNotStartCutMins = cancelNotStartCutMins;
	}

	/**
	 * @return Returns the cancelNotEndCutMins.
	 * @hibernate.property column = "CNX_NOTIFY_END_CUTOVER_MINS"
	 */
	public Integer getCancelNotEndCutMins() {
		return cancelNotEndCutMins;
	}

	public void setCancelNotEndCutMins(Integer cancelNotEndCutMins) {
		this.cancelNotEndCutMins = cancelNotEndCutMins;
	}

	/**
	 * @return Returns the notifyAttempts.
	 * @hibernate.property column = "NOTFICATION_ATTEMPTS"
	 */
	public Integer getNotifyAttempts() {
		return notifyAttempts;
	}

	public void setNotifyAttempts(Integer notifyAttempts) {
		this.notifyAttempts = notifyAttempts;
	}

	/**
	 * @return Returns the notifyStartCutMins.
	 * @hibernate.property column = "NOTIFY_START_CUTOVER_MINS"
	 */
	public Integer getNotifyStartCutMins() {
		return notifyStartCutMins;
	}

	public void setNotifyStartCutMins(Integer notifyStartCutMins) {
		this.notifyStartCutMins = notifyStartCutMins;
	}

	/**
	 * @return Returns the notifyEndCutMins.
	 * @hibernate.property column = "NOTIFY_STOP_CUTOVER_MINS"
	 */
	public Integer getNotifyEndCutMins() {
		return notifyEndCutMins;
	}

	public void setNotifyEndCutMins(Integer notifyEndCutMins) {
		this.notifyEndCutMins = notifyEndCutMins;
	}

}
