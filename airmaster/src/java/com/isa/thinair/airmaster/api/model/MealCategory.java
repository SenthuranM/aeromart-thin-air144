package com.isa.thinair.airmaster.api.model;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @hibernate.class table = "ML_T_MEAL_CATEGORY"
 * 
 *                  Entity class to represent the meal categories.
 * 
 * @author : sanjaya
 * 
 */

public class MealCategory extends Tracking {

	private static final long serialVersionUID = 2253080140661019657L;

	private Integer mealCategoryId;

	private String mealCategoryName;

	/** The i18n message key used for translations. */
	private I18nMessageKey i18nMessageKey;

	/**
	 * @return Returns the i18nMessageKey.
	 * @hibernate.many-to-one lazy="false" column = "I18N_MESSAGE_KEY"
	 *                        class="com.isa.thinair.airmaster.api.model.I18nMessageKey" not-found="ignore"
	 *                        cascade="save-update"
	 * 
	 */
	public I18nMessageKey getI18nMessageKey() {
		return i18nMessageKey;
	}

	/**
	 * @param i18nMessageKey
	 */
	public void setI18nMessageKey(I18nMessageKey i18nMessageKey) {
		this.i18nMessageKey = i18nMessageKey;
	}

	/**
	 * @hibernate.id column="MEAL_CATEGORY_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "ML_S_MEAL_CATEGORY"
	 * 
	 * @return The meal category id.
	 */
	public Integer getMealCategoryId() {
		return mealCategoryId;
	}

	/**
	 * @param mealCategoryId
	 *            : The meal category id to set.
	 */
	public void setMealCategoryId(Integer mealCategoryId) {
		this.mealCategoryId = mealCategoryId;
	}

	/**
	 * @hibernate.property column = "MEAL_CATEGORY"
	 * 
	 * @return : The meal category name.
	 */
	public String getMealCategoryName() {
		return mealCategoryName;
	}

	/**
	 * 
	 * @param mealCategoryName
	 *            : The meal category name to set.
	 */
	public void setMealCategoryName(String mealCategoryName) {
		this.mealCategoryName = mealCategoryName;
	}

}
