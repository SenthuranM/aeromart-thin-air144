/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:10
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import com.isa.thinair.airmaster.api.dto.external.StationTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Tracking;
import com.isa.thinair.commons.core.util.Util;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_STATION"
 * 
 *                  Aircraft is the entity class to repesent a Aircraft model
 * 
 */
public class Station extends Tracking {

	private static final long serialVersionUID = 4054634866764702118L;

	private String stationCode;

	private String stationName;

	private int onlineStatus;

	private String status;

	private String territoryCode;

	private String countryCode;

	private String remarks;

	private String contact;

	private String telephone;

	private String fax;

	private String nameChangeThresholdTime;

	private String lccPublishStatus;
	
	private Integer stateId;

	public static final int ONLINE_STATUS_ACTIVE = 1;

	public static final int ONLINE_STATUS_INACTIVE = 0;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	/**
	 * @return Returns the remarks.
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 *            The remarks to set.
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * returns the stationCode
	 * 
	 * @return Returns the aircraftId.
	 * 
	 * @hibernate.id column = "STATION_CODE" generator-class="assigned"
	 * 
	 */
	public String getStationCode() {
		return stationCode.trim();
	}

	/**
	 * @param stationCode
	 *            The stationCode to set.
	 */
	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}

	/**
	 * @return Returns the stationName.
	 * @hibernate.property column = "STATION_NAME"
	 */
	public String getStationName() {
		return stationName;
	}

	/**
	 * @param stationName
	 *            The stationName to set.
	 */
	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the territoryCode.
	 * @hibernate.property column = "TERRITORY_CODE"
	 */
	public String getTerritoryCode() {
		return territoryCode;
	}

	/**
	 * @param territoryCode
	 *            The territoryCode to set.
	 */
	public void setTerritoryCode(String territoryCode) {
		this.territoryCode = territoryCode;
	}

	/**
	 * @return Returns the onlineStatus.
	 * @hibernate.property column = "ONLINE_STATUS"
	 */
	public int getOnlineStatus() {
		return onlineStatus;
	}

	/**
	 * @param onlineStatus
	 *            The onlineStatus to set.
	 */
	public void setOnlineStatus(int onlineStatus) {
		this.onlineStatus = onlineStatus;
	}

	/**
	 * @return Returns the countryCode.
	 * @hibernate.property column = "COUNTRY_CODE"
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param countryCode
	 *            The countryCode to set.
	 * 
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * Creates StationTO object
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public StationTO toStationTO() throws ModuleException {
		StationTO stationTO = new StationTO();
		Util.copyProperties(stationTO, this);

		return stationTO;
	}

	/**
	 * @return Returns the contact.
	 * @hibernate.property column = "CONTACT"
	 */
	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	/**
	 * @return Returns the telephone.
	 * @hibernate.property column = "TELEPHONE"
	 */
	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * @return Returns the fax.
	 * @hibernate.property column = "FAX"
	 */
	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * @return
	 * @hibernate.property column = "THRESHOLD_TIME"
	 */
	public String getNameChangeThresholdTime() {
		return nameChangeThresholdTime;
	}

	public void setNameChangeThresholdTime(String thresholdTime) {
		this.nameChangeThresholdTime = thresholdTime;
	}

	/**
	 * @hibernate.property column = "LCC_PUBLISH_STATUS"
	 */
	public String getLccPublishStatus() {
		return lccPublishStatus;
	}

	/**
	 * @param lccPublishStatus
	 *            the lccPublishStatus to set
	 */

	public void setLccPublishStatus(String lccPublishStatus) {
		this.lccPublishStatus = lccPublishStatus.intern();
	}

	/**
	 * @hibernate.property column = "STATE_ID"
	 */
	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

}
