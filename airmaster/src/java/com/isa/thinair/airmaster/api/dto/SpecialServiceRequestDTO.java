package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;

public class SpecialServiceRequestDTO implements Serializable {

	private static final long serialVersionUID = 3840204517435641944L;
	private Long ssrID;
	private Long ssrChargeID;
	private String ssrCode;
	private String ssrName;
	private String description;
	private String ondCode;
	private String cabinClass;
	private String chargeAmount;
	private Integer availableQty;
	private String decriptionSelectedLanguage;
	private String status;
	private String cutOffTime;

	public String getSsrCode() {
		return ssrCode;
	}

	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	public String getSsrName() {
		return ssrName;
	}

	public void setSsrName(String ssrName) {
		this.ssrName = ssrName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public String getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(String chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public Long getSsrID() {
		return ssrID;
	}

	public void setSsrID(Long ssrID) {
		this.ssrID = ssrID;
	}

	public Long getSsrChargeID() {
		return ssrChargeID;
	}

	public void setSsrChargeID(Long ssrChargeID) {
		this.ssrChargeID = ssrChargeID;
	}

	public Integer getAvailableQty() {
		return availableQty;
	}

	public void setAvailableQty(Integer availableQty) {
		this.availableQty = availableQty;
	}

	public String getDecriptionSelectedLanguage() {
		return decriptionSelectedLanguage;
	}

	public void setDecriptionSelectedLanguage(String decriptionSelectedLanguage) {
		this.decriptionSelectedLanguage = decriptionSelectedLanguage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SpecialServiceRequestDTO [cabinClass=");
		builder.append(cabinClass);
		builder.append(", chargeAmount=");
		builder.append(chargeAmount);
		builder.append(", description=");
		builder.append(description);
		builder.append(", ondCode=");
		builder.append(ondCode);
		builder.append(", ssrCode=");
		builder.append(ssrCode);
		builder.append(", ssrName=");
		builder.append(ssrName);
		builder.append(", ssrID=");
		builder.append(ssrID);
		builder.append(", availableQty = ");
		builder.append(availableQty);
		builder.append(", status = ");
		builder.append(status);
		builder.append("]");
		return builder.toString();
	}

	public String getCutOffTime() {
		return cutOffTime;
	}

	public void setCutOffTime(String cutOffTime) {
		this.cutOffTime = cutOffTime;
	}
}
