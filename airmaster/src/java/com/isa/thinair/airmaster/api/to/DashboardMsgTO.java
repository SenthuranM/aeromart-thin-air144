package com.isa.thinair.airmaster.api.to;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airmaster.api.model.DashboardMessage;
import com.isa.thinair.airmaster.api.model.DashbrdMsgAgent;
import com.isa.thinair.airmaster.api.model.DashbrdMsgAgentPOS;
import com.isa.thinair.airmaster.api.model.DashbrdMsgAgentType;
import com.isa.thinair.airmaster.api.model.DashbrdMsgUser;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * Transfer Object to hold and convert data from the Front end to Domain Object
 * 
 * @author Navod Ediriweera
 * @since 04Aug2010
 */
public class DashboardMsgTO implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String INCLUDE_STR = "Y";
	public static final String EXCLUDE_STR = "N";

	private Integer messageID;
	private String messageContent;
	private String messageType;

	private Integer priority;
	private String fromDate;
	private String toDate;
	private String version;

	private String radUserSelection;

	private String selectedAgents;
	private String selectedAgentTypes;
	private String selectedPOS;
	private String userList;

	private String agentInclusionStatus;
	private String userInclusionStatus;
	private String agentTypeInclusionStatus;
	private String posInclusionStatus;

	private String newMessageExist;

	private String status;

	public String getSelectedAgents() {
		return selectedAgents;
	}

	public void setSelectedAgents(String selectedAgents) {
		this.selectedAgents = selectedAgents;
	}

	public String getSelectedAgentTypes() {
		return selectedAgentTypes;
	}

	public void setSelectedAgentTypes(String selectedAgentTypes) {
		this.selectedAgentTypes = selectedAgentTypes;
	}

	public String getSelectedPOS() {
		return selectedPOS;
	}

	public void setSelectedPOS(String selectedPOS) {
		this.selectedPOS = selectedPOS;
	}

	public String getAgentInclusionStatus() {
		return agentInclusionStatus;
	}

	public void setAgentInclusionStatus(String agentInclusionStatus) {
		this.agentInclusionStatus = agentInclusionStatus;
	}

	public String getUserInclusionStatus() {
		return userInclusionStatus;
	}

	public void setUserInclusionStatus(String userInclusionStatus) {
		this.userInclusionStatus = userInclusionStatus;
	}

	public String getAgentTypeInclusionStatus() {
		return agentTypeInclusionStatus;
	}

	public void setAgentTypeInclusionStatus(String agentTypeInclusionStatus) {
		this.agentTypeInclusionStatus = agentTypeInclusionStatus;
	}

	public String getPosInclusionStatus() {
		return posInclusionStatus;
	}

	public void setPosInclusionStatus(String posInclusionStatus) {
		this.posInclusionStatus = posInclusionStatus;
	}

	public String getRadUserSelection() {
		return radUserSelection;
	}

	public void setRadUserSelection(String radUserSelection) {
		this.radUserSelection = radUserSelection;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Integer getMessageID() {
		return messageID;
	}

	public void setMessageID(Integer messageID) {
		this.messageID = messageID;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getUserList() {
		return userList;
	}

	public void setUserList(String userList) {
		this.userList = userList;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getNewMessageExist() {
		return newMessageExist;
	}

	public void setNewMessageExist(String newMessageExist) {
		this.newMessageExist = newMessageExist;
	}

	public DashboardMessage parseDomainObj(DashboardMessage current, Integer dashbrdKey) {
		current = this.parsePprimitiveDomainObj(current);
		this.addVisiblityStats(current, dashbrdKey);// TODO: Move to BL
		return current;
	}

	public DashboardMessage parsePprimitiveDomainObj(DashboardMessage current) {
		if (current == null) {
			current = new DashboardMessage();
		}

		if (PlatformUtiltiies.isNotEmptyString(this.priority)) {
			current.setPriorityID(this.priority);
		}
		if (PlatformUtiltiies.isNotEmptyString(this.status)) {
			current.setStatus(this.status);
		}
		if (PlatformUtiltiies.isNotEmptyString(this.messageID)) {
			current.setMessageID(this.messageID);
		}
		if (PlatformUtiltiies.isNotEmptyString(this.messageContent)) {
			current.setMessage(this.messageContent);
		}
		if (PlatformUtiltiies.isNotEmptyString(this.messageType)) {
			current.setMessageType(this.messageType);
		}
		if (PlatformUtiltiies.isNotEmptyString(this.version)) {
			current.setVersion(Long.valueOf(this.version).longValue());
		}
		try {
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			if (PlatformUtiltiies.isNotEmptyString(this.fromDate)) {
				current.setEffectiveFrom(format.parse(this.fromDate + " 00:00:00"));
			}
			if (PlatformUtiltiies.isNotEmptyString(this.toDate)) {
				current.setEffectiveTo(format.parse(this.toDate + " 23:59:59"));
			}
		} catch (ParseException e) {
			throw new RuntimeException("Exception occured while parsing Date");
		}

		current.setNewMessage(this.newMessageExist);

		return current;
	}

	private void addVisiblityStats(DashboardMessage current, Integer dashbrdKey) {
		// FIXME--Move to BL
		current.setAgents(this.getAgentsAsList(current, dashbrdKey));
		current.setAgentType(this.getAgentTypeList(current, dashbrdKey));
		current.setPos(this.getPOSAsList(current, dashbrdKey));
		current.setUser(this.getUsersAsList(current, dashbrdKey));

	}

	private Set<DashbrdMsgAgent> getAgentsAsList(DashboardMessage current, Integer dashbrdKey) {
		Set<DashbrdMsgAgent> agents = current.getAgents();
		if (agents == null)
			agents = new HashSet<DashbrdMsgAgent>();
		Map<String, DashbrdMsgAgent> agentMap = new HashMap<String, DashbrdMsgAgent>();
		for (Iterator<DashbrdMsgAgent> iterator = agents.iterator(); iterator.hasNext();) {
			DashbrdMsgAgent dashbrdMsgAgent = iterator.next();
			agentMap.put(dashbrdMsgAgent.getAgentCode(), dashbrdMsgAgent);
		}
		if (this.selectedAgents != null && !"".equals(selectedAgents)) {
			String[] splitedCodes = selectedAgents.split(",");
			for (int i = 0; i < splitedCodes.length; i++) {
				DashbrdMsgAgent agent = agentMap.remove(splitedCodes[i]);
				if (agent == null) {
					agent = new DashbrdMsgAgent();
					agent.setAgentCode(splitedCodes[i]);
					agent.setDashbrdMsgID(current.getMessageID() == null ? dashbrdKey : current.getMessageID());
				}
				if (!this.agentInclusionStatus.equals(agent.getApplyStatus())) {
					agent.setApplyStatus(this.agentInclusionStatus);
				}
				agents.add(agent);
			}
		}
		agents.removeAll(agentMap.values());
		return agents;
	}

	private Set<DashbrdMsgAgentType> getAgentTypeList(DashboardMessage current, Integer dashbrdKey) {
		Set<DashbrdMsgAgentType> agents = current.getAgentType();
		if (agents == null)
			agents = new HashSet<DashbrdMsgAgentType>();

		Map<String, DashbrdMsgAgentType> agentMap = new HashMap<String, DashbrdMsgAgentType>();
		for (Iterator<DashbrdMsgAgentType> iterator = agents.iterator(); iterator.hasNext();) {
			DashbrdMsgAgentType dashbrdMsgAgentType = iterator.next();
			agentMap.put(dashbrdMsgAgentType.getAgentTypeCode(), dashbrdMsgAgentType);
		}
		if (this.selectedAgentTypes != null && !"".equals(selectedAgentTypes)) {
			String[] splitedCodes = selectedAgentTypes.split(",");
			for (int i = 0; i < splitedCodes.length; i++) {
				DashbrdMsgAgentType agent = agentMap.remove(splitedCodes[i]);
				if (agent == null) {
					agent = new DashbrdMsgAgentType();
					agent.setAgentTypeCode(splitedCodes[i]);
					agent.setDashbrdMsgID(current.getMessageID() == null ? dashbrdKey : current.getMessageID());
				}
				if (!this.agentInclusionStatus.equals(agent.getApplyStatus())) {
					agent.setApplyStatus(this.agentTypeInclusionStatus);
				}
				agents.add(agent);
			}
		}
		agents.removeAll(agentMap.values());
		return agents;
	}

	private Set<DashbrdMsgUser> getUsersAsList(DashboardMessage current, Integer dashbrdKey) {
		Set<DashbrdMsgUser> users = current.getUser();
		if (users == null)
			users = new HashSet<DashbrdMsgUser>();
		Map<String, DashbrdMsgUser> userMap = new HashMap<String, DashbrdMsgUser>();
		for (Iterator<DashbrdMsgUser> iterator = users.iterator(); iterator.hasNext();) {
			DashbrdMsgUser user = iterator.next();
			userMap.put(user.getUserID(), user);
		}
		if (this.userList != null && !"".equals(userList)) {
			String[] splitedCodes = userList.split(",");
			for (int i = 0; i < splitedCodes.length; i++) {
				DashbrdMsgUser actualUser = userMap.remove(splitedCodes[i]);
				if (actualUser == null) {
					actualUser = new DashbrdMsgUser();
					actualUser.setUserID(splitedCodes[i]);
					actualUser.setDashbrdMsgID(current.getMessageID() == null ? dashbrdKey : current.getMessageID());
				}
				if (!this.userInclusionStatus.equals(actualUser.getApplyStatus())) {
					actualUser.setApplyStatus(this.userInclusionStatus);
				}
				users.add(actualUser);
			}
		}
		users.removeAll(userMap.values());
		return users;
	}

	private Set<DashbrdMsgAgentPOS> getPOSAsList(DashboardMessage current, Integer dashbrdKey) {
		Set<DashbrdMsgAgentPOS> posList = current.getPos();
		if (posList == null)
			posList = new HashSet<DashbrdMsgAgentPOS>();
		Map<String, DashbrdMsgAgentPOS> posMap = new HashMap<String, DashbrdMsgAgentPOS>();
		for (Iterator<DashbrdMsgAgentPOS> iterator = posList.iterator(); iterator.hasNext();) {
			DashbrdMsgAgentPOS pos = iterator.next();
			posMap.put(pos.getPosCode(), pos);
		}
		if (selectedPOS != null && !"".equals(selectedPOS)) {
			String[] splitedCodes = selectedPOS.split(",");
			for (int i = 0; i < splitedCodes.length; i++) {
				DashbrdMsgAgentPOS pos = posMap.remove(splitedCodes[i]);
				if (pos == null) {
					pos = new DashbrdMsgAgentPOS();
					pos.setPosCode(splitedCodes[i]);
					pos.setDashbrdMsgID(current.getMessageID() == null ? dashbrdKey : current.getMessageID());
				}
				if (!this.posInclusionStatus.equals(pos.getApplyStatus())) {
					pos.setApplyStatus(this.posInclusionStatus);
				}
				posList.add(pos);
			}
		}
		posList.removeAll(posMap.values());
		return posList;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
