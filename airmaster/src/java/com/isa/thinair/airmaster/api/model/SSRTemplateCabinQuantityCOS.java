package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;

/**
 * 
 * @hibernate.class table = "T_SSR_TEMPLATE_CABIN_QTY_COS"
 * 
 */
public class SSRTemplateCabinQuantityCOS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2650349378031315097L;

	private Integer templateCabinQtyCOSId;

	private SSRTemplateCabinQuantity setSSRQty;

	private String cabinClassCode;

	private String logicalCCCode;

	/**
	 * @return the templateCabinQtyCOSId
	 * 
	 * @hibernate.id column="SSR_TEMPLATE_CABIN_QTY_COS_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_SSR_TEMPLATE_CABIN_QTY_COS"
	 * 
	 */
	public Integer getTemplateCabinQtyCOSId() {
		return templateCabinQtyCOSId;
	}

	/**
	 * @param templateCabinQtyCOSId
	 *            the templateCabinQtyCOSId to set
	 */
	public void setTemplateCabinQtyCOSId(Integer templateCabinQtyCOSId) {
		this.templateCabinQtyCOSId = templateCabinQtyCOSId;
	}

	/**
	 * @return the cabinClassCode
	 * 
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            the cabinClassCode to set
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return the logicalCCCode
	 * 
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	/**
	 * @param logicalCCCode
	 *            the logicalCCCode to set
	 */
	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	/**
	 * @return the setSSRQty
	 * @hibernate.many-to-one column="SSR_TEMPLATE_CABIN_QTY_ID"
	 *                        class="com.isa.thinair.airmaster.api.model.SSRTemplateCabinQuantity"
	 */
	public SSRTemplateCabinQuantity getSetSSRQty() {
		return setSSRQty;
	}

	/**
	 * @param setSSRQty
	 *            the setSSRQty to set
	 */
	public void setSetSSRQty(SSRTemplateCabinQuantity setSSRQty) {
		this.setSSRQty = setSSRQty;
	}

}