/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:13:50
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import java.math.BigDecimal;
import java.util.List;

import com.isa.thinair.commons.core.framework.Tracking;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0 Modified By : Dhanusha
 * @Modified By : Navod Ediriweera - 20 Oct 2009
 * @hibernate.class table = "T_ROUTE_INFO"
 * @hibernate.query name="getRouteInfoByOndCode" query="from RouteInfo r where r.routeCode=?"
 * 
 *                  CityPair is the entity class to repesent a CityPair model
 * 
 */
public class RouteInfo extends Tracking {

	private static final long serialVersionUID = -7600909189323747519L;

	private long routeId;

	private String routeCode;

	private BigDecimal distance = AccelAeroCalculator.getDefaultBigDecimalZero();

	private int duration;

	private BigDecimal durationTolerance = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String fromAirportCode;

	private String toAirportCode;

	private String status;

	private String directFlg;

	private int noOfTransits;

	private List<RouteTransitsInfo> routeTransits;

	private String isLCCPubilshed;

	private String ancilaryRemindersEnabled;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	public static final String DIRECT_TRUE = "Y";

	public static final String DIRECT_FALSE = "N";

	public static final String COL_KEY_ORIGIN = "fromAirportCode";

	public static final String COL_KEY_DESTINATION = "toAirportCode";

	public static final String LCC_PUBLISHED_TRUE = "Y";

	public static final String LCC_PUBLISHED_FALSE = "N";

	/**
	 * @return Returns the routeId.
	 * 
	 * @hibernate.id column="ROUTE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_ROUTE_INFO"
	 */

	public long getRouteId() {
		return routeId;
	}

	/**
	 * @param routeId
	 *            The routeId to set.
	 */
	public void setRouteId(long routeId) {
		this.routeId = routeId;
	}

	/**
	 * returns the cityPairId
	 * 
	 * @return Returns the cityPairId.
	 * 
	 * @hibernate.property column = "OND_CODE"
	 */

	public String getRouteCode() {
		return routeCode;
	}

	public void setRouteCode(String modelRouteCode) {
		this.routeCode = modelRouteCode;
	}

	/**
	 * returns the distance
	 * 
	 * @return Returns the distance.
	 * 
	 * @hibernate.property column = "DISTANCE"
	 */
	public BigDecimal getDistance() {
		return distance;
	}

	/**
	 * sets the distance
	 * 
	 * @param distance
	 *            The distance to set.
	 */
	public void setDistance(BigDecimal distance) {
		this.distance = distance;
	}

	/**
	 * returns the duration
	 * 
	 * @return Returns the duration.
	 * 
	 * @hibernate.property column = "DURATION"
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * sets the duration
	 * 
	 * @param duration
	 *            The duration to set.
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * returns the durationTolerance
	 * 
	 * @return Returns the durationTolerance.
	 * 
	 * @hibernate.property column = "DURATION_TOLERANCE"
	 */
	public BigDecimal getDurationTolerance() {
		return durationTolerance;
	}

	/**
	 * sets the durationTolerance
	 * 
	 * @param durationTolerance
	 *            The durationTolerance to set.
	 */
	public void setDurationTolerance(BigDecimal durationTolerance) {
		this.durationTolerance = durationTolerance;
	}

	/**
	 * @hibernate.property column = "FROM_AIRPORT"
	 * 
	 */
	public String getFromAirportCode() {
		return fromAirportCode;
	}

	public void setFromAirportCode(String fromAirportCode) {
		this.fromAirportCode = fromAirportCode;
	}

	/**
	 * @hibernate.property column = "TO_AIRPORT"
	 * 
	 */
	public String getToAirportCode() {
		return toAirportCode;
	}

	public void setToAirportCode(String toAirportCode) {
		this.toAirportCode = toAirportCode;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 * 
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the directFlg.
	 * 
	 * @hibernate.property column = "DIRECT_FLAG"
	 */
	public String getDirectFlg() {
		return directFlg;
	}

	/**
	 * @param directFlg
	 *            The directFlg to set.
	 */
	public void setDirectFlg(String directFlg) {
		this.directFlg = directFlg;
	}

	/**
	 * @return Returns the noOfTransits.
	 * 
	 * @hibernate.property column = "NO_OF_TRANSITS"
	 */
	public int getNoOfTransits() {
		return noOfTransits;
	}

	/**
	 * @param noOfTransits
	 *            The noOfTransits to set.
	 */
	public void setNoOfTransits(int noOfTransits) {
		this.noOfTransits = noOfTransits;
	}

	/**
	 * @return Returns the routeTransits.
	 * 
	 * @hibernate.bag lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="ROUTE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airmaster.api.model.RouteTransitsInfo"
	 */
	public List<RouteTransitsInfo> getRouteTransits() {
		return routeTransits;
	}

	/**
	 * @param routeTransits
	 *            The routeTransits to set.
	 */
	public void setRouteTransits(List<RouteTransitsInfo> routeTransits) {
		this.routeTransits = routeTransits;
	}

	/**
	 * returns the LCC Published Status
	 * 
	 * @return Returns the duration.
	 * @hibernate.property column = "IS_LCC_PUBLISHED"
	 */
	public String getIsLCCPubilshed() {
		return isLCCPubilshed;
	}

	/**
	 * Retrives the LCC published status
	 * 
	 * @param isLCCPubilshed
	 */
	public void setIsLCCPubilshed(String isLCCPubilshed) {
		this.isLCCPubilshed = isLCCPubilshed;
	}

	public String getAncilaryRemindersEnabled() {
		return ancilaryRemindersEnabled;
	}

	public void setAncilaryRemindersEnabled(String ancilaryRemindersEnabled) {
		this.ancilaryRemindersEnabled = ancilaryRemindersEnabled;
	}

}
