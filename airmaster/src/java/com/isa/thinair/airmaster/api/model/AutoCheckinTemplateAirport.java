package com.isa.thinair.airmaster.api.model;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author aravinth.r
 * @hibernate.class table = "T_AUTO_CHKIN_TEMPLATE_AIRPORT"
 *
 */
public class AutoCheckinTemplateAirport extends Persistent {

	private static final long serialVersionUID = -3681516846590773660L;
	private Integer autoCheckinTemplateAirportId;
	private String airportCode;
	private AutomaticCheckinTemplate automaticCheckinTemplate;

	/**
	 * @return the autoCheckinTemplateAirportId
	 * @hibernate.id column = "AUTO_CHKIN_TEMPLATE_AIRPORT_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_AUTO_CHKIN_TEMPLATE_AIRPORT"
	 *
	 */
	public Integer getAutoCheckinTemplateAirportId() {
		return autoCheckinTemplateAirportId;
	}

	/**
	 * @param autoCheckinTemplateAirportId
	 *            the autoCheckinTemplateAirportId to set
	 */
	public void setAutoCheckinTemplateAirportId(Integer autoCheckinTemplateAirportId) {
		this.autoCheckinTemplateAirportId = autoCheckinTemplateAirportId;
	}

	/**
	 * @return the airportCode
	 * @hibernate.property column="AIRPORT_CODE"
	 */
	public String getAirportCode() {
		return airportCode;
	}

	/**
	 * @param airportCode
	 *            the airportCode to set
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return the automaticCheckinTemplate
	 * @hibernate.many-to-one column="AUTOMATIC_CHECKIN_TEMPLATE_ID"
	 *                        class="com.isa.thinair.airmaster.api.model.AutomaticCheckinTemplate"
	 */
	public AutomaticCheckinTemplate getAutomaticCheckinTemplate() {
		return automaticCheckinTemplate;
	}

	/**
	 * @param automaticCheckinTemplate
	 *            the automaticCheckinTemplate to set
	 */
	public void setAutomaticCheckinTemplate(AutomaticCheckinTemplate automaticCheckinTemplate) {
		this.automaticCheckinTemplate = automaticCheckinTemplate;
	}

	/**
	 * Overrided by default to support lazy loading
	 * 
	 * @return
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(this.autoCheckinTemplateAirportId).toHashCode();
	}

	public boolean equals(Object o) {
		if (o instanceof AutoCheckinTemplateAirport) {
			AutoCheckinTemplateAirport obj = (AutoCheckinTemplateAirport) o;
			if (obj.getAutoCheckinTemplateAirportId() != null && this.getAutoCheckinTemplateAirportId() != null) {

				if (obj.getAutoCheckinTemplateAirportId().intValue() == this.getAutoCheckinTemplateAirportId().intValue()) {
					return true;
				} else {
					return false;
				}

			} else {
				return false;
			}
		} else {
			return false;
		}
	}

}
