package com.isa.thinair.airmaster.api.util;

import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.commons.api.exception.ModuleException;

public class CurrencyProxy {
	private CommonMasterBD masterBD;
	private Map<String, Currency> mapCurrencyExchangeRates = new HashMap<String, Currency>();

	public CurrencyProxy(CommonMasterBD masterBD) {
		this.masterBD = masterBD;
	}

	public Currency getCurrency(String currencyCode) throws ModuleException {
		if (!mapCurrencyExchangeRates.containsKey(currencyCode)) {
			mapCurrencyExchangeRates.put(currencyCode, masterBD.getCurrency(currencyCode));
		}
		return mapCurrencyExchangeRates.get(currencyCode);
	}
}
