package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;

public class PaxTitleDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String titleCode;
	
	private String titleName;

	public String getTitleCode() {
		return titleCode;
	}

	public void setTitleCode(String titleCode) {
		this.titleCode = titleCode;
	}

	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}
	
}
