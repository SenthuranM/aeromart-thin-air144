/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Aug 04, 2010 17:09:25
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;

/**
 * Domain for DashBoard messages Agents
 * 
 * @author : Navod Ediriweera
 * @version : Ver 1.0.0
 * @hibernate.class table = "T_DASHBOARD_AGENT"
 */
public class DashbrdMsgAgent implements Serializable {

	private static final long serialVersionUID = 1102768789054481188L;
	private String agentCode;
	private Integer dashbrdAgentID;
	private String applyStatus;
	private Integer dashbrdMsgID;

	/**
	 * @return Returns the acccId.
	 * @hibernate.id column = "DASHBOARD_AGENT_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_DASHBOARD_AGENT"
	 */
	public Integer getDashbrdAgentID() {
		return dashbrdAgentID;
	}

	/**
	 * @hibernate.property column = "DASHBOARD_MESSAGE_ID"
	 * @return
	 */
	public Integer getDashbrdMsgID() {
		return dashbrdMsgID;
	}

	public void setDashbrdAgentID(Integer dashbrdAgentID) {
		this.dashbrdAgentID = dashbrdAgentID;
	}

	public void setDashbrdMsgID(Integer dashbrdMsgID) {
		this.dashbrdMsgID = dashbrdMsgID;
	}

	/**
	 * @hibernate.property column = "AGENT_CODE"
	 * @return
	 */
	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @hibernate.property column = "APPLY_STATUS"
	 * @return
	 */
	public String getApplyStatus() {
		return applyStatus;
	}

	public void setApplyStatus(String applyStatus) {
		this.applyStatus = applyStatus;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		DashbrdMsgAgent dhObj = (DashbrdMsgAgent) obj;
		if (dhObj.getAgentCode().equals(this.agentCode))
			return true;
		return super.equals(obj);
	}

}
