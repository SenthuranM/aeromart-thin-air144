/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:10
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.service;

import java.util.Collection;

import com.isa.thinair.airmaster.api.dto.external.StationTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Nilindra Fernando
 * @since 2.0
 */
public interface LocationBDExternal {
	public static final String SERVICE_NAME = "LocationService";

	/**
	 * Returns a Collection of StationTO
	 * 
	 * @return
	 * @throws ModuleException
	 * @see com.isa.thinair.airmaster.api.dto.external.StationTO
	 */
	public Collection<StationTO> getAllStations() throws ModuleException;
}
