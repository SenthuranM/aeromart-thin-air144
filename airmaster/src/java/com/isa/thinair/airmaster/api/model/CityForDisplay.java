/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:44
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : Haider
 * 
 *         05Mar09
 * 
 * @hibernate.class table = "T_CITY_FOR_DISPLAY"
 * 
 *                  CityForDisplay is the entity class to represent the translation of city in other language
 * 
 */
public class CityForDisplay extends Persistent {

	private static final long serialVersionUID = -3652972751473858233L;
	
	private Integer cityForDisplayId;
	
	private Integer cityId;

	private String languageCode;

	private String cityIdOl;

	private String cityNameOl;

	/**
	 * returns the cityForDisplayId
	 * 
	 * @return Returns the cityForDisplayId.
	 * 
	 * @hibernate.id column = "CITY_FOR_DISPLAY_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_CITY_FOR_DISPLAY"
	 * 
	 * 
	 */
	public Integer getId() {
		return cityForDisplayId;
	}

	public void setId(Integer id) {
		this.cityForDisplayId = id;
	}

	/**
	 * returns the cityId
	 * 
	 * @return Returns the cityId.
	 * 
	 * @hibernate.property column = "CITY_ID"
	 */
	public Integer getCityId() {
		return cityId;
	}

	/**
	 * sets the cityId
	 * 
	 * @param cityId
	 *            The cityId to set.
	 */
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	/**
	 * returns the cityNameOl
	 * 
	 * @return Returns the cityIdOl.
	 * 
	 * @hibernate.property column = "CITY_CODE_OL"
	 */
	public String getCityCodeOl() {
		return cityIdOl;
	}

	/**
	 * sets the cityNameOl
	 * 
	 * @param cityIdOl
	 *            The cityNameOl to set.
	 */
	public void setCityCodeOl(String cityIdOl) {
		this.cityIdOl = cityIdOl;
	}

	/**
	 * returns the languageCode
	 * 
	 * @return Returns the languageCode.
	 * 
	 * @hibernate.property column = "LANGUAGE_CODE"
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	/**
	 * sets the languageCode
	 * 
	 * @param languageCode
	 *            The languageCode to set.
	 */
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	/**
	 * @return the cityNameOl
	 * @hibernate.property column = "CITY_NAME_OL"
	 */
	public String getCityNameOl() {
		return cityNameOl;
	}

	/**
	 * @param cityNameOl
	 *            the cityNameOl to set
	 */
	public void setCityNameOl(String cityNameOl) {
		this.cityNameOl = cityNameOl;
	}
}
