package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;

/**
 * 
 * @hibernate.class table = "BG_T_BAGGAGE_COS"
 * 
 */
public class BaggageCOS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4838389574292705517L;

	private Integer baggageCOSId;

	private Baggage baggage;

	private String cabinClassCode;

	private String logicalCCCode;

	/**
	 * @return the baggageCOSId
	 * 
	 * @hibernate.id column="BAGGAGE_COS_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "BG_S_BAGGAGE_COS"
	 */
	public Integer getBaggageCOSId() {
		return baggageCOSId;
	}

	/**
	 * @param baggageCOSId
	 *            the baggageCOSId to set
	 */
	public void setBaggageCOSId(Integer baggageCOSId) {
		this.baggageCOSId = baggageCOSId;
	}

	/**
	 * @return the cabinClassCode
	 * 
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            the cabinClassCode to set
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return the logicalCCCode
	 * 
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	/**
	 * @param logicalCCCode
	 *            the logicalCCCode to set
	 */
	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	/**
	 * @return the baggage
	 * @hibernate.many-to-one column="BAGGAGE_ID" class="com.isa.thinair.airmaster.api.model.Baggage"
	 */
	public Baggage getBaggage() {
		return baggage;
	}

	/**
	 * @param baggage
	 *            the baggage to set
	 */
	public void setBaggage(Baggage baggage) {
		this.baggage = baggage;
	}
}
