package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;

public class AirportTransferDTO implements Serializable {

	/**
	 * @author manoji
	 */
	private static final long serialVersionUID = 5699295506805910221L;

	String flightNo;
	String aircraftModel;
	String cabinClassCode;
	String status;

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getAircraftModel() {
		return aircraftModel;
	}

	public void setAircraftModel(String aircraftModel) {
		this.aircraftModel = aircraftModel;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
