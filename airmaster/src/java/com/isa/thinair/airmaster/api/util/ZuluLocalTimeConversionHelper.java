package com.isa.thinair.airmaster.api.util;

/**
 * 
 */
import java.util.Date;

import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * local zulu time conversion helper (uses BD calls)
 * 
 * @@author chandanak
 */

public class ZuluLocalTimeConversionHelper {

	private AirportBD airportBD;

	public static final String GMT_OFFSET_ACTION_MINUS = "-";

	public ZuluLocalTimeConversionHelper(AirportBD airportBD) {
		this.airportBD = airportBD;
	}

	// ///////////////////////////////////////////////////
	// PUBLIC METHODS OF ZuluLocalTimeConversionHelper
	// ///////////////////////////////////////////////////

	/**
	 * method to get converted zulu time/date as local time/date for the given station
	 */
	public Date getLocalDateTime(String airport, Date zuluTime) throws ModuleException {

		AirportDST aptDST = airportBD.getEffectiveAirportDST(airport, zuluTime);
		int gmtOffset = getGMTOffset(airport);
		int dstOffset = (aptDST != null) ? aptDST.getDstAdjustTime() : 0;

		Date localTime = CalendarUtil.getLocalDate(zuluTime, gmtOffset, dstOffset);

		return localTime;
	}

	/**
	 * method to get converted zulu time/date as local time/date for the given station and for a given effective date
	 */
	public Date getLocalDateTimeAsEffective(String airport, Date effectiveDate, Date zuluTime) throws ModuleException {

		AirportDST aptDST = airportBD.getEffectiveAirportDST(airport, effectiveDate);
		int gmtOffset = getGMTOffset(airport);
		int dstOffset = (aptDST != null) ? aptDST.getDstAdjustTime() : 0;

		Date localTime = CalendarUtil.getLocalDate(zuluTime, gmtOffset, dstOffset);

		return localTime;
	}

	/**
	 * method to get converted local time/date as zulu time/date for the given station
	 */
	public Date getZuluDateTime(String airport, Date localDate) throws ModuleException {

		// add local time details to flight
		AirportDST aptDST = airportBD.getEffectiveAirportDST(airport, localDate);
		int gmtOffset = getGMTOffset(airport);
		int dstOffset = (aptDST != null) ? aptDST.getDstAdjustTime() : 0;

		// calculate the local date
		Date zuluDate = CalendarUtil.getZuluDate(localDate, gmtOffset, dstOffset);

		AirportDST aptDSTZulu = airportBD.getEffectiveAirportDST(airport, zuluDate);

		if ((aptDSTZulu != null && aptDST != null && aptDSTZulu.getDstCode() != aptDST.getDstCode())
				|| (aptDSTZulu != null && aptDST == null) || (aptDSTZulu == null && aptDST != null))
			throw new ModuleException("airschedules.arg.invalid.cannot.calculate.zulutime");

		return zuluDate;
	}

	/**
	 * method to get converted local time/date as zulu time/date for the given station and given effective date
	 */
	public Date getZuluDateTimeAsEffective(String airport, Date effectiveDate, Date localDate) throws ModuleException {

		// add local time details to flight
		AirportDST aptDST = airportBD.getEffectiveAirportDST(airport, effectiveDate);
		int gmtOffset = getGMTOffset(airport);
		int dstOffset = (aptDST != null) ? aptDST.getDstAdjustTime() : 0;

		// calculate the local date
		Date zuluDate = CalendarUtil.getZuluDate(localDate, gmtOffset, dstOffset);

		return zuluDate;
	}

	// ////////////////////////////////////////////////////
	// PRIVATE MEHTODS OF ZuluLocalTimeConversionHelper
	// ////////////////////////////////////////////////////

	private int getGMTOffset(String airport) throws ModuleException {
		Airport apt = airportBD.getAirport(airport);
		int gmtOffset = 0;
		if (apt != null) {

			gmtOffset = (apt.getGmtOffsetAction().equals(ZuluLocalTimeConversionHelper.GMT_OFFSET_ACTION_MINUS)) ? (-1 * apt
					.getGmtOffsetHours()) : apt.getGmtOffsetHours();
		}

		return gmtOffset;
	}

}