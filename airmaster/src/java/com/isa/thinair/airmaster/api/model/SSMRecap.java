package com.isa.thinair.airmaster.api.model;

import java.util.Date;

public class SSMRecap {
	
	private int ssmRecapID;
	private int gdsID;
	private String gdsCode;
	private Date scheduledTime;
	private boolean isSelectedRange;
	private Date scheduleStartDate;
	private Date scheduleEndDate;
	private String userID;
	private String emailAddress;
	private String sitaAddress;
	
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public int getSsmRecapID() {
		return ssmRecapID;
	}
	public void setSsmRecapID(int ssmRecapID) {
		this.ssmRecapID = ssmRecapID;
	}
	public int getGdsID() {
		return gdsID;
	}
	public void setGdsID(int gdsID) {
		this.gdsID = gdsID;
	}
	public Date getScheduledTime() {
		return scheduledTime;
	}
	public void setScheduledTime(Date scheduledTime) {
		this.scheduledTime = scheduledTime;
	}
	public boolean isSelectedRange() {
		return isSelectedRange;
	}
	public void setSelectedRange(boolean isSelectedRange) {
		this.isSelectedRange = isSelectedRange;
	}
	public Date getScheduleStartDate() {
		return scheduleStartDate;
	}
	public void setScheduleStartDate(Date scheduleStartDate) {
		this.scheduleStartDate = scheduleStartDate;
	}
	public Date getScheduleEndDate() {
		return scheduleEndDate;
	}
	public void setScheduleEndDate(Date scheduleEndDate) {
		this.scheduleEndDate = scheduleEndDate;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getGdsCode() {
		return gdsCode;
	}
	public void setGdsCode(String gdsCode) {
		this.gdsCode = gdsCode;
	}
	public String getSitaAddress() {
		return sitaAddress;
	}
	public void setSitaAddress(String sitaAddress) {
		this.sitaAddress = sitaAddress;
	}

}
