package com.isa.thinair.airmaster.api.model;

import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author eric
 * @hibernate.class table = "T_I18N_MESSAGE_KEYS" lazy="false"
 */
public class I18nMessageKey extends Persistent {

	private static final long serialVersionUID = 1L;
	private String i18nMsgKey;
	private String messageCategory;

	private Set<I18nMessage> i18nMessages;

	/**
	 * @return Returns the messageKey.
	 * @hibernate.id column = "I18N_MESSAGE_KEY" generator-class = "assigned"
	 */
	public String getI18nMsgKey() {
		return i18nMsgKey;
	}

	/**
	 * @param messageKey
	 */
	public void setI18nMsgKey(String i18nMsgKey) {
		this.i18nMsgKey = i18nMsgKey;
	}

	/**
	 * @return messageCategory.
	 * @hibernate.property column = "MESSAGE_CATEGORY"
	 */
	public String getMessageCategory() {
		return messageCategory;
	}

	/**
	 * @param messageCategory
	 *            .
	 */
	public void setMessageCategory(String messageCategory) {
		this.messageCategory = messageCategory;
	}

	/**
	 * @return the I18nMessages.
	 * @hibernate.set lazy="false" inverse="true" cascade="all-delete-orphan" table="T_I18N_MESSAGE"
	 * @hibernate.collection-key column="I18N_MESSAGE_KEY"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airmaster.api.model.I18nMessage"
	 */
	public Set<I18nMessage> getI18nMessages() {

		if (i18nMessages == null) {
			i18nMessages = new HashSet<I18nMessage>();
		}

		return i18nMessages;
	}

	/**
	 * @param i18nMessages
	 */
	public void setI18nMessages(Set<I18nMessage> i18nMessages) {
		this.i18nMessages = i18nMessages;
	}

	/**
	 * @param i18nMessage
	 */
	public void addI18nMessage(I18nMessage i18nMessage) {
		if (this.getI18nMessages() == null) {
			this.setI18nMessages(new HashSet<I18nMessage>());
		}

		i18nMessage.setI18nMessageKey(this);
		this.getI18nMessages().add(i18nMessage);
	}

	/**
	 * @param i18nMessage
	 */
	public void removeI18nMessage(I18nMessage i18nMessage) {
		if (this.getI18nMessages() != null) {
			i18nMessage.setI18nMessageKey(null);
			this.getI18nMessages().remove(i18nMessage);
		}
	}

	/**
	 * Retrieves the {@link I18nMessage} by the passed language locale. If the message for the particular locale doesn't
	 * exist a new message will be instantiated and returned populated with the passed locale.
	 * 
	 * @param locale
	 *            The language that the I18nMessage must be in.
	 * @return The found I18nMessage or a new instance if the specific message with the given locale doesn't exist
	 */
	public I18nMessage getI18nMessage(String locale) {

		for (I18nMessage msg : getI18nMessages()) {
			if (msg.getMessageLocale().equals(locale)) {
				return msg;
			}
		}

		I18nMessage emptyMessage = new I18nMessage();
		emptyMessage.setI18nMessageKey(this);
		emptyMessage.setMessageLocale(locale);
		return emptyMessage;
	}
}
