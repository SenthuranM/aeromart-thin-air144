package com.isa.thinair.airmaster.api.util;

import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.City;
import com.isa.thinair.airmaster.api.model.Station;
import com.isa.thinair.airmaster.api.model.Territory;
import com.isa.thinair.airsecurity.api.model.Role;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.Agent;

public class MasterDataPublishUtil {

	public static <T> T retrieveMasterDataFromUnpublishedData(String masterDataCode, List<T> unPublishedMasterData) {
		T foundMasterData = null;
		Iterator<T> masterDataIte = unPublishedMasterData.iterator();
		while (masterDataIte.hasNext()) {
			T masterData = masterDataIte.next();
			if (masterData instanceof Airport) {
				Airport tmpAirport = (Airport) masterData;
				if (tmpAirport.getAirportCode().equals(masterDataCode)) {
					foundMasterData = masterData;
					masterDataIte.remove();
					break;
				}
			} else if (masterData instanceof Station) {
				Station tmpStation = (Station) masterData;
				if (tmpStation.getStationCode().equals(masterDataCode)) {
					foundMasterData = masterData;
					masterDataIte.remove();
					break;
				}
			} else if (masterData instanceof Territory) {
				Territory tmpTerritory = (Territory) masterData;
				if (tmpTerritory.getTerritoryCode().equals(masterDataCode)) {
					foundMasterData = masterData;
					masterDataIte.remove();
				}
			} else if (masterData instanceof User) {
				User tmpUser = (User) masterData;
				if (tmpUser.getUserId().equals(masterDataCode)) {
					foundMasterData = masterData;
					masterDataIte.remove();
				}
			} else if (masterData instanceof Role) {
				Role tmpRole = (Role) masterData;
				if (tmpRole.getRoleId().equals(masterDataCode)) {
					foundMasterData = masterData;
					masterDataIte.remove();
				}
			} else if (masterData instanceof Agent) {
				Agent tmpAgent = (Agent) masterData;
				if (tmpAgent.getAgentCode().equals(masterDataCode)) {
					foundMasterData = masterData;
					masterDataIte.remove();
				}
			} else if (masterData instanceof City) {
				City tmpCity = (City) masterData;
				if (tmpCity.getCityCode().equals(masterDataCode)) {
					foundMasterData = masterData;
					masterDataIte.remove();
				}
			}
		}

		return foundMasterData;
	}
}
