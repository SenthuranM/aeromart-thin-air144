/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:25
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : Code Generation Tool
 * 
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_AIRCRAFT_CC_CAPACITY"
 * 
 *                  AircraftCcCapacity is the entity class to repesent a AircraftCcCapacity model
 * 
 */
public class AircraftCabinCapacity extends Persistent {

	private static final long serialVersionUID = -4725511005794990243L;

	private int acccId;

	private int seatCapacity;

	private int infantCapacity;

	private String modelNumber;

	private String ccCode;

	/**
	 * returns the acccId
	 * 
	 * @return Returns the acccId.
	 * @hibernate.id column = "ACCC_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AIRCRAFT_CC_CAPACITY_ID"
	 */
	public int getAcccId() {
		return acccId;
	}

	/**
	 * sets the acccId
	 * 
	 * @param acccId
	 *            The acccId to set.
	 */
	public void setAcccId(int acccId) {
		this.acccId = acccId;
	}

	/**
	 * returns the capacity
	 * 
	 * @return Returns the capacity.
	 * 
	 * @hibernate.property column = "SEAT_CAPACITY"
	 */
	public int getSeatCapacity() {
		return seatCapacity;
	}

	/**
	 * sets the capacity
	 * 
	 * @param capacity
	 *            The capacity to set.
	 */
	public void setSeatCapacity(int capacity) {
		this.seatCapacity = capacity;
	}

	/**
	 * 
	 * @hibernate.property column = "MODEL_NUMBER"
	 */
	public String getAircraftModelNumber() {
		return modelNumber;
	}

	/**
   * 
   */
	public void setAircraftModelNumber(String aircraftModel) {
		this.modelNumber = aircraftModel;
	}

	/**
	 * 
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClassCode() {
		return ccCode;
	}

	/**
   * 
   */
	public void setCabinClassCode(String cabinClassCode) {
		this.ccCode = cabinClassCode;
	}

	/**
	 * 
	 * @hibernate.property column = "INFANT_CAPACITY"
	 */
	public int getInfantCapacity() {
		return infantCapacity;
	}

	/**
 * 
 */
	public void setInfantCapacity(int infantCapacity) {
		this.infantCapacity = infantCapacity;
	}

}
