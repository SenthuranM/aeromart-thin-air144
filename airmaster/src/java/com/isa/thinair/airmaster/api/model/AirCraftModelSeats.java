/**
 * 
 */
package com.isa.thinair.airmaster.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author indika
 * 
 * @hibernate.class table = "SM_T_AIRCRAFT_MODEL_SEATS"
 * 
 */
public class AirCraftModelSeats extends Persistent {

	private static final long serialVersionUID = 1961521561179039709L;

	public static String SEAT_TYPE_EXIT = "EXIT";

	private int seatID;

	private String modelNo;

	private String seatCode;

	private int rowId;

	private int colId;

	private int rowGroupId;

	private int colGroupId;

	private String cabinClassCode;

	private String status;

	private String seatType;

	private String seatVisibility;

	/**
	 * @return Returns the seatID.
	 * @hibernate.id column = "AM_SEAT_ID" generator-class = "assigned"
	 */
	public int getSeatID() {
		return seatID;
	}

	/**
	 * @param seatID
	 *            The seatID to set.
	 * 
	 */
	public void setSeatID(int seatID) {
		this.seatID = seatID;
	}

	/**
	 * @return Returns the cabinClassCode.
	 * 
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 * 
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            The cabinClassCode to set.
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return Returns the colGroupId.
	 * 
	 * @hibernate.property column = "COL_GRP_ID"
	 * 
	 */
	public int getColGroupId() {
		return colGroupId;
	}

	/**
	 * @param colGroupId
	 *            The colGroupId to set.
	 */
	public void setColGroupId(int colGroupId) {
		this.colGroupId = colGroupId;
	}

	/**
	 * @return Returns the colId.
	 * 
	 * @hibernate.property column = "COL_ID"
	 * 
	 */
	public int getColId() {
		return colId;
	}

	/**
	 * @param colId
	 *            The colId to set.
	 */
	public void setColId(int colId) {
		this.colId = colId;
	}

	/**
	 * @return Returns the modelNo.
	 * 
	 * @hibernate.property column = "MODEL_NUMBER"
	 * 
	 */
	public String getModelNo() {
		return modelNo;
	}

	/**
	 * @param modelNo
	 *            The modelNo to set.
	 */
	public void setModelNo(String modelNo) {
		this.modelNo = modelNo;
	}

	/**
	 * @return Returns the rowGroupId.
	 * 
	 * @hibernate.property column = "ROW_GRP_ID"
	 * 
	 */
	public int getRowGroupId() {
		return rowGroupId;
	}

	/**
	 * @param rowGroupId
	 *            The rowGroupId to set.
	 */
	public void setRowGroupId(int rowGroupId) {
		this.rowGroupId = rowGroupId;
	}

	/**
	 * @return Returns the rowId.
	 * 
	 * @hibernate.property column = "ROW_ID"
	 */
	public int getRowId() {
		return rowId;
	}

	/**
	 * @param rowId
	 *            The rowId to set.
	 */
	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	/**
	 * @return Returns the seatCode.
	 * 
	 * @hibernate.property column = "SEAT_CODE"
	 * 
	 */
	public String getSeatCode() {
		return seatCode;
	}

	/**
	 * @param seatCode
	 *            The seatCode to set.
	 */
	public void setSeatCode(String seatCode) {
		this.seatCode = seatCode;
	}

	/**
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "SEAT_TYPE"
	 */
	public String getSeatType() {
		return seatType;
	}

	public void setSeatType(String seatType) {
		this.seatType = seatType;
	}

	/**
	 * @return the seatVisibility
	 * 
	 * @hibernate.property column = "SEAT_VISIBILITY"
	 */
	public String getSeatVisibility() {
		return seatVisibility;
	}

	/**
	 * @param seatVisibility
	 *            the seatVisibility to set
	 */
	public void setSeatVisibility(String seatVisibility) {
		this.seatVisibility = seatVisibility;
	}
}
