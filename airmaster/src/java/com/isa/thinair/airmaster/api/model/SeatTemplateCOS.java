package com.isa.thinair.airmaster.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "SM_T_AM_SEAT_ASSIGNMENT"
 */
public class SeatTemplateCOS extends Persistent {
	
	private static final long serialVersionUID = -3116593234841227366L;

	private Integer seatAssignmentId;

	private Integer seatId;

	private String status;

	private String logicalCCCode;

	private Integer templateId;

	/**
	 * 
	 * @return
	 * @hibernate.property column = "AM_SEAT_ID"
	 */
	public Integer getSeatId() {
		return seatId;
	}

	public void setSeatId(Integer seatId) {
		this.seatId = seatId;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	/**
	 * 
	 * @return
	 * @hibernate.id column = "AM_SEAT_ASSIGNMENT_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "SM_S_AM_SEAT_ASSIGNMENT"
	 *     
	 */
	public Integer getSeatAssignmentId() {
		return seatAssignmentId;
	}

	public void setSeatAssignmentId(Integer seatAssignmentId) {
		this.seatAssignmentId = seatAssignmentId;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "AMC_TEMPLATE_ID"
	 */
	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

}