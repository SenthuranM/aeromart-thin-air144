package com.isa.thinair.airmaster.api.to;

import java.io.Serializable;

public class AirportMessageTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long messageId;
	private String airport;
	private String depArrType;
	private String messageType;
	private String displayFrom;
	private String dispalyTo;
	private String validFrom;
	private String validTo;
	private String message;
	private String version;

	private String createdDate;
	private String createdBy;
	private String modifiedDate;
	private String modifiedBy;

	private String selectedflights;
	private String selectedsalesChannels;
	private String selectedonds;

	private String flightApplyStatus;
	private String ondApplyStatus;

	// language descriptions
	private String languageList;

	private String selectedLanguageTranslations;

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public String getAirport() {
		return airport;
	}

	public void setAirport(String airport) {
		this.airport = airport;
	}

	public String getDepArrType() {
		return depArrType;
	}

	public void setDepArrType(String depArrType) {
		this.depArrType = depArrType;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getDisplayFrom() {
		return displayFrom;
	}

	public void setDisplayFrom(String displayFrom) {
		this.displayFrom = displayFrom;
	}

	public String getDispalyTo() {
		return dispalyTo;
	}

	public void setDispalyTo(String dispalyTo) {
		this.dispalyTo = dispalyTo;
	}

	public String getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	public String getValidTo() {
		return validTo;
	}

	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSelectedflights() {
		return selectedflights;
	}

	public void setSelectedflights(String selectedflights) {
		this.selectedflights = selectedflights;
	}

	public String getSelectedsalesChannels() {
		return selectedsalesChannels;
	}

	public void setSelectedsalesChannels(String selectedsalesChannels) {
		this.selectedsalesChannels = selectedsalesChannels;
	}

	public String getSelectedonds() {
		return selectedonds;
	}

	public void setSelectedonds(String selectedonds) {
		this.selectedonds = selectedonds;
	}

	public String getFlightApplyStatus() {
		return flightApplyStatus;
	}

	public void setFlightApplyStatus(String flightApplyStatus) {
		this.flightApplyStatus = flightApplyStatus;
	}

	public String getOndApplyStatus() {
		return ondApplyStatus;
	}

	public void setOndApplyStatus(String ondApplyStatus) {
		this.ondApplyStatus = ondApplyStatus;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getLanguageList() {
		return languageList;
	}

	public void setLanguageList(String languageList) {
		this.languageList = languageList;
	}

	public String getSelectedLanguageTranslations() {
		return selectedLanguageTranslations;
	}

	public void setSelectedLanguageTranslations(String selectedLanguageTranslations) {
		this.selectedLanguageTranslations = selectedLanguageTranslations;
	}
}
