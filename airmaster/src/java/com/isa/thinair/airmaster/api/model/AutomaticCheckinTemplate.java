package com.isa.thinair.airmaster.api.model;

import java.math.BigDecimal;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * @author aravinth.r
 * @hibernate.class table = "T_AUTOMATIC_CHECKIN_TEMPLATE"
 */
public class AutomaticCheckinTemplate extends Tracking {

	private static final long serialVersionUID = 4618947629202852714L;
	private Integer automaticCheckinTemplateId;
	private BigDecimal amount;
	private String status;
	private Set<AutoCheckinTemplateAirport> autoCheckinTemplateAirport;

	/**
	 * @return the autoCheckinTemplateAirport
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true" orphanRemoval="true"
	 * @hibernate.collection-key column="AUTOMATIC_CHECKIN_TEMPLATE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airmaster.api.model.AutoCheckinTemplateAirport"
	 */
	public Set<AutoCheckinTemplateAirport> getAutoCheckinTemplateAirport() {
		return autoCheckinTemplateAirport;
	}

	/**
	 * @param autoCheckinTemplateAirport
	 *            the autoCheckinTemplateAirport to set
	 */
	public void setAutoCheckinTemplateAirport(Set<AutoCheckinTemplateAirport> autoCheckinTemplateAirport) {
		this.autoCheckinTemplateAirport = autoCheckinTemplateAirport;
	}

	/**
	 * @return the automaticCheckinTemplateId
	 * @hibernate.id column = "AUTOMATIC_CHECKIN_TEMPLATE_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_AUTOMATIC_CHECKIN_TEMPLATE"
	 */
	public Integer getAutomaticCheckinTemplateId() {
		return automaticCheckinTemplateId;
	}

	/**
	 * @param automaticCheckinTemplateId
	 *            the automaticCheckinTemplateId to set
	 */
	public void setAutomaticCheckinTemplateId(Integer automaticCheckinTemplateId) {
		this.automaticCheckinTemplateId = automaticCheckinTemplateId;
	}

	/**
	 * @return the amount
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
}
