/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:10
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @hibernate.class table = "ML_T_MEAL"
 * 
 */
public class Meal extends Tracking implements Serializable {

	private static final long serialVersionUID = -6790539298298260934L;

	private Integer mealId;

	private Integer mealCategoryID;

	private String mealName;

	private String mealDescription;

	private String mealCode;

	private String status;

	private Set<MealCOS> mealCOS;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	private String iataCode;

	private Set<String> channels;

	/**
	 * @return Returns the mealId.
	 * 
	 * @hibernate.id column="MEAL_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "ML_S_MEAL"
	 */
	public Integer getMealId() {
		return mealId;
	}

	public void setMealId(Integer mealId) {
		this.mealId = mealId;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "MEAL_NAME"
	 */
	public String getMealName() {
		return mealName;
	}

	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "MEAL_DESC"
	 */
	public String getMealDescription() {
		return mealDescription;
	}

	public void setMealDescription(String mealDescription) {
		this.mealDescription = mealDescription;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "MEAL_CODE"
	 */
	public String getMealCode() {
		return mealCode;
	}

	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the iataCode
	 * @hibernate.property column = "IATA_CODE"
	 */
	public String getIataCode() {
		return iataCode;
	}

	/**
	 * @param iataCode
	 *            the iataCode to set
	 */
	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	/**
	 * @return the mealCategoryID
	 * @hibernate.property column = "MEAL_CATEGORY_ID"
	 */
	public Integer getMealCategoryID() {
		return mealCategoryID;
	}

	/**
	 * @param mealCategoryID
	 *            the mealCategoryID to set
	 */
	public void setMealCategoryID(Integer mealCategoryID) {
		this.mealCategoryID = mealCategoryID;
	}

	/**
	 * @return the mealCOS
	 * @hibernate.set lazy="false" cascade="all" inverse="true"
	 * @hibernate.collection-key column="MEAL_ID"
	 * @hibernate.collection-one-to-many 
	 *                                   class="com.isa.thinair.airmaster.api.model.MealCOS"
	 */
	public Set<MealCOS> getMealCOS() {
		return mealCOS;
	}

	/**
	 * @param mealCOS
	 *            the mealCOS to set
	 */
	public void setMealCOS(Set<MealCOS> mealCOS) {
		this.mealCOS = mealCOS;
	}
	
	/**
	 * @return the channels
	 * @hibernate.set lazy="false" table="ML_T_MEAL_CHANNEL"
	 * @hibernate.collection-element column="SALES_CHANNEL_CODE" type="string"
	 * @hibernate.collection-key column="MEAL_ID"
	 */
	public Set<String> getChannels() {
		return channels;
	}

	/**
	 * Set the Channel list
	 * 
	 * @param channels
	 *            the channels
	 */
	public void setChannels(Set<String> channels) {
		this.channels = channels;
	}

	/**
	 * Add a Channel
	 * 
	 * @param channel
	 *            the channel
	 */
	public void addChannels(String channel) {
		if (this.channels == null) {
			this.setChannels(new HashSet<String>());
		}
		this.getChannels().add(channel);
	}

}
