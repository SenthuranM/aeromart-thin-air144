package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;

/**
 * The search DTO for meal category.
 * 
 * @author sanjaya
 * 
 */
public class MealCategorySearchDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7689696118987917207L;

	private String mealCategoryName;

	/**
	 * @return : The meal category name.
	 */
	public String getMealCategoryName() {
		return mealCategoryName;
	}

	/**
	 * @param mealCategoryName
	 *            : The meal category name to set.
	 */
	public void setMealCategoryName(String mealCategoryName) {
		this.mealCategoryName = mealCategoryName;
	}

}
