package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;
import java.util.Collection;

public class SmartSearchDTO implements Serializable {
	
	private static final long serialVersionUID = -3707019009690952561L;

	private Integer searchID;

	private String searchText;

	private Collection<String> searchResult;

	private String searchType;

	private String is_search_tag;

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public Collection<String> getSearchResult() {
		return searchResult;
	}

	public void setSearchResult(Collection<String> searchResult) {
		this.searchResult = searchResult;
	}

	public Integer getSearchID() {
		return searchID;
	}

	public void setSearchID(Integer searchID) {
		this.searchID = searchID;
	}

	public String getIs_search_tag() {
		return is_search_tag;
	}

	public void setIs_search_tag(String is_search_tag) {
		this.is_search_tag = is_search_tag;
	}

}
