package com.isa.thinair.airmaster.api.to;

import java.io.Serializable;

import com.isa.thinair.airmaster.api.model.AirportDST;

public class AirportDstTO implements Serializable {

	private static final long serialVersionUID = 2431312261831947169L;
	public static final String CONTAINS_FLIGHTS_YES = "YES";
	public static final String CONTAINS_FLIGHTS_NO = "NO";

	private AirportDST airportDST;
	private String containsFlights;

	/**
	 * @return Returns the airportDST.
	 */
	public AirportDST getAirportDST() {
		return airportDST;
	}

	/**
	 * @param airportDST
	 *            The airportDST to set.
	 */
	public void setAirportDST(AirportDST airportDST) {
		this.airportDST = airportDST;
	}

	/**
	 * @return Returns the containsFlights.
	 */
	public String getContainsFlights() {
		return containsFlights;
	}

	/**
	 * @param containsFlights
	 *            The containsFlights to set.
	 */
	public void setContainsFlights(String containsFlights) {
		this.containsFlights = containsFlights;
	}

}
