package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;

/**
 * 
 * @author : M.Rikaz
 * @hibernate.class table = "T_SSR_APPLICABLE_OND"
 */

public class SSRApplicableOND implements Serializable {

	private static final long serialVersionUID = 8718956196375999768L;

	private Integer appOndId;

	private String ondCode;

	private SSRConstraints appConstraints;

	/**
	 * @hibernate.many-to-one column="SSR_APP_CON_ID" class="com.isa.thinair.airmaster.api.model.SSRConstraints"
	 * @return Returns the ssrInfo.
	 */
	public SSRConstraints getAppConstraints() {
		return appConstraints;
	}

	public void setAppConstraints(SSRConstraints appConstraints) {
		this.appConstraints = appConstraints;
	}

	/**
	 * @return Returns the appOndId.
	 * 
	 * @hibernate.id column="SSR_APP_OND_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_SSR_APPLICABLE_OND"
	 */
	public Integer getAppOndId() {
		return appOndId;
	}

	public void setAppOndId(Integer appOndId) {
		this.appOndId = appOndId;
	}

	/**
	 * @return the ondCode
	 * @hibernate.property column = "OND_CODE"
	 */
	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}
}
