/**
 * 	mano
	May 5, 2011 
	2011
 */
package com.isa.thinair.airmaster.api.model;

import java.math.BigDecimal;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * @author mano
 *
 */

/**
 * 
 * @hibernate.class table = "BG_T_BAGGAGE"
 * 
 */

public class Baggage extends Tracking {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5182130582757508269L;

	private Integer baggageId;

	private String baggageName;

	private String description;

	private String iataCode;

	private String status;

	private Integer sortOrder;

	private BigDecimal baggageWeight;

	private I18nMessageKey i18nMessageKey;

	private Set<BaggageCOS> baggageCOS;

	/**
	 * @return Returns the i18nMessageKey.
	 * @hibernate.many-to-one lazy="false" column = "I18N_MESSAGE_KEY"
	 *                        class="com.isa.thinair.airmaster.api.model.I18nMessageKey" not-found="ignore"
	 *                        cascade="save-update"
	 * 
	 */
	public I18nMessageKey getI18nMessageKey() {
		return i18nMessageKey;
	}

	/**
	 * @param i18nMessageKey
	 */
	public void setI18nMessageKey(I18nMessageKey i18nMessageKey) {
		this.i18nMessageKey = i18nMessageKey;
	}

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	/**
	 * @return Returns the baggageId.
	 * 
	 * @hibernate.id column="BAGGAGE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "BG_S_BAGGAGE"
	 */
	public Integer getBaggageId() {
		return baggageId;
	}

	public void setBaggageId(Integer baggageId) {
		this.baggageId = baggageId;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "BAGGAGE_DESC"
	 */
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the iataCode
	 * @hibernate.property column = "IATA_CODE"
	 */
	public String getIataCode() {
		return iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the baggageName
	 * @hibernate.property column = "BAGGAGE_NAME"
	 */
	public String getBaggageName() {
		return baggageName;
	}

	/**
	 * @param baggageName
	 *            the baggageName to set
	 */
	public void setBaggageName(String baggageName) {
		this.baggageName = baggageName;
	}

	/**
	 * @return the sortOrder
	 * @hibernate.property column = "SORT_ORDER"
	 */
	public Integer getSortOrder() {
		return sortOrder;
	}

	/**
	 * @param sortOrder
	 *            the sortOrder to set
	 */
	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	/**
	 * @return the baggageWeight
	 * @hibernate.property column = "BAGGAGE_WEIGHT"
	 */
	public BigDecimal getBaggageWeight() {
		return baggageWeight;
	}

	/**
	 * @param baggageWeight
	 *            the baggageWeight to set
	 */
	public void setBaggageWeight(BigDecimal baggageWeight) {
		this.baggageWeight = baggageWeight;
	}

	/**
	 * @return the baggageCOS
	 */
	public Set<BaggageCOS> getBaggageCOS() {
		return baggageCOS;
	}

	/**
	 * @param baggageCOS
	 *            the baggageCOS to set
	 * @hibernate.set lazy="false" cascade="all" inverse="true"
	 * @hibernate.collection-key column="BAGGAGE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airmaster.api.model.BaggageCOS"
	 */
	public void setBaggageCOS(Set<BaggageCOS> baggageCOS) {
		this.baggageCOS = baggageCOS;
	}

}
