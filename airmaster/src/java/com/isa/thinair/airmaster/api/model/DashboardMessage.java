/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Aug 04, 2010 17:09:25
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import java.util.Date;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * Domain for DashBoard messages
 * 
 * @author : Navod Ediriweera
 * @version : Ver 1.0.0
 * @hibernate.class table = "T_DASHBOARD_MESSAGE"
 */
public class DashboardMessage extends Persistent {

	public static final String NEW_MESSAGE = "Y";
	public static final String NO_NEW_MESSAGE = "N";
	public static final String MSG_ACTIVE = "ACT";
	public static final String MSG_INACTIVE = "INA";
	public static final String APPLY_STATUS_Y = "Y";
	public static final String ACCLEARO_MSG = "AA";
	public static final Integer ACCLEARO_PRIORITY = 1;


	private static final long serialVersionUID = 1L;

	private Integer messageID;

	private String message;

	private String messageType;

	private Date effectiveFrom;

	private Date effectiveTo;

	private String userList;

	private Integer priorityID;

	private Set<DashbrdMsgAgent> agents;

	private Set<DashbrdMsgAgentPOS> pos;

	private Set<DashbrdMsgAgentType> agentType;

	private Set<DashbrdMsgUser> user;

	private String status;

	private String newMessage;

	/**
	 * returns the acccId
	 * 
	 * @return Returns the acccId.
	 * @hibernate.id column = "DASHBOARD_MESSAGE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_DASHBOARD_MESSAGE"
	 */
	public Integer getMessageID() {
		return messageID;
	}

	/**
	 * @hibernate.property column = "MESSAGE_CONTENT"
	 * @return
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * returns the shcemeIDs related to Agent
	 * 
	 * @hibernate.set lazy="false" inverse="true" cascade="all-delete-orphan" table="T_DASHBOARD_AGENT"
	 * @hibernate.collection-key column="DASHBOARD_MESSAGE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airmaster.api.model.DashbrdMsgAgent"
	 */
	public Set<DashbrdMsgAgent> getAgents() {
		return agents;
	}

	public void setAgents(Set<DashbrdMsgAgent> agents) {
		this.agents = agents;
	}

	/**
	 * returns the shcemeIDs related to Agent
	 * 
	 * @hibernate.set lazy="false" inverse="true" cascade="all-delete-orphan" table="T_DASHBOARD_POS"
	 * @hibernate.collection-key column="DASHBOARD_MESSAGE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airmaster.api.model.DashbrdMsgAgentPOS"
	 */
	public Set<DashbrdMsgAgentPOS> getPos() {
		return pos;
	}

	public void setPos(Set<DashbrdMsgAgentPOS> pos) {
		this.pos = pos;
	}

	/**
	 * returns the shcemeIDs related to Agent
	 * 
	 * @hibernate.set lazy="false" inverse="true" cascade="all-delete-orphan" table="T_DASHBOARD_AGENT_TYPE"
	 * @hibernate.collection-key column="DASHBOARD_MESSAGE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airmaster.api.model.DashbrdMsgAgentType"
	 */
	public Set<DashbrdMsgAgentType> getAgentType() {
		return agentType;
	}

	public void setAgentType(Set<DashbrdMsgAgentType> agentType) {
		this.agentType = agentType;
	}

	/**
	 * returns the shcemeIDs related to Agent
	 * 
	 * @hibernate.set lazy="false" inverse="true" cascade="all-delete-orphan" table="T_DASHBOARD_USER"
	 * @hibernate.collection-key column="DASHBOARD_MESSAGE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airmaster.api.model.DashbrdMsgUser"
	 */
	public Set<DashbrdMsgUser> getUser() {
		return user;
	}

	public void setUser(Set<DashbrdMsgUser> user) {
		this.user = user;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @hibernate.property column = "MESSAGE_TYPE"
	 * @return
	 */
	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	/**
	 * @hibernate.property column = "FROM_DATE"
	 * @return
	 */
	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	/**
	 * @hibernate.property column = "END_DATE"
	 * @return
	 */
	public Date getEffectiveTo() {
		return effectiveTo;
	}

	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	/**
	 * @hibernate.property column = "USER_LIST"
	 * @return
	 */
	public String getUserList() {
		return userList;
	}

	public void setUserList(String userList) {
		this.userList = userList;
	}

	/**
	 * @hibernate.property column = "PRIORITY_ID"
	 * @return
	 */
	public Integer getPriorityID() {
		return priorityID;
	}

	public void setPriorityID(Integer priorityID) {
		this.priorityID = priorityID;
	}

	public void setMessageID(Integer messageID) {
		this.messageID = messageID;
	}

	/**
	 * @hibernate.property column = "NEW_MESSAGE"
	 * @return
	 */
	public String getNewMessage() {
		return newMessage;
	}

	/**
	 * 
	 * @param newMessage
	 */
	public void setNewMessage(String newMessage) {
		this.newMessage = newMessage;
	}

}
