package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

/**
 * 
 * @author Manoj Dhanushka
 * @hibernate.class table = "T_AIRPORT_TRANSFER_TEMPLATE"
 */
public class AirportTransferTemplate implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer airportTransferTemplateId;
	private String ondCode;
	private String cabinClassCode;
	private String flightNumber;
	private String modelNumber;
	private Date departureDate;
	private BigDecimal adultAmount;
	private BigDecimal childAmount;
	private BigDecimal infantAmount;
	private String status;
	private Set<AirportTransferTemplateBC> airportTransferTempBookingClasses;

	/**
	 * @hibernate.id column = "AIRPORT_TRANSFER_TEMPLATE_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_AIRPORT_TRANSFER_TEMPLATE"
	 */
	public Integer getAirportTransferTemplateId() {
		return airportTransferTemplateId;
	}

	/**
	 * @return
	 * @hibernate.property column = "OND_CODE"
	 */
	public String getOndCode() {
		return ondCode;
	}

	/**
	 * @return
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}
	
	/**
	 * @return
	 * @hibernate.property column = "FLIGHT_NUMBER"
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @return
	 * @hibernate.property column = "DEPARTURE_DATE"
	 */	
	public Date getDepartureDate() {
		return departureDate;
	}

	/**
	 * @return
	 * @hibernate.property column = "MODEL_NUMBER"
	 */	
	public String getModelNumber() {
		return modelNumber;
	}

	/**
	 * @return
	 * @hibernate.property column = "ADULT_AMOUNT"
	 */	
	public BigDecimal getAdultAmount() {
		return adultAmount;
	}

	/**
	 * @return
	 * @hibernate.property column = "CHILD_AMOUNT"
	 */	
	public BigDecimal getChildAmount() {
		return childAmount;
	}

	/**
	 * @return
	 * @hibernate.property column = "INFANT_AMOUNT"
	 */	
	public BigDecimal getInfantAmount() {
		return infantAmount;
	}

	/**
	 * @return
	 * @hibernate.property column = "STATUS"
	 */	
	public String getStatus() {
		return status;
	}
	
	
	public void setAirportTransferTemplateId(Integer airportTransferTemplateId) {
		this.airportTransferTemplateId = airportTransferTemplateId;
	}
	
	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}
	
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}
	
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public void setAdultAmount(BigDecimal adultAmount) {
		this.adultAmount = adultAmount;
	}

	public void setChildAmount(BigDecimal childAmount) {
		this.childAmount = childAmount;
	}

	public void setInfantAmount(BigDecimal infantAmount) {
		this.infantAmount = infantAmount;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * @return Booking classes related to Airport Transfer Template 
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true" orphanRemoval="true"
	 * @hibernate.collection-key column="AIRPORT_TRANSFER_TEMPLATE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airmaster.api.model.AirportTransferTemplateBC"
	 */
	public Set<AirportTransferTemplateBC> getAirportTransferTempBookingClasses() {
		return airportTransferTempBookingClasses;
	}

	
	public void setAirportTransferTempBookingClasses(
			Set<AirportTransferTemplateBC> airportTransferTempBookingClasses) {
		this.airportTransferTempBookingClasses = airportTransferTempBookingClasses;
	}
		
}
