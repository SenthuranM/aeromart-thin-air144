/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:20:13
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_NATIONALITY"
 * 
 *                  Nationality is the entity class to repesent a Nationality model
 * 
 */
public class Nationality extends Persistent {

	private static final long serialVersionUID = 7500969398024647976L;

	private int nationalityCode;

	private String description;

	private String isoCode;
	
	private String icaoCode;

	/**
	 * returns the nationalityCode
	 * 
	 * @return Returns the nationalityCode.
	 * 
	 * @hibernate.id column = "NATIONALITY_CODE" generator-class = "assigned"
	 */
	public int getNationalityCode() {
		return nationalityCode;
	}

	/**
	 * sets the nationalityCode
	 * 
	 * @param nationalityCode
	 *            The nationalityCode to set.
	 */
	public void setNationalityCode(int nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	/**
	 * returns the description
	 * 
	 * @return Returns the description.
	 * 
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * sets the description
	 * 
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * returns the iso code
	 * 
	 * @return Returns the iso code.
	 * 
	 * @hibernate.property column = "ISO_CODE"
	 */
	public String getIsoCode() {
		return isoCode;
	}

	/**
	 * sets the iso code
	 * 
	 * @param iso
	 *            code The iso code to set.
	 */
	public void setIsoCode(String isoCode) {
		this.isoCode = isoCode;
	}
	
	/**
	 * @return
	 * @hibernate.property column = "ICAO_CODE"
	 */
	
	public String getIcaoCode(){
		return icaoCode;
	}
	
	public void setIcaoCode(String icaoCode) {
		this.icaoCode = icaoCode;
	}
}
