/*/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:10
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airmaster.api.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airmaster.api.criteria.AirportMsgSearchCriteria;
import com.isa.thinair.airmaster.api.criteria.SitaSearchCriteria;
import com.isa.thinair.airmaster.api.dto.CachedAirlineDTO;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airmaster.api.dto.SegmentCarrierDTO;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.AirportCheckInTime;
import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airmaster.api.model.AirportDSTHistory;
import com.isa.thinair.airmaster.api.model.AirportForDisplay;
import com.isa.thinair.airmaster.api.model.AirportTerminal;
import com.isa.thinair.airmaster.api.model.MealNotifyTimings;
import com.isa.thinair.airmaster.api.model.SITAAddress;
import com.isa.thinair.airmaster.api.to.AirportDstTO;
import com.isa.thinair.airmaster.api.to.AirportMessageTO;
import com.isa.thinair.airmaster.api.to.OperationStatusTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;


/**
 * @author Byorn
 */
public interface AirportBD {

	public static final String SERVICE_NAME = "AirportService";

	public void saveAirport(Airport obj) throws ModuleException;

	public void saveAirport(Airport obj, Collection<AirportForDisplay> airportForDisplay, MealNotifyTimings mealNotifyTiming) throws ModuleException;

	public OperationStatusTO addNewDST(AirportDST obj, FlightAlertDTO alertDTO) throws ModuleException;

	public OperationStatusTO disableAndRollBack(AirportDST airportDST, FlightAlertDTO alertDTO) throws ModuleException;

	public void deleteAirportDST(AirportDST airportDST) throws ModuleException;

	public void removeAirport(String key, boolean isMealNotifyEnable, int mealNotifyId) throws ModuleException;

	public void removeAirport(Airport airport, boolean isMealNotifyEnable, int mealNotifyId) throws ModuleException;

	public Collection<Airport> getAirportsForCountry(String countryCode) throws ModuleException;

	public Collection<Airport> getAirports() throws ModuleException;

	public Airport getAirport(String airportId) throws ModuleException;

	public Page getAirports(int startRec, int noRecs, String countryCode, String airportCode, String airportName)
			throws ModuleException;

	/** Method will return a collection of AirportDstTO **/
	public Collection<AirportDstTO> getAirportDSTs(String airportCode) throws ModuleException;

	public AirportDST getCurrentAirportDST(String airportCode) throws ModuleException;

	public AirportDST getEffectiveAirportDST(String airportCode, Date effectiveDate) throws ModuleException;

	public boolean hasPreviousDST(String airportCode) throws ModuleException;

	public Page getOfflineAirports(int startRec, int noRecs) throws ModuleException;

	public Page getOnlineAirports(int startRec, int noRecs) throws ModuleException;

	public Collection<Airport> getActiveAirports() throws ModuleException;

	/** sita **/
	public Page getSITAAddresses(SitaSearchCriteria criteria, int startRec, int pageSize) throws ModuleException;

	public SITAAddress getSITAAddress(Integer id) throws ModuleException;

	public SITAAddress getPrimarySITAAddress(String airportCode) throws ModuleException;

	public Collection<SITAAddress> getSITAAddresses(String airportCode) throws ModuleException;

	public Collection<SITAAddress> getActiveSITAAddresses(String airportCode, String carrierCode, DCS_PAX_MSG_GROUP_TYPE msgType) throws ModuleException;

	public void saveSITAAddress(SITAAddress sita) throws ModuleException;

	public void removeSITAAddress(int sitaAddressId) throws ModuleException;

	public void removeSITAAddress(SITAAddress address) throws ModuleException;

	public List<Airport> getLCCUnPublishedAirports() throws ModuleException;

	public void updateAirportLCCPublishStatus(List<Airport> originalAirportId, List<String> updatedAirports)
			throws ModuleException;

	public List<AirportDST> getLCCUnpublisedAirportDSTs(String airportCode, Date fromWhen) throws ModuleException;

	/**
	 * Returns the airport terminal for the given airport.
	 * 
	 * @param airportCode
	 *            : The airport code.
	 * @return The default terminal.
	 * @throws ModuleException
	 */
	public AirportTerminal getDefaultTerminal(String airportCode) throws ModuleException;

	public boolean compareCountryOfAirports(String firstAirportCode, String secondAirportCode) throws ModuleException;

	/**
	 * Returns the airport check-in time gap for a given airport/carrier code and flight number combination The check in
	 * time would be picked up as follows.
	 * 
	 * @param airportCode
	 *            : The airport code -
	 * @param carrierCode
	 *            : The carrier code -
	 * @param flightNumber
	 *            : The flight number -
	 * 
	 * @throws ModuleException
	 * @return The airport check in time (may return null if no matching airport check in time is found)
	 */
	public AirportCheckInTime getAirportCheckInTime(String airportCode, String flightNumber) throws ModuleException;

	public int getAirportCheckInTimeGap(String airportCode, String flightNumber) throws ModuleException;

	public int getAirportCheckInClosingTimeGap(String airportCode, String flightNumber) throws ModuleException;

	/** Method will return a collection of AirportCheckInTime **/
	public Collection<AirportCheckInTime> getAirportCITs(String airportCode) throws ModuleException;

	public void addNewCIT(AirportCheckInTime obj) throws ModuleException;

	public void deleteAirportCIT(AirportCheckInTime obj) throws ModuleException;

	public void deleteAirportCIT(String airPortCode) throws ModuleException;

	public Collection<AirportTerminal> getAirportTerminals(String airportCode, boolean isActive) throws ModuleException;

	public void addNewTerminal(AirportTerminal obj) throws ModuleException;

	public void deleteAirportTerminal(AirportTerminal obj) throws ModuleException;

	// Airport message
	public Page getAirportMessages(AirportMsgSearchCriteria airportMsgSearchCriteria, int start, int totalRec)
			throws ModuleException;

	public void saveAirportMessage(AirportMessageTO airportMessageTO) throws ModuleException;

	public void saveAirportMessageTranslations(AirportMessageTO airportMessageTO) throws ModuleException;

	/**
	 * Returns a read only view of both own and lcc airports
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public Map<String, CachedAirportDTO> getAllAirportOperatorMap() throws ModuleException;
	
	/**
	 * Returns a set of {{country_code}} + "_" + {{state_code}} vs city key
	 *
	 * @return
	 * @throws ModuleException
	 */
	public Map<String, Set<String>> getCityVsCountryAndStateCodesMap() throws ModuleException;

	/**
	 * returns a read only view of own airports for given airport codes
	 * 
	 * @param colAirPortCodes
	 * @return
	 * @throws ModuleException
	 */
	public Map<String, CachedAirportDTO> getCachedOwnAirportMap(Collection<String> colAirPortCodes) throws ModuleException;
	
	public  CachedAirportDTO getCachedAirport(String airportCode) throws ModuleException;

	/**
	 * returns a read only view of the own and lcc airports for a given airport codes
	 * 
	 * @param airportCodes
	 * @return
	 * @throws ModuleException
	 */
	public Map<String, CachedAirportDTO> getCachedAllAirportMap(Collection<String> airportCodes) throws ModuleException;

	/**
	 * returns true if the airport only opt by own carrier
	 * 
	 * @param airportCode
	 * @return
	 * @throws ModuleException
	 */
	public boolean isAirportOptByOwn(String airportCode) throws ModuleException;

	/**
	 * returns true if the airport is not operate by own and operate by others
	 * 
	 * @param airportCode
	 * @return
	 * @throws ModuleException
	 */
	public boolean isAirportoptByOther(String airportCode) throws ModuleException;

	/**
	 * return true if the segment is a bus segment
	 * 
	 * @param segmentCode
	 * @return
	 * @throws ModuleException
	 */
	public boolean isBusSegment(String segmentCode) throws ModuleException;

	/**
	 * retunrs true if one of the airports is a bus station
	 * 
	 * @param colAirPortCodes
	 * @return
	 * @throws ModuleException
	 */
	public boolean isContainGroundSegment(Collection<String> colAirPortCodes) throws ModuleException;

	/**
	 * retunrs true if one of the airports is a bus station
	 * 
	 * @param colAirPortCodes
	 * @return
	 * @throws ModuleException
	 */
	public boolean isContainSurfaceAirport(Map<String, CachedAirportDTO> airportCodes);

	public String getOwnCountryCodeForAirport(String airportCode) throws ModuleException;

	public String getOwnCityIdForAirport(String airportCode) throws ModuleException;

	public List<String> getSameCityAirports(String airportCode) throws ModuleException;

	public SegmentCarrierDTO getSegmentCarrierInfo(String origin, String destination, String carrierCode) throws ModuleException;
	
	public List<String> getActiveAirportsForCountry(String countryCode) throws ModuleException;
	
	public boolean isGoshoAgent(String fromAirport) throws ModuleException;
	
	public Collection<AirportDSTHistory> getUnPublishedAirportDSTHistory(String airports) throws ModuleException;

	public void updateAirportDSTHistoryStatus(int dstHistoryId, String status) throws ModuleException;

	public Set<String> getAirportCodeSet() throws ModuleException;
	
	public Collection<String> getActiveAirportsForCity(String airportCode) throws ModuleException;	
	
	public Map<String, CachedAirlineDTO> getLccAirlineList() throws ModuleException;
	
}
