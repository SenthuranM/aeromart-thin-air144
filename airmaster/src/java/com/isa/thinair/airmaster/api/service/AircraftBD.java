/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:10
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.model.FlightSeat;
import com.isa.thinair.airmaster.api.dto.AirCraftModelSeatsDTO;
import com.isa.thinair.airmaster.api.model.AirCraftModelSeats;
import com.isa.thinair.airmaster.api.model.Aircraft;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.api.model.SeatTemplateCOS;
import com.isa.thinair.airpricing.api.model.SeatCharge;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Byorn
 */
public interface AircraftBD {

	public static final String SERVICE_NAME = "AircraftService";

	public void saveAircraft(Aircraft obj) throws ModuleException;

	public void removeAircraft(int key) throws ModuleException;

	public void removeAircraft(Aircraft aircraft) throws ModuleException;

	public void removeAircraftModel(String modelNumber) throws ModuleException;

	public void removeAircraftModel(AircraftModel aircraftModel) throws ModuleException;

	public Collection<Aircraft> getAircrafts() throws ModuleException;

	public Aircraft getAircraft(int AircraftId) throws ModuleException;

	public Page getAircrafts(int startRec, int noRecs) throws ModuleException;

	public Page getAircraftModels(int startRec, int noRecs) throws ModuleException;

	public Collection<AircraftModel> getModels() throws ModuleException;

	public void saveAircraftModel(AircraftModel obj) throws ModuleException;

	public AircraftModel getAircraftModel(String modelNumber) throws ModuleException;

	public Collection<Aircraft> getActiveAircrafts() throws ModuleException;

	public Collection<AircraftModel> getActiveAircraftModels() throws ModuleException;

	public List<AircraftModel> getAircraftModels(Collection<String> aircraftModelNumbers) throws ModuleException;

	public void saveAirCraftModelSeat(AirCraftModelSeats airCraftModelSeats) throws ModuleException;

	public void saveAirCraftModelSeats(Collection<AirCraftModelSeats> airCraftModelSeats) throws ModuleException;

	public AirCraftModelSeatsDTO getAirCraftModelSeats(String model) throws ModuleException;

	public AirCraftModelSeats getAirCraftModelSeats(int seatID) throws ModuleException;

	public Map<String, List<Integer>> getAirCraftModelSeatsSeatIDModelSeats(String modelNo) throws ModuleException;

	public void saveSeatLogicalCabinClass(List<Integer> seatIdList, Integer templateId,
			Collection<SeatTemplateCOS> seatTemplateCOSList) throws ModuleException;

	public boolean isAllowUpdateLogicalCabinClass(ArrayList<Integer> seatList, Integer templateId) throws ModuleException;

	public Map<Integer, String> getFlightSeatData(ArrayList<Integer> seatList, Integer flightSegmentId);

	public List<AirCraftModelSeats> getFlightSeatData(List<Integer> seatList);

	public Map<Integer, Integer> getVersionList(Integer templateId);

	public List<SeatTemplateCOS> getAttachedSeatTemplateData(int flightSegmentId) throws ModuleException;

	public List<FlightSeat> getFlightSeatData(int flightSegmentId) throws ModuleException;

	public List<SeatCharge> getAttachedSeatChargeData(int flightSegmentId) throws ModuleException;

	public Map<String, Integer> getHiddenSeatsMap(String modelNo) throws ModuleException;

	public String getAffectedFlights(AircraftModel obj) throws ModuleException;

	public String getAffectedFlights(AircraftModel obj, Integer flightNo) throws ModuleException;
	
	public List<AircraftCabinCapacity> getAircraftCabinCapacity(String flightNo,Date flightDate) throws ModuleException;
	
	public List<AircraftModel> getAllActiveAircraftsByIataType(String iataType) throws ModuleException;
	
	public boolean isDefaultAircraftAssignedForIataAircraftType(String iataType, String modelNumber) throws ModuleException;

	public Map<Integer, AirCraftModelSeats> getNewReservedAircraftSeats(Integer flightSegId, String modelNo)
			throws ModuleException;
}
