/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:28:40
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_AIRPORT_DST"
 * 
 *                  AirportDestination is the entity class to repesent a AirportDestination model
 * 
 */
public class AirportDST extends Tracking {

	private static final long serialVersionUID = -3794911866479711664L;

	private int dstCode;

	private Date dstStartDateTime;

	private Date dstEndDateTime;

	private int dstAdjustTime;

	private String airportcode;

	private String status;

	public static final String CURRENT_DST = "ACT";

	public static final String INACTIVE_DST = "INA";

	/**
	 * returns the dstCode
	 * 
	 * @return Returns the dstCode.
	 * 
	 * @hibernate.id column = "DST_CODE" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AIRPORT_DST"
	 */

	public int getDstCode() {
		return dstCode;
	}

	/**
	 * sets the dstCode
	 * 
	 * @param dstCode
	 *            The dstCode to set.
	 */
	public void setDstCode(int dstCode) {
		this.dstCode = dstCode;
	}

	/**
	 * returns the dstStartDateTime
	 * 
	 * @return Returns the dstStartDateTime.
	 * 
	 * @hibernate.property column = "DST_START_DATE_TIME"
	 */
	public Date getDstStartDateTime() {
		return dstStartDateTime;
	}

	/**
	 * sets the dstStartDateTime
	 * 
	 * @param dstStartDateTime
	 *            The dstStartDateTime to set.
	 */
	public void setDstStartDateTime(Date dstStartDateTime) {
		this.dstStartDateTime = dstStartDateTime;
	}

	/**
	 * returns the dstEndDateTime
	 * 
	 * @return Returns the dstEndDateTime.
	 * 
	 * @hibernate.property column = "DST_END_DATE_TIME"
	 */
	public Date getDstEndDateTime() {
		return dstEndDateTime;
	}

	/**
	 * sets the dstEndDateTime
	 * 
	 * @param dstEndDateTime
	 *            The dstEndDateTime to set.
	 */
	public void setDstEndDateTime(Date dstEndDateTime) {
		this.dstEndDateTime = dstEndDateTime;
	}

	/**
	 * returns the dstAdjustTime
	 * 
	 * @return Returns the dstAdjustTime.
	 * 
	 * @hibernate.property column = "DST_ADJUST_TIME"
	 */
	public int getDstAdjustTime() {
		return dstAdjustTime;
	}

	/**
	 * sets the dstAdjustTime
	 * 
	 * @param dstAdjustTime
	 *            The dstAdjustTime to set.
	 */
	public void setDstAdjustTime(int dstAdjustTime) {
		this.dstAdjustTime = dstAdjustTime;
	}

	/**
	 * @hibernate.property column = "AIRPORT_CODE"
	 * 
	 */
	public String getAirportCode() {
		return airportcode;
	}

	/**
   * 
   */
	public void setAirportCode(String airportcode) {
		this.airportcode = airportcode;
	}

	/**
	 * @hibernate.property column = "STATUS"
	 * 
	 */
	public String getDSTStatus() {
		return status;
	}

	/**
   * 
   */
	public void setDSTStatus(String status) {
		this.status = status;
	}

}
