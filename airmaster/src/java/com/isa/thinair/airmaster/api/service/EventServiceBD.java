package com.isa.thinair.airmaster.api.service;

import com.isa.thinair.airmaster.api.model.EventProfile;

public interface EventServiceBD {
	
	public static final String SERVICE_NAME = "EventService";

	public void publishEvent(EventProfile eventProfile);

}
