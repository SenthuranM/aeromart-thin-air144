/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:10
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airmaster.api.model;

import java.util.Set;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @hibernate.class table = "T_ITINERARY_ADVERTISEMENT"
 * 
 */

public class ItineraryAdvertisement extends Tracking {

	private static final long serialVersionUID = -6790539298298260935L;

	private Integer advertisementId;

	private Set<String> applicableONDs;

	private String advertisementTitle;

	private String advertisementLanguage;

	private String advertisementImage;

	private String status;

	private String advertisementRemarks;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	/**
	 * @return Returns the advertisementId.
	 * 
	 * @hibernate.id column="ITINERARY_ADVERTISEMENT_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value ="S_ITINERARY_ADVERTISEMENT"
	 */
	public Integer getAdvertisementId() {
		return advertisementId;
	}

	public void setAdvertisementId(Integer advertisementId) {
		this.advertisementId = advertisementId;
	}

	/**
	 * @hibernate.set lazy="false" table="T_ITINERARY_ADVERTISEMENT_OND"
	 * @hibernate.collection-element column="OND_CODE" type="string"
	 * @hibernate.collection-key column="ITINERARY_ADVERTISEMENT_ID"
	 * 
	 * @see #applicableONDs
	 */
	public Set<String> getApplicableONDs() {
		return applicableONDs;
	}

	public void setApplicableONDs(Set<String> applicableONDs) {
		this.applicableONDs = applicableONDs;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "ADVERTISEMENT_TITLE"
	 */
	public String getAdvertisementTitle() {
		return advertisementTitle;
	}

	public void setAdvertisementTitle(String advertisementTitle) {
		this.advertisementTitle = advertisementTitle;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "LANGUAGE_CODE"
	 */
	public String getAdvertisementLanguage() {
		return advertisementLanguage;
	}

	public void setAdvertisementLanguage(String advertisementLanguage) {
		this.advertisementLanguage = advertisementLanguage;
	}

	/**
	 * @return the advertisement image
	 * @hibernate.property column = "BANNER_IMAGE"
	 */
	public String getAdvertisementImage() {
		return advertisementImage;
	}

	public void setAdvertisementImage(String advertisementImage) {
		this.advertisementImage = advertisementImage;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the advertisement remarks
	 * @hibernate.property column = "REMARKS"
	 */
	public String getAdvertisementRemarks() {
		return advertisementRemarks;
	}

	public void setAdvertisementRemarks(String advertisementRemarks) {
		this.advertisementRemarks = advertisementRemarks;
	}

}
