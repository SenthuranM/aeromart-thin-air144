package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;

public class SSRPaxSegmentDTO implements Serializable {

	private static final long serialVersionUID = -2582240743426367387L;
	private int ppfsId;
	private int ppssId;
	private int ssrId;
	private String ssrStatus;
	private int fltSegId;
	private int pnrPaxId;
	private String airportCode;

	public int getPpfsId() {
		return ppfsId;
	}

	public void setPpfsId(int ppfsId) {
		this.ppfsId = ppfsId;
	}

	public int getPpssId() {
		return ppssId;
	}

	public void setPpssId(int ppssId) {
		this.ppssId = ppssId;
	}

	public long getSsrId() {
		return ssrId;
	}

	public void setSsrId(int ssrId) {
		this.ssrId = ssrId;
	}

	public String getSsrStatus() {
		return ssrStatus;
	}

	public void setSsrStatus(String ssrStatus) {
		this.ssrStatus = ssrStatus;
	}

	public long getFltSegId() {
		return fltSegId;
	}

	public void setFltSegId(int fltSegId) {
		this.fltSegId = fltSegId;
	}

	public long getPnrPaxId() {
		return pnrPaxId;
	}

	public void setPnrPaxId(int pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

}
