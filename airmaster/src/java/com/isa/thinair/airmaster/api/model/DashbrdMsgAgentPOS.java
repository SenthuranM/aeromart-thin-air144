/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Aug 04, 2010 17:09:25
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;

/**
 * Domain for DashBoard messages Agents
 * 
 * @author : Navod Ediriweera
 * @version : Ver 1.0.0
 * @hibernate.class table = "T_DASHBOARD_POS"
 */
public class DashbrdMsgAgentPOS implements Serializable {

	private static final long serialVersionUID = 1L;

	private String posCode;
	private Integer dashbrdPOSID;
	private String applyStatus;
	private Integer dashbrdMsgID;

	/**
	 * @return Returns the acccId.
	 * @hibernate.id column = "DASHBOARD_POS_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_DASHBOARD_POS"
	 */
	public Integer getDashbrdPOSID() {
		return dashbrdPOSID;
	}

	public void setDashbrdPOSID(Integer dashbrdPOSID) {
		this.dashbrdPOSID = dashbrdPOSID;
	}

	/**
	 * @hibernate.property column = "DASHBOARD_MESSAGE_ID"
	 * @return
	 */
	public Integer getDashbrdMsgID() {
		return dashbrdMsgID;
	}

	public void setDashbrdMsgID(Integer dashbrdMsgID) {
		this.dashbrdMsgID = dashbrdMsgID;
	}

	/**
	 * @hibernate.property column = "POS_CODE"
	 * @return
	 */
	public String getPosCode() {
		return posCode;
	}

	public void setPosCode(String posCode) {
		this.posCode = posCode;
	}

	/**
	 * @hibernate.property column = "APPLY_STATUS"
	 * @return
	 */
	public String getApplyStatus() {
		return applyStatus;
	}

	public void setApplyStatus(String applyStatus) {
		this.applyStatus = applyStatus;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		DashbrdMsgAgentPOS dhObj = (DashbrdMsgAgentPOS) obj;
		if (dhObj.getDashbrdPOSID().equals(this.posCode))
			return true;
		return super.equals(obj);
	}

}
