package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;

/**
 * @author eric
 * @version : Ver 1.0.0
 * @hibernate.class table = "T_AIRPORT_MSG_FLIGHT"
 */
public class AirportMsgFlight implements Serializable {

	private static final long serialVersionUID = 7031971973013990517L;
	private Long id;
	private String flightNo;
	private String applyStatus;
	private AirportMessage airportMessage;

	/**
	 * @return Returns the id.
	 * @hibernate.id column = "AIRPORT_MSG_FLIGHT_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AIRPORT_MSG_FLIGHT"
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return
	 * @hibernate.property column = "FLIGHT_NUMBER"
	 */
	public String getFlightNo() {
		return flightNo;
	}

	/**
	 * @param flightNo
	 */
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	/**
	 * @return
	 * @hibernate.property column = "APPLY_STATUS"
	 */
	public String getApplyStatus() {
		return applyStatus;
	}

	/**
	 * @param applyStatus
	 */
	public void setApplyStatus(String applyStatus) {
		this.applyStatus = applyStatus;
	}

	/**
	 * @return
	 * @hibernate.many-to-one column="AIRPORT_MSG_ID" class="com.isa.thinair.airmaster.api.model.AirportMessage"
	 */
	public AirportMessage getAirportMessage() {
		return airportMessage;
	}

	/**
	 * @param airportMessage
	 */
	public void setAirportMessage(AirportMessage airportMessage) {
		this.airportMessage = airportMessage;
	}

}
