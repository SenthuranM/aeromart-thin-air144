package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;

/**
 * @author Rikaz
 * 
 */
public class SSRSearchDTO implements Serializable {

	private static final long serialVersionUID = -8455005476878820172L;

	private String ssrCode;

	private String ssrCategory;

	private String ssrSubCategory;

	private String status;

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	public String getSsrCode() {
		return ssrCode;
	}

	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	public String getSsrCategory() {
		return ssrCategory;
	}

	public void setSsrCategory(String ssrCategory) {
		this.ssrCategory = ssrCategory;
	}

	public String getSsrSubCategory() {
		return ssrSubCategory;
	}

	public void setSsrSubCategory(String ssrSubCategory) {
		this.ssrSubCategory = ssrSubCategory;
	}

}
