package com.isa.thinair.airmaster.api.dto;

import java.io.Serializable;

public class CountryDetailsDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String countryCode;
	
	private String countryName;
	
	private String nationalityCode;
	
	private String nationalityDes;
	
	private String phoneCode;

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getNationalityCode() {
		return nationalityCode;
	}

	public void setNationalityCode(String nationalityCode) {
		this.nationalityCode = nationalityCode;
	}

	public String getNationalityDes() {
		return nationalityDes;
	}

	public void setNationalityDes(String nationalityDes) {
		this.nationalityDes = nationalityDes;
	}

	public String getPhoneCode() {
		return phoneCode;
	}

	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}
	
}
