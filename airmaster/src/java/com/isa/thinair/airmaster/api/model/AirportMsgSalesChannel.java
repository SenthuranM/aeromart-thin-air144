package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;

/**
 * @author eric
 * @version : Ver 1.0.0
 * @hibernate.class table = "T_AIRPORT_MSG_SALES_CHANNEL"
 */
public class AirportMsgSalesChannel implements Serializable {

	private static final long serialVersionUID = -5656362262632780163L;
	private Long id;
	private Integer salesChannelId;
	private AirportMessage airportMessage;

	/**
	 * @return Returns the Id.
	 * @hibernate.id column = "AIRPORT_MSG_SALES_CHANNEL_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_AIRPORT_MSG_SALES_CHANNEL"
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return salesChannelId
	 * @hibernate.property column = "SALES_CHANNEL_CODE"
	 * 
	 */

	public Integer getSalesChannelId() {
		return salesChannelId;
	}

	/**
	 * @param salesChannelId
	 */

	public void setSalesChannelId(Integer salesChannelId) {
		this.salesChannelId = salesChannelId;
	}

	/**
	 * @return airportMessage
	 * @hibernate.many-to-one column="AIRPORT_MSG_ID" class="com.isa.thinair.airmaster.api.model.AirportMessage"
	 */
	public AirportMessage getAirportMessage() {
		return airportMessage;
	}

	/**
	 * @param airportMessage
	 */
	public void setAirportMessage(AirportMessage airportMessage) {
		this.airportMessage = airportMessage;
	}

}
