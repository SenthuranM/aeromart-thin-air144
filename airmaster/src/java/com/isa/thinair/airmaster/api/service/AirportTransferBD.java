package com.isa.thinair.airmaster.api.service;

import java.util.List;
import java.util.Map;

import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.dto.AirportTransferDTO;
import com.isa.thinair.airmaster.api.model.AirportTransfer;
import com.isa.thinair.airmaster.api.model.AirportTransferTemplate;
import com.isa.thinair.airmaster.api.model.AirportTransferTemplateBC;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Manoj Dhanushka
 */
public interface AirportTransferBD {
	
	public static final String SERVICE_NAME = "AirportTransferService";

	public AirportTransfer getAirportTransfer(String airportCode)  throws ModuleException;
	
	public AirportTransfer getAirportTransfer(int id)  throws ModuleException;

	Page<AirportTransfer> getAirportTransfers(int startRec, int numOfRecs, String code, String status) throws ModuleException;

	Page<AirportTransferTemplate> getAirportTransferTemplates(int startRec, int numOfRecs, AirportTransferDTO airportTransferDTO)
			throws ModuleException;

	void saveAirportTransfers(AirportTransfer airportTransfer) throws ModuleException;

	void saveAirportTransferTemplate(AirportTransferTemplate airportTransferTemplate) throws ModuleException;
	
	public Map<AirportServiceKeyTO, List<AirportServiceDTO>> getAvailableAirportTransfers(
			Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseSegMap, Map<Integer, Boolean> csOCFlightMapping)
			throws ModuleException;
	
	public Map<String,Integer> getAirportTransferBookingClasses(int airportTransferTempId);
	
	void updateAirportTransferTemplate(AirportTransferTemplate airportTransferTemplate) throws ModuleException;
}
