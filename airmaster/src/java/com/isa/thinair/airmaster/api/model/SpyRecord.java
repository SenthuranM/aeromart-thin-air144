package com.isa.thinair.airmaster.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author Nilindra Fernando
 * 
 * @hibernate.class table = "T_SPY_AUDIT"
 */
public class SpyRecord extends Persistent implements Serializable {

	private static final long serialVersionUID = 5426805859332984778L;

	private Long auditId;

	private String auditCode;

	private Date timeStamp;

	private String userId;

	private String clientInfo;

	private int currentAttempt;

	private int maximumAttempts;

	private String content;

	/**
	 * @hibernate.id column = "SPY_AUDIT_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_SPY_AUDIT"
	 */
	public Long getAuditId() {
		return auditId;
	}

	/**
	 * @param auditId
	 *            the auditId to set
	 */
	public void setAuditId(Long auditId) {
		this.auditId = auditId;
	}

	/**
	 * @return the auditCode
	 * @hibernate.property column = "SPY_CODE"
	 */
	public String getAuditCode() {
		return auditCode;
	}

	/**
	 * @param auditCode
	 *            the auditCode to set
	 */
	public void setAuditCode(String auditCode) {
		this.auditCode = auditCode;
	}

	/**
	 * @return the timeStamp
	 * @hibernate.property column = "TIMESTAMP"
	 */
	public Date getTimeStamp() {
		return timeStamp;
	}

	/**
	 * @param timeStamp
	 *            the timeStamp to set
	 */
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * @return the userId
	 * @hibernate.property column = "USER_ID"
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the currentAttempt
	 * @hibernate.property column = "CURRENT_ATTEMPT"
	 */
	public int getCurrentAttempt() {
		return currentAttempt;
	}

	/**
	 * @param currentAttempt
	 *            the currentAttempt to set
	 */
	public void setCurrentAttempt(int currentAttempt) {
		this.currentAttempt = currentAttempt;
	}

	/**
	 * @return the maximumAttempts
	 * @hibernate.property column = "MAXIMUM_ATTEMPTS"
	 */
	public int getMaximumAttempts() {
		return maximumAttempts;
	}

	/**
	 * @param maximumAttempts
	 *            the maximumAttempts to set
	 */
	public void setMaximumAttempts(int maximumAttempts) {
		this.maximumAttempts = maximumAttempts;
	}

	/**
	 * @return the content
	 * @hibernate.property column = "CONTENT"
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the clientInfo
	 * @hibernate.property column = "CLIENT_INFO"
	 */
	public String getClientInfo() {
		return clientInfo;
	}

	/**
	 * @param clientInfo
	 *            the clientInfo to set
	 */
	public void setClientInfo(String clientInfo) {
		this.clientInfo = clientInfo;
	}

}
