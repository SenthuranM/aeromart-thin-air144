/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:29:42
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.api.model;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_TERRITORY"
 * 
 *                  Territory is the entity class to repesent a Territory model
 * 
 */
public class Territory extends Tracking {

	private static final long serialVersionUID = -4602303493118841009L;

	private String territoryCode;

	private String description;

	private String remarks;

	private String status;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	private String bank;

	private String bankAddress;

	private String accountNumber;

	private String swiftCode;

	private String IBANCode;

	private String beneficiaryName;

	private String lccPublishStatus;

	private String airlineCode;

	/**
	 * returns the territoryCode
	 * 
	 * @return Returns the territoryCode.
	 * 
	 * @hibernate.id column = "TERRITORY_CODE" generator-class = "assigned"
	 */
	public String getTerritoryCode() {
		return territoryCode;
	}

	/**
	 * sets the territoryCode
	 * 
	 * @param territoryCode
	 *            The territoryCode to set.
	 */
	public void setTerritoryCode(String territoryCode) {
		this.territoryCode = territoryCode;
	}

	/**
	 * returns the description
	 * 
	 * @return Returns the description.
	 * 
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * sets the description
	 * 
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * returns the remarks
	 * 
	 * @return Returns the remarks.
	 * 
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * sets the remarks
	 * 
	 * @param remarks
	 *            The remarks to set.
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * returns the bank
	 * 
	 * @return Returns the bank.
	 * 
	 * @hibernate.property column = "BANK"
	 */
	public String getBank() {
		return bank;
	}

	/**
	 * sets the bank
	 * 
	 * @param bank
	 *            The bank to set.
	 */
	public void setBank(String bank) {
		this.bank = bank;
	}

	/**
	 * returns the bankAddress
	 * 
	 * @return Returns the bankAddress.
	 * 
	 * @hibernate.property column = "BANK_ADDRESS"
	 */
	public String getBankAddress() {
		return bankAddress;
	}

	/**
	 * sets the bankAddress
	 * 
	 * @param bankAddress
	 *            The bankAddress to set.
	 */
	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	/**
	 * returns the accountNumber
	 * 
	 * @return Returns the accountNumber.
	 * 
	 * @hibernate.property column = "ACCOUNT_NUMBER"
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * sets the accountNumber
	 * 
	 * @param accountNumber
	 *            The accountNumber to set.
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * returns the swiftCode
	 * 
	 * @return Returns the swiftCode.
	 * 
	 * @hibernate.property column = "SWIFT_CODE"
	 */
	public String getSwiftCode() {
		return swiftCode;
	}

	/**
	 * sets the swiftCode
	 * 
	 * @param swiftCode
	 *            The swiftCode to set.
	 */
	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}

	/**
	 * returns the IBANCode
	 * 
	 * @return Returns the IBANCode.
	 * 
	 * @hibernate.property column = "IBAN_NO"
	 */
	public String getIBANCode() {
		return IBANCode;
	}

	/**
	 * sets the IBANCode
	 * 
	 * @param IBANCode
	 *            The IBANCode to set.
	 * 
	 */
	public void setIBANCode(String iBANCode) {
		IBANCode = iBANCode;
	}

	/**
	 * returns the beneficiaryName
	 * 
	 * @return Returns the beneficiaryName.
	 * 
	 * @hibernate.property column = "BENEFICIARY_NAME"
	 */
	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	/**
	 * sets the beneficiaryName
	 * 
	 * @param beneficiaryName
	 *            The beneficiaryName to set.
	 */
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	/**
	 * @hibernate.property column = "LCC_PUBLISH_STATUS"
	 */
	public String getLccPublishStatus() {
		return lccPublishStatus;
	}

	/**
	 * @param lccPublishStatus
	 *            the lccPublishStatus to set
	 */

	public void setLccPublishStatus(String lccPublishStatus) {
		this.lccPublishStatus = lccPublishStatus.intern();
	}

	/**
	 * @return the airlineCode
	 * @hibernate.property column = "AIRLINE_CODE"
	 */
	public String getAirlineCode() {
		return airlineCode;
	}

	/**
	 * @param airlineCode
	 *            the airlineCode to set
	 */

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode.intern();
	}

}
