package com.isa.thinair.airmaster.core.persistence.hibernate;

import java.util.Collection;
import com.isa.thinair.airmaster.api.model.SmartSearchIndex;
import com.isa.thinair.airmaster.core.persistence.dao.SearchDAO;
import com.isa.thinair.airmaster.core.util.SearchServiceUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 *
 * @isa.module.dao-impl dao-name="SearchDAO"
 */
public class SearchDAOImpl extends PlatformBaseHibernateDaoSupport implements SearchDAO {

	@SuppressWarnings("unchecked")
	@Override
	public Collection<String> search(SmartSearchIndex search) throws ModuleException {

		String smartSearchSQL = "SELECT ri.ond_code FROM T_SMART_SEARCH_INDEX sri INNER JOIN t_route_info ri ON ri.route_id = sri.route_id "
				+ "WHERE sri.search_index_plain_text =:searchIndexText ORDER BY ri.ond_code";
		SearchServiceUtil.filterSearchData(search);
		Collection<String> searchResult = getSession().createSQLQuery(smartSearchSQL)
				.setParameter("searchIndexText", search.getSearchIndexText()).list();

		return searchResult;
	}

}
