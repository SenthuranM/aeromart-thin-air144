/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.airmaster.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * @author Byorn
 * 
 *         AirmasterModule's service interface
 * @isa.module.service-interface module-name="airmaster" description="The airmaster module"
 */
public class AirmasterService extends DefaultModule {
}
