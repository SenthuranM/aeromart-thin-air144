package com.isa.thinair.airmaster.core.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airmaster.api.dto.CachedAirlineDTO;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airmaster.api.dto.SegmentCarrierDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * Class to access lcc database
 * 
 * @author rumesh
 * 
 */
public class LCCAirportDAO {

	/** data souce to get system params */
	private DataSource dataSource;

	@SuppressWarnings("unchecked")
	public Map<String, CachedAirportDTO> getAirports() {
		Map<String, CachedAirportDTO> retMap = new HashMap<String, CachedAirportDTO>();

		try {
			if (dataSource != null) {

				JdbcTemplate templete = new JdbcTemplate(dataSource);
				String queryString = "Select GMT_OFFSET_ACTION, GMT_OFFSET_HOURS, AIRPORT_CODE, IS_SURFACE_STATION, AIRPORT_NAME, PUBLISHED_CARRIERS, COUNTRY_CODE, CONNECTING_AIRPORT , STATE_CODE "
						+ "from LCC_T_AIRPORT ap where STATUS ='ACT'";
				retMap = (Map<String, CachedAirportDTO>) templete.query(queryString, new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<String, CachedAirportDTO> mapAirPorts = new HashMap<String, CachedAirportDTO>();
						CachedAirportDTO cachedAirportDTO;
						// if result set is not null
						if (rs != null) {
							// populate recordMap
							while (rs.next()) {
								cachedAirportDTO = new CachedAirportDTO();
								cachedAirportDTO.setAirportCode(rs.getString("AIRPORT_CODE"));
								cachedAirportDTO.setAirportName(rs.getString("AIRPORT_NAME"));
								cachedAirportDTO.setSurfaceStation(rs.getString("IS_SURFACE_STATION"));
								cachedAirportDTO.setGmtOffsetAction(rs.getString("GMT_OFFSET_ACTION"));
								cachedAirportDTO.setGmtOffsetHours(rs.getInt("GMT_OFFSET_HOURS"));
								cachedAirportDTO.setOperatedBy(rs.getString("PUBLISHED_CARRIERS"));
								cachedAirportDTO.setCountryCode(rs.getString("COUNTRY_CODE"));
								cachedAirportDTO.setConnectingAirport(rs.getString("CONNECTING_AIRPORT"));
								cachedAirportDTO.setStateCode(rs.getString("STATE_CODE"));
								mapAirPorts.put(cachedAirportDTO.getAirportCode(), cachedAirportDTO);
							}
						}
						return mapAirPorts;
					}
				});
			}

			return retMap;
		} catch (DataAccessException e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	/**
	 * If record exist in LCC_T_INTERLINE_ROUTE table, that means its an IL route if not look in LCC_T_AIRLINE_ROUTE and
	 * see what is the carrier, if its not the own carrier code thats IL
	 * 
	 * @param origin
	 * @param destination
	 * @param carrierCode
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public SegmentCarrierDTO getSegmentCarrierInfo(String origin, String destination, String carrierCode) {

		SegmentCarrierDTO segmentCarrierDTO = null;
		String segmentCode = origin.concat("/".concat(destination));

		if (AppSysParamsUtil.isLCCConnectivityEnabled()) {

			try {
				int count = 0;

				JdbcTemplate templete = new JdbcTemplate(dataSource);
				String queryString = "SELECT COUNT(*) as OCCURENCES from LCC_T_INTERLINE_ROUTE ap where STATUS ='ACT' "
						+ " AND OND_CODE LIKE '" + origin + "%" + destination + "'";

				count = (Integer) templete.query(queryString, new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						int count = 0;
						// if result set is not null
						if (rs != null) {
							while (rs.next()) {
								count = rs.getInt("OCCURENCES");
							}
						}
						return count;
					}
				});

				if (count > 0) {
					segmentCarrierDTO = new SegmentCarrierDTO();
					segmentCarrierDTO.setSystem(SYSTEM.INT);
					segmentCarrierDTO.setSegment(segmentCode);
					return segmentCarrierDTO;
				}

				queryString = "SELECT DISTINCT AIRLINE_CODE from LCC_T_AIRLINE_ROUTE ap where STATUS ='ACT' "
						+ " AND OND_CODE LIKE '" + origin + "%" + destination + "'";

				List<String> airlineCodes = (List<String>) templete.query(queryString, new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<String> carrierCodes = new ArrayList<String>();
						if (rs != null) {
							while (rs.next()) {
								carrierCodes.add(rs.getString("AIRLINE_CODE"));
							}
						}
						return carrierCodes;
					}
				});

				// DO NOT TAKE THE OBJECT INIATION OUT
				if (!airlineCodes.isEmpty() && !airlineCodes.contains(carrierCode)) {
					segmentCarrierDTO = new SegmentCarrierDTO();
					segmentCarrierDTO.setSystem(SYSTEM.INT);
					segmentCarrierDTO.setSegment(segmentCode);
				} else if (!airlineCodes.isEmpty()) {
					segmentCarrierDTO = new SegmentCarrierDTO();
					segmentCarrierDTO.setSystem(SYSTEM.AA);
					segmentCarrierDTO.setSegment(segmentCode);
				}

			} catch (DataAccessException e) {
				throw new CommonsDataAccessException(e, null);
			}
		} else {
			segmentCarrierDTO = new SegmentCarrierDTO();
			segmentCarrierDTO.setSystem(SYSTEM.AA);
			segmentCarrierDTO.setSegment(segmentCode);
		}

		return segmentCarrierDTO;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, CachedAirlineDTO> getAirlines() {
		Map<String, CachedAirlineDTO> retMap = new HashMap<String, CachedAirlineDTO>();

		try {
			if (dataSource != null) {

				JdbcTemplate templete = new JdbcTemplate(dataSource);
				String queryString = "Select AIRLINE_CODE, COUNTRY_CODE from LCC_T_AIRLINE al where STATUS ='ACT'";
				retMap = (Map<String, CachedAirlineDTO>) templete.query(queryString, new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<String, CachedAirlineDTO> mapAirlines = new HashMap<String, CachedAirlineDTO>();
						CachedAirlineDTO cachedAirlineDTO;
						// if result set is not null
						if (rs != null) {
							// populate recordMap
							while (rs.next()) {
								cachedAirlineDTO = new CachedAirlineDTO();
								cachedAirlineDTO.setAirlineCode(rs.getString("AIRLINE_CODE"));
								cachedAirlineDTO.setCountryCode(rs.getString("COUNTRY_CODE"));
								mapAirlines.put(cachedAirlineDTO.getAirlineCode(), cachedAirlineDTO);
							}
						}
						return mapAirlines;
					}
				});
			}
			return retMap;
		} catch (DataAccessException e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

}
