/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created in Oct 09,2008
 * $Id: GdsServiceDelegateImpl.java,v 1.0 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airmaster.api.service.GdsBD;

/**
 * @author Haider
 */
@Local
public interface GdsServiceLocalDelegateImpl extends GdsBD {

}
