package com.isa.thinair.airmaster.core.util;

import com.isa.thinair.airmaster.api.dto.SmartSearchDTO;
import com.isa.thinair.airmaster.api.model.SmartSearchIndex;

public class SearchServiceUtil {

	public static void filterSearchData(SmartSearchIndex search) {

		if (search != null) {
			search.setSearchIndexText(search.getSearchIndexText().trim().toUpperCase());
		}
	}

	public static SmartSearchIndex transform(SmartSearchDTO searchDTO) {

		SmartSearchIndex smartSearch = new SmartSearchIndex();
		smartSearch.setSearchIndexText(searchDTO.getSearchText());

		return smartSearch;
	}

}
