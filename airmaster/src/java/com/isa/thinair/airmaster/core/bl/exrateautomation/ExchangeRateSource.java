package com.isa.thinair.airmaster.core.bl.exrateautomation;

import java.util.Map;

import com.isa.thinair.airmaster.api.dto.ExchangeRateInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Chamara Jayaweera
 * 
 */
public interface ExchangeRateSource {
	public Map<String, ExchangeRateInfoDTO> getExchangeRates() throws ModuleException;
}
