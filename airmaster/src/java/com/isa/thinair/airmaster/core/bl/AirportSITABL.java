package com.isa.thinair.airmaster.core.bl;

/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airmaster.api.model.SITAAddress;
import com.isa.thinair.airmaster.core.persistence.dao.AirportDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

/**
 * Class to implement the bussiness logic related to the Flight
 * 
 * @author Byorn
 */
public class AirportSITABL {

	private AirportDAO airportdao;

	private Log log = LogFactory.getLog(AirportSITABL.class);

	public AirportSITABL() {
		LookupService lookup = LookupServiceFactory.getInstance();
		airportdao = (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");

	}

	public void saveSITAAddress(SITAAddress address) throws ModuleException {
		if (address.getSitaAddress() != null) {
			// when inserting check if sita address has been used previously
			address.setSitaAddress(address.getSitaAddress().toUpperCase());

			// if(address.getVersion()<0){
			// checkIfSitaAddressHasBeenUsed(address.getSitaAddress());
			// }

			// when updating the status
			if (address.getVersion() >= 0)

				if (airportCodeOrActveStatusHasChanged(address)) {
					{
						checkIfSitaAddressHasBeenUsed(address.getSitaAddress());
					}

				}
		}

		// checkIfSitaAddressIsUnique(address);

		airportdao.saveSITAAddress(address);
		if (address.getActiveStatus() != SITAAddress.ACTIVE_STATUS_YES) {
			checkForActiveSITAForAirport(address.getAirportCode());
		}

	}

	private boolean airportCodeOrActveStatusHasChanged(SITAAddress address) throws ModuleException {
		SITAAddress oldSitaAddress = airportdao.getSITAAddress(new Integer(address.getAirportSITAId()));
		boolean hasChanged = false;

		if (oldSitaAddress != null) {

			if ((oldSitaAddress.getSitaAddress() != null)
					&& (!address.getSitaAddress().equals(oldSitaAddress.getSitaAddress().toUpperCase()))) {
				checkIfSitaAddressHasBeenUsed(oldSitaAddress.getSitaAddress());
				hasChanged = true;
				return hasChanged;
			}

			if (address.getAirportCode() != null && oldSitaAddress.getAirportCode() != null) {
				if (!address.getAirportCode().equals(oldSitaAddress.getAirportCode())) {
					hasChanged = true;
					return hasChanged;
				}
			}

			if (oldSitaAddress.getAirportCode() != null && address.getAirportCode() == null) {
				hasChanged = true;
				return hasChanged;

			}

		} else {
			throw new ModuleException("module.update.delete.failed", "airmaster.desc");
		}

		return hasChanged;

	}

	public void deleteSITAAddress(int sitaId) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("deleteSITAAddress()" + sitaId);
		}
		SITAAddress address = airportdao.getSITAAddress(new Integer(sitaId));
		// Check for null before check any dependancies
		if ((address != null) && (address.getSitaAddress() != null))
			checkIfSitaAddressHasBeenUsed(address.getSitaAddress());
		airportdao.removeSITAAddress(sitaId);
		// checkForActiveSITAForAirport(address.getAirportCode());
	}

	public void deleteSITAAddress(SITAAddress address) throws ModuleException {
		if ((address.getSitaAddress() != null)) {
			if (log.isDebugEnabled()) {
				log.debug("deleteSITAAddress()" + address.getSitaAddress());
			}
			SITAAddress address1 = airportdao.getSITAAddress(new Integer(address.getAirportSITAId()));
			checkIfSitaAddressHasBeenUsed(address1.getSitaAddress());
		}
		airportdao.removeSITAAddress(address);
		// checkForActiveSITAForAirport(address.getAirportCode());
	}

	private void checkIfSitaAddressHasBeenUsed(String sitaAddress) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("checkIfSitaAddressHasBeenUsed sita address : -" + sitaAddress);
		}
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sql = "select count(*) from T_PNLADL_TX_HISTORY t where upper(t.SITA_ADDRESS) like '" + sitaAddress.toUpperCase()
				+ "'";

		Integer count = (Integer) jdbcTemplate.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = null;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));
				}

				return count;
			}

		});

		if (count.intValue() != 0) {
			log.error("Sita address has been used to send messeges" + sitaAddress);
			throw new ModuleException("sita.is.used");
		}

	}

	private void checkForActiveSITAForAirport(String airportCode) throws ModuleException {
		if (!airportdao.hasActiveSITAAddress(airportCode)) {
			throw new ModuleException("sita.active.error");
		}

	}

	public static DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

}
