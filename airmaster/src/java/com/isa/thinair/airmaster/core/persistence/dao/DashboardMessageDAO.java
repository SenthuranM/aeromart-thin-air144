/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:14:52
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.core.persistence.dao;

import java.util.List;
import java.util.Set;

import com.isa.thinair.airmaster.api.criteria.DashBoardMsgSearchCriteria;
import com.isa.thinair.airmaster.api.model.DashboardMessage;
import com.isa.thinair.airmaster.api.model.DashbrdMsgAgent;
import com.isa.thinair.airmaster.api.model.DashbrdMsgAgentPOS;
import com.isa.thinair.airmaster.api.model.DashbrdMsgAgentType;
import com.isa.thinair.airmaster.api.model.DashbrdMsgUser;
import com.isa.thinair.commons.api.dto.Page;

public interface DashboardMessageDAO {

	Page searchDashboardMessages(DashBoardMsgSearchCriteria dashBoardMsgSearchCriteria, int startRec, int noRecs);

	public Integer saveMessage(DashboardMessage message);

	public DashboardMessage getDashboardMessage(Integer id);

	public List<DashboardMessage> getNewDashbrdMsgIDs();

	public void updateDashboardNewMessageStatus(List<Integer> msgIds, String msgStatus);

	public void saveAgentsForMessage(Set<DashbrdMsgAgent> agent);

	public void savePOSForMessage(Set<DashbrdMsgAgentPOS> pos);

	public void saveUsersForMessage(Set<DashbrdMsgUser> user);

	public void saveAgentTypesForMessage(Set<DashbrdMsgAgentType> agentType);

}
