package com.isa.thinair.airmaster.core.bl.exrateautomation;

public class ExRateAutomateConfigImpl {
	private ExchangeRateSource exRateSource;

	public ExchangeRateSource getExRateSource() {
		return exRateSource;
	}

	public void setExRateSource(ExchangeRateSource exRateSource) {
		this.exRateSource = exRateSource;
	}

}
