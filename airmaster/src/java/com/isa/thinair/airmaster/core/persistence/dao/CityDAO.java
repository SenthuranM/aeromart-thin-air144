package com.isa.thinair.airmaster.core.persistence.dao;

import java.util.List;

import com.isa.thinair.airmaster.api.model.City;

public interface CityDAO {

	public City getCity(String cityCode);

	public List<City> getLCCUnPublishedCities();

	public void saveCity(City city);

	public List<String> getCityCodes();

}
