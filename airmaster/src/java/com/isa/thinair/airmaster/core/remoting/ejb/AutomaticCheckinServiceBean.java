package com.isa.thinair.airmaster.core.remoting.ejb;

import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.isa.thinair.airmaster.api.criteria.AutomaticCheckinTemplatesSearchCriteria;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.dto.AutomaticCheckinDTO;
import com.isa.thinair.airmaster.api.model.AutoCheckinTemplateAirport;
import com.isa.thinair.airmaster.api.model.AutomaticCheckinTemplate;
import com.isa.thinair.airmaster.core.bl.AutoCheckinBL;
import com.isa.thinair.airmaster.core.persistence.dao.AutomaticCheckinDAO;
import com.isa.thinair.airmaster.core.service.bd.AutomaticCheckinServiceDelegateImpl;
import com.isa.thinair.airmaster.core.service.bd.AutomaticCheckinServiceLocalDelegateImpl;
import com.isa.thinair.airmaster.core.util.LookUpUtils;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;

/**
 * @author aravinth.r
 *
 */
@Stateless
@RemoteBinding(jndiBinding = "AutomaticCheckinService.remote")
@LocalBinding(jndiBinding = "AutomaticCheckinService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class AutomaticCheckinServiceBean extends PlatformBaseSessionBean implements AutomaticCheckinServiceDelegateImpl,
		AutomaticCheckinServiceLocalDelegateImpl {

	AutoCheckinBL autoCheckinBL;
	/**
	 * getting bean from lookup utils to avoid duplicate code
	 */
	private AutomaticCheckinDAO autoCheckinDAO = LookUpUtils.getAutoCheckinDAO();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isa.thinair.airmaster.api.service.AutomaticCheckinBD#getAutomaticCheckinTemplate(int)
	 */
	@Override
	public AutomaticCheckinTemplate getAutomaticCheckinTemplate(int templateId) throws ModuleException {
		return autoCheckinDAO.getAutomaticCheckinTemplate(templateId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isa.thinair.airmaster.api.service.AutomaticCheckinBD#getAutomaticCheckinTemplates(int, int,
	 * com.isa.thinair.airmaster.api.dto.AutomaticCheckinTemplatesSearchCriteria)
	 */
	@Override
	public Page<AutomaticCheckinTemplate> getAutomaticCheckinTemplates(int startRec, int numOfRecs,
			AutomaticCheckinTemplatesSearchCriteria actSearchCriteria) throws ModuleException {
		return autoCheckinDAO.getAutomaticCheckinTemplates(startRec, numOfRecs, actSearchCriteria);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.isa.thinair.airmaster.api.service.AutomaticCheckinBD#updateAutomaticCheckinTemplate(com.isa.thinair.airmaster
	 * .api.model.AutomaticCheckinTemplate)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public AutomaticCheckinTemplate updateAutomaticCheckinTemplate(AutomaticCheckinTemplate automaticCheckinTemplate)
			throws ModuleException {
		return autoCheckinDAO.updateAutomaticCheckinTemplate(automaticCheckinTemplate);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isa.thinair.airmaster.api.service.AutomaticCheckinBD#getAvailableAutomaticCheckins(java.util.Map)
	 */
	@Override
	public Map<AirportServiceKeyTO, List<AutomaticCheckinDTO>> getAvailableAutomaticCheckins(
			Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseSegMap) throws ModuleException {

		return getAutoCheckinBL().getAvailableAutomaticCheckins(airportWiseSegMap);
	}

	/**
	 * Creating BL object when required
	 * 
	 * @return
	 */
	private AutoCheckinBL getAutoCheckinBL() {
		if (autoCheckinBL == null) {
			autoCheckinBL = new AutoCheckinBL();
		}

		return autoCheckinBL;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void saveAutomaticCheckinTemplate(AutomaticCheckinTemplate autoCheckinTemplate) throws ModuleException {
		autoCheckinTemplate = (AutomaticCheckinTemplate) setUserDetails(autoCheckinTemplate);
		autoCheckinDAO.saveAutomaticCheckinTemplate(autoCheckinTemplate);
	}

	@Override
	public List<AutomaticCheckinDTO> getAutomaticCheckinDetails(String airportCode) throws ModuleException {
		return getAutoCheckinBL().getAutomaticCheckinDetails(airportCode);
	}

	@Override
	public List<AutoCheckinTemplateAirport> getAutomaticCheckinAirports(int autoCheckinTempId) throws ModuleException {
		return autoCheckinDAO.getAutomaticCheckinAirports(autoCheckinTempId);
	}

	@Override
	public List<String> getAutoCheckinAirportList(List<String> airportList) throws ModuleException {
		return autoCheckinDAO.getAutoCheckinAirportList(airportList);
	}
}
