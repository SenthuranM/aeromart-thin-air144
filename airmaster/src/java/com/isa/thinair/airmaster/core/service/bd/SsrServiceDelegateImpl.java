package com.isa.thinair.airmaster.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airmaster.api.service.SsrBD;

/**
 * 
 * @author Dhanya
 */
@Remote
public interface SsrServiceDelegateImpl extends SsrBD {

}
