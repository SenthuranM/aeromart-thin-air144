package com.isa.thinair.airmaster.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airmaster.api.service.SmartSearchBD;

@Remote
public interface SearchServiceImpl extends SmartSearchBD {

}
