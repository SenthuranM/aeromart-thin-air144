/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:14:52
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.core.persistence.dao;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airmaster.api.dto.CurrencyExchangeRateDTO;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface CurrencyDAO {

	public List<Currency> getCurrencies();

	public List<Currency> getCurrencies(boolean bookVisibility);

	public Page getCurrencies(int startRec, int numOfRec);

	public Page getCurrencies(List<ModuleCriterion> criteria, int startRec, int numOfRec);

	public Currency getCurrency(String currencyCode);

	public void saveCurrency(Currency currency);

	public void removeCurrency(String currencyCode);

	public void removeCurrency(Currency currency);

	public List<Currency> getActiveCurrencies();

	public void saveOrUpdateCurrencyExRates(Collection<CurrencyExchangeRate> currencyExRates);

	public BigDecimal getExchangeRateValue(String currencyCode, Date exRateEffectiveDate, Boolean isPurchaseExRate);

	public CurrencyExchangeRate getExchangeRate(String currencyCode, Date exRateEffectiveDate);

	public Collection<CurrencyExchangeRate> getAllUpdatedCurr(Date effFrom);

	public Collection<CurrencyExchangeRate> getAllUpdatedCurrForCharges(Date date) throws ModuleException;

	public Collection<CurrencyExchangeRate> getAllUpdatedCurrFares(Date date) throws ModuleException;

	public Collection<Currency> getExchangeRateAutomatedCurrencies() throws ModuleException;

	public List<CurrencyExchangeRate> getExchangeRatesForAutomatedUpdate(Date exRateEffectiveDate, String Option);

	public List<CurrencyExchangeRateDTO> getExchangeRates() throws ModuleException;
}
