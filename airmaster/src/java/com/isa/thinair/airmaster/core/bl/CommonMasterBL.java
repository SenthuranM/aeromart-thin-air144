/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.core.bl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airmaster.api.dto.BundleFareCategoryDTO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.model.LogicalCabinClass;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.I18nMessage;
import com.isa.thinair.airmaster.api.model.I18nMessageKey;
import com.isa.thinair.airmaster.api.model.Language;
import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.airmaster.api.model.RouteTransitsInfo;
import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.airmaster.core.persistence.dao.CommonMasterDAO;
import com.isa.thinair.airmaster.core.persistence.dao.HubDAO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.util.I18nTranslationUtil.I18nMessageCategory;
import com.isa.thinair.commons.core.framework.Tracking;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.DataConverterUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
/*******************************************************************************
 * Description : Common Master Business Logic
 * 
 * @author : Nadika Dhanusha Mahatantila
 * @since : Feb 22, 2008
 ******************************************************************************/

public class CommonMasterBL {

	private static Log log = LogFactory.getLog(CommonMasterBL.class);

	/**
	 * This method is used to Save/Update information to the Database.
	 * 
	 * @param routeInfo
	 * @param userId
	 * @throws ModuleException
	 */
	public void saveRouteInfo(RouteInfo routeInfo, String userId) throws ModuleException {

		// Checks the validity of the Legs of a non direct flight
		if (routeInfo.getDirectFlg().equalsIgnoreCase(RouteInfo.DIRECT_FALSE)) {
			boolean isValidLegCodes = isValidLegs(routeInfo);

			if (!isValidLegCodes) {
				throw new ModuleException("airmaster.invalid.legcodes", "airmaster.desc");
			}
		}

		try {
			if (routeInfo.getVersion() >= 0) {
				RouteInfo routeInfoNew = lookupCommonMasterDAO().getRouteInfo(routeInfo);

				if (routeInfoNew == null)
					throw new ModuleException("module.hibernate.objectnotfound");

				if (log.isInfoEnabled()) {
					if (routeInfoNew.getDirectFlg().equalsIgnoreCase(RouteInfo.DIRECT_TRUE) && isDirectRouteUsage(routeInfoNew)) {
						log.debug("CommonMasterBL.saveRouteInfo() - Direct Route Usage");
					}
				}

				// Set the editable values
				routeInfoNew.setFromAirportCode(routeInfo.getFromAirportCode());
				routeInfoNew.setToAirportCode(routeInfo.getToAirportCode());
				routeInfoNew.setDistance(routeInfo.getDistance());
				routeInfoNew.setDuration(routeInfo.getDuration());
				routeInfoNew.setDurationTolerance(routeInfo.getDurationTolerance());
				routeInfoNew.setStatus(routeInfo.getStatus());
				routeInfoNew.setRouteCode(routeInfo.getRouteCode());
				// Setting LCC Published Status @author Navod
				routeInfoNew.setIsLCCPubilshed(RouteInfo.LCC_PUBLISHED_FALSE);

				// Temp collection to have the details
				Set<RouteTransitsInfo> temp = new HashSet<RouteTransitsInfo>();

				// checking on the child information
				Collection<RouteTransitsInfo> routeTransitsInfoCol = routeInfo.getRouteTransits();
				Collection<RouteTransitsInfo> routeTransitsInfoColNew = null;
				Iterator<RouteTransitsInfo> itrDetails = routeTransitsInfoCol.iterator();

				while (itrDetails.hasNext()) {
					RouteTransitsInfo routeTransitsInfo = (RouteTransitsInfo) itrDetails.next();

					routeTransitsInfoColNew = routeInfoNew.getRouteTransits();
					Iterator<RouteTransitsInfo> itrDetailsNew = routeTransitsInfoColNew.iterator();

					while (itrDetailsNew.hasNext()) {

						RouteTransitsInfo routeTransitsInfoNew = (RouteTransitsInfo) itrDetailsNew.next();

						// comparing the id
						if (routeTransitsInfo.getRouteTransitId() == routeTransitsInfoNew.getRouteTransitId()) {
							routeTransitsInfoNew.setRouteInfo(routeInfo);
							routeTransitsInfoNew.setTransitAirportCode(routeTransitsInfo.getTransitAirportCode());
							routeTransitsInfoNew.setSeqNo(routeTransitsInfo.getSeqNo());

							temp.add(routeTransitsInfoNew);
							break;

						} else {
							temp.add(routeTransitsInfo);
							break;
						}
					}
				}

				routeTransitsInfoColNew = routeInfoNew.getRouteTransits();

				if ((routeTransitsInfoCol != null && !routeTransitsInfoCol.isEmpty())
						&& (routeTransitsInfoColNew == null || routeTransitsInfoColNew.isEmpty())) {
					temp.addAll(routeTransitsInfoCol);
				}

				routeInfoNew.getRouteTransits().clear();
				routeInfoNew.getRouteTransits().addAll(temp);
				routeInfoNew.setVersion(routeInfo.getVersion());

				// tracking User details
				routeInfoNew = (RouteInfo) setUserDetails(routeInfoNew, userId);

				// Update the record in the database.
				lookupCommonMasterDAO().saveRouteInfo(routeInfoNew);

			} else {

				// tracking User details
				routeInfo = (RouteInfo) setUserDetails(routeInfo, userId);

				// Insert the record to the database.
				lookupCommonMasterDAO().saveRouteInfo(routeInfo);
			}

			// Checks whether the origin or destination or both are hubs in a direct flight
			// If so, creates Routes
			if (routeInfo.getDirectFlg().equalsIgnoreCase(RouteInfo.DIRECT_TRUE)) {
				createRoutes(routeInfo);
			}
		} catch (CommonsDataAccessException cdaex) {
			if (cdaex != null && cdaex.getExceptionCode() != null) {
				throw new ModuleException(cdaex.getExceptionCode());
			} else {
				throw cdaex;
			}
		}
	}

	/**
	 * This method is used to Delete information from the Database.
	 * 
	 * @param routeCode
	 * @throws ModuleException
	 */
	public void removeRouteInfo(String routeCode) throws ModuleException {

		RouteInfo routeInfo = lookupCommonMasterDAO().getRouteInfo(routeCode);

		if (isRoutesInUse(routeInfo)) {
			throw new ModuleException("airmaster.directrouteusage.error", "airmaster.desc");
		}

		if (routeInfo.getDirectFlg().equalsIgnoreCase(RouteInfo.DIRECT_TRUE) && isDirectRouteUsage(routeInfo)) {
			// lookupCommonMasterDAO().removeRouteInfo(lookupCommonMasterDAO().getDirectRouteUsageList(routeCode));
			Collection<RouteInfo> routeInfoList = lookupCommonMasterDAO().getDirectRouteUsageList(routeCode);
			if (routeInfoList != null) {
				RouteInfo routeInfoDirect = null;
				for (Iterator<RouteInfo> iterator = routeInfoList.iterator(); iterator.hasNext();) {
					routeInfoDirect = (RouteInfo) iterator.next();
					routeInfoDirect.setStatus(RouteInfo.STATUS_INACTIVE);
					lookupCommonMasterDAO().saveRouteInfo(routeInfoDirect);
				}
			}
			log.debug("CommonMasterBL.removeRouteInfo(String routeCode) - Direct Route Usage");
		}
		routeInfo.setStatus(RouteInfo.STATUS_INACTIVE);
		routeInfo.setIsLCCPubilshed(RouteInfo.LCC_PUBLISHED_FALSE);
		lookupCommonMasterDAO().saveRouteInfo(routeInfo);
		// lookupCommonMasterDAO().removeRouteInfo(routeCode);
	}

	/**
	 * This method is used to Delete information from the Database.
	 * 
	 * @param routeInfo
	 * @throws ModuleException
	 */
	public void removeRouteInfo(RouteInfo routeInfo) throws ModuleException {

		if (isRoutesInUse(routeInfo)) {
			throw new ModuleException("airmaster.directrouteusage.error", "airmaster.desc");
		}

		if (routeInfo.getDirectFlg().equalsIgnoreCase(RouteInfo.DIRECT_TRUE) && isDirectRouteUsage(routeInfo)) {
			// lookupCommonMasterDAO().removeRouteInfo(lookupCommonMasterDAO().getDirectRouteUsageList(routeInfo.getRouteCode()));
			Collection<RouteInfo> routeInfoList = lookupCommonMasterDAO().getDirectRouteUsageList(routeInfo.getRouteCode());
			if (routeInfoList != null) {
				RouteInfo routeInfoDirect = null;
				for (Iterator<RouteInfo> iterator = routeInfoList.iterator(); iterator.hasNext();) {
					routeInfoDirect = (RouteInfo) iterator.next();
					routeInfoDirect.setStatus(RouteInfo.STATUS_INACTIVE);
					lookupCommonMasterDAO().saveRouteInfo(routeInfoDirect);
				}
			}
			log.debug("CommonMasterBL.removeRouteInfo(RouteInfo routeInfo) - Direct Route Usage");
		}
		routeInfo.setStatus(RouteInfo.STATUS_INACTIVE);
		routeInfo.setIsLCCPubilshed(RouteInfo.LCC_PUBLISHED_FALSE);
		lookupCommonMasterDAO().saveRouteInfo(routeInfo);
		lookupCommonMasterDAO().removeRouteInfo(routeInfo);
	}

	/**
	 * Checks whether the Legs of the Non direct flight are valid
	 * 
	 * @param routeInfo
	 * @return boolean
	 */
	private boolean isValidLegs(RouteInfo routeInfo) {
		Collection<String> ondCodeCol = getLegCodes(routeInfo);
		int noOfLegs = routeInfo.getNoOfTransits() + 1;

		Integer count = lookupCommonMasterDAO().getValidLegCount(ondCodeCol);

		if (count == null || count.intValue() != noOfLegs) {
			return false;
		} else if (count.intValue() == noOfLegs) {
			return true;
		}

		return false;
	}

	/**
	 * Gets the Leg Codes
	 * 
	 * @param routeInfo
	 * @return Collection
	 */
	public Collection<String> getLegCodes(RouteInfo routeInfo) {
		Collection<String> legCodeCol = new ArrayList<String>();
		String strOndCode = routeInfo.getRouteCode();
		String[] arrSegments = strOndCode.split("/");

		for (int i = 0; i < (arrSegments.length - 1); i++) {
			legCodeCol.add(arrSegments[i] + "/" + arrSegments[i + 1]);
		}

		return legCodeCol;
	}

	/**
	 * Creates Routes automatically, if there is a hub in the leg
	 * 
	 * @param routeInfo
	 * @param userId
	 * @throws ModuleException
	 */
	private void createRoutes(RouteInfo routeInfo) throws ModuleException {
		AirmasterUtils.getCommonMasterBD().generateRoutes(routeInfo);
	}

	/**
	 * Generates the routes, if there is a hub in the leg
	 * 
	 * @param routeInfo
	 * @param userId
	 */
	public void generateRoutes(RouteInfo routeInfo, String userId) {
		Collection<String> validHubCodes = null;
		Collection<String> airportCodes = new ArrayList<String>();

		airportCodes.add(routeInfo.getFromAirportCode());
		airportCodes.add(routeInfo.getToAirportCode());

		validHubCodes = lookupHubDAO().getValidHubCodes(airportCodes);

		if (validHubCodes != null && validHubCodes.size() > 0) {

			Iterator<String> iter = validHubCodes.iterator();
			String strHubCode = null;
			// Collection<RouteInfo> newRoutesCol = new ArrayList<RouteInfo>();
			// Set<RouteInfo> tempRoutesCol = new HashSet<RouteInfo>();
			Map<String, RouteInfo> newRoutesMap = new HashMap<String, RouteInfo>();
			Map<String, RouteInfo> tempRoutesMap = new HashMap<String, RouteInfo>();

			while (iter.hasNext()) {
				strHubCode = (String) iter.next();

				// If origin is a hub
				if (routeInfo.getFromAirportCode().equals(strHubCode.trim())) {

					// Gets two leg routes, based on the hub being either origin or destination
					Collection<RouteInfo> twoLegRoutes = lookupCommonMasterDAO().getTwoLegRoutes(routeInfo.getRouteCode(), true);

					// Gets the direct routes
					Collection<RouteInfo> directRoutesCol = lookupCommonMasterDAO().getDirectRoutes(strHubCode,
							RouteInfo.COL_KEY_DESTINATION);

					if (directRoutesCol != null) {
						Iterator<RouteInfo> iterCol = directRoutesCol.iterator();

						while (iterCol.hasNext()) {
							RouteInfo routeInfor = (RouteInfo) iterCol.next();
							RouteInfo newRoutInfo = createNewRouteInfo(routeInfo, routeInfor, true);
							if (newRoutInfo != null) {
								// tempRoutesCol.add(newRoutInfo);
								tempRoutesMap.put(newRoutInfo.getRouteCode(), newRoutInfo);
							}
						}
					}

					// Remove the already existing routes
					for (Iterator<RouteInfo> itr = twoLegRoutes.iterator(); itr.hasNext();) {
						RouteInfo obj = (RouteInfo) itr.next();
						tempRoutesMap.remove(obj.getRouteCode());
					}
					// tempRoutesCol.removeAll(twoLegRoutes);

					newRoutesMap.putAll(tempRoutesMap);
					// newRoutesCol.addAll(tempRoutesCol);

				} // If destination is a hub
				else if (routeInfo.getToAirportCode().equals(strHubCode.trim())) {

					// Gets two leg routes, based on the hub being either origin or destination
					Collection<RouteInfo> twoLegRoutes = lookupCommonMasterDAO().getTwoLegRoutes(routeInfo.getRouteCode(), false);

					// Gets the direct routes
					Collection<RouteInfo> directRoutesCol = lookupCommonMasterDAO().getDirectRoutes(strHubCode,
							RouteInfo.COL_KEY_ORIGIN);

					if (directRoutesCol != null) {
						Iterator<RouteInfo> iterCol = directRoutesCol.iterator();

						while (iterCol.hasNext()) {
							RouteInfo routeInfor = (RouteInfo) iterCol.next();
							RouteInfo newRoutInfo = createNewRouteInfo(routeInfo, routeInfor, false);
							if (newRoutInfo != null) {
								// tempRoutesCol.add(newRoutInfo);
								tempRoutesMap.put(newRoutInfo.getRouteCode(), newRoutInfo);
							}
						}
					}

					// Remove the already existing routes
					for (Iterator<RouteInfo> itr = twoLegRoutes.iterator(); itr.hasNext();) {
						RouteInfo obj = (RouteInfo) itr.next();
						tempRoutesMap.remove(obj.getRouteCode());
					}
					// tempRoutesCol.removeAll(twoLegRoutes);

					newRoutesMap.putAll(tempRoutesMap);
					// newRoutesCol.addAll(tempRoutesCol);

				}

			}

			// tracking User details

			if (newRoutesMap != null) {

				Iterator<RouteInfo> iterCol = newRoutesMap.values().iterator();

				while (iterCol.hasNext()) {
					setUserDetails((RouteInfo) iterCol.next(), userId);
				}
			}

			lookupCommonMasterDAO().saveGeneratedRoutes(newRoutesMap.values());

		}

	}

	/**
	 * Creates the new routes
	 * 
	 * @param newRouteInfo
	 * @param routeInfo
	 * @param isOrigin
	 * @return RouteInfo
	 */
	private RouteInfo createNewRouteInfo(RouteInfo newRouteInfo, RouteInfo routeInfo, boolean isOrigin) {
		RouteInfo info = new RouteInfo();
		RouteTransitsInfo routeTransitsInfo = null;

		if (isOrigin) {
			info.setFromAirportCode(routeInfo.getFromAirportCode());
			info.setToAirportCode(newRouteInfo.getToAirportCode());
			if (newRouteInfo.getRouteCode().indexOf(routeInfo.getFromAirportCode()) != -1) {
				return null;
			} else {
				info.setRouteCode(routeInfo.getFromAirportCode() + "/" + newRouteInfo.getRouteCode());
			}

			routeTransitsInfo = new RouteTransitsInfo(newRouteInfo.getFromAirportCode(), 1);

		} else {
			info.setFromAirportCode(newRouteInfo.getFromAirportCode());
			info.setToAirportCode(routeInfo.getToAirportCode());

			if (newRouteInfo.getRouteCode().indexOf(routeInfo.getToAirportCode()) != -1) {
				return null;
			} else {
				info.setRouteCode(newRouteInfo.getRouteCode() + "/" + routeInfo.getToAirportCode());
			}

			routeTransitsInfo = new RouteTransitsInfo(newRouteInfo.getToAirportCode(), 1);
		}

		info.setDistance(AccelAeroCalculator.add(newRouteInfo.getDistance(), routeInfo.getDistance()));
		info.setDuration(newRouteInfo.getDuration() + routeInfo.getDuration());

		if (newRouteInfo.getDurationTolerance() != null) {
			info.setDurationTolerance(AccelAeroCalculator.add(newRouteInfo.getDurationTolerance(),
					routeInfo.getDurationTolerance()));
		}

		info.setStatus(RouteInfo.STATUS_ACTIVE);
		info.setDirectFlg(RouteInfo.DIRECT_FALSE);
		// LCC Published Status @author Navod Ediriweera
		info.setIsLCCPubilshed(RouteInfo.LCC_PUBLISHED_FALSE);
		info.setNoOfTransits(1);

		List<RouteTransitsInfo> routeTransitsInfoList = new ArrayList<RouteTransitsInfo>();
		routeTransitsInfo.setRouteInfo(info);
		routeTransitsInfoList.add(routeTransitsInfo);
		info.setRouteTransits(routeTransitsInfoList);

		return info;
	}

	/**
	 * Checks whether the route has been used to create connections
	 * 
	 * @param routeInfo
	 * @return boolean
	 */
	private boolean isDirectRouteUsage(RouteInfo routeInfo) {
		Integer count = lookupCommonMasterDAO().getDirectRouteUsageCount(routeInfo.getRouteCode());

		if (count == null || count.intValue() == 0) {
			return false;
		}

		return true;
	}

	private boolean isRoutesInUse(RouteInfo routeInfo) {
		return lookupCommonMasterDAO().isRoutInUse(routeInfo);
	}

	/**
	 * Set user details
	 * 
	 * @param userID
	 * @param tracking
	 * @return Tracking
	 */
	public Tracking setUserDetails(Tracking tracking, String userID) {
		if (tracking.getVersion() < 0) {
			tracking.setCreatedBy(userID == null ? "" : userID);
			tracking.setCreatedDate(new Date());
		} else {
			tracking.setModifiedBy(userID == null ? "" : userID);
			tracking.setModifiedDate(new Date());
		}
		return tracking;
	}

	/**
	 * Creates a Set<String> of I18nMessageKey IDs and passes it along to be deleted.
	 * 
	 * @param logicalCC
	 *            LogicalCabinClass containing the {@link I18nMessageKey} IDs.
	 */
	public void deleteI18nMessages(LogicalCabinClass logicalCC) {
		Set<String> keySet = new HashSet<String>();
		keySet.add(logicalCC.getDescriptionI18n());
		keySet.add(logicalCC.getFlexiDescriptionI18n());
		keySet.add(logicalCC.getCommentsI18n());
		keySet.add(logicalCC.getFlexiCommentI18n());
		lookupCommonMasterDAO().deleteI18nMessages(keySet);
	}

	/**
	 * Creates and saves {@link I18nMessageKey} classes and {@link I18nMessage} classes for the required I18nMessageKey
	 * IDs stored in the LogicalCabinClass
	 * 
	 * @param logicalCabinClass
	 *            With the I18nMessageKey IDs
	 */
	public void saveI18nMessageKeys(LogicalCabinClass logicalCabinClass) {

		Collection<I18nMessageKey> messageKeys = new ArrayList<I18nMessageKey>();
		Collection<Language> langauges = lookupCommonMasterDAO().getLanguages();
		String messageCategory = I18nMessageCategory.MCC_DES.toString();

		I18nMessageKey descriptionI18nKey = new I18nMessageKey();
		descriptionI18nKey.setI18nMsgKey(logicalCabinClass.getDescriptionI18n());
		descriptionI18nKey.setMessageCategory(messageCategory);

		I18nMessageKey flexiDescI18nKey = new I18nMessageKey();
		flexiDescI18nKey.setI18nMsgKey(logicalCabinClass.getFlexiDescriptionI18n());
		flexiDescI18nKey.setMessageCategory(messageCategory);

		I18nMessageKey commentsI18nKey = new I18nMessageKey();
		commentsI18nKey.setI18nMsgKey(logicalCabinClass.getCommentsI18n());
		commentsI18nKey.setMessageCategory(messageCategory);

		I18nMessageKey flexiCommentsI18nKey = new I18nMessageKey();
		flexiCommentsI18nKey.setI18nMsgKey(logicalCabinClass.getFlexiCommentI18n());
		flexiCommentsI18nKey.setMessageCategory(messageCategory);

		for (Language language : langauges) {
			I18nMessage descMessage = new I18nMessage();
			descMessage.setMessageLocale(language.getLanguageCode());
			descMessage
					.setMessageContent(DataConverterUtil.createClob(StringUtil.convertToHex(logicalCabinClass.getDescription().trim())));
			descriptionI18nKey.addI18nMessage(descMessage);

			I18nMessage flexiDesc = new I18nMessage();
			flexiDesc.setMessageLocale(language.getLanguageCode());
			flexiDesc.setMessageContent(DataConverterUtil.createClob(StringUtil.convertToHex(logicalCabinClass.getFlexiDescription()
					.trim())));
			flexiDescI18nKey.addI18nMessage(flexiDesc);

			I18nMessage comments = new I18nMessage();
			comments.setMessageLocale(language.getLanguageCode());
			comments.setMessageContent(DataConverterUtil.createClob(StringUtil.convertToHex(logicalCabinClass.getComments().trim())));
			commentsI18nKey.addI18nMessage(comments);

			I18nMessage flexiComments = new I18nMessage();
			flexiComments.setMessageLocale(language.getLanguageCode());
			flexiComments.setMessageContent(DataConverterUtil.createClob(StringUtil.convertToHex(logicalCabinClass.getFlexiComment()
					.trim())));
			flexiCommentsI18nKey.addI18nMessage(flexiComments);
		}

		messageKeys.add(descriptionI18nKey);
		messageKeys.add(commentsI18nKey);
		messageKeys.add(flexiDescI18nKey);
		messageKeys.add(flexiCommentsI18nKey);

		lookupCommonMasterDAO().saveOrUpdateI18nMessageKeys(messageKeys);
	}

	/**
	 * Updates the LogicalCabinClass field translations. Updating the English version only.
	 * 
	 * @param logicalCabinClass
	 *            With the I18nMessageKey IDs
	 */
	public void updateI18nMessageKeys(LogicalCabinClass lcc) {

		Map<String, Map<String, String>> trMap = new HashMap<String, Map<String, String>>();
		trMap.put(lcc.getCommentsI18n(), new HashMap<String, String>());
		trMap.put(lcc.getDescriptionI18n(), new HashMap<String, String>());
		trMap.put(lcc.getFlexiCommentI18n(), new HashMap<String, String>());
		trMap.put(lcc.getFlexiDescriptionI18n(), new HashMap<String, String>());

		trMap.get(lcc.getDescriptionI18n()).put(Locale.ENGLISH.toString(), lcc.getDescription());
		trMap.get(lcc.getCommentsI18n()).put(Locale.ENGLISH.toString(), lcc.getComments());
		trMap.get(lcc.getFlexiDescriptionI18n()).put(Locale.ENGLISH.toString(), lcc.getFlexiDescription());
		trMap.get(lcc.getFlexiCommentI18n()).put(Locale.ENGLISH.toString(), lcc.getFlexiComment());

		saveTranslations(trMap);
	}

	public void updateI18nMessageKeys(BundleFareCategoryDTO bundledFareCategory){
		Map<String,BundleFareCategoryDTO.BundledFareCategoryTranslation> bundleCategoryTranslations = bundledFareCategory.getTranslations();

		Map<String, Map<String, String>> tranlsationMap = new HashMap<String, Map<String, String>>();
		tranlsationMap.put(bundledFareCategory.getBundledCategoryId().toString(), new HashMap<String, String>());


//		tranlsationMap.put(bundledFareCategory.getBundledCategoryId(), new HashMap<String, String>());
//		tranlsationMap.put(lcc.getFlexiCommentI18n(), new HashMap<String, String>());
//		tranlsationMap.put(lcc.getFlexiDescriptionI18n(), new HashMap<String, String>());
//
		for(String language: bundleCategoryTranslations.keySet()){
			String id = bundledFareCategory.getBundledCategoryId().toString();
			tranlsationMap.get(id).put(language, bundledFareCategory.getDescription());
			tranlsationMap.get(id).put(language, bundledFareCategory.getCategoryName());
		}
//		tranlsationMap.get(lcc.getCommentsI18n()).put(Locale.ENGLISH.toString(), lcc.getComments());
//		tranlsationMap.get(lcc.getFlexiDescriptionI18n()).put(Locale.ENGLISH.toString(), lcc.getFlexiDescription());
//		tranlsationMap.get(lcc.getFlexiCommentI18n()).put(Locale.ENGLISH.toString(), lcc.getFlexiComment());

		saveTranslations(tranlsationMap);

	}

	/**
	 * Loads I18nMessageKey and I18nMessage classes and updates message content.
	 * 
	 * @param translations
	 *            A map in the form of Map<MessageKey,Map<Locale,Translated Messages>>
	 * 
	 * @throws IllegalArgumentException
	 *             If any of the passed message keys doesn't already exist. In such a scenario consider using
	 *             {@link #saveTranslations(Map, String, String)}
	 */
	public void saveTranslations(Map<String, Map<String, String>> translations) {
		Collection<I18nMessageKey> messageKeys = new ArrayList<I18nMessageKey>();

		for (String msgKey : translations.keySet()) {

			I18nMessageKey i18nKey = lookupCommonMasterDAO().getI18nMessageKey(msgKey);

			if (i18nKey == null) {
				throw new IllegalArgumentException("Invalid I18nMessageKey. The provided key doesn't exist.");
			}

			i18nKey = prepareTranslationDataForSaving(translations.get(msgKey), i18nKey);

			messageKeys.add(i18nKey);
		}

		lookupCommonMasterDAO().saveOrUpdateI18nMessageKeys(messageKeys);
	}

	public void saveTranslationWithoutMessageKey(long id, Map<String, String> translations) {

		Collection<I18nMessageKey> msgKeys = new ArrayList<I18nMessageKey>();
		I18nMessageKey i18nKey = lookupCommonMasterDAO().getI18nMessageKey("PRM_DES" + id);
		if (i18nKey == null) {
			I18nMessageKey msgKey = new I18nMessageKey();
			msgKey.setI18nMsgKey("PRM_DES" + id);
			msgKey.setMessageCategory("PRM_DES");
			// msgKey.setVersion(1);
			Set<I18nMessage> msgSet = new HashSet<I18nMessage>();
			for (String lang : translations.keySet()) {

				I18nMessage msg = new I18nMessage();
				msg.setMessageLocale(lang);
				msg.setMessageContent(DataConverterUtil.createClob(StringUtil.convertToHex(translations.get(lang))));
				// msg.setMessageId(Long.parseLong(innerValues[3]));
				// msg.setI18nMessageKey(msgKey);
				msgKey.addI18nMessage(msg);

			}

			msgKeys.add(msgKey);
		} else {
			for (String lang : translations.keySet()) {
				I18nMessage msg = null;
				if (i18nKey.getI18nMessage(lang) != null) {
					msg = i18nKey.getI18nMessage(lang);
				} else {
					msg = new I18nMessage();
					msg.setMessageLocale(lang);
				}
				msg.setMessageContent(DataConverterUtil.createClob(StringUtil.convertToHex(translations.get(lang))));
				i18nKey.addI18nMessage(msg);
			}
			msgKeys.add(i18nKey);
		}
		lookupCommonMasterDAO().saveOrUpdateI18nMessageKeys(msgKeys);
	}

	/**
	 * 
	 * Loads I18nMessageKey and I18nMessage classes and updates message content. If the message key deosn't exist a new
	 * key will be created.
	 * 
	 * @param translations
	 *            A Map<String,String> containing the translations. Key -> Translation Language Value -> Translated Text
	 * @param i18nMessageKey
	 *            The message key for the translation.
	 * @param i18nMessageCategory
	 *            The category of the message key.
	 */
	public void saveTranslations(Map<String, String> translations, String i18nMessageKey, String i18nMessageCategory) {

		I18nMessageKey i18nKey = lookupCommonMasterDAO().getI18nMessageKey(i18nMessageKey);

		if (i18nKey == null) {
			i18nKey = new I18nMessageKey();
			i18nKey.setI18nMsgKey(i18nMessageKey);
			i18nKey.setMessageCategory(i18nMessageCategory);
		}

		i18nKey = prepareTranslationDataForSaving(translations, i18nKey);

		lookupCommonMasterDAO().saveOrUpdateI18nMessageKeys(Arrays.asList(i18nKey));
	}

	/**
	 * Populates the given {@link I18nMessageKey} with the translation data. Text will be converted to Hexadecimal and
	 * wrapped in a Clob.
	 * 
	 * @param translations
	 *            A Map<String,String> containing the translations. Key -> Translation Language Value -> Translated Text
	 * @param i18nKey
	 *            The Message key to be populated with the data.
	 * @return
	 */
	private I18nMessageKey prepareTranslationDataForSaving(Map<String, String> translations, I18nMessageKey i18nKey) {

		for (Map.Entry<String, String> translationEntry : translations.entrySet()) {
			String language = translationEntry.getKey();
			I18nMessage msg = i18nKey.getI18nMessage(language);
			msg.setMessageContent(DataConverterUtil.createClob(StringUtil.convertToHex(translationEntry.getValue())));
			i18nKey.addI18nMessage(msg);
		}

		return i18nKey;
	}

	/**
	 * Returns translated messages in one language for the passed Set of i18n message keys.
	 * 
	 * @param language
	 *            Language the translations are required
	 * @param keySet
	 *            Set of keys to which the translations are required
	 * @return Results containing the translations in the form of Map<i18n message key,translated message>
	 * @throws ModuleException
	 *             If any of the underlying operations throws an exception
	 */
	public Map<String, String> getTranslatedMessages(String language, Set<String> keySet) {
		return lookupCommonMasterDAO().getTranslatedMessages(language, keySet);
	}

	/**
	 * Returns all the language translations for LogicalCabinClass
	 * 
	 * @return Map that will hold the translations in the format of Map<i18n message key,Map<language,message content>>
	 */
	public Map<String, Map<String, String>> getTranslatedMessagesForLCC() {
		return lookupCommonMasterDAO().getTranslatedMessagesForLCC();
	}

	/**
	 * Returns all the language translations for the given keys
	 * 
	 * @param msgKeys
	 *            i18n message keys whose translations need to be retrieved.
	 * 
	 * @return Map that will hold the translations in the format of Map<i18n message key,Map<language,message content>>
	 */
	public Map<String, Map<String, String>> getTranslatedMessagesForKeys(Collection<String> msgKeys) {
		return msgKeys.isEmpty() ? new HashMap<String, Map<String, String>>() : lookupCommonMasterDAO()
				.getTranslatedMessagesForKeys(msgKeys);
	}

	/**
	 * Returns all the language translations based on the category
	 * 
	 * @param category
	 * @return
	 */
	public Map<String, Map<String, String>> getTranslatedMessages(String category) {
		return lookupCommonMasterDAO().getTranslatedMessages(category);
	}

	public Country getCountryByIpAddress(long originIp) throws ModuleException {
		boolean isCachingDbEnabled = CommonsServices.getAerospikeCachingService().getSystemEnabled();
		boolean isIpToCountryEnabled = AppSysParamsUtil.isAerospikeIPToCountryEnabled();

		Country country = null;
		if (isCachingDbEnabled && isIpToCountryEnabled) {
			String countryCode = CommonsServices.getAerospikeCachingService().getCountryById(originIp);
			if (countryCode != null && !"".equals(countryCode)) {
				country = AirmasterDAOUtil.getCommonMasterDAO().getCountryByCode(countryCode);
			}
		}

		// If caching DB enabled and country is null or caching DB disabled, load from normal DB
		if (country == null) {
			country = AirmasterDAOUtil.getCommonMasterDAO().getCountryFromIPAddress(originIp);
		}
		return country;
	}
	
	/**
	 * 
	 * @param ondInfoTOList
	 * @param requestingCarrier
	 * @return
	 * @throws ModuleException
	 */
	public SYSTEM getSearchSystem(List<OriginDestinationInformationTO> ondInfoTOList, String requestingCarrier)
			throws ModuleException {
		List<String> originDestinations = new ArrayList<String>();
		for (OriginDestinationInformationTO ondInfo : ondInfoTOList) {
			String originDestination = ondInfo.getOrigin().concat("/").concat(ondInfo.getDestination());
			originDestinations.add(originDestination);
		}			
		com.isa.thinair.commons.core.service.AerospikeCachingService.SYSTEM system = CommonsServices.getAerospikeCachingService()
				.getSearchSystem(originDestinations, requestingCarrier);
		if (system == com.isa.thinair.commons.core.service.AerospikeCachingService.SYSTEM.INT) {
			return SYSTEM.INT;
		}
		return SYSTEM.AA;
	}

	/**
	 * Getting the Common Master DAO object
	 * 
	 * @return CommonMasterDAO
	 */
	private CommonMasterDAO lookupCommonMasterDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (CommonMasterDAO) lookupService.getBean("isa:base://modules/airmaster?id=CommonMasterDAOProxy");
	}

	/**
	 * Getting the Hub DAO object
	 * 
	 * @return HubDAO
	 */
	private HubDAO lookupHubDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (HubDAO) lookupService.getBean("isa:base://modules/airmaster?id=HubDAOProxy");
	}

}
