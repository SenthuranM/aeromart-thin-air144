package com.isa.thinair.airmaster.core.bl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.core.util.CommonsServices;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.City;
import com.isa.thinair.airmaster.api.model.Station;
import com.isa.thinair.airmaster.api.model.Territory;
import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.airproxy.api.model.masterDataPublish.AirportDSTPublishRQDataTO;
import com.isa.thinair.airproxy.api.model.masterDataPublish.MasterDataPublishRQDataTO;
import com.isa.thinair.airproxy.api.model.masterDataPublish.MasterDataPublishRSDataTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public class MasterDataPublishBL {
	private static Log log = LogFactory.getLog(MasterDataPublishBL.class);

	public void publishMasterDataUpdates() throws ModuleException {
		List<Airport> unPublishedAirports = getUnPublishedAirports();
		List<Station> unPublishedStations = getUnPublishedStations();
		List<Territory> unPublishedTerritories = getUnPublishedTerritories();
		List<City> unPublishedCities = getUnPublishedCities();

		if ((unPublishedAirports != null && unPublishedAirports.size() > 0)
				|| (unPublishedStations != null && unPublishedStations.size() > 0)
				|| (unPublishedTerritories != null && unPublishedTerritories.size() > 0)
				|| (unPublishedCities != null && unPublishedCities.size() > 0)) {
			MasterDataPublishRSDataTO masterDataPublishRSDataTO = doWSCall(createRequestDataTO(unPublishedAirports,
					unPublishedStations, unPublishedTerritories, unPublishedCities));
			updateAirports(masterDataPublishRSDataTO, unPublishedAirports);
			updateStations(masterDataPublishRSDataTO, unPublishedStations);
			updateTerritories(masterDataPublishRSDataTO, unPublishedTerritories);
			updateCities(masterDataPublishRSDataTO, unPublishedCities);

			processErrors(masterDataPublishRSDataTO);
		}
	}


	private List<Airport> getUnPublishedAirports() throws ModuleException {
		return AirmasterUtils.getAirportBD().getLCCUnPublishedAirports();
	}

	private List<Station> getUnPublishedStations() throws ModuleException {
		return AirmasterUtils.getLocationBD().getLCCUnPublishedAirports();
	}

	private List<Territory> getUnPublishedTerritories() throws ModuleException {
		return AirmasterUtils.getLocationBD().getLCCUnPublishedTerritories();
	}
	
	private List<City> getUnPublishedCities() throws ModuleException {
		return AirmasterUtils.getLocationBD().getLCCUnPublishedCities();
	}

	private MasterDataPublishRQDataTO createRequestDataTO(List<Airport> unPublishedAirports, List<Station> unPublishedStations,
			List<Territory> unPublishedTerritories, List<City> unPublishedCities) throws ModuleException {
		MasterDataPublishRQDataTO masterDataPublishRQDataTO = new MasterDataPublishRQDataTO();
		if (unPublishedAirports != null && unPublishedAirports.size() > 0) {
			createAirportsToLccPublish(masterDataPublishRQDataTO, unPublishedAirports);
		}
		if (unPublishedStations != null && unPublishedStations.size() > 0) {
			masterDataPublishRQDataTO.getPublishStations().addAll(unPublishedStations);
		}
		if (unPublishedTerritories != null && unPublishedTerritories.size() > 0) {
			masterDataPublishRQDataTO.getPublishTerritories().addAll(unPublishedTerritories);
		}
		
		if (unPublishedCities != null && unPublishedCities.size() > 0) {
			masterDataPublishRQDataTO.getPublishCities().addAll(unPublishedCities);
		}

		return masterDataPublishRQDataTO;
	}

	private void
			createAirportsToLccPublish(MasterDataPublishRQDataTO masterDataPublishRQDataTO, List<Airport> unPublishedAirports)
					throws ModuleException {
		List<AirportDSTPublishRQDataTO> colAirportLists = new ArrayList<AirportDSTPublishRQDataTO>();
		Map<String, String> stateCodesMap = getStatesMap(unPublishedAirports);
		Iterator<Airport> iterator = unPublishedAirports.iterator();
		while (iterator.hasNext()) {
			Airport airport = iterator.next();
			airport.setStateCode(stateCodesMap.get(airport.getAirportCode()));
			AirportDSTPublishRQDataTO airportDSTPublishRQDataTO = new AirportDSTPublishRQDataTO();
			airportDSTPublishRQDataTO.setAirport(airport);
			// TODO this has to be taken from a configuration
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) - 1);

			// DST deletion is not allowed. Get newly created and mofied DST for publishing
			airportDSTPublishRQDataTO.setAirportDSTs(AirmasterUtils.getAirportBD().getLCCUnpublisedAirportDSTs(
					airport.getAirportCode(), calendar.getTime()));

			Collection<String> colAirPortCodes = new ArrayList<String>();
			colAirPortCodes.add(airport.getAirportCode());
			Map<String, CachedAirportDTO> mapAirports = new HashMap<String, CachedAirportDTO>();
			if (colAirPortCodes.size() != mapAirports.size()) {
				mapAirports = AirmasterUtils.getAirportBD().getCachedOwnAirportMap(colAirPortCodes);
				airportDSTPublishRQDataTO.setCountryCode(mapAirports.get(airport.getAirportCode()) != null ? mapAirports.get(
						airport.getAirportCode()).getCountryCode() : "");
			}
			colAirportLists.add(airportDSTPublishRQDataTO);
		}
		masterDataPublishRQDataTO.getPublishAirports().addAll(colAirportLists);

	}

	private Map<String, String> getStatesMap(List<Airport> unPublishedAirports) {
		List<String> airportCodes = new ArrayList<>();
		for (Airport airport : unPublishedAirports) {
			airportCodes.add(airport.getAirportCode());
		}
		return CommonsServices.getGlobalConfig().getStateCodesForAirport(airportCodes);
	}

	private MasterDataPublishRSDataTO doWSCall(MasterDataPublishRQDataTO airportPublishRQDataTO) throws ModuleException {
		try {
			return AirmasterUtils.getLCCMasterDataPublisherBD().publishMasterDataToLCC(airportPublishRQDataTO);
		} catch (Exception exception) {
			log.error("Failed while publishing airports");
			throw new ModuleException(exception, "lccclient.airportinfo.invocation.error");
		}
	}

	private void updateAirports(MasterDataPublishRSDataTO msterDataPublishRSDataTO, List<Airport> unPublishedAirports)
			throws ModuleException {
		List<String> updatedAirportCodes = msterDataPublishRSDataTO.getUpdatedAirportCodes();
		AirmasterUtils.getAirportBD().updateAirportLCCPublishStatus(unPublishedAirports, updatedAirportCodes);
	}

	private void updateStations(MasterDataPublishRSDataTO msterDataPublishRSDataTO, List<Station> unPublishedStations)
			throws ModuleException {
		List<String> updatedStationCodes = msterDataPublishRSDataTO.getUpdatedStationCodes();
		AirmasterUtils.getLocationBD().updateStationLCCPublishStatus(unPublishedStations, updatedStationCodes);
	}

	private void updateTerritories(MasterDataPublishRSDataTO msterDataPublishRSDataTO, List<Territory> unPublishedTerritories)
			throws ModuleException {
		List<String> updatedTerritoryCodes = msterDataPublishRSDataTO.getUpdatedTerritoryCodes();
		AirmasterUtils.getLocationBD().updateTerritoryLCCPublishStatus(unPublishedTerritories, updatedTerritoryCodes);
	}

	private void updateCities(MasterDataPublishRSDataTO masterDataPublishRSDataTO, List<City> unPublishedCities)
			throws ModuleException {
		List<String> updatedCityCodes = masterDataPublishRSDataTO.getUpdatedCityCodes();
		AirmasterUtils.getLocationBD().updateCityLCCPublishStatus(unPublishedCities, updatedCityCodes);
	}

	/**
	 * Process errors.
	 * 
	 * @param airportPublishRSDataTO
	 *            the airport publish rs data to
	 */
	private void processErrors(MasterDataPublishRSDataTO airportPublishRSDataTO) {
		if (airportPublishRSDataTO.getErrors().size() > 0) {
			if (log.isDebugEnabled())
				log.debug("############# Errors Present in LCC Airport Publishing - Displaying- ##################");
			for (Iterator<String> iterator = airportPublishRSDataTO.getErrors().iterator(); iterator.hasNext();) {
				String error = iterator.next();
				log.error("Error returned from LCC " + error);
			}
			if (log.isDebugEnabled())
				log.debug("############# End Displaying Errors in LCC Airport Publishing ##################");
		}
	}
}
