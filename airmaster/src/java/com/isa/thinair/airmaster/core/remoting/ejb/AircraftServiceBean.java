/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:29:42
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airinventory.api.model.FlightSeat;
import com.isa.thinair.airmaster.api.dto.AirCraftModelSeatsDTO;
import com.isa.thinair.airmaster.api.model.AirCraftModelSeats;
import com.isa.thinair.airmaster.api.model.Aircraft;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.api.model.SeatTemplateCOS;
import com.isa.thinair.airmaster.core.audit.AuditAirMaster;
import com.isa.thinair.airmaster.core.bl.AircraftModelBL;
import com.isa.thinair.airmaster.core.persistence.dao.AircraftDAO;
import com.isa.thinair.airmaster.core.service.bd.AircraftServiceDelegateImpl;
import com.isa.thinair.airmaster.core.service.bd.AircraftServiceLocalDelegateImpl;
import com.isa.thinair.airpricing.api.model.SeatCharge;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * @author Byorn
 */
@Stateless
@RemoteBinding(jndiBinding = "AircraftService.remote")
@LocalBinding(jndiBinding = "AircraftService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class AircraftServiceBean extends PlatformBaseSessionBean implements AircraftServiceDelegateImpl,
		AircraftServiceLocalDelegateImpl {
	private final Log log = LogFactory.getLog(getClass());

	@Override
	public void saveAircraft(Aircraft obj) throws ModuleException {

		try {

			lookupAircraftDAO().saveAircraft(obj);
			AuditAirMaster.doAudit(obj, getUserPrincipal());
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");

		}
	}

	@Override
	public void saveAircraftModel(AircraftModel obj) throws ModuleException {
		try {

			new AircraftModelBL().saveOrUpdate(obj, getUserPrincipal());

		}

		catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw e;
		} catch (CommonsDataAccessException cde) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cde.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}

	}

	@Override
	public void removeAircraft(int key) throws ModuleException {
		try {
			lookupAircraftDAO().removeAircraft(key);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	@Override
	public void removeAircraft(Aircraft aircraft) throws ModuleException {
		try {
			lookupAircraftDAO().removeAircraft(aircraft);
			AuditAirMaster.doAudit(aircraft, getUserPrincipal());
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	@Override
	public void removeAircraftModel(String key) throws ModuleException {
		try {
			AuditAirMaster.doAudit(key, AuditAirMaster.AIRCRAFT_MODEL, getUserPrincipal());
			lookupAircraftDAO().removeAircraftModel(key);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {

			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	@Override
	public void removeAircraftModel(AircraftModel aircraftModel) throws ModuleException {
		try {
			AuditAirMaster.doAudit(aircraftModel.getModelNumber(), AuditAirMaster.AIRCRAFT_MODEL, getUserPrincipal());
			lookupAircraftDAO().removeAircraftModel(aircraftModel);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, "airmaster.aircraftmodel.hasflights", "airmaster.desc");
		} catch (Exception e1) {

			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	@Override
	public List<Aircraft> getAircrafts() throws ModuleException {
		try {
			return lookupAircraftDAO().getAircrafts();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	@Override
	public Aircraft getAircraft(int AircraftId) throws ModuleException {
		try {
			return lookupAircraftDAO().getAircraft(AircraftId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	@Override
	public Page getAircrafts(int startRec, int noRecs) throws ModuleException {
		try {
			return lookupAircraftDAO().getAircrafts(startRec, noRecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	@Override
	public Page getAircraftModels(int startRec, int noRecs) throws ModuleException {
		try {
			return lookupAircraftDAO().getAircraftModels(startRec, noRecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	@Override
	public AircraftModel getAircraftModel(String aircraftmodel) throws ModuleException {
		try {
			return lookupAircraftDAO().getAircraftModel(aircraftmodel);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	@Override
	public List<AircraftModel> getModels() throws ModuleException {
		try {
			return lookupAircraftDAO().getModels();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	@Override
	public List<Aircraft> getActiveAircrafts() throws ModuleException {
		try {
			return lookupAircraftDAO().getActiveAircrafts();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public List<AircraftModel> getActiveAircraftModels() throws ModuleException {
		try {
			return lookupAircraftDAO().getActiveAircraftModels();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public List<AircraftModel> getAircraftModels(Collection<String> aircraftModelNumbers) throws ModuleException {
		try {
			return lookupAircraftDAO().getAircraftModels(aircraftModelNumbers);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public void saveAirCraftModelSeat(AirCraftModelSeats airCraftModelSeats) throws ModuleException {

		try {
			lookupAircraftDAO().saveAirCraftModelSeat(airCraftModelSeats);
		} catch (CommonsDataAccessException e) {

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");

		}
	}

	@Override
	public void saveAirCraftModelSeats(Collection<AirCraftModelSeats> airCraftModelSeats) throws ModuleException {
		for (AirCraftModelSeats airCraftModelSeat : airCraftModelSeats) {
			saveAirCraftModelSeat(airCraftModelSeat);
		}
	}

	@Override
	public AirCraftModelSeatsDTO getAirCraftModelSeats(String model) throws ModuleException {
		try {
			Collection<AirCraftModelSeats> col = lookupAircraftDAO().getAirCraftModelSeats(model);

			return new AircraftModelBL().getAirCraftModelSeatsDTO(col);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public Map<String, List<Integer>> getAirCraftModelSeatsSeatIDModelSeats(String modelNo) throws ModuleException {
		try {
			return lookupAircraftDAO().getAirCraftModelSeatList(modelNo);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public Map<Integer, AirCraftModelSeats> getNewReservedAircraftSeats(Integer flightSegId, String modelNo)
			throws ModuleException {
		try {
			return lookupAircraftDAO().getDestinationReservedAircraftSeats(flightSegId, modelNo);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public AirCraftModelSeats getAirCraftModelSeats(int seatID) throws ModuleException {
		try {
			return lookupAircraftDAO().getAirCraftModelSeats(seatID);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	private AircraftDAO lookupAircraftDAO() {

		LookupService lookupService = LookupServiceFactory.getInstance();
		return (AircraftDAO) lookupService.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");

	}

	@Override
	public Map<Integer, Integer> getVersionList(Integer templateId) {
		return lookupAircraftDAO().getVersionList(templateId);
	}

	@Override
	public void saveSeatLogicalCabinClass(List<Integer> seatIdList, Integer templateId,
			Collection<SeatTemplateCOS> seatTemplateCOSList) throws ModuleException {
		try {
			lookupAircraftDAO().saveSeatLogicalCabinClass(seatIdList, templateId, seatTemplateCOSList);
		} catch (CommonsDataAccessException e) {
			log.error(e.getMessageString());
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getExceptionCode());
		}
	}

	@Override
	public boolean isAllowUpdateLogicalCabinClass(ArrayList<Integer> seatList, Integer templateId) throws ModuleException {
		return lookupAircraftDAO().isAllowUpdateLogicalCabinClass(seatList, templateId);
	}

	@Override
	public Map<Integer, String> getFlightSeatData(ArrayList<Integer> seatList, Integer flightSegmentId) {
		return lookupAircraftDAO().getFlightSeatData(seatList, flightSegmentId);
	}

	public List<AirCraftModelSeats> getFlightSeatData(List<Integer> seatList) {
		return lookupAircraftDAO().getFlightSeatData(seatList);
	}

	public Map<Integer, String> getFlightSeatData(Set<Integer> flightIdList) {
		return lookupAircraftDAO().getFlightSeatData(flightIdList);
	}

	@Override
	public List<SeatTemplateCOS> getAttachedSeatTemplateData(int flightSegmentId) throws ModuleException {
		return lookupAircraftDAO().getAttachedSeatTemplateData(flightSegmentId);
	}

	@Override
	public List<FlightSeat> getFlightSeatData(int flightSegmentId) throws ModuleException {
		return lookupAircraftDAO().getFlightSeatData(flightSegmentId);
	}

	@Override
	public List<SeatCharge> getAttachedSeatChargeData(int flightSegmentId) throws ModuleException {
		return lookupAircraftDAO().getAttachedSeatChargeData(flightSegmentId);
	}

	public Map<String, Integer> getHiddenSeatsMap(String modelNo) throws ModuleException {
		return lookupAircraftDAO().getHiddenSeatMap(modelNo);
	}

	@Override
	public String getAffectedFlights(AircraftModel obj) throws ModuleException {

		return new AircraftModelBL().getAffectedFiles(obj);

	}

	@Override
	public String getAffectedFlights(AircraftModel obj, Integer flightNo) throws ModuleException {

		return new AircraftModelBL().getAffectedFiles(obj, flightNo);

	}
	
	@Override
	public List<AircraftCabinCapacity> getAircraftCabinCapacity(String flightNo, Date flightDate) throws ModuleException {
		try {
			return lookupAircraftDAO().getAircraftCabinCapacity(flightNo, flightDate);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	@Override
	public List<AircraftModel> getAllActiveAircraftsByIataType(String iataType) throws ModuleException {
		try {
			return lookupAircraftDAO().getAllActiveAircraftsByIataType(iataType);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public boolean isDefaultAircraftAssignedForIataAircraftType(String iataType, String modelNumber) throws ModuleException {
		try {
			return lookupAircraftDAO().isDefaultAircraftAssignedForIataAircraftType(iataType, modelNumber);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}
	
}
