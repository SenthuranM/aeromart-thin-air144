package com.isa.thinair.airmaster.core.persistence.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.Baggage;
import com.isa.thinair.airmaster.api.model.I18nMessage;

public class BaggageUtil {

	private static Log log = LogFactory.getLog(BaggageUtil.class);

	public static void convertClobToString(Collection<Baggage> baggages) {
		for (Object baggageObj : baggages) {
			Baggage airMsg = (Baggage) baggageObj;
			if (airMsg.getI18nMessageKey() != null && airMsg.getI18nMessageKey().getI18nMessages() != null) {
				for (Object i18nMsgObj : airMsg.getI18nMessageKey().getI18nMessages()) {
					I18nMessage i18nMsg = (I18nMessage) i18nMsgObj;
					String message = "";
					if (i18nMsg.getMessageContent() != null) {
						StringBuffer strOut = new StringBuffer();
						String aux;
						try {
							BufferedReader br = new BufferedReader(i18nMsg.getMessageContent().getCharacterStream());
							while ((aux = br.readLine()) != null) {
								strOut.append(aux);
							}
						} catch (IOException e) {
							log.error("IOException in reading Clob message", e);
						} catch (SQLException se) {
							log.error("SQL Exception in reading Clob message", se);
						}

						message = strOut.toString();
					}
					i18nMsg._setMsgContent(message);

				}
			}
		}

	}

}
