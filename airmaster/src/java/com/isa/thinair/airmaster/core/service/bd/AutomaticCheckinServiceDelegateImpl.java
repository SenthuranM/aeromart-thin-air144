package com.isa.thinair.airmaster.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airmaster.api.service.AutomaticCheckinBD;

/**
 * @author aravinth.r
 *
 */

@Remote
public interface AutomaticCheckinServiceDelegateImpl extends AutomaticCheckinBD {

}
