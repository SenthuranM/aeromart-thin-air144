package com.isa.thinair.airmaster.core.remoting.ejb;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.dto.AirportTransferDTO;
import com.isa.thinair.airmaster.api.model.AirportTransfer;
import com.isa.thinair.airmaster.api.model.AirportTransferTemplate;
import com.isa.thinair.airmaster.api.model.AirportTransferTemplateBC;
import com.isa.thinair.airmaster.core.persistence.dao.AirportTransferDAO;
import com.isa.thinair.airmaster.core.service.bd.AirportTransferServiceDelegateImpl;
import com.isa.thinair.airmaster.core.service.bd.AirportTransferServiceLocalDelegateImpl;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * @author Manoj Dhanushka
 */
@Stateless
@RemoteBinding(jndiBinding = "AirportTransferService.remote")
@LocalBinding(jndiBinding = "AirportTransferService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class AirportTransferServiceBean extends PlatformBaseSessionBean implements AirportTransferServiceDelegateImpl,
AirportTransferServiceLocalDelegateImpl {
	
	@Override
	public AirportTransfer getAirportTransfer(String airportCode) throws ModuleException {
		return lookupAirportTransferDAO().getAirportTransfer(airportCode);
	}
	
	@Override
	public AirportTransfer getAirportTransfer(int id) throws ModuleException {
		return lookupAirportTransferDAO().getAirportTransfer(id);
	}

	@Override
	public Page<AirportTransfer> getAirportTransfers(int startRec, int numOfRecs, String airportCode, String status)
			throws ModuleException {
		return lookupAirportTransferDAO().getAirportTransfers(startRec, numOfRecs, airportCode, status);
	}

	@Override
	public Page<AirportTransferTemplate> getAirportTransferTemplates(int startRec, int numOfRecs,
			AirportTransferDTO airportTransferDTO) throws ModuleException {
		return lookupAirportTransferDAO().getAirportTransferTemplates(startRec, numOfRecs, airportTransferDTO);
	}

	@Override
	public void saveAirportTransfers(AirportTransfer airportTransfer) throws ModuleException {
		airportTransfer.setUserDetails(getUserPrincipal());
		lookupAirportTransferDAO().saveAirportTransfers(airportTransfer);
	}
	
	@Override
	public void saveAirportTransferTemplate(AirportTransferTemplate airportTransferTemplate) throws ModuleException {
		lookupAirportTransferDAO().saveAirportTransferTemplate(airportTransferTemplate);
	}

	@Override
	public Map<AirportServiceKeyTO, List<AirportServiceDTO>> getAvailableAirportTransfers(
			Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseSegMap, Map<Integer, Boolean> csOCFlightMapping)
			throws ModuleException {
		return getAvailableApts(airportWiseSegMap, csOCFlightMapping);
	}
	
	@Override
	public Map<String,Integer> getAirportTransferBookingClasses(int airportTransferTempId){
		return lookupAirportTransferDAO().getAirportTransferBookingClasses(airportTransferTempId);
	}
	
	@Override
	public void updateAirportTransferTemplate(AirportTransferTemplate airportTransferTemplate) throws ModuleException{
		lookupAirportTransferDAO().updateAirportTransferTemplate(airportTransferTemplate);
	}
	
	private AirportTransferDAO lookupAirportTransferDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (AirportTransferDAO) lookupService.getBean("isa:base://modules/airmaster?id=AirportTransferDAOProxy");
	}
	
	private Map<AirportServiceKeyTO, List<AirportServiceDTO>> getAvailableApts(
			Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseSegMap, Map<Integer, Boolean> csOCFlightMapping) {
		Map<AirportServiceKeyTO, List<AirportServiceDTO>> airportWiseMap = new LinkedHashMap<AirportServiceKeyTO, List<AirportServiceDTO>>();
		for (Entry<AirportServiceKeyTO, FlightSegmentTO> entry : airportWiseSegMap.entrySet()) {
			List<AirportServiceDTO> airportServiceDTOs = new ArrayList<AirportServiceDTO>();
			AirportServiceKeyTO keyTO = entry.getKey();
			FlightSegmentTO fltSeg = entry.getValue();
			if ((csOCFlightMapping != null && !csOCFlightMapping.get(fltSeg.getFlightSegId())) || csOCFlightMapping == null) {
				List<AirportServiceDTO> lst = lookupAirportTransferDAO().getAvailableAirportTransfers(keyTO, fltSeg);

				if (lst.size() > 0) {
					airportServiceDTOs.addAll(lst);
				}

				if (!airportWiseMap.isEmpty()) {
					boolean matchFound = false;
					for (Entry<AirportServiceKeyTO, List<AirportServiceDTO>> airportWiseMapEntry : airportWiseMap.entrySet()) {
						AirportServiceKeyTO aptKeyTO = airportWiseMapEntry.getKey();
						if (aptKeyTO.getAirport().equals(keyTO.getAirport())
								&& aptKeyTO.getAirportType().equals(keyTO.getAirportType())
								&& aptKeyTO.getDepartureDateTime() == keyTO.getDepartureDateTime()) {
							airportWiseMapEntry.getValue().addAll(airportServiceDTOs);
							matchFound = true;
						}
					}
					if (!matchFound) {
						airportWiseMap.put(keyTO, airportServiceDTOs);
					}
				} else {
					airportWiseMap.put(keyTO, airportServiceDTOs);
				}
			}
		}
		return airportWiseMap;
	}
}
