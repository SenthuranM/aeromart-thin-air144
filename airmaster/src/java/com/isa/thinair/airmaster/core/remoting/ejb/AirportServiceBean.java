/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:29:42
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.core.remoting.ejb;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.TransactionTimeout;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airmaster.api.criteria.AirportMsgSearchCriteria;
import com.isa.thinair.airmaster.api.criteria.SitaSearchCriteria;
import com.isa.thinair.airmaster.api.dto.CachedAirlineDTO;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airmaster.api.dto.SegmentCarrierDTO;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.AirportCheckInTime;
import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airmaster.api.model.AirportDSTHistory;
import com.isa.thinair.airmaster.api.model.AirportForDisplay;
import com.isa.thinair.airmaster.api.model.AirportTerminal;
import com.isa.thinair.airmaster.api.model.MealNotifyTimings;
import com.isa.thinair.airmaster.api.model.SITAAddress;
import com.isa.thinair.airmaster.api.to.AirportDstTO;
import com.isa.thinair.airmaster.api.to.AirportMessageTO;
import com.isa.thinair.airmaster.api.to.OperationStatusTO;
import com.isa.thinair.airmaster.api.util.MasterDataPublishUtil;
import com.isa.thinair.airmaster.core.audit.AuditAirMaster;
import com.isa.thinair.airmaster.core.bl.AirmasterDAOUtil;
import com.isa.thinair.airmaster.core.bl.AirportBL;
import com.isa.thinair.airmaster.core.bl.AirportMessageBL;
import com.isa.thinair.airmaster.core.bl.AirportSITABL;
import com.isa.thinair.airmaster.core.persistence.dao.AirportDAO;
import com.isa.thinair.airmaster.core.persistence.dao.TranslationDAO;
import com.isa.thinair.airmaster.core.service.bd.AirportServiceDelegateImpl;
import com.isa.thinair.airmaster.core.service.bd.AirportServiceLocalDelegateImpl;
import com.isa.thinair.airmaster.core.util.AirportCommonUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * @author Byorn
 */
@Stateless
@RemoteBinding(jndiBinding = "AirportService.remote")
@LocalBinding(jndiBinding = "AirportService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class AirportServiceBean extends PlatformBaseSessionBean implements AirportServiceDelegateImpl,
		AirportServiceLocalDelegateImpl {
	
	private static Log log = LogFactory.getLog(AirportServiceBean.class);	

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveAirport(Airport obj) throws ModuleException {
		try {
			obj = (Airport) setUserDetails(obj);
			AuditAirMaster.doAudit(obj, getUserPrincipal());
			lookupAirportDAO().saveAirport(obj);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveMealNotifyTiming(MealNotifyTimings mealNotifyTiming) throws ModuleException {
		try {
			lookupAirportDAO().saveMealNotifyTiming(mealNotifyTiming);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveAirport(Airport obj, Collection<AirportForDisplay> airportForDisplay, MealNotifyTimings mealNotifyTiming)
			throws ModuleException {
		try {
			obj = (Airport) setUserDetails(obj);
			AuditAirMaster.doAudit(obj, getUserPrincipal());
			lookupAirportDAO().saveAirport(obj);
			lookupAirportDAO().saveMealNotifyTiming(mealNotifyTiming);
			lookupTranslationDAO().saveAirports(airportForDisplay);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 * @ejb.interface-method view-type="both"
	 * @ejb.transaction type="Required"
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@TransactionTimeout(900)
	public OperationStatusTO addNewDST(AirportDST obj, FlightAlertDTO alertDTO) throws ModuleException {
		try {

			obj = (AirportDST) setUserDetails(obj);

			AuditAirMaster.doAudit(obj, getUserPrincipal());
			return new AirportBL().addNewAirportDST(obj, alertDTO);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@TransactionTimeout(900)
	public OperationStatusTO disableAndRollBack(AirportDST obj, FlightAlertDTO alertDTO) throws ModuleException {
		try {
			obj = (AirportDST) setUserDetails(obj);
			AuditAirMaster.doAudit(obj, getUserPrincipal());
			return new AirportBL().disbleAirportDST(obj, alertDTO);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void removeAirport(String key, boolean isMealNotifyEnable, int mealNotifyId) throws ModuleException {
		try {
			AuditAirMaster.doAudit(key, AuditAirMaster.AIRPORT, getUserPrincipal());
			if (isMealNotifyEnable && (mealNotifyId > 0)) {
				lookupTranslationDAO().removeMealNotifyTiming(mealNotifyId);
			}
			lookupAirportDAO().removeAirport(key);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void removeAirport(Airport airport, boolean isMealNotifyEnable, int mealNotifyId) throws ModuleException {
		try {
			AuditAirMaster.doAudit(airport.getAirportCode(), AuditAirMaster.AIRPORT, getUserPrincipal());
			if (isMealNotifyEnable && (mealNotifyId > 0)) {
				lookupTranslationDAO().removeMealNotifyTiming(mealNotifyId);
			}
			lookupAirportDAO().removeAirport(airport);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Airport> getAirports() throws ModuleException {
		try {
			return lookupAirportDAO().getAirports();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Airport getAirport(String airportId) throws ModuleException {
		try {
			return lookupAirportDAO().getAirport(airportId);
		} catch (CommonsDataAccessException e) {
			log.error("###Error getting airport- airportId:" + airportId,  e);
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page getAirports(int startRec, int noRecs, String countryCode, String airportCode, String airportName)
			throws ModuleException {
		try {
			return lookupAirportDAO().getAirports(startRec, noRecs, countryCode, airportCode, airportName);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean hasPreviousDST(String airportCode) throws ModuleException {
		try {
			return lookupAirportDAO().hasDSTRecord(airportCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<AirportDstTO> getAirportDSTs(String airportcode) throws ModuleException {
		try {
			return new AirportBL().getAirportDSTs(airportcode);
		} catch (ModuleException e) {

			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public AirportDST getCurrentAirportDST(String airportcode) throws ModuleException {
		try {
			return new AirportBL().getCurrentAirportDST(airportcode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}

	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public AirportDST getEffectiveAirportDST(String airportcode, Date effectiveDate) throws ModuleException {
		try {
			return lookupAirportDAO().getEffectiveDST(airportcode, effectiveDate);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}

	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page getOfflineAirports(int startRec, int noRecs) throws ModuleException {
		try {
			return lookupAirportDAO().getOfflineAirports(startRec, noRecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}

	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page getOnlineAirports(int startRec, int noRecs) throws ModuleException {
		try {
			return lookupAirportDAO().getOnlineAirports(startRec, noRecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Airport> getAirportsForCountry(String countryCode) throws ModuleException {
		try {
			return lookupAirportDAO().getAirportsForCountry(countryCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}

	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deleteAirportDST(AirportDST airportDST) throws ModuleException {
		try {
			AuditAirMaster.doAudit(String.valueOf(airportDST.getDstCode()), AuditAirMaster.AIRPORT_DST, getUserPrincipal());
			lookupAirportDAO().removeAirportDST(airportDST);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Airport> getActiveAirports() throws ModuleException {
		try {
			return lookupAirportDAO().getActiveAirports();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}

	}

	/** SITA METHODS **/

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<SITAAddress> getSITAAddresses(String airportCode) throws ModuleException {
		try {
			return lookupAirportDAO().getSITAAddresses(airportCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public SITAAddress getSITAAddress(Integer id) throws ModuleException {
		try {
			return lookupAirportDAO().getSITAAddress(id);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public SITAAddress getPrimarySITAAddress(String airportCode) throws ModuleException {
		try {
			return lookupAirportDAO().getPrimarySITAAdress(airportCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<SITAAddress> getActiveSITAAddresses(String airportCode, String carrierCode, DCS_PAX_MSG_GROUP_TYPE msgType)
			throws ModuleException {
		try {
			return lookupAirportDAO().getActiveSITAAddresses(airportCode, carrierCode, msgType);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveSITAAddress(SITAAddress sita) throws ModuleException {

		try {
			AuditAirMaster.doAudit(sita, getUserPrincipal());
			new AirportSITABL().saveSITAAddress(sita);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e1, e1.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void removeSITAAddress(int sitaAddressId) throws ModuleException {
		try {
			AuditAirMaster.doAudit(String.valueOf(sitaAddressId), AuditAirMaster.SITA_ADDRESS, getUserPrincipal());
			new AirportSITABL().deleteSITAAddress(sitaAddressId);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException cde) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cde, cde.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e1.getMessage());
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void removeSITAAddress(SITAAddress address) throws ModuleException {
		try {
			AuditAirMaster.doAudit(String.valueOf(address.getAirportSITAId()), AuditAirMaster.SITA_ADDRESS, getUserPrincipal());
			new AirportSITABL().deleteSITAAddress(address);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException cde) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cde, cde.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page getSITAAddresses(SitaSearchCriteria criteria, int rownum, int pagesize) throws ModuleException {
		try {

			return lookupAirportDAO().getSITAAddresses(criteria, rownum, pagesize);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	private AirportDAO lookupAirportDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (AirportDAO) lookupService.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
	}

	private TranslationDAO lookupTranslationDAO() {
		TranslationDAO dao = null;
		try {
			LookupService lookupService = LookupServiceFactory.getInstance();
			dao = (TranslationDAO) lookupService.getBean("isa:base://modules/airmaster?id=TranslationDAOProxy");
		} catch (Exception ex) {
			System.out.println("ERROR IN LOOKUP");
		}

		return dao;
	}

	@Override
	public List<Airport> getLCCUnPublishedAirports() throws ModuleException {
		try {
			return lookupAirportDAO().getLCCUnpublisedAirports();
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public void updateAirportLCCPublishStatus(List<Airport> originalAirports, List<String> updatedAirportCodes)
			throws ModuleException {
		for (Iterator<String> iterator = updatedAirportCodes.iterator(); iterator.hasNext();) {
			String airportCode = iterator.next();
			Airport airport = lookupAirportDAO().getAirport(airportCode);
			Object tmpAirport = MasterDataPublishUtil.retrieveMasterDataFromUnpublishedData(airportCode, originalAirports);
			if (tmpAirport != null) {
				Airport previousAirport = (Airport) tmpAirport;
				if (airport.getVersion() == previousAirport.getVersion()) {
					airport.setLccPublishStatus(Util.LCC_PUBLISHED);
					lookupAirportDAO().saveAirport(airport);
				}
			}

		}
	}

	@Override
	public List<AirportDST> getLCCUnpublisedAirportDSTs(String airportCode, Date fromWhen) throws ModuleException {
		try {
			return lookupAirportDAO().getLCCUnpublisedAirportDSTs(airportCode, fromWhen);
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public AirportTerminal getDefaultTerminal(String airportCode) throws ModuleException {
		try {
			return lookupAirportDAO().getDefaultTerminal(airportCode);
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public boolean compareCountryOfAirports(String firstAirportCode, String secondAirportCode) throws ModuleException {
		try {
			return lookupAirportDAO().compareCountryOfAirports(firstAirportCode, secondAirportCode);
		} catch (Exception ex) {
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public AirportCheckInTime getAirportCheckInTime(String airportCode, String flightNumber) throws ModuleException {
		try {
			// Temp fix. Sanjaya please fix this
			if ("DR".equals(AppSysParamsUtil.getDefaultCarrierCode())) {
				return lookupAirportDAO().getAirportCheckInTime(airportCode, flightNumber);
			}
			return null;
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public int getAirportCheckInTimeGap(String airportCode, String flightNumber) throws ModuleException {
		try {
			int checkInDiff = 0;
			AirportCheckInTime checkInTime = lookupAirportDAO().getAirportCheckInTime(airportCode, flightNumber);
			if (checkInTime != null && checkInTime.getCheckInGap() > 0) {
				checkInDiff = checkInTime.getCheckInGap();
			} else {
				checkInDiff = AppSysParamsUtil.getTotalMinutes(AppSysParamsUtil.getCheckInTimeDifference());
			}

			return checkInDiff;
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	@Override
	public int getAirportCheckInClosingTimeGap(String airportCode, String flightNumber) throws ModuleException {
		try {
			int checkInClosingTime = AppSysParamsUtil.getTotalMinutes(AppSysParamsUtil.getCheckInClosingTime());
			// Temp fix. Sanjaya please fix this
			if ("DR".equals(AppSysParamsUtil.getDefaultCarrierCode())) {
				AirportCheckInTime checkInTime = lookupAirportDAO().getAirportCheckInTime(airportCode, flightNumber);
				if (checkInClosingTime != 0 && checkInTime != null) {
					if (checkInTime.getCheckInClosingTime() > 0) {
						checkInClosingTime = checkInTime.getCheckInClosingTime();
					}
				}
			}
			return checkInClosingTime;
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<AirportCheckInTime> getAirportCITs(String airportCode) throws ModuleException {

		try {
			return new AirportBL().getAirportCITs(airportCode);
		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 * @ejb.interface-method view-type="both"
	 * @ejb.transaction type="Required"
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void addNewCIT(AirportCheckInTime obj) throws ModuleException {
		try {

			// obj = (AirportCheckInTime)setUserDetails(obj);
			// Do we need to add audit
			// AuditAirMaster.doAudit(obj, getUserPrincipal());
			new AirportBL().addNewAirportCIT(obj);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 * @ejb.interface-method view-type="both"
	 * @ejb.transaction type="Required"
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void addNewTerminal(AirportTerminal obj) throws ModuleException {
		try {

			// obj = (AirportCheckInTime)setUserDetails(obj);
			// Do we need to add audit
			// AuditAirMaster.doAudit(obj, getUserPrincipal());
			new AirportBL().addNewAirportTerminal(obj);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 * @ejb.interface-method view-type="both"
	 * @ejb.transaction type="Required"
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deleteAirportCIT(AirportCheckInTime obj) throws ModuleException {
		try {

			new AirportBL().deleteAirportCIT(obj);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 * @ejb.interface-method view-type="both"
	 * @ejb.transaction type="Required"
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deleteAirportTerminal(AirportTerminal obj) throws ModuleException {
		try {

			new AirportBL().deleteAirportTerminal(obj);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 * @ejb.interface-method view-type="both"
	 * @ejb.transaction type="Required"
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deleteAirportCIT(String airPortCode) throws ModuleException {
		try {

			new AirportBL().deleteAirportCIT(airPortCode);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<AirportTerminal> getAirportTerminals(String airportCode, boolean isActive) throws ModuleException {

		try {
			return new AirportBL().getAirportTerminals(airportCode, isActive);
		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page getAirportMessages(AirportMsgSearchCriteria airportMsgSearchCriteria, int start, int totalRec)
			throws ModuleException {
		try {
			return AirmasterDAOUtil.getAirportMessageDAO().searchAirportMessages(airportMsgSearchCriteria, start, totalRec);
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	/**
	 * @throws ModuleException
	 * @ejb.interface-method view-type="both"
	 * @ejb.transaction type="Required"
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveAirportMessage(AirportMessageTO airportMessageTO) throws ModuleException {
		try {

			AirportMessageBL airportMsg = new AirportMessageBL();

			airportMsg.saveAirportMessage(airportMessageTO, getUserPrincipal().getUserId());

		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 * @ejb.interface-method view-type="both"
	 * @ejb.transaction type="Required"
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveAirportMessageTranslations(AirportMessageTO airportMessageTO) throws ModuleException {
		try {

			AirportMessageBL airportMsg = new AirportMessageBL();
			airportMsg.saveAirportMessageLanguageTranslations(airportMessageTO);

		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, CachedAirportDTO> getAllAirportOperatorMap() throws ModuleException {
		return AirportCommonUtil.getInstance().getAllAirportOperatorList();
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, Set<String>> getCityVsCountryAndStateCodesMap() throws ModuleException {
		return AirportCommonUtil.getInstance().getCityVsCountryStateCodesMap();
	}


	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, CachedAirportDTO> getCachedAllAirportMap(Collection<String> airportCodes) throws ModuleException {
		return AirportCommonUtil.getInstance().getCachedAllAirportMap(airportCodes);
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, CachedAirportDTO> getCachedOwnAirportMap(Collection<String> colAirPortCode) throws ModuleException {
		return AirportCommonUtil.getInstance().getCachedOwnAirportMap(colAirPortCode);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CachedAirportDTO getCachedAirport(String airportCode) throws ModuleException {
		return AirportCommonUtil.getInstance().getCachedAllAirport(airportCode);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isAirportOptByOwn(String airportCode) throws ModuleException {
		return AirportCommonUtil.getInstance().isAirportOptByOwn(airportCode);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isAirportoptByOther(String airportCode) throws ModuleException {
		return AirportCommonUtil.getInstance().isAirportOptByOther(airportCode);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isBusSegment(String segmentCode) throws ModuleException {
		return AirportCommonUtil.getInstance().isBusSegment(segmentCode);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isContainGroundSegment(Collection<String> colAirPortCodes) throws ModuleException {
		return AirportCommonUtil.getInstance().isContainGroundSegment(colAirPortCodes);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isContainSurfaceAirport(Map<String, CachedAirportDTO> airportCodes) {
		return AirportCommonUtil.getInstance().isContainSurfaceAirport(airportCodes);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getOwnCountryCodeForAirport(String airportCode) throws ModuleException {
		return AirportCommonUtil.getInstance().getOwnCountryCode(airportCode);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String getOwnCityIdForAirport(String airportCode) throws ModuleException {
		return AirportCommonUtil.getInstance().getOwnCityId(airportCode);
	}

	@Override
	public List<String> getSameCityAirports(String airportCode) throws ModuleException {
		return AirportCommonUtil.getInstance().getSameCityAirports(airportCode);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public SegmentCarrierDTO getSegmentCarrierInfo(String origin, String destination, String carrierCode) throws ModuleException {
		try {
			return new AirportBL().getSegmentCarrierInfo(origin, destination, carrierCode);
		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	@Override
	public Set<String> getAirportCodeSet() throws ModuleException {
		try {
			return new AirportBL().getAirportCodeSet();
		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	public List<String> getActiveAirportsForCountry(String countryCode) throws ModuleException {
		try {

			return lookupAirportDAO().getAirportListForCountry(countryCode);

		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, "module.dataaccess.exception", "airmaster.desc");
			} else {
				throw new ModuleException(ex, "module.genericexception", "airmaster.desc");
			}
		}
	}

	public boolean isGoshoAgent(String fromAirport) throws ModuleException {
		return lookupAirportDAO().isGoshoAgent(getUserPrincipal().getAgentCode(), fromAirport);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<AirportDSTHistory> getUnPublishedAirportDSTHistory(String airports) throws ModuleException {
		try {
			return lookupAirportDAO().getUnPublishedAirportDSTHistory(airports);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateAirportDSTHistoryStatus(int dstHistoryId, String status) throws ModuleException {
		try {
			lookupAirportDAO().updateAirportDSTHistoryStatus(dstHistoryId, status);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	@Override
	public Collection<String> getActiveAirportsForCity(String cityCode) throws ModuleException {

		try {
			return lookupAirportDAO().getActiveAirportsForCity(cityCode);

		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e1) {
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, CachedAirlineDTO> getLccAirlineList() throws ModuleException {
		return AirportCommonUtil.getInstance().getLccAirlineList();
	}
}
