package com.isa.thinair.airmaster.core.publisher;

import org.springframework.data.redis.core.RedisTemplate;

public abstract class AbstractEventPublisher implements Publisher {
	
	private boolean enabledEventPublish;
	
	private RedisTemplate<String, Object>  eventPublisherTemplate;

	public boolean isEnabledEventPublish() {
		return enabledEventPublish;
	}

	public void setEnabledEventPublish(boolean enabledEventPublish) {
		this.enabledEventPublish = enabledEventPublish;
	}

	public RedisTemplate<String, Object> getEventPublisherTemplate() {
		return eventPublisherTemplate;
	}

	public void setEventPublisherTemplate(RedisTemplate<String, Object> eventPublisherTemplate) {
		this.eventPublisherTemplate = eventPublisherTemplate;
	}
}
