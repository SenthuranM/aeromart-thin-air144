/**
 * 	mano
	May 6, 2011 
	2011
 */
package com.isa.thinair.airmaster.core.bl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.isa.thinair.airmaster.api.model.Baggage;
import com.isa.thinair.airmaster.api.model.I18nMessage;
import com.isa.thinair.airmaster.api.model.I18nMessageKey;
import com.isa.thinair.airmaster.api.model.Language;
import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.airmaster.api.to.BaggageTO;
import com.isa.thinair.airmaster.core.persistence.dao.CommonMasterDAO;
import com.isa.thinair.airpricing.api.model.BaggageCharge;
import com.isa.thinair.airpricing.api.model.BaggageTemplate;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.util.I18nTranslationUtil;
import com.isa.thinair.commons.api.util.I18nTranslationUtil.I18nMessageCategory;
import com.isa.thinair.commons.core.util.DataConverterUtil;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * @author mano
 * 
 */
public class BaggageBusinessLayer {

	private final CommonMasterDAO commonMasterDAO;

	public BaggageBusinessLayer() {

		commonMasterDAO = AirmasterDAOUtil.getCommonMasterDAO();
	}

	public void saveOrUpdateBaggage(Baggage baggage, BaggageTO baggageTO) throws ModuleException {

		if (baggage.getVersion() > -1) {
			// update baggage
			if (baggageTO.isUpdateLanguageTranslations()) {
				// update language translations only
				saveBaggageLanguageTranslation(baggageTO);
			} else {
				// update baggage properties
				Baggage loadedBaggage = commonMasterDAO.getBaggage(baggage.getBaggageId());
				baggage.setI18nMessageKey(loadedBaggage.getI18nMessageKey());

				commonMasterDAO.saveBaggage(baggage, true);

				if (!baggage.getStatus().equals(loadedBaggage.getStatus())) {
					Integer newBaggageChargeId = null;
					// Update Template & Flight Tables
					Collection<BaggageTemplate> colTemp = AirmasterUtils.getChargeBD().getBaggageTemplatesCollect(
							baggage.getBaggageId());
					for (BaggageTemplate temlate : colTemp) {
						Collection<BaggageCharge> colCharge = temlate.getBaggageCharges();
						Set<BaggageCharge> updatedCharge = new HashSet<BaggageCharge>();
						for (BaggageCharge baggageCharge : colCharge) {
							if (baggageCharge.getBaggageId().intValue() == baggage.getBaggageId().intValue()) {
								baggageCharge.setAmount(baggageCharge.getAmount());
								baggageCharge.setStatus(baggage.getStatus());
								newBaggageChargeId = baggageCharge.getChargeId();
							}
							updatedCharge.add(baggageCharge);
						}
						temlate.setBaggageCharges(updatedCharge);
						AirmasterUtils.getChargeBD().saveBaggageTemplate(temlate, newBaggageChargeId);
					}
				}
			}

		} else {
			Integer baggageId = commonMasterDAO.saveBaggage(baggage, false);
			baggageTO.setBaggageId(baggageId);
			saveBaggageLanguageTranslation(baggageTO);
		}

	}

	private void saveBaggageLanguageTranslation(BaggageTO baggageTO) throws ModuleException {

		Baggage baggage = commonMasterDAO.getBaggage(baggageTO.getBaggageId());

		I18nMessageKey i18nMessageKey = baggage.getI18nMessageKey();

		if (i18nMessageKey == null) {
			// New Translations
			String i18nMsgKey = I18nTranslationUtil.getI18nBaggageDescriptionKey(baggage.getBaggageId());

			i18nMessageKey = new I18nMessageKey();
			baggage.setI18nMessageKey(i18nMessageKey);

			i18nMessageKey.setI18nMsgKey(i18nMsgKey);
			i18nMessageKey.setMessageCategory(I18nMessageCategory.BAGGAGE_DES.toString());

			if (baggageTO.getDescription() != null) {
				// SET ENGLISH FOR ALL LANGUAGES
				createTranlatedMessages(baggageTO, i18nMessageKey);
			} else {
				updateTranlatedMessages(baggageTO, i18nMessageKey);
			}

		} else {
			// Update existing Translations
			updateTranlatedMessages(baggageTO, i18nMessageKey);
		}

		commonMasterDAO.saveBaggage(baggage, false);
	}

	private I18nMessage getMessageByLocale(I18nMessageKey i18nMessageKey, String languageCode) {

		Iterator<I18nMessage> iterator = i18nMessageKey.getI18nMessages().iterator();
		while (iterator.hasNext()) {
			I18nMessage i18nMessage = iterator.next();
			if (i18nMessage.getMessageLocale().equalsIgnoreCase(languageCode)) {
				return i18nMessage;
			}
		}
		return null;
	}

	private void updateTranlatedMessages(BaggageTO baggageMessageTO, I18nMessageKey i18nMessageKey) throws ModuleException {

		if (baggageMessageTO.getSelectedLanguageTranslations() != null
				&& !baggageMessageTO.getSelectedLanguageTranslations().isEmpty()) {

			Collection<Language> langauges;
			String translationDescStr = baggageMessageTO.getSelectedLanguageTranslations();
			langauges = AirmasterUtils.getCommonMasterBD().getLanguages();

			for (Language language : langauges) {
				String transStrTemp = getLanguageTranslation(translationDescStr, language.getLanguageCode());

				if (transStrTemp == null || transStrTemp.isEmpty()) {
					transStrTemp = baggageMessageTO.getDescription();
				}

				String hexString = StringUtil.convertToHex(transStrTemp);

				I18nMessage i18nMessage = getMessageByLocale(i18nMessageKey, language.getLanguageCode());

				if (i18nMessage == null) {
					i18nMessage = new I18nMessage();
					i18nMessage.setMessageLocale(language.getLanguageCode());
					i18nMessage.setMessageContent(DataConverterUtil.createClob(hexString));
					i18nMessageKey.addI18nMessage(i18nMessage);
				} else {
					i18nMessage.setMessageLocale(language.getLanguageCode());
					i18nMessage.setMessageContent(DataConverterUtil.createClob(hexString));
				}
			}
		}
	}

	private String getLanguageTranslation(String translationDescStr, String lang) throws ModuleException {

		String langTraStr = null;

		if (translationDescStr != null && !translationDescStr.isEmpty()) {
			String[] splitedCodes = translationDescStr.split("\\~");

			for (int i = 0; i < splitedCodes.length; i++) {
				String[] lan_Translation = splitedCodes[i].split(",");
				String langCode = lan_Translation[1].trim();

				if (lang.equalsIgnoreCase(langCode)) {
					langTraStr = lan_Translation[2].trim();
					break;
				}
			}
		}

		return langTraStr;
	}

	private void createTranlatedMessages(BaggageTO baggageTO, I18nMessageKey i18nMessageKey) throws ModuleException {
		Collection<Language> langauges;

		langauges = AirmasterUtils.getCommonMasterBD().getLanguages();
		for (Language language : langauges) {
			I18nMessage i18nMessage = new I18nMessage();
			i18nMessage.setMessageLocale(language.getLanguageCode());

			String hexString = StringUtil.convertToHex(baggageTO.getDescription().trim());

			String translationDescStr = baggageTO.getSelectedLanguageTranslations();

			if (translationDescStr != null && !translationDescStr.isEmpty()) {

				String transStrTemp = getLanguageTranslation(translationDescStr, language.getLanguageCode());

				if (transStrTemp != null) {
					hexString = StringUtil.convertToHex(transStrTemp);
				}
			}

			i18nMessage.setMessageContent(DataConverterUtil.createClob(hexString));
			i18nMessageKey.addI18nMessage(i18nMessage);
		}

	}

}
