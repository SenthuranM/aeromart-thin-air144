package com.isa.thinair.airmaster.core.util;

import com.isa.thinair.airmaster.core.persistence.dao.AutomaticCheckinDAO;
import com.isa.thinair.airmaster.core.persistence.dao.SsrDAO;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * 
 * @author rumesh
 * 
 */

public class LookUpUtils {

	private static final String lcc_dao_path = "isa:base://modules/airmaster?id=LCCAirportDAO";
	private static final String ssr_dao_proxy = "isa:base://modules/airmaster?id=SsrDAOProxy";
	private static final String auto_checkin_dao_path = "isa:base://modules/airmaster?id=AutomaticCheckinDAOProxy";

	public static LCCAirportDAO lccLookupDAO() {
		LCCAirportDAO lccAirportDAO = (LCCAirportDAO) LookupServiceFactory.getInstance().getBean(lcc_dao_path);
		return lccAirportDAO;
	}

	public static SsrDAO getSsrDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (SsrDAO) lookupService.getBean(ssr_dao_proxy);
	}
	
	public static AutomaticCheckinDAO getAutoCheckinDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (AutomaticCheckinDAO) lookupService.getBean(auto_checkin_dao_path);
	}
}
