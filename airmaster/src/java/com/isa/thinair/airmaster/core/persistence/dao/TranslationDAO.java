/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:14:52
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.core.persistence.dao;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.airmaster.api.model.AirportForDisplay;
import com.isa.thinair.airmaster.api.model.CityForDisplay;
import com.isa.thinair.airmaster.api.model.CurrencyForDisplay;
import com.isa.thinair.airmaster.api.model.MealForDisplay;

/**
 * Haider 05Mar09 BHive Commons: Translation module
 * 
 * TranslationDAO is business delegate interface for the airport and currency translation service apis
 * 
 * 
 */

public interface TranslationDAO {

	public List<AirportForDisplay> getAirports();

	public List<AirportForDisplay> getAirportsByLanguageCode(String languageCode);

	public List<AirportForDisplay> getAirportsByAirportCode(String airportCode);

	public Collection<AirportForDisplay> getAirport(Collection<String> airportCode, String languageCode);

	public Collection<CityForDisplay> getCity(Collection<String> cityIds, String languageCode);
	
	public void saveAirport(AirportForDisplay airport);

	public void saveAirports(Collection<AirportForDisplay> col);

	public void removeAirport(String airportCode);

	public void removeAirport(int id);

	public List<CurrencyForDisplay> getCurrencies();

	public List<CurrencyForDisplay> getCurrenciesByLanguageCode(String languageCode);

	public List<CurrencyForDisplay> getCurrenciesByCurrencyCode(String currencyCode);

	public CurrencyForDisplay getCurrency(String currencyCode, String languageCode);

	public void saveCurrency(CurrencyForDisplay currency);

	public void removeCurrency(String currencyCode);

	public void removeCurrency(int id);

	public List<MealForDisplay> getMeals();

	public void saveMeals(Collection<MealForDisplay> meals);

	public void removeMeals(int mealId);

	public void removeMealNotifyTiming(int mealNotifyId);
	
	public List<CityForDisplay> getCities();
	
	public void saveCities(Collection<CityForDisplay> cities);

	public void removeCities(int cityId);
}
