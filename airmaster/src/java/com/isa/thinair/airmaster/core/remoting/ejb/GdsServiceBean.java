/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Oct 9, 2008 
 * 
 * $Id: GdsServiceBean.java,v 1.0
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.core.remoting.ejb;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.airmaster.core.persistence.dao.GdsDAO;
import com.isa.thinair.airmaster.core.service.bd.GdsServiceDelegateImpl;
import com.isa.thinair.airmaster.core.service.bd.GdsServiceLocalDelegateImpl;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * @author Haider
 */
@Stateless
@RemoteBinding(jndiBinding = "GdsService.remote")
@LocalBinding(jndiBinding = "GdsService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class GdsServiceBean extends PlatformBaseSessionBean implements GdsServiceDelegateImpl, GdsServiceLocalDelegateImpl {

	public void saveGds(Gds obj) throws ModuleException {
		try {
			lookupGdsDAO().saveGds(obj);

		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");

		}
	}

	public void removeGds(int key) throws ModuleException {
		try {
			lookupGdsDAO().removeGds(key);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	public void delete(Gds gds) throws ModuleException {
		try {
			lookupGdsDAO().removeGds(gds);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	public List<Gds> getGDSs() throws ModuleException {
		try {
			return lookupGdsDAO().getGDSs();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	public List<Gds> getActiveGDSs() throws ModuleException {
		try {
			return lookupGdsDAO().getActiveGDSs();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	public Gds getGds(int gdsId) throws ModuleException {
		try {
			return lookupGdsDAO().getGds(gdsId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	public Gds getGDSByCode(String gdsCode) throws ModuleException {
		try {
			return lookupGdsDAO().getGDSByCode(gdsCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	public Page getGDSs(int startRec, int noRecs) throws ModuleException {
		try {
			return lookupGdsDAO().getGDSs(startRec, noRecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	private GdsDAO lookupGdsDAO() {

		GdsDAO dao = null;

		try {
			LookupService lookupService = LookupServiceFactory.getInstance();
			dao = (GdsDAO) lookupService.getBean("isa:base://modules/airmaster?id=GdsDAOProxy");

		} catch (Exception ex) {
			System.out.println("ERROR IN LOOKUP");
		}

		return dao;
	}
}
