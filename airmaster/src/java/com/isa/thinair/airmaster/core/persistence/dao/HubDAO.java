/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:14:52
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.core.persistence.dao;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.airmaster.api.model.HubInfo;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.ondpublish.OndLocationDTO;

/*******************************************************************************
 * Description : Hub Info. Hibernate Data Access Object
 * 
 * @author : Nadika Dhanusha Mahatantila
 * @since : 25 Feb, 2008
 ******************************************************************************/

public interface HubDAO {

	/**
	 * This method is used to search Hub Info. information from the Database.
	 * 
	 * @param searchCriteria
	 * @param startIndex
	 * @param pageSize
	 * @return Page
	 */
	public Page getHubDetails(List<ModuleCriterion> searchCriteria, int startIndex, int pageSize);

	/**
	 * This method is used to Save/Update information to the Database.
	 * 
	 * @param hubInfo
	 */
	public void saveHubInfo(HubInfo hubInfo);

	/**
	 * This method is used to Delete information from the Database.
	 * 
	 * @param hubInfo
	 */
	public void removeHubInfo(HubInfo hubInfo);

	/**
	 * This method is used to get Hub Info. information by givning the Domain Object from the Database.
	 * 
	 * @param hubInfo
	 * @return HubInfo
	 */
	public HubInfo getHubInfo(HubInfo hubInfo);

	/**
	 * Gets the Valid Hub codes
	 * 
	 * @param airportCodes
	 * @return Collection
	 */
	public Collection<String> getValidHubCodes(Collection<String> airportCodes);

	/**
	 * 
	 * Get Hub Information by airport code, in-bound carrier & out-bound carrier
	 * 
	 * @param airportCode
	 * @param ibCarrier
	 * @param obCarrier
	 * @return
	 */
	public HubInfo getHubInfo(String airportCode, String ibCarrier, String obCarrier);

	/**
	 * Gets the Active Hub codes
	 * 
	 * @return Collection
	 */
	public Collection<String> getActiveHubCodes();
	/**
	 * Gets the Active Hub codes
	 * 
	 * @return Collection
	 */
	public Collection<OndLocationDTO> getActiveHubDetails();

}
