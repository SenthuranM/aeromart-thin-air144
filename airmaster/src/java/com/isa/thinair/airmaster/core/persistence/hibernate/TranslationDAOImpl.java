/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:14:52
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airmaster.core.persistence.hibernate;

import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.AirportForDisplay;
import com.isa.thinair.airmaster.api.model.CityForDisplay;
import com.isa.thinair.airmaster.api.model.CurrencyForDisplay;
import com.isa.thinair.airmaster.api.model.MealForDisplay;
import com.isa.thinair.airmaster.api.model.MealNotifyTimings;
import com.isa.thinair.airmaster.core.persistence.dao.TranslationDAO;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 * BHive: Translation module
 * 
 * @author Haider
 * 
 * 
 *         TranslationDAOImpl is the TranslationDAO implementatiion for hibernate
 * @isa.module.dao-impl dao-name="TranslationDAO"
 */
public class TranslationDAOImpl extends PlatformBaseHibernateDaoSupport implements TranslationDAO {

	private Log log = LogFactory.getLog(getClass());

	public List<AirportForDisplay> getAirports() {
		return find("from AirportForDisplay", AirportForDisplay.class);
	}

	public List<AirportForDisplay> getAirportsByLanguageCode(String languageCode) {
		return find("from AirportForDisplay at where at.languageCode= ?", languageCode, AirportForDisplay.class);
	}

	public List<AirportForDisplay> getAirportsByAirportCode(String airportCode) {
		return find("from AirportForDisplay at where at.airportCode= ?", airportCode, AirportForDisplay.class);
	}

	public Collection<AirportForDisplay> getAirport(Collection<String> airportCode, String languageCode) {
		String hql = " from AirportForDisplay at where at.airportCode IN (:airportCodes) and at.languageCode=:language";
		return (Collection<AirportForDisplay>) getSession().createQuery(hql).setParameterList("airportCodes", airportCode)
				.setString("language", languageCode).list();
	}
	

	public Collection<CityForDisplay> getCity(Collection<String> cityCodes, String languageCode) {
		String hql = "select city.cityId from City AS city WHERE city.cityCode IN (:cityCodes)";
		Collection<Integer> cityIds = (Collection<Integer>) getSession().createQuery(hql).setParameterList("cityCodes", cityCodes)
				.list();
		String hqlCityDisplay = "from CityForDisplay AS cityForDisplay WHERE cityForDisplay.languageCode=:languageCode and cityForDisplay.cityId in (:cityIds)";
		return (Collection<CityForDisplay>) getSession().createQuery(hqlCityDisplay).setParameterList("cityIds", cityIds)
				.setParameter("languageCode", languageCode).list();

	}

	public void saveAirport(AirportForDisplay airport) {
		hibernateSaveOrUpdate(airport);
	}

	public void saveAirports(Collection<AirportForDisplay> col) {
		hibernateSaveOrUpdateAll(col);
	}

	public void removeAirport(String airportCode) {
		List<AirportForDisplay> l = getAirportsByAirportCode(airportCode);
		deleteAll(l);
	}

	public void removeAirport(int id) {
		Object obj = load(AirportForDisplay.class, new Integer(id));

		delete(obj);

		if (log.isDebugEnabled()) {
			log.debug("AirportForDisplay Deleted" + id);
		}
	}

	public void removeMealNotifyTiming(int id) {
		Object obj = load(MealNotifyTimings.class, new Integer(id));
		delete(obj);
	}

	public List<CurrencyForDisplay> getCurrencies() {
		return find("from CurrencyForDisplay", CurrencyForDisplay.class);
	}

	public List<CurrencyForDisplay> getCurrenciesByLanguageCode(String languageCode) {
		return find("from CurrencyForDisplay at where at.languageCode= ?", languageCode, CurrencyForDisplay.class);
	}

	public List<CurrencyForDisplay> getCurrenciesByCurrencyCode(String currencyCode) {
		return find("from CurrencyForDisplay at where at.currencyCode= ?", currencyCode, CurrencyForDisplay.class);
	}

	public CurrencyForDisplay getCurrency(String currencyCode, String languageCode) {
		String hql = " from CurrencyForDisplay at where at.currencyCode= ? and at.languageCode= ? ";

		CurrencyForDisplay CurrencyForDisplay = (CurrencyForDisplay) getSession().createQuery(hql).setString(0, currencyCode)
				.setString(1, languageCode).uniqueResult();
		return CurrencyForDisplay;
	}

	public void saveCurrency(CurrencyForDisplay currency) {
		hibernateSaveOrUpdate(currency);
	}

	public void removeCurrency(String currencyCode) {
		List<CurrencyForDisplay> l = getCurrenciesByCurrencyCode(currencyCode);
		deleteAll(l);
	}

	public void removeCurrency(int id) {
		Object obj = load(CurrencyForDisplay.class, new Integer(id));

		delete(obj);

		if (log.isDebugEnabled()) {
			log.debug("CurrencyForDisplay Deleted" + id);
		}
	}

	public List<MealForDisplay> getMeals() {
		return find("from MealForDisplay mdisp order by mdisp.mealId", MealForDisplay.class);
	}

	public void saveMeals(Collection<MealForDisplay> meals) {
		hibernateSaveOrUpdateAll(meals);
	}

	public void removeMeals(int mealId) {
		List<MealForDisplay> l = getMealsByMealId(mealId);
		deleteAll(l);
		if (log.isDebugEnabled()) {
			log.debug("MealForDisplay Deleted " + mealId);
		}
	}

	public List<MealForDisplay> getMealsByMealId(int mealId) {
		return find("from MealForDisplay ml where ml.mealId= ?", mealId, MealForDisplay.class);
	}
	
	public List<CityForDisplay> getCitiesByCityId(int cityId) {
		return find("from CityForDisplay cfd where cfd.cityId= ?", cityId, CityForDisplay.class);
	}
	
	public List<CityForDisplay> getCities() {
		return find("from CityForDisplay cfd order by cfd.cityId", CityForDisplay.class);
	}
	
	public void saveCities(Collection<CityForDisplay> cities) {
		hibernateSaveOrUpdateAll(cities);
	}

	public void removeCities(int cityId) {
		List<CityForDisplay> l = getCitiesByCityId(cityId);
		deleteAll(l);
		if (log.isDebugEnabled()) {
			log.debug("CityForDisplay Deleted " + cityId);
		}
	}

}
