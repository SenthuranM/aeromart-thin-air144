package com.isa.thinair.airmaster.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airmaster.api.service.AirportTransferBD;

@Local
public interface AirportTransferServiceLocalDelegateImpl extends AirportTransferBD {

}
