package com.isa.thinair.airmaster.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airmaster.api.service.AutomaticCheckinBD;

/**
 * @author aravinth.r
 *
 */
@Local
public interface AutomaticCheckinServiceLocalDelegateImpl extends AutomaticCheckinBD {

}
