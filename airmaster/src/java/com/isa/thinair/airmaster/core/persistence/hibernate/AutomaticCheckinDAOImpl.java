/**
 * @author aravinth.r
 */
package com.isa.thinair.airmaster.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants;
import com.isa.thinair.airmaster.api.criteria.AutomaticCheckinTemplatesSearchCriteria;
import com.isa.thinair.airmaster.api.dto.AutomaticCheckinDTO;
import com.isa.thinair.airmaster.api.model.AutoCheckinTemplateAirport;
import com.isa.thinair.airmaster.api.model.AutomaticCheckinTemplate;
import com.isa.thinair.airmaster.core.persistence.dao.AutomaticCheckinDAO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 *
 *         AutomaticCheckinDAOImpl is the AutomaticCheckinDAO implementatiion for hibernate
 * @isa.module.dao-impl dao-name="AutomaticCheckinDAO"
 */
public class AutomaticCheckinDAOImpl extends PlatformBaseHibernateDaoSupport implements AutomaticCheckinDAO {

	@Override
	public List<AutomaticCheckinDTO> getAvailableAutomaticCheckins(String airportCode) {
		List<AutoCheckinTemplateAirport> autoCheckinTemplateAirportList = find(
				"from AutoCheckinTemplateAirport acta where acta.airportCode = ?", airportCode, AutoCheckinTemplateAirport.class);

		List<AutomaticCheckinDTO> automaticCheckinDTOList = new ArrayList<AutomaticCheckinDTO>();
		for (AutoCheckinTemplateAirport autoCheckinTemplateAirport : autoCheckinTemplateAirportList) {
			if ((autoCheckinTemplateAirport.getAutomaticCheckinTemplate() != null)
					&& (autoCheckinTemplateAirport.getAutomaticCheckinTemplate().getStatus()
							.equalsIgnoreCase(AirinventoryCustomConstants.AutoCheckinConstants.INACTIVE))) {
				continue;
			}
			AutomaticCheckinDTO automaticCheckinDTO = new AutomaticCheckinDTO();
			automaticCheckinDTO.setAirportCode(airportCode);
			automaticCheckinDTO.setStatus(autoCheckinTemplateAirport.getAutomaticCheckinTemplate().getStatus());
			automaticCheckinDTO.setAmount(autoCheckinTemplateAirport.getAutomaticCheckinTemplate().getAmount());
			automaticCheckinDTO.setAutomaticCheckinTemplateId(autoCheckinTemplateAirport.getAutomaticCheckinTemplate()
					.getAutomaticCheckinTemplateId());
			automaticCheckinDTOList.add(automaticCheckinDTO);
		}
		return automaticCheckinDTOList;
	}

	@Override
	public Page<AutomaticCheckinTemplate> getAutomaticCheckinTemplates(int startRec, int numOfRecs,
			AutomaticCheckinTemplatesSearchCriteria actSearchCriteria) {
		StringBuilder sql = new StringBuilder();
		getAutoCheckinHql(actSearchCriteria, sql);

		Query query = getSession().createQuery(sql.toString());

		if (actSearchCriteria.getAirportCode() != null) {
			query.setParameter("airportCode", actSearchCriteria.getAirportCode());
		}
		if (actSearchCriteria.getStatus() != null) {
			query.setParameter("status", actSearchCriteria.getStatus());
		}
		int count = query.list().size();
		List<AutomaticCheckinTemplate> list = query.setFirstResult(startRec).setMaxResults(numOfRecs).list();

		return new Page(count, startRec, startRec + numOfRecs - 1, list);
	}

	/**
	 * @param actSearchCriteria
	 * @param sql
	 */
	private StringBuilder getAutoCheckinHql(AutomaticCheckinTemplatesSearchCriteria actSearchCriteria, StringBuilder sql) {
		sql.append("select act from AutomaticCheckinTemplate act ");

		if (actSearchCriteria.getAirportCode() != null) {
			sql.append(" join act.autoCheckinTemplateAirport acta where ");
			sql.append(" acta.airportCode = :airportCode ");
		}
		if (actSearchCriteria.getStatus() != null && actSearchCriteria.getAirportCode() != null) {
			sql.append(" and act.status= :status ");
		} else if (actSearchCriteria.getStatus() != null && actSearchCriteria.getAirportCode() == null) {
			sql.append(" where act.status= :status");
		}
		return sql;
	}

	@Override
	public void saveAutomaticCheckinTemplate(AutomaticCheckinTemplate automaticCheckinTemplate) {
		hibernateSaveOrUpdate(automaticCheckinTemplate);
	}

	@Override
	public AutomaticCheckinTemplate updateAutomaticCheckinTemplate(AutomaticCheckinTemplate automaticCheckinTemplate) {
		AutomaticCheckinTemplate persistedAutoCheckinTemplate = (AutomaticCheckinTemplate) merge(automaticCheckinTemplate);
		return persistedAutoCheckinTemplate;
	}

	/**
	 * This will return the list of AutoCheckinTemplateAirports for given autoCheckinTempId
	 */
	@Override
	public List<AutoCheckinTemplateAirport> getAutomaticCheckinAirports(int autoCheckinTempId) {
		List<AutoCheckinTemplateAirport> templateAirport = find(
				"from AutoCheckinTemplateAirport acta where acta.automaticCheckinTemplate.automaticCheckinTemplateId = ?",
				new Object[] { autoCheckinTempId }, AutoCheckinTemplateAirport.class);
		return templateAirport;
	}

	/**
	 * This will return the list of AutomaticCheckinTemplate for given autoCheckinTempId
	 */
	@Override
	public AutomaticCheckinTemplate getAutomaticCheckinTemplate(int autoCheckinTempId) {
		return (AutomaticCheckinTemplate) get(AutomaticCheckinTemplate.class, autoCheckinTempId);
	}

	/**
	 * This method will return the list of available airport codes for given airport list
	 */
	@Override
	public List<String> getAutoCheckinAirportList(List<String> airportList) {

		String sql = "SELECT AIRPORT_CODE FROM T_AUTO_CHKIN_TEMPLATE_AIRPORT acta WHERE acta.AIRPORT_CODE IN :airportCode";
		Query query = getSession().createSQLQuery(sql);
		query.setParameterList("airportCode", airportList);
		List<String> results = query.list();

		return results;
	}
}
