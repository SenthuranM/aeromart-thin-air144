package com.isa.thinair.airmaster.core.persistence.dao;

import java.util.Collection;

import com.isa.thinair.airmaster.api.model.SmartSearchIndex;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface SearchDAO {

	public Collection<String> search(SmartSearchIndex search) throws ModuleException;

}
