package com.isa.thinair.airmaster.core.bl.exrateautomation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTPConnectionClosedException;

import com.isa.thinair.airmaster.api.dto.ExchangeRateInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.FTPUtil;

public class FTPDldExRateSource implements ExchangeRateSource {

	private FTPUtil ftpUtil = new FTPUtil();

	private String ftpHost;

	private String ftpUserName;

	private String ftpPassword;

	private String fileName;

	private String localFileName;

	private static Log log = LogFactory.getLog(FTPDldExRateSource.class);

	@Override
	public Map<String, ExchangeRateInfoDTO> getExchangeRates() throws ModuleException {

		Map<String, ExchangeRateInfoDTO> rateInfo = new HashMap<String, ExchangeRateInfoDTO>();

		boolean isDownLoaded = downloadFile();
		if (isDownLoaded) {
			if (log.isInfoEnabled()) {
				log.info("Exchange Rate file downloaded successfully from the source");
			}
			rateInfo = getRatesFromFile();
			deleteFile();
		} else {
			log.error("Exchange rate file as " + fileName + " at " + ftpHost + " is failed");
			rateInfo = null;
		}
		return rateInfo;
	}

	private boolean downloadFile() throws ModuleException {
		boolean isdownloaded = false;

		try {
			boolean isConnected = ftpUtil.connectAndLogin(ftpHost, ftpUserName, ftpPassword);

			if (isConnected) {
				ftpUtil.binary();
				isdownloaded = ftpUtil.downloadFile(fileName, localFileName);
				ftpUtil.logout();
				ftpUtil.disconnect();
			}

		} catch (FTPConnectionClosedException e) {
			throw new ModuleException("airmaster.currency.automate.ftp.conclosed");
		} catch (IOException e) {
			throw new ModuleException("airmaster.currency.automate.ftp.confailed");
		}

		return isdownloaded;
	}

	private Map<String, ExchangeRateInfoDTO> getRatesFromFile() throws ModuleException {

		Map<String, ExchangeRateInfoDTO> rates = new HashMap<String, ExchangeRateInfoDTO>();

		ArrayList<String> fileContent = new ArrayList<String>();

		try {
			BufferedReader inReader = new BufferedReader(new FileReader(localFileName));
			String line = inReader.readLine();
			while (line != null) {
				fileContent.add(line);
				line = inReader.readLine();
			}
			inReader.close();

		} catch (Exception exp) {
			throw new ModuleException("airmaster.currency.automate.ftp.filereaderr");
		}

		if (fileContent.size() > 0) {
			for (String line : fileContent) {
				if (line.trim().length() == 77) {
					String typeOfExchangeRate = line.substring(43, 44);

					String currencyCode = line.substring(14, 17);
					String currencyName = line.substring(17, 43);
					String strRate = line.substring(63, 76);
					int noOfDecimals = Integer.parseInt(line.substring(76, 77));
					String strDateOfQuote = line.substring(56, 62);

					strRate = strRate.substring(0, strRate.length() - noOfDecimals) + "."
							+ strRate.substring(strRate.length() - noOfDecimals, strRate.length());
					BigDecimal exRate = BigDecimal.valueOf(Double.parseDouble(strRate));

					Date dateOfQoute = CalendarUtil.getDate(Integer.parseInt(strDateOfQuote.substring(4, 6)),
							Integer.parseInt(strDateOfQuote.substring(2, 4)), Integer.parseInt(strDateOfQuote.substring(0, 2)));

					if (typeOfExchangeRate.equals("0")) {
						ExchangeRateInfoDTO rateInfo = new ExchangeRateInfoDTO();

						rateInfo.setCurrencyCode(currencyCode.trim());
						rateInfo.setCurrencyName(currencyName.trim());
						rateInfo.setExchangeRate(exRate);
						rateInfo.setDateOfQuote(dateOfQoute);

						rates.put(currencyCode, rateInfo);
					}

					if (typeOfExchangeRate.equals("1")) {
						if (rates.get(currencyCode) == null) {
							ExchangeRateInfoDTO rateInfo = new ExchangeRateInfoDTO();

							rateInfo.setCurrencyCode(currencyCode.trim());
							rateInfo.setCurrencyName(currencyName.trim());
							rateInfo.setExchangeRate(exRate);
							rateInfo.setDateOfQuote(dateOfQoute);

							rates.put(currencyCode, rateInfo);
						} else {
							ExchangeRateInfoDTO rateInfo = rates.get(currencyCode);

							rateInfo.setCurrencyCode(currencyCode.trim());
							rateInfo.setCurrencyName(currencyName.trim());
							rateInfo.setExchangeRate(exRate);
							rateInfo.setDateOfQuote(dateOfQoute);
						}
					}

				}
			}
		} else {
			if (log.isInfoEnabled()) {
				log.info("There is no exchange rate info to retrieved from the file");
			}
		}

		return rates;
	}

	public void deleteFile() {

		File fileToDel = new File(localFileName);

		boolean isExists = fileToDel.exists();

		if (isExists) {
			boolean isDeleted = fileToDel.delete();
			if (isDeleted) {
				if (log.isInfoEnabled()) {
					log.info("Exchange rate file successfully deleted");
				}
			} else {
				if (log.isErrorEnabled()) {
					log.error("Exchange rate file deletion failed");
				}
			}
		} else {
			if (log.isErrorEnabled()) {
				log.error("No Exchange Rate file as " + localFileName);
			}
		}
	}

	public FTPUtil getFtpUtil() {
		return ftpUtil;
	}

	public void setFtpUtil(FTPUtil ftpUtil) {
		this.ftpUtil = ftpUtil;
	}

	public String getFtpHost() {
		return ftpHost;
	}

	public void setFtpHost(String ftpHost) {
		this.ftpHost = ftpHost;
	}

	public String getFtpUserName() {
		return ftpUserName;
	}

	public void setFtpUserName(String ftpUserName) {
		this.ftpUserName = ftpUserName;
	}

	public String getFtpPassword() {
		return ftpPassword;
	}

	public void setFtpPassword(String ftpPassword) {
		this.ftpPassword = ftpPassword;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getLocalFileName() {
		return localFileName;
	}

	public void setLocalFileName(String localFileName) {
		this.localFileName = localFileName;
	}

}
