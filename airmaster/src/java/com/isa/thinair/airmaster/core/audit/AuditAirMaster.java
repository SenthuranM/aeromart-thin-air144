package com.isa.thinair.airmaster.core.audit;

/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Set;

import com.isa.thinair.airmaster.api.dto.external.ExRateAutoUpdateResultDTO;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airmaster.api.model.Baggage;
import com.isa.thinair.airmaster.api.model.City;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.HubInfo;
import com.isa.thinair.airmaster.api.model.ItineraryAdvertisement;
import com.isa.thinair.airmaster.api.model.Meal;
import com.isa.thinair.airmaster.api.model.MealCategory;
import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.airmaster.api.model.SITAAddress;
import com.isa.thinair.airmaster.api.model.SSR;
import com.isa.thinair.airmaster.api.model.SSRTemplate;
import com.isa.thinair.airmaster.api.model.SSRTemplateCabinQuantity;
import com.isa.thinair.airmaster.api.model.State;
import com.isa.thinair.airmaster.api.model.Station;
import com.isa.thinair.airmaster.api.model.Territory;
import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.airpricing.api.model.PaymentGatewayWiseCharges;
import com.isa.thinair.airreservation.api.model.GroupBookingRequest;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.dto.SSRInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.framework.Persistent;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.promotion.api.model.BuyNGetPromotionCriteria;
import com.isa.thinair.promotion.api.model.DatePeriod;
import com.isa.thinair.promotion.api.model.DiscountPromotionCriteria;
import com.isa.thinair.promotion.api.model.FreeServicePromotionCriteria;
import com.isa.thinair.promotion.api.model.PromoCode;

/**
 * Class to implement the bussiness logic related to the Flight
 * 
 * @author Byorn
 */
public abstract class AuditAirMaster {

	public static final String COUNTRY = "COUNTRY";
	public static final String STATION = "STATION";
	public static final String TERRITORY = "TERRITORY";
	public static final String ROUTE_INFO = "ROUTE_INFO";
	public static final String CURRENCY = "CURRENCY";
	public static final String AIRPORT = "AIRPORT";
	public static final String AIRCRAFT_MODEL = "AIRCRAFT_MODEL";
	public static final String SITA_ADDRESS = "SITA_ADDRESS";
	public static final String AIRPORT_DST = "AIRPORT_DST";
	public static final String HUB_INFO = "HUB_INFO";
	public static final String SSR = "SSR";
	public static final String SSR_TEMPLATE = "SSR_TEMPLATE";
	public static final String MEAL = "MEAL";
	public static final String MEAL_CATEGORY = "MEAL_CATEGORY";
	public static final String BAGGAGE = "BAGGAGE";
	public static final String PROMO_CODE = "PROMO_CODE";
	public static final String ITINERARY_ADVERTISEMENT = "ITINERARY_ADVERTISEMENT";
	public static final String STATE = "STATE";
	public static final String CITY = "CITY";

	public static void doAudit(String code, String type, UserPrincipal principal) throws ModuleException {

		Audit audit = new Audit();

		if (principal != null) {
			audit.setUserId(principal.getName());
		} else {
			audit.setUserId("");
		}

		audit.setTimestamp(new Date());
		LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();

		if (type.equals(COUNTRY)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DELETE_COUNTRY));
		}
		if (type.equals(STATION)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DELETE_STATION));
		}
		if (type.equals(TERRITORY)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DELETE_TERRITORY));
		}
		if (type.equals(CURRENCY)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DELETE_CURRENCY));
		}
		if (type.equals(AIRPORT)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DELETE_AIRPORT));
		}

		if (type.equals(AIRCRAFT_MODEL)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DELETE_AIRCRAFT_MODELS));
		}

		if (type.equals(SITA_ADDRESS)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DELETE_SITA_ADDRESSES));
		}

		if (type.equals(ROUTE_INFO)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DELETE_ROUTES));
		}

		if (type.equals(AIRPORT_DST)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DISABLE_ROLLBACK_DST));
		}

		// Hub Info. related
		if (type.equals(HUB_INFO)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DELETE_HUB));
		}
		if (type.equals(SSR)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DELETE_SSR));
		}
		if (type.equals(MEAL)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DELETE_MEAL));
		}

		if (type.equals(BAGGAGE)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DELETE_BAGGAGE));
		}

		if (type.equals(MEAL_CATEGORY)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DELETE_MEAL_CATEGORY));
		}

		if (type.equals(SSR_TEMPLATE)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DELETE_SSR_TEMPLATE));
		}

		if (type.equals(PROMO_CODE)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.PROMOTION_DELETE_PROMO_CODE));
		}

		if (type.equals(ITINERARY_ADVERTISEMENT)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DELETE_ADVERTISEMENT));
		}
		if (type.equals(STATE)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DELETE_STATE));
		}
		if (type.equals(CITY)) {
			contents.put("code", code);
			audit.setTaskCode(String.valueOf(TasksUtil.MASTER_DELETE_CITY));
		}
		AirmasterUtils.getAuditorBD().audit(audit, contents);

	}

	public static void doAudit(Persistent persistant, UserPrincipal principal) throws ModuleException {
		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		LinkedHashMap<String, Object> contents = new LinkedHashMap<String, Object>();

		if (principal != null) {
			audit.setUserId(principal.getName());
		} else {
			audit.setUserId("");
		}

		if (persistant instanceof Currency) {
			Currency currency = (Currency) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_CURRENCY));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_CURRENCY));
			}

			contents.put("code", currency.getCurrencyCode());
			contents.put("description", currency.getCurrencyDescriprion());
			contents.put("status", currency.getStatus());
			// TODO
			// contents.put("base_exchange_rate", String.valueOf(currency.getBaseExchangeRate()));
		}

		if (persistant instanceof Country) {
			Country obj = (Country) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_COUNTRY));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_COUNTRY));
			}

			contents.put("code", obj.getCountryCode());
			contents.put("description", obj.getCountryName());
			contents.put("status", obj.getStatus());
			contents.put("remarks", obj.getRemarks());
		}

		if (persistant instanceof Station) {
			Station obj = (Station) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_STATION));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_STATION));
			}

			contents.put("code", obj.getStationCode());
			contents.put("description", obj.getStationName());
			contents.put("status", obj.getStatus());
			contents.put("stateId", obj.getStateId());

		}

		if (persistant instanceof Territory) {
			Territory obj = (Territory) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_TERRITORY));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_TERRITORY));
			}

			contents.put("code", obj.getTerritoryCode());
			contents.put("description", obj.getDescription());
			contents.put("remarks", obj.getRemarks());
			contents.put("status", obj.getStatus());

		}
		if (persistant instanceof Airport) {
			Airport obj = (Airport) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_AIRPORT));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_AIRPORT));
			}

			contents.put("code", obj.getAirportCode());
			contents.put("description", obj.getAirportName());
			contents.put("status", obj.getStatus());
			contents.put("remarks", obj.getRemarks());
			contents.put("closestairport", obj.getClosestAirport());
			contents.put("contact", obj.getContact());
			contents.put("fax", obj.getFax());
			contents.put("latitude", obj.getLatitude());
			contents.put("longitude", obj.getLongitude());
			contents.put("gmt offset hours", String.valueOf(obj.getGmtOffsetHours()));
			contents.put("goshowagentcode", obj.getGoshowAgentCode());

		}

		if (persistant instanceof City) {
			City obj = (City) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_CITY));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_CITY));
			}

			contents.put("code", obj.getCityCode());
			contents.put("name", obj.getCityName());
			contents.put("country", obj.getCountryCode());
			contents.put("status", obj.getStatus());

		}

		if (persistant instanceof SITAAddress) {
			SITAAddress obj = (SITAAddress) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_SITA_ADDRESSES));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_SITA_ADDRESSES));
			}

			contents.put("code", obj.getSitaAddress());
			contents.put("sitalocation", obj.getSitaLocation());
			contents.put("airport code", obj.getAirportCode());
			contents.put("sita id", String.valueOf(obj.getAirportSITAId()));

			Set<String> flightNumbers = obj.getFlightNumbers();
			StringBuffer strFlightNumbers = new StringBuffer();
			if (flightNumbers != null && !flightNumbers.isEmpty()) {
				for (String fltNum : flightNumbers) {
					strFlightNumbers.append(" " + fltNum + ", ");
				}
			} else {
				strFlightNumbers.append(" flight numbers are empty ");
			}
			contents.put("flight numbers", strFlightNumbers.toString());

			Set<String> onds = obj.getOndCodes();
			StringBuffer strOnd = new StringBuffer();
			if (onds != null && !onds.isEmpty()) {
				for (String ond : onds) {
					strOnd.append(" " + ond + ", ");
				}
			} else {
				strOnd.append(" onds are empty ");
			}
			contents.put("onds", strOnd.toString());
		}

		if (persistant instanceof AircraftModel) {
			AircraftModel obj = (AircraftModel) persistant;

			if (persistant.getVersion() <= 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_AIRCRAFT_MODELS));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_AIRCRAFT_MODELS));
			}

			contents.put("code", obj.getModelNumber());
			contents.put("description", obj.getDescription());
			contents.put("model number", obj.getModelNumber());
			contents.put("status", obj.getStatus());
			Set<AircraftCabinCapacity> capcities = obj.getAircraftCabinCapacitys();
			StringBuffer strCapacity = new StringBuffer();
			for (AircraftCabinCapacity cap : capcities) {
				strCapacity.append(cap.getCabinClassCode() + "|" + cap.getSeatCapacity() + "|" + cap.getInfantCapacity() + ",");
			}
			contents.put("capacity", strCapacity.toString());
			Set<AircraftCabinCapacity> oldCapcities = obj.getOldaircraftcapacitys();
			if (oldCapcities != null) {
				StringBuffer strOldCapacity = new StringBuffer();
				for (AircraftCabinCapacity cap : oldCapcities) {
					strOldCapacity
							.append(cap.getCabinClassCode() + "|" + cap.getSeatCapacity() + "|" + cap.getInfantCapacity() + ",");
				}
				contents.put("old capacity", strOldCapacity.toString());
			}
		}

		if (persistant instanceof RouteInfo) {
			RouteInfo obj = (RouteInfo) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_ROUTES));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_ROUTES));
			}

			contents.put("code", obj.getRouteCode());
			contents.put("from airport", obj.getFromAirportCode());
			contents.put("to airport", obj.getToAirportCode());
			contents.put("distance", String.valueOf(obj.getDistance()));
			contents.put("duration", String.valueOf(obj.getDuration()));
			contents.put("duration tolerance", String.valueOf(obj.getDurationTolerance()));
		}

		if (persistant instanceof AirportDST) {
			AirportDST obj = (AirportDST) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_AIRPORT_DST));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_AIRPORT_DST));
			}

			contents.put("airport code", obj.getAirportCode());
			contents.put("dst start date", obj.getDstEndDateTime());
			contents.put("dst end date", obj.getDstEndDateTime());
			contents.put("status", obj.getDSTStatus());
			contents.put("dst adjust time", String.valueOf(obj.getDstAdjustTime()));

		}

		// Hub Info. related
		if (persistant instanceof HubInfo) {
			HubInfo obj = (HubInfo) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_HUB));

			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_HUB));
			}

			contents.put("code", obj.getAirportCode());
			contents.put("min connection time", obj.getMinConnectionTime());
			contents.put("max connection time", obj.getMaxConnectionTime());
			contents.put("ib carrier", obj.getIbCarrierCode());
			contents.put("ob carrier", obj.getObCarrierCode());
			contents.put("active", obj.getStatus());
		}

		// SSR Info
		if (persistant instanceof SSR) {
			SSR obj = (SSR) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_SSR));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_SSR));
			}

			contents.put("SSR code", obj.getSsrCode());
			contents.put("description", obj.getSsrDesc());
			contents.put("SSR Category", obj.getSsrCatId());
			contents.put("Charge", obj.getDefaultCharge());
			contents.put("status", obj.getStatus());

		}

		// SSR Template
		if (persistant instanceof SSRTemplate) {
			SSRTemplate obj = (SSRTemplate) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_SSR_TEMPLATE));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_MODIFY_SSR_TEMPLATE));
			}

			StringBuilder sb = new StringBuilder();
			Set<SSRTemplateCabinQuantity> qtyCol = obj.getSsrTemplateCabinQtySet();
			for (SSRTemplateCabinQuantity ssrQty : qtyCol) {
				SSRInfoDTO ssrInfoDTO = SSRUtil.getSSRInfo(ssrQty.getSsrId());
				sb.append(ssrInfoDTO != null ? ssrInfoDTO.getSSRName() : ssrQty.getSsrId());
				sb.append(" - ");
				sb.append(ssrQty.getMaxQuantity());
				sb.append(" ");
			}

			contents.put("template code", obj.getTemplateCode());
			contents.put("ssr_price", sb.toString());
			contents.put("status", obj.getStatus());
		}

		if (persistant instanceof Meal) {
			Meal obj = (Meal) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_MEAL));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_MEAL));
			}

			contents.put("meal name", obj.getMealName());
			contents.put("description", obj.getMealDescription());
			contents.put("meal code", obj.getMealCode());
			contents.put("status", obj.getStatus());
			StringBuilder sb = new StringBuilder("");
			for(String strChannel : obj.getChannels()){
				sb.append(strChannel+" - ");
			}
			contents.put("Sales Channels", sb.toString());

		}

		if (persistant instanceof MealCategory) {
			MealCategory obj = (MealCategory) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_MEAL_CATEGORY));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_MEAL_CATEGORY));
			}
			contents.put("meal_category_name", obj.getMealCategoryName());
		}

		if (persistant instanceof Baggage) {
			Baggage obj = (Baggage) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_BAGGAGE));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_BAGGAGE));
			}

			contents.put("baggage name", obj.getBaggageName());
			contents.put("description", obj.getDescription());
			contents.put("status", obj.getStatus());

		}

		// //exchange rate automation auditng
		if (persistant instanceof ExRateAutoUpdateResultDTO) {
			ExRateAutoUpdateResultDTO obj = (ExRateAutoUpdateResultDTO) persistant;

			if (obj.isUpdated()) {
				audit.setTaskCode(TasksUtil.MASTER_EXCHANGE_RATE_UPDATE);
				if (obj.getChangedRate() != null && obj.getNewRate() != null) {
					// when existing rate is changed
					contents.put("Currency code", obj.getExchangeRateSourceInfo().getCurrencyCode());
					contents.put("isUpdated", obj.isUpdated());
					contents.put("old rate", obj.getChangedRate().getExrateBaseToCurNumber().toPlainString());
					contents.put("new rate", obj.getNewRate().getExrateBaseToCurNumber().toPlainString());
					contents.put("old period", obj.getChangedRate().getEffectiveFrom().toString() + " - "
							+ obj.getChangedRate().getEffectiveTo().toString());
					contents.put("new period", obj.getNewRate().getEffectiveFrom().toString() + " - "
							+ obj.getNewRate().getEffectiveTo().toString());
				} else if (obj.getChangedRate() == null && obj.getNewRate() != null) {
					// new rate is added
					contents.put("Currency code", obj.getExchangeRateSourceInfo().getCurrencyCode());
					contents.put("isUpdated", obj.isUpdated());
					contents.put("old rate", "");
					contents.put("new rate", obj.getNewRate().getExrateBaseToCurNumber().toPlainString());
					contents.put("old period", "");
					contents.put("new period", obj.getNewRate().getEffectiveFrom().toString() + " - "
							+ obj.getNewRate().getEffectiveTo().toString());

				}
			} else {
				audit.setTaskCode(TasksUtil.MASTER_EXCHANGE_RATE_UPDATE_FAILED);

				contents.put("Currency code", obj.getExchangeRateSourceInfo().getCurrencyCode());
				contents.put("isUpdated", new Boolean(obj.isUpdated()).toString());
				contents.put("reason", obj.getErrorMessage());

			}
		}

		if (persistant instanceof BuyNGetPromotionCriteria) {
			BuyNGetPromotionCriteria obj = (BuyNGetPromotionCriteria) persistant;
			if (obj.getPromoCriteriaID() != null) {
				audit.setTaskCode(TasksUtil.PROMOTION_EDIT_PROMO_CODE);
			} else {
				audit.setTaskCode(TasksUtil.PROMOTION_ADD_PROMO_CODE);
			}
			if (obj.getPromoCodes() != null) {
				StringBuilder promoCodesS = new StringBuilder("");
				for (PromoCode pc : obj.getPromoCodes()) {
					promoCodesS.append(pc.getPromoCode() + " ");
				}
				contents.put("Promo code", promoCodesS.toString());

			}

			contents.put("Promo Name", obj.getPromoName());
			contents.put("Promotion Type", "Buy and get Promotion");
			contents.put("Free criteria", obj.getFreeCriteria().toString());
			contents.put("Limited load factor", obj.getLimitingLoadFactor());
			contents.put("Status", obj.getStatus());

			contents.put("onds", obj.getApplicableONDs().toString());
			contents.put("COS", obj.getApplicableCOSs().toString());
			contents.put("FlightNos", obj.getFlightNOs().toString());
			contents.put("Travel agent", obj.getApplicableAgents().toString());
			contents.put("Limit Per flight", obj.getPerFlightLimit());
			contents.put("Limit Per agent", obj.getNumByAgent());
			contents.put("Limit Per sales channel", obj.getNumBySalesChannel());
			contents.put("Limit Total", obj.getTotalLimit());
			contents.put("Sales channel", obj.getApplicableSalesChannels().toString());
			contents.put("POSCountries", obj.getApplicablePosCountries().toString());

			// applicable member selection audit
			String applicableContent = " LoyaltyMembers:" + (obj.getApplicableForLoyaltyMembers() ? "Y" : "N")
					+ ", RegisteredMembers:" + (obj.getApplicableForRegisteredMembers() ? "Y" : "N") + ", Guests:"
					+ (obj.getApplicableForGuests() ? "Y" : "N");
			contents.put("Applicability", applicableContent);

			// date periods section audit
			if (obj.getRegistrationPeriods() != null) {
				contents.put("Registration Period", getDatePeriodContent(obj.getRegistrationPeriods()));
			}
			if (obj.getFlightPeriods() != null) {
				contents.put("Flight Period", getDatePeriodContent(obj.getFlightPeriods()));
			}

		}
		if (persistant instanceof DiscountPromotionCriteria) {
			DiscountPromotionCriteria obj = (DiscountPromotionCriteria) persistant;
			if (obj.getPromoCriteriaID() != null) {
				audit.setTaskCode(TasksUtil.PROMOTION_EDIT_PROMO_CODE);
			} else {
				audit.setTaskCode(TasksUtil.PROMOTION_ADD_PROMO_CODE);
			}
			if (obj.getPromoCodes() != null) {
				StringBuilder promoCodesS = new StringBuilder("");
				for (PromoCode pc : obj.getPromoCodes()) {
					promoCodesS.append(pc.getPromoCode() + " ");
				}
				contents.put("Promo codes", promoCodesS.toString());

			}
			contents.put("Promo Name", obj.getPromoName());
			contents.put("Promotion Type", "Discount");
			contents.put("Discount", obj.getDiscount().toString());
			contents.put("Status", obj.getStatus());

			contents.put("onds", obj.getApplicableONDs().toString());
			contents.put("COS", obj.getApplicableCOSs().toString());
			contents.put("FlightNos", obj.getFlightNOs().toString());
			contents.put("Travel agent", obj.getApplicableAgents().toString());
			contents.put("Limit Per flight", obj.getPerFlightLimit());
			contents.put("Limit Per agent", obj.getNumByAgent());
			contents.put("Limit Per sales channel", obj.getNumBySalesChannel());
			contents.put("Limit Total", obj.getTotalLimit());
			contents.put("Sales channel", obj.getApplicableSalesChannels().toString());
			contents.put("POSCountries", obj.getApplicablePosCountries().toString());

			// applicable member selection audit
			String applicableContent = " LoyaltyMembers:" + (obj.getApplicableForLoyaltyMembers() ? "Y" : "N")
					+ ", RegisteredMembers:" + (obj.getApplicableForRegisteredMembers() ? "Y" : "N") + ", Guests:"
					+ (obj.getApplicableForGuests() ? "Y" : "N");
			contents.put("Applicability", applicableContent);

			// date periods section audit
			if (obj.getRegistrationPeriods() != null) {
				contents.put("Registration Period", getDatePeriodContent(obj.getRegistrationPeriods()));
			}
			if (obj.getFlightPeriods() != null) {
				contents.put("Flight Period", getDatePeriodContent(obj.getFlightPeriods()));
			}

		}
		if (persistant instanceof FreeServicePromotionCriteria) {
			FreeServicePromotionCriteria obj = (FreeServicePromotionCriteria) persistant;
			if (obj.getPromoCriteriaID() != null) {
				audit.setTaskCode(TasksUtil.PROMOTION_EDIT_PROMO_CODE);
			} else {
				audit.setTaskCode(TasksUtil.PROMOTION_ADD_PROMO_CODE);
			}
			if (obj.getPromoCodes() != null) {
				StringBuilder promoCodesS = new StringBuilder("");
				for (PromoCode pc : obj.getPromoCodes()) {
					promoCodesS.append(pc.getPromoCode() + " ");
				}
				contents.put("Promo codes", promoCodesS.toString());

			}
			contents.put("Promo Name", obj.getPromoName());
			contents.put("Promotion Type", "Free Service");
			contents.put("Free criteria", obj.getDiscount().toString());
			contents.put("Apllicabel Ancilaries", obj.getApplicableAncillaries());

			contents.put("onds", obj.getApplicableONDs().toString());
			contents.put("COS", obj.getApplicableCOSs().toString());
			contents.put("FlightNos", obj.getFlightNOs().toString());
			contents.put("Travel agent", obj.getApplicableAgents().toString());
			contents.put("Limit Per flight", obj.getPerFlightLimit());
			contents.put("Limit Per agent", obj.getNumByAgent());
			contents.put("Limit Per sales channel", obj.getNumBySalesChannel());
			contents.put("Limit Total", obj.getTotalLimit());
			contents.put("Sales channel", obj.getApplicableSalesChannels().toString());
			contents.put("POSCountries", obj.getApplicablePosCountries().toString());

			// applicable member selection audit
			String applicableContent = " LoyaltyMembers:" + (obj.getApplicableForLoyaltyMembers() ? "Y" : "N")
					+ ", RegisteredMembers:" + (obj.getApplicableForRegisteredMembers() ? "Y" : "N") + ", Guests:"
					+ (obj.getApplicableForGuests() ? "Y" : "N");
			contents.put("Applicability", applicableContent);

			// date periods section audit
			if (obj.getRegistrationPeriods() != null) {
				contents.put("Registration Period", getDatePeriodContent(obj.getRegistrationPeriods()));
			}
			if (obj.getFlightPeriods() != null) {
				contents.put("Flight Period", getDatePeriodContent(obj.getFlightPeriods()));
			}

		}
		if (persistant instanceof GroupBookingRequest) {
			GroupBookingRequest obj = (GroupBookingRequest) persistant;

			audit.setTaskCode(TasksUtil.GROUP_BOOKING_REQUEST);

			if (obj.getRequestID() != null) {
				contents.put("Update Req ID", (int) (long) obj.getRequestID());
				contents.put("Req date", obj.getCreatedDate());

			} else {
				contents.put("New req date", obj.getCreatedDate());
			}

			if (obj.getGroupBookingRoutes() != null) {
				contents.put("Routes", obj.getGroupBookingRoutes().size());
				contents.put("Passenger amount", obj.getAdultAmount() + "," + obj.getChildAmount() + "," + obj.getInfantAmount());
				contents.put("Requested fare", obj.getRequestedFare());
			}
			contents.put("Status", obj.getStatusID());

		}

		if (persistant instanceof PaymentGatewayWiseCharges) {
			PaymentGatewayWiseCharges obj = (PaymentGatewayWiseCharges) persistant;

			contents.put("paymentGateway ID", obj.getPaymentGatewayId());
			if (persistant.getVersion() > 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.PGW_WISE_CHARGE_EDIT));

				contents.put("effective to:", obj.getChargeEffectiveToDate() + " to "
						+ obj.getOldPaymentGatewayWiseCharges().getChargeEffectiveToDate());
				contents.put("status Change from:", obj.getStatus() + " to " + obj.getOldPaymentGatewayWiseCharges().getStatus());
				contents.put("value percentage Change from:",
						obj.getValuePercentageFlag() + " to" + obj.getOldPaymentGatewayWiseCharges().getValuePercentageFlag());
				if (obj.getValuePercentageFlag().equals("V")) {
					contents.put("amount change from:", obj.getValueInDefaultCurrency() + " to "
							+ obj.getOldPaymentGatewayWiseCharges().getValueInDefaultCurrency());
				} else {
					contents.put("amount change from:", obj.getChargeValuePercentage() + " to "
							+ obj.getOldPaymentGatewayWiseCharges().getChargeValuePercentage());
				}

			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.PGW_WISE_CHARGE_ADD));

				contents.put("effective from", obj.getChargeEffectiveFromDate());
				contents.put("effective to", obj.getChargeEffectiveToDate());
				contents.put("status", obj.getStatus());
				contents.put("value percentage", obj.getValuePercentageFlag());
				if (obj.getValuePercentageFlag().equals("V")) {
					contents.put("amount", obj.getValueInDefaultCurrency());
				} else {
					contents.put("amount", obj.getChargeValuePercentage());
				}
			}
		}

		if (persistant instanceof ItineraryAdvertisement) {
			ItineraryAdvertisement obj = (ItineraryAdvertisement) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_ADVERTISEMENT));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_ADVERTISEMENT));
			}

			contents.put("title", obj.getAdvertisementTitle());
			contents.put("advertisement img", obj.getAdvertisementImage());
			contents.put("language code", obj.getAdvertisementLanguage());
			contents.put("status", obj.getStatus());

		}

		if (persistant instanceof State) {
			State obj = (State) persistant;

			if (persistant.getVersion() < 0) {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_ADD_STATE));
			} else {
				audit.setTaskCode(String.valueOf(TasksUtil.MASTER_EDIT_STATE));
			}

			contents.put("code", obj.getStateCode());
			contents.put("state name", obj.getStateName());
			contents.put("state country", obj.getCountryCode());
			contents.put("status", obj.getStatus());
			contents.put("tax RegNo", obj.getAirlineOfficeTaxRegNo());
			contents.put("digital signature", obj.getDigitalSignature());

		}

		AirmasterUtils.getAuditorBD().audit(audit, contents);

	}

	private static <T extends DatePeriod> String getDatePeriodContent(Set<T> datePeriodSet) {
		StringBuilder datePeriodContent = new StringBuilder("");
		for (T obj : datePeriodSet) {
			datePeriodContent.append(obj.getFromDate() + "-" + obj.getToDate() + ", ");
		}
		return datePeriodContent.toString();
	}
}
