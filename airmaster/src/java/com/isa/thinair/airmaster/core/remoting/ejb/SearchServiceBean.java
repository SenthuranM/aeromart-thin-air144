package com.isa.thinair.airmaster.core.remoting.ejb;

import java.util.Collection;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airmaster.api.dto.SmartSearchDTO;
import com.isa.thinair.airmaster.core.persistence.dao.SearchDAO;
import com.isa.thinair.airmaster.core.service.bd.SearchServiceImpl;
import com.isa.thinair.airmaster.core.service.bd.SearchServiceLocalImpl;
import com.isa.thinair.airmaster.core.util.SearchServiceUtil;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

@Stateless
@RemoteBinding(jndiBinding = "SearchService.remote")
@LocalBinding(jndiBinding = "SearchService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class SearchServiceBean extends PlatformBaseSessionBean implements SearchServiceImpl, SearchServiceLocalImpl {

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<String> search(SmartSearchDTO search) throws ModuleException {
		try {
			return lookupSearchDAO().search(SearchServiceUtil.transform(search));
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	private SearchDAO lookupSearchDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (SearchDAO) lookupService.getBean("isa:base://modules/airmaster?id=SearchDAOProxy");
	}

}
