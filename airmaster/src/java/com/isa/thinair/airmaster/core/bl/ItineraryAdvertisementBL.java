/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airmaster.core.bl;

import com.isa.thinair.airmaster.api.model.ItineraryAdvertisement;
import com.isa.thinair.airmaster.core.persistence.dao.CommonMasterDAO;
import com.isa.thinair.commons.api.exception.ModuleException;

public class ItineraryAdvertisementBL {
	
	private CommonMasterDAO commonMasterDAO;
	
	public ItineraryAdvertisementBL() {
		commonMasterDAO = AirmasterDAOUtil.getCommonMasterDAO();
	}
	
	public void saveOrUpdateAdvertisement(ItineraryAdvertisement advertisement) throws ModuleException {
		ItineraryAdvertisement exist = null;
	

		if (advertisement.getAdvertisementId() != null && advertisement.getAdvertisementId().intValue() > 0) {
			exist = commonMasterDAO.getAdvertisement(advertisement.getAdvertisementId());
			if(exist != null){
			}
		}
		commonMasterDAO.saveAdvertisement(advertisement);
		if (advertisement.getVersion() > 0) {
			if (!advertisement.getStatus().equals(exist.getStatus())) {
		
			}
		}
	}

}
