package com.isa.thinair.airmaster.core.bl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.dto.SpecialServiceRequestDTO;
import com.isa.thinair.airmaster.api.model.I18nMessage;
import com.isa.thinair.airmaster.api.model.I18nMessageKey;
import com.isa.thinair.airmaster.api.model.Language;
import com.isa.thinair.airmaster.api.model.SSR;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.airmaster.core.persistence.dao.SsrDAO;
import com.isa.thinair.airmaster.core.util.LookUpUtils;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.dto.SSRInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.util.I18nTranslationUtil;
import com.isa.thinair.commons.api.util.I18nTranslationUtil.I18nMessageCategory;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.DataConverterUtil;
import com.isa.thinair.commons.core.util.StringUtil;

public class SsrBL implements Serializable {

	private static final long serialVersionUID = -3096258231720106949L;

	private SsrDAO ssrDAO;

	public SsrBL() {
		if (ssrDAO == null) {
			this.ssrDAO = LookUpUtils.getSsrDAO();
		}
	}

	public List<SpecialServiceRequestDTO> getActiveSSRsByFltSegIds(Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos,
			String ssrCategory, String salesChannel, boolean checkSSRInventory, String selectedLanguage, boolean isModifyAnci,
			boolean isAddEditAnci, boolean hasPrivModifySSRWithinBuffer, boolean isUserDefinedSsrOnly) throws ModuleException {
		List<SpecialServiceRequestDTO> ssrDTOs = new ArrayList<SpecialServiceRequestDTO>();
		if (flightSegIdWiseCos != null) {
			for (Entry<Integer, ClassOfServiceDTO> cosEntry : flightSegIdWiseCos.entrySet()) {
				Integer flightSegId = cosEntry.getKey();
				ClassOfServiceDTO cosDTO = cosEntry.getValue();

				FlightSegement flightSegement = AirmasterUtils.getFlightBD().getFlightSegment(flightSegId);

				// calculating cutover for current segment
				Date currTime = CalendarUtil.getCurrentSystemTimeInZulu();
				Date deptTime = flightSegement.getEstTimeDepatureZulu();

				double cutoverInMin = (deptTime.getTime() - currTime.getTime()) / (1000 * 60);

				List<SpecialServiceRequestDTO> fltSegResponse = ssrDAO.getActiveSSRsByFltSegId(flightSegId,
						cosDTO.getSegmentCode(), ssrCategory, salesChannel, checkSSRInventory, selectedLanguage,
						cosDTO.getCabinClassCode(), cosDTO.getLogicalCCCode(), isModifyAnci, new Double(cutoverInMin).intValue(),
						false, isAddEditAnci, hasPrivModifySSRWithinBuffer, isUserDefinedSsrOnly);
				List<SpecialServiceRequestDTO> ssrList = getApplicableSSRDTOs(fltSegResponse, (isAddEditAnci || isModifyAnci),
						cutoverInMin, hasPrivModifySSRWithinBuffer , isUserDefinedSsrOnly);
				if (ssrList != null) {
					ssrDTOs.addAll(ssrList);
				}
			}
			
		}

		return ssrDTOs;
	}

	public Map<Integer, List<SpecialServiceRequestDTO>> getAvailableInflightService(Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos,
			String salesChannel, boolean checkInventory, boolean isModifyAnci, boolean skipeCutoverValidation)
			throws ModuleException {
		Map<Integer, List<SpecialServiceRequestDTO>> availableServiceMap = new HashMap<Integer, List<SpecialServiceRequestDTO>>();

		if (flightSegIdWiseCos != null) {
			for (Entry<Integer, ClassOfServiceDTO> entry : flightSegIdWiseCos.entrySet()) {

				int flightSegId = entry.getKey();

				FlightSegement flightSegement = AirmasterUtils.getFlightBD().getFlightSegment(flightSegId);

				// calculating cutover for current segment
				Date currTime = CalendarUtil.getCurrentSystemTimeInZulu();
				Date deptTime = flightSegement.getEstTimeDepatureZulu();

				double cutoverInMin = (deptTime.getTime() - currTime.getTime()) / (1000 * 60);

				List<SpecialServiceRequestDTO> specialServiceRequestDTOs = ssrDAO.getActiveSSRsByFltSegId(flightSegId,
						entry.getValue().getSegmentCode(), SSRCategory.ID_INFLIGHT_SERVICE.toString(), salesChannel,
						checkInventory, null, entry.getValue().getCabinClassCode(),
						entry.getValue().getLogicalCCCode(), isModifyAnci, new Double(cutoverInMin).intValue(),
						skipeCutoverValidation, false, false, false);
				availableServiceMap.put(entry.getKey(), specialServiceRequestDTOs);
			}
		}

		return availableServiceMap;
	}

	public Map<AirportServiceKeyTO, List<AirportServiceDTO>> getAvailableAirportServices(
			Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseSegMap, AppIndicatorEnum appIndicator, int ssrCategory,
			Map<String, Integer> segmentBundledFareInclusion, String selectedLanguage, boolean isAddEditAnci)
			throws ModuleException {
		Map<AirportServiceKeyTO, List<AirportServiceDTO>> airportWiseMap = new LinkedHashMap<AirportServiceKeyTO, List<AirportServiceDTO>>();
		for (Entry<AirportServiceKeyTO, FlightSegmentTO> entry : airportWiseSegMap.entrySet()) {
			List<AirportServiceDTO> airportServiceDTOs = new ArrayList<AirportServiceDTO>();
			AirportServiceKeyTO keyTO = entry.getKey();
			FlightSegmentTO fltSeg = entry.getValue();

			// calculating cutover for current segment
			Date currTime = CalendarUtil.getCurrentSystemTimeInZulu();
			Date deptTime = fltSeg.getDepartureDateTimeZulu();

			double cutoverInMin = (deptTime.getTime() - currTime.getTime()) / (1000 * 60);

			if (selectedLanguage == null || "".equals(selectedLanguage)) {
				selectedLanguage = Locale.ENGLISH.getLanguage();
			}

			Integer selectedBundledServicePeriodId = null;
			Set<Integer> freeApsIds = null;
			if (segmentBundledFareInclusion != null && segmentBundledFareInclusion.values() != null
					&& !segmentBundledFareInclusion.values().isEmpty()) {
				List<BundledFareDTO> bundledFareDTOs = AirmasterUtils.getBundledFareBD().getBundledFareDTOsByBundlePeriodIds(
						new HashSet<Integer>(segmentBundledFareInclusion.values()));
				if (segmentBundledFareInclusion != null && segmentBundledFareInclusion.containsKey(fltSeg.getSegmentCode())
						&& segmentBundledFareInclusion.get(fltSeg.getSegmentCode()) != null) {
					selectedBundledServicePeriodId = segmentBundledFareInclusion.get(fltSeg.getSegmentCode());
					for (BundledFareDTO bundledFareDTO : bundledFareDTOs) {
						if (bundledFareDTO.getBundledFarePeriodId().equals(selectedBundledServicePeriodId)) {
							freeApsIds = bundledFareDTO.getApplicableApsList();
						}
					}
				}
			}

			List<AirportServiceDTO> lst = ssrDAO.getAvailableAirportServices(keyTO.getAirport(), keyTO.getAirportType(),
					appIndicator, new Double(cutoverInMin).intValue(), ssrCategory, selectedLanguage, fltSeg.getCabinClassCode(),
					fltSeg.getLogicalCabinClassCode(), selectedBundledServicePeriodId, freeApsIds, isAddEditAnci);

			if (lst.size() > 0) {
				airportServiceDTOs.addAll(lst);
			}

			airportWiseMap.put(keyTO, airportServiceDTOs);

		}

		return airportWiseMap;
	}

	public boolean isAirportServicesAvailable(Map<AirportServiceKeyTO, FlightSegmentDTO> airportWiseSegMap, String appCode,
			int ssrCategory, String selectedLanguage, String cabinClass, String logicalCabinClass) {
		Map<AirportServiceKeyTO, List<AirportServiceDTO>> airportWiseMap = new LinkedHashMap<AirportServiceKeyTO, List<AirportServiceDTO>>();
		boolean status = false;
		for (Entry<AirportServiceKeyTO, FlightSegmentDTO> entry : airportWiseSegMap.entrySet()) {
			List<AirportServiceDTO> airportServiceDTOs = new ArrayList<AirportServiceDTO>();
			AirportServiceKeyTO keyTO = entry.getKey();
			FlightSegmentDTO fltSeg = entry.getValue();

			// calculating cutover for current segment
			Date currTime = CalendarUtil.getCurrentSystemTimeInZulu();
			Date deptTime = fltSeg.getDepartureDateTimeZulu();

			double cutoverInMin = (deptTime.getTime() - currTime.getTime()) / (1000 * 60);

			if (selectedLanguage == null || "".equals(selectedLanguage)) {
				selectedLanguage = Locale.ENGLISH.getLanguage();
			}
			AppIndicatorEnum appIndicator = AppIndicatorEnum.APP_IBE;

			if (appCode == "XBE") {
				appIndicator = AppIndicatorEnum.APP_XBE;

			} else if (appCode == "IBE") {
				appIndicator = AppIndicatorEnum.APP_IBE;
			}
			List<AirportServiceDTO> lst = ssrDAO.getAvailableAirportServices(keyTO.getAirport(), keyTO.getAirportType(),
					appIndicator, new Double(cutoverInMin).intValue(), ssrCategory, selectedLanguage, cabinClass,
					logicalCabinClass, null, null, false);

			if (!lst.isEmpty()) {
				status = true;
			}

		}

		return status;
	}

	public Map<String, List<AirportServiceDTO>> getAvailableAirportServices(Map<Integer, List<String>> airportFltSegMap,
			boolean skipCutoverValidation) throws ModuleException {
		Map<String, List<AirportServiceDTO>> airportWiseMap = new HashMap<String, List<AirportServiceDTO>>();

		FlightBD flightBd = AirmasterUtils.getFlightBD();

		Collection<Integer> fltSegIDs = new ArrayList<Integer>();

		for (Integer fltSegId : airportFltSegMap.keySet()) {
			fltSegIDs.add(fltSegId);
		}

		Collection<FlightSegmentDTO> colSegs = flightBd.getFlightSegments(fltSegIDs);

		for (FlightSegmentDTO fltSeg : colSegs) {
			Integer flightSegId = fltSeg.getSegmentId();
			Calendar departTime = new GregorianCalendar();
			departTime.setTime(fltSeg.getDepartureDateTimeZulu());
			Calendar currTime = new GregorianCalendar();

			long differnce = departTime.getTimeInMillis() - currTime.getTimeInMillis();

			List<String> airports = airportFltSegMap.get(flightSegId);

			for (String airport : airports) {
				List<AirportServiceDTO> apsList = ssrDAO.getSelectedAirportServices(airport, differnce,
						SSRCategory.ID_AIRPORT_SERVICE, skipCutoverValidation);

				airportWiseMap.put(airport + AirportServiceDTO.SEPERATOR + flightSegId, apsList);
			}

		}

		return airportWiseMap;
	}

	public void saveSSRLanguageTranslation(SSRInfoDTO ssrInfoDTO) throws ModuleException {

		SSR ssrInfo = ssrDAO.getSSR(ssrInfoDTO.getSSRId());

		I18nMessageKey i18nMessageKey = ssrInfo.getI18nMessageKey();

		if (i18nMessageKey == null) {
			// New Translations
			String i18nMsgKey = I18nTranslationUtil.getI18nSSRDescriptionKey(ssrInfo.getSsrId());

			i18nMessageKey = new I18nMessageKey();
			ssrInfo.setI18nMessageKey(i18nMessageKey);

			i18nMessageKey.setI18nMsgKey(i18nMsgKey);
			i18nMessageKey.setMessageCategory(I18nMessageCategory.SSR_DES.toString());

			if (ssrInfoDTO.getSSRName() != null) {
				// SET ENGLISH FOR ALL LANGUAGES
				createTranlatedMessages(ssrInfoDTO, i18nMessageKey);
			} else {
				updateTranlatedMessages(ssrInfoDTO, i18nMessageKey);
			}

		} else {
			// Update existing Translations
			updateTranlatedMessages(ssrInfoDTO, i18nMessageKey);
		}

		ssrDAO.saveSSR(ssrInfo);
	}

	private I18nMessage getMessageByLocale(I18nMessageKey i18nMessageKey, String languageCode) {

		Iterator<I18nMessage> iterator = i18nMessageKey.getI18nMessages().iterator();
		while (iterator.hasNext()) {
			I18nMessage i18nMessage = iterator.next();
			if (i18nMessage.getMessageLocale().equalsIgnoreCase(languageCode)) {
				return i18nMessage;
			}
		}
		return null;
	}

	private void updateTranlatedMessages(SSRInfoDTO ssrInfoDTO, I18nMessageKey i18nMessageKey) throws ModuleException {

		if (ssrInfoDTO.getSelectedLanguageTranslations() != null && !ssrInfoDTO.getSelectedLanguageTranslations().isEmpty()) {

			Collection<Language> langauges;
			String translationDescStr = ssrInfoDTO.getSelectedLanguageTranslations();
			langauges = AirmasterUtils.getCommonMasterBD().getLanguages();

			for (Language language : langauges) {
				String transStrTemp = getLanguageTranslation(translationDescStr, language.getLanguageCode());

				if (transStrTemp == null || transStrTemp.isEmpty()) {
					transStrTemp = ssrInfoDTO.getSSRDescription();
				}

				String hexString = StringUtil.convertToHex(transStrTemp);

				I18nMessage i18nMessage = getMessageByLocale(i18nMessageKey, language.getLanguageCode());

				if (i18nMessage == null) {
					i18nMessage = new I18nMessage();
					i18nMessage.setMessageLocale(language.getLanguageCode());
					i18nMessage.setMessageContent(DataConverterUtil.createClob(hexString));
					i18nMessageKey.addI18nMessage(i18nMessage);
				} else {
					i18nMessage.setMessageLocale(language.getLanguageCode());
					i18nMessage.setMessageContent(DataConverterUtil.createClob(hexString));
				}

			}

		}

	}

	private String getLanguageTranslation(String translationDescStr, String lang) throws ModuleException {

		String langTraStr = null;

		if (translationDescStr != null && !translationDescStr.isEmpty()) {
			String[] splitedCodes = translationDescStr.split("\\~");

			for (int i = 0; i < splitedCodes.length; i++) {
				String[] lan_Translation = splitedCodes[i].split(",");
				String langCode = lan_Translation[1].trim();

				if (lang.equalsIgnoreCase(langCode)) {
					langTraStr = lan_Translation[2].trim();
					break;
				}
			}
		}

		return langTraStr;
	}

	private void createTranlatedMessages(SSRInfoDTO ssrInfoDTO, I18nMessageKey i18nMessageKey) throws ModuleException {
		Collection<Language> langauges;

		langauges = AirmasterUtils.getCommonMasterBD().getLanguages();
		for (Language language : langauges) {
			I18nMessage i18nMessage = new I18nMessage();
			i18nMessage.setMessageLocale(language.getLanguageCode());

			String hexString = StringUtil.convertToHex(ssrInfoDTO.getSSRDescription().trim());

			String translationDescStr = ssrInfoDTO.getSelectedLanguageTranslations();
			if (translationDescStr != null && !translationDescStr.isEmpty()) {

				String transStrTemp = getLanguageTranslation(translationDescStr, language.getLanguageCode());

				if (transStrTemp != null) {
					hexString = StringUtil.convertToHex(transStrTemp);
				}
			}

			i18nMessage.setMessageContent(DataConverterUtil.createClob(hexString));
			i18nMessageKey.addI18nMessage(i18nMessage);
		}

	}

	private List<SpecialServiceRequestDTO> getApplicableSSRDTOs(List<SpecialServiceRequestDTO> ssrDTOs, boolean isInEditMode,
			double cutoverInMin, boolean hasPrivModifySSRWithinBuffer, boolean isUserDefinedSsrOnly) {
		// if (isInEditMode) {
		//
		// if (cutoverInMin < modificationStopBuffer) {
		// if (hasPrivModifySSRWithinBuffer) {
		// //return true;
		// } else {
		// //return false;
		// }
		// } else {
		// //return true;
		// }
		// } else if (cutoverInMin < diffMils) {
		//
		// }
		long diffMillis = AppSysParamsUtil.getSSRCutOverTimeInMillis();
		long modificationStopBuffer = AppSysParamsUtil.getInternationalBufferDurationInMillis();
		long depTimeGap = (long) (cutoverInMin * 60 * 1000);
		List<SpecialServiceRequestDTO> ssrList = new ArrayList<SpecialServiceRequestDTO>();

		if (ssrDTOs != null) {
			if (isUserDefinedSsrOnly || (isInEditMode && hasPrivModifySSRWithinBuffer)) {
				ssrList.addAll(ssrDTOs);
			} else {
				for (SpecialServiceRequestDTO ssr : ssrDTOs) {

					if (ssr.getCutOffTime() == null) {
						if (depTimeGap > diffMillis) {
							ssrList.add(ssr);
						}
					} else {
						if (depTimeGap > AppSysParamsUtil.getTimeInMillis(ssr.getCutOffTime())) {
							ssrList.add(ssr);
						}
					}
				}
			}

		}

		return ssrList;
	}

}
