package com.isa.thinair.airmaster.core.bl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.dto.AutomaticCheckinDTO;
import com.isa.thinair.airmaster.core.persistence.dao.AutomaticCheckinDAO;
import com.isa.thinair.airmaster.core.util.LookUpUtils;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Mohamed Rizwan
 *
 */
public class AutoCheckinBL implements Serializable {

	private static final long serialVersionUID = 4186367317372589594L;

	private AutomaticCheckinDAO autoCheckinDAO;

	public AutoCheckinBL() {
		if (autoCheckinDAO == null) {
			this.autoCheckinDAO = LookUpUtils.getAutoCheckinDAO();
		}
	}

	/**
	 * This is used to get available automatic checkin data's for all flight segments
	 * 
	 * @param airportWiseSegMap
	 * @return
	 * @throws ModuleException
	 */
	public Map<AirportServiceKeyTO, List<AutomaticCheckinDTO>> getAvailableAutomaticCheckins(
			Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseSegMap) throws ModuleException {

		Map<AirportServiceKeyTO, List<AutomaticCheckinDTO>> airportWiseMap = new LinkedHashMap<AirportServiceKeyTO, List<AutomaticCheckinDTO>>();
		FlightSegmentTO fltSeg = null;
		AirportServiceKeyTO keyTO = null;
		List<AutomaticCheckinDTO> automaticCheckinDTOs = null;
		for (Entry<AirportServiceKeyTO, FlightSegmentTO> entry : airportWiseSegMap.entrySet()) {
			automaticCheckinDTOs = new ArrayList<AutomaticCheckinDTO>();
			fltSeg = entry.getValue();
			keyTO = entry.getKey();

			if ((keyTO != null) && (keyTO.getAirportType() != null) && (keyTO.getAirport() != null)) {
				// automatic checkin available only at departure airport
				if (keyTO.getAirportType().equalsIgnoreCase(ReservationPaxSegmentSSR.APPLY_ON_ARRIVAL))
					continue;
				automaticCheckinDTOs = autoCheckinDAO.getAvailableAutomaticCheckins(keyTO.getAirport());
			} else {
				if ((fltSeg != null) && (fltSeg.getSegmentCode() != null) && !fltSeg.getSegmentCode().isEmpty()) {
					automaticCheckinDTOs = autoCheckinDAO.getAvailableAutomaticCheckins(fltSeg.getSegmentCode().split("/")[0]);
				}
			}
			airportWiseMap.put(entry.getKey(), automaticCheckinDTOs);
		}

		return airportWiseMap;
	}
	
	public List<AutomaticCheckinDTO> getAutomaticCheckinDetails(String airportCode) throws ModuleException {
		return autoCheckinDAO.getAvailableAutomaticCheckins(airportCode);
	}

}
