package com.isa.thinair.airmaster.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.dto.AirportTransferDTO;
import com.isa.thinair.airmaster.api.dto.ServiceProviderDTO;
import com.isa.thinair.airmaster.api.model.AirportTransfer;
import com.isa.thinair.airmaster.api.model.AirportTransferTemplate;
import com.isa.thinair.airmaster.api.model.AirportTransferTemplateBC;
import com.isa.thinair.airmaster.core.persistence.dao.AirportTransferDAO;
import com.isa.thinair.airmaster.core.util.AncillaryModuleUtils;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 * @author Manoj Dhanushka
 * 
 * AirportTransferDAOImpl is the AirportTransferDAO implementatiion for hibernate
 * @isa.module.dao-impl dao-name="AirportTransferDAO"
 * 
 */
public class AirportTransferDAOImpl extends PlatformBaseHibernateDaoSupport implements AirportTransferDAO{
	
	@Override
	public AirportTransfer getAirportTransfer(String airportCode) {
		List<AirportTransfer> transfers =  find("from AirportTransfer at where at.airportCode = ?", airportCode, AirportTransfer.class);
		if(transfers != null && !transfers.isEmpty()){
			return transfers.get(0);
		}else{
			return null;
		}
	}
	
	@Override
	public AirportTransfer getAirportTransfer(int id) {
		List<AirportTransfer> transfers =  find("from AirportTransfer at where at.airportTransferId = ?", id, AirportTransfer.class);
		if(transfers != null && !transfers.isEmpty()){
			return transfers.get(0);
		}else{
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Page<AirportTransfer> getAirportTransfers(int startRec, int numOfRecs, String airportCode, String status) {
		Page<AirportTransfer> result = null;
		if (airportCode == null && status == null) {
			List<String> ob = new ArrayList<String>();
			ob.add("airportCode");
			result = getPagedData(null, startRec, numOfRecs, AirportTransfer.class, ob);
		} else {
			String common = "from AirportTransfer as transfer where transfer.airportCode like '" 
					          +  (airportCode == null ? "%" : "%" + airportCode) + "'" + " and transfer.status like  '"
					          +  (status == null ? "%" : "%" + status) + "'";
			
			String hql = " Select transfer " + common;
			List list = getSession().createQuery(hql).setFirstResult(startRec).setMaxResults(numOfRecs).list();
			
			String hqlCount = " Select count(transfer) " + common;
			int count = ((Long) getSession().createQuery(hqlCount).uniqueResult()).intValue();
			System.out.println("Count:  " +count);

			result = new Page(count, startRec, startRec + numOfRecs - 1, list);

		}
		return result;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Page<AirportTransferTemplate> getAirportTransferTemplates(int startRec, int numOfRecs,
			AirportTransferDTO airportTransferDTO) {
		String common = "from AirportTransferTemplate as transfer where transfer.modelNumber like '"
					+ (airportTransferDTO.getAircraftModel() == null ? "%" : "%" + airportTransferDTO.getAircraftModel()) + "'"
					+ " and transfer.cabinClassCode like  '"
					+ (airportTransferDTO.getCabinClassCode() == null ? "%" : "%" + airportTransferDTO.getCabinClassCode()) + "'"
					+ " and transfer.flightNumber like  '"
					+ (airportTransferDTO.getFlightNo() == null ? "%" : "%" + airportTransferDTO.getFlightNo()) + "'"
					+ " and transfer.status like  '"
					+ (airportTransferDTO.getStatus() == null ? "%" : "%" + airportTransferDTO.getStatus()) + "'";

			String hql = " Select transfer " + common;
			List list = getSession().createQuery(hql).setFirstResult(startRec).setMaxResults(numOfRecs).list();
			
			String hqlCount = " Select count(transfer) " + common;
			int count = ((Long) getSession().createQuery(hqlCount).uniqueResult()).intValue();
			System.out.println("Count:  " +count);

			return new Page(count, startRec, startRec + numOfRecs - 1, list);
	}

	@Override
	public void saveAirportTransfers(AirportTransfer airportTransfer) {
		hibernateSaveOrUpdate(airportTransfer);
	}
	
	@Override
	public void saveAirportTransferTemplate(AirportTransferTemplate airportTransferTemplate) {
		hibernateSaveOrUpdate(airportTransferTemplate);
	}
	
	@Override
	public Map<String,Integer> getAirportTransferBookingClasses(int airportTransferTempId){
		
		String query = "SELECT * FROM T_AIRPORT_TRANSFER_TEMPLATE_BC WHERE AIRPORT_TRANSFER_TEMPLATE_ID = " + airportTransferTempId;
		JdbcTemplate jdbcTemplate = new JdbcTemplate(AncillaryModuleUtils.getDatasource());
		
		Map<String,Integer> bookingClassIDMap = jdbcTemplate.query(query,
				new ResultSetExtractor<Map<String,Integer>>() {
			@Override
			public Map<String,Integer> extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String,Integer> bookingClassIDMap = new HashMap<String, Integer>();
				while (rs.next()) {
					bookingClassIDMap.put(rs.getString("BOOKING_CODE"), rs.getInt("AIRPORT_TRANS_TEMPLATE_BC_ID"));
				}

				return bookingClassIDMap;
			}
		});
		
		return bookingClassIDMap;
	}
	
	@Override
	public void updateAirportTransferTemplate(AirportTransferTemplate airportTransferTemplate) {
		merge(airportTransferTemplate);
	}
	
	@Override
	public List<AirportServiceDTO> getAvailableAirportTransfers(AirportServiceKeyTO keyTO, FlightSegmentTO fltSeg) {

		StringBuilder sb = new StringBuilder();
		sb.append("   SELECT att.ond_code            , ");
		sb.append("   att.cabin_class_code           , ");
		sb.append("   att.flight_number              , ");
		sb.append("   att.model_number               , ");
		sb.append("   att.departure_date             , ");
		sb.append("   apt.airport_code			  	 , ");
		sb.append("   att.adult_amount               , ");
		sb.append("   att.child_amount               , ");
		sb.append("   att.infant_amount			    ,  ");
		sb.append("   apt.airport_tranfer_id	  ,  ");
		sb.append("   apt.service_provider_name   ,  ");
		sb.append("   apt.service_provider_address,  ");
		sb.append("   apt.service_provider_number ,  ");
		sb.append("   apt.service_provider_email     ");
		sb.append("   FROM t_airport_transfer_template att  ");
		sb.append(" INNER JOIN t_airport_transfer apt  ");
		sb.append("   ON apt.airport_code ='" + keyTO.getAirport() + "'");
		sb.append("   and apt.status ='ACT'  ");
		
		sb.append(" INNER JOIN T_AIRPORT_TRANSFER_TEMPLATE_BC attbc  ");
		sb.append("   ON att.AIRPORT_TRANSFER_TEMPLATE_ID = attbc.AIRPORT_TRANSFER_TEMPLATE_ID ");
		
		sb.append("   WHERE (att.ond_code like '%All/All' or att.ond_code like '" + keyTO.getSegmentCode() + "'");
		sb.append("   or att.ond_code like 'All/"
				+ keyTO.getSegmentCode().split("/")[keyTO.getSegmentCode().split("/").length - 1] + "'");
		sb.append("   or att.ond_code like '" + keyTO.getSegmentCode().split("/")[0] + "/All' ) ");
		
//		sb.append("   and (att.cabin_class_code like 'ANY' or att.cabin_class_code like '" + fltSeg.getLogicalCabinClassCode()
//				+ "')");
		sb.append("   AND attbc.BOOKING_CODE LIKE '"+ fltSeg.getBookingClass() +"'");
		
		sb.append("   and (att.flight_number like 'ANY' or att.flight_number like '" + fltSeg.getFlightNumber() + "')");
		sb.append("   and (att.model_number like 'ANY' or att.model_number = (SELECT f.model_number FROM t_flight_segment fs, ");
		sb.append("   t_flight f WHERE fs.flight_id = f.flight_id AND fs.flt_seg_id = '" + fltSeg.getFlightSegId() + "'))");
		sb.append("   and (TO_CHAR(att.departure_date, 'DD-MON-YYYY') in  ");
		sb.append("   (SELECT TO_CHAR(fs.est_time_departure_zulu, 'DD-MON-YYYY') FROM t_flight_segment fs WHERE fs.flt_seg_id  = '"
				+ fltSeg.getFlightSegId() + "')");
		sb.append("   OR att.departure_date is null )");
		sb.append("   and att.status = 'ACT'");

		JdbcTemplate jdbcTemplate = new JdbcTemplate(AncillaryModuleUtils.getDatasource());

		List<AirportServiceDTO> airportServiceList = jdbcTemplate.query(sb.toString(),
				new ResultSetExtractor<List<AirportServiceDTO>>() {
					@Override
					public List<AirportServiceDTO> extractData(ResultSet rs) throws SQLException, DataAccessException {
						Set<AirportServiceDTO> airportServiceDTOs = new HashSet<AirportServiceDTO>();

						while (rs.next()) {
							AirportServiceDTO airportService = new AirportServiceDTO();
							airportService.setSsrName("Airport Transfer");
							airportService.setDescription("Transport");
							airportService.setAdultAmount(rs.getString("adult_amount"));
							airportService.setChildAmount(rs.getString("child_amount"));
							airportService.setInfantAmount(rs.getString("infant_amount"));
							airportService.setStatus("Y");
							airportService.setAirport(rs.getString("airport_code"));
							ServiceProviderDTO provider = new ServiceProviderDTO();
							provider.setAirportTransferId(rs.getInt("airport_tranfer_id"));
							provider.setName(rs.getString("service_provider_name"));
							provider.setAddress(rs.getString("service_provider_address"));
							provider.setNumber(rs.getString("service_provider_number"));
							provider.setEmailId(rs.getString("service_provider_email"));
							airportService.setProvider(provider);

							airportServiceDTOs.add(airportService);
						}

						return new ArrayList<AirportServiceDTO>(airportServiceDTOs);
					}
				});

		return airportServiceList;
	}

}
