/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Oct 9, 2008 
 * 
 * $Id: TranslationServiceBean.java,v 1.0
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.core.remoting.ejb;

import java.util.Collection;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airmaster.api.model.AirportForDisplay;
import com.isa.thinair.airmaster.api.model.CityForDisplay;
import com.isa.thinair.airmaster.api.model.CurrencyForDisplay;
import com.isa.thinair.airmaster.api.model.MealForDisplay;
import com.isa.thinair.airmaster.core.persistence.dao.TranslationDAO;
import com.isa.thinair.airmaster.core.service.bd.TranslationServiceDelegateImpl;
import com.isa.thinair.airmaster.core.service.bd.TranslationServiceLocalDelegateImpl;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * @author Haider 05Mar09
 */
@Stateless
@RemoteBinding(jndiBinding = "TranslationService.remote")
@LocalBinding(jndiBinding = "TranslationService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class TranslationServiceBean extends PlatformBaseSessionBean implements TranslationServiceDelegateImpl,
		TranslationServiceLocalDelegateImpl {

	public void saveOrUpdateAirport(AirportForDisplay obj) throws ModuleException {
		try {
			lookupTranslationDAO().saveAirport(obj);

		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");

		}
	}

	public void saveOrUpdateAirports(Collection<AirportForDisplay> col) throws ModuleException {
		try {
			lookupTranslationDAO().saveAirports(col);

		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");

		}
	}

	public void removeAirport(String airportCode, boolean isMealNotifyEnable, int mealNotifyId) throws ModuleException {
		try {
			if (isMealNotifyEnable && (mealNotifyId > 0)) {
				lookupTranslationDAO().removeMealNotifyTiming(mealNotifyId);
			}
			lookupTranslationDAO().removeAirport(airportCode);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	public void removeAirport(int id) throws ModuleException {
		try {
			lookupTranslationDAO().removeAirport(id);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	public List<AirportForDisplay> getAirports() throws ModuleException {
		try {			
			return lookupTranslationDAO().getAirports();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	public List<AirportForDisplay> getAirportsByLanguageCode(String languageCode) throws ModuleException {
		try {			
			return lookupTranslationDAO().getAirportsByLanguageCode(languageCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	public List<AirportForDisplay> getAirportsByAirportCode(String airportCode) throws ModuleException {
		try {
			return lookupTranslationDAO().getAirportsByAirportCode(airportCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	public Collection<AirportForDisplay> getAirport(Collection<String> airportCode, String languageCode) throws ModuleException {
		try {
			return (Collection<AirportForDisplay>) lookupTranslationDAO().getAirport(airportCode, languageCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	public Collection<CityForDisplay> getCity(Collection<String> cityCode, String languageCode) throws ModuleException {
		try {
			return (Collection<CityForDisplay>) lookupTranslationDAO().getCity(cityCode, languageCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}
	private TranslationDAO lookupTranslationDAO() {

		TranslationDAO dao = null;

		try {

			LookupService lookupService = LookupServiceFactory.getInstance();
			dao = (TranslationDAO) lookupService.getBean("isa:base://modules/airmaster?id=TranslationDAOProxy");

		} catch (Exception ex) {
			System.out.println("ERROR IN LOOKUP");
		}

		return dao;
	}

	// Currency

	public void saveOrUpdateCurrency(CurrencyForDisplay obj) throws ModuleException {
		try {
			lookupTranslationDAO().saveCurrency(obj);

		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");

		}
	}

	public void removeCurrency(String currencyCode) throws ModuleException {
		try {
			lookupTranslationDAO().removeCurrency(currencyCode);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	public void removeCurrency(int id) throws ModuleException {
		try {
			lookupTranslationDAO().removeCurrency(id);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	public List<CurrencyForDisplay> getCurrencies() throws ModuleException {
		try {
			return lookupTranslationDAO().getCurrencies();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	public List<CurrencyForDisplay> getCurrenciesByLanguageCode(String languageCode) throws ModuleException {
		try {
			return lookupTranslationDAO().getCurrenciesByLanguageCode(languageCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	public List<CurrencyForDisplay> getCurrenciesByCurrencyCode(String currencyCode) throws ModuleException {
		try {
			return lookupTranslationDAO().getCurrenciesByCurrencyCode(currencyCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	public CurrencyForDisplay getCurrency(String currencyCode, String languageCode) throws ModuleException {
		try {
			return lookupTranslationDAO().getCurrency(currencyCode, languageCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	public List<MealForDisplay> getMeals() throws ModuleException {
		try {			
			return lookupTranslationDAO().getMeals();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}

	public void saveOrUpdateMeals(Collection<MealForDisplay> obj) throws ModuleException {
		try {
			lookupTranslationDAO().saveMeals(obj);

		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");

		}
	}

	public void removeMeals(int mealId) throws ModuleException {
		try {
			lookupTranslationDAO().removeMeals(mealId);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}

	}
	
	public List<CityForDisplay> getCities() throws ModuleException {
		try {
			return lookupTranslationDAO().getCities();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	public void saveOrUpdateCities(Collection<CityForDisplay> obj) throws ModuleException {
		try {
			lookupTranslationDAO().saveCities(obj);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");

		}
	}

	public void removeCities(int cityId) throws ModuleException {
		try {
			lookupTranslationDAO().removeCities(cityId);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

}
