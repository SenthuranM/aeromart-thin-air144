/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airmaster.core.bl;

import java.util.Date;

import com.isa.thinair.airmaster.api.model.HubInfo;
import com.isa.thinair.airmaster.core.persistence.dao.HubDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Tracking;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

/*******************************************************************************
 * Description : Hub Info. Business Logic
 * 
 * @author : Nadika Dhanusha Mahatantila
 * @since : Feb 27, 2008
 ******************************************************************************/

public class HubInfoBL {

	/**
	 * This method is used to Save/Update information to the Database.
	 * 
	 * @param hubInfo
	 * @param userId
	 * @throws ModuleException
	 */
	public void saveHubInfo(HubInfo hubInfo, String userId) throws ModuleException {

		if (hubInfo.getVersion() >= 0) {
			HubInfo hubInfoNew = lookupHubDAO().getHubInfo(hubInfo);

			if (hubInfoNew == null)
				throw new ModuleException("module.hibernate.objectnotfound");

			// Set the editable values
			hubInfoNew.setMinConnectionTime(hubInfo.getMinConnectionTime());
			hubInfoNew.setMaxConnectionTime(hubInfo.getMaxConnectionTime());
			hubInfoNew.setIbCarrierCode(hubInfo.getIbCarrierCode());
			hubInfoNew.setObCarrierCode(hubInfo.getObCarrierCode());
			hubInfoNew.setStatus(hubInfo.getStatus());
			hubInfoNew.setEditable(hubInfo.getEditable());
			hubInfoNew.setVersion(hubInfo.getVersion());

			// tracking User details
			hubInfoNew = (HubInfo) setUserDetails(hubInfoNew, userId);

			// Update the record in the database.
			lookupHubDAO().saveHubInfo(hubInfoNew);

		} else {

			// tracking User details
			hubInfo = (HubInfo) setUserDetails(hubInfo, userId);

			// insert the record to the database.
			lookupHubDAO().saveHubInfo(hubInfo);
		}

	}

	/**
	 * Set user details
	 * 
	 * @param userID
	 * @param tracking
	 * @return Tracking
	 */
	public Tracking setUserDetails(Tracking tracking, String userID) {
		if (tracking.getVersion() < 0) {
			tracking.setCreatedBy(userID == null ? "" : userID);
			tracking.setCreatedDate(new Date());
		} else {
			tracking.setModifiedBy(userID == null ? "" : userID);
			tracking.setModifiedDate(new Date());
		}
		return tracking;
	}

	/**
	 * Getting the Hub DAO object
	 * 
	 * @return HubDAO
	 */
	private HubDAO lookupHubDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (HubDAO) lookupService.getBean("isa:base://modules/airmaster?id=HubDAOProxy");
	}

}
