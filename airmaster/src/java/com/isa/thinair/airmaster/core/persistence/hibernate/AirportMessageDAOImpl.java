package com.isa.thinair.airmaster.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import org.hibernate.Query;

import com.isa.thinair.airmaster.api.criteria.AirportMsgSearchCriteria;
import com.isa.thinair.airmaster.api.model.AirportMessage;
import com.isa.thinair.airmaster.api.model.AirportMsgFlight;
import com.isa.thinair.airmaster.api.model.AirportMsgOND;
import com.isa.thinair.airmaster.api.model.AirportMsgSalesChannel;
import com.isa.thinair.airmaster.core.persistence.dao.AirportMessageDAO;
import com.isa.thinair.airmaster.core.persistence.util.AirportMessageUtil;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 * @author eric
 * 
 * @isa.module.dao-impl dao-name="AirportMessageDAO"
 * 
 */
public class AirportMessageDAOImpl extends PlatformBaseHibernateDaoSupport implements AirportMessageDAO {

	@SuppressWarnings("unchecked")
	@Override
	public Page searchAirportMessages(AirportMsgSearchCriteria airportMsgSearchCriteria, int startRec, int noRecs) {
		ArrayList<Object> params = new ArrayList<Object>();
		StringBuilder sb = new StringBuilder("from AirportMessage as am where am.messageId = am.messageId ");
		if (airportMsgSearchCriteria.getAirport() != null && airportMsgSearchCriteria.getAirport().trim().length() > 0) {
			sb.append(" AND am.airport = '");
			sb.append(airportMsgSearchCriteria.getAirport());
			sb.append("'");
		}
		if (airportMsgSearchCriteria.getDepArrType() != null && airportMsgSearchCriteria.getDepArrType().trim().length() > 0) {
			sb.append(" AND am.depArrType = '");
			sb.append(airportMsgSearchCriteria.getDepArrType());
			sb.append("'");
		}
		if (airportMsgSearchCriteria.getDisplayFromDate() != null
				&& airportMsgSearchCriteria.getDisplayFromDate().trim().length() > 0) {
			sb.append(" AND am.displayFrom >= ? ");
			params.add(airportMsgSearchCriteria.getDisplayFromDateAsDateTime());
		}
		if (airportMsgSearchCriteria.getDisplayToDate() != null
				&& airportMsgSearchCriteria.getDisplayToDate().trim().length() > 0) {
			sb.append(" AND am.displayTo <= ? ");
			params.add(airportMsgSearchCriteria.getDisplayToDateAsDateTime());
		}
		if (airportMsgSearchCriteria.getValidFromDate() != null
				&& airportMsgSearchCriteria.getValidFromDate().trim().length() > 0) {
			sb.append(" AND am.validFrom >= ? ");
			params.add(airportMsgSearchCriteria.getValidFromDateAsDate());
		}
		if (airportMsgSearchCriteria.getValidToDate() != null && airportMsgSearchCriteria.getValidToDate().trim().length() > 0) {
			sb.append(" AND am.validTo <= ? ");
			params.add(airportMsgSearchCriteria.getValidToDateAsDate());
		}
		if (airportMsgSearchCriteria.getMessageContent() != null
				&& airportMsgSearchCriteria.getMessageContent().trim().length() > 0) {
			sb.append(" AND am.message LIKE '%");
			sb.append(airportMsgSearchCriteria.getMessageContent());
			sb.append("%'");
		}

		Query query = getSession().createQuery(sb.toString());
		Query queryCount = getSession().createQuery(sb.toString());
		int count = 0;
		for (Object object : params) {
			query.setParameter(count, object);
			queryCount.setParameter(count, object);
			count++;
		}
		query.setFirstResult(startRec).setMaxResults(noRecs);
		Collection<AirportMessage> airportMessages = query.list();
		// Since clob field cannot be read after the serialization, it's converted to a string field.
		AirportMessageUtil.convertClobToString(airportMessages);
		return new Page(queryCount.list().size(), startRec, startRec + noRecs, airportMessages);
	}

	@Override
	public Long saveAirportMessage(AirportMessage message) {
		if (message.getMessageId() == null) {
			return (Long) hibernateSave(message);
		} else {
			hibernateSaveOrUpdate(message);
			return message.getMessageId();
		}
	}

	@Override
	public AirportMessage getAirportMessage(Long id) {
		return (AirportMessage) get(AirportMessage.class, id);
	}

	@Override
	public void saveONDsForMessage(Set<AirportMsgOND> airportMessageOnds) {
		hibernateSaveOrUpdateAll(airportMessageOnds);

	}

	@Override
	public void saveFlightsForMessage(Set<AirportMsgFlight> airportMsgFlights) {
		hibernateSaveOrUpdateAll(airportMsgFlights);

	}

	@Override
	public void saveSalesChannelsForMessage(Set<AirportMsgSalesChannel> airportMessageSalesChannels) {
		hibernateSaveOrUpdateAll(airportMessageSalesChannels);

	}
}
