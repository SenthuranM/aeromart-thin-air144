package com.isa.thinair.airmaster.core.bl.exrateautomation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.dto.ExchangeRateInfoDTO;
import com.isa.thinair.airmaster.api.dto.external.ExRateAutoUpdateResultDTO;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.airmaster.core.audit.AuditAirMaster;
import com.isa.thinair.airmaster.core.bl.CurrencyBL;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;

public class ExRateAutomaterBL {

	private static Log log = LogFactory.getLog(ExRateAutomaterBL.class);

	private static final String EXRATE_UPDATE_RESULT_MSG_TEMPLATE = "exrate_automation_result";

	public void executeAutomateExRate(UserPrincipal userPrincipal) throws ModuleException {

		ExchangeRateSource exRateSource = AirmasterUtils.getExchangeRateSource().getExRateSource();

		if (exRateSource != null) {
			Map<String, ExchangeRateInfoDTO> ratesinfo = exRateSource.getExchangeRates();

			if (ratesinfo != null) {
				if (ratesinfo.size() > 0) {
					CurrencyBL currencyBL = new CurrencyBL();

					Collection<Currency> currencies = currencyBL.getAllCurrencies();

					List<ExRateAutoUpdateResultDTO> updateResult = currencyBL.updateExchangeRates(ratesinfo, (List<Currency>) currencies,
							userPrincipal);

					processUpdateResult(updateResult, userPrincipal);
				} else {
					if (log.isWarnEnabled()) {
						log.warn("No rates in the source");
					}
				}
			} else {
				if (log.isWarnEnabled()) {
					log.warn("No exhange rate information in the source");
				}
			}
		} else {
			if (log.isWarnEnabled()) {
				log.warn("Exchange rate source not defined");
			}
		}

	}

	private void processUpdateResult(List<ExRateAutoUpdateResultDTO> updatedInfo, UserPrincipal userPrincipal) {
		// list to store updating failed,skipped information
		ArrayList<ExRateAutoUpdateResultDTO> notUpdatedList = new ArrayList<ExRateAutoUpdateResultDTO>();

		for (ExRateAutoUpdateResultDTO result : updatedInfo) {
			// auditing for exchange rate updates
			try {
				AuditAirMaster.doAudit(result, userPrincipal);
				if (log.isDebugEnabled()) {
					log.debug("Audit ExRate Successful for " + result.getExchangeRateSourceInfo().getCurrencyCode());
				}
			} catch (ModuleException e) {
				if (log.isErrorEnabled()) {
					log.error("Audit ExRate Automation failed for " + result.getCurrency().getCurrencyCode());
				}
			}

			if (result.isUpdated() == false) {
				notUpdatedList.add(result);
			} else {
				// auditing for currencies
				try {
					AuditAirMaster.doAudit(result.getCurrency(), userPrincipal);
					if (log.isDebugEnabled()) {
						log.debug("Audit Successful for " + result.getExchangeRateSourceInfo().getCurrencyCode());
					}
				} catch (ModuleException e) {
					if (log.isErrorEnabled()) {
						log.error("Audit Currency failed for " + result.getCurrency().getCurrencyCode());
					}
				}
			}
		}

		// email sending for skipped,failed exchange rates
		// get the list configured admin users
		List<String> adminEmails = Arrays.asList(AppSysParamsUtil.getAdminEmails().split(",")); // does not do the email
																								// address validation
																								// assuming that correct
																								// emails entered

		if (adminEmails.size() > 0) {
			try {
				MessageProfile messageProfile = getMessageProfileForExRateAutomation(notUpdatedList, userPrincipal, adminEmails,
						EXRATE_UPDATE_RESULT_MSG_TEMPLATE, null);
				MessagingServiceBD messagingServiceBD = AirmasterUtils.getMessagingBD();
				messagingServiceBD.sendMessage(messageProfile);
			} catch (ModuleException e) {
				if (log.isWarnEnabled()) {
					log.warn("Exchange rate update message sending failed");
				}
			} catch (Exception e) {
				if (log.isWarnEnabled()) {
					log.warn("Exchange rate update message sending failed");
				}
			}
		} else {
			if (log.isWarnEnabled()) {
				log.warn("No admins defined to send the exchange rate automation update status");
			}
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private MessageProfile getMessageProfileForExRateAutomation(List<ExRateAutoUpdateResultDTO> notUpdatedList,
			UserPrincipal userPrincipal, List<String> adminEmails, String topicName, Collection colReservationAudit)
			throws ModuleException {

		// Email ids list
		List<UserMessaging> messageList = new ArrayList<UserMessaging>();
		for (String adminAddress : adminEmails) {
			UserMessaging user = new UserMessaging();
			user.setToAddres(adminAddress);
			messageList.add(user);
		}

		MessageProfile profile = new MessageProfile();
		profile.setUserMessagings(messageList);

		Topic topic = new Topic();
		HashMap map = new HashMap();

		map.put("resultList", notUpdatedList);
		map.put("user", userPrincipal.getUserId());
		map.put("subject", "Exchange Rate Automation Result");

		topic.setTopicName(topicName);

		topic.setTopicParams(map);

		topic.setAuditInfo(colReservationAudit);
		topic.setAttachMessageBody(true);
		profile.setTopic(topic);

		return profile;
	}

}
