package com.isa.thinair.airmaster.core.persistence.dao;

import java.util.List;
import java.util.Map;

import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.dto.AirportTransferDTO;
import com.isa.thinair.airmaster.api.model.AirportTransfer;
import com.isa.thinair.airmaster.api.model.AirportTransferTemplate;
import com.isa.thinair.airmaster.api.model.AirportTransferTemplateBC;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.commons.api.dto.Page;

public interface AirportTransferDAO {

	public AirportTransfer getAirportTransfer(String airportCode);
	
	public AirportTransfer getAirportTransfer(int id);

	Page<AirportTransfer> getAirportTransfers(int startRec, int numOfRecs, String code, String status);

	Page<AirportTransferTemplate> getAirportTransferTemplates(int startRec, int numOfRecs, AirportTransferDTO airportTransferDTO);

	void saveAirportTransfers(AirportTransfer airportTransfer);

	void saveAirportTransferTemplate(AirportTransferTemplate airportTransferTemplate);
	
	public List<AirportServiceDTO> getAvailableAirportTransfers(AirportServiceKeyTO keyTO, FlightSegmentTO fltSeg);
	
	public Map<String,Integer> getAirportTransferBookingClasses(int airportTransferTempId);
	
	void updateAirportTransferTemplate(AirportTransferTemplate airportTransferTemplate);
}
