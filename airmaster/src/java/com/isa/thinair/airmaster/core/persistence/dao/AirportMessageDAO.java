package com.isa.thinair.airmaster.core.persistence.dao;

import java.util.Set;

import com.isa.thinair.airmaster.api.criteria.AirportMsgSearchCriteria;
import com.isa.thinair.airmaster.api.model.AirportMessage;
import com.isa.thinair.airmaster.api.model.AirportMsgFlight;
import com.isa.thinair.airmaster.api.model.AirportMsgOND;
import com.isa.thinair.airmaster.api.model.AirportMsgSalesChannel;
import com.isa.thinair.commons.api.dto.Page;

/**
 * @author eric
 * 
 */
public interface AirportMessageDAO {

	Page searchAirportMessages(AirportMsgSearchCriteria airportMsgSearchCriteria, int startRec, int noRecs);

	public Long saveAirportMessage(AirportMessage airportMessage);

	public AirportMessage getAirportMessage(Long id);

	public void saveONDsForMessage(Set<AirportMsgOND> airportMessageOnds);

	public void saveFlightsForMessage(Set<AirportMsgFlight> airportMsgFlights);

	public void saveSalesChannelsForMessage(Set<AirportMsgSalesChannel> airportMessageSalesChannels);

}
