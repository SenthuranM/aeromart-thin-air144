package com.isa.thinair.airmaster.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.dto.SSRPaxSegmentDTO;
import com.isa.thinair.airmaster.api.dto.SSRSearchDTO;
import com.isa.thinair.airmaster.api.dto.SSRTemplateSearchCriteria;
import com.isa.thinair.airmaster.api.dto.SpecialServiceRequestDTO;
import com.isa.thinair.airmaster.api.model.SSR;
import com.isa.thinair.airmaster.api.model.SSRConstraints;
import com.isa.thinair.airmaster.api.model.SSRSubCategory;
import com.isa.thinair.airmaster.api.model.SSRTemplate;
import com.isa.thinair.airmaster.api.model.SSRTemplateCabinQuantity;
import com.isa.thinair.airmaster.core.audit.AuditAirMaster;
import com.isa.thinair.airmaster.core.bl.SsrBL;
import com.isa.thinair.airmaster.core.persistence.dao.SsrDAO;
import com.isa.thinair.airmaster.core.service.bd.SsrServiceDelegateImpl;
import com.isa.thinair.airmaster.core.service.bd.SsrServiceLocalDelegateImpl;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.SSRInfoDTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * @author Dhanya
 */
@Stateless
@RemoteBinding(jndiBinding = "SsrService.remote")
@LocalBinding(jndiBinding = "SsrService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class SsrServiceBean extends PlatformBaseSessionBean implements SsrServiceDelegateImpl, SsrServiceLocalDelegateImpl {

	SsrBL ssrBL;

	/**
	 * @throws ModuleException
	 */
	@Override
	public Integer saveSSR(SSR obj) throws ModuleException {

		try {
			obj = (SSR) setUserDetails(obj);
			AuditAirMaster.doAudit(obj, getUserPrincipal());

			if (obj.getSsrId() > 0) {
				SSR existSSR = lookupSsrDAO().getSSR(obj.getSsrId());
				obj.setI18nMessageKey(existSSR.getI18nMessageKey());
			}

			return lookupSsrDAO().saveSSR(obj);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}

	}

	@Override
	public void saveSSRs(List<Integer> ssrIds, boolean isIncrement) throws ModuleException {
		try {

			if (ssrIds != null && ssrIds.size() > 0) {
				lookupSsrDAO().saveSSRs(ssrIds, isIncrement);
			}

		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	public void deleteSSR(int key) throws ModuleException {
		try {
			lookupSsrDAO().removeSSR(key);
			AuditAirMaster.doAudit(String.valueOf(key), AuditAirMaster.SSR, getUserPrincipal());
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	public void deleteSSR(SSR ssr) throws ModuleException {
		try {
			lookupSsrDAO().removeSSR(ssr);
			AuditAirMaster.doAudit(ssr.getSsrCode(), AuditAirMaster.SSR, getUserPrincipal());
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	public List<SSR> getSSRs() throws ModuleException {
		try {
			return lookupSsrDAO().getSSRs();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	public SSR getSSR(int id) throws ModuleException {
		try {
			return lookupSsrDAO().getSSR(id);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	public Page getSSRs(int startRec, int noRecs, int ssrCategoryId) throws ModuleException {
		try {

			return lookupSsrDAO().getSSRs(startRec, noRecs, ssrCategoryId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	/**
	 * @throws ModuleException
	 */
	@Override
	public List<SSR> getActiveSSRs() throws ModuleException {
		try {
			return lookupSsrDAO().getActiveSSRs();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public List<SSR> getActiveSSRs(String cabinClass, String logicalCabinClass, Integer ssrCategoryId) throws ModuleException {
		try {
			return lookupSsrDAO().getActiveSSRs(cabinClass, logicalCabinClass, ssrCategoryId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	private SsrDAO lookupSsrDAO() {
		LookupService lookupService = LookupServiceFactory.getInstance();
		return (SsrDAO) lookupService.getBean("isa:base://modules/airmaster?id=SsrDAOProxy");
	}

	@Override
	public Map<Integer, List<SpecialServiceRequestDTO>> getAvailableSSRs(Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos, String salesChannel,
			boolean checkInventory, boolean isModifyAnci, boolean skipeCutoverValidation) throws ModuleException {
		return getSsrBL().getAvailableInflightService(flightSegIdWiseCos, salesChannel, checkInventory, isModifyAnci,
				skipeCutoverValidation);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<SpecialServiceRequestDTO> getActiveSSRsByFltSegIds(Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos,
			String ssrCategory, String salesChannel, boolean checkSSRInventory, String selectedLanguage, boolean isModifyAnci,
			boolean isAddEditAnci, boolean hasPrivModifySSRWithinBuffer, boolean isUserDefinedSsrOnly) throws ModuleException {
		try {
			return getSsrBL().getActiveSSRsByFltSegIds(flightSegIdWiseCos, ssrCategory, salesChannel, checkSSRInventory,
					selectedLanguage, isModifyAnci, isAddEditAnci, hasPrivModifySSRWithinBuffer, isUserDefinedSsrOnly);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public List<SSR> getSSR(String ssrCode) throws ModuleException {
		return lookupSsrDAO().getSSR(ssrCode);
	}

	@Override
	public List<SSRPaxSegmentDTO> getSSRPaxSegmentList(String pnr) {
		return lookupSsrDAO().getSSRPaxSegmentList(pnr);
	}

	@Override
	public Map<AirportServiceKeyTO, List<AirportServiceDTO>> getAvailableAirportServices(
			Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseSegMap, AppIndicatorEnum appIndicator, int ssrCategory,
			String selectedLangugae, Map<String, Integer> segmentBundledFareInclusion, boolean isAddEditAnci)
			throws ModuleException {
		return getSsrBL().getAvailableAirportServices(airportWiseSegMap, appIndicator, ssrCategory, segmentBundledFareInclusion,
				selectedLangugae, isAddEditAnci);
	}

	@Override
	public Map<String, List<AirportServiceDTO>> getAvailableAirportServices(Map<Integer, List<String>> airportFltSegMap,
			boolean skipCutoverValidation) throws ModuleException {
		return getSsrBL().getAvailableAirportServices(airportFltSegMap, skipCutoverValidation);
	}

	private SsrBL getSsrBL() {
		if (ssrBL == null) {
			ssrBL = new SsrBL();
		}

		return ssrBL;
	}

	@Override
	public void deleteApplicableAirports(int appConsId) throws ModuleException {
		try {
			lookupSsrDAO().removeApplicableAirports(appConsId);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	@Override
	public void deleteApplicableOnds(int appConsId) throws ModuleException {
		try {
			lookupSsrDAO().removeApplicableOnds(appConsId);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	@Override
	public Page getSSRs(SSRSearchDTO ssrSearchDTO, int startRec, int noRecs) throws ModuleException {
		try {
			return lookupSsrDAO().getSSRs(ssrSearchDTO, startRec, noRecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public SSRSubCategory getSSRSubCategory(int id) throws ModuleException {
		try {
			return lookupSsrDAO().getSSRSubCategory(id);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public SSRConstraints getSSRConstraint(int id) throws ModuleException {
		try {
			return lookupSsrDAO().getSSRConstraint(id);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public void saveSSRLanguageTranslation(SSRInfoDTO ssrInfoDTO) throws ModuleException {
		try {
			getSsrBL().saveSSRLanguageTranslation(ssrInfoDTO);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	@Override
	public int getSSRCount(int ssrCategory) throws ModuleException {
		try {
			return lookupSsrDAO().getSSRCount(ssrCategory);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public List<SSR> getSSRList(int startSortOrder, int endSortOrder) throws ModuleException {
		try {
			return lookupSsrDAO().getSSRList(startSortOrder, endSortOrder);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public void saveTemplate(SSRTemplate ssrTemplate) throws ModuleException {
		try {
			ssrTemplate = (SSRTemplate) setUserDetails(ssrTemplate);
			AuditAirMaster.doAudit(ssrTemplate, getUserPrincipal());
			lookupSsrDAO().saveTemplate(ssrTemplate);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public void deleteTemplate(int templateId) throws ModuleException {
		try {
			SSRTemplate ssrTemplate = lookupSsrDAO().loadSSRTemplate(templateId);
			AuditAirMaster.doAudit(ssrTemplate.getTemplateCode(), AuditAirMaster.SSR_TEMPLATE, getUserPrincipal());
			lookupSsrDAO().deleteTemplate(ssrTemplate);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public Page<SSRTemplate> getSSRTemplates(SSRTemplateSearchCriteria criteria, int startRec, int noRecs) throws ModuleException {
		try {

			Page<SSRTemplate> page = lookupSsrDAO().getSSRTemplates(criteria, startRec, noRecs);
			return getUniqueSSRTemplateData(page, startRec, noRecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	private static Page<SSRTemplate> getUniqueSSRTemplateData(Page<SSRTemplate> page, int startRec, int numofrecs) {
		Collection<SSRTemplate> colSSRTemplate = page.getPageData();
		Collection<SSRTemplate> uniqueColSSRTemplate = new ArrayList<SSRTemplate>();
		Iterator<SSRTemplate> iter = colSSRTemplate.iterator();
		Map<Integer, SSRTemplate> ssrTemplateMap = new HashMap<Integer, SSRTemplate>();
		while (iter.hasNext()) {
			SSRTemplate grdSSRTemplate = iter.next();
			if (ssrTemplateMap.get(grdSSRTemplate.getTemplateId()) == null) {
				ssrTemplateMap.put(grdSSRTemplate.getTemplateId(), grdSSRTemplate);
			} else {
				Set<SSRTemplateCabinQuantity> uniqueSSRTemplateSet = new HashSet<SSRTemplateCabinQuantity>();
				Set<SSRTemplateCabinQuantity> ssrCabinQtySet = grdSSRTemplate.getSsrTemplateCabinQtySet();
				for (SSRTemplateCabinQuantity ssrCabinQty : ssrCabinQtySet) {
					uniqueSSRTemplateSet.add(ssrCabinQty);
				}
				grdSSRTemplate.setSsrTemplateCabinQtySet(uniqueSSRTemplateSet);
				ssrTemplateMap.put(grdSSRTemplate.getTemplateId(), grdSSRTemplate);
			}
		}
		for (Entry<Integer, SSRTemplate> entry : ssrTemplateMap.entrySet()) {
			uniqueColSSRTemplate.add(entry.getValue());
		}
		return new Page<SSRTemplate>(uniqueColSSRTemplate.size(), startRec, startRec + numofrecs - 1, uniqueColSSRTemplate);
	}

	@Override
	public Integer getTemplateIdForFlightSegment(int flightSegId) throws ModuleException {
		try {
			return lookupSsrDAO().getTemplateIdForFlightSegment(flightSegId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

	@Override
	public boolean isAirportServicesAvailable(Map<AirportServiceKeyTO, FlightSegmentDTO> airportWiseSegMap, String appCode,
			int ssrCategory, String selectedLangugae, String cabinClass, String logicalCabinClass) throws ModuleException {
		return getSsrBL().isAirportServicesAvailable(airportWiseSegMap, appCode, ssrCategory, selectedLangugae, cabinClass,
				logicalCabinClass);
	}

	@Override
	public Integer getSSRCategoryFromSSrId(Integer ssrId) throws ModuleException {
		try {
			return lookupSsrDAO().getSSRCategoryFromSSrId(ssrId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}
	
	public boolean isUserDefinedSSR(String ssrCode) throws ModuleException {
		try {
			return lookupSsrDAO().isUserDefinedSSR(ssrCode);

		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
		}
	}

}
