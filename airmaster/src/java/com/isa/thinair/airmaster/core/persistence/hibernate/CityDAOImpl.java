package com.isa.thinair.airmaster.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.isa.thinair.airmaster.api.model.City;
import com.isa.thinair.airmaster.core.persistence.dao.CityDAO;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.Util;


/**
 * CityDAOImpl is the CityDAO implementatiion for hibernate
 * @isa.module.dao-impl dao-name="CityDAO"
 */
public class CityDAOImpl extends PlatformBaseHibernateDaoSupport implements CityDAO {

	@Override
	public City getCity(String cityCode) {
		City city = null;
		String hql = "from City city where city.cityCode = :cityCode ";
		Collection<City> cities = (Collection<City>) getSession().createQuery(hql).setParameter("cityCode", cityCode).list();
		if (!cities.isEmpty()) {
			city = cities.iterator().next();
		}
		return city;
	}
	
	@Override
	public List<City> getLCCUnPublishedCities() {
		List<String> unpubStatusList = new ArrayList<>(Arrays.asList(Util.LCC_TOBE_REPUBLISHED, Util.LCC_TOBE_PUBLISHED));
		String hql = "from City city where city.lccPublishStatus in (:unpubStatusList)";
		return (List<City>)getSession().createQuery(hql).setParameterList("unpubStatusList", unpubStatusList).list();			
	}

	@Override
	public void saveCity(City city) {
		city.setCityCode(city.getCityCode().toUpperCase());
		hibernateSaveOrUpdate(city);		
	}
	
	@Override
	public List<String> getCityCodes() {
		String hql = "select city.cityCode from City city";
		return (List<String>)getSession().createQuery(hql).list();			
	
	}
}
