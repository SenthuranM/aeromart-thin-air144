/**
 * 
 */
package com.isa.thinair.airmaster.core.util;

import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airmaster.api.model.RouteInfo;

/**
 * @author Navod Ediriweera
 * @since Nov 5, 2009
 */
public class RouteInfoPublishUtil {

	public static RouteInfo retriveObjectFromUnPublishedRouteInfo(String routeID, List<RouteInfo> unPublishedRouteInfoList) {
		RouteInfo foundRI = null;
		int location = 0;
		for (Iterator<RouteInfo> iterator = unPublishedRouteInfoList.iterator(); iterator.hasNext();) {
			RouteInfo ri = iterator.next();
			if (ri.getRouteId() == Long.valueOf(routeID)) {
				foundRI = ri;
				break;
			}
			location++;
		}
		if (foundRI != null) {// remove the object to increase performance
			unPublishedRouteInfoList.remove(location);
		}
		return foundRI;
	}
}
