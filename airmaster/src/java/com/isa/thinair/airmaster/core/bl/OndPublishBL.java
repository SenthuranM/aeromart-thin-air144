package com.isa.thinair.airmaster.core.bl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.AirportForDisplay;
import com.isa.thinair.airmaster.api.model.City;
import com.isa.thinair.airmaster.api.model.CityForDisplay;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.model.CurrencyForDisplay;
import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.service.LocationBD;
import com.isa.thinair.airmaster.api.service.TranslationBD;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.airschedules.api.dto.RouteInfoTO;
import com.isa.thinair.airschedules.api.dto.TransitAirport;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.dto.ondpublish.LanguageDescriptionDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndLocationDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishAirportDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishCurrencyDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishDestinationDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishOriginDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishedTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;

public class OndPublishBL {

	private static Log log = LogFactory.getLog(OndPublishBL.class);

	private final String DEFAULT_LANGUAGE = "en";
	private final String ondjsVersion = "ond.js v2.0.0";
	private final String ondjsVersionDesc = "This version of ond.js is integrated with the city based search functionality change. On this, each city ond point is flagged with city=true";
	private final int DIRECT = 0;
	private final int CONNECTION = 1;

	private ZuluLocalTimeConversionHelper timeHelper;
	private AirportBD airportBD;
	private FlightBD flightBD;
	private TranslationBD translationBD;
	private CommonMasterBD commonMasterBD;
	private GlobalConfig globalConfig;
	private LocationBD locationBD;

	public OndPublishBL(boolean isPrepareOndCombinations) {

		// We intiate bds only when ond combinations generation as javascript publish
		// doesn't want it
		if (isPrepareOndCombinations) {
			this.airportBD = AirmasterUtils.getAirportBD();
			this.flightBD = AirmasterUtils.getFlightBD();
			this.translationBD = AirmasterUtils.getTranslationBD();
			this.commonMasterBD = AirmasterUtils.getCommonMasterBD();
			this.locationBD = AirmasterUtils.getLocationBD();
			this.globalConfig = CommonsServices.getGlobalConfig();
			this.timeHelper = new ZuluLocalTimeConversionHelper(this.airportBD);
		}

	}

	public OndPublishedTO getOndListForSystem() throws ModuleException {

		OndPublishedTO publishedOnds;

		try {

			Map<OndLocationDTO, List<OndPublishDestinationDTO>> ondCombinations = new HashMap<OndLocationDTO, List<OndPublishDestinationDTO>>();
			List<Airport> activeAirportList = new ArrayList<Airport>(airportBD.getActiveAirports());

			if (activeAirportList != null && activeAirportList.size() > 0) {

				Date currentDate = new Date();
				Calendar cal = Calendar.getInstance();
				cal.setTime(currentDate);
				cal.add(Calendar.MONTH, AppSysParamsUtil.getDynamicOndLookupDuration());
				Date endDate = cal.getTime();

				Set<String> airports = new HashSet<String>();
				Set<String> cities = new HashSet<String>();

				Collection<String> activeHubCodes = new ArrayList<>(locationBD.getActiveHubCodes());

				// Generate all possible Ond combinations : for n active airports ,
				// n*(n-1)

				for (Airport originAirport : activeAirportList) {
					Date start = new Date();
					OndLocationDTO currentOriginAirport = new OndLocationDTO(originAirport.getAirportCode(), false);
					OndLocationDTO currentOriginCity = (originAirport.getCityCode() != null
							&& !originAirport.getCityCode().isEmpty())
									? new OndLocationDTO(originAirport.getCityCode(), true)
									: null;

					// Adjusted local time of origin airport

					Date adjustedStartingDate = timeHelper.getLocalDateTime(currentOriginAirport.getOndLocationCode(),
							currentDate);
					Date adjustedEndingDate = timeHelper.getLocalDateTime(currentOriginAirport.getOndLocationCode(),
							endDate);

					for (Airport destAirport : activeAirportList) {

						OndLocationDTO currentDestAirport = new OndLocationDTO(destAirport.getAirportCode(), false);
						OndLocationDTO currentDestCity = (destAirport.getCityCode() != null
								&& !destAirport.getCityCode().isEmpty())
										? new OndLocationDTO(destAirport.getCityCode(), true)
										: null;

						boolean skipOndCombination = false;
						/*
						 * Skipping bus to connecting airport OnD combination
						 */
						if (originAirport.isSurfaceSegment()
								&& originAirport.getConnectingAirport().equals(currentDestAirport)) {
							skipOndCombination = true;
						}

						/*
						 * Skipping connecting airport to bus OnD combination
						 */
						if (!originAirport.isSurfaceSegment() && destAirport.isSurfaceSegment()
								&& destAirport.getConnectingAirport().equals(currentOriginAirport)) {
							skipOndCombination = true;
						}

						if (!currentOriginAirport.equals(currentDestAirport)) {
							// Check for routes availability
							List<RouteInfoTO> availableRoutes = flightBD.getAvailableRoutes(
									currentOriginAirport.getOndLocationCode(), currentDestAirport.getOndLocationCode(),
									AvailableFlightSearchDTO.ALL_SINGLE_CONNECTED_FLIGHTS);

							if (availableRoutes != null && availableRoutes.size() > 0) {

								List<RouteInfoTO> directRoutes = new ArrayList<RouteInfoTO>();
								List<RouteInfoTO> connectionRoutes = new ArrayList<RouteInfoTO>();
								for (RouteInfoTO route : availableRoutes) {

									if (route.isDirectRoute()) {
										directRoutes.add(route);
									} else {
										connectionRoutes.add(route);
									}

								}

								// First check for direct - will contain routes with stop overs as well

								if (directRoutes.size() > 0) {

									List<FlightSegmentDTO> availableFlightSegments = flightBD
											.getMatchingFlightSegmentsForOndGeneration(
													currentOriginAirport.getOndLocationCode(),
													currentDestAirport.getOndLocationCode(), adjustedStartingDate,
													adjustedEndingDate);

									if (availableFlightSegments != null && availableFlightSegments.size() > 0) {
										if (!skipOndCombination) {
											// Valid ond combination add and check next
											// combination

											// origin_type : airport
											addOndCombination(ondCombinations, currentOriginAirport, currentDestAirport,
													currentDestCity, null, DIRECT);

											if (currentOriginCity != null
													&& currentOriginCity.getOndLocationCode() != null
													&& !currentOriginCity.getOndLocationCode().isEmpty()) {
												// origin_type : city
												addOndCombination(ondCombinations, currentOriginCity,
														currentDestAirport, currentDestCity, null, DIRECT);
											}

											airports.add(currentOriginAirport.getOndLocationCode());
											airports.add(currentDestAirport.getOndLocationCode());

											if (currentOriginCity != null
													&& currentOriginCity.getOndLocationCode() != null
													&& !currentOriginCity.getOndLocationCode().isEmpty()) {
												cities.add(currentOriginCity.getOndLocationCode());
											}

											if (currentDestCity != null && currentDestCity.getOndLocationCode() != null
													&& !currentDestCity.getOndLocationCode().isEmpty()) {
												cities.add(currentDestCity.getOndLocationCode());
											}
										}

										continue;

									}
								}

								// Then check for connections

								if (connectionRoutes.size() > 0) {

									for (RouteInfoTO conRoute : connectionRoutes) {

										if (isConnectionFlightsAvailable(currentOriginAirport.getOndLocationCode(),
												currentDestAirport.getOndLocationCode(), currentDate, endDate,
												conRoute)) {
											List<String> transitHubs = getTransitHubList(activeHubCodes, conRoute);
											if (!skipOndCombination) {
												// origin_type : airport, transit_type : airport
												addConnectionOND(ondCombinations, currentOriginAirport,
														currentDestAirport, currentDestCity, transitHubs);

												if (currentOriginCity != null
														&& !currentOriginCity.getOndLocationCode().isEmpty()) {
													// origin_type : city, transit_type : airport
													addConnectionOND(ondCombinations, currentOriginCity,
															currentDestAirport, currentDestCity, transitHubs);
												}
												airports.add(currentOriginAirport.getOndLocationCode());
												airports.add(currentDestAirport.getOndLocationCode());

												if (currentOriginCity != null
														&& currentOriginCity.getOndLocationCode() != null
														&& !currentOriginCity.getOndLocationCode().isEmpty()) {
													cities.add(currentOriginCity.getOndLocationCode());
												}
												if (currentDestCity != null) {
													cities.add(currentDestCity.getOndLocationCode());
												}
											}
										}

									}
								}

							} else {
								// no route found going for next combination
								continue;
							}

						}
					}
					Date end = new Date();
					if (log.isDebugEnabled())
						log.debug("Time spent to generate destinations for " + originAirport.getAirportCode() + " = "
							+ (end.getTime() - start.getTime()) + "ms");
				}

				// Get currencies and language descriptions for origin

				if (ondCombinations.size() > 0) {
					publishedOnds = prepareResponse(ondCombinations, airports, cities);
				} else {
					if (log.isDebugEnabled())
						log.debug("No active ond combinations found for ondpublish");
					// No ond combinations
					publishedOnds = new OndPublishedTO();
				}

			} else {

				if (log.isDebugEnabled())
					log.debug("No active airports found for ondpublish");
				// No active airports
				publishedOnds = new OndPublishedTO();

			}

		} catch (ModuleException ex) {

			log.error("Generating Ond combinations failed");
			throw ex;

		}

		return publishedOnds;

	}

	private void addConnectionOND(Map<OndLocationDTO, List<OndPublishDestinationDTO>> ondCombinations,
			OndLocationDTO origin, OndLocationDTO currentDestAirport, OndLocationDTO currentDestCity,
			List<String> transitHubs) {

		boolean destAlreadyAdded = false;
		if (ondCombinations.containsKey(origin)) {
			if (transitHubs.size() > 0) {
				for (OndPublishDestinationDTO existingDest : ondCombinations.get(origin)) {
					if (existingDest.getOndLocation().equals(currentDestAirport)
							|| existingDest.getOndLocation().equals(currentDestCity)) {
						Collection<List<String>> existingTransitHubs = existingDest.getTransitHubs();
						List<String> hubs = existingTransitHubs.iterator().next();
						if (existingTransitHubs != null && existingTransitHubs.size() > 0
								&& !hubs.isEmpty() && hubs.get(0).equals(transitHubs.get(0))) {
							break;
						}
						existingDest.getTransitHubs().add(transitHubs);
						destAlreadyAdded = true;
						break;
					}
				}
			}
			if (!destAlreadyAdded) {
				addOndCombination(ondCombinations, origin, currentDestAirport, currentDestCity, transitHubs,
						CONNECTION);
			}

		} else {
			addOndCombination(ondCombinations, origin, currentDestAirport, currentDestCity, transitHubs, CONNECTION);

		}
	}

	private void addOndCombination(Map<OndLocationDTO, List<OndPublishDestinationDTO>> ondCombinations,
			OndLocationDTO origin, OndLocationDTO currentDestAirport, OndLocationDTO currentDestCity,
			List<String> transitHubs, int routeType) {

		if (!ondCombinations.containsKey(origin)) {
			List<OndPublishDestinationDTO> destinations = new ArrayList<OndPublishDestinationDTO>();
			ondCombinations.put(origin, destinations);
		}
		// destination_type : airport
		addOndLocation(ondCombinations, origin, currentDestAirport, transitHubs, routeType);

		if (currentDestCity != null && currentDestCity.getOndLocationCode() != null
				&& !currentDestCity.getOndLocationCode().isEmpty()) {
			// destination_type : city
			addOndLocation(ondCombinations, origin, currentDestCity, transitHubs, routeType);
		}

	}

	private void addOndLocation(Map<OndLocationDTO, List<OndPublishDestinationDTO>> ondCombinations,
			OndLocationDTO origin, OndLocationDTO destination, List<String> transitHubs, int routeType) {
		if (!origin.equals(destination)) {
			OndPublishDestinationDTO destinationLocation = new OndPublishDestinationDTO();
			destinationLocation.setOndLocation(destination);
			destinationLocation.setRouteType(routeType);
			if (transitHubs != null) {
				destinationLocation.getTransitHubs().add(transitHubs);
			}
			ondCombinations.get(origin).add(destinationLocation);
		}
	}

	public void generateJavascriptForOndPublish(OndPublishedTO publishedOnds) throws ModuleException {

		List<OndPublishAirportDTO> airports = publishedOnds.getAirports();
		List<OndPublishCurrencyDTO> currencies = publishedOnds.getCurrencies();
		List<OndPublishOriginDTO> origins = publishedOnds.getOrigins();

		StringBuilder javascriptStringBuilder = new StringBuilder();

		javascriptStringBuilder.append("//" + ondjsVersion + "\n");
		javascriptStringBuilder.append("//" + ondjsVersionDesc + "\n");
		javascriptStringBuilder.append("//Last updated on:" + new Date() + "\n\n");
		javascriptStringBuilder.append("var airports = [];\n");

		int arrayIndex = 0;

		Map<OndLocationDTO, Integer> airportIdMap = new HashMap<OndLocationDTO, Integer>();
		Map<String, Integer> currencyIdMap = new HashMap<String, Integer>();

		// Sort airports according to English description

		Collections.sort(airports, new AirportComparator());

		for (OndPublishAirportDTO airport : airports) {
			airportIdMap.put(new OndLocationDTO(airport.getAirportCode(), airport.isCity()), arrayIndex);
			javascriptStringBuilder.append("airports[" + arrayIndex + "] = { code:'");
			javascriptStringBuilder.append(airport.getAirportCode() + "',");
			javascriptStringBuilder.append(" city : ");
			javascriptStringBuilder.append(airport.isCity() ? "true ," : "false ,");

			if (airport.isCity() && airport.getCityAirportCodes() != null && !airport.getCityAirportCodes().isEmpty()) {
				javascriptStringBuilder.append("cityAirportCodes:['");
				for (int i = 0; i < airport.getCityAirportCodes().size(); i++) {
					javascriptStringBuilder.append(airport.getCityAirportCodes().get(i));

					if (i != airport.getCityAirportCodes().size() - 1) {
						javascriptStringBuilder.append("','");
					} else {
						javascriptStringBuilder.append("'");
					}
				}
				javascriptStringBuilder.append("],");
			}
			
			List<LanguageDescriptionDTO> langDescriptions = airport.getLanguageDescriptions();

			for (int i = 0; i < langDescriptions.size(); i++) {
				javascriptStringBuilder.append(langDescriptions.get(i).getLanguageCode() + ":'"
						+ StringEscapeUtils.escapeJavaScript(langDescriptions.get(i).getDescription()));

				if (i != langDescriptions.size() - 1) {
					javascriptStringBuilder.append("',");
				} else {
					javascriptStringBuilder.append("'};\n");
				}
			}

			arrayIndex++;
		}

		arrayIndex = 0;
		javascriptStringBuilder.append("var currencies = [];\n");
		for (OndPublishCurrencyDTO currency : currencies) {
			currencyIdMap.put(currency.getCurrencyCode(), arrayIndex);
			javascriptStringBuilder.append("currencies[" + arrayIndex + "] = { code:'");
			javascriptStringBuilder.append(currency.getCurrencyCode() + "',");

			List<LanguageDescriptionDTO> langDescriptions = currency.getLanguageDescriptions();

			for (int i = 0; i < langDescriptions.size(); i++) {
				javascriptStringBuilder.append(langDescriptions.get(i).getLanguageCode() + ":'"
						+ StringEscapeUtils.escapeJavaScript(langDescriptions.get(i).getDescription()));

				if (i != langDescriptions.size() - 1) {
					javascriptStringBuilder.append("',");
				} else {
					javascriptStringBuilder.append("'};\n");
				}
			}
			arrayIndex++;
		}

		arrayIndex = 0;
		javascriptStringBuilder.append("var origins = [];\n");

		List<String> originsStrings = new ArrayList<String>();

		for (OndPublishOriginDTO origin : origins) {
			// Should check if the airport is only acting as destination

			StringBuilder originsStringBuilder = new StringBuilder();

			if (airportIdMap.get(origin.getOndLocation()) != null) {
				originsStringBuilder.append("origins[" + airportIdMap.get(origin.getOndLocation()) + "] = [");

				List<OndPublishDestinationDTO> destinations = origin.getDestinations();

				Collections.sort(destinations, new DestinationsComparator());

				for (int i = 0; i < destinations.size(); i++) {
					originsStringBuilder.append("[");
					originsStringBuilder.append(airportIdMap.get(destinations.get(i).getOndLocation()) + ",'");
					originsStringBuilder.append(destinations.get(i).getMktCarrierCode() + "',");
					originsStringBuilder.append(currencyIdMap.get(destinations.get(i).getCurrencyCode()) + ",'");

					for (int j = 0; j < destinations.get(i).getOperatingCarriers().size(); j++) {
						originsStringBuilder.append(destinations.get(i).getOperatingCarriers().get(j));

						if (j == (destinations.get(i).getOperatingCarriers().size() - 1)) {
							originsStringBuilder.append("',");
						} else {
							originsStringBuilder.append(",");
						}
					}

					originsStringBuilder.append(destinations.get(i).getRouteType());

					Collection<List<String>> transitHubs = destinations.get(i).getTransitHubs();
					if (transitHubs.size() > 0) {
						Iterator<List<String>> transitHubsIte = transitHubs.iterator();
						originsStringBuilder.append(", [");
						while (transitHubsIte.hasNext()) {
							originsStringBuilder.append("[");
							List<String> transitHubCodes = transitHubsIte.next();
							for (int p = 0; p < transitHubCodes.size(); p++) {
								originsStringBuilder.append("'" + transitHubCodes.get(p) + "'");
								if (p == transitHubCodes.size() - 1) {
									originsStringBuilder.append("]");
								} else {
									originsStringBuilder.append(",");
								}
							}

							if (transitHubsIte.hasNext()) {
								originsStringBuilder.append(",");
							} else {
								originsStringBuilder.append("]");
							}
						}
						originsStringBuilder.append("]");
					} else {
						originsStringBuilder.append(", [[]]]");
					}

					if (i != destinations.size() - 1) {
						originsStringBuilder.append(",");
					} else {
						originsStringBuilder.append("];\n");
					}
				}
			}

			originsStrings.add(originsStringBuilder.toString());

		}

		Collections.sort(originsStrings, new OriginStringComparator());

		for (String originString : originsStrings) {
			javascriptStringBuilder.append(originString);
		}

		writeOndListToJavascriptFile(javascriptStringBuilder.toString());

	}

	private boolean isConnectionFlightsAvailable(String origin, String destination, Date startDate, Date endDate,
			RouteInfoTO route) throws ModuleException {

		List<TransitAirport> stopOverAirports = route.getTransitAirportCodesList();
		boolean isConnectionsPossible = true;

		Date directSearchStartDate = timeHelper.getLocalDateTime(origin, startDate);
		Date directSearchEndDate = timeHelper.getLocalDateTime(origin, endDate);

		String currentOrigin = origin;
		String currentDestination = null;

		// Here we build up connections CMB/SHJ/CMN

		int noOfStopOvers = stopOverAirports.size();

		List<List<FlightSegmentDTO>> connectionSegments = new ArrayList<List<FlightSegmentDTO>>();

		for (int segmentSequence = 0; segmentSequence <= noOfStopOvers; segmentSequence++) {

			if (noOfStopOvers > 0) {
				if (segmentSequence == 0) {
					currentDestination = stopOverAirports.get(segmentSequence).getAirportCode();

				} else {
					currentOrigin = stopOverAirports.get(segmentSequence - 1).getAirportCode();
					if (segmentSequence == stopOverAirports.size()) {
						currentDestination = destination;
					} else {
						currentDestination = stopOverAirports.get(segmentSequence).getAirportCode();
					}

					try {
						String[] minMaxTransitDurations = globalConfig.getMixMaxTransitDurations(currentOrigin, null, null);

						FlightSegmentDTO preFirstSegment = connectionSegments.get(segmentSequence - 1).get(0);
						directSearchStartDate = getAddedTransitTime(preFirstSegment.getArrivalDateTime(),
								minMaxTransitDurations[0]);

						FlightSegmentDTO preLastSegment = connectionSegments.get(segmentSequence - 1)
								.get(connectionSegments.get(segmentSequence - 1).size() - 1);
						directSearchEndDate = getAddedTransitTime(preLastSegment.getArrivalDateTime(), minMaxTransitDurations[1]);
					} catch (ModuleRuntimeException ex) {
						if ("commons.data.transitdurations.notconfigured".equalsIgnoreCase(ex.getExceptionCode())) {
							log.error("Transit duration not defined ");
							return false;
						}
					}

				}
			}

			// Extract all flight segments for next six months for each connection segment
			List<FlightSegmentDTO> tempList = flightBD.getMatchingFlightSegmentsForOndGeneration(currentOrigin,
					currentDestination, directSearchStartDate, directSearchEndDate);

			if (tempList != null && tempList.size() > 0) {
				Collections.sort(tempList);
				connectionSegments.add(tempList);
			} else {
				// If at least one connection segment doesn't have results route cannot be
				// completed.
				// Hence we are aborting.
				isConnectionsPossible = false;
				break;
			}

		}

		return isConnectionsPossible;

	}

	private Date getAddedTransitTime(Date date, String timeInHhmm) {

		String hours = timeInHhmm.substring(0, timeInHhmm.indexOf(":"));
		String mins = timeInHhmm.substring(timeInHhmm.indexOf(":") + 1);

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(date);
		calander.add(GregorianCalendar.HOUR, Integer.parseInt(hours));
		calander.add(GregorianCalendar.MINUTE, Integer.parseInt(mins));

		return calander.getTime();
	}

	private List<OndPublishAirportDTO> prepareAllAirports(Set<String> airportCodes, Set<String> cities)
			throws ModuleException {

		List<OndPublishAirportDTO> ondPublishAirports = new ArrayList<OndPublishAirportDTO>();
		Map<String, String> languagesMap = AppSysParamsUtil.getIBESupportedLanguages();

		OndPublishAirportDTO ondPublishAirport = null;
		LanguageDescriptionDTO langDescription = null;

		for (String airportCode : airportCodes) {
			ondPublishAirport = new OndPublishAirportDTO();
			ondPublishAirport.setAirportCode(airportCode);
			boolean isDefaultLangDescSet = false;

			for (String lang : languagesMap.keySet()) {
				Collection<String> airportList = new ArrayList<String>();
				airportList.add(airportCode);
				Collection<AirportForDisplay> displayCollection = translationBD.getAirport(airportList, lang);
				AirportForDisplay displayAirport = null;

				if (displayCollection != null && !displayCollection.isEmpty()) {
					displayAirport = displayCollection.iterator().next();
				}

				if (displayAirport != null) {
					String airportName = StringUtil.getUnicode(displayAirport.getAirportCodeOl());
					if (!"".equals(airportName)) {
						langDescription = new LanguageDescriptionDTO();
						langDescription.setLanguageCode(lang);
						langDescription.setDescription(airportName);
						ondPublishAirport.addLanguageDescription(langDescription);
						if (DEFAULT_LANGUAGE.equalsIgnoreCase(lang)) {
							isDefaultLangDescSet = true;
						}
					}

				}
			}

			// No airport description has been added.
			if (!isDefaultLangDescSet) {
				langDescription = new LanguageDescriptionDTO();
				langDescription.setLanguageCode(DEFAULT_LANGUAGE);

				Airport airport = airportBD.getAirport(airportCode);

				if (airport != null) {
					langDescription.setDescription(airport.getAirportName());
				} else {
					langDescription.setDescription(airportCode);
				}
				ondPublishAirport.addLanguageDescription(langDescription);
			}

			ondPublishAirports.add(ondPublishAirport);
		}
		ondPublishAirports.addAll(populateCitiesToAirportList(cities));
		return ondPublishAirports;
	}

	private List<OndPublishAirportDTO> populateCitiesToAirportList(Set<String> cityCodes) throws ModuleException {
		List<OndPublishAirportDTO> ondPublishAirports = new ArrayList<OndPublishAirportDTO>();
		Map<String, String> languagesMap = AppSysParamsUtil.getIBESupportedLanguages();

		OndPublishAirportDTO ondPublishCity = null;
		LanguageDescriptionDTO langDescription = null;

		for (String cityCode : cityCodes) {
			ondPublishCity = new OndPublishAirportDTO();
			ondPublishCity.setAirportCode(cityCode);
			boolean isDefaultLangDescSet = false;

			List<String> cityAirportCodes = (List<String>) airportBD.getActiveAirportsForCity(cityCode);
			ondPublishCity.setCityAirportCodes(cityAirportCodes);
			for (String lang : languagesMap.keySet()) {
				Collection<String> cityList = new ArrayList<String>();
				cityList.add(cityCode);
				Collection<CityForDisplay> displayCollection = translationBD.getCity(cityList, lang);
				CityForDisplay displayCity = null;

				if (displayCollection != null && !displayCollection.isEmpty()) {
					displayCity = displayCollection.iterator().next();
				}

				if (displayCity != null) {
					String cityName = StringUtil.getUnicode(displayCity.getCityNameOl());
					if (!"".equals(cityName)) {
						langDescription = new LanguageDescriptionDTO();
						langDescription.setLanguageCode(lang);
						langDescription.setDescription(cityName);
						ondPublishCity.addLanguageDescription(langDescription);
						ondPublishCity.setCity(true);
						if (DEFAULT_LANGUAGE.equalsIgnoreCase(lang)) {
							isDefaultLangDescSet = true;
						}
					}

				}
			}

			// No airport description has been added.
			if (!isDefaultLangDescSet) {
				langDescription = new LanguageDescriptionDTO();
				langDescription.setLanguageCode(DEFAULT_LANGUAGE);

				City city = locationBD.getCity(cityCode);

				if (city != null) {
					langDescription.setDescription(city.getCityName() + " " + AppSysParamsUtil.getCitySearchOnDDescriptionText());
				} else {
					langDescription.setDescription(cityCode + " " + AppSysParamsUtil.getCitySearchOnDDescriptionText());
				}
				ondPublishCity.setCity(true);
				ondPublishCity.addLanguageDescription(langDescription);
			}

			ondPublishAirports.add(ondPublishCity);
		}
		return ondPublishAirports;
	}

	private List<OndPublishCurrencyDTO> prepareAllCurrencies(Set<String> currencyCodes) throws ModuleException {

		List<OndPublishCurrencyDTO> ondPublishCurrencies = new ArrayList<OndPublishCurrencyDTO>();
		Map<String, String> languagesMap = AppSysParamsUtil.getIBESupportedLanguages();

		OndPublishCurrencyDTO ondPublishCurrency = null;
		LanguageDescriptionDTO langDescription = null;
		CurrencyForDisplay displayCurrency = null;

		for (String currencyCode : currencyCodes) {
			ondPublishCurrency = new OndPublishCurrencyDTO();
			ondPublishCurrency.setCurrencyCode(currencyCode);
			boolean isDefaultLangDescSet = false;
			for (String lang : languagesMap.keySet()) {

				displayCurrency = translationBD.getCurrency(currencyCode, lang);

				if (displayCurrency != null) {
					langDescription = new LanguageDescriptionDTO();
					langDescription.setLanguageCode(lang);
					langDescription.setDescription(StringUtil.getUnicode(displayCurrency.getCurrencyNameOl()));
					ondPublishCurrency.addLanguageDescription(langDescription);

					if (DEFAULT_LANGUAGE.equalsIgnoreCase(lang)) {
						isDefaultLangDescSet = true;
					}

				}
			}
			// No currency language has been added.
			if (!isDefaultLangDescSet) {
				langDescription = new LanguageDescriptionDTO();
				langDescription.setLanguageCode(DEFAULT_LANGUAGE);

				Currency currency = commonMasterBD.getCurrency(currencyCode);

				if (currency != null) {
					langDescription.setDescription(currency.getCurrencyDescriprion());
				} else {
					langDescription.setDescription(currencyCode);
				}
				ondPublishCurrency.addLanguageDescription(langDescription);
			}

			ondPublishCurrencies.add(ondPublishCurrency);
		}

		return ondPublishCurrencies;
	}

	private OndPublishedTO prepareResponse(Map<OndLocationDTO, List<OndPublishDestinationDTO>> ondCombinations,
			Set<String> airports, Set<String> cities) throws ModuleException {

		String currencyCode = null;
		List<OndPublishDestinationDTO> destinations = null;
		OndPublishOriginDTO ondPublishOrigin = null;
		CurrencyExchangeRate exRate = null;
		String baseCurrencyCode = AppSysParamsUtil.getBaseCurrency();
		String carrierCode = AppSysParamsUtil.getCarrierCode();
		Set<String> currencies = new HashSet<String>();
		currencies.add(baseCurrencyCode);

		OndPublishedTO publishedOnds = new OndPublishedTO();

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		for (OndLocationDTO origin : ondCombinations.keySet()) {

			// Get currency for the country
			if (origin.getCity()) {
				currencyCode = commonMasterBD.getCurrencyCodeForCityCountry(origin.getOndLocationCode());
			} else {
				currencyCode = commonMasterBD.getCurrencyCodeForAirportCountry(origin.getOndLocationCode());
			}

			if (currencyCode != null) {
				// Check whether the currency has a defined ex. rate

				try {
					exRate = exchangeRateProxy.getCurrencyExchangeRate(currencyCode);
				} catch (ModuleException ex) {
					log.error("Retriving exchange rate for " + currencyCode + "failed");
					exRate = null;
				}

				if (exRate == null) {
					currencyCode = baseCurrencyCode;
				} else {
					currencies.add(currencyCode);
				}

			} else {
				currencyCode = baseCurrencyCode;
			}

			ondPublishOrigin = new OndPublishOriginDTO();
			ondPublishOrigin.setOndLocation(origin);

			destinations = ondCombinations.get(origin);
			for (OndPublishDestinationDTO destination : destinations) {
				destination.setCurrencyCode(currencyCode);
				destination.setMktCarrierCode(carrierCode);
				destination.addOperatingCarrier(carrierCode);
				ondPublishOrigin.addDestination(destination);
			}

			publishedOnds.addOrigin(ondPublishOrigin);

		}

		publishedOnds.setAirports(prepareAllAirports(airports, cities));
		publishedOnds.setCurrencies(prepareAllCurrencies(currencies));

		return publishedOnds;

	}

	private class AirportComparator implements Comparator<OndPublishAirportDTO> {

		@Override
		public int compare(OndPublishAirportDTO airport1, OndPublishAirportDTO airport2) {

			String enDescAirport1 = "";
			String enDescAirport2 = "";
			if (airport1.isCity() == airport2.isCity()) {
				for (LanguageDescriptionDTO desc : airport1.getLanguageDescriptions()) {
					if ("en".equalsIgnoreCase(desc.getLanguageCode())) {
						enDescAirport1 = desc.getDescription();
						break;
					}
				}

				for (LanguageDescriptionDTO desc : airport2.getLanguageDescriptions()) {
					if ("en".equalsIgnoreCase(desc.getLanguageCode())) {
						enDescAirport2 = desc.getDescription();
						break;
					}
				}

				return enDescAirport1.compareTo(enDescAirport2);
			} else {
				return airport1.isCity() ? -1 : 1;
			}

		}

	}

	private class DestinationsComparator implements Comparator<OndPublishDestinationDTO> {

		@Override
		public int compare(OndPublishDestinationDTO destination1, OndPublishDestinationDTO destination2) {
			if (destination1.getOndLocation().getCity().equals(destination2.getOndLocation().getCity())) {
				if (destination1.getRouteType() == destination2.getRouteType()) {
					return 0;
				} else if (destination1.getRouteType() < destination2.getRouteType()) {
					return -1;
				} else {
					return 1;
				}
			} else {
				return destination1.getOndLocation().getCity() ? -1 : 1;
			}
		}

	}

	private class OriginStringComparator implements Comparator<String> {

		@Override
		public int compare(String originString1, String originString2) {

			Integer index1 = Integer
					.parseInt(originString1.substring(originString1.indexOf("[") + 1, originString1.indexOf("]")));
			Integer index2 = Integer
					.parseInt(originString2.substring(originString2.indexOf("[") + 1, originString2.indexOf("]")));

			if (index1 > index2) {
				return 1;
			} else if (index1 < index2) {
				return -1;
			} else {
				return 0;
			}
		}

	}

	private void writeOndListToJavascriptFile(String javascriptString) throws ModuleException {

		// Write to NFS mounted path on the server
		String localFilePath = AppSysParamsUtil.getDynamicOndFilePath();

		if (AppSysParamsUtil.appendTimestampToOndJs()) {
			StringBuilder localFilePathBuilder = new StringBuilder();
			String[] fileInfo = localFilePath.split("\\.");
			if (fileInfo != null && fileInfo.length == 2) {
				localFilePathBuilder.append(fileInfo[0]);
				localFilePathBuilder.append("_")
						.append(CalendarUtil.getDateInFormattedString("yyyyMMddHHmmss", new Date()));
				localFilePathBuilder.append(".").append(fileInfo[1]);
			} else {
				localFilePathBuilder.append(localFilePath).append("_")
						.append(CalendarUtil.getDateInFormattedString("yyyyMMddHHmmss", new Date()));
			}
			localFilePath = localFilePathBuilder.toString();
		}

		File ondJavascript = new File(localFilePath);

		try {
			FileUtils.writeStringToFile(ondJavascript, javascriptString, "UTF-8");
			log.info(javascriptString);
			log.info("Ond list writing successful");

		} catch (IOException ex) {
			// File writing failed
			// Reverting back to already existing file available
			log.error("Ond list writing failed", ex);
			throw new ModuleException(ex, "airmaster.ondpublish.fileupdatefailed");

		}

	}

	private List<String> getTransitHubList(Collection<String> activeHubs, RouteInfoTO routeInfo) {

		List<String> transitHubs = new ArrayList<>();
		if (activeHubs != null && activeHubs.size() > 0) {
			for (TransitAirport transitAirport : routeInfo.getTransitAirportCodesList()) {
				if (transitAirport.getAirportCode() != null && !transitAirport.getAirportCode().isEmpty()
						&& activeHubs.contains(transitAirport.getAirportCode())) {
					transitHubs.add(transitAirport.getAirportCode());
				}
			}
		}

		return transitHubs;
	}

}
