/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:14:52
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airmaster.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;

import com.isa.thinair.airmaster.api.criteria.DashBoardMsgSearchCriteria;
import com.isa.thinair.airmaster.api.model.DashboardMessage;
import com.isa.thinair.airmaster.api.model.DashbrdMsgAgent;
import com.isa.thinair.airmaster.api.model.DashbrdMsgAgentPOS;
import com.isa.thinair.airmaster.api.model.DashbrdMsgAgentType;
import com.isa.thinair.airmaster.api.model.DashbrdMsgUser;
import com.isa.thinair.airmaster.core.persistence.dao.DashboardMessageDAO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.Util;

/**
 * @isa.module.dao-impl dao-name="DashboardMessageDAO"
 */
public class DashboardMessageDAOImpl extends PlatformBaseHibernateDaoSupport implements DashboardMessageDAO {

	@Override
	public Integer saveMessage(DashboardMessage message) {
		if (message.getMessageID() == null) {
			return (Integer) hibernateSave(message);
		} else {
			hibernateSaveOrUpdate(message);
			return message.getMessageID();
		}
	}

	@Override
	public Page searchDashboardMessages(DashBoardMsgSearchCriteria dashBoardMsgSearchCriteria, int startRec, int noRecs) {
		ArrayList<Object> params = new ArrayList<Object>();
		StringBuilder sb = new StringBuilder("from DashboardMessage as dm where dm.messageID = dm.messageID");
		if (dashBoardMsgSearchCriteria.getMessageType() != null) {
			sb.append(" AND dm.messageType = '");
			sb.append(dashBoardMsgSearchCriteria.getMessageType());
			sb.append("'");
		}
		if (dashBoardMsgSearchCriteria.getFromDate() != null) {
			sb.append(" AND dm.effectiveFrom >= ? ");
			params.add(dashBoardMsgSearchCriteria.getFromDateAsDateTime());
		}
		if (dashBoardMsgSearchCriteria.getToDate() != null) {
			sb.append(" AND dm.effectiveTo <= ? ");
			params.add(dashBoardMsgSearchCriteria.getToDateAsDateTime());
		}
		if (dashBoardMsgSearchCriteria.getMessageContent() != null) {
			sb.append(" AND dm.message LIKE '%");
			sb.append(dashBoardMsgSearchCriteria.getMessageContent());
			sb.append("%'");
		}
		if (dashBoardMsgSearchCriteria.getUser() != null) {
			sb.append(" AND dm.userList LIKE '%");
			sb.append(dashBoardMsgSearchCriteria.getUser());
			sb.append("%'");
		}
		Query query = getSession().createQuery(sb.toString());
		Query queryCount = getSession().createQuery("select count(*) ".concat(sb.toString()));		
		int count = 0;
		for (Object object : params) {
			query.setParameter(count, object);
			queryCount.setParameter(count, object);
			count++;
		}
		query.setFirstResult(startRec).setMaxResults(noRecs);
		int totalNoOfRecords = ((Long) queryCount.uniqueResult()).intValue();
		return new Page(totalNoOfRecords, startRec, startRec + noRecs, query.list());
	}

	@Override
	public DashboardMessage getDashboardMessage(Integer id) {
		return (DashboardMessage) get(DashboardMessage.class, id);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DashboardMessage> getNewDashbrdMsgIDs() {
		String hql = "FROM DashboardMessage as message WHERE message.status = :msgStatus "
				+ " AND message.newMessage= :newMessage AND message.effectiveFrom <= :fromDate "
				+ " AND message.effectiveTo >= :toDate ";

		Date date = new Date();

		Query query = getSession().createQuery(hql).setParameter("msgStatus", DashboardMessage.MSG_ACTIVE)
				.setParameter("newMessage", DashboardMessage.NEW_MESSAGE).setParameter("fromDate", date)
				.setParameter("toDate", date);
		return query.list();
	}

	@Override
	public void updateDashboardNewMessageStatus(List<Integer> msgIds, String msgStatus) {
		String hql = "UPDATE DashboardMessage SET newMessage = :msgStatus WHERE messageID in ("
				+ Util.buildIntegerInClauseContent(msgIds) + ")";

		Query qry = getSession().createQuery(hql).setParameter("msgStatus", msgStatus);
		qry.executeUpdate();
	}

	@Override
	public void saveAgentTypesForMessage(Set<DashbrdMsgAgentType> agentType) {
		hibernateSaveOrUpdateAll(agentType);
	}

	@Override
	public void saveAgentsForMessage(Set<DashbrdMsgAgent> agent) {
		hibernateSaveOrUpdateAll(agent);

	}

	@Override
	public void savePOSForMessage(Set<DashbrdMsgAgentPOS> pos) {
		hibernateSaveOrUpdateAll(pos);

	}

	@Override
	public void saveUsersForMessage(Set<DashbrdMsgUser> user) {
		hibernateSaveOrUpdateAll(user);

	}

}
