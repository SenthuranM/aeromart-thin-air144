/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airmaster.core.bl;

import java.util.Collection;
import java.util.Iterator;

import com.isa.thinair.airmaster.api.dto.MealCategoryDTO;
import com.isa.thinair.airmaster.api.model.I18nMessage;
import com.isa.thinair.airmaster.api.model.I18nMessageKey;
import com.isa.thinair.airmaster.api.model.Language;
import com.isa.thinair.airmaster.api.model.Meal;
import com.isa.thinair.airmaster.api.model.MealCategory;
import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.airmaster.core.persistence.dao.CommonMasterDAO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.util.I18nTranslationUtil;
import com.isa.thinair.commons.api.util.I18nTranslationUtil.I18nMessageCategory;
import com.isa.thinair.commons.core.util.DataConverterUtil;
import com.isa.thinair.commons.core.util.StringUtil;

public class MealBL {

	private CommonMasterDAO commonMasterDAO;

	public MealBL() {
		commonMasterDAO = AirmasterDAOUtil.getCommonMasterDAO();
	}

	public void saveOrUpdateMeal(Meal meal) throws ModuleException {
		
		Meal exist = null;		

		if (meal.getMealId() != null && meal.getMealId().intValue() > 0) {
			exist = commonMasterDAO.getMeal(meal.getMealId());
		}
		
		commonMasterDAO.saveMeal(meal);
		
		if (meal.getVersion() > 0) {
			
			if (!meal.getStatus().equals(exist.getStatus())) {
				
				AirmasterUtils.getChargeBD().updateMealTemplates(meal);
				
			}
		}
	}

	public void saveMealCatLanguageTranslation(MealCategoryDTO mealCatDTO) throws ModuleException {

		MealCategory mealCatInfo = commonMasterDAO.getMealCategory(mealCatDTO.getMealCategoryId());
		I18nMessageKey i18nMessageKey = mealCatInfo.getI18nMessageKey();

		if (i18nMessageKey == null) {
			String i18nMsgKey = I18nTranslationUtil.getI18nMealCatDescriptionKey(mealCatInfo.getMealCategoryId());

			i18nMessageKey = new I18nMessageKey();
			mealCatInfo.setI18nMessageKey(i18nMessageKey);

			i18nMessageKey.setI18nMsgKey(i18nMsgKey);
			i18nMessageKey.setMessageCategory(I18nMessageCategory.MEALCAT_DES.toString());

			if (mealCatDTO.getMealCategoryName() != null) {
				// setting English for all languages
				createTranlatedMessages(mealCatDTO, i18nMessageKey);
			} else {
				updateTranlatedMessages(mealCatDTO, i18nMessageKey);
			}
		} else {
			updateTranlatedMessages(mealCatDTO, i18nMessageKey);
		}
		commonMasterDAO.saveMealCategory(mealCatInfo);

	}

	private void createTranlatedMessages(MealCategoryDTO mealCatDTO, I18nMessageKey i18nMessageKey) throws ModuleException {
		Collection<Language> langauges;

		langauges = AirmasterUtils.getCommonMasterBD().getLanguages();
		for (Language language : langauges) {
			I18nMessage i18nMessage = new I18nMessage();
			i18nMessage.setMessageLocale(language.getLanguageCode());

			String hexString = StringUtil.convertToHex(mealCatDTO.getMealCategoryName().trim());

			String translationDescStr = mealCatDTO.getSelectedLanguageTranslations();
			if (translationDescStr != null && !translationDescStr.isEmpty()) {

				String transStrTemp = getLanguageTranslation(translationDescStr, language.getLanguageCode());

				if (transStrTemp != null) {
					hexString = StringUtil.convertToHex(transStrTemp);
				}
			}

			i18nMessage.setMessageContent(DataConverterUtil.createClob(hexString));
			i18nMessageKey.addI18nMessage(i18nMessage);
		}

	}

	private void updateTranlatedMessages(MealCategoryDTO mealCatDTO, I18nMessageKey i18nMessageKey) throws ModuleException {
		if (mealCatDTO.getSelectedLanguageTranslations() != null && !mealCatDTO.getSelectedLanguageTranslations().isEmpty()) {

			Collection<Language> langauges;
			String translationDescStr = mealCatDTO.getSelectedLanguageTranslations();
			langauges = AirmasterUtils.getCommonMasterBD().getLanguages();

			for (Language language : langauges) {
				String transStrTemp = getLanguageTranslation(translationDescStr, language.getLanguageCode());

				if (transStrTemp == null || transStrTemp.isEmpty()) {
					transStrTemp = mealCatDTO.getMealCategoryName();
				}

				String hexString = StringUtil.convertToHex(transStrTemp);

				I18nMessage i18nMessage = getMessageByLocale(i18nMessageKey, language.getLanguageCode());

				if (i18nMessage == null) {
					i18nMessage = new I18nMessage();
					i18nMessage.setMessageLocale(language.getLanguageCode());
					i18nMessage.setMessageContent(DataConverterUtil.createClob(hexString));
					i18nMessageKey.addI18nMessage(i18nMessage);
				} else {
					i18nMessage.setMessageLocale(language.getLanguageCode());
					i18nMessage.setMessageContent(DataConverterUtil.createClob(hexString));
				}

			}

		}

	}

	private String getLanguageTranslation(String translationDescStr, String lang) throws ModuleException {

		String langTraStr = null;

		if (translationDescStr != null && !translationDescStr.isEmpty()) {
			String[] splitedCodes = translationDescStr.split("\\~");

			for (int i = 0; i < splitedCodes.length; i++) {
				String[] lan_Translation = splitedCodes[i].split(",");
				String langCode = lan_Translation[1].trim();

				if (lang.equalsIgnoreCase(langCode)) {
					langTraStr = lan_Translation[2].trim();
					break;
				}
			}
		}

		return langTraStr;
	}

	private I18nMessage getMessageByLocale(I18nMessageKey i18nMessageKey, String languageCode) {

		Iterator<I18nMessage> iterator = i18nMessageKey.getI18nMessages().iterator();
		while (iterator.hasNext()) {
			I18nMessage i18nMessage = iterator.next();
			if (i18nMessage.getMessageLocale().equalsIgnoreCase(languageCode)) {
				return i18nMessage;
			}
		}
		return null;
	}

}
