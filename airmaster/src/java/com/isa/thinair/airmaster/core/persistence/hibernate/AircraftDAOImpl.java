/*
 * ==============================================================================

 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:14:52
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airmaster.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.model.FlightSeat;
import com.isa.thinair.airmaster.api.model.AirCraftModelSeats;
import com.isa.thinair.airmaster.api.model.Aircraft;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.api.model.CabinClass;
import com.isa.thinair.airmaster.api.model.SeatTemplateCOS;
import com.isa.thinair.airmaster.core.persistence.dao.AircraftDAO;
import com.isa.thinair.airpricing.api.model.SeatCharge;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

/**
 * BHive: Aircraft module
 * 
 * @author This is a autogenerated file
 * 
 * 
 *         AircraftDAOImpl is the AircrafttDAO implementatiion for hibernate
 * @isa.module.dao-impl dao-name="AircraftDAO"
 */
public class AircraftDAOImpl extends PlatformBaseHibernateDaoSupport implements AircraftDAO {

	private final Log log = LogFactory.getLog(getClass());

	private DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	/** get list of aircrafts **/
	@Override
	public List<Aircraft> getAircrafts() {

		return find("from Aircraft", Aircraft.class);

	}

	/** get aircraft for id **/
	@Override
	public Aircraft getAircraft(int id) {
		return (Aircraft) get(Aircraft.class, new Integer(id));

	}

	/** save aircraft **/
	@Override
	public void saveAircraft(Aircraft aircraft) {
		hibernateSaveOrUpdate(aircraft);
		if (log.isDebugEnabled()) {
			log.debug("Saving/Updating Aircraft ID: " + aircraft.getAircraftId());
		}
	}

	/** remove aircraft **/
	@Override
	public void removeAircraft(int id) {

		// String hql = "delete AircraftModel am where am.modelNumber=:model"
		Object aircraft = load(Aircraft.class, new Integer(id));

		delete(aircraft);

		if (log.isDebugEnabled()) {
			log.debug("Aicraft Deleted " + id);
		}
	}

	/** get Page of aircrafts **/
	@Override
	public Page getAircrafts(int startRec, int noRecs) throws CommonsDataAccessException {
		List<String> ob = new ArrayList<String>();
		try {
			ob.add("aircraftTailNumber");
			ob.add("modelNumber");
			return getPagedData(null, startRec, noRecs, Aircraft.class, ob);

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}

	}

	/** get Models **/
	@Override
	@SuppressWarnings("unchecked")
	public List<AircraftModel> getModels() {
		return find("from AircraftModel", AircraftModel.class);
	}

	/** save aircraft model **/
	@Override
	public void saveAircraftModel(AircraftModel aircraftmodel) {
		hibernateSaveOrUpdate(aircraftmodel);

	}

	@Override
	public void saveOnlyAircraftModel(AircraftModel aircraftmodel) {

		hibernateSave(aircraftmodel);

	}

	/** get aircraft model for model route id **/
	@Override
	public AircraftModel getAircraftModel(String modelrouteId) {
		return (AircraftModel) get(AircraftModel.class, modelrouteId);
	}

	/** get active aircrafts **/
	@Override
	@SuppressWarnings("unchecked")
	public List<Aircraft> getActiveAircrafts() {

		return find("from Aircraft ac where ac.status = ?", Aircraft.STATUS_ACTIVE, Aircraft.class);
	}

	/** get active aircreaft models **/
	@Override
	@SuppressWarnings("unchecked")
	public List<AircraftModel> getActiveAircraftModels() {

		return find("from AircraftModel acm where acm.status = ?", Aircraft.STATUS_ACTIVE, AircraftModel.class);
	}

	/** remove aircrefat model for model number **/
	@Override
	public void removeAircraftModel(String modelNumber) {

		// String hql = "delete AircraftModel where modelNumber = :modelnumber";

		// int i = getSession().createQuery(hql).setString("modelnumber", modelNumber).executeUpdate();

		Object o = load(AircraftModel.class, modelNumber);
		// getSession().delete(aircraftmodel);
		delete(o);
	}

	/** get page of aicraft models **/
	@Override
	public Page getAircraftModels(int startRec, int noRecs) {
		List<String> ob = new ArrayList<String>();
		try {
			ob.add("modelNumber");
			ob.add("description");
			return getPagedData(null, startRec, noRecs, AircraftModel.class, ob);

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	/** get list of cabin classes **/
	@Override
	public List<CabinClass> getCabinClasses(String model) {
		return find(
				"select cabinclass from CabinClass as cabinclass, AircraftCabinCapacity as acc where cabinclass.cabinClassCode=acc.cabinClassCode and acc.aircraftModelNumber = ?",
				model, CabinClass.class);
	}

	/** get cabin class for code **/
	@Override
	public CabinClass getCabinClass(String code) {
		return (CabinClass) get(CabinClass.class, code);
	}

	@Override
	public void removeAircraft(Aircraft aircraft) {
		delete(aircraft);

	}

	@Override
	public void removeAircraftModel(AircraftModel aircraftModel) {
		delete(aircraftModel);

	}

	/** get selected aicraft models **/
	@Override
	@SuppressWarnings("unchecked")
	public List<AircraftModel> getAircraftModels(Collection<String> aircraftModelNumbers) {
		int aircraftModelCount = aircraftModelNumbers.size();
		if (aircraftModelCount == 0) {
			return new ArrayList<AircraftModel>();
		}
		String hql = "SELECT aircraftModel FROM AircraftModel as aircraftModel "
				+ " WHERE aircraftModel.modelNumber IN (:modelNumbers) ";

		return getSession().createQuery(hql).setParameterList("modelNumbers", aircraftModelNumbers).list();
	}

	@Override
	public void saveAirCraftModelSeat(AirCraftModelSeats airCraftModelSeats) {
		hibernateSaveOrUpdate(airCraftModelSeats);

	}

	public void saveSeatLogicalCabinClass(List<Integer> seatIdList, Integer templateId,
			Collection<SeatTemplateCOS> seatTemplateCOSList) throws CommonsDataAccessException {
		if (seatIdList != null) {
			if (!isAllowUpdateLogicalCabinClass(seatIdList, templateId)) {
				throw new CommonsDataAccessException("Logical Cabin Class cannot be updated for already reserved seats.");
			}
		}
		hibernateSaveOrUpdateAll(seatTemplateCOSList);
	}

	@Override
	public boolean isAllowUpdateLogicalCabinClass(List<Integer> seatList, Integer templateId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sql = "select count(*) from sm_t_flight_am_seat fas, sm_t_am_seat_charge ct where fas.ams_charge_id ="
				+ " ct.ams_charge_id and fas.status = 'RES' and ct.amc_template_id=" + templateId + " and ct.am_seat_id in ("
				+ Util.buildIntegerInClauseContent(seatList) + ")";

		Integer count = (Integer) jdbcTemplate.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					return new Integer(rs.getInt("cnt"));
				}
				return null;

			}
		});
		if (count > 0) {
			return false;
		}
		return true;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<Integer, String> getFlightSeatData(ArrayList<Integer> seatList, Integer flightSegmentId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sql = "SELECT fas.am_seat_id,fas.status,fas.version,fas.flight_am_seat_id FROM sm_t_flight_am_seat fas WHERE ";
		if (flightSegmentId != null) {
			sql = sql + " fas.flt_seg_id = " + flightSegmentId + " AND ";
		}
		if (seatList != null) {
			sql = sql + " fas.am_seat_id IN (" + Util.buildIntegerInClauseContent(seatList) + ") ";
		}
		return (Map<Integer, String>) jdbcTemplate.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, String> seatStatus = new HashMap<Integer, String>();
				while (rs.next()) {
					seatStatus.put(rs.getInt("AM_SEAT_ID"), rs.getInt("FLIGHT_AM_SEAT_ID") + "_" + rs.getString("STATUS") + "_"
							+ rs.getInt("VERSION"));
				}
				return seatStatus;

			}
		});
	}

	public List<AirCraftModelSeats> getFlightSeatData(List<Integer> seatList) {
		int seatListCount = seatList.size();
		if (seatListCount == 0) {
			return new ArrayList<AirCraftModelSeats>();
		}
		String hql = "SELECT aircraftModelSeats FROM AirCraftModelSeats as aircraftModelSeats "
				+ " WHERE aircraftModelSeats.seatID IN (:seatIds) ";

		return getSession().createQuery(hql).setParameterList("seatIds", seatList).list();
	}

	@SuppressWarnings("unchecked")
	public Map<Integer, Integer> getSeatTemplateIds(Integer flightId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sql = "select distinct fas.flt_seg_id, sc.amc_template_id from sm_t_flight_am_seat fas, sm_t_am_seat_charge sc ";
		sql = sql
				+ " where sc.ams_charge_id = fas.ams_charge_id and fas.flt_seg_id in (select fs.flt_seg_id from t_flight_segment fs where fs.flight_id ="
				+ flightId + ")";

		return (Map<Integer, Integer>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, Integer> attachedSeatTemplateIdList = new HashMap<Integer, Integer>();
				while (rs.next()) {
					attachedSeatTemplateIdList.put(rs.getInt("FLT_SEG_ID"), rs.getInt("AMC_TEMPLATE_ID"));
				}
				return attachedSeatTemplateIdList;
			}
		});
	}

	@SuppressWarnings("unchecked")
	public Map<Integer, Integer> getFlightSegementData(Set<Integer> flightId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sql = "select fs.flt_seg_id,fsa.fccsa_id from t_fcc_alloc fa, t_fcc_seg_alloc fsa,t_flight_segment fs where fa.flight_id in ( ";
		sql = sql + Util.buildIntegerInClauseContent(flightId)
				+ ") and fsa.fcca_id = fa.fcca_id and fs.segment_code = fsa.segment_code and ";
		sql = sql + " fa.flight_id=fs.flight_id ";

		return (Map<Integer, Integer>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, Integer> attachedSeatTemplateIdList = new HashMap<Integer, Integer>();
				while (rs.next()) {
					attachedSeatTemplateIdList.put(rs.getInt("FLT_SEG_ID"), rs.getInt("FCCSA_ID"));
				}
				return attachedSeatTemplateIdList;
			}
		});
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<SeatTemplateCOS> getAttachedSeatTemplateData(int flightSegmentId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		String sql = "SELECT sa.* FROM sm_t_flight_am_seat fas,SM_T_AM_SEAT_ASSIGNMENT sa,sm_t_am_seat_charge sc, sm_t_aircraft_model_seats ams ";
		sql = sql + " where fas.ams_charge_id=sc.ams_charge_id and ams.am_seat_id = sa.am_seat_id and ";
		sql = sql + " sc.amc_template_id = sa.amc_template_id and sc.am_seat_id = sa.am_seat_id and fas.flt_seg_id= '";
		sql = sql + flightSegmentId + "' ORDER BY ams.col_id desc,ams.row_id desc";

		return (List<SeatTemplateCOS>) jdbcTemplate.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<SeatTemplateCOS> seatTemplateCOSList = new ArrayList<SeatTemplateCOS>();
				while (rs.next()) {
					SeatTemplateCOS seatTemplateCOS = new SeatTemplateCOS();
					seatTemplateCOS.setLogicalCCCode(rs.getString("LOGICAL_CABIN_CLASS_CODE"));
					seatTemplateCOS.setSeatId(rs.getInt("AM_SEAT_ID"));
					seatTemplateCOS.setStatus(rs.getString("STATUS"));
					seatTemplateCOS.setTemplateId(rs.getInt("AMC_TEMPLATE_ID"));
					seatTemplateCOSList.add(seatTemplateCOS);
				}
				return seatTemplateCOSList;
			}
		});
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<FlightSeat> getFlightSeatData(int flightSegmentId) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		StringBuffer sql = new StringBuffer();
		sql.append(
				"select fas.*, ams.seat_code from sm_t_flight_am_seat fas,sm_t_am_seat_charge sc, sm_t_aircraft_model_seats ams ")
				.append("where fas.ams_charge_id=sc.ams_charge_id and ams.am_seat_id = sc.am_seat_id ")
				.append("and fas.flt_seg_id = '").append(flightSegmentId).append("' order by col_id asc, row_id asc");

		return (List<FlightSeat>) jdbcTemplate.query(sql.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<FlightSeat> flightSeatList = new ArrayList<FlightSeat>();
				while (rs.next()) {
					FlightSeat flightSeat = new FlightSeat();
					flightSeat.setFlightAmSeatId(rs.getInt("FLIGHT_AM_SEAT_ID"));
					flightSeat.setFlightSegId(rs.getInt("FLT_SEG_ID"));
					flightSeat.setChargeId(rs.getInt("AMS_CHARGE_ID"));
					flightSeat.setStatus(rs.getString("STATUS"));
					flightSeat.setVersion(rs.getInt("VERSION"));
					flightSeat.setFlightSegInventoryId(rs.getInt("FCCSA_ID"));
					flightSeat.setTimestamp(rs.getDate("BLOCKED_TIME"));
					flightSeat.setPaxType(rs.getString("PAX_TYPE"));
					flightSeat.setSeatId(rs.getInt("AM_SEAT_ID"));
					flightSeat.setLogicalCCCode(rs.getString("LOGICAL_CABIN_CLASS_CODE"));
					flightSeat.setCreatedBy(rs.getString("CREATED_BY"));
					flightSeat.setModifiedBy(rs.getString("MODIFIED_BY"));
					flightSeat.setCreatedDate(rs.getDate("CREATED_DATE"));
					flightSeat.setModifiedDate(rs.getDate("MODIFIED_DATE"));
					flightSeat.setSeatCode(rs.getString("SEAT_CODE"));

					flightSeatList.add(flightSeat);
				}
				return flightSeatList;
			}
		});
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<SeatCharge> getAttachedSeatChargeData(int flightSegmentId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sql = "SELECT sc.* FROM sm_t_flight_am_seat fas,sm_t_am_seat_charge sc, sm_t_aircraft_model_seats ams ";
		sql = sql + " WHERE fas.ams_charge_id=sc.ams_charge_id and ams.am_seat_id = sc.am_seat_id";
		sql = sql + " AND fas.flt_seg_id = '" + flightSegmentId + "'";
		sql = sql + " ORDER BY ams.col_id desc,ams.row_id desc";

		return (List<SeatCharge>) jdbcTemplate.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<SeatCharge> seatChargeDetails = new ArrayList<SeatCharge>();
				while (rs.next()) {
					SeatCharge seatCharge = new SeatCharge();
					seatCharge.setChargeId(rs.getInt("AMS_CHARGE_ID"));
					seatCharge.setSeatId(rs.getInt("AM_SEAT_ID"));
					seatCharge.setStatus(rs.getString("STATUS"));
					seatCharge.setChargeAmount(rs.getBigDecimal("CHARGE_AMOUNT"));
					seatChargeDetails.add(seatCharge);
				}
				return seatChargeDetails;
			}
		});
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<Integer, Integer> getVersionList(Integer templateId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sql = "SELECT asa.version,asa.am_seat_id FROM sm_t_am_seat_assignment asa " + "	WHERE asa.amc_template_id = '"
				+ templateId + "'";

		return (Map<Integer, Integer>) jdbcTemplate.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, Integer> versionDetails = new HashMap<Integer, Integer>();
				while (rs.next()) {
					int seatIndex = Integer.parseInt(rs.getString("AM_SEAT_ID"));
					versionDetails.put(seatIndex, new Integer(rs.getString("VERSION")));
				}
				return versionDetails;
			}
		});
	}

	@Override
	public Collection<AirCraftModelSeats> getAirCraftModelSeats(String model) {
		return find(
				"SELECT airCraftModelSeats from AirCraftModelSeats as airCraftModelSeats WHERE airCraftModelSeats.modelNo = ?  ORDER BY airCraftModelSeats.cabinClassCode ,airCraftModelSeats.colGroupId ,airCraftModelSeats.rowGroupId ,airCraftModelSeats.rowId, airCraftModelSeats.colId  ",
				model, AirCraftModelSeats.class);
	}

	public Map<String, List<Integer>> getAirCraftModelSeatList(String modelNo) {

		Query query = getSession()
				.createQuery(
						"SELECT airCraftModelSeats.seatID, airCraftModelSeats.cabinClassCode from AirCraftModelSeats as airCraftModelSeats  where airCraftModelSeats.modelNo = :modelNo ORDER BY airCraftModelSeats.colId desc,airCraftModelSeats.rowId desc");
		query.setParameter("modelNo", modelNo);
		List<Object[]> rows = query.list();
		Map<String, List<Integer>> cabinClassSeatListMap = null;
		if (rows != null && !rows.isEmpty()) {
			cabinClassSeatListMap = new HashMap<String, List<Integer>>();
			for (Object[] row : rows) {
				String cabinClass = (String) row[1];
				Integer seatNumber = (Integer) row[0];
				if (cabinClassSeatListMap.containsKey(cabinClass)) {
					cabinClassSeatListMap.get(cabinClass).add(seatNumber);
				} else {
					cabinClassSeatListMap.put(cabinClass, new ArrayList<Integer>(Arrays.asList(seatNumber)));
				}
			}

		}
		return cabinClassSeatListMap;

	}

	@Override
	public AirCraftModelSeats getAirCraftModelSeats(int seatID) {
		return (AirCraftModelSeats) get(AirCraftModelSeats.class, new Integer(seatID));
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, String> getFlightSeatData(Set<Integer> flightIdList) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sql = "SELECT fas.ams_charge_id,fas.am_seat_id,fas.status,fas.version,fas.flight_am_seat_id FROM sm_t_flight_am_seat fas WHERE ";

		if (flightIdList != null) {
			sql = sql + " fas.flt_seg_id IN (SELECT f.flt_seg_id FROM t_flight_segment f WHERE f.flight_id IN ("
					+ Util.buildIntegerInClauseContent(flightIdList) + ")) ";
		}
		return (Map<Integer, String>) jdbcTemplate.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, String> seatDetails = new HashMap<Integer, String>();
				while (rs.next()) {
					seatDetails.put(rs.getInt("AMS_CHARGE_ID"), rs.getInt("FLIGHT_AM_SEAT_ID") + "_" + rs.getString("STATUS")
							+ "_" + rs.getInt("VERSION"));
				}
				return seatDetails;

			}
		});
	}

	@SuppressWarnings("unchecked")
	public Map<String, Integer> getHiddenSeatMap(String modelNo) {
		String sql = "SELECT cabin_class_code, count(*) as count from sm_t_aircraft_model_seats where seat_visibility='N' and model_number= '"
				+ modelNo + "' group by cabin_class_code";
		JdbcTemplate template = new JdbcTemplate(getDatasource());
		return (Map<String, Integer>) template.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, Integer> hiddenSeats = new HashMap<String, Integer>();
				while (rs.next()) {
					hiddenSeats.put(rs.getString("cabin_class_code"), rs.getInt("count"));
				}
				return hiddenSeats;
			}
		});
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<AircraftCabinCapacity> getAircraftCabinCapacity(String flightNo, Date flightDate) {

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT DISTINCT CABIN_CLASS_CODE,SEAT_CAPACITY,INFANT_CAPACITY ");
		sql.append("FROM T_AIRCRAFT_CC_CAPACITY ");
		sql.append("WHERE MODEL_NUMBER ");
		sql.append("		IN (SELECT MODEL_NUMBER FROM T_FLIGHT WHERE FLIGHT_NUMBER ='" + flightNo + "' ");
		sql.append("		AND TRUNC(DEPARTURE_DATE) = TO_DATE('" + sdf.format(flightDate) + "', 'DD-MON-YYYY') )");

		return (List<AircraftCabinCapacity>) jdbcTemplate.query(sql.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<AircraftCabinCapacity> cabinCapacityList = new ArrayList<AircraftCabinCapacity>();
				while (rs.next()) {
					AircraftCabinCapacity cabinCapacity = new AircraftCabinCapacity();

					cabinCapacity.setCabinClassCode(rs.getString("CABIN_CLASS_CODE"));
					cabinCapacity.setSeatCapacity(rs.getInt("SEAT_CAPACITY"));
					cabinCapacity.setInfantCapacity(rs.getInt("INFANT_CAPACITY"));
					cabinCapacityList.add(cabinCapacity);
				}
				return cabinCapacityList;
			}
		});
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<Integer, AirCraftModelSeats> getDestinationReservedAircraftSeats(Integer flightSegId, String modelNo) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT NAMS.*, old_seat.am_seat_id as old_sold_seat_id");
		sql.append(" FROM SM_T_AIRCRAFT_MODEL_SEATS NAMS,");
		sql.append(" (SELECT DISTINCT ams.seat_code, ams.am_seat_id ");
		sql.append("  FROM t_flight_segment fs,");
		sql.append("  t_flight f,");
		sql.append("  SM_T_AM_CHARGE_TEMPLATE amct,");
		sql.append("  SM_T_FLIGHT_AM_SEAT fas,");
		sql.append("  sm_t_pnr_pax_seg_am_seat,");
		sql.append("  SM_T_AIRCRAFT_MODEL_SEATS ams,");
		sql.append("  t_aircraft_model am");
		sql.append("  WHERE fs.flt_seg_id      = " + flightSegId);
		sql.append("  AND fas.status           = 'RES'");
		sql.append("  AND fs.flight_id         = f.flight_id");
		sql.append("  AND amct.amc_template_id = f.amc_template_id");
		sql.append("  AND fas.flt_seg_id       = fs.flt_seg_id");
		sql.append("  AND ams.am_seat_id       = fas.am_seat_id");
		sql.append(" ) old_seat");
		sql.append(" WHERE NAMS.model_number='" + modelNo + "'");
		sql.append(" AND NAMS.seat_code = old_seat.seat_code");

		return (Map<Integer, AirCraftModelSeats>) jdbcTemplate.query(sql.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, AirCraftModelSeats> airCraftModelSeats = new HashMap<>();
				while (rs.next()) {
					AirCraftModelSeats airCraftModelSeat = new AirCraftModelSeats();

					airCraftModelSeat.setCabinClassCode(rs.getString("CABIN_CLASS_CODE"));
					airCraftModelSeat.setSeatID(rs.getInt("AM_SEAT_ID"));
					airCraftModelSeat.setModelNo(rs.getString("MODEL_NUMBER"));
					airCraftModelSeat.setRowId(rs.getInt("ROW_ID"));
					airCraftModelSeat.setColId(rs.getInt("COL_ID"));
					airCraftModelSeat.setRowGroupId(rs.getInt("ROW_GRP_ID"));
					airCraftModelSeat.setColGroupId(rs.getInt("COL_GRP_ID"));
					airCraftModelSeat.setStatus(rs.getString("STATUS"));
					airCraftModelSeat.setSeatCode(rs.getString("SEAT_CODE"));
					airCraftModelSeat.setSeatType(rs.getString("SEAT_TYPE"));
					airCraftModelSeat.setSeatVisibility(rs.getString("SEAT_VISIBILITY"));
					airCraftModelSeat.setVersion(rs.getInt("VERSION"));

					airCraftModelSeats.put(rs.getInt("old_sold_seat_id"), airCraftModelSeat);
				}
				return airCraftModelSeats;
			}
		});
	}

	public List<AircraftModel> getAllActiveAircraftsByIataType(String iataType) {

		List<AircraftModel> resultsList = null;
		if (!StringUtil.isNullOrEmpty(iataType)) {
			String hql = "SELECT aircraftModel FROM AircraftModel as aircraftModel "
					+ " WHERE aircraftModel.iataAircraftType=:iataType and  aircraftModel.status=:status";

			Query hQuery = getSession().createQuery(hql);
			hQuery.setParameter("iataType", iataType);
			hQuery.setParameter("status", "ACT");

			resultsList = hQuery.list();

		}
		return resultsList;
	}

	public boolean isDefaultAircraftAssignedForIataAircraftType(String iataType, String modelNumber) {
		if (!StringUtil.isNullOrEmpty(iataType)) {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT aircraftModel FROM AircraftModel as aircraftModel ");
			sql.append("WHERE aircraftModel.iataAircraftType=:iataType and aircraftModel.modelNumber!=:modelNumber "
					+ "and aircraftModel.status=:status ");
			sql.append("  and aircraftModel.defaultIataAircraft=:defaultAircraft");

			Query hQuery = getSession().createQuery(sql.toString());
			hQuery.setParameter("iataType", iataType);
			hQuery.setParameter("modelNumber", modelNumber);
			hQuery.setParameter("status", "ACT");
			hQuery.setParameter("defaultAircraft", "Y");

			List<AircraftModel> resultsList = hQuery.list();

			if (resultsList != null && resultsList.size() > 0) {
				return true;
			}

		}

		return false;
	}
}
