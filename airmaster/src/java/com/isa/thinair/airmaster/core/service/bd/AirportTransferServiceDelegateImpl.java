package com.isa.thinair.airmaster.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airmaster.api.service.AirportTransferBD;

@Remote
public interface AirportTransferServiceDelegateImpl extends AirportTransferBD {

}
