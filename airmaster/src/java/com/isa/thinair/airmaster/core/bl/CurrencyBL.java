/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airmaster.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.dto.ExchangeRateInfoDTO;
import com.isa.thinair.airmaster.api.dto.external.ExRateAutoUpdateResultDTO;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.core.persistence.dao.CurrencyDAO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * Currency and Currency Exchange Rate manipulation.
 * 
 * @author Mohamed Nasly
 * @author Byorn
 */
public class CurrencyBL {

	private Log log = LogFactory.getLog(CurrencyBL.class);

	private CurrencyDAO currencyDAO;

	public CurrencyBL() {
		currencyDAO = AirmasterDAOUtil.getCurrencyDAO();
	}

	public void saveOrUpdateCurrency(Currency currency) throws ModuleException {
		validateSaveOrUpdateCurrency(currency);
		currencyDAO.saveCurrency(currency);
	}

	public void deleteCurrency(Currency currency) throws ModuleException {
		validateDeleteCurrency(currency.getCurrencyCode());
		currencyDAO.removeCurrency(currency);
	}

	public void deleteCurrency(String currencyCode) throws ModuleException {
		validateDeleteCurrency(currencyCode);
		currencyDAO.removeCurrency(currencyCode);
	}

	private void validateDeleteCurrency(String currencyCode) throws ModuleException {
		Currency currency = currencyDAO.getCurrency(currencyCode);

		if (AppSysParamsUtil.getBaseCurrency().equals(currencyCode)) {
			// Base currency cannot be deleted
			throw new ModuleException("airmaster.currency.delete.basecur.error");
		}

		CurrencyExchangeRate exRate = null;
		Date currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();
		for (Iterator<CurrencyExchangeRate> exRatesIt = currency.getExchangeRates().iterator(); exRatesIt.hasNext();) {
			exRate = (CurrencyExchangeRate) exRatesIt.next();
			if (exRate.getEffectiveFrom().before(currentTimestamp)) {
				// Already effective currencies cannot be deleted
				throw new ModuleException("airmaster.currency.delete.effective.error");
			}
		}
	}

	private void validateSaveOrUpdateCurrency(Currency addedOrUpdatedCurrency) throws ModuleException {
		Currency existingCurrency = null;

		if (AppSysParamsUtil.getBaseCurrency().equals(addedOrUpdatedCurrency.getCurrencyCode())) {
			// Base currency cannot be updated
			throw new ModuleException("airmaster.currency.update.basecur.error");
		}

		if (addedOrUpdatedCurrency.getExchangeRates() == null || addedOrUpdatedCurrency.getExchangeRates().size() == 0) {
			// At least one exchange rate should be defined
			throw new ModuleException("airmaster.currency.update.noexrate.error");
		}

		if (addedOrUpdatedCurrency.getVersion() >= 0) {
			existingCurrency = currencyDAO.getCurrency(addedOrUpdatedCurrency.getCurrencyCode());
		}

		CurrencyExchangeRate addOrUpdatedExRate = null;
		CurrencyExchangeRate existingExRate = null;
		Date currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();
		for (Iterator<CurrencyExchangeRate> updatedExRateIt = addedOrUpdatedCurrency.getExchangeRates().iterator(); updatedExRateIt.hasNext();) {
			addOrUpdatedExRate = (CurrencyExchangeRate) updatedExRateIt.next();

			if (addOrUpdatedExRate.getExchangeRateId() != null) { // Validate possible update
				for (Iterator<CurrencyExchangeRate> existingExRateIt = existingCurrency.getExchangeRates().iterator(); existingExRateIt.hasNext();) {
					existingExRate = (CurrencyExchangeRate) existingExRateIt.next();
					if (existingExRate.getExchangeRateId().equals(addOrUpdatedExRate.getExchangeRateId())) {
						break;
					}
				}

				if ((!existingExRate.getEffectiveFrom().equals(addOrUpdatedExRate.getEffectiveFrom())
						|| !existingExRate.getEffectiveTo().equals(addOrUpdatedExRate.getEffectiveTo())
						// || !existingExRate.getExchangeRate().equals(addOrUpdatedExRate.getExchangeRate())
						|| !existingExRate.getExrateBaseToCurNumber().equals(addOrUpdatedExRate.getExrateBaseToCurNumber())// JIRA
																															// :
																															// AARESAA-3087
				|| !existingExRate.getStatus().equals(addOrUpdatedExRate.getStatus()))
						&& !addOrUpdatedExRate.getIsModified()) {
					// An inconsistent exchange rate update
					throw new ModuleException("airmaster.currency.update.inconsistent.error");
				}

				if (addOrUpdatedExRate.getIsModified()) {
					if (existingExRate.getEffectiveTo().before(currentTimestamp)) {
						// Expired exchange rates cannot be modified
						throw new ModuleException("airmaster.currency.update.expired.error");
					}

					if (existingExRate.getEffectiveFrom().before(currentTimestamp)
							&& (!CalendarUtil.isDateTimeNumericallyEqual(existingExRate.getEffectiveFrom(),
									addOrUpdatedExRate.getEffectiveFrom())
							// || !existingExRate.getExchangeRate().equals(addOrUpdatedExRate.getExchangeRate()))){
							|| !existingExRate.getExrateBaseToCurNumber().equals(addOrUpdatedExRate.getExrateBaseToCurNumber()))) {
						// Cannot modify exchange rate and from date of already effective ex rate
						throw new ModuleException("airmaster.currency.update.effective.error");
					}

					if ((!CalendarUtil.isDateTimeNumericallyEqual(existingExRate.getEffectiveFrom(),
							addOrUpdatedExRate.getEffectiveFrom()) && addOrUpdatedExRate.getEffectiveFrom().before(
							currentTimestamp))
							|| (!CalendarUtil.isDateTimeNumericallyEqual(existingExRate.getEffectiveTo(),
									addOrUpdatedExRate.getEffectiveTo()) && addOrUpdatedExRate.getEffectiveTo().before(
									currentTimestamp))) {
						// Effective From or Effective To cannot be prior to current timestamp
						throw new ModuleException("airmaster.currency.update.before.curtime.error");
					}

					// Validations common for both add/update exchange rates
					if (addOrUpdatedExRate.getEffectiveFrom().after(addOrUpdatedExRate.getEffectiveTo())) {
						// Invalid Effective From and To combination
						throw new ModuleException("airmaster.currency.update.fromto.error");
					}

					if (isOverlaps(addOrUpdatedExRate, addedOrUpdatedCurrency.getExchangeRates())) {
						// Exchange rate effective periods cannot overlap
						throw new ModuleException("airmaster.currency.update.overlap.error");
					}
				}
			} else { // Validations for add exchange rate
				if (addOrUpdatedExRate.getEffectiveFrom().before(currentTimestamp)
						|| addOrUpdatedExRate.getEffectiveTo().before(currentTimestamp)) {
					// Effective From or Effective To cannot be prior to current timestamp
					throw new ModuleException("airmaster.currency.update.before.curtime.error");
				}

				// Validations common for both add/update exchange rates
				if (addOrUpdatedExRate.getEffectiveFrom().after(addOrUpdatedExRate.getEffectiveTo())) {
					// Invalid Effective From and To combination
					throw new ModuleException("airmaster.currency.update.fromto.error");
				}

				if (isOverlaps(addOrUpdatedExRate, addedOrUpdatedCurrency.getExchangeRates())) {
					// Exchange rate effective periods cannot overlap
					throw new ModuleException("airmaster.currency.update.overlap.error");
				}
			}
		}
	}

	private boolean isOverlaps(CurrencyExchangeRate addedOrUpdatedExRate, Set<CurrencyExchangeRate> updatedExRates) {

		boolean overlaps = false;

		CurrencyExchangeRate exRate = null;
		for (Iterator<CurrencyExchangeRate> updatedExRatesIt = updatedExRates.iterator(); updatedExRatesIt.hasNext();) {
			exRate = (CurrencyExchangeRate) updatedExRatesIt.next();

			// Exclude the exchange rate being validated
			if (addedOrUpdatedExRate.getVersion() >= 0) {
				if (addedOrUpdatedExRate.getExchangeRateId().equals(exRate.getExchangeRateId())) {
					continue;
				}
			} else {
				if (addedOrUpdatedExRate.getEffectiveFrom().equals(exRate.getEffectiveFrom())
						&& addedOrUpdatedExRate.getEffectiveTo().equals(exRate.getEffectiveTo())
						// && addedOrUpdatedExRate.getExchangeRate().equals(exRate.getExchangeRate())
						&& addedOrUpdatedExRate.getExrateBaseToCurNumber().equals(exRate.getExrateBaseToCurNumber())// JIRA
																													// :
																													// AARESAA-3087
						&& addedOrUpdatedExRate.getStatus().equals(exRate.getStatus())) {
					continue;
				}
			}

			if (!(addedOrUpdatedExRate.getEffectiveFrom().after(exRate.getEffectiveTo()) || addedOrUpdatedExRate.getEffectiveTo()
					.before(exRate.getEffectiveFrom()))) {
				overlaps = true;
				break;
			}
		}
		return overlaps;
	}

	/**
	 * Returns exchange rate of toCurrCode with respect to fromCurrCode. Returns null if fromCurrCode or toCurrCode does
	 * not have an effective exchange rate.
	 * 
	 * LOGIC:-->
	 * 
	 * ToCurrencyAmount = (FromCurrenciesBER/TOCurrenciesBER) * FromAmount
	 * 
	 * @param isPurchaseExRate
	 *            TODO
	 */
	public BigDecimal getExchangeRateValue(String fromCurrCode, String toCurrCode, Date effectiveDate, Boolean isPurchaseExRate)
			throws ModuleException {
		try {
			if (effectiveDate == null) {
				effectiveDate = new Date();
			}

			BigDecimal fromCurrencysBER = currencyDAO.getExchangeRateValue(fromCurrCode, effectiveDate, isPurchaseExRate);
			if (fromCurrencysBER == null) {
				log.warn("No effective exchange rate exists. [Currency=" + fromCurrCode + ",Date=" + effectiveDate + "]");
				return null;
			}

			if (toCurrCode.equals(AppSysParamsUtil.getBaseCurrency())) {
				return fromCurrencysBER;
			}

			BigDecimal toCurrencysBER = currencyDAO.getExchangeRateValue(toCurrCode, effectiveDate, isPurchaseExRate);
			if (toCurrencysBER == null) {
				log.warn("No effective exchange rate exists. [Currency=" + toCurrCode + ",Date=" + effectiveDate + "]");
				return null;
			}

			return CurrencyExchangeRate.getMultiplyingExchangeRate(fromCurrencysBER, toCurrencysBER);
		} catch (CommonsDataAccessException e) {
			log.error("Error in exchange rate retrieval", e);
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");
		} catch (Exception e) {
			log.error("Error in exchange rate retrieval", e);
			throw new ModuleException("airmaster.technical.error", "airmaster.desc");
		}
	}

	/**
	 * 
	 * @param effFrom
	 * @return
	 */
	public Collection<CurrencyExchangeRate> getAllUpdatedCurr(Date effFrom) {
		return currencyDAO.getAllUpdatedCurr(effFrom);
	}

	/**
	 * 
	 * @param effFrom
	 * @return
	 * @throws ModuleException
	 */
	public Collection<CurrencyExchangeRate> getAllUpdatedCurrForCharges(Date effFrom) throws ModuleException {
		return currencyDAO.getAllUpdatedCurrForCharges(effFrom);
	}

	/**
	 * 
	 * @param effFrom
	 * @return
	 * @throws ModuleException
	 */
	public Collection<CurrencyExchangeRate> getAllUpdatedCurrFares(Date effFrom) throws ModuleException {
		return currencyDAO.getAllUpdatedCurrFares(effFrom);
	}

	/**
	 * Update Currency ExRate
	 * 
	 * @param colCcurrExRate
	 * @throws ModuleException
	 */
	public void saveOrUpdateCurrencyExRate(Collection<CurrencyExchangeRate> colCcurrExRate) throws ModuleException {
		currencyDAO.saveOrUpdateCurrencyExRates(colCcurrExRate);
	}

	public Collection<Currency> getExRateAutomateEnabledCurrencies() throws ModuleException {
		return currencyDAO.getExchangeRateAutomatedCurrencies();
	}

	public Collection<Currency> getAllCurrencies() throws ModuleException {
		return currencyDAO.getCurrencies();
	}

	public List<ExRateAutoUpdateResultDTO> updateExchangeRates(Map<String, ExchangeRateInfoDTO> rateInfo, List<Currency> currencies,
			UserPrincipal userPrincipal) {

		ArrayList<ExRateAutoUpdateResultDTO> resultSummery = new ArrayList<ExRateAutoUpdateResultDTO>();

		ArrayList<Currency> currencyList = (ArrayList<Currency>) currencies;

		for (Currency currency : currencyList) {
			// for logging and auditing purpose, resultDTO is created
			ExRateAutoUpdateResultDTO resultDTO = new ExRateAutoUpdateResultDTO();

			String currencyCode = currency.getCurrencyCode();

			// check currency code is in the source info
			if (rateInfo.get(currencyCode) != null) {

				// check auto update enabled or not
				if (currency.getAutoExRateEnabled().equals(Currency.AUTOMATED_EXRATE_ENABLED)) {

					Set<CurrencyExchangeRate> exchangeRates = currency.getExchangeRates();

					if (exchangeRates != null && exchangeRates.size() > 0) {

						// selecting the appropriate rate by comparing the date range
						CurrencyExchangeRate selectedRateToUpdate = null;
						for (CurrencyExchangeRate rate : (Set<CurrencyExchangeRate>) exchangeRates) {

							Date now = CalendarUtil.getCurrentSystemTimeInZulu();
							Date fromDate = rate.getEffectiveFrom();
							Date toDate = rate.getEffectiveTo();
							if (now.after(fromDate) && now.before(toDate)) {
								selectedRateToUpdate = rate;
							}

						}

						if (selectedRateToUpdate != null) {

							BigDecimal existingExRate = selectedRateToUpdate.getExrateBaseToCurNumber();
							BigDecimal newExRate = rateInfo.get(currencyCode).getExchangeRate();

							// variance calculation
							BigDecimal variancePercentage = AccelAeroCalculator.multiply(
									AccelAeroCalculator.divide(newExRate.subtract(existingExRate), existingExRate),
									new BigDecimal(100)).abs();
							BigDecimal allowedVarianceForCurr = currency.getExRateUpdateVariance();

							if (allowedVarianceForCurr == null || variancePercentage.compareTo(allowedVarianceForCurr) <= 0) {
								resultDTO.setChangedRate((CurrencyExchangeRate) selectedRateToUpdate.clone());

								CurrencyExchangeRate newRateObj = (CurrencyExchangeRate) selectedRateToUpdate.clone();
								// getting current time
								Date currentTime = CalendarUtil.getCurrentSystemTimeInZulu();
								Date oldEffectiveToDate = CalendarUtil.addMinutes(currentTime, 10);

								int oldSecs = CalendarUtil.getSecondsFromDate(oldEffectiveToDate);
								int newSecs = 0;
								if (oldSecs < 59) {
									newSecs = 59 - oldSecs;
								}
								oldEffectiveToDate = CalendarUtil.addSeconds(oldEffectiveToDate, newSecs);
								// Old value needs to increase to 59 and new value by 1 sec
								Date newEffectiveFromDate = CalendarUtil.addSeconds(oldEffectiveToDate, 1);

								// setting dates to the old object and new object
								selectedRateToUpdate.setEffectiveTo(oldEffectiveToDate);
								newRateObj.setEffectiveFrom(newEffectiveFromDate);

								// setting the exchange rates
								newRateObj.setExrateBaseToCurNumber(newExRate);
								newRateObj.setExrateCurToBaseNumber(newExRate);
								newRateObj.setCreatedBy(userPrincipal.getUserId());
								newRateObj.setCreatedDate(currentTime);
								newRateObj.setModifiedBy(null);
								newRateObj.setModifiedDate(null);

								selectedRateToUpdate.setIsModified(true);
								// newRateObj.setIsModified(true);
								selectedRateToUpdate.setModifiedBy(userPrincipal.getUserId());
								selectedRateToUpdate.setModifiedDate(currentTime);
								// JIRA 7233
								newRateObj.setFaresUpdatedForExchangeRateChange(Currency.AUTOMATED_EXRATE_DISABLED);
								newRateObj.setChargesUpdatedForExchangeRateChange(Currency.AUTOMATED_EXRATE_DISABLED);
								newRateObj.setAnciChargesUpdatedForExchangeRateChange(Currency.AUTOMATED_EXRATE_DISABLED);

								exchangeRates.add(newRateObj);

								resultDTO.setExchangeRateSourceInfo(rateInfo.get(currencyCode));
								resultDTO.setNewRate(newRateObj);

							} else {
								// allowed variance is lesser than the variance and should be notified for manually
								// updated
								// log.info("Due to the variance could not update "+
								// selectedRateToUpdate.getCurrency().getCurrencyCode());

								resultDTO.setChangedRate(null);
								resultDTO.setErrorMessage("Rejected due to the variance. Pls Manually update.");
								resultDTO.setExchangeRateSourceInfo(rateInfo.get(currencyCode));
								resultDTO.setNewRate(null);
								resultDTO.setUpdated(false);
								resultDTO.setUpdateStatus(ExRateAutoUpdateResultDTO.RateUpdateStatus.FAILED_TO_VARIANCE);
								resultDTO.setCurrency(currency);

								resultSummery.add(resultDTO);
								continue;

							}
						} else {

							// no rate to select and need to have new record
							CurrencyExchangeRate currencyExchangeRate = new CurrencyExchangeRate();

							currencyExchangeRate.setEffectiveFrom(CalendarUtil.addMinutes(
									CalendarUtil.getCurrentSystemTimeInZulu(), 10));
							currencyExchangeRate.setEffectiveTo(CalendarUtil.getDate(9999, 12, 31));
							currencyExchangeRate.setExrateBaseToCurNumber(rateInfo.get(currencyCode).getExchangeRate());// JIRA
																														// :
																														// AARESAA-3087
							currencyExchangeRate.setExrateCurToBaseNumber(rateInfo.get(currencyCode).getExchangeRate());
							currencyExchangeRate.setStatus(currency.getStatus());
							// currencyExchangeRate.setVersion(0);
							currencyExchangeRate.setCurrency(currency);
							// currencyExchangeRate.setIsModified(true);
							currencyExchangeRate.setCreatedBy(userPrincipal.getUserId());
							currencyExchangeRate.setCreatedDate(CalendarUtil.getCurrentSystemTimeInZulu());
							// JIRA 7233
							currencyExchangeRate.setFaresUpdatedForExchangeRateChange(Currency.AUTOMATED_EXRATE_DISABLED);
							currencyExchangeRate.setChargesUpdatedForExchangeRateChange(Currency.AUTOMATED_EXRATE_DISABLED);
							currencyExchangeRate.setAnciChargesUpdatedForExchangeRateChange(Currency.AUTOMATED_EXRATE_DISABLED);
							
							exchangeRates.add(currencyExchangeRate);

							resultDTO.setChangedRate(null);
							resultDTO.setExchangeRateSourceInfo(rateInfo.get(currencyCode));
							resultDTO.setNewRate(currencyExchangeRate);
						}
					} else {
						// no rates and need to have one record
						CurrencyExchangeRate currencyExchangeRate = new CurrencyExchangeRate();

						currencyExchangeRate.setEffectiveFrom(CalendarUtil.addMinutes(CalendarUtil.getCurrentSystemTimeInZulu(),
								10));
						currencyExchangeRate.setEffectiveTo(CalendarUtil.getDate(9999, 12, 31));
						currencyExchangeRate.setExrateBaseToCurNumber(rateInfo.get(currencyCode).getExchangeRate());
						currencyExchangeRate.setExrateCurToBaseNumber(rateInfo.get(currencyCode).getExchangeRate());
						currencyExchangeRate.setStatus(currency.getStatus());
						// currencyExchangeRate.setVersion(0);
						currencyExchangeRate.setCurrency(currency);
						// currencyExchangeRate.setIsModified(true);
						currencyExchangeRate.setCreatedBy(userPrincipal.getUserId());
						currencyExchangeRate.setCreatedDate(CalendarUtil.getCurrentSystemTimeInZulu());
						// JIRA 7233
						currencyExchangeRate.setFaresUpdatedForExchangeRateChange(Currency.AUTOMATED_EXRATE_DISABLED);
						currencyExchangeRate.setChargesUpdatedForExchangeRateChange(Currency.AUTOMATED_EXRATE_DISABLED);
						currencyExchangeRate.setAnciChargesUpdatedForExchangeRateChange(Currency.AUTOMATED_EXRATE_DISABLED);
						
						if (currency.getExchangeRates() == null) {
							currency.setExchangeRates(new HashSet<CurrencyExchangeRate>());
						}
						currency.getExchangeRates().add(currencyExchangeRate);

						// setting result summery for new exchange rate insertion
						resultDTO.setChangedRate(null);
						resultDTO.setExchangeRateSourceInfo(rateInfo.get(currencyCode));
						resultDTO.setNewRate(currencyExchangeRate);
					}

					try {
						validateSaveOrUpdateCurrency(currency);
						// log.info("Successfully validated : "+currency.getCurrencyCode());
						currencyDAO.saveCurrency(currency);

						resultDTO.setErrorMessage("SUCCESS");
						resultDTO.setUpdated(true);
						resultDTO.setUpdateStatus(ExRateAutoUpdateResultDTO.RateUpdateStatus.SUCCESS);
						resultDTO.setCurrency(currency);
					} catch (ModuleException e) {
						log.error(e.getMessageString() + " " + e.getExceptionCode());

						if (e.getMessageString() != null) {
							resultDTO.setErrorMessage(e.getMessageString());
						} else {
							resultDTO.setErrorMessage("Update Failed, Please contact the administrator");
						}
						resultDTO.setUpdated(false);
						resultDTO.setUpdateStatus(ExRateAutoUpdateResultDTO.RateUpdateStatus.FAILED_TO_EXCEPTION);
						resultDTO.setCurrency(currency);
					} finally {
						resultSummery.add(resultDTO);
					}

				} else {
					// currency is not auto update enabled and need to be notified
					resultDTO.setChangedRate(null);
					resultDTO.setErrorMessage("Auto update is not enabled ");
					resultDTO.setExchangeRateSourceInfo(rateInfo.get(currencyCode));
					resultDTO.setNewRate(null);
					resultDTO.setUpdated(false);
					resultDTO.setUpdateStatus(ExRateAutoUpdateResultDTO.RateUpdateStatus.FAILED_TO_AUTOUPDATE_DISABLED);
					resultDTO.setCurrency(currency);

					resultSummery.add(resultDTO);
				}
			}
		}

		return resultSummery;
	}

}
