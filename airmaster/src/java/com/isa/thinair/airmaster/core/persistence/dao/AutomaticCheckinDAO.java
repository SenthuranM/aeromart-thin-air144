package com.isa.thinair.airmaster.core.persistence.dao;

import java.util.List;

import com.isa.thinair.airmaster.api.criteria.AutomaticCheckinTemplatesSearchCriteria;
import com.isa.thinair.airmaster.api.dto.AutomaticCheckinDTO;
import com.isa.thinair.airmaster.api.model.AutoCheckinTemplateAirport;
import com.isa.thinair.airmaster.api.model.AutomaticCheckinTemplate;
import com.isa.thinair.commons.api.dto.Page;

/**
 * @author aravinth.r
 * 
 *         AutomaticCheckinDAO is business delegate interface for the AutomaticCheckin service apis
 * 
 */
public interface AutomaticCheckinDAO {

	public AutomaticCheckinTemplate getAutomaticCheckinTemplate(int id);

	Page<AutomaticCheckinTemplate> getAutomaticCheckinTemplates(int startRec, int numOfRecs,
			AutomaticCheckinTemplatesSearchCriteria actSearchCriteria);

	void saveAutomaticCheckinTemplate(AutomaticCheckinTemplate automaticCheckinTemplate);

	public AutomaticCheckinTemplate updateAutomaticCheckinTemplate(AutomaticCheckinTemplate automaticCheckinTemplate);

	public List<AutomaticCheckinDTO> getAvailableAutomaticCheckins(String airportCode);

	public List<AutoCheckinTemplateAirport> getAutomaticCheckinAirports(int autoCheckinTempId);

	public List<String> getAutoCheckinAirportList(List<String> airportList);
}
