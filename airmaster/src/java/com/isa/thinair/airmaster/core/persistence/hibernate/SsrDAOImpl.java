package com.isa.thinair.airmaster.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.SSRPaxSegmentDTO;
import com.isa.thinair.airmaster.api.dto.SSRSearchDTO;
import com.isa.thinair.airmaster.api.dto.SSRTemplateSearchCriteria;
import com.isa.thinair.airmaster.api.dto.ServiceProviderDTO;
import com.isa.thinair.airmaster.api.dto.SpecialServiceRequestDTO;
import com.isa.thinair.airmaster.api.model.SSR;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airmaster.api.model.SSRConstraints;
import com.isa.thinair.airmaster.api.model.SSRSubCategory;
import com.isa.thinair.airmaster.api.model.SSRTemplate;
import com.isa.thinair.airmaster.core.persistence.dao.SsrDAO;
import com.isa.thinair.airmaster.core.persistence.util.SSRUtil;
import com.isa.thinair.airmaster.core.util.AncillaryModuleUtils;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.Util;

/**
 * BHive: SSR module
 * 
 * @author Dhanya
 * 
 * 
 *         SsrDAOImpl is the SsrDAO implementation for hibernate
 * @isa.module.dao-impl dao-name="SsrDAO"
 */

public class SsrDAOImpl extends PlatformBaseHibernateDaoSupport implements SsrDAO {

	private final Log log = LogFactory.getLog(SsrDAOImpl.class);

	/** get SSRs **/
	@Override
	@SuppressWarnings("unchecked")
	public List<SSR> getSSRs() {
		return find("from SSR", SSR.class);
	}

	/** get Page of SSRs **/
	@Override
	public Page getSSRs(int StartRec, int NumOfRecs, int ssrCategoryId) throws CommonsDataAccessException {

		try {
			List<String> ob = new ArrayList<String>();
			ob.add("ssrCode");

			if (ssrCategoryId == -1) {
				return getPagedData(null, StartRec, NumOfRecs, SSR.class, ob);
			} else {
				return searchCategoryBasedSSR(ssrCategoryId, StartRec, NumOfRecs);
			}

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	@SuppressWarnings("unchecked")
	private Page searchCategoryBasedSSR(int ssrCategoryId, int StartRec, int NumOfRecs) {

		log.debug("Get SSRs For Category" + ssrCategoryId);

		String hql = "select ssr from SSR as ssr" + " where ssr.ssrCatId = :ssrCategoryId";

		List<SSR> list = getSession().createQuery(hql).setParameter("ssrCategoryId", ssrCategoryId).setFirstResult(StartRec)
				.setMaxResults(NumOfRecs).list();

		String hqlCount = "select count(ssr) from SSR as ssr" + " where ssr.ssrCatId = :ssrCategoryId";
		int count = ((Long) getSession().createQuery(hqlCount).setParameter("ssrCategoryId", ssrCategoryId).uniqueResult())
				.intValue();

		return new Page(count, StartRec, StartRec + NumOfRecs - 1, list);

	}

	@Override
	@SuppressWarnings("unchecked")
	public Page getSSRs(SSRSearchDTO ssrSearchDTO, int startRec, int noRecs) {
		String s[] = constructSSRChargesHql(ssrSearchDTO);
		String hql = s[0];
		String hqlCount = s[1];

		Query qResults = getSession().createQuery(hql).setFirstResult(startRec).setMaxResults(noRecs);
		Query qCount = getSession().createQuery(hqlCount);

		int count = ((Long) qCount.uniqueResult()).intValue();
		Collection<SSR> colList = qResults.list();

		if (colList != null) {
			SSRUtil.convertClobToString(colList);
		}

		return new Page(count, startRec, startRec + noRecs - 1, colList);
	}

	private String[] constructSSRChargesHql(SSRSearchDTO ssrSearchDTO) {
		String completeHql = null;
		String completeHqlCount = null;
		Collection<String> colCriteria = new ArrayList<String>();

		String hql = " select ssr from SSR ssr ";
		String hqlCount = " select count(*) from SSR ssr ";

		String hqlWhere = " ";

		if (ssrSearchDTO != null) {
			if (ssrSearchDTO.getSsrCode() != null) {
				String ssrCode = null;
				if (ssrSearchDTO.getSsrCode().equals("ALL")) {
					ssrCode = "ssr.ssrCode like '%' ";
				} else {
					ssrCode = "ssr.ssrCode like '%" + ssrSearchDTO.getSsrCode() + "%'";
				}
				colCriteria.add(ssrCode);
			}

			if (ssrSearchDTO.getStatus() != null) {
				String stat = ssrSearchDTO.getStatus();
				String status = null;
				if (stat.equals("ALL")) {
					status = "ssr.status like '%' ";
				} else {
					status = "ssr.status ='" + ssrSearchDTO.getStatus() + "' ";
				}
				colCriteria.add(status);
			}

			if (ssrSearchDTO.getSsrCategory() != null) {
				String categoryId = ssrSearchDTO.getSsrCategory();
				String category = null;
				if (!categoryId.equals("ALL")) {
					category = "ssr.ssrSubCategory.category.catId ='" + categoryId + "'";

					colCriteria.add(category);
				}
			}

			if (ssrSearchDTO.getSsrSubCategory() != null) {
				String subCatId = ssrSearchDTO.getSsrSubCategory();
				String subCategory = null;
				if (!subCatId.equals("ALL")) {
					subCategory = "ssr.ssrSubCategory.subCatId ='" + subCatId + "'";

					colCriteria.add(subCategory);
				}
			}
		}

		hqlWhere += " WHERE ssr.ssrSubCategory.adminVisibility = 'Y' ";

		if (colCriteria.size() > 0) {
			Iterator<String> Itr = colCriteria.iterator();
			while (Itr.hasNext()) {
				hqlWhere += " AND " + Itr.next();
			}
		}

		hqlWhere += " order by ssr.sortOrder ";
		completeHql = hql + hqlWhere;
		completeHqlCount = hqlCount + hqlWhere;

		return new String[] { completeHql, completeHqlCount };
	}

	@Override
	public int getSSRCount(int ssrCategory) {
		String hqlCount = " select count(ssr.ssrId) from SSR ssr where ssr.ssrSubCategory.category.catId ='" + ssrCategory
				+ "' and ssr.ssrSubCategory.adminVisibility='Y'";
		Query qCount = getSession().createQuery(hqlCount);

		return ((Long) qCount.uniqueResult()).intValue();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<SSR> getSSRList(int startSortOrder, int endSortOrder) {
		String hql = "select ssr from SSR ssr where ssr.sortOrder >= " + startSortOrder;

		if (endSortOrder != 0) {
			hql += " AND ssr.sortOrder <= " + endSortOrder;
		}

		Query query = getSession().createQuery(hql);

		return query.list();
	}

	@Override
	public void saveTemplate(SSRTemplate ssrTemplate) throws ModuleException {
		hibernateSaveOrUpdate(ssrTemplate);
	}

	@Override
	public SSRTemplate loadSSRTemplate(int templateId) throws ModuleException {
		return (SSRTemplate) get(SSRTemplate.class, templateId);
	}

	@Override
	public void deleteTemplate(SSRTemplate ssrTemplate) throws ModuleException {
		delete(ssrTemplate);
	}

	@Override
	public Page<SSRTemplate> getSSRTemplates(SSRTemplateSearchCriteria criteria, int startRec, int noRecs) {
		String s[] = constructSSRTemplateHql(criteria);
		String hql = s[0];
		String hqlCount = s[1];

		Query qResults = getSession().createQuery(hql).setFirstResult(startRec).setMaxResults(noRecs);
		Query qCount = getSession().createQuery(hqlCount);

		int count = ((Long) qCount.uniqueResult()).intValue();
		@SuppressWarnings("unchecked")
		Collection<SSRTemplate> colList = qResults.list();
		return new Page<SSRTemplate>(count, startRec, startRec + noRecs - 1, colList);
	}

	private String[] constructSSRTemplateHql(SSRTemplateSearchCriteria criteria) {
		String completeHql = null;
		String completeHqlCount = null;
		List<String> lstCriteria = new ArrayList<String>();

		String hql = " select tmpl from SSRTemplate as tmpl ";
		String hqlCount = " select count(tmpl) from SSRTemplate as tmpl ";

		String hqlWhere = " ";

		if (criteria != null) {
			if (criteria.getTemplateId() != null && criteria.getTemplateId().intValue() != 0) {
				lstCriteria.add("tmpl.templateId = " + criteria.getTemplateId());
			}

			if (criteria.getStatus() != null && !criteria.getStatus().equals("ALL")) {
				lstCriteria.add("tmpl.status = '" + criteria.getStatus() + "'");
			}
		}

		if (lstCriteria.size() > 0) {
			hqlWhere += " WHERE ";
			Iterator<String> itr = lstCriteria.iterator();
			while (itr.hasNext()) {
				hqlWhere += itr.next();
				if (itr.hasNext()) {
					hqlWhere += " AND ";
				}
			}
		}

		hqlWhere += " ORDER BY tmpl.templateId ";
		completeHql = hql + hqlWhere;
		completeHqlCount = hqlCount + hqlWhere;

		return new String[] { completeHql, completeHqlCount };
	}

	@Override
	public Integer getTemplateIdForFlightSegment(int flightSegId) {
		String qry = "SELECT am.ssr_template_id FROM t_flight_segment fs, t_flight f, t_aircraft_model am "
				+ " WHERE fs.flight_id= f.flight_id AND f.model_number= am.model_number AND fs.flt_seg_id=?";

		Integer[] sqlParams = { flightSegId };
		JdbcTemplate jdbcTemplate = new JdbcTemplate(AncillaryModuleUtils.getDatasource());

		return jdbcTemplate.queryForObject(qry, sqlParams, Integer.class);

	}

	/** get SSR for id **/
	@Override
	public SSR getSSR(int id) {
		return (SSR) get(SSR.class, id);
	}

	/** get SSR-SUB-CATEGORY for id **/
	@Override
	public SSRSubCategory getSSRSubCategory(int id) {
		return (SSRSubCategory) get(SSRSubCategory.class, id);
	}

	/** get SSR-CATEGORY for id **/
	@Override
	public SSRConstraints getSSRConstraint(int id) {
		return (SSRConstraints) get(SSRConstraints.class, id);
	}

	/** save SSR **/
	@Override
	public Integer saveSSR(SSR ssr) {
		ssr.setSsrCode(ssr.getSsrCode().toUpperCase());
		hibernateSaveOrUpdate(ssr);
		return ssr.getSsrId();
	}

	@Override
	public void saveSSRs(List<Integer> ssrIds, boolean isIncrement) {
		if (ssrIds != null && ssrIds.size() > 0) {
			String sql = "UPDATE t_ssr_info SET ";

			if (isIncrement) {
				sql += " sort_order = sort_order + 1 ";
			} else {
				sql += " sort_order = sort_order - 1 ";
			}

			sql += " WHERE ssr_id IN (" + Util.buildIntegerInClauseContent(ssrIds) + ")";

			JdbcTemplate jdbcTemplate = new JdbcTemplate(AncillaryModuleUtils.getDatasource());
			jdbcTemplate.execute(sql);
		}
	}

	/** remove SSR **/
	@Override
	public void removeSSR(int id) {
		Object ssr = load(SSR.class, id);
		delete(ssr);
	}

	/** remove SSR **/
	@Override
	public void removeApplicableAirports(int appConsId) {
		try {

			String sql = "DELETE FROM T_SSR_APPLICABLE_AIRPORT WHERE SSR_APP_CON_ID=" + appConsId;

			JdbcTemplate jdbcTemplate = new JdbcTemplate(AncillaryModuleUtils.getDatasource());

			jdbcTemplate.update(sql);
		} catch (Exception e) {
			log.error(e);
		}

	}

	@Override
	public void removeApplicableOnds(int appConsId) {
		try {

			String sql = "DELETE FROM T_SSR_APPLICABLE_OND WHERE SSR_APP_CON_ID=" + appConsId;

			JdbcTemplate jdbcTemplate = new JdbcTemplate(AncillaryModuleUtils.getDatasource());

			jdbcTemplate.update(sql);
		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	/** get active SSRs **/
	@Override
	public List<SSR> getActiveSSRs() {

		return find("from SSR ssr1 where ssr1.status =?", SSR.STATUS_ACTIVE, SSR.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<SSR> getActiveSSRs(String cabinClass, String logicalCabinClass, Integer ssrCategoryId) {

		String qry = "select si.* from T_SSR_INFO si, T_SSR_CHARGE sc, T_SSR_SUB_CATEGORY ssc, t_ssr_charge_cos scc where "
				+ " si.ssr_id=sc.ssr_id AND si.SSR_SUB_CAT_ID=ssc.SSR_SUB_CAT_ID "
				+ " AND sc.ssr_charge_id=scc.ssr_charge_id AND ssc.ssr_cat_id=? "
				+ " AND si.status='ACT' AND ssc.admin_visibility='Y' "
				+ AirinventoryUtils.prepareClassOfServiceWhereClause(cabinClass, logicalCabinClass, "scc")
				+ " ORDER BY si.ssr_name";

		String[] sqlParams = { ssrCategoryId.toString() };

		JdbcTemplate jdbcTemplate = new JdbcTemplate(AncillaryModuleUtils.getDatasource());

		return (List<SSR>) jdbcTemplate.query(qry, sqlParams, new ResultSetExtractor() {

			@Override
			public List<SSR> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<SSR> requestDTOs = new ArrayList<SSR>();

				while (rs.next()) {
					SSR ssr = new SSR();
					ssr.setSsrId(rs.getInt("SSR_ID"));
					ssr.setSsrCode(rs.getString("SSR_CODE"));
					ssr.setSsrName(rs.getString("SSR_NAME"));
					ssr.setSsrDesc(rs.getString("SSR_DESC"));
					ssr.setStatus(rs.getString("STATUS"));
					requestDTOs.add(ssr);
				}
				return requestDTOs;
			}
		});
	}

	@Override
	public void removeSSR(SSR ssr) {
		delete(ssr);
	}

	@Override
	public List<SpecialServiceRequestDTO> getActiveSSRsByFltSegId(Integer flightSegId, String segmentCode, String ssrCategory,
			String salesChannelCode, boolean checkSSRInventory, String selectedLanguage, String cabinClass,
			String logicalCabinClass, boolean isModifyAnci, int cutoverTime, boolean skipCutoverValidation, boolean isAddEditAnci,
			boolean hasPrivModifySSRWithinBuffer, boolean isUserDefinedSsrOnly) {
		return getSSRByFltSegId(flightSegId, segmentCode, ssrCategory, salesChannelCode, checkSSRInventory, selectedLanguage,
				cabinClass, logicalCabinClass, isModifyAnci, cutoverTime, skipCutoverValidation, isAddEditAnci, isUserDefinedSsrOnly);
	}

	@Override
	public List<SpecialServiceRequestDTO> getAvailableSSRs(Integer flightSegmentId, String flightSegment, String salesChannel,
			boolean checkInventory, boolean isModifyAnci, int cutoverTime, boolean skipCutoverValidation) {
		return getSSRByFltSegId(flightSegmentId, flightSegment, SSRCategory.ID_INFLIGHT_SERVICE.toString(), salesChannel,
				checkInventory, null, null, null, isModifyAnci, cutoverTime, skipCutoverValidation, false, false);
	}

	@SuppressWarnings("unchecked")
	private List<SpecialServiceRequestDTO> getSSRByFltSegId(Integer flightSegId, String segmentCode, String ssrCategory,
			String salesChannelCode, boolean qtyValidate, String selectedLanguage, String cabinClass, String logicalCabinClass,
			boolean isModifyAnci, int cutoverTime, boolean skipCutoverValidation, boolean isAddEditAnci, boolean isUserDefinedSsrOnly) {

		if (selectedLanguage == null) {
			selectedLanguage = "en";
		}
		if (Integer.toString(SalesChannelsUtil.SALES_CHANNEL_GOQUO).equals(salesChannelCode)) {
			salesChannelCode = Integer.toString(SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES);
		}
		String query = null;
		String[] sqlParams = null;
		if (!isUserDefinedSsrOnly) {
		 query = "SELECT CMBT.SSR_ID, CMBT.SSR_CODE, CMBT.SSR_NAME, CMBT.SSR_DESC,CMBT.SSR_CUTOFF_TIME , CMBT.OND_CODE, CMBT.CABIN_CLASS_CODE, "
				+ " CMBT.ADULT_AMOUNT CHARGE_AMOUNT, CMBT.SSR_CHARGE_ID, CMBT.SELLANGTEXT, CMBT.STATUS, "
				+ preprareInventoryCaseStatment(qtyValidate)
				+ " FROM (SELECT TSI.*,TSAO.OND_CODE,STC.CABIN_CLASS_CODE, SSR_CHARGE.ADULT_AMOUNT, SSR_CHARGE.EFFECTIVE_FROM, "
				+ " SSR_CHARGE.EFFECTIVE_TO , SSR_CHARGE.SSR_CHARGE_ID, MSG.message_content as SELLANGTEXT "
				+ prepareInventoryCountQry(qtyValidate, flightSegId, cabinClass, logicalCabinClass)
				+ " FROM T_SSR_INFO TSI INNER JOIN T_SSR_SUB_CATEGORY TSSC ON TSSC.SSR_SUB_CAT_ID = TSI.SSR_SUB_CAT_ID AND "
				+ " TSSC.SSR_CAT_ID = ? ";

		if (AppSysParamsUtil.isMaintainSSRWiseCutoffTime()) {
			if (isAddEditAnci && !skipCutoverValidation) {
				query += " AND TSSC.BOOK_MOD_CUTOVER_IN_MINS < " + cutoverTime + " ";
			}
		} else {
			if (!skipCutoverValidation) {
				query += " AND TSSC.BOOK_MOD_CUTOVER_IN_MINS < " + cutoverTime + " ";
			}
		}

		query += prepareSSRInfoStatusClause(isModifyAnci) + populateUICondition(salesChannelCode)
				+ " INNER JOIN T_SSR_APPLICABILITY_CONTAINTS TSAC ON "
				+ " TSAC.SSR_ID = TSI.SSR_ID AND TSAC.APPLY_SEGMENT = 'Y' "
				+ " INNER JOIN T_SSR_APPLICABLE_OND TSAO ON TSAO.SSR_APP_CON_ID = TSAC.SSR_APP_CON_ID "
				+ " AND TSAO.OND_CODE in " + prepareStringForOrInClause(segmentCode)
				+ " INNER JOIN T_SSR_CHARGE SSR_CHARGE ON SSR_CHARGE.SSR_ID = TSI.SSR_ID AND SSR_CHARGE.STATUS = 'ACT' "
				+ " INNER JOIN T_SSR_CHARGE_COS SCC ON SSR_CHARGE.SSR_CHARGE_ID = SCC.SSR_CHARGE_ID "
				+ AirinventoryUtils.prepareClassOfServiceWhereClause(cabinClass, logicalCabinClass, "SCC")
				+ " INNER JOIN T_SSR_VISIBLE_CHANNEL TSVC ON TSVC.SSR_ID = TSI.SSR_ID AND TSVC.SALES_CHANNEL_CODE = ? "
				+ " LEFT OUTER JOIN t_i18n_message MSG ON TSI.i18n_message_key = MSG.i18n_message_key "
				+ " AND MSG.MESSAGE_LOCALE = ? "
				+ " INNER JOIN T_SSR_TEMPLATE_CABIN_QTY STC ON TSI.SSR_ID = STC.SSR_ID AND STC.STATUS= 'ACT' "
				+ AirinventoryUtils.prepareClassOfServiceWhereClause(cabinClass, logicalCabinClass, "STC")
				+ " INNER JOIN T_AIRCRAFT_MODEL AM ON AM.SSR_TEMPLATE_ID  = STC.SSR_TEMPLATE_ID "
				+ " INNER JOIN T_FLIGHT F ON F.MODEL_NUMBER= AM.MODEL_NUMBER "
				+ " INNER JOIN T_FLIGHT_SEGMENT FS ON FS.FLIGHT_ID= F.FLIGHT_ID " + " AND FS.FLT_SEG_ID =" + flightSegId
				+ ") CMBT ORDER BY CMBT.SORT_ORDER ASC NULLS LAST";

			sqlParams = new String[] { ssrCategory, salesChannelCode, selectedLanguage };
		} else {
			query = getUserDefinedSsrQueryString();
		}
		JdbcTemplate jdbcTemplate = new JdbcTemplate(AncillaryModuleUtils.getDatasource());

		List<SpecialServiceRequestDTO> ssrs = (List<SpecialServiceRequestDTO>) jdbcTemplate.query(query, sqlParams,
				new SSRDtoExtractorL(qtyValidate));

		for (SpecialServiceRequestDTO sssReq : ssrs) {
			sssReq.setOndCode(segmentCode);
		}

		return ssrs;
	}

	private String preprareInventoryCaseStatment(boolean isCheckInventory) {
		if (isCheckInventory) {
			return " (CASE WHEN AVAIL_QTY <=0 THEN 0 WHEN AVAIL_QTY IS NULL THEN -1 ELSE AVAIL_QTY END) AVAIL_QTY ";
		} else {
			return " -1 AVAIL_QTY ";
		}
	}

	private String prepareInventoryCountQry(boolean isCheckInventory, Integer fltSegIds, String cabinClass,
			String logicalCabinClass) {
		if (isCheckInventory) {
			return " , STC.MAX_QTY - (SELECT COUNT(PPSS.PPSS_ID)  FROM T_PNR_PAX_SEGMENT_SSR PPSS, T_PNR_PAX_FARE_SEGMENT PPFS, "
					+ " T_PNR_SEGMENT PS,T_BOOKING_CLASS BC WHERE PPSS.STATUS = 'CNF' AND PPSS.PPFS_ID=PPFS.PPFS_ID AND PPFS.PNR_SEG_ID=PS.PNR_SEG_ID AND PPSS.SSR_ID=TSI.SSR_ID AND"
					+ " PS.FLT_SEG_ID = " + fltSegIds + " AND PPFS.BOOKING_CODE = BC.BOOKING_CODE "
					+ AirinventoryUtils.prepareClassOfServiceWhereClause(cabinClass, logicalCabinClass, "BC") + ") AVAIL_QTY ";
		} else {
			return " ";
		}
	}

	private String prepareSSRInfoStatusClause(boolean isModifyAnci) {
		if (!isModifyAnci) {
			return " AND TSI.STATUS = 'ACT' ";
		} else {
			return " ";
		}
	}

	private String populateUICondition(String salesChannelCode) {
		if (salesChannelCode.equals("3")) {
			return " AND TSI.SHOW_IN_XBE  = 'Y' ";
		} else if (salesChannelCode.equals("4")) {
			return " AND TSI.SHOW_IN_IBE  = 'Y' ";
		} else if (salesChannelCode.equals("16")) {
			return " AND TSI.SHOW_IN_GDS  = 'Y' ";
		} else if (salesChannelCode.equals("12")) {
			return " AND TSI.SHOW_IN_WS  = 'Y' ";
		} else {
			return " ";
		}
	}

	private String prepareLogicalCabinClassWhereClause(String logicalCC, String tblAlias) {
		String whereClause = " ";

		if (logicalCC != null && !"".equals(logicalCC)) {
			whereClause = " AND " + tblAlias + ".LOGICAL_CABIN_CLASS_CODE='" + logicalCC + "' ";
		}

		return whereClause;
	}

	private String prepareCabinClassWhereClause(String cabinClass, String tblAlias) {
		String whereClause = " ";

		if (cabinClass != null && !"".equals(cabinClass)) {
			whereClause = " AND " + tblAlias + ".CABIN_CLASS_CODE='" + cabinClass + "' ";
		}

		return whereClause;
	}

	public static Set<String> getMultiLegSegList(String segmentCode) {
		Set<String> segments = new HashSet<String>();
		String ssArr[] = segmentCode.split("/");
		if (ssArr.length == 2) {
			segments.add(ssArr[0] + "/" + ssArr[1]);
		} else {
			for (int i = 0; i < ssArr.length - 2; i++) {
				String ori = ssArr[i + 0];
				String con = ssArr[i + 1];
				String des = ssArr[i + 2];

				segments.add(ori + "/" + con);
				segments.add(con + "/" + des);
			}
		}

		return segments;

	}

	private String prepareStringForOrInClause(String segmentCode) {
		StringBuilder builder = new StringBuilder("(");
		Set<String> segments = getMultiLegSegList(segmentCode);
		for (String segment : segments) {
			builder.append("'" + segment + "'");
			builder.append(",");
			String[] ondArray = segment.split("/");
			if (ondArray.length == 2) {
				String origin = ondArray[0];
				String destination = ondArray[1];

				builder.append("'");
				builder.append(origin);
				builder.append("/***'");
				builder.append(",");
				builder.append("'***/");
				builder.append(destination);
				builder.append("'");
				builder.append(",");
			}
		}
		String inClause = builder.substring(0, builder.length() - 1) + ")";
		return inClause;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<SSRPaxSegmentDTO> getSSRPaxSegmentList(String pnr) {
		String query = "select ppss.ppss_id, ppss.ppfs_id, ppss.ssr_id,  ppss.status, ps.flt_seg_id , ppf.pnr_pax_id, ppss.airport_code "
				+ " from t_pnr_pax_fare_segment ppfs, t_pnr_segment ps, t_pnr_pax_segment_ssr ppss , t_pnr_pax_fare ppf "
				+ " where ppfs.pnr_seg_id = ps.pnr_seg_id  and ppss.ppfs_id = ppfs.ppfs_id and ppf.ppf_id = ppfs.ppf_id and ppss.status='CNF' and ps.pnr = ? ";

		String[] sqlParams = { pnr };
		JdbcTemplate jdbcTemplate = new JdbcTemplate(AncillaryModuleUtils.getDatasource());

		List<SSRPaxSegmentDTO> ssrs = (List<SSRPaxSegmentDTO>) jdbcTemplate.query(query, sqlParams, new ResultSetExtractor() {

			@Override
			public List<SSRPaxSegmentDTO> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
				List<SSRPaxSegmentDTO> requestDTOs = new ArrayList<SSRPaxSegmentDTO>();

				while (resultSet.next()) {
					SSRPaxSegmentDTO serviceRequestDTO = new SSRPaxSegmentDTO();
					serviceRequestDTO.setPpfsId(resultSet.getInt("ppfs_id"));
					serviceRequestDTO.setPpssId(resultSet.getInt("ppss_id"));
					serviceRequestDTO.setSsrId(resultSet.getInt("ssr_id"));
					serviceRequestDTO.setSsrStatus(resultSet.getString("status"));
					serviceRequestDTO.setFltSegId(resultSet.getInt("flt_seg_id"));
					serviceRequestDTO.setPnrPaxId(resultSet.getInt("pnr_pax_id"));
					if (resultSet.getString("airport_code") != null) {
						serviceRequestDTO.setAirportCode(resultSet.getString("airport_code"));
					}
					requestDTOs.add(serviceRequestDTO);
				}

				return requestDTOs;
			}
		});
		return ssrs;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<SSR> getSSR(String ssrCode) {
		String query = "SELECT I.SSR_ID, I.SSR_CODE,I.SSR_DESC, I.SSR_SUB_CAT_ID, I.STATUS, " + "I.SHOW_IN_XBE, I.SHOW_IN_IBE "
				+ "FROM T_SSR_INFO I WHERE I.SSR_CODE = ?";

		String[] sqlParams = { ssrCode };

		JdbcTemplate jdbcTemplate = new JdbcTemplate(AncillaryModuleUtils.getDatasource());

		List<SSR> ssrs = (List<SSR>) jdbcTemplate.query(query, sqlParams, new ResultSetExtractor() {

			@Override
			public List<SSR> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
				List<SSR> requestDTOs = new ArrayList<SSR>();

				while (resultSet.next()) {
					SSR serviceRequestDTO = new SSR();
					serviceRequestDTO.setSsrId(resultSet.getInt("SSR_ID"));
					serviceRequestDTO.setSsrCode(resultSet.getString("SSR_CODE"));
					serviceRequestDTO.setSsrDesc(resultSet.getString("SSR_DESC"));
					serviceRequestDTO.setSsrCatId(resultSet.getInt("SSR_SUB_CAT_ID"));
					serviceRequestDTO.setStatus(resultSet.getString("STATUS"));
					requestDTOs.add(serviceRequestDTO);
				}

				return requestDTOs;
			}
		});

		return ssrs;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<AirportServiceDTO> getAvailableAirportServices(String airport, String airportType, AppIndicatorEnum appIndicator,
			int cutoverTime, int ssrCategory, String selectedLanguage, String cabinClass, String logicalCabinClass,
			Integer bundledServicePeriodId, Set<Integer> freeApsIds, boolean isAddEditAnci) {

		final boolean isAirportTransfer = ssrCategory == SSRCategory.ID_AIRPORT_TRANSFER.intValue();
		final Set<Integer> freeAirportServiceIds = freeApsIds;

		StringBuilder sb = new StringBuilder();
		sb.append("   SELECT si.ssr_id            , ");
		sb.append("   sch.ssr_charge_id           ,  ");
		sb.append("   si.ssr_code                 ,  ");
		sb.append("   si.ssr_name                 ,  ");
		sb.append("   si.ssr_desc                 ,  ");
		sb.append("   msg.message_content         ,  ");
		sb.append("   si.status                   ,  ");
		sb.append("   si.sort_order               ,  ");
		sb.append("   sch.airport_code            ,  ");
		sb.append("   sch.applicablity_type       ,  ");
		sb.append("   sch.adult_amount            ,  ");
		sb.append("   sch.child_amount            ,  ");
		sb.append("   sch.infant_amount           ,  ");
		sb.append("   sch.reservation_amount         ");
		if (isAirportTransfer) {
			sb.append(" , apt.service_provider_name   ,  ");
			sb.append("   apt.service_provider_address,  ");
			sb.append("   apt.service_provider_number ,  ");
			sb.append("   apt.airport_tranfer_id	  ,  ");
			sb.append("   apt.service_provider_email     ");
		}
		sb.append("    FROM t_ssr_category sc  ");
		sb.append(" INNER JOIN t_ssr_sub_category ssc  ");
		sb.append("      ON sc.ssr_cat_id          = ssc.ssr_cat_id  ");
		sb.append(" AND sc.status                  ='ACT'  ");

		if (AppSysParamsUtil.isMaintainSSRWiseCutoffTime()) {
			if (isAddEditAnci) {
				sb.append(" AND sc.booking_cutover_in_mins <?  ");
			}
		} else {
			sb.append(" AND sc.booking_cutover_in_mins <?  ");
		}

		sb.append(" AND sc.ssr_cat_id              =?  ");
		sb.append(" AND ssc.status                 ='ACT'  ");
		sb.append(" INNER JOIN t_ssr_info si  ");
		sb.append("      ON ssc.ssr_sub_cat_id= si.ssr_sub_cat_id  ");

		if (AppSysParamsUtil.isMaintainSSRWiseCutoffTime()) {
			if (!isAddEditAnci) {
				sb.append(" AND (SUBSTR(ssr_cutoff_time, 1, INSTR(ssr_cutoff_time, ':', 1 ) -1 ) * 60 ) + ");
				sb.append(" ( SUBSTR(ssr_cutoff_time, 4, INSTR(ssr_cutoff_time, ':', 1 ) )) < ? ");
			}
		}
		sb.append("  AND si.status='ACT' ");
		sb.append(generateApplicationTypeCondition(appIndicator));
		sb.append(" INNER JOIN t_ssr_applicability_containts sac  ");
		sb.append("      ON si.ssr_id      = sac.ssr_id  ");
		sb.append(generateAirportTypeCondition(airportType));
		sb.append(" INNER JOIN t_ssr_charge sch  ");
		sb.append("      ON si.ssr_id=sch.ssr_id  ");
		sb.append(" AND sch.status   ='ACT'  ");
		sb.append(" LEFT OUTER JOIN t_i18n_message msg  ");
		sb.append("      ON si.i18n_message_key=msg.i18n_message_key  ");
		sb.append(" AND msg.message_locale     =?  ");
		sb.append(" INNER JOIN t_ssr_applicable_airport saa  ");
		sb.append("      ON sac.ssr_app_con_id= saa.ssr_app_con_id  ");
		sb.append(" AND saa.airport_code      =?  ");
		sb.append(" INNER JOIN T_SSR_CHARGE_COS scc  ");
		sb.append("      ON sch.ssr_charge_id         =scc.ssr_charge_id  ");
		sb.append(AirinventoryUtils.prepareClassOfServiceWhereClause(cabinClass, logicalCabinClass, "scc"));
		if (isAirportTransfer) {
			sb.append(" INNER JOIN t_airport_transfer apt  ");
			sb.append("      ON apt.airport_code =saa.airport_code  ");
			sb.append("  AND apt.status ='ACT'  ");
		}

		sb.append(" ORDER BY si.sort_order ");
		Object param[];
		if (AppSysParamsUtil.isMaintainSSRWiseCutoffTime()) {
			if (isAddEditAnci) {
				param = new Object[] { cutoverTime, ssrCategory, selectedLanguage, airport };
			} else {
				param = new Object[] { ssrCategory, cutoverTime, selectedLanguage, airport };
			}
		} else {
			param = new Object[] { cutoverTime, ssrCategory, selectedLanguage, airport };
		}

		JdbcTemplate jdbcTemplate = new JdbcTemplate(AncillaryModuleUtils.getDatasource());

		List<AirportServiceDTO> airportServiceList = (List<AirportServiceDTO>) jdbcTemplate.query(sb.toString(), param,
				new ResultSetExtractor() {

					@Override
					public List<AirportServiceDTO> extractData(ResultSet rs) throws SQLException, DataAccessException {
						Set<AirportServiceDTO> airportServiceDTOs = new HashSet<AirportServiceDTO>();

						while (rs.next()) {
							Long ssrId = rs.getLong("ssr_id");
							boolean offerServiceFreeOfCharge = false;
							if (freeAirportServiceIds != null && freeAirportServiceIds.contains(ssrId.intValue())) {
								offerServiceFreeOfCharge = true;
							}
							AirportServiceDTO airportService = new AirportServiceDTO();
							airportService.setSsrID(ssrId);
							airportService.setSsrChargeID(rs.getLong("ssr_charge_id"));
							airportService.setSsrCode(rs.getString("ssr_code"));
							airportService.setSsrName(rs.getString("ssr_name"));
							airportService.setDescription(rs.getString("ssr_desc"));
							airportService.setStatus("ACT".equals(rs.getString("status")) ? "Y" : "N");
							airportService.setAirport(rs.getString("airport_code"));
							airportService.setApplicabilityType(rs.getString("applicablity_type"));
							airportService.setAdultAmount((rs.getString("adult_amount") == null || offerServiceFreeOfCharge)
									? "0"
									: rs.getString("adult_amount"));
							airportService.setChildAmount((rs.getString("child_amount") == null || offerServiceFreeOfCharge)
									? "0"
									: rs.getString("child_amount"));
							airportService.setInfantAmount((rs.getString("infant_amount") == null || offerServiceFreeOfCharge)
									? "0"
									: rs.getString("infant_amount"));
							airportService.setReservationAmount((rs.getString("reservation_amount") == null || offerServiceFreeOfCharge)
									? "0"
									: rs.getString("reservation_amount"));

							if (null != rs.getClob("message_content")) {
								airportService.setDecriptionSelectedLanguage(StringUtil.clobToString(rs
										.getClob("message_content")));
							}
							String ssrUrl = AppSysParamsUtil.getImageUploadPath();
							airportService.setImagePaths(ssrUrl);

							if (isAirportTransfer) {
								// in future there will be multiple providers
								ServiceProviderDTO provider = new ServiceProviderDTO();
								provider.setAirportTransferId(rs.getInt("airport_tranfer_id"));
								provider.setName(rs.getString("service_provider_name"));
								provider.setAddress(rs.getString("service_provider_address"));
								provider.setNumber(rs.getString("service_provider_number"));
								provider.setEmailId(rs.getString("service_provider_email"));
								airportService.setProvider(provider);
							}

							airportServiceDTOs.add(airportService);
						}

						return new ArrayList<AirportServiceDTO>(airportServiceDTOs);
					}
				});

		return airportServiceList;

	}

	@Override
	@SuppressWarnings("unchecked")
	public List<AirportServiceDTO> getSelectedAirportServices(String airport, long cutovertime, int ssrCategory,
			boolean skipCutoverValidation) {
		String query = "SELECT si.ssr_id, sch.ssr_charge_id, si.ssr_code, si.ssr_name, si.ssr_desc, si.status, sch.airport_code, "
				+ "sch.applicablity_type, sch.adult_amount, sch.child_amount, sch.infant_amount, sch.reservation_amount "
				+ "FROM t_ssr_category sc, t_ssr_sub_category ssc, t_ssr_info si, t_ssr_applicability_containts sac, "
				+ "t_ssr_applicable_airport saa, t_ssr_charge sch "
				+ "WHERE sc.ssr_cat_id = ssc.ssr_cat_id AND ssc.ssr_sub_cat_id = si.ssr_sub_cat_id AND si.ssr_id= sac.ssr_id "
				+ "AND sac.ssr_app_con_id= saa.ssr_app_con_id AND sch.ssr_id= si.ssr_id AND sc.ssr_cat_id=? AND saa.airport_code=? AND "
				+ "sc.status='ACT' AND ssc.status='ACT' AND sch.status='ACT' ";

		if (!skipCutoverValidation) {
			query += " AND sc.booking_cutover_in_mins < " + cutovertime;
		}

		Object[] param = { ssrCategory, airport };

		JdbcTemplate jdbcTemplate = new JdbcTemplate(AncillaryModuleUtils.getDatasource());

		List<AirportServiceDTO> airportServiceList = (List<AirportServiceDTO>) jdbcTemplate.query(query, param,
				new ResultSetExtractor() {

					@Override
					public List<AirportServiceDTO> extractData(ResultSet r) throws SQLException, DataAccessException {
						List<AirportServiceDTO> list = new ArrayList<AirportServiceDTO>();

						while (r.next()) {
							AirportServiceDTO airportService = new AirportServiceDTO();
							airportService.setSsrID(r.getLong("ssr_id"));
							airportService.setSsrChargeID(r.getLong("ssr_charge_id"));
							airportService.setSsrCode(r.getString("ssr_code"));
							airportService.setSsrName(r.getString("ssr_name"));
							airportService.setDescription(r.getString("ssr_desc"));
							airportService.setAirport(r.getString("airport_code"));
							airportService.setApplicabilityType(r.getString("applicablity_type"));
							airportService.setAdultAmount(r.getString("adult_amount"));
							airportService.setChildAmount(r.getString("child_amount"));
							airportService.setInfantAmount(r.getString("infant_amount"));
							airportService.setReservationAmount(r.getString("reservation_amount"));
							airportService.setStatus("ACT".equals(r.getString("status")) ? "Y" : "N");
							list.add(airportService);
						}

						return list;
					}
				});

		return airportServiceList;
	}

	private String generateApplicationTypeCondition(AppIndicatorEnum appIndicator) {
		String condition = "";

		if (AppIndicatorEnum.APP_IBE.equals(appIndicator)) {
			condition += " AND si.show_in_ibe='Y' ";
		} else if (AppIndicatorEnum.APP_XBE.equals(appIndicator)) {
			condition += " AND si.show_in_xbe='Y' ";
		} else if (AppIndicatorEnum.APP_GDS.equals(appIndicator)) {
			condition += " AND si.show_in_gds='Y' ";
		} else if (AppIndicatorEnum.APP_WS.equals(appIndicator)) {
			condition += " AND si.show_in_ws='Y' ";
		}

		return condition;
	}

	private String generateAirportTypeCondition(String airportType) {
		String condition = "";

		if (ReservationPaxSegmentSSR.APPLY_ON_DEPARTURE.equals(airportType)) {
			condition += " AND sac.apply_departure='Y' ";
		} else if (ReservationPaxSegmentSSR.APPLY_ON_ARRIVAL.equals(airportType)) {
			condition += " AND sac.apply_arrival='Y' ";
		} else if (ReservationPaxSegmentSSR.APPLY_ON_TRANSIT.equals(airportType)) {
			condition += " AND sac.apply_transit='Y' ";
		}

		return condition;
	}

	/*
	 * Private class to extract the data from the sql result set
	 */

	private class SSRDtoExtractorL implements ResultSetExtractor {

		private boolean validateQty = false;

		/**
		 * Parameter check whether allow zero available capacity entries to be included int the search result
		 * 
		 * @param validateQty
		 */
		public SSRDtoExtractorL(boolean validateQty) {
			this.validateQty = validateQty;
		}

		@Override
		public List<SpecialServiceRequestDTO> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
			List<SpecialServiceRequestDTO> requestDTOs = new ArrayList<SpecialServiceRequestDTO>();

			while (resultSet.next()) {
				String ssrCode = resultSet.getString("SSR_CODE");

				if (validateQty && resultSet.getInt("AVAIL_QTY") < 0) {
					continue;
				}
				SpecialServiceRequestDTO serviceRequestDTO = new SpecialServiceRequestDTO();
				serviceRequestDTO.setSsrID(resultSet.getLong("SSR_ID"));
				serviceRequestDTO.setCabinClass(resultSet.getString("CABIN_CLASS_CODE"));
				serviceRequestDTO.setSsrCode(ssrCode);
				serviceRequestDTO.setSsrName(resultSet.getString("SSR_NAME"));
				serviceRequestDTO.setDescription(resultSet.getString("SSR_DESC"));
				serviceRequestDTO.setOndCode(resultSet.getString("OND_CODE"));
				serviceRequestDTO.setChargeAmount(resultSet.getString("CHARGE_AMOUNT"));
				serviceRequestDTO.setSsrChargeID(resultSet.getLong("SSR_CHARGE_ID"));
				serviceRequestDTO.setAvailableQty(resultSet.getInt("AVAIL_QTY"));
				serviceRequestDTO.setDecriptionSelectedLanguage(resultSet.getString("SELLANGTEXT"));
				serviceRequestDTO.setStatus("ACT".equals(resultSet.getString("STATUS")) ? "Y" : "N");
				serviceRequestDTO.setCutOffTime(resultSet.getString("SSR_CUTOFF_TIME"));
				requestDTOs.add(serviceRequestDTO);

			}

			return requestDTOs;
		}
	}

	@Override
	public Integer getSSRCategoryFromSSrId(Integer ssrId) {

		Integer ssrCategoryId = null;

		StringBuilder sb = new StringBuilder();
		sb.append("  SELECT sc.ssr_cat_id ");
		sb.append("    FROM t_ssr_info si, ");
		sb.append("   t_ssr_sub_category sc ");
		sb.append("   WHERE si.ssr_id     =? ");
		sb.append(" AND si.ssr_sub_cat_id = sc.ssr_sub_cat_id ");

		String sql = sb.toString();

		Object[] param = { ssrId };
		JdbcTemplate jdbcTemplate = new JdbcTemplate(AncillaryModuleUtils.getDatasource());
		ssrCategoryId = (Integer) jdbcTemplate.query(sql, param, new ResultSetExtractor() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					return rs.getInt("ssr_cat_id");
				}
				return null;
			}
		});

		return ssrCategoryId;
	}
	
	private String getUserDefinedSsrQueryString() {

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT TSI.SSR_ID,TSI.SSR_CODE,TSI.SSR_NAME,TSI.SSR_DESC, ");
		sb.append("TSI.SSR_CUTOFF_TIME ,' ' OND_CODE,' ' CABIN_CLASS_CODE, ");
		sb.append("SSR_CHARGE.ADULT_AMOUNT CHARGE_AMOUNT, 0 SSR_CHARGE_ID, ");
		sb.append("' ' SELLANGTEXT, TSI.STATUS, 10 AVAIL_QTY ");
		sb.append("FROM T_SSR_INFO TSI ");
		sb.append("INNER JOIN T_SSR_SUB_CATEGORY TSSC ON TSSC.SSR_SUB_CAT_ID = TSI.SSR_SUB_CAT_ID ");
		sb.append("AND TSSC.SSR_CAT_ID = 1 AND TSI.USER_DEFINED_SSR = 'Y' ");
		sb.append("AND TSI.STATUS = 'ACT' ");
		// sb.append("AND TSI.SHOW_IN_WS = 'Y' ");
		sb.append("INNER JOIN T_SSR_CHARGE SSR_CHARGE ON SSR_CHARGE.SSR_ID  = TSI.SSR_ID AND SSR_CHARGE.STATUS = 'ACT' ");

		return sb.toString();
	}
	
	@SuppressWarnings("unchecked")
	public boolean isUserDefinedSSR(String ssrCode) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(AncillaryModuleUtils.getDatasource());
		boolean isUserDefined = false;
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT COUNT(*) AS CNT FROM T_SSR_INFO WHERE USER_DEFINED_SSR='Y' AND SSR_CODE='" + ssrCode + "'");

		isUserDefined = (boolean) jdbcTemplate.query(sqlBuilder.toString(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {

				if (resultSet != null && resultSet.next()) {
					return resultSet.getInt("CNT") > 0 ? true : false;
				}
				return false;

			}
		});

		return isUserDefined;
	}
}
