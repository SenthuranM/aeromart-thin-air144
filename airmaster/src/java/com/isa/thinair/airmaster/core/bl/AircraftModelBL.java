package com.isa.thinair.airmaster.core.bl;

/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.SegmentDTO;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airmaster.api.dto.AirCraftModelSeatsDTO;
import com.isa.thinair.airmaster.api.model.AirCraftModelSeats;
import com.isa.thinair.airmaster.api.model.Aircraft;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.airmaster.core.audit.AuditAirMaster;
import com.isa.thinair.airmaster.core.persistence.dao.AircraftDAO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.service.ScheduleBD;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * Class to implement the bussiness logic related to the Flight
 * 
 * @author Byorn
 */
public class AircraftModelBL {

	private final FlightBD flightBD = AirmasterUtils.getFlightBD();
	private final FlightInventoryBD flightInventoryBD = AirmasterUtils.getFlightInventoryBD();
	private final ScheduleBD flightScheduleBD = AirmasterUtils.getScheduleBD();

	public AircraftDAO getAircraftDAO() {
		return (AircraftDAO) AirmasterUtils.getInstance().getLocalBean("AircraftDAOProxy");

	}

	public void saveOrUpdate(AircraftModel aircraftModel, UserPrincipal userPri) throws ModuleException {
		// if editing and updating
		if (!(aircraftModel.getVersion() < 0)) {

			// if user has edited the capacities
			if (capacitiesHasChanged(aircraftModel)) {		
				validateForAttachedTemplates(aircraftModel.getModelNumber());		
				if (flightBD.isModelUsedByFlight(aircraftModel.getModelNumber())) {
					AircraftModel existingModel = getAircraftDAO().getAircraftModel(aircraftModel.getModelNumber());

					if (AppSysParamsUtil.enableDowngradeAircraftModel()) {
						// Split the flight schedules and populate the flights
						AircraftModel adhocModel = existingModel;
						String modelNumber = existingModel.getModelNumber() + "_" + existingModel.getVersion();
						adhocModel.setModelNumber(modelNumber);
						adhocModel.setVersion(-1);
						adhocModel.setStatus(Aircraft.STATUS_INACTIVE);
						Set<AircraftCabinCapacity> existingCapacity = existingModel.getAircraftCabinCapacitys();

						if (existingCapacity != null) {
							Set<AircraftCabinCapacity> adhocCapacities = new HashSet<AircraftCabinCapacity>();
							Iterator<AircraftCabinCapacity> adhocCapacity = existingCapacity.iterator();
							while (adhocCapacity.hasNext()) {
								AircraftCabinCapacity addCap = adhocCapacity.next();
								addCap.setVersion(-1);
								addCap.setAircraftModelNumber(modelNumber);
								adhocCapacities.add(addCap);
							}

							adhocModel.setAircraftCabinCapacitys(adhocCapacities);

						}
						adhocModel.setOldaircraftcapacitys(aircraftModel.getOldaircraftcapacitys());
						getAircraftDAO().saveOnlyAircraftModel(adhocModel);
						AuditAirMaster.doAudit(adhocModel, userPri);
						flightScheduleBD.splitBulkSchedule(aircraftModel.getModelNumber(), modelNumber);
					}
					Collection<Flight> flights = flightBD.getFutureNonCNXFlightsForModel(aircraftModel.getModelNumber(),
							new Date(System.currentTimeMillis()));

					if (flights != null && flights.size() != 0) {
						Collection<Integer> flightIds = getFlightIDs(flights);
						// check if the flights with reservations have more than the new seat capacity
						// if eligeble then update the inventories for the the flights
						DefaultServiceResponse servResponce = flightInventoryBD.checkFCCInventoryModelUpdatability(flightIds,
								aircraftModel, existingModel, false, false);
						if (servResponce.isSuccess()) {

							String flightSegmentCode = "";

							Iterator<Flight> flightIterator = flights.iterator();
							if (flightIterator.hasNext()) {
								Flight tmpFlight = flightIterator.next();
								FlightSegement tmpSegment = SegmentUtil
										.getDetailsOfLongestSegment(tmpFlight.getFlightSegements());
								flightSegmentCode = tmpSegment.getSegmentCode();
							}
							Collection<SegmentDTO> flightSegments = SegmentUtil.getSegmentDTOs(flights);
							flightInventoryBD.updateFCCInventoriesForModelUpdate(flightIds, aircraftModel, existingModel,
									flightSegmentCode, false, flightSegments);

						} else {
							ModuleException exception = new ModuleException(servResponce.getResponseCode(), "airmaster.desc");
							if (servResponce.getResponseParam(ResponceCodes.FLIGHT) != null) {
								exception
										.setCustumValidateMessage(servResponce.getResponseParam(ResponceCodes.FLIGHT).toString());
							}
							throw exception;
						}
					}

				}

			}
		}

		// save or update the aircraft model.
		// note: when updating only if constraint appeared code wont come here.
		try {
			getAircraftDAO().saveAircraftModel(aircraftModel);
			AuditAirMaster.doAudit(aircraftModel, userPri);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airmaster.desc");

		}
	}

	/**
	 * 
	 * @param aircraftModel
	 * @return
	 * @throws ModuleException
	 */
	public String getAffectedFiles(AircraftModel aircraftModel) throws ModuleException {
		Collection<Flight> flights = flightBD.getFlightsForModel(aircraftModel.getModelNumber(),
				new Date(System.currentTimeMillis()));
		StringBuilder returnStr = new StringBuilder();
		if (flights != null && flights.size() != 0) {
			Collection<Integer> flightIds = getFlightIDs(flights);
			DefaultServiceResponse returnResponce = flightInventoryBD.getAffectedFlights(flightIds, aircraftModel);
			if (returnResponce.isSuccess()) {
				if (returnResponce.getResponseParam("overbookFlights") != null) {
					String strOverBook = returnResponce.getResponseParam("overbookFlights").toString();
					returnStr.append(strOverBook);
					returnStr.append("      ");
					returnStr.append("<br><b>Please click save to update the aircraft model</b>");
				} else {
					// returnStr.append("");
				}

			}
		} else {
			// returnStr.append("<b>No Overbook flights found.Please click save to update the aircraft model</b>");
		}
		return returnStr.toString();
	}

	public String getAffectedFiles(AircraftModel aircraftModel, Integer flightID) throws ModuleException {

		StringBuilder returnStr = new StringBuilder();

		Collection<Integer> flightIds = new ArrayList<Integer>();
		flightIds.add(flightID);
		DefaultServiceResponse returnResponce = flightInventoryBD.getAffectedFlights(flightIds, aircraftModel);
		if (returnResponce.isSuccess()) {
			if (returnResponce.getResponseParam("overbookFlights") != null) {
				String strOverBook = returnResponce.getResponseParam("overbookFlights").toString();
				returnStr.append(strOverBook);
				returnStr.append("      ");
				returnStr.append("<br><b>Please click save to update the flight</b>");
			} else {
				// returnStr.append("<b>No Overbook flights found.Please click save to update the aircraft model</b>");
			}

		} else {
			// returnStr.append("<b>No Overbook flights found.Please click save to update the aircraft model</b>");
		}
		return returnStr.toString();
	}

	/**
	 * 
	 * @param aircraftModel
	 * @return
	 */
	private boolean vvalidateForAttachedTemplates(AircraftModel aircraftModel) throws ModuleException {

		Set<AircraftCabinCapacity> newCapacities = aircraftModel.getAircraftCabinCapacitys();

		AircraftModel aircraftModel2 = getAircraftDAO().getAircraftModel(aircraftModel.getModelNumber());

		Set<AircraftCabinCapacity> oldCapacities = aircraftModel2.getAircraftCabinCapacitys();

		Iterator<AircraftCabinCapacity> iterator = oldCapacities.iterator();
		aircraftModel.setOldaircraftcapacitys(oldCapacities);
		/*
		 * If sizes of the capacity sets are different that means capacities have been changed definitely. If size is
		 * not considered the following code will not detect a change in capacities if a new Cabin Class is added to the
		 * Aircraft.
		 */
		if (!AppSysParamsUtil.enableDowngradeAircraftModel()) {
			if (flightBD.isAirModelHasSeatTemplate(aircraftModel.getModelNumber())
					&& flightBD.isAirModelHasChargeTemplate(aircraftModel.getModelNumber())) {
				throw new ModuleException("airmaster.aircraftmodel.hasseatmap", "airmaster.desc");
			}
		} else {
			List<String> lstFLights = flightBD.getFLightsWithAirModelHasChargeTemplate(aircraftModel.getModelNumber());
			ModuleException excep = new ModuleException("airmaster.aircraftmodel.hasseatmap", "airmaster.desc");
			StringBuffer strBuf = new StringBuffer();
			strBuf.append("Following flights attached with the seat map:");
			int i = 1;
			for (String str : lstFLights) {
				if (i == lstFLights.size()) {
					strBuf.append(str);
				} else if (i % 8 == 0) {
					strBuf.append(str + "    ,");
				} else {
					strBuf.append(str + ",");
				}
				i++;
			}
			if (lstFLights.size() > 0) {
				excep.setCustumValidateMessage(strBuf.toString());
				throw excep;
			}
		}

		if (newCapacities.size() != oldCapacities.size()) {
			return true;
		}

		while (iterator.hasNext()) {
			AircraftCabinCapacity oldAircraftCabinCapacity = iterator.next();

			// if a capacity has been deleted check if the cos was attached to a flight.
			// if so you can not delte a cos, throw an exception
			if (capacitysHasBeenDeleted(newCapacities, oldAircraftCabinCapacity)) {
				if (flightBD.isModelUsedByFlight(aircraftModel.getModelNumber())) {
					throw new ModuleException("airmaster.aircraftmodel.inuse", "airmaster.desc");
				}
			}

			if (newCapacities.contains(oldAircraftCabinCapacity)) {
				continue;
			} else {

				return true;
			}

		}
		return false;

	}
	
	
	private void validateForAttachedTemplates(String modelNumber) throws ModuleException {
		if (!AppSysParamsUtil.enableDowngradeAircraftModel()) {
			if (flightBD.isAirModelHasSeatTemplate(modelNumber)
					&& flightBD.isAirModelHasChargeTemplate(modelNumber)) {
				throw new ModuleException("airmaster.aircraftmodel.hasseatmap", "airmaster.desc");
			}
		} else {
			List<String> lstFLights = flightBD.getFLightsWithAirModelHasChargeTemplate(modelNumber);
			ModuleException excep = new ModuleException("airmaster.aircraftmodel.hasseatmap", "airmaster.desc");
			StringBuffer strBuf = new StringBuffer();
			strBuf.append("Following flights attached with the seat map:");
			int i = 1;
			for (String str : lstFLights) {
				if (i == lstFLights.size()) {
					strBuf.append(str);
				} else if (i % 8 == 0) {
					strBuf.append(str + "    ,");
				} else {
					strBuf.append(str + ",");
				}
				i++;
			}
			if (lstFLights.size() > 0) {
				excep.setCustumValidateMessage(strBuf.toString());
				throw excep;
			}
		}
	}
	
	
	
	private boolean capacitiesHasChanged(AircraftModel aircraftModel) throws ModuleException {

		Set<AircraftCabinCapacity> newCapacities = aircraftModel.getAircraftCabinCapacitys();

		AircraftModel aircraftModel2 = getAircraftDAO().getAircraftModel(aircraftModel.getModelNumber());

		Set<AircraftCabinCapacity> oldCapacities = aircraftModel2.getAircraftCabinCapacitys();

		Iterator<AircraftCabinCapacity> iterator = oldCapacities.iterator();
		aircraftModel.setOldaircraftcapacitys(oldCapacities);
		
		boolean capacitiesChanged = false;
		
		if (newCapacities.size() != oldCapacities.size()) {
			capacitiesChanged = true;
		}

		if(!capacitiesChanged) {
			while (iterator.hasNext()) {
				AircraftCabinCapacity oldAircraftCabinCapacity = iterator.next();

				// if a capacity has been deleted check if the cos was attached to a flight.
				// if so you can not delte a cos, throw an exception
				if (capacitysHasBeenDeleted(newCapacities, oldAircraftCabinCapacity)) {
					if (flightBD.isModelUsedByFlight(aircraftModel.getModelNumber())) {
						throw new ModuleException("airmaster.aircraftmodel.inuse", "airmaster.desc");
					} else {
						capacitiesChanged = true;
						break;
					}
				}

				if (!newCapacities.contains(oldAircraftCabinCapacity)) {
					capacitiesChanged = true;
					break;
				} 
			}
		}	
		return capacitiesChanged;
	}
	
	
	
	

	private Collection<Integer> getFlightIDs(Collection<Flight> flights) {
		Collection<Integer> flightIDs = new ArrayList<Integer>();
		for (Flight flight : flights) {
			flightIDs.add(flight.getFlightId());
		}
		return flightIDs;
	}

	private boolean capacitysHasBeenDeleted(Set<AircraftCabinCapacity> newCapacities,
			AircraftCabinCapacity oldAircraftCabinCapacity) {

		List<String> coss = new ArrayList<String>();
		Iterator<AircraftCabinCapacity> iter = newCapacities.iterator();
		while (iter.hasNext()) {
			AircraftCabinCapacity cabinCapacity = iter.next();
			coss.add(cabinCapacity.getCabinClassCode());
		}

		if (coss.contains(oldAircraftCabinCapacity.getCabinClassCode())) {
			return false;
		} else {
			return true;
		}
	}

	public AirCraftModelSeatsDTO getAirCraftModelSeatsDTO(Collection<AirCraftModelSeats> airCraftModelSeats) {

		AirCraftModelSeatsDTO airCraftModelSeatsDTO = new AirCraftModelSeatsDTO();
		airCraftModelSeatsDTO.setTotalseats(airCraftModelSeats.size());

		Set<Integer> columnsSet = new HashSet<Integer>();
		Set<Integer> rowsSet = new HashSet<Integer>();
		Iterator<AirCraftModelSeats> Itr = airCraftModelSeats.iterator();
		String aircraftModel = null;
		while (Itr.hasNext()) {
			AirCraftModelSeats airCraftModelSeat = Itr.next();
			columnsSet.add(airCraftModelSeat.getColId());
			rowsSet.add(airCraftModelSeat.getRowId());
			aircraftModel = airCraftModelSeat.getModelNo();
		}
		airCraftModelSeatsDTO.setTotalColumns(columnsSet.size());
		airCraftModelSeatsDTO.setTotalRows(rowsSet.size());
		airCraftModelSeatsDTO.setAirCraftModelSeats(airCraftModelSeats);
		airCraftModelSeatsDTO.setAircraftModel(getAircraftDAO().getAircraftModel(aircraftModel));
		return airCraftModelSeatsDTO;
	}

	public Map<Integer, AirCraftModelSeats>
			getAirCraftModelSeatsSeatIDModelSeat(Collection<AirCraftModelSeats> airCraftModelSeats) {
		Map<Integer, AirCraftModelSeats> map = new HashMap<Integer, AirCraftModelSeats>();
		Iterator<AirCraftModelSeats> Itr = airCraftModelSeats.iterator();
		while (Itr.hasNext()) {
			AirCraftModelSeats airCraftModelSeat = Itr.next();
			map.put(airCraftModelSeat.getSeatID(), airCraftModelSeat);
		}
		return map;
	}
}
