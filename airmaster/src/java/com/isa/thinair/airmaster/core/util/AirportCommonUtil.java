package com.isa.thinair.airmaster.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.dto.CachedAirlineDTO;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airmaster.core.bl.AirmasterDAOUtil;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class AirportCommonUtil {

	private static AirportCommonUtil airportCommonUtil = null;

	private static Map<String, CachedAirportDTO> ownAirportOperatorList = new HashMap<String, CachedAirportDTO>();

	private static Map<String, CachedAirportDTO> lccAirportOperatorList = new HashMap<String, CachedAirportDTO>();

	private static Map<String, CachedAirportDTO> unifiedAirportOperatorList = new HashMap<String, CachedAirportDTO>();

	private static Map<String, Set<String>> unifiedCityVsCountryStateCodesMap = new HashMap<>();
	
	private static Map<String, CachedAirlineDTO> lccAirlineList = null;

	private static final long CASH_TIMEOUT = AppSysParamsUtil.getMasterDataCacheTimeout();

	private volatile Date recordAge = new Date(System.currentTimeMillis() - CASH_TIMEOUT);

	private volatile boolean refreshingAirportList = false;

	private final Object lock = new Object();

	private static Log log = LogFactory.getLog(AirportCommonUtil.class);

	private AirportCommonUtil() {
		super();
	}

	public static synchronized AirportCommonUtil getInstance() {
		if (airportCommonUtil == null) {
			airportCommonUtil = new AirportCommonUtil();
		}
		return airportCommonUtil;
	}

	private void refreshAirportList() throws ModuleException {
		if (System.currentTimeMillis() - recordAge.getTime() >= CASH_TIMEOUT) {
			synchronized (lock) {
				if (System.currentTimeMillis() - recordAge.getTime() >= CASH_TIMEOUT) {
					refreshingAirportList = true;
					try {
						// refreshing own airport list
						Map<String, CachedAirportDTO> ownAirports = AirmasterDAOUtil.getAirportDAO().getCachedAirports();
						ownAirportOperatorList.clear();
						ownAirportOperatorList.putAll(ownAirports);
						
						if(AppSysParamsUtil.enableCityBasesFunctionality()) {
							Map<String, CachedAirportDTO> cities = AirmasterDAOUtil.getAirportDAO().getCachedCitySearchAllowedDetails();
							ownAirportOperatorList.putAll(cities);
						}						
						

						if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
							// refressing lcc airport list
							Map<String, CachedAirportDTO> lccAirports = LookUpUtils.lccLookupDAO().getAirports();
							lccAirportOperatorList.clear();
							lccAirportOperatorList.putAll(lccAirports);
						}
						// refresh the unified airport list
						unifiedAirportOperatorList.clear();
						unifiedAirportOperatorList.putAll(ownAirportOperatorList);
						for (CachedAirportDTO lccAirport : lccAirportOperatorList.values()) {
							if (unifiedAirportOperatorList.get(lccAirport.getAirportCode()) == null) {
								unifiedAirportOperatorList.put(lccAirport.getAirportCode(), lccAirport);
							}
						}

						unifiedCityVsCountryStateCodesMap.clear();
						for (CachedAirportDTO airPort : unifiedAirportOperatorList.values()) {
							if (StringUtils.isNotEmpty(airPort.getCityId()) && StringUtils.isNotEmpty(airPort.getStateCode())) {
								if (unifiedCityVsCountryStateCodesMap.get(airPort.getCityId()) == null) {
									unifiedCityVsCountryStateCodesMap.put(airPort.getCityId(), new HashSet<>());
								}
								unifiedCityVsCountryStateCodesMap.get(airPort.getCityId())
										.add(airPort.getCountryCode() + "_" + airPort.getStateCode());
							}
						}
						
						recordAge.setTime(System.currentTimeMillis());
						if (log.isDebugEnabled()) {
							log.debug("Airport list refresh at " + recordAge);
						}
					}

					catch (CommonsDataAccessException e) {
						log.error("airport refresh fails", e);
						throw new ModuleException(e.getCause(), e.getExceptionCode(), "airmaster.desc");
					} catch (Exception e) {
						log.error("airport refresh fails", e);
						throw new ModuleException("airmaster.technical.error", "airmaster.desc");
					} finally {
						refreshingAirportList = false;
					}
				}
			}
		}
	}
	
	private Map<String, CachedAirportDTO> getOwnAirportOperatorList() throws ModuleException {

		if ((System.currentTimeMillis() - recordAge.getTime()) >= CASH_TIMEOUT) {
			refreshAirportList();
		}

		if (!refreshingAirportList) {
			return Collections.unmodifiableMap(ownAirportOperatorList);
		} else {
			synchronized (lock) {
				return Collections.unmodifiableMap(ownAirportOperatorList);
			}
		}
	}

	private Map<String, CachedAirportDTO> getLccAirportOperatorList() throws ModuleException {

		if ((System.currentTimeMillis() - recordAge.getTime()) >= CASH_TIMEOUT) {
			refreshAirportList();
		}

		if (!refreshingAirportList) {
			return Collections.unmodifiableMap(lccAirportOperatorList);
		} else {
			synchronized (lock) {
				return Collections.unmodifiableMap(lccAirportOperatorList);
			}
		}
	}

	public Map<String, CachedAirportDTO> getAllAirportOperatorList() throws ModuleException {

		if ((System.currentTimeMillis() - recordAge.getTime()) >= CASH_TIMEOUT) {
			refreshAirportList();
		}

		if (!refreshingAirportList) {
			return Collections.unmodifiableMap(unifiedAirportOperatorList);
		} else {
			synchronized (lock) {
				return Collections.unmodifiableMap(unifiedAirportOperatorList);
			}
		}
	}

	public Map<String, Set<String>> getCityVsCountryStateCodesMap() throws ModuleException {

		if ((System.currentTimeMillis() - recordAge.getTime()) >= CASH_TIMEOUT) {
			refreshAirportList();
		}

		if (!refreshingAirportList) {
			return Collections.unmodifiableMap(unifiedCityVsCountryStateCodesMap);
		} else {
			synchronized (lock) {
				return Collections.unmodifiableMap(unifiedCityVsCountryStateCodesMap);
			}
		}
	}
	
	private void loadLccAirlines() {
		if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
			lccAirlineList = new HashMap<String, CachedAirlineDTO>();
			Map<String, CachedAirlineDTO> lccAirlines = LookUpUtils.lccLookupDAO().getAirlines();
			lccAirlineList.putAll(lccAirlines);
		}
	}

	public Map<String, CachedAirlineDTO> getLccAirlineList() throws ModuleException {
		if (lccAirlineList == null) {
			loadLccAirlines();
		}
		return lccAirlineList;
	}
	
	public String getOwnCountryCode(String airPortCode) throws ModuleException {
		return getOwnAirportOperatorList().get(airPortCode).getCountryCode();
	}

	public boolean isAirportOptByOwn(String airportCode) throws ModuleException {
		CachedAirportDTO ownAirport = getOwnAirportOperatorList().get(airportCode);
		return (ownAirport != null);
	}

	public boolean isAirportOptByOther(String airportCode) throws ModuleException {
		CachedAirportDTO lccAirport = getLccAirportOperatorList().get(airportCode);
		if (lccAirport == null) {
			return false;
		} else if (lccAirport.getOperatedBy().equals(AppSysParamsUtil.getDefaultCarrierCode())) {
			// airport is operated only by default carrier.
			return false;
		} else {
			return true;
		}
	}

	public Map<String, CachedAirportDTO> getCachedAllAirportMap(Collection<String> airportCodes) throws ModuleException {
		Map<String, CachedAirportDTO> retMap = new HashMap<String, CachedAirportDTO>();
		for (String airportCode : airportCodes) {
			retMap.put(airportCode, getAllAirportOperatorList().get(airportCode));
		}
		return Collections.unmodifiableMap(retMap);
	}

	public CachedAirportDTO getCachedAllAirport(String airportCode) throws ModuleException {
		return getAllAirportOperatorList().get(airportCode);
	}

	public Map<String, CachedAirportDTO> getCachedOwnAirportMap(Collection<String> airportCodes) throws ModuleException {
		Map<String, CachedAirportDTO> retMap = new HashMap<String, CachedAirportDTO>();
		for (String airportCode : airportCodes) {
			retMap.put(airportCode, getOwnAirportOperatorList().get(airportCode));
		}
		return Collections.unmodifiableMap(retMap);
	}

	public boolean isContainSurfaceAirport(Map<String, CachedAirportDTO> airportCodes) {
		Collection<CachedAirportDTO> airports = airportCodes.values();
		for (Iterator<CachedAirportDTO> iterator = airports.iterator(); iterator.hasNext();) {
			CachedAirportDTO airport = iterator.next();
			if (airport != null && airport.isSurfaceSegment()) {
				return true;
			}
		}
		return false;
	}

	public boolean isBusSegment(String segmentCode) throws ModuleException {
		String arr[] = segmentCode.split("/");
		List<String> list = new ArrayList<String>();
		list.add(arr[0]);
		list.add(arr[arr.length - 1]);
		return isContainGroundSegment(list);
	}

	public boolean isContainGroundSegment(Collection<String> colAirPortCodes) throws ModuleException {
		return isContainSurfaceAirport(getCachedAllAirportMap((colAirPortCodes)));
	}

	public String getOwnCityId(String airPortCode) throws ModuleException {
		return getAllAirportOperatorList().get(airPortCode).getCityId();
	}

	public List<String> getSameCityAirports(String airportCode) throws ModuleException {
		List<String> sameCityAirports = new ArrayList<String>();
		CachedAirportDTO ownAirport = getOwnAirportOperatorList().get(airportCode);
		// input city will be returned always
		sameCityAirports.add(ownAirport.getAirportCode());
		for (CachedAirportDTO cachedAirportDTO : getOwnAirportOperatorList().values()) {
			if (cachedAirportDTO.getCityId() != null && cachedAirportDTO.getCityId().equals(ownAirport.getCityId())
					&& !cachedAirportDTO.getAirportCode().equals(ownAirport.getAirportCode())) {
				sameCityAirports.add(cachedAirportDTO.getAirportCode());
			}
		}
		return sameCityAirports;
	}
}
