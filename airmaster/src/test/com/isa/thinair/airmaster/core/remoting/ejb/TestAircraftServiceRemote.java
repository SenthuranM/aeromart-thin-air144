/*
 * Created on Jul 4, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.isa.thinair.airmaster.core.remoting.ejb;

import java.util.Collection;
import java.util.HashSet;

import java.util.Set;


import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

import com.isa.thinair.platform.api.util.PlatformTestCase;

import com.isa.thinair.airmaster.api.service.AircraftBD;

import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.api.model.CabinClass;






/**
 * @author byorn
 *
 */
public class TestAircraftServiceRemote extends PlatformTestCase{
	protected void setUp() throws Exception{
		super.setUp();
	}
	
//	public void testInterceptor() throws Exception
//	{
//		Aircraft obj1 = new Aircraft();
//		//obj1.setAircraftId();
//		obj1.setAircraftTailNumber("AAA");
//		//obj1.setCapacity(23);
//		//obj1.setModelNumber("RRR");
//		obj1.setSerialNumber("12222223");
//		obj1.setStatus(obj1.STATUS_ACTIVE);
//		obj1.setModelNumber("CCC");
//		//obj1.s
//		
//		try{
//		getAircraftService().saveAircraft(obj1); } 
//		catch(ModuleException ex)
//		{
//			System.out.println("Exception Code :-" + ex.getExceptionCode());
//		}
//		
//		
//	}
//	
//		
//	public void testSaveAircraft() throws Exception
//	{
//		
//		
//		Aircraft obj1 = new Aircraft();
//		//obj1.setAircraftId();
//		obj1.setAircraftTailNumber("AAA");
//		//obj1.setCapacity(23);
//		//obj1.setModelNumber("RRR");
//		obj1.setSerialNumber("12");
//		obj1.setStatus(obj1.STATUS_ACTIVE);
//		obj1.setModelNumber("ISA");
//		//obj1.s
//		getAircraftService().saveAircraft(obj1);
//		//getAircraftService().r
//		
//		//getAircraftService().delete(6);
//		
//		//assertEq
//		//assertNull(getAircraftService().getAircraft(6));
//	}
//
//	
//	
//	public void testGetActiveAircrafts() throws Exception
//	{
//		Collection l = getAircraftService().getActiveAircrafts();
//		assertTrue("No records", l.size()>0);
//	}
//	
//	public void testGetActiveAircraftModels() throws Exception
//	{
//		Collection l = getAircraftService().getActiveAircraftModels();
//		assertTrue("No records", l.size()>0);
//	}
//	
//	public void testLoadAndUpdateAircraft() throws Exception
//	{
//	
//		
//		Aircraft obj1 = getAircraftService().getAircraft(800);
//
//		String oldval = obj1.getAircraftTailNumber();
//		String newval = "newval";
//		obj1.setAircraftTailNumber(newval);
//		getAircraftService().saveAircraft(obj1);
//		
//		assertNotSame(oldval, getAircraftService().getAircraft(800).getAircraftTailNumber());
//				
//	}
//			
	
	
	
	
//	public void testSaveAircraftModel() throws Exception
//	{
//		
//		AircraftModel obj = new AircraftModel();
//		obj.setModelNumber("YUI");
//		obj.setStatus("active");
//		
//				AircraftCabinCapacity c = new AircraftCabinCapacity();
//				//c.setAcccId(11);
//				c.setAircraftModelNumber("HJK");
//				c.setCabinClassCode("1");
//				c.setSeatCapacity(12);
//				c.setInfantCapacity(2);
//				//c.setVersion(-1);
//		
//				Set s = new HashSet();
//				//s =  obj.getAircraftCcCapacitys();
//				s.add(c);
//		
//		obj.setAircraftCabinCapacitys(s);
//		
//		getAircraftService().saveAircraftModel(obj);
//		
//	}
//
//
//
//	
//	
//	
//	public void testRemoveAircraftModel() throws Exception
//	{
//		getAircraftService().removeAircraftModel("YUI");
//		
//	}
	
		
	public void testLoadAndUpdateAircraftModelOrCapacity() throws Exception{
		
		AircraftModel obj = getAircraftService().getAircraftModel("A340");
		
		String olddescription = obj.getDescription();
		String newdescription = "newdescription";
		
		obj.setDescription(newdescription);
			
		Set modelcapacities = obj.getAircraftCabinCapacitys();
		
		AircraftCabinCapacity acc = (AircraftCabinCapacity)modelcapacities.iterator().next();
		assertNotNull(acc);
		
		int oldcapacity = acc.getSeatCapacity();
		int newcapacity = 100;
		
		
		
		acc.setSeatCapacity(newcapacity);
		modelcapacities.add(acc);
		obj.setAircraftCabinCapacitys(modelcapacities);
		
		getAircraftService().saveAircraftModel(obj);
		
		//assertNotSame(oldcapacity, getAircraftService().getAircraftModel("AAA").getAircraftCabinCapacitys().);
		
	}
	
	
	
	
	
	

//	
//	
//	
//	public void testGetAircraftsPaging() throws Exception
//	{
//
//	
//		Page p= getAircraftService().getAircrafts(3,7);
//		assertNotNull(p);
//		//System.out.println("inside get Aircrafts Paging");
//		//System.out.println(l);
//		
//	
//	}
//	
//
//	public void testGetAircrafts() throws Exception
//	{
//		Collection l = getAircraftService().getAircrafts();
//		assertNotNull(l);
//		
//		//System.out.println("inside get Aircrafts");
//		//System.out.println(l);
//		
//	}
//	
//	public void testGetCabinClasses() throws Exception
//	{
//		Collection l = getAircraftService().getCabinClasses("AAA");
//		assertTrue("No Recs",l.size()>0);
//		
//		Collection l1 = getAircraftService().getAllCabinClasses();
//		assertTrue("No Recs",l1.size()>0);
//	
//		CabinClass c = getAircraftService().getCabinClass("Y");
//		assertNotNull(c);
//	
//		
//		//System.out.println("inside get Aircrafts");
//		//System.out.println(l);
//		
//	}

					
	private AircraftBD getAircraftService()
	{ 
		LookupService lookup = LookupServiceFactory.getInstance();
		IModule airmasterModule = lookup.getModule("airmaster");
		System.out.println("Lookup successful...");
		AircraftBD delegate = null;
		try {
			delegate = (AircraftBD)airmasterModule.getServiceBD("aircraft.service.remote");
			System.out.println("Delegate creation successful...");
			return delegate;
			
		} catch (Exception e) {
			System.out.println("Exception in creating delegate");
			e.printStackTrace();
		}
		return delegate;
	}

}
