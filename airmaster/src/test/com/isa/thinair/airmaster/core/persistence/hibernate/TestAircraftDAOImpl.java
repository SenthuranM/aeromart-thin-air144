/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airmaster.core.persistence.hibernate;


import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Collection;

import junit.framework.TestCase;

import com.isa.thinair.airmaster.api.model.AirCraftModelSeats;
import com.isa.thinair.airmaster.api.model.Aircraft;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.core.persistence.dao.AircraftDAO;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.core.controller.ModuleFramework;

/**
 * @author Byorn
 *
 */

public class TestAircraftDAOImpl extends TestCase {

	Aircraft aircraft = null;
	AircraftModel aircraftmodel = null;
	
	public TestAircraftDAOImpl() {
		super();
	}

	public TestAircraftDAOImpl(String arg0) {
		super(arg0);
	}
	
	protected void setUp(){
		//initialize framework
		ModuleFramework.startup();
	}
	
	
	

//	public void testAddAircraftModel() throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
//		
//		
//		AircraftModel am = new AircraftModel();
//		AircraftCabinCapacity aircraftCabinCapacity = new AircraftCabinCapacity();
//		aircraftCabinCapacity.setAircraftModelNumber("JJJ");
//		aircraftCabinCapacity.setCabinClassCode("I");
//		aircraftCabinCapacity.setInfantCapacity(12);
//		aircraftCabinCapacity.setSeatCapacity(23);
//		Set s = new HashSet();
//		s.add(aircraftCabinCapacity);
//		
//		am.setAircraftCabinCapacitys(s);
//		
//		am.setModelNumber("JJJ");
//		am.setStatus(Aircraft.STATUS_ACTIVE);
//		am.setDescription("SDFSDFS");
//		
//		dao.saveAircraft(am);
//
//	}
	
	
//	public void testSaveAircraft() throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
//			
//		assertNotNull(dao);
//		
//		Aircraft obj = new Aircraft();
//		
//		obj.setAircraftTailNumber("KLM");
//		
//		obj.setModelNumber("JYU");
//		obj.setSerialNumber("12313");
//		obj.setStatus(obj.STATUS_ACTIVE);
//		
//		dao.saveAircraft(obj);
//		
//		assertNull(dao.getAircraft(25));
//						
//	}
	
	
	
	
//	
//	public void testGetActiveAircrafts() throws Exception
//	{
//
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
//			List l = dao.getActiveAircraftModels();
//			assertTrue("No Records found", l.size()>0);
//	}
//	
//	public void testGetActiveAircraftModels() throws Exception
//	{
//
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
//			List l = dao.getActiveAircraftModels();
//			assertTrue("No Records found", l.size()>0);
//	}
//	
//	
//	
	public void testSaveAircraftModel() throws Exception
	{
			LookupService lookup = LookupServiceFactory.getInstance();
		
			AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
////			assertNotNull(dao);
////		
////			
////			AircraftModel aircraftModel = dao.getAircraftModel("KLM");
////			
////			
////			Set capacities = aircraftModel.getAircraftCabinCapacitys();
////			Iterator iterator = capacities.iterator();
////			AircraftCabinCapacity aircraftCabinCapacity;
////			while (iterator.hasNext()) {
////				aircraftCabinCapacity = (AircraftCabinCapacity) iterator.next();
////				
////				if (aircraftCabinCapacity.getAircraftModelNumber().equals("KLM")) {
////					
////				}
////				
////			}
//			
			
			AircraftModel obj = new AircraftModel();
			obj.setModelNumber("KLM");
			obj.setStatus(Aircraft.STATUS_ACTIVE);
			obj.setDescription("SDFSD");
			
			
			AircraftCabinCapacity c = new AircraftCabinCapacity();
			//c.setAcccId(11);
			//c.setAircraftModelNumber("JYU");
			
			c.setAircraftModelNumber("KLM");
			c.setCabinClassCode("1");
			c.setSeatCapacity(12);
			c.setInfantCapacity(12);
			//c.setVersion(-1);
		
			Set s = new HashSet();
			//s =  obj.getAircraftCcCapacitys();
		
			s.add(c);
			obj.setAircraftCabinCapacitys(s);
			
			
			AirCraftModelSeats oAirCraftModelSeats = new AirCraftModelSeats();
			oAirCraftModelSeats.setSeatID(1001);
			oAirCraftModelSeats.setModelNo("A320-211");
			oAirCraftModelSeats.setCabinClassCode("Y");
			oAirCraftModelSeats.setSeatCode("A1");
			oAirCraftModelSeats.setColGroupId(1);
			oAirCraftModelSeats.setRowGroupId(1);
			oAirCraftModelSeats.setRowId(2);
			oAirCraftModelSeats.setColId(1);
			oAirCraftModelSeats.setStatus("ACT");
			
			dao.saveAirCraftModelSeat(oAirCraftModelSeats);
			System.out.println("Saved");
			Collection col = dao.getAirCraftModelSeats("A320-211");
			System.out.println("Got Collection"+col.size());
			
			//dao.saveAircraftModel(obj);
			//assertNotNull(dao.getAircraftModel("KLM"));
			assertNotNull(col);
			
				
	}
//	
//	
//	public void testGetModelCapacities()throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
//		AircraftModel obj = dao.getAircraftModel("AAA");	
//		assertNotNull(obj);
//		System.out.println(obj.getAircraftCabinCapacitys());
//		
//	}
//	
//	
	/*public void testLoadAndUpdateAircraftModelOrCapacity() throws Exception{
	
		LookupService lookup = LookupServiceFactory.getInstance();
		
		AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
				
		assertNotNull(dao);
		
		AircraftModel obj = dao.getAircraftModel("KLM");
		
		String olddescription = obj.getDescription();
		String newdescription = "newdescription";
		
		obj.setDescription(newdescription);
		
		
		Set modelcapacities = obj.getAircraftCabinCapacitys();
		AircraftCabinCapacity acc = (AircraftCabinCapacity)modelcapacities.iterator().next();
		assertNotNull(acc);
		
		int oldcapacity = acc.getSeatCapacity();
		int newcapacity = 666;
		acc.setSeatCapacity(newcapacity);
		modelcapacities.add(acc);
		
		obj.setAircraftCabinCapacitys(modelcapacities);
		
		dao.saveAircraftModel(obj);
		
		AircraftModel n = new AircraftModel();
		n = dao.getAircraftModel("KLM");
		assertTrue("Not Updated", n.getDescription().equals(newdescription));
		assertNotNull(n);
		Set newacs = n.getAircraftCabinCapacitys();
		Iterator iterator = newacs.iterator();
		while(iterator.hasNext()){
			AircraftCabinCapacity aircraftCabinCapacity = (AircraftCabinCapacity) iterator.next();
			assertTrue("Not Updated", aircraftCabinCapacity.getSeatCapacity()==newcapacity);
		}
				
	}*/
	
/*	public void testLoadAndDeleteAircraftModel() throws Exception
	{
		LookupService lookup = LookupServiceFactory.getInstance();
		AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
		dao.removeAircraftModel("KLM");
		assertNull(dao.getAircraftModel("KLM"));
	}*/
	
//	
//	
//	public void testLoadAndUpdate() throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
//		
//		
//		
//		assertNotNull(dao);
//		
//		Aircraft obj = dao.getAircraft(21);
//		obj.setAircraftTailNumber("newsdf");
//		//obj.s
//		obj.setModelNumber("AAA");
//		obj.setSerialNumber("12313");
//		obj.setStatus(obj.STATUS_ACTIVE);
//		
//		
//		dao.saveAircraft(obj);
//		
//		assertNotSame("sdt", dao.getAircraft(21).getAircraftTailNumber());
//	}
//		
//	public void testGetAircraftModels () throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
//			
//		assertNotNull(dao);
//		System.out.println("<B>GETTING AIRCRAFT MODELS AND ITS CAPACITIES</B>");
//		
//		List l = dao.getModels();
//		assertNotNull(l);
//		assertTrue("No Records Found", l.size()>0);
//		
//		for(Iterator iter = l.iterator();iter.hasNext();)
//		{
//			aircraftmodel = (AircraftModel)iter.next();
//			assertNotNull(aircraftmodel);
////			System.out.println("**********************************************************");
////			System.out.println("Aircraft Model Number:" + aircraftmodel.getModelNumber() + "||| status:" + aircraftmodel.getStatus() + "|||| version:" + aircraftmodel.getVersion()) ;
//			Collection c = (Collection)aircraftmodel.getAircraftCabinCapacitys();
//			assertNotNull(c);
//			System.out.println("Capacities : " + c);
//		}
//		
//		
//	}
//	 
//	
//	
//	
//	public void testGetAircrafts() throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
//			
//		assertNotNull(dao);
//		
//		System.out.println("<B>GETTING LIST OF AIRCRAFTS </B>");
//		
//		List l = dao.getAircrafts();
//		
//		assertTrue("No Records Found", l.size()>0);		
//		
//		
//	}
//	
//	public void testGetAircraftsPaging() throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
//			
//		assertNotNull(dao);
//		
//		System.out.println("<B> GETTING AIRCRAFTS PAGING LIST </B>");
//		
//		Page p = (Page)dao.getAircrafts(1,3);
//		Collection l = p.getPageData();
//		assertTrue("No Records Found", l.size()>0);
//			 
//	}
//	
//	
//	
//	public void testGetCabinClassesForModel() throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
//			
//		assertNotNull(dao);
//		
//		List l =  dao.getCabinClasses("AAA");
//		assertTrue("no recs", l.size()>0);
//	
//	}
//	
//	public void testGetAllCabinClasses() throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
//			
//		assertNotNull(dao);
//		
//		List l =  dao.getAllCabinClasses();
//		assertTrue("no recs", l.size()>0);
//	
//	}
//	
//	public void testGetCabinClass() throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
//			
//		assertNotNull(dao);
//		
//		CabinClass c =  dao.getCabinClass("Y");
//		assertNotNull(c);
//	
//	}

	
	
		

	
	protected void tearDown()
	{	
	
	}

}
