/*
 * Created on Jul 4, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.isa.thinair.airmaster.core.remoting.ejb;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;




import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

import com.isa.thinair.platform.api.util.PlatformTestCase;


import com.isa.thinair.airmaster.api.service.CommonMasterBD;

import com.isa.thinair.airmaster.api.model.BusinessSystemParameter;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.commons.api.dto.Page;


/**
 * @author byorn
 *
 */
public class TestCommonMasterServiceRemote extends PlatformTestCase{
	protected void setUp() throws Exception{
		super.setUp();
	}
	
	public void testSaveCurrency() throws Exception{

		Currency c = new Currency();
		c.setCurrencyCode("FFF");
		c.setCurrencyDescriprion("JJJJJJJJJ");
		c.setStatus(c.STATUS_ACTIVE);
        //FIXME
		//c.setBaseExchangeRate(12);
		
		getCommonMasterService().saveCurrency(c);
		getCommonMasterService().deleteCurrency("FFF");
		assertNull(getCommonMasterService().getCurrency("FFF"));
	}

//	public void testConvertCurrency() throws Exception{
//
//		BigDecimal amount = getCommonMasterService().ConvertCurrency("AAA", new BigDecimal(100));
//		System.out.println("Converted Amount :" + amount);
//		assertEquals(1215,amount.doubleValue(), amount.doubleValue());
//	
//		BigDecimal amount1 = getCommonMasterService().ConvertCurrency("AAA","AED",new BigDecimal(100));
//		System.out.println("Converted Amount :" + amount1);
//	
//		
//	
//	}
	

	public void testLoadAndUpdateCurrency() throws Exception{

		Currency c = getCommonMasterService().getCurrency("JJJ");
		String oldval = c.getCurrencyDescriprion();
		String newval = "new val";
		c.setCurrencyDescriprion(newval);
		
		
		
		getCommonMasterService().saveCurrency(c);
	
		assertNotSame(oldval, getCommonMasterService().getCurrency("JJJ").getCurrencyDescriprion());
		
	}


	public void testGetNationality() throws Exception{

		Nationality n = new Nationality();
				
		
			n = getCommonMasterService().getNationaly(1);
			//System.out.println(n);
		
			// TODO Auto-generated catch block
	
		}
	
	public void testGetAllRouteInfo() throws Exception
	{
		Collection l = getCommonMasterService().getAllRouteInfos();
		assertTrue("No records", l.size()>0);
	}
	
	public void testSaveRouteInfo() throws Exception
	{
		
		RouteInfo obj = new RouteInfo();
		
		/*obj.setDistance(12.22);
		obj.setDuration(12);
		obj.setDurationTolerance(new BigDecimal(12.00));
		obj.setFromAirportCode("AAA");
		obj.setRouteCode("IOP");
		obj.setToAirportCode("CMB");
		obj.setStatus(obj.STATUS_ACTIVE);
		
		getCommonMasterService().saveRouteInfo(obj);
		
		assertNotNull(getCommonMasterService().getRouteInfo("AAA/CMB"));
	
		getCommonMasterService().removeRouteInfo("AAA/CMB");
		*/
	}
	
	public void testGetRoutesForID() throws Exception
	{

	
		RouteInfo mrs = getCommonMasterService().getRouteInfo("CMB/DOH");
		assertNotNull(mrs);
	
		
	
	}
	

	public void testGetModelRouteInfoForBetweenStaions() throws Exception
	{

	
		Collection  l = getCommonMasterService().getRouteInfo("CMB","DOH");
		assertTrue("No records", l.size()>0);
	
		
	
	}

	public void testGetNationalities() throws Exception {

		Nationality n = new Nationality();
				
	
			Collection l = getCommonMasterService().getNationalities();
			//System.out.println("Inside get NATIONALITIES");
			//System.out.println(l);
	
			// TODO Auto-generated catch block
	
	
	}

	public void testGetNationalitiesPaging() throws Exception {

		Nationality n = new Nationality();
				

			Page p = getCommonMasterService().getNationalities(1, 30);
			//System.out.println("Inside get NATIONALITIES paging");
			//System.out.println(l);

	}
		
	
	public void testGetBusinessSystemParameters() throws Exception {
		Collection col = getCommonMasterService().getBusinessSystemParameters();
		System.out.println("Collection is " + col.size());		
		assertTrue("No records", col.size() > 0);
	}
	
	public void testSaveBusinessSystemParameters() throws Exception {
		Collection col = new ArrayList();
		BusinessSystemParameter businessSystemParameter = new BusinessSystemParameter();
		businessSystemParameter.setParamKey("SALES_2");
		businessSystemParameter.setDescription("Payment Terms");
		businessSystemParameter.setEditable("Y");	
		businessSystemParameter.setParamValue("ww");
		businessSystemParameter.setVersion(Long.parseLong("2"));
		col.add(businessSystemParameter);
		businessSystemParameter = new BusinessSystemParameter();		
		businessSystemParameter.setParamKey("RES_4");
		businessSystemParameter.setDescription("On-hold Duration");
		businessSystemParameter.setEditable("Y");	
		businessSystemParameter.setParamValue("rr");
		businessSystemParameter.setVersion(Long.parseLong("1"));
		col.add(businessSystemParameter);
		getCommonMasterService().saveBusinessSystemParameters(col);
	}
	
	private CommonMasterBD getCommonMasterService(){
		LookupService lookup = LookupServiceFactory.getInstance();
		IModule airmasterModule = lookup.getModule("airmaster");
		System.out.println("Lookup successful...");
		CommonMasterBD delegate = null;
		try {
			delegate = (CommonMasterBD)airmasterModule.getServiceBD("commonmaster.service.remote");
			System.out.println("Delegate creation successful...");
			return delegate;
			
		} catch (Exception e) {
			System.out.println("Exception in creating delegate");
			e.printStackTrace();
		}
		return delegate;
	}
}
