/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airmaster.core.persistence.hibernate;


import com.isa.thinair.airmaster.api.criteria.StationSearchCriteria;
import com.isa.thinair.airmaster.api.model.Station;
import com.isa.thinair.airmaster.core.persistence.dao.StationDAO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Byorn
 *
 */

public class TestStationDAOImpl extends PlatformTestCase {

		
	
	public void testSaveStation() throws Exception
	{
		LookupService lookup = LookupServiceFactory.getInstance();
		
		StationDAO dao = (StationDAO) lookup.getBean("isa:base://modules/airmaster?id=StationDAOProxy");
			
		assertNotNull("could not lookup  dao", dao);
		
		Station obj = new Station();
		obj.setOnlineStatus(obj.ONLINE_STATUS_ACTIVE);
        obj.setRemarks("sdfds");
        obj.setStationCode("AA7");
        obj.setStationName("AA7 station");
        obj.setTerritoryCode("AA1");
        obj.setStatus(obj.STATUS_ACTIVE);
        dao.saveStation(obj);
        assertNotNull(dao.getStation("AA7"));        		
		dao.removeStation("AA7");
        assertNull(dao.getStation("AA7"));
						
	}
	
	
	public void testGetPageOfStations() throws Exception
	{

	    LookupService lookup = LookupServiceFactory.getInstance();
        
        StationDAO dao = (StationDAO) lookup.getBean("isa:base://modules/airmaster?id=StationDAOProxy");
		StationSearchCriteria criteria = new StationSearchCriteria();
        criteria.setCountryCode("aa");
        Page p = dao.getStations(criteria,0,10);
        assertTrue("No recs",p.getPageData().size()>0);
        assertEquals(10,p.getPageData().size());
        assertTrue(p.getTotalNoOfRecords()>10);
    }
	

	 
	
	
	

}
