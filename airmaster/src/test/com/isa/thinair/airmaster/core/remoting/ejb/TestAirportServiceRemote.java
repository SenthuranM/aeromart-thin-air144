/*
 * Created on Jul 4, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.isa.thinair.airmaster.core.remoting.ejb;


import java.util.GregorianCalendar;

import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.core.persistence.dao.AirportDAO;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.util.PlatformTestCase;


/**
 * @author byorn
 *
 */
public class TestAirportServiceRemote extends PlatformTestCase{
//	protected void setUp() throws Exception{
//		super.setUp();
//	}
  
	
//	public void testRecalculateLocalTime() throws Exception{
//		FlightBD flightBD = (FlightBD)AirmasterUtils.lookupServiceBD(AirschedulesConstants.MODULE_NAME, AirschedulesConstants.BDKeys.FLIGHT_SERVICE);
//		assertNotNull(flightBD);
//		
//		Airport airport = getAirportDAO().getAirport("DEL");
//		AirportDST airportDST = getAirportDAO().getDST(89);
//		DSTTO dstto = new DSTTO();
//		dstto.setDstChangeAmount(30);
//		GregorianCalendar calendar = new GregorianCalendar(2000, GregorianCalendar.JANUARY, 5);
//		GregorianCalendar calendar1 = new GregorianCalendar(2020, GregorianCalendar.MARCH, 5);
//		airportDST.setDstStartDateTime(calendar.getTime());
//		airportDST.setDstEndDateTime(calendar1.getTime());
//		dstto.setDstEffectiveFrom(calendar.getTime());
//		dstto.setDstEffectiveTo(calendar1.getTime());
//		List l = new ArrayList();
//		l.add(dstto);
//		
//		flightBD.recalculateLocalTime(airport, airportDST.getDstStartDateTime(), airportDST.getDstEndDateTime(), l, new FlightAlertDTO());
//	}
//	
//	
	public void testEditDsts() throws Exception{
		
		GregorianCalendar calendar = new GregorianCalendar(2006, GregorianCalendar.JANUARY, 1);
		GregorianCalendar calendar1 = new GregorianCalendar(2006, GregorianCalendar.MARCH, 11);
		
		AirportDST obj = getAirportDAO().getDST(3000);
		
		
		obj.setDstStartDateTime(calendar.getTime());
		obj.setDstEndDateTime(calendar1.getTime());
		
		
		try{
		getAirportService().addNewDST(obj, new FlightAlertDTO());
		
		AirportDST airportDST = getAirportDAO().getDST(3000);
	System.out.println("saving start date:" + airportDST.getDstStartDateTime());
	System.out.println("saving end date:" + airportDST.getDstEndDateTime());	
	assertTrue("start date not saved correctly", airportDST.getDstStartDateTime().equals(obj.getDstStartDateTime()));
		assertTrue("end date not saved correctly", airportDST.getDstEndDateTime().equals(obj.getDstEndDateTime()));
		}
		catch(ModuleException e)
		{
			System.out.println("exception code :" + e.getExceptionCode());
		}
		//obj.setDSTStatus(obj.CURRENT_DST);
		//assertNotNull(getAirportService()1);
		//getAirportService().deleteAirportDST("14", "UUU");
		
	}

	
	
//	public void testRecalculateAndRollBack() throws Exception{
//		
//		//GregorianCalendar calendar = new GregorianCalendar(2006, GregorianCalendar.JANUARY, 3);
//		//GregorianCalendar calendar1 = new GregorianCalendar(2006, GregorianCalendar.JANUARY, 21);
//		
//		AirportDST airportDST = getAirportDAO().getDST(3000);
//		FlightAlertDTO alertDTO = new FlightAlertDTO();
//		
//		//obj.setDSTStatus(obj.CURRENT_DST);
//		//assertNotNull(getAirportService()1);
//		
//		getAirportService().disableAndRollBack(airportDST, alertDTO);
//		
//		assertTrue(getAirportDAO().getDST(3000).getDSTStatus().equals(AirportDST.INACTIVE_DST));
//		//getAirportService().deleteAirportDST("14", "UUU");
//		
//	}	
	
//	public void testGetCurrentDST() throws Exception {
//
//		AirportDST airportDST = getAirportService().getCurrentAirportDST("AA2");
//		assertNotNull(airportDST);
//	}
//
//	public void testGetEffectiveDST() throws Exception {
//		
//		GregorianCalendar calendar = new GregorianCalendar(2006, GregorianCalendar.JANUARY, 12);
//		try{
//			AirportDST airportDST = getAirportService().getEffectiveAirportDST("AA1", calendar.getTime());	
//		}
//		catch(ModuleException e)
//		{
//			assertEquals("airmaster.dst.data.overlaps",e.getExceptionCode());
//		}
//		//assertNotNull(airportDST);
//		
//		
//
//	}
	
// public void testGetSITAaddressPageing() throws Exception{
//        
//        
//         SitaSearchCriteria criteria = new SitaSearchCriteria();
//        //criteria.setActiveStatus("Y");
//        //criteria.setSitaAddress("AAAA   ");
//        //criteria.setAirportId("AA1");
//        Page page = getAirportService().getSITAAddresses(criteria,0,10);
//        assertTrue("no recs", page.getPageData().size()>0);
//        
//        
//    }
//	public void testSaveSITAaddress() throws Exception{
//
//		
//		
//		SITAAddress address = new SITAAddress();
//		address.setActiveStatus(address.ACTIVE_STATUS_NO);
//		address.setAddressType(address.ADDRESS_TYPE_PRIMARY);
//		address.setSitaAddress("asfd");
//		//address.setSITAId(0);
//		address.setSitaLocation("byornd");
//		address.setAirportCode("AA3");
//		address.setStatus(address.STATUS_ACTIVE);
//		try{
//		getAirportService().saveSITAAddress(address);
//		}catch(ModuleException e)
//		{System.out.println(e.getExceptionCode());}
//		//dao.removeSITAAddress(1);
//		//assertNull(dao.getPrimarySITAAdress("AA1"));
//		
//	}
//	
//	
//	public void testGetActiveSITAs() throws Exception{
//		
//		assertTrue("no records", getAirportService().getActiveSITAAddresses("AA1").size()>0);
//	}
////	
//	public void testGetPrimarySITAAddress() throws Exception{
//		
//		assertNotNull(getAirportService().getPrimarySITAAddress("AA1"));
//	}
//	
//	
//	public void testSaveAirport() throws Exception{
//
//		Airport obj = new Airport();
//		
//		obj.setAirportCode("UU1");
//		obj.setAirportName("AAAAAAAAAA");
//		obj.setStatus(obj.STATUS_ACTIVE);
//		obj.setClosestAirport("AAA");
//		obj.setContact("sfsfsf");
//		obj.setFax("12112");
//		obj.setGmtOffsetAction("+");
//		obj.setGmtOffsetHours(1112);
//		obj.setLatitude("1212");
//		obj.setLatitudeNorthSouth(obj.LATITUDE_NORTH_SOUTH_N);
//		obj.setLongitude("1212");
//		obj.setLongitudeEastWest(obj.LONGITUDE_EAST_WEST_E);
//		obj.setMinConnectionTime(1121);
//		obj.setMinStopoverTime(3323);
//		obj.setOnlineStatus(obj.ONLINE_STATUS_ACTIVE);
//		obj.setRemarks("asfasfa");
//		obj.setTelephone("23423");
//		obj.setCountryCode("aa");
//		
//		getAirportService().saveAirport(obj);
//	
//	}


	
	
//	
//	public void testGetAirportDSTs() throws Exception{
//		
//		System.out.println("Getting DSTS for airport AAA");
//		Collection l = getAirportService().getAirportDSTs("AAA");
//		assertTrue("No records foount", l.size()>0);
//		
//		//System.out.println(l);
//	}
//	
//	public void testGetAirport() throws Exception{
//		
//		System.out.println("Getting Airport AAA ");
//		Airport s = getAirportService().getAirport("AAA");
//		assertNotNull(s);
//		//System.out.println(s);
//	}
//	
//	public void testGetPaging() throws Exception{
//		System.out.println("Getting Airports Paging ");
//		Page p= getAirportService().getAirports(1,20);
//		assertTrue("No records foound", p.getPageData().size()>0);
//		//System.out.println(l);
//	}
//	
//	public void testGetAirports() throws Exception{
//		System.out.println("Getting List of Airports");
//		Collection l = getAirportService().getAirports();
//		assertTrue("No records foound", l.size()>0);
//	}
//	
//	public void testGetActiveAirports() throws Exception{
//		System.out.println("Getting List of Airports");
//		Collection l = getAirportService().getActiveAirports();
//		assertTrue("No records foound", l.size()>0);
//	}
//	
//	public void testGetCurrentDST() throws Exception{
//		
//		AirportDST dst = getAirportService().getCurrentAirportDST("AAA");
//		assertNotNull(dst);
//	}
//	     
//	
//	public void testGetOfflineAirports() throws Exception{
//			
//		System.out.println("Getting Offline Airports");
//		Page p = getAirportService().getOfflineAirports(1,3);
//		//assertTrue("No records foound", l.size()>0);
//	}
//	
//	public void testGetOnlineAirports() throws Exception{
//		
//		System.out.println("Getting Online Airports");
//		Page p = getAirportService().getOnlineAirports(1,3);
//		//assertTrue("No records foound", l.size()>0);
//	}
//	
//
//	public void testGetAirportsForCountry() throws Exception{
//		
//		System.out.println("Getting Airports For country");
//		Collection l = getAirportService().getAirportsForCountry("aa");
//		assertTrue("No records foound", l.size()>0);
//	}
//	
//	public void testDeleteAirportDST() throws Exception{
//		
//		System.out.println("deleting Staiton dst");
//		getAirportService().deleteAirportDST("1", "AAA");
//		
//	}
	
	private AirportDAO getAirportDAO(){
		LookupService lookup = LookupServiceFactory.getInstance();
		return (AirportDAO) lookup.getBean("isa:base://modules/airmaster?id=AirportDAOProxy");
	}
	private AirportBD getAirportService(){
		LookupService lookup = LookupServiceFactory.getInstance();
		IModule airmasterModule = lookup.getModule("airmaster");
		System.out.println("Lookup successful...");
		AirportBD delegate = null;
		try {
			delegate = (AirportBD)airmasterModule.getServiceBD("airport.service.remote");
			
			System.out.println("Delegate creation successful...");
			return delegate;
			
		} catch (Exception e) {
			System.out.println("Exception in creating delegate");
			e.printStackTrace();
		}
		return delegate;
	}
}
