/*
 * ============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ============================================================================
 */
package com.isa.thinair.airmaster.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.core.controller.ModuleFramework;

import junit.framework.TestCase;

/**
 * @author Mohamed Nasly
 */
public class TestCurrencyBL extends TestCase {
	
	public TestCurrencyBL() {
		super();
	}

	public TestCurrencyBL(String arg0) {
		super(arg0);
	}
	
	protected void setUp(){
		// Initialize modules
		System.setProperty("repository.home", "c:/isaconfig-test");
		ModuleFramework.startup();
	}
	
//	public void testGetExchangeRates(){
//		List<Date> dates = new ArrayList<Date>();
//		
//		Calendar d1 = GregorianCalendar.getInstance();
//		d1.set(Calendar.YEAR, 2008);
//		d1.set(Calendar.MONTH, 11);
//		d1.set(Calendar.DAY_OF_MONTH, 30);
//		dates.add(d1.getTime());
//		
//		Calendar d2 = GregorianCalendar.getInstance();
//		d2.set(Calendar.YEAR, 2009);
//		d2.set(Calendar.MONTH, 0);
//		d2.set(Calendar.DAY_OF_MONTH, 31);
//		dates.add(d2.getTime());
//		
//		Map<Date, BigDecimal> exchangeRates = new CurrencyBL().getExchangeRateValues("AED", dates);
//		assertNotNull(exchangeRates);
//	}
	
	public void testGetExchangeRate() throws ModuleException{
		BigDecimal exRate = new CurrencyBL().getExchangeRateValue("AED", "LKR", new Date());
		assertNotNull(exRate);
		
		BigDecimal exRate2 = new CurrencyBL().getExchangeRateValue("LKR", "AED", new Date());
		assertNotNull(exRate2);
	}
	
	protected void tearDown(){	
	}
}
