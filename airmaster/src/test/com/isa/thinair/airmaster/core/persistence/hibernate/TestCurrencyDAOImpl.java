/*
 * ============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ============================================================================
 */
package com.isa.thinair.airmaster.core.persistence.hibernate;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.core.bl.AirmasterDAOUtil;
import com.isa.thinair.platform.core.controller.ModuleFramework;

/**
 * @author Mohamed Nasly
 */
public class TestCurrencyDAOImpl extends TestCase {

	private static String defaultCurrencyCode = "ABC";
	
	public TestCurrencyDAOImpl() {
		super();
	}

	public TestCurrencyDAOImpl(String arg0) {
		super(arg0);
	}
	
	protected void setUp(){
		// Initialize modules
		System.setProperty("repository.home", "c:/isaconfig-test");
		ModuleFramework.startup();
		
		createDefaultCurrency();
	}
	
	public void testGetCurrency(){
		System.out.println("Starting TestCurrencyDAOImpl.testSaveCurrency()");

		Currency c1 = AirmasterDAOUtil.getCurrencyDAO().getCurrency(defaultCurrencyCode);
		assertNotNull(c1);
		assertNotNull(c1.getExchangeRates());
	}
	
	public void testGetExchangeRate(){
		System.out.println("Starting TestCurrencyDAOImpl.testGetExchangeRate()");
		
		CurrencyExchangeRate currencyExchangeRate = AirmasterDAOUtil.getCurrencyDAO().getExchangeRate(defaultCurrencyCode, new Date());
		assertNotNull(currencyExchangeRate);
		assertNotNull(currencyExchangeRate.getCurrency());
		assertNotNull(currencyExchangeRate.getCurrency().getCurrencyDescriprion());
		assertNotNull(currencyExchangeRate.getCurrency().getStatus());
	}
	
	public void testGetExchangeRateValue(){
		System.out.println("Starting TestCurrencyDAOImpl.testGetExchangeRateValue()");
		
		BigDecimal exRateValue = AirmasterDAOUtil.getCurrencyDAO().getExchangeRateValue(defaultCurrencyCode, new Date());
		assertNotNull(exRateValue);
		assertEquals(exRateValue.doubleValue(), 3.67789, 0.0001);
	}
	
	private void removeDefaultCurrency(){
		AirmasterDAOUtil.getCurrencyDAO().removeCurrency(defaultCurrencyCode);
	}
	
	private void createDefaultCurrency(){
		Currency c = new Currency();
		c.setCurrencyCode(defaultCurrencyCode);
		c.setCurrencyDescriprion("ABC currency");
		c.setStatus("ACT");
		c.setBookVisibililty(0);
		c.setXBEVisibility(0);
		c.setIBEVisibility(0);
		c.setCardPaymentVisibility(0);
		c.setCreatedBy("SYSTEM");
		c.setCreatedDate(new Date());
		
		CurrencyExchangeRate cr = new CurrencyExchangeRate();
		cr.setCurrency(c);
		cr.setExchangeRate(new BigDecimal("3.67789"));
		cr.setStatus("ACT");
		Calendar cFrom = GregorianCalendar.getInstance();
		cFrom.set(Calendar.YEAR, 1970);
		cFrom.set(Calendar.MONTH, 0);
		cFrom.set(Calendar.DAY_OF_MONTH, 1);
		cr.setEffectiveFrom(cFrom.getTime());
		
		Calendar cTo = GregorianCalendar.getInstance();
		cTo.set(Calendar.YEAR, 2020);
		cTo.set(Calendar.MONTH, 11);
		cTo.set(Calendar.DAY_OF_MONTH, 31);
		cr.setEffectiveTo(cTo.getTime());
		cr.setCreatedBy("SYSTEM");
		cr.setCreatedDate(new Date());
		
		Set<CurrencyExchangeRate> crSet = new HashSet<CurrencyExchangeRate>();
		crSet.add(cr);
		c.setExchangeRates(crSet);
		
		AirmasterDAOUtil.getCurrencyDAO().saveCurrency(c);
	}

	protected void tearDown(){	
		removeDefaultCurrency();
	}
}

