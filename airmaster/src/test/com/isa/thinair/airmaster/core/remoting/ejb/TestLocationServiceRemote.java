/*
 * Created on Jul 4, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.isa.thinair.airmaster.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.util.PlatformTestCase;

import com.isa.thinair.airmaster.api.service.LocationBD;

import com.isa.thinair.airmaster.api.criteria.StationSearchCriteria;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.Station;
import com.isa.thinair.airmaster.api.model.Territory;
import com.isa.thinair.airmaster.core.persistence.dao.StationDAO;
import com.isa.thinair.commons.api.dto.Page;

/**
 * @author byorn
 *
 */
public class TestLocationServiceRemote extends PlatformTestCase{
	protected void setUp() throws Exception{
		super.setUp();
	}
	
  
    
/*    public void testSaveStation() throws Exception
    {
      
            
       
        
        Station obj = new Station();
        obj.setOnlineStatus(obj.ONLINE_STATUS_ACTIVE);
        obj.setRemarks("sdfds");
        obj.setStationCode("AA9");
        obj.setStationName("AA9 station");
        obj.setTerritoryCode("AA1");
        obj.setStatus(obj.STATUS_ACTIVE);
        getLocationService().saveStation(obj);
        
        LookupService lookup = LookupServiceFactory.getInstance();
        StationDAO dao = (StationDAO) lookup.getBean("isa:base://modules/airmaster?id=StationDAOProxy");
        assertNotNull(dao.getStation("AA9"));               
        
        getLocationService().removeStation("AA9");
        assertNull(dao.getStation("AA9"));
                        
    }*/
    
    
    public void testGetPageOfStations() throws Exception
    {

              
        StationSearchCriteria criteria = new StationSearchCriteria();
        criteria.setCountryCode("aa");
        Page p =  getLocationService().getStations(criteria,0,20);
        assertTrue("No recs",p.getPageData().size()>0);
        assertEquals(20,p.getPageData().size());
        assertTrue(p.getTotalNoOfRecords()>10);
    }
    
  
    
    
//    
//    public void testSaveCountry() throws Exception{
//
//		Country c = new Country();
//		c.setCountryCode("mm");
//		c.setCountryName("fsdsdfsdf");
//		c.setCurrencyCode("AAA");
//		c.setRemarks("sfsdfs");
//		c.setStatus(c.STATUS_ACTIVE);
//		
//		getLocationService().saveCountry(c);
//		getLocationService().deleteCountry("mm");
//		assertNull(getLocationService().getCountry("mm"));
//		
//		
//	}
//
//
//	public void testLoadAndUpdateCountry() throws Exception{
//
//		Country c = getLocationService().getCountry("aa");
//		String oldval = c.getCountryName();
//		String newval = "newname";
//		c.setCountryName(newval);
//		
//		getLocationService().saveCountry(c);
//		
//		assertNotSame(oldval,getLocationService().getCountry("aa").getCountryName());
//		
//		
//	}
//
//	
//	public void testSaveTerritory()throws Exception{
//
//		Territory t = new Territory();
//		t.setCountryCode("aa");
//		t.setTerritoryCode("JJJ");
//		t.setRemarks("asdfsdf");
//		t.setStatus(t.STATUS_ACTIVE);
//		
//		getLocationService().saveTerritory(t);
//		
//	}
//
//	
//	
//	public void testLoadAndUpdateTerritory() throws Exception{
//
//		Territory c = getLocationService().getTerritory("AAA");
//		String oldval = c.getRemarks();
//		String newval = "newremark";
//		c.setRemarks(newval);
//		
//		getLocationService().saveTerritory(c);
//		
//		assertNotSame(oldval,getLocationService().getTerritory("AAA").getRemarks());
//		
//		
//	}
//
//	
//	
//	public void testGetCountries() throws Exception
//	{
//		
//			Collection l = getLocationService().getCountries();
//			//System.out.println("inside get countries");
//			//System.out.println(l);
//			
//	
//	}
//	
//	public void testGetActiveCountries() throws Exception
//	{
//		
//			Collection l = getLocationService().getActiveCountries();
//			assertTrue("No records found", l.size()>0);
//	
//	}
//	
//	public void testGetActiveTerrotories() throws Exception
//	{
//		
//			Collection l = getLocationService().getActiveTerritories();
//			assertTrue("No records found", l.size()>0);
//	
//	}
//	
//	
//	public void testGetCountriesPaging() throws Exception
//	{
//			Page p = getLocationService().getCountries(1,10);
//			//System.out.println("inside get countries paging");
//			//System.out.println(l);
//			
//	
//	
//	}
//	
//	public void testGetTerritoryForCountry() throws Exception
//	{
//		
//			Collection l = getLocationService().getTerritoryForCountry("aa");
//			//System.out.println("inside get Terrirotyr for country aa");
//			//System.out.println(l);
//			
//	}
//	
//	public void testGetTerritories() throws Exception
//	{
//			Collection l = getLocationService().getTerritories();
//			//System.out.println("inside get Terrirotyries");
//			//System.out.println(l);
//			
//		
//	}
//	
//	public void testGetCountry() throws Exception
//	{
//		
//			Country c  = getLocationService().getCountry("aa");
//			//System.out.println("inside getCountry aa");
//			//System.out.println(c);
//			
//		
//	}
//	
//	
//	public void testGetTerritory() throws Exception
//	{
//			Territory  c  = getLocationService().getTerritory("AAA");
//			//System.out.println("inside getTerritotry");
//			//System.out.println(c);
//			
//		
//		
//	}
//	public void testGetTerritoriesPaging() throws Exception
//	{
//			Page p = getLocationService().getTerritories(1,10);
//			//System.out.println("inside get Terrirotyries Paging");
//			//.out.println(l);
//			
//	
//	
//	}
	
	private LocationBD getLocationService(){
		LookupService lookup = LookupServiceFactory.getInstance();
		IModule airmasterModule = lookup.getModule("airmaster");
		System.out.println("Lookup successful...");
		LocationBD delegate = null;
		try {
			delegate = (LocationBD)airmasterModule.getServiceBD("location.service.remote");
			System.out.println("Delegate creation successful...");
			return delegate;
			
		} catch (Exception e) {
			System.out.println("Exception in creating delegate");
			e.printStackTrace();
		}
		return delegate;
	}
}
