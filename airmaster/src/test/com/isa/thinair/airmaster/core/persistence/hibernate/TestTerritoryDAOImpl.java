/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airmaster.core.persistence.hibernate;

import java.util.Iterator;
import java.util.List;

import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

import com.isa.thinair.platform.core.controller.ModuleFramework;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.Territory;
import com.isa.thinair.airmaster.core.persistence.dao.CountryDAO;
import com.isa.thinair.airmaster.core.persistence.dao.TerritoryDAO;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.core.persistence.dao.CurrencyDAO;
import com.isa.thinair.commons.api.dto.Page;

import junit.framework.TestCase;

/**
 * @author byorn
 *
 */
public class TestTerritoryDAOImpl extends TestCase {

	Territory territory = null;
	public TestTerritoryDAOImpl() {
		super();
	}

	public TestTerritoryDAOImpl(String arg0) {
		super(arg0);
	}
	
	protected void setUp(){
		//initialize framework
		ModuleFramework.startup();
	}
	
	
	public void testSaveAndRemove() throws Exception{
		LookupService lookup = LookupServiceFactory.getInstance();
		TerritoryDAO dao = (TerritoryDAO) lookup.getBean("isa:base://modules/airmaster?id=TerritoryDAOProxy");
		//CountryDAO dao1 = (CountryDAO) lookup.getBean("isa:base://modules/airmaster?id=CountryDAOProxy");
		
			
		assertNotNull(dao);
		Territory obj = new Territory();
		
		//assertNotNull(dao1);
		
		
		obj.setTerritoryCode("YUI");
		obj.setDescription("Teritory1");
		obj.setRemarks("Sfsdfsdf");
		//obj.setCountryCode(dao1.getCountry("555").getCountryCode());
		//obj.setCountryCode("aa");
		obj.setStatus(obj.STATUS_ACTIVE);
		
		//obj.setCurrency()
		
		//dao.saveTerritory(obj);
		dao.removeTerritory("YYY");
		assertNull(dao.getTerritory("55"));
		
		//following should not throw an exception if deletion successful
		
	}
	
	
	public void testSave() throws Exception{
		LookupService lookup = LookupServiceFactory.getInstance();
		TerritoryDAO dao = (TerritoryDAO) lookup.getBean("isa:base://modules/airmaster?id=TerritoryDAOProxy");
		//CountryDAO dao1 = (CountryDAO) lookup.getBean("isa:base://modules/airmaster?id=CountryDAOProxy");
		
		
		
		assertNotNull(dao);
		Territory obj = new Territory();
		
		//assertNotNull(dao1);
		
		
		obj.setTerritoryCode("UIO");
		obj.setDescription("Teritory1");
		obj.setRemarks("Sfsdfsdf");
		//obj.setCountryCode(dao1.getCountry("555").getCountryCode());
		//obj.setCountryCode("aa");
		obj.setStatus(obj.STATUS_ACTIVE);
		//obj.setVersion(-1);
		//obj.setCurrency()
		
		dao.saveTerritory(obj);
		
		assertNotNull(dao.getTerritory("UIO"));
		
		//following should not throw an exception if deletion successful
		
	}
	
	public void testLoadAndUpdate()throws Exception{
		LookupService lookup = LookupServiceFactory.getInstance();
		TerritoryDAO dao = (TerritoryDAO) lookup.getBean("isa:base://modules/airmaster?id=TerritoryDAOProxy");
		//CountryDAO dao1 = (CountryDAO) lookup.getBean("isa:base://modules/airmaster?id=CountryDAOProxy");
		
		
		
		assertNotNull(dao);
		Territory obj = dao.getTerritory("AAA");
		
		String oldname = obj.getDescription();
		String newname = "new name";
		//assertNotNull(dao1);
		obj.setDescription(newname);
		
		dao.saveTerritory(obj);
		assertNotSame(oldname,dao.getTerritory("AAA").getDescription());
		//following should not throw an exception if deletion successful
		
	}
	
	
	
	public void testGetTerritories() throws Exception
	{
		LookupService lookup = LookupServiceFactory.getInstance();
		
		TerritoryDAO dao = (TerritoryDAO) lookup.getBean("isa:base://modules/airmaster?id=TerritoryDAOProxy");
			
		assertNotNull(dao);
		
		List l = dao.getTerritorys();
		assertTrue("no records found",l.size()>0);
		
		
		
	}
	
	public void testGetActiveTerritories() throws Exception
	{
		LookupService lookup = LookupServiceFactory.getInstance();
		
		TerritoryDAO dao = (TerritoryDAO) lookup.getBean("isa:base://modules/airmaster?id=TerritoryDAOProxy");
			
		assertNotNull(dao);
		
		List l = dao.getActiveTerritories();
		assertTrue("no records found",l.size()>0);
		
		
		
	}
	 
	public void testGetTerritoriesPaging() throws Exception
	{
		LookupService lookup = LookupServiceFactory.getInstance();
		
		TerritoryDAO dao = (TerritoryDAO) lookup.getBean("isa:base://modules/airmaster?id=TerritoryDAOProxy");
			
		assertNotNull(dao);
		
		Page p = dao.getTerritorys(1, 3);
		assertNotNull(p);
		System.out.println("Get Territories Paging");
//		for(Iterator iter = l.iterator();iter.hasNext();){
//	        territory = (Territory)iter.next();
//	        assertNotNull(territory);
//	        System.out.println("**********************************************************");
//	        System.out.println("code:" + territory.getTerritoryCode() + "||| name:" + territory.getDescription() + "Country Code:" + territory.getCountryCode() + "|||status: " + territory.getStatus() + "|||versions: " + territory.getVersion() );
//		}
		
		
	}
	
	public void testGetTerritory() throws Exception
	{
		LookupService lookup = LookupServiceFactory.getInstance();
		
		TerritoryDAO dao = (TerritoryDAO) lookup.getBean("isa:base://modules/airmaster?id=TerritoryDAOProxy");
			
		assertNotNull(dao);
		
		Territory n = dao.getTerritory("BBB");
		
		System.out.println("Get Territory for code BBB");
		System.out.println(n.toString());
		
	}
	
	public void testGetTerritorysForCountry() throws Exception
	{
		LookupService lookup = LookupServiceFactory.getInstance();
		
		TerritoryDAO dao = (TerritoryDAO) lookup.getBean("isa:base://modules/airmaster?id=TerritoryDAOProxy");
			
		assertNotNull(dao);
		
		List l = dao.getTerritorysForCountry("aa");
		
		System.out.println("Get Territory for Coutry aa");
		
		for(Iterator iter = l.iterator();iter.hasNext();){
	        territory = (Territory)iter.next();
	        assertNotNull(territory);
	        System.out.println("**********************************************************");
	       // System.out.println("code:" + territory.getTerritoryCode() + "||| name:" + territory.getDescription() + "Country Code:" + territory.getCountryCode() + "|||status: " + territory.getStatus() + "|||versions: " + territory.getVersion() );
		}
		
	}
	
	protected void tearDown(){	
	}

}
