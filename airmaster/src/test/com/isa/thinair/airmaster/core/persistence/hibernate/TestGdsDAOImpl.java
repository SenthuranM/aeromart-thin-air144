/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airmaster.core.persistence.hibernate;


import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import junit.framework.TestCase;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.airmaster.core.persistence.dao.GdsDAO;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.core.controller.ModuleFramework;

/**
 * @author Haider
 *
 */

public class TestGdsDAOImpl extends TestCase {

	Gds gds = null;
	
	public TestGdsDAOImpl() {
		super();
	}

	public TestGdsDAOImpl(String arg0) {
		super(arg0);
	}
	
	protected void setUp(){
		//initialize framework
		ModuleFramework.startup();
	}
	
	
/*	public void testSaveGds() throws Exception
	{
		LookupService lookup = LookupServiceFactory.getInstance();
		
		//GdsDAO dao = (GdsDAO) lookup.getBean("isa:base://modules/airmaster?id=GdsDAOProxy");
		GdsDAO dao = (GdsDAO)  AirmasterUtils.getInstance().getLocalBean("GdsDAOProxy");
			
		assertNotNull(dao);
		
		Gds obj = new Gds();
		obj.setGdsCode("Galilo");
		obj.setRemarks("Test");
		obj.setSchedPublishMechanismCode("BTH");
		obj.setDescription("Test");
		obj.setAvsAvailThreshold(1);
		obj.setAvsClosureThreshold(2);
		obj.setStatus("ACT");
		
		dao.saveGds(obj);
		
//		assertNull(dao.getAircraft(25));
						
	}*/
	
	
	
	
	
/*	public void testGetActiveGDSs() throws Exception
	{

		GdsDAO dao = (GdsDAO)  AirmasterUtils.getInstance().getLocalBean("GdsDAOProxy");

		List l = dao.getActiveGDSs();
		System.out.println("l= "+l);
		if(l!=null)
			assertTrue("No Records found", l.size()>0);
	}*/
	

	
	public void testLoadAndUpdate() throws Exception
	{
	
		GdsDAO dao = (GdsDAO)  AirmasterUtils.getInstance().getLocalBean("GdsDAOProxy");
		
	
		assertNotNull(dao);
		
		Gds obj = dao.getGds(4);
		obj.setAvsAvailThreshold(9);
		obj.setAvsClosureThreshold(4);
		obj.setRemarks("updated");
		dao.saveGds(obj);
	}
//		
//	public void testGetAircraftModels () throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
//			
//		assertNotNull(dao);
//		System.out.println("<B>GETTING AIRCRAFT MODELS AND ITS CAPACITIES</B>");
//		
//		List l = dao.getModels();
//		assertNotNull(l);
//		assertTrue("No Records Found", l.size()>0);
//		
//		for(Iterator iter = l.iterator();iter.hasNext();)
//		{
//			aircraftmodel = (AircraftModel)iter.next();
//			assertNotNull(aircraftmodel);
////			System.out.println("**********************************************************");
////			System.out.println("Aircraft Model Number:" + aircraftmodel.getModelNumber() + "||| status:" + aircraftmodel.getStatus() + "|||| version:" + aircraftmodel.getVersion()) ;
//			Collection c = (Collection)aircraftmodel.getAircraftCabinCapacitys();
//			assertNotNull(c);
//			System.out.println("Capacities : " + c);
//		}
//		
//		
//	}
//	 
//	
//	
//	
//	public void testGetAircrafts() throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
//			
//		assertNotNull(dao);
//		
//		System.out.println("<B>GETTING LIST OF AIRCRAFTS </B>");
//		
//		List l = dao.getAircrafts();
//		
//		assertTrue("No Records Found", l.size()>0);		
//		
//		
//	}
//	
//	public void testGetAircraftsPaging() throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
//			
//		assertNotNull(dao);
//		
//		System.out.println("<B> GETTING AIRCRAFTS PAGING LIST </B>");
//		
//		Page p = (Page)dao.getAircrafts(1,3);
//		Collection l = p.getPageData();
//		assertTrue("No Records Found", l.size()>0);
//			 
//	}
//	
//	
//	
//	public void testGetCabinClassesForModel() throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
//			
//		assertNotNull(dao);
//		
//		List l =  dao.getCabinClasses("AAA");
//		assertTrue("no recs", l.size()>0);
//	
//	}
//	
//	public void testGetAllCabinClasses() throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
//			
//		assertNotNull(dao);
//		
//		List l =  dao.getAllCabinClasses();
//		assertTrue("no recs", l.size()>0);
//	
//	}
//	
//	public void testGetCabinClass() throws Exception
//	{
//		LookupService lookup = LookupServiceFactory.getInstance();
//		
//		AircraftDAO dao = (AircraftDAO) lookup.getBean("isa:base://modules/airmaster?id=AircraftDAOProxy");
//			
//		assertNotNull(dao);
//		
//		CabinClass c =  dao.getCabinClass("Y");
//		assertNotNull(c);
//	
//	}

	
	
		

	
	protected void tearDown()
	{	
	
	}

}
