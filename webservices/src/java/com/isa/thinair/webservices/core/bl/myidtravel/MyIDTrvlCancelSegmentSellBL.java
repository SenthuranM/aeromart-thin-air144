/**
 * 
 */
package com.isa.thinair.webservices.core.bl.myidtravel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.OTACancelRS;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.util.ExceptionUtil;
import com.isa.thinair.webservices.core.util.MyIDTrvlCommonUtil;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterCancelSegmentSellRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterCancelSegmentSellResponse;

/**
 * @author Janaka Padukka
 * 
 */
public class MyIDTrvlCancelSegmentSellBL {

	private final static Log log = LogFactory.getLog(MyIDTrvlCancelSegmentSellBL.class);

	public static MyIDTravelResAdapterCancelSegmentSellResponse execute(
			MyIDTravelResAdapterCancelSegmentSellRequest myIDCanSegSellReq) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN cancelSegmentSell(MyIDTravelResAdapterSegmentSellRequest)");
		}

		MyIDTravelResAdapterCancelSegmentSellResponse myIDCanSegSellRes = null;
		try {
			myIDCanSegSellRes = cancelSegmentSell(myIDCanSegSellReq);
		} catch (Exception ex) {
			log.error("cancelSegmentSell(MyIDTravelResAdapterSegmentSellRequest) failed.", ex);
			if (myIDCanSegSellRes == null) {
				myIDCanSegSellRes = new MyIDTravelResAdapterCancelSegmentSellResponse();
				OTACancelRS otaCancelRS = new OTACancelRS();
				otaCancelRS.setEchoToken(myIDCanSegSellReq.getOTACancelRQ().getEchoToken());
				myIDCanSegSellRes.setOTACancelRS(otaCancelRS);
			}
			if (myIDCanSegSellRes.getOTACancelRS().getErrors() == null)
				myIDCanSegSellRes.getOTACancelRS().setErrors(new ErrorsType());
			ExceptionUtil.addOTAErrrorsAndWarnings(myIDCanSegSellRes.getOTACancelRS().getErrors().getError(), null, ex);
		}
		if (log.isDebugEnabled())
			log.debug("END cancelSegmentSell(MyIDTravelResAdapterSegmentSellRequest)");
		return myIDCanSegSellRes;
	}

	private static MyIDTravelResAdapterCancelSegmentSellResponse cancelSegmentSell(
			MyIDTravelResAdapterCancelSegmentSellRequest myIDCanSegSellReq) throws WebservicesException,
			ModuleException {

		MyIDTravelResAdapterCancelSegmentSellResponse myIDCanSegSellRes = new MyIDTravelResAdapterCancelSegmentSellResponse();
		myIDCanSegSellRes.setEmployeeData(myIDCanSegSellReq.getEmployeeData());
		OTACancelRS otaCancelRS = MyIDTrvlCommonUtil.cancelBooking(myIDCanSegSellReq.getOTACancelRQ());
		myIDCanSegSellRes.setOTACancelRS(otaCancelRS);
		return myIDCanSegSellRes;

	}
}
