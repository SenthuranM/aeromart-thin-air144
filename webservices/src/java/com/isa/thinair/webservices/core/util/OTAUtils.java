package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AAAuditType;
import org.opentravel.ota._2003._05.AAPNRAuditMessageType;
import org.opentravel.ota._2003._05.AAPNRAuditMessagesType;
import org.opentravel.ota._2003._05.AirFeeType;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.AirReservationType;
import org.opentravel.ota._2003._05.AirTaxType;
import org.opentravel.ota._2003._05.AirTravelerType;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.CabinType;
import org.opentravel.ota._2003._05.FareType;
import org.opentravel.ota._2003._05.FareType.BaseFare;
import org.opentravel.ota._2003._05.FareType.Fees;
import org.opentravel.ota._2003._05.FareType.Taxes;
import org.opentravel.ota._2003._05.FareType.TotalFare;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.PTCFareBreakdownType;
import org.opentravel.ota._2003._05.PTCFareBreakdownType.FareBasisCodes;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.PaymentCardType;
import org.opentravel.ota._2003._05.PaymentDetailType;

import com.isa.thinair.aircustomer.api.dto.lms.LoyaltyMemberCoreDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.auditor.api.dto.ReservationModificationDTO;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.BookingCategory;
import com.isa.thinair.commons.api.constants.CommonsConstants.TempPaymentConstants;
import com.isa.thinair.commons.api.dto.BaseContactConfigDTO;
import com.isa.thinair.commons.api.dto.BasePaxConfigDTO;
import com.isa.thinair.commons.api.dto.PaxCategoryFoidTO;
import com.isa.thinair.commons.api.dto.PaxCountryConfigDTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.WSContactConfigDTO;
import com.isa.thinair.commons.api.dto.WSPaxConfigDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.util.LoyaltyNameValidator;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CreditCardUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webservices.api.dtos.ExternalReferenceValidationDTO;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.AAUtils;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.interlineUtil.ReservationQueryInterlineUtil;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;
import com.isa.thinair.webservices.core.util.ThruReflection.MethodsToInvokeSequence;
import com.isa.thinair.wsclient.api.util.LMSConstants;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirTravelerType;

/**
 * Common utilities for processing OTA messages
 * 
 * @author Mehdi Raza
 * @author Webservices Team
 * 
 */
public class OTAUtils {

	private static Log log = LogFactory.getLog(OTAUtils.class);

	/**
	 * Sets amount, currencyCode and decimalPlaces in currency value.
	 * 
	 * @param object
	 * @param amount
	 */
	public static void setAmountAndDefaultCurrency(Object object, BigDecimal amount) {
		setAmountAndCurrency(object, amount, WebServicesModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.BASE_CURRENCY));
	}

	public static void setAmountAndCurrency(Object object, BigDecimal amount, String currencyCode) {
		MethodsToInvokeSequence methods = new MethodsToInvokeSequence();
		methods.put("setAmount", CommonUtil.getBigDecimalWithDefaultPrecision(amount));
		methods.put("setCurrencyCode", currencyCode);
		methods.put("setDecimalPlaces", WebservicesConstants.OTAConstants.DECIMALS_AED);
		try {
			ThruReflection.invoke(object, methods);
		} catch (Exception e) {
			log.error(e);
		}
	}

	public static AirReservationType convert(AirReservation convert) {
		AirReservationType reservation = new AirReservationType();
		reservation.setAirItinerary(convert.getAirItinerary());
		reservation.setFulfillment(convert.getFulfillment());
		reservation.setLastModified(convert.getLastModified());
		reservation.setPriceInfo(convert.getPriceInfo());
		reservation.setQueues(convert.getQueues());
		reservation.getTicketing().addAll(convert.getTicketing());
		reservation.setTPAExtensions(convert.getTPAExtensions());
		reservation.setTravelerInfo(convert.getTravelerInfo());
		reservation.getBookingReferenceID().addAll(convert.getBookingReferenceID());
		return reservation;
	}

	/**
	 * Prepares traveler RPH from AA data.
	 * 
	 * @param reservationPaxType
	 * @param pnrPaxId
	 * @param paxSequence
	 * @param parentPaxSequence
	 * @return
	 */
	public static String prepareTravelerRPH(String reservationPaxType, Integer pnrPaxId, Integer paxSequence,
			Integer parentPaxSequence) {
		String travelerRPH = "";
		if (ReservationInternalConstants.PassengerType.ADULT.equals(reservationPaxType)) {
			travelerRPH = "A" + paxSequence + "$" + pnrPaxId;
		} else if (ReservationInternalConstants.PassengerType.CHILD.equals(reservationPaxType)) {
			travelerRPH = "C" + paxSequence + "$" + pnrPaxId;
		} else if (ReservationInternalConstants.PassengerType.INFANT.equals(reservationPaxType)) {
			travelerRPH = "I" + paxSequence + "/A" + parentPaxSequence + "$" + pnrPaxId;
		}
		return travelerRPH;
	}

	/**
	 * Method to get the pnrPaxId from the traveller RPH.
	 * 
	 * @param rph
	 * @return
	 */
	public static String getPNRPaxIdFromTravellerRPH(String rph) {
		String pnrPaxId = null;
		if ((rph != null) && (!rph.trim().equals("")) && (rph.indexOf("$") != -1)) {
			pnrPaxId = rph.substring(rph.indexOf("$") + 1);
		}
		return pnrPaxId;
	}

	/**
	 * Returns corresponding WS paxTypeCode for AA paxTypeCode.
	 * 
	 * @param aaPaxTypeCode
	 * @return
	 */
	public static String getWSPaxTypeCode(String aaPaxTypeCode) {
		String wsPaxType = null;
		if (ReservationInternalConstants.PassengerType.ADULT.equals(aaPaxTypeCode)) {
			wsPaxType = CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT);
		} else if (ReservationInternalConstants.PassengerType.CHILD.equals(aaPaxTypeCode)) {
			wsPaxType = CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD);
		} else if (ReservationInternalConstants.PassengerType.INFANT.equals(aaPaxTypeCode)) {
			wsPaxType = CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT);
		}
		return wsPaxType;
	}

	/**
	 * Returns corresponding AA paxTypeCode for WS paxTypeCode.
	 * 
	 * @param wsPaxTypeCode
	 * @return
	 */
	public static String getAAPaxTypeCode(String wsPaxTypeCode) {
		String aaPaxType = null;

		if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT).equals(wsPaxTypeCode)) {
			aaPaxType = ReservationInternalConstants.PassengerType.ADULT;
		} else if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD).equals(wsPaxTypeCode)) {
			aaPaxType = ReservationInternalConstants.PassengerType.CHILD;
		} else if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT).equals(wsPaxTypeCode)) {
			aaPaxType = ReservationInternalConstants.PassengerType.INFANT;
		}

		return aaPaxType;
	}

	/**
	 * Extract AA payment details from ota paymentDetails.
	 * 
	 * @param paymentDetails
	 * @param reservationPayments
	 * @param selectedCurrencyCode
	 * @param exchangeRateProxy
	 * @throws WebservicesException
	 * @throws NumberFormatException
	 * @throws ModuleException
	 */
	public static void extractReservationPayments(List<PaymentDetailType> paymentDetails, IPayment reservationPayments,
			String selectedCurrencyCode, ExchangeRateProxy exchangeRateProxy) throws WebservicesException, NumberFormatException,
			ModuleException {
		if (paymentDetails != null && paymentDetails.size() > 0) {

			for (PaymentDetailType paymentDetail : paymentDetails) {
				BigDecimal paymentAmount = paymentDetail.getPaymentAmount().getAmount();
				if (paymentDetail.getDirectBill() != null) {
					String WSPaymentMethod = paymentDetail.getDirectBill().getCompanyName().getCodeContext();
					String agentCode = paymentDetail.getDirectBill().getCompanyName().getCode();
					if (agentCode != null && agentCode.length() > 0) {
						agentCode = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode() + agentCode;
					}
					String paymentMethod = null;
					if (Agent.PAYMENT_MODE_BSP.equals(WSPaymentMethod)) {
						paymentMethod = Agent.PAYMENT_MODE_BSP;
					} else if (WSPaymentMethod != null) {
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
								"Payment type " + WSPaymentMethod + " not supported");
					}
					reservationPayments.addAgentCreditPayment(agentCode, paymentAmount, null,
							WSReservationUtil.getPayCurrencyDTO(agentCode, exchangeRateProxy), null, paymentMethod, null);
				} else if (paymentDetail.getVoucher() != null) {// Payment by passenger credit
					// String debitPaxId = paymentDetail.getVoucher().getSeriesCode();
					// reservationPayments.addCreditPayment(paymentAmount, debitPaxId, null);
					// TODO implement support for other payment types
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
							"Passenger credit payment not supported");
				} else if (paymentDetail.getCash() != null) {// Payment by cash
					// TODO implement support for other payment types
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
							"Cash payment not supported");
				} else if (paymentDetail.getPaymentCard() != null) {// Payment by credit card
					PaymentCardType otaPaymentCard = paymentDetail.getPaymentCard();

					validateCreditCardPaymentDetails(otaPaymentCard);
					validateCreditCardNumber(otaPaymentCard);

					Integer ipgId = null;
					if (isShowPriceInselectedCurrency()) {
						if (isPreferredCurrencyCodeSupportedByPC(paymentDetail.getPaymentAmount().getCurrencyCode())) {
							selectedCurrencyCode = paymentDetail.getPaymentAmount().getCurrencyCode();
							CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy
									.getCurrencyExchangeRate(selectedCurrencyCode);
							if (currencyExchangeRate != null) {
								Currency currency = currencyExchangeRate.getCurrency();
								if (currency.getStatus().equals(Currency.STATUS_ACTIVE) && currency.getXBEVisibility() == 1) {
									if (currency.getCardPaymentVisibility() == 1 && currency.getDefaultXbePGId() != null) {
										ipgId = currency.getDefaultXbePGId();
									}
								}
							}
						} else if (selectedCurrencyCode.equals(paymentDetail.getPaymentAmount().getCurrencyCode())) {
							ipgId = getDefaultIPGForWSChannel();
						} else {
							// TODO Add new error type if needed
							throw new WebservicesException(
									IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
									"Preferred currency code is not supported by Payment gateway.");
						}
					}
					if (ipgId == null) {
						ipgId = getDefaultIPGForWSChannel();
					}
					PayCurrencyDTO payCurrencyDTO = ReservationUtil.getPayCurrencyDTOForCCPay(selectedCurrencyCode,
							exchangeRateProxy, ipgId);
					IPGIdentificationParamsDTO ipgIdentificationParamsDTO = ReservationUtil.getPaymentGatewayParams(
							selectedCurrencyCode, ipgId, payCurrencyDTO);

					reservationPayments.addCardPayment(
							Integer.parseInt(otaPaymentCard.getCardType()),
							StringUtils.substring(otaPaymentCard.getExpireDate(), 2, 4)
									+ StringUtils.substring(otaPaymentCard.getExpireDate(), 0, 2),
							otaPaymentCard.getCardNumber(), otaPaymentCard.getCardHolderName(), "", // card holder
																									// address
							otaPaymentCard.getSeriesCode(), paymentDetail.getPaymentAmount().getAmount(), // amount
							AppIndicatorEnum.APP_XBE, TnxModeEnum.MAIL_TP_ORDER, null, // old card details (for refund
																						// only)
							ipgIdentificationParamsDTO, null, payCurrencyDTO, null, null, null, null);
				}
			}
		}
	}

	/**
	 * Extract AA payment details from ota paymentDetails.
	 * 
	 * @param paymentDetails
	 * @param reservationPayments
	 * @param selectedCurrencyCode
	 * @param exchangeRateProxy
	 * @throws WebservicesException
	 * @throws NumberFormatException
	 * @throws ModuleException
	 */
	public static void extractReservationPayments(List<PaymentDetailType> paymentDetails,
			LCCClientPaymentAssembler reservationPayments, String selectedCurrencyCode, ExchangeRateProxy exchangeRateProxy)
			throws WebservicesException, NumberFormatException, ModuleException {
		if (paymentDetails != null && paymentDetails.size() > 0) {

			for (PaymentDetailType paymentDetail : paymentDetails) {
				BigDecimal paymentAmount = paymentDetail.getPaymentAmount().getAmount();
				if (paymentDetail.getDirectBill() != null) {// Payment by agent on account or BSP
					String paymentMethod = null;
					String agentCode = paymentDetail.getDirectBill().getCompanyName().getCode();
					String WSPaymentMethod = paymentDetail.getDirectBill().getCompanyName().getCodeContext();
					if (agentCode != null && agentCode.length() > 0) {
						agentCode = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode() + agentCode;
					}
					if (Agent.PAYMENT_MODE_BSP.equals(WSPaymentMethod)) {
						paymentMethod = Agent.PAYMENT_MODE_BSP;
					} else if (WSPaymentMethod != null) {
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
								"Payment type " + WSPaymentMethod + " not supported");
					} else {
						paymentMethod = Agent.PAYMENT_MODE_ONACCOUNT;
					}
					reservationPayments.addAgentCreditPayment(agentCode, paymentAmount,
							WSReservationUtil.getPayCurrencyDTO(agentCode, exchangeRateProxy),
							exchangeRateProxy.getExecutionDate(), null, null, paymentMethod, null);
				} else if (paymentDetail.getVoucher() != null) {// Payment by passenger credit
					// String debitPaxId = paymentDetail.getVoucher().getSeriesCode();
					// reservationPayments.addCreditPayment(paymentAmount, debitPaxId, null);
					// TODO implement support for other payment types
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
							"Passenger credit payment not supported");
				} else if (paymentDetail.getCash() != null) {// Payment by cash
					// TODO implement support for other payment types
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
							"Cash payment not supported");
				} else if (paymentDetail.getPaymentCard() != null) {// Payment by credit card
					PaymentCardType otaPaymentCard = paymentDetail.getPaymentCard();

					validateCreditCardPaymentDetails(otaPaymentCard);
					validateCreditCardNumber(otaPaymentCard);

					Integer ipgId = null;
					if (isShowPriceInselectedCurrency()) {
						if (isPreferredCurrencyCodeSupportedByPC(paymentDetail.getPaymentAmount().getCurrencyCode())) {
							selectedCurrencyCode = paymentDetail.getPaymentAmount().getCurrencyCode();
							CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy
									.getCurrencyExchangeRate(selectedCurrencyCode);
							if (currencyExchangeRate != null) {
								Currency currency = currencyExchangeRate.getCurrency();
								if (currency.getStatus().equals(Currency.STATUS_ACTIVE) && currency.getXBEVisibility() == 1) {
									if (currency.getCardPaymentVisibility() == 1 && currency.getDefaultXbePGId() != null) {
										ipgId = currency.getDefaultXbePGId();
									}
								}
							}
						} else if (selectedCurrencyCode.equals(paymentDetail.getPaymentAmount().getCurrencyCode())) {
							ipgId = getDefaultIPGForWSChannel();
						} else {
							// TODO Add new error type if needed
							throw new WebservicesException(
									IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
									"Preferred currency code is not supported by Payment gateway.");
						}
					}
					if (ipgId == null) {
						ipgId = getDefaultIPGForWSChannel();
					}
					PayCurrencyDTO payCurrencyDTO = ReservationUtil.getPayCurrencyDTOForCCPay(selectedCurrencyCode,
							exchangeRateProxy, ipgId);
					IPGIdentificationParamsDTO ipgIdentificationParamsDTO = ReservationUtil.getPaymentGatewayParams(
							selectedCurrencyCode, ipgId, payCurrencyDTO);

					reservationPayments.addInternalCardPayment(
							Integer.parseInt(otaPaymentCard.getCardType()),
							StringUtils.substring(otaPaymentCard.getExpireDate(), 2, 4)
									+ StringUtils.substring(otaPaymentCard.getExpireDate(), 0, 2),
							otaPaymentCard.getCardNumber(), otaPaymentCard.getCardHolderName(), "", // card holder
																									// address
							otaPaymentCard.getSeriesCode(), paymentDetail.getPaymentAmount().getAmount(), // amount
							AppIndicatorEnum.APP_WS, TnxModeEnum.MAIL_TP_ORDER, // old card details (for refund
																				// only)
							ipgIdentificationParamsDTO, payCurrencyDTO, null, null, null, null, null, null);
				}
			}
		}
	}

	public static boolean isPreferredCurrencyCodeSupportedByPC(String preferredCurrencyCode) {
		try {
			return WebServicesModuleUtils.getPaymentBrokerBD().isPaymentGatewaySupportedCurrency(preferredCurrencyCode,
					ApplicationEngine.XBE);
		} catch (Exception ex) {
			return false;
		}
	}

	public static boolean isShowPriceInselectedCurrency() {
		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();
		try {
			return WebServicesModuleUtils.getTravelAgentBD().getShowPriceInSelectedCur(userPrincipal.getAgentCode());
		} catch (Exception ex) {
			return false;
		}
	}

	public static boolean isPreferredCurrencyCodeDefined(String preferredCurrencyCode) {
		try {
			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(preferredCurrencyCode);

			if (currencyExchangeRate != null) {
				Currency currency = currencyExchangeRate.getCurrency();
				if (currency.getStatus().equals(Currency.STATUS_ACTIVE) && currency.getXBEVisibility() == 1) {
					return true;
				}
			}
			return false;
		} catch (Exception ex) {
			return false;
		}
	}
	
	public static void validateExternalReference(ExternalReferenceValidationDTO externalReferenceValidationDTO) throws Exception {
		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();
		ReservationQueryInterlineUtil interlineUtil = new ReservationQueryInterlineUtil();
		int salesChannel = userPrincipal.getSalesChannel();

		String externalReference = externalReferenceValidationDTO.getDirectBillId();
		BigDecimal totalAmount = externalReferenceValidationDTO.getTotalAmount();
		boolean isPaymentExist = externalReferenceValidationDTO.isPaymentExist();
		List<PaymentDetailType> paymentDetails = externalReferenceValidationDTO.getPaymentDetails();

		if (salesChannel == SalesChannelsUtil.SALES_CHANNEL_GOQUO) {
			if (externalReference != null && !externalReference.isEmpty()) {
				ServiceResponce response = interlineUtil.isExternalReferenceExist(externalReference,
						TempPaymentConstants.RESERVATION_SUCCESS, TempPaymentConstants.PRODUCT_TYPE_GOQUO);
				if (response != null && !response.isSuccess()) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_BOOKING_REFERENCE_NOT_FOUND_,
							"ExternalReference is invalid");
				} else if (response != null && !isValidPaymentMethod(paymentDetails, salesChannel)) {
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
							"Payment type is not supported for sales channel: " + salesChannel);
				} else if (response != null && response.getResponseParam(TempPaymentConstants.TOTAL_AMOUNT) != null
						&& response.getResponseParam(TempPaymentConstants.EXTERNAL_REFERENCE) != null) {
					BigDecimal paidAmount = (BigDecimal) response.getResponseParam(TempPaymentConstants.TOTAL_AMOUNT);
					int res = totalAmount.compareTo(paidAmount);

					// GoQuo as SSR support onwards amounts should be equal
					if (AppSysParamsUtil.isEnabledGoQuoServiceAsSSR()) {
						if (res != 0) {
							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
									"Aeromart Pay Amount Should Be Equal with the Price Quoted Value.");
						}
					} else if (res == 1) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
								"Aeromart Pay Amount Should Be Higher Than Price Quoted Value.");
					}
					boolean isExternalReferenceAvailable = (boolean) response
							.getResponseParam(TempPaymentConstants.EXTERNAL_REFERENCE);
					if (isExternalReferenceAvailable) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_RESERVATION_ALREADY_EXISTS_,
								"Aeromart Pay Reference Allready Exists.");
					}
				}

			} else if (isPaymentExist) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_BOOKING_REFERENCE_NOT_FOUND_,
						"ExternalReference is invalid");
			}

		}
	}
	
	private static boolean isValidPaymentMethod(List<PaymentDetailType> paymentDetails, int salesChannel)
			throws WebservicesException, NumberFormatException, ModuleException {
		boolean isValidPaymentMethod = true;

		Map<Integer, List<String>> salesChannelWiseSupportedPaymentMethods = getSalesChannelWiseSupportedPaymentMethods();
		
		List<String> paymentMethodsUsed = getPaymentMethodsUsed(paymentDetails);
		
		List<String> supportedPaymentMethodsForChannel = salesChannelWiseSupportedPaymentMethods.get(salesChannel);
		if (supportedPaymentMethodsForChannel != null && !supportedPaymentMethodsForChannel.isEmpty()) {
			for (String paymentMethod : paymentMethodsUsed) {
				if (!supportedPaymentMethodsForChannel.contains(paymentMethod)) {
					isValidPaymentMethod = false;
					break;
				}
			}
		}

		return isValidPaymentMethod;
	}
	
	/*
	 * TODO make salesChannelWiseSupportedPaymentMethods as configurable xml, currently for goQuo only onaccount payment
	 * enabled for ws pnr
	 */
	private static Map<Integer, List<String>> getSalesChannelWiseSupportedPaymentMethods() {
		Map<Integer, List<String>> salesChannelWiseSupportedPaymentMethods = new HashMap<Integer, List<String>>();
		List<String> externalAgentGoquo = new ArrayList<String>();
		externalAgentGoquo.add(Agent.PAYMENT_MODE_ONACCOUNT);
		salesChannelWiseSupportedPaymentMethods.put(SalesChannelsUtil.SALES_CHANNEL_GOQUO, externalAgentGoquo);

		return salesChannelWiseSupportedPaymentMethods;
	}
	
	private static List<String> getPaymentMethodsUsed(List<PaymentDetailType> paymentDetails){
		List<String> paymentMethodsUsed = new ArrayList<String>();
		if (paymentDetails != null && paymentDetails.size() > 0) {
			String paymentMethod = null;
			for (PaymentDetailType paymentDetail : paymentDetails) {
				if (paymentDetail.getDirectBill() != null) {
					String WSPaymentMethod = paymentDetail.getDirectBill().getCompanyName().getCodeContext();
					if (Agent.PAYMENT_MODE_BSP.equals(WSPaymentMethod)) {
						paymentMethod = Agent.PAYMENT_MODE_BSP;
					} else if (WSPaymentMethod != null) {
						paymentMethod = Agent.PAYMENT_MODE_NOT_SUPPORTED;
					} else {
						paymentMethod = Agent.PAYMENT_MODE_ONACCOUNT;
					}
				} else if (paymentDetail.getVoucher() != null) {
					paymentMethod = Agent.PAYMENT_MODE_VOUCHER;
				} else if (paymentDetail.getCash() != null) {
					paymentMethod = Agent.PAYMENT_MODE_CASH;
				} else if (paymentDetail.getPaymentCard() != null) {
					paymentMethod = Agent.PAYMENT_MODE_CREDITCARD;
				} else {
					paymentMethod = Agent.PAYMENT_MODE_NOT_SUPPORTED;
				}

				paymentMethodsUsed.add(paymentMethod);
			}
		}
		return paymentMethodsUsed;
	}

	/**
	 * @return : The default pamynet getway ID for WS channel <i>Note:</i> We currently use the same PG id for XBE for
	 *         the WS channel.
	 * @throws ModuleException
	 */
	private static Integer getDefaultIPGForWSChannel() throws ModuleException {
		Integer ipgId = null;
		List<IPGPaymentOptionDTO> xbeIpgs = WebServicesModuleUtils.getPaymentBrokerBD().getPaymentGateways("XBE");

		if (xbeIpgs != null && xbeIpgs.size() > 0) {
			for (IPGPaymentOptionDTO xbeIpg : xbeIpgs) {
				if (ReservationInternalConstants.DefaultPaymentGateway.YES.equals(xbeIpg.getIsDefault())) {
					ipgId = xbeIpg.getPaymentGateway();
					break;
				}
			}
		}
		return ipgId;
	}

	/**
	 * Validates card payment details
	 * 
	 * @param otaPaymentCard
	 * @throws WebservicesException
	 */
	private static void validateCreditCardPaymentDetails(PaymentCardType otaPaymentCard) throws WebservicesException {
		try {
			if (otaPaymentCard.getCardType() == null
					|| !("1".equals(otaPaymentCard.getCardType()) || "2".equals(otaPaymentCard.getCardType()))) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_, "Invalid Card Type");
			}

			SimpleDateFormat sdf = new SimpleDateFormat("MMyy");
			sdf.parse(otaPaymentCard.getExpireDate());
			Calendar expCal = sdf.getCalendar();
			expCal.add(Calendar.MONTH, 1);
			Date expiryDate = expCal.getTime();

			if (new Date().after(expiryDate)) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_, "Invalid Card Expiry Date");
			}

			if (otaPaymentCard.getCardNumber() == null || "".equals(otaPaymentCard.getCardNumber())) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_, "Invalid Card Number");
			}

			if (otaPaymentCard.getSeriesCode() == null || otaPaymentCard.getSeriesCode().length() != 3) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_, "Invalid Series Code");
			}
		} catch (WebservicesException wex) {
			throw wex;
		} catch (Exception ex) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_, "Invalid Card Details");
		}
	}

	/**
	 * Returns corresponding WS ExternalPayStatus for AA ExternalPayStatus.
	 * 
	 * @param aaPayTxStatus
	 * @return
	 */
	public static String getWSExtPayTxStatus(String aaPayTxStatus) {
		String wsStatus = CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_INITIATED);

		if (aaPayTxStatus.equals(ReservationInternalConstants.ExtPayTxStatus.SUCCESS)) {
			wsStatus = CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_SUCCESS);
		}

		return wsStatus;
	}

	/**
	 * Returns corresponding WS reservation segment status for AA reservation segment status.
	 * 
	 * @param aaFltSegStatus
	 * @return
	 */
	public static String getWSResSegStatus(String aaFltSegStatus) {
		String wsResSegStatus = "";
		if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(aaFltSegStatus)) {
			wsResSegStatus = CommonUtil.getOTACodeValue(IOTACodeTables.Status_STS_ACTIVE);
		} else if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(aaFltSegStatus)) {
			wsResSegStatus = CommonUtil.getOTACodeValue(IOTACodeTables.Status_STS_CANCELLED);
		}
		return wsResSegStatus;
	}

	/**
	 * Returns corresponding WS external payment transaction status for AA external payment transaction status
	 * 
	 * @param aaExtPayTnxStatus
	 * @return
	 */
	public static String getWSExtPayTnxStatus(String aaExtPayTnxStatus) {
		String wsStatus = "";
		if (ReservationInternalConstants.ExtPayTxStatus.UNKNOWN.equals(aaExtPayTnxStatus)) {
			wsStatus = CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_UNKNOWN);
		} else if (ReservationInternalConstants.ExtPayTxStatus.INITIATED.equals(aaExtPayTnxStatus)) {
			wsStatus = CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_INITIATED);
		} else if (ReservationInternalConstants.ExtPayTxStatus.SUCCESS.equals(aaExtPayTnxStatus)) {
			wsStatus = CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_SUCCESS);
		} else if (ReservationInternalConstants.ExtPayTxStatus.FAILED.equals(aaExtPayTnxStatus)) {
			wsStatus = CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_FAILED);
		} else if (ReservationInternalConstants.ExtPayTxStatus.BANK_EXCESS.equals(aaExtPayTnxStatus)) {
			wsStatus = CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_BANK_EXCESS);
		} else if (ReservationInternalConstants.ExtPayTxStatus.AA_EXCESS.equals(aaExtPayTnxStatus)) {
			wsStatus = CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_AA_EXCESS);
		} else if (ReservationInternalConstants.ExtPayTxStatus.ABORTED.equals(aaExtPayTnxStatus)) {
			wsStatus = CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_ABORTED);
		} else if (ReservationInternalConstants.ExtPayTxStatus.REFUNDED.equals(aaExtPayTnxStatus)) {
			wsStatus = CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_REFUNDED);
		} else if (ReservationInternalConstants.ExtPayTxStatus.REVERTED.equals(aaExtPayTnxStatus)) {
			wsStatus = CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_REVERTED);
		}

		return wsStatus;
	}

	/**
	 * Returns AA external payment transaction status for WS external payment transaction status.
	 * 
	 * @param wsExtPayTnxStatus
	 * @return
	 */
	public static String getAAExtPayTnxStatus(String wsExtPayTnxStatus) {
		String aaStatus = "";

		if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_UNKNOWN).equals(wsExtPayTnxStatus)) {
			aaStatus = ReservationInternalConstants.ExtPayTxStatus.UNKNOWN;
		} else if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_INITIATED)
				.equals(wsExtPayTnxStatus)) {
			aaStatus = ReservationInternalConstants.ExtPayTxStatus.INITIATED;
		} else if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_SUCCESS).equals(wsExtPayTnxStatus)) {
			aaStatus = ReservationInternalConstants.ExtPayTxStatus.SUCCESS;
		} else if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_FAILED).equals(wsExtPayTnxStatus)) {
			aaStatus = ReservationInternalConstants.ExtPayTxStatus.FAILED;
		} else if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_BANK_EXCESS).equals(
				wsExtPayTnxStatus)) {
			aaStatus = ReservationInternalConstants.ExtPayTxStatus.BANK_EXCESS;
		} else if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_AA_EXCESS)
				.equals(wsExtPayTnxStatus)) {
			aaStatus = ReservationInternalConstants.ExtPayTxStatus.AA_EXCESS;
		} else if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_ABORTED).equals(wsExtPayTnxStatus)) {
			aaStatus = ReservationInternalConstants.ExtPayTxStatus.ABORTED;
		} else if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_REFUNDED).equals(wsExtPayTnxStatus)) {
			aaStatus = ReservationInternalConstants.ExtPayTxStatus.REFUNDED;
		} else if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroExternalPayTxStatus_EPS_REVERTED).equals(wsExtPayTnxStatus)) {
			aaStatus = ReservationInternalConstants.ExtPayTxStatus.REVERTED;
		}

		return aaStatus;
	}

	public static AAPNRAuditMessagesType prepareWSResAuditHistory(Collection<ReservationModificationDTO> aaPNRAudits, String pnr)
			throws WebservicesException, ModuleException {
		AAPNRAuditMessagesType wsPNRAudits = new AAPNRAuditMessagesType();
		wsPNRAudits.setBookingReferenceID(pnr);

		for (ReservationModificationDTO aaPNRAudit : aaPNRAudits) {
			AAPNRAuditMessageType wsPNRAudit = new AAPNRAuditMessageType();
			wsPNRAudit.setAuditType(new AAAuditType());
			wsPNRAudit.getAuditType().setCode(aaPNRAudit.getModificationTypeCode());
			wsPNRAudit.getAuditType().setDescription(aaPNRAudit.getModificationTypeDesc());
			wsPNRAudit.setDescription(aaPNRAudit.getDescription());
			wsPNRAudit.setUserNoteText(aaPNRAudit.getUserNote());
			wsPNRAudit.setUserID(aaPNRAudit.getUserId());
			wsPNRAudit.setZuluTimestamp(CommonUtil.parse(aaPNRAudit.getZuluModificationDate()));

			wsPNRAudits.getPNRAuditMessage().add(wsPNRAudit);
		}

		return wsPNRAudits;
	}

	/**
	 * Method to get the pax FOID details
	 * 
	 * @param paxCategoryCode
	 * @param wsPaxTitle
	 * @param wsPaxTypeCode
	 * @return
	 */
	public static PaxCategoryFoidTO getPaxCategoryFOID(String paxCategoryCode, String wsPaxTypeCode) throws WebservicesException {

		PaxCategoryFoidTO paxCategoryFoidTo = null;
		boolean found = false;
		String paxTypeCode = null;

		paxTypeCode = getAAPaxTypeCode(wsPaxTypeCode);

		if (paxTypeCode == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Passenger Type is Invalid");
		}

		List paxConfigList = WebServicesModuleUtils.getGlobalConfig().getpaxConfig(AppIndicatorEnum.APP_WS.toString());

		for (Object obj : paxConfigList) {
			if (obj instanceof WSPaxConfigDTO) {
				WSPaxConfigDTO wsPaxConfig = (WSPaxConfigDTO) obj;
				if (!wsPaxConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PASSPORT)) {
					continue;
				} else {
					if (wsPaxConfig.getPaxCatCode() != null && wsPaxConfig.getPaxTypeCode() != null && (paxCategoryCode != null)
							&& (paxTypeCode != null) && (wsPaxConfig.getPaxCatCode().trim().equals(paxCategoryCode))
							&& (wsPaxConfig.getPaxTypeCode().equals(paxTypeCode))) {
						paxCategoryFoidTo = new PaxCategoryFoidTO();
						String required = "N";
						if (wsPaxConfig.isWsVisibility() && wsPaxConfig.isWsMandatory()) {
							required = "Y";
						}
						paxCategoryFoidTo.setPaxCatFoidRequired(required);

						found = true;
						break;
					}
				}
			}
		}

		if (!found) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"Passenger Category FOID details not found");
		}
		return paxCategoryFoidTo;
	}

	/**
	 * gets the pax FOID details from FOID type and pax type
	 * 
	 * @param foidCode
	 *            foid type
	 * @param wsPaxTypeCode
	 *            pax type
	 * @return
	 * @throws WebservicesException
	 */
	public static PaxCategoryFoidTO getPaxCategoryFOIDFromFOID(String foidCode, String wsPaxTypeCode) throws WebservicesException {

		PaxCategoryFoidTO paxCategoryFoidTo = null;
		boolean found = false;
		String paxTypeCode = getAAPaxTypeCode(wsPaxTypeCode);

		if (paxTypeCode == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Passenger Type is Invalid");
		}

		List paxConfigList = WebServicesModuleUtils.getGlobalConfig().getpaxConfig(AppIndicatorEnum.APP_WS.toString());

		for (Object obj : paxConfigList) {
			if (obj instanceof WSPaxConfigDTO) {
				WSPaxConfigDTO wsPaxConfig = (WSPaxConfigDTO) obj;
				if (!wsPaxConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PASSPORT)) {
					continue;
				} else {
					if (wsPaxConfig.getSsrCode() != null && wsPaxConfig.getPaxTypeCode() != null && (foidCode != null)
							&& (paxTypeCode != null) && (wsPaxConfig.getSsrCode().trim().equals(foidCode))
							&& (wsPaxConfig.getPaxTypeCode().equals(paxTypeCode))) {
						paxCategoryFoidTo = new PaxCategoryFoidTO();
						String required = "N";
						if (wsPaxConfig.isWsVisibility() && wsPaxConfig.isWsMandatory()) {
							required = "Y";
						}
						paxCategoryFoidTo.setPaxCatFoidRequired(required);

						found = true;
						break;
					}
				}
			}
		}

		if (!found) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"Passenger Category FOID details not found");
		}

		return paxCategoryFoidTo;
	}

	/**
	 * Method to get the foidNumber
	 * 
	 * @param list
	 * @param rph
	 * @return
	 */
	public static String getFOIDNumber(List<AAAirTravelerType> list, String rph) {
		String foidNumber = null;
		if (list != null) {
			for (AAAirTravelerType travelerType : list) {
				if (rph.equals(travelerType.getTravelerRefNumberRPH())) {
					foidNumber = travelerType.getFOIDNumber();
					break;
				}
			}
		}
		return foidNumber;
	}

	public static String getNationalIDNo(List<AAAirTravelerType> list, String rph) {
		String nationalIDNo = null;
		if (list != null) {
			for (AAAirTravelerType travelerType : list) {
				if (rph.equals(travelerType.getTravelerRefNumberRPH())) {
					nationalIDNo = travelerType.getNationalIDNo();
					break;
				}
			}
		}
		return nationalIDNo;
	}

	public static Integer getNationalityCode(AirTravelerType traveler) throws ModuleException {
		Integer nationalityCode = null;
		Nationality nationality = null;

		if ((traveler.getDocument().size() > 0) && (traveler.getDocument().get(0).getDocHolderNationality() != null)) {
			nationality = WebServicesModuleUtils.getCommonMasterBD().getNationality(
					traveler.getDocument().get(0).getDocHolderNationality());
		}
		if (nationality != null) {
			nationalityCode = nationality.getNationalityCode();
		}
		return nationalityCode;
	}

	/**
	 * Extract Infant RPH values
	 * 
	 * @param airTravelerType
	 * @return
	 * @throws WebservicesException
	 */
	public static int[] getInfSeqParentSeqAndParentPnrPaxIDForAddInfant(AirTravelerType airTravelerType)
			throws WebservicesException {
		int i[] = new int[3];

		try {
			String travelerRPH = airTravelerType.getTravelerRefNumber().getRPH();
			// expecting rph to be like:I3/A1$15323221
			int infantSeq = Integer.parseInt(travelerRPH.substring(1, travelerRPH.indexOf('/')));
			int parentSeq = Integer.parseInt(travelerRPH.substring(travelerRPH.indexOf('/') + 2, travelerRPH.indexOf('$')));
			int parentPnrPaxID = Integer.parseInt(travelerRPH.substring(travelerRPH.indexOf('$') + 1));
			i[0] = infantSeq;
			i[1] = parentSeq;
			i[2] = parentPnrPaxID;
			return i;
		} catch (Exception e) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_VALUE___TRAVELER_RPH,
					"Please Send Infant RPH as I2/A1$999999");
			// throw new WebservicesExceptionAccelaeroErrorCodes_AAE_INVALID_VALUE___TRAVELER_RPH
		}

	}

	/**
	 * Get POS Airport Code
	 * 
	 * @param airBookModifyRQ
	 * @return
	 * @throws WebservicesException
	 */
	public String getAirportCode(OTAAirBookModifyRQ airBookModifyRQ) throws WebservicesException {
		try {
			return airBookModifyRQ.getPOS().getSource().get(0).getAirportCode();
		} catch (Exception e) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_FIELD_MISSING,
					"Required POS Airport Code was Not Set");
		}
	}

	/**
	 * Get Infant Count
	 * 
	 * @param airBookModifyRQ
	 * @return
	 * @throws WebservicesException
	 */
	public static int getInfantCountToModify(OTAAirBookModifyRQ airBookModifyRQ) throws WebservicesException {
		try {
			Collection<AirTravelerType> travellers = airBookModifyRQ.getAirBookModifyRQ().getTravelerInfo().getAirTraveler();
			for (AirTravelerType airTravelerType : travellers) {
				// @todo have to get
				if (airTravelerType.getPassengerTypeCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
					return airTravelerType.getPassengerTypeQuantity().getQuantity();

				}

			}
		} catch (Exception e) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_FIELD_MISSING,
					"Passenger Type Quantity is Not Set");
		}
		return 0;
	}

	/**
	 * Prepare list of pricing keys corresponds to user selected price quotes (out of all the price quote done within
	 * the current transaction) TODO remove this method after re-price in the booking stage success
	 * 
	 * @param airItinerary
	 * @return
	 * @throws WebservicesException
	 */
	public static Collection<String> getSelectedPricingKeys(AirItineraryType airItinerary) throws WebservicesException {
		Collection<String> pricingKeys = new ArrayList<String>();

		String currentOnDKey = null;
		String previousOnDKey = null;
		boolean isFirstSegOfOnd = true;
		for (OriginDestinationOptionType ond : airItinerary.getOriginDestinationOptions().getOriginDestinationOption()) {
			isFirstSegOfOnd = true;
			for (BookFlightSegmentType flightSegment : ond.getFlightSegment()) {

				if (!AppSysParamsUtil.getDefaultCarrierCode().equals(
						AppSysParamsUtil.extractCarrierCode(flightSegment.getFlightNumber()))) {
					continue; // External segment from interline bookings transfer
				}

				currentOnDKey = flightSegment.getRPH().substring(flightSegment.getRPH().indexOf("-") + 1);

				if (isFirstSegOfOnd) {
					if (!pricingKeys.contains(currentOnDKey)) {
						pricingKeys.add(currentOnDKey);
						previousOnDKey = currentOnDKey;
					} else {
						if (previousOnDKey != null && !previousOnDKey.equals(currentOnDKey)) {
							// Inconsistent data
							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
									"Inconsistent pricing information found");
						}
					}
					isFirstSegOfOnd = false;
				} else {
					if (!pricingKeys.contains(currentOnDKey)) {
						// Inconsistent data
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Inconsistent pricing information found");
					}
				}
			}
		}

		return pricingKeys;
	}

	public static void validatePaxDetails(String title, String fName, String lName, Date dob, Integer nationality,
			Integer travelWith, String passportNo, String passportIssuedCountry, Date passportExpiryDate, String paxType,
			boolean isModify, boolean skipValidation, String employeeId, Date dateOfJoin, String idCategory,
			String bookingCategory, List<String> passportNumbers, String ffid, boolean isDomesticSegmentExist, List<String> nationalIDList,
			String nationalID, List<String> airportCodes) throws WebservicesException, ModuleException {

		Map<String, List<PaxCountryConfigDTO>> countryWiseConfigs = null;
		List paxConfigList = WebServicesModuleUtils.getGlobalConfig().getpaxConfig(AppIndicatorEnum.APP_WS.toString());
		Map<String, Boolean> countryPaxConfigAD = new HashMap<String, Boolean>();
		Map<String, Boolean> countryPaxConfigCH = new HashMap<String, Boolean>();
		Map<String, Boolean> countryPaxConfigIN = new HashMap<String, Boolean>();

		Map<String, Map<String, Boolean>> paxTypeWiseCountryConfigs = new HashMap<>();
		paxTypeWiseCountryConfigs.put(ReservationInternalConstants.PassengerType.ADULT, countryPaxConfigAD);
		paxTypeWiseCountryConfigs.put(ReservationInternalConstants.PassengerType.CHILD, countryPaxConfigCH);
		paxTypeWiseCountryConfigs.put(ReservationInternalConstants.PassengerType.INFANT, countryPaxConfigIN);

		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

		boolean skipNIC = privilegeKeys.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_SKIP_NIC);

		Collection<String[]> paxTitlesAdult = SelectListGenerator.getAdultTitles();
		Collection<String[]> paxTitlesChild = SelectListGenerator.getChildTitle();
		Collection<String[]> paxTitlesInfant = SelectListGenerator.getInfantTitles();
		
		if (airportCodes != null && airportCodes.size() > 0) {
			try {
				countryWiseConfigs = WebServicesModuleUtils.getAirproxyPassengerBD().loadPaxCountryConfig(SYSTEM.AA.toString(),
						AppIndicatorEnum.APP_WS.toString(), airportCodes, AAUtils.getTrackInfo());

			} catch (ModuleException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (countryWiseConfigs != null && countryWiseConfigs.size() > 0) {
				populateCountryWisePAxConfig(paxConfigList, countryWiseConfigs, paxType, countryPaxConfigAD, countryPaxConfigCH,
						countryPaxConfigIN);
			}
		}

		for (WSPaxConfigDTO wsConfig : (List<WSPaxConfigDTO>) paxConfigList) {

			if (wsConfig.getPaxTypeCode().equals(paxType)) {
				Map<String, Boolean> countryPaxConfig = paxTypeWiseCountryConfigs.get(paxType);

				boolean fieldMandatry = false;

				if (countryPaxConfig.get(wsConfig.getFieldName()) != null) {
					fieldMandatry = countryPaxConfig.get(wsConfig.getFieldName());
				} else {
					fieldMandatry = wsConfig.isWsMandatory();
				}

				if (wsConfig.isWsVisibility() && fieldMandatry) {
					if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_TITLE) && (title == null || title.equals(""))) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Passenger Title not present");
						
					} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_TITLE) && (title != null)
							&& (wsConfig.getPaxTypeCode() == "AD") && (!checkTitle(title, paxTitlesAdult))) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Adult Title");

					} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_TITLE) && (title != null)
							&& (wsConfig.getPaxTypeCode() == "CH") && (!checkTitle(title, paxTitlesChild))) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Child Title");

					} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_TITLE) && (title != null)
							&& (wsConfig.getPaxTypeCode() == "IN") && (!checkTitle(title, paxTitlesInfant))) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Infant Title");

					} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_DOB) && dob == null && !skipValidation) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Passenger Date of Birth not present");

					} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_NATIONALITY) && nationality == null
							&& isModify && !skipValidation) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Passenger Nationality not present");

					} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_TRAVELWITH) && travelWith == null) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Infant Travel With not present");

					} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PASSPORT)
							&& (passportNo == null || passportNo.equals("")) && !skipValidation && fieldMandatry) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Passenger Passport Number not present");

					} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PSPT_EXPIRY)
							&& (passportExpiryDate == null || passportExpiryDate.before(new Date())) && !skipValidation
							&& fieldMandatry) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Passenger Passport Expiry is not provided or already expired.");
					} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_PSPT_ISSUED_CNTRY)
							&& (passportIssuedCountry == null || passportIssuedCountry.equals("")) && !skipValidation
							&& fieldMandatry) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Passenger Passport Issued Country not present or invalid country code provided");
					} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_EMPLOYEE_ID)
							&& (employeeId == null || "".equals(employeeId)) && !skipValidation
							&& BookingCategory.HR.getCode().equals(bookingCategory)) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Employee ID not present");

					} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_DATE_OF_JOIN)
							&& (dateOfJoin == null || !skipValidation) && BookingCategory.HR.getCode().equals(bookingCategory)) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Employee date of join present");

					} else if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_ID_CATEGORY)
							&& (idCategory == null || "".equals(idCategory)) && !skipValidation
							&& BookingCategory.HR.getCode().equals(bookingCategory)) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Employee id category not present");

					} else if (!AppSysParamsUtil.isRequestNationalIDForReservationsHavingDomesticSegments()
							&& wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_NATIONAL_ID)
							&& (nationalID == null || "".equals(nationalID)) && !skipNIC) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Passenger National ID Number not present");
					}
				}
				if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_FNAME)
						&& (fName == null || fName.equals(""))) {

					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
							"Passenger First Name not present");
				}
				
				if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_LNAME)
						&& (lName == null || lName.equals(""))) {

					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
						"Passenger Last Name not present");
				}
				
				if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_NATIONAL_ID) && isDomesticSegmentExist
						&& AppSysParamsUtil.isRequestNationalIDForReservationsHavingDomesticSegments()
						&& (nationalID == null || "".equals(nationalID)) && !skipNIC) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
							"Passenger National ID Number not present");
				}
				if ((nationalID != null && !"".equals(nationalID)) && !StringUtils.isAlphanumeric(nationalID)) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
							"Passenger National ID Number is Invalid");
				}
				if (wsConfig.getFieldName().equals(BasePaxConfigDTO.FIELD_NATIONAL_ID) && isDomesticSegmentExist
						&& AppSysParamsUtil.isRequestNationalIDForReservationsHavingDomesticSegments()
						&& (nationalID == null || "".equals(nationalID)) && !skipValidation && fieldMandatry && !skipNIC) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
							"Passenger National ID Number not present");
				}
			}
		}

		// TODO : Thihara : Implement proper validation according to the configs available
		if (!skipValidation && passportNumbers != null && !passportNumbers.isEmpty() && passportNo != null
				&& !passportNo.equals("") && passportNumbers.contains(passportNo)) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"Duplicate passport numbers for adults detected in reservation");
		}

		validateLoyaltyInformation(ffid, fName, lName);
	}

	public static void validateLoyaltyInformation(String ffid, String firstName, String lastName) throws WebservicesException {
		try {
			if (ffid != null && !"".equals(ffid)) {
				LoyaltyMemberCoreDTO loyaltyMemberCoreDTO = WebServicesModuleUtils.getLoyaltyManagementBD()
						.getLoyaltyMemberCoreDetails(ffid);

				if (loyaltyMemberCoreDTO.getReturnCode() != LMSConstants.WSReponse.MEMBER_NOT_FOUND) {
					if (loyaltyMemberCoreDTO == null
							|| loyaltyMemberCoreDTO.getReturnCode() != LMSConstants.WSReponse.RESPONSE_CODE_OK) {
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_AIREWARDS_ID_ENTERED,
								"Invalid Airewards ID " + ffid);
					} else if (!LoyaltyNameValidator.compareNames(firstName, lastName, loyaltyMemberCoreDTO.getMemberFirstName(),
							loyaltyMemberCoreDTO.getMemberLastName())) {
						// TODO:RW pax FFID name validation
						throw new WebservicesException(
								IOTACodeTables.AccelaeroErrorCodes_AAE_PASSENGER_NAME_DOES_NOT_MATCH_WITH_THE_AIREWARDS_EMAIL_ID,
								"Passenger Firstname and Lastname does not match with Airewards member for " + ffid);
					}
				}
			}

		} catch (ModuleException e) {
			log.error("Error in validating Airewards ID for " + ffid, e);
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Airewards ID Validation Failed for "
					+ ffid);
		}
	}

	public static void validateContactInfo(ReservationContactInfo contactInfo) throws WebservicesException {
		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();
		boolean validate = !AppSysParamsUtil.isWebServiceExtaraValidationSkipCarrier(userPrincipal.getAgentCode());

		List contactConfigList = WebServicesModuleUtils.getGlobalConfig().getContactDetailsConfig(
				AppIndicatorEnum.APP_WS.toString());

		for (WSContactConfigDTO config : (List<WSContactConfigDTO>) contactConfigList) {

			switch (config.getGroupId()) {
			case BaseContactConfigDTO.GROUP_NORMAL_CONTACT:

				if (config.isWsVisibility() && config.isMandatory()) {
					if (BaseContactConfigDTO.FIELD_TITLE.equals(config.getFieldName())
							&& (contactInfo.getTitle() == null || contactInfo.getTitle().equals("")) && validate) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Contact Person Title required");

					} else if (BaseContactConfigDTO.FIELD_FIRST_NAME.equals(config.getFieldName())
							&& (contactInfo.getFirstName() == null || contactInfo.getFirstName().equals(""))) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_DATA_MISSING___FIRST_NAME,
								"Contact Person First Name required");

					} else if (BaseContactConfigDTO.FIELD_LAST_NAME.equals(config.getFieldName())
							&& (contactInfo.getLastName() == null || contactInfo.getLastName().equals(""))) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_DATA_MISSING___LAST_NAME,
								"Contact Person Last Name required");

					} else if (BaseContactConfigDTO.FIELD_ADDRESS.equals(config.getFieldName())
							&& (contactInfo.getStreetAddress1() == null || contactInfo.getStreetAddress1().equals(""))
							&& validate) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_DATA_MISSING___ADDRESS,
								"Contact Person Address required");

					} else if (BaseContactConfigDTO.FIELD_CITY.equals(config.getFieldName())
							&& (contactInfo.getCity() == null || contactInfo.getCity().equals("")) && validate) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_CITY_IN_ADDRESS_IS_REQUIRED,
								"Contact Person City in Address required");

					} else if (BaseContactConfigDTO.FIELD_ZIP_CODE.equals(config.getFieldName())
							&& (contactInfo.getZipCode() == null || contactInfo.getZipCode().equals("")) && validate) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Contact Person Zip Code required");

					} else if (BaseContactConfigDTO.FIELD_COUNTRY.equals(config.getFieldName())
							&& (contactInfo.getCountryCode() == null || contactInfo.getCountryCode().equals("")) && validate) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Contact Person Country required");

					} else if (BaseContactConfigDTO.FIELD_MOBILE.equals(config.getFieldName())
							&& (contactInfo.getMobileNo() == null || contactInfo.getMobileNo().equals("")) && validate) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_DATA_MISSING___PHONE_NUMBER,
								"Contact Person Mobile Number required");

					} else if (BaseContactConfigDTO.FIELD_PHONE.equals(config.getFieldName())
							&& (contactInfo.getPhoneNo() == null || contactInfo.getPhoneNo().equals("")) && validate) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_DATA_MISSING___PHONE_NUMBER,
								"Contact Person Telephone Number required");

					} else if (BaseContactConfigDTO.FIELD_FAX.equals(config.getFieldName())
							&& (contactInfo.getFax() == null || contactInfo.getFax().equals("")) && validate) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_DATA_MISSING___PHONE_NUMBER,
								"Contact Person Fax Number required");

					} else if (BaseContactConfigDTO.FIELD_EMAIL.equals(config.getFieldName())
							&& (contactInfo.getEmail() == null || contactInfo.getEmail().equals("")) && validate) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Contact Person Email required");

					} else if (BaseContactConfigDTO.FIELD_PREFERRED_LANG.equals(config.getFieldName())
							&& (contactInfo.getPreferredLanguage() == null || contactInfo.getPreferredLanguage().equals(""))
							&& validate) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Contact Person Preferred Lanaguage required");

					}

				}
				break;

			case BaseContactConfigDTO.GROUP_EMERGENCY_CONTACT:
				if (config.isWsVisibility() && config.isMandatory()) {
					if (validate) {
						if (BaseContactConfigDTO.FIELD_TITLE.equals(config.getFieldName())
								&& (contactInfo.getEmgnTitle() == null || contactInfo.getEmgnTitle().equals(""))) {

							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
									"Emergency Contact Person Title required");

						} else if (BaseContactConfigDTO.FIELD_FIRST_NAME.equals(config.getFieldName())
								&& (contactInfo.getEmgnFirstName() == null || contactInfo.getEmgnFirstName().equals(""))) {

							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_DATA_MISSING___FIRST_NAME,
									"Emergency Contact Person First Name required");

						} else if (BaseContactConfigDTO.FIELD_LAST_NAME.equals(config.getFieldName())
								&& (contactInfo.getEmgnLastName() == null || contactInfo.getEmgnLastName().equals(""))) {

							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_DATA_MISSING___LAST_NAME,
									"Emergency Contact Person Last Name required");

						} else if (BaseContactConfigDTO.FIELD_PHONE.equals(config.getFieldName())
								&& (contactInfo.getEmgnPhoneNo() == null || contactInfo.getEmgnPhoneNo().equals("")) && validate) {

							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_DATA_MISSING___PHONE_NUMBER,
									"Emergency Contact Person Telephone Number required");

						} else if (BaseContactConfigDTO.FIELD_EMAIL.equals(config.getFieldName())
								&& (contactInfo.getEmgnEmail() == null || contactInfo.getEmgnEmail().equals("")) && validate) {

							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
									"Emergency Contact Person Email required");

						}
					}
				}
				break;

			default:
				break;
			}
		}
	}

	/**
	 * 
	 * @param contactInfo
	 * @throws WebservicesException
	 */
	public static void validateContactInfo(CommonReservationContactInfo contactInfo) throws WebservicesException {
		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();
		boolean validate = !AppSysParamsUtil.isWebServiceExtaraValidationSkipCarrier(userPrincipal.getAgentCode());

		List contactConfigList = WebServicesModuleUtils.getGlobalConfig().getContactDetailsConfig(
				AppIndicatorEnum.APP_WS.toString());

		for (WSContactConfigDTO config : (List<WSContactConfigDTO>) contactConfigList) {

			switch (config.getGroupId()) {
			case BaseContactConfigDTO.GROUP_NORMAL_CONTACT:

				if (config.isWsVisibility() && config.isMandatory()) {
					if (BaseContactConfigDTO.FIELD_TITLE.equals(config.getFieldName())
							&& (contactInfo.getTitle() == null || contactInfo.getTitle().equals("")) && validate) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Contact Person Title required");

					} else if (BaseContactConfigDTO.FIELD_FIRST_NAME.equals(config.getFieldName())
							&& (contactInfo.getFirstName() == null || contactInfo.getFirstName().equals(""))) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_DATA_MISSING___FIRST_NAME,
								"Contact Person First Name required");

					} else if (BaseContactConfigDTO.FIELD_LAST_NAME.equals(config.getFieldName())
							&& (contactInfo.getLastName() == null || contactInfo.getLastName().equals(""))) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_DATA_MISSING___LAST_NAME,
								"Contact Person Last Name required");

					} else if (BaseContactConfigDTO.FIELD_ADDRESS.equals(config.getFieldName())
							&& (contactInfo.getStreetAddress1() == null || contactInfo.getStreetAddress1().equals(""))
							&& validate) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_DATA_MISSING___ADDRESS,
								"Contact Person Address required");

					} else if (BaseContactConfigDTO.FIELD_CITY.equals(config.getFieldName())
							&& (contactInfo.getCity() == null || contactInfo.getCity().equals("")) && validate) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_CITY_IN_ADDRESS_IS_REQUIRED,
								"Contact Person City in Address required");

					} else if (BaseContactConfigDTO.FIELD_ZIP_CODE.equals(config.getFieldName())
							&& (contactInfo.getZipCode() == null || contactInfo.getZipCode().equals("")) && validate) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Contact Person Zip Code required");

					} else if (BaseContactConfigDTO.FIELD_COUNTRY.equals(config.getFieldName())
							&& (contactInfo.getCountryCode() == null || contactInfo.getCountryCode().equals("")) && validate) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Contact Person Country required");

					} else if (BaseContactConfigDTO.FIELD_MOBILE.equals(config.getFieldName())
							&& (contactInfo.getMobileNo() == null || contactInfo.getMobileNo().equals("")) && validate) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_DATA_MISSING___PHONE_NUMBER,
								"Contact Person Mobile Number required");

					} else if (BaseContactConfigDTO.FIELD_PHONE.equals(config.getFieldName())
							&& (contactInfo.getPhoneNo() == null || contactInfo.getPhoneNo().equals("")) && validate) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_DATA_MISSING___PHONE_NUMBER,
								"Contact Person Telephone Number required");

					} else if (BaseContactConfigDTO.FIELD_FAX.equals(config.getFieldName())
							&& (contactInfo.getFax() == null || contactInfo.getFax().equals("")) && validate) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_DATA_MISSING___PHONE_NUMBER,
								"Contact Person Fax Number required");

					} else if (BaseContactConfigDTO.FIELD_EMAIL.equals(config.getFieldName())
							&& (contactInfo.getEmail() == null || contactInfo.getEmail().equals("")) && validate) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Contact Person Email required");

					} else if (BaseContactConfigDTO.FIELD_PREFERRED_LANG.equals(config.getFieldName())
							&& (contactInfo.getPreferredLanguage() == null || contactInfo.getPreferredLanguage().equals(""))
							&& validate) {

						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Contact Person Preferred Lanaguage required");

					}

				}
				break;

			case BaseContactConfigDTO.GROUP_EMERGENCY_CONTACT:
				if (config.isWsVisibility() && config.isMandatory()) {
					if (validate) {
						if (BaseContactConfigDTO.FIELD_TITLE.equals(config.getFieldName())
								&& (contactInfo.getEmgnTitle() == null || contactInfo.getEmgnTitle().equals(""))) {

							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
									"Emergency Contact Person Title required");

						} else if (BaseContactConfigDTO.FIELD_FIRST_NAME.equals(config.getFieldName())
								&& (contactInfo.getEmgnFirstName() == null || contactInfo.getEmgnFirstName().equals(""))) {

							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_DATA_MISSING___FIRST_NAME,
									"Emergency Contact Person First Name required");

						} else if (BaseContactConfigDTO.FIELD_LAST_NAME.equals(config.getFieldName())
								&& (contactInfo.getEmgnLastName() == null || contactInfo.getEmgnLastName().equals(""))) {

							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_DATA_MISSING___LAST_NAME,
									"Emergency Contact Person Last Name required");

						} else if (BaseContactConfigDTO.FIELD_PHONE.equals(config.getFieldName())
								&& (contactInfo.getEmgnPhoneNo() == null || contactInfo.getEmgnPhoneNo().equals("")) && validate) {

							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_DATA_MISSING___PHONE_NUMBER,
									"Emergency Contact Person Telephone Number required");

						} else if (BaseContactConfigDTO.FIELD_EMAIL.equals(config.getFieldName())
								&& (contactInfo.getEmgnEmail() == null || contactInfo.getEmgnEmail().equals("")) && validate) {

							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
									"Emergency Contact Person Email required");

						}
					}
				}
				break;

			default:
				break;
			}
		}
	}

	private static void populateCountryWisePAxConfig(List contactConfigList,
			Map<String, List<PaxCountryConfigDTO>> countryWiseConfigs, String paxType, Map<String, Boolean> countryPaxConfigAD,
			Map<String, Boolean> countryPaxConfigCH, Map<String, Boolean> countryPaxConfigIN) {
		for (List<PaxCountryConfigDTO> paxCountryConfigs : countryWiseConfigs.values()) {
			for (PaxCountryConfigDTO paxCountryConfig : paxCountryConfigs) {
				String field = paxCountryConfig.getFieldName();
				if (ReservationInternalConstants.PassengerType.ADULT.equals(paxCountryConfig.getPaxTypeCode())) {
					fillPaxCountryConfigMap(paxCountryConfig, countryPaxConfigAD);
				} else if (ReservationInternalConstants.PassengerType.CHILD.equals(paxCountryConfig.getPaxTypeCode())) {
					fillPaxCountryConfigMap(paxCountryConfig, countryPaxConfigCH);
				} else if (ReservationInternalConstants.PassengerType.INFANT.equals(paxCountryConfig.getPaxTypeCode())) {
					fillPaxCountryConfigMap(paxCountryConfig, countryPaxConfigIN);
				}
			}
		}
	}

	private static void fillPaxCountryConfigMap(PaxCountryConfigDTO paxCountryConfig, Map<String, Boolean> countryPaxConfig) {
		String fieldName = paxCountryConfig.getFieldName();
		if (countryPaxConfig.get(fieldName) == null) {
			countryPaxConfig.put(fieldName, paxCountryConfig.isMandatory());
		} else {
			Boolean currentValue = countryPaxConfig.get(fieldName);
			countryPaxConfig.put(fieldName, (currentValue || paxCountryConfig.isMandatory()));
		}
	}

	/**
	 * @param ondFareDTOs
	 * @return
	 */
	public static boolean isReturnQuote(Collection<OndFareDTO> ondFareDTOs) {

		for (OndFareDTO ondFareDTO : ondFareDTOs) {
			if (ondFareDTO.isInBoundOnd()) {
				return true;
			}
		}
		return false;
	}

	public static CabinType getCabinType(String cabinTypeCode) {
		CabinType cabinType = CabinType.Y;
		for (CabinType ct : CabinType.values()) {
			if (ct.toString().equalsIgnoreCase(cabinTypeCode)) {
				cabinType = ct;
				break;
			}
		}

		return cabinType;
	}

	public static PTCFareBreakdownType clonePtcFareBreakdownType(PTCFareBreakdownType ptcFareBreakdown) {
		PTCFareBreakdownType newPtcFareBreakdownType = new PTCFareBreakdownType();

		FareBasisCodes fareBasisCodes = ptcFareBreakdown.getFareBasisCodes();
		FareType fareType = ptcFareBreakdown.getPassengerFare();
		PassengerTypeQuantityType passengerTypeQuantityType = ptcFareBreakdown.getPassengerTypeQuantity();

		FareBasisCodes newFareBasisCodes = new FareBasisCodes();
		newFareBasisCodes.getFareBasisCode().addAll(fareBasisCodes.getFareBasisCode());

		FareType newFareType = new FareType();

		BaseFare baseFare = fareType.getBaseFare();
		Fees fees = fareType.getFees();
		TotalFare totalFare = fareType.getTotalFare();
		Taxes taxes = fareType.getTaxes();

		BaseFare newBaseFare = new BaseFare();
		newBaseFare.setAmount(baseFare.getAmount());
		newBaseFare.setCurrencyCode(baseFare.getCurrencyCode());
		newBaseFare.setDate(baseFare.getDate());
		newBaseFare.setDecimalPlaces(baseFare.getDecimalPlaces());
		newBaseFare.setFromCurrency(baseFare.getFromCurrency());
		newBaseFare.setRate(baseFare.getRate());
		newBaseFare.setToCurrency(baseFare.getToCurrency());

		Fees newFees = new Fees();
		for (AirFeeType airFeeType : fees.getFee()) {
			AirFeeType newAirFeeType = new AirFeeType();

			newAirFeeType.setAmount(airFeeType.getAmount());
			newAirFeeType.setCurrencyCode(airFeeType.getCurrencyCode());
			newAirFeeType.setDecimalPlaces(airFeeType.getDecimalPlaces());
			newAirFeeType.setFeeCode(airFeeType.getFeeCode());
			newAirFeeType.setValue(airFeeType.getValue());

			newFees.getFee().add(newAirFeeType);
		}

		TotalFare newTotalFare = new TotalFare();
		newTotalFare.setAmount(totalFare.getAmount());
		newTotalFare.setCurrencyCode(totalFare.getCurrencyCode());
		newTotalFare.setDecimalPlaces(totalFare.getDecimalPlaces());

		Taxes newTaxes = new Taxes();
		for (AirTaxType airTaxType : taxes.getTax()) {
			AirTaxType newAirTaxType = new AirTaxType();

			newAirTaxType.setAmount(airTaxType.getAmount());
			newAirTaxType.setCurrencyCode(airTaxType.getCurrencyCode());
			newAirTaxType.setDecimalPlaces(airTaxType.getDecimalPlaces());
			newAirTaxType.setTaxCode(airTaxType.getTaxCode());
			newAirTaxType.setTaxCountry(airTaxType.getTaxCountry());
			newAirTaxType.setTaxName(airTaxType.getTaxName());
			newAirTaxType.setValue(airTaxType.getValue());

			newTaxes.getTax().add(newAirTaxType);
		}
		newFareType.setBaseFare(newBaseFare);
		newFareType.setFees(newFees);
		newFareType.setTaxes(newTaxes);
		newFareType.setTotalFare(newTotalFare);

		PassengerTypeQuantityType newPassengerTypeQuantityType = new PassengerTypeQuantityType();
		newPassengerTypeQuantityType.setAge(passengerTypeQuantityType.getAge());
		newPassengerTypeQuantityType.setCode(passengerTypeQuantityType.getCode());
		newPassengerTypeQuantityType.setCodeContext(passengerTypeQuantityType.getCodeContext());
		newPassengerTypeQuantityType.setQuantity(passengerTypeQuantityType.getQuantity());
		newPassengerTypeQuantityType.setURI(passengerTypeQuantityType.getURI());

		newPtcFareBreakdownType.setFareBasisCodes(newFareBasisCodes);
		newPtcFareBreakdownType.setPassengerFare(newFareType);
		newPtcFareBreakdownType.setPassengerTypeQuantity(newPassengerTypeQuantityType);
		newPtcFareBreakdownType.setPricingSource(ptcFareBreakdown.getPricingSource());

		return newPtcFareBreakdownType;
	}

	public static boolean validateNumberFormat(String numericString) {
		String numericRegex = "\\d+";
		if (numericString != null && !numericString.equals("")) {
			return numericString.matches(numericRegex);
		}
		return true;
	}

	public static void logRequestResponseDetails(String requestResponseType, UserPrincipal principal, TrackInfoDTO trackInfo,
			String transactionID) {
		String ipAddress = "";
		String sessionID = "";
		String userID = "";
		if (trackInfo == null) {
			trackInfo = AAUtils.getTrackInfo();
		}

		if (trackInfo != null) {
			ipAddress = trackInfo.getIpAddress();
			sessionID = trackInfo.getSessionId();
		}

		if (principal == null) {
			principal = ThreadLocalData.getCurrentUserPrincipal();
			;
		}
		if (principal != null) {
			userID = principal.getUserId();
		}
		StringBuilder logs = new StringBuilder();
		logs.append(requestResponseType);
		logs.append(" : USER ID - " + userID);
		logs.append(" : SESSION -" + sessionID);
		logs.append(" : TRANSACTION -" + transactionID);
		logs.append(" : IP - " + ipAddress);
		log.info(logs.toString());

	}

	public static BigDecimal getServiceChargeWithJNTax(EXTERNAL_CHARGES externalCharge, BigDecimal serviceCharge) {
		BigDecimal taxAmount = calculateApplicableTaxAmount(externalCharge, serviceCharge);
		return AccelAeroCalculator.add(serviceCharge, taxAmount);
	}

	private static BigDecimal calculateApplicableTaxAmount(EXTERNAL_CHARGES externalCharge, BigDecimal chargeAmount) {
		BigDecimal taxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		
		Object serviceTaxes = ThreadLocalData.getCurrentTnxParam(Transaction.APPLICABLE_SERVICE_TAXES);
		if (serviceTaxes != null) {
			Set<ServiceTaxContainer> serviceTaxContainers = (Set<ServiceTaxContainer>) serviceTaxes;

			if (!serviceTaxContainers.isEmpty()) {
				for (ServiceTaxContainer taxContainer : serviceTaxContainers) {
					if (taxContainer != null && taxContainer.isTaxApplicable()
							&& taxContainer.getTaxableExternalCharges().contains(externalCharge)) {
						taxAmount = AccelAeroCalculator.multiplyDefaultScale(chargeAmount, taxContainer.getTaxRatio());

					}
				}
			}

		}
		return taxAmount;
	}
	
	private static void validateCreditCardNumber(PaymentCardType otaPaymentCard) throws WebservicesException {
		int cardType = Integer.parseInt(otaPaymentCard.getCardType());
		String cardNumber = otaPaymentCard.getCardNumber();
		boolean isValidCardRange = false;

		switch (cardType) {

		case 1: // Master Card Validation
			if (cardNumber.length() == 16) {
				String cardStartNumber = cardNumber.substring(0, 2);
				int firstTwoDigits = Integer.parseInt(cardStartNumber);
				if (firstTwoDigits >= 51 && firstTwoDigits <= 59) {
					isValidCardRange = true;
				}
			}
			break;

		case 2: // VISA Card Validation
			if (cardNumber.length() == 16 || cardNumber.length() == 13) {
				String firstNumber = cardNumber.substring(0, 1);
				int firstDigit = Integer.parseInt(firstNumber);
				if (firstDigit == 4) {
					isValidCardRange = true;
				}
			}
			break;

		}

		if (!isValidCardRange || !CreditCardUtil.validateCardNumberWithLuhnAlgorithm(cardNumber)) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_, "Invalid Card Number");
		}
	}

	public static boolean isDomesticSegmentExists(List<FlightSegmentTO> flightSegmentTOs) {
		if (flightSegmentTOs != null) {
			for (FlightSegmentTO fltSeg : flightSegmentTOs) {
				if (fltSeg.isDomesticFlight()) {
					return true;
				}
			}
		}
		return false;
	}

// Checking Pax Title
	private static boolean checkTitle(String title, Collection<String[]> paxTitles) {
		for (String[] paxTitle : paxTitles) {
			if (paxTitle[0].equals(title)) {;
				return true;
			}
		}
		return false;
	}
	
	public static String getAutomaticCheckinEmail(List<AAAirTravelerType> list, String rph) {
		String automaticCheckinEmail = null;
		if (list != null) {
			for (AAAirTravelerType travelerType : list) {
				if (rph.equals(travelerType.getTravelerRefNumberRPH())) {
					automaticCheckinEmail = travelerType.getAutoCheckinEmail();
					break;
				}
			}
		}
		return automaticCheckinEmail;
	}
}
