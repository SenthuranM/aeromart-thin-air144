package com.isa.thinair.webservices.core.session;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.IOTACodeTables;

import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

/**
 * Gatekeepers to the session
 * 
 * Handles Session In and Out operations
 *
 */
public class SessionGateway {

	private static final Log log = LogFactory.getLog(SessionGateway.class);

	/**
	 * Handle user session initialization before service call.
	 */
	public static class SessionInHandler {

		/**
		 * 
		 * Initialize UserSession, if not already exists.
		 */
		public static void handleSessionIn(String userId, String password, int salesChannelId) throws WebservicesException {
			log.debug("SessionInHandler::handleSessionIn called");
			AAUserSession userSession = null;
			if (!isAuthenticatedInSession(userId, password, salesChannelId)) {
				userSession = WebServicesModuleUtils.getSessionStore().createNewUserSession(userId, password, salesChannelId);
			} else {
				userSession = WebServicesModuleUtils.getSessionStore().getUserSession(userId);
			}

			// Bind user-session to current thread
			ThreadLocalData.setCurrentUserSession(userSession);

			// Bind user-principal to current thread
			UserPrincipal userPrincipal = (UserPrincipal) userSession.getParameter(AAUserSession.USER_PRINCIPAL);
			ThreadLocalData.setCurrentUserPrincipal(userPrincipal);
		}

		/**
		 * Checks if User is already authenticated
		 * 
		 * Throws WebservicesException if user is already authenticated and UserSession credentials does not match with
		 * request credentials.
		 */
		private static boolean isAuthenticatedInSession(String userName, String password, int salesChannelId)
				throws WebservicesException {
			boolean authenticated = false;

			AAUserSession aaUserSession = WebServicesModuleUtils.getSessionStore().getUserSession(userName);
			if (aaUserSession != null) {
				UserPrincipal userPrincipal = (UserPrincipal) aaUserSession.getParameter(AAUserSession.USER_PRINCIPAL);

				if (userName.equals(userPrincipal.getUserId()) && password.equals(userPrincipal.getPassword())
						&& salesChannelId == userPrincipal.getSalesChannel()) {
					authenticated = true;
				} else {
					log.error("Authenticated username/password does not match with supplied credentials " + "[userId=" + userName
							+ "]");
					throw new WebservicesException(IOTACodeTables.ErrorWarningType_EWT_AUTHENTICATION,
							"Authenticated username/password does not match with supplied credentials " + "[userId=" + userName
									+ "]");
				}
			}
			return authenticated;
		}
	}

	/**
	 * Handle user session after service call.
	 * 
	 * @author Nasly
	 */
	public static class SessionOutHandler {

		public static void handleSessionOut() {
			log.debug("SessionOutHandler::handleSessionOut called");

			try {
				WebServicesModuleUtils.getSessionStore().flushUserSession(ThreadLocalData.getCurrentUserSession());
			} catch (Exception ex) {
				log.error("Failure in flushUserSession", ex);
			}

			try {
				// Unset the UserPrincipal from the current thread
				ThreadLocalData.unsetCurrentUserPrincipal();
			} catch (Exception ex) {
				log.error("Failure in SessionOutHandler for unsetCurrentUserPrincipal ", ex);
			}

			try {
				// Unset the WebservicesContext from the current thread
				ThreadLocalData.unsetCurrentWSContext();
			} catch (Exception ex) {
				log.error("Failure in SessionOutHandler for unsetCurrentWSContext", ex);
			}

			try {
				// Unset the UserSession from the current thread
				ThreadLocalData.unsetCurrentUserSession();
			} catch (Exception ex) {
				log.error("Failure in SessionOutHandler for unsetCurrentUserSession", ex);
			}

		}
	}

}
