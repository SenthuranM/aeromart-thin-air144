package com.isa.thinair.webservices.core.cache.util;

public class CacheConstant {

	public static String cacheModuleSeparator = "-";

	public static String cacheDataSeparator = "_";

	public final static String CACHE_SERVICE_ERROR = "Error Processing your Request,Please re-check";

	public enum CacheModuleName {
		WS
	};

	public interface CacheableBean {
		String CACHE_AVAILABILITY_SEARCH_BEAN = "cacheAvailabilitySearchBean";
		String CACHE_ALLPRICEAVAILABILITY_SEARCH_BEAN = "cacheAllPriceAvailabilitySearchBean";
		String CACHE_PRICE_QUOTE_BEAN = "cachePriceQuoteSearchBean";
	}

	public enum ReflectionMethodName {

		ERRORS("Errors"), WARNINGS("Warnings"), SUCCESS_AND_WARNINGS_AND_PRICEDITINERARIES(
				"SuccessAndWarningsAndPricedItineraries");

		private String name;

		private ReflectionMethodName(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

	public enum CacheKeyParamName {
		CHANNEL("CH"), CABIN_CLASS("CL"), BOOKING_CLASS("BC"), CURRENCY("CUR"), PAX_QTY("PQ"), SEARCH_VARIANCE_BEFORE("VB"), SEARCH_VARIANCE_AFTER(
				"VA"), ORIGIN("ORI"), DESTINATION("DES"), DEPARTURE_DATE("DEP"), DIRECT_FLIGHT("DF"), FLEXI_QUOTE("FQ"), RPH(
				"RPH"), BUNDLED("BD"), SEAT("ST"), MEAL("ML"), SSR("SSR"), BAGGAGE("BAG");

		private String name;

		private CacheKeyParamName(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

	}

	public enum Operation {

		AVAILABILITY_SEARCH("getAvailability"), PRICE_QUOTE("getPrice"), ALL_PRICE_AVAILABILITY_SEARCH("getAllPriceAvailability");

		private String code;

		private Operation(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

	}

	public enum Audit {

		SESSION_ID("SESSION_ID=<"), TRANSACTION_ID("TRANSACTION_ID=<"), RESPONSE_TIME("RESPONSE_TIME=<"), SUCCESS("SUCCESS=<"), CACHE(
				"CACHE=<"), USER_ID("USER_ID=<"), OPERATION("OPERATION=<"), JOURNEY_INFO("JOURNEY_INFO=<"), QUANTITY("QUANTITY=<"), ERROR_CODE(
				"ERROR_CODE=<");

		private String code;

		private Audit(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
	}

	public enum Delimeters {

		END_AND_CONTINUE(">,"), END(">"), SEPARATOR(":");

		private String code;

		private Delimeters(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
	}

}
