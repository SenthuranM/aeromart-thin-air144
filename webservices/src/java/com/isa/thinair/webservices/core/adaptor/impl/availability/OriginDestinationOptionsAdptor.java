package com.isa.thinair.webservices.core.adaptor.impl.availability;

import org.opentravel.ota._2003._05.AAOTAAirAllPriceAvailRS;
import org.opentravel.ota._2003._05.OTAAirAvailRS;

import com.isa.thinair.webservices.core.adaptor.Adaptor;
import com.isa.thinair.webservices.core.adaptor.util.AdaptorUtil;

public class OriginDestinationOptionsAdptor
		implements
		Adaptor<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions, AAOTAAirAllPriceAvailRS.OriginDestinationInformation.OriginDestinationOptions> {
	@Override
	public AAOTAAirAllPriceAvailRS.OriginDestinationInformation.OriginDestinationOptions adapt(
			OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions source) {

		AAOTAAirAllPriceAvailRS.OriginDestinationInformation.OriginDestinationOptions target = new AAOTAAirAllPriceAvailRS.OriginDestinationInformation.OriginDestinationOptions();

		OriginDestinationOptionAdaptor adaptor = new OriginDestinationOptionAdaptor();
		AdaptorUtil.adaptCollection(source.getOriginDestinationOption(), target.getOriginDestinationOption(), adaptor);

		return target;
	}
}