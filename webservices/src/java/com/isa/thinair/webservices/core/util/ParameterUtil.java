package com.isa.thinair.webservices.core.util;

import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class ParameterUtil {
	
	public static boolean isApplyBSPFee() {
		return  AppSysParamsUtil.isApplyBSPTransactionFee(ApplicationEngine.WS);
	}
	
	public static boolean isApplyAdminFee(String agentCode) {
	   return AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.WS, agentCode);
	}
}
