/**
 * 
 */
package com.isa.thinair.webservices.core.cache.delegator.throttle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.IOTACodeTables;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webservices.api.exception.WebservicesException;

public class ThrottleManager {

	private final static Log log = LogFactory.getLog(ThrottleManager.class);

	private boolean enabledThrottling = true;	
	private Integer throttleDuration = 1000;
	private Integer throttleLimit;

	private Long lastExecutionTime;
	private Integer executionCount = 0;

	public boolean isAllowed() throws ModuleException, WebservicesException {
		
		if (enabledThrottling) {
			validateThrottleLimit();
			updateThrottleCounts();
		}

		return true;
	}

	private void validateThrottleLimit() throws WebservicesException {
		if (getLastExecutionTime() != null) {
			increaseExecutionCount();

			long timeElapsed = System.currentTimeMillis() - getLastExecutionTime();

			if ((timeElapsed < throttleDuration && getExecutionCount() > getThrottleLimit()) || getThrottleLimit() == 0) {
				log.error("Throttle limit exceeded, Limit is " + getThrottleLimit() + " per " + getThrottleDuration()
						+ " miliseconds. Reached " + getExecutionCount());

				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_THROTTLE_LIMIT_REACHED_PLEASE_TRY_AGAIN, "Throttle Limit Reached.Please Try Again");

			} else if (timeElapsed > throttleDuration) {
				resetExecutionCount();
			}
		}
	}

	private void updateThrottleCounts() {
		increaseExecutionCount();
		setLastExecutionTime(System.currentTimeMillis());
	}

	/**
	 * @return the throttleDuration
	 */
	public Integer getThrottleDuration() {
		return throttleDuration;
	}

	/**
	 * @param throttleDuration
	 *            the throttleDuration to set
	 */
	public void setThrottleDuration(Integer throttleDuration) {
		this.throttleDuration = throttleDuration;
	}

	/**
	 * @return the throttleLimit
	 */
	public Integer getThrottleLimit() {
		return throttleLimit;
	}

	/**
	 * @param throttleLimit
	 *            the throttleLimit to set
	 */
	public void setThrottleLimit(Integer throttleLimit) {
		this.throttleLimit = throttleLimit;
	}

	public Long getLastExecutionTime() {
		return lastExecutionTime;
	}

	/**
	 * @param lastExecutionTime
	 *            the lastExecutionTime to set
	 */
	public void setLastExecutionTime(Long lastExecutionTime) {
		this.lastExecutionTime = lastExecutionTime;
	}

	/**
	 * @return the executionCount
	 */
	public Integer getExecutionCount() {
		return executionCount;
	}

	public void increaseExecutionCount() {
		++this.executionCount;
	}

	public void resetExecutionCount() {
		this.executionCount = 0;
	}

	public boolean isEnabledThrottling() {
		return enabledThrottling;
	}

	public void setEnabledThrottling(boolean enabledThrottling) {
		this.enabledThrottling = enabledThrottling;
	}
	

}
