package com.isa.thinair.webservices.core.adaptor.impl.availability;

import org.opentravel.ota._2003._05.AAOTAAirAllPriceAvailRS;
import org.opentravel.ota._2003._05.OTAAirAvailRS;

import com.isa.thinair.webservices.core.adaptor.Adaptor;
import com.isa.thinair.webservices.core.adaptor.util.AdaptorUtil;

public class FlightSegmentAdaptor
		implements
		Adaptor<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment, AAOTAAirAllPriceAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment> {

	@Override
	public
			AAOTAAirAllPriceAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment
			adapt(OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment source) {

		AAOTAAirAllPriceAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment target = new AAOTAAirAllPriceAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment();

		target.setAccumulatedDuration(source.getAccumulatedDuration());
		target.setArrivalAirport(source.getArrivalAirport());
		target.setArrivalDateTime(source.getArrivalDateTime());
		target.setDepartureAirport(source.getDepartureAirport());
		target.setDepartureDateTime(source.getDepartureDateTime());

		target.setDistance(source.getDistance());
		target.setFlightNumber(source.getFlightNumber());
		target.setGroundDuration(source.getGroundDuration());
		target.setInfoSource(source.getInfoSource());
		target.setJourneyDuration(source.getJourneyDuration());
		target.setMarketingAirline(source.getMarketingAirline());
		target.setOnTimeRate(source.getOnTimeRate());
		target.setOperatingAirline(source.getOperatingAirline());
		target.setParticipationLevelCode(source.getParticipationLevelCode());
		target.setReturnFlag(source.isReturnFlag());
		target.setRPH(source.getRPH());
		target.setSegmentCode(source.getSegmentCode());
		target.setSmokingAllowed(source.isSmokingAllowed());
		target.setStopQuantity(source.getStopQuantity());
		// target.setTicket(source.getTicket());
		target.setTourOperatorFlightID(source.getTourOperatorFlightID());
		target.setTrafficRestrictionInfo(source.getTrafficRestrictionInfo());

		target.getComment().addAll(source.getComment());
		target.getMarketingCabin().addAll(source.getMarketingCabin());
		AdaptorUtil.adaptCollection(source.getBookingClassAvail(), target.getBookingClassAvail(), new BookingClassAvailAdaptor());

		return target;
	}

}
