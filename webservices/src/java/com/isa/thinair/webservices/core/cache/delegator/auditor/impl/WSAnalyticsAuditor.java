package com.isa.thinair.webservices.core.cache.delegator.auditor.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.cache.util.CacheConstant;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.WebservicesContext;

public class WSAnalyticsAuditor {

	private final static String WS_ANALYSTICS_LOG = "ws-analystics";
	private final static Log log = LogFactory.getLog(WS_ANALYSTICS_LOG);

	public static void audit(boolean success, long startTime, boolean servedFromCache, String source, Exception exception) {

		if (log.isDebugEnabled()) {
			StringBuilder content = new StringBuilder();

			try {
				String transactionId = null;
				if (ThreadLocalData.getCurrentTransaction() != null) {
					transactionId = ThreadLocalData.getCurrentTransaction().getId();
				}

				String errorCode = getErrorCode(exception);

				String userId = null;
				UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();
				if (userPrincipal != null) {
					userId = userPrincipal.getUserId();
				}

				content.append(CacheConstant.Audit.SESSION_ID.getCode());
				content.append(ThreadLocalData.getWSContextParam(WebservicesContext.WEB_SESSION_ID));
				content.append(CacheConstant.Delimeters.END_AND_CONTINUE.getCode());

				content.append(CacheConstant.Audit.TRANSACTION_ID.getCode());
				content.append(transactionId);
				content.append(CacheConstant.Delimeters.END_AND_CONTINUE.getCode());

				content.append(CacheConstant.Audit.SUCCESS.getCode());
				content.append(success);
				content.append(CacheConstant.Delimeters.END_AND_CONTINUE.getCode());

				content.append(CacheConstant.Audit.RESPONSE_TIME.getCode());
				content.append((System.currentTimeMillis() - startTime));
				content.append(CacheConstant.Delimeters.END_AND_CONTINUE.getCode());

				content.append(CacheConstant.Audit.CACHE.getCode());
				content.append(servedFromCache);
				content.append(CacheConstant.Delimeters.END_AND_CONTINUE.getCode());

				content.append(CacheConstant.Audit.USER_ID.getCode());
				content.append(userId);
				content.append(CacheConstant.Delimeters.END_AND_CONTINUE.getCode());

				content.append(CacheConstant.Audit.ERROR_CODE.getCode());
				content.append(errorCode);
				content.append(CacheConstant.Delimeters.END_AND_CONTINUE.getCode());

				content.append(source);
				log.debug(content.toString());

			} catch (Exception ex) {
				log.error("Error in Webservice analytics for the content:" + content.toString(), ex);
			}

		}

	}

	private static String getErrorCode(Exception exception) {
		String errorCode = null;
		if (exception != null) {
			if (exception instanceof WebservicesException) {
				WebservicesException wse = (WebservicesException) exception;
				Map<String, String> errorCodes = wse.getOtaErrorCodes();
				if (errorCodes != null && !errorCodes.isEmpty()) {
					for (String key : errorCodes.keySet()) {
						errorCode = key + CacheConstant.Delimeters.SEPARATOR.getCode() + errorCodes.get(key);
						break;
					}
				}
			}
		}
		return errorCode;
	}
}
