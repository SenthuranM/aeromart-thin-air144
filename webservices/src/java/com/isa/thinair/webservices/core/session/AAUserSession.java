package com.isa.thinair.webservices.core.session;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Holds user data during user session.
 * 
 * @author Nasly
 */
public class AAUserSession implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1432944011278506290L;
	/** User Session data keys */
	public static final String USER_PRINCIPAL = "userPrincipal";
	public static final String USER = "user";
	public static final String USER_AGENT = "userAgent";
	public static final String USER_PRIVILEGES_KEYS = "userPrivKeys";

	/** Holds all the user related data */
	private Map<String, Object> userData = Collections.synchronizedMap(new HashMap<String, Object>());

	/** For expiration purposes */
	private long lastAccessed;

	private int status = 0;

	public AAUserSession() {
		updateLastAccessed();
	}

	/**
	 * Add parameter to the UserSession
	 * 
	 * @param key
	 * @param value
	 */
	public void addParameter(String key, Object value) {
		userData.put(key, value);
		updateLastAccessed();
	}

	/**
	 * Get parameter from the UserSession
	 * 
	 * @param key
	 * @return
	 */
	public Object getParameter(String key) {
		updateLastAccessed();
		return userData.get(key);
	}

	/**
	 * Remove parameter from the UserSession
	 * 
	 * @param key
	 * @return
	 */
	public Object removeParameter(String key) {
		updateLastAccessed();
		return userData.remove(key);
	}

	/**
	 * Sets the last accessed time
	 */
	private void updateLastAccessed() {
		if (getStatus() == STATUS_EXPIRED) {
			throw new RuntimeException("User Session Expired");
		}
		lastAccessed = System.currentTimeMillis();
	}

	/**
	 * Returns last accessed time in millis
	 * 
	 * @return
	 */
	public long getLastAccessedTimestamp() {
		return lastAccessed;
	}

	/**
	 * Returns session status.
	 * 
	 * @return
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * Sets session status.
	 * 
	 * @param status
	 */
	private void setStatus(int status) {
		this.status = status;
	}

	/**
	 * Expire session if timed out.
	 * 
	 * @param sessionTimeoutDuration
	 */
	public boolean qualifyForExpiration(long sessionTimeoutDuration) {
		boolean isExpired = false;
		if (System.currentTimeMillis() - getLastAccessedTimestamp() > sessionTimeoutDuration) {
			setStatus(STATUS_EXPIRED);
			userData.clear();
			isExpired = true;
		}
		return isExpired;
	}

	/** Session statuses */
	private static int STATUS_ACTIVE = 1;

	private static int STATUS_EXPIRED = -1;
}
