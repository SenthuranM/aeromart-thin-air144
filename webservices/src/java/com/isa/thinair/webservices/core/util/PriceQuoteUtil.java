/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AABundledServiceExt;
import org.opentravel.ota._2003._05.AirFeeType;
import org.opentravel.ota._2003._05.AirItineraryPricingInfoType;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.AirTaxType;
import org.opentravel.ota._2003._05.AirTripType;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.BundledService;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.FareType;
import org.opentravel.ota._2003._05.FareType.BaseFare;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirPriceRQ;
import org.opentravel.ota._2003._05.OTAAirPriceRS;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.PTCFareBreakdownType;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.PricedItinerariesType;
import org.opentravel.ota._2003._05.PricedItineraryType;
import org.opentravel.ota._2003._05.SSRType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.BaggageRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.BaggageRequests.BaggageRequest;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.MealRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.MealRequests.MealRequest;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SSRRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SSRRequests.SSRRequest;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SeatRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SeatRequests.SeatRequest;
import org.opentravel.ota._2003._05.StringStringMap;
import org.opentravel.ota._2003._05.SuccessType;
import org.opentravel.ota._2003._05.TravelerInfoSummaryType;
import org.opentravel.ota._2003._05.TravelerInformationType;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.util.AvailableFlightSearchDTOBuilder;
import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.SpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.ExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.converters.FareConvertUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;
import com.isa.thinair.webservices.api.dtos.BundledServiceDTO;
import com.isa.thinair.webservices.api.dtos.FlexiFareSelectionCriteria;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.AAUtils;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.bl.AmadeusCommonUtil;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;

/**
 * Manipulates OTA_PriceRQ/RS
 * 
 * @author Mohamed Nasly
 */
public class PriceQuoteUtil extends BaseUtil {

	private final Log log = LogFactory.getLog(getClass());

	public PriceQuoteUtil() {
		super();
	}

	public OTAAirPriceRS getPrice(OTAAirPriceRQ otaAirPriceRQ) throws ModuleException, WebservicesException {
		OTAAirPriceRS otaAirPriceRS = new OTAAirPriceRS();
		otaAirPriceRS.setErrors(new ErrorsType());
		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
		TrackInfoDTO trackInfo = AAUtils.getTrackInfo();
		OTAUtils.logRequestResponseDetails(WebservicesConstants.RequestResponseTypes.PRICE_REQUEST, principal, trackInfo,
				otaAirPriceRQ.getTransactionIdentifier());

		AvailableFlightSearchDTO availableFlightSearchDTO = extractPriceQuoteReqParamsNew(otaAirPriceRQ);
		if (availableFlightSearchDTO != null) {

			// As the user is initiating a new price quote, we have to clear the fare quote related parameters from the
			// user tnx.
			PriceQuoteUtil.clearPriceQuoteRelatedTransactionParams();

			SelectedFlightDTO selectedFlightDTO = WebServicesModuleUtils.getReservationQueryBD().getFareQuote(
					availableFlightSearchDTO, null);

			if (selectedFlightDTO != null) {
				ServiceTaxContainer applicableServiceTax = WebServicesModuleUtils.getReservationBD().getApplicableServiceTax(
						selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND), EXTERNAL_CHARGES.JN_OTHER);
				if (applicableServiceTax != null) {
					List<ServiceTaxContainer> applicableServiceTaxes = new ArrayList<ServiceTaxContainer>();
					applicableServiceTaxes.add(applicableServiceTax);
					ThreadLocalData.setCurrentTnxParam(Transaction.APPLICABLE_SERVICE_TAXES, applicableServiceTaxes);
				}
			}

			// For the selected flight save the FareSegChargeTO in the user tnx.
			PriceQuoteUtil.addFareSegmentChargeTOToTransaction(selectedFlightDTO, availableFlightSearchDTO);

			preparePriceQuoteResParams(otaAirPriceRQ, otaAirPriceRS, selectedFlightDTO, availableFlightSearchDTO);
		}
		OTAUtils.logRequestResponseDetails(WebservicesConstants.RequestResponseTypes.PRICE_RESPONSE, principal, trackInfo,
				otaAirPriceRQ.getTransactionIdentifier());
		return otaAirPriceRS;
	}

	public AvailableFlightSearchDTO extractPriceQuoteReqParamsNew(OTAAirPriceRQ otaAirPriceRQ) throws ModuleException,
			WebservicesException {

		AvailableFlightSearchDTO availableFlightSearchDTO = new AvailableFlightSearchDTO();

		Collection<String> bookingClassSet = new HashSet<String>();

		// one and only one POS is supported
		if (otaAirPriceRQ.getPOS().getSource().size() != 1) {
			log.error("ERROR detected in extractPriceQuoteReqParams(OTAAirAvailRQ). " + "Invalid POS. " + "[POS count="
					+ otaAirPriceRQ.getPOS().getSource().size() + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"[Only one and only POS is supported, Received " + otaAirPriceRQ.getPOS().getSource().size()
							+ " POS records]");
		}

		boolean isReturnJourney = false;
		List<OriginDestinationOptionType> originDestinationOptions = otaAirPriceRQ.getAirItinerary()
				.getOriginDestinationOptions().getOriginDestinationOption();

		List<FlightSegmentDTO> allFlightSegs = new ArrayList<FlightSegmentDTO>();

		for (OriginDestinationOptionType originDestinationOption : originDestinationOptions) {
			int bookingClassCount = 0;
			for (BookFlightSegmentType bookFlightSegment : originDestinationOption.getFlightSegment()) {
				FlightSegmentDTO aaFlightSegmentDTO = new FlightSegmentDTO();
				aaFlightSegmentDTO.setFromAirport(bookFlightSegment.getDepartureAirport().getLocationCode());
				aaFlightSegmentDTO.setToAirport(bookFlightSegment.getArrivalAirport().getLocationCode());
				aaFlightSegmentDTO.setFlightNumber(bookFlightSegment.getFlightNumber());
				aaFlightSegmentDTO.setDepartureDateTime(bookFlightSegment.getDepartureDateTime().toGregorianCalendar().getTime());
				aaFlightSegmentDTO.setArrivalDateTime(bookFlightSegment.getArrivalDateTime().toGregorianCalendar().getTime());

				if (bookFlightSegment.getResBookDesigCode() != null) {
					bookingClassSet.add(bookFlightSegment.getResBookDesigCode());
					bookingClassCount++;
				}
				allFlightSegs.add(aaFlightSegmentDTO);
			}

			if (bookingClassCount != 0 && bookingClassCount != originDestinationOption.getFlightSegment().size()) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
						"Booking class not specified in all segments");
			}

		}

		List<FlightSegmentDTO> filledOutboundSegsList = null;
		List<FlightSegmentDTO> filledInboundSegsList = null;
		List<FlightSegmentDTO> filledAllSegsList = null;

		if (!allFlightSegs.isEmpty()) {
			Collection<FlightSegmentDTO> filledAllFlightSegmentDTOs = WebServicesModuleUtils.getFlightBD()
					.getFilledFlightSegmentDTOs(allFlightSegs);
			if (filledAllFlightSegmentDTOs.size() != allFlightSegs.size()) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Invalid  Segment(s)]");
			}
			filledAllSegsList = new ArrayList<FlightSegmentDTO>(filledAllFlightSegmentDTOs);
			Collections.sort(filledAllSegsList);
		}

		int segIndex = 0;
		int returnStartingIndex = -1;

		for (FlightSegmentDTO flightSeg : filledAllSegsList) {
			if (segIndex > 0 && flightSeg.getFromAirport().equals(filledAllSegsList.get(segIndex - 1).getToAirport())
					&& flightSeg.getDepartureDateTime().after(filledAllSegsList.get(segIndex - 1).getArrivalDateTime())) {

				if (flightSeg.getToAirport().equals(filledAllSegsList.get(segIndex - 1).getFromAirport())
						&& filledAllSegsList.get(0).getFromAirport()
								.equals(filledAllSegsList.get(filledAllSegsList.size() - 1).getToAirport())) {

					returnStartingIndex = segIndex;
					isReturnJourney = true;
					break;
				}
			} else if (segIndex > 0) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Invalid Journey]");
			}

			segIndex++;

		}

		if (returnStartingIndex != -1) {
			filledOutboundSegsList = filledAllSegsList.subList(0, returnStartingIndex);
			filledInboundSegsList = filledAllSegsList.subList(returnStartingIndex, filledAllSegsList.size());
		} else {
			filledOutboundSegsList = filledAllSegsList;
		}

		// we only support booking class per one journey
		if (!bookingClassSet.isEmpty() && bookingClassSet.size() > 1) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Too many booking classes specified");
		}
		String bookingClass = BeanUtils.getFirstElement(bookingClassSet);
		String bookingType = BookingClass.BookingClassType.NORMAL;
		String cabinClass = null;

		if (bookingClass != null) {
			Collection<String> standByBCList = WebServicesModuleUtils.getBookingClassBD().getStandbyBookingClasses();
			if (standByBCList.contains(bookingClass)) {
				bookingType = BookingClass.BookingClassType.STANDBY;
				bookingClass = null;
			} else {
				cabinClass = WebServicesModuleUtils.getBookingClassBD().getCabinClassForBookingClass(bookingClass);
			}
		}

		if (bookingClass == null) {
			if (AppSysParamsUtil.getDefaultCOS() != null && !AppSysParamsUtil.getDefaultCOS().isEmpty()) {
				cabinClass = AppSysParamsUtil.getDefaultCOS();
			} else {
				cabinClass = "Y";
			}
		}

		BundledServiceDTO bundledServiceDTO = createBundledServiceDTO(otaAirPriceRQ);
		Map<Integer, Set<String>> ondBookingClassSelection = bundledServiceDTO.getOndBookingClasses();
		Map<Integer, Map<String, String>> ondSegBookingClasses = bundledServiceDTO.getOndSegBookingClasses();

		overrideFlexiSelection(bundledServiceDTO.getOndFlexiSelection(), otaAirPriceRQ);

		// If the Flexi fare is selected need to set the flexi quoting.
		Map<Integer, Boolean> ondQuoteFlexi = availableFlightSearchDTO.getOndQuoteFlexi();
		if (otaAirPriceRQ.getFlexiFareSelectionOptions() != null) {
			if (otaAirPriceRQ.getFlexiFareSelectionOptions().getInBoundFlexiSelected() != null
					&& otaAirPriceRQ.getFlexiFareSelectionOptions().getInBoundFlexiSelected() == true) {
				ondQuoteFlexi.put(OndSequence.IN_BOUND, true);
			}

			if (otaAirPriceRQ.getFlexiFareSelectionOptions().getOutBoundFlexiSelected() != null
					&& otaAirPriceRQ.getFlexiFareSelectionOptions().getOutBoundFlexiSelected() == true) {
				ondQuoteFlexi.put(OndSequence.OUT_BOUND, true);
			}
		}

		OTAAirPriceRQ.BundledServiceSelectionOptions bundledServiceSelection = otaAirPriceRQ.getBundledServiceSelectionOptions();
		Integer outboundBundledServiceId = null;
		Integer inboundBundledServiceId = null;
		if (bundledServiceSelection != null) {
			outboundBundledServiceId = bundledServiceSelection.getOutBoundBunldedServiceId();
			inboundBundledServiceId = bundledServiceSelection.getInBoundBunldedServiceId();
		}

		OriginDestinationInfoDTO outboundOndInfo = new OriginDestinationInfoDTO();
		outboundOndInfo.setOrigin(filledOutboundSegsList.get(0).getFromAirport());
		outboundOndInfo.setDestination(filledOutboundSegsList.get(filledOutboundSegsList.size() - 1).getToAirport());
		outboundOndInfo.setDepartureDateTimeStart(CalendarUtil
				.getStartTimeOfDate(filledOutboundSegsList.get(0).getDepatureDate()));
		outboundOndInfo.setDepartureDateTimeEnd(CalendarUtil.getEndTimeOfDate(filledOutboundSegsList.get(0).getDepatureDate()));

		List<Integer> outboundFlightSegmentIds = new ArrayList<Integer>();
		for (FlightSegmentDTO flightSegmentDTO : filledOutboundSegsList) {
			if (flightSegmentDTO.getSegmentId() != null) {
				outboundFlightSegmentIds.add(flightSegmentDTO.getSegmentId());
			} else {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Invalid Inbound Segment. "
						+ flightSegmentDTO.toString() + "]");
			}
		}
		outboundOndInfo.setFlightSegmentIds(outboundFlightSegmentIds);
		outboundOndInfo.setPreferredBookingType(bookingType);
		outboundOndInfo.setPreferredClassOfService(cabinClass);

		if (outboundBundledServiceId != null && ondBookingClassSelection != null) {
			Set<String> bookingClasses = ondBookingClassSelection.get(OndSequence.OUT_BOUND);
			Map<String, String> segBookingClasses = ondSegBookingClasses.get(OndSequence.OUT_BOUND);
			if (bookingClasses != null) {
				if (bookingClasses.size() == 1) {
					outboundOndInfo.setPreferredBookingClass(bookingClasses.iterator().next());
				} else {
					outboundOndInfo.setSegmentBookingClass(segBookingClasses);
				}
			}
		}

		if (ondQuoteFlexi.get(OndSequence.OUT_BOUND) != null) {
			outboundOndInfo.setQuoteFlexi(ondQuoteFlexi.get(OndSequence.OUT_BOUND));
		}

		outboundOndInfo.setPreferredBundledFarePeriodId(outboundBundledServiceId);
		
		availableFlightSearchDTO.addOriginDestination(outboundOndInfo);

		if (isReturnJourney) {
			OriginDestinationInfoDTO inboundOndInfo = new OriginDestinationInfoDTO();
			inboundOndInfo.setOrigin(outboundOndInfo.getDestination());
			inboundOndInfo.setDestination(outboundOndInfo.getOrigin());
			inboundOndInfo.setDepartureDateTimeStart(CalendarUtil.getStartTimeOfDate(filledInboundSegsList.get(0)
					.getDepartureDateTime()));
			inboundOndInfo.setDepartureDateTimeEnd(CalendarUtil.getEndTimeOfDate(filledInboundSegsList.get(0)
					.getDepartureDateTime()));

			List<Integer> inboundFlightSegmentIds = new ArrayList<Integer>();
			for (FlightSegmentDTO flightSegmentDTO : filledInboundSegsList) {
				if (flightSegmentDTO.getSegmentId() != null) {
					inboundFlightSegmentIds.add(flightSegmentDTO.getSegmentId());
				} else {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Invalid Inbound Segment. "
							+ flightSegmentDTO.toString() + "]");
				}
			}
			inboundOndInfo.setFlightSegmentIds(inboundFlightSegmentIds);
			inboundOndInfo.setPreferredBookingType(bookingType);
			inboundOndInfo.setPreferredClassOfService(cabinClass);

			if (inboundBundledServiceId != null && ondBookingClassSelection != null) {
				Set<String> bookingClasses = ondBookingClassSelection.get(OndSequence.IN_BOUND);
				Map<String, String> segBookingClasses = ondSegBookingClasses.get(OndSequence.IN_BOUND);
				if (bookingClasses != null) {
					if (bookingClasses.size() == 1) {
						inboundOndInfo.setPreferredBookingClass(bookingClasses.iterator().next());
					} else {
						inboundOndInfo.setSegmentBookingClass(segBookingClasses);
					}
				}
			}

			if (ondQuoteFlexi.get(OndSequence.IN_BOUND) != null) {
				inboundOndInfo.setQuoteFlexi(ondQuoteFlexi.get(OndSequence.IN_BOUND));
			}

			inboundOndInfo.setPreferredBundledFarePeriodId(inboundBundledServiceId);
			availableFlightSearchDTO.addOriginDestination(inboundOndInfo);
		}

		// pax counts
		TravelerInfoSummaryType travelerInfoSummary = otaAirPriceRQ.getTravelerInfoSummary();
		int adults = 0;
		int children = 0;
		int infants = 0;
		for (TravelerInformationType travelerInformationType : travelerInfoSummary.getAirTravelerAvail()) {
			for (PassengerTypeQuantityType passengerTypeQuantityType : travelerInformationType.getPassengerTypeQuantity()) {
				if (passengerTypeQuantityType.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
					adults += passengerTypeQuantityType.getQuantity();
				} else if (passengerTypeQuantityType.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
					children += passengerTypeQuantityType.getQuantity();
				} else if (passengerTypeQuantityType.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
					infants += passengerTypeQuantityType.getQuantity();
				}
			}
		}
		if (adults == 0 && children == 0) {
			log.error("ERROR detected in extractPriceQuoteReqParams(OTAAirAvailRQ). " + "Invalid adult/children pax count. "
					+ "[adults pax=" + adults + "]" + "[children pax=" + children + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AT_LEAST_ONE_ADULT_MUST_BE_INCLUDED_,
					"Adult and Children pax count cannot be zero");
		}
		availableFlightSearchDTO.setAdultCount(adults);
		availableFlightSearchDTO.setChildCount(children);
		availableFlightSearchDTO.setInfantCount(infants);

		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
		availableFlightSearchDTO.setPosAirport(principal.getAgentStation());
		availableFlightSearchDTO.setAgentCode(principal.getAgentCode());

		availableFlightSearchDTO.setAppIndicator(ApplicationEngine.WS.toString());

		availableFlightSearchDTO.setChannelCode(SalesChannelsUtil
				.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));

		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
		
		int availabilityRestriction = (AuthorizationUtil.hasPrivileges(privilegeKeys, PriviledgeConstants.MAKE_RES_ALLFLIGHTS) || BookingClass.BookingClassType.STANDBY
				.equals(bookingType))
				? AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION
				: AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED;

		availableFlightSearchDTO.setAvailabilityRestrictionLevel(availabilityRestriction);

		String preferredCurrencyCode = null;
		if (travelerInfoSummary.getPriceRequestInformation() != null) {
			preferredCurrencyCode = travelerInfoSummary.getPriceRequestInformation().getCurrencyCode();
		}
		availableFlightSearchDTO.setPreferredCurrencyCode(preferredCurrencyCode);

		boolean blnAllowHalfReturnFaresForNewBookings = AppSysParamsUtil.isAllowHalfReturnFaresForNewBookings();
		if (isReturnJourney && !availableFlightSearchDTO.isOpenReturnSearch()) {
			availableFlightSearchDTO.setHalfReturnFareQuote(blnAllowHalfReturnFaresForNewBookings);
		} else if (otaAirPriceRQ.getModifiedSegmentInfo() != null) {
			String pnr = otaAirPriceRQ.getModifiedSegmentInfo().getBookingReferenceID().get(0).getID();

			if (pnr != null) { // Availability request for Modify OND
				LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
				pnrModesDTO.setPnr(pnr);
				pnrModesDTO.setLoadFares(true);
				pnrModesDTO.setLoadSegView(true);
				Reservation reservation = getReservation(pnrModesDTO, null);

				AirItineraryType.OriginDestinationOptions modifiedOND = otaAirPriceRQ.getModifiedSegmentInfo().getAirItinerary()
						.getOriginDestinationOptions();
				// get the pnrSegIds after checking the validity
				List<Integer> pnrSegIdsForMod = extractPNRSegIds(reservation, modifiedOND);
				ReservationUtil.setExistingSegInfo(reservation, availableFlightSearchDTO, pnrSegIdsForMod);
			}
		}

		return availableFlightSearchDTO;
	}

	public static void overrideFlexiSelection(Map<Integer, Boolean> ondBundledFlexiSelection, OTAAirPriceRQ otaAirPriceRQ) {
		if (ondBundledFlexiSelection != null && !ondBundledFlexiSelection.isEmpty()) {

			if (otaAirPriceRQ.getFlexiFareSelectionOptions() != null) {
				if (otaAirPriceRQ.getFlexiFareSelectionOptions().getInBoundFlexiSelected() == null
						|| otaAirPriceRQ.getFlexiFareSelectionOptions().getInBoundFlexiSelected() == false) {
					otaAirPriceRQ.getFlexiFareSelectionOptions().setOutBoundFlexiSelected(
							ondBundledFlexiSelection.get(OndSequence.IN_BOUND));
				}

				if (otaAirPriceRQ.getFlexiFareSelectionOptions().getOutBoundFlexiSelected() == null
						|| otaAirPriceRQ.getFlexiFareSelectionOptions().getOutBoundFlexiSelected() == false) {
					otaAirPriceRQ.getFlexiFareSelectionOptions().setOutBoundFlexiSelected(
							ondBundledFlexiSelection.get(OndSequence.OUT_BOUND));
				}
			} else {
				otaAirPriceRQ.setFlexiFareSelectionOptions(new OTAAirPriceRQ.FlexiFareSelectionOptions());
				otaAirPriceRQ.getFlexiFareSelectionOptions().setOutBoundFlexiSelected(
						ondBundledFlexiSelection.get(OndSequence.OUT_BOUND));
				otaAirPriceRQ.getFlexiFareSelectionOptions().setInBoundFlexiSelected(
						ondBundledFlexiSelection.get(OndSequence.IN_BOUND));
			}
		}
	}

	@SuppressWarnings("unchecked")
	public static BundledServiceDTO createBundledServiceDTO(OTAAirPriceRQ otaAirPriceRQ) throws WebservicesException {
		BundledServiceDTO bundledServiceDTO = new BundledServiceDTO();
		OTAAirPriceRQ.BundledServiceSelectionOptions bundledServiceSelection = otaAirPriceRQ.getBundledServiceSelectionOptions();
		if (bundledServiceSelection != null) {
			Integer outboundBundledServiceId = bundledServiceSelection.getOutBoundBunldedServiceId();
			Integer inboundBundledServiceId = bundledServiceSelection.getInBoundBunldedServiceId();

			List<AABundledServiceExt> aaBundledServiceExts = (List<AABundledServiceExt>) ThreadLocalData
					.getCurrentTnxParam(Transaction.AVAILABLE_OND_BUNDLED_FARES);

			if (aaBundledServiceExts != null && !aaBundledServiceExts.isEmpty()) {

				setSelectedBundledServiceData(outboundBundledServiceId, OndSequence.OUT_BOUND, bundledServiceDTO);
				setSelectedBundledServiceData(inboundBundledServiceId, OndSequence.IN_BOUND, bundledServiceDTO);

			} else if (outboundBundledServiceId != null || inboundBundledServiceId != null) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_BUNDLED_SERVICE_SELECTED,
						" Invalid Bundled Service Selection");
			}

		}

		return bundledServiceDTO;
	}

	@SuppressWarnings("unchecked")
	public static AABundledServiceExt getOndBundledServiceObj(int ondSequence) {
		List<AABundledServiceExt> aaBundledServiceExts = (List<AABundledServiceExt>) ThreadLocalData
				.getCurrentTnxParam(Transaction.AVAILABLE_OND_BUNDLED_FARES);

		if (aaBundledServiceExts != null && !aaBundledServiceExts.isEmpty()) {
			for (AABundledServiceExt aaBundledServiceExt : aaBundledServiceExts) {
				int bundleOndSeq = aaBundledServiceExt.getApplicableOndSequence();
				if (bundleOndSeq == ondSequence) {
					return aaBundledServiceExt;
				}
			}
		}

		return null;
	}

	private static void setSelectedBundledServiceData(Integer ondSelectedBundledServiceId, int ondSequence,
			BundledServiceDTO bundledServiceDTO) throws WebservicesException {
		if (ondSelectedBundledServiceId != null) {
			AABundledServiceExt aaBundledServiceExt = getOndBundledServiceObj(ondSequence);
			if (aaBundledServiceExt == null) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_BUNDLED_SERVICE_SELECTED,
						" Invalid Bundled Service Selection");
			}

			setBundledServiceData(aaBundledServiceExt.getBundledService(), ondSelectedBundledServiceId, bundledServiceDTO,
					ondSequence);
		}
	}

	private static void setBundledServiceData(List<BundledService> bundledServices, int bundledServiceId,
			BundledServiceDTO bundledServiceDTO, int ondSequence) throws WebservicesException {
		boolean bundledServiceExists = false;
		List<String> bookingClasses = null;
		List<StringStringMap> segBookingClass = null;
		boolean isFlexiIncluded = false;
		if (bundledServices != null && !bundledServices.isEmpty()) {
			for (BundledService bundledService : bundledServices) {
				if (bundledService.getBunldedServiceId() == bundledServiceId) {
					bookingClasses = bundledService.getBookingClasses();
					segBookingClass = bundledService.getSegmentBookingClass();
					List<String> includedServices = bundledService.getIncludedServies();
					for (String serviceName : includedServices) {
						if (serviceName.equals(EXTERNAL_CHARGES.FLEXI_CHARGES.toString())) {
							isFlexiIncluded = true;
						}
					}
					bundledServiceExists = true;
				}
			}
		}

		if (!bundledServiceExists) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_BUNDLED_SERVICE_SELECTED,
					" Invalid Bundled Service Selection");
		}

		if (bookingClasses != null) {
			bundledServiceDTO.addBookingClass(ondSequence, new HashSet<String>(bookingClasses));
		}
		if (segBookingClass != null) {
			bundledServiceDTO.getOndSegBookingClasses().put(ondSequence, TransformUtil.transformStringStringMap(segBookingClass));
		}
		bundledServiceDTO.addFlexiSelection(ondSequence, isFlexiIncluded);
	}

	@Deprecated
	private AvailableFlightSearchDTO extractPriceQuoteReqParams(OTAAirPriceRQ otaAirPriceRQ, OTAAirPriceRS otaAirPriceRS)
			throws ModuleException, WebservicesException {

		AvailableFlightSearchDTOBuilder availableFlightSearchDTO = new AvailableFlightSearchDTOBuilder();

		// TODO - provision for quoting price on a set of descrete collection of related segments without depending on
		// the direction indicator
		if (otaAirPriceRQ.getAirItinerary().getDirectionInd() == null) {
			log.error("ERROR detected in extractPriceQuoteReqParams(OTAAirAvailRQ). " + "Invalid directionIndicator. "
					+ "[directionInd is null]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_FIELD_MISSING,
					"[Direction Indictor is required]");
		}

		if (otaAirPriceRQ.getAirItinerary().getDirectionInd() == AirTripType.RETURN) {
			availableFlightSearchDTO.setReturnFlag(true);
		}

		Collection<FlightSegmentDTO> obFlightSegmentDTOs = new LinkedList<FlightSegmentDTO>();
		Collection<FlightSegmentDTO> ibFlightSegmentDTOs = null;
		if (availableFlightSearchDTO.getSearchDTO().isReturnFlag()) {
			ibFlightSegmentDTOs = new LinkedList<FlightSegmentDTO>();
		}

		// origin,destination,departure date, return date
		Collection<String> obAirports = new ArrayList<String>();
		Collection<String> bookingClassSet = new HashSet<String>();
		for (OriginDestinationOptionType originDestinationOption : otaAirPriceRQ.getAirItinerary().getOriginDestinationOptions()
				.getOriginDestinationOption()) {
			boolean returnSegments = false;
			int bookingClassCount = 0;
			for (BookFlightSegmentType bookFlightSegment : originDestinationOption.getFlightSegment()) {
				/** departure date of the first segment of the journey is taken as departure date */
				if (availableFlightSearchDTO.getDepartureDate() == null) {
					availableFlightSearchDTO.setDepartureDate(bookFlightSegment.getDepartureDateTime().toGregorianCalendar()
							.getTime());
				}
				/** first airport is taken as origin */
				if (obAirports.size() == 0) {
					availableFlightSearchDTO.setFromAirport(bookFlightSegment.getDepartureAirport().getLocationCode());
				}

				if (availableFlightSearchDTO.getSearchDTO().isReturnFlag()
						&& !returnSegments
						&& CommonUtil.isReturnSegment(obAirports, bookFlightSegment.getDepartureAirport().getLocationCode(),
								bookFlightSegment.getArrivalAirport().getLocationCode())) {
					returnSegments = true;
				}
				/** last station of the outbound journey is taken as the destination airport */
				if (!returnSegments) {
					availableFlightSearchDTO.setToAirport(bookFlightSegment.getArrivalAirport().getLocationCode());
				}
				/** departure date of the first return segment is taken as the return date */
				if (returnSegments && availableFlightSearchDTO.getReturnDate() == null) {
					availableFlightSearchDTO.setReturnDate(bookFlightSegment.getDepartureDateTime().toGregorianCalendar()
							.getTime());
				}

				if (!returnSegments) {
					obAirports.add(bookFlightSegment.getDepartureAirport().getLocationCode());
					obAirports.add(bookFlightSegment.getArrivalAirport().getLocationCode());
				}

				FlightSegmentDTO aaFlightSegmentDTO = new FlightSegmentDTO();
				aaFlightSegmentDTO.setFromAirport(bookFlightSegment.getDepartureAirport().getLocationCode());
				aaFlightSegmentDTO.setToAirport(bookFlightSegment.getArrivalAirport().getLocationCode());
				aaFlightSegmentDTO.setFlightNumber(bookFlightSegment.getFlightNumber());
				aaFlightSegmentDTO.setDepartureDateTime(bookFlightSegment.getDepartureDateTime().toGregorianCalendar().getTime());
				aaFlightSegmentDTO.setArrivalDateTime(bookFlightSegment.getArrivalDateTime().toGregorianCalendar().getTime());
				if (!returnSegments) {
					obFlightSegmentDTOs.add(aaFlightSegmentDTO);
				} else {
					if (ibFlightSegmentDTOs == null) {
						log.error("ERROR detected in extractPriceQuoteReqParams(OTAAirAvailRQ). "
								+ "Invalid directionIndicator. " + "[directionInd="
								+ otaAirPriceRQ.getAirItinerary().getDirectionInd() + "]");
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"[Direction Indictor is invalid]");
					}
					ibFlightSegmentDTOs.add(aaFlightSegmentDTO);
				}

				/** Adding the Booking classes */
				if (bookFlightSegment.getResBookDesigCode() != null) {
					bookingClassSet.add(bookFlightSegment.getResBookDesigCode());
					bookingClassCount++;
				}
			}

			if (bookingClassCount != 0 && bookingClassCount != originDestinationOption.getFlightSegment().size()) { // we
				// have
				// user
				// specified
				// CC.Shoud
				// match
				// with
				// the
				// no FS
				log.error("ERROR occured in extractPriceQuoteReqParams(OTAAirAvailRQ). "
						+ "Invalid no of booking class. Booking class not included in all ONDs" + "[booking class given="
						+ bookingClassCount + "] [expected = " + originDestinationOption.getFlightSegment().size() + "]");
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
						"Cabin class missing from FlightSegment");
			}
		}

		// retrieve the additionally required information on flight segments
		Collection<FlightSegmentDTO> filledObFlightSegmentDTOs = WebServicesModuleUtils.getFlightBD().getFilledFlightSegmentDTOs(
				obFlightSegmentDTOs);

		if (filledObFlightSegmentDTOs.size() != obFlightSegmentDTOs.size()) {
			log.error("ERROR detected in extractPriceQuoteReqParams(OTAAirAvailRQ). " + "Invalid Outbound Segment(s). ");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Invalid Outbound Segment(s)]");
		}

		// set outbound and inbound flight segment IDs
		Collection<Integer> obFlightSegmentIds = new ArrayList<Integer>();
		for (FlightSegmentDTO flightSegmentDTO : filledObFlightSegmentDTOs) {
			if (flightSegmentDTO.getSegmentId() != null) {
				obFlightSegmentIds.add(flightSegmentDTO.getSegmentId());
			} else {
				log.error("ERROR detected in extractPriceQuoteReqParams(OTAAirAvailRQ). " + "Invalid Outbound Segment. " + "["
						+ flightSegmentDTO + "]");
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Invalid Outbound Segment. "
						+ flightSegmentDTO + "]");
			}
		}

		availableFlightSearchDTO.setOutBoundFlights(obFlightSegmentIds);

		if (availableFlightSearchDTO.getSearchDTO().isReturnFlag()) {
			Collection<FlightSegmentDTO> filledIbFlightSegmentDTOs = WebServicesModuleUtils.getFlightBD()
					.getFilledFlightSegmentDTOs(ibFlightSegmentDTOs);

			if (filledIbFlightSegmentDTOs.size() != ibFlightSegmentDTOs.size()) {
				log.error("ERROR occured in extractPriceQuoteReqParams(OTAAirAvailRQ). " + "Invalid Inbound Segment(s). ");
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Invalid Inbound Segment(s)]");
			}

			Collection<Integer> ibFlightSegmentIds = new ArrayList<Integer>();
			for (FlightSegmentDTO flightSegmentDTO : filledIbFlightSegmentDTOs) {
				if (flightSegmentDTO.getSegmentId() != null) {
					ibFlightSegmentIds.add(flightSegmentDTO.getSegmentId());
				} else {
					log.error("ERROR occured in extractPriceQuoteReqParams(OTAAirAvailRQ). " + "Invalid Inbound Segment. " + "["
							+ flightSegmentDTO + "]");
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Invalid Inbound Segment. "
							+ flightSegmentDTO + "]");
				}
			}
			availableFlightSearchDTO.setInBoundFlights(ibFlightSegmentIds);
		}

		// pax counts
		TravelerInfoSummaryType travelerInfoSummary = otaAirPriceRQ.getTravelerInfoSummary();
		int adults = 0;
		int children = 0;
		int infants = 0;
		for (TravelerInformationType travelerInformationType : travelerInfoSummary.getAirTravelerAvail()) {
			for (PassengerTypeQuantityType passengerTypeQuantityType : travelerInformationType.getPassengerTypeQuantity()) {
				if (passengerTypeQuantityType.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
					adults += passengerTypeQuantityType.getQuantity();
				} else if (passengerTypeQuantityType.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
					children += passengerTypeQuantityType.getQuantity();
				} else if (passengerTypeQuantityType.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
					infants += passengerTypeQuantityType.getQuantity();
				}
			}
		}
		if (adults == 0 && children == 0) {
			log.error("ERROR detected in extractPriceQuoteReqParams(OTAAirAvailRQ). " + "Invalid adult/children pax count. "
					+ "[adults pax=" + adults + "]" + "[children pax=" + children + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AT_LEAST_ONE_ADULT_MUST_BE_INCLUDED_,
					"Adult and Children pax count cannot be zero");
		}
		availableFlightSearchDTO.setAdultCount(adults);
		availableFlightSearchDTO.setChildCount(children);
		availableFlightSearchDTO.setInfantCount(infants);

		String preferredCurrencyCode = null;
		if (travelerInfoSummary.getPriceRequestInformation() != null) {
			preferredCurrencyCode = travelerInfoSummary.getPriceRequestInformation().getCurrencyCode();
		}
		availableFlightSearchDTO.setPreferredCurrencyCode(preferredCurrencyCode);

		// one and only one POS is supported
		if (otaAirPriceRQ.getPOS().getSource().size() != 1) {
			log.error("ERROR detected in extractPriceQuoteReqParams(OTAAirAvailRQ). " + "Invalid POS. " + "[POS count="
					+ otaAirPriceRQ.getPOS().getSource().size() + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"[Only one and only POS is supported, Received " + otaAirPriceRQ.getPOS().getSource().size()
							+ " POS records]");
		}

		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
		availableFlightSearchDTO.setPosAirport(principal.getAgentStation());
		availableFlightSearchDTO.setAgentCode(principal.getAgentCode());

		// FIXME make followings configurable
		availableFlightSearchDTO.setChannelCode(SalesChannelsUtil
				.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));

		if (!bookingClassSet.isEmpty() && bookingClassSet.size() > 1) { // we only support booking class per one journey
			log.error("ERROR occured in extractPriceQuoteReqParams(OTAAirAvailRQ). "
					+ "Invalid no of booking class. Booking class shoud be same for all Flight segment per Price Quote"
					+ "[booking class given=" + bookingClassSet.size() + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_TOO_MANY_CLASSES_,
					"Too many cabin classes specifyed .Only one is supported per availability search");
		}
		String bookingClass = BeanUtils.getFirstElement(bookingClassSet);
		String bookingType = BookingClass.BookingClassType.NORMAL;
		if (bookingClass != null) {
			Collection<String> standByBCList = WebServicesModuleUtils.getBookingClassBD().getStandbyBookingClasses();
			if (standByBCList.contains(bookingClass)) {
				bookingType = BookingClass.BookingClassType.STANDBY;
			} else {
				availableFlightSearchDTO.setBookingClassCode(bookingClass);
			}
		}
		availableFlightSearchDTO.setCabinClassCode("Y");
		availableFlightSearchDTO.setBookingType(bookingType);

		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

		int availabilityRestriction = (AuthorizationUtil.hasPrivileges(privilegeKeys, PriviledgeConstants.MAKE_RES_ALLFLIGHTS) || BookingClass.BookingClassType.STANDBY
				.equals(bookingType))
				? AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION
				: AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED;

		availableFlightSearchDTO.setAvailabilityRestrictionLevel(availabilityRestriction);

		if (log.isDebugEnabled()) {
			log.debug(availableFlightSearchDTO.getSearchDTO().getSummary().toString());
		}

		boolean blnAllowHalfReturnFaresForNewBookings = AppSysParamsUtil.isAllowHalfReturnFaresForNewBookings();
		if (availableFlightSearchDTO.getSearchDTO().isReturnFlag()
				&& !availableFlightSearchDTO.getSearchDTO().isOpenReturnSearch()) {
			availableFlightSearchDTO.getSearchDTO().setHalfReturnFareQuote(blnAllowHalfReturnFaresForNewBookings);
		} else if (otaAirPriceRQ.getModifiedSegmentInfo() != null) {
			String pnr = otaAirPriceRQ.getModifiedSegmentInfo().getBookingReferenceID().get(0).getID();

			if (pnr != null) { // Availability request for Modify OND
				LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
				pnrModesDTO.setPnr(pnr);
				pnrModesDTO.setLoadFares(true);
				pnrModesDTO.setLoadSegView(true);
				Reservation reservation = getReservation(pnrModesDTO, null);

				AirItineraryType.OriginDestinationOptions modifiedOND = otaAirPriceRQ.getModifiedSegmentInfo().getAirItinerary()
						.getOriginDestinationOptions();
				// get the pnrSegIds after checking the validity
				List<Integer> pnrSegIdsForMod = extractPNRSegIds(reservation, modifiedOND);
				ReservationUtil.setExistingSegInfo(reservation, availableFlightSearchDTO.getSearchDTO(), pnrSegIdsForMod);
			}
		}

		// If the Flexi fare is selected need to set the flexi quoting.
		Map<Integer, Boolean> ondQuoteFlexi = availableFlightSearchDTO.getSearchDTO().getOndQuoteFlexi();
		if (otaAirPriceRQ.getFlexiFareSelectionOptions() != null) {
			if (otaAirPriceRQ.getFlexiFareSelectionOptions().getInBoundFlexiSelected() != null
					&& otaAirPriceRQ.getFlexiFareSelectionOptions().getInBoundFlexiSelected() == true) {
				ondQuoteFlexi.put(OndSequence.IN_BOUND, true);
			}

			if (otaAirPriceRQ.getFlexiFareSelectionOptions().getOutBoundFlexiSelected() != null
					&& otaAirPriceRQ.getFlexiFareSelectionOptions().getOutBoundFlexiSelected() == true) {
				ondQuoteFlexi.put(OndSequence.OUT_BOUND, true);
			}
		}

		return availableFlightSearchDTO.getSearchDTO();
	}

	private void preparePriceQuoteResParams(OTAAirPriceRQ otaAirPriceRQ, OTAAirPriceRS otaAirPriceRS,
			SelectedFlightDTO selectedFlightDTO, AvailableFlightSearchDTO availableFlightSearchDTO) throws WebservicesException,
			ModuleException {

		// FIXME set all the necessary params from the request
		otaAirPriceRS.setVersion(WebservicesConstants.OTAConstants.VERSION_AirPrice);
		otaAirPriceRS.setTransactionIdentifier(otaAirPriceRQ.getTransactionIdentifier());
		otaAirPriceRS.setSequenceNmbr(otaAirPriceRQ.getSequenceNmbr());
		otaAirPriceRS.setEchoToken(otaAirPriceRQ.getEchoToken());
		otaAirPriceRS.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);

		PricedItinerariesType pricedItineraries = new PricedItinerariesType();
		otaAirPriceRS.getSuccessAndWarningsAndPricedItineraries().add(pricedItineraries);

		PricedItineraryType pricedItinerary = new PricedItineraryType();
		pricedItineraries.getPricedItinerary().add(pricedItinerary);
		AirItineraryType airItinerary = new AirItineraryType();
		pricedItinerary.setAirItinerary(airItinerary);
		pricedItinerary.setSequenceNumber(new BigInteger("1"));

		AirItineraryType.OriginDestinationOptions originDestinationOptions = new AirItineraryType.OriginDestinationOptions();
		airItinerary.setOriginDestinationOptions(originDestinationOptions);

		// Flexi Fare Selection Criteria building and saving to Transaction.
		FlexiFareSelectionCriteria flexiSelectionCriteria = new FlexiFareSelectionCriteria();
		// If the Flexi fare is selected need to set the flexi quoting.
		if (otaAirPriceRQ.getFlexiFareSelectionOptions() != null) {
			if (otaAirPriceRQ.getFlexiFareSelectionOptions().getInBoundFlexiSelected() != null
					&& otaAirPriceRQ.getFlexiFareSelectionOptions().getInBoundFlexiSelected() == true) {
				flexiSelectionCriteria.setInBoundFlexiFareSelected(true);
				ThreadLocalData.setCurrentTnxParam(Transaction.RES_INBOUND_FLEXI_SELECTED, new Boolean(true));
			}

			if (otaAirPriceRQ.getFlexiFareSelectionOptions().getOutBoundFlexiSelected() != null
					&& otaAirPriceRQ.getFlexiFareSelectionOptions().getOutBoundFlexiSelected() == true) {
				flexiSelectionCriteria.setOutBoundFlexiFareSelected(true);
				ThreadLocalData.setCurrentTnxParam(Transaction.RES_OUTBOUND_FLEXI_SELECTED, new Boolean(true));
			}
		}

		boolean isFixedFareAvailable = false;
		boolean isNonFixedFareAvailable = false;

		if (isSeatSelected(otaAirPriceRQ)) {
			Map<String, Map<Integer, LCCAirSeatDTO>> selectedSeats = SeatServiceUtil.getSelectedSeatsDetails(
					getFlightDetailsWithSeats(otaAirPriceRQ), selectedFlightDTO);
			SeatServiceUtil.validateSeatRequest(otaAirPriceRQ, selectedSeats);
			Map<String, List<ExternalChgDTO>> chargesMapForSeats = SeatServiceUtil.getExternalChargesMapForSeats(selectedSeats);
			for (String travelerRef : chargesMapForSeats.keySet()) {
				addExternalCharge(travelerRef, chargesMapForSeats.get(travelerRef));
			}
		}

		if (isMealsSelected(otaAirPriceRQ)) {
			Map<String, Map<Integer, List<LCCMealDTO>>> selectedMeals = MealDetailsUtil.getSelectedMealDetails(
					getFlightDetailsWithMeals(otaAirPriceRQ), selectedFlightDTO);
			Map<String, List<ExternalChgDTO>> chargesMapForMeals = MealDetailsUtil.getExternalChargesMapForMeals(selectedMeals);
			for (String travelerRef : chargesMapForMeals.keySet()) {
				addExternalCharge(travelerRef, chargesMapForMeals.get(travelerRef));
			}
		}

		if (isSSRSelected(otaAirPriceRQ)) {
			PaxCountAssembler paxCountAssembler = getPaxCountAssembler(availableFlightSearchDTO);

			Map<SSRType, Map<String, Map<Integer, List<HashMap<String, String>>>>> serviceWiseMap = getFlightDetailsWithSSRs(otaAirPriceRQ);

			// Get External charges related to airport services
			Map<String, Map<Integer, List<AirportServiceDTO>>> selectedAirportServices = SSRDetailsUtil
					.getSelectedAirportServiceDetails(serviceWiseMap.get(SSRType.AIRPORT), paxCountAssembler, selectedFlightDTO);
			Map<String, List<ExternalChgDTO>> chargesMapForSsrs = SSRDetailsUtil.getExternalChargesMapForAirportServices(
					selectedAirportServices, paxCountAssembler);
			for (String travelerRef : chargesMapForSsrs.keySet()) {
				addExternalCharge(travelerRef, chargesMapForSsrs.get(travelerRef));
			}

			// Get External charges related to in-flight services
			Map<String, Map<Integer, List<SpecialServiceRequestDTO>>> selectedInflightServices = SSRDetailsUtil
					.getSelectedInflightServiceDetails(serviceWiseMap.get(SSRType.INFLIGHT));
			Map<String, List<ExternalChgDTO>> chargesMapForInflightServices = SSRDetailsUtil
					.getExternalChargesMapForInflightService(selectedInflightServices, paxCountAssembler);
			for (String travelerRef : chargesMapForInflightServices.keySet()) {
				addExternalCharge(travelerRef, chargesMapForInflightServices.get(travelerRef));
			}

		}

		if (isBaggagesSelected(otaAirPriceRQ)) {
			Map<String, Integer> baggageOndGrpIdWiseCount = new HashMap<String, Integer>();
			Map<String, Map<Integer, LCCBaggageDTO>> selectedBaggages = BaggageDetailsUtil.getSelectedBaggageDetails(
					getFlightDetailsWithBaggages(otaAirPriceRQ), selectedFlightDTO, baggageOndGrpIdWiseCount);
			Map<String, List<ExternalChgDTO>> chargesMapForBaggages = BaggageDetailsUtil.getExternalChargesMapForBaggages(
					selectedBaggages, selectedFlightDTO, baggageOndGrpIdWiseCount);
			for (String travelerRef : chargesMapForBaggages.keySet()) {
				addExternalCharge(travelerRef, chargesMapForBaggages.get(travelerRef));
			}
		}

		if (isInsuranceSelected(otaAirPriceRQ)) {
			ReservationInsurance quotedInsurance = WebServicesModuleUtils.getReservationBD().getReservationInsurance(
					getInsuranceReference(otaAirPriceRQ));
			validateInsurancePriceQuoteRequest(selectedFlightDTO, getPaxCountAssembler(availableFlightSearchDTO), quotedInsurance);
			Map<String, List<ExternalChgDTO>> chargesMapForInsurance = InsuranceServiceUtil.getExternalChargesMapForInsurance(
					otaAirPriceRQ, quotedInsurance);
			for (String travelerRef : chargesMapForInsurance.keySet()) {
				addExternalCharge(travelerRef, chargesMapForInsurance.get(travelerRef));
			}
			ThreadLocalData.setCurrentTnxParam(Transaction.RES_INSURANCE_SELECTED,
					getInsuranceReference(otaAirPriceRQ).toString());
		}

		if (selectedFlightDTO != null && selectedFlightDTO.getFareType() != FareTypes.NO_FARE) {

			AvailableIBOBFlightSegment selOut = selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND);
			AnciAvailabilityRS anciAvailabilityRS = WebServicesModuleUtils.getAirproxySearchAndQuoteBD()
					.getTaxApplicabilityForAncillaries(AppSysParamsUtil.getDefaultCarrierCode(), selOut.getOndCode(),
							EXTERNAL_CHARGES.JN_ANCI, getTrackInfo());

			ServiceTaxCalculator serviceTaxCalculator = new ServiceTaxCalculator(anciAvailabilityRS);
			serviceTaxCalculator.setFlightSegmentDTOs(selOut.getFlightSegmentDTOs());
			serviceTaxCalculator.calculate();

			isNonFixedFareAvailable = true;
			// success if seats/fares available
			otaAirPriceRS.getSuccessAndWarningsAndPricedItineraries().add(new SuccessType());

			String pricingKey = generatePricingKey(new String[] {
					availableFlightSearchDTO.getOriginDestinationInfoDTOs().get(0).getOrigin(),
					availableFlightSearchDTO.getOriginDestinationInfoDTOs().get(0).getDestination() });

			PricedItineraryUtil.preparePricedItinerary(pricedItinerary, selectedFlightDTO, availableFlightSearchDTO, false,
					pricingKey, flexiSelectionCriteria);
			// store the total price in the transaction
			storeTotalPrice(pricedItinerary);

			if (log.isDebugEnabled()) {
				log.debug(getriceQuoteResultsSummary(otaAirPriceRS).toString());
			}
		}

		if (!isFixedFareAvailable && !isNonFixedFareAvailable) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_NO_AVAILABILITY, null);
		}
	}

	private StringBuffer getriceQuoteResultsSummary(OTAAirPriceRS otaAirPriceRS) throws WebservicesException, ModuleException {
		StringBuffer priceQuoteSummary = new StringBuffer();
		String nl = "\n\r \t";
		for (Object item : otaAirPriceRS.getSuccessAndWarningsAndPricedItineraries()) {
			if (item instanceof PricedItinerariesType) {
				for (PricedItineraryType pricedItineraryType : ((PricedItinerariesType) item).getPricedItinerary()) {
					for (OriginDestinationOptionType originDestinationOption : pricedItineraryType.getAirItinerary()
							.getOriginDestinationOptions().getOriginDestinationOption()) {
						for (BookFlightSegmentType bookFlightSegment : originDestinationOption.getFlightSegment()) {
							priceQuoteSummary.append(nl + "Segment Info [fltNumber=" + bookFlightSegment.getFlightNumber());
							priceQuoteSummary.append(",Origin=" + bookFlightSegment.getDepartureAirport().getLocationCode());
							priceQuoteSummary.append(",Destination=" + bookFlightSegment.getArrivalAirport().getLocationCode());
							priceQuoteSummary.append(",DepDateTime="
									+ CommonUtil.getFormattedDate(bookFlightSegment.getDepartureDateTime().toGregorianCalendar()
											.getTime()));
							priceQuoteSummary.append(",ArrDepDateTime="
									+ CommonUtil.getFormattedDate(bookFlightSegment.getArrivalDateTime().toGregorianCalendar()
											.getTime()) + "]");
						}
					}

					AirItineraryPricingInfoType airItineraryPricingInfo = pricedItineraryType.getAirItineraryPricingInfo();
					FareType itinPrice = airItineraryPricingInfo.getItinTotalFare();
					BaseFare baseFare = itinPrice.getBaseFare();
					priceQuoteSummary.append(nl + "Fare Info [total base fare=" + baseFare.getAmount() + ",currency="
							+ baseFare.getCurrencyCode() + "]");

					FareType.TotalFare totalFare = itinPrice.getTotalFare();
					priceQuoteSummary.append(nl + "Total Pricing Info [total price=" + totalFare.getAmount() + ",currency="
							+ totalFare.getCurrencyCode() + "]");

					for (PTCFareBreakdownType ptcFareBreakdownType : airItineraryPricingInfo.getPTCFareBreakdowns()
							.getPTCFareBreakdown()) {
						priceQuoteSummary.append(nl + "Pax info [type="
								+ ptcFareBreakdownType.getPassengerTypeQuantity().getCode() + ",quantiry="
								+ ptcFareBreakdownType.getPassengerTypeQuantity().getQuantity() + "]");
						for (AirTaxType airTaxType : ptcFareBreakdownType.getPassengerFare().getTaxes().getTax()) {
							priceQuoteSummary.append(nl + "TAX Info [amount=" + airTaxType.getAmount() + ",currency="
									+ airTaxType.getCurrencyCode() + ",code=" + airTaxType.getTaxCode() + ",description="
									+ airTaxType.getTaxName() + "]");
						}
						for (AirFeeType airFeeType : ptcFareBreakdownType.getPassengerFare().getFees().getFee()) {
							priceQuoteSummary.append(nl + "SURCHARGE Info [amount=" + airFeeType.getAmount() + ",currency="
									+ airFeeType.getCurrencyCode() + ",code=" + airFeeType.getFeeCode() + "]");
						}
					}
				}
			}
		}

		return priceQuoteSummary;
	}

	/**
	 * Stores the total price in the transaction to be compare with the booking price quart.
	 * 
	 * @param pricedItinerary
	 */
	public static void storeTotalPrice(PricedItineraryType pricedItinerary) {
		BigDecimal totalPrice = (BigDecimal) ThreadLocalData.getCurrentTnxParam(Transaction.RES_SERVICE_TOTAL_PRICE);

		if (totalPrice != null) {
			totalPrice = AccelAeroCalculator.add(totalPrice, pricedItinerary.getAirItineraryPricingInfo().getItinTotalFare()
					.getTotalFare().getAmount());
		} else {
			totalPrice = pricedItinerary.getAirItineraryPricingInfo().getItinTotalFare().getTotalFare().getAmount();
		}

		ThreadLocalData.setCurrentTnxParam(Transaction.RES_SERVICE_TOTAL_PRICE, totalPrice);
	}

	/**
	 * TODO - remove this method.no longer in use.
	 */
	public static void clearQuotedSegsFaresSeatsColMap() {
		Map<String, Collection<OndFareDTO>> quotedSegsFaresSeatsColMap = (Map) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_SEGMENTS_FARES_SEATS_MAP);

		if (quotedSegsFaresSeatsColMap != null) {
			quotedSegsFaresSeatsColMap.clear();
			ThreadLocalData.setCurrentTnxParam(Transaction.QUOTED_SEGMENTS_FARES_SEATS_MAP, null);
		}
	}

	/**
	 * Clears the {@link FareSegChargeTO} from the user Transaction.
	 */
	public static void clearFareSegmentChargeTO() {
		ThreadLocalData.setCurrentTnxParam(Transaction.FARE_SEGMENT_CHARGE_TO, null);
	}

	public static void clearTotalPrice() {
		ThreadLocalData.setCurrentTnxParam(Transaction.RES_SERVICE_TOTAL_PRICE, null);
	}

	/**
	 * Sets the inbound flexi selection to false;
	 */
	public static void clearInBoundFlexiSelection() {
		ThreadLocalData.setCurrentTnxParam(Transaction.RES_INBOUND_FLEXI_SELECTED, new Boolean(false));
	}

	/**
	 * Sets the outbound flexi selection to false;
	 */
	public static void clearOutBoundFlexiSelection() {
		ThreadLocalData.setCurrentTnxParam(Transaction.RES_OUTBOUND_FLEXI_SELECTED, new Boolean(false));
	}

	public static void clearSelectedInsurance() {
		ThreadLocalData.setCurrentTnxParam(Transaction.RES_INSURANCE_SELECTED, null);
	}

	/**
	 * Clears the price quoting related tnx params.
	 */
	public static void clearPriceQuoteRelatedTransactionParams() {
		PriceQuoteUtil.clearFareSegmentChargeTO();
		PriceQuoteUtil.clearInBoundFlexiSelection();
		PriceQuoteUtil.clearOutBoundFlexiSelection();
		PriceQuoteUtil.clearQuotedExternalCharges();
		PriceQuoteUtil.clearSelectedInsurance();
	}

	public static String generatePricingKey(String[] data) {
		StringBuffer buf = new StringBuffer();

		if (data != null && data.length > 0) {
			for (String datum : data) {
				buf.append(datum);
			}
		}

		int r;
		for (int i = 0; i < 5; i++) {
			r = RandomUtils.nextInt(26) + 65;
			buf.append((char) r);
		}

		return buf.toString();
	}

	/**
	 * Extracts pnrSegIds for cancellation/modification from the request.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @return
	 * @throws WebservicesException
	 */
	private static List<Integer> extractPNRSegIds(Reservation reservation, AirItineraryType.OriginDestinationOptions onds)
			throws WebservicesException {

		if (onds.getOriginDestinationOption() == null || onds.getOriginDestinationOption().size() != 1) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"Only one OND at a time allowed to cancel/modify");
		}

		OriginDestinationOptionType ond = onds.getOriginDestinationOption().get(0);
		int ondGroupCode = -1;
		List<Integer> pnrSegIdsForCnx = new ArrayList<Integer>();
		for (BookFlightSegmentType wsFlightSeg : ond.getFlightSegment()) {
			for (Iterator aaFlightSeg = reservation.getSegments().iterator(); aaFlightSeg.hasNext();) {
				ReservationSegment resSegment = (ReservationSegment) aaFlightSeg.next();
				if (resSegment.getPnrSegId() == Integer.parseInt(wsFlightSeg.getRPH())) {
					pnrSegIdsForCnx.add(Integer.parseInt(wsFlightSeg.getRPH()));
					if (ondGroupCode == -1) {
						ondGroupCode = resSegment.getOndGroupId();
					} else if (ondGroupCode != resSegment.getOndGroupId()) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Only one OND at a time allowed to cancel");
					}
					break;
				}
			}
		}
		return pnrSegIdsForCnx;
	}

	/**
	 * Method to get the reservation.
	 * 
	 * @param pnrModesDTO
	 * @param trackingInfoDTO
	 * @return
	 * @throws Exception
	 */
	private Reservation getReservation(LCCClientPnrModesDTO pnrModesDTO, TrackInfoDTO trackingInfoDTO) throws ModuleException,
			WebservicesException {
		Reservation reservation = null;
		try {
			reservation = WPModuleUtils.getReservationBD().getReservation(pnrModesDTO, trackingInfoDTO);
			if (reservation == null) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_BOOKING_REFERENCE_NOT_FOUND_,
						"No matching booking found");
			}
		} catch (ModuleException ex) {
			throw ex;
		}
		return reservation;
	}

	private Map<String, Map<Integer, String>> getFlightDetailsWithSeats(OTAAirPriceRQ otaAirPriceRQ) throws WebservicesException {
		// key|value - traveler RPH | Map<Integer, String>
		// Map<Integer, String> - key|value - flight segment ID | seat number
		Map<String, Map<Integer, String>> flightDetailsAndSeatsMap = new HashMap<String, Map<Integer, String>>();

		Map<Integer, List<String>> flightWiseSeatMap = new HashMap<Integer, List<String>>();

		for (SpecialReqDetailsType specialReqDetailsType : otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
			SeatRequests seatRequests = specialReqDetailsType.getSeatRequests();
			for (SeatRequest seatRequest : seatRequests.getSeatRequest()) {
				String travelerRef = seatRequest.getTravelerRefNumberRPHList().get(0);
				// Infant RPH must not be with seat request
				if (travelerRef.startsWith("I")) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PASSENGER_TYPE_NOT_SUPPORTED_,
							" Seat is not available for infants");
				}
				String seatNo = seatRequest.getSeatNumber();

				Integer flightSegmentID = Integer.parseInt(seatRequest.getFlightRefNumberRPHList().get(0).split("-")[0].trim());

				if (flightDetailsAndSeatsMap.get(travelerRef) == null) {
					flightDetailsAndSeatsMap.put(travelerRef, new HashMap<Integer, String>());
				} else {
					if (flightDetailsAndSeatsMap.get(travelerRef).get(flightSegmentID) != null) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								" One seat can only be selected for one passenger ");
					}
				}

				flightDetailsAndSeatsMap.get(travelerRef).put(flightSegmentID, seatNo);

				if (flightWiseSeatMap.containsKey(flightSegmentID)) {
					if (flightWiseSeatMap.get(flightSegmentID).contains(seatNo)) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								" Same seat cannot be booked for multiple passengers ");
					} else {
						flightWiseSeatMap.get(flightSegmentID).add(seatNo);
					}
				} else {
					List<String> seatList = new ArrayList<String>();
					seatList.add(seatNo);
					flightWiseSeatMap.put(flightSegmentID, seatList);
				}

			}
		}

		return flightDetailsAndSeatsMap;
	}

	private boolean isSeatSelected(OTAAirPriceRQ otaAirPriceRQ) {
		boolean isSeatSelected = false;
		if (otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails() != null) {
			for (SpecialReqDetailsType specialReqDetailsType : otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
				if (specialReqDetailsType != null && specialReqDetailsType.getSeatRequests() != null
						&& specialReqDetailsType.getSeatRequests().getSeatRequest() != null
						&& specialReqDetailsType.getSeatRequests().getSeatRequest().size() > 0) {
					isSeatSelected = true;
				}
			}
		}

		return isSeatSelected;
	}

	private Map<String, Map<Integer, List<String>>> getFlightDetailsWithMeals(OTAAirPriceRQ otaAirPriceRQ)
			throws WebservicesException {
		// This structure is using since multi meal support can also there, in that case one traveler can have
		// many meals with different quantity
		// TraverlerRef -> FlightSegment -> Meal(s)
		Map<String, Map<Integer, List<String>>> flightDetailsWithMeals = new HashMap<String, Map<Integer, List<String>>>();
		for (SpecialReqDetailsType specialReqDetailsType : otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
			MealRequests mealRequests = specialReqDetailsType.getMealRequests();
			for (MealRequest mealRequest : mealRequests.getMealRequest()) {
				String travelerRef = mealRequest.getTravelerRefNumberRPHList().get(0);
				// Availability response is having flight segment id as RPH and we expect client will send that id with
				// mealrequest as well.
				Integer flightSegId = Integer.parseInt(mealRequest.getFlightRefNumberRPHList().get(0).split("-")[0].trim());
				if (travelerRef.startsWith("I")) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PASSENGER_TYPE_NOT_SUPPORTED_,
							" Meals are not available for infants");
				}

				Integer mealQuantity = 1;
				if (mealRequest.getMealQuantity() > 1) {
					mealQuantity = mealRequest.getMealQuantity();
				}
				for (int index = 0; index < mealQuantity; index++) {
					if (flightDetailsWithMeals.get(travelerRef) == null) {
						flightDetailsWithMeals.put(travelerRef, new HashMap<Integer, List<String>>());
						if (flightDetailsWithMeals.get(travelerRef).get(flightSegId) == null) {
							flightDetailsWithMeals.get(travelerRef).put(flightSegId, new ArrayList<String>());
						}
					} else {
						if (flightDetailsWithMeals.get(travelerRef).get(flightSegId) == null) {
							flightDetailsWithMeals.get(travelerRef).put(flightSegId, new ArrayList<String>());
						}
					}

					flightDetailsWithMeals.get(travelerRef).get(flightSegId).add(mealRequest.getMealCode());
				}
			}
		}

		return flightDetailsWithMeals;
	}

	private Map<String, Map<Integer, String>> getFlightDetailsWithBaggages(OTAAirPriceRQ otaAirPriceRQ)
			throws WebservicesException {
		// TraverlerRef -> FlightSegment -> Baggage
		Map<String, Map<Integer, String>> flightDetailsWithBaggages = new HashMap<String, Map<Integer, String>>();
		for (SpecialReqDetailsType specialReqDetailsType : otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
			BaggageRequests baggageRequests = specialReqDetailsType.getBaggageRequests();
			for (BaggageRequest baggageRequest : baggageRequests.getBaggageRequest()) {
				String travelerRef = baggageRequest.getTravelerRefNumberRPHList().get(0);
				// Availability response is having flight segment id as RPH and
				// we expect client will send that id with
				// baggagerequest as well.
				Integer flightSegId = Integer.parseInt(baggageRequest.getFlightRefNumberRPHList().get(0).split("-")[0].trim());
				if (travelerRef.startsWith("I")) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PASSENGER_TYPE_NOT_SUPPORTED_,
							" Baggages are not available for infants");
				}
				if (flightDetailsWithBaggages.get(travelerRef) == null) {
					flightDetailsWithBaggages.put(travelerRef, new HashMap<Integer, String>());
				}
				flightDetailsWithBaggages.get(travelerRef).put(flightSegId, baggageRequest.getBaggageCode());
			}
		}
		return flightDetailsWithBaggages;
	}

	private Integer getInsuranceReference(OTAAirPriceRQ airPriceRQ) throws WebservicesException {
		Integer ref = null;
		for (SpecialReqDetailsType specialReqDetailsType : airPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
			if (specialReqDetailsType.getInsuranceRequests().getInsuranceRequest().getRPH() == null) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_OR_INSUFFICIENT_DETAILS,
						"Insurance RPH must not be empty");
			}
			ref = Integer.parseInt(specialReqDetailsType.getInsuranceRequests().getInsuranceRequest().getRPH());
		}
		return ref;
	}

	private boolean isMealsSelected(OTAAirPriceRQ otaAirPriceRQ) {
		boolean isMealsSelected = false;
		if (otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails() != null) {
			for (SpecialReqDetailsType specialReqDetailsType : otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
				if (specialReqDetailsType != null && specialReqDetailsType.getMealRequests() != null
						&& specialReqDetailsType.getMealRequests().getMealRequest() != null
						&& specialReqDetailsType.getMealRequests().getMealRequest().size() > 0) {
					isMealsSelected = true;
				}
			}
		}

		return isMealsSelected;
	}

	private boolean isBaggagesSelected(OTAAirPriceRQ otaAirPriceRQ) {
		boolean isBaggagesSelected = false;
		if (otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails() != null) {
			for (SpecialReqDetailsType specialReqDetailsType : otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
				if (specialReqDetailsType != null && specialReqDetailsType.getBaggageRequests() != null
						&& specialReqDetailsType.getBaggageRequests().getBaggageRequest() != null
						&& specialReqDetailsType.getBaggageRequests().getBaggageRequest().size() > 0) {
					isBaggagesSelected = true;
				}
			}
		}

		return isBaggagesSelected;
	}

	/**
	 * This method will return traveler selected SSR(s) [Both In-flight Services & Airport Services]. Outer Map contains
	 * {@link SSRType.AIRPORT} & {@link SSRType.INFLIGHT} as keys.
	 * 
	 * @throws ModuleException
	 * 
	 */
	private Map<SSRType, Map<String, Map<Integer, List<HashMap<String, String>>>>> getFlightDetailsWithSSRs(
			OTAAirPriceRQ otaAirPriceRQ) throws WebservicesException {

		Map<SSRType, Map<String, Map<Integer, List<HashMap<String, String>>>>> serviceWiseflightDetailsWithSSRs = new HashMap<SSRType, Map<String, Map<Integer, List<HashMap<String, String>>>>>();
		serviceWiseflightDetailsWithSSRs.put(SSRType.AIRPORT, new HashMap<String, Map<Integer, List<HashMap<String, String>>>>());
		serviceWiseflightDetailsWithSSRs
				.put(SSRType.INFLIGHT, new HashMap<String, Map<Integer, List<HashMap<String, String>>>>());

		for (SpecialReqDetailsType specialReqDetailsType : otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
			SSRRequests ssrRequests = specialReqDetailsType.getSSRRequests();
			List<Integer> flightSegIds = new ArrayList<Integer>();

			for (SSRRequest ssrRequest : ssrRequests.getSSRRequest()) {

				// If user didn't provide service type, consider it as in-flight service
				if (ssrRequest.getServiceType() == null) {
					ssrRequest.setServiceType(SSRType.INFLIGHT);
				}

				if (ssrRequest.getServiceType() == SSRType.INFLIGHT) {
					Map<String, Map<Integer, List<HashMap<String, String>>>> flightDetailsWithSSRs = serviceWiseflightDetailsWithSSRs
							.get(SSRType.INFLIGHT);

					String travelerRef = ssrRequest.getTravelerRefNumberRPHList().get(0);

					Integer flightSegId = Integer.parseInt(ssrRequest.getFlightRefNumberRPHList().get(0).split("-")[0].trim());
					// WebServicesModuleUtils.getFlightBD().getF
					if (travelerRef.startsWith("I")) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PASSENGER_TYPE_NOT_SUPPORTED_,
								" In-flight Services are not available for Infants");
					}

					if (flightDetailsWithSSRs.get(travelerRef) == null) {
						flightDetailsWithSSRs.put(travelerRef, new HashMap<Integer, List<HashMap<String, String>>>());
						if (flightDetailsWithSSRs.get(travelerRef).get(flightSegId) == null) {
							flightDetailsWithSSRs.get(travelerRef).put(flightSegId, new ArrayList<HashMap<String, String>>());
						}
					} else {
						if (flightDetailsWithSSRs.get(travelerRef).get(flightSegId) == null) {
							flightDetailsWithSSRs.get(travelerRef).put(flightSegId, new ArrayList<HashMap<String, String>>());
						} else {
							ArrayList<HashMap<String, String>> sMap = (ArrayList<HashMap<String, String>>) flightDetailsWithSSRs
									.get(travelerRef).get(flightSegId);
							Iterator<HashMap<String, String>> itr = sMap.iterator();
							while (itr.hasNext()) {
								HashMap<String, String> tempMap = itr.next();

								if (ssrRequest.getSsrCode() == null) {
									throw new WebservicesException(
											IOTACodeTables.AccelaeroErrorCodes_AAE_SSR_ALREADY_ADDED_FOR_THE_PASSENGER_,
											" Same In-flight Services request for the Passenger " + travelerRef);
								}

								if (tempMap.get("SSR_CODE").equals(ssrRequest.getSsrCode())) {
									throw new WebservicesException(
											IOTACodeTables.AccelaeroErrorCodes_AAE_SSR_ALREADY_ADDED_FOR_THE_PASSENGER_,
											" Same In-flight Services request for the Passenger " + travelerRef);
								}

							}
						}
					}

					HashMap<String, String> ssrMap = new HashMap<String, String>();
					ssrMap.put("SSR_CODE", ssrRequest.getSsrCode());

					flightDetailsWithSSRs.get(travelerRef).get(flightSegId).add(ssrMap);

					flightSegIds.add(flightSegId);

				} else if (ssrRequest.getServiceType() == SSRType.AIRPORT) {

					Map<String, Map<Integer, List<HashMap<String, String>>>> flightDetailsWithSSRs = serviceWiseflightDetailsWithSSRs
							.get(SSRType.AIRPORT);

					if (ssrRequest.getAirportCode() == null || ssrRequest.getAirportCode().trim().equals("")) {
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_AIRPORT_CODE_IS_REQUIRED_,
								" Airport Code is required");
					}

					if (ssrRequest.getAirportType() == null || ssrRequest.getAirportType().trim().equals("")) {
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_AIRPORT_TYPE_IS_REQUIRED_,
								" Airport Type is required");
					}

					String travelerRef = ssrRequest.getTravelerRefNumberRPHList().get(0);

					Integer flightSegId = Integer.parseInt(ssrRequest.getFlightRefNumberRPHList().get(0).split("-")[0].trim());
					if (travelerRef.startsWith("I")) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PASSENGER_TYPE_NOT_SUPPORTED_,
								" Airport Services are not available for Infants");
					}

					if (flightDetailsWithSSRs.get(travelerRef) == null) {
						flightDetailsWithSSRs.put(travelerRef, new HashMap<Integer, List<HashMap<String, String>>>());
						if (flightDetailsWithSSRs.get(travelerRef).get(flightSegId) == null) {
							flightDetailsWithSSRs.get(travelerRef).put(flightSegId, new ArrayList<HashMap<String, String>>());
						}
					} else {
						if (flightDetailsWithSSRs.get(travelerRef).get(flightSegId) == null) {
							flightDetailsWithSSRs.get(travelerRef).put(flightSegId, new ArrayList<HashMap<String, String>>());
						} else {
							ArrayList<HashMap<String, String>> sMap = (ArrayList<HashMap<String, String>>) flightDetailsWithSSRs
									.get(travelerRef).get(flightSegId);
							Iterator<HashMap<String, String>> itr = sMap.iterator();
							while (itr.hasNext()) {
								HashMap<String, String> tempMap = itr.next();

								if (ssrRequest.getSsrCode() == null || ssrRequest.getAirportCode() == null) {
									throw new WebservicesException(
											IOTACodeTables.AccelaeroErrorCodes_AAE_SSR_ALREADY_ADDED_FOR_THE_PASSENGER_,
											" Same Airport Services request for the Passenger " + travelerRef);
								}

								if (tempMap.get("SSR_CODE").equals(ssrRequest.getSsrCode())
										&& tempMap.get("AIRPORT_CODE").equals(ssrRequest.getAirportCode())) {
									throw new WebservicesException(
											IOTACodeTables.AccelaeroErrorCodes_AAE_SSR_ALREADY_ADDED_FOR_THE_PASSENGER_,
											" Same Airport Services request for the Passenger " + travelerRef);
								}

							}
						}
					}

					HashMap<String, String> ssrMap = new HashMap<String, String>();
					ssrMap.put("SSR_CODE", ssrRequest.getSsrCode());
					ssrMap.put("AIRPORT_CODE", ssrRequest.getAirportCode());
					ssrMap.put("AIRPORT_TYPE", ssrRequest.getAirportType());

					flightDetailsWithSSRs.get(travelerRef).get(flightSegId).add(ssrMap);
				}
			}

			if (flightSegIds.size() > 0) {
				setInflightServiceSegmentCode(serviceWiseflightDetailsWithSSRs.get(SSRType.INFLIGHT), flightSegIds);
			}
		}

		return serviceWiseflightDetailsWithSSRs;
	}

	private void setInflightServiceSegmentCode(Map<String, Map<Integer, List<HashMap<String, String>>>> flightDetailsWithSSRs,
			List<Integer> flightSegIds) throws WebservicesException {
		try {
			Collection<FlightSegmentDTO> flightSegments = WebServicesModuleUtils.getFlightBD().getFlightSegments(flightSegIds);
			Map<Integer, String> fltSegMap = new HashMap<Integer, String>();
			generateFlightSegMap(fltSegMap, flightSegments);

			for (Entry<String, Map<Integer, List<HashMap<String, String>>>> travelEntry : flightDetailsWithSSRs.entrySet()) {
				Map<Integer, List<HashMap<String, String>>> fltSegIdMap = travelEntry.getValue();
				for (Entry<Integer, List<HashMap<String, String>>> fltEntry : fltSegIdMap.entrySet()) {
					Integer fltSegId = fltEntry.getKey();
					List<HashMap<String, String>> serviceList = fltEntry.getValue();
					for (HashMap<String, String> map : serviceList) {
						map.put("SEGMENT_CODE", fltSegMap.get(fltSegId));
					}
				}
			}
		} catch (Exception e) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_SSR_NOT_AVAILABLE_IN_REQUESTED_AIRPORT_,
					"Requested In-flight Services not available for this flight segment");
		}
	}

	private void generateFlightSegMap(Map<Integer, String> fltSegMap, Collection<FlightSegmentDTO> flightSegments) {
		for (FlightSegmentDTO flightSegmentDTO : flightSegments) {
			fltSegMap.put(flightSegmentDTO.getSegmentId(), flightSegmentDTO.getSegmentCode());
		}
	}

	private boolean isSSRSelected(OTAAirPriceRQ otaAirPriceRQ) {
		boolean isSSRSelected = false;
		if (otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails() != null) {
			for (SpecialReqDetailsType specialReqDetailsType : otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
				if (specialReqDetailsType != null && specialReqDetailsType.getSSRRequests() != null
						&& specialReqDetailsType.getSSRRequests().getSSRRequest() != null
						&& specialReqDetailsType.getSSRRequests().getSSRRequest().size() > 0) {
					isSSRSelected = true;
				}
			}
		}

		return isSSRSelected;
	}

	private boolean isInsuranceSelected(OTAAirPriceRQ otaAirPriceRQ) {
		boolean isInsuranceSelected = false;
		if (otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails() != null) {
			for (SpecialReqDetailsType specialReqDetailsType : otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
				if (specialReqDetailsType != null && specialReqDetailsType.getInsuranceRequests() != null
						&& specialReqDetailsType.getInsuranceRequests().getInsuranceRequest() != null) {
					isInsuranceSelected = true;
				}
			}
		}

		return isInsuranceSelected;
	}

	public static void addExternalCharge(String travelerRef, List<ExternalChgDTO> externalCharges) {
		Map<String, List<ExternalChgDTO>> quotedExternalCharges = (Map<String, List<ExternalChgDTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);
		if (quotedExternalCharges == null) {
			quotedExternalCharges = new HashMap<String, List<ExternalChgDTO>>();
			quotedExternalCharges.put(travelerRef, externalCharges);
		} else {
			if (quotedExternalCharges.get(travelerRef) == null) {
				quotedExternalCharges.put(travelerRef, new ArrayList<ExternalChgDTO>());
			}
			quotedExternalCharges.get(travelerRef).addAll(externalCharges);
		}

		ThreadLocalData.setCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES, quotedExternalCharges);
	}

	public static void clearQuotedExternalCharges() {
		Map<String, List<ExternalChargeTO>> quotedExternalCharges = (Map<String, List<ExternalChargeTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);

		if (quotedExternalCharges != null) {
			quotedExternalCharges.clear();
			ThreadLocalData.setCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES, null);
		}

	}

	/**
	 * Creates the FareSegChargeTO from the OndFareDTOList and saves in the current transaction.
	 * 
	 * @param selectedFlight
	 * @param availableFlightSearchDTO
	 * @throws WebservicesException
	 */
	public static void addFareSegmentChargeTOToTransaction(SelectedFlightDTO selectedFlight,
			AvailableFlightSearchDTO availableFlightSearchDTO) throws WebservicesException {

		if (selectedFlight == null || availableFlightSearchDTO == null) {
			throw new IllegalArgumentException("selectedFlight and availableFlightSearchDTO cannot be null");
		}

		int fareType = selectedFlight.getFareType();
		Collection<OndFareDTO> ondFareDTOList = selectedFlight.getOndFareDTOs(false);

		if (ondFareDTOList == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_NO_AVAILABILITY, "No Availabilities");
		}

		FareSegChargeTO fareSegChargeTO = FareConvertUtil.getFareSegChargeTO(fareType, ondFareDTOList,
				getPaxCountAssembler(availableFlightSearchDTO), null);

		fareSegChargeTO.setOndBundledFareDTOs(selectedFlight.getSelectedOndBundledFares());

		ThreadLocalData.setCurrentTnxParam(Transaction.FARE_SEGMENT_CHARGE_TO, fareSegChargeTO);
	}

	/**
	 * @param availableFlightSearchDTO
	 *            : The available flight search holding the original pax counts requested.
	 * 
	 * @return : The pax count assembler.
	 */
	public static PaxCountAssembler getPaxCountAssembler(AvailableFlightSearchDTO availableFlightSearchDTO) {

		if (availableFlightSearchDTO == null) {
			throw new IllegalArgumentException("availableFlightSearchDTO cannot be null");
		}

		List<PassengerTypeQuantityTO> paxQuantities = new ArrayList<PassengerTypeQuantityTO>();

		PassengerTypeQuantityTO adultQuantity = new PassengerTypeQuantityTO();
		adultQuantity.setPassengerType(PaxTypeTO.ADULT);
		adultQuantity.setQuantity(availableFlightSearchDTO.getAdultCount());
		paxQuantities.add(adultQuantity);

		PassengerTypeQuantityTO childQuantity = new PassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);
		childQuantity.setQuantity(availableFlightSearchDTO.getChildCount());
		paxQuantities.add(childQuantity);

		PassengerTypeQuantityTO infantQuantity = new PassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);
		infantQuantity.setQuantity(availableFlightSearchDTO.getInfantCount());
		paxQuantities.add(infantQuantity);

		PaxCountAssembler paxCountAssembler = new PaxCountAssembler(paxQuantities);

		return paxCountAssembler;
	}

	private static void validateInsurancePriceQuoteRequest(SelectedFlightDTO selectedFlightDTO,
			PaxCountAssembler paxCountAssembler, ReservationInsurance insurance) throws WebservicesException {

		if (!insurance.getState().equals(ReservationInternalConstants.INSURANCE_STATES.QO.toString())) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"Invalid insurance quotation.Already sold or not a quoted insurance");
		}

		int totalNoOfAdultsAndChildren = paxCountAssembler.getAdultCount() + paxCountAssembler.getChildCount();
		boolean isReturnTrip = false;

		List<FlightSegmentDTO> outBoundFlightSegments = new ArrayList<FlightSegmentDTO>();
		outBoundFlightSegments.addAll(selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND).getFlightSegmentDTOs());
		Collections.sort(outBoundFlightSegments);

		Date journeyStartDate = outBoundFlightSegments.get(0).getDepartureDateTime();
		Date journeyEndDate = null;
		String journeyStartAirpport = outBoundFlightSegments.get(0).getFromAirport();
		String journeyEndAirport = outBoundFlightSegments.get(outBoundFlightSegments.size() - 1).getToAirport();
		if (selectedFlightDTO.getSelectedOndFlight(OndSequence.IN_BOUND) != null
				&& selectedFlightDTO.getSelectedOndFlight(OndSequence.IN_BOUND).getFlightSegmentDTOs() != null) {
			List<FlightSegmentDTO> inboundFlightSegments = new ArrayList<FlightSegmentDTO>();
			inboundFlightSegments.addAll(selectedFlightDTO.getSelectedOndFlight(OndSequence.IN_BOUND).getFlightSegmentDTOs());
			Collections.sort(inboundFlightSegments);
			journeyEndDate = inboundFlightSegments.get(inboundFlightSegments.size() - 1).getArrivalDateTime();
			isReturnTrip = true;
			if (!journeyStartAirpport.equals(inboundFlightSegments.get(inboundFlightSegments.size() - 1).getToAirport())) {
				journeyEndAirport = inboundFlightSegments.get(inboundFlightSegments.size() - 1).getToAirport();
			}
		} else {
			journeyEndDate = outBoundFlightSegments.get(outBoundFlightSegments.size() - 1).getArrivalDateTime();
		}

		if (!(journeyStartDate.equals(insurance.getDateOfTravel()) && journeyEndDate.equals(insurance.getDateOfReturn())
				&& journeyStartAirpport.equals(insurance.getOrigin()) && journeyEndAirport.equals(insurance.getDestination())
				&& isReturnTrip == (insurance.getRoundTrip().equals("Y") ? true : false) && totalNoOfAdultsAndChildren == insurance
					.getTotalPaxCount())) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_OR_INSUFFICIENT_DETAILS,
					"Invalid insurance quotation.Not matching with the journey requirements");
		}

	}

	private static TrackInfoDTO getTrackInfo() {
		String sessionId = (String) ThreadLocalData.getWSContextParam(WebservicesContext.WEB_SESSION_ID);
		String ipAddress = BeanUtils.nullHandler(ThreadLocalData.getWSContextParam(
				WebservicesContext.WEB_REQUEST_IP));

		TrackInfoDTO trackInfo = new TrackInfoDTO();
		trackInfo.setAppIndicator(AppIndicatorEnum.APP_WS);
		trackInfo.setSessionId(sessionId);
		trackInfo.setIpAddress(ipAddress);
		if (ipAddress.length() > 0) {
			long ipNumber = AmadeusCommonUtil.getReservationOriginIPAddress(ipAddress);
			trackInfo.setOriginIPNumber(ipNumber);
		}
		trackInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());

		return trackInfo;
	}
}