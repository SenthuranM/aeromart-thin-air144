package com.isa.thinair.webservices.core.cache.delegator;

import java.io.Serializable;
import java.util.Map;

public class CacheResponse<T> implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private T otaCacheResponse;
	
	private Map<String, Object> cacheTransactionData;
	
	public CacheResponse(T otaCacheResponse, Map<String, Object> cacheTransactionData) {
		this.otaCacheResponse = otaCacheResponse;
		this.cacheTransactionData = cacheTransactionData;
	}

	public T getOtaCacheResponse() {
		return otaCacheResponse;
	}

	public void setOtaCacheResponse(T otaCacheResponse) {
		this.otaCacheResponse = otaCacheResponse;
	}

	public Map<String, Object> getCacheTransactionData() {
		return cacheTransactionData;
	}

	public void setCacheTransactionData(Map<String, Object> cacheTransactionData) {
		this.cacheTransactionData = cacheTransactionData;
	}	

}
