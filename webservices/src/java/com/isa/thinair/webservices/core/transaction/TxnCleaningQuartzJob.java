package com.isa.thinair.webservices.core.transaction;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

public class TxnCleaningQuartzJob implements Job {

	private static Log log = LogFactory.getLog(TxnCleaningQuartzJob.class);

	public TxnCleaningQuartzJob() {
	}

	public void execute(JobExecutionContext context) throws JobExecutionException {

		try {
			String jobName = context.getJobDetail().getKey().getName();

			log.info("##AAWS## TxnCleaningQuartzJob: " + jobName + " execution start at " + new Date());

			WebServicesModuleUtils.getTrasnsactionStore().removeExpiredTransactions();

			log.info("##AAWS## TxnCleaningQuartzJob: " + jobName + " execution end at " + new Date());
		} catch (Exception e) {
			log.error("##AAWS## TxnCleaningQuartzJob Failed", e);
		}
	}
}