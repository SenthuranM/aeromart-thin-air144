package com.isa.thinair.webservices.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.ejb.TransactionAttributeType;

/**
 * 
 * @author Mehdi Raza
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface WebServiceTransaction {

	public enum TransactionBoundry {
		START, STARTORMIDDLE, MIDDLE, FINISH
	};

	TransactionAttributeType value();

	TransactionBoundry boundry() default TransactionBoundry.MIDDLE;

	boolean forceCommit() default false;
}
