/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005-08 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * ===============================================================================
 */
package com.isa.thinair.webservices.core.transaction;

import java.io.Serializable;
import java.util.Map;

/**
 * Abstracts a webservices transaction. Holds transactional data.
 * 
 * @author Mehdi Raza
 * @author Nasly
 */
public interface ITransaction extends Serializable {

	/** Transaction statuses */
	public static final int STATUS_ACTIVE = 1;

	public static final int STATUS_EXPIRED = -1;

	public String getId();

	public void setParameter(String key, Object value);

	public Object getParameter(String key);

	public Object removeParameter(String key);
	
	public Map<String, Object> getAllParameters();
	
	public void setAllParameters(Map<String, Object> parameters);

	public boolean containsParameter(String key);

	public int getParamsCount();

	public boolean isNew();

	public void commit();

	public boolean isCommitted();

	public long getLastAccessedTimestamp();

	public int getStatus();

	public boolean qualifyForExpiration(long tnxTimeoutDuration);

	public void forceExpiration();

	public long getCreationTime();

	public String getSessionId();

	public void setSessionId(String sessionId);
}
