package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentTransaction;
import com.isa.thinair.airtravelagents.api.util.CollectionTypeEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isaaviation.thinair.webservices.api.receiptupdate.AAReceiptUpdateRQ;
import com.isaaviation.thinair.webservices.api.receiptupdate.AAReceiptUpdateRS;
import com.isaaviation.thinair.webservices.api.receiptupdate.PaymentType;

/**
 * @author Manoj Dhanushka
 */
public class ReceiptUpdateUtil extends BaseUtil {

	private static final Log log = LogFactory.getLog(ReceiptUpdateUtil.class);

	// Agent Types
	public static final String AGENT_TYPE_AIRLINE_AGENT = "AA";

	public static final String AGENT_TYPE_GSA = "GSA";

	public static final String CURRENCY_LOCAL = "L";

	public static final String CURRENCY_BASE = "B";

	public static final String BSP_PAYAMENT_CODE = "BSP";

	public AAReceiptUpdateRS updateReceipt(AAReceiptUpdateRQ aaReceiptUpdateRQ) throws ModuleException {

		AAReceiptUpdateRS aaReceiptUpdateRS = new AAReceiptUpdateRS();
		aaReceiptUpdateRS
				.setStatusErrorsAndWarnings(new com.isaaviation.thinair.webservices.api.receiptupdate.AAStatusErrorsAndWarnings());
		String defaultAirlineCode = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode();
		extractReceiptUpdateReqParams(aaReceiptUpdateRQ);

		String strAgentCode = defaultAirlineCode + aaReceiptUpdateRQ.getAgentCode();

		BigDecimal adjAmount = aaReceiptUpdateRQ.getAmount();
		String strRemarks = aaReceiptUpdateRQ.getRemarks();
		String strCurrency = null;
		String strReceiptNumber = aaReceiptUpdateRQ.getReceiptNo();
		String strAdjType;

		if (AppSysParamsUtil.isPaymentMethodCapturedFromReceiptNumberEnabled()) {
			// AARESAA-22923 - According to the receipt number format, it represents the payment method by 3rd, 4th and
			// 5th characters
			String paymentMethod = strReceiptNumber.substring(2, 5);
			if (paymentMethod.equals(BSP_PAYAMENT_CODE)) {
				strAdjType = BSP_PAYAMENT_CODE;
			} else {
				strAdjType = aaReceiptUpdateRQ.getTypeOfPayment().toString();
			}
		} else {
			strAdjType = aaReceiptUpdateRQ.getTypeOfPayment().toString();
		}

		if (aaReceiptUpdateRQ.getCurrencyCode() == null
				|| aaReceiptUpdateRQ.getCurrencyCode().equals(AppSysParamsUtil.getBaseCurrency())) {
			strCurrency = CURRENCY_BASE;
		} else {
			strCurrency = CURRENCY_LOCAL;
		}

		String strCarrier = null;
		if (AppSysParamsUtil.getAdjustmentCarrierList() != null) {
			strCarrier = aaReceiptUpdateRQ.getCarrier();
		} else {
			strCarrier = AppSysParamsUtil.getDefaultCarrierCode(); // set default carrier as adjustment carrier if no
																	// other carriers.
		}

		if (strAdjType.equals(PaymentType.CASH.toString()))
			WebServicesModuleUtils.getTravelAgentFinanceBD().recordPayment(strAgentCode, adjAmount, strRemarks,
					CollectionTypeEnum.CASH, strCurrency, strCarrier, strReceiptNumber);

		if (strAdjType.equals(PaymentType.CHEQUE.toString()))
			WebServicesModuleUtils.getTravelAgentFinanceBD().recordPayment(strAgentCode, adjAmount, strRemarks,
					CollectionTypeEnum.CHEQUE, strCurrency, strCarrier, strReceiptNumber);

		if (strAdjType.equals(PaymentType.CREDIT.toString()))
			WebServicesModuleUtils.getTravelAgentFinanceBD().recordCredit(strAgentCode, adjAmount, strRemarks, "", strCurrency,
					strCarrier, strReceiptNumber);

		if (strAdjType.equals(PaymentType.DEBIT.toString()))
			WebServicesModuleUtils.getTravelAgentFinanceBD().recordDebit(strAgentCode, adjAmount, strRemarks, "", strCurrency,
					strCarrier, strReceiptNumber);

		if (strAdjType.equals(PaymentType.ADJUSTMENT.toString()))
			WebServicesModuleUtils.getTravelAgentFinanceBD().recordPaymentReverse(strAgentCode, adjAmount, strRemarks,
					strCurrency, strCarrier, strReceiptNumber);

		if (strAdjType.equals(PaymentType.BSP.toString()))
			WebServicesModuleUtils.getTravelAgentFinanceBD().recordBSPCredit(strAgentCode, adjAmount, strRemarks, "",
					strCurrency, strCarrier, strReceiptNumber);

		aaReceiptUpdateRS.getStatusErrorsAndWarnings().setSuccess(true);

		return aaReceiptUpdateRS;
	}

	public void extractReceiptUpdateReqParams(AAReceiptUpdateRQ aaReceiptUpdateRQ) throws ModuleException {

		String defaultAirlineCode = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode();
		String userId = defaultAirlineCode + BeanUtils.nullHandler(aaReceiptUpdateRQ.getUserId());
		User user = WebServicesModuleUtils.getSecurityBD().getUserBasicDetails(userId);
		Agent userAgent = WebServicesModuleUtils.getTravelAgentBD().getAgent(user.getAgentCode());

		String strAgentCode = defaultAirlineCode + BeanUtils.nullHandler(aaReceiptUpdateRQ.getAgentCode());
		Agent agent = WebServicesModuleUtils.getTravelAgentBD().getAgent(strAgentCode);

		if (!userAgent.getAgentTypeCode().equals(AppSysParamsUtil.getCarrierAgent())
				&& !userAgent.getAgentTypeCode().equals(AGENT_TYPE_GSA)) {
			log.debug("User does not have sufficient privilages");
			throw new ModuleException("webservices.unauthorized.access");
		}
		if (aaReceiptUpdateRQ.getAgentCode() == null) {
			throw new ModuleException("webservices.agentcode.invalid");
		} else {

			if (agent == null) {
				throw new ModuleException("webservices.agentcode.invalid");
			} else {
				if (userAgent.getAgentTypeCode().equals(AGENT_TYPE_GSA)) {
					checkForGSAPerformSave(userId, userAgent.getAgentCode(), strAgentCode);
					if (!(agent.getGsaCode().equals(userAgent.getAgentCode()))) {
						throw new ModuleException("webservices.agent.GSA.notreported");
					}
				}
				if (AppSysParamsUtil.isPaymentMethodCapturedFromReceiptNumberEnabled()) {
					String paymentMethod = aaReceiptUpdateRQ.getReceiptNo().substring(2, 5);
					if (!paymentMethod.equals(BSP_PAYAMENT_CODE)
							&& !agent.getAccountCode().equals(aaReceiptUpdateRQ.getAccountCode())) {
						throw new ModuleException("webservices.agentcode.accountcode.invalid");
					}
				} else if (!agent.getAccountCode().equals(aaReceiptUpdateRQ.getAccountCode())) {
					throw new ModuleException("webservices.agentcode.accountcode.invalid");
				}
				if (aaReceiptUpdateRQ.getCurrencyCode() != null) {
					if (!aaReceiptUpdateRQ.getCurrencyCode().equals(AppSysParamsUtil.getBaseCurrency())
							&& !aaReceiptUpdateRQ.getCurrencyCode().equals(agent.getCurrencyCode())) {
						throw new ModuleException("webservices.currencyCode.invalid");
					}
				}
			}
		}

		if (aaReceiptUpdateRQ.getTypeOfPayment() == null) {
			throw new ModuleException("webservices.payment.type.invalid");
		}

		if (aaReceiptUpdateRQ.getCarrier() == null) {
			throw new ModuleException("webservices.carrier.type.invalid");
		} else {
			if (AppSysParamsUtil.getAdjustmentCarrierList() != null) {
				boolean validCarrierCode = false;
				for (String carrier : AppSysParamsUtil.getAdjustmentCarrierList()) {
					if (carrier.equals(aaReceiptUpdateRQ.getCarrier())) {
						validCarrierCode = true;
						break;
					}
				}
				if (!validCarrierCode) {
					throw new ModuleException("webservices.carrier.type.invalid");
				}

				if (!aaReceiptUpdateRQ.getCarrier().equals(user.getDefaultCarrierCode())) {
					throw new ModuleException("webservices.agent.user.airline");
				}
			}
		}

		if (BeanUtils.isNull(aaReceiptUpdateRQ.getReceiptNo())) {
			throw new ModuleException("webservices.receiptnumber.invalid");
		} else {
			AgentTransaction agentTransaction = WebServicesModuleUtils.getTravelAgentFinanceBD().getAgentTransaction(
					aaReceiptUpdateRQ.getReceiptNo());
			if (agentTransaction != null) {
				throw new ModuleException("webservices.receiptnumber.exists");
			}
		}

		if (BeanUtils.isNull(aaReceiptUpdateRQ.getRemarks())) {
			throw new ModuleException("webservices.remarks.invalid");
		}

		if (aaReceiptUpdateRQ.getAmount() == null || aaReceiptUpdateRQ.getAmount().compareTo(BigDecimal.ZERO) < 0) {
			throw new ModuleException("webservices.payment.amount.invalid");
		}
	}

	/**
	 * Method will reassure that Invoice Settlement for Agent Save not initiated by GSA for GSA
	 * 
	 * @param request
	 * @throws ModuleException
	 * @throws ModuleException
	 */
	private static void checkForGSAPerformSave(String userId, String userAgentCode, String agentCode) throws ModuleException {

		String strAgentCode = StringUtil.getNotNullString(agentCode);
		if (userAgentCode.equals(strAgentCode.trim())) {
			log.error("Unauthorized operation:" + userId + ":" + "GSA perform illegal Settlement");
			throw new ModuleException("webservices.unauthorized.operation");
		}
	}

}
