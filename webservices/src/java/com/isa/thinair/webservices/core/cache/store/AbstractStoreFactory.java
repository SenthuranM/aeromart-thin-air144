package com.isa.thinair.webservices.core.cache.store;

import java.util.Map;

public class AbstractStoreFactory<Store> {
	private String storeType;
	private Map<String, Store> stores;

	public String getStoreType() {
		return storeType;
	}

	public void setStoreType(String storeType) {
		this.storeType = storeType;
	}

	public Map<String, Store> getStores() {
		return stores;
	}

	public void setStores(Map<String, Store> stores) {
		this.stores = stores;
	}

	public Store getStore() {
		String storeType = this.getStoreType();
		Map<String, Store> stores = this.getStores();
		return stores.get(storeType);
	}

}
