/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.core.service;

import javax.jws.WebService;

import com.isa.thinair.webservices.api.service.AAResWebServicesForPayWA;

/**
 * @author Dhanya
 * 
 * @since 2.0
 */
@WebService(serviceName = "AAResWebServicesForPayWA", targetNamespace = "http://www.opentravel.org/OTA/2003/05", endpointInterface = "com.isa.thinair.webservices.api.service.AAResWebServicesForPayWA")
public class AAResWebServicesForPayWAImpl extends BaseWebServicesImpl implements AAResWebServicesForPayWA {
}