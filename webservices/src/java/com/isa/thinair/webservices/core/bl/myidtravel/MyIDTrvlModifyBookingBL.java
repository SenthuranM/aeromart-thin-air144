/**
 * 
 */
package com.isa.thinair.webservices.core.bl.myidtravel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAReadRQ;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteBalanceQueryRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteModifyRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants.MyIDTravelConstants;
import com.isa.thinair.webservices.core.interlineUtil.ModifyBookingInterlineUtil;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.ExceptionUtil;
import com.isa.thinair.webservices.core.util.MyIDTrvlCommonUtil;
import com.isa.thinair.webservices.core.util.WSReservationUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterBookingModifyRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterBookingModifyResponse;

/**
 * 
 * @author Janaka Padukka
 * 
 */
public class MyIDTrvlModifyBookingBL {

	private final static Log log = LogFactory.getLog(MyIDTrvlModifyBookingBL.class);

	public static MyIDTravelResAdapterBookingModifyResponse execute(
			MyIDTravelResAdapterBookingModifyRequest myIDModBookingReq) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN modifyBooking(MyIDTravelResAdapterCancelBookingRequest)");
		}

		MyIDTravelResAdapterBookingModifyResponse myIDModBookingRes = null;
		try {			
			myIDModBookingRes = modifyBookingRequote(myIDModBookingReq);
		} catch (Exception ex) {
			log.error("modifyBooking(MyIDTravelResAdapterCancelBookingRequest) failed.", ex);
			if (myIDModBookingRes == null) {
				myIDModBookingRes = new MyIDTravelResAdapterBookingModifyResponse();
				OTAAirBookRS otaAirBookRS = new OTAAirBookRS();
				otaAirBookRS.setEchoToken(myIDModBookingReq.getOTAAirBookModifyRQ().getEchoToken());
				myIDModBookingRes.setOTAAirBookRS(otaAirBookRS);
			}
			if (myIDModBookingRes.getOTAAirBookRS().getErrors() == null)
				myIDModBookingRes.getOTAAirBookRS().setErrors(new ErrorsType());
			ExceptionUtil
					.addOTAErrrorsAndWarnings(myIDModBookingRes.getOTAAirBookRS().getErrors().getError(), null, ex);
		}
		if (log.isDebugEnabled())
			log.debug("END modifyBooking(MyIDTravelResAdapterCancelBookingRequest)");
		return myIDModBookingRes;
	}

	
	
	private static MyIDTravelResAdapterBookingModifyResponse modifyBookingRequote(
			MyIDTravelResAdapterBookingModifyRequest myIDModBookingReq) throws WebservicesException, ModuleException {

		MyIDTravelResAdapterBookingModifyResponse myIDModBookingRes = new MyIDTravelResAdapterBookingModifyResponse();
		myIDModBookingRes.setEmployeeData(myIDModBookingReq.getEmployeeData());

		OTAAirBookModifyRQ otaAirBookModifyRQ = myIDModBookingReq.getOTAAirBookModifyRQ();

		String pnr = otaAirBookModifyRQ.getAirReservation().getBookingReferenceID().get(0).getID();
		LCCClientReservation reservation = MyIDTrvlCommonUtil.getLccReservation(pnr);
		
		int[] paxCounts = { reservation.getTotalPaxAdultCount(), reservation.getTotalPaxChildCount(),
				reservation.getTotalPaxInfantCount() };
		
		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
		
		//MyIDTravel always allow same fare offering in modifications
		
		if (MyIDTravelConstants.MOD_TYPE_CANCEL_SEGMENT.equals(otaAirBookModifyRQ.getAirBookModifyRQ()
				.getModificationType())) {

			WSReservationUtil.authorize(privilegeKeys, null,
					PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_CANCEL_SEGMENT);

			// Extract pnr seg id from rph
			List<Integer> pnrSegIdsForCNX = extractPNRSegIds(reservation, otaAirBookModifyRQ.getAirBookModifyRQ()
					.getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption());
			
			ModifyBookingInterlineUtil.authorizeModification(privilegeKeys, reservation,
					PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX, pnrSegIdsForCNX);
			
			 Map<Integer, LCCClientReservationSegment> existingConfirmedSegmentsMap = getExistingConfirmedSegments(reservation);
			
		
				Pair<String,List<FlightSegmentDTO>>  cancellingFlightSegments = MyIDTrvlCommonUtil.getFlightSegmentDTOs(otaAirBookModifyRQ.getAirBookModifyRQ()
						.getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption());
				
				Pair<String,List<FlightSegmentDTO>>  existingFlightSegments = MyIDTrvlCommonUtil.getFlightSegmentDTOs(otaAirBookModifyRQ.getAirReservation()
						.getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption());
				
				if(cancellingFlightSegments.getLeft().equals(existingFlightSegments.getLeft())) {
					existingFlightSegments.getRight().removeAll(cancellingFlightSegments.getRight());					
					FlightPriceRQ priceRequest = MyIDTrvlCommonUtil.extractPriceQuoteReqParamsForRequote(existingFlightSegments.getRight(), paxCounts, existingConfirmedSegmentsMap, existingFlightSegments.getLeft());
					
					FlightPriceRS priceResponse = WebServicesModuleUtils.getAirproxySearchAndQuoteBD().quoteFlightPrice(priceRequest, null);
					
					if(priceResponse != null && priceResponse.getSelectedPriceFlightInfo() != null) {
						PriceInfoTO requotedPriceInfo = priceResponse.getSelectedPriceFlightInfo();
						CustomChargesTO customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
						RequoteModifyRQ requoteModifyRQ = new RequoteModifyRQ();
						Set<String> removedSegmentIds = new HashSet<String>(pnrSegIdsForCNX.size());
						for (Integer segId : pnrSegIdsForCNX)
							removedSegmentIds.add(segId.toString());
						requoteModifyRQ.setRemovedSegmentIds(removedSegmentIds);
						requoteModifyRQ.setPnr(reservation.getPNR());
						requoteModifyRQ.setCustomCharges(customChargesTO);
						requoteModifyRQ.setVersion(reservation.getVersion());
						QuotedFareRebuildDTO fareInfo = new QuotedFareRebuildDTO(priceResponse.getSelectedPriceFlightInfo().getFareSegChargeTO(), priceRequest, null);
						requoteModifyRQ.setFareInfo(fareInfo);

						requoteModifyRQ.setLastFareQuoteDate(requotedPriceInfo.getLastFareQuotedDate());
						requoteModifyRQ.setFQWithinValidity(requotedPriceInfo.isFQWithinValidity());
						requoteModifyRQ.setPaymentType(ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS);

						WebServicesModuleUtils.getAirproxyReservationBD().requoteModifySegments(requoteModifyRQ, MyIDTrvlCommonUtil.getTrackInfo());
					} else {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_MODIFY,
								"Valid fare not available for the requested modification. Please contact airline to modify");
					}	
				} else {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_MODIFY,
							"Booking classes of new flights different. Please contact airline to modify");
				}
		} else if (MyIDTravelConstants.MOD_TYPE_REBOOK_SEGMENT.equals(otaAirBookModifyRQ.getAirBookModifyRQ()
				.getModificationType())
				|| (MyIDTravelConstants.MOD_TYPE_REBOOK_ALL.equals(otaAirBookModifyRQ.getAirBookModifyRQ()
						.getModificationType()) && otaAirBookModifyRQ.getAirBookModifyRQ().getAirItinerary()
						.getOriginDestinationOptions().getOriginDestinationOption().size() == 1)) {

			
			List<Integer> pnrSegIdsForModification = extractPNRSegIds(reservation, otaAirBookModifyRQ.getAirReservation()
					.getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption());
			ModifyBookingInterlineUtil.authorizeModification(privilegeKeys, reservation,
					PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX, pnrSegIdsForModification);
				
			Map<Integer, LCCClientReservationSegment> existingConfirmedSegmentsMap = getExistingConfirmedSegments(reservation);
			
			List<FlightSegmentDTO> existingFlightSegments = AirProxyReservationUtil.populateFlightSegmentDTO(existingConfirmedSegmentsMap.values());
			
			Pair<String,List<FlightSegmentDTO>>  newFlightSegments = MyIDTrvlCommonUtil.getFlightSegmentDTOs(otaAirBookModifyRQ.getAirBookModifyRQ()
					.getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption());
			
			Pair<String,List<FlightSegmentDTO>>  modifyingFlightSegments = MyIDTrvlCommonUtil.getFlightSegmentDTOs(otaAirBookModifyRQ.getAirReservation()
					.getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption());	
			
			if(newFlightSegments.getLeft().equals(modifyingFlightSegments.getLeft())) {

				List<FlightSegmentDTO> modifyingFlightSegmentList = modifyingFlightSegments.getRight();
				List<FlightSegmentDTO> newFlightSegmentList = newFlightSegments.getRight();
				existingFlightSegments.removeAll(modifyingFlightSegmentList);
				existingFlightSegments.addAll(newFlightSegmentList);
				
		
				
				FlightPriceRQ priceRequest = MyIDTrvlCommonUtil.extractPriceQuoteReqParamsForRequote(existingFlightSegments, paxCounts, existingConfirmedSegmentsMap, newFlightSegments.getLeft());
				
				FlightPriceRS priceResponse = WebServicesModuleUtils.getAirproxySearchAndQuoteBD().quoteFlightPrice(priceRequest, null);
				
				if(priceResponse != null && priceResponse.getSelectedPriceFlightInfo() != null) {
					
					Set<String> removedSegmentIds = new HashSet<String>(pnrSegIdsForModification.size());
					for (Integer segId : pnrSegIdsForModification) {
						removedSegmentIds.add(segId.toString());
					}
						
					
					//Baggage quote			
					Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = null;
					
					if (WebServicesModuleUtils.getMyIDTravelConfig()
							.getAutoBaggageSelectionEnabled()) {

						List<FlightSegmentDTO> baggageQuotingFlightSegments = new ArrayList<FlightSegmentDTO>();
						baggageQuotingFlightSegments.addAll(newFlightSegmentList);
						
						List<Integer> adultChildList = new ArrayList<Integer>();
						for (LCCClientReservationPax passenger : reservation.getPassengers()) {
							adultChildList.add(passenger.getPaxSequence());
						}
	
						if (AppSysParamsUtil.isONDBaggaeEnabled()) {
							String modifyingSegmentBaggageOndGroupId = null;
				
							List<Integer> linkedFlightSegments = new ArrayList<Integer>();
							for(LCCClientReservationSegment reservationSegment:existingConfirmedSegmentsMap.values()) {
								if (pnrSegIdsForModification.get(0).equals(reservationSegment.getPnrSegID())) {
									modifyingSegmentBaggageOndGroupId = reservationSegment.getBaggageONDGroupId();
									break;
								}
							}
								
							for (LCCClientReservationSegment reservationSegment : existingConfirmedSegmentsMap.values()) {
								if (!pnrSegIdsForModification.get(0).equals(reservationSegment.getPnrSegID())
										&& reservationSegment.getBaggageONDGroupId().equals(
												modifyingSegmentBaggageOndGroupId)) {
									linkedFlightSegments.add(FlightRefNumberUtil
											.getSegmentIdFromFlightRPH(reservationSegment.getFlightSegmentRefNumber()));
								}
							}
							
							
							for (Integer flightSegId : linkedFlightSegments) {
								for (FlightSegmentDTO fareQuotedSegment : existingFlightSegments) {
									if (flightSegId.equals(fareQuotedSegment.getSegmentId())) {
										baggageQuotingFlightSegments.add(fareQuotedSegment);
									}
								}
							}
							
						}
						
						paxExtChgMap =  MyIDTrvlCommonUtil.getPaxwiseBaggageExternalChargesForRequote(baggageQuotingFlightSegments, adultChildList, newFlightSegments.getLeft());
					}
					
					
					//Balance check	
					CustomChargesTO customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
					RequoteBalanceQueryRQ balanceQueryRQ = new RequoteBalanceQueryRQ();
					balanceQueryRQ.setRemovedSegmentIds(removedSegmentIds);
					balanceQueryRQ.setPnr(reservation.getPNR());
					balanceQueryRQ.setCustomCharges(customChargesTO);
					balanceQueryRQ.setVersion(reservation.getVersion());
					balanceQueryRQ.setPaxExtChgMap(paxExtChgMap);
					
					PriceInfoTO requotedPriceInfo = priceResponse.getSelectedPriceFlightInfo();
					
					QuotedFareRebuildDTO fareInfo = new QuotedFareRebuildDTO(requotedPriceInfo.getFareSegChargeTO(), priceRequest, null);
					balanceQueryRQ.setFareInfo(fareInfo);
					balanceQueryRQ.setLastFareQuoteDate(requotedPriceInfo.getLastFareQuotedDate());
					balanceQueryRQ.setFQWithinValidity(requotedPriceInfo.isFQWithinValidity());

					ReservationBalanceTO reservationBalanceTO = WebServicesModuleUtils.getAirproxyReservationBD().getRequoteBalanceSummary(
							balanceQueryRQ, MyIDTrvlCommonUtil.getTrackInfo());
					
					
					if(!reservationBalanceTO.hasBalanceToPay()) {
						RequoteModifyRQ requoteModifyRQ = new RequoteModifyRQ();
						requoteModifyRQ.setRemovedSegmentIds(removedSegmentIds);
						requoteModifyRQ.setPnr(reservation.getPNR());
						requoteModifyRQ.setCustomCharges(customChargesTO);
						requoteModifyRQ.setVersion(reservation.getVersion());
						requoteModifyRQ.setFareInfo(fareInfo);
						requoteModifyRQ.setPaxExtChgMap(paxExtChgMap);
						requoteModifyRQ.setLastFareQuoteDate(requotedPriceInfo.getLastFareQuotedDate());
						requoteModifyRQ.setFQWithinValidity(requotedPriceInfo.isFQWithinValidity());
						requoteModifyRQ.setPaymentType(ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS);

						WebServicesModuleUtils.getAirproxyReservationBD().requoteModifySegments(requoteModifyRQ, MyIDTrvlCommonUtil.getTrackInfo());
					} else {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_MODIFY,
								"Modification results in a payment. Please contact airline to modify");
					}
								
				} else {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_MODIFY,
							"Valid fare not available for the requested modification. Please contact airline to modify");
				}	
			} else {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_MODIFY,
						"Booking classes of new flights different. Please contact airline to modify");
			}	
		}
		
		OTAReadRQ readRQ = MyIDTrvlCommonUtil.generateReadRQ(pnr, otaAirBookModifyRQ.getPOS(),
				otaAirBookModifyRQ.getSequenceNmbr(), otaAirBookModifyRQ.getEchoToken());

		OTAAirBookRS otaAirBookRS = MyIDTrvlCommonUtil.generateOTAAirBookRS(readRQ, false, false, true);
		myIDModBookingRes.setOTAAirBookRS(otaAirBookRS);

		return myIDModBookingRes;
		
	}
	
	private static List<Integer> extractPNRSegIds(LCCClientReservation reservation,
			List<OriginDestinationOptionType> originDestOptions) throws WebservicesException {

		List<String> rphs = new ArrayList<String>();
		for (OriginDestinationOptionType originDestOption : originDestOptions) {

			for (BookFlightSegmentType flightSegment : originDestOption.getFlightSegment()) {
				rphs.add(flightSegment.getRPH());
			}

		}
		Collection<LCCClientReservationSegment> segments = reservation.getSegments();

		List<Integer> pnrSegIds = new ArrayList<Integer>();

		for (String rph : rphs) {
			for (LCCClientReservationSegment segment : segments) {
				if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(segment.getStatus())
						&& rph.equals(FlightRefNumberUtil.getPnrSegIdFromPnrSegRPH(segment.getBookingFlightSegmentRefNumber()).toString())) {
					pnrSegIds.add(segment.getPnrSegID());
				}
			}
		}

		return pnrSegIds;
	}
	
	
	private static Map<Integer, LCCClientReservationSegment> getExistingConfirmedSegments(LCCClientReservation reservation) {
		Set<LCCClientReservationSegment> segments = reservation.getSegments();
		Map<Integer, LCCClientReservationSegment> confirmedSegments = new HashMap<Integer, LCCClientReservationSegment>();

		for (LCCClientReservationSegment segment : segments) {
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(segment.getStatus())) {
				confirmedSegments.put(FlightRefNumberUtil.getSegmentIdFromFlightRPH(segment.getFlightSegmentRefNumber()), segment);
			}
		}

		return confirmedSegments;
	}

}
