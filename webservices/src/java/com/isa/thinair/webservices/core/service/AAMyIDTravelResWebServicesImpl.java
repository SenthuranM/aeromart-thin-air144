package com.isa.thinair.webservices.core.service;

import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.ws.soap.Addressing;

import com.isa.thinair.webservices.api.service.AAMyIDTravelResWebServices;
import com.isa.thinair.webservices.core.bl.myidtravel.MyIDTrvlAvailabilitySearchBL;
import com.isa.thinair.webservices.core.bl.myidtravel.MyIDTrvlBookBL;
import com.isa.thinair.webservices.core.bl.myidtravel.MyIDTrvlCancelBookingBL;
import com.isa.thinair.webservices.core.bl.myidtravel.MyIDTrvlCancelSegmentSellBL;
import com.isa.thinair.webservices.core.bl.myidtravel.MyIDTrvlModifyBookingBL;
import com.isa.thinair.webservices.core.bl.myidtravel.MyIDTrvlPingBL;
import com.isa.thinair.webservices.core.bl.myidtravel.MyIDTrvlPriceQuoteBL;
import com.isa.thinair.webservices.core.bl.myidtravel.MyIDTrvlRefundTicketBL;
import com.isa.thinair.webservices.core.bl.myidtravel.MyIDTrvlRetrieveBookingBL;
import com.isa.thinair.webservices.core.bl.myidtravel.MyIDTrvlRetrieveTicketBL;
import com.isa.thinair.webservices.core.bl.myidtravel.MyIDTrvlSegmentSellBL;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterAvailabilityRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterAvailabilityResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterBookingModifyRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterBookingModifyResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterBookingRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterBookingResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterCancelBookingRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterCancelBookingResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterCancelSegmentSellRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterCancelSegmentSellResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterPingRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterPingResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterPricingRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterPricingResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterRefundTicketRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterRefundTicketResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterRetrieveBookingRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterRetrieveBookingResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterRetrieveTicketRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterRetrieveTicketResponse;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterSegmentSellRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterSegmentSellResponse;

/**
 * @author Manoj Dhanushka
 */
@WebService(name = "AAMyIDTravelResWebServices", targetNamespace = "http://www.opentravel.org/OTA/2003/05", endpointInterface = "com.isa.thinair.webservices.api.service.AAMyIDTravelResWebServices")
@HandlerChain(file = "/WEB-INF/myidhandler.xml")
@Addressing
public class AAMyIDTravelResWebServicesImpl implements AAMyIDTravelResWebServices {

	@Override
	public MyIDTravelResAdapterAvailabilityResponse getAvailability(
			MyIDTravelResAdapterAvailabilityRequest myIDTravelResAdapterAvailabilityRequest) {
		return MyIDTrvlAvailabilitySearchBL.execute(myIDTravelResAdapterAvailabilityRequest);
	}

	@Override
	public MyIDTravelResAdapterSegmentSellResponse doSegmentSell(
			MyIDTravelResAdapterSegmentSellRequest myIDTravelResAdapterSegmentSellRequest) {
		return MyIDTrvlSegmentSellBL.execute(myIDTravelResAdapterSegmentSellRequest);
	}

	@Override
	public MyIDTravelResAdapterCancelSegmentSellResponse cancelSegmentSell(
			MyIDTravelResAdapterCancelSegmentSellRequest myIDTravelResAdapterCancelSegmentSellRequest) {
		return MyIDTrvlCancelSegmentSellBL.execute(myIDTravelResAdapterCancelSegmentSellRequest);
	}

	@Override
	public MyIDTravelResAdapterPricingResponse pricing(MyIDTravelResAdapterPricingRequest myIDTravelResAdapterPricingRequest) {
		return MyIDTrvlPriceQuoteBL.execute(myIDTravelResAdapterPricingRequest);
	}

	@Override
	public MyIDTravelResAdapterBookingResponse book(MyIDTravelResAdapterBookingRequest myIDTravelResAdapterBookingRequest) {
		return MyIDTrvlBookBL.execute(myIDTravelResAdapterBookingRequest);
	}

	@Override
	public MyIDTravelResAdapterRetrieveBookingResponse retrieveBooking(
			MyIDTravelResAdapterRetrieveBookingRequest myIDTravelResAdapterRetrieveBookingRequest) {
		return MyIDTrvlRetrieveBookingBL.execute(myIDTravelResAdapterRetrieveBookingRequest);
	}

	@Override
	public MyIDTravelResAdapterRetrieveTicketResponse retrieveTicket(
			MyIDTravelResAdapterRetrieveTicketRequest myIDTravelResAdapterRetrieveTicketRequest) {
		return MyIDTrvlRetrieveTicketBL.execute(myIDTravelResAdapterRetrieveTicketRequest);
	}

	@Override
	public MyIDTravelResAdapterCancelBookingResponse cancelBooking(
			MyIDTravelResAdapterCancelBookingRequest myIDTravelResAdapterCancelBookingRequest) {
		return MyIDTrvlCancelBookingBL.execute(myIDTravelResAdapterCancelBookingRequest);
	}

	@Override
	public MyIDTravelResAdapterBookingModifyResponse modifyBooking(
			MyIDTravelResAdapterBookingModifyRequest myIDTravelResAdapterBookingModifyRequest) {
		return MyIDTrvlModifyBookingBL.execute(myIDTravelResAdapterBookingModifyRequest);
	}

	@Override
	public MyIDTravelResAdapterRefundTicketResponse refundTicket(
			MyIDTravelResAdapterRefundTicketRequest myIDTravelResAdapterRefundTicketRequest) {
		return MyIDTrvlRefundTicketBL.execute(myIDTravelResAdapterRefundTicketRequest);
	}

	@Override
	public MyIDTravelResAdapterPingResponse ping(MyIDTravelResAdapterPingRequest myIDTravelResAdapterPingRequest) {
		return MyIDTrvlPingBL.execute(myIDTravelResAdapterPingRequest);
	}

}
