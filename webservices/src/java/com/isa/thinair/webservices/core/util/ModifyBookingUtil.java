package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AddressType;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.AirReservationType;
import org.opentravel.ota._2003._05.AirReservationType.Fulfillment;
import org.opentravel.ota._2003._05.AirReservationType.Fulfillment.PaymentDetails;
import org.opentravel.ota._2003._05.AirTravelerType;
import org.opentravel.ota._2003._05.AirTravelerType.CustLoyalty;
import org.opentravel.ota._2003._05.AirTravelerType.Document;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookModifyRQ;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.PaymentDetailType;
import org.opentravel.ota._2003._05.SourceType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SpecialServiceRequests.SpecialServiceRequest;
import org.opentravel.ota._2003._05.TPAExtensionsType;
import org.opentravel.ota._2003._05.TravelerInfoType;
import org.opentravel.ota._2003._05.TravelersFulfillments;
import org.w3c.dom.Element;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airproxy.api.dto.ReservationConstants;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientBalancePayment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airreservation.api.dto.AgentCreditInfo;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.dto.PNRModifyPreValidationStatesTO;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO.PAYMENT_REF_TYPE;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.decorator.ReservationMediator;
import com.isa.thinair.airreservation.api.mediators.ReservationValidationUtils;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.ISegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationSSRUtil;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.BookingCategory;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.ClientCommonInfoDTO;
import com.isa.thinair.commons.api.dto.PaxCategoryFoidTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.dto.PAXSummeryDTO;
import com.isa.thinair.webplatform.api.dto.ResBalancesSummaryDTO;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ExternalChargesMediator;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.TPAExtensionUtil;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.config.WebservicesConfig;
import com.isa.thinair.webservices.core.interlineUtil.BookInterlineUtil;
import com.isa.thinair.webservices.core.interlineUtil.ReservationQueryInterlineUtil;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAdminInfoType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookModifyRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirReservationExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAlterationBalancesType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAExternalPayTxType;

/**
 * Reservation modification processes.
 * 
 * @author Mehdi
 * @author Nasly
 * @author Vino
 */
public class ModifyBookingUtil extends BaseUtil {

	protected static final Log log = LogFactory.getLog(ModifyBookingUtil.class);

	private static WebservicesConfig webServicesConfig = WebServicesModuleUtils.getWebServicesConfig();

	/**
	 * Pay outstanding amount for a reservation.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @param airBookModifyRQAAExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void balancePayment(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt airBookModifyRQAAExt, Collection privilegeKeys)
			throws ModuleException, WebservicesException, Exception {

		ReservationBD reservationDelegate = WebServicesModuleUtils.getReservationBD();

		if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {// Already cancelled
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_IS_ALREADY_CANCELLED_, "");
		}

		BigDecimal balanceAmountToPay = ReservationUtil.getTotalBalToPay(reservation);

		if (balanceAmountToPay.doubleValue() == 0) {// already paid
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_IS_ALREADY_PAID, null);
		}

		AAExternalPayTxType wsExternalPayTx = null;

		if (airBookModifyRQAAExt != null) {
			wsExternalPayTx = airBookModifyRQAAExt.getCurrentExtPayTxInfo();
		}

		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();

		SourceType pos = otaAirBookModifyRQ.getPOS().getSource().get(0);

		String BCT = pos.getBookingChannel().getType();
		boolean isSuccess = false;
		int paxCount = 0;

		// extract existing paxIds
		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
		for (ReservationPax reservationPax : reservation.getPassengers()) {
			pnrPaxIds.add(reservationPax.getPnrPaxId());
		}

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		if (otaAirBookModifyRQ.getAirBookModifyRQ().getTravelersFulfillments() == null) {// full payment expected
			PaymentDetails paymentDetails = otaAirBookModifyRQ.getAirBookModifyRQ().getFulfillment().getPaymentDetails();

			PaymentAssembler reservationPayments = new PaymentAssembler();
			OTAUtils.extractReservationPayments(paymentDetails.getPaymentDetail(), reservationPayments,
					userPrincipal.getAgentCurrencyCode(), exchangeRateProxy);

			WSReservationUtil.authorizePayment(privilegeKeys, reservationPayments, userPrincipal.getAgentCode(), pnrPaxIds);

			QuotedChargeDTO serviceCharge = null;
			Object[] firstPaxPnrPaxFareId = null;
			boolean isPmtIntegrationChannel = BCT
					.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_ATM))
					|| BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_CDM))
					|| BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_INTERNET));
			
			String pmtClientIdentifier = (String) ThreadLocalData.getCurrentTnxParam(WebservicesContext.CLIENT_IDENTIFIER);
			if (isPmtIntegrationChannel) {
				serviceCharge = (QuotedChargeDTO) ThreadLocalData.getCurrentTnxParam(Transaction.RES_SERVICE_CHARGE);
				firstPaxPnrPaxFareId = (Object[]) ThreadLocalData.getCurrentTnxParam(Transaction.RES_SERVICE_CHARGED_PAX_INFO);

				if (serviceCharge != null) {
					// Service Charge Per Pax Type only for FAWRY
					SourceType posRq = otaAirBookModifyRQ.getPOS().getSource().get(0);

					if (WebservicesConstants.ClientIdentifiers.FAWRY_EBPP.equals(pmtClientIdentifier)) {
						ReservationPax reservationPax;
						for (Object element : reservation.getPassengers()) {
							reservationPax = (ReservationPax) element;

							// Non infant pax count
							if (!ReservationApiUtils.isInfantType(reservationPax)) {
								paxCount = paxCount + 1;
							}
						}
						balanceAmountToPay = AccelAeroCalculator.add(balanceAmountToPay,
								AccelAeroCalculator.parseBigDecimal(
										serviceCharge.getEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE)
												* paxCount));
						wsExternalPayTx.setPaxCount(String.valueOf(paxCount));
					} else {
						balanceAmountToPay = AccelAeroCalculator.add(balanceAmountToPay,
								AccelAeroCalculator.parseBigDecimal(serviceCharge.getEffectiveChargeAmount(PaxTypeTO.ADULT)));
					}
				} else {
					balanceAmountToPay = AccelAeroCalculator.add(balanceAmountToPay,
							AccelAeroCalculator.getDefaultBigDecimalZero());
				}
			}

			BigDecimal roundedUpBalAmountToPay = CommonUtil.getBigDecimalWithDefaultPrecision(balanceAmountToPay);

			if (BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_CDM))) {
				roundedUpBalAmountToPay = CommonUtil.getRoundedUpValue(balanceAmountToPay, 10);
			}

			if (roundedUpBalAmountToPay.compareTo(reservationPayments.getTotalPayAmount()) != 0) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_PAYMENT_AMOUNT,
						"Expected payment of " + roundedUpBalAmountToPay + ", Received "
								+ reservationPayments.getTotalPayAmount());
			}

			IPassenger passenger = new PassengerAssembler(null);

			if (isPmtIntegrationChannel) {
				wsExternalPayTx.setBankChannelCode(BCT);
				passenger.addExternalPaymentTnxInfo(ExtensionsUtil.prepareAAExtPayTxInfo(wsExternalPayTx));
				ThreadLocalData.setWSConextParam(WebservicesContext.BALANCE_QUERY_KEY,
						wsExternalPayTx.getBalanceQueryRPH());
			} else {
				ThreadLocalData.removeWSContextParam(WebservicesContext.BALANCE_QUERY_KEY);
			}

			BigDecimal paxPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
			String defaultCArrier = (userPrincipal == null)
					? AppSysParamsUtil.getDefaultCarrierCode()
					: userPrincipal.getDefaultCarrierCode();

			if (reservationPayments.getPayments().size() == 1) {// FIXME support multiple payment options in one request
				Object resPayment = reservationPayments.getPayments().iterator().next();
				if (resPayment instanceof AgentCreditInfo) {
					Collection<ReservationPax> reservationPaxes = reservation.getPassengers();
					int adultCount = 1;
					BigDecimal paymentConsumed = AccelAeroCalculator.getDefaultBigDecimalZero();
					LCCClientBalancePayment lccClientBalancePayment = new LCCClientBalancePayment();
					lccClientBalancePayment.setGroupPNR(reservation.getPnr());
					lccClientBalancePayment.setOwnerAgent(reservation.getAdminInfo().getOwnerAgentCode());
					lccClientBalancePayment.setForceConfirm(false);
					lccClientBalancePayment.setContactInfo(getContactInfo(reservation.getContactInfo()));

					boolean isGroupPNR = false;
					String version = String.valueOf(reservation.getVersion() + 1);
					if (reservation.getAdminInfo() != null
							&& reservation.getAdminInfo().getOriginChannelId() == SalesChannelsUtil.SALES_CHANNEL_LCC) {
						isGroupPNR = true;
						version = defaultCArrier + "-" + version;
					}

					for (ReservationPax traveler : reservationPaxes) {
						if (reservation.isInfantPaymentRecordedWithInfant() || !ReservationApiUtils.isInfantType(traveler)) {
							paxPaymentAmount = traveler.getTotalAvailableBalance().doubleValue() > 0
									? traveler.getTotalAvailableBalance()
									: AccelAeroCalculator.getDefaultBigDecimalZero();
							// FIXME handle scenario with some pax with credits

							// Adding service charge, if present, to the first pax
							if (firstPaxPnrPaxFareId != null && traveler.getPnrPaxId().equals(firstPaxPnrPaxFareId[0])) {
								if (serviceCharge != null) {
									if (WebservicesConstants.ClientIdentifiers.FAWRY_EBPP.equals(pmtClientIdentifier)) {
										// Per pax service charge for FAWRY
										paxPaymentAmount = AccelAeroCalculator.add(paxPaymentAmount,
												AccelAeroCalculator.parseBigDecimal(serviceCharge.getEffectiveChargeAmount(
														QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE) * paxCount));
									} else {
										// Per PNR service charge for others
										paxPaymentAmount = AccelAeroCalculator.add(paxPaymentAmount, AccelAeroCalculator
												.parseBigDecimal(serviceCharge.getEffectiveChargeAmount(PaxTypeTO.ADULT)));
									}
								} else {
									paxPaymentAmount = AccelAeroCalculator.add(paxPaymentAmount,
											AccelAeroCalculator.subtract(roundedUpBalAmountToPay, balanceAmountToPay));
								}
							}
							if (adultCount == 1 && roundedUpBalAmountToPay.doubleValue() > balanceAmountToPay.doubleValue()) {
								// add additional payment amount resulted from rounding up, to the first adult as credit
								paxPaymentAmount = AccelAeroCalculator.add(paxPaymentAmount,
										AccelAeroCalculator.subtract(roundedUpBalAmountToPay, balanceAmountToPay));
							}
							paymentConsumed = AccelAeroCalculator.add(paymentConsumed, paxPaymentAmount);
							IPayment paxPayment = new PaymentAssembler();
							String agentCode = ((AgentCreditInfo) resPayment).getAgentCode();
							String wsPaymentMethod = Agent.PAYMENT_MODE_ONACCOUNT;
							if (((AgentCreditInfo) resPayment).getPaymentReferenceTO() != null
									&& ((AgentCreditInfo) resPayment).getPaymentReferenceTO().getPaymentRefType() != null
									&& (((AgentCreditInfo) resPayment).getPaymentReferenceTO()
											.getPaymentRefType()) == PAYMENT_REF_TYPE.BSP) {
								wsPaymentMethod = Agent.PAYMENT_MODE_BSP;
							}
							paxPayment.addAgentCreditPayment(agentCode, paxPaymentAmount, null,
									WSReservationUtil.getPayCurrencyDTO(agentCode, exchangeRateProxy), null, wsPaymentMethod,
									null);

							passenger.addPassengerPayments(traveler.getPnrPaxId(), paxPayment);
							++adultCount;

							// create LCCClientPaymentAssembler for FAWRY payment
							LCCClientPaymentAssembler paymentAssembler = new LCCClientPaymentAssembler();
							// if(log.isDebugEnabled()) log.debug("[ONACC Payment, pay agent =" + agentCode+ ",amount="
							// + paxPaymentAmount + "]"+CommonsConstants.NEWLINE);
							String paymentMethod = null;
							if (Agent.PAYMENT_MODE_BSP.equals(wsPaymentMethod)) {
								paymentMethod = Agent.PAYMENT_MODE_BSP;
							}
							paymentAssembler.addAgentCreditPayment(agentCode, paxPaymentAmount,
									WSReservationUtil.getPayCurrencyDTO(agentCode, exchangeRateProxy), new Date(), null, null,
									paymentMethod, null);
							lccClientBalancePayment.addPassengerPayments(traveler.getPaxSequence(), paymentAssembler);
						}
					}

					if (paymentConsumed.compareTo(reservationPayments.getTotalPayAmount()) != 0) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_, "Invalid payment amount");
					}
					if (isPmtIntegrationChannel && serviceCharge != null) {
						// First adjustment for service charge, then do the payment in single transaction.
						if (WebservicesConstants.ClientIdentifiers.FAWRY_EBPP.equals(pmtClientIdentifier)) {
							ClientCommonInfoDTO clientInfoDTO = new ClientCommonInfoDTO();

							clientInfoDTO.setCarrierCode(defaultCArrier);
							clientInfoDTO.setIpAddress(userPrincipal.getIpAddress());
							// do the adjustment for service charge and balance payment via AirProxy

							WebServicesModuleUtils.getPassengerBD().adjustCreditManual(reservation.getPnr(),
									(Integer) firstPaxPnrPaxFareId[1], serviceCharge.getChargeRateId(),
									AccelAeroCalculator
											.parseBigDecimal(serviceCharge.getEffectiveChargeAmount(PaxTypeTO.ADULT) * paxCount),
									"Booking paid from channel " + SalesChannelsUtil.getSalesChannelName(Integer.parseInt(BCT)),
									reservation.getVersion(), null, null, null);

							TrackInfoDTO trackInfoDTO = getTrackInfo(userPrincipal);
							WebServicesModuleUtils.getAirproxyReservationBD().balancePayment(lccClientBalancePayment, version,
									isGroupPNR, AppSysParamsUtil.isFraudCheckEnabled(SalesChannelsUtil.SALES_CHANNEL_AGENT),
									clientInfoDTO, trackInfoDTO, false, false);

						} else {
							WebServicesModuleUtils.getReservationBD().updateResForPaymentWithSeriveCharge(reservation.getPnr(),
									passenger, false,
									BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_CDM)),
									(Integer) firstPaxPnrPaxFareId[1], serviceCharge.getChargeRateId(),
									AccelAeroCalculator.parseBigDecimal(serviceCharge.getEffectiveChargeAmount(PaxTypeTO.ADULT)),
									"Booking paid from channel " + SalesChannelsUtil.getSalesChannelName(Integer.parseInt(BCT)),
									reservation.getVersion(), null);
						}
					} else if (isPmtIntegrationChannel && reservation.getOriginatorPnr() != null
							&& !reservation.getOriginatorPnr().equals("")) {
						if (!AuthorizationUtil.hasPrivileges(privilegeKeys, PriviledgeConstants.PRIVI_ACC_LCC_RES)) {
							throw new WebservicesException(
									IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
									"Insufficient Privileges for Dry Interline Payments");
						}
						ReservationQueryInterlineUtil interlineUtil = new ReservationQueryInterlineUtil();
						String reservationVersion = defaultCArrier + "-" + String.valueOf(reservation.getVersion());
						ReservationListTO reservationListTO = new ReservationListTO();
						reservationListTO.setPnrNo(reservation.getPnr());
						LCCClientReservation lccReservation = interlineUtil.getReservation(reservationListTO, null,
								userPrincipal);
						ClientCommonInfoDTO clientInfoDTO = new ClientCommonInfoDTO();
						clientInfoDTO.setCarrierCode(defaultCArrier);
						clientInfoDTO.setIpAddress(userPrincipal.getIpAddress());
						TrackInfoDTO trackInfoDTO = getTrackInfo(userPrincipal);
						lccClientBalancePayment.setReservationStatus(lccReservation.getStatus());
						lccClientBalancePayment.setPnrSegments(lccReservation.getSegments());
						lccClientBalancePayment.setPassengers(lccReservation.getPassengers());
						WebServicesModuleUtils.getAirproxyReservationBD().balancePayment(lccClientBalancePayment,
								reservationVersion, isGroupPNR, false, clientInfoDTO, trackInfoDTO, false, false);
					} else {
						boolean isFirstPayment = ReservationInternalConstants.ReservationStatus.ON_HOLD
								.equals(reservation.getStatus());
						WebServicesModuleUtils.getReservationBD().updateReservationForPayment(reservation.getPnr(), passenger,
								false, BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_CDM)),
								reservation.getVersion(), getTrackInfo(userPrincipal), false, false, true, false, true,
								isFirstPayment, null, false, null);
					}
					isSuccess = true;
					if (AuthorizationUtil.hasPrivileges(privilegeKeys,
							PrivilegesKeys.WSFuncPrivilegesKeys.WS_EMAIL_ITINERARY_BALANCE_PAYMENT)) {
						ReservationContactInfo reservationContactInfo = reservation.getContactInfo();
						LCCClientPnrModesDTO pnrModesDTO = BookInterlineUtil.getPnrModesDTO(reservation.getPnr(), false, null,
								false, null, null, null);
						CommonReservationContactInfo contactInfo = new CommonReservationContactInfo();
						contactInfo.setEmail(reservationContactInfo.getEmail());
						contactInfo.setPreferredLanguage(reservationContactInfo.getPreferredLanguage());
						BookInterlineUtil.sendEmailItenary(pnrModesDTO, contactInfo, userPrincipal, false);
					}
				} else {
					// TODO - implement support for other payment types
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
							"Only agent onaccount payment supported");
				}
			} else {
				// TODO - implement for acceptiong multiple payment
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
						"Only single payment type in one request supported");
			}

			if (isSuccess) {
				ThreadLocalData.removeWSContextParam(WebservicesContext.BALANCE_QUERY_KEY);
			}

		} else {// partial payment or payment by multiple sources
			Map<Integer, IPayment> paxPayments = getPaxPaymentsForBalancePay(reservation,
					otaAirBookModifyRQ.getAirBookModifyRQ().getFulfillment(),
					otaAirBookModifyRQ.getAirBookModifyRQ().getTravelersFulfillments());
			boolean isForceConfirm = false;
			if (paxPayments.size() != reservation.getPassengers().size()) {
				isForceConfirm = true;
				// TODO test
				WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_PARTIAL_PAYMNETS);
			}

			IPassenger passenger = new PassengerAssembler(null);
			for (Integer pnrPaxId : paxPayments.keySet()) {
				// TODO test
				// authorize payments
				Collection<Integer> pnrPaxIdCollection = new ArrayList<Integer>();
				pnrPaxIdCollection.add(pnrPaxId);

				WSReservationUtil.authorizePayment(privilegeKeys, paxPayments.get(pnrPaxId), userPrincipal.getAgentCode(),
						pnrPaxIdCollection);

				passenger.addPassengerPayments(pnrPaxId, paxPayments.get(pnrPaxId));
			}

			boolean isFirstPayment = ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus());
			WebServicesModuleUtils.getReservationBD().updateReservationForPayment(reservation.getPnr(), passenger, isForceConfirm,
					false, reservation.getVersion(), null, false, false, true, false, true, isFirstPayment, null, false, null);
		}
	}

	/**
	 * Extracts pax wise payments.
	 * 
	 * TODO - validate passenger credit eligibility.
	 * 
	 * @param reservation
	 * @param fulfillment
	 * @param travelersFulfillments
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 * @throws NumberFormatException
	 */
	private Map<Integer, IPayment> getPaxPaymentsForBalancePay(Reservation reservation,
			AirReservationType.Fulfillment fulfillment, TravelersFulfillments travelersFulfillments)
			throws WebservicesException, NumberFormatException, ModuleException {
		Map<Integer, IPayment> aaPaxPaymentMap = new HashMap<Integer, IPayment>();

		// Putting passengers into a map to optimize looking up
		Map<Integer, ReservationPax> aaPaxMap = new HashMap<Integer, ReservationPax>();
		for (Object element : reservation.getPassengers()) {
			ReservationPax pax = (ReservationPax) element;
			aaPaxMap.put(pax.getPnrPaxId(), pax);
		}

		BigDecimal expectedPaxPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalPaxPayments = AccelAeroCalculator.getDefaultBigDecimalZero();
		Map<Integer, BigDecimal[]> expectedPaxesPayments = new HashMap<Integer, BigDecimal[]>();
		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		for (TravelersFulfillments.TravelerFulfillments travelerPayments : travelersFulfillments.getTravelerFulfillments()) {
			Integer pnrPaxId = Integer.parseInt(OTAUtils.getPNRPaxIdFromTravellerRPH(travelerPayments.getTravelerRefNumberRPH()));
			ReservationPax aaPax = aaPaxMap.get(pnrPaxId);
			expectedPaxPayment = aaPax.getTotalAvailableBalance().doubleValue() > 0
					? aaPax.getTotalAvailableBalance()
					: AccelAeroCalculator.getDefaultBigDecimalZero();

			PaymentAssembler paxPayment = null;
			if (ReservationApiUtils.isAdultType(aaPax) || ReservationApiUtils.isChildType(aaPax)) {
				if (aaPaxPaymentMap.containsKey(pnrPaxId)) {
					paxPayment = (PaymentAssembler) aaPaxPaymentMap.get(pnrPaxId);
					expectedPaxesPayments.remove(pnrPaxId);
				} else {
					paxPayment = new PaymentAssembler();
				}
				OTAUtils.extractReservationPayments(travelerPayments.getPaymentDetail(), paxPayment,
						principal.getAgentCurrencyCode(), exchangeRateProxy);

				// Infant payment is provided seperately in WS payment request
				if (aaPax.getInfants() != null && aaPax.getInfants().size() > 0) {
					ReservationPax infantPax = aaPax.getInfants().iterator().next();

					// TODO - optimize following operation
					for (TravelersFulfillments.TravelerFulfillments innerTravelerPayments : travelersFulfillments
							.getTravelerFulfillments()) {
						Integer innerPnrPaxId = Integer
								.parseInt(OTAUtils.getPNRPaxIdFromTravellerRPH(innerTravelerPayments.getTravelerRefNumberRPH()));
						if (innerPnrPaxId.equals(infantPax.getPnrPaxId())) {
							OTAUtils.extractReservationPayments(innerTravelerPayments.getPaymentDetail(), paxPayment,
									principal.getAgentCurrencyCode(), exchangeRateProxy);
							break;
						}
					}
				}

				ServiceTaxCalculator.calculatePaxJNTax(paxPayment);

				expectedPaxesPayments.put(pnrPaxId, new BigDecimal[] { expectedPaxPayment, paxPayment.getTotalPayAmount() });

				aaPaxPaymentMap.put(pnrPaxId, paxPayment);
			}
		}

		// Validate individual pax payments
		for (Integer integer : expectedPaxesPayments.keySet()) {
			BigDecimal[] expectedPayments = expectedPaxesPayments.get(integer);
			if (log.isDebugEnabled()) {
				log.debug(expectedPayments[0] + " " + expectedPayments[1]);
			}

			if (expectedPayments[0].compareTo(expectedPayments[1]) != 0) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
						"Partial payment for a traveler not allowed");
			}
			totalPaxPayments = AccelAeroCalculator.add(totalPaxPayments, expectedPayments[1]);
		}

		// Check if total payment equals sum of individual pax payments
		PaymentAssembler reservationPayments = new PaymentAssembler();
		OTAUtils.extractReservationPayments(fulfillment.getPaymentDetails().getPaymentDetail(), reservationPayments,
				principal.getAgentCurrencyCode(), exchangeRateProxy);

		if (reservationPayments.getTotalPayAmount().doubleValue() != totalPaxPayments.doubleValue()) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
					"Traveler wise payment does not match total payment");
		}

		return aaPaxPaymentMap;
	}

	/**
	 * Get balances for cancelling reservation.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public static OTAAirBookRS getBalancesForCancelRes(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ)
			throws ModuleException, WebservicesException {
		// TODO test
		ResBalancesSummaryDTO resSummaryDTO = ReservationUtil.getResBalancesForAlt(reservation, null, null,
				ReservationConstants.AlterationType.ALT_CANCEL_RES, null);
		AAAlterationBalancesType aaResCnxBalances = ExtensionsUtil.prepareWSResAlterationBalances(reservation.getPassengers(),
				resSummaryDTO, ReservationConstants.AlterationType.ALT_CANCEL_RES, 0);

		AAAirReservationExt aaAirReservationExt = new AAAirReservationExt();
		aaAirReservationExt.setAlterationBalances(aaResCnxBalances);

		OTAAirBookRS otaAirBookRS = new OTAAirBookRS();

		AirReservation airReservation = new AirReservation();
		airReservation.getBookingReferenceID().add(otaAirBookModifyRQ.getAirBookModifyRQ().getBookingReferenceID().get(0));

		Element extesions = TPAExtensionUtil.getInstance().marshall(aaAirReservationExt);
		airReservation.setTPAExtensions(new TPAExtensionsType());
		airReservation.getTPAExtensions().getAny().add(extesions);

		otaAirBookRS.getSuccessAndWarningsAndAirReservation().add(airReservation);

		return otaAirBookRS;
	}

	/**
	 * Get balances for cancelling specified OND.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	public static OTAAirBookRS getBalancesForCancelOND(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ)
			throws WebservicesException, ModuleException {
		if (otaAirBookModifyRQ.getAirBookModifyRQ().getAirItinerary().getOriginDestinationOptions() == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "OriginDestinationOptions required");
		}

		// get the pnrSegIds after checking the validity
		List<Integer> pnrSegIdsForCnx = extractPNRSegIds(reservation,
				otaAirBookModifyRQ.getAirBookModifyRQ().getAirItinerary().getOriginDestinationOptions());

		// TODO test
		ResBalancesSummaryDTO resSummaryDTO = ReservationUtil.getResBalancesForAlt(reservation, null, pnrSegIdsForCnx,
				ReservationConstants.AlterationType.ALT_CANCEL_OND, null);

		AAAlterationBalancesType aaResCnxBalances = ExtensionsUtil.prepareWSResAlterationBalances(reservation.getPassengers(),
				resSummaryDTO, ReservationConstants.AlterationType.ALT_CANCEL_OND, 0);

		AAAirReservationExt aaAirReservationExt = new AAAirReservationExt();
		aaAirReservationExt.setAlterationBalances(aaResCnxBalances);

		OTAAirBookRS otaAirBookRS = new OTAAirBookRS();

		AirReservation airReservation = new AirReservation();
		airReservation.getBookingReferenceID().add(otaAirBookModifyRQ.getAirBookModifyRQ().getBookingReferenceID().get(0));

		Element extesions = TPAExtensionUtil.getInstance().marshall(aaAirReservationExt);
		airReservation.setTPAExtensions(new TPAExtensionsType());
		airReservation.getTPAExtensions().getAny().add(extesions);

		otaAirBookRS.getSuccessAndWarningsAndAirReservation().add(airReservation);

		return otaAirBookRS;
	}

	/**
	 * Extracts pnrSegIds for cancellation/modification from the request.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @return
	 * @throws WebservicesException
	 */
	private static List<Integer> extractPNRSegIds(Reservation reservation, AirItineraryType.OriginDestinationOptions onds)
			throws WebservicesException {

		if (onds.getOriginDestinationOption() == null || onds.getOriginDestinationOption().size() != 1) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"Only one OND at a time allowed to cancel/modify");
		}

		OriginDestinationOptionType ond = onds.getOriginDestinationOption().get(0);
		int ondGroupCode = -1;
		List<Integer> pnrSegIdsForCnx = new ArrayList<Integer>();
		for (Object element : ond.getFlightSegment()) {
			BookFlightSegmentType wsFlightSeg = (BookFlightSegmentType) element;
			for (Object element2 : reservation.getSegments()) {
				ReservationSegment resSegment = (ReservationSegment) element2;
				if (resSegment.getPnrSegId() == Integer.parseInt(wsFlightSeg.getRPH())) {
					pnrSegIdsForCnx.add(Integer.parseInt(wsFlightSeg.getRPH()));
					if (ondGroupCode == -1) {
						ondGroupCode = resSegment.getOndGroupId();
					} else if (ondGroupCode != resSegment.getOndGroupId()) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Only one OND at a time allowed to cancel");
					}
					break;
				}
			}
		}
		return pnrSegIdsForCnx;
	}

	/**
	 * Returns balances for modify OND.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	public static OTAAirBookRS getBalancesForModifyOND(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ)
			throws WebservicesException, ModuleException {
		AirItineraryType.OriginDestinationOptions newOnds = otaAirBookModifyRQ.getAirBookModifyRQ().getAirItinerary()
				.getOriginDestinationOptions();
		if (newOnds == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "New OriginDestinationOptions required");
		}

		AirItineraryType.OriginDestinationOptions modifiedOND = otaAirBookModifyRQ.getAirReservation().getAirItinerary()
				.getOriginDestinationOptions();
		if (modifiedOND == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"Modified OriginDestinationOptions required");
		}

		// get the pnrSegIds after checking the validity
		List<Integer> pnrSegIdsForMod = extractPNRSegIds(reservation, modifiedOND);

		Collection<OndFareDTO> seatsFaresChargesAndSegDTOCol = getQuotedFareSeatsAndSegDTOCol(otaAirBookModifyRQ, reservation,
				pnrSegIdsForMod);

		if (seatsFaresChargesAndSegDTOCol == null) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION,
					"Modify OND balance query should be preceeded with price quoting");
		}

		// TODO test
		ResBalancesSummaryDTO resSummaryDTO = ReservationUtil.getResBalancesForAlt(reservation, seatsFaresChargesAndSegDTOCol,
				pnrSegIdsForMod, ReservationConstants.AlterationType.ALT_MODIFY_OND, null);

		AAAlterationBalancesType aaResCnxBalances = ExtensionsUtil.prepareWSResAlterationBalances(reservation.getPassengers(),
				resSummaryDTO, ReservationConstants.AlterationType.ALT_MODIFY_OND, pnrSegIdsForMod.size());

		AAAirReservationExt aaAirReservationExt = new AAAirReservationExt();
		aaAirReservationExt.setAlterationBalances(aaResCnxBalances);

		OTAAirBookRS otaAirBookRS = new OTAAirBookRS();

		AirReservation airReservation = new AirReservation();
		airReservation.getBookingReferenceID().add(otaAirBookModifyRQ.getAirBookModifyRQ().getBookingReferenceID().get(0));

		Element extesions = TPAExtensionUtil.getInstance().marshall(aaAirReservationExt);
		airReservation.setTPAExtensions(new TPAExtensionsType());
		airReservation.getTPAExtensions().getAny().add(extesions);

		otaAirBookRS.getSuccessAndWarningsAndAirReservation().add(airReservation);

		return otaAirBookRS;
	}

	/**
	 * Returns balances for add OND.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	public static OTAAirBookRS getBalancesForAddOND(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ)
			throws WebservicesException, ModuleException {
		AirItineraryType.OriginDestinationOptions newOnds = otaAirBookModifyRQ.getAirBookModifyRQ().getAirItinerary()
				.getOriginDestinationOptions();
		if (newOnds == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "OriginDestinationOptions required");
		}

		Collection<OndFareDTO> seatsFaresChargesAndSegDTOCol = getQuotedFareSeatsAndSegDTOCol(otaAirBookModifyRQ, reservation,
				null);

		if (seatsFaresChargesAndSegDTOCol == null) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION,
					"Add OND balance query should be preceeded with price quoting");
		}

		// TODO test
		ResBalancesSummaryDTO resSummaryDTO = ReservationUtil.getResBalancesForAlt(reservation, seatsFaresChargesAndSegDTOCol,
				null, ReservationConstants.AlterationType.ALT_ADD_OND, null);

		AAAlterationBalancesType aaResCnxBalances = ExtensionsUtil.prepareWSResAlterationBalances(reservation.getPassengers(),
				resSummaryDTO, ReservationConstants.AlterationType.ALT_ADD_OND, seatsFaresChargesAndSegDTOCol.size());

		AAAirReservationExt aaAirReservationExt = new AAAirReservationExt();
		aaAirReservationExt.setAlterationBalances(aaResCnxBalances);

		OTAAirBookRS otaAirBookRS = new OTAAirBookRS();

		AirReservation airReservation = new AirReservation();
		airReservation.getBookingReferenceID().add(otaAirBookModifyRQ.getAirBookModifyRQ().getBookingReferenceID().get(0));

		Element extesions = TPAExtensionUtil.getInstance().marshall(aaAirReservationExt);
		airReservation.setTPAExtensions(new TPAExtensionsType());
		airReservation.getTPAExtensions().getAny().add(extesions);

		otaAirBookRS.getSuccessAndWarningsAndAirReservation().add(airReservation);

		return otaAirBookRS;
	}

	public static OTAAirBookRS getBalancesForAddInfant(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ)
			throws WebservicesException, ModuleException {

		// needed for extracting ond charges
		ReservationMediator reservationMediator = new ReservationMediator(reservation);
		// extract posStation
		String posStation = otaAirBookModifyRQ.getPOS().getSource().get(0).getAirportCode();

		String agentCode = null;

		if (otaAirBookModifyRQ.getPOS().getSource().get(0).getBookingChannel() != null
				&& otaAirBookModifyRQ.getPOS().getSource().get(0).getBookingChannel().getCompanyName() != null) {
			agentCode = otaAirBookModifyRQ.getPOS().getSource().get(0).getBookingChannel().getCompanyName().getCode();
		}
		// get no.of.infants ..needed to get OndFareDTO's for performing addinfant
		int noOfInfants = OTAUtils.getInfantCountToModify(otaAirBookModifyRQ);
		// get the traveler list
		TravelerInfoType infoType = otaAirBookModifyRQ.getAirBookModifyRQ().getTravelerInfo();
		// the parent pnr pax ids taken from AirTravelerType
		Collection<Integer> parentPnrPaxIDS = new ArrayList<Integer>();
		if (infoType != null) {
			for (AirTravelerType airTravelerType : infoType.getAirTraveler()) {
				// will return infSeq, Parent Seq, and Parent PnrPaxID
				int ids[] = OTAUtils.getInfSeqParentSeqAndParentPnrPaxIDForAddInfant(airTravelerType);
				parentPnrPaxIDS.add(new Integer(ids[2]));
			}
		}

		// get the OnDFareDTO collection
		Collection<OndFareDTO> inventoryFares = WebServicesModuleUtils.getFlightInventoryResBD()
				.getInfantQuote(reservationMediator.getOndFareDTOs(noOfInfants), posStation, true, agentCode);
		if (inventoryFares == null) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION,
					"Infant Quote Operation has Error");
		}

		// Get the summary of the operation - have to send the parent ids's for calculating the charges by each pax
		ResBalancesSummaryDTO resSummaryDTO = ReservationUtil.getResBalancesForAlt(reservation, inventoryFares, null,
				ReservationConstants.AlterationType.ALT_ADD_INF, parentPnrPaxIDS);

		int confirmedSegcount = 0;
		Collection<ReservationSegmentDTO> resSegs = reservation.getSegmentsView();

		for (ReservationSegmentDTO seg : resSegs) {
			if (seg.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED)) {
				confirmedSegcount++;
			}
		}

		// Prepare the DTO - put it in an TPAExtenstion(marshall) and send.
		AAAlterationBalancesType aaResCnxBalances = ExtensionsUtil.prepareWSResAlterationBalances(reservation.getPassengers(),
				resSummaryDTO, ReservationConstants.AlterationType.ALT_ADD_INF, confirmedSegcount);

		AAAirReservationExt aaAirReservationExt = new AAAirReservationExt();
		aaAirReservationExt.setAlterationBalances(aaResCnxBalances);

		OTAAirBookRS otaAirBookRS = new OTAAirBookRS();

		AirReservation airReservation = new AirReservation();
		airReservation.getBookingReferenceID().add(otaAirBookModifyRQ.getAirBookModifyRQ().getBookingReferenceID().get(0));

		Element extesions = TPAExtensionUtil.getInstance().marshall(aaAirReservationExt);
		airReservation.setTPAExtensions(new TPAExtensionsType());
		airReservation.getTPAExtensions().getAny().add(extesions);

		otaAirBookRS.getSuccessAndWarningsAndAirReservation().add(airReservation);

		return otaAirBookRS;
	}

	/**
	 * Method to add user notes to reservation.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void addUserNotes(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt) throws ModuleException, WebservicesException {
		setUserNotes(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);
		WebServicesModuleUtils.getReservationBD().updateReservation(reservation, null, false, false, null, null);
	}

	/**
	 * Method to split a set of passengers from a reservation.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public Object splitReservation(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ)
			throws ModuleException, WebservicesException {

		List<AirTravelerType> listAirTraveller = otaAirBookModifyRQ.getAirBookModifyRQ().getTravelerInfo().getAirTraveler();

		Collection pnrPaxIds = getPaxIds(listAirTraveller);

		if ((pnrPaxIds.size() > 0) && (listAirTraveller.size() == pnrPaxIds.size())) {
			ServiceResponce serviceResponse = WebServicesModuleUtils.getReservationBD().splitReservation(reservation.getPnr(),
					pnrPaxIds, reservation.getVersion(), null, null, null, null, null);
			return serviceResponse.getResponseParam(CommandParamNames.PNR);
		} else {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_SPLIT_OPERATION_,
					"Pax Ids extracted are not equal to the traveller count");
		}
	}

	/**
	 * Method to update contact details.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void updateContactDetails(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt) throws ModuleException, WebservicesException {

		ReservationContactInfo resContactInfo = null;

		if ((aaAirBookModifyRQExt != null) && (aaAirBookModifyRQExt.getContactInfo() != null)) {
			resContactInfo = ExtensionsUtil.transformToAAContactInfo(aaAirBookModifyRQExt.getContactInfo());
		}

		WebServicesModuleUtils.getReservationBD().updateContactInfo(reservation.getPnr(), reservation.getVersion(),
				resContactInfo, null);
	}

	/**
	 * Method to remove passengers from the reservation.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void removePassenger(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt) throws ModuleException, WebservicesException {

		List<AirTravelerType> listAirTraveller = otaAirBookModifyRQ.getAirBookModifyRQ().getTravelerInfo().getAirTraveler();

		Collection pnrPaxIds = getPaxIds(listAirTraveller);

		if ((pnrPaxIds.size() > 0) && (listAirTraveller.size() == pnrPaxIds.size())) {

			// FIXME - Getting custom cancellation charges
			CustomChargesTO customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
			WebServicesModuleUtils.getPassengerBD().removePassengers(reservation.getPnr(), null, pnrPaxIds, customChargesTO,
					reservation.getVersion(), null, null, null);
		} else {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_SPLIT_OPERATION_,
					"Pax Ids extracted are not equal to the traveller count");
		}

	}

	/**
	 * Method to get the paxIds from the list of AirTraveller
	 * 
	 * @param listAirTraveller
	 * @return
	 */
	private Collection getPaxIds(List<AirTravelerType> listAirTraveller) {
		Collection<Integer> pnrPaxIds = new ArrayList<Integer>();

		String pnrPaxId = null;
		for (AirTravelerType airTraveller : listAirTraveller) {
			pnrPaxId = OTAUtils.getPNRPaxIdFromTravellerRPH(airTraveller.getTravelerRefNumber().getRPH());
			if ((pnrPaxId != null) && (!pnrPaxId.trim().equals(""))) {
				pnrPaxIds.add(new Integer(pnrPaxId));
			} else {
				break;
			}
		}

		return pnrPaxIds;
	}

	/**
	 * Cancel reservation.
	 * 
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void cancelReservation(Reservation res, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt) throws ModuleException, WebservicesException {
		String pnr = otaAirBookModifyRQ.getAirBookModifyRQ().getBookingReferenceID().get(0).getID();

		if (!ReservationInternalConstants.ReservationStatus.CANCEL.equals(res.getStatus())) {
			// FIXME implement custom cancellation charges
			CustomChargesTO customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
			WebServicesModuleUtils.getReservationBD().cancelReservation(pnr, customChargesTO, res.getVersion(), false, false,
					false, null, false, null, GDSInternalCodes.GDSNotifyAction.NO_ACTION.getCode(), null);
		} else {
			// Reservation is already cancelled
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_IS_ALREADY_CANCELLED_, "");
		}
	}

	/**
	 * Cancel OND.
	 * 
	 * @param res
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void cancelOND(Reservation res, OTAAirBookModifyRQ otaAirBookModifyRQ, AAAirBookModifyRQExt aaAirBookModifyRQExt)
			throws ModuleException, WebservicesException {

		String pnr = otaAirBookModifyRQ.getAirBookModifyRQ().getBookingReferenceID().get(0).getID();

		List<Integer> pnrSegIdsForCNX = extractPNRSegIds(res,
				otaAirBookModifyRQ.getAirBookModifyRQ().getAirItinerary().getOriginDestinationOptions());

		Collection<String> privilegeKeys = ThreadLocalData
				.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
		ModifyBookingUtil.authorizeModification(privilegeKeys, res,
				PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX, pnrSegIdsForCNX);

		if (!checkIfSegsAlreadyCancelled(res, pnrSegIdsForCNX)) {
			// FIXME implement custom cancellation charges

			CustomChargesTO customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
			WebServicesModuleUtils.getResSegmentBD().cancelSegments(pnr, pnrSegIdsForCNX, customChargesTO, res.getVersion(), true,
					null, false, null, true);
		} else {
			// OND is already cancelled
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_SEGMENT_IS_ALREADY_CANCELLED_BY_ANOTHER_USER_,
					"");
		}
	}

	/**
	 * Returns true if atleast one of the provided pnrSegId is already cancelled.
	 * 
	 * @param res
	 * @param pnrSegIds
	 * @return
	 */
	private boolean checkIfSegsAlreadyCancelled(Reservation res, List<Integer> pnrSegIds) {
		boolean alreadyCnx = false;
		for (ReservationSegment resSeg : res.getSegments()) {
			if (pnrSegIds.contains(resSeg.getPnrSegId())
					&& ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(resSeg.getStatus())) {
				alreadyCnx = true;
				break;
			}
		}
		return alreadyCnx;
	}

	/**
	 * Adds OND to the reservation.
	 * 
	 * @param res
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void addOND(Reservation res, OTAAirBookModifyRQ otaAirBookModifyRQ, AAAirBookModifyRQExt aaAirBookModifyRQExt)
			throws ModuleException, WebservicesException {
		AirReservationType airBookModRQ = otaAirBookModifyRQ.getAirBookModifyRQ();
		// user id
		String userId = otaAirBookModifyRQ.getPOS().getSource().get(0).getERSPUserID();
		List<OriginDestinationOptionType> newPnrSegments = airBookModRQ.getAirItinerary().getOriginDestinationOptions()
				.getOriginDestinationOption();

		boolean isGroundSegment = AddModifyGroundSegmentUtil.isSegContainGroundSeg(newPnrSegments);

		Collection<OndFareDTO> fareSeatsAndSegDTOCol = null;
		Collection<Integer> pnrSegIdsForMod = null;
		List<OriginDestinationOptionType> allSegmentsList = null;

		if (isGroundSegment && !AppSysParamsUtil.isGroundServiceEnabled()) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION,
					"Ground Segment Service is not enabled");
		}

		if (isGroundSegment) {
			List<OriginDestinationOptionType> existPnrSegments = null;

			try {
				existPnrSegments = otaAirBookModifyRQ.getAirReservation().getAirItinerary().getOriginDestinationOptions()
						.getOriginDestinationOption();
			} catch (Exception e) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION,
						"Parent Segment required for this operation");
			}

			try {
				pnrSegIdsForMod = getRefSegmentsPnr(existPnrSegments);
			} catch (NumberFormatException e) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
						"Flight Reference Number format is invalid");
			}

			if (pnrSegIdsForMod == null || pnrSegIdsForMod.size() == 0) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION,
						"Reference segment invalid");
			}

			fareSeatsAndSegDTOCol = getQuotedFareSeatsAndSegDTOCol(otaAirBookModifyRQ, res, pnrSegIdsForMod);
			allSegmentsList = combineOndExistAndNew(newPnrSegments, existPnrSegments);

			if (!AddModifyGroundSegmentUtil.isValidSegment(allSegmentsList, pnrSegIdsForMod.iterator().next())) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION,
						"Invalid Ground Segment");
			}

			if (!AddModifyGroundSegmentUtil.isModifySegmentValidSurfaceSegment(res, newPnrSegments, pnrSegIdsForMod)) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION,
						"Parent Segment Cancelled or Already there is a Ground Segment");
			}

		} else {
			// recalculating the FareSeatsAndSegmentsCollection from the modify booking request.
			fareSeatsAndSegDTOCol = getQuotedFareSeatsAndSegDTOCol(otaAirBookModifyRQ, res, null);
		}

		if (fareSeatsAndSegDTOCol == null) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION,
					"Add OND must be preceeded with Price Quote");
		}

		ISegment addedResSegs = null;
		if (isGroundSegment && (pnrSegIdsForMod != null && pnrSegIdsForMod.size() > 0)) {

			Map<Integer, String> groundStationBySegmentMap = new LinkedHashMap<Integer, String>();
			Map<Integer, Integer> groundSementFlightByAirFlightMap = new LinkedHashMap<Integer, Integer>();

			try {
				AddModifyGroundSegmentUtil.mapStationsBySegment(allSegmentsList, groundStationBySegmentMap,
						groundSementFlightByAirFlightMap);
			} catch (NumberFormatException e) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
						"Flight Reference Number format is invalid");
			}

			addedResSegs = ReservationUtil.prepareNewResSegments(res, fareSeatsAndSegDTOCol, groundStationBySegmentMap,
					groundSementFlightByAirFlightMap, pnrSegIdsForMod);

		} else {
			// Prepare reservation segments
			addedResSegs = ReservationUtil.prepareNewResSegments(res, fareSeatsAndSegDTOCol, null);
		}

		Integer intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_FULL_PAYMENT;

		// checking insurance is selected and then do the validations and modify the segment
		
		Object insuranceRefObj = ThreadLocalData.getCurrentTnxParam(Transaction.RES_INSURANCE_SELECTED);
		if (insuranceRefObj != null) {
			Integer insuranceRefNo = Integer.parseInt((String) insuranceRefObj);
			InsuranceServiceUtil.validateInsuranceOnModification(res, fareSeatsAndSegDTOCol, insuranceRefNo);
			InsuranceServiceUtil.populateInsurance(res, null, addedResSegs, insuranceRefNo, true);
		}
		
		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

		// Extract payment information
		HashMap<Integer, IPayment> paxPayments = new HashMap<Integer, IPayment>();
		if (airBookModRQ.getFulfillment() == null || airBookModRQ.getFulfillment().getPaymentDetails() == null) {
			if (!AuthorizationUtil.hasPrivilege(privilegeKeys,
					PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_OVERRIDE_ONHOLD)) {
				boolean isHoldAllowed = WSReservationUtil.isHoldAllowed(fareSeatsAndSegDTOCol);
				if (!isHoldAllowed) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
							"Hold not allowed. Need to be ticketed immediately");
				}
			}
			// Add segment without payment
			if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(res.getStatus())) {
				// Force confirm
				// TODO test check privilege
				WSReservationUtil.authorize(privilegeKeys, null,
						PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_NO_PAY);
				intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED;
			} else {// OHD or CNX
				// Retain the OHD status
				// TODO test check privilege
				WSReservationUtil.authorize(privilegeKeys, null,
						PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_NO_PAY);
				intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS;

				// calculate and set new release timestamp
				UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();
				Collection colFlightSegmentDTO = WebplatformUtil.getConfirmedDepartureSegments(res.getSegmentsView());
				Date newReleaseTimestamp = WebplatformUtil.getReleaseTimeStamp(colFlightSegmentDTO,
						privilegeKeys, true, userPrincipal != null ? userPrincipal.getAgentCode() : null,
						AppIndicatorEnum.APP_WS.toString(), new OnHoldReleaseTimeDTO());
				// Date newReleaseTimestamp = ReservationUtil.getHoldRelTimeForAddOrModOND(res, fareSeatsAndSegDTOCol);
				addedResSegs.overrideZuluReleaseTimeStamp(newReleaseTimestamp);
			}
		} else {
			// set pax wise payment
			paxPayments = getPaxPaymentsForAddOnd(res, otaAirBookModifyRQ.getAirBookModifyRQ().getFulfillment(),
					otaAirBookModifyRQ.getAirBookModifyRQ().getTravelersFulfillments(), fareSeatsAndSegDTOCol);

			for (Object element : paxPayments.keySet()) {
				Integer pnrPaxId = (Integer) element;
				addedResSegs.addPassengerPayments(pnrPaxId, paxPayments.get(pnrPaxId));
			}
		}

		// Add zero payments for passengers not having payment
		for (Object element : res.getPassengers()) {
			ReservationPax pnrPax = (ReservationPax) element;

			// Payment should always sent for the adults/children only
			if (!ReservationApiUtils.isInfantType(pnrPax)) {
				if (!paxPayments.keySet().contains(pnrPax.getPnrPaxId())) {
					IPayment payment = new PaymentAssembler();
					BookUtil.addSelectedExternalCharges(payment, CommonServicesUtil.getTravelerRPH(pnrPax, false));
					addedResSegs.addPassengerPayments(pnrPax.getPnrPaxId(), payment);
				}
			}
		}

		// Block seats quoted
		Collection blockedSeatsCol = WebServicesModuleUtils.getReservationBD().blockSeats(fareSeatsAndSegDTOCol, null);
		if (isGroundSegment) {
			WebServicesModuleUtils.getResSegmentBD().addSegments(res.getPnr(), addedResSegs, blockedSeatsCol, intPaymentMode,
					res.getVersion(), null, false, false, Integer.toString(pnrSegIdsForMod.iterator().next()), null, false, false,
					true);
		} else {

			WebServicesModuleUtils.getResSegmentBD().addSegments(res.getPnr(), addedResSegs, blockedSeatsCol, intPaymentMode,
					res.getVersion(), getTrackInfo(ThreadLocalData.getCurrentUserPrincipal()), false, false, null,
					null, false, false, true);
		}

	}

	/**
	 * Adds OND to the reservation.
	 * 
	 * @param res
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 * 
	 *             TODO - complete
	 */
	public void modifyOND(Reservation res, OTAAirBookModifyRQ otaAirBookModifyRQ, AAAirBookModifyRQExt aaAirBookModifyRQExt)
			throws ModuleException, WebservicesException {
		AirReservationType airBookModRQ = otaAirBookModifyRQ.getAirBookModifyRQ();
		// user id
		String userId = otaAirBookModifyRQ.getPOS().getSource().get(0).getERSPUserID();

		AirItineraryType.OriginDestinationOptions newOnds = otaAirBookModifyRQ.getAirBookModifyRQ().getAirItinerary()
				.getOriginDestinationOptions();
		if (newOnds == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "New OriginDestinationOptions required");
		}

		// TODO - verify fare quoted OND matches with newOnd specified

		AirItineraryType.OriginDestinationOptions modifiedOND = otaAirBookModifyRQ.getAirReservation().getAirItinerary()
				.getOriginDestinationOptions();
		if (modifiedOND == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"Modified OriginDestinationOptions required");
		}

		// get the pnrSegIds after checking the validity
		List<Integer> pnrSegIdsForMod = extractPNRSegIds(res, modifiedOND);

		Collection<String> privilegeKeys = ThreadLocalData
				.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
		// authorize modifications
		ModifyBookingUtil.authorizeModification(privilegeKeys, res,
				PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX, pnrSegIdsForMod);

		// Get FareSeatsAndSegmentsCollection from price quote done in current transaction
		Collection<OndFareDTO> fareSeatsAndSegDTOCol = getQuotedFareSeatsAndSegDTOCol(otaAirBookModifyRQ, res, pnrSegIdsForMod);

		int newSegmetsCount = 0;
		for (OndFareDTO newSegment : fareSeatsAndSegDTOCol) {
			newSegmetsCount += newSegment.getSegmentsMap().size();
		}

		if (fareSeatsAndSegDTOCol == null) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION,
					"Add OND must be preceeded with Price Quote");
		}

		// authorize segment modification According to the farerules and the segment modification privileges
		ModifyBookingUtil.authorizeSegmentWiseModification(fareSeatsAndSegDTOCol, modifiedOND, res, pnrSegIdsForMod,
				privilegeKeys);

		// Prepare reservation segments
		ISegment addedResSegs = ReservationUtil.prepareNewResSegments(res, fareSeatsAndSegDTOCol, pnrSegIdsForMod);

		Integer intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_FULL_PAYMENT;

		// checking insurance is selected and then do the validations and modify the segment
		Object insuranceReference = ThreadLocalData.getCurrentTnxParam(Transaction.RES_INSURANCE_SELECTED);
		if (insuranceReference != null) {
			Integer insuranceRefNo = Integer.parseInt((String) insuranceReference);
			InsuranceServiceUtil.validateInsuranceOnModification(res, fareSeatsAndSegDTOCol, insuranceRefNo);
			InsuranceServiceUtil.populateInsurance(res, null, addedResSegs, insuranceRefNo, true);
		}

		// Balance to pay calculation
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		ResBalancesSummaryDTO resSummaryDTO = ReservationUtil.getResBalancesForAlt(res, fareSeatsAndSegDTOCol, pnrSegIdsForMod,
				ReservationConstants.AlterationType.ALT_MODIFY_OND, null);

		BigDecimal totalExternalCharge = BookUtil.getTotalAmountOfSelectedExternalCharges(AppSysParamsUtil.getBaseCurrency(),
				exchangeRateProxy);
		BigDecimal totalAmountDue = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (resSummaryDTO.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) == 1) {

			if (resSummaryDTO.getTotalAvailableBalance().compareTo(totalExternalCharge) != 1) {
				totalAmountDue = AccelAeroCalculator.subtract(totalExternalCharge, resSummaryDTO.getTotalAvailableBalance());
			} else {
				totalAmountDue = AccelAeroCalculator.getDefaultBigDecimalZero();
			}
		} else {
			totalAmountDue = AccelAeroCalculator.add(resSummaryDTO.getTotalAmountDue(), totalExternalCharge);
		}

		// Extract payment information
		HashMap<Integer, IPayment> paxPayments = new HashMap<Integer, IPayment>();
		
		if ((airBookModRQ.getFulfillment() == null || airBookModRQ.getFulfillment().getPaymentDetails() == null)
				&& totalAmountDue.compareTo(BigDecimal.ZERO) == 1) {

			if (!AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_OVERRIDE_ONHOLD)) {
				boolean isHoldAllowed = WSReservationUtil.isHoldAllowed(fareSeatsAndSegDTOCol);
				if (!isHoldAllowed) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
							"Hold not allowed. Need to be ticketed immediately");
				}
			}
			// Add segment without payment
			if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(res.getStatus())) {
				// Force confirm
				// TODO test check privilege
				WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_NO_PAY);
				intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED;
			} else {// OHD or CNX
				// Retain the OHD status
				// TODO test check privilege
				WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_NO_PAY);

				intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS;

				// calculate and set new release timestamp
				// Date newReleaseTimestamp = ReservationUtil.getHoldRelTimeForAddOrModOND(res, fareSeatsAndSegDTOCol);
				Collection colFlightSegmentDTO = WebplatformUtil.getConfirmedDepartureSegments(res.getSegmentsView());
				Date newReleaseTimestamp = WebplatformUtil.getReleaseTimeStamp(colFlightSegmentDTO, privilegeKeys, true, null,
						AppIndicatorEnum.APP_WS.toString(), new OnHoldReleaseTimeDTO());
				addedResSegs.overrideZuluReleaseTimeStamp(newReleaseTimestamp);
			}
		} else {
			paxPayments = getPaxPaymentsForModifyOnd(res, otaAirBookModifyRQ.getAirBookModifyRQ().getFulfillment(),
					otaAirBookModifyRQ.getAirBookModifyRQ().getTravelersFulfillments(), pnrSegIdsForMod, totalAmountDue,
					resSummaryDTO, newSegmetsCount);

			for (Object element : paxPayments.keySet()) {
				Integer pnrPaxId = (Integer) element;
				addedResSegs.addPassengerPayments(pnrPaxId, paxPayments.get(pnrPaxId));
			}
		}

		// Add zero payments for passengers not having payment
		for (Object element : res.getPassengers()) {
			ReservationPax pnrPax = (ReservationPax) element;

			// Payment should always sent for the adults/children only
			if (!ReservationApiUtils.isInfantType(pnrPax)) {
				if (!paxPayments.keySet().contains(pnrPax.getPnrPaxId())) {
					IPayment payment = new PaymentAssembler();
					// add the new external charges to the passenger.
					WSFlexiFareUtil.validateAddFlexiForModification(res, pnrSegIdsForMod);
					BookUtil.addSelectedExternalCharges(payment, CommonServicesUtil.getTravelerRPH(pnrPax, false));
					addedResSegs.addPassengerPayments(pnrPax.getPnrPaxId(), payment);
				}
			}
		}

		// Block seats quoted
		Collection blockedSeatsCol = WebServicesModuleUtils.getReservationBD().blockSeats(fareSeatsAndSegDTOCol, null);
		CustomChargesTO customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
		WebServicesModuleUtils.getResSegmentBD().changeSegments(res.getPnr(), pnrSegIdsForMod, addedResSegs, customChargesTO,
				blockedSeatsCol, intPaymentMode, res.getVersion(),
				getTrackInfo(ThreadLocalData.getCurrentUserPrincipal()), false, false, false, false, false, true);
	}

	/**
	 * Prepare per pax payment details for modify ond.
	 * 
	 * @param res
	 * @param fulfillment
	 * @param travelersFulfillments
	 * @param amountDueWOCC
	 *            TODO
	 * @param balances
	 *            TODO
	 * @param newSegCount
	 *            TODO
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	private HashMap<Integer, IPayment> getPaxPaymentsForModifyOnd(Reservation res, Fulfillment fulfillment,
			TravelersFulfillments travelersFulfillments, List<Integer> modifyingPnrSegIds, BigDecimal amountDueWOCC,
			ResBalancesSummaryDTO balances, int newSegCount) throws WebservicesException, ModuleException {
		HashMap<Integer, IPayment> paxPayments = new HashMap<Integer, IPayment>();

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		Map<Integer, PAXSummeryDTO> paxSummaryMap = new HashMap<Integer, PAXSummeryDTO>();
		Collection<PAXSummeryDTO> paxSummaries = balances.getPaxSummaryDTOCol();

		for (PAXSummeryDTO summary : paxSummaries) {
			paxSummaryMap.put(summary.getPNRPaxId(), summary);
		}

		// TODO: Optimize code and remove duplicates
		if (fulfillment != null && fulfillment.getPaymentDetails() != null) {
			// Individual pax payment not provided; Full outstanding payment is expected
			if (travelersFulfillments == null) {
				PaymentAssembler specifiedPayment = new PaymentAssembler();
				UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
				OTAUtils.extractReservationPayments(fulfillment.getPaymentDetails().getPaymentDetail(), specifiedPayment,
						principal.getAgentCurrencyCode(), exchangeRateProxy);

				BigDecimal calculatedPaymentAmount = amountDueWOCC;

				ReservationPax infantPax = null;
				if (specifiedPayment.getPayments().size() == 1) {
					// FIXME support multiple payment options in one request
					Object resPayment = specifiedPayment.getPayments().iterator().next();
					if (resPayment instanceof AgentCreditInfo) {

						if (calculatedPaymentAmount.doubleValue() != specifiedPayment.getTotalPayAmount().doubleValue()) {
							// Specified payment does not match expected
							// payments
							throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_PAYMENT_AMOUNT,
									"Expected [" + calculatedPaymentAmount + "] received [" + specifiedPayment.getTotalPayAmount()
											+ "]");
						}

						Collection<ReservationPax> paxes = res.getPassengers();
						for (ReservationPax pax : paxes) {
							BigDecimal expectedPaxPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
							PAXSummeryDTO paxSummary = paxSummaryMap.get(pax.getPnrPaxId());

							// If it's an adult
							if (ReservationApiUtils.isAdultType(pax)) {

								expectedPaxPaymentAmount = AccelAeroCalculator.add(expectedPaxPaymentAmount,
										paxSummary.getNewBalance());
								if (pax.getAccompaniedPaxId() != null) {// Parent pax
									infantPax = (ReservationPax) pax.getInfants().iterator().next();
									expectedPaxPaymentAmount = AccelAeroCalculator.add(expectedPaxPaymentAmount,
											paxSummaryMap.get(infantPax.getPnrPaxId()).getNewBalance());
								}

								IPayment paxPayment = new PaymentAssembler();
								String agentCode = ((AgentCreditInfo) resPayment).getAgentCode();
								String wsPaymentMethod = ((((AgentCreditInfo) resPayment).getPaymentReferenceTO()
										.getPaymentRefType()) == PAYMENT_REF_TYPE.BSP) ? Agent.PAYMENT_MODE_BSP : null;
								WSFlexiFareUtil.validateAddFlexiForModification(res, modifyingPnrSegIds);
								BookUtil.addSelectedExternalCharges(paxPayment, CommonServicesUtil.getTravelerRPH(pax, false));
								ServiceTaxCalculator.calculatePaxJNTax(paxPayment);

								paxPayment.addAgentCreditPayment(agentCode, expectedPaxPaymentAmount, null,
										WSReservationUtil.getPayCurrencyDTO(agentCode, exchangeRateProxy), null, wsPaymentMethod,
										null);

								paxPayments.put(pax.getPnrPaxId(), paxPayment);
								// If it's a child
							} else if (ReservationApiUtils.isChildType(pax)) {
								expectedPaxPaymentAmount = AccelAeroCalculator.add(expectedPaxPaymentAmount,
										paxSummary.getNewBalance());
								IPayment paxPayment = new PaymentAssembler();

								String agentCode = ((AgentCreditInfo) resPayment).getAgentCode();
								String wsPaymentMethod = ((((AgentCreditInfo) resPayment).getPaymentReferenceTO()
										.getPaymentRefType()) == PAYMENT_REF_TYPE.BSP) ? Agent.PAYMENT_MODE_BSP : null;
								WSFlexiFareUtil.validateAddFlexiForModification(res, modifyingPnrSegIds);
								BookUtil.addSelectedExternalCharges(paxPayment, CommonServicesUtil.getTravelerRPH(pax, false));
								ServiceTaxCalculator.calculatePaxJNTax(paxPayment);

								paxPayment.addAgentCreditPayment(agentCode, expectedPaxPaymentAmount, null,
										WSReservationUtil.getPayCurrencyDTO(agentCode, exchangeRateProxy), null, wsPaymentMethod,
										null);

								paxPayments.put(pax.getPnrPaxId(), paxPayment);
							}
						}
					} else if (resPayment instanceof CardPaymentInfo) {

						int totalAdultChildCount = res.getTotalPaxAdultCount() + res.getTotalPaxChildCount();
						ExternalChgDTO ccCharge = WSReservationUtil.getCCChargeAmount(calculatedPaymentAmount,
								totalAdultChildCount, newSegCount, ChargeRateOperationType.MAKE_ONLY);
						BigDecimal ccFee = ccCharge.getAmount();
						ccFee = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.CREDIT_CARD, ccFee);

						calculatedPaymentAmount = AccelAeroCalculator.add(calculatedPaymentAmount, ccFee);
						if (calculatedPaymentAmount.doubleValue() != specifiedPayment.getTotalPayAmount().doubleValue()) {
							// Specified payment does not match expected
							// payments
							throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_PAYMENT_AMOUNT,
									"Expected [" + calculatedPaymentAmount + "] received [" + specifiedPayment.getTotalPayAmount()
											+ "]");
						}

						Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();
						extChgMap.put(ccCharge.getExternalChargesEnum(), ccCharge);

						ExternalChargesMediator chgMediator = new ExternalChargesMediator(null, extChgMap, true, true);
						chgMediator.setCalculateJNTaxForCCCharge(true);
						LinkedList perPaxChgs = chgMediator.getExternalChargesForABalancePayment(totalAdultChildCount, 0);

						CardPaymentInfo resCardPayment = (CardPaymentInfo) resPayment;
						Collection<ReservationPax> paxes = res.getPassengers();
						for (ReservationPax pax : paxes) {

							BigDecimal expectedPaxPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
							PAXSummeryDTO paxSummary = paxSummaryMap.get(pax.getPnrPaxId());
							// If it's an adult
							if (ReservationApiUtils.isAdultType(pax)) {
								expectedPaxPaymentAmount = AccelAeroCalculator.add(expectedPaxPaymentAmount,
										paxSummary.getNewBalance());
								if (pax.getAccompaniedPaxId() != null) {// Parent pax
									// one and only one infant per parent exists
									infantPax = (ReservationPax) pax.getInfants().iterator().next();
									// Add infant charges
									expectedPaxPaymentAmount = AccelAeroCalculator.add(expectedPaxPaymentAmount,
											paxSummaryMap.get(infantPax.getPnrPaxId()).getNewBalance());
								}

								IPayment paxPayment = new PaymentAssembler();
								// add the external charges to the payments. we don't need to add to the per pax-
								// totals as it's added in the payment assembler
								WSFlexiFareUtil.validateAddFlexiForModification(res, modifyingPnrSegIds);
								Object extCharges = null;
								if (perPaxChgs.size() > 0) {
									extCharges = perPaxChgs.pop();
								}
								if (extCharges != null) {
									paxPayment.addExternalCharges((Collection<ExternalChgDTO>) extCharges);
								}
								BookUtil.addSelectedExternalCharges(paxPayment, CommonServicesUtil.getTravelerRPH(pax, false));
								ServiceTaxCalculator.calculatePaxJNTax(paxPayment);

								paxPayment.addCardPayment(resCardPayment.getType(), resCardPayment.getEDate(),
										resCardPayment.getNo(), resCardPayment.getName(), resCardPayment.getAddress(),
										resCardPayment.getSecurityCode(), expectedPaxPaymentAmount,
										resCardPayment.getAppIndicator(), resCardPayment.getTnxMode(), null,
										resCardPayment.getIpgIdentificationParamsDTO(), null, resCardPayment.getPayCurrencyDTO(),
										null, null, null, null);
								paxPayments.put(pax.getPnrPaxId(), paxPayment);
								// If it's a child
							} else if (ReservationApiUtils.isChildType(pax)) {
								expectedPaxPaymentAmount = AccelAeroCalculator.add(expectedPaxPaymentAmount,
										paxSummary.getNewBalance());
								IPayment paxPayment = new PaymentAssembler();
								// add the external charges to the payments. we don't need to add to the per pax-
								// totals as it's added in the payment assembler.
								WSFlexiFareUtil.validateAddFlexiForModification(res, modifyingPnrSegIds);
								Object extCharges = perPaxChgs.pop();
								if (extCharges != null) {
									paxPayment.addExternalCharges((Collection<ExternalChgDTO>) extCharges);
								}
								BookUtil.addSelectedExternalCharges(paxPayment, CommonServicesUtil.getTravelerRPH(pax, false));
								ServiceTaxCalculator.calculatePaxJNTax(paxPayment);

								paxPayment.addCardPayment(resCardPayment.getType(), resCardPayment.getEDate(),
										resCardPayment.getNo(), resCardPayment.getName(), resCardPayment.getAddress(),
										resCardPayment.getSecurityCode(), expectedPaxPaymentAmount,
										resCardPayment.getAppIndicator(), resCardPayment.getTnxMode(), null,
										resCardPayment.getIpgIdentificationParamsDTO(), null, resCardPayment.getPayCurrencyDTO(),
										null, null, null, null);

								paxPayments.put(pax.getPnrPaxId(), paxPayment);
							}
						}

					} else {
						// TODO support other payment types (cash, etc.)
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
								"Only agent onaccount & cc payment supported");
					}
				} else {
					// TODO support multiple payments
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
							"Only single payment type in one request supported");
				}
			} else {
				// TODO Individual pax payments are provided; Handle partial payment scenarios
			}
		} else {

			WSFlexiFareUtil.validateAddFlexiForModification(res, modifyingPnrSegIds);
			Collection<ReservationPax> paxes = res.getPassengers();
			for (ReservationPax pax : paxes) {
				IPayment paxPayment = new PaymentAssembler();
				if (!ReservationApiUtils.isInfantType(pax)) {
					paxPayments.put(pax.getPnrPaxId(), paxPayment);
				}
			}
		}

		return paxPayments;
	}

	/**
	 * Prepare per pax payment details for add ond.
	 * 
	 * @param res
	 * @param fulfillment
	 * @param travelersFulfillments
	 * @param fareSeatsAndSegDTOCol
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	private HashMap<Integer, IPayment> getPaxPaymentsForAddOnd(Reservation res, Fulfillment fulfillment,
			TravelersFulfillments travelersFulfillments, Collection<OndFareDTO> fareSeatsAndSegDTOCol)
			throws WebservicesException, ModuleException {
		HashMap<Integer, IPayment> paxPayments = new HashMap<Integer, IPayment>();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		if (fulfillment != null && fulfillment.getPaymentDetails() != null) {
			if (travelersFulfillments == null) {// Individual pax payment not provided; Full outstanding
												// payment is expected
				BigDecimal[] totalNewPrices = ReservationUtil.getQuotedTotalPrices(fareSeatsAndSegDTOCol,
						res.getTotalPaxAdultCount(), res.getTotalPaxChildCount(), res.getTotalPaxInfantCount());
				PaymentAssembler specifiedPayment = new PaymentAssembler();
				UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
				OTAUtils.extractReservationPayments(fulfillment.getPaymentDetails().getPaymentDetail(), specifiedPayment,
						principal.getAgentCurrencyCode(), exchangeRateProxy);

				BigDecimal calculatedPaymentAmount = AccelAeroCalculator.add(res.getTotalAvailableBalance(), totalNewPrices[0]);

				// TODO: Multi-currency support is not fully integrated in
				// modification flow
				// Hence setting base currency in calculating external charges
				// for the time being

				calculatedPaymentAmount = AccelAeroCalculator.add(calculatedPaymentAmount,
						BookUtil.getTotalAmountOfSelectedExternalCharges(AppSysParamsUtil.getBaseCurrency(), exchangeRateProxy));
				if (calculatedPaymentAmount.doubleValue() != specifiedPayment.getTotalPayAmount().doubleValue()) {
					// Specified payment does not match expected payments
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_PAYMENT_AMOUNT,
							"Expected [" + calculatedPaymentAmount + "] received [" + specifiedPayment.getTotalPayAmount() + "]");
				}

				BigDecimal expectedPaxPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
				ReservationPax infantPax = null;
				if (specifiedPayment.getPayments().size() == 1) {// FIXME support multiple payment options in one
																	// request

					Object resPayment = specifiedPayment.getPayments().iterator().next();
					if (resPayment instanceof AgentCreditInfo) {
						Collection<ReservationPax> paxes = res.getPassengers();
						for (ReservationPax pax : paxes) {
							// If it's an adult
							if (ReservationApiUtils.isAdultType(pax)) {
								expectedPaxPaymentAmount = AccelAeroCalculator.add(pax.getTotalAvailableBalance(),
										totalNewPrices[1]);
								if (pax.getAccompaniedPaxId() != null) {// Parent pax
									infantPax = pax.getInfants().iterator().next();// one and only one
																					// infant per parent
																					// exists
									expectedPaxPaymentAmount = AccelAeroCalculator.add(expectedPaxPaymentAmount,
											infantPax.getTotalAvailableBalance(), totalNewPrices[2]);
								}

								IPayment paxPayment = new PaymentAssembler();
								String agentCode = ((AgentCreditInfo) resPayment).getAgentCode();
								String wsPaymentMethod = ((((AgentCreditInfo) resPayment).getPaymentReferenceTO()
										.getPaymentRefType()) == PAYMENT_REF_TYPE.BSP) ? Agent.PAYMENT_MODE_BSP : null;
								// add the external charges to the payments. -
								// we don't need to add to the per pax
								// totals
								// as it's added in the payment assembler.
								WSFlexiFareUtil.validateAddFlexiForModification(res, null);
								BookUtil.addSelectedExternalCharges(paxPayment, CommonServicesUtil.getTravelerRPH(pax, false));
								ServiceTaxCalculator.calculatePaxJNTax(paxPayment);
								paxPayment.addAgentCreditPayment(agentCode, expectedPaxPaymentAmount, null,
										WSReservationUtil.getPayCurrencyDTO(agentCode, exchangeRateProxy), null, wsPaymentMethod,
										null);

								paxPayments.put(pax.getPnrPaxId(), paxPayment);
								// If it's a child
							} else if (ReservationApiUtils.isChildType(pax)) {
								expectedPaxPaymentAmount = AccelAeroCalculator.add(pax.getTotalAvailableBalance(),
										totalNewPrices[3]);
								IPayment paxPayment = new PaymentAssembler();

								String agentCode = ((AgentCreditInfo) resPayment).getAgentCode();
								String wsPaymentMethod = ((((AgentCreditInfo) resPayment).getPaymentReferenceTO()
										.getPaymentRefType()) == PAYMENT_REF_TYPE.BSP) ? Agent.PAYMENT_MODE_BSP : null;
								// add the external charges to the payments. -
								// we don't need to add to the per pax
								// totals
								// as it's added in the payment assembler.
								WSFlexiFareUtil.validateAddFlexiForModification(res, null);
								BookUtil.addSelectedExternalCharges(paxPayment, CommonServicesUtil.getTravelerRPH(pax, false));
								ServiceTaxCalculator.calculatePaxJNTax(paxPayment);

								paxPayment.addAgentCreditPayment(agentCode, expectedPaxPaymentAmount, null,
										WSReservationUtil.getPayCurrencyDTO(agentCode, exchangeRateProxy), null, wsPaymentMethod,
										null);

								paxPayments.put(pax.getPnrPaxId(), paxPayment);
							}
						}
					} else {
						// TODO support other payment types (cash, etc.)
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
								"Only agent onaccount payment supported & cc payment supported");
					}
				} else {
					// TODO support multiple payments
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
							"Only single payment type in one request supported");
				}
			} else {
				// TODO Individual pax payments are provided; Handle partial
				// payment scenarios
			}
		}

		return paxPayments;
	}

	/**
	 * Returns fare quote data done in current transaction.
	 * 
	 * @param res
	 *            TODO
	 * 
	 * @return Collection<OndFareDTO>
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	private static Collection<OndFareDTO> getQuotedFareSeatsAndSegDTOCol(OTAAirBookModifyRQ otaAirBookModifyRQ, Reservation res,
			Collection pnrSegIdsForMod) throws WebservicesException, ModuleException {
		/*
		 * Collection<String> selectedPricingKeys = OTAUtils.getSelectedPricingKeys(airItinerary); return
		 * PriceQuoteUtil.getSelectedSegsFaresSeatsColForBooking(selectedPricingKeys);
		 */
		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();
		PriceQuoteReqParamsExtractUtill priceQuoteReqParamsExtractUtill = new PriceQuoteReqParamsExtractUtill(userPrincipal);

		AirItineraryType itineraryType = otaAirBookModifyRQ.getAirBookModifyRQ().getAirItinerary();
		// pax counts
		int adults = 0;
		int children = 0;
		int infants = 0;

		ReservationPax reservationPax = null;
		for (Object element : res.getPassengers()) {
			reservationPax = (ReservationPax) element;
			if (reservationPax.getPaxType().equals(ReservationInternalConstants.PassengerType.ADULT)) {
				adults++;
			}
			if (reservationPax.getPaxType().equals(ReservationInternalConstants.PassengerType.CHILD)) {
				children++;
			}
			if (reservationPax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {
				infants++;
			}
		}

		Collection<AvailableFlightSearchDTO> availableFlightSearchDTOs = priceQuoteReqParamsExtractUtill
				.extractPriceQuoteReqParams(itineraryType, new int[] { adults, infants, children });
		if (pnrSegIdsForMod != null) {
			for (AvailableFlightSearchDTO flightSearchDTO : availableFlightSearchDTOs) {
				ReservationUtil.setExistingSegInfo(res, flightSearchDTO, pnrSegIdsForMod);
			}
		}
		return priceQuoteReqParamsExtractUtill
				.getSelectedSegsFaresSeatsColForBookingFromFareSegChargeTO(availableFlightSearchDTOs);
	}

	/**
	 * Extend onhold reservation.
	 * 
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void extendOnholdReservation(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt) throws ModuleException, WebservicesException {

		ReservationPax reservationPax = null;
		Date releaseTimeStamp = null;
		Date newReleaseTimeStamp = null;
		long lDiff = 0;

		if ((!ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus()))
				|| (otaAirBookModifyRQ.getAirBookModifyRQ().getTicketing().get(0).getTicketTimeLimit() == null)) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_MODIFICATION_DATA_IS_INVALID_,
					"Reservation is not onhold or the extend onhold timestamp is invalid");
		} else {

			Iterator itReservationPax = reservation.getPassengers().iterator();
			while (itReservationPax.hasNext()) {
				reservationPax = (ReservationPax) itReservationPax.next();
				releaseTimeStamp = reservationPax.getZuluReleaseTimeStamp();
				if (releaseTimeStamp != null) {
					break;
				}
			}

			Calendar cal = new GregorianCalendar();
			cal.setTimeInMillis(
					CommonUtil.getTime(otaAirBookModifyRQ.getAirBookModifyRQ().getTicketing().get(0).getTicketTimeLimit()));
			newReleaseTimeStamp = cal.getTime();

			if (newReleaseTimeStamp.before(CalendarUtil.getCurrentSystemTimeInZulu())) {// Release time cannot be a past
																						// time
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_IS_NOT_ELIGIBLE_FOR_HOLD,
						"Reservation can not extend onhold for expired segment(s)");
			}

			Collection colFlightSegmentDTO = WebplatformUtil.getConfirmedDepartureSegments(reservation.getSegmentsView());
			Date maxAllowedReleaseTimeZulu = WebplatformUtil.getMaxAllowedReleaseTimeZulu(colFlightSegmentDTO);

			if (maxAllowedReleaseTimeZulu == null) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_IS_NOT_ELIGIBLE_FOR_HOLD,
						"Reservation can not extend onhold for expired segment(s)");
			}

			if (newReleaseTimeStamp.after(maxAllowedReleaseTimeZulu)) {
				newReleaseTimeStamp = maxAllowedReleaseTimeZulu;// Release time is allowed up to earliest of flight
																// closure and interline cut-over
			}

			try {
				lDiff = BeanUtils.getIdealReleaseDate(newReleaseTimeStamp, releaseTimeStamp);
			} catch (ParseException e) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_MODIFICATION_DATA_IS_INVALID_,
						"Reservation is not onhold or the extend onhold timestamp is invalid");
			}

			int intDiff = Math.round(lDiff / (60 * 1000));

			WebServicesModuleUtils.getReservationBD().extendOnholdReservation(reservation.getPnr(), intDiff,
					reservation.getVersion(), null);
		}
	}

	/**
	 * Transfer Booking.
	 * 
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void transferBooking(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt) throws ModuleException, WebservicesException {
		AAAdminInfoType adminInfoType = aaAirBookModifyRQExt.getAdminInfo();
		if ((adminInfoType == null) || (adminInfoType.getOwnerAgentCode() == null)) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Agent Code required");
		}
		String ownerAgentCode = adminInfoType.getOwnerAgentCode();

		WebServicesModuleUtils.getReservationBD().transferOwnerShip(reservation.getPnr(), ownerAgentCode,
				reservation.getVersion(), null);
	}

	/**
	 * Add Infant process
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 */
	public void addInfant(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt) throws ModuleException, WebservicesException {

		// pos station
		String posStation = otaAirBookModifyRQ.getPOS().getSource().get(0).getAirportCode();

		// agent code
		String agentCode = null;

		if (otaAirBookModifyRQ.getPOS().getSource().get(0).getBookingChannel() != null
				&& otaAirBookModifyRQ.getPOS().getSource().get(0).getBookingChannel().getCompanyName() != null) {
			agentCode = otaAirBookModifyRQ.getPOS().getSource().get(0).getBookingChannel().getCompanyName().getCode();
		}

		// pnr
		String pnr = otaAirBookModifyRQ.getAirBookModifyRQ().getBookingReferenceID().get(0).getID();

		// user id
		String userId = otaAirBookModifyRQ.getPOS().getSource().get(0).getERSPUserID();

		// num.of.infants...
		int noOfInfants = OTAUtils.getInfantCountToModify(otaAirBookModifyRQ);

		// get the travellers
		Collection<AirTravelerType> travellers = otaAirBookModifyRQ.getAirBookModifyRQ().getTravelerInfo().getAirTraveler();

		// for onhold or confirmed boooking, leave the same.
		Integer intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS;

		// get theh fares and assemble the passenger interface
		ReservationMediator reservationMediator = new ReservationMediator(reservation);
		Collection inventoryFares = WebServicesModuleUtils.getFlightInventoryResBD()
				.getInfantQuote(reservationMediator.getOndFareDTOs(noOfInfants), posStation, true, agentCode);
		IPassenger iPassenger = new PassengerAssembler(inventoryFares);

		// iterate the Ota to-moodify pax and add iinfant to the assembled passenger interface
		for (AirTravelerType airTravelerType : travellers) {

			if (airTravelerType.getPassengerTypeCode()
					.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
				int[] infSeq_ParentSeq_ParentPnrPaxID = OTAUtils.getInfSeqParentSeqAndParentPnrPaxIDForAddInfant(airTravelerType);
				int infSeq = infSeq_ParentSeq_ParentPnrPaxID[0];
				int parentSeq = infSeq_ParentSeq_ParentPnrPaxID[1];
				int pnrPaxID = infSeq_ParentSeq_ParentPnrPaxID[2];

				iPassenger.addInfant(CommonUtil.getValueFromNullSafeList(airTravelerType.getPersonName().getGivenName(), 0),
						airTravelerType.getPersonName().getSurname(), null,
						airTravelerType.getBirthDate() != null
								? airTravelerType.getBirthDate().toGregorianCalendar().getTime()
								: null,
						OTAUtils.getNationalityCode(airTravelerType), infSeq, parentSeq, null, null, null, null, null, null, null,
						null, null, null, null, "", "", "", "", null);

				// blank payment
				IPayment iPaymentPax = new PaymentAssembler();

				iPassenger.addPassengerPayments(new Integer(pnrPaxID), iPaymentPax);

			}

		}

		// seat block
		Collection blockKeyIds = WebServicesModuleUtils.getReservationBD()
				.blockSeats(reservationMediator.getOndFareDTOs(noOfInfants), null);
		// add the infant.
		WebServicesModuleUtils.getPassengerBD().addInfant(pnr, iPassenger, blockKeyIds, intPaymentMode, reservation.getVersion(),
				null, getTrackInfo(ThreadLocalData.getCurrentUserPrincipal()), false, false, false, null);

	}

	/**
	 * Passenger Refund
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public void passengerRefund(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt) throws ModuleException, WebservicesException {

		// TODO - Revise / Test
		IPassenger iPassenger = new PassengerAssembler(null);
		IPayment iPayment = null;
		Integer pnrPaxId = null;
		String userNotes = null;

		PaymentDetails paymentDetails = otaAirBookModifyRQ.getAirBookModifyRQ().getFulfillment().getPaymentDetails();
		for (PaymentDetailType paymentDetailType : paymentDetails.getPaymentDetail()) {
			pnrPaxId = getPnrPaxIdFromRPH(paymentDetailType.getRPH());
			iPayment = validateAndGetPaymentInfoForRefund(paymentDetailType);
			if ((pnrPaxId == null) || (iPayment == null)) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Payment details required");
			}
			iPassenger.addPassengerPayments(pnrPaxId, iPayment);
		}

		if ((aaAirBookModifyRQExt != null) && (aaAirBookModifyRQExt.getUserNotes() != null)) {
			userNotes = ExtensionsUtil.transformToAAUserNote(aaAirBookModifyRQExt.getUserNotes());
		}

		WebServicesModuleUtils.getPassengerBD().passengerRefund(reservation.getPnr(), iPassenger, userNotes, true,
				reservation.getVersion(), null, false, null, null, false);
	}

	/**
	 * Checks if the DTO Sent is proper and extracts the necessary info to IPayment.
	 * 
	 * @param paymentDetailType
	 * @return
	 * @throws WebservicesException
	 */
	private IPayment validateAndGetPaymentInfoForRefund(PaymentDetailType paymentDetailType) throws WebservicesException {

		// if paymentDetailType is null?
		if (paymentDetailType == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Payment details not set]");
		}

		// Hold the payment type with the amount.
		IPayment payment = new PaymentAssembler();
		// Get the paymentMethod type as a constant.
		String paymentMethodConstant = validateAndGetPaymentMethod(paymentDetailType);

		// If the payment method was undecided
		if (paymentMethodConstant == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Payment type not set]");
		}

		// Get the payment amount
		BigDecimal paymentAmount = paymentDetailType.getPaymentAmount() == null
				? null
				: paymentDetailType.getPaymentAmount().getAmount();

		// if payment amount was not set
		if (paymentAmount == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Payment amount not set]");
		}

		// Add payment amount
		if (paymentMethodConstant.equals(WebservicesConstants.PaymentTypes.CASH)) {
			// TODO Support pay currency refund operations
			payment.addCashPayment(paymentAmount, null, null, null, null, null, null);
		}

		// Add on account amount
		if (paymentMethodConstant.equals(WebservicesConstants.PaymentTypes.ON_ACCOUNT)) {
			// TODO Support pay currency refund operations
			payment.addAgentCreditPayment(paymentDetailType.getDirectBill().getDirectBillID(), paymentAmount, null, null, null,
					null, null, null, null);
		}

		// Add credit card amount
		if (paymentMethodConstant.equals(WebservicesConstants.PaymentTypes.CREDIT_CARD)) {
			// TODO Support pay currency refund operations
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Not Supported]");

			/*
			 * PaymentCardType paymentCardtype = paymentDetailType.getPaymentCard(); String cardType =
			 * paymentCardtype.getCardType(); String cardCode = paymentCardtype.getCardCode(); String cardNumber =
			 * paymentCardtype.getCardNumber(); String cardHolderName=paymentCardtype.getCardHolderName(); String
			 * address = getAddress(paymentCardtype.getAddress()); String cvvCode=paymentCardtype.getSeriesCode();
			 * String effectiveDate=paymentCardtype.getEffectiveDate();
			 * 
			 * payment.addCardPayment(Integer.valueOf(cardType).intValue(), effectiveDate, cardNumber, cardHolderName,
			 * address, cvvCode, paymentAmount, AppIndicatorEnum.APP_XBE, TnxModeEnum.MAIL_TP_ORDER, new Integer(1),
			 * WebServicesModuleUtils.getDefaultPaymentIdentificationParamsDTO());
			 */
		}
		return payment;
	}

	/**
	 * Construct the Address to one String.
	 * 
	 * @param addressType
	 * @return
	 * @throws WebservicesException
	 */
	private String getAddress(AddressType addressType) throws WebservicesException {

		String address = "";
		try {
			address = CommonUtil.nullHandler(addressType.getPostalCode());

			for (String line : addressType.getAddressLine()) {
				address = address.trim() + " " + line;
			}

			address = address.trim() + " " + addressType.getCityName();
			address = address.trim() + addressType.getCounty();
		} catch (Exception ex) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, ex.getMessage());
		}

		return address.trim();
	}

	/**
	 * Will check the DTO Structor and will return a constant string of the payment type.
	 * 
	 * @param paymentDetailType
	 * @return
	 */
	private String validateAndGetPaymentMethod(PaymentDetailType paymentDetailType) {

		// initialize returning constant value.
		String constant = null;
		// check if null
		if (paymentDetailType == null) {
			return null;
		}

		// if cash and no card info and no direct bill info then its cash payment
		if (paymentDetailType.getCash().isCashIndicator() && paymentDetailType.getPaymentCard().getCardHolderRPH() == null
				&& paymentDetailType.getDirectBill().getDirectBillID() == null) {
			constant = WebservicesConstants.PaymentTypes.CASH;
		}

		// if Credit Card and no cash and no direct bill info then its Credit Card Payment
		if (paymentDetailType.getPaymentCard().getCardHolderRPH() != null
				&& paymentDetailType.getCash().isCashIndicator() == false
				&& paymentDetailType.getDirectBill().getDirectBillID() == null) {
			constant = WebservicesConstants.PaymentTypes.CREDIT_CARD;
		}
		// if Direct Bill Info and no credi card info and no cash info then its onaccount type
		if (paymentDetailType.getDirectBill().getDirectBillID() != null && paymentDetailType.getCash().isCashIndicator() == false
				&& paymentDetailType.getPaymentCard().getCardHolderRPH() == null) {
			constant = WebservicesConstants.PaymentTypes.ON_ACCOUNT;
		}

		return constant;
	}

	/**
	 * @author Byorn
	 * @param rphValue
	 * @return
	 * @throws WebservicesException
	 */
	private Integer getPnrPaxIdFromRPH(String rphValue) throws WebservicesException {
		Integer pnrNumber = null;
		if (rphValue != null) {
			try {
				String pnrString = rphValue.substring(rphValue.indexOf("$"));
				pnrNumber = new Integer(pnrString);
				return pnrNumber;
			} catch (Exception ex) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
						"[Travel RPH " + rphValue + " or Booking Reference is invalid");
			}
		}
		return pnrNumber;
	}

	/**
	 * Method to update the passenger ssr details.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public static void updateSSR(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt) throws ModuleException, WebservicesException {

		setSSRDetails(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);
		WebServicesModuleUtils.getReservationBD().updateReservation(reservation, null, false, false, null, null);
	}

	/**
	 * Method to change the passenger name details.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public static void nameChange(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt) throws ModuleException, WebservicesException {

		setNameChangeDetails(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);
		setFFIDChangeDetails(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);
		WebServicesModuleUtils.getReservationBD().updateReservation(reservation, null, false, true, null, null);
	}

	/**
	 * Method to update the reservation.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public static void updateReservation(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt) throws ModuleException, WebservicesException {

		setSSRDetails(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

		setNameChangeDetails(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

		setFFIDChangeDetails(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

		setUserNotes(reservation, otaAirBookModifyRQ, aaAirBookModifyRQExt);

		WebServicesModuleUtils.getReservationBD().updateReservation(reservation, null, false, true, null, null);
	}

	/**
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private static void setFFIDChangeDetails(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt) throws ModuleException, WebservicesException {

		ReservationPax resPax = null;
		Set<ReservationPax> setPassenger = null;
		Iterator<ReservationPax> iterPassenger = null;
		Integer pnrPaxId = null;
		String paxTypeCode = null;

		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

		for (AirTravelerType traveler : otaAirBookModifyRQ.getAirBookModifyRQ().getTravelerInfo().getAirTraveler()) {

			pnrPaxId = new Integer(OTAUtils.getPNRPaxIdFromTravellerRPH(traveler.getTravelerRefNumber().getRPH()));

			setPassenger = reservation.getPassengers();

			if (setPassenger != null) {
				iterPassenger = setPassenger.iterator();
				resPax = null;
				while (iterPassenger.hasNext()) {
					resPax = iterPassenger.next();

					if (resPax.getPnrPaxId().equals(pnrPaxId)) {
						paxTypeCode = traveler.getPassengerTypeCode();
						String paxFfid = null;
						String oldPaxFfid = null;
						// validate loyalty information
						if (AppSysParamsUtil.isLMSEnabled()) {
							if (resPax.getPaxAdditionalInfo() != null) {
								oldPaxFfid = resPax.getPaxAdditionalInfo().getFfid();
							}

							if (!traveler.getCustLoyalty().isEmpty()) {
								List<CustLoyalty> custLoyalty = traveler.getCustLoyalty();
								paxFfid = custLoyalty.iterator().next().getMembershipID();
							}

							if (oldPaxFfid != null) {
								if (!oldPaxFfid.equals(paxFfid)) {
									WSReservationUtil.authorize(privilegeKeys, null,
											PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_EDIT_FFID);
								}
							}
							// Pax validation
							if (paxFfid != null) {
								if (paxTypeCode.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))
										|| paxTypeCode.equals(
												CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {

									OTAUtils.validateLoyaltyInformation(paxFfid,
											CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0),
											traveler.getPersonName().getSurname());

								} else if (paxTypeCode
										.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
									paxFfid = null;
								}
							}
							ReservationPaxAdditionalInfo addnInfo = null;
							if (resPax.getPaxAdditionalInfo() == null) {
								addnInfo = new ReservationPaxAdditionalInfo();
							} else {
								addnInfo = resPax.getPaxAdditionalInfo();
							}

							addnInfo.setFfid(paxFfid);
							resPax.setPaxAdditionalInfo(addnInfo);

							break;

						}
					}
				}
			}
		}
	}

	/**
	 * Method to set the updated passenger ssr details.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private static void setSSRDetails(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt) throws ModuleException, WebservicesException {

		Set setPassenger = null;
		Iterator iterPassenger = null;
		ReservationPax resPax = null;
		Integer pnrPaxId = null;
		String ssrCode = null;
		String ssrRemarks = null;
		boolean isSSRValid = false;

		List<SpecialReqDetailsType> specialRequestDetailType = otaAirBookModifyRQ.getAirBookModifyRQ().getTravelerInfo()
				.getSpecialReqDetails();

		for (SpecialReqDetailsType specialReqType : specialRequestDetailType) {
			for (SpecialServiceRequest specialReq : specialReqType.getSpecialServiceRequests().getSpecialServiceRequest()) {

				isSSRValid = false;

				ssrCode = specialReq.getSSRCode();
				if ((ssrCode != null) && (!ssrCode.trim().equals(""))) {
					isSSRValid = WebServicesModuleUtils.getWebServiceDAO().isValidSSR(ssrCode);
				}

				if (!isSSRValid) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
							"SSR code " + ssrCode + " is invalid");
				}

				Integer ssrId = SSRUtil.getSSRId(ssrCode);

				if (SSRDetailsUtil.isChargableSSR(ssrId)) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
							"SSR code [" + ssrCode + "] is a chargable SSR & not supported as non-chargable SSR.");
				}

				ssrRemarks = specialReq.getText();
				for (String travellerRPH : specialReq.getTravelerRefNumberRPHList()) {
					pnrPaxId = new Integer(OTAUtils.getPNRPaxIdFromTravellerRPH(travellerRPH));

					setPassenger = reservation.getPassengers();
					if (setPassenger != null) {
						iterPassenger = setPassenger.iterator();
						resPax = null;

						while (iterPassenger.hasNext()) {
							resPax = (ReservationPax) iterPassenger.next();
							if (resPax.getPnrPaxId().equals(pnrPaxId)) {
								SegmentSSRAssembler segmentSSRs = new SegmentSSRAssembler();

								segmentSSRs.addPaxSegmentSSR(null, SSRUtil.getSSRId(ssrCode), ssrRemarks);
								ReservationSSRUtil.updatePaxSegmentSSRs(resPax, segmentSSRs, false, null);
								break;
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Method to set the updated passenger name details.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private static void setNameChangeDetails(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt) throws ModuleException, WebservicesException {

		ReservationPax resPax = null;
		Nationality nationality = null;
		Set setPassenger = null;
		Iterator iterPassenger = null;
		Integer pnrPaxId = null;
		Integer nationalityCode = null;
		String paxTypeCode = null;
		String title = null;
		String firstName = null;
		String lastName = null;
		String foidPaxTypeCode = null;
		String foidNumber = null;
		PaxCategoryFoidTO paxCategoryFOIDTO = null;
		String nationalIDNo = null;
		List<String> airportCodes = new ArrayList<String>();

		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
		
		boolean isOverrideCountryConfigs = privilegeKeys
				.contains(PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_OVERRIDE_COUNTRY_PAX_CONFIG);
		
		if (!isOverrideCountryConfigs) {
			for (OriginDestinationOptionType ond : otaAirBookModifyRQ.getAirReservation().getAirItinerary()
					.getOriginDestinationOptions().getOriginDestinationOption()) {
				for (BookFlightSegmentType fltSeg : ond.getFlightSegment()) {
					airportCodes.add(fltSeg.getArrivalAirport().getLocationCode());
					airportCodes.add(fltSeg.getDepartureAirport().getLocationCode());
				}
			}
		}

		String bookingCategory = reservation.getBookingCategory();

		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();
		boolean skipValidation = AppSysParamsUtil.isWebServiceExtaraValidationSkipCarrier(userPrincipal.getAgentCode());
		QuotedPriceInfoTO quotedPriceInfoTO = (QuotedPriceInfoTO) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_PRICE_INFO);

		for (AirTravelerType traveler : otaAirBookModifyRQ.getAirBookModifyRQ().getTravelerInfo().getAirTraveler()) {

			pnrPaxId = new Integer(OTAUtils.getPNRPaxIdFromTravellerRPH(traveler.getTravelerRefNumber().getRPH()));

			setPassenger = reservation.getPassengers();

			if (setPassenger != null) {
				iterPassenger = setPassenger.iterator();
				resPax = null;
				nationality = null;
				String employeeId = "";
				Date dateOfJoin = null;
				String idCategory = "";

				while (iterPassenger.hasNext()) {
					resPax = (ReservationPax) iterPassenger.next();

					if (resPax.getPnrPaxId().equals(pnrPaxId)) {
						paxTypeCode = traveler.getPassengerTypeCode();

						foidNumber = null;
						if (traveler.getDocument().size() > 0) {
							for (Document document : traveler.getDocument()) {

								if (CommonsConstants.SSRCode.PSPT.toString().equals(document.getDocType())) {
									if (document.getDocHolderNationality() != null) {
										nationality = WebServicesModuleUtils.getCommonMasterBD()
												.getNationality(document.getDocHolderNationality());
									}

									if (document.getDocID() != null && document.getDocType() != null) {
										foidNumber = document.getDocID();
									}

								} else if (CommonsConstants.SSRCode.ID.toString().equals(document.getDocType())
										&& BookingCategory.HR.getCode().equals(bookingCategory)) {
									if (document.getDocID() != null && document.getDocType() != null) {
										employeeId = document.getDocID();
										dateOfJoin = document.getEffectiveDate() != null
												? document.getEffectiveDate().toGregorianCalendar().getTime()
												: null;
										idCategory = document.getDocIssueAuthority();
									}
								}

								if (nationality == null) {
									if (document.getDocHolderNationality() != null) {
										nationality = WebServicesModuleUtils.getCommonMasterBD()
												.getNationality(document.getDocHolderNationality());
									}
								}

							}
						}

						if (nationality != null) {
							nationalityCode = nationality.getNationalityCode();
						}
						boolean isDomesticSegmentExist = OTAUtils.isDomesticSegmentExists(quotedPriceInfoTO.getQuotedSegments());

						String paxFfid = null;
						String oldPaxFfid = null;
						// validate loyalty information
						if (AppSysParamsUtil.isLMSEnabled() && traveler.getCustLoyalty() != null
								&& !traveler.getCustLoyalty().isEmpty() && traveler.getCustLoyalty().get(0) != null) {
							paxFfid = traveler.getCustLoyalty().get(0).getMembershipID();
						}

						if (resPax.getPaxAdditionalInfo() != null) {
							oldPaxFfid = resPax.getPaxAdditionalInfo().getFfid();
						}

						if (oldPaxFfid != null) {
							if (!oldPaxFfid.equals(paxFfid)) {
								WSReservationUtil.authorize(privilegeKeys, null,
										PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_EDIT_FFID);
							}
						}

						// Pax validation
						if (paxTypeCode.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {

							OTAUtils.validatePaxDetails(
									CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getNameTitle(), 0),
									CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0),
									traveler.getPersonName().getSurname(), null, nationalityCode, null, foidNumber, null, null,
									PaxTypeTO.ADULT, true, skipValidation, employeeId, dateOfJoin, idCategory, bookingCategory,
									null, paxFfid, isDomesticSegmentExist, null, null, airportCodes);

						} else if (paxTypeCode
								.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {

							OTAUtils.validatePaxDetails(
									CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getNameTitle(), 0),
									CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0),
									traveler.getPersonName().getSurname(), null, nationalityCode, null, foidNumber, null, null,
									PaxTypeTO.CHILD, true, skipValidation, employeeId, dateOfJoin, idCategory, bookingCategory,
									null, paxFfid, isDomesticSegmentExist, null, null, airportCodes);

						} else if (paxTypeCode
								.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {

							OTAUtils.validatePaxDetails(null,
									CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0),
									traveler.getPersonName().getSurname(), null, nationalityCode, null, foidNumber, null, null,
									PaxTypeTO.INFANT, true, skipValidation, employeeId, dateOfJoin, idCategory, bookingCategory,
									null, paxFfid, isDomesticSegmentExist, null, null, airportCodes);
						}

						resPax.setTitle(title);
						resPax.setFirstName(CommonUtil.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0));
						resPax.setLastName(traveler.getPersonName().getSurname());
						resPax.setNationalityCode(nationalityCode);

						ReservationPaxAdditionalInfo addnInfo = null;
						if (resPax.getPaxAdditionalInfo() == null) {
							addnInfo = new ReservationPaxAdditionalInfo();
						} else {
							addnInfo = resPax.getPaxAdditionalInfo();
						}

						addnInfo.setPassportNo(foidNumber);
						addnInfo.setNationalIDNo(nationalIDNo);

						resPax.setPaxAdditionalInfo(addnInfo);

						// End Setting FOID details

						break;
					}
				}
			}
		}
	}

	/**
	 * Throws WebserviceExcetion if the user is not authorized to perform requested operation based on the time to
	 * departure/already flown constraints.
	 * 
	 * @param reservation
	 * @param bufferModPrivilege
	 * @return
	 * @throws WebservicesException
	 * 
	 *             TODO test
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public static void authorizeModification(Collection privilegeKeys, Reservation reservation, String bufferModPrivilege,
			Collection modCnxPnrSegIds) throws WebservicesException, ModuleException {

		ModifyBookingUtil.authorizeLCCReservation(reservation);

		PNRModifyPreValidationStatesTO statesTO = ReservationValidationUtils.getReservationOrSegPreValidationStates(reservation,
				modCnxPnrSegIds);
		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();

		// Interline bookings modifiable or not
		if (!statesTO.isInterlinedModifiable()) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR,
					"Interlined Reservations cannot be modified once transfered");
		}

		// Reservation / Segment cancelled or not
		if (statesTO.isCancelled()) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR, "Already cancelled");
		}

		// Confirmed Reservation / Segment having authorization to cancel
		if (!statesTO.isCancelled()
				&& !AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_CANCEL_SEGMENT)) {
			log.warn("Authorizatin failure [userId=" + principal.getUserId() + ",failedPrivilege="
					+ PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_CANCEL_SEGMENT + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR,
					"Cancelled Reservations are not authorized to modify");
		}

		// Restricted Segment
		if (statesTO.isRestricted()) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR,
					"Restricted Segments are not authorized to modify");
		}

		// Reservation / Segment allow modifying
		if (statesTO.isFlown()
				&& !AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_OLD)) {
			log.warn("Authorizatin failure [userId=" + principal.getUserId() + ",failedPrivilege="
					+ PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_OLD + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR,
					"Reservations having flown segment(s) are not authorized to modify");
		}

		if (bufferModPrivilege != null && !"".equals(bufferModPrivilege)) {
			if (statesTO.isInBufferTime() && !AuthorizationUtil.hasPrivilege(privilegeKeys, bufferModPrivilege)) {
				log.warn(
						"Authorizatin failure [userId=" + principal.getUserId() + ",failedPrivilege=" + bufferModPrivilege + "]");
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR,
						"Not allowed within modification buffer duration");
			}
		}
	}

	/**
	 * Method to set the user notes to reservation.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @param aaAirBookModifyRQExt
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private static void setUserNotes(Reservation reservation, OTAAirBookModifyRQ otaAirBookModifyRQ,
			AAAirBookModifyRQExt aaAirBookModifyRQExt) throws ModuleException, WebservicesException {
		String userNote = "";

		if ((aaAirBookModifyRQExt != null) && (aaAirBookModifyRQExt.getUserNotes() != null)) {
			userNote = ExtensionsUtil.transformToAAUserNote(aaAirBookModifyRQExt.getUserNotes());
		}

		if (!userNote.trim().equals("")) {
			reservation.setUserNote(userNote);
		}
	}

	/**
	 * Method to authorize segment modification according to the segment modification privileges and fare rule settings
	 * 
	 * @param newONDs
	 * @param modifiedONDs
	 * @param reservation
	 * @throws modCnxPnrSegIds
	 * @throws privilegeKeys
	 */
	public static void authorizeSegmentWiseModification(Collection<OndFareDTO> newONDFareDTOs,
			AirItineraryType.OriginDestinationOptions modifiedONDs, Reservation reservation, Collection modCnxPnrSegIds,
			Collection privilegeKeys) throws ModuleException, WebservicesException {
		// boolean isReturnTrip = false;
		List<FlightSegmentDTO> outBoundFlightSegments = new ArrayList<FlightSegmentDTO>();
		List<FlightSegmentDTO> inBoundFlightSegments = new ArrayList<FlightSegmentDTO>();
		for (OndFareDTO ondFareDTO : newONDFareDTOs) {
			if (ondFareDTO.isInBoundOnd()) {
				inBoundFlightSegments.addAll(ondFareDTO.getSegmentsMap().values());
			} else {
				outBoundFlightSegments.addAll(ondFareDTO.getSegmentsMap().values());
			}
		}

		Collections.sort(outBoundFlightSegments);
		Collections.sort(inBoundFlightSegments);

		Date journeyStartDate = outBoundFlightSegments.get(0).getDepartureDateTime();
		Date journeyEndDate = null;
		String journeyStartAirpport = outBoundFlightSegments.get(0).getFromAirport();
		String journeyEndAirport = outBoundFlightSegments.get(outBoundFlightSegments.size() - 1).getToAirport();
		if (inBoundFlightSegments.size() > 0) {
			journeyEndDate = inBoundFlightSegments.get(inBoundFlightSegments.size() - 1).getArrivalDateTime();
			// isReturnTrip = true;
			if (!journeyEndAirport.equals(inBoundFlightSegments.get(inBoundFlightSegments.size() - 1).getToAirport())) {
				journeyEndAirport = inBoundFlightSegments.get(inBoundFlightSegments.size() - 1).getToAirport();
			}
		} else {
			journeyEndDate = outBoundFlightSegments.get(outBoundFlightSegments.size() - 1).getArrivalDateTime();
		}

		String modifiedArrivalAirport = "";
		String modifedDepartureAirport = "";
		Date modifiedArrivalTime = null;
		Date modifiedDepartureTime = null;

		// getting modified onds and times
		for (OriginDestinationOptionType originDestinationOptionType : modifiedONDs.getOriginDestinationOption()) {
			for (BookFlightSegmentType bookFlightSegmentType : originDestinationOptionType.getFlightSegment()) {
				modifiedArrivalAirport = bookFlightSegmentType.getArrivalAirport().getLocationCode();
				modifedDepartureAirport = bookFlightSegmentType.getDepartureAirport().getLocationCode();
				modifiedArrivalTime = bookFlightSegmentType.getArrivalDateTime().toGregorianCalendar().getTime();
				modifiedDepartureTime = bookFlightSegmentType.getDepartureDateTime().toGregorianCalendar().getTime();
			}
		}

		// check route modification or date modification
		boolean isRouteModification = !((modifiedArrivalAirport + modifedDepartureAirport)
				.equals(journeyStartAirpport + journeyEndAirport));
		boolean isDateModification = false;
		if (journeyStartDate != null && modifiedDepartureTime != null) {
			if (!(journeyStartDate.equals(modifiedDepartureTime))) {
				isDateModification = true;
			}
		}

		// getting fare rule segment modification statuses
		PNRModifyPreValidationStatesTO statesTO = ReservationValidationUtils.getReservationOrSegPreValidationStates(reservation,
				modCnxPnrSegIds);

		// authorize segment modification according to the fare rule
		if (isRouteModification) {
			if (AuthorizationUtil.hasPrivilege(privilegeKeys,
					PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_MODIFY_SEGMENT_ROUTE)) {
				if (!statesTO.isAllowModifySegRoute()) {
					// throw error
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_MODIFICATION_TYPE,
							"Segment OND cannot be modified according to the fare rule");
				}
			} else {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR,
						"Unauthorized for OND modification in segments");
			}
		}
		if (isDateModification) {
			if (AuthorizationUtil.hasPrivilege(privilegeKeys,
					PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_MODIFY_SEGMENT_DATE)) {
				if (!statesTO.isAllowModifySegDate()) {
					// throw error
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_MODIFICATION_TYPE,
							"Segment Dates cannot be modified according to the fare rule");
				}
			} else {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR,
						"Unauthorized for date modification in segments");
			}
		}
	}

	/**
	 * Temporary validation for not allowing modifications for LCC reservations via web service API
	 * 
	 * @param res
	 * @throws WebservicesException
	 */
	public static void authorizeLCCReservation(Reservation res) throws WebservicesException {
		if (res != null && res.getOriginatorPnr() != null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR,
					"LCC reservations are not allowed for modifications via WS");
		}
	}

	public TrackInfoDTO getTrackInfo(UserPrincipal userPrincipal) {

		String sessionId = (String) ThreadLocalData.getWSContextParam(WebservicesContext.WEB_SESSION_ID);
		String ipAddress = BeanUtils.nullHandler(ThreadLocalData.getWSContextParam(
				WebservicesContext.WEB_REQUEST_IP));

		TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
		trackInfoDTO.setIpAddress(ipAddress);
		trackInfoDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		trackInfoDTO.setSessionId(sessionId);
		trackInfoDTO.setCallingInstanceId(userPrincipal.getCallingInstanceId());
		trackInfoDTO.setPaymentAgent(userPrincipal.getAgentCode());
		trackInfoDTO.setAppIndicator(AppIndicatorEnum.APP_WS);
		trackInfoDTO.setDirectBillId(userPrincipal.getDirectBillId());
		return trackInfoDTO;
	}

	private CommonReservationContactInfo getContactInfo(ReservationContactInfo contactInfo) {

		CommonReservationContactInfo ccontactInfo = new CommonReservationContactInfo();

		ccontactInfo.setTitle(contactInfo.getTitle());
		ccontactInfo.setFirstName(contactInfo.getFirstName());
		ccontactInfo.setLastName(contactInfo.getLastName());
		ccontactInfo.setTitle(contactInfo.getTitle());
		ccontactInfo.setCity(contactInfo.getCity());
		ccontactInfo.setCountryCode(contactInfo.getCountryCode());
		ccontactInfo.setCustomerId(contactInfo.getCustomerId());
		ccontactInfo.setEmail(contactInfo.getEmail());
		ccontactInfo.setState(contactInfo.getState());
		ccontactInfo.setFax(contactInfo.getFax());
		ccontactInfo.setMobileNo(contactInfo.getMobileNo());
		ccontactInfo.setPhoneNo(contactInfo.getPhoneNo());
		if (contactInfo.getNationalityCode() != null) {
			ccontactInfo.setNationalityCode(Integer.parseInt(contactInfo.getNationalityCode()));
		}
		ccontactInfo.setStreetAddress1(contactInfo.getStreetAddress1());
		ccontactInfo.setStreetAddress2(contactInfo.getStreetAddress2());
		ccontactInfo.setPreferredLanguage(contactInfo.getPreferredLanguage());
		ccontactInfo.setZipCode(contactInfo.getZipCode());

		ccontactInfo.setEmgnTitle(contactInfo.getEmgnTitle());
		ccontactInfo.setEmgnFirstName(contactInfo.getEmgnFirstName());
		ccontactInfo.setEmgnLastName(contactInfo.getEmgnLastName());
		ccontactInfo.setEmgnPhoneNo(contactInfo.getEmgnPhoneNo());
		ccontactInfo.setEmgnEmail(contactInfo.getEmgnEmail());

		return ccontactInfo;
	}

	/**
	 * Ground Segment
	 */

	private static Collection<Integer> getRefSegmentsPnr(List<OriginDestinationOptionType> listOndSegment)
			throws NumberFormatException {
		Collection<Integer> pnrCollection = new ArrayList<Integer>();
		for (OriginDestinationOptionType clientSeg : listOndSegment) {
			for (BookFlightSegmentType cliResSegment : clientSeg.getFlightSegment()) {
				String refRPH = cliResSegment.getRPH();

				if (refRPH != null) {
					pnrCollection.add(Integer.parseInt(refRPH));
				}

			}
		}
		return pnrCollection;

	}

	private static List<OriginDestinationOptionType> combineOndExistAndNew(List<OriginDestinationOptionType> newSegments,
			List<OriginDestinationOptionType> existSegments) {

		List<OriginDestinationOptionType> allSegmentList = new ArrayList<OriginDestinationOptionType>();
		allSegmentList.addAll(newSegments);
		allSegmentList.addAll(existSegments);

		return allSegmentList;
	}

	/**
	 * Update balance summary modify ond with quoted external charges
	 * 
	 * @param resSummaryDTO
	 * @param resPaxMap
	 * @throws ModuleException
	 */
	public static void updateResSummaryModificationWithExtCharges(ResBalancesSummaryDTO resSummaryDTO,
			Map<Integer, ReservationPax> resPaxMap) throws ModuleException {

		// Total External charges quoted for new ond
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		BigDecimal totalExternalCharge = BookUtil.getTotalAmountOfSelectedExternalCharges(AppSysParamsUtil.getBaseCurrency(),
				exchangeRateProxy);

		resSummaryDTO.getSegmentSummeryDTO().setNewTotalCharges(
				AccelAeroCalculator.add(resSummaryDTO.getSegmentSummeryDTO().getNewTotalCharges(), totalExternalCharge));
		resSummaryDTO.getSegmentSummeryDTO()
				.setNewCharge(AccelAeroCalculator.add(resSummaryDTO.getSegmentSummeryDTO().getNewCharge(), totalExternalCharge));

		resSummaryDTO.setTotalPrice(AccelAeroCalculator.add(resSummaryDTO.getTotalPrice(), totalExternalCharge));

		if (resSummaryDTO.getTotalAvailableBalance().compareTo(BigDecimal.ZERO) == 1) {
			if (resSummaryDTO.getTotalAvailableBalance().compareTo(totalExternalCharge) == 1) {
				resSummaryDTO.setTotalAvailableBalance(
						AccelAeroCalculator.subtract(resSummaryDTO.getTotalAvailableBalance(), totalExternalCharge));
				resSummaryDTO.setTotalAmountDue(AccelAeroCalculator.getDefaultBigDecimalZero());
			} else {
				resSummaryDTO.setTotalAmountDue(
						AccelAeroCalculator.subtract(totalExternalCharge, resSummaryDTO.getTotalAvailableBalance()));
				resSummaryDTO.setTotalAvailableBalance(AccelAeroCalculator.getDefaultBigDecimalZero());
			}

		} else {
			resSummaryDTO.setTotalAvailableBalance(AccelAeroCalculator.getDefaultBigDecimalZero());
			resSummaryDTO.setTotalAmountDue(AccelAeroCalculator.add(resSummaryDTO.getTotalAmountDue(), totalExternalCharge));
		}

		Collection<PAXSummeryDTO> paxSummaries = resSummaryDTO.getPaxSummaryDTOCol();

		// External charges
		Map<String, BigDecimal> extChargeMap = BookUtil.getQuotedExternalChargesAmountMap();

		for (PAXSummeryDTO paxSummary : paxSummaries) {

			String travelerRPH = CommonServicesUtil.getTravelerRPH(resPaxMap.get(paxSummary.getPNRPaxId()), false);
			if (extChargeMap != null && extChargeMap.containsKey(travelerRPH)) {

				BigDecimal paxExtCharge = extChargeMap.get(travelerRPH);
				paxSummary.setNewTotalCharges(AccelAeroCalculator.add(paxSummary.getNewTotalCharges(), paxExtCharge));
				paxSummary.setNewBalance(AccelAeroCalculator.add(paxSummary.getNewBalance(), paxExtCharge));

			}
		}
	}
}
