package com.isa.thinair.webservices.core.cache.delegator.auditor;
import  com.isa.thinair.webservices.core.cache.util.CacheConstant.Operation;

public abstract class BaseAuditor<Request> {

	public Request request;

	public BaseAuditor(Request request) {
		this.request = request;
	}
	
	public abstract String getAudit(Operation operation);
}
