package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.CompanyNameType;
import org.opentravel.ota._2003._05.CoveredTravelerType;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.InsCoverageDetailType.CoveredTrips.CoveredTrip;
import org.opentravel.ota._2003._05.OTAAirPriceRQ;
import org.opentravel.ota._2003._05.OTAInsuranceQuoteRQ;
import org.opentravel.ota._2003._05.OTAInsuranceQuoteRQ.PlanForQuoteRQ;
import org.opentravel.ota._2003._05.OTAInsuranceQuoteRS;
import org.opentravel.ota._2003._05.OTAInsuranceQuoteRS.PlanForQuoteRS;
import org.opentravel.ota._2003._05.OTAInsuranceQuoteRS.PlanForQuoteRS.QuoteDetail;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.PlanCostType;
import org.opentravel.ota._2003._05.PlanCostType.BasePremium;
import org.opentravel.ota._2003._05.SuccessType;
import org.opentravel.ota._2003._05.TravelerInformationType;
import org.opentravel.ota._2003._05.URLType;
import org.opentravel.ota._2003._05.WarningsType;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredPassengerDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.ISegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.INSURANCE_STATES;
import com.isa.thinair.commons.api.constants.CommonsConstants.INSURANCEPROVIDER;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.core.config.WPConfig;
import com.isa.thinair.webservices.api.dtos.OTAPaxCountTO;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsurableFlightSegment;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;
import com.isa.thinair.wsclient.api.dto.aig.InsurePassenger;
import com.isa.thinair.wsclient.api.dto.aig.InsureSegment;

public class InsuranceServiceUtil {

	private static final Log log = LogFactory.getLog(InsuranceServiceUtil.class);

	private enum TripTypes {
		OneWay, Return
	};

	public OTAInsuranceQuoteRS getInsuranceQuote(OTAInsuranceQuoteRQ otaInsuranceQuoteRQ) throws WebservicesException,
			ModuleException {
		OTAInsuranceQuoteRS otaInsuranceQuoteRS = null;

		if (AppSysParamsUtil.isShowTravelInsurance() && isInsuranceAvailable(otaInsuranceQuoteRQ)) {
			LCCInsuredJourneyDTO journeyDTO = new LCCInsuredJourneyDTO();
			IInsuranceRequest insuranceRequest = composeInsuranceRequest(otaInsuranceQuoteRQ, journeyDTO);
			LCCInsuranceQuotationDTO insuranceQuotationDTO = getInsuranceQuotation(insuranceRequest, otaInsuranceQuoteRQ);
			if (!(insuranceQuotationDTO.getResponseCode().equals("0") && insuranceQuotationDTO.getReponseMessage().equals("OK"))) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_NO_AVAILABILITY,
						"Insurance is not available.Please check the request and try again");
			}

			otaInsuranceQuoteRS = generateInsuranceQuoteRS(insuranceQuotationDTO, otaInsuranceQuoteRQ);

			// add the insurance to the transaction
			insuranceQuotationDTO.setInsuredJourney(journeyDTO);
			ThreadLocalData.setCurrentTnxParam(Transaction.QUOTED_INSURANCE, insuranceQuotationDTO);
		} else {
			// insurance is not available or not enabled
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_NO_AVAILABILITY,
					"Insurance is not available for the requested trip");
		}

		return otaInsuranceQuoteRS;
	}

	private boolean isInsuranceAvailable(OTAInsuranceQuoteRQ otaInsuranceQuoteRQ) throws ModuleException {
		boolean isInsuranceAvailable = false;

		isInsuranceAvailable = WebServicesModuleUtils.getCommonMasterBD().isAIGCountryCodeActive(
				otaInsuranceQuoteRQ.getPlanForQuoteRQ().get(0).getInsCoverageDetail().getCoveredTrips().getCoveredTrip().get(0)
						.getStart());

		return isInsuranceAvailable;
	}

	private IInsuranceRequest composeInsuranceRequest(OTAInsuranceQuoteRQ otaInsuranceQuoteRQ, LCCInsuredJourneyDTO journeyDTO)
			throws WebservicesException {
		IInsuranceRequest insuranceRequest = new InsuranceRequestAssembler();

		if (otaInsuranceQuoteRQ.getPlanForQuoteRQ() != null && otaInsuranceQuoteRQ.getPlanForQuoteRQ().size() == 1) {

			PlanForQuoteRQ planForQuoteRQ = otaInsuranceQuoteRQ.getPlanForQuoteRQ().get(0);

			List<CoveredTravelerType> coveredTravelerTypes = planForQuoteRQ.getCoveredTravelers().getCoveredTraveler();
			if (coveredTravelerTypes != null && coveredTravelerTypes.size() >= 1) {
				for (CoveredTravelerType traveler : coveredTravelerTypes) {
					InsurePassenger insurePassenger = getInsuredPassenger(traveler);
					insuranceRequest.addPassenger(insurePassenger);
				}
			} else {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_SERVICE_REQUESTED_INCORRECT_,
						"At least one passenger must be insured");
			}

			if (planForQuoteRQ.getInsCoverageDetail().getCoveredTrips().getCoveredTrip() != null
					&& planForQuoteRQ.getInsCoverageDetail().getCoveredTrips().getCoveredTrip().size() == 1) {

				InsureSegment insureSegment = getPopulatedInsureSegment(planForQuoteRQ.getInsCoverageDetail().getCoveredTrips()
						.getCoveredTrip().get(0));
				insuranceRequest.addFlightSegment(insureSegment);

				setJourneyDetails(journeyDTO, insureSegment);

				QuotedPriceInfoTO quotedPriceInfoTO = (QuotedPriceInfoTO) ThreadLocalData
						.getCurrentTnxParam(Transaction.QUOTED_PRICE_INFO);
				setInsuredFlightDetails(insuranceRequest, quotedPriceInfoTO);

			}
		} else {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_SERVICE_REQUESTED_INCORRECT_,
					"Invalid insurance request");
		}

		return insuranceRequest;
	}

	private void setInsuredFlightDetails(IInsuranceRequest insuranceRequest, QuotedPriceInfoTO quotedPriceInfoTO) {
		if (quotedPriceInfoTO != null && quotedPriceInfoTO.getQuotedSegments().size() > 0) {
			List<InsurableFlightSegment> flightSegments = new ArrayList<InsurableFlightSegment>();
			for (FlightSegmentTO flightSegmentTO : quotedPriceInfoTO.getQuotedSegments()) {
				InsurableFlightSegment flgtSegInfo = new InsurableFlightSegment();
				flgtSegInfo.setDepartureStationCode(flightSegmentTO.getSegmentCode().substring(0, 3));
				flgtSegInfo.setDepartureDateTimeLocal(flightSegmentTO.getDepartureDateTime());
				flgtSegInfo.setDepartureDateTimeZulu(flightSegmentTO.getDepartureDateTimeZulu());
				flgtSegInfo.setArrivalStationCode(flightSegmentTO.getSegmentCode().substring(
						flightSegmentTO.getSegmentCode().length() - 3, flightSegmentTO.getSegmentCode().length()));
				flgtSegInfo.setFlightNo(flightSegmentTO.getFlightNumber());
				flightSegments.add(flgtSegInfo);
			}
			insuranceRequest.setFlightSegments(flightSegments);
		}
	}

	
	private void setJourneyDetails(LCCInsuredJourneyDTO journeyDTO, InsureSegment insureSegment) {
		journeyDTO.setJourneyStartAirportCode(insureSegment.getFromAirportCode());
		journeyDTO.setJourneyEndAirportCode(insureSegment.getToAirportCode());
		journeyDTO.setJourneyStartDate(insureSegment.getDepartureDate());
		journeyDTO.setJourneyEndDate(insureSegment.getArrivalDate());
		journeyDTO.setRoundTrip(insureSegment.isRoundTrip());
	}

	private InsurePassenger getInsuredPassenger(CoveredTravelerType traveler) {
		InsurePassenger insurePassenger = new InsurePassenger();

		if (traveler.getAddress().get(0).getAddressLine().get(0) != null) {
			insurePassenger.setAddressLn1(traveler.getAddress().get(0).getAddressLine().get(0));
		}

		if (traveler.getAddress().get(0).getAddressLine().size() > 1
				&& traveler.getAddress().get(0).getAddressLine().get(1) != null) {
			insurePassenger.setAddressLn2(traveler.getAddress().get(0).getAddressLine().get(1));
		}

		insurePassenger.setCity(traveler.getAddress().get(0).getCityName());
		if (traveler.getEmail() != null && traveler.getEmail().size() > 0) {
			insurePassenger.setEmail(traveler.getEmail().get(0).getValue());
		}
		insurePassenger.setCountryOfAddress(traveler.getAddress().get(0).getCountryName().getValue());
		insurePassenger.setFirstName(traveler.getCoveredPerson().getGivenName().get(0));
		insurePassenger.setLastName(traveler.getCoveredPerson().getSurname());

		return insurePassenger;
	}

	private InsureSegment getPopulatedInsureSegment(CoveredTrip coveredTrip) {

		InsureSegment insureSegment = new InsureSegment();
		insureSegment.setFromAirportCode(coveredTrip.getStart());
		insureSegment.setToAirportCode(coveredTrip.getEnd());
		insureSegment.setDepartureDate(coveredTrip.getDestinations().getDestination().get(0).getDepartureDate()
				.toGregorianCalendar().getTime());
		insureSegment.setArrivalDate(coveredTrip.getDestinations().getDestination().get(0).getArrivalDate().toGregorianCalendar()
				.getTime());
		if (coveredTrip.getDestinations().getDestination().get(0).getType().equals(TripTypes.OneWay.toString())) {
			insureSegment.setRoundTrip(false);
		} else if (coveredTrip.getDestinations().getDestination().get(0).getType().equals(TripTypes.Return.toString())) {
			insureSegment.setRoundTrip(true);
		}

		// CreateDummy flight number for tune insurance (with Real Carrier code)
		if (coveredTrip.getOperators() != null && coveredTrip.getOperators().getOperator().size() > 0) {
			insureSegment.setDepartureFlightNo(coveredTrip.getOperators().getOperator().get(0).getCode() + "XXX");
			insureSegment.setArrivalFlightNo(coveredTrip.getOperators().getOperator().get(0).getCode() + "XXX");
		} else {
			insureSegment.setDepartureFlightNo(AppSysParamsUtil.getDefaultCarrierCode() + "XXX");
			insureSegment.setArrivalFlightNo(AppSysParamsUtil.getDefaultCarrierCode() + "XXX");
		}

		insureSegment.setSalesChanelCode(SalesChannelsUtil.SALES_CHANNEL_AGENT);

		return insureSegment;
	}

	private LCCInsuranceQuotationDTO getInsuranceQuotation(IInsuranceRequest iRequest, OTAInsuranceQuoteRQ otaInsuranceQuoteRQ) {
		LCCInsuranceQuotationDTO insuranceDTO = new LCCInsuranceQuotationDTO();
		try {
			if (AppSysParamsUtil.isShowTravelInsurance()) {

				List<InsuranceResponse> insuranceResponses = AirproxyModuleUtils.getInsuranceClientBD().quoteInsurancePolicy(
						iRequest);

				if (insuranceResponses != null && !insuranceResponses.isEmpty()) {

					InsuranceResponse insuranceResponse = insuranceResponses.get(0);
					insuranceDTO.setResponseCode(insuranceResponse.getErrorCode());
					insuranceDTO.setReponseMessage(insuranceResponse.getErrorMessage());
					if (insuranceResponse.getErrorCode().equals("0") && insuranceResponse.getErrorMessage().equals("OK")) {
						insuranceDTO.setPolicyCode(insuranceResponse.getPolicyCode());
						insuranceDTO.setQuotedCurrencyCode(insuranceResponse.getQuotedCurrencyCode());
						insuranceDTO.setQuotedTotalPremiumAmount(insuranceResponse.getQuotedTotalPremiumAmount());
						insuranceDTO.setOperatingAirline(AppSysParamsUtil.getDefaultCarrierCode());
						insuranceDTO.setInsTypeCharges(insuranceResponse.getInsTypeCharges());
						insuranceDTO.setSsrFeeCode(insuranceResponse.getSsrFeeCode());
						insuranceDTO.setPlanCode(insuranceResponse.getPlanCode());

						InsureSegment insureSegment = getPopulatedInsureSegment(otaInsuranceQuoteRQ.getPlanForQuoteRQ().get(0)
								.getInsCoverageDetail().getCoveredTrips().getCoveredTrip().get(0));

						Integer insuranceId = persistReservationInsurance(insuranceResponse, insureSegment, iRequest.getNoOfPax());

						if (log.isDebugEnabled()) {
							log.debug("Insurance quote saved successfully, insurance id saved : " + insuranceId);
						}

						insuranceDTO.setInsuranceRefNumber(insuranceId + "");
					}
				}
			}
		} catch (ModuleException e) {
			log.error("Module exception caught.", e);
		} catch (Exception e) {
			log.error("Generic exception caught.", e);
		}

		return insuranceDTO;
	}

	private Integer
			persistReservationInsurance(InsuranceResponse insuranceResponse, InsureSegment insureSegment, Integer paxCount) {

		Integer insuranceId = null;

		ReservationInsurance reservationInsurance = new ReservationInsurance();

		reservationInsurance.setQuoteTime(new Date());
		reservationInsurance.setState(INSURANCE_STATES.QO.toString());

		reservationInsurance.setPolicyCode(insuranceResponse.getPolicyCode());
		reservationInsurance.setMessageId(insuranceResponse.getMessageId());

		reservationInsurance.setQuotedCurrencyCode(insuranceResponse.getQuotedCurrencyCode());

		reservationInsurance.setAmount(insuranceResponse.getTotalPremiumAmount());
		reservationInsurance.setQuotedAmount(insuranceResponse.getQuotedTotalPremiumAmount());

		reservationInsurance.setTaxAmount(insuranceResponse.getTaxAmount());
		reservationInsurance.setQuotedTaxAmout(insuranceResponse.getQuotedTaxAmout());

		reservationInsurance.setNetAmount(insuranceResponse.getNetAmount());
		reservationInsurance.setQuotedNetAmount(insuranceResponse.getQuotedNetAmount());

		reservationInsurance.setSsrFeeCode(insuranceResponse.getSsrFeeCode());
		reservationInsurance.setPlanCode(insuranceResponse.getPlanCode());

		reservationInsurance.setInsuranceProductConfigID(insuranceResponse.getInsProductConfig().getInsuranceProductConfigID());
		reservationInsurance.setRoundTrip(insureSegment.isRoundTrip() ? "Y" : "N");
		reservationInsurance.setTotalPaxCount(paxCount);
		reservationInsurance.setDateOfTravel(insureSegment.getDepartureDate());
		reservationInsurance.setDateOfReturn(insureSegment.getArrivalDate());
		reservationInsurance.setOrigin(insureSegment.getFromAirportCode());
		reservationInsurance.setDestination(insureSegment.getToAirportCode());
		reservationInsurance.setMarketingCarrier(AppSysParamsUtil.getDefaultCarrierCode());
		reservationInsurance.setInsuranceType(0);
		reservationInsurance.setInsuranceTotalTicketPrice(null);
		reservationInsurance.setSalesChannel(insureSegment.getSalesChanelCode());

		insuranceId = AirproxyModuleUtils.getReservationBD().saveReservationInsurance(reservationInsurance);

		return insuranceId;

	}


	private OTAInsuranceQuoteRS generateInsuranceQuoteRS(LCCInsuranceQuotationDTO insuranceQuotationDTO,
			OTAInsuranceQuoteRQ otaInsuranceQuoteRQ) {
		String insuranceLink = WPConfig.getServerMessage("cc_insurance_link");
		OTAInsuranceQuoteRS otaInsuranceQuoteRS = new OTAInsuranceQuoteRS();

		otaInsuranceQuoteRS.setVersion(WebservicesConstants.OTAConstants.VERSION_AirPrice);
		otaInsuranceQuoteRS.setTransactionIdentifier(otaInsuranceQuoteRQ.getTransactionIdentifier());
		otaInsuranceQuoteRS.setSequenceNmbr(otaInsuranceQuoteRQ.getSequenceNmbr());
		otaInsuranceQuoteRS.setEchoToken(otaInsuranceQuoteRQ.getEchoToken());
		otaInsuranceQuoteRS.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);

		otaInsuranceQuoteRS.setErrors(new ErrorsType());
		otaInsuranceQuoteRS.setWarnings(new WarningsType());

		PlanForQuoteRS planForQuoteRS = new PlanForQuoteRS();

		QuoteDetail quoteDetail = new QuoteDetail();
		URLType urlType = new URLType();
		urlType.setValue(insuranceLink);
		quoteDetail.setQuoteDetailURL(urlType);
		CompanyNameType companyNameType = new CompanyNameType();
		
		List<INSURANCEPROVIDER>  insProviders =  AppSysParamsUtil.getInsuranceProvidersList();
		String companyNames =  "";
		if(!insProviders.isEmpty()){
			for (INSURANCEPROVIDER insuranceprovider : insProviders) {
				if(!companyNames.isEmpty()){
					companyNames += " , ";
				}
			companyNames += insuranceprovider;
			}
			companyNameType.setCompanyShortName(companyNames);
			planForQuoteRS.setName(companyNames);
		}

		quoteDetail.setProviderCompany(companyNameType);

		planForQuoteRS.setQuoteDetail(quoteDetail);

		PlanCostType planCostType = new PlanCostType();
		planCostType.setAmount(insuranceQuotationDTO.getQuotedTotalPremiumAmount());
		planCostType.setCurrencyCode(AppSysParamsUtil.getBaseCurrency());
		BasePremium basePremium = new BasePremium();
		basePremium.setAmount(insuranceQuotationDTO.getQuotedTotalPremiumAmount());
		basePremium.setCurrencyCode(AppSysParamsUtil.getBaseCurrency());

		planForQuoteRS.setPlanCost(planCostType);

		
		if (insProviders.contains(INSURANCEPROVIDER.TUNE)) {
			planForQuoteRS.setPlanID(insuranceQuotationDTO.getSsrFeeCode());
		}

		planForQuoteRS.setRPH(insuranceQuotationDTO.getInsuranceRefNumber());
		planForQuoteRS.setType("Travel");

		otaInsuranceQuoteRS.getPlanForQuoteRS().add(planForQuoteRS);
		otaInsuranceQuoteRS.setSuccess(new SuccessType());

		// TODO NAFLY, see if we can add the insurance amount also in the response, so that when doing a price quote
		// we can validate the amount(user will need to send the amount, posibly in the RPH
		return otaInsuranceQuoteRS;
	}

	public static Map<String, List<ExternalChgDTO>> getExternalChargesMapForInsurance(OTAAirPriceRQ otaAirPriceRQ,
			ReservationInsurance resInsurance) throws ModuleException {
		Map<String, List<ExternalChgDTO>> externalChargesMap = new HashMap<String, List<ExternalChgDTO>>();

		int adultCount = 0;
		int childCount = 0;
		for (TravelerInformationType info : otaAirPriceRQ.getTravelerInfoSummary().getAirTravelerAvail()) {
			for (PassengerTypeQuantityType passengerTypeQuantity : info.getPassengerTypeQuantity()) {
				if (passengerTypeQuantity.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
					adultCount += passengerTypeQuantity.getQuantity();
				}
				if (passengerTypeQuantity.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
					childCount += passengerTypeQuantity.getQuantity();
				}
			}
		}

		Collection<EXTERNAL_CHARGES> externalChargesTypes = new ArrayList<EXTERNAL_CHARGES>();
		externalChargesTypes.add(EXTERNAL_CHARGES.INSURANCE);
		Map<EXTERNAL_CHARGES, ExternalChgDTO> quotedMap = WebServicesModuleUtils.getReservationBD().getQuotedExternalCharges(
				externalChargesTypes, null, null);
		ExternalChgDTO quotedChgDTO = quotedMap.get(EXTERNAL_CHARGES.INSURANCE);
		if (quotedChgDTO == null) {
			throw new ModuleException("Insurance quotation failed");
		}

		BigDecimal insPaxWise[] = AccelAeroCalculator.roundAndSplit(resInsurance.getAmount(), (adultCount + childCount));

		for (int i = 1; i <= adultCount; ++i) {
			String travelerRef = "A" + i;
			if (externalChargesMap.get(travelerRef) == null) {
				externalChargesMap.put(travelerRef, new ArrayList<ExternalChgDTO>());
			}
			ExternalChgDTO externalChgDTO = (ExternalChgDTO) quotedChgDTO.clone();
			externalChgDTO.setAmount(insPaxWise[i - 1]);
			externalChargesMap.get(travelerRef).add(externalChgDTO);
		}

		for (int i = 1; i <= childCount; ++i) {
			String travelerRef = "C" + (adultCount + i);
			if (externalChargesMap.get(travelerRef) == null) {
				externalChargesMap.put(travelerRef, new ArrayList<ExternalChgDTO>());
			}
			ExternalChgDTO externalChgDTO = (ExternalChgDTO) quotedChgDTO.clone();
			externalChgDTO.setAmount(insPaxWise[(adultCount + i) - 1]);
			externalChargesMap.get(travelerRef).add(externalChgDTO);
		}

		return externalChargesMap;
	}

	/**
	 * 
	 * @param otaAirPriceRQ
	 * @param resInsurance
	 * @return
	 * @throws ModuleException
	 */
	public static Map<String, List<LCCClientExternalChgDTO>> getExternalChargesMapForInsurance(OTAAirPriceRQ otaAirPriceRQ,
			LCCInsuranceQuotationDTO resInsurance) throws ModuleException {
		Map<String, List<LCCClientExternalChgDTO>> externalChargesMap = new HashMap<String, List<LCCClientExternalChgDTO>>();

		int adultCount = 0;
		int childCount = 0;
		for (TravelerInformationType info : otaAirPriceRQ.getTravelerInfoSummary().getAirTravelerAvail()) {
			for (PassengerTypeQuantityType passengerTypeQuantity : info.getPassengerTypeQuantity()) {
				if (passengerTypeQuantity.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
					adultCount += passengerTypeQuantity.getQuantity();
				}
				if (passengerTypeQuantity.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
					childCount += passengerTypeQuantity.getQuantity();
				}
			}
		}

		BigDecimal insPaxWise[] = AccelAeroCalculator.roundAndSplit(resInsurance.getQuotedTotalPremiumAmount(),
				(adultCount + childCount));

		for (int i = 1; i <= adultCount; ++i) {
			String travelerRef = "A" + i;
			if (externalChargesMap.get(travelerRef) == null) {
				externalChargesMap.put(travelerRef, new ArrayList<LCCClientExternalChgDTO>());
			}
			LCCClientExternalChgDTO externalChgDTO = new LCCClientExternalChgDTO();
			externalChgDTO.setExternalCharges(EXTERNAL_CHARGES.INSURANCE);
			externalChgDTO.setAmount(insPaxWise[i - 1]);
			externalChargesMap.get(travelerRef).add(externalChgDTO);
		}

		for (int i = 1; i <= childCount; ++i) {
			String travelerRef = "C" + (adultCount + i);
			if (externalChargesMap.get(travelerRef) == null) {
				externalChargesMap.put(travelerRef, new ArrayList<LCCClientExternalChgDTO>());
			}
			LCCClientExternalChgDTO externalChgDTO = new LCCClientExternalChgDTO();
			externalChgDTO.setExternalCharges(EXTERNAL_CHARGES.INSURANCE);
			externalChgDTO.setAmount(insPaxWise[(adultCount + i) - 1]);
			externalChargesMap.get(travelerRef).add(externalChgDTO);
		}

		return externalChargesMap;
	}

	public static void populateInsurance(Reservation reservation, IReservation iReservation, ISegment iSegment,
			Integer insuranceRefNo, boolean isModify) throws ModuleException {
		ReservationInsurance reservationInsurance = WebServicesModuleUtils.getReservationBD().getReservationInsurance(
				insuranceRefNo);

		if (!isModify) {
			ReservationAssembler assembler = (ReservationAssembler) iReservation;
			reservation = assembler.getDummyReservation();
		}

		Set<ReservationPax> passengers = reservation.getPassengers();
		ReservationContactInfo contactInfo = reservation.getContactInfo();

		IInsuranceRequest insurance = new InsuranceRequestAssembler();

		InsureSegment insSegment = new InsureSegment();
		insSegment.setArrivalDate(reservationInsurance.getDateOfReturn());
		insSegment.setDepartureDate(reservationInsurance.getDateOfTravel());
		insSegment.setFromAirportCode(reservationInsurance.getOrigin());
		insSegment.setToAirportCode(reservationInsurance.getDestination());
		insSegment.setRoundTrip(reservationInsurance.getRoundTrip().equals("Y") ? true : false);
		insSegment.setSalesChanelCode(SalesChannelsUtil.SALES_CHANNEL_AGENT);

		insurance.setInsuranceId(reservationInsurance.getInsuranceId());
		insurance.addFlightSegment(insSegment);
		insurance.setQuotedTotalPremiumAmount(reservationInsurance.getAmount());
		insurance.setInsuranceType(reservationInsurance.getInsuranceType());
		insurance.setSsrFeeCode(reservationInsurance.getSsrFeeCode());
		insurance.setPlanCode(reservationInsurance.getPlanCode());
		BigDecimal insPaxWise[] = AccelAeroCalculator.roundAndSplit(reservationInsurance.getAmount(), passengers.size());
		int i = 0;
		for (ReservationPax pax : passengers) {
			if (!pax.getPaxType().equals(PaxTypeTO.INFANT)) {
				InsurePassenger insurePassenger = new InsurePassenger();

				// adding AIG Pax info
				insurePassenger.setFirstName(pax.getFirstName());
				insurePassenger.setLastName(pax.getLastName());
				insurePassenger.setDateOfBirth(pax.getDateOfBirth());

				// Adding AIG Contact Info
				insurePassenger.setAddressLn1(contactInfo.getStreetAddress1());
				insurePassenger.setAddressLn2(contactInfo.getStreetAddress2());
				insurePassenger.setCity(contactInfo.getCity());
				insurePassenger.setEmail(contactInfo.getEmail());
				insurePassenger.setCountryOfAddress(contactInfo.getCountryCode());
				insurePassenger.setHomePhoneNumber(contactInfo.getPhoneNo());

				insurance.addPassenger(insurePassenger);
				insurance.addInsuranceCharge(pax.getPaxSequence(), insPaxWise[i++]);
			}
		}

		if (isModify) {
			iSegment.addInsurance(insurance);
		} else {
			iReservation.addInsurance(insurance);
		}
	}

	public static void
			validateInsuranceOnModification(Reservation res, Collection<OndFareDTO> ondFareDTOs, Integer insuraneRefNo)
					throws WebservicesException {
		if (res.getReservationInsurance() != null) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION,
					"Add/Edit insurance is not allowed for already insured reservations");
		}

		ReservationInsurance insurance = WebServicesModuleUtils.getReservationBD().getReservationInsurance(insuraneRefNo);

		boolean isReturnTrip = false;
		List<FlightSegmentDTO> outBoundFlightSegments = new ArrayList<FlightSegmentDTO>();
		List<FlightSegmentDTO> inBoundFlightSegments = new ArrayList<FlightSegmentDTO>();
		for (OndFareDTO ondFareDTO : ondFareDTOs) {
			if (ondFareDTO.isInBoundOnd()) {
				inBoundFlightSegments.addAll(ondFareDTO.getSegmentsMap().values());
			} else {
				outBoundFlightSegments.addAll(ondFareDTO.getSegmentsMap().values());
			}
		}

		Collections.sort(outBoundFlightSegments);
		Collections.sort(inBoundFlightSegments);

		Date journeyStartDate = outBoundFlightSegments.get(0).getDepartureDateTime();
		Date journeyEndDate = null;
		String journeyStartAirpport = outBoundFlightSegments.get(0).getFromAirport();
		String journeyEndAirport = outBoundFlightSegments.get(outBoundFlightSegments.size() - 1).getToAirport();
		if (inBoundFlightSegments.size() > 0) {
			journeyEndDate = inBoundFlightSegments.get(inBoundFlightSegments.size() - 1).getArrivalDateTime();
			isReturnTrip = true;
			if (!journeyEndAirport.equals(inBoundFlightSegments.get(inBoundFlightSegments.size() - 1).getFromAirport())
					&& journeyStartAirpport.equals(inBoundFlightSegments.get(inBoundFlightSegments.size() - 1).getToAirport())) {
				journeyEndAirport = inBoundFlightSegments.get(inBoundFlightSegments.size() - 1).getToAirport();
			}
		} else {
			journeyEndDate = outBoundFlightSegments.get(outBoundFlightSegments.size() - 1).getArrivalDateTime();
		}

		// pax counts
		int adults = 0;
		int children = 0;
		int infants = 0;

		ReservationPax reservationPax = null;
		for (Iterator iterPaxs = res.getPassengers().iterator(); iterPaxs.hasNext();) {
			reservationPax = (ReservationPax) iterPaxs.next();
			if (reservationPax.getPaxType().equals(ReservationInternalConstants.PassengerType.ADULT)) {
				adults++;
			}
			if (reservationPax.getPaxType().equals(ReservationInternalConstants.PassengerType.CHILD)) {
				children++;
			}
			if (reservationPax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {
				infants++;
			}
		}

		int totalNoOfAdultsAndChildren = adults + children;

		if (!(journeyStartDate.equals(insurance.getDateOfTravel()) && journeyEndDate.equals(insurance.getDateOfReturn())
				&& journeyStartAirpport.equals(insurance.getOrigin()) && journeyEndAirport.equals(insurance.getDestination())
				&& isReturnTrip == (insurance.getRoundTrip().equals("Y") ? true : false) && totalNoOfAdultsAndChildren == insurance
					.getTotalPaxCount())) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_OR_INSUFFICIENT_DETAILS,
					"Invalid insurance quotation in the price quote stage.Not matching with the journey requirements");
		}
	}

	/**
	 * 
	 * @param flightSegmentTOs
	 * @param roundTrip
	 * @param isRakinsurance
	 * @return
	 */
	public static LCCInsuredJourneyDTO getInsuranceJourneyDetails(FlightPriceRS flightPriceRS) {

		boolean roundTrip = false;
		IFlightSegment startSeg = null;
		IFlightSegment endSeg = null;

		String endAirportCode = null;

		List<FlightSegmentTO> outboundFlightSegmentTOs = CommonServicesUtil.getFlightSegmentDetails(flightPriceRS, false);
		List<FlightSegmentTO> inboundFlightSegmentTOs = CommonServicesUtil.getFlightSegmentDetails(flightPriceRS, true);

		startSeg = BeanUtils.getFirstElement(outboundFlightSegmentTOs);
		endSeg = BeanUtils.getFirstElement(inboundFlightSegmentTOs);

		IFlightSegment inboundFlightSegmentTO = inboundFlightSegmentTOs.isEmpty() ? null : BeanUtils.getElementByIndex(
				inboundFlightSegmentTOs, inboundFlightSegmentTOs.size() - 1);

		if (inboundFlightSegmentTO == null) {
			if (outboundFlightSegmentTOs.size() > 1) {
				endSeg = BeanUtils.getElementByIndex(outboundFlightSegmentTOs, outboundFlightSegmentTOs.size() - 1);
			}

			endAirportCode = endSeg.getSegmentCode().split("/")[endSeg.getSegmentCode().split("/").length - 1];
		} else {

			endAirportCode = inboundFlightSegmentTOs.get(0).getSegmentCode().split("/")[0];
			endSeg = BeanUtils.getElementByIndex(inboundFlightSegmentTOs, inboundFlightSegmentTOs.size() - 1);
			roundTrip = true;
		}

		String sOffset = getOffset(startSeg.getDepartureDateTime().getTime() - startSeg.getDepartureDateTimeZulu().getTime());
		String eOffset = getOffset(endSeg.getArrivalDateTime().getTime() - endSeg.getArrivalDateTimeZulu().getTime());

		LCCInsuredJourneyDTO jrny1 = new LCCInsuredJourneyDTO();
		jrny1.setJourneyStartAirportCode(startSeg.getSegmentCode().split("/")[0]);
		jrny1.setJourneyEndAirportCode(endAirportCode);
		jrny1.setJourneyStartDate(startSeg.getDepartureDateTime());
		jrny1.setJourneyStartDateOffset(sOffset);
		jrny1.setJourneyEndDate(endSeg.getArrivalDateTime());
		jrny1.setJourneyEndDateOffset(eOffset);
		jrny1.setRoundTrip(roundTrip);
		return jrny1;
	}

	private static String getOffset(long time) {
		StringBuilder sb = new StringBuilder();
		if (time >= 0) {
			sb.append("+");
		} else {
			sb.append("-");
			time *= -1;
		}
		sb.append(BeanUtils.parseDurationFormat(time, "HH:mm"));
		return sb.toString();
	}

	/**
	 * 
	 * @param flightPriceRQ
	 * @return
	 */
	public static List<LCCInsuredPassengerDTO> getLCCInsuredPassengerDTOs(FlightPriceRQ flightPriceRQ) {
		List<LCCInsuredPassengerDTO> insuredPassengerDTOs = new ArrayList<LCCInsuredPassengerDTO>();

		OTAPaxCountTO otaPaxCountTO = new OTAPaxCountTO(flightPriceRQ.getTravelerInfoSummary());

		for (int i = 0; i < otaPaxCountTO.getAdultCount(); i++) {
			insuredPassengerDTOs.add(new LCCInsuredPassengerDTO());
		}
		return insuredPassengerDTOs;
	}
}
