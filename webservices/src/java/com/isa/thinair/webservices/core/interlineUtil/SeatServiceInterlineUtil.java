package com.isa.thinair.webservices.core.interlineUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirPriceRQ;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.TravelerInformationType;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirColumnGroupDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirRowDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirRowGroupDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCCabinClassDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSeatMapDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSeatMapDetailDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSegmentSeatMapDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.util.BaseUtil;
import com.isa.thinair.webservices.core.util.CommonServicesUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

public class SeatServiceInterlineUtil extends BaseUtil {

	private static final Log log = LogFactory.getLog(SeatServiceInterlineUtil.class);

	/**
	 * 
	 * @param flightDetailsAndSeatMap
	 * @param flightPriceRS
	 * @param system
	 * @return
	 * @throws ModuleException
	 */
	public static Map<String, Map<String, LCCAirSeatDTO>> getSelectedSeatsDetails(
			Map<String, Map<String, String>> flightDetailsAndSeatMap, FlightPriceRS flightPriceRS, SYSTEM system)
			throws ModuleException {
		// key|value - travelerRPH|Map<String, LCCAirSeatDTO>
		Map<String, Map<String, LCCAirSeatDTO>> segmentAndSeatsMap = new HashMap<String, Map<String, LCCAirSeatDTO>>();

		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();

		Map<Integer, FlightSegmentTO> flightSegmentMap = CommonServicesUtil.getFlightSegmentDetails(flightPriceRS);

		if (flightSegmentMap != null && !flightSegmentMap.isEmpty()) {
			flightSegmentTOs.addAll(flightSegmentMap.values());
		}

		List<BundledFareDTO> bundledFareDTOs = null;
		if (flightPriceRS != null && flightPriceRS.getSelectedPriceFlightInfo() != null
				&& flightPriceRS.getSelectedPriceFlightInfo().getFareTypeTO() != null) {
			bundledFareDTOs = flightPriceRS.getSelectedPriceFlightInfo().getFareTypeTO().getOndBundledFareDTOs();
		}

		LCCSeatMapDTO inLCCSeatMapDTO = new LCCSeatMapDTO();
		inLCCSeatMapDTO.setFlightDetails(flightSegmentTOs);
		inLCCSeatMapDTO.setQueryingSystem(system);
		inLCCSeatMapDTO.setBundledFareDTOs(bundledFareDTOs);

		LCCSeatMapDTO outSeatMapDTO = WebServicesModuleUtils.getAirproxyAncillaryBD().getSeatMap(inLCCSeatMapDTO, null, null,
				null);

		for (String travelerRef : flightDetailsAndSeatMap.keySet()) {

			// key | value - flight RPH | seat number
			Map<String, String> flightDetailsAndSeat = flightDetailsAndSeatMap.get(travelerRef);

			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {

				for (String flightRPH : flightDetailsAndSeat.keySet()) {
					String seatNo = flightDetailsAndSeat.get(flightRPH);
					// validating against the flight RPH
					if (flightRPH.equals(flightSegmentTO.getFlightRefNumber())) {
						List<LCCAirSeatDTO> airSeatDTOs = getLCCSeatsDTOsByRPH(flightRPH, outSeatMapDTO);
						for (LCCAirSeatDTO lccAirSeatDTO : airSeatDTOs) {
							if (lccAirSeatDTO.getSeatNumber().equals(seatNo)) {

								if (lccAirSeatDTO.getSeatType() != null && lccAirSeatDTO.getSeatType().equalsIgnoreCase("EXIT")) {
									lccAirSeatDTO.getNotAllowedPassengerType().add(PaxTypeTO.INFANT);
									lccAirSeatDTO.getNotAllowedPassengerType().add(PaxTypeTO.CHILD);
								}

								if (segmentAndSeatsMap.get(travelerRef) == null) {
									segmentAndSeatsMap.put(travelerRef, new HashMap<String, LCCAirSeatDTO>());
								}
								segmentAndSeatsMap.get(travelerRef).put(flightRPH, lccAirSeatDTO);
							}
						}
					}
				}
			}
		}

		return segmentAndSeatsMap;
	}

	/**
	 * 
	 * @param flightSegId
	 * @param lccSeatMapDTO
	 * @return
	 */
	private static List<LCCAirSeatDTO> getLCCSeatsDTOsByRPH(String flightRPH, LCCSeatMapDTO lccSeatMapDTO) {
		List<LCCAirSeatDTO> airSeatDTOs = new ArrayList<LCCAirSeatDTO>();

		if (lccSeatMapDTO != null) {
			for (LCCSegmentSeatMapDTO segmentSeatMapDTO : lccSeatMapDTO.getLccSegmentSeatMapDTOs()) {
				FlightSegmentTO flightSegmentTO = segmentSeatMapDTO.getFlightSegmentDTO();

				if (flightSegmentTO.getFlightRefNumber().equals(flightRPH)) {

					LCCSeatMapDetailDTO lccSeatMapDetailDTO = segmentSeatMapDTO.getLccSeatMapDetailDTO();

					for (LCCCabinClassDTO lccCabinClassDTO : lccSeatMapDetailDTO.getCabinClass()) {
						for (LCCAirRowGroupDTO lccAirRowGroupDTO : lccCabinClassDTO.getAirRowGroupDTOs()) {
							for (LCCAirColumnGroupDTO lccAirColumnGroupDTO : lccAirRowGroupDTO.getAirColumnGroups()) {
								for (LCCAirRowDTO lccAirRowDTO : lccAirColumnGroupDTO.getAirRows()) {
									airSeatDTOs.addAll(lccAirRowDTO.getAirSeats());
								}
							}
						}
					}

					break;
				}
			}
		}

		return airSeatDTOs;
	}

	/**
	 * 
	 * @param otaAirPriceRQ
	 * @param seatDetailsMap
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	public static void
			validateLCCSeatRequest(OTAAirPriceRQ otaAirPriceRQ, Map<String, Map<String, LCCAirSeatDTO>> seatDetailsMap)
					throws WebservicesException, ModuleException {

		// to get the adults traveling with infants
		int infants = 0;
		for (TravelerInformationType travelerInformationType : otaAirPriceRQ.getTravelerInfoSummary().getAirTravelerAvail()) {
			for (PassengerTypeQuantityType passengerTypeQuantityType : travelerInformationType.getPassengerTypeQuantity()) {
				if (passengerTypeQuantityType.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
					infants += passengerTypeQuantityType.getQuantity();
				}
			}
		}

		List<String> adultsTravellingWithInfants = new ArrayList<String>();

		for (int i = 0; i < infants; ++i) {
			adultsTravellingWithInfants.add("A" + (i + 1));
		}

		for (String travelerRef : seatDetailsMap.keySet()) {
			Map<String, LCCAirSeatDTO> seatsMap = seatDetailsMap.get(travelerRef);
			for (String flightRPH : seatsMap.keySet()) {
				LCCAirSeatDTO seatDTO = seatsMap.get(flightRPH);
				// validate the seat availability
				if (!seatDTO.getSeatAvailability().equals("VAC")) {
					throw new WebservicesException(IOTACodeTables.AirSeatType_AST_NO_SEAT_AT_THIS_LOCATION,
							"Seat is not available at the moment");
				} else if (seatDTO.getNotAllowedPassengerType() != null && seatDTO.getNotAllowedPassengerType().size() > 0) {
					// validating not allowed passenger types
					if (travelerRef.startsWith("A") && seatDTO.getNotAllowedPassengerType().contains(PaxTypeTO.ADULT)) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PASSENGER_TYPE_NOT_SUPPORTED_,
								"Not allowed seat for adult");
					} else if (travelerRef.startsWith("C") && seatDTO.getNotAllowedPassengerType().contains(PaxTypeTO.CHILD)) {
						throw new WebservicesException(IOTACodeTables.AirSeatType_AST_NOT_ALLOWED_FOR_UNACCOMPANIED_MINORS,
								"Not allowed seat for Child");
					} else if (travelerRef.startsWith("A")) {
						if (adultsTravellingWithInfants.contains(travelerRef)
								&& seatDTO.getNotAllowedPassengerType().contains(PaxTypeTO.INFANT)) {
							throw new WebservicesException(IOTACodeTables.AirSeatType_AST_NOT_ALLOWED_FOR_INFANTS,
									"Not allowed seat for adult travelling with infant");
						}
					}
				}
			}
		}
	}

	/**
	 * 
	 * @param seatDetailsMap
	 * @return
	 * @throws ModuleException
	 */
	public static Map<String, List<LCCClientExternalChgDTO>> getExternalChargesMapForLCCSeats(
			Map<String, Map<String, LCCAirSeatDTO>> seatDetailsMap) throws ModuleException {

		Map<String, List<LCCClientExternalChgDTO>> externalCharegs = new HashMap<String, List<LCCClientExternalChgDTO>>();

		// creating the seat map related external charges against traveler RPH
		for (String travelerRef : seatDetailsMap.keySet()) {
			List<LCCClientExternalChgDTO> charegsList = new ArrayList<LCCClientExternalChgDTO>();
			Map<String, LCCAirSeatDTO> seatsMap = seatDetailsMap.get(travelerRef);
			for (String flightRPH : seatsMap.keySet()) {
				LCCAirSeatDTO airSeatDTO = seatsMap.get(flightRPH);

				LCCClientExternalChgDTO externalChargeTO = new LCCClientExternalChgDTO();

				externalChargeTO.setAmount(airSeatDTO.getSeatCharge());
				externalChargeTO.setCode(airSeatDTO.getSeatNumber());
				externalChargeTO.setExternalCharges(EXTERNAL_CHARGES.SEAT_MAP);
				externalChargeTO.setFlightRefNumber(flightRPH);

				charegsList.add(externalChargeTO);
			}

			externalCharegs.put(travelerRef, charegsList);

		}

		return externalCharegs;
	}

}
