package com.isa.thinair.webservices.core.service;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webservices.api.service.AAResWebServices;

/**
 * @author Nilindra Fernando
 */
public class AAResWebServicesFactory {

	public static AAResWebServices createAAResWebServices() {
		if (AppSysParamsUtil.isEnableInterlineSupportForOTAWebServices()) {
			return new AAResInterlineWebServicesImpl();
		} else {
			return new AAResWebServicesImpl();
		}
	}
}