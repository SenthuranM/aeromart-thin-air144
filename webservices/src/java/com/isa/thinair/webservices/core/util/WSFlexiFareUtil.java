package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AirFeeType;
import org.opentravel.ota._2003._05.AirItineraryPricingInfoType;
import org.opentravel.ota._2003._05.AirTripType;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.FlexiFareType;
import org.opentravel.ota._2003._05.FlexiFareType.PerPaxFlexifareBDS;
import org.opentravel.ota._2003._05.FlexiOperationsType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.PerPaxFlexifareBDType;

import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.dto.FlexiRuleDTO;
import com.isa.thinair.airpricing.api.dto.FlexiRuleFlexibilityDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.ExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlexiInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ONDExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndFareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.flexi.FlexiExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndFlexibility;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webservices.api.dtos.FlexiFareSelectionCriteria;
import com.isa.thinair.webservices.api.dtos.OTAPaxCountTO;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.interlineUtil.PriceQuoteInterlineUtil;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;

/**
 * Flexi Fare related functionality for WS module. 1. Creates flexi fare display information. 2. Price quote related
 * flexi fare methods.
 * 
 * @author sanjaya
 */
public class WSFlexiFareUtil {

	/** Logger */
	private static final Log log = LogFactory.getLog(WSFlexiFareUtil.class);

	/**
	 * Creates the flexi fare display objects to be used in the availablity search.
	 * 
	 * @param airItineraryPricingInfo
	 * @param selectedFlightDTO
	 *            : The selected flight DTO from the price quote.
	 * @param availableFlightSearchDTO
	 *            : The availability search object.
	 * @param currencyCode
	 *            : Currency code of the request.
	 * @param isFixedFare
	 *            : Is fixed fares quote.
	 * 
	 * @throws ModuleException
	 */
	public static void prepareFlexiFareDisplayObjects(AirItineraryPricingInfoType airItineraryPricingInfo,
			SelectedFlightDTO selectedFlightDTO, AvailableFlightSearchDTO availableFlightSearchDTO, String currencyCode,
			ExchangeRateProxy exchangeRateProxy) throws ModuleException {

		Map<AirTripType, Collection<OndFareDTO>> ondFareDTOswithFlexi = getOnDFareDTOsWithFlexi(selectedFlightDTO);
		AirItineraryPricingInfoType.AvailableFlexiFares availableFlexiFares = new AirItineraryPricingInfoType.AvailableFlexiFares();

		boolean outBoundFlexiFaresAvailable = false;
		boolean inBoundFlexiFaresAvailable = false;

		if (ondFareDTOswithFlexi.get(AirTripType.OUTBOUND) != null && ondFareDTOswithFlexi.get(AirTripType.OUTBOUND).size() > 0) {
			availableFlexiFares.setOutBoundFlexiAvailable(true);
			availableFlexiFares.getFlexiFare().add(
					buildFlexiFare(AirTripType.OUTBOUND, ondFareDTOswithFlexi.get(AirTripType.OUTBOUND),
							availableFlightSearchDTO, currencyCode, exchangeRateProxy));
			outBoundFlexiFaresAvailable = true;
		}

		if (ondFareDTOswithFlexi.get(AirTripType.INBOUND) != null && ondFareDTOswithFlexi.get(AirTripType.INBOUND).size() > 0) {
			availableFlexiFares.setInBoundFlexiAvailable(true);
			availableFlexiFares.getFlexiFare().add(
					buildFlexiFare(AirTripType.INBOUND, ondFareDTOswithFlexi.get(AirTripType.INBOUND), availableFlightSearchDTO,
							currencyCode, exchangeRateProxy));
			inBoundFlexiFaresAvailable = true;
		}

		if (inBoundFlexiFaresAvailable || outBoundFlexiFaresAvailable) {
			airItineraryPricingInfo.setAvailableFlexiFares(availableFlexiFares);
		}
	}

	/**
	 * Creates the flexi fare display objects to be used in the availablity search.
	 * 
	 * @param airItineraryPricingInfo
	 * @param fareTypeTO
	 * @param flightAvailRQ
	 * @param currencyCode
	 * @throws ModuleException
	 */
	public static void prepareFlexiFareDisplayObjects(AirItineraryPricingInfoType airItineraryPricingInfo,
			PriceInfoTO priceInfoTO, OTAPaxCountTO otaPaxCountTO, String currencyCode, ExchangeRateProxy exchangeRateProxy)
			throws ModuleException {

		Collection<ExternalChargeTO> externalChargeTOs = new ArrayList<ExternalChargeTO>();

		for (PerPaxPriceInfoTO perPaxPriceInfoTO : priceInfoTO.getPerPaxPriceInfo()) {
			FareTypeTO passengerPrice = perPaxPriceInfoTO.getPassengerPrice();

			if (passengerPrice.getOndExternalCharges() != null) {
				for (ONDExternalChargeTO ondExternalChargeTO : passengerPrice.getOndExternalCharges()) {
					if (ondExternalChargeTO.getExternalCharges() != null && ondExternalChargeTO.getExternalCharges().size() > 0) {
						externalChargeTOs.addAll(ondExternalChargeTO.getExternalCharges());
					}
				}
			}
		}

		Map<AirTripType, Collection<ExternalChargeTO>> externalChargeTOswithFlexi = getExternalChargeTOWithFlexi(
				externalChargeTOs, priceInfoTO.getFareSegChargeTO().getOndFareSegChargeTOs());

		AirItineraryPricingInfoType.AvailableFlexiFares availableFlexiFares = new AirItineraryPricingInfoType.AvailableFlexiFares();

		boolean outBoundFlexiFaresAvailable = false;
		boolean inBoundFlexiFaresAvailable = false;

		if (externalChargeTOswithFlexi.get(AirTripType.OUTBOUND) != null
				&& !externalChargeTOswithFlexi.get(AirTripType.OUTBOUND).isEmpty()) {
			availableFlexiFares.setOutBoundFlexiAvailable(true);
			availableFlexiFares.getFlexiFare().add(
					buildFlexiFare(AirTripType.OUTBOUND, externalChargeTOswithFlexi.get(AirTripType.OUTBOUND), otaPaxCountTO,
							currencyCode, priceInfoTO, exchangeRateProxy));
			outBoundFlexiFaresAvailable = true;
		}

		if (externalChargeTOswithFlexi.get(AirTripType.INBOUND) != null
				&& !externalChargeTOswithFlexi.get(AirTripType.INBOUND).isEmpty()) {
			availableFlexiFares.setInBoundFlexiAvailable(true);
			availableFlexiFares.getFlexiFare().add(
					buildFlexiFare(AirTripType.INBOUND, externalChargeTOswithFlexi.get(AirTripType.INBOUND), otaPaxCountTO,
							currencyCode, priceInfoTO, exchangeRateProxy));
			inBoundFlexiFaresAvailable = true;
		}

		if (inBoundFlexiFaresAvailable || outBoundFlexiFaresAvailable) {
			airItineraryPricingInfo.setAvailableFlexiFares(availableFlexiFares);
		}
	}

	/**
	 * Gets the {@link OndFareDTO} list with {@link AirTripType} which includes flexible fares.
	 * 
	 * @param selectedFlightDTO
	 * @param isFixedFare
	 * @return
	 */
	public static Map<AirTripType, Collection<OndFareDTO>> getOnDFareDTOsWithFlexi(SelectedFlightDTO selectedFlightDTO) {
		return getOnDFareDTOsWithFlexi(selectedFlightDTO.getOndFareDTOs(false));
	}

	/**
	 * Returns a Map containing {@link OndFareDTO} list for the selected flight. NOTE: If any of the OndFareDTOs does
	 * not contain flexi fares for a given journey type, all the other flexi fares for that particular journey type
	 * would be ignored.
	 * 
	 * @param selectedFlightDTO
	 * @param isFixedFare
	 * 
	 * @return : The map of {@link OndFareDTO} grouped by the trip type (inbout/outbound)
	 */
	public static Map<AirTripType, Collection<OndFareDTO>> getOnDFareDTOsWithFlexi(Collection<OndFareDTO> ondFareDTOs) {

		Map<AirTripType, Collection<OndFareDTO>> ondFareDTOsWithFlexiFares = new HashMap<AirTripType, Collection<OndFareDTO>>();

		Collection<OndFareDTO> inBoundOndFareDTOsWithFlexi = new ArrayList<OndFareDTO>();
		Collection<OndFareDTO> outBoundOndFareDTOsWithFlexi = new ArrayList<OndFareDTO>();

		boolean inBoundFlexiFaresAvailable = true;
		boolean outBoundFlexiFaresAvailable = true;

		for (OndFareDTO ondFareDTO : ondFareDTOs) {
			if (ondFareDTO.isInBoundOnd() == false) {
				// Note : if any of the outbound OnDFareDTO does not have flexi fares, we consider outbound flexi as not
				// available.
				if (ondFareDTO.getAllFlexiCharges() != null && ondFareDTO.getAllFlexiCharges().size() > 0) {
					outBoundFlexiFaresAvailable = outBoundFlexiFaresAvailable && true;
					outBoundOndFareDTOsWithFlexi.add(ondFareDTO);
				} else {
					outBoundFlexiFaresAvailable = outBoundFlexiFaresAvailable && false;
				}
			} else if (ondFareDTO.isInBoundOnd() == true) {
				// Note : if any of the inbound OnDFareDTO does not have flexi fares, we consider in-bound flexi as not
				// available.
				if (ondFareDTO.getAllFlexiCharges() != null && ondFareDTO.getAllFlexiCharges().size() > 0) {
					inBoundFlexiFaresAvailable = inBoundFlexiFaresAvailable && true;
					inBoundOndFareDTOsWithFlexi.add(ondFareDTO);
				} else {
					inBoundFlexiFaresAvailable = inBoundFlexiFaresAvailable && false;
				}
			}
		}

		if (outBoundFlexiFaresAvailable) {
			ondFareDTOsWithFlexiFares.put(AirTripType.OUTBOUND, outBoundOndFareDTOsWithFlexi);
		}

		if (inBoundFlexiFaresAvailable) {
			ondFareDTOsWithFlexiFares.put(AirTripType.INBOUND, inBoundOndFareDTOsWithFlexi);
		}

		return ondFareDTOsWithFlexiFares;
	}

	/**
	 * Returns a Map containing {@link ExternalChargeTO} list for the selected flight.
	 * 
	 * @param externalChargeTOs
	 * @param ondFareSegChargeTOs
	 * @return
	 */
	public static Map<AirTripType, Collection<ExternalChargeTO>> getExternalChargeTOWithFlexi(
			Collection<ExternalChargeTO> externalChargeTOs, Collection<OndFareSegChargeTO> ondFareSegChargeTOs) {

		Map<AirTripType, Collection<ExternalChargeTO>> ondFareDTOsWithFlexiFares = new HashMap<AirTripType, Collection<ExternalChargeTO>>();

		Collection<ExternalChargeTO> inBoundExternalChargeTOsWithFlexi = new ArrayList<ExternalChargeTO>();
		Collection<ExternalChargeTO> outBoundExternalChargeTOsWithFlexi = new ArrayList<ExternalChargeTO>();

		boolean inBoundFlexiFaresAvailable = false;
		boolean outBoundFlexiFaresAvailable = false;

		for (ExternalChargeTO externalChargeTO : externalChargeTOs) {
			if (externalChargeTO.getType().equals(EXTERNAL_CHARGES.FLEXI_CHARGES)) {
				for (OndFareSegChargeTO ondFareSegChargeTO : ondFareSegChargeTOs) {
					if (ondFareSegChargeTO.getOndSequence() == externalChargeTO.getOndSequence()
							&& ondFareSegChargeTO.isInbound()) {
						inBoundFlexiFaresAvailable = true;
						inBoundExternalChargeTOsWithFlexi.add(externalChargeTO);
						break;
					} else if (ondFareSegChargeTO.getOndSequence() == externalChargeTO.getOndSequence()
							&& !ondFareSegChargeTO.isInbound()) {
						outBoundFlexiFaresAvailable = true;
						outBoundExternalChargeTOsWithFlexi.add(externalChargeTO);
						break;
					}
				}
			}
		}

		if (outBoundFlexiFaresAvailable) {
			ondFareDTOsWithFlexiFares.put(AirTripType.OUTBOUND, outBoundExternalChargeTOsWithFlexi);
		}

		if (inBoundFlexiFaresAvailable) {
			ondFareDTOsWithFlexiFares.put(AirTripType.INBOUND, inBoundExternalChargeTOsWithFlexi);
		}

		return ondFareDTOsWithFlexiFares;
	}

	/**
	 * Builds the {@link FlexiFareType} object to be displayed.
	 * 
	 * @param tripType
	 *            : The AirTripType (Inbound or OutBound flexi)
	 * @param ondFareDTOs
	 *            : The collection of OnDFareDTOs related to the trip type
	 * @param availableFlightSearchDTO
	 *            : The available flight search dto
	 * @param currencyCode
	 *            : The currency code.
	 * @return : The display object including flexible fares avaialble.
	 * @throws ModuleException
	 */
	public static FlexiFareType buildFlexiFare(AirTripType tripType, Collection<OndFareDTO> ondFareDTOs,
			AvailableFlightSearchDTO availableFlightSearchDTO, String currencyCode, ExchangeRateProxy exchangeRateProxy)
			throws ModuleException {

		boolean showPriceInselectedCurrency = OTAUtils.isShowPriceInselectedCurrency();

		FlexiFareType flexiFare = new FlexiFareType();
		flexiFare.setApplicableJourneyType(tripType);
		BigDecimal totalFlexiFare = AccelAeroCalculator.getDefaultBigDecimalZero();

		// This is to find the effective flexi rule.
		OndFareDTO firstDepartureOndFare = null;
		for (OndFareDTO ondFareDTO : ondFareDTOs) {

			if (firstDepartureOndFare == null
					|| firstDepartureOndFare.getOndDepartureDate().after(ondFareDTO.getOndDepartureDate())) {
				firstDepartureOndFare = ondFareDTO;
			}
		}

		// Get the flexi related details from the effective flexi rule.
		FlexiRuleDTO effectiveFlexiRule = null;
		for (FlexiRuleDTO flexiRule : firstDepartureOndFare.getAllFlexiCharges()) {
			// currently we have no logic to get the effective flexi rule from multiple rules.
			// this is no problem at the moment we have only one flexi rule for a given way.
			effectiveFlexiRule = flexiRule;
			break;
		}

		if (effectiveFlexiRule != null) {
			flexiFare.setFlexiRuleCode(effectiveFlexiRule.getFlexiCode());
			flexiFare.setFlexibilityDescription(effectiveFlexiRule.getDescription());
			flexiFare.setFlexiRuleComment(effectiveFlexiRule.getRuleComments());

			FlexiFareType.AllowedFlexiOperations allowedOperations = new FlexiFareType.AllowedFlexiOperations();
			Collection<FlexiRuleFlexibilityDTO> effectiveFlexibilites = effectiveFlexiRule.getAvailableFlexibilities();
			for (FlexiRuleFlexibilityDTO flexibility : effectiveFlexibilites) {
				FlexiOperationsType operationType = new FlexiOperationsType();
				operationType.setAllowedOperationName(flexibility.getFlexibilityDescription());

				Integer availableCount = new Integer(flexibility.getAvailableCount());
				operationType.setNumberOfAllowedOperations(new BigInteger(availableCount.toString()));

				Long cutoverTime = new Long(flexibility.getCutOverBufferInMins());
				operationType.setFlexiOperationCutoverTimeInMinutes(new BigInteger(cutoverTime.toString()));

				allowedOperations.getFlexiOperations().add(operationType);
			}

			flexiFare.setAllowedFlexiOperations(allowedOperations);
		}

		// Get the per pax type flexi amounts for the list of considered ondFareDTOs.
		Map<String, BigDecimal> perPaxFlexiAmounts = getPerPaxFlexiAmounts(ondFareDTOs);

		BigDecimal totalPerAdultFlexiAmount = perPaxFlexiAmounts.get(PaxTypeTO.ADULT);
		BigDecimal totalPerChildFlexiAmount = perPaxFlexiAmounts.get(PaxTypeTO.CHILD);
		BigDecimal totalPerInfantFlexiAmount = perPaxFlexiAmounts.get(PaxTypeTO.INFANT);

		flexiFare.setPerPaxFlexifareBDS(getPerPaxFlexiChargeBD(totalPerAdultFlexiAmount, totalPerChildFlexiAmount,
				totalPerInfantFlexiAmount, currencyCode, exchangeRateProxy));

		totalFlexiFare = AccelAeroCalculator.add(
				AccelAeroCalculator.multiply(totalPerAdultFlexiAmount, availableFlightSearchDTO.getAdultCount()),
				AccelAeroCalculator.multiply(totalPerChildFlexiAmount, availableFlightSearchDTO.getChildCount()),
				AccelAeroCalculator.multiply(totalPerInfantFlexiAmount, availableFlightSearchDTO.getInfantCount()));

		FlexiFareType.FlexiFareAmount totalFlexiAmount = new FlexiFareType.FlexiFareAmount();
		if (showPriceInselectedCurrency && currencyCode != null) {
			BigDecimal totalFlexiFareInPreferredCurreny = WSReservationUtil.getAmountInSpecifiedCurrency(totalFlexiFare,
					currencyCode, exchangeRateProxy);
			OTAUtils.setAmountAndCurrency(totalFlexiAmount, totalFlexiFareInPreferredCurreny, currencyCode);
		} else {
			OTAUtils.setAmountAndDefaultCurrency(totalFlexiAmount, totalFlexiFare);
		}

		flexiFare.setFlexiFareAmount(totalFlexiAmount);

		return flexiFare;
	}

	/**
	 * we will get only the flexi charge collection here
	 * 
	 * @param tripType
	 * @param externalChargeTOs
	 * @param otaPaxCountTO
	 * @param currencyCode
	 * @return
	 * @throws ModuleException
	 */
	public static FlexiFareType buildFlexiFare(AirTripType tripType, Collection<ExternalChargeTO> externalChargeTOs,
			OTAPaxCountTO otaPaxCountTO, String currencyCode, PriceInfoTO priceInfoTO, ExchangeRateProxy exchangeRateProxy)
			throws ModuleException {

		boolean showPriceInselectedCurrency = OTAUtils.isShowPriceInselectedCurrency();

		FlexiFareType flexiFare = new FlexiFareType();
		flexiFare.setApplicableJourneyType(tripType);
		BigDecimal totalFlexiFare = AccelAeroCalculator.getDefaultBigDecimalZero();

		// Get the flexi related details from the effective flexi rule.
		ExternalChargeTO effectiveFlexiCharge = null;
		for (ExternalChargeTO flexiCharge : externalChargeTOs) {
			// currently we have no logic to get the effective flexi rule from multiple rules.
			// this is no problem at the moment we have only one flexi rule for a given way.
			effectiveFlexiCharge = flexiCharge;
			break;
		}

		if (effectiveFlexiCharge != null) {
			flexiFare.setFlexiRuleCode(effectiveFlexiCharge.getSurchargeCode());

			flexiFare.setFlexibilityDescription(effectiveFlexiCharge.getSurchargeName());
			flexiFare.setFlexiRuleComment(effectiveFlexiCharge.getComments());

			FlexiFareType.AllowedFlexiOperations allowedOperations = new FlexiFareType.AllowedFlexiOperations();
			Collection<FlexiInfoTO> effectiveFlexibilites = effectiveFlexiCharge.getAdditionalDetails();
			for (FlexiInfoTO flexibility : effectiveFlexibilites) {
				FlexiOperationsType operationType = new FlexiOperationsType();
				operationType.setAllowedOperationName(getFlexiMessages(flexibility.getFlexibilityTypeId(),
						flexibility.getAvailableCount(), flexibility.getCutOverBufferInMins()));

				Integer availableCount = new Integer(flexibility.getAvailableCount());
				operationType.setNumberOfAllowedOperations(new BigInteger(availableCount.toString()));

				Long cutoverTime = new Long(flexibility.getCutOverBufferInMins());
				operationType.setFlexiOperationCutoverTimeInMinutes(new BigInteger(cutoverTime.toString()));

				allowedOperations.getFlexiOperations().add(operationType);
			}

			flexiFare.setAllowedFlexiOperations(allowedOperations);
		}

		BigDecimal totalAdultFlexiAmount = getPaxTotalFlexiAmounts(externalChargeTOs, PaxTypeTO.ADULT);
		BigDecimal totalChildFlexiAmount = getPaxTotalFlexiAmounts(externalChargeTOs, PaxTypeTO.CHILD);
		BigDecimal totalInfantFlexiAmount = getPaxTotalFlexiAmounts(externalChargeTOs, PaxTypeTO.INFANT);
		
		BigDecimal totalPerAdultFlexiAmount = getPerPaxFlexiAmount(totalAdultFlexiAmount, otaPaxCountTO.getAdultCount());
		BigDecimal totalPerChildFlexiAmount = getPerPaxFlexiAmount(totalChildFlexiAmount, otaPaxCountTO.getChildCount());
		BigDecimal totalPerInfantFlexiAmount = getPerPaxFlexiAmount(totalInfantFlexiAmount, otaPaxCountTO.getInfantCount());

		flexiFare.setPerPaxFlexifareBDS(getPerPaxFlexiChargeBD(totalPerAdultFlexiAmount, totalPerChildFlexiAmount,
				totalPerInfantFlexiAmount, currencyCode, exchangeRateProxy));

		totalFlexiFare = AccelAeroCalculator.add(totalAdultFlexiAmount, totalChildFlexiAmount, totalInfantFlexiAmount);

		FlexiFareType.FlexiFareAmount totalFlexiAmount = new FlexiFareType.FlexiFareAmount();
		if (showPriceInselectedCurrency && currencyCode != null) {
			BigDecimal totalFlexiFareInPreferredCurreny = WSReservationUtil.getAmountInSpecifiedCurrency(totalFlexiFare,
					currencyCode, exchangeRateProxy);
			OTAUtils.setAmountAndCurrency(totalFlexiAmount, totalFlexiFareInPreferredCurreny, currencyCode);
		} else {
			OTAUtils.setAmountAndDefaultCurrency(totalFlexiAmount, totalFlexiFare);
		}

		flexiFare.setFlexiFareAmount(totalFlexiAmount);

		return flexiFare;
	}

	/**
	 * 
	 * @param flexibilityId
	 * @param availableCount
	 * @param cutoverMins
	 * @return
	 */
	private static String getFlexiMessages(Integer flexibilityId, Integer availableCount, long cutoverMins) {
		String flexiMessage = "#1 #2(s) up to #3 hours before departure";

		flexiMessage = flexiMessage.replace("#1", String.valueOf(availableCount));
		flexiMessage = flexiMessage.replace("#3", String.valueOf((int) cutoverMins / 60));

		if (flexibilityId == 1) {// Modification
			flexiMessage = flexiMessage.replace("#2", "Modification");
		} else if (flexibilityId == 2) {// Cancellation
			flexiMessage = flexiMessage.replace("#2", "Cancellation");
		}

		return flexiMessage;
	}

	/**
	 * Calculates the total per-pax/per-pax-type flexi amounts for a given set of ondFareDTOs.
	 * 
	 * @param ondFareDTOs
	 * @return
	 */
	public static Map<String, BigDecimal> getPerPaxFlexiAmounts(Collection<OndFareDTO> ondFareDTOs) {

		Map<String, BigDecimal> perPaxFlexiAmounts = new HashMap<String, BigDecimal>();

		BigDecimal totalPerAdultFlexiAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalPerChildFlexiAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalPerInfantFlexiAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (OndFareDTO ondFareDTO : ondFareDTOs) {

			for (FlexiRuleDTO flexiRule : ondFareDTO.getAllFlexiCharges()) {
				totalPerAdultFlexiAmount = AccelAeroCalculator.add(totalPerAdultFlexiAmount, new BigDecimal(flexiRule
						.getEffectiveChargeAmount(PaxTypeTO.ADULT).doubleValue()));
				totalPerChildFlexiAmount = AccelAeroCalculator.add(totalPerChildFlexiAmount, new BigDecimal(flexiRule
						.getEffectiveChargeAmount(PaxTypeTO.CHILD).doubleValue()));
				totalPerInfantFlexiAmount = AccelAeroCalculator.add(totalPerInfantFlexiAmount, new BigDecimal(flexiRule
						.getEffectiveChargeAmount(PaxTypeTO.INFANT).doubleValue()));
			}
		}

		perPaxFlexiAmounts.put(PaxTypeTO.ADULT, totalPerAdultFlexiAmount);
		perPaxFlexiAmounts.put(PaxTypeTO.CHILD, totalPerChildFlexiAmount);
		perPaxFlexiAmounts.put(PaxTypeTO.INFANT, totalPerInfantFlexiAmount);

		return perPaxFlexiAmounts;
	}

	/**
	 * Calculates the total pax/pax-type flexi amounts for a given set of PriceInfoTO and pax type
	 * 
	 * @param PriceInfoTO
	 * @return
	 */
	public static BigDecimal getPaxTotalFlexiAmounts(Collection<ExternalChargeTO> externalChargeTOs, String paxType) {

		BigDecimal paxTotalFlexiAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		Collection<ExternalChargeTO> tmpExternalChargeTOs = new ArrayList<ExternalChargeTO>();

		for (ExternalChargeTO externalChargeTO : externalChargeTOs) {
			if (externalChargeTO.getApplicablePassengerTypeCode() != null
					&& externalChargeTO.getApplicablePassengerTypeCode().contains(paxType)) {
				tmpExternalChargeTOs.add(externalChargeTO);
			}
		}

		for (ExternalChargeTO chargeTO : tmpExternalChargeTOs) {
			if (EXTERNAL_CHARGES.FLEXI_CHARGES.equals(chargeTO.getType())) {
				paxTotalFlexiAmount = AccelAeroCalculator.add(paxTotalFlexiAmount, chargeTO.getAmount());
			}
		}

		return paxTotalFlexiAmount;
	}
	
	public static BigDecimal getPerPaxFlexiAmount(BigDecimal totalPerAdultFlexiAmount, int paxCount) {
		BigDecimal perPaxFlexiAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (paxCount > 0) {
			perPaxFlexiAmount = AccelAeroCalculator.divide(totalPerAdultFlexiAmount, paxCount);
		}

		return perPaxFlexiAmount;
	}

	/**
	 * Gets the Per Pax Flexi charge break down
	 * 
	 * @param totalPerAdultFlexiAmount
	 * @param totalPerChildFlexiAmount
	 * @param totalPerInfantFlexiAmount
	 * @param currencyCode
	 * @return
	 * @throws ModuleException
	 */
	public static PerPaxFlexifareBDS getPerPaxFlexiChargeBD(BigDecimal totalPerAdultFlexiAmount,
			BigDecimal totalPerChildFlexiAmount, BigDecimal totalPerInfantFlexiAmount, String currencyCode,
			ExchangeRateProxy exchangeRateProxy) throws ModuleException {
		PerPaxFlexifareBDS perPaxFlexiChargeBD = new PerPaxFlexifareBDS();
		boolean showPriceInselectedCurrency = OTAUtils.isShowPriceInselectedCurrency();

		// Adult BD

		if (totalPerAdultFlexiAmount.compareTo(BigDecimal.ZERO) > 0) {
			PerPaxFlexifareBDType perPaxFlexiBDForAdult = new PerPaxFlexifareBDType();
			PerPaxFlexifareBDType.PerPaxFlexiFareAmount perPaxFlexiBDAmountForAdult = new PerPaxFlexifareBDType.PerPaxFlexiFareAmount();
			perPaxFlexiBDAmountForAdult.setApplicablePaxType(CommonUtil
					.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
			if (showPriceInselectedCurrency && currencyCode != null) {
				BigDecimal perPaxFlexiAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(
						totalPerAdultFlexiAmount, currencyCode, exchangeRateProxy);
				OTAUtils.setAmountAndCurrency(perPaxFlexiBDAmountForAdult, perPaxFlexiAmountInPreferredCurrency, currencyCode);
			} else {
				OTAUtils.setAmountAndDefaultCurrency(perPaxFlexiBDAmountForAdult, totalPerAdultFlexiAmount);
			}
			perPaxFlexiBDForAdult.setPerPaxFlexiFareAmount(perPaxFlexiBDAmountForAdult);
			perPaxFlexiChargeBD.getPerPaxFlexifareBD().add(perPaxFlexiBDForAdult);
		}

		// For Child
		if (totalPerChildFlexiAmount.compareTo(BigDecimal.ZERO) > 0) {
			PerPaxFlexifareBDType perPaxFlexiBDForChild = new PerPaxFlexifareBDType();
			PerPaxFlexifareBDType.PerPaxFlexiFareAmount perPaxFlexiBDAmountForChild = new PerPaxFlexifareBDType.PerPaxFlexiFareAmount();
			perPaxFlexiBDAmountForChild.setApplicablePaxType(CommonUtil
					.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
			if (showPriceInselectedCurrency && currencyCode != null) {
				BigDecimal perPaxFlexiAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(
						totalPerChildFlexiAmount, currencyCode, exchangeRateProxy);
				OTAUtils.setAmountAndCurrency(perPaxFlexiBDAmountForChild, perPaxFlexiAmountInPreferredCurrency, currencyCode);
			} else {
				OTAUtils.setAmountAndDefaultCurrency(perPaxFlexiBDAmountForChild, totalPerChildFlexiAmount);
			}
			perPaxFlexiBDForChild.setPerPaxFlexiFareAmount(perPaxFlexiBDAmountForChild);
			perPaxFlexiChargeBD.getPerPaxFlexifareBD().add(perPaxFlexiBDForChild);
		}

		// For Infant
		if (totalPerInfantFlexiAmount.compareTo(BigDecimal.ZERO) > 0) {
			PerPaxFlexifareBDType perPaxFlexiBDForInfant = new PerPaxFlexifareBDType();
			PerPaxFlexifareBDType.PerPaxFlexiFareAmount perPaxFlexiBDAmountForInfant = new PerPaxFlexifareBDType.PerPaxFlexiFareAmount();
			perPaxFlexiBDAmountForInfant.setApplicablePaxType(CommonUtil
					.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
			if (showPriceInselectedCurrency && currencyCode != null) {
				BigDecimal perPaxFlexiAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(
						totalPerInfantFlexiAmount, currencyCode, exchangeRateProxy);
				OTAUtils.setAmountAndCurrency(perPaxFlexiBDAmountForInfant, perPaxFlexiAmountInPreferredCurrency, currencyCode);
			} else {
				OTAUtils.setAmountAndDefaultCurrency(perPaxFlexiBDAmountForInfant, totalPerInfantFlexiAmount);
			}
			perPaxFlexiBDForInfant.setPerPaxFlexiFareAmount(perPaxFlexiBDAmountForInfant);
			perPaxFlexiChargeBD.getPerPaxFlexifareBD().add(perPaxFlexiBDForInfant);
		}
		return perPaxFlexiChargeBD;
	}

	/**
	 * Builds the {@link AirTripType} display object.
	 * 
	 * @param selectedFlightDTO
	 * @param paxType
	 * @param currencyCode
	 * @param flexiSelectionCriteria
	 * @param isFixedFare
	 * 
	 * @return : The {@link AirTripType} display object to be included in the per pax price break down.
	 * @throws WebservicesException
	 */
	public static AirFeeType getPerPaxAirFeeForFlexiCharges(SelectedFlightDTO selectedFlightDTO, String paxType,
			String currencyCode, FlexiFareSelectionCriteria flexiSelectionCriteria, boolean isFixedFare) throws ModuleException,
			WebservicesException {

		if (selectedFlightDTO == null || StringUtils.isEmpty(paxType) || flexiSelectionCriteria == null) {
			log.error("Required arguments are null in getPerPaxAirFeeForFlexiCharges");
			throw new IllegalArgumentException("Required arguments are null");
		}

		AirFeeType flexiFee = new AirFeeType();
		boolean showPriceInselectedCurrency = OTAUtils.isShowPriceInselectedCurrency();

		Map<String, BigDecimal> perPaxFlexiAmountsMap = getPerPaxFlexiChargesForPriceQuote(selectedFlightDTO,
				flexiSelectionCriteria, isFixedFare);

		BigDecimal perPaxTotalAmount = perPaxFlexiAmountsMap.get(CommonServicesUtil.convertToAccelAeroPaxType(paxType));

		if (showPriceInselectedCurrency && currencyCode != null) {
			// We don't need to convert here as a convesion has already happened in the creation.
			OTAUtils.setAmountAndCurrency(flexiFee, perPaxTotalAmount, currencyCode);
		} else {
			OTAUtils.setAmountAndDefaultCurrency(flexiFee, perPaxTotalAmount);
		}

		flexiFee.setFeeCode("Flexi/Charge for flexible Fares");

		return flexiFee;
	}

	/**
	 * 
	 * @param priceInfoTO
	 * @param paxType
	 * @param currencyCode
	 * @param flexiSelectionCriteria
	 * @param isFixedFare
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public static AirFeeType getPerPaxAirFeeForFlexiCharges(PriceInfoTO priceInfoTO, String paxType, String currencyCode,
			FlexiFareSelectionCriteria flexiSelectionCriteria, boolean isFixedFare, OTAPaxCountTO otaPaxCountTO,
			ExchangeRateProxy exchangeRateProxy) throws ModuleException, WebservicesException {

		if (priceInfoTO == null || StringUtils.isEmpty(paxType) || flexiSelectionCriteria == null) {
			log.error("Required arguments are null in getPerPaxAirFeeForFlexiCharges");
			throw new IllegalArgumentException("Required arguments are null");
		}

		AirFeeType flexiFee = new AirFeeType();
		boolean showPriceInselectedCurrency = OTAUtils.isShowPriceInselectedCurrency();

		Map<String, BigDecimal> perPaxFlexiAmountsMap = getPerPaxFlexiChargesForPriceQuote(priceInfoTO, flexiSelectionCriteria,
				isFixedFare, otaPaxCountTO);

		BigDecimal perPaxTotalAmount = perPaxFlexiAmountsMap.get(CommonServicesUtil.convertToAccelAeroPaxType(paxType));

		if (showPriceInselectedCurrency && currencyCode != null) {
			BigDecimal perPaxTotalAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(perPaxTotalAmount,
					currencyCode, exchangeRateProxy);
			OTAUtils.setAmountAndCurrency(flexiFee, perPaxTotalAmountInPreferredCurrency, currencyCode);
		} else {
			OTAUtils.setAmountAndDefaultCurrency(flexiFee, perPaxTotalAmount);
		}

		flexiFee.setFeeCode("Flexi/Charge for flexible Fares");

		return flexiFee;
	}

	/**
	 * Returns the Map of Per Pax flexi charges for each pax type for the price quote.
	 * 
	 * @param selectedFlightDTO
	 * @param flexiSelectionCriteria
	 * @param isFixedFare
	 * @return
	 * @throws WebservicesException
	 */
	public static Map<String, BigDecimal> getPerPaxFlexiChargesForPriceQuote(SelectedFlightDTO selectedFlightDTO,
			FlexiFareSelectionCriteria flexiSelectionCriteria, boolean isFixedFare) throws WebservicesException {

		if (selectedFlightDTO == null || flexiSelectionCriteria == null) {
			log.error("Required arguments are null in getPerPaxAirFeeForFlexiCharges");
			throw new IllegalArgumentException("Required method arguments are missing");
		}

		Map<String, BigDecimal> perPaxFlexiTotalsApplicableForPriceQuote = new HashMap<String, BigDecimal>();

		if (isFlexiSelected(flexiSelectionCriteria)) {
			Map<AirTripType, Collection<OndFareDTO>> flexiFaresMap = getOnDFareDTOsWithFlexi(selectedFlightDTO);

			if (flexiFaresMap == null) {
				log.error("Flexible fares are not available for the requested flight");
				// User has indicated to include flexi in the price quote. But there aren't any flexi avaialble.
				throw new WebservicesException(
						IOTACodeTables.AccelaeroErrorCodes_AAE_FLEXIBLE_FARES_ARE_NOT_AVAIABLE_FOR_THE_REQUESTED_FLIGHT_,
						"Flexible fares are not available for the requested flight");
			}

			Collection<OndFareDTO> ondFareDTOsWithFlexi = new ArrayList<OndFareDTO>();
			if (flexiSelectionCriteria.isOutBoundFlexiFareSelected()) {
				if (flexiFaresMap.get(AirTripType.OUTBOUND) != null && flexiFaresMap.get(AirTripType.OUTBOUND).size() > 0) {
					ondFareDTOsWithFlexi.addAll(flexiFaresMap.get(AirTripType.OUTBOUND));
				} else {
					log.error("Outbound Flexible fares are not available for the requested flight");
					// User has indicated to include outbound flexi in the price quote. But there aren't any outbound
					// flexi avaialble.
					throw new WebservicesException(
							IOTACodeTables.AccelaeroErrorCodes_AAE_OUTBOUND_FLEXIBLE_FARES_ARE_NOT_AVAIABLE_FOR_THE_REQUESTED_FLIGHT_,
							"Outbound Flexible fares are not available for the requested flight");
				}
			}

			if (flexiSelectionCriteria.isInBoundFlexiFareSelected()) {
				if (flexiFaresMap.get(AirTripType.INBOUND) != null && flexiFaresMap.get(AirTripType.INBOUND).size() > 0) {
					ondFareDTOsWithFlexi.addAll(flexiFaresMap.get(AirTripType.INBOUND));
				} else {
					log.error("Inbound Flexible fares are not available for the requested flight");
					// User has indicated to include inbound flexi in the price quote. But there aren't any inbound
					// flexi available.
					throw new WebservicesException(
							IOTACodeTables.AccelaeroErrorCodes_AAE_INBOUND_FLEXIBLE_FARES_ARE_NOT_AVAIABLE_FOR_THE_REQUESTED_FLIGHT_,
							"Inbound Flexible fares are not available for the requested flight");
				}
			}
			perPaxFlexiTotalsApplicableForPriceQuote = getPerPaxFlexiAmounts(ondFareDTOsWithFlexi);
		}
		return perPaxFlexiTotalsApplicableForPriceQuote;
	}

	/**
	 * 
	 * @param priceInfoTO
	 * @param flexiSelectionCriteria
	 * @param isFixedFare
	 * @return
	 * @throws WebservicesException
	 */
	public static Map<String, BigDecimal> getPerPaxFlexiChargesForPriceQuote(PriceInfoTO priceInfoTO,
			FlexiFareSelectionCriteria flexiSelectionCriteria, boolean isFixedFare, OTAPaxCountTO otaPaxCountTO) throws WebservicesException {

		if (priceInfoTO == null || flexiSelectionCriteria == null) {
			log.error("Required arguments are null in getPerPaxAirFeeForFlexiCharges");
			throw new IllegalArgumentException("Required method arguments are missing");
		}

		Map<String, BigDecimal> perPaxFlexiTotalsApplicableForPriceQuote = new HashMap<String, BigDecimal>();

		if (isFlexiSelected(flexiSelectionCriteria)) {

			FareTypeTO fareTypeTO = priceInfoTO.getFareTypeTO();

			Collection<ExternalChargeTO> externalChargeTOs = new ArrayList<ExternalChargeTO>();

			for (PerPaxPriceInfoTO perPaxPriceInfoTO : priceInfoTO.getPerPaxPriceInfo()) {
				FareTypeTO passengerPrice = perPaxPriceInfoTO.getPassengerPrice();

				if (passengerPrice.getOndExternalCharges() != null) {
					for (ONDExternalChargeTO ondExternalChargeTO : passengerPrice.getOndExternalCharges()) {
						if (ondExternalChargeTO.getExternalCharges() != null
								&& ondExternalChargeTO.getExternalCharges().size() > 0) {
							externalChargeTOs.addAll(ondExternalChargeTO.getExternalCharges());
						}
					}
				}
			}

			// Combine all external charges and get the flexi only with air trip type(INBOUND/OUTBOUND)

			Map<AirTripType, Collection<ExternalChargeTO>> externalChargeTOswithFlexiMap = getExternalChargeTOWithFlexi(
					externalChargeTOs, priceInfoTO.getFareSegChargeTO().getOndFareSegChargeTOs());

			if (externalChargeTOswithFlexiMap == null) {
				log.error("Flexible fares are not available for the requested flight");
				// User has indicated to include flexi in the price quote. But there aren't any flexi avaialble.
				throw new WebservicesException(
						IOTACodeTables.AccelaeroErrorCodes_AAE_FLEXIBLE_FARES_ARE_NOT_AVAIABLE_FOR_THE_REQUESTED_FLIGHT_,
						"Flexible fares are not available for the requested flight");
			}

			Collection<ExternalChargeTO> externalChargeTOsWithFlexi = new ArrayList<ExternalChargeTO>();
			if (flexiSelectionCriteria.isOutBoundFlexiFareSelected()) {
				if (externalChargeTOswithFlexiMap.get(AirTripType.OUTBOUND) != null
						&& !externalChargeTOswithFlexiMap.get(AirTripType.OUTBOUND).isEmpty()) {
					externalChargeTOsWithFlexi.addAll(externalChargeTOswithFlexiMap.get(AirTripType.OUTBOUND));
				} else {
					log.error("Outbound Flexible fares are not available for the requested flight");
					// User has indicated to include outbound flexi in the price quote. But there aren't any outbound
					// flexi avaialble.
					throw new WebservicesException(
							IOTACodeTables.AccelaeroErrorCodes_AAE_OUTBOUND_FLEXIBLE_FARES_ARE_NOT_AVAIABLE_FOR_THE_REQUESTED_FLIGHT_,
							"Outbound Flexible fares are not available for the requested flight");
				}
			}

			if (flexiSelectionCriteria.isInBoundFlexiFareSelected()) {
				if (externalChargeTOswithFlexiMap.get(AirTripType.INBOUND) != null
						&& externalChargeTOswithFlexiMap.get(AirTripType.INBOUND).size() > 0) {
					externalChargeTOsWithFlexi.addAll(externalChargeTOswithFlexiMap.get(AirTripType.INBOUND));
				} else {
					log.error("Inbound Flexible fares are not available for the requested flight");
					// User has indicated to include inbound flexi in the price quote. But there aren't any inbound
					// flexi available.
					throw new WebservicesException(
							IOTACodeTables.AccelaeroErrorCodes_AAE_INBOUND_FLEXIBLE_FARES_ARE_NOT_AVAIABLE_FOR_THE_REQUESTED_FLIGHT_,
							"Inbound Flexible fares are not available for the requested flight");
				}
			}
			
			BigDecimal totalAdultFlexiAmount = getPaxTotalFlexiAmounts(externalChargeTOsWithFlexi, PaxTypeTO.ADULT);
			BigDecimal totalChildFlexiAmount = getPaxTotalFlexiAmounts(externalChargeTOsWithFlexi, PaxTypeTO.CHILD);
			BigDecimal totalInfantFlexiAmount  = getPaxTotalFlexiAmounts(externalChargeTOsWithFlexi, PaxTypeTO.INFANT);
			
			BigDecimal totalPerAdultFlexiAmount = getPerPaxFlexiAmount(totalAdultFlexiAmount, otaPaxCountTO.getAdultCount());
			BigDecimal totalPerChildFlexiAmount = getPerPaxFlexiAmount(totalChildFlexiAmount, otaPaxCountTO.getChildCount());
			BigDecimal totalPerInfantFlexiAmount = getPerPaxFlexiAmount(totalInfantFlexiAmount, otaPaxCountTO.getInfantCount());
			
			perPaxFlexiTotalsApplicableForPriceQuote.put(PaxTypeTO.ADULT,totalPerAdultFlexiAmount);
			perPaxFlexiTotalsApplicableForPriceQuote.put(PaxTypeTO.CHILD,totalPerChildFlexiAmount);
			perPaxFlexiTotalsApplicableForPriceQuote.put(PaxTypeTO.INFANT,totalPerInfantFlexiAmount);
		}
		return perPaxFlexiTotalsApplicableForPriceQuote;
	}

	/**
	 * Calculates the total flexi fare amount for the price quote. NOTE: In base currency.
	 * 
	 * @param selectedFlightDTO
	 * @param availableFlightSearchDTO
	 * @param flexiSelectionCriteria
	 * @param isFixedFare
	 * @return
	 * @throws WebservicesException
	 */
	public static BigDecimal calculateTotalFlexiFareForPriceQuote(SelectedFlightDTO selectedFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO, FlexiFareSelectionCriteria flexiSelectionCriteria,
			boolean isFixedFare) throws WebservicesException {

		if (selectedFlightDTO == null || availableFlightSearchDTO == null || flexiSelectionCriteria == null) {
			log.error("Required arguments are missing for calculateTotalFlexiFareForPriceQuote");
			throw new IllegalArgumentException("Required arguments are null");
		}

		BigDecimal totalFlexiForPriceQuote = AccelAeroCalculator.getDefaultBigDecimalZero();

		Map<String, BigDecimal> perPaxFlexiChargesForPriceQuote = getPerPaxFlexiChargesForPriceQuote(selectedFlightDTO,
				flexiSelectionCriteria, isFixedFare);
		BigDecimal perAdultFlexiCharge = perPaxFlexiChargesForPriceQuote.get(PaxTypeTO.ADULT);
		BigDecimal perChildFlexiCharge = perPaxFlexiChargesForPriceQuote.get(PaxTypeTO.CHILD);
		BigDecimal perInfantFlexiCharge = perPaxFlexiChargesForPriceQuote.get(PaxTypeTO.INFANT);

		totalFlexiForPriceQuote = AccelAeroCalculator.add(
				AccelAeroCalculator.multiply(perAdultFlexiCharge, availableFlightSearchDTO.getAdultCount()),
				AccelAeroCalculator.multiply(perChildFlexiCharge, availableFlightSearchDTO.getChildCount()),
				AccelAeroCalculator.multiply(perInfantFlexiCharge, availableFlightSearchDTO.getInfantCount()));

		return totalFlexiForPriceQuote;
	}

	/**
	 * 
	 * @param selectedFlightDTO
	 * @param availableFlightSearchDTO
	 * @param flexiSelectionCriteria
	 * @param isFixedFare
	 * @return
	 * @throws WebservicesException
	 */
	public static BigDecimal calculateTotalFlexiFareForPriceQuote(PriceInfoTO priceInfoTO, OTAPaxCountTO paxCountTO,
			FlexiFareSelectionCriteria flexiSelectionCriteria, boolean isFixedFare) throws WebservicesException {

		if (priceInfoTO == null || paxCountTO == null || flexiSelectionCriteria == null) {
			log.error("Required arguments are missing for calculateTotalFlexiFareForPriceQuote");
			throw new IllegalArgumentException("Required arguments are null");
		}

		BigDecimal totalFlexiForPriceQuote = AccelAeroCalculator.getDefaultBigDecimalZero();

		Map<String, BigDecimal> perPaxFlexiChargesForPriceQuote = getPerPaxFlexiChargesForPriceQuote(priceInfoTO,
				flexiSelectionCriteria, isFixedFare, paxCountTO);
		BigDecimal perAdultFlexiCharge = perPaxFlexiChargesForPriceQuote.get(PaxTypeTO.ADULT);
		BigDecimal perChildFlexiCharge = perPaxFlexiChargesForPriceQuote.get(PaxTypeTO.CHILD);
		BigDecimal perInfantFlexiCharge = perPaxFlexiChargesForPriceQuote.get(PaxTypeTO.INFANT);

		totalFlexiForPriceQuote = AccelAeroCalculator.add(
				AccelAeroCalculator.multiply(perAdultFlexiCharge, paxCountTO.getAdultCount()),
				AccelAeroCalculator.multiply(perChildFlexiCharge, paxCountTO.getChildCount()),
				AccelAeroCalculator.multiply(perInfantFlexiCharge, paxCountTO.getInfantCount()));

		return totalFlexiForPriceQuote;
	}

	/**
	 * Returns true if either in-bound or out-bound flexi fares are selected in the price quote request.
	 * 
	 * @param flexiSelectionCriteria
	 * @return
	 */
	public static boolean isFlexiSelected(FlexiFareSelectionCriteria flexiSelectionCriteria) {
		boolean isFlexiSelected = false;

		if (flexiSelectionCriteria != null) {
			if (flexiSelectionCriteria.isOutBoundFlexiFareSelected() || flexiSelectionCriteria.isInBoundFlexiFareSelected()) {
				isFlexiSelected = true;
			}
		}
		return isFlexiSelected;
	}

	/**
	 * Gets the {@link ExternalChgDTO}s along with the pax type for a given {@link SelectedFlightDTO} and
	 * {@link FlexiFareSelectionCriteria}.
	 * 
	 * @param selectedFlightDTO
	 * @param flexiSelectionCriteria
	 * @param paxTypes
	 * @param isFixedFare
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public static List<ExternalChgDTO> getExternalChgDTOListForFlexi(SelectedFlightDTO selectedFlightDTO,
			FlexiFareSelectionCriteria flexiSelectionCriteria, String paxType) throws ModuleException, WebservicesException {

		List<ExternalChgDTO> externalChgDTOList = new ArrayList<ExternalChgDTO>();

		Map<AirTripType, Collection<OndFareDTO>> ondFareDTOsMapWithFlexi = getOnDFareDTOsWithFlexi(selectedFlightDTO);

		// validate the flexi selection to make sure that this request in not asking for unavailable flexi.
		validateFlexiSelection(flexiSelectionCriteria, ondFareDTOsMapWithFlexi);

		List<OndFareDTO> allOndFareDTOsWithFlexi = new ArrayList<OndFareDTO>();
		if (flexiSelectionCriteria.isOutBoundFlexiFareSelected()) {
			allOndFareDTOsWithFlexi.addAll(ondFareDTOsMapWithFlexi.get(AirTripType.OUTBOUND));
		}

		if (flexiSelectionCriteria.isInBoundFlexiFareSelected()) {
			allOndFareDTOsWithFlexi.addAll(ondFareDTOsMapWithFlexi.get(AirTripType.INBOUND));
		}

		ExternalChgDTO flexiExternalChgDTO = ReservationUtil.getFlexiCharges();

		for (OndFareDTO ondFareDTO : allOndFareDTOsWithFlexi) {
			if ((!ondFareDTO.isInBoundOnd() && flexiSelectionCriteria.isOutBoundFlexiFareSelected() || (ondFareDTO.isInBoundOnd() && flexiSelectionCriteria
					.isInBoundFlexiFareSelected()))) {
				int ondCodeLength = ondFareDTO.getOndCode().split("/").length - 1;
				Collection<FlightSegmentDTO> flightSegmentsCollection = ondFareDTO.getSegmentsMap().values();

				if (ondFareDTO.getAllFlexiCharges() != null) {
					// Assumption atleast one flexi charge is attached here. Multiple needs to handle in v2
					FlexiRuleDTO flexiRuleDTO = ondFareDTO.getAllFlexiCharges().iterator().next();
					BigDecimal[][] amountBreakDown = new BigDecimal[3][ondCodeLength];
					amountBreakDown[0] = getSplittedArray(flexiRuleDTO.getEffectiveChargeAmount(PaxTypeTO.ADULT), ondCodeLength);
					amountBreakDown[1] = getSplittedArray(flexiRuleDTO.getEffectiveChargeAmount(PaxTypeTO.CHILD), ondCodeLength);
					amountBreakDown[2] = getSplittedArray(flexiRuleDTO.getEffectiveChargeAmount(PaxTypeTO.INFANT), ondCodeLength);

					ondCodeLength = 0;
					for (Object element : flightSegmentsCollection) {
						FlightSegmentDTO flightSegmentDTO = (FlightSegmentDTO) element;

						String aaPaxType = CommonServicesUtil.convertToAccelAeroPaxType(paxType);

						FlexiExternalChgDTO cloneFlexiExternalChgDTO = (FlexiExternalChgDTO) flexiExternalChgDTO.clone();
						cloneFlexiExternalChgDTO.setFlightSegId(flightSegmentDTO.getSegmentId());
						cloneFlexiExternalChgDTO.setExternalChargesEnum(EXTERNAL_CHARGES.FLEXI_CHARGES);
						cloneFlexiExternalChgDTO.setRatioValueInPercentage(false);
						if (PaxTypeTO.ADULT.equals(aaPaxType)) {
							cloneFlexiExternalChgDTO.setAmount(amountBreakDown[0][ondCodeLength]);
						} else if (PaxTypeTO.CHILD.equals(aaPaxType)) {
							cloneFlexiExternalChgDTO.setAmount(amountBreakDown[1][ondCodeLength]);
						} else if (PaxTypeTO.INFANT.equals(aaPaxType)) {
							cloneFlexiExternalChgDTO.setAmount(amountBreakDown[2][ondCodeLength]);
						}

						Collection<FlexiRuleFlexibilityDTO> flexiRuleFlexibilityDTOs = flexiRuleDTO.getAvailableFlexibilities();
						for (Object element2 : flexiRuleFlexibilityDTOs) {
							FlexiRuleFlexibilityDTO flexiRuleFlexibilityDTO = (FlexiRuleFlexibilityDTO) element2;
							ReservationPaxOndFlexibilityDTO reservationPaxOndFlexibilityDTO = new ReservationPaxOndFlexibilityDTO();
							reservationPaxOndFlexibilityDTO.setAvailableCount(flexiRuleFlexibilityDTO.getAvailableCount());
							reservationPaxOndFlexibilityDTO.setUtilizedCount(0);
							reservationPaxOndFlexibilityDTO.setFlexibilityTypeId(flexiRuleFlexibilityDTO.getFlexibilityTypeId());
							reservationPaxOndFlexibilityDTO.setFlexiRateId(flexiRuleDTO.getFlexiRuleRateId());
							reservationPaxOndFlexibilityDTO.setStatus(ReservationPaxOndFlexibility.STATUS_ACTIVE);
							reservationPaxOndFlexibilityDTO.setDescription(flexiRuleFlexibilityDTO.getFlexibilityDescription());
							cloneFlexiExternalChgDTO.addToReservationFlexibilitiesList(reservationPaxOndFlexibilityDTO);
						}
						ondCodeLength++;

						externalChgDTOList.add(cloneFlexiExternalChgDTO);
					}
				}
			}
		}

		return externalChgDTOList;
	}

	/**
	 * 
	 * @param flightPriceRS
	 * @param flexiSelectionCriteria
	 * @param paxType
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<LCCClientExternalChgDTO> getExternalChgDTOListForFlexi(BaseAvailRS baseAvailRS,
			FlexiFareSelectionCriteria flexiSelectionCriteria, String paxType) throws ModuleException, WebservicesException {
		List<LCCClientExternalChgDTO> externalChgDTOList = new ArrayList<LCCClientExternalChgDTO>();

		if (baseAvailRS.getSelectedPriceFlightInfo() != null) {
			PriceInfoTO priceInfoTO = baseAvailRS.getSelectedPriceFlightInfo();

			if (priceInfoTO != null) {
				PerPaxPriceInfoTO perPaxPriceInfoTO = priceInfoTO.getPerPaxPriceInfoTO(CommonServicesUtil
						.convertToAccelAeroPaxType(paxType));
				FareTypeTO fareTypeTO = perPaxPriceInfoTO.getPassengerPrice();
				List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();
				List<ExternalChargeTO> externalCharges = new ArrayList<ExternalChargeTO>();

				for (OriginDestinationInformationTO originDestinationInformationTO : baseAvailRS
						.getOriginDestinationInformationList()) {
					for (OriginDestinationOptionTO originDestinationOptionTO : originDestinationInformationTO
							.getOrignDestinationOptions()) {
						flightSegmentTOs.addAll(originDestinationOptionTO.getFlightSegmentList());
					}
				}

				for (ONDExternalChargeTO ondExternalChargeTO : fareTypeTO.getOndExternalCharges()) {
					externalCharges.addAll(ondExternalChargeTO.getExternalCharges());
				}

				if (externalCharges.size() > 0 && flightSegmentTOs.size() > 0) {
					externalChgDTOList = ReservationUtil.transform(externalCharges, flightSegmentTOs);
				}

			}
		}

		return externalChgDTOList;
	}

	/**
	 * Adds the flexi charges to the external charges map in the transaction context.
	 * 
	 * @param selectedFlightDTO
	 *            : The selected flight DTO.
	 * @param availableFlightSearchDTO
	 *            : The available flight search DTO.
	 * @param flexiSelectionCriteria
	 *            : The flexi selection criteria.
	 * @param paxTypes
	 *            : The list of pax types in the request.
	 * @param isFixedFare
	 *            : Fixed fare boolean.
	 * 
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public static void addFlexiChargesToExternalChargesMap(SelectedFlightDTO selectedFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO, FlexiFareSelectionCriteria flexiSelectionCriteria,
			Collection<String> paxTypes) throws ModuleException, WebservicesException {
		if (WSFlexiFareUtil.isFlexiSelected(flexiSelectionCriteria)) {
			Map<String, List<ExternalChgDTO>> flexiChargesForPaxTypes = getExternalChgDTOMapForFlexi(selectedFlightDTO,
					flexiSelectionCriteria, paxTypes);
			int paxSeq = 0;
			int parentSeq = 0;

			for (String paxType : paxTypes) {
				int paxQuantity = getPaxQuantity(paxType, availableFlightSearchDTO);

				// set traveller reference numbers and introduce new breakdowns if any special services added
				for (int i = 0; i < paxQuantity; i++) {

					String travelerRPH = null;
					if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
						travelerRPH = "A" + (++paxSeq);
					} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
						travelerRPH = "C" + (++paxSeq);
					} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
						travelerRPH = "I" + (++paxSeq) + "/A" + (++parentSeq);
					} else {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Passenger Type");
					}
					PriceQuoteUtil.addExternalCharge(travelerRPH, flexiChargesForPaxTypes.get(paxType));
				}
			}
		}
	}

	/**
	 * 
	 * @param baseAvailRS
	 * @param availableFlightSearchDTO
	 * @param flexiSelectionCriteria
	 * @param paxTypes
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public static void addFlexiChargesToExternalChargesMap(BaseAvailRS baseAvailRS, BaseAvailRQ baseAvailRQ,
			FlexiFareSelectionCriteria flexiSelectionCriteria, Collection<String> paxTypes) throws ModuleException,
			WebservicesException {
		if (WSFlexiFareUtil.isFlexiSelected(flexiSelectionCriteria)) {
			Map<String, List<? extends LCCClientExternalChgDTO>> flexiChargesForPaxTypes = getExternalChgDTOMapForFlexi(
					baseAvailRS, flexiSelectionCriteria, paxTypes);
			int paxSeq = 0;
			int parentSeq = 0;

			for (String paxType : paxTypes) {
				int paxQuantity = getPaxQuantity(paxType, baseAvailRQ);

				// set traveller reference numbers and introduce new breakdowns if any special services added
				for (int i = 0; i < paxQuantity; i++) {

					String travelerRPH = null;
					if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
						travelerRPH = "A" + (++paxSeq);
					} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
						travelerRPH = "C" + (++paxSeq);
					} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
						travelerRPH = "I" + (++paxSeq) + "/A" + (++parentSeq);
					} else {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Passenger Type");
					}
					PriceQuoteInterlineUtil.addExternalCharge(travelerRPH, flexiChargesForPaxTypes.get(paxType));
				}
			}
		}
	}

	/**
	 * Returns the Map of K{PaxType} and V{List<ExternalChgDTO>} for a given price quote.
	 * 
	 * @param selectedFlightDTO
	 * @param flexiSelectionCriteria
	 * @param paxTypes
	 * @param isFixedFare
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public static Map<String, List<ExternalChgDTO>> getExternalChgDTOMapForFlexi(SelectedFlightDTO selectedFlightDTO,
			FlexiFareSelectionCriteria flexiSelectionCriteria, Collection<String> paxTypes) throws ModuleException,
			WebservicesException {

		Map<String, List<ExternalChgDTO>> externalChgDTOMap = new HashMap<String, List<ExternalChgDTO>>();
		for (String paxType : paxTypes) {
			externalChgDTOMap.put(paxType, getExternalChgDTOListForFlexi(selectedFlightDTO, flexiSelectionCriteria, paxType));
		}
		return externalChgDTOMap;
	}

	/**
	 * 
	 * @param baseAvailRS
	 * @param flexiSelectionCriteria
	 * @param paxTypes
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public static Map<String, List<? extends LCCClientExternalChgDTO>> getExternalChgDTOMapForFlexi(BaseAvailRS baseAvailRS,
			FlexiFareSelectionCriteria flexiSelectionCriteria, Collection<String> paxTypes) throws ModuleException,
			WebservicesException {

		Map<String, List<? extends LCCClientExternalChgDTO>> externalChgDTOMap = new HashMap<String, List<? extends LCCClientExternalChgDTO>>();
		for (String paxType : paxTypes) {
			externalChgDTOMap.put(paxType, getExternalChgDTOListForFlexi(baseAvailRS, flexiSelectionCriteria, paxType));
		}
		return externalChgDTOMap;
	}

	// Returns the pax quantity for the given pax type.
	private static int getPaxQuantity(String paxType, AvailableFlightSearchDTO availableFlightSearchDTO)
			throws WebservicesException {

		int paxQuantity;

		if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
			paxQuantity = availableFlightSearchDTO.getAdultCount();
		} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
			paxQuantity = availableFlightSearchDTO.getChildCount();
		} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
			paxQuantity = availableFlightSearchDTO.getInfantCount();
		} else {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Passenger Type");
		}

		return paxQuantity;
	}

	// Returns the pax quantity for the given pax type.
	private static int getPaxQuantity(String paxType, BaseAvailRQ baseAvailRQ) throws WebservicesException, ModuleException {

		int paxQuantity = 0;

		if (paxType != null) {
			String aaPaxType = CommonServicesUtil.convertToAccelAeroPaxType(paxType);

			TravelerInfoSummaryTO infoSummaryTO = baseAvailRQ.getTravelerInfoSummary();

			for (PassengerTypeQuantityTO quantityTO : infoSummaryTO.getPassengerTypeQuantityList()) {
				if (aaPaxType.equals(quantityTO.getPassengerType())) {
					paxQuantity = quantityTO.getQuantity();
					break;
				}
			}
		}

		return paxQuantity;
	}

	/**
	 * Validates the flexi selection criteria.
	 * 
	 * @param flexiSelectionCriteria
	 *            : The flexi selection
	 * @param ondFareDTOsWithFlexi
	 *            : The {@link OndFareDTO}s containing flexi diveded to inbout and outbound.
	 * @throws WebservicesException
	 */
	public static void validateFlexiSelection(FlexiFareSelectionCriteria flexiSelectionCriteria,
			Map<AirTripType, Collection<OndFareDTO>> ondFareDTOsWithFlexi) throws WebservicesException {

		if (isFlexiSelected(flexiSelectionCriteria)) {
			if (ondFareDTOsWithFlexi == null) {
				log.error("Flexible fares are not available for the requested flight");
				// User has indicated to include flexi in the price quote. But there aren't any flexi available.
				throw new WebservicesException(
						IOTACodeTables.AccelaeroErrorCodes_AAE_FLEXIBLE_FARES_ARE_NOT_AVAIABLE_FOR_THE_REQUESTED_FLIGHT_,
						"Flexible fares are not available for the requested flight");
			}

			if (flexiSelectionCriteria.isOutBoundFlexiFareSelected()) {
				if (ondFareDTOsWithFlexi.get(AirTripType.OUTBOUND) == null
						|| ondFareDTOsWithFlexi.get(AirTripType.OUTBOUND).size() <= 0) {
					log.error("Outbound Flexible fares are not available for the requested flight");
					// User has indicated to include outbound flexi in the price quote. But there aren't any flexi
					// available.
					throw new WebservicesException(
							IOTACodeTables.AccelaeroErrorCodes_AAE_OUTBOUND_FLEXIBLE_FARES_ARE_NOT_AVAIABLE_FOR_THE_REQUESTED_FLIGHT_,
							"Outbound Flexible fares are not available for the requested flight");
				}
			}

			if (flexiSelectionCriteria.isInBoundFlexiFareSelected()) {
				if (ondFareDTOsWithFlexi.get(AirTripType.INBOUND) == null
						|| ondFareDTOsWithFlexi.get(AirTripType.INBOUND).size() <= 0) {
					log.error("Inbound Flexible fares are not available for the requested flight");
					throw new WebservicesException(
							IOTACodeTables.AccelaeroErrorCodes_AAE_INBOUND_FLEXIBLE_FARES_ARE_NOT_AVAIABLE_FOR_THE_REQUESTED_FLIGHT_,
							"Inbound Flexible fares are not available for the requested flight");
				}
			}
		}
	}

	private static BigDecimal[] getSplittedArray(Double amount, int number) {
		BigDecimal[] splittedArray = new BigDecimal[number];
		BigDecimal firstAmount = AccelAeroCalculator.divide(new BigDecimal(amount), new BigDecimal(number));
		BigDecimal lastAmount = AccelAeroCalculator.subtract(new BigDecimal(amount),
				AccelAeroCalculator.multiply(firstAmount, new BigDecimal(number - 1)));
		for (int i = 0; i < number - 1; i++) {
			splittedArray[i] = firstAmount;
		}
		splittedArray[number - 1] = lastAmount;
		return splittedArray;
	}

	/**
	 * This method is used to validate adding flexi charges for modifications. We do not allow to add flexi for a
	 * segment already having flexi.
	 * 
	 * @param modifyingPnrSegIdList
	 *            : List of Pnr segment ids which are going to be modified.
	 * @param reservation
	 *            : The existing reservation.
	 * 
	 * @throws WebservicesException
	 *             : If the external flexi charges selected in the price quote stage contains charges for the already
	 *             existing flexi.
	 */
	public static void validateAddFlexiForModification(Reservation reservation, List<Integer> modifyingPnrSegIdList)
			throws WebservicesException {
		// if no flexi are selected at the price quote or if there aren't any segments being modified, then there's no
		// need to validate.
		if (getSelectedFlexiChargesAtPriceQuote().size() <= 0 || modifyingPnrSegIdList == null
				|| modifyingPnrSegIdList.size() <= 0) {
			return;
		}

		Set<Integer> pnrSegmentIdsWithFlexi = new HashSet<Integer>();

		if (reservation != null && modifyingPnrSegIdList != null) {
			Collection<ReservationSegmentDTO> setSegs = reservation.getSegmentsView();

			for (ReservationSegmentDTO segDto : setSegs) {
				if (reservation.getPaxOndFlexibilities() != null && reservation.getPaxOndFlexibilities().size() > 0) {

					if (reservation.getPaxOndFlexibilities() != null && reservation.getPaxOndFlexibilities().size() > 0) {
						Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> ondFlexibilitiesMap = reservation
								.getPaxOndFlexibilities();
						if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(segDto.getStatus())) {
							List<ReservationPaxOndFlexibilityDTO> segmentFlexiList = segmentFlexiList(ondFlexibilitiesMap,
									segDto.getPnrSegId());
							if (segmentFlexiList != null && segmentFlexiList.size() > 0) {
								pnrSegmentIdsWithFlexi.add(segDto.getPnrSegId());
							}
						}
					}
				}
			}
		}

		// if there are flexibilities already in the segments that are going to be modified
		// AND if there are selected flexi charges in the price quote, throw an WS Exception because this
		// is the case of adding flexi to segments already having flexies.
		if (pnrSegmentIdsWithFlexi.size() > 0 && modifyingPnrSegIdList != null && modifyingPnrSegIdList.size() > 0) {
			for (Integer modifyingPnrSegId : modifyingPnrSegIdList) {
				if (pnrSegmentIdsWithFlexi.contains(modifyingPnrSegId) && getSelectedFlexiChargesAtPriceQuote().size() > 0) {
					throw new WebservicesException(
							IOTACodeTables.AccelaeroErrorCodes_AAE_CANNOT_ADD_NEW_FLEXIBILITIES_FOR_SEGMENTS_ALREADY_HAVING_FLEXIBILITIES_,
							"Cannot add flexibilities for segments already having flexi.");
				}
			}
		}
	}

	/**
	 * @return The list of {@link FlexiExternalChgDTO} objects which are selected at the price quote stage.
	 */
	public static List<FlexiExternalChgDTO> getSelectedFlexiChargesAtPriceQuote() {

		Map<String, List<ExternalChgDTO>> quotedExternalCharges = (Map<String, List<ExternalChgDTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);
		List<FlexiExternalChgDTO> selectedFlexiChargesAtPriceQuote = new ArrayList<FlexiExternalChgDTO>();

		if (quotedExternalCharges != null && quotedExternalCharges.size() > 0) {
			for (List<ExternalChgDTO> externalChgDTOList : quotedExternalCharges.values()) {
				for (ExternalChgDTO externalChgDTO : externalChgDTOList) {
					if (externalChgDTO instanceof FlexiExternalChgDTO) {
						selectedFlexiChargesAtPriceQuote.add((FlexiExternalChgDTO) externalChgDTO);
					}
				}
			}
		}

		return selectedFlexiChargesAtPriceQuote;
	}

	/**
	 * Returns the list of {@link ReservationPaxOndFlexibilityDTO} for a given pnr segment id.
	 * 
	 * @param ondFlexibilitiesMap
	 * @param pnrSegId
	 * @return
	 */
	public static List<ReservationPaxOndFlexibilityDTO> segmentFlexiList(
			Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> ondFlexibilitiesMap, Integer pnrSegId) {
		Integer ppfId = WebServicesModuleUtils.getReservationBD().getReservationPaxFareId(pnrSegId);
		return (List<ReservationPaxOndFlexibilityDTO>) ondFlexibilitiesMap.get(ppfId);
	}

	/**
	 * Creates the reservation segment's available flexibilities for display. If there are one or more flexibilities
	 * available then it'll be displayed along with the {@link BookingFlightSegment}.
	 * 
	 * 
	 * @param reservationSegmentDTO
	 *            : The reservation segment
	 * @param reservation
	 *            : The reservation containing the reservation segment.
	 */
	public static void setSegmentFlexiFareAvailabilities(BookFlightSegmentType bookFlightSegmentType,
			ReservationSegmentDTO reservationSegmentDTO, Reservation reservation) {
		BookFlightSegmentType.AvailableFlexiOperations availableFlexiOperations = new BookFlightSegmentType.AvailableFlexiOperations();

		if (bookFlightSegmentType != null && reservationSegmentDTO != null && reservation != null) {
			if (reservation.getPaxOndFlexibilities() != null && reservation.getPaxOndFlexibilities().size() > 0) {

				Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> ondFlexibilitiesMap = reservation
						.getPaxOndFlexibilities();

				if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationSegmentDTO.getStatus())) {
					List<ReservationPaxOndFlexibilityDTO> segmentFlexiList = segmentFlexiList(ondFlexibilitiesMap,
							reservationSegmentDTO.getPnrSegId());
					if (segmentFlexiList != null && segmentFlexiList.size() > 0) {
						for (ReservationPaxOndFlexibilityDTO flexiDTO : segmentFlexiList) {
							if (ReservationPaxOndFlexibility.STATUS_ACTIVE.equals(flexiDTO.getStatus())) {
								FlexiOperationsType flexiOperationType = new FlexiOperationsType();
								Long cutoverTime = new Long(flexiDTO.getCutOverBufferInMins());
								flexiOperationType.setFlexiOperationCutoverTimeInMinutes(new BigInteger(cutoverTime.toString()));
								Long availableCount = new Long(flexiDTO.getAvailableCount());
								flexiOperationType.setNumberOfAllowedOperations(new BigInteger(availableCount.toString()));
								flexiOperationType.setAllowedOperationName(flexiDTO.getDescription());
								availableFlexiOperations.getFlexiOperations().add(flexiOperationType);
							}
						}
					}
				}
			}
		}

		// If there are any flexi operations set it to the booking flight segment.
		if (availableFlexiOperations.getFlexiOperations().size() > 0) {
			bookFlightSegmentType.setAvailableFlexiOperations(availableFlexiOperations);
		}
	}

	/**
	 * Creates the reservation segment's available flexibilities for display. If there are one or more flexibilities
	 * available then it'll be displayed along with the {@link BookingFlightSegment}.
	 * 
	 * @param bookFlightSegmentType
	 * @param lccClientReservationSegment
	 * @param lccReservation
	 */
	public static void setSegmentFlexiFareAvailabilities(BookFlightSegmentType bookFlightSegmentType,
			LCCClientReservationSegment lccClientReservationSegment, LCCClientReservation lccReservation) {
BookFlightSegmentType.AvailableFlexiOperations availableFlexiOperations = new BookFlightSegmentType.AvailableFlexiOperations();
		
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(lccReservation.getPNR());
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadFares(true);
		Reservation reservation = null;

		try {
			reservation = WebServicesModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);
		} catch (ModuleException me) {			
			log.error("Error while loading reservation for adding flexi information", me);
		} catch (Exception e) {
			log.error("Error while loading reservation for adding flexi information", e);
		}
           		
		if (bookFlightSegmentType != null && lccClientReservationSegment != null && reservation != null) {
			if (reservation.getPaxOndFlexibilities() != null && reservation.getPaxOndFlexibilities().size() > 0) {

				Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> ondFlexibilitiesMap = reservation
						.getPaxOndFlexibilities();

				if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(lccClientReservationSegment.getStatus())) {
					List<ReservationPaxOndFlexibilityDTO> segmentFlexiList = segmentFlexiList(ondFlexibilitiesMap,
							lccClientReservationSegment.getPnrSegID());
					if (segmentFlexiList != null && segmentFlexiList.size() > 0) {
						for (ReservationPaxOndFlexibilityDTO flexiDTO : segmentFlexiList) {
							if (ReservationPaxOndFlexibility.STATUS_ACTIVE.equals(flexiDTO.getStatus())) {
								FlexiOperationsType flexiOperationType = new FlexiOperationsType();
								Long cutoverTime = new Long(flexiDTO.getCutOverBufferInMins());
								flexiOperationType.setFlexiOperationCutoverTimeInMinutes(new BigInteger(cutoverTime.toString()));
								Long availableCount = new Long(flexiDTO.getAvailableCount());
								flexiOperationType.setNumberOfAllowedOperations(new BigInteger(availableCount.toString()));
								flexiOperationType.setAllowedOperationName(flexiDTO.getDescription());
								availableFlexiOperations.getFlexiOperations().add(flexiOperationType);
							}
						}
					}
				}
			}
		}

		// If there are any flexi operations set it to the booking flight segment.
		if (availableFlexiOperations.getFlexiOperations().size() > 0) {
			bookFlightSegmentType.setAvailableFlexiOperations(availableFlexiOperations);
		}
	}
}