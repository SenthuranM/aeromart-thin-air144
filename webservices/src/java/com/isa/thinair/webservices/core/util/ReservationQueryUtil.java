/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2007 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.core.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.activation.DataHandler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AAOTAItineraryRS;
import org.opentravel.ota._2003._05.AirFeeType;
import org.opentravel.ota._2003._05.AirItineraryPricingInfoType.PTCFareBreakdowns;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.AirItineraryType.OriginDestinationOptions;
import org.opentravel.ota._2003._05.AirReservationType.Fulfillment;
import org.opentravel.ota._2003._05.AirReservationType.Fulfillment.PaymentDetails;
import org.opentravel.ota._2003._05.AirTaxType;
import org.opentravel.ota._2003._05.AirTravelerType;
import org.opentravel.ota._2003._05.AirTravelerType.Document;
import org.opentravel.ota._2003._05.AirTravelerType.ETicketInfo;
import org.opentravel.ota._2003._05.AirTravelerType.Telephone;
import org.opentravel.ota._2003._05.AirTravelerType.TravelerRefNumber;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.BookingPriceInfoType;
import org.opentravel.ota._2003._05.DirectBillType;
import org.opentravel.ota._2003._05.DirectBillType.CompanyName;
import org.opentravel.ota._2003._05.ETicketStatusType;
import org.opentravel.ota._2003._05.ETicketUsedStatusType;
import org.opentravel.ota._2003._05.EticketInfoType;
import org.opentravel.ota._2003._05.FareType;
import org.opentravel.ota._2003._05.FareType.BaseFare;
import org.opentravel.ota._2003._05.FareType.Fees;
import org.opentravel.ota._2003._05.FareType.Taxes;
import org.opentravel.ota._2003._05.FareType.TotalFare;
import org.opentravel.ota._2003._05.FlightSegmentBaseType.ArrivalAirport;
import org.opentravel.ota._2003._05.FlightSegmentBaseType.DepartureAirport;
import org.opentravel.ota._2003._05.FlightSegmentType;
import org.opentravel.ota._2003._05.FreeTextType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;
import org.opentravel.ota._2003._05.OTAReadRQ;
import org.opentravel.ota._2003._05.OTAReadRQ.ReadRequests.AirReadRequest;
import org.opentravel.ota._2003._05.OTAReadRQ.ReadRequests.GlobalReservationReadRequest;
import org.opentravel.ota._2003._05.OTAReadRQ.ReadRequests.ReadRequest;
import org.opentravel.ota._2003._05.OTAResRetrieveRS;
import org.opentravel.ota._2003._05.OTAResRetrieveRS.ReservationsList;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.PTCFareBreakdownType;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.PaymentCardType;
import org.opentravel.ota._2003._05.PaymentDetailType;
import org.opentravel.ota._2003._05.PaymentDetailType.PaymentAmount;
import org.opentravel.ota._2003._05.PaymentDetailType.PaymentAmountInPayCur;
import org.opentravel.ota._2003._05.PaymentFormType.Cash;
import org.opentravel.ota._2003._05.PaymentFormType.LoyaltyRedemption;
import org.opentravel.ota._2003._05.PersonNameType;
import org.opentravel.ota._2003._05.SSRType;
import org.opentravel.ota._2003._05.SourceType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.BaggageRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.BaggageRequests.BaggageRequest;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.InsuranceRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.InsuranceRequests.InsuranceRequest;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.MealRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.MealRequests.MealRequest;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SSRRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SSRRequests.SSRRequest;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SeatRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SeatRequests.SeatRequest;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SpecialServiceRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SpecialServiceRequests.SpecialServiceRequest;
import org.opentravel.ota._2003._05.SuccessType;
import org.opentravel.ota._2003._05.TPAExtensionsType;
import org.opentravel.ota._2003._05.TicketType;
import org.opentravel.ota._2003._05.TicketingInfoType;
import org.opentravel.ota._2003._05.TravelerInfoType;
import org.opentravel.ota._2003._05.UniqueIDType;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.ChargesDetailDTO;
import com.isa.thinair.airreservation.api.dto.ExtPayTxCriteriaDTO;
import com.isa.thinair.airreservation.api.dto.ItineraryLayoutModesDTO;
import com.isa.thinair.airreservation.api.dto.PNRExtTransactionsTO;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaymentDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.baggage.PaxBaggageTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.model.ExternalPaymentTnx;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo;
import com.isa.thinair.airreservation.api.model.ReservationPaxTnxBreakdown;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationSSRUtil;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.constants.CommonsConstants.WSConstants;
import com.isa.thinair.commons.api.dto.SSRInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webservices.api.dtos.PaymentSummaryTO;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.TPAExtensionUtil;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.config.WebservicesConfig;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAdminInfoType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirReservationExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAPTCCountType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAPTCCountsType;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAResSummaryType;

/**
 * Web Service Wrapper for reservation query functions
 * 
 * @author Nilindra Fernando
 * @since 2.0
 */
public class ReservationQueryUtil extends BaseUtil {

	protected static final Log log = LogFactory.getLog(ReservationQueryUtil.class);

	private static final String ITINERARY_FILE_EXTN = ".html";

	private static WebservicesConfig webServicesConfig = WebServicesModuleUtils.getWebServicesConfig();

	private static GlobalConfig globalConfig = WebServicesModuleUtils.getGlobalConfig();

	/**
	 * Return the reservation list
	 * 
	 * @param oTAReadRQ
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public OTAResRetrieveRS getReservationList(OTAReadRQ oTAReadRQ) throws ModuleException, WebservicesException {
		ReservationSearchDTO reservationSearchDTO = extractReservationSearchReqParams(oTAReadRQ);
		reservationSearchDTO.setIncludePaxInfo(true);
		Collection colReservationDTO = getReservations(reservationSearchDTO);
		return generateOTAResRetrieveRS(oTAReadRQ, colReservationDTO);
	}

	/**
	 * Get Reservations
	 * 
	 * @param reservationSearchDTO
	 * @return
	 * @throws ModuleException
	 */
	private Collection getReservations(ReservationSearchDTO reservationSearchDTO) throws ModuleException {
		return WebServicesModuleUtils.getReservationQueryBD().getReservations(reservationSearchDTO);
	}

	/**
	 * Returns the reservation
	 * 
	 * @param oTAReadRQ
	 * @param loadDataOptions
	 * @param isInternalCall
	 *            TODO
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public OTAAirBookRS getReservation(OTAReadRQ oTAReadRQ, AALoadDataOptionsType loadDataOptions, boolean isInternalCall)
			throws ModuleException, WebservicesException {
		ReservationPaymentDTO reservationPaymentDTO = null;
		OTAAirBookRS otaAirBookRS = null;
		Reservation reservation = null;

		if (oTAReadRQ.getUniqueID() == null) {
			reservationPaymentDTO = extractReservationSearchReqParams(oTAReadRQ);
		} else if (oTAReadRQ.getUniqueID().getType() != null
				&& oTAReadRQ.getUniqueID().getType()
						.equals(CommonUtil.getOTACodeValue(IOTACodeTables.UniqueIdType_UIT_RESERVATION))) {
			reservationPaymentDTO = new ReservationPaymentDTO();
			reservationPaymentDTO.setPnr(oTAReadRQ.getUniqueID().getID());
		}

		// Catering if pnr number does not exist
		if (reservationPaymentDTO.getPnr() == null) {
			ReservationDTO reservationDTO = (ReservationDTO) BeanUtils.getFirstElement(getReservations(reservationPaymentDTO));
			reservation = getReservation(reservationDTO.getPnr(), loadDataOptions);
		} else {
			reservation = getReservation(reservationPaymentDTO.getPnr(), loadDataOptions);
		}

		// AARESAA-6630 - Avoid loading interline/dry bookings
		if (reservation != null
				&& (reservation.getExternalReservationSegment() != null && reservation.getExternalReservationSegment().size() > 0)) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR,
					"Interline bookings are not supported");
		} else if (reservation != null && reservation.getOriginatorPnr() != null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR, "Dry bookings are not supported");
		}

		validateReservation(oTAReadRQ, reservation);

		authorizeReservationAccess(reservation, isInternalCall);

		if (reservation != null) {
			otaAirBookRS = generateOTAAirBookRS(oTAReadRQ, reservation, loadDataOptions);
		}

		return otaAirBookRS;
	}

	private void authorizeReservationAccess(Reservation res, boolean isInternalCall) throws WebservicesException, ModuleException {
		
		Collection<String> privilegesKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();

		// TODO: authorization processes for internal method invocations should be refactored
		// Authorizing booking category
		if (!isInternalCall)

			// authorizing based on owner agency
			if (!userPrincipal.getAgentCode().equals(res.getAdminInfo().getOwnerAgentCode())) {
				if (!AuthorizationUtil.hasPrivilege(privilegesKeys, PrivilegesKeys.AlterResPrivilegesKeys.ANY_PNR_SEARCH)) {

					WSReservationUtil.authorizeBookingCategory(privilegesKeys, res.getBookingCategory(),
							WSConstants.ALLOW_FIND_RESERVATION);
					// authorizing based on the owner channel
					if (!AuthorizationUtil.hasPrivilege(privilegesKeys,
							PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_ACCESS_ANY_CHANNEL)) {
						int ownerChannel = res.getAdminInfo().getOwnerChannelId();

						switch (ownerChannel) {
						case SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES:
							WSReservationUtil.authorize(privilegesKeys, null,
									PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_ACCESS_DNATA_CHANNEL);
							break;
						case SalesChannelsUtil.SALES_CHANNEL_HOLIDAYS:
							WSReservationUtil.authorize(privilegesKeys, null,
									PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_ACCESS_HOLIDAY_CHANNEL);
							break;
						case SalesChannelsUtil.SALES_CHANNEL_AGENT:
							WSReservationUtil.authorize(privilegesKeys, null,
									PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_ACCESS_XBE_CHANNEL);
							break;
						case SalesChannelsUtil.SALES_CHANNEL_WEB:
							WSReservationUtil.authorize(privilegesKeys, null,
									PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_ACCESS_IBE_CHANNEL);
							break;
						case SalesChannelsUtil.SALES_CHANNEL_IOS:
							WSReservationUtil.authorize(privilegesKeys, null,
									PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_ACCESS_IOS_CHANNEL);
							break;
						case SalesChannelsUtil.SALES_CHANNEL_ANDROID:
							WSReservationUtil.authorize(privilegesKeys, null,
									PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_ACCESS_ANDROID_CHANNEL);
							break;
						}
					}
				}
			}

	}

	/**
	 * Validates retrieved reservation's eligibility.
	 * 
	 * @param oTAReadRQ
	 * @param reservation
	 * @throws WebservicesException
	 */
	private void validateReservation(OTAReadRQ oTAReadRQ, Reservation reservation) throws WebservicesException {

		Boolean triggerValidations = (Boolean) ThreadLocalData.getWSContextParam(
				WebservicesContext.TRIGGER_BALANCE_QUERY_VALIDATIONS);

		if (triggerValidations != null && triggerValidations.booleanValue()) {

			SourceType pos = oTAReadRQ.getPOS().getSource().get(0);
			String BCT = pos.getBookingChannel().getType();

			// ATM/CDM specific checkings
			if ((BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_ATM))
					|| BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_CDM)) || BCT
						.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_INTERNET)))) {

				// Do not load cancelled reservations for ATM/CDM
				if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_IS_ALREADY_CANCELLED_,
							"RESERVATION IS ALREADY CANCELLED");
				}

				// Ignore Departure Date Validation for Fawry Webservices
				Boolean triggerIgnorDeptDateValidation = (Boolean) ThreadLocalData.getWSContextParam(
						WebservicesContext.TRIGGER_IGNORE_DEPARTURE_DATE_VALIDATEION);
				if (triggerIgnorDeptDateValidation != null && triggerIgnorDeptDateValidation.booleanValue()) {
					return;
				}

				// Check if first or next segment departure date matches
				if (oTAReadRQ.getReadRequests() != null && oTAReadRQ.getReadRequests().getAirReadRequest().size() > 0) {
					AirReadRequest airReadRequest = oTAReadRQ.getReadRequests().getAirReadRequest().get(0);
					Calendar specifiedFirstDepDate = airReadRequest.getDepartureDate().toGregorianCalendar();

					Calendar[] firstAndNextDep = getFirstAndNextSegDepDateLocal(reservation.getSegmentsView());

					boolean isMatching = false;

					// Check if specified date matches first flight departure
					if (firstAndNextDep[0] != null) {
						if (firstAndNextDep[0].get(Calendar.MONTH) == specifiedFirstDepDate.get(Calendar.MONTH)
								&& firstAndNextDep[0].get(Calendar.DAY_OF_MONTH) == specifiedFirstDepDate
										.get(Calendar.DAY_OF_MONTH)) {
							isMatching = true;
						}
					}

					// Check if specified date matches next flight departure
					if (!isMatching && firstAndNextDep[1] != null) {
						if (firstAndNextDep[1].get(Calendar.MONTH) == specifiedFirstDepDate.get(Calendar.MONTH)
								&& firstAndNextDep[1].get(Calendar.DAY_OF_MONTH) == specifiedFirstDepDate
										.get(Calendar.DAY_OF_MONTH)) {
							isMatching = true;
						}
					}

					if (!isMatching) {
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INCORRECT_FIRST_DEPARTURE_DATE, "");
					}
				} else {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
							"First flight departure date not specified.");
				}
			}// end ATM/CDM specific processing
		}
	}

	/**
	 * Returns first and next fight segment's departure date(Local).
	 * 
	 * @param segmentsView
	 * @return Calendar [] {firstSegDepDateLocal, nextSegDepDateLocal}
	 */
	private Calendar[] getFirstAndNextSegDepDateLocal(Collection<ReservationSegmentDTO> segmentsView) {
		Date firstSegDepDate = null;
		Date firstSegDepDataZulu = null;

		Date nextSegDepDate = null;
		Date nextSegDepDateZulu = null;

		Date currentZuluDate = new Date(System.currentTimeMillis());
		for (ReservationSegmentDTO resSeg : segmentsView) {
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(resSeg.getStatus())) {
				if ((firstSegDepDataZulu == null || firstSegDepDataZulu.after(resSeg.getZuluDepartureDate()))) {
					firstSegDepDate = resSeg.getDepartureDate();
					firstSegDepDataZulu = resSeg.getZuluDepartureDate();
				}

				if (resSeg.getZuluDepartureDate().after(currentZuluDate)
						&& (nextSegDepDateZulu == null || nextSegDepDateZulu.after(resSeg.getZuluDepartureDate()))) {
					nextSegDepDate = resSeg.getDepartureDate();
					nextSegDepDateZulu = resSeg.getZuluDepartureDate();
				}
			}
		}

		Calendar calFirstSeg = null;
		Calendar calNextSeg = null;

		if (firstSegDepDataZulu != null) {
			calFirstSeg = Calendar.getInstance();
			calFirstSeg.setTime(firstSegDepDate);
		}

		if (nextSegDepDateZulu != null) {
			calNextSeg = Calendar.getInstance();
			calNextSeg.setTime(nextSegDepDate);
		}

		return new Calendar[] { calFirstSeg, calNextSeg };
	}

	/**
	 * Returns the Reservation Object
	 * 
	 * @param pnr
	 * @return
	 * @throws ModuleException
	 */
	public Reservation getReservation(String pnr, AALoadDataOptionsType loadDataOptions) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);

		// temperary by defult if loadDataOptions is null everything will be true
		// due to a method called by mehedi's functions
		if (loadDataOptions == null) {
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadOndChargesView(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegViewBookingTypes(true);
			pnrModesDTO.setLoadSegView(true);
		} else {
			if ((loadDataOptions.getLoadPriceInfoTotals() != null && loadDataOptions.getLoadPriceInfoTotals())
					|| (loadDataOptions.getLoadPTCPriceInfo() != null && loadDataOptions.getLoadPTCPriceInfo())) {
				pnrModesDTO.setLoadFares(true);
				pnrModesDTO.setLoadOndChargesView(true);
				pnrModesDTO.setLoadPaxAvaBalance(true);
			}
			if (loadDataOptions.getLoadAirItinery() != null && loadDataOptions.getLoadAirItinery()) {
				pnrModesDTO.setLoadSegViewBookingTypes(true);
				pnrModesDTO.setLoadSegView(true);
			}
			if ((loadDataOptions.getLoadFullFilment() != null && loadDataOptions.getLoadFullFilment())
					|| (loadDataOptions.getLoadPTCFullFilment() != null && loadDataOptions.getLoadPTCFullFilment())) {
				pnrModesDTO.setLoadPaxAvaBalance(true);
			}
		}

		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true);
		pnrModesDTO.setLoadLastUserNote(true);
		pnrModesDTO.setRecordAudit(true);
		pnrModesDTO.setLoadEtickets(true);

		return WebServicesModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);
	}

	/**
	 * Prepare Passenger List Response
	 * 
	 * @param readRQ
	 * @param colReservationDTO
	 * @return
	 * @throws WebservicesException
	 */
	private OTAResRetrieveRS generateOTAResRetrieveRS(OTAReadRQ readRQ, Collection colReservationDTO)
			throws WebservicesException, ModuleException {
		OTAResRetrieveRS oTAResRetrieveRS = getMetaOTAResRetrieveRS(readRQ);
		ReservationsList reservationsList = new ReservationsList();
		oTAResRetrieveRS.setSuccess(new SuccessType());
		oTAResRetrieveRS.setReservationsList(reservationsList);

		ReservationPaxDTO reservationPaxDTO;
		ReservationPaxDetailsDTO reservationPaxDetailsDTO;
		ReservationSegmentDTO reservationSegmentDTO;

		OTAResRetrieveRS.ReservationsList.AirReservation airReservation;
		PersonNameType personNameType;
		FlightSegmentType flightSegmentType;
		DepartureAirport departureAirport;
		ArrivalAirport arrivalAirport;

		// For all the reservations
		for (Iterator itColReservationDTO = colReservationDTO.iterator(); itColReservationDTO.hasNext();) {
			reservationPaxDTO = (ReservationPaxDTO) itColReservationDTO.next();
			airReservation = new OTAResRetrieveRS.ReservationsList.AirReservation();

			// For per passenger
			int addedPaxCount = 0;
			for (Iterator itReservationPaxDTO = reservationPaxDTO.getPassengers().iterator(); itReservationPaxDTO.hasNext();) {
				reservationPaxDetailsDTO = (ReservationPaxDetailsDTO) itReservationPaxDTO.next();

				// include upto 5 adult pax
				if (addedPaxCount < 5) {
					personNameType = new PersonNameType();
					if (reservationPaxDetailsDTO.getTitle() != null) {
						personNameType.getNameTitle().add(reservationPaxDetailsDTO.getTitle());
					}
					personNameType.setSurname(reservationPaxDetailsDTO.getLastName());
					personNameType.getGivenName().add(reservationPaxDetailsDTO.getFirstName());

					airReservation.getTravelerName().add(personNameType);
					++addedPaxCount;
				}
			}

			// Just showing the first segment this should not show all the
			// segment information since it's a search operation
			reservationSegmentDTO = getFirstSegment(reservationPaxDTO.getSegments());

			flightSegmentType = new FlightSegmentType();
			// Setting the flight Number
			flightSegmentType.setFlightNumber(reservationSegmentDTO.getFlightNo());

			// Setting the departure airport information
			departureAirport = new DepartureAirport();
			flightSegmentType.setDepartureAirport(departureAirport);
			departureAirport.setLocationCode(getDepartureOrArrivalSegment(reservationSegmentDTO.getSegmentCode(), true));

			// Setting the arrival aiport information
			arrivalAirport = new ArrivalAirport();
			flightSegmentType.setArrivalAirport(arrivalAirport);
			arrivalAirport.setLocationCode(getDepartureOrArrivalSegment(reservationSegmentDTO.getSegmentCode(), false));

			// Setting departure datetime
			flightSegmentType.setDepartureDateTime(CommonUtil.parse(reservationSegmentDTO.getDepartureDate()));

			airReservation.setFlightSegment(flightSegmentType);

			airReservation.setBookingReferenceID(reservationPaxDTO.getPnr());
			airReservation.setDateBooked(CommonUtil.parse(reservationPaxDTO.getPnrDate()));
			reservationsList.getAirReservation().add(airReservation);
		}

		return oTAResRetrieveRS;
	}

	/**
	 * Returns first confirmed segment. If no confirmed segments, then return the first cancelled segment.
	 * 
	 * @param resSegs
	 * @return
	 */
	private ReservationSegmentDTO getFirstSegment(Collection<ReservationSegmentDTO> resSegs) {
		ReservationSegmentDTO cnfFirstSeg = null;
		ReservationSegmentDTO cnxFirstSeg = null;

		Date cnfFirstDep = null;
		Date cnxFirstDep = null;

		for (ReservationSegmentDTO resSeg : resSegs) {
			if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(resSeg.getStatus())) {
				if (cnfFirstDep == null || cnfFirstDep.after(resSeg.getZuluDepartureDate())) {
					cnfFirstSeg = resSeg;
				}
			} else if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(resSeg.getStatus())) {
				if (cnxFirstDep == null || cnxFirstDep.after(resSeg.getZuluDepartureDate())) {
					cnxFirstSeg = resSeg;
				}
			}
		}

		return cnfFirstSeg == null ? cnxFirstSeg : cnfFirstSeg;
	}

	/**
	 * Extract Read Request Parameters
	 * 
	 * @param oTAReadRQ
	 * @return
	 * @throws ModuleException
	 */
	private ReservationPaymentDTO extractReservationSearchReqParams(OTAReadRQ oTAReadRQ) throws ModuleException {
		ReservationPaymentDTO reservationPaymentDTO = new ReservationPaymentDTO();

		processReadRequest(reservationPaymentDTO, oTAReadRQ.getReadRequests().getReadRequest());
		processGlobalReadRequest(reservationPaymentDTO, oTAReadRQ.getReadRequests().getGlobalReservationReadRequest());
		processAirReadRequest(reservationPaymentDTO, oTAReadRQ.getReadRequests().getAirReadRequest());

		return reservationPaymentDTO;
	}

	/**
	 * Process a global read request
	 * 
	 * @param reservationPaymentDTO
	 * @param globalReservationReadRequest
	 */
	private void processGlobalReadRequest(ReservationPaymentDTO reservationPaymentDTO,
			List<GlobalReservationReadRequest> globalReservationReadRequest) {
		if (globalReservationReadRequest != null) {
			OTAReadRQ.ReadRequests.GlobalReservationReadRequest readRequest;
			String givenName;
			String surname;
			PersonNameType personNameType;

			for (Iterator itListReadRequest = globalReservationReadRequest.iterator(); itListReadRequest.hasNext();) {
				readRequest = (GlobalReservationReadRequest) itListReadRequest.next();
				personNameType = readRequest.getTravelerName();

				if (personNameType != null) {
					// Capturing the given name
					givenName = BeanUtils.nullHandler(personNameType.getGivenName().get(0));
					if (!givenName.equals("")) {
						reservationPaymentDTO.setFirstName(givenName);
					}

					// Capturing the surname
					surname = BeanUtils.nullHandler(personNameType.getSurname());
					if (!surname.equals("")) {
						reservationPaymentDTO.setLastName(surname);
					}
				}
			}
		}
	}

	/**
	 * Process Air Read Request
	 * 
	 * @param reservationPaymentDTO
	 * @param listAirReadRequest
	 */
	private void processAirReadRequest(ReservationPaymentDTO reservationPaymentDTO, List<AirReadRequest> listAirReadRequest) {
		if (listAirReadRequest != null) {
			OTAReadRQ.ReadRequests.AirReadRequest airReadRequest;
			String flightNumber;
			String departureStation;
			String telephoneNo;
			String surname;
			String givenName;
			String maskedCreditCardNumber;
			String creditCardExpiryDate;
			Date departureDate;

			for (Iterator itListAirReadRequest = listAirReadRequest.iterator(); itListAirReadRequest.hasNext();) {
				airReadRequest = (AirReadRequest) itListAirReadRequest.next();

				// If flight number specified
				if (airReadRequest.getFlightNumber() != null) {
					flightNumber = BeanUtils.nullHandler(airReadRequest.getFlightNumber());
					if (!flightNumber.equals("")) {
						reservationPaymentDTO.setFlightNo(flightNumber);
					}
				}

				// If departure airport is not null
				if (airReadRequest.getDepartureAirport() != null) {
					departureStation = BeanUtils.nullHandler(airReadRequest.getDepartureAirport().getLocationCode());
					if (!departureStation.equals("")) {
						reservationPaymentDTO.setFromAirport(departureStation);
					}
				}

				// If departure date is not null
				if (airReadRequest.getDepartureDate() != null) {
					departureDate = airReadRequest.getDepartureDate().toGregorianCalendar().getTime();
					if (departureDate != null) {
						reservationPaymentDTO.setDepartureDate(departureDate);
					}
				}

				// If person name exist
				if (airReadRequest.getName() != null) {
					// Capturing the given name
					givenName = BeanUtils.nullHandler(airReadRequest.getName().getGivenName().get(0));
					if (!givenName.equals("")) {
						reservationPaymentDTO.setFirstName(givenName);
					}

					// Capturing the surname
					surname = BeanUtils.nullHandler(airReadRequest.getName().getSurname());
					if (!surname.equals("")) {
						reservationPaymentDTO.setLastName(surname);
					}
				}

				// If telephone number exist
				if (airReadRequest.getTelephone() != null) {
					telephoneNo = BeanUtils.nullHandler(airReadRequest.getTelephone().getPhoneNumber());
					if (!telephoneNo.equals("")) {
						reservationPaymentDTO.setTelephoneNo(telephoneNo);
					}
				}

				// If the credit card information exist
				if (airReadRequest.getCreditCardInfo() != null) {
					// Capturing the masked credit card number
					maskedCreditCardNumber = BeanUtils.nullHandler(airReadRequest.getCreditCardInfo().getMaskedCardNumber());
					if (!maskedCreditCardNumber.equals("")) {
						if (maskedCreditCardNumber.length() == 16) {
							reservationPaymentDTO.setCreditCardNo(maskedCreditCardNumber.substring(maskedCreditCardNumber
									.length() - 4));
						}
					}

					// Capturing the credit card expiry date
					creditCardExpiryDate = BeanUtils.nullHandler(airReadRequest.getCreditCardInfo().getExpireDate());
					if (!creditCardExpiryDate.equals("")) {
						reservationPaymentDTO.setEDate(creditCardExpiryDate);
					}
				}
			}
		}
	}

	/**
	 * Process Read Request
	 * 
	 * @param reservationPaymentDTO
	 * @param listReadRequest
	 */
	private void processReadRequest(ReservationPaymentDTO reservationPaymentDTO, List<ReadRequest> listReadRequest) {
		if (listReadRequest != null) {
			OTAReadRQ.ReadRequests.ReadRequest readRequest;
			String pnr;
			String telephoneNo;
			String emailAddress;
			String givenName;
			String surname;
			String maskedCreditCardNumber;
			String creditCardExpiryDate;

			for (Iterator itListReadRequest = listReadRequest.iterator(); itListReadRequest.hasNext();) {
				readRequest = (ReadRequest) itListReadRequest.next();

				// Capturing the pnr
				if (readRequest.getUniqueID() != null) {
					pnr = BeanUtils.nullHandler(readRequest.getUniqueID().getID());
					if (!pnr.equals("")) {
						reservationPaymentDTO.setPnr(pnr);
					}
				}

				// Verification is optional to validate the earlier record
				if (readRequest.getVerification() != null) {
					// Capturing the telephone number
					if (readRequest.getVerification().getTelephoneInfo() != null) {
						telephoneNo = BeanUtils.nullHandler(readRequest.getVerification().getTelephoneInfo().getPhoneNumber());
						if (!telephoneNo.equals("")) {
							reservationPaymentDTO.setTelephoneNo(telephoneNo);
						}
					}

					// Capturing the email address
					if (readRequest.getVerification().getEmail() != null) {
						emailAddress = BeanUtils.nullHandler(readRequest.getVerification().getEmail().getValue());
						if (!emailAddress.equals("")) {
							reservationPaymentDTO.setEmail(emailAddress);
						}
					}

					// Capturing the person name
					if (readRequest.getVerification().getPersonName() != null) {
						// Capturing the given name
						givenName = BeanUtils.nullHandler(readRequest.getVerification().getPersonName().getGivenName().get(0));
						if (!givenName.equals("")) {
							reservationPaymentDTO.setFirstName(givenName);
						}

						// Capturing the surname
						surname = BeanUtils.nullHandler(readRequest.getVerification().getPersonName().getSurname());
						if (!surname.equals("")) {
							reservationPaymentDTO.setLastName(surname);
						}
					}

					// Capturing the credit card information
					if (readRequest.getVerification().getPaymentCard() != null) {
						// Capturing the masked credit card number
						maskedCreditCardNumber = BeanUtils.nullHandler(readRequest.getVerification().getPaymentCard()
								.getMaskedCardNumber());
						if (!maskedCreditCardNumber.equals("")) {
							if (maskedCreditCardNumber.length() == 16) {
								reservationPaymentDTO.setCreditCardNo(maskedCreditCardNumber.substring(maskedCreditCardNumber
										.length() - 4));
							}
						}

						// Capturing the credit card expiry date
						creditCardExpiryDate = BeanUtils.nullHandler(readRequest.getVerification().getPaymentCard()
								.getExpireDate());
						if (!creditCardExpiryDate.equals("")) {
							reservationPaymentDTO.setEDate(creditCardExpiryDate);
						}
					}
				}
			}
		}
	}

	/**
	 * Prepare Reservation Search Response
	 * 
	 * @param readRQ
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private OTAAirBookRS generateOTAAirBookRS(OTAReadRQ readRQ, Reservation reservation, AALoadDataOptionsType options)
			throws ModuleException, WebservicesException {
		OTAAirBookRS oTAAirBookRS = getMetaOTAAirBookRS(readRQ);
		AirReservation airReservation = new AirReservation();
		oTAAirBookRS.getSuccessAndWarningsAndAirReservation().add(airReservation);

		SourceType pos = (SourceType) ThreadLocalData.getWSContextParam(WebservicesContext.REQUEST_POS);
		String BCT = pos.getBookingChannel().getType();

		// Setting the booking reference
		UniqueIDType uniqueIDType = new UniqueIDType();
		uniqueIDType.setID(reservation.getPnr());
		uniqueIDType.setType(CommonUtil.getOTACodeValue(IOTACodeTables.UniqueIdType_UIT_RESERVATION));
		airReservation.getBookingReferenceID().add(uniqueIDType);

		// Setting service charge for ATM/CDM
		QuotedChargeDTO serviceChargeDTO = null;
		Object[] firstPaxPnrPaxFareId = null;
		BigDecimal balanceToPay = ReservationUtil.getTotalBalToPay(reservation);

		Agent agent = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_AGENT);
	
		boolean blnServiceChargeIncluded = AppSysParamsUtil.getExtPmtServiceChargeCodeMap().containsKey(agent.getAgentCode());

		boolean isPmtIntegrationChannel = BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_ATM))
				|| BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_CDM))
				|| BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_INTERNET));
		if (balanceToPay.doubleValue() > 0 && isPmtIntegrationChannel) {
						
			String pmtClientIdentifier = (String) ThreadLocalData.getCurrentTnxParam(WebservicesContext.CLIENT_IDENTIFIER);
			if (blnServiceChargeIncluded) {
				serviceChargeDTO = WebServicesModuleUtils.getChargeBD()
						.getCharge(AppSysParamsUtil.getExtPmtServiceChargeCodeMap().get(agent.getAgentCode()), new Date(), null,
								null, false);
			}
			if (serviceChargeDTO != null) {
				// Service Charge Per Pax Type only for FAWRY
				SourceType posRq = readRQ.getPOS().getSource().get(0);

				if (pmtClientIdentifier != null && pmtClientIdentifier.equals(WebservicesConstants.ClientIdentifiers.FAWRY_EBPP)) {
					ReservationPax reservationPax;
					int paxCount = 0;
					for (Iterator itReservationPax = reservation.getPassengers().iterator(); itReservationPax.hasNext();) {
						reservationPax = (ReservationPax) itReservationPax.next();

						// Which means Parent or normal adult or child
						if (!ReservationApiUtils.isInfantType(reservationPax)) {
							paxCount = paxCount + 1;
						}
					}
					balanceToPay = AccelAeroCalculator.add(
							balanceToPay,
							AccelAeroCalculator.parseBigDecimal(serviceChargeDTO
									.getEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE) * paxCount));
					serviceChargeDTO.setChargeBasis("CHARGEPERPAX");
				} else {
					balanceToPay = AccelAeroCalculator.add(balanceToPay, AccelAeroCalculator.parseBigDecimal(serviceChargeDTO
							.getEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE)));
				}

				ThreadLocalData.setCurrentTnxParam(Transaction.RES_SERVICE_CHARGE, serviceChargeDTO);
				firstPaxPnrPaxFareId = WebServicesModuleUtils.getPassengerBD().getFirstPaxPnrPaxFareId(reservation.getPnr());
				ThreadLocalData.setCurrentTnxParam(Transaction.RES_SERVICE_CHARGED_PAX_INFO, firstPaxPnrPaxFareId);
			}
		}

		// check if agency has enough credit left
		BigDecimal availableCredit = WebServicesModuleUtils.getTravelAgentFinanceBD().getAgentAvailableCredit(
				agent.getAgentCode());

		if (balanceToPay.doubleValue() > 0 && balanceToPay.compareTo(availableCredit) == 1) {
			log.warn("Agency available credit is insufficient " + "[agencyCode=" + agent.getAgentCode() + ",agentName="
					+ agent.getAgentDisplayName() + ",availableCredit=" + availableCredit + ",balanceToPay=" + balanceToPay + "]");
			if (isPmtIntegrationChannel) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INSUFFICIENT_AGENT_AVAILABLE_CREDIT_,
						"[Insufficient agency available credit. Please contact finance team.]");
			}
		}

		if (options != null) {
			if (options.getLoadAirItinery() != null && options.getLoadAirItinery()) {// Setting the Air Itinerary
																					// Information
				boolean excludeCanceledSegments = false;
				if (isPmtIntegrationChannel) {
					excludeCanceledSegments = true;
				}
				airReservation.setAirItinerary(getAirItineraryType(reservation, excludeCanceledSegments));
			}

			if (options.getLoadTravelerInfo() != null && options.getLoadTravelerInfo() != null) {// Setting the Traveler
																								// Information
				airReservation.setTravelerInfo(getTravelerInfoType(reservation));
			}

			String baseCurrencyCode = null;
			String preferredCurrencyCode = null;
			if ((options.getLoadFullFilment() != null && options.getLoadFullFilment())
					|| (options.getLoadPTCFullFilment() != null && options.getLoadPTCFullFilment())) {// Setting the
																									// Fullfilment
																									// Information
				if (reservation.getTotalPaidAmount().doubleValue() > 0) {// TODO test this for reservation having
																			// REFUNDS
					// ----TODO set unified payment details for overall reservation; set Pax wise payment details
					Fulfillment fulfillment = getPaymentInfo(reservation);
					airReservation.setFulfillment(fulfillment);
					if (OTAUtils.isShowPriceInselectedCurrency()) {
						List<PaymentDetailType> paymentDetails = fulfillment.getPaymentDetails().getPaymentDetail();
						if (paymentDetails != null && paymentDetails.size() > 0) {
							for (PaymentDetailType paymentDetail : paymentDetails) {
								baseCurrencyCode = paymentDetail.getPaymentAmount().getCurrencyCode();
								preferredCurrencyCode = paymentDetail.getPaymentAmountInPayCur().getCurrencyCode();
							}
						}
					}
				}
			}

			if ((options.getLoadPriceInfoTotals() != null && options.getLoadPriceInfoTotals())
					|| (options.getLoadPTCPriceInfo() != null && options.getLoadPTCPriceInfo())) {// Setting the Pricing
																								// Information
				if (OTAUtils.isShowPriceInselectedCurrency() && baseCurrencyCode != null && preferredCurrencyCode != null
						&& !baseCurrencyCode.equals(preferredCurrencyCode)) {
					airReservation.setPriceInfo(getBookingPriceInfoType(reservation, serviceChargeDTO, firstPaxPnrPaxFareId,
							options.getLoadPriceInfoTotals() != null && options.getLoadPriceInfoTotals(),
							options.getLoadPTCPriceInfo() != null && options.getLoadPTCPriceInfo(), preferredCurrencyCode));
				} else {
					airReservation.setPriceInfo(getBookingPriceInfoType(reservation, serviceChargeDTO, firstPaxPnrPaxFareId,
							options.getLoadPriceInfoTotals() != null && options.getLoadPriceInfoTotals(),
							options.getLoadPTCPriceInfo() != null && options.getLoadPTCPriceInfo()));
				}
			}
		}

		// set ticketing status
		airReservation.getTicketing().add(getTicketingInfo(reservation));

		// setting the extensions
		AAAirReservationExt airReservationExtensions = new AAAirReservationExt();
		if (options.getLoadAccSummary() != null && options.getLoadAccSummary()) {
			airReservationExtensions.setResAccountSummary(ExtensionsUtil.prepareWSAccountSummary(reservation,
					(options != null && options.getLoadPTCAccSummary())));
		}

		airReservationExtensions.setContactInfo(ExtensionsUtil.transformToWSContactInfo(reservation.getContactInfo()));
		if (reservation.getUserNote() != null && !"".equals(reservation.getUserNote())) {
			airReservationExtensions.setUserNotes(ExtensionsUtil.transformToWSUserNotes(reservation.getUserNote()));
		}

		ExternalPaymentTnx currExternalPaymentTnx = null;

		String balanceQueryKey = (String) ThreadLocalData.getWSContextParam(WebservicesContext.BALANCE_QUERY_KEY);

		if (balanceQueryKey != null) {
			ExtPayTxCriteriaDTO currrentExtPayTxCriteriaDTO = new ExtPayTxCriteriaDTO();
			currrentExtPayTxCriteriaDTO.setPnr(reservation.getPnr());
			currrentExtPayTxCriteriaDTO.setBalanceQueryKey(balanceQueryKey);
			Map pnrTnxMap = WebServicesModuleUtils.getReservationQueryBD().getExtPayTransactions(currrentExtPayTxCriteriaDTO);
			PNRExtTransactionsTO pnrPayTnxs = null;
			if (pnrTnxMap != null && pnrTnxMap.containsKey(reservation.getPnr())) {
				PNRExtTransactionsTO pnrExtTransactionsTO = (PNRExtTransactionsTO) pnrTnxMap.get(reservation.getPnr());
				if (pnrExtTransactionsTO != null) {
					currExternalPaymentTnx = (ExternalPaymentTnx) pnrExtTransactionsTO.getExtPayTransactions().iterator().next();
				}
			}
		}

		// set the balance query key for ATM/CDM channels
		if (balanceToPay.doubleValue() > 0 && isPmtIntegrationChannel) {
			ExternalTransactionsUtil exTxUtil = new ExternalTransactionsUtil();

			BigDecimal roundedUpPaymentAmount = balanceToPay;
			if (BCT.equals(CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_BANK_CDM))) {
				roundedUpPaymentAmount = CommonUtil.getRoundedUpValue(balanceToPay, 10);
			}

			ExternalPaymentTnx extPayTx = exTxUtil.initializeExtTransaction(oTAAirBookRS, roundedUpPaymentAmount, BCT);

			airReservationExtensions.setCurrentExtPayTxInfo(ExtensionsUtil.prepareWSExtPayTxInfo(extPayTx));
		} else if (currExternalPaymentTnx != null) {
			airReservationExtensions.setCurrentExtPayTxInfo(ExtensionsUtil.prepareWSExtPayTxInfo(currExternalPaymentTnx));
		}

		// set reservation summary information
		AAResSummaryType aaResSummary = new AAResSummaryType();
		aaResSummary.setPTCCounts(new AAPTCCountsType());

		AAPTCCountType adultsCounts = new AAPTCCountType();
		adultsCounts.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
		adultsCounts.setPassengerTypeQuantity(new BigInteger(Integer.toString(reservation.getTotalPaxAdultCount())));
		aaResSummary.getPTCCounts().getPTCCount().add(adultsCounts);

		AAPTCCountType childCounts = new AAPTCCountType();
		childCounts.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
		childCounts.setPassengerTypeQuantity(new BigInteger(Integer.toString(reservation.getTotalPaxChildCount())));
		aaResSummary.getPTCCounts().getPTCCount().add(childCounts);

		AAPTCCountType infantCount = new AAPTCCountType();
		infantCount.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
		infantCount.setPassengerTypeQuantity(new BigInteger(Integer.toString(reservation.getTotalPaxInfantCount())));
		aaResSummary.getPTCCounts().getPTCCount().add(infantCount);

		AAAdminInfoType aaAdminInfo = new AAAdminInfoType();
		aaAdminInfo.setOriginAgentCode(reservation.getAdminInfo().getOriginAgentCode());
		aaAdminInfo.setOriginSalesTerminal(reservation.getAdminInfo().getOriginSalesTerminal());
		airReservationExtensions.setAdminInfo(aaAdminInfo);

		airReservationExtensions.setResSummary(aaResSummary);

		airReservation.setTPAExtensions(new TPAExtensionsType());
		airReservation.getTPAExtensions().getAny().add(TPAExtensionUtil.getInstance().marshall(airReservationExtensions));

		oTAAirBookRS.getSuccessAndWarningsAndAirReservation().add(new SuccessType());

		return oTAAirBookRS;
	}

	/**
	 * Return Payment information
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 *             TODO fix to extract the correct payment amounts; simplify the code
	 */
	private static Fulfillment getPaymentInfo(Reservation reservation) throws ModuleException {
		Fulfillment fulfillment = new Fulfillment();
		PaymentDetails paymentDetails = new PaymentDetails();
		fulfillment.setPaymentDetails(paymentDetails);

		PassengerBD passengerBD = WebServicesModuleUtils.getPassengerBD();
		Collection colPnrPaxIds = new ArrayList();
		ReservationPax reservationPax;
		for (Iterator itReservationPax = reservation.getPassengers().iterator(); itReservationPax.hasNext();) {
			reservationPax = (ReservationPax) itReservationPax.next();

			// Which means Parent or normal adult or child
			//if (!ReservationApiUtils.isInfantType(reservationPax)) {
				colPnrPaxIds.add(reservationPax.getPnrPaxId());
			//}
		}

		Map pnrPaxIdAndColReservationTnx = passengerBD.getPNRPaxPaymentsAndRefunds(colPnrPaxIds);
		Collection colReservationTnx = new ArrayList();
		for (Iterator itColReservationTnx = pnrPaxIdAndColReservationTnx.values().iterator(); itColReservationTnx.hasNext();) {
			colReservationTnx.addAll((Collection) itColReservationTnx.next());
		}

		Collection colCashNominalCodes = ReservationTnxNominalCode.getCashTypeNominalCodes();
		Collection colVisaNominalCodes = ReservationTnxNominalCode.getVisaTypeNominalCodes();
		Collection colMasterNominalCodes = ReservationTnxNominalCode.getMasterTypeNominalCodes();
		Collection colAmexNominalCodes = ReservationTnxNominalCode.getAmexTypeNominalCodes();
		Collection colDinersNominalCodes = ReservationTnxNominalCode.getDinersTypeNominalCodes();
		Collection colGenericNominalCodes = ReservationTnxNominalCode.getGenericTypeNominalCodes();
		Collection colCMINominalCodes = ReservationTnxNominalCode.getCMITypeNominalCodes();
		Collection colOnAccountNominalCodes = ReservationTnxNominalCode.getOnAccountTypeNominalCodes();
		Collection colCreditNominalCodes = ReservationTnxNominalCode.getCreditTypeNominalCodes();

		Map payTypeAndColReservationTnx = new HashMap();
		ReservationTnx reservationTnx;
		for (Iterator itReservationTnx = colReservationTnx.iterator(); itReservationTnx.hasNext();) {
			reservationTnx = (ReservationTnx) itReservationTnx.next();

			if (colCashNominalCodes.contains(reservationTnx.getNominalCode())) {
				updateNominalMap(payTypeAndColReservationTnx, colCashNominalCodes, reservationTnx);
			} else if (colVisaNominalCodes.contains(reservationTnx.getNominalCode())) {
				updateNominalMap(payTypeAndColReservationTnx, colVisaNominalCodes, reservationTnx);
			} else if (colMasterNominalCodes.contains(reservationTnx.getNominalCode())) {
				updateNominalMap(payTypeAndColReservationTnx, colMasterNominalCodes, reservationTnx);
			} else if (colAmexNominalCodes.contains(reservationTnx.getNominalCode())) {
				updateNominalMap(payTypeAndColReservationTnx, colAmexNominalCodes, reservationTnx);
			} else if (colDinersNominalCodes.contains(reservationTnx.getNominalCode())) {
				updateNominalMap(payTypeAndColReservationTnx, colDinersNominalCodes, reservationTnx);
			} else if (colGenericNominalCodes.contains(reservationTnx.getNominalCode())) {
				updateNominalMap(payTypeAndColReservationTnx, colGenericNominalCodes, reservationTnx);
			} else if (colCMINominalCodes.contains(reservationTnx.getNominalCode())) {
				updateNominalMap(payTypeAndColReservationTnx, colCMINominalCodes, reservationTnx);
			} else if (colOnAccountNominalCodes.contains(reservationTnx.getNominalCode())) {
				updateNominalMap(payTypeAndColReservationTnx, colOnAccountNominalCodes, reservationTnx);
			} else if (colCreditNominalCodes.contains(reservationTnx.getNominalCode())) {
				updateNominalMap(payTypeAndColReservationTnx, colCreditNominalCodes, reservationTnx);
			}
		}

		Collection colPayTypeNominalCodes;

		for (Iterator itPayType = payTypeAndColReservationTnx.keySet().iterator(); itPayType.hasNext();) {
			colPayTypeNominalCodes = (Collection) itPayType.next();
			colReservationTnx = (Collection) payTypeAndColReservationTnx.get(colPayTypeNominalCodes);

			// If it's a cash payment
			if (colPayTypeNominalCodes.equals(colCashNominalCodes)) {
				preparePaymentDetails(paymentDetails, colReservationTnx, "CASH");

				// If it's a on account payment
			} else if (colPayTypeNominalCodes.equals(colOnAccountNominalCodes)) {
				preparePaymentDetails(paymentDetails, colReservationTnx, "ONACC");

				// If it's a credit payment
			} else if (colPayTypeNominalCodes.equals(colCreditNominalCodes)) {
				preparePaymentDetails(paymentDetails, colReservationTnx, "CREDIT");

				// If it's a credit payment
			} else if (colPayTypeNominalCodes.equals(colVisaNominalCodes) || colPayTypeNominalCodes.equals(colMasterNominalCodes)
					|| colPayTypeNominalCodes.equals(colAmexNominalCodes) || colPayTypeNominalCodes.equals(colDinersNominalCodes)
					|| colPayTypeNominalCodes.equals(colGenericNominalCodes) || colPayTypeNominalCodes.equals(colCMINominalCodes)) {
				preparePaymentDetails(paymentDetails, colReservationTnx, "CARD");
			}
		}

		return fulfillment;
	}

	private static void preparePaymentDetails(PaymentDetails paymentDetails, Collection<ReservationTnx> colReservationTnx,
			String paymentType) throws ModuleException {
		Map<Date, PaymentSummaryTO> mapPaymentSummaryTO = new HashMap<Date, PaymentSummaryTO>();
		PaymentSummaryTO paymentSummaryTO;
		ReservationTnx reservationTnx;
		ReservationPaxTnxBreakdown txnBreak;
		PaymentDetailType paymentDetailType = null;
		PaymentAmount paymentAmount = null;
		PaymentAmountInPayCur paymentAmountInPayCur = null;
		Cash cash;
		CompanyName companyName;
		DirectBillType directBillType;
		LoyaltyRedemption loyaltyRedemption;
		PaymentCardType paymentCardType;
		ReservationBD reservationBD;

		// Identify payments (assumption : all transactions having same timestamp assumed to belong to single payment
		// transaction)
		for (Iterator<ReservationTnx> it = colReservationTnx.iterator(); it.hasNext();) {
			reservationTnx = it.next();

			if (!mapPaymentSummaryTO.containsKey(reservationTnx.getDateTime())) {
				paymentSummaryTO = new PaymentSummaryTO();
				mapPaymentSummaryTO.put(reservationTnx.getDateTime(), paymentSummaryTO);
			}
			paymentSummaryTO = mapPaymentSummaryTO.get(reservationTnx.getDateTime());

			if (paymentSummaryTO.getAmount() == null) {
				paymentSummaryTO.setTnxId(reservationTnx.getTnxId());
				paymentSummaryTO.setAmount(reservationTnx.getAmount().negate());
				paymentSummaryTO.setAgencyCode(reservationTnx.getAgentCode());
			} else {
				paymentSummaryTO.setAmount(AccelAeroCalculator.add(paymentSummaryTO.getAmount(), reservationTnx.getAmount()
						.negate()));
			}

			txnBreak = reservationTnx.getReservationPaxTnxBreakdown();
			if (txnBreak != null) { // Code assumes there could be transactions not having transaction breakdown records
									// as well.
				if (paymentSummaryTO.getAmountInPayCurrency() == null) {
					if (!OTAUtils.isShowPriceInselectedCurrency()) {
						paymentSummaryTO.setAmountInPayCurrency(txnBreak.getTotalPriceInPayCurrency().negate());
					} else {
						paymentSummaryTO.setAmountInPayCurrency(AccelAeroCalculator.add(txnBreak.getFareAmountInPayCurrency()
								.negate(), txnBreak.getModAmountInPayCurrency().negate(), txnBreak
								.getSurchargeAmountInPayCurrency().negate(), txnBreak.getTaxAmountInPayCurrency().negate()));
					}
					paymentSummaryTO.setPayCurrencyCode(txnBreak.getPaymentCurrencyCode());
				} else {
					paymentSummaryTO.setAmountInPayCurrency(AccelAeroCalculator.add(paymentSummaryTO.getAmountInPayCurrency(),
							txnBreak.getTotalPriceInPayCurrency().negate()));
				}
			}
		}

		for (PaymentSummaryTO paySummaryTO : mapPaymentSummaryTO.values()) {
			paymentDetailType = new PaymentDetailType();
			paymentDetails.getPaymentDetail().add(paymentDetailType);

			paymentAmount = new PaymentAmount();
			paymentDetailType.setPaymentAmount(paymentAmount);

			paymentAmount.setAmount(paySummaryTO.getAmount());
			paymentAmount.setCurrencyCode(AppSysParamsUtil.getBaseCurrency());
			paymentAmount.setDecimalPlaces(new BigInteger(String.valueOf(AppSysParamsUtil.getNumberOfDecimalPoints())));

			if (paySummaryTO.getAmountInPayCurrency() != null && paySummaryTO.getPayCurrencyCode() != null) {
				paymentAmountInPayCur = new PaymentAmountInPayCur();
				paymentDetailType.setPaymentAmountInPayCur(paymentAmountInPayCur);

				paymentAmountInPayCur.setAmount(paySummaryTO.getAmountInPayCurrency());
				paymentAmountInPayCur.setCurrencyCode(paySummaryTO.getPayCurrencyCode());
				paymentAmountInPayCur
						.setDecimalPlaces(new BigInteger(String.valueOf(AppSysParamsUtil.getNumberOfDecimalPoints())));
			}

			if ("CASH".equals(paymentType)) {
				cash = new Cash();
				cash.setCashIndicator(new Boolean(true));
				paymentDetailType.setCash(cash);
			} else if ("ONACC".equals(paymentType)) {
				directBillType = new DirectBillType();
				companyName = new CompanyName();
				companyName.setCode(paySummaryTO.getAgencyCode());
				companyName.setContactName(paySummaryTO.getAgencyCode());// TODO set agentName
				companyName.setValue(paySummaryTO.getAgencyCode());// TODO set agentName

				directBillType.setCompanyName(companyName);
				paymentDetailType.setDirectBill(directBillType);
			} else if ("CREDIT".equals(paymentType)) {
				loyaltyRedemption = new LoyaltyRedemption();
				loyaltyRedemption.setProgramName("Credit");
				paymentDetailType.setLoyaltyRedemption(loyaltyRedemption);
			} else if ("CARD".equals(paymentType)) {
				reservationBD = WebServicesModuleUtils.getReservationBD();
				Collection<Integer> txnIds = new ArrayList<Integer>();
				txnIds.add(paySummaryTO.getTnxId());

				Map<Integer, CardPaymentInfo> mapCardPayInfo = reservationBD.getCreditCardInfo(txnIds, true);

				paymentCardType = new PaymentCardType();
				paymentDetailType.setPaymentCard(paymentCardType);

				if (mapCardPayInfo != null && mapCardPayInfo.containsKey(paySummaryTO.getTnxId())) {
					CardPaymentInfo cardPaymentInfo = mapCardPayInfo.get(paySummaryTO.getTnxId());

					paymentCardType.setCardType(Integer.toString(cardPaymentInfo.getType()));
					paymentCardType.setCardHolderName(cardPaymentInfo.getName());
					paymentCardType.setCardNumber(cardPaymentInfo.getNoLastDigits());
				}
			}
		}
	}

	/**
	 * Updates the Nominal Map
	 * 
	 * @param payTypesNominalMap
	 * @param colNominalCodes
	 * @param reservationTnx
	 */
	private static void updateNominalMap(Map payTypesNominalMap, Collection colNominalCodes, ReservationTnx reservationTnx) {
		Collection<ReservationTnx> colPayTypeReservationTnx;

		if (payTypesNominalMap.containsKey(colNominalCodes)) {
			colPayTypeReservationTnx = (Collection) payTypesNominalMap.get(colNominalCodes);
			colPayTypeReservationTnx.add(reservationTnx);
		} else {
			colPayTypeReservationTnx = new ArrayList();
			colPayTypeReservationTnx.add(reservationTnx);
			payTypesNominalMap.put(colNominalCodes, colPayTypeReservationTnx);
		}
	}

	/**
	 * Return Price Information
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private static BookingPriceInfoType getBookingPriceInfoType(Reservation reservation, QuotedChargeDTO serviceChargeDTO,
			Object[] firstPaxPnrPaxFareId, boolean setPricingInfoTotals, boolean setPTCPricingInfo) throws ModuleException,
			WebservicesException {
		BookingPriceInfoType bookingPriceInfoType = new BookingPriceInfoType();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		if (setPricingInfoTotals) {// set the total amounts
			FareType fareType = new FareType();
			bookingPriceInfoType.setItinTotalFare(fareType);
			// FIXME Fare Per Pax Type
			BigDecimal dblServiceCharge;
			// For Fawry apply the charge per pax
			if (serviceChargeDTO != null) {
				if (serviceChargeDTO.getChargeBasis() != null
						&& serviceChargeDTO.getChargeBasis().equalsIgnoreCase("CHARGEPERPAX")) {
					ReservationPax reservationPax;
					int paxCount = 0;
					for (Iterator itReservationPax = reservation.getPassengers().iterator(); itReservationPax.hasNext();) {
						reservationPax = (ReservationPax) itReservationPax.next();

						// Which means Parent or normal adult or child
						if (!ReservationApiUtils.isInfantType(reservationPax)) {
							paxCount = paxCount + 1;
						}
					}
					dblServiceCharge = AccelAeroCalculator.parseBigDecimal(serviceChargeDTO
							.getEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE) * paxCount);
				} else
					dblServiceCharge = AccelAeroCalculator.parseBigDecimal(serviceChargeDTO
							.getEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE));
			} else
				dblServiceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

			// dblServiceCharge = (serviceChargeDTO!=null?
			// AccelAeroCalculator.parseBigDecimal(serviceChargeDTO.getEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE)):
			// AccelAeroCalculator.getDefaultBigDecimalZero());

			// Setting the base fare
			BaseFare baseFare = new BaseFare();
			fareType.setBaseFare(baseFare);
			OTAUtils.setAmountAndDefaultCurrency(baseFare, reservation.getTotalTicketFare());

			// Setting the taxes
			Taxes taxes = new Taxes();
			fareType.setTaxes(taxes);
			AirTaxType airTaxType = new AirTaxType();
			taxes.getTax().add(airTaxType);
			airTaxType.setTaxCode("TOTALTAX");// FIXME - set individual taxes breakdonw for overall reservation
			OTAUtils.setAmountAndDefaultCurrency(airTaxType, reservation.getTotalTicketTaxCharge());

			// Setting the total fees (which excludes fare and tax)
			AirFeeType airFeeType = new AirFeeType();
			fareType.setFees(new Fees());
			fareType.getFees().getFee().add(airFeeType);
			airFeeType.setFeeCode("TOTALFEE");// FIXME - set individual fees breakdown for overall reservation
			BigDecimal totalFees = AccelAeroCalculator.add(reservation.getTotalTicketSurCharge(),
					reservation.getTotalTicketCancelCharge(), reservation.getTotalTicketModificationCharge(),
					reservation.getTotalTicketAdjustmentCharge(), dblServiceCharge);
			OTAUtils.setAmountAndDefaultCurrency(airFeeType, totalFees);

			// Setting the total charge amount
			TotalFare totalFare = new TotalFare();
			fareType.setTotalFare(totalFare);
			OTAUtils.setAmountAndDefaultCurrency(totalFare,
					(AccelAeroCalculator.add(AccelAeroCalculator.add(reservation.getTotalChargeAmounts()), dblServiceCharge)));

			// Set total price in agent currency
			UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
			BigDecimal totalFareInAgentCurrency = WSReservationUtil.getAmountInSpecifiedCurrency(totalFare.getAmount(),
					principal.getAgentCurrencyCode(), exchangeRateProxy);

			if (totalFareInAgentCurrency != null) {
				FareType.TotalEquivFare totalEquivFare = new FareType.TotalEquivFare();
				fareType.setTotalEquivFare(totalEquivFare);
				OTAUtils.setAmountAndCurrency(totalEquivFare, totalFareInAgentCurrency, principal.getAgentCurrencyCode());
			}

		}

		if (setPTCPricingInfo) {
			PTCFareBreakdowns ptcBreakdowns = new PTCFareBreakdowns();
			bookingPriceInfoType.setPTCFareBreakdowns(ptcBreakdowns);
			ReservationPax reservationPax;
			PTCFareBreakdownType ptcBreakdown;
			ChargesDetailDTO chargesDetailDTO;
			FareType fare;
			TotalFare totalFare;
			AirFeeType fee;
			AirTaxType tax;
			PTCFareBreakdownType.TravelerRefNumber travelerRPH;

			for (Iterator iterator = reservation.getPassengers().iterator(); iterator.hasNext();) {
				reservationPax = (ReservationPax) iterator.next();
				ptcBreakdown = new PTCFareBreakdownType();
				ptcBreakdowns.getPTCFareBreakdown().add(ptcBreakdown);
				ptcBreakdown.setPassengerTypeQuantity(new PassengerTypeQuantityType());

				// Identifying the passenger type - ADULT
				if (ReservationApiUtils.isAdultType(reservationPax)) {
					ptcBreakdown.getPassengerTypeQuantity().setCode(
							CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
					travelerRPH = new PTCFareBreakdownType.TravelerRefNumber();
					travelerRPH.setRPH(OTAUtils.prepareTravelerRPH(ReservationInternalConstants.PassengerType.ADULT,
							reservationPax.getPnrPaxId(), reservationPax.getPaxSequence(), null));
					ptcBreakdown.getTravelerRefNumber().add(travelerRPH);

					// Identifying the passenger type - CHILD
				} else if (ReservationApiUtils.isChildType(reservationPax)) {
					ptcBreakdown.getPassengerTypeQuantity().setCode(
							CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
					travelerRPH = new PTCFareBreakdownType.TravelerRefNumber();
					travelerRPH.setRPH(OTAUtils.prepareTravelerRPH(ReservationInternalConstants.PassengerType.CHILD,
							reservationPax.getPnrPaxId(), reservationPax.getPaxSequence(), null));
					ptcBreakdown.getTravelerRefNumber().add(travelerRPH);

					// Identifying the passenger type - INFANT
				} else if (ReservationApiUtils.isInfantType(reservationPax)) {
					ptcBreakdown.getPassengerTypeQuantity().setCode(
							CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
					travelerRPH = new PTCFareBreakdownType.TravelerRefNumber();
					travelerRPH.setRPH(OTAUtils.prepareTravelerRPH(ReservationInternalConstants.PassengerType.INFANT,
							reservationPax.getPnrPaxId(), reservationPax.getPaxSequence(), reservationPax.getParent()
									.getPaxSequence()));
					ptcBreakdown.getTravelerRefNumber().add(travelerRPH);

				} else {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Passenger Type");
				}

				ptcBreakdown.getPassengerTypeQuantity().setQuantity(Integer.valueOf(1));
				fare = new FareType();
				ptcBreakdown.setPassengerFare(fare);

				// -----BEGIN FIXME fare basis code
				PTCFareBreakdownType.FareBasisCodes fareBasisCodes = new PTCFareBreakdownType.FareBasisCodes();
				fareBasisCodes.getFareBasisCode().add("P");
				ptcBreakdown.setFareBasisCodes(fareBasisCodes);
				// -----END

				// Setting the base fare
				fare.setBaseFare(new BaseFare());
				OTAUtils.setAmountAndDefaultCurrency(fare.getBaseFare(), reservationPax.getTotalFare());

				// Setting the total ticket amount
				totalFare = new TotalFare();
				fare.setTotalFare(totalFare);
				OTAUtils.setAmountAndDefaultCurrency(totalFare,
						ReservationApiUtils.getTotalChargeAmount(reservationPax.getTotalChargeAmounts()));

				fare.setTaxes(new Taxes());
				fare.setFees(new Fees());

				List<String> bookingClasses = new ArrayList<String>();
				BookingClassBD bookingClassBD = WebServicesModuleUtils.getBookingClassBD();

				for (Iterator j = reservationPax.getOndChargesView().iterator(); j.hasNext();) {
					chargesDetailDTO = (ChargesDetailDTO) j.next();

					if (!chargesDetailDTO.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.FAR)) {

						if (chargesDetailDTO.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.TAX)) {
							tax = new AirTaxType();
							fare.getTaxes().getTax().add(tax);
							tax.setTaxName(chargesDetailDTO.getChargeDescription());
							tax.setTaxCode(chargesDetailDTO.getChargeCode());
							OTAUtils.setAmountAndDefaultCurrency(tax, chargesDetailDTO.getChargeAmount());
						} else {// surcharges and other charges
							fee = new AirFeeType();
							fare.getFees().getFee().add(fee);
							fee.setValue(chargesDetailDTO.getChargeDescription());
							fee.setFeeCode(chargesDetailDTO.getChargeCode());
							OTAUtils.setAmountAndDefaultCurrency(fee, chargesDetailDTO.getChargeAmount());
						}
					} else {

						if (chargesDetailDTO.getBookingCodes() != null) {
							bookingClasses.addAll(chargesDetailDTO.getBookingCodes());
						}
					}
				}

				// Temporary solution.
				// TODO - Optimize the call
				// FIXME - Booking classes of canceled segments are to be excluded
				if (bookingClasses.size() > 0) {
					boolean isHoldAllowed = true;
					Map<String, BookingClass> bcMap = bookingClassBD.getBookingClassesMap(bookingClasses);
					if (bcMap != null && bcMap.size() > 0) {
						for (BookingClass bc : bcMap.values()) {
							if (bc.getOnHold()) {
								isHoldAllowed = false;
								break;
							}
						}
					}

					if (!isHoldAllowed) {
						fareBasisCodes.getFareBasisCode().add("NH");
					}
				}

				// Set the service charge to the first adult pax
				if (serviceChargeDTO != null && reservationPax.getPnrPaxId().equals(firstPaxPnrPaxFareId[0])) {
					fee = new AirFeeType();
					fare.getFees().getFee().add(fee);
					fee.setValue(serviceChargeDTO.getChargeDescription());
					fee.setFeeCode(serviceChargeDTO.getChargeCode());
					// FIXME Fare Per Pax Type
					OTAUtils.setAmountAndDefaultCurrency(fee, AccelAeroCalculator.parseBigDecimal(serviceChargeDTO
							.getEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE)));
				}
			}
		}

		return bookingPriceInfoType;
	}

	/**
	 * Return Price Information
	 * 
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private static BookingPriceInfoType getBookingPriceInfoType(Reservation reservation, QuotedChargeDTO serviceChargeDTO,
			Object[] firstPaxPnrPaxFareId, boolean setPricingInfoTotals, boolean setPTCPricingInfo, String currencyCode)
			throws ModuleException, WebservicesException {
		BookingPriceInfoType bookingPriceInfoType = new BookingPriceInfoType();

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(reservation.getZuluBookingTimestamp());
		PTCFareBreakdowns ptcBreakdowns = new PTCFareBreakdowns();
		BigDecimal totalFareInPreferredCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalFeeInPreferredCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalTaxInPreferredCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (setPTCPricingInfo) {
			ReservationPax reservationPax;
			PTCFareBreakdownType ptcBreakdown;
			ChargesDetailDTO chargesDetailDTO;
			FareType fare;
			TotalFare totalFare;
			AirFeeType fee;
			AirTaxType tax;
			PTCFareBreakdownType.TravelerRefNumber travelerRPH;

			for (Iterator iterator = reservation.getPassengers().iterator(); iterator.hasNext();) {
				reservationPax = (ReservationPax) iterator.next();
				ptcBreakdown = new PTCFareBreakdownType();
				ptcBreakdowns.getPTCFareBreakdown().add(ptcBreakdown);
				ptcBreakdown.setPassengerTypeQuantity(new PassengerTypeQuantityType());
				BigDecimal totalInSelectedCurPerPax = AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal totalFeeInSelectedCurPerPax = AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal totalTaxInSelectedCurPerPax = AccelAeroCalculator.getDefaultBigDecimalZero();

				// Identifying the passenger type - ADULT
				if (ReservationApiUtils.isAdultType(reservationPax)) {
					ptcBreakdown.getPassengerTypeQuantity().setCode(
							CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
					travelerRPH = new PTCFareBreakdownType.TravelerRefNumber();
					travelerRPH.setRPH(OTAUtils.prepareTravelerRPH(ReservationInternalConstants.PassengerType.ADULT,
							reservationPax.getPnrPaxId(), reservationPax.getPaxSequence(), null));
					ptcBreakdown.getTravelerRefNumber().add(travelerRPH);

					// Identifying the passenger type - CHILD
				} else if (ReservationApiUtils.isChildType(reservationPax)) {
					ptcBreakdown.getPassengerTypeQuantity().setCode(
							CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
					travelerRPH = new PTCFareBreakdownType.TravelerRefNumber();
					travelerRPH.setRPH(OTAUtils.prepareTravelerRPH(ReservationInternalConstants.PassengerType.CHILD,
							reservationPax.getPnrPaxId(), reservationPax.getPaxSequence(), null));
					ptcBreakdown.getTravelerRefNumber().add(travelerRPH);

					// Identifying the passenger type - INFANT
				} else if (ReservationApiUtils.isInfantType(reservationPax)) {
					ptcBreakdown.getPassengerTypeQuantity().setCode(
							CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
					travelerRPH = new PTCFareBreakdownType.TravelerRefNumber();
					travelerRPH.setRPH(OTAUtils.prepareTravelerRPH(ReservationInternalConstants.PassengerType.INFANT,
							reservationPax.getPnrPaxId(), reservationPax.getPaxSequence(), reservationPax.getParent()
									.getPaxSequence()));
					ptcBreakdown.getTravelerRefNumber().add(travelerRPH);

				} else {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Passenger Type");
				}

				ptcBreakdown.getPassengerTypeQuantity().setQuantity(Integer.valueOf(1));
				fare = new FareType();
				ptcBreakdown.setPassengerFare(fare);

				// -----BEGIN FIXME fare basis code
				PTCFareBreakdownType.FareBasisCodes fareBasisCodes = new PTCFareBreakdownType.FareBasisCodes();
				fareBasisCodes.getFareBasisCode().add("P");
				ptcBreakdown.setFareBasisCodes(fareBasisCodes);
				// -----END

				// Setting the base fare
				fare.setBaseFare(new BaseFare());
				BigDecimal totalBaseFareInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(
						reservationPax.getTotalFare(), currencyCode, exchangeRateProxy);
				totalInSelectedCurPerPax = AccelAeroCalculator.add(totalInSelectedCurPerPax, totalBaseFareInPreferredCurrency);
				OTAUtils.setAmountAndCurrency(fare.getBaseFare(), totalBaseFareInPreferredCurrency, currencyCode);

				fare.setTaxes(new Taxes());
				fare.setFees(new Fees());

				List<String> bookingClasses = new ArrayList<String>();
				BookingClassBD bookingClassBD = WebServicesModuleUtils.getBookingClassBD();

				for (Iterator j = reservationPax.getOndChargesView().iterator(); j.hasNext();) {
					chargesDetailDTO = (ChargesDetailDTO) j.next();

					if (!chargesDetailDTO.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.FAR)) {

						if (chargesDetailDTO.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.TAX)) {
							tax = new AirTaxType();
							fare.getTaxes().getTax().add(tax);
							tax.setTaxName(chargesDetailDTO.getChargeDescription());
							tax.setTaxCode(chargesDetailDTO.getChargeCode());
							BigDecimal taxInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(
									chargesDetailDTO.getChargeAmount(), currencyCode, exchangeRateProxy);
							totalTaxInSelectedCurPerPax = AccelAeroCalculator.add(totalTaxInSelectedCurPerPax,
									taxInPreferredCurrency);
							totalInSelectedCurPerPax = AccelAeroCalculator.add(totalInSelectedCurPerPax, taxInPreferredCurrency);
							OTAUtils.setAmountAndCurrency(tax, taxInPreferredCurrency, currencyCode);
						} else {// surcharges and other charges
							fee = new AirFeeType();
							fare.getFees().getFee().add(fee);
							fee.setValue(chargesDetailDTO.getChargeDescription());
							fee.setFeeCode(chargesDetailDTO.getChargeCode());
							BigDecimal feeInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(
									chargesDetailDTO.getChargeAmount(), currencyCode, exchangeRateProxy);
							totalFeeInSelectedCurPerPax = AccelAeroCalculator.add(totalFeeInSelectedCurPerPax,
									feeInPreferredCurrency);
							totalInSelectedCurPerPax = AccelAeroCalculator.add(totalInSelectedCurPerPax, feeInPreferredCurrency);
							OTAUtils.setAmountAndCurrency(fee, feeInPreferredCurrency, currencyCode);
						}
					} else {

						if (chargesDetailDTO.getBookingCodes() != null) {
							bookingClasses.addAll(chargesDetailDTO.getBookingCodes());
						}
					}
				}

				// Temporary solution.
				// TODO - Optimize the call
				// FIXME - Booking classes of canceled segments are to be excluded
				if (bookingClasses.size() > 0) {
					boolean isHoldAllowed = true;
					Map<String, BookingClass> bcMap = bookingClassBD.getBookingClassesMap(bookingClasses);
					if (bcMap != null && bcMap.size() > 0) {
						for (BookingClass bc : bcMap.values()) {
							if (bc.getOnHold()) {
								isHoldAllowed = false;
								break;
							}
						}
					}

					if (!isHoldAllowed) {
						fareBasisCodes.getFareBasisCode().add("NH");
					}
				}

				// Set the service charge to the first adult pax
				if (serviceChargeDTO != null && reservationPax.getPnrPaxId().equals(firstPaxPnrPaxFareId[0])) {
					fee = new AirFeeType();
					fare.getFees().getFee().add(fee);
					fee.setValue(serviceChargeDTO.getChargeDescription());
					fee.setFeeCode(serviceChargeDTO.getChargeCode());
					// FIXME Fare Per Pax Type
					BigDecimal feeInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(
							AccelAeroCalculator.parseBigDecimal(serviceChargeDTO
									.getEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE)), currencyCode,
							exchangeRateProxy);
					totalFeeInSelectedCurPerPax = AccelAeroCalculator.add(totalFeeInSelectedCurPerPax, feeInPreferredCurrency);
					totalInSelectedCurPerPax = AccelAeroCalculator.add(totalInSelectedCurPerPax, feeInPreferredCurrency);
					OTAUtils.setAmountAndCurrency(fee, feeInPreferredCurrency, currencyCode);
				}

				// Setting the total ticket amount
				totalFare = new TotalFare();
				fare.setTotalFare(totalFare);
				BigDecimal totalFareInPreferredCurrencyPerPax = AccelAeroCalculator.getDefaultBigDecimalZero();
				if (totalInSelectedCurPerPax != null) {
					totalFareInPreferredCurrencyPerPax = totalInSelectedCurPerPax;
				} else {
					WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(
							ReservationApiUtils.getTotalChargeAmount(reservationPax.getTotalChargeAmounts()), currencyCode,
							exchangeRateProxy);
				}
				OTAUtils.setAmountAndCurrency(totalFare, totalFareInPreferredCurrencyPerPax, currencyCode);

				totalFareInPreferredCurrency = AccelAeroCalculator.add(totalFareInPreferredCurrency,
						totalFareInPreferredCurrencyPerPax);
				totalFeeInPreferredCurrency = AccelAeroCalculator.add(totalFeeInPreferredCurrency, totalFeeInSelectedCurPerPax);
				totalTaxInPreferredCurrency = AccelAeroCalculator.add(totalTaxInPreferredCurrency, totalTaxInSelectedCurPerPax);
			}
		}

		if (setPricingInfoTotals) {// set the total amounts
			FareType fareType = new FareType();
			bookingPriceInfoType.setItinTotalFare(fareType);
			// FIXME Fare Per Pax Type
			BigDecimal dblServiceCharge = (serviceChargeDTO != null ? AccelAeroCalculator.parseBigDecimal(serviceChargeDTO
					.getEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE)) : AccelAeroCalculator
					.getDefaultBigDecimalZero());

			// Setting the base fare
			BaseFare baseFare = new BaseFare();
			fareType.setBaseFare(baseFare);
			BigDecimal totalTicketFareInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(
					reservation.getTotalTicketFare(), currencyCode, exchangeRateProxy);
			OTAUtils.setAmountAndCurrency(baseFare, totalTicketFareInPreferredCurrency, currencyCode);

			// Setting the taxes
			Taxes taxes = new Taxes();
			fareType.setTaxes(taxes);
			AirTaxType airTaxType = new AirTaxType();
			taxes.getTax().add(airTaxType);
			airTaxType.setTaxCode("TOTALTAX");// FIXME - set individual taxes breakdonw for overall reservation
			BigDecimal totalTicketTaxChargeInPreferredCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (totalTaxInPreferredCurrency != null) {
				totalTicketTaxChargeInPreferredCurrency = totalTaxInPreferredCurrency;
			} else {
				totalTicketTaxChargeInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(
						reservation.getTotalTicketTaxCharge(), currencyCode, exchangeRateProxy);
			}
			OTAUtils.setAmountAndCurrency(airTaxType, totalTicketTaxChargeInPreferredCurrency, currencyCode);

			// Setting the total fees (which excludes fare and tax)
			AirFeeType airFeeType = new AirFeeType();
			fareType.setFees(new Fees());
			fareType.getFees().getFee().add(airFeeType);
			airFeeType.setFeeCode("TOTALFEE");// FIXME - set individual fees breakdown for overall reservation
			BigDecimal totalFees = AccelAeroCalculator.add(reservation.getTotalTicketSurCharge(),
					reservation.getTotalTicketCancelCharge(), reservation.getTotalTicketModificationCharge(),
					reservation.getTotalTicketAdjustmentCharge(), dblServiceCharge);
			BigDecimal totalFeesInPreferredCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (totalFeeInPreferredCurrency != null) {
				totalFeesInPreferredCurrency = totalFeeInPreferredCurrency;
			} else {
				totalFeesInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(totalFees,
						currencyCode, exchangeRateProxy);
			}
			OTAUtils.setAmountAndCurrency(airFeeType, totalFeesInPreferredCurrency, currencyCode);

			// Setting the total charge amount
			TotalFare totalFare = new TotalFare();
			fareType.setTotalFare(totalFare);
			BigDecimal totalFareAmountInPreferredCurrency = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (totalFareInPreferredCurrency != null) {
				totalFareAmountInPreferredCurrency = totalFareInPreferredCurrency;
			} else {
				totalFareAmountInPreferredCurrency = WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(
						AccelAeroCalculator.add(AccelAeroCalculator.add(reservation.getTotalChargeAmounts()), dblServiceCharge),
						currencyCode, exchangeRateProxy);
			}
			OTAUtils.setAmountAndCurrency(totalFare, totalFareAmountInPreferredCurrency, currencyCode);

			// Set total price in agent currency
			UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
			BigDecimal totalFareInAgentCurrency = WSReservationUtil.getAmountInSpecifiedCurrencyInSpecifiedTime(
					AccelAeroCalculator.add(AccelAeroCalculator.add(reservation.getTotalChargeAmounts()), dblServiceCharge),
					principal.getAgentCurrencyCode(), exchangeRateProxy);

			if (totalFareInAgentCurrency != null && !currencyCode.equals(principal.getAgentCurrencyCode())) {
				FareType.TotalEquivFare totalEquivFare = new FareType.TotalEquivFare();
				fareType.setTotalEquivFare(totalEquivFare);
				OTAUtils.setAmountAndCurrency(totalEquivFare, totalFareInAgentCurrency, principal.getAgentCurrencyCode());
			} else if (currencyCode.equals(principal.getAgentCurrencyCode())) {
				FareType.TotalEquivFare totalEquivFare = new FareType.TotalEquivFare();
				fareType.setTotalEquivFare(totalEquivFare);
				OTAUtils.setAmountAndDefaultCurrency(totalEquivFare,
						(AccelAeroCalculator.add(AccelAeroCalculator.add(reservation.getTotalChargeAmounts()), dblServiceCharge)));
			}

		}
		if (setPTCPricingInfo) {
			bookingPriceInfoType.setPTCFareBreakdowns(ptcBreakdowns);
		}
		return bookingPriceInfoType;
	}

	/**
	 * Return the travellers information
	 * 
	 * @param reservation
	 * @return
	 * @throws WebservicesException
	 */
	private static TravelerInfoType getTravelerInfoType(Reservation reservation) throws ModuleException, WebservicesException {
		TravelerInfoType travelerInfoType = new TravelerInfoType();
		ReservationPax reservationPaxDetailsDTO;
		AirTravelerType airTravelerType;
		PersonNameType personNameType;
		Telephone telephone;
		SpecialReqDetailsType specialReqDetailsType;
		SpecialServiceRequests ssrs = new SpecialServiceRequests();
		SpecialServiceRequest ssr;
		String paxTitle = null;
		Document doc;
		List<SeatRequest> seatRequests = new ArrayList<SeatRequest>();
		List<MealRequest> mealRequests = new ArrayList<MealRequest>();
		List<SSRRequest> ssrRequests = new ArrayList<SSRRequest>();
		List<BaggageRequest> baggageRequests = new ArrayList<BaggageRequest>();

		Collection<PaxMealTO> combinedMealsInReservation = MealDetailsUtil.getCombinedPaxMealDTOs(reservation.getMeals());

		for (Iterator itReservationPaxDetailsDTO = reservation.getPassengers().iterator(); itReservationPaxDetailsDTO.hasNext();) {
			reservationPaxDetailsDTO = (ReservationPax) itReservationPaxDetailsDTO.next();
			airTravelerType = new AirTravelerType();
			airTravelerType.setTravelerRefNumber(new TravelerRefNumber());
			paxTitle = null;
			doc = new Document();

			// Adult
			if (ReservationInternalConstants.PassengerType.ADULT.equals(reservationPaxDetailsDTO.getPaxType())) {
				airTravelerType.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
				airTravelerType.getTravelerRefNumber().setRPH(
						OTAUtils.prepareTravelerRPH(reservationPaxDetailsDTO.getPaxType(),
								reservationPaxDetailsDTO.getPnrPaxId(), reservationPaxDetailsDTO.getPaxSequence(), null));
				paxTitle = CommonUtil.getWSPaxTitle(reservationPaxDetailsDTO.getTitle());

				// Child
			} else if (ReservationInternalConstants.PassengerType.CHILD.equals(reservationPaxDetailsDTO.getPaxType())) {
				airTravelerType.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
				airTravelerType.getTravelerRefNumber().setRPH(
						OTAUtils.prepareTravelerRPH(reservationPaxDetailsDTO.getPaxType(),
								reservationPaxDetailsDTO.getPnrPaxId(), reservationPaxDetailsDTO.getPaxSequence(), null));
				paxTitle = CommonUtil.getWSPaxTitle(reservationPaxDetailsDTO.getTitle());

				// Infant
			} else if (ReservationInternalConstants.PassengerType.INFANT.equals(reservationPaxDetailsDTO.getPaxType())) {
				airTravelerType.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
				airTravelerType.getTravelerRefNumber().setRPH(
						OTAUtils.prepareTravelerRPH(reservationPaxDetailsDTO.getPaxType(),
								reservationPaxDetailsDTO.getPnrPaxId(), reservationPaxDetailsDTO.getPaxSequence(),
								reservationPaxDetailsDTO.getParent().getPaxSequence()));
				paxTitle = CommonUtil.getWSPaxTitle(reservationPaxDetailsDTO.getTitle());

			} else {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Passenger Type");
			}

			personNameType = new PersonNameType();
			if (paxTitle != null) {
				personNameType.getNameTitle().add(paxTitle);
			}
			personNameType.setSurname(reservationPaxDetailsDTO.getLastName());
			personNameType.getGivenName().add(reservationPaxDetailsDTO.getFirstName());
			airTravelerType.setPersonName(personNameType);

			telephone = new Telephone();
			telephone.setPhoneNumber(reservation.getContactInfo().getPhoneNo());
			airTravelerType.getTelephone().add(telephone);

			ReservationPaxAdditionalInfo addInfo = reservationPaxDetailsDTO.getPaxAdditionalInfo();

			if (reservationPaxDetailsDTO.getNationalityCode() != null) {
				Nationality nationality = WebServicesModuleUtils.getCommonMasterBD().getNationality(
						reservationPaxDetailsDTO.getNationalityCode());
				if ((nationality != null) && (nationality.getIsoCode() != null)) {
					doc.setDocHolderNationality(nationality.getIsoCode());
					airTravelerType.getDocument().add(doc);
					if (addInfo != null) {
						if (addInfo.getPassportNo() != null) {
							doc.setDocID(addInfo.getPassportNo());
							doc.setDocType(CommonsConstants.SSRCode.PSPT.toString());
							if (addInfo.getPassportIssuedCntry() != null) {
								doc.setDocIssueCountry(addInfo.getPassportIssuedCntry());
							}
							if (addInfo.getPassportExpiry() != null) {
								doc.setExpireDate(CommonUtil.parse(addInfo.getPassportExpiry()));
							}
						}
					}
				}
			}

			if (addInfo != null) {
				if (addInfo.getEmployeeID() != null) {
					doc = new Document();
					doc.setDocID(addInfo.getEmployeeID());
					doc.setDocType(CommonsConstants.SSRCode.ID.toString());
					if (addInfo.getDateOfJoin() != null) {
						doc.setEffectiveDate(CommonUtil.parse(addInfo.getDateOfJoin()));
					}
					if (addInfo.getIdCategory() != null) {
						doc.setDocIssueAuthority(addInfo.getIdCategory());
					}
					airTravelerType.getDocument().add(doc);
				}

				if (addInfo.getFfid() != null && !"".equals(addInfo.getFfid())) {
					AirTravelerType.CustLoyalty custLoyalty = new AirTravelerType.CustLoyalty();
					custLoyalty.setMembershipID(addInfo.getFfid());
					airTravelerType.getCustLoyalty().add(custLoyalty);
				}
			}

			travelerInfoType.getAirTraveler().add(airTravelerType);

			/*
			 * Add eticket details.
			 */
			Collection<EticketTO> eTicketList = reservationPaxDetailsDTO.geteTickets();
			airTravelerType.setETicketInfo(new ETicketInfo());
			if (eTicketList != null) {
				for (EticketTO eticket : eTicketList) {
					EticketInfoType eTicketInfo = new EticketInfoType();
					eTicketInfo.setCouponNo(eticket.getCouponNo());
					eTicketInfo.setETicketNo(eticket.getEticketNumber());
					eTicketInfo.setFlightSegmentRPH(String.valueOf(eticket.getPnrSegId()));
					eTicketInfo.setStatus(ETicketStatusType.fromValue(eticket.getTicketStatus()));

					// If "O" then unused. Else used...
					eTicketInfo.setUsedStatus(EticketStatus.OPEN.code().equalsIgnoreCase(eticket.getTicketStatus())
							? ETicketUsedStatusType.UNUSED
							: ETicketUsedStatusType.USED);

					airTravelerType.getETicketInfo().getETicketInfomation().add(eTicketInfo);
				}
			}

			String[] ssrData = ReservationSSRUtil.getPaxSSR(reservationPaxDetailsDTO);
			if (ssrData[0] != null && !"".equals(ssrData[0]) && ssrData[0].length() == 4) { // Need to change schema to
																							// support non 4 letter SSR
																							// codes
				ssr = new SpecialServiceRequest();
				ssr.setSSRCode(ssrData[0]);
				ssr.setText(ssrData[1]);
				ssr.getTravelerRefNumberRPHList().clear();
				ssr.getTravelerRefNumberRPHList().add(airTravelerType.getTravelerRefNumber().getRPH());

				ssrs.getSpecialServiceRequest().add(ssr);

			}

			Collection<SSRInfoDTO> ssrInfoList = ReservationSSRUtil.getPaxAirportServices(reservationPaxDetailsDTO);

			seatRequests.addAll(getSeatRequests(reservation, reservationPaxDetailsDTO, airTravelerType.getTravelerRefNumber()
					.getRPH()));

			if (combinedMealsInReservation != null && combinedMealsInReservation.size() > 0) {
				mealRequests.addAll(getMealRequests(reservation, reservationPaxDetailsDTO, airTravelerType.getTravelerRefNumber()
						.getRPH(), combinedMealsInReservation));
			}

			ssrRequests.addAll(getAirportServiceRequests(airTravelerType.getTravelerRefNumber().getRPH(), reservation,
					ssrInfoList));

			baggageRequests.addAll(getBaggageRequests(reservation, reservationPaxDetailsDTO, airTravelerType
					.getTravelerRefNumber().getRPH()));
		}

		if ((ssrs.getSpecialServiceRequest() != null) && (ssrs.getSpecialServiceRequest().size() > 0)
				|| (seatRequests.size() > 0) || mealRequests.size() > 0 || ssrRequests.size() > 0 || baggageRequests.size() > 0
				|| (reservation.getReservationInsurance() != null)) {
			specialReqDetailsType = new SpecialReqDetailsType();
			if ((ssrs.getSpecialServiceRequest() != null) && (ssrs.getSpecialServiceRequest().size() > 0)) {
				specialReqDetailsType.setSpecialServiceRequests(ssrs);
			}
			if (seatRequests.size() > 0) {
				SeatRequests otaSeatRequests = new SeatRequests();
				for (SeatRequest seatRequest : seatRequests) {
					otaSeatRequests.getSeatRequest().add(seatRequest);
				}
				specialReqDetailsType.setSeatRequests(otaSeatRequests);
			}
			if (mealRequests.size() > 0) {
				MealRequests otaMealRequests = new MealRequests();
				for (MealRequest mealRequest : mealRequests) {
					otaMealRequests.getMealRequest().add(mealRequest);
				}
				specialReqDetailsType.setMealRequests(otaMealRequests);
			}
			if (ssrRequests.size() > 0) {
				SSRRequests otaSsrRequests = new SSRRequests();
				for (SSRRequest ssrRequest : ssrRequests) {
					otaSsrRequests.getSSRRequest().add(ssrRequest);
				}
				specialReqDetailsType.setSSRRequests(otaSsrRequests);
			}
			if (baggageRequests.size() > 0) {
				BaggageRequests otaBaggageRequests = new BaggageRequests();
				for (BaggageRequest baggagRequest : baggageRequests) {
					otaBaggageRequests.getBaggageRequest().add(baggagRequest);
				}
				specialReqDetailsType.setBaggageRequests(otaBaggageRequests);
			}

			if (reservation.getReservationInsurance() != null && !reservation.getReservationInsurance().isEmpty()) {
				ReservationInsurance resInsurance = reservation.getReservationInsurance().get(0);
				InsuranceRequests insuranceRequests = new InsuranceRequests();
				InsuranceRequest insuranceRequest = new InsuranceRequest();
				insuranceRequest.setDepartureDate(CommonUtil.parse(resInsurance.getDateOfTravel()));
				insuranceRequest.setArrivalDate(CommonUtil.parse(resInsurance.getDateOfReturn()));
				insuranceRequest.setDestination(resInsurance.getDestination());
				insuranceRequest.setOrigin(resInsurance.getOrigin());
				insuranceRequest.setRPH(resInsurance.getInsuranceId().toString());
				if (resInsurance != null) {
					if (resInsurance.getState().equals(ReservationInternalConstants.INSURANCE_STATES.SC.toString())) {
						insuranceRequest.setState("Success");
						insuranceRequest.setPolicyCode(resInsurance.getPolicyCode());
					} else if (resInsurance.getState().equals(ReservationInternalConstants.INSURANCE_STATES.OH.toString())) {
						insuranceRequest.setState("On Hold");
						insuranceRequest.setPolicyCode(resInsurance.getPolicyCode());
					} else if (resInsurance.getState().equals(ReservationInternalConstants.INSURANCE_STATES.FL.toString())) {
						insuranceRequest.setState("Failed");
					} else {
						insuranceRequest.setState("Processing");
					}
				}
				insuranceRequests.setInsuranceRequest(insuranceRequest);
				specialReqDetailsType.setInsuranceRequests(insuranceRequests);
			}

			travelerInfoType.getSpecialReqDetails().add(specialReqDetailsType);
		}

		return travelerInfoType;
	}

	private static List<SeatRequest> getSeatRequests(Reservation reservation, ReservationPax reservationPax, String travelereRPH)
			throws ModuleException {
		List<SeatRequest> seatRequests = new ArrayList<SeatRequest>();

		if (reservation.getSeats() != null && reservation.getSeats().size() > 0) {
			Collection<PaxSeatTO> allSeatsInReservation = reservation.getSeats();
			for (PaxSeatTO paxSeatTO : allSeatsInReservation) {
				if (paxSeatTO.getPnrPaxId().equals(reservationPax.getPnrPaxId())) {
					SeatRequest seatRequest = new SeatRequest();
					seatRequest.setSeatNumber(paxSeatTO.getSeatCode());
					seatRequest.getTravelerRefNumberRPHList().add(travelereRPH);
					for (ReservationSegmentDTO reservationSegmentDTO : (Collection<ReservationSegmentDTO>) reservation
							.getSegmentsView()) {
						if (paxSeatTO.getPnrSegId().equals(reservationSegmentDTO.getPnrSegId())) {
							seatRequest.setDepartureDate(CommonUtil.parse(reservationSegmentDTO.getDepartureDate()));
							seatRequest.setFlightNumber(reservationSegmentDTO.getFlightNo());
							seatRequest.getFlightRefNumberRPHList().add(reservationSegmentDTO.getFlightSegId().toString());
						}
					}

					seatRequests.add(seatRequest);
				}
			}
		}

		return seatRequests;
	}

	private static List<MealRequest> getMealRequests(Reservation reservation, ReservationPax reservationPax, String travelerRPH,
			Collection<PaxMealTO> combinedMealsInReservation) throws ModuleException {
		List<MealRequest> mealRequests = new ArrayList<MealRequest>();

		for (PaxMealTO paxMealTO : combinedMealsInReservation) {
			if (paxMealTO.getPnrPaxId().equals(reservationPax.getPnrPaxId())) {
				MealRequest mealRequest = new MealRequest();
				mealRequest.setMealCode(paxMealTO.getMealCode());
				mealRequest.setMealQuantity(paxMealTO.getAllocatedQty());
				mealRequest.getTravelerRefNumberRPHList().add(travelerRPH);
				for (ReservationSegmentDTO reservationSegmentDTO : (Collection<ReservationSegmentDTO>) reservation
						.getSegmentsView()) {
					if (paxMealTO.getPnrSegId().equals(reservationSegmentDTO.getPnrSegId())) {
						mealRequest.setDepartureDate(CommonUtil.parse(reservationSegmentDTO.getDepartureDate()));
						mealRequest.setFlightNumber(reservationSegmentDTO.getFlightNo());
						mealRequest.getFlightRefNumberRPHList().add(reservationSegmentDTO.getFlightSegId().toString());
					}
				}

				mealRequests.add(mealRequest);
			}
		}

		return mealRequests;
	}

	private static List<BaggageRequest> getBaggageRequests(Reservation reservation, ReservationPax reservationPax,
			String travelereRPH) throws ModuleException {
		List<BaggageRequest> baggageRequests = new ArrayList<BaggageRequest>();

		if (reservation.getBaggages() != null && reservation.getBaggages().size() > 0) {
			Collection<PaxBaggageTO> allBaggagesInReservation = reservation.getBaggages();
			for (PaxBaggageTO paxBaggageTO : allBaggagesInReservation) {
				if (paxBaggageTO.getPnrPaxId().equals(reservationPax.getPnrPaxId())) {
					BaggageRequest baggageRequest = new BaggageRequest();
					baggageRequest.setBaggageCode(paxBaggageTO.getBaggageName());
					baggageRequest.setBaggageOndGroupId(paxBaggageTO.getBaggageOndGroupId() != null ? paxBaggageTO
							.getBaggageOndGroupId() : "");
					baggageRequest.getTravelerRefNumberRPHList().add(travelereRPH);
					for (ReservationSegmentDTO reservationSegmentDTO : (Collection<ReservationSegmentDTO>) reservation
							.getSegmentsView()) {
						if (paxBaggageTO.getPnrSegId().equals(reservationSegmentDTO.getPnrSegId())) {
							baggageRequest.setDepartureDate(CommonUtil.parse(reservationSegmentDTO.getDepartureDate()));
							baggageRequest.setFlightNumber(reservationSegmentDTO.getFlightNo());
							baggageRequest.getFlightRefNumberRPHList().add(reservationSegmentDTO.getFlightSegId().toString());
						}
					}

					baggageRequests.add(baggageRequest);
				}
			}
		}

		return baggageRequests;
	}

	private static List<SSRRequest> getAirportServiceRequests(String travelerRPH, Reservation reservation,
			Collection<SSRInfoDTO> ssrInfoList) throws ModuleException {
		List<SSRRequest> ssrRequests = new ArrayList<SSRRequest>();

		if (ssrInfoList != null && ssrInfoList.size() > 0) {
			for (SSRInfoDTO ssrInfoDTO : ssrInfoList) {
				SSRRequest ssrRequest = new SSRRequest();
				ssrRequest.setSsrCode(ssrInfoDTO.getSSRCode());
				ssrRequest.setAirportCode(ssrInfoDTO.getAirportList());
				ssrRequest.setAirportType(ssrInfoDTO.getApplyOn());
				ssrRequest.getTravelerRefNumberRPHList().add(travelerRPH);

				Collection<ReservationSegmentDTO> segmentColle = reservation.getSegmentsView();

				for (ReservationSegmentDTO reservationSegmentDTO : segmentColle) {

					Date depaDate = reservationSegmentDTO.getDepartureDate();
					String flightNo = reservationSegmentDTO.getFlightNo();
					String flightSegRef = reservationSegmentDTO.getFlightSegId().toString();

					if (ssrInfoDTO.getPnrSegId().equals(reservationSegmentDTO.getPnrSegId())) {

						ssrRequest.setDepartureDate(CommonUtil.parse(depaDate));
						ssrRequest.setFlightNumber(flightNo);
						ssrRequest.getFlightRefNumberRPHList().add(flightSegRef);

					}
				}

				ssrRequest.setServiceType(SSRType.AIRPORT);

				ssrRequests.add(ssrRequest);
			}
		}

		return ssrRequests;
	}

	/**
	 * Return the Air Itinerary Information
	 * 
	 * @param reservation
	 * @return
	 * @throws WebservicesException
	 */
	private static AirItineraryType getAirItineraryType(Reservation reservation, boolean excludeCancelledSegments)
			throws WebservicesException, ModuleException {
		AirItineraryType airItineraryType = new AirItineraryType();
		OriginDestinationOptions originDestinationOptions = new OriginDestinationOptions();
		OriginDestinationOptionType originDestinationOptionType;
		ReservationSegmentDTO reservationSegmentDTO;
		BookFlightSegmentType bookFlightSegmentType;
		ArrivalAirport arrivalAirport;
		DepartureAirport departureAirport;
		Map<Integer, OriginDestinationOptionType> tmpOndOptionMap = new HashMap<Integer, OriginDestinationOptionType>();

		AirportBD airportBD = WebServicesModuleUtils.getAirportBD();
		Map mapAirportCodeIsGcc = getAirportCodeGCCMap(airportBD.getActiveAirports());

		for (Iterator itReservationSegmentDTO = reservation.getSegmentsView().iterator(); itReservationSegmentDTO.hasNext();) {
			reservationSegmentDTO = (ReservationSegmentDTO) itReservationSegmentDTO.next();

			if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationSegmentDTO.getStatus())
					&& excludeCancelledSegments) {
				// Exclude canceled segments
				continue;
			}

			departureAirport = new DepartureAirport();
			arrivalAirport = new ArrivalAirport();

			departureAirport.setLocationCode(getDepartureOrArrivalSegment(reservationSegmentDTO.getSegmentCode(), true));
			departureAirport.setTerminal((String) globalConfig.getAirportInfo(departureAirport.getLocationCode(), true)[2]);
			departureAirport.setCodeContext((String) mapAirportCodeIsGcc.get(getDepartureOrArrivalSegment(
					reservationSegmentDTO.getSegmentCode(), true)));

			arrivalAirport.setLocationCode(getDepartureOrArrivalSegment(reservationSegmentDTO.getSegmentCode(), false));
			arrivalAirport.setTerminal((String) globalConfig.getAirportInfo(arrivalAirport.getLocationCode(), true)[2]);
			arrivalAirport.setCodeContext((String) mapAirportCodeIsGcc.get(getDepartureOrArrivalSegment(
					reservationSegmentDTO.getSegmentCode(), false)));

			bookFlightSegmentType = new BookFlightSegmentType();
			bookFlightSegmentType.setDepartureAirport(departureAirport);
			bookFlightSegmentType.setArrivalAirport(arrivalAirport);

			// Short names for airports are referred by ATM and CDM
			FreeTextType airportAbbreviatedNames = new FreeTextType();
			airportAbbreviatedNames.setValue("airport_short_names:" + departureAirport.getLocationCode() + "="
					+ globalConfig.getAirportInfo(departureAirport.getLocationCode(), true)[1] + ","
					+ arrivalAirport.getLocationCode() + "="
					+ globalConfig.getAirportInfo(arrivalAirport.getLocationCode(), true)[1]);
			bookFlightSegmentType.getComment().add(airportAbbreviatedNames);

			bookFlightSegmentType.setFlightNumber(reservationSegmentDTO.getFlightNo());
			bookFlightSegmentType.setStatus(OTAUtils.getWSResSegStatus(reservationSegmentDTO.getStatus()));

			bookFlightSegmentType.setDepartureDateTime(CommonUtil.parse(reservationSegmentDTO.getDepartureDate()));
			bookFlightSegmentType.setArrivalDateTime(CommonUtil.parse(reservationSegmentDTO.getArrivalDate()));
			bookFlightSegmentType.setRPH(String.valueOf(reservationSegmentDTO.getPnrSegId()));
			bookFlightSegmentType.setResCabinClass((reservationSegmentDTO.getCabinClassCode() != null) ? reservationSegmentDTO
					.getCabinClassCode() : "");

			WSFlexiFareUtil.setSegmentFlexiFareAvailabilities(bookFlightSegmentType, reservationSegmentDTO, reservation);

			String[] segmentCodeList = reservationSegmentDTO.getSegmentCode().split("/");
			if (segmentCodeList.length > 2) {
				bookFlightSegmentType.setStopQuantity(BigInteger.valueOf(segmentCodeList.length - 2));
			}

			if (tmpOndOptionMap.containsKey(reservationSegmentDTO.getFareGroupId())) {
				originDestinationOptionType = tmpOndOptionMap.get(reservationSegmentDTO.getFareGroupId());
			} else {
				originDestinationOptionType = new OriginDestinationOptionType();
				originDestinationOptions.getOriginDestinationOption().add(originDestinationOptionType);
				tmpOndOptionMap.put(reservationSegmentDTO.getFareGroupId(), originDestinationOptionType);
			}
			originDestinationOptionType.getFlightSegment().add(bookFlightSegmentType);
		}

		airItineraryType.setOriginDestinationOptions(originDestinationOptions);
		tmpOndOptionMap.clear();
		return airItineraryType;
	}

	private static Map getAirportCodeGCCMap(Collection airports) {
		Map<String, String> mapAirportCodeGCCStatus = new HashMap<String, String>();
		Iterator itrAirport = airports.iterator();
		while (itrAirport.hasNext()) {
			Airport airport = (Airport) itrAirport.next();
			String isGcc = WebservicesConstants.GCC.NON_GCC;
			if (airport.getIsGcc().equalsIgnoreCase("Y")) {
				isGcc = WebservicesConstants.GCC.GCC;
			}
			mapAirportCodeGCCStatus.put(airport.getAirportCode(), isGcc);

		}

		return mapAirportCodeGCCStatus;
	}

	/**
	 * Prepares the reservation status
	 * 
	 * @param reservation
	 * @return
	 * @throws WebservicesException
	 */
	private static TicketingInfoType getTicketingInfo(Reservation reservation) throws WebservicesException, ModuleException {
		TicketingInfoType ticketingInfo = new TicketingInfoType();

		ticketingInfo.setTicketType(TicketType.E_TICKET);

		String otaTicketingStatus = null;
		String advisotryText = null;

		if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {
			if (reservation.getTotalAvailableBalance().doubleValue() > 0) {
				otaTicketingStatus = CommonUtil.getOTACodeValue(IOTACodeTables.TicketingStatus_TST_PARTIAL_TICKETED);
				advisotryText = "Reservation is partially confirmed. Has " + reservation.getTotalAvailableBalance()
						+ " balance amount to pay";
			} else {
				otaTicketingStatus = CommonUtil.getOTACodeValue(IOTACodeTables.TicketingStatus_TST_TICKETED);
				advisotryText = "Reservation is fully paid and confirmed.";
			}
		} else if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())) {
			otaTicketingStatus = CommonUtil.getOTACodeValue(IOTACodeTables.TicketingStatus_TST_TICKET_TIME_LIMIT_TTL);
			ticketingInfo.setTicketTimeLimit(CommonUtil.parse(reservation.getReleaseTimeStamps()[0]));
			advisotryText = "Reservation is onhold. To avoid cancellation, pay before "
					+ CommonUtil.getFormattedDate(reservation.getReleaseTimeStamps()[0]) + " GMT";
		} else if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
			otaTicketingStatus = CommonUtil.getOTACodeValue(IOTACodeTables.TicketingStatus_TST_CANCELLED);
			advisotryText = "Reservation is cancelled.";
		}

		ticketingInfo.setTicketingStatus(otaTicketingStatus);
		ticketingInfo.getTicketAdvisory().add(CommonUtil.getFreeTextType(advisotryText != null ? advisotryText : ""));

		return ticketingInfo;
	}

	/**
	 * Returns the departure segment or arrival segment
	 * 
	 * @param segmentCode
	 * @param isDepartureSegment
	 * @return
	 */
	private static String getDepartureOrArrivalSegment(String segmentCode, boolean isDepartureSegment) {
		if (isDepartureSegment) {
			return segmentCode.substring(0, 3);
		} else {
			return segmentCode.substring(segmentCode.length() - 3);
		}
	}

	/**
	 * Returns Meta OTAResRetrieveRS
	 * 
	 * @param readRQ
	 * @return
	 */
	private static OTAResRetrieveRS getMetaOTAResRetrieveRS(OTAReadRQ readRQ) {
		OTAResRetrieveRS oTAResRetrieveRS = new OTAResRetrieveRS();

		oTAResRetrieveRS.setTransactionIdentifier(readRQ.getTransactionIdentifier());
		oTAResRetrieveRS.setVersion(WebservicesConstants.OTAConstants.VERSION_ResSearch);
		oTAResRetrieveRS.setSequenceNmbr(readRQ.getSequenceNmbr());
		oTAResRetrieveRS.setEchoToken(readRQ.getEchoToken());
		oTAResRetrieveRS.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);

		return oTAResRetrieveRS;
	}

	/**
	 * Returns Meta OTAAirBookRS
	 * 
	 * @param readRQ
	 * @return
	 */
	private static OTAAirBookRS getMetaOTAAirBookRS(OTAReadRQ readRQ) {
		OTAAirBookRS oTAAirBookRS = new OTAAirBookRS();

		oTAAirBookRS.setTransactionIdentifier(readRQ.getTransactionIdentifier());
		oTAAirBookRS.setVersion(WebservicesConstants.OTAConstants.VERSION_ResSearch);
		oTAAirBookRS.setSequenceNmbr(readRQ.getSequenceNmbr());
		oTAAirBookRS.setEchoToken(readRQ.getEchoToken());
		oTAAirBookRS.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);

		return oTAAirBookRS;
	}

	/**
	 * Returns the Itinerary
	 * 
	 * @param oTAReadRQ
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public AAOTAItineraryRS getItinerary(OTAReadRQ oTAReadRQ, boolean includeAttachment) throws ModuleException,
			WebservicesException {
		AAOTAItineraryRS aaOTAItineraryRS = new AAOTAItineraryRS();
		;
		String itinerary = null;
		DataHandler dataHandler = null;

		String pnr = oTAReadRQ.getReadRequests().getReadRequest().get(0).getUniqueID().getID();
		ItineraryLayoutModesDTO itineraryLayoutModesDTO = new ItineraryLayoutModesDTO();
		itineraryLayoutModesDTO.setPnr(pnr);
		itineraryLayoutModesDTO.setLocale(new Locale("en"));
		itineraryLayoutModesDTO.setIncludePassengerPrices(true);
		itinerary = WebServicesModuleUtils.getReservationBD().getItineraryForPrint(itineraryLayoutModesDTO, null, null, false);

		if (includeAttachment) {
			if (itinerary != null) {
				try {
					String fileName = PlatformConstants.getAbsAttachmentsPath() + File.separatorChar + "Itinerary" + pnr
							+ ITINERARY_FILE_EXTN;
					FileOutputStream fos = new FileOutputStream(fileName);
					fos.write(itinerary.getBytes());
					fos.close();
					FileInputStream fis = new FileInputStream(fileName);
					byte[] istContentArr = new byte[itinerary.getBytes().length];
					fis.read(istContentArr);
					dataHandler = new DataHandler(new String(istContentArr), "text/html");

				} catch (Exception e) {
					log.error("error writing itinerary to html file ", e);
				}
			}
			aaOTAItineraryRS.getSuccessAndWarningsAndItineraryContentText().add(dataHandler);
		}
		aaOTAItineraryRS.getSuccessAndWarningsAndItineraryContentText().add(itinerary);
		aaOTAItineraryRS.getSuccessAndWarningsAndItineraryContentText().add(new SuccessType());

		return aaOTAItineraryRS;
	}

}