package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.opentravel.ota._2003._05.AirItineraryPricingInfoType;
import org.opentravel.ota._2003._05.AirItineraryPricingInfoType.PTCFareBreakdowns;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.AirItineraryType.OriginDestinationOptions;
import org.opentravel.ota._2003._05.AirTaxType;
import org.opentravel.ota._2003._05.AirTravelerType;
import org.opentravel.ota._2003._05.AirTravelerType.Document;
import org.opentravel.ota._2003._05.AirTravelerType.Telephone;
import org.opentravel.ota._2003._05.AirTravelerType.TravelerRefNumber;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.BookingPriceInfoType;
import org.opentravel.ota._2003._05.CompanyNameType;
import org.opentravel.ota._2003._05.FareType;
import org.opentravel.ota._2003._05.FareType.BaseFare;
import org.opentravel.ota._2003._05.FareType.Fees;
import org.opentravel.ota._2003._05.FareType.Taxes;
import org.opentravel.ota._2003._05.FareType.TotalFare;
import org.opentravel.ota._2003._05.FlightSegmentBaseType.ArrivalAirport;
import org.opentravel.ota._2003._05.FlightSegmentBaseType.DepartureAirport;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAAirBookRS.AirReservation;
import org.opentravel.ota._2003._05.OTACancelRQ;
import org.opentravel.ota._2003._05.OTACancelRS;
import org.opentravel.ota._2003._05.OTAReadRQ;
import org.opentravel.ota._2003._05.OTAReadRQ.ReadRequests;
import org.opentravel.ota._2003._05.OTAReadRQ.ReadRequests.ReadRequest;
import org.opentravel.ota._2003._05.OperatingAirlineType;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.POSType;
import org.opentravel.ota._2003._05.PTCFareBreakdownType;
import org.opentravel.ota._2003._05.PTCFareBreakdownType.TicketDesignators;
import org.opentravel.ota._2003._05.PTCFareBreakdownType.TicketDesignators.TicketDesignator;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.PersonNameType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SpecialRemarks;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SpecialRemarks.SpecialRemark;
import org.opentravel.ota._2003._05.SuccessType;
import org.opentravel.ota._2003._05.TicketType;
import org.opentravel.ota._2003._05.TicketingInfoType;
import org.opentravel.ota._2003._05.TransactionStatusType;
import org.opentravel.ota._2003._05.TravelerInfoType;
import org.opentravel.ota._2003._05.UniqueIDType;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.baggage.BaggageRq;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO.LCCAncillaryType;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ChargesDetailDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ReservationDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.auditor.api.dto.ReservationModificationDTO;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.api.util.WebservicesConstants.MyIDTravelConstants;
import com.isa.thinair.webservices.core.bl.AmadeusCommonUtil;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;

/**
 * @author Janaka Padukka
 * 
 */
public class MyIDTrvlCommonUtil {

	/**
	 * Create a temporary flight reference number
	 * 
	 * @param aaFlightSegmentDTO
	 * @return
	 */
	public static String getTmpSegRefNo(FlightSegmentDTO aaFlightSegmentDTO) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
		String origin;
		String destination;

		if (aaFlightSegmentDTO.getSegmentCode() != null) {

			String[] originDest = aaFlightSegmentDTO.getSegmentCode().split("/");
			origin = originDest[0];
			destination = originDest[originDest.length - 1];

		} else {
			origin = aaFlightSegmentDTO.getFromAirport();
			destination = aaFlightSegmentDTO.getToAirport();
		}

		return aaFlightSegmentDTO.getFlightNumber() + origin + destination
				+ sdf.format(aaFlightSegmentDTO.getDepartureDateTime()) + sdf.format(aaFlightSegmentDTO.getArrivalDateTime());

	}

	public static Reservation getReservation(String pnr) throws WebservicesException, ModuleException {
		// Load pnr and validate segments and pax info
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadLastUserNote(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadEtickets(true);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);

		Reservation reservation = WebServicesModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);

		if (reservation == null) {
			throw new WebservicesException(IOTACodeTables.MyidtravelErrorCodes_MYD_PNR_NOT_FOUND,
					"No matching booking found: PNR - " + pnr);
		}

		return reservation;
	}

	public static LCCClientReservation getLccReservation(String pnr) throws WebservicesException, ModuleException {
		// Load pnr and validate segments and pax info
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadLastUserNote(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadEtickets(true);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);

		LCCClientReservation reservation = WebServicesModuleUtils.getAirproxyReservationBD().searchReservationByPNR(pnrModesDTO,
				null, getTrackInfo());

		if (reservation == null) {
			throw new WebservicesException(IOTACodeTables.MyidtravelErrorCodes_MYD_PNR_NOT_FOUND,
					"No matching booking found: PNR - " + pnr);
		}

		return reservation;
	}

	public static Object getPricingInfo(Reservation reservation, Map<Integer, String> flightRPHMap,
			Map<Integer, String> paxRPHMap, boolean isPricing, Integer pnrPaxId) throws ModuleException, WebservicesException {
		AirItineraryPricingInfoType airItineraryPricingInfo = null;
		BookingPriceInfoType bookingPriceInfoType = null;

		if (isPricing) {
			airItineraryPricingInfo = new AirItineraryPricingInfoType();
		} else {
			bookingPriceInfoType = new BookingPriceInfoType();
		}

		BigDecimal[] perPaxCreditCardCharges = null;
		ExternalChgDTO ccCharge = null;
		BigDecimal ccFee = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (isPricing) {
			int totalAdultChildCount = reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount();
			BigDecimal calculatedPaymentAmount = ReservationUtil.getTotalBalToPay(reservation);
			ccCharge = WSReservationUtil.getCCChargeAmount(calculatedPaymentAmount, totalAdultChildCount, reservation
					.getSegmentsView().size(), ChargeRateOperationType.MAKE_ONLY);
			ccFee = ccCharge.getAmount();

			ExternalChgDTO jnOther = getJNOtherTax(reservation, ccCharge);

			if (jnOther != null) {
				ccFee = AccelAeroCalculator.add(ccFee, jnOther.getAmount());
			}

			perPaxCreditCardCharges = AccelAeroCalculator.roundAndSplit(ccFee, totalAdultChildCount);

			// Handling fee not handled

		}

		FareType fareType = new FareType();
		if (isPricing) {
			airItineraryPricingInfo.setItinTotalFare(fareType);
		} else {
			bookingPriceInfoType.setItinTotalFare(fareType);
		}
		// Setting the base fare
		BaseFare baseFare = new BaseFare();
		fareType.setBaseFare(baseFare);
		OTAUtils.setAmountAndDefaultCurrency(baseFare, reservation.getTotalTicketFare());

		// Setting the taxes
		Taxes taxes = new Taxes();
		fareType.setTaxes(taxes);
		AirTaxType airTaxType = new AirTaxType();
		taxes.getTax().add(airTaxType);
		airTaxType.setTaxCode("TOTALTAX");
		BigDecimal totalTax = AccelAeroCalculator.add(reservation.getTotalTicketSurCharge(),
				reservation.getTotalTicketTaxCharge(), reservation.getTotalTicketCancelCharge(),
				reservation.getTotalTicketModificationCharge(), reservation.getTotalTicketAdjustmentCharge(), ccFee);
		OTAUtils.setAmountAndDefaultCurrency(airTaxType, totalTax);

		// Setting the total charge amount
		TotalFare totalFare = new TotalFare();
		fareType.setTotalFare(totalFare);
		OTAUtils.setAmountAndDefaultCurrency(totalFare,
				AccelAeroCalculator.add((AccelAeroCalculator.add(reservation.getTotalChargeAmounts())), ccFee));

		int paxCounter = 0;

		PTCFareBreakdowns ptcBreakdowns = new PTCFareBreakdowns();

		if (isPricing) {
			airItineraryPricingInfo.setPTCFareBreakdowns(ptcBreakdowns);
		} else {
			bookingPriceInfoType.setPTCFareBreakdowns(ptcBreakdowns);
		}

		PTCFareBreakdownType ptcBreakdown;
		FareType fare;
		TotalFare paxTotalFare;
		AirTaxType tax;
		PTCFareBreakdownType.TravelerRefNumber travelerRPH;

		Set<ReservationPax> passengers = reservation.getPassengers();

		for (ReservationPax passenger : passengers) {

			if (pnrPaxId != null && !passenger.getPnrPaxId().equals(pnrPaxId)) {
				continue;
			}

			ptcBreakdown = new PTCFareBreakdownType();
			ptcBreakdowns.getPTCFareBreakdown().add(ptcBreakdown);
			ptcBreakdown.setPassengerTypeQuantity(new PassengerTypeQuantityType());

			// Identifying the passenger type - ADULT
			if (ReservationApiUtils.isAdultType(passenger)) {
				ptcBreakdown.getPassengerTypeQuantity().setCode(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
				travelerRPH = new PTCFareBreakdownType.TravelerRefNumber();

				if (isPricing) {
					String paxType = paxRPHMap.get(passenger.getPaxSequence());
					if (!CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT).equals(paxType)) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Inconsistent passenger information");
					}
				}
				travelerRPH.setRPH(passenger.getPaxSequence().toString());
				ptcBreakdown.getTravelerRefNumber().add(travelerRPH);
				// Identifying the passenger type - CHILD
			} else if (ReservationApiUtils.isChildType(passenger)) {
				ptcBreakdown.getPassengerTypeQuantity().setCode(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
				travelerRPH = new PTCFareBreakdownType.TravelerRefNumber();

				if (isPricing) {
					String paxType = paxRPHMap.get(passenger.getPaxSequence());
					if (!CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD).equals(paxType)) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Inconsistent passenger information");
					}
				}
				travelerRPH.setRPH(passenger.getPaxSequence().toString());
				ptcBreakdown.getTravelerRefNumber().add(travelerRPH);

				// Identifying the passenger type - INFANT
			} else if (ReservationApiUtils.isInfantType(passenger)) {
				ptcBreakdown.getPassengerTypeQuantity().setCode(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
				travelerRPH = new PTCFareBreakdownType.TravelerRefNumber();

				if (isPricing) {
					String paxType = paxRPHMap.get(passenger.getPaxSequence());
					if (!CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT).equals(paxType)) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Inconsistent passenger information");
					}
				}
				travelerRPH.setRPH(passenger.getPaxSequence().toString());
				ptcBreakdown.getTravelerRefNumber().add(travelerRPH);

			} else {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Passenger Type");
			}

			TicketDesignators ticketDesignators = new TicketDesignators();

			Set<ReservationPaxFare> fares = passenger.getPnrPaxFares();
			for (ReservationPaxFare paxFare : fares) {
				for (ReservationPaxFareSegment paxFareSegment : paxFare.getPaxFareSegments()) {

					String flightRPH = null;
					TicketDesignator ticketDesignator = new TicketDesignator();
					if (isPricing) {
						flightRPH = flightRPHMap.get(paxFareSegment.getSegment().getFlightSegId());
						if (flightRPH == null) {
							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
									"Inconsistent flight information");
						}
					} else {
						flightRPH = paxFareSegment.getSegment().getFlightSegId().toString();
					}
					ticketDesignator.setFlightRefRPH(flightRPH);
					ticketDesignators.getTicketDesignator().add(ticketDesignator);
				}
			}
			ptcBreakdown.setTicketDesignators(ticketDesignators);

			ptcBreakdown.getPassengerTypeQuantity().setQuantity(Integer.valueOf(1));
			fare = new FareType();
			ptcBreakdown.setPassengerFare(fare);

			PTCFareBreakdownType.FareBasisCodes fareBasisCodes = new PTCFareBreakdownType.FareBasisCodes();
			fareBasisCodes.getFareBasisCode().add("P");
			ptcBreakdown.setFareBasisCodes(fareBasisCodes);

			// Setting the base fare
			fare.setBaseFare(new BaseFare());
			OTAUtils.setAmountAndDefaultCurrency(fare.getBaseFare(), passenger.getTotalFare());

			fare.setTaxes(new Taxes());
			fare.setFees(new Fees());

			List<String> bookingClasses = new ArrayList<String>();
			BookingClassBD bookingClassBD = WebServicesModuleUtils.getBookingClassBD();

			Collection<ChargesDetailDTO> chargesDetailDTOs = passenger.getOndChargesView();
			for (ChargesDetailDTO chargesDetailDTO : chargesDetailDTOs) {
				if (!chargesDetailDTO.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.FAR)) {
					tax = new AirTaxType();
					fare.getTaxes().getTax().add(tax);
					tax.setTaxName(chargesDetailDTO.getChargeDescription());
					tax.setTaxCode(chargesDetailDTO.getChargeCode());
					OTAUtils.setAmountAndDefaultCurrency(tax, chargesDetailDTO.getChargeAmount());
				} else {

					if (chargesDetailDTO.getBookingCodes() != null) {
						bookingClasses.addAll(chargesDetailDTO.getBookingCodes());
					}
				}
			}

			if (bookingClasses.size() > 0) {
				boolean isHoldAllowed = true;
				Map<String, BookingClass> bcMap = bookingClassBD.getBookingClassesMap(bookingClasses);
				if (bcMap != null && bcMap.size() > 0) {
					for (BookingClass bc : bcMap.values()) {
						if (bc.getOnHold()) {
							isHoldAllowed = false;
							break;
						}
					}
				}

				if (!isHoldAllowed) {
					fareBasisCodes.getFareBasisCode().add("NH");
				}
			}

			BigDecimal perPaxTotalCharge = ReservationApiUtils.getTotalChargeAmount(passenger.getTotalChargeAmounts());
			// CC Charge setting for price quote
			if (!ReservationApiUtils.isInfantType(passenger) && perPaxCreditCardCharges != null && ccCharge != null) {
				perPaxTotalCharge = AccelAeroCalculator.add(perPaxTotalCharge, perPaxCreditCardCharges[paxCounter]);
				tax = new AirTaxType();
				fare.getTaxes().getTax().add(tax);
				tax.setTaxName(ccCharge.getChargeDescription());
				tax.setTaxCode(ccCharge.getChargeCode());
				OTAUtils.setAmountAndDefaultCurrency(tax, perPaxCreditCardCharges[paxCounter]);
				paxCounter++;
			}

			// Setting the total ticket amount
			paxTotalFare = new TotalFare();
			fare.setTotalFare(paxTotalFare);
			OTAUtils.setAmountAndDefaultCurrency(paxTotalFare, perPaxTotalCharge);
		}

		if (isPricing) {
			return airItineraryPricingInfo;
		} else {
			return bookingPriceInfoType;
		}

	}

	public static OTAAirBookRS generateOTAAirBookRS(OTAReadRQ readRequest, boolean isExternalRead, boolean isEticketRetrieve,
			boolean excludeCancelledSegs) throws ModuleException, WebservicesException {

		String retEticket = null;
		String pnr = null;

		if (isEticketRetrieve) {
			retEticket = readRequest.getReadRequests().getAirReadRequest().get(0).getTicketNumber().getETicketNumber();
			retEticket = retEticket.replace("-", "");
			ReservationSearchDTO searchDTO = new ReservationSearchDTO();
			searchDTO.setETicket(retEticket);
			Collection<ReservationDTO> reservations = WebServicesModuleUtils.getReservationQueryBD().getReservations(searchDTO);

			if (reservations == null || reservations.isEmpty()) {
				throw new WebservicesException(IOTACodeTables.MyidtravelErrorCodes_MYD_TICKET_NOT_FOUND,
						"No matching ticket found: Eticket - " + retEticket);
			}

			ReservationDTO firstReservation = PlatformUtiltiies.getFirstElement(reservations);
			pnr = firstReservation.getPnr();

		} else {
			pnr = readRequest.getReadRequests().getReadRequest().get(0).getUniqueID().getID();
		}

		Reservation reservation = getReservation(pnr);

		// TODO: Extra validation
		if (isExternalRead) {
			// check for surname verification
		}

		OTAAirBookRS otaAirBookRS = new OTAAirBookRS();
		otaAirBookRS.setVersion(readRequest.getVersion());
		otaAirBookRS.setSequenceNmbr(readRequest.getSequenceNmbr());
		otaAirBookRS.setEchoToken(readRequest.getEchoToken());
		otaAirBookRS.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);

		AirReservation airReservation = new AirReservation();
		otaAirBookRS.getSuccessAndWarningsAndAirReservation().add(airReservation);

		// Setting the booking reference
		UniqueIDType uniqueIDType = new UniqueIDType();
		uniqueIDType.setID(reservation.getPnr());
		uniqueIDType.setType(CommonUtil.getOTACodeValue(IOTACodeTables.UniqueIdType_UIT_RESERVATION));

		CompanyNameType company = new CompanyNameType();
		company.setCode(AppSysParamsUtil.getDefaultCarrierCode());
		uniqueIDType.setCompanyName(company);
		airReservation.getBookingReferenceID().add(uniqueIDType);

		Set<ReservationPax> passengers = reservation.getPassengers();

		BigDecimal availableBalance = AccelAeroCalculator.getDefaultBigDecimalZero();
		Integer selectedPnrPaxId = null;
		Collection<EticketTO> selectedEtickets = null;

		if (isEticketRetrieve) {

			for (ReservationPax passenger : passengers) {
				Collection<EticketTO> etickets = passenger.geteTickets();
				if (etickets != null && !etickets.isEmpty()) {
					for (EticketTO eticket : etickets) {

						if (eticket.getEticketNumber().equals(retEticket)) {
							availableBalance = passenger.getTotalAvailableBalance();
							selectedPnrPaxId = passenger.getPnrPaxId();
							selectedEtickets = etickets;
							break;
						}
					}
				}
			}

			TicketingInfoType ticketingInfo = new TicketingInfoType();
			ticketingInfo.setTicketType(TicketType.E_TICKET);
			ticketingInfo.setTicketingStatus("1");
			airReservation.getTicketing().add(ticketingInfo);
		} else {

			for (ReservationPax passenger : passengers) {
				Collection<EticketTO> etickets = passenger.geteTickets();
				String eticketNumber = null;
				Integer minCoupon = null;
				String cnxEticketNumber = null;
				if (etickets != null && !etickets.isEmpty()) {
					for (EticketTO eticket : etickets) {
						if (!eticket.getTicketStatus().equals(EticketStatus.CLOSED.code())) {

							if ((minCoupon == null || minCoupon.compareTo(eticket.getCouponNo()) > 0)) {
								minCoupon = eticket.getCouponNo();
								eticketNumber = eticket.getEticketNumber();
							}
						} else {
							cnxEticketNumber = eticket.getEticketNumber();
						}
					}
				}

				eticketNumber = (eticketNumber != null ? eticketNumber : cnxEticketNumber);
				TicketingInfoType ticketingInfo = new TicketingInfoType();
				ticketingInfo.setTicketType(TicketType.E_TICKET);
				ticketingInfo.getTravelerRefNumber().add(passenger.getPaxSequence().toString());
				ticketingInfo.getTicketAdvisory().add(
						CommonUtil.getFreeTextType(new StringBuffer(eticketNumber).insert(3, "-").toString()));
				airReservation.getTicketing().add(ticketingInfo);

			}
		}

		airReservation.setAirItinerary(getAirItineraryType(reservation, isEticketRetrieve, excludeCancelledSegs,
				availableBalance, selectedEtickets));
		// Set TravelerInfo
		airReservation.setTravelerInfo(getTravelerInfoType(reservation, selectedPnrPaxId));
		// Set price breakdown
		BookingPriceInfoType priceInfo = (BookingPriceInfoType) getPricingInfo(reservation, null, null, false, selectedPnrPaxId);
		airReservation.setPriceInfo(priceInfo);
		otaAirBookRS.getSuccessAndWarningsAndAirReservation().add(new SuccessType());

		return otaAirBookRS;
	}

	public static TrackInfoDTO getTrackInfo() {
		String sessionId = (String) ThreadLocalData.getWSContextParam(WebservicesContext.WEB_SESSION_ID);
		String ipAddress = BeanUtils.nullHandler(ThreadLocalData.getWSContextParam(
				WebservicesContext.WEB_REQUEST_IP));

		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();

		TrackInfoDTO trackInfo = new TrackInfoDTO();
		trackInfo.setAppIndicator(AppIndicatorEnum.APP_WS);
		trackInfo.setSessionId(sessionId);
		trackInfo.setPaymentAgent(principal.getAgentCode());
		trackInfo.setCallingInstanceId(principal.getCallingInstanceId());
		trackInfo.setIpAddress(ipAddress);
		if (ipAddress.length() > 0) {
			long ipNumber = AmadeusCommonUtil.getReservationOriginIPAddress(ipAddress);
			trackInfo.setOriginIPNumber(ipNumber);
		}
		trackInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());

		return trackInfo;
	}

	private static AirItineraryType getAirItineraryType(Reservation reservation, boolean eticketRetrieval,
			boolean excludeCancelled, BigDecimal availableBalance, Collection<EticketTO> selectedEtickets)
			throws WebservicesException, ModuleException {
		AirItineraryType airItineraryType = new AirItineraryType();
		OriginDestinationOptions originDestinationOptions = new OriginDestinationOptions();
		OriginDestinationOptionType originDestinationOptionType;
		BookFlightSegmentType bookFlightSegmentType;
		ArrivalAirport arrivalAirport;
		DepartureAirport departureAirport;

		Collection<ReservationSegmentDTO> reservationSegments = reservation.getSegmentsView();

		Map<Integer, String> eticketStatusMap = new HashMap<Integer, String>();
		boolean ticketStatusByEticket = false;
		if (selectedEtickets != null) {
			for (EticketTO eticket : selectedEtickets) {
				if (EticketStatus.OPEN.code().equals(eticket.getTicketStatus())
						|| EticketStatus.FLOWN.code().equals(eticket.getTicketStatus())) {
					eticketStatusMap.put(eticket.getPnrSegId(), eticket.getTicketStatus());
				}
			}
			ticketStatusByEticket = WebServicesModuleUtils.getMyIDTravelConfig().getTicketStatusByEticketEnabled();
		}

		for (ReservationSegmentDTO reservationSegment : reservationSegments) {

			if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationSegment.getStatus())
					&& excludeCancelled) {
				continue;
			}

			departureAirport = new DepartureAirport();
			arrivalAirport = new ArrivalAirport();

			departureAirport.setLocationCode(getDepartureOrArrivalSegment(reservationSegment.getSegmentCode(), true));
			departureAirport.setCodeContext(WebservicesConstants.OTAConstants.CODE_CONTEXT_IATA);

			arrivalAirport.setLocationCode(getDepartureOrArrivalSegment(reservationSegment.getSegmentCode(), false));
			arrivalAirport.setCodeContext(WebservicesConstants.OTAConstants.CODE_CONTEXT_IATA);

			bookFlightSegmentType = new BookFlightSegmentType();
			bookFlightSegmentType.setDepartureAirport(departureAirport);
			bookFlightSegmentType.setArrivalAirport(arrivalAirport);

			if (eticketRetrieval) {

				// If cancelled, with credit open
				// If cancelled, without credit refunded
				// If departure date over and not cancelled used
				// If departure date not over and not cancelled locked

				if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationSegment.getStatus())) {

					if (ticketStatusByEticket) {
						String eticketStatus = eticketStatusMap.get(reservationSegment.getPnrSegId());

						if (eticketStatus != null) {
							if (EticketStatus.OPEN.code().equals(eticketStatus)) {
								bookFlightSegmentType.setStatus(MyIDTravelConstants.TICKET_STATUS_OPEN);
							} else if (EticketStatus.FLOWN.code().equals(eticketStatus)) {
								bookFlightSegmentType.setStatus(MyIDTravelConstants.TICKET_STATUS_USED);
							}
						} else {
							bookFlightSegmentType.setStatus(MyIDTravelConstants.TICKET_STATUS_LOCKED);
						}

					} else {
						if (reservationSegment.getZuluDepartureDate().after(CalendarUtil.getCurrentSystemTimeInZulu())) {
							bookFlightSegmentType.setStatus(MyIDTravelConstants.TICKET_STATUS_LOCKED);
						} else {
							bookFlightSegmentType.setStatus(MyIDTravelConstants.TICKET_STATUS_USED);
						}
					}

				} else if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationSegment.getStatus())) {

					if (availableBalance.compareTo(BigDecimal.ZERO) == 0) {
						bookFlightSegmentType.setStatus(MyIDTravelConstants.TICKET_STATUS_REFUNDED);
					} else if (availableBalance.compareTo(BigDecimal.ZERO) < 0) {
						bookFlightSegmentType.setStatus(MyIDTravelConstants.TICKET_STATUS_OPEN);
					} else if (availableBalance.compareTo(BigDecimal.ZERO) > 0) {
						bookFlightSegmentType.setStatus(MyIDTravelConstants.TICKET_STATUS_LOCKED);
					}
				}

			} else {
				if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationSegment.getStatus())) {
					if (reservationSegment.isStandBySegment()) {
						bookFlightSegmentType.setStatus(CommonUtil
								.getOTACodeValue(IOTACodeTables.Status_STS_LIST_SPACE_AVAILABLE));
					} else {
						bookFlightSegmentType.setStatus(CommonUtil.getOTACodeValue(IOTACodeTables.Status_STS_OK));
					}
				} else if (ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationSegment.getStatus())) {
					bookFlightSegmentType.setStatus(CommonUtil.getOTACodeValue(IOTACodeTables.Status_STS_CANCELLED));
				}
			}

			bookFlightSegmentType.setDepartureDateTime(CommonUtil.parse(reservationSegment.getDepartureDate()));
			bookFlightSegmentType.setArrivalDateTime(CommonUtil.parse(reservationSegment.getArrivalDate()));
			bookFlightSegmentType.setRPH(String.valueOf(reservationSegment.getPnrSegId()));
			bookFlightSegmentType.setResBookDesigCode(reservationSegment.getFareTO().getBookingClassCode());

			OperatingAirlineType operatingAirline = new OperatingAirlineType();
			operatingAirline.setCode(reservationSegment.getFlightNo().substring(0, 2));
			operatingAirline.setFlightNumber(reservationSegment.getFlightNo().substring(2));
			bookFlightSegmentType.setOperatingAirline(operatingAirline);

			String[] segmentCodeList = reservationSegment.getSegmentCode().split("/");
			if (segmentCodeList.length > 2) {
				bookFlightSegmentType.setStopQuantity(BigInteger.valueOf(segmentCodeList.length - 2));
			}

			// We don't serve connection fares, hence no need of grouping
			// segments
			originDestinationOptionType = new OriginDestinationOptionType();
			originDestinationOptions.getOriginDestinationOption().add(originDestinationOptionType);
			originDestinationOptionType.getFlightSegment().add(bookFlightSegmentType);
		}

		airItineraryType.setOriginDestinationOptions(originDestinationOptions);
		return airItineraryType;
	}

	private static String getDepartureOrArrivalSegment(String segmentCode, boolean isDepartureSegment) {
		if (isDepartureSegment) {
			return segmentCode.substring(0, 3);
		} else {
			return segmentCode.substring(segmentCode.length() - 3);
		}
	}

	/**
	 * Return the travellers information
	 * 
	 * @param reservation
	 * @param pnrPaxId
	 *            TODO
	 * @return
	 * @throws WebservicesException
	 */
	private static TravelerInfoType getTravelerInfoType(Reservation reservation, Integer pnrPaxId) throws ModuleException,
			WebservicesException {
		TravelerInfoType travelerInfoType = new TravelerInfoType();
		AirTravelerType airTravelerType;
		PersonNameType personNameType;
		Telephone telephone;

		String paxTitle = null;
		Document doc;

		// Any MyID booking will have a user note, we are retrieving first user
		// note, which tracks
		// the booking from MyIDTravel end.
		List<ReservationModificationDTO> colUserNotes = (List<ReservationModificationDTO>) AirproxyModuleUtils
				.getAirReservationQueryBD().getUserNotes(reservation.getPnr(), true);

		SpecialReqDetailsType specialReqDetailsType = new SpecialReqDetailsType();
		specialReqDetailsType.setSpecialRemarks(new SpecialRemarks());
		SpecialRemark specialRemark = new SpecialRemark();
		specialRemark.setText(colUserNotes.get(colUserNotes.size() - 1).getUserNote());
		specialRemark.setRemarkType(WebservicesConstants.MyIDTravelConstants.REMARK_TYPE);
		specialReqDetailsType.getSpecialRemarks().getSpecialRemark().add(specialRemark);
		travelerInfoType.getSpecialReqDetails().add(specialReqDetailsType);

		Set<ReservationPax> passengers = reservation.getPassengers();

		for (ReservationPax passenger : passengers) {

			if (pnrPaxId != null && !passenger.getPnrPaxId().equals(pnrPaxId)) {
				continue;
			}

			airTravelerType = new AirTravelerType();
			airTravelerType.setTravelerRefNumber(new TravelerRefNumber());
			paxTitle = null;
			doc = new Document();

			// Adult
			if (ReservationInternalConstants.PassengerType.ADULT.equals(passenger.getPaxType())) {
				airTravelerType.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
				// Child
			} else if (ReservationInternalConstants.PassengerType.CHILD.equals(passenger.getPaxType())) {
				airTravelerType.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
				// Infant
			} else if (ReservationInternalConstants.PassengerType.INFANT.equals(passenger.getPaxType())) {
				airTravelerType.setPassengerTypeCode(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));

			} else {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Passenger Type");
			}

			airTravelerType.getTravelerRefNumber().setRPH(passenger.getPaxSequence().toString());
			paxTitle = CommonUtil.getWSPaxTitle(passenger.getTitle());

			personNameType = new PersonNameType();
			if (paxTitle != null) {
				personNameType.getNameTitle().add(paxTitle);
			}
			personNameType.setSurname(passenger.getLastName());
			personNameType.getGivenName().add(passenger.getFirstName());
			airTravelerType.setPersonName(personNameType);

			telephone = new Telephone();
			telephone.setPhoneNumber(reservation.getContactInfo().getPhoneNo());
			airTravelerType.getTelephone().add(telephone);

			ReservationPaxAdditionalInfo addInfo = passenger.getPaxAdditionalInfo();

			if (passenger.getNationalityCode() != null) {
				Nationality nationality = WebServicesModuleUtils.getCommonMasterBD().getNationality(
						passenger.getNationalityCode());
				if ((nationality != null) && (nationality.getIsoCode() != null)) {
					doc.setDocHolderNationality(nationality.getIsoCode());
					airTravelerType.getDocument().add(doc);
					if (addInfo != null) {
						if (addInfo.getPassportNo() != null) {
							doc.setDocID(addInfo.getPassportNo());
							doc.setDocType(CommonsConstants.SSRCode.PSPT.toString());
							if (addInfo.getPassportIssuedCntry() != null) {
								doc.setDocIssueCountry(addInfo.getPassportIssuedCntry());
							}
							if (addInfo.getPassportExpiry() != null) {
								doc.setExpireDate(CommonUtil.parse(addInfo.getPassportExpiry()));
							}
						}
					}
				}
			}

			if (addInfo.getEmployeeID() != null) {
				doc = new Document();
				doc.setDocID(addInfo.getEmployeeID());
				doc.setDocType(CommonsConstants.SSRCode.ID.toString());
				if (addInfo.getDateOfJoin() != null) {
					doc.setEffectiveDate(CommonUtil.parse(addInfo.getDateOfJoin()));
				}
				if (addInfo.getIdCategory() != null) {
					doc.setDocIssueAuthority(addInfo.getIdCategory());
				}
				airTravelerType.getDocument().add(doc);
			}

			travelerInfoType.getAirTraveler().add(airTravelerType);
			SpecialRemark specialRemarkPax = new SpecialRemark();
			org.opentravel.ota._2003._05.SpecialRemarkType.TravelerRefNumber refNumber = new org.opentravel.ota._2003._05.SpecialRemarkType.TravelerRefNumber();
			refNumber.setRPH(passenger.getPaxSequence().toString());
			specialRemarkPax.getTravelerRefNumber().add(refNumber);
			specialRemarkPax.setText(addInfo.getIdCategory());
			specialRemarkPax.setRemarkType(WebservicesConstants.MyIDTravelConstants.REMARK_TYPE);
			specialReqDetailsType.getSpecialRemarks().getSpecialRemark().add(specialRemarkPax);
		}

		return travelerInfoType;
	}

	@SuppressWarnings("unchecked")
	public static OTACancelRS cancelBooking(OTACancelRQ otaCancelRQ) throws ModuleException, WebservicesException {

		String pnr = otaCancelRQ.getUniqueID().get(0).getID();
		Reservation reservation = getReservation(pnr);

		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

		WSReservationUtil.authorize(privilegeKeys, null, PrivilegesKeys.AlterResPrivilegesKeys.CANCEL_RES);

		if (!ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
			ModifyBookingUtil.authorizeModification(privilegeKeys, reservation,
					PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX, null);
			CustomChargesTO customChargesTO = new CustomChargesTO(null, null, null, null, null, null);
			WebServicesModuleUtils.getReservationBD().cancelReservation(pnr, customChargesTO, reservation.getVersion(), false,
					false, false, null, false, null, GDSInternalCodes.GDSNotifyAction.NO_ACTION.getCode(), null);
		}

		OTACancelRS otaCancelRS = new OTACancelRS();
		otaCancelRS.setEchoToken(otaCancelRQ.getEchoToken());
		otaCancelRS.setVersion(otaCancelRQ.getVersion());
		otaCancelRS.setSuccess(new SuccessType());
		otaCancelRS.setStatus(TransactionStatusType.CANCELLED);
		otaCancelRS.setTimeStamp(CommonUtil.parse(new Date()));
		otaCancelRS.getUniqueID().add(otaCancelRQ.getUniqueID().get(0));

		return otaCancelRS;
	}

	public static OTAReadRQ generateReadRQ(String pnr, POSType pos, BigInteger sequenceNumber, String echoToken)
			throws ModuleException {
		OTAReadRQ readRQ = new OTAReadRQ();
		readRQ.setPOS(pos);
		readRQ.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		readRQ.setVersion(WebservicesConstants.OTAConstants.VERSION_ResSearch);
		readRQ.setSequenceNmbr(sequenceNumber);
		readRQ.setEchoToken(echoToken);
		readRQ.setTimeStamp(CommonUtil.parse(new Date()));

		ReadRequest resReadRQ = null;
		readRQ.setReadRequests(new ReadRequests());
		readRQ.getReadRequests().getReadRequest().add(resReadRQ = new ReadRequest());

		resReadRQ.setUniqueID(new UniqueIDType());
		resReadRQ.getUniqueID().setType(CommonUtil.getOTACodeValue(IOTACodeTables.UniqueIdType_UIT_RESERVATION));
		resReadRQ.getUniqueID().setID(pnr);

		return readRQ;
	}

	public static Map<Integer, List<LCCClientExternalChgDTO>> getPaxwiseBaggageExternalChargesForRequote(
			List<FlightSegmentDTO> flightSegments, List<Integer> adultChildList, String quotedBookingClass)
			throws ModuleException, WebservicesException {

		BookingClass bookingClass = WebServicesModuleUtils.getBookingClassBD().getBookingClass(quotedBookingClass);

		List<FlightSegmentTO> baggageQuotingSegments = new ArrayList<FlightSegmentTO>();
		Map<String, String> cosMap = new HashMap<String, String>();
		Map<String, String> bcMap = new HashMap<String, String>();

		for (FlightSegmentDTO flightSegment : flightSegments) {
			FlightSegmentTO bagFlightSeg = new FlightSegmentTO();
			bagFlightSeg.setFlightNumber(flightSegment.getFlightNumber());
			bagFlightSeg.setDepartureDateTime(flightSegment.getDepartureDateTime());
			bagFlightSeg.setDepartureDateTimeZulu(flightSegment.getDepartureDateTimeZulu());
			bagFlightSeg.setArrivalDateTime(flightSegment.getArrivalDateTime());
			bagFlightSeg.setArrivalDateTimeZulu(flightSegment.getArrivalDateTimeZulu());
			bagFlightSeg.setSegmentCode(flightSegment.getSegmentCode());
			bagFlightSeg.setFlightSegId(flightSegment.getSegmentId());
			bagFlightSeg.setOperatingAirline(AppSysParamsUtil.getDefaultCarrierCode());
			bagFlightSeg.setReturnFlag(false);
			bagFlightSeg.setCabinClassCode(bookingClass.getCabinClassCode());
			bagFlightSeg.setLogicalCabinClassCode(bookingClass.getLogicalCCCode());
			bagFlightSeg.setBookingClass(bookingClass.getBookingCode());
			bagFlightSeg.setOndSequence(OndSequence.OUT_BOUND);
			bagFlightSeg.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegment));
			baggageQuotingSegments.add(bagFlightSeg);

			bcMap.put(bagFlightSeg.getFlightRefNumber(), quotedBookingClass);
			cosMap.put(bagFlightSeg.getFlightRefNumber(), bookingClass.getCabinClassCode());
		}

		Map<String, Integer> baggageOndGrpIdWiseCount = new HashMap<String, Integer>();
		Map<Integer, Map<String, LCCBaggageDTO>> paxWiseBaggageMap = new HashMap<Integer, Map<String, LCCBaggageDTO>>();

		if (!baggageQuotingSegments.isEmpty()) {
			LCCReservationBaggageSummaryTo ondBaggageSummaryTO = new LCCReservationBaggageSummaryTo();
			ondBaggageSummaryTO.setBookingClasses(bcMap);
			ondBaggageSummaryTO.setClassesOfService(cosMap);

			if (!WSAncillaryUtil.isAncillaryAvailable(baggageQuotingSegments, LCCAncillaryType.BAGGAGE, ondBaggageSummaryTO)) {
				throw new WebservicesException(IOTACodeTables.MyidtravelErrorCodes_MYD_SYSTEM_NOT_AVAILABLE,
						"Baggage not available");
			}

			Map<Integer, LCCBaggageDTO> ondDefaultBaggage = getDefaultBaggage(baggageQuotingSegments, baggageOndGrpIdWiseCount,
					ondBaggageSummaryTO);

			Map<String, LCCBaggageDTO> ondDefaultBaggageKeyConverted = new HashMap<String, LCCBaggageDTO>();
			for (Entry<Integer, LCCBaggageDTO> ondDefaultBaggageEntry : ondDefaultBaggage.entrySet()) {
				ondDefaultBaggageKeyConverted.put(ondDefaultBaggageEntry.getKey().toString(), ondDefaultBaggageEntry.getValue());
			}

			for (Integer paxRph : adultChildList) {
				Map<String, LCCBaggageDTO> baggageMap = paxWiseBaggageMap.get(paxRph);
				if (baggageMap == null) {
					paxWiseBaggageMap.put(paxRph, ondDefaultBaggageKeyConverted);
				} else {
					for (Entry<String, LCCBaggageDTO> entry : ondDefaultBaggageKeyConverted.entrySet()) {
						baggageMap.put(entry.getKey(), entry.getValue());
					}
				}

			}
		}

		return getExternalChargesMapForBaggages(paxWiseBaggageMap);
	}

	public static Map<Integer, List<LCCClientExternalChgDTO>> getExternalChargesMapForBaggages(
			Map<Integer, Map<String, LCCBaggageDTO>> baggageDetailsMap) throws ModuleException {

		Map<Integer, List<LCCClientExternalChgDTO>> externalCharges = new HashMap<Integer, List<LCCClientExternalChgDTO>>();

		for (Integer travelerRef : baggageDetailsMap.keySet()) {
			List<LCCClientExternalChgDTO> chargesList = new ArrayList<LCCClientExternalChgDTO>();
			Map<String, LCCBaggageDTO> baggagesMap = baggageDetailsMap.get(travelerRef);
			for (String flightRPH : baggagesMap.keySet()) {
				LCCBaggageDTO baggageDTO = baggagesMap.get(flightRPH);

				LCCClientExternalChgDTO externalChargeTO = new LCCClientExternalChgDTO();

				externalChargeTO.setAmount(baggageDTO.getBaggageCharge());
				externalChargeTO.setCode(baggageDTO.getBaggageName());
				externalChargeTO.setExternalCharges(EXTERNAL_CHARGES.BAGGAGE);
				externalChargeTO.setFlightRefNumber(flightRPH);
				externalChargeTO.setOndBaggageChargeGroupId(baggageDTO.getOndBaggageChargeId());
				externalChargeTO.setOndBaggageGroupId(baggageDTO.getOndBaggageGroupId());

				chargesList.add(externalChargeTO);

			}
			externalCharges.put(travelerRef, chargesList);
		}
		return externalCharges;
	}

	public static Map<String, List<ExternalChgDTO>> getPaxwiseBaggageExternalCharges(Collection<OndFareDTO> ondFares,
			List<String> adultChildList) throws ModuleException, WebservicesException {

		BookingClassBD bookingClassBD = WebServicesModuleUtils.getBookingClassBD();
		BookingClass bookingClass;

		List<FlightSegmentTO> outboundSegments = new ArrayList<FlightSegmentTO>();
		List<FlightSegmentTO> inboundSegments = new ArrayList<FlightSegmentTO>();

		Map<String, String> outboundBCMap = new HashMap<String, String>();
		Map<String, String> inboundBCMap = new HashMap<String, String>();
		Map<String, String> outboundCosMap = new HashMap<String, String>();
		Map<String, String> inboundCosMap = new HashMap<String, String>();

		for (OndFareDTO ondFare : ondFares) {
			if (!ondFare.isInBoundOnd()) {
				for (FlightSegmentDTO flightSegment : ondFare.getSegmentsMap().values()) {

					bookingClass = bookingClassBD.getBookingClass(ondFare.getFareSummaryDTO().getBookingClassCode());
					FlightSegmentTO bagOutFlightSeg = new FlightSegmentTO();
					bagOutFlightSeg.setFlightNumber(flightSegment.getFlightNumber());
					bagOutFlightSeg.setDepartureDateTime(flightSegment.getDepartureDateTime());
					bagOutFlightSeg.setDepartureDateTimeZulu(flightSegment.getDepartureDateTimeZulu());
					bagOutFlightSeg.setArrivalDateTime(flightSegment.getArrivalDateTime());
					bagOutFlightSeg.setArrivalDateTimeZulu(flightSegment.getArrivalDateTimeZulu());
					bagOutFlightSeg.setSegmentCode(flightSegment.getSegmentCode());
					bagOutFlightSeg.setFlightSegId(flightSegment.getSegmentId());
					bagOutFlightSeg.setOperatingAirline(AppSysParamsUtil.getDefaultCarrierCode());
					bagOutFlightSeg.setReturnFlag(false);
					bagOutFlightSeg.setCabinClassCode(bookingClass.getCabinClassCode());
					bagOutFlightSeg.setLogicalCabinClassCode(bookingClass.getLogicalCCCode());
					bagOutFlightSeg.setBookingClass(bookingClass.getBookingCode());
					bagOutFlightSeg.setOndSequence(ondFare.getOndSequence());
					bagOutFlightSeg.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegment));

					outboundSegments.add(bagOutFlightSeg);
					outboundBCMap.put(bagOutFlightSeg.getFlightRefNumber(), ondFare.getFareSummaryDTO().getBookingClassCode());

					outboundCosMap.put(bagOutFlightSeg.getFlightRefNumber(), bookingClass.getCabinClassCode());

				}

			} else {
				for (FlightSegmentDTO flightSegment : ondFare.getSegmentsMap().values()) {
					bookingClass = bookingClassBD.getBookingClass(ondFare.getFareSummaryDTO().getBookingClassCode());
					FlightSegmentTO bagInFlightSeg = new FlightSegmentTO();
					bagInFlightSeg.setFlightNumber(flightSegment.getFlightNumber());
					bagInFlightSeg.setDepartureDateTime(flightSegment.getDepartureDateTime());
					bagInFlightSeg.setDepartureDateTimeZulu(flightSegment.getDepartureDateTimeZulu());
					bagInFlightSeg.setArrivalDateTime(flightSegment.getArrivalDateTime());
					bagInFlightSeg.setArrivalDateTimeZulu(flightSegment.getArrivalDateTimeZulu());
					bagInFlightSeg.setSegmentCode(flightSegment.getSegmentCode());
					bagInFlightSeg.setFlightSegId(flightSegment.getSegmentId());
					bagInFlightSeg.setOperatingAirline(AppSysParamsUtil.getDefaultCarrierCode());
					bagInFlightSeg.setReturnFlag(true);
					bagInFlightSeg.setCabinClassCode(bookingClass.getCabinClassCode());
					bagInFlightSeg.setLogicalCabinClassCode(bookingClass.getLogicalCCCode());
					bagInFlightSeg.setBookingClass(bookingClass.getBookingCode());
					bagInFlightSeg.setOndSequence(ondFare.getOndSequence());
					bagInFlightSeg.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegment));
					inboundSegments.add(bagInFlightSeg);
					inboundBCMap.put(flightSegment.getSegmentCode(), ondFare.getFareSummaryDTO().getBookingClassCode());

					inboundCosMap.put(bagInFlightSeg.getFlightRefNumber(), bookingClass.getCabinClassCode());

				}
			}

		}

		Map<String, Integer> baggageOndGrpIdWiseCount = new HashMap<String, Integer>();
		Map<String, Map<Integer, LCCBaggageDTO>> paxWiseBaggageMap = new HashMap<String, Map<Integer, LCCBaggageDTO>>();

		if (!outboundSegments.isEmpty()) {
			LCCReservationBaggageSummaryTo outboundBaggageSummaryTO = new LCCReservationBaggageSummaryTo();
			outboundBaggageSummaryTO.setBookingClasses(outboundBCMap);
			outboundBaggageSummaryTO.setClassesOfService(outboundCosMap);

			if (!WSAncillaryUtil.isAncillaryAvailable(outboundSegments, LCCAncillaryType.BAGGAGE, outboundBaggageSummaryTO)) {
				throw new WebservicesException(IOTACodeTables.MyidtravelErrorCodes_MYD_SYSTEM_NOT_AVAILABLE,
						"Baggage not available");
			}
			Map<Integer, LCCBaggageDTO> outBoundDefaultBaggage = getDefaultBaggage(outboundSegments, baggageOndGrpIdWiseCount,
					outboundBaggageSummaryTO);

			populatePaxWiseBaggage(outBoundDefaultBaggage, adultChildList, paxWiseBaggageMap);
		}

		if (!inboundSegments.isEmpty()) {
			LCCReservationBaggageSummaryTo inboundBaggageSummaryTO = new LCCReservationBaggageSummaryTo();
			inboundBaggageSummaryTO.setBookingClasses(inboundBCMap);
			inboundBaggageSummaryTO.setClassesOfService(inboundCosMap);
			if (!WSAncillaryUtil.isAncillaryAvailable(inboundSegments, LCCAncillaryType.BAGGAGE, inboundBaggageSummaryTO)) {
				throw new WebservicesException(IOTACodeTables.MyidtravelErrorCodes_MYD_SYSTEM_NOT_AVAILABLE,
						"Baggage not available");
			}
			Map<Integer, LCCBaggageDTO> inBoundDefaultBaggage = getDefaultBaggage(inboundSegments, baggageOndGrpIdWiseCount,
					inboundBaggageSummaryTO);
			populatePaxWiseBaggage(inBoundDefaultBaggage, adultChildList, paxWiseBaggageMap);
		}

		return BaggageDetailsUtil.getExternalChargesMapForBaggages(paxWiseBaggageMap, null, baggageOndGrpIdWiseCount);

	}

	private static Map<Integer, LCCBaggageDTO> getDefaultBaggage(List<FlightSegmentTO> segments,
			Map<String, Integer> baggageOndGrpIdWiseCount, LCCReservationBaggageSummaryTo baggageSummaryTo)
			throws ModuleException {

		Map<Integer, List<FlightBaggageDTO>> baggages = null;
		if (AppSysParamsUtil.isONDBaggaeEnabled()) {
			BaggageRq baggageRq = new BaggageRq();
			baggageRq.setFlightSegmentTOs(segments);
			baggageRq.setBookingClasses(baggageSummaryTo.getBookingClasses());
			baggageRq.setClassesOfService(baggageSummaryTo.getClassesOfService());
			baggageRq.setLogicalCC(baggageSummaryTo.getLogicalCC());
			baggageRq.setCheckCutoverTime(true);
			baggageRq.setAgent(ThreadLocalData.getCurrentUserPrincipal().getAgentCode());
			baggageRq.setRequote(AppSysParamsUtil.isRequoteEnabled());
			baggageRq.setOwnReservation(true);

			baggages = WebServicesModuleUtils.getBaggageBD().getBaggage(baggageRq);

			if (baggages != null && baggages.size() > 0) {
				for (Entry<Integer, List<FlightBaggageDTO>> entry : baggages.entrySet()) {
					String ondGroupId = BeanUtils.getFirstElement(entry.getValue()).getOndGroupId();
					Integer count = baggageOndGrpIdWiseCount.get(ondGroupId);

					if (count == null) {
						baggageOndGrpIdWiseCount.put(ondGroupId, new Integer(1));
					} else {
						baggageOndGrpIdWiseCount.put(ondGroupId, ++count);
					}
				}
			}
		} else {
			Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();
			for (FlightSegmentTO fltSeg : segments) {
				flightSegIdWiseCos.put(fltSeg.getFlightSegId(), null);
			}
			baggages = WebServicesModuleUtils.getBaggageBD().getBaggages(flightSegIdWiseCos, false, false);
		}

		Map<Integer, LCCBaggageDTO> defaultBaggage = new HashMap<Integer, LCCBaggageDTO>();

		for (Integer flightSegid : baggages.keySet()) {

			for (FlightBaggageDTO availBag : baggages.get(flightSegid)) {
				if (CommonsConstants.YES.equals(availBag.getDefaultBaggage())) {
					LCCBaggageDTO lccBaggageDTO = new LCCBaggageDTO();
					lccBaggageDTO.setBaggageCharge(availBag.getAmount());
					lccBaggageDTO.setBaggageName(availBag.getBaggageName());
					lccBaggageDTO.setOndBaggageChargeId(BeanUtils.nullHandler(availBag.getChargeId()));
					lccBaggageDTO.setOndBaggageGroupId(BeanUtils.nullHandler(availBag.getOndGroupId()));

					defaultBaggage.put(flightSegid, lccBaggageDTO);
				}
			}
		}

		return defaultBaggage;
	}

	private static void populatePaxWiseBaggage(Map<Integer, LCCBaggageDTO> defaultBaggage, List<String> paxRphList,
			Map<String, Map<Integer, LCCBaggageDTO>> paxWiseBaggageMap) {

		for (String paxRph : paxRphList) {
			Map<Integer, LCCBaggageDTO> baggageMap = paxWiseBaggageMap.get(paxRph);
			if (baggageMap == null) {
				paxWiseBaggageMap.put(paxRph, defaultBaggage);
			} else {
				for (Entry<Integer, LCCBaggageDTO> entry : defaultBaggage.entrySet()) {
					baggageMap.put(entry.getKey(), entry.getValue());
				}
			}

		}
	}

	public static List<String> getAdultChildRPHs(List<AirTravelerType> airTravelers) {

		List<String> adultChildRphList = new ArrayList<String>();

		for (AirTravelerType singleTraveller : airTravelers) {

			if (!CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT).equals(
					singleTraveller.getPassengerTypeCode())) {
				adultChildRphList.add(singleTraveller.getTravelerRefNumber().getRPH());
			}
		}
		return adultChildRphList;
	}

	public static ServiceTaxCalculator getServiceTaxCalculator(SelectedFlightDTO selectedFlightDTO) throws ModuleException {

		AvailableIBOBFlightSegment selOut = selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND);

		AnciAvailabilityRS anciAvailabilityRS = WebServicesModuleUtils.getAirproxySearchAndQuoteBD()
				.getTaxApplicabilityForAncillaries(AppSysParamsUtil.getDefaultCarrierCode(), selOut.getOndCode(),
						EXTERNAL_CHARGES.JN_ANCI, getTrackInfo());

		ServiceTaxCalculator serviceTaxCalculator = new ServiceTaxCalculator(anciAvailabilityRS);
		serviceTaxCalculator.setFlightSegmentDTOs(selOut.getFlightSegmentDTOs());
		return serviceTaxCalculator;
	}

	public static ExternalChgDTO getJNOtherTax(Reservation reservation, ExternalChgDTO sourceExternalCharge)
			throws ModuleException {

		ExternalChgDTO jnOther = null;
		BigDecimal taxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		ReservationSegmentDTO firstDepartingSegment = ReservationApiUtils.getFirstConfirmedSegment(reservation.getSegmentsView());
		ServiceTaxContainer serviceTaxContainer = WebServicesModuleUtils.getReservationBD().getApplServiceTaxContainer(
				firstDepartingSegment, EXTERNAL_CHARGES.JN_OTHER);

		if (serviceTaxContainer != null) {
			if (serviceTaxContainer != null && serviceTaxContainer.isTaxApplicable()
					&& serviceTaxContainer.getTaxableExternalCharges().contains(sourceExternalCharge.getExternalChargesEnum())) {
				taxAmount = AccelAeroCalculator.multiplyDefaultScale(sourceExternalCharge.getAmount(),
						serviceTaxContainer.getTaxRatio());
				jnOther = WSReservationUtil.getJNTaxOther(taxAmount);

				((ServiceTaxExtChgDTO) jnOther).setFlightRefNumber(serviceTaxContainer.getTaxApplyingFlightRefNumber());
			}
		}

		return jnOther;
	}

	/**
	 * TODO: Remove this and make uniquue
	 * 
	 * @param otaItinerary
	 * @param paxCounts
	 * @param refSegIdRPHMap
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public static AvailableFlightSearchDTO extractPriceQuoteReqParams(AirItineraryType otaItinerary, int[] paxCounts,
			Map<Integer, String> refSegIdRPHMap) throws ModuleException, WebservicesException {

		AvailableFlightSearchDTO availableFlightSearchDTO = new AvailableFlightSearchDTO();

		Collection<String> bookingClassSet = new HashSet<String>();

		Map<String, String> refSegRPHMap = null;

		boolean updateSegReferences = false;
		if (refSegIdRPHMap != null) {
			refSegRPHMap = new HashMap<String, String>();
			updateSegReferences = true;
		}

		// We cannot depend on the number of OriginDestinationOption elements to identify inbound and outbounds
		// as there MyIDTravel is setting flight segments in different OriginDestinationOption elements when departure
		// gap is more than 5 hrs. Hence we are identifying inbound/outbound journeys considering journey path
		// as multicity and open jaw is not supported for the time being.

		boolean isReturnJourney = false;
		List<OriginDestinationOptionType> originDestinationOptions = otaItinerary.getOriginDestinationOptions()
				.getOriginDestinationOption();

		List<FlightSegmentDTO> allFlightSegs = new ArrayList<FlightSegmentDTO>();

		for (OriginDestinationOptionType originDestinationOption : originDestinationOptions) {
			int bookingClassCount = 0;
			for (BookFlightSegmentType bookFlightSegment : originDestinationOption.getFlightSegment()) {
				FlightSegmentDTO aaFlightSegmentDTO = new FlightSegmentDTO();
				aaFlightSegmentDTO.setFromAirport(bookFlightSegment.getDepartureAirport().getLocationCode());
				aaFlightSegmentDTO.setToAirport(bookFlightSegment.getArrivalAirport().getLocationCode());
				aaFlightSegmentDTO.setFlightNumber(bookFlightSegment.getOperatingAirline().getCode()
						+ bookFlightSegment.getOperatingAirline().getFlightNumber());
				aaFlightSegmentDTO.setDepartureDateTime(bookFlightSegment.getDepartureDateTime().toGregorianCalendar().getTime());
				aaFlightSegmentDTO.setArrivalDateTime(bookFlightSegment.getArrivalDateTime().toGregorianCalendar().getTime());
				if (updateSegReferences) {
					refSegRPHMap.put(MyIDTrvlCommonUtil.getTmpSegRefNo(aaFlightSegmentDTO), bookFlightSegment.getRPH());
				}

				if (bookFlightSegment.getResBookDesigCode() != null) {
					bookingClassSet.add(bookFlightSegment.getResBookDesigCode());
					bookingClassCount++;
				}
				allFlightSegs.add(aaFlightSegmentDTO);
			}

			if (bookingClassCount != 0 && bookingClassCount != originDestinationOption.getFlightSegment().size()) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
						"Booking class not specified in all segments");
			}

		}

		List<FlightSegmentDTO> filledOutboundSegsList = null;
		List<FlightSegmentDTO> filledInboundSegsList = null;
		List<FlightSegmentDTO> filledAllSegsList = null;

		if (!allFlightSegs.isEmpty()) {
			Collection<FlightSegmentDTO> filledAllFlightSegmentDTOs = WebServicesModuleUtils.getFlightBD()
					.getFilledFlightSegmentDTOs(allFlightSegs);
			if (filledAllFlightSegmentDTOs.size() != allFlightSegs.size()) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Invalid  Segment(s)]");
			}
			filledAllSegsList = new ArrayList<FlightSegmentDTO>(filledAllFlightSegmentDTOs);
			Collections.sort(filledAllSegsList);
		}

		int segIndex = 0;
		int returnStartingIndex = -1;

		for (FlightSegmentDTO flightSeg : filledAllSegsList) {
			if (segIndex > 0 && flightSeg.getFromAirport().equals(filledAllSegsList.get(segIndex - 1).getToAirport())
					&& flightSeg.getDepartureDateTime().after(filledAllSegsList.get(segIndex - 1).getArrivalDateTime())) {

				if (flightSeg.getToAirport().equals(filledAllSegsList.get(segIndex - 1).getFromAirport())
						&& filledAllSegsList.get(0).getFromAirport()
								.equals(filledAllSegsList.get(filledAllSegsList.size() - 1).getToAirport())) {

					returnStartingIndex = segIndex;
					isReturnJourney = true;
					break;
				}
			} else if (segIndex > 0) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Invalid Journey]");
			}

			segIndex++;

		}

		if (returnStartingIndex != -1) {
			filledOutboundSegsList = filledAllSegsList.subList(0, returnStartingIndex);
			filledInboundSegsList = filledAllSegsList.subList(returnStartingIndex, filledAllSegsList.size());
		} else {
			filledOutboundSegsList = filledAllSegsList;
		}

		// we only support booking class per one journey
		if (!bookingClassSet.isEmpty() && bookingClassSet.size() > 1) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Too many booking classes specified");
		}
		String bookingClass = BeanUtils.getFirstElement(bookingClassSet);
		String bookingType = BookingClass.BookingClassType.NORMAL;
		String cabinClass = WebServicesModuleUtils.getBookingClassBD().getCabinClassForBookingClass(bookingClass);

		if (bookingClass != null) {
			Collection<String> standByBCList = WebServicesModuleUtils.getBookingClassBD().getStandbyBookingClasses();
			if (standByBCList.contains(bookingClass)) {
				bookingType = BookingClass.BookingClassType.STANDBY;
			}
		}

		OriginDestinationInfoDTO outboundOndInfo = new OriginDestinationInfoDTO();
		outboundOndInfo.setOrigin(filledOutboundSegsList.get(0).getFromAirport());
		outboundOndInfo.setDestination(filledOutboundSegsList.get(filledOutboundSegsList.size() - 1).getToAirport());
		outboundOndInfo.setDepartureDateTimeStart(CalendarUtil
				.getStartTimeOfDate(filledOutboundSegsList.get(0).getDepatureDate()));
		outboundOndInfo.setDepartureDateTimeEnd(CalendarUtil.getEndTimeOfDate(filledOutboundSegsList.get(0).getDepatureDate()));

		List<Integer> outboundFlightSegmentIds = new ArrayList<Integer>();
		for (FlightSegmentDTO flightSegmentDTO : filledOutboundSegsList) {
			if (flightSegmentDTO.getSegmentId() != null) {
				outboundFlightSegmentIds.add(flightSegmentDTO.getSegmentId());
				if (updateSegReferences) {
					refSegIdRPHMap.put(flightSegmentDTO.getSegmentId(),
							refSegRPHMap.get(MyIDTrvlCommonUtil.getTmpSegRefNo(flightSegmentDTO)));
				}

			} else {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Invalid Inbound Segment. "
						+ flightSegmentDTO.toString() + "]");
			}
		}
		outboundOndInfo.setFlightSegmentIds(outboundFlightSegmentIds);
		outboundOndInfo.setPreferredBookingClass(bookingClass);
		outboundOndInfo.setPreferredBookingType(bookingType);
		outboundOndInfo.setPreferredClassOfService(cabinClass);
		availableFlightSearchDTO.addOriginDestination(outboundOndInfo);

		if (isReturnJourney) {
			OriginDestinationInfoDTO inboundOndInfo = new OriginDestinationInfoDTO();
			inboundOndInfo.setOrigin(outboundOndInfo.getDestination());
			inboundOndInfo.setDestination(outboundOndInfo.getOrigin());
			inboundOndInfo.setDepartureDateTimeStart(CalendarUtil.getStartTimeOfDate(filledInboundSegsList.get(0)
					.getDepartureDateTime()));
			inboundOndInfo.setDepartureDateTimeEnd(CalendarUtil.getEndTimeOfDate(filledInboundSegsList.get(0)
					.getDepartureDateTime()));

			List<Integer> inboundFlightSegmentIds = new ArrayList<Integer>();
			for (FlightSegmentDTO flightSegmentDTO : filledInboundSegsList) {
				if (flightSegmentDTO.getSegmentId() != null) {
					if (updateSegReferences) {
						inboundFlightSegmentIds.add(flightSegmentDTO.getSegmentId());
						refSegIdRPHMap.put(flightSegmentDTO.getSegmentId(),
								refSegRPHMap.get(MyIDTrvlCommonUtil.getTmpSegRefNo(flightSegmentDTO)));
					}

				} else {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Invalid Inbound Segment. "
							+ flightSegmentDTO.toString() + "]");
				}
			}
			inboundOndInfo.setFlightSegmentIds(inboundFlightSegmentIds);
			inboundOndInfo.setPreferredBookingClass(bookingClass);
			inboundOndInfo.setPreferredBookingType(bookingType);
			inboundOndInfo.setPreferredClassOfService(cabinClass);
			availableFlightSearchDTO.addOriginDestination(inboundOndInfo);
		}

		availableFlightSearchDTO.setAdultCount(paxCounts[0]);
		availableFlightSearchDTO.setChildCount(paxCounts[1]);
		availableFlightSearchDTO.setInfantCount(paxCounts[2]);

		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
		availableFlightSearchDTO.setPosAirport(principal.getAgentStation());
		availableFlightSearchDTO.setAgentCode(principal.getAgentCode());

		availableFlightSearchDTO.setChannelCode(SalesChannelsUtil
				.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));
		availableFlightSearchDTO.setAvailabilityRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED);

		return availableFlightSearchDTO;
	}

	public static FlightPriceRQ extractPriceQuoteReqParamsForRequote(List<FlightSegmentDTO> flightSegments, int[] paxCounts,
			Map<Integer, LCCClientReservationSegment> existingConfirmedSegmentsMap, String bookingClass) throws ModuleException,
			WebservicesException {

		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();

		// We cannot depend on the number of OriginDestinationOption elements to identify inbound and outbounds
		// as there MyIDTravel is setting flight segments in different OriginDestinationOption elements when departure
		// gap is more than 5 hrs. Hence we are identifying inbound/outbound journeys considering journey path
		// as multicity and open jaw is not supported for the time being.

		boolean isReturnJourney = false;
		
		Collections.sort(flightSegments);
		List<FlightSegmentDTO> filledAllSegsList = flightSegments;
		List<FlightSegmentDTO> filledOutboundSegsList = null;
		List<FlightSegmentDTO> filledInboundSegsList = null;

		int segIndex = 0;
		int returnStartingIndex = -1;

		for (FlightSegmentDTO flightSeg : filledAllSegsList) {
			if (segIndex > 0 && flightSeg.getFromAirport().equals(filledAllSegsList.get(segIndex - 1).getToAirport())
					&& flightSeg.getDepartureDateTime().after(filledAllSegsList.get(segIndex - 1).getArrivalDateTime())) {

				if (flightSeg.getToAirport().equals(filledAllSegsList.get(segIndex - 1).getFromAirport())
						&& filledAllSegsList.get(0).getFromAirport()
								.equals(filledAllSegsList.get(filledAllSegsList.size() - 1).getToAirport())) {

					returnStartingIndex = segIndex;
					isReturnJourney = true;
					break;
				}
			} else if (segIndex > 0) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Invalid Journey]");
			}

			segIndex++;

		}

		if (returnStartingIndex != -1) {
			filledOutboundSegsList = filledAllSegsList.subList(0, returnStartingIndex);
			filledInboundSegsList = filledAllSegsList.subList(returnStartingIndex, filledAllSegsList.size());
		} else {
			filledOutboundSegsList = filledAllSegsList;
		}

		String bookingType = BookingClass.BookingClassType.NORMAL;
		String cabinClass = WebServicesModuleUtils.getBookingClassBD().getCabinClassForBookingClass(bookingClass);

		if (bookingClass != null) {
			Collection<String> standByBCList = WebServicesModuleUtils.getBookingClassBD().getStandbyBookingClasses();
			if (standByBCList.contains(bookingClass)) {
				bookingType = BookingClass.BookingClassType.STANDBY;
			}
		}

		OriginDestinationInformationTO depatureOnD = flightPriceRQ.addNewOriginDestinationInformation();
		depatureOnD.setOrigin(filledOutboundSegsList.get(0).getFromAirport());
		depatureOnD.setDestination(filledOutboundSegsList.get(filledOutboundSegsList.size() - 1).getToAirport());
		depatureOnD.setPreferredDate(filledOutboundSegsList.get(0).getDepatureDate());
		depatureOnD.setDepartureDateTimeStart(CalendarUtil.getStartTimeOfDate(filledOutboundSegsList.get(0).getDepatureDate()));
		depatureOnD.setDepartureDateTimeEnd(CalendarUtil.getEndTimeOfDate(filledOutboundSegsList.get(0).getDepatureDate()));
		depatureOnD.getOrignDestinationOptions().add(getOriginDestinationOptionTO(filledOutboundSegsList, OndSequence.OUT_BOUND));
		depatureOnD.setPreferredBookingClass(bookingClass);
		depatureOnD.setPreferredBookingType(bookingType);
		depatureOnD.setPreferredClassOfService(cabinClass);

		if (existingConfirmedSegmentsMap != null) {

			List<FareTO> existingFareTos = null;

			for (FlightSegmentDTO flightSegmentDTO : filledOutboundSegsList) {
				if (existingConfirmedSegmentsMap.containsKey(flightSegmentDTO.getSegmentId())) {
					LCCClientReservationSegment reservationSegment = existingConfirmedSegmentsMap.get(flightSegmentDTO
							.getSegmentId());
					depatureOnD.getExistingFlightSegIds().add(flightSegmentDTO.getSegmentId());
					depatureOnD.getExistingPnrSegRPHs().add(reservationSegment.getBookingFlightSegmentRefNumber());
					depatureOnD.setUnTouchedOnd(true);

					if (existingFareTos == null) {
						existingFareTos = new ArrayList<FareTO>();
					}
					existingFareTos.add(reservationSegment.getFareTO());
				}
			}

			if (depatureOnD.isUnTouchedOnd()) {
				depatureOnD.setUnTouchedResSegList(depatureOnD.getExistingPnrSegRPHs());
			}

			depatureOnD.setOldPerPaxFareTOList(existingFareTos);
		}

		if (isReturnJourney) {
			OriginDestinationInformationTO returnOnD = flightPriceRQ.addNewOriginDestinationInformation();

			returnOnD.setOrigin(depatureOnD.getDestination());
			returnOnD.setDestination(depatureOnD.getOrigin());
			returnOnD.setPreferredDate(filledInboundSegsList.get(0).getDepartureDateTime());
			returnOnD.setDepartureDateTimeStart(CalendarUtil.getStartTimeOfDate(filledInboundSegsList.get(0)
					.getDepartureDateTime()));
			returnOnD.setDepartureDateTimeEnd(CalendarUtil.getEndTimeOfDate(filledInboundSegsList.get(0).getDepartureDateTime()));
			returnOnD.getOrignDestinationOptions().add(getOriginDestinationOptionTO(filledInboundSegsList, OndSequence.IN_BOUND));
			returnOnD.setPreferredBookingClass(bookingClass);
			returnOnD.setPreferredBookingType(bookingType);
			returnOnD.setPreferredClassOfService(cabinClass);

			if (existingConfirmedSegmentsMap != null) {

				List<FareTO> existingFareTos = null;

				for (FlightSegmentDTO flightSegmentDTO : filledInboundSegsList) {
					if (existingConfirmedSegmentsMap.containsKey(flightSegmentDTO.getSegmentId())) {

						LCCClientReservationSegment reservationSegment = existingConfirmedSegmentsMap.get(flightSegmentDTO
								.getSegmentId());
						returnOnD.getExistingFlightSegIds().add(flightSegmentDTO.getSegmentId());
						returnOnD.getExistingPnrSegRPHs().add(reservationSegment.getBookingFlightSegmentRefNumber());
						returnOnD.setUnTouchedOnd(true);

						if (existingFareTos == null) {
							existingFareTos = new ArrayList<FareTO>();
						}
						existingFareTos.add(reservationSegment.getFareTO());
					}
				}

				if (returnOnD.isUnTouchedOnd()) {
					returnOnD.setUnTouchedResSegList(returnOnD.getExistingPnrSegRPHs());
				}

				returnOnD.setOldPerPaxFareTOList(existingFareTos);
			}
		}

		AvailPreferencesTO availPref = flightPriceRQ.getAvailPreferences();

		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
		availPref.setTravelAgentCode(principal.getAgentCode());
		availPref.setRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED);
		availPref.setQuoteOndFlexi(OndSequence.OUT_BOUND, false);
		availPref.setQuoteOndFlexi(OndSequence.IN_BOUND, false);
		availPref.setQuoteFares(true);
		availPref.setSearchSystem(SYSTEM.AA);
		availPref.setAppIndicator(ApplicationEngine.XBE);

		TravelPreferencesTO travelerPref = flightPriceRQ.getTravelPreferences();
		travelerPref.setBookingType(bookingType);
		travelerPref.setBookingClassCode(bookingClass);

		TravelerInfoSummaryTO traverlerInfo = flightPriceRQ.getTravelerInfoSummary();

		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
		adultsQuantity.setQuantity(paxCounts[0]);

		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);
		childQuantity.setQuantity(paxCounts[1]);

		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);
		infantQuantity.setQuantity(paxCounts[2]);

		return flightPriceRQ;
	}

	public static Pair<String, List<FlightSegmentDTO>> getFlightSegmentDTOs(
			List<OriginDestinationOptionType> originDestinationOptions) throws WebservicesException {

		Pair<String, List<FlightSegmentDTO>> result = null;
		List<FlightSegmentDTO> filledAllSegsList = null;
		List<FlightSegmentDTO> allFlightSegs = new ArrayList<FlightSegmentDTO>();

		String bookingClass = null;
		for (OriginDestinationOptionType originDestinationOption : originDestinationOptions) {
			for (BookFlightSegmentType bookFlightSegment : originDestinationOption.getFlightSegment()) {
				FlightSegmentDTO aaFlightSegmentDTO = new FlightSegmentDTO();
				aaFlightSegmentDTO.setFromAirport(bookFlightSegment.getDepartureAirport().getLocationCode());
				aaFlightSegmentDTO.setToAirport(bookFlightSegment.getArrivalAirport().getLocationCode());
				aaFlightSegmentDTO.setFlightNumber(bookFlightSegment.getOperatingAirline().getCode()
						+ bookFlightSegment.getOperatingAirline().getFlightNumber());
				aaFlightSegmentDTO.setDepartureDateTime(bookFlightSegment.getDepartureDateTime().toGregorianCalendar().getTime());
				aaFlightSegmentDTO.setArrivalDateTime(bookFlightSegment.getArrivalDateTime().toGregorianCalendar().getTime());

				if (bookFlightSegment.getResBookDesigCode() == null) {
					// throw error
				} else {
					if (bookingClass == null) {
						bookingClass = bookFlightSegment.getResBookDesigCode();
					} else {
						if (bookFlightSegment.getResBookDesigCode().equals(bookingClass)) {
							continue;
						} else {
							// throw error
						}
					}
				}

				allFlightSegs.add(aaFlightSegmentDTO);
			}
		}

		if (!allFlightSegs.isEmpty()) {

			try {
				Collection<FlightSegmentDTO> filledAllFlightSegmentDTOs = WebServicesModuleUtils.getFlightBD()
						.getFilledFlightSegmentDTOs(allFlightSegs);
				if (filledAllFlightSegmentDTOs.size() != allFlightSegs.size()) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Invalid  Segment(s)]");
				}
				filledAllSegsList = new ArrayList<FlightSegmentDTO>(filledAllFlightSegmentDTOs);
				Collections.sort(filledAllSegsList);

				result = Pair.of(bookingClass, filledAllSegsList);
			} catch (Exception e) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Invalid  Segment(s)]");
			}

		}

		return result;
	}

	private static OriginDestinationOptionTO getOriginDestinationOptionTO(List<FlightSegmentDTO> flightSegmentDTOs,
			int ondSequence) throws WebservicesException {
		OriginDestinationOptionTO originDestinationOptionTO = null;
		if (flightSegmentDTOs != null) {
			originDestinationOptionTO = new OriginDestinationOptionTO();

			FlightSegmentTO flightSegTO = null;

			for (FlightSegmentDTO flightSegmentDTO : flightSegmentDTOs) {
				if (flightSegmentDTO.getSegmentId() != null) {
					flightSegTO = new FlightSegmentTO();
					flightSegTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentDTO));
					flightSegTO.setFlightSegId(flightSegmentDTO.getSegmentId());
					flightSegTO.setOndSequence(ondSequence);
					originDestinationOptionTO.getFlightSegmentList().add(flightSegTO);
				} else {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Invalid Segment. "
							+ flightSegmentDTO.toString() + "]");
				}
			}
		}
		return originDestinationOptionTO;
	}

}
