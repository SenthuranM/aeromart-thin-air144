package com.isa.thinair.webservices.core.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRQ;
import com.isa.thinair.gdsservices.api.dto.typea.TypeBRS;
import com.isaaviation.thinair.webservices.api.typebreservation.AATypeBReservationRQ;
import com.isaaviation.thinair.webservices.api.typebreservation.AATypeBReservationRS;

/**
 * @author Manoj Dhanushka
 */
public class TypeBResMsgProcessUtill {
	
	private static final Log log = LogFactory.getLog(TypeBResMsgProcessUtill.class);
	
	public static AATypeBReservationRS processTypeBResMessage(AATypeBReservationRQ typeBReservationRQ) {
		AATypeBReservationRS aaTypeBReservationRS = new AATypeBReservationRS();
		try {
			log.error("------- TypeB message processing started -------");
			TypeBRQ typeBRQ = new TypeBRQ();
			typeBRQ.setTypeBMessage(typeBReservationRQ.getMessage());
			typeBRQ.setSaveInOutMsg(true);
			TypeBRS typeBRS = WebServicesModuleUtils.getTypeBServiceBD().processTypeBMessage(typeBRQ);

			log.error("------- TypeB message sending terminated -------");
			if (typeBRS.getResponseStatus().equals(TypeBRS.ResponseStatus.SUCCESSFUL)) {
				aaTypeBReservationRS.setSuccess(true);
			} else {
				aaTypeBReservationRS.setSuccess(false);
			}
			aaTypeBReservationRS.setResMessage(typeBRS.getResponseMessage());

		} catch (ModuleException ex) {
			log.error("------- TypeB message processing failed -------", ex);
		}
		return aaTypeBReservationRS;
	}
}
