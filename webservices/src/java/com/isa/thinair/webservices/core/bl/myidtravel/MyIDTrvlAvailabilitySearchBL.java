package com.isa.thinair.webservices.core.bl.myidtravel;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AirSearchPrefsType.CabinPref;
import org.opentravel.ota._2003._05.EquipmentType;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.FlightSegmentBaseType;
import org.opentravel.ota._2003._05.FlightSegmentType.MarketingAirline;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirAvailRQ;
import org.opentravel.ota._2003._05.OTAAirAvailRQ.OriginDestinationInformation;
import org.opentravel.ota._2003._05.OTAAirAvailRS;
import org.opentravel.ota._2003._05.OperatingAirlineType;
import org.opentravel.ota._2003._05.OriginDestinationInformationType;
import org.opentravel.ota._2003._05.SuccessType;
import org.opentravel.ota._2003._05.TicketType;
import org.opentravel.ota._2003._05.TimeInstantType;
import org.opentravel.ota._2003._05.WarningsType;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.ExceptionUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterAvailabilityRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterAvailabilityResponse;

/**
 * @author Manoj Dhanushka
 * 
 */
public class MyIDTrvlAvailabilitySearchBL {

	private final static Log log = LogFactory.getLog(MyIDTrvlAvailabilitySearchBL.class);

	public static MyIDTravelResAdapterAvailabilityResponse execute(
			MyIDTravelResAdapterAvailabilityRequest myIDTravelResAdapterAvailabilityRequest) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN getAvailability(MyIDTravelResAdapterAvailabilityRequest)");
		}

		MyIDTravelResAdapterAvailabilityResponse myIDAvailRes = null;

		try {
			myIDAvailRes = getAvailability(myIDTravelResAdapterAvailabilityRequest);
		} catch (Exception ex) {
			log.error("getAvailability(MyIDTravelResAdapterAvailabilityRequest) failed.", ex);

			if (myIDAvailRes == null) {
				myIDAvailRes = new MyIDTravelResAdapterAvailabilityResponse();
				OTAAirAvailRS otaAirAvailRS = new OTAAirAvailRS();
				otaAirAvailRS.setEchoToken(myIDTravelResAdapterAvailabilityRequest.getOTAAirAvailRQ().getEchoToken());
				myIDAvailRes.setOTAAirAvailRS(otaAirAvailRS);
			}
			if (myIDAvailRes.getOTAAirAvailRS().getErrors() == null)
				myIDAvailRes.getOTAAirAvailRS().setErrors(new ErrorsType());
			if (myIDAvailRes.getOTAAirAvailRS().getWarnings() == null)
				myIDAvailRes.getOTAAirAvailRS().setWarnings(new WarningsType());
			ExceptionUtil.addOTAErrrorsAndWarnings(myIDAvailRes.getOTAAirAvailRS().getErrors().getError(), myIDAvailRes
					.getOTAAirAvailRS().getWarnings().getWarning(), ex);
		}

		if (log.isDebugEnabled()) {
			log.debug("END getAvailability(MyIDTravelResAdapterAvailabilityRequest)");
		}
		return myIDAvailRes;
	}

	public static MyIDTravelResAdapterAvailabilityResponse getAvailability(
			MyIDTravelResAdapterAvailabilityRequest myIDAvailReq) throws ModuleException, WebservicesException {

		MyIDTravelResAdapterAvailabilityResponse myIDAvailRes = new MyIDTravelResAdapterAvailabilityResponse();
		OTAAirAvailRS otaAirAvailRS = new OTAAirAvailRS();
		myIDAvailRes.setOTAAirAvailRS(otaAirAvailRS);
		myIDAvailRes.setEmployeeData(myIDAvailReq.getEmployeeData());
		OTAAirAvailRQ oTAAirAvailRQ = myIDAvailReq.getOTAAirAvailRQ();
		otaAirAvailRS.setEchoToken(oTAAirAvailRQ.getEchoToken());


		AvailableFlightSearchDTO availableFlightSearchDTO = null;

		try {
			availableFlightSearchDTO = extractAvailabilitySearchReqParams(oTAAirAvailRQ, otaAirAvailRS);
			availableFlightSearchDTO.setQuoteFares(false);
			if (availableFlightSearchDTO != null) {
				AvailableFlightDTO availableFlightDTO = WebServicesModuleUtils.getReservationQueryBD()
						.getAvailableFlightsWithAllFares(availableFlightSearchDTO, null);

				prepareAvailabilitySearchResponse(oTAAirAvailRQ, otaAirAvailRS, availableFlightDTO,
						availableFlightSearchDTO);
			}
		} catch (WebservicesException ex) {
			if (ex.getOtaErrorCodes().get(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE) != null) {
				setEmptyAvailability(otaAirAvailRS);
			} else {
				throw ex;
			}

		}

		return myIDAvailRes;
	}

	/**
	 * OTA params expected version, sequenceNumber, echoToken, Origin
	 * Destination Departure datetime OS - airportCode, agentCode
	 * isDirectFligtsOnly
	 * 
	 * @param otaAirAvailRQ
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private static AvailableFlightSearchDTO extractAvailabilitySearchReqParams(OTAAirAvailRQ otaAirAvailRQ,
			OTAAirAvailRS otaAirAvailRS) throws ModuleException, WebservicesException {

		AvailableFlightSearchDTO availableFlightSearchDTO = new AvailableFlightSearchDTO();

		// Multi-city search not supported
		int ondCount = otaAirAvailRQ.getOriginDestinationInformation().size();

		if (ondCount > 2) {
			log.error("Invalid avialability request:Multiple Onds - Ond Count"
					+ otaAirAvailRQ.getOriginDestinationInformation().size());
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "No availability)");
		}

		for (OriginDestinationInformationType originDestinationInformation : otaAirAvailRQ
				.getOriginDestinationInformation()) {
			OriginDestinationInfoDTO ondInfoDTO = new OriginDestinationInfoDTO();

			ondInfoDTO.setOrigin(originDestinationInformation.getOriginLocation().getLocationCode());
			ondInfoDTO.setDestination(originDestinationInformation.getDestinationLocation().getLocationCode());
			ondInfoDTO.setDepartureDateTimeStart(CalendarUtil.getStartTimeOfDate(CommonUtil.parseDate(
					originDestinationInformation.getDepartureDateTime().getValue()).getTime()));
			ondInfoDTO.setDepartureDateTimeEnd(CalendarUtil.getEndTimeOfDate(CommonUtil.parseDate(
					originDestinationInformation.getDepartureDateTime().getValue()).getTime()));

			ondInfoDTO.setPreferredDateTimeStart(ondInfoDTO.getDepartureDateTimeStart());
			ondInfoDTO.setPreferredDateTimeEnd(ondInfoDTO.getDepartureDateTimeEnd());
			boolean areAirportsValid = WebServicesModuleUtils.getWebServiceDAO().isFromAndToAirportsValid(
					ondInfoDTO.getOrigin(), ondInfoDTO.getDestination());

			if (!areAirportsValid) {
				log.error("Invalid avialability request:Non Operating Airports [From:" + ondInfoDTO.getOrigin()
						+ ", To:" + ondInfoDTO.getDestination() + "]");
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "No availability)");
			}

			String prefferedCabinClass = null;

			if (originDestinationInformation instanceof OriginDestinationInformation) {
				OriginDestinationInformation ondInfo = (OriginDestinationInformation) originDestinationInformation;
				if (ondInfo.getTravelPreferences() != null && ondInfo.getTravelPreferences().getCabinPref() != null) {
					CabinPref cabinPref = BeanUtils.getFirstElement(ondInfo.getTravelPreferences().getCabinPref());
					if (cabinPref.getCabin() != null) {
						prefferedCabinClass = cabinPref.getCabin().value();
					}
				}
			}

			ondInfoDTO.setPreferredClassOfService(prefferedCabinClass == null ? "Y" : prefferedCabinClass);
			availableFlightSearchDTO.addOriginDestination(ondInfoDTO);

		}

		// Only used for flight availability, no importance of pax count
		availableFlightSearchDTO.setAdultCount(1);
		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
		availableFlightSearchDTO.setPosAirport(principal.getAgentStation());
		availableFlightSearchDTO.setAgentCode(principal.getAgentCode());
		availableFlightSearchDTO.setChannelCode(SalesChannelsUtil
				.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));
		availableFlightSearchDTO.setAvailabilityRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED);
		return availableFlightSearchDTO;
	}

	/**
	 * 
	 * @param otaAirAvailRQ
	 * @param otaAirAvailRS
	 * @param availableFlights
	 * @param availableFlightSearchDTO
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	private static void prepareAvailabilitySearchResponse(OTAAirAvailRQ otaAirAvailRQ, OTAAirAvailRS otaAirAvailRS,
			AvailableFlightDTO availableFlights, AvailableFlightSearchDTO availableFlightSearchDTO)
			throws WebservicesException, ModuleException {
		boolean flightsFoundInbound = false;
		boolean flightsFoundOutbound = false;
		otaAirAvailRS.setVersion(WebservicesConstants.OTAConstants.VERSION_AirAvail);
		otaAirAvailRS.setEchoToken(otaAirAvailRQ.getEchoToken());
		otaAirAvailRS.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);

		Collection<AvailableIBOBFlightSegment> outboundFlights = availableFlights
				.getAvailableOndFlights(OndSequence.OUT_BOUND);
		Collection<AvailableIBOBFlightSegment> inboundFlights = null;
		if (availableFlightSearchDTO.getOriginDestinationInfoDTOs().size() == 2
				&& availableFlights.getAvailableOndFlights(OndSequence.IN_BOUND).size() > 0) {
			inboundFlights = availableFlights.getAvailableOndFlights(OndSequence.IN_BOUND);
		}

		List<Integer> allFlightSegsIds = new ArrayList<Integer>();

		if (outboundFlights != null && outboundFlights.size() > 0) {

			flightsFoundOutbound = true;
			for (AvailableIBOBFlightSegment outBoundFlight : outboundFlights) {

				List<FlightSegmentDTO> outBoundFlightSegs = outBoundFlight.getFlightSegmentDTOs();

				if (outBoundFlightSegs != null && outBoundFlightSegs.size() > 0) {
					for (FlightSegmentDTO outBoundFlightSeg : outBoundFlightSegs) {
						if (!allFlightSegsIds.contains(outBoundFlightSeg.getSegmentId())) {
							allFlightSegsIds.add(outBoundFlightSeg.getSegmentId());
						}
					}
				}

			}
		}

		if (inboundFlights != null && inboundFlights.size() > 0) {
			flightsFoundInbound = true;
			for (AvailableIBOBFlightSegment inboundFlight : inboundFlights) {

				List<FlightSegmentDTO> inboundFlightSegs = inboundFlight.getFlightSegmentDTOs();

				if (inboundFlightSegs != null && inboundFlightSegs.size() > 0) {
					for (FlightSegmentDTO inboundFlightSeg : inboundFlightSegs) {
						if (!allFlightSegsIds.contains(inboundFlightSeg.getSegmentId())) {
							allFlightSegsIds.add(inboundFlightSeg.getSegmentId());
						}
					}
				}

			}
		}

		Map<Integer, List<Pair<String, Integer>>> seatAvailabilityMap = WebServicesModuleUtils
				.getFlightInventoryResBD().getBcAvailabilityForMyIDTravel(allFlightSegsIds,
						availableFlightSearchDTO.getAgentCode());

		// Setting outbound flights

		if (outboundFlights != null && outboundFlights.size() > 0) {
			flightsFoundOutbound = setFlightInfo(outboundFlights, otaAirAvailRS, seatAvailabilityMap);
		}
		// Setting inbound flights
		if (inboundFlights != null) {
			flightsFoundInbound = setFlightInfo(inboundFlights, otaAirAvailRS, seatAvailabilityMap);
		}

		if (flightsFoundOutbound || flightsFoundInbound) {
			otaAirAvailRS.setSuccess(new SuccessType());
		} else {
			setEmptyAvailability(otaAirAvailRS);
		}

	}

	private static boolean setFlightInfo(Collection<AvailableIBOBFlightSegment> outboundFlights,
			OTAAirAvailRS otaAirAvailRS, Map<Integer, List<Pair<String, Integer>>> seatAvailMap) throws ModuleException {

		boolean flightOptionsAvailable = false;

		boolean originDestinationInfoSet = false;

		OTAAirAvailRS.OriginDestinationInformation originDestinationInformation = null;

		for (AvailableIBOBFlightSegment availableFlightSegment : outboundFlights) {

			Collection<FlightSegmentDTO> flightSegmentDTOs = availableFlightSegment.getFlightSegmentDTOs();

			// Check whether all segments have at least one bc allocation
			boolean bcAvailableInAll = true;
			for (FlightSegmentDTO segment : flightSegmentDTOs) {
				bcAvailableInAll = (seatAvailMap.get(segment.getSegmentId()) != null ? true : false)
						&& bcAvailableInAll;
			}

			if (bcAvailableInAll) {
				flightOptionsAvailable = true;

				if (!originDestinationInfoSet) {
					originDestinationInformation = new OTAAirAvailRS.OriginDestinationInformation();
					otaAirAvailRS.getOriginDestinationInformation().add(originDestinationInformation);
					OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions originDestinationOptions = new OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions();
					originDestinationInformation.setOriginDestinationOptions(originDestinationOptions);
				}

				OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption originDestinationOption = new OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption();
				originDestinationInformation.getOriginDestinationOptions().getOriginDestinationOption()
						.add(originDestinationOption);

				Iterator<FlightSegmentDTO> flightSegmentDTOsIt = flightSegmentDTOs.iterator();

				OriginDestinationInformationType.OriginLocation origin = null;
				OriginDestinationInformationType.DestinationLocation destination = null;

				for (int i = 0; flightSegmentDTOsIt.hasNext(); ++i) {
					FlightSegmentDTO flightSegmentDTO = flightSegmentDTOsIt.next();
					String[] segmentCodeList = flightSegmentDTO.getSegmentCode().split("/");
					OriginDestinationInformationType.OriginLocation originLocation = new OriginDestinationInformationType.OriginLocation();
					originLocation.setLocationCode(segmentCodeList[0]);
					originLocation.setCodeContext(WebservicesConstants.OTAConstants.CODE_CONTEXT_IATA);

					OriginDestinationInformationType.DestinationLocation destinationLocation = new OriginDestinationInformationType.DestinationLocation();
					destinationLocation.setLocationCode(segmentCodeList[segmentCodeList.length - 1]);
					destinationLocation.setCodeContext(WebservicesConstants.OTAConstants.CODE_CONTEXT_IATA);

					TimeInstantType departureDateTime = new TimeInstantType();
					departureDateTime.setValue(CommonUtil.getFormattedDate(flightSegmentDTO.getDepartureDateTime()));

					TimeInstantType arrivalDateTime = new TimeInstantType();
					arrivalDateTime.setValue(CommonUtil.getFormattedDate(flightSegmentDTO.getArrivalDateTime()));

					if (i == 0) {
						origin = originLocation;
					}

					if (i == flightSegmentDTOs.size() - 1) {
						destination = destinationLocation;
					}

					OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment otaFlightSegment = new OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment();
					originDestinationOption.getFlightSegment().add(otaFlightSegment);

					otaFlightSegment.setRPH(Integer.toString(i));
					if (segmentCodeList.length > 2) {
						otaFlightSegment.setStopQuantity(BigInteger.valueOf(segmentCodeList.length - 2));
					}

					GregorianCalendar departureDateTimeCal = new GregorianCalendar();
					departureDateTimeCal.setTime(flightSegmentDTO.getDepartureDateTime());
					otaFlightSegment.setDepartureDateTime(CommonUtil.getXMLGregorianCalendar(departureDateTimeCal));
					GregorianCalendar arrivalDateTimeCal = new GregorianCalendar();
					arrivalDateTimeCal.setTime(flightSegmentDTO.getArrivalDateTime());
					otaFlightSegment.setArrivalDateTime(CommonUtil.getXMLGregorianCalendar(arrivalDateTimeCal));

					otaFlightSegment.setJourneyDuration(CommonUtil
							.getDuration(flightSegmentDTO.getArrivalDateTimeZulu().getTime()
									- flightSegmentDTO.getDepartureDateTimeZulu().getTime()));
					otaFlightSegment.setFlightNumber(flightSegmentDTO.getFlightNumber().substring(2));

					FlightSegmentBaseType.DepartureAirport departureAirport = new FlightSegmentBaseType.DepartureAirport();
					departureAirport.setLocationCode(segmentCodeList[0]);
					departureAirport.setCodeContext(WebservicesConstants.OTAConstants.CODE_CONTEXT_IATA);
					departureAirport.setTerminal(flightSegmentDTO.getDepartureTerminal());

					FlightSegmentBaseType.ArrivalAirport arrivalAirport = new FlightSegmentBaseType.ArrivalAirport();
					arrivalAirport.setLocationCode(segmentCodeList[segmentCodeList.length - 1]);
					arrivalAirport.setCodeContext(WebservicesConstants.OTAConstants.CODE_CONTEXT_IATA);
					arrivalAirport.setTerminal(flightSegmentDTO.getArrivalTerminal());
					otaFlightSegment.setDepartureAirport(departureAirport);
					otaFlightSegment.setArrivalAirport(arrivalAirport);

					EquipmentType equipment = new EquipmentType();
					equipment.setAirEquipType(flightSegmentDTO.getFlightModelNumber());
					otaFlightSegment.getEquipment().add(equipment);

					MarketingAirline marketingAirline = new MarketingAirline();
					marketingAirline.setCompanyShortName(flightSegmentDTO.getFlightNumber().substring(0, 2));

					OperatingAirlineType operatingAirline = new OperatingAirlineType();
					operatingAirline.setCompanyShortName(flightSegmentDTO.getFlightNumber().substring(0, 2));

					otaFlightSegment.setMarketingAirline(marketingAirline);
					otaFlightSegment.setOperatingAirline(operatingAirline);

					List<Pair<String, Integer>> seatAvailList = seatAvailMap.get(flightSegmentDTO.getSegmentId());

					for (Pair<String, Integer> seatAvail : seatAvailList) {

						OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvail bookingClassAvail = new OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvail();
						bookingClassAvail.setResBookDesigCode(seatAvail.getLeft());
						bookingClassAvail.setResBookDesigQuantity(seatAvail.getRight().toString());
						otaFlightSegment.getBookingClassAvail().add(bookingClassAvail);
					}
					otaFlightSegment.setTicket(TicketType.E_TICKET);
				}
				if (!originDestinationInfoSet) {
					originDestinationInformation.setOriginLocation(origin);
					originDestinationInformation.setDestinationLocation(destination);
					originDestinationInfoSet = true;
				}
			}

		}

		return flightOptionsAvailable;
	}

	private static void setEmptyAvailability(OTAAirAvailRS otaAirAvailRS) {
		OTAAirAvailRS.OriginDestinationInformation originDestinationInformation = new OTAAirAvailRS.OriginDestinationInformation();
		otaAirAvailRS.getOriginDestinationInformation().add(originDestinationInformation);
		OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions originDestinationOptions = new OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions();
		originDestinationInformation.setOriginDestinationOptions(originDestinationOptions);
	}

}
