/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.core.web;

import java.util.Date;
import java.util.Timer;

import javax.servlet.http.HttpServlet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.platform.core.controller.ModuleFramework;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.config.WebservicesConfig;
import com.isa.thinair.webservices.core.session.SessExpirationTimerTask;
import com.isa.thinair.webservices.core.transaction.TxnExpirationTimeTask;
import com.isa.thinair.webservices.core.util.ExceptionUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

/**
 * @author Mohamed Nasly
 */
public class InitServlet extends HttpServlet {

	private static final long serialVersionUID = 1659584818002289334L;

	private static Log log = LogFactory.getLog(InitServlet.class);

	@Override
	public void init() {
		/**
		 * Initializing the application framework Module configurations are picked from first available of the
		 * followings - location specified by repository.home envioronment variable - location sepecified at
		 * PlatformConstants.getConfigRootAbsPath()
		 */
		ModuleFramework.startup();
		scheduleTasks();
		ExceptionUtil.initializeData();
		// CommonResources.initializeAirportInfo();
	}

	/**
	 * Starts session expiration and transaction timing out jobs
	 */
	private void scheduleTasks() {
		long sessTimeout = WebServicesModuleUtils.getWebServicesConfig().getUserSessTimeoutInMills();
		long txnTimeout = WebServicesModuleUtils.getWebServicesConfig().getTxnTimeoutInMillis();

		if (WebservicesConfig.SCHEDULER_OPTION_QUARTZ.equals(WebServicesModuleUtils.getWebServicesConfig().getSchedulerOption())) {

			try {
				LocalQuartzScheduler localQuartzScheduler = new LocalQuartzScheduler();
				localQuartzScheduler.scheduleAndStartScheduler();
			} catch (Exception e) {
				log.error("Starting Quartz scheduler failed", e);
				throw new RuntimeException(e);
			}
		} else {
			long sessCleaningInterval = WebServicesModuleUtils.getWebServicesConfig().getUserSessionCleaningGapInMillis();
			long txnCleaningInterval = WebServicesModuleUtils.getWebServicesConfig().getTxnCleaningGapInMillis();

			log.info("Starting sess expiration task [first execution at="
					+ WebservicesConstants.DATE_FORMAT_FOR_LOGGING.format(new Date(System.currentTimeMillis() + sessTimeout / 2))
					+ ",interval in mins=" + sessCleaningInterval);
			new Timer().scheduleAtFixedRate(new SessExpirationTimerTask(), sessTimeout / 2, sessCleaningInterval);

			log.info("Starting txn expiration task [first execution at="
					+ WebservicesConstants.DATE_FORMAT_FOR_LOGGING.format(new Date(System.currentTimeMillis() + txnTimeout / 2))
					+ ",interval in mins=" + txnCleaningInterval);
			new Timer().scheduleAtFixedRate(new TxnExpirationTimeTask(), txnTimeout / 2, txnCleaningInterval);
		}
		
		
		// FIXME move this to a scheduler job.
		if (WebServicesModuleUtils.getWebServicesConfig().isScheduleExtPayTxnRecon()) {
			log.info("Starting Txn Reconciliation Task [first execution at="
					+ WebservicesConstants.DATE_FORMAT_FOR_LOGGING.format(WebServicesModuleUtils.getWebServicesConfig()
							.getfirstReconStartTime()) + ",interval in mins="
					+ WebServicesModuleUtils.getWebServicesConfig().getExtPayTxnReconInterval() / (1000 * 60));
			new Timer().scheduleAtFixedRate(new TxnReconcilerTimerTask("EBI"), // FIXME
					WebServicesModuleUtils.getWebServicesConfig().getfirstReconStartTime(), WebServicesModuleUtils
							.getWebServicesConfig().getExtPayTxnReconInterval() / (1000 * 60));
		}

		if (WebServicesModuleUtils.getWebServicesConfig().isSchedulePublishInvForOpt()) {
			log.info("Starting Publish Inv For Opt Task [first execution at="
					+ WebservicesConstants.DATE_FORMAT_FOR_LOGGING.format(WebServicesModuleUtils.getWebServicesConfig()
							.getPublishInvForOptStartTime()) + ",interval in mins="
					+ WebServicesModuleUtils.getWebServicesConfig().getPublishInvForOptInterval() / (1000 * 60));
			new Timer().scheduleAtFixedRate(new PublishOptAlertTimerTask(), WebServicesModuleUtils.getWebServicesConfig()
					.getPublishInvForOptStartTime(), WebServicesModuleUtils.getWebServicesConfig().getPublishInvForOptInterval()
					/ (1000 * 60));
		}
	}
}
