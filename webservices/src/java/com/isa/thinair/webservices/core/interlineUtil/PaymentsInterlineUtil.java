package com.isa.thinair.webservices.core.interlineUtil;

import java.math.BigDecimal;
import java.util.Set;

import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.PaymentDetailType;

import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

/**
 * 
 * @author Manoj Dhanushka
 */
public class PaymentsInterlineUtil {

	public static CommonReservationAssembler getReservationAssembler(String groupPNR, LCCClientReservationPax reservationPax,
			BigDecimal refundAmount, String carrierVisePayments, ExchangeRateProxy exchangeRateProxy,
			PaymentDetailType paymentDetailType) throws ModuleException, WebservicesException {

		CommonReservationAssembler reservationAssembler = new CommonReservationAssembler();
		reservationAssembler.getLccreservation().setPNR(groupPNR);

		LCCClientPaymentAssembler paymentAssembler = getPaymentAssembler(refundAmount, carrierVisePayments, exchangeRateProxy,
				paymentDetailType);

		boolean isParent = reservationPax.getInfants().size() > 0 ? true : false;

		PaxAdditionalInfoDTO paxAdditionalInfoDTO = new PaxAdditionalInfoDTO();

		if (reservationPax.getLccClientAdditionPax() != null) {
			paxAdditionalInfoDTO.setPassportNo(reservationPax.getLccClientAdditionPax().getPassportNo());
			paxAdditionalInfoDTO.setPassportExpiry(reservationPax.getLccClientAdditionPax().getPassportExpiry());
			paxAdditionalInfoDTO.setPassportIssuedCntry(reservationPax.getLccClientAdditionPax().getPassportIssuedCntry());
			paxAdditionalInfoDTO.setEmployeeId(reservationPax.getLccClientAdditionPax().getEmployeeId());
			paxAdditionalInfoDTO.setDateOfJoin(reservationPax.getLccClientAdditionPax().getDateOfJoin());
			paxAdditionalInfoDTO.setIdCategory(reservationPax.getLccClientAdditionPax().getIdCategory());

		}

		if (isParent) {

			// Passenger with a Infant - Parent
			reservationAssembler.addParent(reservationPax.getFirstName(), reservationPax.getLastName(),
					reservationPax.getTitle(), reservationPax.getDateOfBirth(), reservationPax.getNationalityCode(),
					reservationPax.getPaxSequence(), reservationPax.getAttachedPaxId(), reservationPax.getTravelerRefNumber(),
					paxAdditionalInfoDTO, reservationPax.getPaxCategory(), paymentAssembler, null, null, null, null, null, null, null, null, null);
		} else if (PaxTypeTO.CHILD.equals(reservationPax.getPaxType())) {
			reservationAssembler.addChild(reservationPax.getFirstName(), reservationPax.getLastName(), reservationPax.getTitle(),
					reservationPax.getDateOfBirth(), reservationPax.getNationalityCode(), reservationPax.getPaxSequence(),
					reservationPax.getTravelerRefNumber(), paxAdditionalInfoDTO, reservationPax.getPaxCategory(),
					paymentAssembler, null, null, null, null, null, null, null, null, null, null);
		} else {
			// Passenger without a Infant - Single
			reservationAssembler.addSingle(reservationPax.getFirstName(), reservationPax.getLastName(),
					reservationPax.getTitle(), reservationPax.getDateOfBirth(), reservationPax.getNationalityCode(),
					reservationPax.getPaxSequence(), reservationPax.getTravelerRefNumber(), paxAdditionalInfoDTO,
					reservationPax.getPaxCategory(), paymentAssembler, null, null, null, null, null, null, null, null,
					null, null);
		}

		return reservationAssembler;
	}

	public static LCCClientPaymentAssembler getPaymentAssembler(BigDecimal refundAmount, String carrierVisePayments,
			ExchangeRateProxy exchangeRateProxy, PaymentDetailType paymentDetailType) throws ModuleException,
			WebservicesException {

		LCCClientPaymentAssembler paymentAssembler = new LCCClientPaymentAssembler();
		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();
		String selAgent = null;
		String hdnSelCurrency = null;

		Set<String> agentcolMethod = null;
		String paymentMethodConstant = validateAndGetPaymentMethod(paymentDetailType);
		if (paymentMethodConstant.equals(WebservicesConstants.PaymentTypes.ON_ACCOUNT)) {
			Agent agent = WebServicesModuleUtils.getTravelAgentBD().getAgent(getSelAgent(selAgent));
			if (hdnSelCurrency == null) {
				hdnSelCurrency = agent.getCurrencyCode();
			}
			if (agent != null) {
				agentcolMethod = agent.getPaymentMethod();
			}
		} else if (paymentMethodConstant.equals(WebservicesConstants.PaymentTypes.CASH)) {
			Agent agent = WebServicesModuleUtils.getTravelAgentBD().getAgent(userPrincipal.getAgentCode());
			if (agent != null) {
				agentcolMethod = agent.getPaymentMethod();
			}
		}

		String loggedInAgentCode = userPrincipal.getAgentCode();
		String loggedInAgentCurrencyCode = userPrincipal.getAgentCurrencyCode();

		if (paymentMethodConstant.equals(WebservicesConstants.PaymentTypes.ON_ACCOUNT)) {
			// PayCurrencyDTO payCurrencyDTO = PaymentUtil.getOnAccPayCurrencyDTO(hdnSelCurrency, loggedInAgentCode,
			// getSelAgent(selAgent), exchangeRateProxy);
			// paymentAssembler.addAgentCreditPayment(getSelAgent(selAgent), refundAmount, payCurrencyDTO, new Date(),
			// null,
			// null, Agent.PAYMENT_MODE_ONACCOUNT);
		} else if (paymentMethodConstant.equals(WebservicesConstants.PaymentTypes.CASH)) {
			// PayCurrencyDTO payCurrencyDTO = PaymentUtil.getOnAccPayCurrencyDTO(loggedInAgentCurrencyCode,
			// loggedInAgentCode,
			// loggedInAgentCode, exchangeRateProxy);
			// paymentAssembler.addCashPayment(refundAmount, payCurrencyDTO, new Date(), null, null);
		} else if (paymentMethodConstant.equals(WebservicesConstants.PaymentTypes.CREDIT_CARD)) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Not Supported]");

		}

		// adding the carrier vise payments . use for payment refund on dry/interline
		paymentAssembler.getPayments().iterator().next().setCarrierVisePayments(carrierVisePayments);

		return paymentAssembler;
	}

	/**
	 * Checks if the DTO Sent is proper and extracts the necessary info to IPayment.
	 * 
	 * @param paymentDetailType
	 * @return
	 * @throws WebservicesException
	 */
	public static BigDecimal validateAndGetPaymentInfoForRefund(PaymentDetailType paymentDetailType) throws WebservicesException {

		// if paymentDetailType is null?
		if (paymentDetailType == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Payment details not set]");
		}

		// Hold the payment type with the amount.
		// IPayment payment = new PaymentAssembler();
		// Get the paymentMethod type as a constant.
		String paymentMethodConstant = validateAndGetPaymentMethod(paymentDetailType);

		// If the payment method was undecided
		if (paymentMethodConstant == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Payment type not set]");
		}

		// Get the payment amount
		BigDecimal paymentAmount = paymentDetailType.getPaymentAmount() == null ? null : paymentDetailType.getPaymentAmount()
				.getAmount();

		// if payment amount was not set
		if (paymentAmount == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Payment amount not set]");
		}

		// // Add payment amount
		// if (paymentMethodConstant.equals(WebservicesConstants.PaymentTypes.CASH)) {
		// // TODO Support pay currency refund operations
		// payment.addCashPayment(paymentAmount, null, null, null, null, null);
		// }
		//
		// // Add on account amount
		// if (paymentMethodConstant.equals(WebservicesConstants.PaymentTypes.ON_ACCOUNT)) {
		// // TODO Support pay currency refund operations
		// payment.addAgentCreditPayment(paymentDetailType.getDirectBill().getDirectBillID(), paymentAmount, null, null,
		// null);
		// }

		// Add credit card amount
		if (paymentMethodConstant.equals(WebservicesConstants.PaymentTypes.CREDIT_CARD)) {
			// TODO Support pay currency refund operations
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "[Not Supported]");
		}
		return paymentAmount;
	}

	/**
	 * Will check the DTO Structor and will return a constant string of the payment type.
	 * 
	 * @param paymentDetailType
	 * @return
	 */
	private static String validateAndGetPaymentMethod(PaymentDetailType paymentDetailType) {

		// initialize returning constant value.
		String constant = null;
		// check if null
		if (paymentDetailType == null) {
			return null;
		}

		// if cash and no card info and no direct bill info then its cash payment
		if (paymentDetailType.getCash().isCashIndicator() && paymentDetailType.getPaymentCard().getCardHolderRPH() == null
				&& paymentDetailType.getDirectBill().getDirectBillID() == null) {
			constant = WebservicesConstants.PaymentTypes.CASH;
		}

		// if Credit Card and no cash and no direct bill info then its Credit Card Payment
		if (paymentDetailType.getPaymentCard().getCardHolderRPH() != null
				&& paymentDetailType.getCash().isCashIndicator() == false
				&& paymentDetailType.getDirectBill().getDirectBillID() == null) {
			constant = WebservicesConstants.PaymentTypes.CREDIT_CARD;
		}
		// if Direct Bill Info and no credi card info and no cash info then its onaccount type
		if (paymentDetailType.getDirectBill().getDirectBillID() != null && paymentDetailType.getCash().isCashIndicator() == false
				&& paymentDetailType.getPaymentCard().getCardHolderRPH() == null) {
			constant = WebservicesConstants.PaymentTypes.ON_ACCOUNT;
		}

		return constant;
	}

	/**
	 * get the selected refund agent
	 * 
	 * @param selAgnet
	 * @return
	 */
	private static String getSelAgent(String selAgent) {
		if (selAgent != null && !selAgent.isEmpty()) {
			return selAgent;
		} else {
			return ThreadLocalData.getCurrentUserPrincipal().getAgentCode();
		}
	}
}
