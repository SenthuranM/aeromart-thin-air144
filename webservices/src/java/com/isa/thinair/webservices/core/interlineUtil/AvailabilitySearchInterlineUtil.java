/**
 * 
 */
package com.isa.thinair.webservices.core.interlineUtil;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AAOTAAirAllPriceAvailRS;
import org.opentravel.ota._2003._05.AAPricedItinerariesType;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.AirItineraryType.OriginDestinationOptions;
import org.opentravel.ota._2003._05.AirSearchPrefsType.CabinPref;
import org.opentravel.ota._2003._05.AirTripType;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.FlightSegmentBaseType;
import org.opentravel.ota._2003._05.FreeTextType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirAvailRQ;
import org.opentravel.ota._2003._05.OTAAirAvailRS;
import org.opentravel.ota._2003._05.OTAAirAvailRS.AAAirAvailRSExt;
import org.opentravel.ota._2003._05.OTAAirAvailRS.OriginDestinationInformation;
import org.opentravel.ota._2003._05.OTAAirPriceRQ;
import org.opentravel.ota._2003._05.OTAAirPriceRS;
import org.opentravel.ota._2003._05.OriginDestinationInformationType;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.PricedItinerariesType;
import org.opentravel.ota._2003._05.PricedItineraryType;
import org.opentravel.ota._2003._05.SuccessType;
import org.opentravel.ota._2003._05.TimeInstantType;
import org.opentravel.ota._2003._05.TravelerInfoSummaryType;
import org.opentravel.ota._2003._05.TravelerInformationType;
import org.opentravel.ota._2003._05.WarningsType;
import org.springframework.util.CollectionUtils;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.util.BookingClassUtil;
import com.isa.thinair.airmaster.api.dto.SegmentCarrierDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.CabinClassWiseDefaultLogicalCCDetailDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.webplatform.api.util.AddModifySurfaceSegmentValidations;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.v2.reservation.FlightSearchDTO;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger.AnalyticSource;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.webservices.api.dtos.FlexiFareSelectionCriteria;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.adaptor.impl.availability.AvailabilityRSAdaptor;
import com.isa.thinair.webservices.core.adaptor.impl.pricing.AirIternaryOriginDestinationOptionAdaptor;
import com.isa.thinair.webservices.core.adaptor.util.AdaptorUtil;
import com.isa.thinair.webservices.core.cache.delegator.CacheTemplateInvoker;
import com.isa.thinair.webservices.core.cache.util.CacheConstant;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;
import com.isa.thinair.webservices.core.util.AvailabilitySearchUtil;
import com.isa.thinair.webservices.core.util.BaseUtil;
import com.isa.thinair.webservices.core.util.CommonServicesUtil;
import com.isa.thinair.webservices.core.util.ServiceTaxUtil;
import com.isa.thinair.webservices.core.util.WSReservationUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.isa.thinair.webservices.core.util.WebservicesContext;


/**
 * @author nafly changed to Airporoxy Request
 * 
 */
public class AvailabilitySearchInterlineUtil extends BaseUtil {

	private static final Log log = LogFactory.getLog(AvailabilitySearchInterlineUtil.class);

	private static GlobalConfig globalConfig = WebServicesModuleUtils.getGlobalConfig();

	public AvailabilitySearchInterlineUtil() {
		super();
	}

	public OTAAirAvailRS getAvailability(OTAAirAvailRQ oTAAirAvailRQ) throws ModuleException, WebservicesException {
		OTAAirAvailRS otaAirAvailRS = new OTAAirAvailRS();
		otaAirAvailRS.setErrors(new ErrorsType());
		otaAirAvailRS.setWarnings(new WarningsType());

		// As the user is initiating a new availability search we have to clear the FareSegChargeTO from the
		// transaction.
		PriceQuoteInterlineUtil.clearFareSegmentChargeTO();
		PriceQuoteInterlineUtil.clearPriceInfo();
		PriceQuoteInterlineUtil.clearReturnPointAirport();
		
		FlightAvailRQ flightAvailRQ = null;
		boolean isMulticitySearch = isMulticitySearch(oTAAirAvailRQ);
		
		if(!isMulticitySearch){
			if (!validateOTAAirAvailRQ(oTAAirAvailRQ)) {
				log.warn("Invalid flight search request found in OTAAirAvailRQ. Selected segment can't be modified to the given route");

				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_ROUTE_MODIFICATION, 
											"Selected route modification for this segment is not allowed. Please cancel and do a fresh booking");
			}
			flightAvailRQ = createFlightAvailRQ(oTAAirAvailRQ,true);
		}else{
			flightAvailRQ = createMulticityFlightAvailRQ(oTAAirAvailRQ);
		}

		FlightAvailRS flightAvailRS;

		if (flightAvailRQ.getAvailPreferences().isQuoteFares()) {
			AnalyticsLogger.logAvlSearch(AnalyticSource.WS_CRE_AVL_WFQ, flightAvailRQ, ThreadLocalData.getCurrentUserPrincipal(),
					null);
			flightAvailRS = WebServicesModuleUtils.getAirproxySearchAndQuoteBD().searchAvailableFlightsWithAllFares(
					flightAvailRQ, null);
		} else {
			AnalyticsLogger.logAvlSearch(AnalyticSource.WS_CRE_AVL, flightAvailRQ, ThreadLocalData.getCurrentUserPrincipal(),
					null);
			flightAvailRS = WebServicesModuleUtils.getAirproxySearchAndQuoteBD().searchAvailableFlights(flightAvailRQ, null, true);
		}

		if (flightAvailRS.getApplicableServiceTaxes().size() > 0) {
			ThreadLocalData.setCurrentTnxParam(Transaction.APPLICABLE_SERVICE_TAXES,
					flightAvailRS.getApplicableServiceTaxes());
		}

		if(!isMulticitySearch){
			createFlightAvailRS(oTAAirAvailRQ, otaAirAvailRS, flightAvailRQ, flightAvailRS);
		}else{
			createMulticityFlightAvailRS(oTAAirAvailRQ, otaAirAvailRS, flightAvailRQ, flightAvailRS);
		}

		return otaAirAvailRS;
	}

	/**
	 * Transform OTA Air Avail Request to Flight Avail Request
	 * 
	 * @author nafly
	 * @param oTAAirAvailRQ
	 * @return
	 */
	public static FlightAvailRQ createFlightAvailRQ(OTAAirAvailRQ oTAAirAvailRQ,boolean isSingleAvailability) throws ModuleException, WebservicesException {
		FlightAvailRQ flightAvailRQ = new FlightAvailRQ();

		Collection<String> obAirports = new HashSet<String>();
		Collection<String> inAirports = new HashSet<String>();
		Collection<String> cabinClasses = new ArrayList<String>(); // OTAAirAvailRQ support per OND booking class but AA
																	// not. All the BC per OND should be equal

		// setting origin destination information
		OriginDestinationInformationTO depatureOnD = flightAvailRQ.addNewOriginDestinationInformation();

		Integer departureVariance = null;
		Integer returnVariance = null;

		Date returnDate = null;
		boolean isReturn = false;
		boolean isCityBaseSearchEnabled = AppSysParamsUtil.enableCityBasesFunctionality();
		OriginDestinationInformationType returnOnDType = null;		
		
		List<FlightSearchDTO> searchDTOs= new ArrayList<FlightSearchDTO>();
		
		for (OriginDestinationInformationType orgDestInfoType : oTAAirAvailRQ.getOriginDestinationInformation()) {

			/** departure date of the very first segment is taken as departure date */
			if (depatureOnD.getDepartureDateTimeStart() == null) {
				depatureOnD.setDepartureDateTimeStart(CommonUtil.parseDate(orgDestInfoType.getDepartureDateTime().getValue())
						.getTime());
			}

			/** very first airport is taken as origin */
			if (obAirports.isEmpty()) {
				depatureOnD.setOrigin(orgDestInfoType.getOriginLocation().getLocationCode());
			}

			if (!isReturn
					&& CommonUtil.isReturnSegment(obAirports, orgDestInfoType.getOriginLocation().getLocationCode(),
							orgDestInfoType.getDestinationLocation().getLocationCode())) {
				isReturn = true;
			}

			// Get the departure and return variance

			if (isSingleAvailability) {
				/** departure/return variance */
				if (orgDestInfoType.getDepartureDateTime().getWindowBefore() != null
						|| orgDestInfoType.getDepartureDateTime().getWindowAfter() != null) {

					int windowBefore = orgDestInfoType.getDepartureDateTime().getWindowBefore() != null ? orgDestInfoType
							.getDepartureDateTime().getWindowBefore().getDays() : 0;
					int windowAfter = orgDestInfoType.getDepartureDateTime().getWindowAfter() != null ? orgDestInfoType
							.getDepartureDateTime().getWindowAfter().getDays() : 0;

					if (!isReturn) {
						if (departureVariance == null || departureVariance.intValue() < windowBefore
								|| departureVariance.intValue() < windowAfter) {
							departureVariance = Math.max(windowBefore, windowAfter);
						}
					} else {
						if (returnVariance == null || returnVariance.intValue() < windowBefore
								|| returnVariance.intValue() < windowAfter) {
							returnVariance = Math.max(windowBefore, windowAfter);
						}
					}
				}
			}

			/** last station of the outbound journey is taken as the destination airport */
			if (!isReturn) {
				depatureOnD.setDestination(orgDestInfoType.getDestinationLocation().getLocationCode());
			}
			
			/** update city base search flag for OriginDestinationInformationTO */
			setMultiAirportCitySearchForOnDInfoTO(isCityBaseSearchEnabled, orgDestInfoType, depatureOnD);

			/** departure date of the very first return segment is taken as the arrival date */
			if (isReturn && returnDate == null) {
				returnDate = CommonUtil.parseDate(orgDestInfoType.getDepartureDateTime().getValue()).getTime();
				returnOnDType = orgDestInfoType;
			}

			if (!isReturn) {
				obAirports.add(orgDestInfoType.getOriginLocation().getLocationCode());
				obAirports.add(orgDestInfoType.getDestinationLocation().getLocationCode());
			} else {
				inAirports.add(orgDestInfoType.getOriginLocation().getLocationCode());
				inAirports.add(orgDestInfoType.getDestinationLocation().getLocationCode());
			}

			/** Adding the cabin classes for validation */
			if (orgDestInfoType instanceof org.opentravel.ota._2003._05.OTAAirAvailRQ.OriginDestinationInformation) { // Collide
																														// with
																														// the
																														// RS.OriginDestinationInformation
				org.opentravel.ota._2003._05.OTAAirAvailRQ.OriginDestinationInformation odInfo = (org.opentravel.ota._2003._05.OTAAirAvailRQ.OriginDestinationInformation) orgDestInfoType;
				if (odInfo.getTravelPreferences() != null && odInfo.getTravelPreferences().getCabinPref() != null) {
					CabinPref cabinPref = BeanUtils.getFirstElement(odInfo.getTravelPreferences().getCabinPref());
					if (cabinPref.getCabin() != null) {
						cabinClasses.add(cabinPref.getCabin().value());
					}
				}
			}
			// Ground segment not allowed in air proxy request
			// if (orgDestInfoType.isGroundSegmentAvailability() != null &&
			// (orgDestInfoType.isGroundSegmentAvailability())) {
			// availableFlightSearchDTO.getSearchDTO().setGroundSegmentAvailability(true);
			// }

			FlightSearchDTO searchDTO = new FlightSearchDTO();
			searchDTO.setFromAirport(orgDestInfoType.getOriginLocation().getLocationCode());
			searchDTO.setToAirport(orgDestInfoType.getDestinationLocation().getLocationCode());

			searchDTOs.add(searchDTO);
			
		}
		// Set the cabin class details
		String cabinClass = getCabinClass(cabinClasses, oTAAirAvailRQ);
		// String defaultLogicalCCCode = getLogicalCabinClass(cabinClass);

		SegmentCarrierDTO segmentCarrierDTO = null;
		boolean areAirportsValid = false;

		// AARESAA-18177 Temp fix TODO once test is completed un comment this block
		// if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
		// segmentCarrierDTO = CarrierCacheUtil.getInstance().getCarrierDetails(depatureOnD.getOrigin(),
		// depatureOnD.getDestination());
		//
		// if (segmentCarrierDTO != null) {
		// areAirportsValid = true;
		// }
		// } else {
		// areAirportsValid =
		// WebServicesModuleUtils.getWebServiceDAO().isFromAndToAirportsValid(depatureOnD.getOrigin(),
		// depatureOnD.getDestination());
		//
		// if (areAirportsValid) {
		// segmentCarrierDTO = new SegmentCarrierDTO();
		// segmentCarrierDTO.setSystem(SYSTEM.AA);
		// }
		//
		// }

		// AARESAA-18177 Temp fix TODO once test is completed remove this block
		areAirportsValid = WebServicesModuleUtils.getWebServiceDAO().isFromAndToAirportsValid(depatureOnD.getOrigin(),
				depatureOnD.getDestination());
		
		if (!areAirportsValid && isCityBaseSearchEnabled
				&& (depatureOnD.isDepartureCitySearch() || depatureOnD.isArrivalCitySearch())) {
			areAirportsValid = WebServicesModuleUtils.getWebServiceDAO()
					.isOriginDestinaitionValidCitySearch(depatureOnD.getOrigin(), depatureOnD.getDestination());
		}

		if (areAirportsValid) {
			segmentCarrierDTO = new SegmentCarrierDTO();
			segmentCarrierDTO.setSystem(SYSTEM.AA);
		}

		if (!areAirportsValid) {
			log.warn("Invalid flight search request found in createFlightAvailRQ(OTAAirAvailRQ). "
					+ "From/To aiports validation failed. " + "[From airport=" + depatureOnD.getOrigin() + "," + "To airport="
					+ depatureOnD.getDestination() + "]");

			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_OND, "Invalid From and/or To Airport(s)");
		}

		// Setting Travel agent code
		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
		AvailPreferencesTO availPref = flightAvailRQ.getAvailPreferences();
		availPref.setTravelAgentCode(principal.getAgentCode());

		availPref.setRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED);

		if (oTAAirAvailRQ.isDirectFlightsOnly()) {
			availPref.setFlightsPerOndRestriction(AvailableFlightSearchDTO.SINGLE_FLIGHTS_ONLY);
		} else {
			availPref.setFlightsPerOndRestriction(AvailableFlightSearchDTO.ALL_SINGLE_CONNECTED_FLIGHTS);
		}

		if (returnDate != null) {
			availPref.setHalfReturnFareQuote(AppSysParamsUtil.isAllowHalfReturnFaresForNewBookings());
		}

		if (isCharterAgent(principal)) {
			availPref.setFixedFareAgent(true);
		}

		availPref.setSearchSystem(segmentCarrierDTO.getSystem());
		availPref.setAppIndicator(ApplicationEngine.WS);

		TravelerInfoSummaryType travelerInfoSummaryType = oTAAirAvailRQ.getTravelerInfoSummary();

		if (travelerInfoSummaryType.getPriceRequestInformation() != null) {
			availPref.setPreferredCurrency(travelerInfoSummaryType.getPriceRequestInformation().getCurrencyCode());
		}
		/** Journey having exact reverse segments of outbound segments is considered as a Return journey */
		if (depatureOnD.isReturnFlag()) {
			if (!CommonUtil.validateReturnJouney(obAirports, inAirports)) {
				log.error("ERROR occured in createFlightAvailRQ(OTAAirAvailRQ). " + "Return journey verification failed. "
						+ "[outbound airports=" + CommonUtil.getCollectionElementsStr(obAirports) + "," + "inbound airports="
						+ CommonUtil.getCollectionElementsStr(inAirports) + "]");
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Flight Segment(s)");
			}
		}

		// Set departure date time variance

		Date depatureDate = depatureOnD.getDepartureDateTimeStart();
		Date depatureDateTimeStart = CalendarUtil.getStartTimeOfDate(depatureDate);
		depatureDateTimeStart = CalendarUtil.getOfssetAddedTime(depatureDateTimeStart,
				departureVariance != null ? Math.min(3, departureVariance.intValue()) * -1 : 0);

		Date depatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(depatureDate);
		depatureDateTimeEnd = CalendarUtil.getOfssetAddedTime(depatureDateTimeEnd,
				departureVariance != null ? Math.min(3, departureVariance.intValue()) : 0);

		TravelPreferencesTO travelerPref = flightAvailRQ.getTravelPreferences();

		String bookingClass = null;
		String bookingType = BookingClass.BookingClassType.NORMAL;

		if (oTAAirAvailRQ.getSpecificFlightInfo() != null && oTAAirAvailRQ.getSpecificFlightInfo().getBookingClassPref() != null) {
			bookingClass = oTAAirAvailRQ.getSpecificFlightInfo().getBookingClassPref().getResBookDesigCode();
			if (bookingClass != null) {
				travelerPref.setBookingClassCode(bookingClass);
				Collection<String> standByBCList = WebServicesModuleUtils.getBookingClassBD().getStandbyBookingClasses();
				if (standByBCList.contains(bookingClass)) {
					bookingType = BookingClass.BookingClassType.STANDBY;
					bookingClass = null;
					availPref.setRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION);
				}
			}
		}
		
		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

		boolean viewFlightsWithLesserSeats = AuthorizationUtil.hasPrivilege(privilegeKeys,
				PrivilegesKeys.MakeResPrivilegesKeys.MAKE_RES_ALLFLIGHTS);

		availPref.setRestrictionLevel(getAvailabilityRestrictionLevel(viewFlightsWithLesserSeats, bookingType));

		
		
		depatureOnD.setPreferredDate(depatureDate);
		depatureOnD.setDepartureDateTimeStart(depatureDateTimeStart);
		depatureOnD.setDepartureDateTimeEnd(depatureDateTimeEnd);
		depatureOnD.setPreferredClassOfService(cabinClass);
		depatureOnD.setPreferredBookingType(bookingType);
		depatureOnD.setPreferredBookingClass(bookingClass);
		// depatureOnD.setPreferredLogicalCabin(defaultLogicalCCCode);
		Map<Integer, Boolean> ondFlexiQuote = new HashMap<Integer, Boolean>();
		if (oTAAirAvailRQ.getFlexiQuote() != null) {
			ondFlexiQuote.put(OndSequence.OUT_BOUND, oTAAirAvailRQ.getFlexiQuote());
		} else {
			ondFlexiQuote.put(OndSequence.OUT_BOUND, false);
		}
		travelerPref.setBookingType(bookingType);

		// Set return date time variance if its a return flight
		if (returnDate != null) {
			// setting the return journey information
			OriginDestinationInformationTO returnOnD = flightAvailRQ.addNewOriginDestinationInformation();
			Date returnDateTimeStart = CalendarUtil.getStartTimeOfDate(returnDate);
			returnDateTimeStart = CalendarUtil.getOfssetAddedTime(returnDateTimeStart,
					returnVariance != null ? Math.min(3, returnVariance.intValue()) * -1 : 0);

			Date returnDateTimeEnd = CalendarUtil.getEndTimeOfDate(returnDate);
			returnDateTimeEnd = CalendarUtil.getOfssetAddedTime(returnDateTimeEnd,
					returnVariance != null ? Math.min(3, returnVariance.intValue()) : 0);

			returnOnD.setPreferredDate(returnDate);
			returnOnD.setDepartureDateTimeStart(returnDateTimeStart);
			returnOnD.setDepartureDateTimeEnd(returnDateTimeEnd);

			returnOnD.setOrigin(depatureOnD.getDestination());
			returnOnD.setDestination(depatureOnD.getOrigin());
			returnOnD.setReturnFlag(true);
			returnOnD.setPreferredClassOfService(cabinClass);
			returnOnD.setPreferredBookingType(bookingType);
			returnOnD.setPreferredBookingClass(bookingClass);
			// returnOnD.setPreferredLogicalCabin(defaultLogicalCCCode);
			if (oTAAirAvailRQ.getFlexiQuote() != null) {
				ondFlexiQuote.put(OndSequence.IN_BOUND, oTAAirAvailRQ.getFlexiQuote());
			} else {
				ondFlexiQuote.put(OndSequence.IN_BOUND, false);
			}
			
			setMultiAirportCitySearchForOnDInfoTO(isCityBaseSearchEnabled, returnOnDType, returnOnD);
			
			returnOnD.setPreferredBookingType(bookingType);
			returnOnD.setPreferredBookingClass(bookingClass);
			ThreadLocalData.setCurrentTnxParam(Transaction.RETURN_POINT_AIRPORT, returnOnD.getOrigin());
		}

		flightAvailRQ.getAvailPreferences().setQuoteOndFlexi(ondFlexiQuote);

		// Get Pax type and counts
		getTravelInfomationSummary(travelerInfoSummaryType, flightAvailRQ);

		// Setting Travel preference information such as booking class and booking type
		

		travelerPref.setBookingType(bookingType);

		String pnr = null;
		if (oTAAirAvailRQ.getModifiedSegmentInfo() != null) {
			pnr = oTAAirAvailRQ.getModifiedSegmentInfo().getBookingReferenceID().get(0).getID();

		}

		if (pnr != null) {
			boolean isFlightInClosure = false;
			List<Date> depDateList = new ArrayList<Date>();
			for (OriginDestinationOptionType originDestOpt : oTAAirAvailRQ.getModifiedSegmentInfo().getAirItinerary()
					.getOriginDestinationOptions().getOriginDestinationOption()) {
				for (BookFlightSegmentType flightSegmentType : originDestOpt.getFlightSegment()) {
					String segmentCode = flightSegmentType.getDepartureAirport().getLocationCode() + "/"
							+ flightSegmentType.getArrivalAirport().getLocationCode();
					Date depDateLocal = flightSegmentType.getDepartureDateTime().toGregorianCalendar().getTime();
					Date depatureDateZulu = DateUtil.getZuluDateTime(depDateLocal, flightSegmentType.getDepartureAirport()
							.getLocationCode());
					depDateList.add(depatureDateZulu);
					int flightId = WebServicesModuleUtils.getFlightBD().getFlightID(segmentCode,
							flightSegmentType.getFlightNumber(), depatureDateZulu);
					isFlightInClosure = ReservationApiUtils.hasReachedFlightClosure(
							FlightUtil.getApplicableResCutoffTime(flightId), depDateList);
					if (!flightAvailRQ.getAvailPreferences().isSourceFlightInCutOffTime()) {
						flightAvailRQ.getAvailPreferences().setSourceFlightInCutOffTime(isFlightInClosure);
					}
				}
			}
			flightAvailRQ.getAvailPreferences().setModifyBooking(true);
		}else{
			boolean onlyGroundSegments = AddModifySurfaceSegmentValidations.isOnlyGroundSegments(searchDTOs);
			if (onlyGroundSegments) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION,
						"Cannot search only for a bus segment ");
			}
		}
		
		if (pnr != null && AppSysParamsUtil.isRequoteEnabled()) {
			flightAvailRQ.getAvailPreferences().setQuoteFares(false);
		} else {
			flightAvailRQ.getAvailPreferences().setQuoteFares(true);
		}

		flightAvailRQ.getAvailPreferences().setQuoteFares(true);
		// TODO Modify segment information needs to be added

		return flightAvailRQ;
	}
	
	public static FlightAvailRQ createMulticityFlightAvailRQ(OTAAirAvailRQ oTAAirAvailRQ) throws ModuleException, WebservicesException {
		FlightAvailRQ flightAvailRQ = new FlightAvailRQ();
		TravelPreferencesTO travelerPref = flightAvailRQ.getTravelPreferences();
		AvailPreferencesTO availPref = flightAvailRQ.getAvailPreferences();
		
		Integer departureVariance = null;
		String bookingClass = null;
		SegmentCarrierDTO segmentCarrierDTO = null;
		boolean areAirportsValid = false;
		String cabinClass = null;
		String bookingType = BookingClass.BookingClassType.NORMAL;

		if (oTAAirAvailRQ.getSpecificFlightInfo() != null && oTAAirAvailRQ.getSpecificFlightInfo().getBookingClassPref() != null) {
			bookingClass = oTAAirAvailRQ.getSpecificFlightInfo().getBookingClassPref().getResBookDesigCode();
			if (bookingClass != null) {
				travelerPref.setBookingClassCode(bookingClass);
				Collection<String> standByBCList = WebServicesModuleUtils.getBookingClassBD().getStandbyBookingClasses();
				if (standByBCList.contains(bookingClass)) {
					bookingType = BookingClass.BookingClassType.STANDBY;
					bookingClass = null;
					
					availPref.setRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION);
				}
			}
		}
		
		travelerPref.setBookingType(bookingType);
		boolean isCityBaseSearchEnabled = AppSysParamsUtil.enableCityBasesFunctionality();
		
		List<FlightSearchDTO> searchDTOs = new ArrayList<FlightSearchDTO>();
		
		for (OriginDestinationInformationType orgDestInfoType : oTAAirAvailRQ.getOriginDestinationInformation()) {
			
			OriginDestinationInformationTO ondInfoTO = flightAvailRQ.addNewOriginDestinationInformation();
			
			ondInfoTO.setOrigin(orgDestInfoType.getOriginLocation().getLocationCode());
			ondInfoTO.setDestination(orgDestInfoType.getDestinationLocation().getLocationCode());

			Date depatureDate = CommonUtil.parseDate(orgDestInfoType.getDepartureDateTime().getValue()).getTime();

			// Get the departure and return variance

			/** departure variance */
			if (orgDestInfoType.getDepartureDateTime().getWindowBefore() != null
					|| orgDestInfoType.getDepartureDateTime().getWindowAfter() != null) {

				int windowBefore = orgDestInfoType.getDepartureDateTime().getWindowBefore() != null ? orgDestInfoType
						.getDepartureDateTime().getWindowBefore().getDays() : 0;
				int windowAfter = orgDestInfoType.getDepartureDateTime().getWindowAfter() != null ? orgDestInfoType
						.getDepartureDateTime().getWindowAfter().getDays() : 0;


				if (departureVariance == null || departureVariance.intValue() < windowBefore
							|| departureVariance.intValue() < windowAfter) {
						departureVariance = Math.max(windowBefore, windowAfter);
				}
		
			}
			
			// Set departure date time variance

			Date depatureDateTimeStart = CalendarUtil.getStartTimeOfDate(depatureDate);
			depatureDateTimeStart = CalendarUtil.getOfssetAddedTime(depatureDateTimeStart,
					departureVariance != null ? Math.min(3, departureVariance.intValue()) * -1 : 0);

			Date depatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(depatureDate);
			depatureDateTimeEnd = CalendarUtil.getOfssetAddedTime(depatureDateTimeEnd,
					departureVariance != null ? Math.min(3, departureVariance.intValue()) : 0);

			/** Adding the cabin classes for validation */
			if (orgDestInfoType instanceof org.opentravel.ota._2003._05.OTAAirAvailRQ.OriginDestinationInformation) { // Collide
																														// with
																														// the
																														// RS.OriginDestinationInformation
				org.opentravel.ota._2003._05.OTAAirAvailRQ.OriginDestinationInformation odInfo = (org.opentravel.ota._2003._05.OTAAirAvailRQ.OriginDestinationInformation) orgDestInfoType;
				if (odInfo.getTravelPreferences() != null && odInfo.getTravelPreferences().getCabinPref() != null) {
					CabinPref cabinPref = BeanUtils.getFirstElement(odInfo.getTravelPreferences().getCabinPref());
					if (cabinPref.getCabin() != null) {
						cabinClass = cabinPref.getCabin().value();
					}
				}
			}
			setMultiAirportCitySearchForOnDInfoTO(isCityBaseSearchEnabled, orgDestInfoType, ondInfoTO);
			// Set the cabin class details
			cabinClass = getCabinClassMultiCity(cabinClass);
		
			ondInfoTO.setPreferredDate(depatureDate);
			ondInfoTO.setDepartureDateTimeStart(depatureDateTimeStart);
			ondInfoTO.setDepartureDateTimeEnd(depatureDateTimeEnd);
			ondInfoTO.setPreferredClassOfService(cabinClass);
			ondInfoTO.setPreferredBookingClass(bookingClass);
			ondInfoTO.setPreferredBookingType(bookingType);
			

			OriginDestinationOptionTO depOndOptionTO = new OriginDestinationOptionTO();

			areAirportsValid = false;

			areAirportsValid = WebServicesModuleUtils.getWebServiceDAO().isFromAndToAirportsValid(ondInfoTO.getOrigin(),
					ondInfoTO.getDestination());
			
			if (!areAirportsValid && isCityBaseSearchEnabled
					&& (ondInfoTO.isDepartureCitySearch() || ondInfoTO.isArrivalCitySearch())) {
				areAirportsValid = WebServicesModuleUtils.getWebServiceDAO()
						.isOriginDestinaitionValidCitySearch(ondInfoTO.getOrigin(), ondInfoTO.getDestination());
			}

			if (areAirportsValid) {
				segmentCarrierDTO = new SegmentCarrierDTO();
				segmentCarrierDTO.setSystem(SYSTEM.AA);
			}

			if (!areAirportsValid) {
				log.warn("Invalid flight search request found in createFlightAvailRQ(OTAAirAvailRQ). "
						+ "From/To aiports validation failed. " + "[From airport=" + ondInfoTO.getOrigin() + "," + "To airport="
						+ ondInfoTO.getDestination() + "]");

				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_OND, "Invalid From and/or To Airport(s)");
			}

			ondInfoTO.getOrignDestinationOptions().add(depOndOptionTO);
			
			FlightSearchDTO searchDTO = new FlightSearchDTO();
			searchDTO.setFromAirport(orgDestInfoType.getOriginLocation().getLocationCode());
			searchDTO.setToAirport(orgDestInfoType.getDestinationLocation().getLocationCode());

			searchDTOs.add(searchDTO);

		}

		boolean createFlow = true;
		if (oTAAirAvailRQ.getModifiedSegmentInfo() != null) {
			createFlow = false;
		}

		boolean onlyGroundSegments = createFlow && AddModifySurfaceSegmentValidations.isOnlyGroundSegments(searchDTOs);
		if (onlyGroundSegments) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION,
					"Cannot search only for a bus segment ");
		}
				
		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();

		availPref.setTravelAgentCode(principal.getAgentCode());

		availPref.setRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED);

		if (oTAAirAvailRQ.isDirectFlightsOnly()) {
			availPref.setFlightsPerOndRestriction(AvailableFlightSearchDTO.SINGLE_FLIGHTS_ONLY);
		} else {
			availPref.setFlightsPerOndRestriction(AvailableFlightSearchDTO.ALL_SINGLE_CONNECTED_FLIGHTS);
		}


		if (isCharterAgent(principal)) {
			availPref.setFixedFareAgent(true);
		}

		availPref.setSearchSystem(segmentCarrierDTO.getSystem());
		availPref.setAppIndicator(ApplicationEngine.WS);

		TravelerInfoSummaryType travelerInfoSummaryType = oTAAirAvailRQ.getTravelerInfoSummary();

		if (travelerInfoSummaryType.getPriceRequestInformation() != null) {
			availPref.setPreferredCurrency(travelerInfoSummaryType.getPriceRequestInformation().getCurrencyCode());
		}
		
		availPref.setMultiCitySearch(true);
		
		Map<Integer, Boolean> ondFlexiQuote = new HashMap<Integer, Boolean>();
		if (oTAAirAvailRQ.getFlexiQuote() != null) {
			ondFlexiQuote.put(OndSequence.OUT_BOUND, oTAAirAvailRQ.getFlexiQuote());
		} else {
			ondFlexiQuote.put(OndSequence.OUT_BOUND, false);
		}

		flightAvailRQ.getAvailPreferences().setQuoteOndFlexi(ondFlexiQuote);

		// Get Pax type and counts
		getTravelInfomationSummary(travelerInfoSummaryType, flightAvailRQ);
		
		return flightAvailRQ;
	}

	/**
	 * Get the logical cabin class if available
	 * 
	 * @param cabinClass
	 * @return
	 */
	public static String getLogicalCabinClass(String cabinClass) {
		String defaultLogicalCCCode = null;
		if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
			for (CabinClassWiseDefaultLogicalCCDetailDTO defaultLogicalCCDetailDTO : CommonsServices.getGlobalConfig()
					.getDefaultLogicalCCDetails()) {
				if (cabinClass != null && cabinClass.equalsIgnoreCase(defaultLogicalCCDetailDTO.getCcCode())) {
					defaultLogicalCCCode = defaultLogicalCCDetailDTO.getDefaultLogicalCCCode();
					break;
				}
			}

		} else {
			defaultLogicalCCCode = cabinClass;
		}
		return defaultLogicalCCCode;
	}

	/**
	 * Get the cabin class details
	 * 
	 * @param cabinClassSet
	 * @param oTAAirAvailRQ
	 * @return
	 */
	private static String getCabinClass(Collection<String> cabinClassSet, OTAAirAvailRQ oTAAirAvailRQ)
			throws WebservicesException {

		Collection<String> ccSet = new HashSet<String>();
		if (!cabinClassSet.isEmpty()) { // we have user specified cabin classes
			if (cabinClassSet.size() != oTAAirAvailRQ.getOriginDestinationInformation().size()) {
				log.error("ERROR occured in createFlightAvailRQ(OTAAirAvailRQ). "
						+ "Invalid no of booking class. Booking class not included in all ONDs" + "[booking class given="
						+ cabinClassSet.size() + "] [expected = " + oTAAirAvailRQ.getOriginDestinationInformation().size() + "]");
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Cabin class missing from OND");
			} else {
				ccSet.addAll(cabinClassSet);
			}
		}

		if (!ccSet.isEmpty() && ccSet.size() > 1) { // we only support booking class per one journey
			log.error("ERROR occured in createFlightAvailRQ(OTAAirAvailRQ). "
					+ "Invalid no of booking class. Booking class shoud be same for all ONDs" + "[booking class given="
					+ cabinClassSet.size() + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_TOO_MANY_CLASSES_,
					"Too many cabin classes specifyed .Only one is supported per availability search");
		}
		String cabinClass = BeanUtils.getFirstElement(ccSet);
		// defaulting to defaultCOS if no CC is specified.
		if (cabinClass == null) {
			if (AppSysParamsUtil.getDefaultCOS() != null && !AppSysParamsUtil.getDefaultCOS().isEmpty()) {
				cabinClass = AppSysParamsUtil.getDefaultCOS();
			} else {
				cabinClass = "Y";
			}
		}

		return cabinClass;
	}
	
	private static String getCabinClassMultiCity(String cabinClass){
		
		if (cabinClass == null || cabinClass.isEmpty()) {
			if (AppSysParamsUtil.getDefaultCOS() != null && !AppSysParamsUtil.getDefaultCOS().isEmpty()) {
				cabinClass = AppSysParamsUtil.getDefaultCOS();
			} else {
				cabinClass = "Y";
			}
		}

		return cabinClass;
	}

	/**
	 * Get Travel information summary details (Pax type and count details)
	 * 
	 * @param infoSummaryType
	 * @return
	 */
	private static TravelerInfoSummaryTO getTravelInfomationSummary(TravelerInfoSummaryType travelerInfoSummaryType,
			FlightAvailRQ flightAvailRQ) throws WebservicesException {
		int adults = 0;
		int children = 0;
		int infants = 0;

		for (TravelerInformationType travelerInformationType : travelerInfoSummaryType.getAirTravelerAvail()) {
			for (PassengerTypeQuantityType passengerTypeQuantityType : travelerInformationType.getPassengerTypeQuantity()) {
				if (passengerTypeQuantityType.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
					adults += passengerTypeQuantityType.getQuantity();
				} else if (passengerTypeQuantityType.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
					children += passengerTypeQuantityType.getQuantity();
				} else if (passengerTypeQuantityType.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
					infants += passengerTypeQuantityType.getQuantity();
				}
			}
		}

		if (adults == 0 && children == 0) {
			log.error("ERROR occured in createFlightAvailRQ(OTAAirAvailRQ). " + "Invalid adults/children pax count. "
					+ "[adults pax=" + adults + "]" + "[children pax=" + children + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AT_LEAST_ONE_ADULT_MUST_BE_INCLUDED_,
					"Adult pax count cannot be zero");
		}
		
		if (adults > AppSysParamsUtil.getMaxChildAdultCount() ) {
			log.error("ERROR occured in createFlightAvailRQ(OTAAirAvailRQ). " + "Maximum adult count exceeded "
					+ "[adults pax=" + adults + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_NUMBER_OF_ADULTS,
					"Maximum adult count exceeded");
		}

		if (infants > adults) {
			log.error("ERROR occured in extractAvailabilitySearchReqParams(OTAAirAvailRQ). "
					+ "Infants cannot be more than adults. " + "[adults pax=" + adults + ", infant pax =" + infants + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_NUMBER_OF_ADULTS,
					"Infants cannot be more than adults");
		}

		TravelerInfoSummaryTO traverlerInfo = flightAvailRQ.getTravelerInfoSummary();

		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
		adultsQuantity.setQuantity(adults);

		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);
		childQuantity.setQuantity(children);

		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);
		infantQuantity.setQuantity(infants);

		return traverlerInfo;

	}

	/**
	 * 
	 * @param otaAirAvailRQ
	 * @param otaAirAvailRS
	 * @param flightAvailRQ
	 * @param flightAvailRS
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	public void createFlightAvailRS(OTAAirAvailRQ otaAirAvailRQ, OTAAirAvailRS otaAirAvailRS, FlightAvailRQ flightAvailRQ,
			FlightAvailRS flightAvailRS) throws WebservicesException, ModuleException {

		boolean flightsFound = false;
		
		boolean selectedIBFlightsExists = false;
		boolean selectedOBFlightsExists = false;
		otaAirAvailRS.setVersion(WebservicesConstants.OTAConstants.VERSION_AirAvail);
		otaAirAvailRS.setTransactionIdentifier(otaAirAvailRQ.getTransactionIdentifier());
		otaAirAvailRS.setSequenceNmbr(otaAirAvailRQ.getSequenceNmbr());
		otaAirAvailRS.setEchoToken(otaAirAvailRQ.getEchoToken());
		otaAirAvailRS.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);

		// Get the available flight information

		// Get out bound and in bound flight information
		List<OriginDestinationOptionTO> outboundFlightOptList = createAvailOptionList(
				flightAvailRS.getOriginDestinationInformationList(), false, false);
		List<OriginDestinationOptionTO> inboundFlightOptList = createAvailOptionList(
				flightAvailRS.getOriginDestinationInformationList(), true, false);
		
		// Get selected inbound and outbound flight options

		List<OriginDestinationOptionTO> selectedOBFlightOptions = createAvailOptionList(
				flightAvailRS.getOriginDestinationInformationList(), false, true);
		List<OriginDestinationOptionTO> selectedIBFlightOptions = createAvailOptionList(
				flightAvailRS.getOriginDestinationInformationList(), true, true);	
		
		boolean hasFlightSearchVariance = hasFlightSearchVariance(otaAirAvailRQ); 
		
		if (!selectedOBFlightOptions.isEmpty()) {
			selectedOBFlightsExists = true;
		}
		
		if (!selectedIBFlightOptions.isEmpty()) {
			selectedIBFlightsExists = true;
		}

		boolean isInboundExists = false;
		if (inboundFlightOptList.size() > 0) {
			isInboundExists = true;
		}

		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();
		// TODO charith need to pass this to bucket filtering
		boolean isFixedFareOnly = false;
		if (CommonUtil.getOTACodeValue(IOTACodeTables.BookingChannelType_BCT_INTERLINED_AA).equals(
				Integer.toString(userPrincipal.getSalesChannel()))) {
			isFixedFareOnly = true;
		}

		for (OriginDestinationOptionTO outBoundOptionTO : outboundFlightOptList) {
			int index = 0;
			List<FlightSegmentTO> outBoundFlightSegments = new ArrayList<FlightSegmentTO>();
			outBoundFlightSegments.addAll(outBoundOptionTO.getFlightSegmentList());

			Collection<OriginDestinationOptionTO> matchingInboundFlights = new ArrayList<OriginDestinationOptionTO>();
			matchingInboundFlights = getMatchingInboundFlights(outBoundOptionTO, inboundFlightOptList);
			boolean hasMore = true;
			boolean outBoundSeletedFlightAvailable = (!selectedIBFlightsExists && matchingInboundFlights.isEmpty() && selectedOBFlightsExists) || (hasFlightSearchVariance && !outBoundFlightSegments.isEmpty());
			boolean inBoundSelectedFlightAvailable = (selectedIBFlightsExists && !matchingInboundFlights.isEmpty()) || (hasFlightSearchVariance && !matchingInboundFlights.isEmpty());
			
			if (outBoundSeletedFlightAvailable || inBoundSelectedFlightAvailable) {
				while (hasMore) {
					flightsFound = true;

					Collection<FlightSegmentTO> allFlightSegments = new ArrayList<FlightSegmentTO>();
					allFlightSegments.addAll(outBoundFlightSegments);

					OTAAirAvailRS.OriginDestinationInformation originDestinationInformation = new OTAAirAvailRS.OriginDestinationInformation();
					otaAirAvailRS.getOriginDestinationInformation().add(originDestinationInformation);

					OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions originDestinationOptions = new OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions();
					originDestinationInformation.setOriginDestinationOptions(originDestinationOptions);

					OriginDestinationOptionTO matchingInBoundFlight = BeanUtils.getElementByIndex(matchingInboundFlights, index);

					if (matchingInBoundFlight != null) {
						allFlightSegments.addAll(matchingInBoundFlight.getFlightSegmentList());
					}

					int j = 0;
					for (FlightSegmentTO flightSegmentTO : allFlightSegments) {
						String[] segmentCodeList = flightSegmentTO.getSegmentCode().split("/");

						// Set the Origin value
						OriginDestinationInformationType.OriginLocation originLocation = new OriginDestinationInformationType.OriginLocation();
						originLocation.setLocationCode(segmentCodeList[0]);
						originLocation.setValue((String) globalConfig.getAirportInfo(originLocation.getLocationCode(), true)[0]);

						// Set the destination value
						OriginDestinationInformationType.DestinationLocation destinationLocation = new OriginDestinationInformationType.DestinationLocation();
						destinationLocation.setLocationCode(segmentCodeList[segmentCodeList.length - 1]);
						destinationLocation.setValue((String) globalConfig.getAirportInfo(destinationLocation.getLocationCode(),
								true)[0]);

						TimeInstantType departureDateTime = new TimeInstantType();
						departureDateTime.setValue(CommonUtil.getFormattedDate(flightSegmentTO.getDepartureDateTime()));

						TimeInstantType arrivalDateTime = new TimeInstantType();
						arrivalDateTime.setValue(CommonUtil.getFormattedDate(flightSegmentTO.getArrivalDateTime()));

						// If the segment is the first one, set the departure time and origin location
						if (j == 0) {
							originDestinationInformation.setOriginLocation(originLocation);
							originDestinationInformation.setDepartureDateTime(departureDateTime);
						}

						if (j == allFlightSegments.size() - 1) {
							// If the segment is the last one, set the arrival time and destination location
							originDestinationInformation.setDestinationLocation(destinationLocation);
							originDestinationInformation.setArrivalDateTime(arrivalDateTime);
						}

						OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption originDestinationOption = new OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption();
						originDestinationOptions.getOriginDestinationOption().add(originDestinationOption);

						OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment otaFlightSegment = new OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment();
						originDestinationOption.getFlightSegment().add(otaFlightSegment);

						otaFlightSegment.setRPH(flightSegmentTO.getFlightRefNumber());

						// if the segment list array has more than 2, that means it has atleast one stop
						if (segmentCodeList.length > 2) {
							otaFlightSegment.setStopQuantity(BigInteger.valueOf(segmentCodeList.length - 2));
						}

						// Set departure time in XML Gregorian Calendar
						GregorianCalendar departureDateTimeCal = new GregorianCalendar();
						departureDateTimeCal.setTime(flightSegmentTO.getDepartureDateTime());
						otaFlightSegment.setDepartureDateTime(CommonUtil.getXMLGregorianCalendar(departureDateTimeCal));

						// Set Arrival time in XML Gregorian Calendar
						GregorianCalendar arrivalDateTimeCal = new GregorianCalendar();
						arrivalDateTimeCal.setTime(flightSegmentTO.getArrivalDateTime());
						otaFlightSegment.setArrivalDateTime(CommonUtil.getXMLGregorianCalendar(arrivalDateTimeCal));

						// Set the journey duration, use Zulu timings for correct calculations
						otaFlightSegment.setJourneyDuration(CommonUtil.getDuration(flightSegmentTO.getArrivalDateTimeZulu()
								.getTime() - flightSegmentTO.getDepartureDateTimeZulu().getTime()));

						// Flight Number
						otaFlightSegment.setFlightNumber(flightSegmentTO.getFlightNumber());

						// Set departure airport and terminal
						FlightSegmentBaseType.DepartureAirport departureAirport = new FlightSegmentBaseType.DepartureAirport();
						departureAirport.setLocationCode(segmentCodeList[0]);
						departureAirport.setTerminal((String) globalConfig.getAirportInfo(departureAirport.getLocationCode(),
								true)[2]);

						// set arrival airport and terminal
						FlightSegmentBaseType.ArrivalAirport arrivalAirport = new FlightSegmentBaseType.ArrivalAirport();
						arrivalAirport.setLocationCode(segmentCodeList[segmentCodeList.length - 1]);
						arrivalAirport
								.setTerminal((String) globalConfig.getAirportInfo(arrivalAirport.getLocationCode(), true)[2]);

						otaFlightSegment.setDepartureAirport(departureAirport);
						otaFlightSegment.setArrivalAirport(arrivalAirport);

						j++;
					}

					if (matchingInboundFlights != null && index < matchingInboundFlights.size() - 1) {
						index++;
						hasMore = true;
					} else {
						hasMore = false;
					}
				}

			}

		}

		if (flightsFound) {
			// one or more matching flight(s) found
			otaAirAvailRS.setSuccess(new SuccessType());
		} else {
			addOndLog(flightAvailRQ, ThreadLocalData.getCurrentUserPrincipal());
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_NO_AVAILABILITY, "No matching flight found");
		}

		// Selected flights
		PriceInfoTO priceInfoTO = flightAvailRS.getSelectedPriceFlightInfo();
		if (priceInfoTO != null && priceInfoTO.getFareTypeTO() != null) {

			boolean hasFare = false;
			for (int i = 0; i < priceInfoTO.getFareTypeTO().getOndWiseFareType().size(); i++) {
				if (priceInfoTO.getFareTypeTO().getOndWiseFareType().get(i) != FareTypes.NO_FARE) {
					hasFare = true;
				}
			}
			if (hasFare) {
				AAAirAvailRSExt aaOTAAirAvailRSExtensions = new AAAirAvailRSExt();
				PricedItinerariesType pricedItineraries = new PricedItinerariesType();
				aaOTAAirAvailRSExtensions.setPricedItineraries(pricedItineraries);
				PricedItineraryType pricedItinerary = new PricedItineraryType();
				pricedItineraries.getPricedItinerary().add(pricedItinerary);
				pricedItinerary.setSequenceNumber(new BigInteger("1"));
				AirItineraryType airItinerary = new AirItineraryType();
				pricedItinerary.setAirItinerary(airItinerary);

				AirItineraryType.OriginDestinationOptions originDestinationOptions = new AirItineraryType.OriginDestinationOptions();
				airItinerary.setOriginDestinationOptions(originDestinationOptions);
				
				ServiceTaxUtil.calculateServiceTax(flightAvailRQ, getQuotedSegements(flightAvailRS), priceInfoTO);
				
				QuotedPriceInfoTO quotedPriceInfoTO = new QuotedPriceInfoTO();
				quotedPriceInfoTO.setBookingType(flightAvailRQ.getTravelPreferences().getBookingType());

				String[] stages = { ReservationInternalConstants.AirportMessageStages.SEARCH_RESULT };
				List<FreeTextType> outboundAirportMessages = AirportMessageInterlineUtil.retrieveAirportMessages(
						selectedOBFlightOptions, stages);
				List<FreeTextType> inboundAirportMessages = AirportMessageInterlineUtil.retrieveAirportMessages(
						selectedIBFlightOptions, stages);

				if (outboundAirportMessages != null && !outboundAirportMessages.isEmpty()) {
					pricedItinerary.getNotes().addAll(outboundAirportMessages);
				}

				if (inboundAirportMessages != null && !inboundAirportMessages.isEmpty()) {
					pricedItinerary.getNotes().addAll(inboundAirportMessages);
				}
					
				PricedItineraryInterlineUtil.preparePricedItinerary(pricedItinerary, selectedOBFlightOptions,
						selectedIBFlightOptions, flightAvailRQ, true, new FlexiFareSelectionCriteria(), flightAvailRS,
						quotedPriceInfoTO, false);

				// store the total price in the transaction
				PriceQuoteInterlineUtil.storeTotalPrice(pricedItinerary);

				quotedPriceInfoTO.setQuotedSegments(getQuotedSegements(flightAvailRS));

				// set the quoted information to the transaction
				ThreadLocalData.setCurrentTnxParam(Transaction.QUOTED_PRICE_INFO, quotedPriceInfoTO);
				ThreadLocalData.setCurrentTnxParam(Transaction.PRICE_INFO, priceInfoTO);

				FareSegChargeTO fareSegChargeTO = priceInfoTO.getFareSegChargeTO();

				ThreadLocalData.setCurrentTnxParam(Transaction.FARE_SEGMENT_CHARGE_TO, fareSegChargeTO);

				// store available OnD bundled fares in the transaction
				AvailabilitySearchUtil.setAvailableOndBundledFares(pricedItinerary);

				otaAirAvailRS.setAAAirAvailRSExt(aaOTAAirAvailRSExtensions);
			} else {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_NO_AVAILABILITY, "No matching flight found");
			}
		} else if (!AppSysParamsUtil.isRequoteEnabled()) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_NO_AVAILABILITY, "No matching flight found");
		}

		if (log.isDebugEnabled()) {
			log.debug(getAvailabilityResultsSummary(otaAirAvailRS).toString());
		}

	}
	
	/**
	 * 
	 * @param otaAirAvailRQ
	 * @param otaAirAvailRS
	 * @param flightAvailRQ
	 * @param flightAvailRS
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	public void createMulticityFlightAvailRS(OTAAirAvailRQ otaAirAvailRQ, OTAAirAvailRS otaAirAvailRS, FlightAvailRQ flightAvailRQ,
			FlightAvailRS flightAvailRS) throws WebservicesException, ModuleException {
		
		boolean flightsFound = false;
		otaAirAvailRS.setVersion(WebservicesConstants.OTAConstants.VERSION_AirAvail);
		otaAirAvailRS.setTransactionIdentifier(otaAirAvailRQ.getTransactionIdentifier());
		otaAirAvailRS.setSequenceNmbr(otaAirAvailRQ.getSequenceNmbr());
		otaAirAvailRS.setEchoToken(otaAirAvailRQ.getEchoToken());
		otaAirAvailRS.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);
		
		List<OriginDestinationOptionTO> flightOptList = createAvailOptionList(
				flightAvailRS.getOriginDestinationInformationList(), false, false);
		
		for (OriginDestinationOptionTO outBoundOptionTO : flightOptList) {

			List<FlightSegmentTO> flightSegments = new ArrayList<FlightSegmentTO>();
			flightSegments.addAll(outBoundOptionTO.getFlightSegmentList());


			OTAAirAvailRS.OriginDestinationInformation originDestinationInformation = new OTAAirAvailRS.OriginDestinationInformation();
			otaAirAvailRS.getOriginDestinationInformation().add(originDestinationInformation);

			OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions originDestinationOptions = new OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions();
			originDestinationInformation.setOriginDestinationOptions(originDestinationOptions);

			int j = 0;
			for (FlightSegmentTO flightSegmentTO : flightSegments) {
				String[] segmentCodeList = flightSegmentTO.getSegmentCode().split("/");
				flightsFound = true;
				// Set the Origin value
				OriginDestinationInformationType.OriginLocation originLocation = new OriginDestinationInformationType.OriginLocation();
				originLocation.setLocationCode(segmentCodeList[0]);
				originLocation.setValue((String) globalConfig.getAirportInfo(originLocation.getLocationCode(), true)[0]);

				// Set the destination value
				OriginDestinationInformationType.DestinationLocation destinationLocation = new OriginDestinationInformationType.DestinationLocation();
				destinationLocation.setLocationCode(segmentCodeList[segmentCodeList.length - 1]);
				destinationLocation.setValue((String) globalConfig.getAirportInfo(destinationLocation.getLocationCode(),true)[0]);

				TimeInstantType departureDateTime = new TimeInstantType();
				departureDateTime.setValue(CommonUtil.getFormattedDate(flightSegmentTO.getDepartureDateTime()));

				TimeInstantType arrivalDateTime = new TimeInstantType();
				arrivalDateTime.setValue(CommonUtil.getFormattedDate(flightSegmentTO.getArrivalDateTime()));

				// If the segment is the first one, set the departure time and origin location
				if (j == 0) {
					originDestinationInformation.setOriginLocation(originLocation);
					originDestinationInformation.setDepartureDateTime(departureDateTime);
				}

				if (j == flightSegments.size() - 1) {
					// If the segment is the last one, set the arrival time and destination location
					originDestinationInformation.setDestinationLocation(destinationLocation);
					originDestinationInformation.setArrivalDateTime(arrivalDateTime);
				}

				OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption originDestinationOption = new OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption();
				originDestinationOptions.getOriginDestinationOption().add(originDestinationOption);

				OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment otaFlightSegment = new OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment();
				originDestinationOption.getFlightSegment().add(otaFlightSegment);

				otaFlightSegment.setRPH(flightSegmentTO.getFlightRefNumber());

				// if the segment list array has more than 2, that means it has atleast one stop
				if (segmentCodeList.length > 2) {
					otaFlightSegment.setStopQuantity(BigInteger.valueOf(segmentCodeList.length - 2));
				}

				// Set departure time in XML Gregorian Calendar
				GregorianCalendar departureDateTimeCal = new GregorianCalendar();
				departureDateTimeCal.setTime(flightSegmentTO.getDepartureDateTime());
				otaFlightSegment.setDepartureDateTime(CommonUtil.getXMLGregorianCalendar(departureDateTimeCal));

				// Set Arrival time in XML Gregorian Calendar
				GregorianCalendar arrivalDateTimeCal = new GregorianCalendar();
				arrivalDateTimeCal.setTime(flightSegmentTO.getArrivalDateTime());
				otaFlightSegment.setArrivalDateTime(CommonUtil.getXMLGregorianCalendar(arrivalDateTimeCal));

				// Set the journey duration, use Zulu timings for correct calculations
				otaFlightSegment.setJourneyDuration(CommonUtil.getDuration(flightSegmentTO.getArrivalDateTimeZulu()
							.getTime() - flightSegmentTO.getDepartureDateTimeZulu().getTime()));

				// Flight Number
				otaFlightSegment.setFlightNumber(flightSegmentTO.getFlightNumber());

				// Set departure airport and terminal
				FlightSegmentBaseType.DepartureAirport departureAirport = new FlightSegmentBaseType.DepartureAirport();
				departureAirport.setLocationCode(segmentCodeList[0]);
				departureAirport.setTerminal((String) globalConfig.getAirportInfo(departureAirport.getLocationCode(),true)[2]);

				// set arrival airport and terminal
				FlightSegmentBaseType.ArrivalAirport arrivalAirport = new FlightSegmentBaseType.ArrivalAirport();
				arrivalAirport.setLocationCode(segmentCodeList[segmentCodeList.length - 1]);
				arrivalAirport.setTerminal((String) globalConfig.getAirportInfo(arrivalAirport.getLocationCode(), true)[2]);

				otaFlightSegment.setDepartureAirport(departureAirport);
				otaFlightSegment.setArrivalAirport(arrivalAirport);

				j++;
			}
					
		}
		
		if (flightsFound) {
			// one or more matching flight(s) found
			otaAirAvailRS.setSuccess(new SuccessType());
		} else {
			addOndLog(flightAvailRQ, ThreadLocalData.getCurrentUserPrincipal());
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_NO_AVAILABILITY, "No matching flight found");
		}

		if (log.isDebugEnabled()) {
			log.debug(getAvailabilityResultsSummary(otaAirAvailRS).toString());
		}
		
	}

	private List<FlightSegmentTO> getQuotedSegements(FlightAvailRS flightAvailRS) {
		List<FlightSegmentTO> flightSegments = new ArrayList<FlightSegmentTO>();

		for (OriginDestinationInformationTO originDestinationInformationTO : flightAvailRS.getOriginDestinationInformationList()) {
			for (OriginDestinationOptionTO optionTO : originDestinationInformationTO.getOrignDestinationOptions()) {	
				if(optionTO.isSelected()) {
					flightSegments.addAll(optionTO.getFlightSegmentList());
				}			
			}
		}

		return flightSegments;
	}

	/**
	 * Get the matching inbound flights for a given outbound flight
	 * 
	 * @param outBoundFlight
	 * @param inboundFlights
	 * @return
	 */
	public static Collection<OriginDestinationOptionTO> getMatchingInboundFlights(OriginDestinationOptionTO outBoundFlight,
			Collection<OriginDestinationOptionTO> inboundFlights) {

		Collection<OriginDestinationOptionTO> matchingInboundFlights = new ArrayList<OriginDestinationOptionTO>();
		Collection<FlightSegmentTO> obFlightSegments = outBoundFlight.getFlightSegmentList();

		if (obFlightSegments != null && !obFlightSegments.isEmpty()) {
			IFlightSegment lastSegment = BeanUtils.getElementByIndex(obFlightSegments, obFlightSegments.size() - 1);

			Calendar lastArrivalCal = Calendar.getInstance();
			lastArrivalCal.setTime(lastSegment.getArrivalDateTime());
			Date obArrivalDateWithTranTime = getDateWithTranTimeAdded(lastArrivalCal).getTime();

			Date inboundDepartureDate = null;
			for (OriginDestinationOptionTO inboundOption : inboundFlights) {
				Collection<FlightSegmentTO> inboundSegList = inboundOption.getFlightSegmentList();

				if (inboundSegList != null && !inboundSegList.isEmpty()) {
					IFlightSegment lastInboundFlightSeg = BeanUtils.getElementByIndex(inboundSegList, inboundSegList.size() - 1);
					inboundDepartureDate = lastInboundFlightSeg.getDepartureDateTime();
					if (inboundDepartureDate.after(obArrivalDateWithTranTime)) {
						matchingInboundFlights.add(inboundOption);
					}
				}
			}

		}
		return matchingInboundFlights;
	}

	/**
	 * Get date with Return Transition Duration added
	 * 
	 * @param date
	 * @return
	 */
	private static Calendar getDateWithTranTimeAdded(Calendar date) {
		String minRetTransitionTimeStr = WebServicesModuleUtils.getGlobalConfig().getBizParam(
				SystemParamKeys.MIN_RETURN_TRANSITION_TIME);
		String hours = minRetTransitionTimeStr.substring(0, minRetTransitionTimeStr.indexOf(":"));
		String mins = minRetTransitionTimeStr.substring(minRetTransitionTimeStr.indexOf(":") + 1);

		Calendar dateWithTranTime = new GregorianCalendar();
		dateWithTranTime.setTime(date.getTime());
		dateWithTranTime.add(Calendar.HOUR, Integer.parseInt(hours));
		dateWithTranTime.add(Calendar.MINUTE, Integer.parseInt(mins));

		return dateWithTranTime;
	}

	/**
	 * if isSelected is true, it will return only the selected flight options, if isSelected is false,all the flight
	 * including selected flight options will be returned
	 * 
	 * @param onDInfoList
	 * @param isReturn
	 * @param isSelected
	 * @return
	 * @throws ModuleException
	 */
	public static List<OriginDestinationOptionTO> createAvailOptionList(List<OriginDestinationInformationTO> onDInfoList,
			boolean isReturn, boolean isSelected) throws ModuleException {

		boolean isAll = isSelected == false ? true : false;
		List<OriginDestinationOptionTO> ondOptionList = new ArrayList<OriginDestinationOptionTO>();

		for (OriginDestinationInformationTO ondInfoTO : onDInfoList) {
			if (ondInfoTO.isReturnFlag() == isReturn) {
				for (OriginDestinationOptionTO optionTO : ondInfoTO.getOrignDestinationOptions()) {
					if (isAll) {
						ondOptionList.add(optionTO);
					} else if (isSelected == optionTO.isSelected()) {
						ondOptionList.add(optionTO);
					}
				}

			}
		}
		return ondOptionList;
	}
	
	
	
	

	public static StringBuffer getAvailabilityResultsSummary(OTAAirAvailRS otaAirAvailRS) throws WebservicesException,
			ModuleException {
		StringBuffer summary = new StringBuffer();
		String nl = "\n\r \t";
		summary.append("Number of search results found = " + otaAirAvailRS.getOriginDestinationInformation().size());
		for (OriginDestinationInformation originDestinationInformationRS : otaAirAvailRS.getOriginDestinationInformation()) {
			summary.append(nl
					+ "Flight Details ["
					+ (originDestinationInformationRS.getOriginLocation() != null ? "Origin= "
							+ originDestinationInformationRS.getOriginLocation().getLocationCode() : "")
					+ (originDestinationInformationRS.getDestinationLocation() != null ? ",Destination= "
							+ originDestinationInformationRS.getDestinationLocation().getLocationCode() : "")
					+ (originDestinationInformationRS.getDepartureDateTime() != null ? ",DepDateTime= "
							+ originDestinationInformationRS.getDepartureDateTime().getValue() : "")
					+ (originDestinationInformationRS.getArrivalDateTime() != null ? ",ArrDateTime="
							+ originDestinationInformationRS.getArrivalDateTime().getValue() : "") + "]");
			for (OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption originDestinationOption : originDestinationInformationRS
					.getOriginDestinationOptions().getOriginDestinationOption()) {
				for (OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment flightSegment : originDestinationOption
						.getFlightSegment()) {
					summary.append(nl + "Segment Info [fltNumber=" + flightSegment.getFlightNumber() + ",Origin="
							+ flightSegment.getDepartureAirport().getLocationCode() + ",Destination="
							+ flightSegment.getArrivalAirport().getLocationCode() + ",DepDateTime="
							+ CommonUtil.getFormattedDate(flightSegment.getDepartureDateTime().toGregorianCalendar().getTime())
							+ ",ArrDateTime="
							+ CommonUtil.getFormattedDate(flightSegment.getArrivalDateTime().toGregorianCalendar().getTime())
							+ "]");
				}
			}
		}
		return summary;
	}

	private static boolean isCharterAgent(UserPrincipal principal) throws ModuleException {
		String agentCode = principal.getAgentCode();
		Agent agent = WebServicesModuleUtils.getTravelAgentBD().getAgent(agentCode);

		return ((agent.getCharterAgent() != null) && (agent.CHARTER_AGENT_YES.equals(agent.getCharterAgent())));
	}

	private void addOndLog(FlightAvailRQ flightAvailRQ, UserPrincipal principal) {
		StringBuilder logs = new StringBuilder();
		logs.append("USER ID :" + principal.getUserId());

		if (flightAvailRQ.getOriginDestinationInformationList() != null) {
			int i = 1;
			for (OriginDestinationInformationTO originDestinationInformationTO : flightAvailRQ
					.getOriginDestinationInformationList()) {
				logs.append(" OND + " + i + " : " + originDestinationInformationTO.getOrigin() + "/"
						+ originDestinationInformationTO.getDestination() + " " + "Departure Date : "
						+ originDestinationInformationTO.getPreferredDate() + "  ");
				i++;
			}
		}

		if (flightAvailRQ.getTravelerInfoSummary().getPassengerTypeQuantityList() != null) {
			logs.append(" Pax Types : ");
			for (PassengerTypeQuantityTO quantityTO : flightAvailRQ.getTravelerInfoSummary().getPassengerTypeQuantityList()) {
				logs.append(" " + quantityTO.getPassengerType() + " : " + quantityTO.getQuantity() + " ");
			}
		}

		log.info(logs.toString());
	}

	public static int getAvailabilityRestrictionLevel(boolean viewFlightsWithLesserSeats, String bookingClassType) {
		if (viewFlightsWithLesserSeats || BookingClassUtil.byPassAvailableSeatsCheck(bookingClassType)) {
			return AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION;
		} else {
			return AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED;
		}
	}	
	
	private static boolean isMulticitySearch(OTAAirAvailRQ oTAAirAvailRQ){
		
		int onds = oTAAirAvailRQ.getOriginDestinationInformation().size();
		String firstONDOrigin , firstONDDestination , secondONDOrigin , secondONDDestination;
		
		if(onds > 2){
			return true;
		}else if(onds == 1){
			return false;
		}else{
			firstONDOrigin = oTAAirAvailRQ.getOriginDestinationInformation().get(0).getOriginLocation().getLocationCode();
			firstONDDestination = oTAAirAvailRQ.getOriginDestinationInformation().get(0).getDestinationLocation().getLocationCode();
			secondONDOrigin = oTAAirAvailRQ.getOriginDestinationInformation().get(1).getOriginLocation().getLocationCode();
			secondONDDestination = oTAAirAvailRQ.getOriginDestinationInformation().get(1).getDestinationLocation().getLocationCode();
			
			if(firstONDOrigin.equals(secondONDDestination) && firstONDDestination.equals(secondONDOrigin)){
				return false;
			}else{
				return true;
			}
		}
		
	}
	
	private static boolean validateOTAAirAvailRQ(OTAAirAvailRQ oTAAirAvailRQ) throws ModuleException{
		
		boolean isCityBaseSearchEnabled = AppSysParamsUtil.enableCityBasesFunctionality();
		boolean isValidRQ = true;
		String pnr;
		String pnrSegRPH;
		
		if (oTAAirAvailRQ.getModifiedSegmentInfo() != null) {
			
			String oldOriginCode = oTAAirAvailRQ.getModifiedSegmentInfo().getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption()
					.get(0).getFlightSegment().get(0).getDepartureAirport().getLocationCode();
			
			String newOriginCode = oTAAirAvailRQ.getOriginDestinationInformation().get(0).getOriginLocation().getLocationCode();
			
			if(oldOriginCode.equals(newOriginCode)){
				return true;
			}
			
			pnr = oTAAirAvailRQ.getModifiedSegmentInfo().getBookingReferenceID().get(0).getID();
			
			ThreadLocalData.setWSConextParam(WebservicesContext.TRIGGER_BALANCE_QUERY_VALIDATIONS,
					new Boolean(true));
			UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();
			
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadOndChargesView(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegViewBookingTypes(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
			pnrModesDTO.setLoadEtickets(true);
			pnrModesDTO.setLoadExternalReference(true);
			
			ModificationParamRQInfo modificationParamRQInfo = new ModificationParamRQInfo();
			modificationParamRQInfo.setAppIndicator(AppIndicatorEnum.APP_WS);
			modificationParamRQInfo.setIsRegisteredUser(false);
			
			LCCClientReservation lccClientReservation =  WebServicesModuleUtils.getAirproxyReservationBD().searchReservationByPNR(pnrModesDTO, modificationParamRQInfo,
					CommonServicesUtil.getTrackInfo(userPrincipal));
			
			String flightRefNo = oTAAirAvailRQ.getModifiedSegmentInfo().getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption()
																										.get(0).getFlightSegment().get(0).getRPH();
			
			String[] taxRegNoEnabledCountries = AppSysParamsUtil.getTaxRegistrationNumberEnabledCountries().split(",");
			
			String newOriginCountryCode = null;
			
			if (!isCityBaseSearchEnabled){
				Object[] locationInfo = globalConfig.retrieveAirportInfo(newOriginCode);
				newOriginCountryCode = (String) locationInfo[3];
			} else {
				Object[] locationInfo = globalConfig.retrieveAirportInfo(newOriginCode);
				// if locationInfo == null , then newOriginCode should be a city
				if (locationInfo != null){
					newOriginCountryCode = (String) locationInfo[3];
				} else {
					locationInfo = globalConfig.retrieveCityInfo(newOriginCode);
					newOriginCountryCode = (String) locationInfo[1];
				}
			}
			
			for(LCCClientReservationSegment seg : lccClientReservation.getSegments()){
				pnrSegRPH = FlightRefNumberUtil.composePnrSegRPHFromFlightRPH(seg.getFlightSegmentRefNumber(), seg.getPnrSegID().toString());
				if(seg.getSegmentSeq() == 1 && pnrSegRPH.equals(flightRefNo)){
					for(String countryCode : taxRegNoEnabledCountries){
						if(newOriginCountryCode.equals(countryCode)){
							isValidRQ = false;
							break;
						}
					}
				}
			}

		}
		
		return isValidRQ;
	}
	
	private boolean hasFlightSearchVariance(OTAAirAvailRQ oTAAirAvailRQ) {		
		
		boolean hasSearchVarince = false;
		
		for (OriginDestinationInformationType orgDestInfoType : oTAAirAvailRQ.getOriginDestinationInformation()) {			
		
			if (orgDestInfoType.getDepartureDateTime().getWindowBefore() != null
					|| orgDestInfoType.getDepartureDateTime().getWindowAfter() != null) {

				int windowBefore = orgDestInfoType.getDepartureDateTime().getWindowBefore() != null ? orgDestInfoType
						.getDepartureDateTime().getWindowBefore().getDays() : 0;
				int windowAfter = orgDestInfoType.getDepartureDateTime().getWindowAfter() != null ? orgDestInfoType
						.getDepartureDateTime().getWindowAfter().getDays() : 0;
				
				if (windowBefore !=0 || windowAfter != 0) {
					hasSearchVarince = true;
					break;
				}				
			}	
			
		}
		
		return hasSearchVarince;
	}
	
	private static void setMultiAirportCitySearchForOnDInfoTO(boolean isCityBaseSearchEnabled,
			OriginDestinationInformationType orgDestInfoType, OriginDestinationInformationTO depatureOnD) {
		if (isCityBaseSearchEnabled) {
			if (orgDestInfoType.getOriginLocation().isMultiAirportCityInd()) {
				depatureOnD.setDepartureCitySearch(orgDestInfoType.getOriginLocation().isMultiAirportCityInd());
			}

			if (orgDestInfoType.getDestinationLocation().isMultiAirportCityInd()) {
				depatureOnD.setArrivalCitySearch(orgDestInfoType.getDestinationLocation().isMultiAirportCityInd());
			}

		}

	}
	
	public AAOTAAirAllPriceAvailRS getAllPriceAvailability(OTAAirAvailRQ oTAAirAvailRQ) throws ModuleException,
			WebservicesException {

		// As the user is initiating a new availability search we have to clear the FareSegChargeTO from the
		// transaction.
		PriceQuoteInterlineUtil.clearFareSegmentChargeTO();
		PriceQuoteInterlineUtil.clearPriceInfo();
		PriceQuoteInterlineUtil.clearReturnPointAirport();

		validateMultiCitySearch(oTAAirAvailRQ);
		resetFlightSearchWindows(oTAAirAvailRQ);

		AAOTAAirAllPriceAvailRS aaOTAAirAllPriceAvailRS = getAvailabilityThroughInvoker(oTAAirAvailRQ);

		if (hasMoreFlightPermutations(aaOTAAirAllPriceAvailRS)) {

			List<OTAAirPriceRS> oTAAirPriceRSList = getPriceQuotes(aaOTAAirAllPriceAvailRS, oTAAirAvailRQ);
			injectPriceQuotes(oTAAirPriceRSList, aaOTAAirAllPriceAvailRS);
		}

		return aaOTAAirAllPriceAvailRS;
	}

	private AAOTAAirAllPriceAvailRS getAvailabilityThroughInvoker(OTAAirAvailRQ oTAAirAvailRQ) {

		OTAAirAvailRS oAirAvailRS = CacheTemplateInvoker.<OTAAirAvailRS, OTAAirAvailRQ> invoke(oTAAirAvailRQ,
				CacheConstant.CacheableBean.CACHE_AVAILABILITY_SEARCH_BEAN, CacheConstant.ReflectionMethodName.WARNINGS);

		AvailabilityRSAdaptor adaptor = new AvailabilityRSAdaptor();
		AAOTAAirAllPriceAvailRS aaOTAAirAllPriceAvailRS = adaptor.adapt(oAirAvailRS);

		return aaOTAAirAllPriceAvailRS;
	}

	/**
	 * Rules: 1. Existenance of at least one fare quote (Default Quote exists) 2. Existence of more than two flights
	 * 
	 * @return
	 */
	private boolean hasMoreFlightPermutations(AAOTAAirAllPriceAvailRS aaOTAAirAllPriceAvailRS) {
		return isValidDefaultQuoteExists(aaOTAAirAllPriceAvailRS)
				&& aaOTAAirAllPriceAvailRS.getOriginDestinationInformation().size() > 1;
	}

	private boolean isValidDefaultQuoteExists(AAOTAAirAllPriceAvailRS aaOTAAirAllPriceAvailRS) {
		return aaOTAAirAllPriceAvailRS != null && aaOTAAirAllPriceAvailRS.getAAAirAvailRSExt() != null
				&& !CollectionUtils.isEmpty(aaOTAAirAllPriceAvailRS.getOriginDestinationInformation())
				&& !CollectionUtils.isEmpty(aaOTAAirAllPriceAvailRS.getAAAirAvailRSExt().getPricedItineraries());
	}

	private void injectPriceQuotes(List<OTAAirPriceRS> oTAAirPriceRSList, AAOTAAirAllPriceAvailRS aaOTAAirAllPriceAvailRS) {
		if (!CollectionUtils.isEmpty(oTAAirPriceRSList)) {
			// clean the default price quote and add the avaible available price quotes
			aaOTAAirAllPriceAvailRS.getAAAirAvailRSExt().getPricedItineraries().clear();
			Iterator<OTAAirPriceRS> iterator = oTAAirPriceRSList.iterator();

			while (iterator.hasNext()) {
				OTAAirPriceRS oTAAirPriceRS = iterator.next();
				// TODO locate the default fare quote and pass to injectPriceQuote
				injectPriceQuote(aaOTAAirAllPriceAvailRS, oTAAirPriceRS, false);
			}
		}
	}

	private void validateMultiCitySearch(OTAAirAvailRQ oTAAirAvailRQ) throws WebservicesException {
		boolean isMulticitySearch = isMulticitySearch(oTAAirAvailRQ);
		if (isMulticitySearch) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_OPERATION,
					"Not Implemented.Multicity not supported");
		}
	}

	private void resetFlightSearchWindows(OTAAirAvailRQ oTAAirAvailRQ) {
		for (OriginDestinationInformationType orgDestInfoType : oTAAirAvailRQ.getOriginDestinationInformation()) {
			orgDestInfoType.getDepartureDateTime().setWindowBefore(null);
			orgDestInfoType.getDepartureDateTime().setWindowAfter(null);
		}
	}

	private List<OTAAirPriceRS> getPriceQuotes(AAOTAAirAllPriceAvailRS aaOTAAirAllPriceAvailRS, OTAAirAvailRQ oTAAirAvailRQ) {

		List<OTAAirPriceRS> OTAAirPriceRSList = new ArrayList<OTAAirPriceRS>();

		List<AAOTAAirAllPriceAvailRS.OriginDestinationInformation> flightCombinations = aaOTAAirAllPriceAvailRS
				.getOriginDestinationInformation();
		Iterator<AAOTAAirAllPriceAvailRS.OriginDestinationInformation> iterator = flightCombinations.iterator();

		while (iterator.hasNext()) {
			AAOTAAirAllPriceAvailRS.OriginDestinationInformation originDestinationCombination = iterator.next();

			OTAAirPriceRQ otaAirPriceRQ = composePriceQuoteRQ(originDestinationCombination, oTAAirAvailRQ);

			OTAAirPriceRS oTAAirPriceRS = getPriceThroughInvoker(otaAirPriceRQ);

			if (isSuccessfulPriceQuote(oTAAirPriceRS)) {
				OTAAirPriceRSList.add(oTAAirPriceRS);
			}
		}

		return OTAAirPriceRSList;

	}

	private boolean isSuccessfulPriceQuote(OTAAirPriceRS oTAAirPriceRS) {
		boolean valid = false;

		if (oTAAirPriceRS != null) {
			List results = oTAAirPriceRS.getSuccessAndWarningsAndPricedItineraries();
			if (!CollectionUtils.isEmpty(results)) {
				PricedItinerariesType sourceBaseIternary = (PricedItinerariesType) results.get(0);
				if (sourceBaseIternary != null) {
					valid = true;
					// Further we can filter for the errors
				}
			}
		}
		return valid;
	}

	private OTAAirPriceRQ composePriceQuoteRQ(AAOTAAirAllPriceAvailRS.OriginDestinationInformation originDestinationCombination,
			OTAAirAvailRQ oTAAirAvailRQ) {

		OTAAirPriceRQ oTAAirPriceRQ = new OTAAirPriceRQ();

		// POS
		oTAAirPriceRQ.setPOS(oTAAirAvailRQ.getPOS());

		// Iternary (Flight related information)
		AirItineraryType airItineraryType = new AirItineraryType();
		AirTripType directionIndicator = derivedAirTripType(oTAAirAvailRQ);
		airItineraryType.setDirectionInd(directionIndicator);

		AirItineraryType.OriginDestinationOptions targetOriginDestinationOptions = getTargetOriginDestinationOptions(originDestinationCombination);
		airItineraryType.setOriginDestinationOptions(targetOriginDestinationOptions);

		oTAAirPriceRQ.setAirItinerary(airItineraryType);

		// Traveller information
		oTAAirPriceRQ.setTravelerInfoSummary(oTAAirAvailRQ.getTravelerInfoSummary());

		return oTAAirPriceRQ;
	}

	private AirItineraryType.OriginDestinationOptions getTargetOriginDestinationOptions(
			AAOTAAirAllPriceAvailRS.OriginDestinationInformation originDestinationCombination) {
		AirItineraryType.OriginDestinationOptions targetOriginDestinationOptions = new AirItineraryType.OriginDestinationOptions();

		// setting origin destination otpions
		AAOTAAirAllPriceAvailRS.OriginDestinationInformation.OriginDestinationOptions sourceOptions = originDestinationCombination
				.getOriginDestinationOptions();
		AirIternaryOriginDestinationOptionAdaptor optionAdaptor = new AirIternaryOriginDestinationOptionAdaptor();
		AdaptorUtil.adaptCollection(sourceOptions.getOriginDestinationOption(),
				targetOriginDestinationOptions.getOriginDestinationOption(), optionAdaptor);

		// set bundle
		targetOriginDestinationOptions.getAABundledServiceExt();
		log.info("set bundle information");
		return targetOriginDestinationOptions;

	}

	private OTAAirPriceRS getPriceThroughInvoker(OTAAirPriceRQ otaAirPriceRQ) {
		OTAAirPriceRS oTAAirPriceRS = CacheTemplateInvoker.<OTAAirPriceRS, OTAAirPriceRQ> invoke(otaAirPriceRQ,
				CacheConstant.CacheableBean.CACHE_PRICE_QUOTE_BEAN,
				CacheConstant.ReflectionMethodName.SUCCESS_AND_WARNINGS_AND_PRICEDITINERARIES);
		return oTAAirPriceRS;
	}

	private void injectPriceQuote(AAOTAAirAllPriceAvailRS aaOTAAirAllPriceAvailRS, OTAAirPriceRS oTAAirPriceRS,
			boolean selectedPriceQuote) {

		if (oTAAirPriceRS != null) {
			// TODO error handling of price quote response
			List results = oTAAirPriceRS.getSuccessAndWarningsAndPricedItineraries();

			if (!CollectionUtils.isEmpty(results)) {
				PricedItinerariesType sourceBaseIternary = (PricedItinerariesType) results.get(0);
				List<PricedItineraryType> sourcePricedIternaries = sourceBaseIternary.getPricedItinerary();

				AAPricedItinerariesType targetPricedIternary = new AAPricedItinerariesType();
				targetPricedIternary.setSelectedPriceQuote(selectedPriceQuote);
				targetPricedIternary.getPricedItinerary().addAll(sourcePricedIternaries);

				List<AAPricedItinerariesType> targetPricedIternaries = aaOTAAirAllPriceAvailRS.getAAAirAvailRSExt()
						.getPricedItineraries();
				targetPricedIternaries.add(targetPricedIternary);

			}
		}

	}

	private AirTripType derivedAirTripType(OTAAirAvailRQ oTAAirAvailRQ) {

		AirTripType directionIndicator = AirTripType.ONE_WAY;

		Set<String> obAirports = new HashSet<String>();
		Set<String> inAirports = new HashSet<String>();

		for (OTAAirAvailRQ.OriginDestinationInformation originDetination : oTAAirAvailRQ.getOriginDestinationInformation()) {
			String origin = originDetination.getOriginLocation().getLocationCode();
			obAirports.add(origin);

			String destination = originDetination.getDestinationLocation().getLocationCode();
			inAirports.add(destination);
		}

		boolean isReturn = CommonUtil.validateReturnJouney(obAirports, inAirports);

		if (isReturn) {
			directionIndicator = AirTripType.RETURN;
		}

		return directionIndicator;
	}

}
