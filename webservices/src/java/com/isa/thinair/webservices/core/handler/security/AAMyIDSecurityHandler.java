package com.isa.thinair.webservices.core.handler.security;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.w3c.dom.DOMException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants.MyIDTravelConstants;
import com.isa.thinair.webservices.core.session.SessionGateway;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.isa.thinair.webservices.core.util.WebservicesContext;
import com.sun.xml.wss.ProcessingContext;
import com.sun.xml.wss.XWSSProcessor;
import com.sun.xml.wss.XWSSProcessorFactory;

/**
 * @author Janaka Padukka
 * 
 */
public class AAMyIDSecurityHandler implements SOAPHandler<SOAPMessageContext> {

	private static final Log log = LogFactory.getLog(AAMyIDSecurityHandler.class);

	private static final String DUMP_MESSAGES = "false";

	private XWSSProcessor sprocessor = null;

	private static final String namespaceURI = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
	private static final String localPart = "Security";
	private static final String prefix = "wsse";

	private static final Map securityConfigMap = WebServicesModuleUtils.getMyIDTravelConfig().getSecurityConfigMap();
	private static final boolean securityEnabled = securityConfigMap.get(MyIDTravelConstants.SECURITY_ENABLED).toString()
			.equalsIgnoreCase("true");

	public AAMyIDSecurityHandler() {
		InputStream in = null;

		try {
			XWSSProcessorFactory factory = XWSSProcessorFactory.newInstance();
			in = getStream();
			sprocessor = factory.createProcessorForSecurityConfiguration(getStream(), new AAMyIDSecurityEnvHandler());
		} catch (Exception e) {
			log.error(e);
			throw new RuntimeException(e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					log.error(e);
				}
			}
		}
	}

	public Set<QName> getHeaders() {

		QName securityHeader = new QName(namespaceURI, localPart, prefix);
		HashSet<QName> headers = new HashSet<QName>();
		headers.add(securityHeader);
		return headers;
	}

	public boolean handleFault(SOAPMessageContext messageContext) {
		logSOAPMessage(messageContext);
		return true;
	}

	public boolean handleMessage(SOAPMessageContext messageContext) {
		secureServer(messageContext);
		return true;
	}

	public void close(MessageContext messageContext) {
		try {
			SessionGateway.SessionOutHandler.handleSessionOut();
		} catch (Throwable ex) {
			log.error("Sessioin out handler failed", ex);
		}
	}

	private void secureServer(SOAPMessageContext messageContext) {
		Boolean outMessageIndicator = (Boolean) messageContext.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		SOAPMessage message = messageContext.getMessage();	

		if (outMessageIndicator.booleanValue()) {

			try {
				if (log.isDebugEnabled()) {
					log.debug("\nOutbound Plain SOAP : \n");
					logSOAPMessage(messageContext);
				}

				if (securityEnabled) {
					ProcessingContext context = sprocessor.createProcessingContext(message);
					context.setSOAPMessage(message);
					SOAPMessage securedMsg = sprocessor.secureOutboundMessage(context);
					modifySecurityHeader(message);
					messageContext.setMessage(securedMsg);
					if (log.isDebugEnabled()) {
						log.debug("\nOutbound Secured SOAP : \n");
						logSOAPMessage(messageContext);
					}
				}

			} catch (Exception ex) {
				log.error(ex);
				throw new WebServiceException(ex);
			}
			return;
		} else {

			try {

				if (securityEnabled) {
					if (log.isDebugEnabled()) {
						log.debug("\nInbound Secured SOAP : ");
						logSOAPMessage(messageContext);
					}
					ProcessingContext context = sprocessor.createProcessingContext(message);
					context.setSOAPMessage(message);
					SOAPMessage verifiedMsg = sprocessor.verifyInboundMessage(context);
					messageContext.setMessage(verifiedMsg);
				}

				if (log.isDebugEnabled()) {
					log.debug("\nInbound Plain SOAP : ");
					logSOAPMessage(messageContext);
				}

				String sessionId = null;
				HttpSession httpSession = null;
				try {
					httpSession = ((HttpServletRequest) messageContext.get(MessageContext.SERVLET_REQUEST))
							.getSession();
					if (httpSession != null) {
						sessionId = httpSession.getId();
						log.debug("HTTP sessionID=" + sessionId);
					} else {
						log.debug("HTTP sessionID is null");
					}
				} catch (Exception e) {
					log.error("HTTP session initialization failed", e);
				}

				String forwardingIP = ((HttpServletRequest) messageContext.get(MessageContext.SERVLET_REQUEST))
						.getHeader("X-Forwarded-For");

				if (forwardingIP == null || forwardingIP.trim().length() == 0) {
					forwardingIP = ((HttpServletRequest) messageContext.get(MessageContext.SERVLET_REQUEST)).getRemoteAddr();
				}
				// If there are two ip addresses, take the first one
				if (forwardingIP != null && forwardingIP.length() != 0) {
					if (forwardingIP.contains(",")) {
						String[] ips = forwardingIP.split(",");
						if (ips.length > 0) {
							forwardingIP = ips[0].trim();
						}
					}
				}

				String callingAirline = getMyIDCallingAirline(messageContext);

				String[] userCredentials = WebServicesModuleUtils.getSecurityBD().getMyIDUserForCarrier(callingAirline);

				if (userCredentials == null) {
					throw new WebservicesException(new Exception("No user id defined in AA for MyIDCarrier:"
							+ callingAirline));
				}

				// Bind the current WebservicesContext to the current thread
				ThreadLocalData.setCurrentWSContext(new WebservicesContext());

				if (forwardingIP != null)
					ThreadLocalData.setWSConextParam(WebservicesContext.WEB_REQUEST_IP, forwardingIP);

				if (httpSession != null) {
					// Propagate the web session to be send to CC Fraud check
					ThreadLocalData.setWSConextParam(WebservicesContext.WEB_SESSION_ID, sessionId);
				} else {
					log.debug("HTTP sessionID is null");
				}

				handleAuthentication(userCredentials[0], userCredentials[1],
						securityConfigMap.get(MyIDTravelConstants.AA_CHANNEL).toString());

			} catch (Exception ex) {
				log.error(ex);
				throw new WebServiceException(ex);
			}
		}
	}

	private void handleAuthentication(String user, String password, String channel) {

		try {
			Integer salesChannelId = Integer.parseInt(channel);
			// Invoke client login module
			ForceLoginInvoker.webserviceLogin(user, password, salesChannelId);
			// Initialize user session, if not already exists
			SessionGateway.SessionInHandler.handleSessionIn(user, password, salesChannelId);
			log.debug("SecurityHandler:: Successfully authenticated user [userId=" + user + "]");
		} catch (LoginException le) {
			String msg = "Authentication failed for [userId=" + user + "]";
			ThreadLocalData.setTriggerPreProcessError(
					new WebservicesException(IOTACodeTables.ErrorWarningType_EWT_AUTHENTICATION, msg));
			log.error(msg, le);
		} catch (WebservicesException we) {
			ThreadLocalData.setTriggerPreProcessError(we);
			log.error("User session creation failed [userId=" + user + "]", we);
		}
	}

	private String getMyIDCallingAirline(SOAPMessageContext context) throws SOAPException {

		SOAPBodyElement request = (SOAPBodyElement) context.getMessage().getSOAPPart().getEnvelope().getBody()
				.getChildElements().next();

		QName empData = new QName("http://bos.service.resadapter.myidtravel.lhsystems.com", "employeeData");
		Iterator iteEmployeeData = request.getChildElements(empData);
		SOAPBodyElement employeeData = (SOAPBodyElement) iteEmployeeData.next();

		QName empAirline = new QName("http://bos.service.resadapter.myidtravel.lhsystems.com", "employingAirline");
		Iterator iteEmployeeDataElements = employeeData.getChildElements(empAirline);
		SOAPBodyElement employeeAirline = (SOAPBodyElement) iteEmployeeDataElements.next();

		return employeeAirline.getTextContent();
	}

	/**
	 * XWSS 3.0 xenc:ReferenceList is a child of wsse:Security, where as Rampart
	 * expects it to be a child of EncryptedKey. Hence we are modifing XWSS 3.0
	 * generated security header to adhere Rampart requirement when sending out
	 * response.
	 * 
	 * @param message
	 * @throws DOMException
	 * @throws SOAPException
	 */
	private void modifySecurityHeader(SOAPMessage message) throws DOMException, SOAPException {

		NodeList nodeList = message.getSOAPPart().getEnvelope().getHeader()
				.getElementsByTagNameNS(namespaceURI, localPart);

		// Security
		Node referenceList = nodeList.item(0).getChildNodes().item(2);
		Node clonedReferenceList = nodeList.item(0).getChildNodes().item(2).cloneNode(true);

		Node encryptedKey = nodeList.item(0).getChildNodes().item(1);
		encryptedKey.appendChild(clonedReferenceList);

		nodeList.item(0).removeChild(referenceList);

	}

	private InputStream getStream() {


		StringBuilder securityConfigBuilder = new StringBuilder();

		securityConfigBuilder.append("<xwss:SecurityConfiguration dumpMessages=\""+ DUMP_MESSAGES+ "\" xmlns:xwss=\"http://java.sun.com/xml/ns/xwss/config\">");
		securityConfigBuilder.append("<xwss:Timestamp/>");
		securityConfigBuilder.append("<xwss:Sign includeTimestamp=\"false\">");
		securityConfigBuilder.append("<xwss:X509Token certificateAlias=\""+ securityConfigMap.get(MyIDTravelConstants.PRIVATE_KEY_ALIAS).toString()+ "\"/>");
		securityConfigBuilder.append("<xwss:CanonicalizationMethod algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#\" disableInclusivePrefix=\"true\"/>");
		securityConfigBuilder.append("<xwss:SignatureMethod algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\"/>");
		securityConfigBuilder.append("<xwss:SignatureTarget type=\"qname\" value=\"SOAP-BODY\">");
		securityConfigBuilder.append("<xwss:DigestMethod algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/>");
		securityConfigBuilder.append("<xwss:Transform algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#\" disableInclusivePrefix=\"true\"/>");
		securityConfigBuilder.append("</xwss:SignatureTarget>");
		securityConfigBuilder.append("<xwss:SignatureTarget type=\"qname\" value=\"{http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd}Timestamp\">");
		securityConfigBuilder.append("<xwss:DigestMethod algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/>");
		securityConfigBuilder.append("<xwss:Transform algorithm=\"http://www.w3.org/2001/10/xml-exc-c14n#\" disableInclusivePrefix=\"true\"/>");
		securityConfigBuilder.append("</xwss:SignatureTarget>");
		securityConfigBuilder.append("</xwss:Sign>");
		securityConfigBuilder.append("<xwss:Encrypt>");
		securityConfigBuilder.append("<xwss:X509Token encodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\" ");
		securityConfigBuilder.append("valueType=\" http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509SubjectKeyIdentifier\" ");
		securityConfigBuilder.append("certificateAlias=\""+ securityConfigMap.get(MyIDTravelConstants.PUBLIC_CERT_ALIAS).toString()+ "\" keyReferenceType=\"Identifier\"/>");
		securityConfigBuilder.append("<xwss:KeyEncryptionMethod algorithm=\"http://www.w3.org/2001/04/xmlenc#rsa-1_5\"/>");
		securityConfigBuilder.append("<xwss:DataEncryptionMethod algorithm=\"http://www.w3.org/2001/04/xmlenc#aes128-cbc\"/>");
		securityConfigBuilder.append("<xwss:EncryptionTarget type=\"qname\" value=\"SOAP-BODY\"/>");
		securityConfigBuilder.append("</xwss:Encrypt>");
		securityConfigBuilder.append("<xwss:RequireEncryption/>");
		securityConfigBuilder.append("<xwss:RequireSignature/>");
		securityConfigBuilder.append("</xwss:SecurityConfiguration>");

		InputStream is = null;
		try {
			is = new ByteArrayInputStream(securityConfigBuilder.toString().getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			log.error(e);
		}

		return is;
	}

	private void logSOAPMessage(SOAPMessageContext mtcx) {
		SOAPMessage message = mtcx.getMessage();
		StringOutputStream outputStream = new StringOutputStream();
		try {
			message.writeTo(outputStream);
			if (log.isDebugEnabled()) {
				log.debug("\n" + outputStream.toString() + "\n");
			}
		} catch (Exception e) {
			log.error("Exception in handler: " + e);
		}
	}

	class StringOutputStream extends OutputStream {

		private StringBuilder string = new StringBuilder();

		@Override
		public void write(int b) {
			this.string.append((char) b);
		}

		public String toString() {
			return this.string.toString();
		}
	}
}
