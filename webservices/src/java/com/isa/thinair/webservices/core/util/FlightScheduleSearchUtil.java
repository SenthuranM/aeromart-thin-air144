/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.core.util;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.FlightSegmentBaseType.ArrivalAirport;
import org.opentravel.ota._2003._05.FlightSegmentBaseType.DepartureAirport;
import org.opentravel.ota._2003._05.FreeTextType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirScheduleRQ;
import org.opentravel.ota._2003._05.OTAAirScheduleRS;
import org.opentravel.ota._2003._05.OTAAirScheduleRS.OriginDestinationOptions;
import org.opentravel.ota._2003._05.OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption;
import org.opentravel.ota._2003._05.OTAAirScheduleRS.OriginDestinationOptions.OriginDestinationOption.FlightSegment;
import org.opentravel.ota._2003._05.OperationScheduleType;
import org.opentravel.ota._2003._05.OperationSchedulesType;
import org.opentravel.ota._2003._05.SuccessType;
import org.opentravel.ota._2003._05.WarningsType;

import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.utils.ModelNamesAndFieldNames;
import com.isa.thinair.airschedules.api.utils.ScheduleBuildStatusEnum;
import com.isa.thinair.airschedules.api.utils.ScheduleStatusEnum;
import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;

/**
 * Manipulates OTAAirScheduleRS/RQ
 * 
 * @author Byorn de Silva
 */
public class FlightScheduleSearchUtil extends BaseUtil {

	private static final Log log = LogFactory.getLog(FlightScheduleSearchUtil.class);

	private static GlobalConfig globalConfig = WebServicesModuleUtils.getGlobalConfig();

	public FlightScheduleSearchUtil() {
		super();
	}

	/**
	 * Returns flight schedule information matching the requested criteria.
	 * 
	 * @param otaAirScheduleRQ
	 * @return
	 * @throws WebservicesException
	 */
	public static OTAAirScheduleRS searchFlightSchedule(OTAAirScheduleRQ otaAirScheduleRQ) throws WebservicesException,
			ModuleException {

		String flightNumber = null;
		if (otaAirScheduleRQ.getFlightInfo() != null) {
			otaAirScheduleRQ.getFlightInfo().getFlightNumber();
		}
		String fromAirport = otaAirScheduleRQ.getOriginDestinationInformation().getOriginLocation().getLocationCode();
		String toAirport = otaAirScheduleRQ.getOriginDestinationInformation().getDestinationLocation().getLocationCode();
		String strStartDate = otaAirScheduleRQ.getOriginDestinationInformation().getDepartureDateTime().getValue();
		String strStopDate = otaAirScheduleRQ.getOriginDestinationInformation().getDepartureDateTime().getValue();

		Date startDate = null;
		Date endDate = null;

		try {
			startDate = CommonUtil.parseDate(strStartDate).getTime();
			endDate = CommonUtil.parseDate(strStopDate).getTime();
		} catch (Exception e) {
			log.error(e);
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_DATE, "Schedule Start or End Date is invalid");
		}

		// setting the criteria list to search
		List<ModuleCriterion> criteria = new ArrayList<ModuleCriterion>();
		// setting the start date as criteria
		if (startDate != null) {
			ModuleCriterion moduleCriterionStartD = new ModuleCriterion();
			moduleCriterionStartD.setCondition(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS);
			moduleCriterionStartD.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.START_DATE); // start
																												// date
																												// of
																												// the
																												// schedule
			List<Date> valueStartDate = new ArrayList<Date>();
			valueStartDate.add(startDate);
			moduleCriterionStartD.setValue(valueStartDate);

			criteria.add(moduleCriterionStartD);
		}
		// setting the stop date as criteria
		if (endDate != null) {
			ModuleCriterion moduleCriterionStopD = new ModuleCriterion();
			moduleCriterionStopD.setCondition(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS);
			moduleCriterionStopD.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.STOP_DATE); // stop date
																											// of the
																											// schedule
			List<Date> valueStopDate = new ArrayList<Date>();
			valueStopDate.add(endDate);
			moduleCriterionStopD.setValue(valueStopDate);

			criteria.add(moduleCriterionStopD);
		}

		// loading only active schedules
		ModuleCriterion moduleCriterionStatus = new ModuleCriterion();
		moduleCriterionStatus.setCondition(ModuleCriterion.CONDITION_EQUALS);
		moduleCriterionStatus.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.STATUS_CODE); // status
																											// code of
																											// the
																											// schedule
		List<String> valueStatus = new ArrayList<String>();
		valueStatus.add(ScheduleStatusEnum.ACTIVE.getCode());
		moduleCriterionStatus.setValue(valueStatus);
		criteria.add(moduleCriterionStatus);

		// loading only already built schedules
		ModuleCriterion moduleCriterionBuildStatus = new ModuleCriterion();
		moduleCriterionBuildStatus.setCondition(ModuleCriterion.CONDITION_EQUALS);
		moduleCriterionBuildStatus.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.BUILD_STATUS_CODE); // build
																														// status
																														// of
																														// the
																														// schedule
		List<String> valueBuildStatus = new ArrayList<String>();
		valueBuildStatus.add(ScheduleBuildStatusEnum.BUILT.getCode());
		moduleCriterionBuildStatus.setValue(valueBuildStatus);
		criteria.add(moduleCriterionBuildStatus);

		// setting the flight number as criteria
		if (flightNumber != null) {
			ModuleCriterion moduleCriterionFlightNo = new ModuleCriterion();
			moduleCriterionFlightNo.setCondition(ModuleCriterion.CONDITION_EQUALS);
			moduleCriterionFlightNo.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.FLIGHT_NUMBER); // flight
																													// number
																													// of
																													// the
																													// schedule
			List<String> valueFlightNo = new ArrayList<String>();
			valueFlightNo.add(flightNumber.toUpperCase());
			moduleCriterionFlightNo.setValue(valueFlightNo);

			criteria.add(moduleCriterionFlightNo);
		}
		// setting the departure airport code as criteria
		if (fromAirport != null) {
			ModuleCriterion moduleCriterionFromApt = new ModuleCriterion();
			moduleCriterionFromApt.setCondition(ModuleCriterion.CONDITION_EQUALS);
			moduleCriterionFromApt.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.DEPARTURE_APT_CODE);// departure
																														// airport
																														// code
																														// of
																														// the
																														// schedule
			List<String> valueFromApt = new ArrayList<String>();
			valueFromApt.add(fromAirport);
			moduleCriterionFromApt.setValue(valueFromApt);

			criteria.add(moduleCriterionFromApt);
		}
		// setting the arrival airport code as criteria
		if (toAirport != null) {
			ModuleCriterion moduleCriterionToApt = new ModuleCriterion();
			moduleCriterionToApt.setCondition(ModuleCriterion.CONDITION_EQUALS);
			moduleCriterionToApt.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.ARRIVAL_APT_CODE);// arrival
																													// airport
																													// code
																													// of
																													// the
																													// schedule
			List<String> valueToApt = new ArrayList<String>();
			valueToApt.add(toAirport);
			moduleCriterionToApt.setValue(valueToApt);

			criteria.add(moduleCriterionToApt);
		}

		Page p = null;
		try {
			p = WebServicesModuleUtils.getScheduleBD().searchFlightSchedulesForDisplay(criteria, 0, 20, false);
		} catch (ModuleException e) {
			log.error(e);
			throw new WebservicesException(e);
		}

		return prepareResponse(p, otaAirScheduleRQ);
	}

	/**
	 * Extract the Schedule Data and prepares OTAAirScheduleRS.
	 * 
	 * @param p
	 * @return
	 * @throws WebservicesException
	 */
	private static OTAAirScheduleRS prepareResponse(Page p, OTAAirScheduleRQ airScheduleRQ) throws WebservicesException,
			ModuleException {

		Collection<FlightSchedule> aaFlightSchedules = p.getPageData();
		OTAAirScheduleRS airScheduleRS = new OTAAirScheduleRS();
		airScheduleRS.setErrors(new ErrorsType());
		airScheduleRS.setWarnings(new WarningsType());

		Collection<FlightScheduleLeg> aaFlightScheduleLegs = null;
		OriginDestinationOptions originDestinationOptions = new OriginDestinationOptions();
		int flightStopQuantity = 0;
		for (FlightSchedule aaFlightSchedule : aaFlightSchedules) {
			OriginDestinationOption originDestinationOption = new OriginDestinationOption();

			aaFlightScheduleLegs = aaFlightSchedule.getFlightScheduleLegs();
			flightStopQuantity = aaFlightScheduleLegs.size() - 1;
			for (FlightScheduleLeg aaFltScheduleLeg : aaFlightScheduleLegs) {
				FlightSegment flightSegment = new FlightSegment();

				// Set the arival airport
				ArrivalAirport arrAirport = new ArrivalAirport();
				arrAirport.setLocationCode(aaFltScheduleLeg.getDestination());
				flightSegment.setArrivalAirport(arrAirport);
				arrAirport.setTerminal((String) globalConfig.getAirportInfo(arrAirport.getLocationCode(), true)[2]);

				// Set the departure airport
				DepartureAirport departureAirport = new DepartureAirport();
				departureAirport.setLocationCode(aaFltScheduleLeg.getOrigin());
				flightSegment.setDepartureAirport(departureAirport);
				departureAirport.setTerminal((String) globalConfig.getAirportInfo(departureAirport.getLocationCode(), true)[2]);

				// AARESAA-10516 - Only time will not return by  SOAP, hence we are setting dummy date with time
				// Set departure/arrival times
				flightSegment.setDepartureDateTime(CommonUtil.parse(aaFltScheduleLeg.getEstDepartureTimeLocal()));
				flightSegment.setArrivalDateTime(CommonUtil.parse(aaFltScheduleLeg.getEstArrivalTimeLocal()));
				
				// Set Journey Duration
				flightSegment.setJourneyDuration(getFlightDuration(aaFltScheduleLeg.getDuration()));

				// Set Stop quantity for the flight
				flightSegment.setStopQuantity(BigInteger.valueOf(flightStopQuantity));

				// Set the flight number
				flightSegment.setFlightNumber(aaFlightSchedule.getFlightNumber());

				// Set start/end dates
				flightSegment.setScheduleValidStartDate(CommonUtil.parse(aaFlightSchedule.getStartDate()));
				flightSegment.setScheduleValidEndDate(CommonUtil.parse(aaFlightSchedule.getStopDate()));

				// Set days of operation
				flightSegment.setDaysOfOperation(getDaysOfOperation(aaFlightSchedule.getFrequencyLocal()));
				
				FreeTextType modelText = new FreeTextType();
				modelText.setLanguage("en");
				modelText.setValue("Aircraft Model:" + aaFlightSchedule.getModelNumber());
				flightSegment.getComment().add(modelText);

				originDestinationOption.getFlightSegment().add(flightSegment);
				
			}
			originDestinationOptions.getOriginDestinationOption().add(originDestinationOption);
		}
		airScheduleRS.setOriginDestinationOptions(originDestinationOptions);
		airScheduleRS.setSuccess(new SuccessType());
		return airScheduleRS;
	}

	/**
	 * Returns WS days of operation.
	 * 
	 * @param aaScheduleFrequency
	 * @return
	 */
	private static OperationSchedulesType getDaysOfOperation(Frequency aaScheduleFrequency) throws ModuleException {
		OperationSchedulesType scheduleFrequencies = new OperationSchedulesType();
		OperationScheduleType scheduleFrequency = new OperationScheduleType();
		OperationScheduleType.OperationTimes.OperationTime wsFrequency = new OperationScheduleType.OperationTimes.OperationTime();

		Collection aaDaysOfWeek = CalendarUtil.getDaysFromFrequency(aaScheduleFrequency);

		if (aaDaysOfWeek.contains(DayOfWeek.MONDAY)) {
			wsFrequency.setMon(true);
		}
		if (aaDaysOfWeek.contains(DayOfWeek.TUESDAY)) {
			wsFrequency.setTue(true);
		}
		if (aaDaysOfWeek.contains(DayOfWeek.WEDNESDAY)) {
			wsFrequency.setWeds(true);
		}
		if (aaDaysOfWeek.contains(DayOfWeek.THURSDAY)) {
			wsFrequency.setThur(true);
		}
		if (aaDaysOfWeek.contains(DayOfWeek.FRIDAY)) {
			wsFrequency.setFri(true);
		}
		if (aaDaysOfWeek.contains(DayOfWeek.SATURDAY)) {
			wsFrequency.setSat(true);
		}
		if (aaDaysOfWeek.contains(DayOfWeek.SUNDAY)) {
			wsFrequency.setSun(true);
		}
		scheduleFrequency.setOperationTimes(new OperationScheduleType.OperationTimes());
		scheduleFrequency.getOperationTimes().getOperationTime().add(wsFrequency);

		scheduleFrequencies.getOperationSchedule().add(scheduleFrequency);

		return scheduleFrequencies;
	}

	/**
	 * Returns flight duration in days and minutes.
	 * 
	 * @param durationInMinutes
	 * @return
	 * @throws WebservicesException
	 */
	private static Duration getFlightDuration(int durationInMinutes) throws WebservicesException, ModuleException {
		try {
			return DatatypeFactory.newInstance().newDuration(true, DatatypeConstants.FIELD_UNDEFINED,
					DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED, durationInMinutes / 60,
					durationInMinutes % 60, 0);
		} catch (DatatypeConfigurationException e) {
			log.error(e);
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, e.getMessage());
		}
	}
}
