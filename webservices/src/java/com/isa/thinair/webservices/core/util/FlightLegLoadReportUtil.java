package com.isa.thinair.webservices.core.util;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.cesar.FlightLegPaxTypeCountTO;
import com.isa.thinair.airreservation.api.dto.cesar.PaxTypeCountTO;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isaaviation.thinair.webservices.api.airschedules.AAFligthtLegPaxCountInfoRQ;
import com.isaaviation.thinair.webservices.api.airschedules.AAFligthtLegPaxCountInfoRS;
import com.isaaviation.thinair.webservices.api.airschedules.AAPaxTypeCountDetailType;
import com.isaaviation.thinair.webservices.api.airschedules.AAPaxTypeCountSummaryType;

/**
 * @author Byorn
 */
public class FlightLegLoadReportUtil extends BaseUtil {

	protected static final Log log = LogFactory.getLog(FlightLegLoadReportUtil.class);

	public static AAFligthtLegPaxCountInfoRS getPaxTypesForFlightLeg(AAFligthtLegPaxCountInfoRQ flightLetPaxCountRQ) {

		AAFligthtLegPaxCountInfoRS countInfoRS = new AAFligthtLegPaxCountInfoRS();
		AAPaxTypeCountSummaryType cesarCountSummaryType = new AAPaxTypeCountSummaryType();

		try {

			Date departureDateZulu = flightLetPaxCountRQ.getDepartureZulu().toGregorianCalendar().getTime();

			Collection records = ReservationModuleUtils.getReservationAuxilliaryBD().getFlightLegPaxTypeCount(
					flightLetPaxCountRQ.getFlightNumber(), flightLetPaxCountRQ.getOrigin(), flightLetPaxCountRQ.getDestination(),
					departureDateZulu);
			Iterator iter = records.iterator();
			while (iter.hasNext()) {

				FlightLegPaxTypeCountTO flp = (FlightLegPaxTypeCountTO) iter.next();
				// add confirmed
				PaxTypeCountTO cnf = flp.getConfirmedPaxSummary();
				if (cnf != null) {

					AAPaxTypeCountDetailType cesarCountDetailType = new AAPaxTypeCountDetailType();
					cesarCountDetailType.setAdultFemaleCount(cnf.getAdultFemaleCount());
					cesarCountDetailType.setAdultMaleCount(cnf.getAdultMaleCount());
					cesarCountDetailType.setChildCount(cnf.getChildCount());
					cesarCountDetailType.setInfantCount(cnf.getInfantCount());
					cesarCountDetailType.setOtherCount(cnf.getOthersCount());
					cesarCountDetailType.setBaggageWeight(cnf.getBaggageWeight());
					cesarCountSummaryType.getConfirmedPax().add(cesarCountDetailType);

				}
				// add on hold
				PaxTypeCountTO ohd = flp.getOnholdPaxSummary();
				if (ohd != null) {

					AAPaxTypeCountDetailType cesarCountDetailType = new AAPaxTypeCountDetailType();
					cesarCountDetailType.setAdultFemaleCount(ohd.getAdultFemaleCount());
					cesarCountDetailType.setAdultMaleCount(ohd.getAdultMaleCount());
					cesarCountDetailType.setChildCount(ohd.getChildCount());
					cesarCountDetailType.setInfantCount(ohd.getInfantCount());
					cesarCountDetailType.setOtherCount(ohd.getOthersCount());
					cesarCountDetailType.setBaggageWeight(ohd.getBaggageWeight());
					cesarCountSummaryType.getOnholdPax().add(cesarCountDetailType);
				
				}
				cesarCountSummaryType.setFlightLegId(flp.getFlightLegId());

			}

			countInfoRS.setPaxTypeCounts(cesarCountSummaryType);

		} catch (ModuleException e) {
			log.error(e);
		} catch (Throwable e) {
			log.error(e);
		}
		return countInfoRS;

	}

}
