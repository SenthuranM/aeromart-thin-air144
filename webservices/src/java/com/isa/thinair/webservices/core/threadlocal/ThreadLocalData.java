package com.isa.thinair.webservices.core.threadlocal;

import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.transaction.ITransaction;
import com.isa.thinair.webservices.core.util.WebservicesContext;

/**
 * Holds thread local data
 * 
 * @author Nasly
 */
public class ThreadLocalData {

	/** UserPrincipal bound to current thread */
	private static ThreadLocal<UserPrincipal> threadLocalUserPrincipal = new ThreadLocal<UserPrincipal>();

	/**
	 * WebserviceContext bound to current thread Used for passing parameters across differet methods during current
	 * thread processing
	 */
	private static ThreadLocal<WebservicesContext> threadLocalWSContext = new ThreadLocal<WebservicesContext>();

	private static ThreadLocal<AAUserSession> threadLocalUserSession = new ThreadLocal<AAUserSession>();

	private static ThreadLocal<ITransaction> threadLocalTransaction = new ThreadLocal<ITransaction>();

	public static void setCurrentUserPrincipal(UserPrincipal currentUserPrincipal) {
		threadLocalUserPrincipal.set(currentUserPrincipal);
	}

	public static UserPrincipal getCurrentUserPrincipal() {
		return threadLocalUserPrincipal.get();
	}

	/**
	 * Unset current UserPrincipal NOTE: MUST make sure to call this before each service thread completes
	 */
	public static void unsetCurrentUserPrincipal() {
		threadLocalUserPrincipal.set(null);
	}

	public static void setCurrentWSContext(WebservicesContext currentWebservicesContext) {
		threadLocalWSContext.set(currentWebservicesContext);
	}

	public static WebservicesContext getCurrentWSContext() {
		return threadLocalWSContext.get();
	}

	/**
	 * Unset current WebservicesContext NOTE: MUST make sure to call this before each service thread completes
	 */
	public static void unsetCurrentWSContext() {
		threadLocalWSContext.set(null);
	}

	public static Object getWSContextParam(String key) {
		Object value = null;
		if (threadLocalWSContext.get() != null) {
			value = threadLocalWSContext.get().getParameter(key);
		}
		return value;
	}

	public static void setWSConextParam(String key, Object value) {
		if (threadLocalWSContext.get() != null) {
			threadLocalWSContext.get().setParameter(key, value);
		} else {
			// TODO handle
		}
	}

	public static Object removeWSContextParam(String key) {
		Object value = null;
		if (threadLocalWSContext.get() != null) {
			value = threadLocalWSContext.get().removeParameter(key);
		}
		return value;
	}

	public static void removeAllWSContextParams() {
		if (threadLocalWSContext.get() != null) {
			threadLocalWSContext.get().removeAllParameters();
		}
	}

	/**
	 * Sets whether or not an error is triggered during current service call, before delegating the call to the service
	 * implementation.
	 * 
	 * @param errorObject
	 */
	public static void setTriggerPreProcessError(Throwable errorObject) {
		setWSConextParam(WebservicesContext.TRIGGER_PREPROCESS_ERROR, new Boolean(true));
		setWSConextParam(WebservicesContext.PREPROCESS_ERROR_OBJECT, errorObject);
	}

	/**
	 * @return Returns any error triggered during current service call.
	 */
	public static WebservicesException getTriggeredProProcessError() {
		return (WebservicesException) getWSContextParam(WebservicesContext.PREPROCESS_ERROR_OBJECT);
	}

	/**
	 * Sets whether or not an error is triggered during current service call, after delegating the call to the service
	 * implementation.
	 * 
	 * @param errorObject
	 */
	public static void setTriggerPostProcessError(Throwable errorObject) {
		setWSConextParam(WebservicesContext.TRIGGER_POSTPROCESS_ERROR, new Boolean(true));
	}

	public static void setCurrentUserSession(AAUserSession userSession) {
		threadLocalUserSession.set(userSession);
	}

	public static AAUserSession getCurrentUserSession() {
		return threadLocalUserSession.get();
	}

	public static void unsetCurrentUserSession() {
		threadLocalUserSession.set(null);
	}

	public static <T> T getCurrentUserSessionParam(String key) {
		T value = null;
		if (threadLocalUserSession.get() != null) {
			value = (T) threadLocalUserSession.get().getParameter(key);
		}
		return value;
	}

	public static <V> void setCurrentUserSessionParam(String key, V value) {
		if (threadLocalUserSession.get() != null) {
			threadLocalUserSession.get().addParameter(key, value);
		}

	}

	public static void setCurrentTransaction(ITransaction transaction) {
		threadLocalTransaction.set(transaction);
	}

	public static ITransaction getCurrentTransaction() {
		return threadLocalTransaction.get();
	}

	public static Object getCurrentTnxParam(String key) {
		Object value = null;
		if (threadLocalTransaction.get() != null) {
			value = threadLocalTransaction.get().getParameter(key);
		}
		return value;
	}

	public static void setCurrentTnxParam(String key, Object value) {
		if (threadLocalTransaction.get() != null) {
			threadLocalTransaction.get().setParameter(key, value);
		}
	}

	/**
	 * Unset current transaction bound to the current thread
	 * 
	 * NOTE: MUST make sure to call this before each service thread completes
	 */
	public static void unsetCurrentTransaction() {
		threadLocalTransaction.set(null);
	}

}
