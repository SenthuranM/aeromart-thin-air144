/**
 * 
 */
package com.isa.thinair.webservices.core.bl.myidtravel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.OTACancelRS;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.util.ExceptionUtil;
import com.isa.thinair.webservices.core.util.MyIDTrvlCommonUtil;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterCancelBookingRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterCancelBookingResponse;

/**
 * @author Janaka Padukka
 * 
 */
public class MyIDTrvlCancelBookingBL {

	private final static Log log = LogFactory.getLog(MyIDTrvlCancelBookingBL.class);

	public static MyIDTravelResAdapterCancelBookingResponse execute(
			MyIDTravelResAdapterCancelBookingRequest myIDCanBookingReq) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN cancelBooking(MyIDTravelResAdapterCancelBookingRequest)");
		}

		MyIDTravelResAdapterCancelBookingResponse myIDCanBookingRes = null;
		try {
			myIDCanBookingRes = cancelBooking(myIDCanBookingReq);
		} catch (Exception ex) {
			log.error("cancelBooking(MyIDTravelResAdapterCancelBookingRequest) failed.", ex);
			if (myIDCanBookingRes == null) {
				myIDCanBookingRes = new MyIDTravelResAdapterCancelBookingResponse();
				OTACancelRS otaCancelRS = new OTACancelRS();
				otaCancelRS.setEchoToken(myIDCanBookingReq.getOTACancelRQ().getEchoToken());
				myIDCanBookingRes.setOTACancelRS(otaCancelRS);
			}
			if (myIDCanBookingRes.getOTACancelRS().getErrors() == null)
				myIDCanBookingRes.getOTACancelRS().setErrors(new ErrorsType());
			ExceptionUtil.addOTAErrrorsAndWarnings(myIDCanBookingRes.getOTACancelRS().getErrors().getError(), null, ex);
		}
		if (log.isDebugEnabled())
			log.debug("END cancelBooking(MyIDTravelResAdapterCancelBookingRequest)");
		return myIDCanBookingRes;
	}

	private static MyIDTravelResAdapterCancelBookingResponse cancelBooking(
			MyIDTravelResAdapterCancelBookingRequest myIDCanBookingReq) throws WebservicesException, ModuleException {

		MyIDTravelResAdapterCancelBookingResponse myIDCanBookingRes = new MyIDTravelResAdapterCancelBookingResponse();
		myIDCanBookingRes.setEmployeeData(myIDCanBookingReq.getEmployeeData());
		OTACancelRS otaCancelRS = MyIDTrvlCommonUtil.cancelBooking(myIDCanBookingReq.getOTACancelRQ());
		myIDCanBookingRes.setOTACancelRS(otaCancelRS);
		return myIDCanBookingRes;

	}
}
