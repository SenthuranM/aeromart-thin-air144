package com.isa.thinair.webservices.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ChargesDetailDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.core.bl.common.ReservationBO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.api.dtos.OnDChargeSummaryDTO;
import com.isaaviation.thinair.webservices.api.airreservation.AAFeeType;
import com.isaaviation.thinair.webservices.api.airreservation.AAFeesType;
import com.isaaviation.thinair.webservices.api.airreservation.AAPNRFlightSegmentType;
import com.isaaviation.thinair.webservices.api.airreservation.AAPNRFlightSegmentsType;
import com.isaaviation.thinair.webservices.api.airreservation.AAPNRPaxSegPricingType;
import com.isaaviation.thinair.webservices.api.airreservation.AAPNRPaxSegsPricingType;
import com.isaaviation.thinair.webservices.api.airreservation.AAPNRPaxTicketingDataRQ;
import com.isaaviation.thinair.webservices.api.airreservation.AAPNRPaxTicketingDataRS;
import com.isaaviation.thinair.webservices.api.airreservation.AAPNRPaxTicketingDataType;
import com.isaaviation.thinair.webservices.api.airreservation.AAPNRPaxTransactionType;
import com.isaaviation.thinair.webservices.api.airreservation.AAPNRPaxTransactionsType;
import com.isaaviation.thinair.webservices.api.airreservation.AAPNRPaxesTicketingDataType;
import com.isaaviation.thinair.webservices.api.airreservation.AATaxType;
import com.isaaviation.thinair.webservices.api.airreservation.AATaxesType;

public class AgentSalesDataUtil {

	public static AAPNRPaxTicketingDataRS getAgentSalesData(AAPNRPaxTicketingDataRQ aapnrPaxTicketingDataRQ)
			throws ModuleException {

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(aapnrPaxTicketingDataRQ.getPnr());

		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadLastUserNote(true);
		pnrModesDTO.setRecordAudit(true);

		Reservation reservation = WebServicesModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);

		AAPNRPaxTicketingDataRS res = null;
		if (reservation != null) {
			res = new AAPNRPaxTicketingDataRS();

			Collection colCashNominalCodes = ReservationTnxNominalCode.getCashTypeNominalCodes();
			Collection colVisaNominalCodes = ReservationTnxNominalCode.getVisaTypeNominalCodes();
			Collection colMasterNominalCodes = ReservationTnxNominalCode.getMasterTypeNominalCodes();
			Collection colAmexNominalCodes = ReservationTnxNominalCode.getAmexTypeNominalCodes();
			Collection colDinersNominalCodes = ReservationTnxNominalCode.getDinersTypeNominalCodes();
			Collection colGenericNominalCodes = ReservationTnxNominalCode.getGenericTypeNominalCodes();
			Collection colCMINominalCodes = ReservationTnxNominalCode.getCMITypeNominalCodes();
			Collection colOnAccountNominalCodes = ReservationTnxNominalCode.getOnAccountTypeNominalCodes();
			Collection colCreditNominalCodes = ReservationTnxNominalCode.getCreditTypeNominalCodes();

			List<AAPNRFlightSegmentType> rsSegsList = new ArrayList<AAPNRFlightSegmentType>();
			Date firstDepartureZulu = null;
			Date firstDepartureLocal = null;
			String routing = "";

			// Segments
			for (Iterator<ReservationSegmentDTO> segIt = reservation.getSegmentsView().iterator(); segIt.hasNext();) {
				ReservationSegmentDTO seg = segIt.next();

				if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(seg.getStatus())) {
					AAPNRFlightSegmentType rsSeg = new AAPNRFlightSegmentType();
					rsSegsList.add(rsSeg);

					rsSeg.setPnrFltSegRPH(seg.getPnrSegId().toString());
					rsSeg.setCarrierCode(StringUtils.substring(seg.getFlightNo(), 0, 2));
					rsSeg.setFlightNumber(seg.getFlightNo());
					rsSeg.setClassOfService("Y");
					rsSeg.setOriginAirport(StringUtils.substring(seg.getSegmentCode(), 0, 3));
					rsSeg.setDestinationAirport(StringUtils.substringAfterLast(seg.getSegmentCode(), "/"));
					rsSeg.setDepartureDatetime(CommonUtil.parse(seg.getDepartureDate()));
					rsSeg.setArrivalDatetime(CommonUtil.parse(seg.getArrivalDate()));

					if (firstDepartureZulu == null || firstDepartureZulu.after(seg.getZuluDepartureDate())) {
						firstDepartureZulu = seg.getZuluDepartureDate();
						firstDepartureLocal = seg.getDepartureDate();
					}

					// FIXME - check if segments are in the order of departure date
					if (!"".equals(routing)
							&& StringUtils.substring(seg.getSegmentCode(), 0, 3).equals(
									StringUtils.substringAfterLast(routing, "/"))) {
						routing += "/" + StringUtils.substring(seg.getSegmentCode(), 4);
					} else {
						if (!"".equals(routing)) {
							routing += "/";
						}
						routing += seg.getSegmentCode();
					}
				}
			}

			ReservationPax pax = null;
			Iterator<ReservationPax> paxIt = null;
			Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
			for (paxIt = reservation.getPassengers().iterator(); paxIt.hasNext();) {
				pax = (ReservationPax) paxIt.next();

				if (!ReservationApiUtils.isInfantType(pax)) {
					pnrPaxIds.add(pax.getPnrPaxId());
				}
			}
			Map<Integer, Collection<ReservationTnx>> mapTnxs = ReservationBO.getPNRPaxPaymentsAndRefunds(pnrPaxIds, false, true);

			res.setPnrPaxesTicketingData(new AAPNRPaxesTicketingDataType());
			for (paxIt = reservation.getPassengers().iterator(); paxIt.hasNext();) {

				pax = paxIt.next();
				if (!ReservationApiUtils.isInfantType(pax)) {
					AAPNRPaxTicketingDataType paxData = new AAPNRPaxTicketingDataType();
					res.getPnrPaxesTicketingData().getPnrPaxTicketingData().add(paxData);

					paxData.setAirlineCode(AppSysParamsUtil.getDefaultCarrierCode());
					paxData.setPnr(reservation.getPnr());
					paxData.setRouting(routing);
					paxData.setFirstTravelDate(CommonUtil.parse(firstDepartureLocal));

					AAPNRPaxTicketingDataType.Pax rsPax = new AAPNRPaxTicketingDataType.Pax();
					rsPax.setName(StringUtils.upperCase((pax.getTitle() == null ? "" : pax.getTitle() + " ") + pax.getFirstName()
							+ " " + pax.getLastName()));
					rsPax.setTypeCode(pax.getPaxType());
					rsPax.setRPH(pax.getPnrPaxId().toString());

					// Payments
					AAPNRPaxTransactionType aapnrPaxTransactionType = null;
					paxData.setPaxTransactions(new AAPNRPaxTransactionsType());
					if (mapTnxs != null && mapTnxs.containsKey(pax.getPnrPaxId())) {
						Collection<ReservationTnx> txns = mapTnxs.get(pax.getPnrPaxId());

						for (ReservationTnx txn : txns) {
							aapnrPaxTransactionType = new AAPNRPaxTransactionType();
							paxData.getPaxTransactions().getPaxTransaction().add(aapnrPaxTransactionType);

							aapnrPaxTransactionType.setTxnRPH(txn.getTnxId().toString());
							aapnrPaxTransactionType.setCollectedUserId(txn.getUserId());
							aapnrPaxTransactionType.setDate(CommonUtil.parse(txn.getDateTime()));

							AAPNRPaxTransactionType.AmountInBaseCurrency payAmountInBase = new AAPNRPaxTransactionType.AmountInBaseCurrency();
							aapnrPaxTransactionType.setAmountInBaseCurrency(payAmountInBase);
							OTAUtils.setAmountAndDefaultCurrency(payAmountInBase, txn.getAmount().negate());

							AAPNRPaxTransactionType.AmountInPayCurrency payAmountInPayCur = new AAPNRPaxTransactionType.AmountInPayCurrency();
							aapnrPaxTransactionType.setAmountInPayCurrency(payAmountInPayCur);
							if (txn.getReservationPaxTnxBreakdown() != null) {
								OTAUtils.setAmountAndCurrency(payAmountInPayCur, txn.getReservationPaxTnxBreakdown()
										.getTotalPriceInPayCurrency().negate(), txn.getReservationPaxTnxBreakdown()
										.getPaymentCurrencyCode());
							} else {
								OTAUtils.setAmountAndDefaultCurrency(payAmountInPayCur, txn.getAmount().negate());
							}

							aapnrPaxTransactionType.setAgencyCode(txn.getAgentCode());
							if (colCashNominalCodes.contains(txn.getNominalCode())) {
								aapnrPaxTransactionType.setPaymentTypeCode("CA");
							} else if (colOnAccountNominalCodes.contains(txn.getNominalCode())) {
								aapnrPaxTransactionType.setPaymentTypeCode("OA");
							} else {
								aapnrPaxTransactionType.setPaymentTypeCode("CC");
							}
						}
					}

					// Segments
					paxData.setPnrFlightSegments(new AAPNRFlightSegmentsType());
					paxData.getPnrFlightSegments().getPnrFlightSegment().addAll(rsSegsList);

					// Charges
					Collection<ChargesDetailDTO> chargesDetailDTOs = pax.getOndChargesView();
					Map<String, OnDChargeSummaryDTO> ondChargeSummaryMap = new HashMap<String, OnDChargeSummaryDTO>();

					OnDChargeSummaryDTO ondChargeSummaryDTO = null;
					for (ChargesDetailDTO chargesDetailDTO : chargesDetailDTOs) {
						if (ondChargeSummaryMap.containsKey(chargesDetailDTO.getOndCode())) {
							ondChargeSummaryDTO = ondChargeSummaryMap.get(chargesDetailDTO.getOndCode());
						} else {
							ondChargeSummaryDTO = new OnDChargeSummaryDTO();
							ondChargeSummaryDTO.setOndCode(chargesDetailDTO.getOndCode());
							ondChargeSummaryMap.put(chargesDetailDTO.getOndCode(), ondChargeSummaryDTO);
						}

						if ("FAR".equals(chargesDetailDTO.getChargeGroupCode())) {
							if (ondChargeSummaryDTO.getFareAmount() == null) {
								ondChargeSummaryDTO.setFareAmount(chargesDetailDTO.getChargeAmount());
							} else {
								ondChargeSummaryDTO.setFareAmount(AccelAeroCalculator.add(ondChargeSummaryDTO.getFareAmount(),
										chargesDetailDTO.getChargeAmount()));
							}
						} else if ("TAX".equals(chargesDetailDTO.getChargeGroupCode())) {
							if (ondChargeSummaryDTO.getTaxes() == null
									|| !ondChargeSummaryDTO.getTaxes().containsKey(chargesDetailDTO.getChargeCode())) {
								ondChargeSummaryDTO.addTax(chargesDetailDTO.getChargeCode(), chargesDetailDTO.getChargeAmount());
							} else {
								ondChargeSummaryDTO.addTax(chargesDetailDTO.getChargeCode(), AccelAeroCalculator.add(
										ondChargeSummaryDTO.getTaxes().get(chargesDetailDTO.getChargeCode()),
										chargesDetailDTO.getChargeAmount()));
							}
						} else { // surcharges
							if (ondChargeSummaryDTO.getSurcharges() == null
									|| !ondChargeSummaryDTO.getSurcharges().containsKey(chargesDetailDTO.getChargeCode())) {
								ondChargeSummaryDTO.addSurcharge(chargesDetailDTO.getChargeCode(),
										chargesDetailDTO.getChargeAmount());
							} else {
								ondChargeSummaryDTO.addSurcharge(chargesDetailDTO.getChargeCode(), AccelAeroCalculator.add(
										ondChargeSummaryDTO.getSurcharges().get(chargesDetailDTO.getChargeCode()),
										chargesDetailDTO.getChargeAmount()));
							}
						}
					}

					if (ondChargeSummaryMap != null) {
						paxData.setPaxSegsPricing(new AAPNRPaxSegsPricingType());
						for (String ondCode : ondChargeSummaryMap.keySet()) {
							AAPNRPaxSegPricingType aapnrPaxSegPricingType = new AAPNRPaxSegPricingType();
							paxData.getPaxSegsPricing().getPaxSegPricing().add(aapnrPaxSegPricingType);

							aapnrPaxSegPricingType.setSegmentCode(ondCode);
							ondChargeSummaryDTO = ondChargeSummaryMap.get(ondCode);

							AAPNRPaxSegPricingType.Fare aaAmountType = new AAPNRPaxSegPricingType.Fare();
							aapnrPaxSegPricingType.setFare(aaAmountType);
							OTAUtils.setAmountAndDefaultCurrency(aaAmountType, ondChargeSummaryDTO.getFareAmount());

							if (ondChargeSummaryDTO.getTaxes() != null) {
								aapnrPaxSegPricingType.setTaxes(new AATaxesType());
								for (String chargeCode : ondChargeSummaryDTO.getTaxes().keySet()) {
									AATaxType aaTaxType = new AATaxType();
									aapnrPaxSegPricingType.getTaxes().getTax().add(aaTaxType);

									aaTaxType.setTaxCode(chargeCode);
									OTAUtils.setAmountAndDefaultCurrency(aaTaxType, ondChargeSummaryDTO.getTaxes()
											.get(chargeCode));
								}
							}

							if (ondChargeSummaryDTO.getSurcharges() != null) {
								aapnrPaxSegPricingType.setFees(new AAFeesType());
								for (String chargeCode : ondChargeSummaryDTO.getSurcharges().keySet()) {
									AAFeeType aaFeeType = new AAFeeType();
									aapnrPaxSegPricingType.getFees().getFee().add(aaFeeType);

									aaFeeType.setFeeCode(chargeCode);
									OTAUtils.setAmountAndDefaultCurrency(aaFeeType,
											ondChargeSummaryDTO.getSurcharges().get(chargeCode));
								}
							}
						}
					}
				}
			}
		}
		return res;
	}
}
