/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005-08 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * ===============================================================================
 */
package com.isa.thinair.webservices.core.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * For propagating data across different method calls during a service execution.
 * 
 * @author Mohamed Nasly
 */
public class WebservicesContext {

	/** Context param keys */
	public static final String TX_START_TIMESTAMP = "txStartTimestamp";
	public static final String BALANCE_QUERY_KEY = "balQKey";
	public static final String TRIGGER_BALANCE_QUERY_VALIDATIONS = "trigBalQValidations";
	public static final String TRIGGER_IGNORE_DEPARTURE_DATE_VALIDATEION = "trigIngDeptDateValidation";
	public static final String REQUEST_POS = "pos";
	public static final String TRIGGER_PREPROCESS_ERROR = "trigPreErr";
	public static final String PREPROCESS_ERROR_OBJECT = "preErrObj";
	public static final String TRIGGER_POSTPROCESS_ERROR = "trigPostErr";
	public static final String TRIGGER_NEW_TXN_CREATED = "trigNewTxnCreated";
	public static final String LOGIN_NAME = "loginName";
	public static final String LOGIN_PASS = "loginPass";
	public static final String SALES_CHANNEL_ID = "salesChannelId";
	public static final String WEB_SESSION_ID = "webSessionId";
	public static final String WEB_REQUEST_IP = "requestIp";
	public static final String TOTAL_BALANCE_AMT = "balAmt";
	public static final String TOTAL_BALANCE_AMT_BASE = "baseAmt";
	public static final String CLIENT_IDENTIFIER = "clientIdentifier";
	public static final String EXTERNAL_REQ_ID = "reqUID";

	private Map<String, Object> wsContexParams = new HashMap<String, Object>();

	/**
	 * Add parameter to the WebserviceContext
	 * 
	 * @param key
	 * @param value
	 */
	public void setParameter(String key, Object value) {
		wsContexParams.put(key, value);
	}

	/**
	 * Get parameter from the WebserviceContext
	 * 
	 * @param key
	 * @return
	 */
	public Object getParameter(String key) {
		return wsContexParams.get(key);
	}

	/**
	 * Remove parameter from the WebserviceContext
	 * 
	 * @param key
	 * @return Parameter being removed
	 */
	public Object removeParameter(String key) {
		return wsContexParams.remove(key);
	}

	/**
	 * Removes all the parameters from the Webservices context.
	 */
	public void removeAllParameters() {
		wsContexParams.clear();
	}

	/**
	 * Get all parameter keys from the WebservicesContext
	 * 
	 * @return
	 */
	public Iterator<String> getAllParamsKeysIterator() {
		return wsContexParams.keySet().iterator();
	}
}
