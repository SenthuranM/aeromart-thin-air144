package com.isa.thinair.webservices.core.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.Duration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.CompanyNameType;
import org.opentravel.ota._2003._05.EquipmentType;
import org.opentravel.ota._2003._05.OTAAirFlifoRQ;
import org.opentravel.ota._2003._05.OTAAirFlifoRS;
import org.opentravel.ota._2003._05.OTAAirFlifoRS.FlightInfoDetails;
import org.opentravel.ota._2003._05.SuccessType;

import com.isa.thinair.airschedules.api.dto.DisplayFlightDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.ModelNamesAndFieldNames;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;

/**
 * Prepares flight information.
 * 
 * @author Nasly
 * 
 */
public class FlightSearchUtil extends BaseUtil {

	private static final Log log = LogFactory.getLog(FlightSearchUtil.class);

	private static GlobalConfig globalConfig = WebServicesModuleUtils.getGlobalConfig();

	/**
	 * Searches matching flight information and prepares OTAAirFlifoRS.
	 * 
	 * @param otaAirFlifoRQ
	 * @return
	 * @throws WebservicesException
	 */
	public static OTAAirFlifoRS getFlightInfo(OTAAirFlifoRQ otaAirFlifoRQ) throws WebservicesException, ModuleException {
		Date depDate = new Date(CommonUtil.getTime(otaAirFlifoRQ.getDepartureDate()));
		String flightNumber = otaAirFlifoRQ.getFlightNumber();
		String fromAirport = null;
		if (otaAirFlifoRQ.getDepartureAirport() != null) {
			fromAirport = otaAirFlifoRQ.getDepartureAirport().getLocationCode();
		}
		String toAirport = null;
		if (otaAirFlifoRQ.getArrivalAirport() != null) {
			toAirport = otaAirFlifoRQ.getArrivalAirport().getLocationCode();
		}

		List<ModuleCriterion> criteria = new ArrayList<ModuleCriterion>();

		// Departure date
		if (depDate != null) {
			Calendar sartDateCal = new GregorianCalendar();
			sartDateCal.setTime(depDate);
			sartDateCal.set(Calendar.HOUR_OF_DAY, 0);
			sartDateCal.set(Calendar.MINUTE, 0);
			sartDateCal.set(Calendar.SECOND, 0);
			sartDateCal.set(Calendar.MILLISECOND, 0);
			ModuleCriterion criterionStartDate = new ModuleCriterion();
			criterionStartDate.setCondition(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS);
			criterionStartDate.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE);
			List<Date> valueStartDate = new ArrayList<Date>();
			valueStartDate.add(sartDateCal.getTime());
			criterionStartDate.setValue(valueStartDate);
			criteria.add(criterionStartDate);

			// end date
			Calendar stopDateCal = new GregorianCalendar();
			stopDateCal.setTime(depDate);
			stopDateCal.set(Calendar.HOUR_OF_DAY, 23);
			stopDateCal.set(Calendar.MINUTE, 59);
			stopDateCal.set(Calendar.SECOND, 59);
			stopDateCal.set(Calendar.MILLISECOND, 999);

			ModuleCriterion criterionStopDate = new ModuleCriterion();
			criterionStopDate.setCondition(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS);
			criterionStopDate.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE);
			List<Date> valueStopDate = new ArrayList<Date>();
			valueStopDate.add(stopDateCal.getTime());
			criterionStopDate.setValue(valueStopDate);
			criteria.add(criterionStopDate);
		}

		// Flight Number
		if (flightNumber != null) {
			ModuleCriterion criterionFlightNumber = new ModuleCriterion();
			criterionFlightNumber.setCondition(ModuleCriterion.CONDITION_EQUALS);
			criterionFlightNumber.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.FLIGHT_NUMBER);
			List<String> valueFlightNo = new ArrayList<String>();
			valueFlightNo.add(flightNumber);
			criterionFlightNumber.setValue(valueFlightNo);
			criteria.add(criterionFlightNumber);
		}

		// Origin Airport
		if (fromAirport != null) {
			ModuleCriterion criterionFromApt = new ModuleCriterion();
			criterionFromApt.setCondition(ModuleCriterion.CONDITION_EQUALS);
			criterionFromApt.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.ORIGIN_APT_CODE);
			List<String> valueFromApt = new ArrayList<String>();
			valueFromApt.add(fromAirport);
			criterionFromApt.setValue(valueFromApt);
			criteria.add(criterionFromApt);
		}

		// Destination Airport
		if (toAirport != null) {
			ModuleCriterion criterionToApt = new ModuleCriterion();
			criterionToApt.setCondition(ModuleCriterion.CONDITION_EQUALS);
			criterionToApt.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DESTINATION_APT_CODE);
			List<String> valueToApt = new ArrayList<String>();
			valueToApt.add(toAirport);
			criteria.add(criterionToApt);
		}

		// Exclude cancelled flights
		ModuleCriterion criterionStatus = new ModuleCriterion();
		criterionStatus.setCondition(ModuleCriterion.CONDITION_EQUALS);
		criterionStatus.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.STATUS);
		List<String> valueStatus = new ArrayList<String>();
		valueStatus.add(FlightStatusEnum.ACTIVE.getCode());
		valueStatus.add(FlightStatusEnum.CLOSED.getCode());
		valueStatus.add(FlightStatusEnum.CLOSED_FOR_RESERVATION.getCode());

		criterionStatus.setValue(valueStatus);
		criteria.add(criterionStatus);

		Page page = null;
		Collection<DisplayFlightDTO> aaFlightsInfo = null;
		try {
			page = WebServicesModuleUtils.getFlightBD().searchFlightsForDisplay(criteria, 0, 20, true, null, true, false, null);
			if (page != null && page.getPageData() != null) {
				aaFlightsInfo = page.getPageData();
			}
		} catch (ModuleException me) {
			log.error(me);
			throw new WebservicesException(me);
		}

		return prepareResponse(aaFlightsInfo);
	}

	/**
	 * Prepares OTAAirFlifoRS.
	 * 
	 * TODO - Set departure date (currently only departure time is set)
	 * 
	 * @param aaFlightsInfo
	 * @return
	 * @throws WebservicesException
	 */
	private static OTAAirFlifoRS prepareResponse(Collection<DisplayFlightDTO> aaFlightsInfo) throws WebservicesException,
			ModuleException {
		OTAAirFlifoRS otaAirFlifoRS = new OTAAirFlifoRS();
		if (aaFlightsInfo != null) {
			Collection<FlightSegement> aaFlightSegs = null;
			Flight aaFlight = null;

			for (DisplayFlightDTO aaFlightDTO : aaFlightsInfo) {
				aaFlight = aaFlightDTO.getFlight();
				FlightInfoDetails flightInfoDetails = new FlightInfoDetails();
				otaAirFlifoRS.setFlightInfoDetails(flightInfoDetails);

				aaFlightSegs = aaFlight.getFlightSegements();
				for (FlightSegement aaFlightSeg : aaFlightSegs) {
					FlightInfoDetails.FlightLegInfo flightLegInfo = new FlightInfoDetails.FlightLegInfo();

					// Arrival airport
					int tmpLength = aaFlightSeg.getSegmentCode().length();
					OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.ArrivalAirport arrAirport = new OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.ArrivalAirport();
					arrAirport.setLocationCode(aaFlightSeg.getSegmentCode().substring(tmpLength - 3));
					flightLegInfo.getArrivalAirport().add(arrAirport);
					arrAirport.setTerminal((String) globalConfig.getAirportInfo(arrAirport.getLocationCode(), true)[2]);

					// Departure airport
					OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.DepartureAirport departureAirport = new OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.DepartureAirport();
					departureAirport.setLocationCode(aaFlightSeg.getSegmentCode().substring(0, 3));
					flightLegInfo.setDepartureAirport(departureAirport);
					departureAirport
							.setTerminal((String) globalConfig.getAirportInfo(departureAirport.getLocationCode(), true)[2]);

					// Departure time
					OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.DepartureDateTime departureDateTime = new OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.DepartureDateTime();
					departureDateTime.setEstimated(CommonUtil.getFormattedDate(aaFlightSeg.getEstTimeDepatureLocal()));
					flightLegInfo.setDepartureDateTime(departureDateTime);

					// Arrival time
					OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.ArrivalDateTime arrivalDateTime = new OTAAirFlifoRS.FlightInfoDetails.FlightLegInfo.ArrivalDateTime();
					arrivalDateTime.setEstimated(CommonUtil.getFormattedDate(aaFlightSeg.getEstTimeArrivalLocal()));
					flightLegInfo.setArrivalDateTime(arrivalDateTime);

					// Duration
					Duration journeyDuration = CommonUtil.getDuration(aaFlightSeg.getEstTimeArrivalZulu().getTime()
							- aaFlightSeg.getEstTimeDepatureZulu().getTime());
					flightLegInfo.setJourneyDuration(journeyDuration);

					// Marketing airline
					CompanyNameType companyName = new CompanyNameType();
					companyName.setCode("SDFSD");
					companyName.setValue(aaFlight.getFlightNumber().substring(0, 2));
					companyName.setCode(aaFlight.getFlightNumber().substring(0, 2));
					flightLegInfo.setMarketingAirline(companyName);

					// FIXME - Take Equipment Type from database
					EquipmentType equipmentType = new EquipmentType();
					equipmentType.setAirEquipType("A320");
					equipmentType.setValue("Airbus 320");
					flightLegInfo.setEquipment(equipmentType);

					flightInfoDetails.getFlightLegInfo().add(flightLegInfo);
				}
			}
		}
		otaAirFlifoRS.setSuccess(new SuccessType());

		return otaAirFlifoRS;
	}
}
