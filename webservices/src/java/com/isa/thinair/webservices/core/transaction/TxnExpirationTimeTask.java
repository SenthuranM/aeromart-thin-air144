package com.isa.thinair.webservices.core.transaction;

import java.util.TimerTask;

import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

/**
 * Time Task for timing out and removing expired transactions.
 * 
 * @author Mohamed Nasly
 */
public class TxnExpirationTimeTask extends TimerTask {

	@Override
	public void run() {
		WebServicesModuleUtils.getTrasnsactionStore().removeExpiredTransactions();
	}
}
