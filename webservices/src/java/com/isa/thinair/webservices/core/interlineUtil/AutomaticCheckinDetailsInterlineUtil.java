/**
 * 
 */
package com.isa.thinair.webservices.core.interlineUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.IOTACodeTables;

import com.isa.thinair.airmaster.api.dto.AutomaticCheckinDTO;
import com.isa.thinair.airmaster.core.bl.AutoCheckinBL;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.util.BaseUtil;
import com.isa.thinair.webservices.core.util.CommonServicesUtil;

public class AutomaticCheckinDetailsInterlineUtil extends BaseUtil {

	private static final Log log = LogFactory.getLog(AutomaticCheckinDetailsInterlineUtil.class);
	private static final String DELIM = "$";

	public static Map<String, Map<String, List<AutomaticCheckinDTO>>> getSelectedAutoCheckinDetails(
			Map<String, Map<String, String>> travelerFlightDetailsWithAutoCheckins, FlightPriceRS flightPriceRS, SYSTEM system)
			throws ModuleException, WebservicesException {

		Map<String, Map<String, List<AutomaticCheckinDTO>>> segmentWithAutoCheckins = new HashMap<String, Map<String, List<AutomaticCheckinDTO>>>();

		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();

		Map<Integer, FlightSegmentTO> flightSegmentMap = CommonServicesUtil.getFlightSegmentDetails(flightPriceRS);

		if (flightSegmentMap != null && !flightSegmentMap.isEmpty()) {
			flightSegmentTOs.addAll(flightSegmentMap.values());
		}

		for (String travelerRef : travelerFlightDetailsWithAutoCheckins.keySet()) {
			Map<String, String> flightRPHMap = travelerFlightDetailsWithAutoCheckins.get(travelerRef);
			for (String flightRPH : flightRPHMap.keySet()) {
				List<AutomaticCheckinDTO> autoCheckinDTOs = new ArrayList<AutomaticCheckinDTO>();
				for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
					if (flightRPH.equals(flightSegmentTO.getFlightRefNumber())) {
						String departAirportCode = extractDepartAirportCodeFromFlightRefNumber(flightRPH);
						AutoCheckinBL autoCheckinBL = new AutoCheckinBL();
						boolean isAutoCheckinFound = false;
						List<AutomaticCheckinDTO> autoCheckinDTOList = autoCheckinBL
								.getAutomaticCheckinDetails(departAirportCode);
						if (autoCheckinDTOList != null && autoCheckinDTOList.size() > 0) {
							AutomaticCheckinDTO automaticCheckinDTO = autoCheckinDTOList.get(0);
							if (automaticCheckinDTO != null) {
								isAutoCheckinFound = true;
								autoCheckinDTOs.add(automaticCheckinDTO);
							}
						}
						if (!isAutoCheckinFound) {
							throw new WebservicesException(
									IOTACodeTables.AccelaeroErrorCodes_AAE_SELECTED_AUTOCHECKINS_ARE_NOT_AVAILABLE_AT_THE_MOMENT,
									"");
						}
						if (segmentWithAutoCheckins.get(travelerRef) == null) {
							segmentWithAutoCheckins.put(travelerRef, new HashMap<String, List<AutomaticCheckinDTO>>());
						}
						segmentWithAutoCheckins.get(travelerRef).put(flightSegmentTO.getFlightRefNumber(), autoCheckinDTOs);
					}
				}
			}
		}

		return segmentWithAutoCheckins;
	}

	public static Map<String, List<LCCClientExternalChgDTO>> getExternalChargesMapForAutoCheckins(
			Map<String, Map<String, List<AutomaticCheckinDTO>>> autoCheckinDetailsMap) throws ModuleException {

		Map<String, List<LCCClientExternalChgDTO>> externalCharges = new HashMap<String, List<LCCClientExternalChgDTO>>();

		for (String travelerRef : autoCheckinDetailsMap.keySet()) {
			List<LCCClientExternalChgDTO> chargesList = new ArrayList<LCCClientExternalChgDTO>();
			Map<String, List<AutomaticCheckinDTO>> autoCheckinsMap = autoCheckinDetailsMap.get(travelerRef);
			for (String flightRPH : autoCheckinsMap.keySet()) {
				List<AutomaticCheckinDTO> autoCheckinDTOs = autoCheckinsMap.get(flightRPH);
				for (AutomaticCheckinDTO autoCheckinDTO : autoCheckinDTOs) {
					LCCClientExternalChgDTO externalChargeTO = new LCCClientExternalChgDTO();
					externalChargeTO.setCode(String.valueOf(autoCheckinDTO.getAutomaticCheckinTemplateId()));
					externalChargeTO.setAmount(autoCheckinDTO.getAmount());
					externalChargeTO.setExternalCharges(EXTERNAL_CHARGES.AUTOMATIC_CHECKIN);
					externalChargeTO.setFlightRefNumber(flightRPH);

					chargesList.add(externalChargeTO);
				}
			}
			externalCharges.put(travelerRef, chargesList);
		}
		return externalCharges;
	}

	public static String extractDepartAirportCodeFromFlightRefNumber(String flightRefNumber) {
		String departAirportCode = null;
		if (flightRefNumber.indexOf(DELIM) != -1) {
			String[] values = StringUtils.split(flightRefNumber, DELIM);
			String segmentCode = values[1];
			departAirportCode = segmentCode.substring(0, segmentCode.indexOf("/"));
		}
		return departAirportCode;
	}
}