package com.isa.thinair.webservices.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AAOTAAirMealDetailsRQ;
import org.opentravel.ota._2003._05.AAOTAAirMealDetailsRQ.MealDetailsRequests.MealDetailsRequest;
import org.opentravel.ota._2003._05.AAOTAAirMealDetailsRS;
import org.opentravel.ota._2003._05.AAOTAAirMealDetailsRS.MealDetailsResponses;
import org.opentravel.ota._2003._05.AAOTAAirMealDetailsRS.MealDetailsResponses.MealDetailsResponse;
import org.opentravel.ota._2003._05.AAOTAAirMealDetailsRS.MealDetailsResponses.MealDetailsResponse.FlightSegmentInfo;
import org.opentravel.ota._2003._05.AAOTAAirMultiMealDetailsRQ;
import org.opentravel.ota._2003._05.AAOTAAirMultiMealDetailsRS;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.FlightSegmentBaseType.ArrivalAirport;
import org.opentravel.ota._2003._05.FlightSegmentBaseType.DepartureAirport;
import org.opentravel.ota._2003._05.FlightSegmentType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.Meal;
import org.opentravel.ota._2003._05.MultiMeal;
import org.opentravel.ota._2003._05.SuccessType;
import org.opentravel.ota._2003._05.WarningsType;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO.LCCAncillaryType;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentMealsDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealResponceDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.meal.MealExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;

public class MealDetailsUtil extends BaseUtil {
	private static final Log log = LogFactory.getLog(MealDetailsUtil.class);

	public MealDetailsUtil() {
		super();
	}

	/**
	 * 
	 * @Deprecated use the {@link MultiMeal}
	 */
	@Deprecated
	public AAOTAAirMealDetailsRS getMealDetails(AAOTAAirMealDetailsRQ aaOtaAirMealDetailsRQ) throws WebservicesException {

		AAOTAAirMealDetailsRS aaOtaAirMealDetailsRS = null;
		try {
			List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();

			for (MealDetailsRequest mealDetailsRequest : aaOtaAirMealDetailsRQ.getMealDetailsRequests().getMealDetailsRequest()) {
				FlightSegmentType flightSegmentType = mealDetailsRequest.getFlightSegmentInfo();
				FlightSegmentTO flightSegmentTo = getFlightSegmentTO(flightSegmentType);
				if (flightSegmentTo.getFlightRefNumber() == null || "".equals(flightSegmentTo.getFlightRefNumber())) {
					// If RPH value is not getting from client side, retrieve flight segment id
					if (flightSegmentTo.getFlightSegId() == null) {
						Collection<Integer> segIds = SegmentUtil.getFlightSegmentIds(flightSegmentTo);
						if (segIds.size() > 0) {
							for (Integer segId : segIds) {
								flightSegmentTo.setFlightSegId(segId);
								flightSegmentTo.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentTo));
								flightSegmentTOs.add(flightSegmentTo);
							}
						}
					} else {
						flightSegmentTo.setFlightRefNumber(flightSegmentType.getRPH());
						flightSegmentTOs.add(flightSegmentTo);
					}
				} else {
					flightSegmentTOs.add(flightSegmentTo);
				}
			}
			if (WSAncillaryUtil.isAncillaryAvailable(flightSegmentTOs, LCCAncillaryType.MEALS, null)) {

				List<LCCMealRequestDTO> lccMealRequestDTOs = composeMealRequest(flightSegmentTOs, null,
						aaOtaAirMealDetailsRQ.getTransactionIdentifier());
				ThreadLocalData.getCurrentTnxParam(Transaction.FARE_SEGMENT_CHARGE_TO);
				LCCMealResponceDTO mealResponceDTO = WebServicesModuleUtils.getAirproxyAncillaryBD().getAvailableMeals(
						lccMealRequestDTOs, null, null, WSAncillaryUtil.getOndSelectedBundledFares(), SYSTEM.AA,
						aaOtaAirMealDetailsRQ.getTransactionIdentifier(), ApplicationEngine.WS, null);

				aaOtaAirMealDetailsRS = generateMealResponse(mealResponceDTO);
			} else {
				throw new WebservicesException(
						IOTACodeTables.AccelaeroErrorCodes_AAE_MEAL_DETAILS_NOT_AVAILABLE_FOR_REQUESTED_CARRIER_, "");
			}
		} catch (ModuleException e) {
			log.error(e);
			if (e.getExceptionCode().equals("airinventory.flight.notfound")) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_FLIGHT_DOES_NOT_OPERATE_ON_DATE_REQUESTED, "");
			}
			throw new WebservicesException(e);
		}

		return aaOtaAirMealDetailsRS;
	}

	public AAOTAAirMultiMealDetailsRS getMultiMealDetails(AAOTAAirMultiMealDetailsRQ aaOtaAirMultiMealDetailsRQ)
			throws WebservicesException {

		AAOTAAirMultiMealDetailsRS aaOtaAirMultiMealDetailsRS = null;
		try {
			List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();

			for (AAOTAAirMultiMealDetailsRQ.MealDetailsRequests.MealDetailsRequest mealDetailsRequest : aaOtaAirMultiMealDetailsRQ
					.getMealDetailsRequests().getMealDetailsRequest()) {
				FlightSegmentType flightSegmentType = mealDetailsRequest.getFlightSegmentInfo();
				FlightSegmentTO flightSegmentTo = getFlightSegmentTO(flightSegmentType);
				if (flightSegmentTo.getFlightRefNumber() == null || "".equals(flightSegmentTo.getFlightRefNumber())) {
					// If RPH value is not getting from client side, retrieve flight segment id
					if (flightSegmentTo.getFlightSegId() == null) {
						Collection<Integer> segIds = SegmentUtil.getFlightSegmentIds(flightSegmentTo);
						if (segIds.size() > 0) {
							for (Integer segId : segIds) {
								flightSegmentTo.setFlightSegId(segId);
								flightSegmentTo.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentTo));
								flightSegmentTOs.add(flightSegmentTo);
							}
						}
					} else {
						flightSegmentTo.setFlightRefNumber(flightSegmentType.getRPH());
						flightSegmentTOs.add(flightSegmentTo);
					}
				} else {
					flightSegmentTOs.add(flightSegmentTo);
				}
			}
			if (WSAncillaryUtil.isAncillaryAvailable(flightSegmentTOs, LCCAncillaryType.MEALS, null)) {

				List<LCCMealRequestDTO> lccMealRequestDTOs = composeMealRequest(flightSegmentTOs, null,
						aaOtaAirMultiMealDetailsRQ.getTransactionIdentifier());
				ThreadLocalData.getCurrentTnxParam(Transaction.FARE_SEGMENT_CHARGE_TO);
				LCCMealResponceDTO mealResponceDTO = WebServicesModuleUtils.getAirproxyAncillaryBD().getAvailableMeals(
						lccMealRequestDTOs, null, null, WSAncillaryUtil.getOndSelectedBundledFares(), SYSTEM.AA,
						aaOtaAirMultiMealDetailsRQ.getTransactionIdentifier(), ApplicationEngine.WS, null);

				aaOtaAirMultiMealDetailsRS = generateMultiMealResponse(mealResponceDTO);
			} else {
				throw new WebservicesException(
						IOTACodeTables.AccelaeroErrorCodes_AAE_MEAL_DETAILS_NOT_AVAILABLE_FOR_REQUESTED_CARRIER_, "");
			}
		} catch (ModuleException e) {
			log.error(e);
			if (e.getExceptionCode().equals("airinventory.flight.notfound")) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_FLIGHT_DOES_NOT_OPERATE_ON_DATE_REQUESTED, "");
			}
			throw new WebservicesException(e);
		}

		return aaOtaAirMultiMealDetailsRS;
	}

	private FlightSegmentTO getFlightSegmentTO(FlightSegmentType flightSegmentType) {
		FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
		flightSegmentTO.setArrivalDateTime(flightSegmentType.getArrivalDateTime().toGregorianCalendar().getTime());
		flightSegmentTO.setArrivalTerminalName(flightSegmentType.getArrivalAirport().getTerminal());
		flightSegmentTO.setDepartureDateTime(flightSegmentType.getDepartureDateTime().toGregorianCalendar().getTime());
		flightSegmentTO.setFlightNumber(flightSegmentType.getFlightNumber());
		flightSegmentTO.setOperatingAirline(flightSegmentType.getOperatingAirline().getCode());
		if (flightSegmentType.getSegmentCode() != null) {
			flightSegmentTO.setSegmentCode(flightSegmentType.getSegmentCode());
		} else {
			flightSegmentTO.setSegmentCode(flightSegmentType.getDepartureAirport().getLocationCode() + "/"
					+ flightSegmentType.getArrivalAirport().getLocationCode());
		}
		if (flightSegmentType.getRPH() != null && !flightSegmentType.getRPH().equals("")) {
			Integer flightSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegmentType.getRPH());
			flightSegmentTO.setFlightSegId(flightSegId);
		}

		String cabinClassCode = "Y";
		if (AppSysParamsUtil.getDefaultCOS() != null && !AppSysParamsUtil.getDefaultCOS().isEmpty()) {
			cabinClassCode = AppSysParamsUtil.getDefaultCOS();
		}
		flightSegmentTO.setCabinClassCode(cabinClassCode);
		flightSegmentTO.setFlightRefNumber(flightSegmentType.getRPH());
		CommonServicesUtil.setOndSequence(flightSegmentTO);
		return flightSegmentTO;
	}

	/**
	 * Compose the meal request list from the flight segment list
	 * 
	 * @param flightSegTO
	 * @param txnId
	 * @return List<LCCMealRequestDTO>
	 */
	private List<LCCMealRequestDTO> composeMealRequest(List<FlightSegmentTO> flightSegTO, String cabinClass, String txnId) {
		List<LCCMealRequestDTO> mL = new ArrayList<LCCMealRequestDTO>();
		for (FlightSegmentTO fst : flightSegTO) {
			LCCMealRequestDTO mrd = new LCCMealRequestDTO();
			mrd.setTransactionIdentifier(txnId);
			if (cabinClass != null) {
				mrd.setCabinClass(cabinClass);
			} else {
				mrd.setCabinClass(fst.getCabinClassCode());
			}
			mrd.getFlightSegment().add(fst);
			mL.add(mrd);
		}
		return mL;
	}

	/**
	 * generate WS meal response
	 * 
	 * @param mealDetailsDTO
	 * @return aaOtaAirMealDetailsRS
	 * @throws ModuleException
	 * @Deprecated use the {@link MultiMeal}
	 */
	@Deprecated
	public AAOTAAirMealDetailsRS generateMealResponse(LCCMealResponceDTO mealDetailsDTO) throws ModuleException {

		AAOTAAirMealDetailsRS aaOtaAirMealDetailsRS = new AAOTAAirMealDetailsRS();
		Map<Integer, Boolean> flightMultiMealSelection = getFlightMultiMealSelection();

		aaOtaAirMealDetailsRS.setErrors(new ErrorsType());
		aaOtaAirMealDetailsRS.setWarnings(new WarningsType());

		MealDetailsResponses mealDetailsResponses = new MealDetailsResponses();

		for (LCCFlightSegmentMealsDTO flightSegmentMealsDto : mealDetailsDTO.getFlightSegmentMeals()) {
			MealDetailsResponse mealDetailsResponse = new MealDetailsResponse();
			FlightSegmentTO flightSegmentTO = flightSegmentMealsDto.getFlightSegmentTO();
			FlightSegmentInfo flightSegmentInfo = getFlightSegmentInfo(flightSegmentTO);

			mealDetailsResponse.setFlightSegmentInfo(flightSegmentInfo);

			for (LCCMealDTO meal : flightSegmentMealsDto.getMeals()) {
				Meal wsMeal = new Meal();
				wsMeal.setMealCode(meal.getMealCode());
				wsMeal.setMealName(meal.getMealName());
				wsMeal.setMealDescription(meal.getMealDescription());
				wsMeal.setMealCharge(meal.getMealCharge());
				wsMeal.setAvailableMeals(meal.getAvailableMeals());
				wsMeal.setSoldMeals(meal.getSoldMeals());
				wsMeal.setAllocatedMeals(meal.getAllocatedMeals());
				wsMeal.setMealImageLink(meal.getMealImagePath());
				wsMeal.setMealCategoryCode(meal.getMealCategoryCode());

				mealDetailsResponse.getMeal().add(wsMeal);
			}
			mealDetailsResponses.getMealDetailsResponse().add(mealDetailsResponse);

			flightMultiMealSelection.put(flightSegmentTO.getFlightSegId(), (mealDetailsDTO.isMultipleMealSelectionEnabled()
					&& flightSegmentMealsDto.isMultiMealEnabledForLogicalCabinClass()));

		}
		mealDetailsResponses.setMultipleMealSelectionEnabled(mealDetailsDTO.isMultipleMealSelectionEnabled());
		aaOtaAirMealDetailsRS.setMealDetailsResponses(mealDetailsResponses);
		aaOtaAirMealDetailsRS.setSuccess(new SuccessType());

		return aaOtaAirMealDetailsRS;
	}

	/**
	 * generate WS meal response
	 * 
	 * @param mealDetailsDTO
	 * @return aaOtaAirMealDetailsRS
	 * @throws ModuleException
	 */
	public AAOTAAirMultiMealDetailsRS generateMultiMealResponse(LCCMealResponceDTO mealDetailsDTO) throws ModuleException {

		AAOTAAirMultiMealDetailsRS aaOtaAirMultiMealDetailsRS = new AAOTAAirMultiMealDetailsRS();
		Map<Integer, Boolean> flightMultiMealSelection = getFlightMultiMealSelection();

		aaOtaAirMultiMealDetailsRS.setErrors(new ErrorsType());
		aaOtaAirMultiMealDetailsRS.setWarnings(new WarningsType());

		AAOTAAirMultiMealDetailsRS.MealDetailsResponses mealDetailsResponses = new AAOTAAirMultiMealDetailsRS.MealDetailsResponses();

		for (LCCFlightSegmentMealsDTO flightSegmentMealsDto : mealDetailsDTO.getFlightSegmentMeals()) {
			AAOTAAirMultiMealDetailsRS.MealDetailsResponses.MealDetailsResponse mealDetailsResponse = new AAOTAAirMultiMealDetailsRS.MealDetailsResponses.MealDetailsResponse();
			FlightSegmentTO flightSegmentTO = flightSegmentMealsDto.getFlightSegmentTO();
			AAOTAAirMultiMealDetailsRS.MealDetailsResponses.MealDetailsResponse.FlightSegmentInfo flightSegmentInfo = getMultiMealFlightSegmentInfo(
					flightSegmentTO);

			mealDetailsResponse.setFlightSegmentInfo(flightSegmentInfo);

			for (LCCMealDTO meal : flightSegmentMealsDto.getMeals()) {
				MultiMeal wsMeal = new MultiMeal();
				wsMeal.setMealCode(meal.getMealCode());
				wsMeal.setMealName(meal.getMealName());
				wsMeal.setMealDescription(meal.getMealDescription());
				wsMeal.setMealCharge(meal.getMealCharge());
				wsMeal.setAvailableMeals(meal.getAvailableMeals());
				wsMeal.setSoldMeals(meal.getSoldMeals());
				wsMeal.setAllocatedMeals(meal.getAllocatedMeals());
				wsMeal.setMealImageLink(meal.getMealImagePath());
				wsMeal.setMealCategoryCode(meal.getMealCategoryCode());
				wsMeal.setMealCategoryRestricted(meal.isCategoryRestricted());

				mealDetailsResponse.getMultiMeal().add(wsMeal);
			}
			mealDetailsResponses.getMealDetailsResponse().add(mealDetailsResponse);

			flightMultiMealSelection.put(flightSegmentTO.getFlightSegId(), (mealDetailsDTO.isMultipleMealSelectionEnabled()
					&& flightSegmentMealsDto.isMultiMealEnabledForLogicalCabinClass()));

		}
		mealDetailsResponses.setMultipleMealSelectionEnabled(mealDetailsDTO.isMultipleMealSelectionEnabled());
		aaOtaAirMultiMealDetailsRS.setMealDetailsResponses(mealDetailsResponses);
		aaOtaAirMultiMealDetailsRS.setSuccess(new SuccessType());

		return aaOtaAirMultiMealDetailsRS;
	}

	/**
	 * Build flight segment Info
	 * 
	 * @param flightSegmentTO
	 * @return flightSegmentInfo
	 * @throws ModuleException
	 */
	@Deprecated
	public FlightSegmentInfo getFlightSegmentInfo(FlightSegmentTO flightSegmentTO) throws ModuleException {
		FlightSegmentInfo flightSegmentInfo = new FlightSegmentInfo();

		String[] airPorts = flightSegmentTO.getSegmentCode().split("/");

		ArrivalAirport arrivalAirport = new ArrivalAirport();
		arrivalAirport.setTerminal(flightSegmentTO.getArrivalTerminalName());
		arrivalAirport.setLocationCode(airPorts[airPorts.length - 1]);

		DepartureAirport departureAirport = new DepartureAirport();
		departureAirport.setTerminal(flightSegmentTO.getDepartureTerminalName());
		departureAirport.setLocationCode(airPorts[0]);

		flightSegmentInfo.setArrivalAirport(arrivalAirport);
		flightSegmentInfo.setDepartureAirport(departureAirport);
		flightSegmentInfo.setArrivalDateTime(CommonUtil.parse(flightSegmentTO.getArrivalDateTime()));
		flightSegmentInfo.setDepartureDateTime(CommonUtil.parse(flightSegmentTO.getDepartureDateTime()));
		flightSegmentInfo.setFlightNumber(flightSegmentTO.getFlightNumber());
		flightSegmentInfo.setSegmentCode(flightSegmentTO.getSegmentCode());
		flightSegmentInfo.setRPH(flightSegmentTO.getFlightSegId() + "");
		return flightSegmentInfo;
	}

	/**
	 * 
	 * TODO Rename method name to getFlightSegmentInfo
	 */
	public AAOTAAirMultiMealDetailsRS.MealDetailsResponses.MealDetailsResponse.FlightSegmentInfo
			getMultiMealFlightSegmentInfo(FlightSegmentTO flightSegmentTO) throws ModuleException {
		AAOTAAirMultiMealDetailsRS.MealDetailsResponses.MealDetailsResponse.FlightSegmentInfo flightSegmentInfo = new AAOTAAirMultiMealDetailsRS.MealDetailsResponses.MealDetailsResponse.FlightSegmentInfo();

		String[] airPorts = flightSegmentTO.getSegmentCode().split("/");

		ArrivalAirport arrivalAirport = new ArrivalAirport();
		arrivalAirport.setTerminal(flightSegmentTO.getArrivalTerminalName());
		arrivalAirport.setLocationCode(airPorts[airPorts.length - 1]);

		DepartureAirport departureAirport = new DepartureAirport();
		departureAirport.setTerminal(flightSegmentTO.getDepartureTerminalName());
		departureAirport.setLocationCode(airPorts[0]);

		flightSegmentInfo.setArrivalAirport(arrivalAirport);
		flightSegmentInfo.setDepartureAirport(departureAirport);
		flightSegmentInfo.setArrivalDateTime(CommonUtil.parse(flightSegmentTO.getArrivalDateTime()));
		flightSegmentInfo.setDepartureDateTime(CommonUtil.parse(flightSegmentTO.getDepartureDateTime()));
		flightSegmentInfo.setFlightNumber(flightSegmentTO.getFlightNumber());
		flightSegmentInfo.setSegmentCode(flightSegmentTO.getSegmentCode());
		flightSegmentInfo.setRPH(flightSegmentTO.getFlightSegId() + "");
		return flightSegmentInfo;
	}

	/**
	 * @param travelerFlightDetailsWithMeals
	 * @param selectedFlightDTO
	 * @return segmentWithMeals
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public static Map<String, Map<Integer, List<LCCMealDTO>>> getSelectedMealDetails(
			Map<String, Map<Integer, List<String>>> travelerFlightDetailsWithMeals, SelectedFlightDTO selectedFlightDTO)
			throws ModuleException, WebservicesException {

		Map<String, Map<Integer, List<LCCMealDTO>>> segmentWithMeals = new HashMap<String, Map<Integer, List<LCCMealDTO>>>();

		Collection<FlightSegmentDTO> flightSegmentDTOs = new ArrayList<FlightSegmentDTO>();
		List<Integer> flightSegmentIDs = new ArrayList<Integer>();
		int salesChannelCode = ThreadLocalData.getCurrentUserPrincipal().getSalesChannel();

		Collection<OndFareDTO> ondFareDTOList = selectedFlightDTO.getOndFareDTOs(false);

		for (OndFareDTO ondFareDTO : ondFareDTOList) {
			flightSegmentDTOs.addAll(ondFareDTO.getSegmentsMap().values());
			flightSegmentIDs.addAll(ondFareDTO.getSegmentsMap().keySet());
		}

		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();
		for (Integer fltSegId : flightSegmentIDs) {
			flightSegIdWiseCos.put(fltSegId, null);
		}

		Map<Integer, List<FlightMealDTO>> meals = WebServicesModuleUtils.getMealBD().getMeals(flightSegIdWiseCos, null, false,
				null, salesChannelCode);
		if (meals.isEmpty()) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_SELECTED_MEALS_ARE_NOT_AVAILABLE_AT_THE_MOMENT,
					"");
		}

		// override meals if bundled fare is selected
		setMealOffers(meals, selectedFlightDTO, salesChannelCode);

		for (String travelerRef : travelerFlightDetailsWithMeals.keySet()) {
			// FlightSegmentId -> Meal(s)
			Map<Integer, List<String>> flightDetailsWithMeals = travelerFlightDetailsWithMeals.get(travelerRef);
			for (Integer flightSegId : flightDetailsWithMeals.keySet()) {
				List<LCCMealDTO> lccMealDTOs = new ArrayList<LCCMealDTO>();
				List<String> mealDetails = flightDetailsWithMeals.get(flightSegId);
				if (mealDetails != null) {
					for (String selectedMeal : mealDetails) {
						Integer mealQuantity = Collections.frequency(mealDetails, selectedMeal);
						boolean isCatRestriction = false;
						for (FlightSegmentDTO flightSegmentDTO : flightSegmentDTOs) {
							if (flightSegId.equals(flightSegmentDTO.getSegmentId())) {
								List<FlightMealDTO> flightMealDTOs = meals.get(flightSegId);
								boolean isMealFound = false;
								for (FlightMealDTO flightMealDTO : flightMealDTOs) {
									if (flightMealDTO.getMealCode().equals(selectedMeal)
											&& flightMealDTO.getAvailableMeals() > mealQuantity) {
										isMealFound = true;
										LCCMealDTO lccMealDTO = new LCCMealDTO();
										lccMealDTO.setMealCharge(flightMealDTO.getAmount());
										lccMealDTO.setPopularity(flightMealDTO.getPopularity());
										lccMealDTO.setMealCode(flightMealDTO.getMealCode());
										lccMealDTO.setCategoryRestricted(flightMealDTO.isCategoryRestricted());
										if (flightMealDTO.isCategoryRestricted()) {
											isCatRestriction = true;
										}
										lccMealDTOs.add(lccMealDTO);
									}
								}
								if (segmentWithMeals.get(travelerRef) == null) {
									segmentWithMeals.put(travelerRef, new HashMap<Integer, List<LCCMealDTO>>());
								}
								segmentWithMeals.get(travelerRef).put(flightSegmentDTO.getSegmentId(), lccMealDTOs);
								if (!isMealFound) {
									throw new WebservicesException(
											IOTACodeTables.AccelaeroErrorCodes_AAE_SELECTED_MEALS_ARE_NOT_AVAILABLE_AT_THE_MOMENT,
											selectedMeal);
								}

								if ((!isMultiMealEnabled(flightSegId) || isCatRestriction)
										&& (mealQuantity > 1 || lccMealDTOs.size() > 1)) {
									throw new WebservicesException(
											IOTACodeTables.AccelaeroErrorCodes_AAE_MULTI_MEAL_NOT_ENABLED_IN_THE_SYSTEM, "");
								}
							}

						}
					}
				}
			}
		}

		return segmentWithMeals;
	}

	private static void setMealOffers(Map<Integer, List<FlightMealDTO>> meals, SelectedFlightDTO selectedFlightDTO, int salesChannelCode)
			throws ModuleException {
		Map<Integer, Integer> fltSegWiseMealTemplates = WSAncillaryUtil
				.getFlightSegmentWiseBundledServiceTemplate(selectedFlightDTO, EXTERNAL_CHARGES.MEAL.toString());

		if (fltSegWiseMealTemplates != null && !fltSegWiseMealTemplates.isEmpty()) {
			for (Entry<Integer, List<FlightMealDTO>> mealEntry : meals.entrySet()) {
				Integer fltSegId = mealEntry.getKey();
				if (fltSegWiseMealTemplates.containsKey(fltSegId) && fltSegWiseMealTemplates.get(fltSegId) != null) {
					Integer mealTemplateId = fltSegWiseMealTemplates.get(fltSegId);

					List<FlightMealDTO> bundledFareMeals = WebServicesModuleUtils.getMealBD().getMealsByTemplate(mealTemplateId,
							null, null, null, fltSegId, null, salesChannelCode);

					if (!bundledFareMeals.isEmpty()) {
						meals.put(fltSegId, bundledFareMeals);
					}
				}
			}
		}
	}

	/**
	 * @param mealDetailsMap
	 * @return externalCharegs
	 * @throws ModuleException
	 */
	public static Map<String, List<ExternalChgDTO>>
			getExternalChargesMapForMeals(Map<String, Map<Integer, List<LCCMealDTO>>> mealDetailsMap) throws ModuleException {

		Collection<EXTERNAL_CHARGES> exChargestypes = new HashSet<EXTERNAL_CHARGES>();
		exChargestypes.add(EXTERNAL_CHARGES.MEAL);
		Map<EXTERNAL_CHARGES, ExternalChgDTO> quotedMap = WebServicesModuleUtils.getReservationBD()
				.getQuotedExternalCharges(exChargestypes, null, null);

		MealExternalChgDTO quotedChgDTO = (MealExternalChgDTO) quotedMap.get(EXTERNAL_CHARGES.MEAL);

		if (quotedChgDTO == null) {
			throw new ModuleException("Can not locate the Meal external charges details");
		}

		Map<String, List<ExternalChgDTO>> externalCharges = new HashMap<String, List<ExternalChgDTO>>();

		for (String travelerRef : mealDetailsMap.keySet()) {
			List<ExternalChgDTO> chargesList = new ArrayList<ExternalChgDTO>();
			Map<Integer, List<LCCMealDTO>> mealsMap = mealDetailsMap.get(travelerRef);
			for (Integer flightSegmentId : mealsMap.keySet()) {
				// If multi meal enabled and multiple meals selected following list contains more than 1 elements
				List<LCCMealDTO> mealDTOs = mealsMap.get(flightSegmentId);
				for (LCCMealDTO mealDTO : mealDTOs) {
					MealExternalChgDTO externalChargeTO = new MealExternalChgDTO();

					externalChargeTO.setAmount(mealDTO.getMealCharge());
					externalChargeTO.setChargeCode(mealDTO.getMealCode());
					externalChargeTO
							.setChargeDescription(quotedChgDTO.getChargeCode() + "/" + quotedChgDTO.getChargeDescription());
					externalChargeTO.setChgGrpCode(ReservationInternalConstants.ChargeGroup.SUR);
					externalChargeTO.setChgRateId(quotedChgDTO.getChgRateId());
					externalChargeTO.setExternalChargesEnum(EXTERNAL_CHARGES.MEAL);
					externalChargeTO.setMealCharge(mealDTO.getMealCharge());
					externalChargeTO.setMealCode(mealDTO.getMealCode());
					externalChargeTO.setFlightSegId(flightSegmentId);

					chargesList.add(externalChargeTO);
				}
			}
			externalCharges.put(travelerRef, chargesList);
		}
		return externalCharges;
	}

	/**
	 * @param mealsInReservation
	 * @return mealsInReservation
	 */
	public static Collection<PaxMealTO> getCombinedPaxMealDTOs(Collection<PaxMealTO> mealsInReservation) {

		List<PaxMealTO> mealList = null;

		if (mealsInReservation != null) {
			Map<String, PaxMealTO> mealMap = new HashMap<String, PaxMealTO>();
			for (PaxMealTO paxMealTO : mealsInReservation) {
				String mealKey = paxMealTO.getMealCode() + "$" + paxMealTO.getPnrSegId() + "$" + paxMealTO.getPnrPaxId();
				PaxMealTO meal = mealMap.get(mealKey);

				if (meal == null) {
					mealMap.put(mealKey, paxMealTO);
				} else {
					meal.setAllocatedQty(meal.getAllocatedQty() + 1);
				}
			}

			mealList = new ArrayList<PaxMealTO>(mealMap.values());
		}

		return mealList;
	}

	public static boolean isMultiMealEnabled(Integer flightSegId) {
		Map<Integer, Boolean> flightMultiMealSelection = getFlightMultiMealSelection();
		boolean isAllowMultiMeal = false;
		if (flightMultiMealSelection != null && flightMultiMealSelection.containsKey(flightSegId)
				&& flightMultiMealSelection.get(flightSegId) != null) {
			isAllowMultiMeal = flightMultiMealSelection.get(flightSegId);
		} else {
			isAllowMultiMeal = AppSysParamsUtil.isMultipleMealSelectionEnabled();
		}
		return isAllowMultiMeal;
	}

	@SuppressWarnings("unchecked")
	private static Map<Integer, Boolean> getFlightMultiMealSelection() {
		Map<Integer, Boolean> flightMultiMealSelection = null;
		Object sessionObj = ThreadLocalData.getCurrentTnxParam(Transaction.FLIGHT_MULTI_MEAL_SELECTION);
		if (sessionObj == null) {
			flightMultiMealSelection = new HashMap<Integer, Boolean>();
		} else {
			flightMultiMealSelection = (Map<Integer, Boolean>) sessionObj;
		}

		ThreadLocalData.setCurrentTnxParam(Transaction.FLIGHT_MULTI_MEAL_SELECTION, flightMultiMealSelection);

		return flightMultiMealSelection;
	}
}
