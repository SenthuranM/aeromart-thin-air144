/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.core.util;

import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AAOTAPaxCreditRS;
import org.opentravel.ota._2003._05.AAOTAPaxCreditReadRQ;
import org.opentravel.ota._2003._05.AAPaxCreditReadRequestType;
import org.opentravel.ota._2003._05.AAPaxCreditType;
import org.opentravel.ota._2003._05.AAPaxCreditType.CreditAmount;
import org.opentravel.ota._2003._05.AAPnrPaxCreditType;
import org.opentravel.ota._2003._05.AAPnrPaxCreditsType;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.AirItineraryType.OriginDestinationOptions;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.FlightSegmentBaseType.ArrivalAirport;
import org.opentravel.ota._2003._05.FlightSegmentBaseType.DepartureAirport;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.SuccessType;

import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaymentDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;

public class PassengerCreditSearchUtil extends BaseUtil {

	private final Log log = LogFactory.getLog(getClass());

	public PassengerCreditSearchUtil() {
		super();
	}

	/**
	 * 
	 * @param aaOTAPaxCreditReadRQ
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public AAOTAPaxCreditRS getPassengerCredit(AAOTAPaxCreditReadRQ aaOTAPaxCreditReadRQ) throws ModuleException,
			WebservicesException {

		AAOTAPaxCreditRS aaOTAPaxCreditRS = new AAOTAPaxCreditRS();
		aaOTAPaxCreditRS.setErrors(new ErrorsType());

		Collection col = extractPassengerCreditSearchReqParams(aaOTAPaxCreditReadRQ, aaOTAPaxCreditRS);
		preparePassengerCreditSearchResponse(aaOTAPaxCreditReadRQ, aaOTAPaxCreditRS, col);

		return aaOTAPaxCreditRS;
	}

	/**
	 * 
	 * @param aaOTAPaxCreditReadRQ
	 * @param aaOTAPaxCreditRS
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private Collection extractPassengerCreditSearchReqParams(AAOTAPaxCreditReadRQ aaOTAPaxCreditReadRQ,
			AAOTAPaxCreditRS aaOTAPaxCreditRS) throws WebservicesException, ModuleException {

		// TODO - Test all search criterias

		ReservationPaymentDTO reservationPaymentDTO = new ReservationPaymentDTO();
		Collection col = null;
		String pnr = null;
		String firstName = null;
		String lastName = null;
		String creditCardNo = null;
		String expiryDate = null;
		boolean creditCardSearch = false;

		AAPaxCreditReadRequestType aaPaxCreditReadRequestType = aaOTAPaxCreditReadRQ.getPaxCreditReadRequest();

		pnr = aaPaxCreditReadRequestType.getBookingReferenceID();
		firstName = aaPaxCreditReadRequestType.getFirstName();
		lastName = aaPaxCreditReadRequestType.getLastName();
		creditCardNo = aaPaxCreditReadRequestType.getCreditCardNumber();
		expiryDate = aaPaxCreditReadRequestType.getExpiryDate();

		// if ((pnr != null) && (!pnr.trim().equals(""))) {
		// creditCardSearch = false;
		// } else if (((creditCardNo != null) && (!creditCardNo.trim().equals("")))
		// || ((expiryDate != null) && (!expiryDate.trim().equals("")))) {
		// creditCardSearch = true;
		// }

		if ((creditCardNo != null && !creditCardNo.trim().equals("")) || (expiryDate != null && !expiryDate.trim().equals(""))) {
			creditCardSearch = true;
		} else {
			creditCardSearch = false;
		}

		reservationPaymentDTO.setFilterZeroCredits(aaPaxCreditReadRequestType.isExcludeZeroBalanceRecords());
		
		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);


		boolean hasAuthority = AuthorizationUtil.hasPrivileges(privilegeKeys,
				PrivilegesKeys.AlterResPrivilegesKeys.PAX_ADJUST_CREDIT,
				PrivilegesKeys.AlterResPrivilegesKeys.PAX_ADJUST_CREDIT_ANY);
		if (!hasAuthority) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR, "");
		}

		if (!AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.AlterResPrivilegesKeys.PAX_ADJUST_CREDIT_ANY)) {
			reservationPaymentDTO.setOriginAgentCode(ThreadLocalData.getCurrentUserPrincipal().getAgentCode());
			if ((firstName == null || firstName.trim().equals("")) || (lastName == null || lastName.trim().equals(""))) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR, "Passenger name required");
			}
		}

		if (!creditCardSearch) {

			if (((pnr == null) || (pnr.trim().equals(""))) && ((firstName == null) || (firstName.trim().equals("")))
					&& ((lastName == null) || (lastName.trim().equals("")))) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
						"Pnr number, first name or last name is required.");
			}

			/** Default Serach */
			if (pnr != null && !pnr.trim().equals("")) {
				reservationPaymentDTO.setPnr(pnr);
			}

			/**
			 * if exact match is not checked the first and the lastname should have '*' added at the end.
			 */
			if (!aaPaxCreditReadRequestType.isExactMatchOnly()) {
				if (firstName != null) {
					firstName += "*";
				}
				if (lastName != null) {
					lastName += "*";
				}
			}
			reservationPaymentDTO.setFirstName(firstName);
			reservationPaymentDTO.setLastName(lastName);

			/** Advanced Search Options */
			if (aaPaxCreditReadRequestType.getFromAirport() != null) {
				reservationPaymentDTO.setFromAirport(aaPaxCreditReadRequestType.getFromAirport().getLocationCode());
			}
			if (aaPaxCreditReadRequestType.getToAirport() != null) {
				reservationPaymentDTO.setToAirport(aaPaxCreditReadRequestType.getToAirport().getLocationCode());
			}
			if (aaPaxCreditReadRequestType.getDepartureDateTime() != null) {
				reservationPaymentDTO.setDepartureDate(CommonUtil.parseDate(aaPaxCreditReadRequestType.getDepartureDateTime())
						.getTime());
			}
			if (aaPaxCreditReadRequestType.getArrivalDateTime() != null) {
				reservationPaymentDTO.setReturnDate(CommonUtil.parseDate(aaPaxCreditReadRequestType.getArrivalDateTime())
						.getTime());
			}

			reservationPaymentDTO.setTelephoneNo(aaPaxCreditReadRequestType.getTelephoneNumber());
			reservationPaymentDTO.setFlightNo(aaPaxCreditReadRequestType.getFlightNumber());

			col = WebServicesModuleUtils.getReservationQueryBD().getPnrPassengerSumCredit(reservationPaymentDTO);
		} else {

			if (((creditCardNo == null) || (creditCardNo.trim().equals("")))
					&& ((expiryDate == null) || (!expiryDate.trim().equals("")))) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
						"Both credit card number and the expiry date is required.");
			}

			/** Card Search */
			reservationPaymentDTO = new ReservationPaymentDTO();
			reservationPaymentDTO.setCreditCardNo(creditCardNo);
			reservationPaymentDTO.setEDate(expiryDate);

			col = WebServicesModuleUtils.getReservationQueryBD().getReservationsForCreditCardInfo(reservationPaymentDTO);
		}

		return col;
	}

	/**
	 * 
	 * @param aaOTAPaxCreditReadRQ
	 * @param aaOTAPaxCreditRS
	 * @param col
	 * @throws WebservicesException
	 */
	private void preparePassengerCreditSearchResponse(AAOTAPaxCreditReadRQ aaOTAPaxCreditReadRQ,
			AAOTAPaxCreditRS aaOTAPaxCreditRS, Collection col) throws WebservicesException, ModuleException {

		aaOTAPaxCreditRS.setVersion(WebservicesConstants.OTAConstants.VERSION_PaxCreditReq);
		aaOTAPaxCreditRS.setTransactionIdentifier(aaOTAPaxCreditReadRQ.getTransactionIdentifier());
		aaOTAPaxCreditRS.setSequenceNmbr(aaOTAPaxCreditReadRQ.getSequenceNmbr());
		aaOTAPaxCreditRS.setEchoToken(aaOTAPaxCreditReadRQ.getEchoToken());
		aaOTAPaxCreditRS.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);

		AAPnrPaxCreditsType aaPnrPaxCreditsType = new AAPnrPaxCreditsType();

		boolean dataFound = false;

		Iterator it = col.iterator();
		while (it.hasNext()) {
			ReservationPaxDTO reservationPaxDTO = (ReservationPaxDTO) it.next();

			if (reservationPaxDTO != null) {
				Collection colPassengers = reservationPaxDTO.getPassengers();

				if (colPassengers != null) {
					dataFound = true;

					AAPnrPaxCreditType aaPnrPaxCreditType = new AAPnrPaxCreditType();
					aaPnrPaxCreditType.setBookingReferenceID(reservationPaxDTO.getPnr());

					// Segment Information
					Collection colSegments = reservationPaxDTO.getSegments();
					AirItineraryType airItineraryType = getAirItineraryType(colSegments);
					aaPnrPaxCreditType.setAirItinerary(airItineraryType);

					// Passenger Information
					Iterator itPassegners = colPassengers.iterator();
					while (itPassegners.hasNext()) {
						ReservationPaxDetailsDTO reservationPaxDetailsDTO = (ReservationPaxDetailsDTO) itPassegners.next();

						AAPaxCreditType aaPaxCreditType = new AAPaxCreditType();
						aaPaxCreditType.setFirstName(reservationPaxDetailsDTO.getFirstName());
						aaPaxCreditType.setLastName(reservationPaxDetailsDTO.getLastName());
						aaPaxCreditType.setRPH(reservationPaxDetailsDTO.getPnrPaxId().toString());

						CreditAmount creditAmount = new CreditAmount();
						OTAUtils.setAmountAndDefaultCurrency(creditAmount, reservationPaxDetailsDTO.getCredit());
						aaPaxCreditType.setCreditAmount(creditAmount);

						aaPnrPaxCreditType.getPaxCredit().add(aaPaxCreditType);
					}

					aaPnrPaxCreditsType.getPnrPaxCredit().add(aaPnrPaxCreditType);
				}
			}
		}

		if (dataFound) {
			// one or more matching details found
			aaOTAPaxCreditRS.getSuccessAndWarningsAndPaxCredits().add(new SuccessType());
			aaOTAPaxCreditRS.getSuccessAndWarningsAndPaxCredits().add(aaPnrPaxCreditsType);
		} else {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_NO_MATCH_FOUND, "No matching details found");
		}
		if (log.isDebugEnabled())
			log.debug(getPaxCreditResultsSummary(aaOTAPaxCreditRS).toString());
	}

	/**
	 * Returns the AirItinerary Information.
	 * 
	 * @param colSegments
	 * @return
	 * @throws WebservicesException
	 */
	private static AirItineraryType getAirItineraryType(Collection<ReservationSegmentDTO> colSegments)
			throws WebservicesException, ModuleException {
		AirItineraryType airItineraryType = new AirItineraryType();
		OriginDestinationOptions originDestinationOptions = new OriginDestinationOptions();
		OriginDestinationOptionType originDestinationOptionType;
		ReservationSegmentDTO reservationSegmentDTO;
		BookFlightSegmentType bookFlightSegmentType;
		ArrivalAirport arrivalAirport;
		DepartureAirport departureAirport;

		for (Iterator itReservationSegmentDTO = colSegments.iterator(); itReservationSegmentDTO.hasNext();) {

			reservationSegmentDTO = (ReservationSegmentDTO) itReservationSegmentDTO.next();

			departureAirport = new DepartureAirport();
			arrivalAirport = new ArrivalAirport();
			departureAirport.setLocationCode(getDepartureOrArrivalSegment(reservationSegmentDTO.getSegmentCode(), true));
			arrivalAirport.setLocationCode(getDepartureOrArrivalSegment(reservationSegmentDTO.getSegmentCode(), false));

			bookFlightSegmentType = new BookFlightSegmentType();
			bookFlightSegmentType.setDepartureAirport(departureAirport);
			bookFlightSegmentType.setArrivalAirport(arrivalAirport);
			bookFlightSegmentType.setFlightNumber(reservationSegmentDTO.getFlightNo());
			bookFlightSegmentType.setDepartureDateTime(CommonUtil.parse(reservationSegmentDTO.getDepartureDate()));
			bookFlightSegmentType.setArrivalDateTime(CommonUtil.parse(reservationSegmentDTO.getArrivalDate()));

			bookFlightSegmentType.setRPH(reservationSegmentDTO.getPnrSegId().toString());
			bookFlightSegmentType.setStatus(OTAUtils.getWSResSegStatus(reservationSegmentDTO.getStatus()));

			originDestinationOptionType = new OriginDestinationOptionType();
			originDestinationOptionType.getFlightSegment().add(bookFlightSegmentType);
			originDestinationOptions.getOriginDestinationOption().add(originDestinationOptionType);
		}

		airItineraryType.setOriginDestinationOptions(originDestinationOptions);
		return airItineraryType;
	}

	/**
	 * Returns the departure segment or arrival segment
	 * 
	 * @param segmentCode
	 * @param isDepartureSegment
	 * @return
	 */
	private static String getDepartureOrArrivalSegment(String segmentCode, boolean isDepartureSegment) {
		if (isDepartureSegment) {
			return segmentCode.substring(0, 3);
		} else {
			return segmentCode.substring(segmentCode.length() - 3);
		}
	}

	/**
	 * 
	 * @param aaOTAPaxCreditRS
	 * @return
	 * @throws WebservicesException
	 */
	private StringBuffer getPaxCreditResultsSummary(AAOTAPaxCreditRS aaOTAPaxCreditRS) throws WebservicesException,
			ModuleException {
		StringBuffer paxCreditResultsSummary = new StringBuffer();
		String nl = "\n\r \t";
		for (Object item : aaOTAPaxCreditRS.getSuccessAndWarningsAndPaxCredits()) {
			if (item instanceof AAPnrPaxCreditsType) {
				for (AAPnrPaxCreditType aaPnrPaxCreditType : ((AAPnrPaxCreditsType) item).getPnrPaxCredit()) {
					paxCreditResultsSummary
							.append(nl + "Booking Info [PNR = " + aaPnrPaxCreditType.getBookingReferenceID() + "]");

					for (OriginDestinationOptionType originDestinationOption : aaPnrPaxCreditType.getAirItinerary()
							.getOriginDestinationOptions().getOriginDestinationOption()) {
						for (BookFlightSegmentType bookFlightSegment : originDestinationOption.getFlightSegment()) {
							paxCreditResultsSummary.append(nl + "Segment Info [fltNumber = "
									+ bookFlightSegment.getFlightNumber());
							paxCreditResultsSummary.append(",Origin = "
									+ bookFlightSegment.getDepartureAirport().getLocationCode());
							paxCreditResultsSummary.append(",Destination = "
									+ bookFlightSegment.getArrivalAirport().getLocationCode());
							paxCreditResultsSummary.append(",DepDateTime = "
									+ CommonUtil.getFormattedDate(bookFlightSegment.getDepartureDateTime().toGregorianCalendar()
											.getTime()));
							paxCreditResultsSummary.append(",ArrDepDateTime = "
									+ CommonUtil.getFormattedDate(bookFlightSegment.getArrivalDateTime().toGregorianCalendar()
											.getTime()) + "]");
						}
					}
				}
			}
		}
		return paxCreditResultsSummary;
	}
}
