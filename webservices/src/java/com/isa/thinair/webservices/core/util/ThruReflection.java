package com.isa.thinair.webservices.core.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Reflection API calls.
 * 
 * @author Mehdi Raza
 */
public class ThruReflection {

	private final static Log log = LogFactory.getLog(ThruReflection.class);

	public static Object[] invoke(Object o, MethodsToInvokeSequence methods) throws Exception {
		Object[] response = new Object[methods.getMethods().size()];
		int count = 0;
		for (Object[] details : methods.getMethods()) {
			Class[] clazz = new Class[((Object[]) details[1]).length];
			Object[] params = (Object[]) details[1];
			for (int x = 0; x < params.length; x++) {
				clazz[x] = params[x].getClass();
			}
			Method m = null;

			try {
				m = o.getClass().getMethod(details[0].toString(), clazz);
				response[count] = m.invoke(o, params);
				++count;
			} catch (Exception e) {
				log.error("ThruReflection:: invoking method [" + m.getName() + "] failed", e);
				throw e;
			}
		}
		return response;
	}

	/**
	 * Sets specified attributes in the object
	 * 
	 * @param object
	 * @param attributeNames
	 * @param attributeValues
	 * @throws Exception
	 */
	public static void setAttributeValues(Object object, String[] attributeNames, Object[] attributeValues) throws Exception {

		MethodsToInvokeSequence methods = new MethodsToInvokeSequence();
		for (int i = 0; i < attributeNames.length; i++) {
			methods.put("set" + attributeNames[i], attributeValues[i]);
		}

		ThruReflection.invoke(object, methods);

	}

	public static Map<String, Object> getAttributeValues(Object object, String[] attributeNames) throws Exception {
		Map<String, Object> values = new HashMap<String, Object>();

		if (object != null && attributeNames != null) {
			Method m = null;
			String getterMethodName = null;
			Object attribValue = null;
			for (String attributeName : attributeNames) {
				getterMethodName = "get" + attributeName;
				m = object.getClass().getMethod(getterMethodName);
				attribValue = m.invoke(object);
				values.put(attributeName, attribValue);
			}
		}

		return values;
	}

	/**
	 * Returns requested attributes value (obtained through reflection).
	 * 
	 * @param object
	 * @param attributeName
	 * @return
	 * @throws Exception
	 */
	public static Object getAttributeValue(Object object, String attributeName) throws Exception {
		Object value = null;
		Map valuesMap = getAttributeValues(object, new String[] { attributeName });
		if (valuesMap != null) {
			value = valuesMap.get(attributeName);
		}
		return value;
	}

	public static class MethodsToInvokeSequence {
		private List<Object[]> _methods = new ArrayList<Object[]>();

		public void put(String methodName, Object... args) {
			_methods.add(new Object[] { methodName, args });
		}

		public List<Object[]> getMethods() {
			return _methods;
		}
	}
}
