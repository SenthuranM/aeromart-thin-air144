package com.isa.thinair.webservices.core.interlineUtil;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AirFeeType;
import org.opentravel.ota._2003._05.AirItineraryPricingInfoType;
import org.opentravel.ota._2003._05.AirItineraryType;
import org.opentravel.ota._2003._05.AirTaxType;
import org.opentravel.ota._2003._05.AirTripType;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.FareType;
import org.opentravel.ota._2003._05.FreeTextType;
import org.opentravel.ota._2003._05.FareType.BaseFare;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirPriceRQ;
import org.opentravel.ota._2003._05.OTAAirPriceRQ.FlexiFareSelectionOptions;
import org.opentravel.ota._2003._05.OTAAirPriceRS;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.PTCFareBreakdownType;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.PricedItinerariesType;
import org.opentravel.ota._2003._05.PricedItineraryType;
import org.opentravel.ota._2003._05.SSRType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.AutomaticCheckinRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.AutomaticCheckinRequests.AutomaticCheckinRequest;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.BaggageRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.BaggageRequests.BaggageRequest;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.MealRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.MealRequests.MealRequest;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SSRRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SSRRequests.SSRRequest;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SeatRequests;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SeatRequests.SeatRequest;
import org.opentravel.ota._2003._05.SuccessType;
import org.opentravel.ota._2003._05.TravelerInfoSummaryType;
import org.opentravel.ota._2003._05.TravelerInformationType;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airmaster.api.dto.AutomaticCheckinDTO;
import com.isa.thinair.airmaster.api.dto.SegmentCarrierDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirSeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCBaggageDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredJourneyDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCMealDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ClassOfServicePreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.CabinClassWiseDefaultLogicalCCDetailDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger;
import com.isa.thinair.webplatform.api.v2.util.AnalyticsLogger.AnalyticSource;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;
import com.isa.thinair.webservices.api.dtos.BundledServiceDTO;
import com.isa.thinair.webservices.api.dtos.FlexiFareSelectionCriteria;
import com.isa.thinair.webservices.api.dtos.OTAPaxCountTO;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;
import com.isa.thinair.webservices.core.util.BaseUtil;
import com.isa.thinair.webservices.core.util.CommonServicesUtil;
import com.isa.thinair.webservices.core.util.InsuranceServiceUtil;
import com.isa.thinair.webservices.core.util.PriceQuoteUtil;
import com.isa.thinair.webservices.core.util.ServiceTaxCalculator;
import com.isa.thinair.webservices.core.util.ServiceTaxUtil;
import com.isa.thinair.webservices.core.util.WSReservationUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

/**
 * Manipulates OTA_PriceRQ/RS
 * 
 * @author Mohamed Nasly
 * @author nafly - Modified to support dry/interline
 */
public class PriceQuoteInterlineUtil extends BaseUtil {

	private final Log log = LogFactory.getLog(getClass());

	public PriceQuoteInterlineUtil() {
		super();
	}

	/**
	 * @param otaAirPriceRQ
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public OTAAirPriceRS getPrice(OTAAirPriceRQ otaAirPriceRQ) throws ModuleException, WebservicesException {
		OTAAirPriceRS otaAirPriceRS = new OTAAirPriceRS();
		otaAirPriceRS.setErrors(new ErrorsType());
		boolean isMultiCity = false;
		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();

		FlightPriceRQ flightPriceRQ;
		
		if (otaAirPriceRQ.getAirItinerary().getDirectionInd() == AirTripType.MULTI_CITY
				|| otaAirPriceRQ.getAirItinerary().getDirectionInd() == AirTripType.OPEN_JAW) {
			flightPriceRQ = createFlightPriceRQForMultiCity(otaAirPriceRQ, principal);
			isMultiCity = true;
		} else {
			flightPriceRQ = createFlightPriceRQ(otaAirPriceRQ, principal);
		}
		
		ServiceTaxUtil.validatePassangerEmbarkationInformation(otaAirPriceRQ);

		if (flightPriceRQ.getAvailPreferences().isRequoteFlightSearch()) {
			String pnr = otaAirPriceRQ.getModifiedSegmentInfo().getBookingReferenceID().get(0).getID();
			AnalyticsLogger.logRequoteSearch(AnalyticSource.WS_MOD_REQUOTE, flightPriceRQ, principal, pnr, null);
		} else {
			AnalyticsLogger.logAvlSearch(AnalyticSource.WS_AVL_FQ, flightPriceRQ, principal, null);
		}
		// As the user is initiating a new price quote, we have to clear the fare quote related parameters from the
		// user tnx.
		String inboundFirstSegmentOrigin = (String) ThreadLocalData.getCurrentTnxParam(Transaction.RETURN_POINT_AIRPORT);
		PriceQuoteInterlineUtil.clearPriceQuoteRelatedTransactionParams();
		ThreadLocalData.setCurrentTnxParam(Transaction.RETURN_POINT_AIRPORT, inboundFirstSegmentOrigin);

		FlightPriceRS flightPriceRS = WebServicesModuleUtils.getAirproxySearchAndQuoteBD().quoteFlightPrice(flightPriceRQ, null);
		if (flightPriceRS != null) {
			// For the selected flight save the FareSegChargeTO in the user tnx.
			PriceQuoteInterlineUtil.addFareSegmentChargeTOToTransaction(flightPriceRS);

			if (flightPriceRS.getApplicableServiceTaxes().size() > 0) {
				ThreadLocalData.setCurrentTnxParam(Transaction.APPLICABLE_SERVICE_TAXES,
						flightPriceRS.getApplicableServiceTaxes());
			}

			createFlightPriceRS(otaAirPriceRQ, otaAirPriceRS, flightPriceRQ, flightPriceRS, principal, isMultiCity);
		}

		return otaAirPriceRS;
	}

	/**
	 * Transform Airproxy price quote response to OTA Price Quote response
	 * 
	 * @param otaAirPriceRQ
	 * @param otaAirPriceRS
	 * @param flightPriceRQ
	 * @param flightPriceRS
	 * @param isMultiCity 
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private void createFlightPriceRS(OTAAirPriceRQ otaAirPriceRQ, OTAAirPriceRS otaAirPriceRS, FlightPriceRQ flightPriceRQ,
			FlightPriceRS flightPriceRS, UserPrincipal userPrincipal, boolean isMultiCity) throws ModuleException, WebservicesException {

		// FIXME set all the necessary params from the request
		otaAirPriceRS.setVersion(WebservicesConstants.OTAConstants.VERSION_AirPrice);
		otaAirPriceRS.setTransactionIdentifier(otaAirPriceRQ.getTransactionIdentifier());
		otaAirPriceRS.setSequenceNmbr(otaAirPriceRQ.getSequenceNmbr());
		otaAirPriceRS.setEchoToken(otaAirPriceRQ.getEchoToken());
		otaAirPriceRS.setPrimaryLangID(WebservicesConstants.OTAConstants.PRIMARY_LANG_CODE);

		PricedItinerariesType pricedItineraries = new PricedItinerariesType();
		otaAirPriceRS.getSuccessAndWarningsAndPricedItineraries().add(pricedItineraries);

		PricedItineraryType pricedItinerary = new PricedItineraryType();
		pricedItineraries.getPricedItinerary().add(pricedItinerary);
		AirItineraryType airItinerary = new AirItineraryType();
		pricedItinerary.setAirItinerary(airItinerary);
		pricedItinerary.setSequenceNumber(new BigInteger("1"));

		AirItineraryType.OriginDestinationOptions originDestinationOptions = new AirItineraryType.OriginDestinationOptions();
		airItinerary.setOriginDestinationOptions(originDestinationOptions);

		// Flexi Fare Selection Criteria building and saving to Transaction.
		FlexiFareSelectionCriteria flexiSelectionCriteria = new FlexiFareSelectionCriteria();

		// If the Flexi fare is selected need to set the flexi quoting.
		if (otaAirPriceRQ.getFlexiFareSelectionOptions() != null) {
			FlexiFareSelectionOptions flexiOptions = otaAirPriceRQ.getFlexiFareSelectionOptions();

			flexiSelectionCriteria.setInBoundFlexiFareSelected(flexiOptions.getInBoundFlexiSelected() != null ? flexiOptions
					.getInBoundFlexiSelected() : false);
			flexiSelectionCriteria.setOutBoundFlexiFareSelected(flexiOptions.getOutBoundFlexiSelected() != null ? flexiOptions
					.getOutBoundFlexiSelected() : false);
		}

		SYSTEM system = flightPriceRQ.getAvailPreferences().getSearchSystem();

		if (isSeatSelected(otaAirPriceRQ)) {
			Map<String, Map<String, LCCAirSeatDTO>> selectedSeats = SeatServiceInterlineUtil.getSelectedSeatsDetails(
					getFlightDetailsWithSeats(otaAirPriceRQ), flightPriceRS, system);
			SeatServiceInterlineUtil.validateLCCSeatRequest(otaAirPriceRQ, selectedSeats);
			Map<String, List<LCCClientExternalChgDTO>> chargesMapForSeats = SeatServiceInterlineUtil
					.getExternalChargesMapForLCCSeats(selectedSeats);
			for (String travelerRef : chargesMapForSeats.keySet()) {
				addExternalCharge(travelerRef, chargesMapForSeats.get(travelerRef));
			}
		}

		if (isMealsSelected(otaAirPriceRQ)) {
			Map<String, Map<String, List<LCCMealDTO>>> selectedMeals = MealDetailsInterlineUtil.getSelectedMealDetails(
					getFlightDetailsWithMeals(otaAirPriceRQ), flightPriceRS, system);
			Map<String, List<LCCClientExternalChgDTO>> chargesMapForMeals = MealDetailsInterlineUtil
					.getExternalChargesMapForLCCMeals(selectedMeals);
			for (String travelerRef : chargesMapForMeals.keySet()) {
				addExternalCharge(travelerRef, chargesMapForMeals.get(travelerRef));
			}
		}

		if (isSSRSelected(otaAirPriceRQ)) {

			Map<SSRType, Map<String, Map<String, List<HashMap<String, String>>>>> serviceWiseMap = getFlightDetailsWithSSRs(otaAirPriceRQ);

			// Get External charges related to airport services
			OTAPaxCountTO otaPaxCountTO = new OTAPaxCountTO(flightPriceRQ.getTravelerInfoSummary());

			PaxCountAssembler paxCountAssembler = new PaxCountAssembler(otaPaxCountTO.getAdultCount(),
					otaPaxCountTO.getChildCount(), otaPaxCountTO.getInfantCount());
			// TODO, instead of loading all SSR Types and iterating to find the requested one.
			// Its better if we can send the selected services and get a response on it from Airproxy

			Map<String, Map<String, List<LCCAirportServiceDTO>>> selectedAirportServices = SSRDetailsInterlineUtil
					.getSelectedAirportServiceDetails(serviceWiseMap.get(SSRType.AIRPORT), paxCountAssembler, system,
							flightPriceRS);
			Map<String, List<LCCClientExternalChgDTO>> chargesMapForSsrs = SSRDetailsInterlineUtil
					.getExternalChargesMapForLCCAirportServices(selectedAirportServices, paxCountAssembler);
			for (String travelerRef : chargesMapForSsrs.keySet()) {
				addExternalCharge(travelerRef, chargesMapForSsrs.get(travelerRef));
			}

			// Get External charges related to in-flight services
			Map<String, Map<String, List<LCCSpecialServiceRequestDTO>>> selectedInflightServices = SSRDetailsInterlineUtil
					.getSelectedInflightServiceDetails(serviceWiseMap.get(SSRType.INFLIGHT), flightPriceRS, system);
			Map<String, List<LCCClientExternalChgDTO>> chargesMapForInflightServices = SSRDetailsInterlineUtil
					.getExternalChargesMapForLCCInflightService(selectedInflightServices, paxCountAssembler);
			for (String travelerRef : chargesMapForInflightServices.keySet()) {
				addExternalCharge(travelerRef, chargesMapForInflightServices.get(travelerRef));
			}

		}

		if (isBaggagesSelected(otaAirPriceRQ)) {
			Map<String, Integer> baggageOndGrpIdWiseCount = new HashMap<String, Integer>();
			Map<String, Map<String, LCCBaggageDTO>> selectedBaggages = BaggageDetailsInterlineUtil.getSelectedBaggageDetails(
					getFlightDetailsWithBaggages(otaAirPriceRQ), flightPriceRS, system, baggageOndGrpIdWiseCount);
			if (AppSysParamsUtil.isONDBaggaeEnabled()) {
				this.isBaggageSelectionValid(baggageOndGrpIdWiseCount, selectedBaggages);
			}
			Map<String, List<LCCClientExternalChgDTO>> chargesMapForBaggages = BaggageDetailsInterlineUtil
					.getExternalChargesMapForBaggages(selectedBaggages, baggageOndGrpIdWiseCount);
			for (String travelerRef : chargesMapForBaggages.keySet()) {
				addExternalCharge(travelerRef, chargesMapForBaggages.get(travelerRef));
			}
		}

		if (isInsuranceSelected(otaAirPriceRQ)) {

			OTAPaxCountTO otaPaxCountTO = new OTAPaxCountTO(flightPriceRQ.getTravelerInfoSummary());

			PaxCountAssembler paxCountAssembler = new PaxCountAssembler(otaPaxCountTO.getAdultCount(),
					otaPaxCountTO.getChildCount(), otaPaxCountTO.getInfantCount());

			LCCInsuranceQuotationDTO lccInsuranceQuotationDTO = (LCCInsuranceQuotationDTO) ThreadLocalData
					.getCurrentTnxParam(Transaction.QUOTED_INSURANCE);

			validateInsurancePriceQuoteRequest(flightPriceRS, paxCountAssembler, lccInsuranceQuotationDTO);
			Map<String, List<LCCClientExternalChgDTO>> chargesMapForInsurance = InsuranceServiceUtil
					.getExternalChargesMapForInsurance(otaAirPriceRQ, lccInsuranceQuotationDTO);
			for (String travelerRef : chargesMapForInsurance.keySet()) {
				addExternalCharge(travelerRef, chargesMapForInsurance.get(travelerRef));
			}

			ThreadLocalData.setCurrentTnxParam(Transaction.RES_INSURANCE_SELECTED,
					getInsuranceReference(otaAirPriceRQ).toString());

		}
		
		if (isAutomaticCheckinsSelected(otaAirPriceRQ)) {
			Map<String, Map<String, List<AutomaticCheckinDTO>>> selectedAutoCheckins = AutomaticCheckinDetailsInterlineUtil
					.getSelectedAutoCheckinDetails(getFlightDetailsWithAutomaticCheckins(otaAirPriceRQ), flightPriceRS, system);
			Map<String, List<LCCClientExternalChgDTO>> chargesMapForAutoCheckins = AutomaticCheckinDetailsInterlineUtil
					.getExternalChargesMapForAutoCheckins(selectedAutoCheckins);
			for (String travelerRef : chargesMapForAutoCheckins.keySet()) {
				addExternalCharge(travelerRef, chargesMapForAutoCheckins.get(travelerRef));
			}
		}

		// Selected flights
		PriceInfoTO priceInfoTO = flightPriceRS.getSelectedPriceFlightInfo();

		if (priceInfoTO != null && priceInfoTO.getFareTypeTO() != null) {
			int ondLength = flightPriceRS.getOriginDestinationInformationList().size();
			boolean faresAvailable = false;
			for (int i = 0; i < ondLength; i++) {
				if (priceInfoTO.getFareTypeTO().getOndWiseFareType().get(i) == FareTypes.NO_FARE) {
					faresAvailable = false;
					break;
				}
				faresAvailable = true;
			}

			if (faresAvailable) {
				// success if seats/fares available
				otaAirPriceRS.getSuccessAndWarningsAndPricedItineraries().add(new SuccessType());

				List<OriginDestinationOptionTO> selectedOBFlightOptions = AvailabilitySearchInterlineUtil.createAvailOptionList(
						flightPriceRS.getOriginDestinationInformationList(), false, true);
				List<OriginDestinationOptionTO> selectedIBFlightOptions = AvailabilitySearchInterlineUtil.createAvailOptionList(
						flightPriceRS.getOriginDestinationInformationList(), true, true);
				
				if(isMultiCity){
					for(OriginDestinationOptionTO ondDestinationOptionTO : selectedOBFlightOptions){
						String operatingCarrier = AppSysParamsUtil.getDefaultCarrierCode();
						String ondCode = ondDestinationOptionTO.getOndCode();
						AnciAvailabilityRS anciAvailabilityRS = WebServicesModuleUtils.getAirproxySearchAndQuoteBD()
								.getTaxApplicabilityForAncillaries(operatingCarrier, ondCode, EXTERNAL_CHARGES.JN_ANCI, null);
		
						ServiceTaxCalculator serviceTaxCalculator = new ServiceTaxCalculator(anciAvailabilityRS);
						serviceTaxCalculator.calculateForInterline(ondDestinationOptionTO.getFlightSegmentList());
					}
				}else{
					OriginDestinationOptionTO ondDestinationOptionTO = selectedOBFlightOptions.iterator().next();
					String operatingCarrier = AppSysParamsUtil.getDefaultCarrierCode();
					String ondCode = ondDestinationOptionTO.getOndCode();
					AnciAvailabilityRS anciAvailabilityRS = WebServicesModuleUtils.getAirproxySearchAndQuoteBD()
							.getTaxApplicabilityForAncillaries(operatingCarrier, ondCode, EXTERNAL_CHARGES.JN_ANCI, null);
	
					ServiceTaxCalculator serviceTaxCalculator = new ServiceTaxCalculator(anciAvailabilityRS);
					serviceTaxCalculator.calculateForInterline(ondDestinationOptionTO.getFlightSegmentList());
				}
				
				ServiceTaxUtil.calculateServiceTax(flightPriceRQ, getQuotedSegements(flightPriceRS), priceInfoTO);

				QuotedPriceInfoTO quotedPriceInfoTO = new QuotedPriceInfoTO();
				quotedPriceInfoTO.setBookingType(flightPriceRQ.getTravelPreferences().getBookingType());

				quotedPriceInfoTO.setQuotedSegments(getQuotedSegements(flightPriceRS));

				String[] stages = { ReservationInternalConstants.AirportMessageStages.SEARCH_RESULT,
						ReservationInternalConstants.AirportMessageStages.CARD_PAYMENT };

				List<FreeTextType> outboundAirportMessages = AirportMessageInterlineUtil.retrieveAirportMessages(
						selectedOBFlightOptions, stages);
				List<FreeTextType> inboundAirportMessages = AirportMessageInterlineUtil.retrieveAirportMessages(
						selectedIBFlightOptions, stages);

				if (outboundAirportMessages != null && !outboundAirportMessages.isEmpty()) {
					pricedItinerary.getNotes().addAll(outboundAirportMessages);
				}

				if (inboundAirportMessages != null && !inboundAirportMessages.isEmpty()) {
					pricedItinerary.getNotes().addAll(inboundAirportMessages);
				}
				
				PricedItineraryInterlineUtil.preparePricedItinerary(pricedItinerary, selectedOBFlightOptions,
						selectedIBFlightOptions, flightPriceRQ, false, flexiSelectionCriteria, flightPriceRS, quotedPriceInfoTO, isMultiCity);

				// set the total price to the transaction
				PriceQuoteInterlineUtil.storeTotalPrice(pricedItinerary);
				
				//Clear Bundled Fare Related Params in the transaction
				PriceQuoteInterlineUtil.clearAvailableOndBundledFare();
				// Set selected bundled in the transaction
				addSelectedBundledToTransaction(otaAirPriceRQ);

				// set the quoted information to the transaction
				ThreadLocalData.setCurrentTnxParam(Transaction.QUOTED_PRICE_INFO, quotedPriceInfoTO);
				ThreadLocalData.setCurrentTnxParam(Transaction.PRICE_INFO, priceInfoTO);
				ThreadLocalData.setCurrentTnxParam(Transaction.AVAILABLE_OND_BUNDLED_FARES,
						pricedItinerary.getAirItinerary().getOriginDestinationOptions().getAABundledServiceExt());
				if (log.isDebugEnabled()) {
					log.debug(getriceQuoteResultsSummary(otaAirPriceRS).toString());
				}
			} else {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_NO_AVAILABILITY, null);
			}
		} else {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_NO_AVAILABILITY, null);
		}
	}

	/**
	 * 
	 * @param flightPriceRS
	 * @return
	 */
	private List<FlightSegmentTO> getQuotedSegements(FlightPriceRS flightPriceRS) {

		List<FlightSegmentTO> flightSegments = new ArrayList<FlightSegmentTO>();

		for (OriginDestinationInformationTO originDestinationInformationTO : flightPriceRS.getOriginDestinationInformationList()) {
			for (OriginDestinationOptionTO optionTO : originDestinationInformationTO.getOrignDestinationOptions()) {
				if (optionTO.isSelected()) {
					flightSegments.addAll(optionTO.getFlightSegmentList());
				}
			}
		}

		return flightSegments;
	}

	/**
	 * Transform OTA Air price request to Air Proxy Request
	 * 
	 * @param otaAirPriceRQ
	 * @return
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public FlightPriceRQ createFlightPriceRQ(OTAAirPriceRQ otaAirPriceRQ, UserPrincipal principal) throws ModuleException,
			WebservicesException {
		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();

		Date returnDate = null;
		// TODO - provision for quoting price on a set of descrete collection of related segments without depending on
		// the direction indicator
		if (otaAirPriceRQ.getAirItinerary().getDirectionInd() == null) {
			log.error("ERROR detected in createFlightPriceRQ(OTAAirAvailRQ). " + "Invalid directionIndicator. "
					+ "[directionInd is null]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_REQUIRED_FIELD_MISSING,
					"[Direction Indictor is required]");
		}

		// origin,destination,departure date, return date
		Collection<String> obAirports = new ArrayList<String>();
		Collection<String> bookingClassSet = new HashSet<String>();
		Collection<String> obRPHList;
		Collection<String> ibRPHList;
		String cabinClass = null;
		String defaultLogicalCCCode = null;
		// setting origin destination information
		OriginDestinationInformationTO depatureOnD = flightPriceRQ.addNewOriginDestinationInformation();
		OriginDestinationInformationTO returnOnD = null;
		if (otaAirPriceRQ.getAirItinerary().getDirectionInd() == AirTripType.RETURN) {
			returnOnD = flightPriceRQ.addNewOriginDestinationInformation();
		}

		Collection<String> standByBCList = WebServicesModuleUtils.getBookingClassBD().getStandbyBookingClasses();

		BundledServiceDTO bundledServiceDTO = PriceQuoteUtil.createBundledServiceDTO(otaAirPriceRQ);
		Map<Integer, Set<String>> ondBookingClassSelection = bundledServiceDTO.getOndBookingClasses();
		Map<Integer, Map<String, String>> ondSegBookingClasses = bundledServiceDTO.getOndSegBookingClasses();
		Map<Integer, Integer> ondBundledServices = getOndBundledServiceSelection(otaAirPriceRQ);

		PriceQuoteUtil.overrideFlexiSelection(bundledServiceDTO.getOndFlexiSelection(), otaAirPriceRQ);

		int ondSequence = 0;
		boolean returnSegments = false;

		for (OriginDestinationOptionType originDestinationOption : otaAirPriceRQ.getAirItinerary().getOriginDestinationOptions()
				.getOriginDestinationOption()) {

			
			obRPHList = new ArrayList<String>();
			ibRPHList = new ArrayList<String>();
			for (BookFlightSegmentType bookFlightSegment : originDestinationOption.getFlightSegment()) {

				/** departure date of the first segment of the journey is taken as departure date */
				if (depatureOnD.getDepartureDateTimeStart() == null) {
					depatureOnD.setDepartureDateTimeStart(bookFlightSegment.getDepartureDateTime().toGregorianCalendar()
							.getTime());
				}

				/** first airport is taken as origin */
				if (obAirports.isEmpty()) {
					depatureOnD.setOrigin(bookFlightSegment.getDepartureAirport().getLocationCode());
				}

				if (otaAirPriceRQ.getAirItinerary().getDirectionInd() == AirTripType.RETURN && !returnSegments
						&& CommonUtil.isReturnSegment(obAirports, bookFlightSegment.getDepartureAirport().getLocationCode(),
								bookFlightSegment.getArrivalAirport().getLocationCode())) {
					returnSegments = true;
				}

				/** last station of the outbound journey is taken as the destination airport */
				if (!returnSegments) {
					depatureOnD.setDestination(bookFlightSegment.getArrivalAirport().getLocationCode());
					obRPHList.add(bookFlightSegment.getRPH());
				} else {
					ibRPHList.add(bookFlightSegment.getRPH());
				}

				/** departure date of the first return segment is taken as the return date */
				if (returnSegments && returnDate == null) {
					returnDate = bookFlightSegment.getDepartureDateTime().toGregorianCalendar().getTime();
				}

				if (!returnSegments) {
					obAirports.add(bookFlightSegment.getDepartureAirport().getLocationCode());
					obAirports.add(bookFlightSegment.getArrivalAirport().getLocationCode());
				}

				/** Adding the Booking classes */
				if (bookFlightSegment.getResBookDesigCode() != null) {
					bookingClassSet.add(bookFlightSegment.getResBookDesigCode());
				}
				cabinClass = getCabinClass(bookFlightSegment.getResCabinClass());
				defaultLogicalCCCode = AvailabilitySearchInterlineUtil.getLogicalCabinClass(cabinClass);
				depatureOnD.setPreferredClassOfService(cabinClass);
				depatureOnD.setPreferredLogicalCabin(defaultLogicalCCCode);

				String bookingType = BookingClass.BookingClassType.NORMAL;
				String bookingClass = bookFlightSegment.getResBookDesigCode();

				if (bookingClass != null && !bookingClass.equals("")) {
					if (standByBCList.contains(bookFlightSegment.getResBookDesigCode())) {
						bookingType = BookingClass.BookingClassType.STANDBY;
						bookingClass = null;
					} else {
						cabinClass = WebServicesModuleUtils.getBookingClassBD().getCabinClassForBookingClass(bookingClass);
					}
				}

				if ((cabinClass == null || cabinClass.equals("")) && (bookingClass == null || bookingClass.equals(""))) {
					if (AppSysParamsUtil.getDefaultCOS() != null && !AppSysParamsUtil.getDefaultCOS().isEmpty()) {
						cabinClass = AppSysParamsUtil.getDefaultCOS();
					} else {
						cabinClass = "Y";
					}
				}

				depatureOnD.setPreferredBookingType(bookingType);
				depatureOnD.setPreferredBookingClass(bookingClass);
				depatureOnD.setPreferredClassOfService(cabinClass);
			}

			// we have user specified CC.Shoud match with the no FS
			if (!bookingClassSet.isEmpty() && bookingClassSet.size() != originDestinationOption.getFlightSegment().size()) {
				log.error("ERROR occured in createFlightPriceRQ(OTAAirAvailRQ). "
						+ "Invalid no of booking class. Booking class not included in all ONDs" + "[booking class given="
						+ bookingClassSet.size() + "] [expected = " + originDestinationOption.getFlightSegment().size() + "]");
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
						"Cabin class missing from FlightSegment");
			}

			if (returnDate == null) {
				Date depatureDate = depatureOnD.getDepartureDateTimeStart();
				Date depatureDateTimeStart = CalendarUtil.getStartTimeOfDate(depatureDate);

				Date depatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(depatureDate);

				depatureOnD.setPreferredDate(depatureDate);
				depatureOnD.setDepartureDateTimeStart(depatureDateTimeStart);
				depatureOnD.setDepartureDateTimeEnd(depatureDateTimeEnd);
				depatureOnD.getOrignDestinationOptions().add(setFlightSegmentTo(obRPHList, OndSequence.OUT_BOUND));

				if (ondBundledServices.containsKey(OndSequence.OUT_BOUND) && ondBookingClassSelection != null) {
					Set<String> bookingClasses = ondBookingClassSelection.get(OndSequence.OUT_BOUND);
					Map<String, String> segBookingClasses = ondSegBookingClasses.get(OndSequence.OUT_BOUND);
					if (bookingClasses != null) {
						if (bookingClasses.size() == 1) {
							depatureOnD.setPreferredBookingClass(bookingClasses.iterator().next());
						} else {
							depatureOnD.setSegmentBookingClassSelection(segBookingClasses);
						}
					}

					depatureOnD.setPreferredBundleFarePeriodId(ondBundledServices.get(OndSequence.OUT_BOUND));
				}

			} else {
				// setting the return journey information
				Date returnDateTimeStart = CalendarUtil.getStartTimeOfDate(returnDate);

				Date returnDateTimeEnd = CalendarUtil.getEndTimeOfDate(returnDate);
				returnOnD.setPreferredDate(returnDate);
				returnOnD.setDepartureDateTimeStart(returnDateTimeStart);
				returnOnD.setDepartureDateTimeEnd(returnDateTimeEnd);

				returnOnD.setOrigin(depatureOnD.getDestination());
				returnOnD.setDestination(depatureOnD.getOrigin());
				returnOnD.setReturnFlag(true);

				returnOnD.setPreferredBookingType(depatureOnD.getPreferredBookingType());
				returnOnD.setPreferredBookingClass(depatureOnD.getPreferredBookingClass());

				returnOnD.setPreferredClassOfService(depatureOnD.getPreferredClassOfService());
				returnOnD.setPreferredLogicalCabin(depatureOnD.getPreferredLogicalCabin());
				returnOnD.getOrignDestinationOptions().add(setFlightSegmentTo(ibRPHList, OndSequence.IN_BOUND));

				if (ondBundledServices.containsKey(OndSequence.IN_BOUND) && ondBookingClassSelection != null) {
					Set<String> bookingClasses = ondBookingClassSelection.get(OndSequence.IN_BOUND);
					Map<String, String> segBookingClasses = ondSegBookingClasses.get(OndSequence.IN_BOUND);
					if (bookingClasses != null) {
						if (bookingClasses.size() == 1) {
							returnOnD.setPreferredBookingClass(bookingClasses.iterator().next());
						} else {
							returnOnD.setSegmentBookingClassSelection(segBookingClasses);
						}
					}

					returnOnD.setPreferredBundleFarePeriodId(ondBundledServices.get(OndSequence.IN_BOUND));
				}
			}

			ondSequence++;
		}

		TravelerInfoSummaryType travelerInfoSummaryType = otaAirPriceRQ.getTravelerInfoSummary();
		// Set pax information
		getTravelInfomationSummary(travelerInfoSummaryType, flightPriceRQ);

		SegmentCarrierDTO segmentCarrierDTO = new SegmentCarrierDTO();

		//Uncomment below Code when WebService is Supporting for Dry/Interline
		
		// if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
		// segmentCarrierDTO = CarrierCacheUtil.getInstance().getCarrierDetails(depatureOnD.getOrigin(),
		// depatureOnD.getDestination());
		// } else {
		segmentCarrierDTO.setSystem(SYSTEM.AA);
		// }

		AvailPreferencesTO availPref = flightPriceRQ.getAvailPreferences();
		setAvailPreference(availPref, otaAirPriceRQ, principal, segmentCarrierDTO);

		// one and only one POS is supported
		if (otaAirPriceRQ.getPOS().getSource().size() != 1) {
			log.error("ERROR detected in createFlightPriceRQ(OTAAirAvailRQ). " + "Invalid POS. " + "[POS count="
					+ otaAirPriceRQ.getPOS().getSource().size() + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"[Only one and only POS is supported, Received " + otaAirPriceRQ.getPOS().getSource().size()
							+ " POS records]");
		}

		if (!bookingClassSet.isEmpty() && bookingClassSet.size() > 1) { // we only support booking class per one journey
			log.error("ERROR occured in createFlightPriceRQ(OTAAirAvailRQ). "
					+ "Invalid no of booking class. Booking class shoud be same for all Flight segment per Price Quote"
					+ "[booking class given=" + bookingClassSet.size() + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_TOO_MANY_CLASSES_,
					"Too many cabin classes specifyed .Only one is supported per availability search");
		}

		// Setting Travel preference information such as booking class and booking type
		TravelPreferencesTO travelerPref = flightPriceRQ.getTravelPreferences();
		setTravelPreference(bookingClassSet, travelerPref);

		// TODO set modify reservation information

		List<OriginDestinationInformationTO> originDestinationInformationTOs = flightPriceRQ
				.getOriginDestinationInformationList();

		if (otaAirPriceRQ.getModifiedSegmentInfo() != null) {
			String pnr = otaAirPriceRQ.getModifiedSegmentInfo().getBookingReferenceID().get(0).getID();

			if (pnr != null) { // Availability request for Modify OND
				LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
				pnrModesDTO.setPnr(pnr);
				pnrModesDTO.setLoadFares(true);
				pnrModesDTO.setLoadSegView(true);
				pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
				Reservation reservation = getReservation(pnrModesDTO, null);

				AirItineraryType.OriginDestinationOptions modifiedOND = otaAirPriceRQ.getModifiedSegmentInfo().getAirItinerary()
						.getOriginDestinationOptions();
				WSReservationUtil.setExistingSegInfo(reservation, flightPriceRQ.getOriginDestinationInformationList(),
						extractPNRSegRPHs(reservation, modifiedOND));
				flightPriceRQ.getAvailPreferences().setRequoteFlightSearch(true);
			}
		}

		return flightPriceRQ;
	}
	
	/**
	 * Transform OTA Air price request to Air Proxy Request for MultiCity price quote 
	 * 
	 * @param otaAirPriceRQ
	 * @return FlightPriceRQ
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private FlightPriceRQ createFlightPriceRQForMultiCity(OTAAirPriceRQ otaAirPriceRQ, UserPrincipal principal)
			throws ModuleException, WebservicesException {
		
		FlightPriceRQ flightPriceRQ = new FlightPriceRQ();
		
		SegmentCarrierDTO segmentCarrierDTO = new SegmentCarrierDTO();
		int ondSequence = 0;

		Collection<String> bookingClassSet = new HashSet<String>();
		Collection<String> standByBCList = WebServicesModuleUtils.getBookingClassBD().getStandbyBookingClasses();

		BundledServiceDTO bundledServiceDTO = PriceQuoteUtil.createBundledServiceDTO(otaAirPriceRQ);
		Map<Integer, Set<String>> ondBookingClassSelection = bundledServiceDTO.getOndBookingClasses();
		Map<Integer, Map<String, String>> ondSegBookingClasses = bundledServiceDTO.getOndSegBookingClasses();
		Map<Integer, Integer> ondBundledServices = getOndBundledServiceSelection(otaAirPriceRQ);

		PriceQuoteUtil.overrideFlexiSelection(bundledServiceDTO.getOndFlexiSelection(), otaAirPriceRQ);
		
		List<List<BookFlightSegmentType>> onds = new ArrayList<List<BookFlightSegmentType>>();
		List<BookFlightSegmentType> ond = null;
		
		Date prevSegmentArrival = null;
		GlobalConfig globalConfig = WebServicesModuleUtils.getGlobalConfig();
		String maxConnectingTime;
		Date maxDepartingTime;
		String departureAirport;
		
		for (OriginDestinationOptionType originDestinationOptionType : otaAirPriceRQ.getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption()) {
			BookFlightSegmentType segment = originDestinationOptionType.getFlightSegment().get(0);
			if(prevSegmentArrival != null){
				
				departureAirport = segment.getDepartureAirport().getLocationCode();
				
				if(globalConfig.isHubAirport(departureAirport)){
					
					maxConnectingTime = globalConfig.getMixMaxTransitDurations(departureAirport, null, null)[1];
					maxDepartingTime = getAddedTransitTime(prevSegmentArrival, maxConnectingTime);
					
					if(segment.getDepartureDateTime().toGregorianCalendar().getTime().before(maxDepartingTime)){

						onds.get(onds.size()-1).add(segment);
						
					}else {
						ond = new ArrayList<BookFlightSegmentType>();
						ond.add(segment);
						onds.add(ond);
					}
					
				}else{
					ond = new ArrayList<BookFlightSegmentType>();
					ond.add(segment);
					onds.add(ond);
				}
				
			}else{
				ond = new ArrayList<BookFlightSegmentType>();
				ond.add(segment);
				onds.add(ond);
			}
			
			prevSegmentArrival = segment.getArrivalDateTime().toGregorianCalendar().getTime();
			
		}
		
		
		for (List<BookFlightSegmentType> segments : onds) {
			
			OriginDestinationInformationTO ondInfoTO = flightPriceRQ.addNewOriginDestinationInformation();
			
			for (BookFlightSegmentType bookFlightSegment : segments) {
				
				if(ondInfoTO.getOrigin() == null){
					ondInfoTO.setOrigin(bookFlightSegment.getDepartureAirport().getLocationCode());
					Date depatureDate = bookFlightSegment.getDepartureDateTime().toGregorianCalendar().getTime();
					Date depatureDateTimeStart = CalendarUtil.getStartTimeOfDate(depatureDate);
					Date depatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(depatureDate);

					ondInfoTO.setPreferredDate(depatureDate);
					ondInfoTO.setDepartureDateTimeStart(depatureDateTimeStart);
					ondInfoTO.setDepartureDateTimeEnd(depatureDateTimeEnd);
					
					String bookingType = BookingClass.BookingClassType.NORMAL;
					String bookingClass = bookFlightSegment.getResBookDesigCode();
					
					String cabinClass = getCabinClass(bookFlightSegment.getResCabinClass());
					String defaultLogicalCCCode = AvailabilitySearchInterlineUtil.getLogicalCabinClass(cabinClass);
					ondInfoTO.setPreferredLogicalCabin(defaultLogicalCCCode);
					ondInfoTO.setPreferredBookingClass(bookingClass);
					ondInfoTO.setPreferredBookingType(bookingType);
					
					if (bookingClass != null && !bookingClass.equals("")) {
						cabinClass = WebServicesModuleUtils.getBookingClassBD().getCabinClassForBookingClass(bookingClass);
						if (standByBCList.contains(bookFlightSegment.getResBookDesigCode())) {
							bookingType = BookingClass.BookingClassType.STANDBY;
							bookingClass = null;
						}
					}

					if (cabinClass == null || cabinClass.equals("")) {
						if (AppSysParamsUtil.getDefaultCOS() != null && !AppSysParamsUtil.getDefaultCOS().isEmpty()) {
							cabinClass = AppSysParamsUtil.getDefaultCOS();
						} else {
							cabinClass = "Y";
						}
					}

					ondInfoTO.setPreferredBookingType(bookingType);
					ondInfoTO.setPreferredBookingClass(bookingClass);
					ondInfoTO.setPreferredClassOfService(cabinClass);
					
				}
				
				ondInfoTO.setDestination(bookFlightSegment.getArrivalAirport().getLocationCode());
				
				/** Adding the Booking classes */
				if (bookFlightSegment.getResBookDesigCode() != null) {
					bookingClassSet.add(bookFlightSegment.getResBookDesigCode());
				}
								
				if (ondBundledServices.containsKey(ondSequence) && ondBookingClassSelection != null) {
					Set<String> bookingClasses = ondBookingClassSelection.get(ondSequence);
					Map<String, String> segBookingClasses = ondSegBookingClasses.get(ondSequence);
					if (bookingClasses != null) {
						if (bookingClasses.size() == 1) {
							ondInfoTO.setPreferredBookingClass(bookingClasses.iterator().next());
						} else {
							ondInfoTO.setSegmentBookingClassSelection(segBookingClasses);
						}
					}

					ondInfoTO.setPreferredBundleFarePeriodId(ondBundledServices.get(ondSequence));
				}
				
				OriginDestinationOptionTO ondOptionTO = new OriginDestinationOptionTO();
				FlightSegmentTO flightSegementTO = new FlightSegmentTO();
				String rph = bookFlightSegment.getRPH();
				if (rph.indexOf("#") != -1) {
					String arr[] = rph.split("#");
					flightSegementTO.setFlightRefNumber(arr[0]);
					flightSegementTO.setRouteRefNumber(arr[1]);
					flightSegementTO.setSegmentCode(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(arr[0]));
					flightSegementTO.setFlightNumber(FlightRefNumberUtil.getOperatingAirline(arr[0]));
					flightSegementTO.setOperatingAirline(FlightRefNumberUtil.getOperatingAirline(arr[0]));
				} else {
					flightSegementTO.setFlightRefNumber(bookFlightSegment.getRPH());
					flightSegementTO.setSegmentCode(FlightRefNumberUtil.getSegmentCodeFromFlightRPH(rph));
					flightSegementTO.setFlightNumber(FlightRefNumberUtil.getOperatingAirline(rph));
					flightSegementTO.setOperatingAirline(FlightRefNumberUtil.getOperatingAirline(rph));
				}
				
				ondOptionTO.getFlightSegmentList().add(flightSegementTO);
				ondInfoTO.getOrignDestinationOptions().add(ondOptionTO);

				if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
					segmentCarrierDTO = CarrierCacheUtil.getInstance().getCarrierDetails(ondInfoTO.getOrigin(),
							ondInfoTO.getDestination());
				} else {
					segmentCarrierDTO.setSystem(SYSTEM.AA);
				}
				
				ondSequence++;

			}
			
			// we have user specified CC.Shoud match with the no FS
			if (!bookingClassSet.isEmpty() && bookingClassSet.size() != segments.size()) {
				log.error("ERROR occured in createFlightPriceRQ(OTAAirAvailRQ). "
						+ "Invalid no of booking class. Booking class not included in all ONDs" + "[booking class given="
						+ bookingClassSet.size() + "] [expected = " + segments.size() + "]");
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
						"Cabin class missing from FlightSegment");
			}

		}
		
		TravelerInfoSummaryType travelerInfoSummaryType = otaAirPriceRQ.getTravelerInfoSummary();
		// Set pax information
		getTravelInfomationSummary(travelerInfoSummaryType, flightPriceRQ);

		AvailPreferencesTO availPref = flightPriceRQ.getAvailPreferences();
		setAvailPreference(availPref, otaAirPriceRQ, principal, segmentCarrierDTO);

		// one and only one POS is supported
		if (otaAirPriceRQ.getPOS().getSource().size() != 1) {
			log.error("ERROR detected in createFlightPriceRQ(OTAAirAvailRQ). " + "Invalid POS. " + "[POS count="
					+ otaAirPriceRQ.getPOS().getSource().size() + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"[Only one and only POS is supported, Received " + otaAirPriceRQ.getPOS().getSource().size()
							+ " POS records]");
		}

		if (!bookingClassSet.isEmpty() && bookingClassSet.size() > 1) { // we only support booking class per one journey
			log.error("ERROR occured in createFlightPriceRQ(OTAAirAvailRQ). "
					+ "Invalid no of booking class. Booking class shoud be same for all Flight segment per Price Quote"
					+ "[booking class given=" + bookingClassSet.size() + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_TOO_MANY_CLASSES_,
					"Too many cabin classes specifyed .Only one is supported per availability search");
		}

		TravelPreferencesTO travelerPref = flightPriceRQ.getTravelPreferences();
		setTravelPreference(bookingClassSet, travelerPref);

		return flightPriceRQ;
	}

	/**
	 * Extracts pnrSegIds for cancellation/modification from the request.
	 * 
	 * @param reservation
	 * @param otaAirBookModifyRQ
	 * @return
	 * @throws WebservicesException
	 */
	private static Map<Integer, String>
			extractPNRSegRPHs(Reservation reservation, AirItineraryType.OriginDestinationOptions onds)
					throws WebservicesException {

		OriginDestinationOptionType ond = onds.getOriginDestinationOption().get(0);
		int ondGroupCode = -1;
		Map<Integer, String> modifyingPnrSegRPHs = new HashMap<Integer, String>();
		for (BookFlightSegmentType wsFlightSeg : ond.getFlightSegment()) {
			for (Iterator aaFlightSeg = reservation.getSegments().iterator(); aaFlightSeg.hasNext();) {
				ReservationSegment resSegment = (ReservationSegment) aaFlightSeg.next();
				Integer pnrSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(wsFlightSeg.getRPH());
				if (resSegment.getPnrSegId().equals(pnrSegId)) {
					modifyingPnrSegRPHs.put(pnrSegId, wsFlightSeg.getRPH());
					// modifyingPnrSegRPHs.add(wsFlightSeg.getRPH());
					if (ondGroupCode == -1) {
						ondGroupCode = resSegment.getOndGroupId();
					} else if (ondGroupCode != resSegment.getOndGroupId()) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Only one OND at a time allowed to cancel");
					}
				}
			}
		}
		return modifyingPnrSegRPHs;
	}

	/**
	 * Method to get the reservation.
	 * 
	 * @param pnrModesDTO
	 * @param trackingInfoDTO
	 * @return
	 * @throws Exception
	 */
	private static Reservation getReservation(LCCClientPnrModesDTO pnrModesDTO, TrackInfoDTO trackingInfoDTO)
			throws ModuleException, WebservicesException {
		Reservation reservation = null;
		try {
			reservation = WPModuleUtils.getReservationBD().getReservation(pnrModesDTO, trackingInfoDTO);
			if (reservation == null) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_BOOKING_REFERENCE_NOT_FOUND_,
						"No matching booking found");
			}
		} catch (ModuleException ex) {
			throw ex;
		}
		return reservation;
	}

	/**
	 * Get Travel information summary details (Pax type and count details)
	 * 
	 * @param infoSummaryType
	 * @return
	 * @throws ModuleException 
	 */
	private TravelerInfoSummaryTO getTravelInfomationSummary(TravelerInfoSummaryType travelerInfoSummaryType,
			FlightPriceRQ flightPriceRQ) throws WebservicesException, ModuleException {
		int adults = 0;
		int children = 0;
		int infants = 0;

		for (TravelerInformationType travelerInformationType : travelerInfoSummaryType.getAirTravelerAvail()) {
			for (PassengerTypeQuantityType passengerTypeQuantityType : travelerInformationType.getPassengerTypeQuantity()) {
				if (passengerTypeQuantityType.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
					adults += passengerTypeQuantityType.getQuantity();
				} else if (passengerTypeQuantityType.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
					children += passengerTypeQuantityType.getQuantity();
				} else if (passengerTypeQuantityType.getCode().equals(
						CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
					infants += passengerTypeQuantityType.getQuantity();
				}
			}
		}

		if (adults == 0 && children == 0) {
			log.error("ERROR occured in createFlightPriceRQ(OTAAirAvailRQ). " + "Invalid adults/children pax count. "
					+ "[adults pax=" + adults + "]" + "[children pax=" + children + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AT_LEAST_ONE_ADULT_MUST_BE_INCLUDED_,
					"Adult pax count cannot be zero");
		}
		
		if ((adults) > WebplatformUtil.getMaxAdultCount(ThreadLocalData.getCurrentUserPrincipal().getUserId() , "WS")) {
			log.error("ERROR occured in createFlightAvailRQ(OTAAirAvailRQ). " + "Maximum adult count exceeded "
					+ "[adults pax=" + adults + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_NUMBER_OF_ADULTS,
					"Maximum Adult count exceeded");
		}

		if (infants > adults) {
			log.error("ERROR occured in createFlightPriceRQ(OTAAirAvailRQ). " + "Infants cannot be more than adults. "
					+ "[adults pax=" + adults + ", infant pax =" + infants + "]");
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_NUMBER_OF_ADULTS,
					"Infants cannot be more than adults");
		}

		TravelerInfoSummaryTO traverlerInfo = flightPriceRQ.getTravelerInfoSummary();

		PassengerTypeQuantityTO adultsQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		adultsQuantity.setPassengerType(PaxTypeTO.ADULT);
		adultsQuantity.setQuantity(adults);

		PassengerTypeQuantityTO childQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		childQuantity.setPassengerType(PaxTypeTO.CHILD);
		childQuantity.setQuantity(children);

		PassengerTypeQuantityTO infantQuantity = traverlerInfo.addNewPassengerTypeQuantityTO();
		infantQuantity.setPassengerType(PaxTypeTO.INFANT);
		infantQuantity.setQuantity(infants);

		return traverlerInfo;

	}

	/**
	 * 
	 * @param bookingClassSet
	 * @param travelPreferencesTO
	 * @throws ModuleException
	 */
	public static void setTravelPreference(Collection<String> bookingClassSet, TravelPreferencesTO travelPreferencesTO)
			throws ModuleException {

		String bookingClass = BeanUtils.getFirstElement(bookingClassSet);
		String bookingType = BookingClass.BookingClassType.NORMAL;
		if (bookingClass != null) {
			travelPreferencesTO.setBookingClassCode(bookingClass);
			Collection<String> standByBCList = WebServicesModuleUtils.getBookingClassBD().getStandbyBookingClasses();
			if (standByBCList.contains(bookingClass)) {
				bookingType = BookingClass.BookingClassType.STANDBY;
			}
		}

		travelPreferencesTO.setBookingType(bookingType);
	}

	/**
	 * 
	 * @param cosPref
	 * @param cabinClass
	 */
	public static void setClassOfServicePreference(ClassOfServicePreferencesTO cosPref, String cabinClass) {

		// defaulting to defaultCOS if no CC is specified.
		if (cabinClass == null) {
			if (AppSysParamsUtil.getDefaultCOS() != null && !AppSysParamsUtil.getDefaultCOS().isEmpty()) {
				cabinClass = AppSysParamsUtil.getDefaultCOS();
			} else {
				cabinClass = "Y";
			}
		}

		cosPref.getCabinClassSelection().put(cabinClass, null);

		String defaultLogicalCCCode = null;
		if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
			for (CabinClassWiseDefaultLogicalCCDetailDTO defaultLogicalCCDetailDTO : CommonsServices.getGlobalConfig()
					.getDefaultLogicalCCDetails()) {
				if (cabinClass != null && cabinClass.equalsIgnoreCase(defaultLogicalCCDetailDTO.getCcCode())) {
					defaultLogicalCCCode = defaultLogicalCCDetailDTO.getDefaultLogicalCCCode();
					break;
				}
			}

		}

		if (defaultLogicalCCCode != null && !"".equals(defaultLogicalCCCode)) {
			cosPref.getLogicalCabinClassSelection().put(defaultLogicalCCCode, null);
		}

	}

	/**
	 * 
	 * @param cabinClass
	 */
	public static String getCabinClass(String cabinClass) {
		// defaulting to defaultCOS if no CC is specified.
		if (cabinClass == null) {
			if (AppSysParamsUtil.getDefaultCOS() != null && !AppSysParamsUtil.getDefaultCOS().isEmpty()) {
				cabinClass = AppSysParamsUtil.getDefaultCOS();
			} else {
				cabinClass = "Y";
			}
		}
		return cabinClass;
	}

	/**
	 * 
	 * @param availPreferencesTO
	 * @param otaAirPriceRQ
	 * @throws ModuleException
	 */
	public void setAvailPreference(AvailPreferencesTO availPreferencesTO, OTAAirPriceRQ otaAirPriceRQ, UserPrincipal principal,
			SegmentCarrierDTO segmentCarrierDTO) throws ModuleException {

		TravelerInfoSummaryType travelerInfoSummaryType = otaAirPriceRQ.getTravelerInfoSummary();

		if (travelerInfoSummaryType.getPriceRequestInformation() != null) {
			availPreferencesTO.setPreferredCurrency(travelerInfoSummaryType.getPriceRequestInformation().getCurrencyCode());
		}

		availPreferencesTO.setTravelAgentCode(principal.getAgentCode());
		availPreferencesTO.setRestrictionLevel(AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED);

		if (otaAirPriceRQ.getAirItinerary().getDirectionInd() == AirTripType.RETURN) {
			availPreferencesTO.setHalfReturnFareQuote(AppSysParamsUtil.isAllowHalfReturnFaresForNewBookings());
		}

		// If the Flexi fare is selected need to set the flexi quoting.
		if (otaAirPriceRQ.getFlexiFareSelectionOptions() != null) {
			FlexiFareSelectionOptions flexiOptions = otaAirPriceRQ.getFlexiFareSelectionOptions();

			availPreferencesTO.setQuoteOndFlexi(OndSequence.IN_BOUND, flexiOptions.getInBoundFlexiSelected() != null
					? flexiOptions.getInBoundFlexiSelected()
					: false);
			availPreferencesTO.setQuoteOndFlexi(OndSequence.OUT_BOUND, flexiOptions.getOutBoundFlexiSelected() != null
					? flexiOptions.getOutBoundFlexiSelected()
					: false);
		} else {
			availPreferencesTO.setQuoteOndFlexi(OndSequence.IN_BOUND, false);
			availPreferencesTO.setQuoteOndFlexi(OndSequence.OUT_BOUND, false);
		}

		if (isCharterAgent(principal)) {
			availPreferencesTO.setFixedFareAgent(true);
		}

		availPreferencesTO.setQuoteFares(true);

		// TODO find which system to use based on OND info
		availPreferencesTO.setSearchSystem(segmentCarrierDTO.getSystem());

		availPreferencesTO.setAppIndicator(ApplicationEngine.WS);
	}

	/**
	 * 
	 * @param rphList
	 * @param ondSequence
	 * @return
	 */
	public static OriginDestinationOptionTO setFlightSegmentTo(Collection<String> rphList, int ondSequence) {
		OriginDestinationOptionTO originDestinationOptionTO = null;
		if (rphList != null) {
			originDestinationOptionTO = new OriginDestinationOptionTO();

			FlightSegmentTO flightSegTO = null;

			for (String rph : rphList) {
				flightSegTO = new FlightSegmentTO();
				flightSegTO.setFlightRefNumber(rph);
				flightSegTO.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(rph));
				flightSegTO.setOndSequence(ondSequence);
				originDestinationOptionTO.getFlightSegmentList().add(flightSegTO);
			}
		}
		return originDestinationOptionTO;
	}

	/**
	 * Clears the {@link FareSegChargeTO} from the user Transaction.
	 */
	public static void clearFareSegmentChargeTO() {
		ThreadLocalData.setCurrentTnxParam(Transaction.FARE_SEGMENT_CHARGE_TO, null);
	}

	public static void clearTotalPrice() {
		ThreadLocalData.setCurrentTnxParam(Transaction.RES_SERVICE_TOTAL_PRICE, null);
	}

	/**
	 * Sets the inbound flexi selection to false;
	 */
	public static void clearInBoundFlexiSelection() {
		ThreadLocalData.setCurrentTnxParam(Transaction.RES_INBOUND_FLEXI_SELECTED, new Boolean(false));
	}

	/**
	 * Sets the outbound flexi selection to false;
	 */
	public static void clearOutBoundFlexiSelection() {
		ThreadLocalData.setCurrentTnxParam(Transaction.RES_OUTBOUND_FLEXI_SELECTED, new Boolean(false));
	}

	public static void clearSelectedInsurance() {
		ThreadLocalData.setCurrentTnxParam(Transaction.RES_INSURANCE_SELECTED, null);
	}

	public static void clearQuotedExternalCharges() {
		Map<String, List<ExternalChargeTO>> quotedExternalCharges = (Map<String, List<ExternalChargeTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);

		if (quotedExternalCharges != null) {
			quotedExternalCharges.clear();
			ThreadLocalData.setCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES, null);
		}

	}

	public static void clearQuotedInsurance() {
		ThreadLocalData.setCurrentTnxParam(Transaction.QUOTED_INSURANCE, null);
	}

	public static void clearQuotedPriceInfo() {
		ThreadLocalData.setCurrentTnxParam(Transaction.QUOTED_PRICE_INFO, null);
	}

	public static void clearPriceInfo() {
		ThreadLocalData.setCurrentTnxParam(Transaction.PRICE_INFO, null);
	}

	public static void clearReturnPointAirport() {
		ThreadLocalData.setCurrentTnxParam(Transaction.RETURN_POINT_AIRPORT, null);
	}
	
	public static void clearAvailableOndBundledFare() {
		ThreadLocalData.setCurrentTnxParam(Transaction.AVAILABLE_OND_BUNDLED_FARES, null);
	}
	
	public static void clearQuotedBundled() {
		ThreadLocalData.setCurrentTnxParam(Transaction.PRICE_QUOTED_BUNDLED, null);
	}
	
	/**
	 * Clears the price quoting related tnx params.
	 */
	public static void clearPriceQuoteRelatedTransactionParams() {
		PriceQuoteInterlineUtil.clearFareSegmentChargeTO();
		PriceQuoteInterlineUtil.clearInBoundFlexiSelection();
		PriceQuoteInterlineUtil.clearOutBoundFlexiSelection();
		PriceQuoteInterlineUtil.clearQuotedExternalCharges();
		PriceQuoteInterlineUtil.clearSelectedInsurance();
		PriceQuoteInterlineUtil.clearQuotedPriceInfo();
		PriceQuoteInterlineUtil.clearPriceInfo();
		PriceQuoteInterlineUtil.clearReturnPointAirport();
		PriceQuoteInterlineUtil.clearAvailableOndBundledFare();
		clearQuotedBundled();
	}

	/**
	 * 
	 * @param otaAirPriceRS
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	private StringBuffer getriceQuoteResultsSummary(OTAAirPriceRS otaAirPriceRS) throws WebservicesException, ModuleException {
		StringBuffer priceQuoteSummary = new StringBuffer();
		String nl = "\n\r \t";
		for (Object item : otaAirPriceRS.getSuccessAndWarningsAndPricedItineraries()) {
			if (item instanceof PricedItinerariesType) {
				for (PricedItineraryType pricedItineraryType : ((PricedItinerariesType) item).getPricedItinerary()) {
					for (OriginDestinationOptionType originDestinationOption : pricedItineraryType.getAirItinerary()
							.getOriginDestinationOptions().getOriginDestinationOption()) {
						for (BookFlightSegmentType bookFlightSegment : originDestinationOption.getFlightSegment()) {
							priceQuoteSummary.append(nl + "Segment Info [fltNumber=" + bookFlightSegment.getFlightNumber());
							priceQuoteSummary.append(",Origin=" + bookFlightSegment.getDepartureAirport().getLocationCode());
							priceQuoteSummary.append(",Destination=" + bookFlightSegment.getArrivalAirport().getLocationCode());
							priceQuoteSummary.append(",DepDateTime="
									+ CommonUtil.getFormattedDate(bookFlightSegment.getDepartureDateTime().toGregorianCalendar()
											.getTime()));
							priceQuoteSummary.append(",ArrDepDateTime="
									+ CommonUtil.getFormattedDate(bookFlightSegment.getArrivalDateTime().toGregorianCalendar()
											.getTime()) + "]");
						}
					}

					AirItineraryPricingInfoType airItineraryPricingInfo = pricedItineraryType.getAirItineraryPricingInfo();
					FareType itinPrice = airItineraryPricingInfo.getItinTotalFare();
					BaseFare baseFare = itinPrice.getBaseFare();
					priceQuoteSummary.append(nl + "Fare Info [total base fare=" + baseFare.getAmount() + ",currency="
							+ baseFare.getCurrencyCode() + "]");

					FareType.TotalFare totalFare = itinPrice.getTotalFare();
					priceQuoteSummary.append(nl + "Total Pricing Info [total price=" + totalFare.getAmount() + ",currency="
							+ totalFare.getCurrencyCode() + "]");

					for (PTCFareBreakdownType ptcFareBreakdownType : airItineraryPricingInfo.getPTCFareBreakdowns()
							.getPTCFareBreakdown()) {
						priceQuoteSummary.append(nl + "Pax info [type="
								+ ptcFareBreakdownType.getPassengerTypeQuantity().getCode() + ",quantiry="
								+ ptcFareBreakdownType.getPassengerTypeQuantity().getQuantity() + "]");
						for (AirTaxType airTaxType : ptcFareBreakdownType.getPassengerFare().getTaxes().getTax()) {
							priceQuoteSummary.append(nl + "TAX Info [amount=" + airTaxType.getAmount() + ",currency="
									+ airTaxType.getCurrencyCode() + ",code=" + airTaxType.getTaxCode() + ",description="
									+ airTaxType.getTaxName() + "]");
						}
						for (AirFeeType airFeeType : ptcFareBreakdownType.getPassengerFare().getFees().getFee()) {
							priceQuoteSummary.append(nl + "SURCHARGE Info [amount=" + airFeeType.getAmount() + ",currency="
									+ airFeeType.getCurrencyCode() + ",code=" + airFeeType.getFeeCode() + "]");
						}
					}
				}
			}
		}

		return priceQuoteSummary;
	}

	/**
	 * Stores the total price in the transaction to be compare with the booking price quart.
	 * 
	 * @param pricedItinerary
	 */
	public static void storeTotalPrice(PricedItineraryType pricedItinerary) {
		BigDecimal totalPrice = (BigDecimal) ThreadLocalData.getCurrentTnxParam(Transaction.RES_SERVICE_TOTAL_PRICE);

		if (totalPrice != null) {
			totalPrice = AccelAeroCalculator.add(totalPrice, pricedItinerary.getAirItineraryPricingInfo().getItinTotalFare()
					.getTotalFare().getAmount());
		} else {
			totalPrice = pricedItinerary.getAirItineraryPricingInfo().getItinTotalFare().getTotalFare().getAmount();
		}

		ThreadLocalData.setCurrentTnxParam(Transaction.RES_SERVICE_TOTAL_PRICE, totalPrice);
	}

	private Map<String, Map<String, String>> getFlightDetailsWithSeats(OTAAirPriceRQ otaAirPriceRQ) throws WebservicesException {
		// key|value - traveler RPH | Map<Integer, String>
		// Map<String, String> - key|value - flight RPH | seat number
		Map<String, Map<String, String>> flightDetailsAndSeatsMap = new HashMap<String, Map<String, String>>();

		Map<String, List<String>> flightSelSeatNos = new HashMap<String, List<String>>();
		for (SpecialReqDetailsType specialReqDetailsType : otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
			SeatRequests seatRequests = specialReqDetailsType.getSeatRequests();
			for (SeatRequest seatRequest : seatRequests.getSeatRequest()) {
				List<String> selectedSeatNos = null;
				String travelerRef = seatRequest.getTravelerRefNumberRPHList().get(0);
				String flightRefRPH = seatRequest.getFlightRefNumberRPHList().get(0);
				if (flightRefRPH == null || flightRefRPH.equals("")) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, " Invalid Flight Ref RPH Value ");
				}

				if (flightSelSeatNos.containsKey(flightRefRPH)) {
					selectedSeatNos = flightSelSeatNos.get(flightRefRPH);
				} else {
					selectedSeatNos = new ArrayList<String>();
					flightSelSeatNos.put(flightRefRPH, selectedSeatNos);
				}

				// Infant RPH must not be with seat request
				if (travelerRef.startsWith("I")) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PASSENGER_TYPE_NOT_SUPPORTED_,
							" Seat is not available for infants");
				}
				String seatNo = seatRequest.getSeatNumber();
				// validating seat selection duplicates
				if (selectedSeatNos.contains(seatNo)) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
							" One seat can only be selected for one passenger ");
				} else {
					selectedSeatNos.add(seatNo);
				}
				// Integer flightSegmentID =
				// FlightRefNumberUtil.getSegmentIdFromFlightRPH(seatRequest.getFlightRefNumberRPHList()
				// .get(0));

				if (flightDetailsAndSeatsMap.get(travelerRef) == null) {
					flightDetailsAndSeatsMap.put(travelerRef, new HashMap<String, String>());
				}
				flightDetailsAndSeatsMap.get(travelerRef).put(seatRequest.getFlightRefNumberRPHList().get(0), seatNo);
			}
		}

		return flightDetailsAndSeatsMap;
	}

	/**
	 * 
	 * @param otaAirPriceRQ
	 * @return
	 */
	private boolean isSeatSelected(OTAAirPriceRQ otaAirPriceRQ) {
		boolean isSeatSelected = false;
		if (otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails() != null) {
			for (SpecialReqDetailsType specialReqDetailsType : otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
				if (specialReqDetailsType != null && specialReqDetailsType.getSeatRequests() != null
						&& specialReqDetailsType.getSeatRequests().getSeatRequest() != null
						&& specialReqDetailsType.getSeatRequests().getSeatRequest().size() > 0) {
					isSeatSelected = true;
				}
			}
		}

		return isSeatSelected;
	}

	/**
	 * This ideally should be List<? extends LCCClientExternalChgDTO> externalCharges, but for some reason, generic is
	 * not working when I try to add a collection of ? extends LCCClientExternalChgDTO to the map, so I am removing
	 * Generic from this
	 * 
	 * @param travelerRef
	 * @param externalCharges
	 */
	public static void addExternalCharge(String travelerRef, List externalCharges) {

		Map<String, List<? extends LCCClientExternalChgDTO>> quotedExternalCharges = (Map<String, List<? extends LCCClientExternalChgDTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);

		if (quotedExternalCharges == null) {
			quotedExternalCharges = new HashMap<String, List<? extends LCCClientExternalChgDTO>>();
			quotedExternalCharges.put(travelerRef, externalCharges);
		} else {
			if (quotedExternalCharges.get(travelerRef) == null) {
				quotedExternalCharges.put(travelerRef, new ArrayList<LCCClientExternalChgDTO>());
			}

			quotedExternalCharges.get(travelerRef).addAll(externalCharges);
		}

		ThreadLocalData.setCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES, quotedExternalCharges);
	}

	private boolean isMealsSelected(OTAAirPriceRQ otaAirPriceRQ) {
		boolean isMealsSelected = false;
		if (otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails() != null) {
			for (SpecialReqDetailsType specialReqDetailsType : otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
				if (specialReqDetailsType != null && specialReqDetailsType.getMealRequests() != null
						&& specialReqDetailsType.getMealRequests().getMealRequest() != null
						&& specialReqDetailsType.getMealRequests().getMealRequest().size() > 0) {
					isMealsSelected = true;
				}
			}
		}

		return isMealsSelected;
	}
	
	private boolean isAutomaticCheckinsSelected(OTAAirPriceRQ otaAirPriceRQ) {
		boolean isAutomaticCheckinsSelected = false;
		if (otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails() != null) {
			for (SpecialReqDetailsType specialReqDetailsType : otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
				if (specialReqDetailsType != null && specialReqDetailsType.getAutomaticCheckinRequests() != null
						&& specialReqDetailsType.getAutomaticCheckinRequests().getAutomaticCheckinRequest() != null
						&& specialReqDetailsType.getAutomaticCheckinRequests().getAutomaticCheckinRequest().size() > 0) {
					isAutomaticCheckinsSelected = true;
				}
			}
		}

		return isAutomaticCheckinsSelected;
	}
	
	private Map<String, Map<String, String>> getFlightDetailsWithAutomaticCheckins(OTAAirPriceRQ otaAirPriceRQ)
			throws WebservicesException {
		Map<String, Map<String, String>> flightDetailsWithAutomaticCheckins = new HashMap<String, Map<String, String>>();

		for (SpecialReqDetailsType specialReqDetailsType : otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
			AutomaticCheckinRequests autoCheckinRequests = specialReqDetailsType.getAutomaticCheckinRequests();
			for (AutomaticCheckinRequest autoCheckinRequest : autoCheckinRequests.getAutomaticCheckinRequest()) {
				String travelerRef = autoCheckinRequest.getTravelerRefNumberRPHList().get(0);
				if (travelerRef.startsWith("I")) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PASSENGER_TYPE_NOT_SUPPORTED_,
							" AutoChekins are not available for infants");
				}
				if (flightDetailsWithAutomaticCheckins.get(travelerRef) == null) {
					flightDetailsWithAutomaticCheckins.put(travelerRef, new HashMap<String, String>());
				}
				flightDetailsWithAutomaticCheckins.get(travelerRef).put(autoCheckinRequest.getFlightRefNumberRPHList().get(0),
						autoCheckinRequest.getFlightRefNumberRPHList().get(0));
			}
		}

		return flightDetailsWithAutomaticCheckins;
	}
	
	private Map<String, Map<String, List<String>>> getFlightDetailsWithMeals(OTAAirPriceRQ otaAirPriceRQ)
			throws WebservicesException {
		// This structure is using since multi meal support can also there, in that case one traveler can have
		// many meals with different quantity
		// TraverlerRef -> FlightSegment -> Meal(s)
		Map<String, Map<String, List<String>>> flightDetailsWithMeals = new HashMap<String, Map<String, List<String>>>();

		for (SpecialReqDetailsType specialReqDetailsType : otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
			MealRequests mealRequests = specialReqDetailsType.getMealRequests();
			for (MealRequest mealRequest : mealRequests.getMealRequest()) {
				String travelerRef = mealRequest.getTravelerRefNumberRPHList().get(0);
				// Availability response is having flight segment id as RPH and we expect client will send that id with
				// mealrequest as well.
				// Integer flightSegId =
				// FlightRefNumberUtil.getSegmentIdFromFlightRPH(mealRequest.getFlightRefNumberRPHList()
				// .get(0));
				if (travelerRef.startsWith("I")) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PASSENGER_TYPE_NOT_SUPPORTED_,
							" Meals are not available for infants");
				}

				String flightRPH = mealRequest.getFlightRefNumberRPHList().get(0);

				Integer mealQuantity = 1;
				if (mealRequest.getMealQuantity() > 1) {
					mealQuantity = mealRequest.getMealQuantity();
				}
				for (int index = 0; index < mealQuantity; index++) {
					if (flightDetailsWithMeals.get(travelerRef) == null) {
						flightDetailsWithMeals.put(travelerRef, new HashMap<String, List<String>>());
						if (flightDetailsWithMeals.get(travelerRef).get(flightRPH) == null) {
							flightDetailsWithMeals.get(travelerRef).put(flightRPH, new ArrayList<String>());
						}
					} else {
						if (flightDetailsWithMeals.get(travelerRef).get(flightRPH) == null) {
							flightDetailsWithMeals.get(travelerRef).put(flightRPH, new ArrayList<String>());
						}
					}

					flightDetailsWithMeals.get(travelerRef).get(flightRPH).add(mealRequest.getMealCode());
				}
			}
		}

		return flightDetailsWithMeals;
	}

	private boolean isSSRSelected(OTAAirPriceRQ otaAirPriceRQ) {
		boolean isSSRSelected = false;
		if (otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails() != null) {
			for (SpecialReqDetailsType specialReqDetailsType : otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
				if (specialReqDetailsType != null && specialReqDetailsType.getSSRRequests() != null
						&& specialReqDetailsType.getSSRRequests().getSSRRequest() != null
						&& specialReqDetailsType.getSSRRequests().getSSRRequest().size() > 0) {
					isSSRSelected = true;
				}
			}
		}

		return isSSRSelected;
	}

	/**
	 * This method will return traveler selected SSR(s) [Both In-flight Services & Airport Services]. Outer Map contains
	 * {@link SSRType.AIRPORT} & {@link SSRType.INFLIGHT} as keys.
	 * 
	 * @throws ModuleException
	 * 
	 */
	private Map<SSRType, Map<String, Map<String, List<HashMap<String, String>>>>> getFlightDetailsWithSSRs(
			OTAAirPriceRQ otaAirPriceRQ) throws WebservicesException {

		Map<SSRType, Map<String, Map<String, List<HashMap<String, String>>>>> serviceWiseflightDetailsWithSSRs = new HashMap<SSRType, Map<String, Map<String, List<HashMap<String, String>>>>>();
		serviceWiseflightDetailsWithSSRs.put(SSRType.AIRPORT, new HashMap<String, Map<String, List<HashMap<String, String>>>>());
		serviceWiseflightDetailsWithSSRs.put(SSRType.INFLIGHT, new HashMap<String, Map<String, List<HashMap<String, String>>>>());

		for (SpecialReqDetailsType specialReqDetailsType : otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
			SSRRequests ssrRequests = specialReqDetailsType.getSSRRequests();
			List<String> flightRPHList = new ArrayList<String>();

			for (SSRRequest ssrRequest : ssrRequests.getSSRRequest()) {

				// If user didn't provide service type, consider it as in-flight service
				if (ssrRequest.getServiceType() == null) {
					ssrRequest.setServiceType(SSRType.INFLIGHT);
				}

				if (ssrRequest.getServiceType() == SSRType.INFLIGHT || ssrRequest.getServiceType() == SSRType.USERDEFINED) {
					Map<String, Map<String, List<HashMap<String, String>>>> flightDetailsWithSSRs = serviceWiseflightDetailsWithSSRs
							.get(SSRType.INFLIGHT);

					String travelerRef = ssrRequest.getTravelerRefNumberRPHList().get(0);

					// Integer flightSegId =
					// FlightRefNumberUtil.getSegmentIdFromFlightRPH(ssrRequest.getFlightRefNumberRPHList()
					// .get(0));

					String flightRPH = ssrRequest.getFlightRefNumberRPHList().get(0);

					if (travelerRef.startsWith("I")) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PASSENGER_TYPE_NOT_SUPPORTED_,
								" In-flight Services are not available for Infants");
					}

					if (flightDetailsWithSSRs.get(travelerRef) == null) {
						flightDetailsWithSSRs.put(travelerRef, new HashMap<String, List<HashMap<String, String>>>());
						if (flightDetailsWithSSRs.get(travelerRef).get(flightRPH) == null) {
							flightDetailsWithSSRs.get(travelerRef).put(flightRPH, new ArrayList<HashMap<String, String>>());
						}
					} else {
						if (flightDetailsWithSSRs.get(travelerRef).get(flightRPH) == null) {
							flightDetailsWithSSRs.get(travelerRef).put(flightRPH, new ArrayList<HashMap<String, String>>());
						} else {
							ArrayList<HashMap<String, String>> sMap = (ArrayList<HashMap<String, String>>) flightDetailsWithSSRs
									.get(travelerRef).get(flightRPH);
							Iterator<HashMap<String, String>> itr = sMap.iterator();
							while (itr.hasNext()) {
								HashMap<String, String> tempMap = itr.next();

								if (ssrRequest.getSsrCode() == null) {
									throw new WebservicesException(
											IOTACodeTables.AccelaeroErrorCodes_AAE_SSR_ALREADY_ADDED_FOR_THE_PASSENGER_,
											" Same In-flight Services request for the Passenger " + travelerRef);
								}

								if (tempMap.get("SSR_CODE").equals(ssrRequest.getSsrCode())) {
									throw new WebservicesException(
											IOTACodeTables.AccelaeroErrorCodes_AAE_SSR_ALREADY_ADDED_FOR_THE_PASSENGER_,
											" Same In-flight Services request for the Passenger " + travelerRef);
								}

							}
						}
					}

					HashMap<String, String> ssrMap = new HashMap<String, String>();
					ssrMap.put("SSR_CODE", ssrRequest.getSsrCode());
					if (ssrRequest.getServiceType() == SSRType.USERDEFINED) {
						ssrMap.put("USER_DEFINED_SSR", "Y");
					}					

					flightDetailsWithSSRs.get(travelerRef).get(flightRPH).add(ssrMap);

					flightRPHList.add(ssrRequest.getFlightRefNumberRPHList().get(0));

				} else if (ssrRequest.getServiceType() == SSRType.AIRPORT) {

					Map<String, Map<String, List<HashMap<String, String>>>> flightDetailsWithSSRs = serviceWiseflightDetailsWithSSRs
							.get(SSRType.AIRPORT);

					if (ssrRequest.getAirportCode() == null || ssrRequest.getAirportCode().trim().equals("")) {
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_AIRPORT_CODE_IS_REQUIRED_,
								" Airport Code is required");
					}

					if (ssrRequest.getAirportType() == null || ssrRequest.getAirportType().trim().equals("")) {
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_AIRPORT_TYPE_IS_REQUIRED_,
								" Airport Type is required");
					}

					String travelerRef = ssrRequest.getTravelerRefNumberRPHList().get(0);

					// Integer flightSegId =
					// Integer.parseInt(ssrRequest.getFlightRefNumberRPHList().get(0).split("-")[0].trim());
					String flightRPH = ssrRequest.getFlightRefNumberRPHList().get(0);
					if (travelerRef.startsWith("I")) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PASSENGER_TYPE_NOT_SUPPORTED_,
								" Airport Services are not available for Infants");
					}

					if (flightDetailsWithSSRs.get(travelerRef) == null) {
						flightDetailsWithSSRs.put(travelerRef, new HashMap<String, List<HashMap<String, String>>>());
						if (flightDetailsWithSSRs.get(travelerRef).get(flightRPH) == null) {
							flightDetailsWithSSRs.get(travelerRef).put(flightRPH, new ArrayList<HashMap<String, String>>());
						}
					} else {
						if (flightDetailsWithSSRs.get(travelerRef).get(flightRPH) == null) {
							flightDetailsWithSSRs.get(travelerRef).put(flightRPH, new ArrayList<HashMap<String, String>>());
						} else {
							ArrayList<HashMap<String, String>> sMap = (ArrayList<HashMap<String, String>>) flightDetailsWithSSRs
									.get(travelerRef).get(flightRPH);
							Iterator<HashMap<String, String>> itr = sMap.iterator();
							while (itr.hasNext()) {
								HashMap<String, String> tempMap = itr.next();

								if (ssrRequest.getSsrCode() == null || ssrRequest.getAirportCode() == null) {
									throw new WebservicesException(
											IOTACodeTables.AccelaeroErrorCodes_AAE_SSR_ALREADY_ADDED_FOR_THE_PASSENGER_,
											" Same Airport Services request for the Passenger " + travelerRef);
								}

								if (tempMap.get("SSR_CODE").equals(ssrRequest.getSsrCode())
										&& tempMap.get("AIRPORT_CODE").equals(ssrRequest.getAirportCode())) {
									throw new WebservicesException(
											IOTACodeTables.AccelaeroErrorCodes_AAE_SSR_ALREADY_ADDED_FOR_THE_PASSENGER_,
											" Same Airport Services request for the Passenger " + travelerRef);
								}

							}
						}
					}

					HashMap<String, String> ssrMap = new HashMap<String, String>();
					ssrMap.put("SSR_CODE", ssrRequest.getSsrCode());
					ssrMap.put("AIRPORT_CODE", ssrRequest.getAirportCode());
					ssrMap.put("AIRPORT_TYPE", ssrRequest.getAirportType());

					flightDetailsWithSSRs.get(travelerRef).get(flightRPH).add(ssrMap);
				}
			}

			if (!flightRPHList.isEmpty()) {
				setInflightServiceSegmentCode(serviceWiseflightDetailsWithSSRs.get(SSRType.INFLIGHT), flightRPHList);
			}
		}

		return serviceWiseflightDetailsWithSSRs;
	}

	private void setInflightServiceSegmentCode(Map<String, Map<String, List<HashMap<String, String>>>> flightDetailsWithSSRs,
			List<String> flightRPHList) throws WebservicesException {
		try {
			Map<String, String> fltSegMap = new HashMap<String, String>();
			generateFlightSegMap(fltSegMap, flightRPHList);

			for (Entry<String, Map<String, List<HashMap<String, String>>>> travelEntry : flightDetailsWithSSRs.entrySet()) {
				Map<String, List<HashMap<String, String>>> flightRPHMap = travelEntry.getValue();
				for (Entry<String, List<HashMap<String, String>>> fltEntry : flightRPHMap.entrySet()) {
					String flightRPH = fltEntry.getKey();
					List<HashMap<String, String>> serviceList = fltEntry.getValue();
					for (HashMap<String, String> map : serviceList) {
						map.put("SEGMENT_CODE", fltSegMap.get(flightRPH));
					}
				}
			}
		} catch (Exception e) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_SSR_NOT_AVAILABLE_IN_REQUESTED_AIRPORT_,
					"Requested In-flight Services not available for this flight segment");
		}
	}

	private boolean isBaggagesSelected(OTAAirPriceRQ otaAirPriceRQ) {
		boolean isBaggagesSelected = false;
		if (otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails() != null) {
			for (SpecialReqDetailsType specialReqDetailsType : otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
				if (specialReqDetailsType != null && specialReqDetailsType.getBaggageRequests() != null
						&& specialReqDetailsType.getBaggageRequests().getBaggageRequest() != null
						&& specialReqDetailsType.getBaggageRequests().getBaggageRequest().size() > 0) {
					isBaggagesSelected = true;
				}
			}
		}

		return isBaggagesSelected;
	}

	private void generateFlightSegMap(Map<String, String> fltSegMap, Collection<String> flightRPHs) {
		for (String rph : flightRPHs) {
			fltSegMap.put(rph, FlightRefNumberUtil.getSegmentCodeFromFlightRPH(rph));
		}
	}

	private Map<String, Map<String, String>> getFlightDetailsWithBaggages(OTAAirPriceRQ otaAirPriceRQ)
			throws WebservicesException {
		// TraverlerRef -> flightRPH -> Baggage
		Map<String, Map<String, String>> flightDetailsWithBaggages = new HashMap<String, Map<String, String>>();

		for (SpecialReqDetailsType specialReqDetailsType : otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
			BaggageRequests baggageRequests = specialReqDetailsType.getBaggageRequests();
			for (BaggageRequest baggageRequest : baggageRequests.getBaggageRequest()) {
				String travelerRef = baggageRequest.getTravelerRefNumberRPHList().get(0);
				// Availability response is having flight segment id as RPH and
				// we expect client will send that id with
				// baggagerequest as well.
				// Integer flightSegId =
				// Integer.parseInt(baggageRequest.getFlightRefNumberRPHList().get(0).split("-")[0].trim());
				if (travelerRef.startsWith("I")) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PASSENGER_TYPE_NOT_SUPPORTED_,
							" Baggages are not available for infants");
				}
				if (flightDetailsWithBaggages.get(travelerRef) == null) {
					flightDetailsWithBaggages.put(travelerRef, new HashMap<String, String>());
				}
				flightDetailsWithBaggages.get(travelerRef).put(baggageRequest.getFlightRefNumberRPHList().get(0),
						baggageRequest.getBaggageCode());
			}
		}
		return flightDetailsWithBaggages;
	}

	private boolean isInsuranceSelected(OTAAirPriceRQ otaAirPriceRQ) {
		boolean isInsuranceSelected = false;
		if (otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails() != null) {
			for (SpecialReqDetailsType specialReqDetailsType : otaAirPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
				if (specialReqDetailsType != null && specialReqDetailsType.getInsuranceRequests() != null
						&& specialReqDetailsType.getInsuranceRequests().getInsuranceRequest() != null) {
					isInsuranceSelected = true;
				}
			}
		}

		return isInsuranceSelected;
	}

	private Integer getInsuranceReference(OTAAirPriceRQ airPriceRQ) throws WebservicesException {
		Integer ref = null;
		for (SpecialReqDetailsType specialReqDetailsType : airPriceRQ.getTravelerInfoSummary().getSpecialReqDetails()) {
			if (specialReqDetailsType.getInsuranceRequests().getInsuranceRequest().getRPH() == null) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_OR_INSUFFICIENT_DETAILS,
						"Insurance RPH must not be empty");
			}
			ref = Integer.parseInt(specialReqDetailsType.getInsuranceRequests().getInsuranceRequest().getRPH());
		}
		return ref;
	}

	private static void validateInsurancePriceQuoteRequest(FlightPriceRS flightPriceRS, PaxCountAssembler paxCountAssembler,
			LCCInsuranceQuotationDTO insurance) throws WebservicesException {

		if (insurance == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
					"Invalid insurance quotation.Already sold or not a quoted insurance");
		}

		boolean isReturnTrip = false;

		List<FlightSegmentTO> outBoundFlightSegments = new ArrayList<FlightSegmentTO>();
		outBoundFlightSegments.addAll(CommonServicesUtil.getFlightSegmentDetails(flightPriceRS, false));
		Collections.sort(outBoundFlightSegments);

		Date journeyStartDate = new Date(outBoundFlightSegments.get(0).getDepartureDateTime().getTime());
		Date journeyEndDate = null;
		String journeyStartAirpport = getOriginAndDestination(outBoundFlightSegments.get(0).getSegmentCode())[0];
		String journeyEndAirport = getOriginAndDestination(outBoundFlightSegments.get(outBoundFlightSegments.size() - 1)
				.getSegmentCode())[1];

		Collection<FlightSegmentTO> inboundFlights = CommonServicesUtil.getFlightSegmentDetails(flightPriceRS, true);
		if (inboundFlights != null && !inboundFlights.isEmpty()) {
			List<FlightSegmentTO> inboundFlightSegments = new ArrayList<FlightSegmentTO>();
			inboundFlightSegments.addAll(inboundFlights);
			Collections.sort(inboundFlightSegments);
			journeyEndDate = new Date(inboundFlightSegments.get(inboundFlightSegments.size() - 1).getArrivalDateTime().getTime());
			isReturnTrip = true;
			if (!journeyStartAirpport.equals(getOriginAndDestination(inboundFlightSegments.get(inboundFlightSegments.size() - 1)
					.getSegmentCode())[1])) {
				journeyEndAirport = getOriginAndDestination(inboundFlightSegments.get(inboundFlightSegments.size() - 1)
						.getSegmentCode())[1];
			}
		} else {
			journeyEndDate = new Date(outBoundFlightSegments.get(outBoundFlightSegments.size() - 1).getArrivalDateTime()
					.getTime());
		}

		LCCInsuredJourneyDTO insuredJourneyDTO = insurance.getInsuredJourney();

		if (!(journeyStartDate.equals(insuredJourneyDTO.getJourneyStartDate())
				&& journeyEndDate.equals(insuredJourneyDTO.getJourneyEndDate())
				&& journeyStartAirpport.equals(insuredJourneyDTO.getJourneyStartAirportCode())
				&& journeyEndAirport.equals(insuredJourneyDTO.getJourneyEndAirportCode()) && isReturnTrip == insuredJourneyDTO
					.isRoundTrip())) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_OR_INSUFFICIENT_DETAILS,
					"Invalid insurance quotation.Not matching with the journey requirements");
		}

	}

	/**
	 * 
	 * @param segmentCode
	 * @return
	 */
	private static String[] getOriginAndDestination(String segmentCode) {
		String[] airports = StringUtils.split(segmentCode, "/");

		return airports;
	}

	/**
	 * Creates the FareSegChargeTO saves in the current transaction.
	 * 
	 * @param flightPriceRS
	 * @throws WebservicesException
	 */
	public static void addFareSegmentChargeTOToTransaction(FlightPriceRS flightPriceRS) throws WebservicesException {

		if (flightPriceRS == null || flightPriceRS.getSelectedPriceFlightInfo() == null) {

			if (flightPriceRS != null && flightPriceRS.getSelectedPriceFlightInfo() == null) {
				throw new WebservicesException(
						IOTACodeTables.AccelaeroErrorCodes_AAE_SELECTED_FLIGHT_SEATS_OR_FARES_NOT_AVAILABLE,
						"No seats/fares available");
			}
			throw new IllegalArgumentException("selectedFlight and availableFlightSearchDTO cannot be null");

		}

		PriceInfoTO priceInfoTO = flightPriceRS.getSelectedPriceFlightInfo();

		if (priceInfoTO.getFareSegChargeTO() == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_NO_AVAILABILITY, "No Availabilities");
		}
		FareSegChargeTO fareSegChargeTO = priceInfoTO.getFareSegChargeTO();

		ThreadLocalData.setCurrentTnxParam(Transaction.FARE_SEGMENT_CHARGE_TO, fareSegChargeTO);

	}

	private static boolean isCharterAgent(UserPrincipal principal) throws ModuleException {
		String agentCode = principal.getAgentCode();
		Agent agent = WebServicesModuleUtils.getTravelAgentBD().getAgent(agentCode);

		return ((agent.getCharterAgent() != null) && (agent.CHARTER_AGENT_YES.equals(agent.getCharterAgent())));
	}

	private Map<Integer, Integer> getOndBundledServiceSelection(OTAAirPriceRQ otaAirPriceRQ) {
		Map<Integer, Integer> ondBundledServiceSelection = new HashMap<Integer, Integer>();
		OTAAirPriceRQ.BundledServiceSelectionOptions bundledServiceSelection = otaAirPriceRQ.getBundledServiceSelectionOptions();
		if (bundledServiceSelection != null) {
			ondBundledServiceSelection.put(OndSequence.OUT_BOUND, bundledServiceSelection.getOutBoundBunldedServiceId());
			ondBundledServiceSelection.put(OndSequence.IN_BOUND, bundledServiceSelection.getInBoundBunldedServiceId());
		}		
		
		return ondBundledServiceSelection;
	}
	
	private void addSelectedBundledToTransaction(OTAAirPriceRQ otaAirPriceRQ) {
		ThreadLocalData.setCurrentTnxParam(Transaction.PRICE_QUOTED_BUNDLED, getOndBundledServiceSelection(otaAirPriceRQ));
	}
	
	
	private void isBaggageSelectionValid(Map<String, Integer> baggageOndGrpIdWiseCount,
			Map<String, Map<String, LCCBaggageDTO>> selectedBaggages) throws WebservicesException {
		
		for (String paxRph : selectedBaggages.keySet()) {
			Map<String, String> selectedBagMap = new HashMap<String, String>();
			Map<String, LCCBaggageDTO> paxBagSelection = selectedBaggages.get(paxRph);

			for (String flightRPH : paxBagSelection.keySet()) {
				LCCBaggageDTO bagDto = paxBagSelection.get(flightRPH);
				if (selectedBagMap.containsKey(bagDto.getOndBaggageGroupId())) {
					String selectedBagName = selectedBagMap.get(bagDto.getOndBaggageGroupId());
					if (!selectedBagName.equals(bagDto.getBaggageName())) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
								"Same baggage should select for all the flights in an OND");
					}
				} else {
					selectedBagMap.put(bagDto.getOndBaggageGroupId(), bagDto.getBaggageName());
				}
			}
		}

	}
	
	private static Date getAddedTransitTime(Date date, String timeInHhmm) {

		String hours = timeInHhmm.substring(0, timeInHhmm.indexOf(":"));
		String mins = timeInHhmm.substring(timeInHhmm.indexOf(":") + 1);

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(date);
		calander.add(GregorianCalendar.HOUR, Integer.parseInt(hours));
		calander.add(GregorianCalendar.MINUTE, Integer.parseInt(mins));

		return calander.getTime();
	}
}
