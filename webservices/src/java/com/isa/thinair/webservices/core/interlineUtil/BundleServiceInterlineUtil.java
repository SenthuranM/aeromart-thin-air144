package com.isa.thinair.webservices.core.interlineUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AABundledServiceExt;
import org.opentravel.ota._2003._05.BundledService;
import org.opentravel.ota._2003._05.IOTACodeTables;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;
import com.isa.thinair.webservices.core.util.PriceQuoteUtil;
import com.isa.thinair.webservices.core.util.WSAncillaryUtil;

public class BundleServiceInterlineUtil {

	private static final Log log = LogFactory.getLog(BundleServiceInterlineUtil.class);

	// Applicable Bundle Services
	private static final List<EXTERNAL_CHARGES> BUNDLE_SERVICES = new ArrayList<>(Arrays.asList(EXTERNAL_CHARGES.BAGGAGE,
			EXTERNAL_CHARGES.SEAT_MAP, EXTERNAL_CHARGES.MEAL, EXTERNAL_CHARGES.AIRPORT_SERVICE));

	private static final List<String> APPLICABLE_BUNDLE_SERVICES = createApplicableBundleService();

	private static final List<String> BUNDLE_AIRPORT_SERVICES = new ArrayList<>(Arrays.asList(EXTERNAL_CHARGES.AIRPORT_SERVICE
			.name()));

	private static final String OND_SPLIT_DELIMETER = "/";

	private static void createMessage(Map<String, Boolean> ancillaryOutBoundValidateMap,
			Map<String, Boolean> ancillaryInBoundValidateMap) throws WebservicesException {

		HashSet<String> ancillaries = new HashSet<>();
		ancillaries.addAll(ancillaryOutBoundValidateMap.keySet());
		ancillaries.addAll(ancillaryInBoundValidateMap.keySet());

		if (!ancillaries.isEmpty()) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_PLEASE_ADD_FREE_BUNDLE_SERVICES,
					ancillaries.toString());
		}
	}

	private static List<String> createApplicableBundleService() {

		List<String> bundleServiceList = new ArrayList<>();

		for (EXTERNAL_CHARGES charges : BUNDLE_SERVICES) {
			bundleServiceList.add(charges.name());
		}

		return bundleServiceList;
	}

	private static List<String> getListOfFlightSegments(String ondCode) {

		String[] airports = ondCode.split(OND_SPLIT_DELIMETER);
		List<String> flightSegmentCodes = new ArrayList<String>();

		for (int i = 0; i < airports.length - 1; i++) {
			flightSegmentCodes.add(airports[i] + OND_SPLIT_DELIMETER + airports[i + 1]);
		}

		return flightSegmentCodes;
	}

	private static void
			validateAncillary(AABundledServiceExt boundBundleServices,
					Map<String, List<LCCClientExternalChgDTO>> selectedAncillaryExternalCharges,
					Map<String, Boolean> ancillaryValidateMap) {

		if (boundBundleServices.getBundledService() != null) {

			for (BundledService bundledService : boundBundleServices.getBundledService()) {

				List<String> bundleServices = bundledService.getIncludedServies();

				for (String bundleService : bundleServices) {

					if (APPLICABLE_BUNDLE_SERVICES.contains(bundleService)) {

						List<String> flightSegmentCodes = getListOfFlightSegments(boundBundleServices.getApplicableOnd());

						for (String flightSegmentCode : flightSegmentCodes) {

							boolean isAdded = false;

							for (String travelerRef : selectedAncillaryExternalCharges.keySet()) {

								List<LCCClientExternalChgDTO> externalChgDTOs = selectedAncillaryExternalCharges.get(travelerRef);

								for (LCCClientExternalChgDTO lccClientExternalChgDTO : externalChgDTOs) {

									if (bundleService.equals(lccClientExternalChgDTO.getExternalCharges().name())) {

										if (flightSegmentCode.contains(FlightRefNumberUtil
												.getSegmentCodeFromFlightRPH(lccClientExternalChgDTO.getFlightRefNumber()))
												|| BUNDLE_AIRPORT_SERVICES.contains(bundleService)) {
											isAdded = true;
											break;
										}
									}
								}
							}
							if (isAdded == false) {
								ancillaryValidateMap.put(bundleService, false);
							}
						}
					}
				}

			}
		}

	}

	@SuppressWarnings("unchecked")
	private static Map<String, List<LCCClientExternalChgDTO>> getPriceQuotedExternalCharges() {

		Map<String, List<LCCClientExternalChgDTO>> quotedExternalCharges = (Map<String, List<LCCClientExternalChgDTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);

		Map<String, List<LCCClientExternalChgDTO>> ancilarySelectedChargeMap = new HashMap<String, List<LCCClientExternalChgDTO>>();

		if (quotedExternalCharges != null) {

			for (String travelerRef : quotedExternalCharges.keySet()) {
				
				List<LCCClientExternalChgDTO> listExternalCharges = null;

				List<LCCClientExternalChgDTO> externalChgDTOs = quotedExternalCharges.get(travelerRef);

				if (externalChgDTOs != null && externalChgDTOs.size() > 0) {

					for (LCCClientExternalChgDTO extCharge : externalChgDTOs) {

						if (BUNDLE_SERVICES.contains(extCharge.getExternalCharges())) {

							if (listExternalCharges == null) {
								listExternalCharges = new ArrayList<LCCClientExternalChgDTO>();
							} else {
								listExternalCharges = ancilarySelectedChargeMap.get(travelerRef);
							}

							listExternalCharges.add(extCharge);
							ancilarySelectedChargeMap.put(travelerRef, listExternalCharges);
						}

					}
				}
			}
		}

		return ancilarySelectedChargeMap;

	}

	private static AABundledServiceExt getBundleDetails(AABundledServiceExt boundBundleServices,
			List<BundledFareDTO> ondSelectedBundledFares, boolean bundledApplicable) {

		AABundledServiceExt aaBundledServiceExt = new AABundledServiceExt();

		if (bundledApplicable && boundBundleServices != null && ondSelectedBundledFares != null && boundBundleServices.getBundledService() != null) {

			for (BundledFareDTO bundledFareDTO : ondSelectedBundledFares) {

				for (BundledService bundledService : boundBundleServices.getBundledService())

					if (bundledFareDTO != null && bundledService != null && 
							bundledFareDTO.getBundledFarePeriodId() == bundledService.getBunldedServiceId()) {

						if (aaBundledServiceExt.getApplicableOnd() == null) {
							aaBundledServiceExt.setApplicableOnd(boundBundleServices.getApplicableOnd());
							aaBundledServiceExt.setApplicableOndSequence(boundBundleServices.getApplicableOndSequence());
						}

						aaBundledServiceExt.getBundledService().add(bundledService);
					}
			}
		}
		return aaBundledServiceExt;
	}
	
	
	private static boolean bundledApplicableOutboundInbound(int ondSequence) {		
		boolean bundledApplicable = false;
		Map selectedBundled = (HashMap) ThreadLocalData.getCurrentTnxParam(Transaction.PRICE_QUOTED_BUNDLED);
		
		if (selectedBundled == null) {
			bundledApplicable =  false;
		} else if (selectedBundled.get(ondSequence) != null) {
			bundledApplicable = true;
		}
		
		return bundledApplicable;		
	}
	

	public static void validateSelectedBundleServices() throws WebservicesException {

		Map<String, Boolean> ancillaryOutboundValidateMap = new HashMap<>();
		Map<String, Boolean> ancillaryInboundValidateMap = new HashMap<>();

		List<BundledFareDTO> ondSelectedBundledFares = WSAncillaryUtil.getOndSelectedBundledFares();

		// Get last price quoted available bundle details
		AABundledServiceExt outBoundBundleServices = PriceQuoteUtil.getOndBundledServiceObj(OndSequence.OUT_BOUND);
		AABundledServiceExt inBoundBundleServices = PriceQuoteUtil.getOndBundledServiceObj(OndSequence.IN_BOUND);

		AABundledServiceExt selectedOutBoundBundleServices = getBundleDetails(outBoundBundleServices, ondSelectedBundledFares, bundledApplicableOutboundInbound(OndSequence.OUT_BOUND));
		AABundledServiceExt selectedInBoundBundleServices = getBundleDetails(inBoundBundleServices, ondSelectedBundledFares , bundledApplicableOutboundInbound(OndSequence.IN_BOUND));

		// Get selected ancillary per pax
		Map<String, List<LCCClientExternalChgDTO>> selectedAncillaryExternalCharges = getPriceQuotedExternalCharges();

		// Validate ancillary
		validateAncillary(selectedOutBoundBundleServices, selectedAncillaryExternalCharges, ancillaryOutboundValidateMap);
		validateAncillary(selectedInBoundBundleServices, selectedAncillaryExternalCharges, ancillaryInboundValidateMap);

		// Generate the message
		createMessage(ancillaryOutboundValidateMap, ancillaryInboundValidateMap);

	}

	private static boolean isAllNullCollection(List<?> list) {

		for (Object obj : list) {
			if (obj != null) {
				return false;
			}
		}
		return true;
	}

	public static boolean isValideSelectedBundleFare(List<BundledFareDTO> ondSelectedBundledFares) {

		boolean isValid = true;

		if (!isValidateBundleServicesSelectionForWebService()) {
			isValid = false;
		} else {

			if (ondSelectedBundledFares == null || ondSelectedBundledFares.isEmpty()) {
				isValid = false;
			} else if (isAllNullCollection(ondSelectedBundledFares)) {
				isValid = false;
			}
		}

		return isValid;
	}

	private static boolean isValidateBundleServicesSelectionForWebService() {
		return AppSysParamsUtil.isBundleFarePopupEnabled(ApplicationEngine.WS);
	}
	
	public static String getBundleApplicableONDCode(OriginDestinationOptionTO originDestinationOptionTO) {		
		
		List<String> segmentCodes = originDestinationOptionTO.getFlightSegmentList().
									stream().
									filter(flightSegmentTO -> (flightSegmentTO.getOperationType() != AirScheduleCustomConstants.OperationTypes.BUS_SERVICE)).
									map(FlightSegmentTO::getSegmentCode).
									collect(Collectors.toList());		
		
		return ReservationApiUtils.getOndCode(segmentCodes);
		
	}

}
