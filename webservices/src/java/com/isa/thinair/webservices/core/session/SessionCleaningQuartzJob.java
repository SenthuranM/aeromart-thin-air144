package com.isa.thinair.webservices.core.session;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

public class SessionCleaningQuartzJob implements Job {

	private static Log log = LogFactory.getLog(SessionCleaningQuartzJob.class);

	public SessionCleaningQuartzJob() {
	}

	public void execute(JobExecutionContext context) throws JobExecutionException {

		String jobName = context.getJobDetail().getKey().getName();

		log.info("SessionCleaningQuartzJob: " + jobName + " execution start at " + new Date());

		WebServicesModuleUtils.getSessionStore().removeExpiredUserSessions();

		log.info("SessionCleaningQuartzJob: " + jobName + " execution end at " + new Date());
	}
}