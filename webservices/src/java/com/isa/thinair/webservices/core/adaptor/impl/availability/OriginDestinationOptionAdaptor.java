package com.isa.thinair.webservices.core.adaptor.impl.availability;

import org.opentravel.ota._2003._05.AAOTAAirAllPriceAvailRS;
import org.opentravel.ota._2003._05.OTAAirAvailRS;

import com.isa.thinair.webservices.core.adaptor.Adaptor;
import com.isa.thinair.webservices.core.adaptor.util.AdaptorUtil;

public class OriginDestinationOptionAdaptor
		implements
		Adaptor<OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption, AAOTAAirAllPriceAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption> {

	@Override
	public AAOTAAirAllPriceAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption adapt(
			OTAAirAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption source) {

		AAOTAAirAllPriceAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption target = new AAOTAAirAllPriceAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption();
		AdaptorUtil.adaptCollection(source.getFlightSegment(), target.getFlightSegment(), new FlightSegmentAdaptor());

		return target;
	}

}
