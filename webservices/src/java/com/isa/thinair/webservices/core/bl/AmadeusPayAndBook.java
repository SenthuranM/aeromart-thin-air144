package com.isa.thinair.webservices.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.amadeus.ama._2008._03.AMATLAPayAndBookRQ;
import com.amadeus.ama._2008._03.AMATLAPayAndBookRS;
import com.amadeus.ama._2008._03.CompanyNameType;
import com.amadeus.ama._2008._03.ErrorWarningType;
import com.amadeus.ama._2008._03.SourceType;
import com.amadeus.ama._2008._03.SuccessType;
import com.amadeus.ama._2008._03.TLAAddressType;
import com.amadeus.ama._2008._03.TLAAssociationType;
import com.amadeus.ama._2008._03.TLABookingClassDataTypeRQ;
import com.amadeus.ama._2008._03.TLAContactType;
import com.amadeus.ama._2008._03.TLAErrorType;
import com.amadeus.ama._2008._03.TLAFareCategoryType;
import com.amadeus.ama._2008._03.TLAFareTypeRQ;
import com.amadeus.ama._2008._03.TLAOriginDestinationInformationType;
import com.amadeus.ama._2008._03.TLAPassengerSSRDataTypeB;
import com.amadeus.ama._2008._03.TLAPassengerType;
import com.amadeus.ama._2008._03.TLAPassengersType;
import com.amadeus.ama._2008._03.TLAPaymentCardTypeRS;
import com.amadeus.ama._2008._03.TLAPaymentDetailTypeBRQ;
import com.amadeus.ama._2008._03.TLAPaymentDetailTypeRS;
import com.amadeus.ama._2008._03.TLAPaymentDetailTypeRS.PaymentAmount;
import com.amadeus.ama._2008._03.TLAPaymentDetailsTypeBRQ;
import com.amadeus.ama._2008._03.TLAPaymentDetailsTypeRS;
import com.amadeus.ama._2008._03.TLAReplyStatusType;
import com.amadeus.ama._2008._03.TLASegmentTypeM;
import com.amadeus.ama._2008._03.TLASingleOriginDestinationTypeRRQ;
import com.amadeus.ama._2008._03.TLAUniqueIDType;
import com.amadeus.ama._2008._03.TLAUseCaseType;
import com.amadeus.ama._2008._03.TelephoneInfoType;
import com.amadeus.ama._2008._03.UniqueIDObjectType;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.util.OndFareUtil;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.model.SSR;
import com.isa.thinair.airmaster.api.service.SsrBD;
import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationSegmentTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ItineraryLayoutModesDTO;
import com.isa.thinair.airreservation.api.dto.ReservationAdminInfoTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ResponseCodes;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webplatform.api.v2.reservation.ExternalChargesMediator;
import com.isa.thinair.webservices.api.dtos.AmadeusPaxTO;
import com.isa.thinair.webservices.api.dtos.AmadeusPriceHolderDTO;
import com.isa.thinair.webservices.api.dtos.AmadeusTotalPriceDTO;
import com.isa.thinair.webservices.api.dtos.SelectedOndFareContainer;
import com.isa.thinair.webservices.api.dtos.SelectedOndFareDTO;
import com.isa.thinair.webservices.api.exception.AmadeusWSException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.api.util.WebservicesConstants.AmadeusConstants;
import com.isa.thinair.webservices.api.util.WebservicesConstants.AmadeusConstants.AmadeusErrorCodes;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.isa.thinair.webservices.core.util.WebservicesContext;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class AmadeusPayAndBook {
	private final static Log log = LogFactory.getLog(AmadeusPayAndBook.class);

	public static AMATLAPayAndBookRS execute(AMATLAPayAndBookRQ amaTLAPayAndBookRQ) {
		AMATLAPayAndBookRS payAndBookRS = new AMATLAPayAndBookRS();
		payAndBookRS.setRelease(WebservicesConstants.AmadeusConstants.ReleasePayAndBookRS);
		payAndBookRS.setVersion(WebservicesConstants.AmadeusConstants.VersionPayAndBookRS);
		try {
			if (amaTLAPayAndBookRQ.getUseCase() == TLAUseCaseType.BOOKING_CREATION) {
				payAndCreateBooking(payAndBookRS, amaTLAPayAndBookRQ);
				payAndBookRS.setSuccess(new SuccessType());
			} else if (amaTLAPayAndBookRQ.getUseCase() == TLAUseCaseType.BOOKING_UPDATE) {
				throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_INTERNAL_ERROR_DO_NOT_RETRY,
						ErrorWarningType.NO_IMPLEMENTATION, "Booking update not available");
			} else {
				throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_QUERY_UNABLE_TO_PARSE,
						ErrorWarningType.REQUIRED_FIELD_MISSING, "Invalid use case");
			}
		} catch (ModuleException e) {
			log.error("Error occured", e);
			TLAErrorType error = new TLAErrorType();
			if (e instanceof AmadeusWSException) {
				AmadeusWSException ae = (AmadeusWSException) e;
				error.setCode(ae.getErrorCode());
				error.setType(ae.getErrorWarningType());
				// TODO fetch the descriptive message from resource bundle
				if (ae.getDescription() != null && !"".equals(ae.getDescription())) {
					error.setValue(ae.getDescription());
				}
			} else if (e.getExceptionCode().equals(ResponseCodes.STANDARD_PAYMENT_BROKER_ERROR)) {
				error = null;
				// Amadeus requested to add success type should confirm
				payAndBookRS.setSuccess(new SuccessType());
				payAndBookRS.setReplyStatus(TLAReplyStatusType.REFUSED);
				payAndBookRS.getRecordLocator().addAll(amaTLAPayAndBookRQ.getRecordLocatorsList().getRecordLocator());
				payAndBookRS.setDisplayCurrencyCode(amaTLAPayAndBookRQ.getDisplayCurrencyCode());
				TLAPaymentDetailsTypeRS paymentStatus = new TLAPaymentDetailsTypeRS();
				TLAPaymentDetailTypeRS paymentDetail = new TLAPaymentDetailTypeRS();
				paymentStatus.getPayment().add(paymentDetail);
				for (TLAPaymentDetailTypeBRQ payment : amaTLAPayAndBookRQ.getFormsOfPayment().getPayment()) {
					if (payment.getPaymentCard() != null) {
						paymentDetail.setPaymentCard(payment.getPaymentCard());
						PaymentAmount payAmt = new PaymentAmount();
						payAmt.setAmount(payment.getPaymentAmount().getAmount());
						payAmt.setCurrencyCode(payment.getPaymentAmount().getCurrencyCode());
						paymentDetail.setPaymentAmount(payAmt);
					}
				}
				payAndBookRS.setPaymentStatus(paymentStatus);
			} else {
				error.setCode(Integer.valueOf(AmadeusErrorCodes.GENERIC_INTERNAL_ERROR_RETRY.getErrorCode()));
				error.setValue(e.getExceptionCode());
				error.setType(ErrorWarningType.UNKNOWN);
			}
			payAndBookRS.setError(error);
		}
		return payAndBookRS;
	}

	/**
	 * Create booking
	 * 
	 * @param payAndBookRS2
	 * 
	 * @param amaTLAPayAndBookRQ
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void payAndCreateBooking(AMATLAPayAndBookRS payAndBookRS, AMATLAPayAndBookRQ amaTLAPayAndBookRQ)
			throws ModuleException {
		int adultCount = 0;
		int childCount = 0;
		int infantCount = 0;

		if (amaTLAPayAndBookRQ.getRecordLocatorsList() == null
				|| amaTLAPayAndBookRQ.getRecordLocatorsList().getRecordLocator().size() != 1) {
			throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_QUERY_INCOHERENT_DATA_FOUND,
					ErrorWarningType.PROTOCOL_VIOLATION, "Invalid record locator from Amadeus");
		}

		for (TLAPassengerType pax : amaTLAPayAndBookRQ.getPassengerList().getPassenger()) {
			if (pax.getPassengerTypeCode().equals(AmadeusConstants.ADULT)) {
				adultCount++;
			} else if (pax.getPassengerTypeCode().equals(AmadeusConstants.CHILD)) {
				childCount++;
			} else if (pax.getPassengerTypeCode().equals(AmadeusConstants.INFANT)) {
				infantCount++;
			}
		}
		if (amaTLAPayAndBookRQ.getDisplayCurrencyCode() == null) {
			throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_INTERNAL_ERROR_DO_NOT_RETRY,
					ErrorWarningType.PROTOCOL_VIOLATION, "Invalid currency code for booking "
							+ amaTLAPayAndBookRQ.getDisplayCurrencyCode());
		}
		AmadeusValidationUtil.validateCurrency(amaTLAPayAndBookRQ.getDisplayCurrencyCode());

		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
		String originISOCountryCode = null;
		if (amaTLAPayAndBookRQ.getPOS() != null) {
			if (amaTLAPayAndBookRQ.getPOS().getSource() != null && amaTLAPayAndBookRQ.getPOS().getSource().size() > 0) {
				SourceType pos = amaTLAPayAndBookRQ.getPOS().getSource().get(0);
				originISOCountryCode = pos.getISOCountry();
			}
		}
		String currencyCode = AmadeusCommonUtil.getPGWCurrencyCode(amaTLAPayAndBookRQ.getDisplayCurrencyCode(),
				originISOCountryCode);

		if (!currencyCode.equals(amaTLAPayAndBookRQ.getDisplayCurrencyCode())) {
			throw new AmadeusWSException(AmadeusErrorCodes.REQUESTED_CURRENCY_NOT_SUPPORTED, ErrorWarningType.BIZ_RULE,
					"Currency is not supported, cannot continue with authorization.");
		}

		CurrencyExchangeRate currencyExgRate = AmadeusCommonUtil.getCurrencyExchangeRate(currencyCode);

		SelectedOndFareContainer ondFareContainer = new SelectedOndFareContainer();
		AmadeusTotalPriceDTO totalPriceDTO = new AmadeusTotalPriceDTO();
		FlightBD flightDelegate = WebServicesModuleUtils.getFlightBD();
		ZuluLocalTimeConversionHelper zuluHelper = new ZuluLocalTimeConversionHelper(WebServicesModuleUtils.getAirportBD());

		List<Object[]> rphArr = new ArrayList<Object[]>();
		Map<Integer, String> segIdOriginatorMap = new HashMap<Integer, String>();

		BigDecimal perPaxConvertedCCCharge = BigDecimal.ZERO;
		BigDecimal recalConvertedCCCharge = BigDecimal.ZERO;
		// BigDecimal totalFareInSelCur = BigDecimal.ZERO;
		// BigDecimal totalTaxInSelCur = BigDecimal.ZERO;
		// BigDecimal totalSSRInSelCur = BigDecimal.ZERO;
		BigDecimal totalAmoutInSelCur = BigDecimal.ZERO;
		ExternalChgDTO ccChgDTO = null;

		// change for cc charge as fixed value
		int tottalSegmentCount = 0;

		// Map<TLASingleOriginDestinationTypeRRQ, Map<String,SelectedOndFareDTO>> ondWiseBookingClassFareMap = new
		// HashMap<TLASingleOriginDestinationTypeRRQ, Map<String,SelectedOndFareDTO>>();

		for (TLASingleOriginDestinationTypeRRQ singleOndRQ : amaTLAPayAndBookRQ.getItinerary().getSingleOriginDestination()) {

			Collection<FlightSegmentDTO> obFlightSegmentDTOs = new ArrayList<FlightSegmentDTO>();
			TLAOriginDestinationInformationType ondInfo = singleOndRQ.getOriginDestinationInfo();
			Map<String, String[]> refSegRPHMap = new HashMap<String, String[]>();
			for (TLASegmentTypeM segment : singleOndRQ.getJourney().getSegment()) {

				FlightSegmentDTO aaFlightSegmentDTO = new FlightSegmentDTO();
				aaFlightSegmentDTO.setFromAirport(segment.getDepartureAirport().getLocationCode());
				aaFlightSegmentDTO.setToAirport(segment.getArrivalAirport().getLocationCode());
				aaFlightSegmentDTO.setFlightNumber(segment.getOperatingAirline().getCode()
						+ segment.getOperatingAirline().getFlightNumber());
				aaFlightSegmentDTO.setDepartureDateTime(CommonUtil.parseDateTime(segment.getDepartureDateTime().toXMLFormat()));
				aaFlightSegmentDTO.setArrivalDateTime(CommonUtil.parseDateTime(segment.getArrivalDateTime().toXMLFormat()));
				refSegRPHMap.put(AmadeusCommonUtil.getTmpSegRefNo(aaFlightSegmentDTO), new String[] { segment.getRPH(),
						segment.getOriginatorFlightSegmentID().getID() });

				rphArr.add(new Object[] { aaFlightSegmentDTO.getDepartureDateTime(), segment.getRPH() });
				obFlightSegmentDTOs.add(aaFlightSegmentDTO);
			}
			// update segment count
			tottalSegmentCount += obFlightSegmentDTOs.size();

			// FIXME - avoid caching the BC mapping and avoid all the BC loaded to the memory
			// FIXME - booking classes mapping
			// Assuming a single booking class for ond
			/*
			 * TLAFareReferenceType fareType =
			 * singleOndRQ.getJourney().getFaresForSegment().iterator().next().getBookingClassData().getFareBasis();
			 * String bookingClassCode = null; Map<BookingClassCharMappingTO, String> bookingClassCharMappingMap =
			 * WebServicesModuleUtils.getGlobalConfig().getBookingClassCharMappingMap(); Set<BookingClassCharMappingTO>
			 * keys = new HashSet<BookingClassCharMappingTO>(); String singleCharBC = fareType.getResBookDesigCode(); if
			 * (singleCharBC == null){ singleCharBC =
			 * AmadeusCommonUtil.getBookingClassFromAmadeusFBC(fareType.getValue()); }
			 */

			Collection<FlightSegmentDTO> filledFltSegDTOs = flightDelegate.getFilledFlightSegmentDTOs(obFlightSegmentDTOs);

			Map<Integer, String> segIdRPHMap = new HashMap<Integer, String>();
			List<Integer> segIds = new ArrayList<Integer>();
			for (FlightSegmentDTO fltSegDTO : filledFltSegDTOs) {
				segIds.add(fltSegDTO.getSegmentId());
				segIdRPHMap.put(fltSegDTO.getSegmentId(), refSegRPHMap.get(AmadeusCommonUtil.getTmpSegRefNo(fltSegDTO))[0]);
				segIdOriginatorMap
						.put(fltSegDTO.getSegmentId(), refSegRPHMap.get(AmadeusCommonUtil.getTmpSegRefNo(fltSegDTO))[1]);
			}

			AvailableFlightSearchDTO searchDTO = AmadeusCommonUtil.composeAvailSearchDTO(ondInfo, adultCount, childCount,
					infantCount, currencyCode, principal, zuluHelper, segIds);
			searchDTO.setOutBoundFlights(segIds);
			searchDTO.setExcludeConnectionFares(true);
			searchDTO.setExcludeReturnFares(true);

			// if(WebServicesModuleUtils.getAmadeusConfig().isBookingClassMappingEnabled()){
			// for (Entry<BookingClassCharMappingTO, String> entry : bookingClassCharMappingMap.entrySet()) {
			// if (entry.getValue().equals(singleCharBC)) {
			// if(entry.getKey().getGdsId() == AmadeusConstants.GDS_ID) {
			// bookingClassCode = entry.getKey().getBookingCode();
			// fareType.setResBookDesigCode(bookingClassCode);
			// break;
			// }
			// }
			// }
			//
			// searchDTO.setBookingClassCode(bookingClassCode);
			SelectedFlightDTO selectedFlightDTO = WebServicesModuleUtils.getReservationQueryBD().getFareQuote(searchDTO, null);

			Collection<OndFareDTO> selectedOndCollection = null;
			if (selectedFlightDTO.isSeatsAvailable()) {
				Collection<OndFareDTO> ondFareDTOColl = selectedFlightDTO.getOndFareDTOs(false);
				// FIXME - validate fare
				// if (isMatchingBookingClass(ondFareDTOColl, singleOndRQ.getJourney().getFaresForSegment(),
				// segIdRPHMap)) {
				selectedOndCollection = ondFareDTOColl;
				// }
			} else {
				throw new AmadeusWSException(AmadeusErrorCodes.FARES_RETRIEVAL_FAILED, ErrorWarningType.BIZ_RULE,
						"Fares not available");
			}

			// FIXME if both fixed and normal fare available, minimum of the two is to be given out
			if (selectedOndCollection == null && selectedFlightDTO.isSeatsAvailable()) {
				log.warn("FIXME - FIXED FARES ARE NOT HANDLED YET!");
				// Collection<OndFareDTO> ondFareDTOColl =
				// selectedFlightDTO.getFixedQuotaFareChargesSeatsSegmentsDTOs();
				// if (isMatchingBookingClass(ondFareDTOColl, singleOndRQ.getJourney().getFaresForSegment(),
				// segIdRPHMap)) {
				// selectedOndCollection = ondFareDTOColl;
				// }
			}

			if (selectedOndCollection == null) {
				throw new AmadeusWSException(AmadeusErrorCodes.FARES_RETRIEVAL_FAILED, ErrorWarningType.BIZ_RULE,
						"Fares not available");
			}

			SelectedOndFareDTO selectedOndFareDTO = new SelectedOndFareDTO();
			selectedOndFareDTO.setFareDTOs(selectedOndCollection);
			selectedOndFareDTO.setFareType(selectedFlightDTO.getFareType());
			ondFareContainer.addSelectedOndFareDTO(selectedOndFareDTO);

			for (OndFareDTO ondFare : selectedOndCollection) {
				BigDecimal baseFare = BigDecimal.ZERO;
				BigDecimal taxNCharges = BigDecimal.ZERO;
				BigDecimal baseFareInSelCur = BigDecimal.ZERO;
				BigDecimal taxesInSelCur = BigDecimal.ZERO;
				if (adultCount > 0) {
					baseFare = AccelAeroCalculator
							.add(baseFare, AccelAeroCalculator.multiply(
									AccelAeroCalculator.parseBigDecimal(ondFare.getAdultFare()), adultCount));
					taxNCharges = AccelAeroCalculator.add(taxNCharges, AccelAeroCalculator.multiply(
							AccelAeroCalculator.parseBigDecimal(ondFare.getTotalCharges()[0]), adultCount));

					// convert ond fare to exchange rate -

					if (currencyExgRate != null) {

						BigDecimal adultFareInSelectedCur = AccelAeroCalculator.parseBigDecimal(AccelAeroCalculator.multiply(
								AccelAeroCalculator.parseBigDecimal(ondFare.getAdultFare()),
								currencyExgRate.getMultiplyingExchangeRate()).doubleValue());

						BigDecimal adultChargesInSelectedCur = AccelAeroCalculator.parseBigDecimal(AccelAeroCalculator.multiply(
								AccelAeroCalculator.parseBigDecimal(ondFare.getTotalCharges()[0]),
								currencyExgRate.getMultiplyingExchangeRate()).doubleValue());

						baseFareInSelCur = AccelAeroCalculator.add(
								AccelAeroCalculator.multiply(adultFareInSelectedCur, new BigDecimal(adultCount)),
								baseFareInSelCur);
						taxesInSelCur = AccelAeroCalculator.add(
								AccelAeroCalculator.multiply(adultChargesInSelectedCur, new BigDecimal(adultCount)),
								taxesInSelCur);

					}

				}

				if (childCount > 0) {
					baseFare = AccelAeroCalculator.add(baseFare, AccelAeroCalculator.multiply(

					AccelAeroCalculator.parseBigDecimal(ondFare.getChildFare()), childCount));
					taxNCharges = AccelAeroCalculator.add(taxNCharges, AccelAeroCalculator.multiply(
							AccelAeroCalculator.parseBigDecimal(ondFare.getTotalCharges()[2]), childCount));

					if (currencyExgRate != null) {

						BigDecimal childFareInSelectedCur = AccelAeroCalculator.parseBigDecimal(AccelAeroCalculator.multiply(
								AccelAeroCalculator.parseBigDecimal(ondFare.getChildFare()),
								currencyExgRate.getMultiplyingExchangeRate()).doubleValue());

						BigDecimal childChargesInSelectedCur = AccelAeroCalculator.parseBigDecimal(AccelAeroCalculator.multiply(
								AccelAeroCalculator.parseBigDecimal(ondFare.getTotalCharges()[2]),
								currencyExgRate.getMultiplyingExchangeRate()).doubleValue());

						baseFareInSelCur = AccelAeroCalculator.add(
								AccelAeroCalculator.multiply(childFareInSelectedCur, new BigDecimal(childCount)),
								baseFareInSelCur);
						taxesInSelCur = AccelAeroCalculator.add(
								AccelAeroCalculator.multiply(childChargesInSelectedCur, new BigDecimal(childCount)),
								taxesInSelCur);
					}

				}
				if (infantCount > 0) {
					baseFare = AccelAeroCalculator.add(baseFare, AccelAeroCalculator.multiply(
							AccelAeroCalculator.parseBigDecimal(ondFare.getInfantFare()), infantCount));
					taxNCharges = AccelAeroCalculator.add(taxNCharges, AccelAeroCalculator.multiply(
							AccelAeroCalculator.parseBigDecimal(ondFare.getTotalCharges()[1]), infantCount));

					if (currencyExgRate != null) {

						BigDecimal infantFareInSelectedCur = AccelAeroCalculator.parseBigDecimal(AccelAeroCalculator.multiply(
								AccelAeroCalculator.parseBigDecimal(ondFare.getInfantFare()),
								currencyExgRate.getMultiplyingExchangeRate()).doubleValue());

						BigDecimal infantChargesInSelectedCur = AccelAeroCalculator.parseBigDecimal(AccelAeroCalculator.multiply(
								AccelAeroCalculator.parseBigDecimal(ondFare.getTotalCharges()[1]),
								currencyExgRate.getMultiplyingExchangeRate()).doubleValue());

						baseFareInSelCur = AccelAeroCalculator.add(
								AccelAeroCalculator.multiply(infantFareInSelectedCur, new BigDecimal(infantCount)),
								baseFareInSelCur);
						taxesInSelCur = AccelAeroCalculator.add(
								AccelAeroCalculator.multiply(infantChargesInSelectedCur, new BigDecimal(infantCount)),
								taxesInSelCur);
					}
				}

				totalPriceDTO.setTotalFare(AccelAeroCalculator.add(totalPriceDTO.getTotalFare(), baseFare));
				totalPriceDTO.setTotalTax(AccelAeroCalculator.add(totalPriceDTO.getTotalTax(), taxNCharges));

				if (currencyExgRate != null) {
					totalPriceDTO.setTotalInSelectedCurrency(AccelAeroCalculator.add(totalPriceDTO.getTotalInSelectedCurrency(),
							AccelAeroCalculator.add(baseFareInSelCur, taxesInSelCur)));
				} else {
					totalPriceDTO.setTotalInSelectedCurrency(AccelAeroCalculator.add(totalPriceDTO.getTotalInSelectedCurrency(),
							AccelAeroCalculator.add(baseFare, taxNCharges)));
				}
			}
			// TODO - clean up
			/*
			 * } else {
			 * 
			 * //Do fare quotes for all amadeus exposed booking classes and get the matching one if available
			 * 
			 * Map<String,SelectedOndFareDTO> bookingClassFareMap = new HashMap<String, SelectedOndFareDTO>();
			 * 
			 * int counter = 0; for (Entry<BookingClassCharMappingTO, String> entry :
			 * bookingClassCharMappingMap.entrySet()) {
			 * 
			 * counter++; bookingClassCode = entry.getKey().getBookingCode();
			 * searchDTO.setBookingClassCode(bookingClassCode); SelectedFlightDTO selectedFlightDTO =
			 * WebServicesModuleUtils.getReservationQueryBD() .getFareQuote(searchDTO);
			 * 
			 * 
			 * Collection<OndFareDTO> selectedTempOndCollection = null;
			 * 
			 * if (selectedFlightDTO.isSeatsAvailable()) { Collection<OndFareDTO> ondFareDTOColl =
			 * selectedFlightDTO.getFareChargesSeatsSegmentsDTOs(); if (isMatchingBookingClass(ondFareDTOColl,
			 * singleOndRQ.getJourney().getFaresForSegment(), segIdRPHMap)) { selectedTempOndCollection =
			 * ondFareDTOColl; } }
			 * 
			 * if (selectedTempOndCollection == null && selectedFlightDTO.isFixedSeatsAvailable()) {
			 * Collection<OndFareDTO> ondFareDTOColl = selectedFlightDTO.getFixedQuotaFareChargesSeatsSegmentsDTOs(); if
			 * (isMatchingBookingClass(ondFareDTOColl, singleOndRQ.getJourney().getFaresForSegment(), segIdRPHMap)) {
			 * selectedTempOndCollection = ondFareDTOColl; } }
			 * 
			 * if (selectedTempOndCollection == null && bookingClassFareMap.size() == 0) {
			 * 
			 * if(counter == bookingClassCharMappingMap.size()){ throw new
			 * AmadeusWSException(AmadeusErrorCodes.FARES_RETRIEVAL_FAILED, ErrorWarningType.BIZ_RULE,
			 * "Fares not available"); } else { continue; } }
			 * 
			 * if(selectedTempOndCollection != null){ SelectedOndFareDTO selectedOndFareDTO = new SelectedOndFareDTO();
			 * selectedOndFareDTO.setFareDTOs(selectedTempOndCollection);
			 * selectedOndFareDTO.setFareType(selectedFlightDTO.getFareType());
			 * bookingClassFareMap.put(bookingClassCode, selectedOndFareDTO); }
			 * 
			 * }
			 * 
			 * ondWiseBookingClassFareMap.put(singleOndRQ, bookingClassFareMap);
			 * 
			 * }
			 */
		}

		/*
		 * if(WebServicesModuleUtils.getAmadeusConfig().isBookingClassMappingEnabled()){
		 * 
		 * ccChgDTO = AmadeusCommonUtil.calculateCCCharges(totalPriceDTO.getTotalWithoutFee(), adultCount + childCount);
		 * BigDecimal ccAmt = ccChgDTO.getAmount(); totalPriceDTO.setTotalFee(ccAmt);
		 * 
		 * perPaxConvertedCCCharge = AmadeusCommonUtil.getDispValue(AccelAeroCalculator.divide(ccChgDTO.getAmount(), new
		 * BigDecimal(adultCount + childCount)),currencyExgRate);
		 * 
		 * recalConvertedCCCharge = AccelAeroCalculator.multiply(perPaxConvertedCCCharge, new BigDecimal(adultCount +
		 * childCount)); totalFareInSelCur = AmadeusCommonUtil.getDispValue(totalPriceDTO.getTotalFare(),
		 * currencyExgRate); totalTaxInSelCur = AmadeusCommonUtil.getDispValue(totalPriceDTO.getTotalTax(),
		 * currencyExgRate); totalSSRInSelCur = AmadeusCommonUtil.getDispValue(totalPriceDTO.getTotalSSR(),
		 * currencyExgRate); totalAmoutInSelCur =
		 * AccelAeroCalculator.add(totalFareInSelCur,totalTaxInSelCur,totalSSRInSelCur,recalConvertedCCCharge);
		 * 
		 * 
		 * if (amaTLAPayAndBookRQ.getTotalAmount().getAmount().compareTo(totalAmoutInSelCur) != 0) { throw new
		 * AmadeusWSException(AmadeusErrorCodes.INCORRECT_PAYMENTS, ErrorWarningType.BIZ_RULE,
		 * "Incorrect payments and/or charges"); }
		 * 
		 * } else {
		 * 
		 * 
		 * //Generate all combinations of pricing for the OnDs List<AmadeusPriceHolderDTO> priceCombinationHolders = new
		 * ArrayList<AmadeusPriceHolderDTO>();
		 * 
		 * int[] paxCounts = {adultCount,childCount,infantCount}; for (TLASingleOriginDestinationTypeRRQ singleOndRQ :
		 * ondWiseBookingClassFareMap.keySet()) { generatePriceCombinations(priceCombinationHolders, singleOndRQ,
		 * ondWiseBookingClassFareMap.get(singleOndRQ), paxCounts, currencyExgRate); }
		 * 
		 * if(priceCombinationHolders.size() == 0){ throw new
		 * AmadeusWSException(AmadeusErrorCodes.FARES_RETRIEVAL_FAILED, ErrorWarningType.BIZ_RULE,
		 * "Fares not available"); } else {
		 * 
		 * int noOfCombinations = 0;
		 * 
		 * for(AmadeusPriceHolderDTO priceHolder: priceCombinationHolders){ noOfCombinations++; AmadeusTotalPriceDTO
		 * totalPrice = priceHolder.getCalculatedTotalPrice(); ccChgDTO =
		 * AmadeusCommonUtil.calculateCCCharges(totalPrice.getTotalWithoutFee(), adultCount + childCount); BigDecimal
		 * ccAmt = ccChgDTO.getAmount(); totalPriceDTO.setTotalFee(ccAmt);
		 * 
		 * perPaxConvertedCCCharge = AmadeusCommonUtil.getDispValue(AccelAeroCalculator.divide(ccChgDTO.getAmount(), new
		 * BigDecimal(adultCount + childCount)),currencyExgRate);
		 * 
		 * recalConvertedCCCharge = AccelAeroCalculator.multiply(perPaxConvertedCCCharge, new BigDecimal(adultCount +
		 * childCount)); totalAmoutInSelCur =
		 * AccelAeroCalculator.add(totalPrice.getTotalInSelectedCurrency(),recalConvertedCCCharge);
		 * 
		 * 
		 * if (amaTLAPayAndBookRQ.getTotalAmount().getAmount().compareTo(totalAmoutInSelCur) == 0) { totalPriceDTO =
		 * totalPrice; for(SelectedOndFareDTO selectedOndFareDTO : priceHolder.getOriginDestinationList().values()){
		 * ondFareContainer.addSelectedOndFareDTO(selectedOndFareDTO); }
		 * 
		 * break;
		 * 
		 * }
		 * 
		 * if (amaTLAPayAndBookRQ.getTotalAmount().getAmount().compareTo(totalAmoutInSelCur) != 0 &&
		 * priceCombinationHolders.size() == noOfCombinations) { throw new
		 * AmadeusWSException(AmadeusErrorCodes.INCORRECT_PAYMENTS, ErrorWarningType.BIZ_RULE,
		 * "Incorrect payments and/or charges"); } } }
		 * 
		 * }
		 */

		// Calculate possible total amounts and match them
		ccChgDTO = AmadeusCommonUtil.calculateCCCharges(totalPriceDTO.getTotalWithoutFee(), adultCount + childCount,
				tottalSegmentCount, ChargeRateOperationType.MAKE_ONLY);
		BigDecimal ccAmt = ccChgDTO.getAmount();
		totalPriceDTO.setTotalFee(ccAmt);

		perPaxConvertedCCCharge = AmadeusCommonUtil.getDispValue(
				AccelAeroCalculator.divide(ccChgDTO.getAmount(), new BigDecimal(adultCount + childCount)), currencyExgRate);

		recalConvertedCCCharge = AccelAeroCalculator.multiply(perPaxConvertedCCCharge, new BigDecimal(adultCount + childCount));
		totalAmoutInSelCur = AccelAeroCalculator.add(totalPriceDTO.getTotalInSelectedCurrency(), recalConvertedCCCharge);

		if (amaTLAPayAndBookRQ.getTotalAmount().getAmount().compareTo(totalAmoutInSelCur) != 0) {
			log.error("Price quoted mismatches with requested booking price. Invalid booking price or requested price no longer available."
					+ " [Expected total = "
					+ totalAmoutInSelCur
					+ ",Received total ="
					+ amaTLAPayAndBookRQ.getTotalAmount().getAmount() + "]");
			throw new AmadeusWSException(AmadeusErrorCodes.FARES_RETRIEVAL_FAILED, ErrorWarningType.BIZ_RULE,
					"Fares not available");
		}

		if (amaTLAPayAndBookRQ.getFormsOfPayment().getPayment().size() > 1) {
			throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_QUERY_INCOHERENT_DATA_FOUND,
					ErrorWarningType.NO_IMPLEMENTATION, "Multiple payment elements detected");
		}
		TLAPaymentDetailTypeBRQ payment = amaTLAPayAndBookRQ.getFormsOfPayment().getPayment().iterator().next();
		if (payment.getPaymentCard() != null) {
			if (payment.getPaymentAmount().getCurrencyCode() != null
					&& !payment.getPaymentAmount().getCurrencyCode().equals(currencyCode)) {
				throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_QUERY_INCOHERENT_DATA_FOUND, ErrorWarningType.BIZ_RULE,
						"Payment currency not matching with total currency code");
			}
			if (payment.getPaymentAmount().getAmount().compareTo(totalAmoutInSelCur) != 0) {
				throw new AmadeusWSException(AmadeusErrorCodes.INCORRECT_PAYMENTS, ErrorWarningType.BIZ_RULE,
						"Incorrect payments and/or charges");
			}
		} else {
			throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_QUERY_INCOHERENT_DATA_FOUND, ErrorWarningType.BIZ_RULE,
					"No Credit Card Payments");
		}

		Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();
		extChgMap.put(ccChgDTO.getExternalChargesEnum(), ccChgDTO);
		Map<String, AmadeusPaxTO> paxMap = new HashMap<String, AmadeusPaxTO>();

		if (amaTLAPayAndBookRQ.getSSRGroup() != null) {
			SsrBD ssrBD = WebServicesModuleUtils.getSsrServiceBD();
			Collections.sort(rphArr, new Comparator<Object[]>() {
				@Override
				public int compare(Object[] o1, Object[] o2) {
					return ((Date) o1[0]).compareTo(((Date) o2[0]));
				}

			});
			Map<String, Integer> segmentSeqMap = new HashMap<String, Integer>();
			int i = 1;
			for (Object[] obj : rphArr) {
				segmentSeqMap.put((String) obj[1], i++);
			}
			for (TLAPassengerSSRDataTypeB paxData : amaTLAPayAndBookRQ.getSSRGroup().getPassengerSSRData()) {
				paxData.getPassengerRPHs().getPassengerRPH().iterator().next().getValue();
				paxData.getFlightSegmentRPHs().getFlightSegmentRPH().iterator().next().getValue();
				if (paxData.getType() == TLAFareCategoryType.SSR) {
					List<SSR> ssrCodeList = ssrBD.getSSR(paxData.getCode());
					composeSSRAsm(paxMap, paxData, ssrCodeList, segmentSeqMap);
				}

			}
		}

		boolean isBypassingCreditCard = false;

		String[] byPassingCreditCard = { "", "" };

		ExternalChargesMediator chgMediator = new ExternalChargesMediator(paxMap.values(), extChgMap, true, true);
		chgMediator.setCalculateJNTaxForCCCharge(true);
		for (TLAPaymentDetailTypeBRQ paymentDetail : amaTLAPayAndBookRQ.getFormsOfPayment().getPayment()) {
			if (paymentDetail.getPaymentCard() != null) {
				AmadeusValidationUtil.validateCreditCardDetails(paymentDetail.getPaymentCard());

				// Replaces sent credit card number with test credit card number if bypass enabled

				if (WebServicesModuleUtils.getAmadeusConfig().isCreditCardBypassEnabled()) {

					Map bypassCCMap = WebServicesModuleUtils.getAmadeusConfig().getBypassingCreditCardNoMap();

					if (bypassCCMap.containsKey(paymentDetail.getPaymentCard().getCardNumber())
							&& paymentDetail.getPaymentCard().getCardCode()
									.equalsIgnoreCase(bypassCCMap.get(paymentDetail.getPaymentCard().getCardNumber()).toString())) {

						isBypassingCreditCard = true;
						byPassingCreditCard[0] = paymentDetail.getPaymentCard().getCardCode();
						byPassingCreditCard[1] = paymentDetail.getPaymentCard().getCardNumber();
						paymentDetail.getPaymentCard().setCardCode(
								WebServicesModuleUtils.getAmadeusConfig().getReplacingCreditCardType());
						paymentDetail.getPaymentCard().setCardNumber(
								WebServicesModuleUtils.getAmadeusConfig().getReplacingCreditCardNo());
					} else {
						throw new AmadeusWSException(AmadeusErrorCodes.YOUR_CREDIT_CARD_COULD_NOT_BE_PROCESSED_PLEASE_CHECK,
								ErrorWarningType.BIZ_RULE, "General decline of the card");
					}
				}

			}
		}

		IReservation iReservation = composeReservationAssembler(amaTLAPayAndBookRQ.getPassengerList(),
				amaTLAPayAndBookRQ.getContactPoint(), amaTLAPayAndBookRQ.getFormsOfPayment(), ondFareContainer, chgMediator,
				paxMap, new int[] { adultCount, childCount, infantCount }, currencyCode, segIdOriginatorMap);

		// Only single record locator is supported in create booking
		TLAUniqueIDType recordLocator = amaTLAPayAndBookRQ.getRecordLocatorsList().getRecordLocator().iterator().next();
		iReservation.setExtRecordLocator(AmadeusCommonUtil.getAmadeusRLoc(recordLocator, amaTLAPayAndBookRQ.getCreationDate()));
		// Set reservation admin info
		if (amaTLAPayAndBookRQ.getPOS() != null) {
			if (amaTLAPayAndBookRQ.getPOS().getSource() != null && amaTLAPayAndBookRQ.getPOS().getSource().size() > 0) {
				SourceType pos = amaTLAPayAndBookRQ.getPOS().getSource().get(0);
				ReservationAdminInfoTO reservationAdminInfoTO = new ReservationAdminInfoTO();
				String salesTerminal = "";
				if (pos != null) {
					if (pos.getAgentSine() != null) {
						salesTerminal += pos.getAgentSine();
					}
					if (pos.getAgentDutyCode() != null) {
						salesTerminal += ("_" + pos.getAgentDutyCode());
					}
					if (pos.getRequestorID() != null) {
						if (pos.getRequestorID().getID() != null) {
							salesTerminal += ("_" + pos.getRequestorID().getID());
						}
					}
					salesTerminal = salesTerminal.trim();
					int targetLength = 250;
					if (salesTerminal.length() > targetLength) {
						salesTerminal = salesTerminal.substring(0, targetLength);
					}
					reservationAdminInfoTO.setOriginSalesTerminal(salesTerminal);
					reservationAdminInfoTO.setLastSalesTerminal(salesTerminal);
					iReservation.setReservationAdminInfoTO(reservationAdminInfoTO);
				}
			}
		}
		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();

		ServiceResponce serviceResponce = null;
		Collection collBlockID = null;
		String pnr = null;
		try {
			collBlockID = WebServicesModuleUtils.getReservationBD().blockSeats(ondFareContainer.getOndFareDTOs(), null);

			TrackInfoDTO trackInfoDTO = getTrackInfo();
			trackInfoDTO.setPaymentAgent(userPrincipal.getAgentCode());
			serviceResponce = WebServicesModuleUtils.getReservationBD().createCCReservation(iReservation, collBlockID,
					trackInfoDTO, AppSysParamsUtil.isFraudCheckEnabled(userPrincipal.getSalesChannel()), true);
			pnr = (String) serviceResponce.getResponseParam(CommandParamNames.PNR);
		} catch (Exception ex) {
			if (collBlockID != null) {
				try {
					if (ex instanceof RuntimeException || (ex.getCause() != null && ex.getCause() instanceof RuntimeException)) {
						WebServicesModuleUtils.getReservationBD().releaseBlockedSeats(collBlockID);
					}
				} catch (Exception ex2) {
					log.error("Releasing blocked seats failed", ex2);
				}
			}
			log.error("Error in creating booking", ex);
			AmadeusErrorCodes exceptionCode = AmadeusErrorCodes.GENERIC_QUERY_INCOHERENT_DATA_FOUND;
			if (ex.getCause() != null && (ex.getCause() instanceof com.isa.thinair.commons.api.exception.ICommonsException)) {
				com.isa.thinair.commons.api.exception.ICommonsException me = (com.isa.thinair.commons.api.exception.ICommonsException) ex
						.getCause();
				if ("airreservations.arg.cc.error".equals(me.getExceptionCode())) {
					exceptionCode = AmadeusErrorCodes.YOUR_CREDIT_CARD_COULD_NOT_BE_PROCESSED_PLEASE_CHECK;
					throw new AmadeusWSException(exceptionCode, ErrorWarningType.BIZ_RULE, "General decline of the card.");
				} else if (me.getExceptionCode().contains("paymentbroker.error")
						|| me.getExceptionCode().contains("paymentbroker.result.null")
						|| me.getExceptionCode().contains("paymentbroker.reverse.invalid")
						|| me.getExceptionCode().contains("paymentbroker.invalid.parameters")
						|| me.getExceptionCode().equals("airreservations.arg.cc.noresponse")) {
					exceptionCode = AmadeusErrorCodes.PAYMENT_NOT_APPROVED_RETRY_LATER;
					throw new AmadeusWSException(exceptionCode, ErrorWarningType.BIZ_RULE,
							"No response from authorization service.");
				} else if (me.getExceptionCode().equals("paymentbroker.invalid.expdate")) {
					exceptionCode = AmadeusErrorCodes.INVALID_EXPIRATION_DATE;
					throw new AmadeusWSException(exceptionCode, ErrorWarningType.BIZ_RULE,
							"The expiry date for the selected card is no longer valid.");
				} else if (me.getExceptionCode().equals("paymentbroker.invalid.cvv")) {
					exceptionCode = AmadeusErrorCodes.INVALID_OR_MISSING_CREDI_CARD_SECURITY_CODE;
					throw new AmadeusWSException(exceptionCode, ErrorWarningType.BIZ_RULE, "Security code verification failed");
				}
			}
			throw new AmadeusWSException(exceptionCode, ErrorWarningType.BIZ_RULE, "Booking creation failed.");
		}

		if (serviceResponce != null && serviceResponce.isSuccess()) {
			Map<String, String> paymentRefMap = (Map<String, String>) serviceResponce
					.getResponseParam(CommandParamNames.PAYMENT_AUTHID_MAP);
			TLAPaymentDetailsTypeRS paymentStatus = new TLAPaymentDetailsTypeRS();
			for (TLAPaymentDetailTypeBRQ paymentDetail : amaTLAPayAndBookRQ.getFormsOfPayment().getPayment()) {
				if (paymentDetail.getPaymentCard() != null) {

					if (WebServicesModuleUtils.getAmadeusConfig().isCreditCardBypassEnabled() && isBypassingCreditCard) {
						paymentDetail.getPaymentCard().setCardCode(byPassingCreditCard[0]);
						paymentDetail.getPaymentCard().setCardNumber(byPassingCreditCard[1]);
					}

					TLAPaymentDetailTypeRS paymentRS = new TLAPaymentDetailTypeRS();
					paymentRS.setPaymentCard(paymentDetail.getPaymentCard());
					PaymentAmount payAmt = new PaymentAmount();
					payAmt.setAmount(paymentDetail.getPaymentAmount().getAmount());
					payAmt.setCurrencyCode(paymentDetail.getPaymentAmount().getCurrencyCode());
					payAmt.setApprovalCode(paymentRefMap.get(paymentDetail.getPaymentCard().getCardNumber()));
					paymentRS.setPaymentAmount(payAmt);
					paymentStatus.getPayment().add(paymentRS);
				}
			}

			CompanyNameType company = new CompanyNameType();
			company.setCode(AppSysParamsUtil.getDefaultCarrierCode());

			TLAUniqueIDType rLoc = new TLAUniqueIDType();
			rLoc.setType(UniqueIDObjectType.RESERVATION);
			// FIXME :Removes first digit of the PNR to be compatible with Amadeus, temporary solution
			rLoc.setID(pnr.substring(1));
			rLoc.setCompanyName(company);

			payAndBookRS.setReplyStatus(TLAReplyStatusType.CONFIRMED);
			payAndBookRS.setPaymentStatus(paymentStatus);
			payAndBookRS.setDisplayCurrencyCode(amaTLAPayAndBookRQ.getDisplayCurrencyCode());
			payAndBookRS.getRecordLocator().add(rLoc);
			payAndBookRS.getRecordLocator().addAll(amaTLAPayAndBookRQ.getRecordLocatorsList().getRecordLocator());

			// Email the itinerary for CNF booking

			// Note : For Maroc prices are given out to Amadeus in EUR. To not to create a conflict, itinerary
			// would be emailed without charges details in PASSENGER DETAILS section and present payment amount
			// under payment details section
			try {

				// Gets contact info email from request, validate and send
				List<String> email = amaTLAPayAndBookRQ.getContactPoint().getEmail();

				if (email != null && email.size() > 0) {
					if (AmadeusCommonUtil.isValidEmailAddress(email.get(0))) {
						// Email itinerary
						ItineraryLayoutModesDTO itineraryLayoutModesDTO = new ItineraryLayoutModesDTO();
						itineraryLayoutModesDTO.setPnr(pnr);
						itineraryLayoutModesDTO.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
						itineraryLayoutModesDTO.setLocale(new Locale(WebServicesModuleUtils.getAmadeusConfig()
								.getDefaultLanguage()));
						itineraryLayoutModesDTO.setIncludePassengerPrices(false);
						itineraryLayoutModesDTO.setIncludeDetailCharges(false);
						WebServicesModuleUtils.getReservationBD().emailItinerary(itineraryLayoutModesDTO, null, null, false);

					} else {
						log.error("Invalid contact email address. Could not send itinerary for PNR " + pnr);
					}
				} else {
					log.error("Contact email address not specified. Could not send itineraray for PNR " + pnr);
				}

			} catch (Exception ex) { // do not fail even if itinerary email fails
				log.error("Error in emailing itinerary for PNR " + pnr, ex);
			}
		} else {
			throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_QUERY_INCOHERENT_DATA_FOUND, ErrorWarningType.BIZ_RULE,
					"Booking creation failed.");
		}
	}

	/**
	 * Compose SSR Assembler
	 * 
	 * @param paxMap
	 * @param paxData
	 * @param ssrCodeList
	 * @param segmentSeqMap
	 */
	private static void composeSSRAsm(Map<String, AmadeusPaxTO> paxMap, TLAPassengerSSRDataTypeB paxData, List<SSR> ssrCodeList,
			Map<String, Integer> segmentSeqMap) {
		Map<String, Integer> segSSRIdMap = new HashMap<String, Integer>();
		for (TLAAssociationType segRPH : paxData.getFlightSegmentRPHs().getFlightSegmentRPH()) {
			for (SSR ssr : ssrCodeList) {
				segSSRIdMap.put(segRPH.getValue(), ssr.getSsrId());
			}
		}
		for (TLAAssociationType paxRPH : paxData.getPassengerRPHs().getPassengerRPH()) {
			if (!paxMap.containsKey(paxRPH.getValue())) {
				paxMap.put(paxRPH.getValue(), new AmadeusPaxTO());
			}
			AmadeusPaxTO amaPax = paxMap.get(paxRPH.getValue());
			if (amaPax.getSSRAssembler() == null) {
				amaPax.setSegmentSSRAssembler(new SegmentSSRAssembler());
			}
			SegmentSSRAssembler ssrAsm = amaPax.getSSRAssembler();
			for (String segRPH : segSSRIdMap.keySet()) {
				ssrAsm.addPaxSegmentSSR(segmentSeqMap.get(segRPH), segSSRIdMap.get(segRPH), "", paxData.getOriginatorSSRID()
						.getID());
			}
		}

	}

	/**
	 * Get tracking info
	 * 
	 * @return
	 */
	private static TrackInfoDTO getTrackInfo() {
		String sessionId = (String) ThreadLocalData.getWSContextParam(WebservicesContext.WEB_SESSION_ID);
		String ipAddress = BeanUtils.nullHandler(ThreadLocalData.getWSContextParam(
				WebservicesContext.WEB_REQUEST_IP));

		TrackInfoDTO trackInfo = new TrackInfoDTO();
		trackInfo.setAppIndicator(AppIndicatorEnum.APP_WS);
		trackInfo.setSessionId(sessionId);
		trackInfo.setIpAddress(ipAddress);
		if (ipAddress.length() > 0) {
			long ipNumber = AmadeusCommonUtil.getReservationOriginIPAddress(ipAddress);
			trackInfo.setOriginIPNumber(ipNumber);
		}
		trackInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());

		return trackInfo;
	}

	/**
	 * Compose reservation assembler
	 * 
	 * @param tlaPassengersType
	 * @param tlaContactType
	 * @param tlaPaymentDetailsTypeBRQ
	 * @param ondFareContainer
	 * @param chgMediator
	 * @param paxMap
	 * @param paxCounts
	 * @param currencyCode
	 * @param segIdOriginatorMap
	 * @return
	 * @throws ModuleException
	 */
	private static IReservation composeReservationAssembler(TLAPassengersType tlaPassengersType, TLAContactType tlaContactType,
			TLAPaymentDetailsTypeBRQ tlaPaymentDetailsTypeBRQ, SelectedOndFareContainer ondFareContainer,
			ExternalChargesMediator chgMediator, Map<String, AmadeusPaxTO> paxMap, int[] paxCounts, String currencyCode,
			Map<Integer, String> segIdOriginatorMap) throws ModuleException {

		IReservation iReservation = new ReservationAssembler(ondFareContainer.getOndFareDTOs(), null);
		// Setting selected currency to reservation
		iReservation.setLastCurrencyCode(currencyCode);
		populateContactInfo(iReservation, tlaContactType);
		populatePaxInfo(iReservation, tlaPassengersType, ondFareContainer.getOndFareDTOs(), chgMediator, paxMap, paxCounts,
				currencyCode, tlaPaymentDetailsTypeBRQ);
		populateOndSegments(iReservation, ondFareContainer, segIdOriginatorMap);
		return iReservation;
	}

	/**
	 * Populate Ond Segments and set Ids
	 * 
	 * @param iReservation
	 * @param ondFareContainer
	 * @param segIdOriginatorMap
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private static void populateOndSegments(IReservation iReservation, SelectedOndFareContainer ondFareContainer,
			Map<Integer, String> segIdOriginatorMap) throws ModuleException {
		int segmentSeq = 1;
		int intOndGrp = 1;
		int intReturnOndGrp = 1;
		for (SelectedOndFareDTO ondFareDTO : ondFareContainer.getSelectedOndFareDTOs()) {
			int fareTypeId = ondFareDTO.getFareType();
			Object[] inOutboundSegments = ReservationUtil.extractNewResSegmentsInfo(ondFareDTO.getFareDTOs(), 1, 1);

			Collection outBoundSegments = (Collection) inOutboundSegments[0];
			Collection inBoundSegments = (Collection) inOutboundSegments[1];

			Date openRTConfirmExpiry = OndFareUtil.getOpenReturnPeriods(ondFareDTO.getFareDTOs()).getConfirmBefore();

			for (Iterator obSegIt = outBoundSegments.iterator(); obSegIt.hasNext();) {
				ReservationSegmentTO segmentInfo = (ReservationSegmentTO) obSegIt.next();

				iReservation.addOutgoingSegment(segmentSeq++, segmentInfo.getFlightSegId(), intOndGrp, intReturnOndGrp,
						segIdOriginatorMap.get(segmentInfo.getFlightSegId()), segmentInfo.getSelectedBundledFarePeriodId(), null, null);
				if (fareTypeId == FareTypes.PER_FLIGHT_FARE_AND_OND_FARE || fareTypeId == FareTypes.PER_FLIGHT_FARE) {
					intOndGrp++;
					intReturnOndGrp++;
				}
			}

			if (fareTypeId == FareTypes.RETURN_FARE || fareTypeId == FareTypes.OND_FARE
					|| fareTypeId == FareTypes.OND_FARE_AND_PER_FLIGHT_FARE || fareTypeId == FareTypes.HALF_RETURN_FARE) {
				intOndGrp++;
			}
			if (fareTypeId == FareTypes.OND_FARE || fareTypeId == FareTypes.OND_FARE_AND_PER_FLIGHT_FARE) {
				intReturnOndGrp++;
			}

			if (inBoundSegments != null) {
				for (Iterator ibSegIt = inBoundSegments.iterator(); ibSegIt.hasNext();) {
					ReservationSegmentTO segmentInfo = (ReservationSegmentTO) ibSegIt.next();
					iReservation.addReturnSegment(segmentSeq++, segmentInfo.getFlightSegId(), intOndGrp, openRTConfirmExpiry,
							intReturnOndGrp, segIdOriginatorMap.get(segmentInfo.getFlightSegId()),
							segmentInfo.getSelectedBundledFarePeriodId(), null, null);
					if (fareTypeId == FareTypes.OND_FARE_AND_PER_FLIGHT_FARE || fareTypeId == FareTypes.PER_FLIGHT_FARE) {
						intOndGrp++;
						intReturnOndGrp++;
					}
				}
			}

		}
	}

	/**
	 * Populate passenger information
	 * 
	 * @param iReservation
	 * @param passengerList
	 * @param ondFareDTOs
	 * @param chgMediator
	 * @param paxMap
	 * @param paxCounts
	 * @param currencyCode
	 * @param tlaPaymentDetailsTypeBRQ
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void populatePaxInfo(IReservation iReservation, TLAPassengersType passengerList,
			Collection<OndFareDTO> ondFareDTOs, ExternalChargesMediator chgMediator, Map<String, AmadeusPaxTO> paxMap,
			int[] paxCounts, String currencyCode, TLAPaymentDetailsTypeBRQ tlaPaymentDetailsTypeBRQ) throws ModuleException {

		Set<TLAPassengerType> infants = new HashSet<TLAPassengerType>();
		IPGPaymentOptionDTO ipgPaymentOptionDTO = new IPGPaymentOptionDTO();
		ipgPaymentOptionDTO.setSelCurrency(currencyCode);
		ipgPaymentOptionDTO.setModuleCode("XBE");
		List<IPGPaymentOptionDTO> pgwList = WebServicesModuleUtils.getPaymentBrokerBD().getPaymentGateways(ipgPaymentOptionDTO);
		IPGPaymentOptionDTO selectedPGW = null;
		for (IPGPaymentOptionDTO pgw : pgwList) {
			if (pgw.getSelCurrency().equals(currencyCode)) {
				selectedPGW = pgw;
				break;
			}
		}

		if (selectedPGW == null) {
			String defaultCardPayCurrency = AppSysParamsUtil.getDefaultPGCurrency();
			for (IPGPaymentOptionDTO pgw : pgwList) {
				if (pgw.getSelCurrency().equals(defaultCardPayCurrency)) {
					selectedPGW = pgw;
					break;
				}
			}
		}

		int ipgID;
		String ipgCurrencyCode;
		if (selectedPGW != null) {
			ipgID = selectedPGW.getPaymentGateway();
			ipgCurrencyCode = selectedPGW.getSelCurrency();
		} else {
			ipgID = pgwList.get(0).getPaymentGateway();
			ipgCurrencyCode = pgwList.get(0).getSelCurrency();
		}

		log.debug("Amadeus bookings - card payment gateway [Payment Currency = " + ipgCurrencyCode + " ,pgwID=" + ipgID + "]");
		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgID, ipgCurrencyCode);

		boolean isPaymentByBaseCurrency = false;

		CurrencyExchangeRate exchangeRate = AmadeusCommonUtil.getCurrencyExchangeRate(ipgCurrencyCode);

		if (exchangeRate == null) {
			isPaymentByBaseCurrency = true;
		}

		Map<String, BigDecimal[]> fareMap = new HashMap<String, BigDecimal[]>();

		BigDecimal[] adultFare = { BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO };
		BigDecimal[] childFare = { BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO };
		BigDecimal[] infantFare = { BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO };
		fareMap.put(AmadeusConstants.ADULT, adultFare);
		fareMap.put(AmadeusConstants.CHILD, childFare);
		fareMap.put(AmadeusConstants.INFANT, infantFare);

		for (OndFareDTO fareDTO : ondFareDTOs) {

			adultFare = fareMap.get(AmadeusConstants.ADULT);
			adultFare[0] = AccelAeroCalculator.add(adultFare[0], AccelAeroCalculator.parseBigDecimal(fareDTO.getAdultFare()));
			adultFare[1] = AccelAeroCalculator.add(adultFare[1],
					AccelAeroCalculator.parseBigDecimal(fareDTO.getTotalCharges()[0]));

			childFare = fareMap.get(AmadeusConstants.CHILD);
			childFare[0] = AccelAeroCalculator.add(childFare[0], AccelAeroCalculator.parseBigDecimal(fareDTO.getChildFare()));
			childFare[1] = AccelAeroCalculator.add(childFare[1],
					AccelAeroCalculator.parseBigDecimal(fareDTO.getTotalCharges()[2]));

			infantFare = fareMap.get(AmadeusConstants.INFANT);
			infantFare[0] = AccelAeroCalculator.add(infantFare[0], AccelAeroCalculator.parseBigDecimal(fareDTO.getInfantFare()));
			infantFare[1] = AccelAeroCalculator.add(infantFare[1],
					AccelAeroCalculator.parseBigDecimal(fareDTO.getTotalCharges()[1]));

			if (!isPaymentByBaseCurrency) {
				adultFare[2] = AccelAeroCalculator.add(
						adultFare[2],
						new BigDecimal(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(
								AccelAeroCalculator.parseBigDecimal(fareDTO.getAdultFare()),
								exchangeRate.getMultiplyingExchangeRate()))));
				adultFare[3] = AccelAeroCalculator.add(
						adultFare[3],
						new BigDecimal(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(
								AccelAeroCalculator.parseBigDecimal(fareDTO.getTotalCharges()[0]),
								exchangeRate.getMultiplyingExchangeRate()))));

				childFare[2] = AccelAeroCalculator.add(
						childFare[2],
						new BigDecimal(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(
								AccelAeroCalculator.parseBigDecimal(fareDTO.getChildFare()),
								exchangeRate.getMultiplyingExchangeRate()))));
				childFare[3] = AccelAeroCalculator.add(
						childFare[3],
						new BigDecimal(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(
								AccelAeroCalculator.parseBigDecimal(fareDTO.getTotalCharges()[2]),
								exchangeRate.getMultiplyingExchangeRate()))));

				infantFare[2] = AccelAeroCalculator.add(
						infantFare[2],
						new BigDecimal(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(
								AccelAeroCalculator.parseBigDecimal(fareDTO.getInfantFare()),
								exchangeRate.getMultiplyingExchangeRate()))));
				infantFare[3] = AccelAeroCalculator.add(
						infantFare[3],
						new BigDecimal(AccelAeroCalculator.formatAsDecimal(AccelAeroCalculator.multiply(
								AccelAeroCalculator.parseBigDecimal(fareDTO.getTotalCharges()[1]),
								exchangeRate.getMultiplyingExchangeRate()))));
			}

			fareMap.put(AmadeusConstants.ADULT, adultFare);
			fareMap.put(AmadeusConstants.CHILD, childFare);
			fareMap.put(AmadeusConstants.INFANT, infantFare);

		}

		TLAPaymentCardTypeRS ccPayment = null;
		AmadeusCardAdapter ccAdapt = null;
		for (TLAPaymentDetailTypeBRQ fop : tlaPaymentDetailsTypeBRQ.getPayment()) {
			if (fop.getPaymentCard() != null) {
				ccPayment = fop.getPaymentCard();
			}
		}
		if (ccPayment == null) {
			throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_QUERY_INCOHERENT_DATA_FOUND,
					ErrorWarningType.REQUIRED_FIELD_MISSING, "No credit card payments were found");
		} else {
			ccAdapt = new AmadeusCardAdapter(ccPayment);
		}

		LinkedList perPaxChgs = chgMediator.getExternalChargesForAFreshPayment(paxCounts[0] + paxCounts[1], 0);

		for (TLAPassengerType pax : passengerList.getPassenger()) {
			if (pax.getPassengerTypeCode().equals(AmadeusConstants.INFANT)) {
				infants.add(pax);
				continue;
			} else {
				AmadeusPaxTO amaPax = paxMap.get(pax.getTravelerRefNumber().getRPH());
				IPayment iPayment = new PaymentAssembler();

				Collection<ExternalChgDTO> extChgs = new ArrayList<ExternalChgDTO>();

				if (perPaxChgs != null) {
					Object extCharges = perPaxChgs.pop();
					if (extCharges != null) {
						extChgs.addAll((Collection<ExternalChgDTO>) extCharges);
					}
				}

				SegmentSSRAssembler ssrAsm = null;
				if (amaPax != null) {
					extChgs.addAll(amaPax.getExternalCharges());
					ssrAsm = (amaPax.getSSRAssembler() != null) ? amaPax.getSSRAssembler() : new SegmentSSRAssembler();
				} else {
					ssrAsm = new SegmentSSRAssembler();
				}

				BigDecimal totalFare = AccelAeroCalculator.add(fareMap.get(pax.getPassengerTypeCode())[0],
						fareMap.get(pax.getPassengerTypeCode())[1]);

				if (pax.getAccompaniedInfantRef() != null && !"".equals(pax.getAccompaniedInfantRef())) {
					/* assuming only infant doesn't have external charges */
					totalFare = AccelAeroCalculator.add(totalFare, fareMap.get(AmadeusConstants.INFANT)[0],
							fareMap.get(AmadeusConstants.INFANT)[1]);
				}

				PayCurrencyDTO payCurrencyDTO = null;
				if (!isPaymentByBaseCurrency) {
					payCurrencyDTO = new PayCurrencyDTO(ipgCurrencyCode, exchangeRate.getMultiplyingExchangeRate());
					payCurrencyDTO.setSkipCurrencyConversion(true);

					// Assumes there is no external charge other than CC charge
					BigDecimal totalPerPaxAmountInPayCurrency = BigDecimal.ZERO;
					BigDecimal totalPerPaxExtChargesInPayCurrency = BigDecimal.ZERO;

					if (extChgs.size() > 0) {
						for (ExternalChgDTO ext : extChgs) {
							totalPerPaxExtChargesInPayCurrency = AccelAeroCalculator.add(totalPerPaxExtChargesInPayCurrency,
									AmadeusCommonUtil.getDispValue(ext.getAmount(), exchangeRate));
						}
					}

					totalPerPaxAmountInPayCurrency = AccelAeroCalculator.add(totalPerPaxAmountInPayCurrency,
							fareMap.get(pax.getPassengerTypeCode())[2], fareMap.get(pax.getPassengerTypeCode())[3],
							totalPerPaxExtChargesInPayCurrency);

					if (pax.getAccompaniedInfantRef() != null && !"".equals(pax.getAccompaniedInfantRef())) {
						/* assuming only infant doesn't have external charges */
						totalPerPaxAmountInPayCurrency = AccelAeroCalculator.add(totalPerPaxAmountInPayCurrency,
								fareMap.get(AmadeusConstants.INFANT)[2], fareMap.get(AmadeusConstants.INFANT)[3]);
					}

					payCurrencyDTO.setTotalPayCurrencyAmount(totalPerPaxAmountInPayCurrency);
				}

				// Set payment amount in payment currency amount

				iPayment.addExternalCharges(extChgs);

				iPayment.addCardPayment(ccAdapt.getType(), ccAdapt.getExpDate(), ccAdapt.getCardNo(), ccAdapt.getName(),
						ccAdapt.getAddress(), ccAdapt.getSecurityCode(), totalFare, AppIndicatorEnum.APP_XBE,
						TnxModeEnum.MAIL_TP_ORDER, null, ipgIdentificationParamsDTO, null, payCurrencyDTO, null, null, null, null);

				addPassengerToReservation(iReservation, pax, iPayment, ssrAsm);
			}
		}

		for (TLAPassengerType pax : infants) {
			IPayment iPaymentInf = new PaymentAssembler();
			addPassengerToReservation(iReservation, pax, iPaymentInf, new SegmentSSRAssembler());
		}
	}

	private static void addPassengerToReservation(IReservation iReservation, TLAPassengerType pax, IPayment paymentAsm,
			SegmentSSRAssembler ssrAssembler) throws ModuleException {
		String firstName = AmadeusCommonUtil.join(pax.getPersonName().getGivenName(), " ");
		String lastName = pax.getPersonName().getSurname();
		String title = pax.getPersonName().getNamePrefix().iterator().next();
		Date dateOfBirth = null;
		Integer nationalityCode = null;
		int paxSequence = Integer.parseInt(pax.getTravelerRefNumber().getRPH());
		PaxAdditionalInfoDTO paxAdditionalInfoDTO = new PaxAdditionalInfoDTO();
		// String eTicketNo = pax.getOriginatorPassengerID().getID();
		if (pax.getPassengerTypeCode().equals(AmadeusConstants.ADULT)) {
			if (pax.getAccompaniedInfantRef() != null) {
				int infantSeqId = Integer.parseInt(pax.getAccompaniedInfantRef());
				iReservation.addParent(firstName, lastName, title, dateOfBirth, nationalityCode, paxSequence, infantSeqId,
						paxAdditionalInfoDTO, null, paymentAsm, ssrAssembler, null, null, null, null,
						CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);
			} else {
				iReservation.addSingle(firstName, lastName, title, dateOfBirth, nationalityCode, paxSequence,
						paxAdditionalInfoDTO, null, paymentAsm, ssrAssembler, null, null, null, null,
						CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);
			}

		} else if (pax.getPassengerTypeCode().equals(AmadeusConstants.CHILD)) {
			iReservation.addChild(firstName, lastName, title, dateOfBirth, nationalityCode, paxSequence, paxAdditionalInfoDTO,
					null, paymentAsm, ssrAssembler, null, null, null, null, CommonsConstants.INDIVIDUAL_MEMBER, null,
					null, null, null, null);

		} else if (pax.getPassengerTypeCode().equals(AmadeusConstants.INFANT)) {
			int parentSequence = Integer.parseInt(pax.getAccompanyingAdultRef());
			iReservation.addInfant(firstName, lastName, title, dateOfBirth, nationalityCode, paxSequence, parentSequence,
					paxAdditionalInfoDTO, null, paymentAsm, new SegmentSSRAssembler(), null, null, null, null,
					CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);

		}
	}

	/**
	 * Populate contact information
	 * 
	 * @param iReservation
	 * @param contactPoint
	 */
	private static void populateContactInfo(IReservation iReservation, TLAContactType contactPoint) throws ModuleException {
		ReservationContactInfo contactInfo = new ReservationContactInfo();
		String fName = AmadeusCommonUtil.join(contactPoint.getPersonName().getGivenName(), " ");
		contactInfo.setFirstName(fName);
		contactInfo.setLastName(contactPoint.getPersonName().getSurname());

		// only the first prefix is used
		contactInfo.setTitle(contactPoint.getPersonName().getNamePrefix().iterator().next());
		if (contactPoint.getEmail().size() > 0) {
			contactInfo.setEmail(contactPoint.getEmail().get(0));
		}
		if (contactPoint.getTelephone() == null || contactPoint.getTelephone().size() == 0) {
			throw new AmadeusWSException(AmadeusErrorCodes.TELEPHONE_NUMBER_REQUIRED, ErrorWarningType.BIZ_RULE,
					"Invalid booking request. Please provide at least one of HomePhone MobilePhone or WorkPhone");
		}
		if (contactPoint.getTelephone().size() >= 1) {
			TelephoneInfoType tl = contactPoint.getTelephone().get(0);
			contactInfo.setPhoneNo(tl.getPhoneNumber());
		}
		if (contactPoint.getTelephone().size() >= 2) {
			TelephoneInfoType tl = contactPoint.getTelephone().get(1);
			contactInfo.setMobileNo(tl.getPhoneNumber());
		}

		List<TLAAddressType> addresses = contactPoint.getAddress();
		if (addresses != null && addresses.size() > 0) {
			TLAAddressType addr = addresses.get(0);
			contactInfo.setCity(addr.getCityName());
			int i = 0;

			if (addr.getAddressLine() != null) {
				String line1 = null;
				String line2 = null;
				for (String addrLine : addr.getAddressLine()) {
					if (i == 0) {
						line1 = addrLine;
					} else if (line2 == null) {
						line2 = addrLine;
					} else {
						line2 += "," + addrLine;
					}
				}
				if (line1 != null) {
					contactInfo.setStreetAddress1(line1);
				}
				if (line2 != null) {
					contactInfo.setStreetAddress2(line2);
				}
			}
			if (addr.getCountryName() != null) {
				if (addr.getCountryName().getCode() != null) {
					contactInfo.setCountryCode(addr.getCountryName().getCode());
				} else if (addr.getCountryName().getValue() != null && addr.getCountryName().getValue().length() == 2) {
					contactInfo.setCountryCode(addr.getCountryName().getValue().toUpperCase());
				}
			}
		}
		// TODO Replace this if Amadeus requests supports other languages
		if (contactInfo.getPreferredLanguage() == null) {
			contactInfo.setPreferredLanguage(WebServicesModuleUtils.getAmadeusConfig().getDefaultLanguage());
		}
		iReservation.addContactInfo("", contactInfo, null);
	}

	/**
	 * Check for matching booking class
	 * 
	 * @param ondFareDTOColl
	 * @param faresForSegment
	 * @param segIdRPHMap
	 * @return
	 */
	private static boolean isMatchingBookingClass(Collection<OndFareDTO> ondFareDTOColl, List<TLAFareTypeRQ> faresForSegment,
			Map<Integer, String> segIdRPHMap) {
		if (ondFareDTOColl == null) {
			return false;
		}
		Map<String, FareSummaryDTO> fareMap = new HashMap<String, FareSummaryDTO>();
		for (OndFareDTO ondFareDTO : ondFareDTOColl) {
			if (ondFareDTO.getSegmentsMap().values().size() > 1) {
				continue; // FIXME connection fares not supported
			}
			FlightSegmentDTO segment = ondFareDTO.getSegmentsMap().values().iterator().next();
			String segmentRPH = segIdRPHMap.get(segment.getSegmentId());
			fareMap.put(segmentRPH, ondFareDTO.getFareSummaryDTO());
		}

		boolean found = true;
		if (faresForSegment.size() <= 0 || fareMap.size() <= 0) {
			found = false;
		}
		String bc = null;
		String fb = null;
		for (TLAFareTypeRQ ffSeg : faresForSegment) {
			TLABookingClassDataTypeRQ bcData = ffSeg.getBookingClassData();
			bc = bcData.getFareBasis().getResBookDesigCode();
			if (bc == null) {
				bc = AmadeusCommonUtil.getBookingClassFromAmadeusFBC(bcData.getFareBasis().getValue());
			}

			fb = AmadeusCommonUtil.getFareBasisFromAmadeusFBC(bcData.getFareBasis().getValue());
			FareSummaryDTO fsDTO = fareMap.get(ffSeg.getFlightSegmentRPH());
			try {
				String mappedBookingClass = AmadeusCommonUtil.getBookingClassCode(fsDTO);
				String mappedFareCode = AmadeusCommonUtil.getFareCode(fsDTO);

				if (!bc.equals(mappedBookingClass) || !fb.equals(mappedFareCode)) {
					found = false;
					break;
				}

			} catch (AmadeusWSException e) {
				found = false;
			}

		}
		return found;
	}

	private static void generatePriceCombinations(List<AmadeusPriceHolderDTO> currentPriceList,
			TLASingleOriginDestinationTypeRRQ singleOndRQ, Map<String, SelectedOndFareDTO> bookingClassFareMap, int[] paxCounts,
			CurrencyExchangeRate exRate) {

		if (currentPriceList.size() == 0) {

			for (String bookingClass : bookingClassFareMap.keySet()) {

				AmadeusPriceHolderDTO priceHolder = new AmadeusPriceHolderDTO(paxCounts[0], paxCounts[1], paxCounts[2], exRate);
				priceHolder.addEntry(singleOndRQ, bookingClass, bookingClassFareMap.get(bookingClass));
				currentPriceList.add(priceHolder);
			}

		} else {

			List<AmadeusPriceHolderDTO> newPriceHolderList = new ArrayList<AmadeusPriceHolderDTO>();

			for (AmadeusPriceHolderDTO priceHolder : currentPriceList) {

				// Multiply possibilities
				int counter = 0;
				for (String bookingClass : bookingClassFareMap.keySet()) {
					counter++;

					if (counter != bookingClassFareMap.size()) {
						AmadeusPriceHolderDTO copiedPriceHolder = priceHolder.copy();
						copiedPriceHolder.addEntry(singleOndRQ, bookingClass, bookingClassFareMap.get(bookingClass));
						newPriceHolderList.add(copiedPriceHolder);
					} else {
						priceHolder.addEntry(singleOndRQ, bookingClass, bookingClassFareMap.get(bookingClass));
					}

				}

			}

			currentPriceList.addAll(newPriceHolderList);
		}

	}

}
