/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * ===============================================================================
 */
package com.isa.thinair.webservices.core.cache.store.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.cache.store.TransactionStore;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.ITransaction;
import com.isa.thinair.webservices.core.transaction.Transaction;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.isa.thinair.webservices.core.util.WebservicesContext;

/**
 * Manages web services transactions.
 * 
 * @author Nasly
 */
public class InMemoryTransactionStore implements TransactionStore {

	private static final Log log = LogFactory.getLog(InMemoryTransactionStore.class);

	private Map<String, Object> transactions = Collections.synchronizedMap(new HashMap<String, Object>());

	private volatile AtomicBoolean isCleaningRunning = new AtomicBoolean(false);

	private Object lock = new Object();

	@Override
	public ITransaction createNewTransaction() throws WebservicesException {
		ITransaction transaction = new Transaction();
		transactions.put(transaction.getId(), transaction);

		ThreadLocalData.setWSConextParam(WebservicesContext.TRIGGER_NEW_TXN_CREATED, new Boolean(true));

		if (log.isDebugEnabled()) {
			log.debug("Created New Webservices Transaction. " + "[Transaction Id = " + transaction.getId()
					+ ",Creation Timestamp="
					+ CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date(transaction.getCreationTime()))
					+ ",Total Active Transactions = " + transactions.size() + "]");
		}
		return transaction;
	}

	@Override
	public void removeTransaction(String transactionId) throws WebservicesException {
		ITransaction transaction = null;
		try {
			transaction = (ITransaction) transactions.remove(transactionId);
		} finally {
			if (log.isDebugEnabled()) {
				log.debug("After Removing Webservices Transaction. "
						+ "[Transaction Id = "
						+ transactionId
						+ (transaction != null
								? ",lastAccessTimestamp="
										+ CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date(transaction
												.getLastAccessedTimestamp()))
								: "") + "]");
			}
		}
	}

	public boolean isTransactionExists(String transactionId) {
		return transactions.containsKey(transactionId);
	}

	@Override
	public ITransaction getTransaction(String transactionId) throws WebservicesException {
		// This method need to be synchronize only when the removeExpiredTransactions is running.
		if (isCleaningRunning.get()) {
			synchronized (lock) {
				ITransaction iTransaction = (ITransaction) transactions.get(transactionId);
				if (iTransaction.getStatus() == ITransaction.STATUS_EXPIRED) {
					if (log.isDebugEnabled()) {
						log.debug("Access to an expired transaction is requested. " + "[Transaction Id = " + transactionId + "]");
					}
					transactions.remove(transactionId);
					return null;
				} else {
					return iTransaction;
				}
			}
		} else {
			return (ITransaction) transactions.get(transactionId);
		}

	}

	/**
	 * Removes expired user transactions
	 */
	@Override
	public void removeExpiredTransactions() {
		Long startTimestamp = System.currentTimeMillis();
		int countAtStart = transactions.size();
		int clearedCount = 0;
		try {
			isCleaningRunning.set(true);

			if (log.isInfoEnabled())
				log.info("##AAWS## Begin trasnactions timing out [Timestamp="
						+ CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date(System.currentTimeMillis()))
						+ ", Total Transactions = " + transactions.size() + "]");
			Collection<String> expiredTxnIDs = new ArrayList<String>();
			ITransaction transaction = null;

			synchronized (lock) {
				HashSet<String> txnKeys = new HashSet<String>();
				txnKeys.addAll(transactions.keySet());
				for (String txnID : txnKeys) {
					transaction = (ITransaction) transactions.get(txnID);
					if (transaction.qualifyForExpiration(WebServicesModuleUtils.getWebServicesConfig().getTxnTimeoutInMillis())) {
						expiredTxnIDs.add(txnID);
					}
				}
				if (expiredTxnIDs.size() > 0) {
					for (String id : expiredTxnIDs) {
						try {
							removeTransaction(id);
						} catch (WebservicesException e) {
							log.error("In Memory failuer in removeExpiredTransactions for the transaction Id :" + id, e);
						}
					}
				}

				clearedCount = expiredTxnIDs.size();
			}
		} finally {
			isCleaningRunning.set(false);
			if (log.isInfoEnabled())
				log.info("##AAWS## End trasactions timing out [" + "Time Spent (seconds)="
						+ (System.currentTimeMillis() - startTimestamp) / 1000 + ", Timestamp="
						+ CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date(System.currentTimeMillis()))
						+ ", Start Count = " + countAtStart + ", End Count = " + transactions.size() + ", Cleared count = "
						+ clearedCount + "]");
		}
	}

	/**
	 * Forces transaction expiration and then removes the transaction.
	 */
	@Override
	public void forceExpireAndRemoveTransaction(String transactionId) throws WebservicesException {
		ITransaction transaction = (ITransaction) transactions.get(transactionId);
		if (transaction != null) {
			transaction.forceExpiration();
			removeTransaction(transactionId);
		}
	}

	@Override
	public void flushTransaction(ITransaction transaction) throws WebservicesException {
		// Not required since its managed in application Memory
	}
}
