/**
 * 
 */
package com.isa.thinair.webservices.core.config;

import java.util.Map;

/**
 * @author Janaka Padukka
 * 
 */
public class MyIDTravelConfig {

	private Map securityConfigMap;
	
	private boolean autoBaggageSelectionEnabled;
	
	private boolean ticketStatusByEticketEnabled;

	public Map getSecurityConfigMap() {
		return securityConfigMap;
	}

	public void setSecurityConfigMap(Map securityConfigMap) {
		this.securityConfigMap = securityConfigMap;
	}

	public boolean getAutoBaggageSelectionEnabled() {
		return autoBaggageSelectionEnabled;
	}

	public void setAutoBaggageSelectionEnabled(boolean autoBaggageSelectionEnabled) {
		this.autoBaggageSelectionEnabled = autoBaggageSelectionEnabled;
	}

	public boolean getTicketStatusByEticketEnabled() {
		return ticketStatusByEticketEnabled;
	}

	public void setTicketStatusByEticketEnabled(boolean ticketStatusByEticketEnabled) {
		this.ticketStatusByEticketEnabled = ticketStatusByEticketEnabled;
	}
	
}
