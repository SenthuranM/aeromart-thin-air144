package com.isa.thinair.webservices.core.cache.delegator.logic;

import java.util.HashMap;

import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.OTAAirPriceRQ;
import org.opentravel.ota._2003._05.OTAAirPriceRS;
import org.opentravel.ota._2003._05.OTAAirPriceRQ.ServiceTaxCriteriaOptions;

import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxBaseCriteriaDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.cache.delegator.AbstractCacheTemplate;
import com.isa.thinair.webservices.core.cache.delegator.CacheResponse;
import com.isa.thinair.webservices.core.cache.delegator.auditor.impl.PriceQuoteAuditor;
import com.isa.thinair.webservices.core.cache.delegator.keygen.PriceQuoteRequestKey;
import com.isa.thinair.webservices.core.cache.util.CacheConstant.Operation;
import com.isa.thinair.webservices.core.interlineUtil.PriceQuoteInterlineUtil;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.ITransaction;
import com.isa.thinair.webservices.core.transaction.Transaction;

public class CachePriceQuoteSearch extends AbstractCacheTemplate<OTAAirPriceRS, OTAAirPriceRQ> {

	@Override
	public String generateCacheStoreKey(OTAAirPriceRQ request) throws ModuleException, WebservicesException {
		return new PriceQuoteRequestKey(request, getCacheOperationCode()).generateKey();
	}

	@Override
	public OTAAirPriceRS execute(OTAAirPriceRQ request) throws ModuleException, WebservicesException {
		return new PriceQuoteInterlineUtil().getPrice(request);
	}

	@Override
	public OTAAirPriceRS emptyResponse() {
		OTAAirPriceRS response = new OTAAirPriceRS();
		response.setErrors(new ErrorsType());
		return response;
	}

	@Override
	public String getAudit(OTAAirPriceRQ request) {
		PriceQuoteAuditor auditor = new PriceQuoteAuditor(request);
		return auditor.getAudit(Operation.PRICE_QUOTE);
	}

	@Override
	public OTAAirPriceRS cachePostProcess(CacheResponse<OTAAirPriceRS> cacheResponse, OTAAirPriceRQ request)
			throws ModuleException, WebservicesException {
		
		ITransaction currentTransaction = ThreadLocalData.getCurrentTransaction();
		currentTransaction.setAllParameters(new HashMap<>(cacheResponse.getCacheTransactionData()));
		
		// Set specific transaction data
		ServiceTaxBaseCriteriaDTO taxQuoteBaseDTO = (ServiceTaxBaseCriteriaDTO) ThreadLocalData.
													  getCurrentTnxParam(Transaction.SERVICE_TAX_BASE_CRITERIA_TO);
		ServiceTaxCriteriaOptions serviceTaxOptions = request.getServiceTaxCriteriaOptions();
			
		if (taxQuoteBaseDTO != null && serviceTaxOptions != null) {
			taxQuoteBaseDTO.setPaxTaxRegistrationNo(serviceTaxOptions.getTaxRegistrationNo());
			ThreadLocalData.setCurrentTnxParam(Transaction.SERVICE_TAX_BASE_CRITERIA_TO, taxQuoteBaseDTO);
		}		
		
		return cacheResponse.getOtaCacheResponse();
	}
}
