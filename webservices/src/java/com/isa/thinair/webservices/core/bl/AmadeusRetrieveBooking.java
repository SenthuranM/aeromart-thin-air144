package com.isa.thinair.webservices.core.bl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.amadeus.ama._2008._03.AMATLAGetBookingRQ;
import com.amadeus.ama._2008._03.AMATLAGetBookingRS;
import com.amadeus.ama._2008._03.CompanyNameType;
import com.amadeus.ama._2008._03.CountryNameType;
import com.amadeus.ama._2008._03.ErrorWarningType;
import com.amadeus.ama._2008._03.PersonNameType;
import com.amadeus.ama._2008._03.PhoneLocationCategoryType;
import com.amadeus.ama._2008._03.StateType;
import com.amadeus.ama._2008._03.SuccessType;
import com.amadeus.ama._2008._03.TLAAddressType;
import com.amadeus.ama._2008._03.TLAAirportLocationType;
import com.amadeus.ama._2008._03.TLAAssociationType;
import com.amadeus.ama._2008._03.TLABookingClassDataType;
import com.amadeus.ama._2008._03.TLAContactType;
import com.amadeus.ama._2008._03.TLACurrencyAmountType;
import com.amadeus.ama._2008._03.TLAErrorType;
import com.amadeus.ama._2008._03.TLAFareCategoryCode;
import com.amadeus.ama._2008._03.TLAFareCategoryType;
import com.amadeus.ama._2008._03.TLAFareReferenceType;
import com.amadeus.ama._2008._03.TLAFareType;
import com.amadeus.ama._2008._03.TLAFareType.PassengerRPHs;
import com.amadeus.ama._2008._03.TLAFlightSegmentType;
import com.amadeus.ama._2008._03.TLAJourneySingleFareType;
import com.amadeus.ama._2008._03.TLALocationType;
import com.amadeus.ama._2008._03.TLAOperatingAirlineType;
import com.amadeus.ama._2008._03.TLAOriginDestinationInformationType;
import com.amadeus.ama._2008._03.TLAOriginatorType;
import com.amadeus.ama._2008._03.TLAPassengerMonetaryDataTypeWS;
import com.amadeus.ama._2008._03.TLAPassengerMonetaryDatasType;
import com.amadeus.ama._2008._03.TLAPassengerRPHType;
import com.amadeus.ama._2008._03.TLAPassengerSSRDataTypeBRS;
import com.amadeus.ama._2008._03.TLAPassengerSSRDatasTypeBRS;
import com.amadeus.ama._2008._03.TLAPassengerType;
import com.amadeus.ama._2008._03.TLAPassengersType;
import com.amadeus.ama._2008._03.TLAPaymentCardTypeRS;
import com.amadeus.ama._2008._03.TLAPaymentDetailTypeRS;
import com.amadeus.ama._2008._03.TLAPaymentDetailTypeRS.PaymentAmount;
import com.amadeus.ama._2008._03.TLAPaymentDetailsTypeRS;
import com.amadeus.ama._2008._03.TLARecordLocatorsType;
import com.amadeus.ama._2008._03.TLAReplyStatusType;
import com.amadeus.ama._2008._03.TLASegmentTypeM;
import com.amadeus.ama._2008._03.TLASingleOriginDestinationType;
import com.amadeus.ama._2008._03.TLASingleOriginDestinationsType;
import com.amadeus.ama._2008._03.TLAUniqueIDType;
import com.amadeus.ama._2008._03.TelephoneInfoType;
import com.amadeus.ama._2008._03.TravelerRefNumberType;
import com.amadeus.ama._2008._03.UniqueIDObjectType;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ChargesDetailDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.TempPaymentTnx;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.paymentbroker.api.model.CreditCardTransaction;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.api.dtos.AmadeusTotalPriceDTO;
import com.isa.thinair.webservices.api.exception.AmadeusWSException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.api.util.WebservicesConstants.AmadeusConstants;
import com.isa.thinair.webservices.api.util.WebservicesConstants.AmadeusConstants.AmadeusErrorCodes;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

/**
 * 
 * @author Dilan Anuruddha
 * 
 */
public class AmadeusRetrieveBooking {
	private final static Log log = LogFactory.getLog(AmadeusAvailabilitySearchBL.class);

	public static AMATLAGetBookingRS execute(AMATLAGetBookingRQ amaTLAGetBookingRQ) {
		AMATLAGetBookingRS bookingRS = new AMATLAGetBookingRS();
		bookingRS.setRelease(WebservicesConstants.AmadeusConstants.ReleaseRetrieveBooking);
		bookingRS.setVersion(WebservicesConstants.AmadeusConstants.VersionRetrieveBooking);
		try {
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setExtRecordLocatorId(amaTLAGetBookingRQ.getRecordLocator().getID());
			pnrModesDTO.setBookingCreationDate(amaTLAGetBookingRQ.getCreationDate().toXMLFormat());
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadOndChargesView(true);
			pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
			pnrModesDTO.setLoadSegViewBookingTypes(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadLastUserNote(true);
			pnrModesDTO.setLoadSSRInfo(true);
			Reservation reservation = WebServicesModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);
			transform(bookingRS, reservation, amaTLAGetBookingRQ);
			bookingRS.setSuccess(new SuccessType());
		} catch (ModuleException e) {
			log.error("Error occured", e);
			TLAErrorType error = new TLAErrorType();
			if (e instanceof AmadeusWSException) {
				AmadeusWSException ae = (AmadeusWSException) e;
				error.setCode(ae.getErrorCode());
				error.setType(ae.getErrorWarningType());
				// TODO fetch the descriptive message from resource bundle
				if (ae.getDescription() != null && !"".equals(ae.getDescription())) {
					error.setValue(ae.getDescription());
				}
			} else {
				error.setCode(Integer.valueOf(AmadeusErrorCodes.GENERIC_INTERNAL_ERROR_RETRY.getErrorCode()));
				error.setValue(e.getExceptionCode());
				error.setType(ErrorWarningType.UNKNOWN);
			}
			bookingRS.setError(error);
		}
		return bookingRS;
	}

	/**
	 * Transform AccelAero reservation to Amadeus Reservation
	 * 
	 * @param bookingRS
	 * @param reservation
	 * @param amaTLAGetBookingRQ
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static void transform(AMATLAGetBookingRS bookingRS, Reservation reservation, AMATLAGetBookingRQ amaTLAGetBookingRQ)
			throws ModuleException {

		if (reservation == null) {
			throw new AmadeusWSException(AmadeusErrorCodes.REQUESTED_PNR_NOT_FOUND, ErrorWarningType.BIZ_RULE,
					"Reservation not found");
		}

		/* reservation status */
		if (reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.CONFIRMED)) {
			bookingRS.setBookingStatus(TLAReplyStatusType.CONFIRMED);
		} else {
			throw new AmadeusWSException(AmadeusErrorCodes.GENERIC_INTERNAL_ERROR_DO_NOT_RETRY, ErrorWarningType.UNKNOWN,
					"Invalid reservation status");
		}

		/* contact details */
		ReservationContactInfo contactInfo = reservation.getContactInfo();
		TLAContactType contactPoint = new TLAContactType();

		PersonNameType personNameType = new PersonNameType();
		personNameType.getGivenName().add(contactInfo.getFirstName());
		personNameType.getNamePrefix().add(contactInfo.getTitle());
		personNameType.setSurname(contactInfo.getLastName());
		contactPoint.setPersonName(personNameType);

		TLAAddressType address = new TLAAddressType();
		address.getAddressLine().add(contactInfo.getStreetAddress1());
		address.getAddressLine().add(contactInfo.getStreetAddress2());
		address.setCityName(contactInfo.getCity());
		CountryNameType country = new CountryNameType();
		country.setCode(contactInfo.getCountryCode());
		address.setCountryName(country);
		contactPoint.getAddress().add(address);

		if (contactInfo.getEmail() != null) {
			contactPoint.getEmail().add(contactInfo.getEmail());
		}

		if (contactInfo.getMobileNo() != null) {
			TelephoneInfoType mobileNo = new TelephoneInfoType();
			mobileNo.setPhoneLocationType(PhoneLocationCategoryType.OTHER);
			mobileNo.setPhoneNumber(contactInfo.getMobileNo());
			contactPoint.getTelephone().add(mobileNo);
		}

		if (contactInfo.getPhoneNo() != null) {
			TelephoneInfoType landNo = new TelephoneInfoType();
			landNo.setPhoneLocationType(PhoneLocationCategoryType.HOME);
			landNo.setPhoneNumber(contactInfo.getPhoneNo());
			contactPoint.getTelephone().add(landNo);
		}

		bookingRS.setContactPoint(contactPoint);

		String currencyCode = null;
		CurrencyExchangeRate currencyExgRate = null;
		Date tnxDate = null;

		/* set the payment status */
		Collection<TempPaymentTnx> tempPaymentEntries = WebServicesModuleUtils.getReservationBD().getTempPaymentsForReservation(
				reservation.getPnr());
		Map<Integer, CreditCardTransaction> tptCardTnxMap = WebServicesModuleUtils.getPaymentBrokerBD()
				.loadCreditCardTransactionMap(reservation.getPnr());
		TLAPaymentDetailsTypeRS paymentStatus = new TLAPaymentDetailsTypeRS();
		for (TempPaymentTnx tmpTnx : tempPaymentEntries) {
			if (ReservationInternalConstants.TempPaymentTnxTypes.RESERVATION_SUCCESS.equals(tmpTnx.getStatus())) {
				CreditCardTransaction ccTnx = tptCardTnxMap.get(tmpTnx.getTnxId());
				TLAPaymentDetailTypeRS paymentDetail = new TLAPaymentDetailTypeRS();
				PaymentAmount paymentAmount = new PaymentAmount();
				paymentAmount.setAmount(AmadeusCommonUtil.getDispValue(tmpTnx.getPaymentCurrencyAmount(), null));
				paymentAmount.setApprovalCode(ccTnx.getAidCccompnay());

				paymentDetail.setPaymentAmount(paymentAmount);

				TLAPaymentCardTypeRS paymentCard = new TLAPaymentCardTypeRS();
				// Hard coded dummy card details as we are not saving them
				paymentCard.setCardCode("VI");
				paymentCard.setCardNumber("1");
				paymentCard.setExpireDate("1111");
				paymentCard.setSeriesCode("1");
				paymentDetail.setPaymentCard(paymentCard);
				paymentStatus.getPayment().add(paymentDetail);

				currencyCode = tmpTnx.getPaymentCurrencyCode();
				tnxDate = tmpTnx.getPaymentTimeStamp();
			}
		}
		bookingRS.setPaymentStatus(paymentStatus);

		/* set record locators */
		CompanyNameType company = new CompanyNameType();
		company.setCode(AppSysParamsUtil.getDefaultCarrierCode());

		TLAUniqueIDType rLoc = new TLAUniqueIDType();
		rLoc.setType(UniqueIDObjectType.RESERVATION);
		// FIXME :Removes first digit of the PNR to be compatible with Amadeus, temporary solution
		rLoc.setID(reservation.getPnr().substring(1));
		rLoc.setCompanyName(company);

		TLARecordLocatorsType recordLocators = new TLARecordLocatorsType();
		recordLocators.getRecordLocator().add(amaTLAGetBookingRQ.getRecordLocator());
		amaTLAGetBookingRQ.getRecordLocator().getCompanyName().setCode(AmadeusConstants.COMPANY_CODE);
		recordLocators.getRecordLocator().add(rLoc);
		bookingRS.setRecordLocatorsList(recordLocators);

		// Should set the currency of the last reservation

		currencyExgRate = AmadeusCommonUtil.getCurrencyExchangeRate(reservation.getLastCurrencyCode(), tnxDate);

		Map<Integer, List<ReservationSegmentDTO>> ondMap = new HashMap<Integer, List<ReservationSegmentDTO>>();
		Map<String, Map<String, TLABookingClassDataType>> segPaxPrice = new HashMap<String, Map<String, TLABookingClassDataType>>();
		Map<String, Boolean> paxFareProcessMap = new HashMap<String, Boolean>();

		paxFareProcessMap.put(AmadeusConstants.ADULT, false);
		paxFareProcessMap.put(AmadeusConstants.CHILD, false);
		paxFareProcessMap.put(AmadeusConstants.INFANT, false);

		List<String> adultRPHs = new ArrayList<String>();
		List<String> infantRPHs = new ArrayList<String>();
		List<String> childRPHs = new ArrayList<String>();

		AmadeusTotalPriceDTO totalPriceDTO = new AmadeusTotalPriceDTO();
		TLAPassengerRPHType paymentPaxRPH = new TLAPassengerRPHType();

		Map<String, Object[]> ssrMap = new HashMap<String, Object[]>();

		/* pax details */
		TLAFlightSegmentType fltSegRPHs = null;
		TLAPassengersType paxList = new TLAPassengersType();

		int noOfAdults = 0;
		int noOfChilds = 0;

		for (ReservationPax pax : (Collection<ReservationPax>) reservation.getPassengers()) {
			TLAPassengerType amaPax = new TLAPassengerType();
			String paxRPH = pax.getPaxSequence().toString();
			if (pax.getPaxType().equals(PaxTypeTO.ADULT) || pax.getPaxType().equals(PaxTypeTO.PARENT)) {
				amaPax.setPassengerTypeCode(AmadeusConstants.ADULT);
				if (pax.getAccompaniedSequence() != null) {
					amaPax.setAccompaniedInfantRef(pax.getAccompaniedSequence().toString());
				}
				adultRPHs.add(paxRPH);
				noOfAdults++;
			} else if (pax.getPaxType().equals(PaxTypeTO.CHILD)) {
				amaPax.setPassengerTypeCode(AmadeusConstants.CHILD);
				childRPHs.add(paxRPH);
				noOfChilds++;
			} else if (pax.getPaxType().equals(PaxTypeTO.INFANT)) {
				amaPax.setPassengerTypeCode(AmadeusConstants.INFANT);
				amaPax.setAccompanyingAdultRef(pax.getAccompaniedSequence().toString());
				infantRPHs.add(paxRPH);
			}
			TravelerRefNumberType travellerRefNo = new TravelerRefNumberType();
			travellerRefNo.setRPH(paxRPH);
			amaPax.setTravelerRefNumber(travellerRefNo);
			PersonNameType personName = new PersonNameType();
			personName.setSurname(pax.getLastName());
			personName.getNamePrefix().add(pax.getTitle());
			personName.getGivenName().add(pax.getFirstName());
			amaPax.setPersonName(personName);
			// if(pax.getStatus().equals(ReservationInternalConstants.ReservationPaxStatus.CONFIRMED){}
			amaPax.setState(StateType.NEW);

			if (pax.getDateOfBirth() != null) {
				amaPax.setBirthDate(CommonUtil.parse(pax.getDateOfBirth()));
			}

			TLAOriginatorType originatorType = new TLAOriginatorType();
			// As discussed with Nili and Dilan , e ticket number is not unique with new e ticket implementation. so we
			// added pnrPaxId to operation type instead of e ticket number.
			originatorType.setID((pax.getPnrPaxId() != null) ? pax.getPnrPaxId().toString() : null);
			originatorType.setType(UniqueIDObjectType.PNR_ELEMENT);
			amaPax.setOriginatorPassengerID(originatorType);

			if (!paxFareProcessMap.get(amaPax.getPassengerTypeCode())) {

				Map<String, Object[]> ondPriceMap = new HashMap<String, Object[]>();
				for (ChargesDetailDTO ondCharge : (Collection<ChargesDetailDTO>) pax.getOndChargesView()) {
					String ondCode = ondCharge.getOndCode();

					if (!ondPriceMap.containsKey(ondCode)) {
						Object[] arr = new Object[4];
						arr[0] = BigDecimal.ZERO;// fare
						arr[1] = BigDecimal.ZERO;// tax surcharge
						arr[2] = BigDecimal.ZERO;// fee
						arr[3] = ""; // booking class
						ondPriceMap.put(ondCode, arr);
					}

					Object[] dataArr = ondPriceMap.get(ondCode);

					if (ondCharge.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.FAR)) {
						dataArr[0] = AccelAeroCalculator.add((BigDecimal) dataArr[0], ondCharge.getChargeAmount());
					} else if (ondCharge.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.SUR)
							&& ondCharge.getChargeCode().equals("CC")) { // fee
						dataArr[2] = AccelAeroCalculator.add((BigDecimal) dataArr[2], ondCharge.getChargeAmount());
					} else {
						dataArr[1] = AccelAeroCalculator.add((BigDecimal) dataArr[1], ondCharge.getChargeAmount());

					}

					dataArr[3] = ondCharge.getBookingCode();

				}

				for (String ondCode : ondPriceMap.keySet()) {
					Object[] dataArr = ondPriceMap.get(ondCode);
					if (!segPaxPrice.containsKey(ondCode)) {
						segPaxPrice.put(ondCode, new HashMap<String, TLABookingClassDataType>());
					}
					Map<String, TLABookingClassDataType> paxTypePrice = segPaxPrice.get(ondCode);
					if (!paxTypePrice.containsKey(amaPax.getPassengerTypeCode()) && dataArr != null) {

						TLABookingClassDataType bc = new TLABookingClassDataType();

						TLAFareReferenceType fareBasis = new TLAFareReferenceType();
						fareBasis.setResBookDesigCode(getSingleCharBC((String) dataArr[3]));
						bc.setFareBasis(fareBasis);

						TLACurrencyAmountType totalAmt = new TLACurrencyAmountType();
						totalAmt.setAmount(AmadeusCommonUtil.getDispValue(
								AccelAeroCalculator.add((BigDecimal) dataArr[0], (BigDecimal) dataArr[1]), currencyExgRate));
						bc.setTotalAmount(totalAmt);

						TLACurrencyAmountType taxAmt = new TLACurrencyAmountType();
						taxAmt.setAmount(AmadeusCommonUtil.getDispValue((BigDecimal) dataArr[1], currencyExgRate));
						bc.setInformativeTaxAmount(taxAmt);

						totalPriceDTO
								.setTotalFare(AccelAeroCalculator.add(totalPriceDTO.getTotalFare(), (BigDecimal) dataArr[0]));
						totalPriceDTO.setTotalTax(AccelAeroCalculator.add(totalPriceDTO.getTotalTax(), (BigDecimal) dataArr[1]));
						totalPriceDTO.setTotalFee(AccelAeroCalculator.add(totalPriceDTO.getTotalFee(), (BigDecimal) dataArr[2]));

						paxTypePrice.put(amaPax.getPassengerTypeCode(), bc);
					}
				}

				paxFareProcessMap.put(amaPax.getPassengerTypeCode(), true);
			}

			Map<Integer, String> pnrSegRPHMap = new HashMap<Integer, String>();

			if (fltSegRPHs == null) {
				fltSegRPHs = new TLAFlightSegmentType();
				for (ReservationSegmentDTO resSeg : (Collection<ReservationSegmentDTO>) pax.getReservation().getSegmentsView()) {
					TLAAssociationType segRPH = new TLAAssociationType();
					segRPH.setTargetState(StateType.NEW);
					segRPH.setValue(resSeg.getSegmentSeq().toString());
					fltSegRPHs.getFlightSegmentRPH().add(segRPH);

					pnrSegRPHMap.put(resSeg.getPnrSegId(), resSeg.getSegmentSeq().toString());

					if (!ondMap.containsKey(resSeg.getFareGroupId())) {
						ondMap.put(resSeg.getFareGroupId(), new ArrayList<ReservationSegmentDTO>());
					}
					ondMap.get(resSeg.getFareGroupId()).add(resSeg);
				}
			}
			amaPax.setFlightSegmentRPHs(fltSegRPHs);

			if (!amaPax.getPassengerTypeCode().equals(AmadeusConstants.INFANT)) {
				TLAAssociationType assocType = new TLAAssociationType();
				assocType.setTargetState(StateType.NEW);
				assocType.setValue(paxRPH);

				paymentPaxRPH.getPassengerRPH().add(assocType);
			}

			if (pax.getPaxSSR() != null) {
				for (PaxSSRDTO paxSSR : (Collection<PaxSSRDTO>) pax.getPaxSSR()) {
					if (!ssrMap.containsKey(paxSSR.getSsrCode())) {
						Object objArr[] = new Object[3];
						objArr[0] = new ArrayList<String>();// paxRPH
						objArr[1] = new ArrayList<String>();// segRPH
						objArr[2] = null; // Ext reference
						ssrMap.put(paxSSR.getSsrCode(), objArr);
					}
					Object objArr[] = ssrMap.get(paxSSR.getSsrCode());
					((List) objArr[0]).add(paxRPH);
					((List) objArr[1]).add(pnrSegRPHMap.get(paxSSR.getPnrSegId()));
					objArr[2] = paxSSR.getExternalReference();
				}
			}

			paxList.getPassenger().add(amaPax);
		}

		bookingRS.setPassengerList(paxList);

		/* itinerary details */
		TLASingleOriginDestinationsType singleOriginDestinations = new TLASingleOriginDestinationsType();
		for (List<ReservationSegmentDTO> ondSegs : ondMap.values()) {
			TLAJourneySingleFareType journey = new TLAJourneySingleFareType();

			Collections.sort(ondSegs, new Comparator<ReservationSegmentDTO>() {
				public int compare(ReservationSegmentDTO s1, ReservationSegmentDTO s2) {
					return s1.getZuluDepartureDate().compareTo(s2.getZuluDepartureDate());
				}
			});

			TLAOriginDestinationInformationType ondTypeInfo = new TLAOriginDestinationInformationType();

			int i = 1;
			for (ReservationSegmentDTO seg : ondSegs) {
				TLASegmentTypeM amaSeg = new TLASegmentTypeM();

				String airportCodes[] = seg.getSegmentCode().split("/");

				TLAAirportLocationType arrLoc = new TLAAirportLocationType();
				arrLoc.setLocationCode(airportCodes[airportCodes.length - 1]);
				amaSeg.setArrivalAirport(arrLoc);

				TLAAirportLocationType depLoc = new TLAAirportLocationType();
				depLoc.setLocationCode(airportCodes[0]);
				amaSeg.setDepartureAirport(depLoc);

				amaSeg.setArrivalDateTime(CommonUtil.parse(seg.getArrivalDate()));
				amaSeg.setDepartureDateTime(CommonUtil.parse(seg.getDepartureDate()));

				amaSeg.setRPH(seg.getSegmentSeq().toString());

				amaSeg.setState(StateType.NEW);

				TLAOperatingAirlineType operatingAirline = new TLAOperatingAirlineType();
				operatingAirline.setCode(seg.getCarrierCode());
				operatingAirline.setFlightNumber(seg.getFlightNo().substring(seg.getCarrierCode().length()));
				amaSeg.setOperatingAirline(operatingAirline);

				if (ondSegs.size() == 1 || (ondSegs.size() > 1 && i == 1)) {
					ondTypeInfo.setDepartureDateTime(CommonUtil.parseZuluDate(seg.getZuluDepartureDate()).toXMLFormat());
					TLALocationType originLoc = new TLALocationType();
					originLoc.setLocationCode(airportCodes[0]);
					ondTypeInfo.setOriginLocation(originLoc);
				}
				if (ondSegs.size() == 1 || (ondSegs.size() > 1 && i == ondSegs.size())) {
					TLALocationType destLoc = new TLALocationType();
					destLoc.setLocationCode(airportCodes[airportCodes.length - 1]);
					ondTypeInfo.setDestinationLocation(destLoc);
				}

				TLAOriginatorType originatorType = new TLAOriginatorType();
				originatorType.setID(seg.getExternalReference());
				// id
				originatorType.setType(UniqueIDObjectType.PNR_ELEMENT);
				amaSeg.setOriginatorFlightSegmentID(originatorType);

				journey.getSegment().add(amaSeg);

				if (adultRPHs.size() > 0) {
					TLAFareType fareSeg = new TLAFareType();
					fareSeg.setFlightSegmentRPH(seg.getSegmentSeq().toString());
					PassengerRPHs paxRPH = new PassengerRPHs();
					paxRPH.getPassengerRPH().addAll(adultRPHs);
					fareSeg.setPassengerRPHs(paxRPH);
					TLABookingClassDataType bc = segPaxPrice.get(seg.getSegmentCode()).get(AmadeusConstants.ADULT);
					fareSeg.setBookingClassData(bc);
					if (bc != null) {
						journey.getFaresForSegment().add(fareSeg);
					}
				}
				if (childRPHs.size() > 0) {
					TLAFareType fareSeg = new TLAFareType();
					fareSeg.setFlightSegmentRPH(seg.getSegmentSeq().toString());
					PassengerRPHs paxRPH = new PassengerRPHs();
					paxRPH.getPassengerRPH().addAll(childRPHs);
					fareSeg.setPassengerRPHs(paxRPH);
					TLABookingClassDataType bc = segPaxPrice.get(seg.getSegmentCode()).get(AmadeusConstants.CHILD);
					fareSeg.setBookingClassData(bc);
					if (bc != null) {
						journey.getFaresForSegment().add(fareSeg);
					}
				}
				if (infantRPHs.size() > 0) {
					TLAFareType fareSeg = new TLAFareType();
					fareSeg.setFlightSegmentRPH(seg.getSegmentSeq().toString());
					PassengerRPHs paxRPH = new PassengerRPHs();
					paxRPH.getPassengerRPH().addAll(infantRPHs);
					fareSeg.setPassengerRPHs(paxRPH);
					TLABookingClassDataType bc = segPaxPrice.get(seg.getSegmentCode()).get(AmadeusConstants.INFANT);
					fareSeg.setBookingClassData(bc);
					if (bc != null) {
						journey.getFaresForSegment().add(fareSeg);
					}
				}

				i++;
			}
			TLASingleOriginDestinationType singleOnd = new TLASingleOriginDestinationType();

			singleOnd.setJourney(journey);

			singleOnd.setOriginDestinationInfo(ondTypeInfo);
			singleOriginDestinations.getSingleOriginDestination().add(singleOnd);

		}
		bookingRS.setItinerary(singleOriginDestinations);

		/* ssrs */
		if (ssrMap.size() > 0) {
			TLAPassengerSSRDatasTypeBRS ssrGrp = new TLAPassengerSSRDatasTypeBRS();
			for (String ssrCode : ssrMap.keySet()) {
				Object objArr[] = ssrMap.get(ssrCode);
				List<String> paxRPHs = (List<String>) objArr[0];
				List<String> segRPHs = (List<String>) objArr[1];
				String extRef = (String) objArr[2];

				TLAPassengerSSRDataTypeBRS paxSSRData = new TLAPassengerSSRDataTypeBRS();

				paxSSRData.setCode(ssrCode);
				paxSSRData.setType(TLAFareCategoryType.SSR);

				TLAFlightSegmentType fltSegs = new TLAFlightSegmentType();
				for (String segRPH : segRPHs) {
					TLAAssociationType tlaAssoc = new TLAAssociationType();
					tlaAssoc.setValue(segRPH);
					tlaAssoc.setTargetState(StateType.NEW);
					fltSegs.getFlightSegmentRPH().add(tlaAssoc);
				}
				paxSSRData.setFlightSegmentRPHs(fltSegs);

				TLAPassengerRPHType paxTypeRPHs = new TLAPassengerRPHType();
				for (String paxRPH : paxRPHs) {
					TLAAssociationType assocType = new TLAAssociationType();
					assocType.setValue(paxRPH);
					assocType.setTargetState(StateType.NEW);
					paxTypeRPHs.getPassengerRPH().add(assocType);
				}
				paxSSRData.setPassengerRPHs(paxTypeRPHs);

				TLAOriginatorType orignatorType = new TLAOriginatorType();
				orignatorType.setID(extRef);
				orignatorType.setType(UniqueIDObjectType.PNR_ELEMENT);
				paxSSRData.setOriginatorSSRID(orignatorType);

				TLACurrencyAmountType amt = new TLACurrencyAmountType();
				/*
				 * no chargeable ssrs supported
				 */
				amt.setAmount(AmadeusCommonUtil.getDispValue(BigDecimal.ZERO, null));
				paxSSRData.setAmount(amt);

				ssrGrp.getPassengerSSRData().add(paxSSRData);

			}
			bookingRS.setSSRGroup(ssrGrp);
		}

		/* setting charges group */
		TLAPassengerMonetaryDatasType feeChargesGroup = new TLAPassengerMonetaryDatasType();
		TLAPassengerMonetaryDataTypeWS ccMonetaryData = new TLAPassengerMonetaryDataTypeWS();
		ccMonetaryData.setChargeCode(TLAFareCategoryCode.CC);
		ccMonetaryData.setType(TLAFareCategoryType.FEE);
		TLACurrencyAmountType tlaAmount = new TLACurrencyAmountType();

		// FIXME: Should reconsider this logic
		if (noOfAdults > 0 && noOfChilds > 0) {
			tlaAmount.setAmount(AmadeusCommonUtil.getDispValue(AccelAeroCalculator.divide(totalPriceDTO.getTotalFee(), 2),
					currencyExgRate));
		} else {
			tlaAmount.setAmount(AmadeusCommonUtil.getDispValue(totalPriceDTO.getTotalFee(), currencyExgRate));
		}

		ccMonetaryData.setAmount(tlaAmount);
		ccMonetaryData.setQuantity(BigInteger.ONE);

		ccMonetaryData.setPassengerRPHs(paymentPaxRPH);
		feeChargesGroup.getPassengerMonetaryData().add(ccMonetaryData);
		bookingRS.setFeesAndChargesGroup(feeChargesGroup);
		bookingRS.setDisplayCurrencyCode(currencyCode);
	}

	private static String getSingleCharBC(String bookingClassCode) {
		String singleCharBC = "";

		// Check default booking class mapping enabled if it does get the default booking class
		if (WebServicesModuleUtils.getAmadeusConfig().isBookingClassMappingEnabled()) {
			Map<Integer, Map<String, String>> gdsBookingClassMap = WebServicesModuleUtils.getGlobalConfig()
					.getGdsBookingClassMap();
			if(!gdsBookingClassMap.get(AmadeusConstants.GDS_ID).isEmpty()){
				singleCharBC = gdsBookingClassMap.get(AmadeusConstants.GDS_ID).get(bookingClassCode);
			}
		} else {
			singleCharBC = WebServicesModuleUtils.getAmadeusConfig().getDefaultBookingClass();
		}

		return singleCharBC;
	}
}
