package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AAOTAAirSSRDetailsRQ;
import org.opentravel.ota._2003._05.AAOTAAirSSRDetailsRQ.SSRDetailsRequests.SSRDetailsRequest;
import org.opentravel.ota._2003._05.AAOTAAirSSRDetailsRQ.SSRDetailsRequests.SSRDetailsRequest.SSRDetails;
import org.opentravel.ota._2003._05.AAOTAAirSSRDetailsRQ.SSRDetailsRequests.SSRDetailsRequest.SSRDetails.CabinClass;
import org.opentravel.ota._2003._05.AAOTAAirSSRDetailsRS;
import org.opentravel.ota._2003._05.AAOTAAirSSRDetailsRS.SSRDetailsResponses;
import org.opentravel.ota._2003._05.AAOTAAirSSRDetailsRS.SSRDetailsResponses.SSRDetailsResponse;
import org.opentravel.ota._2003._05.AAOTAAirSSRDetailsRS.SSRDetailsResponses.SSRDetailsResponse.FlightSegmentInfo;
import org.opentravel.ota._2003._05.AAOTAAirSSRSetAmountRQ;
import org.opentravel.ota._2003._05.AAOTAAirSSRSetAmountRS;
import org.opentravel.ota._2003._05.AirTravelerType;
import org.opentravel.ota._2003._05.AirportService;
import org.opentravel.ota._2003._05.CabinType;
import org.opentravel.ota._2003._05.ErrorsType;
import org.opentravel.ota._2003._05.FlightSegmentBaseType.ArrivalAirport;
import org.opentravel.ota._2003._05.FlightSegmentBaseType.DepartureAirport;
import org.opentravel.ota._2003._05.FlightSegmentType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.InflightService;
import org.opentravel.ota._2003._05.UserDefinedSSR;
import org.opentravel.ota._2003._05.SSRType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType;
import org.opentravel.ota._2003._05.SpecialReqDetailsType.SSRRequests.SSRRequest;
import org.opentravel.ota._2003._05.SuccessType;
import org.opentravel.ota._2003._05.TravelerInfoType;
import org.opentravel.ota._2003._05.UserDefinedSSRAmount;
import org.opentravel.ota._2003._05.WarningsType;

import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.dto.SpecialServiceRequestDTO;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airpricing.api.model.SSRCharge;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO.LCCAncillaryType;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentAirportServiceDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCFlightSegmentSSRDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSpecialServiceRequestDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ssr.SSRExternalChargeDTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webservices.api.dtos.SSRCustomDTO;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;

public class SSRDetailsUtil extends BaseUtil {
	private static final Log log = LogFactory.getLog(SSRDetailsUtil.class);
	private static final String ADULT_REF_PREFIX = "A";
	private static final String CHILD_REF_PREFIX = "C";
	private static final String INFANT_REF_PREFIX = "I";

	public AAOTAAirSSRDetailsRS getSSRDetails(AAOTAAirSSRDetailsRQ aaOtaAirSSRDetailsRQ) throws WebservicesException {

		AAOTAAirSSRDetailsRS aaOtaAirSSRDetailsRS = null;
		// Holds Available airport services
		List<LCCFlightSegmentAirportServiceDTO> airportServicesList = null;
		// Holds Available in-flight services
		List<LCCFlightSegmentSSRDTO> inflightServicesList = null;

		String cabinClass = null;

		try {
			List<FlightSegmentTO> airportServiceFlightSegmentTOs = new ArrayList<FlightSegmentTO>();
			List<FlightSegmentTO> inflightServiceFlightSegmentTOs = new ArrayList<FlightSegmentTO>();

			for (SSRDetailsRequest ssrDetailsRequest : aaOtaAirSSRDetailsRQ.getSSRDetailsRequests().getSSRDetailsRequest()) {
				FlightSegmentType flightSegmentType = ssrDetailsRequest.getFlightSegmentInfo();
				FlightSegmentTO flightSegmentTo = getFlightSegmentTO(flightSegmentType);
				if (flightSegmentTo.getFlightRefNumber() == null || "".equals(flightSegmentTo.getFlightRefNumber())) {
					// If RPH value is not getting from client side, retrieve flight segment id
					if (flightSegmentTo.getFlightSegId() == null) {
						Collection<Integer> segIds = SegmentUtil.getFlightSegmentIds(flightSegmentTo);
						if (segIds.size() > 0) {
							for (Integer segId : segIds) {
								flightSegmentTo.setFlightSegId(segId);
								flightSegmentTo.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentTo));
							}
						}
					} else {
						flightSegmentTo.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentTo));
					}
				}

				SSRDetails ssrDetails = ssrDetailsRequest.getSSRDetails();

				if (cabinClass == null) {
					if (ssrDetails != null && ssrDetails.getCabinClass() != null && ssrDetails.getCabinClass().size() > 0) {
						CabinClass ccType = ssrDetails.getCabinClass().get(0);
						cabinClass = (ccType != null) ? ccType.getCabinType().value() : null;
					}
				}

				if (ssrDetails != null && ssrDetails.getServiceType() != null) {
					if (SSRType.AIRPORT != ssrDetails.getServiceType()) {
						inflightServiceFlightSegmentTOs.add(flightSegmentTo);
					} else {
						airportServiceFlightSegmentTOs.add(flightSegmentTo);
					}
				} else {
					inflightServiceFlightSegmentTOs.add(flightSegmentTo);
				}
			}

			if (cabinClass == null) {
				cabinClass = CabinType.Y.value();
			}

			boolean checkAirportService = false;
			boolean checkInflightService = false;
			boolean isAirportServiceAvailable = false;
			boolean isInflightServiceAvailable = false;

			if (airportServiceFlightSegmentTOs.size() > 0) {
				checkAirportService = true;
			}

			if (inflightServiceFlightSegmentTOs.size() > 0) {
				checkInflightService = true;
			}

			if (checkAirportService) {
				isAirportServiceAvailable = WSAncillaryUtil.isAncillaryAvailable(airportServiceFlightSegmentTOs,
						LCCAncillaryType.AIRPORT_SERVICE, null);
			}

			if (checkInflightService) {
				isInflightServiceAvailable = WSAncillaryUtil.isAncillaryAvailable(inflightServiceFlightSegmentTOs,
						LCCAncillaryType.SSR, null);
			}

			if ((checkAirportService && !isAirportServiceAvailable) || (checkInflightService && !isInflightServiceAvailable)) {
				throw new WebservicesException(
						IOTACodeTables.AccelaeroErrorCodes_AAE_SSR_DETAILS_NOT_AVAILABLE_FOR_REQUESTED_CARRIER_, "");
			} else {
				aaOtaAirSSRDetailsRS = new AAOTAAirSSRDetailsRS();
				aaOtaAirSSRDetailsRS.setErrors(new ErrorsType());
				aaOtaAirSSRDetailsRS.setWarnings(new WarningsType());
				aaOtaAirSSRDetailsRS.setSSRDetailsResponses(new SSRDetailsResponses());

				// Get available airport service
				if (checkAirportService) {
					Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> aiportWiseAvailability = WebServicesModuleUtils
							.getAirproxyAncillaryBD().getAvailableAiportServices(airportServiceFlightSegmentTOs,
									AppIndicatorEnum.APP_WS, SSRCategory.ID_AIRPORT_SERVICE, SYSTEM.AA,
									Locale.ENGLISH.getLanguage(), aaOtaAirSSRDetailsRQ.getTransactionIdentifier(), null, null,
									false, WSAncillaryUtil.getOndSelectedBundledFares(), null);

					airportServicesList = populateLCCAiportServicesList(aiportWiseAvailability);
					generateAirportServiceResponse(airportServicesList, aaOtaAirSSRDetailsRS);
				}

				// Get available in-flight service
				if (checkInflightService) {
					inflightServicesList = WebServicesModuleUtils.getAirproxyAncillaryBD().getSpecialServiceRequests(
							inflightServiceFlightSegmentTOs, aaOtaAirSSRDetailsRQ.getTransactionIdentifier(), SYSTEM.AA,
							Locale.ENGLISH.getLanguage(), null, null, false, false, false, false);
					generateInflightServiceResponse(inflightServicesList, aaOtaAirSSRDetailsRS);
				}

				aaOtaAirSSRDetailsRS.setSuccess(new SuccessType());
			}

		} catch (ModuleException e) {
			log.error(e);
			if (e.getExceptionCode().equals("airinventory.flight.notfound")) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_FLIGHT_DOES_NOT_OPERATE_ON_DATE_REQUESTED, "");
			}
			throw new WebservicesException(e);
		}

		return aaOtaAirSSRDetailsRS;
	}

	private static FlightSegmentTO getFlightSegmentTO(FlightSegmentType flightSegmentType) {
		FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
		flightSegmentTO.setArrivalDateTime(flightSegmentType.getArrivalDateTime().toGregorianCalendar().getTime());
		flightSegmentTO.setArrivalTerminalName(flightSegmentType.getArrivalAirport().getTerminal());
		flightSegmentTO.setDepartureDateTime(flightSegmentType.getDepartureDateTime().toGregorianCalendar().getTime());
		flightSegmentTO.setFlightNumber(flightSegmentType.getFlightNumber());

		flightSegmentTO.setDepartureDateTimeZulu(CalendarUtil.getZuluDate(flightSegmentType.getDepartureDateTime()
				.toGregorianCalendar().getTime(), 0, 0));
		flightSegmentTO.setArrivalDateTimeZulu(CalendarUtil.getZuluDate(flightSegmentType.getArrivalDateTime()
				.toGregorianCalendar().getTime(), 0, 0));

		if (flightSegmentType.getOperatingAirline().getCode() != null) {
			flightSegmentTO.setOperatingAirline(flightSegmentType.getOperatingAirline().getCode());
		}

		if (flightSegmentType.getSegmentCode() != null) {
			flightSegmentTO.setSegmentCode(flightSegmentType.getSegmentCode());
		} else {
			flightSegmentTO.setSegmentCode(flightSegmentType.getDepartureAirport().getLocationCode() + "/"
					+ flightSegmentType.getArrivalAirport().getLocationCode());
		}

		if (flightSegmentType.getRPH() != null && !flightSegmentType.getRPH().equals("")) {
			Integer flightSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegmentType.getRPH());
			flightSegmentTO.setFlightSegId(flightSegId);
		}

		String cabinClassCode = "Y";
		if (AppSysParamsUtil.getDefaultCOS() != null && !AppSysParamsUtil.getDefaultCOS().isEmpty()) {
			cabinClassCode = AppSysParamsUtil.getDefaultCOS();
		}
		flightSegmentTO.setCabinClassCode(cabinClassCode);
		flightSegmentTO.setFlightRefNumber(flightSegmentType.getRPH());
		CommonServicesUtil.setOndSequence(flightSegmentTO);

		return flightSegmentTO;
	}

	/**
	 * generate available Airport services for WS SSR response
	 * 
	 * @param aaOtaAirSSRDetailsRS
	 * @param List
	 *            of LCCFlightSegmentAirportServiceDTO
	 * 
	 * @return AAOTAAirSSRDetailsRS
	 * @throws ModuleException
	 */
	public static void generateAirportServiceResponse(List<LCCFlightSegmentAirportServiceDTO> airportServicesList,
			AAOTAAirSSRDetailsRS aaOtaAirSSRDetailsRS) throws ModuleException {

		SSRDetailsResponses ssrDetailsResponses = aaOtaAirSSRDetailsRS.getSSRDetailsResponses();

		Iterator<LCCFlightSegmentAirportServiceDTO> itr = airportServicesList.iterator();

		while (itr.hasNext()) {
			LCCFlightSegmentAirportServiceDTO fltSegAirportSerDTO = (LCCFlightSegmentAirportServiceDTO) itr.next();

			SSRDetailsResponse ssrDetailsResponse = new SSRDetailsResponse();
			for (LCCAirportServiceDTO lccAirportServiceDTO : fltSegAirportSerDTO.getAirportServices()) {

				FlightSegmentTO flightSegmentTO = fltSegAirportSerDTO.getFlightSegmentTO();
				FlightSegmentInfo flightSegmentInfo = getFlightSegmentInfo(flightSegmentTO);

				ssrDetailsResponse.setFlightSegmentInfo(flightSegmentInfo);
				ssrDetailsResponse.setAirportCode(flightSegmentTO.getAirportCode());
				ssrDetailsResponse.setAirportType(flightSegmentTO.getAirportType());

				AirportService ssr = new AirportService();
				ssr.setSsrCode(lccAirportServiceDTO.getSsrCode());
				ssr.setSsrName(lccAirportServiceDTO.getSsrName());
				ssr.setSsrDescription(lccAirportServiceDTO.getSsrDescription());
				ssr.setApplicabilityType(lccAirportServiceDTO.getApplicabilityType());
				ssr.setAdultAmount(lccAirportServiceDTO.getAdultAmount());
				ssr.setChildAmount(lccAirportServiceDTO.getChildAmount());
				ssr.setInfantAmount(lccAirportServiceDTO.getInfantAmount());
				ssr.setReservationAmount(lccAirportServiceDTO.getReservationAmount());

				ssrDetailsResponse.getAirportService().add(ssr);

			}
			ssrDetailsResponses.getSSRDetailsResponse().add(ssrDetailsResponse);
		}
	}

	public static void generateInflightServiceResponse(List<LCCFlightSegmentSSRDTO> inflightServiceList,
			AAOTAAirSSRDetailsRS aaOtaAirSSRDetailsRS) throws ModuleException {
		SSRDetailsResponses ssrDetailsResponses = aaOtaAirSSRDetailsRS.getSSRDetailsResponses();

		for (LCCFlightSegmentSSRDTO lccFlightSegmentSSRDTO : inflightServiceList) {
			SSRDetailsResponse ssrDetailsResponse = new SSRDetailsResponse();
			for (LCCSpecialServiceRequestDTO ssrDto : lccFlightSegmentSSRDTO.getSpecialServiceRequest()) {
				FlightSegmentTO flightSegmentTO = lccFlightSegmentSSRDTO.getFlightSegmentTO();
				FlightSegmentInfo flightSegmentInfo = getFlightSegmentInfo(flightSegmentTO);

				ssrDetailsResponse.setFlightSegmentInfo(flightSegmentInfo);

				InflightService inflightService = new InflightService();
				inflightService.setSsrCode(ssrDto.getSsrCode());
				inflightService.setSsrName(ssrDto.getSsrName());
				inflightService.setSsrDescription(ssrDto.getDescription());
				inflightService.setServiceAmount(ssrDto.getCharge());
				inflightService.setAvailableQty(ssrDto.getAvailableQty());
				ssrDetailsResponse.getInflightService().add(inflightService);
			}

			ssrDetailsResponses.getSSRDetailsResponse().add(ssrDetailsResponse);
		}
	}

	/**
	 * Build flight segment Info
	 * 
	 * @param flightSegmentTO
	 * @return flightSegmentInfo
	 * @throws ModuleException
	 */
	public static FlightSegmentInfo getFlightSegmentInfo(FlightSegmentTO flightSegmentTO) throws ModuleException {
		FlightSegmentInfo flightSegmentInfo = new FlightSegmentInfo();

		String[] airPorts = flightSegmentTO.getSegmentCode().split("/");

		ArrivalAirport arrivalAirport = new ArrivalAirport();
		arrivalAirport.setTerminal(flightSegmentTO.getArrivalTerminalName());
		arrivalAirport.setLocationCode(airPorts[airPorts.length - 1]);

		DepartureAirport departureAirport = new DepartureAirport();
		departureAirport.setTerminal(flightSegmentTO.getDepartureTerminalName());
		departureAirport.setLocationCode(airPorts[0]);

		flightSegmentInfo.setArrivalAirport(arrivalAirport);
		flightSegmentInfo.setDepartureAirport(departureAirport);
		flightSegmentInfo.setArrivalDateTime(CommonUtil.parse(flightSegmentTO.getArrivalDateTime()));
		flightSegmentInfo.setDepartureDateTime(CommonUtil.parse(flightSegmentTO.getDepartureDateTime()));
		flightSegmentInfo.setFlightNumber(flightSegmentTO.getFlightNumber());
		flightSegmentInfo.setSegmentCode(flightSegmentTO.getSegmentCode());
		flightSegmentInfo.setRPH(flightSegmentTO.getFlightSegId() + "");
		return flightSegmentInfo;
	}

	public static List<LCCFlightSegmentAirportServiceDTO> populateLCCAiportServicesList(
			Map<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> aiportWiseAvailability) {
		List<LCCFlightSegmentAirportServiceDTO> lccAvailabilityList = new ArrayList<LCCFlightSegmentAirportServiceDTO>();

		for (Entry<AirportServiceKeyTO, LCCFlightSegmentAirportServiceDTO> entry : aiportWiseAvailability.entrySet()) {
			AirportServiceKeyTO keyTO = entry.getKey();

			LCCFlightSegmentAirportServiceDTO airportServiceDTO = entry.getValue();
			airportServiceDTO.setFlightSegmentTO(airportServiceDTO.getFlightSegmentTO().clone());
			airportServiceDTO.getFlightSegmentTO().setAirportCode(keyTO.getAirport());
			airportServiceDTO.getFlightSegmentTO().setAirportType(keyTO.getAirportType());
			lccAvailabilityList.add(airportServiceDTO);
		}

		SortUtil.sortAirportServicesByFltDeparture(lccAvailabilityList);

		return lccAvailabilityList;
	}

	public static Map<String, Map<Integer, List<SpecialServiceRequestDTO>>> getSelectedInflightServiceDetails(
			Map<String, Map<Integer, List<HashMap<String, String>>>> travelerFlightDetailsWithSSRs) throws ModuleException,
			WebservicesException {

		Map<String, Map<Integer, List<SpecialServiceRequestDTO>>> segmentWithSSRs = new HashMap<String, Map<Integer, List<SpecialServiceRequestDTO>>>();
		Map<Integer, ClassOfServiceDTO> fltSegCosMap = new HashMap<Integer, ClassOfServiceDTO>();

		//FIXME: Inject cabin class and logical cabin class
		for (Entry<String, Map<Integer, List<HashMap<String, String>>>> entry : travelerFlightDetailsWithSSRs.entrySet()) {
			Map<Integer, List<HashMap<String, String>>> flightDetailsWithSSRs = entry.getValue();
			for (Entry<Integer, List<HashMap<String, String>>> innerEnrty : flightDetailsWithSSRs.entrySet()) {
				List<HashMap<String, String>> serviceList = innerEnrty.getValue();
				Integer fltSegId = innerEnrty.getKey();

				for (HashMap<String, String> hashMap : serviceList) {
					fltSegCosMap.put(fltSegId, new ClassOfServiceDTO(null, null, hashMap.get("SEGMENT_CODE")));
				}
			}
		}

		Integer salesChannel = new Integer(ReservationInternalConstants.SalesChannel.DNATA);

		Map<Integer, List<SpecialServiceRequestDTO>> selectedSSRs = WebServicesModuleUtils.getSsrServiceBD().getAvailableSSRs(
				fltSegCosMap, salesChannel.toString(), AppSysParamsUtil.isInventoryCheckForSSREnabled(), false, false);

		Map<Integer, Map<String, Integer>> fltWiseCount = getFlightWiseCount(travelerFlightDetailsWithSSRs);

		for (Entry<String, Map<Integer, List<HashMap<String, String>>>> entry : travelerFlightDetailsWithSSRs.entrySet()) {
			String travelerRef = entry.getKey();
			Map<Integer, List<HashMap<String, String>>> flightDetailsWithSSRs = entry.getValue();
			for (Entry<Integer, List<HashMap<String, String>>> innerEnrty : flightDetailsWithSSRs.entrySet()) {
				Integer fltSegId = innerEnrty.getKey();
				List<HashMap<String, String>> serviceList = innerEnrty.getValue();

				for (HashMap<String, String> hashMap : serviceList) {
					String ssrCode = hashMap.get("SSR_CODE");
					List<SpecialServiceRequestDTO> ssrList = selectedSSRs.get(fltSegId);
					SpecialServiceRequestDTO ssrDTO = getInflightServiceDTO(ssrCode, ssrList);

					if (ssrDTO == null) {
						throw new WebservicesException(
								IOTACodeTables.AccelaeroErrorCodes_AAE_SSR_NOT_AVAILABLE_IN_REQUESTED_FLIGHT_SEGMENT_,
								" Requested In-flight Services not available for this flight segment");
					}

					int requestedSSRCount = fltWiseCount.get(fltSegId).get(ssrCode);

					if (requestedSSRCount > ssrDTO.getAvailableQty()) {
						throw new WebservicesException(
								IOTACodeTables.AccelaeroErrorCodes_AAE_SSR_NOT_AVAILABLE_IN_REQUESTED_FLIGHT_SEGMENT_,
								" Not enough In-flight Service [" + ssrCode + "] available for requested flight segment");
					}

					if (segmentWithSSRs.get(travelerRef) == null) {
						segmentWithSSRs.put(travelerRef, new HashMap<Integer, List<SpecialServiceRequestDTO>>());
						if (segmentWithSSRs.get(travelerRef).get(fltSegId) == null) {
							segmentWithSSRs.get(travelerRef).put(fltSegId, new ArrayList<SpecialServiceRequestDTO>());
						}
					} else {
						if (segmentWithSSRs.get(travelerRef).get(fltSegId) == null) {
							segmentWithSSRs.get(travelerRef).put(fltSegId, new ArrayList<SpecialServiceRequestDTO>());
						}
					}

					segmentWithSSRs.get(travelerRef).get(fltSegId).add(ssrDTO);

				}
			}
		}

		return segmentWithSSRs;
	}

	public static Map<Integer, Map<String, Integer>> getFlightWiseCount(
			Map<String, Map<Integer, List<HashMap<String, String>>>> paxWiseMap) {
		Map<Integer, Map<String, Integer>> fltWiseCount = new HashMap<Integer, Map<String, Integer>>();

		for (Entry<String, Map<Integer, List<HashMap<String, String>>>> paxEntry : paxWiseMap.entrySet()) {
			Map<Integer, List<HashMap<String, String>>> flightDetailsWithSSRs = paxEntry.getValue();
			for (Entry<Integer, List<HashMap<String, String>>> flightEntry : flightDetailsWithSSRs.entrySet()) {
				int flightSegKey = flightEntry.getKey();
				if (fltWiseCount.get(flightSegKey) == null || !fltWiseCount.containsKey(flightSegKey)) {
					fltWiseCount.put(flightSegKey, new HashMap<String, Integer>());
				}

				Map<String, Integer> ssrCountMap = fltWiseCount.get(flightSegKey);

				List<HashMap<String, String>> serviceList = flightEntry.getValue();
				for (HashMap<String, String> hashMap : serviceList) {
					String ssrCode = hashMap.get("SSR_CODE");
					if (ssrCountMap.get(ssrCode) == null || !ssrCountMap.containsKey(ssrCode)) {
						ssrCountMap.put(ssrCode, 1);
					} else {
						ssrCountMap.put(ssrCode, ssrCountMap.get(ssrCode) + 1);
					}

				}

			}
		}

		return fltWiseCount;
	}

	/**
	 * @param travelerFlightDetailsWithSSRs
	 * @param selectedFlightDTO
	 * @param selectedFlightDTO
	 * @return segmentWithSSRs
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public static Map<String, Map<Integer, List<AirportServiceDTO>>> getSelectedAirportServiceDetails(
			Map<String, Map<Integer, List<HashMap<String, String>>>> travelerFlightDetailsWithSSRs,
			PaxCountAssembler paxCountAssembler, SelectedFlightDTO selectedFlightDTO) throws ModuleException,
			WebservicesException {

		Map<String, Map<Integer, List<AirportServiceDTO>>> segmentWithSSRs = new HashMap<String, Map<Integer, List<AirportServiceDTO>>>();

		Map<Integer, List<String>> airportFltSegIdMap = new HashMap<Integer, List<String>>();

		Map<String, AirportServiceDTO> ssrApplByReserMap = new HashMap<String, AirportServiceDTO>();

		for (String travelerRef : travelerFlightDetailsWithSSRs.keySet()) {
			Map<Integer, List<HashMap<String, String>>> flightDetailsWithSSRs = travelerFlightDetailsWithSSRs.get(travelerRef);

			for (Integer fltSegId : flightDetailsWithSSRs.keySet()) {
				List<HashMap<String, String>> serviceListByAptList = flightDetailsWithSSRs.get(fltSegId);
				Iterator<HashMap<String, String>> itr = serviceListByAptList.iterator();
				while (itr.hasNext()) {
					HashMap<String, String> serviceByAptMap = itr.next();

					String aptCode = serviceByAptMap.get("AIRPORT_CODE");

					if (airportFltSegIdMap.containsKey(fltSegId) && (!airportFltSegIdMap.containsValue(aptCode))) {
						airportFltSegIdMap.get(fltSegId).add(aptCode);
					} else if (airportFltSegIdMap.get(fltSegId) == null) {
						airportFltSegIdMap.put(fltSegId, new ArrayList<String>());
						airportFltSegIdMap.get(fltSegId).add(aptCode);
					}

				}
			}

		}

		Map<String, List<AirportServiceDTO>> selectedAPSMap = WebServicesModuleUtils.getSsrServiceBD()
				.getAvailableAirportServices(airportFltSegIdMap, false);

		setAirportServiceOffers(selectedAPSMap, selectedFlightDTO);

		for (String travelerRef : travelerFlightDetailsWithSSRs.keySet()) {
			Map<Integer, List<HashMap<String, String>>> flightDetailsWithSSRs = travelerFlightDetailsWithSSRs.get(travelerRef);

			for (Integer fltSegId : flightDetailsWithSSRs.keySet()) {
				List<HashMap<String, String>> serviceListByAptList = flightDetailsWithSSRs.get(fltSegId);

				Iterator<HashMap<String, String>> itr = serviceListByAptList.iterator();
				while (itr.hasNext()) {
					HashMap<String, String> serviceByAptMap = itr.next();

					String airportCode = serviceByAptMap.get("AIRPORT_CODE");
					String airportType = serviceByAptMap.get("AIRPORT_TYPE");
					String ssrCode = serviceByAptMap.get("SSR_CODE");

					AirportServiceDTO airportServiceDTO = getAirportServiceDTO(fltSegId, airportCode, ssrCode, selectedAPSMap);

					if (airportServiceDTO == null) {
						throw new WebservicesException(
								IOTACodeTables.AccelaeroErrorCodes_AAE_SSR_NOT_AVAILABLE_IN_REQUESTED_AIRPORT_,
								" Requested Airport Services not available for this flight segment");
					}

					if (airportServiceDTO != null) {

						airportServiceDTO.setAirport(airportCode);
						airportServiceDTO.setApplyOn(airportType);

						if (segmentWithSSRs.get(travelerRef) == null) {
							segmentWithSSRs.put(travelerRef, new HashMap<Integer, List<AirportServiceDTO>>());
							if (segmentWithSSRs.get(travelerRef).get(fltSegId) == null) {
								segmentWithSSRs.get(travelerRef).put(fltSegId, new ArrayList<AirportServiceDTO>());
							}
						} else {
							if (segmentWithSSRs.get(travelerRef).get(fltSegId) == null) {
								segmentWithSSRs.get(travelerRef).put(fltSegId, new ArrayList<AirportServiceDTO>());
							}
						}

						if (AirportServiceDTO.APPLICABILITY_TYPE_RESERVATION.equals(airportServiceDTO.getApplicabilityType())) {
							// airport services applies by reservation
							// applicable by reservation will be added @ last
							String key = fltSegId + AirportServiceDTO.SEPERATOR + airportCode;
							ssrApplByReserMap.put(key, airportServiceDTO);
						} else {
							segmentWithSSRs.get(travelerRef).get(fltSegId).add(airportServiceDTO);
						}

					}

				}

			}
		}

		if (ssrApplByReserMap != null && ssrApplByReserMap.size() > 0) {
			return includeAirportServicesByReservation(paxCountAssembler, segmentWithSSRs, ssrApplByReserMap);
		} else {
			return segmentWithSSRs;
		}
	}

	private static void setAirportServiceOffers(Map<String, List<AirportServiceDTO>> selectedAPSMap,
			SelectedFlightDTO selectedFlightDTO) {
		Map<Integer, Boolean> fltSegWiseHalaFreeOffer = WSAncillaryUtil.getFlightSegmentWiseFreeServiceOffer(selectedFlightDTO,
				EXTERNAL_CHARGES.AIRPORT_SERVICE.toString());

		if (fltSegWiseHalaFreeOffer != null && !fltSegWiseHalaFreeOffer.isEmpty()) {
			for (Entry<String, List<AirportServiceDTO>> HalaEntry : selectedAPSMap.entrySet()) {
				String halaKey = HalaEntry.getKey();
				String[] keyArr = halaKey.split("\\" + AirportServiceDTO.SEPERATOR);

				if (keyArr.length > 1 && keyArr[1] != null) {
					Integer fltSegId = Integer.parseInt(keyArr[1]);
					if (fltSegWiseHalaFreeOffer.containsKey(fltSegId) && fltSegWiseHalaFreeOffer.get(fltSegId) != null) {
						Boolean offerHalaFreeOfCharge = fltSegWiseHalaFreeOffer.get(fltSegId);
						if (offerHalaFreeOfCharge != null && offerHalaFreeOfCharge) {
							List<AirportServiceDTO> apsDTO = HalaEntry.getValue();
							for (AirportServiceDTO airportServiceDTO : apsDTO) {
								airportServiceDTO.setAdultAmount("0");
								airportServiceDTO.setChildAmount("0");
								airportServiceDTO.setInfantAmount("0");
								airportServiceDTO.setReservationAmount("0");
							}
						}

					}
				}

			}
		}
	}

	private static Map<String, Map<Integer, List<AirportServiceDTO>>> includeAirportServicesByReservation(
			PaxCountAssembler paxCountAssembler, Map<String, Map<Integer, List<AirportServiceDTO>>> segmentWithSSRsNew,
			Map<String, AirportServiceDTO> ssrByReserMap) throws ModuleException {

		Map<String, Map<Integer, List<AirportServiceDTO>>> segmentWithSSRsMap = new HashMap<String, Map<Integer, List<AirportServiceDTO>>>();

		try {

			Collection<String> paxTypes = new ArrayList<String>();
			if (paxCountAssembler.getAdultCount() > 0)
				paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
			if (paxCountAssembler.getChildCount() > 0)
				paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));

			int paxSeq = 0;

			for (String paxType : paxTypes) {
				int paxQuantity = getPaxQuantity(paxType, paxCountAssembler);
				for (int i = 0; i < paxQuantity; i++) {
					String travelerRPH = null;
					if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
						travelerRPH = "A" + (++paxSeq);
					} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
						travelerRPH = "C" + (++paxSeq);
					}

					segmentWithSSRsMap.put(travelerRPH, new HashMap<Integer, List<AirportServiceDTO>>());
				}
			}

			for (String travelerRPH : segmentWithSSRsMap.keySet()) {
				if (segmentWithSSRsNew.get(travelerRPH) != null) {
					segmentWithSSRsMap.get(travelerRPH).putAll(segmentWithSSRsNew.get(travelerRPH));
				}

				if (ssrByReserMap != null && ssrByReserMap.size() > 0) {

					for (String fltSegRefKey : ssrByReserMap.keySet()) {

						AirportServiceDTO availableSSRDTO = ssrByReserMap.get(fltSegRefKey);

						String[] fltSegRefKeyArr = fltSegRefKey.split("\\" + AirportServiceDTO.SEPERATOR);

						Integer fltSegRef = Integer.parseInt(fltSegRefKeyArr[0]);

						if (segmentWithSSRsMap.get(travelerRPH) == null) {
							segmentWithSSRsMap.put(travelerRPH, new HashMap<Integer, List<AirportServiceDTO>>());
							if (segmentWithSSRsMap.get(travelerRPH).get(fltSegRef) == null) {
								segmentWithSSRsMap.get(travelerRPH).put(fltSegRef, new ArrayList<AirportServiceDTO>());
							}
						} else {
							if (segmentWithSSRsMap.get(travelerRPH).get(fltSegRef) == null) {
								segmentWithSSRsMap.get(travelerRPH).put(fltSegRef, new ArrayList<AirportServiceDTO>());
							}
						}

						segmentWithSSRsMap.get(travelerRPH).get(fltSegRef).add(availableSSRDTO);

					}

				}

			}

		} catch (Exception e) {
			throw new ModuleException("Error while adding Airport services applies by reservation");
		}

		return segmentWithSSRsMap;
	}

	private static AirportServiceDTO getAirportServiceDTO(Integer fltSegId, String airportCode, String ssrCode,
			Map<String, List<AirportServiceDTO>> selectedAPSMap) {

		if (selectedAPSMap != null && selectedAPSMap.size() > 0) {
			for (String seledtedAPSKey : selectedAPSMap.keySet()) {
				String[] seledtedAPSKeyArr = seledtedAPSKey.split("\\" + AirportServiceDTO.SEPERATOR);

				if (Integer.parseInt(seledtedAPSKeyArr[1]) == fltSegId && seledtedAPSKeyArr[0].equals(airportCode)) {

					List<AirportServiceDTO> airportServiceList = selectedAPSMap.get(seledtedAPSKey);
					Iterator<AirportServiceDTO> itr = airportServiceList.iterator();
					while (itr.hasNext()) {
						AirportServiceDTO airportServiceDTO = itr.next();

						if (airportServiceDTO.getSsrCode().equals(ssrCode)) {
							return airportServiceDTO;
						}
					}

				}
			}
		}

		return null;
	}

	private static SpecialServiceRequestDTO getInflightServiceDTO(String ssrCode, List<SpecialServiceRequestDTO> ssrLisr) {
		for (SpecialServiceRequestDTO specialServiceRequestDTO : ssrLisr) {
			if (specialServiceRequestDTO.getSsrCode().equals(ssrCode)) {
				return specialServiceRequestDTO;
			}
		}

		return null;
	}

	public static Map<String, List<ExternalChgDTO>> getExternalChargesMapForInflightService(
			Map<String, Map<Integer, List<SpecialServiceRequestDTO>>> ssrDetailsMap, PaxCountAssembler paxCountAssembler)
			throws ModuleException {
		Collection<EXTERNAL_CHARGES> exChargestypes = new HashSet<EXTERNAL_CHARGES>();
		exChargestypes.add(EXTERNAL_CHARGES.INFLIGHT_SERVICES);
		Map<EXTERNAL_CHARGES, ExternalChgDTO> quotedMap = WebServicesModuleUtils.getReservationBD().getQuotedExternalCharges(
				exChargestypes, null, null);

		SSRExternalChargeDTO quotedChgDTO = (SSRExternalChargeDTO) quotedMap.get(EXTERNAL_CHARGES.INFLIGHT_SERVICES);

		if (quotedChgDTO == null) {
			throw new ModuleException("Can not locate the SSR external charges details");
		}

		Map<String, List<ExternalChgDTO>> externalCharges = new HashMap<String, List<ExternalChgDTO>>();

		for (Entry<String, Map<Integer, List<SpecialServiceRequestDTO>>> paxEntry : ssrDetailsMap.entrySet()) {
			List<ExternalChgDTO> chargesList = new ArrayList<ExternalChgDTO>();
			Map<Integer, List<SpecialServiceRequestDTO>> fltSSRMap = paxEntry.getValue();
			String travelerRef = paxEntry.getKey();

			for (Entry<Integer, List<SpecialServiceRequestDTO>> fltEntry : fltSSRMap.entrySet()) {
				Integer fltSegId = fltEntry.getKey();
				List<SpecialServiceRequestDTO> ssrList = fltEntry.getValue();

				for (SpecialServiceRequestDTO specialServiceRequestDTO : ssrList) {
					SSRExternalChargeDTO externalChargeTO = new SSRExternalChargeDTO();
					BigDecimal amount = new BigDecimal(specialServiceRequestDTO.getChargeAmount());

					if ((!travelerRef.startsWith(INFANT_REF_PREFIX))) {

						externalChargeTO.setAmount(amount);
						externalChargeTO.setChargeCode(specialServiceRequestDTO.getSsrCode());
						externalChargeTO.setChargeDescription(quotedChgDTO.getChargeCode() + "/"
								+ quotedChgDTO.getChargeDescription());

						externalChargeTO.setChgGrpCode(ReservationInternalConstants.ChargeGroup.SUR);
						externalChargeTO.setChgRateId(quotedChgDTO.getChgRateId());
						externalChargeTO.setExternalChargesEnum(EXTERNAL_CHARGES.INFLIGHT_SERVICES);

						externalChargeTO.setSSRChargeId(specialServiceRequestDTO.getSsrChargeID().intValue());

						externalChargeTO.setSSRId(specialServiceRequestDTO.getSsrID().intValue());
						externalChargeTO.setFlightRPH(fltSegId.toString());
						externalChargeTO.setApplyOn(ReservationPaxSegmentSSR.APPLY_ON_SEGMENT);

						chargesList.add(externalChargeTO);
					}
				}

			}

			externalCharges.put(travelerRef, chargesList);

		}

		return externalCharges;

	}

	public static Map<String, List<ExternalChgDTO>> getExternalChargesMapForAirportServices(
			Map<String, Map<Integer, List<AirportServiceDTO>>> ssrDetailsMap, PaxCountAssembler paxCountAssembler)
			throws ModuleException {

		Collection<EXTERNAL_CHARGES> exChargestypes = new HashSet<EXTERNAL_CHARGES>();
		exChargestypes.add(EXTERNAL_CHARGES.AIRPORT_SERVICE);
		Map<EXTERNAL_CHARGES, ExternalChgDTO> quotedMap = WebServicesModuleUtils.getReservationBD().getQuotedExternalCharges(
				exChargestypes, null, null);

		SSRExternalChargeDTO quotedChgDTO = (SSRExternalChargeDTO) quotedMap.get(EXTERNAL_CHARGES.AIRPORT_SERVICE);

		if (quotedChgDTO == null) {
			throw new ModuleException("Can not locate the SSR external charges details");
		}

		Map<String, List<ExternalChgDTO>> externalCharges = new HashMap<String, List<ExternalChgDTO>>();

		int paxCount = paxCountAssembler.getAdultCount() + paxCountAssembler.getChildCount();

		int i = 1;

		for (String travelerRef : ssrDetailsMap.keySet()) {

			List<ExternalChgDTO> chargesList = new ArrayList<ExternalChgDTO>();

			Map<Integer, List<AirportServiceDTO>> ssrsMap = ssrDetailsMap.get(travelerRef);

			for (Integer flightSegmentId : ssrsMap.keySet()) {
				List<AirportServiceDTO> airportServiceDTOs = ssrsMap.get(flightSegmentId);
				for (AirportServiceDTO airportServiceDTO : airportServiceDTOs) {
					SSRExternalChargeDTO externalChargeTO = new SSRExternalChargeDTO();

					BigDecimal amount = BigDecimal.ZERO;

					if (AirportServiceDTO.APPLICABILITY_TYPE_PAX.equals(airportServiceDTO.getApplicabilityType())) {
						// PAX wise
						if (travelerRef.startsWith(ADULT_REF_PREFIX)) {
							// adult
							amount = new BigDecimal(airportServiceDTO.getAdultAmount());
						} else if (travelerRef.startsWith(CHILD_REF_PREFIX)) {
							// child
							amount = new BigDecimal(airportServiceDTO.getChildAmount());
						} else if (travelerRef.startsWith(INFANT_REF_PREFIX)) {
							// infant
							amount = new BigDecimal(airportServiceDTO.getInfantAmount());
						}

					} else {
						// Reservation
						// if the service is per reservation then split for all PAX(AD+CH)
						amount = new BigDecimal(airportServiceDTO.getReservationAmount());
						BigDecimal[] roundUpValues = AccelAeroCalculator.roundAndSplit(amount, paxCount);
						if (i != paxCount) {
							amount = roundUpValues[0];
						} else {
							amount = roundUpValues[paxCount - 1];
						}

					}

					if ((!travelerRef.startsWith(INFANT_REF_PREFIX))) {

						externalChargeTO.setAmount(amount);
						externalChargeTO.setChargeCode(airportServiceDTO.getSsrCode());
						externalChargeTO.setChargeDescription(quotedChgDTO.getChargeCode() + "/"
								+ quotedChgDTO.getChargeDescription());

						externalChargeTO.setChgGrpCode(ReservationInternalConstants.ChargeGroup.SUR);
						externalChargeTO.setChgRateId(quotedChgDTO.getChgRateId());
						externalChargeTO.setExternalChargesEnum(EXTERNAL_CHARGES.AIRPORT_SERVICE);

						externalChargeTO.setSSRChargeId(airportServiceDTO.getSsrChargeID().intValue());

						externalChargeTO.setSSRId(airportServiceDTO.getSsrID().intValue());
						externalChargeTO.setFlightRPH(flightSegmentId.toString());
						externalChargeTO.setAirportCode(airportServiceDTO.getAirport());
						externalChargeTO.setApplyOn(airportServiceDTO.getApplyOn());

						chargesList.add(externalChargeTO);
					}

				}
			}
			externalCharges.put(travelerRef, chargesList);

			if (travelerRef.startsWith(ADULT_REF_PREFIX) || travelerRef.startsWith(CHILD_REF_PREFIX)) {
				i++;
			}

		}
		return externalCharges;
	}

	private static int getPaxQuantity(String paxType, PaxCountAssembler paxCountAssembler) {

		int paxQuantity = 0;

		if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
			paxQuantity = paxCountAssembler.getAdultCount();
		} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
			paxQuantity = paxCountAssembler.getChildCount();
		} else if (paxType.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
			paxQuantity = paxCountAssembler.getInfantCount();
		}

		return paxQuantity;
	}

	public static Map<String, List<SSRCustomDTO>> processPaxWiseSpecialServices(TravelerInfoType travelerInfoType)
			throws ModuleException, WebservicesException {
		Map<String, List<SSRCustomDTO>> paxWiseServiceMap = new HashMap<String, List<SSRCustomDTO>>();

		List<SpecialReqDetailsType> specialReqList = travelerInfoType.getSpecialReqDetails();

		if (specialReqList == null) {
			return paxWiseServiceMap;
		}

		for (SpecialReqDetailsType specialReqDetailsType : specialReqList) {
			if (specialReqDetailsType.getSSRRequests() != null && specialReqDetailsType.getSSRRequests().getSSRRequest() != null) {
				for (SSRRequest ssrRequest : specialReqDetailsType.getSSRRequests().getSSRRequest()) {
					boolean isAirportService = false;
					String errorMessage = "";

					/*
					 * If service has an airport code then service is an Airport Service otherwise is an In-flight
					 * Service
					 */
					if (ssrRequest.getAirportCode() != null && !"".equals(ssrRequest.getAirportCode())) {
						isAirportService = true;
						errorMessage = "Airport Service(Hala)";
					} else {
						isAirportService = false;
						errorMessage = "In-flight Service";
					}
					if (ssrRequest != null
							&& (ssrRequest.getSsrCode().length() > 4 || !WebServicesModuleUtils.getWebServiceDAO().isValidSSR(
									ssrRequest.getSsrCode()))) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, errorMessage + " code ["
								+ ssrRequest.getSsrCode() + "] is not supported.");
					}

					int ssrId = SSRUtil.getSSRId(ssrRequest.getSsrCode());

					SSRCharge ssrCharge = WebServicesModuleUtils.getChargeBD().getSSRChargesBySSRId(ssrId);

					if (ssrCharge == null) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Service charge for "
								+ errorMessage + " code [" + ssrRequest.getSsrCode() + "] is not available.");
					}

					SSRCustomDTO ssrCustomDTO = new SSRCustomDTO();
					ssrCustomDTO.setSsrCode(ssrRequest.getSsrCode());
					if (isAirportService) {
						ssrCustomDTO.setAirportCode(ssrRequest.getAirportCode());
						ssrCustomDTO.setAirportType(ssrRequest.getAirportType());
					} else {
						ssrCustomDTO.setText(ssrRequest.getText());
						ssrCustomDTO.setAirportType(ReservationPaxSegmentSSR.APPLY_ON_SEGMENT);
					}
					ssrCustomDTO.setDepartureDate(ssrRequest.getDepartureDate());
					ssrCustomDTO.setFlightNumber(ssrRequest.getFlightNumber());
					ssrCustomDTO.getFlightRefNumberRPHList().addAll(ssrRequest.getFlightRefNumberRPHList());

					List<String> paxRphList = null;

					if (isAirportService) {
						if (AirportServiceDTO.APPLICABILITY_TYPE_RESERVATION.equals(ssrCharge.getApplicablityType())) {
							paxRphList = new ArrayList<String>();
							for (AirTravelerType airTraveler : travelerInfoType.getAirTraveler()) {
								if (!CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT).equals(
										airTraveler.getPassengerTypeCode())) {
									paxRphList.add(airTraveler.getTravelerRefNumber().getRPH());
								}
							}
						} else if (AirportServiceDTO.APPLICABILITY_TYPE_PAX.equals(ssrCharge.getApplicablityType())) {
							paxRphList = ssrRequest.getTravelerRefNumberRPHList();
						}
					} else {
						paxRphList = ssrRequest.getTravelerRefNumberRPHList();
					}

					int count = 0;
					for (String paxRph : paxRphList) {

						AirTravelerType airTravelerType = getAirTravelerTypeObj(paxRph, travelerInfoType);

						BigDecimal serviceCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

						if (isAirportService) {
							serviceCharge = getApplicableSSRCharge(
									ssrCharge,
									airTravelerType.getPassengerTypeCode(),
									(airTravelerType.isAccompaniedByInfant() == false ? false : airTravelerType
											.isAccompaniedByInfant()), paxCountWithoutInf(travelerInfoType.getAirTraveler()),
									count);
						} else {
							boolean isUserDefinedSSR = WebServicesModuleUtils.getSsrServiceBD()
									.isUserDefinedSSR(ssrRequest.getSsrCode());
							serviceCharge = ssrCharge.getAdultAmount();
							if (isUserDefinedSSR) {
								serviceCharge = getUserDefinedSSRChargeAmountFromTnx(ssrRequest.getSsrCode(), false);
							}				
						}

						ssrCustomDTO.setServiceCharge(serviceCharge);

						List<SSRCustomDTO> ssrDtoList = null;
						if (paxWiseServiceMap.get(paxRph) == null) {
							paxWiseServiceMap.put(paxRph, new ArrayList<SSRCustomDTO>());
						}

						ssrDtoList = paxWiseServiceMap.get(paxRph);
						if (!isServiceExists(ssrDtoList, ssrCustomDTO, isAirportService)) {
							ssrDtoList.add(ssrCustomDTO);
						}

						count++;
					}
				}
			}
		}

		return paxWiseServiceMap;

	}

	private static boolean isServiceExists(List<SSRCustomDTO> paxServiceList, SSRCustomDTO service, boolean isAirportService) {
		boolean exists = false;

		for (SSRCustomDTO paxService : paxServiceList) {
			if (paxService.getFlightRefNumberRPHList() != null && service.getFlightRefNumberRPHList() != null) {
				String paxFltRef = BeanUtils.getFirstElement(paxService.getFlightRefNumberRPHList()).split("-")[0];
				String fltRef = BeanUtils.getFirstElement(service.getFlightRefNumberRPHList()).split("-")[0];
				if (isAirportService) {
					if (paxService.getSsrCode().equals(service.getSsrCode())
							&& paxService.getAirportCode().equals(service.getAirportCode()) && paxFltRef.equals(fltRef)) {
						exists = true;
						break;
					}
				} else {
					if (paxService.getSsrCode().equals(service.getSsrCode()) && paxFltRef.equals(fltRef)) {
						exists = true;
						break;
					}
				}
			}
		}

		return exists;
	}

	private static AirTravelerType getAirTravelerTypeObj(String travelerRph, TravelerInfoType travelerInfoType) {
		AirTravelerType airTravelerType = null;
		for (AirTravelerType airTraveler : travelerInfoType.getAirTraveler()) {
			if (airTraveler.getTravelerRefNumber().getRPH().equals(travelerRph)) {
				airTravelerType = airTraveler;
				break;
			}
		}

		return airTravelerType;
	}

	private static int paxCountWithoutInf(List<AirTravelerType> airTravelers) {
		int count = 0;

		if (airTravelers != null) {
			for (AirTravelerType airTravelerType : airTravelers) {
				if (!CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT).equals(
						airTravelerType.getPassengerTypeCode())) {
					count++;
				}
			}
		}

		return count;
	}

	private static BigDecimal getApplicableSSRCharge(SSRCharge ssrCharge, String paxType, boolean isParent, int paxCount,
			int paxSeq) {
		BigDecimal serviceCharge = null;

		if (AirportServiceDTO.APPLICABILITY_TYPE_RESERVATION.equals(ssrCharge.getApplicablityType())) {
			serviceCharge = AccelAeroCalculator.roundAndSplit(ssrCharge.getReservationAmount(), paxCount)[paxSeq];
		} else if (AirportServiceDTO.APPLICABILITY_TYPE_PAX.equals(ssrCharge.getApplicablityType())) {
			if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT).equals(paxType) && isParent) {
				serviceCharge = AccelAeroCalculator.add(ssrCharge.getAdultAmount(), ssrCharge.getInfantAmount());
			} else if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT).equals(paxType) && !isParent) {
				serviceCharge = ssrCharge.getAdultAmount();
			} else if (CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD).equals(paxType)) {
				serviceCharge = ssrCharge.getChildAmount();
			}
		}

		return serviceCharge;
	}

	public static boolean isChargableSSR(int ssrId) throws ModuleException {
		boolean isChargableSSR = false;

		SSRCharge ssrCharge = WebServicesModuleUtils.getChargeBD().getSSRChargesBySSRId(ssrId);

		if (ssrCharge != null) {
			if (ssrCharge.getAdultAmount() != null
					&& ssrCharge.getAdultAmount().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) == 1) {
				isChargableSSR = true;
			} else if (ssrCharge.getChildAmount() != null
					&& ssrCharge.getChildAmount().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) == 1) {
				isChargableSSR = true;
			} else if (ssrCharge.getInfantAmount() != null
					&& ssrCharge.getInfantAmount().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) == 1) {
				isChargableSSR = true;
			} else if (ssrCharge.getReservationAmount() != null
					&& ssrCharge.getReservationAmount().compareTo(AccelAeroCalculator.getDefaultBigDecimalZero()) == 1) {
				isChargableSSR = true;
			}
		}

		return isChargableSSR;
	}
	
	public AAOTAAirSSRDetailsRS getUserDefinedSSRDetails(AAOTAAirSSRDetailsRQ aaotaAirSSRDetailsRQ) throws WebservicesException {

		AAOTAAirSSRDetailsRS aaOtaAirSSRDetailsRS = null;
		// Holds Available in-flight services
		List<LCCFlightSegmentSSRDTO> userDefinedServicesAsSSR = null;
		try {
			List<FlightSegmentTO> airportServiceFlightSegmentTOs = new ArrayList<FlightSegmentTO>();
			List<FlightSegmentTO> inflightServiceFlightSegmentTOs = new ArrayList<FlightSegmentTO>();
			boolean isOtherSsrServicesRequested = false;
			for (SSRDetailsRequest ssrDetailsRequest : aaotaAirSSRDetailsRQ.getSSRDetailsRequests().getSSRDetailsRequest()) {
				FlightSegmentType flightSegmentType = ssrDetailsRequest.getFlightSegmentInfo();
				FlightSegmentTO flightSegmentTo = getFlightSegmentTO(flightSegmentType);
				if (flightSegmentTo.getFlightRefNumber() == null || "".equals(flightSegmentTo.getFlightRefNumber())) {
					// If RPH value is not getting from client side, retrieve flight segment id
					if (flightSegmentTo.getFlightSegId() == null) {
						Collection<Integer> segIds = SegmentUtil.getFlightSegmentIds(flightSegmentTo);
						if (segIds.size() > 0) {
							for (Integer segId : segIds) {
								flightSegmentTo.setFlightSegId(segId);
								flightSegmentTo.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentTo));
							}
						}
					} else {
						flightSegmentTo.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentTo));
					}
				}

				SSRDetails ssrDetails = ssrDetailsRequest.getSSRDetails();

				inflightServiceFlightSegmentTOs.add(flightSegmentTo);

				if (ssrDetails != null && ssrDetails.getServiceType() != null
						&& SSRType.USERDEFINED != ssrDetails.getServiceType()) {
					isOtherSsrServicesRequested = true;
				}
			}

			if (isOtherSsrServicesRequested) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_REQUESTED_SSR_SERVICE_TYPE_IS_INVALID_,
						"Cannot combine other service type request with user defined SSR");
			}

			aaOtaAirSSRDetailsRS = new AAOTAAirSSRDetailsRS();

			aaOtaAirSSRDetailsRS.setErrors(new ErrorsType());
			aaOtaAirSSRDetailsRS.setWarnings(new WarningsType());
			aaOtaAirSSRDetailsRS.setSSRDetailsResponses(new SSRDetailsResponses());

			userDefinedServicesAsSSR = WebServicesModuleUtils.getAirproxyAncillaryBD().getSpecialServiceRequests(
					inflightServiceFlightSegmentTOs, aaotaAirSSRDetailsRQ.getTransactionIdentifier(), SYSTEM.AA,
					Locale.ENGLISH.getLanguage(), null, null, false, false, false, true);

			generateUserDefinedSSRServiceResponse(userDefinedServicesAsSSR, aaOtaAirSSRDetailsRS);

			aaOtaAirSSRDetailsRS.setSuccess(new SuccessType());
		} catch (ModuleException e) {
			log.error(e);
			if (e.getExceptionCode().equals("airinventory.flight.notfound")) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_FLIGHT_DOES_NOT_OPERATE_ON_DATE_REQUESTED, "");
			}
			throw new WebservicesException(e);
		}

		return aaOtaAirSSRDetailsRS;
	}

	public static void generateUserDefinedSSRServiceResponse(List<LCCFlightSegmentSSRDTO> inflightServiceList,
			AAOTAAirSSRDetailsRS aaOtaAirSSRDetailsRS) throws ModuleException {
		SSRDetailsResponses ssrDetailsResponses = aaOtaAirSSRDetailsRS.getSSRDetailsResponses();

		for (LCCFlightSegmentSSRDTO lccFlightSegmentSSRDTO : inflightServiceList) {
			SSRDetailsResponse ssrDetailsResponse = new SSRDetailsResponse();
			for (LCCSpecialServiceRequestDTO ssrDto : lccFlightSegmentSSRDTO.getSpecialServiceRequest()) {
				FlightSegmentTO flightSegmentTO = lccFlightSegmentSSRDTO.getFlightSegmentTO();
				FlightSegmentInfo flightSegmentInfo = getFlightSegmentInfo(flightSegmentTO);

				ssrDetailsResponse.setFlightSegmentInfo(flightSegmentInfo);

				UserDefinedSSR inflightService = new UserDefinedSSR();
				String ssrCode = ssrDto.getSsrCode();
				inflightService.setSsrCode(ssrCode);
				inflightService.setSsrName(ssrDto.getSsrName());
				inflightService.setSsrDescription(ssrDto.getDescription());

				BigDecimal definedCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

				try {
					definedCharge = getUserDefinedSSRChargeAmountFromTnx(ssrCode, true);
				} catch (Exception ex) {
				}
				
				if (definedCharge == null)
					definedCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

				inflightService.setAmount(definedCharge);

				ssrDetailsResponse.getUserDefinedSSR().add(inflightService);
			}

			ssrDetailsResponses.getSSRDetailsResponse().add(ssrDetailsResponse);
		}
	}

	public AAOTAAirSSRSetAmountRS setSSRPrice(AAOTAAirSSRSetAmountRQ aaotaAirSSRSetAmountRQ) throws WebservicesException {

		AAOTAAirSSRSetAmountRS aaotaAirSSRSetAmountRS = new AAOTAAirSSRSetAmountRS();

		Map<String, BigDecimal> userDefinedSsrPriceMap = new HashMap<>();

		for (UserDefinedSSRAmount userDefinedSSRAmount : aaotaAirSSRSetAmountRQ.getUserDefinedSSRAmounts()
				.getUserDefinedSSRAmount()) {
			BigDecimal amount = userDefinedSSRAmount.getAmount();
			if (amount == null || AccelAeroCalculator.isLessThan(amount, AccelAeroCalculator.getDefaultBigDecimalZero())) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_AMOUNT,
						"User Defined SSR Charge Amount is invalid");
			}
			amount = AccelAeroCalculator.scaleValueDefault(amount);
			userDefinedSsrPriceMap.put(userDefinedSSRAmount.getSsrCode(), amount);
		}

		ThreadLocalData.setCurrentTnxParam(Transaction.USER_DEFINED_SSR_PRICE, userDefinedSsrPriceMap);
		aaotaAirSSRSetAmountRS.setSSRSetAmountSucsses(true);

		return aaotaAirSSRSetAmountRS;
	}
	
}
