package com.isa.thinair.webservices.core.web;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimerTask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.config.WebservicesConfig;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.isa.thinair.wsclient.api.service.WSClientBD;

/**
 * Timer Task for External Payment Transaction Reconcilation
 * 
 * @author Mohamed Nasly
 */
public class TxnReconcilerTimerTask extends TimerTask {

	private static Log log = LogFactory.getLog(TxnReconcilerTimerTask.class);
	String serviceProviderCode = null;

	public TxnReconcilerTimerTask(String serviceProviderCode) {
		this.serviceProviderCode = serviceProviderCode;
	}

	@Override
	public void run() {
		try {
			log.info("Start External Payment Transaction Reconcilation Task " + "[Current Timestamp="
					+ WebservicesConstants.DATE_FORMAT_FOR_LOGGING.format(new Date()) + "]");

			WSClientBD wsClientBD = WebServicesModuleUtils.getWSClientBD();

			int dayOffset = decideDayOffset();

			Calendar startTimestamp = new GregorianCalendar();
			startTimestamp.add(Calendar.DAY_OF_MONTH, dayOffset);
			startTimestamp.set(Calendar.HOUR_OF_DAY, 0);
			startTimestamp.set(Calendar.MINUTE, 0);
			startTimestamp.set(Calendar.SECOND, 0);
			startTimestamp.set(Calendar.MILLISECOND, 0);

			Calendar endTimestmap = new GregorianCalendar();
			endTimestmap.add(Calendar.DAY_OF_MONTH, dayOffset);
			endTimestmap.set(Calendar.HOUR_OF_DAY, 23);
			endTimestmap.set(Calendar.MINUTE, 59);
			endTimestmap.set(Calendar.SECOND, 59);
			endTimestmap.set(Calendar.MILLISECOND, 999);

			log.info("External Payment Transaction Reconcilation Task " + "[startTimestamp="
					+ WebservicesConstants.DATE_FORMAT_FOR_LOGGING.format(startTimestamp.getTime()) + ",endTimestamp="
					+ WebservicesConstants.DATE_FORMAT_FOR_LOGGING.format(endTimestmap.getTime()) + "]");
			WebservicesConfig wsconfig =  WebServicesModuleUtils.getWebServicesConfig();
			ForceLoginInvoker.login(wsconfig.getUserName(), wsconfig.getPassword());
			wsClientBD.requestDailyBankTnxReconcilation(startTimestamp.getTime(), endTimestmap.getTime(),
					this.serviceProviderCode);

			log.info("Completed External Payment Transaction Reconcilation Task " + "[Current Timestamp="
					+ WebservicesConstants.DATE_FORMAT_FOR_LOGGING.format(new Date()) + "]");
		} catch (Throwable e) {
			log.error("External Payment Txn Reconciliation Job Failed.", e);
		}finally {
			ForceLoginInvoker.close();
		}
	}

	private int decideDayOffset() {
		int dayOffset = 0;
		Calendar curdateCal = new GregorianCalendar();
		curdateCal.setTime(new Date());
		// if (curdateCal.get(Calendar.HOUR_OF_DAY) == 0){
		dayOffset = -1;
		// }

		return dayOffset;
	}
}
