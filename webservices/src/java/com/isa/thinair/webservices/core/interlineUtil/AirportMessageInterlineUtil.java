package com.isa.thinair.webservices.core.interlineUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.opentravel.ota._2003._05.FreeTextType;

import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.AirportFlightDetail;
import com.isa.thinair.commons.api.dto.AirportMessageDisplayDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

public class AirportMessageInterlineUtil {

	public static List<FreeTextType> retrieveAirportMessages(List<OriginDestinationOptionTO> selectedFlightOptions,
			String... stages) throws ModuleException {

		List<FreeTextType> airportMessages = null;

		List<AirportFlightDetail> airportMessageFlightDetailList = new ArrayList<AirportFlightDetail>();
		AirportFlightDetail departureFlightDetail = null;
		AirportFlightDetail arrivalFlightDetail = null;

		for (OriginDestinationOptionTO originDestinationOption : selectedFlightOptions) {
			if (originDestinationOption.isSelected()) {
				Date currentDate = new Date();
				for (FlightSegmentTO flightSegmentTO : originDestinationOption.getFlightSegmentList()) {
					for (String stage : stages) {
						String segmentCode = flightSegmentTO.getSegmentCode();
						String[] segmentArray = segmentCode.split("/");

						departureFlightDetail = new AirportFlightDetail();
						departureFlightDetail.setStage(stage);
						departureFlightDetail.setFlightNumber(flightSegmentTO.getFlightNumber());
						departureFlightDetail.setTravelDate(currentDate);
						departureFlightDetail.setFlightDepDate(flightSegmentTO.getDepartureDateTimeZulu());
						departureFlightDetail.setSalesChannel(ReservationInternalConstants.AirportMessageSalesChannel.API);
						departureFlightDetail.setTravelWay(ReservationInternalConstants.AirportMessageTravelWay.DEP);
						departureFlightDetail.setAirportCode(segmentArray[0]);
						departureFlightDetail.setOnd(segmentCode);
						airportMessageFlightDetailList.add(departureFlightDetail);

						arrivalFlightDetail = new AirportFlightDetail();
						arrivalFlightDetail.setStage(stage);
						arrivalFlightDetail.setFlightNumber(flightSegmentTO.getFlightNumber());
						arrivalFlightDetail.setTravelDate(currentDate);
						arrivalFlightDetail.setFlightDepDate(flightSegmentTO.getDepartureDateTimeZulu());
						arrivalFlightDetail.setSalesChannel(ReservationInternalConstants.AirportMessageSalesChannel.API);
						arrivalFlightDetail.setTravelWay(ReservationInternalConstants.AirportMessageTravelWay.ARR);
						arrivalFlightDetail.setAirportCode(segmentArray[segmentArray.length - 1]);
						arrivalFlightDetail.setOnd(segmentCode);
						airportMessageFlightDetailList.add(arrivalFlightDetail);
					}

				}
			}
		}

		if (!airportMessageFlightDetailList.isEmpty()) {
			List<AirportMessageDisplayDTO> airportMessageDisplayDTOList = WebServicesModuleUtils.getCommonMasterBD()
					.airportMessages(airportMessageFlightDetailList);

			if (airportMessageDisplayDTOList != null) {
				airportMessages = new ArrayList<FreeTextType>();

				FreeTextType airportMessage = null;
				for (AirportMessageDisplayDTO airportMessageDisplayDTO : airportMessageDisplayDTOList) {
					airportMessage = new FreeTextType();
					StringBuilder msg = new StringBuilder();
					msg.append(ReservationInternalConstants.AirportMessageSuffix.API_SUFFIX);
					msg.append(airportMessageDisplayDTO.getAirportMessage());

					airportMessage.setValue(msg.toString());
					airportMessages.add(airportMessage);
				}

			}
		}
		return airportMessages;
	}
}
