package com.isa.thinair.webservices.core.service;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isa.thinair.commons.api.dto.ets.AeroMartUpdateCouponStatusRq;
import com.isa.thinair.commons.api.dto.ets.AeroMartUpdateCouponStatusRs;
import com.isa.thinair.commons.api.dto.ets.AeroMartUpdateExternalCouponStatusRq;
import com.isa.thinair.commons.api.dto.ets.AeroMartUpdateExternalCouponStatusRs;
import com.isa.thinair.commons.api.dto.ets.ServiceError;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webservices.api.service.AATicketServices;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

@Controller
@RequestMapping(value = "aeromart/ticket",
		method = RequestMethod.POST,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
)
public class AATicketServicesImpl implements AATicketServices {

	@ResponseBody
	@RequestMapping(value = "/updateCouponStatus")
	public AeroMartUpdateCouponStatusRs updateCouponStatus(@RequestBody AeroMartUpdateCouponStatusRq request) {
		AeroMartUpdateCouponStatusRs response = null;
		try {
			response = WebServicesModuleUtils.getGdsServicesBD().updateCouponStatus(request);
		} catch (ModuleException e) {
			response = new AeroMartUpdateCouponStatusRs();
			response.setSuccess(false);
			ServiceError serviceError = new ServiceError();
			serviceError.setCode(e.getExceptionCode());
			serviceError.setMessage(e.getMessage());
			response.setServiceError(serviceError);
		}
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/updateExternalCouponStatus")
	public AeroMartUpdateExternalCouponStatusRs updateExternalCouponStatus(@RequestBody AeroMartUpdateExternalCouponStatusRq request) {

		AeroMartUpdateExternalCouponStatusRs response = null;
		try {
			response = new AeroMartUpdateExternalCouponStatusRs();
			response.setSuccess(true);
			response = WebServicesModuleUtils.getGdsServicesBD().updateExternalCouponStatus(request);
		} catch (ModuleException e) {
			response = new AeroMartUpdateExternalCouponStatusRs();
			response.setSuccess(false);
			ServiceError serviceError = new ServiceError();
			serviceError.setCode(e.getExceptionCode());
			serviceError.setMessage(e.getMessage());
			response.setServiceError(serviceError);
		}
		return response;

	}
}
