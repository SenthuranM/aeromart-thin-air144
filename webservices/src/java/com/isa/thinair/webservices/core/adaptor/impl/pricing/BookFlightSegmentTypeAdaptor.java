package com.isa.thinair.webservices.core.adaptor.impl.pricing;

import org.opentravel.ota._2003._05.AAOTAAirAllPriceAvailRS;
import org.opentravel.ota._2003._05.AAOTAAirAllPriceAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment;
import org.opentravel.ota._2003._05.BookFlightSegmentType;

import com.isa.thinair.webservices.core.adaptor.Adaptor;

public class BookFlightSegmentTypeAdaptor
		implements
		Adaptor<AAOTAAirAllPriceAvailRS.OriginDestinationInformation.OriginDestinationOptions.OriginDestinationOption.FlightSegment, BookFlightSegmentType> {

	@Override
	public BookFlightSegmentType adapt(FlightSegment source) {

		BookFlightSegmentType target = new BookFlightSegmentType();

		target.setArrivalAirport(source.getArrivalAirport());
		target.setArrivalDateTime(source.getArrivalDateTime());
		// target.setAvailableFlexiOperations();
		// target.setBookingClassAvails(value);
		target.setDepartureAirport(source.getDepartureAirport());
		target.setDepartureDateTime(source.getDepartureDateTime());
		// target.setDepartureDay(source.getDe);
		// target.setETicketEligibility(value);
		target.setFlightNumber(source.getFlightNumber());
		target.setInfoSource(source.getInfoSource());
		target.setMarketingAirline(source.getMarketingAirline());
		// target.setMarriageGrp();
		// target.setMealCode(value);
		// target.setNumberInParty(value);
		target.setOperatingAirline(source.getOperatingAirline());
		// target.setResBookDesigCode(source.getRe);
		// target.setResCabinClass();
		// target.setReturnFlag();
		target.setRPH(source.getRPH());
		target.setSegmentCode(source.getSegmentCode());
		// target.setStatus(source);
		// target.setStopQuantity(value);
		// target.setTourOperatorFlightID(value);

		return target;
	}

}
