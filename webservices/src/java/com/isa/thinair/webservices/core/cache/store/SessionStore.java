package com.isa.thinair.webservices.core.cache.store;

import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.session.AAUserSession;

public interface SessionStore {

	AAUserSession createNewUserSession(String userID, String password, int salesChannelId) throws WebservicesException;

	AAUserSession getUserSession(String userID) throws WebservicesException;

	void removeExpiredUserSessions();

	void flushUserSession(AAUserSession aaUserSession) throws WebservicesException;

}
