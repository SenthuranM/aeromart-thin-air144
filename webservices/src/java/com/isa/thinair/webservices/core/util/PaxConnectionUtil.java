package com.isa.thinair.webservices.core.util;

import com.isa.thinair.airreservation.api.dto.ConnectionPaxInfoTO;
import com.isa.thinair.airschedules.api.dto.RouteInfoTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isaaviation.thinair.webservices.api.connectiondetails.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class PaxConnectionUtil {

    private static final Log log = LogFactory.getLog(PaxConnectionUtil.class);

    public static AAGetRouteRS getConnections(AAGetRouteRQ aaGetRouteRQ) {

        AAGetRouteRS aaGetRouteRS = new AAGetRouteRS();

         try {
             List<RouteInfoTO> routeInfo = WebServicesModuleUtils.getFlightBD().getAvailableRoutes(aaGetRouteRQ.getOrigin());
             aaGetRouteRS.getRoutes().addAll(adapt(routeInfo));

        } catch (ModuleException e) {
            log.error("Available routes retrieval failed");
        }

        return aaGetRouteRS;
    }

    public static AAGetConnectionPaxRS getConnectionPax(AAGetConnectionPaxRQ aaGetConnectionPaxRQ) {

        AAGetConnectionPaxRS aaGetConnectionPaxRS = new AAGetConnectionPaxRS();

        try {
            List<ConnectionPaxInfoTO> connectionPax = WebServicesModuleUtils.getPassengerBD().getConnectionPaxInfo(
                    new Date(aaGetConnectionPaxRQ.getStartDate()), new Date(aaGetConnectionPaxRQ.getEndDate()),
                    aaGetConnectionPaxRQ.getConnectionTime(), aaGetConnectionPaxRQ.getSegments());

            List<AAGetConnectionPaxInfoRS> aaGetConnectionPaxInfoRSes =  adaptConnectionPax(connectionPax);

            aaGetConnectionPaxRS.getConnectionPaxDetails().addAll(aaGetConnectionPaxInfoRSes);

        } catch (ModuleException e) {
            log.error("Connection passenger retrieval failed");
        }

        return aaGetConnectionPaxRS;
    }

    private static List<AARoute> adapt(List<RouteInfoTO> routeInfo) {

        if(routeInfo == null || routeInfo.isEmpty()) {
            return Collections.emptyList();
        }

        List<AARoute> aaRoutes = new ArrayList<>();

        for (RouteInfoTO routeInfoTO : routeInfo) {
            AARoute aaRoute = new AARoute();
            aaRoute.setOndCode(routeInfoTO.getOndCode());
            aaRoute.setIsDirectRoute(routeInfoTO.isDirectRoute());
            aaRoutes.add(aaRoute);
        }

        return aaRoutes;
    }

    private static List<AAGetConnectionPaxInfoRS> adaptConnectionPax(List<ConnectionPaxInfoTO> connectionPax) {

        if(connectionPax == null || connectionPax.isEmpty()) {
            return Collections.emptyList();
        }

        List<AAGetConnectionPaxInfoRS> aaGetConnectionPaxInfoRSes = new ArrayList<>();

        for (ConnectionPaxInfoTO connectionPaxInfoTO : connectionPax) {

            AAGetConnectionPaxInfoRS aaGetConnectionPaxInfoRS = new AAGetConnectionPaxInfoRS();

            aaGetConnectionPaxInfoRS.setDate(connectionPaxInfoTO.getDate().getTime());
            aaGetConnectionPaxInfoRS.setSegmentCode(connectionPaxInfoTO.getSegmentCode());
            aaGetConnectionPaxInfoRS.setTotalPax(connectionPaxInfoTO.getTotalPax());
            aaGetConnectionPaxInfoRS.setConnectionPax(connectionPaxInfoTO.getConnectionPax());

            aaGetConnectionPaxInfoRSes.add(aaGetConnectionPaxInfoRS);

        }

        return aaGetConnectionPaxInfoRSes;
    }
}
