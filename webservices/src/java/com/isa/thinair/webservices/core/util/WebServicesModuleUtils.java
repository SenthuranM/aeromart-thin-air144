/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006/07 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.core.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.security.auth.login.LoginException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.cache.CacheManager;

import com.isa.thinair.aircustomer.api.service.AirCustomerServiceBD;
import com.isa.thinair.aircustomer.api.service.LoyaltyCreditServiceBD;
import com.isa.thinair.aircustomer.api.service.LoyaltyCustomerServiceBD;
import com.isa.thinair.aircustomer.api.utils.AircustomerConstants;
import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;
import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airinventory.api.service.LogicalCabinClassBD;
import com.isa.thinair.airinventory.api.service.MealBD;
import com.isa.thinair.airinventory.api.service.SeatMapBD;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.service.LocationBD;
import com.isa.thinair.airmaster.api.service.SsrBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airpricing.api.utils.AirpricingConstants;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.service.AirproxyAncillaryBD;
import com.isa.thinair.airproxy.api.service.AirproxyAutomaticCheckinBD;
import com.isa.thinair.airproxy.api.service.AirproxyPassengerBD;
import com.isa.thinair.airproxy.api.service.AirproxyReservationBD;
import com.isa.thinair.airproxy.api.service.AirproxyReservationQueryBD;
import com.isa.thinair.airproxy.api.utils.AirproxyConstants;
import com.isa.thinair.airreservation.api.dto.PnrChargesDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.service.ScheduleBD;
import com.isa.thinair.airschedules.api.utils.AirschedulesConstants;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.airsecurity.api.utils.AirsecurityConstants;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.airtravelagents.api.service.TravelAgentFinanceBD;
import com.isa.thinair.airtravelagents.api.utils.AirtravelagentsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.gdsservices.api.service.GDSServicesBD;
import com.isa.thinair.gdsservices.api.utils.GdsservicesConstants;
import com.isa.thinair.lccclient.api.service.LCCSearchAndQuoteBD;
import com.isa.thinair.lccclient.api.utils.LccclientConstants;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.msgbroker.api.service.TypeBServiceBD;
import com.isa.thinair.msgbroker.api.utils.MsgbrokerConstants;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.utils.PaymentbrokerConstants;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.platform.core.bean.UnifiedBeanFactory;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.promotion.api.utils.PromotionConstants;
import com.isa.thinair.reporting.api.service.DataExtractionBD;
import com.isa.thinair.reporting.api.utils.ReportingConstants;
import com.isa.thinair.webplatform.core.util.LookupUtils;
import com.isa.thinair.webplatform.core.util.WebServicesDAO;
import com.isa.thinair.webservices.core.cache.store.SessionStore;
import com.isa.thinair.webservices.core.cache.store.TransactionStore;
import com.isa.thinair.webservices.core.cache.store.impl.SessionStoreFactory;
import com.isa.thinair.webservices.core.cache.store.impl.TransactionStoreFactory;
import com.isa.thinair.webservices.core.config.AmadeusConfig;
import com.isa.thinair.webservices.core.config.MyIDTravelConfig;
import com.isa.thinair.webservices.core.config.WebservicesConfig;
import com.isa.thinair.wsclient.api.service.WSClientBD;
import com.isa.thinair.wsclient.api.utils.WsclientConstants;

/**
 * @author Mohamed Nasly
 * @author Nilindra
 * @author Mehdi
 */
public class WebServicesModuleUtils {
	private static Log log = LogFactory.getLog(WebServicesModuleUtils.class);

	/**
	 * Lookup an exernal module
	 * 
	 * @param moduleName
	 * @return IModule Module Implemenation
	 */
	public static IModule lookupModule(String moduleName) {
		return LookupServiceFactory.getInstance().getModule(moduleName);
	}

	/**
	 * Lookup an external module's BD
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @return IServiceDelegate BD implementation
	 */
	private static IServiceDelegate lookupServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getWebServicesConfig(),
				"webservices", "webservices.config.dependencymap.invalid");
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getWebServicesConfig(), "webservices",
				"webservices.config.dependencymap.invalid");
	}

	/**
	 * Retrieves the webservices module configurations
	 * 
	 * @return WebservicesConfig
	 */
	public static WebservicesConfig getWebServicesConfig() {
		return (WebservicesConfig) LookupServiceFactory.getInstance().getBean(
				UnifiedBeanFactory.URI_SCHEME_PLUS_SLASHES + "modules/webservices?id=webservicesModuleConfig");
	}
	
	
	/**
	 * Retrieves the bean by given name and type
	 * @return bean 
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getBeanByName(String beanName) {
		return (T)LookupServiceFactory.getInstance().getBean(
				UnifiedBeanFactory.URI_SCHEME_PLUS_SLASHES + "modules/webservices?id=" + beanName);
	}
	
	public static TransactionStore getTrasnsactionStore() {
		TransactionStoreFactory factory = getBeanByName("transactionStoreFactory");
		return factory.getStore();
	}

	public static SessionStore getSessionStore() {
		SessionStoreFactory factory = getBeanByName("sessionStoreFactory");
		return factory.getStore();
	}

	/**
	 * Retrieves application's global configurations
	 * 
	 * @return GlobalConfig
	 */
	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}

	/**
	 * Return reservation query service delegate
	 * 
	 * @return
	 */
	public static ReservationQueryBD getReservationQueryBD() {
		return (ReservationQueryBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationQueryBD.SERVICE_NAME);
	}

	/**
	 * Return reservation service delegate
	 * 
	 * @return
	 */
	public static ReservationBD getReservationBD() {
		return (ReservationBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationBD.SERVICE_NAME);
	}

	public static AirCustomerServiceBD getCustomerBD() {
		return (AirCustomerServiceBD) lookupEJB3Service(AircustomerConstants.MODULE_NAME, AirCustomerServiceBD.SERVICE_NAME);
	}

	/**
	 * Return passenger service delegate
	 * 
	 * @return
	 */
	public static PassengerBD getPassengerBD() {
		return (PassengerBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, PassengerBD.SERVICE_NAME);
	}

	/**
	 * Return flight service delegate
	 * 
	 * @return
	 */
	public static FlightBD getFlightBD() {
		return (FlightBD) lookupEJB3Service(AirschedulesConstants.MODULE_NAME, FlightBD.SERVICE_NAME);
	}

	public static FareBD getFareBD() {
		return (FareBD) lookupEJB3Service(AirpricingConstants.MODULE_NAME, FareBD.SERVICE_NAME);
	}

	public static CommonMasterBD getCommonMasterBD() {
		return (CommonMasterBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, CommonMasterBD.SERVICE_NAME);
	}

	/**
	 * @author Byorn
	 * @return ScheduleBD
	 */
	public static ScheduleBD getScheduleBD() {
		return (ScheduleBD) lookupEJB3Service(AirschedulesConstants.MODULE_NAME, ScheduleBD.SERVICE_NAME);
	}

	public static WebServicesDAO getWebServiceDAO() {
		return LookupUtils.getWebServiceDAO();
	}

	public static SegmentBD getResSegmentBD() {
		return (SegmentBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, SegmentBD.SERVICE_NAME);
	}

	public static SecurityBD getSecurityBD() {
		return (SecurityBD) lookupEJB3Service(AirsecurityConstants.MODULE_NAME, SecurityBD.SERVICE_NAME);
	}

	public static FlightInventoryBD getFlightInventoryBD() {
		return (FlightInventoryBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, FlightInventoryBD.SERVICE_NAME);
	}

	public static FlightInventoryResBD getFlightInventoryResBD() {
		return (FlightInventoryResBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, FlightInventoryResBD.SERVICE_NAME);
	}

	public static TravelAgentBD getTravelAgentBD() {
		return (TravelAgentBD) lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME, TravelAgentBD.SERVICE_NAME);
	}

	public static ChargeBD getChargeBD() {
		return (ChargeBD) lookupEJB3Service(AirpricingConstants.MODULE_NAME, ChargeBD.SERVICE_NAME);
	}

	public static TravelAgentFinanceBD getTravelAgentFinanceBD() {
		return (TravelAgentFinanceBD) lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME, TravelAgentFinanceBD.SERVICE_NAME);
	}

	/**
	 * Gets the Data Extraction Service from Reporting
	 * 
	 * @return DataExtractionBD the data extraction delegate
	 */
	public static DataExtractionBD getDataExtractionBD() {
		return (DataExtractionBD) lookupEJB3Service(ReportingConstants.MODULE_NAME, DataExtractionBD.SERVICE_NAME);
	}

	/**
	 * Return transparent airport class delegate
	 * 
	 * @return
	 */
	public static AirportBD getAirportBD() {
		return (AirportBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, AirportBD.SERVICE_NAME);
	}

	public static AirproxyAncillaryBD getAirproxyAncillaryBD() {
		return (AirproxyAncillaryBD) lookupEJB3Service(AirproxyConstants.MODULE_NAME, AirproxyAncillaryBD.SERVICE_NAME);
	}

	/**
	 * Return transparent booking class delegate
	 * 
	 * @return
	 */
	public static BookingClassBD getBookingClassBD() {
		return (BookingClassBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, BookingClassBD.SERVICE_NAME);
	}

	public static LogicalCabinClassBD getLogicalCabinClassBD() {
		return (LogicalCabinClassBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, LogicalCabinClassBD.SERVICE_NAME);
	}

	public static WSClientBD getWSClientBD() {
		return (WSClientBD) lookupEJB3Service(WsclientConstants.MODULE_NAME, WSClientBD.SERVICE_NAME);
	}

	public static SsrBD getSsrServiceBD() {
		return (SsrBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, SsrBD.SERVICE_NAME);
	}
	
	public static TypeBServiceBD getTypeBServiceBD() {
		return (TypeBServiceBD) lookupEJB3Service(MsgbrokerConstants.MODULE_NAME, TypeBServiceBD.SERVICE_NAME);
	}
	
	public static LocationBD getLocationDB() {
		return (LocationBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, LocationBD.SERVICE_NAME);
	}

	public static class ReservationUtil {
		public static Reservation getReservation(String pnr, TrackInfoDTO trackDTO, boolean loadFares, boolean loadPaxAvaBalance,
				boolean loadSegView, boolean loadLocalTimes, boolean loadLastUserNote, boolean loadSegViewBookingTypes,
				boolean loadOndChargesView) throws ModuleException {
			LCCClientPnrModesDTO modes = new LCCClientPnrModesDTO();
			modes.setPnr(pnr);
			modes.setLoadFares(loadFares);
			modes.setLoadLastUserNote(loadLastUserNote);
			modes.setLoadLocalTimes(loadLocalTimes);
			modes.setLoadOndChargesView(loadOndChargesView);
			modes.setLoadPaxAvaBalance(loadPaxAvaBalance);
			modes.setLoadSegView(loadSegView);
			modes.setLoadSegViewBookingTypes(loadSegViewBookingTypes);
			Reservation res = getReservationBD().getReservation(modes, trackDTO);
			return res;
		}

		public static Collection<PnrChargesDTO> getOndChargesForCancellation(String pnr, long version, Integer... segmentIds)
				throws ModuleException {
			return new ArrayList<PnrChargesDTO>(WebServicesModuleUtils.getResSegmentBD().getOndChargesForCancellation(pnr,
					Arrays.asList(segmentIds), false, version, null, false, false));
		}
	}

	public static LoyaltyCustomerServiceBD getLoyaltyCustomerBD() {
		return (LoyaltyCustomerServiceBD) lookupEJB3Service(AircustomerConstants.MODULE_NAME,
				LoyaltyCustomerServiceBD.SERVICE_NAME);
	}

	public static LoyaltyCreditServiceBD getLoyaltyCreditBD() {
		return (LoyaltyCreditServiceBD) lookupEJB3Service(AircustomerConstants.MODULE_NAME, LoyaltyCreditServiceBD.SERVICE_NAME);
	}

	public static AirCustomerServiceBD getAirCustomerServiceBD() {
		return (AirCustomerServiceBD) lookupEJB3Service(AircustomerConstants.MODULE_NAME, AirCustomerServiceBD.SERVICE_NAME);
	}

	public static PaymentBrokerBD getPaymentBrokerBD() {
		return (PaymentBrokerBD) lookupEJB3Service(PaymentbrokerConstants.MODULE_NAME, PaymentBrokerBD.SERVICE_NAME);
	}

	public static AmadeusConfig getAmadeusConfig() {
		return (AmadeusConfig) LookupServiceFactory.getInstance().getBean(
				UnifiedBeanFactory.URI_SCHEME_PLUS_SLASHES + "modules/webservices?id=amadeusConfig");
	}

	public static MyIDTravelConfig getMyIDTravelConfig() {
		return (MyIDTravelConfig) LookupServiceFactory.getInstance().getBean(
				UnifiedBeanFactory.URI_SCHEME_PLUS_SLASHES + "modules/webservices?id=myIDTravelConfig");
	}

	/**
	 * Get AirproxyReservationBD
	 * 
	 * @return AirproxyReservationBD the Proxy Reservation Delegate
	 */
	public final static AirproxyReservationBD getAirproxyReservationBD() {
		AirproxyReservationBD airproxyReservationBD = null;
		try {
			airproxyReservationBD = (AirproxyReservationBD) lookupEJB3Service(AirproxyConstants.MODULE_NAME,
					AirproxyReservationQueryBD.SERVICE_NAME);
		} catch (Exception ex) {
			log.error("Error locating AirproxyReservationBD", ex);
		}
		return airproxyReservationBD;
	}

	/**
	 * Get AirproxyPassengerBD
	 * 
	 * @return AirproxyPassengerBD the Proxy Reservation Delegate
	 */
	public final static AirproxyPassengerBD getAirproxyPassengerBD() {
		AirproxyPassengerBD airproxyPassenger = null;
		try {
			airproxyPassenger = (AirproxyPassengerBD) lookupEJB3Service(AirproxyConstants.MODULE_NAME,
					AirproxyPassengerBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating AirproxyPassengerBD :", e);
		}
		return airproxyPassenger;
	}

	public static SeatMapBD getSeatMapBD() {
		return (SeatMapBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, SeatMapBD.SERVICE_NAME);
	}

	public static MealBD getMealBD() {
		return (MealBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, MealBD.SERVICE_NAME);
	}

	public static BaggageBusinessDelegate getBaggageBD() {
		return (BaggageBusinessDelegate) lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				BaggageBusinessDelegate.SERVICE_NAME);
	}

	/**
	 * Get AirproxyReservationQuery BD
	 * 
	 * @return AirproxySearchAndQuoteBD the Proxy Search and Quote Delegate
	 */
	public final static AirproxyReservationQueryBD getAirproxySearchAndQuoteBD() {
		AirproxyReservationQueryBD airproxySearchAndQuoteBD = null;
		try {
			airproxySearchAndQuoteBD = (AirproxyReservationQueryBD) lookupEJB3Service(AirproxyConstants.MODULE_NAME,
					AirproxyReservationQueryBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating AirproxySearchAndQuoteBD :", e);
		}
		return airproxySearchAndQuoteBD;
	}

	/**
	 * gets the lcc client biz delegate
	 * 
	 * @return LCCSearchAndQuoteBD
	 */
	public final static LCCSearchAndQuoteBD getLCCSearchAndQuoteBD() {
		LCCSearchAndQuoteBD lccSearchAndQuoteBD = null;
		try {
			lccSearchAndQuoteBD = (LCCSearchAndQuoteBD) lookupEJB3Service(LccclientConstants.MODULE_NAME,
					LCCSearchAndQuoteBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating getLCCSearchAndQuoteBD: ", e);
		}
		return lccSearchAndQuoteBD;
	}

	public final static LoyaltyManagementBD getLoyaltyManagementBD() {
		return (LoyaltyManagementBD) lookupEJB3Service(PromotionConstants.MODULE_NAME, LoyaltyManagementBD.SERVICE_NAME);
	}


	public final static GDSServicesBD getGdsServicesBD() throws ModuleException {
		if (getWebServicesConfig().isInvokeCredentials()) {
			try {
				ForceLoginInvoker.webserviceLogin(getWebServicesConfig().getEjbAccessUsername(),
						getWebServicesConfig().getEjbAccessPassword(), SalesChannelsUtil.SALES_CHANNEL_GDS);
			} catch (LoginException e) {
				throw new ModuleException(e, "module.invalid.user");
			}
		}
		return (GDSServicesBD) lookupEJB3Service(GdsservicesConstants.MODULE_NAME, GDSServicesBD.SERVICE_NAME);
	}
	
	public final static AirproxyAutomaticCheckinBD getAutomaticCheckinBD() {
		AirproxyAutomaticCheckinBD airproxyAutomaticCheckinBD = null;
		try {
			airproxyAutomaticCheckinBD = (AirproxyAutomaticCheckinBD) lookupEJB3Service(AirproxyConstants.MODULE_NAME,
					AirproxyAutomaticCheckinBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating AirproxyAutomaticCheckinBD :", e);
		}
		return airproxyAutomaticCheckinBD;
	}

	/**
	 * Return Reservation Auxilliary Business delegate
	 * 
	 * @return
	 */
	public static ReservationAuxilliaryBD getReservationAuxilliaryBD() {
		ReservationAuxilliaryBD auxilliaryBD = null;

		try {
			if (getWebServicesConfig().isInvokeCredentials()) {
				try {
					ForceLoginInvoker.webserviceLogin(getWebServicesConfig().getEjbAccessUsername(), getWebServicesConfig()
							.getEjbAccessPassword(), SalesChannelsUtil.SALES_CHANNEL_GDS);
				} catch (LoginException e) {
					throw new ModuleException(e, "module.invalid.user");
				}
			}
			auxilliaryBD = (ReservationAuxilliaryBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME,
					ReservationAuxilliaryBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating ReservationAuxilliaryBD :", e);
		}
		return auxilliaryBD;
	}

	/**
	 * Return transparent flight inventory reservation delegate
	 * 
	 * @return
	 */
	public static MessagingServiceBD getMessagingServiceBD() {
		MessagingServiceBD messagingServiceBD = null;
		try {
			if (getWebServicesConfig().isInvokeCredentials()) {
				try {
					ForceLoginInvoker.webserviceLogin(getWebServicesConfig().getEjbAccessUsername(), getWebServicesConfig()
							.getEjbAccessPassword(), SalesChannelsUtil.SALES_CHANNEL_GDS);
				} catch (LoginException e) {
					throw new ModuleException(e, "module.invalid.user");
				}
			}
			messagingServiceBD = (MessagingServiceBD) lookupEJB3Service(MessagingConstants.MODULE_NAME,
					MessagingServiceBD.SERVICE_NAME);
		} catch (Exception e) {
			log.error("Error locating MessagingServiceBD :", e);
		}
		return messagingServiceBD;
	}

	public static CacheManager getCacheManager() {
		return (CacheManager)LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.DEFAULT_CACHE_MANAGER_BEAN);		
	}
}