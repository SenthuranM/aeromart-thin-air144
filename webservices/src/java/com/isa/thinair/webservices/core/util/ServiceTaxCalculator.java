package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;

/**
 * 
 * @author rumesh
 * 
 */
public class ServiceTaxCalculator {

	private AnciAvailabilityRS anciAvailabilityRS;

	private Map<String, List<ExternalChgDTO>> quotedExternalCharges;

	private List<FlightSegmentDTO> flightSegmentDTOs;

	public ServiceTaxCalculator(AnciAvailabilityRS anciAvailabilityRS) {
		this.anciAvailabilityRS = anciAvailabilityRS;
	}

	@SuppressWarnings("unchecked")
	public void calculate() throws ModuleException {
		if (anciAvailabilityRS.isJnTaxApplicable()) {

			Map<String, List<ExternalChgDTO>> quotedExternalCharges = null;
			if (this.getQuotedExternalCharges() != null) {
				quotedExternalCharges = this.getQuotedExternalCharges();
			} else {
				quotedExternalCharges = (Map<String, List<ExternalChgDTO>>) ThreadLocalData
						.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);
			}

			if (quotedExternalCharges != null && !quotedExternalCharges.isEmpty()) {
				// Get tax applicable flight segment
				Collections.sort(flightSegmentDTOs);
				FlightSegmentDTO flightSegmentDTO = flightSegmentDTOs.get(0);

				Collection<EXTERNAL_CHARGES> exChargestypes = new HashSet<EXTERNAL_CHARGES>();
				exChargestypes.add(EXTERNAL_CHARGES.JN_ANCI);
				Map<EXTERNAL_CHARGES, ExternalChgDTO> quotedMap = WebServicesModuleUtils.getReservationBD()
						.getQuotedExternalCharges(exChargestypes, null, null);

				ServiceTaxExtChgDTO quotedChgDTO = (ServiceTaxExtChgDTO) quotedMap.get(EXTERNAL_CHARGES.JN_ANCI);

				for (Entry<String, List<ExternalChgDTO>> entry : quotedExternalCharges.entrySet()) {
					List<ExternalChgDTO> extChgs = entry.getValue();
					if (extChgs != null && !extChgs.isEmpty()) {
						BigDecimal totAnci = AccelAeroCalculator.getDefaultBigDecimalZero();
						for (ExternalChgDTO externalChgDTO : extChgs) {
							totAnci = AccelAeroCalculator.add(totAnci, externalChgDTO.getAmount());
						}

						if (AccelAeroCalculator.isGreaterThan(totAnci, AccelAeroCalculator.getDefaultBigDecimalZero())) {
							if (quotedChgDTO == null) {
								throw new ModuleException("Cannot locate the Anci JN Tax charges details");
							}

							BigDecimal paxServiceTax = AccelAeroCalculator.multiplyDefaultScale(totAnci,
									anciAvailabilityRS.getTaxRatio());

							ServiceTaxExtChgDTO serviceTaxExtChgDTO = (ServiceTaxExtChgDTO) quotedChgDTO.clone();
							serviceTaxExtChgDTO.setAmount(paxServiceTax);
							serviceTaxExtChgDTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentDTO));

							extChgs.add(serviceTaxExtChgDTO);
						}
					}
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void calculateForInterline(List<FlightSegmentTO> flightSegmentTOs) throws ModuleException {
		if (anciAvailabilityRS.isJnTaxApplicable()) {
			Map<String, List<LCCClientExternalChgDTO>> quotedExternalCharges = (Map<String, List<LCCClientExternalChgDTO>>) ThreadLocalData
					.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);
			if (quotedExternalCharges != null && !quotedExternalCharges.isEmpty()) {
				// Get tax applicable flight segment
				Collections.sort(flightSegmentTOs);
				FlightSegmentTO flightSegmentTO = flightSegmentTOs.get(0);

				for (Entry<String, List<LCCClientExternalChgDTO>> entry : quotedExternalCharges.entrySet()) {
					List<LCCClientExternalChgDTO> lccExtChgs = entry.getValue();
					if (lccExtChgs != null && !lccExtChgs.isEmpty()) {
						BigDecimal totAnci = AccelAeroCalculator.getDefaultBigDecimalZero();
						for (LCCClientExternalChgDTO lccExtChg : lccExtChgs) {
							totAnci = AccelAeroCalculator.add(totAnci, lccExtChg.getAmount());
						}

						if (AccelAeroCalculator.isGreaterThan(totAnci, AccelAeroCalculator.getDefaultBigDecimalZero())) {
							BigDecimal paxServiceTax = AccelAeroCalculator.multiplyDefaultScale(totAnci,
									anciAvailabilityRS.getTaxRatio());

							Collection<EXTERNAL_CHARGES> exChargestypes = new HashSet<EXTERNAL_CHARGES>();
							exChargestypes.add(EXTERNAL_CHARGES.JN_ANCI);
							Map<EXTERNAL_CHARGES, ExternalChgDTO> quotedMap = WebServicesModuleUtils.getReservationBD()
									.getQuotedExternalCharges(exChargestypes, null, null);

							ServiceTaxExtChgDTO quotedChgDTO = (ServiceTaxExtChgDTO) quotedMap.get(EXTERNAL_CHARGES.JN_ANCI);

							LCCClientExternalChgDTO serviceTaxDto = new LCCClientExternalChgDTO();
							serviceTaxDto.setExternalCharges(EXTERNAL_CHARGES.JN_ANCI);
							serviceTaxDto.setAmount(paxServiceTax);
							serviceTaxDto.setFlightRefNumber(flightSegmentTO.getFlightRefNumber());
							serviceTaxDto.setCarrierCode(FlightRefNumberUtil.getOperatingAirline(flightSegmentTO
									.getFlightRefNumber()));
							serviceTaxDto.setCode(quotedChgDTO.getChargeCode());
							serviceTaxDto.setUserNote(quotedChgDTO.getChargeDescription());
							lccExtChgs.add(serviceTaxDto);
						}
					}
				}
			}
		}
	}

	public static void calculatePaxJNTax(IPayment iPayment) throws ModuleException {
		
		Object serviceTaxes = ThreadLocalData.getCurrentTnxParam(Transaction.APPLICABLE_SERVICE_TAXES);
		
		if (serviceTaxes!= null) {
			Set<ServiceTaxContainer> serviceTaxContainers = (Set<ServiceTaxContainer>) serviceTaxes;

			if (!serviceTaxContainers.isEmpty()) {
				for (ServiceTaxContainer serviceTaxContainer : serviceTaxContainers) {
					if (serviceTaxContainer != null && serviceTaxContainer.isTaxApplicable()) {
						PaymentAssembler paymentAssembler = (PaymentAssembler) iPayment;
						Set<EXTERNAL_CHARGES> taxableExternalCharges = serviceTaxContainer.getTaxableExternalCharges();
						if (taxableExternalCharges != null && !taxableExternalCharges.isEmpty()) {
							BigDecimal taxableTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
							for (EXTERNAL_CHARGES extChg : taxableExternalCharges) {
								Collection<ExternalChgDTO> externalChgDTOs = paymentAssembler.getPerPaxExternalCharges(extChg);
								if (externalChgDTOs != null && !externalChgDTOs.isEmpty()) {
									for (ExternalChgDTO externalChgDTO : externalChgDTOs) {
										taxableTotal = AccelAeroCalculator.add(taxableTotal, externalChgDTO.getAmount());
									}
								}
							}

							if (AccelAeroCalculator.isGreaterThan(taxableTotal, AccelAeroCalculator.getDefaultBigDecimalZero())) {
								BigDecimal taxAmount = AccelAeroCalculator.multiplyDefaultScale(taxableTotal,
										serviceTaxContainer.getTaxRatio());
								ExternalChgDTO serviceTAxDTO = getJNTaxOther(taxAmount);
								((ServiceTaxExtChgDTO) serviceTAxDTO).setFlightRefNumber(serviceTaxContainer
										.getTaxApplyingFlightRefNumber());
								Collection<ExternalChgDTO> colTaxes = new ArrayList<ExternalChgDTO>();
								colTaxes.add(serviceTAxDTO);
								paymentAssembler.addExternalCharges(colTaxes);
							}
						}
					}
				}
			}

		}
	}

	public static void calculatePaxJNTax(LCCClientPaymentAssembler paxPayment) throws ModuleException {
		
		Object serviceTaxes = ThreadLocalData.getCurrentTnxParam(Transaction.APPLICABLE_SERVICE_TAXES);
		
		if (serviceTaxes != null) {
			Set<ServiceTaxContainer> serviceTaxContainers = (Set<ServiceTaxContainer>) serviceTaxes;

			if (!serviceTaxContainers.isEmpty()) {
				for (ServiceTaxContainer serviceTaxContainer : serviceTaxContainers) {
					if (serviceTaxContainer != null && serviceTaxContainer.isTaxApplicable()) {
						Set<EXTERNAL_CHARGES> taxableExternalCharges = serviceTaxContainer.getTaxableExternalCharges();
						if (taxableExternalCharges != null && !taxableExternalCharges.isEmpty()) {
							BigDecimal taxableTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
							for (EXTERNAL_CHARGES extChg : taxableExternalCharges) {
								Collection<LCCClientExternalChgDTO> externalChgDTOs = paxPayment.getPerPaxExternalCharges();
								if (externalChgDTOs != null && !externalChgDTOs.isEmpty()) {
									for (LCCClientExternalChgDTO externalChgDTO : externalChgDTOs) {
										if (externalChgDTO.getExternalCharges() == extChg) {
											taxableTotal = AccelAeroCalculator.add(taxableTotal, externalChgDTO.getAmount());
										}
									}
								}
							}

							if (AccelAeroCalculator.isGreaterThan(taxableTotal, AccelAeroCalculator.getDefaultBigDecimalZero())) {
								BigDecimal taxAmount = AccelAeroCalculator.multiplyDefaultScale(taxableTotal,
										serviceTaxContainer.getTaxRatio());
								ExternalChgDTO serviceTAxDTO = getJNTaxOther(taxAmount);
								((ServiceTaxExtChgDTO) serviceTAxDTO).setFlightRefNumber(serviceTaxContainer
										.getTaxApplyingFlightRefNumber());
								Collection<ExternalChgDTO> colTaxes = new ArrayList<ExternalChgDTO>();
								colTaxes.add(serviceTAxDTO);
								paxPayment.addExternalCharges(colTaxes);
							}
						}
					}
				}
			}
		}
	}

	private static ExternalChgDTO getJNTaxOther(BigDecimal taxAmount) throws ModuleException {
		Collection colEXTERNAL_CHARGES = new ArrayList();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.JN_OTHER);

		ReservationBD reservationBD = WPModuleUtils.getReservationBD();
		Map externalChargesMap = reservationBD.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, null);

		ExternalChgDTO externalChgDTO = (ExternalChgDTO) externalChargesMap.get(EXTERNAL_CHARGES.JN_OTHER);
		if (externalChgDTO == null) {
			String msg = "Error loading JN Tax Other charge info[null]";
			throw new RuntimeException(msg);
		} else {
			externalChgDTO = (ServiceTaxExtChgDTO) externalChgDTO.clone();
			externalChgDTO.setAmount(taxAmount);
		}
		return externalChgDTO;
	}

	public Map<String, List<ExternalChgDTO>> getQuotedExternalCharges() {
		return quotedExternalCharges;
	}

	public void setQuotedExternalCharges(Map<String, List<ExternalChgDTO>> quotedExternalCharges) {
		this.quotedExternalCharges = quotedExternalCharges;
	}

	public List<FlightSegmentDTO> getFlightSegmentDTOs() {
		return flightSegmentDTOs;
	}

	public void setFlightSegmentDTOs(List<FlightSegmentDTO> flightSegmentDTOs) {
		this.flightSegmentDTOs = flightSegmentDTOs;
	}
}
