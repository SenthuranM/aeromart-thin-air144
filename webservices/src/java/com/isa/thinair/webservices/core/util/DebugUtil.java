package com.isa.thinair.webservices.core.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DebugUtil {

	private static Log logger = LogFactory.getLog(DebugUtil.class);
	
	public static void dumpValues(Log log, Object o) {
		if (o instanceof Collection) {
			for (Iterator i = ((Collection) o).iterator(); i.hasNext();) {
				dumpObject(log, i.next());
				log.info("class: " + o.getClass());
			}
		} else if (o instanceof Map) {
			for (Iterator i = ((Map) o).keySet().iterator(); i.hasNext();) {
				Object key = i.next();
				log.info("key:" + key);
				dumpObject(log, ((Map) o).get(key));
				log.info("class: " + o.getClass());
			}
		} else {
			dumpObject(log, o);
			log.info("class: " + o.getClass());
		}

	}

	private static void dumpObject(Log log, Object o) {
		if (o instanceof String) {
			log.info(o);
			return;
		}
		Method[] m = o.getClass().getMethods();
		for (int x = 0; x < m.length; x++) {
			if (m[x].getName().startsWith("get")
					&& (m[x].getReturnType().isPrimitive() || m[x].getReturnType() == Integer.class || m[x].getReturnType() == String.class)
					&& (m[x].getParameterTypes() == null || m[x].getParameterTypes().length == 0)) {
				try {
					Object value = m[x].invoke(o);
					log.info(m[x].getName() + " = " + value);
					if (value != null && value.getClass().isArray()) {
						if (value.getClass() == double[].class) {
							double[] valueArray = (double[]) value;
							for (int y = 0; y < valueArray.length; y++) {
								log.info("valueArray[" + y + "]=" + valueArray[y]);
							}
						}
					}
				} catch (IllegalArgumentException e) {
					log.error(e);
				} catch (IllegalAccessException e) {
					log.error(e);
				} catch (InvocationTargetException e) {
					log.error(e);
				}
			}
		}
	}

	public static Log getFileLogger(File file) {
		Log log = null;
		try {
			if (file.exists()) {
				file.delete();
				file = file.getAbsoluteFile();
			}
			log = (Log) Proxy.newProxyInstance(DebugUtil.class.getClassLoader(), new Class[] { Log.class },
					new DebugUtil.FileLoggerImpl(file));
		} catch (IllegalArgumentException e) {
			log.error(e);
		} catch (FileNotFoundException e) {
			log.error(e);
		}
		return log;
	}

	public static class FileLoggerImpl implements InvocationHandler {

		private File _file = null;
		private OutputStream _stream = null;

		private FileLoggerImpl(File file) throws FileNotFoundException {
			_file = file;
			_stream = new FileOutputStream(file);
			Runtime.getRuntime().addShutdownHook(new Thread() {
				@Override
				public void run() {
					try {
						_stream.write("closing stream...".getBytes());
						_stream.close();
					} catch (IOException e) {
						logger.error(e);
					}
				}
			});
		}

		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
			Method m = FileLoggerImpl.class.getMethod(method.getName(), new Class[] { Object.class });
			if (m != null) {
				m.invoke(this, args);
				_stream.write("\n".getBytes());
				_stream.flush();
			}
			return null;
		}

		public void debug(Object arg0) {
			try {
				_stream.write(arg0.toString().getBytes());
			} catch (IOException e) {
				logger.error(e);
			}
		}

		public void debug(Object arg0, Throwable arg1) {
			try {
				_stream.write(arg0.toString().getBytes());
			} catch (IOException e) {
				logger.error(e);
			}
		}

		public void error(Object arg0) {
			try {
				_stream.write(arg0.toString().getBytes());
			} catch (IOException e) {
				logger.error(e);
			}
		}

		public void error(Object arg0, Throwable arg1) {
			try {
				_stream.write(arg0.toString().getBytes());
			} catch (IOException e) {
				logger.error(e);
			}
		}

		public void fatal(Object arg0) {
			try {
				_stream.write(arg0.toString().getBytes());
			} catch (IOException e) {
				logger.error(e);
			}
		}

		public void fatal(Object arg0, Throwable arg1) {
			try {
				_stream.write(arg0.toString().getBytes());
			} catch (IOException e) {
				logger.error(e);
			}
		}

		public void info(Object arg0) {
			try {
				_stream.write(arg0.toString().getBytes());
			} catch (IOException e) {
				logger.error(e);
			}
		}

		public void info(Object arg0, Throwable arg1) {
			try {
				_stream.write(arg0.toString().getBytes());
			} catch (IOException e) {
				logger.error(e);
			}
		}

		public boolean isDebugEnabled() {

			return false;
		}

		public boolean isErrorEnabled() {

			return false;
		}

		public boolean isFatalEnabled() {

			return false;
		}

		public boolean isInfoEnabled() {

			return false;
		}

		public boolean isTraceEnabled() {

			return false;
		}

		public boolean isWarnEnabled() {

			return false;
		}

		public void trace(Object arg0) {
			try {
				_stream.write(arg0.toString().getBytes());
			} catch (IOException e) {
				logger.error(e);
			}
		}

		public void trace(Object arg0, Throwable arg1) {
			try {
				_stream.write(arg0.toString().getBytes());
			} catch (IOException e) {
				logger.error(e);
			}
		}

		public void warn(Object arg0) {
			try {
				_stream.write(arg0.toString().getBytes());
			} catch (IOException e) {
				logger.error(e);
			}
		}

		public void warn(Object arg0, Throwable arg1) {
			try {
				_stream.write(arg0.toString().getBytes());
			} catch (IOException e) {
				logger.error(e);
			}
		}

	}
}
