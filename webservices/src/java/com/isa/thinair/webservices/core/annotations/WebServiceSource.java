package com.isa.thinair.webservices.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @author b
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface WebServiceSource {

	public enum MethodSource {
		AA, OTA, FAWRY, AMADEUS, MYIDTRAVEL, LOGIN_API, MEAL_SERVICES
	};

	MethodSource source() default MethodSource.OTA;

}
