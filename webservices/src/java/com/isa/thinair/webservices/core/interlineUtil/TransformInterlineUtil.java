package com.isa.thinair.webservices.core.interlineUtil;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.AAOTAAirTaxInvoiceGetRQ;
import org.opentravel.ota._2003._05.AAOTAAirTaxInvoiceGetRS;
import org.opentravel.ota._2003._05.AirFeeType;
import org.opentravel.ota._2003._05.AirTaxType;
import org.opentravel.ota._2003._05.AirTravelerType;
import org.opentravel.ota._2003._05.AirTravelerType.CustLoyalty;
import org.opentravel.ota._2003._05.AirTravelerType.Document;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.FareType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OTAAirBookRQ;
import org.opentravel.ota._2003._05.OTAAirBookRQ.PriceInfo;
import org.opentravel.ota._2003._05.OTAAirBookRS;
import org.opentravel.ota._2003._05.OTAReadRQ;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.PTCFareBreakdownType;
import org.opentravel.ota._2003._05.PaymentDetailType;
import org.opentravel.ota._2003._05.SourceType;
import org.opentravel.ota._2003._05.TravelersFulfillments;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airmaster.api.model.Nationality;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuranceQuotationDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCSelectedSegmentAncillaryDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.InterlineBlockSeatRQInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxChargesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAdminInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonReservationContactInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientOnAccountPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationInsurance;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.utils.ReleaseTimeUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.BookingCategory;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.webplatform.api.dto.PaxSet;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.webservices.api.dtos.OTAPaxCountTO;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;
import com.isa.thinair.webservices.core.util.CommonServicesUtil;
import com.isa.thinair.webservices.core.util.ExtensionsUtil;
import com.isa.thinair.webservices.core.util.OTAUtils;
import com.isa.thinair.webservices.core.util.ParameterUtil;
import com.isa.thinair.webservices.core.util.PriceQuoteReqParamsExtractUtill;
import com.isa.thinair.webservices.core.util.ServiceTaxCalculator;
import com.isa.thinair.webservices.core.util.ServiceTaxUtil;
import com.isa.thinair.webservices.core.util.WSReservationUtil;
import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;
import com.isa.thinair.webservices.core.util.WebservicesContext;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AAAirBookRQExt;
import com.isaaviation.thinair.webservices.ota.extensions._2003._05.AALoadDataOptionsType;

/**
 * 
 * @author nafly
 * 
 */
public class TransformInterlineUtil {

	private final Log log = LogFactory.getLog(getClass());
	private static final double ERROR_MARGIN = 0.5;

	public static enum TransformTO {
		AirBookRS, ResRetrieveRS
	}

	/**
	 * 
	 * @param otaAirBookRQ
	 * @param aaAirBookRQExt
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	public CommonReservationAssembler createBookRequestParam(OTAAirBookRQ otaAirBookRQ, AAAirBookRQExt aaAirBookRQExt,
			UserPrincipal userPrincipal, Collection<String> privilegeKeys) throws WebservicesException, ModuleException {

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		CommonReservationAssembler lcclientReservationAssembler = new CommonReservationAssembler();

		setContactInfo(aaAirBookRQExt, lcclientReservationAssembler, otaAirBookRQ);

		String bookingCategory = aaAirBookRQExt.getBookingCategoryType().getTypeCode();
		lcclientReservationAssembler.setBookingCategory(bookingCategory);

		OTAPaxCountTO otaPaxCountTO = getPaxCounts(otaAirBookRQ.getTravelerInfo().getAirTraveler());

		PriceQuoteReqParamsExtractUtill priceQuoteReqParamsExtractUtill = new PriceQuoteReqParamsExtractUtill(userPrincipal);

		List<String> obRPHList = new ArrayList<String>();
		List<String> ibRPHList = new ArrayList<String>();

		QuotedPriceInfoTO quotedPriceInfoTO = (QuotedPriceInfoTO) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_PRICE_INFO);

		if (quotedPriceInfoTO == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
					"Quoted price no longer available,Please search again");
		}

		FareSegChargeTO fareSegChargeTO = (FareSegChargeTO) ThreadLocalData
				.getCurrentTnxParam(Transaction.FARE_SEGMENT_CHARGE_TO);
		List<BundledFareDTO> ondBundledFareDTOs = null;
		if (fareSegChargeTO != null) {
			ondBundledFareDTOs = fareSegChargeTO.getOndBundledFareDTOs();
		}

		if (BundleServiceInterlineUtil.isValideSelectedBundleFare(ondBundledFareDTOs)) {
			BundleServiceInterlineUtil.validateSelectedBundleServices();
		}

		Map<Integer, String> defaultBaggageOndMap = getDefaultOndBaggageGroupMap(quotedPriceInfoTO);

		FlightPriceRQ flightPriceRQ = priceQuoteReqParamsExtractUtill.extractPriceQuoteReq(otaAirBookRQ.getAirItinerary(),
				otaPaxCountTO, obRPHList, ibRPHList, lcclientReservationAssembler, quotedPriceInfoTO, defaultBaggageOndMap);

		// Collection<OndFareDTO> quotedFaresSegmentsSeatsInfo = null;
		// try {
		// quotedFaresSegmentsSeatsInfo = priceQuoteReqParamsExtractUtill
		// .getSelectedSegsFaresSeatsColForBookingFromFareSegChargeTO(flightPriceRQ, lcclientReservationAssembler);
		// } catch (ModuleException ex) {
		// // If inventory is exhausted between the price quote and the book request, a meaningful error message
		// // should be
		// // propagated. User understands exhaustion of booking classes only by the price difference, therefore
		// // error message
		// // is shown as below
		// if (ex.getExceptionCode().equalsIgnoreCase("airinventory.booking.class.inventory.unavilable")) {
		// throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
		// "Quoted price no longer available,Please search again");
		// } else {
		// throw ex;
		// }
		// }

		priceQuoteReqParamsExtractUtill.getFareSegChargeFromQuotedIno(flightPriceRQ, lcclientReservationAssembler);

		flightPriceRQ.getTravelPreferences().setBookingType(quotedPriceInfoTO.getBookingType());

		String currencyCode = null;

		if (OTAUtils.isShowPriceInselectedCurrency()) {
			OTAAirBookRQ.Fulfillment fulfillment = otaAirBookRQ.getFulfillment();
			if (fulfillment != null && fulfillment.getPaymentDetails() != null) {
				List<PaymentDetailType> paymentDetails = fulfillment.getPaymentDetails().getPaymentDetail();
				for (PaymentDetailType paymentDetail : paymentDetails) {
					currencyCode = paymentDetail.getPaymentAmount().getCurrencyCode();
					break;
				}
			}
		}

		if (currencyCode == null) {
			currencyCode = AppSysParamsUtil.getBaseCurrency();
		}

		lcclientReservationAssembler.setFlightPriceRQ(flightPriceRQ);

		HashMap<String, Object[]> paxRelationships = preparePaxRelationships(otaAirBookRQ.getTravelerInfo().getAirTraveler());

		// calculate handling fee
		Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargeMap = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();
		ExternalChgDTO handlingFee = ReservationUtil.getHangleCharges();
		if (handlingFee != null) {
			PaxSet paxSet = new PaxSet(otaPaxCountTO.getAdultCount(), otaPaxCountTO.getChildCount(),
					otaPaxCountTO.getInfantCount());

			ExternalChargeUtil.calculateExternalChargeAmount(quotedPriceInfoTO, paxSet, handlingFee);
			externalChargeMap.put(EXTERNAL_CHARGES.HANDLING_CHARGE, handlingFee);
		}

		HashMap<String, LCCClientPaymentAssembler> travelersPayments = null;

		// get the par pax wise handling fee for the agent.
		travelersPayments = getHandlerFee(otaAirBookRQ.getTravelerInfo().getAirTraveler(), otaPaxCountTO, handlingFee);

		Date holdReleaseTimestamp = getHoldReleaseTimeStamp(otaAirBookRQ, otaPaxCountTO, quotedPriceInfoTO, currencyCode,
				priceQuoteReqParamsExtractUtill, aaAirBookRQExt, userPrincipal, travelersPayments, externalChargeMap,
				paxRelationships, lcclientReservationAssembler, exchangeRateProxy);

		lcclientReservationAssembler.setPnrZuluReleaseTimeStamp(holdReleaseTimestamp);

		addPassengerDetails(otaAirBookRQ, userPrincipal, lcclientReservationAssembler, quotedPriceInfoTO, paxRelationships,
				aaAirBookRQExt, travelersPayments);

		lcclientReservationAssembler.setChargesApplicability(ChargeRateOperationType.MAKE_ONLY);

		setAdminInfo(lcclientReservationAssembler);

		// set insurance info- If price quote with INSURANCE by sending insurance RPH in getPriceQuoteREQ
		LCCInsuranceQuotationDTO lccInsuranceQuotationDTO = (LCCInsuranceQuotationDTO) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_INSURANCE);
		// add Insurance if priceQuote with Insurance
		if (ThreadLocalData.getCurrentTnxParam(Transaction.RES_INSURANCE_SELECTED) != null) {
			populateInsurance(lccInsuranceQuotationDTO, lcclientReservationAssembler);
		}

		lcclientReservationAssembler.setTargetSystem(flightPriceRQ.getAvailPreferences().getSearchSystem());

		// Authorize payments
		if (!lcclientReservationAssembler.isOnHoldBooking()) {
			for (LCCClientReservationPax pax : lcclientReservationAssembler.getLccreservation().getPassengers()) {
				WSReservationUtil.authorizePayment(privilegeKeys, pax.getLccClientPaymentAssembler(),
						userPrincipal.getAgentCode());
			}
		}

		if (lcclientReservationAssembler.isOnHoldBooking()
				&& !AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_OVERRIDE_ONHOLD)) {
			if (quotedPriceInfoTO != null) {
				if (!quotedPriceInfoTO.isOnholdAllowed()) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
							"Hold not allowed. Need to be ticketed immediately");
				}
			}
		}

		// AARESAA-14740 : Check duplicate name priviledge and skip it
		if (AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_DUPLI_NAME_SKIP)) {
			lcclientReservationAssembler.setSkipDuplicateCheck(true);
		}

		// block seats
		if (!SYSTEM.INT.equals(lcclientReservationAssembler.getTargetSystem())) {
			Collection blockSeatIds = WebServicesModuleUtils.getAirproxySearchAndQuoteBD().priviledgeUserBlockSeat(flightPriceRQ,
					lcclientReservationAssembler.getSelectedFareSegChargeTO());

			if (blockSeatIds != null && !blockSeatIds.isEmpty()) {

				lcclientReservationAssembler.getSelectedFareSegChargeTO().setBlockSeatIds(blockSeatIds);
			}
			if (log.isDebugEnabled()) {
				log.debug("BlockSeatUID generated [blockSeatUID = " + blockSeatIds + "]");
			}
		} else if (lcclientReservationAssembler.getTargetSystem().equals(SYSTEM.INT)) {

			String trxID = otaAirBookRQ.getTransactionIdentifier();

			InterlineBlockSeatRQInfoTO infoTO = new InterlineBlockSeatRQInfoTO();
			infoTO.setOutFlightRPHList(obRPHList);
			infoTO.setRetFlightRPHList(ibRPHList);
			infoTO.setTransactionIdentifier(trxID);

			boolean isSuccess = WebServicesModuleUtils.getLCCSearchAndQuoteBD().priviledgeUserBlockSeat(infoTO,
					CommonServicesUtil.getBasicTrackInfo(userPrincipal));
			if (!isSuccess) {
				throw new WebservicesException(IOTACodeTables.SeatAvailability_SAV_SEAT_BLOCKED_FOR_OTHER_REASON,
						"Block seat failed, search again");
			}
		}

		return lcclientReservationAssembler;
	}

	@SuppressWarnings("unchecked")
	private static Map<Integer, String> getDefaultOndBaggageGroupMap(QuotedPriceInfoTO quotedPriceInfoTO) {

		Map<Integer, String> baggageMap = new HashMap<Integer, String>();

		if (AppSysParamsUtil.isONDBaggaeEnabled()) {
			Map<String, List<? extends LCCClientExternalChgDTO>> quotedExternalCharges = (Map<String, List<? extends LCCClientExternalChgDTO>>) ThreadLocalData
					.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);
			boolean isBaggageSelected = false;

			if (quotedExternalCharges != null && !quotedExternalCharges.isEmpty()) {
				for (String pax : quotedExternalCharges.keySet()) {

					List<LCCClientExternalChgDTO> externalCharges = (List<LCCClientExternalChgDTO>) quotedExternalCharges
							.get(pax);

					if (externalCharges != null && !externalCharges.isEmpty()) {

						for (LCCClientExternalChgDTO externalCharge : externalCharges) {
							if (EXTERNAL_CHARGES.BAGGAGE.equals(externalCharge.getExternalCharges())) {
								isBaggageSelected = true;
								break;
							}
						}

					}
				}
			}

			if (!isBaggageSelected) {

				List<FlightSegmentTO> quotedSegments = quotedPriceInfoTO.getQuotedSegments();
				FlightSegmentTO firstSegment = quotedSegments.get(0);
				int lastOndSequence = firstSegment.getOndSequence();

				// TODO: When LCC enabled baggage ondgroup id has to be taken by a LCC call replacing this
				String lastBaggageOndGroupId = new Integer(WebServicesModuleUtils.getBaggageBD().getNextBaggageOndGroupId())
						.toString();
				baggageMap.put(firstSegment.getFlightSegId(), lastBaggageOndGroupId);

				for (int i = 1; i < quotedSegments.size(); i++) {
					if (lastOndSequence != quotedSegments.get(i).getOndSequence()) {
						lastBaggageOndGroupId = new Integer(WebServicesModuleUtils.getBaggageBD().getNextBaggageOndGroupId())
								.toString();
					}
					baggageMap.put(quotedSegments.get(i).getFlightSegId(), lastBaggageOndGroupId);
					lastOndSequence = quotedSegments.get(i).getOndSequence();
				}
			}
		}
		return baggageMap;
	}

	public static void populateInsurance(LCCInsuranceQuotationDTO insuranceQuote,
			CommonReservationAssembler commonReservationAssembler) {
		if (insuranceQuote == null) {
			commonReservationAssembler.getLccreservation().setReservationInsurances(null);
		} else {

			List<LCCClientReservationInsurance> reservationInsurances = new ArrayList<LCCClientReservationInsurance>();

			LCCClientReservationInsurance reservationInsurance = new LCCClientReservationInsurance();
			reservationInsurance.setInsuranceQuoteRefNumber(insuranceQuote.getInsuranceRefNumber());
			reservationInsurance.setQuotedTotalPremium(insuranceQuote.getQuotedTotalPremiumAmount());
			reservationInsurance.setSsrFeeCode(insuranceQuote.getSsrFeeCode());
			reservationInsurance.setPlanCode(insuranceQuote.getPlanCode());
			if (insuranceQuote.getInsuredJourney() != null) {
				reservationInsurance.setOrigin(insuranceQuote.getInsuredJourney().getJourneyStartAirportCode());
				reservationInsurance.setDestination(insuranceQuote.getInsuredJourney().getJourneyEndAirportCode());
				reservationInsurance.setDateOfTravel(insuranceQuote.getInsuredJourney().getJourneyStartDate());
				reservationInsurance.setDateOfReturn(insuranceQuote.getInsuredJourney().getJourneyEndDate());
				reservationInsurance.setInsuredJourneyDTO(insuranceQuote.getInsuredJourney());
			}

			reservationInsurance.setOperatingAirline(insuranceQuote.getOperatingAirline());
			reservationInsurance.setInsuranceType(insuranceQuote.getInsuranceType());

			reservationInsurances.add(reservationInsurance);

			commonReservationAssembler.getLccreservation().setReservationInsurances(reservationInsurances);
		}
	}

	/**
	 * 
	 * @param commonReservationAssembler
	 */
	private void setAdminInfo(CommonReservationAssembler commonReservationAssembler) {

		// Set reservation admin info
		SourceType pos = (SourceType) ThreadLocalData.getCurrentWSContext()
				.getParameter(WebservicesContext.REQUEST_POS);
		CommonReservationAdminInfo adminInfo = new CommonReservationAdminInfo();
		adminInfo.setOriginSalesTerminal(pos.getTerminalID());
		adminInfo.setLastSalesTerminal(pos.getTerminalID());
		commonReservationAssembler.getLccreservation().setAdminInfo(adminInfo);

		commonReservationAssembler.setAppIndicator(AppIndicatorEnum.APP_WS);

	}

	/**
	 * 
	 * @param otaAirBookRQ
	 * @param userPrincipal
	 * @param commonReservationAssembler
	 * @param quotedFaresSegmentsSeatsInfo
	 * @param paxRelationships
	 * @param aaAirBookRQExt
	 * @param travelersPayments
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	private void addPassengerDetails(OTAAirBookRQ otaAirBookRQ, UserPrincipal userPrincipal,
			CommonReservationAssembler commonReservationAssembler, QuotedPriceInfoTO quotedPriceInfoTO,
			HashMap<String, Object[]> paxRelationships, AAAirBookRQExt aaAirBookRQExt,
			HashMap<String, LCCClientPaymentAssembler> travelersPayments) throws WebservicesException, ModuleException {

		Integer nationalityCode = null;
		LCCClientPaymentAssembler payment = null;
		Object[] currentPaxSequences;
		String foidNumber = null;
		String passportIssuedPlace = null;
		Date passportExpiryDate = null;
		String employeeId = "";
		Date dateOfJoin = null;
		String idCategory = "";
		String bookingCategory = aaAirBookRQExt.getBookingCategoryType().getTypeCode();
		boolean travellerPaymentsExist;
		String nationalIDNo = "";
		List<String> airportCodes = new ArrayList<String>();
		
		Collection<String> privilegesKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
		boolean isOverrideCountryConfigs = privilegesKeys
				.contains(PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_OVERRIDE_COUNTRY_PAX_CONFIG);
		if (!isOverrideCountryConfigs) {
			for (OriginDestinationOptionType ond : otaAirBookRQ.getAirItinerary().getOriginDestinationOptions()
					.getOriginDestinationOption()) {
				for (BookFlightSegmentType fltSeg : ond.getFlightSegment()) {
					airportCodes.add(fltSeg.getArrivalAirport().getLocationCode());
					airportCodes.add(fltSeg.getDepartureAirport().getLocationCode());
				}
			}
		}

		ArrayList<String> passportNumbers = new ArrayList<String>();
		ArrayList<String> nationalIDList = new ArrayList<String>();

		boolean skipValidation = AppSysParamsUtil.isWebServiceExtaraValidationSkipCarrier(userPrincipal.getAgentCode());
		AirTravelerType firstAdultPax = getFirstAdultPax(otaAirBookRQ.getTravelerInfo().getAirTraveler());

		// Map contains both In-flight services & Airport Services
		Map<String, List<LCCSelectedSegmentAncillaryDTO>> paxWiseSpecialServiceMap = SSRDetailsInterlineUtil
				.processPaxWiseSpecialServices(otaAirBookRQ.getTravelerInfo());

		// Set segment seq LCCSelectedSegmentAncillaryDTO by flight segment info
		setSegmentSequenceForSpecialServices(paxWiseSpecialServiceMap, quotedPriceInfoTO.getQuotedSegments());

		for (AirTravelerType traveler : otaAirBookRQ.getTravelerInfo().getAirTraveler()) {

			currentPaxSequences = paxRelationships.get(traveler.getTravelerRefNumber().getRPH());

			// pax nationalitiy
			nationalityCode = null;
			Nationality nationality = null;
			foidNumber = null;

			// Supports only PSPT and ID
			if (!traveler.getDocument().isEmpty()) {
				for (Document document : traveler.getDocument()) {
					if (CommonsConstants.SSRCode.PSPT.toString().equals(document.getDocType())) {
						if (document.getDocHolderNationality() != null) {
							nationality = WebServicesModuleUtils.getCommonMasterBD().getNationality(
									document.getDocHolderNationality());
						}

						passportIssuedPlace = WebServicesModuleUtils.getCommonMasterBD().getNationality(
								document.getDocIssueCountry()) == null ? null : document.getDocIssueCountry();

						passportExpiryDate = document.getExpireDate() == null ? null : document.getExpireDate()
								.toGregorianCalendar().getTime();

						if (document.getDocID() != null && document.getDocType() != null) {
							foidNumber = document.getDocID();
						}

					} else if (CommonsConstants.SSRCode.ID.toString().equals(document.getDocType())
							&& BookingCategory.HR.getCode().equals(bookingCategory)) {
						if (document.getDocID() != null && document.getDocType() != null) {
							employeeId = document.getDocID();
							dateOfJoin = document.getEffectiveDate() != null ? document.getEffectiveDate().toGregorianCalendar()
									.getTime() : null;
							idCategory = document.getDocIssueAuthority();

						}
					}

					if (nationality == null) {
						if (document.getDocHolderNationality() != null) {
							nationality = WebServicesModuleUtils.getCommonMasterBD().getNationality(
									document.getDocHolderNationality());
						}
					}
				}
			}

			if (nationality != null) {
				nationalityCode = nationality.getNationalityCode();
			} else {// only enforce the nationality code validation for agents with extra validation
				if (!skipValidation) {
					// nationality is only required for the first adult.
					if (firstAdultPax.getTravelerRefNumber().getRPH().equals(traveler.getTravelerRefNumber().getRPH())) {
						throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "NationalityCode not present");
					}
				}
			}

			// fall back to the previous way of obtaining the foidNumber & paxFOIDId
			if (foidNumber == null) {
				if (aaAirBookRQExt.getTravelerAdditionalInfo() != null) {
					foidNumber = OTAUtils.getFOIDNumber(aaAirBookRQExt.getTravelerAdditionalInfo().getAAAirTravelersType(),
							traveler.getTravelerRefNumber().getRPH());
				}
			}
			if (aaAirBookRQExt.getTravelerAdditionalInfo() != null) {
				nationalIDNo = OTAUtils.getNationalIDNo(aaAirBookRQExt.getTravelerAdditionalInfo().getAAAirTravelersType(),
						traveler.getTravelerRefNumber().getRPH());
			}

			boolean isDomesticSegmentExist = OTAUtils.isDomesticSegmentExists(quotedPriceInfoTO.getQuotedSegments());
			// Holds In-flight Services & Airport Services requested by User
			List<LCCSelectedSegmentAncillaryDTO> specialServiceList = new ArrayList<LCCSelectedSegmentAncillaryDTO>();

			if (paxWiseSpecialServiceMap != null) {
				specialServiceList = paxWiseSpecialServiceMap.get(traveler.getTravelerRefNumber().getRPH());
			}

			String paxFfid = null;
			// Capture loyalty information
			if (AppSysParamsUtil.isLMSEnabled() && !traveler.getCustLoyalty().isEmpty()) {
				List<CustLoyalty> custLoyalty = traveler.getCustLoyalty();
				paxFfid = custLoyalty.iterator().next().getMembershipID();
			}

			PaxAdditionalInfoDTO paxAdditionalInfoDTO = new PaxAdditionalInfoDTO();
			paxAdditionalInfoDTO.setPassportNo(CommonUtil.removeNonAlphaNumericCharactersFromString(foidNumber));
			paxAdditionalInfoDTO.setPassportExpiry(passportExpiryDate);
			paxAdditionalInfoDTO.setPassportIssuedCntry(passportIssuedPlace);
			paxAdditionalInfoDTO.setEmployeeId(employeeId);
			paxAdditionalInfoDTO.setDateOfJoin(dateOfJoin);
			paxAdditionalInfoDTO.setIdCategory(idCategory);
			paxAdditionalInfoDTO.setNationalIDNo(nationalIDNo);
			paxAdditionalInfoDTO.setFfid(paxFfid);

			// If it's an adult
			if (traveler.getPassengerTypeCode()
					.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {

				travellerPaymentsExist = (travelersPayments != null)
						&& travelersPayments.get(traveler.getTravelerRefNumber().getRPH()).isPaymentExist();

				if (travellerPaymentsExist) {
					payment = travelersPayments.get(traveler.getTravelerRefNumber().getRPH());
				} else {
					if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
							&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.WS, userPrincipal.getAgentCode())) {

						LCCClientPaymentAssembler tempPayment = travelersPayments.get(traveler.getTravelerRefNumber().getRPH());
						payment = new LCCClientPaymentAssembler();// On Hold booking.
						payment.addAllExternalCharges(tempPayment.getPerPaxExternalCharges(EXTERNAL_CHARGES.CREDIT_CARD));

						if (tempPayment.getPerPaxExternalCharges(EXTERNAL_CHARGES.JN_OTHER) != null) {
							payment.addAllExternalCharges(tempPayment.getPerPaxExternalCharges(EXTERNAL_CHARGES.JN_OTHER));
						}
					} else {
						payment = new LCCClientPaymentAssembler();// On Hold booking.
					}

					// adds any selected external charges - etc : flexi/seat/meal for the on-hold booking.
					BookInterlineUtil.addSelectedExternalCharges(payment, traveler.getTravelerRefNumber().getRPH());
					ServiceTaxCalculator.calculatePaxJNTax(payment);
				}

				if ((Integer) currentPaxSequences[1] == -1) {// adult traveler

					Date dob = traveler.getBirthDate() != null ? traveler.getBirthDate().toGregorianCalendar().getTime() : null; // birth
																																	// date

					// Adult validation
					OTAUtils.validatePaxDetails(CommonUtil.removeNonLetterCharactersFromString(CommonUtil
							.getValueFromNullSafeList(traveler.getPersonName().getNameTitle(), 0)), CommonUtil
							.removeNonLetterCharactersFromString(CommonUtil.getValueFromNullSafeList(traveler.getPersonName()
									.getGivenName(), 0)), CommonUtil.removeNonLetterCharactersFromString(traveler.getPersonName()
							.getSurname()), dob, nationalityCode, null, foidNumber, passportIssuedPlace, passportExpiryDate,
							PaxTypeTO.ADULT, false, skipValidation, employeeId, dateOfJoin, idCategory, bookingCategory,
							passportNumbers, paxFfid, isDomesticSegmentExist, nationalIDList, nationalIDNo, airportCodes);

					passportNumbers.add(CommonUtil.removeNonAlphaNumericCharactersFromString(foidNumber));
					nationalIDList.add(nationalIDNo);

					validateAge(dob, PaxTypeTO.ADULT, otaAirBookRQ);

					commonReservationAssembler.addSingle(
							CommonUtil.removeNonLetterCharactersFromString(CommonUtil.getValueFromNullSafeList(traveler
									.getPersonName().getGivenName(), 0)), // first
							// name
							CommonUtil.removeNonLetterCharactersFromString(traveler.getPersonName().getSurname()), // last
																													// name
							CommonUtil.removeNonLetterCharactersFromString(CommonUtil.getValueFromNullSafeList(traveler
									.getPersonName().getNameTitle(), 0)), // title
							dob, // birth date
							nationalityCode, // nationality
							(Integer) currentPaxSequences[0], // pax sequence
							traveler.getTravelerRefNumber().getRPH(), paxAdditionalInfoDTO, null, payment, specialServiceList,
							null, null, null, null, null, null, null, null, null);

				} else {// parent (adult with an infant) traveler

					Date dob = traveler.getBirthDate() != null ? traveler.getBirthDate().toGregorianCalendar().getTime() : null; // birth
																																	// date

					OTAUtils.validatePaxDetails(CommonUtil.removeNonLetterCharactersFromString(CommonUtil
							.getValueFromNullSafeList(traveler.getPersonName().getNameTitle(), 0)), CommonUtil
							.removeNonLetterCharactersFromString(CommonUtil.getValueFromNullSafeList(traveler.getPersonName()
									.getGivenName(), 0)), CommonUtil.removeNonLetterCharactersFromString(traveler.getPersonName()
							.getSurname()), dob, nationalityCode, null, foidNumber, passportIssuedPlace, passportExpiryDate,
							PaxTypeTO.ADULT, false, skipValidation, employeeId, dateOfJoin, idCategory, bookingCategory,
							passportNumbers, paxFfid, isDomesticSegmentExist, nationalIDList, nationalIDNo, airportCodes);

					validateAge(dob, PaxTypeTO.ADULT, otaAirBookRQ);

					passportNumbers.add(CommonUtil.removeNonAlphaNumericCharactersFromString(foidNumber));
					nationalIDList.add(nationalIDNo);

					commonReservationAssembler.addParent(CommonUtil.removeNonLetterCharactersFromString(CommonUtil
							.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0)), // first
							// name
							CommonUtil.removeNonLetterCharactersFromString(traveler.getPersonName().getSurname()), // last
																													// name
							CommonUtil.removeNonLetterCharactersFromString(CommonUtil.getValueFromNullSafeList(traveler
									.getPersonName().getNameTitle(), 0)), // title
							dob, // birth date
							nationalityCode, // nationality
							(Integer) currentPaxSequences[0], // pax sequence
							(Integer) currentPaxSequences[1],// infant sequece
							paxAdditionalInfoDTO, null, payment, specialServiceList, null);
				}

				// If it's a child
			} else if (traveler.getPassengerTypeCode().equals(
					CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {

				travellerPaymentsExist = (travelersPayments != null)
						&& travelersPayments.get(traveler.getTravelerRefNumber().getRPH()).isPaymentExist();

				if (travellerPaymentsExist) {
					payment = travelersPayments.get(traveler.getTravelerRefNumber().getRPH());
				} else {

					if (AppSysParamsUtil.isAdminFeeRegulationEnabled()) {
						LCCClientPaymentAssembler tempPayment = travelersPayments.get(traveler.getTravelerRefNumber().getRPH());
						payment = new LCCClientPaymentAssembler();// On Hold booking.
						payment.addAllExternalCharges(tempPayment.getPerPaxExternalCharges(EXTERNAL_CHARGES.CREDIT_CARD));

						if (tempPayment.getPerPaxExternalCharges(EXTERNAL_CHARGES.JN_OTHER) != null) {
							payment.addAllExternalCharges(tempPayment.getPerPaxExternalCharges(EXTERNAL_CHARGES.JN_OTHER));
						}
					} else {
						payment = new LCCClientPaymentAssembler();// 0 payment
					}

					// adds any selected external charges - etc : flexi/seat/meal for the on-hold booking.
					BookInterlineUtil.addSelectedExternalCharges(payment, traveler.getTravelerRefNumber().getRPH());
					ServiceTaxCalculator.calculatePaxJNTax(payment);
				}

				Date dob = traveler.getBirthDate() != null ? traveler.getBirthDate().toGregorianCalendar().getTime() : null; // birth
																																// date

				// Child validation
				OTAUtils.validatePaxDetails(CommonUtil.removeNonLetterCharactersFromString(CommonUtil.getValueFromNullSafeList(
						traveler.getPersonName().getNameTitle(), 0)), CommonUtil.removeNonLetterCharactersFromString(CommonUtil
						.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0)), CommonUtil
						.removeNonLetterCharactersFromString(traveler.getPersonName().getSurname()), dob, nationalityCode, null,
						foidNumber, passportIssuedPlace, passportExpiryDate, PaxTypeTO.CHILD, false, skipValidation, employeeId,
						dateOfJoin, idCategory, bookingCategory, null, paxFfid, isDomesticSegmentExist, nationalIDList,
						nationalIDNo, airportCodes);

				validateAge(dob, PaxTypeTO.CHILD, otaAirBookRQ);
				nationalIDList.add(nationalIDNo);

				commonReservationAssembler.addChild(
						CommonUtil.removeNonLetterCharactersFromString(CommonUtil.getValueFromNullSafeList(traveler
								.getPersonName().getGivenName(), 0)), // first
						// name
						CommonUtil.removeNonLetterCharactersFromString(traveler.getPersonName().getSurname()), // last
																												// name
						CommonUtil.removeNonLetterCharactersFromString(CommonUtil.getValueFromNullSafeList(traveler
								.getPersonName().getNameTitle(), 0)), // title
						dob, // birth date
						nationalityCode, // nationality
						(Integer) currentPaxSequences[0], // pax sequence
						traveler.getTravelerRefNumber().getRPH(), paxAdditionalInfoDTO, null, payment, specialServiceList, null,
						null, null, null, null, null, null, null, null);

				// If it's an infant
			} else if (traveler.getPassengerTypeCode().equals(
					CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {// infant
				// traveler

				travellerPaymentsExist = (travelersPayments != null)
						&& travelersPayments.get(traveler.getTravelerRefNumber().getRPH()).isPaymentExist();

				if (travellerPaymentsExist) {
					payment = travelersPayments.get(traveler.getTravelerRefNumber().getRPH());
				} else {
					payment = new LCCClientPaymentAssembler();// 0 payment
				}

				Date dob = traveler.getBirthDate() != null ? traveler.getBirthDate().toGregorianCalendar().getTime() : null; // birth
				// date

				// Infant validation
				OTAUtils.validatePaxDetails(CommonUtil.removeNonLetterCharactersFromString(CommonUtil.getValueFromNullSafeList(
						traveler.getPersonName().getNameTitle(), 0)), CommonUtil.removeNonLetterCharactersFromString(CommonUtil
						.getValueFromNullSafeList(traveler.getPersonName().getGivenName(), 0)), CommonUtil
						.removeNonLetterCharactersFromString(traveler.getPersonName().getSurname()), dob, nationalityCode,
						(Integer) currentPaxSequences[1], foidNumber, passportIssuedPlace, passportExpiryDate, PaxTypeTO.INFANT,
						false, skipValidation, employeeId, dateOfJoin, idCategory, bookingCategory, null, paxFfid,
						isDomesticSegmentExist, nationalIDList, nationalIDNo, airportCodes);

				validateAge(dob, PaxTypeTO.INFANT, otaAirBookRQ);
				nationalIDList.add(nationalIDNo);

				commonReservationAssembler.addInfant(
						CommonUtil.removeNonLetterCharactersFromString(CommonUtil.getValueFromNullSafeList(traveler
								.getPersonName().getGivenName(), 0)), // first
						// name
						CommonUtil.removeNonLetterCharactersFromString(traveler.getPersonName().getSurname()), // last
																												// name
						CommonUtil.removeNonLetterCharactersFromString(CommonUtil.getValueFromNullSafeList(traveler
								.getPersonName().getNameTitle(), 0)), // title
						dob, // birth date
						nationalityCode, // nationality
						(Integer) currentPaxSequences[0], // pax sequence
						(Integer) currentPaxSequences[1], // parent sequence
						traveler.getTravelerRefNumber().getRPH(), paxAdditionalInfoDTO, null, specialServiceList, null, null,
						null, null, null, null, null, null, payment);
			}
		}

	}

	/**
	 * 
	 * @param paxWiseAirportServiceMap
	 * @param colOndFareDTOs
	 */
	private void setSegmentSequenceForSpecialServices(Map<String, List<LCCSelectedSegmentAncillaryDTO>> paxWiseAirportServiceMap,
			List<FlightSegmentTO> flightSegmentTOs) {

		if (flightSegmentTOs != null) {

			Collections.sort(flightSegmentTOs);

			for (int i = 0; i < flightSegmentTOs.size(); i++) {

				for (Entry<String, List<LCCSelectedSegmentAncillaryDTO>> entry : paxWiseAirportServiceMap.entrySet()) {
					List<LCCSelectedSegmentAncillaryDTO> customDTOList = entry.getValue();

					if (customDTOList != null) {
						for (LCCSelectedSegmentAncillaryDTO ssrCustomDTO : customDTOList) {
							String flightRPH = ssrCustomDTO.getFlightSegmentTO().getFlightRefNumber();
							if (flightSegmentTOs.get(i).getFlightSegId()
									.equals(FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightRPH))) {
								ssrCustomDTO.getFlightSegmentTO().setSegmentSequence(i + 1);
							}
						}
					}
				}

			}

		}
	}

	/**
	 * 
	 * @param aaAirBookRQExt
	 * @param commonReservationAssembler
	 * @param otaAirBookRQ
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	private void setContactInfo(AAAirBookRQExt aaAirBookRQExt, CommonReservationAssembler commonReservationAssembler,
			OTAAirBookRQ otaAirBookRQ) throws WebservicesException, ModuleException {
		CommonReservationContactInfo contactInfo = null;

		String userNotes = null;

		// Extract the Contact information from the request
		if (aaAirBookRQExt.getContactInfo() != null) {
			contactInfo = ExtensionsUtil.getAAContactInfoFromWSRequest(aaAirBookRQExt.getContactInfo());
		}

		// Extract the user notes from the request
		if (aaAirBookRQExt.getUserNotes() != null) {
			userNotes = ExtensionsUtil.transformToAAUserNote(aaAirBookRQExt.getUserNotes());
		}

		// If contact information not found,use the first adult information as contact info
		if (contactInfo == null) {
			contactInfo = prepareContactInfoTravelersInfo(otaAirBookRQ.getTravelerInfo().getAirTraveler());
		}

		if (contactInfo != null) {
			commonReservationAssembler.addContactInfo(userNotes, contactInfo, null);
		}
	}

	/**
	 * 
	 * @param otaAirBookRQ
	 * @param otaPaxCountTO
	 * @param quotedPriceInfoTO
	 * @param currencyCode
	 * @param priceQuoteReqParamsExtractUtill
	 * @param aaAirBookRQExt
	 * @param userPrincipal
	 * @param travelersPayments
	 * @param externalChargeMap
	 * @param paxRelationships
	 * @param lcclientReservationAssembler
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	private Date getHoldReleaseTimeStamp(OTAAirBookRQ otaAirBookRQ, OTAPaxCountTO otaPaxCountTO,
			QuotedPriceInfoTO quotedPriceInfoTO, String currencyCode,
			PriceQuoteReqParamsExtractUtill priceQuoteReqParamsExtractUtill, AAAirBookRQExt aaAirBookRQExt,
			UserPrincipal userPrincipal, HashMap<String, LCCClientPaymentAssembler> travelersPayments,
			Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargeMap, HashMap<String, Object[]> paxRelationships,
			CommonReservationAssembler lcclientReservationAssembler, ExchangeRateProxy exchangeRateProxy)
			throws WebservicesException, ModuleException {
		BigDecimal totalPayment = BigDecimal.ZERO;
		Date holdReleaseTimestamp = null;

		int[] paxTypeQuantities = { otaPaxCountTO.getAdultCount(), otaPaxCountTO.getInfantCount(), otaPaxCountTO.getChildCount() };

		BigDecimal totalFareFromPriceBreakdown = priceQuoteReqParamsExtractUtill.getTotalAmountFromPriceBreakdown(otaPaxCountTO,
				currencyCode, quotedPriceInfoTO, externalChargeMap, exchangeRateProxy);

		// adding selected external charges
		totalFareFromPriceBreakdown = AccelAeroCalculator.add(totalFareFromPriceBreakdown,
				BookInterlineUtil.getTotalAmountOfSelectedExternalCharges(currencyCode, exchangeRateProxy));

		boolean isHoldBooking = false;
		boolean isPartialPayment = false;
		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

		if (otaAirBookRQ.getFulfillment() == null || otaAirBookRQ.getFulfillment().getPaymentDetails() == null) {

			
			if(AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.MakeResPrivilegesKeys.RES_MAKE_ONHOLD)){
				isHoldBooking = true;
			}else{
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE,
						"Hold not allowed. Need to be ticketed immediately");

			}
			
			if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
					&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.WS, userPrincipal.getAgentCode())) {

				this.addCCCharge(totalFareFromPriceBreakdown, quotedPriceInfoTO, paxTypeQuantities, otaAirBookRQ,
						travelersPayments, currencyCode, exchangeRateProxy);
			}

		} else {
			travelersPayments = getTravlersPayments(totalFareFromPriceBreakdown, paxRelationships, travelersPayments,
					paxTypeQuantities, quotedPriceInfoTO, aaAirBookRQExt, currencyCode, otaAirBookRQ, exchangeRateProxy);

			for (LCCClientPaymentAssembler travelerPayment : travelersPayments.values()) {
				WSReservationUtil.authorizePayment(privilegeKeys, travelerPayment, userPrincipal.getAgentCode());
				totalPayment.add(travelerPayment.getTotalPayAmount());
			}
			if (travelersPayments.size() != (paxTypeQuantities[0] + paxTypeQuantities[1] + paxTypeQuantities[2])) {
				isPartialPayment = true;
			}
		}

		if (totalPayment.compareTo(totalFareFromPriceBreakdown) == 0) {
			lcclientReservationAssembler.setActualPayment(true);
		}

		lcclientReservationAssembler.setOnHoldBooking(isHoldBooking);
		lcclientReservationAssembler.setSplitBooking(isPartialPayment);

		if (log.isDebugEnabled()) {
			log.debug("################ BOOK REQUEST Onhold ? " + isHoldBooking);
			log.debug("################ BOOK REQUEST partialPay ? " + isPartialPayment);
		}

		if (isHoldBooking || isPartialPayment) {
			holdReleaseTimestamp = WebplatformUtil.getReleaseTimeStamp(
					ReleaseTimeUtil.getFlightDepartureInfo(quotedPriceInfoTO.getQuotedSegments()), privilegeKeys, true,
					userPrincipal.getAgentCode(), AppIndicatorEnum.APP_WS.toString(), new OnHoldReleaseTimeDTO());
		} else {
			holdReleaseTimestamp = WebplatformUtil.getReleaseTimeStamp(
					ReleaseTimeUtil.getFlightDepartureInfo(quotedPriceInfoTO.getQuotedSegments()), privilegeKeys, false,
					userPrincipal.getAgentCode(), AppIndicatorEnum.APP_WS.toString(), new OnHoldReleaseTimeDTO());
		}

		if (holdReleaseTimestamp == null) {
			if (isPartialPayment) {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_SPLIT_OPERATION_,
						"Current partially paid reservation is not eligible for hold");
			} else {
				throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_RESERVATION_IS_NOT_ELIGIBLE_FOR_HOLD, null);
			}
		}
		return holdReleaseTimestamp;
	}

	/**
	 * Extracts pax wise payment details from AirBookRQ
	 * 
	 * @param totalFareFromPriceBreakdown
	 * @param paxRelationships
	 * @param travelersPayments
	 * @param paxCounts
	 * @param quotedPricingSegmentsInfo
	 * @param aaBookRQAAExt
	 * @param totalFareFromPriceBreakdownInSelectedCurrency
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	private HashMap<String, LCCClientPaymentAssembler> getTravlersPayments(BigDecimal totalFareFromPriceBreakdown,
			HashMap<String, Object[]> paxRelationships, HashMap<String, LCCClientPaymentAssembler> travelersPayments,
			int[] paxCounts, QuotedPriceInfoTO quotedPriceInfoTO, AAAirBookRQExt aaBookRQAAExt, String selectedCurrencyCode,
			OTAAirBookRQ otaAirBookRQ, ExchangeRateProxy exchangeRateProxy) throws WebservicesException, ModuleException {

		OTAAirBookRQ.Fulfillment fulfillment = otaAirBookRQ.getFulfillment();
		List<AirTravelerType> travelers = otaAirBookRQ.getTravelerInfo().getAirTraveler();
		TravelersFulfillments travelersFulfillments = otaAirBookRQ.getTravelersFulfillments();
		PriceInfo priceInfo = otaAirBookRQ.getPriceInfo();

		if (fulfillment != null && fulfillment.getPaymentDetails() != null) {
			// BigDecimal[] expectedPaymentAmounts = ReservationUtil.getQuotedTotalPrices(quotedPricingSegmentsInfo,
			// paxCounts[0],
			// paxCounts[2], paxCounts[1]);

			UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
			LCCClientPaymentAssembler reservationPayments = new LCCClientPaymentAssembler();
			OTAUtils.extractReservationPayments(fulfillment.getPaymentDetails().getPaymentDetail(), reservationPayments,
					principal.getAgentCurrencyCode(), exchangeRateProxy);

			// extra validation on total price and price breakdown
			if (!AppSysParamsUtil.isWebServiceExtaraValidationSkipCarrier(principal.getAgentCode())) {
				// total fare validation from ItinTotalFare
				if (!isTotalPricesEqualFromItin(priceInfo, totalFareFromPriceBreakdown)) {
					log.error("TotalFare given in ItinTotalFare does not match with expected Total fair" + " [TotalFare given = "
							+ priceInfo.getItinTotalFare().getTotalFare().getAmount() + ", TotalFare expected = "
							+ totalFareFromPriceBreakdown + "]");
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
							"TotalFare given in ItinTotalFare does not match with expected Total fair");
				}

				// total fare validation from the price breakdown info
				if (!isTotalPricesEqualsFromPriceInfo(priceInfo, totalFareFromPriceBreakdown)) {
					log.error("TotalFare calculated from priceInfo does not match with expected Total fair");
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
							"TotalFare calculated from priceInfo does not match with expected Total fair");
				}
				// }
			}

			if (travelersFulfillments == null) {// full payment, single payment type expected

				BigDecimal paxPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

				if (reservationPayments.getPayments().size() == 1) {
					// FIXME support multiple payment options in one request
					Object resPayment = reservationPayments.getPayments().iterator().next();
					if (resPayment instanceof LCCClientOnAccountPaymentInfo) {

						String paymentMethod = null;
						boolean isBSPPayment = Agent.PAYMENT_MODE_BSP.equals(((LCCClientOnAccountPaymentInfo) resPayment)
								.getPaymentMethod());
						boolean applyBSPFee = isBSPPayment && ParameterUtil.isApplyBSPFee();
						int totalpayablePaxCount = paxCounts[0] + paxCounts[2] + paxCounts[1];
						BigDecimal totalServiceTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
						BigDecimal totalBSPFeeWithServiceTaxAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
						LinkedList bspExternalChargeLinkedList;
						
						if (isBSPPayment) {
							paymentMethod = Agent.PAYMENT_MODE_BSP;
						}

						if (applyBSPFee) {
							
							ExternalChgDTO externalChgDTO = ReservationUtil.getBSPCharge(totalpayablePaxCount);							
							BigDecimal bspFee = AccelAeroCalculator.getDefaultBigDecimalZero();						
							bspFee = externalChgDTO.getAmount();						

							totalServiceTaxAmount = ServiceTaxUtil.calculateServiceTaxForCCFeeOnly(
									ServiceTaxUtil.getDummyBaseAvailRQ(paxCounts[0], paxCounts[2], paxCounts[1]),
									quotedPriceInfoTO.getQuotedSegments(), totalpayablePaxCount, externalChgDTO, false, true);
							totalBSPFeeWithServiceTaxAmount = AccelAeroCalculator.add(totalServiceTaxAmount, bspFee);
							totalFareFromPriceBreakdown = AccelAeroCalculator.add(totalFareFromPriceBreakdown,
									totalBSPFeeWithServiceTaxAmount);

							bspExternalChargeLinkedList = ReservationUtil.getExternalChagersPerPassengers(totalpayablePaxCount,
									externalChgDTO);

						} else {
							bspExternalChargeLinkedList = new LinkedList<>();
						}

						if (!(AppSysParamsUtil.isAdminFeeRegulationEnabled() && AppSysParamsUtil
								.isAdminFeeChannelWiseConfigured(ApplicationEngine.WS, principal.getAgentCode()))) {

							if (OTAUtils.isShowPriceInselectedCurrency()) {
								BigDecimal difference = AccelAeroCalculator.subtract(reservationPayments.getTotalPayAmount(),
										totalFareFromPriceBreakdown).abs();
								if (difference.doubleValue() > ERROR_MARGIN) {
									log.error("Payment does not match with the charges " + " [Received payment = "
											+ reservationPayments.getTotalPayAmount() + ", Expected Payment = "
											+ totalFareFromPriceBreakdown + "]");
									throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
											"Quoted price no longer available,Please search again.");
								}
							} else {
								// FIXME change the error msg till the PQ issue is solved .
								if (reservationPayments.getTotalPayAmount().compareTo(totalFareFromPriceBreakdown) != 0) {
									log.error("Payment does not match with the charges " + " [Received payment = "
											+ reservationPayments.getTotalPayAmount() + ", Expected Payment = "
											+ totalFareFromPriceBreakdown + "]");
									throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
											"Quoted price no longer available,Please search again.");
								}
							}

							for (AirTravelerType traveler : travelers) {

								paxPaymentAmount = getPaxAmount(traveler, quotedPriceInfoTO, paxRelationships);
								LCCClientPaymentAssembler paxPayment = travelersPayments.get(traveler.getTravelerRefNumber()
										.getRPH());

								// adding selected external charges
								BookInterlineUtil
										.addSelectedExternalCharges(paxPayment, traveler.getTravelerRefNumber().getRPH());

								if (applyBSPFee) {
									paxPayment.addExternalCharges((Collection) bspExternalChargeLinkedList.pop());
								}

								ServiceTaxCalculator.calculatePaxJNTax(paxPayment);

								String agentCode = ((LCCClientOnAccountPaymentInfo) resPayment).getAgentCode();
								paxPayment.addAgentCreditPayment(agentCode, paxPaymentAmount,
										WSReservationUtil.getPayCurrencyDTO(agentCode, exchangeRateProxy), null, null, null,
										paymentMethod, null);
							}

						} else {

							int totalSementCount = quotedPriceInfoTO.getQuotedSegments().size();
								
							ExternalChgDTO externalChgDTO = WSReservationUtil.getCCChargeAmount(totalFareFromPriceBreakdown, totalpayablePaxCount, totalSementCount,
									ChargeRateOperationType.MAKE_ONLY);


							BigDecimal CCCharge = WSReservationUtil.getAmountInSpecifiedCurrency(
									ReservationUtil.getCCCharge(totalFareFromPriceBreakdown, totalpayablePaxCount,
											totalSementCount, ChargeRateOperationType.MAKE_ONLY).getAmount(),
									selectedCurrencyCode, exchangeRateProxy);
							CCCharge = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.CREDIT_CARD, CCCharge);
							
							totalServiceTaxAmount = ServiceTaxUtil.calculateServiceTaxForCCFeeOnly(ServiceTaxUtil.getDummyBaseAvailRQ(paxCounts[0], paxCounts[2], paxCounts[1]),
									quotedPriceInfoTO.getQuotedSegments(), totalpayablePaxCount, externalChgDTO, false,true);
							CCCharge = AccelAeroCalculator.add(CCCharge, totalServiceTaxAmount);

							BigDecimal expectedTotalPayment = AccelAeroCalculator.add(totalFareFromPriceBreakdown, CCCharge);

							if (OTAUtils.isShowPriceInselectedCurrency()) {
								BigDecimal difference = AccelAeroCalculator.subtract(reservationPayments.getTotalPayAmount(),
										expectedTotalPayment).abs();
								if (difference.doubleValue() > ERROR_MARGIN) {
									log.error("Payment does not match with the charges " + " [Received payment = "
											+ reservationPayments.getTotalPayAmount() + ", Expected Payment = "
											+ expectedTotalPayment + " ]");
									throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
											"Quoted price no longer available,Please search again.");
								}
							} else {
								// FIXME change the error msg till the PQ issue is solved .
								if (reservationPayments.getTotalPayAmount().compareTo(expectedTotalPayment) != 0) {
									log.error("Payment does not match with the charges " + " [Received payment = "
											+ reservationPayments.getTotalPayAmount() + ", Expected Payment = "
											+ expectedTotalPayment + " ]");
									throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
											"Quoted price no longer available,Please search again.");
								}
							}

							// Splitting CC charges to per pax wise to be added to the payment.
							LinkedList externalChargeSplit = ReservationUtil.getExternalChagersPerPassengers(
									totalpayablePaxCount, ReservationUtil.getCCCharge(totalFareFromPriceBreakdown,
											totalpayablePaxCount, totalSementCount, ChargeRateOperationType.MAKE_ONLY));

							for (AirTravelerType traveler : travelers) {
								paxPaymentAmount = getPaxAmount(traveler, quotedPriceInfoTO, paxRelationships);
								LCCClientPaymentAssembler paxPayment = travelersPayments.get(traveler.getTravelerRefNumber()
										.getRPH());

								paxPayment.addExternalCharges((Collection) externalChargeSplit.pop());

								// adding selected external charges
								BookInterlineUtil
										.addSelectedExternalCharges(paxPayment, traveler.getTravelerRefNumber().getRPH());

								if (applyBSPFee) {
									paxPayment.addExternalCharges((Collection) bspExternalChargeLinkedList.pop());
								}

								ServiceTaxCalculator.calculatePaxJNTax(paxPayment);

								String agentCode = ((LCCClientOnAccountPaymentInfo) resPayment).getAgentCode();
								paxPayment.addAgentCreditPayment(agentCode, paxPaymentAmount,
										WSReservationUtil.getPayCurrencyDTO(agentCode, exchangeRateProxy), null, null, null,
										paymentMethod, null);
							}
						}

					} else if (resPayment instanceof CommonCreditCardPaymentInfo) {
						int totalSementCount = quotedPriceInfoTO.getQuotedSegments().size();
						int totalpayablePaxCount = paxCounts[0] + paxCounts[2];

						ExternalChgDTO externalChgDTO = WSReservationUtil.getCCChargeAmount(totalFareFromPriceBreakdown,
								totalpayablePaxCount, totalSementCount, ChargeRateOperationType.MAKE_ONLY);

						BigDecimal CCCharge = WSReservationUtil.getAmountInSpecifiedCurrency(externalChgDTO.getAmount(),
								selectedCurrencyCode, exchangeRateProxy);
						CCCharge = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.CREDIT_CARD, CCCharge);

						BigDecimal totalServiceTaxAmount = ServiceTaxUtil.calculateServiceTaxForCCFeeOnly(
								ServiceTaxUtil.getDummyBaseAvailRQ(paxCounts[0], paxCounts[2], paxCounts[1]),
								quotedPriceInfoTO.getQuotedSegments(), totalpayablePaxCount, externalChgDTO, false, false);
						CCCharge = AccelAeroCalculator.add(CCCharge, totalServiceTaxAmount);

						BigDecimal expectedTotalPayment = AccelAeroCalculator.add(totalFareFromPriceBreakdown, CCCharge);

						if (OTAUtils.isShowPriceInselectedCurrency()) {
							BigDecimal difference = AccelAeroCalculator.subtract(reservationPayments.getTotalPayAmount(),
									expectedTotalPayment).abs();
							if (difference.doubleValue() > ERROR_MARGIN) {
								log.error("Payment does not match with the charges " + " [Received payment = "
										+ reservationPayments.getTotalPayAmount() + ", Expected Payment = "
										+ expectedTotalPayment + " ]");
								throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
										"Quoted price no longer available,Please search again.");
							}
						} else {
							// FIXME change the error msg till the PQ issue is solved .
							if (reservationPayments.getTotalPayAmount().compareTo(expectedTotalPayment) != 0) {
								log.error("Payment does not match with the charges " + " [Received payment = "
										+ reservationPayments.getTotalPayAmount() + ", Expected Payment = "
										+ expectedTotalPayment + " ]");
								throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
										"Quoted price no longer available,Please search again.");
							}
						}

						// Splitting CC charges to per pax wise to be added to the payment.
						LinkedList externalChargeSplit = ReservationUtil.getExternalChagersPerPassengers(totalpayablePaxCount,
								WSReservationUtil.getCCChargeAmount(totalFareFromPriceBreakdown, totalpayablePaxCount,
										totalSementCount, ChargeRateOperationType.MAKE_ONLY));

						if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
								&& !AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.WS, principal.getAgentCode())) {
							externalChargeSplit.clear();
						}

						for (AirTravelerType traveler : travelers) {
							paxPaymentAmount = getPaxAmount(traveler, quotedPriceInfoTO, paxRelationships);
							LCCClientPaymentAssembler paxPayment = travelersPayments
									.get(traveler.getTravelerRefNumber().getRPH());

							if (!externalChargeSplit.isEmpty()) {
								paxPayment.addExternalCharges((Collection) externalChargeSplit.pop());
							}

							// adding selected external charges
							BookInterlineUtil.addSelectedExternalCharges(paxPayment, traveler.getTravelerRefNumber().getRPH());

							ServiceTaxCalculator.calculatePaxJNTax(paxPayment);

							CommonCreditCardPaymentInfo cardPayInfo = (CommonCreditCardPaymentInfo) resPayment;

							paxPayment.addInternalCardPayment(cardPayInfo.getType(), cardPayInfo.geteDate(), cardPayInfo.getNo(),
									cardPayInfo.getName(), cardPayInfo.getAddress(), cardPayInfo.getSecurityCode(),
									paxPaymentAmount, AppIndicatorEnum.APP_XBE, TnxModeEnum.MAIL_TP_ORDER,
									cardPayInfo.getIpgIdentificationParamsDTO(), cardPayInfo.getPayCurrencyDTO(), null, null,
									null, cardPayInfo.getOldCCRecordId(), null, cardPayInfo.getName(),
									cardPayInfo.getAuthorizationId());
						}
					} else {
						// TODO implement support for other payment types
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
								"Only agent onaccount payment supported");
					}
				} else {
					// TODO implement for accepting multiple payment
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_OPERATION_OR_VALUE_NOT_SUPPORTED_,
							"For multiple payments type, pax payment break down should be provided");
				}

			} else {// partial payment or payments from two or more payment types
				String travelerRPH = null;
				String infTravelerRPH = null;
				Object[] paxRelationship = null;
				AirTravelerType traveler = null;
				BigDecimal expectedPaxPayment = AccelAeroCalculator.getDefaultBigDecimalZero();
				BigDecimal totalPaxPayments = AccelAeroCalculator.getDefaultBigDecimalZero();
				for (TravelersFulfillments.TravelerFulfillments travelerPayments : travelersFulfillments
						.getTravelerFulfillments()) {
					travelerRPH = travelerPayments.getTravelerRefNumberRPH();
					paxRelationship = paxRelationships.get(travelerRPH);
					traveler = (AirTravelerType) paxRelationship[3];
					PerPaxChargesTO perPaxChargesTO = null;
					// Adult or Parent
					if (traveler.getPassengerTypeCode().equals(
							CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
						perPaxChargesTO = quotedPriceInfoTO.getPerPaxChargeByPaxType(CommonUtil
								.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
						LCCClientPaymentAssembler paxPayment = new LCCClientPaymentAssembler();

						// adding selected external charges
						BookInterlineUtil.addSelectedExternalCharges(paxPayment, travelerRPH);

						ServiceTaxCalculator.calculatePaxJNTax(paxPayment);

						OTAUtils.extractReservationPayments(travelerPayments.getPaymentDetail(), paxPayment,
								principal.getAgentCurrencyCode(), exchangeRateProxy);
						if (perPaxChargesTO != null) {
							expectedPaxPayment = perPaxChargesTO.getTotalFare();
						}
						totalPaxPayments = AccelAeroCalculator.add(totalPaxPayments, paxPayment.getTotalPayAmount());
						if (expectedPaxPayment.compareTo(paxPayment.getTotalPayAmount()) != 0) {
							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
									"Partial payment for a traveler not allowed");
						}

						travelersPayments.put(travelerRPH, paxPayment);
					}

					// Child
					else if (traveler.getPassengerTypeCode().equals(
							CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
						LCCClientPaymentAssembler paxPayment = new LCCClientPaymentAssembler();
						perPaxChargesTO = quotedPriceInfoTO.getPerPaxChargeByPaxType(CommonUtil
								.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
						// adding selected external charges
						BookInterlineUtil.addSelectedExternalCharges(paxPayment, travelerRPH);

						ServiceTaxCalculator.calculatePaxJNTax(paxPayment);

						OTAUtils.extractReservationPayments(travelerPayments.getPaymentDetail(), paxPayment,
								principal.getAgentCurrencyCode(), exchangeRateProxy);
						if (perPaxChargesTO != null) {
							expectedPaxPayment = perPaxChargesTO.getTotalFare();
						}
						totalPaxPayments = AccelAeroCalculator.add(totalPaxPayments, paxPayment.getTotalPayAmount());
						if (paxPayment.getTotalPayAmount().compareTo(expectedPaxPayment) != 0) {
							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
									"Partial payment for a traveler not allowed");
						}

						travelersPayments.put(travelerRPH, paxPayment);
					}

					// Child
					else if (traveler.getPassengerTypeCode().equals(
							CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
						LCCClientPaymentAssembler paxPayment = new LCCClientPaymentAssembler();
						perPaxChargesTO = quotedPriceInfoTO.getPerPaxChargeByPaxType(CommonUtil
								.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
						// adding selected external charges
						BookInterlineUtil.addSelectedExternalCharges(paxPayment, travelerRPH);

						ServiceTaxCalculator.calculatePaxJNTax(paxPayment);

						OTAUtils.extractReservationPayments(travelerPayments.getPaymentDetail(), paxPayment,
								principal.getAgentCurrencyCode(), exchangeRateProxy);
						if (perPaxChargesTO != null) {
							expectedPaxPayment = perPaxChargesTO.getTotalFare();
						}
						totalPaxPayments = AccelAeroCalculator.add(totalPaxPayments, paxPayment.getTotalPayAmount());
						if (paxPayment.getTotalPayAmount().compareTo(expectedPaxPayment) != 0) {
							throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
									"Partial payment for a traveler not allowed");
						}

						travelersPayments.put(travelerRPH, paxPayment);
					}

				}

				if (totalPaxPayments.compareTo(reservationPayments.getTotalPayAmount()) != 0) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_PAYMENT_REJECTED_,
							"Traveler wise payment does not match total payment");
				}
			}
			
			addAutomaticCheckinEmail(travelers, travelersPayments, aaBookRQAAExt);
		}

		return travelersPayments;
	}
	
	private void addAutomaticCheckinEmail(List<AirTravelerType> travelers,
			HashMap<String, LCCClientPaymentAssembler> travelersPayments, AAAirBookRQExt aaBookRQAAExt) {
		String automaticCheckinEmail = null;
		if (aaBookRQAAExt.getTravelerAdditionalInfo() != null) {
			for (AirTravelerType traveler : travelers) {
				automaticCheckinEmail = OTAUtils.getAutomaticCheckinEmail(aaBookRQAAExt.getTravelerAdditionalInfo()
						.getAAAirTravelersType(), traveler.getTravelerRefNumber().getRPH());
				if (automaticCheckinEmail != null) {
					LCCClientPaymentAssembler paxPayment = travelersPayments.get(traveler.getTravelerRefNumber().getRPH());
					BookInterlineUtil.addAutomaticCheckinEmail(paxPayment, traveler.getTravelerRefNumber().getRPH(),
							automaticCheckinEmail);
				}
			}

		}
	}

	/**
	 * 
	 * @param traveler
	 * @param quotedPriceInfoTO
	 * @param paxRelationships
	 * @return
	 */

	private BigDecimal getPaxAmount(AirTravelerType traveler, QuotedPriceInfoTO quotedPriceInfoTO,
			HashMap<String, Object[]> paxRelationships) {
		BigDecimal paxPaymentAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		PerPaxChargesTO perPaxChargesTO = null;

		if (traveler.getPassengerTypeCode().equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
			// add adult total payment
			perPaxChargesTO = quotedPriceInfoTO.getPerPaxChargeByPaxType(CommonUtil
					.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
			if (perPaxChargesTO != null) {
				paxPaymentAmount = AccelAeroCalculator.add(paxPaymentAmount, perPaxChargesTO.getTotalFare());
			}
		} else if (traveler.getPassengerTypeCode().equals(
				CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
			// add child total payment
			perPaxChargesTO = quotedPriceInfoTO.getPerPaxChargeByPaxType(CommonUtil
					.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
			if (perPaxChargesTO != null) {
				paxPaymentAmount = AccelAeroCalculator.add(paxPaymentAmount, perPaxChargesTO.getTotalFare());
			}
		} else if (traveler.getPassengerTypeCode().equals(
				CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
			// add child total payment
			perPaxChargesTO = quotedPriceInfoTO.getPerPaxChargeByPaxType(CommonUtil
					.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));
			if (perPaxChargesTO != null) {
				paxPaymentAmount = AccelAeroCalculator.add(paxPaymentAmount, perPaxChargesTO.getTotalFare());
			}
		}
		return paxPaymentAmount;
	}

	/**
	 * checks if the total fare price from the ItinTotalFare is equal to the expected one,
	 * 
	 * @param priceInfo
	 * @param expectedPaymentAmounts
	 * @return
	 */
	private boolean isTotalPricesEqualFromItin(PriceInfo priceInfo, BigDecimal expectedPaymentAmounts) {
		BigDecimal totalTicketPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (priceInfo != null && priceInfo.getItinTotalFare() != null) {
			totalTicketPrice = priceInfo.getItinTotalFare().getTotalFare().getAmount();
			if (OTAUtils.isShowPriceInselectedCurrency()) {
				BigDecimal difference = AccelAeroCalculator.subtract(expectedPaymentAmounts, totalTicketPrice).abs();
				return (difference.doubleValue() < ERROR_MARGIN);
			} else {
				return (totalTicketPrice.compareTo(expectedPaymentAmounts) == 0);
			}
		}

		return true; // total price is not present in the ItinTotalFare.
	}

	/**
	 * checks if the total fare price from the price info equals to the expected one
	 * 
	 * @param priceInfo
	 * @param expectedPaymentAmounts
	 * @return
	 */
	private boolean isTotalPricesEqualsFromPriceInfo(PriceInfo priceInfo, BigDecimal expectedPaymentAmounts) {
		BigDecimal totalTicketPrice = AccelAeroCalculator.getDefaultBigDecimalZero();
		if (priceInfo != null && priceInfo.getPTCFareBreakdowns() != null) {
			for (PTCFareBreakdownType breakdownType : priceInfo.getPTCFareBreakdowns().getPTCFareBreakdown()) {
				Integer paxCount = breakdownType.getPassengerTypeQuantity().getQuantity();
				BigDecimal totalFarePerPax = AccelAeroCalculator.getDefaultBigDecimalZero();
				FareType fareType = breakdownType.getPassengerFare();
				if (fareType != null) {
					BigDecimal baseFare = fareType.getBaseFare().getAmount();
					BigDecimal taxFare = AccelAeroCalculator.getDefaultBigDecimalZero();
					BigDecimal fees = AccelAeroCalculator.getDefaultBigDecimalZero();
					if (fareType.getTaxes() != null) {
						for (AirTaxType airTaxType : fareType.getTaxes().getTax()) {
							taxFare = AccelAeroCalculator.add(taxFare, airTaxType.getAmount());
						}
					}
					if (fareType.getFees() != null) {
						for (AirFeeType fee : fareType.getFees().getFee()) {
							fees = AccelAeroCalculator.add(fees, fee.getAmount());
						}
					}
					totalFarePerPax = AccelAeroCalculator.add(baseFare, taxFare, fees);
				}
				totalTicketPrice = AccelAeroCalculator.add(totalTicketPrice,
						AccelAeroCalculator.multiply(totalFarePerPax, paxCount));
			}
			if (OTAUtils.isShowPriceInselectedCurrency()) {
				BigDecimal difference = AccelAeroCalculator.subtract(expectedPaymentAmounts, totalTicketPrice).abs();
				return (difference.doubleValue() < ERROR_MARGIN);
			} else {
				return (totalTicketPrice.compareTo(expectedPaymentAmounts) == 0);
			}
		}
		return true; // price info not present return true.
	}

	/**
	 * Decodes Adult/Infant relationships and Prepares paxSequence Numbers based on travelerRPH Valid travelerRPH are
	 * Ai, Ij/Ai where i,j are mutually exclusive subsets of positive integers
	 * 
	 * @return HashMap of Object [] {paxSequence, infant/parent paxSeqence or -1, infant/parent paxRPH or null,
	 *         airTraveler}
	 */
	private HashMap<String, Object[]> preparePaxRelationships(List<AirTravelerType> airTravelers) throws WebservicesException {
		HashMap<String, Object[]> paxSequencesMap = new HashMap<String, Object[]>();
		int paxSequence;
		String travelerRPH;
		String parentRPH;
		List<Integer> paxSequenceList = new ArrayList<Integer>();// for validating sequence uniqueness
		for (AirTravelerType airTraveler : airTravelers) {
			if (airTraveler.getPassengerTypeCode().equals(
					CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))
					|| airTraveler.getPassengerTypeCode().equals(
							CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
				travelerRPH = airTraveler.getTravelerRefNumber().getRPH();
				paxSequence = Integer.parseInt(travelerRPH.substring(1));
				if (paxSequenceList.contains(new Integer(paxSequence))) {
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_VALUE___TRAVELER_RPH,
							"TravelerRPH [" + travelerRPH + "] is in conflict.");
				} else {
					paxSequenceList.add(new Integer(paxSequence));
				}
				paxSequencesMap.put(travelerRPH, new Object[] { paxSequence, -1, null, airTraveler });
			}
		}

		Object[] parentSeqences;
		for (AirTravelerType airTraveler : airTravelers) {
			if (airTraveler.getPassengerTypeCode().equals(
					CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
				travelerRPH = airTraveler.getTravelerRefNumber().getRPH();
				// infant sequence
				paxSequence = Integer.parseInt(travelerRPH.substring(1, travelerRPH.indexOf('/')));
				if (paxSequenceList.contains(new Integer(paxSequence))) {
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_VALUE___TRAVELER_RPH,
							"TravelerRPH [" + travelerRPH + "] is in conflict.");
				} else {
					paxSequenceList.add(new Integer(paxSequence));
				}

				parentRPH = travelerRPH.substring(travelerRPH.indexOf('/') + 1);
				parentSeqences = paxSequencesMap.get(parentRPH);
				if (parentSeqences == null) {
					throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_PARENT_TRAVELER_NOT_FOUND_,
							"Infant of RPH [" + airTraveler.getTravelerRefNumber().getRPH() + "] is not linked to any adult.");
				}
				parentSeqences[1] = paxSequence;// infant sequence
				parentSeqences[2] = travelerRPH;// infant RPH
				paxSequencesMap.put(parentRPH, parentSeqences);

				paxSequencesMap.put(travelerRPH, new Object[] { paxSequence, parentSeqences[0], parentRPH, airTraveler });
			}
		}
		return paxSequencesMap;
	}

	/**
	 * Extracts the pax wise handling chargers
	 * 
	 * @param travelers
	 * @param paxCounts
	 * @return
	 * @throws ModuleException
	 */
	private HashMap<String, LCCClientPaymentAssembler> getHandlerFee(List<AirTravelerType> travelers,
			OTAPaxCountTO otaPaxCountTO, ExternalChgDTO handlingFee) throws ModuleException {

		HashMap<String, LCCClientPaymentAssembler> travelersPayments = new HashMap<String, LCCClientPaymentAssembler>();

		int adultAndChildCount = otaPaxCountTO.getAdultCount() + otaPaxCountTO.getChildCount();

		LinkedList externalChargeSplit = ReservationUtil.getExternalChagersPerPassengers(adultAndChildCount, handlingFee);
		for (AirTravelerType traveler : travelers) {
			LCCClientPaymentAssembler lccClientPaymentAssembler = new LCCClientPaymentAssembler();
			// Handling fee will be added to only adults and children
			if (!traveler.getPassengerTypeCode().equals(
					CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
				lccClientPaymentAssembler.addExternalCharges((Collection<ExternalChgDTO>) externalChargeSplit.pop());
			}
			travelersPayments.put(traveler.getTravelerRefNumber().getRPH(), lccClientPaymentAssembler);
		}
		return travelersPayments;
	}

	/**
	 * Prepares contact info from the first adult traveler
	 * 
	 * @throws WebservicesException
	 */
	private CommonReservationContactInfo prepareContactInfoTravelersInfo(List<AirTravelerType> travelers)
			throws WebservicesException {
		CommonReservationContactInfo contactInfo = new CommonReservationContactInfo();
		AirTravelerType firstAdultTraveler = getFirstAdultPax(travelers);

		contactInfo.setFirstName(CommonUtil.getValueFromNullSafeList(firstAdultTraveler.getPersonName().getGivenName(), 0));
		contactInfo.setLastName(firstAdultTraveler.getPersonName().getSurname());

		AirTravelerType.Email email = CommonUtil.getValueFromNullSafeList(firstAdultTraveler.getEmail(), 0);
		if (email != null && email.getValue() != null) {
			contactInfo.setEmail(CommonUtil.removeInvalidCharactersFromEmailAddress(email.getValue()));
		}

		AirTravelerType.Address address = CommonUtil.getValueFromNullSafeList(firstAdultTraveler.getAddress(), 0);
		if (address != null) {
			contactInfo.setCountryCode(address.getCountryName().getCode());
			contactInfo.setCity(address.getCityName());

			contactInfo.setStreetAddress1(CommonUtil.getValueFromNullSafeList(address.getAddressLine(), 0));
			contactInfo.setStreetAddress2(CommonUtil.getValueFromNullSafeList(address.getAddressLine(), 1));
		}

		String telephoneCountryCode = null;
		String telephoneAreaCode = null;
		AirTravelerType.Telephone telephone = CommonUtil.getValueFromNullSafeList(firstAdultTraveler.getTelephone(), 0);
		if (telephone != null) {
			telephoneCountryCode = telephone.getCountryAccessCode();
			telephoneAreaCode = telephone.getAreaCityCode();
			String phoneNumber = telephoneCountryCode != null ? telephoneCountryCode
					+ WebservicesConstants.PHONE_NUMBER_SEPERATOR : "";
			if (telephone.getAreaCityCode() != null) {
				phoneNumber += telephoneAreaCode + WebservicesConstants.PHONE_NUMBER_SEPERATOR;
			}
			phoneNumber += telephone.getPhoneNumber();

			contactInfo.setPhoneNo(phoneNumber);
		}
		return contactInfo;
	}

	/**
	 * Extracts the traveler with the least RPH ( valid adult/child traveler RPH are of the form Ai where i is a
	 * possitive integer)
	 */
	private AirTravelerType getFirstAdultPax(List<AirTravelerType> travelers) {
		AirTravelerType firstAdultPax = null;
		for (AirTravelerType traveler : travelers) {
			if (traveler.getPassengerTypeCode()
					.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))
					|| traveler.getPassengerTypeCode().equals(
							CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
				if (firstAdultPax == null
						|| Integer.parseInt(firstAdultPax.getTravelerRefNumber().getRPH().substring(1)) > Integer
								.parseInt(traveler.getTravelerRefNumber().getRPH().substring(1))) {
					firstAdultPax = traveler;
				}
			}
		}
		return firstAdultPax;
	}

	/**
	 * @return int [] {adultsCount, infantsCount, children}
	 * @throws WebservicesException
	 */
	private OTAPaxCountTO getPaxCounts(List<AirTravelerType> travelers) throws WebservicesException {
		int adults = 0;
		int infants = 0;
		int children = 0;
		for (AirTravelerType airTraveler : travelers) {
			if (airTraveler.getPassengerTypeCode().equals(
					CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))) {
				++adults;
			} else if (airTraveler.getPassengerTypeCode().equals(
					CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
				++children;
			} else if (airTraveler.getPassengerTypeCode().equals(
					CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT))) {
				++infants;
			} else {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, "Invalid Passenger Type");
			}
		}
		return new OTAPaxCountTO(adults, children, infants);
	}

	/**
	 * This method validate the given age(for the arrival datetime) of a traveler with the given age category
	 * 
	 * @param dob
	 *            date of birth
	 * @param passengerType
	 *            AD,CH, or IN
	 * @throws WebservicesException
	 *             if validation fails.
	 */
	private void validateAge(Date dateOfBirth, String passengerType, OTAAirBookRQ otaAirBookRQ) throws WebservicesException {

		UserPrincipal userPrincipal = ThreadLocalData.getCurrentUserPrincipal();

		if (dateOfBirth != null && !AppSysParamsUtil.isWebServiceExtaraValidationSkipCarrier(userPrincipal.getAgentCode())) {
			GlobalConfig globalConfig = WebServicesModuleUtils.getGlobalConfig();
			int maxAgeLimitInYears = globalConfig.getPaxTypeMap().get(passengerType).getCutOffAgeInYears();
			Calendar dob = Calendar.getInstance();
			Date startTimeOfDOB = CalendarUtil.getStartTimeOfDate(dateOfBirth);
			dob.setTime(startTimeOfDOB);

			// get the first travel date see AARESAA-2715
			List<Date> arrivalDateList = new ArrayList<Date>();
			for (OriginDestinationOptionType originDestinationOptionType : otaAirBookRQ.getAirItinerary()
					.getOriginDestinationOptions().getOriginDestinationOption()) {
				for (BookFlightSegmentType bookFlightSegmentType : originDestinationOptionType.getFlightSegment()) {
					arrivalDateList.add(bookFlightSegmentType.getArrivalDateTime().toGregorianCalendar().getTime());
				}
			}
			Collections.sort(arrivalDateList);
			Calendar flyDate = Calendar.getInstance();
			Date startTimeOfFltDate = CalendarUtil.getStartTimeOfDate(arrivalDateList.get(arrivalDateList.size() - 1));
			flyDate.setTime(startTimeOfFltDate);

			Calendar dobLowerBoundary = Calendar.getInstance();
			dobLowerBoundary.setTime(startTimeOfFltDate);
			dobLowerBoundary.add(Calendar.YEAR, -maxAgeLimitInYears);

			long difference = dob.getTime().getTime() - dobLowerBoundary.getTime().getTime();

			int dayDiff = (int) TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS);

			if (dayDiff < 0) { // validation error for DOB. DOB is not fit to the age category
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, String.format("Invalide dob for %s",
						passengerType));
			}

			// validate the lower bound for age.
			if (passengerType.equals(PaxTypeTO.ADULT)) {
				int minAgeinDays = globalConfig.getPaxTypeMap().get(passengerType).getAgeLowerBoundaryInMonths();
				int diffInMonth = getMonthDifference((Calendar) dob.clone(), flyDate);
				if (diffInMonth < minAgeinDays) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, String.format(
							"Invalide dob for %s", passengerType));
				}
			} else if (passengerType.equals(PaxTypeTO.CHILD)) {
				int minAgeInMonths = globalConfig.getPaxTypeMap().get(passengerType).getAgeLowerBoundaryInMonths();
				int diffInMonth = getMonthDifferenceWithoutBoundry((Calendar) dob.clone(), flyDate);
				if (diffInMonth < minAgeInMonths) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, String.format(
							"Invalide dob for %s", passengerType));
				}

			} else if (passengerType.equals(PaxTypeTO.INFANT)) {
				int minAgeInDays = globalConfig.getPaxTypeMap().get(passengerType).getAgeLowerBoundaryInDays();
				int diffInDays = getDateDifference((Calendar) dob.clone(), flyDate);
				if (diffInDays < minAgeInDays) {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_INVALID_VALUE, String.format(
							"Invalide dob for %s", passengerType));
				}
			}
		}
	}

	private int getYearDifference(Calendar fromCalendar, Calendar toCalendar) {
		int count = 0;
		for (fromCalendar.add(Calendar.YEAR, 1); fromCalendar.compareTo(toCalendar) <= 0; fromCalendar.add(Calendar.YEAR, 1)) {
			count++;
		}
		return count;
	}

	private int getMonthDifference(Calendar fromCalendar, Calendar toCalendar) {
		int count = 0;
		for (fromCalendar.add(Calendar.MONTH, 1); fromCalendar.compareTo(toCalendar) <= 0; fromCalendar.add(Calendar.MONTH, 1)) {
			count++;
		}
		return count;
	}

	private int getMonthDifferenceWithoutBoundry(Calendar fromCalendar, Calendar toCalendar) {
		int count = 0;
		for (fromCalendar.add(Calendar.MONTH, 1); fromCalendar.compareTo(toCalendar) < 0; fromCalendar.add(Calendar.MONTH, 1)) {
			count++;
		}
		return count;
	}

	private int getDateDifference(Calendar fromCalendar, Calendar toCalendar) {
		long diff = toCalendar.getTimeInMillis() - fromCalendar.getTimeInMillis();
		return (int) (diff / (1000 * 60 * 60 * 24) + 1);
	}

	/**
	 * This method is implemented only for add cc charge for onhold bookings
	 * 
	 * @param totalFareFromPriceBreakdown
	 * @param quotedPriceInfoTO
	 * @param paxCounts
	 * @param otaAirBookRQ
	 * @param travelersPayments
	 * @param selectedCurrencyCode
	 * @param exchangeRateProxy
	 * 
	 * @throws WebservicesException
	 *             , ModuleException
	 */
	private void addCCCharge(BigDecimal totalFareFromPriceBreakdown, QuotedPriceInfoTO quotedPriceInfoTO, int[] paxCounts,
			OTAAirBookRQ otaAirBookRQ, HashMap<String, LCCClientPaymentAssembler> travelersPayments, String selectedCurrencyCode,
			ExchangeRateProxy exchangeRateProxy) throws WebservicesException, ModuleException {

		int totalSementCount = quotedPriceInfoTO.getQuotedSegments().size();
		int totalpayablePaxCount = paxCounts[0] + paxCounts[2];

		List<AirTravelerType> travelers = otaAirBookRQ.getTravelerInfo().getAirTraveler();

		// Splitting CC charges to per pax wise to be added to the payment.
		LinkedList externalChargeSplit = ReservationUtil.getExternalChagersPerPassengers(paxCounts[0] + paxCounts[2],
				ReservationUtil.getCCCharge(totalFareFromPriceBreakdown, totalpayablePaxCount, totalSementCount,
						ChargeRateOperationType.MAKE_ONLY));

		for (AirTravelerType traveler : travelers) {
			// Adult Or Parent or Child
			if (traveler.getPassengerTypeCode()
					.equals(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT))
					|| traveler.getPassengerTypeCode().equals(
							CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD))) {
				LCCClientPaymentAssembler paxPayment = travelersPayments.get(traveler.getTravelerRefNumber().getRPH());

				paxPayment.addExternalCharges((Collection) externalChargeSplit.pop());

				// adding selected external charges
				BookInterlineUtil.addSelectedExternalCharges(paxPayment, traveler.getTravelerRefNumber().getRPH());
			}
		}
	}

	/**
	 * 
	 * @param airBookRS
	 * @param transformTO
	 * @param readRQ
	 * @param loadDataOptions
	 * @param pnrModesDTO
	 * @param userPrincipal
	 * @throws WebservicesException
	 * @throws ModuleException
	 * @throws ParseException
	 */
	public void createAirBookResponse(OTAAirBookRS airBookRS, TransformTO transformTO, OTAReadRQ readRQ,
			AALoadDataOptionsType loadDataOptions, UserPrincipal userPrincipal, boolean isNewBooking)
			throws WebservicesException, ModuleException, ParseException {

		ReservationQueryInterlineUtil interlineUtil = new ReservationQueryInterlineUtil();

		if (transformTO.equals(TransformTO.AirBookRS)) {
			String PNR = readRQ.getReadRequests().getReadRequest().get(0).getUniqueID().getID();
			if (!ReservationApiUtils.isPNRValid(PNR)) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_BOOKING_REFERENCE_INVALID_, "Booking Reference ["
						+ PNR + "] is invalid");
			}
			interlineUtil.getReservation(airBookRS, readRQ, loadDataOptions, userPrincipal, isNewBooking);
		} else {
			// TODO This is for getReservation LIST
		}

	}

	/**
	 * 
	 * @param aaOTAAirTaxInvoiceGetRS
	 * @param aaOTAAirTaxInvoiceGetRQ
	 * @throws WebservicesException
	 * @throws ModuleException
	 * @throws ParseException
	 */
	public void createTaxInvoiceByPnrResponse(AAOTAAirTaxInvoiceGetRS aaOTAAirTaxInvoiceGetRS,
			AAOTAAirTaxInvoiceGetRQ aaOTAAirTaxInvoiceGetRQ) throws WebservicesException, ModuleException, ParseException {
		ReservationQueryInterlineUtil interlineUtil = new ReservationQueryInterlineUtil();

		String PNR = aaOTAAirTaxInvoiceGetRQ.getTaxInvoiceByPnr().getUniqueID().getID();
		if (!ReservationApiUtils.isPNRValid(PNR)) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_BOOKING_REFERENCE_INVALID_, "Booking Reference [" + PNR
					+ "] is invalid");
		}
		interlineUtil.getTaxInvoice(aaOTAAirTaxInvoiceGetRS, aaOTAAirTaxInvoiceGetRQ);
	}

	private boolean isDomesticSegmentExists(List<FlightSegmentTO> flightSegmentTOs) {
		for (FlightSegmentTO fltSeg : flightSegmentTOs) {
			if (fltSeg.isDomesticFlight()) {
				return true;
			}
		}
		return false;
	}

}
