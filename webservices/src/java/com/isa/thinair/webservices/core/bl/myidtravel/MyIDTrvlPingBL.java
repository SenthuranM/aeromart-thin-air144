/**
 * 
 */
package com.isa.thinair.webservices.core.bl.myidtravel;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterPingRequest;
import com.lhsystems.myidtravel.resadapter.service.bos.MyIDTravelResAdapterPingResponse;

/**
 * @author Janaka Padukka
 * 
 */
public class MyIDTrvlPingBL {

	private final static Log log = LogFactory.getLog(MyIDTrvlPingBL.class);

	public static MyIDTravelResAdapterPingResponse execute(MyIDTravelResAdapterPingRequest myIDPingReq) {
		if (log.isDebugEnabled()) {
			log.debug("BEGIN ping(MyIDTravelResAdapterPingRequest)");
		}

		MyIDTravelResAdapterPingResponse myIDPingRes = new MyIDTravelResAdapterPingResponse();
		myIDPingRes.setTime(new SimpleDateFormat("HH:mm:ss").format(new Date()));
		myIDPingRes.setToken(myIDPingReq.getToken());

		if (log.isDebugEnabled())
			log.debug("END ping(MyIDTravelResAdapterPingRequest)");
		return myIDPingRes;
	}

}
