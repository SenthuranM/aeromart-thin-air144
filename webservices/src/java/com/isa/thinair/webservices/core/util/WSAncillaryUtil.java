package com.isa.thinair.webservices.core.util;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityInDTO.LCCAncillaryType;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryAvailabilityOutDTO;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCAncillaryStatus;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCReservationBaggageSummaryTo;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;

public class WSAncillaryUtil {

	/**
	 * Check whether ancillary available in given flight segments
	 * 
	 * @param flightSegmentTOs
	 * @param anciType
	 * @param baggageSummaryTO
	 *            TODO
	 * @return isAnciAvailable
	 * @throws ModuleException
	 */
	public static boolean isAncillaryAvailable(List<FlightSegmentTO> flightSegmentTOs, String anciType,
			LCCReservationBaggageSummaryTo baggageSummaryTO) throws ModuleException {

		boolean isAnciAvailable = false;

		LCCAncillaryAvailabilityInDTO lccAncillaryAvailabilityInDTO = new LCCAncillaryAvailabilityInDTO();
		lccAncillaryAvailabilityInDTO.setFlightDetails(flightSegmentTOs);
		lccAncillaryAvailabilityInDTO.addAncillaryType(anciType);
		lccAncillaryAvailabilityInDTO.setAppIndicator(AppIndicatorEnum.APP_XBE);
		lccAncillaryAvailabilityInDTO.setModifyAncillary(false);
		lccAncillaryAvailabilityInDTO.setOnHold(false);
		lccAncillaryAvailabilityInDTO.setBaggageSummaryTo(baggageSummaryTO);
		lccAncillaryAvailabilityInDTO.setQueryingSystem(SYSTEM.AA);
		lccAncillaryAvailabilityInDTO.setDepartureSegmentCode(ReservationUtil.getDepartureSegmentCode(flightSegmentTOs));

		AnciAvailabilityRS anciAvailabilityRS = WebServicesModuleUtils.getAirproxyAncillaryBD().getAncillaryAvailability(
				lccAncillaryAvailabilityInDTO, null);
		List<LCCAncillaryAvailabilityOutDTO> lccAncillaryAvailabilityOutDTOs = anciAvailabilityRS
				.getLccAncillaryAvailabilityOutDTOs();
		lccAncillaryAvailabilityOutDTOs = checkPrivilegesAndBufferTimes(lccAncillaryAvailabilityOutDTOs, anciType);

		isAnciAvailable = anciActiveInLeastOneSeg(lccAncillaryAvailabilityOutDTOs, anciType);

		return isAnciAvailable;
	}

	public static List<BundledFareDTO> getOndSelectedBundledFares() {
		FareSegChargeTO fareSegChargeTO = (FareSegChargeTO) ThreadLocalData
				.getCurrentTnxParam(Transaction.FARE_SEGMENT_CHARGE_TO);
		if (fareSegChargeTO != null) {
			return fareSegChargeTO.getOndBundledFareDTOs();
		}

		return null;
	}

	public static Map<Integer, Integer> getFlightSegmentWiseBundledServiceTemplate(SelectedFlightDTO selectedFlightDTO,
			String serviceName) {
		Map<Integer, Integer> fltSegWiseBundledTemplate = null;
		List<BundledFareDTO> selectedOndBundledFares = selectedFlightDTO.getSelectedOndBundledFares();

		if (selectedOndBundledFares != null && !selectedOndBundledFares.isEmpty()) {
			Collection<OndFareDTO> ondFareDTOs = selectedFlightDTO.getOndFareDTOs(false);
			fltSegWiseBundledTemplate = new HashMap<Integer, Integer>();

			for (OndFareDTO ondFareDTO : ondFareDTOs) {
				int ondSequence = ondFareDTO.getOndSequence();
				if (selectedOndBundledFares.size() > ondSequence) {
					BundledFareDTO selectedBundledFare = selectedOndBundledFares.get(ondSequence);
					if (selectedBundledFare != null) {
						Integer templateId = selectedBundledFare.getApplicableServiceTemplateId(serviceName);

						for (Integer fltSegId : ondFareDTO.getFlightSegmentIds()) {
							fltSegWiseBundledTemplate.put(fltSegId, templateId);
						}
					}
				}
			}
		}

		return fltSegWiseBundledTemplate;
	}

	public static Map<Integer, Boolean> getFlightSegmentWiseFreeServiceOffer(SelectedFlightDTO selectedFlightDTO,
			String serviceName) {
		Map<Integer, Boolean> fltSegWiseBundledFreeServiceOffer = null;
		List<BundledFareDTO> selectedOndBundledFares = selectedFlightDTO.getSelectedOndBundledFares();

		if (selectedOndBundledFares != null && !selectedOndBundledFares.isEmpty()) {
			Collection<OndFareDTO> ondFareDTOs = selectedFlightDTO.getOndFareDTOs(false);
			fltSegWiseBundledFreeServiceOffer = new HashMap<Integer, Boolean>();

			for (OndFareDTO ondFareDTO : ondFareDTOs) {
				int ondSequence = ondFareDTO.getOndSequence();
				if (selectedOndBundledFares.size() > ondSequence) {
					BundledFareDTO selectedBundledFare = selectedOndBundledFares.get(ondSequence);
					if (selectedBundledFare != null) {
						for (Integer fltSegId : ondFareDTO.getFlightSegmentIds()) {
							fltSegWiseBundledFreeServiceOffer.put(fltSegId, selectedBundledFare.isServiceIncluded(serviceName));
						}
					}
				}
			}
		}

		return fltSegWiseBundledFreeServiceOffer;
	}

	private static boolean anciActiveInLeastOneSeg(List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO, String anciType) {
		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
					if ((ancillaryStatus.isAvailable())
							&& (ancillaryStatus.getAncillaryType().getAncillaryType().equalsIgnoreCase(anciType))) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private static List<LCCAncillaryAvailabilityOutDTO> checkPrivilegesAndBufferTimes(
			List<LCCAncillaryAvailabilityOutDTO> availabilityOutDTO, String anciType) {
		if (availabilityOutDTO != null) {
			for (LCCAncillaryAvailabilityOutDTO ancillaryAvailabilityOutDTO : availabilityOutDTO) {
				Date departureDate = ancillaryAvailabilityOutDTO.getFlightSegmentTO().getDepartureDateTime();
				String airportCode = ancillaryAvailabilityOutDTO.getFlightSegmentTO().getSegmentCode().split("/")[0];
				Date depatureDateZulu = null;
				try {
					depatureDateZulu = DateUtil.getZuluDateTime(departureDate, airportCode);
				} catch (ModuleException e) {
					depatureDateZulu = departureDate;
				}
				for (LCCAncillaryStatus ancillaryStatus : ancillaryAvailabilityOutDTO.getAncillaryStatusList()) {
					ancillaryStatus.setAvailable(ancillaryStatus.isAvailable() && isEnableService(anciType, depatureDateZulu));

				}
			}
		}
		return availabilityOutDTO;
	}

	private static boolean isEnableService(String anciType, Date departDate) {

		long diffMils = 3600000;

		Calendar currentTime = Calendar.getInstance();

		if (anciType.equalsIgnoreCase(LCCAncillaryType.MEALS)) {
			diffMils = AppSysParamsUtil.getTimeInMillis(AppSysParamsUtil.getMealCutOverTime());
		}
		if (departDate.getTime() - diffMils > currentTime.getTimeInMillis()) {
			return true;
		} else {
			return false;
		}

	}

}
