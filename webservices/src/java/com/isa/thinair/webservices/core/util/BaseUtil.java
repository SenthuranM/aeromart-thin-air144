/*
 * ==============================================================================
 *
 * Copyright (c) 2006/07 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;
import java.util.Map;

import org.opentravel.ota._2003._05.IOTACodeTables;

import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;

public abstract class BaseUtil {

	public BaseUtil() {
	}
	
	@SuppressWarnings("unchecked")
	public static BigDecimal getUserDefinedSSRChargeAmountFromTnx(String ssrCode, boolean skipValidation)
			throws WebservicesException {
		Map<String, BigDecimal> userDefinedSsrPriceMap = (Map<String, BigDecimal>) ThreadLocalData
				.getCurrentTnxParam(Transaction.USER_DEFINED_SSR_PRICE);
		BigDecimal amount = null;
		if (userDefinedSsrPriceMap != null && !userDefinedSsrPriceMap.isEmpty() && !StringUtil.isNullOrEmpty(ssrCode)
				&& userDefinedSsrPriceMap.get(ssrCode) != null) {
			amount = userDefinedSsrPriceMap.get(ssrCode);
		}

		if (!skipValidation && amount == null) {
			throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_SSR_CHARGE_AMOUNT_IS_NOT_DEFINED,
					"User defined SSR Charge Amount is not defined for requested code [" + ssrCode + "].");
		}

		return amount;
	}
}
