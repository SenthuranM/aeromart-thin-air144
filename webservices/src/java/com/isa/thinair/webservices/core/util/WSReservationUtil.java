/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.IOTACodeTables;

import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonCreditCardPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientCashPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientOnAccountPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaxCreditPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPaymentAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.AgentCreditInfo;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CashPaymentInfo;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditInfo;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO.PAYMENT_REF_TYPE;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.VoucherPaymentInfo;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO.PAYMENT_REF_TYPE;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.constants.CommonsConstants.WSConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.lccclient.api.util.PaxTypeUtils;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.v2.reservation.ExternalChargesMediator;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;

public class WSReservationUtil {

	private static final Log log = LogFactory.getLog(WSReservationUtil.class);

	/**
	 * Returns the Reservation
	 * 
	 * @param pnr
	 * @param trackInfoDTO
	 * @param loadFares
	 * @param loadLastUserNote
	 * @param loadLocalTimes
	 * @param loadOndChargesView
	 * @param loadPaxAvaBalance
	 * @param loadSegView
	 * @param loadSegViewBookingTypes
	 * @return
	 * @throws ModuleException
	 */
	public static Reservation getReservation(String pnr, TrackInfoDTO trackInfoDTO, boolean loadFares, boolean loadLastUserNote,
			boolean loadLocalTimes, boolean loadOndChargesView, boolean loadPaxAvaBalance, boolean loadSegView,
			boolean loadSegViewBookingTypes) throws ModuleException {

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(loadFares);
		pnrModesDTO.setLoadLastUserNote(loadLastUserNote);
		pnrModesDTO.setLoadLocalTimes(loadLocalTimes);
		pnrModesDTO.setLoadOndChargesView(loadOndChargesView);
		pnrModesDTO.setLoadPaxAvaBalance(loadPaxAvaBalance);
		pnrModesDTO.setLoadSegViewBookingTypes(loadSegViewBookingTypes);

		if (pnrModesDTO.isLoadOndChargesView()) {
			pnrModesDTO.setLoadSegView(true);
		} else {
			pnrModesDTO.setLoadSegView(loadSegView);
		}

		return WebServicesModuleUtils.getReservationBD().getReservation(pnrModesDTO, trackInfoDTO);
	}

	/**
	 * Method to authorize a functionality. If not authorized, throw WebservicesException.
	 * 
	 * @param operation
	 *            TODO
	 * @param privilegeKey
	 * 
	 * @throws WebservicesException
	 */
	public static void authorize(Collection<String> privilegeKeys, String operation, String... privilegeKey)
			throws WebservicesException {
		for (int i = 0; i < privilegeKey.length; i++) {
			if (!AuthorizationUtil.hasPrivileges(privilegeKeys, privilegeKey[i])) {
				UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
				log.warn("Authorizatin failure [userId=" + principal.getUserId() + ",failedPrivilege=" + privilegeKey[i] + "]");

				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR,
						"Unauthorized Operation [" + (operation == null ? "" : operation) + "]");
			}
		}
	}

	/**
	 * Method to authorize segment modification. If not authorized, throw WebservicesException.
	 * 
	 * @param privilegeKeys
	 * @param segmentDateModificationPrivilege
	 * @param segmentRouteModificationPrivilege
	 * 
	 * @throws WebservicesException
	 */
	public static void auhtorizeModifySegment(Collection<String> privilegeKeys, String segmentDateModificationPrivilege,
			String segmentRouteModificationPrivilege) throws WebservicesException {
		boolean isDateModificationAuthorized = false;
		if (AuthorizationUtil.hasPrivilege(privilegeKeys, segmentDateModificationPrivilege)) {
			isDateModificationAuthorized = true;
		}

		boolean isRouteModificationAuthorized = false;
		if (AuthorizationUtil.hasPrivilege(privilegeKeys, segmentRouteModificationPrivilege)) {
			isRouteModificationAuthorized = true;
		}
		if (!isDateModificationAuthorized && !isRouteModificationAuthorized) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR, "Unauthorized Operation");
		}
	}

	/**
	 * Authorizes specified mode of payment. If payment mode is not authorized to use, then throw WebservicesException.
	 * 
	 * @param privilegeKeys
	 * @param payments
	 * @param requestingAgencyCode
	 * @throws WebservicesException
	 */
	public static void authorizePayment(Collection<String> privilegeKeys, IPayment payments, String requestingAgencyCode,
			Collection<Integer> existingPnrPaxIds) throws WebservicesException {
		if (payments != null && ((PaymentAssembler) payments).getPayments() != null
				&& ((PaymentAssembler) payments).getPayments().size() > 0) {
			for (Object paymentObj : ((PaymentAssembler) payments).getPayments()) {
				if (paymentObj instanceof AgentCreditInfo) {
					if (!((AgentCreditInfo) paymentObj).getAgentCode().equals(requestingAgencyCode)) {

						if (WebServicesModuleUtils.getWebServiceDAO().isReportingAgency(requestingAgencyCode,
								((AgentCreditInfo) paymentObj).getAgentCode())) { // check if payment reporting agency
							authorize(privilegeKeys, "Agent Credit Payment Rpt",
									PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_ONACCOUNT_PAYMNETS_REPORTING);
						} else {
							authorize(privilegeKeys, "Agent Credit Payment Any",
									PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_ONACCOUNT_PAYMNETS_ANY);
						}
					}
					if (((AgentCreditInfo) paymentObj).getPaymentReferenceTO() != null
							&& ((AgentCreditInfo) paymentObj).getPaymentReferenceTO().getPaymentRefType() != null
							&& Agent.PAYMENT_MODE_BSP
									.equals(((AgentCreditInfo) paymentObj).getPaymentReferenceTO().getPaymentRefType().name())) {
						authorize(privilegeKeys, "BSP Payment", PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_BSP_PAYMNETS);
					} else {
						authorize(privilegeKeys, "Agent Credit Payment",
								PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_ONACCOUNT_PAYMNETS);
					}
				} else if (paymentObj instanceof CashPaymentInfo) {
					authorize(privilegeKeys, "Cash Payment", PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_CASH_PAYMNETS);
				} else if (paymentObj instanceof CardPaymentInfo) {
					authorize(privilegeKeys, "Credit Card Payment",
							PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_CREDIT_CARD_PAYMNETS);
				} else if (paymentObj instanceof PaxCreditInfo) {
					if (existingPnrPaxIds != null) {
						// TODO - Authorize any credit transfer
					}
					authorize(privilegeKeys, "Pax Credit Payment", PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_CREDIT_PAYMNETS);
				} else if (paymentObj instanceof VoucherPaymentInfo) {

					authorize(privilegeKeys, "Voucher Payment", PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_VOUCHER_PAYMNETS);
				}
			}
		}
	}

	/**
	 * Authorizes specified mode of payment. If payment mode is not authorized to use, then throw WebservicesException.
	 * 
	 * @param privilegeKeys
	 * @param payments
	 * @param requestingAgencyCode
	 * @throws WebservicesException
	 */
	public static void authorizePayment(Collection<String> privilegeKeys, LCCClientPaymentAssembler payments,
			String requestingAgencyCode) throws WebservicesException {

		if (payments != null && payments.getPayments() != null && !payments.getPayments().isEmpty()) {

			for (Object paymentObj : payments.getPayments()) {
				if (paymentObj instanceof LCCClientOnAccountPaymentInfo) {

					if (!((LCCClientOnAccountPaymentInfo) paymentObj).getAgentCode().equals(requestingAgencyCode)) {

						if (WebServicesModuleUtils.getWebServiceDAO().isReportingAgency(requestingAgencyCode,
								((LCCClientOnAccountPaymentInfo) paymentObj).getAgentCode())) { // check if payment
																								// reporting agency
							authorize(privilegeKeys, "Agent Credit Payment Rpt",
									PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_ONACCOUNT_PAYMNETS_REPORTING);
						} else {
							authorize(privilegeKeys, "Agent Credit Payment Any",
									PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_ONACCOUNT_PAYMNETS_ANY);
						}
					}
					if (((LCCClientOnAccountPaymentInfo) paymentObj).getPaymentMethod() != null
							&& Agent.PAYMENT_MODE_BSP.equals(((LCCClientOnAccountPaymentInfo) paymentObj).getPaymentMethod())) {
						authorize(privilegeKeys, "BSP Payment", PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_BSP_PAYMNETS);
					} else {
						authorize(privilegeKeys, "Agent Credit Payment",
								PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_ONACCOUNT_PAYMNETS);
					}

				} else if (paymentObj instanceof LCCClientCashPaymentInfo) {
					authorize(privilegeKeys, "Cash Payment", PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_CASH_PAYMNETS);
				} else if (paymentObj instanceof CommonCreditCardPaymentInfo) {
					authorize(privilegeKeys, "Credit Card Payment",
							PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_CREDIT_CARD_PAYMNETS);
				} else if (paymentObj instanceof LCCClientPaxCreditPaymentInfo) {
					authorize(privilegeKeys, "Pax Credit Payment", PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_CREDIT_PAYMNETS);
				} else if (paymentObj instanceof LCCClientPaxCreditPaymentInfo) {
					authorize(privilegeKeys, "Voucher Payment", PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_VOUCHER_PAYMNETS);
				}
			}
		}
	}

	/**
	 * Returns the PayCurrencyDTO information for the given agent
	 * 
	 * @param agentCode
	 * @param exchangeRateProxy
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	public static PayCurrencyDTO getPayCurrencyDTO(String agentCode, ExchangeRateProxy exchangeRateProxy)
			throws WebservicesException, ModuleException {

		if (AppSysParamsUtil.isStorePaymentsInAgentCurrencyEnabled()) {
			UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
			String integrationAgentCode = PlatformUtiltiies.nullHandler(principal.getAgentCode());
			agentCode = PlatformUtiltiies.nullHandler(agentCode);

			Agent agent = null;
			if (!"".equals(agentCode) && agentCode != null) {
				agent = loadTravelAgent(agentCode);
			}

			if ("".equals(agentCode) || agentCode == null || agent == null) {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_COMPANY_OR_TRAVEL_AGENT_ADDRESS_REQUIRED,
						"Invalid payment agency");
			}

			CommonMasterBD commonMasterBD = WebServicesModuleUtils.getCommonMasterBD();
			Currency currency = commonMasterBD.getCurrency(agent.getCurrencyCode());

			if (currency == null || Currency.STATUS_INACTIVE.equals(currency.getStatus())) {
				log.warn("Currency Code failure [AgentCode =" + agentCode + ", Integration AgentCode =" + integrationAgentCode
						+ "]");
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_COMPANY_OR_TRAVEL_AGENT_ADDRESS_REQUIRED,
						"Agent currency is inactive");
			}

			String baseCurrCode = AppSysParamsUtil.getBaseCurrency();
			BigDecimal multiplyingExchangeRate = exchangeRateProxy.getExchangeRate(baseCurrCode, currency.getCurrencyCode());

			return new PayCurrencyDTO(currency.getCurrencyCode(), multiplyingExchangeRate, currency.getBoundryValue(),
					currency.getBreakPoint());

		} else {
			return null;
		}
	}

	/**
	 * loads a travel agent
	 * 
	 * @param agentCode
	 * @return
	 * @throws ModuleException
	 */
	private static Agent loadTravelAgent(String agentCode) throws ModuleException {
		Collection<String> agentCodes = new ArrayList<String>();
		agentCodes.add(agentCode);
		Collection<Agent> colAgents = WebServicesModuleUtils.getTravelAgentBD().getAgents(agentCodes);
		return (Agent) BeanUtils.getFirstElement(colAgents);
	}

	public static boolean isHoldAllowed(Collection<OndFareDTO> ondFareDTOCol) {
		boolean isOnholdAllowed = true;
		outer: for (Iterator collFaresIt = ondFareDTOCol.iterator(); collFaresIt.hasNext();) {
			OndFareDTO ondFareDTO = (OndFareDTO) collFaresIt.next();
			for (Iterator segIt = ondFareDTO.getSegmentSeatDistsDTO().iterator(); segIt.hasNext();) {
				SegmentSeatDistsDTO segSeatDistsDTO = (SegmentSeatDistsDTO) segIt.next();
				for (Iterator seatsIt = segSeatDistsDTO.getSeatDistribution().iterator(); seatsIt.hasNext();) {
					if (((SeatDistribution) seatsIt.next()).isOnholdRestricted()) {
						isOnholdAllowed = false;
						break outer;
					}
				}
			}
		}
		return isOnholdAllowed;
	}

	/**
	 * 
	 * @param priceInfoTO
	 * @return
	 */
	public static boolean isHoldAllowed(PriceInfoTO priceInfoTO) {
		boolean isOnholdAllowed = true;

		FareTypeTO fareTypeTO = priceInfoTO.getFareTypeTO();
		if (fareTypeTO != null && fareTypeTO.isOnHoldRestricted()) {
			isOnholdAllowed = false;
		}
		return isOnholdAllowed;
	}

	public static BigDecimal getAmountInSpecifiedCurrency(BigDecimal amountInBase, String currencyCode,
			ExchangeRateProxy exchangeRateProxy) throws ModuleException {
		String baseCurrCode = AppSysParamsUtil.getBaseCurrency();
		BigDecimal amountInSpecifiedCur = null;
		if (baseCurrCode.equals(currencyCode)) {
			amountInSpecifiedCur = amountInBase;
		} else {
			CommonMasterBD commonMasterBD = WebServicesModuleUtils.getCommonMasterBD();
			Currency currency = commonMasterBD.getCurrency(currencyCode);

			if (currency != null && !Currency.STATUS_INACTIVE.equals(currency.getStatus())) {
				BigDecimal multiplyingExchangeRate = exchangeRateProxy.getExchangeRate(baseCurrCode, currency.getCurrencyCode());
				amountInSpecifiedCur = AccelAeroRounderPolicy.convertAndRound(amountInBase, multiplyingExchangeRate,
						currency.getBoundryValue(), currency.getBreakPoint());
			}
		}

		return amountInSpecifiedCur;
	}

	public static BigDecimal getAmountInSpecifiedCurrencyInSpecifiedTime(BigDecimal amountInBase, String currencyCode,
			ExchangeRateProxy exchangeRateProxy) throws ModuleException {
		String baseCurrCode = AppSysParamsUtil.getBaseCurrency();
		BigDecimal amountInSpecifiedCur = null;
		if (baseCurrCode.equals(currencyCode)) {
			amountInSpecifiedCur = amountInBase;
		} else {
			CommonMasterBD commonMasterBD = WebServicesModuleUtils.getCommonMasterBD();
			Currency currency = commonMasterBD.getCurrency(currencyCode);

			if (currency != null && !Currency.STATUS_INACTIVE.equals(currency.getStatus())) {
				BigDecimal multiplyingExchangeRate = exchangeRateProxy.getExchangeRate(baseCurrCode, currency.getCurrencyCode());
				amountInSpecifiedCur = AccelAeroRounderPolicy.convertAndRound(amountInBase, multiplyingExchangeRate,
						currency.getBoundryValue(), currency.getBreakPoint());
			}
		}

		return amountInSpecifiedCur;
	}

	/**
	 * Validation for booking category
	 * 
	 * @param privilegeKeys
	 * @param bookingCategoryCode
	 * @param privilegeType
	 *            TODO
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	public static void authorizeBookingCategory(Collection<String> privilegeKeys, String bookingCategoryCode, int privilegeType)
			throws ModuleException, WebservicesException {

		Collection<String[]> bookingCategories = new ArrayList<String[]>();
		bookingCategories = SelectListGenerator.getAllBookingCategories();

		String[] allowedBookingCategory = null;
		for (String[] bookingCategory : bookingCategories) {
			if (bookingCategory[0].equals(bookingCategoryCode)) {
				allowedBookingCategory = bookingCategory;
				break;
			}
		}

		if (allowedBookingCategory != null) {
			switch (privilegeType) {
			case WSConstants.ALLOW_MAKE_RESERVATION:
				authorize(privilegeKeys, "Unauthorized booking category for make reservation", allowedBookingCategory[2]);
				break;
			case WSConstants.ALLOW_FIND_RESERVATION:
				authorize(privilegeKeys, "Unauthorized boooking category for retrieve reservation", allowedBookingCategory[3]);
				break;
			case WSConstants.ALLOW_MODIFY_RESERVATION:
				authorize(privilegeKeys, "Unauthorized booking category for modification", allowedBookingCategory[2],
						allowedBookingCategory[3]);
				break;
			}
		} else {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_AUTHORIZATION_ERROR,
					"Unauthorized Operation [No active booking category for the requested]");
		}

	}

	public static ExternalChgDTO getCCChargeAmount(BigDecimal totalTicketPriceExcludingCC, int noOfPayablePax, int noOfSegments,
			int chargeRatetype) throws ModuleException {
		Collection colEXTERNAL_CHARGES = new ArrayList();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.CREDIT_CARD);
		Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = WebServicesModuleUtils.getReservationBD()
				.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, chargeRatetype);
		ExternalChgDTO externalChgDTO = extChgMap.get(EXTERNAL_CHARGES.CREDIT_CARD);
		String agentCode = ThreadLocalData.getCurrentUserPrincipal().getAgentCode();
		if (externalChgDTO.isRatioValueInPercentage()) {
			externalChgDTO.calculateAmount(totalTicketPriceExcludingCC);
		} else {
			externalChgDTO.calculateAmount(noOfPayablePax, noOfSegments);
		}

		if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
				&& !AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.WS, agentCode)) {
			externalChgDTO.setAmount(BigDecimal.ZERO);
		}

		return externalChgDTO;
	}

	public static ExternalChgDTO getJNOtherCharge(Set<ServiceTaxContainer> serviceTaxContainers,
			LCCClientPaymentAssembler paymentAssembler, BigDecimal ccChargeAmount, int noOfPayablePax, int noOfSegments,
			int chargeRatetype) throws ModuleException {

		ExternalChgDTO serviceTAxDTO = null;

		if (!serviceTaxContainers.isEmpty()) {
			for (ServiceTaxContainer serviceTaxContainer : serviceTaxContainers) {
				if (serviceTaxContainer != null && serviceTaxContainer.isTaxApplicable()) {
					Set<EXTERNAL_CHARGES> taxableExternalCharges = serviceTaxContainer.getTaxableExternalCharges();
					if (taxableExternalCharges != null && !taxableExternalCharges.isEmpty()) {
						BigDecimal taxableTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
						for (EXTERNAL_CHARGES extChg : taxableExternalCharges) {
							Collection<LCCClientExternalChgDTO> externalChgDTOs = paymentAssembler
									.getPerPaxExternalCharges(extChg);
							if (externalChgDTOs != null && !externalChgDTOs.isEmpty()) {
								for (LCCClientExternalChgDTO externalChgDTO : externalChgDTOs) {
									taxableTotal = AccelAeroCalculator.add(taxableTotal, externalChgDTO.getAmount());
								}
							}
						}

						if (AccelAeroCalculator.isGreaterThan(taxableTotal, AccelAeroCalculator.getDefaultBigDecimalZero())) {
							BigDecimal taxAmount = AccelAeroCalculator.multiplyDefaultScale(taxableTotal,
									serviceTaxContainer.getTaxRatio());
							serviceTAxDTO = getJNTaxOther(taxAmount);
							((ServiceTaxExtChgDTO) serviceTAxDTO)
									.setFlightRefNumber(serviceTaxContainer.getTaxApplyingFlightRefNumber());
							return serviceTAxDTO;
						}
					}
				}
			}
		}

		return serviceTAxDTO;
	}

	public static ExternalChgDTO getJNTaxOther(BigDecimal taxAmount) throws ModuleException {
		Collection colEXTERNAL_CHARGES = new ArrayList();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.JN_OTHER);

		ReservationBD reservationBD = WPModuleUtils.getReservationBD();
		Map externalChargesMap = reservationBD.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, null);

		ExternalChgDTO externalChgDTO = (ExternalChgDTO) externalChargesMap.get(EXTERNAL_CHARGES.JN_OTHER);
		if (externalChgDTO == null) {
			String msg = "Error loading JN Tax Other charge info[null]";
			throw new RuntimeException(msg);
		} else {
			externalChgDTO = (ServiceTaxExtChgDTO) externalChgDTO.clone();
			externalChgDTO.setAmount(taxAmount);
		}
		return externalChgDTO;
	}

	public static Map<Integer, List<LCCClientExternalChgDTO>> getPassengerExtChgMap(
			Map<String, List<LCCClientExternalChgDTO>> quotedExternalCharges, Set<LCCClientReservationPax> lccPassengers) {
		Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap = new HashMap<Integer, List<LCCClientExternalChgDTO>>();
		if (quotedExternalCharges != null && quotedExternalCharges.size() > 0) {
			for (LCCClientReservationPax lccClientReservationPax : lccPassengers) {
				String trimmedTravRef = getTrimmedTravelerRef(lccClientReservationPax.getTravelerRefNumber());
				if (!trimmedTravRef.equals("") && quotedExternalCharges.get(trimmedTravRef) != null) {
					paxExtChgMap.put(lccClientReservationPax.getPaxSequence(), quotedExternalCharges.get(trimmedTravRef));
				}
			}
		}

		return paxExtChgMap;
	}

	private static String getTrimmedTravelerRef(String travelerRef) {
		travelerRef = travelerRef.replace(" ", "");
		String[] trrefArray = travelerRef.split(",");

		String DELIM = "$";

		for (String traveler : trrefArray) {
			if (traveler.indexOf("$") != -1 && traveler.indexOf("|") != -1) {
				String[] tArray = StringUtils.split(traveler, "$");
				return StringUtils.split(tArray[0], "|")[1];
			}
		}
		return "";
	}

	@SuppressWarnings("unchecked")
	public static Map<String, LCCClientPaymentAssembler> getPassengerPayments(ReservationBalanceTO reservationBalance,
			LCCClientReservation lccClientReservation, PaymentAssembler reservationPayment, int newOndSize,
			Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap, String loggedInAgencyCode)
			throws ModuleException, WebservicesException {
		Map<String, LCCClientPaymentAssembler> paymentMap = new HashMap<String, LCCClientPaymentAssembler>();
		Collection<LCCClientReservationPax> paxList = lccClientReservation.getPassengers();
		// Consider only single payment
		Object resPayment = reservationPayment.getPayments().iterator().next();

		int noOfAdults = 0;
		for (LCCClientPassengerSummaryTO paxSummaryTo : reservationBalance.getPassengerSummaryList()) {
			if (!paxSummaryTo.getPaxType().equals(PaxTypeTO.INFANT)) {
				noOfAdults++;
			}
		}

		BigDecimal calculatedPaymentAmount = reservationBalance.getTotalAmountDue();
		PayCurrencyDTO cardPayCurrencyDTO = null;
		PayCurrencyDTO onAccPayCurrencyDTO = null;

		Map<Integer, LCCClientReservationPax> resPaxMap = new HashMap<Integer, LCCClientReservationPax>();
		for (LCCClientReservationPax resPax : paxList) {
			resPaxMap.put(resPax.getPaxSequence(), resPax);
		}

		// ReservationUtil.getApplicableServiceTax(bookingShoppingCart, EXTERNAL_CHARGES.JN_OTHER);

		String agentCode = ThreadLocalData.getCurrentUserPrincipal().getAgentCode();
		
		Map<EXTERNAL_CHARGES, ExternalChgDTO> extChgMap = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();
		ExternalChargesMediator creditExtChargesMediator = new ExternalChargesMediator(null, extChgMap, true, true);
		if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
				&& !AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.WS, agentCode)) {
			creditExtChargesMediator = new ExternalChargesMediator(null, extChgMap, false, true);
		}
		LinkedList perPaxCreditExternalCharges = creditExtChargesMediator.getExternalChargesForABalancePayment(noOfAdults, 0);

		IPGIdentificationParamsDTO ipgIdentificationParamsDTO = null;
		Date paymentTimestamp = new Date();

		for (LCCClientPassengerSummaryTO paxSummary : reservationBalance.getPassengerSummaryList()) {
			if (!paxSummary.getPaxType().equals(PaxTypeTO.INFANT)) {
				calculatedPaymentAmount = reservationBalance.getTotalAmountDue();
				BigDecimal amountDueWithExt = AccelAeroCalculator.add(paxSummary.getTotalCreditAmount().negate(),
						paxSummary.getTotalAmountDue());
				LCCClientPaymentAssembler paymentAssembler = new LCCClientPaymentAssembler();

				BigDecimal amountWithoutExt = amountDueWithExt;

				if (resPayment instanceof AgentCreditInfo) {
					if (!(AppSysParamsUtil.isAdminFeeRegulationEnabled()
							&& AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.WS, agentCode))) {
						if (reservationBalance.getTotalAmountDue().doubleValue() != reservationPayment.getTotalPayAmount()
								.doubleValue()) {
							throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_PAYMENT_AMOUNT,
									"Expected [" + reservationBalance.getTotalAmountDue() + "] received ["
											+ reservationPayment.getTotalPayAmount() + "]");
						}

						List<LCCClientExternalChgDTO> extCharges = new ArrayList<>();

						if (perPaxCreditExternalCharges.size() > 0) {
							extCharges.addAll(
									converToProxyExtCharges((Collection<ExternalChgDTO>) perPaxCreditExternalCharges.pop()));
						}

						if (paxExtChgMap != null && paxExtChgMap.size() > 0) {
							if (paxExtChgMap.get(PaxTypeUtils.getPaxSeq(paxSummary.getTravelerRefNumber())) != null) {
								extCharges.addAll(paxExtChgMap.get(PaxTypeUtils.getPaxSeq(paxSummary.getTravelerRefNumber())));
							}
						}

						paymentAssembler.getPerPaxExternalCharges().addAll(extCharges);
						BigDecimal extChgTotal = getTotalExtCharge(extCharges);
						amountWithoutExt = AccelAeroCalculator.subtract(amountDueWithExt, extChgTotal);

						AgentCreditInfo agentCreditInfo = (AgentCreditInfo) resPayment;
						onAccPayCurrencyDTO = agentCreditInfo.getPayCurrencyDTO();
						String paymentMethod = null;
						if (PAYMENT_REF_TYPE.BSP
								.equals(((AgentCreditInfo) resPayment).getPaymentReferenceTO().getPaymentRefType())) {
							paymentMethod = Agent.PAYMENT_MODE_BSP;
						} else {
							paymentMethod = Agent.PAYMENT_MODE_ONACCOUNT;
						}
						paymentAssembler.addAgentCreditPayment(agentCreditInfo.getAgentCode(), amountWithoutExt,
								onAccPayCurrencyDTO, paymentTimestamp, agentCreditInfo.getPaymentReferenceTO().getPaymentRef(),
								agentCreditInfo.getPaymentReferenceTO().getActualPaymentMode(), paymentMethod, null);
					} else {
						ExternalChgDTO ccCharge = getCCChargeAmount(reservationBalance.getTotalAmountDue(), noOfAdults,
								newOndSize, ChargeRateOperationType.ANY);
						BigDecimal ccFee = ccCharge.getAmount();
						ccFee = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.CREDIT_CARD, ccFee);

						calculatedPaymentAmount = AccelAeroCalculator.add(calculatedPaymentAmount, ccFee);

						if (calculatedPaymentAmount.doubleValue() != reservationPayment.getTotalPayAmount().doubleValue()) {
							throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_PAYMENT_AMOUNT,
									"Expected [" + calculatedPaymentAmount + "] received ["
											+ reservationPayment.getTotalPayAmount() + "]");
						}

						extChgMap.put(ccCharge.getExternalChargesEnum(), ccCharge);

						Set<ServiceTaxContainer> serviceTaxContainers = (Set<ServiceTaxContainer>) ThreadLocalData
								.getCurrentTnxParam(Transaction.APPLICABLE_SERVICE_TAXES);

						creditExtChargesMediator = new ExternalChargesMediator(null, extChgMap, true, true);
						creditExtChargesMediator.setCalculateJNTaxForCCCharge(true);
						perPaxCreditExternalCharges = creditExtChargesMediator.getExternalChargesForABalancePayment(noOfAdults,
								0);

						List<LCCClientExternalChgDTO> extCharges = converToProxyExtCharges(
								(Collection<ExternalChgDTO>) perPaxCreditExternalCharges.pop());

						if (paxExtChgMap != null && paxExtChgMap.size() > 0) {
							if (paxExtChgMap.get(PaxTypeUtils.getPaxSeq(paxSummary.getTravelerRefNumber())) != null) {
								extCharges.addAll(paxExtChgMap.get(PaxTypeUtils.getPaxSeq(paxSummary.getTravelerRefNumber())));
							}
						}

						paymentAssembler.getPerPaxExternalCharges().addAll(extCharges);

						ExternalChgDTO jnOtherCharge = getJNOtherCharge(serviceTaxContainers, paymentAssembler,
								ccCharge.getAmount(), noOfAdults, newOndSize, ChargeRateOperationType.ANY);
						if (jnOtherCharge != null) {
							LCCClientExternalChgDTO lccExternalChgDTO = new LCCClientExternalChgDTO(jnOtherCharge);
							extCharges.add(lccExternalChgDTO);
							paymentAssembler.getPerPaxExternalCharges().add(lccExternalChgDTO);
						}

						BigDecimal ccChgTotal = getTotalExtCharge(extCharges);
						amountDueWithExt = AccelAeroCalculator.add(amountDueWithExt, ccChgTotal);

						paxExtChgMap.put(PaxTypeUtils.getPaxSeq(paxSummary.getTravelerRefNumber()), extCharges);

						BigDecimal extChgTotal = getTotalExtCharge(extCharges);
						amountWithoutExt = AccelAeroCalculator.subtract(amountDueWithExt, extChgTotal);

						AgentCreditInfo agentCreditInfo = (AgentCreditInfo) resPayment;
						onAccPayCurrencyDTO = agentCreditInfo.getPayCurrencyDTO();
						String paymentMethod = null;
						if (PAYMENT_REF_TYPE.BSP
								.equals(((AgentCreditInfo) resPayment).getPaymentReferenceTO().getPaymentRefType())) {
							paymentMethod = Agent.PAYMENT_MODE_BSP;
						} else {
							paymentMethod = Agent.PAYMENT_MODE_ONACCOUNT;
						}
						paymentAssembler.addAgentCreditPayment(agentCreditInfo.getAgentCode(), amountWithoutExt,
								onAccPayCurrencyDTO, paymentTimestamp, agentCreditInfo.getPaymentReferenceTO().getPaymentRef(),
								agentCreditInfo.getPaymentReferenceTO().getActualPaymentMode(), paymentMethod, null);
					}
				} else if (resPayment instanceof CardPaymentInfo) {
					CardPaymentInfo cardPaymentInfo = (CardPaymentInfo) resPayment;
					int ipgId = cardPaymentInfo.getIpgIdentificationParamsDTO().getIpgId();
					ExternalChgDTO ccCharge = getCCChargeAmount(reservationBalance.getTotalAmountDue(), noOfAdults, newOndSize,
							ChargeRateOperationType.ANY);
					BigDecimal ccFee = ccCharge.getAmount();
					ccFee = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.CREDIT_CARD, ccFee);

					calculatedPaymentAmount = AccelAeroCalculator.add(calculatedPaymentAmount, ccFee);

					if (calculatedPaymentAmount.doubleValue() != reservationPayment.getTotalPayAmount().doubleValue()) {
						throw new WebservicesException(IOTACodeTables.AccelaeroErrorCodes_AAE_INVALID_PAYMENT_AMOUNT, "Expected ["
								+ calculatedPaymentAmount + "] received [" + reservationPayment.getTotalPayAmount() + "]");
					}

					extChgMap.put(ccCharge.getExternalChargesEnum(), ccCharge);

					Set<ServiceTaxContainer> serviceTaxContainers = (Set<ServiceTaxContainer>) ThreadLocalData
							.getCurrentTnxParam(Transaction.APPLICABLE_SERVICE_TAXES);

					creditExtChargesMediator = new ExternalChargesMediator(null, extChgMap, true, true);
					creditExtChargesMediator.setCalculateJNTaxForCCCharge(true);

					if (AppSysParamsUtil.isAdminFeeRegulationEnabled()
							&& !AppSysParamsUtil.isAdminFeeChannelWiseConfigured(ApplicationEngine.WS, agentCode)) {
						creditExtChargesMediator = new ExternalChargesMediator(null, extChgMap, false, true);
						creditExtChargesMediator.setCalculateJNTaxForCCCharge(false);
					}

					perPaxCreditExternalCharges = creditExtChargesMediator.getExternalChargesForABalancePayment(noOfAdults, 0);

					List<LCCClientExternalChgDTO> extCharges = converToProxyExtCharges(
							(Collection<ExternalChgDTO>) perPaxCreditExternalCharges.pop());

					if (paxExtChgMap != null && paxExtChgMap.size() > 0) {
						if (paxExtChgMap.get(PaxTypeUtils.getPaxSeq(paxSummary.getTravelerRefNumber())) != null) {
							extCharges.addAll(paxExtChgMap.get(PaxTypeUtils.getPaxSeq(paxSummary.getTravelerRefNumber())));
						}
					}

					paymentAssembler.getPerPaxExternalCharges().addAll(extCharges);
					BigDecimal extChgTotal = getTotalExtCharge(extCharges);
					amountWithoutExt = AccelAeroCalculator.subtract(amountDueWithExt, extChgTotal);

					ExternalChgDTO jnOtherCharge = getJNOtherCharge(serviceTaxContainers, paymentAssembler, ccCharge.getAmount(),
							noOfAdults, newOndSize, ChargeRateOperationType.ANY);
					if (jnOtherCharge != null) {
						LCCClientExternalChgDTO lccExternalChgDTO = new LCCClientExternalChgDTO(jnOtherCharge);
						extCharges.add(lccExternalChgDTO);
						paymentAssembler.getPerPaxExternalCharges().add(lccExternalChgDTO);
					}

					extChgTotal = getTotalExtCharge(extCharges);
					amountDueWithExt = AccelAeroCalculator.add(amountWithoutExt, extChgTotal);

					paxExtChgMap.put(PaxTypeUtils.getPaxSeq(paxSummary.getTravelerRefNumber()), extCharges);
					
					amountWithoutExt = AccelAeroCalculator.subtract(amountDueWithExt, extChgTotal);

					cardPayCurrencyDTO = cardPaymentInfo.getPayCurrencyDTO();
					ipgIdentificationParamsDTO = new IPGIdentificationParamsDTO(ipgId, cardPayCurrencyDTO.getPayCurrencyCode());

					paymentAssembler.addInternalCardPayment(cardPaymentInfo.getType(), cardPaymentInfo.getEDate().trim(),
							cardPaymentInfo.getNo().trim(), cardPaymentInfo.getName().trim(), cardPaymentInfo.getAddress().trim(),
							cardPaymentInfo.getSecurityCode().trim(), amountWithoutExt, AppIndicatorEnum.APP_XBE,
							TnxModeEnum.MAIL_TP_ORDER, ipgIdentificationParamsDTO, cardPayCurrencyDTO, paymentTimestamp,
							cardPaymentInfo.getName(), null, null, null, null);
				}

				if (PaxTypeTO.ADULT.equals(paxSummary.getPaxType()) && paxSummary.hasInfant()) {
					paymentAssembler.setPaxType(PaxTypeTO.PARENT);
				} else {
					paymentAssembler.setPaxType(paxSummary.getPaxType());
				}
				paymentMap.put(paxSummary.getTravelerRefNumber(), paymentAssembler);
			}

		}

		return paymentMap;
	}

	private static List<LCCClientExternalChgDTO> converToProxyExtCharges(Collection<ExternalChgDTO> extChgs) {
		List<LCCClientExternalChgDTO> extChgList = new ArrayList<LCCClientExternalChgDTO>();
		if (extChgs != null && extChgs.size() > 0) {
			for (ExternalChgDTO chg : extChgs) {
				extChgList.add(new LCCClientExternalChgDTO(chg));
			}
		}
		return extChgList;
	}

	private static BigDecimal getTotalExtCharge(List<LCCClientExternalChgDTO> extChgList) {
		BigDecimal total = BigDecimal.ZERO;
		for (LCCClientExternalChgDTO chg : extChgList) {
			total = AccelAeroCalculator.add(total, chg.getAmount());
		}
		return total;
	}

	public static Map<EXTERNAL_CHARGES, ExternalChgDTO>
			getExternalChgMap(Map<Integer, List<LCCClientExternalChgDTO>> paxExtChgMap) throws ModuleException {
		Set<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new HashSet<EXTERNAL_CHARGES>();

		for (Integer paxSeq : paxExtChgMap.keySet()) {
			for (LCCClientExternalChgDTO extChg : paxExtChgMap.get(paxSeq)) {
				colEXTERNAL_CHARGES.add(extChg.getExternalCharges());
			}

		}

		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.CREDIT_CARD);

		Map<EXTERNAL_CHARGES, ExternalChgDTO> extExternalChgDTOMap = new HashMap<EXTERNAL_CHARGES, ExternalChgDTO>();
		if (colEXTERNAL_CHARGES.size() > 0) {
			extExternalChgDTOMap = WebServicesModuleUtils.getReservationBD().getQuotedExternalCharges(colEXTERNAL_CHARGES, null,
					ChargeRateOperationType.MODIFY_ONLY);
		}
		return extExternalChgDTOMap;
	}

	public static void setExistingSegInfo(Reservation reservation,
			List<OriginDestinationInformationTO> originDestinationInformationTOs, Map<Integer, String> modifyingPnrSegRPHs)
			throws ModuleException {
		Map<Integer, List<ReservationSegmentDTO>> ondWiseExistingSegment = new HashMap<Integer, List<ReservationSegmentDTO>>();

		for (ReservationSegmentDTO existingSegment : reservation.getSegmentsView()) {
			if (!modifyingPnrSegRPHs.containsKey(existingSegment.getPnrSegId())
					&& ondWiseExistingSegment.get(existingSegment.getFareGroupId()) == null) {
				List<ReservationSegmentDTO> existingSegments = new ArrayList<ReservationSegmentDTO>();
				existingSegments.add(existingSegment);
				ondWiseExistingSegment.put(existingSegment.getFareGroupId(), existingSegments);
			} else if (!modifyingPnrSegRPHs.containsKey(existingSegment.getPnrSegId())
					&& ondWiseExistingSegment.get(existingSegment.getFareGroupId()) != null) {
				ondWiseExistingSegment.get(existingSegment.getFareGroupId()).add(existingSegment);
			} else {
				for (OriginDestinationInformationTO originDestinationInformationTO : originDestinationInformationTOs) {
					if (originDestinationInformationTO.getOrigin().equals(existingSegment.getOrigin())
							&& originDestinationInformationTO.getDestination().equals(existingSegment.getDestination())) {
						originDestinationInformationTO.getExistingFlightSegIds().add(existingSegment.getFlightSegId());
						originDestinationInformationTO.getExistingPnrSegRPHs()
								.add(modifyingPnrSegRPHs.get(existingSegment.getPnrSegId()));
						FareTO oldPerPaxFareTO = getFareTO(existingSegment);
						originDestinationInformationTO.setOldPerPaxFare(oldPerPaxFareTO.getAmount());
						List<FareTO> oldFateTOs = new ArrayList<FareTO>();
						oldFateTOs.add(oldPerPaxFareTO);
						originDestinationInformationTO.setOldPerPaxFareTOList(oldFateTOs);

						List<Integer> dateChgSegList = new ArrayList<Integer>();
						dateChgSegList.add(existingSegment.getPnrSegId());
						originDestinationInformationTO.setDateChangedResSegList(dateChgSegList);
						break;
					}
				}
			}
		}

		if (ondWiseExistingSegment.size() > 0) {
			ReservationSegment reservationSegment = reservation.getSegments().iterator().next();
			for (Integer ondGroupId : ondWiseExistingSegment.keySet()) {
				List<ReservationSegmentDTO> existingSegments = sortBySegSequnce(ondWiseExistingSegment.get(ondGroupId));
				OriginDestinationInformationTO ondInfoTO = new OriginDestinationInformationTO();
				ReservationSegmentDTO firstSegment = existingSegments.get(0);
				boolean hasMultiSeg = existingSegments.size() > 1 ? true : false;

				ondInfoTO.setOrigin(firstSegment.getOrigin());

				Date depatureDate = firstSegment.getDepartureDate();

				Date depatureDateTimeStart = CalendarUtil.getStartTimeOfDate(depatureDate);
				depatureDateTimeStart = CalendarUtil.getOfssetAddedTime(depatureDateTimeStart, 0);

				Date depatureDateTimeEnd = CalendarUtil.getEndTimeOfDate(depatureDate);
				depatureDateTimeEnd = CalendarUtil.getOfssetAddedTime(depatureDateTimeEnd, 0);

				if (hasMultiSeg) {
					ReservationSegmentDTO lastSegment = existingSegments.get(existingSegments.size() - 1);
					ondInfoTO.setDestination(lastSegment.getDestination());
				} else {
					ondInfoTO.setDestination(firstSegment.getDestination());
				}

				ondInfoTO.setPreferredDate(depatureDate);
				ondInfoTO.setDepartureDateTimeStart(depatureDateTimeStart);
				ondInfoTO.setDepartureDateTimeEnd(depatureDateTimeEnd);

				ondInfoTO.setPreferredLogicalCabin(reservationSegment.getLogicalCCCode());
				ondInfoTO.setPreferredClassOfService(reservationSegment.getCabinClassCode());
				if (firstSegment.getReturnFlag().equals("Y")) {
					ondInfoTO.setReturnFlag(true);
				}

				OriginDestinationOptionTO ondOptionTO = new OriginDestinationOptionTO();
				for (ReservationSegmentDTO tmpSegment : existingSegments) {
					FlightSegmentTO flightSegementTO = new FlightSegmentTO();
					flightSegementTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(tmpSegment));
					flightSegementTO.setSegmentCode(tmpSegment.getSegmentCode());
					flightSegementTO.setFlightNumber(tmpSegment.getFlightNo());
					ondOptionTO.getFlightSegmentList().add(flightSegementTO);
				}
				ondInfoTO.getOrignDestinationOptions().add(ondOptionTO);

				originDestinationInformationTOs.add(ondInfoTO);
			}
		}

	}

	private static FareTO getFareTO(ReservationSegmentDTO existingSegment) throws ModuleException {
		FareTO oldPerPaxFareTO = new FareTO();
		oldPerPaxFareTO.setAmount(existingSegment.getFareTO().getAdultFareAmount());
		oldPerPaxFareTO.setBookingClassCode(existingSegment.getFareTO().getBookingClassCode());
		oldPerPaxFareTO.setFareId(existingSegment.getFareTO().getFareId());
		oldPerPaxFareTO.setSegmentCode(existingSegment.getFareTO().getOndCode());
		oldPerPaxFareTO.setCarrierCode(existingSegment.getCarrierCode());
		oldPerPaxFareTO.setLogicalCCCode(existingSegment.getLogicalCCCode());
		oldPerPaxFareTO.setBulkTicketFareRule(
				!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(existingSegment.getStatus())
						&& existingSegment != null && existingSegment.getFareTO() != null
						&& existingSegment.getFareTO().isBulkTicketFareRule());

		return oldPerPaxFareTO;
	}

	private static List<ReservationSegmentDTO> sortBySegSequnce(List<ReservationSegmentDTO> resSegments) {
		Collections.sort(resSegments, new Comparator<ReservationSegmentDTO>() {
			@Override
			public int compare(ReservationSegmentDTO at1, ReservationSegmentDTO at2) {
				return (new Integer(at1.getSegmentSeq())).compareTo(at2.getSegmentSeq());
			}
		});
		return resSegments;
	}

	public static boolean hasPrivilege(Collection<String> privilegesKeys, String privilegeKey) {
		return AuthorizationUtil.hasPrivileges(privilegesKeys, privilegeKey);
	}

}
