package com.isa.thinair.webservices.core.cache.delegator.auditor.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.OTAAirPriceRQ;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.TravelerInfoSummaryType;
import org.opentravel.ota._2003._05.TravelerInformationType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webservices.core.cache.delegator.auditor.BaseAuditor;
import com.isa.thinair.webservices.core.cache.delegator.dto.JourenyInfo;
import com.isa.thinair.webservices.core.cache.util.CacheConstant;
import com.isa.thinair.webservices.core.cache.util.CacheConstant.Operation;

public class PriceQuoteAuditor extends BaseAuditor<OTAAirPriceRQ> {

	private static final Log log = LogFactory.getLog(PriceQuoteAuditor.class);

	public PriceQuoteAuditor(OTAAirPriceRQ oTAAirPriceRQ) {
		super(oTAAirPriceRQ);
	}

	@Override
	public String getAudit(Operation operation) {
		StringBuilder audit = new StringBuilder();

		try {
			List<JourenyInfo> journeyList = getOriginDestinationInfo();
			Gson gson = new GsonBuilder().create();
			String routeInfo = gson.toJson(journeyList);

			Map<String, Integer> travelerQuantity = getTravelerQuantityInfo();
			String quantity = gson.toJson(travelerQuantity);

			audit.append(CacheConstant.Audit.OPERATION.getCode());
			audit.append(operation.getCode());
			audit.append(CacheConstant.Delimeters.END_AND_CONTINUE.getCode());

			audit.append(CacheConstant.Audit.JOURNEY_INFO.getCode());
			audit.append(routeInfo);
			audit.append(CacheConstant.Delimeters.END_AND_CONTINUE.getCode());

			audit.append(CacheConstant.Audit.QUANTITY.getCode());
			audit.append(quantity);
			audit.append(CacheConstant.Delimeters.END.getCode());
		} catch (Exception e) {
			log.error("Error in PriceQuoteAuditor", e);
		}

		return audit.toString();

	}

	private List<JourenyInfo> getOriginDestinationInfo() {

		List<JourenyInfo> jourenyList = new ArrayList<JourenyInfo>();

		for (OriginDestinationOptionType originDestinationOption : this.request.getAirItinerary().getOriginDestinationOptions()
				.getOriginDestinationOption()) {
			for (BookFlightSegmentType bookFlightSegment : originDestinationOption.getFlightSegment()) {
				JourenyInfo jourenyInfo = new JourenyInfo();

				String origin = bookFlightSegment.getArrivalAirport().getLocationCode();
				String destination = bookFlightSegment.getDepartureAirport().getLocationCode();
				Date departureDate = bookFlightSegment.getDepartureDateTime().toGregorianCalendar().getTime();
				String formattedDate = CalendarUtil.formatDateYYYYMMDD(departureDate);

				jourenyInfo.setOrigin(origin);
				jourenyInfo.setDetination(destination);
				jourenyInfo.setDepartureDate(formattedDate);

				jourenyList.add(jourenyInfo);
			}
		}

		return jourenyList;

	}

	private Map<String, Integer> getTravelerQuantityInfo() {

		Map<String, Integer> quantityMap = new HashMap<String, Integer>();

		TravelerInfoSummaryType travelerInfoSummaryType = this.request.getTravelerInfoSummary();
		if (travelerInfoSummaryType != null && travelerInfoSummaryType.getAirTravelerAvail() != null) {
			for (TravelerInformationType travelerInformationType : travelerInfoSummaryType.getAirTravelerAvail()) {
				if (travelerInformationType.getPassengerTypeQuantity() != null) {
					for (PassengerTypeQuantityType passengerTypeQuantityType : travelerInformationType.getPassengerTypeQuantity()) {
						quantityMap.put(passengerTypeQuantityType.getCode(), passengerTypeQuantityType.getQuantity());
					}
				}
			}
		}

		return quantityMap;
	}

}
