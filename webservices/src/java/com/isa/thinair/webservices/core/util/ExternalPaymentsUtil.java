package com.isa.thinair.webservices.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.opentravel.ota._2003._05.AAOTATransactionsReconRQ;
import org.opentravel.ota._2003._05.AAOTATransactionsReconRS;
import org.opentravel.ota._2003._05.AAPNRPaymentTransactionType;

import com.isa.thinair.airreservation.api.dto.PNRExtTransactionsTO;
import com.isa.thinair.airreservation.api.model.ExternalPaymentTnx;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;

/**
 * External payment transactions related functionalities.
 * 
 * @author Nasly
 */
public class ExternalPaymentsUtil extends BaseUtil {

	/**
	 * Reconciles AA transactions with bank transactions.
	 * 
	 * @param reconRQ
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	public static AAOTATransactionsReconRS reconExternalPayTransactions(AAOTATransactionsReconRQ reconRQ)
			throws WebservicesException, ModuleException {
		Collection<PNRExtTransactionsTO> pnrExtTransactionsTO = prepareAAPNRExtTransactionsTOs(reconRQ
				.getPNRPaymentTransactions());

		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();

		WebServicesModuleUtils.getReservationBD().reconcileExtPayTransactions(pnrExtTransactionsTO, false,
				reconRQ.getStartTimestamp().toGregorianCalendar().getTime(),
				reconRQ.getEndTimestamp().toGregorianCalendar().getTime(), principal.getAgentCode()); // FIXME -
																										// Separate
																										// agency
																										// identification
																										// by each bank
																										// to be done.

		AAOTATransactionsReconRS reconRS = new AAOTATransactionsReconRS();
		return reconRS;
	}

	/**
	 * Prepares collection of PNRExtTransactionsTO
	 * 
	 * @param wsPNRPaymentTransactions
	 * @return
	 */
	private static Collection<PNRExtTransactionsTO> prepareAAPNRExtTransactionsTOs(
			List<AAPNRPaymentTransactionType> wsPNRPaymentTransactions) {
		Collection<PNRExtTransactionsTO> pnrExtTransactionsTOs = new ArrayList<PNRExtTransactionsTO>();
		for (AAPNRPaymentTransactionType wsPNRTnxs : wsPNRPaymentTransactions) {
			PNRExtTransactionsTO aaPNRTnxs = new PNRExtTransactionsTO();
			aaPNRTnxs.setPnr(wsPNRTnxs.getPNRNo());
			for (AAPNRPaymentTransactionType.PaymentTransaction wsTnx : wsPNRTnxs.getPaymentTransaction()) {
				ExternalPaymentTnx aaTnx = new ExternalPaymentTnx();

				aaTnx.setPnr(wsPNRTnxs.getPNRNo());
				aaTnx.setBalanceQueryKey(wsTnx.getAARefNo());
				aaTnx.setAmount(AccelAeroCalculator.parseBigDecimal(wsTnx.getAmount()));
				aaTnx.setChannel(wsTnx.getBankChannel());
				aaTnx.setExternalPayStatus(OTAUtils.getAAExtPayTnxStatus(wsTnx.getBankStatus()));
				aaTnx.setExternalPayId(wsTnx.getBankRefNo());
				aaTnx.setExternalTnxEndTimestamp(wsTnx.getTimestamp().toGregorianCalendar().getTime());

				aaPNRTnxs.addExtPayTransactions(aaTnx);
			}
			pnrExtTransactionsTOs.add(aaPNRTnxs);
		}
		return pnrExtTransactionsTOs;
	}
}
