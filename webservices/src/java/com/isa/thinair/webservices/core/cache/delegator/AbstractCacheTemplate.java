package com.isa.thinair.webservices.core.cache.delegator;

import java.util.Map;

import org.springframework.cache.CacheManager;

import com.isa.thinair.webservices.core.cache.delegator.throttle.ThrottleManager;
import com.isa.thinair.webservices.core.cache.util.CacheCommonUtil;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;

public abstract class AbstractCacheTemplate<T, V> implements StatelessCacheTemplate<T, V> {	

	private CacheManager cacheManager = null;

	private String cacheOperationCode;

	private ThrottleManager throttleManager;

	private boolean enabledCache = true;

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	@Override
	public CacheManager getCacheManager() {
		return cacheManager;
	}

	public String getCacheOperationCode() {
		return cacheOperationCode;
	}

	public void setCacheOperationCode(String cacheOperationCode) {
		this.cacheOperationCode = cacheOperationCode;
	}

	@Override
	public String getCacheName() {
		return CacheCommonUtil.getFullQualifiedCacheName(this.cacheOperationCode);
	}

	/**
	 * @return the throttleManager
	 */
	public ThrottleManager getThrottleManager() {
		return throttleManager;
	}

	/**
	 * @param throttleManager
	 *            the throttleManager to set
	 */
	public void setThrottleManager(ThrottleManager throttleManager) {
		this.throttleManager = throttleManager;
	}

	public boolean isEnabledCache() {
		return enabledCache;
	}

	public void setEnabledCache(boolean enabledCache) {
		this.enabledCache = enabledCache;
	}

	@SuppressWarnings("unchecked")
	@Override
	public  T composeCacheStoreResponse(T response) {
		Map<String, Object> transactionData = ThreadLocalData.getCurrentTransaction().getAllParameters();
		CacheResponse<T> cacheResponse = new CacheResponse<>(response, transactionData);
		return (T) cacheResponse;
	}
	
}
