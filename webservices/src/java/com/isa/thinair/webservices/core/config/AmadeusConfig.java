package com.isa.thinair.webservices.core.config;

import java.util.Map;

public class AmadeusConfig {

	private boolean bookingClassMappingEnabled;

	private String defaultBookingClass;

	private String defaultOneWayFareCode;

	private String defaultReturnFareCode;

	private boolean creditCardBypassEnabled;

	private Map bypassingCreditCardNoMap;

	private String replacingCreditCardNo;

	private String replacingCreditCardType;

	private String defaultLanguage;

	public boolean isBookingClassMappingEnabled() {
		return bookingClassMappingEnabled;
	}

	public void setBookingClassMappingEnabled(boolean bookingClassMappingEnabled) {
		this.bookingClassMappingEnabled = bookingClassMappingEnabled;
	}

	public String getDefaultBookingClass() {
		return defaultBookingClass;
	}

	public void setDefaultBookingClass(String defaultBookingClass) {
		this.defaultBookingClass = defaultBookingClass;
	}

	public String getDefaultOneWayFareCode() {
		return defaultOneWayFareCode;
	}

	public void setDefaultOneWayFareCode(String defaultOneWayFareCode) {
		this.defaultOneWayFareCode = defaultOneWayFareCode;
	}

	public String getDefaultReturnFareCode() {
		return defaultReturnFareCode;
	}

	public void setDefaultReturnFareCode(String defaultReturnFareCode) {
		this.defaultReturnFareCode = defaultReturnFareCode;
	}

	public boolean isCreditCardBypassEnabled() {
		return creditCardBypassEnabled;
	}

	public void setCreditCardBypassEnabled(boolean creditCardBypassEnabled) {
		this.creditCardBypassEnabled = creditCardBypassEnabled;
	}

	public Map getBypassingCreditCardNoMap() {
		return bypassingCreditCardNoMap;
	}

	public void setBypassingCreditCardNoMap(Map bypassingCreditCardNoMap) {
		this.bypassingCreditCardNoMap = bypassingCreditCardNoMap;
	}

	public String getReplacingCreditCardNo() {
		return replacingCreditCardNo;
	}

	public void setReplacingCreditCardNo(String replacingCreditCardNo) {
		this.replacingCreditCardNo = replacingCreditCardNo;
	}

	public String getReplacingCreditCardType() {
		return replacingCreditCardType;
	}

	public void setReplacingCreditCardType(String replacingCreditCardType) {
		this.replacingCreditCardType = replacingCreditCardType;
	}

	public String getDefaultLanguage() {
		return defaultLanguage;
	}

	public void setDefaultLanguage(String defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

}
