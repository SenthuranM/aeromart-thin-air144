package com.isa.thinair.webservices.core.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.opentravel.ota._2003._05.AirFeeType;
import org.opentravel.ota._2003._05.AirItineraryPricingInfoType;
import org.opentravel.ota._2003._05.AirTaxType;
import org.opentravel.ota._2003._05.BookFlightSegmentType;
import org.opentravel.ota._2003._05.FareType;
import org.opentravel.ota._2003._05.FlightSegmentBaseType;
import org.opentravel.ota._2003._05.IOTACodeTables;
import org.opentravel.ota._2003._05.OperatingAirlineType;
import org.opentravel.ota._2003._05.OriginDestinationOptionType;
import org.opentravel.ota._2003._05.PTCFareBreakdownType;
import org.opentravel.ota._2003._05.PTCFareBreakdownType.FareBasisCodes;
import org.opentravel.ota._2003._05.PTCFareBreakdownType.TicketDesignators;
import org.opentravel.ota._2003._05.PTCFareBreakdownType.TicketDesignators.TicketDesignator;
import org.opentravel.ota._2003._05.PassengerTypeQuantityType;
import org.opentravel.ota._2003._05.PricedItineraryType;
import org.opentravel.ota._2003._05.PricingSourceType;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.util.FareQuoteUtil;
import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.webplatform.api.dto.PaxSet;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.ExternalChargeUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.core.config.WPModuleUtils;
import com.isa.thinair.webservices.api.exception.WebservicesException;
import com.isa.thinair.webservices.api.util.WebservicesConstants.OTAConstants;
import com.isa.thinair.webservices.core.session.AAUserSession;
import com.isa.thinair.webservices.core.threadlocal.ThreadLocalData;
import com.isa.thinair.webservices.core.transaction.Transaction;

/**
 * Prepare PricedItinerary from SelectedFlightDTO
 * 
 * @author Mohamed Nasly
 */
public class MyIDTrvlPricedItineraryUtil {

	private static GlobalConfig globalConfig = WebServicesModuleUtils.getGlobalConfig();

	public static void preparePricedItinerary(PricedItineraryType pricedItinerary, SelectedFlightDTO selectedFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO, boolean isFixedPriceQuote, Map<String, String[]> refSegRPHMap,
			Map<String, List<String>> refPaxRPHMap) throws WebservicesException, ModuleException {
		// set flight segments info
		int fareType = selectedFlightDTO.getFareType();

		boolean isOutboundHasONDFare = false;
		boolean isInboudHasONDFare = false;
		if (fareType == FareTypes.OND_FARE || fareType == FareTypes.OND_FARE_AND_PER_FLIGHT_FARE) {
			isOutboundHasONDFare = true;
		}

		OriginDestinationOptionType originDestinationOption = new OriginDestinationOptionType();
		pricedItinerary.getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption().add(originDestinationOption);

		// pricedItinerary.getAirItinerary().getOriginDestinationOptions().
		// Itinerary
		addFlightSegments(pricedItinerary, selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND), isOutboundHasONDFare,
				refSegRPHMap);

		if (selectedFlightDTO.isReturnFareQuote()) {
			if (fareType == FareTypes.OND_FARE || fareType == FareTypes.PER_FLIGHT_FARE_AND_OND_FARE) {
				isInboudHasONDFare = true;
			}
			addFlightSegments(pricedItinerary, selectedFlightDTO.getSelectedOndFlight(OndSequence.IN_BOUND), isInboudHasONDFare,
					refSegRPHMap);
		}

		// set individual passenger fare/taxes breakdowns and total fare/taxes
		setItineraryPricingInfo(pricedItinerary, selectedFlightDTO, availableFlightSearchDTO, isFixedPriceQuote, refSegRPHMap,
				refPaxRPHMap);
	}

	/**
	 * Sets indivdual fare breakdowns and total price for itinerary
	 * 
	 * @param pricedItinerary
	 * @param selectedFlightDTO
	 * @param availableFlightSearchDTO
	 * @param refPaxRPHMap
	 *            TODO
	 * @param flexiSelectionCriteria
	 *            : The flexibility selection criteria.
	 * @throws WebservicesException
	 */
	private static void setItineraryPricingInfo(PricedItineraryType pricedItinerary, SelectedFlightDTO selectedFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO, boolean isFixedFare, Map<String, String[]> refSegRPHMap,
			Map<String, List<String>> refPaxRPHMap) throws WebservicesException, ModuleException {

		// fares and tax for single adult pax is set
		AirItineraryPricingInfoType airItineraryPricingInfo = new AirItineraryPricingInfoType();
		pricedItinerary.setAirItineraryPricingInfo(airItineraryPricingInfo);
		airItineraryPricingInfo.setPricingSource(PricingSourceType.PUBLISHED);

		// Calculating the Handling fare for the agent
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
		colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.HANDLING_CHARGE);
		ReservationBD reservationBD = WPModuleUtils.getReservationBD();
		@SuppressWarnings("unchecked")
		Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap = reservationBD.getQuotedExternalCharges(colEXTERNAL_CHARGES,
				null, ChargeRateOperationType.MAKE_ONLY);

		// calculate the handling fee
		if (externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE) != null) {
			calculateHandlingFee(selectedFlightDTO, availableFlightSearchDTO,
					externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE));
		}

		// set Passenger Type, fare and taxes/surcharges breakdown
		setPassengerFareBreakdowns(airItineraryPricingInfo, selectedFlightDTO, availableFlightSearchDTO, externalChargesMap,
				isFixedFare, refSegRPHMap, refPaxRPHMap);
	}

	/**
	 * @param selectedFlightDTO
	 * @param availableFlightSearchDTO
	 * @param externalChgDTO
	 * @param isFixedFare
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	private static void calculateHandlingFee(SelectedFlightDTO selectedFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO, ExternalChgDTO externalChgDTO) throws WebservicesException,
			ModuleException {

		PaxSet paxSet = new PaxSet(availableFlightSearchDTO.getAdultCount(), availableFlightSearchDTO.getChildCount(),
				availableFlightSearchDTO.getInfantCount());

		ExternalChargeUtil.calculateExternalChargeAmount(selectedFlightDTO.getOndFareDTOs(false), paxSet, externalChgDTO,
				selectedFlightDTO.isReturnFareQuote());

	}

	/**
	 * Sets flight segments information
	 * 
	 * @param pricedItinerary
	 * @param aaFlightSegmentDTOs
	 * @throws WebservicesException
	 */
	private static void addFlightSegments(PricedItineraryType pricedItinerary,
			AvailableIBOBFlightSegment availableIBOBFlightSegment, boolean isSingleFare, Map<String, String[]> refSegRPHMap)
			throws WebservicesException, ModuleException {
		Collection<FlightSegmentDTO> aaFlightSegmentDTOs = availableIBOBFlightSegment.getFlightSegmentDTOs();
		// OriginDestinationOptionType originDestinationOption = null;
		for (FlightSegmentDTO aaFlightSegmentDTO : aaFlightSegmentDTOs) {
			// if (!isSingleFare || (isSingleFare && originDestinationOption == null)) {
			// originDestinationOption = new OriginDestinationOptionType();
			// pricedItinerary.getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption()
			// .add(originDestinationOption);
			// }

			BookFlightSegmentType bookFlightSegment = new BookFlightSegmentType();
			pricedItinerary.getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption().get(0)
					.getFlightSegment().add(bookFlightSegment);
			bookFlightSegment.setRPH(refSegRPHMap.get(MyIDTrvlCommonUtil.getTmpSegRefNo(aaFlightSegmentDTO))[0]);
			bookFlightSegment.setResBookDesigCode(refSegRPHMap.get(MyIDTrvlCommonUtil.getTmpSegRefNo(aaFlightSegmentDTO))[1]);

			GregorianCalendar departureDateTimeCal = new GregorianCalendar();
			departureDateTimeCal.setTime(aaFlightSegmentDTO.getDepartureDateTime());
			bookFlightSegment.setDepartureDateTime(CommonUtil.getXMLGregorianCalendar(departureDateTimeCal));

			GregorianCalendar arrivalDateTimeCal = new GregorianCalendar();
			arrivalDateTimeCal.setTime(aaFlightSegmentDTO.getArrivalDateTime());
			bookFlightSegment.setArrivalDateTime(CommonUtil.getXMLGregorianCalendar(arrivalDateTimeCal));

			bookFlightSegment.setFlightNumber(aaFlightSegmentDTO.getFlightNumber());

			OperatingAirlineType operatingAirline = new OperatingAirlineType();
			operatingAirline.setCode(bookFlightSegment.getFlightNumber().substring(0, 2));
			operatingAirline.setFlightNumber(bookFlightSegment.getFlightNumber().substring(2));
			operatingAirline.setCodeContext(OTAConstants.CODE_CONTEXT_IATA);
			bookFlightSegment.setOperatingAirline(operatingAirline);

			String[] segmentCodeList = aaFlightSegmentDTO.getSegmentCode().split("/");

			FlightSegmentBaseType.DepartureAirport departureAirport = new FlightSegmentBaseType.DepartureAirport();
			departureAirport.setLocationCode(segmentCodeList[0]);
			departureAirport.setCodeContext(OTAConstants.CODE_CONTEXT_IATA);

			FlightSegmentBaseType.ArrivalAirport arrivalAirport = new FlightSegmentBaseType.ArrivalAirport();
			arrivalAirport.setLocationCode(segmentCodeList[segmentCodeList.length - 1]);
			arrivalAirport.setCodeContext(OTAConstants.CODE_CONTEXT_IATA);

			bookFlightSegment.setDepartureAirport(departureAirport);
			bookFlightSegment.setArrivalAirport(arrivalAirport);

			if (segmentCodeList.length > 2) {
				bookFlightSegment.setStopQuantity(BigInteger.valueOf(segmentCodeList.length - 2));
			}
		}
	}

	/**
	 * Sets each passenger type wise fare, tax break down and surcharges(fees) breakdown
	 * 
	 * @param airItineraryPricingInfo
	 * @param selectedFlightDTO
	 * @param availableFlightSearchDTO
	 * @param refPaxRPHMap
	 *            TODO
	 * @param flexiSelectionCriteria
	 *            : The selected flexibilites for the price quote.
	 * @throws WebservicesException
	 */
	private static void setPassengerFareBreakdowns(AirItineraryPricingInfoType airItineraryPricingInfo,
			SelectedFlightDTO selectedFlightDTO, AvailableFlightSearchDTO availableFlightSearchDTO,
			Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap, boolean isFixedFare, Map<String, String[]> refSegRPHMap,
			Map<String, List<String>> refPaxRPHMap) throws WebservicesException, ModuleException {

		AirItineraryPricingInfoType.PTCFareBreakdowns ptcFareBreakdowns = new AirItineraryPricingInfoType.PTCFareBreakdowns();
		// airItineraryPricingInfo.setPTCFareBreakdowns(ptcFareBreakdowns);
		BigDecimal totalInSelectedCurByFareBreakdown = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalSelecteExternalCharges = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalSelecteExternalChargesInSelCur = AccelAeroCalculator.getDefaultBigDecimalZero();

		Collection<String> paxTypes = new ArrayList<String>();
		if (availableFlightSearchDTO.getAdultCount() > 0)
			paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_ADULT));
		if (availableFlightSearchDTO.getChildCount() > 0)
			paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_CHILD));
		if (availableFlightSearchDTO.getInfantCount() > 0)
			paxTypes.add(CommonUtil.getOTACodeValue(IOTACodeTables.AccelaeroPaxTypeCodes_PTC_INFANT));

		// calculating the per pax handling charge
		int handlingFeePaxCount = availableFlightSearchDTO.getAdultCount() + availableFlightSearchDTO.getChildCount();
		ExternalChgDTO externalChgDTO = externalChargesMap.get(EXTERNAL_CHARGES.HANDLING_CHARGE);
		BigDecimal handlingCharge = externalChgDTO.getAmount();
		handlingCharge = OTAUtils.getServiceChargeWithJNTax(EXTERNAL_CHARGES.HANDLING_CHARGE, handlingCharge);

		BigDecimal[] perPaxHandlingCharges = AccelAeroCalculator.roundAndSplit(handlingCharge, handlingFeePaxCount);

		int paxSeq = 0;
		int parentSeq = 0;
		int count = 0; // to get the per pax handling fare from the array.

		boolean isHoldAllowed = true;
		AvailableIBOBFlightSegment availableOutboundFlightSegment = selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND);
		AvailableIBOBFlightSegment availableInboundFlightSegment = selectedFlightDTO.getSelectedOndFlight(OndSequence.IN_BOUND);

		Collection<String> privilegeKeys = ThreadLocalData.getCurrentUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
		if (!AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_OVERRIDE_ONHOLD)) {
			isHoldAllowed = WSReservationUtil.isHoldAllowed(selectedFlightDTO.getOndFareDTOs(false));
		}

		UserPrincipal principal = ThreadLocalData.getCurrentUserPrincipal();
		boolean showPriceInselectedCurrency = OTAUtils.isShowPriceInselectedCurrency();
		String currencyCode = null;
		if (showPriceInselectedCurrency) {
			if (availableFlightSearchDTO.getPreferredCurrencyCode() != null
					&& OTAUtils.isPreferredCurrencyCodeSupportedByPC(availableFlightSearchDTO.getPreferredCurrencyCode())) {
				currencyCode = availableFlightSearchDTO.getPreferredCurrencyCode();
			} else {
				currencyCode = principal.getAgentCurrencyCode();
			}
		}

		Map<String, List<ExternalChgDTO>> quotedExternalCharges = (Map<String, List<ExternalChgDTO>>) ThreadLocalData
				.getCurrentTnxParam(Transaction.QUOTED_EXTERNAL_CHARGES);

		List<PTCFareBreakdownType> fareBreakdowns = new ArrayList<PTCFareBreakdownType>();

		if (availableOutboundFlightSegment == null) {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_NO_AVAILABILITY, "Flight not available");
		}

		int adultCount = availableFlightSearchDTO.getAdultCount();
		int childCount = availableFlightSearchDTO.getChildCount();
		int infantCount = availableFlightSearchDTO.getInfantCount();

		BigDecimal totalBaseFareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalFareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		// Calculate CC charge per pax/segment
		// Segment wise amount

		if (availableOutboundFlightSegment.isDirectFlight()) {
			AvailableFlightSegment availableFlightSegment = (AvailableFlightSegment) availableOutboundFlightSegment;
			String segmentRPH = refSegRPHMap.get(MyIDTrvlCommonUtil.getTmpSegRefNo(availableFlightSegment.getFlightSegmentDTO()))[0];

			// Generate breakdown for each pax type for the segment
			String selectedLogicalCabinClass = availableFlightSegment.getSelectedLogicalCabinClass();
			if (selectedLogicalCabinClass != null && availableFlightSegment.getFare(selectedLogicalCabinClass) != null) {

				Collection<QuotedChargeDTO> outboundCharges = availableFlightSegment
						.getEffectiveCharges(selectedLogicalCabinClass);

				if (adultCount > 0) {
					PTCFareBreakdownType ptcFareBreakdown = new PTCFareBreakdownType();
					setSegmentPriceBreakdownForPaxType(ptcFareBreakdown, availableFlightSegment, adultCount, segmentRPH,
							refPaxRPHMap.get(PaxTypeTO.ADULT), currencyCode, PaxTypeTO.ADULT, outboundCharges);
					ptcFareBreakdowns.getPTCFareBreakdown().add(ptcFareBreakdown);
					totalBaseFareAmount = AccelAeroCalculator.add(totalBaseFareAmount, AccelAeroCalculator.multiply(
							ptcFareBreakdown.getPassengerFare().getBaseFare().getAmount(), adultCount));
					totalFareAmount = AccelAeroCalculator.add(totalFareAmount, AccelAeroCalculator.multiply(ptcFareBreakdown
							.getPassengerFare().getTotalFare().getAmount(), adultCount));

				}

				if (childCount > 0) {
					PTCFareBreakdownType ptcFareBreakdown = new PTCFareBreakdownType();
					setSegmentPriceBreakdownForPaxType(ptcFareBreakdown, availableFlightSegment, childCount, segmentRPH,
							refPaxRPHMap.get(PaxTypeTO.CHILD), currencyCode, PaxTypeTO.CHILD, outboundCharges);
					ptcFareBreakdowns.getPTCFareBreakdown().add(ptcFareBreakdown);
					totalBaseFareAmount = AccelAeroCalculator.add(totalBaseFareAmount, AccelAeroCalculator.multiply(
							ptcFareBreakdown.getPassengerFare().getBaseFare().getAmount(), childCount));
					totalFareAmount = AccelAeroCalculator.add(totalFareAmount, AccelAeroCalculator.multiply(ptcFareBreakdown
							.getPassengerFare().getTotalFare().getAmount(), childCount));
				}

				if (infantCount > 0) {
					PTCFareBreakdownType ptcFareBreakdown = new PTCFareBreakdownType();
					setSegmentPriceBreakdownForPaxType(ptcFareBreakdown, availableFlightSegment, infantCount, segmentRPH,
							refPaxRPHMap.get(PaxTypeTO.INFANT), currencyCode, PaxTypeTO.INFANT, outboundCharges);
					ptcFareBreakdowns.getPTCFareBreakdown().add(ptcFareBreakdown);
					totalBaseFareAmount = AccelAeroCalculator.add(totalBaseFareAmount, AccelAeroCalculator.multiply(
							ptcFareBreakdown.getPassengerFare().getBaseFare().getAmount(), infantCount));
					totalFareAmount = AccelAeroCalculator.add(totalFareAmount, AccelAeroCalculator.multiply(ptcFareBreakdown
							.getPassengerFare().getTotalFare().getAmount(), infantCount));
				}

			} else {
				throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_GENERAL_FAILURE_OCCURRED, "General Error");
			}

		} else if (!availableOutboundFlightSegment.isDirectFlight()) {
			// TODO:
		}

		if (availableFlightSearchDTO.isReturnFlag()) {

			if (availableInboundFlightSegment.isDirectFlight()) {
				AvailableFlightSegment availableFlightSegmentRet = (AvailableFlightSegment) availableInboundFlightSegment;
				String selectedLogicalCabinClass = availableFlightSegmentRet.getSelectedLogicalCabinClass();
				String segmentRPH = refSegRPHMap.get(MyIDTrvlCommonUtil.getTmpSegRefNo(availableFlightSegmentRet
						.getFlightSegmentDTO()))[0];

				// Generate breakdown for each pax type for the segment
				if (selectedLogicalCabinClass != null && availableFlightSegmentRet.getFare(selectedLogicalCabinClass) != null) {
					Collection<QuotedChargeDTO> inboundCharges = availableFlightSegmentRet
							.getEffectiveCharges(selectedLogicalCabinClass);
					if (adultCount > 0) {
						PTCFareBreakdownType ptcFareBreakdown = new PTCFareBreakdownType();
						setSegmentPriceBreakdownForPaxType(ptcFareBreakdown, availableFlightSegmentRet, adultCount, segmentRPH,
								refPaxRPHMap.get(PaxTypeTO.ADULT), currencyCode, PaxTypeTO.ADULT, inboundCharges);
						ptcFareBreakdowns.getPTCFareBreakdown().add(ptcFareBreakdown);
						totalBaseFareAmount = AccelAeroCalculator.add(totalBaseFareAmount, AccelAeroCalculator.multiply(
								ptcFareBreakdown.getPassengerFare().getBaseFare().getAmount(), adultCount));
						totalFareAmount = AccelAeroCalculator.add(totalFareAmount, AccelAeroCalculator.multiply(ptcFareBreakdown
								.getPassengerFare().getTotalFare().getAmount(), adultCount));

					}

					if (childCount > 0) {
						PTCFareBreakdownType ptcFareBreakdown = new PTCFareBreakdownType();
						setSegmentPriceBreakdownForPaxType(ptcFareBreakdown, availableFlightSegmentRet, childCount, segmentRPH,
								refPaxRPHMap.get(PaxTypeTO.CHILD), currencyCode, PaxTypeTO.CHILD, inboundCharges);
						ptcFareBreakdowns.getPTCFareBreakdown().add(ptcFareBreakdown);
						totalBaseFareAmount = AccelAeroCalculator.add(totalBaseFareAmount, AccelAeroCalculator.multiply(
								ptcFareBreakdown.getPassengerFare().getBaseFare().getAmount(), childCount));
						totalFareAmount = AccelAeroCalculator.add(totalFareAmount, AccelAeroCalculator.multiply(ptcFareBreakdown
								.getPassengerFare().getTotalFare().getAmount(), childCount));
					}

					if (infantCount > 0) {
						PTCFareBreakdownType ptcFareBreakdown = new PTCFareBreakdownType();
						setSegmentPriceBreakdownForPaxType(ptcFareBreakdown, availableFlightSegmentRet, infantCount, segmentRPH,
								refPaxRPHMap.get(PaxTypeTO.INFANT), currencyCode, PaxTypeTO.INFANT, inboundCharges);
						ptcFareBreakdowns.getPTCFareBreakdown().add(ptcFareBreakdown);
						totalBaseFareAmount = AccelAeroCalculator.add(totalBaseFareAmount, AccelAeroCalculator.multiply(
								ptcFareBreakdown.getPassengerFare().getBaseFare().getAmount(), infantCount));
						totalFareAmount = AccelAeroCalculator.add(totalFareAmount, AccelAeroCalculator.multiply(ptcFareBreakdown
								.getPassengerFare().getTotalFare().getAmount(), infantCount));
					}

				} else {
					throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_GENERAL_FAILURE_OCCURRED, "General Error");
				}

			} else if (!availableInboundFlightSegment.isDirectFlight()) {
				// TODO:
			}
		}

		FareType itinearyFareInfo = new FareType();
		airItineraryPricingInfo.setItinTotalFare(itinearyFareInfo);

		FareType.BaseFare baseFare = new FareType.BaseFare();
		itinearyFareInfo.setBaseFare(baseFare);
		OTAUtils.setAmountAndDefaultCurrency(baseFare, totalBaseFareAmount);

		FareType.TotalFare totalFare = new FareType.TotalFare();
		itinearyFareInfo.setTotalFare(totalFare);
		OTAUtils.setAmountAndDefaultCurrency(totalFare, totalFareAmount);

		airItineraryPricingInfo.setPTCFareBreakdowns(ptcFareBreakdowns);

	}

	private static void setSegmentPriceBreakdownForPaxType(PTCFareBreakdownType ptcFareBreakdown,
			AvailableFlightSegment availableFlightSegment, int paxCount, String segmentRPH, List<String> paxRPHs,
			String currencyCode, String paxTypeCode, Collection<QuotedChargeDTO> allCharges) throws WebservicesException,
			ModuleException {

		PassengerTypeQuantityType passengerTypeQuantity = new PassengerTypeQuantityType();
		passengerTypeQuantity.setQuantity(paxCount);
		ptcFareBreakdown.setPassengerTypeQuantity(passengerTypeQuantity);
		FareBasisCodes fareBasisCodes = new FareBasisCodes();
		fareBasisCodes.getFareBasisCode().add(
				availableFlightSegment.getFare(availableFlightSegment.getSelectedLogicalCabinClass()).getFareBasisCode());
		ptcFareBreakdown.setFareBasisCodes(fareBasisCodes);
		TicketDesignators ticketDesignators = new TicketDesignators();
		TicketDesignator ticketDesignator = new TicketDesignator();
		ticketDesignator.setFlightRefRPH(segmentRPH);
		ticketDesignators.getTicketDesignator().add(ticketDesignator);
		ptcFareBreakdown.setTicketDesignators(ticketDesignators);

		FareType passengerFareInfo = new FareType();
		ptcFareBreakdown.setPassengerFare(passengerFareInfo);

		// base fare for single pax
		FareType.BaseFare baseFare = new FareType.BaseFare();
		passengerFareInfo.setBaseFare(baseFare);

		BigDecimal totalPerPax = BigDecimal.ZERO;
		BigDecimal fareAmount = new BigDecimal(availableFlightSegment.getFare(
				availableFlightSegment.getSelectedLogicalCabinClass()).getFareAmount(paxTypeCode));

		OTAUtils.setAmountAndDefaultCurrency(baseFare, fareAmount);
		totalPerPax = AccelAeroCalculator.add(totalPerPax, fareAmount);

		if (allCharges != null && allCharges.size() > 0) {
			for (QuotedChargeDTO charge : allCharges) {

				Set<String> applicablePaxTypes = FareQuoteUtil.populatePaxTypes(charge.getApplicableTo());

				if (applicablePaxTypes.contains(paxTypeCode)) {
					if (ChargeGroups.TAX.equals(charge.getChargeGroupCode())) {

						if (passengerFareInfo.getTaxes() == null) {
							FareType.Taxes ptcTaxes = new FareType.Taxes();
							passengerFareInfo.setTaxes(ptcTaxes);
						}

						AirTaxType taxType = new AirTaxType();
						passengerFareInfo.getTaxes().getTax().add(taxType);
						taxType.setTaxCode(charge.getChargeCode());
						BigDecimal taxAmount = AccelAeroCalculator.parseBigDecimal(charge.getEffectiveChargeAmount(paxTypeCode));
						OTAUtils.setAmountAndDefaultCurrency(taxType, taxAmount);
						totalPerPax = AccelAeroCalculator.add(totalPerPax, taxAmount);

					} else {

						if (passengerFareInfo.getFees() == null) {
							FareType.Fees ptcFees = new FareType.Fees();
							passengerFareInfo.setFees(ptcFees);
						}

						AirFeeType feeType = new AirFeeType();
						passengerFareInfo.getFees().getFee().add(feeType);
						feeType.setFeeCode(charge.getChargeCode());
						BigDecimal feeAmount = AccelAeroCalculator.parseBigDecimal(charge.getEffectiveChargeAmount(paxTypeCode));
						OTAUtils.setAmountAndDefaultCurrency(feeType, feeAmount);
						totalPerPax = AccelAeroCalculator.add(totalPerPax, feeAmount);
					}
				}

			}
		}

		FareType.TotalFare totalFare = new FareType.TotalFare();
		passengerFareInfo.setTotalFare(totalFare);

		if (currencyCode != null) {
			OTAUtils.setAmountAndCurrency(totalFare, totalPerPax, currencyCode);
		} else {
			OTAUtils.setAmountAndDefaultCurrency(totalFare, totalPerPax);

		}

		if (paxRPHs != null && paxRPHs.size() > 0) {

			for (String rph : paxRPHs) {
				PTCFareBreakdownType.TravelerRefNumber travelerRefNumber = new PTCFareBreakdownType.TravelerRefNumber();
				travelerRefNumber.setRPH(rph);
				ptcFareBreakdown.getTravelerRefNumber().add(travelerRefNumber);
			}

		} else {
			throw new WebservicesException(IOTACodeTables.ErrorCodes_ERR_GENERAL_FAILURE_OCCURRED, "Invalid Pax RPHs");
		}
	}

}
