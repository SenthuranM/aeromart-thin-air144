package com.isa.thinair.webservices.core.adaptor;

public interface Adaptor<S, T> {
	T adapt(S source);
}
