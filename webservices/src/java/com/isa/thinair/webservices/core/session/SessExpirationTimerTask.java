package com.isa.thinair.webservices.core.session;

import java.util.TimerTask;

import com.isa.thinair.webservices.core.util.WebServicesModuleUtils;

/**
 * Time Task for invalidating and removing expired user sessions.
 * 
 * @author Mohamed Nasly
 */
public class SessExpirationTimerTask extends TimerTask {

	@Override
	public void run() {
		WebServicesModuleUtils.getSessionStore().removeExpiredUserSessions();
	}
}
